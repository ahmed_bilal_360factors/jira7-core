package com.atlassian.jira.util.system;

import com.atlassian.collectors.CollectorsUtil;
import com.atlassian.core.util.ClassLoaderUtils;
import com.atlassian.jdk.utilities.JvmProperties;
import com.atlassian.jdk.utilities.exception.InvalidVersionException;
import com.atlassian.jdk.utilities.runtimeinformation.MemoryInformation;
import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.bc.license.JiraLicenseService;
import com.atlassian.jira.cache.HashRegistryCache;
import com.atlassian.jira.cluster.ClusterManager;
import com.atlassian.jira.cluster.Node;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.LocaleManager;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.config.properties.JiraProperties;
import com.atlassian.jira.config.util.JiraHome;
import com.atlassian.jira.configurableobjects.ConfigurableObjectUtil;
import com.atlassian.jira.datetime.DateTimeFormatter;
import com.atlassian.jira.datetime.DateTimeFormatterFactory;
import com.atlassian.jira.datetime.DateTimeStyle;
import com.atlassian.jira.issue.attachment.AttachmentConstants;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.security.auth.trustedapps.TrustedApplicationInfo;
import com.atlassian.jira.security.auth.trustedapps.TrustedApplicationService;
import com.atlassian.jira.security.auth.trustedapps.TrustedApplicationUtil;
import com.atlassian.jira.service.JiraService;
import com.atlassian.jira.service.JiraServiceContainer;
import com.atlassian.jira.service.ServiceManager;
import com.atlassian.jira.service.services.analytics.JiraStartStopAnalyticHelper;
import com.atlassian.jira.startup.PluginComparator;
import com.atlassian.jira.upgrade.UpgradeVersionHistoryItem;
import com.atlassian.jira.upgrade.UpgradeVersionHistoryManager;
import com.atlassian.jira.util.BuildUtilsInfo;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.collect.MapBuilder;
import com.atlassian.jira.util.log.JiraLogLocator;
import com.atlassian.modzdetector.Modifications;
import com.atlassian.modzdetector.ModzRegistryException;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import org.apache.commons.lang.StringUtils;
import org.ofbiz.core.entity.GenericValue;
import org.ofbiz.core.entity.config.EntityConfigUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import webwork.action.ActionContext;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static com.atlassian.jira.component.ComponentAccessor.getComponent;
import static com.atlassian.jira.component.ComponentAccessor.getComponentSafely;
import static com.atlassian.jira.util.dbc.Assertions.notNull;
import static java.util.Arrays.asList;

/**
 * Implementation for the ExtendedSystemInfoUtils interface.
 *
 * @since v3.13
 */
public class ExtendedSystemInfoUtilsImpl implements ExtendedSystemInfoUtils {
    private static final Logger log = LoggerFactory.getLogger(ExtendedSystemInfoUtilsImpl.class);
    private static final List<String> DEFAULT_PROPS = ImmutableList.copyOf(asList(
            "user.dir",
            "java.version",
            "java.vendor",
            "java.vm.specification.version",
            "java.vm.specification.vendor",
            "java.vm.version",
            "java.runtime.name",
            "java.vm.name",
            "user.name",
            "user.timezone",
            "file.encoding",
            "os.name",
            "os.version",
            "os.arch"));

    private static final List<Pattern> SECURITY_BLACKLISTED_PROPS = compile(
            "License.*",
            "jira\\.sid\\.key",
            "org\\.apache\\.shindig\\.common\\.crypto\\.BlobCrypter\\:key",
            "applinks\\..*");

    //users and groups excluded and handled manually: check getUsageStast()
    private static final Map<String, String> DB_STATS_WHITELIST = ImmutableMap.<String, String>builder()
            .put("issues", "admin.systeminfo.issues")
            .put("projects", "admin.systeminfo.projects")
            .put("customfields", "admin.systeminfo.custom.fields")
            .put("workflows", "admin.systeminfo.workflows")
            .put("comments", "admin.systeminfo.comments")
            .put("versions", "admin.systeminfo.versions")
            .put("components", "admin.systeminfo.components")
            .put("status", "admin.systeminfo.statuses")
            .put("resolutions", "admin.systeminfo.resolutions")
            .put("issuetypes", "admin.systeminfo.issuetypes")
            .put("priorities", "admin.systeminfo.priorities")
            .put("screens", "admin.systeminfo.screens")
            .put("screensschemes", "admin.systeminfo.screensschemes")
            .put("permissionsschemes", "admin.systeminfo.permissionschemes")
            .put("issuesecuritylevels", "admin.systeminfo.issuesecuritylevels")
            .build();

    private static final Comparator<GenericValue> COMPARE_BY_NAME =
            (gv1, gv2) -> gv1.getString("name").compareTo(gv2.getString("name"));

    private final DateFormat dateFormatter = new SimpleDateFormat("EEEEE, dd MMM yyyy");
    private final DateFormat timeFormatter = new SimpleDateFormat("HH:mm:ss Z");

    private final SystemInfoUtils systemInfoUtils;
    private final ServiceManager serviceManager;
    private final PluginAccessor pluginAccessor;
    private final ApplicationProperties applicationProperties;
    private final TrustedApplicationService trustedAppService;
    private final OfBizDelegator ofBizDelegator;
    private final I18nHelper i18nHelper;
    private final HashRegistryCache registry;
    private final LocaleManager localeManager;
    private final Optional<JiraLicenseService> jiraLicenseServiceRef;
    private final BuildUtilsInfo buildUtilsInfo;
    private final Optional<UpgradeVersionHistoryManager> upgradeVersionHistoryManagerRef;
    private final JiraProperties jiraSystemProperties;
    private final ClusterManager clusterManager;
    private final JiraStartStopAnalyticHelper jiraStartStopAnalyticHelper;

    protected ExtendedSystemInfoUtilsImpl(final SystemInfoUtils systemInfoUtils, final ServiceManager serviceManager,
                                          final PluginAccessor pluginAccessor, final ApplicationProperties applicationProperties,
                                          final TrustedApplicationService trustedAppService, final OfBizDelegator ofBizDelegator,
                                          final I18nHelper i18nHelper, final HashRegistryCache registry, final LocaleManager localeManager,
                                          final Optional<JiraLicenseService> jiraLicenseServiceRef, final BuildUtilsInfo buildUtilsInfo,
                                          final Optional<UpgradeVersionHistoryManager> upgradeVersionHistoryManagerRef, final JiraProperties jiraSystemProperties,
                                          final ClusterManager clusterManager,
                                          final JiraStartStopAnalyticHelper jiraStartStopAnalyticHelper) {
        this.systemInfoUtils = systemInfoUtils;
        this.serviceManager = serviceManager;
        this.pluginAccessor = pluginAccessor;
        this.applicationProperties = applicationProperties;
        this.trustedAppService = trustedAppService;
        this.ofBizDelegator = ofBizDelegator;
        this.i18nHelper = i18nHelper;
        this.registry = registry;
        this.localeManager = localeManager;
        this.jiraSystemProperties = jiraSystemProperties;
        this.clusterManager = clusterManager;
        this.jiraStartStopAnalyticHelper = jiraStartStopAnalyticHelper;
        this.jiraLicenseServiceRef = jiraLicenseServiceRef;
        this.buildUtilsInfo = notNull("buildUtilsInfo", buildUtilsInfo);
        this.upgradeVersionHistoryManagerRef = upgradeVersionHistoryManagerRef;
    }

    public ExtendedSystemInfoUtilsImpl(final I18nHelper i18nHelper) {
        this(new SystemInfoUtilsImpl(), getComponent(ServiceManager.class), getComponent(PluginAccessor.class),
                ComponentAccessor.getApplicationProperties(), getComponent(TrustedApplicationService.class),
                ComponentAccessor.getOfBizDelegator(), i18nHelper, getComponent(HashRegistryCache.class),
                getComponent(LocaleManager.class), getComponentSafely(JiraLicenseService.class),
                getComponent(BuildUtilsInfo.class), getComponentSafely(UpgradeVersionHistoryManager.class),
                getComponent(JiraProperties.class), getComponent(ClusterManager.class),
                getComponent(JiraStartStopAnalyticHelper.class));
    }

    public SystemInfoUtils getSystemInfoUtils() {
        return systemInfoUtils;
    }

    public Map<String, String> getProps() {
        return getProps(false);
    }

    public Map<String, String> getProps(final boolean showSensitiveInfo) {
        final MapBuilder<String, String> props = MapBuilder.newBuilder();
        final Map<String, String> sysProps = jiraSystemProperties.getSanitisedProperties();

        props.add(getText("admin.systeminfo.system.date"), dateFormatter.format(new Date()));
        props.add(getText("admin.systeminfo.system.time"), timeFormatter.format(new Date()));
        props.add(getText("admin.systeminfo.system.cwd"), sysProps.get("user.dir"));

        props.add(getText("admin.systeminfo.java.version"), sysProps.get("java.version"));
        props.add(getText("admin.systeminfo.java.vendor"), sysProps.get("java.vendor"));
        props.add(getText("admin.systeminfo.jvm.version"), sysProps.get("java.vm.specification.version"));
        props.add(getText("admin.systeminfo.jvm.vendor"), sysProps.get("java.vm.specification.vendor"));
        props.add(getText("admin.systeminfo.jvm.implementation.version"), sysProps.get("java.vm.version"));
        props.add(getText("admin.systeminfo.java.runtime"), sysProps.get("java.runtime.name"));
        props.add(getText("admin.systeminfo.java.vm"), sysProps.get("java.vm.name"));

        props.add(getText("admin.systeminfo.user.name"), sysProps.get("user.name"));
        props.add(getText("admin.systeminfo.user.timezone"), sysProps.get("user.timezone"));
        props.add(getText("admin.systeminfo.user.locale"), (Locale.getDefault() == null ? "null" : Locale.getDefault().getDisplayName()));
        props.add(getText("admin.systeminfo.system.encoding"), sysProps.get("file.encoding"));

        props.add(getText("admin.systeminfo.operating.system"), sysProps.get("os.name") + " " + sysProps.get("os.version"));
        props.add(getText("admin.systeminfo.os.architecture"), sysProps.get("os.arch"));

        String serverInfo = "";
        try {
            serverInfo = (ActionContext.getServletContext().getServerInfo());
        } catch (final Exception e) {
            //do nothing - we may not have an action context here.
        }

        props.add(getText("admin.systeminfo.application.server.container"), serverInfo);
        props.add(getText("admin.systeminfo.database.type"), systemInfoUtils.getDatabaseType());
        props.add(getText("admin.systeminfo.database.jndi.address"), systemInfoUtils.getDbDescriptorValue());

        try {
            final SystemInfoUtils.DatabaseMetaData databaseMetaData = systemInfoUtils.getDatabaseMetaData();
            if (showSensitiveInfo) {
                props.add(getText("admin.systeminfo.database.url"), databaseMetaData.getMaskedURL());
            } else {
                props.add(getText("admin.systeminfo.database.url"), getText("admin.systeminfo.hidden.field"));
            }
            props.add(getText("admin.systeminfo.database.version"), databaseMetaData.getDatabaseProductVersion());
            props.add(getText("admin.systeminfo.database.driver"), databaseMetaData.getDriverName() + " " + databaseMetaData.getDriverVersion());
            props.add(getText("admin.systeminfo.database.collation"), databaseMetaData.getCollation());
        } catch (final Exception e) {
            props.add(getText("admin.systeminfo.database.accesserror"), e.toString());
            log.error(e.getMessage(), e);
        }

        try {
            props.add(
                    getText("admin.generalconfiguration.external.user.management"),
                    applicationProperties.getOption(APKeys.JIRA_OPTION_USER_EXTERNALMGT) ? getText("admin.common.words.on") : getText("admin.common.words.off"));
        } catch (final Exception e) {
            log.error("Error getting the " + APKeys.JIRA_OPTION_USER_EXTERNALMGT + " application property: " + e, e);
            props.add(getText("admin.generalconfiguration.external.user.management"), "ERROR");
        }

        if (isJvmJava5OrGreater()) {
            props.add(getText("admin.systeminfo.jvm.input.arguments"), systemInfoUtils.getJvmInputArguments());
        } else {
            props.add(getText("admin.systeminfo.jvm.input.arguments"), getText("admin.systeminfo.not.possible"));
        }
        if (showSensitiveInfo) {
            String modifiedFilesDescription = getText("admin.systeminfo.modz.missing");
            String removedFilesDescription = getText("admin.systeminfo.modz.missing");

            // in the interest of keeping the page load relatively quick (at least
            // for subsequent loads) we only do the hashing and detection once and
            // then cache the results
            try {
                final Modifications modifications = registry.getModifications();
                if (!modifications.modifiedFiles.isEmpty()) {
                    modifiedFilesDescription = StringUtils.join(modifications.modifiedFiles, ", ");
                } else {
                    modifiedFilesDescription = getText("admin.systeminfo.modz.nomodifications");
                }
                if (!modifications.removedFiles.isEmpty()) {
                    removedFilesDescription = StringUtils.join(modifications.removedFiles, ", ");
                } else {
                    removedFilesDescription = getText("admin.systeminfo.modz.noremovals");
                }
            } catch (final ModzRegistryException | RuntimeException e) {
                // no need to handle the exception. just leaving modifications
                // unset will result in us doing the right thing: displaying a
                // vaguely informative error message and then trying to re-detect
                // changes the next time the page is loaded.
                log.error(e.getMessage(), e);
            }

            final String installType = "[" + getText("admin.systeminfo.installation.type") + ": " + systemInfoUtils.getInstallationType() + "] ";
            props.add(getText("admin.systeminfo.modz.modified"), installType + modifiedFilesDescription);
            props.add(getText("admin.systeminfo.modz.removed"), installType + removedFilesDescription);
        }

        props.add(getText("admin.systeminfo.clustered"), clusterManager.isClustered() ? getText("admin.common.words.on") : getText("admin.common.words.off"));
        if (clusterManager.isClustered()) {
            props.add(getText("admin.systeminfo.clustered.node.id"), clusterManager.getNodeId());
        }

        return props.toListOrderedMap();
    }

    public Map<String, String> getApplicationPropertiesFormatted(final String suffix) {
        Map<String, Object> properties = applicationProperties.asMap();
        final Map<String, String> props = new TreeMap<>();
        for (final Map.Entry<String, Object> entry : properties.entrySet()) {
            boolean blacklist = false;
            for (Pattern rule : SECURITY_BLACKLISTED_PROPS) {
                if (rule.matcher(entry.getKey()).matches()) {
                    blacklist = true;
                    break;
                }
            }
            if (!blacklist) {
                props.put(entry.getKey(), String.valueOf(entry.getValue()));
            }
        }
        return new LinkedHashMap<>(props);
    }

    public Map<String, String> getSystemPropertiesFormatted(final String suffix) {
        final Map<String, String> sysProps = jiraSystemProperties.getSanitisedProperties();

        final boolean isWindows = sysProps.get("os.name").toLowerCase(Locale.ENGLISH).startsWith("windows");
        final Map<String, String> props = new TreeMap<>();
        final Map<String, String> pathProps = new TreeMap<>();
        for (Map.Entry<String, String> entry : sysProps.entrySet()) {
            final String propertyName = entry.getKey();
            final String value = entry.getValue();
            if (!DEFAULT_PROPS.contains(propertyName)) {
                if (propertyName.endsWith(".path")) {
                    String htmlValue = value;
                    if (!isWindows)//for non-windows operating systems split on colons as well
                    {
                        htmlValue = breakSeperators(htmlValue, ":", suffix);
                    }
                    //as this entry spans multiple lines, put it at the end of the map
                    pathProps.put(propertyName, breakSeperators(htmlValue, ";", suffix));
                } else {
                    props.put(propertyName, value);
                }
            }
        }

        final Map<String, String> propsMap = new LinkedHashMap<>();
        propsMap.putAll(props);
        //put all the properties to be added later to the end
        propsMap.putAll(pathProps);
        return propsMap;
    }

    private String breakSeperators(final String input, final String seperator, final String suffix) {
        return input.replaceAll(seperator, seperator + suffix);
    }

    public Map<String, String> getJvmStats() {
        final MapBuilder<String, String> jvmStats = MapBuilder.newBuilder();
        jvmStats.add(getText("admin.systeminfo.total.memory"), systemInfoUtils.getTotalMemory() + " MB");
        jvmStats.add(getText("admin.systeminfo.free.memory"), systemInfoUtils.getFreeMemory() + " MB");
        jvmStats.add(getText("admin.systeminfo.used.memory"), systemInfoUtils.getUsedMemory() + " MB");
        if (isJvmWithPermGen()) {
            jvmStats.add(getText("admin.systeminfo.total.perm.gen.memory"), systemInfoUtils.getTotalPermGenMemory() + " MB");
            jvmStats.add(getText("admin.systeminfo.free.perm.gen.memory"), systemInfoUtils.getFreePermGenMemory() + " MB");
            jvmStats.add(getText("admin.systeminfo.used.perm.gen.memory"), systemInfoUtils.getUsedPermGenMemory() + " MB");
        }
        return jvmStats.toListOrderedMap();
    }

    public Map<String, String> getCommonConfigProperties() {
        final Map<String, String> map = new LinkedHashMap<>();
        map.put(getText("admin.config.allow.attachments"), String.valueOf(applicationProperties.getOption(APKeys.JIRA_OPTION_ALLOWATTACHMENTS)));
        map.put(getText("admin.config.allow.voting"), String.valueOf(applicationProperties.getOption(APKeys.JIRA_OPTION_VOTING)));
        map.put(getText("admin.config.allow.issue.watching"), String.valueOf(applicationProperties.getOption(APKeys.JIRA_OPTION_WATCHING)));
        map.put(getText("admin.config.allow.unassigned"), String.valueOf(applicationProperties.getOption(APKeys.JIRA_OPTION_ALLOWUNASSIGNED)));
        map.put(getText("admin.config.allow.subtasks"), String.valueOf(applicationProperties.getOption(APKeys.JIRA_OPTION_ALLOWSUBTASKS)));
        map.put(getText("admin.config.allow.issue.linking"), String.valueOf(applicationProperties.getOption(APKeys.JIRA_OPTION_ISSUELINKING)));
        map.put(getText("admin.config.timetracking.enabled"), String.valueOf(applicationProperties.getOption(APKeys.JIRA_OPTION_TIMETRACKING)));
        map.put(getText("admin.config.timetracking.hours.per.day"), applicationProperties.getDefaultBackedString(APKeys.JIRA_TIMETRACKING_HOURS_PER_DAY));
        map.put(getText("admin.config.timetracking.days.per.week"), applicationProperties.getDefaultBackedString(APKeys.JIRA_TIMETRACKING_DAYS_PER_WEEK));

        return map;
    }

    public List<MemoryInformation> getMemoryPoolInformation() {
        return systemInfoUtils.getMemoryPoolInformation();
    }

    public Map<String, String> getBuildStats() {
        final MapBuilder<String, String> buildstats = MapBuilder.newBuilder();
        buildstats.add(getText("admin.systeminfo.uptime"), systemInfoUtils.getUptime(i18nHelper.getDefaultResourceBundle()));
        buildstats.add(getText("admin.systeminfo.version"), buildUtilsInfo.getVersion());
        buildstats.add(getText("admin.systeminfo.build.number"), buildUtilsInfo.getCurrentBuildNumber());
        buildstats.add(getText("admin.systeminfo.build.date"), String.valueOf(buildUtilsInfo.getCurrentBuildDate()));
        buildstats.add(getText("admin.systeminfo.build.revision"), String.valueOf(buildUtilsInfo.getCommitId()));
        buildstats.add(getText("admin.license.partner.name"), buildUtilsInfo.getBuildPartnerName());
        buildstats.add(getText("admin.systeminfo.installation.type"), systemInfoUtils.getInstallationType());
        buildstats.add(getText("admin.server.id"), jiraLicenseServiceRef.map(JiraLicenseService::getServerId).orElse(""));;

        // if there is an upgrade history - display it
        final List<UpgradeVersionHistoryItem> historyItems = getUpgradeHistory();
        if (!historyItems.isEmpty()) {
            final UpgradeVersionHistoryItem lastUpgrade = historyItems.get(0);
            final StringBuilder sb = new StringBuilder(getDateTimeFormatter().withStyle(DateTimeStyle.COMPLETE)
                    .format(lastUpgrade.getTimePerformed()));

            // if we have a previous version, also display which version was last used
            if (!StringUtils.isBlank(lastUpgrade.getOriginalVersion()) && !StringUtils.isBlank(lastUpgrade.getOriginalBuildNumber())) {
                final String versionPriorToUpgrade = lastUpgrade.getOriginalVersion();
                final String numberPriorToUpgrade = lastUpgrade.getOriginalBuildNumber();
                sb.append(String.format(" (v%s%s%s)", versionPriorToUpgrade, "#", numberPriorToUpgrade));
            }

            buildstats.add(getText("admin.systeminfo.last.upgrade"), sb.toString());
        }

        return buildstats.toListOrderedMap();
    }

    public List<UpgradeVersionHistoryItem> getUpgradeHistory() {
        return upgradeVersionHistoryManagerRef.map(UpgradeVersionHistoryManager::getAllUpgradeVersionHistory)
                .orElseGet(ImmutableList::of);
    }

    public String getDefaultLanguage() {
        return applicationProperties.getDefaultLocale().getDisplayName(i18nHelper.getLocale());
    }

    public String getBaseUrl() {
        return applicationProperties.getDefaultBackedString(APKeys.JIRA_BASEURL);
    }

    public boolean isUsingSystemLocale() {
        return localeManager.getLocale(applicationProperties.getDefaultBackedString(APKeys.JIRA_I18N_DEFAULT_LOCALE)) == null;
    }

    public Map<String, String> getUsageStats() {
        final Map<String, String> usageStats = new TreeMap<>();
        try {
            boolean externalUserManagment = false;

            try {
                externalUserManagment = applicationProperties.getOption(APKeys.JIRA_OPTION_USER_EXTERNALMGT);
            } catch (final Exception e) {
                log.error("Error reading " + APKeys.JIRA_OPTION_USER_EXTERNALMGT + " application property: " + e, e);
            }

            if (externalUserManagment) {
                usageStats.put(getText("admin.systeminfo.users"),
                        ofBizDelegator.getCount("User") + " " + getText("admin.generalconfiguration.external.user.management.statistics"));
                usageStats.put(getText("admin.systeminfo.groups"),
                        ofBizDelegator.getCount("Group") + " " + getText("admin.generalconfiguration.external.user.management.statistics"));

            } else {
                usageStats.put(getText("admin.systeminfo.users"), Long.toString(ofBizDelegator.getCount("User")));
                usageStats.put(getText("admin.systeminfo.groups"), Long.toString(ofBizDelegator.getCount("Group")));
            }
            usageStats.put(getText("admin.systeminfo.attachments"), Long.toString(ofBizDelegator.getCount(AttachmentConstants.ATTACHMENT_ENTITY_NAME)));

            final Map<String, Object> stats = jiraStartStopAnalyticHelper.getOnStartUsageStats(true);
            stats.entrySet().stream()
                    .filter(entry -> DB_STATS_WHITELIST.containsKey(entry.getKey()))
                    .forEach(entry -> usageStats.put(
                            getText(DB_STATS_WHITELIST.get(entry.getKey())),
                            String.valueOf(entry.getValue())));
        } catch (final Exception e) {
            log.debug("Error while retrieving usage statistics", e);
            return ImmutableMap.of();
        }

        return Collections.unmodifiableMap(usageStats);
    }

    public String getEntityEngineXmlPath() {
        try {
            return ClassLoaderUtils.getResource(EntityConfigUtil.ENTITY_ENGINE_XML_FILENAME, getClass()).toExternalForm();
        } catch (final Exception e) {
            log.error("Could not load " + EntityConfigUtil.ENTITY_ENGINE_XML_FILENAME + " path " + e.getMessage(), e);
            return "Could not load " + EntityConfigUtil.ENTITY_ENGINE_XML_FILENAME + "  path.  Exception " + e.getMessage();
        }
    }

    public String getLogPath() {
        final JiraLogLocator locator = getComponent(JiraLogLocator.class);
        final File logFile = locator.findJiraLogFile();
        final String path;
        if (logFile != null) {
            path = logFile.getAbsolutePath();
        } else {
            path = "Could not find " + JiraLogLocator.AJL_FILE_NAME + ".";
            log.debug(path);
        }
        return path;
    }

    public String getIndexLocation() {
        return ComponentAccessor.getIndexPathManager().getIndexRootPath();
    }

    public String getAttachmentsLocation() {
        return ComponentAccessor.getAttachmentPathManager().getAttachmentPath();
    }

    public String getBackupLocation() {
        final JiraHome jiraHome = getComponent(JiraHome.class);
        try {
            return jiraHome.getExportDirectory().getPath();
        } catch (final IllegalStateException e) {
            return "";
        }
    }

    public String getJiraHomeLocation() {
        final JiraHome jiraHome = getComponent(JiraHome.class);
        String path;
        try {
            path = jiraHome.getHomePath();
        } catch (final IllegalStateException e) {
            path = "";
        }
        return path;
    }

    public String getJiraLocalHomeLocation() {
        final JiraHome jiraHome = getComponent(JiraHome.class);
        String path;
        try {
            path = jiraHome.getLocalHomePath();
        } catch (final IllegalStateException e) {
            path = "";
        }
        return path;
    }

    public Collection<GenericValue> getListeners() {
        final Collection<GenericValue> listeners = new TreeSet<>(COMPARE_BY_NAME);
        listeners.addAll(ofBizDelegator.findAll("ListenerConfig"));
        return listeners;
    }

    public Collection<JiraServiceContainer> getServices() {
        final Collection<JiraServiceContainer> services = new TreeSet<>(JiraService.NAME_COMPARATOR);
        try {
            services.addAll(serviceManager.getServices());
        } catch (IllegalStateException ise) {
            log.debug("Cannot collect services info", ise);
        }
        return services;
    }

    public Map<String, String> getServicePropertyMap(final JiraServiceContainer serviceContainer) {
        try {
            return ConfigurableObjectUtil.getPropertyMap(serviceContainer);
        } catch (final RuntimeException e) {
            throw e;
        } catch (final Exception e) {
            throw new RuntimeException(e);
        }
    }

    public long getMillisecondsToMinutes(final long ms) {
        return TimeUnit.MILLISECONDS.toMinutes(ms);
    }

    public Collection<Plugin> getPlugins() {
        final SortedSet<Plugin> plugins = new TreeSet<>(new PluginComparator());
        for (Plugin plugin : pluginAccessor.getPlugins()) {
            if (!plugins.add(plugin)) {
                throw new IllegalStateException("Multiple plugins with the same key and version:" + plugin.getKey() + " " + plugin.getPluginsVersion());
            }
        }
        return Collections.unmodifiableSet(plugins);
    }

    public boolean isPluginEnabled(final Plugin plugin) {
        return pluginAccessor.isPluginEnabled(plugin.getKey());
    }

    public Set<TrustedApplicationInfo> getTrustedApplications(final JiraServiceContext jiraServiceContext) {
        final Set<TrustedApplicationInfo> trustedApplications = new TreeSet<>(TrustedApplicationService.NAME_COMPARATOR);
        trustedApplications.addAll(trustedAppService.getAll(jiraServiceContext));
        return trustedApplications;
    }

    public Set<String> getIPMatches(final TrustedApplicationInfo info) {
        return TrustedApplicationUtil.getLines(info.getIpMatch());
    }

    public Set<String> getUrlMatches(final TrustedApplicationInfo info) {
        return TrustedApplicationUtil.getLines(info.getUrlMatch());
    }

    public boolean isJvmJava5OrGreater() {
        try {
            return JvmProperties.isJvmVersion(1.5F);
        } catch (final InvalidVersionException e) {
            return false;
        }
    }

    @Override
    public boolean isJvmWithPermGen() {
        return systemInfoUtils.getTotalPermGenMemory() > 0;
    }

    private DateTimeFormatter getDateTimeFormatter() {
        return ComponentAccessor.getComponent(DateTimeFormatterFactory.class).formatter().withLocale(i18nHelper.getLocale());
    }

    private String getText(final String key) {
        return i18nHelper.getText(key);
    }

    private String getText(final String key, final String value1) {
        return i18nHelper.getText(key, value1);
    }

    @Override
    public boolean isClustered() {
        return clusterManager.isClustered();
    }

    @Override
    public Map<Node, Boolean> getClusterNodeInformation() {
        if (!clusterManager.isClustered()) {
            return ImmutableMap.of();
        }

        final Set<String> liveNodes = clusterManager.findLiveNodes().stream()
                .map(Node::getNodeId)
                .collect(Collectors.toSet());

        final ImmutableMap.Builder<Node, Boolean> nodeMap = ImmutableMap.builder();
        clusterManager.getAllNodes().forEach(node -> nodeMap.put(node, liveNodes.contains(node.getNodeId())));
        return nodeMap.build();
    }

    private static List<Pattern> compile(String... regexes) {
        return asList(regexes).stream()
                .map(Pattern::compile)
                .collect(CollectorsUtil.toImmutableList());
    }
}
