package com.atlassian.jira.workflow.function.issue;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.ModifiedValue;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.fields.OrderableField;
import com.atlassian.jira.issue.util.InlineIssuePropertySetter;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.user.ApplicationUser;
import com.google.common.annotations.VisibleForTesting;
import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.StoreException;
import com.opensymphony.workflow.WorkflowException;
import com.opensymphony.workflow.loader.DescriptorFactory;
import com.opensymphony.workflow.loader.FunctionDescriptor;
import com.opensymphony.workflow.loader.StepDescriptor;
import com.opensymphony.workflow.loader.WorkflowDescriptor;
import com.opensymphony.workflow.spi.Step;
import com.opensymphony.workflow.spi.WorkflowEntry;
import com.opensymphony.workflow.spi.WorkflowStore;
import org.codehaus.jackson.JsonNode;

import java.sql.Timestamp;
import java.util.Map;

public class IssueCreateFunction implements FunctionProvider {

    public void execute(Map transientVars, Map args, PropertySet ps) throws WorkflowException {
        try {
            FieldManager fieldManager = getComponentOfType(FieldManager.class);

            MutableIssue issue = cast(transientVars.get("issue"));

            // Initialise the time stamps
            Timestamp now = new Timestamp(System.currentTimeMillis());
            if (issue.getCreated() == null) {
                issue.setCreated(now);
            }
            if (issue.getUpdated() == null) {
                issue.setUpdated(now);
            }

            // Set the issue key
            Project project = issue.getProjectObject();
            long incCount = getComponentOfType(ProjectManager.class).getNextId(project);
            issue.setKey(project.getKey() + "-" + incCount);

            // Initialise votes
            if (issue.getVotes() == null) {
                issue.setVotes(0L);
            }
            // Initialise watches
            if (issue.getWatches() == null) {
                issue.setWatches(0L);
            }

            // Initialise workflow entry
            WorkflowEntry entry = cast(transientVars.get("entry"));
            issue.setWorkflowId(entry.getId());

            // Initialise issue's status
            WorkflowStore store = cast(transientVars.get("store"));
            Step step = cast(store.findCurrentSteps(entry.getId()).get(0));
            WorkflowDescriptor descriptor = cast(transientVars.get("descriptor"));
            StepDescriptor stepDescriptor = descriptor.getStep(step.getStepId());
            issue.setStatusId(cast(stepDescriptor.getMetaAttributes().get("jira.status.id")));

            // Store the issue
            issue.store();

            // Maybe move this code to the issue.store() method
            Map<String, ModifiedValue> modifiedFields = issue.getModifiedFields();
            for (final String fieldId : modifiedFields.keySet()) {
                if (fieldManager.isOrderableField(fieldId)) {
                    OrderableField field = fieldManager.getOrderableField(fieldId);
                    Object newValue = modifiedFields.get(fieldId).getNewValue();
                    if (newValue != null) {
                        field.createValue(issue, newValue);
                    }
                }
            }
            // Reset the fields as they all have been persisted to the db. Maybe move this code to the "createValue"
            // method of the issue, so that the field removes itself from the modified list as soon as it is persisted.
            issue.resetModifiedFields();

            //
            // if any issue properties are present then set them to
            setIssueProperties(issue.getId(), transientVars);

        } catch (StoreException e) {
            throw new WorkflowException(e);
        }
    }

    public static FunctionDescriptor makeDescriptor() {
        FunctionDescriptor descriptor = DescriptorFactory.getFactory().createFunctionDescriptor();
        descriptor.setType("class");
        //noinspection unchecked
        descriptor.getArgs().put("class.name", IssueCreateFunction.class.getName());
        return descriptor;
    }

    private void setIssueProperties(Long issueId, Map transientVars) {
        Map<String, JsonNode> issueProperties = cast(transientVars.get("issueProperties"));
        ApplicationUser user = cast(transientVars.get("user"));
        if (issueProperties != null) {
            InlineIssuePropertySetter propertySetter = getComponentOfType(InlineIssuePropertySetter.class);
            propertySetter.setIssueProperties(user, issueId, issueProperties, false);
        }
    }

    // damn you 2004 and your lack of generics!
    private <T> T cast(Object obj) {
        //noinspection unchecked
        return (T) obj;
    }

    @VisibleForTesting
    <T> T getComponentOfType(Class<T> componentClass) {
        return ComponentAccessor.getComponentOfType(componentClass);
    }
}
