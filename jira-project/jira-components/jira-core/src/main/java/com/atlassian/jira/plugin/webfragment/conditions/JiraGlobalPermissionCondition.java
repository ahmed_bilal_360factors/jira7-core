package com.atlassian.jira.plugin.webfragment.conditions;

import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.plugin.PluginParseException;

import java.util.Map;
import java.util.Optional;

/**
 * Checks if the user has the global permission
 */
public class JiraGlobalPermissionCondition extends AbstractWebCondition {

    private final GlobalPermissionManager permissionManager;

    private GlobalPermissionKey permissionKey;

    public JiraGlobalPermissionCondition(GlobalPermissionManager permissionManager) {
        this.permissionManager = permissionManager;
    }

    @Override
    public void init(Map<String, String> params) throws PluginParseException {
        String providedKey = params.get("permission");
        permissionKey = legacyPermissionKey(providedKey).orElse(GlobalPermissionKey.of(providedKey));
        super.init(params);
    }

    public boolean shouldDisplay(ApplicationUser user, JiraHelper jiraHelper) {
        return permissionManager.hasPermission(permissionKey, user);
    }

    private Optional<GlobalPermissionKey> legacyPermissionKey(String providedKey) {
        int legacyPermission = Permissions.getType(providedKey);
        if (legacyPermission != -1) {
            GlobalPermissionKey globalPermissionKey = GlobalPermissionKey.GLOBAL_PERMISSION_ID_TRANSLATION.get(legacyPermission);
            return Optional.ofNullable(globalPermissionKey);
        } else {
            return Optional.empty();
        }
    }
}
