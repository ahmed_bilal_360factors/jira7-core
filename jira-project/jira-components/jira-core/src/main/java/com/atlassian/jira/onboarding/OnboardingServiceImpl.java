package com.atlassian.jira.onboarding;

import com.atlassian.collectors.CollectorsUtil;
import com.atlassian.jira.extension.Startable;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.web.action.issue.util.ConditionalDescriptorPredicate;
import com.atlassian.jira.web.landingpage.LandingPageRedirectManager;
import com.atlassian.jira.web.landingpage.PageRedirect;
import com.atlassian.plugin.PluginAccessor;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Iterables;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

public class OnboardingServiceImpl implements OnboardingService, Startable {
    private static final int ONBOARDING_PRIORITY = 10;

    private static final Logger log = LoggerFactory.getLogger(OnboardingServiceImpl.class);

    private final ConditionalDescriptorPredicate CONDITIONAL_DESCRIPTOR_PREDICATE = new ConditionalDescriptorPredicate(ImmutableMap.of());

    private final PluginAccessor pluginAccessor;
    private final OnboardingStore store;
    private final UserChecker userChecker;
    private final LandingPageRedirectManager landingPageRedirectManager;

    public OnboardingServiceImpl(final PluginAccessor pluginAccessor,
                                 final OnboardingStore store,
                                 final UserChecker userChecker,
                                 final LandingPageRedirectManager landingPageRedirectManager) {
        this.pluginAccessor = pluginAccessor;
        this.store = store;
        this.userChecker = userChecker;
        this.landingPageRedirectManager = landingPageRedirectManager;
    }

    @Override
    public boolean hasCompletedFirstUseFlow(@Nonnull final ApplicationUser user) {
        return store.getBoolean(user, OnboardingStore.FIRST_USE_FLOW_COMPLETED);
    }

    @Override
    public void completeFirstUseFlow(@Nonnull final ApplicationUser user) {
        store.setBoolean(user, OnboardingStore.FIRST_USE_FLOW_COMPLETED, true);
    }

    @Override
    public void setCurrentFirstUseFlowSequence(@Nonnull final ApplicationUser user, @Nonnull final String sequenceKey) {
        store.setString(user, OnboardingStore.FIRST_USE_FLOW_CURRENT_SEQUENCE, sequenceKey);
    }

    @Nullable
    @Override
    public String getCurrentFirstUseFlowSequence(@Nonnull final ApplicationUser user) {
        return store.getString(user, OnboardingStore.FIRST_USE_FLOW_CURRENT_SEQUENCE);
    }

    @Nullable
    @Override
    public FirstUseFlow getFirstUseFlow(@Nullable final ApplicationUser user) {
        if (user == null || hasResolvedFirstUseFlow(user)) {
            return null;
        }

        FirstUseFlow firstUseFlow = evaluateFirstUseFlowChoice(user);
        // when firstUseFlow is evaluated as null it means the onboarding must not be shown to this user, so let's store
        // this status to avoid re-evaluating the onboarding flow again in any future requests from this user. It is also
        // useful when the loginCount is reset (e.g., when user directories are changed)
        if (firstUseFlow == null) {
            resolveFirstUseFlow(user);
        } else {
            // before returning the first use flow, let's ensure the impersonation feature is not active, otherwise we
            // simply return null as the onboarding should be displayed only for the real user later
            if (userChecker.isImpersonationActive(user)) {
                log.info("User Impersonation is active: onboarding will not be displayed for the current user ({})", user.getUsername());
                return null;
            }
        }

        return firstUseFlow;
    }

    /**
     * Checks whether the user should do an onboarding flow for the product. Returns the appropriate onboarding flow to
     * put the user through (if any).
     *
     * @return a {@link FirstUseFlow} if the user is eligible for one (and hasn't done one before), null otherwise.
     */
    @Nullable
    private FirstUseFlow evaluateFirstUseFlowChoice(@Nonnull final ApplicationUser user) {
        if (hasCompletedFirstUseFlow(user)) {
            return null;
        } else if (hasStartedFirstUseFlow(user)) {
            final String startedFlowKey = getStartedFirstUseFlowKey(user);
            if (startedFlowKey == null) {
                return null;
            }

            FirstUseFlowModuleDescriptor chosenFlowDescriptor = getFirstUseFlowModuleDescriptorByKey(startedFlowKey);
            if (chosenFlowDescriptor == null) {
                return null;
            }

            return chosenFlowDescriptor.getModule();
        } else if (userChecker.firstTimeLoggingIn(user)) {
            return startHighestWeightFlow(user);
        }

        return null;
    }


    /**
     * Start the highest weight flow in the system
     *
     * @param user to find flow for
     * @return the flow that was marked as being started.
     */
    @Nullable
    private FirstUseFlow startHighestWeightFlow(@Nonnull final ApplicationUser user) {
        List<FirstUseFlowModuleDescriptor> flows = getOrderedFirstUseFlowDescriptors();
        for (FirstUseFlowModuleDescriptor flow : flows) {
            if (flow.getModule().isApplicable(user)) {
                markFirstUseFlowStarted(user, flow);
                return flow.getModule();
            }
        }
        return null;
    }

    /**
     * Store that we have started a first use flow.
     *
     * @param user                         that the first use flow is being completed by
     * @param firstUseFlowModuleDescriptor of the first use flow to complete
     */
    private void markFirstUseFlowStarted(@Nonnull final ApplicationUser user, @Nonnull final FirstUseFlowModuleDescriptor firstUseFlowModuleDescriptor) {
        store.setString(user, OnboardingStore.STARTED_FLOW_KEY, firstUseFlowModuleDescriptor.getKey());
    }

    /**
     * Returns if a user has already started a first use flow.
     *
     * @param user to check against.
     * @return true if already started a first use flow
     */
    private boolean hasStartedFirstUseFlow(@Nonnull final ApplicationUser user) {
        return store.isSet(user, OnboardingStore.STARTED_FLOW_KEY);
    }

    /**
     * @param user to check against
     * @return whether the first use flow was resolved (and skipped) for this user before.
     */
    private boolean hasResolvedFirstUseFlow(@Nonnull final ApplicationUser user) {
        return store.getBoolean(user, OnboardingStore.FIRST_USE_FLOW_RESOLVED);
    }


    @Override
    @Nullable
    public String getStartedFirstUseFlowKey(@Nonnull final ApplicationUser user) {
        return store.getString(user, OnboardingStore.STARTED_FLOW_KEY);
    }

    /**
     * Mark the user as having the onboarding flow resolved
     *
     * @param user to mark onboarding as resolved
     */
    private void resolveFirstUseFlow(@Nonnull final ApplicationUser user) {
        store.setBoolean(user, OnboardingStore.FIRST_USE_FLOW_RESOLVED, true);
    }

    /**
     * @param key of the first flow module that we want to retrieve.
     * @return the firstUseFlowModuleDescriptor for the specific key.
     */
    @Nullable
    private FirstUseFlowModuleDescriptor getFirstUseFlowModuleDescriptorByKey(@Nonnull final String key) {
        Iterable<FirstUseFlowModuleDescriptor> unfilteredDescriptors = pluginAccessor
                .getEnabledModuleDescriptorsByClass(FirstUseFlowModuleDescriptor.class);
        FirstUseFlowModuleDescriptor matchingDescriptor = null;

        Iterable<FirstUseFlowModuleDescriptor> filteredDescriptors = Iterables.filter(unfilteredDescriptors,
                input -> (null != input) && key.equals(input.getKey()));

        Iterator<FirstUseFlowModuleDescriptor> iterator = filteredDescriptors.iterator();
        if (iterator.hasNext()) {
            matchingDescriptor = iterator.next();
        }

        return matchingDescriptor;
    }

    /**
     * Finds and sorts through all the registered FirstUseFlows, returning a list where the most important flow is
     * first. Does not return flows which shouldn't be displayed (does not pass the condition).
     *
     * @return a list of flows ordered by importance (first to last).
     */
    @Nonnull
    private List<FirstUseFlowModuleDescriptor> getOrderedFirstUseFlowDescriptors() {
        return pluginAccessor.getEnabledModuleDescriptorsByClass(FirstUseFlowModuleDescriptor.class).stream()
                .filter(CONDITIONAL_DESCRIPTOR_PREDICATE)
                .sorted()
                .collect(CollectorsUtil.toImmutableList());
    }

    @Override
    public void start() {
        landingPageRedirectManager.registerRedirect(new OnboardingLandingPageRedirect(), ONBOARDING_PRIORITY);
    }

    /**
     * Landing page redirect for onboarding.
     */
    private class OnboardingLandingPageRedirect implements PageRedirect {
        @Override
        public Optional<String> url(final ApplicationUser user) {
            return Optional.ofNullable(user).map(OnboardingServiceImpl.this::getFirstUseFlow).map(FirstUseFlow::getUrl);
        }
    }
}
