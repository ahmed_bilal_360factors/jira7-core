package com.atlassian.jira.web.landingpage;


import com.atlassian.annotations.Internal;
import com.atlassian.collectors.CollectorsUtil;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.user.ApplicationUser;
import com.google.common.collect.ImmutableSet;

import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;
import javax.annotation.concurrent.ThreadSafe;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * Allows to add redirects for user's landing page (location where users are taken after login).
 * <p>
 * Currently landing page redirects work on:
 * - Dashboard - default landing page for users in Server mode
 * - MyJIRAHome - default landing page for users in Cloud
 * - Jira Setup Completed - when user finished setting up JIRA.
 *
 * @since v7.0
 */
@ThreadSafe
@Internal
@ParametersAreNonnullByDefault
public class LandingPageRedirectManager {
    public static final String DARK_FEATURE_DISABLE_LANDING_PAGE_REDIRECT_FLAG = "jira.onboarding.feature.disabled";

    private final static PageRedirectComparator COMPARATOR = new PageRedirectComparator();

    private final AtomicReference<ImmutableSet<RedirectWithPriority>> redirectsRef = new AtomicReference<>(ImmutableSet.of());
    private final FeatureManager featureManager;

    public LandingPageRedirectManager(final FeatureManager featureManager) {
        this.featureManager = featureManager;
    }

    /**
     * Adds page redirection of landing page to a specified location.
     *
     * @param redirect to add.
     * @param priority of the redirect; the lower the value the higher the priority. Redirects with higher priority
     *                 will take a precedence.
     */
    public void registerRedirect(final PageRedirect redirect, final int priority) {
        notNull("redirect", redirect);
        final RedirectWithPriority redirectWithPriority = new RedirectWithPriority(redirect, priority);
        redirectsRef.getAndUpdate(redirects -> {
                    if (redirects.contains(redirectWithPriority)) {
                        return redirects;
                    }
                    return addPageRedirect(redirectWithPriority, redirects);
                }
        );
    }

    /**
     * Removes redirection to the given url.
     *
     * @param redirectToRemove which should be removed
     * @return true if page redirect has been removed.
     */
    public boolean unregisterRedirect(PageRedirect redirectToRemove) {
        notNull("urlToRemove", redirectToRemove);
        final RedirectWithPriority redirectWithPriority = new RedirectWithPriority(redirectToRemove, 0);
        ImmutableSet<RedirectWithPriority> previous = redirectsRef.getAndUpdate(redirects ->
                removeFromSet(redirectWithPriority, redirects));

        return previous.contains(redirectWithPriority);
    }

    /**
     * Determines the redirect URL for landing page.
     *
     * @return url that landing page should be redirected to or {@link Optional#empty()} if there are no active
     * redirects.
     */
    public Optional<String> redirectUrl(@Nullable ApplicationUser applicationUser) {
        if (featureManager.isEnabled(DARK_FEATURE_DISABLE_LANDING_PAGE_REDIRECT_FLAG)) {
            return Optional.empty();
        }

        return redirectsRef.get().stream()
                .map(r -> r.url(applicationUser))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .findFirst();
    }

    /**
     * Constructs new immutable set from already sorted set and new element. New set will be sorted by order
     * defined by the {@link #COMPARATOR}.
     *
     * @param newElement       to add to new set
     * @param existingElements already sorted immutable set
     */
    private ImmutableSet<RedirectWithPriority> addPageRedirect(RedirectWithPriority newElement,
                                                               ImmutableSet<RedirectWithPriority> existingElements) {
        //We are not using TreeSet here as it uses comparator to determine if objects are equal
        //which is not true here (comparator only compares priority). We want object considered
        //the same if equal method returns true.
        List<RedirectWithPriority> redirects = new ArrayList<>(existingElements.size() + 1);
        redirects.addAll(existingElements);
        redirects.add(newElement);
        redirects.sort(COMPARATOR);
        return ImmutableSet.copyOf(redirects);
    }

    private ImmutableSet<RedirectWithPriority> removeFromSet(RedirectWithPriority redirectToRemove,
                                                             ImmutableSet<RedirectWithPriority> existingElements) {
        return existingElements.stream().filter(existingElement -> !existingElement.equals(redirectToRemove))
                .collect(CollectorsUtil.toImmutableSet());
    }

    private static final class PageRedirectComparator implements Comparator<RedirectWithPriority> {
        @Override
        public int compare(final RedirectWithPriority redirect1, final RedirectWithPriority redirect2) {
            return redirect1.priority() - redirect2.priority();
        }
    }

    private final static class RedirectWithPriority implements PageRedirect {
        private final PageRedirect pageRedirect;
        private final int priority;

        public RedirectWithPriority(final PageRedirect pageRedirect, final int priority) {
            this.pageRedirect = pageRedirect;
            this.priority = priority;
        }

        public int priority() {
            return priority;
        }

        @Override
        public Optional<String> url(final ApplicationUser user) {
            return pageRedirect.url(user);
        }

        @Override
        public boolean equals(final Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }
            final RedirectWithPriority that = (RedirectWithPriority) o;
            return Objects.equals(pageRedirect, that.pageRedirect);
        }

        @Override
        public int hashCode() {
            return Objects.hash(pageRedirect);
        }
    }
}
