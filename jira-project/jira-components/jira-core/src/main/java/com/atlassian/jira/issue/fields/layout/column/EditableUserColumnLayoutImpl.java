package com.atlassian.jira.issue.fields.layout.column;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.customfields.CustomFieldUtils;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.user.ApplicationUser;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;


public class EditableUserColumnLayoutImpl extends EditableColumnLayoutImpl implements EditableUserColumnLayout {
    private final ApplicationUser user;

    public EditableUserColumnLayoutImpl(List<ColumnLayoutItem> columnLayoutItems, ApplicationUser user, ColumnConfig columnConfig) {
        // Ensure the Layout List is editable
        super(new ArrayList<ColumnLayoutItem>(columnLayoutItems), columnConfig);
        if (user == null)
            throw new IllegalArgumentException("ApplicationUser cannot be null.");
        this.user = user;
    }

    public EditableUserColumnLayoutImpl(List<ColumnLayoutItem> columnLayoutItems, ApplicationUser user) {
        this(columnLayoutItems, user, ColumnConfig.USER);
    }

    public ApplicationUser getUser() {
        return user;
    }

    public List<ColumnLayoutItem> getColumnLayoutItems() {
        FieldManager fieldManager = ComponentAccessor.getFieldManager();

        // Return the internal list minus the project custom fields the user can not see.
        List<ColumnLayoutItem> columnLayoutItems = new LinkedList<ColumnLayoutItem>();
        for (final ColumnLayoutItem columnLayoutItem : getInternalList()) {
            if (fieldManager.isCustomField(columnLayoutItem.getNavigableField())) {
                CustomField customField = (CustomField) columnLayoutItem.getNavigableField();
                if (CustomFieldUtils.isUserHasPermissionToProjects(customField, getUser())) {
                    columnLayoutItems.add(columnLayoutItem);
                }
            } else {
                columnLayoutItems.add(columnLayoutItem);
            }
        }
        return columnLayoutItems;
    }
}
