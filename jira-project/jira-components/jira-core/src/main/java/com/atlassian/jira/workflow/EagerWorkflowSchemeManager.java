package com.atlassian.jira.workflow;

import com.atlassian.beehive.ClusterLock;
import com.atlassian.beehive.ClusterLockService;
import com.atlassian.cache.CacheManager;
import com.atlassian.cache.CachedReference;
import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.EventComponent;
import com.atlassian.jira.association.NodeAssociationStore;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.database.QueryDslAccessor;
import com.atlassian.jira.entity.EntityUtils;
import com.atlassian.jira.event.ClearCacheEvent;
import com.atlassian.jira.event.scheme.AbstractSchemeAddedToProjectEvent;
import com.atlassian.jira.event.scheme.AbstractSchemeCopiedEvent;
import com.atlassian.jira.event.scheme.AbstractSchemeEvent;
import com.atlassian.jira.event.scheme.AbstractSchemeRemovedFromProjectEvent;
import com.atlassian.jira.event.scheme.AbstractSchemeUpdatedEvent;
import com.atlassian.jira.event.workflow.WorkflowSchemeAddedToProjectEvent;
import com.atlassian.jira.event.workflow.WorkflowSchemeCopiedEvent;
import com.atlassian.jira.event.workflow.WorkflowSchemeCreatedEvent;
import com.atlassian.jira.event.workflow.WorkflowSchemeDeletedEvent;
import com.atlassian.jira.event.workflow.WorkflowSchemeRemovedFromProjectEvent;
import com.atlassian.jira.event.workflow.WorkflowSchemeUpdatedEvent;
import com.atlassian.jira.exception.DataAccessException;
import com.atlassian.jira.exception.RemoveException;
import com.atlassian.jira.extension.Startable;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.model.querydsl.QNodeAssociation;
import com.atlassian.jira.model.querydsl.QWorkflowScheme;
import com.atlassian.jira.model.querydsl.QWorkflowSchemeEntity;
import com.atlassian.jira.model.querydsl.WorkflowSchemeEntityDTO;
import com.atlassian.jira.ofbiz.FieldMap;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.permission.PermissionContextFactory;
import com.atlassian.jira.permission.PermissionTypeManager;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.scheme.AbstractSchemeManager;
import com.atlassian.jira.scheme.Scheme;
import com.atlassian.jira.scheme.SchemeEntity;
import com.atlassian.jira.scheme.SchemeFactory;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.tenancy.TenantAware;
import com.atlassian.jira.tenancy.TenantInfo;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.ApplicationUsers;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.collect.MapBuilder;
import com.atlassian.jira.util.dbc.Assertions;
import com.atlassian.jira.workflow.migration.WorkflowSchemeMigrationTaskAccessor;
import com.atlassian.util.concurrent.ManagedLocks;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.querydsl.core.Tuple;
import com.querydsl.core.types.ExpressionUtils;
import com.querydsl.sql.SQLExpressions;
import org.apache.commons.lang.StringUtils;
import org.ofbiz.core.entity.EntityUtil;
import org.ofbiz.core.entity.GenericEntityException;
import org.ofbiz.core.entity.GenericValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.locks.Lock;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import static com.atlassian.jira.util.dbc.Assertions.notNull;
import static com.atlassian.jira.workflow.EagerWorkflowSchemeManager.WorkflowAction.DELETE_ENTITY;
import static com.atlassian.jira.workflow.EagerWorkflowSchemeManager.WorkflowAction.DELETE_SCHEME;
import static com.atlassian.jira.workflow.EagerWorkflowSchemeManager.WorkflowAction.DELETE_WORKFLOW_SCHEME;
import static com.atlassian.jira.workflow.EagerWorkflowSchemeManager.WorkflowAction.UPDATE_DRAFT_WORKFLOW_SCHEME;
import static com.atlassian.jira.workflow.EagerWorkflowSchemeManager.WorkflowAction.UPDATE_SCHEME;
import static com.atlassian.jira.workflow.EagerWorkflowSchemeManager.WorkflowAction.UPDATE_WORKFLOW_SCHEME;
import static java.lang.String.format;

@TenantInfo(TenantAware.UNRESOLVED)
@EventComponent
public class EagerWorkflowSchemeManager extends AbstractSchemeManager implements WorkflowSchemeManager, Startable {
    private final static String DARK_FEATURE_REQUEST_CACHE_ENABLE_KEY = "jira.jvc.EagerWorkflowSchemeManager.caches.request";

    /**
     * The workflow actions that we guard with locks.
     */
    enum WorkflowAction {

        DELETE_ENTITY,
        DELETE_SCHEME,
        DELETE_WORKFLOW_SCHEME,
        UPDATE_DRAFT_WORKFLOW_SCHEME,
        UPDATE_SCHEME,
        UPDATE_WORKFLOW_SCHEME;

        /**
         * Returns the name of this action's lock.
         *
         * @param id the ID of the thing being acted upon
         * @return a non-blank name
         */
        private String getLockName(final Long id) {
            return WorkflowAction.class.getName() + "." + this + "_" + id;
        }
    }

    private static final Logger log = LoggerFactory.getLogger(EagerWorkflowSchemeManager.class);
    private static final String ALL_ISSUE_TYPES = "0";
    private static final String SCHEME_ENTITY_NAME = "WorkflowScheme";
    private static final String WORKFLOW_ENTITY_NAME = "WorkflowSchemeEntity";
    private static final String SCHEME_DESC = "Workflow";
    private static final String DEFAULT_NAME_KEY = "admin.schemes.workflows.default";
    private static final String DEFAULT_DESC_KEY = "admin.schemes.workflows.default.desc";

    private static final String COLUMN_ISSUETYPE = "issuetype";
    private static final String COLUMN_WORKFLOW = "workflow";

    private static final ThreadLocal<Boolean> UPDATED_WORKFLOW_SCHEME_FIRE_EVENT = ThreadLocal.withInitial(() -> true);

    // Fields
    private final AssignableWorkflowScheme defaultScheme;

    private final WorkflowManager workflowManager;
    private final ConstantsManager constantsManager;
    private final OfBizDelegator ofBizDelegator;
    private final DraftWorkflowSchemeStore draftWorkflowSchemeStore;
    private final UserManager userManager;
    private final I18nHelper.BeanFactory i18nFactory;
    private final AssignableWorkflowSchemeStore assignableWorkflowSchemeStore;
    private final ClusterLockService clusterLockService;
    private final QueryDslAccessor queryDslAccessor;

    private final CachedReference<WorkflowSchemeEntitiesCache> cache;

    public EagerWorkflowSchemeManager(final ProjectManager projectManager, final PermissionTypeManager permissionTypeManager,
                                      final PermissionContextFactory permissionContextFactory, final SchemeFactory schemeFactory,
                                      final WorkflowManager workflowManager, final ConstantsManager constantsManager,
                                      final OfBizDelegator ofBizDelegator, final EventPublisher eventPublisher,
                                      final NodeAssociationStore nodeAssociationStore, final GroupManager groupManager,
                                      final DraftWorkflowSchemeStore draftWorkflowSchemeStore,
                                      final JiraAuthenticationContext context, final UserManager userManager, final I18nHelper.BeanFactory i18nFactory,
                                      final AssignableWorkflowSchemeStore assignableWorkflowSchemeStore, final CacheManager cacheManager,
                                      final ClusterLockService clusterLockService, QueryDslAccessor queryDslAccessor) {
        super(projectManager, permissionTypeManager, permissionContextFactory, schemeFactory,
                nodeAssociationStore, ofBizDelegator, groupManager, eventPublisher, cacheManager);
        this.workflowManager = workflowManager;
        this.constantsManager = constantsManager;
        this.ofBizDelegator = ofBizDelegator;
        this.draftWorkflowSchemeStore = draftWorkflowSchemeStore;
        this.userManager = userManager;
        this.i18nFactory = i18nFactory;
        this.assignableWorkflowSchemeStore = assignableWorkflowSchemeStore;
        this.clusterLockService = clusterLockService;
        this.defaultScheme = new DefaultWorkflowScheme(context);
        this.queryDslAccessor = queryDslAccessor;

        cache = cacheManager.getCachedReference(getClass().getName() + ".cache",
                this::loadAllWorkflowSchemeEntities);
    }


    public void start() {
        clearWorkflowCache();
        eventPublisher.register(this);
    }

    @EventListener
    public void onClearCache(final ClearCacheEvent event) {
        try {
            super.onClearCache(event);
        } finally {
            clearWorkflowCache();
        }
    }

    @Override
    public String getSchemeEntityName() {
        return SCHEME_ENTITY_NAME;
    }

    @Override
    public String getEntityName() {
        return WORKFLOW_ENTITY_NAME;
    }

    public void clearWorkflowCache() {
        cache.reset();
    }

    @Override
    public String getSchemeDesc() {
        return SCHEME_DESC;
    }

    @Override
    public String getDefaultNameKey() {
        return DEFAULT_NAME_KEY;
    }

    @Override
    public String getDefaultDescriptionKey() {
        return DEFAULT_DESC_KEY;
    }

    @Override
    public GenericValue getWorkflowScheme(final GenericValue project) throws GenericEntityException {
        return EntityUtil.getOnly(getSchemes(project));
    }

    @Override
    public GenericValue getWorkflowScheme(final Project project) throws GenericEntityException {
        return getWorkflowScheme(project.getGenericValue());
    }

    private Map<String, String> toWorkflowMap(Collection<WorkflowSchemeEntityDTO> related) {
        return related.stream()
                .filter(dto -> dto.getIssuetype() != null)
                .collect(Collectors.toMap(dto -> ALL_ISSUE_TYPES.equals(dto.getIssuetype()) ? null : dto.getIssuetype(),
                        WorkflowSchemeEntityDTO::getWorkflow));
    }

    @Override
    public boolean hasDraft(@Nonnull AssignableWorkflowScheme scheme) {
        notNull("scheme", scheme);

        return !scheme.isDefault() && scheme.getId() != null && draftWorkflowSchemeStore.hasDraftForParent(scheme.getId());
    }

    @Nonnull
    @Override
    public AssignableWorkflowScheme createScheme(@Nonnull AssignableWorkflowScheme workflowScheme) {
        notNull("wokflowScheme", workflowScheme);
        AssignableWorkflowSchemeStore.AssignableState savedState;
        try {
            AssignableWorkflowSchemeStore.AssignableState.Builder builder = assignableWorkflowSchemeStore.builder();
            builder.setName(workflowScheme.getName())
                    .setDescription(workflowScheme.getDescription())
                    .setMappings(workflowScheme.getMappings());

            savedState = assignableWorkflowSchemeStore.create(builder.build());
        } finally {
            clearWorkflowCache();
        }
        eventPublisher.publish(createSchemeCreatedEvent(getSchemeObject(savedState.getId())));
        return toWorkflowScheme(savedState);
    }

    public GenericValue createSchemeEntity(final GenericValue scheme, final SchemeEntity schemeEntity)
            throws GenericEntityException {
        return createSchemeEntity(scheme.getLong("id"), schemeEntity);
    }

    protected GenericValue createSchemeEntityNoEvent(final GenericValue scheme, final SchemeEntity schemeEntity)
            throws GenericEntityException {
        return createSchemeEntity(scheme.getLong("id"), schemeEntity);
    }

    public GenericValue createSchemeEntity(long schemeId, final SchemeEntity schemeEntity)
            throws GenericEntityException {
        Assertions.is("Workflow scheme IDs must be String values", schemeEntity.getEntityTypeId() instanceof String);

        try {
            return EntityUtils.createValue(getEntityName(), FieldMap.build("scheme", schemeId, COLUMN_WORKFLOW, schemeEntity.getType(),
                    COLUMN_ISSUETYPE, schemeEntity.getEntityTypeId().toString()));
        } finally {
            clearWorkflowCache();
        }
    }

    public List<GenericValue> getEntities(final GenericValue scheme, final String issuetype)
            throws GenericEntityException {
        final WorkflowSchemeEntityDTO value = cache.get().getByWorkflowId(getCacheKeyForScheme(scheme)).get(issuetype);
        if (value == null) {
            return Collections.emptyList();
        }
        return Collections.singletonList(value.toGenericValue(ofBizDelegator));
    }

    public Map<String, String> getWorkflowMap(Project project) {
        return getWorkflowMap(cache.get().getWorkflowIdForProject(project.getId()));
    }

    @VisibleForTesting
    Map<String, String> getWorkflowMap(Long schemeId) {
        if (schemeId == null) {
            return MapBuilder.build(null, JiraWorkflow.DEFAULT_WORKFLOW_NAME);
        } else {
            return toWorkflowMap(cache.get().getByWorkflowId(schemeId).values());
        }
    }

    public String getWorkflowName(Project project, String issueType) {
        return getWorkflowName(cache.get().getWorkflowIdForProject(project.getId()), issueType);
    }

    public String getWorkflowName(GenericValue scheme, String issueType) {
        if (scheme != null) {
            return getWorkflowName(scheme.getLong("id"), issueType);
        }

        // otherwise always return the default workflow
        return JiraWorkflow.DEFAULT_WORKFLOW_NAME;
    }

    private String getWorkflowName(Long schemeId, String issueType) {
        if (schemeId != null) {
            final Map<String, WorkflowSchemeEntityDTO> map = cache.get().getByWorkflowId(schemeId);
            WorkflowSchemeEntityDTO value = map.get(issueType);
            if (value == null) {
                value = map.get(ALL_ISSUE_TYPES);
            }
            if (value != null) {
                return value.getWorkflow();
            }
        }

        // otherwise always return the default workflow
        return JiraWorkflow.DEFAULT_WORKFLOW_NAME;
    }

    @Override
    public boolean isUsingDefaultScheme(Project project) {
        return cache.get().getWorkflowIdForProject(project.getId()) == null;
    }

    @VisibleForTesting
    GenericValue getSchemeForProject(final Project project) {
        return getSchemeForProject(project.getGenericValue());
    }

    @VisibleForTesting
    private GenericValue getSchemeForProject(final GenericValue project) {
        try {
            return EntityUtil.getOnly(getSchemes(project));
        } catch (GenericEntityException e) {
            throw new DataAccessException(e);
        }
    }

    private static Long getCacheKeyForScheme(final GenericValue scheme) {
        return scheme.getLong("id");
    }

    public List<GenericValue> getEntities(final GenericValue scheme, final Long entityTypeId) throws GenericEntityException {
        throw new IllegalArgumentException("Workflow scheme IDs must be String values.");
    }

    public List<GenericValue> getEntities(final GenericValue scheme, final Long entityTypeId, final String parameter) throws GenericEntityException {
        throw new IllegalArgumentException("Workflow scheme IDs must be String values.");
    }

    public List<GenericValue> getEntities(final GenericValue scheme, final String type, final Long entityTypeId) throws GenericEntityException {
        throw new IllegalArgumentException("Workflow scheme IDs must be String values.");
    }

    public GenericValue getDefaultEntity(final GenericValue scheme) throws GenericEntityException {
        return EntityUtil.getOnly(getEntities(scheme, ALL_ISSUE_TYPES));
    }

    @Override
    public AssignableWorkflowScheme getDefaultWorkflowScheme() {
        return defaultScheme;
    }

    public List<GenericValue> getNonDefaultEntities(final GenericValue scheme) throws GenericEntityException {
        return getEntities(scheme).stream()
                .filter(gv -> !ALL_ISSUE_TYPES.equals(gv.getString(COLUMN_ISSUETYPE)))
                .collect(Collectors.toList());
    }

    public Collection<String> getActiveWorkflowNames() throws GenericEntityException, WorkflowException {
        return cache.get().getActiveWorkflows();
    }

    public void addWorkflowToScheme(final GenericValue scheme, final String workflowName, final String issueTypeId) throws GenericEntityException {
        try {
            final SchemeEntity schemeEntity = new SchemeEntity(workflowName, issueTypeId);

            // prevent adding the same workflow multiple times to one scheme
            if (getEntities(scheme, issueTypeId).isEmpty()) {
                createSchemeEntity(scheme, schemeEntity);
            }
        } finally {
            clearWorkflowCache();
        }
    }

    @Override
    @Nonnull
    public DraftWorkflowScheme createDraftOf(ApplicationUser creator, @Nonnull AssignableWorkflowScheme workflowScheme) {
        notNull("workflowScheme", workflowScheme);
        Assertions.not("workflowScheme.default", workflowScheme.isDefault());
        notNull("workflowScheme.id", workflowScheme.getId());
        Assertions.not("scheme already has draft.", hasDraft(workflowScheme));

        final DraftWorkflowSchemeStore.DraftState.Builder builder = draftWorkflowSchemeStore.builder(workflowScheme.getId());
        builder.setMappings(workflowScheme.getMappings());
        builder.setLastModifiedUser(creator != null ? creator.getKey() : null);

        final DraftWorkflowSchemeStore.DraftState state = draftWorkflowSchemeStore.create(builder.build());
        return toWorkflowScheme(state);
    }

    @Nonnull
    @Override
    public DraftWorkflowScheme createDraft(ApplicationUser creator, @Nonnull DraftWorkflowScheme workflowScheme) {
        notNull("workflowScheme", workflowScheme);
        AssignableWorkflowScheme parentScheme = workflowScheme.getParentScheme();
        notNull("workflowScheme.parentScheme", parentScheme);
        notNull("workflowScheme.parentScheme.id", parentScheme.getId());

        Assertions.not("scheme already has draft.", hasDraft(parentScheme));

        final DraftWorkflowSchemeStore.DraftState.Builder builder = draftWorkflowSchemeStore.builder(parentScheme.getId());
        builder.setMappings(workflowScheme.getMappings());
        builder.setLastModifiedUser(ApplicationUsers.getKeyFor(creator));

        final DraftWorkflowSchemeStore.DraftState state = draftWorkflowSchemeStore.create(builder.build());
        return toWorkflowScheme(state);
    }

    @Override
    @Nonnull
    public Collection<AssignableWorkflowScheme> getAssignableSchemes() {
        Iterable<AssignableWorkflowScheme> workflowSchemes = StreamSupport.stream(assignableWorkflowSchemeStore.getAll().spliterator(), false)
                .map(toAssignableFunction())
                .collect(Collectors.toList());
        return WorkflowSchemes.nameOrdering().immutableSortedCopy(workflowSchemes);
    }

    public void updateSchemesForRenamedWorkflow(final String oldWorkflowName, final String newWorkflowName) {
        Assertions.notBlank("oldWorkflowName", oldWorkflowName);
        Assertions.notBlank("newWorkflowName", newWorkflowName);
        try {
            //TODO: Will have to fix this for workflow schemes.
            ofBizDelegator.bulkUpdateByAnd(getEntityName(), ImmutableMap.of(COLUMN_WORKFLOW, newWorkflowName), ImmutableMap.of(COLUMN_WORKFLOW, oldWorkflowName));
            draftWorkflowSchemeStore.renameWorkflow(oldWorkflowName, newWorkflowName);
        } finally {
            clearWorkflowCache();
        }
    }

    public Collection<GenericValue> getSchemesForWorkflow(final JiraWorkflow workflow) {
        // TODO This does not cater for default workflow which is used by schemes with no default for all issue types and
        // by projects that do not have a workflow scheme assigned
        final Collection<GenericValue> schemes = new LinkedList<>();
        final Set<Long> schemeIds = new HashSet<>();
        // Find all scheme entities with the workflow name
        final List<WorkflowSchemeEntityDTO> schemeEntities = cache.get().getAllEntities().stream()
                .filter(dto -> workflow.getName().equals(dto.getWorkflow()))
                .collect(Collectors.toList());
        // Loop through all the entities and retrieve the scheme ids
        for (final WorkflowSchemeEntityDTO schemeEntity : schemeEntities) {
            final Long schemeId = schemeEntity.getScheme();
            // Only retrieve schemes that we have not retrieved already
            if (schemeIds.add(schemeId)) {
                schemes.add(getScheme(schemeId));
            }
        }

        return schemes;
    }

    @Override
    public Iterable<WorkflowScheme> getSchemesForWorkflowIncludingDrafts(JiraWorkflow workflow) {
        if (workflow.isSystemWorkflow()) {
            throw new IllegalArgumentException("Can't get schemes for system workflow");
        }

        return Stream.concat(
                getSchemesForWorkflow(workflow).stream()
                        .map(this::toWorkflowScheme),
                StreamSupport.stream(draftWorkflowSchemeStore.getSchemesUsingWorkflow(workflow).spliterator(), false)
                        .map(this::toWorkflowScheme))
                .collect(Collectors.toList());
    }

    @Override
    public void deleteEntity(final Long id) throws DataAccessException {
        deleteEntities(Collections.singleton(id));
    }

    @Override
    public void deleteEntities(final Iterable<Long> ids) {
        acquireLocksAndExecute(ids, DELETE_ENTITY, () -> {
            try {
                EagerWorkflowSchemeManager.super.deleteEntities(ids);
            } finally {
                clearWorkflowCache();
            }
        });
    }

    /**
     * Gets all locks for ids and then after having acquired them executes the action.
     */
    private void acquireLocksAndExecute(final Iterable<Long> ids, final WorkflowAction lockType, final Runnable action) {
        if (Iterables.isEmpty(ids)) {
            action.run();
        } else {
            final Set<Long> sortedIds = Sets.newTreeSet(ids);
            ManagedLocks.manage(getLock(Iterables.getFirst(sortedIds, null), lockType)).withLock(() -> {
                acquireLocksAndExecute(Iterables.skip(sortedIds, 1), lockType, action);
            });
        }
    }

    private ClusterLock getLock(final Long id, final WorkflowAction action) {
        return clusterLockService.getLockForName(action.getLockName(id));
    }

    @Override
    protected SchemeEntity makeSchemeEntity(final GenericValue entity) {
        return new SchemeEntity(entity.getString(COLUMN_WORKFLOW), entity.getString(COLUMN_ISSUETYPE));
    }

    @Override
    protected Object createSchemeEntityDeletedEvent(final GenericValue entity) {
        return null;
    }

    @Override
    public boolean removeEntities(final GenericValue scheme, final Long entityTypeId) throws RemoveException {
        try {
            return super.removeEntities(scheme, entityTypeId);
        } finally {
            clearWorkflowCache();
        }
    }

    @Override
    public GenericValue createScheme(final String name, final String description) throws GenericEntityException {
        try {
            return super.createScheme(name, description);
        } finally {
            clearWorkflowCache();
        }
    }

    @Override
    protected AbstractSchemeEvent createSchemeCreatedEvent(final Scheme scheme) {
        return new WorkflowSchemeCreatedEvent(scheme);
    }

    @Override
    @Nonnull
    protected AbstractSchemeCopiedEvent createSchemeCopiedEvent(@Nonnull final Scheme oldScheme, @Nonnull final Scheme newScheme) {
        return new WorkflowSchemeCopiedEvent(oldScheme, newScheme);
    }

    @Override
    public void deleteScheme(final Long id) {
        if (id == null) {
            return;
        }
        ManagedLocks.manage(getLock(id, DELETE_SCHEME)).withLock(() -> doDeleteScheme(getWorkflowSchemeObj(id)));
    }

    @Nonnull
    @Override
    protected AbstractSchemeAddedToProjectEvent createSchemeAddedToProjectEvent(final Scheme scheme, final Project project) {
        return new WorkflowSchemeAddedToProjectEvent(scheme, project);
    }

    void doDeleteScheme(AssignableWorkflowScheme scheme) {
        try {
            checkMigration(scheme);

            try {
                super.deleteScheme(scheme.getId());
            } catch (GenericEntityException e) {
                throw new DataAccessException(e);
            }

            draftWorkflowSchemeStore.deleteByParentId(scheme.getId());
        } finally {
            clearWorkflowCache();
        }
        eventPublisher.publish(new WorkflowSchemeDeletedEvent(scheme.getId(), scheme.getName()));
    }

    @Override
    protected void flushProjectSchemes() {
        try {
            super.flushProjectSchemes();
        } finally {
            clearWorkflowCache();
        }
    }

    private Collection<IssueType> getAllIssueTypes() {
        return constantsManager.getAllIssueTypeObjects();
    }

    private JiraWorkflow getWorkflowFromScheme(final GenericValue workflowScheme, final String issueTypeId) {
        return workflowManager.getWorkflowFromScheme(workflowScheme, issueTypeId);
    }

    @Nullable
    @Override
    public AssignableWorkflowScheme getWorkflowSchemeObj(long id) {
        return toWorkflowScheme(getScheme(id));
    }

    @Override
    public AssignableWorkflowScheme getWorkflowSchemeObj(String name) {
        return toWorkflowScheme(getScheme(name));
    }

    @Override
    public DraftWorkflowScheme getDraftForParent(@Nonnull AssignableWorkflowScheme scheme) {
        notNull("scheme", scheme);

        if (scheme.isDefault() || scheme.getId() == null) {
            return null;
        } else {
            return getDraftForParent(scheme.getId());
        }
    }

    private DraftWorkflowScheme getDraftForParent(long parentId) {
        return toWorkflowScheme(draftWorkflowSchemeStore.getDraftForParent(parentId));
    }

    @Override
    public DraftWorkflowScheme getDraft(long id) {
        return toWorkflowScheme(draftWorkflowSchemeStore.get(id));
    }

    public AssignableWorkflowScheme getParentForDraft(long draftSchemeId) {
        return getWorkflowSchemeObj(draftWorkflowSchemeStore.getParentId(draftSchemeId));
    }

    @Override
    public boolean isActive(@Nonnull WorkflowScheme scheme) {
        if (notNull("scheme", scheme).isDraft()) {
            return false;
        }
        return cache.get().isWorkflowActive(scheme.getId()) ||
                (scheme.equals(defaultScheme) && cache.get().isDefaultWorkflowActive());
    }

    @Override
    public boolean deleteWorkflowScheme(@Nonnull final WorkflowScheme scheme) {
        notNull("scheme", scheme);
        Assertions.not("scheme.default", scheme.isDefault());
        notNull("scheme.id", scheme.getId());

        final long lockSchemeId;
        if (scheme.isDraft()) {
            lockSchemeId = ((DraftWorkflowScheme) scheme).getParentScheme().getId();
        } else {
            lockSchemeId = scheme.getId();
        }

        final Lock lock = getLock(lockSchemeId, DELETE_WORKFLOW_SCHEME);
        return ManagedLocks.manage(lock).withLock((com.atlassian.util.concurrent.Supplier<Boolean>) () -> {
            if (scheme.isDraft()) {
                DraftWorkflowScheme draftScheme = (DraftWorkflowScheme) scheme;

                WorkflowSchemeMigrationTaskAccessor taskAccessor = getTaskAccessor();
                if (taskAccessor.getActiveByProjects(draftScheme, true, true) != null) {
                    throw new SchemeIsBeingMigratedException();
                }

                return draftWorkflowSchemeStore.delete(scheme.getId());
            } else {
                Assertions.not("Cannot delete active scheme.", isActive(scheme));
                doDeleteScheme((AssignableWorkflowScheme) scheme);
                return true;
            }
        });
    }

    private void checkMigration(AssignableWorkflowScheme scheme) {
        if (scheme == null) {
            return;
        }

        WorkflowSchemeMigrationTaskAccessor taskAccessor = getTaskAccessor();

        if (taskAccessor.getActive(scheme) != null) {
            throw new SchemeIsBeingMigratedException();
        }
    }

    @Override
    public DraftWorkflowScheme updateDraftWorkflowScheme(final ApplicationUser user, @Nonnull final DraftWorkflowScheme scheme) {
        notNull("scheme", scheme);
        notNull("scheme.id", scheme.getId());

        // We lock the parent scheme in case of drafts
        final Lock lock = getLock(scheme.getParentScheme().getId(), UPDATE_DRAFT_WORKFLOW_SCHEME);
        return ManagedLocks.manage(lock).withLock((com.atlassian.util.concurrent.Supplier<DraftWorkflowScheme>) () -> {
            WorkflowSchemeMigrationTaskAccessor taskAccessor = getTaskAccessor();
            if (taskAccessor.getActiveByProjects(scheme, true) != null) {
                throw new SchemeIsBeingMigratedException();
            }

            DraftWorkflowSchemeStore.DraftState savedState = draftWorkflowSchemeStore.get(scheme.getId());

            notNull(format("scheme with id %d does not exist.", scheme.getId()), savedState);

            DraftWorkflowSchemeStore.DraftState.Builder builder = savedState.builder();
            builder.setMappings(scheme.getMappings());
            builder.setLastModifiedUser(user == null ? null : user.getKey());

            return toWorkflowScheme(draftWorkflowSchemeStore.update(builder.build()));
        });
    }

    @Override
    public AssignableWorkflowScheme updateWorkflowScheme(@Nonnull final AssignableWorkflowScheme workflowScheme) {
        notNull("scheme", workflowScheme);
        notNull("scheme.id", workflowScheme.getId());

        final Lock lock = getLock(workflowScheme.getId(), UPDATE_WORKFLOW_SCHEME);
        return ManagedLocks.manage(lock).withLock((com.atlassian.util.concurrent.Supplier<AssignableWorkflowScheme>) () -> {
            UPDATED_WORKFLOW_SCHEME_FIRE_EVENT.set(false);
            try {
                checkMigration(workflowScheme);

                final Scheme originalScheme = getSchemeObject(workflowScheme.getId());

                // Delete all existing entitites.
                for (SchemeEntity entity : originalScheme.getEntities()) {
                    deleteEntity(entity.getId());
                }

                doUpdateScheme(
                        new Scheme(
                                workflowScheme.getId(),
                                getSchemeEntityName(),
                                workflowScheme.getName(),
                                workflowScheme.getDescription(),
                                Collections.emptyList()
                        )
                );

                createSchemeEntities(workflowScheme);

                eventPublisher.publish(createSchemeUpdatedEvent(getSchemeObject(workflowScheme.getId()), originalScheme));

                return getWorkflowSchemeObj(workflowScheme.getId());
            } finally {
                UPDATED_WORKFLOW_SCHEME_FIRE_EVENT.remove();
            }
        });
    }

    @Override
    public void updateScheme(final Scheme scheme) throws DataAccessException {
        ManagedLocks.manage(getLock(scheme.getId(), UPDATE_SCHEME)).withLock(() -> {
            final AssignableWorkflowScheme schemeObj = toWorkflowScheme(getScheme(scheme.getId()));
            checkMigration(schemeObj);
            doUpdateScheme(scheme);
        });
    }

    @Override
    @Nonnull
    protected AbstractSchemeRemovedFromProjectEvent createSchemeRemovedFromProjectEvent(
            final Scheme scheme, final Project project) {
        return new WorkflowSchemeRemovedFromProjectEvent(scheme, project);
    }

    @Override
    protected AbstractSchemeUpdatedEvent createSchemeUpdatedEvent(final Scheme scheme, final Scheme originalScheme) {
        return new WorkflowSchemeUpdatedEvent(scheme, originalScheme);
    }

    private void doUpdateScheme(Scheme scheme) {
        try {
            super.updateScheme(scheme);
        } finally {
            clearWorkflowCache();
        }
    }

    WorkflowSchemeMigrationTaskAccessor getTaskAccessor() {
        return ComponentAccessor.getComponent(WorkflowSchemeMigrationTaskAccessor.class);
    }

    @Override
    public <T> T waitForUpdatesToFinishAndExecute(AssignableWorkflowScheme scheme, Callable<T> task) throws Exception {
        if (scheme == null || scheme.getId() == null) {
            return task.call();
        }
        final Collection<Lock> locks = Lists.newArrayList();
        try {
            for (final WorkflowAction action : WorkflowAction.values()) {
                final ClusterLock lock = getLock(scheme.getId(), action);
                lock.lock();
                locks.add(lock);
            }
            return task.call();
        } finally {
            unlockAll(locks);
        }
    }

    private void unlockAll(final Collection<Lock> locks) {
        final List<RuntimeException> exceptions = Lists.newArrayList();
        for (final Lock lock : locks) {
            try {
                lock.unlock();
            } catch (RuntimeException ex) {
                // Keep trying to release the others
                exceptions.add(ex);
                log.error("Error releasing lock " + lock, ex);
            }
        }
        if (!exceptions.isEmpty()) {
            // Probably OK to ignore any others as they'll likely have the same cause
            throw exceptions.iterator().next();
        }
    }

    private void createSchemeEntities(AssignableWorkflowScheme workflowScheme) {
        // Create all new entities
        for (Map.Entry<String, String> mappingEntry : workflowScheme.getMappings().entrySet()) {
            String issueTypeId = mappingEntry.getKey();
            if (issueTypeId == null) {
                issueTypeId = ALL_ISSUE_TYPES;
            }

            String workflowName = mappingEntry.getValue();

            SchemeEntity schemeEntity = new SchemeEntity(workflowName, issueTypeId);
            try {
                createSchemeEntity(workflowScheme.getId(), schemeEntity);
            } catch (GenericEntityException e) {
                throw new DataAccessException(e);
            }
        }
    }

    @Nonnull
    @Override
    public AssignableWorkflowScheme getWorkflowSchemeObj(@Nonnull Project project) {
        notNull("project", project);

        final GenericValue schemeForProject = getSchemeForProject(project);
        if (schemeForProject != null) {
            return toWorkflowScheme(schemeForProject);
        } else {
            return defaultScheme;
        }
    }

    @Nonnull
    @Override
    public List<Project> getProjectsUsing(@Nonnull AssignableWorkflowScheme workflowScheme) {
        Assertions.notNull("workflowScheme", workflowScheme);
        ImmutableList.Builder<Project> projects = new ImmutableList.Builder<>();

        for (Project project : projectManager.getProjectObjects()) {
            final GenericValue schemeForProject = getSchemeForProject(project);
            if (schemeForProject == null) {
                if (workflowScheme.isDefault()) {
                    projects.add(project);
                }
            } else {
                final Long id = schemeForProject.getLong("id");
                if (id != null && id.equals(workflowScheme.getId())) {
                    projects.add(project);
                }
            }
        }
        return projects.build();
    }

    @Override
    public AssignableWorkflowScheme cleanUpSchemeDraft(Project project, ApplicationUser user) {
        AssignableWorkflowScheme workflowScheme = getWorkflowSchemeObj(project);

        if (workflowScheme.isDefault()) {
            return null;
        }

        List<Project> projectsUsing = getProjectsUsing(workflowScheme);
        if (projectsUsing.size() > 1) {
            return null;
        }

        DraftWorkflowScheme draft = getDraftForParent(workflowScheme);
        if (draft == null) {
            return null;
        }

        AssignableWorkflowScheme.Builder copyOfDraftBuilder = new AssignableWorkflowSchemeBuilder()
                .setName(getNameForCopy(draft))
                .setDescription(getDescriptionForCopy(user, workflowScheme))
                .setMappings(draft.getMappings());

        AssignableWorkflowScheme scheme = createScheme(copyOfDraftBuilder.build());
        deleteWorkflowScheme(draft);

        return scheme;
    }

    @Override
    public AssignableWorkflowScheme copyDraft(DraftWorkflowScheme draft, ApplicationUser user, String newDescription) {
        AssignableWorkflowScheme.Builder copyOfDraftBuilder = new AssignableWorkflowSchemeBuilder()
                .setName(getNameForCopy(draft))
                .setDescription(newDescription)
                .setMappings(draft.getMappings());

        return createScheme(copyOfDraftBuilder.build());
    }

    private String getNameForCopy(WorkflowScheme scheme) {
        return getNameForCopy(scheme.getName(), 255);
    }

    @Override
    public void replaceSchemeWithDraft(DraftWorkflowScheme draft) {
        AssignableWorkflowScheme parentScheme = getParentForDraft(draft.getId())
                .builder()
                .setMappings(draft.getMappings())
                .build();

        updateWorkflowScheme(parentScheme);

        deleteWorkflowScheme(draft);
    }

    @Override
    public AssignableWorkflowScheme.Builder assignableBuilder() {
        return new AssignableWorkflowSchemeBuilder();
    }

    @Override
    public DraftWorkflowScheme.Builder draftBuilder(AssignableWorkflowScheme parent) {
        Assertions.notNull("parent", parent);
        Assertions.notNull("parent.id", parent.getId());

        return new DraftWorkflowSchemeBuilder(parent);
    }

    String getDescriptionForCopy(ApplicationUser user, AssignableWorkflowScheme workflowScheme) {
        StringBuilder sb = new StringBuilder();

        if (StringUtils.isNotBlank(workflowScheme.getDescription())) {
            sb.append(workflowScheme.getDescription()).append(" ");
        }

        sb.append(i18nFactory.getInstance(user).getText("admin.workflowschemes.manager.draft.auto.generated", workflowScheme.getName()));

        return sb.toString();
    }

    private AssignableWorkflowScheme toWorkflowScheme(GenericValue genericValue) {
        if (genericValue == null) {
            return null;
        } else {
            String name = genericValue.getString("name");
            String description = genericValue.getString("description");
            Long id = genericValue.getLong("id");
            final Map<String, String> map = getWorkflowMap(id);

            return new WorkflowSchemeImpl(id, name, description, map);
        }
    }

    private Function<AssignableWorkflowSchemeStore.AssignableState, AssignableWorkflowScheme> toAssignableFunction() {
        return EagerWorkflowSchemeManager::toWorkflowScheme;
    }

    private DraftWorkflowScheme toWorkflowScheme(DraftWorkflowSchemeStore.DraftState state) {
        if (state == null) {
            return null;
        } else {
            final ApplicationUser userByKey = userManager.getUserByKey(state.getLastModifiedUser());
            return new DraftWorkflowSchemeImpl(state, userByKey, getWorkflowSchemeObj(state.getParentSchemeId()));
        }
    }

    private static AssignableWorkflowScheme toWorkflowScheme(AssignableWorkflowSchemeStore.AssignableState state) {
        if (state == null) {
            return null;
        } else {
            return new WorkflowSchemeImpl(state);
        }
    }

    @Override
    public GenericValue copyScheme(final GenericValue oldScheme) throws GenericEntityException {
        try {
            return super.copyScheme(oldScheme);
        } finally {
            clearWorkflowCache();
        }
    }

    @Override
    public Scheme copyScheme(Scheme oldScheme) {
        try {
            return super.copyScheme(oldScheme);
        } finally {
            clearWorkflowCache();
        }
    }

    @Override
    public void updateScheme(final GenericValue entity) throws GenericEntityException {
        try {
            if (UPDATED_WORKFLOW_SCHEME_FIRE_EVENT.get()) {
                super.updateScheme(entity);
            } else {
                entity.store();
                flushProjectSchemes();
            }
        } finally {
            clearWorkflowCache();
        }
    }

    @Override
    public void removeSchemesFromProject(final Project project) throws DataAccessException {
        try {
            super.removeSchemesFromProject(project);
        } finally {
            clearWorkflowCache();
        }
    }


    @Override
    public void removeSchemesFromProject(final GenericValue project) throws GenericEntityException {
        try {
            super.removeSchemesFromProject(project);
        } finally {
            clearWorkflowCache();
        }
    }


    @Override
    public void addDefaultSchemeToProject(final GenericValue project) throws GenericEntityException {
        try {
            super.addDefaultSchemeToProject(project);
        } finally {
            clearWorkflowCache();
        }
    }

    @Override
    public void addDefaultSchemeToProject(final Project project) throws DataAccessException {
        try {
            super.addDefaultSchemeToProject(project);
        } finally {
            clearWorkflowCache();
        }
    }

    @Override
    public void addSchemeToProject(final GenericValue project, final GenericValue scheme) throws GenericEntityException {
        try {
            super.addSchemeToProject(project, scheme);
        } finally {
            clearWorkflowCache();
        }
    }

    @Override
    public void addSchemeToProject(final Project project, final Scheme scheme) throws DataAccessException {
        try {
            super.addSchemeToProject(project, scheme);
        } finally {
            clearWorkflowCache();
        }
    }

    @Override
    public Scheme createSchemeAndEntities(final Scheme scheme) throws DataAccessException {
        try {
            return super.createSchemeAndEntities(scheme);
        } finally {
            clearWorkflowCache();
        }
    }

    @Override
    public boolean removeEntities(final String type, final String parameter) throws RemoveException {
        try {
            return super.removeEntities(type, parameter);
        } finally {
            clearWorkflowCache();
        }
    }

    @Override
    protected GenericValue createSchemeNoEvent(final String name, final String description) throws GenericEntityException {
        try {
            return super.createSchemeNoEvent(name, description);
        } finally {
            clearWorkflowCache();
        }
    }

    @Override
    public Scheme createSchemeObject(final String name, final String description) {
        try {
            return super.createSchemeObject(name, description);
        } finally {
            clearWorkflowCache();
        }
    }

    private WorkflowSchemeEntitiesCache loadAllWorkflowSchemeEntities() {
        QNodeAssociation filteredNodeAssociation = new QNodeAssociation("NodeAssociationFiltered");
        return new WorkflowSchemeEntitiesCache(queryDslAccessor.executeQuery(callback -> callback.newSqlQuery()
                .select(filteredNodeAssociation.sourceNodeId, QWorkflowScheme.WORKFLOW_SCHEME.id, QWorkflowSchemeEntity.WORKFLOW_SCHEME_ENTITY)
                .from(QWorkflowScheme.WORKFLOW_SCHEME)
                .leftJoin(QWorkflowSchemeEntity.WORKFLOW_SCHEME_ENTITY)
                .on(QWorkflowScheme.WORKFLOW_SCHEME.id.eq(QWorkflowSchemeEntity.WORKFLOW_SCHEME_ENTITY.scheme))
                .leftJoin(SQLExpressions.select(QNodeAssociation.NODE_ASSOCIATION)
                                .from(QNodeAssociation.NODE_ASSOCIATION)
                                .where(QNodeAssociation.NODE_ASSOCIATION.sourceNodeEntity.eq("Project").and(
                                        QNodeAssociation.NODE_ASSOCIATION.sinkNodeEntity.eq("WorkflowScheme"))),
                        ExpressionUtils.path(QNodeAssociation.NODE_ASSOCIATION.getType(), "NodeAssociationFiltered"))
                .on(QWorkflowScheme.WORKFLOW_SCHEME.id.eq(filteredNodeAssociation.sinkNodeId))
                .fetch()), projectManager.getProjects());
    }

    private static class WorkflowSchemeEntitiesCache {
        private final Map<Long, Long> projectIdToSchemeId;
        private final Map<Long, Map<String, WorkflowSchemeEntityDTO>> workflowSchemeEntities;
        private final Set<String> activeWorkflows;
        private final boolean defaultWorkflowActive;

        public WorkflowSchemeEntitiesCache(List<Tuple> content, List<Project> projects) {
            Map<Long, Long> projectIdToSchemeId = Maps.newHashMap();
            Map<Long, Map<String, WorkflowSchemeEntityDTO>> workflowSchemeEntities = Maps.newHashMap();
            Set<String> activeWorkflows = Sets.newHashSet();
            boolean defaultWorkflowActive = false;

            for (Tuple tuple : content) {
                Long projectId = tuple.get(0, Long.class);
                Long workflowId = tuple.get(1, Long.class);
                WorkflowSchemeEntityDTO dto = tuple.get(2, WorkflowSchemeEntityDTO.class);
                // There is no project associated with this workflow.
                if (projectId != null) {
                    projectIdToSchemeId.put(projectId, workflowId);
                }
                workflowSchemeEntities.putIfAbsent(workflowId, Maps.newHashMap());
                if (dto != null && dto.getId() != null) {
                    workflowSchemeEntities.get(workflowId).put(dto.getIssuetype(), dto);
                    if (projectId != null) {
                        activeWorkflows.add(dto.getWorkflow());
                    }
                }
            }
            // If there's no workflow associated with the project, then it uses the default workflow scheme.
            // Because projects can be deleted, we can't just compare the sizes of the map and the list of projects.
            for (Project project : projects) {
                if (!projectIdToSchemeId.containsKey(project.getId())) {
                    activeWorkflows.add(JiraWorkflow.DEFAULT_WORKFLOW_NAME);
                    defaultWorkflowActive = true;
                    break;
                }
            }

            this.projectIdToSchemeId = Collections.unmodifiableMap(projectIdToSchemeId);
            this.workflowSchemeEntities = Collections.unmodifiableMap(workflowSchemeEntities);
            this.activeWorkflows = Collections.unmodifiableSet(activeWorkflows);
            this.defaultWorkflowActive = defaultWorkflowActive;
        }

        public Long getWorkflowIdForProject(Long projectId) {
            return projectIdToSchemeId.get(projectId);
        }

        public Map<String, WorkflowSchemeEntityDTO> getByWorkflowId(Long schemeId) {
            return workflowSchemeEntities.getOrDefault(schemeId, Collections.emptyMap());
        }

        public Set<String> getActiveWorkflows() {
            return activeWorkflows;
        }

        public List<WorkflowSchemeEntityDTO> getAllEntities() {
            return workflowSchemeEntities.values().stream()
                    .flatMap(entry -> entry.values().stream())
                    .collect(Collectors.toList());
        }

        public boolean isWorkflowActive(Long workflowId) {
            return projectIdToSchemeId.containsValue(workflowId);
        }

        public boolean isDefaultWorkflowActive() {
            return defaultWorkflowActive;
        }
    }
}
