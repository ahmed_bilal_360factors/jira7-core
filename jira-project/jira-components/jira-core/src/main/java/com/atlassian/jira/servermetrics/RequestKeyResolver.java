package com.atlassian.jira.servermetrics;


import com.atlassian.jira.util.collect.MapBuilder;
import com.google.common.collect.ImmutableMap;

import javax.servlet.http.HttpServletRequest;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;
import java.util.StringJoiner;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Transform request path into request key compatible with browser.metrics.
 * <p><strong>
 * This is temporary solution, consider joining events with browser metrics via correlationId or if this will be
 * impossible transfer reponsibility into plugins to provide request keys.
 * </strong></p>
 */
public class RequestKeyResolver {
    private static final LinkedHashMap<Pattern, String> KNOWN_REQUESTS = MapBuilder.<Pattern, String>newBuilder()
            .add(Pattern.compile(".*/IssueNavAction.*\\?.*\\bjql\\b.*\\bserverRenderedViewIssue\\b.*"), "jira.issue.nav-detail")
            .add(Pattern.compile(".*/IssueNavAction.*\\?.*\\bjql\\b.*"), "jira.issue.nav-list")
            .add(Pattern.compile(".*/IssueNavAction.*"), "jira.issue.view")
            .add(Pattern.compile(".*/Dashboard.*"), "jira.dashboard")
            .add(Pattern.compile(".*/RapidBoard.*"), "jira.agile.work")
            .toLinkedHashMap();

    public Optional<String> getRequestId(HttpServletRequest httpServletRequest) {
        final String request = RequestMetricsDispatcher.getRequestPath(httpServletRequest);
        final String parameterString = httpServletRequest
                .getParameterMap()
                .keySet()
                .stream()
                .sorted()
                .collect(Collectors.joining("&"));
        final String pathWithParameters = request + "?" + parameterString;

        return KNOWN_REQUESTS.entrySet().stream()
                .filter(entry -> entry.getKey().matcher(pathWithParameters).matches())
                .map(Map.Entry::getValue)
                .findFirst();
    }
}
