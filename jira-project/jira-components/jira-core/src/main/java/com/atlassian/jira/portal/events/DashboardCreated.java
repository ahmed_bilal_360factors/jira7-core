package com.atlassian.jira.portal.events;

import com.atlassian.jira.portal.PortalPage;
import com.atlassian.jira.user.ApplicationUser;
import com.google.common.base.Objects;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * Event fired when a portal page gets created.
 *
 * @since 5.0
 */
public final class DashboardCreated {
    public final PortalPage page;
    public final ApplicationUser loggedInUser;

    public DashboardCreated(final PortalPage page, final ApplicationUser user) {
        this.page = page;
        this.loggedInUser = user;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DashboardCreated that = (DashboardCreated) o;

        return Objects.equal(this.page, that.page) &&
                Objects.equal(this.loggedInUser, that.loggedInUser);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(page, loggedInUser);
    }
}
