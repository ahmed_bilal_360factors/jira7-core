package com.atlassian.jira.plugin.webpanel.notification;

import com.atlassian.collectors.CollectorsUtil;
import com.atlassian.jira.application.ApplicationRole;
import com.atlassian.jira.application.ApplicationRoleManager;
import com.atlassian.jira.util.BaseUrl;
import com.atlassian.plugin.web.ContextProvider;
import com.google.common.collect.ImmutableMap;

import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import static com.atlassian.jira.util.dbc.Assertions.notNull;

public final class UserLimitExceededContextProvider implements ContextProvider {
    private final ApplicationRoleManager applicationRoleManager;
    private final BaseUrl baseUrl;

    public UserLimitExceededContextProvider(final ApplicationRoleManager applicationRoleManager, final BaseUrl baseUrl) {
        this.baseUrl = baseUrl;
        this.applicationRoleManager = notNull("applicationRoleManager", applicationRoleManager);
    }

    @Override
    public void init(final Map<String, String> map) {
    }

    @Override
    public Map<String, Object> getContextMap(final Map<String, Object> map) {
        if (!applicationRoleManager.isAnyRoleLimitExceeded()) {
            throw new IllegalStateException("Application roles are not exceeded  - user limit exceeded context provider should not be called");
        }

        final List<ApplicationRole> affectedRoles = getAffectedRoles().collect(CollectorsUtil.toImmutableList());
        final ImmutableMap.Builder<String, Object> contextBuilder = ImmutableMap.<String, Object>builder()
                .put("baseUrl", baseUrl.getBaseUrl());
        final boolean justOne = affectedRoles.size() == 1;
        contextBuilder.put("justOne", justOne);
        if (justOne) {
            final ApplicationRole applicationRole = affectedRoles.stream().findFirst().get();
            contextBuilder.put("roleName", applicationRole.getName());
            contextBuilder.put("roleKey", applicationRole.getKey().value());
        }

        return contextBuilder.build();
    }

    private Stream<ApplicationRole> getAffectedRoles() {
        return applicationRoleManager.getRoles().stream()
                .filter(r -> applicationRoleManager.isRoleLimitExceeded(r.getKey()));
    }
}
