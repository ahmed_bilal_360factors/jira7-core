package com.atlassian.jira.upgrade.tasks.role;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.jira.ofbiz.FieldMap;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.google.common.collect.Sets;
import org.apache.commons.lang3.StringUtils;
import org.ofbiz.core.entity.GenericValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * @since v7.0
 */
public class DefaultApplicationDaoImpl implements DefaultApplicationDao {
    private static final Logger log = LoggerFactory.getLogger(DefaultApplicationDaoImpl.class);

    private static final String DEFAULT_APPLICATION_ENTITY = "LicenseRoleDefault";

    private final OfBizDelegator db;

    private static class Columns {
        private static final String ROLE_NAME = "licenseRoleName";
    }

    public DefaultApplicationDaoImpl(final OfBizDelegator db) {
        this.db = notNull("db", db);
    }

    @Override
    public Set<ApplicationKey> get() {
        final List<GenericValue> allDefaultApplications = db.findAll(DEFAULT_APPLICATION_ENTITY);
        final List<String> skippedRoleNames = allDefaultApplications.stream()
                .map(gv -> gv.getString(Columns.ROLE_NAME))
                .filter(this::isApplicationKeyCorrupted)
                .collect(Collectors.toList());

        if (!skippedRoleNames.isEmpty()) {
            log.warn("The applications keys are corrupted and will be skipped. Application keys = {}.", StringUtils.join(skippedRoleNames, ", "));
        }

        return allDefaultApplications.stream()
                .map(gv -> gv.getString(Columns.ROLE_NAME))
                .filter(ApplicationKey::isValid)
                .map(ApplicationKey::valueOf)
                .collect(Collectors.toSet());
    }

    @Override
    public void setApplicationsAsDefault(final Set<ApplicationKey> applicationKeys) {
        Set<ApplicationKey> currentDefaults = get();
        Sets.difference(applicationKeys, currentDefaults).stream()
                .forEach(this::put);
    }

    private void put(final ApplicationKey applicationKey) {
        log.info("The application {} will be stored to the database as a default one.", applicationKey);
        db.createValue(DEFAULT_APPLICATION_ENTITY, FieldMap.build(Columns.ROLE_NAME, applicationKey.value()));
    }

    public boolean isApplicationKeyCorrupted(String key) {
        return !ApplicationKey.isValid(key);
    }
}
