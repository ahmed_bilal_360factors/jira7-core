package com.atlassian.jira.web.filters;

public interface JiraImportProgressRedirectHandler {

    public static final String JOHNSON_ERRORS_PAGE = "/secure/errors.jsp";

    String getSetupPage();

    String getValidImportPage();

    String getInvalidImportPage();

    String getNoProgressPage();
}
