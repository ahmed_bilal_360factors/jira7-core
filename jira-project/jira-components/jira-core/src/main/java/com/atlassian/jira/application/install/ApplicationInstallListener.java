package com.atlassian.jira.application.install;

import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.plugin.event.events.PluginFrameworkStartingEvent;
import org.slf4j.LoggerFactory;

import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Connects to plugin system starting event and starts application instalation.
 *
 * @since v6.5
 */
public class ApplicationInstallListener {
    private static final org.slf4j.Logger log = LoggerFactory.getLogger(ApplicationInstallListener.class);
    private final EventPublisher eventPublisher;
    private final ApplicationInstaller installer;
    private final AtomicBoolean wasInstallationPerformed = new AtomicBoolean(false);

    public ApplicationInstallListener(final EventPublisher eventPublisher, final ApplicationInstaller installer) {
        this.eventPublisher = eventPublisher;
        this.installer = installer;
    }

    public void register() {
        eventPublisher.register(this);
    }

    @EventListener
    @SuppressWarnings("unused")
    public void onPluginSystemStarting(final PluginFrameworkStartingEvent event) {
        try {
            if (wasInstallationPerformed.compareAndSet(false, true)) {
                installer.installApplications();
            }
        } catch (final Exception e) {
            log.error("Failed to install applications", e);
        }
    }
}
