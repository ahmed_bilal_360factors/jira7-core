package com.atlassian.jira.issue.security;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.database.QueryDslAccessor;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.index.IndexException;
import com.atlassian.jira.issue.index.IssueIndexingService;
import com.atlassian.jira.issue.util.IssueIdsIssueIterable;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.scheme.Scheme;
import com.atlassian.jira.task.ProvidesTaskProgress;
import com.atlassian.jira.task.TaskProgressSink;
import com.atlassian.jira.task.context.Context;
import com.atlassian.jira.task.context.Contexts;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.jira.util.collect.FixedSized;
import com.atlassian.jira.util.dbc.Assertions;
import com.google.common.collect.ImmutableMap;
import com.querydsl.core.types.dsl.BooleanExpression;
import org.slf4j.Logger;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.atlassian.jira.model.querydsl.QIssue.ISSUE;
import static java.util.function.Function.identity;

/**
 * Background command for assigning a new issue security scheme to a project
 * @since v7.1.1
 */
public class AssignIssueSecuritySchemeCommand implements Callable<AssignIssueSecuritySchemeCommand.AssignSecurityLevelResult>, ProvidesTaskProgress {
    private final Project project;
    private final QueryDslAccessor queryDslAccessor;
    private final Long newSchemeId;
    private final Map<Long, Long> oldToNewLevelMapping;
    private final IssueSecuritySchemeManager issueSecuritySchemeManager;
    private final IssueSecurityLevelManager issueSecurityLevelManager;
    private final IssueIndexingService issueIndexingService;
    private final IssueManager issueManager;

    private final Logger log;
    private final I18nHelper i18nHelper;
    private volatile TaskProgressSink taskProgressSink;


    public AssignIssueSecuritySchemeCommand(final Project project,
                                            final Long newSchemeId,
                                            final Map<Long, Long> oldToNewLevelMapping,
                                            final QueryDslAccessor queryDslAccessor,
                                            final IssueSecuritySchemeManager issueSecuritySchemeManager,
                                            final IssueIndexingService issueIndexingService,
                                            final IssueManager issueManager,
                                            final Logger log,
                                            final I18nHelper i18nHelper) {
        this.newSchemeId = newSchemeId;
        this.issueSecurityLevelManager = ComponentAccessor.getComponent(IssueSecurityLevelManager.class);
        this.oldToNewLevelMapping = ImmutableMap.copyOf(oldToNewLevelMapping);
        this.project = project;
        this.queryDslAccessor = queryDslAccessor;
        this.issueSecuritySchemeManager = issueSecuritySchemeManager;
        this.issueIndexingService = issueIndexingService;
        this.issueManager = issueManager;
        this.log = log;
        this.i18nHelper = i18nHelper;
    }

    public AssignSecurityLevelResult call() {

        final Map<Long, Long> completeIssueMapping = makeCompleteIssueMapping();
        final int issuesToModify = countIssuesToModify(completeIssueMapping);
        final Context context = Contexts.builder()
                .sized(new FixedSized((issuesToModify * 101) / 100))
                .progressSubTask(taskProgressSink, i18nHelper, "admin.iss.associate.security.scheme.level")
                .build();

        try {
            changeLevels(context, project.getId(), completeIssueMapping);
            associateProjectToScheme(project, newSchemeId);
        } catch (IndexException | RuntimeException e) {
            log.error(e.getMessage(), e);
            return new AssignSecurityLevelResult(new SimpleErrorCollection(e.getMessage(), ErrorCollection.Reason.SERVER_ERROR));
        } finally {
            // clear the cache - Ugly stuff
            issueSecurityLevelManager.clearProjectLevels(project);
            // In case the operation does less work than we expected (due to bugs/errors/etc), we complete the progress manually.
            for (int i = 0; i < context.getNumberOfTasksToCompletion(); i++) {
                context.start(null).complete();
            }
        }
        return new AssignSecurityLevelResult();
    }

    private int countIssuesToModify(final Map<Long, Long> securityLevelMapping) {
        int totalSize = 0;
        for (final Map.Entry<Long, Long> mapping : securityLevelMapping.entrySet()) {
            final Long oldLevel = mapping.getKey();
            final Long newLevel = mapping.getValue();
            if (!oldLevel.equals(newLevel)) {
                final Long realOldLevelId = correctLevel(oldLevel);
                totalSize += issueSecurityLevelManager.getIssueCount(realOldLevelId, project.getId());
            }
        }
        return totalSize;
    }

    /**
     * Sets the levels of all affected issues to the newly specified levels
     *
     * @throws IndexException If indexing goes awry.
     */
    private void changeLevels(final Context taskContext, final Long projectId, final Map<Long, Long> oldToNewLevelMapping) throws IndexException {
        for (final Map.Entry<Long, Long> mapping : oldToNewLevelMapping.entrySet()) {
            final Long oldLevel = mapping.getKey();
            final Long newLevel = mapping.getValue();

            if (!oldLevel.equals(newLevel)) {
                final IssueSecurityLevel issueSecurityLevel = newLevel == null ? null : issueSecurityLevelManager.getSecurityLevel(newLevel);
                taskContext.setName(issueSecurityLevel != null ? issueSecurityLevel.getName() : i18nHelper.getText("common.words.none"));
                mapIssueSecurity(taskContext, projectId, oldLevel, newLevel);
            }
        }

    }

    private void mapIssueSecurity(final Context taskContext, final Long projectId, final Long oldLevel, final Long newLevel) throws IndexException {
        final long batchSize = 100;

        final Long realOldLevelId = correctLevel(oldLevel);
        final Long realNewLevelId = correctLevel(newLevel);

        while (true) {
            final List<Long> matchingIssues = queryDslAccessor.executeQuery(
                    dbConnection -> {
                        final BooleanExpression issueSecLevelPredicate = realOldLevelId == null ? ISSUE.security.isNull() : ISSUE.security.eq(realOldLevelId);
                        final List<Long> issues = dbConnection.newSqlQuery()
                                .select(ISSUE.id)
                                .from(ISSUE)
                                .where(issueSecLevelPredicate.and(ISSUE.project.eq(projectId)))
                                .limit(batchSize)
                                .fetch();

                        if (!issues.isEmpty()) {
                            final Context.Task task = taskContext.start(issues.get(0));
                            try {
                                if (!issues.isEmpty()) {
                                    dbConnection.update(ISSUE)
                                            .set(ISSUE.security, realNewLevelId)
                                            .where(ISSUE.id.in(issues))
                                            .execute();
                                }
                            } finally {
                                task.complete();
                            }
                        }
                        return issues;
                    });

            //we're done when there are no more issues left which use the old constant
            if (matchingIssues.isEmpty()) {
                break;
            }
            issueIndexingService.reIndexIssues(new IssueIdsIssueIterable(matchingIssues, issueManager), taskContext);
        }
    }

    /**
     * Null issue securities are represented in the UI and mappings as -1, we need to make sure null securities are stored as null.
     */
    private Long correctLevel(final Long level) {
        if (level == null) {
            return null;
        }
        final IssueSecurityLevel issueSecurity = issueSecurityLevelManager.getSecurityLevel(level);
        return issueSecurity == null ? null : issueSecurity.getId();
    }

    /**
     * Associates the project to the new issue security scheme
     *
     * @param project
     * @param newSchemeId
     */
    private void associateProjectToScheme(final Project project, final Long newSchemeId)  {
        //remove the current schemes from the project
        issueSecuritySchemeManager.removeSchemesFromProject(project);

        //if there is a new scheme then add it, otherwise we are just removing the current scheme (set to "None").
        if (this.newSchemeId != null) {
            //get the new scheme
            final Scheme scheme = issueSecuritySchemeManager.getSchemeObject(newSchemeId);
            if (scheme == null) {
                throw new RuntimeException("Issue Security Scheme '" + newSchemeId + "' not found");
            }
            //add it to the project
            issueSecuritySchemeManager.addSchemeToProject(project, scheme);
        }
    }

    private Map<Long, Long> makeCompleteIssueMapping() {
        final Map<Long, Long> completeOldToNewLevelMapping = new HashMap<>(oldToNewLevelMapping);

        final Long oldSchemeId = issueSecuritySchemeManager.getSchemeIdFor(project);

        if (oldSchemeId != null) {
            //Add missing levels to mapping result
            issueSecurityLevelManager.getIssueSecurityLevels(oldSchemeId).stream()
                    .map(IssueSecurityLevel::getId)
                    .filter(id -> !oldToNewLevelMapping.containsKey(id))
                    .forEach(issueSecurityLevelKey -> completeOldToNewLevelMapping.put(issueSecurityLevelKey, null));
        }
        return completeOldToNewLevelMapping;
    }

    public void setTaskProgressSink(final TaskProgressSink taskProgressSink) {
        this.taskProgressSink = taskProgressSink;
    }

    public static final class AssignSecurityLevelResult implements Serializable {
        private final SimpleErrorCollection errorCollection;

        public AssignSecurityLevelResult(final ErrorCollection errorCollection) {
            Assertions.notNull("errorCollection", errorCollection);
            this.errorCollection = new SimpleErrorCollection(errorCollection);
        }

        public AssignSecurityLevelResult() {
            this.errorCollection = new SimpleErrorCollection();
        }

        public ErrorCollection getErrorCollection() {
            return errorCollection;
        }

        public boolean isSuccessful() {
            return !errorCollection.hasAnyErrors();
        }

    }
}