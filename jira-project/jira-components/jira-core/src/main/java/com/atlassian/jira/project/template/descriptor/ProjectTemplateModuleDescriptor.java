package com.atlassian.jira.project.template.descriptor;

import com.atlassian.jira.cluster.ClusterSafe;
import com.atlassian.jira.project.template.hook.AddProjectModule;
import com.atlassian.jira.project.template.module.AddProjectModuleBuilder;
import com.atlassian.jira.project.template.module.Icon;
import com.atlassian.jira.project.template.module.ProjectTemplateModule;
import com.atlassian.jira.project.template.module.ProjectTemplateModuleBuilder;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginException;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.Resources;
import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.elements.ResourceDescriptor;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugin.web.Condition;
import com.atlassian.plugin.web.WebInterfaceManager;
import com.atlassian.plugin.web.baseconditions.AbstractConditionElementParser;
import com.atlassian.plugin.web.conditions.AlwaysDisplayCondition;
import com.atlassian.plugin.web.conditions.ConditionLoadingException;
import com.atlassian.plugin.web.descriptors.ConditionElementParser;
import com.atlassian.plugin.web.descriptors.ConditionalDescriptor;
import com.atlassian.plugin.webresource.WebResourceUrlProvider;
import com.atlassian.util.concurrent.ResettableLazyReference;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.ImmutableList;
import org.dom4j.Element;

import javax.annotation.Nonnull;
import javax.validation.constraints.NotNull;
import java.io.FileNotFoundException;
import java.util.Optional;

import static com.atlassian.jira.project.template.descriptor.PluginParseHelper.NULL_PARSE_HELPER;
import static org.apache.commons.lang3.StringUtils.isBlank;

public class ProjectTemplateModuleDescriptor extends AbstractModuleDescriptor<ProjectTemplateModule>
        implements ConditionalDescriptor {
    private final ConfigTemplateParser configTemplateParser;
    private final WebResourceUrlProvider urlProvider;
    private final ResourceDescriptorFactory resourceDescriptorFactory;
    private final WebInterfaceManager webInterfaceManager;
    private final ConditionElementParser conditionElementParser;

    /**
     * The XML element.
     */
    private Element element;

    /**
     * This module's condition.
     */
    private Condition condition;

    @ClusterSafe("Plugins are kept in synch across the cluster")
    private final ResettableLazyReference<ProjectTemplateModule> module = new ResettableLazyReference<ProjectTemplateModule>() {
        @Override
        protected ProjectTemplateModule create() {
            return createModuleFromXml(element);
        }
    };

    @SuppressWarnings("UnusedDeclaration")
    public ProjectTemplateModuleDescriptor(ModuleFactory moduleFactory, WebResourceUrlProvider urlProvider,
                                           ResourceDescriptorFactory resourceDescriptorFactory, WebInterfaceManager webInterfaceManager,
                                           ConfigTemplateParser configTemplateParser) {
        this(moduleFactory, urlProvider, resourceDescriptorFactory, webInterfaceManager, null, configTemplateParser);
    }

    @VisibleForTesting
    public ProjectTemplateModuleDescriptor(ModuleFactory moduleFactory, WebResourceUrlProvider urlProvider,
                                           ResourceDescriptorFactory resourceDescriptorFactory, WebInterfaceManager webInterfaceManager,
                                           ConditionElementParser.ConditionFactory conditionFactory, ConfigTemplateParser configTemplateParser) {
        super(moduleFactory);
        this.urlProvider = urlProvider;
        this.resourceDescriptorFactory = resourceDescriptorFactory;
        this.webInterfaceManager = webInterfaceManager;
        this.configTemplateParser = configTemplateParser;
        this.conditionElementParser = new ConditionElementParser(conditionFactory != null ? conditionFactory : new TemplateConditionFactory());
    }

    @Override
    public void init(@NotNull Plugin plugin, @NotNull Element element) throws PluginParseException {
        super.init(plugin, element);
        this.element = element;
    }

    @Override
    public void enabled() {
        super.enabled();
        condition = conditionElementParser.makeConditions(plugin, element, AbstractConditionElementParser.CompositeType.AND);
        module.reset();
    }

    @Override
    public void disabled() {
        super.disabled();
        module.reset();
    }

    @Override
    public ProjectTemplateModule getModule() {
        return module.get();
    }

    @Nonnull
    @Override
    public Condition getCondition() {
        return condition != null ? condition : new AlwaysDisplayCondition();
    }

    public void addResource(String path, String name) {
        try {
            ResourceDescriptor resource = resourceDescriptorFactory.createResource(plugin, name, path, Optional.empty());
            addToResources(resource);
        } catch (FileNotFoundException e) {
            throw new PluginException(String.format("File for '%s' does not exist at location: %s", name, path));
        }
    }

    private ProjectTemplateModule createModuleFromXml(Element element) {
        PluginParseHelper template = new DefaultPluginParseHelper(element, "//project-blueprint[@key='" + element.attribute("key").getValue() + "']");
        ProjectTemplateModuleBuilder builder = new ProjectTemplateModuleBuilder();

        String weight = template.optAttribute("weight");
        return builder.key(getPluginKey() + ":" + template.attribute("key"))
                .weight(weight != null ? Integer.valueOf(weight) : 100)
                .labelKey(template.element("label").attribute("key"))
                .descriptionKey(template.element("description").attribute("key"))
                .longDescriptionKey(template.optElement("longDescription").map(longDescElem -> longDescElem.attribute("key")))
                .icon(createIconFor(template, "icon"))
                .backgroundIcon(createIconFor(template, "backgroundIcon"))
                .infoSoyPath(template.optElement("infoPage").orElse(NULL_PARSE_HELPER).attribute("soy-template"))
                .addProjectModule(createAddProjectModuleForTemplateElement(template))
                .projectTypeKey(template.element("projectTypeKey").text())
                .build();
    }

    private AddProjectModule createAddProjectModuleForTemplateElement(PluginParseHelper template) {
        Optional<PluginParseHelper> addProjectElement = template.optElement("add-project");
        AddProjectModule addProjectModule = null;
        if (addProjectElement.isPresent()) {
            AddProjectModuleBuilder addProjectModuleBuilder = getProjectTemplateModuleBuilder();
            addProjectModule = addProjectModuleBuilder
                    .addProjectHookClassName(checkClassExists(addProjectElement.get().optElement("hook").orElse(NULL_PARSE_HELPER).attribute("class")))
                    .templateConfigurationFile(addProjectElement.get().optElement("descriptor").orElse(NULL_PARSE_HELPER).attribute("file"))
                    .build();
        }

        return addProjectModule;
    }

    /**
     * Returns a new Icon instance for the element with the given name if it is present, adding the icon to this
     * module's list of resources. Returns null if there is no element called {@code elementName}.
     *
     * @param template    the project-blueprint element
     * @param elementName the name of the icon element
     * @return an Icon
     */
    private Icon createIconFor(PluginParseHelper template, String elementName) {
        Optional<PluginParseHelper> iconElement = template.optElement(elementName);
        String location = iconElement.orElse(NULL_PARSE_HELPER).attribute("location");
        String contentType = iconElement.orElse(NULL_PARSE_HELPER).optAttribute("content-type");
        if (isBlank(location)) {
            return new Icon();
        }

        String resourceName = elementName + getFileExtension(location);
        try {
            // add the file extension to the resource name for readability
            ResourceDescriptor resource = resourceDescriptorFactory.createResource(plugin, resourceName, location, Optional.ofNullable(contentType));
            addToResources(resource);

            return new Icon(urlProvider, location, getCompleteKey(), resource.getName());
        } catch (FileNotFoundException e) {
            throw new PluginException(String.format("Icon file for '%s' does not exist at location: %s", elementName, location));
        }
    }

    /**
     * Returns extension of the file at the given location, if any.
     *
     * @param path the path to the file
     * @return the file extension, or ""
     */
    private String getFileExtension(String path) {
        int indexOfDot = path != null ? path.lastIndexOf('.') : -1;
        if (indexOfDot != -1) {
            return path.substring(indexOfDot);
        }

        return "";
    }

    private void addToResources(ResourceDescriptor resource) {
        ImmutableList<ResourceDescriptor> build = ImmutableList.<ResourceDescriptor>builder()
                .addAll(resources.getResourceDescriptors())
                .add(resource).build();

        // add 1 resource to this module
        resources = new Resources(build);
    }

    /**
     * Checks if the given class exists.
     *
     * @param className a class name
     * @return className if it exists. Null otherwise.
     */
    private String checkClassExists(String className) {
        if (isBlank(className)) {
            return null;
        }

        try {
            // Here we just check whether the class exists; in the ProjectTemplateModule we'll check whether it is an instance of AddProjectHook.
            // We can't do it the other way around because we would be creating a class of a non-initialized project-create-hook module descriptor.
            getPlugin().loadClass(className, this.getClass());
            return className;
        } catch (ClassNotFoundException e) {
            throw new PluginException("Class '" + className + "' not found.", e);
        }
    }

    protected AddProjectModuleBuilder getProjectTemplateModuleBuilder() {
        return new AddProjectModuleBuilder(configTemplateParser, moduleFactory, this);
    }

    private class TemplateConditionFactory implements ConditionElementParser.ConditionFactory {
        public Condition create(String className, Plugin plugin) throws ConditionLoadingException {
            //noinspection deprecation
            return webInterfaceManager.getWebFragmentHelper().loadCondition(className, plugin);
        }
    }
}
