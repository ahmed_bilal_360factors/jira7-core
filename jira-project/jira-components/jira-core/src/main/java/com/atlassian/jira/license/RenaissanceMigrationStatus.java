package com.atlassian.jira.license;

/**
 * A store to indicate if renaissance migration (aka JIRA 6.x global permission to JIRA 7.0 application roles)
 * has been run. Components that need to change their behaviour based on if the migration has occurred
 * can use this component to detect their mode.
 *
 * @since v7.0
 */
public interface RenaissanceMigrationStatus {
    /**
     * Mark the migration as done.
     *
     * @return the previous value of the predicate.
     */
    boolean markDone();

    /**
     * Return true if the renaissance migration is done.
     *
     * @return true if the renaissance migration is done.
     */
    boolean hasMigrationRun();
}

