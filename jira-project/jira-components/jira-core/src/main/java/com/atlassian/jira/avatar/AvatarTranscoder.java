package com.atlassian.jira.avatar;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * @since 7.0
 * <p>
 * A component that can perform the conversion of images from vector (SVG) format to raster format (PNG).
 */
public interface AvatarTranscoder {
    /**
     * Creates a PNG image file for the given avatar based on the base avatar image file which should be in SVG format.
     * The created file is also saved on disk.
     * The subsequent requests for the same avatar and size will not trigger the conversion, previously saved file will be returned.
     *
     * @param avatar to be transcoded
     * @param size   of requested raster
     * @param inputStream the data of the avatar to be transcoded
     * @return a handle to avatar image file in PNG format
     * @throws IOException
     */
    File getOrCreateRasterizedAvatarFile(Avatar avatar, Avatar.Size size, InputStream inputStream) throws IOException;

    /**
     * Converts given SVG data to PNG format. The default avatar size will be used which is
     * {@link Avatar.Size}.MEDIUM.
     *
     * @param inputStream  to be converted
     * @param outputStream to which the result should be written
     * @throws IOException
     */
    void transcodeAndTag(InputStream inputStream, OutputStream outputStream) throws IOException;

    /**
     * Converts a given SVG data to PNG format.
     * <p>
     * This method will save the converted image to disk so that if the method is invoked
     * once again with the same {@code imageKey} and {@code size} the previous result will
     * be returned. Therefore it is very important to choose {@code imageKey} carefully -
     * it should uniquely identify the image.
     * </p>
     *
     * @param imageKey    name of the file that
     * @param inputStream svg data
     * @param size        size of the output
     * @return byte array containing the PNG image
     */
    byte[] transcodeAndTag(String imageKey, InputStream inputStream, Avatar.Size size) throws IOException;
}
