package com.atlassian.jira.mail;

import com.atlassian.jira.issue.subscription.FilterSubscription;
import com.atlassian.jira.user.ApplicationUser;

public interface SubscriptionMailQueueItemFactory {
    SubscriptionMailQueueItem getSubscriptionMailQueueItem(FilterSubscription sub);

    SubscriptionSingleRecepientMailQueueItem createSelfEvaluatingEmailQueueItem(final FilterSubscription sub,
                                                                                final ApplicationUser recipient);
}
