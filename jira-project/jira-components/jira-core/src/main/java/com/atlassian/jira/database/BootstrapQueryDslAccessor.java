package com.atlassian.jira.database;

import com.atlassian.core.ofbiz.CoreFactory;
import com.atlassian.jira.config.database.DatabaseConfigurationManager;
import com.google.common.annotations.VisibleForTesting;
import org.ofbiz.core.entity.DelegatorInterface;

import javax.annotation.Nonnull;
import java.sql.Connection;

/**
 * Provides access to the database from within the bootstrap container if and only if the database has actually
 * been configured.
 * <p>
 * All methods throw an {@code IllegalStateException} if anything tries to use this before the database is configured.
 * They can still get it injected, though.  In other words, you can look, but you can't touch.  'Cause the best things
 * in life ain't cheap, no!
 * </p>
 *
 * @since v7.1.0
 */
public class BootstrapQueryDslAccessor implements QueryDslAccessor {
    protected final DatabaseConfigurationManager databaseConfigurationManager;

    private volatile QueryDslAccessor delegate;

    public BootstrapQueryDslAccessor(DatabaseConfigurationManager databaseConfigurationManager) {
        this.databaseConfigurationManager = databaseConfigurationManager;
    }


    @Override
    public <T> T executeQuery(@Nonnull QueryCallback<T> callback) {
        return getDelegate().executeQuery(callback);
    }

    @Override
    public void execute(@Nonnull SqlCallback callback) {
        getDelegate().execute(callback);
    }

    @Override
    public ConnectionProvider withNewConnection() {
        return getDelegate().withNewConnection();
    }

    @Override
    public ConnectionProvider withLegacyOfBizTransaction() {
        return getDelegate().withLegacyOfBizTransaction();
    }

    @Override
    public DbConnection withDbConnection(Connection connection) {
        return getDelegate().withDbConnection(connection);
    }

    private QueryDslAccessor getDelegate() {
        final QueryDslAccessor existing = delegate;
        return (existing != null) ? existing : initDelegate();
    }

    synchronized private QueryDslAccessor initDelegate() {
        if (delegate == null) {
            if (!databaseConfigurationManager.isDatabaseSetup()) {
                throw new IllegalStateException("The database has not been configured, yet!");
            }

            delegate = createDelegate();
        }

        return delegate;
    }

    @VisibleForTesting
    QueryDslAccessor createDelegate() {
        final DelegatorInterface delegatorInterface = CoreFactory.getGenericDelegator();
        final DatabaseAccessor databaseAccessor = new DatabaseAccessorImpl(databaseConfigurationManager, delegatorInterface);
        return new DefaultQueryDslAccessor(databaseAccessor, delegatorInterface, databaseConfigurationManager);
    }
}
