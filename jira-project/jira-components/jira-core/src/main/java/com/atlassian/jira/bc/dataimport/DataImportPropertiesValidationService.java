package com.atlassian.jira.bc.dataimport;

import com.atlassian.jira.bc.ServiceResult;
import com.atlassian.jira.bc.ServiceResultImpl;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.ozymandias.SafePluginPointAccess;
import com.atlassian.plugin.PluginAccessor;

import java.util.Set;

import static com.atlassian.jira.bc.dataimport.DataImportOSPropertyValidator.DataImportProperties;
import static com.google.common.collect.Sets.newHashSet;

/**
 * Aggregates all enabled instances of {@link DataImportOSPropertyValidator}.
 */
public class DataImportPropertiesValidationService {

    private final PluginAccessor pluginAccessor;

    public DataImportPropertiesValidationService(PluginAccessor pluginAccessor) {
        this.pluginAccessor = pluginAccessor;
    }

    /**
     * Generates the collection of property keys that should be recorded when we parse the backup XML so we can get the
     * associated values and validate them. This is achieved by aggregating all property keys that will be validated by
     * all of our validators.
     *
     * @return The collection of property keys that should be recorded when we parse the backup XML
     */
    Set<String> getPropertyKeysToRecord() {
        Set<String> propertyKeys = newHashSet();
        SafePluginPointAccess.to(pluginAccessor).forType(DataImportOSPropertyValidatorModuleDescriptor.class,
                (DataImportOSPropertyValidatorModuleDescriptor descriptor, DataImportOSPropertyValidator validator) -> {
                    propertyKeys.addAll(validator.getPropertyKeysToValidate());
                });

        return propertyKeys;
    }

    /**
     * Validates the backup XML. In this method, we call {@link DataImportOSPropertyValidator#validate(DataImportParams, DataImportOSPropertyValidator.DataImportProperties)}
     * on each of our validators.
     *
     * @param dataImportParams Import parameters provided by user during import process
     * @param dataImportProperties Property entries loaded from the backup XML being validated.
     *
     * @return A valid {@link com.atlassian.jira.bc.ServiceResult} if there are no validation errors. Else, an invalid result
     *         containing all the validation errors.
     */
    ServiceResult validate(DataImportParams dataImportParams, DataImportProperties dataImportProperties) {
        ErrorCollection errorCollection = new SimpleErrorCollection();
        SafePluginPointAccess.to(pluginAccessor).forType(DataImportOSPropertyValidatorModuleDescriptor.class,
                (DataImportOSPropertyValidatorModuleDescriptor descriptor, DataImportOSPropertyValidator validator) -> {
                    ServiceResult result = validator.validate(dataImportParams, dataImportProperties);
                    errorCollection.addErrorCollection(result.getErrorCollection());
                });

        return new ServiceResultImpl(errorCollection);
    }
}
