package com.atlassian.jira.avatar.temporary;

import com.atlassian.jira.avatar.Avatar;
import com.atlassian.jira.avatar.AvatarManager;
import com.atlassian.jira.avatar.TemporaryAvatar;
import com.atlassian.jira.avatar.TemporaryAvatars;
import com.atlassian.jira.icon.IconOwningObjectId;
import com.atlassian.jira.icon.IconType;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.dbc.Assertions;
import com.atlassian.jira.web.ExecutingHttpRequest;
import com.atlassian.jira.web.SessionKeys;

import javax.annotation.Nonnull;
import javax.servlet.http.HttpSession;

public class TemporaryAvatarsImpl implements TemporaryAvatars {
    private final AvatarManager avatarManager;

    public TemporaryAvatarsImpl(final AvatarManager avatarManager) {
        this.avatarManager = avatarManager;
    }

    @Override
    public void storeTemporaryAvatar(ApplicationUser remoteUser, TemporaryAvatar avatar, Avatar.Type type, String ownerId)
            throws IllegalAccessException {
        Assertions.notNull("type", type);
        IconType iconType = IconType.of(type.getName());
        if (iconType == null) {
            throw new IllegalAccessException("The icon type " + type.getName() + " is unknown.");
        }
        storeTemporaryAvatar(remoteUser, avatar, iconType, new IconOwningObjectId(ownerId));
    }

    @Override
    public void storeTemporaryAvatar(ApplicationUser remoteUser, TemporaryAvatar avatar, @Nonnull IconType iconType, IconOwningObjectId ownerId)
            throws IllegalAccessException {

        if (!avatarManager.userCanCreateFor(remoteUser, iconType, ownerId)) {
            throw new IllegalAccessException("User cannot store temporary avatars");
        }

        ExecutingHttpRequest.get().getSession().setAttribute(SessionKeys.TEMP_AVATAR, avatar);
    }

    @Override
    public TemporaryAvatar getCurrentTemporaryAvatar() {
        final HttpSession session = ExecutingHttpRequest.get().getSession();

        return (TemporaryAvatar) session.getAttribute(SessionKeys.TEMP_AVATAR);
    }

    @Override
    public void dispose(TemporaryAvatar avatar) {
        avatar.getFile().delete();

        final HttpSession session = ExecutingHttpRequest.get().getSession();
        session.removeAttribute(SessionKeys.TEMP_AVATAR);
    }
}
