package com.atlassian.jira.upgrade.tasks;

import com.atlassian.jira.config.properties.JiraProperties;
import com.atlassian.jira.config.properties.SystemPropertyKeys;
import com.atlassian.jira.upgrade.UpgradeProvider;
import com.atlassian.upgrade.spi.UpgradeTask;
import com.atlassian.upgrade.spi.UpgradeTaskFactory;
import java.util.Collection;
import java.util.Optional;
import javax.annotation.Nonnull;

/**
 * The upgrade factory for JIRA, this provides all of the {@link UpgradeTask}s that can be run.
 */
public class JiraUpgradeTaskFactory implements UpgradeTaskFactory {

    private final UpgradeProvider upgradeProvider;
    private final JiraProperties jiraProperties;

    public JiraUpgradeTaskFactory(final UpgradeProvider upgradeProvider, final JiraProperties jiraProperties) {
        this.upgradeProvider = upgradeProvider;
        this.jiraProperties = jiraProperties;
    }

    /**
     * JIRA checks minimum required build number on its own hence here we always return 0.
     *
     * @return minimum required build number
     * @see com.atlassian.jira.appconsistency.db.MinimumUpgradableVersionCheck
     */
    @Override
    public int getMinimumBuildNumber() {
        return 0;
    }

    @Override
    public String getProductDisplayName() {
        return "JIRA";
    }

    @Override
    public String getProductMinimumVersion() {
        return "4.0";
    }

    @Nonnull
    @Override
    public Collection<UpgradeTask> getAllUpgradeTasks() {
        return buildNumberLimit()
                .map(upgradeProvider::<UpgradeTask>getUpgradeTasksBoundByBuild)
                .orElseGet(upgradeProvider::<UpgradeTask>getUpgradeTasks);
    }

    private Optional<Long> buildNumberLimit() {
        return Optional.ofNullable(jiraProperties.getLong(SystemPropertyKeys.UPGRADE_TASKS_LIMIT));
    }
}
