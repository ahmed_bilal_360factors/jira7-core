package com.atlassian.jira.plugin.renderer;

import com.atlassian.jira.plugin.AbstractJiraModuleDescriptor;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.plugin.module.ModuleFactory;

public class RendererComponentDecoratorFactoryDescriptor extends AbstractJiraModuleDescriptor<RendererComponentDecoratorFactory> {
    public RendererComponentDecoratorFactoryDescriptor(final JiraAuthenticationContext authenticationContext, final ModuleFactory moduleFactory) {
        super(authenticationContext, moduleFactory);
    }
}
