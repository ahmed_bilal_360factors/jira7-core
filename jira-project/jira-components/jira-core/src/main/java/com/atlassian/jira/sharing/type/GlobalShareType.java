package com.atlassian.jira.sharing.type;

import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;

/**
 * Implementation of the ShareType that allows a {@link com.atlassian.jira.sharing.SharedEntity} to be shared with all users on a JIRA instance.
 * This includes the sharing with the anonymous user.
 *
 * This contrasts with the {@link AuthenticatedUserShareType} that restricts the shared entity to be visible only to logged in users.
 *
 * @since v3.13
 */
public class GlobalShareType extends AbstractShareType {
    public static final Name TYPE = ShareType.Name.GLOBAL;
    public static final int PRIORITY = 4;
    private final ApplicationProperties applicationProperties;

    public GlobalShareType(final GlobalShareTypeRenderer renderer, final GlobalShareTypeValidator validator, final ApplicationProperties applicationProperties) {
        super(GlobalShareType.TYPE, true, GlobalShareType.PRIORITY, renderer, validator, new GlobalShareTypePermissionChecker(),
                new GlobalShareQueryFactory(), new DefaultSharePermissionComparator(GlobalShareType.TYPE));
        this.applicationProperties = applicationProperties;
    }

    @Override
    public boolean isAvailable() {
        return applicationProperties.getOption(APKeys.JIRA_OPTION_GLOBAL_SHARING);
    }
}
