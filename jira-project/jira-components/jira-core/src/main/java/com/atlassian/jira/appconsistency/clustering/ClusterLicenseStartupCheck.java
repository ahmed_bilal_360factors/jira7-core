package com.atlassian.jira.appconsistency.clustering;

import com.atlassian.fugue.Option;
import com.atlassian.jira.application.ApplicationKeys;
import com.atlassian.jira.license.Jira6xServiceDeskPluginEncodedLicenseSupplier;
import com.atlassian.jira.license.JiraLicenseManager;
import com.atlassian.jira.license.LicenseCheck;
import com.atlassian.jira.license.LicenseDetails;
import com.atlassian.jira.startup.StartupCheck;
import com.atlassian.jira.util.I18nHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Checks that this instance is appropriately licensed for clustered operation ("Data Center").
 * <p>
 * Note: This class was introduced in 6.3 as {@code com.atlassian.jira.appconsistency.clustering.ClusterLicenseCheck},
 * and has been renamed to its current name in 7.0.
 *
 * @since 7.0
 */
public class ClusterLicenseStartupCheck implements StartupCheck {
    private static final Logger LOG = LoggerFactory.getLogger(ClusterLicenseStartupCheck.class);

    static final String NAME = "JIRA Cluster License Check";
    static final String FAULT_DESC = "startup.cluster.license.check";

    private final JiraLicenseManager licenseManager;
    private final I18nHelper i18nHelper;
    private final LicenseCheck licenseCheck;
    private final Jira6xServiceDeskPluginEncodedLicenseSupplier serviceDeskLicenseSupplier;

    private LicenseCheck.Result licenseCheckResult;

    public ClusterLicenseStartupCheck(com.atlassian.jira.license.ClusterLicenseCheck licenseCheck, I18nHelper i18nHelper,
                                      JiraLicenseManager licenseManager, Jira6xServiceDeskPluginEncodedLicenseSupplier serviceDeskLicenseSupplier) {
        this.licenseManager = licenseManager;
        this.licenseCheck = licenseCheck;
        this.i18nHelper = i18nHelper;
        this.serviceDeskLicenseSupplier = serviceDeskLicenseSupplier;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public boolean isOk() {
        LOG.debug("Performing JIRA Cluster License Check");
        licenseCheckResult = licenseCheck.evaluate();
        return licenseCheckResult.isPass() || removeNonDataCenterLicenses();
    }

    @Override
    public String getFaultDescription() {
        return i18nHelper.getText(FAULT_DESC);
    }

    @Override
    public String getHTMLFaultDescription() {
        return getFaultDescription();
    }

    /**
     * In 7.0, ServiceDesk plugin license (if present) is returned along with other JIRA licenses, which fails the
     * basic test that all licenses must be DataCenter in order to run in clustered mode; so if 1 or more non-datacentre
     * licenses are present, we forcibly remove them so as not to leave the instance in an unusable state.
     *
     * @since 7.0
     * @deprecated This is only relevant when moving from pre-7.0 and can be removed when we no longer need to support
     * that.
     */
    @Deprecated
    private boolean removeNonDataCenterLicenses() {
        assert licenseCheckResult != null : "removeNonDataCenterLicenses() called before isOk()";
        assert !licenseCheckResult.isPass() : "removeNonDataCenterLicenses() called when isOk() returned true";

        List<LicenseDetails> failedLicenses = licenseCheckResult.getFailedLicenses();
        try {
            licenseManager.removeLicenses(failedLicenses);
        } catch (Throwable unexpected) {
            LOG.error("Problem encountered while trying to remove DataCenter-incompatible licenses. "
                    + "DataCenter may not be able to start up. If this problem persists, you may need to remove "
                    + "licenses directly from the data store.", unexpected);
            return false;
        }

        // special-case check for presence of a ServiceDesk plugin license, which as of 7.0 is included in the
        // collection of returned licenses from `licenseManager.getLicenses()`. If the SD license has already been
        // migrated to the central 7.0 location for licenses then it will have been remove above. However, if migration
        // hasn't run, we'll need to remove it from UPM explicitly and put it in a special deleted state.
        if (containsServiceDeskPluginLicense(failedLicenses)) {
            Option<String> serviceDeskLicense = serviceDeskLicenseSupplier.get();
            if (serviceDeskLicense.isDefined()) {
                // then remove it from UPM plugin store
                serviceDeskLicenseSupplier.moveToUpgradeStore();
            }
        }

        LOG.warn("Some licenses were found to be incompatible with DataCenter and have been removed from this instance");
        return true;
    }

    /**
     * Returns true if any of the passed licenses grants seats for the ServiceDesk application.
     */
    private boolean containsServiceDeskPluginLicense(List<LicenseDetails> failedLicenses) {
        return failedLicenses.stream().anyMatch(lic -> lic.hasApplication(ApplicationKeys.SERVICE_DESK));
    }
}
