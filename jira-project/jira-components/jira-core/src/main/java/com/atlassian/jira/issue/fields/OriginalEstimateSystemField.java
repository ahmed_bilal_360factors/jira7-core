package com.atlassian.jira.issue.fields;

import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueFieldConstants;
import com.atlassian.jira.issue.comparator.IssueLongFieldComparator;
import com.atlassian.jira.issue.export.ExportableSystemField;
import com.atlassian.jira.issue.export.FieldExportParts;
import com.atlassian.jira.issue.export.FieldExportPartsBuilder;
import com.atlassian.jira.issue.search.LuceneFieldSorter;
import com.atlassian.jira.issue.statistics.TimeTrackingStatisticsMapper;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.template.VelocityTemplatingEngine;

public class OriginalEstimateSystemField extends AbstractDurationSystemField implements ExportableSystemField {
    public OriginalEstimateSystemField(VelocityTemplatingEngine templatingEngine, ApplicationProperties applicationProperties, JiraAuthenticationContext authenticationContext) {
        super(IssueFieldConstants.TIME_ORIGINAL_ESTIMATE, "common.concepts.original.estimate", "common.concepts.original.estimate", ORDER_DESCENDING, new IssueLongFieldComparator(IssueFieldConstants.TIME_ORIGINAL_ESTIMATE), templatingEngine, applicationProperties, authenticationContext);
    }

    public LuceneFieldSorter getSorter() {
        return TimeTrackingStatisticsMapper.TIME_ESTIMATE_ORIG;
    }

    public String getHiddenFieldId() {
        return IssueFieldConstants.TIMETRACKING;
    }

    protected Long getDuration(Issue issue) {
        return issue.getOriginalEstimate();
    }


    @Override
    public FieldExportParts getRepresentationFromIssue(Issue issue) {
        final Long duration = getDuration(issue);
        if (duration != null) {
            return FieldExportPartsBuilder.buildSinglePartRepresentation(getId(), getName(), String.valueOf(duration));
        } else {
            return FieldExportPartsBuilder.buildSinglePartRepresentation(getId(), getName(), "");
        }
    }
}
