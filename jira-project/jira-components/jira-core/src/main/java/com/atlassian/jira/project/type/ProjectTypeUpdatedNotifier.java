package com.atlassian.jira.project.type;

import com.atlassian.jira.project.Project;
import com.atlassian.jira.user.ApplicationUser;

/**
 * Class in charge to send notifications to all {@link com.atlassian.jira.project.type.ProjectTypeUpdatedHandler}.
 */
public interface ProjectTypeUpdatedNotifier {
    /**
     * Notifies all {@link com.atlassian.jira.project.type.ProjectTypeUpdatedHandler} objects that a project type update has happened.
     *
     * @param user           The user performing the project update
     * @param project        The project for which the project type has been updated
     * @param oldProjectType The old project type
     * @param newProjectType The new project type
     * @return False if any of the handlers failed while handling the notification. True if everything went ok.
     */
    boolean notifyAllHandlers(ApplicationUser user, Project project, ProjectTypeKey oldProjectType, ProjectTypeKey newProjectType);
}
