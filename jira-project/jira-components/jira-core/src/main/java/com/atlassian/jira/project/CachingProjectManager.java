package com.atlassian.jira.project;

import com.atlassian.cache.CacheManager;
import com.atlassian.cache.CachedReference;
import com.atlassian.event.api.EventListener;
import com.atlassian.jira.EventComponent;
import com.atlassian.jira.association.NodeAssociationStore;
import com.atlassian.jira.bc.project.ProjectCreationData;
import com.atlassian.jira.bc.project.component.ProjectComponentManager;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.event.ClearCacheEvent;
import com.atlassian.jira.exception.DataAccessException;
import com.atlassian.jira.exception.RemoveException;
import com.atlassian.jira.project.type.ProjectTypeKey;
import com.atlassian.jira.project.util.ProjectKeyStore;
import com.atlassian.jira.task.context.Context;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;

@EventComponent
public class CachingProjectManager extends AbstractProjectManager {
    private final ProjectManager delegateProjectManager;
    private final ProjectComponentManager projectComponentManager;
    private final ProjectFactory projectFactory;
    private final ProjectKeyStore projectKeyStore;
    private final CachedReference<ProjectCache> cache;
    private final NodeAssociationStore nodeAssociationStore;

    public CachingProjectManager(ProjectManager delegateProjectManager, ProjectComponentManager projectComponentManager,
                                 ProjectFactory projectFactory, UserManager userManager, ApplicationProperties applicationProperties,
                                 ProjectKeyStore projectKeyStore, CacheManager cacheManager, NodeAssociationStore nodeAssociationStore) {
        super(userManager, applicationProperties);
        this.delegateProjectManager = delegateProjectManager;
        this.projectComponentManager = projectComponentManager;
        this.projectFactory = projectFactory;
        this.projectKeyStore = projectKeyStore;
        this.nodeAssociationStore = nodeAssociationStore;
        this.cache = cacheManager.getCachedReference(getClass().getName() + ".cache",
                () -> new ProjectCache(delegateProjectManager, projectKeyStore, nodeAssociationStore));
    }

    @EventListener
    public void onClearCache(final ClearCacheEvent event) {
        updateCache();
        //make sure the project count cache is reloaded (the delegate may also do some caching)
        delegateProjectManager.refresh();
    }

    public void updateCache() {
        // refresh the project cache
        projectKeyStore.refresh();
        cache.reset();
    }

    // Business Methods ------------------------------------------------------------------------------------------------
    // Create Methods --------------------------------------------------------------------------------------------------
    @Override
    public long getNextId(Project project) {
        return delegateProjectManager.getNextId(project);
    }

    @Override
    public void refresh() {
        updateCache();
        delegateProjectManager.refresh();
    }

    // Get / Finder Methods --------------------------------------------------------------------------------------------
    @Override
    public Project getProjectObj(Long id) {
        return cache.get().getProject(id);
    }

    @Override
    public Project getProjectObjByKey(String projectKey) {
        return cache.get().getProjectByKey(projectKey);
    }

    @Override
    public Project getProjectByCurrentKey(final String projectKey) {
        return cache.get().getProjectByCurrentKey(projectKey);
    }

    @Override
    public Project getProjectByCurrentKeyIgnoreCase(final String projectKey) {
        return cache.get().getProjectByCurrentKeyIgnoreCase(projectKey);
    }

    @Override
    public Project getProjectObjByKeyIgnoreCase(final String projectKey) {
        return cache.get().getProjectByKeyIgnoreCase(projectKey);
    }

    @Override
    public Set<String> getAllProjectKeys(Long projectId) {
        return cache.get().getAllProjectKeys(projectId);
    }

    @Override
    public Project getProjectObjByName(String projectName) {
        return cache.get().getProjectByName(projectName);
    }

    @Nonnull
    @Override
    public List<Project> getProjects() {
        List<Project> projects = cache.get().getProjectObjects();
        if (projects == null) {
            return Collections.emptyList();
        }
        return projects;
    }

    @Nonnull
    @Override
    public List<Project> getProjectObjects() {
        return getProjects();
    }

    @Override
    public long getProjectCount() throws DataAccessException {
        return getProjectObjects().size();
    }

    protected static <T> Collection<T> noNull(Collection<T> col) {
        if (col == null) {
            return Collections.emptyList();
        } else {
            return col;
        }
    }

    @Override
    public Project createProject(@Nonnull ApplicationUser user, @Nonnull ProjectCreationData projectCreationData) {
        try {
            return delegateProjectManager.createProject(user, projectCreationData);
        } finally {
            updateCache();
        }
    }


    @Override
    public Project updateProject(UpdateProjectParameters parameters) {
        try {
            return delegateProjectManager.updateProject(parameters);
        } finally {
            updateCache();
        }
    }

    @Override
    public Project updateProject(final Project updatedProject, final String name, final String description,
                                 final String leadKey, final String url, final Long assigneeType, final Long avatarId, final String projectKey) {
        try {
            return delegateProjectManager.updateProject(updatedProject, name, description, leadKey, url, assigneeType, avatarId, projectKey);
        } finally {
            updateCache();
        }
    }

    @Override
    public Project updateProjectType(final ApplicationUser user, final Project project, final ProjectTypeKey newProjectType) {
        try {
            return delegateProjectManager.updateProjectType(user, project, newProjectType);
        } finally {
            updateCache();
        }
    }

    @Override
    public void removeProjectIssues(final Project project) throws RemoveException {
        delegateProjectManager.removeProjectIssues(project);
    }

    @Override
    public void removeProjectIssues(Project project, Context taskContext) throws RemoveException {
        delegateProjectManager.removeProjectIssues(project, taskContext);
    }

    @Override
    public void removeProject(final Project project) {
        try {
            delegateProjectManager.removeProject(project);
        } finally {
            updateCache();
        }
    }

    @Override
    public Collection<ProjectCategory> getAllProjectCategories() {
        return noNull(cache.get().getProjectCategories());
    }

    @Override
    public ProjectCategory getProjectCategory(Long id) {
        return getProjectCategoryObject(id);
    }

    @Override
    @Nullable
    public ProjectCategory getProjectCategoryObject(Long id) {
        return cache.get().getProjectCategory(id);
    }

    @Override
    public void updateProjectCategory(ProjectCategory projectCategory) throws DataAccessException {
        try {
            delegateProjectManager.updateProjectCategory(projectCategory);
        } finally {
            updateCache();
        }
    }

    @Override
    public Collection<Project> getProjectsFromProjectCategory(ProjectCategory projectCategory)
            throws DataAccessException {
        return getProjectObjectsFromProjectCategory(projectCategory.getId());
    }

    @Override
    public Collection<Project> getProjectObjectsFromProjectCategory(final Long projectCategoryId) {
        return cache.get().getProjectsFromProjectCategory(projectCategoryId);
    }

    @Override
    @Nullable
    public ProjectCategory getProjectCategoryForProject(Project project) {
        return cache.get().getProjectCategoryForProject(project);
    }

    @Override
    public Collection<Project> getProjectObjectsWithNoCategory() throws DataAccessException {
        return cache.get().getProjectsWithNoCategory();
    }

    @Override
    public void setProjectCategory(Project project, ProjectCategory category) throws DataAccessException {
        try {
            delegateProjectManager.setProjectCategory(project, category);
        } finally {
            updateCache();
        }
    }

    @Override
    public List<Project> getProjectsLeadBy(ApplicationUser leadUser) {
        return delegateProjectManager.getProjectsLeadBy(leadUser);
    }

    @Override
    public ProjectCategory createProjectCategory(String name, String description) {
        try {
            return delegateProjectManager.createProjectCategory(name, description);
        } finally {
            updateCache();
        }
    }

    @Override
    public void removeProjectCategory(Long id) {
        try {
            delegateProjectManager.removeProjectCategory(id);
        } finally {
            updateCache();
        }
    }

    @Override
    public boolean isProjectCategoryUnique(final String projectCategory) {
        return delegateProjectManager.isProjectCategoryUnique(projectCategory);
    }

    @Override
    public long getCurrentCounterForProject(Long id) {
        return delegateProjectManager.getCurrentCounterForProject(id);
    }

    @Override
    public void setCurrentCounterForProject(Project project, long counter) {
        delegateProjectManager.setCurrentCounterForProject(project, counter);
    }
}
