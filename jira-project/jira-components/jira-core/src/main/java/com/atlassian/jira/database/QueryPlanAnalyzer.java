package com.atlassian.jira.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Returns the explain plan over a SQL statement - optionally runs Analyze.
 *
 * @since v7.2
 */
public class QueryPlanAnalyzer {

    private static String EXPLAIN = "EXPLAIN";
    private static String EXPLAIN_ANALYZE = "EXPLAIN ANALYZE";

    public String explainStatement(final Connection connection, final PreparedStatement preparedStatement, final boolean analyze) {
        final StringBuilder qryBuilder = new StringBuilder();
        if (analyze) {
            qryBuilder.append(EXPLAIN_ANALYZE);
        } else {
            qryBuilder.append(EXPLAIN);
        }
        qryBuilder.append(" ").append(preparedStatement);
        try (Statement statement = connection.createStatement()) {
            ResultSet rs = statement.executeQuery(qryBuilder.toString());
            String plan = "";
            while (rs.next()) {
                plan += rs.getString(1);
            }
            return plan;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
