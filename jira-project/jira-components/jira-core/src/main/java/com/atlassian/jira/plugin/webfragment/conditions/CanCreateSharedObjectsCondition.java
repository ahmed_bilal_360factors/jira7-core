package com.atlassian.jira.plugin.webfragment.conditions;

import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.security.GlobalPermissionManager;

/**
 * Checks if the logged in user has the rights to create shared objects.
 *
 * @since v4.0
 */
public class CanCreateSharedObjectsCondition extends AbstractFixedPermissionCondition {
    public CanCreateSharedObjectsCondition(GlobalPermissionManager permissionManager) {
        super(permissionManager, GlobalPermissionKey.CREATE_SHARED_OBJECTS);
    }
}
