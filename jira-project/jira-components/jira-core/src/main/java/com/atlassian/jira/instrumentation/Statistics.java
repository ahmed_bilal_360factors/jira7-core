package com.atlassian.jira.instrumentation;

import java.io.Serializable;
import java.util.List;

/**
 * Represents instrumentation data on a 'name'. The keys of the map identify each instrument.
 *
 * @since v7.1
 */
public interface Statistics extends Serializable {
    String getName();

    String getLoggingKey();

    List<String> getTags();

    Object getStats();
}
