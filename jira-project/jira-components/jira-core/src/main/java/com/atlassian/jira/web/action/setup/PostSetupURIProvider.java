package com.atlassian.jira.web.action.setup;

import com.atlassian.application.api.Application;
import com.atlassian.application.api.ApplicationManager;
import com.atlassian.application.api.PlatformApplication;
import com.atlassian.fugue.Option;
import com.atlassian.fugue.Suppliers;
import com.atlassian.jira.web.action.issue.URLUtil;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import org.apache.commons.httpclient.URIException;
import org.apache.commons.httpclient.util.URIUtil;
import org.apache.log4j.Logger;

import java.net.URI;
import java.net.URISyntaxException;

/**
 * @since v7.0
 */
public class PostSetupURIProvider {
    private static Logger log = Logger.getLogger(PostSetupURIProvider.class);
    private final ApplicationManager applicationManager;

    public PostSetupURIProvider(final ApplicationManager applicationManager) {
        this.applicationManager = applicationManager;
    }

    public String getPostSetupRedirect(Option<String> origin) throws URISyntaxException {
        final Option<Application> applicationWithPostInstall = getApplicationWithPostInstall();

        final Option<URI> postInstallUri = applicationWithPostInstall.flatMap(new Function<Application, Option<URI>>() {
            @Override
            public Option<URI> apply(final Application input) {
                return input.getPostInstallURI();
            }
        });

        final String rawUri = postInstallUri.getOrElse(getDefaultPostSetupURI()).toASCIIString();
        final String redirectWithOrigin = origin.fold(Suppliers.ofInstance(rawUri), new Function<String, String>() {
            @Override
            public String apply(final String input) {
                return addParameterToURI(rawUri, input);
            }
        });

        return redirectWithOrigin;
    }

    private String addParameterToURI(final String rawUri, final String input) {
        try {
            return URLUtil.addRequestParameter(rawUri, "src=" + URIUtil.encodeQuery(input));
        } catch (URIException e) {
            return rawUri;
        }
    }

    private URI getDefaultPostSetupURI() throws URISyntaxException {
        // should it be defined in PlatformApplication?
        return new URI("Dashboard.jspa");
    }

    private Option<Application> getApplicationWithPostInstall() {
        final Option<Iterable<Application>> applications = Option.option(applicationManager.getApplications());

        return applications.flatMap(new Function<Iterable<Application>, Option<? extends Application>>() {
            @Override
            public Option<Application> apply(final Iterable<Application> applications) {
                return com.atlassian.fugue.Iterables.findFirst(applications, new Predicate<Application>() {
                    @Override
                    public boolean apply(final Application input) {
                        boolean isPlatform = input instanceof PlatformApplication;
                        boolean hasPostInstallStep = input.getPostInstallURI().isDefined();

                        if (log.isDebugEnabled()) {
                            log.debug(String.format(
                                    "Application %s(%s), isPlatform %b, postInstallStep: %s",
                                    input.getName(), input.getKey(), isPlatform, input.getPostInstallURI()));
                        }

                        return !isPlatform && hasPostInstallStep;
                    }
                });
            }
        });
    }
}
