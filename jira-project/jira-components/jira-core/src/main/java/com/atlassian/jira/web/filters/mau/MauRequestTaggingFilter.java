package com.atlassian.jira.web.filters.mau;

import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.fugue.Either;
import com.atlassian.jira.application.ApplicationKeys;
import com.atlassian.jira.event.mau.MauApplicationKey;
import com.atlassian.jira.event.mau.MauEventService;

import org.apache.commons.lang3.StringUtils;

import static com.atlassian.jira.application.ApplicationKeys.CORE;
import static com.atlassian.jira.application.ApplicationKeys.SERVICE_DESK;
import static com.atlassian.jira.application.ApplicationKeys.SOFTWARE;

/**
 * Servlet filter which will tag the current request with the mauApplicationKey specified in the initial filter params
 * for mau tracking purposes.
 *
 * @since v7.0.1
 */
public class MauRequestTaggingFilter extends AbstractMauRequestTaggingFilter {
    private FilterConfig filterConfig;

    @Override
    public void tagRequest(final MauEventService mauEventService, final HttpServletRequest request) {
        MauApplicationKey mauApplicationKey = determineMauApplicationKey(filterConfig.getInitParameter("mauApplicationKey"));
        
        if (mauApplicationKey != null) {
            mauEventService.setApplicationForThread(mauApplicationKey);            
        }
    }
    
    private MauApplicationKey determineMauApplicationKey(String mauKey) {
        if (StringUtils.isEmpty(mauKey)) {
            return null;
        }
        
        ApplicationKey applicationKey = toApplicationKey(mauKey);
        
        if (applicationKey != null) {
            return MauApplicationKey.forApplication(applicationKey);
        } else if (MauApplicationKey.family().getKey().equals(mauKey)) {
            return MauApplicationKey.family();
        }
        return null;
    }

    private ApplicationKey toApplicationKey(String key) {
        Either<String, ApplicationKey> apply = ApplicationKeys.TO_APPLICATION_KEY.apply(key);
        if (apply.isRight()) {
            ApplicationKey applicationKey = apply.right().get();
            if (CORE.equals(applicationKey) || SERVICE_DESK.equals(applicationKey) || SOFTWARE.equals(applicationKey)) {
                return applicationKey;
            }
        }
        return null;
    }

    @Override
    public void init(final FilterConfig filterConfig) throws ServletException {
        this.filterConfig = filterConfig;
    }
}
