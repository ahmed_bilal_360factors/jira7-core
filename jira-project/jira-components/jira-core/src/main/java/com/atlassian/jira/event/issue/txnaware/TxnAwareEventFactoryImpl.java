package com.atlassian.jira.event.issue.txnaware;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.event.issue.IssueChangedEvent;
import com.atlassian.jira.event.issue.IssueChangedEventImpl;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.changehistory.ChangeHistory;
import com.atlassian.jira.issue.changehistory.ChangeHistoryManager;
import com.atlassian.jira.issue.comments.Comment;
import com.atlassian.jira.issue.history.ChangeItemBean;
import com.atlassian.jira.transaction.RequestLocalTransactionRunnableQueueFactory;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.ComponentLocator;
import com.atlassian.ozymandias.SafePluginPointAccess;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static com.atlassian.core.ofbiz.util.CoreTransactionUtil.inTransaction;
import static java.util.Optional.ofNullable;

public class TxnAwareEventFactoryImpl implements TxnAwareEventFactory {
    private final EventPublisher eventPublisher;
    private final RequestLocalTransactionRunnableQueueFactory runnableQueueFactory;
    private final ComponentLocator componentLocator;

    public TxnAwareEventFactoryImpl(EventPublisher eventPublisher, RequestLocalTransactionRunnableQueueFactory runnableQueueFactory, ComponentLocator componentLocator) {
        this.eventPublisher = eventPublisher;
        this.runnableQueueFactory = runnableQueueFactory;
        this.componentLocator = componentLocator;
    }

    @Override
    public void issueChangedEventOnCommit(long issueId, ApplicationUser author, Comment comment, long changeGroupId) {
        issueChangedEventOnCommit(issueId, ofNullable(author), ofNullable(comment), changeGroupId);
    }

    private void issueChangedEventOnCommit(long issueId, Optional<ApplicationUser> authorOption, Optional<Comment> commentOption, long changeGroupIdOption) {
        runThisOnCommit(() -> {
            // calculate all things after the commit, so we get the latest data.
            Optional<ChangeHistory> changeHistory = getChangeHistory(changeGroupIdOption);
            changeHistory.ifPresent(history -> {
                List<ChangeItemBean> changeItems = history.getChangeItemBeans();
                // we fire if something has actually changed.
                if (changeItems != null && !changeItems.isEmpty()) {
                    final IssueChangedEvent issueChangeEvent = makeChangedEvent(issueId, authorOption, commentOption, changeItems, history.getTimePerformed());
                    publishEvent(issueChangeEvent);
                }
            });
        });
    }

    private Optional<ChangeHistory> getChangeHistory(long changeGroupId) {
        //
        // the following calls are actually LIVE DB calls :(  Thank you 2002 best practices!
        //
        // But we are making them here after the txn commit and hence they are consistent.
        //
        ChangeHistory changeHistory = componentLocator.getComponent(ChangeHistoryManager.class).getChangeHistoryById(changeGroupId);
        return ofNullable(changeHistory);
    }

    private IssueChangedEvent makeChangedEvent(long issueId, Optional<ApplicationUser> author, Optional<Comment> comment, List<ChangeItemBean> changeItems, Timestamp timePerformed) {
        Issue issue = getIssueObject(issueId);
        Date eventTime = new Date(timePerformed.getTime());
        return new IssueChangedEventImpl(issue, author, changeItems, comment, eventTime);
    }

    private Issue getIssueObject(long issueId) {
        IssueManager issueManager = componentLocator.getComponent(IssueManager.class);
        return issueManager.getIssueObject(issueId);
    }

    private void runThisOnCommit(Runnable onCommit) {
        if (inTransaction()) {
            // on commit make this event happen
            runnableQueueFactory.getRunnablesQueue().offer(onCommit);
        } else {
            // if not in a transaction run immediately
            SafePluginPointAccess.to().runnable(onCommit);
        }
    }

    private void publishEvent(IssueChangedEvent event) {
        eventPublisher.publish(event);
    }
}
