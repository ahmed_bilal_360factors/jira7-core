package com.atlassian.jira.config;

import com.atlassian.jira.config.database.DatabaseConfigurationManager;

import javax.annotation.Nullable;

/**
 * DatabaseConfigurationService loaded during bootstrap.
 * <p>
 * During bootstrap, it can be the case that the DB is not yet configured, so this needs to be careful about
 * not trying to read config in its constructor.
 * </p>
 *
 * @since 6.4.4
 */
public class BootstrapDatabaseConfigurationService implements DatabaseConfigurationService {
    private final DatabaseConfigurationManager databaseConfigurationManager;

    public BootstrapDatabaseConfigurationService(final DatabaseConfigurationManager databaseConfigurationManager) {
        this.databaseConfigurationManager = databaseConfigurationManager;
    }

    @Override
    @Nullable
    public String getSchemaName() {
        return databaseConfigurationManager.getDatabaseConfiguration().getSchemaName();
    }
}
