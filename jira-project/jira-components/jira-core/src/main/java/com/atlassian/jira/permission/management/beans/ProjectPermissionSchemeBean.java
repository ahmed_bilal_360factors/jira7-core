package com.atlassian.jira.permission.management.beans;

import com.google.common.base.Objects;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import org.codehaus.jackson.annotate.JsonAutoDetect;

import java.util.List;

@JsonAutoDetect
public class ProjectPermissionSchemeBean {
    Long id;
    String name;
    String description;
    List<ProjectPermissionBean> permissions;
    ProjectPermissionRenderingBean displayRendering;
    ProjectPermissionOperationResultBean operationResult;

    public ProjectPermissionSchemeBean() {
    }

    private ProjectPermissionSchemeBean(Long id, String name, String description, Iterable<ProjectPermissionBean> permissions, ProjectPermissionRenderingBean displayRendering, ProjectPermissionOperationResultBean operationResult) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.permissions = permissions != null ? ImmutableList.copyOf(permissions) : null;
        this.displayRendering = displayRendering;
        this.operationResult = operationResult;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<ProjectPermissionBean> getPermissions() {
        return permissions;
    }

    public void setPermissions(List<ProjectPermissionBean> permissions) {
        this.permissions = permissions != null ? ImmutableList.copyOf(permissions) : null;
    }

    public ProjectPermissionRenderingBean getDisplayRendering() {
        return displayRendering;
    }

    public void setDisplayRendering(ProjectPermissionRenderingBean displayRendering) {
        this.displayRendering = displayRendering;
    }

    public ProjectPermissionOperationResultBean getOperationResult() {
        return operationResult;
    }

    public void setOperationResult(ProjectPermissionOperationResultBean operationResult) {
        this.operationResult = operationResult;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static Builder builder(ProjectPermissionSchemeBean data) {
        return new Builder(data);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ProjectPermissionSchemeBean that = (ProjectPermissionSchemeBean) o;

        return Objects.equal(this.id, that.id) &&
                Objects.equal(this.name, that.name) &&
                Objects.equal(this.description, that.description) &&
                Objects.equal(this.permissions, that.permissions) &&
                Objects.equal(this.displayRendering, that.displayRendering) &&
                Objects.equal(this.operationResult, that.operationResult);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id, name, description, permissions, displayRendering, operationResult);
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("id", id)
                .add("name", name)
                .add("description", description)
                .add("permissions", permissions)
                .add("displayRendering", displayRendering)
                .add("operationResult", operationResult)
                .toString();
    }

    public static final class Builder {
        private Long id;
        private String name;
        private String description;
        private List<ProjectPermissionBean> permissions = Lists.newArrayList();
        private ProjectPermissionRenderingBean displayRendering;
        private ProjectPermissionOperationResultBean operationResult;

        private Builder() {
        }

        private Builder(ProjectPermissionSchemeBean initialData) {
            this.id = initialData.id;
            this.name = initialData.name;
            this.description = initialData.description;
            this.permissions = initialData.permissions;
            this.displayRendering = initialData.displayRendering;
            this.operationResult = initialData.operationResult;
        }

        public Builder setId(Long id) {
            this.id = id;
            return this;
        }

        public Builder setName(String name) {
            this.name = name;
            return this;
        }

        public Builder setDescription(String description) {
            this.description = description;
            return this;
        }

        public Builder setPermissions(List<ProjectPermissionBean> permissions) {
            this.permissions = permissions;
            return this;
        }

        public Builder addPermission(ProjectPermissionBean permission) {
            this.permissions.add(permission);
            return this;
        }

        public Builder addPermissions(Iterable<ProjectPermissionBean> permissions) {
            for (ProjectPermissionBean permission : permissions) {
                addPermission(permission);
            }
            return this;
        }

        public Builder setDisplayRendering(ProjectPermissionRenderingBean displayRendering) {
            this.displayRendering = displayRendering;
            return this;
        }

        public Builder setOperationResult(ProjectPermissionOperationResultBean operationResult) {
            this.operationResult = operationResult;
            return this;
        }

        public ProjectPermissionSchemeBean build() {
            return new ProjectPermissionSchemeBean(id, name, description, permissions, displayRendering, operationResult);
        }
    }
}
