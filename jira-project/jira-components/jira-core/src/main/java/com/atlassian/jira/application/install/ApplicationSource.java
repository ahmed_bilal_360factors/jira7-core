package com.atlassian.jira.application.install;

import javax.annotation.Nullable;
import java.io.File;
import java.util.Arrays;

import static org.apache.commons.lang3.ArrayUtils.isNotEmpty;

/**
 * Holds information about application installation source. It's a folder with all application plugins within.
 * sourceName is the name of folder and application files are files within that folder.
 *
 * @since v6.5
 */
class ApplicationSource {
    private final String applicationSourceName;

    private final File[] applicationBundles;

    ApplicationSource(final String applicationSourceName, final File[] applicationBundles) {
        this.applicationSourceName = applicationSourceName;
        this.applicationBundles = applicationBundles;

        // add file order predictability
        Arrays.sort(this.applicationBundles, (o1, o2) ->
                o1.getName().compareTo(o2.getName()));
    }

    public String getApplicationSourceName() {
        return applicationSourceName;
    }

    public File[] getApplicationBundles() {
        return applicationBundles;
    }

    @Nullable
    static ApplicationSource readFromDir(final File dir) {
        final File[] applicationFiles = dir.listFiles(ApplicationSource::jarsOnly);

        return isNotEmpty(applicationFiles) ?
                new ApplicationSource(dir.getName(), applicationFiles) :
                null;
    }

    static boolean jarsOnly(final File file) {
        return file.isFile() && file.getName().endsWith(".jar");
    }
}
