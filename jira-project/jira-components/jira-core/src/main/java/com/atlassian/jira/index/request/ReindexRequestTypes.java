package com.atlassian.jira.index.request;

import java.util.EnumSet;

public class ReindexRequestTypes {
    private static final EnumSet<ReindexRequestType> ALL_ALLOWED = EnumSet.allOf(ReindexRequestType.class);
    private static final EnumSet<ReindexRequestType> NONE_ALLOWED = EnumSet.noneOf(ReindexRequestType.class);
    private static final EnumSet<ReindexRequestType> ONLY_IMMEDIATE_ALLOWED = EnumSet.of(ReindexRequestType.IMMEDIATE);

    public static EnumSet<ReindexRequestType> allAllowed() {
        return ALL_ALLOWED;
    }

    public static EnumSet<ReindexRequestType> noneAllowed() {
        return NONE_ALLOWED;
    }

    public static EnumSet<ReindexRequestType> onlyImmediate() {
        return ONLY_IMMEDIATE_ALLOWED;
    }
}
