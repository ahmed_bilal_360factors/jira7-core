package com.atlassian.jira.application;

import com.atlassian.annotations.Internal;
import com.atlassian.application.api.ApplicationKey;
import com.atlassian.application.host.ApplicationConfigurationManager;
import com.atlassian.application.host.events.ApplicationDefinedEvent;
import com.atlassian.application.host.events.ApplicationUndefinedEvent;
import com.atlassian.cache.Cache;
import com.atlassian.cache.CacheLoader;
import com.atlassian.cache.CacheManager;
import com.atlassian.cache.CacheSettingsBuilder;
import com.atlassian.cache.CachedReference;
import com.atlassian.collectors.CollectorsUtil;
import com.atlassian.crowd.embedded.api.CrowdService;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.crowd.embedded.impl.IdentifierUtils;
import com.atlassian.crowd.embedded.spi.DirectoryDao;
import com.atlassian.crowd.event.DirectoryEvent;
import com.atlassian.crowd.event.application.ApplicationDirectoryOrderUpdatedEvent;
import com.atlassian.crowd.event.directory.DirectoryUpdatedEvent;
import com.atlassian.crowd.event.directory.RemoteDirectorySynchronisationFinishedEvent;
import com.atlassian.crowd.event.group.GroupCreatedEvent;
import com.atlassian.crowd.event.group.GroupDeletedEvent;
import com.atlassian.crowd.event.group.GroupMembershipDeletedEvent;
import com.atlassian.crowd.event.group.GroupMembershipsCreatedEvent;
import com.atlassian.crowd.event.group.GroupUpdatedEvent;
import com.atlassian.crowd.event.migration.XMLRestoreFinishedEvent;
import com.atlassian.crowd.event.user.AutoUserUpdatedEvent;
import com.atlassian.crowd.event.user.UserDeletedEvent;
import com.atlassian.crowd.event.user.UserEditedEvent;
import com.atlassian.crowd.event.user.UserUpdatedEvent;
import com.atlassian.crowd.manager.directory.SynchronisationStatusManager;
import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.fugue.Option;
import com.atlassian.jira.CachingComponent;
import com.atlassian.jira.EventComponent;
import com.atlassian.jira.auditing.AffectedApplication;
import com.atlassian.jira.auditing.AuditingCategory;
import com.atlassian.jira.auditing.AuditingService;
import com.atlassian.jira.auditing.ChangedValue;
import com.atlassian.jira.auditing.ChangedValueImpl;
import com.atlassian.jira.cluster.ClusterSafe;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.crowd.embedded.ofbiz.ExtendedUserDao;
import com.atlassian.jira.crowd.embedded.ofbiz.InternalMembershipDao;
import com.atlassian.jira.crowd.embedded.ofbiz.db.OfBizTransactionManager;
import com.atlassian.jira.event.ClearCacheEvent;
import com.atlassian.jira.event.ComponentManagerShutdownEvent;
import com.atlassian.jira.license.JiraLicenseManager;
import com.atlassian.jira.license.LicenseChangedEvent;
import com.atlassian.jira.license.LicenseCountService;
import com.atlassian.jira.license.LicenseDetails;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.tenancy.TenantAware;
import com.atlassian.jira.tenancy.TenantInfo;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.RecoveryMode;
import com.atlassian.jira.user.util.Users;
import com.google.common.base.Stopwatch;
import com.google.common.base.Supplier;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Multimap;
import com.google.common.collect.SetMultimap;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.stream.Collectors;

import static com.atlassian.collectors.CollectorsUtil.toImmutableSet;
import static com.atlassian.extras.common.LicensePropertiesConstants.UNLIMITED_USERS;
import static com.atlassian.jira.component.ComponentAccessor.getComponent;
import static com.atlassian.jira.user.util.Users.isAnonymous;
import static com.atlassian.jira.util.dbc.Assertions.notNull;
import static com.google.common.collect.Sets.newHashSet;
import static com.google.common.util.concurrent.Futures.immediateFuture;

/**
 * Default implementation of {@link com.atlassian.jira.application.ApplicationRoleManager}.
 *
 * @since 6.3
 */
@TenantInfo(TenantAware.TENANTED)
@EventComponent
public class DefaultApplicationRoleManager implements ApplicationRoleManager, CachingComponent,
        LicenseCountService, ApplicationConfigurationManager {
    private static final Logger log = LoggerFactory.getLogger(DefaultApplicationRoleManager.class);
    private static final String AUDIT_CATEGORY = AuditingCategory.APPLICATIONS.getId();
    private static final String ACTIVE_USERS_COUNT_CACHE_ASYNCHRONOUS_FEATURE_FLAG = "com.atlassian.jira.application.active.users.count.cache.asynchronous";

    private final ApplicationRoleDefinitions definitions;
    private final GroupManager groupManager;
    private final ApplicationRoleStore applicationRoleStore;
    private final JiraLicenseManager licenseManager;
    private final RecoveryMode recoveryMode;
    private final CrowdService crowdService;
    private final EventPublisher eventPublisher;

    private final InternalMembershipDao internalMembershipDao;
    private final DirectoryDao directoryDao;
    private final ExtendedUserDao ofBizUserDao;
    private final OfBizTransactionManager ofBizTransactionManager;

    private final SynchronisationStatusManager crowdSyncStatusManager;
    private final FeatureManager featureManager;

    @ClusterSafe
    @TenantInfo(TenantAware.TENANTED)
    private final Cache<ApplicationKey, Option<ApplicationRole>> appRoleCache;

    @ClusterSafe
    @TenantInfo(value = TenantAware.TENANTED, comment = "Maps application to the number of active users")
    private final Cache<ApplicationKey, Integer> activeUsersCountForLicense;

    private final ThreadPoolExecutor cacheRefreshExecutor;
    private volatile Option<Future<Void>> lastRefresh = Option.none();

    @TenantInfo(value = TenantAware.TENANTED, comment = "Contains the number of billable users")
    private final CachedReference<Integer> billableUsersCount;

    public DefaultApplicationRoleManager(@Nonnull CacheManager cacheManager, @Nonnull ApplicationRoleStore store,
                                         @Nonnull ApplicationRoleDefinitions definitions, @Nonnull GroupManager groupManager,
                                         @Nonnull JiraLicenseManager licenseManager,
                                         @Nonnull RecoveryMode recoveryMode, @Nonnull CrowdService crowdService,
                                         @Nonnull EventPublisher eventPublisher,
                                         @Nonnull InternalMembershipDao internalMembershipDao,
                                         @Nonnull DirectoryDao directoryDao, @Nonnull ExtendedUserDao ofBizUserDao,
                                         @Nonnull OfBizTransactionManager ofBizTransactionManager,
                                         @Nonnull SynchronisationStatusManager crowdSyncStatusManager,
                                         @Nonnull FeatureManager featureManager) {
        notNull("cacheManager", cacheManager);

        this.recoveryMode = notNull("recoveryMode", recoveryMode);
        this.groupManager = notNull("groupManager", groupManager);
        this.applicationRoleStore = notNull("store", store);
        this.definitions = notNull("definitions", definitions);
        this.licenseManager = notNull("licenseManager", licenseManager);
        this.crowdService = notNull("crowdService", crowdService);
        this.eventPublisher = notNull("eventPublisher", eventPublisher);
        this.internalMembershipDao = notNull("internalMembershipDao", internalMembershipDao);
        this.directoryDao = notNull("directoryDao", directoryDao);
        this.ofBizUserDao = notNull("ofBizUserDao", ofBizUserDao);
        this.ofBizTransactionManager = notNull("ofBizTransactionManager", ofBizTransactionManager);
        this.crowdSyncStatusManager = notNull("crowdSyncStatusManager", crowdSyncStatusManager);
        this.cacheRefreshExecutor = new ThreadPoolExecutor(
                1,
                1,
                0L,
                TimeUnit.MILLISECONDS,
                new LinkedBlockingQueue<>(2), // We need a two-element queue to avoid race
                // conditions in rejectedExceptionHandler. It prevents a situation when we are
                // rejected and our refresh request won't be processed and we don't have a future in
                // the queue to delegate to. If we're rejected from the two-elements queue and get
                // null from the queue.peek() it means we don't need to wait anymore. We don't have
                // the same guarantee from a one-element queue
                new ThreadFactoryBuilder()
                        .setNameFormat("default-application-role-manager-cache-refresh-%d")
                        .build(),
                (currentRunnable, executor) -> {
                    log.debug("Queue already full. Skipping cache refresh");
                    final DelegatingSettableFutureTask currentFutureTask = (DelegatingSettableFutureTask) currentRunnable;
                    final Future refreshRunnableFromQueue = (Future) executor.getQueue().peek();
                    currentFutureTask.setDelegate(refreshRunnableFromQueue != null ?
                            refreshRunnableFromQueue :
                            immediateFuture(null));
                }
        );

        this.appRoleCache = cacheManager.getCache(
                DefaultApplicationRoleManager.class.getName() + ".applicationRoleGroups",
                new RoleLoader(),
                new CacheSettingsBuilder().expireAfterAccess(30, TimeUnit.MINUTES).maxEntries(100).build());

        this.activeUsersCountForLicense = cacheManager.getCache(
                "com.atlassian.jira.application.DefaultApplicationRoleManager.activeUsersCountForLicenseUnflushableCache",
                key -> getRole(key)
                        .map(this::countApplicationUsers)
                        .getOrElse(0),
                new CacheSettingsBuilder()
                        .remote()
                        .replicateViaCopy() // this flag has no effect on the cache,
                                            // it's here to be consistent
                                            // with the ehcache.xml config.
                        .unflushable()
                        .build());

        this.billableUsersCount = cacheManager.getCachedReference(
                getClass().getName() + ".billableUsersCount", new BillableUserCountLoader(),
                new CacheSettingsBuilder().expireAfterWrite(2, TimeUnit.HOURS).build());

        licenseManager.subscribeToClearCache(Void -> clearCache());
        this.featureManager = featureManager;
    }

    private void synchronizeActiveUsersCountCache() {
        final Future<Void> future = lastRefresh.getOrNull();
        if (future != null) {
            final int timeout = 60;
            try {
                final Stopwatch stopwatch = Stopwatch.createStarted();
                future.get(timeout, TimeUnit.SECONDS);
                log.debug("Waiting for active users count cache to be refreshed took {}", stopwatch);
            } catch (final InterruptedException | ExecutionException e) {
                throw new RuntimeException("Exception occurred while waiting for queue synchronization", e);
            } catch (final TimeoutException e) {
                throw new RuntimeException("ActiveUsersCountForLicenseCache refresh took over " + timeout + " seconds." +
                        " This operation takes too long and it may affect JIRA performance and stability. " +
                        "Please see https://confluence.atlassian.com/display/JIRAKB/KB-JRASERVER-64384-docs for details.", e);
            }

        }
    }

    private boolean shouldSynchronizeActiveUsersCountCache() {
        return !featureManager.isEnabled(ACTIVE_USERS_COUNT_CACHE_ASYNCHRONOUS_FEATURE_FLAG);
    }

    private void refreshActiveUsersCount() {
        log.debug("Request for refresh active users count cache.");
        final DelegatingSettableFutureTask future = new DelegatingSettableFutureTask(this::tryRefreshActiveUsersCountForLicense);
        lastRefresh = Option.some(future);
        final Future headOfQueueFuture = (Future) cacheRefreshExecutor.getQueue().peek();
        if (headOfQueueFuture != null) {
            // This is an optimization: if there already is a future in queue that has not yet started executing
            // we can just delegate our future to that future.
            future.setDelegate(headOfQueueFuture);
        } else {
            cacheRefreshExecutor.execute(future);
        }
    }

    static class DelegatingSettableFutureTask extends FutureTask<Void> {

        private volatile Future futureTask;

        DelegatingSettableFutureTask(final Runnable runnable) {
            super(runnable, null);
        }

        public void setDelegate(final Future futureTask) {
            this.futureTask = futureTask;
            set(null); // this will notify any threads that are waiting on call to super.get()
        }

        @Override
        public Void get() throws InterruptedException, ExecutionException {
            throw new UnsupportedOperationException();
        }

        @Override
        public Void get(final long timeout, final TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
            super.get(timeout, unit);
            if(futureTask != null)
                futureTask.get(timeout, unit);
            return null;
        }
    }

    private void tryRefreshActiveUsersCountForLicense() {
        try {
            refreshActiveUsersCountForLicense();
        } catch (Exception e) {
            log.error("Error occurred while trying to refresh active users count for license", e);
        }
    }

    private void refreshActiveUsersCountForLicense() {
        final Collection<ApplicationKey> applicationKeys = licenseManager.getAllLicensedApplicationKeys();
        final List<ApplicationKey> allNotCoreApplicationKeysToRefresh = applicationKeys.stream()
                .filter(applicationKey -> !ApplicationKeys.CORE.equals(applicationKey))
                .collect(CollectorsUtil.toImmutableListWithSizeOf(applicationKeys));

        allNotCoreApplicationKeysToRefresh.forEach(applicationKey -> {
            final Option<ApplicationRole> applicationRoleOption = getRole(applicationKey);
            if (applicationRoleOption.isDefined()) {
                final ApplicationRole applicationRole = applicationRoleOption.get();
                final int count = countApplicationRoleUsers(applicationRole);
                activeUsersCountForLicense.put(applicationKey, count);
            } else {
                activeUsersCountForLicense.put(applicationKey, 0);
                log.debug("Can't find application role for applicationKey '{}'", applicationKey);
            }
        });

        // we need user counts for all other applications to calculate count for core users.
        if (getRole(ApplicationKeys.CORE).isDefined()) {
            final int coreUsersCount = countCoreUsers();
            activeUsersCountForLicense.put(ApplicationKeys.CORE, coreUsersCount);
        } else {
            log.debug("Can't find application role. Skipping active CORE users cache update.");
        }
    }

    @Nonnull
    @Override
    public Option<ApplicationRole> getRole(@Nonnull final ApplicationKey role) {
        notNull("role", role);

        //We know this can't be null because we use options to represent null.
        //noinspection ConstantConditions
        return appRoleCache.get(role);
    }

    @Override
    @Nonnull
    public Set<ApplicationRole> getRoles() {
        final Set<ApplicationRole> roles = newHashSet();
        for (ApplicationKey appKey : licenseManager.getAllLicensedApplicationKeys()) {
            Iterables.addAll(roles, getRole(appKey));
        }
        return Collections.unmodifiableSet(roles);
    }

    @Nonnull
    @Override
    @Deprecated
    public Set<ApplicationRole> getDefaultRoles() {
        return getRoles().stream().filter(ApplicationRole::isSelectedByDefault).collect(toImmutableSet());
    }

    @Nonnull
    @Override
    public Set<ApplicationKey> getDefaultApplicationKeys() {
        //noinspection deprecation
        return getDefaultRoles().stream().map(ApplicationRole::getKey).collect(toImmutableSet());
    }

    @Override
    public boolean hasAnyRole(@Nullable ApplicationUser user) {
        if (Users.isAnonymous(user)) {
            return false;
        }

        return getRoles().stream().anyMatch(role -> userHasRole(user, role)) || recoveryMode.isRecoveryUser(user);
    }

    @Override
    public boolean userHasRole(@Nullable ApplicationUser user, ApplicationKey key) {
        notNull("key", key);

        // Anon. users are in no groups
        if (isAnonymous(user)) {
            return false;
        }

        Set<Group> groups = getRole(key).map(ApplicationRole::getGroups).getOrElse(ImmutableSet.of());
        for (Group appGroup : groups) {
            if (groupManager.isUserInGroup(user, appGroup)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean userOccupiesRole(@Nullable ApplicationUser user, ApplicationKey key) {
        return getOccupiedLicenseRolesForUser(user).stream().anyMatch(r -> r.getKey().equals(key));
    }

    @Override
    public boolean isAnyRoleLimitExceeded() {
        final Set<ApplicationRole> roles = this.getRoles();
        if (roles.isEmpty()) {
            // then no roles
            return true;
        }

        // check that active user count <= #seats granted in license for role
        for (ApplicationRole role : roles) {
            if (isRoleLimitExceeded(role.getKey())) {
                return true;
            }
        }

        return false;
    }

    @Override
    public boolean isRoleLimitExceeded(@Nonnull final ApplicationKey applicationKey) {
        if (shouldSynchronizeActiveUsersCountCache()) {
            synchronizeActiveUsersCountCache();
        }
        return isRoleLimitExceededCurrentValue(applicationKey);
    }

    private boolean isRoleLimitExceededCurrentValue(@Nonnull final ApplicationKey applicationKey) {
        int numGrantedSeats = getNumberOfSeatsGrantedToRole(applicationKey);
        if (numGrantedSeats == UNLIMITED_USERS) {
            // then role is licensed for unlimited seats
            return false;
        }

        // then #users for role exceeds #licensed seats
        return getUserCountCurrentValue(applicationKey) > numGrantedSeats;
    }

    @Override
    public boolean hasExceededAllRoles(@Nonnull ApplicationUser user) {
        if (shouldSynchronizeActiveUsersCountCache()) {
            synchronizeActiveUsersCountCache();
        }
        return hasExceededAllRolesCurrentValue(user);
    }

    private boolean hasExceededAllRolesCurrentValue(@Nonnull ApplicationUser user) {
        final Set<ApplicationRole> rolesForUser = getRolesForUser(notNull(user));

        // it will return true if rolesForUser is empty
        return rolesForUser.stream().allMatch(role -> isRoleLimitExceededCurrentValue(role.getKey()));
    }

    @Override
    public Set<ApplicationRole> getRolesForUser(@Nonnull ApplicationUser user) {
        notNull(user);
        return getRoles().stream().filter(role -> userHasRole(user, role)).collect(toImmutableSet());
    }

    @Override
    public Set<ApplicationRole> getOccupiedLicenseRolesForUser(@Nonnull ApplicationUser user) {
        notNull(user);

        final Set<ApplicationRole> roles = getRolesForUser(user);
        if (roles.size() > 1 && roles.stream().anyMatch(r -> ApplicationKeys.CORE.equals(r.getKey()))) {
            boolean allNonCoreRolesExceeded = roles.stream().filter(r -> !ApplicationKeys.CORE.equals(r.getKey())).allMatch(r -> isRoleLimitExceeded(r.getKey()));
            if (!allNonCoreRolesExceeded) {
                return roles.stream().filter(r -> !ApplicationKeys.CORE.equals(r.getKey())).collect(CollectorsUtil.toImmutableSet());
            }
        }

        return roles;
    }

    @Override
    public Set<ApplicationRole> getRolesForGroup(@Nonnull Group group) {
        notNull("group", group);
        Set<ApplicationRole> roles = newHashSet();

        for (ApplicationRole role : getRoles()) {
            for (Group roleGroup : role.getGroups()) {
                // note that the isGroupMemberOfGroup lookup as of present does not cross crowd directory boundaries
                if (roleGroup.equals(group) || crowdService.isGroupMemberOfGroup(group, roleGroup)) {
                    roles.add(role);
                    break;
                }
            }
        }

        return Collections.unmodifiableSet(roles);
    }

    @Override
    @Nonnull
    public Set<Group> getGroupsForLicensedRoles() {
        return getRoles().stream()
                .map(ApplicationRole::getGroups)
                .flatMap(Collection::stream)
                .collect(toImmutableSet());
    }

    @Override
    public void removeGroupFromRoles(@Nonnull Group group) {
        notNull("group", group);

        logRemovedGroup(group);

        applicationRoleStore.removeGroup(group.getName());
        clearCache();
    }

    private void logRemovedGroup(final @Nonnull Group group) {
        final AuditingService auditingService = getComponent(AuditingService.class);

        final Set<ApplicationRole> roles = getRoles();
        for (ApplicationRole role : roles) {
            final Set<Group> groups = role.getGroups();

            if (groups.contains(group)) {
                if (auditingService != null) {
                    // We could use the ApplicationRoleDiff here,
                    // but it turned out to involve lots of unnecessary data manipulation.
                    final Iterable<ChangedValue> associated = ImmutableList.of(new ChangedValueImpl(
                            group.getName(),
                            "Associated",
                            "Group deleted"));
                    auditingService.storeRecord(
                            AUDIT_CATEGORY, "Group was deleted", new AffectedApplication(role), associated, null, null);
                }

            }
        }
    }

    @Override
    public boolean isRoleInstalledAndLicensed(@Nonnull final ApplicationKey key) {
        notNull("key", key);

        return definitions.isDefined(key) && definitions.isLicensed(key);
    }

    @Nonnull
    @Override
    public ApplicationRole setRole(@Nonnull final ApplicationRole role) {
        validateRole(notNull("role", role));

        logDiffForRole(role);

        final ApplicationKey key = role.getKey();
        try {
            applicationRoleStore.save(new ApplicationRoleStore.ApplicationRoleData(key, groupNames(role.getGroups()),
                    groupNames(role.getDefaultGroups()), role.isSelectedByDefault()));
        } finally {
            clearCacheLine(key);
        }

        //noinspection ConstantConditions,RedundantCast
        return appRoleCache.get(key).getOrElse((Supplier<IdApplicationRole>) (() -> IdApplicationRole.empty(key)));
    }

    @Override
    public void clearConfiguration(final ApplicationKey key) {
        final AuditingService auditingService = getComponent(AuditingService.class);
        if (auditingService != null) {
            final Option<ApplicationRole> oldRole = definitions.getDefined(key).map(ard -> applicationRoleDefinitionToApplicationRole(key, ard));

            if (oldRole.isDefined()) {
                final ApplicationRole role = oldRole.get();
                auditingService.storeRecord(
                        AUDIT_CATEGORY,
                        "Application configuration cleared.",
                        new AffectedApplication(role),
                        ApplicationRoleDiff.diffRemoved(role).messages(),
                        null, null);
            }
        }

        try {
            applicationRoleStore.removeByKey(key);
        } finally {
            clearCacheLine(key);
            eventPublisher.publish(new ApplicationConfigurationEvent(ImmutableSet.of(key)));
        }
    }

    @Override
    public int getUserCount(@Nonnull ApplicationKey key) {
        notNull("key", key);
        if (shouldSynchronizeActiveUsersCountCache()) {
            synchronizeActiveUsersCountCache();
        }

        //noinspection ConstantConditions
        return getUserCountCurrentValue(key);
    }

    /**
     * This will return 0 if the role does not exist.
     */
    private int getUserCountCurrentValue(@Nonnull final ApplicationKey key) {
        final int userCount = getRole(key).fold(
                () -> 0,
                (role) -> activeUsersCountForLicense.get(key));
        log.trace("getUserCountCurrentValue({}) = {}", key, userCount);
        return userCount;
    }

    @Override
    public int getRemainingSeats(@Nonnull final ApplicationKey key) {
        if (shouldSynchronizeActiveUsersCountCache()) {
            synchronizeActiveUsersCountCache();
        }
        return getRemainingSeatsCurrentValue(key);
    }

    private int getRemainingSeatsCurrentValue(@Nonnull final ApplicationKey key) {
        return getRole(notNull("key", key)).map(applicationRole ->
        {
            final int numberOfSeats = applicationRole.getNumberOfSeats();
            if (numberOfSeats == UNLIMITED_USERS) {
                return UNLIMITED_USERS;
            }
            final int activeUsers = getUserCountCurrentValue(key);
            return Math.max(numberOfSeats - activeUsers, 0);
        }).getOrElse(0);
    }

    @Override
    public boolean hasSeatsAvailable(@Nonnull final ApplicationKey key, final int seatCount) {
        if (shouldSynchronizeActiveUsersCountCache()) {
            synchronizeActiveUsersCountCache();
        }
        return hasSeatsAvailableCurrentValue(key, seatCount);
    }

    private boolean hasSeatsAvailableCurrentValue(@Nonnull final ApplicationKey key, final int seatCount) {
        notNull("key", key);

        if (seatCount < 0) {
            throw new IllegalArgumentException("seatCount < 0");
        }

        final int remainingSeats = getRemainingSeatsCurrentValue(key);
        return remainingSeats == UNLIMITED_USERS || seatCount <= remainingSeats;
    }

    @Nonnull
    @Override
    public Set<Group> getDefaultGroups(@Nonnull final ApplicationKey key) {
        notNull("key", key);

        return getRole(key).map(ApplicationRole::getDefaultGroups).getOrElse(ImmutableSet.of());
    }

    @Override
    public boolean rolesEnabled() {
        return true;
    }

    @EventListener
    public void onClearCache(ClearCacheEvent event) {
        clearCaches(event);

        // Fire a Crowd Embedded XMLRestoreFinishedEvent to clear Crowd caches.
        eventPublisher.publish(new XMLRestoreFinishedEvent(this));
    }

    @EventListener
    public void onApplicationDefined(ApplicationDefinedEvent event) {
        // HIROL-1288: clear cache when application is installed
        clearCaches(event);
    }

    @EventListener
    public void onApplicationUndefined(ApplicationUndefinedEvent event) {
        // HIROL-1288: clear cache when application is uninstalled
        clearCaches(event);
    }

    @EventListener
    public void onLicenseChanged(LicenseChangedEvent event) {
        //Applications might be created or removed.
        clearCaches(event);
    }

    @EventListener
    public void onGroupCreated(GroupCreatedEvent createdEvent) {
        //New group could activate old groups. Need to clear the cache.
        if (shouldRepopulateCache(createdEvent)) {
            clearCaches(createdEvent);
        }
    }

    @EventListener
    public void onGroupDeleted(GroupDeletedEvent deletedEvent) {
        //Group may need to be removed from cache.
        if (shouldRepopulateCache(deletedEvent)) {
            clearCaches(deletedEvent);
        }
    }

    @EventListener
    public void onGroupUpdated(GroupUpdatedEvent updatedEvent) {
        //Group may have changed in a way that moves it out of the cache.
        if (shouldRepopulateCache(updatedEvent)) {
            clearCaches(updatedEvent);
        }
    }

    @EventListener
    public void onDirectoryReorder(final ApplicationDirectoryOrderUpdatedEvent event) {
        //- This can cause shadowed users to become active which could change membership.
        //- This can cause the name of the group to change case.
        clearCaches(event);
    }

    @EventListener
    public void onDirectoryUpdated(final DirectoryUpdatedEvent event) {
        //This directory can be disabled. This can change the users and groups.
        clearCaches(event);
    }

    @EventListener
    public void onGroupMembershipsCreated(final GroupMembershipsCreatedEvent e) {
        //Does not affect the configuration but might affect the count. This event is fired when:
        // - User is created (if that user is also added to groups)
        // - User is added to a group via the Crowd API or sync.
        clearUserCountsIfNotInSync(e);
    }

    @EventListener
    public void onGroupMembershipDeleted(final GroupMembershipDeletedEvent e) {
        //Does not affect the configuration but might affect the count. This event is fired when:
        // - User is removed from a group via the Crowd API or sync.
        // Importantly, this event may or may not be fired when deleting a user (it depends on the directory
        // being used). See HIROL-856
        clearUserCountsIfNotInSync(e);
    }

    @EventListener
    public void onUserDeleted(final UserDeletedEvent e) {
        //Does not affect the configuration but might affect the count. When users are deleted through crowd sync this
        //*can* sometimes be the only event fired (i.e. there is no GroupMembershipDeletedEvent fired to indicate
        // the user is leaving groups). See HIROL-856.
        clearUserCountsIfNotInSync(e);
    }

    @EventListener
    public void onUserUpdated(final UserEditedEvent event) {
        //When a user is activated or de-activated the count may change.
        clearCacheForUpdatedUser(event, event.getOriginalUser(), event.getUser());
    }

    @EventListener
    public void onUserUpdated(final AutoUserUpdatedEvent event) {
        //When a user is activated or de-activated the count may change.
        clearCacheForUpdatedUser(event, event.getOriginalUser(), event.getUser());
    }

    @EventListener
    public void onSyncFinished(final RemoteDirectorySynchronisationFinishedEvent e) {
        clearUserCounts(e);
    }

    @EventListener
    public void onComponentManagerShutdown(final ComponentManagerShutdownEvent shutdownEvent) {
        cacheRefreshExecutor.shutdown();
    }

    @Override
    @Internal
    public void clearCache() {
        appRoleCache.removeAll();
        refreshActiveUsersCount();
        flushBillableUsersCache();
    }

    private boolean shouldRepopulateCache(final DirectoryEvent event) {
        // When a crowd synchronization is in progress we avoid repopulating the cache
        // as licensed user counts are expensive to calculate. This should be done
        // for any event which may be triggered during a synchronisation which
        // affects the licensed user count. The idea is to minimise the amount of
        // work that happens during a synchronisation so they can complete as fast
        // as possible
        if (!featureManager.isEnabled("com.atlassian.jira.ldap.nonblocking.sync.disabled") &&
                crowdSyncStatusManager.getDirectorySynchronisationInformation(event.getDirectory()).isSynchronising()) {
            log.debug("Directory (id={}) synchronization is in progress. Delaying repopulating license cache.", event.getDirectory().getId());
            return false;
        }
        return true;
    }

    @Override
    public int totalBillableUsers() {
        return billableUsersCount.get();
    }

    @Override
    public void flush() {
        flushBillableUsersCache();
    }

    @Override
    public void flushBillableUsersCache() {
        billableUsersCount.reset();
    }

    /**
     * Returns the total number of seats granted to the given {@link com.atlassian.application.api.ApplicationKey} by
     * the {@link JiraLicenseManager#getLicenses() current set of installed and valid licenses}.
     * <p>
     * The number of seats returned will either be: (1) a positive, finite number of seats, (2) zero, indicating the
     * given role is not granted by any license, or (3) {@link com.atlassian.extras.common.LicensePropertiesConstants#UNLIMITED_USERS}.
     * <p>
     * Note that this method does NOT exclude licenses that are {@link com.atlassian.jira.license.LicenseDetails#isExpired()
     * expired}.
     *
     * @return granted user count by application role.
     * @throws java.lang.IllegalStateException if more than 1 license grants seats to the given role.
     * @see com.atlassian.extras.common.LicensePropertiesConstants#UNLIMITED_USERS
     * @see JiraLicenseManager#getLicenses()
     * @since 7.0
     */
    private int getNumberOfSeatsGrantedToRole(@Nonnull ApplicationKey roleKey) {
        int numSeats = 0;

        // loop through all licenses (ie: don't exit early on finding the first license that grants
        // the role) as a sanity check that the given role only has seats granted in a single license.
        //
        // This is a cheap check as licenses are cached and retrieved all at once, and the parsing of
        // the application roles within is cached per license.
        for (final LicenseDetails license : licenseManager.getLicenses()) {
            final int num = license.getLicensedApplications().getUserLimit(roleKey);
            if (num != 0) {
                if (numSeats != 0) {
                    throw new IllegalStateException("Application role has users granted in > 1 license: " + roleKey);
                }

                numSeats = num;
            }
        }

        return numSeats;
    }

    private void validateRole(ApplicationRole role) {
        if (!licenseManager.getAllLicensedApplicationKeys().contains(role.getKey())) {
            throw new IllegalArgumentException(
                    String.format("The '%s' role is not provided by any license.", role.getKey()));
        }
        for (Group group : role.getGroups()) {
            if (!groupManager.groupExists(group)) {
                throw new IllegalArgumentException(
                        String.format("The '%s' role is associated with group '%s', which does not exist.",
                                role.getKey(), group.getName()));
            }
        }
    }

    private boolean userHasRole(@Nonnull ApplicationUser user, @Nonnull ApplicationRole role) {
        return role.getGroups().stream().anyMatch(group -> groupManager.isUserInGroup(user, group));
    }

    private void clearCaches(@Nullable Object event) {
        final String eventName = event != null ? event.getClass().getName() : "<UNKNOWN>";
        log.debug("Clearing the cache on {}.", eventName);
        clearCache();
    }

    private void clearUserCountsIfNotInSync(final DirectoryEvent event) {
        if (shouldRepopulateCache(event)) {
            clearUserCounts(event);
        }
    }

    private void clearUserCounts(@Nullable Object event) {
        final String eventName = event != null ? event.getClass().getName() : "<UNKNOWN>";
        log.debug("Clearing the user counts on {}.", eventName);
        refreshActiveUsersCount();
        flushBillableUsersCache();
    }

    private void clearCacheForUpdatedUser(@Nullable UserUpdatedEvent event, User originalUser, @Nullable User updatedUser) {
        if (shouldRepopulateCache(event) && (updatedUser == null || originalUser.isActive() != updatedUser.isActive())) {
            clearUserCounts(event);
        }
    }

    private void clearCacheLine(final ApplicationKey key) {
        log.debug("Clearing the cache line {}.", key);

        //When users are in a group that has been associated with the platform application and another application
        //the user would count towards the other application and not platform application. Only when the group is
        //exclusively associated to platform should those users count towards platform. Because of this well always clear
        //platform when another application has changes.
        //Make sure platform cache always gets cleared and only clears once per request (expensive call)
        if (!ApplicationKeys.CORE.equals(key)) {
            appRoleCache.remove(ApplicationKeys.CORE);
        }
        appRoleCache.remove(key);
        refreshActiveUsersCount();
        flushBillableUsersCache();
    }

    private static Iterable<String> groupNames(Collection<Group> groups) {
        return groups.stream().map(Group::getName).collect(toImmutableSet());
    }

    private class RoleLoader implements CacheLoader<ApplicationKey, Option<ApplicationRole>> {
        @Nonnull
        @Override
        public Option<ApplicationRole> load(@Nonnull final ApplicationKey key) {
            return definitions.getLicensed(key).map(appDef ->
            {
                final ApplicationRoleStore.ApplicationRoleData applicationRoleData = applicationRoleStore.get(key);
                final Set<Group> realGroups = applicationRoleData.getGroups().stream()
                        .map(groupManager::getGroup)
                        .filter(Objects::nonNull)
                        .collect(toImmutableSet());
                final Set<Group> defaultGroups = applicationRoleData.getDefaultGroups().stream()
                        .map(groupManager::getGroup)
                        .filter(Objects::nonNull)
                        .filter(realGroups::contains)
                        .collect(toImmutableSet());
                return new DefinitionApplicationRole(definitions, appDef, realGroups, defaultGroups,
                        getNumberOfSeatsGrantedToRole(key), applicationRoleData.isSelectedByDefault());
            });
        }
    }

    private ApplicationRole applicationRoleDefinitionToApplicationRole(final @Nonnull ApplicationKey key,
                                                                       final @NotNull ApplicationRoleDefinitions.ApplicationRoleDefinition appDef) {
        final ApplicationRoleStore.ApplicationRoleData applicationRoleData = applicationRoleStore.get(key);
        final Set<Group> realGroups = applicationRoleData.getGroups().stream()
                .map(groupManager::getGroup)
                .filter(Objects::nonNull)
                .collect(toImmutableSet());
        final Set<Group> defaultGroups = applicationRoleData.getDefaultGroups().stream()
                .map(groupManager::getGroup)
                .filter(Objects::nonNull)
                .filter(realGroups::contains)
                .collect(toImmutableSet());
        return new DefinitionApplicationRole(definitions, appDef, realGroups, defaultGroups, getNumberOfSeatsGrantedToRole(key),
                applicationRoleData.isSelectedByDefault());
    }

    private int countApplicationUsers(ApplicationRole role) {
        Stopwatch timer = Stopwatch.createStarted();

        int count;
        if (ApplicationKeys.CORE.equals(role.getKey())) {
            count = countCoreUsers();
        } else {
            count = countApplicationRoleUsers(role);
        }

        log.debug("{} count: {}", role.getName(), count);
        log.debug("Count application users: {} {}", timer, role.getName());
        return count;
    }

    /**
     * Counts number of active users for an application role.  Does not exclude core users that do not take a
     * license seat.
     *
     * @param role the application role.
     * @return number of active users.
     */
    private int countApplicationRoleUsers(final ApplicationRole role) {
        Set<String> groupNames = role.getGroups().stream().map(Group::getName).collect(Collectors.toSet());
        return countUsersInGroups(groupNames);
    }

    /**
     * Counts the number of core users.  Users are counted if they take a core seat only; they are not counted if
     * they use both a core seat and another non-core application license seat.
     *
     * @throws IllegalStateException if CORE role is not available
     * @return number of users using a core seat.
     */
    private int countCoreUsers() {
        final Set<String> nonCoreGroupNames = getRoles().stream()
                .filter(appRole -> !appRole.getKey().equals(ApplicationKeys.CORE) && !isRoleLimitExceededCurrentValue(appRole.getKey()))
                .flatMap(appRole -> appRole.getGroups().stream())
                .map(Group::getName)
                .collect(Collectors.toSet());

        final Set<String> usersInNonCoreGroupsThatAreAlreadyCounted = readUsersInGroups(nonCoreGroupNames);
        final ApplicationRole coreRole = getRole(ApplicationKeys.CORE)
                .getOrThrow(() -> new IllegalStateException("Can't fetch role for core application. CORE role is not available"));
        final Set<String> coreGroupNames = coreRole.getGroups().stream()
                .map(Group::getName)
                .collect(Collectors.toSet());

        return countUsersInGroupsExcluding(coreGroupNames, usersInNonCoreGroupsThatAreAlreadyCounted);
    }

    /**
     * Expands groups, resolving nesting.  Groups are expanded per directory.
     *
     * @param groups the names of the groups to expand.
     * @return a multimap mapping directory IDs to collections of expanded group names.  Includes the original groups if
     * they exist.
     */
    private Multimap<Long, String> readExpandedGroupNamesByDirectory(Set<String> groups) {
        SetMultimap<Long, String> groupMap = HashMultimap.create();

        for (Directory directory : directoryDao.findAll()) {
            if (directory.isActive()) {
                long directoryId = directory.getId();
                Set<String> groupsForCurrentDirectory = groupMap.get(directoryId);
                groupsForCurrentDirectory.addAll(groups);
                readExpandedGroupNamesForDirectory(directoryId, groupsForCurrentDirectory);
            }
        }

        return groupMap;
    }

    /**
     * For a particular directory, expands groups by resolving nesting.
     *
     * @param directoryId the ID of the directory.
     * @param groups      a set of groups that is expanded.  All these groups are read, and the additional expanded groups
     *                    are added to this set.
     */
    private void readExpandedGroupNamesForDirectory(long directoryId, Set<String> groups) {
        boolean more;

        Collection<String> children = groups;

        //This call is not recursive, so need to loop this call until number of groups no longer changes
        do {
            children = internalMembershipDao.findGroupChildrenOfGroups(directoryId, children);
            more = groups.addAll(children);
        }
        while (more);
    }

    /**
     * Reads users per-directory for a multimap of expanded groups.
     *
     * @param groupsForDirectories a multimap mapping directory ID to expanded group names.
     * @return a multimap mapping directory ID to user ID defining users that belong to the groups in each directory.
     */
    private Multimap<Long, String> readUsersInGroupsByDirectory(Multimap<Long, String> groupsForDirectories) {
        SetMultimap<Long, String> usersByDirectory = HashMultimap.create();
        for (Map.Entry<Long, Collection<String>> directoryToGroupsEntry : groupsForDirectories.asMap().entrySet()) {
            long directoryId = directoryToGroupsEntry.getKey();
            Collection<String> expandedGroups = directoryToGroupsEntry.getValue();
            Collection<String> userNames = internalMembershipDao.findUserChildrenOfGroups(directoryId, expandedGroups);
            usersByDirectory.putAll(directoryId, userNames.stream().map(IdentifierUtils::toLowerCase).collect(Collectors.toSet()));
        }
        return usersByDirectory;
    }

    /**
     * Reads the IDs of the users in all the specified groups.  Takes into account directory shadowing and nested groups.
     *
     * @param groupNames the names of groups.
     * @return a set of user IDs.
     */
    private Set<String> readUsersInGroups(Set<String> groupNames) {
        return readUsersInGroupsExcluding(groupNames, Collections.emptySet());
    }

    /**
     * Reads the IDs of the users in the specified groups, not including a set of user IDs.  Takes into account directory
     * shadowing and nested groups.
     *
     * @param groupNames     the names of groups.
     * @param excludeUserIds a set of user IDs to not include in the results.
     * @return the result user IDs.
     */
    private Set<String> readUsersInGroupsExcluding(Set<String> groupNames, Set<String> excludeUserIds) {
        final Set<String> allUsers = new HashSet<>();

        final Stopwatch readExpandedGroupNamesByDirectoryTimer = Stopwatch.createStarted();

        //Keys are directory IDs
        final Multimap<Long, String> expandedGroupNamesByDirectory = readExpandedGroupNamesByDirectory(groupNames);
        final Multimap<Long, String> usersInGroupsByDirectory = readUsersInGroupsByDirectory(expandedGroupNamesByDirectory);

        log.debug("Time taken for readExpandedGroupNamesByDirectory: {}", readExpandedGroupNamesByDirectoryTimer);
        log.debug("Expanded group names by directory: " + expandedGroupNamesByDirectory.size());

        final Stopwatch processUsersTimer = Stopwatch.createStarted();
        ofBizUserDao.processUsers((User user) ->
        {
            if (!user.isActive()) {
                return;
            }

            final String usernameLowerCase = IdentifierUtils.toLowerCase(user.getName());
            //Compare by directory - groups can be different in each directory, therefore the users that map to these
            //groups can also be different
            if (!usersInGroupsByDirectory.get(user.getDirectoryId()).contains(usernameLowerCase)) {
                return;
            }

            if (!excludeUserIds.contains(usernameLowerCase)) {
                allUsers.add(usernameLowerCase);
            }
        });
        log.debug("Time taken for processing users {}", processUsersTimer);

        return allUsers;
    }

    private int countUsersInGroups(Set<String> groupNames) {
        return readUsersInGroups(groupNames).size();
    }

    private int countUsersInGroupsExcluding(Set<String> groupNames, Set<String> excludeUserIds) {
        return readUsersInGroupsExcluding(groupNames, excludeUserIds).size();
    }

    /**
     * Supplies the count of billable users that are: 1) active, 2) not sysadmins (only relevant in cloud), and 3) not
     * Connect users.
     */
    private final class BillableUserCountLoader implements com.atlassian.cache.Supplier<Integer> {
        @Override
        public Integer get() {
            Stopwatch timer = Stopwatch.createStarted();

            final Set<Group> groupsThatGrantUserAccess = getGroupsThatGrantUserAccess();
            final Set<String> groupNames = groupsThatGrantUserAccess.stream()
                    .map(Group::getName)
                    .collect(Collectors.toSet());

            log.debug("Time taken for reading base group names: {}", timer);

            int count = countUsersInGroups(groupNames);

            log.debug("License check time: {}", timer);

            return count;
        }

        private Set<Group> getGroupsThatGrantUserAccess() {
            return getGroupsForLicensedRoles();
        }
    }

    private GlobalPermissionManager getGlobalPermissionManager() {
        return getComponent(GlobalPermissionManager.class);
    }

    private void logDiffForRole(@Nonnull final ApplicationRole newRole) {
        final Option<ApplicationRole> oldRole = getRole(newRole.getKey());

        final ApplicationRoleDiff diff = ApplicationRoleDiff.diff(oldRole, newRole);

        storeRecord(newRole, diff);
    }

    private void storeRecord(final @Nonnull ApplicationRole newRole, @Nonnull final ApplicationRoleDiff diff) {
        // there's a circular dependency if we inject this. See https://jdog.jira-dev.com/browse/HIROL-1134
        final AuditingService auditingService = getComponent(AuditingService.class);
        if (auditingService != null && !diff.isEmpty()) {
            auditingService.storeRecord(AUDIT_CATEGORY, diff.summary(), new AffectedApplication(newRole), diff.messages(), null, null);
        }
    }
}
