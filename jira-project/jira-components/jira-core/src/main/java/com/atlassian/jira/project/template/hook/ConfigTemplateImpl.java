package com.atlassian.jira.project.template.hook;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static com.google.common.collect.Sets.newHashSet;
import static java.util.Collections.unmodifiableList;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ConfigTemplateImpl implements ConfigTemplate {
    private final Optional<WorkflowSchemeTemplate> workflowSchemeTemplate;
    private final Optional<IssueTypeSchemeTemplate> issueTypeSchemeTemplate;
    private final Optional<IssueTypeScreenSchemeTemplate> issueTypeScreenSchemeTemplate;
    private final List<? extends ResolutionTemplate> resolutionTemplates;

    public ConfigTemplateImpl(
            @JsonProperty("workflow-scheme") WorkflowSchemeTemplateImpl workflowSchemeTemplate,
            @JsonProperty("issue-type-scheme") IssueTypeSchemeTemplateImpl issueTypeSchemeTemplate,
            @JsonProperty("issue-type-screen-scheme") IssueTypeScreenSchemeTemplateImpl issueTypeScreenSchemeTemplate,
            @JsonProperty("resolutions") List<ResolutionTemplateImpl> resolutionTemplates) {
        this.workflowSchemeTemplate = Optional.ofNullable(workflowSchemeTemplate);
        this.issueTypeSchemeTemplate = Optional.ofNullable(issueTypeSchemeTemplate);
        this.issueTypeScreenSchemeTemplate = Optional.ofNullable(issueTypeScreenSchemeTemplate);
        this.resolutionTemplates = (resolutionTemplates == null) ? Collections.<ResolutionTemplate>emptyList() : resolutionTemplates;

        validate();
    }

    @Override
    @Nonnull
    public Optional<WorkflowSchemeTemplate> workflowSchemeTemplate() {
        return workflowSchemeTemplate;
    }

    @Override
    @Nonnull
    public Optional<IssueTypeSchemeTemplate> issueTypeSchemeTemplate() {
        return issueTypeSchemeTemplate;
    }

    @Override
    @Nonnull
    public Optional<IssueTypeScreenSchemeTemplate> issueTypeScreenSchemeTemplate() {
        return issueTypeScreenSchemeTemplate;
    }

    @Override
    @Nonnull
    public Collection<ResolutionTemplate> resolutionTemplates() {
        return unmodifiableList(resolutionTemplates);
    }

    private void validate() {
        if (issueTypeSchemeTemplate.isPresent()) {
            if (workflowSchemeTemplate.isPresent()) {
                checkTemplateWithIssueTypeSchemeAndWorkflowScheme();
            } else {
                checkTemplateWithIssueTypeSchemeButNoWorkflowScheme();
            }

            if (issueTypeScreenSchemeTemplate.isPresent()) {
                checkTemplateWithIssueTypeSchemeAndIssueTypeScreenScheme();
            } else {
                checkTemplateWithIssueTypeSchemeButNoIssueTypeScreenScheme();
            }
        }
    }

    private void checkTemplateWithIssueTypeSchemeButNoWorkflowScheme() {
        // check whether the issue types don't have workflows defined
        for (IssueTypeTemplate issueTypeTemplate : issueTypeSchemeTemplate.get().issueTypeTemplates()) {
            if (issueTypeTemplate.workflow().isPresent()) {
                throw new IllegalArgumentException("Issue Type '" + issueTypeTemplate.key() +
                        "' has a workflow configured, but no workflow scheme has been configured in the configuration scheme");
            }
        }
    }

    private void checkTemplateWithIssueTypeSchemeAndWorkflowScheme() {
        Set<String> workflowKeys = newHashSet();
        for (IssueTypeTemplate issueTypeTemplate : issueTypeSchemeTemplate.get().issueTypeTemplates()) {
            if (issueTypeTemplate.workflow().isPresent()) {
                if (!workflowSchemeTemplate.get().hasWorkflow(issueTypeTemplate.workflow().get())) {
                    throw new IllegalArgumentException("Issue Type '" + issueTypeTemplate.key() +
                            "' refers to an unknown workflow key " + issueTypeTemplate.workflow().get());
                }
                workflowKeys.add(issueTypeTemplate.workflow().get());
            }
        }
        if (workflowSchemeTemplate.get().defaultWorkflow().isPresent()) {
            workflowKeys.add(workflowSchemeTemplate.get().defaultWorkflow().get());
        }
        checkAllDefinedWorkflowsAreUsed(workflowKeys);
    }

    private void checkAllDefinedWorkflowsAreUsed(Set<String> workflowKeys) {
        for (WorkflowTemplate workflowTemplate : workflowSchemeTemplate.get().workflowTemplates()) {
            if (!workflowKeys.contains(workflowTemplate.key())) {
                throw new IllegalArgumentException("Workflow with key '" + workflowTemplate.key() + "' is defined but not used.");
            }
        }
    }

    private void checkTemplateWithIssueTypeSchemeAndIssueTypeScreenScheme() {
        Set<String> usedScreenSchemeKeys = newHashSet();
        for (IssueTypeTemplate issueTypeTemplate : issueTypeSchemeTemplate.get().issueTypeTemplates()) {
            if (issueTypeTemplate.screenScheme().isPresent()) {
                if (!issueTypeScreenSchemeTemplate.get().hasScreenScheme(issueTypeTemplate.screenScheme().get())) {
                    throw new IllegalArgumentException("Issue Type '" + issueTypeTemplate.key() +
                            "' refers to an unknown screen scheme key " + issueTypeTemplate.screenScheme().get());
                }
                usedScreenSchemeKeys.add(issueTypeTemplate.screenScheme().get());
            }
        }
        usedScreenSchemeKeys.add(issueTypeScreenSchemeTemplate.get().defaultScreenScheme());
        checkAllDefinedScreenSchemesAreUsed(usedScreenSchemeKeys);
    }

    private void checkAllDefinedScreenSchemesAreUsed(Set<String> usedScreenSchemeKeys) {
        for (ScreenSchemeTemplate screenSchemeTemplate : issueTypeScreenSchemeTemplate.get().screenSchemeTemplates()) {
            if (!usedScreenSchemeKeys.contains(screenSchemeTemplate.key())) {
                throw new IllegalArgumentException("Screen scheme with key '" + screenSchemeTemplate.key() + "' is defined but not used.");
            }
        }
    }

    private void checkTemplateWithIssueTypeSchemeButNoIssueTypeScreenScheme() {
        for (IssueTypeTemplate issueTypeTemplate : issueTypeSchemeTemplate.get().issueTypeTemplates()) {
            if (issueTypeTemplate.screenScheme().isPresent()) {
                throw new IllegalArgumentException("Issue Type '" + issueTypeTemplate.key() +
                        "' has a screen scheme configured, but no issue type screen scheme has been configured in the configuration scheme");
            }
        }
    }
}
