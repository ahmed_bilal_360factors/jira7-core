package com.atlassian.jira.upgrade;

import com.atlassian.beehive.ClusterLock;
import com.atlassian.beehive.ClusterLockService;
import com.atlassian.jira.index.request.ReindexRequestType;
import com.atlassian.upgrade.api.UpgradeContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Set;

public class ClusterLockingUpgradeService {
    private static final Logger LOGGER = LoggerFactory.getLogger(ClusterLockingUpgradeService.class);

    private static final String UPGRADE_FRAMEWORK_LOCKED_ERROR = "Could not acquire the lock for running upgrades";

    private final ClusterLockService clusterLockService;
    private final LicenseCheckingUpgradeService licenseCheckingUpgradeService;

    public ClusterLockingUpgradeService(
            final ClusterLockService clusterLockService,
            final LicenseCheckingUpgradeService licenseCheckingUpgradeService
    ) {
        this.clusterLockService = clusterLockService;
        this.licenseCheckingUpgradeService = licenseCheckingUpgradeService;
    }

    public UpgradeResult runUpgrades(
            final Set<ReindexRequestType> reindexRequestTypes,
            final UpgradeContext upgradeContext
    ) {
        final ClusterLock lock = getClusterLock();
        if (lock.tryLock()) {
            try {
                return licenseCheckingUpgradeService.runUpgrades(reindexRequestTypes, upgradeContext);
            } finally {
                lock.unlock();
            }
        } else {
            LOGGER.error("The Upgrade Service has been locked as there is another attempt to run upgrades.");
            return new UpgradeResult(UPGRADE_FRAMEWORK_LOCKED_ERROR);
        }
    }

    private ClusterLock getClusterLock() {
        return clusterLockService.getLockForName(UpgradeService.class.getName());
    }

    public boolean areUpgradesRunning() {
        final ClusterLock lock = getClusterLock();
        if (lock.tryLock()) {
            lock.unlock();
            return false;
        }
        return true;
    }
}
