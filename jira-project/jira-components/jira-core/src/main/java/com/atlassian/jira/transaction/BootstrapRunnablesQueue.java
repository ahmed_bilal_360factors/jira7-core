package com.atlassian.jira.transaction;

/**
 * Queue for JIRA setup container phase. It is not allowed in here to offer anything.
 * Offering a runnables for this queue only happens, if there is a issue change transaction.
 */
public class BootstrapRunnablesQueue implements RunnablesQueue {
    @Override
    public void offer(Runnable runnable) {
        throw new UnsupportedOperationException("Offering runnables to this queue during bootstrap or setup Container phase is not supported");
    }

    @Override
    public void clear() {
        //do nothing
    }

    @Override
    public void runAndClear() {
        //do nothing
    }
}
