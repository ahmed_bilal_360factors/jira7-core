package com.atlassian.jira.license;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.cache.CacheManager;
import com.atlassian.cache.CachedReference;
import com.atlassian.collectors.CollectorsUtil;
import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.fugue.Option;
import com.atlassian.jira.CachingComponent;
import com.atlassian.jira.EventComponent;
import com.atlassian.jira.application.ApplicationConfigurationHelper;
import com.atlassian.jira.auditing.AuditingManager;
import com.atlassian.jira.auditing.handlers.SystemAuditEventHandler;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.event.ClearCacheEvent;
import com.atlassian.license.SIDManager;
import com.google.common.base.Supplier;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSortedSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.apache.commons.lang.StringUtils;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.atlassian.jira.component.ComponentAccessor.getComponent;
import static com.atlassian.jira.util.dbc.Assertions.containsNoNulls;
import static com.atlassian.jira.util.dbc.Assertions.notBlank;
import static com.atlassian.jira.util.dbc.Assertions.notNull;
import static com.google.common.collect.Iterables.transform;

/**
 * @since v4.0
 */
@EventComponent
public class JiraLicenseManagerImpl implements JiraLicenseManager, CachingComponent {
    private final SIDManager sidManager;
    private final EventPublisher eventPublisher;
    private final LicenseDetailsFactory licenseDetailsFactory;
    private final ApplicationProperties applicationProperties;
    private final MultiLicenseStore multiLicenseStore;
    private final CachedReference<CachedLicenses> cache;
    private final CopyOnWriteArrayList<Consumer<Void>> clearCacheConsumers;

    public JiraLicenseManagerImpl(SIDManager sidManager,
                                  EventPublisher eventPublisher,
                                  MultiLicenseStore multiLicenseStore,
                                  LicenseDetailsFactory licenseDetailsFactory,
                                  ApplicationProperties applicationProperties,
                                  CacheManager cacheManager) {
        this.eventPublisher = notNull("eventPublisher", eventPublisher);
        this.licenseDetailsFactory = notNull("licenseDetailsFactory", licenseDetailsFactory);
        this.multiLicenseStore = notNull("multiLicenseStore", multiLicenseStore);
        this.sidManager = notNull("sidManager", sidManager);
        this.applicationProperties = notNull("applicationProperties", applicationProperties);
        this.cache = cacheManager.getCachedReference(JiraLicenseManager.class, "License.cache", this::loadLicenses);
        clearCacheConsumers = Lists.newCopyOnWriteArrayList();
    }

    @Override
    public String getServerId() {
        String serverId = multiLicenseStore.retrieveServerId();
        if (StringUtils.isBlank(serverId)) {
            serverId = sidManager.generateSID();
            multiLicenseStore.storeServerId(serverId);
        }
        return serverId;
    }

    @Nonnull
    @Override
    public LicenseDetails getLicense(@Nonnull String licenseString) {
        notNull("licenseString", licenseString);

        //noinspection RedundantCast
        return cache.get().lookupByLicenseString(licenseString)
                .getOrElse((Supplier<LicenseDetails>) () -> licenseDetailsFactory.getLicense(licenseString));
    }

    @Override
    @Nonnull
    public List<LicenseDetails> getLicenses() {
        return cache.get().licenses();
    }

    @Override
    @Nonnull
    public Set<ApplicationKey> getAllLicensedApplicationKeys() {
        return cache.get().licensedKeys();
    }

    @Override
    public SortedSet<String> getSupportEntitlementNumbers() {
        return cache.get().sens();
    }

    @Override
    public Option<LicenseDetails> getLicense(@Nonnull final ApplicationKey application) {
        return cache.get().lookupLicenseByKey(notNull("application", application));
    }

    @Override
    public boolean isLicensed(@Nonnull final ApplicationKey application) {
        return cache.get().isLicensed(notNull("application", application));
    }

    @Override
    public boolean isDecodeable(String licenseString) {
        notBlank("licenseString", licenseString);

        return this.licenseDetailsFactory.isDecodeable(licenseString);
    }

    @Override
    public LicenseDetails setLicense(String licenseString) {
        notBlank("licenseString", licenseString);

        return addLicense(licenseString, true);
    }

    @Override
    public LicenseDetails setLicenseNoEvent(String licenseString) {
        notBlank("licenseString", licenseString);

        return addLicense(licenseString, false);
    }

    @Override
    public void removeLicense(@Nonnull ApplicationKey application) {
        notNull("application", application);
        final List<LicenseDetails> licensesToKeep = Lists.newArrayList();
        final List<LicenseDetails> licenseDetailsToBeRemoved = Lists.newArrayList();

        for (LicenseDetails licenseDetails : getLicenses()) {
            if (licenseDetails.hasApplication(application)) {
                licenseDetailsToBeRemoved.add(licenseDetails);
            } else {
                licensesToKeep.add(licenseDetails);
            }
        }
        updateStoreLicenses(licensesToKeep, licenseDetailsToBeRemoved);
    }

    @Override
    public void removeLicenses(@Nonnull Iterable<? extends LicenseDetails> licenses) {
        containsNoNulls("licenses", licenses);

        Set<LicenseDetails> licensesToKeep = Sets.newHashSet(getLicenses());
        Set<LicenseDetails> licensesToRemove = Sets.newHashSet(licenses);
        licensesToRemove.retainAll(licensesToKeep);
        licensesToKeep.removeAll(licensesToRemove);
        updateStoreLicenses(licensesToKeep, licensesToRemove);
    }

    private void updateStoreLicenses(Collection<LicenseDetails> licensesToKeep, Collection<LicenseDetails> licenseDetailsToBeRemoved) {
        if (licensesToKeep.isEmpty()) {
            throw new IllegalStateException("Unable to remove license, JIRA needs to contain at least one license to be functional");
        } else if (!licenseDetailsToBeRemoved.isEmpty()) {
            final List<String> licenseStringsToKeep = licensesToKeep.stream()
                    .map(LicenseDetails::getLicenseString).collect(Collectors.toList());
            store(licenseStringsToKeep);
            resetOldBuildConfirmationConsideringGracePeriod();
            for (LicenseDetails removed : licenseDetailsToBeRemoved) {
                publishLicenseChangedEvent(new LicenseChangedEvent(Option.option(removed), Option.none()));
            }
        }
    }

    private void publishLicenseChangedEvent(final LicenseChangedEvent event) {
        getComponent(ApplicationConfigurationHelper.class).configureLicense(event);
        eventPublisher.publish(event);
    }

    @Override
    public boolean isLicenseSet() {
        return !cache.get().licenses().isEmpty();
    }

    @Override
    public void clearAndSetLicense(String licenseString) {
        notBlank("licenseString", licenseString);

        final Option<LicenseDetails> previousAssociatedLicense;
        final List<LicenseDetails> licenses = getLicenses();
        if (licenses.isEmpty()) {
            previousAssociatedLicense = Option.none();
        } else {
            // In Server there could be more than one license, but we only care about providing the event with one that
            // is being removed and shares a key with the new license to classify it as an update event.
            // If no current license shares a key with the new one, we send an empty option.
            previousAssociatedLicense = findExistingLicenseDetails(getLicense(licenseString));
        }

        LicenseDetails newLicenseDetails = clearAndSetLicenseNoEvent(licenseString);
        publishLicenseChangedEvent(new LicenseChangedEvent(previousAssociatedLicense, Option.option(newLicenseDetails)));
    }

    @Override
    public LicenseDetails clearAndSetLicenseNoEvent(String licenseString) {
        notBlank("licenseString", licenseString);

        verifyEncodable(licenseString);
        store(ImmutableList.of(licenseString));
        return getLicense(licenseString);
    }

    @EventListener
    public void onCacheClear(ClearCacheEvent clearEvent) {
        clearCache();
    }

    private void verifyEncodable(final String licenseString) {
        if (!isDecodeable(licenseString)) {
            throw new IllegalArgumentException("The licenseString is invalid and will not be stored.");
        }
    }

    private Option<LicenseDetails> findExistingLicenseDetails(@Nonnull final LicenseDetails licenseDetails) {
        for (ApplicationKey licensedApplication : licenseDetails.getLicensedApplications().getKeys()) {
            final Option<LicenseDetails> license = getLicense(licensedApplication);
            if (license.isDefined()) {
                return license;
            }
        }
        return Option.none();
    }

    private LicenseDetails addLicense(String licenseString, boolean fireEvent) {
        verifyEncodable(licenseString);

        final LicenseDetails licenseDetails = getLicense(licenseString);
        final LicenseChangedEvent event = new LicenseChangedEvent(findExistingLicenseDetails(licenseDetails), Option.option(licenseDetails));

        replaceLicense(licenseString);

        // if all licenses are within license terms, and the instance is in the grace evaluation state from
        // one or more previous licenses having fallen out of maintenance, then we need to reset the grace period
        // flag.
        resetOldBuildConfirmationWithoutConsideringGracePeriod();

        if (fireEvent) {
            publishLicenseChangedEvent(event);
        } else {
            // HIROL-86 Adding audit log of new licenses. We added this here because it was the only way to get the info
            // when the system was restore and the license was changed, cause PICO was not initialized at that point.
            // Please check JDEV-11469 for more info about PICO container not initialized.
            notNull("auditingManager", getComponent(AuditingManager.class)).store(
                    notNull("systemAuditEventHandler", getComponent(SystemAuditEventHandler.class)).onLicenseChangedEvent(event));
        }

        return licenseDetails;
    }

    private void resetOldBuildConfirmationConsideringGracePeriod() {
        resetOldBuildConfirmationIfNecessary(check -> check.evaluate().isPass());
    }

    private void resetOldBuildConfirmationWithoutConsideringGracePeriod() {
        resetOldBuildConfirmationIfNecessary(check -> check.evaluateWithoutGracePeriod().isPass());
    }

    private void resetOldBuildConfirmationIfNecessary(Function<BuildVersionLicenseCheck, Boolean> check) {
        if (hasLicenseTooOldForBuildConfirmationBeenDone()) {
            final BuildVersionLicenseCheck buildVersionLicenseCheck = getComponent(BuildVersionLicenseCheck.class);
            if (check.apply(buildVersionLicenseCheck)) {
                multiLicenseStore.resetOldBuildConfirmation();
            }
        }
    }

    /**
     * Removes all licenses that share a role with this license, and then stores this license.
     */
    private void replaceLicense(String licenseString) {
        LicenseDetails license = getLicense(licenseString);
        final Set<ApplicationKey> newLicensedApplications = license.getLicensedApplications().getKeys();
        final List<String> withoutNewRoles = getLicenses().stream()
                .filter(details -> Collections.disjoint(details.getLicensedApplications().getKeys(), newLicensedApplications))
                .map(LicenseDetails::getLicenseString)
                .collect(CollectorsUtil.toImmutableList());
        store(Iterables.concat(ImmutableList.of(licenseString), withoutNewRoles));
    }

    @Override
    public void confirmProceedUnderEvaluationTerms(String userName) {
        // note that there may be 0 expired licenses
        BuildVersionLicenseCheck buildVersionLicenseCheck = getComponent(BuildVersionLicenseCheck.class);
        LicenseCheck.Result result = buildVersionLicenseCheck.evaluateWithoutGracePeriod();
        List<LicenseDetails> expiredLicenses = result.getFailedLicenses();

        multiLicenseStore.confirmProceedUnderEvaluationTerms(userName);
        eventPublisher.publish(new ConfirmEvaluationLicenseEvent(userName, expiredLicenses));
    }

    @Override
    public boolean hasLicenseTooOldForBuildConfirmationBeenDone() {
        return applicationProperties.getOption(APKeys.JIRA_CONFIRMED_INSTALL_WITH_OLD_LICENSE);
    }

    @Override
    public void subscribeToClearCache(@Nonnull final Consumer<Void> consumer) {
        clearCacheConsumers.addIfAbsent(consumer);
    }

    @Override
    public void unSubscribeFromClearCache(@Nonnull final Consumer<Void> consumer) {
        clearCacheConsumers.remove(consumer);
    }

    @Override
    public void clearCache() {
        cache.reset();
        clearCacheConsumers.stream().forEach(consumer -> consumer.accept(null));
    }

    private void store(Iterable<String> licenseString) {
        multiLicenseStore.store(licenseString);
        clearCache();
    }

    private CachedLicenses loadLicenses() {
        return new CachedLicenses(transform(multiLicenseStore.retrieve(), licenseDetailsFactory::getLicense));
    }

    private static class CachedLicenses {
        private final ImmutableSortedSet<String> sens;
        private final ImmutableList<LicenseDetails> licenses;
        private final ImmutableMap<ApplicationKey, LicenseDetails> licensesByApplicationKey;
        private final ImmutableMap<String, LicenseDetails> licensesByLicenseString;

        private CachedLicenses(Iterable<? extends LicenseDetails> details) {
            ImmutableSortedSet.Builder<String> sens = ImmutableSortedSet.orderedBy(Comparator.<String>naturalOrder());
            ImmutableList.Builder<LicenseDetails> licenses = ImmutableList.builder();
            ImmutableMap.Builder<ApplicationKey, LicenseDetails> licensesByApplicationKey = ImmutableMap.builder();
            ImmutableMap.Builder<String, LicenseDetails> licensesByLicenseString = ImmutableMap.builder();

            for (LicenseDetails detail : details) {
                if (detail != null) {
                    licenses.add(detail);
                    licensesByLicenseString.put(detail.getLicenseString(), detail);
                    for (ApplicationKey applicationKey : detail.getLicensedApplications().getKeys()) {
                        licensesByApplicationKey.put(applicationKey, detail);
                    }
                    String sen = StringUtils.stripToNull(detail.getSupportEntitlementNumber());
                    if (sen != null) {
                        sens.add(sen);
                    }
                }
            }
            this.sens = sens.build();
            this.licenses = licenses.build();
            this.licensesByApplicationKey = licensesByApplicationKey.build();
            this.licensesByLicenseString = licensesByLicenseString.build();
        }

        private SortedSet<String> sens() {
            return sens;
        }

        private List<LicenseDetails> licenses() {
            return licenses;
        }

        private Set<ApplicationKey> licensedKeys() {
            return licensesByApplicationKey.keySet();
        }

        private Option<LicenseDetails> lookupLicenseByKey(ApplicationKey key) {
            return Option.option(licensesByApplicationKey.get(key));
        }

        private boolean isLicensed(ApplicationKey key) {
            return licensesByApplicationKey.containsKey(key);
        }

        private Option<LicenseDetails> lookupByLicenseString(String license) {
            return Option.option(licensesByLicenseString.get(license));
        }
    }
}
