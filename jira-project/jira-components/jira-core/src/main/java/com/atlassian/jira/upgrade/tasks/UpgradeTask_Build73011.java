package com.atlassian.jira.upgrade.tasks;

import com.atlassian.jira.database.QueryCallback;
import com.atlassian.jira.database.QueryDslAccessor;
import com.atlassian.jira.model.querydsl.QIssue;
import com.atlassian.jira.model.querydsl.QOSCurrentStep;
import com.atlassian.jira.model.querydsl.QOSHistoryStep;
import com.atlassian.jira.model.querydsl.QOSHistoryStepPrev;
import com.atlassian.jira.model.querydsl.QProject;
import com.atlassian.upgrade.api.UpgradeContext;
import com.atlassian.upgrade.spi.UpgradeTask;
import com.google.common.base.Preconditions;
import com.google.common.base.Stopwatch;
import com.opensymphony.workflow.spi.WorkflowEntry;
import com.querydsl.core.Tuple;
import com.querydsl.core.types.Expression;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.sql.SQLQuery;
import org.ofbiz.core.entity.DelegatorInterface;
import org.ofbiz.core.entity.GenericEntityException;
import org.ofbiz.core.entity.GenericHelper;
import org.ofbiz.core.entity.jdbc.DatabaseUtil;
import org.ofbiz.core.entity.model.ModelEntity;
import org.ofbiz.core.entity.model.ModelIndex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

import static com.atlassian.jira.model.querydsl.QOSCurrentStep.O_S_CURRENT_STEP;
import static com.atlassian.jira.model.querydsl.QOSHistoryStep.O_S_HISTORY_STEP;
import static com.atlassian.jira.model.querydsl.QOSHistoryStepPrev.O_S_HISTORY_STEP_PREV;
import static com.atlassian.jira.model.querydsl.QOSWorkflowEntry.O_S_WORKFLOW_ENTRY;
import static com.querydsl.sql.SQLExpressions.select;
import static java.util.Objects.nonNull;

/**
 * Restore OS_CURRENTSTEP entries for all issues that are in final state and currently have no OS_CURRENTSTEP entries.
 *
 * jiraissue.workflow_id <=> os_wfentry.id
 * os_wfentry.id <=> os_currentstep.entry_id
 * os_wfentry.id <=> os_historystep.entry_id
 *
 */
public class UpgradeTask_Build73011 implements UpgradeTask {
    private static final Logger log = LoggerFactory.getLogger(UpgradeTask_Build73011.class);

    private static final long MIN_ID_INITIAL = -1L;
    private static final long BATCH_SIZE = 500L;
    private static final long FAILED_ISSUES_REPORT_LIMIT = 1000L;

    private final QueryDslAccessor dbConnectionManager;
    private final DelegatorInterface delegator;

    public UpgradeTask_Build73011(final QueryDslAccessor dbConnectionManager, final DelegatorInterface delegator) {
        this.dbConnectionManager = dbConnectionManager;
        this.delegator = delegator;
    }

    @Override
    public int getBuildNumber() {
        return 73011;
    }

    @Override
    public String getShortDescription() {
        return "Make all issues active in workflow";
    }

    private DelegatorInterface getDelegator() {
        return delegator;
    }

    @Override
    public void runUpgrade(UpgradeContext upgradeContext) {
        if (upgradeContext.getTrigger() == UpgradeContext.UpgradeTrigger.PROVISIONING) {
            log.debug("Skipping upgrade task");
            return;
        }

        log.debug("Starting upgrade task, limit: {}", BATCH_SIZE);

        final Stopwatch stopwatchBatch = Stopwatch.createUnstarted();
        final Stopwatch overallStopwatch = Stopwatch.createStarted();

        final IndexWorker indexWorker = new IndexWorker();

        dropPreviouslyCreatedIndexIfAny(indexWorker);

        try {
            indexWorker.createIndex();

            List<Long> workflowIds = getWorkflowIdsForClosedIssues(MIN_ID_INITIAL, BATCH_SIZE);

            while (!workflowIds.isEmpty()) {
                stopwatchBatch.reset().start();
                restoreEntriesFromHistory(workflowIds);

                deleteRestoredEntriesFromHistory(workflowIds);
                deleteRestoredEntriesFromHistoryPrev(workflowIds);

                activateRestoredEntries(workflowIds);

                final Long minId = workflowIds.get(workflowIds.size() - 1);
                workflowIds = getWorkflowIdsForClosedIssues(minId, BATCH_SIZE);
                log.debug("Processing batch took {}", stopwatchBatch);
            }

            logIssuesNotFixed();
        } finally {
            indexWorker.dropIndex();
        }

        log.debug("[DONE] Upgrade task done in {}", overallStopwatch);
    }

    private void dropPreviouslyCreatedIndexIfAny(IndexWorker indexWorker) {
        try {
            log.debug("Dropping previously created index");
            indexWorker.dropIndex();
        } catch (Exception e) {
            log.debug("Dropping previously created index", e);
        }
    }

    private void activateRestoredEntries(List<Long> workflowIdsFinal) {
        final Stopwatch stopwatch = Stopwatch.createStarted();
        log.debug("Updating OS_WORKFLOW_ENTRY");
        final long updated = dbConnectionManager.executeQuery(dbConnection ->
                dbConnection.update(O_S_WORKFLOW_ENTRY)
                        .set(O_S_WORKFLOW_ENTRY.state, WorkflowEntry.ACTIVATED)
                        .where(O_S_WORKFLOW_ENTRY.id.in(
                                select(O_S_CURRENT_STEP.entryId).from(O_S_CURRENT_STEP).where(O_S_CURRENT_STEP.entryId.in(workflowIdsFinal))
                        ))
                        .execute());
        log.debug("[DONE] Updating OS_WORKFLOW_ENTRY, {}, took {}", updated, stopwatch);
    }

    private void deleteRestoredEntriesFromHistoryPrev(List<Long> workflowIdsFinal) {
        final Stopwatch stopwatch = Stopwatch.createStarted();
        log.debug("Delete from OS_HISTORYSTEP_PREV");
        final long deleted = dbConnectionManager.executeQuery(dbConnection ->
                        dbConnection
                                .delete(O_S_HISTORY_STEP_PREV)
                                .where(O_S_HISTORY_STEP_PREV.id.in(select(O_S_CURRENT_STEP.id).from(O_S_CURRENT_STEP).where(O_S_CURRENT_STEP.entryId.in(workflowIdsFinal))))
                                .execute());
        log.debug("[DONE] Delete from OS_HISTORYSTEP_PREV, {}, took {}", deleted, stopwatch);

    }

    private void deleteRestoredEntriesFromHistory(List<Long> workflowIdsFinal) {
        final Stopwatch stopwatch = Stopwatch.createStarted();
        log.debug("Delete from OS_HISTORYSTEP");
        final long deleted = dbConnectionManager.executeQuery(dbConnection ->
                        dbConnection
                                .delete(O_S_HISTORY_STEP)
                                .where(O_S_HISTORY_STEP.id.in(select(O_S_CURRENT_STEP.id).from(O_S_CURRENT_STEP).where(O_S_CURRENT_STEP.entryId.in(workflowIdsFinal))))
                                .execute());
        log.debug("[DONE] Delete from OS_HISTORYSTEP done, {}, took {}", deleted, stopwatch);
    }

    private void restoreEntriesFromHistory(final List<Long> workflowIdsFinal) {
        final Stopwatch stopwatch = Stopwatch.createStarted();
        log.debug("Inserting OS_CURRENTSTEP. No of processed entries: {}", workflowIdsFinal.size());
        //table aliases
        QOSHistoryStep historyStep = new QOSHistoryStep("hs");
        QOSHistoryStep hs2 = new QOSHistoryStep("hs2");
        QOSHistoryStepPrev prevStep = new QOSHistoryStepPrev("prevStep");

        //columns that will be inserted
        final Path[] currentStepColumns = {O_S_CURRENT_STEP.id,
                O_S_CURRENT_STEP.entryId,
                O_S_CURRENT_STEP.stepId,
                O_S_CURRENT_STEP.actionId,
                O_S_CURRENT_STEP.owner,
                O_S_CURRENT_STEP.startDate,
                O_S_CURRENT_STEP.dueDate,
                O_S_CURRENT_STEP.finishDate,
                O_S_CURRENT_STEP.status,
                O_S_CURRENT_STEP.caller};

        //columns that will be selected
        final Expression[] historyStepColumns = {historyStep.id,
                historyStep.entryId,
                historyStep.stepId,
                historyStep.actionId,
                historyStep.owner,
                historyStep.startDate,
                historyStep.dueDate,
                Expressions.nullExpression(),
                historyStep.status,
                historyStep.caller};

        Preconditions.checkState(currentStepColumns.length == historyStepColumns.length);

        final long inserted = dbConnectionManager.executeQuery(dbConnection ->
                dbConnection.insert(O_S_CURRENT_STEP).columns(currentStepColumns)
                        .select(
                                select(historyStepColumns)
                                        .from(historyStep)
                                        .where(
                                                historyStep.entryId.in(workflowIdsFinal)
                                                        //ensure that issue with specified workflow_id still has no os_currentstep (eq. in the mean time someone could migrate this issue to new workflow)
                                                        .and(
                                                                select(QIssue.ISSUE.id)
                                                                        .from(QIssue.ISSUE)
                                                                        .where(
                                                                                QIssue.ISSUE.workflowId.eq(historyStep.entryId)
                                                                                        .and(select(O_S_CURRENT_STEP.id).from(O_S_CURRENT_STEP).where(O_S_CURRENT_STEP.entryId.eq(QIssue.ISSUE.workflowId)).notExists())
                                                                        )
                                                                        .exists()
                                                        )
                                                        //take only last record from os_historystep - order by finish_date and ensure by querying os_historystep_prev that this is the last one (there is no record that has os_historystep_prev.previous_id pointing to current os_history)
                                                        .and(historyStep.id.eq(
                                                                select(hs2.id).from(hs2).where(
                                                                        historyStep.entryId.eq(hs2.entryId)
                                                                                .and(hs2.finishDate.isNotNull())
                                                                                //hs2 is last step according to os_historystep_prev
                                                                                .and(select(prevStep.id).from(prevStep).where(prevStep.previousId.eq(hs2.id)).notExists())
                                                                ).orderBy(hs2.finishDate.desc()).limit(1)
                                                        ))
                                                        //prevent situation when there is ambiguity between OS_HISTORYSTEP (sort os_historystep by finish date + selecting latest record by querying os_historystep_prev did not result in 1 record)
                                                        .and(select(hs2.finishDate.count()).from(hs2).where(
                                                                historyStep.entryId.eq(hs2.entryId)
                                                                        .and(hs2.finishDate.isNotNull())
                                                                        .and(select(prevStep.id).from(prevStep).where(prevStep.previousId.eq(hs2.id)).notExists())
                                                                ).groupBy(hs2.finishDate).orderBy(hs2.finishDate.desc()).limit(1).eq(1l)
                                                        )
                                        )
                        ).execute());

        log.debug("[DONE] Inserted OS_CURRENTSTEP. Inserted size: {}, took: {}", inserted, stopwatch);
    }

    private class IndexWorker {
        final DatabaseUtil dbUtil;
        final ModelEntity historyStepPrev;
        final ModelIndex historystepPrevIdx;

        private IndexWorker() {
            try {
                GenericHelper helper = getDelegator().getEntityHelper("OSHistoryStepPrev");
                dbUtil = new DatabaseUtil(helper.getHelperName());
                historyStepPrev = getDelegator().getModelEntity("OSHistoryStepPrev");
                historystepPrevIdx = new ModelIndex();
                historystepPrevIdx.setName("os_hsp_prev_id_idx161125");
                historystepPrevIdx.setMainEntity(historyStepPrev);
                historystepPrevIdx.setUnique(false);
                historystepPrevIdx.addIndexField("previousId");
            } catch (GenericEntityException e) {
                throw new RuntimeException("Failed to create IndexWorker", e);
            }
        }

        private void createIndex() {
            final Stopwatch stopwatch = Stopwatch.createStarted();
            log.debug("Index creation start");
            final String error = dbUtil.createDeclaredIndex(historyStepPrev, historystepPrevIdx);
            if (error != null) {
                throw new RuntimeException("Could not create index: " + error);
            }
            log.debug("Index creation took: {}", stopwatch);
        }

        private void dropIndex() {
            final Stopwatch stopwatch = Stopwatch.createStarted();
            log.debug("Index drop start");
            final String error = dbUtil.deleteDeclaredIndex(historyStepPrev, historystepPrevIdx);
            if (error != null) {
                throw new RuntimeException("Could not drop index: " + error);
            }
            log.debug("Index drop took: {}", stopwatch);
        }
    }

    private void logIssuesNotFixed() {
        List<Tuple> notFixed = getInvalidEntries(FAILED_ISSUES_REPORT_LIMIT);
        if (notFixed.size() > 0) {
            log.error("Not fixed issues (limit to {}), [issueId, projectId, workflowId, projectKey, issueNumber]: {}, {}", FAILED_ISSUES_REPORT_LIMIT, notFixed.size(), notFixed);
        } else {
            log.debug("All issues fixed");
        }
    }

    private List<Long> getWorkflowIdsForClosedIssues(Long minId, Long limit) {
        final Stopwatch stopwatch = Stopwatch.createStarted();
        log.debug("Fetching workflowIds, limit: {}, minId: {}", limit, minId);
        QueryCallback<List<Long>> callback = dbConnection -> {
            SQLQuery<Long> workflowIdsForClosedIssuesQuery = dbConnection.newSqlQuery()
                    .select(QIssue.ISSUE.workflowId)
                    .from(QIssue.ISSUE)
                    .leftJoin(O_S_CURRENT_STEP)
                    .on(O_S_CURRENT_STEP.entryId.eq(QIssue.ISSUE.workflowId))
                    .where(O_S_CURRENT_STEP.id.isNull().and(QIssue.ISSUE.workflowId.gt(minId)))
                    .orderBy(QIssue.ISSUE.workflowId.asc());
            if (nonNull(limit)) {
                workflowIdsForClosedIssuesQuery.limit(limit);
            }

            return workflowIdsForClosedIssuesQuery.fetch();
        };
        final List<Long> workflowIdsForClosedIssues = dbConnectionManager.executeQuery(callback);

        log.debug("Fetched workflowIds, size: {}, ids: {}, took: {}", workflowIdsForClosedIssues.size(), workflowIdsForClosedIssues, stopwatch);

        return workflowIdsForClosedIssues;
    }

    private List<Tuple> getInvalidEntries(Long limit) {
        final Stopwatch stopwatch = Stopwatch.createStarted();
        final QIssue issue = new QIssue("i");
        final QOSCurrentStep cs = new QOSCurrentStep("cs");
        final QProject proj = new QProject("p");

        log.debug("Fetching invalid entries, limit: {}", limit);
        final List<Tuple> invalidEntries = dbConnectionManager.executeQuery(dbConnection -> {
            SQLQuery<Tuple> invalidEntriesQuery = dbConnection.newSqlQuery()
                    .select(issue.id, issue.project, issue.workflowId, proj.key, issue.number)
                    .from(issue)
                    .leftJoin(cs)
                    .on(cs.entryId.eq(issue.workflowId))
                    .leftJoin(proj).on(issue.project.eq(proj.id))
                    .where(cs.id.isNull())
                    .orderBy(issue.id.asc());
            if (nonNull(limit)) {
                invalidEntriesQuery.limit(limit);
            }

            return invalidEntriesQuery.fetch();
        });

        log.debug("Fetched invalid entries, size: {}, took: {}", invalidEntries.size(), stopwatch);

        return invalidEntries;
    }

}
