package com.atlassian.jira.license;

import com.atlassian.jira.util.johnson.JohnsonProvider;
import com.atlassian.johnson.JohnsonEventContainer;
import com.atlassian.johnson.event.Event;
import com.atlassian.johnson.event.EventLevel;
import com.atlassian.johnson.event.EventType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;

import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * An implementation of {@link LicenseJohnsonEventRaiser}
 *
 * @since v4.0
 */
public class LicenseJohnsonEventRaiserImpl implements LicenseJohnsonEventRaiser {
    private static final Logger log = LoggerFactory.getLogger(LicenseJohnsonEventRaiserImpl.class);

    private final BuildVersionLicenseCheck buildVersionLicenseCheck;
    private final JohnsonProvider johnsonProvider;

    public LicenseJohnsonEventRaiserImpl(@Nonnull BuildVersionLicenseCheck buildVersionLicenseCheck,
                                         JohnsonProvider johnsonProvider) {
        this.buildVersionLicenseCheck = notNull("buildVersionLicenseCheck", buildVersionLicenseCheck);
        this.johnsonProvider = notNull("johnsonProvider", johnsonProvider);
    }

    @Override
    public boolean checkLicenseIsTooOldForBuild() {
        LicenseCheck.Result result = buildVersionLicenseCheck.evaluate();
        if (!result.isPass()) {
            // then user needs to update their license(s) or confirm the installation under
            // the Evaluation Terms (Note: the user can always fall back to their previous release of JIRA)
            String failureMessage = result.getFailureMessage();
            log.error(failureMessage);

            JohnsonEventContainer cont = johnsonProvider.getContainer();

            for (LicenseDetails license : result.getFailedLicenses()) {
                String eventString = license.isEnterpriseLicenseAgreement() ? SUBSCRIPTION_EXPIRED : LICENSE_TOO_OLD;
                Event newEvent = new Event(EventType.get(eventString), failureMessage, EventLevel.get(EventLevel.ERROR));
                cont.addEvent(newEvent);
            }

            return true;
        } else {
            // licenses are valid for current build or user is in grace (evaluation) period
            return false;
        }
    }
}
