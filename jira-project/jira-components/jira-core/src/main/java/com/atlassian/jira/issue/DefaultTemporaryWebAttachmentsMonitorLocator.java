package com.atlassian.jira.issue;

import com.atlassian.fugue.Option;
import com.atlassian.jira.issue.attachment.TemporaryWebAttachmentsMonitorLocator;
import com.atlassian.jira.web.action.issue.TemporaryWebAttachmentsMonitor;

/**
 * Default implementation that used to use the http session for storage but no longer does.  The existence of this class should be questioned now
 *
 * @since v6.4
 */
public class DefaultTemporaryWebAttachmentsMonitorLocator implements TemporaryWebAttachmentsMonitorLocator {
    private final TemporaryWebAttachmentsMonitor webAttachmentsMonitor;

    public DefaultTemporaryWebAttachmentsMonitorLocator(final TemporaryWebAttachmentsMonitor temporaryWebAttachmentsMonitor) {
        webAttachmentsMonitor = temporaryWebAttachmentsMonitor;
    }

    @Override
    public Option<TemporaryWebAttachmentsMonitor> get() {
        return Option.some(webAttachmentsMonitor);
    }

    @Override
    public TemporaryWebAttachmentsMonitor getOrCreate() {
        return webAttachmentsMonitor;
    }
}
