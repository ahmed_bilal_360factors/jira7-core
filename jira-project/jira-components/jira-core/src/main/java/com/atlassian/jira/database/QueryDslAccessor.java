package com.atlassian.jira.database;

import javax.annotation.Nonnull;
import java.sql.Connection;

/**
 * Provides access to database connections from the Database Connection Pool with QueryDsl support built in.
 *
 * @since 7.0.6
 */
public interface QueryDslAccessor {
    /**
     * Executes SQL statements as defined in the callback function and returns the results.
     * <p>
     * This method is mostly useful for running SELECT statements and returning the given results in some form.
     * <p>
     * This method will attempt to run the callback within an existing OfBiz transaction if one is running within
     * this thread.
     * If not, it will borrow a new connection, start a transaction and manage that transaction for you.
     * That is, the connection will always be in autocommit=false, and the commit or rollback will be completely managed
     * for you.
     * <p>
     * <strong>Important: </strong> This method will NOT put the new connection into the OfBiz ThreadLocal transaction
     * if there wasn't one there already. See {@link #withLegacyOfBizTransaction()} instead.
     * <p>
     * If you want to rollback the transaction then throw a RuntimeException. The transaction will be marked as
     * "rollback required" such that the code that started the transaction will eventually call rollback on the
     * Connection and the exception will be propagated.
     * If the callback returns normally, then this indicates that the underlying transaction is allowed to be
     * committed at the appropriate time.
     * <p>
     * Because the connection (borrowing and returning from the pool) and its transaction are managed for you,
     * then specific {@link Connection} methods are illegal to call and will cause a RuntimeException including:
     * <ul>
     * <li>{@link Connection#setAutoCommit(boolean)} autocommit is always false</li>
     * <li>{@link Connection#commit()}</li> commit will occur in the broader transaction context if no errors are encountered
     * <li>{@link Connection#rollback()}</li> rollback will occur if a RuntimeException is thrown from the callback function
     * <li>{@link Connection#close()}</li> the connection will be returned to the pool for you at the appropriate time
     * </ul>
     * <p>
     * Example Usage:
     * <pre>
     *    WorklogDTO worklogDTO = queryDslAccessor.executeQuery(
     *            dbConnection -> dbConnection.newSqlQuery()
     *                    .from(WORKLOG)
     *                    .where(WORKLOG.id.eq(worklog.getId()))
     *                    .singleResult(WORKLOG));
     * </pre>
     *
     * @param callback the callback function that runs the query
     * @param <T>      type of results
     * @return results of the callback function
     * @see #execute(SqlCallback)
     * @see #withNewConnection()
     * @since 7.0.6
     */
    <T> T executeQuery(@Nonnull QueryCallback<T> callback);

    /**
     * Executes SQL statements as defined in the callback function.
     * <p>
     * This method does not return results and is mostly useful for running INSERT, UPDATE, and DELETE operations.
     * <p>
     * This method will attempt to run the callback within an existing OfBiz transaction if one is running within
     * this thread.
     * If not, it will borrow a new connection, start a transaction and manage that transaction for you.
     * That is, the connection will always be in autocommit=false, and the commit or rollback will be completely managed
     * for you.
     * If you want to rollback the transaction then throw a RuntimeException. The transaction will be marked as
     * "rollback required" such that the code that started the transaction will eventually call rollback on the
     * Connection and the exception will be propagated.
     * If the callback returns normally, then this indicates that the underlying transaction is allowed to be
     * committed at the appropriate time.
     * <p>
     * Because the connection (borrowing and returning from the pool) and its transaction are managed for you,
     * then specific {@link Connection} methods are illegal to call and will cause a RuntimeException including:
     * <ul>
     * <li>{@link Connection#setAutoCommit(boolean)} autocommit is always false</li>
     * <li>{@link Connection#commit()}</li> commit will occur in the broader transaction context if no errors are encountered
     * <li>{@link Connection#rollback()}</li> rollback will occur if a RuntimeException is thrown from the callback function
     * <li>{@link Connection#close()}</li> the connection will be returned to the pool for you at the appropriate time
     * </ul>
     * <p>
     * Example Usage:
     * <pre>
     *     queryDslAccessor.execute(dbConnection -> {
     *             dbConnection.update(QIssueLink.ISSUE_LINK)
     *                     .set(QIssueLink.ISSUE_LINK.sequence, newSeq)
     *                     .where(QIssueLink.ISSUE_LINK.id.eq(issueLinkId))
     *                     .execute();
     *     });
     * </pre>
     *
     * @param callback the callback function that runs the query
     * @see #executeQuery(QueryCallback)
     * @see #withNewConnection()
     * @since 7.0.6
     */
    void execute(@Nonnull SqlCallback callback);

    /**
     * Get a connection provider that will always borrow a new DB connection from the connection pool.
     * <p>
     * <strong>Important: </strong>
     * This method will borrow a new connection from the pool, pass it to the callback function and then return it to
     * the pool after the callback has completed.
     * Even if OfBiz is currently running in a ThreadLocal transaction, this will retrieve a fresh connection from
     * the pool.
     * If you want to run in an existing OfBiz transaction then see instead {@link #executeQuery(QueryCallback)} or
     * {@link #execute(SqlCallback)}
     * <p>
     * The connection will have the default auto-commit value as defined by the JIRA connection pool.
     * As at JIRA 7.0 this means autocommit == true.
     * (See {@link org.apache.commons.dbcp2.PoolableConnectionFactory} for details.)
     * <ul>
     * <li>You can set {@code autocommit(false)} and then {@code commit()} to use a transaction.</li>
     * <li>To cause a rollback you can either call {@code rollback()} explicitly, or throw a RuntimeException.</li>
     * </ul>
     * Note that this is very different to the behaviour of the default methods in this accessor where the
     * transaction is completely managed for you.
     * <p>
     * Example Usage:
     * <pre>
     *     queryDslAccessor.withNewConnection().execute(dbConnection -> {
     *             dbConnection.update(QIssueLink.ISSUE_LINK)
     *                     .set(QIssueLink.ISSUE_LINK.sequence, newSeq)
     *                     .where(QIssueLink.ISSUE_LINK.id.eq(issueLinkId))
     *                     .execute();
     *     });
     * </pre>
     *
     * @return a provider that allows you to run QueryDsl queries against a freshly borrowed DB connection.
     * @see #executeQuery(QueryCallback)
     * @see #execute(SqlCallback)
     * @see #withLegacyOfBizTransaction()
     * @since 7.0.6
     */
    ConnectionProvider withNewConnection();

    /**
     * Get a connection provider that will ensure that there is a Connection in OfBiz's Transaction ThreadLocal and then
     * use that.
     * <p>
     * This method is mostly useful if you want to call a mixture of OfBiz and Querydsl methods within your callback and
     * ensure that the OfBiz code participates in the same transaction as the Querydsl code. In other words, its a
     * handy workaround for dealing with legacy code, but the preferred long-term solution would usually be to convert
     * the OfBiz code to Querydsl.
     * <p>
     * If there is already an OfBiz ThreadLocal transaction in play, then this will be passed to the callback function.
     * If not, then a new OfBiz ThreadLocal transaction is started and it's connection is passed to the callback function.
     *
     * This method will attempt to run the callback within an existing OfBiz transaction if one is running within
     * this thread.
     * If not, it will start a new OfBiz ThreadLocal transaction which will be committed or rolled back once the
     * callback terminates.
     * <p>
     * The connection will always be in autocommit=false, and the commit or rollback will be completely managed for you.
     * If you want to rollback the transaction then throw a RuntimeException. The transaction will be marked as
     * "rollback required" such that the code that started the transaction will eventually call rollback on the
     * Connection and the exception will be propagated.
     * If the callback returns normally, then this indicates that the underlying transaction is allowed to be
     * committed at the appropriate time.
     * <p>
     * Because the connection (borrowing and returning from the pool) and its transaction are managed for you,
     * some specific {@link Connection} methods are illegal to call and will cause a RuntimeException including:
     * <ul>
     * <li>{@link Connection#setAutoCommit(boolean)} autocommit is always false</li>
     * <li>{@link Connection#commit()}</li> commit will occur in the broader transaction context if no errors are encountered
     * <li>{@link Connection#rollback()}</li> rollback will occur if a RuntimeException is thrown from the callback function
     * <li>{@link Connection#close()}</li> the connection will be returned to the pool for you at the appropriate time
     * </ul>
     * <p>
     * Example Usage:
     * <pre>
     *  queryDslAccessor.withLegacyOfBizTransaction().execute(dbConnection -> {
     *      doQueryDslStuffWith(dbConnection);
     *      callLegacyCodeThatUsesOfBiz();
     *  });
     * </pre>
     *
     * @return a provider that allows you to run QueryDsl queries and also have Ofbiz participate in the same Transaction.
     * @see #withNewConnection()
     * @see #executeQuery(QueryCallback)
     * @see #execute(SqlCallback)
     * @since 7.2.4
     */
    ConnectionProvider withLegacyOfBizTransaction();

    /**
     * Wrap an existing connection in a DbConnection to allow the use of QueryDsl against it.
     *
     * @param connection an already established connection.
     * @return a db connection that allows you to run QueryDsl queries against a predefined DB connection.
     * @since 7.2.4
     */
    DbConnection withDbConnection(Connection connection);
}
