package com.atlassian.jira.cache.request;

import com.atlassian.cache.CacheLoader;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.Closeable;
import java.util.HashMap;
import java.util.IdentityHashMap;
import java.util.Map;

/**
 * Utility class for explicitly managing request-scoped context.
 * <p>
 * A "request" is any relatively short-lived unit of work, which might be a user's HTTP request,
 * processing the incoming mail queue, reindexing a batch of issues, etc.  The request-scoped
 * context is managed implicitly for HTTP requests and jobs the run within atlassian-scheduler.
 * It is also implied by the use of {@link com.atlassian.jira.util.thread.JiraThreadLocalUtil},
 * and can alternatively be managed explicitly through this static utility.
 * </p>
 *
 * @since v6.4.8
 */
public class RequestCacheController {
    private static final ThreadLocal<CacheContext> CACHE_CONTEXT_THREAD_LOCAL = new ThreadLocal<>();

    /**
     * Establish or re-enter a request caching context.
     * <p>
     * Calls to this method must be paired with the same number of calls to {@link #closeContext()}.
     * If possible, this should be arranged by using a {@code try...finally} that closes the context
     * in the {@code finally} block.  Alternatively, {@link #process(Runnable)} will take care of
     * opening and closing the context for you.
     * </p>
     */
    public static void startContext() {
        final CacheContext existing = getContext();
        if (existing == null) {
            CACHE_CONTEXT_THREAD_LOCAL.set(new CacheContext());
        } else {
            existing.reopen();
        }
    }

    /**
     * Exit from a request caching context, destroying it if this context was not nested within another.
     *
     * @throws IllegalStateException if no request cache context had been established
     */
    public static void closeContext() {
        final CacheContext existing = getContext();
        if (existing == null) {
            throw new IllegalStateException("closeContext() without a matching startContext()");
        }
        existing.close();
    }

    /**
     * Explicitly clears all request caches for the current thread.
     * <p>
     * You probably do not need this.  It is intended for extreme cases, such as a data import performed from
     * a user's thread, where it is known that the system's data has been drastically modified from within a
     * single request's scope.
     * </p>
     * <p>
     * <strong>WARNING</strong>: This is only effective for the current thread.  If you think you need to clear
     * the request caches for all threads, then 1) You're wrong, and 2) That is impossible, anyway.
     * </p>
     */
    public static void clearAll() {
        final CacheContext existing = getContext();
        if (existing != null) {
            existing.clearAll();
        }
    }

    /**
     * Returns {@code true} if a request-scoped caching context has been established.
     * When no request-scoped caching context is established, request caches revert to a read-through behaviour;
     * that is, they will always pass every get request on to their cache loaders.
     *
     * @return {@code true} if a request-scoped caching context has been established; {@code false} otherwise.
     */
    public static boolean isInContext() {
        return getContext() != null;
    }

    /**
     * Returns the current {@link CacheContext}, if there is one.
     *
     * @return the current {@link CacheContext}, if there is one; {@code null} otherwise.
     */
    @Nullable
    static CacheContext getContext() {
        return CACHE_CONTEXT_THREAD_LOCAL.get();
    }

    /**
     * A convenient wrapper for running code within a context that participates in request caching.
     * <p>
     * If a request-scoped caching context does not currently exist, then one is established for the
     * duration of {@code runnable} and destroyed afterwards.  If a request-scoped caching context
     * has already been established, then there are no special side-effects; that is, the request
     * caches are already available and will not be flushed or destroyed by this method.
     * </p>
     *
     * @param runnable work to perform within a request-scoped context
     */
    public static void process(Runnable runnable) {
        startContext();
        try {
            runnable.run();
        } finally {
            closeContext();
        }
    }

    /**
     * Creates a new request-scoped cache.
     * See the linked factory methods for more information.
     *
     * @see RequestCacheFactory#createRequestCache(String)}
     * @see RequestCacheFactory#createRequestCache(String, CacheLoader)}
     */
    static <K, V> RequestCache<K, V> createRequestCache(@Nonnull final String name,
                                                        @Nonnull final CacheLoader<K, V> cacheLoader) {
        return new RequestCacheImpl<>(name, cacheLoader);
    }


    /**
     * Represents the current "request-scoped" thread-local caching context.
     */
    static class CacheContext implements Closeable {
        private final Map<RequestCache<?, ?>, Map<Object, Object>> mapOfCaches = new IdentityHashMap<>();

        private int nesting = 0;

        /**
         * Returns the request-scoped storage for the given request cache.
         * If that storage does not already exist, then one is created.
         *
         * @param requestCache the cache for which to locate
         * @return the local storage corresponding to the given request cache
         */
        @Nonnull
        Map<Object, Object> getLocalMap(final RequestCache<?, ?> requestCache) {
            Map<Object, Object> cache = mapOfCaches.get(requestCache);
            if (cache == null) {
                // hasn't been used in this context yet
                cache = new HashMap<>();
                mapOfCaches.put(requestCache, cache);
            }
            return cache;
        }

        /**
         * Returns the request-scoped storage for the given request cache, if it exists.
         *
         * @param requestCache the cache for which to locate
         * @return the local storage corresponding to the given request cache, if it already exists;
         * otherwise {@code null}.
         */
        @Nullable
        Map<Object, Object> getLocalMapIfExists(final RequestCache<?, ?> requestCache) {
            return mapOfCaches.get(requestCache);
        }

        /**
         * Clears <strong>all</strong> local storage for this thread.
         * Any request caches that belong to other threads are not affected by this.
         */
        void clearAll() {
            mapOfCaches.clear();
        }

        /**
         * Clears a specific request cache's local storage for this thread.
         * Any request caches that belong to other threads are not affected by this.
         *
         * @param requestCache the cache for which to clear the current thread's local storage
         */
        void clearLocalMap(final RequestCache<?, ?> requestCache) {
            mapOfCaches.remove(requestCache);
        }

        /**
         * Increments the nesting depth for this cache context.
         */
        void reopen() {
            ++nesting;
        }

        /**
         * Decrements the nesting depth for this cache context, or closes and destroys the current
         * context if the nesting depth was already {@code 0} (meaning that this matches the outermost
         * opening of the context).
         */
        @Override
        public void close() {
            if (nesting > 0) {
                --nesting;
            } else {
                CACHE_CONTEXT_THREAD_LOCAL.remove();
            }
        }
    }


}
