package com.atlassian.jira.issue.views.csv;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.IssueFieldConstants;
import com.atlassian.jira.issue.export.customfield.CsvIssueExporter;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.Field;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.web.component.TableLayoutFactory;

import static com.google.common.collect.Lists.newArrayList;

/**
 * Exports all of the available fields for a given issue search as a CsvOutput.
 *
 * @since 7.2.0
 */
public class SearchRequestCsvViewAllFields extends AbstractCsvSearchRequestView {
    private final JiraAuthenticationContext authenticationContext;
    private final TableLayoutFactory tableLayoutFactory;

    private static final String CUSTOM_FIELD_ID = "custom_field";

    private static final List<String> FIELD_ORDERING = newArrayList(
            IssueFieldConstants.SUMMARY,
            IssueFieldConstants.ISSUE_KEY,
            IssueFieldConstants.ISSUE_TYPE,
            IssueFieldConstants.STATUS,
            IssueFieldConstants.PROJECT,
            IssueFieldConstants.PRIORITY,
            IssueFieldConstants.RESOLUTION,
            IssueFieldConstants.ASSIGNEE,
            IssueFieldConstants.REPORTER,
            IssueFieldConstants.CREATOR,
            IssueFieldConstants.CREATED,
            IssueFieldConstants.UPDATED,
            IssueFieldConstants.LAST_VIEWED,
            IssueFieldConstants.RESOLUTION_DATE,
            IssueFieldConstants.AFFECTED_VERSIONS,
            IssueFieldConstants.FIX_FOR_VERSIONS,
            IssueFieldConstants.COMPONENTS,
            IssueFieldConstants.DUE_DATE,
            IssueFieldConstants.VOTES,
            IssueFieldConstants.LABELS,
            IssueFieldConstants.DESCRIPTION,
            IssueFieldConstants.ENVIRONMENT,
            IssueFieldConstants.WATCHES,
            IssueFieldConstants.WORKLOG,
            IssueFieldConstants.TIME_ORIGINAL_ESTIMATE,
            IssueFieldConstants.TIME_ESTIMATE,
            IssueFieldConstants.TIME_SPENT,
            IssueFieldConstants.PROGRESS,
            IssueFieldConstants.WORKRATIO,
            IssueFieldConstants.AGGREGATE_TIME_ORIGINAL_ESTIMATE,
            IssueFieldConstants.AGGREGATE_TIME_ESTIMATE,
            IssueFieldConstants.AGGREGATE_TIME_SPENT,
            IssueFieldConstants.AGGREGATE_PROGRESS,
            IssueFieldConstants.SECURITY,
            IssueFieldConstants.ISSUE_LINKS,
            IssueFieldConstants.ATTACHMENT,
            CUSTOM_FIELD_ID, //All custom fields should go here in Alphabetical ordering.
            IssueFieldConstants.COMMENT
    );

    public SearchRequestCsvViewAllFields(final ApplicationProperties applicationProperties,
                                         final CsvIssueExporter csvIssueExporter,
                                         final JiraAuthenticationContext authenticationContext,
                                         final TableLayoutFactory tableLayoutFactory) {
        super(applicationProperties, csvIssueExporter);
        this.authenticationContext = authenticationContext;
        this.tableLayoutFactory = tableLayoutFactory;
    }

    @Override
    protected List<Field> getFieldsToBeExported(final SearchRequest searchRequest) {
        final List<Field> fields =  newArrayList(tableLayoutFactory.getAllUserCsvColumnsFields(searchRequest, authenticationContext.getLoggedInUser()));
        Collections.sort(fields, fieldLayoutComparator);
        return fields;
    }

    /**
     * Comparator for the field layouts to order the fields. If they are both Custom Fields order
     * them by the name of the field. Else order them by the field ordering specified by
     * {@link SearchRequestCsvViewAllFields#FIELD_ORDERING}.
     */
    private static final Comparator<Field> fieldLayoutComparator = (firstField, secondField) -> {
        final boolean firstIsCustomField = (firstField instanceof CustomField);
        final boolean secondIsCustomField = (secondField instanceof CustomField);
        final boolean bothCustomFields = firstIsCustomField && secondIsCustomField;
        if (bothCustomFields) {
            return firstField.getName().compareTo(secondField.getName());
        } else {

            String firstId = firstField.getId();
            if (firstIsCustomField) {
                firstId = CUSTOM_FIELD_ID;
            }

            String secondId = secondField.getId();
            if (secondIsCustomField) {
                secondId = CUSTOM_FIELD_ID;
            }

            Integer firstIndex = FIELD_ORDERING.indexOf(firstId);
            Integer secondIndex = FIELD_ORDERING.indexOf(secondId);

            //Deals with cases where one or both fields are unknown
            if (firstIndex == -1 && secondIndex == -1) {
                return firstField.getName().compareTo(secondField.getName());
            } else if (firstIndex == -1) {
                return 1;
            } else if (secondIndex == -1) {
                return -1;
            }

            return firstIndex - secondIndex;
        }
    };
}
