package com.atlassian.jira.config;

import com.atlassian.jira.bc.ServiceResult;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.user.ApplicationUser;

/**
 * Provide possibility to mainipulate subtasks, move, check possibility to move and stuff like that.
 */
public interface SubTaskService {
    public static final String REORDER_COLUMN_HIDDEN_DF_KEY = "com.atlassian.jira.issuetable.move.links.hidden";

    public ServiceResult moveSubTask(ApplicationUser user, Issue parentIssue, Long currentSequence, Long sequence);

    public boolean isReorderColumnShown();

    public boolean canMoveSubtask(ApplicationUser user, Issue parentIssue);
}
