package com.atlassian.jira.permission.management.beans;

import com.atlassian.fugue.Option;
import com.google.common.base.Objects;
import org.codehaus.jackson.annotate.JsonAutoDetect;

@JsonAutoDetect
public class GrantToPermissionInputBean {
    private String securityType;
    private String value;

    public GrantToPermissionInputBean() {
    }

    public GrantToPermissionInputBean(String securityType, String value) {
        this.securityType = securityType;
        this.value = value;
    }

    public String getSecurityType() {
        return securityType;
    }

    public void setSecurityType(String securityType) {
        this.securityType = securityType;
    }

    public Option<String> getValue() {
        if (value == null || value.isEmpty()) {
            return Option.none();
        }
        return Option.option(value);
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        GrantToPermissionInputBean that = (GrantToPermissionInputBean) o;

        return Objects.equal(this.securityType, that.securityType) &&
                Objects.equal(this.value, that.value);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(securityType, value);
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("securityType", securityType)
                .add("value", value)
                .toString();
    }
}
