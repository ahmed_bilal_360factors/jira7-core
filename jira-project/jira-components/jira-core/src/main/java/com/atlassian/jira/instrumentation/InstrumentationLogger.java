package com.atlassian.jira.instrumentation;

import com.atlassian.annotations.Internal;

import javax.annotation.ParametersAreNonnullByDefault;
import java.util.List;
import java.util.Optional;

/**
 * Collects the instrumentation data and saves it to a datastore.
 *
 * @since v7.1
 */
@ParametersAreNonnullByDefault
@Internal
public interface InstrumentationLogger {
    /**
     * Saves request data to the queue above. The queue will store the last 100 requests by default. As new requests are
     * received the oldest are removed.
     * @param traceId     The ID for this request.
     * @param path        the path of the URL.
     * @param requestData The list of cache statistics.
     * @param requestTime The time in nanoseconds that the request took to complete.
     */
    void save(String traceId, String path, List<? extends Statistics> requestData, Optional<Long> requestTime);

    /**
     * Saves request data to the queue above. The queue will store the last 100 requests by default. As new requests are
     * received the oldest are removed.
     *
     * @param requestData  data associated with the request.
     * @param requestStats The list of cache statistics.
     * @param requestTime  The time of the request in nanoseconds. If zero, we did not collect any time.
     */
    void save(RequestData requestData, List<? extends Statistics> requestStats, Optional<Long> requestTime);

    /**
     * Saves request data to the queue above and generates a stacktrace that it inserts in the log. The queue will store
     * the last 100 requests by default. As new requests are received the oldest are removed.
     *
     * @param requestData  data associated with the request.
     * @param requestStats The list of cache statistics.
     * @param requestTime  The time of the request in nanoseconds. If zero, we did not collect any time.
     * @param exception    An exception to be included with the log. This can be used if you require a stack trace to be
     *                     recorded.
     */
    void save(RequestData requestData, List<? extends Statistics> requestStats, Optional<Long> requestTime,
              Exception exception);

    /**
     * Fetch the current list of statistics stored in memory.
     *
     * @return A copy of the current list.
     */
    List<LogEntry> getLogEntriesFromBuffer();

    /**
     * Clear all the data collected in the memory buffer.
     */
    void clearMemoryBuffer();
}
