package com.atlassian.jira.warmer;

/**
 * Common interface for all warmers in JIRA.
 *
 * @since v7.1
 */
public interface JiraWarmer extends Runnable {

}
