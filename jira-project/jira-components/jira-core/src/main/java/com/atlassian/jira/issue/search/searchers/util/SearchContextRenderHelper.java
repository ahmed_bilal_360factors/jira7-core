package com.atlassian.jira.issue.search.searchers.util;

import com.atlassian.jira.issue.search.SearchContext;
import com.atlassian.jira.util.Named;
import com.opensymphony.util.TextUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Adds search context parameters to velocityParams. Used by searchContextDescriptionTitle in macros.vm
 *
 * @since v5.2
 */
public class SearchContextRenderHelper {
    public static void addSearchContextParams(SearchContext searchContext, Map<String, Object> velocityParams) {
        velocityParams.put("contextProjectNames", joinNames(searchContext.getProjects()));
        velocityParams.put("contextIssueTypeNames", joinNames(searchContext.getIssueTypes()));
    }

    private static String joinNames(Collection<? extends Named> items) {
        final List<String> names = new ArrayList<>(items.size());
        items.stream()
                .map(Named::getName)
                .map(TextUtils::htmlEncode)
                .forEach(names::add);
        return StringUtils.join(names, ", ");
    }
}
