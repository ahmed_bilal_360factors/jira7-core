package com.atlassian.jira.issue.label;

import com.atlassian.jira.issue.customfields.CustomFieldUtils;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.template.VelocityTemplatingEngine;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.JiraVelocityUtils;
import com.google.common.annotations.VisibleForTesting;
import org.apache.velocity.exception.VelocityException;

import java.util.Map;

import static com.atlassian.jira.template.TemplateSources.file;

/**
 * @since v4.3
 */
public class DefaultAlphabeticalLabelRenderer implements AlphabeticalLabelRenderer {
    private AlphabeticalLabelGroupingService alphabeticalLabelGroupingService;
    private final VelocityTemplatingEngine templatingEngine;
    private final JiraAuthenticationContext authenticationContext;
    private final FieldManager fieldManager;
    private final ProjectManager projectManager;
    private final LabelUtil labelUtil;
    private final I18nHelper.BeanFactory beanFactory;

    public DefaultAlphabeticalLabelRenderer(
            final AlphabeticalLabelGroupingService alphabeticalLabelGroupingService,
            final VelocityTemplatingEngine templatingEngine,
            final JiraAuthenticationContext authenticationContext,
            final FieldManager fieldManager,
            final ProjectManager projectManager,
            final LabelUtil labelUtil,
            final I18nHelper.BeanFactory beanFactory) {
        this.alphabeticalLabelGroupingService = alphabeticalLabelGroupingService;
        this.templatingEngine = templatingEngine;
        this.authenticationContext = authenticationContext;
        this.fieldManager = fieldManager;
        this.projectManager = projectManager;
        this.labelUtil = labelUtil;
        this.beanFactory = beanFactory;
    }

    @Override
    public String getHtml(final ApplicationUser remoteUser, final Long projectId, final String fieldId, final boolean isOtherFieldsExist) {
        try {
            final AlphabeticalLabelGroupingSupport alphabeticallyGroupedLabels = alphabeticalLabelGroupingService.getAlphabeticallyGroupedLabels(remoteUser, projectId, fieldId);
            final Map<String, Object> startingParams = getDefaultVelocityParams();

            startingParams.put("field", fieldManager.getField(fieldId));
            startingParams.put("project", projectManager.getProjectObj(projectId));
            startingParams.put("labelUtils", labelUtil);
            startingParams.put("labelCount", alphabeticallyGroupedLabels.getUniqueLabelsCount());
            startingParams.put("alphaSupport", alphabeticallyGroupedLabels);
            startingParams.put("isCustomField", fieldId.startsWith(CustomFieldUtils.CUSTOM_FIELD_PREFIX));
            startingParams.put("remoteUser", remoteUser);
            startingParams.put("i18n", beanFactory.getInstance(remoteUser));
            startingParams.put("isOtherFieldsExist", isOtherFieldsExist);

            return templatingEngine.render(file("templates/plugins/jira/projectpanels/labels-alphabetical.vm"))
                    .applying(startingParams)
                    .asHtml();
        } catch (VelocityException e) {
            throw new RuntimeException(e);
        }
    }

    @VisibleForTesting
    Map<String, Object> getDefaultVelocityParams() {
        return JiraVelocityUtils.getDefaultVelocityParams(authenticationContext);
    }
}
