package com.atlassian.jira.sharing.type;

import com.atlassian.jira.sharing.SharePermission;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.dbc.Assertions;

/**
 * Permissions Checker for shared {@link com.atlassian.jira.sharing.SharedEntity}.
 *
 * @since v7.2.2
 */
public class AuthenticatedUserShareTypePermissionChecker implements ShareTypePermissionChecker {
    /**
     * All JIRA authenticated users are able to see/use shared {@link com.atlassian.jira.sharing.SharedEntity} so this
     * will always return true, if the user is not null.
     *
     * @param user       The user to check.
     * @param permission Must be a permission for a {@link AuthenticatedUserShareType}
     * @return true if user is not null.
     */
    @Override
    public boolean hasPermission(final ApplicationUser user, final SharePermission permission) {
        Assertions.notNull("permission", permission);
        Assertions.equals("permission-type", AuthenticatedUserShareType.TYPE, permission.getType());

        return user != null;
    }
}
