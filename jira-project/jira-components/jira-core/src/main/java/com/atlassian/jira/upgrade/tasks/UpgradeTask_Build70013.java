package com.atlassian.jira.upgrade.tasks;

import com.atlassian.jira.entity.Delete;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.portal.CachingPortletConfigurationStore;
import com.atlassian.jira.portal.OfbizPortletConfigurationStore;
import com.atlassian.jira.upgrade.AbstractImmediateUpgradeTask;


/**
 * Removes the JIRA Admin gadget.
 *
 * @since v7.0
 */
public class UpgradeTask_Build70013 extends AbstractImmediateUpgradeTask {
    private static final String ADMIN_DASHBOARD_ITEM_KEY = "com.atlassian.jira.gadgets:admin-dashboard-item";
    private static final String ADMIN_GADGET_XML = "rest/gadgets/1.0/g/com.atlassian.jira.gadgets:admin-gadget/gadgets/admin-gadget.xml";

    private final OfBizDelegator ofBizDelegator;
    private final CachingPortletConfigurationStore cachingPortletConfigurationStore;


    public UpgradeTask_Build70013(final OfBizDelegator ofBizDelegator,
                                  final CachingPortletConfigurationStore cachingPortletConfigurationStore) {
        this.ofBizDelegator = ofBizDelegator;
        this.cachingPortletConfigurationStore = cachingPortletConfigurationStore;
    }

    @Override
    public int getBuildNumber() {
        return 70013;
    }

    @Override
    public String getShortDescription() {
        return "Removing the JIRA Admin gadget.";
    }

    @Override
    public void doUpgrade(final boolean setupMode) throws Exception {
        try {
            Delete.from(OfbizPortletConfigurationStore.TABLE)
                    .whereLike(OfbizPortletConfigurationStore.Columns.MODULE_KEY, ADMIN_DASHBOARD_ITEM_KEY)
                    .execute(ofBizDelegator);

            Delete.from(OfbizPortletConfigurationStore.TABLE)
                    .whereLike(OfbizPortletConfigurationStore.Columns.GADGET_XML, ADMIN_GADGET_XML)
                    .execute(ofBizDelegator);
        } finally {
            cachingPortletConfigurationStore.flush();
        }
    }

    @Override
    public boolean isDowngradeTaskRequired() {
        //No downgrade task required as this change cannot be reversed (we removed all info about portlet configuration
        //for admin gadget). This is no big deal as admin-gadget is not very useful and can be added again.
        return false;
    }
}
