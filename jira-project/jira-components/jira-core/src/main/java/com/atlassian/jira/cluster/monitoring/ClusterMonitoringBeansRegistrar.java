package com.atlassian.jira.cluster.monitoring;

import com.atlassian.jira.ComponentManager;
import com.atlassian.jira.startup.ClusteringLauncher;
import com.google.common.annotations.VisibleForTesting;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.management.InstanceAlreadyExistsException;
import javax.management.InstanceNotFoundException;
import javax.management.MBeanRegistrationException;
import javax.management.MBeanServer;
import javax.management.MalformedObjectNameException;
import javax.management.NotCompliantMBeanException;
import javax.management.ObjectName;
import java.lang.management.ManagementFactory;

/**
 * Initialise the Management Bean Server that exposes JMX resources used for cluster monitoring.
 *
 * @since v7.3
 */
public class ClusterMonitoringBeansRegistrar {
    public static final String CLUSTER_MONITORING_DARK_FEATURE = "jira.zdu.jmx-monitoring";

    private static final Logger LOG = LoggerFactory.getLogger(ClusterMonitoringBeansRegistrar.class);

    private final MBeanServer mBeanServer;

    public ClusterMonitoringBeansRegistrar() {
        this(ManagementFactory.getPlatformMBeanServer());
    }

    /**
     * This lets us inject a stub MBeanServer for testing.
     *
     * @param mBeanServer In regular use, we just take the default instance from Java's ManagementFactory.
     */
    @VisibleForTesting
    ClusterMonitoringBeansRegistrar(MBeanServer mBeanServer) {
        this.mBeanServer = mBeanServer;
    }

    /**
     * Expose our JMX beans so that JConsole/Nagios/NewRelic can play with them. This should only be called _once_
     * during initialisation, but nothing bad will happen if you call it twice ;). Three times though? Forget about it.
     *
     * @see ClusteringLauncher#start()
     */
    public void registerClusterMonitoringMBeans() {
        registerMBean(ClusterNodeStatusMBean.BEAN_NAME, ClusterNodeStatus.class);
    }

    /**
     * This should run whenever this component is torn down.
     */
    public void unregisterClusterMonitorMBeans() {
        unregisterMBean(ClusterNodeStatusMBean.BEAN_NAME);
    }

    private void registerMBean(String objectName, Class<?> implementationClass) {
        try {
            ObjectName beanName = getObjectName(objectName);
            if (mBeanServer.isRegistered(beanName)) {
                LOG.debug(objectName + " is already registered, skipping...");
                return;
            }
            mBeanServer.registerMBean(ComponentManager.getComponent(implementationClass), beanName);
            LOG.debug("Registered monitoring bean " + beanName);
        } catch (InstanceAlreadyExistsException | MBeanRegistrationException | NotCompliantMBeanException e) {
            LOG.warn("Unable to register monitoring bean: " + objectName, e);
        }
    }

    private void unregisterMBean(final String objectName) {
        ObjectName beanName = getObjectName(objectName);
        try {
            mBeanServer.unregisterMBean(beanName);
            LOG.debug("Unregistered monitoring bean " + beanName);
        } catch (InstanceNotFoundException | MBeanRegistrationException e) {
            LOG.error("Unable to unregister monitoring bean " + beanName, e);
        }
    }

    private ObjectName getObjectName(final String beanName) {
        try {
            return new ObjectName(beanName);
          } catch (MalformedObjectNameException e) {
            throw new RuntimeException("Unable to name monitoring bean", e);
        }
    }
}
