package com.atlassian.jira.issue.views.conditions;

import com.atlassian.jira.config.FeatureManager;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.web.Condition;

import java.util.Map;

public class IsCsvExportEnabledCondition implements Condition {
    protected static final String DARK_FEATURE_CSV_EXPORT_ENABLED = "jira.export.csv.enabled";
    protected static final String DARK_FEATURE_CSV_EXPORT_DISABLED = "jira.export.csv.disabled";

    private final FeatureManager featureManager;

    public IsCsvExportEnabledCondition(final FeatureManager featureManager) {
        this.featureManager = featureManager;
    }

    @Override
    public void init(final Map<String, String> params) throws PluginParseException {

    }

    @Override
    public boolean shouldDisplay(final Map<String, Object> context) {
        return featureManager != null
            && featureManager.isEnabled(DARK_FEATURE_CSV_EXPORT_ENABLED) && !featureManager.isEnabled(DARK_FEATURE_CSV_EXPORT_DISABLED);
    }
}
