package com.atlassian.jira.web.action.admin.customfields;

import com.atlassian.jira.config.managedconfiguration.ManagedConfigurationItemService;
import com.atlassian.jira.issue.customfields.CustomFieldUtils;
import com.atlassian.jira.issue.customfields.impl.VersionCFType;
import com.atlassian.jira.issue.customfields.manager.GenericConfigManager;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.security.xsrf.RequiresXsrfCheck;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.sal.api.websudo.WebSudoRequired;
import webwork.action.ActionContext;

import java.util.HashMap;
import java.util.Map;

/**
 * Represents action that handles the update of option order on version picker custom field
 */
@WebSudoRequired
public class EditVersionPickerCustomFieldOptionsOrder extends AbstractEditConfigurationItemAction {
    private final I18nHelper i18nHelper;
    private final GenericConfigManager genericConfigManager;

    public EditVersionPickerCustomFieldOptionsOrder(ManagedConfigurationItemService managedConfigurationItemService, final I18nHelper i18nHelper, GenericConfigManager genericConfigManager) {
        super(managedConfigurationItemService);
        this.i18nHelper = i18nHelper;
        this.genericConfigManager = genericConfigManager;
    }

    public String doDefault() throws Exception {
        setReturnUrl(null);
        if (!(getCustomField().getCustomFieldType() instanceof VersionCFType)) {
            addErrorMessage(getText("admin.errors.customfields.cannot.set.options", "'" + getCustomField().getCustomFieldType().getName() + "'"));
        }

        return super.doDefault();
    }

    @RequiresXsrfCheck
    protected String doExecute() throws Exception {
        final CustomField customField = getCustomField();

        final Map map = new HashMap();
        Map actionParams = ActionContext.getContext().getParameters();

        customField.populateFromParams(map, actionParams);
        int versionOrder = retrieveVersionOrderParam(actionParams);
        setVersionOrder(getFieldConfig(), versionOrder);

        return getRedirect("ConfigureCustomField!default.jspa?customFieldId=" + customField.getIdAsLong());
    }

    protected void doValidation() {
        if (getCustomField() == null) {
            addErrorMessage(getText("admin.errors.customfields.no.field.selected.for.edit"));
        }

        if (validateFieldLocked()) {
            return;
        }
    }

    private void setVersionOrder(final FieldConfig fieldConfig, final int versionOrder) {
        genericConfigManager.update(VersionCFType.VersionOrder.getDatabaseType(), fieldConfig.getId().toString(), versionOrder);
    }

    private int retrieveVersionOrderParam(Map<String, String[]> parameters) {
        for (final Map.Entry<String, String[]> parameter : parameters.entrySet()) {
            final String key = parameter.getKey();
            final String customFieldKey = CustomFieldUtils.getCustomFieldKey(key);
            if (key != null && getCustomField().getId().equals(customFieldKey)) {
                return Integer.parseInt(parameter.getValue()[0]);
            }
        }
        return -1;
    }

    public Map<Integer, String> getVersionOrderSelectList() throws Exception {
        Map<Integer, String> map = new HashMap<Integer, String>();
        for (VersionCFType.VersionOrder versionOrder : VersionCFType.VersionOrder.values()) {
            map.put(versionOrder.getId(), versionOrder.getTranslation(i18nHelper));
        }
        return map;
    }

    public String getVersionOrder() throws Exception {
        Object versionOrder = genericConfigManager.retrieve(VersionCFType.VersionOrder.getDatabaseType(), getFieldConfig().getId().toString());
        if (versionOrder != null) {
            return versionOrder.toString();
        }
        return null;
    }
}
