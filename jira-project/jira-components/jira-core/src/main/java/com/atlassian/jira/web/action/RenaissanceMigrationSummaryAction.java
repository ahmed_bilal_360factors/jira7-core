package com.atlassian.jira.web.action;

import com.atlassian.application.api.Application;
import com.atlassian.application.api.ApplicationManager;
import com.atlassian.collectors.CollectorsUtil;
import com.atlassian.jira.application.ApplicationKeys;
import com.atlassian.jira.application.ApplicationRole;
import com.atlassian.jira.application.ApplicationRoleAdminService;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.util.collect.MapBuilder;
import com.atlassian.jira.web.landingpage.RenaissanceMigrationPageRedirect;
import com.atlassian.webresource.api.assembler.PageBuilderService;
import com.google.common.collect.ImmutableList;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Map;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static com.atlassian.jira.upgrade.tasks.role.MoveJira6xABPServiceDeskPermissions.PROPERTY_SERVICEDESK_NOT_MIGRATED_GROUPS;
import static com.atlassian.jira.web.landingpage.RenaissanceMigrationPageRedirect.PROPERTY_POST_MIGRATION_PAGE_DISPLAYED;

/**
 * Presents summary of Renaissance migration to the user.
 *
 * @since v7.0
 */
public class RenaissanceMigrationSummaryAction extends JiraWebActionSupport {
    public static final String URL_JIRA_HOME = "/secure/MyJiraHome.jspa";

    private final ApplicationManager applicationManager;
    private final ApplicationProperties applicationProperties;
    private final RenaissanceMigrationPageRedirect pageRedirect;
    private final PageBuilderService pageBuilderService;
    private final ApplicationRoleAdminService applicationRoleAdminService;


    private boolean dismiss;

    public RenaissanceMigrationSummaryAction(final ApplicationManager applicationManager,
                                             final ApplicationProperties applicationProperties, final RenaissanceMigrationPageRedirect pageRedirect,
                                             final PageBuilderService pageBuilderService, final ApplicationRoleAdminService applicationRoleAdminService) {
        this.applicationManager = applicationManager;
        this.applicationProperties = applicationProperties;
        this.pageRedirect = pageRedirect;
        this.pageBuilderService = pageBuilderService;
        this.applicationRoleAdminService = applicationRoleAdminService;
    }

    @Override
    protected String doExecute() throws Exception {
        if (dismiss) {
            applicationProperties.setOption(PROPERTY_POST_MIGRATION_PAGE_DISPLAYED, true);
            //It is no longer needed, so there is no point in checking if page should be redirected to it.
            pageRedirect.unregisterSelf();
            return getRedirect(URL_JIRA_HOME);

        }
        pageBuilderService.assembler().resources().requireWebResource("jira.webresources:renaissance-migration");
        return super.doExecute();
    }

    public boolean isDismiss() {
        return dismiss;
    }

    public void setDismiss(final boolean dismiss) {
        this.dismiss = dismiss;
    }

    @ActionViewData("success")
    public Map<String, Object> getData() {
        Application application = StreamSupport.stream(applicationManager.getApplications().spliterator(), false)
                .filter(app -> !ApplicationKeys.CORE.equals(app.getKey()))
                .findFirst()
                .orElseGet(applicationManager::getPlatform);

        return MapBuilder.<String, Object>newBuilder()
                .add("productName", application.getName())
                .add("productVersion", application.getVersion())
                .add("notMigratedGroups", notMigratedGroups())
                .add("notDefinedRoles", notDefinedRoles())
                .toMap();
    }

    private Collection<ApplicationRole> notDefinedRoles() {
        return Optional.ofNullable(applicationRoleAdminService.getRoles().get()).orElse(Collections.emptySet())
                .stream()
                .filter(ar -> !ar.isDefined())
                .sorted(Comparator.comparing(ApplicationRole::getName))
                .collect(CollectorsUtil.toImmutableList());
    }

    private ImmutableList<String> notMigratedGroups() {
        return Optional.ofNullable(applicationProperties.getString(PROPERTY_SERVICEDESK_NOT_MIGRATED_GROUPS))
                .map(groups -> groups.split(","))
                .map(Arrays::asList)
                .orElse(Collections.emptyList())
                .stream()
                .map(String::trim)
                .collect(CollectorsUtil.toImmutableList());
    }
}
