package com.atlassian.jira.plugin.webresource;

import com.atlassian.jira.config.properties.JiraProperties;
import com.atlassian.jira.config.properties.JiraSystemProperties;
import com.atlassian.plugin.webresource.cdn.CDNStrategy;
import com.atlassian.plugin.webresource.prebake.PrebakeConfig;
import org.apache.commons.lang.StringUtils;

import java.util.Optional;

/**
 * CDN Strategy for JIRA that takes a static prefix. This will transform cdn-able resources to be served from
 * a single host, with the app's hostname as the first section of the url. For example, {@code /my/resource.js} will be
 * transformed to be served from {@code //my.cdnhost.com/my.jirahost.com/my/resource.js}
 * <p>
 * <ul>
 * <li>
 * Set the dark feature {@code jira.fixed.cdn.enabled} to true to enable
 * </li>
 * <li>
 * (Set the dark feature {@code jira.fixed.cdn.disabled} to true to override any enablement and disable the feature.)
 * </li>
 * <li>
 * Set the system propery {@code jira.fixed.cdn.prefix} to the cdn prefix, eg
 * {@code //my.cdnhost.com/my.jira.com}. This must not end with a trailing slash.
 * </li>
 * </ul>
 *
 * @since v6.4
 */
public class JiraPrefixCDNStrategy implements CDNStrategy {
    static final String ENABLED_FEATURE_KEY = "jira.fixed.cdn.enabled";
    static final String PREFIX_SYSTEM_PROPERTY = "jira.fixed.cdn.prefix";

    private final JiraProperties jiraSystemProperties = JiraSystemProperties.getInstance();
    private final Optional<PrebakeConfig> prebakeConfig;

    public JiraPrefixCDNStrategy(final Optional<PrebakeConfig> prebakeConfig) {
        this.prebakeConfig = prebakeConfig;
    }

    @Override
    public boolean supportsCdn() {
        return !StringUtils.isBlank(getPrefix());
    }

    @Override
    public String transformRelativeUrl(String s) {
        return getPrefix() + s;
    }

    private String getPrefix() {
        return jiraSystemProperties.getProperty(PREFIX_SYSTEM_PROPERTY);
    }

    @Override
    public Optional<PrebakeConfig> getPrebakeConfig() {
        return prebakeConfig;
    }
}
