package com.atlassian.jira.upgrade.tasks.role;

import com.atlassian.crowd.embedded.api.CrowdService;
import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.crowd.embedded.api.GroupWithAttributes;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.google.common.collect.ImmutableList;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

import static com.atlassian.crowd.search.query.entity.EntityQuery.ALL_RESULTS;

public final class MigrationGroupServiceImpl implements MigrationGroupService {
    private final CrowdService crowdService;
    private final GlobalPermissionDao globalPermissionDao;


    public MigrationGroupServiceImpl(GlobalPermissionDao globalPermissionDao, final CrowdService crowdService) {
        this.crowdService = crowdService;
        this.globalPermissionDao = globalPermissionDao;
    }

    @Override
    public GroupWithAttributes getGroupWithAttributes(Group group) {
        return crowdService.getGroupWithAttributes(group.getName());
    }

    @Override
    public Set<UserWithPermissions> getUsersInGroup(Group group) {
        return usersInTheGroup(group).stream()
                .filter(User::isActive)
                .map(this::buildUserWithPermissions)
                .collect(Collectors.toSet());
    }

    private Collection<User> usersInTheGroup(final Group group) {
        Iterable<User> usersIterable = crowdService.search(
                QueryBuilder.queryFor(User.class, EntityDescriptor.user())
                        .childrenOf(EntityDescriptor.group())
                        .withName(group.getName())
                        .returningAtMost(ALL_RESULTS));
        return ImmutableList.copyOf(usersIterable);
    }

    private UserWithPermissions buildUserWithPermissions(User user) {
        final boolean hasUse = globalPermissionDao.groupsWithUsePermission().stream()
                .anyMatch(useGroup -> crowdService.isUserMemberOfGroup(user, useGroup));
        final boolean hasAdmin = globalPermissionDao.groupsWithAdminPermission().stream()
                .anyMatch(adminGroup -> crowdService.isUserMemberOfGroup(user, adminGroup));
        return new UserWithPermissions(user, hasUse, hasAdmin);
    }
}
