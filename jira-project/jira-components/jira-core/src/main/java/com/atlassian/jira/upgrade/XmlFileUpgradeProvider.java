package com.atlassian.jira.upgrade;

import com.atlassian.core.util.ClassLoaderUtils;
import com.atlassian.jira.util.JiraUtils;
import electric.xml.Document;
import electric.xml.Element;
import electric.xml.ParseException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.Collections;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;

import static com.atlassian.collectors.CollectorsUtil.toImmutableList;

public class XmlFileUpgradeProvider implements UpgradeProvider {
    private static final String UPGRADE_XML_TYPE = "upgrade";

    private final String upgradesXmlFilePath;

    @SuppressWarnings("unused")
    public XmlFileUpgradeProvider() {
        this("upgrades.xml");
    }

    XmlFileUpgradeProvider(final String upgradesXmlFilePath) {
        this.upgradesXmlFilePath = upgradesXmlFilePath;
    }

    @Override
    public <T> Collection<T> getUpgradeTasks() {
        return findUpgradeTasks().stream()
                .map(getClassNameFromUpgradeTaskNode)
                .map(this::<T>loadUpgradeTaskComponent)
                .collect(toImmutableList());
    }

    @Override
    public <T> Collection<T> getUpgradeTasksBoundByBuild(long buildNumberUpperBound) {
        return findUpgradeTasks().stream()
                .filter(isBuildNumberLessThanOrEqualTo(buildNumberUpperBound))
                .map(getClassNameFromUpgradeTaskNode)
                .map(this::<T>loadUpgradeTaskComponent)
                .collect(toImmutableList());
    }

    @SuppressWarnings({"unchecked"})
    private Collection<Element> findUpgradeTasks() {
        try (InputStream is = getFileInputStream()) {
            final Document doc = new Document(is);
            final Element root = doc.getRoot();
            return Collections.<Element>list(root.getElements(UPGRADE_XML_TYPE));
        } catch (final ParseException | IOException e) {
            throw new RuntimeException("Error parsing tasks file '" + upgradesXmlFilePath + "'", e);
        }
    }

    private InputStream getFileInputStream() {
        return Optional.ofNullable(ClassLoaderUtils.getResourceAsStream(upgradesXmlFilePath, this.getClass()))
                .orElseThrow(() -> new RuntimeException("Could not read upgrade file '" + upgradesXmlFilePath + "'"));
    }

    private <T> T loadUpgradeTaskComponent(final String taskName) {
        try {
            return JiraUtils.loadComponent(taskName, this.getClass());
        } catch (final ClassNotFoundException | RuntimeException e) {
            throw new RuntimeException("Failed to instantiate task: " + taskName, e);
        }
    }

    private static final Function<Element, String> getClassNameFromUpgradeTaskNode = node ->
            node.getElement("class").getTextString();

    private static final Predicate<Element> isBuildNumberLessThanOrEqualTo(final Long build) {
        return node -> Long.valueOf(node.getAttribute("build")) <= build;
    }
}
