package com.atlassian.jira.permission.management.beans;

import com.google.common.base.Objects;
import org.codehaus.jackson.annotate.JsonAutoDetect;

@JsonAutoDetect
public class SecurityTypeValueBean {
    private Long id;
    private String value;
    private String displayValue;
    private Boolean valid;

    @Deprecated
    public SecurityTypeValueBean() {
    }

    private SecurityTypeValueBean(Long id, String value, String displayValue, Boolean valid) {
        this.id = id;
        this.value = value;
        this.displayValue = displayValue;
        this.valid = valid;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getDisplayValue() {
        return displayValue;
    }

    public void setDisplayValue(String displayValue) {
        this.displayValue = displayValue;
    }

    public Boolean getValid() {
        return valid;
    }

    public void setValid(Boolean valid) {
        this.valid = valid;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static Builder builder(SecurityTypeValueBean data) {
        return new Builder(data);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SecurityTypeValueBean that = (SecurityTypeValueBean) o;

        return Objects.equal(this.id, that.id) &&
                Objects.equal(this.value, that.value) &&
                Objects.equal(this.displayValue, that.displayValue) &&
                Objects.equal(this.valid, that.valid);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id, value, displayValue, valid);
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("id", id)
                .add("value", value)
                .add("displayValue", displayValue)
                .add("valid", valid)
                .toString();
    }

    public static final class Builder {
        private Long id;
        private String value;
        private String displayValue;
        private Boolean valid;

        private Builder() {
        }

        private Builder(SecurityTypeValueBean initialData) {

            this.id = initialData.id;
            this.value = initialData.value;
            this.displayValue = initialData.displayValue;
        }


        public Builder setId(Long id) {
            this.id = id;
            return this;
        }


        public Builder setValue(String value) {
            this.value = value;
            return this;
        }


        public Builder setDisplayValue(String displayValue) {
            this.displayValue = displayValue;
            return this;
        }

        public Builder setValid(Boolean valid) {
            this.valid = valid;
            return this;
        }

        public SecurityTypeValueBean build() {
            return new SecurityTypeValueBean(id, value, displayValue, valid);
        }
    }
}
