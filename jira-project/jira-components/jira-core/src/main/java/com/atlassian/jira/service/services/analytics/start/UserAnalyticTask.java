package com.atlassian.jira.service.services.analytics.start;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.license.LicenseCountService;
import com.atlassian.jira.service.services.analytics.JiraAnalyticTask;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.collect.MapBuilder;
import com.google.common.annotations.VisibleForTesting;

import java.util.Map;

/**
 * Gets the information of active users / total users
 */
public class UserAnalyticTask implements JiraAnalyticTask {

    private LicenseCountService licenseCountService;
    private UserManager userManager;


    public UserAnalyticTask() {
    }

    @VisibleForTesting
    UserAnalyticTask(final LicenseCountService licenseCountService, final UserManager userManager) {
        this.licenseCountService = licenseCountService;
        this.userManager = userManager;
    }

    @Override
    public void init() {
        this.licenseCountService = ComponentAccessor.getComponent(LicenseCountService.class);
        this.userManager = ComponentAccessor.getComponent(UserManager.class);
    }

    @Override
    public Map<String, Object> getAnalytics() {
        final MapBuilder<String, Object> builder = MapBuilder.newBuilder();

        builder.add("users.total", userManager.getTotalUserCount());
        builder.add("users.active", licenseCountService.totalBillableUsers());

        return builder.toMap();
    }

    @Override
    public boolean isReportingDataShape() {
        return true;
    }
}
