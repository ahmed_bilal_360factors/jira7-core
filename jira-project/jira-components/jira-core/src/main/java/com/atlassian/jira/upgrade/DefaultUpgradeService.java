package com.atlassian.jira.upgrade;

import com.atlassian.jira.cluster.zdu.ClusterUpgradeStateManager;
import com.atlassian.jira.util.BuildUtilsInfo;
import com.atlassian.upgrade.spi.UpgradeTaskFactory;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Upgrade service that executes upgrades depending on which upgrades are pending.
 */
public class DefaultUpgradeService implements UpgradeService {

    private final IndexingUpgradeService indexingUpgradeService;
    private final DelayedUpgradeService delayedUpgradeService;
    private final UpgradeTaskFactory upgradeTaskFactory;
    private final BuildUtilsInfo buildUtilsInfo;
    private final ClusterUpgradeStateManager clusterUpgradeStateManager;

    public DefaultUpgradeService(
            final IndexingUpgradeService indexingUpgradeService,
            final DelayedUpgradeService delayedUpgradeService,
            final UpgradeTaskFactory upgradeTaskFactory,
            final BuildUtilsInfo buildUtilsInfo,
            final ClusterUpgradeStateManager clusterUpgradeStateManager
    ) {
        this.indexingUpgradeService = indexingUpgradeService;
        this.delayedUpgradeService = delayedUpgradeService;
        this.upgradeTaskFactory = upgradeTaskFactory;
        this.buildUtilsInfo = buildUtilsInfo;
        this.clusterUpgradeStateManager = clusterUpgradeStateManager;
    }

    /**
     * Executed upgrades depending on which upgrades are pending.
     * <p>
     *     First a check for pending {@link AbstractImmediateUpgradeTask}s is done. If there are some all upgrades
     *     will be run synchronously. If not it will try to schedule delayed upgrade tasks.
     * </p>
     *
     * @return the result of running the upgrades.
     */
    @Override
    public UpgradeResult runUpgrades() {

        final List<com.atlassian.upgrade.spi.UpgradeTask> pendingUpgradeTasks = upgradeTaskFactory
                .getAllUpgradeTasks().stream()
                .filter(this::isPending)
                .collect(Collectors.toList());

        final boolean immediateUpgradeTasksPending = pendingUpgradeTasks.stream()
                .anyMatch(this::isImmediate);

        if (immediateUpgradeTasksPending) {
            return indexingUpgradeService.runUpgrades();
        }

        if (!clusterUpgradeStateManager.areDelayedUpgradesHandledByCluster()) {
            return delayedUpgradeService.scheduleUpgrades();
        }

        return UpgradeResult.OK;
    }

    private boolean isPending(final com.atlassian.upgrade.spi.UpgradeTask upgradeTask) {
        return upgradeTask.getBuildNumber() > buildUtilsInfo.getDatabaseBuildNumber();
    }

    /**
     * An upgrade task is immediate if it implements {@link UpgradeTask} and is scheduled to run before JIRA starts.
     *
     * @param upgradeTask to be checked
     * @return true if and only if upgrade task is immediate, false otherwise
     */
    private boolean isImmediate(final com.atlassian.upgrade.spi.UpgradeTask upgradeTask) {
        if (UpgradeTask.class.isAssignableFrom(upgradeTask.getClass())) {
            return ((UpgradeTask) upgradeTask).getScheduleOption() == UpgradeTask.ScheduleOption.BEFORE_JIRA_STARTED;
        }

        return false;
    }

    @Override
    public boolean areUpgradesRunning() {
        return indexingUpgradeService.areUpgradesRunning();
    }
}
