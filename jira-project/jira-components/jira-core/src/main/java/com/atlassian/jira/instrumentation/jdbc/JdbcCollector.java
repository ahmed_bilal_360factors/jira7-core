package com.atlassian.jira.instrumentation.jdbc;

import com.atlassian.instrumentation.caches.RequestListener;
import com.atlassian.instrumentation.driver.JdbcThreadLocalCollector;
import com.google.common.collect.ImmutableMap;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.LongSummaryStatistics;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * This class acts as the collector for JDBC stats that are collected in the JDBC proxy.
 *
 * @since v7.1.0
 */
public class JdbcCollector implements RequestListener {

    private static final String JDBC_LOGGING_KEY = "jdbc";

    private final boolean sqlExecutionTimesEnabled;

    public JdbcCollector(boolean sqlExecutionTimesEnabled) {
        this.sqlExecutionTimesEnabled = sqlExecutionTimesEnabled;
    }

    @Override
    public String getName() {
        return JDBC_LOGGING_KEY;
    }

    @Override
    public void onRequestStart() {
        // Do nothing yet
        JdbcThreadLocalCollector.start();
    }

    @Override
    public String getLoggingKey() {
        return JDBC_LOGGING_KEY;
    }

    @Override
    public Map<String, Object> onRequestEnd() {

        Map<String, LongSummaryStatistics> counts = JdbcThreadLocalCollector.getStatistics();
        Map<String, Long> medians = JdbcThreadLocalCollector.getMedianTime();
        Map<String, List<Long[]>> executionTimes = JdbcThreadLocalCollector.getStartAndEndTimes();
        final Map<String, List<List<StackTraceElement>>> stackTraces = JdbcThreadLocalCollector.getStackTraces();
        JdbcThreadLocalCollector.clear();

        return counts.entrySet().stream()
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        (entry) -> {
                            Map<String, Object> r = new HashMap<>(6);
                            final String query = entry.getKey();
                            r.put("count", entry.getValue().getCount());
                            r.put("average", entry.getValue().getAverage());
                            r.put("median", medians.get(query));
                            r.put("max", entry.getValue().getMax());
                            r.put("min", entry.getValue().getMin());
                            final List<List<StackTraceElement>> traces = stackTraces.get(query);
                            if (traces != null) {
                                final List<List<StackTraceElement>> filtered = traces.stream()
                                        .map(StackTraceHelper::filterStackTrace)
                                        .collect(Collectors.toList());
                                r.put("stackTraces", filtered);
                            }

                            if (sqlExecutionTimesEnabled) {
                                r.put("xeqTimes", executionTimes.get(query));
                            }
                            return r;
                        }));
    }

    @Override
    public List<String> getTags() {
        return Collections.singletonList(JDBC_LOGGING_KEY);
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public void setEnabled(boolean enabled) {
        // Do nothing
    }

    private Map<String, Object> stackTraceToJson(StackTraceElement element) {
        return ImmutableMap.<String, Object>builder()
                .put("class", element.getClassName())
                .put("method", element.getMethodName())
                .put("lineNumber", element.getLineNumber())
                .build();
    }
}
