package com.atlassian.jira.issue.views.util.csv;

import java.util.Date;

import javax.annotation.Nullable;

import com.atlassian.jira.datetime.DateTimeFormatter;
import com.atlassian.jira.datetime.DateTimeFormatterFactory;
import com.atlassian.jira.datetime.DateTimeStyle;

/**
 * @since 7.2.0
 */
public class CsvDateFormatterImpl implements CsvDateFormatter {
    private final DateTimeFormatterFactory formatterFactory;

    public CsvDateFormatterImpl(final DateTimeFormatterFactory formatterFactory) {
        this.formatterFactory = formatterFactory;
    }

    @Nullable
    public String formatDateTime(@Nullable Date date) {
        return format(formatterFactory.formatter().forLoggedInUser(), date);
    }

    @Nullable
    public String formatDate(@Nullable Date date) {
        return format(formatterFactory.formatter().forLoggedInUser().withSystemZone(), date);
    }

    private String format(DateTimeFormatter formatter, Date date) {
        return date != null ? formatter.withStyle(DateTimeStyle.COMPLETE).format(date) : null;
    }
}
