package com.atlassian.jira.util;

import com.atlassian.jira.user.ApplicationUser;

public interface GroupPermissionChecker {
    boolean hasViewGroupPermission(String group, ApplicationUser user);
}
