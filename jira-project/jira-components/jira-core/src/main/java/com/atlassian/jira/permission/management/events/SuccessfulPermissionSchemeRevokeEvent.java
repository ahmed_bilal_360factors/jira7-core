package com.atlassian.jira.permission.management.events;

import com.atlassian.analytics.api.annotations.EventName;
import com.atlassian.jira.event.AbstractEvent;
import com.google.common.base.Objects;

/**
 * Event fired when a revoke operation was successfully executed
 */
@EventName("jira.projectpermissions.revoke.success")
public final class SuccessfulPermissionSchemeRevokeEvent extends AbstractEvent {
    private final Long schemeId;
    private final String permissionKey; // will be a comma,separated,list if multiple permissions were sent
    private final Integer deletedCount;

    public SuccessfulPermissionSchemeRevokeEvent(
            final Long schemeId,
            final String permissionKey,
            final Integer deletedCount
    ) {
        this.schemeId = schemeId;
        this.permissionKey = permissionKey;
        this.deletedCount = deletedCount;
    }

    public Long getSchemeId() {
        return schemeId;
    }

    public String getPermissionKey() {
        return permissionKey;
    }

    public Integer getDeletedCount() {
        return deletedCount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SuccessfulPermissionSchemeRevokeEvent that = (SuccessfulPermissionSchemeRevokeEvent) o;
        return Objects.equal(schemeId, that.schemeId) &&
                Objects.equal(permissionKey, that.permissionKey) &&
                Objects.equal(deletedCount, that.deletedCount);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(super.hashCode(), schemeId, permissionKey, deletedCount);
    }

    @Override
    public String toString() {
        return "SuccessfulPermissionSchemeRevokeEvent{" +
                "schemeId=" + schemeId +
                ", permissionKey='" + permissionKey + '\'' +
                ", numberOfPermissions=" + deletedCount +
                '}';
    }
}
