package com.atlassian.jira.service.util;

import com.atlassian.jira.util.I18nHelper;
import com.atlassian.scheduler.cron.CronSyntaxException;
import com.atlassian.scheduler.cron.ErrorCode;
import com.google.common.collect.ImmutableMap;

import javax.annotation.Nonnull;
import java.util.Map;


public final class CronValidationErrorMappingUtil {
    private static final Map<ErrorCode, Mapper> MAPPERS = ImmutableMap.<ErrorCode, Mapper>builder()
            .put(ErrorCode.COMMA_WITH_LAST_DOM, simple("cron.expression.invalid.COMMA_WITH_LAST_DOM"))
            .put(ErrorCode.COMMA_WITH_LAST_DOW, simple("cron.expression.invalid.COMMA_WITH_LAST_DOW"))
            .put(ErrorCode.COMMA_WITH_NTH_DOW, simple("cron.expression.invalid.COMMA_WITH_NTH_DOW"))
            .put(ErrorCode.COMMA_WITH_WEEKDAY_DOM, simple("cron.expression.invalid.COMMA_WITH_WEEKDAY_DOM"))
            .put(ErrorCode.INTERNAL_PARSER_FAILURE, withArg("cron.expression.invalid.INTERNAL_PARSER_FAILURE"))
            .put(ErrorCode.ILLEGAL_CHARACTER, withArg("cron.expression.invalid.ILLEGAL_CHARACTER"))
            .put(ErrorCode.ILLEGAL_CHARACTER_AFTER_QM, withArg("cron.expression.invalid.ILLEGAL_CHARACTER_AFTER_QM"))
            .put(ErrorCode.ILLEGAL_CHARACTER_AFTER_HASH, simple("cron.expression.invalid.ILLEGAL_CHARACTER_AFTER_HASH"))
            .put(ErrorCode.ILLEGAL_CHARACTER_AFTER_INTERVAL, withArg("cron.expression.invalid.ILLEGAL_CHARACTER_AFTER_INTERVAL"))
            .put(ErrorCode.INVALID_STEP, simple("cron.expression.invalid.INVALID_STEP"))
            .put(ErrorCode.INVALID_STEP_DAY_OF_MONTH, withArg("cron.expression.invalid.INVALID_STEP_DAY_OF_MONTH"))
            .put(ErrorCode.INVALID_STEP_DAY_OF_WEEK, withArg("cron.expression.invalid.INVALID_STEP_DAY_OF_WEEK"))
            .put(ErrorCode.INVALID_STEP_HOUR, withArg("cron.expression.invalid.INVALID_STEP_HOUR"))
            .put(ErrorCode.INVALID_STEP_MONTH, withArg("cron.expression.invalid.INVALID_STEP_MONTH"))
            .put(ErrorCode.INVALID_STEP_SECOND_OR_MINUTE, withArg("cron.expression.invalid.INVALID_STEP_SECOND_OR_MINUTE"))
            .put(ErrorCode.INVALID_NAME, withArg("cron.expression.invalid.INVALID_NAME"))
            .put(ErrorCode.INVALID_NAME_FIELD, withArg("cron.expression.invalid.INVALID_NAME_FIELD"))
            .put(ErrorCode.INVALID_NAME_RANGE, simple("cron.expression.invalid.INVALID_NAME_RANGE"))
            .put(ErrorCode.INVALID_NAME_MONTH, withArg("cron.expression.invalid.INVALID_NAME_MONTH"))
            .put(ErrorCode.INVALID_NAME_DAY_OF_WEEK, withArg("cron.expression.invalid.INVALID_NAME_DAY_OF_WEEK"))
            .put(ErrorCode.INVALID_NUMBER_SEC_OR_MIN, simple("cron.expression.invalid.INVALID_NUMBER_SEC_OR_MIN"))
            .put(ErrorCode.INVALID_NUMBER_HOUR, simple("cron.expression.invalid.INVALID_NUMBER_HOUR"))
            .put(ErrorCode.INVALID_NUMBER_DAY_OF_MONTH, simple("cron.expression.invalid.INVALID_NUMBER_DAY_OF_MONTH"))
            .put(ErrorCode.INVALID_NUMBER_DAY_OF_MONTH_OFFSET, simple("cron.expression.invalid.INVALID_NUMBER_DAY_OF_MONTH_OFFSET"))
            .put(ErrorCode.INVALID_NUMBER_MONTH, simple("cron.expression.invalid.INVALID_NUMBER_MONTH"))
            .put(ErrorCode.INVALID_NUMBER_DAY_OF_WEEK, simple("cron.expression.invalid.INVALID_NUMBER_DAY_OF_WEEK"))
            .put(ErrorCode.INVALID_NUMBER_YEAR, simple("cron.expression.invalid.INVALID_NUMBER_YEAR"))
            .put(ErrorCode.INVALID_NUMBER_YEAR_RANGE, simple("cron.expression.invalid.INVALID_NUMBER_YEAR_RANGE"))
            .put(ErrorCode.QM_CANNOT_USE_HERE, simple("cron.expression.invalid.QM_CANNOT_USE_HERE"))
            .put(ErrorCode.QM_CANNOT_USE_FOR_BOTH_DAYS, simple("cron.expression.invalid.QM_CANNOT_USE_FOR_BOTH_DAYS"))
            .put(ErrorCode.QM_MUST_USE_FOR_ONE_OF_DAYS, simple("cron.expression.invalid.QM_MUST_USE_FOR_ONE_OF_DAYS"))
            .put(ErrorCode.UNEXPECTED_TOKEN_FLAG_L, simple("cron.expression.invalid.UNEXPECTED_TOKEN_FLAG_L"))
            .put(ErrorCode.UNEXPECTED_TOKEN_FLAG_W, simple("cron.expression.invalid.UNEXPECTED_TOKEN_FLAG_W"))
            .put(ErrorCode.UNEXPECTED_TOKEN_HASH, simple("cron.expression.invalid.UNEXPECTED_TOKEN_HASH"))
            .put(ErrorCode.UNEXPECTED_TOKEN_HYPHEN, simple("cron.expression.invalid.UNEXPECTED_TOKEN_HYPHEN"))
            .put(ErrorCode.UNEXPECTED_END_OF_EXPRESSION, simple("cron.expression.invalid.UNEXPECTED_END_OF_EXPRESSION"))
            .build();

    private CronValidationErrorMappingUtil() {

    }

    public static String internalError(@Nonnull final String message, @Nonnull I18nHelper i18nHelper) {
        return i18nHelper.getText("cron.expression.invalid.INTERNAL_PARSER_FAILURE", message);
    }

    public static String mapError(@Nonnull final CronSyntaxException cse, @Nonnull I18nHelper i18nHelper) {
        final Mapper mapper = MAPPERS.get(cse.getErrorCode());
        if (mapper != null) {
            return mapper.getText(i18nHelper, cse);
        }
        return cse.getMessage();
    }


    private static Mapper simple(final String messageKey) {
        return new Simple(messageKey);
    }

    private static Mapper withArg(final String messageKey) {
        return new WithArg(messageKey);
    }

    static interface Mapper {
        String getText(I18nHelper i18n, CronSyntaxException cse);
    }

    static class Simple implements Mapper {
        final String messageKey;

        Simple(final String messageKey) {
            this.messageKey = messageKey;
        }

        @Override
        public String getText(I18nHelper i18n, CronSyntaxException cse) {
            return i18n.getText(messageKey);
        }
    }

    static class WithArg extends Simple {
        WithArg(String messageKey) {
            super(messageKey);
        }

        @Override
        public String getText(I18nHelper i18n, CronSyntaxException cse) {
            return i18n.getText(messageKey, cse.getValue());
        }
    }
}
