package com.atlassian.jira.workflow.migration;

import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.issue.status.Status;
import com.atlassian.jira.task.TaskDescriptor;
import com.atlassian.jira.task.TaskProgressSink;
import com.atlassian.jira.web.action.admin.workflow.WorkflowMigrationResult;
import com.atlassian.jira.workflow.WorkflowException;
import com.atlassian.jira.workflow.WorkflowScheme;
import org.apache.log4j.Logger;
import org.ofbiz.core.entity.GenericEntityException;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.RejectedExecutionException;

/**
 * @since v5.2
 */
public interface WorkflowSchemeMigrationHelper<T extends WorkflowScheme> {
    List<IssueType> getTypesNeedingMigration();

    Collection<Status> getStatusesNeedingMigration(IssueType issueType);

    void addMapping(IssueType issueType, Status oldStatus, Status newStatus);

    boolean isHaveIssuesToMigrate() throws GenericEntityException;

    boolean doQuickMigrate() throws GenericEntityException;

    TaskDescriptor<WorkflowMigrationResult> migrateAsync() throws RejectedExecutionException;

    Logger getLogger();

    // Returns a collection of errors associated with issues in the workflow migration
    WorkflowMigrationResult migrate(TaskProgressSink sink) throws GenericEntityException, WorkflowException;

}
