package com.atlassian.jira.plugin.webfragment.conditions;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.mail.settings.MailSettings;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.mail.MailFactory;
import com.atlassian.mail.server.MailServerManager;
import com.google.common.annotations.VisibleForTesting;

/**
 * Only displays a {@link com.atlassian.plugin.web.descriptors.WebItemModuleDescriptor} if an SMTP mail server is
 * configured.
 *
 * @since v5.0
 */
public class SmtpMailServerConfiguredCondition extends AbstractWebCondition {
    public static boolean isOutgoingMailEnabled(final MailSettings mailSettings) {
        return isOutgoingMailEnabled(mailSettings, MailFactory.getServerManager());

    }

    @VisibleForTesting
    static boolean isOutgoingMailEnabled(final MailSettings mailSettings, final MailServerManager mailServerManager) {
        return !mailSettings.send().isDisabledViaApplicationProperty() &&
                mailServerManager.getDefaultSMTPMailServer() != null;
    }

    @Override
    public boolean shouldDisplay(final ApplicationUser user, final JiraHelper jiraHelper) {
        return SmtpMailServerConfiguredCondition.isOutgoingMailEnabled(getMailSettings());
    }

    MailSettings getMailSettings() {
        return ComponentAccessor.getComponent(MailSettings.class);
    }
}
