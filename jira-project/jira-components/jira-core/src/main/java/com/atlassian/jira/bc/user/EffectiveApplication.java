package com.atlassian.jira.bc.user;

import com.atlassian.application.api.ApplicationKey;
import com.google.common.base.Objects;
import org.codehaus.jackson.annotate.JsonAutoDetect;

import javax.annotation.Nonnull;

/**
 * Effective application bean
 *
 * @since v7.0
 */
@JsonAutoDetect
public final class EffectiveApplication {
    private final ApplicationKey key;
    private final String name;

    public EffectiveApplication(@Nonnull final ApplicationKey key, @Nonnull final String name) {
        this.key = key;
        this.name = name;
    }

    public String getKey() {
        return key.value();
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final EffectiveApplication that = (EffectiveApplication) o;
        return Objects.equal(key, that.key) &&
                Objects.equal(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(key, name);
    }

    @Override
    public String toString() {
        return "EffectiveApplication{" +
                "key=" + key +
                ", name='" + name + '\'' +
                '}';
    }
}
