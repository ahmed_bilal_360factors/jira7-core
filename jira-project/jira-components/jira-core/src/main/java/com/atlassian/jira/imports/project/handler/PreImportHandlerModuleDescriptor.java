package com.atlassian.jira.imports.project.handler;

import com.atlassian.jira.plugin.AbstractJiraModuleDescriptor;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.plugin.module.ModuleFactory;

/**
 * Module descriptor for Project Import Pre Import handlers.
 * These handlers will be run before the OfBiz and AO data is imported.
 *
 * @since v6.5
 */
public class PreImportHandlerModuleDescriptor extends AbstractJiraModuleDescriptor<PluggableImportRunnable> {
    public PreImportHandlerModuleDescriptor(final JiraAuthenticationContext authenticationContext, final ModuleFactory moduleFactory) {
        super(authenticationContext, moduleFactory);
    }
}
