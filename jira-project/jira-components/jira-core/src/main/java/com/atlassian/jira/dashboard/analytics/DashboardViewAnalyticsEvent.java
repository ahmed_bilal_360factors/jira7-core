package com.atlassian.jira.dashboard.analytics;

import com.atlassian.analytics.api.annotations.EventName;

import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * Published when the dashboard page is viewed.
 */
@EventName("jira.dashboard.view")
public class DashboardViewAnalyticsEvent {
    private final Long dashboardId;
    private final Long shownItemsCount;
    private final Long hiddenItemsCount;

    public DashboardViewAnalyticsEvent(Long dashboardId, Long shownItemsCount, Long hiddenItemsCount) {
        this.dashboardId = dashboardId;
        this.shownItemsCount = notNull(shownItemsCount);
        this.hiddenItemsCount = notNull(hiddenItemsCount);
    }

    /**
     * @return the dashboard ID
     */
    public Long getDashboardId() {
        return dashboardId;
    }

    /**
     * @return the number of dashboard items which were rendered on the dashboard.
     */
    public Long getShownItemsCount() {
        return shownItemsCount;
    }

    /**
     * @return the number of dashboard items which were not rendered as they were hidden from the dashboard.
     */
    public Long getHiddenItemsCount() {
        return hiddenItemsCount;
    }
}
