package com.atlassian.jira.crowd.embedded.ofbiz;

import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.OperationType;
import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.embedded.spi.DirectoryDao;
import com.atlassian.crowd.model.application.ApplicationImpl;
import com.atlassian.crowd.model.application.ApplicationType;
import com.atlassian.crowd.model.application.DirectoryMapping;
import com.atlassian.crowd.model.application.RemoteAddress;
import com.atlassian.jira.crowd.embedded.ofbiz.db.OfBizHelper;
import com.atlassian.jira.util.dbc.Assertions;
import org.apache.commons.lang.BooleanUtils;
import org.ofbiz.core.entity.GenericValue;

import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumSet;
import java.util.List;

import static com.atlassian.jira.crowd.embedded.ofbiz.ApplicationEntity.ACTIVE;
import static com.atlassian.jira.crowd.embedded.ofbiz.ApplicationEntity.APPLICATION_ID;
import static com.atlassian.jira.crowd.embedded.ofbiz.ApplicationEntity.APPLICATION_TYPE;
import static com.atlassian.jira.crowd.embedded.ofbiz.ApplicationEntity.CREATED_DATE;
import static com.atlassian.jira.crowd.embedded.ofbiz.ApplicationEntity.DESCRIPTION;
import static com.atlassian.jira.crowd.embedded.ofbiz.ApplicationEntity.NAME;
import static com.atlassian.jira.crowd.embedded.ofbiz.ApplicationEntity.UPDATED_DATE;

public class OfBizApplication extends ApplicationImpl {
    private DirectoryDao directoryDao;
    private static final EnumSet<OperationType> ALLOWED_OPERATIONS = EnumSet.allOf(OperationType.class);

    private OfBizApplication(final GenericValue applicationGenericValue, final List<GenericValue> remoteAddressGenericValue) {
        Assertions.notNull(applicationGenericValue);
        id = applicationGenericValue.getLong(APPLICATION_ID);
        setName(applicationGenericValue.getString(NAME));
        setCredential(new PasswordCredential(applicationGenericValue.getString(ApplicationEntity.CREDENTIAL), true));
        setType(ApplicationType.valueOf(applicationGenericValue.getString(APPLICATION_TYPE)));
        active = BooleanUtils.toBoolean(applicationGenericValue.getInteger(ACTIVE));
        createdDate = OfBizHelper.convertToUtilDate(applicationGenericValue.getTimestamp(CREATED_DATE));
        updatedDate = OfBizHelper.convertToUtilDate(applicationGenericValue.getTimestamp(UPDATED_DATE));

        setDescription(applicationGenericValue.getString(DESCRIPTION));

        if (remoteAddressGenericValue != null) {
            setRemoteAddresses(RemoteAddressEntity.toRemoteAddresses(remoteAddressGenericValue));
        } else {
            setRemoteAddresses(Collections.<RemoteAddress>emptySet());
        }
    }

    static OfBizApplication from(final GenericValue genericValue, final List<GenericValue> remoteAddressGenericValue) {
        return new OfBizApplication(Assertions.notNull(genericValue), remoteAddressGenericValue);
    }

    public List<DirectoryMapping> getDirectoryMappings() {
        List<Directory> directories = directoryDao.findAll();
        List<DirectoryMapping> mappings = new ArrayList<DirectoryMapping>(directories.size());
        for (Directory directory : directories) {
            mappings.add(new DirectoryMapping(OfBizApplication.this, directory, true, ALLOWED_OPERATIONS));
        }
        return Collections.unmodifiableList(mappings);
    }

    public DirectoryMapping getDirectoryMapping(long directoryId) {
        List<Directory> directories = directoryDao.findAll();
        for (Directory directory : directories) {
            if (directory.getId().equals(directoryId)) {
                return new DirectoryMapping(OfBizApplication.this, directory, true, ALLOWED_OPERATIONS);
            }
        }

        return null;
    }

    void setDirectoryDao(final OfBizDirectoryDao directoryDao) {
        this.directoryDao = directoryDao;
    }
}
