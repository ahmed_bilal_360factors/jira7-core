package com.atlassian.jira.avatar;

import com.atlassian.jira.config.util.JiraHome;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageTypeSpecifier;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.metadata.IIOInvalidTreeException;
import javax.imageio.metadata.IIOMetadata;
import javax.imageio.metadata.IIOMetadataNode;
import javax.imageio.stream.FileImageOutputStream;
import javax.imageio.stream.ImageOutputStream;
import javax.imageio.stream.MemoryCacheImageOutputStream;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.NoSuchElementException;

import static javax.imageio.ImageTypeSpecifier.createFromBufferedImageType;
import static org.apache.commons.io.FilenameUtils.removeExtension;

/**
 * Saves an image as a png with metadata signifying this image is a JIRA Avatar (used by the email handler to decide
 * whether or not to attach an image)
 *
 * @since v6.1
 */
public class AvatarTaggerImpl implements AvatarTagger {
    public static final String FORMAT_NAME = "javax_imageio_png_1.0";
    private static final String AVATAR_DIRECTORY = "data/avatars";
    private static final String TAGGED_AVATAR_FILE_SUFFIX = "jrvtg.png";
    private final JiraHome jiraHome;

    public AvatarTaggerImpl(final JiraHome jiraHome) {
        this.jiraHome = jiraHome;
    }

    @Override
    public String tagAvatar(final long id, final String filename) throws IOException {
        final File base = getAvatarBaseDirectory();
        for (Avatar.Size size : Avatar.Size.values()) {
            final File avatarFileInstance = new File(base, AvatarFilenames.getAvatarFilename(id, filename, size));
            if (avatarFileInstance.exists()) {
                tagSingleAvatarFile(avatarFileInstance, new File(toTaggedName(avatarFileInstance.getAbsolutePath())));
            }
        }

        // Don't delete the old files until we're done, in case we can't tag all versions of the files
        try {
            for (Avatar.Size size : Avatar.Size.values()) {
                final File oldAvatarFile = new File(base, AvatarFilenames.getAvatarFilename(id, filename, size));
                if (oldAvatarFile.exists()) {
                    oldAvatarFile.delete();
                }
            }
        } catch (SecurityException ignored) {
        }

        return toTaggedName(filename);
    }

    @Override
    public void tagSingleAvatarFile(final File source, final File destination) throws IOException {
        final BufferedImage in = ImageIO.read(source);
        final FileImageOutputStream outputStream = new FileImageOutputStream(destination);
        write(in, outputStream);
    }

    @Override
    public void tag(final InputStream inputStream, final OutputStream outputStream) throws IOException {
        final BufferedImage in = ImageIO.read(ImageIO.createImageInputStream(inputStream));
        final ImageOutputStream out = ImageIO.createImageOutputStream(outputStream);
        write(in, out);
    }

    @Override
    public void saveTaggedAvatar(final RenderedImage in, final String format, final OutputStream targetStream) throws IOException {
        try {
            write(in, new MemoryCacheImageOutputStream(targetStream), format);
        } catch (NoSuchElementException x) {
            throw new IllegalArgumentException("format: '" + format + "'");
        }
    }

    @Override
    public void saveTaggedAvatar(final RenderedImage in, final String name, final File file) throws IOException {
        try (FileImageOutputStream stream = new FileImageOutputStream(file)) {
            write(in, stream);
        }
    }

    private void write(final RenderedImage in, final Object output) throws IOException {
        write(in, output, "png");
    }

    private void write(final RenderedImage in, final Object output, final String format) throws IOException {
        final ImageWriter writer = ImageIO.getImageWritersByFormatName(format).next();

        final ImageWriteParam writeParam = writer.getDefaultWriteParam();
        final IIOMetadata metadata = metadata(writer, writeParam);

        writer.setOutput(output);
        writer.write(metadata, new IIOImage(in, null, metadata), writeParam);
    }

    private File getAvatarBaseDirectory() {
        return new File(jiraHome.getHome(), AVATAR_DIRECTORY);
    }

    private static String toTaggedName(final String filename) {
        return removeExtension(filename) + TAGGED_AVATAR_FILE_SUFFIX;
    }

    private static IIOMetadata metadata(final ImageWriter writer, final ImageWriteParam writeParam) throws IIOInvalidTreeException {
        final ImageTypeSpecifier typeSpecifier = createFromBufferedImageType(BufferedImage.TYPE_INT_RGB);
        final IIOMetadata metadata = writer.getDefaultImageMetadata(typeSpecifier, writeParam);

        // Create metadata
        final IIOMetadataNode root = new IIOMetadataNode(FORMAT_NAME);
        final IIOMetadataNode text = new IIOMetadataNode("tEXt");
        final IIOMetadataNode textEntry = new IIOMetadataNode("tEXtEntry");
        textEntry.setAttribute("keyword", JIRA_SYSTEM_IMAGE_TYPE);
        textEntry.setAttribute("value", AVATAR_SYSTEM_IMAGE_TYPE);
        textEntry.setAttribute("encoding", "UTF-8");
        textEntry.setAttribute("language", "EN");
        textEntry.setAttribute("compression", "none");

        text.appendChild(textEntry);
        root.appendChild(text);
        metadata.mergeTree(FORMAT_NAME, root);
        return metadata;
    }
}
