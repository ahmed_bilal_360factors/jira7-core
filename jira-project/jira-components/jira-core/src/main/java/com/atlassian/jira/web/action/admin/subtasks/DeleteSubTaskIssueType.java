package com.atlassian.jira.web.action.admin.subtasks;

import com.atlassian.jira.config.IssueTypeService;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.security.xsrf.RequiresXsrfCheck;
import com.atlassian.jira.web.action.admin.issuetypes.DeleteIssueType;
import com.atlassian.sal.api.websudo.WebSudoRequired;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

import java.util.Collection;

@WebSudoRequired
public class DeleteSubTaskIssueType extends DeleteIssueType {
    private final IssueTypeService issueTypeService;

    public DeleteSubTaskIssueType(IssueTypeService issueTypeService) {
        super(issueTypeService);
        this.issueTypeService = issueTypeService;
    }

    @Override
    @RequiresXsrfCheck
    protected String doExecute() throws Exception {
        return super.doExecute();
    }

    protected String getNiceConstantName() {
        return getText("admin.issue.constant.subtask.issuetype.lowercase");
    }

    protected String getRedirectPage() {
        return "ManageSubTasks.jspa";
    }

    protected Collection<IssueType> getConstants() {
        return Lists.newArrayList(Iterables.filter(issueTypeService.getIssueTypes(getLoggedInUser()), new Predicate<IssueType>() {
            @Override
            public boolean apply(final IssueType issueType) {
                return issueType.isSubTask();
            }
        }));
    }
}
