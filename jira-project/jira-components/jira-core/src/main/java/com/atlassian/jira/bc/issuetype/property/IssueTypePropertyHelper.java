package com.atlassian.jira.bc.issuetype.property;

import com.atlassian.fugue.Function2;
import com.atlassian.fugue.Option;
import com.atlassian.jira.config.IssueTypeManager;
import com.atlassian.jira.config.IssueTypePermissionService;
import com.atlassian.jira.entity.property.EntityProperty;
import com.atlassian.jira.entity.property.EntityPropertyHelper;
import com.atlassian.jira.entity.property.EntityPropertyType;
import com.atlassian.jira.event.entity.EntityPropertyDeletedEvent;
import com.atlassian.jira.event.entity.EntityPropertySetEvent;
import com.atlassian.jira.event.issuetype.property.IssueTypePropertyDeletedEvent;
import com.atlassian.jira.event.issuetype.property.IssueTypePropertySetEvent;
import com.atlassian.jira.issue.issuetype.IssueTypeWithID;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.google.common.base.Function;

import static com.atlassian.jira.util.ErrorCollection.Reason.FORBIDDEN;
import static com.atlassian.jira.util.ErrorCollection.Reason.NOT_FOUND;

/**
 * @since 7.0
 */
public class IssueTypePropertyHelper implements EntityPropertyHelper<IssueTypeWithID> {
    private final IssueTypeManager issueTypeManager;
    private final IssueTypePermissionService issueTypePermissionService;
    private final I18nHelper i18n;
    private final CheckPermissionFunction<IssueTypeWithID> hasEditPermissionFunction = new CheckPermissionFunction<IssueTypeWithID>() {
        @Override
        public ErrorCollection apply(final ApplicationUser applicationUser, final IssueTypeWithID issueTypeWithID) {
            if (issueTypePermissionService.hasPermissionToEditIssueType(applicationUser)) {
                return noError();
            }
            ErrorCollection canViewResult = hasViewPermissionFunction.apply(applicationUser, issueTypeWithID);

            if (canViewResult.hasAnyErrors()) {
                return canViewResult;
            }
            return new SimpleErrorCollection(i18n.getText("rest.authorization.admin.required"), FORBIDDEN);
        }
    };

    private final CheckPermissionFunction<IssueTypeWithID> hasViewPermissionFunction = new CheckPermissionFunction<IssueTypeWithID>() {
        @Override
        public ErrorCollection apply(final ApplicationUser applicationUser, final IssueTypeWithID issueTypeWithID) {
            if (issueTypePermissionService.hasPermissionToViewIssueType(applicationUser, issueTypeWithID.getId().toString())) {
                return noError();
            }
            return new SimpleErrorCollection(i18n.getText("admin.error.issue.type.get.not.exist"), NOT_FOUND);
        }
    };
    private final Function<Long, Option<IssueTypeWithID>> entityByIdFunction = new Function<Long, Option<IssueTypeWithID>>() {
        @Override
        public Option<IssueTypeWithID> apply(Long id) {
            return Option.option(IssueTypeWithID.fromIssueType(issueTypeManager.getIssueType(String.valueOf(id))));
        }
    };
    private final Function2<ApplicationUser, EntityProperty, IssueTypePropertySetEvent> setPropertyEventFunction = new Function2<ApplicationUser, EntityProperty, IssueTypePropertySetEvent>() {
        @Override
        public IssueTypePropertySetEvent apply(final ApplicationUser user, final EntityProperty entityProperty) {
            return new IssueTypePropertySetEvent(entityProperty, user);
        }
    };
    private final Function2<ApplicationUser, EntityProperty, IssueTypePropertyDeletedEvent> deletePropertyEventFunction = new Function2<ApplicationUser, EntityProperty, IssueTypePropertyDeletedEvent>() {
        @Override
        public IssueTypePropertyDeletedEvent apply(final ApplicationUser user, final EntityProperty entityProperty) {
            return new IssueTypePropertyDeletedEvent(entityProperty, user);
        }
    };

    public IssueTypePropertyHelper(IssueTypeManager issueTypeManager, IssueTypePermissionService issueTypePermissionService, I18nHelper i18n) {
        this.issueTypeManager = issueTypeManager;
        this.issueTypePermissionService = issueTypePermissionService;
        this.i18n = i18n;
    }

    @Override
    public CheckPermissionFunction<IssueTypeWithID> hasEditPermissionFunction() {
        return hasEditPermissionFunction;
    }

    @Override
    public CheckPermissionFunction<IssueTypeWithID> hasReadPermissionFunction() {
        return hasViewPermissionFunction;
    }

    @Override
    public Function<Long, Option<IssueTypeWithID>> getEntityByIdFunction() {
        return entityByIdFunction;
    }

    @Override
    public Function2<ApplicationUser, EntityProperty, ? extends EntityPropertySetEvent> createSetPropertyEventFunction() {
        return setPropertyEventFunction;
    }

    @Override
    public Function2<ApplicationUser, EntityProperty, ? extends EntityPropertyDeletedEvent> createDeletePropertyEventFunction() {
        return deletePropertyEventFunction;
    }

    @Override
    public EntityPropertyType getEntityPropertyType() {
        return EntityPropertyType.ISSUE_TYPE_PROPERTY;
    }

    private SimpleErrorCollection noError() {
        return new SimpleErrorCollection();
    }
}
