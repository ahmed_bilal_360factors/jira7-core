package com.atlassian.jira.issue.context.persistence;

import com.atlassian.cache.CacheManager;
import com.atlassian.cache.CachedReference;
import com.atlassian.jira.issue.context.JiraContextNode;
import com.atlassian.jira.issue.context.ProjectContext;
import com.atlassian.jira.issue.fields.config.FieldConfigScheme;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.util.collect.MapBuilder;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import org.ofbiz.core.entity.GenericValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static com.atlassian.jira.util.collect.MapBuilder.build;

/**
 * Used by CachingFieldConfigContextPersister to do real DB work.
 */
public class FieldConfigContextPersisterWorker {
    public static final String ENTITY_TABLE_NAME = "ConfigurationContext";

    public static final String ENTITY_PROJECT = JiraContextNode.FIELD_PROJECT;

    public static final String ENTITY_KEY = "key";
    public static final String ENTITY_SCHEME_ID = "fieldconfigscheme";

    private final OfBizDelegator delegator;
    private final ProjectManager projectManager;

    private static final Logger log = LoggerFactory.getLogger(FieldConfigContextPersisterWorker.class);
    private final CachedReference<Multimap<Long, GenericValue>> configContextsBySchemeId;

    public FieldConfigContextPersisterWorker(final OfBizDelegator delegator, final ProjectManager projectManager, final CacheManager cacheManager) {
        this.delegator = delegator;
        this.projectManager = projectManager;
        this.configContextsBySchemeId = cacheManager.getCachedReference(getClass().getName() + ".configContextsBySchemeId",
                this::loadConfigContext);
    }

    @Nonnull
    public List<JiraContextNode> getAllContextsForConfigScheme(@Nonnull final FieldConfigScheme fieldConfigScheme) {
        return configContextsBySchemeId.get().get(fieldConfigScheme.getId()).stream()
                .filter(this::filterGenericValues)
                .map(this::transformToDomainObject)
                .collect(Collectors.toList());
    }

    public void removeContextsForConfigScheme(@Nonnull final FieldConfigScheme fieldConfigScheme) {
        final int result = delegator.removeByAnd(ENTITY_TABLE_NAME, build(ENTITY_SCHEME_ID, fieldConfigScheme.getId()));
        invalidateAll();
        log.debug("{} contexts deleted for field config scheme with id '{}'", result, fieldConfigScheme.getId());
    }

    public void removeContextsForProject(@Nonnull final Project project) {
        final int result = delegator.removeByAnd(ENTITY_TABLE_NAME, build(ENTITY_PROJECT, project.getId()));
        invalidateAll();
        log.debug("{} contexts deleted for {}", result, project);
    }

    /**
     * Returns a Long object representing the id of the FieldConfigScheme
     *
     * @param context the context
     * @param key     the database key
     */
    @Nullable
    public Object retrieve(@Nullable final JiraContextNode context, final String key) {
        if (context != null) {
            final List<GenericValue> result = delegator.findByAnd(ENTITY_TABLE_NAME, transformToFieldsMap(context).add(ENTITY_KEY,
                    key).toMap());
            if ((result != null) && !result.isEmpty()) {
                final Long schemeId = result.iterator().next().getLong(ENTITY_SCHEME_ID);
                if (result.size() > 1) {
                    log.warn("More than one FieldConfigScheme returned for a given context. Database may be corrupted."
                            + "Returning first Long: {}. Context: {} with key: {} returned {}.", schemeId, context, key, result);
                }
                return schemeId;
            }
        }
        return null;
    }

    public void store(final String fieldId, final JiraContextNode contextNode, final FieldConfigScheme fieldConfigScheme) {
        if (retrieve(contextNode, fieldId) != null) {
            remove(contextNode, fieldId);
        }

        // if fieldConfigScheme==null just remove it
        if (fieldConfigScheme != null) {
            final MapBuilder<String, Object> props = transformToFieldsMap(contextNode);
            props.add(ENTITY_KEY, fieldId);
            props.add(ENTITY_SCHEME_ID, fieldConfigScheme.getId());
            delegator.createValue(ENTITY_TABLE_NAME, props.toMap());
        }
        invalidateAll();
    }

    public void remove(@Nullable final JiraContextNode context, @Nullable final String key) {
        if ((context != null) && (key != null)) {
            delegator.removeByAnd(ENTITY_TABLE_NAME, transformToFieldsMap(context).add(ENTITY_KEY, key).toMap());
            invalidateAll();
        } else {
            log.warn("Context or key was null. Nothing was removed");
        }
    }

    void invalidateAll() {
        configContextsBySchemeId.reset();
    }

    private MapBuilder<String, Object> transformToFieldsMap(final JiraContextNode contextNode) {
        return MapBuilder.newBuilder(contextNode.appendToParamsMap(Collections.<String, Object>emptyMap()));
    }

    private JiraContextNode transformToDomainObject(final GenericValue contextAsGv) {
        return new ProjectContext(contextAsGv.getLong(ENTITY_PROJECT), projectManager);
    }

    private boolean filterGenericValues(@Nonnull final GenericValue contextAsGv) {
        final Long projectId = contextAsGv.getLong(ENTITY_PROJECT);
        return projectId == null || projectManager.getProjectObj(projectId) != null;
    }

    private Multimap<Long, GenericValue> loadConfigContext() {
        final List<GenericValue> genericValues = delegator.findAll(ENTITY_TABLE_NAME);
        final Multimap<Long, GenericValue> cacheValues = ArrayListMultimap.create();
        for (GenericValue genericValue : genericValues) {
            cacheValues.put(genericValue.getLong(ENTITY_SCHEME_ID), genericValue);
        }
        return cacheValues;
    }
}
