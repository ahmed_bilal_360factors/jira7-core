package com.atlassian.jira.bc.filter;

import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.webresource.api.assembler.RequiredResources;

/**
 * Generates HTML for filter deletion confirmation dialog. The HTML may contain one or more warning messages. Some examples:
 * warning when the filter to be deleted has one or more subscriptions against it, warning when one or more users (other than
 * the current user) have favourited the filter.
 * 
 * Other filter consumers can also provide warnings by declaring a web panel at a predefined location.
 *
 * <pre>
 * *******************************
 * This is in the DMZ; search for usages before removing it. The Issue Navigator plugin is one known user.
 * *******************************
 * </pre>
 */
public interface FilterDeletionWarningViewProvider {
    /**
     * Generates the HTML view for filter deletion confirmation dialog. This method grabs all warning messages
     * from various filter consumers. 
     *  
     * @param searchRequest The filter that is about to be deleted
     * @return HTML view
     */
    String getWarningHtml(SearchRequest searchRequest);

    /**
     * Includes required web resources to style the dialog HTML.
     *
     * @param requiredResources
     */
    void requireDefaultStyle(RequiredResources requiredResources);
}
