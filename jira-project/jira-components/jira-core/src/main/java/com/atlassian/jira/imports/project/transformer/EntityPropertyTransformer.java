package com.atlassian.jira.imports.project.transformer;

import com.atlassian.jira.external.beans.ExternalEntityProperty;
import com.atlassian.jira.imports.project.core.EntityRepresentation;

/**
 * Converts the object representation into {@link com.atlassian.jira.imports.project.core.EntityRepresentation}.
 *
 * @since v7.1
 */
public interface EntityPropertyTransformer {
    /**
     * Gets an EntityRepresentation that contains the correct attributes based on the populated fields in the provided
     * entity property.
     *
     * @param entityProperty contains the populated fields that will end up in the EntityRepresentations map
     * @param newEntityId    new id for external entity
     * @return an EntityRepresentation that can be persisted using OfBiz
     */
    EntityRepresentation getEntityRepresentation(ExternalEntityProperty entityProperty, Long newEntityId);
}
