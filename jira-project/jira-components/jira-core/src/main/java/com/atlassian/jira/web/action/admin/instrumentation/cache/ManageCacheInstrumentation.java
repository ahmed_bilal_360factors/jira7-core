package com.atlassian.jira.web.action.admin.instrumentation.cache;

import com.atlassian.cache.CacheManager;
import com.atlassian.jira.instrumentation.InstrumentationListenerManager;
import com.atlassian.jira.instrumentation.InstrumentationLogger;
import com.atlassian.jira.security.xsrf.RequiresXsrfCheck;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.sal.api.websudo.WebSudoRequired;

import javax.annotation.Nonnull;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Manage the caches that we are instrumenting and manage where the data ends up.
 *
 * @since v7.1
 */
@WebSudoRequired
public class ManageCacheInstrumentation extends JiraWebActionSupport {
    private final InstrumentationListenerManager listenerManager;
    private final CacheManager cacheManager;
    private final InstrumentationLogger instrumentationlogger;

    public ManageCacheInstrumentation(CacheManager cacheManager, InstrumentationListenerManager listenerManager,
                                      InstrumentationLogger instrumentationLogger) {
        this.cacheManager = cacheManager;
        this.listenerManager = listenerManager;
        this.instrumentationlogger = instrumentationLogger;
    }

    /**
     * Creates the list of diplayable caches
     *
     * @return The list of caches.
     */
    public List<CacheManagementBean> getCaches() {
        return cacheManager.getManagedCaches().stream()
                .map(cache -> new CacheManagementBean(cache.getName()))
                .collect(Collectors.toList());
    }

    public String getBufferCount() {
        return String.valueOf(instrumentationlogger.getLogEntriesFromBuffer().size());
    }


    @RequiresXsrfCheck
    public String doClearBuffer() {
        instrumentationlogger.clearMemoryBuffer();
        return SUCCESS;
    }

    @RequiresXsrfCheck
    public String doEnableAll() {
        listenerManager.applyToAllListeners(r -> r.setEnabled(true));
        return SUCCESS;
    }

    @RequiresXsrfCheck
    public String doDisableAll() {
        listenerManager.applyToAllListeners(r -> r.setEnabled(false));
        return SUCCESS;
    }

    class CacheManagementBean {
        private final String name;
        private boolean enabled;

        @SuppressWarnings("ConstantConditions")
        @Nonnull
        public CacheManagementBean(String name) {
            this.name = name;
            this.enabled = cacheManager.getManagedCache(name).isStatisticsEnabled();
        }

        public String getName() {
            return name;
        }

        public boolean isEnabled() {
            return enabled;
        }
    }
}
