package com.atlassian.jira.project;

import com.atlassian.jira.bc.project.component.ProjectComponent;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.exception.DataAccessException;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static com.atlassian.jira.permission.ProjectPermissions.ASSIGNABLE_USER;

public abstract class AbstractProjectManager implements ProjectManager {
    private static final Logger log = LoggerFactory.getLogger(AbstractProjectManager.class);
    final UserManager userManager;
    private final ApplicationProperties applicationProperties;

    public AbstractProjectManager(final UserManager userManager, ApplicationProperties applicationProperties) {
        this.userManager = userManager;
        this.applicationProperties = applicationProperties;
    }

    // Business Methods ------------------------------------------------------------------------------------------------
    // Create Methods --------------------------------------------------------------------------------------------------

    // Get / Finder Methods --------------------------------------------------------------------------------------------
    @Override
    public Project getProjectByCurrentKey(final String projectKey) {
        return getProjectObjByKey(projectKey);
    }

    @Override
    public ProjectCategory getProjectCategoryObjectByName(final String projectCategoryName) throws DataAccessException {
        if (projectCategoryName == null) {
            return null;
        }
        // The production version has all ProjectCategorys cached, so this is cool.
        for (ProjectCategory projectCategory : getAllProjectCategories()) {
            if (projectCategoryName.equals(projectCategory.getName())) {
                return projectCategory;
            }
        }
        return null;
    }

    @Override
    public ProjectCategory getProjectCategoryObjectByNameIgnoreCase(final String projectCategoryName) throws DataAccessException {
        if (projectCategoryName == null) {
            return null;
        }
        // The production version has all ProjectCategorys cached, so this is cool.
        for (ProjectCategory projectCategory : getAllProjectCategories()) {
            if (projectCategoryName.equalsIgnoreCase(projectCategory.getName())) {
                return projectCategory;
            }
        }
        return null;
    }

    @Override
    public List<Project> convertToProjectObjects(final Collection<Long> projectIds) {
        if (projectIds == null) {
            return null;
        }
        final List<Project> projects = new ArrayList<Project>(projectIds.size());
        for (final Long id : projectIds) {
            final Project project = getProjectObj(id);
            if (project != null) {
                projects.add(project);
            }
        }
        return projects;
    }

    public ApplicationUser getDefaultAssignee(Project project, ProjectComponent component) {
        if (component == null) {
            return getDefaultAssignee(project, Collections.emptyList());
        } else {
            return getDefaultAssignee(project, Collections.singleton(component));
        }
    }

    public ApplicationUser getDefaultAssignee(Project project, Collection<ProjectComponent> components) throws DefaultAssigneeException {
        ApplicationUser defaultAssignee = getConfiguredDefaultAssignee(project, components);
        // now check if this is a valid default assignee for this project:
        if (defaultAssignee == null) {
            // are unassigned issues allowed?
            if (applicationProperties.getOption(APKeys.JIRA_OPTION_ALLOWUNASSIGNED)) {
                return null;
            } else {
                throw new DefaultAssigneeException("Invalid default assignee for project '" + project.getKey() +
                        "'. Unassigned issues not allowed.");
            }
        } else {
            // User is assignable check is done in getConfiguredDefaultAssignee()
            return defaultAssignee;
        }
    }

    private ApplicationUser getConfiguredDefaultAssignee(Project project, Collection<ProjectComponent> components) throws DefaultAssigneeException {
        boolean useProjectLead = false;
        boolean useUnassigned = false;
        // Loop over the components and try to find the most specific default assignee setting:
        for (ProjectComponent component : components) {
            long assigneeType = component.getAssigneeType();
            if (assigneeType == AssigneeTypes.COMPONENT_LEAD) {
                // Component Lead is the most specific of all settings
                try {
                    return getDefaultAssignee(project, component.getLead());
                } catch (InvalidAssigneeException ex) {
                    // Log the error and try another default assignee - we really want to create the issue if we can.
                    log.warn("Unable to assign default assignee for " + project.getKey() + " component '" + component.getName() + "'. " + ex.getMessage());
                }
            } else if (assigneeType == AssigneeTypes.PROJECT_LEAD) {
                useProjectLead = true;
            } else if (assigneeType == AssigneeTypes.UNASSIGNED) {
                useUnassigned = true;
            }
        }
        // No components that default assignee to Component Lead were found - check for other default settings
        if (useProjectLead) {
            // At least one component specified to use "Project Lead"
            return useProjectLeadAsDefaultAssignee(project);
        } else if (useUnassigned) {
            // At least one component specified to use Unassigned
            return null;
        } else {
            // Use the Project Default
            Long projectAssigneeType = project.getAssigneeType();
            if (projectAssigneeType != null && projectAssigneeType.longValue() == AssigneeTypes.UNASSIGNED) {
                return null;
            } else {
                return useProjectLeadAsDefaultAssignee(project);
            }
        }
    }

    private ApplicationUser useProjectLeadAsDefaultAssignee(Project project) {
        try {
            return getDefaultAssignee(project, project.getLeadUserKey());
        } catch (InvalidAssigneeException ex) {
            // Project Lead is not found or not assignable
            log.warn("Unable to assign default assignee for project " + project.getKey() + ". " + ex.getMessage());
            throw new DefaultAssigneeException(ex.getMessage());
        }
    }

    /**
     * Finds the user for the given username or throws DefaultAssigneeException if not found.
     *
     * @param userkey userkey
     * @return the User for the given username
     * @throws DefaultAssigneeException if the user with the given username is not found in the system.
     */
    private ApplicationUser getDefaultAssignee(Project project, String userkey) throws InvalidAssigneeException {
        if (userkey == null) {
            throw new InvalidAssigneeException("Lead user not configured.");
        }
        ApplicationUser assignee = userManager.getUserByKey(userkey);
        if (assignee == null) {
            throw new InvalidAssigneeException("Cannot find user '" + userkey + "'.");
        }
        if (isUserAssignable(project, assignee)) {
            return assignee;
        } else {
            throw new InvalidAssigneeException("User '" + userkey + "' does not have assign permission.");
        }
    }

    private static boolean isUserAssignable(Project project, ApplicationUser user) {
        return ComponentAccessor.getPermissionManager().hasPermission(ASSIGNABLE_USER, project, user);
    }

    public Project updateProject(Project updatedProject, String name, String description, String lead, String url, Long assigneeType) {
        return updateProject(updatedProject, name, description, lead, url, assigneeType, null);
    }

    public Project updateProject(Project updatedProject, String name, String description, String lead, String url, Long assigneeType, Long avatarId) {
        return updateProject(updatedProject, name, description, lead, url, assigneeType, avatarId, null);
    }

    private class InvalidAssigneeException extends Exception {
        public InvalidAssigneeException(String message) {
            super(message);
        }
    }
}
