package com.atlassian.jira.web.action.issue.bulkedit;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.json.marshal.Jsonable;
import com.atlassian.webresource.api.data.WebResourceDataProvider;
import com.google.common.collect.ImmutableList;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.map.ObjectMapper;

import java.util.Collection;
import java.util.stream.Collectors;

/**
 * Returns all subtask ids that are found in this instance of JIRA.
 *
 * @since v7.0
 */
public class SubtaskIdDataProvider implements WebResourceDataProvider {
    private static ObjectMapper OBJECT_MAPPER;

    static {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(JsonGenerator.Feature.AUTO_CLOSE_TARGET, false);
        OBJECT_MAPPER = mapper;
    }

    public SubtaskIdDataProvider() {

    }

    @Override
    public Jsonable get() {
        ConstantsManager constantsManager = ComponentAccessor.getConstantsManager();
        return writer -> {
            Collection<String> subTaskIds = constantsManager != null ? constantsManager.getSubTaskIssueTypeObjects().stream().map(it -> it.getId()).collect(Collectors.toList()) : ImmutableList.of();
            OBJECT_MAPPER.writeValue(writer, subTaskIds);
        };
    }
}
