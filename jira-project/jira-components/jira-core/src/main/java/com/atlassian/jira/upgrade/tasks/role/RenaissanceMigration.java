package com.atlassian.jira.upgrade.tasks.role;

/**
 * Performs renaissance migration (i.e. JIRA 6.x single use permission to JIRA 7.x multiple application). This
 * involves:
 * <ol>
 * <li>Moving JIRA 6.x licenses into the new JIRA 7.0 store.</li>
 * <li>Moving JIRA 6.x users into their associated JIRA 7.0 products.</li>
 * </ol>
 * NOTE: This class it written to access the database as it was in JIRA 6.x and should not be updated to reflect
 * changes that happen after this point.
 *
 * @see Move6xUsePermissionOverToCoreAndSoftwareApplications
 * @see Move6xServiceDeskLicenseTo70Store
 * @see Move6xLicenseTo70Store
 * @since v7.0
 */
public interface RenaissanceMigration {
    /**
     * Executes all migration tasks and validates the result. Does not apply any changes.
     *
     * @return state after the migration.
     */
    MigrationState executeTasksAndValidate();

    /**
     * Executes all migration tasks, validates the result and saves the state.
     */
    void migrate();
}
