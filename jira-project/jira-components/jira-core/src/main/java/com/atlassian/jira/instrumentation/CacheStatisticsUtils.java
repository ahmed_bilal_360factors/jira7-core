package com.atlassian.jira.instrumentation;

import com.atlassian.instrumentation.caches.CacheKeys;
import com.atlassian.instrumentation.caches.RequestListener;
import com.atlassian.vcache.internal.LongMetric;
import com.atlassian.vcache.internal.MetricLabel;
import com.atlassian.vcache.internal.core.metrics.DefaultLongMetric;
import com.google.common.collect.ImmutableList;

import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

/**
 * Various utilities for Cache Statistics.
 *
 * @since v7.1
 */
public final class CacheStatisticsUtils {
    private CacheStatisticsUtils() {
    }

    /**
     * Converts Atlassian Cache statistics gathered by {@link RequestListener} into {@link CacheStatistics} object.
     *
     * @param requestListener to convert.
     * @param cacheType       - type of a cache (C - regular cache, R - cached reference)
     */
    public static CacheStatistics fromRequestListenerStatistics(RequestListener requestListener, String cacheType) {
        final Map<String, Object> stats = requestListener.onRequestEnd();
        return new CacheStatistics.CacheStatisticsBuilder()
                .withName(requestListener.getName())
                .withTags(requestListener.getTags())
                .withType(cacheType)
                .withHits((long) stats.getOrDefault(CacheKeys.HITS.getName(), 0L))
                .withMisses((long) stats.getOrDefault(CacheKeys.MISSES.getName(), 0L))
                .withLoadTime((double) stats.getOrDefault(CacheKeys.LOAD_TIME.getName(), 0.0))
                .withOtherStats(stats)
                .build();
    }

    /**
     * Converts VCache statistics into {@link CacheStatistics} object.
     *
     * @param name      name of the cache.
     * @param cacheType type of the cache (e.g. External, Request, JVM).
     * @param metrics   metrics gathered for the cache.
     */
    public static CacheStatistics fromVCacheStatistics(String name, String cacheType,
                                                       EnumMap<MetricLabel, LongMetric> metrics) {
        LongMetric emptyMetric = new DefaultLongMetric();

        Map<String, Object> otherStats = new HashMap<>(metrics.size());
        for (Map.Entry<MetricLabel, LongMetric> entry : metrics.entrySet()) {
            otherStats.put(entry.getKey().name().toLowerCase(), entry.getValue());
        }

        return new CacheStatistics.CacheStatisticsBuilder()
                .withName(name)
                .withTags(ImmutableList.of("cache", "vcache", cacheType))
                .withType(cacheType)
                .withHits((int) metrics.getOrDefault(MetricLabel.NUMBER_OF_HITS, emptyMetric).getSampleCount())
                .withMisses((int) metrics.getOrDefault(MetricLabel.NUMBER_OF_MISSES, emptyMetric).getSampleCount())
                .withLoadTime(mean(metrics.getOrDefault(MetricLabel.TIMED_SUPPLIER_CALL, emptyMetric)))
                .withGetTime(mean(metrics.getOrDefault(MetricLabel.TIMED_GET_CALL, emptyMetric)))
                .withPutTime(mean(metrics.getOrDefault(MetricLabel.TIMED_PUT_CALL, emptyMetric)))
                .withOtherStats(otherStats)
                .build();
    }

    /**
     * Converts VCaches statistics into stream of {@link CacheStatistics}.
     *
     * @param cacheType     type of the cache (e.g. External, Request, JVM).
     * @param cachesMetrics map containg cache name as a key and metrics for that cache as a value.
     * @return stream of {@link CacheStatistics}
     */
    public static Stream<CacheStatistics> convertVCacheMetrics(final String cacheType,
                                                               final Map<String, EnumMap<MetricLabel, ? extends LongMetric>> cachesMetrics) {
        return cachesMetrics.entrySet().stream().map(m -> fromVCacheStatistics(m.getKey(),
                cacheType, (EnumMap) m.getValue()));
    }

    private static long mean(LongMetric metric) {
        return metric.getSampleCount() == 0 ? 0 : metric.getSamplesTotal() / metric.getSampleCount();
    }
}
