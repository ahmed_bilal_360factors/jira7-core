package com.atlassian.jira.message;

import com.atlassian.jira.help.HelpUrl;
import com.atlassian.jira.help.HelpUrls;
import com.atlassian.jira.util.BaseUrl;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.web.util.ExternalLinkUtil;
import com.google.common.base.Function;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.net.URI;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.Set;

import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * Default implementation of the message utility.
 *
 * @since 7.0
 */
public class MessageUtilImpl implements MessageUtil {
    private final I18nHelper i18nHelper;
    private final HelpUrls helpUrls;
    private final ExternalLinkUtil externalLinkUtil;
    private final BaseUrl baseUrl;

    MessageUtilImpl(final I18nHelper i18nHelper, final HelpUrls helpUrls, final ExternalLinkUtil externalLinkUtil,
                    final BaseUrl baseUrl) {
        this.i18nHelper = notNull("i18nHelper", i18nHelper);
        this.helpUrls = notNull("helpUrls", helpUrls);
        this.externalLinkUtil = notNull("externalLinkUtil", externalLinkUtil);
        this.baseUrl = notNull("baseUrl", baseUrl);
    }

    @Nonnull
    @Override
    public HelpUrl getUrl(final String key) {
        return helpUrls.getUrl(key);
    }

    @Nonnull
    @Override
    public HelpUrl getDefaultUrl() {
        return helpUrls.getDefaultUrl();
    }

    @Nonnull
    @Override
    public Set<String> getUrlKeys() {
        return helpUrls.getUrlKeys();
    }

    @Override
    public String getExternalLink(final String key) {
        return externalLinkUtil.getProperty(key);
    }

    @Override
    public String getExternalLink(final String key, final String value1) {
        return externalLinkUtil.getProperty(key, value1);
    }

    @Override
    public String getExternalLink(final String key, final String value1, final String value2) {
        return externalLinkUtil.getProperty(key, value1, value2);
    }

    @Override
    public String getExternalLink(final String key, final String value1, final String value2, final String value3) {
        return externalLinkUtil.getProperty(key, value1, value2, value3);
    }

    @Override
    public String getExternalLink(final String key, final String value1, final String value2, final String value3,
                                  final String value4) {
        return externalLinkUtil.getProperty(key, value1, value2, value3, value4);
    }

    @Override
    public String getExternalLink(final String key, final Object parameters) {
        return externalLinkUtil.getProperty(key, parameters);
    }

    @Override
    public String getAnchorTagWithInternalLink(String keyOfLink) {
        final HelpUrl helpUrl = getUrl(keyOfLink);
        return String.format("<a href=\"%s%s\">%s</a>", getBaseUrl(), helpUrl.getUrl(), getText(helpUrl.getTitle()));
    }

    @Override
    public Locale getLocale() {
        return i18nHelper.getLocale();
    }

    @Override
    public ResourceBundle getDefaultResourceBundle() {
        return i18nHelper.getDefaultResourceBundle();
    }

    @Override
    public String getUnescapedText(final String key) {
        return i18nHelper.getUnescapedText(key);
    }

    @Override
    public String getUntransformedRawText(final String key) {
        return i18nHelper.getUntransformedRawText(key);
    }

    @Override
    public boolean isKeyDefined(final String key) {
        return i18nHelper.isKeyDefined(key);
    }

    @Override
    public String getText(final String key) {
        return i18nHelper.getText(key);
    }

    @Override
    public String getText(final String key, final String value1) {
        return i18nHelper.getText(key, value1);
    }

    @Override
    public String getText(final String key, final String value1, final String value2) {
        return i18nHelper.getText(key, value1, value2);
    }

    @Override
    public String getText(final String key, final String value1, final String value2, final String value3) {
        return i18nHelper.getText(key, value1, value2, value3);
    }

    @Override
    public String getText(final String key, final String value1, final String value2, final String value3,
                          final String value4) {
        return i18nHelper.getText(key, value1, value2, value3, value4);
    }

    @Override
    public String getText(final String key, final Object value1, final Object value2, final Object value3) {
        return i18nHelper.getText(key, value1, value2, value3);
    }

    @Override
    public String getText(final String key, final Object value1, final Object value2, final Object value3,
                          final Object value4) {
        return i18nHelper.getText(key, value1, value2, value3, value4);
    }

    @Override
    public String getText(final String key, final Object value1, final Object value2, final Object value3,
                          final Object value4, final Object value5) {
        return i18nHelper.getText(key, value1, value2, value3, value4, value5);
    }

    @Override
    public String getText(final String key, final Object value1, final Object value2, final Object value3,
                          final Object value4, final Object value5, final Object value6) {
        return i18nHelper.getText(key, value1, value2, value3, value4, value5, value6);
    }

    @Override
    public String getText(final String key, final Object value1, final Object value2, final Object value3,
                          final Object value4, final Object value5, final Object value6, final Object value7) {
        return i18nHelper.getText(key, value1, value2, value3, value4, value5, value6, value7);
    }

    @Override
    public String getText(final String key, final String value1, final String value2, final String value3,
                          final String value4, final String value5, final String value6, final String value7) {
        return i18nHelper.getText(key, value1, value2, value3, value4, value5, value6, value7);
    }

    @Override
    public String getText(final String key, final Object value1, final Object value2, final Object value3,
                          final Object value4, final Object value5, final Object value6, final Object value7, final Object value8) {
        return i18nHelper.getText(key, value1, value2, value3, value4, value5, value6, value7, value8);
    }

    @Override
    public String getText(final String key, final String value1, final String value2, final String value3,
                          final String value4, final String value5, final String value6, final String value7, final String value8,
                          final String value9) {
        return i18nHelper.getText(key, value1, value2, value3, value4, value5, value6, value7, value8, value9);
    }

    @Override
    public String getText(final String key, final Object parameters) {
        return i18nHelper.getText(key, parameters);
    }

    @Override
    public Set<String> getKeysForPrefix(final String prefix) {
        return i18nHelper.getKeysForPrefix(prefix);
    }

    @Override
    public ResourceBundle getResourceBundle() {
        return i18nHelper.getResourceBundle();
    }

    @Nonnull
    @Override
    public String getBaseUrl() {
        return baseUrl.getBaseUrl();
    }

    @Nonnull
    @Override
    public String getCanonicalBaseUrl() {
        return baseUrl.getCanonicalBaseUrl();
    }

    @Override
    public URI getBaseUri() {
        return baseUrl.getBaseUri();
    }

    @Nullable
    @Override
    public <I, O> O runWithStaticBaseUrl(@Nullable final I input, @Nonnull final Function<I, O> runnable) {
        return baseUrl.runWithStaticBaseUrl(input, runnable);
    }
}