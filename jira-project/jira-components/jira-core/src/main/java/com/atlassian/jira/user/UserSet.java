package com.atlassian.jira.user;

import com.atlassian.crowd.embedded.impl.IdentifierUtils;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Keeps a unique set of Users based on the username only.
 * <p>
 * This "collection" will work properly with shadowed users, mixed case usernames and the different hashcodes of
 * ApplicationUser and Directory Users.
 *
 * @since v6.0
 */
public class UserSet {
    private final Map<String, ApplicationUser> userMap;

    public UserSet(Collection<ApplicationUser> users) {
        userMap = new HashMap<String, ApplicationUser>();

        for (ApplicationUser user : users) {
            add(user);
        }
    }

    public boolean contains(ApplicationUser user) {
        return userMap.containsKey(lowerUsername(user));
    }

    public void add(ApplicationUser user) {
        userMap.put(lowerUsername(user), user);
    }

    public Collection<ApplicationUser> values() {
        return userMap.values();
    }

    public Set<ApplicationUser> toSet() {
        return new HashSet<ApplicationUser>(values());
    }

    private String lowerUsername(ApplicationUser user) {
        return IdentifierUtils.toLowerCase(user.getName());
    }
}
