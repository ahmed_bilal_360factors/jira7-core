package com.atlassian.jira.web.action.util;

import com.atlassian.jira.util.BuildUtilsInfo;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import webwork.action.Action;

import com.atlassian.plugin.webresource.UrlMode;
import com.atlassian.plugin.webresource.WebResourceUrlProvider;

/**
 * Displays the JIRA credits page
 *
 * @since v4.3
 */
public class JiraCreditsPage extends JiraWebActionSupport {
    private final String buildVersion;
    private final WebResourceUrlProvider webResourceUrlProvider;
    private static String webResourceKey = "jira.webfragments.user.navigation.bar:jira-credits-web-resource";

    public JiraCreditsPage(final BuildUtilsInfo buildUtilsInfo, final WebResourceUrlProvider webResourceUrlProvider) {
        buildVersion = buildUtilsInfo.getVersion();
        this.webResourceUrlProvider = webResourceUrlProvider;
    }

    protected String doExecute() throws Exception {
        if (isInlineDialogMode()) {
            return returnComplete();
        }

        return super.doExecute();
    }

    public String doDefault() throws Exception {
        return Action.INPUT;
    }


    public String getBuildVersion() {
        return buildVersion;
    }

    public String getStyleURL() {
        return webResourceUrlProvider.getStaticPluginResourceUrl(webResourceKey, "credits.css", UrlMode.RELATIVE);
    }

    public String getCursorImageURL() {
        return webResourceUrlProvider.getStaticPluginResourceUrl(webResourceKey, "cursor_32_lt.gif", UrlMode.RELATIVE);
    }

    public String getGrassImageURL() {
        return webResourceUrlProvider.getStaticPluginResourceUrl(webResourceKey, "grass.gif", UrlMode.RELATIVE);
    }

    public String getHandImageURL() {
        return webResourceUrlProvider.getStaticPluginResourceUrl(webResourceKey, "hand.gif", UrlMode.RELATIVE);
    }

    public String getJiraViiImageURL() {
        return webResourceUrlProvider.getStaticPluginResourceUrl(webResourceKey, "jiravii_title_03.gif", UrlMode.RELATIVE);
    }

    public String getMapImageURL() {
        return webResourceUrlProvider.getStaticPluginResourceUrl(webResourceKey, "map.gif", UrlMode.RELATIVE);
    }

    public String getSelect1SoundURL() {
        return webResourceUrlProvider.getStaticPluginResourceUrl(webResourceKey, "select1.wav", UrlMode.RELATIVE);
    }

    public String getSelect2SoundURL() {
        return webResourceUrlProvider.getStaticPluginResourceUrl(webResourceKey, "select2.wav", UrlMode.RELATIVE);
    }

    public String getTurnoff1SoundURL() {
        return webResourceUrlProvider.getStaticPluginResourceUrl(webResourceKey, "turnoff1.wav", UrlMode.RELATIVE);
    }

    public String getTurnon1SoundURL() {
        return webResourceUrlProvider.getStaticPluginResourceUrl(webResourceKey, "turnon1.wav", UrlMode.RELATIVE);
    }

}
