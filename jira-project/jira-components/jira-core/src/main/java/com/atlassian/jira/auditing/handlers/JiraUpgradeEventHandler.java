package com.atlassian.jira.auditing.handlers;

import com.atlassian.jira.auditing.RecordRequest;
import com.atlassian.jira.cluster.zdu.JiraUpgradeApprovedEvent;
import com.atlassian.jira.cluster.zdu.JiraUpgradeCancelledEvent;
import com.atlassian.jira.cluster.zdu.JiraUpgradeFinishedEvent;
import com.atlassian.jira.cluster.zdu.JiraUpgradeStartedEvent;

/**
 * @since v7.3
 */
public interface JiraUpgradeEventHandler {
    RecordRequest onStartedEvent(JiraUpgradeStartedEvent event);
    RecordRequest onCancelledEvent(JiraUpgradeCancelledEvent event);
    RecordRequest onApprovedEvent(JiraUpgradeApprovedEvent event);
    RecordRequest onFinishedEvent(JiraUpgradeFinishedEvent event);
}
