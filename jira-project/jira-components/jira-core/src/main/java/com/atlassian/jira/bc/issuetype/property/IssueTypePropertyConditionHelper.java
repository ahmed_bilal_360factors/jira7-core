package com.atlassian.jira.bc.issuetype.property;

import com.atlassian.jira.entity.property.AbstractEntityPropertyConditionHelper;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.issuetype.IssueTypeWithID;
import com.atlassian.jira.plugin.webfragment.JiraWebContext;

import java.util.Map;
import java.util.Optional;

public final class IssueTypePropertyConditionHelper extends AbstractEntityPropertyConditionHelper<IssueTypeWithID> {

    public IssueTypePropertyConditionHelper(IssueTypePropertyService propertyService) {
        super(propertyService, IssueTypeWithID.class, "issuetype");
    }

    @Override
    public Optional<Long> getEntityId(JiraWebContext context) {
        Optional<Issue> issueOpt = context.get("issue", Issue.class);
        return issueOpt.map(issue -> IssueTypeWithID.fromIssueType(issue.getIssueType()).getId());
    }

}
