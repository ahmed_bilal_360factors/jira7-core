package com.atlassian.jira.crowd.embedded.ofbiz.db;

import java.util.function.Consumer;
import java.util.function.Function;

public class DefaultOfBizTransactionManager implements OfBizTransactionManager {
    @Override
    public void withTransaction(Consumer<? super OfBizTransaction> transactionUsingFunction) {
        DefaultOfBizTransaction.withTransaction(transactionUsingFunction);
    }

    @Override
    public <R> R withTransaction(Function<? super OfBizTransaction, R> transactionUsingFunction) {
        return DefaultOfBizTransaction.withTransaction(transactionUsingFunction);
    }
}
