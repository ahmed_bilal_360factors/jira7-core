package com.atlassian.jira.cluster.zdu;

import com.atlassian.event.api.EventListener;
import com.atlassian.jira.EventComponent;
import com.atlassian.jira.cluster.ClusterMessageConsumer;
import com.atlassian.jira.cluster.ClusterMessagingService;
import com.atlassian.jira.config.properties.JiraProperties;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import static com.atlassian.jira.cluster.zdu.ClusterUpgradeStateManager.CLUSTER_UPGRADE_STATE_CHANGED;
import static com.atlassian.jira.cluster.zdu.UpgradeState.READY_TO_UPGRADE;
import static com.atlassian.jira.cluster.zdu.UpgradeState.RUNNING_UPGRADE_TASKS;
import static com.atlassian.jira.cluster.zdu.UpgradeState.STABLE;

@EventComponent
public class ClusterUpgradeLogger implements ClusterMessageConsumer {
    private static final Logger log = Logger.getLogger(ClusterUpgradeLogger.class);
    private final ClusterMessagingService messagingService;
    private final JiraProperties jiraProperties;

    public ClusterUpgradeLogger(ClusterMessagingService messagingService, JiraProperties jiraProperties) {
        this.messagingService = messagingService;
        this.jiraProperties = jiraProperties;
        
        this.messagingService.registerListener(CLUSTER_UPGRADE_STATE_CHANGED, this);
    }

    private String formatBigMessage(String message) {
        final String newLine = jiraProperties.getProperty("line.separator");
        final String starLine = StringUtils.repeat("*", message.length());
        return newLine + newLine + starLine + newLine + message + newLine + starLine + newLine;
    }

    void logClusterUpgradeState(String upgradeState) {
        log.info(formatBigMessage("Cluster upgrade state changed to " + upgradeState));
    }

    void logClusterUpgradeState(String upgradeState, String senderId) {
        log.info(formatBigMessage("Cluster upgrade state changed to " + upgradeState + " (sender: " + senderId + ")"));
    }

    @EventListener
    public void onUpgradeStarted(JiraUpgradeStartedEvent event) {
        logClusterUpgradeState(READY_TO_UPGRADE.toString());
    }

    @EventListener
    public void onUpgradeApproved(JiraUpgradeApprovedEvent event) {
        logClusterUpgradeState(RUNNING_UPGRADE_TASKS.toString());
    }

    @EventListener
    public void onUpgradeFinished(JiraUpgradeFinishedEvent event) {
        logClusterUpgradeState(STABLE.toString());
    }

    @EventListener
    public void onUpgradeCancelled(JiraUpgradeCancelledEvent event) {
        logClusterUpgradeState(STABLE.toString());
    }

    @Override
    public void receive(String channel, String message, String senderId) {
        logClusterUpgradeState(message, senderId);
    }
}
