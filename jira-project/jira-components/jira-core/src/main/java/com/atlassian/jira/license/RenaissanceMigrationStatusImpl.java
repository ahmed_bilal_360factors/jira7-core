package com.atlassian.jira.license;

import com.atlassian.jira.config.properties.ApplicationProperties;

import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * @since v7.0
 */
public class RenaissanceMigrationStatusImpl implements RenaissanceMigrationStatus {
    private static final String KEY = "renaissanceMigrationDone";

    private final ApplicationProperties properties;

    public RenaissanceMigrationStatusImpl(final ApplicationProperties properties) {
        this.properties = notNull("properties", properties);
    }

    @Override
    public boolean markDone() {
        boolean old = hasMigrationRun();
        properties.setOption(KEY, true);
        return old;
    }

    public boolean hasMigrationRun() {
        return properties.getOption(KEY);
    }
}
