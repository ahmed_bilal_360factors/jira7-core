package com.atlassian.jira.task;

import com.google.common.base.Objects;


/**
 * General context that can be used for a task that has NO requirement to be unique, i.e. many of these can run at the same time.
 */
public class NonExclusiveTaskContext implements TaskContext {

    private final String progressURL;

    private final String taskContextName;

    /**
     * @param progressURL     progress URL to be returned as-is by {@link #buildProgressURL}
     * @param taskContextName this name should be unique between task submitters, to identify their task
     */
    public NonExclusiveTaskContext(final String progressURL, final String taskContextName) {
        this.progressURL = progressURL;
        this.taskContextName = taskContextName;
    }


    @Override
    public String buildProgressURL(final Long taskId) {
        return progressURL + "?taskId=" + taskId;
    }

    public String getTaskContextName() {
        return taskContextName;
    }

    @Override
    public boolean equals(final Object obj) {
        // This context only equals itself.
        if (obj == null) {
            return false;
        }
        return obj == this;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public String toString() {
        return "NonExclusiveTaskContext{" +
                "progressURL='" + progressURL + '\'' +
                ", taskContextName='" + taskContextName + '\'' +
                '}';
    }
}
