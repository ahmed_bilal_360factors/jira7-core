package com.atlassian.jira.issue.search.providers;

import com.atlassian.analytics.api.annotations.EventName;
import com.atlassian.event.api.AsynchronousPreferred;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

@EventName("com.atlassian.jira.issue.search.providers.lucene.unknown.result.size")
@AsynchronousPreferred
public class SearchUnknownResultSizeEvent {
    private final int resultSize;

    public SearchUnknownResultSizeEvent(final int resultSize) {
        this.resultSize = resultSize;
    }

    public int getResultSize() {
        return resultSize;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        final SearchUnknownResultSizeEvent that = (SearchUnknownResultSizeEvent) o;

        return new EqualsBuilder()
                .append(resultSize, that.resultSize)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(resultSize)
                .toHashCode();
    }
}
