package com.atlassian.jira.config;

import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.issue.priority.Priority;
import com.atlassian.jira.issue.resolution.Resolution;
import com.atlassian.jira.issue.status.Status;
import com.atlassian.jira.model.querydsl.IssueTypeDTO;
import com.atlassian.jira.model.querydsl.PriorityDTO;
import com.atlassian.jira.model.querydsl.ResolutionDTO;
import com.atlassian.jira.model.querydsl.StatusDTO;
import org.ofbiz.core.entity.GenericValue;

/**
 * Converts issue constant DTOs into their associated objects.
 *
 * @since v5.2
 */
public interface IssueConstantFactory {
    Priority createPriority(PriorityDTO priorityDTO);

    /**
     * @deprecated Since 7.0  Use {@link #createPriority(PriorityDTO)}
     */
    Priority createPriority(GenericValue priorityGv);

    IssueType createIssueType(IssueTypeDTO issueTypeDTO);

    /**
     * @deprecated Since 7.0 Use {@link #createIssueType(IssueTypeDTO)}
     */
    IssueType createIssueType(GenericValue issueTypeGv);

    Resolution createResolution(ResolutionDTO resolutionDTO);

    /**
     * @deprecated Since 7.0 Use {@link #createResolution(ResolutionDTO)}
     */
    Resolution createResolution(GenericValue resolutionGv);

    Status createStatus(StatusDTO statusDTO);

    /**
     * @deprecated Since 7.0 Use {@link #createStatus(StatusDTO)}
     */
    Status createStatus(GenericValue statusGv);
}
