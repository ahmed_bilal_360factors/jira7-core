package com.atlassian.jira.dashboard;

import com.atlassian.beehive.ClusterLockService;
import com.atlassian.fugue.Option;
import com.atlassian.gadgets.DashboardItemState;
import com.atlassian.gadgets.DashboardItemStateVisitor;
import com.atlassian.gadgets.GadgetId;
import com.atlassian.gadgets.GadgetState;
import com.atlassian.gadgets.LocalDashboardItemModuleId;
import com.atlassian.gadgets.LocalDashboardItemState;
import com.atlassian.gadgets.OpenSocialDashboardItemModuleId;
import com.atlassian.gadgets.dashboard.DashboardId;
import com.atlassian.gadgets.dashboard.DashboardNotFoundException;
import com.atlassian.gadgets.dashboard.DashboardState;
import com.atlassian.gadgets.dashboard.spi.DashboardStateStore;
import com.atlassian.gadgets.dashboard.spi.DashboardStateStoreException;
import com.atlassian.gadgets.dashboard.spi.changes.DashboardChange;
import com.atlassian.jira.exception.DataAccessException;
import com.atlassian.jira.portal.PortalPage;
import com.atlassian.jira.portal.PortalPageManager;
import com.atlassian.jira.portal.PortalPageStore;
import com.atlassian.jira.portal.PortletConfiguration;
import com.atlassian.jira.portal.PortletConfigurationImpl;
import com.atlassian.jira.portal.PortletConfigurationStore;
import com.atlassian.plugin.ModuleCompleteKey;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.MapDifference;
import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.net.URI;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.Lock;

import static com.atlassian.gadgets.dashboard.DashboardState.dashboard;
import static com.atlassian.jira.dashboard.DashboardUtil.toLong;
import static com.atlassian.jira.util.dbc.Assertions.notNull;
import static com.google.common.collect.Iterables.transform;
import static com.google.common.collect.Maps.uniqueIndex;

/**
 * Provides CRUD operations for dashboards.  Uses the existing {@link com.atlassian.jira.portal.PortalPageStore} and
 * {@link com.atlassian.jira.portal.PortletConfigurationStore} implementations. Note that this class does not need to do
 * any permission checks, since this is the responsibility of the {@link com.atlassian.jira.dashboard.permission.JiraPermissionService}.
 *
 * @since v4.0
 */
public class JiraDashboardStateStoreManager implements DashboardStateStore {
    private static final Logger log = LoggerFactory.getLogger(JiraDashboardStateStoreManager.class);

    private final ClusterLockService clusterLockService;
    private final PortalPageManager portalPageManager;
    private final PortalPageStore portalPageStore;
    private final PortletConfigurationStore portletConfigurationStore;

    public JiraDashboardStateStoreManager(
            final PortalPageStore portalPageStore,
            final PortletConfigurationStore portletConfigurationStore,
            final PortalPageManager portalPageManager,
            final ClusterLockService clusterLockService) {
        this.clusterLockService = clusterLockService;
        this.portalPageManager = portalPageManager;
        this.portalPageStore = portalPageStore;
        this.portletConfigurationStore = portletConfigurationStore;
    }

    /**
     * Converts a PortletConfiguration to a GadgetState.
     */
    private final Function<PortletConfiguration, DashboardItemState> toDashboardItemState = new Function<PortletConfiguration, DashboardItemState>() {
        @Override
        public DashboardItemState apply(final PortletConfiguration portletConfiguration) {
            final Option<URI> openSocialSpecUri = portletConfiguration.getOpenSocialSpecUri();
            final Option<ModuleCompleteKey> moduleKey = portletConfiguration.getCompleteModuleKey();
            // The case when dashboard-item replaces an open-social based gadget
            if (openSocialSpecUri.isDefined() && moduleKey.isDefined()) {
                return createLocalDashboardItem(portletConfiguration);
            } else if (moduleKey.isDefined()) { // the case when we simply add a dashboard-item to the dashboard
                return createLocalDashboardItem(portletConfiguration);
            } else if (openSocialSpecUri.isDefined()) { // gadget
                return createOpenSocialGadget(portletConfiguration, openSocialSpecUri.get());
            } else { // should not happen
                final URI uriToLegacyGadget = URI.create("/invalid/legacy/portlet/Please_remove_this_gadget_from_your_dashboard!");
                return createOpenSocialGadget(portletConfiguration, uriToLegacyGadget);
            }
        }

        private DashboardItemState createLocalDashboardItem(final PortletConfiguration portletConfiguration) {
            return LocalDashboardItemState.builder()
                    .gadgetId(getGadgetId(portletConfiguration))
                    .color(portletConfiguration.getColor())
                    .properties(portletConfiguration.getUserPrefs())
                    .dashboardItemModuleId(getLocalDashboardItemModuleId(portletConfiguration)).build();
        }

        private LocalDashboardItemModuleId getLocalDashboardItemModuleId(final PortletConfiguration portletConfiguration) {
            final ModuleCompleteKey fullModuleKey = portletConfiguration.getCompleteModuleKey().get();
            final Option<OpenSocialDashboardItemModuleId> openSocialId = portletConfiguration.getOpenSocialSpecUri().map(
                    new Function<URI, OpenSocialDashboardItemModuleId>() {
                        @Override
                        public OpenSocialDashboardItemModuleId apply(final URI openSocialSpecUri) {
                            return new OpenSocialDashboardItemModuleId(openSocialSpecUri);
                        }
                    });
            return new LocalDashboardItemModuleId(fullModuleKey, openSocialId);
        }

        private DashboardItemState createOpenSocialGadget(final PortletConfiguration portletConfiguration, final URI gadgetUri) {
            return GadgetState
                    .gadget(getGadgetId(portletConfiguration))
                    .specUri(gadgetUri)
                    .color(portletConfiguration.getColor())
                    .userPrefs(portletConfiguration.getUserPrefs())
                    .build();
        }

        private GadgetId getGadgetId(final PortletConfiguration portletConfiguration) {
            return GadgetId.valueOf(portletConfiguration.getId().toString());
        }
    };

    @VisibleForTesting
    static String getWriteLockName(final DashboardId dashboardId) {
        return JiraDashboardStateStoreManager.class.getName() + ".dashboard-" + dashboardId;
    }

    public DashboardState retrieve(final DashboardId dashboardId)
            throws DashboardNotFoundException, DashboardStateStoreException {
        notNull("dashboardId", dashboardId);
        final Long portalPageId = toLong(dashboardId);

        // We read the dashboard under an optimistic lock to ensure we read
        // a consistent state from the DB, not some 'half-written' state.
        Long versionBefore, versionAfter;
        DashboardState dashboardState;
        do {
            versionBefore = getPortalPageVersion(portalPageId);
            dashboardState = getDashboardState(dashboardId, portalPageId);
            versionAfter = getPortalPageVersion(portalPageId);
        }
        while (!ObjectUtils.equals(versionBefore, versionAfter));
        return dashboardState;
    }

    @Nullable
    private Long getPortalPageVersion(@Nullable final Long portalPageId) {
        if (portalPageId == null) {
            return null;
        }
        final PortalPage portalPage = portalPageStore.getPortalPage(portalPageId);
        return portalPage == null ? null : portalPage.getVersion();
    }

    private DashboardState getDashboardState(final DashboardId dashboardId, final Long portalPageId) {
        try {
            final PortalPage portalPage = portalPageStore.getPortalPage(portalPageId);
            if (portalPage == null) {
                throw new DashboardNotFoundException(dashboardId);
            }

            final List<List<PortletConfiguration>> pcColumns = portalPageManager.getPortletConfigurations(portalPageId);

            // Convert the JIRA portalPage to a DashboardState/GadgetState class
            final Iterable<Iterable<DashboardItemState>> dashboardColumns = transform(pcColumns, new Function<List<PortletConfiguration>, Iterable<DashboardItemState>>() {
                @Override
                public Iterable<DashboardItemState> apply(final List<PortletConfiguration> input) {
                    return transform(input, toDashboardItemState);
                }
            });

            return dashboard(dashboardId)
                    .title(portalPage.getName())
                    .version(portalPage.getVersion() == null ? 1L : portalPage.getVersion())
                    .dashboardColumns(dashboardColumns)
                    .layout(portalPage.getLayout())
                    .build();
        } catch (DataAccessException e) {
            throw new DashboardStateStoreException(
                    "Unknown error occurred while retrieving dashboard with id '" + portalPageId + "'.", e);
        }
    }

    public DashboardState update(final DashboardState dashboardState, final Iterable<DashboardChange> dashboardChanges)
            throws DashboardStateStoreException {
        notNull("dashboardState", dashboardState);
        notNull("dashboardChanges", dashboardChanges);

        final DashboardId dashboardId = dashboardState.getId();
        // Writing the dashboard is done under a cluster lock (by dashboardId)
        // to ensure a consistent state is written to the DB by only one thread.
        final Lock writeLock = clusterLockService.getLockForName(getWriteLockName(dashboardId));
        writeLock.lock();
        try {
            acquireOptimisticWriteLock(dashboardState);

            // If no specific changes were submitted, persist the entire dashboard state
            if (!dashboardChanges.iterator().hasNext()) {
                return storeDashboardState(dashboardState);
            }

            new JiraDashboardChangeHandler(dashboardState, portletConfigurationStore, portalPageStore)
                    .accept(dashboardChanges);

            final DashboardState storedState = retrieve(dashboardId);
            // This should never happen, but if updating via {@link com.atlassian.gadgets.spi.changes.DashboardChange}s
            // doesn't work, we try rewriting the entire dashboard state from scratch.
            if (!storedState.equals(dashboardState)) {
                log.warn("Stored state for dashboard with id '{}' is not the same as in-memory state."
                        + " Trying to rewrite the entire state...", dashboardId);
                return storeDashboardState(dashboardState);
            }
            return storedState;
        } catch (final DataAccessException e) {
            throw new DashboardStateStoreException("Error updating dashboard state with id '" + dashboardId + "'.", e);
        } finally {
            writeLock.unlock();
        }
    }

    private void acquireOptimisticWriteLock(final DashboardState dashboardState) {
        // Optimistic lock solution. The *very first* thing we do is update the dashboard version.
        // If this fails, another thread has updated in the meantime and we throw an exception.
        final DashboardId dashboardId = dashboardState.getId();
        final boolean optimisticLock =
                portalPageStore.updatePortalPageOptimisticLock(toLong(dashboardId), dashboardState.getVersion());
        if (!optimisticLock) {
            // looks like the optimistic lock (i.e. version) for this dashboard was already out of date.
            throw new DashboardStateStoreException(
                    "Dashboard with id '" + dashboardId + "' is out of sync with the currently persisted state.");
        }
    }

    public void remove(final DashboardId dashboardId) throws DashboardStateStoreException {
        notNull("dashboardId", dashboardId);

        final Long portalPageId = toLong(dashboardId);
        // Removing the dashboard is done under a cluster lock (by dashboardId)
        // to ensure all threads trying to read this dashboard will block
        final Lock writeLock = clusterLockService.getLockForName(getWriteLockName(dashboardId));
        writeLock.lock();
        try {
            portalPageManager.delete(portalPageId);
        } catch (final DataAccessException e) {
            throw new DashboardStateStoreException("Error removing dashboard state with id'" + dashboardId + "'.", e);
        } finally {
            writeLock.unlock();
        }
    }

    public DashboardState findDashboardWithGadget(final GadgetId gadgetId) throws DashboardNotFoundException {
        notNull("gagdetId", gadgetId);

        try {
            final PortletConfiguration portletConfiguration = portletConfigurationStore.getByPortletId(toLong(gadgetId));
            if (portletConfiguration == null) {
                throw new DashboardStateStoreException("Gadget with id '" + gadgetId + "' not found!");
            }
            return retrieve(DashboardId.valueOf(Long.toString(portletConfiguration.getDashboardPageId())));
        } catch (DataAccessException e) {
            throw new DashboardStateStoreException("Error looking up gadget with id '" + gadgetId + "'.", e);
        }
    }

    private DashboardState storeDashboardState(final DashboardState dashboardState) {
        notNull("dashboardState", dashboardState);

        final DashboardId dashboardId = dashboardState.getId();
        final long portalPageId = toLong(dashboardId);

        final PortalPage portalPage = portalPageStore.getPortalPage(portalPageId);
        if (portalPage == null) {
            throw new DashboardStateStoreException("No portal page found with id '" + portalPageId + "'");
        }
        updatePortalPage(portalPage, dashboardState);

        final Map<Long, PortletConfiguration> oldPortlets = portletSeqToMap(portletConfigurationStore.getByPortalPage(portalPageId));
        final Map<Long, PortletConfiguration> newPortlets = portletSeqToMap(getPortletsFromDashboardState(dashboardState));

        final MapDifference<Long, PortletConfiguration> difference = Maps.difference(oldPortlets, newPortlets);
        final Iterable<PortletConfiguration> portletsToUpdate = Maps.transformValues(difference.entriesDiffering(), new Function<MapDifference.ValueDifference<PortletConfiguration>, PortletConfiguration>() {
            @Override
            public PortletConfiguration apply(final MapDifference.ValueDifference<PortletConfiguration> input) {
                return input.rightValue();
            }
        }).values();
        final Iterable<PortletConfiguration> portletsToAdd = difference.entriesOnlyOnRight().values();
        final Iterable<PortletConfiguration> portletsToRemove = difference.entriesOnlyOnLeft().values();

        for (PortletConfiguration toUpdate : portletsToUpdate) {
            portletConfigurationStore.store(toUpdate);
        }

        for (PortletConfiguration toAdd : portletsToAdd) {
            portletConfigurationStore.addDashboardItem(toAdd.getDashboardPageId(), toAdd.getId(), toAdd.getColumn(), toAdd.getRow(),
                    toAdd.getOpenSocialSpecUri(), toAdd.getColor(), toAdd.getUserPrefs(), toAdd.getCompleteModuleKey());
        }

        for (PortletConfiguration toRemove : portletsToRemove) {
            portletConfigurationStore.delete(toRemove);
        }

        return retrieve(dashboardId);
    }

    private void updatePortalPage(final PortalPage portalPage, final DashboardState dashboardState) {
        //update the portalPageStore's title and layout if they changed.
        if (!portalPage.getLayout().equals(dashboardState.getLayout()) ||
                !StringUtils.equals(portalPage.getName(), dashboardState.getTitle())) {
            final PortalPage.Builder builder = PortalPage.portalPage(portalPage);
            builder.name(dashboardState.getTitle());
            builder.layout(dashboardState.getLayout());
            portalPageStore.update(builder.build());
        }
    }

    /**
     * Returns all {@link com.atlassian.jira.portal.PortletConfiguration} from the new state of the {@link com.atlassian.gadgets.dashboard.DashboardState}.
     *
     * @param dashboardState the new state of the dashboard.
     * @return all {@link com.atlassian.jira.portal.PortletConfiguration} from the {@link com.atlassian.gadgets.dashboard.DashboardState}.
     */
    private Iterable<PortletConfiguration> getPortletsFromDashboardState(final DashboardState dashboardState) {
        final ImmutableList.Builder<PortletConfiguration> listBuilder = ImmutableList.<PortletConfiguration>builder();
        for (DashboardState.ColumnIndex columnIndex : dashboardState.getLayout().getColumnRange()) {
            int row = 0;
            for (final DashboardItemState dashboardItemState : dashboardState.getDashboardColumns().getItemsInColumn(columnIndex)) {
                listBuilder.add(toPortletConfiguration(dashboardItemState, toLong(dashboardState.getId()), columnIndex.index(), row));
                row += 1;
            }
        }
        return listBuilder.build();
    }

    private PortletConfiguration toPortletConfiguration(final DashboardItemState dashboardItemState, final Long page, final Integer column, final Integer row) {
        final long itemId = toLong(dashboardItemState.getId());
        return dashboardItemState.accept(new DashboardItemStateVisitor<PortletConfiguration>() {
            @Override
            public PortletConfiguration visit(final GadgetState state) {
                return new PortletConfigurationImpl(itemId, page, column, row, Option.some(state.getGadgetSpecUri()),
                        state.getColor(), state.getUserPrefs(), Option.<ModuleCompleteKey>none());
            }

            @Override
            public PortletConfiguration visit(final LocalDashboardItemState state) {
                final Option<URI> openSocialSpecUri = state.getDashboardItemModuleId().getReplacedGadgetId().map(new Function<OpenSocialDashboardItemModuleId, URI>() {
                    @Override
                    public URI apply(final OpenSocialDashboardItemModuleId input) {
                        return input.getSpecUri();
                    }
                });
                final Option<ModuleCompleteKey> moduleKey = Option.some(state.getDashboardItemModuleId().getFullModuleKey());
                return new PortletConfigurationImpl(itemId, page, column, row, openSocialSpecUri,
                        state.getColor(), Collections.<String, String>emptyMap(), moduleKey);
            }
        });
    }

    private Map<Long, PortletConfiguration> portletSeqToMap(final Iterable<PortletConfiguration> portlets) {
        return uniqueIndex(portlets, new Function<PortletConfiguration, Long>() {
            @Override
            public Long apply(final PortletConfiguration portletConfiguration) {
                return portletConfiguration.getId();
            }
        });
    }
}
