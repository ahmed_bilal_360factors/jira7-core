package com.atlassian.jira;

import com.atlassian.core.ofbiz.CoreFactory;
import com.atlassian.jira.cluster.ClusterServicesManager;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.memoryinspector.MemoryInspector;
import com.atlassian.jira.task.TaskManager;
import com.atlassian.jira.util.index.IndexLifecycleManager;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.picocontainer.PicoContainer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import webwork.util.ValueStack;

/**
 * Previously used to provide static methods for obtaining 'Manager' classes, now it only has globalRefresh()
 */
@SuppressWarnings({"JavaDoc"})
public class ManagerFactory {
    private static final Logger log = LoggerFactory.getLogger(ManagerFactory.class);

    /**
     * This should *never* be called, except in tests, or if you are importing or seting up for the first time. The
     * reason this is called is to ensure that all the managers are reinitialised after the license has changed.
     * <p>
     * Note: Make sure the scheduler is shutdown
     */
    public static synchronized void globalRefresh() {
        log.debug("ManagerFactory.globalRefresh");

        getIndexLifecycleManager().shutdown();

        // shutdown task manager
        final TaskManager taskManager = ComponentManager.getInstance().getTaskManager();
        taskManager.shutdownAndWait(0);

        ComponentAccessor.getComponent(ClusterServicesManager.class).stopServices();
        ComponentManager.getInstance().stop();
        ComponentManager.getInstance().dispose();

        // JDEV-29269 Cleanup resources kept by MultiThreadedHttpConnectionManager - the only
        // (civilized) way to get rid of the MultiThreadedHttpConnectionManager cleanup thread.
        MultiThreadedHttpConnectionManager.shutdownAll();

        // Look for possible memory leaks
        new MemoryInspector().inspectMemoryAfterJiraShutdown();

        // Rebuild all the registered objects
        ComponentManager.getInstance().initialise();

        // clear the method cache for JRA-16750
        ValueStack.clearMethods();

        // Need to bootstrap all the components that need initialising
        ComponentManager.getInstance().start();
        ComponentManager.getInstance().lateStart();

        CoreFactory.globalRefresh();

        getConstantsManager().refresh();
    }

    private ManagerFactory() {
    }

    private static ConstantsManager getConstantsManager() {
        return getContainer().getComponent(ConstantsManager.class);
    }

    private static IndexLifecycleManager getIndexLifecycleManager() {
        return ComponentManager.getInstance().getIndexLifecycleManager();
    }

    private static PicoContainer getContainer() {
        return ComponentManager.getInstance().getContainer();
    }
}
