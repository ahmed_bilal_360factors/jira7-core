package com.atlassian.jira.issue.util;

import com.atlassian.jira.event.issue.IssueEventBundleFactory;
import com.atlassian.jira.event.issue.IssueEventManager;
import com.atlassian.jira.event.issue.txnaware.TxnAwareEventFactory;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueFactory;
import com.atlassian.jira.issue.IssueFieldConstants;
import com.atlassian.jira.issue.changehistory.metadata.HistoryMetadataManager;
import com.atlassian.jira.issue.history.ChangeItemBean;
import com.atlassian.jira.issue.history.ChangeLogUtils;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.transaction.Transaction;
import com.atlassian.jira.transaction.Txn;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.ComponentLocator;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import org.ofbiz.core.entity.GenericValue;
import org.ofbiz.core.util.UtilDateTime;

import java.util.List;
import java.util.Optional;

public class DefaultIssueUpdater implements IssueUpdater {
    private final OfBizDelegator ofBizDelegator;
    private final IssueEventManager issueEventManager;
    private final IssueEventBundleFactory issueEventBundleFactory;
    private final InlineIssuePropertySetter issuePropertySetter;
    private final ComponentLocator componentLocator;
    private final TxnAwareEventFactory txnAwareEventFactory;

    public DefaultIssueUpdater(OfBizDelegator ofBizDelegator,
                               IssueEventManager issueEventManager,
                               IssueEventBundleFactory issueEventBundleFactory,
                               InlineIssuePropertySetter issuePropertySetter,
                               ComponentLocator componentLocator,
                               TxnAwareEventFactory txnAwareEventFactory) {
        this.ofBizDelegator = ofBizDelegator;
        this.issueEventManager = issueEventManager;
        this.issueEventBundleFactory = issueEventBundleFactory;
        this.issuePropertySetter = issuePropertySetter;
        this.componentLocator = componentLocator;
        this.txnAwareEventFactory = txnAwareEventFactory;
    }

    public void doUpdate(IssueUpdateBean iub, boolean generateChangeItems) {
        iub.getChangedIssue().set(IssueFieldConstants.UPDATED, UtilDateTime.nowTimestamp());

        // We want to store the fields that have changed, not the whole issue. Unfortunately, IssueUpdateBean.getChangeItems() can be incomplete,
        // so we need to compare the before and after issues to compute the list of modified fields.
        final List<ChangeItemBean> changeItems = ChangeLogUtils.generateChangeItems(iub.getOriginalIssue(), iub.getChangedIssue());
        int size = changeItems.size() + (iub.getChangeItems() == null ? 0 : iub.getChangeItems().size());

        List<ChangeItemBean> modifiedFields = Lists.newArrayListWithCapacity(size);
        modifiedFields.addAll(changeItems);

        if (iub.getChangeItems() != null) {
            modifiedFields.addAll(iub.getChangeItems());
        }
        boolean needsUpdate = !modifiedFields.isEmpty() || iub.getComment() != null || iub.getProperties().size() > 0;
        if (needsUpdate) {
            storeModifiedFields(iub, generateChangeItems, modifiedFields);
        }
    }

    private void storeModifiedFields(final IssueUpdateBean iub, boolean generateChangeItems, List<ChangeItemBean> modifiedFields) {
        // Now construct an empty GenericValue and populate it with only the fields that have been modified
        final GenericValue updateIssueGV = new GenericValue(iub.getChangedIssue().getDelegator(), iub.getChangedIssue().getModelEntity());
        updateIssueGV.setPKFields(iub.getChangedIssue().getPrimaryKey().getAllFields());
        for (ChangeItemBean modifiedField : modifiedFields) {
            String fieldName = modifiedField.getField();
            if (IssueFieldConstants.ISSUE_TYPE.equals(fieldName)) {
                // issuetype is the only field whose name inside the GenericValue is different (type in this case).
                // I haven't found a generic way to map external field names to GV internal names, hence this ugly if...
                fieldName = "type";
            }
            if (updateIssueGV.getModelEntity().isField(fieldName)) {
                updateIssueGV.put(fieldName, iub.getChangedIssue().get(fieldName));
            }
        }
        // Also add the "update" field
        updateIssueGV.put(IssueFieldConstants.UPDATED, iub.getChangedIssue().get(IssueFieldConstants.UPDATED));

        final Transaction txn = Txn.begin();

        try {
            ofBizDelegator.storeAll(ImmutableList.of(updateIssueGV));

            Long issueId = iub.getOriginalIssue().getLong("id");
            final ApplicationUser user = iub.getApplicationUser();
            final GenericValue changeGroup = ChangeLogUtils.createChangeGroup(user,
                    iub.getOriginalIssue(), iub.getChangedIssue(),
                    iub.getChangeItems(), generateChangeItems);

            if (changeGroup != null && iub.getHistoryMetadata() != null) {
                // avoids a circular dependency :(
                HistoryMetadataManager historyMetadataManager = componentLocator.getComponent(HistoryMetadataManager.class);
                historyMetadataManager.saveHistoryMetadata(changeGroup.getLong("id"), user, iub.getHistoryMetadata());
            }
            //
            // set any properties passed in at update time without a event
            issuePropertySetter.setIssueProperties(user, issueId, iub.getProperties(), false);


            txn.commit();
            //
            // Don't roll your eyes at me!  Yes - we have a 3rd event being fired. This time its after the successful commit.  See
            // see JRA-59976 as for why
            //
            if (changeGroup != null) {
                txnAwareEventFactory.issueChangedEventOnCommit(issueId, user, iub.getComment(), changeGroup.getLong("id"));
            }

            //only fire events if something has changed, comment on its own counts as special case
            if (changeGroup != null || iub.getComment() != null) {
                if (iub.isDispatchEvent()) {
                    // Get the full newly updated issue from the database to make sure indexing is always accurate. (Status could be different)
                    final GenericValue updatedIssue = ofBizDelegator.findByPrimaryKey("Issue", updateIssueGV.getLong("id"));
                    // avoids a circular dependency :(
                    final IssueFactory issueFactory = componentLocator.getComponent(IssueFactory.class);
                    final Issue issue = issueFactory.getIssue(updatedIssue);
                    //
                    // dispatch events out to JIRA land so people can react to this new issue update
                    //
                    issueEventManager.dispatchRedundantEvent(iub.getEventTypeId(), issue,
                            user, iub.getComment(), iub.getWorklog(), changeGroup, iub.getParams(),
                            iub.isSendMail(), iub.isSubtasksUpdated());
                    //
                    // Publish new events
                    //
                    issueEventManager.dispatchEvent(issueEventBundleFactory.createIssueUpdateEventBundle(issue,
                            changeGroup, iub, user));
                }
            }
        } finally {
            txn.finallyRollbackIfNotCommitted();
        }
    }
}