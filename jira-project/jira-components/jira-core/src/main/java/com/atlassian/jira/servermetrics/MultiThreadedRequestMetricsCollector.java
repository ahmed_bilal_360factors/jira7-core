package com.atlassian.jira.servermetrics;

import com.google.common.base.Ticker;

import javax.annotation.ParametersAreNonnullByDefault;
import javax.annotation.concurrent.ThreadSafe;
import java.time.Duration;
import java.util.Optional;

/**
 * Manages per-thread collection of request server metrics
 */
@ParametersAreNonnullByDefault
@ThreadSafe
public class MultiThreadedRequestMetricsCollector implements ServerMetricsDetailCollector {
    private final ThreadLocal<NestedRequestsMetricsCollector> perThreadCollectors;
    private final Ticker ticker;

    public MultiThreadedRequestMetricsCollector(Ticker ticker) {
        this.ticker = ticker;
        perThreadCollectors =
                ThreadLocal.withInitial(() -> new NestedRequestsMetricsCollector(this.ticker));
    }

    public void startCollectionInCurrentThread() {
        perThreadCollectors.get().startCollectionInCurrentThread();
    }

    @Override
    public void checkpointReached(final String checkpointName) {
        perThreadCollectors.get().checkpointReached(checkpointName);
    }

    @Override
    public void checkpointReachedOnce(String checkpointName) {
        perThreadCollectors.get().checkpointReachedOnce(checkpointName);
    }

    @Override
    public void checkpointReachedOverride(String checkpointName) {
        perThreadCollectors.get().checkpointReachedOverride(checkpointName);
    }

    @Override
    public void addTimeSpentInActivity(String activityName, Duration duration) {
        perThreadCollectors.get().addTimeSpentInActivity(activityName, duration);
    }

    public void setTimeSpentInActivity(String activityName, Duration duration) {
        perThreadCollectors.get().setTimeSpentInActivity(activityName, duration);
    }

    public Optional<TimingInformation> finishCollectionInCurrentThread() {
        return perThreadCollectors.get().finishCollection();
    }
}
