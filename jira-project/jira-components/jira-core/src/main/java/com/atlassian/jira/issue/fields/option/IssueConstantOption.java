package com.atlassian.jira.issue.fields.option;

import com.atlassian.jira.issue.IssueConstant;
import com.atlassian.jira.issue.issuetype.IssueType;

public class IssueConstantOption extends AbstractOption implements Option {
    public static boolean isSubTask(Option option) {
        return option instanceof IssueConstantOption && ((IssueConstantOption) option).isSubTask();
    }

    private final IssueConstant constant;

    public IssueConstantOption(IssueConstant constant) {
        this.constant = constant;
    }

    public String getId() {
        return constant.getId();
    }

    public String getName() {
        return constant.getNameTranslation();
    }

    public String getDescription() {
        return constant.getDescTranslation();
    }

    public String getImagePath() {
        return constant.getIconUrl();
    }

    public boolean isSubTask() {
        return constant instanceof IssueType && ((IssueType) constant).isSubTask();
    }

    @Override
    public String toString() {
        return "IssueConstantOption[constant=" + constant + ']';
    }
}
