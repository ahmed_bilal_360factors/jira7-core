package com.atlassian.jira;

import com.atlassian.beehive.ClusterLockService;
import com.atlassian.beehive.db.ClusterNodeHeartbeatService;
import com.atlassian.beehive.db.spi.ClusterLockDao;
import com.atlassian.beehive.db.spi.ClusterNodeHeartBeatDao;
import com.atlassian.beehive.simple.SimpleClusterLockService;
import com.atlassian.jira.bc.dataimport.ha.ClusterImportListener;
import com.atlassian.jira.bc.dataimport.ha.ClusterImportService;
import com.atlassian.jira.bc.dataimport.ha.DefaultClusterImportService;
import com.atlassian.jira.cluster.ClusterInfo;
import com.atlassian.jira.cluster.ClusterManager;
import com.atlassian.jira.cluster.ClusterMessagingService;
import com.atlassian.jira.cluster.ClusterNodeProperties;
import com.atlassian.jira.cluster.ClusterNodePropertiesImpl;
import com.atlassian.jira.cluster.ClusterNodes;
import com.atlassian.jira.cluster.ClusterServicesManager;
import com.atlassian.jira.cluster.DatabaseClusterMessagingService;
import com.atlassian.jira.cluster.DefaultClusterManager;
import com.atlassian.jira.cluster.DefaultClusterNodes;
import com.atlassian.jira.cluster.DefaultClusterServicesManager;
import com.atlassian.jira.cluster.DefaultNodeStateManager;
import com.atlassian.jira.cluster.DefaultNodeStateService;
import com.atlassian.jira.cluster.MessageHandlerService;
import com.atlassian.jira.cluster.NodeStateManager;
import com.atlassian.jira.cluster.NodeStateService;
import com.atlassian.jira.cluster.OfBizClusterMessageStore;
import com.atlassian.jira.cluster.OfBizClusterNodeStore;
import com.atlassian.jira.cluster.OfBizMessageHandlerService;
import com.atlassian.jira.cluster.lock.JiraClusterLockDao;
import com.atlassian.jira.cluster.lock.JiraClusterNodeHeartBeatDao;
import com.atlassian.jira.cluster.lock.NullClusterNodeHeartbeatService;
import com.atlassian.jira.cluster.lock.NullJiraClusterNodeHeartBeatDao;
import com.atlassian.jira.cluster.lock.StartableClusterNodeHeartbeatService;
import com.atlassian.jira.cluster.lock.StartableDatabaseClusterLockService;
import com.atlassian.jira.cluster.lock.TimedClusterNodeHeartBeatDao;
import com.atlassian.jira.cluster.monitoring.ClusterNodeStatus;
import com.atlassian.jira.cluster.monitoring.ClusterNodeStatusMBean;
import com.atlassian.jira.cluster.zdu.ClusterStateManager;
import com.atlassian.jira.cluster.zdu.ClusterUpgradeLogger;
import com.atlassian.jira.cluster.zdu.ClusterUpgradePluginLoaderFactory;
import com.atlassian.jira.cluster.zdu.ClusterUpgradeStateDao;
import com.atlassian.jira.cluster.zdu.ClusterUpgradeStateManager;
import com.atlassian.jira.cluster.zdu.DefaultClusterUpgradePluginLoaderFactory;
import com.atlassian.jira.cluster.zdu.DefaultClusterUpgradeStateDao;
import com.atlassian.jira.cluster.zdu.DefaultClusterUpgradeStateManager;
import com.atlassian.jira.cluster.zdu.DefaultNodeBuildInfoFactory;
import com.atlassian.jira.cluster.zdu.JiraUpgradeApprovedListener;
import com.atlassian.jira.cluster.zdu.NodeBuildInfoFactory;
import com.atlassian.jira.cluster.zdu.analytics.ClusterUpgradeAnalyticsListener;
import com.atlassian.jira.index.ha.DefaultIndexCopyService;
import com.atlassian.jira.index.ha.DefaultNodeReindexService;
import com.atlassian.jira.index.ha.DefaultReplicatedIndexManager;
import com.atlassian.jira.index.ha.IndexCopyService;
import com.atlassian.jira.index.ha.IndexUtils;
import com.atlassian.jira.index.ha.NodeReindexService;
import com.atlassian.jira.index.ha.NullReplicatedIndexManager;
import com.atlassian.jira.index.ha.OfBizNodeIndexCounterStore;
import com.atlassian.jira.index.ha.OfBizReplicatedIndexOperationStore;
import com.atlassian.jira.index.ha.ReplicatedIndexManager;
import com.atlassian.jira.index.ha.SharedEntityResolver;
import com.atlassian.jira.plugin.freeze.DefaultFreezeFileManagerFactory;
import com.atlassian.jira.plugin.freeze.FreezeFileManagerFactory;
import com.atlassian.jira.plugin.ha.DefaultPluginSyncService;
import com.atlassian.jira.plugin.ha.MessageEventRegistry;
import com.atlassian.jira.plugin.ha.PluginMessageSender;
import com.atlassian.jira.plugin.ha.PluginSyncService;
import com.atlassian.jira.plugin.ha.ReplicatedPluginManager;

import static com.atlassian.jira.ComponentContainer.Scope.INTERNAL;
import static com.atlassian.jira.ComponentContainer.Scope.PROVIDED;

/**
 * Registers the cluster-related services.
 * If the system is not clustered, some alternative non-cluster services are registered.
 *
 * @since 6.3
 */
public class ClusterServicesRegistrar {
    /**
     * Registers the services with the given component container.
     *
     * @param register the container with which to register the services (required)
     */
    static void register(final ComponentContainer register) {
        register.implementation(PROVIDED, ClusterNodeProperties.class, ClusterNodePropertiesImpl.class);
        MultipleKeyRegistrant.registrantFor(DefaultClusterManager.class)
                .implementing(ClusterInfo.class)
                .implementing(ClusterManager.class)
                .registerWith(PROVIDED, register);
        register.implementation(INTERNAL, IndexCopyService.class, DefaultIndexCopyService.class);
        register.implementation(INTERNAL, SharedEntityResolver.class);
        register.implementation(INTERNAL, OfBizReplicatedIndexOperationStore.class);
        register.implementation(INTERNAL, OfBizNodeIndexCounterStore.class);
        register.implementation(INTERNAL, OfBizClusterMessageStore.class);
        register.implementation(INTERNAL, NodeReindexService.class, DefaultNodeReindexService.class);
        register.implementation(INTERNAL, IndexUtils.class);
        register.implementation(INTERNAL, MessageHandlerService.class, OfBizMessageHandlerService.class);
        register.implementation(PROVIDED, ClusterMessagingService.class, DatabaseClusterMessagingService.class);
        register.implementation(INTERNAL, PluginMessageSender.class);
        register.implementation(PROVIDED, ClusterServicesManager.class, DefaultClusterServicesManager.class);
        register.implementation(PROVIDED, NodeStateManager.class, DefaultNodeStateManager.class);
        register.implementation(INTERNAL, ClusterNodes.class, DefaultClusterNodes.class);
        register.implementation(PROVIDED, NodeStateService.class, DefaultNodeStateService.class);
        register.implementation(INTERNAL, OfBizClusterNodeStore.class);
        register.implementation(INTERNAL, MessageEventRegistry.class);
        register.implementation(INTERNAL, ReplicatedPluginManager.class);
        register.implementation(PROVIDED, PluginSyncService.class, DefaultPluginSyncService.class);
        register.implementation(PROVIDED, ClusterImportService.class, DefaultClusterImportService.class);
        register.implementation(INTERNAL, ClusterImportListener.class);
        register.implementation(INTERNAL, ClusterUpgradeStateDao.class, DefaultClusterUpgradeStateDao.class);
        MultipleKeyRegistrant.registrantFor(DefaultClusterUpgradeStateManager.class)
                .implementing(ClusterUpgradeStateManager.class)
                .implementing(ClusterStateManager.class)
                .registerWith(PROVIDED, register);
        register.implementation(INTERNAL, JiraUpgradeApprovedListener.class);
        register.implementation(INTERNAL, ClusterUpgradeLogger.class);
        register.implementation(INTERNAL, ClusterNodeStatusMBean.class, ClusterNodeStatus.class);
        register.implementation(INTERNAL, ClusterUpgradePluginLoaderFactory.class, DefaultClusterUpgradePluginLoaderFactory.class);
        register.implementation(INTERNAL, NodeBuildInfoFactory.class, DefaultNodeBuildInfoFactory.class);
        register.implementation(INTERNAL, FreezeFileManagerFactory.class, DefaultFreezeFileManagerFactory.class);

        if (isClustered(register)) {
            registerDatabaseLockService(register);
            registerClusteredReplicatedIndexManager(register);
            registerClusterUpgradeAnalytics(register);
        } else {
            registerJvmLockService(register);
            registerNonClusteredReplicatedIndexManager(register);
        }
    }

    private static boolean isClustered(final ComponentContainer register) {
        final ClusterNodeProperties clusterNodeProperties = register.getComponentInstance(ClusterNodeProperties.class);
        return clusterNodeProperties != null && clusterNodeProperties.getNodeId() != null;
    }

    private static void registerDatabaseLockService(final ComponentContainer register) {
        // required dependency for ClusterLockService
        register.implementation(INTERNAL, ClusterNodeHeartbeatService.class, StartableClusterNodeHeartbeatService.class);
        // ClusterLock Service
        register.implementation(PROVIDED, ClusterLockService.class, StartableDatabaseClusterLockService.class);
        // DAOs for the DatabaseClusterLockService
        register.implementation(INTERNAL, ClusterLockDao.class, JiraClusterLockDao.class);
        registerHeartbeatDao(register, JiraClusterNodeHeartBeatDao.class);
    }

    private static void registerJvmLockService(final ComponentContainer register) {
        register.implementation(PROVIDED, ClusterLockService.class, SimpleClusterLockService.class);
        register.implementation(INTERNAL, ClusterNodeHeartbeatService.class, NullClusterNodeHeartbeatService.class);
        registerHeartbeatDao(register, NullJiraClusterNodeHeartBeatDao.class);
    }

    private static void registerHeartbeatDao(final ComponentContainer register,
                                             final Class<? extends TimedClusterNodeHeartBeatDao> implClass) {
        MultipleKeyRegistrant.registrantFor(implClass)
                .implementing(ClusterNodeHeartBeatDao.class)
                .implementing(TimedClusterNodeHeartBeatDao.class)
                .registerWith(INTERNAL, register);
    }

    /**
     * Register a replicated index manager for use in a cluster
     *
     * @param register
     */
    private static void registerClusteredReplicatedIndexManager(final ComponentContainer register) {
        register.implementation(PROVIDED, ReplicatedIndexManager.class, DefaultReplicatedIndexManager.class);
    }

    /**
     * Register a replicated index manager for use in a non-clustered environment.
     *
     * @param register
     */
    private static void registerNonClusteredReplicatedIndexManager(final ComponentContainer register) {
        register.implementation(PROVIDED, ReplicatedIndexManager.class, NullReplicatedIndexManager.class);
    }

    /**
     * Register the events listener to track zero-downtime upgrades and produce analytics. This shouldn't exist at all
     * outside of clustered mode.
     *
     * @param register
     */
    private static void registerClusterUpgradeAnalytics(ComponentContainer register) {
        register.implementation(INTERNAL, ClusterUpgradeAnalyticsListener.class);
    }
}
