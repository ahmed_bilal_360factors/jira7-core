package com.atlassian.jira.plugin.freeze;

import com.atlassian.plugin.PluginException;
import com.atlassian.plugin.loaders.classloading.DeploymentUnit;
import com.atlassian.plugin.loaders.classloading.Scanner;
import com.google.common.collect.ImmutableList;

import java.util.Collection;
import java.util.Set;

import static com.google.common.collect.ImmutableList.copyOf;
import static com.google.common.collect.Sets.newTreeSet;

/**
 * Switching scanner that allows switching of the underlying scanner at runtime.
 * Handles switching correctly by adjusting the results of the {@link #scan()} method
 * after a switch.
 *
 * @since v7.3
 */
public class SwitchingScanner implements Scanner {
    private final Scanner originalScanner;
    private final Scanner alternateScanner;
    private volatile Scanner activeScanner;

    private Collection<DeploymentUnit> unitsForNextScan;
    private Collection<DeploymentUnit> unitsAtLastScan;

    public SwitchingScanner(final Scanner originalScanner, final Scanner alternateScanner) {
        this.originalScanner = originalScanner;
        this.alternateScanner = alternateScanner;
        this.activeScanner = originalScanner;
    }

    public Scanner getActiveScanner() {
        return activeScanner;
    }

    public boolean isAlternate() {
        return activeScanner == alternateScanner;
    }

    public void switchTo(final boolean alternate) {
        switchTo(alternate ? this.alternateScanner : this.originalScanner);
    }

    private synchronized void switchTo(final Scanner scanner) {
        if (unitsAtLastScan != null) {
            Set<DeploymentUnit> oldSet = newTreeSet(unitsAtLastScan);
            scanner.scan();
            Set<DeploymentUnit> difference = newTreeSet(scanner.getDeploymentUnits());
            difference.removeAll(oldSet);
            unitsForNextScan = difference;
        }
        activeScanner = scanner;
    }

    @Override
    public synchronized Collection<DeploymentUnit> scan() {
        Scanner activeScanner = this.activeScanner;
        Collection<DeploymentUnit> scan = activeScanner.scan();
        unitsAtLastScan = copyOf(activeScanner.getDeploymentUnits());
        if (unitsForNextScan != null) {
            ImmutableList<DeploymentUnit> ret = ImmutableList.<DeploymentUnit>builder()
                    .addAll(unitsForNextScan)
                    .addAll(scan)
                    .build();
            unitsForNextScan = null;
            return ret;
        }
        return scan;
    }

    @Override
    public Collection<DeploymentUnit> getDeploymentUnits() {
        return activeScanner.getDeploymentUnits();
    }

    @Override
    public void reset() {
        activeScanner.reset();
    }

    @Override
    public void remove(final DeploymentUnit unit) throws PluginException {
        activeScanner.remove(unit);
    }
}
