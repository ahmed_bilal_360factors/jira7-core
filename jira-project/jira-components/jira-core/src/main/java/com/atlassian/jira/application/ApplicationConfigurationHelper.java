package com.atlassian.jira.application;

import com.atlassian.application.api.Application;
import com.atlassian.application.api.ApplicationKey;
import com.atlassian.application.api.ApplicationManager;
import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.crowd.embedded.impl.IdentifierUtils;
import com.atlassian.crowd.exception.CrowdException;
import com.atlassian.crowd.exception.OperationNotPermittedException;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.fugue.Option;
import com.atlassian.jira.config.group.GroupConfigurationIdentifier;
import com.atlassian.jira.license.JiraLicenseManager;
import com.atlassian.jira.license.LicenseChangedEvent;
import com.atlassian.jira.license.LicenseDetails;
import com.atlassian.jira.message.MessageUtil;
import com.atlassian.jira.message.MessageUtil.Factory;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.permission.GlobalPermissionType;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.johnson.JohnsonProvider;
import com.atlassian.johnson.JohnsonEventContainer;
import com.atlassian.johnson.event.Event;
import com.google.common.base.Supplier;
import com.google.common.collect.Sets;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.Collection;
import java.util.Collections;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static com.atlassian.jira.permission.GlobalPermissionKey.ADMINISTER;
import static com.atlassian.jira.util.dbc.Assertions.notNull;
import static com.google.common.collect.Sets.newHashSet;

/**
 * Used to configure JIRA Applications (for example JIRA Software, JIRA Core, JIRA Service Desk) with default
 * configurations such as default groups, default global permissions, admin access to applications and configuration of
 * instance's default application; used during user creation.
 *
 * @since v7.0
 */
@ParametersAreNonnullByDefault
public class ApplicationConfigurationHelper {
    private static final Logger log = LoggerFactory.getLogger(ApplicationConfigurationHelper.class);

    private final ApplicationManager applicationManager;
    private final GroupManager groupManager;
    private final ApplicationRoleManager applicationRoleManager;
    private final GlobalPermissionManager globalPermissionManager;
    private final UserManager userManager;
    private final ApplicationRoleStore applicationRoleStore;
    private final MessageUtil.Factory messageUtilFactory;
    private final GroupConfigurationIdentifier groupConfigurationIdentifier;
    private final Supplier<Set<GlobalPermissionType>> globalPermissionTypeSupplier;
    private final EventPublisher eventPublisher;
    private final JiraAuthenticationContext authenticationContext;
    private final JiraLicenseManager licenseManager;
    private final JohnsonProvider johnsonProvider;

    private static final String IMPORT_EVENT_TYPE = "import";

    public ApplicationConfigurationHelper(
            final ApplicationManager applicationManager,
            final GroupManager groupManager,
            final ApplicationRoleManager applicationRoleManager,
            final GlobalPermissionManager globalPermissionManager,
            final UserManager userManager,
            final ApplicationRoleStore applicationRoleStore,
            final Factory messageUtilFactory,
            final GroupConfigurationIdentifier groupConfigurationIdentifier,
            final EventPublisher eventPublisher,
            final JiraAuthenticationContext authenticationContext,
            final JiraLicenseManager jiraLicenseManager,
            final JohnsonProvider johnsonProvider) {
        this.applicationRoleManager = notNull("applicationRoleManager", applicationRoleManager);
        this.groupManager = notNull("groupManager", groupManager);
        this.applicationManager = notNull("applicationManager", applicationManager);
        this.globalPermissionManager = notNull("globalPermissionManager", globalPermissionManager);
        this.userManager = notNull("userManager", userManager);
        this.applicationRoleStore = notNull("applicationRoleStore", applicationRoleStore);
        this.messageUtilFactory = notNull("messageUtilFactory", messageUtilFactory);
        this.groupConfigurationIdentifier = notNull("groupConfigurationIdentifier", groupConfigurationIdentifier);
        this.globalPermissionTypeSupplier = new DefaultGlobalPermissionsSupplier();
        this.eventPublisher = notNull("eventPublisher", eventPublisher);
        this.authenticationContext = notNull("authenticationContext", authenticationContext);
        this.licenseManager = notNull("jiraLicenseManager", jiraLicenseManager);
        this.johnsonProvider = notNull("jiraLicenseManager", johnsonProvider);
    }

    /**
     * Validate whether the specified application can be configured. This will also determine whether default group can
     * be created by the provided user (or anonymously if no user has been provided).
     *
     * @param applicationKey key identifying application.
     * @param user           user performing configuration or null if this is not being performed by a user.
     * @return an {@link Optional} with an warning message if validation did not pass or {@link Optional#empty()} if
     * there are no warning.
     */
    public Optional<String> validateApplicationForConfiguration(final ApplicationKey applicationKey,
                                                                @Nullable final ApplicationUser user) {
        notNull("applicationKey", applicationKey);

        final MessageUtil msgUtil = user != null ?
                messageUtilFactory.getNewInstance(user) :
                messageUtilFactory.getNewInstance();

        if (noConfigPermission(user)) {
            return Optional.of(msgUtil.getText("application.installation.configuration.not.admin"));
        }

        if (doesApplicationHaveActiveDefaultGroupConfigured(applicationKey)) {
            //Restoring existing configuration
            return Optional.empty();
        }

        final String defaultGroupName = getAvailableDefaultGroupName(applicationKey)
                .orElse(getDefaultGroupName(applicationKey));
        final String applicationDisplayName = getApplicationDisplayName(applicationKey);

        if (groupManager.groupExists(defaultGroupName)) {
            return Optional.of(msgUtil.getText("application.installation.configuration.group.exist",
                    defaultGroupName, applicationDisplayName,
                    msgUtil.getAnchorTagWithInternalLink("app_access_local")));
        }

        if (groupConfigurationIdentifier.groupHasExistingConfiguration(defaultGroupName)) {
            return Optional.of(msgUtil.getText("application.installation.configuration.group.config.exist",
                    defaultGroupName, applicationDisplayName,
                    msgUtil.getAnchorTagWithInternalLink("app_access_local")));
        }

        if (!userManager.hasGroupWritableDirectory()) {
            return Optional.of(msgUtil.getText("application.installation.configuration.use.to.app",
                    msgUtil.getUrl("user-app-access").getUrl(), applicationDisplayName));
        }

        return Optional.empty();
    }

    /**
     * Performs initial configuration for applications during JIRA Setup.
     * <ul>
     * <li>Create the default group(s) for all the application role(s).</li>
     * <li>Add the additionalGroups to all the application role(s).</li>
     * <li>Grant all admin groups the default application global permissions</li>
     * </ul>
     *
     * @param additionalGroups Groups that will be added to all the licensed applications. They won't be added as
     *                         defaults.
     * @param publishEvent     If true then {@link ApplicationConfigurationEvent} will be published if there were any
     *                         changes made by this method. This is especially handy during initial setup, where sending events might
     *                         cause unwanted actions (like User Management sync)
     */
    public void configureApplicationsForSetup(final Collection<Group> additionalGroups, boolean publishEvent) {
        notNull("additionalGroups", additionalGroups);
        // Default groups are set for every licensed application regardless if it's installed or not
        final Set<ApplicationKey> confApps = StreamSupport.stream(licenseManager.getLicenses().spliterator(), false)
                .flatMap(licenseDetails -> configureApplicationForLicense(licenseDetails, Optional.empty()).stream())
                .collect(Collectors.toSet());
        applicationRoleManager.getRoles()
                .forEach(role -> addGroupsToRole(role, Optional.empty(), newHashSet(additionalGroups), true));
        configureAdminGroupsForGlobalPermissions();
        if (publishEvent) {
            publishAppConfigEvent(confApps);
        }
    }

    /**
     * Performs initial configuration for applications during JIRA Import. This action is performed anonymously as the
     * user performing the import might no longer exist after import.
     */
    public void configureApplicationsForImport(final LicenseDetails licenseDetailsForImport) {
        publishAppConfigEvent(configureApplicationForLicense(licenseDetailsForImport, Optional.empty()));
    }

    /**
     * Configure the passed admin for all the default applications by adding the admin to all the applications default
     * groups. This method is only useful if the applications in JIRA are configured with default groups.
     *
     * @param user the user to configure.
     */
    public void setupAdminForDefaultApplications(final ApplicationUser user) {
        notNull("user", user);
        applicationRoleManager.getRoles().forEach(role ->
                role.getDefaultGroups().forEach(group ->
                        addUserToGroup(user, group)));
    }

    public void configureLicense(LicenseChangedEvent event) {
        if (!isImportInProgress()) {
            if (event.getNewLicenseDetails().isDefined()) {
                configureApplicationForLicense(event.getNewLicenseDetails().get(),
                        Optional.ofNullable(authenticationContext.getLoggedInUser()));
            }
            publishAppConfigEventForLicense(event);
        }
    }

    private boolean isImportInProgress() {
        final JohnsonEventContainer container = johnsonProvider.getContainer();
        for (Event event : container.getEvents()) {
            if (IMPORT_EVENT_TYPE.equals(event.getKey().getType())) {
                log.info("Received new license while import in progress, skipping application configuration.");
                return true;
            }
        }
        return false;
    }

    private void publishAppConfigEventForLicense(final LicenseChangedEvent event) {
        final Set<ApplicationKey> configuredApps = Sets.newHashSet();
        if (event.getNewLicenseDetails().isDefined()) {
            configuredApps.addAll(event.getNewLicenseDetails().get().getLicensedApplications().getKeys());
        }
        if (event.getPreviousLicenseDetails().isDefined()) {
            configuredApps.addAll(event.getPreviousLicenseDetails().get().getLicensedApplications().getKeys());
        }
        publishAppConfigEvent(configuredApps);
    }

    private Set<ApplicationKey> configureApplicationForLicense(final LicenseDetails licenseDetails,
                                                               Optional<ApplicationUser> optionalUser) {
        notNull("licenseDetails", licenseDetails);
        if (!userManager.hasGroupWritableDirectory()) {
            log.warn("There are no writable directories to create the default group for the new license.");
            return Collections.emptySet();
        }

        final Set<ApplicationKey> configuredApps = Sets.newHashSet();
        for (final ApplicationKey key : licenseDetails.getLicensedApplications().getKeys()) {
            if (!doesApplicationHaveActiveDefaultGroupConfigured(key)) {
                if (configureApplicationWithDefaultGroup(key, optionalUser)) {
                    configuredApps.add(key);
                } else {
                    log.info("Unable to perform application configuration for {}.", key.toString());
                }
            } else {
                log.info("Application already configured for {}.", key.toString());
            }
        }
        return configuredApps;
    }

    private boolean configureApplicationWithDefaultGroup(final ApplicationKey key,
                                                         final Optional<ApplicationUser> optionalUser) {
        log.info("Attempting to perform application configuration for {}.", key.toString());
        final Option<ApplicationRole> roleOption = applicationRoleManager.getRole(key);
        if (roleOption.isDefined()) {
            final Optional<String> optionGroupName = getAvailableDefaultGroupName(key);
            if (!optionGroupName.isPresent()) {
                return false;
            }
            final Group defaultGroup = getOrCreateGroup(optionGroupName.get());
            if (defaultGroup != null) {
                final boolean groupAlreadyConfigured = applicationRoleStore.get(key).getDefaultGroups().stream()
                        .anyMatch(groupName -> groupName.equals(toGroupName(defaultGroup.getName())));
                if (groupAlreadyConfigured) {
                    log.info("Application already {} already configured with group {}.", key.toString(), defaultGroup.getName());
                    return false;
                }

                addGroupsToRole(roleOption.get(), Optional.of(defaultGroup), Collections.emptySet(), false);
                configureGroupForGlobalPermissions(defaultGroup);
                optionalUser.ifPresent(user -> addUserToGroup(user, defaultGroup));
                log.info("Application configured for {}.", key.toString());
                return true;
            }
        }
        return false;
    }

    /**
     * Adds the default application global permissions to the given group of the Application.
     * Default configuration is adding CREATE_SHARED_OBJECTS, MANAGE_GROUP_FILTER_SUBSCRIPTIONS, BULK_CHANGE
     * and USER_PICKER permissions
     */
    private void configureGroupForGlobalPermissions(final Group group) {
        globalPermissionTypeSupplier.get()
                .forEach(gpt -> addPermissionToGroupIfItHasNotBeenGrantedYet(gpt, group));
    }

    private void addPermissionToGroupIfItHasNotBeenGrantedYet(final GlobalPermissionType globalPermissionType, final Group group) {
        if (globalPermissionManager.getGroupsWithPermission(globalPermissionType.getGlobalPermissionKey()).contains(group)) {
            return;
        }
        globalPermissionManager.addPermission(globalPermissionType, toGroupName(group.getName()));

    }

    /**
     * Adds the default global permission to all admin groups of the Application.
     * {@link this#configureGroupForGlobalPermissions(com.atlassian.crowd.embedded.api.Group)}
     */
    private void configureAdminGroupsForGlobalPermissions() {
        globalPermissionManager.getGroupsWithPermission(GlobalPermissionKey.ADMINISTER)
                .forEach(adminGroup -> configureGroupForGlobalPermissions(adminGroup));
    }

    private void publishAppConfigEvent(final Set<ApplicationKey> confApps) {
        if (!confApps.isEmpty()) {
            eventPublisher.publish(new ApplicationConfigurationEvent(confApps));
        }
    }

    private ApplicationRole addGroupsToRole(final ApplicationRole role, final Optional<Group> defaultGroup,
                                            final Set<Group> additionalGroups, final boolean forceIsSelectedByDefault) {
        final Set<Group> groups = newHashSet(role.getGroups());
        groups.addAll(additionalGroups);
        defaultGroup.ifPresent(groups::add);

        final Set<Group> defaultGroups = newHashSet(role.getDefaultGroups());
        defaultGroup.ifPresent(defaultGroups::add);

        return applicationRoleManager.setRole(role.withGroups(groups, defaultGroups)
                .withSelectedByDefault(forceIsSelectedByDefault || role.isSelectedByDefault()));
    }

    private boolean noConfigPermission(final @Nullable ApplicationUser user) {
        return (user != null) && !globalPermissionManager.hasPermission(ADMINISTER, user);
    }

    private String getApplicationDisplayName(final ApplicationKey applicationKey) {
        return applicationManager.getApplication(applicationKey).map(Application::getName)
                .getOrElse(applicationRoleManager.getRole(applicationKey).map(ApplicationRole::getName)
                        .getOrElse(applicationKey.toString()));
    }

    private Optional<String> getAvailableDefaultGroupName(final ApplicationKey applicationKey) {
        // First try the default
        final String defaultGroupName = getDefaultGroupName(applicationKey);
        if (isGroupNameAvailable(defaultGroupName)) {
            return Optional.of(defaultGroupName);
        }
        // Try next available group name
        for (int groupCounter = 1; groupCounter <= 5; groupCounter++) {
            final String groupWithInc = String.format("%s-%d", defaultGroupName, groupCounter);
            if (isGroupNameAvailable(groupWithInc)) {
                return Optional.of(groupWithInc);
            }
        }
        // Try ugly group name that is even more unlikely to be used
        final String dateString = new DateTime().toString(DateTimeFormat.forPattern("yyyyMMdd"));
        for (int i = 1; i <= 999; i++) {
            final String uglyGroupName = String.format("%s-%s-%s-%d", defaultGroupName, "auto", dateString, i);
            if (isGroupNameAvailable(uglyGroupName)) {
                return Optional.of(uglyGroupName);
            }
        }
        log.warn(String.format("The default group name [%s] for [%2$s] already exist. "
                        + "Unable to provide an unused group name for [%2$s].", defaultGroupName,
                getApplicationDisplayName(applicationKey)));
        return Optional.empty();
    }

    private boolean isGroupNameAvailable(final String defaultGroupName) {
        return !groupManager.groupExists(defaultGroupName) &&
                !groupConfigurationIdentifier.groupHasExistingConfiguration(defaultGroupName);
    }

    private String getDefaultGroupName(final ApplicationKey applicationKey) {
        return toGroupName(applicationManager.getApplication(applicationKey).map(Application::getDefaultGroup)
                .getOrElse((Supplier<String>) () -> String.format("%s-users", applicationKey.value())));
    }

    private String toGroupName(final String groupName) {
        return IdentifierUtils.toLowerCase(groupName);
    }

    private boolean doesApplicationHaveActiveDefaultGroupConfigured(final ApplicationKey applicationKey) {
        return applicationRoleStore.get(applicationKey).getDefaultGroups().stream()
                .anyMatch(groupManager::groupExists);
    }

    private Group getOrCreateGroup(String groupName) {
        final Group group = groupManager.getGroup(groupName);
        if (group != null) {
            return group;
        }
        try {
            return groupManager.createGroup(groupName);
        } catch (com.atlassian.crowd.exception.embedded.InvalidGroupException e) {
            // groupManager.getGroup reads the cache only, the group might be in the directory server already.
            // this is likely to occur during syncing.
            if ("Group already exists".equals(e.getMessage())) {
                return e.getGroup();
            } else {
                log.warn("Unable to create group during application configuration.", e);
            }
        } catch (OperationNotPermittedException e) {
            log.warn("Unable to create group during application configuration.", e);
        }
        return null;
    }

    private void addUserToGroup(final ApplicationUser user, final Group group) {
        if (!groupManager.isUserInGroup(user, group)) {
            try {
                groupManager.addUserToGroup(user, group);
            } catch (CrowdException e) {
                log.warn("Unable to add user to group during application configuration.", e);
            }
        }
    }

    private class DefaultGlobalPermissionsSupplier implements Supplier<Set<GlobalPermissionType>> {
        @Override
        public Set<GlobalPermissionType> get() {
            return GlobalPermissionKey.DEFAULT_APP_GLOBAL_PERMISSIONS.stream()
                    .map(globalPermissionManager::getGlobalPermission)
                    .map(Option::getOrNull)
                    .filter(Objects::nonNull)
                    .collect(Collectors.toSet());
        }
    }
}