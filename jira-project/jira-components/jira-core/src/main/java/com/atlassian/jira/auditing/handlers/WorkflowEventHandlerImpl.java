package com.atlassian.jira.auditing.handlers;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.atlassian.collectors.CollectorsUtil;
import com.atlassian.fugue.Option;
import com.atlassian.jira.auditing.AssociatedItem;
import com.atlassian.jira.auditing.AuditingCategory;
import com.atlassian.jira.auditing.ChangedValue;
import com.atlassian.jira.auditing.RecordRequest;
import com.atlassian.jira.event.DraftWorkflowPublishedEvent;
import com.atlassian.jira.event.WorkflowCopiedEvent;
import com.atlassian.jira.event.WorkflowCreatedEvent;
import com.atlassian.jira.event.WorkflowDeletedEvent;
import com.atlassian.jira.event.WorkflowRenamedEvent;
import com.atlassian.jira.event.WorkflowUpdatedEvent;
import com.atlassian.jira.workflow.JiraWorkflow;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Sets;
import com.opensymphony.workflow.loader.ResultDescriptor;
import com.opensymphony.workflow.loader.StepDescriptor;

import static com.atlassian.jira.workflow.JiraWorkflow.STEP_STATUS_KEY;

/**
 * @since v6.2
 */
public class WorkflowEventHandlerImpl implements WorkflowEventHandler {

    @Override
    public RecordRequest onWorkflowCreatedEvent(final WorkflowCreatedEvent event) {
        return workflowCreated(event.getWorkflow());
    }

    @Override
    public RecordRequest onWorkflowCopiedEvent(final WorkflowCopiedEvent event) {
        return workflowCreated(event.getNewWorkflow());
    }

    @Override
    public Option<RecordRequest> onWorkflowUpdatedEvent(WorkflowUpdatedEvent event) {
        if (event.getWorkflow().isDraftWorkflow()) {
            return Option.none(); //for now we ignore any changes to draft workflows
        } else {
            return Option.some(workflowUpdated(event.getOriginalWorkflow(), event.getWorkflow()));
        }
    }

    @Override
    public RecordRequest onDraftWorkflowPublishedEvent(DraftWorkflowPublishedEvent event) {
        return workflowUpdated(event.getOriginalWorkflow(), event.getWorkflow());
    }

    @Override
    public RecordRequest onWorkflowRenamedEvent(WorkflowRenamedEvent event) {
        return new RecordRequest(AuditingCategory.WORKFLOWS, "jira.auditing.workflow.renamed")
                .forObject(AssociatedItem.Type.WORKFLOW, event.getNewWorkflowName(), event.getNewWorkflowName())
                .withChangedValues(new ChangedValuesBuilder().addIfDifferent("common.words.name", event.getOldWorkflowName(), event.getNewWorkflowName()).build());
    }

    @Override
    public RecordRequest onWorkflowDeletedEvent(final WorkflowDeletedEvent event) {
        return new RecordRequest(AuditingCategory.WORKFLOWS, "jira.auditing.workflow.deleted")
                .forObject(AssociatedItem.Type.WORKFLOW, event.getWorkflow().getDisplayName(), event.getWorkflow().getName());
    }

    private RecordRequest workflowCreated(final JiraWorkflow workflow) {
        return new RecordRequest(AuditingCategory.WORKFLOWS, "jira.auditing.workflow.created")
                .forObject(AssociatedItem.Type.WORKFLOW, workflow.getDisplayName(), workflow.getName())
                .withChangedValues(computeChangedValues(workflow));
    }

    private RecordRequest workflowUpdated(JiraWorkflow fromWorkflow, final JiraWorkflow toWorkflow) {
        return new RecordRequest(AuditingCategory.WORKFLOWS, "jira.auditing.workflow.updated")
                .forObject(AssociatedItem.Type.WORKFLOW, toWorkflow.getDisplayName(), toWorkflow.getName())
                .withChangedValues(computeChangedValues(fromWorkflow, toWorkflow));
    }

    private ImmutableList<ChangedValue> computeChangedValues(final JiraWorkflow currentWorkflow) {
        return computeChangedValues(null, currentWorkflow);
    }

    private ImmutableList<ChangedValue> computeChangedValues(final JiraWorkflow originalWorkflow, final JiraWorkflow currentWorkflow) {
        final ChangedValuesBuilder changedValues = new ChangedValuesBuilder();

        changedValues.addIfDifferent("common.words.name", originalWorkflow == null ? null : originalWorkflow.getDisplayName(), currentWorkflow.getDisplayName())
                .addIfDifferent("common.words.description", originalWorkflow == null ? null : originalWorkflow.getDescription(), currentWorkflow.getDescription());

        diffSet(changedValues, "common.words.status", getLinkedStatuses(originalWorkflow), getLinkedStatuses(currentWorkflow));
        diffSet(changedValues, "admin.workflowtransition.transition", getActions(originalWorkflow), getActions(currentWorkflow));

        return changedValues.build();
    }

    private void diffSet(ChangedValuesBuilder changedValues, String name, Set<ChangeItem> oldList, Set<ChangeItem> newList) {
        Sets.difference(oldList, newList)
                .forEach(removedStatus -> changedValues.add(name, removedStatus.name, null));

        Sets.difference(newList, oldList)
                .forEach(addedStatus -> changedValues.add(name, null, addedStatus.name));

        Sets.intersection(newList, oldList)
                .forEach(addedStatus -> {
                    final String oldName = oldList.stream().filter(addedStatus::equals).findFirst().map(item -> item.name).orElse(null);
                    changedValues.addIfDifferent(name, oldName, addedStatus.name);
                });
    }

    private Set<ChangeItem> getLinkedStatuses(final JiraWorkflow workflow) {
        return Optional.ofNullable(workflow).map(JiraWorkflow::getLinkedStatusObjects).orElse(ImmutableList.of()).stream()
                    .map(status -> {
                        final Optional<String> stepName = findStatusName(workflow, step -> status.getId().equals(step.getMetaAttributes().getOrDefault(STEP_STATUS_KEY, null)));
                        return new ChangeItem(status.getId(), stepName.orElse(status.getName()));
                    })
                .collect(CollectorsUtil.toImmutableSet());
    }

    private Set<ChangeItem> getActions(final JiraWorkflow workflow) {
        return Optional.ofNullable(workflow).map(JiraWorkflow::getAllActions).orElse(ImmutableList.of()).stream()
                .map(action -> {
                    final ResultDescriptor result = action.getUnconditionalResult();

                    final Optional<String> sourceStepName;
                    if (workflow.isGlobalAction(action)) {
                        sourceStepName = Optional.empty();
                    } else if (workflow.isCommonAction(action)) {
                        final Stream<String> stepNames = workflow.getStepsForTransition(action).stream().map(StepDescriptor::getName);
                        sourceStepName = Optional.of(String.format("[%s]", stepNames.collect(Collectors.joining(", "))));
                    } else {
                        sourceStepName = workflow.getStepsForTransition(action).stream().findAny().map(StepDescriptor::getName);
                    }

                    final Optional<String> targetStepName = findStatusName(workflow, step -> result.getStep() == step.getId());

                    final String changeName;

                    if (sourceStepName.isPresent() && targetStepName.isPresent()) {
                        changeName = String.format("%s (%s -> %s)", action.getName(), sourceStepName.orElse(""), targetStepName.orElse(""));
                    } else if (sourceStepName.isPresent() || targetStepName.isPresent()) {
                        // very unlikely scenario, a bug?
                        changeName = String.format("%s (%s)", action.getName(), sourceStepName.orElse(targetStepName.orElse("Unknown")));
                    } else {
                        changeName = action.getName();
                    }

                    return new ChangeItem(Integer.toString(action.getId()), changeName);
                }).collect(CollectorsUtil.toImmutableSet());
    }

    /**
     * JiraWorklow contains statuses that are not immutable, so on status name change we get same name in original and current workflow.
     */
    private Optional<String> findStatusName(final JiraWorkflow workflow, final Predicate<? super StepDescriptor> filterFn) {
        return ((List<StepDescriptor>) workflow.getDescriptor().getSteps()).stream().filter(filterFn).findAny().map(StepDescriptor::getName);
    }

    static class ChangeItem {
        final String id;
        final String name;

        ChangeItem(final String id, final String name) {
            this.id = id;
            this.name = name;
        }

        @Override
        public boolean equals(final Object o) {
            if (this == o) {
                return true;
            }

            if (o == null || getClass() != o.getClass()) {
                return false;
            }
            
            final ChangeItem that = (ChangeItem) o;
            return Objects.equals(id, that.id);
        }

        @Override
        public int hashCode() {
            return Objects.hash(id);
        }
    }
}
