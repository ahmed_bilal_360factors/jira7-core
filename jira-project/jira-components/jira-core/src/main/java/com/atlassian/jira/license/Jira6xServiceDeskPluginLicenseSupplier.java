package com.atlassian.jira.license;

import com.atlassian.cache.Supplier;
import com.atlassian.extras.api.LicenseException;
import com.atlassian.fugue.Option;
import com.atlassian.jira.application.ApplicationKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.google.common.annotations.VisibleForTesting;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.inject.Inject;

import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * Supplies a plugin license for Service Desk version 1 or 2.
 * <p>
 * NOTE: This class is intended to be used solely for the purposes of checking & migrating pre-7.0
 * Service Desk licenses. It is not a general-purposes SD license supplier, instead, see
 * {@link com.atlassian.jira.license.JiraLicenseManager#getLicense(com.atlassian.application.api.ApplicationKey).
 * </p>
 *
 * @see com.atlassian.jira.license.JiraLicenseManager#getLicense(com.atlassian.application.api.ApplicationKey)
 * @see com.atlassian.jira.application.ApplicationRoleManager
 * @since 7.0
 */
public class Jira6xServiceDeskPluginLicenseSupplier implements Supplier<Option<LicenseDetails>> {
    private static final Logger LOG = LoggerFactory.getLogger(Jira6xServiceDeskPluginLicenseSupplier.class);

    private final Jira6xServiceDeskPluginEncodedLicenseSupplier encodedLicenseSupplier;
    private final LicenseDetailsFactory licenseDetailsFactory;

    @Inject
    public Jira6xServiceDeskPluginLicenseSupplier(@Nonnull ApplicationProperties applicationProperties,
                                                  @Nonnull LicenseDetailsFactory licenseDetailsFactory) {
        this(new Jira6xServiceDeskPluginEncodedLicenseSupplier(notNull("applicationProperties", applicationProperties)),
                licenseDetailsFactory);
    }

    @VisibleForTesting
    Jira6xServiceDeskPluginLicenseSupplier(@Nonnull Jira6xServiceDeskPluginEncodedLicenseSupplier raw,
                                           @Nonnull LicenseDetailsFactory licenseDetailsFactory) {
        this.encodedLicenseSupplier = notNull("raw", raw);
        this.licenseDetailsFactory = notNull("licenseDetailsFactory", licenseDetailsFactory);
    }

    /**
     * @see com.atlassian.jira.upgrade.tasks.role.Move6xServiceDeskLicenseTo70Store
     */
    @Override
    public Option<LicenseDetails> get() {
        try {
            return encodedLicenseSupplier.get()
                    .map(licenseDetailsFactory::getLicense)
                    .filter(Jira6xServiceDeskPluginLicenseSupplier::isServiceDeskLicense);
        } catch (LicenseException invalidLicense) {
            LOG.debug("Invalid Service Desk plugin license", invalidLicense);
            return Option.none();
        }
    }

    /**
     * Move the license from the UPM store into a location for upgrade. This logically removes the license from JIRA
     * but leaves it accessible to upgrade tasks.
     *
     * @see {@link Jira6xServiceDeskPluginEncodedLicenseSupplier} for details.
     */
    public void moveToUpgradeStore() {
        encodedLicenseSupplier.moveToUpgradeStore();
    }

    private static boolean isServiceDeskLicense(LicenseDetails licenseDetails) {
        if (!licenseDetails.hasApplication(ApplicationKeys.SERVICE_DESK)) {
            LOG.debug("Invalid Service Desk plugin license");
            return false;
        } else {
            return true;
        }
    }
}
