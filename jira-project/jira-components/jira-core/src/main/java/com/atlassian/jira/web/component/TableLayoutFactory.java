package com.atlassian.jira.web.component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.bean.SubTaskBean;
import com.atlassian.jira.config.SubTaskService;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueFieldConstants;
import com.atlassian.jira.issue.export.ExportableSystemField;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.Field;
import com.atlassian.jira.issue.fields.FieldException;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.fields.FieldRenderingContext;
import com.atlassian.jira.issue.fields.NavigableField;
import com.atlassian.jira.issue.fields.layout.column.ColumnLayoutItem;
import com.atlassian.jira.issue.fields.layout.column.ColumnLayoutStorageException;
import com.atlassian.jira.issue.fields.layout.column.ExcelColumnLayoutItem;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.issue.search.util.SearchSortUtil;
import com.atlassian.jira.jql.context.QueryContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.web.component.subtask.ColumnLayoutItemFactory;
import com.atlassian.query.Query;
import com.atlassian.query.order.SearchSort;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.Lists;

import org.apache.commons.collections.CollectionUtils;

/**
 * A class for creating {@link IssueTableLayoutBean} objects.
 */
public class TableLayoutFactory {
    private final FieldManager fieldManager;
    private final TableLayoutUtils tableLayoutUtils;
    private final ColumnLayoutItemFactory columnLayoutItemFactory;
    private final SearchService searchService;
    private final SearchSortUtil searchSortUtil;
    private final SubTaskService subtasks;

    public TableLayoutFactory(FieldManager fieldManager,
                              TableLayoutUtils tableLayoutUtils,
                              ColumnLayoutItemFactory columnLayoutItemFactory,
                              SearchService searchService,
                              SearchSortUtil searchSortUtil,
                              SubTaskService subtasks) {
        this.fieldManager = fieldManager;
        this.tableLayoutUtils = tableLayoutUtils;
        this.columnLayoutItemFactory = columnLayoutItemFactory;
        this.searchService = searchService;
        this.searchSortUtil = searchSortUtil;
        this.subtasks = subtasks;
    }

    public IssueTableLayoutBean getStandardLayout(SearchRequest searchRequest, ApplicationUser user) {
        final IssueTableLayoutBean layoutBean = new IssueTableLayoutBean(getUserColumns(searchRequest, user), getSearchSorts(searchRequest));
        if (user != null) {
            layoutBean.setShowActionColumn(true);
        }
        return layoutBean;
    }

    /**
     * Get the columns relevant for the dashboard, but limiting to a specific list of column names.  The column names have to match {@link com.atlassian.jira.issue.IssueFieldConstants}.  If you pass null
     * you will get the default columns.
     *
     * @param columnNames The columns to display.  If no columns are specified, this will default to the columns specified in the jira-application.properties
     *                    file with the key represented by {@link com.atlassian.jira.config.properties.APKeys#ISSUE_TABLE_COLS_DASHBOARD}
     * @return A layout using the columns specified or default, with the correct display properties for the Dashboard.
     */

    public IssueTableLayoutBean getDashboardLayout(ApplicationUser user, List<String> columnNames) throws FieldException {
        IssueTableLayoutBean layout = new IssueTableLayoutBean(getDashboardColumns(user, columnNames));
        layout.setSortingEnabled(false);
        layout.setDisplayHeader(false);
        layout.setShowExteriorTable(false);
        layout.setTableCssClass("grid issuetable-db maxWidth");

        // note that the uniqueId here _should_ be used to set unique ids on the table, as there may be multiple
        // tables on one page (as is the case with AbstractSearchResultsPortlet and all portlets that extend it).
        //
        // However, a lot of the CSS for the issue table was based on the css 'id', not based on the css 'class' as it
        // should have been.  To change to id would have been a large change, and I'm afraid I didn't get to it (SF - 15/Nov/08)
        //
        // As a result of this, there will be two tables with the same 'id' on the dashboard.  This doesn't seem to cause
        // any problems that I could see.
        //
        // layout.setTableHtmlId(uniqueId);
        return layout;
    }

    private List<ColumnLayoutItem> getDashboardColumns(final ApplicationUser user, List<String> columnNames) throws FieldException {
        if (columnNames == null || columnNames.isEmpty())
            columnNames = tableLayoutUtils.getDefaultColumnNames(APKeys.ISSUE_TABLE_COLS_DASHBOARD);

        return tableLayoutUtils.getColumns(user, columnNames);
    }

    public IssueTableLayoutBean getPrintableLayout(SearchRequest searchRequest, ApplicationUser user) {
        IssueTableLayoutBean layout = new IssueTableLayoutBean(getUserColumns(searchRequest, user), getSearchSorts(searchRequest));
        layout.setSortingEnabled(false); // printable doesn't have sorting
        layout.addCellDisplayParam(FieldRenderingContext.PRINT_VIEW, Boolean.TRUE);
        return layout;
    }

    public IssueTableLayoutBean getStandardExcelLayout(SearchRequest searchRequest, ApplicationUser user) {
        IssueTableLayoutBean standardLayout = new IssueTableLayoutBean(getExcelUserColumns(searchRequest, user), getSearchSorts(searchRequest));
        setExcelLayout(standardLayout);
        return standardLayout;
    }

    public IssueTableLayoutBean getAllColumnsExcelLayout(SearchRequest searchRequest, ApplicationUser user) {
        IssueTableLayoutBean standardLayout = new IssueTableLayoutBean(getAllUserExcelColumns(searchRequest, user), getSearchSorts(searchRequest));
        setExcelLayout(standardLayout);
        return standardLayout;
    }

    /**
     * Get the layout for sub-tasks on the view issue page.
     * <p>
     * Users can specify which columns to show in jira-application.properties.  The default columns that are always shown:
     * <ul>
     * <li>The sequence number (1, 2, etc)
     * <li>The summary of the sub-task
     * <li>The controls to re-order sub-tasks (if they are not disabled by dark feature)
     * <li>The links to update workflow on the subtasks
     * </ul>
     * <p>
     * The standard columns are retrieved from {@link com.atlassian.jira.web.component.subtask.ColumnLayoutItemFactory}.
     *
     * @param user             To get the available columns from
     * @param parentIssue      The parent issue of all the subTasks
     * @param subTaskBean      The subTask bean that contains all the subtasks that will be displayed on this page
     * @param subTaskView      The 'view' which is passed to the subTaskBean to get the list of subtasks to display.  Usually either 'unresolved' or 'all'
     * @param timeTrackingData whether or not time tracking data should be shown
     * @return the IssueTableLayoutBean based on the application configuration.
     * @throws ColumnLayoutStorageException if there is a problem accessing the column layout backing data
     * @throws FieldException               if there is a problem accessing the field backing data
     */
    public IssueTableLayoutBean getSubTaskIssuesLayout(ApplicationUser user, final Issue parentIssue, final SubTaskBean subTaskBean, final String subTaskView, boolean timeTrackingData) throws ColumnLayoutStorageException, FieldException {
        List<String> userSpecifiedColumns = tableLayoutUtils.getDefaultColumnNames(APKeys.ISSUE_TABLE_COLS_SUBTASK);

        // please don't look at this code, your morals will be compromised...
        /// QUALITY: OFF
        if (!timeTrackingData && userSpecifiedColumns != null) {
            userSpecifiedColumns = new ArrayList<String>(userSpecifiedColumns);
            userSpecifiedColumns.remove("progress");
        }
        /// QUALITY: ON
        // ok you can look again now

        final List<ColumnLayoutItem> columns = new ArrayList<ColumnLayoutItem>();

        final ColumnLayoutItem displaySequence = columnLayoutItemFactory.getSubTaskDisplaySequenceColumn(subTaskBean, subTaskView);
        columns.add(displaySequence);

        final ColumnLayoutItem simpleSummary = columnLayoutItemFactory.getSubTaskSimpleSummaryColumn();
        columns.add(simpleSummary);

        columns.addAll(tableLayoutUtils.getColumns(user, userSpecifiedColumns));

        if(subtasks.isReorderColumnShown()) {
            final ColumnLayoutItem subTaskReorder = columnLayoutItemFactory.getSubTaskReorderColumn(user, parentIssue, subTaskBean, subTaskView);
            columns.add(subTaskReorder);
        }

        final IssueTableLayoutBean layout = new IssueTableLayoutBean(columns, Collections.<SearchSort>emptyList());
        layout.setSortingEnabled(false);
        layout.setDisplayHeader(false);
        layout.setShowExteriorTable(false);
        layout.setTableCssClass(""); //override the grid CSS class
        layout.setShowActionColumn(true);
        return layout;
    }

    private void setExcelLayout(IssueTableLayoutBean layoutBean) {
        layoutBean.setSortingEnabled(false);
        layoutBean.setAlternateRowColors(false);
        layoutBean.addCellDisplayParam(IssueTableLayoutBean.CELL_NO_LINK, Boolean.TRUE);
        layoutBean.addCellDisplayParam(IssueTableLayoutBean.CELL_TEXT_ONLY, Boolean.TRUE);
        layoutBean.addCellDisplayParam(IssueTableLayoutBean.FULL_LINK, Boolean.TRUE);
        layoutBean.addCellDisplayParam(FieldRenderingContext.EXCEL_VIEW, Boolean.TRUE);
    }

    private List<SearchSort> getSearchSorts(SearchRequest searchRequest) {
        if (searchRequest != null) {
            return searchSortUtil.getSearchSorts(searchRequest.getQuery());
        }
        return null;
    }

    private List<NavigableField> getAllUserCommonForExcelAndCsvColumnsFields(final SearchRequest searchRequest, final ApplicationUser user) {
        try {
            final Set<NavigableField> availableFields;
            final Query query = searchRequest.getQuery();

            if (query.getWhereClause() == null) {
                availableFields = fieldManager.getAvailableNavigableFieldsWithScope(user);
            } else {
                final QueryContext queryContext = searchService.getQueryContext(user, query);
                availableFields = fieldManager.getAvailableNavigableFieldsWithScope(user, queryContext);
            }

            // Remove all custom fields that do not have view values - JRA-11514
            for (Iterator<NavigableField> iterator = availableFields.iterator(); iterator.hasNext(); ) {
                final NavigableField field = iterator.next();
                if (field instanceof CustomField) {
                    final CustomField customField = (CustomField) field;
                    if (!customField.getCustomFieldType().getDescriptor().isViewTemplateExists() &&
                            !customField.getCustomFieldType().getDescriptor().isColumnViewTemplateExists()) {
                        iterator.remove();
                    }
                }
            }

            return new ArrayList<NavigableField>(availableFields);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Gets all the available navigable fields for the given user and search requests project, also removing any custom
     * fields with no view ({@link com.atlassian.jira.plugin.customfield.CustomFieldTypeModuleDescriptor#TEMPLATE_NAME_VIEW}
     * and {@link com.atlassian.jira.plugin.customfield.CustomFieldTypeModuleDescriptor#TEMPLATE_NAME_COLUMN}) defined.
     *
     * @param searchRequest used to determine the project for which to check field visibility
     * @param user          used to determine the user for which to check field visibility
     * @return {@link List} of {@link ExcelColumnLayoutItem}
     */
    private List<ColumnLayoutItem> getAllUserExcelColumns(SearchRequest searchRequest, ApplicationUser user) {
        final List<NavigableField> fields = getAllUserCommonForExcelAndCsvColumnsFields(searchRequest, user);
        final List<ColumnLayoutItem> columnItems = Lists.newArrayListWithCapacity(fields.size());

        for (NavigableField field : fields) {
            columnItems.add(new ExcelColumnLayoutItem(field, columnItems.size()));
        }

        return columnItems;
    }

    private boolean isCsvExportableField(final Field field) {
        return field instanceof ExportableSystemField ||
                (field instanceof CustomField);
    }

    public List<Field> getAllUserCsvColumnsFields(final SearchRequest searchRequest, final ApplicationUser user) {
        final List<NavigableField> potentialFields = getAllUserCommonForExcelAndCsvColumnsFields(searchRequest, user);
        final List<String> additionalFields = new ArrayList<String>();
        additionalFields.add(IssueFieldConstants.COMMENT);
        additionalFields.add(IssueFieldConstants.WORKLOG);
        additionalFields.add(IssueFieldConstants.ATTACHMENT);

        final List<Field> finalFields = new ArrayList<Field>(potentialFields.size() + additionalFields.size());

        for (Field field : potentialFields) {
            if (isCsvExportableField(field)) {
                finalFields.add(field);
                additionalFields.remove(field.getId());
            }
        }

        for (String fieldId : additionalFields) {
            finalFields.add(fieldManager.getField(fieldId));
        }

        return finalFields;
    }

    public List<Field> getCurrentUserCsvColumnsFields(final SearchRequest searchRequest, final ApplicationUser user) {
        final List<ColumnLayoutItem> columns = getUserColumns(searchRequest, user);
        final List<Field> fields = new ArrayList<>();

        for (ColumnLayoutItem columnLayoutItem : columns) {
            final Field field = columnLayoutItem.getNavigableField();

            if (isCsvExportableField(field)) {
                fields.add(field);
            }
        }

        return fields;
    }

    @VisibleForTesting
    List<ColumnLayoutItem> getUserColumns(SearchRequest searchRequest, ApplicationUser user) {
        if (searchRequest == null) {
            throw new NullPointerException("searchRequest cannot be null");
        }
        try {
            return getColumnsProvider().getColumns(user, searchRequest);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @VisibleForTesting
    ColumnLayoutItemProvider getColumnsProvider() {
        return new ColumnLayoutItemProvider();
    }

    private List<ColumnLayoutItem> getExcelUserColumns(SearchRequest searchRequest, ApplicationUser user) {
        final List<ColumnLayoutItem> visibleColumns = new ArrayList<ColumnLayoutItem>(getUserColumns(searchRequest, user));

        CollectionUtils.transform(visibleColumns, input -> new ExcelColumnLayoutItem((ColumnLayoutItem) input));

        return visibleColumns;
    }
}
