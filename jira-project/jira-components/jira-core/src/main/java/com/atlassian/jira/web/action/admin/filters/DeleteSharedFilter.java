package com.atlassian.jira.web.action.admin.filters;

import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.bc.filter.FilterDeletionWarningViewProvider;
import com.atlassian.jira.bc.filter.SearchRequestService;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.issue.search.SearchRequestManager;
import com.atlassian.jira.issue.search.managers.IssueSearcherManager;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.xsrf.RequiresXsrfCheck;

import org.apache.commons.lang.StringUtils;

/**
 * The Delete Shared Filters action
 *
 * @since v4.4
 */
public class DeleteSharedFilter extends AbstractAdministerFilter {
    private static final int FILTERS_PER_PAGE = 20;

    private final SearchRequestService searchRequestService;
    private final FilterDeletionWarningViewProvider filterDeletionWarningViewProvider;

    public DeleteSharedFilter(final IssueSearcherManager issueSearcherManager,
                              final SearchRequestService searchRequestService,
                              final SearchService searchService,
                              final PermissionManager permissionManager,
                              final SearchRequestManager searchRequestManager,
                              final FilterDeletionWarningViewProvider filterDeletionWarningViewProvider) {
        super(issueSearcherManager, searchRequestService, searchService, permissionManager, searchRequestManager);
        this.searchRequestService = searchRequestService;
        this.filterDeletionWarningViewProvider = filterDeletionWarningViewProvider;
    }

    @Override
    @RequiresXsrfCheck
    protected String doExecute() throws Exception {
        final JiraServiceContext ctx = getJiraServiceContext();
        if (getFilterId() != null) {
            searchRequestService.validateForDelete(ctx, getFilterId());

            if (hasAnyErrors()) {
                return ERROR;
            }
            searchRequestService.deleteFilter(getJiraServiceContext(getFilterId()), getFilterId());
            if (hasAnyErrors()) {
                return ERROR;
            }
        } else {
            addErrorMessage(getText("admin.errors.filters.cannot.delete.filter"));
            return ERROR;
        }

        setSearchRequest(null);
        repaginateIfNeeded();
        if (isInlineDialogMode()) {
            return returnCompleteWithInlineRedirect(buildReturnUri());
        } else {
            String returnUrl = buildReturnUri();
            setReturnUrl(null);
            return forceRedirect(returnUrl);
        }
    }

    private void repaginateIfNeeded() {
        // only need to repaginate if on last page
        final int pagingOffset = StringUtils.isNotBlank(getPagingOffset()) ? Integer.parseInt(getPagingOffset()) - 1 : -1;
        final int newResultCount = StringUtils.isNotBlank(getTotalResultCount()) ? Integer.parseInt(getTotalResultCount()) - 1 : -1;
        if (pagingOffset >= 0) {
            setTotalResultCount("" + newResultCount);
            if (newResultCount % FILTERS_PER_PAGE == 0) {
                setPagingOffset("" + pagingOffset);
            }
        }
    }

    public boolean canDelete() {
        return !hasAnyErrors();
    }

    public String getWarningHtml() {
        return filterDeletionWarningViewProvider.getWarningHtml(getFilter());
    }
}
