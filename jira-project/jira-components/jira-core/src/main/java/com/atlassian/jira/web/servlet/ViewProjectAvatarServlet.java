package com.atlassian.jira.web.servlet;

import com.atlassian.jira.avatar.AvatarManager;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.icon.IconType;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.web.filters.johnson.ServiceUnavailableResponder;
import org.apache.commons.lang.StringUtils;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

/**
 * Serves avatar images for projects.
 *
 * @since v4.0
 */
public class ViewProjectAvatarServlet extends AbstractAvatarServlet {

    @Override
    protected Long validateInput(String projectId, Long avatarId, final HttpServletResponse response) throws IOException {
        IconType iconType = IconType.PROJECT_ICON_TYPE;

        Optional<AvatarManager> avatarManagerOptional = getAvatarManager();
        if (!avatarManagerOptional.isPresent()) {
            ServiceUnavailableResponder.respondWithEmpty503(response);
            return null;
        }

        if (StringUtils.isBlank(projectId)) {
            return avatarManagerOptional.get().getDefaultAvatarId(iconType);
        } else {
            Optional<ProjectManager> projectManagerOptional = getProjectManager();
            if (!projectManagerOptional.isPresent()) {
                ServiceUnavailableResponder.respondWithEmpty503(response);
                return null;
            }
            final Project project = projectManagerOptional.get().getProjectObj(Long.parseLong(projectId));
            if (project == null) {
                response.sendError(HttpServletResponse.SC_NOT_FOUND, "Unknown project");
                return null;
            }
            if (!avatarManagerOptional.get().userCanView(getAuthenticationContext().getLoggedInUser(), project.getAvatar())) {
                // no permission to see any avatar for this project
                response.sendError(HttpServletResponse.SC_FORBIDDEN, "Unknown project");
                return null;
            }

            if (avatarId == null) {
                return project.getAvatar().getId();
            }
        }
        return avatarId;
    }


    @Override
    protected String getOwnerIdParamName() {
        return "pid";
    }

    JiraAuthenticationContext getAuthenticationContext() {
        return ComponentAccessor.getJiraAuthenticationContext();
    }

    Optional<ProjectManager> getProjectManager() {
        return ComponentAccessor.getComponentSafely(ProjectManager.class);
    }
}
