package com.atlassian.jira.bc.project;

import com.atlassian.beehive.ClusterLock;
import com.atlassian.beehive.ClusterLockService;
import com.atlassian.fugue.Either;
import com.atlassian.fugue.Option;
import com.atlassian.jira.association.NodeAssociationStore;
import com.atlassian.jira.avatar.Avatar;
import com.atlassian.jira.avatar.AvatarManager;
import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.bc.JiraServiceContextImpl;
import com.atlassian.jira.bc.ServiceOutcome;
import com.atlassian.jira.bc.ServiceOutcomeImpl;
import com.atlassian.jira.bc.ServiceResult;
import com.atlassian.jira.bc.ServiceResultImpl;
import com.atlassian.jira.bc.project.component.ProjectComponentManager;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.event.project.ProjectEventManager;
import com.atlassian.jira.exception.DataAccessException;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.fields.screen.issuetype.IssueTypeScreenSchemeManager;
import com.atlassian.jira.issue.security.IssueSecurityLevelScheme;
import com.atlassian.jira.issue.security.IssueSecuritySchemeManager;
import com.atlassian.jira.notification.NotificationSchemeManager;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.permission.PermissionSchemeManager;
import com.atlassian.jira.project.AssigneeTypes;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectAssigneeTypes;
import com.atlassian.jira.project.ProjectCategory;
import com.atlassian.jira.project.ProjectCreateNotifier;
import com.atlassian.jira.project.ProjectCreatedData;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.project.UpdateProjectParameters;
import com.atlassian.jira.project.template.ProjectTemplate;
import com.atlassian.jira.project.template.ProjectTemplateKey;
import com.atlassian.jira.project.template.ProjectTemplateManager;
import com.atlassian.jira.project.type.ProjectTypeKey;
import com.atlassian.jira.project.type.ProjectTypeUpdatedNotifier;
import com.atlassian.jira.project.util.ProjectKeyStore;
import com.atlassian.jira.project.version.VersionManager;
import com.atlassian.jira.scheme.Scheme;
import com.atlassian.jira.scheme.SchemeFactory;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.sharing.SharePermissionDeleteUtils;
import com.atlassian.jira.task.TaskManager;
import com.atlassian.jira.task.TaskProgressSink;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.ApplicationUsers;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.ErrorCollections;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.JiraKeyUtils;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.jira.util.dbc.Assertions;
import com.atlassian.jira.workflow.WorkflowManager;
import com.atlassian.jira.workflow.WorkflowSchemeManager;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.opensymphony.util.TextUtils;
import org.ofbiz.core.entity.GenericEntityException;
import org.ofbiz.core.entity.GenericValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.Callable;
import java.util.function.Consumer;
import java.util.function.Supplier;

import static com.atlassian.jira.util.ErrorCollection.Reason;
import static java.util.Optional.ofNullable;
import static com.atlassian.jira.entity.ProjectCategoryFactory.NONE_PROJECT_CATEGORY_ID;

public class DefaultProjectService implements ProjectService {
    /**
     * Maximum length for the URL field.
     */
    private static final int MAX_FIELD_LENGTH = 255;
    /**
     * Default project key length when stored value is not a number.
     */
    private static final int DEFAULT_KEY_LENGTH = 10;
    private static final Logger log = LoggerFactory.getLogger(DefaultProjectService.class);

    private final JiraAuthenticationContext jiraAuthenticationContext;
    private final ProjectManager projectManager;
    private final ApplicationProperties applicationProperties;
    private final PermissionManager permissionManager;
    private final GlobalPermissionManager globalPermissionManager;
    private final PermissionSchemeManager permissionSchemeManager;
    private final NotificationSchemeManager notificationSchemeManager;
    private final IssueSecuritySchemeManager issueSecuritySchemeManager;
    private final SchemeFactory schemeFactory;
    private final WorkflowSchemeManager workflowSchemeManager;
    private final IssueTypeScreenSchemeManager issueTypeScreenSchemeManager;
    private final CustomFieldManager customFieldManager;
    private final NodeAssociationStore nodeAssociationStore;
    private final VersionManager versionManager;
    private final ProjectComponentManager projectComponentManager;
    private final SharePermissionDeleteUtils sharePermissionDeleteUtils;
    private final AvatarManager avatarManager;
    private final I18nHelper.BeanFactory i18nFactory;
    private final WorkflowManager workflowManager;
    private final UserManager userManager;
    private final ProjectEventManager projectEventManager;
    private final ProjectKeyStore projectKeyStore;
    private final ProjectNameValidator projectNameValidator;
    private final ProjectTypeValidator projectTypeValidator;
    private final ProjectTypeUpdatedNotifier projectTypeUpdatedNotifier;
    private final ProjectCreateNotifier projectCreateNotifier;
    private final ClusterLockService clusterLockService;
    private final ProjectTemplateManager projectTemplateManager;
    private final ProjectSchemeAssociationManager projectSchemeAssociationManager;
    private final TaskManager taskManager;
    private final IssueManager issueManager;

    public DefaultProjectService(
            JiraAuthenticationContext jiraAuthenticationContext,
            ProjectManager projectManager,
            ApplicationProperties applicationProperties,
            PermissionManager permissionManager,
            GlobalPermissionManager globalPermissionManager,
            PermissionSchemeManager permissionSchemeManager,
            NotificationSchemeManager notificationSchemeManager,
            IssueSecuritySchemeManager issueSecuritySchemeManager,
            SchemeFactory schemeFactory,
            WorkflowSchemeManager workflowSchemeManager,
            IssueTypeScreenSchemeManager issueTypeScreenSchemeManager,
            CustomFieldManager customFieldManager,
            NodeAssociationStore nodeAssociationStore,
            VersionManager versionManager,
            ProjectComponentManager projectComponentManager,
            SharePermissionDeleteUtils sharePermissionDeleteUtils,
            AvatarManager avatarManager,
            I18nHelper.BeanFactory i18nFactory,
            WorkflowManager workflowManager,
            UserManager userManager,
            ProjectEventManager projectEventManager,
            ProjectKeyStore projectKeyStore,
            ProjectTypeValidator projectTypeValidator,
            ProjectCreateNotifier projectCreateNotifier,
            ProjectTypeUpdatedNotifier projectTypeUpdatedNotifier,
            ClusterLockService clusterLockService,
            ProjectTemplateManager projectTemplateManager,
            ProjectSchemeAssociationManager projectSchemeAssociationManager,
            TaskManager taskManager, IssueManager issueManager) {
        this.jiraAuthenticationContext = jiraAuthenticationContext;
        this.projectManager = projectManager;
        this.applicationProperties = applicationProperties;
        this.permissionManager = permissionManager;
        this.globalPermissionManager = globalPermissionManager;
        this.permissionSchemeManager = permissionSchemeManager;
        this.notificationSchemeManager = notificationSchemeManager;
        this.issueSecuritySchemeManager = issueSecuritySchemeManager;
        this.schemeFactory = schemeFactory;
        this.workflowSchemeManager = workflowSchemeManager;
        this.issueTypeScreenSchemeManager = issueTypeScreenSchemeManager;
        this.customFieldManager = customFieldManager;
        this.nodeAssociationStore = nodeAssociationStore;
        this.versionManager = versionManager;
        this.projectComponentManager = projectComponentManager;
        this.sharePermissionDeleteUtils = sharePermissionDeleteUtils;
        this.avatarManager = avatarManager;
        this.i18nFactory = i18nFactory;
        this.workflowManager = workflowManager;
        this.userManager = userManager;
        this.projectEventManager = projectEventManager;
        this.projectKeyStore = projectKeyStore;
        this.projectTypeValidator = projectTypeValidator;
        this.projectCreateNotifier = projectCreateNotifier;
        this.projectTypeUpdatedNotifier = projectTypeUpdatedNotifier;
        this.clusterLockService = clusterLockService;
        this.projectTemplateManager = projectTemplateManager;
        this.projectSchemeAssociationManager = projectSchemeAssociationManager;
        this.taskManager = taskManager;
        this.issueManager = issueManager;

        this.projectNameValidator = new ProjectNameValidator(this, projectManager);
    }

    @Override
    public UpdateProjectValidationResult validateUpdateProject(ApplicationUser user, String name, String key, String description, ApplicationUser lead, String url, Long assigneeType) {
        return validateUpdateProject(user, name, key, description, nameFor(lead), url, assigneeType);
    }

    @Override
    public UpdateProjectValidationResult validateUpdateProject(ApplicationUser user, String name, String key, String description, ApplicationUser lead, String url, Long assigneeType, Long avatarId) {
        return validateUpdateProject(user, name, key, description, nameFor(lead), url, assigneeType, avatarId);
    }

    @Override
    public DeleteProjectValidationResult validateDeleteProject(ApplicationUser user, String key) {
        final I18nHelper i18nBean = getI18nBean(user);
        if (!globalPermissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, user)) {
            return new DeleteProjectValidationResult(ErrorCollections.create(i18nBean.getText("admin.projects.service.error.no.admin.permission"), Reason.FORBIDDEN));
        }

        //check if the project exists.  If not return with an error.
        final GetProjectResult oldProjectResult = getProjectByKeyForAction(user, key, ProjectAction.EDIT_PROJECT_CONFIG);
        if (!oldProjectResult.isValid() || oldProjectResult.getProject() == null) {
            return new DeleteProjectValidationResult(ErrorCollections.copyOf(oldProjectResult.getErrorCollection()));
        }

        return new DeleteProjectValidationResult(ErrorCollections.empty(), oldProjectResult.getProject());

    }

    @Override
    public DeleteProjectResult deleteProject(ApplicationUser user, DeleteProjectValidationResult deleteProjectValidationResult) {
        if (deleteProjectValidationResult == null) {
            throw new IllegalArgumentException("You can not delete a project with a null validation result.");
        }
        if (!deleteProjectValidationResult.isValid()) {
            throw new IllegalStateException("You can not delete a project with an invalid validation result.");
        }

        DeleteProjectCommand deleteProjectCommand = new DeleteProjectCommand(projectManager, workflowSchemeManager, issueTypeScreenSchemeManager,
                customFieldManager, nodeAssociationStore, versionManager, projectComponentManager, sharePermissionDeleteUtils,
                i18nFactory, workflowManager, permissionSchemeManager, projectEventManager, issueManager, user, deleteProjectValidationResult.getProject());


        deleteProjectCommand.setTaskProgressSink(TaskProgressSink.NULL_SINK);

        return deleteProjectCommand.call();
    }

    @Override
    public DeleteProjectResult deleteProjectAsynchronous(ApplicationUser user, DeleteProjectValidationResult deleteProjectValidationResult) {
        if (deleteProjectValidationResult == null) {
            throw new IllegalArgumentException("You can not delete a project with a null validation result.");
        }
        if (!deleteProjectValidationResult.isValid()) {
            throw new IllegalStateException("You can not delete a project with an invalid validation result.");
        }

        Project project = deleteProjectValidationResult.getProject();
        final String taskName = getI18nBean(user).getText("admin.projects.delete.project.progress", project.getName());
        Callable<ProjectService.DeleteProjectResult> command =
                new DeleteProjectCommand(projectManager, workflowSchemeManager, issueTypeScreenSchemeManager,
                        customFieldManager, nodeAssociationStore, versionManager, projectComponentManager, sharePermissionDeleteUtils,
                        i18nFactory, workflowManager, permissionSchemeManager, projectEventManager, issueManager, user, deleteProjectValidationResult.getProject());
        String progressURL = taskManager.submitTask(command, taskName, new DeleteProjectTaskContext(project), true).getProgressURL();
        return new ProjectService.DeleteProjectResult(progressURL);
    }

    @Override
    public GetProjectResult getProjectById(Long id) {
        return getProjectById(jiraAuthenticationContext.getUser(), id);
    }

    @Override
    public GetProjectResult getProjectByKey(String key) {
        return getProjectByKey(jiraAuthenticationContext.getUser(), key);
    }

    @Override
    public
    @Nonnull
    CreateProjectValidationResult validateCreateProject(ApplicationUser user, @Nonnull ProjectCreationData projectCreationData) {
        final JiraServiceContext serviceContext = getServiceContext(user, ErrorCollections.empty());
        final I18nHelper i18nBean = getI18nBean(user);

        if (!globalPermissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, user)) {
            serviceContext.getErrorCollection().addErrorMessage(i18nBean.getText("admin.projects.service.error.no.admin.permission"), Reason.FORBIDDEN);
            return new CreateProjectValidationResult(serviceContext.getErrorCollection());
        }

        final ProjectCreationData projectCreationDataWithType = setProjectTypeFromTemplateIfNonePresent(projectCreationData);
        isValidAllProjectData(serviceContext, projectCreationDataWithType);

        if (serviceContext.getErrorCollection().hasAnyErrors()) {
            return new CreateProjectValidationResult(serviceContext.getErrorCollection());
        }

        return new CreateProjectValidationResult(serviceContext.getErrorCollection(), user, projectCreationDataWithType);
    }

    private ProjectCreationData setProjectTypeFromTemplateIfNonePresent(final ProjectCreationData projectCreationData) {
        final ProjectTypeKey projectTypeKey = projectCreationData.getProjectTypeKey();
        if (projectTypeKey != null && projectTypeKey.getKey() != null) {
            return projectCreationData;
        }

        final Optional<ProjectTemplate> projectTemplate = projectTemplateManager.getProjectTemplate(projectCreationData.getProjectTemplateKey());
        if (!projectTemplate.isPresent()) {
            return projectCreationData;
        }

        final ProjectTypeKey newProjectTypeKey = projectTemplate.get().getProjectTypeKey();
        return new ProjectCreationData.Builder().from(projectCreationData).withType(newProjectTypeKey).build();
    }

    @Nonnull
    @Override
    public CreateProjectValidationResult validateCreateProjectBasedOnExistingProject(
            final ApplicationUser user,
            @Nonnull final Long existingProjectId,
            @Nonnull ProjectCreationData input
    ) {
        final JiraServiceContext serviceContext = getServiceContext(user, ErrorCollections.empty());
        final I18nHelper i18nBean = getI18nBean(user);

        if (!globalPermissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, user)) {
            serviceContext.getErrorCollection().addErrorMessage(i18nBean.getText("admin.projects.service.error.no.admin.permission"), Reason.FORBIDDEN);
            return new CreateProjectValidationResult(serviceContext.getErrorCollection());
        }

        final GetProjectResult projectById = getProjectByIdForAction(user, existingProjectId, ProjectAction.EDIT_PROJECT_CONFIG);
        if (!projectById.isValid()) {
            return new CreateProjectValidationResult(projectById.getErrorCollection());
        }
        final ProjectCreationData creationData = new ProjectCreationData.Builder().fromExistingProject(projectById.get(), input).build();
        isValidAllProjectData(serviceContext, creationData);

        return new CreateProjectValidationResult(serviceContext.getErrorCollection(), user, creationData, Optional.of(existingProjectId));
    }

    protected JiraServiceContext getServiceContext(final ApplicationUser user, final ErrorCollection errorCollection) {
        return new JiraServiceContextImpl(user, errorCollection);
    }

    @Override
    public Project createProject(CreateProjectValidationResult result) {
        Consumer<Project> projectConsumer;
        if (shouldCreateProjectBasedOnExisting(result)) {
            projectConsumer = (newProject) -> {
                final Optional<Project> existing = ofNullable(projectManager.getProjectObj(result.getExistingProjectId().get()));
                final Project existingProject = existing.orElseThrow(() -> new IllegalStateException("Could not retrieve existing project."));

                final Optional<ProjectCategory> projectCategory = ofNullable(existingProject.getProjectCategory());
                projectCategory.ifPresent(category -> projectManager.setProjectCategory(newProject, category));

                projectSchemeAssociationManager.associateSchemesOfExistingProjectWithNewProject(newProject, existingProject);
            };
        } else {
            projectConsumer = projectSchemeAssociationManager::associateDefaultSchemesWithProject;
        }
        return createProjectInternal(result, projectConsumer);
    }

    private Project createProjectInternal(final CreateProjectValidationResult result, final Consumer<Project> projectConsumer) {
        if (result == null) {
            throw new IllegalArgumentException("You can not create a project with a null validation result.");
        }

        if (!result.isValid()) {
            throw new IllegalStateException("You can not create a project with an invalid validation result.");
        }

        if (result.getExistingProjectId().isPresent()) {
            ofNullable(projectManager.getProjectObj(result.getExistingProjectId().get())).orElseThrow(() -> new IllegalStateException("Existing project could not be found."));
        }

        final ApplicationUser user = result.getUser();
        final ProjectCreationData projectCreationData = result.getProjectCreationData();
        final Project newProject = projectManager.createProject(user, new ProjectCreationData.Builder()
                .from(projectCreationData)
                .withKey(projectCreationData.getKey().toUpperCase())
                .withLead(projectCreationData.getLead())
                .withAssigneeType(effectiveAssigneeType(projectCreationData.getAssigneeType()))
                .build());

        projectConsumer.accept(newProject);

        // Refresh the workflow cache
        workflowSchemeManager.clearWorkflowCache();

        notifyHandlersOfProjectCreated(user, newProject, result);
        projectEventManager.dispatchProjectCreated(user, newProject);

        return newProject;
    }

    private boolean shouldCreateProjectBasedOnExisting(final CreateProjectValidationResult result) {
        return result != null && result.getExistingProjectId().isPresent();
    }

    private void notifyHandlersOfProjectCreated(ApplicationUser user, Project newProject, CreateProjectValidationResult result) {
        final ProjectCreatedData.Builder projectCreatedData = new ProjectCreatedData.Builder().withProject(newProject);
        Optional.ofNullable(result.getProjectCreationData().getProjectTemplateKey()).ifPresent(projectCreatedData::withProjectTemplateKey);
        result.getExistingProjectId().ifPresent(projectCreatedData::withExistingProjectId);

        boolean notificationWasSuccessful = projectCreateNotifier.notifyAllHandlers(projectCreatedData.build());
        if (!notificationWasSuccessful) {
            deleteProject(user, new DeleteProjectValidationResult(ErrorCollections.empty(), newProject));
            throw new RuntimeException("An error occurred while notifying that a project was created");
        }
    }

    @Override
    public UpdateProjectValidationResult validateUpdateProject(final ApplicationUser user, final String name, final String key,
                                                               final String description, final String leadName, final String url, final Long assigneeType) {
        return validateUpdateProject(user, name, key, description, leadName, url, assigneeType, null);
    }

    @Override
    public ServiceResult validateUpdateProject(final ApplicationUser user, final String key) {
        //check if the project exists.  If not return with an error.
        final GetProjectResult oldProjectResult = getProjectByKeyForAction(user, key, ProjectAction.EDIT_PROJECT_CONFIG);
        if (!oldProjectResult.isValid()) {
            return new ServiceResultImpl(ErrorCollections.copyOf(oldProjectResult.getErrorCollection()));
        }

        return new ServiceResultImpl(ErrorCollections.empty());
    }

    @Override
    public UpdateProjectValidationResult validateUpdateProject(final ApplicationUser applicationUser, final Project originalProject, final String name, final String key,
                                                               final String description, final ApplicationUser lead, final String url, final Long assigneeType, Long avatarId) {
        return validateUpdateProject(applicationUser, originalProject, name, key, description, nameFor(lead), url, assigneeType, avatarId);
    }

    @Override
    public UpdateProjectValidationResult validateUpdateProject(final ApplicationUser applicationUser, final Project originalProject, final String name, final String key,
                                                               final String description, final String leadName, final String url, final Long assigneeType, Long avatarId) {
        return validateUpdateProject(applicationUser, name, originalProject.getKey(), key, description, leadName, url, assigneeType, avatarId);
    }

    @Override
    public UpdateProjectValidationResult validateUpdateProject(final ApplicationUser user, final String name, final String key,
                                                               final String description, final String leadName, final String url, final Long assigneeType, Long avatarId) {
        return validateUpdateProject(user, name, key, key, description, leadName, url, assigneeType, avatarId);
    }

    private UpdateProjectValidationResult validateUpdateProject(final ApplicationUser user, final String name, final String oldKey,
                                                                final String newKey, final String description, final String leadUsername, final String url, final Long assigneeType, final Long avatarId) {
        //check if the project exists.  If not return with an error.
        final GetProjectResult getProjectResult = getProjectByKeyForAction(user, oldKey, ProjectAction.EDIT_PROJECT_CONFIG);
        if (!getProjectResult.isValid()) {
            return new UpdateProjectValidationResult(ErrorCollections.copyOf(getProjectResult.getErrorCollection()));
        }

        Project project = getProjectResult.getProject();
        final ApplicationUser lead = getUserByName(leadUsername);

        final ErrorCollection validationErrors = validateUpdateProjectData(user, name, project,
                lead, url, assigneeType, avatarId);

        if (validationErrors.hasAnyErrors()) {
            return new UpdateProjectValidationResult(validationErrors);
        }

        UpdateProjectRequest updateProjectRequest = new UpdateProjectRequest(project);

        updateProjectRequest.name(name)
                .key(newKey)
                .description(description)
                .leadUsername(leadUsername)
                .url(url)
                .assigneeType(assigneeType)
                .avatarId(avatarId);

        return validateUpdateProject(user, updateProjectRequest);
    }

    @Override
    public UpdateProjectValidationResult validateUpdateProject(final ApplicationUser user, final UpdateProjectRequest updateProjectRequest) {
        if (user == null) {
            throw new IllegalArgumentException("You can not update a project with a null user.");
        }

        final I18nHelper i18nHelper = getI18nBean(user);

        UpdateProjectParameters projectParameters = updateProjectRequest.getUpdateProjectParameters();

        //check if the project exists.  If not return with an error.
        final GetProjectResult getProjectResult = getProjectByIdForAction(user, projectParameters.getProjectId(), ProjectAction.EDIT_PROJECT_CONFIG);
        if (!getProjectResult.isValid()) {
            return new UpdateProjectValidationResult(ErrorCollections.copyOf(getProjectResult.getErrorCollection()));
        }

        Project project = getProjectResult.getProject();

        final ErrorCollection errors = validateUpdateProjectParameters(user, project, projectParameters);

        if (errors.hasAnyErrors()) {
            return new UpdateProjectValidationResult(errors);
        } else {
            return new UpdateProjectValidationResult(errors, project, user, updateProjectRequest);
        }
    }

    @Override
    public Project updateProject(final UpdateProjectValidationResult result) {
        if (result == null) {
            throw new IllegalArgumentException("You can not update a project with a null validation result.");
        }

        if (!result.isValid()) {
            throw new IllegalStateException("You can not update a project with an invalid validation result.");
        }

        final ApplicationUser user = result.getUser();
        final Project originalProject = result.getOriginalProject();

        UpdateProjectParameters updateProjectParameters = result.getUpdateProjectRequest().getUpdateProjectParameters();
        Project updatedProject = projectManager.updateProject(updateProjectParameters);

        Either<Project, ErrorCollection> projectTypeUpdate = Either.left(updatedProject);
        if (!originalProject.getProjectTypeKey().equals(updatedProject.getProjectTypeKey())) {
            projectTypeUpdate = notifyProjectTypeUpdated(user, updatedProject,
                    originalProject.getProjectTypeKey(), updatedProject.getProjectTypeKey(), false);
        }

        if (projectTypeUpdate.isLeft()) {
            projectEventManager.dispatchProjectUpdated(user, updatedProject, originalProject, result.getUpdateProjectRequest().getRequestSourceType());
        } else {
            //Revert the changes
            final UpdateProjectParameters originalProjectParameters = UpdateProjectParameters.forProject(updateProjectParameters.getProjectId());
            updatedProject = projectManager.updateProject(originalProjectParameters);
        }

        return updatedProject;
    }

    @Override
    public UpdateProjectSchemesValidationResult validateUpdateProjectSchemes(final ApplicationUser user, final Long permissionSchemeId,
                                                                             final Long notificationSchemeId, final Long issueSecuritySchemeId) {
        final ErrorCollection errorCollection = ErrorCollections.empty();
        final I18nHelper i18nBean = getI18nBean(user);

        if (!permissionManager.hasPermission(Permissions.ADMINISTER, user)) {
            errorCollection.addErrorMessage(i18nBean.getText("admin.projects.service.error.no.admin.permission"), Reason.VALIDATION_FAILED);
            return new UpdateProjectSchemesValidationResult(errorCollection);
        }

        if (permissionSchemeId != null && permissionSchemeId.longValue() != -1L) {
            try {
                GenericValue scheme = permissionSchemeManager.getScheme(permissionSchemeId);
                if (scheme == null) {
                    errorCollection.addErrorMessage(i18nBean.getText("admin.errors.project.validation.permission.scheme.not.retrieved"), Reason.VALIDATION_FAILED);
                }
            } catch (GenericEntityException e) {
                errorCollection.addErrorMessage(i18nBean.getText("admin.errors.project.validation.permission.scheme.not.retrieved.error", e.getMessage()), Reason.VALIDATION_FAILED);
            }
        }

        if (notificationSchemeId != null && notificationSchemeId.longValue() != -1L) {
            try {
                GenericValue scheme = notificationSchemeManager.getScheme(notificationSchemeId);
                if (scheme == null) {
                    errorCollection.addErrorMessage(i18nBean.getText("admin.errors.project.validation.notification.scheme.not.retrieved"), Reason.VALIDATION_FAILED);
                }
            } catch (GenericEntityException e) {
                errorCollection.addErrorMessage(i18nBean.getText("admin.errors.project.validation.notification.scheme.not.retrieved.error", e.getMessage()), Reason.VALIDATION_FAILED);
            }
        }

        if (issueSecuritySchemeId != null && issueSecuritySchemeId.longValue() != -1L) {
            IssueSecurityLevelScheme scheme = issueSecuritySchemeManager.getIssueSecurityLevelScheme(issueSecuritySchemeId);
            if (scheme == null) {
                errorCollection.addErrorMessage(i18nBean.getText("admin.errors.project.validation.issuesecurity.scheme.not.retrieved"), Reason.VALIDATION_FAILED);
            }
        }

        if (errorCollection.hasAnyErrors()) {
            return new UpdateProjectSchemesValidationResult(errorCollection);
        }
        return new UpdateProjectSchemesValidationResult(errorCollection, permissionSchemeId, notificationSchemeId,
                issueSecuritySchemeId);
    }

    @Override
    public void updateProjectSchemes(final UpdateProjectSchemesValidationResult result, Project project) {
        if (result == null) {
            throw new IllegalArgumentException("You can not update project schemes with a null validation result.");
        }

        if (!result.isValid()) {
            throw new IllegalStateException("You can not update project schemes with an invalid validation result.");
        }

        if (project == null) {
            throw new IllegalArgumentException("You can not update project schemes for a null project.");
        }

        //now add all the schemes to the project.
        final Long permissionSchemeId = result.getPermissionSchemeId();
        final Long notificationSchemeId = result.getNotificationSchemeId();
        Long issueSecuritySchemeId = result.getIssueSecuritySchemeId();
        try {
            final Scheme currentNotificationScheme = notificationSchemeManager.getSchemeFor(project);
            if (currentNotificationScheme == null || !currentNotificationScheme.getId().equals(notificationSchemeId)) {
                notificationSchemeManager.removeSchemesFromProject(project);
                if (notificationSchemeId != null && !new Long(-1).equals(notificationSchemeId)) {
                    GenericValue scheme = notificationSchemeManager.getScheme(notificationSchemeId);
                    notificationSchemeManager.addSchemeToProject(project, schemeFactory.getScheme(scheme));
                }
            }

            final Scheme currentPermissionScheme = permissionSchemeManager.getSchemeFor(project);
            if (currentPermissionScheme == null || !currentPermissionScheme.getId().equals(permissionSchemeId)) {
                if (permissionSchemeId != null && !new Long(-1).equals(permissionSchemeId)) {
                    // A project should always be linked to a permission scheme.
                    permissionSchemeManager.removeSchemesFromProject(project);
                    GenericValue scheme = permissionSchemeManager.getScheme(permissionSchemeId);
                    permissionSchemeManager.addSchemeToProject(project, schemeFactory.getScheme(scheme));
                }
            }

            final Scheme currentIssueSecurityScheme = issueSecuritySchemeManager.getSchemeFor(project);
            if (currentIssueSecurityScheme == null || !currentIssueSecurityScheme.getId().equals(issueSecuritySchemeId)) {
                // This check for -1 doesn't seem to be used, but it was there before ...
                if (new Long(-1).equals(issueSecuritySchemeId)) {
                    issueSecuritySchemeId = null;
                }
                issueSecuritySchemeManager.setSchemeForProject(project, issueSecuritySchemeId);
            }
        } catch (GenericEntityException e) {
            throw new DataAccessException(e);
        }
    }

    @Override
    public boolean isValidAllProjectData(JiraServiceContext serviceContext, ProjectCreationData projectCreationData) {
        final ErrorCollection errorCollection = ErrorCollections.empty();
        final I18nHelper i18nHelper = getI18nBean(serviceContext.getLoggedInApplicationUser());

        isValidRequiredProjectData(getServiceContext(serviceContext.getLoggedInApplicationUser(), errorCollection), projectCreationData);
        validateProjectUrl(projectCreationData.getUrl(), errorCollection, i18nHelper);
        validateProjectAssigneeType(projectCreationData.getAssigneeType(), errorCollection, i18nHelper);
        validateAvatarId(projectCreationData.getAvatarId(), projectManager.getProjectObjByKey(projectCreationData.getKey()), errorCollection, i18nHelper);
        validateProjectTypeAndTemplate(projectCreationData.getProjectTypeKey(), projectCreationData.getProjectTemplateKey(), errorCollection, i18nHelper);

        if (errorCollection.hasAnyErrors()) {
            serviceContext.getErrorCollection().addErrorCollection(errorCollection);
            return false;
        }

        return true;
    }

    private void validateProjectTypeAndTemplate(final ProjectTypeKey projectTypeKey, final ProjectTemplateKey projectTemplateKey, final ErrorCollection errorCollection, final I18nHelper i18nBean) {
        // Project template is optional
        if (projectTemplateKey == null || projectTemplateKey.getKey() == null) {
            return;
        }

        final Optional<ProjectTemplate> projectTemplate = projectTemplateManager.getProjectTemplate(projectTemplateKey);
        if (!projectTemplate.isPresent()) {
            errorCollection.addErrorMessage(i18nBean.getText("admin.errors.invalid.project.template"), Reason.VALIDATION_FAILED);
        } else if (!projectTypeMatchesProjectTemplate(projectTypeKey, projectTemplate.get())) {
            errorCollection.addErrorMessage(i18nBean.getText("admin.errors.project.template.type.mismatch"), Reason.VALIDATION_FAILED);
        }
    }

    private boolean projectTypeMatchesProjectTemplate(final ProjectTypeKey projectTypeKey, final ProjectTemplate projectTemplate) {
        return projectTemplate.getProjectTypeKey().equals(projectTypeKey);
    }

    @Override
    public boolean isValidRequiredProjectData(JiraServiceContext serviceContext, ProjectCreationData projectCreationData) {
        final ErrorCollection errorCollection = ErrorCollections.empty();
        final I18nHelper i18nBean = getI18nBean(serviceContext.getLoggedInApplicationUser());

        validateProjectName(projectCreationData.getName(), errorCollection, i18nBean);
        validateProjectKey(null, projectCreationData.getKey(), errorCollection, i18nBean);
        validateProjectLead(Option.option(projectCreationData.getLead()), errorCollection, i18nBean);
        validateProjectType(serviceContext.getLoggedInApplicationUser(), projectCreationData.getProjectTypeKey(), errorCollection, i18nBean);

        if (errorCollection.hasAnyErrors()) {
            serviceContext.getErrorCollection().addErrorCollection(errorCollection);
            return false;
        }

        return true;
    }

    @Override
    public boolean isValidProjectKey(JiraServiceContext serviceContext, String key) {
        final ErrorCollection errorCollection = ErrorCollections.empty();
        final I18nHelper i18nBean = getI18nBean(serviceContext.getLoggedInApplicationUser());

        validateProjectKey(null, key, errorCollection, i18nBean);

        if (errorCollection.hasAnyErrors()) {
            serviceContext.getErrorCollection().addErrorCollection(errorCollection);
            return false;
        }

        return true;
    }

    private ErrorCollection validateUpdateProjectParameters(final ApplicationUser user, final Project project, final UpdateProjectParameters parameters) {
        final I18nHelper i18nHelper = getI18nBean(user);
        final ErrorCollection errors = ErrorCollections.empty();

        if (parameters.getKey().isDefined()) {
            boolean canEditProjectKey = checkActionPermission(user, project, ProjectAction.EDIT_PROJECT_KEY);

            if (!canEditProjectKey) {
                errors.addErrorMessage(i18nHelper.getText(ProjectAction.EDIT_PROJECT_KEY.getErrorKey()), Reason.FORBIDDEN);
                return errors;
            }

            validateProjectKey(project, parameters.getKey().get(), errors, i18nHelper);
            if (errors.hasAnyErrors()) {
                return errors;
            }
        }

        if (parameters.getName().isDefined()) {
            validateProjectNameForUpdate(parameters.getName().get(), project.getKey(), errors, i18nHelper);
        }

        if (parameters.getLeadUserKey().isDefined()) {
            final ApplicationUser lead = userManager.getUserByKey(parameters.getLeadUserKey().get());
            validateProjectLead(Option.option(lead), errors, i18nHelper);
        } else if (parameters.getLeadUsername().isDefined()) {
            final ApplicationUser lead = userManager.getUserByName(parameters.getLeadUsername().get());
            validateProjectLead(Option.option(lead), errors, i18nHelper);
        }

        if (parameters.getUrl().isDefined()) {
            validateProjectUrl(parameters.getUrl().get(), errors, i18nHelper);
        }

        if (parameters.getAssigneeType().isDefined()) {
            validateProjectAssigneeType(parameters.getAssigneeType().get(), errors, i18nHelper);
        }

        if (parameters.getAvatarId().isDefined()) {
            validateAvatarId(parameters.getAvatarId().get(), project, errors, i18nHelper);
        }

        if (parameters.getProjectTypeKey().isDefined()) {
            validateProjectType(user, new ProjectTypeKey(parameters.getProjectTypeKey().get()), errors, i18nHelper);
        }

        if (parameters.getProjectCategoryId().isDefined()) {
            validateProjectCategoryId(parameters.getProjectCategoryId().get(), user, errors, i18nHelper);
        }

        return errors;
    }

    /*
     * Does the same as isValidAllProjectData except that the project key doesn't need to be validated.
     */
    private ErrorCollection validateUpdateProjectData(ApplicationUser user, String name, Project oldProject, ApplicationUser lead,
                                                      String url, Long assigneeType, Long avatarId) {
        final ErrorCollection errorCollection = ErrorCollections.empty();
        final I18nHelper i18nHelper = getI18nBean(user);

        validateProjectNameForUpdate(name, oldProject.getKey(), errorCollection, i18nHelper);
        validateProjectLead(Option.option(lead), errorCollection, i18nHelper);
        validateProjectUrl(url, errorCollection, i18nHelper);
        validateProjectAssigneeType(assigneeType, errorCollection, i18nHelper);
        validateAvatarId(avatarId, oldProject, errorCollection, i18nHelper);

        return errorCollection;
    }


    @Override
    public String getProjectKeyDescription() {
        String projectKeyDescription = applicationProperties.getDefaultBackedString(APKeys.JIRA_PROJECTKEY_DESCRIPTION);
        final I18nHelper i18nBean = jiraAuthenticationContext.getI18nHelper();
        if (TextUtils.stringSet(projectKeyDescription)) {
            return i18nBean.getText(projectKeyDescription);
        } else {
            return i18nBean.getText("admin.projects.key.description");
        }
    }

    @Override
    public GetProjectResult getProjectByIdForAction(ApplicationUser user, Long id, ProjectAction action) {
        Project project = projectManager.getProjectObj(id);
        if (project == null || !checkActionPermission(user, project, ProjectAction.VIEW_PROJECT)) {
            return new ProjectNotFoundResult(user, "admin.errors.project.not.found.for.id", String.valueOf(id));
        }

        if (!checkActionPermission(user, project, action)) {
            return new PermissionErrorResult(user, action);
        }

        return new GetProjectResult(project);
    }

    @Override
    public GetProjectResult getProjectById(ApplicationUser user, Long id) {
        return getProjectByIdForAction(user, id, ProjectAction.VIEW_ISSUES);
    }

    @Override
    public GetProjectResult getProjectByKey(ApplicationUser user, String key) {
        return getProjectByKeyForAction(user, key, ProjectAction.VIEW_ISSUES);
    }

    @Override
    public GetProjectResult getProjectByKeyForAction(ApplicationUser user, String key, ProjectAction action) {
        Assertions.notNull("action", action);

        Project project = projectManager.getProjectObjByKey(key);
        if (project == null || !checkActionPermission(user, project, ProjectAction.VIEW_PROJECT)) {
            return new ProjectNotFoundResult(user, "admin.errors.project.not.found.for.key", key);
        }

        if (!checkActionPermission(user, project, action)) {
            return new PermissionErrorResult(user, action);
        }

        return new GetProjectResult(project);
    }


    @Override
    public int getMaximumNameLength() {
        final String maxStr = applicationProperties.getDefaultBackedString(APKeys.JIRA_PROJECTNAME_MAX_LENGTH);
        try {
            return Integer.parseInt(maxStr);
        } catch (NumberFormatException e) {
            log.error("Unable to read the MaximalProjectNameLength value '" + maxStr + "'.  Defaulting to " + DEFAULT_NAME_LENGTH + ".");
            return DEFAULT_NAME_LENGTH;
        }
    }

    @Override
    public int getMaximumKeyLength() {
        final String maxStr = applicationProperties.getDefaultBackedString(APKeys.JIRA_PROJECTKEY_MAX_LENGTH);
        try {
            return Integer.parseInt(maxStr);
        } catch (NumberFormatException e) {
            log.error("Unable to read the MaximalProjectKeyLength value '" + maxStr + "'.  Defaulting to " + DEFAULT_KEY_LENGTH + ".");
            return DEFAULT_KEY_LENGTH;
        }
    }

    @Override
    public long getProjectCount() {
        return projectManager.getProjectCount();
    }

    @Override
    public ServiceOutcome<List<Project>> getAllProjects(final ApplicationUser user) {
        return getAllProjectsForAction(user, ProjectAction.VIEW_ISSUES);
    }

    @Override
    public ServiceOutcome<List<Project>> getAllProjectsForAction(final ApplicationUser user, final ProjectAction action) {
        final Iterable<Project> projects = Iterables.filter(projectManager.getProjectObjects(), new Predicate<Project>() {
            @Override
            public boolean apply(@Nullable Project input) {
                return (input != null) && checkActionPermission(user, input, action);
            }
        });
        return new ServiceOutcomeImpl<List<Project>>(ErrorCollections.empty(), Lists.newArrayList(projects));

    }

    @Override
    public Either<Project, ErrorCollection> updateProjectType(ApplicationUser user, Project project, ProjectTypeKey newProjectType) {
        I18nHelper i18nBean = getI18nBean(user);
        ErrorCollection errorCollection = new SimpleErrorCollection();
        if (!checkActionPermission(user, project, ProjectAction.EDIT_PROJECT_CONFIG)) {
            errorCollection.addErrorMessage(i18nBean.getText("admin.errors.project.type.update.no.permission"), Reason.FORBIDDEN);
        }
        validateProjectType(user, newProjectType, errorCollection, i18nBean);

        if (errorCollection.hasAnyErrors()) {
            return Either.right(errorCollection);
        }

        return executeUnderClusterLock(getLockNameFor(project), () -> {
            Project updatedProject = projectManager.updateProjectType(user, project, newProjectType);
            return notifyProjectTypeUpdated(user, updatedProject, project.getProjectTypeKey(), newProjectType, true);
        });
    }

    private Either<Project, ErrorCollection> executeUnderClusterLock(String lockName, Supplier<Either<Project, ErrorCollection>> supplier) {
        ClusterLock lock = clusterLockService.getLockForName(lockName);
        lock.lock();
        try {
            return supplier.get();
        } finally {
            lock.unlock();
        }
    }

    private String getLockNameFor(Project project) {
        return DefaultProjectService.class.getName() + ".updateProjectType." + project.getId();
    }

    private Either<Project, ErrorCollection> notifyProjectTypeUpdated(ApplicationUser user, Project project, ProjectTypeKey oldProjectType, ProjectTypeKey newProjectType, boolean rollback) {
        boolean notificationWasSuccessful = projectTypeUpdatedNotifier.notifyAllHandlers(user, project, oldProjectType, newProjectType);
        if (!notificationWasSuccessful) {
            // rollback the project type update, since something went wrong on one of the project type update handlers
            if (rollback) {
                projectManager.updateProjectType(user, project, oldProjectType);
            }
            return Either.right(new SimpleErrorCollection("admin.errors.project.type.update.error", Reason.SERVER_ERROR));
        }
        return Either.left(project);
    }

    /*
     * Default Assignee field is not on all forms, only validate if present
     */
    private void validateProjectAssigneeType(final Long assigneeType, final ErrorCollection errorCollection,
                                             final I18nHelper i18nHelper) {
        if (assigneeType != null && !ProjectAssigneeTypes.isValidType(assigneeType)) {
            errorCollection.addErrorMessage(i18nHelper.getText("admin.errors.invalid.default.assignee"));
        }
    }

    /**
     * Checks that the avatar id is for an existing suitable avatar.
     *
     * @param avatarId        null or the id of an existing avatar
     * @param oldProject      the project before the update, null if this is a create.
     * @param errorCollection the errorCollection
     * @param i18nHelper      the I18nHelper
     */
    private void validateAvatarId(final Long avatarId, final Project oldProject, final ErrorCollection errorCollection,
                                  final I18nHelper i18nHelper) {
        if (avatarId == null) {
            return;
        }
        final Avatar avatar = avatarManager.getById(avatarId);
        if (avatar == null) {
            errorCollection.addErrorMessage(i18nHelper.getText("admin.errors.avatar.does.not.exist", avatarId), Reason.VALIDATION_FAILED);
        } else if (!avatar.isSystemAvatar() && !oldProject.getId().toString().equals(avatar.getOwner())) {
            errorCollection.addErrorMessage(i18nHelper.getText("admin.errors.invalid.avatar"));
        }
    }

    /*
     * URL is optional, only validate if present
     */
    private void validateProjectUrl(final String url, final ErrorCollection errorCollection, final I18nHelper i18nBean) {
        if (TextUtils.stringSet(url)) {
            if (url.length() > MAX_FIELD_LENGTH) {
                errorCollection.addError(PROJECT_URL, i18nBean.getText("admin.errors.project.url.too.long"));
            } else if (!TextUtils.verifyUrl(url)) {
                errorCollection.addError(PROJECT_URL, i18nBean.getText("admin.errors.url.specified.is.not.valid"));
            }
        }
    }

    private void validateProjectType(final ApplicationUser user, final ProjectTypeKey projectType, final ErrorCollection errorCollection, final I18nHelper i18nBean) {
        if (!projectTypeValidator.isValid(user, projectType)) {
            errorCollection.addError(PROJECT_TYPE, i18nBean.getText("admin.errors.invalid.project.type"), Reason.VALIDATION_FAILED);
        }
    }

    private void validateProjectCategoryId(@Nonnull final Long projectCategoryId, @Nonnull ApplicationUser user, @Nonnull final ErrorCollection errorCollection, @Nonnull final I18nHelper i18nBean) {
        if (!globalPermissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, user)) {
            errorCollection.addError(PROJECT_CATEGORY_ID, i18nBean.getText("admin.projects.service.error.no.admin.permission.projectcategory"), Reason.FORBIDDEN);
        } else if (!isValidProjectCategory(projectCategoryId)) {
            errorCollection.addError(PROJECT_CATEGORY_ID, i18nBean.getText("admin.errors.project.specify.project.category"), Reason.VALIDATION_FAILED);
        }
    }

    private boolean isValidProjectCategory(Long projectCategoryId) {
        return projectCategoryId.equals(NONE_PROJECT_CATEGORY_ID) || projectManager.getProjectCategory(projectCategoryId) != null;
    }

    /*
     * check key is set, is valid, is unique and not an OS reserved word
     */
    private void validateProjectKey(final Project oldProject, final String key, final ErrorCollection errorCollection, final I18nHelper i18nBean) {
        if (!isProjectKeyValid(key)) {
            String projectKeyWarning = applicationProperties.getDefaultBackedString(APKeys.JIRA_PROJECTKEY_WARNING);
            if (TextUtils.stringSet(projectKeyWarning)) {
                errorCollection.addError(PROJECT_KEY, i18nBean.getText(projectKeyWarning));
            } else {
                errorCollection.addError(PROJECT_KEY, i18nBean.getText("admin.errors.must.specify.unique.project.key"));
            }
        } else if (isReservedKeyword(key)) {
            errorCollection.addError(PROJECT_KEY, i18nBean.getText("admin.errors.project.keyword.invalid"));
        } else {
            if (key.length() > getMaximumKeyLength()) {
                errorCollection.addError(PROJECT_KEY, i18nBean.getText("admin.errors.project.key.too.long", getMaximumKeyLength()));
            } else {
                final Project projectByNewKey = projectManager.getProjectObjByKey(key);
                if (projectByNewKey != null) {
                    if (oldProject == null || !oldProject.getId().equals(projectByNewKey.getId())) {
                        errorCollection.addError(PROJECT_KEY, i18nBean.getText("admin.errors.project.with.that.key.already.exists", projectByNewKey.getName()));
                    }
                } else {
                    //JDEV-24899 we defensively check against ProjectKey that is associated with non-existent Project
                    final Long projectId = projectKeyStore.getProjectId(key);
                    if (projectId != null) {
                        errorCollection.addError(PROJECT_KEY, i18nBean.getText("admin.errors.project.with.that.key.already.exists.unknown"));
                    }
                }
            }
        }
    }

    /*
     * check project lead is set and exists in the system
     */
    private void validateProjectLead(final Option<ApplicationUser> lead, final ErrorCollection errorCollection, final I18nHelper i18nBean) {
        if (lead.isEmpty()) {
            errorCollection.addError(PROJECT_LEAD, i18nBean.getText("admin.errors.must.specify.project.lead"));
        }
    }

    /*
     * check project name is set and is unique
     */
    private void validateProjectName(final String name, final ErrorCollection errorCollection, final I18nHelper i18nBean) {
        projectNameValidator.validateForCreate(name, errorCollection, i18nBean);
    }

    /*
     * check project name is set and is unique or belongs to the same project
     */
    private void validateProjectNameForUpdate(final String name, final String key, final ErrorCollection errorCollection,
                                              final I18nHelper i18nBean) {
        projectNameValidator.validateForUpdate(name, key, errorCollection, i18nBean);
    }

    /**
     * @since v6.1
     */
    final protected I18nHelper getI18nBean(ApplicationUser user) {
        return i18nFactory.getInstance(user);
    }

    boolean isReservedKeyword(final String key) {
        return JiraKeyUtils.isReservedKeyword(key);
    }

    boolean isProjectKeyValid(final String key) {
        return JiraKeyUtils.validProjectKey(key);
    }

    /**
     * Check whether a user has the permission to perform the given action on the given project.
     *
     * @param user    user to check
     * @param project project to check the permission on
     * @param action  action to check
     * @return whether the user has the permission to perform the given action on the given project
     * @since v6.1
     */
    boolean checkActionPermission(ApplicationUser user, Project project, ProjectAction action) {
        return action.hasPermission(permissionManager, user, project);
    }

    /**
     * A GetProjectResult that indicates a project was not found.
     */
    protected class ProjectNotFoundResult extends GetProjectResult {
        protected ProjectNotFoundResult(ApplicationUser user, String i18nKey, String... params) {
            super(makeErrorCollection(user, i18nKey, Reason.NOT_FOUND, params));
        }
    }

    /**
     * A GetProjectResult that indicates the calling user does not have permission to perform an action on a project.
     */
    protected class PermissionErrorResult extends GetProjectResult {
        protected PermissionErrorResult(ApplicationUser user, ProjectAction action) {
            super(makeErrorCollection(user, action.getErrorKey(), Reason.FORBIDDEN));
        }
    }

    /**
     * Creates a new ErrorCollection instance for a single error message. If the reason "forbidden" and the user is
     * null, this method adds an additional error message along the lines of "You are not logged in, please log in".
     *
     * @param user    the ApplicationUser for whom the message will be i18n'ed
     * @param i18nKey a String containing an i18n key
     * @param reason  a Reason code
     * @param params  a String array containing the i18n params  @return a new ErrorCollection
     * @return a new ErrorCollection
     * @since v6.1
     */
    protected final ErrorCollection makeErrorCollection(ApplicationUser user, String i18nKey, ErrorCollection.Reason reason, String... params) {
        final ErrorCollection errors = ErrorCollections.empty();
        final I18nHelper i18nBean = getI18nBean(user);
        errors.addErrorMessage(i18nBean.getText(i18nKey, params), reason);

        // lack of permissions may be due to the user not being logged in
        if (reason == Reason.FORBIDDEN && user == null) {
            errors.addErrorMessage(i18nBean.getText("common.forms.ajax.unauthorised.alert"), Reason.NOT_LOGGED_IN);
        }

        return errors;
    }

    /**
     * Returns the AssigneeType to use for the new project. If {@code requestedAssigneeType} is non-null then that
     * assignee type is used. Otherwise the assignee type is selected according to the JIRA "Allow Unassigned Issues"
     * global configuration:
     * <ul>
     * <li>if AllowUnassigned=ON in JIRA, the default assignee is {@link com.atlassian.jira.project.AssigneeTypes#UNASSIGNED}</li>
     * <li>otherwise the default assignee is {@link com.atlassian.jira.project.AssigneeTypes#PROJECT_LEAD}</li>
     * </ul>
     *
     * @param requestedAssigneeType the requested assignee type
     * @return an assignee type
     */
    private Long effectiveAssigneeType(Long requestedAssigneeType) {
        if (requestedAssigneeType != null) {
            return requestedAssigneeType;
        }

        if (applicationProperties.getOption(APKeys.JIRA_OPTION_ALLOWUNASSIGNED)) {
            return AssigneeTypes.UNASSIGNED;
        } else {
            return AssigneeTypes.PROJECT_LEAD;
        }
    }


    private String keyFor(String username) {
        return ApplicationUsers.getKeyFor(getUserByName(username));
    }

    private String nameFor(ApplicationUser user) {
        return (user != null) ? user.getUsername() : null;
    }

    private ApplicationUser getUserByName(final String leadName) {
        return userManager.getUserByName(leadName);
    }

}
