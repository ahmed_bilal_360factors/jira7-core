package com.atlassian.jira.project;

import com.atlassian.core.ofbiz.util.OFBizPropertyUtils;
import com.atlassian.jira.avatar.Avatar;
import com.atlassian.jira.avatar.AvatarManager;
import com.atlassian.jira.bc.project.component.ProjectComponent;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.icon.IconType;
import com.atlassian.jira.issue.fields.config.manager.IssueTypeSchemeManager;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.project.type.ProjectTypeKey;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.UserUtils;
import org.ofbiz.core.entity.GenericValue;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.NoSuchElementException;

/**
 * Represents an immutable Project domain object for JIRA.
 */
public class ProjectImpl implements Project {
    private final GenericValue projectGV;
    private final String key;
    private final Long id;
    private ApplicationUser lead;

    public ProjectImpl(GenericValue projectGv) {
        this.projectGV = projectGv;
        key = getStringFromGV("key");
        id = getLongFromGV("id");
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return getStringFromGV("name");
    }

    public String getKey() {
        return key;
    }

    public String getUrl() {
        return getStringFromGV("url");
    }

    @Override
    public String getEmail() {
        return OFBizPropertyUtils.getPropertySet(projectGV).getString(ProjectKeys.EMAIL_SENDER);
    }

    @Override
    public ApplicationUser getProjectLead() {
        return ComponentAccessor.getUserManager().getUserByKey(getLeadUserKey());
    }

    public ApplicationUser getLead() {
        if (getLeadUserName() != null && lead == null) {
            lead = getUser(getLeadUserName());
        }
        return lead;
    }

    @Override
    public String getLeadUserName() {
        ApplicationUser lead = getProjectLead();
        if (lead == null) {
            return getLeadUserKey();
        }
        return lead.getUsername();
    }

    @Override
    public String getLeadUserKey() {
        return getStringFromGV("lead");
    }

    @Override
    public String getOriginalKey() {
        return getStringFromGV("originalkey");
    }

    public String getDescription() {
        String value = getStringFromGV("description");
        return value == null ? "" : value;
    }

    public Long getAssigneeType() {
        return getLongFromGV("assigneetype");
    }

    @Override
    public ProjectTypeKey getProjectTypeKey() {
        return new ProjectTypeKey(getStringFromGV("projecttype"));
    }

    public Collection<ProjectComponent> getComponents() {
        return ComponentAccessor.getProjectComponentManager().findAllForProject(getId());
    }

    public Collection<ProjectComponent> getProjectComponents() {
        return getComponents();
    }

    public Collection<Version> getVersions() {
        return ComponentAccessor.getVersionManager().getVersions(projectGV.getLong("id"));
    }

    @Override
    public Collection<IssueType> getIssueTypes() {
        return ComponentAccessor.getComponent(IssueTypeSchemeManager.class).getIssueTypesForProject(this);
    }

    @Override
    public ProjectCategory getProjectCategory() {
        return ComponentAccessor.getProjectManager().getProjectCategoryForProject(this);
    }

    @Override
    public ProjectCategory getProjectCategoryObject() {
        return getProjectCategory();
    }

    @Nonnull
    public Avatar getAvatar() {
        final AvatarManager avatarManager = ComponentAccessor.getAvatarManager();
        final Avatar projectAvatar = avatarManager.getById(getLongFromGV("avatar"));
        if (null == projectAvatar) {
            final Avatar defaultAvatar = avatarManager.getDefaultAvatar(IconType.PROJECT_ICON_TYPE);
            if (null == defaultAvatar) {
                throw new NoSuchElementException("There is no project default avatar - configuration failure!");
            }

            return defaultAvatar;
        } else {
            return projectAvatar;
        }
    }

    public GenericValue getGenericValue() {
        return projectGV;
    }

    private String getStringFromGV(String key) {
        if (projectGV != null) {
            return projectGV.getString(key);
        }
        return null;
    }

    private Long getLongFromGV(String key) {
        if (projectGV != null) {
            return projectGV.getLong(key);
        }
        return null;
    }

    private ApplicationUser getUser(String username) {
        return UserUtils.getUserEvenWhenUnknown(username);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Project)) {
            return false;
        }

        final Project other = (Project) o;
        // JRA-20184, JRADEV-21134: All the Project properties can change except ID

        if (getId() == null) {
            return other.getId() == null;
        } else {
            return getId().equals(other.getId());
        }
    }

    @Override
    public int hashCode() {
        final Long id = getId();
        return id != null ? id.hashCode() : 0;
    }

    public String toString() {
        return "Project: " + getKey();
    }
}
