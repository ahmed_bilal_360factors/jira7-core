package com.atlassian.jira.cluster.distribution;

import com.atlassian.jira.cluster.ClusterManager;
import com.atlassian.jira.cluster.Node;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.component.ComponentReference;
import com.atlassian.jira.util.log.RateLimitingLogger;
import net.sf.ehcache.CacheException;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Ehcache;
import net.sf.ehcache.distribution.CachePeer;
import net.sf.ehcache.distribution.RMICacheManagerPeerProvider;

import java.net.MalformedURLException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.stream.Collectors;

/**
 * Jira Cache Manager Peer Provider This provider will match with the cluster manager to get all the live nodes and
 * build the proper rmi points where to propagate the information
 *
 * @since 6.3.4
 */
public class JiraCacheManagerPeerProvider extends RMICacheManagerPeerProvider {
    private static final RateLimitingLogger log = new RateLimitingLogger(JiraCacheManagerPeerProvider.class);
    private final ExecutorService executorService;

    public JiraCacheManagerPeerProvider(final CacheManager cacheManager, final ExecutorService executorService) {
        super(cacheManager);
        this.executorService = executorService;
    }

    private final ComponentReference<ClusterManager> clusterManagerRef = ComponentAccessor.getComponentReference(ClusterManager.class);

    @Override
    public void init() {
    }

    @Override
    public long getTimeForClusterToForm() {
        return 0;
    }

    @Override
    public final void registerPeer(final String rmiUrl) {
        //Nothing to do here
    }

    @Override
    public List<CachePeer> listRemoteCachePeers(final Ehcache cache) throws CacheException {
        final ClusterManager clusterManager = getClusterManager();
        final String currentNodeId = clusterManager.getNodeId();
        final Collection<Node> liveNodes = clusterManager.findLiveNodes();


        return waitForAll(
                liveNodes.stream()
                        .filter(node -> node != null && !currentNodeId.equals(node.getNodeId()))
                        .map(node -> getCachePeerAsync(cache, node))
                        .collect(Collectors.toList())
        );
    }

    private CompletableFuture<Optional<CachePeer>> getCachePeerAsync(final Ehcache cache, final Node node) {
        final String rmiUrl = buildBaseUrl(node, cache.getName());

        return CompletableFuture.supplyAsync(new ClassLoaderSwitchingSupplier<>(() -> {
            try {
                return Optional.of(lookupRemoteCachePeer(rmiUrl));
            } catch (final RemoteException e) {
                log.warn("Looking up rmiUrl " + rmiUrl + " threw a connection exception. This could mean that a node has gone offline "
                        + " or it may indicate network connectivity difficulties. Details: " + e.getMessage());
            } catch (final MalformedURLException e) {
                log.error("Looking up rmiUrl " + rmiUrl + " through exception . Urls are not well formed. Please fix this.");
            } catch (final NotBoundException e) {
                log.debug("Looking up rmiUrl " + rmiUrl + " threw a connection exception. This may be normal if a node has gone offline."
                        + " Or it may indicate network connectivity difficulties. Details : " + e.getMessage());
            }
            return Optional.empty();
        }), executorService);
    }

    private <T> List<T> waitForAll(final List<CompletableFuture<Optional<T>>> stages) {
        return stages.stream()
                .map(CompletableFuture::join)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toList());
    }

    @Override
    protected boolean stale(final Date date) {
        return false;
    }

    private String buildBaseUrl(final Node node, final String cacheName) {
        return "//" + node.getIp() + ':' + node.getCacheListenerPort() + '/' + cacheName;
    }

    /**
     * We need to do this cause we cannot assign the cluster manager in initialization
     *
     * @return the cluster manager
     */
    private ClusterManager getClusterManager() {
        return clusterManagerRef.get();
    }
}
