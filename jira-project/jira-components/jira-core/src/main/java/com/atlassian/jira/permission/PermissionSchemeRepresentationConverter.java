package com.atlassian.jira.permission;

import com.atlassian.fugue.Option;
import com.atlassian.jira.permission.data.PermissionGrantImpl;
import com.atlassian.jira.permission.data.PermissionSchemeImpl;
import com.atlassian.jira.scheme.Scheme;
import com.atlassian.jira.scheme.SchemeEntity;
import com.atlassian.jira.security.plugin.ProjectPermissionKey;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;

import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;

import static com.atlassian.fugue.Options.flatten;
import static com.atlassian.jira.permission.PermissionSchemeUtil.getPermissionKey;
import static com.atlassian.jira.permission.data.CustomPermissionHolderType.permissionHolderType;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Iterables.transform;

@ParametersAreNonnullByDefault
public final class PermissionSchemeRepresentationConverter {
    private static final String SCHEME_TYPE = "permission";

    public PermissionSchemeRepresentationConverter() {
    }

    public PermissionScheme permissionScheme(final Scheme scheme) {
        return new PermissionSchemeImpl(
                scheme.getId(),
                scheme.getName(),
                scheme.getDescription(),
                flatten(transform(scheme.getEntities(), new Function<SchemeEntity, Option<PermissionGrant>>() {
                    @Override
                    public Option<PermissionGrant> apply(final SchemeEntity input) {
                        return permissionGrant(input);
                    }
                })));
    }

    public Scheme scheme(final PermissionScheme permissionScheme) {
        return new Scheme(
                permissionScheme.getId(),
                SCHEME_TYPE,
                permissionScheme.getName(),
                permissionScheme.getDescription(),
                ImmutableList.copyOf(transform(permissionScheme.getPermissions(), new Function<PermissionGrant, SchemeEntity>() {
                    @Override
                    public SchemeEntity apply(final PermissionGrant input) {
                        return schemeEntity(input, permissionScheme.getId());
                    }
                }))
        );
    }

    public Scheme scheme(final PermissionSchemeInput permissionScheme) {
        return new Scheme(
                null,
                SCHEME_TYPE,
                permissionScheme.getName(),
                permissionScheme.getDescription().getOrNull(),
                ImmutableList.copyOf(transform(permissionScheme.getPermissions(), new Function<PermissionGrantInput, SchemeEntity>() {
                    @Override
                    public SchemeEntity apply(final PermissionGrantInput input) {
                        return schemeEntity(input, null);
                    }
                }))
        );
    }

    public Option<PermissionGrant> permissionGrant(final SchemeEntity schemeEntity) {
        return getPermissionKey(schemeEntity).map(new Function<ProjectPermissionKey, PermissionGrant>() {
            @Override
            public PermissionGrant apply(final ProjectPermissionKey permissionKey) {
                return new PermissionGrantImpl(
                        schemeEntity.getId(),
                        holder(schemeEntity.getType(), schemeEntity.getParameter()),
                        permissionKey
                );
            }
        });
    }

    public SchemeEntity schemeEntity(final PermissionGrant permissionGrant, Long schemeId) {
        checkNotNull(schemeId);
        return new SchemeEntity(
                permissionGrant.getId(),
                permissionGrant.getHolder().getType().getKey(),
                permissionGrant.getHolder().getParameter().getOrNull(),
                permissionGrant.getPermission(),
                null,
                schemeId
        );
    }

    public SchemeEntity schemeEntity(final PermissionGrantInput permissionGrant, @Nullable Long schemeId) {
        return new SchemeEntity(
                null,
                permissionGrant.getHolder().getType().getKey(),
                permissionGrant.getHolder().getParameter().getOrNull(),
                permissionGrant.getPermission(),
                null,
                schemeId
        );
    }

    private static PermissionHolder holder(final String key, @Nullable final String parameter) {
        Option<? extends PermissionHolderType> jiraPermissionHolderType = JiraPermissionHolderType.fromKey(key, parameter);
        if (jiraPermissionHolderType.isDefined()) {
            return PermissionHolder.holder(jiraPermissionHolderType.get(), parameter);
        } else {
            return PermissionHolder.holder(permissionHolderType(key, parameter), parameter);
        }
    }

}
