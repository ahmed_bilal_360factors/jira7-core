package com.atlassian.jira.event.issue.txnaware;

import com.atlassian.jira.issue.comments.Comment;
import com.atlassian.jira.user.ApplicationUser;

/**
 * An event factory that will create events and fire them on commit boundaries
 *
 * @since v7.2
 */
public interface TxnAwareEventFactory {

    void issueChangedEventOnCommit(long issueId, ApplicationUser caller, Comment comment, long changeGroupId);
}
