package com.atlassian.jira.issue.customfields.impl;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.export.FieldExportParts;
import com.atlassian.jira.issue.export.FieldExportPartsBuilder;
import com.atlassian.jira.issue.export.customfield.CustomFieldExportContext;
import com.atlassian.jira.issue.customfields.manager.GenericConfigManager;
import com.atlassian.jira.issue.customfields.persistence.CustomFieldValuePersister;
import com.atlassian.jira.issue.fields.TextFieldCharacterLengthValidator;
import com.atlassian.jira.issue.export.customfield.ExportableCustomFieldType;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.opensymphony.util.UrlUtils;

public class URLCFType extends GenericTextCFType implements ExportableCustomFieldType {

    public URLCFType(final CustomFieldValuePersister customFieldValuePersister, final GenericConfigManager genericConfigManager,
                     final TextFieldCharacterLengthValidator textFieldCharacterLengthValidator, final JiraAuthenticationContext jiraAuthenticationContext) {
        super(customFieldValuePersister, genericConfigManager, textFieldCharacterLengthValidator, jiraAuthenticationContext);
    }

    public String getSingularObjectFromString(final String string) throws FieldValidationException {
        // JRA-14998 - trim URLs before validating. URLs will also be saved in trim form.
        final String uri = (string == null) ? null : string.trim();
        if (!UrlUtils.verifyHierachicalURI(uri)) {
            throw new FieldValidationException("Not a valid URL");
        }
        return uri;
    }

    @Override
    public Object accept(VisitorBase visitor) {
        if (visitor instanceof Visitor) {
            return ((Visitor) visitor).visitURL(this);
        }

        return super.accept(visitor);
    }

    @Override
    public FieldExportParts getRepresentationFromIssue(Issue issue, CustomFieldExportContext context) {
        final String value = getValueFromIssue(context.getCustomField(), issue);
        return FieldExportPartsBuilder.buildSinglePartRepresentation(context.getCustomField().getId(), context.getDefaultColumnHeader(), value);
    }

    public interface Visitor<T> extends VisitorBase<T> {
        T visitURL(URLCFType urlCustomFieldType);
    }
}
