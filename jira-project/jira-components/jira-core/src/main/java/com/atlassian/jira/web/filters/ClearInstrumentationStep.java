package com.atlassian.jira.web.filters;

import com.atlassian.instrumentation.operations.OpTimerFactory;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.instrumentation.Instrumentation;
import com.atlassian.jira.web.filters.steps.FilterCallContext;
import com.atlassian.jira.web.filters.steps.FilterStep;

public class ClearInstrumentationStep implements FilterStep {
    @Override
    public FilterCallContext beforeDoFilter(FilterCallContext callContext) {
        // clear instruments for thread
        Instrumentation.snapshotThreadLocalOperationsAndClear(
                ComponentAccessor.getComponentSafely(OpTimerFactory.class).orElse(null));

        return callContext;
    }

    @Override
    public FilterCallContext finallyAfterDoFilter(FilterCallContext callContext) {
        return callContext;
    }
}
