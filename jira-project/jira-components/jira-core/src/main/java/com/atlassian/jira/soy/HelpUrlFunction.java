package com.atlassian.jira.soy;

import com.atlassian.jira.help.HelpUrl;
import com.atlassian.jira.help.HelpUrls;

import javax.annotation.Nonnull;

/**
 * getJiraHelpUrl('help_url_key_here') soy function.
 *
 * <p>Soy function that returns the help URL for the provided help key.
 *
 * @since v7.0
 */
public class HelpUrlFunction extends AbstractHelpFunction {

    public HelpUrlFunction(HelpUrls helpUrls) {
        super(helpUrls);
    }

    @Override
    public String getName() {
        return "getJiraHelpUrl";
    }

    @Override
    @Nonnull
    String getHelpValue(HelpUrl helpUrl) {
        return helpUrl.getUrl();
    }
}
