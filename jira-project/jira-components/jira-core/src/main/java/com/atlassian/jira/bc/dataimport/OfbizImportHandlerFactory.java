package com.atlassian.jira.bc.dataimport;

import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.config.util.AttachmentPathManager;
import com.atlassian.jira.config.util.IndexPathManager;
import com.atlassian.jira.ofbiz.OfBizDelegator;

import java.util.Set;
import java.util.concurrent.Executor;

/**
 * Creates an instance of {@link com.atlassian.jira.bc.dataimport.OfbizImportHandler}. Mostly introduced
 * to make {@link com.atlassian.jira.bc.dataimport.DefaultDataImportService} a bit more unit testable.
 */
public class OfbizImportHandlerFactory {
    private final OfBizDelegator ofBizDelegator;

    private final IndexPathManager indexPathManager;

    private final AttachmentPathManager attachmentPathManager;

    private final FeatureManager featureManager;

    public OfbizImportHandlerFactory(OfBizDelegator ofBizDelegator, IndexPathManager indexPathManager,
                                     AttachmentPathManager attachmentPathManager, FeatureManager featureManager) {
        this.ofBizDelegator = ofBizDelegator;
        this.indexPathManager = indexPathManager;
        this.attachmentPathManager = attachmentPathManager;
        this.featureManager = featureManager;
    }

    OfbizImportHandler create(boolean useDefaultPaths, Set<String> propertyKeysToRecord, Executor executor) {
        return new OfbizImportHandler(ofBizDelegator, executor, indexPathManager,
                attachmentPathManager, useDefaultPaths, propertyKeysToRecord);
    }
}
