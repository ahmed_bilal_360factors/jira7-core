package com.atlassian.jira.plugin.dataprovider;

import com.atlassian.jira.security.AdminIssueLockoutFlagManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.ComponentLocator;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;
import com.atlassian.json.marshal.Jsonable;
import com.atlassian.webresource.api.data.WebResourceDataProvider;
import com.google.common.base.Supplier;
import com.google.common.base.Suppliers;
import com.google.common.collect.ImmutableMap;

import javax.inject.Inject;
import java.util.Map;

/**
 * Warn the admin when they can't see issues because they don't have any applications.
 *
 * @since v7.0
 */
public class AdminIssueLockoutFlagDataProvider implements WebResourceDataProvider {
    private final static Map<String, Object> DATA = ImmutableMap.of("noprojects", true,
            "manageAccessUrl", "secure/admin/ApplicationAccess.jspa",
            "flagId", AdminIssueLockoutFlagManager.FLAG);

    /**
     * See {@link com.atlassian.jira.plugin.dataprovider.AdminIssueLockoutFlagDataProvider.Delegate}.
     */
    private final Supplier<Delegate> delegateSupplier;

    @Inject
    public AdminIssueLockoutFlagDataProvider(ComponentLocator locator) {
        this.delegateSupplier = Suppliers.memoize(() ->
        {
            final AdminIssueLockoutFlagManager lockout = locator.getComponent(AdminIssueLockoutFlagManager.class);
            final JiraAuthenticationContext context = locator.getComponent(JiraAuthenticationContext.class);

            return new Delegate(lockout, context);
        });
    }

    @Override
    public Jsonable get() {
        return writer ->
        {
            try {
                getObject().write(writer);
            } catch (JSONException e) {
                throw new Jsonable.JsonMappingException(e);
            }
        };
    }

    private JSONObject getObject() {
        if (delegateSupplier.get().isAdminWithoutIssuePermission()) {
            return new JSONObject(DATA);
        } else {
            return new JSONObject();
        }
    }

    /**
     * Why do we have this delegate? The AdminIssueLockoutFlagDataProvider class is created by the plugin system
     * before JIRA is ready so we have this delegate so that we can lazy load the dependencies the first
     * time its called.
     */
    private static class Delegate {
        private final AdminIssueLockoutFlagManager noAccessManager;
        private final JiraAuthenticationContext jiraAuthenticationContext;

        private Delegate(final AdminIssueLockoutFlagManager noAccessManager,
                         final JiraAuthenticationContext jiraAuthenticationContext) {
            this.noAccessManager = noAccessManager;
            this.jiraAuthenticationContext = jiraAuthenticationContext;
        }

        private boolean isAdminWithoutIssuePermission() {
            final ApplicationUser loggedInUser = jiraAuthenticationContext.getLoggedInUser();
            return noAccessManager.isAdminWithoutIssuePermission(loggedInUser);
        }
    }
}
