package com.atlassian.jira.upgrade.tasks.role;

import com.atlassian.fugue.Option;
import com.atlassian.jira.application.ApplicationKeys;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.license.Jira6xServiceDeskPluginEncodedLicenseSupplier;
import com.google.common.base.Supplier;
import com.google.common.base.Suppliers;

import static com.atlassian.jira.util.dbc.Assertions.notNull;


public class Jira6xServiceDeskLicenseProviderImpl implements Jira6xServiceDeskLicenseProvider {
    private final Jira6xServiceDeskPluginEncodedLicenseSupplier sdLicenseSupplier;
    private final LicenseDao licenseDao;

    private final Supplier<Option<License>> cachedLicense;

    public Jira6xServiceDeskLicenseProviderImpl(final Jira6xServiceDeskPluginEncodedLicenseSupplier sdLicenseSupplier,
                                                final LicenseDao licenseDao) {
        this.sdLicenseSupplier = notNull("serviceDeskLicenseSupplier", sdLicenseSupplier);
        this.licenseDao = notNull("licenseDao", licenseDao);
        this.cachedLicense = Suppliers.memoize(this::retrieveLicense);
    }

    @Override
    public Option<License> serviceDeskLicense() {
        return cachedLicense.get();
    }

    private Option<License> retrieveLicense() {
        return sdLicenseSupplier.getCurrentOrUpgrade()
                .map(License::new)
                .orElse(this::get6xSdLicenseFromOldStoreSafely);
    }

    private Option<License> get6xSdLicenseFromOldStoreSafely() {
        try {
            return licenseDao.get6xLicense().filter(LicenseUtils::isServiceDeskLicense);
        } catch (MigrationFailedException e) {
            //We do not care if license is broken in old store here, as there might be valid one in multistore
            return Option.none();
        }
    }

    private Option<License> sdLicenseForCloud() {
        return licenseDao.getLicenses().license(ApplicationKeys.SERVICE_DESK)
                .filter(LicenseUtils::isServiceDeskLicense)
                .orElse(licenseDao::get6xLicense)
                .filter(LicenseUtils::isServiceDeskLicense);
    }
}
