package com.atlassian.jira.imports.project.ao.handler;

import com.atlassian.jira.plugin.AbstractJiraModuleDescriptor;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.plugin.module.ModuleFactory;

/**
 * Module descriptor for Project Import AO Import handlers
 *
 * @since v6.5
 */
public class AoImportHandlerModuleDescriptor extends AbstractJiraModuleDescriptor<PluggableImportAoEntityHandler> {
    public AoImportHandlerModuleDescriptor(final JiraAuthenticationContext authenticationContext, final ModuleFactory moduleFactory) {
        super(authenticationContext, moduleFactory);
    }
}
