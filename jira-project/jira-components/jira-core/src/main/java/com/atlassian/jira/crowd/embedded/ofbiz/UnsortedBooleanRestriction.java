package com.atlassian.jira.crowd.embedded.ofbiz;

import com.atlassian.crowd.embedded.api.SearchRestriction;
import com.atlassian.crowd.search.query.entity.restriction.BooleanRestriction;
import com.atlassian.crowd.search.query.entity.restriction.BooleanRestrictionImpl;

import java.util.Collection;

/**
 * Internal flagging restriction type that behaves like a normal BooleanRestriction but also indicates results may be
 * unsorted.  Can be used for increased performance for queries with large numbers of results.
 */
public class UnsortedBooleanRestriction extends BooleanRestrictionImpl implements UnsortedRestriction {
    public UnsortedBooleanRestriction(final BooleanLogic booleanLogic, final Collection<SearchRestriction> restrictions) {
        super(booleanLogic, restrictions);
    }

    public static UnsortedBooleanRestriction from(BooleanRestriction br) {
        return new UnsortedBooleanRestriction(br.getBooleanLogic(), br.getRestrictions());
    }
}

