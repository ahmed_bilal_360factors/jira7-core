package com.atlassian.jira.util.xml;

import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Wrapper around an XML DOM node object providing helper methods for simple parsing of XML files.
 *
 * @since v6.4.6
 */
public class XmlNode {
    private final Node node;

    /**
     * Create a new XmlNode wrapping the given DOM Node.
     *
     * @param node the DOM Node
     */
    public XmlNode(final Node node) {
        this.node = node;
    }

    public String getNodeName() {
        return node.getNodeName();
    }

    @Nonnull
    public List<XmlNode> getChildren() {
        NodeList childNodes = node.getChildNodes();
        final List<XmlNode> children = new ArrayList<XmlNode>(childNodes.getLength());
        for (int i = 0; i < childNodes.getLength(); i++) {
            children.add(new XmlNode(childNodes.item(i)));
        }
        return children;
    }

    /**
     * Return a list of all child nodes with the given name.
     *
     * @param name the node name
     * @return a list of all child nodes with the given name.
     */
    @Nonnull
    public List<XmlNode> getChildren(final String name) {
        final List<XmlNode> children = new LinkedList<XmlNode>();
        List<XmlNode> allChildren = getChildren();
        for (XmlNode child : allChildren) {
            if (child.getNodeName().equals(name))
                children.add(child);
        }
        return children;
    }

    /**
     * Return the first child with the given name.
     *
     * @param name the name
     * @return the first child with the given name (or null if none exist).
     */
    @Nullable
    public XmlNode getFirstChild(final String name) {
        List<XmlNode> children = getChildren();
        for (XmlNode child : children) {
            if (child.getNodeName().equals(name))
                return child;
        }
        return null;
    }

    /**
     * Return the attribute with the given name.
     *
     * @param name the attribute name
     * @return the attribute with the given name (or null if it doesn't exist).
     */
    @Nullable
    public String getAttribute(final String name) {
        NamedNodeMap attributeMap = node.getAttributes();
        if (attributeMap != null) {
            Node attribute = attributeMap.getNamedItem(name);
            if (attribute != null) {
                return attribute.getNodeValue();
            }
        }
        return null;
    }

    /**
     * Returns the text content of this node and its descendants.
     *
     * @return the text content of this node and its descendants.
     * @see org.w3c.dom.Node#getTextContent
     */
    public String getTextContent() {
        return node.getTextContent();
    }
}
