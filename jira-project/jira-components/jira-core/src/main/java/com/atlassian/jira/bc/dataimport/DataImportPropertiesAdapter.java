package com.atlassian.jira.bc.dataimport;

import java.util.Optional;

import static java.util.Optional.ofNullable;

/**
 * Wrapper around {@link com.atlassian.jira.bc.dataimport.OfbizImportHandler} so {@link DataImportOSPropertyValidator}
 * does not have a direct dependency on {@link com.atlassian.jira.bc.dataimport.OfbizImportHandler}.
 */
class DataImportPropertiesAdapter implements DataImportOSPropertyValidator.DataImportProperties {
    private final OfbizImportHandler ofbizImportHandler;

    DataImportPropertiesAdapter(OfbizImportHandler ofbizImportHandler) {
        this.ofbizImportHandler = ofbizImportHandler;
    }

    @Override
    public Optional<String> getStringProperty(String propertyKey) {
        return getId(propertyKey)
                .map(id -> ofbizImportHandler.getOsPropertyStringMap().get(id));
    }

    @Override
    public Optional<String> getTextProperty(String propertyKey) {
        return getId(propertyKey)
                .map(id -> ofbizImportHandler.getOsPropertyTextMap().get(id));
    }

    @Override
    public Optional<Long> getNumberProperty(String propertyKey) {
        return getId(propertyKey)
                .map(id -> ofbizImportHandler.getOsPropertyNumberMap().get(id))
                .map(Long::parseLong);
    }

    private Optional<String> getId(String propertyKey) {
        return ofNullable(ofbizImportHandler.getPropertyKeyToId().get(propertyKey));
    }
}
