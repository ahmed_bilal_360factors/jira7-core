package com.atlassian.jira.upgrade.tasks;

import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.upgrade.LegacyImmediateUpgradeTask;

import javax.annotation.Nullable;

/**
 * Turn soap on for new installations
 *
 * @since v4.4
 */
public class UpgradeTask_Build642 extends LegacyImmediateUpgradeTask {
    // RPC was removed with JIRA 7.0
    private static final String JIRA_OPTION_RPC_ALLOW = "jira.option.rpc.allow";

    private final ApplicationProperties applicationProperties;

    public UpgradeTask_Build642(ApplicationProperties applicationProperties) {
        super();
        this.applicationProperties = applicationProperties;
    }

    public int getBuildNumber() {
        return 642;
    }

    public String getShortDescription() {
        return "Turn AllowRPC On for new installations only.";
    }

    public void doUpgrade(boolean setupMode) throws Exception {
        boolean allowRpc;
        if (setupMode) {
            allowRpc = true;
        } else {
            allowRpc = applicationProperties.getOption(JIRA_OPTION_RPC_ALLOW);
        }
        applicationProperties.setOption(JIRA_OPTION_RPC_ALLOW, allowRpc);
    }


    @Nullable
    @Override
    public Integer dependsUpon() {
        return 640;
    }

}
