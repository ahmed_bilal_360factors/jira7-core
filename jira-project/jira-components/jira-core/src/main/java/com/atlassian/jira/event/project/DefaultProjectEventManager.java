package com.atlassian.jira.event.project;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.event.ProjectCreatedEvent;
import com.atlassian.jira.event.ProjectDeletedEvent;
import com.atlassian.jira.event.ProjectUpdatedCategoryChangedEvent;
import com.atlassian.jira.event.ProjectUpdatedDetailedChangesEvent;
import com.atlassian.jira.event.ProjectUpdatedEvent;
import com.atlassian.jira.event.ProjectUpdatedKeyChangedEvent;
import com.atlassian.jira.event.ProjectUpdatedTypeChangedEvent;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.web.action.RequestSourceType;
import org.apache.commons.lang.StringUtils;

public class DefaultProjectEventManager implements ProjectEventManager {
    private final EventPublisher eventPublisher;

    public DefaultProjectEventManager(final EventPublisher eventPublisher) {
        this.eventPublisher = eventPublisher;
    }

    @Override
    public void dispatchProjectUpdated(final ApplicationUser user, final Project newProject, final Project oldProject, RequestSourceType requestSourceType) {
        eventPublisher.publish(new ProjectUpdatedEvent(user, newProject, oldProject));
        eventPublisher.publish(new ProjectUpdatedDetailedChangesEvent(oldProject, newProject, requestSourceType));

        if (StringUtils.equals(oldProject.getKey(), newProject.getKey()) == false) {
            eventPublisher.publish(new ProjectUpdatedKeyChangedEvent(requestSourceType));
        }
        if (oldProject.getProjectTypeKey() != null && newProject.getProjectTypeKey() != null &&
                oldProject.getProjectTypeKey().equals(newProject.getProjectTypeKey()) == false) {
            eventPublisher.publish(new ProjectUpdatedTypeChangedEvent(oldProject.getProjectTypeKey(), newProject.getProjectTypeKey(), requestSourceType));
        }
    }

    @Override
    public void dispatchProjectCategoryChanged(RequestSourceType requestSourceType) {
        eventPublisher.publish(new ProjectUpdatedCategoryChangedEvent(requestSourceType));
    }

    @Override
    public void dispatchProjectCreated(final ApplicationUser user, final Project newProject) {
        eventPublisher.publish(new ProjectCreatedEvent(user, newProject));
    }

    @Override
    public void dispatchProjectDeleted(final ApplicationUser user, final Project oldProject) {
        eventPublisher.publish(new ProjectDeletedEvent(user, oldProject));
    }
}
