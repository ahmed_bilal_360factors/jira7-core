package com.atlassian.jira.web.servlet;

import com.atlassian.fugue.Unit;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.exception.AttachmentNotFoundException;
import com.atlassian.jira.exception.PermissionException;
import com.atlassian.jira.issue.attachment.Attachment;
import com.atlassian.jira.util.io.InputStreamConsumer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

import static com.atlassian.jira.util.BrowserUtils.USER_AGENT_HEADER;

public class ViewAttachmentServlet extends AbstractViewFileServlet {
    @Override
    protected int getContentLength(String attachmentPath) {
        final Attachment attachment = getAttachment(attachmentPath);
        return attachment.getFilesize().intValue();
    }

    @Override
    protected boolean supportsRangeRequests() {
        return true;
    }

    /**
     * @see AbstractViewFileServlet#getInputStream(String, com.atlassian.jira.util.io.InputStreamConsumer)
     */
    @Override
    protected void getInputStream(String attachmentQuery, InputStreamConsumer<Unit> consumer)
            throws PermissionException, IOException {
        Attachment attachment = getAttachment(attachmentQuery);

        if (!loggedInUserHasPermissionToViewAttachment(attachment)) {
            throw new PermissionException("You do not have permissions to view this issue");
        }

        ComponentAccessor.getAttachmentManager().streamAttachmentContent(attachment, consumer);
    }

    /**
     * Sets the content type, content length and "Content-Disposition" header
     * of the response based on the values of the attachement found.
     *
     * @param request  HTTP request
     * @param rangeResponse if we are doing a partial response this holds teh range we will return
     * @param response HTTP response
     * @throws AttachmentNotFoundException
     * @throws IOException
     */
    @Override
    protected void setResponseHeaders(HttpServletRequest request, Optional<RangeResponse> rangeResponse, HttpServletResponse response)
            throws AttachmentNotFoundException, IOException {
        final Attachment attachment = getAttachment(attachmentQuery(request));
        response.setContentType(attachment.getMimetype());

        if (rangeResponse.isPresent()) {
            // Status Code: 206 Partial Content
            response.setStatus(HttpServletResponse.SC_PARTIAL_CONTENT);
            // the Content Length for the partial response
            response.setContentLength(rangeResponse.get().calculateContentLength());
            // The range we are returning eg "Content-Range: bytes 21010-47021/47022"
            response.setHeader("Content-Range", rangeResponse.get().calculateContentRange());
        } else {
            // We will return the whole file
            response.setContentLength(attachment.getFilesize().intValue());
        }
        // JDEV-36274: Indicate that we support byte ranges
        response.setHeader("Accept-Ranges", "bytes");

        getMimeSniffingKit().setAttachmentResponseHeaders(attachment, request.getHeader(USER_AGENT_HEADER), response);
        HttpResponseHeaders.cachePrivatelyForAboutOneYear(response);
    }

    /**
     * Gets MimeSniffingKit from PICO container, you should not cache it in the servlet because servlets have a different
     * lifecycle than PICO.
     */
    private MimeSniffingKit getMimeSniffingKit() {
        return ComponentAccessor.getComponent(MimeSniffingKit.class);
    }
}
