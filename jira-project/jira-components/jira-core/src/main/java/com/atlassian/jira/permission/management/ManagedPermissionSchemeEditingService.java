package com.atlassian.jira.permission.management;

import com.atlassian.jira.exception.DataAccessException;
import com.atlassian.jira.permission.management.beans.PermissionsInputBean;
import com.atlassian.jira.scheme.Scheme;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.ErrorCollection;

public interface ManagedPermissionSchemeEditingService {

    /**
     * Validates whether the security types can be added to the permissions.
     *
     * @param loggedInUser current logged-in user
     * @param inputBean    the permission instructions under validation
     * @return an error collection filled with errors messages when validation fails else empty
     */
    ErrorCollection validateAddPermissions(final ApplicationUser loggedInUser, final PermissionsInputBean inputBean);

    /**
     * Adds a set of security types values to a set of permissions.
     * <p/>
     * This method does not perform any validation see {@link #validateAddPermissions(ApplicationUser, PermissionsInputBean)}
     *
     * @param schemeObject the scheme to add to
     * @param inputBean    the input bean of changes
     * @return true when new entry has been added else false
     * @throws DataAccessException unexpected error that occurred while adding entry to data store
     */
    boolean addNewSecurityTypes(final Scheme schemeObject, final PermissionsInputBean inputBean);
}
