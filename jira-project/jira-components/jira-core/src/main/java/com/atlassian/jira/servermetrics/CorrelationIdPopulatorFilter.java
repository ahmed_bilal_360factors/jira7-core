package com.atlassian.jira.servermetrics;

import com.atlassian.core.filters.AbstractHttpFilter;
import com.atlassian.webresource.api.assembler.PageBuilderService;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

public class CorrelationIdPopulatorFilter extends AbstractHttpFilter {
    private final ServletSafeComponentReference<PageBuilderService> pageBuilderService =
            ServletSafeComponentReference.build(PageBuilderService.class);

    @Override
    protected void doFilter(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws IOException, ServletException {

        final Optional<String> correlationID = TimingInformationToEvent.extractCorrelationID(httpServletRequest);
        correlationID.ifPresent(correlationId ->
                pageBuilderService.get()
                        .ifPresent(
                                builderService -> builderService
                                        .assembler()
                                        .data()
                                        .requireData("jira.request.correlation-id", correlationId)
                        ));

        filterChain.doFilter(httpServletRequest, httpServletResponse);
    }
}
