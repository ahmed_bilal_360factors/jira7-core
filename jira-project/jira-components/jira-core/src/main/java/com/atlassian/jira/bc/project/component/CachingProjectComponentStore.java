package com.atlassian.jira.bc.project.component;

import com.atlassian.beehive.ClusterLock;
import com.atlassian.beehive.ClusterLockService;
import com.atlassian.cache.Cache;
import com.atlassian.cache.CacheManager;
import com.atlassian.event.api.EventListener;
import com.atlassian.jira.EventComponent;
import com.atlassian.jira.bc.EntityNotFoundException;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.event.ClearCacheEvent;
import com.atlassian.jira.exception.DataAccessException;
import com.atlassian.jira.extension.Startable;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.util.dbc.Assertions;
import com.opensymphony.util.TextUtils;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Decorates an implementation of the project component delegateStore with caching. The actual delegateStore
 * implementation is delegated so this class is a Composite and also a Decorator.
 */
@EventComponent
public class CachingProjectComponentStore implements ProjectComponentStore, Startable {
    private static final String UPDATE_LOCK_NAME = CachingProjectComponentStore.class.getName() + ".updateLock";

    private ClusterLock updateLock;

    /**
     * component ID -> component. (Map&lt;Long, MutableProjectComponent&gt;)
     */
    private final Cache<Long, Optional<ProjectComponent>> componentIdToComponentMap;

    /**
     * project ID -> list of components. (Map&lt;Long, List&lt;MutableProjectComponent&gt;&gt;)
     */
    private final Cache<Long, Collection<ProjectComponent>> projectIdToComponentsMap;

    private final ProjectComponentStore delegateStore;
    private final ClusterLockService clusterLockService;

    public CachingProjectComponentStore(final ProjectComponentStore delegateStore,
                                        final ClusterLockService clusterLockService,
                                        final CacheManager cacheManager) {

        this.delegateStore = delegateStore;
        this.clusterLockService = clusterLockService;
        componentIdToComponentMap = cacheManager.getCache(CachingProjectComponentStore.class.getName() + ".componentIdToComponentMap",
                this::loadComponentById
        );

        projectIdToComponentsMap = cacheManager.getCache(CachingProjectComponentStore.class.getName() + ".projectIdToComponentsMap",
                this::loadComponentsByProjectId);
    }

    @SuppressWarnings("UnusedParameters")
    @EventListener
    public void onClearCache(final ClearCacheEvent event) {
        projectIdToComponentsMap.removeAll();
        componentIdToComponentMap.removeAll();
    }

    @Override
    public void start() {
        updateLock = clusterLockService.getLockForName(UPDATE_LOCK_NAME);
    }

    /**
     * Looks up the project component by the given ID and returns it. If not found, throws the EntityNotFoundException,
     * it never returns null.
     *
     * @param id project component ID
     * @return project component found by a given ID
     * @throws EntityNotFoundException if the component not found
     */
    public MutableProjectComponent find(final Long id) throws EntityNotFoundException {
        Assertions.notNull("id", id);

        return componentIdToComponentMap.get(id)
                .map(MutableProjectComponent::copy)
                .orElseThrow(() -> new EntityNotFoundException("The component with id '" + id + "' does not exist."));
    }

    @Override
    public ProjectComponent getProjectComponent(final Long projectComponentId) {
        Assertions.notNull("id", projectComponentId);

        final Optional<ProjectComponent> cacheObject = componentIdToComponentMap.get(projectComponentId);
        return cacheObject.orElse(null);
    }

    /**
     * Looks up all components that are related to the project with given ID.
     *
     * @param projectId project ID
     * @return a collection of ProjectComponent objects that are related to the project with given ID
     */
    public Collection<MutableProjectComponent> findAllForProject(final Long projectId) {
        final Collection<ProjectComponent> components = projectIdToComponentsMap.get(projectId);
        return MutableProjectComponent.copy(components);
    }

    /**
     * Looks up the component with the given name in the project with the given id.
     *
     * @param projectId     id of the project.
     * @param componentName name of the component.
     * @return the component.
     * @throws EntityNotFoundException if no such component can be found.
     */
    public MutableProjectComponent findByComponentName(final Long projectId, final String componentName)
            throws EntityNotFoundException {
        final Collection<MutableProjectComponent> components = findAllForProject(projectId);
        for (MutableProjectComponent c : components) {
            if (c.getName().equals(componentName)) {
                return c;
            }
        }
        throw new EntityNotFoundException("The project with id '" + projectId + "' is not associated with a component with the name '" + componentName + "'.");
    }

    /**
     * Finds one or more ProjectComponent with a given name.
     *
     * @param componentName the name of the component to find.
     * @return a Collection of Components with the given name.
     */
    @SuppressWarnings("unchecked")
    public Collection<MutableProjectComponent> findByComponentNameCaseInSensitive(final String componentName) {
        return findAll().stream()
                .filter(component -> component.getName().equalsIgnoreCase(componentName))
                .map(MutableProjectComponent::copy)
                .collect(Collectors.toList());
    }

    /**
     * Looks up the project ID for the given component ID. If project is not found, throws EntityNotFoundException.
     *
     * @param componentId component ID
     * @return project ID
     * @throws EntityNotFoundException if project not found for the given component ID
     */
    public Long findProjectIdForComponent(final Long componentId) throws EntityNotFoundException {
        return componentIdToComponentMap.get(componentId)
                .orElseThrow(() -> new EntityNotFoundException("The component with the id '" + componentId + "' does not exist."))
                .getProjectId();
    }

    /**
     * Checks whether component with specified name is stored.
     *
     * @param name component name, null will cause IllegalArgumentException
     * @return true if new name is stored
     * @throws IllegalArgumentException if name or projectId is null
     */
    public boolean containsName(final String name, final Long projectId) {
        if (projectId == null) {
            throw new IllegalArgumentException("Component project ID can not be null!");
        }
        if (name == null) {
            throw new IllegalArgumentException("Component name can not be null!");
        }
        final Collection<ProjectComponent> components = projectIdToComponentsMap.get(projectId);
        return containsNameIgnoreCase(components, name);
    }

    private static boolean containsNameIgnoreCase(final Collection<ProjectComponent> components, final String name) {
        return components != null
                && components.stream()
                .filter(component -> name.equalsIgnoreCase(component.getName()))
                .findAny()
                .isPresent();
    }

    /**
     * Persist the component. If component has no ID (null) it is inserted to the database and added to the cache,
     * otherwise an update operation is performed on both cache and database.
     *
     * @param component component to persist
     * @throws EntityNotFoundException                          in case of update if the component does not exist (maybe was deleted :-)
     * @throws com.atlassian.jira.exception.DataAccessException if cannot persist the component
     */
    public MutableProjectComponent store(final MutableProjectComponent component)
            throws EntityNotFoundException, DataAccessException {
        final MutableProjectComponent copy = MutableProjectComponent.copy(component);
        final MutableProjectComponent newComponent;
        if (copy.getId() == null) {
            newComponent = insert(copy);
        } else {
            newComponent = update(copy);
        }
        return newComponent;
    }

    /**
     * Removes the component from the persistent storage and a cache.
     *
     * @param componentId the id of the component to delete
     * @throws EntityNotFoundException if component does not exist (maybe was removed previously :-)
     */
    public void delete(final Long componentId) throws EntityNotFoundException {
        ProjectComponent component = null;
        if (componentId != null) {
            component = delegateStore.find(componentId);
        }
        delegateStore.delete(componentId);
        if (componentId != null) {
            componentIdToComponentMap.remove(componentId);
        }
        if (component != null) {
            projectIdToComponentsMap.remove(component.getProjectId());
        }
    }

    @Override
    public void deleteAllComponents(@Nonnull final Long projectId) {
        delegateStore.deleteAllComponents(projectId);
        componentIdToComponentMap.removeAll();
        projectIdToComponentsMap.remove(projectId);
    }

    /**
     * Retrieves all ProjectComponents that have the given user as their lead.
     * Not synchronised, because findAll() returns a private copy of all components.
     *
     * @param userKey key of the lead user
     * @return possibly empty Collection of ProjectComponents.
     */
    public Collection findComponentsBylead(final String userKey) {
        final Collection<ProjectComponent> leadComponents = new ArrayList<>();
        final Collection components = findAll();
        for (final Object component : components) {
            final MutableProjectComponent projectComponent = (MutableProjectComponent) component;
            if (projectComponent != null && TextUtils.stringSet(projectComponent.getLead()) && projectComponent.getLead().equals(userKey)) {
                leadComponents.add(projectComponent);
            }

        }
        return leadComponents;
    }

    /**
     * Retrieve all ProjectComponent objects stored.
     *
     * @return all ProjectComponent objects stored
     */
    public Collection<ProjectComponent> findAll() {
        // Can't inject the component manager because of circular dependency.
        final ProjectManager projectManager = ComponentAccessor.getProjectManager();
        final List<ProjectComponent> components = new ArrayList<>();
        final List<Project> allProjects = projectManager.getProjectObjects();
        for (Project project : allProjects) {
            components.addAll(projectIdToComponentsMap.get(project.getId()));
        }
        return components;
    }

    // helper methods

    /**
     * Add the specified component to the persistent storage and the cache. The returned component has ID set -
     * indicating that it has been persisted.
     *
     * @param component the component to stored
     * @return the updated component (with ID set)
     */
    private MutableProjectComponent insert(MutableProjectComponent component) {
        updateLock.lock();
        try {
            final String name = component.getName();
            if (containsName(name, component.getProjectId())) {
                throw createIllegalArgumentExceptionForName(name);
            }

            try {
                component = delegateStore.store(component);
                return component;
            } catch (EntityNotFoundException e) {
                // This exception should never be thrown - insertion should always complete successfully
                return null;
            } finally {
                componentIdToComponentMap.remove(component.getId());
                projectIdToComponentsMap.remove(component.getProjectId());
            }
        } finally {
            updateLock.unlock();
        }
    }

    /**
     * Retrieve the component with the ID of the component specified and update it with the new values of the given
     * component.
     *
     * @param component component with new values
     * @return updated component
     * @throws EntityNotFoundException                          if component with ID does not exist
     * @throws com.atlassian.jira.exception.DataAccessException if cannot persist the component
     * @throws IllegalArgumentException                         if duplicate name
     */
    private MutableProjectComponent update(final MutableProjectComponent component)
            throws EntityNotFoundException, DataAccessException {
        updateLock.lock();
        try {
            final MutableProjectComponent old = find(component.getId());
            if (!old.equalsName(component)) {
                if (containsName(component.getName(), component.getProjectId())) {
                    throw new IllegalArgumentException("New component name '" + component.getName() + "' is not unique!");
                }
            }
            delegateStore.store(component);
            updateCache(component);
            return component;
        } finally {
            updateLock.unlock();
        }
    }

    /**
     * Invalidate the caches for the given component/project.
     *
     * @param component component to be updated in cache
     */
    private void updateCache(final ProjectComponent component) {
        final Long id = component.getId();
        final Long projectId = component.getProjectId();
        componentIdToComponentMap.remove(id);
        projectIdToComponentsMap.remove(projectId);
    }

    private IllegalArgumentException createIllegalArgumentExceptionForName(final String name) {
        return new IllegalArgumentException("Component name = '" + name + "' is not unique");
    }

    /**
     * Sorts the List of {@link MutableProjectComponent} in projectIdToComponentsMap using the
     * ProjectComponentComparator.
     * When returning components for a project the components have to be sorted, due to compatiblity with the
     * deprecated ProjectManager.
     */
    private Collection<ProjectComponent> sortByComponentNames(final Collection<MutableProjectComponent> components) {
        final ArrayList<ProjectComponent> componentList = new ArrayList<>(components);
        Collections.sort(componentList, ProjectComponentComparator.INSTANCE);
        return componentList;
    }



    @Nonnull
    private Collection<ProjectComponent> loadComponentsByProjectId(final Long projectId) {
        return sortByComponentNames(CachingProjectComponentStore.this.delegateStore.findAllForProject(projectId));
    }

    @Nonnull
    private Optional<ProjectComponent> loadComponentById(final Long projectId) {
        try {
            return Optional.of(CachingProjectComponentStore.this.delegateStore.find(projectId));
        } catch (EntityNotFoundException e) {
            return Optional.empty();
        }
    }
}
