package com.atlassian.jira.service.services.analytics.start;

import com.atlassian.jira.cluster.ClusterManager;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.properties.JiraProperties;
import com.atlassian.jira.service.services.analytics.JiraAnalyticTask;
import com.atlassian.jira.util.BuildUtilsInfo;
import com.atlassian.jira.util.collect.MapBuilder;
import com.atlassian.jira.util.system.SystemInfoUtils;
import com.atlassian.plugin.parsers.SafeModeCommandLineArguments;
import com.atlassian.plugin.parsers.SafeModeCommandLineArgumentsFactory;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.hash.Hashing;

import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;

import static com.atlassian.jira.service.services.analytics.start.JiraStartAnalyticEvent.UNKNOWN;
import static java.lang.String.join;
import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;


/**
 * Tracks information of the system like : DB, Java Version, variables, etc
 */
public class JiraSystemAnalyticTask implements JiraAnalyticTask {

    private BuildUtilsInfo info;
    private SystemInfoUtils systemInfoUtils;
    private ClusterManager clusterManager;
    private JiraProperties jiraProperties;
    private SafeModeCommandLineArguments safeModeCommandLineArguments;

    public JiraSystemAnalyticTask() {
    }

    @VisibleForTesting
    JiraSystemAnalyticTask(final BuildUtilsInfo info, final SystemInfoUtils systemInfoUtils, final ClusterManager clusterManager, final JiraProperties jiraProperties, final SafeModeCommandLineArgumentsFactory safeModeCommandLineArgumentsFactory) {
        this.info = info;
        this.systemInfoUtils = systemInfoUtils;
        this.clusterManager = clusterManager;
        this.jiraProperties = jiraProperties;
        this.safeModeCommandLineArguments = safeModeCommandLineArgumentsFactory.get();
    }

    @Override
    public void init() {
        info = ComponentAccessor.getComponent(BuildUtilsInfo.class);
        systemInfoUtils = ComponentAccessor.getComponent(SystemInfoUtils.class);
        clusterManager = ComponentAccessor.getComponent(ClusterManager.class);
        jiraProperties = ComponentAccessor.getComponent(JiraProperties.class);
        safeModeCommandLineArguments = ComponentAccessor.getComponent(SafeModeCommandLineArgumentsFactory.class).get();
    }

    @Override
    public Map<String, Object> getAnalytics() {
        final MapBuilder<String, Object> builder = MapBuilder.newBuilder();

        // Jira Config
        final int[] versionNumbers = info.getVersionNumbers();
        builder.add("release.version", versionNumbers[0]);
        builder.add("major.version", versionNumbers[1]);
        builder.add("minor.version", versionNumbers[2]);
        builder.add("build.number", info.getCurrentBuildNumber());
        builder.add("build.date", String.valueOf(info.getCurrentBuildDate()));

        // DC License
        builder.add("license.dc", clusterManager.isClusterLicensed());

        // JVM
        builder.add("java.specification.version", jiraProperties.getProperty("java.specification.version"));
        builder.add("java.version", jiraProperties.getProperty("java.version"));
        builder.add("java.vendor", jiraProperties.getProperty("java.vendor"));

        // OS
        builder.add("os.name", jiraProperties.getProperty("os.name", UNKNOWN));
        builder.add("os.version", jiraProperties.getProperty("os.version", UNKNOWN));
        builder.add("os.arch", jiraProperties.getProperty("os.arch", UNKNOWN));
        builder.add("plugins.nonsystemaddonsdisabledonstartup", safeModeCommandLineArguments.isSafeMode());
        builder.add("plugins.specificaddonsweredisabledonstartup", safeModeCommandLineArguments.getDisabledPlugins().isPresent());

        List<String> keys = safeModeCommandLineArguments.getDisabledPlugins().orElse(emptyList())
                .stream()
                .map(JiraSystemAnalyticTask::hash)
                .collect(toList());
        builder.add("plugins.addonsdisabledonstartup",  join(":", keys));

        // DB
        builder.add("database.name", systemInfoUtils.getDatabaseType());

        try {
            final SystemInfoUtils.DatabaseMetaData databaseMetaData = systemInfoUtils.getDatabaseMetaData();
            builder.add("database.version", databaseMetaData.getDatabaseProductVersion());
            builder.add("database.driver.version", databaseMetaData.getDriverVersion());
        } catch (Exception e) {
            // Something went wrong, we don't care
            builder.add("database.version", UNKNOWN);
            builder.add("database.driver.version", UNKNOWN);
        }

        return builder.toMap();
    }

    /**
     * Decimal numbers can get through the analytics filter unscathed, so the hash is converted to a long.
     * @param value analytics value to be hashed
     * @return the first 8 bytes of the sha256 hash as a decimal value
     */
    private static String hash(String value) {
        return Long.toString(Hashing.sha256()
                .hashString(value, StandardCharsets.UTF_8).asLong());
    }

    @Override
    public boolean isReportingDataShape() {
        return false;
    }

}
