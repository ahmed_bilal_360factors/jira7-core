package com.atlassian.jira.permission.management.beans;

import com.google.common.base.Objects;
import com.google.common.collect.ImmutableList;
import org.codehaus.jackson.annotate.JsonAutoDetect;

import java.util.List;

@JsonAutoDetect
public class PermissionsInputBean {
    private List<String> permissionKeys;
    private List<GrantToPermissionInputBean> grants;

    public PermissionsInputBean() {
    }

    public PermissionsInputBean(Iterable<String> permissionKeys, Iterable<GrantToPermissionInputBean> grants) {
        this.permissionKeys = permissionKeys != null ? ImmutableList.copyOf(permissionKeys) : null;
        this.grants = grants != null ? ImmutableList.copyOf(grants) : null;
    }

    public List<String> getPermissionKeys() {
        return permissionKeys;
    }

    public void setPermissionKeys(List<String> permissionKeys) {
        this.permissionKeys = permissionKeys != null ? ImmutableList.copyOf(permissionKeys) : null;
    }

    public List<GrantToPermissionInputBean> getGrants() {
        return grants;
    }

    public void setGrants(List<GrantToPermissionInputBean> grants) {
        this.grants = grants != null ? ImmutableList.copyOf(grants) : null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PermissionsInputBean that = (PermissionsInputBean) o;

        return Objects.equal(this.permissionKeys, that.permissionKeys) &&
                Objects.equal(this.grants, that.grants);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(permissionKeys, grants);
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("permissionKeys", permissionKeys)
                .add("grants", grants)
                .toString();
    }
}
