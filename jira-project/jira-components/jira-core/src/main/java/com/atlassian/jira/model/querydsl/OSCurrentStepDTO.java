package com.atlassian.jira.model.querydsl;

import com.atlassian.jira.ofbiz.FieldMap;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import org.ofbiz.core.entity.GenericValue;

import javax.annotation.Generated;

/**
 * Data Transfer Object for the OSCurrentStep entity.
 * <p>
 * Generated by the JIRA Querydsl code generation tool - https://bitbucket.org/atlassian/jira-querydsl-codegen/
 * </p>
 *
 * @see QOSCurrentStep
 */
@Generated("com.atlassian.jira.tool.querydsl.CodeGenerator")
public class OSCurrentStepDTO implements DTO {
    private final Long id;
    private final Long entryId;
    private final Integer stepId;
    private final Integer actionId;
    private final String owner;
    private final java.sql.Timestamp startDate;
    private final java.sql.Timestamp dueDate;
    private final java.sql.Timestamp finishDate;
    private final String status;
    private final String caller;

    public Long getId() {
        return id;
    }

    public Long getEntryId() {
        return entryId;
    }

    public Integer getStepId() {
        return stepId;
    }

    public Integer getActionId() {
        return actionId;
    }

    public String getOwner() {
        return owner;
    }

    public java.sql.Timestamp getStartDate() {
        return startDate;
    }

    public java.sql.Timestamp getDueDate() {
        return dueDate;
    }

    public java.sql.Timestamp getFinishDate() {
        return finishDate;
    }

    public String getStatus() {
        return status;
    }

    public String getCaller() {
        return caller;
    }

    public OSCurrentStepDTO(Long id, Long entryId, Integer stepId, Integer actionId, String owner, java.sql.Timestamp startDate, java.sql.Timestamp dueDate, java.sql.Timestamp finishDate, String status, String caller) {
        this.id = id;
        this.entryId = entryId;
        this.stepId = stepId;
        this.actionId = actionId;
        this.owner = owner;
        this.startDate = startDate;
        this.dueDate = dueDate;
        this.finishDate = finishDate;
        this.status = status;
        this.caller = caller;
    }

    /**
     * Creates a GenericValue object from the values in this Data Transfer Object.
     * <p>
     * This can be useful when QueryDsl code needs to interact with legacy OfBiz code.
     * </p>
     *
     * @param ofBizDelegator OfBizDelegator will have makeValue() called on it.
     * @return a GenericValue object constructed from the values in this Data Transfer Object.
     */
    public GenericValue toGenericValue(final OfBizDelegator ofBizDelegator) {
        return ofBizDelegator.makeValue("OSCurrentStep", new FieldMap()
                .add("id", id)
                .add("entryId", entryId)
                .add("stepId", stepId)
                .add("actionId", actionId)
                .add("owner", owner)
                .add("startDate", startDate)
                .add("dueDate", dueDate)
                .add("finishDate", finishDate)
                .add("status", status)
                .add("caller", caller)
        );
    }

    /**
     * Constructs a new instance of this Data Transfer object from the values in the given GenericValue.
     * <p>
     * This can be useful when QueryDsl code needs to interact with legacy OfBiz code.
     * </p>
     *
     * @param gv the GenericValue
     * @return a new instance of this Data Transfer object with the values in the given GenericValue.
     */
    public static OSCurrentStepDTO fromGenericValue(GenericValue gv) {
        return new OSCurrentStepDTO(
                gv.getLong("id"),
                gv.getLong("entryId"),
                gv.getInteger("stepId"),
                gv.getInteger("actionId"),
                gv.getString("owner"),
                gv.getTimestamp("startDate"),
                gv.getTimestamp("dueDate"),
                gv.getTimestamp("finishDate"),
                gv.getString("status"),
                gv.getString("caller")
        );
    }

    public static Builder builder() {
        return new Builder();
    }

    public static Builder builder(OSCurrentStepDTO oSCurrentStepDTO) {
        return new Builder(oSCurrentStepDTO);
    }

    public static class Builder {
        private Long id;
        private Long entryId;
        private Integer stepId;
        private Integer actionId;
        private String owner;
        private java.sql.Timestamp startDate;
        private java.sql.Timestamp dueDate;
        private java.sql.Timestamp finishDate;
        private String status;
        private String caller;

        public Builder() {
        }

        public Builder(OSCurrentStepDTO oSCurrentStepDTO) {
            this.id = oSCurrentStepDTO.id;
            this.entryId = oSCurrentStepDTO.entryId;
            this.stepId = oSCurrentStepDTO.stepId;
            this.actionId = oSCurrentStepDTO.actionId;
            this.owner = oSCurrentStepDTO.owner;
            this.startDate = oSCurrentStepDTO.startDate;
            this.dueDate = oSCurrentStepDTO.dueDate;
            this.finishDate = oSCurrentStepDTO.finishDate;
            this.status = oSCurrentStepDTO.status;
            this.caller = oSCurrentStepDTO.caller;
        }

        public OSCurrentStepDTO build() {
            return new OSCurrentStepDTO(id, entryId, stepId, actionId, owner, startDate, dueDate, finishDate, status, caller);
        }

        public Builder id(Long id) {
            this.id = id;
            return this;
        }
        public Builder entryId(Long entryId) {
            this.entryId = entryId;
            return this;
        }
        public Builder stepId(Integer stepId) {
            this.stepId = stepId;
            return this;
        }
        public Builder actionId(Integer actionId) {
            this.actionId = actionId;
            return this;
        }
        public Builder owner(String owner) {
            this.owner = owner;
            return this;
        }
        public Builder startDate(java.sql.Timestamp startDate) {
            this.startDate = startDate;
            return this;
        }
        public Builder dueDate(java.sql.Timestamp dueDate) {
            this.dueDate = dueDate;
            return this;
        }
        public Builder finishDate(java.sql.Timestamp finishDate) {
            this.finishDate = finishDate;
            return this;
        }
        public Builder status(String status) {
            this.status = status;
            return this;
        }
        public Builder caller(String caller) {
            this.caller = caller;
            return this;
        }
    }
}