package com.atlassian.jira.issue.views.csv;

import java.io.IOException;
import java.io.Writer;
import java.util.List;

import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.export.customfield.CsvIssueExporter;
import com.atlassian.jira.issue.fields.Field;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.issue.views.util.SearchRequestViewUtils;
import com.atlassian.jira.issue.views.util.WordViewUtils;
import com.atlassian.jira.plugin.searchrequestview.AbstractSearchRequestView;
import com.atlassian.jira.plugin.searchrequestview.RequestHeaders;
import com.atlassian.jira.plugin.searchrequestview.SearchRequestParams;
import com.atlassian.jira.util.JiraUrlCodec;
import com.atlassian.jira.util.RuntimeIOException;

/**
 * This is the request view for dealing with the exportation of issues into a Csv format.
 *
 * @since 7.2.0
 */
public abstract class AbstractCsvSearchRequestView extends AbstractSearchRequestView {
    private final ApplicationProperties applicationProperties;
    protected final CsvIssueExporter csvIssueExporter;

    public AbstractCsvSearchRequestView(
            final ApplicationProperties applicationProperties,
            final CsvIssueExporter csvIssueExporter) {
        this.applicationProperties = applicationProperties;
        this.csvIssueExporter = csvIssueExporter;
    }

    @Override
    public void writeHeaders(final SearchRequest searchRequest, final RequestHeaders requestHeaders, final SearchRequestParams searchRequestParams) {
        WordViewUtils.writeGenericNoCacheHeaders(requestHeaders);
        WordViewUtils.writeEncodedAttachmentFilenameHeader(
                requestHeaders,
                JiraUrlCodec.encode(SearchRequestViewUtils.getTitle(searchRequest, applicationProperties.getDefaultBackedString(APKeys.JIRA_TITLE)), true) + ".csv",
                searchRequestParams.getUserAgent(),
                applicationProperties.getEncoding());
    }

    @Override
    public void writeSearchResults(final SearchRequest searchRequest, final SearchRequestParams searchRequestParams, final Writer writer)
            throws SearchException {
        try {

            final List<Field> fields = getFieldsToBeExported(searchRequest);

            csvIssueExporter.export(writer, searchRequest, searchRequestParams, fields);
        } catch (IOException e) {
            throw new RuntimeIOException(e);
        }
    }

    /**
     * Get the fields that should be exported for this Csv export.
     * @param searchRequest to do search on
     * @return fields that will be exported for each issue.
     */
    protected abstract List<Field> getFieldsToBeExported(SearchRequest searchRequest);
}
