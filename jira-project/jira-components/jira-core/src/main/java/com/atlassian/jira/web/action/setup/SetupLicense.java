package com.atlassian.jira.web.action.setup;

import com.atlassian.jira.bc.license.JiraLicenseService;
import com.atlassian.jira.bc.license.JiraLicenseUpdaterService;
import com.atlassian.jira.config.properties.JiraProperties;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.FileFactory;
import com.atlassian.jira.util.JiraProductInformation;
import com.atlassian.jira.util.system.JiraSystemRestarter;
import webwork.action.ActionContext;

import javax.servlet.http.HttpServletResponse;

public class SetupLicense extends AbstractSetupAction {
    private static final String LICENSE_VALIDATION_RESULTS = "json";

    private final JiraLicenseUpdaterService licenseService;
    private final JiraSystemRestarter jiraSystemRestarter;
    private final MyAtlassianComRedirect myAtlassianComRedirect;

    /**
     * Properties for when finally submitting.
     */
    private String licenseString;
    private JiraLicenseService.ValidationResult validationResult;

    /**
     * The license validation response.
     */
    private String licenseValidationJson;

    /**
     * The license to validate.
     */
    private String licenseToValidate;

    /**
     * Constructor.
     */
    public SetupLicense(final FileFactory fileFactory, final JiraLicenseUpdaterService licenseService,
                        final JiraSystemRestarter jiraSystemRestarter, final JiraProperties jiraProperties,
                        final JiraProductInformation jiraProductInformation,
                        final MyAtlassianComRedirect myAtlassianComRedirect) {
        super(fileFactory, jiraProperties, jiraProductInformation);
        this.licenseService = licenseService;
        this.jiraSystemRestarter = jiraSystemRestarter;
        this.myAtlassianComRedirect = myAtlassianComRedirect;
    }

    /**
     * A Simple action to allow ajax calls to verify a license, returing a json representation.
     *
     * @return The json view of the validation.
     */
    public String doValidateLicense() {
        final JiraLicenseService.ValidationResult validationResults = licenseService.validateApplicationLicense(this, licenseToValidate);
        licenseValidationJson = new LicenseValidationResults(validationResults).toJson();
        if (validationResults.getErrorCollection().hasAnyErrors()) {
            ActionContext.getContext().getResponse().setStatus(HttpServletResponse.SC_FORBIDDEN);
        }
        return LICENSE_VALIDATION_RESULTS;
    }

    public void doValidation() {
        validationResult = licenseService.validateApplicationLicense(this, licenseString);
        final ErrorCollection errorCollection = validationResult.getErrorCollection();
        if (errorCollection.hasAnyErrors()) {
            addErrorCollection(errorCollection);
        }
        super.doValidation();
    }

    protected String doExecute() {
        if (setupAlready()) {
            return SETUP_ALREADY;
        }

        final boolean licensePreviouslySet = licenseService.isLicenseSet();

        licenseService.setLicense(validationResult);

        // Avoid restarting if license previously set,
        // as some plugins that are brought up in the full container may contain servlet filters that break for
        // mid-request reload
        // Example: JWDSendRedirectFilter
        if (!licensePreviouslySet) {
            jiraSystemRestarter.ariseSirJIRA();
        }

        return getRedirect("SetupAdminAccount!default.jspa");
    }

    public String getMacRedirect() {
        return myAtlassianComRedirect.newRedirectWithCallbackTo("/secure/SetupLicense!default.jspa")
                .buildWithPostReturnParameter("setupLicenseKey");
    }

    public void setLicenseToValidate(final String license) {
        this.licenseToValidate = license.replace(' ', '+');
    }

    public String getLicenseToValidate() {
        return licenseToValidate;
    }

    public String getLicenseValidationResults() {
        return licenseValidationJson;
    }

    public void setSetupLicenseKey(final String licenseString) {
        this.licenseString = licenseString;
    }

    public String getLicenseString() {
        return licenseString;
    }
}
