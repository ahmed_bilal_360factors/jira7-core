package com.atlassian.jira.plugin.entity;

import com.atlassian.jira.entity.property.EntityPropertyConditionHelper;
import com.atlassian.jira.plugin.AbstractJiraModuleDescriptor;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.plugin.module.ModuleFactory;

/**
 * Handles <entity-property-condition-helper /> modules. These modules are only supposed to provide a class, which implements
 * {@link EntityPropertyConditionHelper}.
 *
 * @since v7.1
 */
public class EntityPropertyConditionHelperModuleDescriptor extends AbstractJiraModuleDescriptor<EntityPropertyConditionHelper> {

    public EntityPropertyConditionHelperModuleDescriptor(JiraAuthenticationContext authenticationContext, ModuleFactory moduleFactory) {
        super(authenticationContext, moduleFactory);
    }

}
