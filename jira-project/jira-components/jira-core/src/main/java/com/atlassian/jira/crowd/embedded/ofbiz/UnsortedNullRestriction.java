package com.atlassian.jira.crowd.embedded.ofbiz;

import com.atlassian.crowd.search.query.entity.restriction.NullRestriction;

public final class UnsortedNullRestriction implements NullRestriction, UnsortedRestriction {
    public static final UnsortedNullRestriction INSTANCE = new UnsortedNullRestriction();

    private UnsortedNullRestriction() {
    }

    //These are the same as NullRestrictionImpl's - to Crowd, apart from the unsorted hint, this restriction is the
    //same as any other NullRestriction

    @Override
    public boolean equals(final Object o) {
        return o instanceof NullRestriction;
    }

    @Override
    public int hashCode() {
        return 1;
    }
}