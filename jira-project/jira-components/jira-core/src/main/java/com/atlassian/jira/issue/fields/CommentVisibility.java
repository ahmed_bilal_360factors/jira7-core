package com.atlassian.jira.issue.fields;

import java.util.Map;

/**
 * Represents the visibility of the comment to roles or groups of users.
 */
public class CommentVisibility {
    private String commentLevel;
    private static final String ROLE_PREFIX = "role:";
    private static final String GROUP_PREFIX = "group:";
    private static final String VISIBLE_BY_ALL = "";

    /**
     * Gets a commentLevel string out of a map and then will tell you if the
     * provided param is a role or group level visibility param.
     */
    public CommentVisibility(Map params, String paramName) {
        String[] value;
        value = (String[]) params.get(paramName);
        if (value != null && value.length > 0) {
            commentLevel = value[0];
            // transform empty string to null
            if (commentLevel != null && VISIBLE_BY_ALL.equals(commentLevel.trim())) {
                commentLevel = null;
            }
        }
    }

    /**
     * Digests the passed in commentLevel string, will look something like
     * 'role:ProjectRole' or 'group:jira-users'.
     *
     * @param commentLevel
     */
    public CommentVisibility(String commentLevel) {
        this.commentLevel = commentLevel;
    }

    public Boolean isVisibleByAll() {
        return commentLevel == null;
    }

    public Boolean isPresent() {
        return isVisibleByAll() || getRoleLevel() != null || getGroupLevel() != null;
    }

    /**
     * Gets the role level from the commentLevel if it is of type role, null
     * otherwise.
     *
     * @return role level
     */
    public String getRoleLevel() {
        if (commentLevel != null && commentLevel.startsWith(ROLE_PREFIX)) {
            return commentLevel.substring(ROLE_PREFIX.length());
        }
        return null;
    }

    /**
     * Gets the group level from the commentLevel if it is of type group, null
     * otherwise.
     *
     * @return group level
     */
    public String getGroupLevel() {
        if (commentLevel != null && commentLevel.startsWith(GROUP_PREFIX)) {
            return commentLevel.substring(GROUP_PREFIX.length());
        }
        return null;
    }

    public static String getCommentLevelFromLevels(String groupLevel, Long roleLevelId) {
        if (groupLevel != null) {
            return GROUP_PREFIX + groupLevel;
        } else if (roleLevelId != null) {
            return ROLE_PREFIX + roleLevelId;
        }
        return null;
    }

    public static String getRoleLevelWithPrefix(String roleLevel) {
        return ROLE_PREFIX + roleLevel;
    }
}
