package com.atlassian.jira.model.querydsl;

import com.atlassian.jira.ofbiz.FieldMap;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import org.ofbiz.core.entity.GenericValue;

import javax.annotation.Generated;

/**
 * Data Transfer Object for the Label entity.
 * <p>
 * Generated by the JIRA Querydsl code generation tool - https://bitbucket.org/atlassian/jira-querydsl-codegen/
 * </p>
 *
 * @see QLabel
 */
@Generated("com.atlassian.jira.tool.querydsl.CodeGenerator")
public class LabelDTO implements DTO {
    private final Long id;
    private final Long fieldid;
    private final Long issue;
    private final String label;

    public Long getId() {
        return id;
    }

    public Long getFieldid() {
        return fieldid;
    }

    public Long getIssue() {
        return issue;
    }

    public String getLabel() {
        return label;
    }

    public LabelDTO(Long id, Long fieldid, Long issue, String label) {
        this.id = id;
        this.fieldid = fieldid;
        this.issue = issue;
        this.label = label;
    }

    /**
     * Creates a GenericValue object from the values in this Data Transfer Object.
     * <p>
     * This can be useful when QueryDsl code needs to interact with legacy OfBiz code.
     * </p>
     *
     * @param ofBizDelegator OfBizDelegator will have makeValue() called on it.
     * @return a GenericValue object constructed from the values in this Data Transfer Object.
     */
    public GenericValue toGenericValue(final OfBizDelegator ofBizDelegator) {
        return ofBizDelegator.makeValue("Label", new FieldMap()
                .add("id", id)
                .add("fieldid", fieldid)
                .add("issue", issue)
                .add("label", label)
        );
    }

    /**
     * Constructs a new instance of this Data Transfer object from the values in the given GenericValue.
     * <p>
     * This can be useful when QueryDsl code needs to interact with legacy OfBiz code.
     * </p>
     *
     * @param gv the GenericValue
     * @return a new instance of this Data Transfer object with the values in the given GenericValue.
     */
    public static LabelDTO fromGenericValue(GenericValue gv) {
        return new LabelDTO(
                gv.getLong("id"),
                gv.getLong("fieldid"),
                gv.getLong("issue"),
                gv.getString("label")
        );
    }

    public static Builder builder() {
        return new Builder();
    }

    public static Builder builder(LabelDTO labelDTO) {
        return new Builder(labelDTO);
    }

    public static class Builder {
        private Long id;
        private Long fieldid;
        private Long issue;
        private String label;

        public Builder() {
        }

        public Builder(LabelDTO labelDTO) {
            this.id = labelDTO.id;
            this.fieldid = labelDTO.fieldid;
            this.issue = labelDTO.issue;
            this.label = labelDTO.label;
        }

        public LabelDTO build() {
            return new LabelDTO(id, fieldid, issue, label);
        }

        public Builder id(Long id) {
            this.id = id;
            return this;
        }
        public Builder fieldid(Long fieldid) {
            this.fieldid = fieldid;
            return this;
        }
        public Builder issue(Long issue) {
            this.issue = issue;
            return this;
        }
        public Builder label(String label) {
            this.label = label;
            return this;
        }
    }
}