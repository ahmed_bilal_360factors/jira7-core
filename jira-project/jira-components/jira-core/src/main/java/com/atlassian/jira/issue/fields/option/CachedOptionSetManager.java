package com.atlassian.jira.issue.fields.option;

import com.atlassian.cache.Cache;
import com.atlassian.cache.CacheManager;
import com.atlassian.event.api.EventListener;
import com.atlassian.jira.EventComponent;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.event.ClearCacheEvent;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.issue.fields.config.manager.FieldConfigManager;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.Collection;

import static java.util.Objects.requireNonNull;

@EventComponent
@ParametersAreNonnullByDefault
public class CachedOptionSetManager implements OptionSetManager {

    private final OptionSetManager delegate;
    private final Cache<Long, OptionSet> cache;

    public CachedOptionSetManager(final OptionSetManagerImpl delegate,
                                  final CacheManager cacheManager) {

        this.delegate = delegate;
        this.cache = cacheManager.getCache(CachedOptionSetManager.class.getName() + ".cache",
                this::loadOptionsForConfig);
    }

    @EventListener
    @SuppressWarnings("unused")
    public void onClearCache(final ClearCacheEvent event) {
        cache.removeAll();
    }

    @Nonnull
    @Override
    public OptionSet getOptionsForConfig(FieldConfig config) {
        requireNonNull(config, "config");
        return cache.get(config.getId());
    }

    @Nonnull
    @Override
    public OptionSet createOptionSet(FieldConfig config, Collection<String> optionIds) {
        requireNonNull(config, "config");
        try {
            return delegate.updateOptionSet(config, optionIds);
        } finally {
            cache.remove(config.getId());
        }
    }

    @Nonnull
    @Override
    public OptionSet updateOptionSet(FieldConfig config, Collection<String> optionIds) {
        requireNonNull(config, "config");
        try {
            return delegate.updateOptionSet(config, optionIds);
        } finally {
            cache.remove(config.getId());
        }
    }

    @Override
    public OptionSet addOptionToOptionSet(FieldConfig config, String optionId) {
        requireNonNull(config, "config");
        requireNonNull(optionId, "optionId");
        try {
            return delegate.addOptionToOptionSet(config, optionId);
        } finally {
            cache.remove(config.getId());
        }
    }

    @Override
    public OptionSet removeOptionFromOptionSet(FieldConfig config, String optionId) {
        requireNonNull(config, "config");
        requireNonNull(optionId, "optionId");
        try {
            return delegate.removeOptionFromOptionSet(config, optionId);
        } finally {
            cache.remove(config.getId());
        }
    }


    @Override
    public void removeOptionSet(FieldConfig config) {
        requireNonNull(config, "config");
        try {
            delegate.removeOptionSet(config);
        } finally {
            cache.remove(config.getId());
        }
    }

    @Nonnull
    private OptionSet loadOptionsForConfig(final Long fieldConfigId) {
        // Injecting this component causes a circular dependency
        final FieldConfigManager fieldConfigManager = ComponentAccessor.getComponent(FieldConfigManager.class);
        return delegate.getOptionsForConfig(fieldConfigManager.getFieldConfig(fieldConfigId));
    }
}
