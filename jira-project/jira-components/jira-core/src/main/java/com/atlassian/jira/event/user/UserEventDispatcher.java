package com.atlassian.jira.event.user;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.collect.MapBuilder;

import java.util.HashMap;
import java.util.Map;

public class UserEventDispatcher {
    public static void dispatchEvent(int type, ApplicationUser user) {
        dispatchEvent(type, user, new HashMap<String, Object>());
    }

    public static void dispatchEvent(int type, ApplicationUser user, Map<String, Object> params) {
        final MapBuilder<String, Object> mapBuilder = MapBuilder.newBuilder(params);
        mapBuilder.add("baseurl", ComponentAccessor.getApplicationProperties().getString(APKeys.JIRA_BASEURL));
        UserEvent event = new UserEvent(mapBuilder.toMap(), user, type);

        ComponentAccessor.getComponentOfType(EventPublisher.class).publish(event);
    }
}
