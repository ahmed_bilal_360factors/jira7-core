package com.atlassian.jira.imports.project.validation;

import com.atlassian.jira.imports.project.core.BackupProject;
import com.atlassian.jira.issue.fields.TextFieldCharacterLengthValidator;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.MessageSet;
import com.atlassian.jira.util.MessageSetImpl;

/**
 * Checks if the largest (in terms of length) system field found in the project does not exceed the jira character
 * limit.
 */
public class SystemFieldsMaxTextLengthValidator {
    private final TextFieldCharacterLengthValidator textFieldCharacterLengthValidator;

    public SystemFieldsMaxTextLengthValidator(final TextFieldCharacterLengthValidator textFieldCharacterLengthValidator) {
        this.textFieldCharacterLengthValidator = textFieldCharacterLengthValidator;
    }

    public MessageSet validate(final BackupProject backupProject, final I18nHelper i18nHelper) {
        final MessageSet messageSet = new MessageSetImpl();
        final int maxTextFieldLength = backupProject.getSystemFieldMaxTextLength();

        if (textFieldCharacterLengthValidator.isTextTooLong(maxTextFieldLength)) {
            messageSet.addErrorMessage(i18nHelper.getText("admin.errors.project.import.field.text.too.long",
                    String.valueOf(maxTextFieldLength),
                    String.valueOf(textFieldCharacterLengthValidator.getMaximumNumberOfCharacters())));
        }

        return messageSet;
    }
}
