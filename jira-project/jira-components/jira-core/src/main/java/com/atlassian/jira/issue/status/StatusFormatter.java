package com.atlassian.jira.issue.status;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Map;

/**
 * Status formatter that caches rendered statuses in request bound cache for better performance.
 *
 * @since v6.5
 */
public interface StatusFormatter {
    @Nonnull
    String getColumnViewHtml(@Nullable final Status status, @Nonnull final Map<String, Object> displayParams);

    @Nonnull
    String getColumnViewHtml(@Nullable final SimpleStatus status, @Nonnull final Map<String, Object> displayParams);
}
