package com.atlassian.jira.appconsistency.integrity.integritycheck;

public interface EntityIntegrityCheck extends IntegrityCheck {
    public String getEntityName();
}
