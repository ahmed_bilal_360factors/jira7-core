package com.atlassian.jira.issue.attachment;

import com.atlassian.jira.issue.Issue;
import com.atlassian.util.concurrent.Promise;
import com.atlassian.util.concurrent.Promises;
import org.apache.commons.io.FileUtils;

import javax.annotation.Nonnull;
import java.io.File;
import java.io.IOException;

public abstract class WithDirectoryAccessorThumbnailAccessor implements ThumbnailAccessor {
    private final AttachmentDirectoryAccessor attachmentDirectoryAccessor;

    public WithDirectoryAccessorThumbnailAccessor(final AttachmentDirectoryAccessor ada) {
        attachmentDirectoryAccessor = ada;
    }

    @Override
    @Nonnull
    public final File getThumbnailDirectory(final @Nonnull Issue issue) {
        return attachmentDirectoryAccessor.getThumbnailDirectory(issue);
    }

    @Override
    @Nonnull
    public final File getThumbnailDirectory(final @Nonnull Issue issue, final boolean createDirectory) {
        return attachmentDirectoryAccessor.getThumbnailDirectory(issue, createDirectory);
    }

    @Override
    public Promise<Void> deleteThumbnailDirectory(@Nonnull final Issue issue) {
        final File thumbnailDirectory = attachmentDirectoryAccessor.getThumbnailDirectory(issue, false);
        try {
            FileUtils.deleteDirectory(thumbnailDirectory);
            return Promises.promise(null);
        } catch (IOException e) {
            return Promises.rejected(new AttachmentCleanupException(e));
        }

    }

    protected final AttachmentDirectoryAccessor getAttachmentDirectoryAccessor() {
        return attachmentDirectoryAccessor;
    }
}