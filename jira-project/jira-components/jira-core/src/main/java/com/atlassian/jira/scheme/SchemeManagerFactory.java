package com.atlassian.jira.scheme;

/**
 * Given a string which represents the SchemeManager type this will return you an instance of the SchemeManager you
 * are looking for.
 */
public interface SchemeManagerFactory {
    static final String PERMISSION_SCHEME_MANAGER = "PermissionScheme";
    static final String NOTIFICATION_SCHEME_MANAGER = "NotificationScheme";
    static final String WORKFLOW_SCHEME_MANAGER = "WorkflowScheme";
    static final String ISSUE_SECURITY_SCHEME_MANAGER = "IssueSecurityScheme";

    SchemeManager getSchemeManager(String managerType);

    Iterable<SchemeManager> getAllSchemeManagers();
}
