package com.atlassian.jira.issue.customfields.manager;

import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.config.properties.JiraProperties;
import com.atlassian.jira.entity.EntityUtils;
import com.atlassian.jira.exception.DataAccessException;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.util.map.NotNullHashMap;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.basic.AbstractSingleValueConverter;
import com.thoughtworks.xstream.security.AnyTypePermission;
import com.thoughtworks.xstream.security.NoTypePermission;
import com.thoughtworks.xstream.security.NullPermission;
import com.thoughtworks.xstream.security.PrimitiveTypePermission;
import org.apache.commons.lang.StringUtils;
import org.ofbiz.core.entity.GenericEntityException;
import org.ofbiz.core.entity.GenericValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.atlassian.jira.util.dbc.Assertions.notNull;

public class DefaultGenericConfigManager implements GenericConfigManager {

    private static final Logger log = LoggerFactory.getLogger(DefaultGenericConfigManager.class);

    private final OfBizDelegator delegator;

    private final XStreamBuilder streamBuilder;

    public DefaultGenericConfigManager(OfBizDelegator delegator,
                                       FeatureManager featureManager, JiraProperties jiraProperties) {
        this.delegator = notNull("OfBizDelegator", delegator);
        this.streamBuilder = new XStreamBuilder(featureManager, jiraProperties);
    }

    public void create(String dataType, String key, @Nullable Object obj) {
        if (obj != null) {
            String xml = toXml(obj);
            log.debug(obj + " stored as " + xml);
            Map fields = toFieldsMap(dataType, key, xml);

            EntityUtils.createValue(ENTITY_TABLE_NAME, fields);
        }
    }

    public void update(String dataType, String key, @Nullable Object obj) {
        if (obj != null) {
            Map fields = toFieldsMap(dataType, key, null);

            try {
                final List gvs = delegator.findByAnd(ENTITY_TABLE_NAME, fields);
                String xml = toXml(obj);

                if (gvs != null && !gvs.isEmpty()) {
                    // @TODO Probably should do something when more than 1 is returned
                    final GenericValue gv = (GenericValue) gvs.iterator().next();
                    gv.setString(ENTITY_XML_VALUE, xml);
                    gv.store();
                } else {
                    fields.put(ENTITY_XML_VALUE, xml);
                    EntityUtils.createValue(ENTITY_TABLE_NAME, fields);
                }
            } catch (GenericEntityException e) {
                throw new DataAccessException(e);
            }
        } else {
            remove(dataType, key);
        }
    }

    public Object retrieve(String dataType, String key) {
        Map fields = toFieldsMap(dataType, key, null);

        final List gvs = delegator.findByAnd(ENTITY_TABLE_NAME, fields);

        if (gvs != null && !gvs.isEmpty()) {
            // @TODO Probably should do something when more than 1 is returned
            final GenericValue gv = (GenericValue) gvs.iterator().next();
            String xml = gv.getString(ENTITY_XML_VALUE);
            return fromXml(xml);
        } else {
            return null;
        }

    }

    public void remove(String dataType, String key) {
        Map fields = toFieldsMap(dataType, key, null);
        delegator.removeByAnd(ENTITY_TABLE_NAME, fields);
    }

    private Map toFieldsMap(String dataType, String key, String xml) {
        Map fields = new NotNullHashMap();
        fields.put(ENTITY_DATA_TYPE, dataType);
        fields.put(ENTITY_DATA_KEY, key);
        fields.put(ENTITY_XML_VALUE, xml);
        return fields;
    }

    private String toXml(Object obj) {
        if (obj != null) {
            XStream xStream = new XStream();
            xStream.registerConverter(new TimestampConverter());
            String xml = xStream.toXML(obj);
            return xml;
        } else {
            return null;
        }
    }

    /**
     * Custom serializer-deserializer for Timestamps. The reason for needing this is that
     * in the past xStream serialized timestamps in current timezone while from 1.4.8 on it assumes is UTC
     * so it breaks already existing custom fields.
     *
     * It serializes with a SimpleDateFormat in current timezone.
     * When deserializing, it assumes that it was serialized in current timezone.
     */
    private static class TimestampConverter extends AbstractSingleValueConverter {
        private static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss.S";

        public boolean canConvert(Class type) {
            return type != null && type.equals(Timestamp.class);
        }

        public String toString(Object source) {
            Date date = new Date(((Timestamp)source).getTime());
            SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT);
            String str = format.format(date);
            return str;
        }

        public Object fromString(String str) {
            SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT);
            try {
                Date date = formatter.parse(str);
                return new Timestamp(date.getTime());
            } catch (ParseException e) {
                log.error("Could not parse {} as Timestamp", str);
                return null;
            }
        }
    }

    private Object fromXml(String xml) {
        if (StringUtils.isNotEmpty(xml)) {
            XStream xStream = streamBuilder.getXStreamInstance();
            xStream.registerConverter(new TimestampConverter());
            return xStream.fromXML(xml);
        } else {
            return null;
        }
    }

    /**
     * This class constructs XStream instances that are pre-configured with the correct filters on what classes
     * can be serialised and deserialised. It takes in to account whether it is running in cloud or BTF.
     */
    class XStreamBuilder {

        private static final String JIRA_WHITELIST_FILTERING_DISABLED = "enable.generic.config.serialization.whitelistdisabled";
        final private boolean isOnDemand;
        final private boolean filteringIsEnabled;

        public XStreamBuilder(FeatureManager featureManager, JiraProperties jiraProperties) {
            isOnDemand = featureManager.isOnDemand();
            this.filteringIsEnabled = !jiraProperties.getBoolean(JIRA_WHITELIST_FILTERING_DISABLED);;
        }

        XStream getXStreamInstance() {
            XStream stream = new XStream();
            buildBtfXStreamPermissions(stream);
            return stream;
        }

        private void buildBtfXStreamPermissions(XStream stream) {
            // allow all types of deserialisation by default.
            stream.addPermission(AnyTypePermission.ANY);
            excludeBtfSpecificClasses(stream);
        }

        private void excludeBtfSpecificClasses(XStream stream) {
            final String[] blockClasses = {
                    "org.apache.commons.collections.comparators.TransformingComparator",
                    "org.apache.commons.collections.comparators.ComparableComparator",
                    "org.apache.commons.collections.functors.InvokerTransformer",
                    "org.apache.xalan.xsltc.trax.TemplatesImpl"
            };
            stream.denyTypes(blockClasses);
        }

        private void buildCloudXStreamPermissions(XStream stream) {
            // In cloud only allow selected classes to be de-serialised.
            stream.addPermission(NoTypePermission.NONE);
            stream.addPermission(PrimitiveTypePermission.PRIMITIVES);
            stream.addPermission(NullPermission.NULL);
            addCloudSpecificClasses(stream);
        }

        private void addCloudSpecificClasses(XStream stream) {
            final Class[] allowedClasses = {
                    String.class,
                    Map.class,
                    Set.class,
                    List.class
            };
            final String[] allowedClassesByName = {
                    "java.sql.Timestamp",
                    "com.atlassian.jira.issue.customfields.view.CustomFieldParamsImpl",
                    "java.util.Collections$UnmodifiableList"
            };
            stream.allowTypes(allowedClasses);
            stream.allowTypes(allowedClassesByName);
        }
    }

}
