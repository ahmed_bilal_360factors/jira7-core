package com.atlassian.jira.bc.security.login;

import org.apache.commons.lang.builder.ToStringBuilder;


/**
 * @since v4.0.1
 */
public final class LoginInfoImpl implements LoginInfo {
    private final Long lastLoginTime;
    private final Long previousLoginTime;
    private final Long loginCount;
    private final Long currentFailedLoginCount;
    private final Long totalFailedLoginCount;
    private final Long lastFailedLoginTime;
    private final boolean elevatedSecurityCheckRequired;
    private final Long maxAuthenticationAttemptsAllowed;

    private LoginInfoImpl(Builder builder) {
        this.lastLoginTime = builder.getLastLoginTime();
        this.previousLoginTime = builder.getPreviousLoginTime();
        this.lastFailedLoginTime = builder.getLastFailedLoginTime();
        this.loginCount = builder.getLoginCount();
        this.currentFailedLoginCount = builder.getCurrentFailedLoginCount();
        this.totalFailedLoginCount = builder.getTotalFailedLoginCount();
        this.maxAuthenticationAttemptsAllowed = builder.getMaxAuthenticationAttemptsAllowed();
        this.elevatedSecurityCheckRequired = builder.isElevatedSecurityCheckRequired();
    }

    public Long getMaxAuthenticationAttemptsAllowed() {
        return maxAuthenticationAttemptsAllowed;
    }

    public Long getLastLoginTime() {
        return lastLoginTime;
    }

    public Long getPreviousLoginTime() {
        return previousLoginTime;
    }

    public Long getLoginCount() {
        return loginCount;
    }

    public Long getCurrentFailedLoginCount() {
        return currentFailedLoginCount;
    }

    public Long getTotalFailedLoginCount() {
        return totalFailedLoginCount;
    }

    public Long getLastFailedLoginTime() {
        return lastFailedLoginTime;
    }

    public boolean isElevatedSecurityCheckRequired() {
        return elevatedSecurityCheckRequired;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final LoginInfoImpl loginInfo = (LoginInfoImpl) o;

        if (elevatedSecurityCheckRequired != loginInfo.elevatedSecurityCheckRequired) {
            return false;
        }
        if (currentFailedLoginCount != null ? !currentFailedLoginCount.equals(loginInfo.currentFailedLoginCount) : loginInfo.currentFailedLoginCount != null) {
            return false;
        }
        if (lastFailedLoginTime != null ? !lastFailedLoginTime.equals(loginInfo.lastFailedLoginTime) : loginInfo.lastFailedLoginTime != null) {
            return false;
        }
        if (lastLoginTime != null ? !lastLoginTime.equals(loginInfo.lastLoginTime) : loginInfo.lastLoginTime != null) {
            return false;
        }
        if (loginCount != null ? !loginCount.equals(loginInfo.loginCount) : loginInfo.loginCount != null) {
            return false;
        }
        if (maxAuthenticationAttemptsAllowed != null ? !maxAuthenticationAttemptsAllowed.equals(loginInfo.maxAuthenticationAttemptsAllowed) : loginInfo.maxAuthenticationAttemptsAllowed != null) {
            return false;
        }
        if (previousLoginTime != null ? !previousLoginTime.equals(loginInfo.previousLoginTime) : loginInfo.previousLoginTime != null) {
            return false;
        }
        if (totalFailedLoginCount != null ? !totalFailedLoginCount.equals(loginInfo.totalFailedLoginCount) : loginInfo.totalFailedLoginCount != null) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = lastLoginTime != null ? lastLoginTime.hashCode() : 0;
        result = 31 * result + (previousLoginTime != null ? previousLoginTime.hashCode() : 0);
        result = 31 * result + (loginCount != null ? loginCount.hashCode() : 0);
        result = 31 * result + (currentFailedLoginCount != null ? currentFailedLoginCount.hashCode() : 0);
        result = 31 * result + (totalFailedLoginCount != null ? totalFailedLoginCount.hashCode() : 0);
        result = 31 * result + (lastFailedLoginTime != null ? lastFailedLoginTime.hashCode() : 0);
        result = 31 * result + (elevatedSecurityCheckRequired ? 1 : 0);
        result = 31 * result + (maxAuthenticationAttemptsAllowed != null ? maxAuthenticationAttemptsAllowed.hashCode() : 0);
        return result;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static Builder builder(LoginInfo info) {
        return new Builder(info);
    }

    /**
     * @since 7.0
     */
    public static class Builder {
        private Long lastLoginTime;
        private Long previousLoginTime;
        private Long loginCount;
        private Long currentFailedLoginCount;
        private Long totalFailedLoginCount;
        private Long lastFailedLoginTime;
        private boolean elevatedSecurityCheckRequired;
        private Long maxAuthenticationAttemptsAllowed;

        private Builder() {
        }

        private Builder(final LoginInfo info) {
            this.lastLoginTime = info.getLastLoginTime();
            this.previousLoginTime = info.getPreviousLoginTime();
            this.loginCount = info.getLoginCount();
            this.currentFailedLoginCount = info.getCurrentFailedLoginCount();
            this.totalFailedLoginCount = info.getTotalFailedLoginCount();
            this.lastFailedLoginTime = info.getLastFailedLoginTime();
            this.elevatedSecurityCheckRequired = info.isElevatedSecurityCheckRequired();
            this.maxAuthenticationAttemptsAllowed = info.getMaxAuthenticationAttemptsAllowed();
        }

        public Long getLastLoginTime() {
            return lastLoginTime;
        }

        public Long getPreviousLoginTime() {
            return previousLoginTime;
        }

        public Long getLoginCount() {
            return loginCount;
        }

        public Long getCurrentFailedLoginCount() {
            return currentFailedLoginCount;
        }

        public Long getTotalFailedLoginCount() {
            return totalFailedLoginCount;
        }

        public Long getLastFailedLoginTime() {
            return lastFailedLoginTime;
        }

        public boolean isElevatedSecurityCheckRequired() {
            return elevatedSecurityCheckRequired;
        }

        public Long getMaxAuthenticationAttemptsAllowed() {
            return maxAuthenticationAttemptsAllowed;
        }

        public Builder setLastLoginTime(final Long lastLoginTime) {
            this.lastLoginTime = lastLoginTime;
            return this;
        }

        public Builder setPreviousLoginTime(final Long previousLoginTime) {
            this.previousLoginTime = previousLoginTime;
            return this;
        }

        public Builder setLoginCount(final Long loginCount) {
            this.loginCount = loginCount;
            return this;
        }

        public Builder setCurrentFailedLoginCount(final Long currentFailedLoginCount) {
            this.currentFailedLoginCount = currentFailedLoginCount;
            return this;
        }

        public Builder setTotalFailedLoginCount(final Long totalFailedLoginCount) {
            this.totalFailedLoginCount = totalFailedLoginCount;
            return this;
        }

        public Builder setLastFailedLoginTime(final Long lastFailedLoginTime) {
            this.lastFailedLoginTime = lastFailedLoginTime;
            return this;
        }

        public Builder setElevatedSecurityCheckRequired(final boolean elevatedSecurityCheckRequired) {
            this.elevatedSecurityCheckRequired = elevatedSecurityCheckRequired;
            return this;
        }

        public Builder setMaxAuthenticationAttemptsAllowed(final Long maxAuthenticationAttemptsAllowed) {
            this.maxAuthenticationAttemptsAllowed = maxAuthenticationAttemptsAllowed;
            return this;
        }

        public Builder failedAt(long now) {
            this.currentFailedLoginCount = increment(this.currentFailedLoginCount);
            this.totalFailedLoginCount = increment(this.totalFailedLoginCount);
            this.lastFailedLoginTime = now;

            return this;
        }

        public Builder succeededAt(long now) {
            this.lastFailedLoginTime = 0L;
            this.currentFailedLoginCount = 0L;
            this.loginCount = increment(this.loginCount);
            if (this.lastLoginTime != null) {
                this.previousLoginTime = this.lastLoginTime;
            }
            this.lastLoginTime = now;

            return this;
        }

        public LoginInfo build() {
            return new LoginInfoImpl(this);
        }

        private static Long increment(Long value) {
            if (value == null || value < 0) {
                return 1L;
            } else {
                return value + 1;
            }
        }
    }
}
