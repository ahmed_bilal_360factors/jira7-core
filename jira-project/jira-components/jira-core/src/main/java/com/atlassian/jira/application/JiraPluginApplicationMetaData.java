package com.atlassian.jira.application;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.application.api.ApplicationPlugin;
import com.atlassian.application.host.plugin.PluginApplicationMetaData;
import com.atlassian.fugue.Option;
import com.atlassian.jira.project.type.ProjectType;
import com.google.common.collect.ImmutableList;
import org.joda.time.DateTime;

import java.net.URI;

/**
 * Metadata for JIRA application definitions. It JIRA application definition is exactly the same as any other
 * regular application definition but it also allows the definition of project types.
 */
public class JiraPluginApplicationMetaData implements PluginApplicationMetaData {
    private final PluginApplicationMetaData applicationMetaData;
    private final Iterable<ProjectType> projectTypes;

    public JiraPluginApplicationMetaData(final PluginApplicationMetaData applicationMetaData, Iterable<ProjectType> projectTypes) {
        this.applicationMetaData = applicationMetaData;
        this.projectTypes = ImmutableList.copyOf(projectTypes);
    }

    public Iterable<ProjectType> getProjectTypes() {
        return projectTypes;
    }

    @Override
    public ApplicationKey getKey() {
        return applicationMetaData.getKey();
    }

    @Override
    public String getName() {
        return applicationMetaData.getName();
    }

    @Override
    public String getVersion() {
        return applicationMetaData.getVersion();
    }

    @Override
    public String getDescriptionKey() {
        return applicationMetaData.getDescriptionKey();
    }

    @Override
    public String getUserCountKey() {
        return applicationMetaData.getUserCountKey();
    }

    @Override
    public Option<URI> getConfigurationURI() {
        return applicationMetaData.getConfigurationURI();
    }

    @Override
    public Option<URI> getPostInstallURI() {
        return applicationMetaData.getPostInstallURI();
    }

    @Override
    public Option<URI> getPostUpdateURI() {
        return applicationMetaData.getPostUpdateURI();
    }

    @Override
    public DateTime buildDate() {
        return applicationMetaData.buildDate();
    }

    @Override
    public String getDefinitionModuleKey() {
        return applicationMetaData.getDefinitionModuleKey();
    }

    @Override
    public Iterable<ApplicationPlugin> getPlugins() {
        return applicationMetaData.getPlugins();
    }

    @Override
    public ApplicationPlugin getPrimaryPlugin() {
        return applicationMetaData.getPrimaryPlugin();
    }

    @Override
    public Iterable<ApplicationPlugin> getApplicationPlugins() {
        return applicationMetaData.getApplicationPlugins();
    }

    @Override
    public Iterable<ApplicationPlugin> getUtilityPlugins() {
        return applicationMetaData.getUtilityPlugins();
    }

    @Override
    public String getDefaultGroup() {
        return applicationMetaData.getDefaultGroup();
    }

    @Override
    public Option<URI> getProductHelpServerSpaceURI() {
        return applicationMetaData.getProductHelpServerSpaceURI();
    }

    @Override
    public Option<URI> getProductHelpCloudSpaceURI() {
        return applicationMetaData.getProductHelpCloudSpaceURI();
    }
}
