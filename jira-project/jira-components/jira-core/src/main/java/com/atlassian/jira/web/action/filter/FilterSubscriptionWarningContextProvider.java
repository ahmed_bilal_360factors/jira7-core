package com.atlassian.jira.web.action.filter;

import com.atlassian.jira.bc.filter.FilterSubscriptionService;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.issue.search.SearchRequestManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.plugin.web.ContextProvider;
import com.google.common.annotations.VisibleForTesting;

import java.util.Map;

import static com.atlassian.jira.bc.filter.FilterDeletionWarningViewProviderImpl.CONTEXT_KEY_SEARCH_REQUEST;

/**
 * Provides context for the template that generates warning messages whenever a user is about
 * to delete a filter that has one or more subscriptions against it.
 */
public class FilterSubscriptionWarningContextProvider implements ContextProvider {
    @VisibleForTesting
    static final String CONTEXT_KEY_SUBSCRIPTION_COUNT = "subscriptionCount";
    
    @VisibleForTesting
    static final String CONTEXT_KEY_SUBSCRIPTION_PAGE_URL = "subscriptionPageUrl";
    
    private final FilterSubscriptionService subscriptionService;
    private final SearchRequestManager searchRequestManager;

    public FilterSubscriptionWarningContextProvider(final FilterSubscriptionService subscriptionService,
                                                    final SearchRequestManager searchRequestManager) {
        this.subscriptionService = subscriptionService;
        this.searchRequestManager = searchRequestManager;
    }

    @Override
    public void init(final Map<String, String> map) {
    }

    @Override
    public Map<String, Object> getContextMap(final Map<String, Object> map) {
        SearchRequest searchRequest = (SearchRequest) map.get(CONTEXT_KEY_SEARCH_REQUEST);
        map.put(CONTEXT_KEY_SUBSCRIPTION_COUNT, getSubscriptionCount(searchRequest));
        map.put(CONTEXT_KEY_SUBSCRIPTION_PAGE_URL, getManageSubscriptionsLink(searchRequest));
        return map;
    }

    private int getSubscriptionCount(final SearchRequest searchRequest) {
        ApplicationUser filterOwner = searchRequestManager.getSearchRequestOwner(searchRequest.getId());
        return subscriptionService.getVisibleFilterSubscriptions(filterOwner, searchRequest).size();
    }

    private String getManageSubscriptionsLink(final SearchRequest searchRequest) {
        return "/secure/ViewSubscriptions.jspa?filterId=" + searchRequest.getId();
    }
}
