package com.atlassian.jira.upgrade;

import com.atlassian.jira.database.DbConnectionManager;
import com.atlassian.jira.ofbiz.OfBizDelegator;

import javax.annotation.Nonnull;

/**
 * Implement this to create a Downgrade Task.
 * A downgrade task is the reversal of an Upgrade Task. Not all upgrade tasks require an explicit reverse operation -
 * indeed most of them are a no-op to downgrad.
 *
 * @see UpgradeTask#isDowngradeTaskRequired()
 * @since v6.4.6
 */
public interface DowngradeTask {
    /**
     * @return The build number that this downgrade task downgrades from.
     */
    int getBuildNumber();

    /**
     * A short (<50 chars) description of the downgrade task
     */
    @Nonnull
    String getShortDescription();

    /**
     * Perform the downgrade.
     */
    void downgrade() throws DowngradeException;

    /**
     * Use this to declare if the downgrade task will require reindexing JIRA.
     *
     * @return reindexing requirement of this downgrade task.
     */
    @Nonnull
    ReindexRequirement reindexRequired();

    /**
     * Provides access to the DB for this Downgrade Task.
     *
     * @return the DbConnectionManager
     */
    @Nonnull
    DbConnectionManager getDbConnectionManager();

    /**
     * Provides access to the DB for this Downgrade Task.
     *
     * @return the OfBizDelegator
     */
    @Nonnull
    OfBizDelegator getOfBizDelegator();

    /**
     * This is how the Downgrade task framework injects the DbConnectionManager.
     * <p>This method is implemented by AbstractDowngradeTask.</p>
     */
    void setDbConnectionManager(@Nonnull DbConnectionManager dbConnectionManager);

    /**
     * This is how the Downgrade task framework injects the OfBizDelegator.
     * <p>This method is implemented by AbstractDowngradeTask.</p>
     */
    void setOfBizDelegator(@Nonnull OfBizDelegator ofBizDelegator);
}