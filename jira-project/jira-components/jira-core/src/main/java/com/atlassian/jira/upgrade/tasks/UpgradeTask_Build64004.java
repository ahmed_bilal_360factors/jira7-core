package com.atlassian.jira.upgrade.tasks;

import com.atlassian.jira.index.request.AffectedIndex;
import com.atlassian.jira.index.request.ReindexRequestType;
import com.atlassian.jira.index.request.SharedEntityType;

import java.util.EnumSet;

/**
 * Indexes worklogs in their own index so they are JQL searchable. Update is performed only if the time tracing option
 * is set to on and there are any worklogs on the instance.
 */
public class UpgradeTask_Build64004 extends AbstractReindexUpgradeTask {
    @Override
    public int getBuildNumber() {
        return 64004;
    }

    @Override
    public void doUpgrade(boolean setupMode) throws Exception {
        getReindexRequestService().requestReindex(ReindexRequestType.DELAYED, EnumSet.of(AffectedIndex.WORKLOG), EnumSet.noneOf(SharedEntityType.class));
    }

    @Override
    public boolean isDowngradeTaskRequired() {
        // Flagged as not downgradable because it was created before the Downgrade Task framework was built.
        return true;
    }

    @Override
    public String getShortDescription() {
        return "Indexes worklogs in their own index so they are JQL searchable.";
    }
}
