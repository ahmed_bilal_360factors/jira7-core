/*
 * Editing of project lead and deafult assignee
 *
 * @since v4.4
 */

package com.atlassian.jira.web.action.project;

import com.atlassian.fugue.Either;
import com.atlassian.jira.avatar.AvatarService;
import com.atlassian.jira.bc.project.ProjectService;
import com.atlassian.jira.bc.user.search.UserSearchService;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.security.xsrf.RequiresXsrfCheck;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.web.action.RequestSourceType;
import com.google.common.base.Strings;

import java.net.URI;

import static com.atlassian.jira.avatar.Avatar.Size.SMALL;
import static com.atlassian.jira.bc.project.ProjectService.UpdateProjectRequest;
import static com.atlassian.jira.bc.project.ProjectService.UpdateProjectValidationResult;

public class EditProjectLeadAndDefaultAssignee extends ViewProject {
    private final ProjectService projectService;
    private final AvatarService avatarService;
    private final UserManager userManager;
    private final UserSearchService userSearchService;
    private String leadError;
    private ApplicationUser leadUserObj;

    public EditProjectLeadAndDefaultAssignee(ProjectService projectService, AvatarService avatarService, UserManager userManager, UserSearchService userSearchService) {
        this.projectService = projectService;
        this.avatarService = avatarService;
        this.userManager = userManager;
        this.userSearchService = userSearchService;
    }

    public String doDefault() throws Exception {
        // check if the project exists:
        if (getProjectObject() == null) {
            return handleProjectDoesNotExist();
        }
        if (!(hasProjectAdminPermission() || hasAdminPermission())) {
            return "securitybreach";
        }
        setName(getProjectObject().getName());
        setAvatarId(getProjectObject().getAvatar() != null ? getProjectObject().getAvatar().getId() : null);
        setLead(getProjectObject().getLeadUserName());
        setUrl(getProjectObject().getUrl());
        setDescription(getProjectObject().getDescription());
        setAssigneeType(getProjectObject().getAssigneeType());

        return INPUT;
    }

    private String handleProjectDoesNotExist() throws Exception {
        if (hasAdminPermission()) {
            // User is admin - admit that the Project Doesn't exist because they have permission to see any project.
            // We will show the Edit Project Page, but without any values in the fields (and with an error message).
            // This is consistent with what happens if we start to edit a project, but it gets deleted before we save it.
            setName("???");
            addErrorMessage(getText("admin.errors.project.no.project.with.id"));

            return super.doDefault();
        } else {
            // User is not admin - show security breach because this isn't a Project they have permission to edit.
            return "securitybreach";
        }
    }

    protected void doValidation() {
        // First check that the Project still exists
        if (getProjectObject() == null) {
            addErrorMessage(getText("admin.errors.project.no.project.with.id"));
            // Don't try to do any more validation.
            return;
        }
        final UpdateProjectValidationResult result = validateProjectUpdate();
        if (!result.isValid()) {
            //map keyed errors to JSP field names
            mapErrorCollection(result.getErrorCollection());
        }

        if (getLeadUserObj() == null) {
            setLeadError(getLead());
        }

    }

    @RequiresXsrfCheck
    protected String doExecute() throws Exception {
        if (!(hasProjectAdminPermission() || hasAdminPermission())) {
            return "securitybreach";
        }


        final Project projectObject = getProjectObject();
        final UpdateProjectValidationResult result = validateProjectUpdate();
        projectService.updateProject(result);

        if (isInlineDialogMode()) {
            return returnComplete();
        }

        return getRedirect("/plugins/servlet/project-config/" + getProjectObject().getKey() + "/roles");
    }

    private UpdateProjectValidationResult validateProjectUpdate() {
        final UpdateProjectRequest request = new UpdateProjectRequest(getProjectObject());
        request.leadUsername(Strings.nullToEmpty(getLead()));
        request.assigneeType(getAssigneeType());
        request.requestSourceType(isInlineDialogMode() ? RequestSourceType.DIALOG : RequestSourceType.PAGE);
        return projectService.validateUpdateProject(getLoggedInUser(), request);
    }

    public boolean userPickerDisabled() {
        return !userSearchService.canPerformAjaxSearch(this.getJiraServiceContext());
    }

    public ApplicationUser getLeadUserObj() {
        if (getLead() != null && leadUserObj == null) {
            leadUserObj = userManager.getUserByName(getLead());
        }
        return leadUserObj;
    }

    public URI getLeadUserAvatarUrl() {
        return avatarService.getAvatarURL(getLoggedInUser(), getLead(), SMALL);
    }

    public String getLeadError() {
        return leadError;
    }

    public void setLeadError(String leadError) {
        this.leadError = leadError;
    }
}
