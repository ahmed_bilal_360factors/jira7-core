package com.atlassian.jira.admin;

/**
 * This class defines whether the project-centric sidebar in the project administration should
 * be displayed for a given user.
 *
 * @since v7.1
 */
public interface ProjectAdminSidebarFeature {
    boolean shouldDisplay();
}
