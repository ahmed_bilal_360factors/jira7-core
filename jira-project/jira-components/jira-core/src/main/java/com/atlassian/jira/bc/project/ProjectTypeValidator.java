package com.atlassian.jira.bc.project;

import com.atlassian.jira.project.type.ProjectTypeKey;
import com.atlassian.jira.project.type.ProjectTypeManager;
import com.atlassian.jira.user.ApplicationUser;

/**
 * Validator for project types.
 */
public class ProjectTypeValidator {
    private final ProjectTypeManager projectTypeManager;

    public ProjectTypeValidator(ProjectTypeManager projectTypeManager) {
        this.projectTypeManager = projectTypeManager;
    }

    /**
     * Validates the given project type. A project type can not be null and its value must be defined on the JIRA
     * instance.
     *
     * @param user           The user that performs the action during which the project type gets validated
     * @param projectTypeKey The project type key to validate
     * @return Whether the project type is valid or not
     */
    public boolean isValid(ApplicationUser user, ProjectTypeKey projectTypeKey) {
        return projectTypeKey != null && projectTypeManager.getAccessibleProjectType(projectTypeKey).isDefined();
    }
}
