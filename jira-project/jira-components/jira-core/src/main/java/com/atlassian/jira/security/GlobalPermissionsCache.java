package com.atlassian.jira.security;

import com.atlassian.cache.CacheManager;
import com.atlassian.cache.CachedReference;
import com.atlassian.cache.Supplier;
import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.crowd.embedded.impl.ImmutableGroup;
import com.atlassian.jira.config.group.GroupConfigurable;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import org.ofbiz.core.entity.GenericValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 * This is a very basic cache that stores permissions
 * <p>
 * When constructed, or when you call refresh() - it will find and cache all permissions
 */
public class GlobalPermissionsCache implements GroupConfigurable {
    private static final Logger log = LoggerFactory.getLogger(GlobalPermissionsCache.class);

    private final OfBizDelegator ofBizDelegator;

    // set of all permissions
    private final CachedReference<Set<GlobalPermissionEntry>> permissions;

    private final PermissionsSupplier permissionsSupplier;

    /**
     * Create a new permissions cache.
     */
    GlobalPermissionsCache(OfBizDelegator ofBizDelegator, CacheManager cacheManager) {
        this.ofBizDelegator = ofBizDelegator;
        if (log.isDebugEnabled()) {
            log.debug("GlobalPermissionsCache.GlobalPermissionsCache");
        }
        permissionsSupplier = new PermissionsSupplier();
        permissions = cacheManager.getCachedReference("com.atlassian.jira.security.GlobalPermissionsCache.permissions",
                permissionsSupplier);
        permissions.reset();
    }

    public void clearCache() {
        permissions.reset();
    }

    public boolean hasPermission(final GlobalPermissionEntry jiraPermission) {
        return permissions.get().contains(jiraPermission);
    }

    public Set<GlobalPermissionEntry> getPermissions() {
        return permissions.get();
    }

    /**
     * Get a Collection of permission based on a permissionType
     *
     * @param permissionType must be global permission type
     * @return Collction of Permission objects
     */
    public Collection<GlobalPermissionEntry> getPermissions(final String permissionType) {
        final Set<GlobalPermissionEntry> globalPermissionEntries = permissions.get();
        final List<GlobalPermissionEntry> matchingPerms = Lists.newArrayListWithCapacity(globalPermissionEntries.size());
        for (final GlobalPermissionEntry perm : globalPermissionEntries) {
            if (perm.getPermissionKey().equals(permissionType)) {
                matchingPerms.add(perm);
            }
        }
        return matchingPerms;
    }

    @Override
    public boolean isGroupUsed(@Nonnull final Group group) {
        return permissions.get().stream()
                .map(GlobalPermissionEntry::getGroup)
                .filter(Objects::nonNull)
                .map(ImmutableGroup::new)
                .anyMatch(group::equals);
    }

    private class PermissionsSupplier implements Supplier<Set<GlobalPermissionEntry>> {
        @Override
        public Set<GlobalPermissionEntry> get() {
            Set<GlobalPermissionEntry> permissions = new HashSet<GlobalPermissionEntry>();

            final Collection<GenericValue> allPermissions = ofBizDelegator.findAll("GlobalPermissionEntry");
            for (final GenericValue permissionGV : allPermissions) {
                final GlobalPermissionEntry permEntry = new GlobalPermissionEntry(permissionGV);
                boolean added = permissions.add(permEntry);
                if (!added) {
                    log.warn("Could not add permission " + permEntry + " - it already existed?");
                }
            }
            return ImmutableSet.copyOf(permissions);
        }
    }
}
