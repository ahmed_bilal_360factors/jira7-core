package com.atlassian.jira.upgrade;

import com.atlassian.jira.util.xml.XmlNode;
import com.atlassian.security.xml.SecureXmlParserFactory;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Parses the downgrades.xml file.
 *
 * @since v6.4.6
 */
public class DowngradeTaskFileParser {
    public Collection<String> parse(final InputStream inputStream) throws ParserConfigurationException, IOException, SAXException {
        final DocumentBuilderFactory docBuilderFactory = SecureXmlParserFactory.newDocumentBuilderFactory();

        DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
        final Document doc = docBuilder.parse(inputStream);

        XmlNode docNode = new XmlNode(doc);
        return parseDowngrades(docNode.getFirstChild("downgrades"));
    }

    private Collection<String> parseDowngrades(final XmlNode downgradesNode) {
        // <downgrades>
        //     <downgrade>
        //         <class>com.atlassian.jira.upgrade.tasks.DowngradeTask_Build64012</class>
        //     </downgrade>

        final List<XmlNode> downgradeNodes = downgradesNode.getChildren("downgrade");
        final Collection<String> downgradesList = new ArrayList<String>(downgradeNodes.size());
        for (XmlNode downgradeNode : downgradeNodes) {
            String downgradeTask = parseDowngradeTask(downgradeNode);
            downgradesList.add(downgradeTask);
        }

        return downgradesList;
    }

    private String parseDowngradeTask(final XmlNode downgradeNode) {
        //     <downgrade>
        //         <class>com.atlassian.jira.upgrade.tasks.DowngradeTask_Build64012</class>
        //     </downgrade>
        final XmlNode child = downgradeNode.getFirstChild("class");
        if (child == null) {
            throw new IllegalStateException("Downgrade task XML config file is in an invalid state.");
        }
        return child.getTextContent();
    }
}
