package com.atlassian.jira.help;

import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.util.BuildUtilsInfo;

/**
 * This is the actual {@link com.atlassian.jira.help.HelpUrlsParserBuilderFactory} in PICO.
 *
 * @since 7.0
 */
public class InitialHelpUrlsParserBuilderFactory extends DefaultHelpUrlsParserBuilderFactory {
    public InitialHelpUrlsParserBuilderFactory(final FeatureManager featureManager, final LocalHelpUrls localUrls, final BuildUtilsInfo buildUtilsInfo) {
        super(localUrls, buildUtilsInfo);
    }
}
