package com.atlassian.jira.cluster.zdu;

import com.atlassian.beehive.ClusterLock;
import com.atlassian.beehive.ClusterLockService;
import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.EventComponent;
import com.atlassian.jira.cluster.ClusterInfo;
import com.atlassian.jira.cluster.ClusterManager;
import com.atlassian.jira.cluster.MessageHandlerService;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.event.JiraDelayedUpgradeCompletedEvent;
import com.atlassian.jira.extension.JiraStartedEvent;
import com.atlassian.jira.model.querydsl.ClusterUpgradeStateDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.util.EnumSet;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

import static com.atlassian.jira.cluster.zdu.UpgradeState.MIXED;
import static com.atlassian.jira.cluster.zdu.UpgradeState.READY_TO_RUN_UPGRADE_TASKS;
import static com.atlassian.jira.cluster.zdu.UpgradeState.READY_TO_UPGRADE;
import static com.atlassian.jira.cluster.zdu.UpgradeState.RUNNING_UPGRADE_TASKS;
import static com.atlassian.jira.cluster.zdu.UpgradeState.STABLE;
import static java.util.stream.Collectors.toSet;

/**
 * @since v7.3
 */
@EventComponent
public class DefaultClusterUpgradeStateManager implements ClusterUpgradeStateManager {

    private static final Logger log = LoggerFactory.getLogger(DefaultClusterUpgradeStateManager.class);

    private static final String CLUSTER_UPGRADE_TATE_LOCK_NAME = DefaultClusterUpgradeStateManager.class.getName() + ".clusterUpgradeState";
    private static final Runnable DO_NOTHING = () -> { };
    private static final EnumSet<UpgradeState> AUTO_TRANSITION_STATES = EnumSet.of(MIXED, READY_TO_UPGRADE, READY_TO_RUN_UPGRADE_TASKS);

    private final ClusterManager clusterManager;
    private final ClusterLock lock;
    private final ClusterUpgradeStateDao clusterUpgradeStateDao;
    private final ClusterInfo clusterInfo;
    private final EventPublisher eventPublisher;
    private final FeatureManager featureManager;
    private final MessageHandlerService messageHandlerService;
    private final NodeBuildInfoFactory nodeBuildInfoFactory;

    public DefaultClusterUpgradeStateManager(
            ClusterManager clusterManager,
            ClusterLockService clusterLockService,
            ClusterUpgradeStateDao clusterUpgradeStateDao,
            ClusterInfo clusterInfo,
            EventPublisher eventPublisher,
            FeatureManager featureManager,
            MessageHandlerService messageHandlerService,
            NodeBuildInfoFactory nodeBuildInfoFactory
    ) {
        this.clusterManager = clusterManager;
        lock = clusterLockService.getLockForName(CLUSTER_UPGRADE_TATE_LOCK_NAME);
        this.clusterUpgradeStateDao = clusterUpgradeStateDao;
        this.clusterInfo = clusterInfo;
        this.eventPublisher = eventPublisher;
        this.featureManager = featureManager;
        this.messageHandlerService = messageHandlerService;
        this.nodeBuildInfoFactory = nodeBuildInfoFactory;
    }

    @Override
    public void startUpgrade() {
        final Runnable actionsAfterLockDropped;
        lock.lock();
        try {
            UpgradeState upgradeState = getDatabaseUpgradeState();
            if (upgradeState != STABLE) {
                throw new IllegalStateException("Can only transition to " + READY_TO_UPGRADE +
                        " from " + STABLE + " but was in " + upgradeState);
            }
            NodeBuildInfo nodeBuildInfo = getNodeBuildInfo();
            setUpgradeState(nodeBuildInfo, READY_TO_UPGRADE);
            actionsAfterLockDropped = () -> {
                eventPublisher.publish(new JiraUpgradeStartedEvent(nodeBuildInfo));
                notifyCluster(READY_TO_UPGRADE);
            };
        } finally {
            lock.unlock();
        }
        actionsAfterLockDropped.run();
    }

    @Override
    public void cancelUpgrade() {
        recalculateState();
        final Runnable actionsAfterLockDropped;
        lock.lock();
        try {
            UpgradeState upgradeState = getDatabaseUpgradeState();
            NodeBuildInfo clusterBuildInfo = getClusterBuildInfo();

            switch (upgradeState) {
                case READY_TO_UPGRADE:
                    setUpgradeState(clusterBuildInfo, STABLE);
                    break;
                default:
                    throw new IllegalStateException(String.format(
                            "Cluster can not be rolled back to stable state until upgrade is completed. Current state is [%s]",
                            upgradeState
                    ));
            }
            actionsAfterLockDropped = () -> {
                eventPublisher.publish(new JiraUpgradeCancelledEvent(clusterBuildInfo));
                notifyCluster(STABLE);
            };
        } finally {
            lock.unlock();
        }
        actionsAfterLockDropped.run();
    }

    @Override
    public void approveUpgrade() {
        recalculateState();
        final Runnable actionsAfterLockDropped;
        lock.lock();
        try {
            UpgradeState upgradeState = getDatabaseUpgradeState();
            if (upgradeState != READY_TO_RUN_UPGRADE_TASKS) {
                throw new IllegalStateException(String.format(
                        "Cluster not ready to run upgrade tasks yet, because it in [%s] state",
                        upgradeState));
            }
            actionsAfterLockDropped = () -> eventPublisher.publish(new JiraUpgradeApprovedEvent(getClusterBuildInfo(), getNodeBuildInfo()));

        } finally {
            lock.unlock();
        }
        actionsAfterLockDropped.run();
    }

    @Override
    public void runUpgrade() {
        lock.lock();
        try {
            UpgradeState upgradeState = getDatabaseUpgradeState();
            switch (upgradeState) {
                case READY_TO_RUN_UPGRADE_TASKS:
                    setUpgradeState(getClusterBuildInfo(), RUNNING_UPGRADE_TASKS);
                    break;
                default:
                    throw new IllegalStateException(String.format(
                            "Cluster not ready to run upgrade tasks yet, because it in [%s] state",
                            upgradeState));
            }
        } finally {
            lock.unlock();
        }
        notifyCluster(RUNNING_UPGRADE_TASKS);
    }

    @EventListener
    public void onJiraUpgradeCompleted(JiraDelayedUpgradeCompletedEvent event) {
        final Runnable actionsAfterLockDropped;
        lock.lock();
        try {
            UpgradeState currentUpgradeState = getDatabaseUpgradeState();
            NodeBuildInfo nodeBuildInfo = getNodeBuildInfo();
            NodeBuildInfo clusterBuildInfo = getClusterBuildInfo();

            if (currentUpgradeState == RUNNING_UPGRADE_TASKS) {
                setUpgradeState(nodeBuildInfo, STABLE);
                actionsAfterLockDropped = () -> {
                    eventPublisher.publish(new JiraUpgradeFinishedEvent(clusterBuildInfo, nodeBuildInfo));
                    notifyCluster(STABLE);
                };
            } else {
                log.warn("Unexpected JiraUpgradedEvent. Cluster wasn't in RUNNING_UPGRADE_TASKS state");
                actionsAfterLockDropped = DO_NOTHING;
            }
        } finally {
            lock.unlock();
        }
        actionsAfterLockDropped.run();
    }

    @EventListener
    public void updateState(JiraStartedEvent event) {
        recalculateState();
    }

    private void recalculateState() {
        final Runnable actionsAfterLockDropped;
        lock.lock();
        try {
            UpgradeState currentUpgradeState = getDatabaseUpgradeState();
            if (AUTO_TRANSITION_STATES.contains(currentUpgradeState)) {
                final UpgradeState targetUpgradeState;
                clusterManager.refreshLiveNodes();
                Set<NodeBuildInfo> buildNumbers = clusterManager.findLiveNodes().stream()
                        .map(nodeBuildInfoFactory::create)
                        // build number could be null from old entries, get rid of those
                        .filter(Objects::nonNull)
                        .collect(toSet());
                if (buildNumbers.isEmpty()) {
                    if (clusterInfo.isClustered()) {
                        log.warn("No cluster node entries, cannot determine upgrade state transition.");
                        targetUpgradeState = null;
                    } else {
                        if (Objects.equals(getNodeBuildInfo(), getClusterBuildInfo())) {
                            targetUpgradeState = null;
                        } else {
                            targetUpgradeState = READY_TO_RUN_UPGRADE_TASKS;
                        }
                    }
                } else if (buildNumbers.size() > 1) {
                    targetUpgradeState = MIXED;
                } else {

                    NodeBuildInfo nodesBuildNumber = buildNumbers.iterator().next();
                    NodeBuildInfo clusterBuildNumber = getClusterBuildInfo();

                    //If we are still at the old version
                    if (clusterBuildNumber.equals(nodesBuildNumber)) {
                        targetUpgradeState = READY_TO_UPGRADE;
                    } else {
                        targetUpgradeState = READY_TO_RUN_UPGRADE_TASKS;
                    }
                }

                if (targetUpgradeState != null && targetUpgradeState != currentUpgradeState) {
                    log.info("Transitioning cluster upgrade state from " + currentUpgradeState + " to " + targetUpgradeState);
                    setUpgradeState(getClusterBuildInfo(), targetUpgradeState);
                    actionsAfterLockDropped = () -> notifyCluster(targetUpgradeState);
                } else {
                    actionsAfterLockDropped = DO_NOTHING;
                }
            } else {
                actionsAfterLockDropped = DO_NOTHING;
            }
        } finally {
            lock.unlock();
        }
        actionsAfterLockDropped.run();
    }

    @Override
    @Nonnull
    public UpgradeState getUpgradeState() {
        if (!featureManager.isEnabled(CLUSTER_UPGRADE_STATE_DARK_FEATURE)) {
            return STABLE;
        }
        recalculateState();
        return getDatabaseUpgradeState();
    }

    private UpgradeState getDatabaseUpgradeState() {
        Optional<ClusterUpgradeStateDTO> currentState = clusterUpgradeStateDao.getCurrent();
        return currentState
                .map(ClusterUpgradeStateDTO::getState)
                .map(UpgradeState::valueOf)
                .orElse(STABLE);
    }

    @Override
    public NodeBuildInfo getClusterBuildInfo() {
        if (!featureManager.isEnabled(CLUSTER_UPGRADE_STATE_DARK_FEATURE)) {
            return getNodeBuildInfo();
        }
        Optional<ClusterUpgradeStateDTO> currentState = clusterUpgradeStateDao.getCurrent();
        return currentState
                .map(nodeBuildInfoFactory::create)
                .orElse(getNodeBuildInfo());
    }

    private NodeBuildInfo getNodeBuildInfo() {
        return nodeBuildInfoFactory.currentApplicationInfo();
    }

    private void setUpgradeState(NodeBuildInfo nodeBuildInfo, @Nonnull UpgradeState upgradeState) {
        clusterUpgradeStateDao.writeState(nodeBuildInfo, upgradeState);
    }

    private void notifyCluster(final UpgradeState upgradeState) {
        messageHandlerService.sendRemote(CLUSTER_UPGRADE_STATE_CHANGED, upgradeState.toString());
    }

    /**
     * Checks if delayed upgrades are handled by the cluster.
     * <p>
     *     Delayed upgrade tasks are handled by the cluster when {@link #CLUSTER_UPGRADE_STATE_DARK_FEATURE} is
     *     turned on <strong>and</strong> cluster is not in {@link UpgradeState#STABLE}. In non-clustered environment
     *     this will always return false
     * </p>
     *
     * @return true if and only if delayed upgrades are handled by the cluster
     */
    @Override
    public boolean areDelayedUpgradesHandledByCluster() {
        final boolean isClusterUpgradeStateDarkFeatureEnabled =
                featureManager.isEnabled(CLUSTER_UPGRADE_STATE_DARK_FEATURE);

        if (isClusterUpgradeStateDarkFeatureEnabled) {
            return getUpgradeState() != UpgradeState.STABLE;
        }

        return false;
    }
}
