package com.atlassian.jira.issue.fields;

import com.atlassian.jira.bc.ServiceOutcome;
import com.atlassian.jira.bc.issue.watcher.WatcherService;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueFieldConstants;
import com.atlassian.jira.issue.export.ExportableSystemField;
import com.atlassian.jira.issue.export.FieldExportParts;
import com.atlassian.jira.issue.export.FieldExportPartsBuilder;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem;
import com.atlassian.jira.issue.fields.rest.FieldJsonRepresentation;
import com.atlassian.jira.issue.fields.rest.FieldTypeInfo;
import com.atlassian.jira.issue.fields.rest.FieldTypeInfoContext;
import com.atlassian.jira.issue.fields.rest.RestAwareField;
import com.atlassian.jira.issue.fields.rest.json.JsonData;
import com.atlassian.jira.issue.fields.rest.json.JsonType;
import com.atlassian.jira.issue.fields.rest.json.JsonTypeBuilder;
import com.atlassian.jira.issue.fields.rest.json.beans.JiraBaseUrls;
import com.atlassian.jira.issue.fields.rest.json.beans.WatchersJsonBean;
import com.atlassian.jira.issue.search.LuceneFieldSorter;
import com.atlassian.jira.issue.statistics.WatchesStatisticsMapper;
import com.atlassian.jira.issue.watchers.WatcherManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.template.VelocityTemplatingEngine;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.lang.Pair;

import java.text.Collator;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import static java.util.stream.Stream.empty;

/**
 * @since 4.4
 */
public class WatchesSystemField extends NavigableFieldImpl implements RestAwareField, ExportableSystemField {
    private final JiraBaseUrls jiraBaseUrls;

    public WatchesSystemField(VelocityTemplatingEngine templatingEngine, ApplicationProperties applicationProperties, JiraAuthenticationContext authenticationContext, JiraBaseUrls jiraBaseUrls) {
        super(IssueFieldConstants.WATCHES, "issue.field.watch", "issue.column.heading.watch", NavigableField.ORDER_DESCENDING, templatingEngine, applicationProperties, authenticationContext);
        this.jiraBaseUrls = jiraBaseUrls;
    }

    public LuceneFieldSorter getSorter() {
        return WatchesStatisticsMapper.MAPPER;
    }

    public String getColumnViewHtml(FieldLayoutItem fieldLayoutItem, Map displayParams, Issue issue) {
        Map velocityParams = getVelocityParams(fieldLayoutItem, getAuthenticationContext().getI18nHelper(), displayParams, issue);
        velocityParams.put(getId(), issue.getWatches());
        return renderTemplate("watches-columnview.vm", velocityParams);
    }

    @Override
    public FieldTypeInfo getFieldTypeInfo(FieldTypeInfoContext fieldTypeInfoContext) {
        return new FieldTypeInfo(null, null);
    }

    @Override
    public JsonType getJsonSchema() {
        return JsonTypeBuilder.system(JsonType.WATCHES_TYPE, IssueFieldConstants.WATCHES);
    }

    @Override
    public FieldJsonRepresentation getJsonFromIssue(Issue issue, boolean renderedVersionRequired, FieldLayoutItem fieldLayoutItem) {
        WatcherManager watcherManager = ComponentAccessor.getWatcherManager();
        if (watcherManager.isWatchingEnabled()) {
            return new FieldJsonRepresentation(new JsonData(WatchersJsonBean.shortBean(issue.getKey(), issue.getWatches(), watcherManager.isWatching(authenticationContext.getUser(), issue), jiraBaseUrls)));
        }
        return null;
    }

    /**
     * This creates a multi-column representation of the watchers, with value in it being one of the watchers. This
     * is to be compatible with the JIM importer.
     *
     * @param issue to get field representation for
     * @return Exportable representation of the field
     */

    @Override
    public FieldExportParts getRepresentationFromIssue(Issue issue) {
        // we need to call component accessor here to break cyclic dependency
        final WatcherService watcherService = ComponentAccessor.getComponent(WatcherService.class);

        if (watcherService.isWatchingEnabled()) {
            ServiceOutcome<Pair<Integer, List<ApplicationUser>>> watchersData = watcherService.getWatchers(issue, authenticationContext.getLoggedInUser());

            if (watchersData != null && watchersData.get().second() != null) {
                final Stream<String> values = watchersData.get().second().stream()
                        .map(ApplicationUser::getName)
                        .sorted(Collator.getInstance(authenticationContext.getLocale()));
                return FieldExportPartsBuilder.buildSinglePartRepresentation(getId(), getName(), values);
            } else {
                return FieldExportPartsBuilder.buildSinglePartRepresentation(getId(), getName(), empty());
            }
        } else {
            return new FieldExportPartsBuilder().build();
        }
    }
}
