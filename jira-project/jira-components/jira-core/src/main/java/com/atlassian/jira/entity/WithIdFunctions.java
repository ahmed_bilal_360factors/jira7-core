package com.atlassian.jira.entity;

import com.google.common.base.Function;

import javax.annotation.Nullable;

public class WithIdFunctions {
    public static Function<WithId, Long> getId() {
        return new Function<WithId, Long>() {
            @Override
            public Long apply(@Nullable final WithId input) {
                return input != null ? input.getId() : null;
            }
        };
    }
}