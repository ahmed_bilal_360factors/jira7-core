package com.atlassian.jira.web.filters.accesslog;

/**
 * Examines request URLs and parameters, and masks parameter values if they are thought to be passwords.
 *
 * @since 6.4.9
 */
class PasswordMasker {
    private static final String MASKED_PASSWORD_REPLACEMENT = "********";

    private final boolean urlIndicatesParametersMayBePasswords;

    /**
     * Constructs a masker that will be used to examine parameters associated with the given URL.
     *
     * @param requestUrl the request URL that the parameters to be examined are associated with
     */
    PasswordMasker(final String requestUrl) {
        urlIndicatesParametersMayBePasswords = containsPasswordString(requestUrl);
    }

    /**
     * Sanitises a parameter by masking its values if it is thought to be a password.
     * <p>
     * Both the parameter name and the request URL given to {@link this#PasswordMasker(String)}
     * may be used in making this determination.
     *
     * @param parameterName  the name of the parameter
     * @param parameterValue the value associated with the parameter name
     * @return the given parameter value, or masked password string if the parameter is thought to be a password
     */
    String maskIfLikelyToBePassword(final String parameterName, final String parameterValue) {
        if (urlIndicatesParametersMayBePasswords || containsPasswordString(parameterName)) {
            return MASKED_PASSWORD_REPLACEMENT;
        } else {
            return parameterValue;
        }
    }

    private boolean containsPasswordString(String s) {
        return s.toLowerCase().contains("password");
    }
}