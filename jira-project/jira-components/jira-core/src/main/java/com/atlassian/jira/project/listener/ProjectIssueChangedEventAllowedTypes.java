package com.atlassian.jira.project.listener;

import com.atlassian.jira.event.type.EventType;
import com.google.common.collect.ImmutableSet;

import java.util.Set;

public class ProjectIssueChangedEventAllowedTypes {
    public static final Set<Long> eventTypes = ImmutableSet.of(
            EventType.ISSUE_CREATED_ID,
            EventType.ISSUE_UPDATED_ID,
            EventType.ISSUE_ASSIGNED_ID,
            EventType.ISSUE_RESOLVED_ID,
            EventType.ISSUE_CLOSED_ID,
            EventType.ISSUE_REOPENED_ID,
            EventType.ISSUE_DELETED_ID,
            EventType.ISSUE_MOVED_ID,
            EventType.ISSUE_GENERICEVENT_ID
        );
}
