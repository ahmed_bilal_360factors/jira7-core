package com.atlassian.jira.crowd.embedded.ofbiz;

class UserWithDirectoryEntity {
    static final String ENTITY = "UserWithDirectory";
    static final String ACTIVE = "directoryActive";
    static final String POSITION = "position";

    private UserWithDirectoryEntity() {
    }
}
