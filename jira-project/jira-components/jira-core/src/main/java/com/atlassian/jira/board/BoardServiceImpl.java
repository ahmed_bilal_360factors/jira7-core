package com.atlassian.jira.board;

import com.atlassian.jira.bc.ServiceOutcome;
import com.atlassian.jira.bc.ServiceOutcomeImpl;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.bc.issue.search.SearchService.ParseResult;
import com.atlassian.jira.bc.project.ProjectAction;
import com.atlassian.jira.bc.project.ProjectService;
import com.atlassian.jira.board.BoardCreationData.Builder;
import com.atlassian.jira.jql.builder.JqlQueryBuilder;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.ErrorCollections;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.I18nHelper.BeanFactory;
import com.atlassian.jira.util.MessageSet;
import com.atlassian.jira.util.SimpleErrorCollection;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import static com.atlassian.jira.bc.ServiceOutcomeImpl.error;
import static com.atlassian.jira.bc.ServiceOutcomeImpl.ok;
import static com.atlassian.jira.bc.project.ProjectAction.EDIT_PROJECT_CONFIG;
import static com.atlassian.jira.bc.project.ProjectAction.VIEW_PROJECT;
import static com.atlassian.jira.util.ErrorCollection.Reason.FORBIDDEN;
import static com.atlassian.jira.util.ErrorCollection.Reason.NOT_FOUND;

public class BoardServiceImpl implements BoardService {
    private static final String NO_CREATE_SHARED_OBJECTS_PERMISSION = "core.board.service.error.no.create.shared.permission";
    private static final String NO_VIEW_AT_LEAST_ONE_PROJECT_PERMISSION = "core.board.service.error.no.view.board.permission";
    private static final String NO_BOARD_FOUND_FOR_ID = "core.board.service.error.no.board.found.for.id";

    private final GlobalPermissionManager globalPermissionManager;
    private final PermissionManager permissionManager;
    private final BeanFactory i18nFactory;
    private final BoardManager boardManager;
    private final SearchService searchService;
    private final ProjectService projectService;


    public BoardServiceImpl(BoardManager boardManager,
                            GlobalPermissionManager globalPermissionManager,
                            BeanFactory i18nFactory,
                            SearchService searchService,
                            ProjectService projectService,
                            PermissionManager permissionManager) {
        this.boardManager = boardManager;
        this.globalPermissionManager = globalPermissionManager;
        this.i18nFactory = i18nFactory;
        this.searchService = searchService;
        this.projectService = projectService;
        this.permissionManager = permissionManager;
    }

    @Override
    public ServiceOutcome<Board> createDefaultBoardForProject(ApplicationUser user, long projectId) {
        ServiceOutcome<Board> permissionResult = checkProjectPermission(user, projectId, EDIT_PROJECT_CONFIG);
        if (!permissionResult.isValid()) {
            return permissionResult;
        }
        BoardCreationData boardCreationData = new Builder().jql(getJql(projectId)).projectId(projectId).build();
        ErrorCollection jqlParseResult = parseJql(user, boardCreationData);

        if (jqlParseResult.hasAnyErrors()) {
            return new ServiceOutcomeImpl<>(jqlParseResult);
        }
        return ok(boardManager.createBoard(boardCreationData));
    }

    @Override
    public ServiceOutcome<Board> createBoard(ApplicationUser user, @Nonnull BoardCreationData boardCreationData) {
        if (!hasCreateSharedObjectsPermission(user)) {
            return new ServiceOutcomeImpl<>(ErrorCollections.create(i18n(user).getText(NO_CREATE_SHARED_OBJECTS_PERMISSION), FORBIDDEN));
        }
        ErrorCollection jqlValidateResult = parseAndValidateJql(user, boardCreationData);
        if (jqlValidateResult.hasAnyErrors()) {
            return new ServiceOutcomeImpl<>(jqlValidateResult);
        }
        return ok(boardManager.createBoard(boardCreationData));
    }

    @Override
    public ServiceOutcome<List<Board>> getBoardsForProject(ApplicationUser user, long projectId) {
        ServiceOutcome<List<Board>> permissionResult = checkProjectPermission(user, projectId, VIEW_PROJECT);
        if (!permissionResult.isValid()) {
            return permissionResult;
        }
        return ok(boardManager.getBoardsForProject(projectId));
    }

    @Override
    public ServiceOutcome<Boolean> hasBoardInProject(ApplicationUser user, long projectId) {
        ServiceOutcome permissionResult = checkProjectPermission(user, projectId, VIEW_PROJECT);
        if (!permissionResult.isValid()) {
            return ServiceOutcomeImpl.error(permissionResult);
        }
        return ServiceOutcomeImpl.ok(boardManager.hasBoardForProject(projectId));
    }

    @Override
    public ServiceOutcome<Board> getBoard(ApplicationUser user, @Nonnull BoardId boardId) {
        if (!permissionManager.hasProjects(ProjectPermissions.BROWSE_PROJECTS, user)) {
            return new ServiceOutcomeImpl<>(ErrorCollections.create(i18n(user).getText(NO_VIEW_AT_LEAST_ONE_PROJECT_PERMISSION), FORBIDDEN));
        }

        Optional<Board> boardOptional = boardManager.getBoard(boardId);
        return boardOptional.map(board -> ok(board))
                .orElse(error(i18n(user).getText(NO_BOARD_FOUND_FOR_ID, boardId.getId()), NOT_FOUND));
    }

    @Override
    public ServiceOutcome<Boolean> deleteBoard(ApplicationUser user, @Nonnull BoardId boardId) {
        if (!hasCreateSharedObjectsPermission(user)) {
            ErrorCollection error = ErrorCollections.create(i18n(user).getText(NO_CREATE_SHARED_OBJECTS_PERMISSION), FORBIDDEN);
            return new ServiceOutcomeImpl<>(error);
        }
        return ok(boardManager.deleteBoard(boardId));
    }

    private ErrorCollection parseJql(ApplicationUser user, BoardCreationData boardCreationData) {
        ParseResult queryParseResult = searchService.parseQuery(user, boardCreationData.getJql());

        if (!queryParseResult.isValid()) {
            return errorCollection(queryParseResult.getErrors().getErrorMessages());
        }
        return new SimpleErrorCollection();
    }

    private ErrorCollection parseAndValidateJql(ApplicationUser user, BoardCreationData boardCreationData) {
        ParseResult queryParseResult = searchService.parseQuery(user, boardCreationData.getJql());

        if (!queryParseResult.isValid()) {
            return errorCollection(queryParseResult.getErrors().getErrorMessages());
        } else {
            MessageSet validateResult = searchService.validateQuery(user, queryParseResult.getQuery());
            if (validateResult.hasAnyErrors()) {
                return errorCollection(validateResult.getErrorMessages());
            }
        }
        return new SimpleErrorCollection();
    }

    private ServiceOutcome checkProjectPermission(ApplicationUser user, long projectId, ProjectAction projectAction) {
        final ProjectService.GetProjectResult getProjectResult = projectService.getProjectByIdForAction(user, projectId, projectAction);
        if (getProjectResult.isValid()) {
            return new ServiceOutcomeImpl<>(new SimpleErrorCollection());
        }
        return new ServiceOutcomeImpl<>(getProjectResult.getErrorCollection());
    }

    private String getJql(long projectId) {
        return searchService.getJqlString(JqlQueryBuilder.newBuilder()
                .where()
                .project(projectId)
                .endWhere()
                .buildQuery()
        );
    }

    private ErrorCollection errorCollection(Collection<String> messages) {
        ErrorCollection errorCollection = new SimpleErrorCollection();
        errorCollection.addErrorMessages(messages);
        return errorCollection;
    }

    private boolean hasCreateSharedObjectsPermission(ApplicationUser user) {
        return globalPermissionManager.hasPermission(GlobalPermissionKey.CREATE_SHARED_OBJECTS, user);
    }

    private I18nHelper i18n(ApplicationUser user) {
        return i18nFactory.getInstance(user);
    }
}
