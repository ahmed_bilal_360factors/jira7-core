package com.atlassian.jira.entity;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.link.IssueLink;
import com.atlassian.jira.issue.link.IssueLinkImpl;
import com.atlassian.jira.issue.link.IssueLinkTypeManager;
import com.atlassian.jira.ofbiz.FieldMap;
import org.ofbiz.core.entity.GenericValue;

/**
 * IssueLink EntityFactory
 *
 * @since v5.0
 */
public class IssueLinkFactory extends AbstractEntityFactory<IssueLink> {
    public static final String ID = "id";
    public static final String LINK_TYPE = "linktype";
    public static final String SOURCE = "source";
    public static final String DESTINATION = "destination";
    public static final String SEQUENCE = "sequence";

    @Override
    public String getEntityName() {
        return "IssueLink";
    }

    @Override
    public IssueLink build(final GenericValue genericValue) {
        if (genericValue == null) {
            return null;
        }

        return new IssueLinkImpl(genericValue, ComponentAccessor.getComponent(IssueLinkTypeManager.class), ComponentAccessor.getIssueManager());
    }

    @Override
    public FieldMap fieldMapFrom(final IssueLink value) {
        return new FieldMap(ID, value.getId())
                .add(LINK_TYPE, value.getLinkTypeId())
                .add(SOURCE, value.getSourceId())
                .add(DESTINATION, value.getDestinationId())
                .add(SEQUENCE, value.getSequence());
    }
}
