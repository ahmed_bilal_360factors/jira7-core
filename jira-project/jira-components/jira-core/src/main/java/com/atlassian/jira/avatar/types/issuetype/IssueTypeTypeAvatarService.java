package com.atlassian.jira.avatar.types.issuetype;

import com.atlassian.jira.avatar.AvatarManager;
import com.atlassian.jira.avatar.types.BasicTypedTypeAvatarService;
import com.atlassian.jira.icon.IconType;
import com.atlassian.jira.plugin.icon.IssueTypeIconTypeDefinition;

public class IssueTypeTypeAvatarService extends BasicTypedTypeAvatarService {
    public IssueTypeTypeAvatarService(final AvatarManager avatarManager, final IssueTypeIconTypeDefinition issueTypeIconType) {
        super(IconType.ISSUE_TYPE_ICON_TYPE, avatarManager);
    }
}
