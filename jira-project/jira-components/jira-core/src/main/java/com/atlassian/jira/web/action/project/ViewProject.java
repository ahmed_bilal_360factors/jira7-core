package com.atlassian.jira.web.action.project;


import com.atlassian.jira.action.component.ComponentUtils;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectAssigneeTypes;
import org.ofbiz.core.entity.GenericEntityException;

import static com.atlassian.jira.permission.ProjectPermissions.ADMINISTER_PROJECTS;
import static com.atlassian.jira.security.Permissions.ADMINISTER;

public class ViewProject extends AbstractProjectAction {
    Long pid;
    Project project;

    public String doDefault() throws Exception {
        final Project projectObject = getProjectObject();
        if (projectObject != null) {
            return getRedirect("/plugins/servlet/project-config/" + projectObject.getKey() + "/summary");
        }

        return getRedirect("/plugins/servlet/project-config/" + "UNKNOWN" + "/summary");

    }

    protected void doValidation() {
        // no errors as we are just redirecting
    }

    protected String doExecute() throws Exception {
        final Project projectObject = getProjectObject();
        if (projectObject != null) {
            return getRedirect("/plugins/servlet/project-config/" + projectObject.getKey() + "/summary");
        }

        return getRedirect("/plugins/servlet/project-config/" + "UNKNOWN" + "/summary");
    }

    public boolean hasProjectAdminPermission() {
        return hasAdminPermission() ||
                ComponentAccessor.getPermissionManager().hasPermission(ADMINISTER_PROJECTS, getProjectObject(), getLoggedInUser());
    }

    public boolean hasAdminPermission() {
        return ComponentAccessor.getPermissionManager().hasPermission(ADMINISTER, getLoggedInUser());
    }

    public boolean hasAssociateRolesPermission() throws Exception {
        return hasAdminPermission() || (hasProjectAdminPermission());
    }

    public Project getProjectObject() {
        if (project == null) {
            if (getPid() != null) {
                project = getProjectManager().getProjectObj(getPid());
            } else if (getKey() != null) {
                project = getProjectManager().getProjectObjByKey(getKey());
                if (project != null) {
                    setPid(project.getId());
                }
            }
        }
        return project;
    }

    public boolean isDefaultAssigneeAssignable() throws GenericEntityException {
        Long assigneeType = getProjectObject().getAssigneeType();
        if (assigneeType != null && ProjectAssigneeTypes.PROJECT_LEAD == assigneeType) {
            return ComponentUtils.isProjectLeadAssignable(getProjectObject());
        } else {
            return true;
        }
    }

    public void setPid(Long pid) {
        this.pid = pid;
    }

    public Long getPid() {
        return pid;
    }

}
