package com.atlassian.jira.issue.util;

import com.atlassian.jira.entity.property.EntityProperty;
import com.atlassian.jira.entity.property.EntityPropertyType;
import com.atlassian.jira.entity.property.JsonEntityPropertyManager;
import com.atlassian.jira.event.entity.EntityPropertySetEvent;
import com.atlassian.jira.event.issue.property.IssuePropertySetEvent;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.InjectableComponent;
import com.atlassian.jira.util.json.JSONObject;
import org.codehaus.jackson.JsonNode;

import java.util.Map;
import java.util.function.BiFunction;

/**
 * This helper component allows issue create / update to "inline" set issue properties
 * at the time of the operation (versus afterwards via the Issue Properties API)
 */
@InjectableComponent
public class InlineIssuePropertySetter {

    private final JsonEntityPropertyManager jsonEntityPropertyManager;

    public InlineIssuePropertySetter(JsonEntityPropertyManager jsonEntityPropertyManager) {
        this.jsonEntityPropertyManager = jsonEntityPropertyManager;
    }

    /**
     * Called to set issue properties on an issue from the "inside"
     *
     * @param user          the user in play
     * @param issueId       the id of the issue in play
     * @param properties    the map of issue properties to set
     * @param dispatchEvent whether an event should be dispatched
     */

    public void setIssueProperties(ApplicationUser user, Long issueId, Map<String, JsonNode> properties, boolean dispatchEvent) {

        // we don't dispatch events since we are inside the issue update and bound by its event boundaries.
        final BiFunction<ApplicationUser, EntityProperty, ? extends EntityPropertySetEvent> eventCreatorFunction = (applicationUser, entityProperty) -> new IssuePropertySetEvent(entityProperty, applicationUser);

        properties.forEach((key, jsonObject) -> {
            // toString is defined in Jackson to mean a valid JSON representation of the node
            String jsonValue = jsonObject.toString();
            //
            // we go straight to lower level entity manager so we can control event boundaries
            jsonEntityPropertyManager.put(user, EntityPropertyType.ISSUE_PROPERTY.getDbEntityName(), issueId, key, jsonValue, eventCreatorFunction, dispatchEvent);
        });
    }
}
