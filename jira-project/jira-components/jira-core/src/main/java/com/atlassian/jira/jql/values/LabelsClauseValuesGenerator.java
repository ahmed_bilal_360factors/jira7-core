package com.atlassian.jira.jql.values;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.customfields.CustomFieldUtils;
import com.atlassian.jira.issue.label.LabelManager;
import com.atlassian.jira.issue.search.constants.SystemSearchConstants;
import com.atlassian.jira.issue.search.managers.SearchHandlerManager;
import com.atlassian.jira.user.ApplicationUser;

import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Generates completions to be used in the JQL autocomplete.  Possible values will be combined for all matching
 * labels fields. (Custom fields with the same name)
 *
 * @since v4.2
 */
public class LabelsClauseValuesGenerator implements ClauseValuesGenerator {
    private final LabelManager labelManager;

    public LabelsClauseValuesGenerator(final LabelManager labelManager) {
        this.labelManager = labelManager;
    }

    public Results getPossibleValues(final ApplicationUser user, final String jqlClauseName, final String valuePrefix, final int maxNumResults) {
        return new Results(
                getSearchHandlerManager()
                        .getFieldIds(user, jqlClauseName)
                        .stream()
                        .flatMap(fieldId -> getSuggestionsByFieldId(fieldId, valuePrefix, user))
                        .limit(maxNumResults)
                        .map(Result::new)
                        .collect(Collectors.toList())
        );
    }

    private Stream<String> getSuggestionsByFieldId(final String fieldId, final String valuePrefix, final ApplicationUser user) {
        if (isCustomField(fieldId)) {
            return labelManager.getSuggestedLabels(user, null, CustomFieldUtils.getCustomFieldId(fieldId), valuePrefix).stream();
        } else {
            return labelManager.getSuggestedLabels(user, null, valuePrefix).stream();
        }
    }

    private boolean isCustomField(final String fieldId) {
        return !fieldId.equals(SystemSearchConstants.forLabels().getFieldId());
    }

    //can't be injected due to circular deps
    SearchHandlerManager getSearchHandlerManager() {
        return ComponentAccessor.getComponentOfType(SearchHandlerManager.class);
    }
}
