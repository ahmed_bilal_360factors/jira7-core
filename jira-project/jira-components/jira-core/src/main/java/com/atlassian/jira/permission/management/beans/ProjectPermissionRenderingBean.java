package com.atlassian.jira.permission.management.beans;

import com.google.common.base.Objects;
import com.google.common.collect.ImmutableList;
import org.codehaus.jackson.annotate.JsonAutoDetect;

import java.util.List;

@JsonAutoDetect
public class ProjectPermissionRenderingBean {
    private Boolean readOnly;
    private List<ProjectPermissionRenderingSectionBean> grouping;

    public ProjectPermissionRenderingBean() {
    }

    public ProjectPermissionRenderingBean(Boolean readOnly, Iterable<ProjectPermissionRenderingSectionBean> grouping) {
        this.readOnly = readOnly;
        this.grouping = grouping != null ? ImmutableList.copyOf(grouping) : null;
    }

    public Boolean getReadOnly() {
        return readOnly;
    }

    public void setReadOnly(Boolean readOnly) {
        this.readOnly = readOnly;
    }

    public List<ProjectPermissionRenderingSectionBean> getGrouping() {
        return grouping;
    }

    public void setGrouping(List<ProjectPermissionRenderingSectionBean> grouping) {
        this.grouping = grouping != null ? ImmutableList.copyOf(grouping) : null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ProjectPermissionRenderingBean that = (ProjectPermissionRenderingBean) o;

        return Objects.equal(this.readOnly, that.readOnly) &&
                Objects.equal(this.grouping, that.grouping);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(readOnly, grouping);
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("readOnly", readOnly)
                .add("grouping", grouping)
                .toString();
    }
}
