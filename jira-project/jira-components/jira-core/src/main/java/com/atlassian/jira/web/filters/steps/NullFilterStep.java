package com.atlassian.jira.web.filters.steps;

/**
 * A filter step that doesn't do anything at all.
 * This is meant to act as a placeholder for a filter step that is being skipped for some reason.
 *
 * @since v7.0.0
 */
public class NullFilterStep implements FilterStep {
    @Override
    public FilterCallContext beforeDoFilter(final FilterCallContext callContext) {
        return callContext;
    }

    @Override
    public FilterCallContext finallyAfterDoFilter(final FilterCallContext callContext) {
        return callContext;
    }
}
