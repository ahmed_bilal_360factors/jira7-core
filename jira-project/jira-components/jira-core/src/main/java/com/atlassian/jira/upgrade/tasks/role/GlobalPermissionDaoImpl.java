package com.atlassian.jira.upgrade.tasks.role;

import com.atlassian.collectors.CollectorsUtil;
import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.crowd.embedded.impl.ImmutableGroup;
import com.atlassian.fugue.Option;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.google.common.base.Supplier;
import com.google.common.base.Suppliers;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Multimap;
import org.ofbiz.core.entity.EntityExpr;
import org.ofbiz.core.entity.EntityOperator;
import org.ofbiz.core.entity.GenericValue;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * @since v7.0
 */
final class GlobalPermissionDaoImpl extends GlobalPermissionDao {
    private static final String AGENT_GLOBAL_PERMISSION_NAME = "com.atlassian.servicedesk.agent.access";

    private final OfBizDelegator db;
    private final Supplier<Set<Group>> useGroups;
    private final Supplier<Multimap<Group, AdminPermission>> adminGroups;
    private final Supplier<Set<Group>> sdAgentGroups;

    GlobalPermissionDaoImpl(final OfBizDelegator db) {
        this.db = notNull("db", db);
        this.useGroups = Suppliers.memoize(() -> getGlobalGroups("USE"));
        this.adminGroups = Suppliers.memoize(this::getAdminGroups);
        this.sdAgentGroups = Suppliers.memoize(() -> getGlobalGroups(AGENT_GLOBAL_PERMISSION_NAME));
    }

    @Override
    Set<Group> groupsWithUsePermission() {
        return useGroups.get();
    }

    @Override
    Set<Group> groupsWithAdminPermission() {
        return adminGroups.get().keySet();
    }

    @Override
    Multimap<Group, AdminPermission> groupsWithAdminPermissionAndPermissionType() {
        return adminGroups.get();
    }

    @Override
    Set<Group> groupsWithSdAgentPermission() {
        return sdAgentGroups.get();
    }

    private ImmutableSet<Group> getGlobalGroups(final String permission) {
        //noinspection ConstantConditions
        return db.findByAnd("GlobalPermissionEntry", ImmutableMap.of("permission", permission))
                .stream()
                .map(gv -> gv.getString("group_id"))
                .filter(Objects::nonNull)
                .map(ImmutableGroup::new)
                .collect(CollectorsUtil.toImmutableSet());
    }

    private Multimap<Group, AdminPermission> getAdminGroups() {
        List<EntityExpr> entityExpressions = new ArrayList<>();
        entityExpressions.add(new EntityExpr("permission", EntityOperator.EQUALS, AdminPermission.ADMIN.ofBizKey()));
        entityExpressions.add(new EntityExpr("permission", EntityOperator.EQUALS, AdminPermission.SYSADMIN.ofBizKey()));

        HashMultimap<Group, AdminPermission> result = HashMultimap.create();
        List<GenericValue> adminGroupValues = db.findByOr("GlobalPermissionEntry", entityExpressions, ImmutableList.of("permission"));
        for (GenericValue gv : adminGroupValues) {
            String groupName = gv.getString("group_id");
            String permissionKey = gv.getString("permission");
            if (groupName != null && permissionKey != null) {
                Option<AdminPermission> permission = AdminPermission.from(permissionKey);

                if (permission.isDefined()) {
                    ImmutableGroup group = new ImmutableGroup(groupName);
                    result.put(group, permission.get());
                }
            }
        }

        return ImmutableMultimap.copyOf(result);
    }
}
