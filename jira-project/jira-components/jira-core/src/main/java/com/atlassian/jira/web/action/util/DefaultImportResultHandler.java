package com.atlassian.jira.web.action.util;

import com.atlassian.jira.bc.dataimport.DataImportService;
import com.atlassian.jira.license.LicenseJohnsonEventRaiser;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.johnson.JohnsonProvider;
import com.atlassian.johnson.JohnsonEventContainer;
import com.atlassian.johnson.event.Event;
import com.atlassian.johnson.event.EventLevel;
import com.atlassian.johnson.event.EventType;

/**
 * An error handler to be used both by {@link com.atlassian.jira.web.action.setup.SetupImport} and {@link ImportAll} for
 * some consistent error handling.
 *
 * @since v4.4
 */
public class DefaultImportResultHandler implements ImportResultHandler {
    private final LicenseJohnsonEventRaiser licenseJohnsonEventRaiser;
    private final JohnsonProvider johnsonProvider;

    public DefaultImportResultHandler(final LicenseJohnsonEventRaiser licenseJohnsonEventRaiser,
                                      final JohnsonProvider johnsonProvider) {
        this.licenseJohnsonEventRaiser = licenseJohnsonEventRaiser;
        this.johnsonProvider = johnsonProvider;
    }

    public boolean handleErrorResult(DataImportService.ImportResult lastResult, I18nHelper i18n,
                                     ErrorCollection errorCollection) {
        switch (lastResult.getImportError()) {
            case UPGRADE_EXCEPTION:
                final JohnsonEventContainer eventCont = johnsonProvider.getContainer();
                final Event errorEvent = new Event(EventType.get("upgrade"),
                        "An error occurred performing JIRA upgrade task", lastResult.getSpecificErrorMessage(),
                        EventLevel.get(EventLevel.ERROR));

                if (eventCont != null) {
                    // Use licenseJohnsonEventRaiser to check for specific license errors; if it's not a Johnson problem
                    // report it normally.
                    if (!licenseJohnsonEventRaiser.checkLicenseIsTooOldForBuild()) {
                        eventCont.addEvent(errorEvent);
                    }
                }
                return true;
            case CUSTOM_PATH_EXCEPTION:
                errorCollection.addErrorMessage(i18n.getText("admin.errors.custom.path", "<a id=\"reimport\" href=\"#\">", "</a>"));
                return false;
            case DOWNGRADE_FROM_ONDEMAND:
                errorCollection.addErrorMessage(i18n.getText("admin.errors.import.downgrade.error", lastResult.getSpecificErrorMessage(), "<a id='acknowledgeDowngradeError' href='#'>", "</a>"));
                return false;
            case FAILED_VALIDATION:
                errorCollection.addErrorMessages(lastResult.getValidationErrorMessages());
                return false;
            default:
                return !lastResult.getErrorCollection().hasAnyErrors() && checkLicenseIsInvalidOrTooOldForBuild();
        }
    }

    private boolean checkLicenseIsInvalidOrTooOldForBuild() {
        return licenseJohnsonEventRaiser.checkLicenseIsTooOldForBuild();
    }
}
