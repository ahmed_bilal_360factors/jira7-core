package com.atlassian.jira.upgrade;

import com.atlassian.beehive.ClusterLock;
import com.atlassian.beehive.ClusterLockService;
import com.atlassian.event.api.EventListener;
import com.atlassian.jira.InitializingComponent;
import com.atlassian.jira.cluster.zdu.ClusterUpgradeStateManager;
import com.atlassian.jira.cluster.zdu.ClusterUpgradeStateManager;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.event.events.PluginEnabledEvent;
import com.atlassian.upgrade.api.UpgradeContext;
import com.atlassian.upgrade.core.UpgradeTaskManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.atlassian.upgrade.api.UpgradeContext.UpgradeTrigger.PROVISIONING;
import static com.atlassian.upgrade.api.UpgradeContext.UpgradeTrigger.UPGRADE;

public class PluginUpgradeService implements InitializingComponent {

    private static final Logger LOGGER = LoggerFactory.getLogger(PluginUpgradeService.class);

    private final UpgradeTaskManager upgradeTaskManager;
    private final ClusterLockService clusterLockService;
    private final PluginEventManager pluginEventManager;
    private final ApplicationProperties applicationProperties;
    private final ClusterUpgradeStateManager clusterUpgradeStateManager;

    public PluginUpgradeService(
            final UpgradeTaskManager upgradeTaskManager,
            final ClusterLockService clusterLockService,
            final PluginEventManager pluginEventManager,
            final ApplicationProperties applicationProperties,
            final ClusterUpgradeStateManager clusterUpgradeStateManager
    ) {
        this.upgradeTaskManager = upgradeTaskManager;
        this.clusterLockService = clusterLockService;
        this.pluginEventManager = pluginEventManager;
        this.applicationProperties = applicationProperties;
        this.clusterUpgradeStateManager = clusterUpgradeStateManager;
    }

    public void upgradePlugins() {
        final ClusterLock lock = clusterLockService.getLockForName(PluginUpgradeService.class.getName());

        if (lock.tryLock()) {
            try {
                LOGGER.debug("Upgrading plugins");
                upgradeTaskManager.upgradePlugins(() -> UPGRADE);
            } finally {
                lock.unlock();
            }
        } else {
            LOGGER.warn("Plugin upgrades are being performed by other node");
        }
    }

    @EventListener
    public void onPluginInstall(final PluginEnabledEvent event) {
        final Plugin plugin = event.getPlugin();
        final String pluginKey = plugin.getKey();

        if (clusterUpgradeStateManager.areDelayedUpgradesHandledByCluster()) {
            LOGGER.debug("Plugin not upgraded, waiting for ZDU to handle plugin upgrades.");
            return;
        }

        upgradePlugin(pluginKey);
    }

    private void upgradePlugin(final String pluginKey) {
        final String lockName = "plugin.upgrade." + pluginKey;
        final ClusterLock lock = clusterLockService.getLockForName(lockName);

        if (lock.tryLock()) {
            try {
                LOGGER.debug("Upgrading " + pluginKey);
                upgradeTaskManager.upgradePlugin(this::getUpgradeTrigger, pluginKey);
            } finally {
                lock.unlock();
            }
        } else {
            LOGGER.warn("{} plugin upgrades are being performed by other node", pluginKey);
        }
    }

    private UpgradeContext.UpgradeTrigger getUpgradeTrigger() {
        if (isJiraSetup()) {
            return UPGRADE;
        } else {
            return PROVISIONING;
        }
    }

    private boolean isJiraSetup() {
        return "true".equals(applicationProperties.getString(APKeys.JIRA_SETUP));
    }

    @Override
    public void afterInstantiation() throws Exception {
        pluginEventManager.register(this);
    }
}
