package com.atlassian.jira.crowd.embedded.ofbiz;

import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.embedded.spi.UserDao;
import com.atlassian.crowd.exception.DirectoryNotFoundException;
import com.atlassian.crowd.exception.UserAlreadyExistsException;
import com.atlassian.crowd.exception.UserNotFoundException;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.model.user.UserTemplateWithCredentialAndAttributes;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.util.BatchResult;
import com.atlassian.event.api.EventListener;
import com.atlassian.jira.EventComponent;
import com.atlassian.jira.bc.user.search.UserId;
import com.atlassian.jira.bc.user.search.UserIndexer;
import com.atlassian.jira.cluster.ClusterMessageConsumer;
import com.atlassian.jira.cluster.ClusterMessagingService;
import com.atlassian.jira.crowd.embedded.EventuallyConsistentQuery;
import com.atlassian.jira.crowd.embedded.lucene.CrowdQueryTranslator;
import com.atlassian.jira.crowd.embedded.ofbiz.db.OfBizTransactionManager;
import com.atlassian.jira.extension.JiraStartedEvent;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Stopwatch;
import com.google.common.collect.Lists;
import org.apache.lucene.search.Sort;
import org.apache.lucene.search.SortField;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.StringJoiner;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.atlassian.jira.bc.user.search.UserIndexer.USER_NAME;

/**
 * Optimizes full text search.
 * Some queries might be unsuitable for the optimization. In such cases it falls back to the unoptimized search.
 * It's cluster-safe with the caveat of eventual consistency.
 */
@EventComponent
public class IndexedUserDao extends DelegatingUserDao {

    private static final Logger LOG = LoggerFactory.getLogger(IndexedUserDao.class);

    private static final String USER_INDEX_CHANNEL = "jira.UserIndex";

    private final ExtendedUserDao dao;
    private final UserIndexer indexer;
    private final CrowdQueryTranslator translator;
    private final ClusterMessagingService clusterMessagingService;
    private final OfBizTransactionManager ofBizTransactionManager;
    private final int maxBatchSize;

    /**
     * Registering consumer does not stop it being GCed so keep a strong reference via field.
     *
     * @see <a href="https://developer.atlassian.com/jiradev/jira-platform/jira-data-center/plugin-guide-to-jira-high-availability-and-clustering">Plugin Guide to JIRA High Availability and Clustering</a>
     */
    @SuppressWarnings("FieldCanBeLocal")
    private final InvalidationClusterMessageConsumer invalidationClusterMessageConsumer
            = new InvalidationClusterMessageConsumer();

    /**
     * Lock around multiple indexer calls that need to be performed one-after-another and in-step with the database.
     */
    private final Object indexLock = new Object();

    public IndexedUserDao(
            ExtendedUserDao dao,
            UserIndexer indexer,
            CrowdQueryTranslator translator,
            ClusterMessagingService clusterMessagingService,
            OfBizTransactionManager ofBizTransactionManager,
            int maxBatchSize
    ) {
        this.dao = dao;
        this.indexer = indexer;
        this.translator = translator;
        this.clusterMessagingService = clusterMessagingService;
        this.ofBizTransactionManager = ofBizTransactionManager;
        this.maxBatchSize = maxBatchSize;
    }

    @Override
    protected final ExtendedUserDao delegate() {
        return dao;
    }

    @VisibleForTesting
    static <T> BatchResult<T> combineBatchResults(Collection<BatchResult<T>> individualResults) {
        int totalCount = individualResults.stream().mapToInt(BatchResult::getTotalAttempted).sum();
        BatchResult<T> combinedResult = new BatchResult<>(totalCount);
        for (BatchResult<T> individualResult : individualResults) {
            combinedResult.addSuccesses(individualResult.getSuccessfulEntities());
            combinedResult.addFailures(individualResult.getFailedEntities());
        }
        return combinedResult;
    }

    /**
     * Splits a larger set into many smaller sets, each smaller set having at most <code>size</code> elements.
     * All elements in the original set will be present in one of the smaller sets.
     *
     * @param original the original set to split.
     * @param size     the maximum size of each smaller set.
     * @return a list of smaller sets.
     */
    @VisibleForTesting
    static <T> List<? extends Set<T>> partitionSet(Set<T> original, int size) {
        List<List<T>> partitionList = Lists.partition(new ArrayList<>(original), size);
        return partitionList.stream().map(LinkedHashSet::new).collect(Collectors.toList());
    }

    @Override
    public User add(User user, PasswordCredential credential)
            throws UserAlreadyExistsException, IllegalArgumentException, DirectoryNotFoundException {
        synchronized (indexLock) {
            try {
                User addedUser = super.add(user, credential);
                OfBizUser addedOfBizUser = toOfBizUser(addedUser);

                indexer.index(addedOfBizUser);

                invalidateUsersOnOtherNodes(addedOfBizUser);

                return addedUser;
            } catch (Throwable t) {
                reindexFromScratch(user);
                throw t;
            }
        }
    }

    @Override
    public BatchResult<User> addAll(Set<UserTemplateWithCredentialAndAttributes> users) {

        //If we have a large user count, then break operation into batches
        if (users.size() > maxBatchSize) {
            List<BatchResult<User>> results = new ArrayList<>();
            for (Set<UserTemplateWithCredentialAndAttributes> partition : partitionSet(users, maxBatchSize)) {
                BatchResult<User> curResult = addAll(partition);
                results.add(curResult);
            }
            return combineBatchResults(results);
        }

        //Otherwise we just process all the users in the list
        synchronized (indexLock) {
            try {
                //Add all users to the database
                BatchResult<User> addResult = super.addAll(users);
                Collection<OfBizUser> addedUsers = addResult.getSuccessfulEntities().stream()
                        .map(this::toOfBizUser)
                        .collect(Collectors.toList());

                //Add the new users as read by the database into the index
                indexer.index(addedUsers.toArray(new OfBizUser[addedUsers.size()]));

                Set<Long> userIds = addedUsers.stream().map(OfBizUser::getId).collect(Collectors.toSet());
                invalidateUsersOnOtherNodes(userIds);

                return addResult;
            } catch (Throwable t) {
                for (UserTemplateWithCredentialAndAttributes user : users) {
                    reindexFromScratch(user);
                }
                throw t;
            }
        }
    }

    private void invalidateUsersOnOtherNodes(OfBizUser... users) {
        invalidateUsersOnOtherNodes(Stream.of(users).filter(Objects::nonNull).map(OfBizUser::getId).toArray(Long[]::new));
    }

    private void invalidateUsersOnOtherNodes(Long... internalIds) {
        invalidateUsersOnOtherNodes(new LinkedHashSet<>(Arrays.asList(internalIds)));
    }

    private void invalidateUserOnOtherNodes(OfBizUser user) {
        invalidateUserOnOtherNodes(user.getId());
    }

    private void invalidateUserOnOtherNodes(Long internalId) {
        invalidateUsersOnOtherNodes(internalId);
    }

    private void invalidateUsersOnOtherNodes(Collection<Long> internalIds) {

        //Batch these into groups of ten to make sure we are under 200 character limit
        if (internalIds.size() > 10) {
            for (Set<Long> curPartition : partitionSet(new HashSet<>(internalIds), 10)) {
                invalidateUsersOnOtherNodes(curPartition);
            }
            return;
        }

        //Use user ID because user name can get bigger than what cluster messages allow (200 characters)
        StringJoiner joiner = new StringJoiner(" ");
        for (Long internalId : internalIds) {
            joiner.add(String.valueOf(internalId));
        }
        clusterMessagingService.sendRemote(USER_INDEX_CHANNEL, joiner.toString());
    }

    private void reindexFromScratch(User user) {
        reindexFromScratch(user, user.getName());
    }

    private void reindexFromScratch(User user, String actualName) {
        reindexFromScratch(user.getDirectoryId(), actualName);
    }

    /**
     * Called in failure conditions to reindex a user from scratch by re-reading from database.
     * Any exceptions will be logged and not rethrown.
     */
    private void reindexFromScratch(long directoryId, String name) {
        try {
            UserId id = new UserId(name, directoryId);
            indexer.deindex(id);
            OfBizUser dbUser = findByNameOrNull(directoryId, name);
            if (dbUser != null) {
                indexer.index(dbUser);
                invalidateUsersOnOtherNodes(dbUser);
            }
        } catch (Exception e) {
            LOG.error("Error reindexing user from scratch (" + directoryId + ", " + name + ").  User index may be inconsistent.", e);
        }
    }

    @Override
    public User rename(User user, String newName) throws UserNotFoundException, UserAlreadyExistsException, IllegalArgumentException {
        synchronized (indexLock) {
            try {
                OfBizUser renamedUser = toOfBizUser(super.rename(user, newName));
                indexer.deindex(extractId(user));
                indexer.index(renamedUser);
                invalidateUsersOnOtherNodes(renamedUser);
                return renamedUser;
            } catch (Throwable t) {
                reindexFromScratch(user);
                reindexFromScratch(user, newName);
                throw t;
            }
        }
    }

    @Override
    public User update(User user) throws UserNotFoundException, IllegalArgumentException {
        //Updated users never change directory so we don't have to recalculate shadowing here
        synchronized (indexLock) {
            try {
                OfBizUser updatedUser = toOfBizUser(super.update(user));
                indexer.deindex(extractId(user));
                indexer.index(updatedUser);
                invalidateUserOnOtherNodes(updatedUser);
                return updatedUser;
            } catch (Throwable t) {
                reindexFromScratch(user);
                throw t;
            }
        }
    }

    private UserId extractId(User user) {
        return new UserId(user.getName(), user.getDirectoryId());
    }

    @Override
    public void remove(User user) throws UserNotFoundException {
        synchronized (indexLock) {
            try {
                OfBizUser ofBizUser = toOfBizUser(user);
                super.remove(user);
                indexer.deindex(extractId(user));

                //Delete may have been vetoed at DAO level so read back the user from underlying store and index that
                //if it exists
                OfBizUser afterDeleteUser = super.findById(ofBizUser.getId());
                if (afterDeleteUser != null) {
                    indexer.index(afterDeleteUser);
                }

                invalidateUsersOnOtherNodes(ofBizUser);
            } catch (Throwable t) {
                reindexFromScratch(user);
                throw t;
            }
        }
    }

    @Override
    public BatchResult<String> removeAllUsers(long directoryId, Set<String> userNames) {

        //If we have a large user count, then break operation into batches
        if (userNames.size() > maxBatchSize) {
            List<BatchResult<String>> results = new ArrayList<>();
            for (Set<String> partition : partitionSet(userNames, maxBatchSize)) {
                BatchResult<String> curResult = removeAllUsers(directoryId, partition);
                results.add(curResult);
            }
            return combineBatchResults(results);
        }

        synchronized (indexLock) {
            try {
                List<OfBizUser> usersToDelete = userNames
                        .stream()
                        .map(userName -> toOfBizUser(findByNameOrNull(directoryId, userName)))
                        .filter(user -> user != null)
                        .collect(Collectors.toList());
                BatchResult<String> removeResult = super.removeAllUsers(directoryId, userNames);
                List<String> removedUserNames = removeResult.getSuccessfulEntities();
                UserId[] userIds = removedUserNames.stream()
                        .map(userName -> new UserId(userName, directoryId))
                        .toArray(UserId[]::new);
                indexer.deindex(userIds);

                //Delete may have been vetoed at DAO level so read back the user from underlying store and index that
                //if it exists
                List<OfBizUser> usersExistingAfterDelete = new ArrayList<>();
                for (OfBizUser userToDelete : usersToDelete) {
                    OfBizUser afterDeleteUser = super.findById(userToDelete.getId());
                    if (afterDeleteUser != null) {
                        usersExistingAfterDelete.add(afterDeleteUser);
                    }
                }
                if (!usersExistingAfterDelete.isEmpty()) {
                    indexer.index(usersExistingAfterDelete.toArray(new OfBizUser[usersExistingAfterDelete.size()]));
                }

                invalidateUsersOnOtherNodes(
                        usersToDelete
                                .stream()
                                .filter(user -> removedUserNames.contains(user.getName()))
                                .toArray(OfBizUser[]::new)
                );

                return removeResult;
            } catch (Throwable t) {
                for (String userName : userNames) {
                    reindexFromScratch(directoryId, userName);
                }
                throw t;
            }
        }
    }

    @Override
    public <T> List<T> search(long directoryId, EntityQuery<T> query) {
        if (query instanceof EventuallyConsistentQuery) {
            return trySearching(directoryId, query)
                    .orElseGet(() -> super.search(directoryId, query));
        } else {
            return super.search(directoryId, query);
        }
    }

    private <T> Optional<List<T>> trySearching(long directoryId, EntityQuery<T> query) {
        return translator.tryQuerying(directoryId, query)
                .flatMap(usersInDirectory -> {
                    Sort sort = new Sort(new SortField(USER_NAME, SortField.STRING));
                    Supplier<List<com.atlassian.crowd.embedded.api.User>> users = () -> indexer.search(
                            usersInDirectory,
                            query.getStartIndex(),
                            query.getMaxResults(),
                            sort
                    );
                    return tryMapping(users, query.getReturnType());
                });
    }

    private <T> Optional<List<T>> tryMapping(
            Supplier<List<com.atlassian.crowd.embedded.api.User>> users,
            Class<T> returnType
    ) {
        if (returnType.isAssignableFrom(com.atlassian.crowd.embedded.api.User.class)) {
            return Optional.of(cast(users.get(), returnType));
        } else if (returnType.isAssignableFrom(String.class)) {
            List<String> userNames = users
                    .get()
                    .stream()
                    .map(Principal::getName)
                    .collect(Collectors.toList());
            return Optional.of(cast(userNames, returnType));
        } else {
            LOG.debug("Unsupported returnType=" + returnType);
            return Optional.empty();
        }
    }

    /**
     * All {@link UserDao#search}es are forced to be abandon type-safety and this one is no exception.
     *
     * @param source elements to cast
     * @param target runtime target type
     * @param <S>    compile source type
     * @param <T>    compile target type
     * @return cast elements
     */
    private <S, T> List<T> cast(List<S> source, Class<T> target) {
        return source.stream()
                .map(target::cast)
                .collect(Collectors.toList());
    }

    @Override
    public void flushCache() {
        super.flushCache();
        reindex();
    }

    private void reindex() {
        Stopwatch timer = Stopwatch.createStarted();
        synchronized (indexLock) {
            //We use a transaction because some databases (Postgres) will not do streaming unless query is run in one
            ofBizTransactionManager.withTransaction(t -> {
                indexer.replaceAllUsers(userSource -> super.processUsers(user -> userSource.accept((OfBizUser) user)));
            });
        }
        LOG.info("Reindex all users took: " + timer);
    }

    private OfBizUser toOfBizUser(com.atlassian.crowd.embedded.api.User user) {
        if (user == null) {
            return null;
        } else if (user instanceof OfBizUser) {
            return (OfBizUser) user;
        }

        return super.findByNameOrNull(user.getDirectoryId(), user.getName());
    }

    private void reindexUser(long userId) {
        synchronized (indexLock) {
            indexer.deindexById(userId);
            OfBizUser user = findById(userId);
            if (user != null) {
                indexer.index(user);
            }
        }
    }

    @EventListener
    public void onApplicationStarted(JiraStartedEvent event) {
        clusterMessagingService.registerListener(USER_INDEX_CHANNEL, invalidationClusterMessageConsumer);
        reindex();
    }

    private class InvalidationClusterMessageConsumer implements ClusterMessageConsumer {
        @Override
        public void receive(String channel, String message, String senderId) {
            LOG.debug("Received clustered user invalidation message: " + message);
            try {
                //Message is whitespace separated list of internal user IDs
                String[] internalUserIdStrings = message.split("\\s");
                for (String internalUserIdString : internalUserIdStrings) {
                    long userId = Long.parseLong(internalUserIdString);
                    reindexUser(userId);
                }
            } catch (NumberFormatException e) {
                LOG.error("Received an invalid cluster message to invalidate user: " + message, e);
            }
        }
    }
}
