package com.atlassian.jira.cluster.monitoring;

import com.atlassian.jira.cluster.zdu.ClusterStateManager;
import com.atlassian.jira.util.BuildUtilsInfo;

/**
 * Implementation of {@link ClusterNodeStatusMBean} to expose via JMX.
 *
 * @since v7.3
 */
public class ClusterNodeStatus implements ClusterNodeStatusMBean {
    private final BuildUtilsInfo buildUtilsInfo;
    private final ClusterStateManager clusterStateManager;

    public ClusterNodeStatus(BuildUtilsInfo buildUtilsInfo,
                             ClusterStateManager clusterStateManager) {
        this.buildUtilsInfo = buildUtilsInfo;
        this.clusterStateManager = clusterStateManager;
    }

    @Override
    public String getNodeVersion() {
        return buildUtilsInfo.getVersion();
    }

    @Override
    public String getClusterUpgradeState() {
        return clusterStateManager.getUpgradeState().name();
    }
}
