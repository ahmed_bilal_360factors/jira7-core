package com.atlassian.jira.web.filters;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import java.io.IOException;

/**
 * Bypasses the rest of the filter chain to get directly to the startup servlet.
 *
 * @since v7.1.0
 */
public class StartupProgressFilter implements Filter {
    /**
     * The {@code &lt;servlet-name&gt;} specified for {@link com.atlassian.jira.web.startup.StartupServlet}
     * in {@code web.xml}.
     */
    private static final String STARTUP_SERVLET_NAME = "startup";

    private volatile RequestDispatcher startupJsp;

    public void init(FilterConfig config) throws ServletException {
        startupJsp = config.getServletContext().getNamedDispatcher(STARTUP_SERVLET_NAME);
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain)
            throws ServletException, IOException {
        startupJsp.forward(req, resp);
    }

    public void destroy() {
    }
}
