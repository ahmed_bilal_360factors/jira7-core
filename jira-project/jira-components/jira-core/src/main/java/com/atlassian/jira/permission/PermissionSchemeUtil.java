package com.atlassian.jira.permission;

import com.atlassian.fugue.Option;
import com.atlassian.jira.scheme.SchemeEntity;
import com.atlassian.jira.security.plugin.ProjectPermissionKey;

import static com.atlassian.fugue.Option.option;
import static com.atlassian.fugue.Option.some;
import static com.atlassian.jira.permission.LegacyProjectPermissionKeyMapping.getKey;

public final class PermissionSchemeUtil {
    private PermissionSchemeUtil() {
    }

    /**
     * Retrieves a permission key from a scheme entity's {@code entityTypeId} field.
     * <p>
     * As {@link com.atlassian.jira.scheme.SchemeEntity}'s {@code entityTypeId} field
     * is of type {@code Object} there can be a lot of different things there. We
     * accept and are able to process the following classes:
     * <ul>
     * <li>ProjectPermissionKey</li> - returned verbatim
     * <li>String</li> - literal permission key
     * <li>Long</li> - we use {@link LegacyProjectPermissionKeyMapping} to resolve the key
     * <li>Integer</li> - we use {@link LegacyProjectPermissionKeyMapping} to resolve the key
     * </ul>
     * </p>
     * <p>
     * Option.none() is returned if {@code entityTypeId} if we couldn't find a legacy mapping from numeric value.
     * </p>
     *
     * @return some project permission key if successful, none() otherwise
     * @throws java.lang.IllegalArgumentException when {@code entityTypeId} field is not a valid class.
     */
    public static Option<ProjectPermissionKey> getPermissionKey(SchemeEntity schemeEntity) {
        Object entityTypeId = schemeEntity.getEntityTypeId();

        if (entityTypeId instanceof ProjectPermissionKey) {
            return some((ProjectPermissionKey) entityTypeId);
        }
        if (entityTypeId instanceof String) {
            return some(new ProjectPermissionKey((String) entityTypeId));
        }
        if (entityTypeId instanceof Long) {
            return option(getKey((Long) entityTypeId));
        }
        if (entityTypeId instanceof Integer) {
            return option(getKey((Integer) entityTypeId));
        }

        throw new IllegalArgumentException("Permission scheme IDs must be ProjectPermissionKey, String, long or int values, not: " + entityTypeId.getClass());
    }
}
