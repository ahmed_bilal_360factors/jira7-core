package com.atlassian.jira.project.type;

import com.atlassian.annotations.Internal;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.user.ApplicationUser;

import java.util.List;

/**
 * Manager to decide what project types should be shown for a specific user.
 *
 * @since 7.0
 */
@Internal
public interface BrowseProjectTypeManager {
    /**
     * @param user the specified user
     * @return a list of installed project types plus the types of the projects that are allowed to be browsed by the user.
     * The project types are distinct with no duplications and sorted by weight.
     */
    List<ProjectType> getAllProjectTypes(final ApplicationUser user);

    /**
     * @param project the specified project
     * @return a boolean depending on whether the type of the specified project can be changed or not.
     * @since 7.1
     */
    boolean isProjectTypeChangeAllowed(Project project);
}
