package com.atlassian.jira.web.action.issue;

import com.atlassian.jira.help.HelpUrls;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;
import com.atlassian.json.marshal.Jsonable;
import com.atlassian.webresource.api.data.WebResourceDataProvider;
import com.google.common.collect.Maps;

import java.util.HashMap;

/**
 *
 * @since v7.1
 */
public class DefaultCommentSecurityLevelHelpLinkDataProvider implements WebResourceDataProvider {

    private final HelpUrls helpUrls;

    public DefaultCommentSecurityLevelHelpLinkDataProvider(HelpUrls helpUrls) {
        this.helpUrls = helpUrls;
    }

    public Jsonable get() {
        return writer -> {
            try {
                DefaultCommentSecurityLevelHelpLinkDataProvider.this.getJsonData().write(writer);
            } catch (JSONException e) {
                throw new Jsonable.JsonMappingException(e);
            }
        };
    }

    private JSONObject getJsonData() {
        HashMap values = Maps.newHashMap();
        values.put("isLocal", false);
        values.put("url", helpUrls.getUrl("commentissue.defaultsecuritylevel").getUrl());
        values.put("title", helpUrls.getUrl("commentissue.defaultsecuritylevel").getTitle());
        values.put("extraClasses", "default-comment-level-help");

        return new JSONObject(values);
    }
}
