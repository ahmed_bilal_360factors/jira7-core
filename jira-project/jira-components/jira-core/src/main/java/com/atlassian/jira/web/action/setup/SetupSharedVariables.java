package com.atlassian.jira.web.action.setup;

import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.web.HttpServletVariables;
import com.google.common.collect.ImmutableMap;

import java.util.Map;

import static com.atlassian.jira.util.http.JiraUrl.constructBaseUrl;

/**
 * Stores items shared across setup steps. Future work should make this class vanish.
 */
public class SetupSharedVariables {
    private static final String SETUP_INSTANT_JIRA_LICENSE_KEY = "setup-instant-jira-license-key";
    private static final String SETUP_INSTANT_EMAIL = "setup-instant-email";
    private static final String SETUP_INSTANT_USERNAME = "setup-instant-username";
    private static final String SETUP_INSTANT_PASSWORD = "setup-instant-password";
    private static final String SETUP_JIRA_LOCAL = "setup-jira-local";

    private final ApplicationProperties applicationProperties;
    private final HttpServletVariables servletVariables;

    public SetupSharedVariables(final HttpServletVariables servletVariables,
                                final ApplicationProperties applicationProperties) {
        this.servletVariables = servletVariables;
        this.applicationProperties = applicationProperties;
    }

    /**
     * Get previously saved JIRA evaluation license key. This method is intended to be used
     * only within Instant Setup path.
     *
     * @return JIRA license
     */
    public String getJiraLicenseKey() {
        return (String) servletVariables.getHttpSession().getAttribute(SETUP_INSTANT_JIRA_LICENSE_KEY);
    }

    /**
     * Save JIRA evaluation license key. This method is intended to be used only within Instant Setup path.
     *
     * @param licenseKey JIRA license
     */
    public void setJiraLicenseKey(String licenseKey) {
        servletVariables.getHttpSession().setAttribute(SETUP_INSTANT_JIRA_LICENSE_KEY, licenseKey);
    }

    /**
     * Save entered by user MAC credentials, which will be used to create an admin account.
     * Should be used by Instant Setup only.
     *
     * @param email    the email associated with MAC account
     * @param password the password for MAC account
     */
    public void setUserCredentials(final String email, final String username, final String password) {
        servletVariables.getHttpSession().setAttribute(SETUP_INSTANT_EMAIL, email);
        servletVariables.getHttpSession().setAttribute(SETUP_INSTANT_USERNAME, username);
        servletVariables.getHttpSession().setAttribute(SETUP_INSTANT_PASSWORD, password);
    }

    /**
     * Return entered by user MAC credentials. Should be used by Instant Setup only.
     *
     * @return a map containing email and password
     */
    public Map<String, String> getUserCredentials() {
        return ImmutableMap.of(
                "email", (String) servletVariables.getHttpSession().getAttribute(SETUP_INSTANT_EMAIL),
                "username", (String) servletVariables.getHttpSession().getAttribute(SETUP_INSTANT_USERNAME),
                "password", (String) servletVariables.getHttpSession().getAttribute(SETUP_INSTANT_PASSWORD)
        );
    }

    /**
     * Save whether current setup process run in instant mode.
     *
     * @param isInstant whether setup run in instant mode
     */
    public void setIsInstantSetup(boolean isInstant) {
        applicationProperties.setOption(APKeys.JIRA_SETUP_IS_INSTANT, isInstant);
        applicationProperties.setOption(APKeys.JIRA_SETUP_MODE_DECIDED, true);
    }

    /**
     * Return the information whether current setup process run in instant mode.
     *
     * @return boolean saying if in instant mode
     */
    public boolean getIsInstantSetup() {
        return applicationProperties.getOption(APKeys.JIRA_SETUP_IS_INSTANT);
    }

    /**
     * Return the information whether it has been decided already in which mode setup should be running.
     *
     * @return boolean if decided
     */
    public boolean isSetupModeDecided() {
        return applicationProperties.getOption(APKeys.JIRA_SETUP_MODE_DECIDED);
    }

    /**
     * Save the given locale in session, so that in can be utilized later by Instant Setup after initial data import.
     *
     * @param locale locale to save
     */
    public void setLocale(final String locale) {
        servletVariables.getHttpSession().setAttribute(SETUP_JIRA_LOCAL, locale);
    }

    /**
     * Retrieve the locale saved in session.
     *
     * @return saved previously locale
     */
    public String getLocale() {
        return (String) servletVariables.getHttpSession().getAttribute(SETUP_JIRA_LOCAL);
    }

    /**
     * Get JIRA base url.
     */
    public String getBaseUrl() {
        return constructBaseUrl(servletVariables.getHttpRequest());
    }
}