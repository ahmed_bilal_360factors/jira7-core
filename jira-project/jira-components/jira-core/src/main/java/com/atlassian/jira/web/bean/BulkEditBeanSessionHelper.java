package com.atlassian.jira.web.bean;

import com.atlassian.jira.web.SessionKeys;
import webwork.action.ActionContext;

/**
 * Helper class for static session storage.
 *
 * @since v5.0
 * @deprecated Since 7.1. Storing information on the httpsession is obsoleted. Please move this functionality to the frontend or other type of storage.
 */
@Deprecated
public class BulkEditBeanSessionHelper {
    /**
    * @deprecated Since 7.1. Storing information on the httpsession is obsoleted. Please move this functionality to the frontend or other type of storage.
    */
    @Deprecated
    public void storeToSession(final BulkEditBean bulkEditBean) {
        ActionContext.getSession().put(SessionKeys.BULKEDITBEAN, bulkEditBean);
    }

    /**
     * @deprecated Since 7.1. Storing information on the httpsession is obsoleted. Please move this functionality to the frontend or other type of storage.
     */
    @Deprecated
    public BulkEditBean getFromSession() {
        return (BulkEditBean) ActionContext.getSession().get(SessionKeys.BULKEDITBEAN);
    }

    /**
     * @deprecated Since 7.1. Storing information on the httpsession is obsoleted. Please move this functionality to the frontend or other type of storage.
     */
    @Deprecated
    public void removeFromSession() {
        ActionContext.getSession().remove(SessionKeys.BULKEDITBEAN);
    }
}
