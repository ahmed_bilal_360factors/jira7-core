package com.atlassian.jira.issue.attachment.store;

import com.atlassian.jira.util.RuntimeIOException;

import javax.annotation.Nonnull;
import java.io.IOException;

/**
 * @since v6.4
 */
public class NotEnoughDataIOException extends RuntimeIOException {
    public NotEnoughDataIOException(@Nonnull final String message, @Nonnull final IOException cause) {
        super(message, cause);
    }

    public NotEnoughDataIOException(@Nonnull final IOException cause) {
        super(cause);
    }

    public NotEnoughDataIOException(@Nonnull final String message) {
        super(message);
    }
}
