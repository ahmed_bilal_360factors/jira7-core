package com.atlassian.jira.mail;

import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.jira.issue.subscription.FilterSubscription;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.mail.MailException;
import com.atlassian.mail.queue.AbstractMailQueueItem;
import com.atlassian.mail.queue.MailQueue;
import com.atlassian.util.concurrent.Lazy;
import com.atlassian.util.concurrent.Supplier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import static org.apache.commons.lang.StringUtils.isNotEmpty;

public class SubscriptionMailQueueItem extends AbstractMailQueueItem {
    // Username was a used as a foreign key until JIRA 6.0, which is why the column is called "username" in the db table.
    public static final String USER_KEY_COLUMN_NAME = "username";
    private static final Logger log = LoggerFactory.getLogger(SubscriptionMailQueueItem.class);
    public static final String ATTACHMENTS_MANAGER_KEY = "attachmentsManager";

    private final FilterSubscription subscription;
    private final Supplier<ApplicationUser> subscriptionCreator;
    private final UserManager userManager;
    private final GroupManager groupManager;
    private final SubscriptionMailQueueItemFactory mailQueueItemFactory;
    private final MailQueue mailQueue;

    SubscriptionMailQueueItem(final FilterSubscription sub, final UserManager userManager,
                              final GroupManager groupManager, final MailQueue mailQueue,
                              final SubscriptionMailQueueItemFactory mailQueueItemFactory) {
        super();
        this.subscription = sub;
        this.userManager = userManager;
        this.groupManager = groupManager;
        this.mailQueue = mailQueue;
        this.mailQueueItemFactory = mailQueueItemFactory;
        this.subscriptionCreator = Lazy.supplier(() -> userManager.getUserByKey(subscription.getUserKey()));
    }

    public void send() throws MailException {
        incrementSendCount();

        //Retrieve all the users to send the filter to.
        final String groupName = subscription.getGroupName();
        final Collection<ApplicationUser> recipients;
        if (isNotEmpty(groupName)) {
            final Group group = userManager.getGroup(groupName);
            if (group == null) {
                log.warn("Group '{}' referenced in subscription '{}' of filter '{}' does not exist.",
                        groupName, subscription.getId(), subscription.getFilterId());
                recipients = Collections.emptySet();
            } else {
                recipients = new ArrayList<>(groupManager.getUsersInGroup(groupName, false));
            }
        } else {
            final String getKey = subscription.getUserKey();
            final ApplicationUser user = getSubscriptionUser();
            if (user == null) {
                log.warn("User '{}' referenced in subscription '{}' of filter '{}' does not exist.",
                        getKey, subscription.getId(), subscription.getFilterId());
                recipients = Collections.emptySet();
            } else {
                recipients = Collections.singleton(user);
            }
        }

        for (final ApplicationUser user : recipients) {
            mailQueue.addItem(mailQueueItemFactory.createSelfEvaluatingEmailQueueItem(subscription, user));
        }
    }

    private ApplicationUser getSubscriptionUser() {
        return subscriptionCreator.get();
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof SubscriptionMailQueueItem)) {
            return false;
        }

        final SubscriptionMailQueueItem subscriptionMailQueueItem = (SubscriptionMailQueueItem) o;

        if (!subscription.equals(subscriptionMailQueueItem.subscription)) {
            return false;
        }
        if (subscriptionCreator != null ? !subscriptionCreator.equals(subscriptionMailQueueItem.subscriptionCreator) : subscriptionMailQueueItem.subscriptionCreator != null) {
            return false;
        }

        return true;
    }

    public int hashCode() {
        int result;
        result = subscription.hashCode();
        result = 29 * result + (subscriptionCreator != null ? subscriptionCreator.hashCode() : 0);
        return result;
    }

    public String toString() {
        final ApplicationUser subscriptionUser = getSubscriptionUser();
        return this.getClass().getName() + " owner: '" + String.valueOf(subscriptionUser) + "'";
    }
}
