package com.atlassian.jira.application;

import com.atlassian.application.api.ApplicationAccess;
import com.atlassian.application.api.ApplicationKey;
import com.atlassian.extras.common.LicensePropertiesConstants;
import com.atlassian.fugue.Iterables;
import com.atlassian.fugue.Option;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.sal.api.user.UserKey;

import javax.annotation.Nullable;
import java.net.URI;

import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * JIRA's implementation of the {@link com.atlassian.application.api.ApplicationAccess} interface.
 *
 * @since 7.0
 */
class JiraApplicationAccess implements ApplicationAccess {
    static final String URL_USER_BROWSER_BTF = "/secure/admin/user/UserBrowser.jspa?applicationFilter=%s";

    private final ApplicationRole role;
    private final ApplicationAuthorizationService authorizationService;
    private final UserManager userManager;

    JiraApplicationAccess(final ApplicationRole role,
                          final ApplicationAuthorizationService authorizationService,
                          final UserManager userManager) {
        this.role = notNull("role", role);
        this.authorizationService = notNull("authorizationService", authorizationService);
        this.userManager = notNull("userManager", userManager);
    }

    @Override
    public ApplicationKey getApplicationKey() {
        return role.getKey();
    }

    @Override
    public Option<Integer> getMaximumUserCount() {
        final int seats = role.getNumberOfSeats();
        if (seats == LicensePropertiesConstants.UNLIMITED_USERS) {
            return Option.none();
        } else {
            return Option.some(seats);
        }
    }

    @Override
    public int getActiveUserCount() {
        return authorizationService.getUserCount(role.getKey());
    }

    @Override
    public boolean canUserAccessApplication(@Nullable final UserKey userKey) {
        return authorizationService.canUseApplication(getApplicationUserFromKey(userKey), role.getKey());
    }

    @Override
    public Option<AccessError> getAccessError(@Nullable final UserKey userKey) {
        final ApplicationUser user = getApplicationUserFromKey(userKey);
        final ApplicationKey key = role.getKey();

        // Returns the most important error, from enumSet ordered by priority
        return Iterables.first(authorizationService.getAccessErrors(user, key));
    }

    /**
     * Returns the ApplicationUser corresponding to a UserKey. Returns null if UserKey is null.
     *
     * @param userKey key for identifying user
     * @return the ApplicationUser corresponding to passed key, or null if key is null
     */
    private ApplicationUser getApplicationUserFromKey(@Nullable final UserKey userKey) {
        if (userKey == null) {
            // then user is anonymous
            return null;
        }
        return userManager.getUserByKey(userKey.getStringValue());
    }

    @Override
    public URI getManagementPage() {
        return getManagementPageForRole(role.getKey());
    }

    public static URI getManagementPageForRole(final ApplicationKey key) {
        return URI.create(String.format(URL_USER_BROWSER_BTF, key.value()));
    }
}
