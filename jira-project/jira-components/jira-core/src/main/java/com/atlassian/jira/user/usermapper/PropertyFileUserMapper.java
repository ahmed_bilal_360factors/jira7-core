package com.atlassian.jira.user.usermapper;

import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;

import java.util.Properties;

public class PropertyFileUserMapper extends AbstractUserMapper {
    private final Properties properties;

    public PropertyFileUserMapper(UserManager userManager, Properties properties) {
        super(userManager);
        this.properties = properties;
    }

    public ApplicationUser getUserFromEmailAddress(String emailAddress) {
        String username = properties.getProperty(emailAddress);
        return getUser(username);
    }
}
