package com.atlassian.jira.upgrade;

import com.atlassian.jira.index.request.ReindexRequestType;
import com.atlassian.upgrade.api.UpgradeContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Set;
import java.util.function.Supplier;

/**
 * Wraps upgrade operations with logging.
 */
public class LoggingUpgradeService {
    private static final Logger LOGGER = LoggerFactory.getLogger(LoggingUpgradeService.class);

    private final ClusterLockingUpgradeService delegate;

    public LoggingUpgradeService(final ClusterLockingUpgradeService delegate) {
        this.delegate = delegate;
    }

    public UpgradeResult runUpgradesWithLogging(
            final Set<ReindexRequestType> reindexRequestTypes,
            final UpgradeContext upgradeContext,
            final String description
    ) {
        return runWithTaskLogging(description, () -> delegate.runUpgrades(reindexRequestTypes, upgradeContext));
    }

    /**
     * This allows us to simplify the logging of the upgrade process that is being done.
     *
     * @param description of the task that is being completed for the service
     * @param operation the actual task that is being run
     * @return the result of running the operation
     */
    private UpgradeResult runWithTaskLogging(final String description, final Supplier<UpgradeResult> operation) {
        final long startTime = System.currentTimeMillis();
        UpgradeResult upgradeResult = null;
        try {
            LOGGER.info("{} has started", description);
            upgradeResult = operation.get();
            return upgradeResult;
        } finally {
            final long duration = System.currentTimeMillis() - startTime;
            final boolean success = upgradeResult != null && upgradeResult.successful();

            LOGGER.info(
                    "{} has finished {}successfully, and took {} milliseconds to process.",
                    description,
                    success ? "" : "un",
                    duration
            );
        }
    }

    public boolean areUpgradesRunning() {
        LOGGER.info("Checking to see if any upgrades are currently running");
        final boolean upgradesRunning = delegate.areUpgradesRunning();
        LOGGER.info("{} Upgrade Tasks currently running", upgradesRunning ? "" : "No");
        return upgradesRunning;
    }
}
