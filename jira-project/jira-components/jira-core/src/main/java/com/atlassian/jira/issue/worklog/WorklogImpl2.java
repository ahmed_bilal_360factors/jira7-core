package com.atlassian.jira.issue.worklog;


import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.security.roles.ProjectRole;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.JiraDateUtils;
import com.atlassian.jira.workflow.WorkflowFunctionUtils;

import java.util.Date;

import static com.google.common.base.Preconditions.checkArgument;

/**
 * Represents an issue worklog.
 */
public class WorklogImpl2 implements Worklog {
    private final Long id;
    private final String authorKey;
    private final String updateAuthorKey;
    private final String comment;
    private final String groupLevel;
    private final Long roleLevelId;
    private final Date created;
    private final Date updated;
    private final Date startDate;
    private final Long timeSpent;
    private final Issue issue;
    private final ProjectRole projectRole;

    public WorklogImpl2(Issue issue,
                        Long id,
                        String authorKey,
                        String comment,
                        Date startDate,
                        String groupLevel,
                        Long roleLevelId,
                        Long timeSpent,
                        ProjectRole projectRole) {
        this(issue,
                id,
                authorKey,
                comment,
                startDate,
                groupLevel,
                roleLevelId,
                timeSpent,
                authorKey,
                new Date(),
                null,
                projectRole);
    }

    public WorklogImpl2(Issue issue,
                        Long id,
                        String authorKey,
                        String comment,
                        Date startDate,
                        String groupLevel,
                        Long roleLevelId,
                        Long timeSpent,
                        String updateAuthorKey,
                        Date created,
                        Date updated,
                        ProjectRole projectRole) {
        checkTimeSpent(timeSpent);
        this.authorKey = authorKey;
        if (updateAuthorKey == null) {
            updateAuthorKey = this.authorKey;
        }
        this.updateAuthorKey = updateAuthorKey;
        this.comment = comment;
        this.groupLevel = groupLevel;
        this.roleLevelId = roleLevelId;
        this.timeSpent = timeSpent;
        Date createdDate = JiraDateUtils.copyOrCreateDateNullsafe(created);
        this.startDate = (startDate == null) ? createdDate : startDate;
        this.created = createdDate;
        this.updated = (updated == null) ? createdDate : updated;
        this.issue = issue;
        this.id = id;
        this.projectRole = projectRole;
    }

    private void checkTimeSpent(final Long timeSpent) {
        checkArgument(timeSpent != null, "timeSpent must be set!");
    }

    @Override
    public Long getId() {
        return this.id;
    }

    @Override
    public String getAuthor() {
        return authorKey;
    }

    @Override
    public String getAuthorFullName() {
        ApplicationUser user = getAuthorObject();
        if (user != null) {
            return user.getDisplayName();
        }
        return authorKey;
    }

    @Override
    public String getUpdateAuthor() {
        return updateAuthorKey;
    }

    @Override
    public String getUpdateAuthorFullName() {
        ApplicationUser user = getUpdateAuthorObject();
        if (user != null) {
            return user.getDisplayName();
        }
        return updateAuthorKey;
    }

    @Override
    public String getAuthorKey() {
        return authorKey;
    }

    @Override
    public ApplicationUser getAuthorObject() {
        return WorkflowFunctionUtils.getUserByKey(authorKey);
    }

    @Override
    public String getUpdateAuthorKey() {
        return updateAuthorKey;
    }

    @Override
    public ApplicationUser getUpdateAuthorObject() {
        return WorkflowFunctionUtils.getUserByKey(updateAuthorKey);
    }

    @Override
    public Date getStartDate() {
        return JiraDateUtils.copyDateNullsafe(startDate);
    }

    @Override
    public Long getTimeSpent() {
        return timeSpent;
    }

    @Override
    public String getGroupLevel() {
        return groupLevel;
    }

    @Override
    public Long getRoleLevelId() {
        return roleLevelId;
    }

    @Override
    public ProjectRole getRoleLevel() {
        return projectRole;
    }

    @Override
    public String getComment() {
        return this.comment;
    }

    @Override
    public Date getCreated() {
        return created;
    }

    @Override
    public Date getUpdated() {
        return updated;
    }

    @Override
    public Issue getIssue() {
        return issue;
    }

}

