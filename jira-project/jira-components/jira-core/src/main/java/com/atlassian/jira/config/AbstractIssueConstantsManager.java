package com.atlassian.jira.config;

import com.atlassian.core.ofbiz.util.OFBizPropertyUtils;
import com.atlassian.jira.database.QueryDslAccessor;
import com.atlassian.jira.issue.IssueConstant;
import com.atlassian.jira.issue.IssueConstantImpl;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.index.IndexException;
import com.atlassian.jira.issue.index.IssueIndexingService;
import com.atlassian.jira.issue.util.IssueIdsIssueIterable;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.task.context.Contexts;
import com.querydsl.core.types.dsl.StringPath;
import org.ofbiz.core.entity.GenericEntityException;
import org.ofbiz.core.entity.GenericValue;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import static com.atlassian.jira.model.querydsl.QIssue.ISSUE;

/**
 * @since v5.0
 */
public abstract class AbstractIssueConstantsManager<T extends IssueConstant> {
    protected final ConstantsManager constantsManager;
    protected final OfBizDelegator ofBizDelegator;
    protected final IssueIndexingService issueIndexingService;
    protected final IssueManager issueManager;
    protected final QueryDslAccessor queryDslAccessor;

    public AbstractIssueConstantsManager(ConstantsManager constantsManager, OfBizDelegator ofBizDelegator, IssueIndexingService issueIndexingService, IssueManager issueManager, QueryDslAccessor queryDslAccessor) {
        this.constantsManager = constantsManager;
        this.ofBizDelegator = ofBizDelegator;
        this.issueIndexingService = issueIndexingService;
        this.issueManager = issueManager;
        this.queryDslAccessor = queryDslAccessor;
    }

    protected GenericValue createConstant(Map<String, Object> fields) {
        return ofBizDelegator.createValue(getIssueConstantField(), fields);
    }

    protected long getMaxSequenceNo() {
        long maxSequence = 0;
        for (IssueConstant genericValue : getAllValues()) {
            long thisSequence = genericValue.getSequence();
            if (thisSequence > maxSequence) {
                maxSequence = thisSequence;
            }
        }
        return maxSequence;
    }

    protected String getNextStringId() throws GenericEntityException {
        return ofBizDelegator.getDelegatorInterface().getNextSeqId(getIssueConstantField()).toString();
    }

    /**
     * Removes a constant value.
     * @param field        The field in QIssue which may contain the constant
     * @param oldConstant  The constant to be removed
     * @param newId        The ID of the constant to replace it with
     */
    protected void removeConstant(StringPath field, T oldConstant, String newId) throws GenericEntityException, IndexException {
        final long batchSize = 1000;

        //JVS-66: convert a batch of issues at a time, to avoid exhausting available memory
        while (true) {
            List<Long> matchingIssues = queryDslAccessor.executeQuery(
                    dbConnection -> {
                        List<Long> issues = dbConnection.newSqlQuery()
                                .select(ISSUE.id)
                                .from(ISSUE)
                                .where(field.eq(oldConstant.getId()))
                                .limit(batchSize)
                                .fetch();

                        if (! issues.isEmpty()) {
                            dbConnection.update(ISSUE)
                                    .set(field, newId)
                                    .where(ISSUE.id.in(issues))
                                    .execute();
                        }

                        return issues;
                    });

            //we're done when there are no more issues left which use the old constant
            if (matchingIssues.isEmpty()) {
                break;
            }

            issueIndexingService.reIndexIssues(new IssueIdsIssueIterable(matchingIssues, issueManager), Contexts.nullContext());
        }

        // Remove translations (if any)
        GenericValue constantGv = ((IssueConstantImpl) oldConstant).getGenericValue();
        String id = constantGv.getString("id");
        constantGv.set("id", new Long(id));
        removePropertySet(constantGv);
        constantGv.set("id", id);
        constantGv.remove();

        clearCaches();

        postProcess(oldConstant);
    }

    protected void removePropertySet(GenericValue constantGv) {
        OFBizPropertyUtils.removePropertySet(constantGv);
    }

    protected void postProcess(T constant) {
    }

    protected void clearCaches() {
    }

    protected abstract String getIssueConstantField();

    protected abstract List<T> getAllValues();

    protected void moveUp(T constant) {
        final List<T> allValues = getAllValues();
        List<GenericValue> reordered = new ArrayList<GenericValue>(allValues.size());
        for (T cons : allValues) {
            GenericValue value = ((IssueConstantImpl) cons).getGenericValue();
            if (cons.getId().equals(constant.getId()) && reordered.size() == 0) {
                break;
            } else if (cons.getId().equals(constant.getId())) {
                reordered.add(reordered.size() - 1, value);
            } else {
                reordered.add(value);
            }
        }
        storeAndClearCaches(reordered);
    }

    protected void moveDown(T constant) {
        final List<T> allValues = getAllValues();
        final List<GenericValue> reordered = new ArrayList<GenericValue>(allValues.size());
        for (Iterator<T> iterator = allValues.iterator(); iterator.hasNext(); ) {
            T cons = iterator.next();
            GenericValue value = ((IssueConstantImpl) cons).getGenericValue();

            if (cons.getId().equals(constant.getId()) && !iterator.hasNext()) {
                break;
            } else if (cons.getId().equals(constant.getId())) {
                reordered.add(((IssueConstantImpl) iterator.next()).getGenericValue());
                reordered.add(value);
            } else
                reordered.add(value);
        }
        storeAndClearCaches(reordered);
    }

    private void storeAndClearCaches(List<GenericValue> reordered) {
        for (int i = 0; i < reordered.size(); i++) {
            GenericValue value = reordered.get(i);
            value.set("sequence", new Long(i + 1));
        }

        ofBizDelegator.storeAll(reordered);

        clearCaches();
    }
}
