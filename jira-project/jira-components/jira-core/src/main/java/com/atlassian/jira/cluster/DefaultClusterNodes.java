package com.atlassian.jira.cluster;

import com.atlassian.jira.util.BuildUtilsInfo;
import com.atlassian.jira.util.JiraUtils;
import com.atlassian.util.concurrent.ResettableLazyReference;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Objects;
import com.google.common.collect.ImmutableSet;
import org.apache.commons.lang3.StringUtils;

import java.util.Set;

import static com.atlassian.jira.cluster.Node.NodeState.ACTIVE;
import static com.atlassian.jira.cluster.Node.NodeState.OFFLINE;

/**
 * @since v7.0.6
 */
public class DefaultClusterNodes implements ClusterNodes {

    // A reference to the node where this cluster instance is running
    @ClusterSafe
    private final ResettableLazyReference<Node> nodeRef = new ResettableLazyReference<Node>() {
        @Override
        protected Node create() {
            return initializeNode();
        }
    };
    private final ClusterNodeProperties clusterNodeProperties;
    private final OfBizClusterNodeStore ofBizClusterNodeStore;
    private final String hostname;
    private final long nodeBuildNumber;
    private final String nodeVersion;

    public DefaultClusterNodes(final ClusterNodeProperties clusterNodeProperties, final OfBizClusterNodeStore ofBizClusterNodeStore,
                               final BuildUtilsInfo buildUtilsInfo) {
        this.clusterNodeProperties = clusterNodeProperties;
        this.ofBizClusterNodeStore = ofBizClusterNodeStore;
        hostname = buildHostname();
        nodeBuildNumber = buildUtilsInfo.getApplicationBuildNumber();
        nodeVersion = buildUtilsInfo.getVersion();
    }

    @Override
    public Node current() {
        return nodeRef.get();
    }

    @Override
    public void reset() {
        nodeRef.reset();
    }

    @Override
    public Set<Node> all() {
        return ImmutableSet.copyOf(ofBizClusterNodeStore.getAllNodes());
    }

    Node initializeNode() {
        final String nodeId = clusterNodeProperties.getNodeId();
        if (StringUtils.isBlank(nodeId)) {
            return Node.NOT_CLUSTERED;
        }
        return getOrCreateNode(nodeId);
    }

    /**
     * We get or create the node.
     * But if we get the node and the state is different we need to update it also
     *
     * @param nodeId the node id
     * @return the new node
     */
    private Node getOrCreateNode(final String nodeId) {
        Node node = ofBizClusterNodeStore.getNode(nodeId);

        //This is the first scenario when the cluster was started correctly
        if (node == null) {
            node = ofBizClusterNodeStore.createNode(nodeId, ACTIVE, hostname, getCacheListenerPort(), nodeBuildNumber, nodeVersion);
        } else if (stateHasChanged(node)) {
            //This scenario occurs when a graceful shutdown occurred and we need to restart
            node = ofBizClusterNodeStore.updateNode(nodeId, ACTIVE, hostname, getCacheListenerPort(), nodeBuildNumber, nodeVersion);
        }

        return node;
    }

    /**
     * We validate if the state of the node has changed.
     * <p>
     * 1) if the node is in offline state in the db, and we are initializing it.
     * 2) if the node has a different IP address.
     * 3) if the port of multicasting has changed
     *
     * @param node the node
     * @return true if the state has changed, false if not
     */
    protected boolean stateHasChanged(Node node) {
        return node.getState() == OFFLINE
                || !StringUtils.equalsIgnoreCase(hostname, node.getIp())
                || !Objects.equal(node.getCacheListenerPort(), getCacheListenerPort())
                || !Objects.equal(node.getNodeBuildNumber(), nodeBuildNumber)
                || !Objects.equal(node.getNodeVersion(), nodeVersion);
    }

    protected Long getCacheListenerPort() {
        final String port = clusterNodeProperties.getProperty(DefaultEhCacheConfigurationFactory.EHCACHE_LISTENER_PORT);
        return Long.valueOf(port != null ? port : DefaultEhCacheConfigurationFactory.DEFAULT_LISTENER_PORT);
    }

    /**
     * We are going to evaluate if the user set a hostname in the .properties file
     * if that is the case then we need to use that one all over the cluster
     *
     * @return the ip/hostname
     */
    @VisibleForTesting
    protected String buildHostname() {
        final String hostname = clusterNodeProperties.getProperty(DefaultEhCacheConfigurationFactory.EHCACHE_LISTENER_HOSTNAME);
        return hostname != null ? hostname : JiraUtils.getHostname();
    }

    @Override
    public String getHostname() {
        return hostname;
    }
}
