package com.atlassian.jira.web.session;

import com.atlassian.jira.issue.pager.NextPreviousPager;
import com.atlassian.jira.util.NonInjectableComponent;

/**
 * Provides access to getting and setting {@link NextPreviousPager} objects in session.
 *
 * @see SessionSearchObjectManagerFactory#createNextPreviousPagerManager()
 * @see SessionSearchObjectManagerFactory#createNextPreviousPagerManager(javax.servlet.http.HttpServletRequest)
 * @see SessionSearchObjectManagerFactory#createNextPreviousPagerManager(com.atlassian.jira.util.velocity.VelocityRequestSession)
 * @since v4.2
 * @deprecated since 7.1 Storing information on the httpsession is obsoleted. Please move this functionality to the frontend or other type of storage. This class will be removed in 8.0.
 */
@Deprecated
@NonInjectableComponent
public interface SessionNextPreviousPagerManager extends SessionSearchObjectManager<NextPreviousPager> {
}
