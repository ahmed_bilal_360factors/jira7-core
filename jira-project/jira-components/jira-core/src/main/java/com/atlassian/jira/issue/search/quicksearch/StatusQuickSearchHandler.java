package com.atlassian.jira.issue.search.quicksearch;

import com.atlassian.core.util.map.EasyMap;
import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.issue.IssueConstant;

import java.util.Map;

public class StatusQuickSearchHandler extends SingleWordQuickSearchHandler {
    public final ConstantsManager constantsManager;

    public StatusQuickSearchHandler(ConstantsManager constantsManager) {
        this.constantsManager = constantsManager;
    }

    protected Map handleWord(String word, QuickSearchResult searchResult) {
        IssueConstant statusByName = getStatusByName(word);
        return statusByName != null ? EasyMap.build("status", statusByName.getId()) : null;
    }

    private IssueConstant getStatusByName(String name) {
        return getIssueConstantByName(constantsManager.getStatuses(), name);
    }

}
