package com.atlassian.jira.web.action.setup;

import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.JiraProperties;
import com.atlassian.jira.license.JiraLicenseManager;
import com.atlassian.jira.setup.AsynchronousJiraSetupFactory;
import com.atlassian.jira.setup.InstantSetupStrategy;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.FileFactory;
import com.atlassian.jira.util.JiraProductInformation;
import com.atlassian.jira.util.http.JiraUrl;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.SortedSet;

import static com.atlassian.jira.component.ComponentAccessor.getComponent;
import static com.atlassian.jira.user.util.SneakyAutoLoginUtil.logUserInByName;

public class SetupFinishing extends AbstractSetupAction {

    private String email;
    private String username;
    private String jiraLicense;
    private String password;

    private final SetupSharedVariables sharedVariables;
    private final SetupCompleteRedirectHelper setupCompleteRedirectHelper;

    public SetupFinishing(final FileFactory fileFactory, final JiraProperties jiraProperties,
                          final SetupSharedVariables sharedVariables, final SetupCompleteRedirectHelper setupCompleteRedirectHelper,
                          final JiraProductInformation jiraProductInformation) {
        super(fileFactory, jiraProperties, jiraProductInformation);
        this.sharedVariables = sharedVariables;
        this.setupCompleteRedirectHelper = setupCompleteRedirectHelper;
    }

    public String doDefault() throws Exception {
        if (setupAlready()) {
            return SETUP_ALREADY;
        }

        return INPUT;
    }

    private String getRedirectPathAfterSetup() {
        return getRedirectPathAfterSetup(null);
    }

    private String getRedirectPathAfterSetup(ApplicationUser user) {
        if (!setupAlready()) {
            return null;
        }

        return setupCompleteRedirectHelper.getRedirectUrl(user != null ? user : getLoggedInApplicationUser());
    }

    public String doExecute() {
        if (setupAlready()) {
            return getRedirect(getRedirectPathAfterSetup());
        }

        sharedVariables.setUserCredentials(email, username, password);
        sharedVariables.setJiraLicenseKey(jiraLicense);
        sharedVariables.setLocale(getLocale().toString());

        InstantSetupStrategy.setupJiraBaseUrl(JiraUrl.constructBaseUrl(getHttpRequest()));

        return getRedirect("SetupFinishing!default.jspa");
    }

    public String doTriggerSetup() throws JSONException, IOException {
        if (setupAlready()) {
            throw new RuntimeException("Can not be done after setup finished");
        }

        AsynchronousJiraSetupFactory.getInstance().setupJIRA(getSetupSessionId(),
                InstantSetupStrategy.SetupParameters.builder()
                        .setJiraLicenseKey(sharedVariables.getJiraLicenseKey())
                        .setBaseUrl(JiraUrl.constructBaseUrl(getHttpRequest()))
                        .setServerId(getServerId())
                        .setCredentials(sharedVariables.getUserCredentials())
                        .setLocale(sharedVariables.getLocale())
                        .build());

        final JSONObject json = new JSONObject();
        json.put("result", "OK");
        writeJSONToHttpResponse(json);
        return NONE;
    }

    public String doSetupFinished() throws IOException, JSONException {
        final JSONObject json = new JSONObject();
        if (setupAlready() && AsynchronousJiraSetupFactory.getInstance().isSetupFinished(getSetupSessionId())) {
            json.put("redirectUrl", getRedirectUrl());
            json.put("SEN", getSupportEntitlementNumber());
        } else {
            throw new RuntimeException("Setup is not yet finished");
        }
        writeJSONToHttpResponse(json);
        return NONE;
    }

    private String getSupportEntitlementNumber() {
        SortedSet<String> sortedSens = getComponent(JiraLicenseManager.class).getSupportEntitlementNumbers();
        return sortedSens != null && sortedSens.size() > 0 ? sortedSens.first() : "";
    }

    private String getRedirectUrl() {
        ApplicationUser user = logUserInByName(sharedVariables.getUserCredentials().get("username"), getHttpRequest());
        return getApplicationProperties().getString(APKeys.JIRA_BASEURL) + getRedirectPathAfterSetup(user);
    }

    private void writeJSONToHttpResponse(final JSONObject json) throws IOException {
        final HttpServletResponse response = getHttpResponse();
        response.setContentType("application/json");
        final String result = json.toString();
        response.getWriter().write(result);
        response.getWriter().flush();
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setJiraLicense(String jiraLicense) {
        this.jiraLicense = jiraLicense;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}

