/*
 * Copyright (c) 2002-2015
 * All rights reserved.
 */

package com.atlassian.jira.web.action.admin;


import com.atlassian.collectors.CollectorsUtil;
import com.atlassian.crowd.embedded.api.CrowdService;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.crowd.exception.FailedAuthenticationException;
import com.atlassian.fugue.Option;
import com.atlassian.jira.bc.license.JiraLicenseService;
import com.atlassian.jira.bc.license.JiraLicenseUpdaterService;
import com.atlassian.jira.cluster.ClusterManager;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.JiraProperties;
import com.atlassian.jira.config.properties.SystemPropertyKeys;
import com.atlassian.jira.license.JiraLicenseManager;
import com.atlassian.jira.license.LicenseDetails;
import com.atlassian.jira.license.LicenseMaintenancePredicate;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.ApplicationUsers;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.BuildUtilsInfo;
import com.atlassian.jira.util.johnson.JohnsonProvider;
import com.atlassian.jira.util.system.JiraSystemRestarter;
import com.atlassian.jira.web.action.ActionViewDataMappings;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.jira.web.util.ExternalLinkUtil;
import com.atlassian.jira.web.util.ExternalLinkUtilImpl;
import com.atlassian.jira.web.util.MetalResourcesManager;
import com.atlassian.johnson.JohnsonEventContainer;
import com.atlassian.johnson.event.Event;
import com.atlassian.johnson.event.EventLevel;
import com.atlassian.johnson.event.EventType;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static com.atlassian.jira.license.LicenseJohnsonEventRaiser.CLUSTERING_UNLICENSED;
import static com.atlassian.jira.license.LicenseJohnsonEventRaiser.LICENSE_TOO_OLD;
import static com.atlassian.jira.license.LicenseJohnsonEventRaiser.SUBSCRIPTION_EXPIRED;
import static com.atlassian.jira.util.dbc.Assertions.notNull;
import static com.google.common.collect.ImmutableList.copyOf;

/**
 * Displays the page to update the current JIRA license when it has been detected that the current license is
 * <em>&quot;too old&quot;</em>.
 * <p>
 *     Security: This action is only accessible when a Johnson Event of type {@link
 * com.atlassian.jira.license.LicenseJohnsonEventRaiser#LICENSE_TOO_OLD} is present in the {@link JohnsonEventContainer}
 * </p>
 * <p>
 *     Trigger: The link to display this action is displayed in the Johnson errors page (errors.jsp)
 * </p>
 *
 * @see com.atlassian.jira.license.LicenseJohnsonEventRaiser
 * @see com.atlassian.jira.upgrade.UpgradeLauncher
 * @see JohnsonEventContainer
 */
public class ConfirmNewInstallationWithOldLicense extends JiraWebActionSupport {
    static final String RADIO_OPTION_LICENSE = "license";
    static final String RADIO_OPTION_EVALUATION = "evaluation";
    static final String RADIO_OPTION_REMOVE_EXPIRED = "remove-expired";
    private static final String EXTERNAL_LINK_JIRA_LICENSE_VIEW_CLUSTERED = "external.link.jira.license.data.center.contact";
    private static final String EXTERNAL_LINK_JIRA_LICENSE_VIEW_TIMEBOMB = "external.link.jira.license.view.timebomb";
    private static final String CLUSTERED_TIMEBOMB_ANCHOR = "TimebombLicensesforTesting-TestingDataCentercompatibility";
    private static final String EXTERNAL_LINK_JIRA_LICENSE_VIEW = "external.link.jira.license.view";
    private static final String LICENSE_DESC_LINK_CONTENT = "licenseDescLinkContent";
    private static final String LICENSE_DESC_LINK_TIMEBOMB_CONTENT = "licenseDescLinkTimebombContent";
    private static final int CROWD_EMBEDDED_INTEGRATION_VERSION = 602;

    /**
     * Key used for primary message about maintenance expiry.
     */
    static final String MAINTENANCE_EXPIRY_PRIMARY_KEY = "admin.license.support.and.updates";

    /**
     * Key used for secondary message about maintenance expiry.
     */
    static final String MAINTENANCE_EXPIRY_SECONDARY_KEY = "admin.license.renewal.target";

    /**
     * Key use for primary message about subscription expiry.
     */
    static final String SUBSCRIPTION_EXPIRY_PRIMARY_KEY = "admin.license.evaluation";

    /**
     * Key use for secondary message about subscription expiry.
     */
    static final String SUBSCRIPTION_EXPIRY_SECONDARY_KEY = "admin.license.evaluation.renew";

    private final JiraLicenseUpdaterService jiraLicenseService;
    private final BuildUtilsInfo buildUtilsInfo;
    private final JiraSystemRestarter jiraSystemRestarter;
    private final CrowdService crowdService;
    private final GlobalPermissionManager permissionManager;
    private final ClusterManager clusterManager;
    private final UserManager userManager;
    private final JiraProperties jiraSystemProperties;
    private final JohnsonProvider johnsonProvider;
    private final LicenseMaintenancePredicate maintenancePredicate;
    private final JiraLicenseManager jiraLicenseManager;

    private String userName;
    private String password;
    private String licenseString;  // License pasted in the form
    private String radioOption;
    private boolean licenseUpdated = false;
    private boolean installationConfirmed = false;
    private List<LicenseDetails> licensesDetails;
    private JiraLicenseService.ValidationResult validationResult;
    private boolean loginInvalid = false;
    private boolean radioOptionInvalid = false;

    public ConfirmNewInstallationWithOldLicense(JiraLicenseUpdaterService jiraLicenseService,
                                                BuildUtilsInfo buildUtilsInfo,
                                                JiraSystemRestarter jiraSystemRestarter,
                                                CrowdService crowdService,
                                                GlobalPermissionManager permissionManager,
                                                JiraProperties jiraSystemProperties,
                                                ClusterManager clusterManager,
                                                UserManager userManager,
                                                JohnsonProvider johnsonProvider,
                                                LicenseMaintenancePredicate maintenancePredicate,
                                                JiraLicenseManager jiraLicenseManager) {
        this.jiraLicenseManager = notNull("jiraLicenseManager", jiraLicenseManager);
        this.maintenancePredicate = notNull("LicenseMaintenancePredicate", maintenancePredicate);
        this.crowdService = notNull("crowdService", crowdService);
        this.clusterManager = notNull("clusterManager", clusterManager);
        this.johnsonProvider = notNull("johnsonProvider", johnsonProvider);
        this.userManager = notNull("userManager", userManager);
        this.jiraLicenseService = notNull("jiraLicenseService", jiraLicenseService);
        this.permissionManager = notNull("permissionManager", permissionManager);
        this.buildUtilsInfo = notNull("buildUtilsInfo", buildUtilsInfo);
        this.jiraSystemRestarter = notNull("jiraSystemRestarter", jiraSystemRestarter);
        this.jiraSystemProperties = notNull("jiraSystemProperties", jiraSystemProperties);
    }

    /**
     * Whether this page should be displayed to the end user.
     *
     * @return true if the page should be displayed; otherwise, false.
     */
    private boolean shouldDisplay() {
        return getJohnsonEventContainer().getEvents().stream()
                .anyMatch(ConfirmNewInstallationWithOldLicense::isLicenseEvent);
    }

    private boolean isEvaluationOptionDisplayable() {
        if (clusterManager.isClustered()) {
            return false;
        }
        boolean expiredDataCenterOrEnterpriseAgreement = this.getLicensesDetails().stream()
                .filter(maintenancePredicate.negate())
                .anyMatch(lic -> lic.isDataCenter() || lic.isEnterpriseLicenseAgreement());
        return !expiredDataCenterOrEnterpriseAgreement;
    }

    private boolean areThereLicensesWithinMaintenance() {
        List<LicenseDetails> licensesDetails = this.getLicensesDetails();
        return licensesDetails.stream().anyMatch(maintenancePredicate);
    }

    @Override
    public String doDefault() throws Exception {
        if (!shouldDisplay()) {
            return "securitybreach";
        }

        return INPUT;
    }

    protected void doValidation() {
        if (!shouldDisplay()) {
            return; // break out of here early if we are not allowed to access this page.
        }

        if (StringUtils.isBlank(radioOption)) {
            log.warn("Neither the License nor the Install Confirmation have been supplied.");
            addErrorMessage(getText("admin.errors.no.license"));
            setRadioOptionInvalid(true);
            return;
        }

        if (getUserInfoAvailable()) {
            //check that the user is an admin and that the password is correct
            ApplicationUser user = getCrowdUser();
            if (user == null || nonAdminUpgradeAllowed() || !checkUserIsAdmin(user)) {
                return;
            }
        }

        if (radioOption.equals(RADIO_OPTION_LICENSE)) {
            validationResult = jiraLicenseService.validate(this, licenseString);
            addErrorCollection(validationResult.getErrorCollection());
        }
    }

    private boolean checkUserIsAdmin(ApplicationUser applicationUser) {
        if (!permissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, applicationUser)) {
            addErrorMessage(getText("admin.errors.no.admin.permission"));
            setLoginInvalid(true);
            return false;
        }
        return true;
    }

    private ApplicationUser getCrowdUser() {
        User user = crowdService.getUser(userName);

        if (user == null) {
            addErrorMessage(getText("admin.errors.invalid.username.or.pasword"));
            setLoginInvalid(true);
            return null;
        }

        try {
            crowdService.authenticate(userName, password);
            return ApplicationUsers.from(user);
        } catch (FailedAuthenticationException e) {
            addErrorMessage(getText("admin.errors.invalid.username.or.pasword"));
            setLoginInvalid(true);
            return null;
        }
    }

    protected String doExecute() throws Exception {
        if (!shouldDisplay()) {
            return "securitybreach";
        }

        Option<String> error = Option.none();
        // Check if the license has been entered
        switch (radioOption) {
            case RADIO_OPTION_LICENSE:
                error = executeNewLicense();
                break;
            case RADIO_OPTION_REMOVE_EXPIRED:
                error = removeOutOfMaintenanceLicenses();
                break;
            case RADIO_OPTION_EVALUATION:
                // Check that the Installation under the Evaluation Terms has been confirmed
                jiraLicenseManager.confirmProceedUnderEvaluationTerms(userName);
                installationConfirmed = true;
                break;
            default:
                throw new IllegalStateException("This will never happen!");
        }

        if (error.isDefined()) {
            return error.get();
        }

        // rock JIRA's world! 
        jiraSystemRestarter.ariseSirJIRA();

        // Remove the Old Licence Event
        JohnsonEventContainer cont = johnsonProvider.getContainer();
        Lists.newArrayList(cont.getEvents()).stream()
                .filter(Objects::nonNull)
                .filter(ConfirmNewInstallationWithOldLicense::isLicenseEvent)
                .forEach(cont::removeEvent);

        //JRADEV-22455 :- prevent anyone from clicking the link before restart - in 6.1 this link is removed
        cont.addEvent(new Event(EventType.get("restart"),
                getText("system.error.restart.for.changes"),
                EventLevel.get(EventLevel.FATAL)));

        return SUCCESS;
    }

    /**
     * Performs the action of entering a new license
     *
     * @return Option containing errors if any
     */
    private Option<String> executeNewLicense() {
        LicenseDetails licenseDetails = jiraLicenseService.setLicense(validationResult);
        if (!maintenancePredicate.test(licenseDetails)) {
            addError("license", getText("admin.errors.license.too.old"));
            return Option.option(ERROR);
        } else {
            licenseUpdated = true;
        }
        return Option.none();
    }

    /**
     * Performs the action of removing out of maintenance licenses
     *
     * @return Option containing errors if any
     */
    private Option<String> removeOutOfMaintenanceLicenses() {
        final List<LicenseDetails> allLicenses = copyOf(jiraLicenseManager.getLicenses());
        final List<LicenseDetails> outOfDate = allLicenses.stream()
                .filter(maintenancePredicate.negate())
                .collect(CollectorsUtil.toImmutableList());

        if (allLicenses.equals(outOfDate)) {
            throw new IllegalStateException("Removing out of maintenance licenses would remove all JIRA licenses.");
        }
        jiraLicenseManager.removeLicenses(outOfDate);
        return Option.none();
    }

    private static boolean isLicenseEvent(Event event) {
        return ImmutableList.of(LICENSE_TOO_OLD, CLUSTERING_UNLICENSED, SUBSCRIPTION_EXPIRED)
                .stream()
                .map(EventType::get)
                .anyMatch(key -> event.getKey().equals(key));
    }

    private JohnsonEventContainer getJohnsonEventContainer() {
        return johnsonProvider.getContainer();
    }

    private List<LicenseDetails> getLicensesDetails() {
        if (licensesDetails == null) {
            licensesDetails = ImmutableList.copyOf(jiraLicenseManager.getLicenses());
        }
        return licensesDetails;
    }

    private Map<String, String> getLicensesStatusMessages() {
        final List<LicenseDetails> licensesDetails = getLicensesDetails();
        final Map<String, String> messages = new HashMap<>();
        for (LicenseDetails licenseDetails : licensesDetails) {
            LicenseDetails.LicenseStatusMessage licenseStatusMessage
                    = licenseDetails.getLicenseStatusMessage(getI18nHelper(), userManager);
            if (licenseStatusMessage != null) {
                // null means "don't show"
                messages.putAll(licenseStatusMessage.getAllMessages());
            }
        }
        return messages;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLicense() {
        return licenseString;
    }

    public void setLicense(String licenseString) {
        this.licenseString = licenseString;
    }

    public String getRadioOption() {
        return radioOption;
    }

    public void setRadioOption(String radioOption) {
        this.radioOption = radioOption;
    }

    public boolean getLoginInvalid() {
        return loginInvalid;
    }

    public void setLoginInvalid(boolean loginInvalid) {
        this.loginInvalid = loginInvalid;
    }

    public boolean getRadioOptionInvalid() {
        return radioOptionInvalid;
    }

    public void setRadioOptionInvalid(boolean radioOptionInvalid) {
        this.radioOptionInvalid = radioOptionInvalid;
    }

    private boolean nonAdminUpgradeAllowed() {
        return jiraSystemProperties.getBoolean(SystemPropertyKeys.UPGRADE_SYSTEM_PROPERTY);
    }

    public BuildUtilsInfo getBuildUtilsInfo() {
        return buildUtilsInfo;
    }

    public String getCurrentBuildDate() {
        return getDateTimeFormatter().format(buildUtilsInfo.getCurrentBuildDate());
    }

    /**
     * During upgrades from 4.2 or earlier to 4.3 or later the user information is not available until after the upgrade
     * has run.
     *
     * @return True if user information is available and we can authenticate users.
     */
    private boolean getUserInfoAvailable() {
        // If the code version running is pre the crowd integration then just return true.
        if (buildUtilsInfo.getApplicationBuildNumber() < CROWD_EMBEDDED_INTEGRATION_VERSION) {
            return true;
        }

        return buildUtilsInfo.getDatabaseBuildNumber() > CROWD_EMBEDDED_INTEGRATION_VERSION;
    }

    @ActionViewDataMappings({"input", "error"})
    public Map<String, Object> getDataMap() {
        Map<String, Object> data = new HashMap<>();
        final String serverId = getServerId();
        data.put("serverId", serverId);
        data.put("errors", getErrors());
        data.put("radioOption", getRadioOption());
        data.put("radioOptionLicense", RADIO_OPTION_LICENSE);
        data.put("radioOptionEvaluation", RADIO_OPTION_EVALUATION);
        data.put("radioRemoveExpired", RADIO_OPTION_REMOVE_EXPIRED);
        data.put("expired", isAnyLicenseExpired());
        data.put("evaluationOptionDisplayable", isEvaluationOptionDisplayable());
        data.put("clustered", clusterManager.isClustered());
        data.put("thereAreValidLicenses", areThereLicensesWithinMaintenance());

        if (clusterManager.isClustered()) {
            data.put(LICENSE_DESC_LINK_CONTENT,
                    getAnchorTagForLink(EXTERNAL_LINK_JIRA_LICENSE_VIEW_CLUSTERED, null));
            data.put(LICENSE_DESC_LINK_TIMEBOMB_CONTENT,
                    getAnchorTagForLink(EXTERNAL_LINK_JIRA_LICENSE_VIEW_TIMEBOMB, CLUSTERED_TIMEBOMB_ANCHOR));
        } else {
            // generating opening tag for link redirecting to generating evaluation license online
            String[] linkParams = new String[]{
                    getBuildUtilsInfo().getVersion(),
                    getBuildUtilsInfo().getCurrentBuildNumber(),
                    "Enterprise",
                    serverId
            };
            data.put(LICENSE_DESC_LINK_CONTENT, getAnchorTagForLink(EXTERNAL_LINK_JIRA_LICENSE_VIEW, linkParams));
        }

        // dealing with general error messages
        final String generalErrorMessage = getErrorMessages().isEmpty() ? null : getErrorMessages().iterator().next();
        data.put("loginErrorMessageContent", getLoginInvalid() ? generalErrorMessage : null);
        setLoginErrorMessageTitle(generalErrorMessage, data);

        data.put("radioOptionErrorMessageContent", getRadioOptionInvalid() ? generalErrorMessage : null);

        // license status messages
        final Map<String, String> licenseStatusMessages = getLicensesStatusMessages();
        if (licenseStatusMessages.get(MAINTENANCE_EXPIRY_PRIMARY_KEY) != null) {
            data.put("licenseStatusMessage1Content", licenseStatusMessages.get(MAINTENANCE_EXPIRY_PRIMARY_KEY));
            data.put("licenseStatusMessage2Content", licenseStatusMessages.get(MAINTENANCE_EXPIRY_SECONDARY_KEY));
        } else {
            data.put("licenseStatusMessage1Content", licenseStatusMessages.get(SUBSCRIPTION_EXPIRY_PRIMARY_KEY));
            data.put("licenseStatusMessage2Content", licenseStatusMessages.get(SUBSCRIPTION_EXPIRY_SECONDARY_KEY));
        }

        // values of fields
        data.put("userNameValue", getUserName());
        data.put("licenseValue", getLicense());

        // for the fake decorator
        data.put("jiraTitle", getApplicationProperties().getDefaultBackedString(APKeys.JIRA_TITLE));
        data.put("jiraLogoUrl", getApplicationProperties().getDefaultBackedString(APKeys.JIRA_LF_LOGO_URL));

        data.put("resourcesContent", MetalResourcesManager.getMetalResources(getHttpRequest().getContextPath()));

        return data;
    }

    private boolean isAnyLicenseExpired() {
        return getLicensesDetails().stream().anyMatch(LicenseDetails::isExpired);
    }

    @ActionViewDataMappings({"success"})
    public Map<String, Object> getDataMapSuccess() {
        Map<String, Object> data = new HashMap<>();

        data.put("licenseUpdated", licenseUpdated);
        data.put("installationConfirmed", installationConfirmed);

        // for the fake decorator
        data.put("jiraTitle", getApplicationProperties().getDefaultBackedString(APKeys.JIRA_TITLE));
        data.put("jiraLogoUrl", getApplicationProperties().getDefaultBackedString(APKeys.JIRA_LF_LOGO_URL));

        data.put("resourcesContent", MetalResourcesManager.getMetalResources(getHttpRequest().getContextPath()));

        return data;
    }

    /**
     * Set the title to be used with the login error message.
     */
    private void setLoginErrorMessageTitle(String generalErrorMessage, Map<String, Object> data) {
        if (StringUtils.equals(generalErrorMessage, getText("admin.errors.no.admin.permission"))) {
            data.put("loginErrorMessageTitle", getLoginInvalid() ?
                    getText("admin.errors.no.admin.permission.title") : null);
        }
    }

    private String getAnchorTagForLink(final String link, final Object linkParams) {
        final ExternalLinkUtil util = ExternalLinkUtilImpl.getInstance();
        if (linkParams != null) {
            return "<a href=\"" + util.getProperty(link, linkParams) + "\">";
        } else {
            return "<a href=\"" + util.getProperty(link) + "\">";
        }
    }
}
