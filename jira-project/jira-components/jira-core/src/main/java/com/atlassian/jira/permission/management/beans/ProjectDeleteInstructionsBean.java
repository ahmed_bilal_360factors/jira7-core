package com.atlassian.jira.permission.management.beans;

import org.codehaus.jackson.annotate.JsonAutoDetect;

import java.util.List;

@JsonAutoDetect
public class ProjectDeleteInstructionsBean {
    private List<Long> grantsToDelete;

    public ProjectDeleteInstructionsBean() {
    }

    public ProjectDeleteInstructionsBean(final List<Long> grantsToDelete) {
        this.grantsToDelete = grantsToDelete;
    }

    public List<Long> getGrantsToDelete() {
        return grantsToDelete;
    }
}
