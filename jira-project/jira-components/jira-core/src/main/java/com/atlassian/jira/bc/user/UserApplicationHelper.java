package com.atlassian.jira.bc.user;

import com.atlassian.annotations.Internal;
import com.atlassian.application.api.ApplicationKey;
import com.atlassian.collectors.CollectorsUtil;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.json.JSONArray;
import com.google.common.base.Objects;
import org.codehaus.jackson.annotate.JsonAutoDetect;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collection;
import java.util.EnumSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static com.atlassian.jira.application.ApplicationKeys.CORE;

/**
 * @since v7.0
 */
@Internal
public interface UserApplicationHelper {
    /**
     * Return the {@code ApplicationSelection} calculated from the current state of JIRA. The passed
     * applications are marked as selected (if they exist). The returned list will be ordered based on the Application
     * name except that JIRA Core will be returned last (if it exists).
     *
     * @param selectedApplicationKeys the set of application keys to mark as selected.
     * @param directoryId             optional directoryId to use for application selection
     * @return an immutable list of {@code ApplicationSelection} ordered by their name except that JIRA Core is always
     * last.
     */
    @Nonnull
    List<ApplicationSelection> getApplicationsForSelection(@Nonnull Set<ApplicationKey> selectedApplicationKeys, @Nonnull final Optional<Long> directoryId);

    /**
     * Return the {@code ApplicationSelection} calculated from the current state of JIRA for given (@code ApplicationUser).
     *
     * @param user subject application user to get applications for.
     * @return an immutable list of {@code ApplicationSelection} ordered by their name except that JIRA Core is always
     * last.
     */
    @Nonnull
    List<ApplicationSelection> getApplicationsForUser(@Nonnull ApplicationUser user);

    /**
     * Return the list of {@code com.atlassian.jira.bc.user.GroupView} connected with the given user.
     * It respects nested groups.
     *
     * @param applicationUser the user
     * @return an immutable list of {@code com.atlassian.jira.bc.user.GroupView}
     */
    @Nonnull
    List<GroupView> getUserGroups(@Nonnull ApplicationUser applicationUser);

    /**
     * Validates if creating a user with default application access in given directory won't exceed license limit
     *
     * @param directoryId the Directory ID, or {@link java.util.Optional#empty()} if default directory should be checked
     * @return a collection of errors
     */
    @Nonnull
    Collection<String> validateDefaultApplications(EnumSet<CreateUserApplicationHelper.ValidationScope> validationScope, Optional<Long> directoryId);

    /**
     * Validates if creating a user with given application access in given directory won't exceed license limit
     *
     * @param directoryId     the Directory ID, or {@link java.util.Optional#empty()} if default directory should be checked
     * @param applicationKeys validated application keys
     * @return a collection of errors
     */
    @Nonnull
    Collection<String> validateApplicationKeys(@Nonnull Optional<Long> directoryId, @Nonnull Set<ApplicationKey> applicationKeys);

    /**
     * Validates if creating a user with given application access in given directory won't cause issues defined in {@link com.atlassian.jira.bc.user.UserApplicationHelper.ValidationScope}
     *
     * @param directoryId     the Directory ID, or {@link java.util.Optional#empty()} if default directory should be checked
     * @param applicationKeys validated application keys
     * @param validationScope required validation scope
     * @return a collection of errors
     */
    @Nonnull
    Collection<String> validateApplicationKeys(@Nonnull Optional<Long> directoryId, @Nonnull Set<ApplicationKey> applicationKeys, @Nonnull EnumSet<ValidationScope> validationScope);

    /**
     * Validates if adding a user with given application access to given applications won't exceed license limit
     *
     * @param user            validated user
     * @param applicationKeys validated application keys
     * @return a collection of errors
     */
    @Nonnull
    Collection<String> validateApplicationKeys(@Nonnull ApplicationUser user, @Nonnull Set<ApplicationKey> applicationKeys);

    /**
     * Check if given user can login to JIRA.
     * Return true if user has any role or has admin/sysadmin privileges
     *
     * @param applicationUser given user
     * @return true if user has any role or is an admin
     */
    boolean canUserLogin(@Nullable ApplicationUser applicationUser);

    /**
     * What should be the validation scope, all type of problems or just the seat count
     */
    enum ValidationScope {
        /**
         * Validate application access related problems
         */
        ACCESS,

        /**
         * Validate license user limit
         */
        SEATS,

        /**
         * Validate license expire
         */
        EXPIRE
    }

    @JsonAutoDetect
    final class ApplicationSelection implements Comparable<ApplicationSelection> {
        private final ApplicationKey key;
        private final String name;
        private final String displayName;
        private final String message;
        private final String messageMarkup;
        private final boolean selectable;
        private final boolean selected;
        private final boolean defined;
        private final boolean deselectable;
        private final boolean indeterminate;

        // Determines whether this application can be implicitly selected by selecting other application.
        private final boolean effectiveOfOtherApplication;
        private final Set<EffectiveApplication> effectiveApplications;

        ApplicationSelection(@Nonnull final ApplicationKey key, @Nonnull final String name,
                             @Nonnull final String displayName, @Nullable final String message,
                             @Nullable final String messageMarkup, final boolean selectable,
                             final boolean selected, final boolean defined,
                             final boolean indeterminate, final boolean effectiveOfOtherApplication, final boolean deselectable,
                             @Nullable final Set<EffectiveApplication> effectiveApplications) {
            this.key = key;
            this.name = name;
            this.displayName = displayName;
            this.message = message;
            this.messageMarkup = messageMarkup;
            this.selectable = selectable;
            this.selected = selected;
            this.defined = defined;
            this.indeterminate = indeterminate;
            this.effectiveOfOtherApplication = effectiveOfOtherApplication;
            this.deselectable = deselectable;
            this.effectiveApplications = effectiveApplications;
        }

        public String getKey() {
            return key.value();
        }

        public String getName() {
            return name;
        }

        public String getDisplayName() {
            return displayName;
        }

        public String getMessage() {
            return message;
        }

        public String getMessageMarkup() {
            return messageMarkup;
        }

        public boolean isSelectable() {
            return selectable;
        }

        public boolean isSelected() {
            return selected;
        }

        public boolean isDefined() {
            return defined;
        }

        public boolean isIndeterminate() {
            return indeterminate;
        }

        public boolean isEffectiveOfOtherApplication() {
            return effectiveOfOtherApplication;
        }

        public boolean isDeselectable() {
            return deselectable;
        }

        public Set<EffectiveApplication> getEffectiveApplications() {
            return effectiveApplications;
        }

        public String getEffectiveApplicationsJson() {
            return new JSONArray(effectiveApplications.stream().map(EffectiveApplication::getKey).collect(CollectorsUtil.toImmutableSet())).toString();
        }

        @Override
        public boolean equals(final Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }
            final ApplicationSelection that = (ApplicationSelection) o;
            return Objects.equal(selectable, that.selectable) &&
                    Objects.equal(selected, that.selected) &&
                    Objects.equal(indeterminate, that.indeterminate) &&
                    Objects.equal(effectiveOfOtherApplication, that.effectiveOfOtherApplication) &&
                    Objects.equal(deselectable, that.deselectable) &&
                    Objects.equal(key, that.key) &&
                    Objects.equal(displayName, that.displayName) &&
                    Objects.equal(name, that.name) &&
                    Objects.equal(message, that.message) &&
                    Objects.equal(effectiveApplications, that.effectiveApplications) &&
                    Objects.equal(defined, that.defined) &&
                    Objects.equal(messageMarkup, that.messageMarkup);
        }

        @Override
        public int hashCode() {
            return Objects.hashCode(key, displayName, name, message, selectable, selected, indeterminate, deselectable, effectiveApplications, defined, messageMarkup);
        }

        @Override
        public int compareTo(@Nullable final ApplicationSelection other) {
            if (this == other) {
                return 0;
            } else if (other == null) {
                return -1;
            } else if (this.key.equals(other.key)) {
                return compareDisplayName(other);
            } else if (CORE.equals(this.key)) {
                return 1;
            } else if (CORE.equals(other.key)) {
                return -1;
            } else {
                return compareDisplayName(other);
            }
        }

        private int compareDisplayName(final ApplicationSelection other) {
            return String.CASE_INSENSITIVE_ORDER.compare(this.displayName, other.displayName);
        }

        @Override
        public String toString() {
            return "ApplicationSelection{" +
                    "key=" + key +
                    ", name='" + name + '\'' +
                    ", displayName='" + displayName + '\'' +
                    ", message='" + message + '\'' +
                    ", messageMarkup='" + messageMarkup + '\'' +
                    ", selectable=" + selectable +
                    ", selected=" + selected +
                    ", defined=" + defined +
                    ", deselectable=" + deselectable +
                    ", indeterminate=" + indeterminate +
                    ", effectiveOfOtherApplication=" + effectiveOfOtherApplication +
                    ", effectiveApplications=" + effectiveApplications +
                    '}';
        }
    }
}
