package com.atlassian.jira.plugin;

import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.factories.PluginFactory;
import com.atlassian.plugin.loaders.DirectoryPluginLoader;
import com.atlassian.plugin.loaders.PluginLoader;

import java.util.List;

/**
 * @since v7.3
 */
public class BootstrapPluginDirectoryLoaderFactory implements PluginDirectoryLoaderFactory {

    private final PluginPath pathFactory;
    private final PluginEventManager pluginEventManager;

    public BootstrapPluginDirectoryLoaderFactory(final PluginPath pathFactory, final PluginEventManager pluginEventManager) {
        this.pathFactory = pathFactory;
        this.pluginEventManager = pluginEventManager;
    }

    @Override
    public PluginLoader getDirectoryPluginLoader(List<PluginFactory> pluginFactories) {
        return new DirectoryPluginLoader(pathFactory.getInstalledPluginsDirectory(), pluginFactories, pluginEventManager);
    }
}
