package com.atlassian.jira.model.querydsl;

import com.querydsl.core.types.dsl.*;
import com.querydsl.sql.ColumnMetadata;

import javax.annotation.Generated;
import java.sql.Types;

/**
 * QUpgradeHistory is a Querydsl query object.
 * <p>
 * Generated by the JIRA Querydsl code generation tool - https://bitbucket.org/atlassian/jira-querydsl-codegen/
 * </p>
 */
@Generated("com.atlassian.jira.tool.querydsl.CodeGenerator")
public class QUpgradeHistory extends JiraRelationalPathBase<UpgradeHistoryDTO> {
    public static final QUpgradeHistory UPGRADE_HISTORY = new QUpgradeHistory("UPGRADE_HISTORY");

    public final NumberPath<Long> id = createNumber("id", Long.class);
    public final StringPath upgradeclass = createString("upgradeclass");
    public final StringPath targetbuild = createString("targetbuild");
    public final StringPath status = createString("status");
    public final StringPath downgradetaskrequired = createString("downgradetaskrequired");

    public QUpgradeHistory(String alias) {
        super(UpgradeHistoryDTO.class, alias, "upgradehistory");
        addMetadata();
    }

    private void addMetadata() {
        addMetadata(id, ColumnMetadata.named("id").withIndex(1).ofType(Types.NUMERIC).withSize(18));
        addMetadata(upgradeclass, ColumnMetadata.named("upgradeclass").withIndex(2).ofType(Types.VARCHAR).withSize(255));
        addMetadata(targetbuild, ColumnMetadata.named("targetbuild").withIndex(3).ofType(Types.VARCHAR).withSize(255));
        addMetadata(status, ColumnMetadata.named("status").withIndex(4).ofType(Types.VARCHAR).withSize(255));
        addMetadata(downgradetaskrequired, ColumnMetadata.named("downgradetaskrequired").withIndex(5).ofType(Types.CHAR).withSize(1));
    }

    /**
     * Returns the ofbiz model's entity name for this query object.
     *
     * @return the ofbiz model's entity name for this query object.
     */
    @Override
    public String getEntityName() {
        return "UpgradeHistory";
    }

    @Override
    public NumberPath<Long> getNumericIdPath() {
        return id;
    }
}

