package com.atlassian.jira.web.action.admin.workflow.tabs;

import com.opensymphony.workflow.loader.ActionDescriptor;
import com.opensymphony.workflow.loader.ConditionDescriptor;
import com.opensymphony.workflow.loader.ConditionsDescriptor;
import com.opensymphony.workflow.loader.RestrictionDescriptor;

import java.util.Collection;
import java.util.Map;

import static com.atlassian.jira.web.action.admin.workflow.tabs.WorkflowTransitionContextUtils.getTransition;

public class WorkflowTransitionConditionsContextProvider extends WorkflowTransitionContextProvider {

    public int getCount(final Map<String, Object> context) {
        final RestrictionDescriptor restriction = getTransition(context).map(ActionDescriptor::getRestriction).orElse(null);
        if (restriction != null) {
            return getNumberConditions(restriction.getConditionsDescriptor());
        } else {
            return 0;
        }
    }

    private int getNumberConditions(final ConditionsDescriptor conditionsDescriptor) {
        int number = 0;
        if (conditionsDescriptor != null) {
            Collection conditions = conditionsDescriptor.getConditions();
            if (conditions != null) {
                for (Object o : conditions) {
                    if (o instanceof ConditionDescriptor) {
                        number++;
                    } else if (o instanceof ConditionsDescriptor) {
                        number += getNumberConditions((ConditionsDescriptor) o);
                    } else {
                        throw new IllegalArgumentException("Invalid object " + o + " found in condition collection.");
                    }
                }
            }
        }

        return number;
    }
}
