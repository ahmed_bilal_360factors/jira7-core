package com.atlassian.jira.project;

import com.atlassian.jira.exception.CreateException;
import com.atlassian.jira.tenancy.TenantAware;
import com.atlassian.jira.tenancy.TenantInfo;
import com.google.common.annotations.VisibleForTesting;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static com.atlassian.collectors.CollectorsUtil.toImmutableList;
import static com.google.common.collect.Lists.newArrayList;

public class ProjectCreateRegistrarImpl implements ProjectCreateRegistrar, ProjectCreateNotifier {
    private static final Logger log = LoggerFactory.getLogger(ProjectCreateRegistrarImpl.class);
    static final String APPLY_PROJECT_TEMPLATE_HANDLER_ID = "com.atlassian.jira.project-templates-plugin:apply-project-template-handler";

    @TenantInfo(value = TenantAware.TENANTLESS, comment = "This stores ProjectCreateHandlers interested in project creation. Any handler added here should be tenantless.")
    protected final Map<String, ProjectCreateHandler> handlers;

    public ProjectCreateRegistrarImpl() {
        this.handlers = new ConcurrentHashMap<>();
    }

    @Override
    public boolean notifyAllHandlers(final ProjectCreatedData projectCreatedData) {
        List<ProjectCreateHandler> notifiedHandlers = newArrayList();
        boolean handlersWereSuccessful = true;
        for (ProjectCreateHandler handler : handlersWithProjectTemplateHandlerFirst()) {
            try {
                notifiedHandlers.add(handler);
                handler.onProjectCreated(projectCreatedData);
            } catch (CreateException | RuntimeException exception) {
                log.error("The handler with id " + handler.getHandlerId() + " threw an exception while handling a notification about a project being created", exception);
                handlersWereSuccessful = false;
                break;
            }
        }

        if (!handlersWereSuccessful) {
            notifiedHandlers.forEach(handler -> handler.onProjectCreationRolledBack(projectCreatedData));
        }

        return handlersWereSuccessful;
    }

    private List<ProjectCreateHandler> handlersWithProjectTemplateHandlerFirst() {
        return getHandlers().stream()
                .sorted((handler1, handler2) -> handler1.getHandlerId().equals(APPLY_PROJECT_TEMPLATE_HANDLER_ID) ? -1 : 0)
                .collect(toImmutableList());
    }

    @VisibleForTesting
    protected Collection<ProjectCreateHandler> getHandlers() {
        return handlers.values();
    }

    @Override
    public void register(final ProjectCreateHandler handlerToAdd) {
        handlers.put(handlerToAdd.getHandlerId(), handlerToAdd);
    }

    @Override
    public void unregister(final ProjectCreateHandler handlerToRemove) {
        handlers.remove(handlerToRemove.getHandlerId());
    }
}
