package com.atlassian.jira.issue.search.providers;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.instrumentation.operations.OpTimer;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.config.properties.JiraSystemProperties;
import com.atlassian.jira.entity.property.EntityPropertyType;
import com.atlassian.jira.index.property.AliasClauseInformation;
import com.atlassian.jira.index.property.JqlAliasManager;
import com.atlassian.jira.instrumentation.Instrumentation;
import com.atlassian.jira.instrumentation.InstrumentationName;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueFactory;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.fields.NavigableField;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchProvider;
import com.atlassian.jira.issue.search.SearchProviderFactory;
import com.atlassian.jira.issue.search.SearchResults;
import com.atlassian.jira.issue.search.TotalHitsAwareCollector;
import com.atlassian.jira.issue.search.managers.SearchHandlerManager;
import com.atlassian.jira.issue.search.parameters.lucene.CachedWrappedFilterCache;
import com.atlassian.jira.issue.search.parameters.lucene.PermissionsFilterGenerator;
import com.atlassian.jira.issue.search.parameters.lucene.sort.StringSortComparator;
import com.atlassian.jira.issue.search.util.SearchSortUtil;
import com.atlassian.jira.jql.ClauseHandler;
import com.atlassian.jira.jql.query.LuceneQueryBuilder;
import com.atlassian.jira.jql.query.QueryCreationContext;
import com.atlassian.jira.jql.query.QueryCreationContextImpl;
import com.atlassian.jira.security.JiraAuthenticationContextImpl;
import com.atlassian.jira.security.RequestCacheKeys;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.RuntimeIOException;
import com.atlassian.jira.web.bean.PagerFilter;
import com.atlassian.jira.web.filters.ThreadLocalQueryProfiler;
import com.atlassian.query.Query;
import com.atlassian.query.clause.Property;
import com.atlassian.query.order.SearchSort;
import com.atlassian.query.order.SortOrder;
import com.atlassian.util.profiling.UtilTimerStack;
import com.google.common.collect.ImmutableList;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.CachingWrapperFilter;
import org.apache.lucene.search.Collector;
import org.apache.lucene.search.Filter;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.MatchAllDocsQuery;
import org.apache.lucene.search.QueryWrapperFilter;
import org.apache.lucene.search.Sort;
import org.apache.lucene.search.SortField;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.search.TotalHitCountCollector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static com.atlassian.collectors.CollectorsUtil.toNewArrayListWithCapacity;
import static java.lang.String.format;
import static org.apache.commons.lang3.ArrayUtils.isNotEmpty;

public class LuceneSearchProvider implements SearchProvider {
    private static final Logger log = LoggerFactory.getLogger(LuceneSearchProvider.class);
    private static final Logger slowLog = LoggerFactory.getLogger(LuceneSearchProvider.class.getName() + "_SLOW");
    private static final int DEFAULT_SEARCH_LIMIT = 1000;
    private static final int MAX_ALLOWED_SEARCH_LIMIT = 100000;
    private static final String RESULTS_LIMIT_DISABLED_FEATURE_KEY = "jira.lucene.search.filter.limit.disabled";


    private final IssueFactory issueFactory;
    private final PermissionsFilterGenerator permissionsFilterGenerator;
    private final SearchHandlerManager searchHandlerManager;
    private final SearchSortUtil searchSortUtil;
    private final LuceneQueryBuilder luceneQueryBuilder;
    private final JqlAliasManager jqlAliasManager;
    private final FeatureManager featureManager;
    private final EventPublisher eventPublisher;
    private final SearchProviderFactory searchProviderFactory;


    public LuceneSearchProvider(final IssueFactory issueFactory, final SearchProviderFactory searchProviderFactory,
                                final PermissionsFilterGenerator permissionsFilterGenerator, final SearchHandlerManager searchHandlerManager,
                                final SearchSortUtil searchSortUtil, final LuceneQueryBuilder luceneQueryBuilder, JqlAliasManager jqlAliasManager,
                                final FeatureManager featureManager, final EventPublisher eventPublisher) {
        this.issueFactory = issueFactory;
        this.searchProviderFactory = searchProviderFactory;
        this.permissionsFilterGenerator = permissionsFilterGenerator;
        this.searchHandlerManager = searchHandlerManager;
        this.searchSortUtil = searchSortUtil;
        this.luceneQueryBuilder = luceneQueryBuilder;
        this.jqlAliasManager = jqlAliasManager;
        this.featureManager = featureManager;
        this.eventPublisher = eventPublisher;
    }

    public SearchResults search(final Query query, final ApplicationUser searcher, final PagerFilter pager) throws SearchException {
        return search(query, searcher, pager, null);
    }

    public SearchResults search(final Query query, final ApplicationUser searcher, final PagerFilter pager, final org.apache.lucene.search.Query andQuery) throws SearchException {
        return search(query, searcher, pager, andQuery, false);
    }

    public SearchResults searchOverrideSecurity(final Query query, final ApplicationUser searcher, final PagerFilter pager, final org.apache.lucene.search.Query andQuery) throws SearchException {
        return search(query, searcher, pager, andQuery, true);
    }

    public long searchCount(final Query query, final ApplicationUser user) throws SearchException {
        return getHitCount(query, user, null, null, false, getIssueSearcher(), null);
    }

    public long searchCountOverrideSecurity(final Query query, final ApplicationUser user) throws SearchException {
        return getHitCount(query, user, null, null, true, getIssueSearcher(), null);
    }

    public void search(final Query query, final ApplicationUser user, final Collector collector) throws SearchException {
        search(query, user, collector, null, false);
    }

    public void search(final Query query, final ApplicationUser searcher, final Collector collector, final org.apache.lucene.search.Query andQuery)
            throws SearchException {
        search(query, searcher, collector, andQuery, false);
    }

    @Override
    public void searchOverrideSecurity(final Query query, final ApplicationUser user, final Collector collector) throws SearchException {
        search(query, user, collector, null, true);
    }

    @Override
    public void searchAndSort(final Query query, final ApplicationUser user, final Collector collector, final PagerFilter pagerFilter) throws SearchException {
        searchAndSort(query, user, collector, pagerFilter, false);
    }

    public void searchAndSortOverrideSecurity(final Query query, final ApplicationUser user, final Collector collector, final PagerFilter pagerFilter) throws SearchException {
        searchAndSort(query, user, collector, pagerFilter, true);
    }

    private IndexSearcher getIssueSearcher() {
        return searchProviderFactory.getSearcher(SearchProviderFactory.ISSUE_INDEX);
    }

    /**
     * WARNING: Please don't change signature and semantics of this function in 7.2 and 7.3 stable.
     * JIRA Service Desk uses this API via jira-pocketknife-search and relies on it.
     * -- Yours Sincerely, JSD Server team
     *
     * Returns 0 if there are no Lucene parameters (search request is null), otherwise returns the hit count
     * <p/>
     * The count is 0 if there are no matches.
     *
     * @param searchQuery      search request
     * @param searchUser       user performing the search
     * @param sortField        array of fields to sort by
     * @param andQuery         a query to join with the request
     * @param overrideSecurity ignore the user security permissions
     * @param issueSearcher    the IndexSearcher to be used when searching
     * @param pager            a pager which holds information about which page of search results is actually required.
     * @return hit count
     * @throws SearchException                                                 if error occurs
     * @throws com.atlassian.jira.issue.search.ClauseTooComplexSearchException if query creates a lucene query that is too complex to be processed.
     */
    private long getHitCount(final Query searchQuery, final ApplicationUser searchUser, final SortField[] sortField, final org.apache.lucene.search.Query andQuery, boolean overrideSecurity, IndexSearcher issueSearcher, final PagerFilter pager) throws SearchException {
        if (searchQuery == null) {
            return 0;
        }
        try {
            final Filter permissionsFilter = getPermissionsFilter(overrideSecurity, searchUser);
            final org.apache.lucene.search.Query finalQuery = createLuceneQuery(searchQuery, andQuery, searchUser, overrideSecurity);
            final TotalHitCountCollector hitCountCollector = new TotalHitCountCollector();
            issueSearcher.search(finalQuery, permissionsFilter, hitCountCollector);
            return hitCountCollector.getTotalHits();
        } catch (IOException e) {
            throw new SearchException(e);
        }

    }

    /**
     * Returns null if there are no Lucene parameters (search request is null), otherwise returns a collection of Lucene
     * Document objects.
     * <p/>
     * The collection has 0 results if there are no matches.
     *
     * @param searchQuery      search request
     * @param searchUser       user performing the search
     * @param sortField        array of fields to sort by
     * @param andQuery         a query to join with the request
     * @param overrideSecurity ignore the user security permissions
     * @param issueSearcher    the IndexSearcher to be used when searching
     * @param pager            a pager which holds information about which page of search results is actually required.
     * @return hits
     * @throws SearchException                                                 if error occurs
     * @throws com.atlassian.jira.issue.search.ClauseTooComplexSearchException if query creates a lucene query that is too complex to be processed.
     */
    private TopDocs getHits(final Query searchQuery, final ApplicationUser searchUser, final SortField[] sortField, final org.apache.lucene.search.Query andQuery, boolean overrideSecurity, IndexSearcher issueSearcher, final PagerFilter pager) throws SearchException {
        if (searchQuery == null) {
            return null;
        }
        try {
            final Filter permissionsFilter = getPermissionsFilter(overrideSecurity, searchUser);
            final org.apache.lucene.search.Query finalQuery = createLuceneQuery(searchQuery, andQuery, searchUser, overrideSecurity);
            if (log.isDebugEnabled()) {
                log.debug("JQL sorts: " + Arrays.toString(sortField));
            }
            return runSearch(null, finalQuery, permissionsFilter, sortField, searchQuery.toString(), pager);
        } catch (final IOException e) {
            throw new SearchException(e);
        }
    }

    /**
     * WARNING: Please don't change signature and semantics of this function in 7.2 and 7.3 stable.
     * JIRA Service Desk uses this API via jira-pocketknife-search and relies on it.
     * -- Yours Sincerely, JSD Server team
     */
    private void search(final Query searchQuery, final ApplicationUser user, final Collector collector, org.apache.lucene.search.Query andQuery, boolean overrideSecurity) throws SearchException {
        final long start = System.currentTimeMillis();
        org.apache.lucene.search.Query finalQuery = andQuery;

        if (searchQuery.getWhereClause() != null) {
            final QueryCreationContext context = new QueryCreationContextImpl(user, overrideSecurity);
            final org.apache.lucene.search.Query query = luceneQueryBuilder.createLuceneQuery(context, searchQuery.getWhereClause());
            if (query != null) {
                if (finalQuery != null) {
                    BooleanQuery join = new BooleanQuery();
                    join.add(finalQuery, BooleanClause.Occur.MUST);
                    join.add(query, BooleanClause.Occur.MUST);
                    finalQuery = join;
                } else {
                    finalQuery = query;
                }
                log.debug("JQL query: " + searchQuery.toString());
                log.debug("JQL lucene query: " + finalQuery);
            } else {
                log.debug("Got a null query from the JQL Query.");
            }
        }

        final Filter permissionsFilter = getPermissionsFilter(overrideSecurity, user);
        UtilTimerStack.push("Searching with Collector");

        // NOTE: we do this because when you are searching for everything the query is EMPTY
        if (finalQuery == null) {
            finalQuery = new MatchAllDocsQuery();
        }
        try {
            getIssueSearcher().search(finalQuery, permissionsFilter, collector);
        } catch (IOException e) {
            throw new SearchException("Exception whilst searching for issues " + e.getMessage(), e);
        }
        UtilTimerStack.pop("Searching with Collector");
        ThreadLocalQueryProfiler.store(ThreadLocalQueryProfiler.LUCENE_GROUP, finalQuery.toString(), (System.currentTimeMillis() - start));
    }

    private org.apache.lucene.search.Query createLuceneQuery(Query searchQuery, org.apache.lucene.search.Query andQuery, ApplicationUser searchUser, boolean overrideSecurity)
            throws SearchException {
        final String jqlSearchQuery = searchQuery.toString();

        org.apache.lucene.search.Query finalQuery = andQuery;

        if (searchQuery.getWhereClause() != null) {
            final QueryCreationContext context = new QueryCreationContextImpl(searchUser, overrideSecurity);
            final org.apache.lucene.search.Query query = luceneQueryBuilder.createLuceneQuery(context, searchQuery.getWhereClause());
            if (query != null) {
                if (log.isDebugEnabled()) {
                    log.debug("JQL query: " + jqlSearchQuery);
                }

                if (finalQuery != null) {
                    BooleanQuery join = new BooleanQuery();
                    join.add(finalQuery, BooleanClause.Occur.MUST);
                    join.add(query, BooleanClause.Occur.MUST);
                    finalQuery = join;
                } else {
                    finalQuery = query;
                }
            } else {
                if (log.isDebugEnabled()) {
                    log.debug("Got a null query from the JQL Query.");
                }
            }
        }

        // NOTE: we do this because when you are searching for everything the query is null
        if (finalQuery == null) {
            finalQuery = new MatchAllDocsQuery();
        }

        if (log.isDebugEnabled()) {
            log.debug("JQL lucene query: " + finalQuery);
        }
        return finalQuery;
    }

    private SearchResults search(final Query query, final ApplicationUser searcher, final PagerFilter pager, final org.apache.lucene.search.Query andQuery, final boolean overrideSecurity) throws SearchException {
        UtilTimerStack.push("Lucene Query");
        final TopDocs luceneMatches = getHits(query, searcher, getSearchSorts(searcher, query), andQuery, overrideSecurity, null, pager);
        UtilTimerStack.pop("Lucene Query");

        if (luceneMatches == null || luceneMatches.totalHits < pager.getStart()) {
            final int totalIssueCount = luceneMatches == null ? 0 : luceneMatches.totalHits;
            return new SearchResults(ImmutableList.of(), totalIssueCount, pager);
        }

        try {
            UtilTimerStack.push("Retrieve From cache/db and filter");

            final int end = Math.min(pager.getEnd(), luceneMatches.totalHits);
            final DocumentProvider documentProvider = new DocumentProvider(getIssueSearcher());
            final List<Issue> issues = Arrays.stream(luceneMatches.scoreDocs, pager.getStart(), end)
                    .map(scoreDoc -> scoreDoc.doc)
                    .map(documentProvider::fetchFromIndex)
                    .map(issueFactory::getIssue)
                    .collect(toNewArrayListWithCapacity(end - pager.getStart()));

            UtilTimerStack.pop("Retrieve From cache/db and filter");

            return new SearchResults(issues, luceneMatches.totalHits, pager);
        } catch (final RuntimeIOException e) {
            throw new SearchException("Exception whilst searching for issues " + e.getMessage(), e);
        }
    }

    private void searchAndSort(final Query query, final ApplicationUser user, final Collector collector, final PagerFilter pagerFilter, final boolean overrideSecurity) throws SearchException {
        final long start = System.currentTimeMillis();

        UtilTimerStack.push("Searching and sorting with Collector");
        try {
            final TopDocs hits = getHits(query, user, getSearchSorts(user, query), null, overrideSecurity, null, pagerFilter);
            if (hits != null) {
                if (collector instanceof TotalHitsAwareCollector) {
                    ((TotalHitsAwareCollector) collector).setTotalHits(hits.totalHits);
                }

                if (hits.totalHits >= pagerFilter.getStart()) {
                    final int end = Math.min(pagerFilter.getEnd(), hits.totalHits);
                    final IndexReader indexReader = getIssueSearcher().getIndexReader();
                    collector.setNextReader(indexReader, 0);
                    for (int i = pagerFilter.getStart(); i < end; i++) {
                        collector.collect(hits.scoreDocs[i].doc);
                    }
                }
            }
        } catch (IOException e) {
            throw new SearchException("Exception whilst searching for issues " + e.getMessage(), e);
        }

        UtilTimerStack.pop("Searching and sorting with Collector");
        ThreadLocalQueryProfiler.store(ThreadLocalQueryProfiler.LUCENE_GROUP, String.valueOf(query), (System.currentTimeMillis() - start));
    }

    private CachedWrappedFilterCache getCachedWrappedFilterCache() {
        CachedWrappedFilterCache cache = (CachedWrappedFilterCache) JiraAuthenticationContextImpl.getRequestCache().get(
                RequestCacheKeys.CACHED_WRAPPED_FILTER_CACHE);

        if (cache == null) {
            if (log.isDebugEnabled()) {
                log.debug("Creating new CachedWrappedFilterCache");
            }
            cache = new CachedWrappedFilterCache();
            JiraAuthenticationContextImpl.getRequestCache().put(RequestCacheKeys.CACHED_WRAPPED_FILTER_CACHE, cache);
        }

        return cache;
    }

    private Filter getPermissionsFilter(final boolean overRideSecurity, final ApplicationUser searchUser) {
        if (!overRideSecurity) {
            enablePermissionFilterCachingFlagWhenDoingMultipleSearches();

            if (getEnabledCachedWrapperFlag()) {
//              JRA-14980: first attempt to retrieve the filter from cache
                final CachedWrappedFilterCache cache = getCachedWrappedFilterCache();

                Filter filter = cache.getFilter(searchUser);
                if (filter != null) {
                    return filter;
                }

                // if not in cache, construct a query (also using a cache)
                final org.apache.lucene.search.Query permissionQuery = permissionsFilterGenerator.getQuery(searchUser);
                filter = new CachingWrapperFilter(new QueryWrapperFilter(permissionQuery));

                // JRA-14980: store the wrapped filter in the cache this is because the CachingWrapperFilter gives us an extra
                // benefit of precalculating its BitSet, and so we should use this for the duration of the request.
                cache.storeFilter(filter, searchUser);

                return filter;
            } else {
                final org.apache.lucene.search.Query permissionQuery = permissionsFilterGenerator.getQuery(searchUser);
                return new QueryWrapperFilter(permissionQuery);
            }
        } else {
            return null;
        }
    }

    private TopDocs runSearch(final IndexSearcher searcher, final org.apache.lucene.search.Query query, final Filter filter, final SortField[] sortFields, final String searchQueryString, final PagerFilter pager) throws IOException {
        log.debug("Lucene boolean Query: {}", query);

        UtilTimerStack.push("Lucene Search");

        final OpTimer opTimer = Instrumentation.pullTimer(InstrumentationName.ISSUE_INDEX_READS);
        try {
            final TopDocs hits = hasLimit(pager) ?
                    searchDocuments(query, filter, sortFields, pager.getEnd()) :
                    searchDocuments(query, filter, sortFields);

            // NOTE: this is only here so we can flag any queries in production that are taking long and try to figure out
            // why they are doing that.
            final long timeQueryTook = opTimer.end().getMillisecondsTaken();
            if (timeQueryTook > 400) {
                logSlowQuery(query, searchQueryString, timeQueryTook);
            }
            return hits;
        } finally {
            UtilTimerStack.pop("Lucene Search");
        }
    }

    private TopDocs searchDocuments(org.apache.lucene.search.Query query, Filter filter, SortField[] sortFields, int maxHits) throws IOException {
        final IndexSearcher issueSearcher = getIssueSearcher();
        return isNotEmpty(sortFields) ?
                issueSearcher.search(query, filter, maxHits, new Sort(sortFields)) :
                issueSearcher.search(query, filter, maxHits);
    }

    private TopDocs searchDocuments(org.apache.lucene.search.Query query, Filter filter, SortField[] sortFields) throws IOException {
        final TopDocs hits = searchDocuments(query, filter, sortFields, DEFAULT_SEARCH_LIMIT);
        eventPublisher.publish(new SearchUnknownResultSizeEvent(hits.totalHits));
        if (hits.totalHits <= DEFAULT_SEARCH_LIMIT) {
            return hits;
        }
        if (hits.totalHits > MAX_ALLOWED_SEARCH_LIMIT && isSearchLimited()) {
            final String message = format("Search for the query [%s] was performed and results count (%d) exceeds max allowed search limit (%d)." +
                            " Large result sets could lead to OutOfMemoryError. If you're sure you need to do the query you can remove the limit by enabling %s feature.",
                    query, hits.totalHits, MAX_ALLOWED_SEARCH_LIMIT, RESULTS_LIMIT_DISABLED_FEATURE_KEY);
            log.debug(message, new SearchException(message));
        } else {
            log.debug("Search for the query [{}] was performed and results count ({}) exceeds default limit ({})", query, hits.totalHits, DEFAULT_SEARCH_LIMIT);
        }
        return searchDocuments(query, filter, sortFields, hits.totalHits);
    }

    private boolean isSearchLimited() {
        return !featureManager.isEnabled(RESULTS_LIMIT_DISABLED_FEATURE_KEY);
    }

    private boolean hasLimit(PagerFilter pager) {
        return pager != null && pager.getEnd() > 0 && pager.getEnd() < Integer.MAX_VALUE;
    }

    private SortField[] getSearchSorts(final ApplicationUser searcher, Query query) {
        if (query == null) {
            return null;
        }

        List<SearchSort> sorts = searchSortUtil.getSearchSorts(query);

        final List<SortField> luceneSortFields = new ArrayList<SortField>();
        // When the sorts have been specifically set to null then we run the search with no sorts
        if (sorts != null) {
            final FieldManager fieldManager = ComponentAccessor.getFieldManager();

            for (SearchSort searchSort : sorts) {
                if (searchSort.getProperty().isDefined() && EntityPropertyType.isJqlClause(searchSort.getField())) {
                    final EntityPropertyType entityPropertyType = EntityPropertyType.getEntityPropertyTypeForClause(searchSort.getField());
                    final Property property = searchSort.getProperty().get();
                    luceneSortFields.add(new SortField(entityPropertyType.getIndexPrefix() + "_" + property.getAsPropertyString(), SortField.STRING, getSortOrder(searchSort, null)));
                } else {
                    // Lets figure out what field this searchSort is referring to. The {@link SearchSort#getField} method
                    //actually a JQL name.
                    final List<String> fieldIds = new ArrayList<String>(searchHandlerManager.getFieldIds(searcher, searchSort.getField()));
                    // sort to get consistent ordering of fields for clauses with multiple fields
                    Collections.sort(fieldIds);

                    for (String fieldId : fieldIds) {

                        if (fieldManager.isNavigableField(fieldId)) {
                            final NavigableField field = fieldManager.getNavigableField(fieldId);
                            luceneSortFields.addAll(field.getSortFields(getSortOrder(searchSort, field)));
                        } else {
                            log.debug("Search sort contains invalid field: " + searchSort);
                        }
                    }
                }
                if (jqlAliasManager.isJqlAlias(searchSort.getField())) {
                    Collection<ClauseHandler> clauseHandler = searchHandlerManager.getClauseHandler(searchSort.getField());
                    clauseHandler.stream()
                            .filter((handler) -> handler.getInformation() instanceof AliasClauseInformation)
                            .forEach((handler) ->
                            {
                                String indexField = handler.getInformation().getIndexField();
                                luceneSortFields.add(new SortField(indexField, new StringSortComparator(), getSortOrder(searchSort, null)));
                            });
                }
            }
        }

        return luceneSortFields.toArray(new SortField[luceneSortFields.size()]);
    }

    private boolean getSortOrder(final SearchSort searchSort, final NavigableField field) {
        boolean order;

        if (searchSort.getOrder() == null) {
            // We need to handle the case where the sort order is null, we will delegate off to the fields
            // default SearchSort for order in this case.
            String defaultSortOrder = field == null ? "DESC" : field.getDefaultSortOrder();
            if (defaultSortOrder == null) {
                order = false;
            } else {
                order = SortOrder.parseString(defaultSortOrder) == SortOrder.DESC;
            }
        } else {
            order = searchSort.isReverse();
        }
        return order;
    }

    protected void logSlowQuery(final org.apache.lucene.search.Query query, final String searchQueryString, final long timeQueryTook) {
        if (log.isDebugEnabled() || slowLog.isInfoEnabled()) {
            // truncate lucene query at 800 characters
            String msg = format("JQL query '%s' produced lucene query '%-1.800s' and took '%d' ms to run.", searchQueryString, query.toString(), timeQueryTook);
            if (log.isDebugEnabled()) {
                log.debug(msg);
            }
            if (slowLog.isInfoEnabled()) {
                slowLog.info(msg);
            }
        }
    }

    private void enablePermissionFilterCachingFlagWhenDoingMultipleSearches() {
        final Integer searchCounter = (Integer) JiraAuthenticationContextImpl.getRequestCache().get("filter.usage.count");

        if (searchCounter == null) {
            JiraAuthenticationContextImpl.getRequestCache().put("filter.usage.count", 1);
            return;
        }
        //if search counter equals one, this means we are performing second search and we should enable
        //permission filter caching. Once we enable this flag, we do not have to do it again
        if (searchCounter == 1 && !getEnabledCachedWrapperFlag()) {
            JiraAuthenticationContextImpl.getRequestCache().put(RequestCacheKeys.WRAP_PERMISSION_FILTER_IN_CACHED_WRAPPER_CACHE, Boolean.TRUE);
            if (log.isDebugEnabled()) {
                log.debug("Caching wrapper for permission filter was enabled, because more than one search during single request was performed");
            }
        }

        countAndLogSearchesInDevMode();
    }

    private void countAndLogSearchesInDevMode() {
        if (JiraSystemProperties.getInstance().isDevMode()) {
            final Integer devSearchCounter = (Integer) JiraAuthenticationContextImpl.getRequestCache().get("devmode.filter.usage.count");
            final Integer devNewSearchCounter = (devSearchCounter == null) ? 1 : devSearchCounter + 1;
            JiraAuthenticationContextImpl.getRequestCache().put("devmode.filter.usage.count", devNewSearchCounter);

            if (devSearchCounter != null && devSearchCounter >= 2 && !getEnabledCachedWrapperFlag()) {
                log.warn("This request asked for permission filter " + devNewSearchCounter + " times already " +
                        "and RequestCacheKeys.WRAP_PERMISSION_FILTER_IN_CACHED_WRAPPER_CACHE flag is set to 'false' while it should be set to 'true'");
            }
        }
    }

    private Boolean getEnabledCachedWrapperFlag() {
        final Boolean flag = (Boolean) JiraAuthenticationContextImpl.getRequestCache().get(RequestCacheKeys.WRAP_PERMISSION_FILTER_IN_CACHED_WRAPPER_CACHE);
        return (flag == null) ? Boolean.FALSE : flag;
    }
}
