package com.atlassian.jira.util.thread;

import com.atlassian.jira.cache.JiraVCacheInitialisationUtils;
import com.atlassian.jira.cache.request.RequestCacheController;
import com.atlassian.jira.instrumentation.DefaultInstrumentationListenerManager;
import com.atlassian.jira.issue.index.DefaultIndexManager;
import com.atlassian.jira.security.JiraAuthenticationContextImpl;
import com.atlassian.jira.util.ImportUtils;
import com.atlassian.jira.util.thread.JiraThreadLocalUtil.WarningCallback;
import com.atlassian.jira.web.filters.ThreadLocalQueryProfiler;
import org.apache.log4j.Logger;
import org.ofbiz.core.entity.TransactionUtil;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.IOException;

// WARNING: The JiraThreadLocalUtil component should be preferred over using this class
// directly because it is injectable (and therefore can be mocked).  However, there are
// some parts of JIRA (like ImportTaskManagerImpl) that can not use an injectable
// component.  Also, prior to JIRA v6.0, plugin developers had no way to accomplish
// this cleanup except to access this jira-core class directly.  Therefore,  this
// class must remain around.  Do not deprecate it, remove it, or move the logic into
// JiraThreadLocalUtilImpl without considering those obstacles, first.

/**
 * This class has static methods that perform a number of standard operations at the start and
 * end of "runnable code" such as a {@link com.atlassian.jira.service.JiraServiceContainerImpl}
 * or a {@link com.atlassian.jira.task.TaskManagerImpl}.  Plugin developers that have previously
 * used this class directly should change to using {@link JiraThreadLocalUtil} from the API,
 * so that they can avoid having to depend on {@code jira-core}.
 * <p>
 * The main purpose of this class is to setup and clear {@code ThreadLocal}
 * variables that can otherwise interfere with the smooth running of JIRA
 * by leaking resources or polluting the information used by the next request.
 * </p>
 * <p>
 * You MUST remember to call {@link #postCall(Logger, WarningCallback)} in a
 * {@code finally} block to guarantee correct behaviour.  For example:
 * </p>
 * <code><pre>
 * public void run()
 * {
 *     JiraThreadLocalUtils.preCall();
 *     try
 *     {
 *         // do runnable code here
 *     }
 *     finally
 *     {
 *         JiraThreadLocalUtils.postCall(log, myWarningCallback);
 *     }
 * }
 * </pre></code>
 *
 * @see JiraThreadLocalUtil
 * @since v3.13
 */
public class JiraThreadLocalUtils {

    /**
     * This should be called <strong>before</strong> any "runnable code" is called.
     * This will setup a clean {@code ThreadLocal} environment for the runnable
     * code to execute in.
     */
    public static void preCall() {
        //FIXME: this will not work in tenantless cloud implementation. This is tracked in JVC-25.
        JiraVCacheInitialisationUtils.initVCache(JiraVCacheInitialisationUtils.getFakeTenant());

        DefaultInstrumentationListenerManager.startContext(Thread.currentThread().getName());
        JiraAuthenticationContextImpl.clearRequestCache();
        RequestCacheController.startContext();
        ThreadLocalQueryProfiler.start();
    }

    /**
     * This should be called in a {@code finally} block to clear up {@code ThreadLocal}s
     * once the runnable stuff has been done.
     *
     * @param log             the log to write error messages to in casse of any problems
     * @param warningCallback the callback to invoke in case where problems are
     *                        detected after the runnable code is done running and its not cleaned up properly.
     *                        This may be {@code null}, in which case those problems are logged as errors.
     */
    public static void postCall(@Nonnull final Logger log, @Nullable final WarningCallback warningCallback) {
        try {
            ThreadLocalQueryProfiler.end();
        } catch (final IOException e) {
            log.error("Unable to call ThreadLocalQueryProfiler.end()", e);
        }

        DefaultIndexManager.flushThreadLocalSearchers();

        if (!ImportUtils.isIndexIssues()) {
            log.error("Indexing thread local not cleared. Clearing...");
            ImportUtils.setIndexIssues(true);
        }

        try {
            if (TransactionUtil.getLocalTransactionConnection() != null) {
                try {
                    if (warningCallback != null) {
                        warningCallback.onOpenTransaction();
                    } else {
                        log.error("Uncommitted database transaction detected.  Closing...");
                    }
                } finally {
                    // Close the connection and clear the thead local
                    TransactionUtil.closeAndClearThreadLocalConnection();
                }
            }
        } catch (Exception t) {
            log.error("Error while inspecting transaction thread local.", t);
        }
        RequestCacheController.closeContext();
        DefaultInstrumentationListenerManager.endContext();
        JiraVCacheInitialisationUtils.cleanupVCache();

    }
}
