package com.atlassian.jira.plugin.connect;

/**
 * A collection of properties relating to Connect Addons
 *
 * @since v7.0
 */
public class ConnectAddOnProperties {
    public static final String CONNECT_GROUP_NAME = "atlassian-addons";
    public static final String CONNECT_USER_PREFIX = "addon_";
}
