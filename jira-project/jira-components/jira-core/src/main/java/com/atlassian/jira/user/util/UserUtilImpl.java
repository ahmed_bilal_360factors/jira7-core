package com.atlassian.jira.user.util;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.collectors.CollectorsUtil;
import com.atlassian.crowd.embedded.api.CrowdService;
import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.crowd.exception.InvalidCredentialException;
import com.atlassian.crowd.exception.OperationNotPermittedException;
import com.atlassian.crowd.exception.runtime.OperationFailedException;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.search.query.membership.MembershipQuery;
import com.atlassian.jira.application.ApplicationRoleManager;
import com.atlassian.jira.bc.EntityNotFoundException;
import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.bc.JiraServiceContextImpl;
import com.atlassian.jira.bc.filter.SearchRequestService;
import com.atlassian.jira.bc.license.JiraLicenseService;
import com.atlassian.jira.bc.portal.PortalPageService;
import com.atlassian.jira.bc.project.component.MutableProjectComponent;
import com.atlassian.jira.bc.project.component.ProjectComponent;
import com.atlassian.jira.bc.project.component.ProjectComponentManager;
import com.atlassian.jira.bc.projectroles.ProjectRoleService;
import com.atlassian.jira.bc.user.UserService;
import com.atlassian.jira.bc.user.UserService.CreateUserRequest;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.entity.property.EntityPropertyType;
import com.atlassian.jira.entity.property.JsonEntityPropertyManager;
import com.atlassian.jira.event.user.UserEventType;
import com.atlassian.jira.exception.AddException;
import com.atlassian.jira.exception.CreateException;
import com.atlassian.jira.exception.PermissionException;
import com.atlassian.jira.exception.RemoveException;
import com.atlassian.jira.issue.comparator.UserCachingComparator;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchProvider;
import com.atlassian.jira.issue.security.IssueSecurityLevelManager;
import com.atlassian.jira.issue.subscription.SubscriptionManager;
import com.atlassian.jira.issue.vote.VoteManager;
import com.atlassian.jira.issue.watchers.WatcherManager;
import com.atlassian.jira.jql.builder.JqlQueryBuilder;
import com.atlassian.jira.license.LicenseCountService;
import com.atlassian.jira.notification.NotificationSchemeManager;
import com.atlassian.jira.notification.type.SingleUser;
import com.atlassian.jira.project.AssigneeTypes;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.security.login.LoginManager;
import com.atlassian.jira.security.roles.ProjectRoleActor;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.ApplicationUsers;
import com.atlassian.jira.user.UserDetails;
import com.atlassian.jira.user.UserHistoryManager;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.jira.util.dbc.Assertions;
import com.atlassian.query.Query;
import com.atlassian.seraph.spi.rememberme.RememberMeTokenDao;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.apache.commons.lang.StringUtils;
import org.ofbiz.core.entity.GenericEntityException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collection;
import java.util.Collections;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import static com.atlassian.jira.permission.GlobalPermissionKey.ADMINISTER;
import static com.atlassian.jira.permission.GlobalPermissionKey.SYSTEM_ADMIN;
import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * This is the default implementation of the UserUtil interface.
 */
public class UserUtilImpl implements UserUtil {
    public static final String USERNAME = "username";
    public static final String EMAIL = "email";
    public static final String DISPLAY_NAME = "fullname";
    public static final String DIRECTORY_NAME = "directoryName";
    public static final String DIRECTORY_ID = "directoryId";
    public static final String PASSWORD_TOKEN = "password.token";
    public static final String PASSWORD_HOURS = "password.hours";
    public static final String SEND_EMAIL = "sendEmail";

    private static final Logger log = LoggerFactory.getLogger(UserUtilImpl.class);

    private final IssueSecurityLevelManager issueSecurityLevelManager;
    private final GlobalPermissionManager globalPermissionManager;
    private final CrowdService crowdService;
    private final PermissionManager permissionManager;
    private final ApplicationProperties applicationProperties;
    private final SearchProvider searchProvider;
    private final ProjectManager projectManager;
    private final ProjectRoleService projectRoleService;
    private final ProjectComponentManager componentManager;
    private final SubscriptionManager subscriptionManager;
    private final NotificationSchemeManager notificationSchemeManager;
    private final UserHistoryManager userHistoryManager;
    private final UserManager userManager;
    private final ApplicationRoleManager applicationRoleManager;
    private final JsonEntityPropertyManager jsonEntityPropertyManager;

    private final LicenseCountService licenseCountService;

    public UserUtilImpl(final IssueSecurityLevelManager issueSecurityLevelManager,
                        final GlobalPermissionManager globalPermissionManager, final CrowdService crowdService,
                        final PermissionManager permissionManager, final ApplicationProperties applicationProperties,
                        final SearchProvider searchProvider, final ProjectManager projectManager,
                        final ProjectRoleService projectRoleService, final ProjectComponentManager componentManager,
                        final SubscriptionManager subscriptionManager, final NotificationSchemeManager notificationSchemeManager,
                        final UserHistoryManager userHistoryManager, final UserManager userManager,
                        final LicenseCountService licenseCountService, ApplicationRoleManager applicationRoleManager,
                        final JsonEntityPropertyManager jsonEntityPropertyManager) {
        this.issueSecurityLevelManager = issueSecurityLevelManager;
        this.globalPermissionManager = globalPermissionManager;
        this.permissionManager = permissionManager;
        this.applicationProperties = applicationProperties;
        this.searchProvider = searchProvider;
        this.projectManager = projectManager;
        this.projectRoleService = projectRoleService;
        this.componentManager = componentManager;
        this.subscriptionManager = subscriptionManager;
        this.notificationSchemeManager = notificationSchemeManager;
        this.userHistoryManager = userHistoryManager;
        this.userManager = userManager;
        this.crowdService = crowdService;
        this.licenseCountService = licenseCountService;
        this.applicationRoleManager = applicationRoleManager;
        this.jsonEntityPropertyManager = jsonEntityPropertyManager;
    }

    /**
     * A Factory method to get the SearchRequestService.  This helps break the cyclic dependency of SearchRequestService
     * to UserUtils AND allows test to override the value used.
     *
     * @return a SearchRequestService
     */
    protected SearchRequestService getSearchRequestService() {
        return ComponentAccessor.getComponent(SearchRequestService.class);
    }

    /**
     * A Factory method to get the PortalPageService.  This helps break the cyclic dependency of PortalPageService to
     * UserUtils AND allows test to override the value used.
     *
     * @return a PortalPageService
     */
    protected PortalPageService getPortalPageService() {
        return ComponentAccessor.getComponent(PortalPageService.class);
    }

    /**
     * Protected level factory method to allow for better test integration
     *
     * @param user the user in action
     * @return a new JiraServiceContext
     */
    protected JiraServiceContext getServiceContext(final ApplicationUser user) {
        return new JiraServiceContextImpl(user);
    }

    @Override
    public ApplicationUser createUserNoNotification(final String username, String password, final String emailAddress,
                                                    final String displayName)
            throws PermissionException, CreateException {
        return createUser(new UserDetails(username, displayName)
                .withPassword(password).withEmail(emailAddress), false, UserEventType.USER_CREATED, null);
    }

    @Override
    public ApplicationUser createUserNoNotification(String username, String password, String emailAddress,
                                                    String displayName, Long directoryId)
            throws PermissionException, CreateException {
        return createUser(new UserDetails(username, displayName)
                        .withPassword(password).withEmail(emailAddress).withDirectory(directoryId),
                false, UserEventType.USER_CREATED, null);
    }

    @Override
    public ApplicationUser createUserWithNotification(final String username, final String password, final String email,
                                                      final String fullname, final int userEventType) throws PermissionException, CreateException {
        return createUser(new UserDetails(username, fullname)
                .withPassword(password).withEmail(email).withDirectory(null), true, userEventType, null);
    }

    @Override
    public ApplicationUser createUserWithNotification(String username, String password, String email, String fullname,
                                                      Long directoryId, int userEventType) throws PermissionException, CreateException {
        return createUser(new UserDetails(username, fullname)
                .withPassword(password).withEmail(email).withDirectory(directoryId), true, userEventType, null);
    }

    @Nonnull
    public ApplicationUser createUser(@Nonnull final UserDetails userData, final boolean sendEmailNotification,
                                      final int eventType, @Nullable Set<ApplicationKey> applicationKeys)
            throws PermissionException, CreateException {

        CreateUserRequest createUserRequest = CreateUserRequest
                .withUserDetails(null, userData.getUsername(), userData.getPassword(), userData.getEmailAddress(), userData.getDisplayName())
                .inDirectory(userData.getDirectoryId().orElse(null))
                .sendNotification(sendEmailNotification)
                .withApplicationAccess(applicationKeys)
                .withEventUserEvent(eventType)
                .skipValidation();

        final UserService userService = ComponentAccessor.getComponent(UserService.class);
        return userService.createUser(userService.validateCreateUser(createUserRequest));
    }


    @Override
    public void removeUser(final ApplicationUser loggedInUser, final ApplicationUser user) {
        if (user == null) {
            return;
        }
        final SimpleErrorCollection errors = new SimpleErrorCollection();
        final UserManager.UserState state = userManager.getUserState(user);

        try {
            if (!state.isInMultipleDirectories()) {
                removeUserReferences(loggedInUser, user, errors);
            }

            try {
                // Cascading delete of group memberships
                // TODO: remove it once CWD-3138 is resolved, make appropriate fix
                final Iterable<String> userGroups = crowdService.search(getGroupMembershipQuery(user));
                for (String groupName : userGroups) {
                    crowdService.removeUserFromGroup(user.getDirectoryUser(), crowdService.getGroup(groupName));
                }

                crowdService.removeUser(user.getDirectoryUser());
            } catch (OperationNotPermittedException e) {
                throw new PermissionException(e);
            }
        } catch (final Exception e) {
            log.error("There was an error trying to remove user: " + user.getDisplayName(), e);
            throw new RuntimeException(e);
        }

        clearCache();
    }

    private void removeUserReferences(final ApplicationUser loggedInUser, final ApplicationUser user, final SimpleErrorCollection errors)
            throws RemoveException, GenericEntityException {
        projectRoleService.removeAllRoleActorsByNameAndType(user.getKey(), ProjectRoleActor.USER_ROLE_ACTOR_TYPE, errors);
        permissionManager.removeUserPermissions(user);
        removeWatchesForUser(user);
        removeVotesForUser(user);
        subscriptionManager.deleteSubscriptionsForUser(user);
        notificationSchemeManager.removeEntities(SingleUser.DESC, user.getName());
        removeComponentLeadsForUser(user);
        getSearchRequestService().deleteAllFiltersForUser(getServiceContext(loggedInUser), user);
        getPortalPageService().deleteAllPortalPagesForUser(user);
        userHistoryManager.removeHistoryForUser(user);
        jsonEntityPropertyManager.deleteByEntity(EntityPropertyType.USER_PROPERTY.getDbEntityName(), user.getId());
    }

    @Override
    public long getNumberOfReportedIssuesIgnoreSecurity(ApplicationUser loggedInUser, ApplicationUser user)
            throws SearchException {
        final Query query = JqlQueryBuilder.newBuilder().where().reporterUser(user.getUsername()).buildQuery();
        return searchProvider.searchCountOverrideSecurity(query, loggedInUser);
    }

    @Override
    public long getNumberOfAssignedIssuesIgnoreSecurity(ApplicationUser loggedInUser, ApplicationUser user)
            throws SearchException {
        final Query query = JqlQueryBuilder.newBuilder().where().assigneeUser(user.getUsername()).buildQuery();
        return searchProvider.searchCountOverrideSecurity(query, loggedInUser);
    }

    @Override
    public Collection<ProjectComponent> getComponentsUserLeads(final ApplicationUser user) {
        return componentManager.findComponentsByLead(user.getUsername());
    }

    @Override
    public Collection<Project> getProjectsLeadBy(final ApplicationUser user) {
        return projectManager.getProjectsLeadBy(user);
    }

    @SuppressWarnings("deprecation")
    @Override
    public boolean isNonSysAdminAttemptingToDeleteSysAdmin(final ApplicationUser loggedInUser, final ApplicationUser user) {
        return permissionManager.hasPermission(Permissions.SYSTEM_ADMIN, user) && !permissionManager.hasPermission(Permissions.SYSTEM_ADMIN,
                loggedInUser);
    }

    private void removeVotesForUser(final ApplicationUser userForDelete) {
        final VoteManager voteManager = ComponentAccessor.getVoteManager();
        // remove user's votes
        if (applicationProperties.getOption(APKeys.JIRA_OPTION_VOTING)) {
            voteManager.removeVotesForUser(userForDelete);
        }
    }

    private void removeWatchesForUser(final ApplicationUser userForDelete) {
        final WatcherManager watcherManager = ComponentAccessor.getWatcherManager();
        // remove user's watches
        if (applicationProperties.getOption(APKeys.JIRA_OPTION_WATCHING)) {
            watcherManager.removeAllWatchesForUser(userForDelete);
        }
    }

    private void removeComponentLeadsForUser(final ApplicationUser user) {
        for (final ProjectComponent component : getComponentsUserLeads(user)) {
            MutableProjectComponent newProjectComponent = MutableProjectComponent.copy(component);
            newProjectComponent.setLead(null);
            if (component.getAssigneeType() == AssigneeTypes.COMPONENT_LEAD) {
                newProjectComponent.setAssigneeType(AssigneeTypes.PROJECT_DEFAULT);
            }
            try {
                componentManager.update(newProjectComponent);
            } catch (EntityNotFoundException e) {
                // We only just fetched this component, but perhaps someone deleted it concurrently ...
            }
        }
    }

    @Override
    public void addUserToGroup(Group group, ApplicationUser userToAdd) throws PermissionException, AddException {
        doAddUserToGroup(group, userToAdd);
        clearCache();
    }

    private void clearCache() {
        try {
            if (issueSecurityLevelManager != null) {
                issueSecurityLevelManager.clearUsersLevels();
            }
        } catch (final UnsupportedOperationException uoe) {
            log.debug("Unsupported operation was thrown when trying to clear the issue security level manager cache", uoe);
        }
    }

    void doAddUserToGroup(final Group group, final ApplicationUser userToAdd) throws PermissionException, AddException {
        validateParameters(group, userToAdd);
        if (!crowdService.isUserMemberOfGroup(userToAdd.getDirectoryUser(), group)) {
            try {
                crowdService.addUserToGroup(userToAdd.getDirectoryUser(), group);
            } catch (OperationNotPermittedException e) {
                throw new PermissionException(e);
            } catch (OperationFailedException e) {
                throw new AddException(e);
            }
        }
    }

    @Override
    public void addUserToGroups(Collection<Group> groups, ApplicationUser userToAdd)
            throws PermissionException, AddException {
        try {
            for (final Group group : groups) {
                doAddUserToGroup(group, userToAdd);
            }
        } finally {
            clearCache();
        }
    }

    @Override
    public void removeUserFromGroup(final Group group, final ApplicationUser userToRemove)
            throws PermissionException, RemoveException {
        validateParameters(group, userToRemove);
        if (crowdService.isUserDirectGroupMember(userToRemove.getDirectoryUser(), group)) {
            try {
                crowdService.removeUserFromGroup(userToRemove.getDirectoryUser(), group);
            } catch (OperationNotPermittedException e) {
                throw new PermissionException(e);
            } catch (OperationFailedException e) {
                throw new RemoveException(e);
            }
        }
        clearCache();
    }

    @Override
    public void removeUserFromGroups(final Collection<Group> groups, final ApplicationUser userToRemove)
            throws PermissionException, RemoveException {
        for (final Group group : groups) {
            removeUserFromGroup(group, userToRemove);
        }
        clearCache();
    }

    @Override
    public PasswordResetToken generatePasswordResetToken(final ApplicationUser user) {
        return new PasswordResetTokenBuilder(crowdService).generateToken(user);
    }

    @Override
    public PasswordResetTokenValidation validatePasswordResetToken(final ApplicationUser user, final String token) {
        Assertions.notNull("user", user);

        final PasswordResetTokenValidation.Status status = new PasswordResetTokenBuilder(crowdService).validateToken(user, token);
        return () -> status;
    }

    @Override
    public void changePassword(final ApplicationUser user, final String newPassword)
            throws PermissionException, InvalidCredentialException {
        Assertions.notNull("user", user);

        new PasswordResetTokenBuilder(crowdService).resetToken(user);

        ComponentAccessor.getComponent(RememberMeTokenDao.class).removeAllForUser(user.getName());
        ComponentAccessor.getComponent(LoginManager.class).resetFailedLoginCount(user);

        try {
            crowdService.updateUserCredential(user.getDirectoryUser(), newPassword);
        } catch (OperationNotPermittedException e) {
            throw new PermissionException(e);
        } catch (InvalidCredentialException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public int getActiveUserCount() {
        return licenseCountService.totalBillableUsers();
    }

    @Override
    public int getTotalUserCount() {
        return userManager.getTotalUserCount();
    }

    @Override
    public void clearActiveUserCount() {
        licenseCountService.flush();
    }

    @Override
    public boolean canActivateNumberOfUsers(final int numUsers) {
        notNull("numUsers is null.", numUsers);
        if (numUsers < 0) {
            throw new IllegalArgumentException("numUsers must be non-negative");
        }
        if (numUsers == 0) {
            return true;
        }

        final Set<ApplicationKey> defaultApps = applicationRoleManager.getDefaultApplicationKeys();
        if (defaultApps.isEmpty()) {
            log.warn("No default Applications configured for JIRA. User won't be added to any groups.");
        }
        return defaultApps.stream()
                .allMatch(appKey -> applicationRoleManager.hasSeatsAvailable(appKey, numUsers));
    }

    @Nonnull
    @Override
    public Collection<ApplicationUser> getUsers() {
        // UserManager has a special shortcut for all users that is quicker than a normal Crowd search
        return userManager.getUsers();
    }

    @Nonnull
    @Override
    public Collection<ApplicationUser> getAllApplicationUsers() {
        return userManager.getAllApplicationUsers();
    }

    private User getUserCwd(final String userName) {
        return crowdService.getUser(userName);
    }

    /**
     * Get a User by name.
     *
     * @param userName the name of the user
     * @return a User
     */
    public ApplicationUser getUser(final String userName) {
        if (StringUtils.isNotEmpty(userName)) {
            return getUserByName(userName);
        }
        return null;
    }

    @Override
    public ApplicationUser getUserByKey(String key) {
        if (key == null) {
            return null;
        }
        return userManager.getUserByKey(key);
    }

    @Override
    public ApplicationUser getUserByName(String username) {
        return userManager.getUserByName(username);
    }

    @Override
    public ApplicationUser getUserObject(String userName) {
        return getUser(userName);
    }

    public boolean userExists(final String userName) {
        return StringUtils.isNotEmpty(userName) && getUserCwd(userName) != null;
    }

    public Collection<ApplicationUser> getAdministrators() {
        return getJiraAdministrators();
    }

    @Override
    public Collection<ApplicationUser> getJiraAdministrators() {
        return getAllUsersInGroups(globalPermissionManager.getGroupsWithPermission(ADMINISTER));
    }

    public Collection<ApplicationUser> getSystemAdministrators() {
        return getJiraSystemAdministrators();
    }

    @Override
    public Collection<ApplicationUser> getJiraSystemAdministrators() {
        return getAllUsersInGroups(globalPermissionManager.getGroupsWithPermission(SYSTEM_ADMIN));
    }

    public void addToJiraUsePermission(final ApplicationUser user) {
        // JRA-10393: only add user to USE groups if by doing so we will not exceed the user limit
        if (canActivateNumberOfUsers(1)) {
            //JRA-22984 Prevent new users from being added to the system administrators group
            final Collection<Group> groups = applicationRoleManager.getDefaultApplicationKeys().stream()
                    .flatMap(appKey -> applicationRoleManager.getDefaultGroups(appKey).stream())
                    .collect(CollectorsUtil.toImmutableSet());

            for (final Group group : groups) {
                try {
                    doAddUserToGroup(group, user);
                } catch (PermissionException | AddException ignored) {
                    // Ignore and try the rest (based on pre-Crowd behaviour)
                }
            }
            clearCache();
        }
    }


    @Override
    public String getDisplayableNameSafely(ApplicationUser user) {
        if (user == null) {
            return null;
        }

        final String fullName = user.getDisplayName();
        if (StringUtils.isNotBlank(fullName)) {
            return fullName;
        }
        return user.getUsername();
    }

    public SortedSet<ApplicationUser> getAllUsersInGroups(final Collection<Group> groups) {
        return getUsersInGroups(groups);
    }

    public SortedSet<ApplicationUser> getUsersInGroups(final Collection<Group> groups) {
        notNull("groups", groups);
        final Collection<String> groupNames = Lists.newArrayListWithCapacity(groups.size());
        for (final Group group : groups) {
            if (group != null) {
                groupNames.add(group.getName());
            }
        }
        return getUsersInGroupNames(groupNames);
    }

    public SortedSet<ApplicationUser> getAllUsersInGroupNames(final Collection<String> groupNames) {
        Collection<ApplicationUser> allUsersUnsorted = getAllUsersInGroupNamesUnsorted(groupNames);
        TreeSet<ApplicationUser> allUsersSorted = Sets.newTreeSet(new UserCachingComparator());
        allUsersSorted.addAll(allUsersUnsorted);
        return allUsersSorted;
    }

    public Set<ApplicationUser> getAllUsersInGroupNamesUnsorted(Collection<String> groupNames) {
        notNull("groupNames", groupNames);
        Set<ApplicationUser> setOfUsers = Sets.newHashSet();
        for (final String groupName : groupNames) {
            if (groupName != null) {
                Iterable<User> users = getGroupMembers(groupName);
                setOfUsers.addAll(ApplicationUsers.from(users));
            }
        }
        return Collections.unmodifiableSet(setOfUsers);
    }

    public SortedSet<ApplicationUser> getUsersInGroupNames(final Collection<String> groupNames) {
        return getAllUsersInGroupNames(groupNames);
    }

    public SortedSet<Group> getGroupsForUser(final String userName) {
        notNull("userName", userName);
        final SortedSet<Group> setOfGroups = new TreeSet<>();

        Iterable<Group> groups = getGroupsForUserFromCrowd(userName);
        for (Group group : groups) {
            setOfGroups.add(group);
        }
        return Collections.unmodifiableSortedSet(setOfGroups);
    }

    public SortedSet<String> getGroupNamesForUser(final String userName) {
        notNull("userName", userName);
        final SortedSet<String> setOfGroups = new TreeSet<>();

        Iterable<String> groups = getGroupNamesForUserFromCrowd(userName);
        for (String groupName : groups) {
            setOfGroups.add(groupName);
        }
        return Collections.unmodifiableSortedSet(setOfGroups);
    }

    private Group getGroupCwd(final String groupName) {
        return crowdService.getGroup(groupName);
    }

    /**
     * Get a Group by name.
     *
     * @param groupName the name of the group
     * @return a Group
     */
    public Group getGroup(final String groupName) {
        if (StringUtils.isNotEmpty(groupName)) {
            return getGroupCwd(groupName);
        }
        return null;
    }

    @Override
    public Group getGroupObject(@Nullable String groupName) {
        return getGroup(groupName);
    }

    private void validateParameters(final Group group, final ApplicationUser userParam) {
        if (group == null) {
            throw new IllegalArgumentException("Group must not be null if trying to add or delete a user from it.");
        }
        if (userParam == null) {
            throw new IllegalArgumentException("User must not be null if trying to add or delete them from a group.");
        }
    }

    JiraLicenseService getLicenseService() {
        return ComponentAccessor.getComponent(JiraLicenseService.class);
    }

    private Iterable<User> getGroupMembers(final String groupName) {
        final com.atlassian.crowd.search.query.membership.MembershipQuery<User> membershipQuery =
                QueryBuilder.queryFor(User.class, EntityDescriptor.user()).childrenOf(EntityDescriptor.group()).withName(groupName).returningAtMost(EntityQuery.ALL_RESULTS);
        return crowdService.search(membershipQuery);
    }

    private Iterable<Group> getGroupsForUserFromCrowd(final String userName) {
        final com.atlassian.crowd.search.query.membership.MembershipQuery<Group> membershipQuery =
                QueryBuilder.queryFor(Group.class, EntityDescriptor.group()).parentsOf(EntityDescriptor.user()).withName(userName).returningAtMost(EntityQuery.ALL_RESULTS);

        return crowdService.search(membershipQuery);
    }

    private Iterable<String> getGroupNamesForUserFromCrowd(final String userName) {
        final com.atlassian.crowd.search.query.membership.MembershipQuery<String> membershipQuery =
                QueryBuilder.queryFor(String.class, EntityDescriptor.group()).parentsOf(EntityDescriptor.user()).withName(userName).returningAtMost(EntityQuery.ALL_RESULTS);

        return crowdService.search(membershipQuery);
    }

    private MembershipQuery<String> getGroupMembershipQuery(final ApplicationUser user) {
        return QueryBuilder.queryFor(String.class, EntityDescriptor.group())
                .parentsOf(EntityDescriptor.user())
                .withName(user.getName())
                .returningAtMost(EntityQuery.ALL_RESULTS);
    }
}
