package com.atlassian.jira.upgrade.tasks;

import com.atlassian.jira.database.DbConnection;
import com.atlassian.jira.database.QueryDslAccessor;
import com.atlassian.upgrade.api.UpgradeContext;
import com.atlassian.upgrade.spi.UpgradeTask;
import com.google.common.annotations.VisibleForTesting;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.stream.Collectors;

import static com.atlassian.jira.model.querydsl.QUpgradeHistory.UPGRADE_HISTORY;
import static com.atlassian.jira.model.querydsl.QUpgradeTaskHistory.UPGRADE_TASK_HISTORY;

/**
 * <p>
 * With the implementation of the new upgrade task framework we will begin to use a new table UpgradeTaskHistory to
 * store all of the entries for the Upgrades. To simplify the implementation of the new framework, when we complete a
 * new entry we always put it in the new table. However, this new framework could be used to run old upgrade tasks that
 * were made before this new framework. In this case we want to make sure that all of these upgrade tasks completion go
 * into the old table.
 * </p>
 * <p>
 * A sample scenario for this UpgradeTask is in the case of importing data with the following information in the
 * UpgradeHistory table:
 * <ul>
 *     <li>5000</li>
 *     <li>6300</li>
 *     <li>71001</li>
 * </ul>
 * </p>
 * <p>
 * We need to run all upgrade tasks, e.g. to 74000, and it will use the new upgrade framework. All upgrades run will be
 * placed into the UpgradeTaskHistory table like so:
 * <ul>
 *    <li>71002</li>
 *    <li>72001</li>
 *    <li>72002</li>
 *    <li>73010</li>
 * </ul>
 * </p>
 * <p>
 * At this point we want to move all upgrade tasks before 73010 to the old table. The result would be the following for
 * the UpgradeHistoryTable:
 * <ul>
 *    <li>5000</li>
 *    <li>6300</li>
 *    <li>71001</li>
 *    <li>71002</li>
 *    <li>72001</li>
 *    <li>72002</li>
 * </ul>
 * </p>
 * <p>
 * And the following content would be in the new UpgradeTaskHistory table:
 * <ul>
 *    <li>73010</li>
 * </ul>
 * </p>
 * <p>
 * Any more upgrade tasks after this would be kept in the new UpgradeTaskTable.
 * </p>
 */
public class UpgradeTask_Build73010 implements UpgradeTask {
    @VisibleForTesting
    static final Integer BUILD_NUMBER = 73010;

    private static final Logger log = LoggerFactory.getLogger(UpgradeTask_Build73010.class);

    private final QueryDslAccessor queryDslAccessor;

    public UpgradeTask_Build73010(final QueryDslAccessor queryDslAccessor) {
        this.queryDslAccessor = queryDslAccessor;
    }

    @Override
    public int getBuildNumber() {
        return BUILD_NUMBER;
    }

    @Override
    public String getShortDescription() {
        return "Moves any of the old upgrade task build numbers from the new table back to the old table as when they " +
                "are executed in the new framework they are placed into the new UpgradeTaskHistory table which is not " +
                "desired.";
    }

    @Override
    public void runUpgrade(UpgradeContext upgradeContext) {
        log.info("Starting: migration upgrade task {}", getBuildNumber());
        queryDslAccessor.execute(connection -> {
            List<Integer> buildNumbersInNewTable = getOldUpgradesCompletedInTheNewTable(connection);

            // We want to delete any of the upgrades completed in the new table from the old table to remove any
            // primary key violations. This could happen if it was pending in the old table but completed in the
            // new table.
            removeUpgradesFromOldTable(connection, buildNumbersInNewTable);

            removeUpgradesFromNewTable(connection, buildNumbersInNewTable);

            addUpgradesToOldTable(connection, buildNumbersInNewTable);
        });

        log.info("Finished: migration upgrade task {}", getBuildNumber());
    }

    @VisibleForTesting
    void addUpgradesToOldTable(DbConnection connection, List<Integer> buildNumbersInNewTable) {
        buildNumbersInNewTable.forEach(buildNumber -> connection
                .insert(UPGRADE_HISTORY)
                .set(UPGRADE_HISTORY.upgradeclass, getOldUpgradeTaskClassName(buildNumber))
                .set(UPGRADE_HISTORY.targetbuild, String.valueOf(buildNumber))
                .set(UPGRADE_HISTORY.status, "complete")
                .set(UPGRADE_HISTORY.downgradetaskrequired, "N")
                .executeWithId());
    }

    private void removeUpgradesFromNewTable(DbConnection connection, List<Integer> buildNumbersInNewTable) {
        connection
                .delete(UPGRADE_TASK_HISTORY)
                .where(UPGRADE_TASK_HISTORY.buildNumber.in(buildNumbersInNewTable))
                .execute();
    }

    private void removeUpgradesFromOldTable(DbConnection connection, List<Integer> buildNumbersInNewTable) {
        List<String> upgradeClassNames = buildNumbersInNewTable.stream()
                .map(this::getOldUpgradeTaskClassName)
                .collect(Collectors.toList());

        connection.delete(UPGRADE_HISTORY)
                .where(UPGRADE_HISTORY.upgradeclass.in(upgradeClassNames))
                .execute();
    }

    private List<Integer> getOldUpgradesCompletedInTheNewTable(DbConnection connection) {
        return connection.newSqlQuery()
                .select(UPGRADE_TASK_HISTORY.buildNumber)
                .from(UPGRADE_TASK_HISTORY)
                .where(UPGRADE_TASK_HISTORY.buildNumber.lt(getBuildNumber()))
                .fetch();
    }

    /**
     * Manually forms this class as we know that these classes before this upgrade task follow the format.
     *
     * @param buildNumber for the upgrade task
     * @return the class name for the upgrade task
     */
    private String getOldUpgradeTaskClassName(final Integer buildNumber) {
        return "com.atlassian.jira.upgrade.tasks.UpgradeTask_Build" + buildNumber;
    }
}
