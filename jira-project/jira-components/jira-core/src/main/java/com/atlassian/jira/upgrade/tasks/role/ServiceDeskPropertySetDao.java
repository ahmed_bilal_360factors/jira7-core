package com.atlassian.jira.upgrade.tasks.role;

/**
 * Allows to set {@link com.opensymphony.module.propertyset.PropertySet} for Service Desk namespace.
 *
 * @since 7.0
 */
public interface ServiceDeskPropertySetDao {
    /**
     * Writes string property.
     *
     * @param key   of the property
     * @param value of the property.
     */
    void writeStringProperty(String key, String value);
}
