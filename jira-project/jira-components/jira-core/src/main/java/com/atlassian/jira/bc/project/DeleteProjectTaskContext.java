package com.atlassian.jira.bc.project;

import com.atlassian.jira.project.Project;

/**
 * Context for project disrupting operations. There can be only one such operation
 * per project at any given time.
 *
 * @since v7.1.1
 */
public class DeleteProjectTaskContext extends ProjectTaskContext {

    public DeleteProjectTaskContext(final Project project) {
        super(project);
    }

    @Override
    public String buildProgressURL(final Long taskId) {
        return "/secure/project/DeleteProjectProgress.jspa?pid=" + projectId + "&taskId=" + taskId;
    }

}
