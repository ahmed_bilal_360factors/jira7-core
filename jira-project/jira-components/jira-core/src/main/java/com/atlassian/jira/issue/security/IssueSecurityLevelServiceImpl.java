package com.atlassian.jira.issue.security;

import com.atlassian.jira.bc.ServiceOutcome;
import com.atlassian.jira.bc.ServiceOutcomeImpl;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.Predicate;
import com.atlassian.jira.util.SimpleErrorCollection;

import java.util.function.Function;
import java.util.function.Supplier;

public class IssueSecurityLevelServiceImpl implements IssueSecurityLevelService {

    private final IssueSecurityLevelManager levelManager;

    private final GlobalPermissionManager globalPermissionManager;
    private final I18nHelper i18n;

    private final int MAX_LEVEL_LENGTH = 255;

    public IssueSecurityLevelServiceImpl(IssueSecurityLevelManager levelManager,
                                         GlobalPermissionManager globalPermissionManager,
                                         I18nHelper i18n) {
        this.levelManager = levelManager;
        this.globalPermissionManager = globalPermissionManager;
        this.i18n = i18n;
    }

    @Override
    public UpdateValidationResult validateUpdate(ApplicationUser user, IssueSecurityLevel currentLevel, String newName, String newDescription) {
        requireNotNull(currentLevel.getId(), "currentLevel.id");
        ErrorCollection errors = new SimpleErrorCollection();
        errors.addErrorCollection(validateLevelName(newName));
        errors.addErrorCollection(validateDuplicates(currentLevel, newName));
        errors.addErrorCollection(validatePermissions(user));

        if (errors.hasAnyErrors()) {
            return new UpdateValidationResult(null, errors);
        }
        return new UpdateValidationResult(new IssueSecurityLevelImpl(currentLevel.getId(), newName, newDescription, currentLevel.getSchemeId()), errors);
    }

    @Override
    public ServiceOutcome<IssueSecurityLevel> update(ApplicationUser callingUser, UpdateValidationResult validationResult) {
        return checkIsAdminAndPerform(callingUser,
                validationResult,
                (vr) -> {
                    return new ServiceOutcomeImpl<IssueSecurityLevel>(vr.getErrors(), levelManager.updateIssueSecurityLevel(vr.getLevel()));
                });
    }

    @Override
    public CreateValidationResult validateCreate(ApplicationUser user, long schemeId, String name, String description) {
        ErrorCollection errors = new SimpleErrorCollection();
        errors.addErrorCollection(validateLevelName(name));
        errors.addErrorCollection(validateDuplicates(name, schemeId));
        errors.addErrorCollection(validatePermissions(user));
        if (errors.hasAnyErrors()) {
            return new CreateValidationResult(null, errors);
        }
        return new CreateValidationResult(new IssueSecurityLevelImpl(null, name, description, schemeId), errors);
    }

    @Override
    public ServiceOutcome<IssueSecurityLevel> create(ApplicationUser callingUser, CreateValidationResult validationResult) {
        return checkIsAdminAndPerform(callingUser,
                validationResult,
                (vr) -> new ServiceOutcomeImpl<IssueSecurityLevel>(vr.getErrors(), levelManager.createIssueSecurityLevel(vr.getLevel())));
    }

    private boolean isDuplicatedIssueSecurityLevelName(String levelName, Long schemeId) {
        return levelName != null && levelManager.getSecurityLevelByNameAndSchema(levelName.trim(), schemeId) != null;
    }

    private boolean isEmptyIssueSecurityLevelName(String levelName) {
        return levelName == null || levelName.trim().isEmpty();
    }

    private boolean isTooLongIssueSecurityLevelName(String levelName) {
        return levelName != null && levelName.length() > MAX_LEVEL_LENGTH;
    }

    private ErrorCollection validateLevelName(String levelName) {
        ErrorCollection errors = new SimpleErrorCollection();
        if (isEmptyIssueSecurityLevelName(levelName)) {
            errors.addError("name", i18n.getText("admin.errors.specify.name.for.security"));
        }
        if (isTooLongIssueSecurityLevelName(levelName)) {
            errors.addError("name", i18n.getText("admin.errors.level.name.too.long"));
        }
        return errors;
    }

    private ErrorCollection validateDuplicates(IssueSecurityLevel currentLevel, String newLevelName) {
        if (newLevelName != null && newLevelName.equalsIgnoreCase(currentLevel.getName())) {
            return new SimpleErrorCollection();
        }
        return validateDuplicates(newLevelName, currentLevel.getSchemeId());
    }

    private ErrorCollection validatePermissions(ApplicationUser user) {
        ErrorCollection errors = new SimpleErrorCollection();
        if (!isAdmin(user)) {
            String forbiddenMessage = i18n.getText("admin.schemes.permissions.forbidden");
            errors.addErrorMessage(forbiddenMessage);
        }
        return errors;
    }

    private ErrorCollection validateDuplicates(String newLevelName, long schemeId) {
        ErrorCollection errors = new SimpleErrorCollection();
        if (isDuplicatedIssueSecurityLevelName(newLevelName, schemeId)) {
            errors.addError("name", i18n.getText("admin.errors.security.level.with.name.already.exists"));
        }
        return errors;
    }


    private <T> ServiceOutcome<T> checkIsAdminAndPerform(ApplicationUser callingUser, LevelValidationResult validationResult, Function<LevelValidationResult, ServiceOutcome<T>> actionOnValidationResult) {
        requireNotNull(validationResult, "validationResult");
        if (!validationResult.isValid()) {
            throw new IllegalStateException("You can not create an IssueSecurityLevel with an invalid validation result.");
        }
        return checkPermissionsPerformAction(callingUser,
                (user) -> isAdmin(user),
                () -> actionOnValidationResult.apply(validationResult));

    }

    private void requireNotNull(Object toCheck, String toCheckName) {
        if (toCheck == null) {
            throw new IllegalArgumentException(String.format("%s can't be null.", toCheckName));
        }
    }

    private <T> ServiceOutcome<T> checkPermissionsPerformAction(ApplicationUser user, Predicate<ApplicationUser> hasPermissions, Supplier<ServiceOutcome<T>> action) {
        String forbiddenMessage = i18n.getText("admin.schemes.permissions.forbidden");
        if (user == null) {
            return ServiceOutcomeImpl.error(forbiddenMessage, ErrorCollection.Reason.NOT_LOGGED_IN);
        }
        if (!hasPermissions.evaluate(user)) {
            return ServiceOutcomeImpl.error(forbiddenMessage, ErrorCollection.Reason.FORBIDDEN);
        }
        return action.get();
    }

    private boolean isAdmin(final ApplicationUser user) {
        return globalPermissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, user);
    }

}
