package com.atlassian.jira.crowd.embedded.ofbiz;

import com.atlassian.jira.util.Visitor;

import java.util.Collection;
import java.util.List;

/**
 * The User Cache is used to enhance the performance of access to user data.
 *
 * @since v7.0
 */
interface OfBizUserCache {
    /**
     * Retrieve a User by userName case insensitively.
     *
     * @param directoryId The directory.
     * @param userName    User name.
     * @return A user.
     */
    OfBizUser getCaseInsensitive(long directoryId, String userName);

    /**
     * Retrieve all Users by id given a list of ids.
     *
     * @param directoryId The directory.
     * @param userIds     List of user Ids
     * @return A list of {@link OfBizUser}.
     */
    List<OfBizUser> getAllCaseInsensitive(long directoryId, Collection<String> userIds);

    /**
     * Remove a user from the cache.
     *
     * @param directoryId The directory.
     * @param userName    User name.
     */
    void remove(long directoryId, String userName);

    /**
     * Remove a user from the cache.
     *
     * @param key The Users key
     */
    void remove(DirectoryEntityKey key);

    /**
     * Visit all the users in the directory.
     * Some implementations may process this directly from the underlying persistent store rather than the
     * cache if they do not keep a canonical set of all users in the cache.
     *
     * @param directoryId The directory
     * @param visitor     A Visitor
     */
    void visitAllUserIdsInDirectory(long directoryId, Visitor<String> visitor);

    /**
     * Refresh the cache entry for the supplied user.
     * Should be called when a user is added or updated.  The implementation may do this
     * either by storing he supplied value or refreshing the data from the underlying persistent store.
     *
     * @param user User to be refreshed in the cache
     * @return The key of the user.
     */
    DirectoryEntityKey refresh(OfBizUser user);

    /**
     * Refresh all entries in the cache.
     */
    void refresh();

    /**
     * Return true if the cache is initialised and can be used.
     * JIRA relies heavily on having access to users and if the cache needs time to initialise before it
     * can be relied on to provide correct results, JIRA needs to wait.
     *
     * @return true if initialised.
     */
    boolean isCacheInitialized();
}
