package com.atlassian.jira.startup;

import com.atlassian.jira.bc.ServiceOutcomeImpl;
import com.atlassian.jira.bc.ServiceResult;
import com.atlassian.jira.config.properties.JiraProperties;
import com.atlassian.jira.util.StopWatch;
import com.atlassian.jira.util.johnson.JohnsonProvider;
import com.atlassian.johnson.event.Event;
import com.atlassian.johnson.event.EventLevel;
import com.atlassian.johnson.event.EventType;
import org.apache.log4j.MDC;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

import static com.atlassian.jira.component.ComponentAccessor.getComponentOfType;

public class DefaultInstantUpgradeManager implements InstantUpgradeManager {

    private static final Logger log = LoggerFactory.getLogger(DefaultInstantUpgradeManager.class);
    private static final String LOG_KEY = "jira.launcher.start.instanceenabled.millis";
    private static HoldingCounters holdingCounters = new HoldingCounters();

    private final AtomicReference<StartupTask> taskReference = new AtomicReference<>();
    private final AtomicBoolean activated = new AtomicBoolean(false);
    //This latch will reach zero once JIRA is fully started
    private final CountDownLatch fullyStarted = new CountDownLatch(1);
    private final JiraProperties jiraProperties;

    public DefaultInstantUpgradeManager(final JiraProperties jiraProperties) {
        this.jiraProperties = jiraProperties;
    }

    public void doNowOrWhenInstanceBecomesActive(Runnable task, String description) {
        if (!isInstantUpgradeEnabled()) {
            runTask(new StartupTask(task, description)); // Run immediately
        } else if (taskReference.compareAndSet(null, new StartupTask(task, description))) {
            log.info("Instant Upgrade in progress. Task [{}] registered. Awaiting instance to be activated", description);
        } else {
            throw new IllegalStateException("Instance Upgrade task has already been registered");
        }
    }

    private void runTask(StartupTask task) {
        try {
            log.info("Now running [{}]", task.getDescription());
            StopWatch stopWatch = new StopWatch();

            task.run();

            long elapsedTime = stopWatch.getTotalTime();
            MDC.put(LOG_KEY, elapsedTime);
            log.info("{} took {}s", task.getDescription(), elapsedTime / 1000);
            MDC.remove(LOG_KEY);
        } finally {
            fullyStarted.countDown();
        }
    }

    @Override
    public State getState() {
        if (!isInstantUpgradeEnabled()) {
            return State.DISABLED;
        } else if (hasErrorOrFatalJohnsonEvents()) {
            return State.ERROR;
        } else if (fullyStarted.getCount() == 0) {
            return State.FULLY_STARTED;
        } else if (taskReference.get() == null) {
            //DefaultJiraLauncher doesn't register a StartupTask until the end of early startup"
            return State.EARLY_STARTUP;
        } else if (!activated.get()) {
            return State.WAITING_FOR_ACTIVATION;
        } else {
            return State.LATE_STARTUP;
        }
    }

    public ServiceResult activateInstance() {
        if (!isInstantUpgradeEnabled()) {
            return ServiceOutcomeImpl.error("Instance upgrade is NOT enabled, ignoring instance activation request.");
        } else if (taskReference.get() == null) {
            return ServiceOutcomeImpl.error("Instance upgrade task was not set, ignoring instance activation request.");
        } else if (!activated.compareAndSet(false, true)) {
            return ServiceOutcomeImpl.error("Instance is already activated, ignoring instance activation request.");
        } else {
            return runDelayedTask();
        }
    }
    
    private ServiceOutcomeImpl runDelayedTask(){
        try {
            runTask(taskReference.get());
            return ServiceOutcomeImpl.ok(null);
        } catch (Exception e) {
            final JohnsonProvider johnsonProvider = getComponentOfType(JohnsonProvider.class);
            log.error("Unable to start JIRA.", e);
            String errorMessage = "Unexpected exception during JIRA startup. This JIRA instance will not be able to recover. Please check the logs for details.";
            johnsonProvider.getContainer().addEvent(new Event(EventType.get("startup-unexpected"), errorMessage,
                    EventLevel.get("fatal")));
            return ServiceOutcomeImpl.error(errorMessage);
        } finally {
            holdingCounters.logInformation();
        }
    }

    private boolean isInstantUpgradeEnabled() {
        return jiraProperties.getBoolean(INSTANT_UPGRADE);
    }

    private boolean hasErrorOrFatalJohnsonEvents() {
        Collection<Event> events = getComponentOfType(JohnsonProvider.class).getContainer().getEvents();
        boolean hasErrors =  events.stream().anyMatch(event -> event.getLevel() != EventLevel.get(EventLevel.WARNING));
        if (hasErrors) {
            log.debug("Application state is ERROR");
        }
        return hasErrors;
    }

    private static class StartupTask implements Runnable {
        private final Runnable runnable;
        private final String description;

        public StartupTask(Runnable runnable, String description) {
            this.runnable = runnable;
            this.description = description;
        }

        public String getDescription() {
            return description;
        }

        public void run() {
            this.runnable.run();
        }
    }
    
    public boolean waitTillFullyStarted(long time, TimeUnit unit) throws InterruptedException {
        holdingCounters.incrementWaiting();
        boolean countReachedZero = fullyStarted.await(time, unit);
        if (!countReachedZero) {
            holdingCounters.timeOut();
        }
        return countReachedZero;
    }

    /**
     * Logs information about {@link com.atlassian.jira.web.filters.InstantUpgradeHoldingFilter}.
     * The reason for placing it here is that only InstantUpgraeManager has control over when to log.
     */
    private static class HoldingCounters {
        private static final String NUMBER_HELD_REQUESTS = "jira.instant.holdingfilter.total.held";
        private static final String NUMBER_RELEASED_REQUESTS = "jira.instant.holdingfilter.total.released";
        private static final String NUMBER_TIMEDOUT_REQUESTS = "jira.instant.holdingfilter.total.timedout";

        private AtomicInteger timedOut = new AtomicInteger(0);
        private AtomicInteger allThreads = new AtomicInteger(0);

        public void incrementWaiting() {
            allThreads.incrementAndGet();
        }
        
        public void timeOut() {
            timedOut.incrementAndGet();
        }

        private void logInformation() {
            // When this method is called the instance has finished starting so all waiting threads are going to be
            // almost immediately released. We'll count them as such.
            long allHeldThreads = allThreads.get();
            long timedOutThreads = timedOut.get();
            long releasedThreads = allHeldThreads - timedOutThreads;
            
            MDC.put(NUMBER_HELD_REQUESTS, allHeldThreads);
            MDC.put(NUMBER_RELEASED_REQUESTS, releasedThreads);
            MDC.put(NUMBER_TIMEDOUT_REQUESTS,  timedOutThreads);
            log.info("{} threads have been held. {} have been successfully released and {} have timed out",
                    allHeldThreads, releasedThreads, timedOutThreads);
            MDC.remove(NUMBER_HELD_REQUESTS);
            MDC.remove(NUMBER_RELEASED_REQUESTS);
            MDC.remove(NUMBER_TIMEDOUT_REQUESTS);
        }
    }
}
