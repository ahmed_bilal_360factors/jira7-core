package com.atlassian.jira.crowd.embedded;

import com.atlassian.crowd.directory.PasswordConstraintsLoaderImpl;
import com.atlassian.crowd.embedded.api.Attributes;
import com.atlassian.crowd.embedded.api.PasswordConstraint;
import com.atlassian.crowd.embedded.api.PasswordScoreService;
import com.atlassian.jira.config.FeatureManager;

import java.util.Collections;
import java.util.Set;

/**
 * Because crowd shouldn't be telling us where SAL's DarkFeatureManager should live.
 */
public class JiraPasswordConstraintsLoader extends PasswordConstraintsLoaderImpl {
    public static final String PASSWORD_POLICY_FEATURE = "um.password.policy";
    private final FeatureManager featureManager;

    public JiraPasswordConstraintsLoader(PasswordScoreService passwordScoreService, FeatureManager featureManager) {
        super(passwordScoreService);
        this.featureManager = featureManager;
    }

    @Override
    public Set<PasswordConstraint> getFromDirectoryAttributes(long directoryId, Attributes attributes) {
        //this dark feature is probably not used but I have currently no way to verify this
        if (featureManager.getDarkFeatures().getGlobalEnabledFeatureKeys().contains(PASSWORD_POLICY_FEATURE)){
            return super.getFromDirectoryAttributes(directoryId, attributes);
        } else {
            return Collections.emptySet();
        }
    }
}