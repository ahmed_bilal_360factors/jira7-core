package com.atlassian.jira.application.install;

import com.atlassian.plugin.JarPluginArtifact;
import com.atlassian.plugin.PluginInformation;
import com.atlassian.plugin.osgi.util.OsgiHeaderUtil;
import org.osgi.framework.Constants;
import org.osgi.framework.Version;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.io.File;
import java.util.Optional;
import java.util.jar.Manifest;

/**
 * @since v6.5
 */
@Nonnull
public class BundlesVersionDiscovery {
    public static final Logger LOGGER = LoggerFactory.getLogger(BundlesVersionDiscovery.class);

    public static class PluginIdentification {
        private final String symbolicName;
        private final Version version;

        public PluginIdentification(final String symbolicName, final String version) {
            this.symbolicName = symbolicName;
            this.version = new Version(version);
        }

        public String getSymbolicName() {
            return symbolicName;
        }

        public Version getVersion() {
            return version;
        }

        @Override
        public boolean equals(final Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }

            final PluginIdentification that = (PluginIdentification) o;

            return symbolicName.equals(that.symbolicName) && version.equals(that.version);

        }

        @Override
        public int hashCode() {
            int result = symbolicName.hashCode();
            result = 31 * result + version.hashCode();
            return result;
        }
    }

    public Optional<PluginIdentification> getBundleNameAndVersion(final File jarFile) {
        try {
            final JarPluginArtifact pluginArtifact = new JarPluginArtifact(jarFile);
            final Manifest manifest = OsgiHeaderUtil.getManifest(pluginArtifact);

            return Optional.ofNullable(getBundleNameAndVersion(manifest));
        } catch (Exception x) {
            LOGGER.warn("Can't read manifest of file: " + jarFile.getAbsolutePath(), x);
            return Optional.empty();
        }
    }

    private PluginIdentification getBundleNameAndVersion(final Manifest manifest) {
        final PluginInformation pluginInformation = OsgiHeaderUtil.extractOsgiPluginInformation(manifest, true);

        final String pluginKey = OsgiHeaderUtil.getValidatedAttribute(manifest, Constants.BUNDLE_SYMBOLICNAME);
        final String version = pluginInformation.getVersion();

        return new PluginIdentification(pluginKey, version);
    }
}
