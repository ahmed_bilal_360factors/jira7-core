package com.atlassian.jira.database;

import com.atlassian.jira.config.database.DatabaseConfig;
import com.atlassian.jira.config.database.DatabaseType;
import com.atlassian.jira.config.database.jdbcurlparser.JdbcUrlParser;
import com.atlassian.jira.exception.ParseException;
import com.google.common.annotations.VisibleForTesting;

import javax.annotation.Nullable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DatabaseCollationReader {

    private static final String ORACLE_GET_COLLATION_SQL = "SELECT VALUE from NLS_DATABASE_PARAMETERS WHERE PARAMETER='NLS_SORT'";
    private static final String MYSQL_GET_COLLATION_SQL = "SELECT DEFAULT_COLLATION_NAME from information_schema.SCHEMATA S where SCHEMA_NAME = ?";
    private static final String POSTGRES_GET_COLLATION_SQL = "SELECT datcollate FROM pg_database WHERE datname = ?";
    private static final String SQL_SERVER_GET_COLLATION_SQL = "SELECT collation_name FROM sys.databases WHERE name = DB_NAME()";

    private DatabaseCollationReader() {
    }

    @Nullable
    public static String findCollation(final Connection connection, final DatabaseConfig databaseConfig) throws SQLException, ParseException {
        try (PreparedStatement ps = generateCollationQuery(databaseConfig, connection)) {
            if (ps != null) {
                ResultSet rs = ps.executeQuery();
                rs.next();
                return rs.getString(1);
            } else {
                return null;
            }
        }
    }

    private static PreparedStatement generateCollationQuery(final DatabaseConfig databaseConfig, final Connection connection)
            throws SQLException, ParseException {
        PreparedStatement ps = null;

        if (databaseConfig.isOracle()) {
            ps = connection.prepareStatement(ORACLE_GET_COLLATION_SQL);
        } else if (databaseConfig.isMySql()) {
            ps = connection.prepareStatement(MYSQL_GET_COLLATION_SQL);
            ps.setString(1, getDatabaseName(databaseConfig, connection));
        } else if (databaseConfig.isPostgres()) {
            ps = connection.prepareStatement(POSTGRES_GET_COLLATION_SQL);
            ps.setString(1, getDatabaseName(databaseConfig, connection));
        } else if (databaseConfig.isSqlServer()) {
            ps = connection.prepareStatement(SQL_SERVER_GET_COLLATION_SQL);
        }

        return ps;
    }

    /**
     * Returns the database name from the JDBC url
     */
    private static String getDatabaseName(final DatabaseConfig databaseConfig, final Connection connection)
            throws ParseException, SQLException {
        JdbcUrlParser urlParser = DatabaseType.forDatabaseTypeName(databaseConfig.getDatabaseType()).getJdbcUrlParser();
        String url = connection.getMetaData().getURL();

        return urlParser.parseUrl(url).getInstance();
    }

    @VisibleForTesting
    static String[] getCollationQueries() {
        return new String[]{
                ORACLE_GET_COLLATION_SQL,
                MYSQL_GET_COLLATION_SQL,
                POSTGRES_GET_COLLATION_SQL,
                SQL_SERVER_GET_COLLATION_SQL
        };
    }
}
