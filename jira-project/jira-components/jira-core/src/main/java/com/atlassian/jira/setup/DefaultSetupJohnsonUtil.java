package com.atlassian.jira.setup;

import com.atlassian.jira.web.ServletContextProvider;
import com.atlassian.johnson.Johnson;
import com.atlassian.johnson.JohnsonEventContainer;

import java.util.Collection;


public class DefaultSetupJohnsonUtil implements SetupJohnsonUtil {
    public Collection getEvents() {
        final JohnsonEventContainer johnsonContainer = Johnson.getEventContainer(ServletContextProvider.getServletContext());

        return johnsonContainer.getEvents();
    }
}
