package com.atlassian.jira.issue.fields.config.persistence;

import com.atlassian.cache.Cache;
import com.atlassian.cache.CacheManager;
import com.atlassian.cache.CacheSettingsBuilder;
import com.atlassian.event.api.EventListener;
import com.atlassian.jira.EventComponent;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.database.QueryDslAccessor;
import com.atlassian.jira.event.ClearCacheEvent;
import com.atlassian.jira.issue.context.persistence.FieldConfigContextPersister;
import com.atlassian.jira.issue.fields.ConfigurableField;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.issue.fields.config.FieldConfigScheme;
import com.atlassian.jira.issue.fields.config.manager.FieldConfigManager;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.google.common.collect.ImmutableList;
import org.ofbiz.core.entity.GenericEntityException;
import org.ofbiz.core.entity.GenericValue;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

/**
 * A simple caching wrapper
 * <p>
 * NOTE : you may be wondering about the cache invalidation strategy on this cache.  Will the top level classes
 * that use this cache such as {@link com.atlassian.jira.issue.CustomFieldManager@refreshCache} call {@link #init()}
 * and this clears the cache.
 * <p>
 * TODO: This probably should be rewritten so that the upper lays of code are not responsible for clearing the lower level caches
 * and also the "cache inheritance" pattern should be removed.
 */
@EventComponent
public class CachedFieldConfigSchemePersister extends FieldConfigSchemePersisterImpl {

    private final Cache<Long, Optional<FieldConfigScheme>> cacheById;
    private final Cache<String, List<FieldConfigScheme>> cacheByCustomField;
    private final Cache<Long, FieldConfigScheme> cacheByFieldConfig;
    private final FieldConfigManager fieldConfigManager;

    public CachedFieldConfigSchemePersister(final OfBizDelegator delegator,
                                            final QueryDslAccessor queryDslAccessor,
                                            final ConstantsManager constantsManager,
                                            final FieldConfigPersister fieldConfigPersister,
                                            final FieldConfigContextPersister fieldContextPersister,
                                            final FieldConfigManager fieldConfigManager,
                                            final CacheManager cacheManager) {
        super(delegator, queryDslAccessor, constantsManager, fieldConfigPersister, fieldContextPersister, cacheManager);

        this.fieldConfigManager = fieldConfigManager;

        this.cacheById = cacheManager.getCache(getClass().getName() + ".cacheById",
                id -> Optional.ofNullable(super.getFieldConfigScheme(id)),
                new CacheSettingsBuilder().expireAfterAccess(30, TimeUnit.MINUTES).build());

        this.cacheByCustomField = cacheManager.getCache(getClass().getName() + ".cacheByCustomField",
                this::loadSchemesByCustomFieldId,
                new CacheSettingsBuilder().expireAfterAccess(30, TimeUnit.MINUTES).build());

        this.cacheByFieldConfig = cacheManager.getCache(getClass().getName() + ".cacheByFieldConfig",
                this::loadSchemeByFieldConfigId,
                new CacheSettingsBuilder().expireAfterAccess(30, TimeUnit.MINUTES).build());
    }

    @Override
    public void init() {
        super.init();
        clearCaches();
    }


    @SuppressWarnings({"UnusedDeclaration"})
    @EventListener
    public void onClearCache(final ClearCacheEvent event) {
        super.invalidateAll();
        clearCaches();
    }

    @Nullable
    @Override
    public FieldConfigScheme getFieldConfigScheme(final Long configSchemeId) {
        if (configSchemeId == null) {
            return null;
        }
        return cacheById.get(configSchemeId).orElse(null);
    }

    @Nullable
    @Override
    public List<FieldConfigScheme> getConfigSchemesForCustomField(final ConfigurableField field) {
        if (field == null) {
            return null;
        }
        return cacheByCustomField.get(field.getId());
    }

    @Nullable
    @Override
    public FieldConfigScheme getConfigSchemeForFieldConfig(final FieldConfig fieldConfig) {
        if (fieldConfig == null) {
            return null;
        }
        return cacheByFieldConfig.get(fieldConfig.getId());
    }

    @Override
    public FieldConfigScheme create(final FieldConfigScheme configScheme, final ConfigurableField field) {
        FieldConfigScheme createdConfigScheme = null;
        try {
            createdConfigScheme = super.create(configScheme, field);
        } finally {
            if (createdConfigScheme != null) {
                cacheById.remove(createdConfigScheme.getId());
                cacheByCustomField.remove(field.getId());
                removeFieldConfigsFromCache(createdConfigScheme);
            }
        }
        return createdConfigScheme;
    }

    @Override
    public FieldConfigScheme update(final FieldConfigScheme configScheme) {
        FieldConfigScheme updatedConfigScheme = null;
        try {
            updatedConfigScheme = super.update(configScheme);
        } finally {
            if (updatedConfigScheme != null) {
                cacheById.remove(updatedConfigScheme.getId());
                final ConfigurableField field = updatedConfigScheme.getField();
                if (field != null) {
                    cacheByCustomField.remove(field.getId());
                }
                // Note: Old configs will get invalidated by removeRelatedConfigsForUpdate.  This round may not
                // even be necessary because the update deletes and recreates these, pretty much guaranteeing
                // that the new configs have new IDs.  Deliberately being paranoid for now.
                removeFieldConfigsFromCache(updatedConfigScheme);
            }
        }
        return updatedConfigScheme;
    }

    @Override
    protected void removeRelatedConfigsForUpdate(@Nonnull final FieldConfigScheme configScheme, @Nonnull final GenericValue gv)
            throws GenericEntityException {
        try {
            super.removeRelatedConfigsForUpdate(configScheme, gv);
        } finally {
            removeFieldConfigsFromCache(configScheme);
        }
    }

    @Override
    public void remove(final Long fieldConfigSchemeId) {
        FieldConfigScheme deletedFieldConfigScheme = null;
        try {
            deletedFieldConfigScheme = super.removeIfExist(fieldConfigSchemeId);
        } finally {
            cacheById.remove(fieldConfigSchemeId);
            if (deletedFieldConfigScheme != null) {
                final ConfigurableField field = deletedFieldConfigScheme.getField();
                if (field != null) {
                    cacheByCustomField.remove(field.getId());
                }
                removeFieldConfigsFromCache(deletedFieldConfigScheme);
            }
        }
    }

    @Override
    public void removeByIssueType(final IssueType issueType) {
        try {
            super.removeByIssueType(issueType);
        } finally {
            clearCaches();
        }
    }

    private void removeFieldConfigsFromCache(final FieldConfigScheme fieldConfigScheme) {
        fieldConfigScheme.getConfigs()
                .values()
                .stream()
                .map(FieldConfig::getId)
                .forEach(cacheByFieldConfig::remove);
    }

    private void clearCaches() {
        cacheById.removeAll();
        cacheByCustomField.removeAll();
        cacheByFieldConfig.removeAll();
    }

    @Nonnull
    private FieldConfigScheme loadSchemeByFieldConfigId(final Long fieldConfigId) {
        final FieldConfig fieldConfig = fieldConfigManager.getFieldConfig(fieldConfigId);
        return super.getConfigSchemeForFieldConfig(fieldConfig);
    }

    @Nonnull
    private List<FieldConfigScheme> loadSchemesByCustomFieldId(final String fieldId) {
        final ConfigurableField field = (ConfigurableField) ComponentAccessor.getFieldManager().getField(fieldId);
        return ImmutableList.copyOf(super.getConfigSchemesForCustomField(field));
    }
}
