package com.atlassian.jira.config.feature;

import com.atlassian.fugue.Either;
import com.atlassian.fugue.Option;
import com.atlassian.fugue.Pair;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.config.properties.JiraProperties;
import com.atlassian.jira.io.ResourceLoader;
import com.atlassian.jira.util.RuntimeIOException;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.Resources;
import com.atlassian.plugin.elements.ResourceDescriptor;
import com.google.common.base.Function;
import com.google.common.base.Supplier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Stream;

import static com.atlassian.sal.api.features.DarkFeatureManager.DARKFEATURES_PROPERTIES_FILE_PROPERTY;
import static com.atlassian.sal.api.features.DarkFeatureManager.DARKFEATURES_PROPERTIES_FILE_PROPERTY_DEFAULT;
import static java.util.Collections.emptyMap;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;

/**
 * Default loader of features used in core JIRA.
 * Properties are loaded both from default JIRA locations and plugins.
 *
 * @since v6.5
 */
public class CoreFeaturesLoader implements FeaturesLoader {
    public static final String FEATURE_RESOURCE_TYPE = "feature";
    public static final String CORE_FEATURES_RESOURCE = "/jira-features.properties";
    private static final Resources.TypeFilter FEATURE_TYPE_FILTER = new Resources.TypeFilter(FEATURE_RESOURCE_TYPE);
    /**
     * Logger for this DefaultFeatureManager instance.
     */
    private static final Logger log = LoggerFactory.getLogger(CoreFeaturesLoader.class);


    private final JiraProperties jiraSystemProperties;
    private final ResourceLoader resourceLoader;
    private final PluginAccessor pluginAccessor;

    public CoreFeaturesLoader(final JiraProperties jiraSystemProperties, final PluginAccessor pluginAccessor, final ResourceLoader resourceLoader) {
        this.jiraSystemProperties = jiraSystemProperties;
        this.pluginAccessor = pluginAccessor;
        this.resourceLoader = resourceLoader;
    }

    @Override
    public Properties loadCoreProperties() {

        final Properties properties = new Properties();
        // properties should be overridden in right order most important are system, then JIRA then sal
        // so we add them in revers order
        properties.putAll(getSalDarkFeatures());
        properties.putAll(getCoreFeatures());
        properties.putAll(getSystemDarkFeatures());
        return properties;
    }

    @Override
    public Iterable<Properties> loadPluginsFeatureProperties() {
        final Stream<Pair<ResourceDescriptor, Plugin>> allResources = pluginAccessor.getEnabledPlugins().stream()
                .flatMap(this::getFeatureResources);

        return allResources
                .map(this::getPropertiesFromPlugin)
                .collect(toList());
    }


    @Override
    public boolean hasFeatureResources(final Plugin plugin) {
        return getFeatureResources(plugin).findAny().isPresent();
    }

    private Properties getPropertiesFromPlugin(final Pair<ResourceDescriptor, Plugin> pluginFeatureDesc) {
        return loadRequiredProperties(
                path -> resourceLoader.getResourceAsStream(pluginFeatureDesc.right(), path),
                pluginFeatureDesc.left().getLocation());
    }

    private Properties loadRequiredProperties(final Function<String, Option<InputStream>> resourceLoader, final String path) {
        final Function<? super IOException, Properties> rethrowException = exception -> {
            throw new RuntimeIOException("Unable to load properties from " + path, exception);
        };
        final Supplier<IllegalStateException> missingFileExceptionSupplier = () -> {
            throw new IllegalStateException(String.format("Resource %s not found", path));
        };
        final Function<Option<Properties>, Properties> unFoldProperties =
                (Option<Properties> propertiesOption) -> propertiesOption.getOrThrow(missingFileExceptionSupplier);

        return tryLoadProperties(() -> resourceLoader.apply(path)).fold(
                rethrowException,
                unFoldProperties);
    }

    private Either<IOException, Option<Properties>> tryLoadProperties(final Supplier<Option<InputStream>> resourceSupplier) {
        final Option<InputStream> streamOption = resourceSupplier.get();
        final Supplier<Either<IOException, Option<Properties>>> emptyPropertiesSupplier =
                () -> Either.<IOException, Option<Properties>>right(Option.none());

        return streamOption.map(this::tryLoadProperties).
                getOrElse(emptyPropertiesSupplier);
    }

    private Either<IOException, Option<Properties>> tryLoadProperties(final InputStream propStream) {
        final Properties props = new Properties();
        try {
            props.load(propStream);
        } catch (final IOException e) {
            return Either.left(e);
        }
        return Either.<IOException, Option<Properties>>right(Option.some(props));
    }

    private Properties getCoreFeatures() {
        return loadRequiredProperties(resourceLoader::getResourceAsStream, CORE_FEATURES_RESOURCE);
    }

    private Map<Object, Object> getSalDarkFeatures() {
        final String salDarkFeaturesProperty = jiraSystemProperties.getProperty(
                DARKFEATURES_PROPERTIES_FILE_PROPERTY, DARKFEATURES_PROPERTIES_FILE_PROPERTY_DEFAULT);
        final Supplier<Properties> emptyProperties = Properties::new;

        return tryLoadProperties(() -> resourceLoader.getResourceAsStream(salDarkFeaturesProperty))
                .fold(ex -> emptyMap(),
                        (Option<Properties> propertiesOption) -> propertiesOption.getOrElse(emptyProperties));
    }

    private Map<Object, Object> getSystemDarkFeatures() {
        // add system properties
        final Stream<String> systemFeatureNames = jiraSystemProperties.getProperties().stringPropertyNames().stream()
                .filter(key -> key.startsWith(FeatureManager.SYSTEM_PROPERTY_PREFIX));

        final Map<Object, Object> systemProperties = systemFeatureNames
                .collect(toMap(
                        key -> key.substring(FeatureManager.SYSTEM_PROPERTY_PREFIX.length(), key.length()),
                        jiraSystemProperties::getProperty));
        //log overrides from sys props
        if (log.isDebugEnabled()) {
            systemProperties.entrySet().stream().forEach(entry ->
                            log.trace("Feature '{}' is set to '{}' by system properties", entry.getKey(), entry.getValue())
            );
        }
        return systemProperties;
    }

    private Stream<Pair<ResourceDescriptor, Plugin>> getFeatureResources(final Plugin plugin) {
        return plugin.getResourceDescriptors()
                .stream().filter(FEATURE_TYPE_FILTER::apply)
                .map(resource -> new Pair<>(resource, plugin));
    }
}
