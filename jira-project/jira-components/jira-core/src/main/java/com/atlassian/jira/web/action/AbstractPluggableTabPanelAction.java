package com.atlassian.jira.web.action;

import com.atlassian.core.AtlassianCoreException;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.exception.PermissionException;
import com.atlassian.jira.plugin.PluggableTabPanelModuleDescriptor;
import com.atlassian.jira.plugin.util.ModuleDescriptorComparator;
import com.atlassian.jira.user.preferences.UserPreferencesManager;
import com.atlassian.plugin.PluginAccessor;

import java.util.List;
import java.util.StringTokenizer;

import static java.util.stream.Collectors.toList;

/**
 * Manages the setting, retrieval, checking and activating of a "selected tab"
 * for any page that wishes to provide navigation to "tab panels".
 *
 * @since v6.1
 */
public abstract class AbstractPluggableTabPanelAction<TabPanelClass
        extends PluggableTabPanelModuleDescriptor> extends IssueActionSupport {

    protected final PluginAccessor pluginAccessor;
    private final UserPreferencesManager userPreferencesManager;

    private List<TabPanelClass> tabPanels;
    private String persistenceKey;

    /**
     * @deprecated Use {@link AbstractPluggableTabPanelAction#AbstractPluggableTabPanelAction(PluginAccessor, UserPreferencesManager)}},
     *             which enables the user preferences manager to be injected.
     */
    @Deprecated
    public AbstractPluggableTabPanelAction(PluginAccessor pluginAccessor) {
        this(pluginAccessor, ComponentAccessor.getUserPreferencesManager());
    }

    public AbstractPluggableTabPanelAction(PluginAccessor pluginAccessor, UserPreferencesManager userPreferencesManager) {
        super();
        this.pluginAccessor = pluginAccessor;
        this.userPreferencesManager = userPreferencesManager;
        this.persistenceKey = AbstractPluggableTabPanelAction.class.toString(); // For lack of a sensible default.
    }

    /**
     * @return A list of {@link TabPanelClass} objects, never null
     */
    public List<TabPanelClass> getTabPanels() {
        if (tabPanels == null) {
            tabPanels = initTabPanels();
        }
        return tabPanels;
    }

    /**
     * Retrieves and initialises the tab panels via the plugin accessor
     *
     * @return list of {@link TabPanelClass} objects, never null
     * @since v3.10
     */
    protected List<TabPanelClass> initTabPanels() {
        final List<TabPanelClass> tabPanels = getTabPanelModuleDescriptors();
        return tabPanels
                .stream()
                .filter(this::isVisible)
                .sorted(ModuleDescriptorComparator.COMPARATOR)
                .collect(toList());
    }

    /**
     * Retrieves the tab panels for this page via the plugin accessor.
     * <p>
     * Used by initialisation method to collect the list of tab panels for this page
     */
    protected abstract List<TabPanelClass> getTabPanelModuleDescriptors();

    /**
     * Returns true if the tab panel of the given descriptor should be hidden from the current view
     *
     * @param descriptor module descriptor
     * @return true if hidden, false otherwise
     * @throws PermissionException if project is invalid or not visible to the current user
     * @since v3.10
     */
    protected abstract boolean isTabPanelHidden(final TabPanelClass descriptor) throws PermissionException;

    protected boolean canSeeTab(final String tabKey) {
        if (tabKey == null) {
            return false;
        }
        final StringTokenizer tokenizer = new StringTokenizer(tabKey, ":");
        if (tokenizer.countTokens() == 2) {
            // Get the key of the currently selected tab
            tokenizer.nextToken();
            final String tabName = tokenizer.nextToken();
            return getTabPanels()
                    .stream()
                    .anyMatch(tabPanel -> tabPanel.getKey().equals(tabName));
        }
        return false;
    }

    public TabPanelClass getSelectedTabPanel() {
        return (TabPanelClass) pluginAccessor.getEnabledPluginModule(getSelectedTab());
    }

    @SuppressWarnings({"unused"}) // Used in JSPs to mark the 'active' state of the navigation.
    public String getSelected() {
        return getSelectedTab();
    }

    /**
     * Retrieve the name of the tab panel that is selected.
     * <p>
     * Protected because it's used in some error log messages.
     *
     * @return The complete module key of the selected tab.
     * If no tab is currently selected, it will return the first available tab on the page.
     * If there are no tabs to select, returns null.
     */
    protected String getSelectedTab() {
        final String currentKey = userPreferencesManager.getExtendedPreferences(getLoggedInUser()).getText(persistenceKey);

        if (canSeeTab(currentKey)) {
            return currentKey;
        }

        final List<TabPanelClass> projectTabPanels = getTabPanels();
        if (!projectTabPanels.isEmpty()) {
            final String key = (projectTabPanels.get(0)).getCompleteKey();
            setSelectedTab(key);
            return key;
        }

        return null;
    }

    /**
     * Set the name of the selected tab.
     * <p>
     * Used by {@link com.atlassian.jira.webwork.JiraSafeActionParameterSetter#setActionProperty(java.lang.reflect.Method,
     * webwork.action.Action, String[])}.
     *
     * @param selectedTab a complete module descriptor key for the {@link TabPanelClass} to set as the selected tab.
     */
    public void setSelectedTab(final String selectedTab) {
        try {
            if (getLoggedInUser() != null) {
                userPreferencesManager.getExtendedPreferences(getLoggedInUser()).setText(persistenceKey, selectedTab);
            }
        } catch (AtlassianCoreException e) {
            log.error("Exception occurred while trying to set the selected tab:", e);
        }
    }

    private boolean isVisible(TabPanelClass tabPanelClass) {
        try {
            return tabPanelClass != null && !isTabPanelHidden(tabPanelClass);
        } catch (PermissionException e) {
            return false;
        }
    }
}