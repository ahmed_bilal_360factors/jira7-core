package com.atlassian.jira.model.querydsl;

import com.atlassian.jira.ofbiz.FieldMap;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import org.ofbiz.core.entity.GenericValue;

import javax.annotation.Generated;

/**
 * Data Transfer Object for the OSPropertyEntry entity.
 * <p>
 * Generated by the JIRA Querydsl code generation tool - https://bitbucket.org/atlassian/jira-querydsl-codegen/
 * </p>
 *
 * @see QOSPropertyEntry
 */
@Generated("com.atlassian.jira.tool.querydsl.CodeGenerator")
public class OSPropertyEntryDTO implements DTO {
    private final Long id;
    private final String entityName;
    private final Long entityId;
    private final String propertyKey;
    private final Integer type;

    public Long getId() {
        return id;
    }

    public String getEntityName() {
        return entityName;
    }

    public Long getEntityId() {
        return entityId;
    }

    public String getPropertyKey() {
        return propertyKey;
    }

    public Integer getType() {
        return type;
    }

    public OSPropertyEntryDTO(Long id, String entityName, Long entityId, String propertyKey, Integer type) {
        this.id = id;
        this.entityName = entityName;
        this.entityId = entityId;
        this.propertyKey = propertyKey;
        this.type = type;
    }

    /**
     * Creates a GenericValue object from the values in this Data Transfer Object.
     * <p>
     * This can be useful when QueryDsl code needs to interact with legacy OfBiz code.
     * </p>
     *
     * @param ofBizDelegator OfBizDelegator will have makeValue() called on it.
     * @return a GenericValue object constructed from the values in this Data Transfer Object.
     */
    public GenericValue toGenericValue(final OfBizDelegator ofBizDelegator) {
        return ofBizDelegator.makeValue("OSPropertyEntry", new FieldMap()
                .add("id", id)
                .add("entityName", entityName)
                .add("entityId", entityId)
                .add("propertyKey", propertyKey)
                .add("type", type)
        );
    }

    /**
     * Constructs a new instance of this Data Transfer object from the values in the given GenericValue.
     * <p>
     * This can be useful when QueryDsl code needs to interact with legacy OfBiz code.
     * </p>
     *
     * @param gv the GenericValue
     * @return a new instance of this Data Transfer object with the values in the given GenericValue.
     */
    public static OSPropertyEntryDTO fromGenericValue(GenericValue gv) {
        return new OSPropertyEntryDTO(
                gv.getLong("id"),
                gv.getString("entityName"),
                gv.getLong("entityId"),
                gv.getString("propertyKey"),
                gv.getInteger("type")
        );
    }

    public static Builder builder() {
        return new Builder();
    }

    public static Builder builder(OSPropertyEntryDTO oSPropertyEntryDTO) {
        return new Builder(oSPropertyEntryDTO);
    }

    public static class Builder {
        private Long id;
        private String entityName;
        private Long entityId;
        private String propertyKey;
        private Integer type;

        public Builder() {
        }

        public Builder(OSPropertyEntryDTO oSPropertyEntryDTO) {
            this.id = oSPropertyEntryDTO.id;
            this.entityName = oSPropertyEntryDTO.entityName;
            this.entityId = oSPropertyEntryDTO.entityId;
            this.propertyKey = oSPropertyEntryDTO.propertyKey;
            this.type = oSPropertyEntryDTO.type;
        }

        public OSPropertyEntryDTO build() {
            return new OSPropertyEntryDTO(id, entityName, entityId, propertyKey, type);
        }

        public Builder id(Long id) {
            this.id = id;
            return this;
        }
        public Builder entityName(String entityName) {
            this.entityName = entityName;
            return this;
        }
        public Builder entityId(Long entityId) {
            this.entityId = entityId;
            return this;
        }
        public Builder propertyKey(String propertyKey) {
            this.propertyKey = propertyKey;
            return this;
        }
        public Builder type(Integer type) {
            this.type = type;
            return this;
        }
    }
}