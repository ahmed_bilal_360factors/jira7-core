package com.atlassian.jira.issue.search;

import com.atlassian.collectors.CollectorsUtil;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.issue.context.IssueContext;
import com.atlassian.jira.issue.context.IssueContextImpl;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.util.ObjectUtils;
import org.apache.commons.collections.ListUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.collections.functors.AndPredicate;
import org.apache.commons.collections.functors.InstanceofPredicate;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

public class SearchContextImpl implements SearchContext {
    private static final List<Long> ALL_PROJECTS = Collections.singletonList(null);
    private static final List<String> ALL_ISSUE_TYPES = Collections.singletonList(null);

    private static final Logger log = LoggerFactory.getLogger(SearchContextImpl.class);

    private static final Predicate LONG_PREDICATE = new AndPredicate(InstanceofPredicate.getInstance(Long.class), ObjectUtils.getIsSetPredicate());
    private static final Predicate STRING_PREDICATE = new AndPredicate(InstanceofPredicate.getInstance(String.class), ObjectUtils.getIsSetPredicate());

    @Nonnull
    private final List<Long> projectCategoryIds;

    @Nonnull
    private final List<Long> projectIds;

    @Nonnull
    private final List<String> issueTypeIds;

    private List<Project> projects;
    private List<IssueType> issueTypes;

    private final ConstantsManager constantsManager;
    private final ProjectManager projectManager;

    public SearchContextImpl() {
        this(null, null, null);
    }

    public SearchContextImpl(List<Long> projectCategoryIds, List<Long> projectIds, List<String> issueTypeIds) {
        this.constantsManager = ComponentAccessor.getConstantsManager();
        this.projectManager = ComponentAccessor.getProjectManager();
        this.projectCategoryIds = prepareProjectList(projectCategoryIds);
        this.projectIds = prepareProjectList(projectIds);
        this.issueTypeIds = ListUtils.predicatedList(constantsManager.expandIssueTypeIds(issueTypeIds), STRING_PREDICATE);
    }

    public SearchContextImpl(SearchContext searchContext) {
        this(searchContext.getProjectCategoryIds(), searchContext.getProjectIds(), searchContext.getIssueTypeIds());
    }

    public boolean isForAnyProjects() {
        return projectCategoryIds.isEmpty() && projectIds.isEmpty();
    }

    public boolean isForAnyIssueTypes() {
        return issueTypeIds.isEmpty();
    }

    public boolean isSingleProjectContext() {
        return projectIds.size() == 1;
    }

    @Override
    public Project getSingleProject() {
        if (isSingleProjectContext()) {
            return projectManager.getProjectObj(projectIds.get(0));
        } else {
            throw new IllegalStateException("This is not a single project context");
        }
    }

    @Nonnull
    public List<Long> getProjectCategoryIds() {
        return projectCategoryIds;
    }

    @Nonnull
    public List<Long> getProjectIds() {
        return projectIds;
    }

    @Nonnull
    public List<String> getIssueTypeIds() {
        return issueTypeIds;
    }

    public List<IssueContext> getAsIssueContexts() {
        List<Long> projectIds = this.projectIds.isEmpty() ? ALL_PROJECTS : this.projectIds;
        List<String> issueTypeIds = this.issueTypeIds.isEmpty() ? ALL_ISSUE_TYPES : this.issueTypeIds;
        List<IssueContext> issueContexts = new ArrayList<>(projectIds.size() * issueTypeIds.size());
        projectIds.forEach(projectId -> issueTypeIds.stream()
                .map(issueTypeId -> new IssueContextImpl(projectId, issueTypeId))
                .forEach(issueContexts::add));
        return issueContexts;
    }

    public void verify() {
        for (Iterator<Long> iterator = projectIds.iterator(); iterator.hasNext(); ) {
            Long projectId = iterator.next();
            if (projectManager.getProjectObj(projectId) == null) {
                log.warn("Project id {} found in searchContext but is not valid. Being removed.", projectId);
                iterator.remove();
            }
        }

        for (Iterator<String> iterator = issueTypeIds.iterator(); iterator.hasNext(); ) {
            String issueTypeId = iterator.next();
            if (constantsManager.getIssueType(issueTypeId) == null) {
                log.warn("Issue type id {} found in searchContext but is not valid. Being removed.", issueTypeId);
                iterator.remove();
            }
        }
    }

    @Nonnull
    private static List<Long> prepareProjectList(List<Long> list) {
        if (list == null) {
            return Collections.emptyList();
        } else if (list.size() == 1 && !ObjectUtils.isValueSelected(list.get(0))) {
            return Collections.emptyList();
        } else {
            return ListUtils.predicatedList(list, LONG_PREDICATE);
        }
    }

    public String toString() {
        return new ToStringBuilder(this)
                .append("projectCategoryIds", projectCategoryIds)
                .append("projectIds", projectIds)
                .append("issueTypeIds", issueTypeIds)
                .toString();
    }

    public boolean equals(@Nullable Object o) {
        return o == this || (o instanceof SearchContextImpl && equals((SearchContextImpl) o));
    }

    private boolean equals(@Nonnull SearchContextImpl rhs) {
        return Objects.equals(projectCategoryIds, rhs.projectCategoryIds)
                && Objects.equals(projectIds, rhs.projectIds)
                && Objects.equals(issueTypeIds, rhs.issueTypeIds);
    }

    public int hashCode() {
        return Objects.hash(projectCategoryIds, projectIds, issueTypeIds);
    }

    @Nonnull
    @Override
    public List<Project> getProjects() {
        if (projects == null) {
            projects = projectIds.stream()
                    .map(projectManager::getProjectObj)
                    .filter(Objects::nonNull)
                    .collect(CollectorsUtil.toImmutableListWithSizeOf(projectIds));
        }
        return projects;
    }

    @Nonnull
    @Override
    public List<IssueType> getIssueTypes() {
        if (issueTypes == null) {
            issueTypes = issueTypeIds.stream()
                    .map(constantsManager::getIssueType)
                    .filter(Objects::nonNull)
                    .collect(CollectorsUtil.toImmutableListWithSizeOf(issueTypeIds));
        }
        return issueTypes;
    }
}
