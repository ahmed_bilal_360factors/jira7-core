package com.atlassian.jira.plugin;

import com.atlassian.application.api.ApplicationPlugin;
import com.atlassian.application.host.plugin.ApplicationMetaDataModuleDescriptor;
import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.manager.ApplicationDefinedPluginsProvider;

import java.util.Set;

import static com.atlassian.collectors.CollectorsUtil.toImmutableSet;
import static java.util.stream.StreamSupport.stream;

/**
 * Allows to return the list of plugin keys that are defined in the application manager descriptors
 * This is used to not disable such plugins during the "safe startup" phase
 * @since 7.3
 */
public class JiraApplicationDefinedPluginProvider implements ApplicationDefinedPluginsProvider {

    @Override
    public Set<String> getPluginKeys(Iterable<ModuleDescriptor> descriptors) {
        return stream(descriptors.spliterator(), false)
                .filter(ApplicationMetaDataModuleDescriptor.class::isInstance)
                .map(ApplicationMetaDataModuleDescriptor.class::cast)
                .flatMap(descriptor -> stream(descriptor.getModule().getPlugins().spliterator(), false))
                .map(ApplicationPlugin::getPluginKey)
                .collect(toImmutableSet());
    }
}
