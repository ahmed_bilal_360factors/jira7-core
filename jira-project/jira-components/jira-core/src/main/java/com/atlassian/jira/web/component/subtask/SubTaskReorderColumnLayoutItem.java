package com.atlassian.jira.web.component.subtask;

import com.atlassian.jira.bean.SubTaskBean;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.template.VelocityTemplatingEngine;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.I18nHelper;
import com.google.common.collect.ImmutableMap;

import java.util.Map;

/**
 * This class displays a column which displays arrows for changing the sequence of sub-tasks on the view issue page.
 * <p>
 * Note that a new instance should be created for every view issue page, as it uses sequence numbers which need to be
 * reset after each use.
 */
public class SubTaskReorderColumnLayoutItem extends AbstractSubTaskColumnLayoutItem {
    private final static String REORDER_COLUMN_TEMPLATE_PATH = "templates/jira/issue/subtask/issue-subtask-reorder.vm";

    private int displaySequence = 0;

    private final boolean allowedReorderSubTasks;

    private final SubTaskBean subTaskBean;
    private final String subTaskView;
    private final String contextPath;
    private final Issue parentIssue;
    private final I18nHelper i18n;

    public SubTaskReorderColumnLayoutItem(PermissionManager permissionManager, ApplicationUser user,
                                          VelocityTemplatingEngine templatingEngine, SubTaskBean subTaskBean,
                                          String subTaskView, String contextPath, Issue parentIssue, I18nHelper i18n) {
        super(templatingEngine, subTaskBean, subTaskView);
        this.allowedReorderSubTasks = permissionManager.hasPermission(Permissions.EDIT_ISSUE, parentIssue, user);


        this.subTaskBean = subTaskBean;
        this.subTaskView = subTaskView;
        this.contextPath = contextPath;
        this.parentIssue = parentIssue;
        this.i18n = i18n;
    }

    protected String getColumnCssClass() {
        return "streorder";
    }

    @Override
    protected Map<String, Object> getContext(Issue issue) {
        // this is a bit ugly in that the sequence isn't passed in to us.  We just have to assume the render method is only called once
        displaySequence++;

        Long subtaskSequence = getCurrentSubTaskSequence(issue);

        ImmutableMap.Builder<String, Object> builder = ImmutableMap.<String, Object>builder()
            .put("displaySequence", displaySequence)
            .put("allowedReorderSubTasks", allowedReorderSubTasks)
            .put("contextPath", contextPath)
            .put("parentIssue", parentIssue)
            .put("subtaskSequence", subtaskSequence);

        if (displaySequence != 1) {
            //for move up
            builder
                .put("previousSequence", subTaskBean.getPreviousSequence(subtaskSequence, subTaskView))
                .put("subtaskReorderMoveUpTitle", i18n.getText("admin.workflowdescriptor.move.up"))
                .put("subtaskReorderMoveUpText", i18n.getText("admin.workflowdescriptor.move.up"));
        }

        if (displaySequence != getSubTasks().size()) {
            //for move down
            builder
                .put("nextSequence", subTaskBean.getNextSequence(subtaskSequence, subTaskView))
                .put("subtaskReorderMoveDownTitle", i18n.getText("admin.workflowdescriptor.move.down"))
                .put("subtaskReorderMoveDownText", i18n.getText("admin.workflowdescriptor.move.down"));
        }

        return builder.build();
    }

    @Override
    protected String getTemplate() {
        return REORDER_COLUMN_TEMPLATE_PATH;
    }
}