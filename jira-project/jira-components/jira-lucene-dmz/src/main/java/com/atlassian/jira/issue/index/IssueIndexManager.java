package com.atlassian.jira.issue.index;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.comments.Comment;
import com.atlassian.jira.issue.search.SearchProviderFactory;
import com.atlassian.jira.issue.util.IssuesIterable;
import com.atlassian.jira.issue.worklog.Worklog;
import com.atlassian.jira.task.context.Context;
import com.atlassian.jira.util.index.IndexLifecycleManager;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.search.IndexSearcher;
import org.ofbiz.core.entity.GenericValue;

import java.util.Collection;
import java.util.Set;

/**
 * Manages Lucene search indexes.
 */
public interface IssueIndexManager extends IndexLifecycleManager, IssueIndexingService {
    // ================================================================
    // Stuff that actually seems to belong here in the DMZ because it
    // is Lucene-specific or too detailed (like hold and release) to
    // really fit into the long-term picture.
    // ================================================================

    /**
     * Temporarily suspend indexing on this thread.  All index requests will be queued and processed when release is
     * called.
     *
     * @since v5.1
     */
    void hold();

    /**
     * Return true if the index is held.
     *
     * @since v5.1
     */
    boolean isHeld();

    /**
     * Release indexing on this thread.  All queued index requests will be processed.
     *
     * @return Reindex time in ms.
     * @throws IndexException if an error occurs
     * @since v5.1
     */
    long release() throws IndexException;

    /**
     * Get the root path of the index directory for plugins. Any plugin that keeps indexes should create its own
     * sub-directory under this path and create its indexes in its own sub-directory
     */
    String getPluginsRootPath();

    /**
     * Returns a collection of Strings, each one representing the absolute path to the actual <b>existing</b> directory
     * where a plugin keeps its indexes. Each directory in the collection should be a sub-directory under the plugin's
     * index root path. See {@link #getPluginsRootPath()}.
     * <p>
     *     If a plugin index root path does not exist, or is empty
     * (no sub-directopries exist) then an empty collection will be returned.
     * </p>
     */
    Collection<String> getExistingPluginsPaths();

    /**
     * Get an {@link IndexSearcher} that can be used to search the issue index.
     * <p>
     * Note: This is an unmanaged IndexSearcher. You MUST call {@link IndexSearcher#close()} when you are done with it.
     * Alternatively you should really call {@link SearchProviderFactory#getSearcher(String))} passing in {@link
     * SearchProviderFactory#ISSUE_INDEX} as it is a managed searcher and all the closing semantics are handled for
     * you.
     */
    IndexSearcher getIssueSearcher();

    /**
     * Get an {@link IndexSearcher} that can be used to search the comment index.
     * <p>
     * Note: This is an unmanaged IndexSearcher. You MUST call {@link IndexSearcher#close()} when you are done with it.
     * Alternatively you should really call {@link SearchProviderFactory#getSearcher(String))} passing in {@link
     * SearchProviderFactory#COMMENT_INDEX} as it is a managed searcher and all the closing semantics are handled for
     * you.
     */
    IndexSearcher getCommentSearcher();

    /**
     * Get an {@link IndexSearcher} that can be used to search the change history index.
     * <p>
     * Note: This is an unmanaged IndexSearcher. You MUST call {@link IndexSearcher#close()} when you are done with it.
     * Alternatively you should really call {@link SearchProviderFactory#getSearcher(String))} passing in {@link
     * SearchProviderFactory#CHANGE_HISTORY_INDEX} as it is a managed searcher and all the closing semantics are handled
     * for you.
     */
    IndexSearcher getChangeHistorySearcher();

    /**
     * Get an {@link IndexSearcher} that can be used to search the worklog index.
     * <p>
     * Note: This is an unmanaged IndexSearcher. You MUST call {@link IndexSearcher#close()} when you are done with it.
     * Alternatively you should really call {@link SearchProviderFactory#getSearcher(String))} passing in {@link
     * SearchProviderFactory#WORKLOG_INDEX} as it is a managed searcher and all the closing semantics are handled for
     * you.
     */
    IndexSearcher getWorklogSearcher();

    /**
     * Returns an {@link Analyzer} for searching.
     *
     * @return an analyzer for searching
     */
    Analyzer getAnalyzerForSearching();

    /**
     * Returns an {@link Analyzer} for indexing.
     *
     * @return an analyzer for indexing.
     */
    Analyzer getAnalyzerForIndexing();

    /**
     * Runs the given runnable under the 'stop the world' reindex lock.
     *
     * @param runnable The runnable to be executed
     * @return true if the lock could be acquired, false otherwise
     * @since v6.3
     */
    boolean withReindexLock(Runnable runnable);


    // ================================================================================
    // These methods are now declared by IssueIndexingService instead and should be
    // called directly on that interface instead of calling them on the IssueIndexManager
    // ================================================================================

    /**
     * Reindex all issues.
     *
     * @return Reindex time in ms.
     * @deprecated Call this as {@link IssueIndexingService#reIndexAll()} instead. Since v7.0.
     */
    @Deprecated
    @Override
    long reIndexAll() throws IndexException;

    /**
     * Reindex all issues.
     *
     * @param context                    used to report progress back to the user or to the logs. Must not be null
     * @param useBackgroundReindexing    whether to index in the background or not. If the useBackgroundReindexing option
     *                                   is set to true, then all related fields will not be reindexed.
     * @param updateReplicatedIndexStore whether to update the replicated index or not
     * @return Reindex time in ms.
     * @deprecated Call this as {@link IssueIndexingService#reIndexAll(Context, boolean, boolean)} instead. Since v7.0.
     */
    @Deprecated
    @Override
    long reIndexAll(Context context, boolean useBackgroundReindexing, boolean updateReplicatedIndexStore)
            throws IndexException;

    /**
     * Reindex all issues. If the useBackgroundReindexing option is set to true, then only the basic issue information
     * will be reindexed, unless the indexing parameters are also set. This is considered the normal mode for background
     * re-indexing and is sufficient to correct the index for changes in the system configuration, but not for changes
     * to the indexing language. If useBackgroundReindexing is set to false, than everything is always reindexed.
     *
     * @param context                    used to report progress back to the user or to the logs. Must not be null
     * @param useBackgroundReindexing    whether to index in the background or not
     * @param issueIndexingParams        determines witch related objects should be indexed together with issues. Only relevant
     *                                   for background reindex operations.
     * @param updateReplicatedIndexStore whether to update the replicated index or not
     * @return Reindex time in ms.
     * @since v6.4
     * @deprecated Call this as {@link IssueIndexingService#reIndexAll(Context, boolean, IssueIndexingParams, boolean)}
     * instead. Since v7.0.
     */
    @Deprecated
    @Override
    long reIndexAll(Context context, boolean useBackgroundReindexing, IssueIndexingParams issueIndexingParams,
                    boolean updateReplicatedIndexStore) throws IndexException;

    /**
     * Reindex a list of issues, passing an optional event that will be set progress
     *
     * @param issuesIterable IssuesIterable
     * @param context        used to report progress back to the user or to the logs. Must not be null.
     * @return Reindex time in ms.
     * @deprecated Call this as {@link IssueIndexingService#reIndexIssues(IssuesIterable, Context)} instead. Since v7.0.
     */
    @Deprecated
    @Override
    long reIndexIssues(IssuesIterable issuesIterable, Context context) throws IndexException;

    /**
     * Reindex a list of issues, passing an optional event that will be set progress. This method can optionally also
     * index the comments and change history.
     *
     * @param issuesIterable      IssuesIterable
     * @param context             used to report progress back to the user or to the logs. Must not be null.
     * @param issueIndexingParams determines witch related objects should be indexed together with issue.
     * @return Reindex time in ms.
     * @since v6.4
     * @deprecated Call this as {@link IssueIndexingService#reIndexIssues(IssuesIterable, Context, IssueIndexingParams)}
     * instead. Since v7.0.
     */
    @Deprecated
    @Override
    long reIndexIssues(IssuesIterable issuesIterable, Context context, IssueIndexingParams issueIndexingParams)
            throws IndexException;

    /**
     * Reindex an issue (eg. after field updates).
     *
     * @deprecated Call this as {@link IssueIndexingService#reIndex(Issue)} instead. Since v7.0.
     */
    @Deprecated
    @Override
    void reIndex(Issue issue) throws IndexException;

    /**
     * Reindex an issue (eg. after field updates).
     *
     * @since 6.3
     * @deprecated Call this as {@link IssueIndexingService#reIndex(Issue, IssueIndexingParams)} instead. Since v7.0.
     */
    @Deprecated
    @Override
    void reIndex(Issue issue, IssueIndexingParams issueIndexingParams) throws IndexException;

    /**
     * Reindexes a collection of comments.
     *
     * @param comments a collection of Comment
     * @since v5.2
     * @deprecated Call this as {@link IssueIndexingService#reIndexComments(Collection)} instead. Since v7.0.
     */
    @Deprecated
    @Override
    long reIndexComments(Collection<Comment> comments) throws IndexException;

    /**
     * Reindexes a collection of comments.
     *
     * @param comments a collection of Comment
     * @param context  used to report progress back to the user or to the logs. Must not be null.
     * @since v5.2
     * @deprecated Call this as {@link IssueIndexingService#reIndexComments(Collection, Context)} instead. Since v7.0.
     */
    @Deprecated
    @Override
    long reIndexComments(Collection<Comment> comments, Context context) throws IndexException;

    /**
     * Reindexes a collection of comments.
     *
     * @param comments                   a collection of Comment
     * @param context                    used to report progress back to the user or to the logs. Must not be null.
     * @param updateReplicatedIndexStore whether to update the replicated index or not
     * @since v6.1
     * @deprecated Call this as {@link IssueIndexingService#reIndexComments(Collection, Context, boolean)} instead.
     * Since v7.0.
     */
    @Deprecated
    @Override
    long reIndexComments(Collection<Comment> comments, Context context, boolean updateReplicatedIndexStore)
            throws IndexException;

    /**
     * Reindexes a collection of worklogs.
     *
     * @param worklogs a collection of Worklogs
     * @since v6.4
     * @deprecated Call this as {@link IssueIndexingService#reIndexWorklogs(Collection)} instead. Since v7.0.
     */
    @Deprecated
    @Override
    long reIndexWorklogs(Collection<Worklog> worklogs) throws IndexException;

    /**
     * Reindexes a collection of worklogs.
     *
     * @param worklogs a collection of Worklogs
     * @param context  used to report progress back to the user or to the logs. Must not be null.
     * @since v6.4
     * @deprecated Call this as {@link IssueIndexingService#reIndexWorklogs(Collection, Context)} instead. Since v7.0.
     */
    @Deprecated
    @Override
    long reIndexWorklogs(Collection<Worklog> worklogs, Context context) throws IndexException;

    /**
     * Reindexes a collection of worklogs.
     *
     * @param worklogs                   a collection of Worklogs
     * @param context                    used to report progress back to the user or to the logs. Must not be null.
     * @param updateReplicatedIndexStore whether to update the replicated index or not
     * @since v6.4
     * @deprecated Call this as {@link IssueIndexingService#reIndexWorklogs(Collection, Context, boolean)}
     * instead. Since v7.0.
     */
    @Deprecated
    @Override
    long reIndexWorklogs(Collection<Worklog> worklogs, Context context, boolean updateReplicatedIndexStore)
            throws IndexException;

    /**
     * Remove an issue from the search index.
     *
     * @deprecated Call this as {@link IssueIndexingService#deIndex(Issue)} instead. Since v7.0.
     */
    @Deprecated
    @Override
    void deIndex(Issue issue) throws IndexException;

    /**
     * Remove a set of issues from the search index.
     *
     * @deprecated Call this as {@link IssueIndexingService#deIndexIssueObjects(Set, boolean)} instead. Since v7.0.
     */
    @Deprecated
    @Override
    void deIndexIssueObjects(Set<Issue> issuesToDelete, boolean updateReplicatedIndexStore) throws IndexException;

    /**
     * Reindex a set of issues.
     *
     * @param issueObjects Set of {@link Issue}s to reindex.
     * @return Reindex time in ms.
     * @since v5.2
     * @deprecated Call this as {@link IssueIndexingService#reIndexIssueObjects(Collection)} instead. Since v7.0.
     */
    @Deprecated
    @Override
    long reIndexIssueObjects(final Collection<? extends Issue> issueObjects) throws IndexException;

    /**
     * Reindex a set of issues.
     *
     * @param issueObjects        Set of {@link Issue}s to reindex.
     * @param issueIndexingParams Determines witch related objects should be indexed together with issues.
     * @return Reindex time in ms.
     * @since v6.4
     * @deprecated Call this as {@link IssueIndexingService#reIndexIssueObjects(Collection, IssueIndexingParams)}
     * instead. Since v7.0.
     */
    @Deprecated
    @Override
    long reIndexIssueObjects(final Collection<? extends Issue> issueObjects, IssueIndexingParams issueIndexingParams)
            throws IndexException;

    /**
     * Reindex a set of issues.
     *
     * @param issueObjects               Set of {@link Issue}s to reindex.
     * @param issueIndexingParams        Determines witch related objects should be indexed together with issues.
     * @param updateReplicatedIndexStore whether to store index operations in the replicated index store
     * @return Reindex time in ms.
     * @since v6.4
     * @deprecated Call this as {@link IssueIndexingService#reIndexIssueObjects(Collection, IssueIndexingParams,
     * boolean)} instead. Since v7.0.
     */
    @Deprecated
    @Override
    long reIndexIssueObjects(final Collection<? extends Issue> issueObjects, IssueIndexingParams issueIndexingParams, boolean updateReplicatedIndexStore)
            throws IndexException;


    // ================================================================
    // These methods were deprecated prior to v7.0, so they do not have
    // direct replacements in IssueIndexingService.
    // ================================================================

    /**
     * Reindex all issues. If the useBackgroundReindexing option is set to true, then only the basic issue information
     * will be reindexed, unless the reIndexComments or reIndexChangeHistory parameters are also set. This is considered
     * the normal mode for background re-indexing and is sufficient to correct the index for changes in the system
     * configuration, but not for changes to the indexing language. If useBackgroundReindexing is set to false, than
     * everything is always reindexed.
     *
     * @param context                    used to report progress back to the user or to the logs. Must not be null
     * @param useBackgroundReindexing    whether to index in the background or not
     * @param reIndexComments            Also reindex all the issue comments. Only relevant for background reindex operations.
     * @param reIndexChangeHistory       Also reindex the issue change history. Only relevant for background reindex
     *                                   operations.
     * @param updateReplicatedIndexStore whether to update the replicated index or not
     * @return Reindex time in ms.
     * @since 6.2
     * @deprecated since v6.4 use {@link #reIndexAll(Context, boolean, IssueIndexingParams, boolean)} instead.  Since
     * v7.0 use {@link IssueIndexingService#reIndexAll(Context, boolean, IssueIndexingParams, boolean)} instead.
     */
    @Deprecated
    long reIndexAll(Context context, boolean useBackgroundReindexing, boolean reIndexComments,
                    boolean reIndexChangeHistory, boolean updateReplicatedIndexStore) throws IndexException;

    /**
     * Reindex an issue (eg. after field updates).
     *
     * @deprecated Since v5.0. Use {@link #reIndex(Issue)} instead.  Since v7.0, use {@link
     * IssueIndexingService#reIndex(Issue)} instead.
     */
    @Deprecated
    void reIndex(GenericValue issue) throws IndexException;

    /**
     * Reindex a list of issues, passing an optional event that will be set progress. This method can optionally also
     * index the comments and change history.
     *
     * @param issuesIterable       IssuesIterable
     * @param context              used to report progress back to the user or to the logs. Must not be null.
     * @param reIndexComments      a boolean indicating whether to index issue comments
     * @param reIndexChangeHistory a boolean indicating whether to index issue change history
     * @return Reindex time in ms.
     * @since v5.2
     * @deprecated Since v6.4, use {@link #reIndexIssues(IssuesIterable, Context, IssueIndexingParams)}.  Since v7.0,
     * use {@link IssueIndexingService#reIndexIssues(IssuesIterable, Context, IssueIndexingParams)}.
     */
    @Deprecated
    long reIndexIssues(IssuesIterable issuesIterable, Context context, boolean reIndexComments,
                       boolean reIndexChangeHistory) throws IndexException;

    /**
     * Reindex an issue (eg. after field updates).
     *
     * @since v5.2
     * @deprecated Since v6.4, use {@link #reIndex(Issue, IssueIndexingParams)}.  Since v7.0, use {@link
     * IssueIndexingService#reIndex(Issue, IssueIndexingParams)}.
     */
    @Deprecated
    void reIndex(Issue issue, boolean reIndexComments, boolean reIndexChangeHistory) throws IndexException;

    /**
     * Remove an issue from the search index.
     *
     * @deprecated Since v5.0 use {@link #deIndex(Issue)} instead. Since v7.0 use {@link
     * IssueIndexingService#deIndex(Issue)} instead.
     */
    @Deprecated
    void deIndex(GenericValue issue) throws IndexException;

    /**
     * Reindex a set of issues (GenericValues). Use {@link #reIndexIssueObjects(Collection)} instead when possible.
     *
     * @param issues The Issue {@link GenericValue}s to reindex.
     * @return Reindex time in ms.
     * @deprecated Since v5.0, use {@link #reIndexIssueObjects(Collection)} instead. Since v7.0, use
     * {@link IssueIndexingService#reIndexIssueObjects(Collection)} instead.
     */
    @Deprecated
    long reIndexIssues(final Collection<GenericValue> issues) throws IndexException;

    /**
     * Reindex a set of issues.
     *
     * @param issueObjects Set of {@link Issue}s to reindex.
     * @return Reindex time in ms.
     * @deprecated Since v6.4, use {@link #reIndexIssueObjects(Collection, IssueIndexingParams)}.  Since v7.0, use
     * {@link IssueIndexingService#reIndexIssueObjects(Collection, IssueIndexingParams)}.
     */
    @Deprecated
    long reIndexIssueObjects(final Collection<? extends Issue> issueObjects, boolean reIndexComments,
                             boolean reIndexChangeHistory) throws IndexException;

    /**
     * Reindex a set of issues.
     *
     * @param issueObjects               Set of {@link Issue}s to reindex.
     * @param reIndexComments            whether to reindex the comments or not
     * @param reIndexChangeHistory       whether to reindex changeHistory or not
     * @param updateReplicatedIndexStore whether to store index operations in the replicated index store
     * @return Reindex time in ms.
     * @deprecated Since v6.4, use {@link #reIndexIssueObjects(Collection, IssueIndexingParams, boolean)}.  Since v7.0,
     * use {@link IssueIndexingService#reIndexIssueObjects(Collection, IssueIndexingParams, boolean)}.
     */
    @Deprecated
    long reIndexIssueObjects(final Collection<? extends Issue> issueObjects, boolean reIndexComments,
                             boolean reIndexChangeHistory, boolean updateReplicatedIndexStore) throws IndexException;
}
