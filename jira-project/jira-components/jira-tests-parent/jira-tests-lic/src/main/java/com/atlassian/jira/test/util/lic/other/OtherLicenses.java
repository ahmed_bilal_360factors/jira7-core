package com.atlassian.jira.test.util.lic.other;

import com.atlassian.jira.test.util.lic.License;
import com.atlassian.jira.test.util.lic.LicenseReader;

/**
 * Other licenses used in testing, these licenses aren't known JIRA Applications (they represent possible future
 * products).
 *
 * @since v7.0
 */
public final class OtherLicenses {
    /**
     * A JIRA Other Application with three users (made-up application).
     */
    public static final License LICENSE_OTHER = getLicense("other-app.lic");
    /**
     * A JIRA Reference Application with five users (made-up application).
     */
    public static final License LICENSE_REFERENCE = getLicense("reference-app.lic");

    /**
     * An old JIRA Pre Renaissance (before JIRA Applications) license with 2 users.
     */
    public static final License LICENSE_PRE_JIRA_APP_V2_COMMERCIAL = getLicense("pre-jira-app-commercial-old.lic");

    private static License getLicense(final String license) {
        return LicenseReader.readLicense(license, OtherLicenses.class);
    }

    private OtherLicenses() {
        //I don't want to be created.
    }
}
