package com.atlassian.jira.test.util.lic.sd;

import com.atlassian.jira.test.util.lic.License;
import com.atlassian.jira.test.util.lic.LicenseReader;

/**
 * Some Service Desk (SD) licenses used in testing. SD has three different types of license:
 * <p>
 * <table summary="License Summary">
 * <tr>
 * <td>License Type</td>
 * <td>SD Version</td>
 * <td>Plugin or JIRA License</td>
 * <td>Description</td>
 * </tr>
 * <tr>
 * <td>Tier Based Pricing (TBP)</td>
 * <td>1.x</td>
 * <td>Plugin</td>
 * <td>This is where SD has to be licensed for the same number of users as JIRA. For example, a JIRA licensed
 * with a JIRA commercial 500 user license will require a SD commercial 500 user license.
 * This means that every user is a SD agent (even customers).</td>
 * </tr>
 * <tr>
 * <td>Agent Based Pricing (ABP)</td>
 * <td>2.x</td>
 * <td>Plugin</td>
 * <td>This is where SD has its own user limit on the number of agents. The only requirement is that the the
 * SD agent limit be less than or equal to the JIRA user limit. This allows SD to bless a subset of
 * the JIRA users as SD agents (e.g supporters) which allows customers to only pay for the agents
 * they need. As an added advantage people using the customer view are free.</td>
 * </tr>
 * <tr>
 * <td>Role Based Pricing (RBP)</td>
 * <td>3.x</td>
 * <td>JIRA</td>
 * <td>Same as the ABP (mostly) except that it is licensed as a JIRA Product rather than a plugin.</td>
 * </tr>
 * </table>
 *
 * @since v7.0
 */
public final class ServiceDeskLicenses {
    /**
     * A JIRA based Service Desk RBP license (aka. Service Desk v3).
     */
    public static final License LICENSE_SERVICE_DESK_RBP = getLicense("service-desk-rbp-app-role.lic");

    /**
     * A JIRA based Service Desk RBP evaluation license (aka. Service Desk v3).
     */
    public static final License LICENSE_SERVICE_DESK_RBP_EVAL = getLicense("service-desk-rbp-app-role-evaluation.lic");

    /**
     * Service Desk TBP license (aka. Service Desk v1).
     */
    public static final License LICENSE_SERVICE_DESK_TBP_UNLIMITED = getLicense("service-desk-tbp-unlimited.lic");

    /**
     * Service Desk TBP license (aka. Service Desk v1).
     */
    public static final License LICENSE_SERVICE_DESK_TBP = getLicense("service-desk-tbp.lic");

    /**
     * Service Desk TBP evaluation license (aka. Service Desk v1).
     */
    public static final License LICENSE_SERVICE_DESK_TBP_EVAL = getLicense("service-desk-tbp-evaluation.lic");

    /**
     * Service Desk TBP evaluation license that has already expired (aka. Service Desk v1).
     */
    public static final License LICENSE_SERVICE_DESK_TBP_EVAL_EXPIRED = getLicense("service-desk-tbp-eval-expired.lic");

    /**
     * Service Desk ABP license (aka. Service Desk v2) with 100 agents.
     */
    public static final License LICENSE_SERVICE_DESK_ABP = getLicense("service-desk-abp.lic");

    /**
     * Service Desk ABP evaluation license (aka. Service Desk v2).
     */
    public static final License LICENSE_SERVICE_DESK_ABP_EVAL = getLicense("service-desk-abp-evaluation.lic");

    /**
     * Service Desk ABP academic license (aka. Service Desk v2).
     */
    public static final License LICENSE_SERVICE_DESK_ABP_ACADEMIC = getLicense("service-desk-abp-academic.lic");

    /**
     * Service Desk ABP community license (aka. Service Desk v2).
     */
    public static final License LICENSE_SERVICE_DESK_ABP_COMMUNITY = getLicense("service-desk-abp-community.lic");

    /**
     * A Service Desk legacy license with no-user count. Its an invalid license.
     */
    public static final License LICENSE_SERVICE_DESK_BAD_LEGACY = getLicense("service-desk-legacy-bad.lic");

    /**
     * Service Desk license with both application and plugin properties.
     */
    public static final License LICENSE_SERVICE_DESK_JIRA7 = getLicense("service-desk-jira7.lic");

    /**
     * Service Desk license with application properties only.
     */
    public static final License LICENSE_SERVICE_DESK_JIRA7_NON_BACKWARD_COMPATIBLE =
            getLicense("service-desk-jira7-non-backward-compatible.lic");


    private static License getLicense(final String license) {
        return LicenseReader.readLicense(license, ServiceDeskLicenses.class);
    }

    private ServiceDeskLicenses() {
        //I don't want to be created.
    }
}
