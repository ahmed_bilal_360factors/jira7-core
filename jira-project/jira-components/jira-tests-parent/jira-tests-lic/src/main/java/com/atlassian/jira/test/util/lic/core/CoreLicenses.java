package com.atlassian.jira.test.util.lic.core;

import com.atlassian.jira.test.util.lic.License;
import com.atlassian.jira.test.util.lic.LicenseReader;

/**
 * JIRA Core Application licenses used in testing.
 *
 * @since v7.0
 */
public final class CoreLicenses {
    /**
     * A JIRA Core Application with three users.
     */
    public static final License LICENSE_CORE = getLicense("core-app.lic");

    private static License getLicense(final String license) {
        return LicenseReader.readLicense(license, CoreLicenses.class);
    }

    private CoreLicenses() {
        //I don't want to be created.
    }
}
