package com.atlassian.jira.test.util.lic;

public final class ApplicationLicenseConstants {
    public static final String TEST_KEY = "jira-func-test";
    public static final String CORE_KEY = "jira-core";
    public static final String SOFTWARE_KEY = "jira-software";
    public static final String SERVICE_DESK_KEY = "jira-servicedesk";
    public static final String REFERENCE_KEY = "jira-reference";

    private ApplicationLicenseConstants() {
    }
}
