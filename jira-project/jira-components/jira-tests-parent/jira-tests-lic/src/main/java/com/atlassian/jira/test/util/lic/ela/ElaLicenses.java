package com.atlassian.jira.test.util.lic.ela;

import com.atlassian.jira.test.util.lic.License;
import com.atlassian.jira.test.util.lic.LicenseReader;

/**
 * JIRA ELA licenses used in testing.
 *
 * @since v7.0
 */
public final class ElaLicenses {
    /**
     * A JIRA ELA license with:
     * JIRA Func Test       1 user limit.
     * JIRA Service Desk    2 user limit.
     * JIRA Core            3 user limit.
     * JIRA Software        4 user limit.
     * JIRA Reference       5 user limit.
     * JIRA Other           6 user limit.
     */
    public static final License LICENSE_ELA_CORE_SW_SD_TEST_REF_OTHER = getLicense("ela-core-sw-servicedesk-test-reference-other.lic");

    /**
     * JIRA 6.x ELA license with Service Desk TBP
     */
    public static final License LICENSE_ELA_6x_SERVICEDESK_TBP = getLicense("ela-6x-servicedesk-tbp.lic");

    /**
     * JIRA 6.x ELA license with Service Desk ABP
     */
    public static final License LICENSE_ELA_6x_SERVICEDESK_ABP = getLicense("ela-6x-servicedesk-abp.lic");

    private static License getLicense(final String license) {
        return LicenseReader.readLicense(license, ElaLicenses.class);
    }

    private ElaLicenses() {
        //I don't want to be created.
    }
}
