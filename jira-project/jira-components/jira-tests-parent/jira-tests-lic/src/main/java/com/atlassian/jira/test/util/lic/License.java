package com.atlassian.jira.test.util.lic;

import com.atlassian.extras.api.LicenseType;
import com.atlassian.fugue.Option;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Date;
import java.util.Map;

/**
 * Represents a license that can be used in testing.
 *
 * @since v7.0
 */
public interface License {
    /**
     * Return the raw license key.
     *
     * @return the raw license key.
     */
    String getLicenseString();

    /**
     * Return the description contained in the license.
     *
     * @return the description contained in the license.
     */
    String getDescription();

    /**
     * Return the organisation in the license.
     *
     * @return the organisation in the license.
     */
    String getOrganisation();

    /**
     * Return the SEN in the license.
     *
     * @return the SEN in the license.
     */
    String getSen();

    /**
     * Is the license an evaluation license?
     *
     * @return is the license an evaluation license?
     */
    boolean isEvaluation();

    /**
     * Return the expiry date of the license or {@link com.atlassian.fugue.Option#none()} if its a perpetual license.
     *
     * @return the expiry date of the license or {@link com.atlassian.fugue.Option#none()} if its a perpetual license.
     */
    Option<Date> getSubscriptionExpiryDate();

    /**
     * Return the maintenance expiry of the license or {@link com.atlassian.fugue.Option#none()} if its a
     * perpetual license.
     *
     * @return the maintenance expiry of the license or {@link com.atlassian.fugue.Option#none()} if its a
     * perpetual license.
     */
    Option<Date> getMaintenanceExpiryDate();

    /**
     * Return a map of the roles in the license.
     *
     * @return a map of the roles in the license.
     */
    Map<String, Role> getRoles();

    /**
     * Return the {@link com.atlassian.extras.api.LicenseType} for the license.
     *
     * @return the license type.
     */
    LicenseType getLicenseType();

    @Nullable
    String getProperty(@Nonnull String property);

    interface Role {
        String getProduct();

        int getNumberOfUsers();

        boolean isUnlimitedUsers();
    }
}
