package com.atlassian.jira.test.util.lic;

import com.atlassian.extras.api.LicenseType;
import com.atlassian.extras.api.ProductLicense;
import com.atlassian.fugue.Option;
import com.google.common.collect.ImmutableMap;

import javax.annotation.Nullable;
import java.util.Date;
import java.util.Map;

import static com.google.common.base.Preconditions.checkNotNull;
import static org.apache.commons.lang.StringUtils.stripToNull;

final class ProductLicenseAdapter implements License {
    private final String key;
    private final ProductLicense productLicense;
    private final Map<String, Role> roles;

    ProductLicenseAdapter(final String key, final ProductLicense productLicense, Map<String, Role> roles) {
        this.productLicense = checkNotNull(productLicense, "productLicense is null");
        this.key = checkNotNull(stripToNull(key), "key is empty");
        this.roles = ImmutableMap.copyOf(checkNotNull(roles, "roles is blank"));
    }

    @Override
    public String getLicenseString() {
        return key;
    }

    @Override
    public String getDescription() {
        return productLicense.getDescription();
    }

    @Override
    public String getOrganisation() {
        return productLicense.getOrganisation().getName();
    }

    @Override
    public String getSen() {
        return productLicense.getSupportEntitlementNumber();
    }

    @Override
    public boolean isEvaluation() {
        return productLicense.isEvaluation();
    }

    @Override
    public Option<Date> getSubscriptionExpiryDate() {
        return Option.option(productLicense.getExpiryDate());
    }

    @Override
    public Option<Date> getMaintenanceExpiryDate() {
        return Option.option(productLicense.getMaintenanceExpiryDate());
    }

    @Override
    public Map<String, Role> getRoles() {
        return roles;
    }

    @Override
    public LicenseType getLicenseType() {
        return productLicense.getLicenseType();
    }

    @Override
    @Nullable
    public String getProperty(String property) {
        return productLicense.getProperty(property);
    }


    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final ProductLicenseAdapter that = (ProductLicenseAdapter) o;

        if (!key.equals(that.key)) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        return key.hashCode();
    }

    @Override
    public String toString() {
        return key;
    }
}
