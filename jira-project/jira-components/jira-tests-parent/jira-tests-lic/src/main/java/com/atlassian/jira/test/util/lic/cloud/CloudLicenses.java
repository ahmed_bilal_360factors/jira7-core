package com.atlassian.jira.test.util.lic.cloud;

import com.atlassian.jira.test.util.lic.License;
import com.atlassian.jira.test.util.lic.LicenseReader;

/**
 * JIRA Cloud licenses used in testing.
 *
 * @since v7.0
 */
public final class CloudLicenses {
    /**
     * JIRA 6.x OnDemand license with  Service Desk TBP
     */
    public static final License LICENSE_ONDEMAND_6x_SERVICEDESK_TBP = getLicense("ondemand-6x-servicedesk-tbp.lic");

    private static License getLicense(final String license) {
        return LicenseReader.readLicense(license, CloudLicenses.class);
    }

    private CloudLicenses() {

    }
}
