package com.atlassian.jira.test.util.lic.software;

import com.atlassian.jira.test.util.lic.License;
import com.atlassian.jira.test.util.lic.LicenseReader;

/**
 * JIRA Software Application licenses used in testing.
 *
 * @since v7.0
 */
public final class SoftwareLicenses {
    /**
     * A JIRA Software Application with four users.
     */
    public static final License LICENSE_SOFTWARE = getLicense("software-app.lic");

    private static License getLicense(final String license) {
        return LicenseReader.readLicense(license, SoftwareLicenses.class);
    }

    private SoftwareLicenses() {
        //I don't want to be created.
    }
}
