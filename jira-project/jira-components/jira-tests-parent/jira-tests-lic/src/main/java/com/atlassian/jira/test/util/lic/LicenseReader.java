package com.atlassian.jira.test.util.lic;

import com.atlassian.extras.api.AtlassianLicense;
import com.atlassian.extras.api.LicenseManager;
import com.atlassian.extras.api.Product;
import com.atlassian.extras.api.ProductLicense;
import com.atlassian.extras.common.util.LicenseProperties;
import com.atlassian.extras.common.util.ProductLicenseProperties;
import com.atlassian.extras.core.AtlassianLicenseFactory;
import com.atlassian.extras.core.DefaultAtlassianLicenseFactory;
import com.atlassian.extras.core.DefaultLicenseManager;
import com.atlassian.extras.core.DefaultProductLicense;
import com.atlassian.extras.core.ProductLicenseFactory;
import com.atlassian.extras.core.jira.JiraProductLicenseFactory;
import com.atlassian.extras.core.plugins.PluginLicenseFactory;
import com.atlassian.extras.decoder.api.DelegatingLicenseDecoder;
import com.atlassian.extras.decoder.api.LicenseDecoder;
import com.atlassian.extras.decoder.v1.Version1LicenseDecoder;
import com.atlassian.extras.decoder.v2.Version2LicenseDecoder;
import com.atlassian.fugue.Option;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.google.common.collect.Maps;
import org.apache.commons.io.IOUtils;

import javax.annotation.Nullable;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.atlassian.extras.common.LicensePropertiesConstants.ACTIVE_FLAG;
import static com.atlassian.extras.common.LicensePropertiesConstants.ACTIVE_VALUE;
import static com.atlassian.extras.common.LicensePropertiesConstants.MAX_NUMBER_OF_USERS;
import static com.atlassian.jira.test.util.lic.License.Role;

/**
 * Simply utility class to read {@link License} objects used in tests.
 *
 * @since v7.0
 */
public final class LicenseReader {
    private static final Product SERVICE_DESK = new Product("ServiceDesk", "com.atlassian.servicedesk", true);
    private static final Product JIRA_ROLE = new Product("JIRA Role", "jira", false);
    private static final List<Product> LOOKUP_ORDER = ImmutableList.of(JIRA_ROLE, SERVICE_DESK, Product.JIRA);

    private static final Pattern ACTIVE_PATTERN = Pattern.compile("jira\\.product\\.(.+)\\.active");
    private static final String NUMBER_USERS_TEMPLATE = "jira.product.%s.NumberOfUsers";

    private static final String SERVICE_DESK_ACTIVE = "com.atlassian.servicedesk.active";
    private static final String SERVICE_DESK_COUNT = "com.atlassian.servicedesk.numRoleCount";

    private static final String SOFTWARE_PRODUCT = "jira-software";
    private static final String SERVICE_DESK_PRODUCT = "jira-servicedesk";

    private static final LicenseDecoder licenseDecoder = new DelegatingLicenseDecoder(ImmutableList.<LicenseDecoder>of(
            new Version2LicenseDecoder(), new Version1LicenseDecoder()));
    private static final LicenseManager licenseManager = getLicenseManager(licenseDecoder);

    private LicenseReader() {
    }

    /**
     * Try and read the passed resource as an encoded license string. The search for the resource trys two methods:
     * <ul>
     * <li>{@link Class#getResourceAsStream(String)} on {@code base}.</li>
     * <li>{@link java.lang.ClassLoader#getResourceAsStream(String)} on {@code base.getClassLoader()}.</li>
     * </ul>
     * <p>
     * A runtime exception will be thrown if the license cannot be found.
     *
     * @param resource the resource to read.
     * @param base     the class that will be used to resolve the resource.
     * @return the encoded license string.
     * @throws java.lang.RuntimeException if the license cannot be found.
     */
    public static String readLicenseEncodedString(final String resource, final Class<?> base) {
        try (InputStream input = openResource(resource, base)) {
            return IOUtils.toString(input, StandardCharsets.UTF_8.name());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Try and read the passed resource as a License. The search for the resource tries two methods:
     * <ul>
     * <li>{@link Class#getResourceAsStream(String)} on {@code base}.</li>
     * <li>{@link java.lang.ClassLoader#getResourceAsStream(String)} on {@code base.getClassLoader()}.</li>
     * </ul>
     * <p>
     * A runtime exception will be thrown if the license cannot be found.
     *
     * @param resource the resource to read.
     * @param base     the class that will be used to resolve the resource.
     * @return the parsed license.
     * @throws java.lang.RuntimeException if the license cannot be found.
     */
    public static License readLicense(final String resource, final Class<?> base) {
        final String key = readLicenseEncodedString(resource, base);
        final ProductLicense productLicense = getProductLicense(licenseManager.getLicense(key));
        if (productLicense == null) {
            throw new IllegalArgumentException(String.format("Resource '%s' does not contain a license.", resource));
        }

        return new ProductLicenseAdapter(key, productLicense, parseRoles(key));
    }

    private static ProductLicense getProductLicense(final AtlassianLicense license) {
        for (final Product product : LOOKUP_ORDER) {
            final ProductLicense productLicense = license.getProductLicense(product);
            if (productLicense != null) {
                return productLicense;
            }
        }

        return null;
    }

    private static InputStream openResource(final String resource, final Class<?> base) {
        InputStream stream = base.getResourceAsStream(resource);
        if (stream == null) {
            stream = base.getClassLoader().getResourceAsStream(resource);
            if (stream == null) {
                throw new RuntimeException(String.format("Unable to find resource '%s'.", resource));
            }
        }
        return stream;
    }

    private static LicenseManager getLicenseManager(final LicenseDecoder decoder) {
        final Map<Product, ProductLicenseFactory> licenseFactoryMap = new HashMap<>();

        licenseFactoryMap.put(Product.JIRA, new JiraProductLicenseFactory());
        licenseFactoryMap.put(SERVICE_DESK, new PluginLicenseFactory(SERVICE_DESK));
        licenseFactoryMap.put(JIRA_ROLE, new JiraRoleBasedLicenseFactory());

        final AtlassianLicenseFactory atlassianLicenseFactory = new DefaultAtlassianLicenseFactory(licenseFactoryMap);
        return new DefaultLicenseManager(decoder, atlassianLicenseFactory);
    }

    private static Map<String, Role> parseRoles(String license) {
        final List<Role> roles = new ArrayList<>();
        final Properties properties = licenseDecoder.decode(license);
        for (Map.Entry<Object, Object> entry : properties.entrySet()) {
            final String key = entry.getKey().toString();
            final String value = entry.getValue().toString();
            final Matcher matcher = ACTIVE_PATTERN.matcher(key);
            if (matcher.matches() && ACTIVE_VALUE.equals(value)) {
                final String namespace = matcher.group(1);
                final String roleUsers = properties.getProperty(String.format(NUMBER_USERS_TEMPLATE, namespace));
                Iterables.addAll(roles, roleFor(namespace, roleUsers));
            }
        }

        if (roles.isEmpty()) {
            Iterables.addAll(roles, fromOldJiraLicense(properties));
            Iterables.addAll(roles, fromServiceDeskLicense(properties));
        }

        return Maps.uniqueIndex(roles, new Function<Role, String>() {
            @Override
            public String apply(@Nullable final Role input) {
                return input != null ? input.getProduct() : "NULL";
            }
        });
    }

    private static Option<Role> fromServiceDeskLicense(Properties properties) {
        if (Boolean.parseBoolean(properties.getProperty(SERVICE_DESK_ACTIVE))) {
            String userCount = properties.getProperty(SERVICE_DESK_COUNT);
            if (userCount == null) {
                userCount = properties.getProperty(MAX_NUMBER_OF_USERS);
            }
            return roleFor(SERVICE_DESK_PRODUCT, userCount);
        } else {
            return Option.none();
        }
    }

    private static Option<Role> fromOldJiraLicense(Properties properties) {
        final ProductLicenseProperties licenseProperties = new ProductLicenseProperties(Product.JIRA, properties);
        if (licenseProperties.getBoolean(ACTIVE_FLAG)) {
            return roleFor(SOFTWARE_PRODUCT, licenseProperties.getProperty(MAX_NUMBER_OF_USERS));
        } else {
            return Option.none();
        }
    }

    private static Option<Role> roleFor(String product, @Nullable String count) {
        if (count != null) {
            final int countNumber = Integer.parseInt(count);
            return Option.some(countNumber < 0 ? new UnlimitedRole(product) : new LimitedRole(product, countNumber));
        } else {
            return Option.none();
        }
    }

    private static class UnlimitedRole implements Role {
        private final String product;

        private UnlimitedRole(final String product) {
            this.product = product;
        }

        @Override
        public String getProduct() {
            return product;
        }

        @Override
        public int getNumberOfUsers() {
            return -1;
        }

        @Override
        public boolean isUnlimitedUsers() {
            return true;
        }
    }

    private static class LimitedRole implements Role {
        private final int count;
        private final String product;

        private LimitedRole(final String product, final int count) {
            this.count = count;
            this.product = product;
        }

        @Override
        public String getProduct() {
            return product;
        }

        @Override
        public int getNumberOfUsers() {
            return count;
        }

        @Override
        public boolean isUnlimitedUsers() {
            return false;
        }
    }

    private static class JiraRoleBasedLicenseFactory extends JiraProductLicenseFactory {
        private static final String PRODUCT_PREFIX = "jira.product";

        @Override
        public ProductLicense getLicenseInternal(final Product product, final LicenseProperties licenseProperties) {
            return new TestProductLicense(product, licenseProperties);
        }

        @Override
        public boolean hasLicense(final Product product, final LicenseProperties licenseProperties) {
            for (final String key : licenseProperties.getPropertiesEndingWith(ACTIVE_FLAG).keySet()) {
                if (key.startsWith(PRODUCT_PREFIX)) {
                    return true;
                }
            }
            return false;
        }
    }

    private static class TestProductLicense extends DefaultProductLicense {
        private TestProductLicense(final Product product, final LicenseProperties properties) {
            super(product, properties);
        }
    }
}
