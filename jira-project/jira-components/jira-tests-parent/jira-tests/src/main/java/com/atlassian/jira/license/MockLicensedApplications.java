package com.atlassian.jira.license;

import com.atlassian.application.api.ApplicationKey;

import javax.annotation.Nonnull;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import static com.atlassian.extras.common.LicensePropertiesConstants.UNLIMITED_USERS;

/**
 * @since v7.0
 */
public class MockLicensedApplications implements LicensedApplications {
    /**
     * Map of application role key to number of seats for that role.
     */
    private Map<ApplicationKey, Integer> applicationUserCounts;

    public MockLicensedApplications() {
        applicationUserCounts = new HashMap<>();
    }

    public MockLicensedApplications(@Nonnull Map<ApplicationKey, Integer> applicationsWithCounts) {
        applicationUserCounts = new HashMap<>(applicationsWithCounts);
    }

    public MockLicensedApplications(@Nonnull Iterable<ApplicationKey> roles) {
        this();
        for (ApplicationKey role : roles) {
            applicationUserCounts.put(role, UNLIMITED_USERS);
        }
    }

    public MockLicensedApplications(@Nonnull ApplicationKey... roles) {
        this(Arrays.asList(roles));
    }

    /**
     * Add the given number of seats to the given role, returning the new total.
     */
    public MockLicensedApplications addSeats(@Nonnull ApplicationKey application, int numSeats) {
        assert numSeats > 0;
        int currentSeats = getUserLimit(application);
        if (currentSeats == UNLIMITED_USERS) return this;

        applicationUserCounts.put(application, numSeats + currentSeats);

        return this;
    }

    @Nonnull
    @Override
    public Set<ApplicationKey> getKeys() {
        return applicationUserCounts.keySet();
    }

    @Override
    public int getUserLimit(@Nonnull ApplicationKey role) {
        Integer numSeats = applicationUserCounts.get(role);
        return (numSeats != null) ? numSeats : 0;
    }

    @Nonnull
    @Override
    public String getDescription() {
        return "";
    }

    @Override
    public boolean hasNativeRole() {
        return false;
    }

    @Override
    public String toString() {
        return applicationUserCounts.keySet().toString();
    }
}
