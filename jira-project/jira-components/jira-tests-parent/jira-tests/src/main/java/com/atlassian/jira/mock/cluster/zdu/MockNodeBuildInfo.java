package com.atlassian.jira.mock.cluster.zdu;

import com.atlassian.jira.cluster.zdu.NodeBuildInfo;

import java.util.Objects;

public class MockNodeBuildInfo implements NodeBuildInfo {
    private final long buildNumber;
    private final String version;

    public MockNodeBuildInfo(final long buildNumber, final String version) {
        this.buildNumber = buildNumber;
        this.version = version;
    }

    @Override
    public long getBuildNumber() {
        return buildNumber;
    }

    @Override
    public String getVersion() {
        return version;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MockNodeBuildInfo that = (MockNodeBuildInfo) o;
        return getBuildNumber() == that.getBuildNumber() &&
                Objects.equals(getVersion(), that.getVersion());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getBuildNumber(), getVersion());
    }
}
