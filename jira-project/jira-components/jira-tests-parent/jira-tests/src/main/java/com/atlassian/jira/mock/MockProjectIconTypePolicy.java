package com.atlassian.jira.mock;

import com.atlassian.jira.avatar.Avatar;
import com.atlassian.jira.bc.project.ProjectService;
import com.atlassian.jira.icon.IconOwningObjectId;
import com.atlassian.jira.plugin.icon.IconTypePolicy;
import com.atlassian.jira.plugin.icon.ProjectIconTypePolicy;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.ApplicationUser;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public class MockProjectIconTypePolicy extends ProjectIconTypePolicy implements IconTypePolicy {

    public MockProjectIconTypePolicy() {
        super(null, null, null, null);
    }

    public MockProjectIconTypePolicy(GlobalPermissionManager globalPermissionManager, PermissionManager permissionManager, ProjectService projectService, ProjectManager projectManager) {
        super(globalPermissionManager, permissionManager, projectService, projectManager);
    }

    @Override
    public boolean userCanView(@Nullable ApplicationUser remoteUser, @Nonnull Avatar icon) {
        return true;
    }

    @Override
    public boolean userCanDelete(@Nullable ApplicationUser remoteUser, @Nonnull Avatar icon) {
        return true;
    }

    @Override
    public boolean userCanCreateFor(@Nullable ApplicationUser remoteUser, @Nonnull IconOwningObjectId owningObjectId) {
        return true;
    }
}
