package com.atlassian.jira.matchers;

import com.atlassian.fugue.Pair;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.hamcrest.TypeSafeDiagnosingMatcher;
import org.hamcrest.TypeSafeMatcher;

import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.stream.Collectors;

import static com.atlassian.fugue.Pair.pair;

/**
 * Matchers for {@link java.util.Map}s.
 *
 * @since v5.2
 */
public final class MapMatchers {
    private MapMatchers() {
        throw new AssertionError("Don't instantiate me");
    }

    public static <K, V> Matcher<Map<K, V>> isSingletonMap(final K expectedKey, final V expectedValue) {
        return new TypeSafeMatcher<Map<K, V>>() {
            @Override
            protected boolean matchesSafely(Map<K, V> map) {
                return map.size() == 1 && map.containsKey(expectedKey) && map.containsValue(expectedValue);
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("a map with exactly one entry: [").appendValue(expectedKey)
                        .appendText(", ").appendValue(expectedValue).appendText("]");

            }
        };
    }

    public static <K> Matcher<Map<? extends K, ?>> hasKeyThat(final Matcher<K> keyMatcher) {
        return new TypeSafeMatcher<Map<? extends K, ?>>() {
            @Override
            protected boolean matchesSafely(Map<? extends K, ?> map) {
                return map.keySet().stream().anyMatch(keyMatcher::matches);
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("a map containing key that ").appendDescriptionOf(keyMatcher);
            }
        };
    }

    /**
     * Matcher that checks that the elements of a Map are in a particular order.
     *
     * @param map the map the match against.
     * @param <K> the type of the keys.
     * @param <V> the type of the values.
     * @return a matcher that checks that the elements of a Map are in a particular order.
     */
    public static <K, V> OrderedMapMatcher<K, V> orderedMap(SortedMap<K, V> map) {
        return OrderedMapMatcher.orderedMap(map);
    }

    /**
     * Matcher that checks that the elements of a Map are in a particular order.
     *
     * @param key1   the value of the first key.
     * @param value1 the value of the second key.
     * @param <K>    the type of the keys.
     * @param <V>    the type of the values.
     * @return a matcher that checks that the elements of a Map are in a particular order.
     */
    public static <K, V> OrderedMapMatcher<K, V> orderedMap(K key1, V value1) {
        return OrderedMapMatcher.orderedMap(key1, value1);
    }

    /**
     * Matcher that checks that the elements of a Map are in a particular order.
     *
     * @param key1   the value of the first key.
     * @param value1 the value of the first value.
     * @param key2   the value of the second key.
     * @param value2 the value of the second value.
     * @param <K>    the type of the keys.
     * @param <V>    the type of the values.
     * @return a matcher that checks that the elements of a Map are in a particular order.
     */
    public static <K, V> OrderedMapMatcher<K, V> orderedMap(K key1, V value1, K key2, V value2) {
        return OrderedMapMatcher.orderedMap(key1, value1, key2, value2);
    }

    /**
     * Matcher that checks that the elements of a Map are in a particular order.
     *
     * @param key1   the value of the first key.
     * @param value1 the value of the first value.
     * @param key2   the value of the second key.
     * @param value2 the value of the second value.
     * @param key3   the value of the third key.
     * @param value3 the value of the third value.
     * @param <K>    the type of the keys.
     * @param <V>    the type of the values.
     * @return a matcher that checks that the elements of a Map are in a particular order.
     */
    public static <K, V> OrderedMapMatcher<K, V> orderedMap(K key1, V value1, K key2, V value2, K key3, V value3) {
        return OrderedMapMatcher.orderedMap(key1, value1, key2, value2, key3, value3);
    }

    /**
     * Guava factory style map matcher for a map containing exactly 2 entries matching the arguments.
     *
     * @param key1   key of the first entry
     * @param value1 value of the first entry
     * @param key2   key of the second entry
     * @param value2 key of the second entry
     * @param <K>    key type
     * @param <V>    value type
     * @return matcher for the map equal to a map of provided keys and values
     */
    public static <K, V> Matcher<Map<K, V>> isMapOf(final K key1, final V value1, final K key2, final V value2) {
        // HAMCREST 1.2, Y U USE STUPID GENERIC BOUNDS :/
        return (Matcher<Map<K, V>>) Matchers.equalTo((Map<K, V>) ImmutableMap.of(key1, value1, key2, value2));
    }

    public static <K, V> Matcher<Map<K, V>> hasEntries(final Map<K, V> expectedMap) {
        return new TypeSafeMatcher<Map<K, V>>() {
            @Override
            protected boolean matchesSafely(final Map<K, V> item) {
                return expectedMap.entrySet()
                        .stream()
                        .allMatch(kv -> {
                            final V v = item.get(kv.getKey());
                            return v != null && v == expectedMap.get(kv.getKey());
                        });
            }

            @Override
            public void describeTo(final Description description) {
                description.appendText("Expected map ").appendValue(expectedMap);
            }
        };
    }

    /**
     * Matcher that checks that the elements of a Map are in a particular order. You can use this class to make sure the
     * elements in the list have the right order and content. For example:
     * <p>
     * <pre>
     * {@code
     *
     *      public void makeSureElementsReturnedInDescendingOrder()
     *      {
     *          //This is a bad implementation as elements in ascending order.
     *          final ImmutableSortedMap<Integer, Integer> sortedMap = ImmutableSortedMap.of(1, 1, 2, 2, 3, 3);
     *
     *          //This assertion will fail (correctly).
     *          assertThat(sortedMap, OrderedMapMatcher.orderedMap(3, 3, 2, 2, 1, 1));
     *      }
     * }
     * </pre>
     * <p>
     * Some people will try to use the code below to make the above assertion. However, <strong>it will pass as the Map
     * interface does not take order into account when doing equals.</strong>
     * <p>
     * <pre>
     * {@code
     *
     *      public void makeSureElementsReturnedInDescendingOrder()
     *      {
     *          final LinkedHashMap<Integer, Integer> linked = Maps.newLinkedHashMap();
     *          linked.put(3, 3);
     *          linked.put(2, 2);
     *          linked.put(1, 1);
     *
     *          //This is a bad implementation as elements are in ascending order.
     *          final ImmutableSortedMap<Integer, Integer> sortedMap = ImmutableSortedMap.of(1, 1, 2, 2, 3, 3);
     *
     *          //This assertion will pass (incorrectly) even tough the sortedMap is not in ascending order.
     *          assertThat(sortedMap, equalsTo(linked));
     *      }
     * }
     * </pre>
     * <p>
     * This is really only useful for Maps that
     * have a defined order like {@code LinkedHashMap}, {@code ImmutableMap} or {@code SortedMap} instances.
     *
     * @param <K> the type of the key.
     * @param <V> the type of the values.
     * @since v7.0
     */
    public static class OrderedMapMatcher<K, V> extends TypeSafeDiagnosingMatcher<Map<K, V>> {
        public static <K, V> OrderedMapMatcher<K, V> orderedMap(SortedMap<K, V> map) {
            return new OrderedMapMatcher<>(toList(map));
        }

        public static <K, V> OrderedMapMatcher<K, V> orderedMap(Iterable<Pair<K, V>> pairs) {
            return new OrderedMapMatcher<>(pairs);
        }

        public static <K, V> OrderedMapMatcher<K, V> orderedMap(K key1, V value1) {
            return orderedMap(ImmutableList.of(pair(key1, value1)));
        }

        public static <K, V> OrderedMapMatcher<K, V> orderedMap(K key1, V value1, K key2, V value2) {
            return orderedMap(ImmutableList.of(pair(key1, value1), pair(key2, value2)));
        }

        public static <K, V> OrderedMapMatcher<K, V> orderedMap(K key1, V value1, K key2, V value2, K key3, V value3) {
            return orderedMap(ImmutableList.of(pair(key1, value1), pair(key2, value2), pair(key3, value3)));
        }

        private List<Pair<K, V>> pairs;

        private OrderedMapMatcher(final Iterable<Pair<K, V>> pairs) {
            this.pairs = ImmutableList.copyOf(pairs);
        }

        @Override
        protected boolean matchesSafely(final Map<K, V> actual, final Description description) {
            final List<Pair<K, V>> actualPairs = toList(actual);
            if (actualPairs.equals(pairs)) {
                return true;
            } else {
                description.appendValue(actualPairs);
                return false;
            }
        }

        @Override
        public void describeTo(final Description description) {
            description.appendValue(pairs);
        }

        private static <K, V> List<Pair<K, V>> toList(Map<? extends K, ? extends V> map) {
            return map.entrySet().stream()
                    .map(e -> pair(e.getKey(), e.getValue()))
                    .collect(Collectors.toList());
        }
    }
}
