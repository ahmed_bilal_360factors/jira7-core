package com.atlassian.jira.matchers;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.hamcrest.TypeSafeDiagnosingMatcher;

import java.util.Iterator;
import java.util.stream.StreamSupport;

import static com.atlassian.collectors.CollectorsUtil.toImmutableList;
import static com.atlassian.collectors.CollectorsUtil.toImmutableListWithCapacity;
import static java.util.Arrays.stream;

/**
 * Match if given sequence has given elements in apropriate order there can be any numbers of elements in given sequence
 * between expected elements. Sequences sizes doesnt have to match.
 * <pre>
 * {@code
 * assertThat( [1,2,3,4,5,6], hasMatchingItemsInOrder(1,4,6) )
 * assertThat( [1,2,3,4,5,6], hasMatchingItemsInOrder(6))
 * assertThat( [1,2,3,4,5,6], hasMatchingItemsInOrder() )
 * assertThat( [], hasMatchingItemsInOrder() )
 * assertThat( [1,1,2,2,3,3], hasMatchingItemsInOrder(1,2,3)
 *
 * assertThat( [1,2,3,4,5,6], not(hasMatchingItemsInOrder(1,2,7)) )
 * assertThat( [1,2,3,4,5,6], not(hasMatchingItemsInOrder(1,1)) )
 * assertThat( [], not(hasMatchingItemsInOrder(1)) )
 * }
 * </pre>
 *
 * @since v7.0
 */
public class HasItemsInOrderMatcher<T> extends TypeSafeDiagnosingMatcher<Iterable<T>> {
    private final Iterable<Matcher<T>> expectedElements;

    HasItemsInOrderMatcher(Iterable<Matcher<T>> expectedElements) {
        this.expectedElements = expectedElements;
    }

    @Override
    protected boolean matchesSafely(final Iterable<T> items, final Description mismatchDescription) {
        boolean containsAll = true;
        Iterator<T> elementsLookup = items.iterator();
        for (Matcher<T> expectedElement : expectedElements) {
            boolean elementFound = false;
            while (elementsLookup.hasNext() && !elementFound) {
                elementFound = expectedElement.matches(elementsLookup.next());
            }
            if (!elementFound) {
                mismatchDescription.appendText("expected element ").appendDescriptionOf(expectedElement).appendText(" did not match in ").appendValueList("[", ", ", "]", items);
                containsAll = false;
                break;
            }
        }

        return containsAll;
    }

    @Override
    public void describeTo(final Description description) {
        description
                .appendText("looking for elements in order: ")
                .appendList("[", ", ", "]", expectedElements);
    }

    public static <U> Matcher<Iterable<U>> hasMatchingItemsInOrder(Iterable<Matcher<U>> expected) {
        return new HasItemsInOrderMatcher<>(expected);
    }

    @SafeVarargs
    public static <U> Matcher<Iterable<U>> hasItemsInOrder(U... expected) {
        final Iterable<Matcher<U>> itemz = stream(expected)
                .map(Matchers::equalTo)
                .collect(toImmutableListWithCapacity(expected.length));

        return hasMatchingItemsInOrder(itemz);
    }

    public static <U> Matcher<Iterable<U>> hasItemsInOrder(final Iterable<U> expected) {
        final Iterable<Matcher<U>> itemz = StreamSupport.stream(expected.spliterator(), false)
                .map(Matchers::equalTo)
                .collect(toImmutableList());

        return hasMatchingItemsInOrder(itemz);
    }
}
