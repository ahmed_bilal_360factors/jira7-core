package com.atlassian.jira.application;

import com.atlassian.application.api.ApplicationKey;

import static com.atlassian.jira.application.ApplicationRoleDefinitions.ApplicationRoleDefinition;

public class MockApplicationRoleDefinition implements ApplicationRoleDefinition {
    private final ApplicationKey key;
    private final String name;

    public MockApplicationRoleDefinition(String key, String name) {
        this.key = ApplicationKey.valueOf(key);
        this.name = name;
    }

    public MockApplicationRoleDefinition(String keyAndName) {
        this(keyAndName, keyAndName);
    }

    @Override
    public ApplicationKey key() {
        return key;
    }

    @Override
    public String name() {
        return name;
    }
}
