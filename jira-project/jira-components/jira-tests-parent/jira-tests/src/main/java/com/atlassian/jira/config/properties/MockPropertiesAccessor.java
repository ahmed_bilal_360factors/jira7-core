package com.atlassian.jira.config.properties;

import javax.annotation.Nonnull;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * @since v7.0
 */
public class MockPropertiesAccessor implements PropertiesAccessor {
    private final Map<String, String> propertyOverrides = new HashMap<>();

    @Override
    public String getProperty(@Nonnull final String key) {
        if (propertyOverrides.containsKey(key)) {
            return propertyOverrides.get(key);
        } else {
            return System.getProperty(key);
        }
    }

    @Override
    public void setProperty(@Nonnull final String key, @Nonnull final String value) {
        propertyOverrides.put(key, value);
    }

    @Override
    public void unsetProperty(@Nonnull final String key) {
        // In the mock implementation we never want to fuck with actual System properties, because we might
        // affect other unit tests that run after this.
        propertyOverrides.put(key, null);
    }

    @Override
    public Properties getProperties() {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public void setProperties(@Nonnull final Properties props) {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public Boolean getBoolean(@Nonnull final String key) {
        // Never return null to be consistent with Boolean.getBoolean()
        return Boolean.parseBoolean(getProperty(key));
    }

    @Override
    public Integer getInteger(@Nonnull final String key) {
        final String val = getProperty(key);
        return val == null ? null : Integer.parseInt(val);
    }

    @Override
    public Long getLong(@Nonnull final String key) {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public void refresh() {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public void refresh(@Nonnull final String key) {
        throw new UnsupportedOperationException("Not implemented");
    }
}
