package com.atlassian.jira.application;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.jira.bc.ServiceOutcome;
import com.atlassian.jira.bc.ServiceOutcomeImpl;
import com.atlassian.jira.util.ErrorCollection;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class MockApplicationRoleAdminService implements ApplicationRoleAdminService {
    public static final String NOT_FOUND = "Not Found";

    private Map<ApplicationKey, MockApplicationRole> roles = Maps.newHashMap();

    @Nonnull
    @Override
    public ServiceOutcome<Set<ApplicationRole>> getRoles() {
        return ServiceOutcomeImpl.<Set<ApplicationRole>>ok(Sets.<ApplicationRole>newHashSet(roles.values()));
    }

    @Nonnull
    @Override
    public ServiceOutcome<ApplicationRole> getRole(@Nonnull final ApplicationKey key) {
        final MockApplicationRole role = roles.get(key);
        if (role == null) {
            return notFound();
        } else {
            return ServiceOutcomeImpl.<ApplicationRole>ok(role);
        }
    }

    @Nonnull
    @Override
    public ServiceOutcome<ApplicationRole> setRole(@Nonnull final ApplicationRole role) {
        final MockApplicationRole oldRole = roles.get(role.getKey());
        if (oldRole == null) {
            return notFound();
        } else {
            final MockApplicationRole newRole = new MockApplicationRole(role);
            roles.put(role.getKey(), newRole);
            return ServiceOutcomeImpl.<ApplicationRole>ok(newRole);
        }
    }

    @Nonnull
    @Override
    public ServiceOutcome<Set<ApplicationRole>> setRoles(@Nonnull final Collection<ApplicationRole> rolesList) {
        for (ApplicationRole role : rolesList) {
            final MockApplicationRole oldRole = roles.get(role.getKey());
            if (oldRole == null) {
                return notFoundSet();
            }
        }

        for (ApplicationRole role : rolesList) {
            roles.put(role.getKey(), new MockApplicationRole(role));
        }

        return ServiceOutcomeImpl.ok(new HashSet<>(rolesList));
    }

    private ServiceOutcomeImpl<ApplicationRole> notFound() {
        return ServiceOutcomeImpl.error(NOT_FOUND, ErrorCollection.Reason.NOT_FOUND);
    }

    private ServiceOutcomeImpl<Set<ApplicationRole>> notFoundSet() {
        return ServiceOutcomeImpl.error(NOT_FOUND, ErrorCollection.Reason.NOT_FOUND);
    }

    public MockApplicationRole addApplicationRole(String id) {
        final MockApplicationRole role = new MockApplicationRole().key(id);
        roles.put(role.getKey(), role);
        return role;
    }
}
