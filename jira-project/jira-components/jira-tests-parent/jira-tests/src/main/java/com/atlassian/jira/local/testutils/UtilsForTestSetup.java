package com.atlassian.jira.local.testutils;

import com.atlassian.core.ofbiz.CoreFactory;
import com.atlassian.core.ofbiz.util.CoreTransactionUtil;
import com.atlassian.jira.config.database.DatabaseConfig;
import com.atlassian.jira.config.database.JdbcDatasource;
import org.ofbiz.core.entity.GenericDelegator;
import org.ofbiz.core.entity.GenericEntityException;
import org.ofbiz.core.entity.model.ModelEntity;
import org.ofbiz.core.entity.model.ModelReader;
import org.ofbiz.core.entity.model.ModelViewEntity;

import java.util.Collection;
import java.util.Collections;

public class UtilsForTestSetup {
    public static void deleteAllEntities() throws GenericEntityException {
        CoreTransactionUtil.setUseTransactions(true);
        final boolean beganTransaction = CoreTransactionUtil.begin();
        try {
            final GenericDelegator delegator = CoreFactory.getGenericDelegator();
            final ModelReader reader = delegator.getModelReader();
            final Collection<String> entityNames = reader.getEntityNames();
            for (final String entityName : entityNames) {
                final ModelEntity modelEntity = delegator.getModelReader().getModelEntity(entityName);
                // Delete only normal (non-view) entities
                if (!(modelEntity instanceof ModelViewEntity)) {
                    delegator.removeByAnd(entityName, Collections.emptyMap());
                }
            }
        } finally {
            CoreTransactionUtil.commit(beganTransaction);
        }
    }

    public static DatabaseConfig getDatabaseConfig() {
        final JdbcDatasource datasource = new JdbcDatasource("jdbc:h2:mem:h2db", "org.h2.Driver", "sa", "", 10, null, 4000L, 5000L);
        return new DatabaseConfig("h2", "PUBLIC", datasource);
    }

}
