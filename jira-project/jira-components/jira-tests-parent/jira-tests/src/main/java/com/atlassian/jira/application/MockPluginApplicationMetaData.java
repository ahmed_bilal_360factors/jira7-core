package com.atlassian.jira.application;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.application.api.ApplicationPlugin;
import com.atlassian.application.host.plugin.PluginApplicationMetaData;
import com.atlassian.fugue.Option;
import org.joda.time.DateTime;

import java.net.URI;

public class MockPluginApplicationMetaData implements PluginApplicationMetaData {
    private ApplicationKey key;
    private String name;
    private String version;

    public static MockPluginApplicationMetaData valueOf(ApplicationKey key) {
        return new MockPluginApplicationMetaData().key(key);
    }

    public MockPluginApplicationMetaData key(ApplicationKey key) {
        this.key = key;
        return this;
    }

    public MockPluginApplicationMetaData name(String name) {
        this.name = name;
        return this;
    }

    public MockPluginApplicationMetaData version(String version) {
        this.version = version;
        return this;
    }

    @Override
    public ApplicationKey getKey() {
        return key;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getVersion() {
        return version;
    }

    @Override
    public String getDescriptionKey() {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public String getUserCountKey() {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public Option<URI> getConfigurationURI() {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public Option<URI> getPostInstallURI() {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public Option<URI> getPostUpdateURI() {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public DateTime buildDate() {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public String getDefinitionModuleKey() {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public Iterable<ApplicationPlugin> getPlugins() {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public ApplicationPlugin getPrimaryPlugin() {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public String getDefaultGroup() {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public Option<URI> getProductHelpServerSpaceURI() {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public Option<URI> getProductHelpCloudSpaceURI() {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public Iterable<ApplicationPlugin> getApplicationPlugins() {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public Iterable<ApplicationPlugin> getUtilityPlugins() {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final MockPluginApplicationMetaData that = (MockPluginApplicationMetaData) o;
        return key.equals(that.key);

    }

    @Override
    public int hashCode() {
        return key.hashCode();
    }
}
