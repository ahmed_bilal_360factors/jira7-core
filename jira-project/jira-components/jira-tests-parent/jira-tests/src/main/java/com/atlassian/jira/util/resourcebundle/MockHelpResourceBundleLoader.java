//package com.atlassian.jira.util.resourcebundle;
//
//import java.util.Collections;
//import java.util.EnumMap;
//import java.util.Locale;
//import java.util.Map;
//
//import com.google.common.collect.Maps;
//
///**
// * @since v6.2.3
// */
//public class MockHelpResourceBundleLoader extends HelpResourceBundleLoader
//{
//    private final Locale locale;
//    private Mode mode;
//    private Map<Mode, Map<String, String>> result = new EnumMap<>(Mode.class);
//
//    public MockHelpResourceBundleLoader()
//    {
//        this(Locale.TAIWAN);
//    }
//
//    private MockHelpResourceBundleLoader(final Locale locale)
//    {
//        super(null);
//        this.locale = locale;
//    }
//
//    public MockHelpResourceBundleLoader helpText()
//    {
//        mode = Mode.HELP;
//        return this;
//    }
//
//    public MockHelpResourceBundleLoader i18n()
//    {
//        mode = Mode.I18N;
//        return this;
//    }
//
//    public Map<String, String>  load(final Locale locale)
//    {
//        Map<String, String> loadResult = result.get(mode);
//        if (loadResult == null)
//        {
//            loadResult = emptyResult();
//        }
//        return loadResult;
//    }
//
//    private Map<String, String> emptyResult()
//    {
//        return Collections.<String, String>emptyMap();
//    }
//
//    public MockHelpResourceBundleLoader registerHelp(Map<String, String> text)
//    {
//        return register(Mode.HELP, text);
//    }
//
//    private MockHelpResourceBundleLoader register(final Mode mode, Map<String, String> text)
//    {
//        result.put(mode, text);
//        return this;
//    }
//
//    private enum Mode
//    {
//        I18N, HELP, CORE_HELP,
//    }
//}
