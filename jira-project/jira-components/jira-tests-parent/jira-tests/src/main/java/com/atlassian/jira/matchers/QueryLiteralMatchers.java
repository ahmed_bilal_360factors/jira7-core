package com.atlassian.jira.matchers;

import com.atlassian.jira.jql.operand.QueryLiteral;
import com.google.common.collect.Lists;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.hamcrest.TypeSafeMatcher;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;

/**
 * Hamcrest matchers for {@code QueryLiteral} values and collections.
 *
 * @since v6.4
 */
@SuppressWarnings({
        // Matcher utility classes often have to do awkward things like these...
        "AnonymousInnerClassWithTooManyMethods",
        "MethodWithMultipleReturnPoints",
        "CastToConcreteClass",
        "OverlyComplexMethod",
        "ChainOfInstanceofChecks"})
public class QueryLiteralMatchers {
    private QueryLiteralMatchers() {
        // static-only
    }

    public static Matcher<Iterable<QueryLiteral>> emptyIterable() {
        return Matchers.emptyIterableOf(QueryLiteral.class);
    }

    public static Matcher<QueryLiteral> literal() {
        return new TypeSafeMatcher<QueryLiteral>() {
            @Override
            protected boolean matchesSafely(final QueryLiteral item) {
                return item.isEmpty();
            }

            @Override
            public void describeTo(final Description description) {
                description.appendText("QueryLiteral[EMPTY]");
            }
        };
    }

    public static Matcher<QueryLiteral> literal(final String expectedStringValue) {
        if (expectedStringValue == null) {
            return literal();
        }
        return new TypeSafeMatcher<QueryLiteral>() {
            @Override
            protected boolean matchesSafely(final QueryLiteral item) {
                return expectedStringValue.equals(item.getStringValue());
            }

            @Override
            public void describeTo(final Description description) {
                description.appendText("QueryLiteral[\"").appendValue(expectedStringValue).appendText("\"]");
            }
        };
    }

    public static Matcher<QueryLiteral> literal(final Long expectedLongValue) {
        if (expectedLongValue == null) {
            return literal();
        }
        return new TypeSafeMatcher<QueryLiteral>() {
            @Override
            protected boolean matchesSafely(final QueryLiteral item) {
                return expectedLongValue.equals(item.getLongValue());
            }

            @Override
            public void describeTo(final Description description) {
                description.appendText("QueryLiteral[").appendValue(expectedLongValue).appendText("]");
            }
        };
    }

    public static Matcher<? super QueryLiteral> literal(final Object expectedValue) {
        if (expectedValue == null) {
            return literal();
        }
        if (expectedValue instanceof String) {
            return literal((String) expectedValue);
        }
        if (expectedValue instanceof Long) {
            return literal((Long) expectedValue);
        }
        if (expectedValue instanceof QueryLiteral) {
            return equalTo((QueryLiteral) expectedValue);
        }
        throw new IllegalArgumentException("Cannot match instance of " + expectedValue.getClass().getName() +
                " as a QueryLiteral");
    }

    public static Matcher<Iterable<? extends QueryLiteral>> literals(Object... values) {
        return contains(Lists.newArrayList(Arrays.asList(values).stream().map(QueryLiteralMatchers::literal).iterator()));
    }

    public static Matcher<Iterable<? extends QueryLiteral>> literals(List<?> values) {
        return contains(Lists.newArrayList(values.stream().map(QueryLiteralMatchers::literal).iterator()));

    }
}
