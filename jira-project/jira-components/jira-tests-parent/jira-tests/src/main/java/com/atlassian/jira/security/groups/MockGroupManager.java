package com.atlassian.jira.security.groups;

import com.atlassian.collectors.CollectorsUtil;
import com.atlassian.crowd.embedded.api.CrowdService;
import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.crowd.embedded.impl.ImmutableGroup;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.MockCrowdService;
import com.atlassian.jira.user.MockGroup;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.SetMultimap;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.atlassian.jira.util.Page;
import com.atlassian.jira.util.PageRequest;
import com.atlassian.jira.util.Pages;

import static com.atlassian.jira.plugin.connect.ConnectAddOnProperties.CONNECT_GROUP_NAME;

/**
 * @since v4.3
 */
public class MockGroupManager implements GroupManager {
    private Map<String, Group> groupMap = new HashMap<>();
    private Map<String, Exception> groupExceptionMap = new HashMap<>();
    private SetMultimap<String, ApplicationUser> membershipMap = HashMultimap.create();
    private SetMultimap<String, Group> userToGroups = HashMultimap.create();
    private final CrowdService crowdService;

    public MockGroupManager() {
        this(new MockCrowdService());
    }

    public MockGroupManager(final CrowdService crowdService) {
        this.crowdService = crowdService;
    }

    @Override
    public Collection<Group> getAllGroups() {
        return groupMap.values();
    }

    public boolean groupExists(final String groupName) {
        return groupMap.containsKey(groupName);
    }

    @Override
    public boolean groupExists(@Nonnull final Group group) {
        return groupExists(group.getName());
    }

    @Override
    public Group createGroup(String groupName) {
        if (groupExceptionMap.containsKey(groupName)) {
            throwCheckedException(groupExceptionMap.get(groupName));
        }
        groupMap.put(groupName, new ImmutableGroup(groupName));
        return groupMap.get(groupName);
    }

    public <T extends Exception> void throwExceptionOnCreateGroup(final String groupName, final Exception cause) {
        groupExceptionMap.put(groupName, cause);
    }

    @SuppressWarnings("unchecked")
    private static <T extends Throwable> void throwCheckedException(Throwable exception) throws T {
        throw (T) exception;
    }

    public Group getGroup(final String groupName) {
        return groupMap.get(groupName);
    }

    @Override
    public Group getGroupEvenWhenUnknown(String groupName) {
        if (groupMap.containsKey(groupName)) {
            return groupMap.get(groupName);
        }
        return new ImmutableGroup(groupName);
    }

    @Override
    public Group getGroupObject(String groupName) {
        return groupMap.get(groupName);
    }

    @Override
    public boolean isUserInGroup(@Nullable final String username, @Nullable final String groupName) {
        final Set<ApplicationUser> members = membershipMap.get(groupName);
        return members != null && Iterables.any(members, user -> user.getUsername().equalsIgnoreCase(username));
    }

    @Override
    public boolean isUserInGroup(@Nullable final ApplicationUser user, @Nullable final Group group) {
        final Set<ApplicationUser> members = membershipMap.get(group.getName());
        return members != null && members.contains(user);
    }

    @Override
    public boolean isUserInGroup(@Nullable final ApplicationUser user, @Nullable final String groupName) {
        return user != null && isUserInGroup(user.getUsername(), groupName);
    }

    public Collection<ApplicationUser> getUsersInGroup(final String groupName) {
        return membershipMap.get(groupName);
    }

    @Override
    public Collection<ApplicationUser> getUsersInGroup(final String groupName, final Boolean includeInactive) {
        return getUsersInGroup(groupName).stream().filter(u -> includeInactive || u.isActive()).collect(toList());
    }

    @Override
    public Page<ApplicationUser> getUsersInGroup(final String groupName, final Boolean active, final PageRequest pageRequest) {
        final Set<ApplicationUser> applicationUsers = membershipMap.get(groupName)
                .stream()
                .filter(u -> active || u.isActive())
                .collect(Collectors.toSet());
        return Pages.toPage(applicationUsers, pageRequest);
    }

    @Override
    public Collection<ApplicationUser> getUsersInGroup(Group group) {
        return membershipMap.get(group.getName());
    }

    @Override
    public int getUsersInGroupCount(final Group group) {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public int getUsersInGroupCount(final String groupName) {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public List<String> getNamesOfDirectMembersOfGroups(Collection<String> groupNames, int limit) {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public Collection<String> filterUsersInAllGroupsDirect(Collection<String> userNames, Collection<String> groupNames) {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public Collection<String> getUserNamesInGroup(Group group) {
        return getUserNamesInGroup(group.getName());
    }

    @Override
    public Collection<String> getUserNamesInGroups(final Collection<Group> groups) {
        Collection<String> users = Lists.newArrayList();
        for (Group group : groups) {
            users.addAll(getUserNamesInGroup(group));
        }
        return ImmutableSet.copyOf(users);
    }

    @Override
    public Collection<String> getUserNamesInGroup(String groupName) {
        Collection<String> usernames = Lists.newArrayList();
        usernames.addAll(getUsersInGroup(groupName).stream()
                .map(user -> user.getName())
                .collect(toList()));
        return usernames;
    }

    @Override
    public Collection<ApplicationUser> getDirectUsersInGroup(Group group) {
        return getUsersInGroup(group);
    }

    public Collection<Group> getGroupsForUser(final String userName) {
        final Set<Group> groups = userToGroups.get(userName);
        if (groups != null) {
            return groups;
        }
        return Collections.emptySet();
    }

    public Collection<Group> getGroupsForUser(final ApplicationUser user) {
        final Set<Group> groups = userToGroups.get(user.getName());
        if (groups != null) {
            return groups;
        }
        return Collections.emptySet();
    }

    public Collection<String> getGroupNamesForUser(final String userName) {
        final Set<Group> groups = userToGroups.get(userName);
        if (groups == null) {
            return Collections.emptySet();
        }
        return groups.stream()
                .map(Group::getName)
                .sorted()
                .collect(CollectorsUtil.toImmutableListWithSizeOf(groups));
    }

    @Override
    public Collection<String> getGroupNamesForUser(@Nonnull final ApplicationUser user) {
        return getGroupNamesForUser(user.getName());
    }

    @Override
    public void addUserToGroup(ApplicationUser user, Group group) {
        groupMap.put(group.getName(), group);
        membershipMap.put(group.getName(), user);
        userToGroups.put(user.getName(), group);
    }

    @Override
    @Deprecated
    public Set<ApplicationUser> getConnectUsers() {
        return ImmutableSet.of();
    }

    public Group addGroup(String groupName) {
        final ImmutableGroup group = new ImmutableGroup(groupName);
        addGroup(group);

        return group;
    }

    public MockGroupManager addGroup(Group group) {
        groupMap.put(group.getName(), group);
        return this;
    }

    public void addMember(String groupName, String userName) {
        Group group = new MockGroup(groupName);
        ApplicationUser user = new MockApplicationUser(userName);
        addUserToGroup(user, group);
    }

    public MockGroupManager setUserMembership(final ApplicationUser user, final Group... groups) {
        for (Iterator<Map.Entry<String, ApplicationUser>> iterator = membershipMap.entries().iterator(); iterator.hasNext(); ) {
            Map.Entry<String, ApplicationUser> entry = iterator.next();
            if (entry.getValue().equals(user)) {
                iterator.remove();
            }
        }
        Arrays.asList(groups).forEach(g -> addUserToGroup(user, g));
        return this;
    }
}
