package com.atlassian.jira.application;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.crowd.embedded.api.Group;
import com.google.common.collect.Sets;
import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;

import java.util.Collections;
import java.util.Set;
import java.util.stream.Collectors;

public class ApplicationRoleDataMatcher extends TypeSafeMatcher<ApplicationRoleStore.ApplicationRoleData> {
    private Set<String> groups = Collections.emptySet();
    private Set<String> defaultGroups = Collections.emptySet();
    private ApplicationKey key;
    private boolean selectedByDefault;

    public ApplicationRoleDataMatcher match(ApplicationRoleStore.ApplicationRoleData data) {
        this.key = data.getKey();
        this.groups = data.getGroups();
        this.defaultGroups = data.getDefaultGroups();
        this.selectedByDefault = data.isSelectedByDefault();

        return this;
    }

    public ApplicationRoleDataMatcher merge(ApplicationRole role) {
        key(role.getKey());
        groups(role.getGroups().stream().map(Group::getName).collect(Collectors.toList()));
        defaultGroups(role.getDefaultGroups().stream().map(Group::getName).collect(Collectors.toList()));
        selectedByDefault(role.isSelectedByDefault());

        return this;
    }

    public ApplicationRoleDataMatcher selectedByDefault(final boolean selectedByDefault) {
        this.selectedByDefault = selectedByDefault;
        return this;
    }

    public ApplicationRoleDataMatcher key(String key) {
        this.key = ApplicationKey.valueOf(key);
        return this;
    }

    public ApplicationRoleDataMatcher key(ApplicationKey key) {
        this.key = key;
        return this;
    }

    public ApplicationRoleDataMatcher groups(Iterable<String> groups) {
        this.groups = Sets.newHashSet(groups);
        return this;
    }

    public ApplicationRoleDataMatcher groups(String... groups) {
        this.groups = Sets.newHashSet(groups);
        return this;
    }

    public ApplicationRoleDataMatcher defaultGroups(String... defaultGroups) {
        this.defaultGroups = Sets.newHashSet(defaultGroups);
        return this;
    }

    public ApplicationRoleDataMatcher defaultGroups(Iterable<String> defaultGroups) {
        this.defaultGroups = Sets.newHashSet(defaultGroups);
        ;
        return this;
    }

    @Override
    protected boolean matchesSafely(final ApplicationRoleStore.ApplicationRoleData applicationRoleData) {
        return applicationRoleData.getKey().equals(key)
                && applicationRoleData.getGroups().equals(groups)
                && applicationRoleData.getDefaultGroups().equals(defaultGroups)
                && applicationRoleData.isSelectedByDefault() == selectedByDefault;
    }

    @Override
    public void describeTo(final Description description) {
        description.appendText(String.format("[%s, %s, %s, %s]", key, groups, defaultGroups, selectedByDefault));
    }

    @Override
    protected void describeMismatchSafely(final ApplicationRoleStore.ApplicationRoleData item, final Description mismatchDescription) {
        mismatchDescription.appendText(String.format("[%s, %s, %s, %s]", item.getKey(), item.getGroups(),
                item.getDefaultGroups(), selectedByDefault));
    }
}
