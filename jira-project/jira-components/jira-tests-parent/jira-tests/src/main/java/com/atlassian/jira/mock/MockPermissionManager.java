package com.atlassian.jira.mock;

import com.atlassian.jira.exception.RemoveException;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.security.DefaultPermissionManager;
import com.atlassian.jira.security.JiraPermission;
import com.atlassian.jira.security.plugin.ProjectPermissionKey;
import com.atlassian.jira.user.ApplicationUser;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;

public class MockPermissionManager extends DefaultPermissionManager {
    protected Collection<JiraPermission> permissions;

    public boolean isDefaultPermission() {
        return defaultPermission;
    }

    public void setDefaultPermission(boolean defaultPermission) {
        this.defaultPermission = defaultPermission;
    }

    private boolean defaultPermission;

    public MockPermissionManager() {
        super(null, null);
        permissions = new HashSet<JiraPermission>(4);
    }

    /**
     * Creates a PermissionManager implementation where, by default, all permissions are given or denied based on the
     * given value.
     *
     * @param defaultPermission if true, everything is permitted, if false, everything is denied.
     */
    public MockPermissionManager(final boolean defaultPermission) {
        super(null, null);
        this.defaultPermission = defaultPermission;
    }

    @Override
    public void removeGroupPermissions(String group) throws RemoveException {
        final Iterator<JiraPermission> iterator = permissions.iterator();
        while (iterator.hasNext()) {
            if (iterator.next().getGroup().equals(group)) {
                iterator.remove();
            }
        }
    }

    @Override
    public boolean hasPermission(final int permissionsId, final ApplicationUser user) {
        return defaultPermission;
    }

    @Override
    public boolean hasPermission(final int permissionsId, final Issue issue, final ApplicationUser user) {
        return defaultPermission;
    }

    @Override
    public boolean hasPermission(final int permissionsId, final Project project, final ApplicationUser user) {
        return defaultPermission;
    }

    @Override
    public boolean hasPermission(final int permissionsId, final Project project, final ApplicationUser user, final boolean issueCreation) {
        return defaultPermission;
    }

    @Override
    public boolean hasPermission(@Nonnull final ProjectPermissionKey projectPermissionKey, @Nonnull final Project project, final ApplicationUser user, final boolean issueCreation) {
        return defaultPermission;
    }

    @Override
    public boolean hasProjects(final int permissionId, final ApplicationUser user) {
        return defaultPermission;
    }

    @Override
    public boolean hasPermission(@Nonnull final ProjectPermissionKey projectPermissionKey, @Nonnull final Project project, final ApplicationUser user) {
        return defaultPermission;
    }
}
