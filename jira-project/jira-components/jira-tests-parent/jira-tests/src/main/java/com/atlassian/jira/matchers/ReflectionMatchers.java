package com.atlassian.jira.matchers;

import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.collect.ImmutableList;
import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;

import java.lang.reflect.Field;
import java.util.Map;

/**
 * Class containing static factory methods for useful reflection-based matchers.
 */
public final class ReflectionMatchers {
    private ReflectionMatchers() {
    }

    /**
     * This matcher is similar to {@link org.hamcrest.Matchers#hasProperty} but better!
     * <p>
     * Sometimes you need to assert on a property that is deep down in the object, and then you would do:
     * <p>
     * <pre>
     *     hasProperty("a", hasProperty("b", hasProperty("c", equalTo("abc")))
     * </pre>
     * <p>
     * But with this matcher you can just write:
     * <p>
     * <pre>
     *     hasProperty("a.b.c", equalTo("abc"))
     * </pre>
     * <p>
     * It's worth noting that the two above calls are completely equivalent, i.e. this method
     * is implemented in terms of {@link org.hamcrest.Matchers#hasProperty}.
     *
     * @param chain        property to check, can be chained (e.g. "a.b.c")
     * @param valueMatcher matcher on the property
     * @param <T>          type of the thing we assert on
     * @return the matcher
     */
    public static <T> Matcher<? super T> hasProperty(String chain, Matcher<?> valueMatcher) {
        ImmutableList<String> elements = ImmutableList.copyOf(Splitter.on('.').omitEmptyStrings().trimResults().split(chain));
        if (elements.isEmpty()) {
            return (Matcher<T>) valueMatcher;
        } else {
            return Matchers.hasProperty(elements.get(0), hasProperty(Joiner.on('.').join(elements.stream().skip(1).iterator()), valueMatcher));
        }
    }

    public static <T> Matcher<? super T> propertiesMatcher(Map<String, Matcher<Object>> values) {
        return new BaseMatcher<T>() {
            @Override
            public void describeTo(Description description) {
                description.appendValue("object with properties=" + values);
            }

            @Override
            public void describeMismatch(Object item, Description mismatchDescription) {
                mismatchDescription.appendText("was ");
                mismatchDescription.appendText(item.getClass().getSimpleName()+"{");
                String sep = "";
                for (String property : values.keySet()) {
                    mismatchDescription.appendText(sep);
                    mismatchDescription.appendText(property + "=" + getFieldFromBean(item, property));
                    sep = ", ";
                }
                mismatchDescription.appendText("}");
            }

            @Override
            public boolean matches(Object item) {

                for (Map.Entry<String, Matcher<Object>> property : values.entrySet()) {
                    final Object value = getFieldFromBean(item, property.getKey());
                    if (!property.getValue().matches(value)) {
                        return false;
                    }
                }
                return true;
            }

            private Object getFieldFromBean(Object item, String fieldName) {
                try {
                    final Field declaredField = item.getClass().getDeclaredField(fieldName);
                    declaredField.setAccessible(true);
                    final Object value = declaredField.get(item);
                    return value;
                } catch (IllegalAccessException e) {
                    throw new RuntimeException(e);
                } catch (NoSuchFieldException e) {
                    throw new RuntimeException(e);
                }

            }
        };
    }


}