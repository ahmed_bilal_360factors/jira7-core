package com.atlassian.jira.application;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.extras.common.LicensePropertiesConstants;
import com.atlassian.jira.user.MockGroup;
import com.google.common.collect.Sets;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class MockApplicationRole implements ApplicationRole {
    private ApplicationKey key;
    private Set<Group> groups;
    private Set<Group> defaultGroups;
    private String name;
    private int numberOfLicenseSeats;
    private boolean selectedByDefault;
    private boolean defined;
    private boolean platform;

    public MockApplicationRole() {
        this.groups = Sets.newHashSet();
        this.defaultGroups = Sets.newHashSet();
    }

    public MockApplicationRole(ApplicationKey key) {
        this();
        this.key = key;
        this.name = key.toString();
    }

    public MockApplicationRole(final ApplicationRole copy) {
        this.key = copy.getKey();
        this.groups = Sets.newHashSet(copy.getGroups());
        this.defaultGroups = Sets.newHashSet(copy.getDefaultGroups());
        this.name = copy.getName();
        this.numberOfLicenseSeats = copy.getNumberOfSeats();
        this.selectedByDefault = copy.isSelectedByDefault();
        this.defined = copy.isDefined();
        this.platform = copy.isPlatform();
    }

    public MockApplicationRole key(String id) {
        this.key = ApplicationKey.valueOf(id);
        return this;
    }

    public MockApplicationRole key(ApplicationKey key) {
        this.key = key;
        return this;
    }

    public MockApplicationRole name(String name) {
        this.name = name;
        return this;
    }

    public MockApplicationRole groups(Group... groups) {
        this.groups = Sets.newHashSet(groups);
        return this;
    }

    public MockApplicationRole groupNames(String... groups) {
        return groups(toGroups(groups));
    }

    public MockApplicationRole groups(Iterable<Group> groups) {
        this.groups = Sets.newHashSet(groups);
        return this;
    }

    public MockApplicationRole groupNames(Iterable<String> groups) {
        return groups(toGroups(groups));
    }

    public MockApplicationRole defaultGroups(Group... group) {
        defaultGroups = Sets.newHashSet(group);
        return this;
    }

    public MockApplicationRole defaultGroupNames(String... names) {
        return defaultGroups(toGroups(names));
    }

    public MockApplicationRole defaultGroups(Iterable<Group> groups) {
        defaultGroups = Sets.newHashSet(groups);
        return this;
    }

    public MockApplicationRole defaultGroupNames(Iterable<String> names) {
        return defaultGroups(toGroups(names));
    }

    public MockApplicationRole numberOfSeats(int numberOfLicenseSeats) {
        this.numberOfLicenseSeats = numberOfLicenseSeats;
        return this;
    }

    public MockApplicationRole selectedByDefault(boolean selectedByDefault) {
        this.selectedByDefault = selectedByDefault;
        return this;
    }

    public MockApplicationRole defined(boolean defined) {
        this.defined = defined;
        return this;
    }

    public MockApplicationRole unlimitedSeats() {
        return numberOfSeats(LicensePropertiesConstants.UNLIMITED_USERS);
    }

    @Nonnull
    @Override
    public ApplicationKey getKey() {
        return key;
    }

    @Nonnull
    @Override
    public String getName() {
        return name;
    }

    @Nonnull
    @Override
    public Set<Group> getGroups() {
        return groups;
    }

    @Nonnull
    @Override
    public Set<Group> getDefaultGroups() {
        return defaultGroups;
    }

    @Nonnull
    @Override
    public ApplicationRole withGroups(@Nonnull final Iterable<Group> groups, @Nonnull final Iterable<Group> defaultGroups) {
        return new MockApplicationRole(this).groups(groups).defaultGroups(defaultGroups);
    }

    @Override
    public int getNumberOfSeats() {
        return numberOfLicenseSeats;
    }

    @Override
    public boolean isSelectedByDefault() {
        return selectedByDefault;
    }

    @Override
    public boolean isDefined() {
        return defined;
    }

    @Override
    public boolean isPlatform() {
        return platform;
    }

    @Override
    public ApplicationRole withSelectedByDefault(final boolean selectedByDefault) {
        return new MockApplicationRole(this).selectedByDefault(selectedByDefault);
    }

    public MockApplicationRole copy() {
        return new MockApplicationRole(this);
    }

    @Override
    public boolean equals(@Nullable final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final MockApplicationRole that = (MockApplicationRole) o;

        //It is intentional that we only look at id.
        return key != null ? key.equals(that.key) : that.key == null;

    }

    @Override
    public int hashCode() {
        //It is intentional that we only look at id.
        return key != null ? key.hashCode() : 0;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
                .append("key", getKey())
                .append("groups", groups)
                .append("defaultGroups", defaultGroups)
                .append("numberOfSeats", numberOfLicenseSeats)
                .append("selectedByDefault", selectedByDefault)
                .append("defined", defined)
                .append("platform", platform)
                .toString();
    }

    private static Iterable<Group> toGroups(Iterable<String> names) {
        return StreamSupport.stream(names.spliterator(), false).map(MockGroup::new).collect(Collectors.toList());
    }

    private static Iterable<Group> toGroups(String... names) {
        return Arrays.stream(names).map(MockGroup::new).collect(Collectors.toList());
    }

    private static Group toGroup(String name) {
        return new MockGroup(name);
    }
}
