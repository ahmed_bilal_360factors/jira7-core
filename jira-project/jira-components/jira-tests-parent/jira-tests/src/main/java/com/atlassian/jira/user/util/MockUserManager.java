package com.atlassian.jira.user.util;

import com.atlassian.crowd.directory.InternalDirectory;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.DirectoryType;
import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.crowd.embedded.api.OperationType;
import com.atlassian.crowd.embedded.impl.IdentifierUtils;
import com.atlassian.crowd.embedded.impl.ImmutableUser;
import com.atlassian.jira.exception.CreateException;
import com.atlassian.jira.exception.PermissionException;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.ApplicationUsers;
import com.atlassian.jira.user.DelegatingApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.MockCrowdService;
import com.atlassian.jira.user.MockUserKeyService;
import com.atlassian.jira.user.UserDetails;
import com.google.common.base.Objects;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

/**
 * Really simple mock implementation
 *
 * @since v4.1
 */
public class MockUserManager implements UserManager {
    final MockUserKeyService mockUserKeyService;
    private final Map<Long, Directory> directories = Maps.newHashMap(ImmutableMap.of(1L, new MockDirectory()));
    final Map<String, ApplicationUser> userMap = new HashMap<>();

    private boolean writableDirectory = true;
    private boolean groupWritableDirectory = true;
    private MockCrowdService crowdService;

    private boolean alwaysReturnUsers = false;
    private boolean useCrowdServiceToGetUsers = false;

    public MockUserManager() {
        this(new MockUserKeyService());
    }

    public MockUserManager(final MockUserKeyService mockUserKeyService) {
        this.mockUserKeyService = mockUserKeyService;
    }

    public MockUserManager(final MockCrowdService crowdService) {
        this();
        this.crowdService = crowdService;
    }

    public MockUserManager alwaysReturnUsers() {
        this.alwaysReturnUsers = true;
        return this;
    }

    public MockUserManager useCrowdServiceToGetUsers() {
        this.useCrowdServiceToGetUsers = true;
        return this;
    }

    public MockUserManager doNotUseCrowdServiceToGetUsers() {
        this.useCrowdServiceToGetUsers = false;
        return this;
    }

    @Override
    public Optional<ApplicationUser> getUserById(final Long id) {
        throw new UnsupportedOperationException("Not implemented"); // implement if you wish to use this method in your tests
    }

    public void setCrowdService(MockCrowdService crowdService) {
        this.crowdService = crowdService;
    }

    @Override
    public int getTotalUserCount() {
        return userMap.size();
    }

    @Override
    @Nonnull
    public Set<ApplicationUser> getAllUsers() {
        return Sets.newHashSet(getUsers());
    }

    @Override
    public ApplicationUser getUser(final @Nullable String userName) {
        return userMap.get(IdentifierUtils.toLowerCase(userName));
    }

    @Override
    public ApplicationUser getUserObject(@Nullable String userName) {
        return userMap.get(IdentifierUtils.toLowerCase(userName));
    }

    @Override
    public ApplicationUser getUserByKey(@Nullable String userKey) {
        String username = mockUserKeyService.getUsernameForKey(userKey);
        ApplicationUser user = userMap.get(username);
        if (user == null) {
            return null;
        }
        return user;
    }

    @Override
    public ApplicationUser getUserByName(@Nullable String userName) {
        if (userName == null) {
            return null;
        }
        final String lowerUserName = IdentifierUtils.toLowerCase(userName);
        ApplicationUser user = userMap.get(lowerUserName);
        if (user == null) {
            return null;
        }
        return user;
    }

    @Override
    public ApplicationUser getUserByKeyEvenWhenUnknown(@Nullable String userKey) {
        if (userKey == null) {
            return null;
        }
        ApplicationUser user = getUserByKey(userKey);
        if (user != null) {
            return user;
        }
        return new DelegatingApplicationUser(MockApplicationUser.SEQUENCE.getAndIncrement(), userKey, new ImmutableUser(-1, userKey, userKey, "?", false));
    }

    @Override
    public ApplicationUser getUserByNameEvenWhenUnknown(@Nullable String userName) {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public ApplicationUser findUserInDirectory(String userName, Long directoryId) {
        if (directoryId == 1L) {
            return userMap.get(IdentifierUtils.toLowerCase(userName));
        }
        return null;
    }

    @Override
    public ApplicationUser getUserEvenWhenUnknown(final String userName) {
        return userMap.get(userName);
    }

    @Override
    public boolean canUpdateUser(ApplicationUser user) {
        return user != null && writableDirectory;
    }

    @Override
    public boolean userCanUpdateOwnDetails(@Nonnull final ApplicationUser user) {
        return canUpdateUser(user);
    }

    @Override
    public boolean canRenameUser(ApplicationUser user) {
        return canUpdateUser(user);
    }

    @Override
    public void updateUser(ApplicationUser user) {
        userMap.put(user.getKey(), user);
    }

    @Override
    public boolean canUpdateUserPassword(ApplicationUser user) {
        return true;
    }

    @Override
    public boolean canUpdateGroupMembershipForUser(ApplicationUser user) {
        return true;
    }

    @Override
    public Set<Group> getAllGroups() {
        return Collections.emptySet();
    }

    @Override
    public Group getGroup(final @Nullable String groupName) {
        return null;
    }

    @Override
    public Group getGroupObject(@Nullable String groupName) {
        return null;
    }

    @Override
    @Nonnull
    public List<Directory> getWritableDirectories() {
        if (writableDirectory) {
            return Collections.singletonList(directories.get(1L));
        }
        return Collections.emptyList();
    }

    @Nonnull
    @Override
    public Optional<Directory> getDefaultCreateDirectory() {
        if (writableDirectory) {
            return Optional.of(directories.get(1L));
        } else {
            return Optional.empty();
        }
    }

    @Override
    public boolean hasWritableDirectory() {
        return writableDirectory;
    }

    public void setWritableDirectory(boolean writableDirectory) {
        this.writableDirectory = writableDirectory;
    }

    @Override
    public boolean hasPasswordWritableDirectory() {
        return true;
    }

    @Override
    public boolean hasGroupWritableDirectory() {
        return groupWritableDirectory;
    }

    public void setGroupWritableDirectory(boolean groupWritableDirectory) {
        this.groupWritableDirectory = groupWritableDirectory;
    }

    @Override
    public boolean canDirectoryUpdateUserPassword(Directory directory) {
        return true;
    }

    @Override
    public Directory getDirectory(Long directoryId) {
        return directories.getOrDefault(directoryId, null);
    }

    @Override
    public boolean isUserExisting(@Nullable ApplicationUser user) {
        return user != null && user.getDirectoryId() != -1;
    }

    @Nonnull
    @Override
    public String generateRandomPassword() {
        return "secret01";
    }

    @Override
    @Nonnull
    public Collection<ApplicationUser> getUsers() {
        if (useCrowdServiceToGetUsers) {
            return ImmutableSet.copyOf(ApplicationUsers.from(crowdService.getAllUsers()));
        }
        return userMap.values();
    }

    @Override
    @Nonnull
    public Collection<ApplicationUser> getAllApplicationUsers() {
        return getAllUsers();
    }

    @Override
    public Collection<com.atlassian.crowd.embedded.api.Group> getGroups() {
        return Collections.emptySet();
    }

    public ApplicationUser addUser(ApplicationUser user) {
        final String username = IdentifierUtils.toLowerCase(user.getUsername());
        userMap.put(username, user);
        mockUserKeyService.setMapping(user.getKey(), username);
        return user;
    }

    public MockUserKeyService getMockUserKeyService() {
        return mockUserKeyService;
    }

    @Nonnull
    @Override
    public UserState getUserState(@Nullable final ApplicationUser user) {
        if (user == null) {
            return UserState.INVALID_USER;
        }
        return getUserState(user.getUsername(), user.getDirectoryId());
    }

    @Nonnull
    @Override
    public ApplicationUser createUser(@Nonnull final UserDetails userData)
            throws CreateException, PermissionException {
        ApplicationUser user = new MockApplicationUser(userData.getUsername(), userData.getDisplayName(), userData.getEmailAddress());
        addUser(user);

        if (!userData.getDirectoryId().isPresent() && crowdService != null) {
            String password = Strings.isNullOrEmpty(userData.getPassword()) ? "foo" : userData.getPassword();
            crowdService.addUser(user.getDirectoryUser(), password);
        }

        return user;
    }

    @Override
    public Optional<UserIdentity> getUserIdentityById(final Long id) {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public Optional<UserIdentity> getUserIdentityByKey(final String key) {
        return getUserIdentityByUsername(Objects.firstNonNull(mockUserKeyService.getUsernameForKey(key), key));
    }

    @Override
    public Optional<UserIdentity> getUserIdentityByUsername(final String username) {
        if (alwaysReturnUsers) {
            return Optional.of(UserIdentity.withId(MockApplicationUser.SEQUENCE.getAndIncrement()).key(username).andUsername(username));
        } else {
            ApplicationUser appUser = userMap.get(username);
            return appUser != null ?
                    Optional.of(UserIdentity.withId(appUser.getId()).key(appUser.getKey()).andUsername(appUser.getUsername())) :
                    Optional.empty();
        }
    }

    @Nonnull
    @Override
    public UserState getUserState(@Nonnull final String username, final long directoryId) {
        if (findUserInDirectory(username, directoryId) != null) {
            return UserState.NORMAL_USER;
        }
        return UserState.INVALID_USER;
    }

    public void putDirectory(final Long directoryId, final Directory directory) {
        directories.put(directoryId, directory);
    }

    class MockDirectory implements Directory {
        private final Date CREATED = new Date();

        @Override
        public Long getId() {
            return 1L;
        }

        @Override
        public String getName() {
            return "Mock Internal Directory";
        }

        @Override
        public boolean isActive() {
            return true;
        }

        @Override
        public String getEncryptionType() {
            return "plaintext";
        }

        @Override
        public Map<String, String> getAttributes() {
            return Collections.emptyMap();
        }

        @Override
        public Set<OperationType> getAllowedOperations() {
            if (writableDirectory) {
                return EnumSet.allOf(OperationType.class);
            }
            return EnumSet.noneOf(OperationType.class);
        }

        @Override
        public String getDescription() {
            return "Mock internal directory for unit tests";
        }

        @Override
        public DirectoryType getType() {
            return DirectoryType.INTERNAL;
        }

        @Override
        public String getImplementationClass() {
            return InternalDirectory.class.getName();
        }

        @Override
        public Date getCreatedDate() {
            return CREATED;
        }

        @Override
        public Date getUpdatedDate() {
            return CREATED;
        }

        @Override
        public Set<String> getValues(final String s) {
            throw new UnsupportedOperationException("Not implemented");
        }

        @Override
        public String getValue(final String s) {
            throw new UnsupportedOperationException("Not implemented");
        }

        @Override
        public Set<String> getKeys() {
            return userMap.keySet();
        }

        @Override
        public boolean isEmpty() {
            throw new UnsupportedOperationException("Not implemented");
        }
    }
}
