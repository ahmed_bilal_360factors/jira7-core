package com.atlassian.jira.database;

import com.atlassian.jira.util.DuckTypeProxyFactory;
import com.google.common.collect.Sets;
import org.apache.commons.lang3.StringEscapeUtils;

import javax.annotation.Nonnull;
import java.sql.PreparedStatement;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Supplier;

import static java.util.Objects.requireNonNull;
import static org.apache.commons.lang3.StringEscapeUtils.escapeJava;

/**
 * Mocks out some of PreparedStatement for use as a Duck Type proxy.
 *
 * @since v6.4
 */
public class DuckTypeConnection {
    private Map<String, Iterable<ResultRow>> queryResults = new ConcurrentHashMap<>();
    private Map<String, Supplier<Integer>> updateResults = new ConcurrentHashMap<>();
    private Map<String, Runnable> sqlQueryActions = new ConcurrentHashMap<>();
    private Set<String> queriesRun = Sets.newSetFromMap(new ConcurrentHashMap<>());
    private Optional<Iterable<ResultRow>> defaultQueryResult = Optional.empty();
    private Optional<Integer> defaultUpdateResult = Optional.empty();

    // -----------------------------------------------------------------------------------------------------------------
    // Implemented Methods
    // -----------------------------------------------------------------------------------------------------------------

    public void reset() {
        queryResults.clear();
        updateResults.clear();
        sqlQueryActions.clear();
        queriesRun.clear();
    }

    public PreparedStatement prepareStatement(final String sql) {
        return DuckTypeProxyFactory.newStrictProxyInstance(PreparedStatement.class, new DuckTypePreparedStatement(this, sql));
    }

    public void setAutoCommit(boolean autoCommit) {
        // ignored
    }

    public void commit() {
        // OK
    }

    // -----------------------------------------------------------------------------------------------------------------
    // Housekeeping Methods for setting up and verifying expectations
    // -----------------------------------------------------------------------------------------------------------------

    public void setQueryResults(String sql, Iterable<ResultRow> expectedResults) {
        queryResults.put(sql, expectedResults);
    }

    @Nonnull
    public Iterable<ResultRow> getQueryResults(final String sql) {
        final Iterable<ResultRow> resultRows = queryResults.get(sql);
        if (resultRows == null) {
            // As part of our test asserts by default we strictly only allow SQL that has been registered as expected.
            // This also makes it easy to write tests:
            //  - let the test fail, review the SQL from the error message, and copy/paste
            return defaultQueryResult.orElseThrow(() -> new AssertionError("Unexpected DB call: \n\n" + sql + "\n"));
        }

        runSqlQueryAction(sql);
        return resultRows;
    }

    /**
     * Sets the expected results for a SQL statement.
     *
     * @param sql the expected SQL statement
     * @param rowCount what to return for the number of affected rows
     */
    public void setUpdateResults(String sql, int rowCount) {
        updateResults.put(sql, () -> rowCount);
    }

    /**
     * Sets an expected SQL statement to throw an exception.
     *
     * @param sql the expected SQL statement
     * @param exFactory a factory that supplies the runtime exception to be thrown for that SQL
     */
    public void setUpdateResults(String sql, Supplier<RuntimeException> exFactory) {
        updateResults.put(sql, () -> {
            throw requireNonNull(exFactory.get(), "exFactory.get()");
        });
    }

    public int getUpdateResults(final String sql) {
        Supplier<Integer> rowCount = updateResults.get(sql);
        if (rowCount == null) {
            // for now, always be strict
            return defaultUpdateResult.orElseThrow(() -> new AssertionError("Unexpected DB call: \n\n" + sql + "\n"));
        }

        runSqlQueryAction(sql);
        return rowCount.get();
    }

    public void setDefaultQueryResult(Iterable<ResultRow> defaultQueryResult) {
        this.defaultQueryResult = Optional.of(defaultQueryResult);
    }

    public void setDefaultUpdateResult(int defaultUpdateResult) {
        this.defaultUpdateResult = Optional.of(defaultUpdateResult);
    }

    public void assertAllExpectedStatementsWereRun() {
        try {
            assertStatementsRun(queryResults.keySet());
            assertStatementsRun(updateResults.keySet());
        } finally {
            queryResults.clear();
            updateResults.clear();
            queriesRun.clear();
        }
    }

    private void assertStatementsRun(final Set<String> expectedStatements) {
        for (String expectedStatement : expectedStatements) {
            if (!queriesRun.contains(expectedStatement)) {
                throw new AssertionError("The following SQL was expected to run but was not: \n\n" + expectedStatement + "\n");
            }
        }
    }

    public void onSqlListener(String sql, Runnable action) {
        sqlQueryActions.put(sql, action);
    }

    protected void runSqlQueryAction(String sql) {
        queriesRun.add(sql);
        final Runnable callback = sqlQueryActions.get(sql);
        if (callback != null) {
            callback.run();
        }
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder(4096).append("DuckTypeConnection {{{");
        appendResults(sb, "queryResults", queryResults);
        appendResults(sb, "updateResults", updateResults);
        if (!queriesRun.isEmpty()) {
            sb.append("\n\tqueriesRun:\n");
            queriesRun.forEach(query -> {
                appendQuery(sb, query);
                sb.append('\n');
            });
        }
        return sb.append("}}}").toString();
    }

    private static void appendResults(StringBuilder sb, String name, Map<String, ?> results) {
        if (results.isEmpty()) {
            return;
        }

        sb.append("\n\t").append(name).append(":\n");
        results.forEach((query, result) -> {
            appendQuery(sb, query);
            sb.append(" => ").append(result).append('\n');
        });
    }

    private static void appendQuery(StringBuilder sb, String query) {
        sb.append("\t\t\"").append(escapeJava(query)).append('"');
    }
}
