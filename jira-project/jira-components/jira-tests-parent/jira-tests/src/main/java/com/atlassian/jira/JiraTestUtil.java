package com.atlassian.jira;

import com.atlassian.core.ofbiz.CoreFactory;
import com.atlassian.core.ofbiz.test.mock.MockSequenceUtil;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.seraph.auth.DefaultAuthenticator;
import com.mockobjects.servlet.MockHttpServletRequest;
import com.mockobjects.servlet.MockHttpServletResponse;
import org.ofbiz.core.entity.model.ModelEntity;
import webwork.action.ActionContext;
import webwork.action.ServletActionContext;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

import static org.mockito.Mockito.mock;

public class JiraTestUtil {

    public static final String TESTS_BASE = "com/atlassian/jira";

    /**
     * This method will login a user to the current webwork action context.
     *
     * @param user The user to login, or null if you want to behave anonymously.
     */
    public static void loginUser(ApplicationUser user) {
        loginUserImpl(user);
    }

    private static void loginUserImpl(ApplicationUser user) {
        final Map session = ActionContext.getSession();
        session.put(DefaultAuthenticator.LOGGED_IN_KEY, user);
        session.put(DefaultAuthenticator.LOGGED_OUT_KEY, null);
        ComponentAccessor.getComponent(JiraAuthenticationContext.class).setLoggedInUser(user);
    }

    /**
     * This method will logout a user to the current webwork action context.
     *
     * @param user The user to logout
     */
    public static void logoutUser(ApplicationUser user) {
        logoutUserImpl(user);
    }


    private static void logoutUserImpl(ApplicationUser user) {
        ActionContext.getSession().put(DefaultAuthenticator.LOGGED_OUT_KEY, user);
        ComponentAccessor.getComponent(JiraAuthenticationContext.class).clearLoggedInUser();
    }

    public static void resetRequestAndResponse() {
        ServletActionContext.setRequest(null);
        ServletActionContext.setResponse(null);
    }

    /**
     * @param url expected url
     * @return mock response
     * @throws IOException when error occurs
     * @deprecated use #mockExpectedRedirect(String)
     */
    @Deprecated
    public static MockHttpServletResponse setupExpectedRedirect(final String url) throws IOException {
        MockHttpServletResponse response = new MockHttpServletResponse();
        response.setExpectedRedirect(url);
        ServletActionContext.setResponse(response);
        return response;
    }

    public static HttpServletResponse mockExpectedRedirect(final String url) {
        HttpServletResponse response = mock(HttpServletResponse.class);
        ServletActionContext.setResponse(response);
        return response;
    }

//    protected void sendInternalRedirect(String redirectLocation) throws ServletException, IOException {
//        final HttpServletRequest request = ServletActionContext.getRequest();
//        request.getRequestDispatcher(redirectLocation).forward(request, ServletActionContext.getResponse());
//    }

    public static MockHttpServletResponse setupExpectedInternalRedirect(final String url) throws IOException {
        final MockHttpServletRequest request = new MockHttpServletRequest();
        ServletActionContext.setRequest(request);

        final MockHttpServletResponse response = new MockHttpServletResponse();
        response.setExpectedRedirect(url);
        ServletActionContext.setResponse(response);

        return response;
    }

    public static void setupMockSequenceUtil() {
        String helperName = CoreFactory.getGenericDelegator().getEntityHelperName("SequenceValueItem");
        ModelEntity seqEntity = CoreFactory.getGenericDelegator().getModelEntity("SequenceValueItem");
        CoreFactory.getGenericDelegator().setSequencer(new MockSequenceUtil(helperName, seqEntity, "seqName", "seqId"));
    }

    public static MockHttpServletRequest setupGetContextPath(String contextPath) {
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.setupGetContextPath(contextPath);
        ServletActionContext.setRequest(request);
        return request;
    }

}
