package com.atlassian.jira.help;

import com.atlassian.fugue.Option;

import javax.annotation.Nonnull;

public class MockHelpUrlsParserBuilder implements HelpUrlsParserBuilderFactory.HelpUrlsParserBuilder {

    private final MockHelpUrlsParser parser;

    public MockHelpUrlsParserBuilder(final MockHelpUrlsParser parser) {
        this.parser = parser;
    }

    @Nonnull
    @Override
    public HelpUrlsParserBuilderFactory.HelpUrlsParserBuilder defaultUrl(@Nonnull final String url, final String title) {
        parser.defaultUrl(url, title);
        return this;
    }

    @Nonnull
    @Override
    public HelpUrlsParserBuilderFactory.HelpUrlsParserBuilder applicationHelpSpace(@Nonnull final Option<String> applicationHelpSpace) {
        parser.setApplicationHelpSpace(applicationHelpSpace);
        return this;
    }

    @Nonnull
    @Override
    public HelpUrlsParser build() {
        return parser;
    }
}
