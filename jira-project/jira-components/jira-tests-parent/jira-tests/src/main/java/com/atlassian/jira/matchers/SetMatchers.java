package com.atlassian.jira.matchers;

import com.google.common.collect.Sets;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeDiagnosingMatcher;

import java.util.Set;

/**
 * @since v7.0
 */
public final class SetMatchers {
    private SetMatchers() {
    }

    public static <T> Matcher<Set<T>> subsetOf(Set<T> superSet) {
        return new TypeSafeDiagnosingMatcher<Set<T>>() {
            @Override
            protected boolean matchesSafely(final Set<T> item, final Description mismatchDescription) {
                if (superSet.containsAll(item)) {
                    return true;
                } else {
                    final Set<T> badItems = Sets.difference(item, superSet);
                    mismatchDescription.appendValue(String.format("but contained extra items %s.", badItems));
                    return false;
                }
            }

            @Override
            public void describeTo(final Description description) {
                description.appendValue(String.format("Subset of %s", superSet));
            }
        };
    }
}
