package com.atlassian.jira.application;

import com.atlassian.application.api.ApplicationAccess;
import com.atlassian.application.api.ApplicationKey;
import com.atlassian.fugue.Option;
import com.atlassian.sal.api.user.UserKey;
import org.joda.time.DateTime;

import javax.annotation.Nullable;
import java.net.URI;

/**
 * @since v7.0
 */
public class MockApplicationAccess implements ApplicationAccess {
    private ApplicationKey key;
    private DateTime buildDate;

    public MockApplicationAccess(ApplicationKey key, DateTime buildDate) {
        this.key = key;
        this.buildDate = buildDate;
    }

    @Override
    public ApplicationKey getApplicationKey() {
        return key;
    }

    @Override
    public Option<Integer> getMaximumUserCount() {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public int getActiveUserCount() {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public boolean canUserAccessApplication(@Nullable final UserKey userKey) {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public Option<AccessError> getAccessError(@Nullable final UserKey userKey) {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public URI getManagementPage() {
        throw new UnsupportedOperationException("Not implemented");
    }

    public DateTime buildDate() {
        return buildDate;
    }
}
