package com.atlassian.jira.matchers;

import com.google.common.collect.ImmutableList;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeDiagnosingMatcher;

import java.util.Collections;
import java.util.List;

/**
 * Match if given sequence has given subsequence in itself. Sequences sizes doesnt have to match.
 * <pre>{@code
 * assertThat( [1,2,3,4,5,6], hasSubsequenceOf([2,3,4]) )
 * assertThat( [1,2,3,4,5,6], hasSubsequenceOf([6]) )
 * assertThat( [1,2,3,4,5,6], hasSubsequenceOf([])
 * assertThat( [1,2,1,2,3,3], hasSubsequenceOf([1,2,3])
 * <p>
 * assertThat( [1,2,3,4,5,6], not(hasSubsequenceOf([1,2,6])) )
 * assertThat( [1,2,3,4,5,6], not(hasSubsequenceOf([1,1])) )
 * }
 * </pre>
 *
 * @since v7.0
 */
public class HasSubsequenceMatcher<T> extends TypeSafeDiagnosingMatcher<List<T>> {
    private final List<T> expectedElements;

    HasSubsequenceMatcher(List<T> expectedElements) {
        this.expectedElements = expectedElements;
    }


    @Override
    protected boolean matchesSafely(final List<T> items, final Description mismatchDescription) {
        final boolean hasSubsequence = Collections.indexOfSubList(items, expectedElements) != -1;
        if (!hasSubsequence) {
            mismatchDescription.appendText("input was ").appendValueList("[", ",", "]", items);
        }
        return hasSubsequence;
    }

    @Override
    public void describeTo(final Description description) {
        description
                .appendText("looking for subsequence: ")
                .appendValueList("[", ", ", "]", expectedElements);
    }

    public static <U> Matcher<List<U>> hasSubsequence(List<U> items) {
        return new HasSubsequenceMatcher<>(items);
    }

    @SafeVarargs
    public static <U> Matcher<List<U>> hasSubsequenceOf(U... items) {
        return hasSubsequence(ImmutableList.copyOf(items));
    }

}
