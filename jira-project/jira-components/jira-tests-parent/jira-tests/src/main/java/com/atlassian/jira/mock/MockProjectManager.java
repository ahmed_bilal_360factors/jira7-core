package com.atlassian.jira.mock;

import com.atlassian.jira.bc.project.ProjectCreationData;
import com.atlassian.jira.exception.DataAccessException;
import com.atlassian.jira.exception.RemoveException;
import com.atlassian.jira.project.AbstractProjectManager;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectCategory;
import com.atlassian.jira.project.ProjectCategoryImpl;
import com.atlassian.jira.project.UpdateProjectParameters;
import com.atlassian.jira.project.type.ProjectTypeKey;
import com.atlassian.jira.task.context.Context;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import org.ofbiz.core.entity.GenericValue;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.atomic.AtomicInteger;

import static com.atlassian.jira.util.dbc.Assertions.notNull;

public class MockProjectManager extends AbstractProjectManager {
    private final Map<Long, Project> projects = new HashMap<>();
    private final Map<Long, ProjectCategory> projectCategories = new HashMap<>();
    private final AtomicInteger nextId = new AtomicInteger();
    private long projectCategoryCounter = 100;

    public MockProjectManager() {
        super(null, null);
    }

    public void addProject(Project project) {
        projects.put(project.getId(), project);
    }

    public void addProject(GenericValue gv) {
        final Long id = gv.getLong("id");
        projects.put(id, new MockProject(id, gv.getString("key"), gv.getString("name"), gv));
    }

    public void removeAllProjects() {
        projects.clear();
    }

    @Override
    public Project createProject(@Nonnull ApplicationUser user, @Nonnull ProjectCreationData projectCreationData) {
        MockProject project = new MockProject(
                nextId.addAndGet(1),
                projectCreationData.getKey(),
                projectCreationData.getName()
        );
        project.setAssigneeType(projectCreationData.getAssigneeType());
        return project;
    }

    @Override
    public Project updateProject(final Project updatedProject, final String name, final String description,
                                 final String leadKey, final String url, final Long assigneeType, final Long avatarId, final String projectKey) {
        return updateProject(UpdateProjectParameters.forProject(updatedProject.getId())
                .name(name)
                .description(description)
                .leadUsername(leadKey)
                .url(url)
                .assigneeType(assigneeType)
                .avatarId(avatarId)
                .key(projectKey));
    }

    @Override
    public Project updateProject(UpdateProjectParameters parameters) {
        final Project project = notNull(getProjectObj(parameters.getProjectId()));

        MockProject newProject = new MockProject(parameters.getProjectId(), project.getKey(), project.getName());
        if (parameters.getName().isDefined()) {
            newProject.setName(parameters.getName().get());
        }

        if (parameters.getDescription().isDefined()) {
            newProject.setDescription(parameters.getDescription().get());
        }

        if (parameters.getLeadUsername().isDefined()) {
            newProject.setLead(new MockApplicationUser(parameters.getLeadUsername().get()));
        }

        if (parameters.getUrl().isDefined()) {
            newProject.setUrl(parameters.getUrl().get());
        }

        if (parameters.getAssigneeType().isDefined()) {
            newProject.setAssigneeType(parameters.getAssigneeType().get());
        }

        if (parameters.getProjectTypeKey().isDefined()) {
            newProject.setProjectTypeKey(parameters.getProjectTypeKey().get());
        }
        projects.put(newProject.getId(), newProject);
        return newProject;
    }

    @Override
    public void removeProjectIssues(final Project project) throws RemoveException {
        throw new UnsupportedOperationException();
    }

    @Override
    public void removeProjectIssues(Project project, Context taskContext) throws RemoveException {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public void removeProject(final Project project) {
        throw new UnsupportedOperationException();
    }

    /**
     * Returns project object or null if project with that id doesn't exist.
     *
     * @param id project id
     * @return project object or null if project with that id doesn't exist
     */
    @Override
    public Project getProjectObj(Long id) {
        return projects.get(id);
    }

    @Override
    public Project getProjectObjByKey(String projectKey) {
        for (Project project : getProjectObjects()) {
            if (projectKey.equals(project.getKey())) {
                return project;
            }
        }
        return null;
    }

    @Override
    public Project getProjectByCurrentKeyIgnoreCase(String projectKey) {
        for (Project project : getProjectObjects()) {
            if (projectKey.equalsIgnoreCase(project.getKey())) {
                return project;
            }
        }
        return null;
    }

    @Override
    public Project getProjectObjByKeyIgnoreCase(final String projectKey) {
        return getProjectByCurrentKeyIgnoreCase(projectKey);
    }

    @Override
    public Set<String> getAllProjectKeys(Long projectId) {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public Project getProjectObjByName(String projectName) {
        for (Project project : getProjectObjects()) {
            if (projectName.equals(project.getName())) {
                return project;
            }
        }
        return null;
    }

    @Nonnull
    @Override
    public List<Project> getProjects() {
        return new ArrayList<>(projects.values());
    }

    @Nonnull
    @Override
    public List<Project> getProjectObjects() {
        return getProjects();
    }

    @Override
    public long getProjectCount() throws DataAccessException {
        return projects.size();
    }

    @Override
    public Collection<ProjectCategory> getAllProjectCategories() {
        return projectCategories.values();
    }

    @Override
    public ProjectCategory getProjectCategory(Long id) {
        throw new UnsupportedOperationException();
    }

    @Override
    @Nullable
    public ProjectCategory getProjectCategoryObject(Long id) throws DataAccessException {
        throw new UnsupportedOperationException();
    }

    @Override
    public void updateProjectCategory(ProjectCategory projectCategory) throws DataAccessException {
        throw new UnsupportedOperationException();
    }

    @Override
    public Collection<Project> getProjectsFromProjectCategory(ProjectCategory projectCategory)
            throws DataAccessException {
        throw new UnsupportedOperationException();
    }

    @Override
    public Collection<Project> getProjectObjectsFromProjectCategory(final Long projectCategoryId) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Collection<Project> getProjectObjectsWithNoCategory() {
        throw new UnsupportedOperationException();
    }

    @Override
    public ProjectCategory getProjectCategoryForProject(Project project) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void setProjectCategory(Project project, ProjectCategory category) {
        throw new UnsupportedOperationException();
    }

    @Override
    public ProjectCategory createProjectCategory(String name, String description) {
        ProjectCategory projectCategory = new ProjectCategoryImpl(++projectCategoryCounter, name, description);
        projectCategories.put(projectCategory.getId(), projectCategory);
        return projectCategory;
    }

    @Override
    public void removeProjectCategory(Long id) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean isProjectCategoryUnique(final String projectCategory) {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public List<Project> getProjectsLeadBy(ApplicationUser leadUser) {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public long getNextId(Project project) throws DataAccessException {
        throw new UnsupportedOperationException("not implemented");
    }

    @Override
    public long getCurrentCounterForProject(Long id) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void setCurrentCounterForProject(Project project, long counter) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Project updateProjectType(ApplicationUser user, Project project, ProjectTypeKey newProjectType) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void refresh() {
        throw new UnsupportedOperationException();
    }
}
