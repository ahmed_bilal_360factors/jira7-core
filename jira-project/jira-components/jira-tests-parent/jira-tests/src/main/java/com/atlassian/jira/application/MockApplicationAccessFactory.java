package com.atlassian.jira.application;

import com.atlassian.application.api.ApplicationAccess;
import com.atlassian.application.api.ApplicationKey;
import com.atlassian.application.host.ApplicationAccessFactory;
import org.joda.time.DateTime;

/**
 * @since v7.0
 */
public class MockApplicationAccessFactory implements ApplicationAccessFactory {
    @Override
    public ApplicationAccess access(final ApplicationKey key, final DateTime buildDate) {
        return new MockApplicationAccess(key, buildDate);
    }
}
