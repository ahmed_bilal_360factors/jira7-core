package com.atlassian.application.host.plugin;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.fugue.Iterables;
import com.atlassian.fugue.Option;
import com.google.common.base.Predicate;
import com.google.common.collect.Sets;

import javax.annotation.Nullable;
import java.util.Set;

public class MockPluginApplicationMetaDataManager implements PluginApplicationMetaDataManager {
    private Set<PluginApplicationMetaData> metaDataList = Sets.newHashSet();

    @Override
    public Iterable<PluginApplicationMetaData> getApplications() {
        return metaDataList;
    }

    @Override
    public Option<PluginApplicationMetaData> getApplication(final ApplicationKey key) {
        return Iterables.findFirst(getApplications(), new Predicate<PluginApplicationMetaData>() {
            @Override
            public boolean apply(@Nullable final PluginApplicationMetaData input) {
                return input != null && input.getKey().equals(key);
            }
        });
    }

    public MockPluginApplicationMetaDataManager addMetaData(PluginApplicationMetaData metaData) {
        metaDataList.add(metaData);

        return this;
    }
}
