package com.atlassian.jira.database;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Helps you build a list of ResultRow objects for use in MockDbConnectionManager.
 *
 * @see com.atlassian.jira.database.MockDbConnectionManager#setQueryResults
 * @since v6.4.4
 */
public class ResultRowBuilder implements Iterable<ResultRow> {
    private final List<ResultRow> rows = new ArrayList<ResultRow>();

    /**
     * Add the given row to the results.
     * <p>
     * It will usually be more convenient to use {@link #addRow(Object...)}.
     * </p>
     *
     * @param row the row to add to the results
     * @return this builder
     */
    public ResultRowBuilder add(@Nonnull ResultRow row) {
        rows.add(row);
        return this;
    }

    /**
     * Add a row with the given list of column values.
     * <p>
     * Only a partial list of values is required - missing values are treated as NULL
     *
     * @param values a (partial) list of column values
     * @return this builder
     */
    public ResultRowBuilder addRow(Object... values) {
        return add(new ResultRow(values));
    }

    public List<ResultRow> toList() {
        return rows;
    }

    @Override
    public Iterator<ResultRow> iterator() {
        return rows.iterator();
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder(1024).append("ResultRowBuilder: [").append(rows.size()).append(']');
        if (!rows.isEmpty()) {
            rows.forEach(row -> sb.append("\n\t\t\t").append(row));
            sb.append('\n');
        }
        return sb.toString();
    }
}
