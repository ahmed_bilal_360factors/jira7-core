package com.atlassian.jira.util;

import com.google.common.base.Function;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.net.URI;

public class MockBaseUrl implements BaseUrl {
    private String currentBaseUrl;
    private final String staticBaseUrl;

    public MockBaseUrl(final String baseUrl, final String staticBaseUrl) {
        this.staticBaseUrl = staticBaseUrl;
        this.currentBaseUrl = baseUrl;
    }

    @Nonnull
    @Override
    public String getBaseUrl() {
        return currentBaseUrl;
    }

    @Nonnull
    @Override
    public String getCanonicalBaseUrl() {
        return currentBaseUrl;
    }

    @Override
    public URI getBaseUri() {
        return URI.create(currentBaseUrl);
    }

    @Nullable
    @Override
    public <I, O> O runWithStaticBaseUrl(@Nullable final I input,
                                         @Nonnull final Function<I, O> runnable) {
        String lastUrl = currentBaseUrl;
        currentBaseUrl = staticBaseUrl;
        try {
            return runnable.apply(input);
        } finally {
            currentBaseUrl = lastUrl;
        }
    }
}