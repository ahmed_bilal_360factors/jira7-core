package com.atlassian.jira.bc.user;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.jira.exception.AddException;
import com.atlassian.jira.exception.CreateException;
import com.atlassian.jira.exception.PermissionException;
import com.atlassian.jira.exception.RemoveException;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.UserDetails;
import com.atlassian.jira.user.util.MockUserManager;
import com.atlassian.jira.user.util.UserManager;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;


public class MockUserService implements UserService {
    private UserManager userManager;

    public MockUserService() {
        this.userManager = new MockUserManager();
    }

    public MockUserService(UserManager userManager) {
        this.userManager = userManager;
    }

    @Override
    public CreateUserValidationResult validateCreateUser(final CreateUserRequest request) {
        CreateUserValidationResult validation = new CreateUserValidationResult(request.getUsername(),
                request.getPassword(), request.getEmailAddress(), request.getDisplayName(), request.getDirectoryId(),
                null);
        return validation;
    }

    @Override
    public ApplicationUser createUser(final CreateUserValidationResult result)
            throws PermissionException, CreateException {
        UserDetails details = new UserDetails(result.getUsername(), result.getFullname())
                .withDirectory(result.getDirectoryId())
                .withEmail(result.getEmail())
                .withPassword(result.getPassword());

        final ApplicationUser user = userManager.createUser(details);
        return user;
    }

    @Nonnull
    @Override
    public ApplicationUserBuilder newUserBuilder(@Nonnull final ApplicationUser user) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public CreateUserValidationResult validateCreateUserForSignup(final ApplicationUser loggedInUser, final String username, final String password, final String confirmPassword, final String email, final String fullname) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public CreateUserValidationResult validateCreateUserForSetup(final ApplicationUser loggedInUser, final String username, final String password, final String confirmPassword, final String email, final String fullname) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public CreateUserValidationResult validateCreateUserForSignupOrSetup(final ApplicationUser user, final String username, final String password, final String confirmPassword, final String email, final String fullname) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public CreateUserValidationResult validateCreateUserForAdmin(final ApplicationUser loggedInUser, final String username, final String password, final String confirmPassword, final String email, final String fullname) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public CreateUserValidationResult validateCreateUserForAdmin(final ApplicationUser loggedInUser, final String username, final String password, final String confirmPassword, final String email, final String fullname, @Nullable final Long directoryId) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public CreateUsernameValidationResult validateCreateUsername(final ApplicationUser loggedInUser, final String username) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public CreateUsernameValidationResult validateCreateUsername(final ApplicationUser loggedInUser, final String username, final Long directoryId) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public ApplicationUser createUserFromSignup(final CreateUserValidationResult result)
            throws PermissionException, CreateException {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public ApplicationUser createUserWithNotification(final CreateUserValidationResult result)
            throws PermissionException, CreateException {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public ApplicationUser createUserNoNotification(final CreateUserValidationResult result)
            throws PermissionException, CreateException {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public UpdateUserValidationResult validateUpdateUser(final ApplicationUser user) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void updateUser(final UpdateUserValidationResult updateUserValidationResult) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public DeleteUserValidationResult validateDeleteUser(final ApplicationUser loggedInUser, final String username) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public DeleteUserValidationResult validateDeleteUser(final ApplicationUser loggedInUser, final ApplicationUser userForDelete) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void removeUser(final ApplicationUser loggedInUser, final DeleteUserValidationResult result) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public AddUserToApplicationValidationResult validateAddUserToApplication(ApplicationUser user, ApplicationKey applicationKey) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public AddUserToApplicationValidationResult validateAddUserToApplication(final ApplicationUser loggedInUser, final ApplicationUser user, final ApplicationKey applicationKey) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void addUserToApplication(final AddUserToApplicationValidationResult result)
            throws AddException, PermissionException {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public RemoveUserFromApplicationValidationResult validateRemoveUserFromApplication(final ApplicationUser user, final ApplicationKey applicationKey) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public RemoveUserFromApplicationValidationResult validateRemoveUserFromApplication(final ApplicationUser loggedInUser, final ApplicationUser user, final ApplicationKey applicationKey) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void removeUserFromApplication(final RemoveUserFromApplicationValidationResult result)
            throws RemoveException, PermissionException {
        throw new UnsupportedOperationException("Not yet implemented");
    }
}
