package com.atlassian.jira.config.properties;

/**
 * @since v7.0
 */
public class MockJiraProperties extends JiraPropertiesImpl {
    public MockJiraProperties() {
        super(new MockPropertiesAccessor());
    }
}
