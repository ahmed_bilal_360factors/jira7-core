package com.atlassian.jira.database;

import com.atlassian.jira.util.DuckTypeProxyFactory;
import com.google.common.base.Preconditions;
import com.querydsl.sql.AbstractSQLQuery;
import com.querydsl.sql.SQLTemplates;
import org.ofbiz.core.entity.DelegatorInterface;

import javax.annotation.Nonnull;
import java.sql.Connection;
import java.util.Collections;
import java.util.function.Supplier;

import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;

/**
 * @since v7.0.6
 */
public class MockQueryDslAccessor implements QueryDslAccessor {
    private static final SQLTemplates DIALECT = SQLTemplates.DEFAULT;

    private final DuckTypeConnection duckTypeConnection;
    private final Connection proxyConnection;
    private final DelegatorInterface mockDelegatorInterface;

    public MockQueryDslAccessor() {
        duckTypeConnection = new DuckTypeConnection();
        proxyConnection = DuckTypeProxyFactory.newStrictProxyInstance(Connection.class, duckTypeConnection);
        mockDelegatorInterface = mock(DelegatorInterface.class);
    }

    @Override
    public <T> T executeQuery(@Nonnull final QueryCallback<T> callback) {
        return assertFinished(callback.runQuery(new DbConnectionImpl(proxyConnection, DIALECT, mockDelegatorInterface)));
    }

    @Override
    public void execute(@Nonnull final SqlCallback callback) {
        callback.run(new DbConnectionImpl(proxyConnection, DIALECT, mockDelegatorInterface));
    }

    @Override
    public ConnectionProvider withNewConnection() {
        return new ConnectionProvider() {
            @Override
            public <T> T executeQuery(@Nonnull QueryCallback<T> callback) {
                return assertFinished(callback.runQuery(new DbConnectionImpl(proxyConnection, DIALECT, mockDelegatorInterface)));
            }

            @Override
            public void execute(@Nonnull SqlCallback callback) {
                callback.run(new DbConnectionImpl(proxyConnection, DIALECT, mockDelegatorInterface));
            }
        };
    }

    @Override
    public ConnectionProvider withLegacyOfBizTransaction() {
        return new ConnectionProvider() {
            @Override
            public <T> T executeQuery(@Nonnull QueryCallback<T> callback) {
                return assertFinished(callback.runQuery(new DbConnectionImpl(proxyConnection, DIALECT, mockDelegatorInterface)));
            }

            @Override
            public void execute(@Nonnull SqlCallback callback) {
                callback.run(new DbConnectionImpl(proxyConnection, DIALECT, mockDelegatorInterface));
            }
        };
    }

    @Override
    public DbConnection withDbConnection(Connection connection) {
        return new DbConnectionImpl(connection, DIALECT, mockDelegatorInterface);
    }

    @Nonnull
    public DuckTypeConnection getConnection() {
        return duckTypeConnection;
    }

    public void setQueryResults(final String sql, final Iterable<ResultRow> expectedResults) {
        duckTypeConnection.setQueryResults(sql, expectedResults);
    }

    public void setQueryResults(final String sql, final ResultRow expectedResult) {
        setQueryResults(sql, Collections.singletonList(expectedResult));
    }

    public void setUpdateResults(String sql, Supplier<RuntimeException> exFactory) {
        duckTypeConnection.setUpdateResults(sql, exFactory);
    }

    public void reset() {
        duckTypeConnection.reset();
    }

    /**
     * Require MockQueryDslAccessor to invoke some action, when specified sql is being executed.
     *
     * @param sql    SQL query we listen on
     * @param action Action to be invoked when the sql is run
     */
    public void onSqlListener(String sql, Runnable action) {
        duckTypeConnection.onSqlListener(sql, action);
    }

    /**
     * Configures the accessor to return `firstResults` the first time `executeQuery(sql)` is called, and `laterResults`
     * on all subsequent queries.
     */
    public void setQueryEphemeralResults(final String sql, final Iterable<ResultRow> firstResults, final Iterable<ResultRow> laterResults) {
        duckTypeConnection.setQueryResults(sql, firstResults);
        duckTypeConnection.onSqlListener(sql, () -> duckTypeConnection.setQueryResults(sql, laterResults));
    }

    public void setUpdateResults(String sql, int rowCount) {
        duckTypeConnection.setUpdateResults(sql, rowCount);
    }

    public void assertAllExpectedStatementsWereRun() {
        duckTypeConnection.assertAllExpectedStatementsWereRun();
    }

    public DelegatorInterface getMockDelegatorInterface() {
        return mockDelegatorInterface;
    }

    /**
     * Set default answer for queries that do not have answer defined by {@link #setQueryResults(String, Iterable)} or
     * {@link #setQueryEphemeralResults(String, Iterable, Iterable)}. If no default answer is set querying DB with
     * query that doesn't have answer defined by {@code setQueryResult()} will result in {@link AssertionError}
     *
     * @param defaultQueryResult result for queries that do not have result defined explicitly
     */
    public void setDefaultQueryResult(@Nonnull Iterable<ResultRow> defaultQueryResult) {
        Preconditions.checkNotNull(defaultQueryResult);
        duckTypeConnection.setDefaultQueryResult(defaultQueryResult);
    }

    /**
     * Set default answer for update queries that do not have answer defined by
     * {@link #setUpdateResults(String, int)}. If no default answer is set querying DB with query
     * that doesn't have answer defined by {@code setUpdateResults()} will result in {@link AssertionError}
     *
     * @param defaultUpdateResult result for queries that do not have result defined explicitly
     */
    public void setDefaultUpdateResult(int defaultUpdateResult) {
        duckTypeConnection.setDefaultUpdateResult(defaultUpdateResult);
    }

    private static <T> T assertFinished(T result) {
        if (result instanceof AbstractSQLQuery) {
            fail("Query completed with type '" + result.getClass().getName() +
                    "'.  You probably closed the executeQuery callback prematurely.\n" + result);
        }
        return result;
    }

    @Override
    public String toString() {
        return "MockQueryDslAccessor[duckTypeConnection=" + duckTypeConnection + ']';
    }
}
