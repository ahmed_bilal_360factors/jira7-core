package com.atlassian.jira.security.roles;

import com.atlassian.jira.user.ApplicationUser;
import com.google.common.collect.ImmutableSet;

import java.util.Set;

public class MockUserRoleActor extends MockRoleActor {
    private final ApplicationUser user;

    public MockUserRoleActor(Long projectRoleId, Long projectId, ApplicationUser user) {
        super(projectRoleId, projectId, user.getName(), ProjectRoleActor.USER_ROLE_ACTOR_TYPE);
        this.user = user;
    }

    public Set<ApplicationUser> getUsers() {
        return ImmutableSet.of(user);
    }

    public boolean contains(ApplicationUser user) {
        return this.user.getKey().equals(user.getKey());
    }
}