package com.atlassian.jira.license;

import com.atlassian.extras.api.Contact;
import com.atlassian.extras.api.LicenseEdition;
import com.atlassian.extras.api.LicenseType;
import com.atlassian.extras.api.Organisation;
import com.atlassian.extras.api.Partner;
import com.atlassian.extras.api.Product;
import com.atlassian.extras.api.jira.JiraLicense;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static com.atlassian.extras.common.LicensePropertiesConstants.UNLIMITED_USERS;

/**
 * @since v6.3
 */
public class MockLicense implements JiraLicense {
    private LicenseType licenseType;
    private Date expiryDate;
    private boolean isExpired;
    private boolean evaluation;
    private Date maintenanceExpiryDate;
    private boolean maintenanceExpired;
    private int numberOfDaysBeforeMaintenanceExpiry;
    private int licenseVersion;
    private int numberOfUsers = 0;
    private final Map<String, String> properties = new HashMap<>();


    @Override
    public int getLicenseVersion() {
        return licenseVersion;
    }

    @Override
    public String getDescription() {
        return null;
    }

    @Override
    public Product getProduct() {
        return null;
    }

    @Override
    public Iterable<Product> getProducts() {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public String getServerId() {
        return null;
    }

    @Override
    public Partner getPartner() {
        return null;
    }

    @Override
    public Organisation getOrganisation() {
        return null;
    }

    @Override
    public Collection<Contact> getContacts() {
        return null;
    }

    @Override
    public Date getCreationDate() {
        return null;
    }

    @Override
    public Date getPurchaseDate() {
        return null;
    }

    @Override
    public LicenseType getLicenseType() {
        return licenseType;
    }

    @Override
    public String getProperty(final String name) {
        return properties.get(name);
    }

    @Override
    public boolean isExpired() {
        return isExpired;
    }

    @Override
    public Date getGracePeriodEndDate() {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public int getNumberOfDaysBeforeGracePeriodExpiry() {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public boolean isWithinGracePeriod() {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public boolean isGracePeriodExpired() {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public Date getExpiryDate() {
        return expiryDate;
    }

    @Override
    public int getNumberOfDaysBeforeExpiry() {
        return 0;
    }

    @Override
    public String getSupportEntitlementNumber() {
        return null;
    }

    @Override
    public Date getMaintenanceExpiryDate() {
        return maintenanceExpiryDate;
    }

    @Override
    public int getNumberOfDaysBeforeMaintenanceExpiry() {
        return numberOfDaysBeforeMaintenanceExpiry;
    }

    @Override
    public boolean isMaintenanceExpired() {
        return maintenanceExpired;
    }

    @Override
    public int getMaximumNumberOfUsers() {
        return numberOfUsers;
    }

    @Override
    public boolean isUnlimitedNumberOfUsers() {
        return UNLIMITED_USERS == numberOfUsers;
    }

    @Override
    public boolean isEvaluation() {
        return evaluation;
    }

    @Override
    public boolean isSubscription() {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public boolean isClusteringEnabled() {
        return false;
    }

    @Override
    public LicenseEdition getLicenseEdition() {
        return null;
    }

    public String setProperty(String property, String value) {
        return properties.put(property, value);
    }

    public void setLicenseType(final LicenseType licenseType) {
        this.licenseType = licenseType;
    }

    public void setExpiryDate(final Date date) {
        expiryDate = date;
    }

    public void setExpiryDate(long time) {
        setExpiryDate(new Date(time));
    }

    public void setExpired(final boolean isExpired) {
        this.isExpired = isExpired;
    }

    public void setEvaluation(boolean evaluation) {
        this.evaluation = evaluation;
    }

    public void setMaintenanceExpiryDate(long maintenanceExpiryDate) {
        setMaintenanceExpiryDate(new Date(maintenanceExpiryDate));
    }

    public void setMaintenanceExpiryDate(final Date maintenanceExpiryDate) {
        this.maintenanceExpiryDate = maintenanceExpiryDate;
    }

    public void setMaintenanceExpired(final boolean maintenanceExpired) {
        this.maintenanceExpired = maintenanceExpired;
    }

    public void setNumberOfDaysBeforeMaintenanceExpiry(final int numberOfDaysBeforeMaintenanceExpiry) {
        this.numberOfDaysBeforeMaintenanceExpiry = numberOfDaysBeforeMaintenanceExpiry;
    }

    public void setLicenseVersion(final int licenseVersion) {
        this.licenseVersion = licenseVersion;
    }

    public void setNumberOfUsers(final int numberOfUsers) {
        this.numberOfUsers = numberOfUsers;
    }
}
