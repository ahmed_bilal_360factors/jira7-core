package com.atlassian.jira.matchers;

import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.hamcrest.Description;
import org.hamcrest.TypeSafeDiagnosingMatcher;

import static org.apache.commons.lang3.StringUtils.join;

public class ErrorCollectionMatcher extends TypeSafeDiagnosingMatcher<ErrorCollection> {
    private final ErrorCollection expected;
    private final boolean expectedNoErrors;
    private final boolean expectSingleError;

    public ErrorCollectionMatcher(final ErrorCollection errorCollection, final boolean expectedNoErrors, final boolean expectSingleError) {
        expected = errorCollection;
        this.expectedNoErrors = expectedNoErrors;
        this.expectSingleError = expectSingleError;
    }

    public static ErrorCollectionMatcher hasError(final String fieldName, final String errorMessage) {
        SimpleErrorCollection errorCollection = new SimpleErrorCollection();
        errorCollection.addError(fieldName, errorMessage);
        return new ErrorCollectionMatcher(errorCollection, false, false);
    }

    public static ErrorCollectionMatcher hasError(final String fieldName, final String errorMessage, final ErrorCollection.Reason reason) {
        SimpleErrorCollection errorCollection = new SimpleErrorCollection();
        errorCollection.addError(fieldName, errorMessage);
        errorCollection.setReasons(ImmutableSet.of(reason));
        return new ErrorCollectionMatcher(errorCollection, false, false);
    }

    public static ErrorCollectionMatcher hasOnlyError(final String fieldName, final String errorMessage) {
        SimpleErrorCollection errorCollection = new SimpleErrorCollection();
        errorCollection.addError(fieldName, errorMessage);
        return new ErrorCollectionMatcher(errorCollection, false, true);
    }

    public static ErrorCollectionMatcher hasErrorMessage(final String errorMessage) {
        SimpleErrorCollection errorCollection = new SimpleErrorCollection();
        errorCollection.addErrorMessage(errorMessage);
        return new ErrorCollectionMatcher(errorCollection, false, false);
    }

    public static ErrorCollectionMatcher hasErrorMessage(final String errorMessage, final ErrorCollection.Reason reason) {
        SimpleErrorCollection errorCollection = new SimpleErrorCollection();
        errorCollection.addErrorMessage(errorMessage, reason);
        return new ErrorCollectionMatcher(errorCollection, false, false);
    }

    public static ErrorCollectionMatcher hasOnlyErrorMessage(final String errorMessage, final ErrorCollection.Reason reason) {
        SimpleErrorCollection errorCollection = new SimpleErrorCollection();
        errorCollection.addErrorMessage(errorMessage, reason);
        return new ErrorCollectionMatcher(errorCollection, false, true);
    }

    public static ErrorCollectionMatcher hasErrorMessages(final String... errorMessage) {
        SimpleErrorCollection errorCollection = new SimpleErrorCollection();
        errorCollection.addErrorMessages(Lists.newArrayList(errorMessage));
        return new ErrorCollectionMatcher(errorCollection, false, false);
    }

    public static ErrorCollectionMatcher hasErrors(@javax.annotation.Nonnull ErrorCollection errorCollection) {
        return new ErrorCollectionMatcher(errorCollection, false, false);
    }

    public static ErrorCollectionMatcher hasNoErrors() {
        return new ErrorCollectionMatcher(null, true, false);
    }

    @Override
    protected boolean matchesSafely(final ErrorCollection errorCollection, final Description description) {
        boolean hasErrorMessages = errorCollection.getErrorMessages() != null && !errorCollection.getErrorMessages().isEmpty();
        boolean hasErrors = errorCollection.getErrors() != null && !errorCollection.getErrors().isEmpty();
        final int errorMessagesCount = hasErrorMessages ? errorCollection.getErrorMessages().size() : 0;
        final int errorsCount = hasErrors ? errorCollection.getErrors().size() : 0;

        if (expectedNoErrors) {
            if (errorCollection.hasAnyErrors() || hasErrorMessages || hasErrors) {
                description.appendText(String.format("Has errors: %s %n", toString(errorCollection)));
                return false;
            } else {
                //success - has no errors
                return true;
            }
        }

        boolean expectedErrorMessages = expected.getErrorMessages() != null && !expected.getErrorMessages().isEmpty();
        boolean expectedErrors = expected.getErrors() != null && !expected.getErrors().isEmpty();
        boolean expectedReasons = expected.getReasons() != null && !expected.getReasons().isEmpty();
        final int expectedErrorMessagesCount = expectedErrorMessages ? expected.getErrorMessages().size() : 0;
        final int expectedErrorsCount = expectedErrors ? expected.getErrors().size() : 0;

        if (expectSingleError) {
            if (errorMessagesCount + errorsCount != 1) {
                description.appendText(String.format("Has more than one error: %s %n", toString(errorCollection)));
                return false;
            }

            if (expectedErrorMessages && !expected.getErrorMessages().equals(errorCollection.getErrorMessages())) {
                appendError(errorCollection, description);
                return false;
            }

            if (expectedErrors && !expected.getErrors().equals(errorCollection.getErrors())) {
                appendError(errorCollection, description);
                return false;
            }
        }

        if (errorMessagesCount < expectedErrorMessagesCount || errorsCount < expectedErrorsCount) {
            appendError(errorCollection, description);
            return false;
        }

        if (expectedErrorMessages) {
            if (!errorCollection.getErrorMessages().containsAll(expected.getErrorMessages())) {
                appendError(errorCollection, description);
                return false;
            }
        }

        if (expectedErrors) {
            if (!Maps.difference(errorCollection.getErrors(), expected.getErrors()).entriesOnlyOnRight().isEmpty()) {
                appendError(errorCollection, description);
                return false;
            }

            for (String key : expected.getErrors().keySet()) {
                if (!errorCollection.getErrors().get(key).equals(expected.getErrors().get(key))) {
                    appendError(errorCollection, description);
                    return false;
                }
            }
        }

        if (expectedReasons) {
            if (!errorCollection.getReasons().containsAll(expected.getReasons())) {
                appendError(errorCollection, description);
                return false;
            }
        }

        return true;
    }

    private void appendError(final ErrorCollection errorCollection, final Description description) {
        description.appendText(String.format("Found: %s %n", toString(errorCollection)));
    }

    @Override
    public void describeTo(final Description description) {
        if (expectedNoErrors) {
            description.appendText("No errors");
            return;
        }
        description.appendText(toString(expected));
    }

    private String toString(ErrorCollection errorCollection) {
        return String.format("[ErrorMessages: {%s}, Errors: {%s}, Reasons: {%s}]",
                errorCollection.getErrorMessages() == null ? "null" : join(errorCollection.getErrorMessages(), ','),
                errorCollection.getErrors() == null ? "null" : join(errorCollection.getErrors().entrySet(), ','),
                errorCollection.getReasons() == null ? "null" : join(errorCollection.getReasons(), ','));
    }
}
