package com.atlassian.jira.mock;

import com.atlassian.jira.avatar.Avatar;
import com.atlassian.jira.icon.IconType;
import com.atlassian.jira.plugin.icon.IssueTypeIconTypePolicy;
import com.atlassian.jira.plugin.icon.ProjectIconTypePolicy;
import com.atlassian.jira.plugin.icon.UserIconTypePolicy;
import org.mockito.Mockito;

import javax.annotation.Nonnull;

/**
 * Non production bean implementation of {@link com.atlassian.jira.avatar.Avatar}.
 *
 * @since v4.0
 */
public class MockAvatar implements Avatar {
    private long id;
    private IconType iconType;
    private String contentType;
    private String fileName;
    private String owner;
    private boolean system;

    public MockAvatar(final long id, final String fileName, final String contentType, final IconType iconType, final String owner, final boolean system) {
        this.id = id;
        this.contentType = contentType;
        this.fileName = fileName;
        this.owner = owner;
        this.system = system;
        this.iconType = iconType;
    }

    @Deprecated
    public MockAvatar(final long id, final String fileName, final String contentType, final Avatar.Type type, final String owner, final boolean system) {
        this(id, fileName, contentType, (IconType) null, owner, system);

        IconType iconType;
        switch (type) {
            case USER:
                UserIconTypePolicy uPolicy = Mockito.mock(UserIconTypePolicy.class);
                iconType = IconType.USER_ICON_TYPE;
                break;
            case PROJECT:
                ProjectIconTypePolicy pPolicy = Mockito.mock(ProjectIconTypePolicy.class);
                iconType = IconType.PROJECT_ICON_TYPE;
                break;
            case ISSUETYPE:
                IssueTypeIconTypePolicy iPolicy = Mockito.mock(IssueTypeIconTypePolicy.class);
                iconType = IconType.ISSUE_TYPE_ICON_TYPE;
                break;
            default:
                iconType = null;
                break;
        }
        this.iconType = iconType;
    }

    @Override
    public Type getAvatarType() {
        if (Type.supportsName(iconType.getKey())) {
            return Type.getByName(iconType.getKey());
        } else {
            return Type.OTHER;
        }
    }

    @Nonnull
    @Override
    public IconType getIconType() {
        return iconType;
    }

    public String getContentType() {
        return contentType;
    }

    public String getFileName() {
        return fileName;
    }

    public Long getId() {
        return id;
    }

    public String getOwner() {
        return owner;
    }

    public boolean isSystemAvatar() {
        return system;
    }
}
