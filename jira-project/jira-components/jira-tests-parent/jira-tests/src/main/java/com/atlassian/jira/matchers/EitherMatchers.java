package com.atlassian.jira.matchers;

import com.atlassian.fugue.Either;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.hamcrest.TypeSafeDiagnosingMatcher;

/**
 * Matcher for {@link com.atlassian.fugue.Either} instances.
 *
 * @since v7.0
 */
public final class EitherMatchers {
    private EitherMatchers() {
        //don't create me.
    }

    public static <L, R> Matcher<Either<L, R>> left() {
        return left(Matchers.anything());
    }

    public static <L, R> Matcher<Either<L, R>> left(L item) {
        return left(Matchers.equalTo(item));
    }

    public static <L, R> Matcher<Either<L, R>> left(Matcher<? super L> matcher) {
        return new LeftMatcher<>(matcher);
    }

    public static <L, R> Matcher<Either<L, R>> right() {
        return right(Matchers.anything());
    }

    public static <L, R> Matcher<Either<L, R>> right(R item) {
        return right(Matchers.equalTo(item));
    }

    public static <L, R> Matcher<Either<L, R>> right(Matcher<? super R> matcher) {
        return new RightMatcher<>(matcher);
    }

    private static class LeftMatcher<L, R> extends TypeSafeDiagnosingMatcher<Either<L, R>> {
        private Matcher<? super L> matcher;

        private LeftMatcher(Matcher<? super L> matcher) {
            this.matcher = matcher;
        }

        @Override
        protected boolean matchesSafely(final Either<L, R> item, final Description mismatchDescription) {
            if (!item.isLeft()) {
                mismatchDescription.appendText("RIGHT[")
                        .appendValue(item.right().get())
                        .appendText("]");
                return false;
            } else if (!matcher.matches(item.left().get())) {
                mismatchDescription.appendText("LEFT[");
                describeMismatch(item.left().get(), mismatchDescription);
                mismatchDescription.appendText("]");
                return false;
            } else {
                return true;
            }
        }

        @Override
        public void describeTo(final Description description) {
            description.appendText("LEFT[").appendDescriptionOf(matcher).appendText("]");
        }
    }

    private static class RightMatcher<L, R> extends TypeSafeDiagnosingMatcher<Either<L, R>> {
        private Matcher<? super R> matcher;

        private RightMatcher(Matcher<? super R> matcher) {
            this.matcher = matcher;
        }

        @Override
        protected boolean matchesSafely(final Either<L, R> item, final Description mismatchDescription) {
            if (!item.isRight()) {
                mismatchDescription.appendText("LEFT[")
                        .appendValue(item.left().get())
                        .appendText("]");
                return false;
            } else if (!matcher.matches(item.right().get())) {
                mismatchDescription.appendText("RIGHT[");
                matcher.describeMismatch(item.right().get(), mismatchDescription);
                mismatchDescription.appendText("]");
                return false;
            } else {
                return true;
            }
        }

        @Override
        public void describeTo(final Description description) {
            description.appendText("RIGHT[").appendDescriptionOf(matcher).appendText("]");
        }
    }
}
