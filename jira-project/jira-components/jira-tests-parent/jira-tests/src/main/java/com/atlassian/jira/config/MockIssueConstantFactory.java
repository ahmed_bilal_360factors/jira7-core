package com.atlassian.jira.config;

import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.issue.issuetype.IssueTypeImpl;
import com.atlassian.jira.issue.priority.Priority;
import com.atlassian.jira.issue.priority.PriorityImpl;
import com.atlassian.jira.issue.resolution.Resolution;
import com.atlassian.jira.issue.resolution.ResolutionImpl;
import com.atlassian.jira.issue.status.Status;
import com.atlassian.jira.issue.status.StatusImpl;
import com.atlassian.jira.model.querydsl.IssueTypeDTO;
import com.atlassian.jira.model.querydsl.PriorityDTO;
import com.atlassian.jira.model.querydsl.ResolutionDTO;
import com.atlassian.jira.model.querydsl.StatusDTO;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import org.ofbiz.core.entity.GenericValue;

/**
 * @since v5.2
 */
public class MockIssueConstantFactory implements IssueConstantFactory {

    private final StatusCategoryManager statusCategoryManager;
    private final OfBizDelegator ofBizDelegator;

    public MockIssueConstantFactory(final OfBizDelegator ofBizDelegator) {
        this.ofBizDelegator = ofBizDelegator;
        statusCategoryManager = null;
    }

    public MockIssueConstantFactory(StatusCategoryManager statusCategoryManager, final OfBizDelegator ofBizDelegator) {
        this.statusCategoryManager = statusCategoryManager;
        this.ofBizDelegator = ofBizDelegator;
    }

    @Override
    public Priority createPriority(GenericValue priorityGv) {
        return new PriorityImpl(priorityGv, null, null, null);
    }

    @Override
    public IssueType createIssueType(GenericValue issueTypeGv) {
        return new IssueTypeImpl(issueTypeGv, null, null, null, null);
    }

    @Override
    public Resolution createResolution(GenericValue resolutionGv) {
        return new ResolutionImpl(resolutionGv, null, null, null);
    }

    @Override
    public Status createStatus(GenericValue statusGv) {
        return new StatusImpl(statusGv, null, null, null, statusCategoryManager);
    }

    @Override
    public Priority createPriority(final PriorityDTO priorityDTO) {
        final GenericValue gv = priorityDTO.toGenericValue(ofBizDelegator);
        return createPriority(gv);
    }

    @Override
    public IssueType createIssueType(IssueTypeDTO issueTypeDTO) {
        final GenericValue gv = issueTypeDTO.toGenericValue(ofBizDelegator);
        return createIssueType(gv);
    }

    @Override
    public Resolution createResolution(final ResolutionDTO resolutionDTO) {
        final GenericValue gv = resolutionDTO.toGenericValue(ofBizDelegator);
        return createResolution(gv);
    }

    @Override
    public Status createStatus(final StatusDTO statusDTO) {
        final GenericValue gv = statusDTO.toGenericValue(ofBizDelegator);
        return createStatus(gv);
    }
}
