package com.atlassian.jira.license;

public class MockRenaissanceMigrationStatus implements RenaissanceMigrationStatus {
    private boolean mark = false;

    @Override
    public boolean markDone() {
        boolean result = mark;
        mark = true;
        return result;
    }

    @Override
    public boolean hasMigrationRun() {
        return mark;
    }
}
