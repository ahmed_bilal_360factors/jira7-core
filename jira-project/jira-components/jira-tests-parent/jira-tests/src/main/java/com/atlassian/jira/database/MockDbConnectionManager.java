package com.atlassian.jira.database;

import com.querydsl.sql.SQLTemplates;
import org.ofbiz.core.entity.DelegatorInterface;

import javax.annotation.Nonnull;
import java.util.function.Supplier;

/**
 * Since DbConnectionManager has been deprecated, this pretty much just maps everything onto
 * MockQueryDslAccessor, instead.
 *
 * @since v6.4
 */
public class MockDbConnectionManager implements DbConnectionManager {
    private final MockQueryDslAccessor delegate = new MockQueryDslAccessor();

    @Override
    public <T> T executeQuery(@Nonnull final QueryCallback<T> callback) {
        return delegate.executeQuery(callback);
    }

    @Override
    public void execute(@Nonnull final SqlCallback callback) {
        delegate.execute(callback);
    }

    @Nonnull
    @Override
    public SQLTemplates getDialect() {
        return SQLTemplates.DEFAULT;
    }

    public void setQueryResults(final String sql, final Iterable<ResultRow> expectedResults) {
        delegate.setQueryResults(sql, expectedResults);
    }

    public void setUpdateResults(String sql, int rowCount) {
        delegate.setUpdateResults(sql, rowCount);
    }

    public void setUpdateResults(String sql, Supplier<RuntimeException> exFactory) {
        delegate.setUpdateResults(sql, exFactory);
    }

    public void assertAllExpectedStatementsWereRun() {
        delegate.assertAllExpectedStatementsWereRun();
    }

    public void reset() {
        delegate.reset();
    }

    /**
     * Require MockDbConnectionManager to invoke some action, when specified sql is being executed.
     *
     * @param sql    SQL query we listen on
     * @param action Action to be invoked when the sql is run
     */
    public void onSqlListener(String sql, Runnable action) {
        delegate.onSqlListener(sql, action);
    }

    public DelegatorInterface getMockDelegatorInterface() {
        return delegate.getMockDelegatorInterface();
    }

}
