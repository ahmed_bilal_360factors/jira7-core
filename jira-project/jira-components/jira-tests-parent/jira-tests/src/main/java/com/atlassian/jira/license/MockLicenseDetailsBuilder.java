package com.atlassian.jira.license;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.extras.api.LicenseType;
import com.google.common.collect.Maps;
import org.mockito.Matchers;

import java.util.Date;
import java.util.Map;

import static com.atlassian.jira.license.DefaultLicenseDetails.PAID_LICENSE_TYPES;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Mock LicenseDetails Builder
 *
 * @since v7.0
 */
public class MockLicenseDetailsBuilder {
    private String rawLicense;
    private String description;
    private Map<ApplicationKey, ApplicationRoleHolder> applicationSeatMap = Maps.newHashMap();
    private LicenseType licenseType;
    private boolean isMaintenanceValidForBuildDate = true;
    private boolean isDataCentreLicense = false;
    private boolean isEvaulationLicense = false;

    public MockLicenseDetailsBuilder setApplicationRoleWithLimit(ApplicationKey key, int userLimit) {
        if (applicationSeatMap.containsKey(key)) {
            throw new IllegalArgumentException("Application [" + key + "] already used in mock");
        }
        ApplicationRoleHolder applicationRoleHolder = new ApplicationRoleHolder()
                .setKey(key)
                .setUserLimit(userLimit);
        this.applicationSeatMap.put(key, applicationRoleHolder);
        return this;
    }

    public MockLicenseDetailsBuilder setRawLicense(String rawLicense) {
        if (this.rawLicense != null) {
            throw new IllegalArgumentException("Already have a raw license");
        }
        this.rawLicense = rawLicense;
        return this;
    }

    public MockLicenseDetailsBuilder setDescription(String description) {
        this.description = description;
        return this;
    }

    public MockLicenseDetailsBuilder setLicenseType(final LicenseType licenseType) {
        this.licenseType = licenseType;
        return this;
    }

    public MockLicenseDetailsBuilder setAsDataCenter(final boolean isDataCentreLicense) {
        this.isDataCentreLicense = isDataCentreLicense;
        return this;
    }

    public MockLicenseDetailsBuilder setAsEvaluationLicense(final boolean isEvaulationLicense) {
        this.isEvaulationLicense = isEvaulationLicense;
        return this;
    }

    public LicenseDetails build() {
        LicenseDetails mockLicenseDetails = mock(LicenseDetails.class);

        if (rawLicense != null) {
            when(mockLicenseDetails.getLicenseString()).thenReturn(rawLicense);
        }
        if (description != null) {
            when(mockLicenseDetails.getDescription()).thenReturn(description);
        }
        //There will always be a DefaultLicenseApplications, getLicensedApplications would never be null
        LicensedApplications licensedApplications = mock(LicensedApplications.class);
        when(mockLicenseDetails.getLicensedApplications()).thenReturn(licensedApplications);
        if (!applicationSeatMap.isEmpty()) {
            when(licensedApplications.getKeys()).thenReturn(applicationSeatMap.keySet());
            for (ApplicationRoleHolder applicationRoleHolder : applicationSeatMap.values()) {
                when(licensedApplications.getUserLimit(applicationRoleHolder.key))
                        .thenReturn(applicationRoleHolder.userLimit);
            }
        }
        when(mockLicenseDetails.hasApplication(any(ApplicationKey.class))).thenAnswer(
                invoc -> applicationSeatMap.containsKey((ApplicationKey) invoc.getArguments()[0]));

        when(mockLicenseDetails.getLicenseVersion()).thenReturn(2);

        if (licenseType != null) {
            when(mockLicenseDetails.getLicenseType()).thenReturn(licenseType);
            if (licenseType == LicenseType.STARTER) {
                when(mockLicenseDetails.isStarter()).thenReturn(true);
            }
            if (PAID_LICENSE_TYPES.contains(licenseType)) {
                when(mockLicenseDetails.isPaidType()).thenReturn(true);
            }
        } else {
            when(mockLicenseDetails.getLicenseType()).thenReturn(LicenseType.COMMERCIAL);
        }
        when(mockLicenseDetails.isMaintenanceValidForBuildDate(Matchers.<Date>any())).thenReturn(isMaintenanceValidForBuildDate);
        when(mockLicenseDetails.isDataCenter()).thenReturn(isDataCentreLicense);
        when(mockLicenseDetails.isEvaluation()).thenReturn(isEvaulationLicense);
        return mockLicenseDetails;
    }

    public MockLicenseDetailsBuilder isMaintenanceValidForBuildDate(boolean isMaintenanceValidForBuildDate) {
        this.isMaintenanceValidForBuildDate = isMaintenanceValidForBuildDate;
        return this;
    }

    private static class ApplicationRoleHolder {
        private ApplicationKey key;
        private int userLimit = 0;

        public ApplicationRoleHolder setKey(ApplicationKey key) {
            this.key = key;
            return this;
        }

        public ApplicationRoleHolder setUserLimit(int userLimit) {
            this.userLimit = userLimit;
            return this;
        }
    }
}
