package com.atlassian.jira.matchers;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.hamcrest.TypeSafeDiagnosingMatcher;

import java.util.Optional;

/*
 * Hamcrest Matchers for {@link java.util.Optional}. For {@link com.atlassian.fugue.Option} matchers look into
 * {@link OptionMatcher}.
 */
public class OptionalMatchers {

    public static <T> Matcher<Optional<T>> none() {
        return new NoneMatchers<>();
    }

    public static <T> Matcher<Optional<T>> some(Matcher<? super T> matcher) {
        return new SomeMatchers<>(matcher);
    }

    public static <T> Matcher<Optional<T>> some(T value) {
        return new SomeMatchers<>(Matchers.equalTo(value));
    }

    private static class NoneMatchers<T> extends TypeSafeDiagnosingMatcher<Optional<T>> {
        @Override
        protected boolean matchesSafely(final Optional<T> item, final Description mismatchDescription) {
            if (item.isPresent()) {
                mismatchDescription.appendValue(item);
                return false;
            } else {
                return true;
            }
        }

        @Override
        public void describeTo(final Description description) {
            description.appendValue(Optional.empty());
        }
    }

    private static class SomeMatchers<T> extends TypeSafeDiagnosingMatcher<Optional<T>> {
        private final Matcher<? super T> matcher;

        private SomeMatchers(final Matcher<? super T> matcher) {
            this.matcher = matcher;
        }

        @Override
        protected boolean matchesSafely(final Optional<T> item, final Description mismatchDescription) {
            if (!item.isPresent()) {
                mismatchDescription.appendValue(item);
                return false;
            } else {
                if (matcher.matches(item.get())) {
                    return true;
                } else {
                    mismatchDescription.appendText("some(");
                    matcher.describeMismatch(item.get(), mismatchDescription);
                    mismatchDescription.appendText(")");
                    return false;
                }
            }
        }

        @Override
        public void describeTo(final Description description) {
            description.appendText("some(").appendDescriptionOf(matcher).appendText(")");
        }
    }
}
