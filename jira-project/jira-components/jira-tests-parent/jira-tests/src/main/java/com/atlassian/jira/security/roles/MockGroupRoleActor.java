package com.atlassian.jira.security.roles;

import com.atlassian.crowd.embedded.api.CrowdService;
import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserUtil;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.ImmutableSortedSet;

import java.util.SortedSet;

public class MockGroupRoleActor extends MockRoleActor {
    private final Group group;

    public MockGroupRoleActor(Long projectRoleId, Long projectId, Group group) {
        super(projectRoleId, projectId, group.getName(), GROUP_ROLE_ACTOR_TYPE);
        this.group = group;
    }

    @Override
    public SortedSet<ApplicationUser> getUsers() {
        UserUtil userUtil = ComponentAccessor.getUserUtil();
        return ImmutableSortedSet.copyOf(userUtil.getAllUsersInGroups(ImmutableSet.of(group)));
    }

    @Override
    public boolean contains(ApplicationUser user) {
        CrowdService crowdService = ComponentAccessor.getCrowdService();
        return crowdService.isUserMemberOfGroup(user.getDirectoryUser(), group);
    }
}