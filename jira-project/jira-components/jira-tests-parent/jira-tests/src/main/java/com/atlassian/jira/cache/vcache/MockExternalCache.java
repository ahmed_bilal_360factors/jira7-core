package com.atlassian.jira.cache.vcache;


import com.atlassian.vcache.CasIdentifier;
import com.atlassian.vcache.DirectExternalCache;
import com.atlassian.vcache.ExternalCacheSettings;
import com.atlassian.vcache.IdentifiedValue;
import com.atlassian.vcache.JvmCache;
import com.atlassian.vcache.JvmCacheSettingsBuilder;
import com.atlassian.vcache.Marshaller;
import com.atlassian.vcache.MarshallerException;
import com.atlassian.vcache.PutPolicy;
import com.atlassian.vcache.StableReadExternalCache;
import com.atlassian.vcache.internal.core.service.GuavaJvmCache;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.CompletionStage;
import java.util.function.Function;
import java.util.function.Supplier;

import static java.util.concurrent.CompletableFuture.completedFuture;

@ParametersAreNonnullByDefault
public class MockExternalCache<T> implements DirectExternalCache<T>, StableReadExternalCache<T> {
    private final String name;
    private final Marshaller<T> marshaller;
    private final JvmCache<String, byte[]> internalCache;

    public MockExternalCache(final String name, final Marshaller<T> marshaller, final ExternalCacheSettings settings) {
        this.name = name;
        this.marshaller = marshaller;
        final JvmCacheSettingsBuilder jvmCacheSettingsBuilder = new JvmCacheSettingsBuilder();
        settings.getDefaultTtl().ifPresent(jvmCacheSettingsBuilder::defaultTtl);
        jvmCacheSettingsBuilder.maxEntries(settings.getEntryCountHint().orElse(Integer.MAX_VALUE));

        this.internalCache = new GuavaJvmCache<>(name + "-internal", jvmCacheSettingsBuilder.build());
    }

    @Nonnull
    @Override
    public CompletionStage<Optional<IdentifiedValue<T>>> getIdentified(final String s) {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Nonnull
    @Override
    public CompletionStage<Map<String, Optional<IdentifiedValue<T>>>> getBulkIdentified(final Iterable<String> iterable) {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Nonnull
    @Override
    public CompletionStage<Boolean> removeIf(final String s, final CasIdentifier casIdentifier) {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Nonnull
    @Override
    public CompletionStage<Boolean> replaceIf(final String s, final CasIdentifier casIdentifier, final T t) {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Nonnull
    @Override
    public CompletionStage<Optional<T>> get(final String key) {
        return completedFuture(internalCache.get(key).map(this::unmarshall));
    }

    @Nonnull
    @Override
    public CompletionStage<T> get(final String key, final Supplier<T> supplier) {
        final byte[] raw = internalCache.get(key, () -> marshall(supplier.get()));
        return completedFuture(unmarshall(raw));
    }

    @Nonnull
    @Override
    public CompletionStage<Map<String, Optional<T>>> getBulk(final Iterable<String> iterable) {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Nonnull
    @Override
    public CompletionStage<Map<String, T>> getBulk(final Function<Set<String>, Map<String, T>> function,
                                                   final Iterable<String> iterable) {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Nonnull
    @Override
    public CompletionStage<Boolean> put(final String key, final T value, final PutPolicy putPolicy) {
        switch (putPolicy) {
            case ADD_ONLY:
                return completedFuture(!internalCache.putIfAbsent(key, marshall(value)).isPresent());
            case PUT_ALWAYS:
                internalCache.put(key, marshall(value));
                return completedFuture(true);
            case REPLACE_ONLY:
                final boolean result = internalCache.get(key).map((v) -> {
                    internalCache.put(key, marshall(value));
                    return true;
                }).orElseGet(() -> false);
                return completedFuture(result);
            default:
                throw new UnsupportedOperationException("Not implemented");
        }

    }

    @Nonnull
    @Override
    public CompletionStage<Void> remove(final Iterable<String> iterable) {
        iterable.forEach(internalCache::remove);
        return completedFuture(null);
    }

    @Nonnull
    @Override
    public CompletionStage<Void> removeAll() {
        internalCache.removeAll();
        return completedFuture(null);
    }

    @Nonnull
    @Override
    public String getName() {
        return name;
    }

    private byte[] marshall(T object) {
        try {
            return marshaller.marshall(object);
        } catch (MarshallerException e) {
            throw new RuntimeException(e);
        }
    }

    private T unmarshall(byte[] raw) {
        try {
            return marshaller.unmarshall(raw);
        } catch (MarshallerException e) {
            throw new RuntimeException(e);
        }
    }
}
