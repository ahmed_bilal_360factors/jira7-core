package com.atlassian.jira.matchers;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

import java.nio.file.Files;
import java.nio.file.Path;

/**
 * Matchers for java.nio.Path
 *
 * @since v7.3
 */
public final class NioFileMatchers {
    private NioFileMatchers() {
        throw new AssertionError("Don't instantiate me");
    }

    public static Matcher<Path> exists() {
        return new TypeSafeMatcher<Path>() {
            private Path testedFile;

            @Override
            protected boolean matchesSafely(final Path file) {
                testedFile = file;
                return Files.exists(file);
            }

            @Override
            public void describeTo(final Description description) {
                description.appendText("that file ");
                description.appendValue(testedFile);
                description.appendText(" exists");
            }
        };
    }
}
