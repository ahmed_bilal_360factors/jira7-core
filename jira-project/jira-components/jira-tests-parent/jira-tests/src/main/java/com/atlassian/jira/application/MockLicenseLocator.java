package com.atlassian.jira.application;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.application.host.license.LicenseLocator;
import com.atlassian.fugue.Option;
import com.atlassian.sal.api.license.SingleProductLicenseDetailsView;
import com.google.common.collect.Maps;
import org.mockito.Mockito;

import javax.annotation.Nullable;
import java.util.Map;

import static org.mockito.Mockito.when;

public class MockLicenseLocator implements LicenseLocator {
    private Map<ApplicationKey, SingleProductLicenseDetailsView> detailsViewMap = Maps.newHashMap();

    @Override
    public Option<SingleProductLicenseDetailsView> apply(@Nullable final ApplicationKey input) {
        return Option.option(detailsViewMap.get(input));
    }

    public SingleProductLicenseDetailsView add(ApplicationKey key) {
        final SingleProductLicenseDetailsView detailsView = Mockito.mock(SingleProductLicenseDetailsView.class);
        when(detailsView.getProductKey()).thenReturn(key.value());

        detailsViewMap.put(key, detailsView);

        return detailsView;
    }
}

