package com.atlassian.jira.application;

import com.atlassian.application.api.Application;
import com.atlassian.application.api.ApplicationAccess;
import com.atlassian.application.api.ApplicationKey;
import com.atlassian.application.api.ApplicationManager;
import com.atlassian.application.api.PlatformApplication;
import com.atlassian.fugue.Option;
import com.google.common.collect.Iterables;
import com.google.common.collect.Maps;

import java.util.Map;

/**
 * @since v7.0
 */
public class MockApplicationManager implements ApplicationManager {
    private Option<PlatformApplication> platformApplication = Option.some(new MockPlatformApplication("platform"));
    private Map<ApplicationKey, Application> applications = Maps.newHashMap();

    @Override
    public PlatformApplication getPlatform() {
        return platformApplication.getOrThrow(() -> new IllegalStateException("No PlatformApplication available."));
    }

    @Override
    public Iterable<Application> getApplications() {
        return Iterables.concat(platformApplication, applications.values());
    }

    @Override
    public Option<Application> getApplication(final ApplicationKey key) {
        final Application application = applications.get(key);
        if (application == null) {
            if (platformApplication.isDefined() && platformApplication.get().getKey().equals(key)) {
                return platformApplication.map(Application.class::cast);
            } else {
                return Option.none();
            }
        } else {
            return Option.some(application);
        }
    }

    @Override
    public <A extends Application> Option<A> getApplication(final ApplicationKey key, final Class<A> type) {
        return getApplication(key).filter(type::isInstance).map(type::cast);
    }

    @Override
    public Option<ApplicationAccess> getAccess(final ApplicationKey key) {
        return getApplication(key).map(Application::getAccess);
    }

    public MockApplication addApplication(ApplicationKey key) {
        MockApplication application = new MockApplication(key);
        applications.put(key, application);
        return application;
    }

    /**
     * Remove the {@link MockApplication} identified by the specified application key.
     *
     * @param key key used to identify the {@link MockApplication}.
     */
    public void removeApplication(ApplicationKey key) {
        applications.remove(key);
    }
}
