package com.atlassian.jira.matchers;

import org.hamcrest.Description;
import org.hamcrest.TypeSafeDiagnosingMatcher;

import java.util.Comparator;

/**
 * Matches any sequence of elements which is sorted.
 *
 * @since v7.1
 */
public class OrderedSequenceMatcher<T> extends TypeSafeDiagnosingMatcher<Iterable<T>> {
    private final Comparator<T> cmp;
    private String cmpDescription;

    /**
     * @param cmp   The comparator that defines the ordering under which the sequence should be sorted.
     */
    private  OrderedSequenceMatcher(String cmpDescription, Comparator<T> cmp) {
        this.cmpDescription = cmpDescription;
        this.cmp = cmp;
    }

    public static <T> OrderedSequenceMatcher<T> isOrdered(String cmpDescription, Comparator<T> cmp) {
        return new OrderedSequenceMatcher<>(cmpDescription, cmp);
    }

    public static <T extends Comparable<T>> OrderedSequenceMatcher<T> isOrdered() {
        return new OrderedSequenceMatcher<>("its natural order", T::compareTo);
    }

    @Override
    protected boolean matchesSafely(Iterable<T> ts, Description description) {
        T prev = null;
        boolean prevSet = false;
        boolean result = true;

        for(T current : ts) {
            if (prevSet) {
                boolean valid = cmp.compare(prev, current) <= 0;
                result &= valid;
                description.appendText(valid ? " ≤ " : " ≰ ");
                description.appendValue(current);

            } else {
                prev = current;
                prevSet = true;
                description.appendValue(prev);
            }
        }

        return result;
    }

    @Override
    public void describeTo(Description description) {
        description.appendText("The iterable should be ordered over " + cmpDescription);
    }

}
