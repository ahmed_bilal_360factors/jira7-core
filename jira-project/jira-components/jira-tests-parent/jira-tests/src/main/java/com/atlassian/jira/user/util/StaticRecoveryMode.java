package com.atlassian.jira.user.util;

import com.atlassian.fugue.Option;
import com.atlassian.jira.user.ApplicationUser;

import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * Simple implementation of {@link com.atlassian.jira.user.util.RecoveryMode} that uses a static user.
 */
public class StaticRecoveryMode implements RecoveryMode {
    private final String recoveryUsername;

    public StaticRecoveryMode(final String recoveryUsername) {
        this.recoveryUsername = recoveryUsername;
    }

    @Override
    public boolean isRecoveryModeOn() {
        return recoveryUsername != null;
    }

    @Override
    public boolean isRecoveryUser(final ApplicationUser user) {
        return isRecoveryUsername(user == null ? null : user.getName());
    }

    @Override
    public boolean isRecoveryUsername(final String username) {
        return recoveryUsername != null && recoveryUsername.equals(username);
    }

    @Override
    public Option<String> getRecoveryUsername() {
        return Option.option(recoveryUsername);
    }

    public static StaticRecoveryMode enabled(String user) {
        return new StaticRecoveryMode(notNull("user", user));
    }

    public static StaticRecoveryMode disabled() {
        return new StaticRecoveryMode(null);
    }
}
