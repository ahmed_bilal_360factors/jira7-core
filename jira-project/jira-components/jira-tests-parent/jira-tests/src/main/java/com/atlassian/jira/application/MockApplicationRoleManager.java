package com.atlassian.jira.application;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.fugue.Option;
import com.atlassian.jira.user.ApplicationUser;
import com.google.common.base.Supplier;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Maps;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Map;
import java.util.Set;

/**
 * @since v7.0
 */
public class MockApplicationRoleManager implements ApplicationRoleManager {
    private final Map<ApplicationKey, ApplicationRole> data = Maps.newHashMap();

    @Nonnull
    @Override
    public Option<ApplicationRole> getRole(@Nonnull final ApplicationKey key) {
        return Option.option(data.get(key));
    }

    @Nonnull
    @Override
    public Set<ApplicationRole> getRoles() {
        return ImmutableSet.copyOf(data.values());
    }

    @Nonnull
    @Override
    public Set<ApplicationRole> getDefaultRoles() {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Nonnull
    @Override
    public Set<ApplicationKey> getDefaultApplicationKeys() {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public boolean hasAnyRole(@Nullable final ApplicationUser user) {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public boolean userHasRole(@Nullable final ApplicationUser user, final ApplicationKey key) {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public boolean userOccupiesRole(@Nullable final ApplicationUser user, final ApplicationKey key) {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public boolean hasExceededAllRoles(@Nonnull final ApplicationUser user) {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public boolean isAnyRoleLimitExceeded() {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public boolean isRoleLimitExceeded(@Nonnull final ApplicationKey role) {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public Set<ApplicationRole> getRolesForUser(@Nonnull final ApplicationUser user) {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public Set<ApplicationRole> getOccupiedLicenseRolesForUser(@Nonnull final ApplicationUser user) {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public Set<ApplicationRole> getRolesForGroup(@Nonnull final Group group) {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Nonnull
    @Override
    public Set<Group> getGroupsForLicensedRoles() {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public void removeGroupFromRoles(@Nonnull final Group group) {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public boolean isRoleInstalledAndLicensed(@Nonnull final ApplicationKey key) {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Nonnull
    @Override
    public ApplicationRole setRole(@Nonnull final ApplicationRole role) {
        data.put(role.getKey(), role);
        return role;
    }

    @Override
    public int getUserCount(@Nonnull final ApplicationKey key) {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public int getRemainingSeats(@Nonnull final ApplicationKey key) {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public boolean hasSeatsAvailable(@Nonnull final ApplicationKey key, final int seatCount) {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Nonnull
    @Override
    public Set<Group> getDefaultGroups(@Nonnull final ApplicationKey key) {
        final Supplier<Set<Group>> of = ImmutableSet::of;
        return getRole(key).map(ApplicationRole::getDefaultGroups).getOrElse(of);
    }

    public MockApplicationRole createRole(ApplicationKey key) {
        final MockApplicationRole mockApplicationRole = new MockApplicationRole(key);
        data.put(key, mockApplicationRole);
        return mockApplicationRole;
    }
}
