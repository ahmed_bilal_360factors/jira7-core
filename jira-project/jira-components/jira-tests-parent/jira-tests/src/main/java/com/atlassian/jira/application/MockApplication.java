package com.atlassian.jira.application;

import com.atlassian.application.api.Application;
import com.atlassian.application.api.ApplicationAccess;
import com.atlassian.application.api.ApplicationKey;
import com.atlassian.fugue.Option;
import com.atlassian.sal.api.license.SingleProductLicenseDetailsView;
import com.google.common.base.Supplier;
import org.joda.time.DateTime;

import java.net.URI;
import java.util.Date;

/**
 * @since v7.0
 */
public class MockApplication implements Application {
    private final ApplicationKey key;
    private DateTime buildDate = DateTime.now();

    public MockApplication(ApplicationKey key) {
        this.key = key;
    }

    public MockApplication(String key) {
        this(ApplicationKey.valueOf(key));
    }

    @Override
    public ApplicationKey getKey() {
        return key;
    }

    @Override
    public String getName() {
        return String.format("%s-name", key.value());
    }

    @Override
    public String getDescription() {
        return String.format("%s-description", key.value());
    }

    @Override
    public String getVersion() {
        return String.format("%s-version", key);
    }

    @Override
    public String getUserCountDescription(final Option<Integer> count) {
        final Supplier<String> unlimited = () -> String.format("Unlimited %s users", key);
        return count.map(c -> String.format("%d %s users", c, key)).getOrElse(unlimited);
    }

    @Override
    public Option<URI> getConfigurationURI() {
        return Option.none();
    }

    @Override
    public Option<URI> getPostInstallURI() {
        return Option.none();
    }

    @Override
    public Option<URI> getPostUpdateURI() {
        return Option.none();
    }

    @Override
    public Option<URI> getProductHelpServerSpaceURI() {
        return Option.none();
    }

    @Override
    public Option<URI> getProductHelpCloudSpaceURI() {
        return Option.none();
    }

    @Override
    public DateTime buildDate() {
        return buildDate;
    }

    @Override
    public Option<SingleProductLicenseDetailsView> getLicense() {
        return Option.none();
    }

    @Override
    public ApplicationAccess getAccess() {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public String getDefaultGroup() {
        return key.value() + "-group";
    }

    @Override
    public void clearConfiguration() {
        throw new UnsupportedOperationException("Not implemented");
    }

    public MockApplication buildDate(Date date) {
        this.buildDate = new DateTime(date);
        return this;
    }
}
