package com.atlassian.jira.user.util;

import com.atlassian.fugue.Option;
import com.atlassian.jira.user.ApplicationUser;

public class DisabledRecoveryMode implements RecoveryMode {
    @Override
    public boolean isRecoveryModeOn() {
        return false;
    }

    @Override
    public boolean isRecoveryUser(final ApplicationUser user) {
        return false;
    }

    @Override
    public boolean isRecoveryUsername(final String username) {
        return false;
    }

    @Override
    public Option<String> getRecoveryUsername() {
        return Option.none();
    }
}
