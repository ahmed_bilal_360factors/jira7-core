package com.atlassian.jira.matchers;

import com.atlassian.jira.util.ErrorCollection;
import org.hamcrest.Matcher;

/**
 * Matchers for {@link com.atlassian.jira.util.ErrorCollection}.
 *
 * @since v6.3
 */
public class ErrorCollectionMatchers {
    private ErrorCollectionMatchers() {
    }

    public static Matcher<ErrorCollection> isEmpty() {
        return ErrorCollectionMatcher.hasNoErrors();
    }

    public static Matcher<ErrorCollection> containsSystemError(final String errorMessage) {
        return ErrorCollectionMatcher.hasErrorMessage(errorMessage);
    }

    public static Matcher<ErrorCollection> containsFieldError(final String field, final String errorMessage) {
        return ErrorCollectionMatcher.hasError(field, errorMessage);
    }
}
