package com.atlassian.jira.user;

import com.atlassian.crowd.embedded.api.CrowdService;
import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.crowd.embedded.api.GroupWithAttributes;
import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.embedded.api.Query;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.crowd.embedded.api.UserCapabilities;
import com.atlassian.crowd.embedded.api.UserWithAttributes;
import com.atlassian.crowd.embedded.impl.DelegatingGroupWithAttributes;
import com.atlassian.crowd.embedded.impl.DelegatingUserWithAttributes;
import com.atlassian.crowd.embedded.impl.ImmutableAttributes;
import com.atlassian.crowd.embedded.impl.ImmutableUser;
import com.atlassian.crowd.exception.FailedAuthenticationException;
import com.atlassian.crowd.exception.InactiveAccountException;
import com.atlassian.crowd.exception.InvalidCredentialException;
import com.atlassian.crowd.exception.InvalidUserException;
import com.atlassian.crowd.exception.runtime.GroupNotFoundException;
import com.atlassian.crowd.exception.runtime.OperationFailedException;
import com.atlassian.crowd.exception.runtime.UserNotFoundException;
import com.atlassian.crowd.search.query.entity.GroupQuery;
import com.atlassian.crowd.search.query.entity.UserQuery;
import com.atlassian.crowd.search.query.membership.GroupMembersOfGroupQuery;
import com.atlassian.crowd.search.query.membership.GroupMembershipQuery;
import com.atlassian.crowd.search.query.membership.UserMembersOfGroupQuery;
import com.atlassian.jira.user.util.MockUserKeyStore;
import com.atlassian.jira.user.util.UserKeyStore;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import com.google.common.collect.SetMultimap;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @since v4.1
 */
public class MockCrowdService implements CrowdService {
    protected Map<String, User> users = new HashMap<>();
    protected Map<String, PasswordCredential> credentials = new HashMap<>();

    protected Map<String, Map<String, Set<String>>> userAttributes = new HashMap<>();
    protected Map<String, Group> groups = new HashMap<>();
    protected Map<String, SetMultimap<String, String>> groupAttributes = new HashMap<>();

    protected Multimap<String, User> groupMembers = HashMultimap.create();
    protected Multimap<String, Group> nestedGroups = HashMultimap.create();

    public User addUser(final User user, final String credential) {
        // Create a new User that lives in Directory 1
        User newUser = ImmutableUser.newUser(user).directoryId(1L).toUser();
        users.put(newUser.getName().toLowerCase(), newUser);
        credentials.put(newUser.getName().toLowerCase(), PasswordCredential.unencrypted(credential));
        getUserKeyStore().ensureUniqueKeyForNewUser(newUser.getName());
        return newUser;
    }

    @Override
    public UserWithAttributes addUser(UserWithAttributes user, String credential) {
        final User newUser = addUser((User) user, credential);
        for (String key : user.getKeys()) {
            setUserAttribute(newUser,key,user.getValue(key));
        }
        return new DelegatingUserWithAttributes(newUser,user);
    }

    public User addUser(final ApplicationUser user)
            throws InvalidUserException, InvalidCredentialException {
        return addUser(user.getDirectoryUser(), null);
    }

    protected UserKeyStore getUserKeyStore() {
        return new MockUserKeyStore();
    }

    public User updateUser(final User user) throws InvalidUserException {
        users.put(user.getName().toLowerCase(), user);
        return user;
    }

    @Override
    public User renameUser(User user, String newUsername) {
        throw new UnsupportedOperationException("Not implemented");
    }

    public void updateUserCredential(final User user, final String credential)
            throws InvalidCredentialException {
        credentials.put(user.getName().toLowerCase(), PasswordCredential.unencrypted(credential));
    }

    public void setUserAttribute(final User user, final String key, String value) {
        Map<String, Set<String>> allValues = userAttributes.get(user.getName());
        if (allValues == null) {
            allValues = new HashMap<>();
            userAttributes.put(user.getName(), allValues);
        }
        allValues.put(key, Collections.singleton(value));
    }

    public void setUserAttribute(final User user, final String key, Set<String> value) {
        throw new UnsupportedOperationException("Not implemented");
    }

    public void removeUserAttribute(final User user, final String key) {
        Map<String, Set<String>> allValues = userAttributes.get(user.getName());
        if (allValues != null) {
            allValues.remove(key);
        }
    }

    public void removeAllUserAttributes(final User user) {
        userAttributes.remove(user.getName());
    }

    public boolean removeUser(final User user) {
        if (users.containsKey(user.getName().toLowerCase())) {
            users.remove(user.getName().toLowerCase());
            credentials.remove(user.getName().toLowerCase());

            for (Group group : groups.values()) {
                removeUserFromGroup(user, group);
            }
            return true;
        }
        return false;
    }

    public Group addGroup(final Group group) {
        groups.put(group.getName(), group);
        return group;
    }

    public boolean mockUsersInGroup(final int numberOfUsers, final Group group) {
        for (int i = 0; i < numberOfUsers; i++) {
            groupMembers.put(group.getName(), new MockUser("User" + Math.random()));
        }
        return true;
    }

    public Group updateGroup(final Group group) {
        groups.put(group.getName(), group);
        return group;
    }

    public void setGroupAttribute(final Group group, final String attributeName, final String attributeValue) {
        final String groupName = group.getName();
        if (getGroup(groupName) == null) {
            throw new GroupNotFoundException(groupName);
        }
        final SetMultimap<String, String> attributes = groupAttributes.getOrDefault(group, HashMultimap.create());
        groupAttributes.put(groupName, attributes);

        attributes.put(attributeName, attributeValue);
    }

    public void setGroupAttribute(final Group group, final String attributeName, final Set<String> attributeValues) {
        throw new UnsupportedOperationException("Not implemented");
    }

    public void removeGroupAttribute(final Group group, final String attributeName) {
        throw new UnsupportedOperationException("Not implemented");
    }

    public void removeAllGroupAttributes(final Group group) {
        groupAttributes.remove(group.getName());
    }

    public boolean removeGroup(final Group group) {
        if (groups.containsKey(group.getName())) {
            groups.remove(group.getName());
            groupMembers.removeAll(group.getName());
            groupAttributes.remove(group.getName());
            return true;
        }
        return false;
    }

    public boolean addUserToGroup(final User user, final Group group) {
        return groupMembers.put(group.getName(), user);
    }

    public boolean addUserToGroup(final ApplicationUser user, final Group group) {
        return addUserToGroup(user.getDirectoryUser(), group);
    }

    public boolean addGroupToGroup(final Group childGroup, final Group parentGroup)
            throws com.atlassian.crowd.exception.runtime.GroupNotFoundException {
        //No circular reference checking
        if (!groups.containsKey(childGroup.getName()) || !groups.containsKey(parentGroup.getName())) {
            throw new GroupNotFoundException("Either child group or parent group does not exist");
        }

        return nestedGroups.put(parentGroup.getName(), childGroup);
    }

    public boolean removeUserFromGroup(final User user, final Group group) {
        return groupMembers.remove(group.getName(), user);
    }

    public boolean removeGroupFromGroup(final Group childGroup, final Group parentGroup) {
        throw new UnsupportedOperationException("Not implemented");
    }

    public boolean isUserDirectGroupMember(final User user, final Group group) {
        return getGroupMembers(group, false).contains(user);
    }

    public boolean isGroupDirectGroupMember(final Group childGroup, final Group parentGroup) {
        throw new UnsupportedOperationException("Not implemented");
    }

    public User authenticate(final String name, final String credential)
            throws OperationFailedException, FailedAuthenticationException {
        //Authenticate if we find the user
        User user = getUser(name);
        if (user == null) {
            throw new InactiveAccountException(name);
        }

        final PasswordCredential passwordCredential = credentials.get(name.toLowerCase());
        if (passwordCredential == null || passwordCredential.isEncryptedCredential()) {
            throw new FailedAuthenticationException(name);
        }
        final String knownCredential = passwordCredential.getCredential();

        if (knownCredential != null && knownCredential.equals(credential)) {
            return user;
        }
        throw new FailedAuthenticationException(name);
    }

    public User getUser(final String name) {
        return users.get(name.toLowerCase());
    }

    @Override
    public User getRemoteUser(String name) {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public User userAuthenticated(String s) throws UserNotFoundException, OperationFailedException, InactiveAccountException {
        throw new UnsupportedOperationException("Not implemented");
    }

    public UserWithAttributes getUserWithAttributes(final String name) {
        User user = getUser(name);
        if (user != null) {
            return new MockUser(name, user.getDisplayName(), user.getEmailAddress(), userAttributes.get(user.getName()));
        }
        return null;
    }

    public Group getGroup(final String name) {
        return groups.get(name);
    }


    public GroupWithAttributes getGroupWithAttributes(final String name) {
        final Group group = getGroup(name);
        if (group != null) {
            final SetMultimap<String, String> attributes = groupAttributes.getOrDefault(group.getName(), HashMultimap.create());

            return new DelegatingGroupWithAttributes(group, new ImmutableAttributes((Map) attributes.asMap()));
        }
        return null;
    }

    @SuppressWarnings({"unchecked"})
    public <T> Iterable<T> search(final Query<T> query) {
        if (query instanceof UserQuery) {
            if (query.getReturnType().isAssignableFrom(String.class)) {
                return (Iterable<T>) users.keySet();
            } else {
                return (Iterable<T>) users.values();
            }
        } else if (query instanceof GroupQuery) {
            if (query.getReturnType().isAssignableFrom(String.class)) {
                return (Iterable<T>) groups.keySet();
            } else {
                return (Iterable<T>) groups.values();
            }
        } else if (query instanceof GroupMembershipQuery) {
            List<T> groupList = new ArrayList<>();
            String userName = ((GroupMembershipQuery) query).getEntityNameToMatch();
            User user = getUser(userName);
            if (user == null) {
                return Collections.emptyList();
            }
            for (Group group : groups.values()) {
                if (isUserMemberOfGroup(user, group)) {
                    if (query.getReturnType().isAssignableFrom(String.class)) {
                        groupList.add((T) group.getName());
                    } else {
                        groupList.add((T) group);

                    }
                }
            }
            return groupList;
        } else if (query instanceof UserMembersOfGroupQuery) {
            List<T> userList = new ArrayList<>();
            String groupName = ((UserMembersOfGroupQuery) query).getEntityNameToMatch();
            Group group = getGroup(groupName);
            if (group != null) {
                for (User user : getGroupMembers(group, true)) {
                    if (query.getReturnType().isAssignableFrom(String.class)) {
                        userList.add((T) user.getName());
                    } else {
                        userList.add((T) user);
                    }
                }
            }
            return userList;
        } else if (query instanceof GroupMembersOfGroupQuery) {
            List<T> groupList = new ArrayList<>();
            String groupName = ((GroupMembersOfGroupQuery) query).getEntityNameToMatch();
            Group group = getGroup(groupName);
            if (group != null) {
                for (Group g : groups.values()) {
                    if (isGroupMemberOfGroup(g, group)) {
                        if (query.getReturnType().isAssignableFrom(String.class)) {
                            groupList.add((T) g.getName());
                        } else {
                            groupList.add((T) g);
                        }
                    }
                }
            }
            return groupList;
        }
        throw new UnsupportedOperationException("Unrecognized Query type '" + query + "'.");
    }

    public boolean isUserMemberOfGroup(final User user, final Group group) {
        return isUserMemberOfGroup(user.getName(), group.getName());
    }

    public boolean isUserMemberOfGroup(final String userName, final String groupName) {
        final List<User> groupMembers = getGroupMembers(groupName, true);
        for (User groupMember : groupMembers) {
            if (userName.equals(groupMember.getName())) {
                return true;
            }
        }
        return false;
    }

    public boolean isGroupMemberOfGroup(final String childGroupName, final String parentGroup) {
        for (Group child : nestedGroups.get(parentGroup)) {
            if (child.getName().equalsIgnoreCase(childGroupName)) {
                return true;
            }
            if (isGroupMemberOfGroup(childGroupName, child.getName())) {
                return true;
            }

        }
        return false;
    }

    public boolean isGroupMemberOfGroup(final Group childGroup, final Group parentGroup) {
        return isGroupMemberOfGroup(childGroup.getName(), parentGroup.getName());
    }

    public Iterable<User> searchUsersAllowingDuplicateNames(final Query<User> userQuery) {
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    @Override
    public UserCapabilities getCapabilitiesForNewUsers() {
        return () -> false;
    }

    private List<User> getGroupMembers(Group group, boolean nested) {
        if (group == null) {
            throw new IllegalArgumentException("null group");
        }

        return getGroupMembers(group.getName(), nested);
    }

    private List<User> getGroupMembers(String groupname, boolean nested) {
        Collection<User> members = groupMembers.get(groupname);
        List<User> allMembers = new ArrayList<>(members);
        if (nested) {
            for (Group g : nestedGroups.get(groupname)) {
                allMembers.addAll(getGroupMembers(g.getName(), true));
            }
        }
        return allMembers;
    }

    public Collection<User> getAllUsers() {
        return users.values();
    }
}
