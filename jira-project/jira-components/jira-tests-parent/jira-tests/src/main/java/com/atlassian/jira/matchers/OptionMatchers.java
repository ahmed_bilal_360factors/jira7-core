package com.atlassian.jira.matchers;

import com.atlassian.fugue.Option;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.hamcrest.TypeSafeDiagnosingMatcher;

/**
 * Hamcrest Matchers for {@link Option}. For {@link java.util.Optional} matchers look into
 * {@link OptionalMatchers}.
 */
public abstract class OptionMatchers<T> extends TypeSafeDiagnosingMatcher<Option<T>> {
    public static <T> OptionMatchers<T> none() {
        return new NoneMatchers<>();
    }

    public static <T> OptionMatchers<T> none(final Class<? extends T> ignored) {
        return new NoneMatchers<>();
    }

    public static <T> OptionMatchers<T> some(Matcher<? super T> matcher) {
        return new SomeMatchers<T>(matcher);
    }

    public static <T> OptionMatchers<T> some(T value) {
        return new SomeMatchers<>(Matchers.equalTo(value));
    }

    public static <T> OptionMatchers<T> fromOption(Option<T> option) {
        return option.map(OptionMatchers::some).getOrElse(OptionMatchers.none());
    }

    private static class NoneMatchers<T> extends OptionMatchers<T> {
        @Override
        protected boolean matchesSafely(final Option<T> item, final Description mismatchDescription) {
            if (item.isEmpty()) {
                return true;
            } else {
                mismatchDescription.appendValue(item);
                return false;
            }
        }

        @Override
        public void describeTo(final Description description) {
            description.appendValue(Option.none());
        }
    }

    private static class SomeMatchers<T> extends OptionMatchers<T> {
        private final Matcher<? super T> matcher;

        private SomeMatchers(final Matcher<? super T> matcher) {
            this.matcher = matcher;
        }

        @Override
        protected boolean matchesSafely(final Option<T> item, final Description mismatchDescription) {
            if (item.isEmpty()) {
                mismatchDescription.appendValue(item);
                return false;
            } else {
                if (matcher.matches(item.get())) {
                    return true;
                } else {
                    mismatchDescription.appendText("some(");
                    matcher.describeMismatch(item.get(), mismatchDescription);
                    mismatchDescription.appendText(")");
                    return false;
                }
            }
        }

        @Override
        public void describeTo(final Description description) {
            description.appendText("some(").appendDescriptionOf(matcher).appendText(")");
        }
    }
}
