package com.atlassian.jira.junit.rules;

import com.atlassian.jira.web.HttpServletVariables;
import com.atlassian.jira.web.action.MockRedirectSanitiser;
import com.atlassian.jira.web.action.RedirectSanitiser;
import com.atlassian.web.servlet.api.ServletForwarder;
import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;
import org.mockito.Mockito;

import javax.annotation.ParametersAreNonnullByDefault;

@ParametersAreNonnullByDefault
public class JiraWebActionSupportDependencies implements TestRule {
    private final MockComponentContainer componentContainer;

    private final MockRedirectSanitiser redirectSanitizerMock;
    private final HttpServletVariables httpServletVariablesMock;
    private final ServletForwarder servletForwarderMock;

    public JiraWebActionSupportDependencies(MockComponentContainer componentContainer) {
        this.componentContainer = componentContainer;
        redirectSanitizerMock = new MockRedirectSanitiser();
        httpServletVariablesMock = Mockito.mock(HttpServletVariables.class);
        servletForwarderMock = Mockito.mock(ServletForwarder.class);
    }

    public static JiraWebActionSupportDependencies build(MockitoContainer mockitoContainer) {
        return new JiraWebActionSupportDependencies(mockitoContainer.getMockComponentContainer());
    }

    @Override
    public Statement apply(Statement base, Description description) {
        return new Statement() {
            @Override
            public void evaluate() throws Throwable {
                componentContainer.addMock(RedirectSanitiser.class, redirectSanitizerMock);
                componentContainer.addMock(HttpServletVariables.class, httpServletVariablesMock);
                componentContainer.addMock(ServletForwarder.class, servletForwarderMock);

                base.evaluate();
            }
        };
    }

    public MockRedirectSanitiser getRedirectSanitizerMock() {
        return redirectSanitizerMock;
    }

    public HttpServletVariables getHttpServletVariablesMock() {
        return httpServletVariablesMock;
    }

    public ServletForwarder getServletForwarderMock() {
        return servletForwarderMock;
    }
}
