package com.atlassian.jira.application;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.collectors.CollectorsUtil;
import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.fugue.Option;
import com.google.common.collect.Maps;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static java.util.Arrays.asList;


public class MockApplicationRoleStore implements ApplicationRoleStore {
    private final Map<ApplicationKey, ApplicationRoleData> data = Maps.newHashMap();

    @Nonnull
    @Override
    public ApplicationRoleData get(@Nonnull final ApplicationKey key) {
        final ApplicationRoleData applicationRoleData = data.get(key);
        if (applicationRoleData == null) {
            return new ApplicationRoleData(key, Collections.<String>emptyList(), Option.<String>none(), false);
        } else {
            return applicationRoleData;
        }
    }

    @Nonnull
    @Override
    public ApplicationRoleData save(@Nonnull final ApplicationRoleData data) {
        this.data.put(data.getKey(), data);
        return data;
    }

    @Override
    public void removeGroup(@Nonnull String groupName) {
        for (Map.Entry<ApplicationKey, ApplicationRoleData> e : data.entrySet()) {
            // whether groupName is actually in the set of groups for ApplicationRoleData
            // doesn't matter, as #get can return an empty collection of groups.
            final ApplicationRoleData value = e.getValue();
            final Set<String> eGroups = value.getGroups();
            if (value.getGroups().contains(groupName)) {
                HashSet<String> newGroup = new HashSet<String>(eGroups);
                newGroup.remove(groupName);
                data.put(value.getKey(), new ApplicationRoleData(value.getKey(), newGroup, value.getDefaultGroups(), value.isSelectedByDefault()));
            }
        }
    }

    @Override
    public void removeByKey(@Nonnull final ApplicationKey key) {
        data.remove(key);
    }

    public MockApplicationRoleStore save(ApplicationKey key, String... groups) {
        save(new ApplicationRoleData(key, asList(groups), Option.none(String.class), false));
        return this;
    }

    public MockApplicationRoleStore save(ApplicationKey key, Group... groups) {
        save(new ApplicationRoleData(key, toNames(asList(groups)), Option.none(String.class), false));
        return this;
    }

    public MockApplicationRoleStore save(ApplicationRole role) {
        save(new ApplicationRoleData(role.getKey(), toNames(role.getGroups()), toNames(role.getDefaultGroups()), role.isSelectedByDefault()));
        return this;
    }

    private static Iterable<String> toNames(Collection<Group> groups) {
        return groups.stream().map(Group::getName).collect(CollectorsUtil.toImmutableSet());
    }
}
