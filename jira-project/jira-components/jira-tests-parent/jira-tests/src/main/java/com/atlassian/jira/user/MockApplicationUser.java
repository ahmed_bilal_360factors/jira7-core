package com.atlassian.jira.user;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.crowd.embedded.impl.IdentifierUtils;

import java.util.concurrent.atomic.AtomicLong;

/**
 * A convenient mock for {@link ApplicationUser}.
 *
 * @since v6.0
 */
public class MockApplicationUser implements ApplicationUser {
    public final static AtomicLong SEQUENCE = new AtomicLong(10000);

    private final String key;
    private final User user;
    public static final long FAKE_USER_DIRECTORY_ID = -1L;
    private final long id;

    private boolean active = true;

    /**
     * Uses the {@link IdentifierUtils#toLowerCase(String) lowercase} form of
     * the supplied {@code username} as the key.
     *
     * @param username as for {@link MockUser#MockUser(String)}
     */
    public MockApplicationUser(final String username) {
        this(IdentifierUtils.toLowerCase(username), new MockUser(username));
    }

    public MockApplicationUser(final String username, final long directoryId) {
        this(IdentifierUtils.toLowerCase(username), new MockUser(username, directoryId));
    }

    /**
     * Uses the {@link IdentifierUtils#toLowerCase(String) lowercase} form of
     * the supplied {@code username} as the key.
     *
     * @param username    as for {@link MockUser#MockUser(String, String, String)}
     * @param displayName as for {@link MockUser#MockUser(String, String, String)}
     * @param email       as for {@link MockUser#MockUser(String, String, String)}
     */
    public MockApplicationUser(final String username, final String displayName, final String email) {
        this(IdentifierUtils.toLowerCase(username), new MockUser(username, displayName, email));
    }

    /**
     * Uses the given key as-is.
     *
     * @param userKey  desired user's Key, the value to be returned for {@link com.atlassian.jira.user.ApplicationUser#getKey()}
     * @param username as for {@link MockUser#MockUser(String)}
     */
    public MockApplicationUser(final String userKey, final String username) {
        this(userKey, new MockUser(username));
    }

    /**
     * Uses the given key as-is.
     *
     * @param userKey     desired user's Key, the value to be returned for {@link com.atlassian.jira.user.ApplicationUser#getKey()}
     * @param username    as for {@link MockUser#MockUser(String, String, String)}
     * @param displayName as for {@link MockUser#MockUser(String, String, String)}
     * @param email       as for {@link MockUser#MockUser(String, String, String)}
     */
    public MockApplicationUser(final String userKey, final String username, final String displayName, final String email) {
        this(userKey, new MockUser(username, displayName, email));
    }

    public MockApplicationUser(final String userKey, final User user) {
        this.id = SEQUENCE.getAndIncrement();
        this.key = userKey;
        this.user = user;
    }

    /**
     * A {@code MockUser} is always active by default.
     */
    @Override
    public boolean isActive() {
        return active;
    }

    public MockApplicationUser setActive(boolean active) {
        this.active = active;
        if (user instanceof MockUser) {
            ((MockUser) user).setActive(active);
        }
        return this;
    }

    @Override
    public String getKey() {
        return key;
    }

    @Override
    public String getEmailAddress() {
        return user.getEmailAddress();
    }

    @Override
    public String getDisplayName() {
        return user.getDisplayName();
    }

    @Override
    public User getDirectoryUser() {
        return user;
    }

    @Override
    public String getUsername() {
        return user.getName();
    }

    @Override
    public String getName() {
        return user.getName();
    }

    @Override
    public long getDirectoryId() {
        return user.getDirectoryId();
    }

    @Override
    public boolean equals(final Object obj) {
        if (obj instanceof ApplicationUser) {
            final ApplicationUser other = (ApplicationUser) obj;
            return key.equals(other.getKey());
        }
        if (obj instanceof User) {
            throw new IllegalArgumentException("You must update your code to use ApplicationUser (you passed User to equals here)");
        }
        return false;
    }

    @Override
    public int hashCode() {
        return key.hashCode();
    }

    @Override
    public String toString() {
        return getUsername() + '(' + getKey() + ')';
    }

    @Override
    public Long getId() {
        return id;
    }
}
