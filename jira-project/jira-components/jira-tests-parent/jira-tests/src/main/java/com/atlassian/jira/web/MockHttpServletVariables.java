package com.atlassian.jira.web;

import com.atlassian.jira.mock.servlet.MockHttpServletRequest;
import com.atlassian.jira.mock.servlet.MockHttpServletResponse;
import com.atlassian.jira.mock.servlet.MockHttpSession;
import com.atlassian.jira.mock.servlet.MockServletContext;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class MockHttpServletVariables implements HttpServletVariables {
    private MockHttpServletRequest request = new MockHttpServletRequest(new MockHttpSession());
    private MockHttpServletResponse response = new MockHttpServletResponse();
    private MockServletContext context = new MockServletContext();

    @Override
    public HttpServletRequest getHttpRequest() {
        return request;
    }

    @Override
    public HttpSession getHttpSession() {
        return request.getSession();
    }

    @Override
    public HttpServletResponse getHttpResponse() {
        return response;
    }

    @Override
    public ServletContext getServletContext() {
        return context;
    }
}
