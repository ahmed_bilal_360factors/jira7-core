package com.atlassian.jira.cache.vcache;

import com.atlassian.vcache.DirectExternalCache;
import com.atlassian.vcache.ExternalCacheSettings;
import com.atlassian.vcache.JvmCache;
import com.atlassian.vcache.JvmCacheSettings;
import com.atlassian.vcache.Marshaller;
import com.atlassian.vcache.RequestCache;
import com.atlassian.vcache.StableReadExternalCache;
import com.atlassian.vcache.TransactionalExternalCache;
import com.atlassian.vcache.VCacheFactory;
import com.atlassian.vcache.internal.RequestContext;
import com.atlassian.vcache.internal.core.DefaultRequestCache;
import com.atlassian.vcache.internal.core.DefaultRequestContext;

import javax.annotation.Nonnull;
import javax.annotation.OverridingMethodsMustInvokeSuper;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Supplier;

@ParametersAreNonnullByDefault
public class MockVCacheFactory implements VCacheFactory {
    @Nonnull
    @Override
    public <K, V> JvmCache<K, V> getJvmCache(final String s, final JvmCacheSettings jvmCacheSettings) {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Nonnull
    @Override
    public <K, V> RequestCache<K, V> getRequestCache(final String s) {
        return new DefaultRequestCache<>(s, new ThreadAwareContextSupplier());
    }

    @Nonnull
    @Override
    public <V> TransactionalExternalCache<V> getTransactionalExternalCache(final String s, final Marshaller<V> marshaller, final ExternalCacheSettings externalCacheSettings) {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Nonnull
    @Override
    public <V> StableReadExternalCache<V> getStableReadExternalCache(final String cacheName, final Marshaller<V> marshaller, final ExternalCacheSettings externalCacheSettings) {
        return new MockExternalCache<>(cacheName, marshaller, externalCacheSettings);
    }

    @Nonnull
    @Override
    public <V> DirectExternalCache<V> getDirectExternalCache(final String cacheName, final Marshaller<V> marshaller,
                                                             final ExternalCacheSettings externalCacheSettings) {
        return new MockExternalCache<>(cacheName, marshaller, externalCacheSettings);
    }

    static class ThreadAwareContextSupplier implements Supplier<RequestContext> {
        private final ThreadLocal<RequestContext> localRequestContext = new ThreadLocal<RequestContext>(){
            @Override
            protected RequestContext initialValue() {
                return new DefaultRequestContext("staticTenant" + this.hashCode());
            }
        };

        @Override
        public RequestContext get() {
            return localRequestContext.get();
        }
    }
}
