package com.atlassian.jira.mock.servlet;

import javax.servlet.Filter;
import javax.servlet.FilterRegistration;
import javax.servlet.RequestDispatcher;
import javax.servlet.Servlet;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;
import javax.servlet.SessionCookieConfig;
import javax.servlet.SessionTrackingMode;
import javax.servlet.descriptor.JspConfigDescriptor;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Enumeration;
import java.util.EventListener;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

/**
 * A mock implementation of  {@link javax.servlet.ServletContext}
 *
 * @since v4.0
 */
public class MockServletContext implements ServletContext {
    private final Map<String, Object> attributes = new HashMap<String, Object>();
    private final String realPath;

    public MockServletContext() {
        this(null);
    }

    public MockServletContext(String realPath) {
        this.realPath = realPath;
    }

    public ServletContext getContext(final String s) {
        return this;
    }

    public int getMajorVersion() {
        return 0;
    }

    public int getMinorVersion() {
        return 0;
    }

    public String getMimeType(final String s) {
        return null;
    }

    public Set getResourcePaths(final String s) {
        return null;
    }

    public URL getResource(final String s) throws MalformedURLException {
        return null;
    }

    public InputStream getResourceAsStream(final String s) {
        return null;
    }

    public RequestDispatcher getRequestDispatcher(final String s) {
        return null;
    }

    public RequestDispatcher getNamedDispatcher(final String s) {
        return null;
    }

    public Servlet getServlet(final String s) throws ServletException {
        return null;
    }

    public Enumeration getServlets() {
        return null;
    }

    public Enumeration getServletNames() {
        return null;
    }

    public void log(final String s) {
    }

    public void log(final Exception e, final String s) {
    }

    public void log(final String s, final Throwable throwable) {
    }

    public String getRealPath(final String s) {
        return realPath;
    }

    public String getServerInfo() {
        return "JIRA Mock Application Server";
    }

    public String getInitParameter(final String s) {
        return null;
    }

    public Enumeration getInitParameterNames() {
        return null;
    }

    public Object getAttribute(final String s) {
        return attributes.get(s);
    }

    public Enumeration getAttributeNames() {
        final Set<String> set = attributes.keySet();
        return new Vector<String>(set).elements();
    }

    public void setAttribute(final String s, final Object o) {
        attributes.put(s, o);
    }

    public void removeAttribute(final String s) {
        attributes.remove(s);
    }


    public String getServletContextName() {
        return "jira";
    }

    @Override
    public String getContextPath() {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public int getEffectiveMajorVersion() {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public int getEffectiveMinorVersion() {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public boolean setInitParameter(final String name, final String value) {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public ServletRegistration.Dynamic addServlet(final String servletName, final String className) {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public ServletRegistration.Dynamic addServlet(final String servletName, final Servlet servlet) {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public ServletRegistration.Dynamic addServlet(final String servletName, final Class<? extends Servlet> servletClass) {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public <T extends Servlet> T createServlet(final Class<T> clazz) throws ServletException {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public ServletRegistration getServletRegistration(final String servletName) {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public Map<String, ? extends ServletRegistration> getServletRegistrations() {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public FilterRegistration.Dynamic addFilter(final String filterName, final String className) {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public FilterRegistration.Dynamic addFilter(final String filterName, final Filter filter) {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public FilterRegistration.Dynamic addFilter(final String filterName, final Class<? extends Filter> filterClass) {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public <T extends Filter> T createFilter(final Class<T> clazz) throws ServletException {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public FilterRegistration getFilterRegistration(final String filterName) {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public Map<String, ? extends FilterRegistration> getFilterRegistrations() {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public SessionCookieConfig getSessionCookieConfig() {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public void setSessionTrackingModes(final Set<SessionTrackingMode> sessionTrackingModes) {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public Set<SessionTrackingMode> getDefaultSessionTrackingModes() {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public Set<SessionTrackingMode> getEffectiveSessionTrackingModes() {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public void addListener(final String className) {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public <T extends EventListener> void addListener(final T t) {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public void addListener(final Class<? extends EventListener> listenerClass) {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public <T extends EventListener> T createListener(final Class<T> clazz) throws ServletException {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public JspConfigDescriptor getJspConfigDescriptor() {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public ClassLoader getClassLoader() {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public void declareRoles(final String... roleNames) {
        throw new UnsupportedOperationException("Not implemented");
    }
}
