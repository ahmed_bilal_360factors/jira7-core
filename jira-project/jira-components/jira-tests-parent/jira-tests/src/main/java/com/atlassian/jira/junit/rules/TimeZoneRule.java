package com.atlassian.jira.junit.rules;

import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

import java.util.TimeZone;

/**
 * Change TimeZones for unit tests
 *
 * @since v7.2
 */
public class TimeZoneRule extends TestWatcher{

    private TimeZone defaultTimeZoneRule = TimeZone.getDefault();
    private TimeZone timeZone;

    public TimeZoneRule(TimeZone timeZone) {
        this.timeZone = timeZone;
    }

    @Override
    protected void starting(Description description) {
        TimeZone.setDefault(timeZone);
    }

    @Override
    protected void finished(Description description) {
        TimeZone.setDefault(defaultTimeZoneRule);
    }
}
