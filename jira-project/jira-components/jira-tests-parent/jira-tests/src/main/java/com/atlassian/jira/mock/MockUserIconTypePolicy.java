package com.atlassian.jira.mock;

import com.atlassian.jira.avatar.Avatar;
import com.atlassian.jira.icon.IconOwningObjectId;
import com.atlassian.jira.plugin.icon.IconTypePolicy;
import com.atlassian.jira.plugin.icon.UserIconTypePolicy;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.ApplicationUser;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public class MockUserIconTypePolicy extends UserIconTypePolicy implements IconTypePolicy {

    public MockUserIconTypePolicy() {
        super(null, null);
    }

    public MockUserIconTypePolicy(GlobalPermissionManager globalPermissionManager, PermissionManager permissionManager) {
        super(globalPermissionManager, permissionManager);
    }

    @Override
    public boolean userCanView(@Nullable ApplicationUser remoteUser, @Nonnull Avatar icon) {
        return true;
    }

    @Override
    public boolean userCanDelete(@Nullable ApplicationUser remoteUser, @Nonnull Avatar icon) {
        return true;
    }

    @Override
    public boolean userCanCreateFor(@Nullable ApplicationUser remoteUser, @Nonnull IconOwningObjectId owningObjectId) {
        return true;
    }
}
