package com.atlassian.jira.mock;

import com.atlassian.collectors.CollectorsUtil;
import com.atlassian.fugue.Option;
import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.exception.CreateException;
import com.atlassian.jira.exception.RemoveException;
import com.atlassian.jira.issue.IssueConstant;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.issue.issuetype.IssueTypeImpl;
import com.atlassian.jira.issue.priority.Priority;
import com.atlassian.jira.issue.resolution.Resolution;
import com.atlassian.jira.issue.status.Status;
import com.atlassian.jira.issue.status.StatusImpl;
import com.atlassian.jira.mock.ofbiz.MockGenericValue;
import com.atlassian.jira.ofbiz.FieldMap;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.lang.Pair;
import com.google.common.collect.Maps;
import com.google.common.collect.Ordering;
import org.ofbiz.core.entity.GenericValue;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class MockConstantsManager implements ConstantsManager {
    Map<String, IssueType> issueTypes;
    Map<String, Priority> priorities;
    Map<String, Resolution> resolutions;
    Map<String, Status> statuses;

    public MockConstantsManager() {
        issueTypes = Maps.newHashMap();
        priorities = Maps.newHashMap();
        resolutions = Maps.newHashMap();
        statuses = Maps.newHashMap();
    }

    public Status getStatus(String id) {
        return statuses.get(id);
    }

    @Nonnull
    public Collection<Status> getStatuses() {
        return statuses.values();
    }

    public void refreshStatuses() {
        throw new UnsupportedOperationException();
    }

    @Nullable
    public List<IssueConstant> convertToConstantObjects(String constantType, Collection<?> ids) {
        return null;
    }

    public boolean constantExists(String constantType, String name) {
        return false;
    }

    public IssueType insertIssueType(String name, Long sequence, String style, String description, String iconurl) {
        Long id = findNextIdFor(issueTypes);
        final MockGenericValue issueType = new MockGenericValue("IssueType",
                FieldMap.build("id", id)
                        .add("name", name)
                        .add("sequence", sequence)
                        .add("style", style)
                        .add("description", description)
                        .add("iconurl", iconurl)
        );
        addIssueType(issueType);
        return new IssueTypeImpl(issueType, null, null, null, null);
    }

    @Override
    public IssueType insertIssueType(final String name, final Long sequence, final String style, final String description, final Long avatarId)
            throws CreateException {
        Long id = findNextIdFor(issueTypes);
        final MockGenericValue issueType = new MockGenericValue("IssueType",
                FieldMap.build("id", id)
                        .add("name", name)
                        .add("sequence", sequence)
                        .add("style", style)
                        .add("description", description)
                        .add(IssueTypeImpl.AVATAR_FIELD, avatarId)
        );
        addIssueType(issueType);
        return new IssueTypeImpl(issueType, null, null, null, null);
    }

    private static Long findNextIdFor(Map<String, ? extends IssueConstant> issueTypes) {
        long maxId = 0L;
        for (IssueConstant genericValue : issueTypes.values()) {
            Long id = Long.valueOf(genericValue.getId());
            if (id > maxId)
                maxId = id;
        }
        return maxId + 1;
    }

    public void validateCreateIssueType(String name, String style, String description, String iconurl, ErrorCollection errors, String nameFieldName) {

    }

    @Override
    public void validateCreateIssueTypeWithAvatar(final String name, final String style, final String description, final String avatarId, final ErrorCollection errors, final String nameFieldName) {

    }

    public void updateIssueType(String id, String name, Long sequence, String style, String description, String iconurl) {
        throw new UnsupportedOperationException();
    }

    public void updateIssueType(String id, String name, Long sequence, String style, String description, Long avatarId) {
        throw new UnsupportedOperationException();
    }

    public void removeIssueType(String id) throws RemoveException {

    }

    public IssueConstant getConstantByNameIgnoreCase(final String constantType, final String name) {
        return null;
    }

    public IssueConstant getIssueConstantByName(String constantType, String name) {
        return null;
    }

    public List<IssueType> getEditableSubTaskIssueTypes() {
        return Collections.emptyList();
    }

    public List<String> getAllIssueTypeIds() {
        return Collections.emptyList();
    }

    public IssueConstant getIssueConstant(GenericValue issueConstantGV) {
        throw new UnsupportedOperationException("Not implemented.");
    }

    public void storeIssueTypes(List<GenericValue> issueTypes) {
        throw new UnsupportedOperationException();
    }

    public void refresh() {
        throw new UnsupportedOperationException();
    }

    @Override
    public Option<Pair<String, ErrorCollection.Reason>> validateName(final String name, Option<IssueType> issueType) {
        return Option.none();
    }

    public void invalidateAll() {
        throw new UnsupportedOperationException();
    }

    public void invalidate(final IssueConstant issueConstant) {
        throw new UnsupportedOperationException("Not implemented");
    }

    // Note: The logic of this method is clearly wrong, as the first "special" ID encountered overrides
    // anything else that might be in the collection.  However, this is how the real one "works", so...
    public List<String> expandIssueTypeIds(Collection<String> issueTypeIds) {
        if (issueTypeIds == null) {
            return Collections.emptyList();
        }

        for (final String issueTypeId : issueTypeIds) {
            if (ALL_STANDARD_ISSUE_TYPES.equals(issueTypeId)) {
                return getConstantIds(getRegularIssueTypeObjects());
            } else if (ALL_SUB_TASK_ISSUE_TYPES.equals(issueTypeId)) {
                return getConstantIds(getSubTaskIssueTypeObjects());
            } else if (ALL_ISSUE_TYPES.equals(issueTypeId)) {
                return getAllIssueTypeIds();
            }
        }

        return new ArrayList<>(issueTypeIds);
    }

    @Nonnull
    public Collection<Priority> getPriorities() {
        List<Priority> priorityList = new ArrayList<Priority>(priorities.values());
        return Ordering.from(new Comparator<IssueConstant>() {
            @Override
            public int compare(final IssueConstant issueConstant, final IssueConstant t1) {
                return issueConstant.getSequence().compareTo(t1.getSequence());
            }
        }).immutableSortedCopy(priorityList);
    }

    public String getPriorityName(String id) {
        return null;
    }

    public Priority getPriorityObject(String id) {
        throw new UnsupportedOperationException("Not implemented.");
    }

    public Priority getDefaultPriority() {
        return null;
    }

    public Priority getDefaultPriorityObject() {
        return null;
    }

    public void refreshPriorities() {
        throw new UnsupportedOperationException();
    }

    public Collection<Resolution> getResolutions() {
        return resolutions.values();
    }

    public Collection<Resolution> getResolutionObjects() {
        return getResolutions();
    }

    public Resolution getResolution(String id) {
        return resolutions.get(id);
    }

    public Resolution getResolutionObject(String id) {
        return getResolution(id);
    }

    public void refreshResolutions() {
        throw new UnsupportedOperationException();
    }

    public IssueType getIssueType(String id) {
        return issueTypes.get(id);
    }

    public void refreshIssueTypes() {
        throw new UnsupportedOperationException();
    }

    public IssueConstant getConstantObject(String constantType, String id) {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    public Collection<? extends IssueConstant> getConstantObjects(String constantType) {
        return Collections.emptyList();
    }

    public void addIssueType(GenericValue type) {
        issueTypes.put(type.getString("id"), new IssueTypeImpl(type, null, null, null, null));
    }

    public void addIssueType(IssueType type) {
        issueTypes.put(type.getId(), type);
    }

    public void addStatus(GenericValue status) {
        statuses.put(status.getString("id"), new StatusImpl(status, null, null, null, null));
    }

    public Collection<IssueType> getAllIssueTypeObjects() {
        return issueTypes.values();
    }

    public Collection<IssueType> getRegularIssueTypeObjects() {
        return issueTypes.values().stream()
                .filter(issueType -> !issueType.isSubTask())
                .collect(Collectors.toList());
    }

    @Nonnull
    public Collection<IssueType> getSubTaskIssueTypeObjects() {
        return issueTypes.values().stream()
                .filter(IssueType::isSubTask)
                .collect(Collectors.toList());
    }

    public Status getStatusByName(String name) {
        return null;
    }

    public Status getStatusByNameIgnoreCase(String name) {
        return null;
    }

    public Status getStatusByTranslatedName(String name) {
        return null;
    }

    @Nonnull
    @Override
    public List<IssueConstant> getConstantsByIds(@Nonnull final CONSTANT_TYPE constantType, @Nonnull final Collection<String> ids) {
        if (constantType == CONSTANT_TYPE.ISSUE_TYPE) {
            return getAllIssueTypeObjects().stream()
                    .filter(issueType -> ids.contains(issueType.getId()))
                    .collect(Collectors.toList());
        }
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public void recalculateIssueTypeSequencesAndStore(final List<IssueType> issueTypes) {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public void recalculatePrioritySequencesAndStore(final List<Priority> priorities) {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public void recalculateStatusSequencesAndStore(final List<Status> statuses) {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public void recalculateResolutionSequencesAndStore(final List<Resolution> resolutions) {
        throw new UnsupportedOperationException("Not implemented");
    }

    private static <T extends IssueConstant> List<String> getConstantIds(Collection<T> constants) {
        return constants.stream()
                .map(IssueConstant::getId)
                .collect(CollectorsUtil.toNewArrayListWithSizeOf(constants));
    }
}
