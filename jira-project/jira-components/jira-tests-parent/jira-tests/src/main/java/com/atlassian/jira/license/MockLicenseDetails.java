package com.atlassian.jira.license;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.extras.api.LicenseType;
import com.atlassian.extras.api.jira.JiraLicense;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.web.util.OutlookDate;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collection;
import java.util.Date;

/**
 * @since v6.2.3
 */
public class MockLicenseDetails implements LicenseDetails {
    private boolean evaluation;
    private int daysToExpiry = Integer.MAX_VALUE;
    private int daysMaintenanceExpiry = Integer.MAX_VALUE;
    private boolean developer;
    private LicensedApplications licensedApplications = new MockLicensedApplications();
    private String briefDescription = "JIRA For Unit Tests";
    private String sen = "SEN123";
    private Date maintenanceDate;
    private String licenseString;

    @Override
    public int getLicenseVersion() {
        return 2;
    }

    @Override
    public boolean isEntitledToSupport() {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public boolean isLicenseAlmostExpired() {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public JiraLicense getJiraLicense() {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Nonnull
    @Override
    public LicensedApplications getLicensedApplications() {
        return licensedApplications;
    }

    @Override
    public boolean hasApplication(@Nonnull ApplicationKey application) {
        return licensedApplications.getKeys().contains(application);
    }

    public MockLicenseDetails setLicensedApplications(LicensedApplications licensedApplications) {
        this.licensedApplications = licensedApplications;
        return this;
    }

    @Override
    public LicenseStatusMessage getLicenseStatusMessage(final I18nHelper i18n, UserManager userManager) {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public LicenseStatusMessage getMaintenanceMessage(@Nonnull final I18nHelper i18n, final String applicationName) {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public String getLicenseStatusMessage(@Nullable final ApplicationUser user, final String delimiter, UserManager userManager) {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public String getLicenseStatusMessage(final I18nHelper i18n, @Nullable final OutlookDate ignored, final String delimiter, UserManager userManager) {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public String getLicenseExpiryStatusMessage(@Nullable final ApplicationUser user) {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public String getLicenseExpiryStatusMessage(final I18nHelper i18n, @Nullable final OutlookDate ignored) {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public String getBriefMaintenanceStatusMessage(final I18nHelper i18n) {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Nullable
    @Override
    public Date getMaintenanceExpiryDate() {
        return maintenanceDate;
    }

    @Override
    public String getMaintenanceEndString(final OutlookDate outlookDate) {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public boolean isUnlimitedNumberOfUsers() {
        return false;
    }

    @Override
    public String getDescription() {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public String getPartnerName() {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public boolean isExpired() {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public String getPurchaseDate(final OutlookDate outlookDate) {
        throw new UnsupportedOperationException("Not implemented");
    }

    public MockLicenseDetails setEvaluation(boolean eval) {
        this.evaluation = eval;
        return this;
    }

    @Override
    public boolean isEvaluation() {
        return evaluation;
    }

    @Override
    public boolean isStarter() {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public boolean isPaidType() {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public boolean isCommercial() {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public boolean isPersonalLicense() {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public boolean isCommunity() {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public boolean isOpenSource() {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public boolean isNonProfit() {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public boolean isDemonstration() {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public boolean isOnDemand() {
        throw new UnsupportedOperationException("Not implemented");
    }


    @Override
    public boolean isDataCenter() {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public boolean isEnterpriseLicenseAgreement() {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public boolean isDeveloper() {
        return developer;
    }

    public MockLicenseDetails setDeveloper(boolean developer) {
        this.developer = developer;
        return this;
    }

    @Override
    public String getOrganisation() {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public String getLicenseString() {
        return licenseString;
    }

    @Override
    public boolean isMaintenanceValidForBuildDate(final Date currentBuildDate) {
        return maintenanceDate.compareTo(currentBuildDate) >= 0;
    }

    @Override
    public String getSupportEntitlementNumber() {
        return sen;
    }

    @Override
    public Collection<LicenseContact> getContacts() {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public int getDaysToLicenseExpiry() {
        return daysToExpiry;
    }

    public MockLicenseDetails setDaysToLicenseExpiry(int days) {
        this.daysToExpiry = days;
        return this;
    }

    @Override
    public int getDaysToMaintenanceExpiry() {
        return daysMaintenanceExpiry;
    }

    @Override
    public LicenseType getLicenseType() {
        return null;
    }

    @Nonnull
    @Override
    public String getApplicationDescription() {
        return briefDescription;
    }

    public MockLicenseDetails setBriefDescription(String description) {
        briefDescription = description;
        return this;
    }

    public MockLicenseDetails setDaysToMaintenanceExpiry(final int days) {
        this.daysMaintenanceExpiry = days;
        return this;
    }

    public MockLicenseDetails setSupportEntitlementNumber(@Nonnull String sen) {
        this.sen = sen;
        return this;
    }

    public MockLicenseDetails setMaintenanceDate(Date maintenanceDate) {
        this.maintenanceDate = maintenanceDate;
        return this;
    }

    @Override
    public String toString() {
        return String.format("License[%s]", licensedApplications);
    }

    public MockLicenseDetails setLicenseString(final String licensString) {
        this.licenseString = licensString;
        return this;
    }

    public MockLicenseDetails setLicensedApplications(ApplicationKey... keys) {
        final MockLicensedApplications mockLicensedApplications = new MockLicensedApplications(keys);
        return this.setLicensedApplications(mockLicensedApplications);
    }
}
