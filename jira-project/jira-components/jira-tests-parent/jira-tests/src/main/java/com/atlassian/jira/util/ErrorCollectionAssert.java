package com.atlassian.jira.util;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

import java.util.List;
import java.util.Map;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

/**
 * Provides methods to help unit tests make assertions about ErrorCollection objects.
 *
 * @since v3.13
 */
public class ErrorCollectionAssert {
    private static final List<String> NO_ERROR_MESSAGES = ImmutableList.of();
    private static final Map<String, String> NO_FIELD_ERRORS = ImmutableMap.of();

    /**
     * Asserts that the given ErrorCollection contains only the given error message.
     *
     * @param errorCollection      ErrorCollection under test.
     * @param expectedErrorMessage The expected error message.
     */
    public static void assert1ErrorMessage(final ErrorCollection errorCollection, final String expectedErrorMessage) {
        assertThat("Expected to find exactly one global error message", errorCollection.getErrorMessages(), contains(expectedErrorMessage));
        assertEquals("Expected only a single global error, but also found a field-specific error.", NO_FIELD_ERRORS, errorCollection.getErrors());
    }

    /**
     * Asserts that the given ErrorCollection contains only the given field-specific error.
     *
     * @param errorCollection      ErrorCollection under test.
     * @param fieldName            The field name that we expected the error for.
     * @param expectedErrorMessage The expected error message.
     */
    public static void assert1FieldError(final ErrorCollection errorCollection, final String fieldName, final String expectedErrorMessage) {
        assertEquals("Expected only a field error, but also found a global error", NO_ERROR_MESSAGES, errorCollection.getErrorMessages());
        assertFieldError(errorCollection, fieldName, expectedErrorMessage);
    }

    /**
     * Asserts that the given ErrorCollection contains the given field-specific error.
     *
     * @param errorCollection      ErrorCollection under test.
     * @param fieldName            The field name that we expected the error for.
     * @param expectedErrorMessage The expected error message.
     */
    public static void assertFieldError(final ErrorCollection errorCollection, final String fieldName, final String expectedErrorMessage) {
        assertThat("Error for field " + fieldName, errorCollection.getErrors().get(fieldName), is(expectedErrorMessage));
        assertTrue("hasAnyErrors()", errorCollection.hasAnyErrors());
    }

    /**
     * Asserts that the given ErrorCollection has no errors.
     *
     * @param errorCollection ErrorCollection under test.
     */
    public static void assertNoErrors(final ErrorCollection errorCollection) {
        assertEquals("getErrorMessages()", NO_ERROR_MESSAGES, errorCollection.getErrorMessages());
        assertEquals("getErrors()", NO_FIELD_ERRORS, errorCollection.getErrors());
        assertFalse("hasAnyErrors()", errorCollection.hasAnyErrors());
    }
}
