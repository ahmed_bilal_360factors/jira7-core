package com.atlassian.jira.util;

import org.mockito.stubbing.Answer;

import java.util.Optional;

/**
 * Mockito answers
 */
public class AnswerWith {

    public static <X> Answer<X> firstParameter() {
        return invocationOnMock -> (X) invocationOnMock.getArguments()[0];
    }

    public static <F, T> Answer<T> firstParameterMapped(final com.google.common.base.Function<F, T> transformation) {
        return invocationOnMock -> {
            F from = (F) invocationOnMock.getArguments()[0];
            return transformation.apply(from);
        };
    }

    public static <X> Answer<X> mockInstance() {
        return invocationOnMock -> (X) invocationOnMock.getMock();
    }

    public static final Answer<?> EMPTY_OPTIONAL_IF_APPLICABLE = invocation ->
            invocation.getMethod().getReturnType() == Optional.class ? Optional.empty() : null;

    public static <X> Answer<X> throwAssertionError() {
        return invocationOnMock -> {
            throw new AssertionError();
        };
    }
}
