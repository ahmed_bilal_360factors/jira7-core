package com.atlassian.jira.license;

import com.atlassian.collectors.CollectorsUtil;
import com.google.common.collect.ImmutableList;

import javax.annotation.Nonnull;
import java.util.Arrays;
import java.util.List;

/**
 * @since v7.0
 */
public class MockMultiLicenseStore implements MultiLicenseStore {
    private List<String> licenses;
    private String serverId;
    private String evalUser;

    public MockMultiLicenseStore() {
        this.licenses = ImmutableList.of();
    }

    public MockMultiLicenseStore(String... license) {
        this.licenses = ImmutableList.copyOf(license);
    }

    public MockMultiLicenseStore(Iterable<? extends String> license) {
        this.licenses = ImmutableList.copyOf(license);
    }

    @Nonnull
    @Override
    public List<String> retrieve() {
        return licenses;
    }

    @Override
    public void store(@Nonnull final Iterable<String> licenses) {
        this.licenses = ImmutableList.copyOf(licenses);
    }

    public MockMultiLicenseStore store(final String... licenses) {
        this.licenses = ImmutableList.copyOf(licenses);
        return this;
    }

    public MockMultiLicenseStore storeDetails(final LicenseDetails... licenses) {
        this.licenses = Arrays.stream(licenses)
                .map(LicenseDetails::getLicenseString)
                .collect(CollectorsUtil.toImmutableList());

        return this;
    }

    @Override
    public String retrieveServerId() {
        return serverId;
    }

    @Override
    public void storeServerId(final String serverId) {
        this.serverId = serverId;
    }

    @Override
    public void resetOldBuildConfirmation() {
        this.evalUser = null;
    }

    @Override
    public void confirmProceedUnderEvaluationTerms(final String userName) {
        this.evalUser = userName;
    }

    public String getEvalUser() {
        return evalUser;
    }

    @Override
    public void clear() {
        this.licenses = ImmutableList.of();
    }

}
