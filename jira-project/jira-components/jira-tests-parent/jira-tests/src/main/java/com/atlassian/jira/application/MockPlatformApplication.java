package com.atlassian.jira.application;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.application.api.PlatformApplication;

/**
 * @since v7.0
 */
public class MockPlatformApplication extends MockApplication implements PlatformApplication {
    public MockPlatformApplication(final ApplicationKey key) {
        super(key);
    }

    public MockPlatformApplication(final String key) {
        super(key);
    }
}
