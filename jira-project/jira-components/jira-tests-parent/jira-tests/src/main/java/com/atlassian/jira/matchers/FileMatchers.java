package com.atlassian.jira.matchers;

import com.atlassian.jira.util.RuntimeIOException;
import com.google.common.io.Files;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Matchers for java.io.File
 *
 * @since v6.1
 */
public final class FileMatchers {
    private FileMatchers() {
        throw new AssertionError("Don't instantiate me");
    }

    public static Matcher<File> exists() {
        return new TypeSafeMatcher<File>() {
            public File testedFile;

            @Override
            protected boolean matchesSafely(final File file) {
                testedFile = file;
                return file.exists();
            }

            @Override
            public void describeTo(final Description description) {
                description.appendText("that file ");
                description.appendValue(testedFile);
                description.appendText(" exists");
            }
        };
    }

    public static Matcher<File> isFile() {
        return new TypeSafeMatcher<File>() {
            public File testedFile;

            @Override
            protected boolean matchesSafely(final File file) {
                testedFile = file;
                return file.isFile();
            }

            @Override
            public void describeTo(final Description description) {
                description.appendText("that file ");
                description.appendValue(testedFile);
                description.appendText(" is a regular file");
            }
        };
    }

    public static Matcher<File> isDirectory() {
        return new TypeSafeMatcher<File>() {
            public File testedFile;

            @Override
            protected boolean matchesSafely(final File file) {
                testedFile = file;
                return file.isDirectory();
            }

            @Override
            public void describeTo(final Description description) {
                description.appendText("that file ");
                description.appendValue(testedFile);
                description.appendText(" is a directory");
            }
        };
    }

    public static Matcher<File> isEmptyDirectory() {
        return new TypeSafeMatcher<File>() {
            private File testedFile;

            @Override
            protected boolean matchesSafely(final File file) {
                testedFile = file;
                return file.isDirectory() && isEmpty(file);
            }

            private boolean isEmpty(final File file) {
                final File[] files = file.listFiles();
                return (files == null || files.length == 0);
            }

            @Override
            public void describeTo(final Description description) {
                description.appendText("that file ");
                description.appendValue(testedFile);
                description.appendText(" is an empty directory");
            }
        };
    }

    public static Matcher<File> named(final String name) {
        return new TypeSafeMatcher<File>() {
            @Override
            public boolean matchesSafely(final File item) {
                return item.getName().equals(name);
            }

            @Override
            public void describeTo(final Description description) {
                description.appendText("a file named ")
                        .appendValue(name);
            }

            public void describeMismatchSafely(final Description description, final File item) {
                description.appendText("a file named ")
                        .appendValue(name)
                        .appendText(" in ")
                        .appendValue(item.getParentFile());
            }
        };
    }

    public static Matcher<File> hasContent(final String content) {
        return new TypeSafeMatcher<File>() {
            @Override
            protected boolean matchesSafely(final File file) {
                final String fileContent = getFileContent(file);
                return fileContent.equals(content);
            }

            private String getFileContent(final File file) {
                final String fileContent;
                try {
                    fileContent = FileUtils.readFileToString(file, "UTF-8");
                } catch (final IOException e) {
                    throw new RuntimeIOException(e);
                }
                return fileContent;
            }

            @Override
            public void describeTo(final Description description) {
                description.appendText("got file with content ")
                        .appendValue(content);
            }

            @Override
            protected void describeMismatchSafely(final File item, final Description mismatchDescription) {
                mismatchDescription.appendText("file with content ")
                        .appendValue(getFileContent(item));
            }
        };
    }

    public static Matcher<File> sameContents(final File expected) {
        return new TypeSafeMatcher<File>() {
            @Override
            protected boolean matchesSafely(final File item) {
                try {
                    return Files.equal(expected, item);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }

            @Override
            public void describeTo(final Description description) {
                description.appendText("file with same contents as " + expected);
            }

            @Override
            protected void describeMismatchSafely(final File item, final Description mismatchDescription) {
                mismatchDescription.appendText(String.format("content of '%s' not the same as '%s'.", item, expected));
            }
        };
    }

    public static Matcher<File> sameContents(final InputStream expected) {
        return new TypeSafeMatcher<File>() {
            @Override
            protected boolean matchesSafely(final File item) {
                try (FileInputStream actual = new FileInputStream(item)) {
                    return IOUtils.contentEquals(expected, actual);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }

            @Override
            public void describeTo(final Description description) {
                description.appendText("file with same contents as " + expected);
            }

            @Override
            protected void describeMismatchSafely(final File item, final Description mismatchDescription) {
                mismatchDescription.appendText(String.format("content of '%s' not the same as '%s'.", item, expected));
            }
        };
    }
}
