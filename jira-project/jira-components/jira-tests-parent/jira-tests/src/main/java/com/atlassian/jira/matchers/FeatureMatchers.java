package com.atlassian.jira.matchers;

import org.hamcrest.FeatureMatcher;
import org.hamcrest.Matcher;

import java.util.function.Function;

/**
 * A shorthand for building {@link org.hamcrest.FeatureMatcher} classes. This uses an
 * accessor to simplify construction. In most cases, the accessor may be expressed as
 * method reference, which greatly simplifies the code.
 */
public class FeatureMatchers {
    /**
     * Builds a feature matcher using an accessor method to get the feature.
     * <p>
     * Method name is tailored to static importing.
     *
     * @param featureAccessor    a function, that returns the instance of the feature
     *                           from the matched object
     * @param innerMatcher       a matcher for the feature instance
     * @param featureDescription Descriptive text to use in describeTo
     * @param featureName        Identifying text for mismatch message
     * @param <X>                type to by matched by this matcher
     * @param <Y>                type of the feature to be accessed
     * @return a feature matcher
     * @see org.hamcrest.FeatureMatcher
     */
    public static <X, Y> FeatureMatcher<X, Y> hasFeature(final Function<X, Y> featureAccessor,
                                                         final Matcher<Y> innerMatcher,
                                                         final String featureDescription,
                                                         final String featureName) {
        return new FeatureMatcher<X, Y>(innerMatcher, featureDescription, featureName) {
            @Override
            protected Y featureValueOf(final X actual) {
                return featureAccessor.apply(actual);
            }
        };
    }

}
