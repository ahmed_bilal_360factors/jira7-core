package com.atlassian.jira.message;

import com.atlassian.jira.help.MockHelpUrls;
import com.atlassian.jira.mock.security.MockAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.MockBaseUrl;
import com.atlassian.jira.util.NoopI18nFactory;
import com.atlassian.jira.util.NoopI18nHelper;
import com.atlassian.jira.web.util.MockExternalLinkUtil;

import javax.annotation.ParametersAreNonnullByDefault;
import java.util.Locale;

/**
 * Mock implementation of {@link MessageUtil.Factory}, delegating to the following mock objects:
 * <p>
 * {@link NoopI18nFactory}, {@link MockAuthenticationContext}, {@link MockExternalLinkUtil}, {@link MockHelpUrls}
 * and {@link NoopI18nHelper}
 *
 * @since 7.0
 */
@ParametersAreNonnullByDefault
public class MockMessageUtilFactory implements MessageUtil.Factory {
    @Override
    public MessageUtil getNewInstance() {
        return new MessageUtilFactory(new MockAuthenticationContext(null), new NoopI18nFactory(),
                new MockExternalLinkUtil(), new MockHelpUrls(), new MockBaseUrl("base", "static"))
                .getNewInstance();
    }

    @Override
    public MessageUtil getNewInstance(final ApplicationUser user) {
        final NoopI18nFactory i1BeanFactory = new NoopI18nFactory();
        final I18nHelper i18nHelper = i1BeanFactory.getInstance(user);
        final MockAuthenticationContext context = new MockAuthenticationContext(user, i18nHelper);
        return new MessageUtilFactory(context, i1BeanFactory, new MockExternalLinkUtil(), new MockHelpUrls(),
                new MockBaseUrl("base", "static"))
                .getNewInstance(user);
    }

    @Override
    public MessageUtil getNewInstance(final Locale locale) {
        final NoopI18nFactory i1BeanFactory = new NoopI18nFactory();
        final I18nHelper i18nHelper = i1BeanFactory.getInstance(locale);
        final MockAuthenticationContext context = new MockAuthenticationContext(null, i18nHelper);
        return new MessageUtilFactory(context, i1BeanFactory, new MockExternalLinkUtil(), new MockHelpUrls(),
                new MockBaseUrl("base", "static"))
                .getNewInstance(locale);
    }
}