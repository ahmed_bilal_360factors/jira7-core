package com.atlassian.jira.database;

import java.util.Arrays;

/**
 * @since v6.4
 */
@SuppressWarnings("AssignmentToCollectionOrArrayFieldFromParameter")
public class ResultRow {
    private final Object[] values;

    /**
     * Create a row with the given list of column values.
     * <p>
     * Only a partial list of values is required - missing values are treated as NULL
     */
    public ResultRow(Object... values) {
        this.values = values;
    }

    public Object getObject(final int columnIndex) {
        if (columnIndex > values.length) {
            return null;
        }
        return values[columnIndex - 1];
    }

    @Override
    public boolean equals(final Object o) {
        return o instanceof ResultRow && Arrays.deepEquals(values, ((ResultRow)o).values);
    }

    @Override
    public int hashCode() {
        return values != null ? Arrays.hashCode(values) : 0;
    }

    @Override
    public String toString() {
        return Arrays.toString(values);
    }
}
