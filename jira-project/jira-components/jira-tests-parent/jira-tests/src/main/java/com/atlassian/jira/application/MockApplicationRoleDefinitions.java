package com.atlassian.jira.application;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.fugue.Option;
import com.google.common.collect.Maps;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.Map;

@ParametersAreNonnullByDefault
public class MockApplicationRoleDefinitions implements ApplicationRoleDefinitions {
    private Map<ApplicationKey, ApplicationRoleDefinition> defined = Maps.newHashMap();
    private Map<ApplicationKey, ApplicationRoleDefinition> licensed = Maps.newHashMap();

    @Nonnull
    @Override
    public Iterable<ApplicationRoleDefinition> getDefined() {
        return defined.values();
    }

    @Nonnull
    @Override
    public Option<ApplicationRoleDefinition> getDefined(final ApplicationKey key) {
        return Option.option(defined.get(key));
    }

    @Override
    public boolean isDefined(final ApplicationKey key) {
        return getDefined(key).isDefined();
    }

    @Nonnull
    @Override
    public Iterable<ApplicationRoleDefinition> getLicensed() {
        return licensed.values();
    }

    @Override
    public Option<ApplicationRoleDefinition> getLicensed(ApplicationKey key) {
        return Option.option(licensed.get(key));
    }

    @Override
    public boolean isLicensed(final ApplicationKey key) {
        return getLicensed(key).isDefined();
    }

    public void addDefined(ApplicationRoleDefinition definition) {
        defined.put(definition.key(), definition);
    }

    public MockApplicationRoleDefinition addDefined(String key, String name) {
        final MockApplicationRoleDefinition definition = new MockApplicationRoleDefinition(key, name);
        addDefined(definition);
        return definition;
    }

    public void addLicensed(ApplicationRoleDefinition definition) {
        licensed.put(definition.key(), definition);
    }

    public MockApplicationRoleDefinition addLicensed(String key, String name) {
        final MockApplicationRoleDefinition definition = new MockApplicationRoleDefinition(key, name);
        licensed.put(definition.key(), definition);
        return definition;
    }

    public void removeDefinition(ApplicationKey key) {
        defined.remove(key);
    }

    public void removeLicensed(ApplicationKey key) {
        licensed.remove(key);
    }

    public MockApplicationRoleDefinitions removeDefinition(ApplicationRoleDefinition def) {
        defined.remove(def.key());
        return this;
    }

    public MockApplicationRoleDefinitions removeLicensed(ApplicationRoleDefinition def) {
        licensed.remove(def.key());
        return this;
    }
}
