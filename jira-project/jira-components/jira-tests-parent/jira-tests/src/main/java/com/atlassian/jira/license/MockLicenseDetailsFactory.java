package com.atlassian.jira.license;

import com.atlassian.extras.api.LicenseException;
import com.google.common.collect.Maps;

import javax.annotation.Nonnull;
import java.util.Map;
import java.util.function.Function;

/**
 * @since v7.0
 */
public class MockLicenseDetailsFactory implements LicenseDetailsFactory {
    private Map<String, MockLicenseDetails> details = Maps.newHashMap();
    private Function<String, MockLicenseDetails> defaultFunction = MockLicenseDetailsFactory::throwError;

    @Nonnull
    @Override
    public LicenseDetails getLicense(@Nonnull final String licenseString) throws LicenseException {
        final MockLicenseDetails localDetails = details.get(licenseString);
        if (localDetails != null) {
            return localDetails;
        } else {
            return defaultFunction.apply(licenseString);
        }
    }

    @Override
    public boolean isDecodeable(@Nonnull final String licenseString) {
        return details.containsKey(licenseString);
    }

    public MockLicenseDetailsFactory byDefaultReturnLicense() {
        defaultFunction = MockLicenseDetailsFactory::alwaysWork;
        return this;
    }

    public MockLicenseDetailsFactory byDefaultThrowError() {
        defaultFunction = MockLicenseDetailsFactory::throwError;
        return this;
    }

    private static MockLicenseDetails alwaysWork(String licenseString) {
        return new MockLicenseDetails().setLicenseString(licenseString);
    }

    private static MockLicenseDetails throwError(String licenseString) {
        throw new LicenseException(String.format("'%s' is not a valid license.", licenseString));
    }

    public MockLicenseDetails addLicense(final String license) {
        final MockLicenseDetails newLicense = new MockLicenseDetails();
        details.put(license, newLicense);
        return newLicense.setLicenseString(license);
    }
}
