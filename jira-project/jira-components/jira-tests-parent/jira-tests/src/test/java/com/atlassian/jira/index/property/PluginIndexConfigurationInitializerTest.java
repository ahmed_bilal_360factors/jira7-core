package com.atlassian.jira.index.property;

import com.atlassian.jira.plugin.index.EntityPropertyIndexDocumentModuleDescriptor;
import com.atlassian.jira.plugin.util.PluginModuleTrackerFactory;
import com.google.common.collect.ImmutableList;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import java.util.List;

import static org.hamcrest.Matchers.empty;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class PluginIndexConfigurationInitializerTest {
    @Rule
    public MethodRule mockito = MockitoJUnit.rule();
    @Mock
    private PluginModuleTrackerFactory pluginModuleTrackerFactory;
    @Mock
    private com.atlassian.plugin.tracker.PluginModuleTracker<Void, EntityPropertyIndexDocumentModuleDescriptor> moduleTracker;

    private PluginIndexConfigurationInitializer initializer;

    @Before
    public void setUp() throws Exception {
        when(pluginModuleTrackerFactory.create(EntityPropertyIndexDocumentModuleDescriptor.class)).thenReturn(moduleTracker);
        initializer = new PluginIndexConfigurationInitializer(pluginModuleTrackerFactory);
    }

    @Test
    public void shouldInitialiseWhenThereAreNoModules() throws Exception {
        when(moduleTracker.getModuleDescriptors()).thenReturn(ImmutableList.of());
        final List<String> modules = initializer.initialisePluginIndexConfiguration();
        assertThat(modules, empty());
    }

    @Test
    public void shouldInitialiseModules() {
        final EntityPropertyIndexDocumentModuleDescriptor module1 = mock(EntityPropertyIndexDocumentModuleDescriptor.class);
        when(module1.getCompleteKey()).thenReturn("module1");
        final EntityPropertyIndexDocumentModuleDescriptor module2 = mock(EntityPropertyIndexDocumentModuleDescriptor.class);
        when(module2.getCompleteKey()).thenReturn("module2");
        when(moduleTracker.getModuleDescriptors()).thenReturn(ImmutableList.of(module1, module2));

        final List<String> modules = initializer.initialisePluginIndexConfiguration();
        assertThat(modules, Matchers.contains("module1", "module2"));
        verify(module1).init();
        verify(module2).init();
    }
}