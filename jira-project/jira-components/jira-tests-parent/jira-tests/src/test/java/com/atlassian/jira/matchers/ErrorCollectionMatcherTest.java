package com.atlassian.jira.util;

import com.atlassian.jira.util.ErrorCollection.Reason;
import org.junit.Test;

import static com.atlassian.jira.matchers.ErrorCollectionMatcher.hasError;
import static com.atlassian.jira.matchers.ErrorCollectionMatcher.hasErrorMessage;
import static com.atlassian.jira.matchers.ErrorCollectionMatcher.hasErrorMessages;
import static com.atlassian.jira.matchers.ErrorCollectionMatcher.hasErrors;
import static com.atlassian.jira.matchers.ErrorCollectionMatcher.hasNoErrors;
import static com.atlassian.jira.matchers.ErrorCollectionMatcher.hasOnlyError;
import static com.atlassian.jira.matchers.ErrorCollectionMatcher.hasOnlyErrorMessage;
import static org.hamcrest.core.IsNot.not;
import static org.junit.Assert.assertThat;

public class ErrorCollectionMatcherTest {
    @Test
    public void testHasNoErrors() throws Exception {
        assertThat(new SimpleErrorCollection(), hasNoErrors());
        SimpleErrorCollection errors = new SimpleErrorCollection();
        errors.addError("A", "B");
        errors.addError("C", "D");
        assertThat(errors, not(hasNoErrors()));
    }

    @Test
    public void testHasNoErrorMessages() throws Exception {
        assertThat(new SimpleErrorCollection(), hasNoErrors());
        SimpleErrorCollection errors = new SimpleErrorCollection();
        errors.addErrorMessage("A");
        assertThat(errors, not(hasNoErrors()));
    }

    @Test
    public void testHasOnlyErrorMessage() throws Exception {
        SimpleErrorCollection errors = new SimpleErrorCollection();
        errors.addErrorMessage("A", Reason.SERVER_ERROR);
        assertThat(errors, hasOnlyErrorMessage("A", Reason.SERVER_ERROR));
        errors.addErrorMessage("C", Reason.SERVER_ERROR);
        assertThat(errors, not(hasOnlyErrorMessage("A", Reason.SERVER_ERROR)));
    }

    @Test
    public void testHasErrorMessagesWithReasons() throws Exception {
        SimpleErrorCollection errors = new SimpleErrorCollection();
        errors.addErrorMessage("A", Reason.SERVER_ERROR);
        assertThat(errors, hasErrorMessage("A", Reason.SERVER_ERROR));
        errors.addErrorMessage("C", Reason.CONFLICT);
        assertThat(errors, hasErrorMessage("C", Reason.CONFLICT));
    }

    @Test
    public void testHasOnlyError() throws Exception {
        SimpleErrorCollection errors = new SimpleErrorCollection();
        errors.addError("A", "B");
        assertThat(errors, hasOnlyError("A", "B"));
        errors.addError("C", "D");
        assertThat(errors, not(hasOnlyError("A", "B")));
    }

    @Test
    public void testHasError() throws Exception {
        SimpleErrorCollection errors = new SimpleErrorCollection();
        errors.addError("A", "B");
        errors.addError("C", "D");
        assertThat(errors, not(hasError("X", "X")));
        assertThat(errors, hasError("A", "B"));
        assertThat(errors, hasError("C", "D"));
        assertThat(errors, not(hasError("C", "X")));
    }

    @Test
    public void testHasErrorMessage() {
        SimpleErrorCollection errors = new SimpleErrorCollection();
        errors.addErrorMessage("A");
        errors.addErrorMessage("C");
        assertThat(errors, not(hasErrorMessage("X")));
        assertThat(errors, hasErrorMessage("A"));
        assertThat(errors, hasErrorMessage("C"));
    }

    @Test
    public void testHasErrorMessages() {
        SimpleErrorCollection errors = new SimpleErrorCollection();
        errors.addErrorMessage("A");
        errors.addErrorMessage("C");
        assertThat(errors, not(hasErrorMessages("X", "A", "C")));
        assertThat(errors, hasErrorMessages("A", "C"));
        assertThat(errors, hasErrorMessages("C", "A"));
        assertThat(errors, not(hasErrorMessages("A", "C", "F")));
    }

    @Test
    public void testErrorExactMatch() {
        SimpleErrorCollection expectedErrors = new SimpleErrorCollection();
        expectedErrors.addErrorMessage("A");
        expectedErrors.addErrorMessage("C");
        expectedErrors.addError("A", "B");
        expectedErrors.addError("C", "D");
        expectedErrors.addReason(Reason.CONFLICT);
        expectedErrors.addReason(Reason.FORBIDDEN);

        SimpleErrorCollection errors = new SimpleErrorCollection();
        errors.addErrorMessage("A");
        errors.addErrorMessage("C");
        errors.addError("A", "B");
        errors.addError("C", "D");
        errors.addReason(Reason.CONFLICT);
        errors.addReason(Reason.FORBIDDEN);

        assertThat(errors, hasErrors(expectedErrors));
    }

    @Test
    public void testErrorReasonMissing() {
        SimpleErrorCollection expectedErrors = new SimpleErrorCollection();
        expectedErrors.addErrorMessage("A");
        expectedErrors.addErrorMessage("C");
        expectedErrors.addError("A", "B");
        expectedErrors.addError("C", "D");
        expectedErrors.addReason(Reason.CONFLICT);
        expectedErrors.addReason(Reason.FORBIDDEN);

        SimpleErrorCollection errors = new SimpleErrorCollection();
        errors.addErrorMessage("A");
        errors.addErrorMessage("C");
        errors.addError("A", "B");
        errors.addError("C", "D");
        errors.addReason(Reason.FORBIDDEN);

        assertThat(errors, not(hasErrors(expectedErrors)));
    }

    @Test
    public void testErrorFieldMissing() {
        SimpleErrorCollection expectedErrors = new SimpleErrorCollection();
        expectedErrors.addErrorMessage("A");
        expectedErrors.addErrorMessage("C");
        expectedErrors.addError("A", "B");
        expectedErrors.addError("C", "D");
        expectedErrors.addReason(Reason.CONFLICT);
        expectedErrors.addReason(Reason.FORBIDDEN);

        SimpleErrorCollection errors = new SimpleErrorCollection();
        errors.addErrorMessage("A");
        errors.addErrorMessage("C");
        errors.addError("C", "D");
        errors.addReason(Reason.CONFLICT);
        errors.addReason(Reason.FORBIDDEN);

        assertThat(errors, not(hasErrors(expectedErrors)));
    }

    @Test
    public void testErrorMessageMissing() {
        SimpleErrorCollection expectedErrors = new SimpleErrorCollection();
        expectedErrors.addErrorMessage("A");
        expectedErrors.addErrorMessage("C");
        expectedErrors.addError("A", "B");
        expectedErrors.addError("C", "D");
        expectedErrors.addReason(Reason.CONFLICT);
        expectedErrors.addReason(Reason.FORBIDDEN);

        SimpleErrorCollection errors = new SimpleErrorCollection();
        errors.addErrorMessage("C");
        errors.addError("A", "B");
        errors.addError("C", "D");
        errors.addReason(Reason.CONFLICT);
        errors.addReason(Reason.FORBIDDEN);

        assertThat(errors, not(hasErrors(expectedErrors)));
    }

    @Test
    public void testHasErrorMessageFieldAndReason() throws Exception {
        assertThat(new SimpleErrorCollection(), hasNoErrors());
        SimpleErrorCollection errors = new SimpleErrorCollection();
        errors.addError("field", "message");
        errors.addReason(Reason.CONFLICT);
        assertThat(errors, hasError("field", "message", Reason.CONFLICT));
        assertThat(errors, not(hasError("field", "message", Reason.FORBIDDEN)));
        errors.addReason(Reason.FORBIDDEN);
        assertThat(errors, hasError("field", "message", Reason.CONFLICT));
        assertThat(errors, hasError("field", "message", Reason.FORBIDDEN));
        errors.addError("field3", "message3");
        errors.addError("field4", "message4");
        assertThat(errors, hasError("field", "message", Reason.CONFLICT));
        assertThat(errors, hasError("field", "message", Reason.FORBIDDEN));
        assertThat(errors, hasError("field3", "message3", Reason.CONFLICT));
        assertThat(errors, hasError("field4", "message4", Reason.FORBIDDEN));
        assertThat(errors, not(hasError("field4", "message4", Reason.NOT_FOUND)));
    }
}