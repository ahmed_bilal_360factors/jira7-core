package com.atlassian.jira.upgrade.tasks;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.database.MockDbConnectionManager;
import com.atlassian.jira.database.ResultRow;
import com.atlassian.jira.mock.component.MockComponentWorker;
import com.atlassian.jira.util.lang.Pair;
import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableList;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.Map;

import static com.atlassian.jira.util.lang.Pair.of;

public class TestUpgradeTask_Build70024 {
    private static final long FALLBACK_ICON_ID = 999L;
    private final MockDbConnectionManager dbConnectionManager = new MockDbConnectionManager();

    private final UpgradeTask_Build70024 upgradeTask = new UpgradeTask_Build70024(dbConnectionManager);

    private final ResultRow subtask_alternate = new ResultRow("subtask_alternate.png", 1L);
    private final ResultRow task_agile = new ResultRow("task_agile.png", 2L);
    private final ResultRow delete = new ResultRow("delete.png", 3L);
    private final ResultRow blank = new ResultRow("blank.png", 4L);
    private final ResultRow all_unassigned = new ResultRow("all_unassigned.png", 5L);

    private final ResultRow subtask = new ResultRow("subtask.png", 100L);
    private final ResultRow task = new ResultRow("task.png", 200L);
    private final ResultRow remove_issue = new ResultRow("remove_feature.png", 300L);
    private final ResultRow genericissue = new ResultRow("genericissue.png", 400L);

    @Before
    public void setUp() {
        ComponentAccessor.initialiseWorker(new MockComponentWorker());
    }

    @Test
    public void testDoUpgrade() throws Exception {
        final ImmutableList<ResultRow> replacementIcons = ImmutableList.of(subtask, task, remove_issue, genericissue);
        final List<Pair<Long, Long>> issueTypeIconsReplacements =
                ImmutableList.of(of(1L, 100L), of(2L, 200L), of(3L, 300L), of(4L, 400L), of(5L, 400L));

        queryForFallBackIcon();
        queryForIssueTypeIconsToReplace();
        queryForIssueTypeReplacementIcons(replacementIcons);
        queryForIssueTypeAvatarReplacement(issueTypeIconsReplacements);
        queryForDeleteOldIssueTypeIcons();
        queryForUpdateOtherIssueTypeIcons();
        queryForAddNewIssueTypeIcons();
        queryForUpdateDefaultIssueTypeIcons();

        queryForUpdateProjectAvatars();
        queryForAddNewProjectIcons();
        queryForUpdateDefaultProject();

        queryForAddNewUserIcons();
        queryForUpdateUserIcons();

        upgradeTask.doUpgrade(false);

        dbConnectionManager.assertAllExpectedStatementsWereRun();
    }

    @Test
    public void testDoUpgradeShouldUseFallbackIcon() throws Exception {
        final ImmutableList<ResultRow> replacementIcons = ImmutableList.of(subtask, task);
        final List<Pair<Long, Long>> issueTypeIconsReplacementsFallbackCase =
                ImmutableList.of(of(1L, 100L), of(2L, 200L), of(3L, FALLBACK_ICON_ID), of(4L, FALLBACK_ICON_ID), of(5L, FALLBACK_ICON_ID));

        queryForFallBackIcon();
        queryForIssueTypeIconsToReplace();
        queryForIssueTypeReplacementIcons(replacementIcons);
        queryForIssueTypeAvatarReplacement(issueTypeIconsReplacementsFallbackCase);
        queryForDeleteOldIssueTypeIcons();
        queryForUpdateOtherIssueTypeIcons();
        queryForAddNewIssueTypeIcons();
        queryForUpdateDefaultIssueTypeIcons();

        queryForUpdateProjectAvatars();
        queryForAddNewProjectIcons();
        queryForUpdateDefaultProject();

        queryForAddNewUserIcons();
        queryForUpdateUserIcons();

        upgradeTask.doUpgrade(false);

        dbConnectionManager.assertAllExpectedStatementsWereRun();
    }

    @Test
    public void shouldNotUpgradeIssueTypesWhenNoFallbackIcon() throws Exception {
        dbConnectionManager.setQueryResults("select AVATAR.id\n"
                + "from avatar AVATAR\n"
                + "where AVATAR.avatartype = 'issuetype' and AVATAR.systemavatar = 1 and AVATAR.filename = 'undefined.png'\n"
                + "order by AVATAR.id asc\n"
                + "limit 1", ImmutableList.of());

        dbConnectionManager.setQueryResults("select AVATAR.id\n"
                + "from avatar AVATAR\n"
                + "where AVATAR.avatartype = 'issuetype' and AVATAR.systemavatar = 1\n"
                + "limit 1", ImmutableList.of());

        queryForUpdateProjectAvatars();
        queryForAddNewProjectIcons();
        queryForUpdateDefaultProject();

        queryForAddNewUserIcons();
        queryForUpdateUserIcons();

        upgradeTask.doUpgrade(false);

        dbConnectionManager.assertAllExpectedStatementsWereRun();
    }

    private void queryForFallBackIcon() {
        dbConnectionManager.setQueryResults(
                "select AVATAR.id\n"
                        + "from avatar AVATAR\n"
                        + "where AVATAR.avatartype = 'issuetype' and AVATAR.systemavatar = 1 and AVATAR.filename = 'undefined.png'\n"
                        + "order by AVATAR.id asc\n"
                        + "limit 1", ImmutableList.of(new ResultRow(FALLBACK_ICON_ID)));
    }

    private void queryForIssueTypeIconsToReplace() {
        final String elements = Joiner.on("', '").join(UpgradeTask_Build70024.issueTypeIconsToReplace.keySet());
        final String in = "('" + elements + "')";

        dbConnectionManager.setQueryResults(
                "select AVATAR.filename, min(AVATAR.id)\n" +
                        "from avatar AVATAR\n" +
                        "where AVATAR.avatartype = 'issuetype' and AVATAR.systemavatar = 1 and AVATAR.filename in " + in + "\n" +
                        "group by AVATAR.filename", ImmutableList.of(subtask_alternate, task_agile, delete, blank, all_unassigned));
    }

    private void queryForDeleteOldIssueTypeIcons() {
        final String elements = Joiner.on("', '").join(UpgradeTask_Build70024.issueTypeIconsToReplace.keySet());
        final String in = "('" + elements + "')";

        dbConnectionManager.setUpdateResults(
                "delete from avatar\n" +
                        "where avatar.avatartype = 'issuetype' and avatar.systemavatar = 1 and avatar.filename in " + in,
                1);
    }

    private void queryForIssueTypeReplacementIcons(final List<ResultRow> results) {
        final String elements = Joiner.on("', '").join(UpgradeTask_Build70024.issueTypeIconsToReplace.values());
        final String in = "('" + elements + "')";

        dbConnectionManager.setQueryResults(
                "select AVATAR.filename, min(AVATAR.id)\n" +
                        "from avatar AVATAR\n" +
                        "where AVATAR.avatartype = 'issuetype' and AVATAR.systemavatar = 1 and AVATAR.filename in " + in + "\n" +
                        "group by AVATAR.filename", results);
    }

    private void queryForIssueTypeAvatarReplacement(final List<Pair<Long, Long>> replacements) {
        for (Pair<Long, Long> issueTypeIconsReplacement : replacements) {
            final String replacementId = issueTypeIconsReplacement.second().toString();
            final String idToReplace = issueTypeIconsReplacement.first().toString();

            dbConnectionManager.setUpdateResults(
                    "update issuetype\n" +
                            "set avatar = " + replacementId + "\n" +
                            "where issuetype.avatar = " + idToReplace, 1);
        }
    }

    private void queryForUpdateOtherIssueTypeIcons() {
        for (Map.Entry<String, String> renames : UpgradeTask_Build70024.issueTypeIconsToRename.entrySet()) {
            dbConnectionManager.setUpdateResults(
                    "update avatar\n" +
                            "set filename = '" + renames.getValue() + "', contenttype = 'image/svg+xml'\n" +
                            "where avatar.avatartype = 'issuetype' and avatar.systemavatar = 1 and avatar.filename = '" + renames.getKey() + "'",
                    1);
        }
    }

    private void queryForAddNewIssueTypeIcons() {
        for (String iconToAdd : UpgradeTask_Build70024.issueTypeIconsToAdd) {
            dbConnectionManager.setQueryResults(
                    "select count(*)\n" +
                            "from avatar AVATAR\n" +
                            "where AVATAR.avatartype = 'issuetype' and AVATAR.systemavatar = 1 and AVATAR.filename = '" + iconToAdd + "' and AVATAR.contenttype = 'image/svg+xml'",
                    ImmutableList.of(new ResultRow(0L)));

            dbConnectionManager.setUpdateResults(
                    "insert into avatar (filename, contenttype, avatartype, systemavatar, id)\n" +
                            "values ('" + iconToAdd + "', 'image/svg+xml', 'issuetype', 1, 0)",
                    1);
        }
    }

    private void queryForUpdateDefaultIssueTypeIcons() {
        for (Map.Entry<String, String> iconToUpdate : UpgradeTask_Build70024.defaultIssueTypeIconsToRename.entrySet()) {
            final String oldPath = iconToUpdate.getKey();
            final String newPath = iconToUpdate.getValue();

            dbConnectionManager.setUpdateResults(
                    "update issuetype\n" +
                            "set avatar = (select AVATAR.id\n" +
                            "from avatar AVATAR\n" +
                            "where AVATAR.avatartype = 'issuetype' and AVATAR.systemavatar = 1 and AVATAR.filename = '" + newPath + "')\n" +
                            "where issuetype.avatar is null and issuetype.iconurl = '" + oldPath + "'",
                    1);
        }
    }

    private void queryForUpdateProjectAvatars() {
        for (Map.Entry<String, String> iconsToRename : UpgradeTask_Build70024.projectIconsToRename.entrySet()) {
            dbConnectionManager.setUpdateResults(
                    "update avatar\n" +
                            "set filename = '" + iconsToRename.getValue() + "', contenttype = 'image/svg+xml'\n" +
                            "where avatar.avatartype = 'project' and avatar.systemavatar = 1 and avatar.filename = '" + iconsToRename.getKey() + "'",
                    1);

        }
    }

    private void queryForAddNewProjectIcons() {
        for (String iconToAdd : UpgradeTask_Build70024.projectIconsToAdd) {
            dbConnectionManager.setQueryResults(
                    "select count(*)\n" +
                            "from avatar AVATAR\n" +
                            "where AVATAR.avatartype = 'project' and AVATAR.systemavatar = 1 and AVATAR.filename = '" + iconToAdd + "' and AVATAR.contenttype = 'image/svg+xml'",
                    ImmutableList.of(new ResultRow(0L)));

            dbConnectionManager.setUpdateResults(
                    "insert into avatar (filename, contenttype, avatartype, systemavatar, id)\n" +
                            "values ('" + iconToAdd + "', 'image/svg+xml', 'project', 1, 0)",
                    1);
        }
    }

    private void queryForUpdateDefaultProject() {
        final String iconName = UpgradeTask_Build70024.DEFAULT_PROJECT_AVATAR_NAME;
        final long defaultIconId = 1L;
        dbConnectionManager.setQueryResults(
                "select AVATAR.id\n" +
                        "from avatar AVATAR\n" +
                        "where AVATAR.avatartype = 'project' and AVATAR.systemavatar = 1 and AVATAR.filename = '" + iconName + "'\n" +
                        "limit 1",
                ImmutableList.of(new ResultRow(defaultIconId)));

        dbConnectionManager.setUpdateResults(
                "update propertystring\n" +
                        "set propertyvalue = '" + defaultIconId + "'\n" +
                        "where propertystring.id = (select O_S_PROPERTY_ENTRY.id\n" +
                        "from propertyentry O_S_PROPERTY_ENTRY\n" +
                        "where O_S_PROPERTY_ENTRY.property_key = 'jira.avatar.default.id')",
                1);

    }

    private void queryForAddNewUserIcons() {
        for (String iconToAdd : UpgradeTask_Build70024.userIconsToAdd) {
            dbConnectionManager.setQueryResults(
                    "select count(*)\n" +
                            "from avatar AVATAR\n" +
                            "where AVATAR.avatartype = 'user' and AVATAR.systemavatar = 1 and AVATAR.filename = '" + iconToAdd + "' and AVATAR.contenttype = 'image/svg+xml'",
                    ImmutableList.of(new ResultRow(0L)));

            dbConnectionManager.setUpdateResults(
                    "insert into avatar (filename, contenttype, avatartype, systemavatar, id)\n" +
                            "values ('" + iconToAdd + "', 'image/svg+xml', 'user', 1, 0)",
                    1);
        }
    }

    private void queryForUpdateUserIcons() {
        for (Map.Entry<String, String> iconToUpdate : UpgradeTask_Build70024.userIconsToUpdate.entrySet()) {
            final String oldPath = iconToUpdate.getKey();
            final String newPath = iconToUpdate.getValue();

            dbConnectionManager.setUpdateResults(
                    "update avatar\n" +
                            "set filename = '" + newPath + "', contenttype = 'image/svg+xml'\n" +
                            "where avatar.avatartype = 'user' and avatar.systemavatar = 1 and avatar.filename = '" + oldPath + "'",
                    1);
        }
    }
}