package com.atlassian.jira.upgrade.tasks;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.database.MockDbConnectionManager;
import com.atlassian.jira.database.ResultRow;
import com.atlassian.jira.mock.component.MockComponentWorker;
import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableList;
import org.junit.Before;
import org.junit.Test;

import java.util.Map;

public class TestUpgradeTask_Build70024_UpgradedData {
    private final MockDbConnectionManager dbConnectionManager = new MockDbConnectionManager();

    private final UpgradeTask_Build70024 upgradeTask = new UpgradeTask_Build70024(dbConnectionManager);

    @Before
    public void setUp() {
        ComponentAccessor.initialiseWorker(new MockComponentWorker());
    }

    @Test
    public void testUpgradedData() throws Exception {
        queryForFallBackIcon();
        queryForIssueTypeIconsToReplace();
        queryForIssueTypeReplacementIcons();
        queryForDeleteOldIssueTypeIcons();
        queryForUpdateOtherIssueTypeIcons();
        queryForAddNewIssueTypeIcons();
        queryForUpdateDefaultIssueTypeIcons();

        queryForUpdateProjectAvatars();
        queryForAddNewProjectIcons();
        queryForUpdateDefaultProject();

        queryForAddNewUserIcons();
        queryForUpdateUserIcons();

        upgradeTask.doUpgrade(false);

        dbConnectionManager.assertAllExpectedStatementsWereRun();
    }

    private void queryForFallBackIcon() {
        dbConnectionManager.setQueryResults(
                "select AVATAR.id\n"
                        + "from avatar AVATAR\n"
                        + "where AVATAR.avatartype = 'issuetype' and AVATAR.systemavatar = 1 and AVATAR.filename = 'undefined.png'\n"
                        + "order by AVATAR.id asc\n"
                        + "limit 1", ImmutableList.of(new ResultRow(1L)));
    }

    private void queryForIssueTypeIconsToReplace() {
        final String elements = Joiner.on("', '").join(UpgradeTask_Build70024.issueTypeIconsToReplace.keySet());
        final String in = "('" + elements + "')";

        dbConnectionManager.setQueryResults(
                "select AVATAR.filename, min(AVATAR.id)\n" +
                        "from avatar AVATAR\n" +
                        "where AVATAR.avatartype = 'issuetype' and AVATAR.systemavatar = 1 and AVATAR.filename in " + in + "\n" +
                        "group by AVATAR.filename", ImmutableList.of());
    }

    private void queryForDeleteOldIssueTypeIcons() {
        final String elements = Joiner.on("', '").join(UpgradeTask_Build70024.issueTypeIconsToReplace.keySet());
        final String in = "('" + elements + "')";

        dbConnectionManager.setUpdateResults(
                "delete from avatar\n" +
                        "where avatar.avatartype = 'issuetype' and avatar.systemavatar = 1 and avatar.filename in " + in,
                0);
    }

    private void queryForIssueTypeReplacementIcons() {
        final String elements = Joiner.on("', '").join(UpgradeTask_Build70024.issueTypeIconsToReplace.values());
        final String in = "('" + elements + "')";

        dbConnectionManager.setQueryResults(
                "select AVATAR.filename, min(AVATAR.id)\n" +
                        "from avatar AVATAR\n" +
                        "where AVATAR.avatartype = 'issuetype' and AVATAR.systemavatar = 1 and AVATAR.filename in " + in + "\n" +
                        "group by AVATAR.filename", ImmutableList.of());
    }

    private void queryForUpdateOtherIssueTypeIcons() {
        for (Map.Entry<String, String> renames : UpgradeTask_Build70024.issueTypeIconsToRename.entrySet()) {
            dbConnectionManager.setUpdateResults(
                    "update avatar\n" +
                            "set filename = '" + renames.getValue() + "', contenttype = 'image/svg+xml'\n" +
                            "where avatar.avatartype = 'issuetype' and avatar.systemavatar = 1 and avatar.filename = '" + renames.getKey() + "'",
                    0);
        }
    }

    private void queryForAddNewIssueTypeIcons() {
        for (String iconToAdd : UpgradeTask_Build70024.issueTypeIconsToAdd) {
            dbConnectionManager.setQueryResults(
                    "select count(*)\n" +
                            "from avatar AVATAR\n" +
                            "where AVATAR.avatartype = 'issuetype' and AVATAR.systemavatar = 1 and AVATAR.filename = '" + iconToAdd + "' and AVATAR.contenttype = 'image/svg+xml'",
                    ImmutableList.of(new ResultRow(1L)));
        }
    }

    private void queryForUpdateDefaultIssueTypeIcons() {
        for (Map.Entry<String, String> iconToUpdate : UpgradeTask_Build70024.defaultIssueTypeIconsToRename.entrySet()) {
            final String oldPath = iconToUpdate.getKey();
            final String newPath = iconToUpdate.getValue();

            dbConnectionManager.setUpdateResults(
                    "update issuetype\n" +
                            "set avatar = (select AVATAR.id\n" +
                            "from avatar AVATAR\n" +
                            "where AVATAR.avatartype = 'issuetype' and AVATAR.systemavatar = 1 and AVATAR.filename = '" + newPath + "')\n" +
                            "where issuetype.avatar is null and issuetype.iconurl = '" + oldPath + "'",
                    1);
        }
    }

    private void queryForUpdateProjectAvatars() {
        for (Map.Entry<String, String> iconsToRename : UpgradeTask_Build70024.projectIconsToRename.entrySet()) {
            dbConnectionManager.setUpdateResults(
                    "update avatar\n" +
                            "set filename = '" + iconsToRename.getValue() + "', contenttype = 'image/svg+xml'\n" +
                            "where avatar.avatartype = 'project' and avatar.systemavatar = 1 and avatar.filename = '" + iconsToRename.getKey() + "'",
                    0);

        }
    }

    private void queryForAddNewProjectIcons() {
        for (String iconToAdd : UpgradeTask_Build70024.projectIconsToAdd) {
            dbConnectionManager.setQueryResults(
                    "select count(*)\n" +
                            "from avatar AVATAR\n" +
                            "where AVATAR.avatartype = 'project' and AVATAR.systemavatar = 1 and AVATAR.filename = '" + iconToAdd + "' and AVATAR.contenttype = 'image/svg+xml'",
                    ImmutableList.of(new ResultRow(1L)));
        }
    }

    private void queryForUpdateDefaultProject() {
        final String iconName = UpgradeTask_Build70024.DEFAULT_PROJECT_AVATAR_NAME;
        final long defaultIconId = 1L;
        dbConnectionManager.setQueryResults(
                "select AVATAR.id\n" +
                        "from avatar AVATAR\n" +
                        "where AVATAR.avatartype = 'project' and AVATAR.systemavatar = 1 and AVATAR.filename = '" + iconName + "'\n" +
                        "limit 1",
                ImmutableList.of(new ResultRow(defaultIconId)));

        dbConnectionManager.setUpdateResults(
                "update propertystring\n" +
                        "set propertyvalue = '" + defaultIconId + "'\n" +
                        "where propertystring.id = (select O_S_PROPERTY_ENTRY.id\n" +
                        "from propertyentry O_S_PROPERTY_ENTRY\n" +
                        "where O_S_PROPERTY_ENTRY.property_key = 'jira.avatar.default.id')",
                1);

    }

    private void queryForAddNewUserIcons() {
        for (String iconToAdd : UpgradeTask_Build70024.userIconsToAdd) {
            dbConnectionManager.setQueryResults(
                    "select count(*)\n" +
                            "from avatar AVATAR\n" +
                            "where AVATAR.avatartype = 'user' and AVATAR.systemavatar = 1 and AVATAR.filename = '" + iconToAdd + "' and AVATAR.contenttype = 'image/svg+xml'",
                    ImmutableList.of(new ResultRow(1L)));
        }
    }

    private void queryForUpdateUserIcons() {
        for (Map.Entry<String, String> iconToUpdate : UpgradeTask_Build70024.userIconsToUpdate.entrySet()) {
            final String oldPath = iconToUpdate.getKey();
            final String newPath = iconToUpdate.getValue();

            dbConnectionManager.setUpdateResults(
                    "update avatar\n" +
                            "set filename = '" + newPath + "', contenttype = 'image/svg+xml'\n" +
                            "where avatar.avatartype = 'user' and avatar.systemavatar = 1 and avatar.filename = '" + oldPath + "'",
                    1);
        }
    }

}