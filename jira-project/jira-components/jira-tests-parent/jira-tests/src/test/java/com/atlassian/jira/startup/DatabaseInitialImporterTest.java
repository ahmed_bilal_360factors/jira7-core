package com.atlassian.jira.startup;

import com.atlassian.fugue.Option;
import com.atlassian.jira.application.ApplicationRoleManager;
import com.atlassian.jira.bc.dataimport.DataImportParams;
import com.atlassian.jira.bc.dataimport.DataImportService;
import com.atlassian.jira.bc.dataimport.DataImportService.ImportValidationResult;
import com.atlassian.jira.config.util.JiraHome;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.license.MultiLicenseStore;
import com.atlassian.jira.matchers.FileMatchers;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.util.SimpleErrorCollection;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.TemporaryFolder;
import org.junit.rules.TestRule;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.ofbiz.core.entity.DelegatorInterface;
import org.ofbiz.core.entity.GenericEntityException;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import static com.atlassian.jira.bc.dataimport.DataImportService.ImportResult;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.same;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

public class DatabaseInitialImporterTest {
    private static final String PERMISSION_SCHEME_ENTITY_NAME = "PermissionScheme";
    private static final String STARTUPDATABASE_XML = "startupdatabase.xml";

    @Rule
    public TestRule container = MockitoMocksInContainer.forTest(this);

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Rule
    public TemporaryFolder folder = new TemporaryFolder();

    @Mock
    @AvailableInContainer
    private OfBizDelegator delegator;

    @Mock
    private DelegatorInterface delegatorInterface;

    @Mock
    @AvailableInContainer
    private DataImportService dataImportService;

    @Mock
    @AvailableInContainer
    private MultiLicenseStore multiLicenseStore;

    @Mock
    @AvailableInContainer
    private JiraHome jiraHome;

    @Mock
    @AvailableInContainer
    private ApplicationRoleManager applicationRoleManager;

    private DatabaseInitialImporter importer = new DatabaseInitialImporter();
    private File importHome;
    private final MockApplicationUser user = new MockApplicationUser("brenden");

    @Before
    public void setup() throws IOException {
        when(delegator.getDelegatorInterface()).thenReturn(delegatorInterface);
        importHome = folder.newFolder("test");
        when(jiraHome.getImportDirectory()).thenReturn(importHome);
    }

    @Test
    public void dataAlreadyLoadedReturnsTrueWhenDataInDatabase() throws GenericEntityException {
        //given
        when(delegatorInterface.countAll(PERMISSION_SCHEME_ENTITY_NAME)).thenReturn(10);

        //when
        final boolean alreadyLoaded = importer.dataAlreadyLoaded();

        //then
        assertThat(alreadyLoaded, Matchers.equalTo(true));
    }

    @Test
    public void dataAlreadyLoadedReturnsFalseWhenDataInDatabase() throws GenericEntityException {
        //given
        when(delegatorInterface.countAll(PERMISSION_SCHEME_ENTITY_NAME)).thenReturn(0);

        //when
        final boolean alreadyLoaded = importer.dataAlreadyLoaded();

        //then
        assertThat(alreadyLoaded, Matchers.equalTo(false));
    }

    @Test
    public void dataAlreadyLoadedReturnsReThrowsDatabaseError() throws GenericEntityException {
        //given
        final GenericEntityException exception = new GenericEntityException();
        when(delegatorInterface.countAll(PERMISSION_SCHEME_ENTITY_NAME)).thenThrow(exception);

        //then
        this.exception.expect(RuntimeException.class);

        //when
        importer.dataAlreadyLoaded();
    }

    @Test
    public void importInitialDataLicenseAndServerIdProvided() {
        //given
        final ImportValidationResult validation = goodValidation();
        final ImportResult result = goodImport();
        final String serverId = "20";
        final String license = "license";
        final ArgumentCaptor<DataImportParams> argumentCaptor = ArgumentCaptor.forClass(DataImportParams.class);

        when(dataImportService.validateImport(eq(user), argumentCaptor.capture())).thenReturn(validation);
        when(dataImportService.doImport(eq(user), same(validation), any())).thenReturn(result);

        //when
        importer.importInitialData(user, Option.some(serverId), Option.some(license));

        //then
        final DataImportParams importParams = argumentCaptor.getValue();

        verify(dataImportService).validateImport(eq(user), same(importParams));
        verify(dataImportService).doImport(eq(user), same(validation), any());
        verifyImportParams(license, importParams);
        verify(multiLicenseStore).storeServerId(serverId);
    }

    @Test
    public void importInitialDataOnlyLicenseProvided() {
        //given
        final ImportValidationResult validation = goodValidation();
        final ImportResult result = goodImport();
        final String license = "license";
        final ArgumentCaptor<DataImportParams> argumentCaptor = ArgumentCaptor.forClass(DataImportParams.class);

        when(dataImportService.validateImport(eq(user), argumentCaptor.capture())).thenReturn(validation);
        when(dataImportService.doImport(eq(user), same(validation), any())).thenReturn(result);

        //when
        importer.importInitialData(user, Option.none(), Option.some(license));

        //then
        final DataImportParams importParams = argumentCaptor.getValue();

        verify(dataImportService).validateImport(eq(user), same(importParams));
        verify(dataImportService).doImport(eq(user), same(validation), any());
        verifyImportParams(license, importParams);
        verifyNoMoreInteractions(multiLicenseStore);
    }

    @Test
    public void importInitialDataOnlyServerIdProvided() {
        //given
        final ImportValidationResult validation = goodValidation();
        final ImportResult result = goodImport();
        final String serverId = "20";
        final ArgumentCaptor<DataImportParams> argumentCaptor = ArgumentCaptor.forClass(DataImportParams.class);

        when(dataImportService.doImport(eq(user), same(validation), any())).thenReturn(result);
        when(dataImportService.validateImport(eq(user), argumentCaptor.capture())).thenReturn(validation);

        //when
        importer.importInitialData(user, Option.some(serverId), Option.none());

        //then
        final DataImportParams importParams = argumentCaptor.getValue();

        verify(dataImportService).validateImport(eq(user), same(importParams));
        verify(dataImportService).doImport(eq(user), same(validation), any());
        verifyImportParams(null, importParams);
        verify(multiLicenseStore).storeServerId(serverId);
    }

    @Test
    public void importInitialDataNoServerIdOrLicense() {
        //given
        final ImportValidationResult validation = goodValidation();
        final ImportResult result = goodImport();
        final ArgumentCaptor<DataImportParams> argumentCaptor = ArgumentCaptor.forClass(DataImportParams.class);

        when(dataImportService.doImport(eq(user), same(validation), any())).thenReturn(result);
        when(dataImportService.validateImport(eq(user), argumentCaptor.capture())).thenReturn(validation);

        //when
        importer.importInitialData(user, Option.none(), Option.none());

        //then
        final DataImportParams importParams = argumentCaptor.getValue();

        verify(dataImportService).validateImport(eq(user), same(importParams));
        verify(dataImportService).doImport(eq(user), same(validation), any());
        verifyImportParams(null, importParams);
        verifyNoMoreInteractions(multiLicenseStore);
    }

    @Test
    public void importInitialBadImportIgnored() {
        //given
        final String serverId = "id";
        final String license = "license";
        final ImportValidationResult validation = badValidation();
        final ImportResult result = badImport();
        final ArgumentCaptor<DataImportParams> argumentCaptor = ArgumentCaptor.forClass(DataImportParams.class);

        when(dataImportService.doImport(eq(user), same(validation), any())).thenReturn(result);
        when(dataImportService.validateImport(eq(user), argumentCaptor.capture())).thenReturn(validation);

        //when
        importer.importInitialData(user, Option.some(serverId), Option.some(license));

        //then
        final DataImportParams importParams = argumentCaptor.getValue();

        verify(dataImportService).validateImport(eq(user), same(importParams));
        verify(dataImportService).doImport(eq(user), same(validation), any());
        verifyImportParams(license, importParams);
        verify(multiLicenseStore).storeServerId(serverId);
    }

    @Test
    public void importInitialDataUserOnly() {
        //given
        final ImportValidationResult validation = goodValidation();
        final ImportResult result = goodImport();
        final ArgumentCaptor<DataImportParams> argumentCaptor = ArgumentCaptor.forClass(DataImportParams.class);

        when(dataImportService.doImport(eq(user), same(validation), any())).thenReturn(result);
        when(dataImportService.validateImport(eq(user), argumentCaptor.capture())).thenReturn(validation);

        //when
        importer.importInitialData(user);

        //then
        final DataImportParams importParams = argumentCaptor.getValue();

        verify(dataImportService).validateImport(eq(user), same(importParams));
        verify(dataImportService).doImport(eq(user), same(validation), any());
        verifyImportParams(null, importParams);
        verifyNoMoreInteractions(multiLicenseStore);
    }

    private void verifyImportParams(final String license, final DataImportParams importParams) {
        doVerifyImportParams(license, STARTUPDATABASE_XML, importParams);
    }

    private void doVerifyImportParams(final String license, final String databaseXmlFile, final DataImportParams importParams) {
        final File expectedFile = new File(importHome, databaseXmlFile);

        assertThat(importParams.getFilename(), is(databaseXmlFile));
        assertThat(importParams.isSetup(), is(true));
        assertThat(importParams.isAllowDowngrade(), is(false));
        assertThat(importParams.isStartupDataOnly(), is(true));
        assertThat(importParams.isUseDefaultPaths(), is(true));
        assertThat(importParams.isNoLicenseCheck(), is(false));
        assertThat(importParams.isQuickImport(), is(true));
        if (license == null) {
            assertThat(importParams.getLicenseString(), nullValue());
        } else {
            assertThat(importParams.getLicenseString(), is(license));
        }
        assertThat(importParams.getUnsafeJiraBackup(), is(expectedFile));
        assertThat(importParams.getUnsafeAOBackup(), nullValue());
        assertThat(importParams.shouldChangeOutgoingMail(), is(true));
        assertThat(importParams.outgoingMail(), is(true));

        try (InputStream expected = getSourceStream(databaseXmlFile)) {
            assertThat(expectedFile, FileMatchers.sameContents(expected));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private InputStream getSourceStream(final String databaseXmlFile) {
        return DatabaseInitialImporterTest.class.getResourceAsStream("/" + databaseXmlFile);
    }

    private static ImportValidationResult goodValidation() {
        return new ImportValidationResult(new SimpleErrorCollection(), parameters());
    }

    private static ImportValidationResult badValidation() {
        final SimpleErrorCollection errorCollection = new SimpleErrorCollection();
        errorCollection.addErrorMessage("Error");
        return new ImportValidationResult(errorCollection, parameters());
    }

    private static ImportResult goodImport() {
        return new ImportResult.Builder(parameters()).build();
    }

    private static ImportResult badImport() {
        return new ImportResult.Builder(parameters()).addErrorMessage("Error").build();
    }

    private static DataImportParams parameters() {
        return new DataImportParams.Builder("data.xml").build();
    }
}