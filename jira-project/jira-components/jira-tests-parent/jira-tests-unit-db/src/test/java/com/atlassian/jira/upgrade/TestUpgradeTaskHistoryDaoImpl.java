package com.atlassian.jira.upgrade;

import com.atlassian.jira.database.QueryDslAccessor;
import com.atlassian.jira.junit.rules.DatabaseContainer;
import com.atlassian.jira.model.querydsl.UpgradeTaskHistoryAuditLogDTO;
import com.atlassian.jira.model.querydsl.UpgradeTaskHistoryDTO;
import com.atlassian.upgrade.core.dao.UpgradeTaskHistory;
import com.google.common.collect.Lists;
import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import static com.atlassian.jira.model.querydsl.QUpgradeHistory.UPGRADE_HISTORY;
import static com.atlassian.jira.model.querydsl.QUpgradeTaskHistory.UPGRADE_TASK_HISTORY;
import static com.atlassian.jira.model.querydsl.QUpgradeTaskHistoryAuditLog.UPGRADE_TASK_HISTORY_AUDIT_LOG;
import static com.atlassian.upgrade.core.HostUpgradeTaskCollector.HOST_APP_KEY;
import static com.atlassian.upgrade.core.dao.UpgradeTaskHistory.Status.COMPLETED;
import static com.atlassian.upgrade.core.dao.UpgradeTaskHistory.Status.IN_PROGRESS;
import static com.atlassian.upgrade.core.dao.UpgradeTaskHistory.UpgradeType.SERVER;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;

public class TestUpgradeTaskHistoryDaoImpl  {

    private final static Integer UNUSED_ID = null;
    private final static String EXISTING_FACTORY_WITH_TASKS = "original";
    private final static String NEW_FACTORY_KEY_WITH_NO_TASKS = "key";
    private final static Integer originalFirstBuildNumber = 1001;
    private final static Integer originalSecondBuildNumber = 1002;
    private static final int TOTAL_NUMBER_OF_TASKS = 2;

    private Integer originalFirstBuildId;
    private Integer originalSecondBuildId;

    @Rule
    public DatabaseContainer database = DatabaseContainer.rule(this);

    private QueryDslAccessor queryDslAccessor;

    private UpgradeTaskHistoryDaoImpl upgradeTaskHistoryDaoImpl;

    @Before
    public void setUp() throws Exception {
        queryDslAccessor = database.getAttachToDatabase().getQueryDslAccessor();

        upgradeTaskHistoryDaoImpl = new UpgradeTaskHistoryDaoImpl(queryDslAccessor);

        originalFirstBuildId = addUpgradeTaskToHistory(EXISTING_FACTORY_WITH_TASKS, originalFirstBuildNumber, IN_PROGRESS, SERVER);
        originalSecondBuildId = addUpgradeTaskToHistory(EXISTING_FACTORY_WITH_TASKS, originalSecondBuildNumber, IN_PROGRESS, SERVER);
    }

    @Test
    public void createUpgradeTaskHistoryAddsToTheTable() {
        final UpgradeTaskHistory upgradeTaskHistory = new UpgradeTaskHistory(UNUSED_ID, NEW_FACTORY_KEY_WITH_NO_TASKS, 5,
                IN_PROGRESS, SERVER);

        long id = upgradeTaskHistoryDaoImpl.createUpgradeTaskHistory(upgradeTaskHistory);

        List<UpgradeTaskHistoryDTO> upgradeTasks = getAllUpgradeTasks(NEW_FACTORY_KEY_WITH_NO_TASKS);
        assertThat(upgradeTasks, hasSize(1));
        UpgradeTaskHistoryDTO task = upgradeTasks.get(0);
        assertThat(task.getId(), equalTo(id));
        assertThat(task.getBuildNumber(), equalTo(5));
        assertThat(task.getUpgradeTaskFactoryKey(), equalTo(NEW_FACTORY_KEY_WITH_NO_TASKS));
        assertThat(task.getUpgradeType(), equalTo(upgradeTaskHistory.getUpgradeType().toString()));
    }

    @Test
    public void createUpgradeTaskHistoryAddsMultipleUpgradeTasksToTheTable() {
        final UpgradeTaskHistory upgradeTaskHistory1 = new UpgradeTaskHistory(UNUSED_ID, NEW_FACTORY_KEY_WITH_NO_TASKS, 5,
                IN_PROGRESS, SERVER);
        final UpgradeTaskHistory upgradeTaskHistory2 = new UpgradeTaskHistory(UNUSED_ID, NEW_FACTORY_KEY_WITH_NO_TASKS, 6,
                IN_PROGRESS, SERVER);

        long id1 = upgradeTaskHistoryDaoImpl.createUpgradeTaskHistory(upgradeTaskHistory1);
        long id2 = upgradeTaskHistoryDaoImpl.createUpgradeTaskHistory(upgradeTaskHistory2);

        List<UpgradeTaskHistoryDTO> upgradeTasks = getAllUpgradeTasks(NEW_FACTORY_KEY_WITH_NO_TASKS);
        assertThat(upgradeTasks, hasSize(2));
        assertThat(upgradeTasks.get(0).getId(), equalTo(id1));
        assertThat(upgradeTasks.get(1).getId(), equalTo(id2));
    }

    @Test
    public void updateStatusCorrectlyModifiesASingleUpgradeTaskAndUpdatesAuditLog() {
        upgradeTaskHistoryDaoImpl.updateStatus(originalFirstBuildId, COMPLETED);

        List<UpgradeTaskHistoryDTO> upgradeTasks = getAllUpgradeTasks(EXISTING_FACTORY_WITH_TASKS);

        UpgradeTaskHistoryDTO updatedTask = upgradeTasks.get(0);
        assertThat(updatedTask.getStatus(), equalTo(COMPLETED.toString()));
        UpgradeTaskHistoryDTO nonUpdatedTask = upgradeTasks.get(1);
        assertThat(nonUpdatedTask.getStatus(), equalTo(IN_PROGRESS.toString()));

        List<UpgradeTaskHistoryAuditLogDTO> auditLogTasks = getAllAuditLogUpgradeTasks();
        assertThat(auditLogTasks, hasSize(1));
        UpgradeTaskHistoryAuditLogDTO newLogEntry = auditLogTasks.get(0);
        assertThat(newLogEntry, isEquivalentEntry(updatedTask));
        assertThat(newLogEntry.getAction(), equalTo("upgrade"));
    }

    @Test
    public void updateStatusDoesNothingIfEntryDoesNotExistAndLeavesAuditLogUntouched() {
        final Integer unknownId = 9;
        upgradeTaskHistoryDaoImpl.updateStatus(unknownId, COMPLETED);

        List<UpgradeTaskHistoryDTO> upgradeTasks = getAllUpgradeTasks();
        assertThat(upgradeTasks, hasSize(TOTAL_NUMBER_OF_TASKS));
        assertThat(upgradeTasks.get(0).getStatus(), equalTo(IN_PROGRESS.toString()));
        assertThat(upgradeTasks.get(1).getStatus(), equalTo(IN_PROGRESS.toString()));

        List<UpgradeTaskHistoryAuditLogDTO> auditLogTasks = getAllAuditLogUpgradeTasks();
        assertThat(auditLogTasks, hasSize(0));
    }

    @Test
    public void getIdByBuildNumberReturnsEmptyIfNoMatchingUpgradeTaskBuild() {
        final Integer unknownBuildId = 9;
        Optional<Integer> id = upgradeTaskHistoryDaoImpl.getIdByBuildNumber(EXISTING_FACTORY_WITH_TASKS, unknownBuildId);

        assertThat(id.isPresent(), equalTo(false));
    }

    @Test
    public void getIdByBuildNumberReturnsEmptyIfNoMatchingUpgradeTaskFactory() {
        Optional<Integer> id = upgradeTaskHistoryDaoImpl.getIdByBuildNumber("unknown factory", originalFirstBuildNumber);

        assertThat(id.isPresent(), equalTo(false));
    }

    @Test
    public void getIdByBuildNumberReturnsIdIfThereIsAMatchingFactoryBuild() {
        Optional<Integer> id = upgradeTaskHistoryDaoImpl.getIdByBuildNumber(EXISTING_FACTORY_WITH_TASKS, originalFirstBuildNumber);

        assertThat(id.isPresent(), equalTo(true));
        assertThat(id.orElse(-1), equalTo(originalFirstBuildId));
    }

    @Test
    public void getDatabaseBuildNumberReturnsZeroIfNoCompletedUpgradeTasks() {
        int databaseBuildNumber = upgradeTaskHistoryDaoImpl.getDatabaseBuildNumber(EXISTING_FACTORY_WITH_TASKS);

        assertThat(databaseBuildNumber, equalTo(0));
    }

    @Test
    public void getDatabaseBuildNumberReturnsHighestCompletedUpgradeTask() {
        upgradeTaskHistoryDaoImpl.updateStatus(originalFirstBuildId, COMPLETED);
        int databaseBuildNumber = upgradeTaskHistoryDaoImpl.getDatabaseBuildNumber(EXISTING_FACTORY_WITH_TASKS);
        assertThat(databaseBuildNumber, equalTo(originalFirstBuildNumber));

        upgradeTaskHistoryDaoImpl.updateStatus(originalSecondBuildId, COMPLETED);
        databaseBuildNumber = upgradeTaskHistoryDaoImpl.getDatabaseBuildNumber(EXISTING_FACTORY_WITH_TASKS);
        assertThat(databaseBuildNumber, equalTo(originalSecondBuildNumber));
    }

    @Test
    public void getDatabaseBuildNumberReturnsZeroIfNoMatchingFactory() {
        upgradeTaskHistoryDaoImpl.updateStatus(originalFirstBuildId, COMPLETED);
        int databaseBuildNumber = upgradeTaskHistoryDaoImpl.getDatabaseBuildNumber("unknown factory");

        assertThat(databaseBuildNumber, equalTo(0));
    }

    //@Test(expected = IllegalStateException.class)
    @Test
    public void getDatabaseBuildNumberForHostThrowsExceptionIfReturnsZero() {
        int buildNumber = upgradeTaskHistoryDaoImpl.getDatabaseBuildNumber(HOST_APP_KEY);

        //Changed to allow zero as a value so the exception above was not thrown.
        assertThat(buildNumber, equalTo(0));
    }

    @Test
    public void getDatabaseBuildNumberForHostReturnsOldTableBuildNumberIfNewTableEmpty() {
        addUpgradeTaskToOldHistoryTable(72001, "complete");

        int databaseBuildNumber = upgradeTaskHistoryDaoImpl.getDatabaseBuildNumber(HOST_APP_KEY);

        assertThat(databaseBuildNumber, equalTo(72001));
    }

    @Test
    public void getDatabaseBuildNumberForHostReturnsHighestCompletedOldTableBuildNumberIfNewTableEmpty() {
        addUpgradeTaskToOldHistoryTable(71001, "complete");

        addUpgradeTaskToOldHistoryTable(71002, "pending");

        int databaseBuildNumber = upgradeTaskHistoryDaoImpl.getDatabaseBuildNumber(HOST_APP_KEY);

        assertThat(databaseBuildNumber, equalTo(71001));
    }

    @Test
    public void getDatabaseBuildNumberForHostReturnsNewTableValueBeforeOld() {
        addUpgradeTaskToOldHistoryTable(71001, "complete");

        addUpgradeTaskToHistory(HOST_APP_KEY, 100001, COMPLETED, SERVER);

        int databaseBuildNumber = upgradeTaskHistoryDaoImpl.getDatabaseBuildNumber(HOST_APP_KEY);

        assertThat(databaseBuildNumber, equalTo(100001));
    }

    @Test
    public void getDatabaseBuildNumberForHostReturnsCorrectNumericalHighestIfNewTableIsEmpty() {
        addUpgradeTaskToOldHistoryTableWithNoTargetBuildNumber("complete");
        addUpgradeTaskToOldHistoryTableWithNullTargetBuildNumber("complete");
        addUpgradeTaskToOldHistoryTableWithInvalidTargetBuildNumber("complete");
        addUpgradeTaskToOldHistoryTable(90, "complete");
        addUpgradeTaskToOldHistoryTable(71001, "complete");
        addUpgradeTaskToOldHistoryTable(72001, "complete");
        addUpgradeTaskToOldHistoryTable(72002, "complete");
        addUpgradeTaskToOldHistoryTable(100000, "complete");
        addUpgradeTaskToOldHistoryTable(100001, "complete");

        int databaseBuildNumber = upgradeTaskHistoryDaoImpl.getDatabaseBuildNumber(HOST_APP_KEY);

        assertThat(databaseBuildNumber, equalTo(100001));
    }

    @Test
    public void getDatabaseBuildNumberForHostIgnoresMalformedUpgradeClassNames() {
        addUpgradeTaskToOldHistoryTableWithUpgradeClass("com.atlassian.jira.upgrade.tasks.professional.UpgradeTask1_2");
        addUpgradeTaskToOldHistoryTableWithUpgradeClass("com.atlassian.jira.upgrade.tasks.UpgradeTask_Build10");

        int databaseBuildNumber = upgradeTaskHistoryDaoImpl.getDatabaseBuildNumber(HOST_APP_KEY);

        assertThat(databaseBuildNumber, equalTo(10));
    }


    @Test
    public void getDatabaseBuildNumberWhenNoneHaveCompleteStatus() {
        addUpgradeTaskToOldHistoryTable(5006, "");
        addUpgradeTaskToOldHistoryTable(5306, "");
        addUpgradeTaskToOldHistoryTable(6004, "");
        addUpgradeTaskToOldHistoryTable(6010, "");

        int databaseBuildNumber = upgradeTaskHistoryDaoImpl.getDatabaseBuildNumber(HOST_APP_KEY);

        assertThat(databaseBuildNumber, equalTo(6010));
    }

    @Test
    public void getDatabaseBuildNumberWhenNoneHaveTargetBuildNumberSet() {
        addUpgradeTaskToOldHistoryTableWithNoTargetBuildNumber(466);
        addUpgradeTaskToOldHistoryTableWithNoTargetBuildNumber(471);
        addUpgradeTaskToOldHistoryTableWithNoTargetBuildNumber(505);

        int databaseBuildNumber = upgradeTaskHistoryDaoImpl.getDatabaseBuildNumber(HOST_APP_KEY);

        assertThat(databaseBuildNumber, equalTo(505));
    }

    //@Test(expected = IllegalStateException.class)
    @Test
    public void getDatabaseBuildNumberWithNoTargetBuildNumberAndInvalidUpgradeClassFormatThrowsException() {
        addUpgradeTaskToOldHistoryTableWithInvalidTargetBuildNumber("");

        int databaseBuildNumber = upgradeTaskHistoryDaoImpl.getDatabaseBuildNumber(HOST_APP_KEY);

        //This was changed to zero because it was changed to allow zero as a valid host build number.
        assertThat(databaseBuildNumber, equalTo(0));
    }

    @Test
    public void getAllUpgradeTaskHistoryReturnsAnEmptyListWhenNoFactoryEntriesFound() {
        Collection<UpgradeTaskHistory> history = upgradeTaskHistoryDaoImpl.getAllUpgradeTaskHistory("someKey");

        assertThat(history, hasSize(0));
    }

    @Test
    public void getAllUpgradeTaskHistoryReturnsAllEntries() {
        List<UpgradeTaskHistory> history = Lists.newArrayList(upgradeTaskHistoryDaoImpl.getAllUpgradeTaskHistory(EXISTING_FACTORY_WITH_TASKS));

        assertThat(history, hasSize(TOTAL_NUMBER_OF_TASKS));
        assertThat(history.get(0).getId(), equalTo(originalFirstBuildId));
        assertThat(history.get(0).getUpgradeTaskFactoryKey(), equalTo(EXISTING_FACTORY_WITH_TASKS));
        assertThat(history.get(0).getBuildNumber(), equalTo(originalFirstBuildNumber));
        assertThat(history.get(0).getStatus(), equalTo(IN_PROGRESS));
        assertThat(history.get(0).getUpgradeType(), equalTo(SERVER));

        assertThat(history.get(1).getId(), equalTo(originalSecondBuildId));
        assertThat(history.get(1).getUpgradeTaskFactoryKey(), equalTo(EXISTING_FACTORY_WITH_TASKS));
        assertThat(history.get(1).getBuildNumber(), equalTo(originalSecondBuildNumber));
        assertThat(history.get(1).getStatus(), equalTo(IN_PROGRESS));
        assertThat(history.get(1).getUpgradeType(), equalTo(SERVER));
    }

    @Test
    public void getAllUpgradeTaskHistoryDoesNotIncludeOtherFactoryEntries() {
        originalFirstBuildId = addUpgradeTaskToHistory(NEW_FACTORY_KEY_WITH_NO_TASKS, 5, IN_PROGRESS, SERVER);

        List<UpgradeTaskHistory> history = Lists.newArrayList(upgradeTaskHistoryDaoImpl.getAllUpgradeTaskHistory(EXISTING_FACTORY_WITH_TASKS));

        assertThat(history, hasSize(TOTAL_NUMBER_OF_TASKS));
    }

    private int addUpgradeTaskToHistory(final String factoryKey, final int buildNumber, final UpgradeTaskHistory.Status status,
                                        final UpgradeTaskHistory.UpgradeType upgradeType) {
        return queryDslAccessor.executeQuery(dbConnection -> dbConnection
                .insert(UPGRADE_TASK_HISTORY)
                .set(UPGRADE_TASK_HISTORY.upgradeTaskFactoryKey, factoryKey)
                .set(UPGRADE_TASK_HISTORY.buildNumber, buildNumber)
                .set(UPGRADE_TASK_HISTORY.status, status.toString())
                .set(UPGRADE_TASK_HISTORY.upgradeType, upgradeType.toString())
                .executeWithId().intValue());
    }

    private void addUpgradeTaskToOldHistoryTableWithNoTargetBuildNumber(String status) {
        queryDslAccessor.execute(dbConnection -> dbConnection
                .insert(UPGRADE_HISTORY)
                .set(UPGRADE_HISTORY.upgradeclass, "someclasswithnobuildnumber")
                .set(UPGRADE_HISTORY.targetbuild, "")
                .set(UPGRADE_HISTORY.status, status)
                .set(UPGRADE_HISTORY.downgradetaskrequired, "N")
                .executeWithId());
    }

    private void addUpgradeTaskToOldHistoryTableWithNoTargetBuildNumber(int buildNumber) {
        queryDslAccessor.execute(dbConnection -> dbConnection
                .insert(UPGRADE_HISTORY)
                .set(UPGRADE_HISTORY.upgradeclass, "com.atlassian.jira.upgrade.tasks.UpgradeTask_Build" + buildNumber)
                .set(UPGRADE_HISTORY.targetbuild, "")
                .set(UPGRADE_HISTORY.status, "")
                .set(UPGRADE_HISTORY.downgradetaskrequired, "")
                .executeWithId());
    }

    private void addUpgradeTaskToOldHistoryTableWithNullTargetBuildNumber(String status) {
        queryDslAccessor.execute(dbConnection -> dbConnection
                .insert(UPGRADE_HISTORY)
                .set(UPGRADE_HISTORY.upgradeclass, "someclasswithnullbuildnumber")
                .setNull(UPGRADE_HISTORY.targetbuild)
                .set(UPGRADE_HISTORY.status, status)
                .set(UPGRADE_HISTORY.downgradetaskrequired, "N")
                .executeWithId());
    }

    private void addUpgradeTaskToOldHistoryTableWithInvalidTargetBuildNumber(String status) {
        queryDslAccessor.execute(dbConnection -> dbConnection
                .insert(UPGRADE_HISTORY)
                .set(UPGRADE_HISTORY.upgradeclass, "someclasswithinvalidbuildnumber")
                .set(UPGRADE_HISTORY.targetbuild, "not a number")
                .set(UPGRADE_HISTORY.status, status)
                .set(UPGRADE_HISTORY.downgradetaskrequired, "N")
                .executeWithId());
    }

    private void addUpgradeTaskToOldHistoryTableWithUpgradeClass(String upgradeClass) {
        queryDslAccessor.execute(dbConnection -> dbConnection
                .insert(UPGRADE_HISTORY)
                .set(UPGRADE_HISTORY.upgradeclass, upgradeClass)
                .executeWithId());
    }

    private void addUpgradeTaskToOldHistoryTable(int buildNumber, String status) {
        queryDslAccessor.execute(dbConnection -> dbConnection
                .insert(UPGRADE_HISTORY)
                .set(UPGRADE_HISTORY.upgradeclass, "someclass" + buildNumber)
                .set(UPGRADE_HISTORY.targetbuild, String.valueOf(buildNumber))
                .set(UPGRADE_HISTORY.status, status)
                .set(UPGRADE_HISTORY.downgradetaskrequired, "N")
                .executeWithId());
    }

    private List<UpgradeTaskHistoryDTO> getAllUpgradeTasks() {
        return queryDslAccessor.executeQuery(dbConnection -> dbConnection.newSqlQuery()
                .select(UPGRADE_TASK_HISTORY)
                .from(UPGRADE_TASK_HISTORY)
                .orderBy(UPGRADE_TASK_HISTORY.id.asc())
                .fetch());
    }

    private List<UpgradeTaskHistoryDTO> getAllUpgradeTasks(final String factoryKey) {
        return queryDslAccessor.executeQuery(dbConnection -> dbConnection.newSqlQuery()
                .select(UPGRADE_TASK_HISTORY)
                .from(UPGRADE_TASK_HISTORY)
                .where(UPGRADE_TASK_HISTORY.upgradeTaskFactoryKey.eq(factoryKey))
                .orderBy(UPGRADE_TASK_HISTORY.id.asc())
                .fetch());
    }

    private List<UpgradeTaskHistoryAuditLogDTO> getAllAuditLogUpgradeTasks() {
        return queryDslAccessor.executeQuery(dbConnection -> dbConnection.newSqlQuery()
                .select(UPGRADE_TASK_HISTORY_AUDIT_LOG)
                .from(UPGRADE_TASK_HISTORY_AUDIT_LOG)
                .orderBy(UPGRADE_TASK_HISTORY_AUDIT_LOG.timeperformed.asc())
                .fetch());
    }

    static Matcher<UpgradeTaskHistoryAuditLogDTO> isEquivalentEntry(UpgradeTaskHistoryDTO lhs) {
        return new EquivalentEntry(lhs);
    }

    private static class EquivalentEntry extends BaseMatcher<UpgradeTaskHistoryAuditLogDTO> {

        private final UpgradeTaskHistoryDTO lhs;

        EquivalentEntry(UpgradeTaskHistoryDTO lhs) {
            this.lhs = lhs;
        }

        @Override
        public boolean matches(Object item) {
            if ((item == null && lhs != null) || UpgradeTaskHistoryAuditLogDTO.class != item.getClass()) {
                return false;
            }
            UpgradeTaskHistoryAuditLogDTO rhs = (UpgradeTaskHistoryAuditLogDTO) item;
            return lhs.getBuildNumber().equals(rhs.getBuildNumber()) &&
                    lhs.getStatus().equals(rhs.getStatus()) &&
                    lhs.getUpgradeTaskFactoryKey().equals(rhs.getUpgradeTaskFactoryKey()) &&
                    lhs.getUpgradeType().equals(rhs.getUpgradeType());
        }

        @Override
        public void describeTo(Description description) {
            description.appendValue(lhs);
        }
    }
}