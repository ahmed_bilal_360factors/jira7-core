package com.atlassian.jira.datageneration;

import com.atlassian.jira.database.DbConnection;
import com.atlassian.jira.database.QueryCallback;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.concurrent.atomic.AtomicLong;

import static com.atlassian.jira.model.querydsl.QConfigurationContext.CONFIGURATION_CONTEXT;

public class ConfigurationContextGeneration {
    private static final AtomicLong GENERATOR = new AtomicLong(10000L);

    public static Long addConfigurationContext(@Nonnull DbConnection db, @Nullable Long projectId, @Nonnull String key,
                                               long fieldConfigSchemeId) {
        return addConfigurationContext(projectId, key, fieldConfigSchemeId).runQuery(db);
    }

    public static QueryCallback<Long> addConfigurationContext(@Nullable Long projectId, @Nonnull String fieldId,
                                                              long fieldConfigSchemeId) {
        return db -> {
            final Long id = GENERATOR.getAndIncrement();
            db.insert(CONFIGURATION_CONTEXT)
                    .set(CONFIGURATION_CONTEXT.id, id)
                    .set(CONFIGURATION_CONTEXT.project, projectId)
                    .set(CONFIGURATION_CONTEXT.key, fieldId)
                    .set(CONFIGURATION_CONTEXT.fieldconfigscheme, fieldConfigSchemeId)
                    .execute();
            return id;
        };
    }
}
