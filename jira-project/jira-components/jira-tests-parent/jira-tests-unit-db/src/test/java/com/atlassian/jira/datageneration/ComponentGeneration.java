package com.atlassian.jira.datageneration;

import com.atlassian.jira.database.QueryCallback;
import com.atlassian.jira.database.SqlCallback;
import com.atlassian.jira.model.querydsl.ComponentDTO;
import com.atlassian.jira.model.querydsl.NodeAssociationDTO;

import java.util.concurrent.atomic.AtomicLong;

import static com.atlassian.jira.model.querydsl.QComponent.COMPONENT;
import static com.atlassian.jira.model.querydsl.QNodeAssociation.NODE_ASSOCIATION;

public class ComponentGeneration {

    private static AtomicLong id = new AtomicLong(10000L);

    public static QueryCallback<Long> addComponent(Long projectId, String name) {
        return dbConnection -> {
            Long newId = id.getAndIncrement();
            dbConnection.insert(COMPONENT).populate(ComponentDTO.builder()
                    .id(newId)
                    .name(name)
                    .project(projectId)
                    .build()
            ).execute();
            return newId;
        };
    }

    public static SqlCallback assignIssueToComponent(Long issueId, Long componentId) {
        return dbConnection ->
                dbConnection.insert(NODE_ASSOCIATION).populate(NodeAssociationDTO.builder()
                        .sourceNodeId(issueId)
                        .sourceNodeEntity("Issue")
                        .sinkNodeEntity("Component")
                        .associationType("IssueComponent")
                        .sinkNodeId(componentId)
                        .build()
                ).execute();
    }
}
