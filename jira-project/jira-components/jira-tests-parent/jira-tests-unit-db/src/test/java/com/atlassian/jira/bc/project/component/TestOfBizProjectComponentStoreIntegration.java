package com.atlassian.jira.bc.project.component;

import com.atlassian.jira.bc.EntityNotFoundException;
import com.atlassian.jira.junit.rules.DatabaseContainer;
import com.atlassian.jira.ofbiz.FieldMap;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.ofbiz.core.entity.GenericValue;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

/**
 * I copied this test here to have an example of a test which uses OfBizDelegator to read/write the database.
 *
 * @since v7.1
 */
public class TestOfBizProjectComponentStoreIntegration {

    @Rule
    public DatabaseContainer databaseContainer = DatabaseContainer.rule(this);

    OfBizDelegator ofBizDelegator;

    protected ProjectComponentStoreTester tester;
    private static final String COMPONENT_DESCRIPTION = "component description";
    private ProjectComponentStore store;

    @Before
    public void setUp() {
        this.ofBizDelegator = databaseContainer.getAttachToDatabase().getOfBizDelegator();
        this.store = createStore(ofBizDelegator);
        this.tester = new ProjectComponentStoreTester(store);
    }

    @Test
    public void testFind() {
        tester.testFind();
    }

    @Test
    public void testFindAllForProject() {
        tester.testFindAllForProject();
    }

    @Test
    public void testFindByComponentName() {
        tester.testFindByComponentName();
    }

    @Test
    public void testFindProjectIdForComponent() {
        tester.testFindProjectIdForComponent();
    }

    @Test
    public void testDelete() {
        tester.testDelete();
    }

    @Test
    public void testDeleteAll() {
    }

    @Test
    public void testUpdateCommon() {
        tester.testUpdate();
    }

    /**
     * Test that the components description is never set to empty string when creating new components. JRA-12193
     */
    @Test
    public void testDescriptionIsNeverSetToEmptyStringOnCreate() {
        //add a component with description and assert it has not changed
        try {
            MutableProjectComponent component = new MutableProjectComponent(null, "name1", COMPONENT_DESCRIPTION, null, 0, ProjectComponentStoreTester.PROJECT_ID_1);
            assertEquals(COMPONENT_DESCRIPTION, component.getDescription());
            assertNull(COMPONENT_DESCRIPTION, component.getGenericValue());//check its creating new component
            component = store.store(component);
            assertEquals(COMPONENT_DESCRIPTION, component.getDescription());
            assertNotNull(component.getGenericValue());//check its created
            assertEquals(COMPONENT_DESCRIPTION, component.getGenericValue().getString("description"));
        } catch (EntityNotFoundException e) {
            fail();
        }

        //add a component with NULL description and assert it remains NULL
        try {
            MutableProjectComponent component = new MutableProjectComponent(null, "name2", null /* description */, null, 0, ProjectComponentStoreTester.PROJECT_ID_1);
            assertNull(component.getDescription());
            assertNull(component.getGenericValue());//check its creating new component
            component = store.store(component);
            assertNull(component.getDescription());
            assertNotNull(component.getGenericValue());//check its created
            assertNull(component.getGenericValue().getString("description"));
        } catch (EntityNotFoundException e) {
            fail();
        }

        //add a component with no description (empty string "") and assert it is NULL
        try {
            MutableProjectComponent component = new MutableProjectComponent(null, "name3", "" /* description */, null, 0, ProjectComponentStoreTester.PROJECT_ID_1);
            assertNull(component.getDescription());
            assertNull(COMPONENT_DESCRIPTION, component.getGenericValue());//check its creating new component
            component = store.store(component);
            assertNull(component.getDescription());
            assertNotNull(component.getGenericValue());//check its created
            assertNull(component.getGenericValue().getString("description"));
        } catch (EntityNotFoundException e) {
            fail();
        }
    }

    /**
     * Test that the components description is never set to empty string when updating components. JRA-12193
     */
    @Test
    public void testDescriptionIsNeverSetToEmptyStringOnUpdate() {
        //Create a component and continually update the component for the following tests
        MutableProjectComponent existingComponent = new MutableProjectComponent(null, "existing component", COMPONENT_DESCRIPTION, null, 0, ProjectComponentStoreTester.PROJECT_ID_1);
        try {
            existingComponent = store.store(existingComponent);
        } catch (EntityNotFoundException e) {
            fail("Failed to create a new component for testing");
        }

        //update existingComponent with new description
        try {
            assertEquals(COMPONENT_DESCRIPTION, existingComponent.getDescription());
            assertNotNull(COMPONENT_DESCRIPTION, existingComponent.getGenericValue());//check its updating an existing component
            assertEquals(COMPONENT_DESCRIPTION, existingComponent.getGenericValue().getString("description"));
            //set the new component description
            existingComponent.setDescription("EDITED " + COMPONENT_DESCRIPTION);
            existingComponent = store.store(existingComponent);
            assertEquals("EDITED " + COMPONENT_DESCRIPTION, existingComponent.getDescription());
            assertEquals("EDITED " + COMPONENT_DESCRIPTION, existingComponent.getGenericValue().getString("description"));
        } catch (EntityNotFoundException e) {
            fail();
        }

        //update existingComponent with new NULL description
        try {
            assertNotNull(existingComponent.getGenericValue());//check its updating an existing component
            assertNotNull(existingComponent.getGenericValue().getString("description"));
            //set the new component description
            existingComponent.setDescription(null);
            existingComponent = store.store(existingComponent);
            assertNull(existingComponent.getDescription());
            assertNull(existingComponent.getGenericValue().getString("description"));
        } catch (EntityNotFoundException e) {
            fail();
        }

        //update existingComponent with new empty string ("") description
        try {
            assertNotNull(existingComponent.getGenericValue());//check its updating an existing component
            assertNull(existingComponent.getGenericValue().getString("description"));
            //set the new component description
            existingComponent.setDescription("");
            existingComponent = store.store(existingComponent);
            assertNull(existingComponent.getDescription());
            assertNull(existingComponent.getGenericValue().getString("description"));
        } catch (EntityNotFoundException e) {
            fail();
        }
    }

    @Test
    public void testUpdate() {
        MutableProjectComponent component1 = new MutableProjectComponent(null, "name1", "desc", null, 0, ProjectComponentStoreTester.PROJECT_ID_1);
        MutableProjectComponent component2 = new MutableProjectComponent(null, "name1", "desc", null, 0, ProjectComponentStoreTester.PROJECT_ID_1);

        // test that the store does not care about duplicate component name in insert operation
        try {
            store.store(component1);
            store.store(component2);
            // it passess here for OfBiz
        } catch (EntityNotFoundException e) {
            fail();
        }

        // insert valid second component
        try {
            component2.setName("name2");
            store.store(component2);
        } catch (EntityNotFoundException e) {
            fail();
        }

        // test that the store does not care about duplicate component name in update operation
        try {
            component1.setName("name2");
            store.store(component1);
        } catch (EntityNotFoundException e) {
            fail();
        }
    }

    /**
     * Check that the {@link OfBizProjectComponentStore} checks that
     * the component has a non-null name
     */
    @Test
    public void testOfbizProjectComponentStoreValidatesName() {
        MutableProjectComponent component1 = new MutableProjectComponent(null, "name1", "desc", null, 0, ProjectComponentStoreTester.PROJECT_ID_1);

        // test that component must have a name for create
        try {
            component1.setName(null);
            store.store(component1);
            fail("Error: added component with no name");
        } catch (EntityNotFoundException e) {
            fail();
        } catch (IllegalArgumentException e) {
            //should get null name error
        }

        // test that component must have a name for update
        try {
            //first create the component
            store.store(component1);
            //update the component with null name
            component1.setName(null);
            store.store(component1);
            fail("Error: updated component with no name");
        } catch (EntityNotFoundException e) {
            fail();
        } catch (IllegalArgumentException e) {
            //should get null name error
        }
    }

    /**
     * Check that the {@link OfBizProjectComponentStore} checks that
     * the component has a non-null project id
     */
    @Test
    public void testOfbizProjectComponentStoreValidatesProject() {
        //setup a component with no project id
        MutableProjectComponent component1 = new MutableProjectComponent(null, "name1", "desc", null, 0, null);

        // test that component must have a project id
        try {
            store.store(component1);
            fail("Error: added component with no project id");
        } catch (EntityNotFoundException e) {
            fail();
        } catch (IllegalArgumentException e) {
            //should get null name error
        }

        //theres no way to change the project id to test update
    }

    @Test
    public void testContainsName() {
        tester.testContainsName();
    }

    protected ProjectComponentStore createStore(OfBizDelegator ofBizDelegator) {
        return new OfBizProjectComponentStore(ofBizDelegator, null);
    }

}
