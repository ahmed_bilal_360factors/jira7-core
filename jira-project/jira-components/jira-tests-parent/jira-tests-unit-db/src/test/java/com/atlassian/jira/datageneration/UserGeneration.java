package com.atlassian.jira.datageneration;

import com.atlassian.jira.database.QueryCallback;
import com.atlassian.jira.model.querydsl.ApplicationUserDTO;
import com.atlassian.jira.model.querydsl.UserDTO;

import java.util.concurrent.atomic.AtomicLong;

import static com.atlassian.jira.model.querydsl.QApplicationUser.APPLICATION_USER;
import static com.atlassian.jira.model.querydsl.QUser.USER;

public class UserGeneration {

    private static AtomicLong cwdUserId = new AtomicLong(10000);
    // offset application user id by a lot to ensure we don't accidentally rely on ids matching.
    private static AtomicLong applicationUserId = new AtomicLong(20000);

    private UserGeneration() {
    }

    public static QueryCallback<Long> addUser(String username, String displayName) {
        return addRenamedUser(username, username, displayName);
    }

    public static QueryCallback<Long> addRenamedUser(String oldUsername, String newUsername, String displayName) {
        return dbConnection -> {
            long localCwdUserId = cwdUserId.getAndIncrement();
            long localApplicationUserId = applicationUserId.getAndIncrement();

            final UserDTO user = UserDTO.builder()
                    .id(localCwdUserId)
                    .displayName(displayName)
                    .lowerDisplayName(displayName.toLowerCase())
                    .userName(newUsername)
                    .lowerUserName(newUsername.toLowerCase())
                    .build();

            final ApplicationUserDTO appUser = ApplicationUserDTO.builder()
                    .id(localApplicationUserId)
                    .userKey(oldUsername.toLowerCase())
                    .lowerUserName(newUsername.toLowerCase())
                    .build();

            dbConnection.insert(USER).populate(user).execute();
            dbConnection.insert(APPLICATION_USER).populate(appUser).execute();
            return localCwdUserId;
        };
    }

}
