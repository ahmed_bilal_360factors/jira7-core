package com.atlassian.jira.upgrade.tasks;

import com.atlassian.jira.database.DbConnection;
import com.atlassian.jira.database.QueryDslAccessor;
import com.atlassian.jira.junit.rules.DatabaseContainer;
import com.atlassian.jira.model.querydsl.UpgradeHistoryDTO;
import com.atlassian.jira.upgrade.UpgradeTaskHistoryDaoImpl;
import com.atlassian.upgrade.api.UpgradeContext;
import com.atlassian.upgrade.core.HostUpgradeTaskCollector;
import com.atlassian.upgrade.core.dao.UpgradeTaskHistory;
import com.atlassian.upgrade.core.dao.UpgradeTaskHistoryDao;
import com.google.common.collect.ImmutableList;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.util.List;
import java.util.stream.Collectors;

import static com.atlassian.jira.model.querydsl.QUpgradeHistory.UPGRADE_HISTORY;
import static com.atlassian.upgrade.core.dao.UpgradeTaskHistory.Status.COMPLETED;
import static com.atlassian.upgrade.core.dao.UpgradeTaskHistory.Status.IN_PROGRESS;
import static com.atlassian.upgrade.core.dao.UpgradeTaskHistory.UpgradeType.SERVER;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.fail;

public class TestUpgradeTask_Build73010 {
    private final UpgradeContext UPGRADE_CONTEXT = () -> UpgradeContext.UpgradeTrigger.UPGRADE;
    private final String UPGRADE_FACTORY_KEY = HostUpgradeTaskCollector.HOST_APP_KEY;

    @Rule
    public DatabaseContainer container = DatabaseContainer.rule(this);

    private UpgradeTaskHistoryDao upgradeTaskHistoryDao;
    private QueryDslAccessor queryDslAccessor;
    private UpgradeTask_Build73010 upgradeTask;

    private final List<Integer> OLD_UPGRADE_TASKS = ImmutableList.of(
            4000,
            4001,
            5000,
            5300,
            6300,
            6310,
            64001,
            64011,
            70007,
            72001,
            72002
    );

    private static final Integer BUILD_NUMBER = UpgradeTask_Build73010.BUILD_NUMBER;

    @Before
    public void setup() {
        queryDslAccessor = container.getAttachToDatabase().getQueryDslAccessor();

        upgradeTask = new UpgradeTask_Build73010(queryDslAccessor);
        upgradeTaskHistoryDao = new UpgradeTaskHistoryDaoImpl(queryDslAccessor);
    }

    @Test
    public void whenAllUpgradesHadBeenDoneInTheOldFrameworkNothingIsDone() {
        //setup
        allUpgradeTaskDoneInOldFramework();
        addNewUpgradeTaskIsInProgess(BUILD_NUMBER);

        //run
        upgradeTask.runUpgrade(UPGRADE_CONTEXT);

        //post-assert
        assertUpgradeHistoryInCorrectState();
    }

    @Test
    public void whenAllUpgradesHadBeenDoneInTheNewFrameworkAllAreMovedToTheOldTable() {
        //setup
        allUpgradeTasksDoneInNewFramework();
        addNewUpgradeTaskIsInProgess(BUILD_NUMBER);

        //run
        upgradeTask.runUpgrade(UPGRADE_CONTEXT);

        //post-assert
        assertUpgradeHistoryInCorrectState();
    }

    @Test
    public void upgradesDoneInBothAndOldFrameworkResultInThemBeingMovedToTheOldTable() {
        //setup
        upgradesDoneInOldAndThenNewFramework();
        addNewUpgradeTaskIsInProgess(BUILD_NUMBER);

        //run
        upgradeTask.runUpgrade(UPGRADE_CONTEXT);

        //post-assert
        assertUpgradeHistoryInCorrectState();
    }

    @Test
    public void upgradesThatWerePendingInTheOldTableWillGetOverwrittenWithComplete() {
        //setup
        addOldUpgradeTaskHasRun(72000, "pending");
        addNewUpgradeTaskHasRun(72000);
        addNewUpgradeTaskIsInProgess(BUILD_NUMBER);

        //run
        upgradeTask.runUpgrade(UPGRADE_CONTEXT);

        //post-assert
        assertOldTableHasItemsCompleted(72000);
        assertNewTableHasThisBuild();
    }

    @Test
    public void upgradeClassIsProperlyPopulatedAfterMigration() {
        //setup
        allUpgradeTasksDoneInNewFramework();
        addNewUpgradeTaskIsInProgess(BUILD_NUMBER);

        //run
        upgradeTask.runUpgrade(UPGRADE_CONTEXT);

        //post-assert
        assertUpgradeHistoryInCorrectState();

        //Check that the upgrade task classes were generated correctly
        getOldUpgradeTasksRun().stream().forEach(oldUpgradeTask ->
                assertThat(oldUpgradeTask.getUpgradeclass(),
                        equalTo("com.atlassian.jira.upgrade.tasks.UpgradeTask_Build" + oldUpgradeTask.getTargetbuild())));
    }

    @Test
    public void anyUpgradeBeforeMinVersionAreMaintained() {
        addOldUpgradeTaskHasRun(6300);

        //run
        upgradeTask.runUpgrade(UPGRADE_CONTEXT);

        //post-assert
        assertThat(getOldUpgradeTasksRun().stream().filter(upgradeTask -> upgradeTask.getTargetbuild().equals("6300")).count(),
                equalTo(1L));
    }

    @Test
    public void ifInsertingTheNewEntriesIntoTheOldTableFailsTheChangesAreRolledBackSoNoDataLoss() {
        //setup
        upgradeTask = new UpgradeTask_Build73010(queryDslAccessor) {
            @Override
            void addUpgradesToOldTable(DbConnection connection, List<Integer> buildNumbers) {
                throw new RuntimeException("Error inserting into second table");
            }
        };

        allUpgradeTaskDoneInOldFramework();

        //run
        try {
            upgradeTask.runUpgrade(UPGRADE_CONTEXT);
            fail("Should not have successfully upgraded");

            //post-assert
        } catch (Throwable e) {
            assertThat(getOldUpgradeTasksRun(), hasSize(11));
            assertThat(upgradeTaskHistoryDao.getAllUpgradeTaskHistory(UPGRADE_FACTORY_KEY), hasSize(0));
        }
    }

    private List<UpgradeHistoryDTO> getOldUpgradeTasksRun() {
        return getOldUpgradeTasksRun(0);
    }

    private List<UpgradeHistoryDTO> getOldUpgradeTasksRun(int minimumBuildToInclude) {
        return queryDslAccessor.executeQuery(callback -> callback.newSqlQuery()
                .select(UPGRADE_HISTORY)
                .from(UPGRADE_HISTORY)
                .fetch()).stream().filter(upgradeTask -> Integer.parseInt(upgradeTask.getTargetbuild()) >= minimumBuildToInclude)
                .collect(Collectors.toList());

    }

    private void addNewUpgradeTaskHasRun(int buildNumber) {
        upgradeTaskHistoryDao.createUpgradeTaskHistory(new UpgradeTaskHistory(null, UPGRADE_FACTORY_KEY, buildNumber,
                COMPLETED, SERVER));
    }

    private void addNewUpgradeTaskIsInProgess(int buildNumber) {
        upgradeTaskHistoryDao.createUpgradeTaskHistory(new UpgradeTaskHistory(null, UPGRADE_FACTORY_KEY, buildNumber,
                IN_PROGRESS, SERVER));
    }

    private void addOldUpgradeTaskHasRun(int buildNumber) {
        addOldUpgradeTaskHasRun(buildNumber, "complete");
    }

    private void addOldUpgradeTaskHasRun(int buildNumber, String status) {
        queryDslAccessor.execute(callback -> callback
                .insert(UPGRADE_HISTORY)
                .set(UPGRADE_HISTORY.upgradeclass, "com.atlassian.jira.upgrade.tasks.UpgradeTask_Build" + buildNumber)
                .set(UPGRADE_HISTORY.targetbuild, String.valueOf(buildNumber))
                .set(UPGRADE_HISTORY.status, status)
                .set(UPGRADE_HISTORY.downgradetaskrequired, "N")
                .executeWithId());
    }

    private void assertUpgradeHistoryInCorrectState() {
        List<Integer> oldUpgradeTaskBuildIds = getOldUpgradeTasksRun().stream()
                .filter(upgradeTask -> upgradeTask.getStatus().equals("complete"))
                .filter(upgradeTask -> upgradeTask.getDowngradetaskrequired().equals("N"))
                .map(UpgradeHistoryDTO::getTargetbuild)
                .map(Integer::parseInt)
                .collect(Collectors.toList());

        assertThat("Should have all of the old upgrade tasks in the old table", oldUpgradeTaskBuildIds, hasSize(OLD_UPGRADE_TASKS.size()));

        OLD_UPGRADE_TASKS.forEach(buildNumber -> {
            assertThat("Build " + buildNumber + " should be in the old upgrade table", oldUpgradeTaskBuildIds, hasItem(buildNumber));
        });

        assertNewTableHasThisBuild();
    }

    private void assertNewTableHasThisBuild() {
        List<Integer> newUpgradeTaskBuildIds = upgradeTaskHistoryDao.getAllUpgradeTaskHistory(UPGRADE_FACTORY_KEY).stream()
                .filter(upgradeTask -> upgradeTask.getUpgradeType().equals(SERVER))
                .map(UpgradeTaskHistory::getBuildNumber)
                .collect(Collectors.toList());

        assertThat("Should only have build 73000 in the UpgradeTaskHistory table", newUpgradeTaskBuildIds, contains(BUILD_NUMBER));
    }

    private void assertOldTableHasItemsCompleted(int... buildNumbers) {

        List<Integer> oldUpgradeTaskBuildIds = getOldUpgradeTasksRun().stream()
                .filter(upgradeTask -> upgradeTask.getStatus().equals("complete"))
                .filter(upgradeTask -> upgradeTask.getDowngradetaskrequired().equals("N"))
                .map(UpgradeHistoryDTO::getTargetbuild)
                .map(Integer::parseInt)
                .collect(Collectors.toList());

        assertThat("Should have all of the old upgrade tasks in the old table", oldUpgradeTaskBuildIds, hasSize(buildNumbers.length));

        for (int buildNumber : buildNumbers) {
            assertThat("Build " + buildNumber + " should be in the old upgrade table", oldUpgradeTaskBuildIds, hasItem(buildNumber));
        }
    }

    private void allUpgradeTaskDoneInOldFramework() {
        OLD_UPGRADE_TASKS.forEach(this::addOldUpgradeTaskHasRun);
    }

    private void allUpgradeTasksDoneInNewFramework() {
        OLD_UPGRADE_TASKS.forEach(this::addNewUpgradeTaskHasRun);
    }

    private void upgradesDoneInOldAndThenNewFramework() {
        for (int i = 0; i < OLD_UPGRADE_TASKS.size(); ++i) {
            final Integer buildNumber = OLD_UPGRADE_TASKS.get(i);

            if (i < (OLD_UPGRADE_TASKS.size() / 2)) {
                addOldUpgradeTaskHasRun(buildNumber);
            } else {
                addNewUpgradeTaskHasRun(buildNumber);
            }
        }
    }
}
