package com.atlassian.jira.datageneration;

import com.atlassian.jira.database.SqlCallback;
import com.atlassian.jira.model.querydsl.NodeAssociationDTO;

import static com.atlassian.jira.model.querydsl.QNodeAssociation.NODE_ASSOCIATION;

public class NodeAssociationGeneration {

    public static SqlCallback addNodeAssociation(String sourceNodeEntity, Long sourceNodeId, String sinkNodeEntity, Long sinkNodeId, String associationType) {
        return dbConnection -> {
            NodeAssociationDTO nodeAssociation = NodeAssociationDTO.builder()
                    .sourceNodeId(sourceNodeId)
                    .sourceNodeEntity(sourceNodeEntity)
                    .sinkNodeEntity(sinkNodeEntity)
                    .associationType(associationType)
                    .sinkNodeId(sinkNodeId)
                    .build();
            dbConnection.insert(NODE_ASSOCIATION).populate(nodeAssociation).execute();
        };
    }
}
