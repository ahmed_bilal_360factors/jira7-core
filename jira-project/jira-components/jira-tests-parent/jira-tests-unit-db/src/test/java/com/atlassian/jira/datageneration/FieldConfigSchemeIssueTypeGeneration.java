package com.atlassian.jira.datageneration;

import com.atlassian.jira.database.DbConnection;
import com.atlassian.jira.database.QueryCallback;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.concurrent.atomic.AtomicLong;

import static com.atlassian.jira.model.querydsl.QFieldConfigSchemeIssueType.FIELD_CONFIG_SCHEME_ISSUE_TYPE;

public class FieldConfigSchemeIssueTypeGeneration {
    private static final AtomicLong GENERATOR = new AtomicLong(4000L);

    public static Long addFieldConfigSchemeIssueType(@Nonnull DbConnection db, long fieldConfigSchemeId,
                                                     long fieldConfigId) {
        return addFieldConfigSchemeIssueType(db, null, fieldConfigSchemeId, fieldConfigId);
    }

    public static Long addFieldConfigSchemeIssueType(@Nonnull DbConnection db, @Nullable String issueTypeId,
                                                     long fieldConfigSchemeId, long fieldConfigId) {
        return addFieldConfigSchemeIssueType(issueTypeId, fieldConfigSchemeId, fieldConfigId).runQuery(db);
    }

    public static QueryCallback<Long> addFieldConfigSchemeIssueType(long fieldConfigSchemeId, long fieldConfigId) {
        return addFieldConfigSchemeIssueType((String) null, fieldConfigSchemeId, fieldConfigId);
    }

    public static QueryCallback<Long> addFieldConfigSchemeIssueType(@Nullable String issueTypeId,
                                                                    long fieldConfigSchemeId, long fieldConfigId) {
        return db -> {
            final Long id = GENERATOR.getAndIncrement();
            db.insert(FIELD_CONFIG_SCHEME_ISSUE_TYPE)
                    .set(FIELD_CONFIG_SCHEME_ISSUE_TYPE.id, id)
                    .set(FIELD_CONFIG_SCHEME_ISSUE_TYPE.issuetype, issueTypeId)
                    .set(FIELD_CONFIG_SCHEME_ISSUE_TYPE.fieldconfigscheme, fieldConfigSchemeId)
                    .set(FIELD_CONFIG_SCHEME_ISSUE_TYPE.fieldconfiguration, fieldConfigId)
                    .execute();
            return id;
        };
    }
}
