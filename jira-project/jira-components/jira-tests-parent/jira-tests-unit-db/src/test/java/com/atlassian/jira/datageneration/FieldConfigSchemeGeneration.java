package com.atlassian.jira.datageneration;

import com.atlassian.jira.database.DbConnection;
import com.atlassian.jira.database.QueryCallback;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.concurrent.atomic.AtomicLong;

import static com.atlassian.jira.model.querydsl.QFieldConfigScheme.FIELD_CONFIG_SCHEME;

public class FieldConfigSchemeGeneration {
    private static final AtomicLong GENERATOR = new AtomicLong(3000L);

    public static Long addFieldConfigScheme(@Nonnull DbConnection db, @Nonnull String name,
                                            @Nullable String description, @Nonnull String fieldId) {
        return addFieldConfigScheme(name, description, fieldId).runQuery(db);
    }

    public static QueryCallback<Long> addFieldConfigScheme(@Nonnull String name, @Nullable String description,
                                                           @Nonnull String fieldId) {
        return db -> {
            final Long id = GENERATOR.getAndIncrement();
            db.insert(FIELD_CONFIG_SCHEME)
                    .set(FIELD_CONFIG_SCHEME.id, id)
                    .set(FIELD_CONFIG_SCHEME.name, name)
                    .set(FIELD_CONFIG_SCHEME.description, description)
                    .set(FIELD_CONFIG_SCHEME.fieldid, fieldId)
                    .execute();
            return id;
        };
    }
}
