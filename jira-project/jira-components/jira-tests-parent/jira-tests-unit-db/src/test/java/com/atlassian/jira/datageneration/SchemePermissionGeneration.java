package com.atlassian.jira.datageneration;

import com.atlassian.jira.database.QueryCallback;
import com.atlassian.jira.database.SqlCallback;
import com.atlassian.jira.model.querydsl.PermissionSchemeDTO;
import com.atlassian.jira.model.querydsl.QPermissionScheme;
import com.atlassian.jira.model.querydsl.SchemePermissionsDTO;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import static com.atlassian.jira.datageneration.NodeAssociationGeneration.addNodeAssociation;
import static com.atlassian.jira.model.querydsl.QSchemePermissions.SCHEME_PERMISSIONS;
import static com.atlassian.jira.permission.ProjectPermissions.BROWSE_PROJECTS;

public class SchemePermissionGeneration {

    private static AtomicLong id = new AtomicLong(10000);
    private static AtomicLong nodeId = new AtomicLong(10000);

    private SchemePermissionGeneration() {
    }

    public static QueryCallback<Long> setupDefaultSchemePermissionWithBrowsePermissions(String... additionalGroups) {
        return dbConnection -> {
            PermissionSchemeDTO defaultPermissionScheme = PermissionSchemeDTO.builder()
                    .id(0L)
                    .name("Default Permission Scheme")
                    .build();
            dbConnection.insert(QPermissionScheme.PERMISSION_SCHEME).populate(defaultPermissionScheme).execute();

            List<String> groups = new ArrayList<>();
            groups.add("jira-administrators");

            if (additionalGroups != null) {
                groups.addAll(Arrays.asList(additionalGroups));
            }

            groups.stream().map(group -> {
                long localId = id.getAndIncrement();
                return SchemePermissionsDTO.builder()
                        .id(localId)
                        .permission(0L)
                        .permissionKey(BROWSE_PROJECTS.permissionKey())
                        .type("group")
                        .scheme(0L)
                        .parameter(group)
                        .build();
            }).forEach(dto -> {
                dbConnection.insert(SCHEME_PERMISSIONS).populate(dto).execute();
            });

            return 0L;
        };
    }

    public static SqlCallback addSchemePermission(final long schemeId, final long permission, final String permissionKey, final String permType, final String param) {
        return dbConnection -> {
            long localId = id.getAndIncrement();
            SchemePermissionsDTO dto = SchemePermissionsDTO.builder()
                    .id(localId)
                    .permission(permission)
                    .permissionKey(permissionKey)
                    .type(permType)
                    .scheme(schemeId)
                    .parameter(param)
                    .build();
            dbConnection.insert(SCHEME_PERMISSIONS).populate(dto).execute();
        };

    }

    public static SqlCallback assignPermissionSchemeToProject(Long projectId, Long permissionSchemeId) {
        return addNodeAssociation("Project", projectId, "PermissionScheme", permissionSchemeId, "ProjectScheme");
    }

}