package com.atlassian.jira.datageneration;

import com.atlassian.jira.database.DbConnection;
import com.atlassian.jira.database.QueryCallback;

import javax.annotation.Nonnull;
import java.util.concurrent.atomic.AtomicLong;

import static com.atlassian.jira.model.querydsl.QOptionConfiguration.OPTION_CONFIGURATION;

public class OptionConfigurationGeneration {
    private static final AtomicLong GENERATOR = new AtomicLong(5000L);

    public static Long addOptionConfiguration(@Nonnull DbConnection db, @Nonnull String issueTypeId,
                                              long fieldConfigId, long sequence) {
        return addOptionConfiguration(issueTypeId, fieldConfigId, sequence).runQuery(db);
    }

    public static QueryCallback<Long> addOptionConfiguration(@Nonnull String issueTypeId, long fieldConfigId,
                                                             long sequence) {
        return db -> {
            final Long id = GENERATOR.getAndIncrement();
            db.insert(OPTION_CONFIGURATION)
                    .set(OPTION_CONFIGURATION.id, id)
                    .set(OPTION_CONFIGURATION.fieldid, "issuetype")
                    .set(OPTION_CONFIGURATION.optionid, issueTypeId)
                    .set(OPTION_CONFIGURATION.fieldconfig, fieldConfigId)
                    .set(OPTION_CONFIGURATION.sequence, sequence)
                    .execute();
            return id;
        };
    }
}
