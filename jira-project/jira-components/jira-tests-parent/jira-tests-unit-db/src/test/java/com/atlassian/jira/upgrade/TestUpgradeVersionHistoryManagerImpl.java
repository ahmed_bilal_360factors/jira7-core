package com.atlassian.jira.upgrade;

import com.atlassian.jira.database.QueryDslAccessor;
import com.atlassian.jira.junit.rules.DatabaseContainer;
import com.atlassian.jira.model.querydsl.UpgradeVersionHistoryDTO;
import com.google.common.collect.Lists;
import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.List;

import static com.atlassian.jira.model.querydsl.QUpgradeHistory.UPGRADE_HISTORY;
import static com.atlassian.jira.model.querydsl.QUpgradeVersionHistory.UPGRADE_VERSION_HISTORY;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.when;

public class TestUpgradeVersionHistoryManagerImpl {

    private static final Date DO_NOT_TEST = Date.from(Instant.EPOCH);

    private static final Date NOW = Date.from(Instant.now());
    private static final Date A_MINUTE_AGO = MINUTES_AGO(1);
    private static final Date TWO_MINUTES_AGO = MINUTES_AGO(2);
    private static final Date THREE_MINUTES_AGO = MINUTES_AGO(3);
    private static final Date FOUR_MINUTES_AGO = MINUTES_AGO(4);

    @Rule
    public DatabaseContainer database = DatabaseContainer.rule(this);

    @Mock
    BuildVersionRegistry buildVersionRegistry;

    private UpgradeVersionHistoryManagerImpl upgradeVersionHistoryManager;

    private QueryDslAccessor queryDslAccessor;

    @Before
    public void setUp() throws Exception {
        queryDslAccessor = database.getAttachToDatabase().getQueryDslAccessor();

        upgradeVersionHistoryManager = new UpgradeVersionHistoryManagerImpl(buildVersionRegistry, queryDslAccessor);
    }

    @Test
    public void whenThereIsNoUpgradeHistoryOrUpgradeVersionHistoryAnEmptyListIsReturned() {
        assertThat(upgradeVersionHistoryManager.getAllUpgradeVersionHistory(), empty());
    }

    @Test
    public void whenThereIsAnUpgradeHistoryEntryWithAnInvalidBuildNumberNoHistoryIsReturned() {
        addUpgradeHistoryWithClassName("BAD_CLASS");

        assertThat(upgradeVersionHistoryManager.getAllUpgradeVersionHistory(), empty());
    }

    @Test
    public void whenAllUpgradeHistoryHasTargetBuildsNoUpgradeVersionHistoryIsReturned() {
        addUpgradeHistoryWithTargetBuild(100, "1.0.0");

        assertThat(upgradeVersionHistoryManager.getAllUpgradeVersionHistory(), empty());
    }

    @Test
    public void whenThereIsUpgradeHistoryWithNoTargetBuildsTheLatestIsIncludedInTheVersionHistoryWithInferredVersion() {
        addUpgradeHistoryWithNoTargetBuild(706, "7.0.6");
        addUpgradeHistoryWithNoTargetBuild(1001, "10.0.1");
        addUpgradeHistoryWithNoTargetBuild(2003, "20.0.3");
        addUpgradeHistoryWithNoTargetBuild(2005, "20.0.5");

        final UpgradeVersionHistoryItem expectedHistoryItem = new UpgradeVersionHistoryItemImpl(null, "2005", "20.0.5", "2005", null, true);
        assertThat(upgradeVersionHistoryManager.getAllUpgradeVersionHistory(), isUpgradeHistoryItemList(expectedHistoryItem));
    }

    @Test
    public void upgradeVersionHistoryIsIncludedInTheListAndPreviousIsLinkedCorrectly() {
        addUpgradeVersionHistory(100, "1.0-SNAPSHOT", A_MINUTE_AGO);
        addUpgradeVersionHistory(200, "2.0-SNAPSHOT", NOW);

        UpgradeVersionHistoryItem expectedFirstHistoryItem = new UpgradeVersionHistoryItemImpl(NOW, "200", "2.0-SNAPSHOT", "100", "1.0-SNAPSHOT");
        UpgradeVersionHistoryItem expectedSecondHistoryItem = new UpgradeVersionHistoryItemImpl(A_MINUTE_AGO, "100", "1.0-SNAPSHOT", null, null);
        assertThat(upgradeVersionHistoryManager.getAllUpgradeVersionHistory(), isUpgradeHistoryItemList(expectedFirstHistoryItem, expectedSecondHistoryItem));
    }

    @Test
    public void upgradeTaskInformationWithNoTargetBuildPopulatesPreviousOfVersionHistory() {
        addUpgradeHistoryWithNoTargetBuild(706, "7.0.6");
        addUpgradeVersionHistory(100, "1.0-SNAPSHOT", A_MINUTE_AGO);
        addUpgradeVersionHistory(200, "2.0-SNAPSHOT", NOW);

        UpgradeVersionHistoryItem expectedFirstHistoryItem = new UpgradeVersionHistoryItemImpl(NOW, "200", "2.0-SNAPSHOT", "100", "1.0-SNAPSHOT");
        UpgradeVersionHistoryItem expectedSecondHistoryItem = new UpgradeVersionHistoryItemImpl(A_MINUTE_AGO, "100", "1.0-SNAPSHOT", "706", "7.0.6");
        UpgradeVersionHistoryItem expectedThirdHistoryItem = new UpgradeVersionHistoryItemImpl(null, "706", "7.0.6", "706", null, true);
        assertThat(upgradeVersionHistoryManager.getAllUpgradeVersionHistory(), isUpgradeHistoryItemList(expectedFirstHistoryItem, expectedSecondHistoryItem, expectedThirdHistoryItem));
    }

    @Test
    public void worksWithLotsOfUpgradeHistoryAndUpgradeVersionHistoryItems() {
        addUpgradeHistoryWithNoTargetBuild(20, "0.2.0");
        addUpgradeHistoryWithNoTargetBuild(30, "0.3.0");
        addUpgradeHistoryWithNoTargetBuild(40, "0.4.0");
        addUpgradeHistoryWithNoTargetBuild(50, "0.5.0");

        addUpgradeVersionHistory(200, "2.0-SNAPSHOT", FOUR_MINUTES_AGO);
        addUpgradeVersionHistory(300, "3.0-SNAPSHOT", THREE_MINUTES_AGO);
        addUpgradeVersionHistory(400, "4.0-SNAPSHOT", TWO_MINUTES_AGO);
        addUpgradeVersionHistory(500, "5.0-SNAPSHOT", A_MINUTE_AGO);
        addUpgradeVersionHistory(600, "6.0-SNAPSHOT", NOW);

        List<UpgradeVersionHistoryItem> expectedVersionHistoryItems = Lists.newArrayList(
                new UpgradeVersionHistoryItemImpl(NOW, "600", "6.0-SNAPSHOT", "500", "5.0-SNAPSHOT"),
                new UpgradeVersionHistoryItemImpl(A_MINUTE_AGO, "500", "5.0-SNAPSHOT", "400", "4.0-SNAPSHOT"),
                new UpgradeVersionHistoryItemImpl(TWO_MINUTES_AGO, "400", "4.0-SNAPSHOT", "300", "3.0-SNAPSHOT"),
                new UpgradeVersionHistoryItemImpl(THREE_MINUTES_AGO, "300", "3.0-SNAPSHOT", "200", "2.0-SNAPSHOT"),
                new UpgradeVersionHistoryItemImpl(FOUR_MINUTES_AGO, "200", "2.0-SNAPSHOT", "50", "0.5.0"),
                new UpgradeVersionHistoryItemImpl(null, "50", "0.5.0", "50", null, true)
        );

        assertThat(upgradeVersionHistoryManager.getAllUpgradeVersionHistory(), isUpgradeHistoryItemList(expectedVersionHistoryItems));
    }

    @Test
    public void addingAHistoryItemPutsItInCorrectly() {
        upgradeVersionHistoryManager.addUpgradeVersionHistory(200, "2.0-SNAPSHOT");

        List<UpgradeVersionHistoryDTO> upgradeVersionHistory = getAllUpgradeVersionHistoryItems();
        assertThat(upgradeVersionHistory, hasSize(1));

        UpgradeVersionHistoryDTO historyItem = upgradeVersionHistory.get(0);
        assertThat(historyItem.getTargetbuild(), equalTo("200"));
        assertThat(historyItem.getTargetversion(), equalTo("2.0-SNAPSHOT"));
    }

    @Test
    public void addingATargetBuildToHistoryThatAlreadyExistsDoesNothing() {
        addUpgradeVersionHistory(100, "1.0-SNAPSHOT", NOW);

        upgradeVersionHistoryManager.addUpgradeVersionHistory(100, "2.0-SNAPSHOT");

        List<UpgradeVersionHistoryDTO> upgradeVersionHistory = getAllUpgradeVersionHistoryItems();
        assertThat(upgradeVersionHistory, hasSize(1));

        UpgradeVersionHistoryDTO historyItem = upgradeVersionHistory.get(0);
        assertThat(historyItem.getTargetbuild(), equalTo("100"));
        assertThat(historyItem.getTargetversion(), equalTo("1.0-SNAPSHOT"));
    }

    private Matcher<List<UpgradeVersionHistoryItem>> isUpgradeHistoryItemList(UpgradeVersionHistoryItem... expectedHistoryList) {
        return isUpgradeHistoryItemList(Lists.newArrayList(expectedHistoryList));
    }

    private Matcher<List<UpgradeVersionHistoryItem>> isUpgradeHistoryItemList(final List<UpgradeVersionHistoryItem> expectedHistoryList) {
        return new BaseMatcher<List<UpgradeVersionHistoryItem>>() {

            private String matchingError = null;

            @Override
            public void describeTo(Description description) {
                description.appendText("Expected all of the elements to match in the list of history items");
            }

            @Override
            public boolean matches(Object o) {
                List<UpgradeVersionHistoryItem> actualHistoryList = (List<UpgradeVersionHistoryItem>) o;

                if (expectedHistoryList.size() != actualHistoryList.size()) {
                    matchingError = String.format("The length of history items do not match (actual: %s, expected: %s)",
                            actualHistoryList.size(), expectedHistoryList.size());
                    return false;
                }

                for (int index = 0; index < actualHistoryList.size(); ++index) {
                    UpgradeVersionHistoryItem actual = actualHistoryList.get(index);
                    UpgradeVersionHistoryItem expected = expectedHistoryList.get(index);

                    if (!compare(index, "originalBuildNumber", expected.getOriginalBuildNumber(), actual.getOriginalBuildNumber()) ||
                            !compare(index, "originalVersion", expected.getOriginalVersion(), actual.getOriginalVersion()) ||
                            !compare(index, "targetBuildNumber", expected.getTargetBuildNumber(), actual.getTargetBuildNumber()) ||
                            !compare(index, "targetVersion", expected.getTargetVersion(), actual.getTargetVersion()) ||
                            !compare(index, "isInferred", expected.isInferred(), actual.isInferred())) {
                        return false;
                    } else if (!DO_NOT_TEST.equals(expected.getTimePerformed()) &&
                            !compare(index, "timePerformed", expected.getTimePerformed(), actual.getTimePerformed())) {
                        return false;
                    }
                }

                return true;
            }

            @Override
            public void describeMismatch(Object item, Description description) {
                description.appendText(matchingError);
            }

            private boolean compare(int index, String itemName, Object expected, Object actual) {
                if (expected == null && actual == null) {
                    return true;
                } else if (expected == null || actual == null || !expected.equals(actual)) {
                    matchingError = String.format("Item Index: '%s', Field: '%s' do not match (expected: %s, actual: %s)",
                            index, itemName, expected, actual);
                    return false;
                } else {
                    return true;
                }
            }
        };
    }

    private void addUpgradeHistoryWithClassName(String className) {
        addUpgradeHistory(className, "", 0, "0");
    }

    private void addUpgradeHistoryWithNoTargetBuild(int buildNumber, String buildVersion) {
        addUpgradeHistory("UpgradeTask_Build" + buildNumber, "", buildNumber, buildVersion);
    }

    private void addUpgradeHistoryWithTargetBuild(int buildNumber, String buildVersion) {
        addUpgradeHistory("UpgradeTask_Build" + buildNumber, String.valueOf(buildNumber), buildNumber, buildVersion);
    }

    private void addUpgradeHistory(String className, String targetBuild, int buildNumber, String buildVersion) {
        queryDslAccessor.execute(dbConnection -> dbConnection
                .insert(UPGRADE_HISTORY)
                .set(UPGRADE_HISTORY.upgradeclass, className)
                .set(UPGRADE_HISTORY.targetbuild, targetBuild)
                .set(UPGRADE_HISTORY.status, "complete")
                .executeWithId());

        when(buildVersionRegistry.getVersionForBuildNumber(buildNumber)).thenReturn(new BuildVersionImpl(buildNumber, buildVersion));
    }

    private void addUpgradeVersionHistory(int targetBuild, String targetVersion, Date timePerformed) {
        queryDslAccessor.execute(dbConnection -> dbConnection
                .insert(UPGRADE_VERSION_HISTORY)
                .set(UPGRADE_VERSION_HISTORY.timeperformed, new Timestamp(timePerformed.getTime()))
                .set(UPGRADE_VERSION_HISTORY.targetbuild, String.valueOf(targetBuild))
                .set(UPGRADE_VERSION_HISTORY.targetversion, targetVersion)
                .executeWithId());
    }

    private List<UpgradeVersionHistoryDTO> getAllUpgradeVersionHistoryItems() {
        return queryDslAccessor.executeQuery(dbConnection -> dbConnection.newSqlQuery()
                .select(UPGRADE_VERSION_HISTORY)
                .from(UPGRADE_VERSION_HISTORY)
                .fetch());
    }

    private static Date MINUTES_AGO(int minutesAgo) {
        return Date.from(Instant.now().minus(minutesAgo, ChronoUnit.MINUTES));
    }
}
