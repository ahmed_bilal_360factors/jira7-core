package com.atlassian.jira.datageneration;

import com.atlassian.jira.database.QueryCallback;

import java.util.concurrent.atomic.AtomicLong;

import static com.atlassian.jira.model.querydsl.QUserHistoryItem.USER_HISTORY_ITEM;


public class UserHistoryItemGeneration {

    private static AtomicLong id = new AtomicLong(10000);

    public static QueryCallback<Long> addUserHistoryItem(String entityType, String entityId, String userKey, long lastViewed) {
        final long localId = id.getAndIncrement();
        return dbConnection -> {
            dbConnection.insert(USER_HISTORY_ITEM)
                    .set(USER_HISTORY_ITEM.id, localId)
                    .set(USER_HISTORY_ITEM.type, entityType)
                    .set(USER_HISTORY_ITEM.entityId, entityId)
                    .set(USER_HISTORY_ITEM.username, userKey)
                    .set(USER_HISTORY_ITEM.lastViewed, lastViewed)
                    .execute();
            return localId;
        };
    }

}
