package com.atlassian.jira.datageneration;

import com.atlassian.jira.database.QueryCallback;
import com.atlassian.jira.model.querydsl.MembershipDTO;

import java.util.concurrent.atomic.AtomicLong;

import static com.atlassian.jira.model.querydsl.QMembership.MEMBERSHIP;

public class MembershipGeneration {

    private static AtomicLong id = new AtomicLong(10000);

    private MembershipGeneration() {
    }

    public static QueryCallback<Long> addUserToGroup(String username, String groupName) {
        return dbConnection -> {
            long localId = id.getAndIncrement();

            MembershipDTO membership = MembershipDTO.builder()
                    .id(localId)
                    .parentName(groupName)
                    .lowerParentName(groupName.toLowerCase())
                    .childName(username)
                    .lowerChildName(username.toLowerCase())
                    .build();

            dbConnection.insert(MEMBERSHIP).populate(membership).execute();
            return localId;
        };
    }
}
