package com.atlassian.jira.datageneration;

import com.atlassian.jira.database.IdGeneratingSQLInsertClause;
import com.atlassian.jira.database.QueryCallback;
import com.atlassian.jira.util.collect.MapBuilder;
import com.querydsl.core.types.Path;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

import static com.atlassian.jira.model.querydsl.QIssue.ISSUE;
import static com.atlassian.jira.model.querydsl.QProject.PROJECT;

/**
 * A set of helper functions which produce QueryCallback instances for creating issues and projects.
 * Constructed callbacks are to be passed to {@link com.atlassian.jira.database.QueryDslAccessor#executeQuery(QueryCallback)}
 * and return the id of the item created.
 */
public class IssueGeneration {

    private static AtomicLong issueId = new AtomicLong(10000);
    private static AtomicLong projectId = new AtomicLong(10000);

    public static QueryCallback<Long> addProject(String key, String name) {
        final long localProjectId = projectId.getAndIncrement();
        return dbConnection -> {
            dbConnection.insert(PROJECT)
                    .set(PROJECT.id, localProjectId)
                    .set(PROJECT.key, key)
                    .set(PROJECT.name, name)
                    .execute();
            return localProjectId;
        };
    }

    public static QueryCallback<Long> addIssue(long projectId) {
        return addIssue(projectId, new HashMap<>());
    }

    public static QueryCallback<Long> addIssue(long projectId, Map<Path, Object> extraColumns) {
        return addIssue(projectId, "The big dog was here", extraColumns);
    }

    public static QueryCallback<Long> addIssue(long projectId, String summary, Map<Path, Object> extraColumns) {
        return addIssue(projectId, summary, "1", extraColumns);
    }

    public static QueryCallback<Long> addIssue(long projectId, String summary) {
        return addIssue(projectId, summary, "1", MapBuilder.emptyMap());
    }

    public static QueryCallback<Long> addIssue(long projectId, String summary, String type, Map<Path, Object> extraColumns) {
        final long localIssueId = issueId.getAndIncrement();
        return dbConnection -> {
            IdGeneratingSQLInsertClause query = dbConnection.insert(ISSUE)
                    .set(ISSUE.id, localIssueId)
                    .set(ISSUE.project, projectId)
                    .set(ISSUE.summary, summary)
                    .set(ISSUE.type, type);
            for (Path path : extraColumns.keySet()) {
                query = query.set(path, extraColumns.get(path));
            }

            query.execute();
            return localIssueId;
        };
    }

}
