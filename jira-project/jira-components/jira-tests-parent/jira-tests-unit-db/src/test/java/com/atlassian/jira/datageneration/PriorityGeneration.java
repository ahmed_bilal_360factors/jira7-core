package com.atlassian.jira.datageneration;

import com.atlassian.jira.database.IdGeneratingSQLInsertClause;
import com.atlassian.jira.database.QueryCallback;
import com.atlassian.jira.util.collect.MapBuilder;
import com.querydsl.core.types.Path;

import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

import static com.atlassian.jira.model.querydsl.QPriority.PRIORITY;
import static java.util.Collections.emptyMap;

public class PriorityGeneration {
    private static AtomicLong id = new AtomicLong(10000);

    public static QueryCallback<Long> addPriority(String name) {
        return addPriority(name, emptyMap());
    }

    public static QueryCallback<Long> addPriority(String name, Long sequence) {
        return addPriority(name, MapBuilder.build(PRIORITY.sequence, sequence));
    }

    public static QueryCallback<Long> addPriority(String name, Map<Path, Object> extraColumns) {
        final long localId = id.getAndIncrement();
        return dbConnection -> {
            IdGeneratingSQLInsertClause query = dbConnection.insert(PRIORITY)
                    .set(PRIORITY.id, String.valueOf(localId))
                    .set(PRIORITY.name, name);

            for (Path path : extraColumns.keySet()) {
                query = query.set(path, extraColumns.get(path));
            }

            query.execute();
            return localId;
        };
    }
}
