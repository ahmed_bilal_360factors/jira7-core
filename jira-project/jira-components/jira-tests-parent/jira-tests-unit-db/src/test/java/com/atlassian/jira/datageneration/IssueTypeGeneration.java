package com.atlassian.jira.datageneration;

import com.atlassian.jira.database.QueryCallback;

import java.util.concurrent.atomic.AtomicLong;

import static com.atlassian.jira.model.querydsl.QIssueType.ISSUE_TYPE;

public class IssueTypeGeneration {
    private static AtomicLong id = new AtomicLong(10000);

    public static QueryCallback<Long> addIssueType(String name, long sequence) {
        final long localId = id.getAndIncrement();
        return dbConnection -> {
            dbConnection.insert(ISSUE_TYPE)
                    .set(ISSUE_TYPE.id, String.valueOf(localId))
                    .set(ISSUE_TYPE.name, name)
                    .set(ISSUE_TYPE.sequence, sequence)
                    .execute();
            return localId;
        };
    }
}
