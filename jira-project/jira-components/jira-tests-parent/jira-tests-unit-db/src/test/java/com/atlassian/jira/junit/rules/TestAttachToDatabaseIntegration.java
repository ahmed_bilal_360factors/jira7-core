package com.atlassian.jira.junit.rules;

import com.atlassian.jira.database.QueryDslAccessor;
import com.querydsl.sql.SQLQuery;
import org.apache.commons.lang3.time.StopWatch;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;
import java.util.List;

import static com.atlassian.jira.model.querydsl.QIssue.ISSUE;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class TestAttachToDatabaseIntegration {
    private static final Logger log = LoggerFactory.getLogger(AttachToDatabase.class);

    @Rule
    public DatabaseContainer database = DatabaseContainer.rule(this);

    private QueryDslAccessor queryDslAccessor;

    private StopWatch stopwatch = new StopWatch();

    @Before
    public void setup() {
        queryDslAccessor = database.getAttachToDatabase().getQueryDslAccessor();
        stopwatch.start();
    }

    @After
    public void tearDown() {
        stopwatch.stop();
        log.info("Database test ran in {} millis", stopwatch.getTime());
    }

    @Test
    public void testOneTime() throws Exception {
        // This would blow up if the clear of the database failed in any prior test.
        doTest();
    }

    @Test
    public void testAnotherTime() throws Exception {
        // This would blow up if the clear of the database failed in any prior test.
        doTest();
    }

    @Test
    public void testDuplicateKey() throws Exception {
        // We expect a duplicate key exception if we try and add the same issue row twice.
        addIssue();
        try {
            addIssue();
        } catch (RuntimeException e) {
            Throwable cause = e.getCause();
            assertThat(cause, instanceOf(SQLException.class));
            assertThat(((SQLException) cause).getSQLState(), is("23505"));
        }
    }

    private void doTest() {
        addIssue();

        queryDslAccessor.execute(dbConnection -> {
            SQLQuery<Long> query = dbConnection.newSqlQuery().select(ISSUE.id).from(ISSUE);
            List<Long> result = query.fetch();
            assertThat(result, contains(1L));
        });
    }

    private void addIssue() {
        queryDslAccessor.execute(dbConnection -> {
            dbConnection.insert(ISSUE)
                    .set(ISSUE.id, 1L)
                    .set(ISSUE.project, 10000L)
                    .set(ISSUE.summary, "The big dog was here")
                    .set(ISSUE.type, "1")
                    .execute();
        });
    }
}