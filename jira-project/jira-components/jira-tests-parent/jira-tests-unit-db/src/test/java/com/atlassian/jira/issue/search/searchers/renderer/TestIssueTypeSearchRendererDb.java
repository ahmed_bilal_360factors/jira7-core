package com.atlassian.jira.issue.search.searchers.renderer;

import com.atlassian.collectors.CollectorsUtil;
import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.database.DbConnection;
import com.atlassian.jira.database.QueryDslAccessor;
import com.atlassian.jira.datageneration.ProjectGeneration;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.issue.issuetype.MockIssueType;
import com.atlassian.jira.issue.search.constants.SimpleFieldSearchConstants;
import com.atlassian.jira.issue.search.constants.SystemSearchConstants;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.DatabaseContainer;
import com.atlassian.jira.mock.MockConstantsManager;
import com.atlassian.jira.model.querydsl.ProjectDTO;
import com.atlassian.jira.model.querydsl.QProject;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.util.collect.MultiMap;
import com.atlassian.jira.util.collect.MultiMaps;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSortedSet;
import com.google.common.collect.Sets;
import org.apache.commons.lang3.StringUtils;
import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static com.atlassian.jira.datageneration.ConfigurationContextGeneration.addConfigurationContext;
import static com.atlassian.jira.datageneration.FieldConfigSchemeGeneration.addFieldConfigScheme;
import static com.atlassian.jira.datageneration.FieldConfigSchemeIssueTypeGeneration.addFieldConfigSchemeIssueType;
import static com.atlassian.jira.datageneration.FieldConfigurationGeneration.addFieldConfiguration;
import static com.atlassian.jira.datageneration.OptionConfigurationGeneration.addOptionConfiguration;
import static com.atlassian.jira.issue.IssueFieldConstants.ISSUE_TYPE;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class TestIssueTypeSearchRendererDb {
    private static final Map<String, IssueType> ISSUE_TYPES = ImmutableMap.of(
            "bug", new MockIssueType("bug", "Bug"),
            "story", new MockIssueType("story", "Story"),
            "substory", new MockIssueType("substory", "Sub-Story", true),
            "task", new MockIssueType("task", "Task"),
            "subtask", new MockIssueType("subtask", "Sub-Task", true));

    @Rule
    public DatabaseContainer databaseContainer = DatabaseContainer.rule(this);

    @AvailableInContainer(interfaceClass = ConstantsManager.class)
    private MockConstantsManager constantsManager = new MockConstantsManager();

    private SimpleFieldSearchConstants searchConstants = SystemSearchConstants.forIssueType();
    private QueryDslAccessor queryDslAccessor;
    private IssueTypeSearchRenderer searchRenderer;

    private List<Project> projects;

    // Generated field config scheme ids
    private long[] schemes;

    // Generated field configuration ids
    private long[] configs;


    @Before
    public void setUp() {
        queryDslAccessor = databaseContainer.getAttachToDatabase().getQueryDslAccessor();

        ISSUE_TYPES.values().forEach(constantsManager::addIssueType);

        searchRenderer = new IssueTypeSearchRenderer(null, constantsManager, queryDslAccessor, null, searchConstants,
                null, null, null, null);
    }

    @After
    public void tearDown() {
        // Just being nice... Make sure we don't hold onto a bunch of garbage even if something holds onto the test
        projects = null;
        schemes = null;
        configs = null;
    }

    private void givenBasicSetup() {
        queryDslAccessor.execute(db -> {
            generateProjects(db, 5);
            generateSchemesAndConfigs(db, 5);

            setIssueTypesForConfig(db, 1, "bug", "task");
            setIssueTypesForConfig(db, 2, "task", "subtask");
            setIssueTypesForConfig(db, 3, "story", "substory");
            setIssueTypesForConfig(db, 4, "bug", "story", "substory");

            IntStream.rangeClosed(1, 4)
                    .forEach(x -> setIssueTypeScheme(db, projects(x), x));
        });
    }


    @Test
    public void getVisibleIssueTypeIdsInBasicSetup() {
        givenBasicSetup();

        // Verify each project by itself
        assertIssueTypes(projects(0), "bug", "task", "subtask", "story", "substory");
        assertIssueTypes(projects(1), "bug", "task");
        assertIssueTypes(projects(2), "task", "subtask");
        assertIssueTypes(projects(3), "story", "substory");
        assertIssueTypes(projects(4), "bug", "story", "substory");

        // Mix and match them...
        assertIssueTypes(projects(0, 1), "bug", "task", "subtask", "story", "substory");
        assertIssueTypes(projects(1, 2), "bug", "task", "subtask");
        assertIssueTypes(projects(2, 3), "task", "subtask", "story", "substory");
        assertIssueTypes(projects(3, 4), "bug", "story", "substory");
        assertIssueTypes(projects(4, 2), "bug", "task", "subtask", "story", "substory");
        assertIssueTypes(projects(4, 0), "bug", "task", "subtask", "story", "substory");
        assertIssueTypes(projects(4, 1, 2), "bug", "task", "subtask", "story", "substory");
        assertIssueTypes(projects(3, 1, 4), "bug", "task", "story", "substory");
        assertIssueTypes(projects, "bug", "task", "subtask", "story", "substory");
    }

    @Test
    public void getVisibleIssueTypeIdsWithCssInfoInBasicSetup() {
        givenBasicSetup();

        // Verify each project by itself
        assertCssMap(projects(0), isCssMap().withSameConfig(0, "bug", "task", "subtask", "story", "substory"));
        assertCssMap(projects(1), isCssMap().withSameConfig(1, "bug", "task"));
        assertCssMap(projects(2), isCssMap().withSameConfig(2, "task", "subtask"));
        assertCssMap(projects(3), isCssMap().withSameConfig(3, "story", "substory"));
        assertCssMap(projects(4), isCssMap().withSameConfig(4, "bug", "story", "substory"));

        // Mix and match them...
        assertCssMap(projects(0, 1), isCssMap()
                .with("bug", 0, 1)
                .with("task", 0, 1)
                .with("subtask", 0)
                .with("story", 0)
                .with("substory", 0));
        assertCssMap(projects(1, 2), isCssMap()
                .with("bug", 1)
                .with("task", 1, 2)
                .with("subtask", 2));
        assertCssMap(projects(2, 3), isCssMap()
                .with("task", 2)
                .with("subtask", 2)
                .with("story", 3)
                .with("substory", 3));
        assertCssMap(projects(3, 4), isCssMap()
                .with("bug", 4)
                .with("story", 3, 4)
                .with("substory", 3, 4));
        assertCssMap(projects(4, 0), isCssMap()
                .with("bug", 0, 4)
                .with("task", 0)
                .with("subtask", 0)
                .with("story", 0, 4)
                .with("substory", 0, 4));
        assertCssMap(projects(4, 1, 2), isCssMap()
                .with("bug", 1, 4)
                .with("task", 1, 2)
                .with("subtask", 2)
                .with("story", 4)
                .with("substory", 4));
        assertCssMap(projects(3, 1, 4), isCssMap()
                .with("bug", 1, 4)
                .with("task", 1)
                .with("story", 3, 4)
                .with("substory", 3, 4));
        assertCssMap(projects, isCssMap()
                .with("bug", 0, 1, 4)
                .with("task", 0, 1, 2)
                .with("subtask", 0, 2)
                .with("story", 0, 3, 4)
                .with("substory", 0, 3, 4));
    }


    private void givenHugeSetup() {
        queryDslAccessor.execute(db -> generateProjects(db, 2000));
        queryDslAccessor.execute(db -> generateSchemesAndConfigs(db, 2000));

        queryDslAccessor.execute(db -> IntStream.range(500, 1000)
                .forEach(x -> setIssueTypesForConfig(db, x, "task", "subtask")));
        queryDslAccessor.execute(db -> IntStream.range(1000, 1500)
                .forEach(x -> setIssueTypesForConfig(db, x, "story", "substory")));
        queryDslAccessor.execute(db -> IntStream.range(1500, 2000)
                .forEach(x -> setIssueTypesForConfig(db, x, "bug", "task")));

        queryDslAccessor.execute(db -> IntStream.range(500, 2000)
                .forEach(x -> setIssueTypeScheme(db, projects(x), x)));
    }

    @Test
    public void getVisibleIssueTypeIdsInHugeSetup() {
        givenHugeSetup();

        // Verify some of projects by themselves
        assertIssueTypes(projects(0), "bug", "task", "subtask", "story", "substory");
        assertIssueTypes(projects(500), "task", "subtask");
        assertIssueTypes(projects(1000), "story", "substory");
        assertIssueTypes(projects(1500), "bug", "task");

        // Mix and match few of them...
        assertIssueTypes(projects(0, 500), "bug", "task", "subtask", "story", "substory");
        assertIssueTypes(projects(500, 1000), "task", "subtask", "story", "substory");
        assertIssueTypes(projects(1000, 1500), "bug", "task", "story", "substory");
        assertIssueTypes(projects(1500, 500), "bug", "task", "subtask");
        assertIssueTypes(projects(500, 1000, 1500), "bug", "task", "subtask", "story", "substory");
        assertIssueTypes(projects(0, 1000), "bug", "task", "subtask", "story", "substory");

        assertIssueTypes(projectsRange(500, 1500), "task", "subtask", "story", "substory");
        assertIssueTypes(projects, "bug", "task", "subtask", "story", "substory");
    }

    @Test
    public void getVisibleIssueTypeIdsWithCssInfoInHugeSetup() {
        givenHugeSetup();

        // Verify some of projects by themselves
        assertCssMap(projects(0), isCssMap().withSameConfig(0, "bug", "task", "subtask", "story", "substory"));
        assertCssMap(projects(500), isCssMap().withSameConfig(500, "task", "subtask"));
        assertCssMap(projects(1000), isCssMap().withSameConfig(1000, "story", "substory"));
        assertCssMap(projects(1500), isCssMap().withSameConfig(1500, "bug", "task"));

        // Mix and match few of them...
        assertCssMap(projects(0, 500), isCssMap()
                .with("bug", 0)
                .with("task", 0, 500)
                .with("subtask", 0, 500)
                .with("story", 0)
                .with("substory", 0));
        assertCssMap(projects(768, 1776), isCssMap()
                .with("bug", 1776)
                .with("task", 768, 1776)
                .with("subtask", 768));
        assertCssMap(projects(1066, 1776), isCssMap()
                .with("bug", 1776)
                .with("task", 1776)
                .with("story", 1066)
                .with("substory", 1066));

        // Note: Projects 0-499 are all using the default scheme, which is at index 0
        assertCssMap(projects(42, 768, 1066, 1776), isCssMap()
                .with("bug", 0, 1776)
                .with("task", 0, 768, 1776)
                .with("subtask", 0, 768)
                .with("story", 0, 1066)
                .with("substory", 0, 1066));

        // Large ranges of them...
        assertCssMap(projectsRange(500, 1500), isCssMap()
                .withRange("task", 500, 1000)
                .withRange("subtask", 500, 1000)
                .withRange("story", 1000, 1500)
                .withRange("substory", 1000, 1500));
        assertCssMap(projectsRange(900, 1100), isCssMap()
                .withRange("task", 900, 1000)
                .withRange("subtask", 900, 1000)
                .withRange("story", 1000, 1100)
                .withRange("substory", 1000, 1100));
        assertCssMap(projectsRange(900, 1600), isCssMap()
                .withRange("bug", 1500, 1600)
                .withRange("task", 900, 1000)
                .withRange("task", 1500, 1600)
                .withRange("subtask", 900, 1000)
                .withRange("story", 1000, 1500)
                .withRange("substory", 1000, 1500));
        assertCssMap(projectsRange(300, 1700), isCssMap()
                .withSameConfig(0, "bug", "task", "subtask", "story", "substory")
                .withRange("bug", 1500, 1700)
                .withRange("task", 500, 1000)
                .withRange("task", 1500, 1700)
                .withRange("subtask", 500, 1000)
                .withRange("story", 1000, 1500)
                .withRange("substory", 1000, 1500));
        assertCssMap(projects, isCssMap()
                .withSameConfig(0, "bug", "task", "subtask", "story", "substory")
                .withRange("bug", 1500, 2000)
                .withRange("task", 500, 1000)
                .withRange("task", 1500, 2000)
                .withRange("subtask", 500, 1000)
                .withRange("story", 1000, 1500)
                .withRange("substory", 1000, 1500));
    }


    private void generateProjects(DbConnection db, int count) {
        db.delete(QProject.PROJECT).execute();
        projects = IntStream.rangeClosed(1, count)
                .mapToObj(num -> addProject(db, num))
                .collect(CollectorsUtil.toNewArrayListWithCapacity(count + 1));
    }

    private static Project addProject(DbConnection db, int num) {
        final long id = ProjectGeneration.addProject(db, ProjectDTO.builder()
                .key("PROJ" + num)
                .name("Project " + num)
                .lead("fred")
                .build());
        return new MockProject(id);
    }

    private void generateSchemesAndConfigs(DbConnection db, int count) {
        schemes = new long[count];
        configs = new long[count];

        IntStream.range(0, count).forEach(i -> {
            // These are "Issue Type Schemes", which is what the UI calls a FieldConfigScheme that has "issuetype"
            // as the field ID instead of "customfield_12345".
            schemes[i] = addFieldConfigScheme(db, "schemes[" + i + "]", null, ISSUE_TYPE);

            // Issue Type Schemes always have exactly one field config, as the issue type field is the only one
            // that it governs.
            configs[i] = addFieldConfiguration(db, "configs[" + i + "]", ISSUE_TYPE);

            // FieldConfigSchemeIssueType is the thing that binds the FieldConfigScheme (an Issue Type Scheme)
            // to is corresponding FieldConfiguration (which links and orders the Issue Types in that scheme).
            addFieldConfigSchemeIssueType(db, "fcsit[" + i + "]", schemes[i], configs[i]);
        });

        // The field configuration for the default Issue Type Scheme should always exist and should always
        // contain all of the existing issue types (in some order, not that the order is relevant to this test).
        addConfigurationContext(db, null, ISSUE_TYPE, schemes[0]);
        setIssueTypesForConfig(db, 0, ISSUE_TYPES.keySet());
    }

    private void setIssueTypeScheme(DbConnection db, Collection<Project> projects, int schemeIdx) {
        projects.forEach(project -> addConfigurationContext(db, project.getId(), ISSUE_TYPE, schemes[schemeIdx]));
    }

    private void setIssueTypesForConfig(DbConnection db, int configIdx, String... issueTypeIds) {
        setIssueTypesForConfig(db, configIdx, ImmutableList.copyOf(issueTypeIds));
    }

    private void setIssueTypesForConfig(DbConnection db, int configIdx, Collection<String> issueTypeIds) {
        int i = 0;
        for (String issueTypeId : issueTypeIds) {
            addOptionConfiguration(db, issueTypeId, configs[configIdx], i);
            ++i;
        }
    }


    private void assertIssueTypes(Collection<Project> projects, String... expected) {
        final Collection<String> issueTypeIds = searchRenderer.getVisibleIssueTypeIds(projects);
        final Set<String> sortedIssueTypeIds = new TreeSet<>(issueTypeIds);
        if (issueTypeIds.size() != sortedIssueTypeIds.size()) {
            fail("Collection of issueTypeIds contained duplicates: " + issueTypeIds);
        }

        assertEquals(ImmutableSortedSet.copyOf(expected), sortedIssueTypeIds);
    }

    private void assertCssMap(Collection<Project> projects, CssMatcher matcher) {
        final Map<String, String> cssMap = searchRenderer.getVisibleIssueTypeIdsWithCssInfo(projects);
        if (!matcher.matches(cssMap)) {
            fail("\n\tExp: " + matcher + "\n\tGot: " + cssMap);
        }

    }


    private List<Project> projectsRange(int start, int stop) {
        return IntStream.range(start, stop)
                .mapToObj(projects::get)
                .collect(Collectors.toList());
    }

    private List<Project> projects(int... indexes) {
        return Arrays.stream(indexes)
                .mapToObj(projects::get)
                .collect(Collectors.toList());
    }

    private CssMatcher isCssMap() {
        return new CssMatcher();
    }

    class CssMatcher extends TypeSafeMatcher<Map<String, String>> {
        private final MultiMap<String, Long, Set<Long>> expected = MultiMaps.createSetMultiMap();

        CssMatcher with(String issueTypeId, int... fcIndexes) {
            Arrays.stream(fcIndexes)
                    .mapToLong(x -> configs[x])
                    .forEach(fcId -> expected.putSingle(issueTypeId, fcId));
            return this;
        }

        CssMatcher withSameConfig(int fcIndex, String... issueTypeIds) {
            Arrays.stream(issueTypeIds).forEach(issueTypeId -> with(issueTypeId, fcIndex));
            return this;
        }

        CssMatcher withRange(String issueTypeId, int start, int stop) {
            final int[] fcIndexes = IntStream.range(start, stop).toArray();
            return with(issueTypeId, fcIndexes);
        }

        @Override
        protected boolean matchesSafely(Map<String, String> optionCssClasses) {
            assertEquals("CSS map has the wrong issueTypeIds", expected.keySet(), optionCssClasses.keySet());

            for (Map.Entry<String, Set<Long>> entry : expected.entrySet()) {
                final String issueTypeId = entry.getKey();
                final Set<Long> expected = entry.getValue();
                final Set<Long> actual = Sets.newHashSetWithExpectedSize(expected.size());

                Arrays.stream(StringUtils.split(optionCssClasses.get(issueTypeId), ' '))
                        .map(Long::valueOf)
                        .forEach(fcId -> {
                            // Fail outright if we find a duplicate fcId for an issueTypeId
                            if (!actual.add(fcId)) {
                                fail("issueTypeId '" + issueTypeId + "' had duplicate fcId " + fcId);
                            }
                        });

                if (!expected.equals(actual)) {
                    assertEquals("issueTypeId='" + issueTypeId + "' has the wrong field config Ids",
                            new TreeSet<>(expected), new TreeSet<>(actual));
                    return false;
                }
            }

            return true;
        }

        @Override
        public void describeTo(Description description) {
            description.appendValue(expected);
        }
    }
}
