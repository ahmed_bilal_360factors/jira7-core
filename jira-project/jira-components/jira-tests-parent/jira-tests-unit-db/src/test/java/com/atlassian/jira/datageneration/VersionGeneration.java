package com.atlassian.jira.datageneration;

import com.atlassian.jira.database.QueryCallback;
import com.atlassian.jira.database.SqlCallback;
import com.atlassian.jira.model.querydsl.NodeAssociationDTO;

import java.util.concurrent.atomic.AtomicLong;

import static com.atlassian.jira.model.querydsl.QNodeAssociation.NODE_ASSOCIATION;
import static com.atlassian.jira.model.querydsl.QVersion.VERSION;

public class VersionGeneration {

    private static AtomicLong id = new AtomicLong(10000);

    public static QueryCallback<Long> addVersion(String name, long sequence, long project) {
        final long localId = id.getAndIncrement();
        return dbConnection -> {
            dbConnection.insert(VERSION)
                    .set(VERSION.id, localId)
                    .set(VERSION.name, name)
                    .set(VERSION.sequence, sequence)
                    .set(VERSION.project, project)
                    .execute();
            return localId;
        };
    }

    public static SqlCallback assignFixFor(long issueId, long versionId) {
        return assignIssueToVersion(issueId, versionId, "IssueFixVersion");
    }

    public static SqlCallback assignIssueToVersion(long issueId, long versionId, String associationType) {
        return dbConnection -> {
            NodeAssociationDTO nodeAssociation = NodeAssociationDTO.builder()
                    .sourceNodeId(issueId)
                    .sourceNodeEntity("Issue")
                    .sinkNodeEntity("Version")
                    .associationType(associationType)
                    .sinkNodeId(versionId)
                    .build();
            dbConnection.insert(NODE_ASSOCIATION).populate(nodeAssociation).execute();
        };
    }
}
