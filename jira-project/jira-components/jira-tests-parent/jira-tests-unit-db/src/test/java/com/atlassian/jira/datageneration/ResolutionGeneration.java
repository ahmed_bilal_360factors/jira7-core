package com.atlassian.jira.datageneration;

import com.atlassian.jira.database.IdGeneratingSQLInsertClause;
import com.atlassian.jira.database.QueryCallback;
import com.querydsl.core.types.Path;

import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

import static com.atlassian.jira.model.querydsl.QResolution.RESOLUTION;
import static java.util.Collections.emptyMap;

public class ResolutionGeneration {
    private static AtomicLong id = new AtomicLong(10000);

    public static QueryCallback<Long> addResolution(String name) {
        return addResolution(name, emptyMap());
    }

    public static QueryCallback<Long> addResolution(String name, Map<Path, Object> extraColumns) {
        final long localId = id.getAndIncrement();
        return dbConnection -> {
            IdGeneratingSQLInsertClause query = dbConnection.insert(RESOLUTION)
                    .set(RESOLUTION.id, String.valueOf(localId))
                    .set(RESOLUTION.name, name);

            for (Path path : extraColumns.keySet()) {
                query = query.set(path, extraColumns.get(path));
            }

            query.execute();
            return localId;
        };
    }
}
