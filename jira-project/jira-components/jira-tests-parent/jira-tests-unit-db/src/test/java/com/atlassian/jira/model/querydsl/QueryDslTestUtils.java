package com.atlassian.jira.model.querydsl;

import com.atlassian.jira.database.DbConnection;
import com.atlassian.jira.sharing.SharePermissionImpl;
import com.atlassian.jira.sharing.search.AbstractShareTypeSearchParameter;
import com.atlassian.jira.sharing.search.GroupShareTypeSearchParameter;
import com.atlassian.jira.sharing.search.PrivateShareTypeSearchParameter;
import com.atlassian.jira.sharing.search.ProjectShareTypeSearchParameter;
import com.atlassian.jira.sharing.type.ShareType;

import static com.atlassian.jira.model.querydsl.QPortalPage.PORTAL_PAGE;
import static com.atlassian.jira.model.querydsl.QProject.PROJECT;
import static com.atlassian.jira.model.querydsl.QProjectRole.PROJECT_ROLE;
import static com.atlassian.jira.model.querydsl.QProjectRoleActor.PROJECT_ROLE_ACTOR;
import static com.atlassian.jira.model.querydsl.QSharePermissions.SHARE_PERMISSIONS;
import static com.atlassian.jira.security.roles.ProjectRoleActor.USER_ROLE_ACTOR_TYPE;

public class QueryDslTestUtils {
    /** This is a very hacky way to generate unique IDs for entities while providing useful error messages on test failure.
     * New IDs are obtained by calling newId(String name), which appends a line of the form "$ID: $NAME" to IdMapping
     * This string can then be prepended to assertion messages, so that developers can easily determine which entity
     * the message refers to (as it would otherwise only contain numeric IDs).
     */
    private static long nextId;
    private static String IdMapping = "\n\t";

    private QueryDslTestUtils() {}

    public static final SharePermissionImpl GLOBAL_PERMISSION = new SharePermissionImpl(ShareType.Name.GLOBAL, null, null);

    public static String getIdMapping() {
        return IdMapping;
    }

    /**
     * Creates a dashboard.
     * @param owner         The user who created the dashboard.
     * @param name          The name of the dashboard.
     * @param searchParams  The shares of the dashboard.
     * @return              The id of the dashboard.
     */
    public static long createDashboard(DbConnection con, String owner, String name, AbstractShareTypeSearchParameter... searchParams) {
        long id = newId(name + "_dash");
        return createDashboard(con, new PortalPageDTO(id, owner, name, name + "_description", null, null, null, null), searchParams);
    }

    /**
     * Creates a dashboard.
     * @param values        An object containing the values stored in the record.
     * @param searchParams  The shares of the dashboard.
     * @return              The id of the dashboard.
     */
    public static long createDashboard(DbConnection con, PortalPageDTO values, AbstractShareTypeSearchParameter... searchParams) {
        final String name = values.getPagename();
        final long id = values.getId();

        con.insert(PORTAL_PAGE)
                .populate(values)
                .execute();

        for (AbstractShareTypeSearchParameter param : searchParams) {
            if (param == PrivateShareTypeSearchParameter.PRIVATE_PARAMETER) {
                //Dashboards created (by users) as private do not have any entries in the sharepermissions table,
                //so make sure our test data mimics that.
                continue;
            }

            SharePermissionsDTO perm = new SharePermissionsDTO(newId(String.format("dash_%d_perm_%s", id, param.getType().get())), id, "PortalPage", param.getType().get(), getParam1(param), getParam2(param));
            con.insert(SHARE_PERMISSIONS)
                    .populate(perm)
                    .execute();
        }

        return id;
    }

    /**
     * Helper method for generating UIDs.
     */
    public static long newId(String name){
        long res =  nextId++;
        IdMapping += Long.toString(res) + ": " + name + "\n\t";
        return res;
    }

    /**
     * Creates a project with the specified name.
     * @return The project ID.
     */
    public static long createProject(DbConnection con, String name) {
        long id = newId("project_" + name);

        con.insert(PROJECT)
                .values(id, name, null, null, null, null, null, null, null, null, null)
                .execute();

        return id;
    }

    /**
     * Creates a project role.
     * @return The role ID.
     */
    public static long addProjectRole(DbConnection con, String name){
        long id = newId("role_" + name);

        con.insert(PROJECT_ROLE)
                .values(id, name)
                .execute();

        return id;
    }

    /**
     * Adds a user to a project role.
     * At most one of groupId and projectId may be null.
     */
    public static void addUserToProjectRole(DbConnection con, String username, Long roleId, Long projectId) {
        //id col has no significance, so we generate an ID directly instead of labelling it
        con.insert(PROJECT_ROLE_ACTOR)
                .values(nextId++, projectId, roleId, USER_ROLE_ACTOR_TYPE, username)
                .execute();
    }

    public static String getParam1(AbstractShareTypeSearchParameter param) {
        if (param instanceof GroupShareTypeSearchParameter) {
            return ((GroupShareTypeSearchParameter) param).getGroupName();

        } else if (param instanceof ProjectShareTypeSearchParameter) {
            return ((ProjectShareTypeSearchParameter) param).getProjectId().toString();

        } else {
            return null;
        }
    }

    public static String getParam2(AbstractShareTypeSearchParameter param) {
        if (param instanceof ProjectShareTypeSearchParameter) {
            ProjectShareTypeSearchParameter projectParam = (ProjectShareTypeSearchParameter) param;
            return projectParam.hasRole() ? projectParam.getRoleId().toString() : null;

        } else {
            return null;
        }
    }

}
