package com.atlassian.jira.datageneration;

import com.atlassian.jira.database.IdGeneratingSQLInsertClause;
import com.atlassian.jira.database.QueryCallback;
import com.atlassian.jira.util.collect.MapBuilder;
import com.querydsl.core.types.Path;

import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

import static com.atlassian.jira.model.querydsl.QStatus.STATUS;

public class StatusGeneration {

    private static AtomicLong statusId = new AtomicLong(10000);

    public static QueryCallback<Long> addStatus(long sequence) {
        return addStatus(sequence, MapBuilder.emptyMap());
    }

    public static QueryCallback<Long> addStatus(long sequence, Map<Path, Object> extraColumns) {
        final long localStatusId = statusId.getAndIncrement();
        return dbConnection -> {
            IdGeneratingSQLInsertClause query = dbConnection.insert(STATUS)
                    .set(STATUS.id, String.valueOf(localStatusId))
                    .set(STATUS.sequence, sequence);

            for (Path path : extraColumns.keySet()) {
                query = query.set(path, extraColumns.get(path));
            }

            query.execute();
            return localStatusId;
        };
    }

}
