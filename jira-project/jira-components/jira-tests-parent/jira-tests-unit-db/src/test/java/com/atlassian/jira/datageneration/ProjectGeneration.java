package com.atlassian.jira.datageneration;

import com.atlassian.jira.database.DbConnection;
import com.atlassian.jira.database.IdGeneratingSQLInsertClause;
import com.atlassian.jira.database.QueryCallback;
import com.atlassian.jira.model.querydsl.ProjectDTO;

import java.util.concurrent.atomic.AtomicLong;

import static com.atlassian.jira.model.querydsl.QProject.PROJECT;

public class ProjectGeneration {
    private static final AtomicLong GENERATOR = new AtomicLong(10000L);

    public static QueryCallback<Long> addProject(final String leadUserKey) {
        return addProject(ProjectDTO.builder()
                .lead(leadUserKey)
                .build());
    }

    public static QueryCallback<Long> addProject(final long projectId, final String leadUserKey) {
        return addProject(ProjectDTO.builder()
                .id(projectId)
                .lead(leadUserKey)
                .build());
    }

    public static Long addProject(final DbConnection db, final ProjectDTO project) {
        return addProject(project).runQuery(db);
    }

    public static QueryCallback<Long> addProject(final ProjectDTO project) {
        return dbConnection -> {
            final IdGeneratingSQLInsertClause insert = dbConnection.insert(PROJECT).populate(project);

            Long id = project.getId();
            if (id == null) {
                id = GENERATOR.getAndIncrement();
                insert.set(PROJECT.id, id);
            }
            if (project.getKey() == null) {
                insert.set(PROJECT.key, "PROJ" + id);
            }
            if (project.getName() == null) {
                insert.set(PROJECT.name, "Project " + id);
            }

            insert.execute();
            return id;
        };
    }
}
