package com.atlassian.jira.junit.rules;

import org.junit.rules.RuleChain;
import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

/**
 * A {@link TestRule} which provides a {@link AttachToDatabase} instance with a pre-initialised {@link MockitoContainer}.
 * <p/>
 * Use this instead of initialising the {@link AttachToDatabase} rule directly.
 * <pre>
 * &#64;Rule public DatabaseContainer database = DatabaseContainer.rule(this);
 * </pre>
 * or
 * <pre>
 * &#64;Rule public MockitoContainer container = MockitoMocksInContainer.rule(this);
 * &#64;Rule public DatabaseContainer database = DatabaseContainer.withContainer(container);
 * </pre>
 * where <tt>this</tt> refers to the class under test.
 *
 * @see AttachToDatabase
 * @since v7.1
 */
public class DatabaseContainer implements TestRule {
    private final RuleChain innerChain;
    private final AttachToDatabase attachToDatabase;
    private final MockitoContainer mockitoContainer;

    private DatabaseContainer(MockitoContainer mockitoContainer, AttachToDatabase attachToDatabase) {
        this.attachToDatabase = attachToDatabase;
        this.mockitoContainer = mockitoContainer;
        this.innerChain = RuleChain.outerRule(mockitoContainer)
                .around(attachToDatabase);
    }

    public static DatabaseContainer rule(Object test) {
        return withContainer(new MockitoContainer(test));
    }

    public static DatabaseContainer withContainer(MockitoContainer mockitoContainer) {
        final AttachToDatabase attachToDatabase = new AttachToDatabase(mockitoContainer);
        return new DatabaseContainer(mockitoContainer, attachToDatabase);
    }

    @Override
    public Statement apply(Statement base, Description description) {
        return innerChain.apply(base, description);
    }

    public AttachToDatabase getAttachToDatabase() {
        return attachToDatabase;
    }

    public MockitoContainer getMockitoContainer() {
        return mockitoContainer;
    }
}
