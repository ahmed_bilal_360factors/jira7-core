package com.atlassian.jira.junit.rules;

import com.atlassian.jira.config.database.DatabaseConfig;
import com.atlassian.jira.config.database.DatabaseConfigurationManager;
import com.atlassian.jira.config.database.Datasource;
import com.atlassian.jira.database.DatabaseAccessor;
import com.atlassian.jira.database.DatabaseAccessorImpl;
import com.atlassian.jira.database.DatabaseSchema;
import com.atlassian.jira.database.DbConnection;
import com.atlassian.jira.database.DefaultQueryDslAccessor;
import com.atlassian.jira.database.QueryDslAccessor;
import com.atlassian.jira.ofbiz.DefaultOfBizDelegator;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import org.apache.commons.lang3.time.StopWatch;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.mockito.Mockito;
import org.ofbiz.core.entity.DelegatorInterface;
import org.ofbiz.core.entity.GenericDelegator;
import org.ofbiz.core.entity.config.DatasourceInfo;
import org.ofbiz.core.entity.config.EntityConfigUtil;
import org.ofbiz.core.entity.config.JdbcDatasourceInfo;
import org.ofbiz.core.entity.transaction.DBCPConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Initialises the container pieces you need to attach to a real database when running unit tests.
 * <p/>
 * This rule has the side effect of clearing all tables in the database schema after each test is run. Use {@link DatabaseContainer}
 * to activate this rule in your tests.
 * <p/>
 * Note that our current intention is to only run these tests against PostgreSQL. As such these tests will not work with
 * other databases out of the box. Further changes may be required.
 *
 * @see DatabaseContainer
 * @since v7.1
 */
public class AttachToDatabase extends TestWatcher {
    private static final Logger log = LoggerFactory.getLogger(AttachToDatabase.class);

    private static final String SCHEMA_NAME = "PUBLIC";
    private static final String DATASOURCE_NAME = "defaultDS";

    private final MockitoContainer mockitoContainer;

    private QueryDslAccessor queryDslAccessor;
    private OfBizDelegator ofBizDelegator;
    private DatabaseConfig databaseConfig;
    /**
     * Instead of instantiating this directly, you probably want to use {@link DatabaseContainer#rule(Object)}
     * @param mockitoContainer the container to load components into
     */
    public AttachToDatabase(MockitoContainer mockitoContainer) {
        this.mockitoContainer = mockitoContainer;
    }

    @Override
    protected void starting(Description description) {
        mockitoContainer.getMockWorker().getMockDatabaseConfigurationService().setSchemaName(SCHEMA_NAME);
        DatabaseSchema.reset();

        final String dataSourceName = DATASOURCE_NAME;
        final DatasourceInfo datasourceInfo = createDynamicDatasourceInfo();
        final String fieldType = datasourceInfo.getFieldTypeName();

        DatabaseConfigurationManager databaseConfigurationManager = Mockito.mock(DatabaseConfigurationManager.class);
        Datasource datasource = Mockito.mock(Datasource.class);

        databaseConfig = new DatabaseConfig(dataSourceName, DatabaseConfig.DEFAULT_DELEGATOR_NAME, fieldType, SCHEMA_NAME, datasource);
        Mockito.when(databaseConfigurationManager.getDatabaseConfiguration()).thenReturn(databaseConfig);

        //Create tables from schema using OfBiz magic
        GenericDelegator.removeGenericDelegator("default");
        GenericDelegator genericDelegator = GenericDelegator.getGenericDelegator("default");

        DatabaseAccessor databaseAccessor = new DatabaseAccessorImpl(databaseConfigurationManager, genericDelegator);
        queryDslAccessor = new DefaultQueryDslAccessor(databaseAccessor, genericDelegator, databaseConfigurationManager);
        ofBizDelegator = new DefaultOfBizDelegator(genericDelegator);

        mockitoContainer.getMockWorker().addMock(DelegatorInterface.class, genericDelegator);
        mockitoContainer.getMockWorker().addMock(DatabaseConfigurationManager.class, databaseConfigurationManager);
        mockitoContainer.getMockWorker().addMock(OfBizDelegator.class, ofBizDelegator);
        mockitoContainer.getMockWorker().addMock(QueryDslAccessor.class, queryDslAccessor);

        //If the database is still dirty, need to cleanup before running test.
        queryDslAccessor.execute(dbConnection -> {
            try {
                Connection connection = dbConnection.getJdbcConnection();

                if (isDirty(connection)) {
                    log.warn("Previous test failed to cleanup");
                    cleanDatabase(connection);
                }

                //Set the dirty flag before running the test
                //Note that we always INSERT, because cleanDatabase() will TRUNCATE __dirty
                connection.prepareStatement("INSERT INTO __dirty VALUES (TRUE)").execute();

            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        });
    }

    private DatasourceInfo createDynamicDatasourceInfo() {
        DatasourceInfo sourceDatasourceInfo = EntityConfigUtil.getInstance().getDatasourceInfo(DATASOURCE_NAME);
        final JdbcDatasourceInfo sourceJdbcInfo = sourceDatasourceInfo.getJdbcDatasource();

        final DatasourceInfo newDsInfo = new DatasourceInfo(sourceDatasourceInfo.getName(), sourceDatasourceInfo.getFieldTypeName(), sourceDatasourceInfo.getSchemaName(),
                new JdbcDatasourceInfo(sourceJdbcInfo.getUri(), sourceJdbcInfo.getDriverClassName(),
                        sourceJdbcInfo.getUsername(), sourceJdbcInfo.getPassword(), sourceJdbcInfo.getIsolationLevel(),
                        sourceJdbcInfo.getConnectionProperties(), sourceJdbcInfo.getConnectionPoolInfo())
        );
        EntityConfigUtil.getInstance().addDatasourceInfo(newDsInfo);
        return newDsInfo;
    }

    @Override
    protected void succeeded(Description description) {
        super.succeeded(description);
    }

    @Override
    protected void finished(Description description) {
        StopWatch stopwatch = new StopWatch();
        stopwatch.start();
        queryDslAccessor.execute(dbConnection -> cleanDatabase(dbConnection.getJdbcConnection()));
        stopwatch.stop();
        log.info("Clean database ran in {} millis", stopwatch.getTime());
        Mockito.validateMockitoUsage();
        DBCPConnectionFactory.removeDatasource(DATASOURCE_NAME);
    }

    /**
     * Returns true if the database is in an inconsistent state. i.e. the last test did not clean up.
     */
    private boolean isDirty(Connection connection) throws SQLException {
        //Table might not exist if we haven't used this image before
        //Note that we can't use 'IF NOT EXISTS' as it was introduced in 9.1
        try (ResultSet dirty_table_exists = connection
                .getMetaData()
                .getTables(null, DatabaseSchema.getSchemaName(), "__dirty", new String[]{"TABLE"})) {

            if (! dirty_table_exists.next()) {
                connection
                        .prepareStatement("CREATE TABLE __dirty (is_dirty BOOLEAN)")
                        .execute();
            }
        }

        //Perform actual check
        try (ResultSet state = connection
                .prepareStatement("SELECT is_dirty FROM __dirty LIMIT 1")
                .executeQuery()) {

            return state.next() && state.getBoolean(1);
        }
    }

    private void cleanDatabase(Connection connection) {
        try {
            try (ResultSet tables = connection.getMetaData().getTables(null, DatabaseSchema.getSchemaName(), null, new String[]{"TABLE"})) {
                while (tables.next()) {
                    String tableName = tables.getString("TABLE_NAME");
                    ResultSet rs = connection.prepareStatement("SELECT COUNT(*) C FROM " + tableName).executeQuery();
                    if (rs.next() && rs.getInt("C") > 0) {
                        //DELETE FROM is faster than TRUNCATE by a factor of 3 for <1000 records
                        connection.prepareStatement("DELETE FROM " + tableName).execute();
                    }
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public QueryDslAccessor getQueryDslAccessor() {
        return queryDslAccessor;
    }

    public OfBizDelegator getOfBizDelegator() {
        return ofBizDelegator;
    }

}