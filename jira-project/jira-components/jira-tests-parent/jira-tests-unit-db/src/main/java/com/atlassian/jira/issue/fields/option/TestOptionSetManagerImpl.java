package com.atlassian.jira.issue.fields.option;

import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.database.DbConnection;
import com.atlassian.jira.database.QueryDslAccessor;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.junit.rules.DatabaseContainer;
import com.atlassian.jira.model.querydsl.QFieldConfiguration;
import com.atlassian.utt.concurrency.Barrier;
import com.atlassian.utt.concurrency.TestThread;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;

import javax.annotation.Nonnull;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import static com.atlassian.jira.model.querydsl.QFieldConfiguration.FIELD_CONFIGURATION;
import static com.atlassian.jira.model.querydsl.QOptionConfiguration.OPTION_CONFIGURATION;
import static com.atlassian.utt.concurrency.TestThread.runTest;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestOptionSetManagerImpl {
    private static final String ISSUE_TYPE_FIELD_ID = "IssueType";
    private static final Long FIELD_CONFIG_ID = 42L;

    @Rule
    public DatabaseContainer databaseContainer = DatabaseContainer.rule(this);

    private OptionSetManagerImpl optionSetManager;

    @Mock
    private ConstantsManager constantsManager;

    @Mock
    private FieldConfig fieldConfig;

    private QueryDslAccessor queryDslAccessor;

    @Before
    public void setUp() throws Exception {
        queryDslAccessor = databaseContainer.getAttachToDatabase().getQueryDslAccessor();

        queryDslAccessor.execute(db -> {
            db.insert(FIELD_CONFIGURATION)
                    .set(FIELD_CONFIGURATION.id, FIELD_CONFIG_ID)
                    .set(FIELD_CONFIGURATION.name, "Some name")
                    .set(FIELD_CONFIGURATION.description, "Description")
                    .set(FIELD_CONFIGURATION.fieldid, ISSUE_TYPE_FIELD_ID)
                    .set(FIELD_CONFIGURATION.customfield, 1L)
                    .execute();
        });

        optionSetManager = new OptionSetManagerImpl(constantsManager, queryDslAccessor);

        when(fieldConfig.getId()).thenReturn(FIELD_CONFIG_ID);
        when(fieldConfig.getFieldId()).thenReturn(ISSUE_TYPE_FIELD_ID);
    }

    @Test
    public void creatingAnOptionSetIsStoredCorrectly() {
        mockIssueTypeExists("1", "2", "3");

        OptionSet optionSet = optionSetManager.createOptionSet(fieldConfig, Lists.newArrayList("1", "2", "3"));

        assertThat(optionSet.getOptionIds(), contains("1", "2", "3"));
    }

    @Test
    public void updatingAnOptionSetReplacesTheCurrentOptionSetItems() {
        mockIssueTypeExists("1", "2", "3", "4");
        optionSetManager.createOptionSet(fieldConfig, Lists.newArrayList("1", "2"));

        OptionSet optionSet = optionSetManager.updateOptionSet(fieldConfig, Lists.newArrayList("3", "4"));

        assertThat(optionSet.getOptionIds(), contains("3", "4"));
    }

    @Test
    public void removingAnOptionMaintainsTheOptionSetOrdering() {
        mockIssueTypeExists("1", "2", "3");
        optionSetManager.createOptionSet(fieldConfig, Lists.newArrayList("1", "2", "3"));

        OptionSet optionSet = optionSetManager.removeOptionFromOptionSet(fieldConfig, "2");

        assertThat(optionSet.getOptionIds(), contains("1", "3"));
    }

    @Test
    public void removingAnOptionThatDoesNotExistInTheSetDoesNothing() {
        mockIssueTypeExists("1", "2", "3");
        optionSetManager.createOptionSet(fieldConfig, Lists.newArrayList("1", "2", "3"));

        OptionSet optionSet = optionSetManager.removeOptionFromOptionSet(fieldConfig, "20");

        assertThat(optionSet.getOptionIds(), contains("1", "2", "3"));
    }

    @Test
    public void addAnOptionToAnOptionSetAppendsItToTheEnd() {
        mockIssueTypeExists("1", "2", "3");
        optionSetManager.createOptionSet(fieldConfig, Lists.newArrayList("1", "2"));

        OptionSet optionSet = optionSetManager.addOptionToOptionSet(fieldConfig, "3");

        assertThat(optionSet.getOptionIds(), contains("1", "2", "3"));
    }

    @Test
    public void optionCanBeAddedToAnEmptyOptionSet() {
        final String optionId = "1";
        mockIssueTypeExists(optionId);

        OptionSet optionSet = optionSetManager.addOptionToOptionSet(fieldConfig, optionId);

        assertThat(optionSet.getOptionIds(), contains("1"));
    }

    @Test
    public void gettingOptionSetIgnoresIssueTypesThatDoNotExist() {
        mockIssueTypeExists("1", "2");
        optionSetManager.createOptionSet(fieldConfig, Lists.newArrayList("1", "4", "2"));

        OptionSet optionSet = optionSetManager.getOptionsForConfig(fieldConfig);

        assertThat(optionSet.getOptionIds(), contains("1", "2"));
    }

    @Test
    public void deletingTheOptionSetRemovesAllEntriesForIt() {
        mockIssueTypeExists("1", "2");
        optionSetManager.createOptionSet(fieldConfig, Lists.newArrayList("1", "2"));

        optionSetManager.removeOptionSet(fieldConfig);

        assertThat(optionSetManager.getOptionsForConfig(fieldConfig).getOptionIds(), hasSize(0));
    }

    /**
     * <p>
     * This is to test to make sure that we lock the option set via the {@link QFieldConfiguration#FIELD_CONFIGURATION}.
     * We show this locking by trying to add two options to an option set at the same time.
     * </p>
     * <p>
     * If you want to show this failing you may comment out the body of
     * {@link OptionSetManagerImpl#lockFieldConfigurationForEditing(DbConnection, Long)}
     * </p>
     **/
    @Test
    public void runningWithFieldConfigurationLockedBlocksConcurrentModificationsOfTheOptionSet() {
        mockIssueTypeExists("1", "2", "3", "4");
        optionSetManager.createOptionSet(fieldConfig, Lists.newArrayList("1", "2"));

        // This barrier is used to get us into the state that the first thread is at the process of adding the option
        // and the second thread can begin trying to add their own.
        final Barrier firstThreadHasGottenSeqForOption = new Barrier();

        // The first thread then uses the latch to wait until the second thread has indicated that it is about to add
        // an option. However, this should never happen because we should be able to happen as it should be locked
        // by the field configuration
        final CountDownLatch concurrentOptionAddCountDownLatch = new CountDownLatch(1);

        final TestThread addOptionThreeThread = new TestThread("addThree") {
            @Override
            protected void go() throws Exception {
                final OptionSetManager optionSetManager = new OptionSetManagerImpl(constantsManager, queryDslAccessor) {
                    @Override
                    void insertSingleOption(@Nonnull DbConnection db, @Nonnull String fieldId, @Nonnull Long fieldConfigId,
                                            @Nonnull String optionId, @Nonnull Long sequence) {
                        // At this point we know the sequence for the option we are wanting to add for this thread.
                        firstThreadHasGottenSeqForOption.signal();

                        try {
                            // We wait for the second thread to begin the process of adding an option. This should
                            // never be signalled because the second thread is blocked by the database.
                            boolean wasSignalled = concurrentOptionAddCountDownLatch.await(1000, TimeUnit.MILLISECONDS);

                            assertThat("It should not be possible that both the first and second add option should be " +
                                            "completing that option addition at the same time. Therefore, this failure shows a " +
                                            "race condition.",
                                    wasSignalled, equalTo(false));
                        } catch (InterruptedException e) {
                            fail("This should not have been interrupted");
                        }

                        super.insertSingleOption(db, fieldId, fieldConfigId, optionId, sequence);
                    }
                };

                optionSetManager.addOptionToOptionSet(fieldConfig, "3");
            }
        };

        final TestThread addOptionFourThread = new TestThread("addFour") {
            @Override
            protected void go() throws Exception {
                // wait until the first thread has gotten the sequence for the insert so we can begin to try and insert
                // another option at the same time.
                firstThreadHasGottenSeqForOption.await();

                final OptionSetManager optionSetManager = new OptionSetManagerImpl(constantsManager, queryDslAccessor) {
                    @Override
                    void insertSingleOption(@Nonnull DbConnection db, @Nonnull String fieldId, @Nonnull Long fieldConfigId,
                                            @Nonnull String optionId, @Nonnull Long sequence) {
                        // It should not get into this function until the thread above is fully finished. If it gets
                        // here before that an assertion exception is thrown in the thread above.
                        concurrentOptionAddCountDownLatch.countDown();

                        super.insertSingleOption(db, fieldId, fieldConfigId, optionId, sequence);
                    }
                };

                optionSetManager.addOptionToOptionSet(fieldConfig, "4");
            }
        };

        runTest(addOptionThreeThread, addOptionFourThread);

        assertThat(optionSetManager.getOptionsForConfig(fieldConfig).getOptionIds(), contains("1", "2", "3", "4"));
        assertThatOptionSetSequencesAreUnique();
    }

    private void assertThatOptionSetSequencesAreUnique() {
        queryDslAccessor.executeQuery(db -> {
            List<Long> sequences = db.newSqlQuery().select(OPTION_CONFIGURATION.sequence)
                    .from(OPTION_CONFIGURATION)
                    .where(OPTION_CONFIGURATION.fieldid.eq(ISSUE_TYPE_FIELD_ID))
                    .where(OPTION_CONFIGURATION.fieldconfig.eq(FIELD_CONFIG_ID))
                    .orderBy(OPTION_CONFIGURATION.sequence.desc())
                    .fetch();
            assertThat("Sequence numbers for the option set should all be unique", sequences.stream().distinct().count(), equalTo((long)sequences.size()));
            return null;
        });
    }

    private void mockIssueTypeExists(String... issueTypeIds) {
        for (String issueTypeId : issueTypeIds) {
            IssueType issueType = mock(IssueType.class);
            when(issueType.getId()).thenReturn(issueTypeId);
            when(constantsManager.getConstantObject(ISSUE_TYPE_FIELD_ID, issueTypeId)).thenReturn(issueType);
        }
    }
}