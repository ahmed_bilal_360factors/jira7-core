Feature: Renaissance Service Desk migration for Agent

  Scenario Outline: Groups with agent access are migrated if ALL users have USE/ADMINISTER/SYSADMIN permission WITH good existing JIRA license
    Given Service Desk license <service desk license> in plugin store
    And JIRA license <jira license> in old store
    And following groups:
      | name                | permissions                            |
      | users               | USE                                    |
      | admins              | ADMINISTER                             |
      | agents              | com.atlassian.servicedesk.agent.access |
      | service-desk-agents | com.atlassian.servicedesk.agent.access |
      | <anyone>            | com.atlassian.servicedesk.agent.access |
    And group 'service-desk-agents' with attribute 'synch.created.by.jira.service.desk' set to 'synch.created.by.jira.service.desk'
    And following users:
      | user     | groups                    |
      | agent1   | users, agents             |
      | agent-sd | users, service-desk-agent |
      | admin    | admins, users             |
    When migration runs
    Then groups have been migrated as follows:
      | name                | permissions                            | applications             | default applications     |
      | users               | USE                                    | jira-software, jira-core | jira-software, jira-core |
      | admins              | ADMINISTER                             | jira-software, jira-core |                          |
      | agents              | com.atlassian.servicedesk.agent.access | jira-servicedesk         |                          |
      | service-desk-agents | com.atlassian.servicedesk.agent.access | jira-servicedesk         | jira-servicedesk         |
    And Service Desk property 'com.atlassian.servicedesk.renaissance.migration.type' is set to value 'ABP'
    And Renaissance migration flagged done
    And there is license <service desk license> in multistore for jira-servicedesk application
    And there is license <jira license> in multistore for jira-software application
    Examples:
      | service desk license         | jira license       |
      | SERVICE_DESK_ABP_UNLIMITED   | JIRA_6X_COMMERCIAL |
      | SERVICE_DESK_RENAISSANCE_RBP | JIRA_6X_COMMERCIAL |

  Scenario Outline: Groups with agent access are migrated if ALL users have USE/ADMINISTER/SYSADMIN permission AND SD license removed
    Given Service Desk license <service desk license> in migration store
    And JIRA license <jira license> in old store
    And following groups:
      | name                | permissions                            |
      | users               | USE                                    |
      | admins              | ADMINISTER                             |
      | agents              | com.atlassian.servicedesk.agent.access |
      | service-desk-agents | com.atlassian.servicedesk.agent.access |
      | <anyone>            | com.atlassian.servicedesk.agent.access |
    And group 'service-desk-agents' with attribute 'synch.created.by.jira.service.desk' set to 'synch.created.by.jira.service.desk'
    And following users:
      | user     | groups                    |
      | agent1   | users, agents             |
      | agent-sd | users, service-desk-agent |
      | admin    | admins, users             |
    When migration runs
    Then groups have been migrated as follows:
      | name                | permissions                            | applications             | default applications     |
      | users               | USE                                    | jira-software, jira-core | jira-software, jira-core |
      | admins              | ADMINISTER                             | jira-software, jira-core |                          |
      | agents              | com.atlassian.servicedesk.agent.access | jira-servicedesk         |                          |
      | service-desk-agents | com.atlassian.servicedesk.agent.access | jira-servicedesk         | jira-servicedesk         |
    And Service Desk property 'com.atlassian.servicedesk.renaissance.migration.type' is set to value 'ABP'
    And Renaissance migration flagged done
    And license <service desk license> is not in multistore
    And there is license <jira license> in multistore for jira-software application
    Examples:
      | service desk license         | jira license       |
      | SERVICE_DESK_ABP_UNLIMITED   | JIRA_6X_COMMERCIAL |
      | SERVICE_DESK_RENAISSANCE_RBP | JIRA_6X_COMMERCIAL |

  Scenario Outline: Groups with agent access are migrated if ALL users have USE/ADMINISTER/SYSADMIN permission for combined licenses
    And JIRA license <jira license> in old store
    And following groups:
      | name                | permissions                            |
      | users               | USE                                    |
      | admins              | ADMINISTER                             |
      | agents              | com.atlassian.servicedesk.agent.access |
      | service-desk-agents | com.atlassian.servicedesk.agent.access |
      | <anyone>            | com.atlassian.servicedesk.agent.access |
    And group 'service-desk-agents' with attribute 'synch.created.by.jira.service.desk' set to 'synch.created.by.jira.service.desk'
    And following users:
      | user     | groups                    |
      | agent1   | users, agents             |
      | agent-sd | users, service-desk-agent |
      | admin    | admins, users             |
    When migration runs
    Then groups have been migrated as follows:
      | name                | permissions                            | applications             | default applications     |
      | users               | USE                                    | jira-software, jira-core | jira-software, jira-core |
      | admins              | ADMINISTER                             | jira-software, jira-core |                          |
      | agents              | com.atlassian.servicedesk.agent.access | jira-servicedesk         |                          |
      | service-desk-agents | com.atlassian.servicedesk.agent.access | jira-servicedesk         | jira-servicedesk         |
    And Service Desk property 'com.atlassian.servicedesk.renaissance.migration.type' is set to value 'ABP'
    And Renaissance migration flagged done
    And there is license <jira license> in multistore for jira-servicedesk application
    Examples:
      | jira license                                   |
      | JIRA_6X_ELA_SERVICEDESK_ABP                    |

  Scenario Outline: Groups with agent access are migrated if ALL users have USE/ADMINISTER/SYSADMIN permission with only SD license available
    Given Service Desk license <service desk license> in plugin store
    And no JIRA license in old store
    And following groups:
      | name                | permissions                                        |
      | users               | USE                                                |
      | admins              | ADMINISTER, com.atlassian.servicedesk.agent.access |
      | agents              | com.atlassian.servicedesk.agent.access             |
      | service-desk-agents | com.atlassian.servicedesk.agent.access             |
      | <anyone>            | USE, SYSTEM_ADMIN                                  |
    And group 'service-desk-agents' with attribute 'synch.created.by.jira.service.desk' set to 'synch.created.by.jira.service.desk'
    And following users:
      | user     | groups                    |
      | agent1   | users, agents             |
      | agent-sd | users, service-desk-agent |
      | admin    | admins, users             |
    When migration runs
    Then groups have been migrated as follows:
      | name                | permissions                                        | applications                               | default applications     |
      | users               | USE                                                | jira-software, jira-core                   | jira-software, jira-core |
      | admins              | ADMINISTER, com.atlassian.servicedesk.agent.access | jira-software, jira-core, jira-servicedesk |                          |
      | agents              | com.atlassian.servicedesk.agent.access             | jira-servicedesk                           |                          |
      | service-desk-agents | com.atlassian.servicedesk.agent.access             | jira-servicedesk                           | jira-servicedesk         |
    And Service Desk property 'com.atlassian.servicedesk.renaissance.migration.type' is set to value 'ABP'
    And Renaissance migration flagged done
    And there is license <service desk license> in multistore for jira-servicedesk application
    Examples:
      | service desk license         |
      | SERVICE_DESK_ABP_UNLIMITED   |
      | SERVICE_DESK_RENAISSANCE_RBP |

  Scenario Outline: Groups with agent access are migrated if ALL users have USE/ADMINISTER/SYSADMIN permission with valid SD license in multistore
    Given Service Desk license <service desk license> in plugin store
    Given license JIRA7_SERVICE_DESK_RBP_NON_BACKWARD_COMPATIBLE in multistore
    And no JIRA license in old store
    And following groups:
      | name                | permissions                                        |
      | users               | USE                                                |
      | admins              | ADMINISTER, com.atlassian.servicedesk.agent.access |
      | agents              | com.atlassian.servicedesk.agent.access             |
      | service-desk-agents | com.atlassian.servicedesk.agent.access             |
      | <anyone>            | USE, SYSTEM_ADMIN                                  |
    And group 'service-desk-agents' with attribute 'synch.created.by.jira.service.desk' set to 'synch.created.by.jira.service.desk'
    And following users:
      | user     | groups                    |
      | agent1   | users, agents             |
      | agent-sd | users, service-desk-agent |
      | admin    | admins, users             |
    When migration runs
    Then groups have been migrated as follows:
      | name                | permissions                                        | applications                               | default applications     |
      | users               | USE                                                | jira-software, jira-core                   | jira-software, jira-core |
      | admins              | ADMINISTER, com.atlassian.servicedesk.agent.access | jira-software, jira-core, jira-servicedesk |                          |
      | agents              | com.atlassian.servicedesk.agent.access             | jira-servicedesk                           |                          |
      | service-desk-agents | com.atlassian.servicedesk.agent.access             | jira-servicedesk                           | jira-servicedesk         |
    And Service Desk property 'com.atlassian.servicedesk.renaissance.migration.type' is set to value 'ABP'
    And Renaissance migration flagged done
    And there is license JIRA7_SERVICE_DESK_RBP_NON_BACKWARD_COMPATIBLE in multistore for jira-servicedesk application
    And license <service desk license> is not in multistore
    Examples:
      | service desk license         |
      | SERVICE_DESK_ABP_UNLIMITED   |
      | SERVICE_DESK_RENAISSANCE_RBP |

  Scenario Outline: Groups with agent access are migrated if ALL users have USE/ADMINISTER/SYSADMIN permission with valid Software license in multistore
    Given Service Desk license <service desk license> in plugin store
    Given license JIRA_6X_COMMERCIAL in multistore
    And no JIRA license in old store
    And following groups:
      | name                | permissions                                        |
      | users               | USE                                                |
      | admins              | ADMINISTER, com.atlassian.servicedesk.agent.access |
      | agents              | com.atlassian.servicedesk.agent.access             |
      | service-desk-agents | com.atlassian.servicedesk.agent.access             |
      | <anyone>            | USE, SYSTEM_ADMIN                                  |
    And group 'service-desk-agents' with attribute 'synch.created.by.jira.service.desk' set to 'synch.created.by.jira.service.desk'
    And following users:
      | user     | groups                    |
      | agent1   | users, agents             |
      | agent-sd | users, service-desk-agent |
      | admin    | admins, users             |
    When migration runs
    Then groups have been migrated as follows:
      | name                | permissions                                        | applications                               | default applications     |
      | users               | USE                                                | jira-software, jira-core                   | jira-software, jira-core |
      | admins              | ADMINISTER, com.atlassian.servicedesk.agent.access | jira-software, jira-core, jira-servicedesk |                          |
      | agents              | com.atlassian.servicedesk.agent.access             | jira-servicedesk                           |                          |
      | service-desk-agents | com.atlassian.servicedesk.agent.access             | jira-servicedesk                           | jira-servicedesk         |
    And Service Desk property 'com.atlassian.servicedesk.renaissance.migration.type' is set to value 'ABP'
    And Renaissance migration flagged done
    And there is license JIRA_6X_COMMERCIAL in multistore for jira-software application
    And license <service desk license> is not in multistore
    Examples:
      | service desk license         |
      | SERVICE_DESK_ABP_UNLIMITED   |
      | SERVICE_DESK_RENAISSANCE_RBP |

  Scenario Outline: Groups with agent access are not migrated if ALL users do not have USE/ADMINISTER/SYSADMIN permission
    Given Service Desk license <service desk license> in plugin store
    And JIRA license <jira license> in old store
    And following groups:
      | name                | permissions                            |
      | users               | USE                                    |
      | admins              | ADMINISTER                             |
      | agents              | com.atlassian.servicedesk.agent.access |
      | service-desk-agents | com.atlassian.servicedesk.agent.access |
      | <anyone>            | com.atlassian.servicedesk.agent.access |
    And following users:
      | user     | groups              |
      | agent1   | agents              |
      | agent-sd | service-desk-agents |
      | admin    | admins, users       |
    When migration runs
    Then groups have been migrated as follows:
      | name                | permissions                            | applications             | default applications     |
      | users               | USE                                    | jira-software, jira-core | jira-software, jira-core |
      | admins              | ADMINISTER                             | jira-software, jira-core |                          |
      | agents              | com.atlassian.servicedesk.agent.access |                          |                          |
      | service-desk-agents | com.atlassian.servicedesk.agent.access |                          |                          |
    And Service Desk property 'com.atlassian.servicedesk.renaissance.migration.type' is set to value 'ABP'
    And there is license <service desk license> in multistore for jira-servicedesk application
    And there is license <jira license> in multistore for jira-software application
    And Renaissance migration flagged done
    Examples:
      | service desk license         | jira license       |
      | SERVICE_DESK_ABP_UNLIMITED   | JIRA_6X_COMMERCIAL |
      | SERVICE_DESK_RENAISSANCE_RBP | JIRA_6X_COMMERCIAL |

  Scenario Outline: Groups with agent access are not migrated if ALL users do not have USE/ADMINISTER/SYSADMIN permission and SD license removed
    Given Service Desk license <service desk license> in migration store
    And JIRA license <jira license> in old store
    And following groups:
      | name                | permissions                            |
      | users               | USE                                    |
      | admins              | ADMINISTER                             |
      | agents              | com.atlassian.servicedesk.agent.access |
      | service-desk-agents | com.atlassian.servicedesk.agent.access |
      | <anyone>            | com.atlassian.servicedesk.agent.access |
    And following users:
      | user     | groups              |
      | agent1   | agents              |
      | agent-sd | service-desk-agents |
      | admin    | admins, users       |
    When migration runs
    Then groups have been migrated as follows:
      | name                | permissions                            | applications             | default applications     |
      | users               | USE                                    | jira-software, jira-core | jira-software, jira-core |
      | admins              | ADMINISTER                             | jira-software, jira-core |                          |
      | agents              | com.atlassian.servicedesk.agent.access |                          |                          |
      | service-desk-agents | com.atlassian.servicedesk.agent.access |                          |                          |
    And Service Desk property 'com.atlassian.servicedesk.renaissance.migration.type' is set to value 'ABP'
    And license <service desk license> is not in multistore
    And there is license <jira license> in multistore for jira-software application
    And Renaissance migration flagged done
    Examples:
      | service desk license         | jira license       |
      | SERVICE_DESK_ABP_UNLIMITED   | JIRA_6X_COMMERCIAL |
      | SERVICE_DESK_RENAISSANCE_RBP | JIRA_6X_COMMERCIAL |

  Scenario Outline: Groups with agent access are not migrated if ALL users do not have USE/ADMINISTER/SYSADMIN permission for combined licenses
    Given Service Desk license NONE in migration store
    And JIRA license <jira license> in old store
    And following groups:
      | name                | permissions                            |
      | users               | USE                                    |
      | admins              | ADMINISTER                             |
      | agents              | com.atlassian.servicedesk.agent.access |
      | service-desk-agents | com.atlassian.servicedesk.agent.access |
      | <anyone>            | com.atlassian.servicedesk.agent.access |
    And following users:
      | user     | groups              |
      | agent1   | agents              |
      | agent-sd | service-desk-agents |
      | admin    | admins, users       |
    When migration runs
    Then groups have been migrated as follows:
      | name                | permissions                            | applications             | default applications     |
      | users               | USE                                    | jira-software, jira-core | jira-software, jira-core |
      | admins              | ADMINISTER                             | jira-software, jira-core |                          |
      | agents              | com.atlassian.servicedesk.agent.access |                          |                          |
      | service-desk-agents | com.atlassian.servicedesk.agent.access |                          |                          |
    And Service Desk property 'com.atlassian.servicedesk.renaissance.migration.type' is set to value 'ABP'
    And there is license <jira license> in multistore for jira-servicedesk application
    And Renaissance migration flagged done
    Examples:
      | jira license                            |
      | JIRA_6X_ELA_SERVICEDESK_ABP             |

  Scenario Outline: Service-desk-agents group with ADMINSTER permission will not be set as default
    Given Service Desk license <service desk license> in plugin store
    And JIRA license <jira license> in old store
    And following groups:
      | name                | permissions                                        |
      | users               | USE                                                |
      | admins              | ADMINISTER                                         |
      | service-desk-agents | com.atlassian.servicedesk.agent.access, ADMINISTER |
      | <anyone>            | com.atlassian.servicedesk.agent.access             |
    And group 'service-desk-agents' with attribute 'synch.created.by.jira.service.desk' set to 'synch.created.by.jira.service.desk'
    And following users:
      | user     | groups              |
      | agent-sd | service-desk-agents |
      | admin    | admins, users       |
    When migration runs
    Then groups have been migrated as follows:
      | name                | permissions                                        | applications                               | default applications     |
      | users               | USE                                                | jira-software, jira-core                   | jira-software, jira-core |
      | admins              | ADMINISTER                                         | jira-software, jira-core                   |                          |
      | service-desk-agents | com.atlassian.servicedesk.agent.access, ADMINISTER | jira-servicedesk, jira-software, jira-core |                          |
    And Service Desk property 'com.atlassian.servicedesk.renaissance.migration.type' is set to value 'ABP'
    And Renaissance migration flagged done
    And there is license <jira license> in multistore for jira-software application
    And there is license <service desk license> in multistore for jira-servicedesk application
    Examples:
      | service desk license         | jira license       |
      | SERVICE_DESK_ABP_UNLIMITED   | JIRA_6X_COMMERCIAL |
      | SERVICE_DESK_RENAISSANCE_RBP | JIRA_6X_COMMERCIAL |

  Scenario Outline: Service-desk-agents group with ADMINSTER permission will not be set as default AND SD License Removed
    Given Service Desk license <service desk license> in migration store
    And JIRA license <jira license> in old store
    And following groups:
      | name                | permissions                                        |
      | users               | USE                                                |
      | admins              | ADMINISTER                                         |
      | service-desk-agents | com.atlassian.servicedesk.agent.access, ADMINISTER |
      | <anyone>            | com.atlassian.servicedesk.agent.access             |
    And group 'service-desk-agents' with attribute 'synch.created.by.jira.service.desk' set to 'synch.created.by.jira.service.desk'
    And following users:
      | user     | groups              |
      | agent-sd | service-desk-agents |
      | admin    | admins, users       |
    When migration runs
    Then groups have been migrated as follows:
      | name                | permissions                                        | applications                               | default applications     |
      | users               | USE                                                | jira-software, jira-core                   | jira-software, jira-core |
      | admins              | ADMINISTER                                         | jira-software, jira-core                   |                          |
      | service-desk-agents | com.atlassian.servicedesk.agent.access, ADMINISTER | jira-servicedesk, jira-software, jira-core |                          |
    And Service Desk property 'com.atlassian.servicedesk.renaissance.migration.type' is set to value 'ABP'
    And Renaissance migration flagged done
    And there is license <jira license> in multistore for jira-software application
    And license <service desk license> is not in multistore
    Examples:
      | service desk license         | jira license       |
      | SERVICE_DESK_ABP_UNLIMITED   | JIRA_6X_COMMERCIAL |
      | SERVICE_DESK_RENAISSANCE_RBP | JIRA_6X_COMMERCIAL |

  Scenario Outline: Service-desk-agents group with ADMINSTER permission will not be set as default for combined licenses
    Given Service Desk license NONE in plugin store
    And JIRA license <jira license> in old store
    And following groups:
      | name                | permissions                                        |
      | users               | USE                                                |
      | admins              | ADMINISTER                                         |
      | service-desk-agents | com.atlassian.servicedesk.agent.access, ADMINISTER |
      | <anyone>            | com.atlassian.servicedesk.agent.access             |
    And group 'service-desk-agents' with attribute 'synch.created.by.jira.service.desk' set to 'synch.created.by.jira.service.desk'
    And following users:
      | user     | groups              |
      | agent-sd | service-desk-agents |
      | admin    | admins, users       |
    When migration runs
    Then groups have been migrated as follows:
      | name                | permissions                                        | applications                               | default applications     |
      | users               | USE                                                | jira-software, jira-core                   | jira-software, jira-core |
      | admins              | ADMINISTER                                         | jira-software, jira-core                   |                          |
      | service-desk-agents | com.atlassian.servicedesk.agent.access, ADMINISTER | jira-servicedesk, jira-software, jira-core |                          |
    And Service Desk property 'com.atlassian.servicedesk.renaissance.migration.type' is set to value 'ABP'
    And Renaissance migration flagged done
    And there is license <jira license> in multistore for jira-servicedesk application
    Examples:
      | jira license                            |
      | JIRA_6X_ELA_SERVICEDESK_ABP             |

  Scenario Outline: Groups with agent access are not migrated if some users do not have USE/ADMINISTER/SYSADMIN permission
    Given Service Desk license <service desk license> in plugin store
    And JIRA license <jira license> in old store
    And following groups:
      | name                | permissions                            |
      | users               | USE                                    |
      | admins              | ADMINISTER                             |
      | agents              | com.atlassian.servicedesk.agent.access |
      | service-desk-agents | com.atlassian.servicedesk.agent.access |
      | <anyone>            | com.atlassian.servicedesk.agent.access |
    And following users:
      | user      | groups                     |
      | agent1    | agents                     |
      | agent2    | agents, users              |
      | agent-sd1 | service-desk-agents        |
      | agent-sd2 | service-desk-agents,admins |
      | admin     | admins, users              |
    When migration runs
    Then groups have been migrated as follows:
      | name                | permissions                            | applications             | default applications     |
      | users               | USE                                    | jira-software, jira-core | jira-software, jira-core |
      | admins              | ADMINISTER                             | jira-software, jira-core |                          |
      | agents              | com.atlassian.servicedesk.agent.access |                          |                          |
      | service-desk-agents | com.atlassian.servicedesk.agent.access |                          |                          |
    And Service Desk property 'com.atlassian.servicedesk.renaissance.migration.type' is set to value 'ABP'
    And Renaissance migration flagged done
    And there is license <service desk license> in multistore for jira-servicedesk application
    And there is license <jira license> in multistore for jira-software application
    Examples:
      | service desk license         | jira license       |
      | SERVICE_DESK_ABP_UNLIMITED   | JIRA_6X_COMMERCIAL |
      | SERVICE_DESK_RENAISSANCE_RBP | JIRA_6X_COMMERCIAL |

  Scenario Outline: Groups with agent access are not migrated if some users do not have USE/ADMINISTER/SYSADMIN permission AND SD license removed
    Given Service Desk license <service desk license> in migration store
    And JIRA license <jira license> in old store
    And following groups:
      | name                | permissions                            |
      | users               | USE                                    |
      | admins              | ADMINISTER                             |
      | agents              | com.atlassian.servicedesk.agent.access |
      | service-desk-agents | com.atlassian.servicedesk.agent.access |
      | <anyone>            | com.atlassian.servicedesk.agent.access |
    And following users:
      | user      | groups                     |
      | agent1    | agents                     |
      | agent2    | agents, users              |
      | agent-sd1 | service-desk-agents        |
      | agent-sd2 | service-desk-agents,admins |
      | admin     | admins, users              |
    When migration runs
    Then groups have been migrated as follows:
      | name                | permissions                            | applications             | default applications     |
      | users               | USE                                    | jira-software, jira-core | jira-software, jira-core |
      | admins              | ADMINISTER                             | jira-software, jira-core |                          |
      | agents              | com.atlassian.servicedesk.agent.access |                          |                          |
      | service-desk-agents | com.atlassian.servicedesk.agent.access |                          |                          |
    And Service Desk property 'com.atlassian.servicedesk.renaissance.migration.type' is set to value 'ABP'
    And Renaissance migration flagged done
    And license <service desk license> is not in multistore
    And there is license <jira license> in multistore for jira-software application
    Examples:
      | service desk license         | jira license       |
      | SERVICE_DESK_ABP_UNLIMITED   | JIRA_6X_COMMERCIAL |
      | SERVICE_DESK_RENAISSANCE_RBP | JIRA_6X_COMMERCIAL |

  Scenario Outline: Groups with agent access are not migrated if some users do not have USE/ADMINISTER/SYSADMIN permission combined licenses
    Given Service Desk license NONE in plugin store
    And JIRA license <jira license> in old store
    And following groups:
      | name                | permissions                            |
      | users               | USE                                    |
      | admins              | ADMINISTER                             |
      | agents              | com.atlassian.servicedesk.agent.access |
      | service-desk-agents | com.atlassian.servicedesk.agent.access |
      | <anyone>            | com.atlassian.servicedesk.agent.access |
    And following users:
      | user      | groups                     |
      | agent1    | agents                     |
      | agent2    | agents, users              |
      | agent-sd1 | service-desk-agents        |
      | agent-sd2 | service-desk-agents,admins |
      | admin     | admins, users              |
    When migration runs
    Then groups have been migrated as follows:
      | name                | permissions                            | applications             | default applications     |
      | users               | USE                                    | jira-software, jira-core | jira-software, jira-core |
      | admins              | ADMINISTER                             | jira-software, jira-core |                          |
      | agents              | com.atlassian.servicedesk.agent.access |                          |                          |
      | service-desk-agents | com.atlassian.servicedesk.agent.access |                          |                          |
    And Service Desk property 'com.atlassian.servicedesk.renaissance.migration.type' is set to value 'ABP'
    And Renaissance migration flagged done
    And there is license <jira license> in multistore for jira-servicedesk application
    Examples:
      | jira license                            |
      | JIRA_6X_ELA_SERVICEDESK_ABP             |

  Scenario Outline: Service-desk-agents group is not set to default if it has admin permission
    Given Service Desk license <service desk license> in plugin store
    And JIRA license <jira license> in old store
    And following groups:
      | name                | permissions                            |
      | users               | USE                                    |
      | admins              | ADMINISTER                             |
      | service-desk-agents | com.atlassian.servicedesk.agent.access |
      | <anyone>            | com.atlassian.servicedesk.agent.access |
    And following users:
      | user      | groups                      |
      | agent-sd1 | service-desk-agents, users  |
      | agent-sd2 | service-desk-agents, admins |
      | admin     | admins, users               |
    When migration runs
    Then groups have been migrated as follows:
      | name                | permissions                            | applications             | default applications    |
      | users               | USE                                    | jira-software, jira-core | jira-software,jira-core |
      | admins              | ADMINISTER                             | jira-software, jira-core |                         |
      | service-desk-agents | com.atlassian.servicedesk.agent.access | jira-servicedesk         |                         |
    And Service Desk property 'com.atlassian.servicedesk.renaissance.migration.type' is set to value 'ABP'
    And Renaissance migration flagged done
    And there is license <service desk license> in multistore for jira-servicedesk application
    And there is license <jira license> in multistore for jira-software application
    Examples:
      | service desk license         | jira license       |
      | SERVICE_DESK_ABP_UNLIMITED   | JIRA_6X_COMMERCIAL |
      | SERVICE_DESK_RENAISSANCE_RBP | JIRA_6X_COMMERCIAL |

  Scenario Outline: Service-desk-agents group is not set to default if it has admin permission AND SD license removed
    Given Service Desk license <service desk license> in migration store
    And JIRA license <jira license> in old store
    And following groups:
      | name                | permissions                            |
      | users               | USE                                    |
      | admins              | ADMINISTER                             |
      | service-desk-agents | com.atlassian.servicedesk.agent.access |
      | <anyone>            | com.atlassian.servicedesk.agent.access |
    And following users:
      | user      | groups                      |
      | agent-sd1 | service-desk-agents, users  |
      | agent-sd2 | service-desk-agents, admins |
      | admin     | admins, users               |
    When migration runs
    Then groups have been migrated as follows:
      | name                | permissions                            | applications             | default applications    |
      | users               | USE                                    | jira-software, jira-core | jira-software,jira-core |
      | admins              | ADMINISTER                             | jira-software, jira-core |                         |
      | service-desk-agents | com.atlassian.servicedesk.agent.access | jira-servicedesk         |                         |
    And Service Desk property 'com.atlassian.servicedesk.renaissance.migration.type' is set to value 'ABP'
    And Renaissance migration flagged done
    And license <service desk license> is not in multistore
    And there is license <jira license> in multistore for jira-software application
    Examples:
      | service desk license         | jira license       |
      | SERVICE_DESK_ABP_UNLIMITED   | JIRA_6X_COMMERCIAL |
      | SERVICE_DESK_RENAISSANCE_RBP | JIRA_6X_COMMERCIAL |

  Scenario Outline: Service-desk-agents group is not set to default if it has admin permission combined licenses
    Given Service Desk license NONE in plugin store
    And JIRA license <jira license> in old store
    And following groups:
      | name                | permissions                            |
      | users               | USE                                    |
      | admins              | ADMINISTER                             |
      | service-desk-agents | com.atlassian.servicedesk.agent.access |
      | <anyone>            | com.atlassian.servicedesk.agent.access |
    And following users:
      | user      | groups                      |
      | agent-sd1 | service-desk-agents, users  |
      | agent-sd2 | service-desk-agents, admins |
      | admin     | admins, users               |
    When migration runs
    Then groups have been migrated as follows:
      | name                | permissions                            | applications             | default applications    |
      | users               | USE                                    | jira-software, jira-core | jira-software,jira-core |
      | admins              | ADMINISTER                             | jira-software, jira-core |                         |
      | service-desk-agents | com.atlassian.servicedesk.agent.access | jira-servicedesk         |                         |
    And Service Desk property 'com.atlassian.servicedesk.renaissance.migration.type' is set to value 'ABP'
    And Renaissance migration flagged done
    And there is license <jira license> in multistore for jira-servicedesk application
    Examples:
      | jira license                            |
      | JIRA_6X_ELA_SERVICEDESK_ABP             |

  Scenario Outline: Groups with agent access are migrated if users that do not have USE/ADMINISTER/SYSADMIN permission are disabled
    Given Service Desk license <service desk license> in plugin store
    And JIRA license <jira license> in old store
    And following groups:
      | name                | permissions                            |
      | users               | USE                                    |
      | service-desk-agents | com.atlassian.servicedesk.agent.access |
      | <anyone>            | com.atlassian.servicedesk.agent.access |
    And group 'service-desk-agents' with attribute 'synch.created.by.jira.service.desk' set to 'synch.created.by.jira.service.desk'
    And following users:
      | user      | groups                    | disabled |
      | agent-sd1 | service-desk-agents       | true     |
      | agent-sd2 | service-desk-agents,users | false    |
    When migration runs
    Then groups have been migrated as follows:
      | name                | permissions                            | applications             | default applications     |
      | users               | USE                                    | jira-software, jira-core | jira-software, jira-core |
      | service-desk-agents | com.atlassian.servicedesk.agent.access | jira-servicedesk         | jira-servicedesk         |
    And Service Desk property 'com.atlassian.servicedesk.renaissance.migration.type' is set to value 'ABP'
    And Renaissance migration flagged done
    And there is license <service desk license> in multistore for jira-servicedesk application
    And there is license <jira license> in multistore for jira-software application
    Examples:
      | service desk license         | jira license       |
      | SERVICE_DESK_ABP_UNLIMITED   | JIRA_6X_COMMERCIAL |
      | SERVICE_DESK_RENAISSANCE_RBP | JIRA_6X_COMMERCIAL |

  Scenario Outline: Groups with agent access are migrated if users that do not have USE/ADMINISTER/SYSADMIN permission are disabled AND SD license removed
    Given Service Desk license <service desk license> in migration store
    And JIRA license <jira license> in old store
    And following groups:
      | name                | permissions                            |
      | users               | USE                                    |
      | service-desk-agents | com.atlassian.servicedesk.agent.access |
      | <anyone>            | com.atlassian.servicedesk.agent.access |
    And group 'service-desk-agents' with attribute 'synch.created.by.jira.service.desk' set to 'synch.created.by.jira.service.desk'
    And following users:
      | user      | groups                    | disabled |
      | agent-sd1 | service-desk-agents       | true     |
      | agent-sd2 | service-desk-agents,users | false    |
    When migration runs
    Then groups have been migrated as follows:
      | name                | permissions                            | applications             | default applications     |
      | users               | USE                                    | jira-software, jira-core | jira-software, jira-core |
      | service-desk-agents | com.atlassian.servicedesk.agent.access | jira-servicedesk         | jira-servicedesk         |
    And Service Desk property 'com.atlassian.servicedesk.renaissance.migration.type' is set to value 'ABP'
    And Renaissance migration flagged done
    And license <service desk license> is not in multistore
    And there is license <jira license> in multistore for jira-software application
    Examples:
      | service desk license         | jira license       |
      | SERVICE_DESK_ABP_UNLIMITED   | JIRA_6X_COMMERCIAL |
      | SERVICE_DESK_RENAISSANCE_RBP | JIRA_6X_COMMERCIAL |

  Scenario Outline: Groups with agent access are migrated if users that do not have USE/ADMINISTER/SYSADMIN permission are disabled for combined licenses
    Given Service Desk license NONE in plugin store
    And JIRA license <jira license> in old store
    And following groups:
      | name                | permissions                            |
      | users               | USE                                    |
      | service-desk-agents | com.atlassian.servicedesk.agent.access |
      | <anyone>            | com.atlassian.servicedesk.agent.access |
    And group 'service-desk-agents' with attribute 'synch.created.by.jira.service.desk' set to 'synch.created.by.jira.service.desk'
    And following users:
      | user      | groups                    | disabled |
      | agent-sd1 | service-desk-agents       | true     |
      | agent-sd2 | service-desk-agents,users | false    |
    When migration runs
    Then groups have been migrated as follows:
      | name                | permissions                            | applications             | default applications     |
      | users               | USE                                    | jira-software, jira-core | jira-software, jira-core |
      | service-desk-agents | com.atlassian.servicedesk.agent.access | jira-servicedesk         | jira-servicedesk         |
    And Service Desk property 'com.atlassian.servicedesk.renaissance.migration.type' is set to value 'ABP'
    And Renaissance migration flagged done
    And there is license <jira license> in multistore for jira-servicedesk application
    Examples:
      | jira license                            |
      | JIRA_6X_ELA_SERVICEDESK_ABP             |

  Scenario Outline: Groups with agent access are NOT migrated if there is no Service Desk license
    Given Service Desk license <service desk license> in <store type> store
    And JIRA license <jira license> in old store
    And following groups:
      | name                | permissions                            |
      | users               | USE                                    |
      | service-desk-agents | com.atlassian.servicedesk.agent.access |
      | <anyone>            | com.atlassian.servicedesk.agent.access |
    And group 'service-desk-agents' with attribute 'synch.created.by.jira.service.desk' set to 'synch.created.by.jira.service.desk'
    And following users:
      | user      | groups                    | disabled |
      | agent-sd1 | service-desk-agents       | true     |
      | agent-sd2 | service-desk-agents,users | false    |
    When migration runs
    Then groups have been migrated as follows:
      | name                | permissions                            | applications             | default applications     |
      | users               | USE                                    | jira-software, jira-core | jira-software, jira-core |
      | service-desk-agents | com.atlassian.servicedesk.agent.access |                          |                          |
    And Service Desk property 'com.atlassian.servicedesk.renaissance.migration.type' is set to value 'none'
    And Renaissance migration flagged done
    Examples:
      | service desk license       | jira license       | store type | store type |
      | NONE                       | JIRA_6X_COMMERCIAL | plugin     | plugin     |

  @failedMigration
  Scenario Outline: Migration with invalid or malformed Service Desk license in the plugin store
    Given Service Desk license <service desk license> in plugin store
    And JIRA license JIRA_6X_COMMERCIAL in old store
    When migration runs
    Then migration failed
    Examples:
      | service desk license |
      | MALFORMED_LICENSE    |
      | JIRA_6X_COMMERCIAL   |

  @failedMigration
  Scenario: Bad license provided by user stops the migration
    Given JIRA license JIRA_6X_COMMERCIAL in old store
    And Service Desk license SERVICE_DESK_TBP_UNLIMITED in migration store
    And MALFORMED_LICENSE license provided by user
    And following groups:
      | name  | permissions |
      | users | USE         |
    When migration runs
    Then migration failed