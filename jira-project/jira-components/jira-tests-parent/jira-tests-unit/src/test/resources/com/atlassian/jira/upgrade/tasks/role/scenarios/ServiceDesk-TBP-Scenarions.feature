Feature: Renaissance migration for Service Desk Tier based Licenses

  Scenario Outline: Service Desk Tier Based Pricing Migration when no license in multistore
    Given Service Desk license <plugin store license> in plugin store
    And JIRA license <Jira license> in old store
    And license NONE in multistore
    And following groups:
      | name       | permissions                   |
      | users      | USE                           |
      | admins     | ADMINISTER                    |
      | useAdmins  | USE, SYSTEM_ADMIN             |
      | sysAdmins  | SYSTEM_ADMIN                  |
      | everything | SYSTEM_ADMIN, ADMINISTER, USE |
      | <anyone>   | USE, SYSTEM_ADMIN             |
    When migration runs
    Then groups have been migrated as follows:
      | name       | permissions                   | applications                               | default applications                       |
      | users      | USE                           | jira-servicedesk, jira-software, jira-core | jira-servicedesk, jira-software, jira-core |
      | admins     | ADMINISTER                    | jira-servicedesk, jira-software, jira-core |                                            |
      | useAdmins  | SYSTEM_ADMIN, USE             | jira-servicedesk, jira-software, jira-core |                                            |
      | sysAdmins  | SYSTEM_ADMIN                  | jira-servicedesk, jira-software, jira-core |                                            |
      | everything | SYSTEM_ADMIN, ADMINISTER, USE | jira-servicedesk, jira-software, jira-core |                                            |
    And Service Desk property 'com.atlassian.servicedesk.renaissance.migration.type' is set to value 'TBP'
    And Renaissance migration flagged done
    And there is license <ServiceDesk License in multistore> in multistore for jira-servicedesk application
    Examples:
      | Jira license                                   | plugin store license       | ServiceDesk License in multistore              |
      | JIRA_6X_COMMERCIAL                             | SERVICE_DESK_TBP_UNLIMITED | SERVICE_DESK_TBP_UNLIMITED                     |
      | JIRA_6X_ELA_SERVICEDESK_TBP                    | NONE                       | JIRA_6X_ELA_SERVICEDESK_TBP                    |
      | JIRA_6X_ELA_SERVICEDESK_TBP                    | SERVICE_DESK_TBP_UNLIMITED | JIRA_6X_ELA_SERVICEDESK_TBP                    |

  Scenario Outline: Service Desk Tier Based Pricing Migration when license in multistore already
    Given Service Desk license <plugin store license> in plugin store
    And JIRA license <Jira license> in old store
    And license <Multistore before migration> in multistore
    And following groups:
      | name       | permissions                   |
      | users      | USE                           |
      | admins     | ADMINISTER                    |
      | useAdmins  | USE, SYSTEM_ADMIN             |
      | sysAdmins  | SYSTEM_ADMIN                  |
      | everything | SYSTEM_ADMIN, ADMINISTER, USE |
      | <anyone>   | USE, SYSTEM_ADMIN             |
    When migration runs
    Then groups have been migrated as follows:
      | name       | permissions                   | applications                               | default applications                       |
      | users      | USE                           | jira-servicedesk, jira-software, jira-core | jira-servicedesk, jira-software, jira-core |
      | admins     | ADMINISTER                    | jira-servicedesk, jira-software, jira-core |                                            |
      | useAdmins  | SYSTEM_ADMIN, USE             | jira-servicedesk, jira-software, jira-core |                                            |
      | sysAdmins  | SYSTEM_ADMIN                  | jira-servicedesk, jira-software, jira-core |                                            |
      | everything | SYSTEM_ADMIN, ADMINISTER, USE | jira-servicedesk, jira-software, jira-core |                                            |
    And Service Desk property 'com.atlassian.servicedesk.renaissance.migration.type' is set to value 'TBP'
    And Renaissance migration flagged done
    And license <plugin store license> is not in multistore
    Examples:
      | Multistore before migration  | Jira license       | plugin store license         |
      | SOFTWARE_RENAISSANCE         | JIRA_6X_COMMERCIAL | SERVICE_DESK_TBP_UNLIMITED   |
      | SERVICE_DESK_RENAISSANCE_RBP | JIRA_6X_COMMERCIAL | SERVICE_DESK_TBP_UNLIMITED   |


  Scenario Outline: Service Desk Tier Based Pricing Migration Runs When License Removed at Startup
    Given Service Desk license <plugin store license> in migration store
    And JIRA license <Jira license> in old store
    And license <Multistore before migration> in multistore
    And following groups:
      | name       | permissions                   |
      | users      | USE                           |
      | admins     | ADMINISTER                    |
      | useAdmins  | USE, SYSTEM_ADMIN             |
      | sysAdmins  | SYSTEM_ADMIN                  |
      | everything | SYSTEM_ADMIN, ADMINISTER, USE |
      | <anyone>   | USE, SYSTEM_ADMIN             |
    When migration runs
    Then groups have been migrated as follows:
      | name       | permissions                   | applications                               | default applications                       |
      | users      | USE                           | jira-servicedesk, jira-software, jira-core | jira-servicedesk, jira-software, jira-core |
      | admins     | ADMINISTER                    | jira-servicedesk, jira-software, jira-core |                                            |
      | useAdmins  | SYSTEM_ADMIN, USE             | jira-servicedesk, jira-software, jira-core |                                            |
      | sysAdmins  | SYSTEM_ADMIN                  | jira-servicedesk, jira-software, jira-core |                                            |
      | everything | SYSTEM_ADMIN, ADMINISTER, USE | jira-servicedesk, jira-software, jira-core |                                            |
    And Service Desk property 'com.atlassian.servicedesk.renaissance.migration.type' is set to value 'TBP'
    And Renaissance migration flagged done
    And there is no license for jira-servicedesk in multistore
    Examples:
      | Multistore before migration | Jira license       | plugin store license       |
      | NONE                        | JIRA_6X_COMMERCIAL | SERVICE_DESK_TBP_UNLIMITED |
      | SOFTWARE_RENAISSANCE        | JIRA_6X_COMMERCIAL | SERVICE_DESK_TBP_UNLIMITED |

  @failedMigration
  Scenario Outline: Migration with invalid or malformed Service Desk license in the plugin store
    Given Service Desk license <service desk license> in plugin store
    And JIRA license JIRA_6X_COMMERCIAL in old store
    When migration runs
    Then migration failed
    Examples:
      | service desk license |
      | MALFORMED_LICENSE    |
      | JIRA_6X_COMMERCIAL   |

  @failedMigration
  Scenario: Bad license provided by user stops the migration
    Given JIRA license JIRA_6X_COMMERCIAL in old store
    And Service Desk license SERVICE_DESK_TBP_UNLIMITED in migration store
    And MALFORMED_LICENSE license provided by user
    And following groups:
      | name  | permissions |
      | users | USE         |
    When migration runs
    Then migration failed
