Feature: Software and Core migration from Jira 6x

  Scenario Outline: Groups with USE and ADMIN permission are given Software and Core application roles regardless
  of license type
    Given JIRA license <license> in old store
    And following groups:
      | name        | permissions       |
      | users       | USE               |
      | admins      | ADMINISTER        |
      | useAdmins   | USE, ADMINISTER   |
      | sysAdmins   | SYSTEM_ADMIN      |
      | <anyone>    | USE, SYSTEM_ADMIN |
    When migration runs
    Then there is license <license> in multistore for <migrated application> application
    And groups have been migrated as follows:
      | name      | permissions  | applications             | default applications     |
      | users     | USE          | jira-software, jira-core | jira-software, jira-core |
      | admins    | ADMINISTER   | jira-software, jira-core |                          |
      | sysAdmins | SYSTEM_ADMIN | jira-software, jira-core |                          |
    And Service Desk property 'com.atlassian.servicedesk.renaissance.migration.type' is set to value 'none'
    And Renaissance migration flagged done
    Examples:
      | license              | migrated application |
      | JIRA_6X_COMMERCIAL   | jira-software        |
      | CORE_RENAISSANCE     | jira-core            |
      | SOFTWARE_RENAISSANCE | jira-software        |

  Scenario: Groups with USE and ADMIN permission are given Software and Core application if there is no license
    Given no JIRA license in old store
    And following groups:
      | name        | permissions       |
      | users       | USE               |
      | admins      | ADMINISTER        |
      | useAdmins   | USE, ADMINISTER   |
      | sysAdmins   | SYSTEM_ADMIN      |
      | <anyone>    | USE, ADMINISTER   |
    When migration runs
    Then there is no license in multistore
    And groups have been migrated as follows:
      | name      | permissions  | applications             | default applications     |
      | users     | USE          | jira-software, jira-core | jira-software, jira-core |
      | admins    | ADMINISTER   | jira-software, jira-core |                          |
      | sysAdmins | SYSTEM_ADMIN | jira-software, jira-core |                          |
    And Service Desk property 'com.atlassian.servicedesk.renaissance.migration.type' is set to value 'none'
    And Renaissance migration flagged done

  Scenario Outline: Bad license does not affect migration when good license provided by user for import
    Given JIRA license MALFORMED_LICENSE in old store
    And <license> license provided by user
    And following groups:
      | name        | permissions       |
      | users       | USE               |
      | admins      | ADMINISTER        |
      | useAdmins   | USE, ADMINISTER   |
      | sysAdmins   | SYSTEM_ADMIN      |
      | <anyone>    | USE, SYSTEM_ADMIN |
    When migration runs
    Then there is license <license> in multistore for <migrated application> application
    And license MALFORMED_LICENSE is not in multistore
    And groups have been migrated as follows:
      | name      | permissions  | applications             | default applications     |
      | users     | USE          | jira-software, jira-core | jira-software, jira-core |
      | admins    | ADMINISTER   | jira-software, jira-core |                          |
      | sysAdmins | SYSTEM_ADMIN | jira-software, jira-core |                          |
    And Service Desk property 'com.atlassian.servicedesk.renaissance.migration.type' is set to value 'none'
    And Renaissance migration flagged done
    Examples:
      | license              | migrated application |
      | JIRA_6X_COMMERCIAL   | jira-software        |
      | CORE_RENAISSANCE     | jira-core            |
      | SOFTWARE_RENAISSANCE | jira-software        |

  Scenario Outline: JIRA 6x license ignored when any license already in multistore
    Given JIRA license JIRA_6X_COMMERCIAL in old store
    And <license> license provided by user
    And following groups:
      | name        | permissions       |
      | users       | USE               |
      | admins      | ADMINISTER        |
      | useAdmins   | USE, ADMINISTER   |
      | sysAdmins   | SYSTEM_ADMIN      |
      | <anyone>    | USE, SYSTEM_ADMIN |
    When migration runs
    Then there is license <license> in multistore for <migrated application> application
    And license JIRA_6X_COMMERCIAL is not in multistore
    And groups have been migrated as follows:
      | name      | permissions  | applications             | default applications     |
      | users     | USE          | jira-software, jira-core | jira-software, jira-core |
      | admins    | ADMINISTER   | jira-software, jira-core |                          |
      | sysAdmins | SYSTEM_ADMIN | jira-software, jira-core |                          |
    And Service Desk property 'com.atlassian.servicedesk.renaissance.migration.type' is set to value 'none'
    And Renaissance migration flagged done
    Examples:
      | license              | migrated application |
      | CORE_RENAISSANCE     | jira-core            |
      | SOFTWARE_RENAISSANCE | jira-software        |

  @failedMigration
  Scenario: Bad license in old store stops the migration
    Given JIRA license MALFORMED_LICENSE in old store
    And following groups:
      | name        | permissions       |
      | users       | USE               |
    When migration runs
    Then migration failed

  @failedMigration
  Scenario: Bad license provided by user stops the migration
    Given JIRA license JIRA_6X_COMMERCIAL in old store
    And MALFORMED_LICENSE license provided by user
    And following groups:
      | name        | permissions       |
      | users       | USE               |
    When migration runs
    Then migration failed