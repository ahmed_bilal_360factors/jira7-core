package com.atlassian.jira.upgrade.tasks.role;

import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.crowd.embedded.impl.ImmutableGroup;
import com.google.common.collect.ImmutableList;

import java.util.Arrays;
import java.util.Collections;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @since v7.0
 */
final class TestUtils {
    private TestUtils() {
    }

    static Licenses toLicenses() {
        return new Licenses(Collections.emptyList());
    }

    static Licenses toLicenses(License... license) {
        return new Licenses(Arrays.asList(license));
    }

    static Licenses toLicenses(com.atlassian.jira.test.util.lic.License... license) {
        return new Licenses(Arrays.stream(license)
                .map(TestUtils::toLicense)
                .collect(Collectors.toList()));
    }

    static License toLicense(com.atlassian.jira.test.util.lic.License license) {
        return new License(license.getLicenseString());
    }

    static MigrationState emptyState() {
        return new MigrationState(toLicenses(), emptyRoles(), ImmutableList.<Runnable>of(),
                new MigrationLogImpl());
    }

    static MigrationState licensesState(com.atlassian.jira.test.util.lic.License... licenses) {
        return new MigrationState(toLicenses(licenses), emptyRoles(), ImmutableList.of(), new MigrationLogImpl());
    }

    static ApplicationRoles emptyRoles() {
        return new ApplicationRoles(ImmutableList.of());
    }

    static Licenses emptyLicenses() {
        return toLicenses();
    }

    static Set<Group> asGroups(String... groupNames) {
        return Arrays.stream(groupNames).map(TestUtils::newGroup).collect(Collectors.toSet());
    }

    static Group newGroup(String groupName) {
        return new ImmutableGroup(groupName);
    }
}
