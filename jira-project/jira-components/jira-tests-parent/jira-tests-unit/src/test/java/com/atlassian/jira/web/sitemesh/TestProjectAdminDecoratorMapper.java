package com.atlassian.jira.web.sitemesh;

import com.atlassian.jira.admin.ProjectAdminSidebarFeature;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.opensymphony.module.sitemesh.DecoratorMapper;
import com.opensymphony.module.sitemesh.Page;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.http.HttpServletRequest;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public final class TestProjectAdminDecoratorMapper {
    private static final String ADMIN_DECORATOR = "admin";
    private static final String PROJECT_CONFIG_SECTION = "atl.jira.proj.config";
    private static final String LOAD_SIDEBAR_PARAM = "shouldLoadSidebar";

    @Rule
    public RuleChain mocksInContainer = MockitoMocksInContainer.forTest(this);
    @Mock
    @AvailableInContainer
    private ProjectAdminSidebarFeature projectAdminSidebarFeature;
    @Mock
    private DecoratorMapper parentDecoratorMapper;
    @Mock
    private HttpServletRequest request;
    @Mock
    private Page page;

    private ProjectAdminDecoratorMapper projectAdminDecoratorMapper;

    @Before
    public void setUp() throws InstantiationException {
        projectAdminDecoratorMapper = new ProjectAdminDecoratorMapper();
        projectAdminDecoratorMapper.init(null, null, parentDecoratorMapper);

        shouldDisplaySidebar(true);
        setDecorator(ADMIN_DECORATOR);
        setActiveAdminSection(PROJECT_CONFIG_SECTION);
    }

    @Test
    public void shouldSetSidebarFlagWhenAllConditionsAreValid() {
        projectAdminDecoratorMapper.getDecorator(request, page);

        verify(request, times(1)).setAttribute(LOAD_SIDEBAR_PARAM, "true");
    }

    @Test
    public void shouldNotSetSidebarFlagWhenDarkFeatureIsOffButOtherConditionsAreTrue() {
        shouldDisplaySidebar(false);

        projectAdminDecoratorMapper.getDecorator(request, page);

        verify(request, never()).setAttribute(LOAD_SIDEBAR_PARAM, "true");
    }

    @Test
    public void shouldNotSetSidebarFlagWhenDecoratorIsNotAdminButOtherConditionsAreTrue() {
        setDecorator("something-admin-else");

        projectAdminDecoratorMapper.getDecorator(request, page);

        verify(request, never()).setAttribute(LOAD_SIDEBAR_PARAM, "true");
    }

    @Test
    public void shouldNotSetSidebarFlagWhenAdminActiveSectionIsNotProjectViewButOtherConditionsAreTrue() {
        setActiveAdminSection("invalid.jira.proj.config");

        projectAdminDecoratorMapper.getDecorator(request, page);

        verify(request, never()).setAttribute(LOAD_SIDEBAR_PARAM, "true");
    }

    @Test
    public void shouldNotSetSidebarFlagPageObjectIsNullButOtherConditionsAreTrue() {
        projectAdminDecoratorMapper.getDecorator(request, null);

        verify(request, never()).setAttribute(LOAD_SIDEBAR_PARAM, "true");
    }

    private void shouldDisplaySidebar(final boolean status) {
        when(projectAdminSidebarFeature.shouldDisplay()).thenReturn(status);
    }

    private void setDecorator(String decorator) {
        when(page.getProperty("meta.decorator")).thenReturn(decorator);
    }

    private void setActiveAdminSection(String section) {
        when(page.getProperty("meta.admin.active.section")).thenReturn(section);
    }
}
