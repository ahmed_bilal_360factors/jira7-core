package com.atlassian.jira.jql.query;

import com.atlassian.jira.issue.security.IssueSecurityLevel;
import com.atlassian.jira.issue.security.IssueSecurityLevelImpl;
import com.atlassian.jira.jql.operand.JqlOperandResolver;
import com.atlassian.jira.jql.operand.QueryLiteral;
import com.atlassian.jira.jql.resolver.IssueSecurityLevelResolver;
import com.atlassian.jira.jql.validator.MockJqlOperandResolver;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.query.clause.TerminalClauseImpl;
import com.atlassian.query.operand.EmptyOperand;
import com.atlassian.query.operand.MultiValueOperand;
import com.atlassian.query.operator.Operator;
import com.atlassian.query.operator.OperatorDoesNotSupportOperand;
import com.google.common.collect.ImmutableList;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Collections;

import static com.atlassian.jira.jql.operand.SimpleLiteralFactory.createLiteral;
import static com.google.common.collect.Lists.newArrayList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestIssueSecurityLevelClauseQueryFactory {
    private ApplicationUser theUser;
    @Rule
    public MockitoRule mockito = MockitoJUnit.rule();

    @Mock
    private IssueSecurityLevelResolver issueSecurityLevelResolver;


    private QueryCreationContext queryCreationContext;
    private IssueSecurityLevelClauseQueryFactory factory;


    @Before
    public void setUp() throws Exception {
        theUser = new MockApplicationUser("fred");
        final JqlOperandResolver jqlOperandResolver = MockJqlOperandResolver.createSimpleSupport();
        queryCreationContext = new QueryCreationContextImpl(theUser);

        factory = new IssueSecurityLevelClauseQueryFactory(issueSecurityLevelResolver, jqlOperandResolver);
    }

    @Test
    public void testConstructors() throws Exception {
        final JqlOperandResolver jqlOperandResolver = mock(JqlOperandResolver.class);

        try {
            new IssueSecurityLevelClauseQueryFactory(null, null);
            fail("Expected exception");
        } catch (IllegalArgumentException expected) {
        }

        try {
            new IssueSecurityLevelClauseQueryFactory(issueSecurityLevelResolver, null);
            fail("Expected exception");
        } catch (IllegalArgumentException expected) {
        }

        new IssueSecurityLevelClauseQueryFactory(issueSecurityLevelResolver, jqlOperandResolver);
    }

    @Test
    public void testIsEmpty() throws Exception {
        when(issueSecurityLevelResolver.getIssueSecurityLevels(theUser, Collections.singletonList(new QueryLiteral()))).thenReturn(Collections.singletonList(null));
        when(issueSecurityLevelResolver.getIssueSecurityLevels(theUser, Collections.singletonList(new QueryLiteral()))).thenReturn(Collections.singletonList(null));


        QueryFactoryResult result = factory.getQuery(queryCreationContext, new TerminalClauseImpl("level", Operator.IS, EmptyOperand.EMPTY));
        assertEquals("issue_security_level:-1", result.getLuceneQuery().toString());
        assertFalse(result.mustNotOccur());

        result = factory.getQuery(queryCreationContext, new TerminalClauseImpl("level", Operator.EQUALS, EmptyOperand.EMPTY));
        assertEquals("issue_security_level:-1", result.getLuceneQuery().toString());
        assertFalse(result.mustNotOccur());
    }

    @Test
    public void testIsNotEmpty() throws Exception {
        when(issueSecurityLevelResolver.getIssueSecurityLevels(theUser, ImmutableList.of(new QueryLiteral()))).thenReturn(Collections.singletonList(null));
        when(issueSecurityLevelResolver.getIssueSecurityLevels(theUser, ImmutableList.of(new QueryLiteral()))).thenReturn(Collections.singletonList(null));

        QueryFactoryResult result = factory.getQuery(queryCreationContext, new TerminalClauseImpl("level", Operator.IS_NOT, EmptyOperand.EMPTY));
        assertEquals("issue_security_level:-1", result.getLuceneQuery().toString());
        assertTrue(result.mustNotOccur());

        result = factory.getQuery(queryCreationContext, new TerminalClauseImpl("level", Operator.NOT_EQUALS, EmptyOperand.EMPTY));
        assertEquals("issue_security_level:-1", result.getLuceneQuery().toString());
        assertTrue(result.mustNotOccur());
    }

    @Test
    public void testEquals() throws Exception {
        when(issueSecurityLevelResolver.getIssueSecurityLevels(theUser, ImmutableList.of(createLiteral("TheName")))).thenReturn(ImmutableList.of(createMockSecurityLevel(5L, "TheName")));

        QueryFactoryResult result = factory.getQuery(queryCreationContext, new TerminalClauseImpl("level", Operator.EQUALS, "TheName"));
        assertEquals("issue_security_level:5", result.getLuceneQuery().toString());
        assertFalse(result.mustNotOccur());
    }

    @Test
    public void testEqualsOverrideSecurity() throws Exception {
        queryCreationContext = new QueryCreationContextImpl(theUser, true);

        when(issueSecurityLevelResolver.getIssueSecurityLevelsOverrideSecurity(Collections.singletonList(createLiteral("TheName"))))
                .thenReturn(ImmutableList.<IssueSecurityLevel>of(createMockSecurityLevel(5L, "TheName")));

        QueryFactoryResult result = factory.getQuery(queryCreationContext, new TerminalClauseImpl("level", Operator.EQUALS, "TheName"));
        assertEquals("issue_security_level:5", result.getLuceneQuery().toString());
        assertFalse(result.mustNotOccur());
    }

    @Test
    public void testNotEquals() throws Exception {
        when(issueSecurityLevelResolver.getIssueSecurityLevels(theUser, ImmutableList.of(createLiteral("TheName"))))
                .thenReturn(ImmutableList.of(createMockSecurityLevel(5L, "TheName")));

        QueryFactoryResult result = factory.getQuery(queryCreationContext, new TerminalClauseImpl("level", Operator.NOT_EQUALS, "TheName"));
        assertEquals("-issue_security_level:-1 -issue_security_level:5", result.getLuceneQuery().toString());
        assertFalse(result.mustNotOccur());
    }

    @Test
    public void testIn() throws Exception {
        when(issueSecurityLevelResolver.getIssueSecurityLevels(
                theUser,
                ImmutableList.of(createLiteral(5L), createLiteral(6L))

        )).thenReturn(
                ImmutableList.of(createMockSecurityLevel(5L, "TheName"), createMockSecurityLevel(6L, "TheName"))
        );

        QueryFactoryResult result = factory.getQuery(queryCreationContext, new TerminalClauseImpl("level", Operator.IN, new MultiValueOperand(5L, 6L)));
        assertEquals("issue_security_level:5 issue_security_level:6", result.getLuceneQuery().toString());
        assertFalse(result.mustNotOccur());
    }

    @Test
    public void testInWithOnlyOne() throws Exception {
        when(issueSecurityLevelResolver.getIssueSecurityLevels(
                theUser,
                ImmutableList.of(createLiteral(6L))

        )).thenReturn(
                ImmutableList.of(createMockSecurityLevel(6L, "TheName"))

        );

        QueryFactoryResult result = factory.getQuery(queryCreationContext, new TerminalClauseImpl("level", Operator.IN, new MultiValueOperand(6L)));
        assertEquals("issue_security_level:6", result.getLuceneQuery().toString());
        assertFalse(result.mustNotOccur());
    }

    @Test
    public void testNotIn() throws Exception {
        when(issueSecurityLevelResolver.getIssueSecurityLevels(
                theUser,
                ImmutableList.of(createLiteral(5L), createLiteral(6L))
        )).thenReturn(
                ImmutableList.of(createMockSecurityLevel(5L, "TheName"), createMockSecurityLevel(6L, "TheName"))

        );

        QueryFactoryResult result = factory.getQuery(queryCreationContext, new TerminalClauseImpl("level", Operator.NOT_IN, new MultiValueOperand(5L, 6L)));
        assertEquals("-issue_security_level:-1 -issue_security_level:5 -issue_security_level:6", result.getLuceneQuery().toString());
        assertFalse(result.mustNotOccur());
    }

    @Test
    public void testNotInWithOnlyOne() throws Exception {
        when(issueSecurityLevelResolver.getIssueSecurityLevels(
                theUser,
                ImmutableList.of(createLiteral(6L))

        )).thenReturn(
                ImmutableList.of(createMockSecurityLevel(6L, "TheName"))

        );

        QueryFactoryResult result = factory.getQuery(queryCreationContext, new TerminalClauseImpl("level", Operator.NOT_IN, new MultiValueOperand(6L)));
        assertEquals("-issue_security_level:-1 -issue_security_level:6", result.getLuceneQuery().toString());
        assertFalse(result.mustNotOccur());
    }

    @Test
    public void testNotInWithOnlyEmpty() throws Exception {
        when(issueSecurityLevelResolver.getIssueSecurityLevels(
                theUser,
                ImmutableList.of(new QueryLiteral())
        )).thenReturn(
                Collections.<IssueSecurityLevel>singletonList(null)
        );

        QueryFactoryResult result = factory.getQuery(queryCreationContext, new TerminalClauseImpl("level", Operator.NOT_IN, new MultiValueOperand(EmptyOperand.EMPTY)));
        assertEquals("issue_security_level:-1", result.getLuceneQuery().toString());
        assertTrue(result.mustNotOccur());
    }

    @Test
    public void testInNotAllAreResolved() throws Exception {
        when(issueSecurityLevelResolver.getIssueSecurityLevels(
                theUser,
                ImmutableList.of(
                        createLiteral(5L),
                        createLiteral(6L),
                        createLiteral(7L),
                        new QueryLiteral())
        )).thenReturn(
                newArrayList(
                        createMockSecurityLevel(5L, "TheName"),
                        createMockSecurityLevel(6L, "TheName"),
                        null)
        );

        QueryFactoryResult result = factory.getQuery(queryCreationContext, new TerminalClauseImpl("level", Operator.IN, new MultiValueOperand(createLiteral(5L), createLiteral(6L), createLiteral(7L), new QueryLiteral())));
        assertEquals("issue_security_level:5 issue_security_level:6 issue_security_level:-1", result.getLuceneQuery().toString());
        assertFalse(result.mustNotOccur());
    }

    @Test
    public void testNotInNotAllAreResolved() throws Exception {
        when(issueSecurityLevelResolver.getIssueSecurityLevels(
                theUser,
                ImmutableList.of(
                        createLiteral(5L),
                        createLiteral(6L),
                        createLiteral(7L))
        )).thenReturn(
                ImmutableList.of(
                        createMockSecurityLevel(5L, "TheName"),
                        createMockSecurityLevel(6L, "TheName"))
        );

        QueryFactoryResult result = factory.getQuery(queryCreationContext, new TerminalClauseImpl("level", Operator.NOT_IN, new MultiValueOperand(5L, 6L, 7L)));
        assertEquals("-issue_security_level:-1 -issue_security_level:5 -issue_security_level:6", result.getLuceneQuery().toString());
        assertFalse(result.mustNotOccur());
    }

    @Test
    public void testNullLiteralsReturned() throws Exception {
        final MultiValueOperand operand = new MultiValueOperand(5L, 6L, 7L);
        final TerminalClauseImpl clause = new TerminalClauseImpl("level", Operator.NOT_IN, operand);

        final JqlOperandResolver jqlOperandResolver = mock(JqlOperandResolver.class);
        when(jqlOperandResolver.getValues(queryCreationContext, operand, clause)).thenReturn(null);

        IssueSecurityLevelClauseQueryFactory factory = new IssueSecurityLevelClauseQueryFactory(issueSecurityLevelResolver, jqlOperandResolver);

        QueryFactoryResult result = factory.getQuery(queryCreationContext, clause);
        assertEquals("", result.getLuceneQuery().toString());
        assertFalse(result.mustNotOccur());
    }

    @Test
    public void testCreateQueryForValuesWithBadOperator() throws Exception {
        for (Operator operator : Operator.values()) {
            if (Operator.IN == operator || Operator.NOT_IN == operator ||
                    Operator.EQUALS == operator || Operator.NOT_EQUALS == operator) {
                continue;
            }

            try {
                factory.createQueryForValues(operator, Collections.<String>emptyList());
            } catch (OperatorDoesNotSupportOperand expected) {
            }
        }
    }

    private IssueSecurityLevel createMockSecurityLevel(final Long id, final String name) {
        return new IssueSecurityLevelImpl(id, name, null, null);
    }
}