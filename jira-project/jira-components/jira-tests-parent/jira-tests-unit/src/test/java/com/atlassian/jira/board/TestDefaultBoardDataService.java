package com.atlassian.jira.board;

import com.atlassian.jira.bc.ServiceOutcome;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.board.model.BoardColumn;
import com.atlassian.jira.board.model.BoardData;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchResults;
import com.atlassian.jira.issue.status.Status;
import com.atlassian.jira.issue.status.category.StatusCategory;
import com.atlassian.jira.issue.status.category.StatusCategoryImpl;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.util.ErrorCollection.Reason;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.NoopI18nHelper;
import com.atlassian.jira.web.action.issue.IssueSearchLimits;
import com.atlassian.jira.web.bean.PagerFilter;
import com.atlassian.jira.workflow.JiraWorkflow;
import com.atlassian.query.Query;
import com.opensymphony.workflow.loader.ActionDescriptor;
import com.opensymphony.workflow.loader.ResultDescriptor;
import com.opensymphony.workflow.loader.StepDescriptor;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.IntStream;

import static com.atlassian.collectors.CollectorsUtil.toImmutableList;
import static com.atlassian.collectors.CollectorsUtil.toImmutableSet;
import static com.atlassian.jira.bc.ServiceOutcomeImpl.error;
import static com.atlassian.jira.bc.ServiceOutcomeImpl.ok;
import static com.atlassian.jira.util.ErrorCollection.Reason.PRECONDITION_FAILED;
import static com.atlassian.jira.util.ErrorCollection.Reason.SERVER_ERROR;
import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Sets.newHashSet;
import static java.util.Collections.emptyList;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestDefaultBoardDataService {
    private static final BoardId BOARD_ID = new BoardId(1L);
    private static final String BOARD_JQL = "some jql";
    private static final Board BOARD = new Board(BOARD_ID, BOARD_JQL);
    private static final ApplicationUser ASSIGNEE1 = new MockApplicationUser("assignee1");
    private static final ApplicationUser ASSIGNEE2 = new MockApplicationUser("assignee2");
    private static final ApplicationUser EXCLUDED_ASSIGNEE = new MockApplicationUser("excluded assignee");
    private static final Status STATUS1 = statusWithId("1");
    private static final Status STATUS2 = statusWithId("2");

    @Mock
    private BoardWorkflowService boardWorkflowService;

    @Mock
    private I18nHelper.BeanFactory beanFactory;

    @Mock
    private IssueSearchLimits issueSearchLimits;

    @Mock
    private SearchService searchService;

    @Mock
    private BoardQueryService boardQueryService;

    private DefaultBoardDataService defaultBoardDataService;

    private ApplicationUser loggedInUser;
    private Query baseQuery;
    private Query augmentedQuery;

    @Before
    public void setup() {
        loggedInUser = new MockApplicationUser("some user");
        i18nHelperForUserIs(loggedInUser, new NoopI18nHelper());
        baseQuery = getBaseQueryForBoard(BOARD);
        augmentedQuery = augmentQueryWithAssigneeConstraint(baseQuery);

        defaultBoardDataService = new DefaultBoardDataService(
                boardWorkflowService,
                boardQueryService,
                beanFactory,
                issueSearchLimits,
                searchService
        );
    }

    @Test
    public void
    correctBoardDataIsReturnedIfTheBaseQueryIsValidAndASearchExceptionIsNotThrownAndEachIssueHasADifferentAssignee() throws SearchException {
        Issue issue1 = issueWithAssignee(ASSIGNEE1);
        Issue issue2 = issueWithAssignee(ASSIGNEE2);
        Issue issue3 = issueWithAssignee(EXCLUDED_ASSIGNEE);
        issuesForQueryWithMaxLimit(3, baseQuery, issue1, issue2, issue3);
        issuesForQueryWithMaxLimit(3, augmentedQuery, issue1, issue2);
        accessibleStatusesForQueryAre(augmentedQuery, STATUS1, STATUS2);
        numberOfWorkflowsForQueryIs(augmentedQuery, 1);

        ServiceOutcome<BoardData> boardDataOutcome = defaultBoardDataService.getDataForBoard(loggedInUser, BOARD);

        assertThat(boardDataOutcome.isValid(), is(true));
        BoardData expectedBoardData = boardDataWith(
                newArrayList(issue1, issue2),
                newArrayList(STATUS1, STATUS2),
                newArrayList(ASSIGNEE1, ASSIGNEE2),
                newArrayList(STATUS1, STATUS2),
                2,
                2,
                true
        );
        assertThat(boardDataOutcome.get(), is(expectedBoardData));
    }

    @Test
    public void
    correctBoardDataIsReturnedIfTheBaseQueryIsValidAndASearchExceptionIsNotThrownAndThereAreSeveralIssuesAssignedToTheSameUser() throws SearchException {
        Issue issue1 = issueWithAssignee(ASSIGNEE1);
        Issue issue2 = issueWithAssignee(ASSIGNEE1);
        Issue issue3 = issueWithAssignee(EXCLUDED_ASSIGNEE);
        issuesForQueryWithMaxLimit(3, baseQuery, issue1, issue2, issue3);
        issuesForQueryWithMaxLimit(3, augmentedQuery, issue1, issue2);
        accessibleStatusesForQueryAre(augmentedQuery, STATUS1, STATUS2);
        numberOfWorkflowsForQueryIs(augmentedQuery, 1);

        ServiceOutcome<BoardData> boardDataOutcome = defaultBoardDataService.getDataForBoard(loggedInUser, BOARD);

        assertThat(boardDataOutcome.isValid(), is(true));
        BoardData expectedBoardData = boardDataWith(
                newArrayList(issue1, issue2),
                newArrayList(STATUS1, STATUS2),
                newArrayList(ASSIGNEE1),
                newArrayList(STATUS1, STATUS2),
                2,
                2,
                true
        );
        assertThat(boardDataOutcome.get(), is(expectedBoardData));
    }

    @Test
    public void correctBoardDataIsReturnedIfValidWorkflowIsAssociatedWithGivenQuery() throws SearchException {
        Issue issue1 = issueWithAssignee(ASSIGNEE1);
        Issue issue2 = issueWithAssignee(ASSIGNEE1);
        Issue issue3 = issueWithAssignee(EXCLUDED_ASSIGNEE);
        issuesForQueryWithMaxLimit(3, baseQuery, issue1, issue2, issue3);
        issuesForQueryWithMaxLimit(3, augmentedQuery, issue1, issue2);
        accessibleStatusesForQueryAre(augmentedQuery, STATUS1, STATUS2);
        JiraWorkflow workflow = validWorkflow();
        workflowsForQueryAre(augmentedQuery, workflow);

        ServiceOutcome<BoardData> boardDataOutcome = defaultBoardDataService.getDataForBoard(loggedInUser, BOARD);

        assertThat(boardDataOutcome.isValid(), is(true));
        BoardData expectedBoardData = boardDataWith(
                newArrayList(issue1, issue2),
                newArrayList(STATUS1, STATUS2),
                newArrayList(ASSIGNEE1),
                newArrayList(STATUS1, STATUS2),
                2,
                2,
                true
        );
        assertThat(boardDataOutcome.get(), is(expectedBoardData));
    }

    @Test
    public void returnedListOfIssuesIsReducedIfTheTotalNumberOfIssuesIsLessThanTheMaximumAllowedNumber() throws SearchException {
        Issue issue1 = issueWithAssignee(ASSIGNEE1);
        Issue issue2 = issueWithAssignee(ASSIGNEE1);
        Issue issue3 = issueWithAssignee(EXCLUDED_ASSIGNEE);
        issuesForQueryWithMaxLimit(1, baseQuery, issue1, issue2, issue3);
        issuesForQueryWithMaxLimit(1, augmentedQuery, issue1, issue2);
        accessibleStatusesForQueryAre(augmentedQuery, STATUS1, STATUS2);
        numberOfWorkflowsForQueryIs(augmentedQuery, 1);

        ServiceOutcome<BoardData> boardDataOutcome = defaultBoardDataService.getDataForBoard(loggedInUser, BOARD);

        assertThat(boardDataOutcome.isValid(), is(true));
        BoardData expectedBoardData = boardDataWith(
                newArrayList(issue1),
                newArrayList(STATUS1, STATUS2),
                newArrayList(ASSIGNEE1),
                newArrayList(STATUS1, STATUS2),
                1,
                2,
                true
        );
        assertThat(boardDataOutcome.get(), is(expectedBoardData));
    }

    @Test
    public void
    noBoardDataIsReturnedAndTheCorrectReasonIsProvidedIfTheBaseQueryIsValidAndASearchExceptionIsThrown() throws SearchException {
        searchingForIssuesForQueryThrowsAnException(augmentedQuery);
        numberOfWorkflowsForQueryIs(augmentedQuery, 1);

        ServiceOutcome<BoardData> boardDataOutcome = defaultBoardDataService.getDataForBoard(loggedInUser, BOARD);

        assertThat(boardDataOutcome.isValid(), is(false));
        assertThat(boardDataOutcome.getErrorCollection().getReasons(), is(newHashSet(SERVER_ERROR)));
    }

    @Test
    public void columnsWithInitialStatusesAppearBeforeColumnsWithNonInitialStatuses() throws SearchException {
        issuesForQueryWithMaxLimit(0, augmentedQuery);
        Status nonInitialStatus = STATUS1;
        Status initialStatus = STATUS2;
        accessibleStatusesForQueryAre(augmentedQuery, nonInitialStatus, initialStatus);
        initialStatusesForQueryAre(augmentedQuery, initialStatus);
        numberOfWorkflowsForQueryIs(augmentedQuery, 1);

        ServiceOutcome<BoardData> boardDataOutcome = defaultBoardDataService.getDataForBoard(loggedInUser, BOARD);

        assertThat(boardDataOutcome.isValid(), is(true));
        BoardData expectedBoardData = boardDataWith(
                emptyList(),
                newArrayList(nonInitialStatus, initialStatus),
                emptyList(),
                newArrayList(initialStatus, nonInitialStatus),
                0,
                0,
                false
        );
        assertThat(boardDataOutcome.get(), is(expectedBoardData));
    }

    @Test
    public void columnsWithNonInitialStatusesAreSortedFirstByStatusCategoryAndThenByIdNumerically() throws SearchException {
        issuesForQueryWithMaxLimit(0, augmentedQuery);
        Status nonInitialStatus1 = statusWithIdAndCategory("10000", "Done");
        Status nonInitialStatus2 = statusWithIdAndCategory("2", "In Progress");
        Status nonInitialStatus3 = statusWithIdAndCategory("3", "Done");
        Status initialStatus = statusWithId("4");
        accessibleStatusesForQueryAre(augmentedQuery, nonInitialStatus1, nonInitialStatus2, nonInitialStatus3, initialStatus);
        initialStatusesForQueryAre(augmentedQuery, initialStatus);
        numberOfWorkflowsForQueryIs(augmentedQuery, 1);

        ServiceOutcome<BoardData> boardDataOutcome = defaultBoardDataService.getDataForBoard(loggedInUser, BOARD);

        assertThat(boardDataOutcome.isValid(), is(true));
        BoardData expectedBoardData = boardDataWith(
                emptyList(),
                newArrayList(nonInitialStatus1, nonInitialStatus2, nonInitialStatus3, initialStatus),
                emptyList(),
                newArrayList(initialStatus, nonInitialStatus2, nonInitialStatus3, nonInitialStatus1),
                0,
                0,
                false
        );
        assertThat(boardDataOutcome.get(), is(expectedBoardData));
    }

    @Test
    public void shouldReturnErrorOutcomeIfMoreThanOneWorkflowIsPresentInAugmentedQuery() {
        numberOfWorkflowsForQueryIs(augmentedQuery, 2);

        ServiceOutcome<BoardData> boardDataOutcome = defaultBoardDataService.getDataForBoard(loggedInUser, BOARD);

        Reason errorReason = boardDataOutcome.getErrorCollection().getReasons().stream().findFirst().get();
        assertThat(errorReason, is(PRECONDITION_FAILED));
    }

    @Test
    public void shouldReturnErrorOutcomeIfNoWorkflowIsPresentInAugmentedQuery() {
        numberOfWorkflowsForQueryIs(augmentedQuery, 0);

        ServiceOutcome<BoardData> boardDataOutcome = defaultBoardDataService.getDataForBoard(loggedInUser, BOARD);

        Reason errorReason = boardDataOutcome.getErrorCollection().getReasons().stream().findFirst().get();
        assertThat(errorReason, is(PRECONDITION_FAILED));
    }

    @Test
    public void shouldSupportWorkflowsWithTransitionHasScreens() throws SearchException {
        Issue issue1 = issueWithAssignee(ASSIGNEE1);
        Issue issue2 = issueWithAssignee(ASSIGNEE1);
        issuesForQueryWithMaxLimit(2, augmentedQuery, issue1, issue2);
        JiraWorkflow workflow = mock(JiraWorkflow.class);
        ActionDescriptor actionDescriptor1 = mock(ActionDescriptor.class);
        ActionDescriptor actionDescriptor2 = mock(ActionDescriptor.class);
        when(workflow.getAllActions()).thenReturn(newArrayList(actionDescriptor1, actionDescriptor2));
        when(actionDescriptor2.getView()).thenReturn("stubView");
        workflowsForQueryAre(augmentedQuery, workflow);

        ServiceOutcome<BoardData> boardDataOutcome = defaultBoardDataService.getDataForBoard(loggedInUser, BOARD);

        BoardData expectedBoardData = boardDataWith(
                newArrayList(issue1, issue2),
                emptyList(),
                newArrayList(ASSIGNEE1),
                emptyList(),
                2,
                2,
                false
        );

        assertThat(boardDataOutcome.isValid(), is(true));
        assertThat(boardDataOutcome.get(), is(expectedBoardData));
    }

    @Test
    public void shouldReturnErrorOutcomeIfMoreThanOneTransitionExistsBetweenTwoStatuses() {
        workflowsForQueryAre(augmentedQuery, workflowWithMultipleTransitionsBetweenSameStatuses());

        ServiceOutcome<BoardData> boardDataOutcome = defaultBoardDataService.getDataForBoard(loggedInUser, BOARD);

        Reason reason = boardDataOutcome.getErrorCollection().getReasons().stream().findFirst().get();
        assertThat(reason, is(PRECONDITION_FAILED));
    }

    @Test
    public void noBoardDataIsReturnedIfThereWasAnErrorGettingTheBaseQueryOfTheBoard() {
        gettingBaseQueryForBoardResultsInAnError(BOARD, "error");

        ServiceOutcome<BoardData> boardDataOutcome = defaultBoardDataService.getDataForBoard(loggedInUser, BOARD);

        assertThat(boardDataOutcome.isValid(), is(false));
        assertThat(boardDataOutcome.getErrorCollection().getErrorMessages(), is(newArrayList("error")));
    }

    @Test
    public void hasFilteredIssuesIsFalseIfTheNumberOfIssuesInTheAugmentedQueryIsTheSameAsTheNumberOfIssuesInTheBaseQuery() throws SearchException {
        Issue issue1 = issueWithAssignee(ASSIGNEE1);
        Issue issue2 = issueWithAssignee(ASSIGNEE1);
        issuesForQueryWithMaxLimit(2, baseQuery, issue1, issue2);
        issuesForQueryWithMaxLimit(2, augmentedQuery, issue1, issue2);
        accessibleStatusesForQueryAre(augmentedQuery, STATUS1, STATUS2);
        numberOfWorkflowsForQueryIs(augmentedQuery, 1);

        ServiceOutcome<BoardData> boardDataOutcome = defaultBoardDataService.getDataForBoard(loggedInUser, BOARD);

        assertThat(boardDataOutcome.isValid(), is(true));
        BoardData expectedBoardData = boardDataWith(
                newArrayList(issue1, issue2),
                newArrayList(STATUS1, STATUS2),
                newArrayList(ASSIGNEE1),
                newArrayList(STATUS1, STATUS2),
                2,
                2,
                false
        );
        assertThat(boardDataOutcome.get(), is(expectedBoardData));
    }

    private Query getBaseQueryForBoard(Board board) {
        Query query = mock(Query.class);
        when(boardQueryService.getBaseQueryForBoard(loggedInUser, board)).thenReturn(ok(query));

        return query;
    }

    private void gettingBaseQueryForBoardResultsInAnError(Board board, String errorMessage) {
        when(boardQueryService.getBaseQueryForBoard(loggedInUser, board)).thenReturn(error(errorMessage));
    }

    private JiraWorkflow validWorkflow() {
        JiraWorkflow workflow = mock(JiraWorkflow.class);
        ActionDescriptor actionDescriptor1 = mock(ActionDescriptor.class);
        ActionDescriptor actionDescriptor2 = mock(ActionDescriptor.class);
        ActionDescriptor actionDescriptor3 = mock(ActionDescriptor.class);
        StepDescriptor stepDescriptor = mock(StepDescriptor.class);
        ResultDescriptor resultDescriptor1 = mock(ResultDescriptor.class);
        ResultDescriptor resultDescriptor2 = mock(ResultDescriptor.class);
        ResultDescriptor resultDescriptor3 = mock(ResultDescriptor.class);
        List<ActionDescriptor> actionDescriptors = newArrayList(actionDescriptor1, actionDescriptor2, actionDescriptor3);

        when(workflow.getAllActions()).thenReturn(actionDescriptors);
        when(workflow.getStepsForTransition(isA(ActionDescriptor.class))).thenReturn(newArrayList(stepDescriptor));
        when(stepDescriptor.getActions()).thenReturn(actionDescriptors);
        when(actionDescriptor1.getUnconditionalResult()).thenReturn(resultDescriptor1);
        when(actionDescriptor2.getUnconditionalResult()).thenReturn(resultDescriptor2);
        when(actionDescriptor3.getUnconditionalResult()).thenReturn(resultDescriptor3);
        when(resultDescriptor1.getStep()).thenReturn(12);
        when(resultDescriptor2.getStep()).thenReturn(23);
        when(resultDescriptor3.getStep()).thenReturn(34);

        return workflow;
    }

    private JiraWorkflow workflowWithMultipleTransitionsBetweenSameStatuses() {
        JiraWorkflow workflow = mock(JiraWorkflow.class);
        ActionDescriptor actionDescriptor1 = mock(ActionDescriptor.class);
        ActionDescriptor actionDescriptor2 = mock(ActionDescriptor.class);
        StepDescriptor stepDescriptor = mock(StepDescriptor.class);
        ResultDescriptor resultDescriptor = mock(ResultDescriptor.class);
        List<ActionDescriptor> actionDescriptors = newArrayList(actionDescriptor1, actionDescriptor2);

        when(workflow.getAllActions()).thenReturn(actionDescriptors);
        when(workflow.getStepsForTransition(actionDescriptor2)).thenReturn(newArrayList(stepDescriptor));
        when(stepDescriptor.getActions()).thenReturn(actionDescriptors);
        when(actionDescriptor1.getUnconditionalResult()).thenReturn(resultDescriptor);
        when(actionDescriptor2.getUnconditionalResult()).thenReturn(resultDescriptor);
        when(resultDescriptor.getStep()).thenReturn(54);

        return workflow;
    }

    private void issuesForQueryWithMaxLimit(int limit, Query query, Issue... issues) throws SearchException {
        SearchResults searchResults = new SearchResults(newArrayList(issues), new PagerFilter<>(limit));
        when(searchService.search(eq(loggedInUser), eq(query), any(PagerFilter.class))).thenReturn(searchResults);
        when(searchService.searchCount(loggedInUser, query)).thenReturn((long) issues.length);
    }

    private void accessibleStatusesForQueryAre(Query query, Status... statuses) {
        Set<Status> accessibleStatuses = new LinkedHashSet<>();
        Collections.addAll(accessibleStatuses, statuses);
        when(boardWorkflowService.getAccessibleStatuses(loggedInUser, query)).thenReturn(accessibleStatuses);
    }

    private void searchingForIssuesForQueryThrowsAnException(Query query) throws SearchException {
        when(searchService.search(eq(loggedInUser), eq(query), any(PagerFilter.class))).thenThrow(SearchException.class);
    }

    private void initialStatusesForQueryAre(Query query, Status... statuses) {
        when(boardWorkflowService.getInitialStatusesForQuery(loggedInUser, query)).thenReturn(newHashSet(statuses));
    }

    private void i18nHelperForUserIs(ApplicationUser user, I18nHelper i18nHelper) {
        when(beanFactory.getInstance(user)).thenReturn(i18nHelper);
    }

    private Issue issueWithAssignee(ApplicationUser assignee) {
        Issue issue = mock(Issue.class);
        when(issue.getAssignee()).thenReturn(assignee);

        return issue;
    }

    private static Status statusWithId(String id) {
        Status status = mock(Status.class);
        when(status.getId()).thenReturn(id);

        StatusCategory statusCategory = mock(StatusCategory.class);
        when(status.getStatusCategory()).thenReturn(statusCategory);

        return status;
    }

    private Status statusWithIdAndCategory(String id, String categoryName) {
        Status status = statusWithId(id);
        when(status.getStatusCategory()).thenReturn(StatusCategoryImpl.findByName(categoryName));

        return status;
    }

    private BoardData boardDataWith(List<Issue> issues, List<Status> statuses, List<ApplicationUser> assignees,
                                    List<Status> statusesForColumns, int numberOfResults, int totalNumberOfResults,
                                    boolean hasFilteredIssues) {
        return new BoardData.Builder()
                .board(BOARD)
                .jql(BOARD_JQL)
                .id(BOARD_ID)
                .issues(issues)
                .people(assignees)
                .columns(columnsFromStatuses(statusesForColumns))
                .maxResults(numberOfResults)
                .total(totalNumberOfResults)
                .hasFilteredDoneIssues(hasFilteredIssues)
                .build();
    }

    private List<BoardColumn> columnsFromStatuses(List<Status> statuses) {
        return statuses
                .stream()
                .map(status -> new BoardColumn(status, newArrayList(status)))
                .collect(toImmutableList());
    }

    private void numberOfWorkflowsForQueryIs(Query query, int numberOfWorkflows) {
        Set<JiraWorkflow> workflows = IntStream.range(0, numberOfWorkflows)
                .mapToObj(i -> mock(JiraWorkflow.class))
                .collect(toImmutableSet());
        workflowsForQueryAre(query, workflows.toArray(new JiraWorkflow[]{}));
    }

    private void workflowsForQueryAre(Query query, JiraWorkflow... workflows) {
        when(boardWorkflowService.getWorkflowsForQuery(loggedInUser, query)).thenReturn(newHashSet(workflows));
    }

    private Query augmentQueryWithAssigneeConstraint(Query baseQuery) {
        Query augmentedQuery = mock(Query.class);
        when(boardQueryService.getAugmentedQueryForDoneIssues(baseQuery)).thenReturn(augmentedQuery);

        return augmentedQuery;
    }
}
