package com.atlassian.jira.datetime;

import org.junit.Test;

import java.util.Locale;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

public class TestDateInputAdapter {

    private final DateInputAdapter adapter = DateInputAdapter.INSTANCE;

    @Test
    public void testMarIsTransformedToMarchInGermanyLocale() {
        assertThat(adapter.adapt("Mar", Locale.GERMANY), equalTo("M\u00e4r"));
        assertThat(adapter.adapt("1/Mar/2017", Locale.GERMANY), equalTo("1/M\u00e4r/2017"));

        assertThat(adapter.adapt("Marz", Locale.GERMANY), equalTo("M\u00e4r"));
        assertThat(adapter.adapt("1/Marz/2017", Locale.GERMANY), equalTo("1/M\u00e4r/2017"));
    }

    @Test
    public void testMrzIsTransformedToMarchInGermanyLocale() {
        assertThat(adapter.adapt("Mrz", Locale.GERMANY), equalTo("M\u00e4r"));
        assertThat(adapter.adapt("1/Mrz/2017", Locale.GERMANY), equalTo("1/M\u00e4r/2017"));
    }

    @Test
    public void testMrzIsTransformedToMarchInGermanyLocaleCaseInsensitive() {
        assertThat(adapter.adapt("mrz", Locale.GERMANY), equalTo("M\u00e4r"));
        assertThat(adapter.adapt("1/mrz/2017", Locale.GERMANY), equalTo("1/M\u00e4r/2017"));
    }

    @Test
    public void testMrzIsTransformedToMarchInGermanyLocaleAdvancedDateFormat() {
        assertThat(adapter.adapt("2017 -- Mrz 21", Locale.GERMANY), equalTo("2017 -- M\u00e4r 21"));
    }

    @Test
    public void testMrzIsNotTransformedToMarchInOtherLocales() {
        assertThat(adapter.adapt("Mrz", null), equalTo("Mrz"));
        assertThat(adapter.adapt("Mrz", Locale.ENGLISH), equalTo("Mrz"));
        assertThat(adapter.adapt("1/Mrz/2017", Locale.ENGLISH), equalTo("1/Mrz/2017"));
    }

    @Test
    public void testMrzIsNotTransformedIfPartOfWord() {
        assertThat(adapter.adapt("derMrz", Locale.GERMANY), equalTo("derMrz"));
        assertThat(adapter.adapt("MrzSchmetterling", Locale.GERMANY), equalTo("MrzSchmetterling"));
        assertThat(adapter.adapt("1/MrzSchmetterling/2017", Locale.GERMANY), equalTo("1/MrzSchmetterling/2017"));
        assertThat(adapter.adapt("2017 -- Mrz21", Locale.GERMANY), equalTo("2017 -- Mrz21"));
    }

    @Test
    public void testNullIsTransformedToNull() {
        assertThat(adapter.adapt(null, Locale.GERMANY), equalTo(null));
        assertThat(adapter.adapt(null, null), equalTo(null));
    }

    @Test
    public void emptyStringIsTransformedToEmptyString() {
        assertThat(adapter.adapt("", Locale.GERMANY), equalTo(""));
        assertThat(adapter.adapt("", null), equalTo(""));
    }
}