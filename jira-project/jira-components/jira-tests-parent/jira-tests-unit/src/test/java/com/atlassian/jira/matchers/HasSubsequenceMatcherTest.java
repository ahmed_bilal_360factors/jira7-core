package com.atlassian.jira.matchers;

import com.google.common.collect.ImmutableList;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.List;
import java.util.function.Function;

import static com.atlassian.jira.matchers.HasItemsInOrderMatcherTest.describedFunction;
import static com.google.common.collect.ImmutableList.of;
import static org.junit.Assert.assertThat;

@RunWith(Parameterized.class)
public class HasSubsequenceMatcherTest {
    final List<Integer> expected;
    final List<Integer> actual;
    final Function<Matcher<List<Integer>>, Matcher<List<Integer>>> notModifier;

    public HasSubsequenceMatcherTest(
            final List<Integer> actual,
            final Function<Matcher<List<Integer>>, Matcher<List<Integer>>> notModifier,
            final List<Integer> expected) {

        this.expected = expected;
        this.actual = actual;
        this.notModifier = notModifier;
    }

    @Parameterized.Parameters(name = "{0} {1} {2}")
    public static Iterable<Object[]> parameters() {
        final Function<Matcher<List<Integer>>, Matcher<List<Integer>>> hasSubsequence = describedFunction(Matchers::is, "has subsequence");
        final Function<Matcher<List<Integer>>, Matcher<List<Integer>>> hasNotSubsequence = describedFunction(Matchers::not, "has not subsequence");

        final ImmutableList<Integer> LIST_1_TO_6 = of(1, 2, 3, 4, 5, 6);
        return ImmutableList.copyOf(new Object[][]{
                        {LIST_1_TO_6, hasSubsequence, of(2, 3, 4)},
                        {LIST_1_TO_6, hasSubsequence, of()},
                        {LIST_1_TO_6, hasSubsequence, of(6)},
                        {of(1, 2, 1, 2, 3, 3), hasSubsequence, of(1, 2, 3)},
                        {of(), hasSubsequence, of()},

                        {LIST_1_TO_6, hasNotSubsequence, of(1, 2, 6)},
                        {LIST_1_TO_6, hasNotSubsequence, of(1, 1)},
                        {of(), hasNotSubsequence, of(1)}
                }
        );
    }

    @Test
    public void checkIfMatches() {
        final Matcher<List<Integer>> testedMatcher = HasSubsequenceMatcher.hasSubsequence(expected);
        final Matcher<List<Integer>> modifiedTestedMatcher = notModifier.apply(testedMatcher);

        assertThat(actual, modifiedTestedMatcher);
    }
}