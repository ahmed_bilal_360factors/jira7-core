package com.atlassian.jira.bc.imports.project;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.bc.JiraServiceContextImpl;
import com.atlassian.jira.bc.MockJiraServiceContext;
import com.atlassian.jira.bc.project.ProjectTypeValidator;
import com.atlassian.jira.bc.project.component.ProjectComponent;
import com.atlassian.jira.bc.project.component.ProjectComponentManager;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.config.util.MockAttachmentPathManager;
import com.atlassian.jira.external.ExternalException;
import com.atlassian.jira.external.beans.ExternalCustomField;
import com.atlassian.jira.external.beans.ExternalProject;
import com.atlassian.jira.imports.project.ProjectImportManager;
import com.atlassian.jira.imports.project.core.BackupOverview;
import com.atlassian.jira.imports.project.core.BackupOverviewImpl;
import com.atlassian.jira.imports.project.core.BackupProject;
import com.atlassian.jira.imports.project.core.BackupProjectImpl;
import com.atlassian.jira.imports.project.core.BackupSystemInformation;
import com.atlassian.jira.imports.project.core.BackupSystemInformationImpl;
import com.atlassian.jira.imports.project.core.MappingResult;
import com.atlassian.jira.imports.project.core.ProjectImportData;
import com.atlassian.jira.imports.project.core.ProjectImportDataImpl;
import com.atlassian.jira.imports.project.core.ProjectImportOptions;
import com.atlassian.jira.imports.project.core.ProjectImportOptionsImpl;
import com.atlassian.jira.imports.project.core.ProjectImportResults;
import com.atlassian.jira.imports.project.core.ProjectImportResultsImpl;
import com.atlassian.jira.imports.project.core.ValidationMessage;
import com.atlassian.jira.imports.project.customfield.ExternalCustomFieldConfiguration;
import com.atlassian.jira.imports.project.handler.AbortImportException;
import com.atlassian.jira.imports.project.mapper.ProjectImportMapperImpl;
import com.atlassian.jira.imports.project.taskprogress.TaskProgressInterval;
import com.atlassian.jira.imports.project.taskprogress.TaskProgressProcessor;
import com.atlassian.jira.imports.project.util.AoImportTemporaryFiles;
import com.atlassian.jira.imports.project.util.AoImportTemporaryFilesImpl;
import com.atlassian.jira.imports.project.util.MockProjectImportTemporaryFiles;
import com.atlassian.jira.imports.project.util.ProjectImportTemporaryFiles;
import com.atlassian.jira.issue.AttachmentManager;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.index.IndexException;
import com.atlassian.jira.junit.rules.InitMockitoMocks;
import com.atlassian.jira.mock.component.MockComponentWorker;
import com.atlassian.jira.plugin.PluginVersion;
import com.atlassian.jira.plugin.PluginVersionImpl;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.project.type.ProjectTypeKey;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.project.version.VersionManager;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.upgrade.UpgradeConstraints;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.MessageSet;
import com.atlassian.jira.util.MessageSetImpl;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.jira.util.collect.MapBuilder;
import com.atlassian.jira.web.bean.MockI18nBean;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.PluginInformation;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.compress.archivers.zip.ZipArchiveOutputStream;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.TestRule;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.xml.sax.SAXException;

import javax.annotation.Nonnull;
import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.atlassian.core.util.collection.EasyList.build;
import static com.atlassian.jira.config.properties.APKeys.JIRA_OPTION_ALLOWUNASSIGNED;
import static com.atlassian.jira.matchers.ErrorCollectionMatchers.containsFieldError;
import static com.atlassian.jira.matchers.ErrorCollectionMatchers.containsSystemError;
import static com.atlassian.jira.matchers.ErrorCollectionMatchers.isEmpty;
import static java.lang.String.format;
import static java.lang.System.getProperty;
import static java.util.Collections.emptyList;
import static java.util.Collections.emptyMap;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.iterableWithSize;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.refEq;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @since v3.13
 */
public class TestDefaultProjectImportService {
    @Rule
    public TestRule initMocks = new InitMockitoMocks(this);

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    private static final Set<String> emptyEntitySet = Collections.emptySet();
    private final ApplicationUser mockUser = new MockApplicationUser("test");
    @Mock
    private UpgradeConstraints upgradeConstraints;
    @Mock
    private ProjectTypeValidator projectTypeValidator;
    private File tempDirectory;
    private AoImportTemporaryFiles aoImportTemporaryFiles;
    private EventPublisher eventPublisher = Mockito.mock(EventPublisher.class);
    private static final String SAMPLE_PATH = "/some/path";
    private ProjectImportMapperImpl projectImportMapper;

    @Before
    public void setUp() throws Exception {
        ComponentAccessor.initialiseWorker(new MockComponentWorker());
        tempDirectory = createTempDirectory();
        aoImportTemporaryFiles = new AoImportTemporaryFilesImpl(tempDirectory);
        projectImportMapper = new ProjectImportMapperImpl(null, null);

        when(projectTypeValidator.isValid(any(), any())).thenReturn(true);
    }

    @After
    public void tearDown() throws Exception {
        FileUtils.deleteDirectory(tempDirectory);
    }

    @Test
    public void testValidateGetBackupOverviewHappyPathNoAttachmentPathProvided() throws IOException {
        PermissionManager mockPermissionManager = getPermissionManagerWithSysAdminAccess();

        DefaultProjectImportService projectImportService = new DefaultProjectImportService(mockPermissionManager, null, null, null, null, null, null, null, null, null, null, upgradeConstraints, eventPublisher, projectTypeValidator);

        final File tempFile = createTempBackup();
        tempFile.deleteOnExit();
        JiraServiceContext jiraServiceContext = getContext();
        projectImportService.validateGetBackupOverview(jiraServiceContext, new ProjectImportOptionsImpl(tempFile.getAbsolutePath(), null));

        assertThat(jiraServiceContext.getErrorCollection(), isEmpty());
    }

    @Test
    public void testValidateGetBackupOverviewHappyPathAttachmentPathProvided() throws IOException {
        final AttachmentManager mockAttachmentManager = mock(AttachmentManager.class);
        when(mockAttachmentManager.attachmentsEnabled()).thenReturn(true);

        PermissionManager mockPermissionManager = getPermissionManagerWithSysAdminAccess();

        DefaultProjectImportService projectImportService = new DefaultProjectImportService(mockPermissionManager, null, null, null, null, null, null, null, null, mockAttachmentManager, new MockAttachmentPathManager("/jira/attachments"), upgradeConstraints, eventPublisher, projectTypeValidator);

        final File tempFile = createTempBackup();
        tempFile.deleteOnExit();
        JiraServiceContext jiraServiceContext = getContext();
        projectImportService.validateGetBackupOverview(jiraServiceContext, new ProjectImportOptionsImpl(tempFile.getAbsolutePath(), System.getProperty("java.io.tmpdir")));

        assertThat(jiraServiceContext.getErrorCollection(), isEmpty());
    }

    @Test
    public void testValidateGetBackupOverviewPathToBackupNotAFile() throws IOException {
        PermissionManager mockPermissionManager = getPermissionManagerWithSysAdminAccess();

        DefaultProjectImportService projectImportService = new DefaultProjectImportService(mockPermissionManager, null, null, null, null, null, null, null, null, null, null, upgradeConstraints, eventPublisher, projectTypeValidator);

        JiraServiceContext jiraServiceContext = getContextWithI18HelperReturningKey();
        projectImportService.validateGetBackupOverview(jiraServiceContext, new ProjectImportOptionsImpl(System.getProperty("java.io.tmpdir"), null));

        assertThat(
                jiraServiceContext.getErrorCollection(),
                containsFieldError("backupPath", "admin.errors.project.import.invalid.backup.path"));
    }

    @Test
    public void testValidateGetBackupOverviewAttachmentPathProvidedDoesNotExist() throws IOException {
        final AttachmentManager mockAttachmentManager = mock(AttachmentManager.class);
        when(mockAttachmentManager.attachmentsEnabled()).thenReturn(true);

        PermissionManager mockPermissionManager = getPermissionManagerWithSysAdminAccess();

        DefaultProjectImportService projectImportService = new DefaultProjectImportService(mockPermissionManager, null, null, null, null, null, null, null, null, mockAttachmentManager, null, upgradeConstraints, eventPublisher, projectTypeValidator);

        final File tempFile = createTempBackup();
        tempFile.deleteOnExit();

        JiraServiceContext jiraServiceContext = getContextWithI18HelperReturningKey();
        projectImportService.validateGetBackupOverview(jiraServiceContext, new ProjectImportOptionsImpl(tempFile.getAbsolutePath(), "/thispathwillneverexistonanysystem"));

        assertThat(
                jiraServiceContext.getErrorCollection(),
                containsFieldError("backupAttachmentPath", "admin.errors.project.import.invalid.attachment.backup.path"));
    }

    @Test
    public void testValidateGetBackupOverviewAttachmentPathProvidedIsTheSameAsSystem() throws IOException {
        final AttachmentManager mockAttachmentManager = mock(AttachmentManager.class);
        when(mockAttachmentManager.attachmentsEnabled()).thenReturn(true);

        PermissionManager mockPermissionManager = getPermissionManagerWithSysAdminAccess();

        String attachmentPath = System.getProperty("java.io.tmpdir");

        final MockAttachmentPathManager attachmentPathManager = new MockAttachmentPathManager(attachmentPath);

        DefaultProjectImportService projectImportService = new DefaultProjectImportService(mockPermissionManager, null, null, null, null, null, null, null, null, mockAttachmentManager, attachmentPathManager, upgradeConstraints, eventPublisher, projectTypeValidator);

        final File tempFile = createTempBackup();
        tempFile.deleteOnExit();

        JiraServiceContext jiraServiceContext = getContextWithI18HelperReturningKey();
        projectImportService.validateGetBackupOverview(jiraServiceContext, new ProjectImportOptionsImpl(tempFile.getAbsolutePath(), attachmentPath));

        assertThat(
                jiraServiceContext.getErrorCollection(),
                containsFieldError("backupAttachmentPath", "admin.errors.project.import.attachment.backup.path.same.as.system"));
    }

    @Test
    public void testValidateGetBackupOverviewAttachmentPathProvidedButAttachmentsDisabled() throws IOException {
        final AttachmentManager mockAttachmentManager = mock(AttachmentManager.class);
        when(mockAttachmentManager.attachmentsEnabled()).thenReturn(false);

        PermissionManager mockPermissionManager = getPermissionManagerWithSysAdminAccess();

        DefaultProjectImportService projectImportService = new DefaultProjectImportService(mockPermissionManager, null, null, null, null, null, null, null, null, mockAttachmentManager, null, upgradeConstraints, eventPublisher, projectTypeValidator);

        final File tempFile = createTempBackup();
        tempFile.deleteOnExit();

        JiraServiceContext jiraServiceContext = getContextWithI18HelperReturningKey();
        projectImportService.validateGetBackupOverview(jiraServiceContext, new ProjectImportOptionsImpl(tempFile.getAbsolutePath(), System.getProperty("java.io.tmpdir")));

        assertThat(
                jiraServiceContext.getErrorCollection(),
                containsFieldError("backupAttachmentPath", "admin.errors.project.import.attachments.not.enabled"));
    }

    @Test
    public void testValidateGetBackupOverviewAttachmentPathIsEmptyStringHappyPath() throws IOException {
        PermissionManager mockPermissionManager = getPermissionManagerWithSysAdminAccess();

        DefaultProjectImportService projectImportService = new DefaultProjectImportService(mockPermissionManager, null, null, null, null, null, null, null, null, null, null, upgradeConstraints, eventPublisher, projectTypeValidator);

        final File tempFile = createTempBackup();
        tempFile.deleteOnExit();

        JiraServiceContext jiraServiceContext = getContext();
        projectImportService.validateGetBackupOverview(jiraServiceContext, new ProjectImportOptionsImpl(tempFile.getAbsolutePath(), ""));
        assertThat(
                jiraServiceContext.getErrorCollection(),
                isEmpty());
    }

    @Test
    public void testValidateGetBackupOverviewAttachmentPathProvidedIsNotADirectory() throws IOException {
        final AttachmentManager mockAttachmentManager = mock(AttachmentManager.class);
        when(mockAttachmentManager.attachmentsEnabled()).thenReturn(true);

        PermissionManager mockPermissionManager = getPermissionManagerWithSysAdminAccess();

        DefaultProjectImportService projectImportService = new DefaultProjectImportService(mockPermissionManager, null, null, null, null, null, null, null, null, mockAttachmentManager, null, upgradeConstraints, eventPublisher, projectTypeValidator);

        final File tempFile = createTempBackup();
        tempFile.deleteOnExit();

        JiraServiceContext jiraServiceContext = getContextWithI18HelperReturningKey();
        projectImportService.validateGetBackupOverview(jiraServiceContext, new ProjectImportOptionsImpl(tempFile.getAbsolutePath(), tempFile.getAbsolutePath()));

        assertThat(
                jiraServiceContext.getErrorCollection(),
                containsFieldError("backupAttachmentPath", "admin.errors.project.import.invalid.attachment.backup.path"));
    }

    @Test
    public void testValidateGetBackupOverviewBackupPathDoesNotExist() {
        final AttachmentManager mockAttachmentManager = mock(AttachmentManager.class);
        when(mockAttachmentManager.attachmentsEnabled()).thenReturn(true);

        PermissionManager mockPermissionManager = getPermissionManagerWithSysAdminAccess();

        DefaultProjectImportService projectImportService = new DefaultProjectImportService(mockPermissionManager, null, null, null, null, null, null, null, null, mockAttachmentManager, new MockAttachmentPathManager("/jira/attachments"), upgradeConstraints, eventPublisher, projectTypeValidator);

        JiraServiceContext jiraServiceContext = getContextWithI18HelperReturningKey();
        projectImportService.validateGetBackupOverview(jiraServiceContext, new ProjectImportOptionsImpl("/thispathwillneverexistonanysystem", System.getProperty("java.io.tmpdir")));

        assertThat(
                jiraServiceContext.getErrorCollection(),
                containsFieldError("backupPath", "admin.errors.project.import.invalid.backup.path"));
    }

    @Test
    public void testValidateGetBackupOverviewBackupPathDoesNotExistAndImportFileIsInvalidToo() {
        final AttachmentManager mockAttachmentManager = mock(AttachmentManager.class);
        when(mockAttachmentManager.attachmentsEnabled()).thenReturn(true);

        PermissionManager mockPermissionManager = getPermissionManagerWithSysAdminAccess();

        DefaultProjectImportService projectImportService = new DefaultProjectImportService(mockPermissionManager, null, null, null, null, null, null, null, null, mockAttachmentManager, null, upgradeConstraints, eventPublisher, projectTypeValidator);

        JiraServiceContext jiraServiceContext = getContextWithI18HelperReturningKey();
        projectImportService.validateGetBackupOverview(jiraServiceContext, new ProjectImportOptionsImpl("/thispathwillneverexistonanysystem/bak.xml", "/thispathwillneverexistonanysystem"));
        // We now should have 2 errors
        final Map<String, String> errorMessages = jiraServiceContext.getErrorCollection().getErrors();
        assertThat(errorMessages, hasEntry("backupPath", "admin.errors.project.import.invalid.backup.path"));
        assertThat(errorMessages, hasEntry("backupAttachmentPath", "admin.errors.project.import.invalid.attachment.backup.path"));
        assertThat(errorMessages.entrySet(), hasSize(2));
    }

    @Test
    public void testValidateGetBackupOverviewBackupPathDoesNotExistAndImportFileIsMissing() {
        final AttachmentManager mockAttachmentManager = mock(AttachmentManager.class);
        when(mockAttachmentManager.attachmentsEnabled()).thenReturn(true);

        final PermissionManager mockPermissionManager = getPermissionManagerWithSysAdminAccess();

        DefaultProjectImportService projectImportService = new DefaultProjectImportService(mockPermissionManager, null, null, null, null, null, null, null, null, mockAttachmentManager, null, upgradeConstraints, eventPublisher, projectTypeValidator);

        JiraServiceContext jiraServiceContext = getContextWithI18HelperReturningKey();
        projectImportService.validateGetBackupOverview(jiraServiceContext, new ProjectImportOptionsImpl("", "/thispathwillneverexistonanysystem"));
        // We now should have 2 errors

        final Map<String, String> errorMessages = jiraServiceContext.getErrorCollection().getErrors();
        assertThat(errorMessages, hasEntry("backupPath", "admin.errors.project.import.provide.backup.path"));
        assertThat(errorMessages, hasEntry("backupAttachmentPath", "admin.errors.project.import.invalid.attachment.backup.path"));
        assertThat(errorMessages.entrySet(), hasSize(2));
    }

    @Test
    public void testValidateGetBackupOverviewNoBackupPathProvided() {
        final AttachmentManager mockAttachmentManager = mock(AttachmentManager.class);
        when(mockAttachmentManager.attachmentsEnabled()).thenReturn(true);

        final PermissionManager mockPermissionManager = getPermissionManagerWithSysAdminAccess();

        DefaultProjectImportService projectImportService = new DefaultProjectImportService(mockPermissionManager, null, null, null, null, null, null, null, null, mockAttachmentManager, new MockAttachmentPathManager("/jira/attachments"), upgradeConstraints, eventPublisher, projectTypeValidator);

        JiraServiceContext jiraServiceContext = getContextWithI18HelperReturningKey();
        projectImportService.validateGetBackupOverview(jiraServiceContext, new ProjectImportOptionsImpl(null, System.getProperty("java.io.tmpdir")));
        assertThat(
                jiraServiceContext.getErrorCollection(),
                containsFieldError("backupPath", "admin.errors.project.import.provide.backup.path"));
    }

    @Test
    public void testValidateGetBackupOverviewNoPermission() {
        final PermissionManager mockPermissionManager = getPermissionManagerWithoutSysAdminAccess();
        DefaultProjectImportService projectImportService = new DefaultProjectImportService(mockPermissionManager, null, null, null, null, null, null, null, null, null, null, upgradeConstraints, eventPublisher, projectTypeValidator);

        JiraServiceContext jiraServiceContext = getContextWithI18HelperReturningKey();
        projectImportService.validateGetBackupOverview(jiraServiceContext, new ProjectImportOptionsImpl(getProperty("java.io.tmpdir"), getProperty("java.io.tmpdir")));
        assertThat(jiraServiceContext.getErrorCollection(), containsSystemError("admin.errors.project.import.must.be.admin"));
        assertThat(
                jiraServiceContext.getErrorCollection(),
                containsSystemError("admin.errors.project.import.must.be.admin"));
    }

    @Test
    public void testValidateJiraServiceContext() {
        DefaultProjectImportService projectImportService = new DefaultProjectImportService(null, null, null, null, null, null, null, null, null, null, null, upgradeConstraints, eventPublisher, projectTypeValidator);

        expectedException.expect(IllegalArgumentException.class);
        projectImportService.validateJiraServiceContext(null);
    }

    @Test
    public void testUserHasSysAdminPermission() {
        final PermissionManager mockPermissionManager = getPermissionManagerWithSysAdminAccess();

        DefaultProjectImportService projectImportService = new DefaultProjectImportService(mockPermissionManager, null, null, null, null, null, null, null, null, null, null, upgradeConstraints, eventPublisher, projectTypeValidator);

        assertThat(projectImportService.userHasSysAdminPermission(null), is(true));
    }

    @Test
    public void testUserNotHasSysAdminPermission() {
        final PermissionManager mockPermissionManager = getPermissionManagerWithoutSysAdminAccess();
        DefaultProjectImportService projectImportService = new DefaultProjectImportService(mockPermissionManager, null, null, null, null, null, null, null, null, null, null, upgradeConstraints, eventPublisher, projectTypeValidator);

        assertThat(projectImportService.userHasSysAdminPermission(null), is(false));
    }

    @Test
    public void testGetBackupOverviewHappyPath() throws IOException, SAXException {
        final PermissionManager mockPermissionManager = getPermissionManagerWithSysAdminAccess();
        ProjectImportManager mockProjectImportManager = mock(ProjectImportManager.class);
        BackupSystemInformation backupSystemInformation = new BackupSystemInformationImpl("1", "Prof", emptyList(), true, emptyMap(), emptyEntitySet, 0);
        BackupOverview backupOverview = new BackupOverviewImpl(backupSystemInformation, emptyList());
        when(mockProjectImportManager.getBackupOverview(eq(SAMPLE_PATH), any(), any())).thenReturn(backupOverview);
        when(upgradeConstraints.getTargetDatabaseBuildNumber()).thenReturn(1);

        DefaultProjectImportService projectImportService = new DefaultProjectImportService(mockPermissionManager, mockProjectImportManager, null, null, null, null, null, null, null, null, null, upgradeConstraints, eventPublisher, projectTypeValidator);
        JiraServiceContext jiraServiceContext = getContext();

        BackupOverview returnBackupOverview = projectImportService.getBackupOverview(jiraServiceContext, new ProjectImportOptionsImpl(SAMPLE_PATH, null), null);
        assertThat(returnBackupOverview, equalTo(backupOverview));
        assertThat(
                jiraServiceContext.getErrorCollection(),
                isEmpty());
    }

    @Test
    public void testGetBackupOverviewNoPermission() {
        final PermissionManager mockPermissionManager = getPermissionManagerWithoutSysAdminAccess();
        DefaultProjectImportService projectImportService = new DefaultProjectImportService(mockPermissionManager, null, null, null, null, null, null, null, null, null, null, upgradeConstraints, eventPublisher, projectTypeValidator);
        JiraServiceContext jiraServiceContext = getContextWithI18HelperReturningKey();

        final BackupOverview backupOverview = projectImportService.getBackupOverview(jiraServiceContext, new ProjectImportOptionsImpl(SAMPLE_PATH, null), null);
        assertThat(backupOverview, nullValue());
        assertThat(
                jiraServiceContext.getErrorCollection(),
                containsSystemError("admin.errors.project.import.must.be.admin"));
    }

    @Test
    public void testGetBackupOverviewSaxException() throws IOException, SAXException {
        final PermissionManager mockPermissionManager = getPermissionManagerWithSysAdminAccess();
        ProjectImportManager mockProjectImportManager = mock(ProjectImportManager.class);
        final String exceptionMessage = "I am an exception.";
        when(mockProjectImportManager.getBackupOverview(eq(SAMPLE_PATH), any(), any())).thenThrow(new SAXException(exceptionMessage));

        DefaultProjectImportService projectImportService = new DefaultProjectImportService(mockPermissionManager, mockProjectImportManager, null, null, null, null, null, null, null, null, null, upgradeConstraints, eventPublisher, projectTypeValidator);
        JiraServiceContext jiraServiceContext = getContext();

        final BackupOverview backupOverview = projectImportService.getBackupOverview(jiraServiceContext, new ProjectImportOptionsImpl(SAMPLE_PATH, null), null);

        assertThat(backupOverview, nullValue());

        assertThat(
                jiraServiceContext.getErrorCollection(),
                containsSystemError(format("There was a problem parsing the backup XML file at %s: %s", SAMPLE_PATH, exceptionMessage)));
    }

    @Test
    public void testGetBackupOverviewIoException() throws IOException, SAXException {
        final PermissionManager mockPermissionManager = getPermissionManagerWithSysAdminAccess();
        ProjectImportManager mockProjectImportManager = mock(ProjectImportManager.class);
        when(mockProjectImportManager.getBackupOverview(eq(SAMPLE_PATH), any(), any())).thenThrow(new IOException());

        DefaultProjectImportService projectImportService = new DefaultProjectImportService(mockPermissionManager, mockProjectImportManager, null, null, null, null, null, null, null, null, null, upgradeConstraints, eventPublisher, projectTypeValidator);
        JiraServiceContext jiraServiceContext = getContext();

        final BackupOverview backupOverview = projectImportService.getBackupOverview(jiraServiceContext, new ProjectImportOptionsImpl(SAMPLE_PATH, null), null);

        assertThat(backupOverview, nullValue());
        assertThat(
                jiraServiceContext.getErrorCollection(),
                containsSystemError(format("There was a problem accessing the backup XML file at %s.", SAMPLE_PATH)));
    }

    @Test
    public void testGetBackupOverviewWrongBuildNumber() throws IOException, SAXException {
        final int currentBuildNumber = 12;
        final String assumedBuildNumber = "10";

        final PermissionManager mockPermissionManager = getPermissionManagerWithSysAdminAccess();
        BackupSystemInformation backupSystemInformation = new BackupSystemInformationImpl(assumedBuildNumber, "Professional", emptyList(), true, emptyMap(), emptyEntitySet, 0);
        BackupOverview expectedBackupOverview = new BackupOverviewImpl(backupSystemInformation, emptyList());
        ProjectImportManager mockProjectImportManager = mock(ProjectImportManager.class);
        when(mockProjectImportManager.getBackupOverview(eq(SAMPLE_PATH), any(), any())).thenReturn(expectedBackupOverview);
        when(upgradeConstraints.getTargetDatabaseBuildNumber()).thenReturn(currentBuildNumber);

        DefaultProjectImportService projectImportService = new DefaultProjectImportService(mockPermissionManager, mockProjectImportManager, null, null, null, null, null, null, null, null, null, upgradeConstraints, eventPublisher, projectTypeValidator);
        JiraServiceContext jiraServiceContext = getContext();

        // With wrong build number we return null
        final BackupOverview backupOverview = projectImportService.getBackupOverview(jiraServiceContext, new ProjectImportOptionsImpl(SAMPLE_PATH, null), null);

        assertThat(backupOverview, nullValue());
        assertThat(
                jiraServiceContext.getErrorCollection(),
                containsSystemError(format("This data appears to be from an older version of JIRA. Please upgrade the data and try again. The current version of JIRA is at build number '%s', but the supplied backup file was for build number '%s'.", currentBuildNumber, assumedBuildNumber)));
    }

    @Test
    public void testValidateBackupProjectImportableSystemLevelHappyPath() {
        Long projectId = 123L;
        String projectKey = "TST";

        final PermissionManager mockPermissionManager = getPermissionManagerWithSysAdminAccess();
        ProjectManager mockProjectManager = mock(ProjectManager.class);
        when(mockProjectManager.getProjectObjByKey(projectKey)).thenReturn(new MockProject(projectId.longValue(), projectKey));

        final IssueManager mockIssueManager = mock(IssueManager.class);
        when(mockIssueManager.getIssueCountForProject(projectId)).thenReturn(0L);

        final VersionManager mockVersionManager = mock(VersionManager.class);
        when(mockVersionManager.getVersions(projectId)).thenReturn(emptyList());

        final ProjectComponentManager mockProjectComponentManager = mock(ProjectComponentManager.class);
        when(mockProjectComponentManager.findAllForProject(projectId)).thenReturn(emptyList());

        PluginInformation pluginInfo = new PluginInformation();
        pluginInfo.setVersion("1.0");

        Plugin mockPlugin = mock(Plugin.class);
        when(mockPlugin.getPluginInformation()).thenReturn(pluginInfo);

        PluginAccessor mockPluginAccessor = mock(PluginAccessor.class);
        when(mockPluginAccessor.getPlugin(any())).thenReturn(mockPlugin);

        DefaultProjectImportService projectImportService = new DefaultProjectImportService(mockPermissionManager, null, mockProjectManager, null, mockIssueManager, mockVersionManager, mockProjectComponentManager, mockPluginAccessor, null, null, null, upgradeConstraints, eventPublisher, projectTypeValidator);

        JiraServiceContext jiraServiceContext = getContext();

        ExternalProject externalProject = new ExternalProject();
        externalProject.setId(projectId.toString());
        externalProject.setKey(projectKey);
        externalProject.setAssigneeType("2");

        ExternalCustomField externalCustomField = new ExternalCustomField("678", "cust field", "cust.field.key:module");
        ExternalCustomFieldConfiguration customFieldConfiguration = new ExternalCustomFieldConfiguration(emptyList(), null, externalCustomField, "321");

        BackupProject backupProject = new BackupProjectImpl(externalProject, emptyList(), emptyList(), build(customFieldConfiguration), build(1L, 2L), 0, ImmutableMap.of());

        PluginVersion pluginVersion = new PluginVersionImpl("cust.field.key", "blah", "1.0", new Date());
        BackupSystemInformation backupSystemInformation = new BackupSystemInformationImpl("456", "Prof", build(pluginVersion), true, emptyMap(), emptyEntitySet, 0);

        MessageSet messageSet = projectImportService.validateBackupProjectImportableSystemLevel(jiraServiceContext, backupProject, backupSystemInformation);

        assertThat(messageSet.getErrorMessages(), empty());
        assertThat(messageSet.getWarningMessages(), empty());
        assertThat(jiraServiceContext.getErrorCollection(), isEmpty());
    }

    /**
     * This test will test that errors are reported when the Project to be imported into has issues, versions, or
     * components. It tests all three at once on purpose in order to verify that we can handle multiple errors
     * collected
     * in a single validation.
     */
    @Test
    public void testValidateBackupProjectImportableSystemLevelIssueVersionAndComponentCountNonZero() {
        Long projectId = 123L;
        String projectKey = "TST";
        final PermissionManager mockPermissionManager = getPermissionManagerWithSysAdminAccess();
        ProjectManager mockProjectManager = mock(ProjectManager.class);
        when(mockProjectManager.getProjectObjByKey(projectKey)).thenReturn(new MockProject(projectId.longValue(), projectKey));

        final IssueManager mockIssueManager = mock(IssueManager.class);
        when(mockIssueManager.getIssueCountForProject(projectId)).thenReturn(12L);

        final VersionManager mockVersionManager = mock(VersionManager.class);
        when(mockVersionManager.getVersions(projectId)).thenReturn(ImmutableList.of(mock(Version.class), mock(Version.class)));

        final ProjectComponentManager mockProjectComponentManager = mock(ProjectComponentManager.class);
        when(mockProjectComponentManager.findAllForProject(projectId)).thenReturn(ImmutableList.of(mock(ProjectComponent.class)));

        PluginInformation pluginInfo = new PluginInformation();
        pluginInfo.setVersion("1.0");

        Plugin mockPlugin = mock(Plugin.class);
        when(mockPlugin.getPluginInformation()).thenReturn(pluginInfo);

        PluginAccessor mockPluginAccessor = mock(PluginAccessor.class);
        when(mockPluginAccessor.getPlugin(any())).thenReturn(mockPlugin);

        ApplicationProperties mockApplicationProperties = mock(ApplicationProperties.class);
        when(mockApplicationProperties.getOption(JIRA_OPTION_ALLOWUNASSIGNED)).thenReturn(true);

        DefaultProjectImportService projectImportService = new DefaultProjectImportService(mockPermissionManager, null, mockProjectManager, null, mockIssueManager, mockVersionManager, mockProjectComponentManager, mockPluginAccessor, mockApplicationProperties, null, null, upgradeConstraints, eventPublisher, projectTypeValidator);

        JiraServiceContext jiraServiceContext = getContext();

        ExternalProject externalProject = new ExternalProject();
        externalProject.setId(projectId.toString());
        externalProject.setKey(projectKey);

        ExternalCustomField externalCustomField = new ExternalCustomField("678", "cust field", "cust.field.key:module");
        ExternalCustomFieldConfiguration customFieldConfiguration = new ExternalCustomFieldConfiguration(emptyList(), null, externalCustomField, "321");

        BackupProject backupProject = new BackupProjectImpl(externalProject, emptyList(), emptyList(), ImmutableList.of(customFieldConfiguration), ImmutableList.of(1L, 2L), 0, ImmutableMap.of());

        PluginVersion pluginVersion = new PluginVersionImpl("cust.field.key", "blah", "1.0", new Date());
        BackupSystemInformation backupSystemInformation = new BackupSystemInformationImpl("456", "Prof", ImmutableList.of(pluginVersion), true, emptyMap(), emptyEntitySet, 0);

        MessageSet messageSet = projectImportService.validateBackupProjectImportableSystemLevel(jiraServiceContext, backupProject, backupSystemInformation);
        assertThat(messageSet.getErrorMessages(), iterableWithSize(3));
        // No waarings (only errors)
        assertThat(messageSet.getWarningMessages(), empty());
        // Three errors

        final Collection<String> errorMessages = jiraServiceContext.getErrorCollection().getErrorMessages();
        assertThat(errorMessages, containsInAnyOrder(
                "The existing project with key 'TST' contains '12' issues. You can not import a backup project into a project that contains existing issues.",
                "The existing project with key 'TST' contains '2' versions. You can not import a backup project into a project that contains existing versions.",
                "The existing project with key 'TST' contains '1' components. You can not import a backup project into a project that contains existing components."
        ));
    }

    @Test
    public void testValidateBackupProjectImportableSystemLevelInvalidProjectType() {
        Long projectId = 123L;
        String projectKey = "TST";

        final PermissionManager mockPermissionManager = getPermissionManagerWithSysAdminAccess();
        ProjectManager mockProjectManager = mock(ProjectManager.class);
        when(mockProjectManager.getProjectObjByKey(projectKey)).thenReturn(new MockProject(projectId.longValue(), projectKey));

        final IssueManager mockIssueManager = mock(IssueManager.class);
        when(mockIssueManager.getIssueCountForProject(projectId)).thenReturn(0L);

        final VersionManager mockVersionManager = mock(VersionManager.class);
        when(mockVersionManager.getVersions(projectId)).thenReturn(emptyList());

        final ProjectComponentManager mockProjectComponentManager = mock(ProjectComponentManager.class);
        when(mockProjectComponentManager.findAllForProject(projectId)).thenReturn(emptyList());

        PluginInformation pluginInfo = new PluginInformation();
        pluginInfo.setVersion("1.0");

        Plugin mockPlugin = mock(Plugin.class);
        when(mockPlugin.getPluginInformation()).thenReturn(pluginInfo);

        PluginAccessor mockPluginAccessor = mock(PluginAccessor.class);
        when(mockPluginAccessor.getPlugin(any())).thenReturn(mockPlugin);

        DefaultProjectImportService projectImportService = new DefaultProjectImportService(mockPermissionManager, null, mockProjectManager, null, mockIssueManager, mockVersionManager, mockProjectComponentManager, mockPluginAccessor, null, null, null, upgradeConstraints, eventPublisher, projectTypeValidator);

        JiraServiceContext jiraServiceContext = getContext();

        ExternalProject externalProject = new ExternalProject();
        externalProject.setId(projectId.toString());
        externalProject.setKey(projectKey);
        externalProject.setAssigneeType("2");
        externalProject.setProjectTypeKey("invalidKey");

        ExternalCustomField externalCustomField = new ExternalCustomField("678", "cust field", "cust.field.key:module");
        ExternalCustomFieldConfiguration customFieldConfiguration = new ExternalCustomFieldConfiguration(emptyList(), null, externalCustomField, "321");

        BackupProject backupProject = new BackupProjectImpl(externalProject, emptyList(), emptyList(), build(customFieldConfiguration), build(1L, 2L), 0, ImmutableMap.of());

        PluginVersion pluginVersion = new PluginVersionImpl("cust.field.key", "blah", "1.0", new Date());
        BackupSystemInformation backupSystemInformation = new BackupSystemInformationImpl("456", "Prof", build(pluginVersion), true, emptyMap(), emptyEntitySet, 0);

        when(projectTypeValidator.isValid(any(ApplicationUser.class), eq(new ProjectTypeKey("invalidKey")))).thenReturn(false);
        MessageSet messageSet = projectImportService.validateBackupProjectImportableSystemLevel(jiraServiceContext, backupProject, backupSystemInformation);

        final Collection<String> errorMessages = jiraServiceContext.getErrorCollection().getErrorMessages();
        assertThat(errorMessages, containsInAnyOrder(
                "The project with key 'TST' is a 'InvalidKey' project. You can not import this backup project, until the application providing this type has been installed and licensed."
        ));
    }

    /**
     * Tests the case where the project has Default Assignee as Unassigned, but this is illegal because this JIRA does
     * not allow unassigned issues.
     */
    @Test
    public void testValidateBackupProjectImportableSystemLevelProjectHasDefaultAssigneeUnassignedNontAllowed() {
        Long projectId = 123L;
        String projectKey = "TST";
        final PermissionManager mockPermissionManager = getPermissionManagerWithSysAdminAccess();
        ProjectManager mockProjectManager = mock(ProjectManager.class);
        when(mockProjectManager.getProjectObjByKey(projectKey)).thenReturn(new MockProject(projectId.longValue(), projectKey));

        final IssueManager mockIssueManager = mock(IssueManager.class);
        when(mockIssueManager.getIssueCountForProject(projectId)).thenReturn(0L);

        final VersionManager mockVersionManager = mock(VersionManager.class);
        when(mockVersionManager.getVersions(projectId)).thenReturn(emptyList());

        final ProjectComponentManager mockProjectComponentManager = mock(ProjectComponentManager.class);
        when(mockProjectComponentManager.findAllForProject(projectId)).thenReturn(emptyList());

        PluginInformation pluginInfo = new PluginInformation();
        pluginInfo.setVersion("1.0");

        Plugin mockPlugin = mock(Plugin.class);
        when(mockPlugin.getPluginInformation()).thenReturn(pluginInfo);

        PluginAccessor mockPluginAccessor = mock(PluginAccessor.class);
        when(mockPluginAccessor.getPlugin(any())).thenReturn(mockPlugin);

        ApplicationProperties mockApplicationProperties = mock(ApplicationProperties.class);
        when(mockApplicationProperties.getOption(JIRA_OPTION_ALLOWUNASSIGNED)).thenReturn(false);


        DefaultProjectImportService projectImportService = new DefaultProjectImportService(mockPermissionManager, null, mockProjectManager, null, mockIssueManager, mockVersionManager, mockProjectComponentManager, mockPluginAccessor, mockApplicationProperties, null, null, upgradeConstraints, eventPublisher, projectTypeValidator);

        JiraServiceContext jiraServiceContext = getContext();

        ExternalProject externalProject = new ExternalProject();
        externalProject.setId(projectId.toString());
        externalProject.setKey(projectKey);

        ExternalCustomField externalCustomField = new ExternalCustomField("678", "cust field", "cust.field.key:module");
        ExternalCustomFieldConfiguration customFieldConfiguration = new ExternalCustomFieldConfiguration(emptyList(), null, externalCustomField, "321");

        BackupProject backupProject = new BackupProjectImpl(externalProject, emptyList(), emptyList(),
                ImmutableList.of(customFieldConfiguration), ImmutableList.of(1L, 2L), 0, ImmutableMap.of());

        PluginVersion pluginVersion = new PluginVersionImpl("cust.field.key", "blah", "1.0", new Date());
        BackupSystemInformation backupSystemInformation = new BackupSystemInformationImpl("456", "Prof", ImmutableList.of(pluginVersion), true, emptyMap(), emptyEntitySet, 0);

        MessageSet messageSet = projectImportService.validateBackupProjectImportableSystemLevel(jiraServiceContext, backupProject, backupSystemInformation);

        assertThat(messageSet.getErrorMessages(), not(empty()));
        // No Warnings
        assertThat(messageSet.getWarningMessages(), empty());

        final Collection<String> errorMessages = jiraServiceContext.getErrorCollection().getErrorMessages();
        assertThat(errorMessages, containsInAnyOrder(
                "The backup project 'null' has 'unassigned' default assignee, but this JIRA instance does not allow unassigned issues."
        ));
    }

    @Test
    public void testValidateBackupProjectImportableSystemLevelProjectDoesntExist() {
        Long projectId = 123L;
        String projectKey = "TST";
        final PermissionManager mockPermissionManager = getPermissionManagerWithSysAdminAccess();
        ProjectManager mockProjectManager = mock(ProjectManager.class);
        when(mockProjectManager.getProjectObjByKey(projectKey)).thenReturn(null);

        PluginInformation pluginInfo = new PluginInformation();
        pluginInfo.setVersion("1.0");

        Plugin mockPlugin = mock(Plugin.class);
        when(mockPlugin.getPluginInformation()).thenReturn(pluginInfo);

        PluginAccessor mockPluginAccessor = mock(PluginAccessor.class);
        when(mockPluginAccessor.getPlugin(any())).thenReturn(mockPlugin);

        DefaultProjectImportService projectImportService = new DefaultProjectImportService(mockPermissionManager, null, mockProjectManager, null, null, null, null, mockPluginAccessor, null, null, null, upgradeConstraints, eventPublisher, projectTypeValidator);

        JiraServiceContext jiraServiceContext = getContext();

        ExternalProject externalProject = new ExternalProject();
        externalProject.setId(projectId.toString());
        externalProject.setKey(projectKey);

        ExternalCustomField externalCustomField = new ExternalCustomField("678", "cust field", "cust.field.key:module");
        ExternalCustomFieldConfiguration customFieldConfiguration = new ExternalCustomFieldConfiguration(emptyList(), null, externalCustomField, "321");

        BackupProject backupProject = new BackupProjectImpl(externalProject, emptyList(), emptyList(), ImmutableList.of(customFieldConfiguration), ImmutableList.of(1L, 2L), 0, ImmutableMap.of());

        PluginVersion pluginVersion = new PluginVersionImpl("cust.field.key", "blah", "1.0", new Date());
        BackupSystemInformation backupSystemInformation = new BackupSystemInformationImpl("456", "Prof", ImmutableList.of(pluginVersion), true, emptyMap(), emptyEntitySet, 0);

        MessageSet messageSet = projectImportService.validateBackupProjectImportableSystemLevel(jiraServiceContext, backupProject, backupSystemInformation);

        assertThat(messageSet.getWarningMessages(), empty());
        assertThat(messageSet.getErrorMessages(), contains("JIRA did not find a project with key 'TST' to import your issues into. Please create a project with key 'TST' that contains compatible workflow and issue types."));
        assertThat(jiraServiceContext.getErrorCollection(), containsSystemError("JIRA did not find a project with key 'TST' to import your issues into. Please create a project with key 'TST' that contains compatible workflow and issue types."));
    }

    @Test
    public void testValidateBackupProjectImportableSystemLevelNullBackupProject() {
        DefaultProjectImportService projectImportService = new DefaultProjectImportService(null, null, null, null, null, null, null, null, null, null, null, upgradeConstraints, eventPublisher, projectTypeValidator);

        JiraServiceContext jiraServiceContext = getContext();
        BackupSystemInformation backupSystemInformation = new BackupSystemInformationImpl("456", "Prof", Collections.emptyList(), true, Collections.emptyMap(), emptyEntitySet, 0);

        final MessageSet messageSet = projectImportService.validateBackupProjectImportableSystemLevel(jiraServiceContext, null, backupSystemInformation);

        assertThat(messageSet.getErrorMessages(), not(empty()));
        assertThat(jiraServiceContext.getErrorCollection(), containsSystemError("You can not import a null project."));
    }

    @Test
    public void testNoPermission() {
        final PermissionManager mockPermissionManager = getPermissionManagerWithoutSysAdminAccess();
        DefaultProjectImportService defaultProjectImportService = new DefaultProjectImportService(mockPermissionManager, null, null, null, null, null, null, null, null, null, null, upgradeConstraints, eventPublisher, projectTypeValidator);
        JiraServiceContext jiraServiceContext = getContext();
        ExternalProject externalProject = new ExternalProject();
        externalProject.setId("999");
        externalProject.setKey("None");
        BackupProject backupProject = new BackupProjectImpl(externalProject, emptyList(), emptyList(), emptyList(), ImmutableList.of(1L, 2L), 0, ImmutableMap.of());

        BackupSystemInformation backupSystemInformation = new BackupSystemInformationImpl("456", "Prof", emptyList(), true, emptyMap(), emptyEntitySet, 0);
        final MessageSet messageSet = defaultProjectImportService.validateBackupProjectImportableSystemLevel(jiraServiceContext, backupProject, backupSystemInformation);

        assertThat(messageSet.getErrorMessages(), containsInAnyOrder(
                "You must be a JIRA System Administrator to perform a project import."
        ));
    }

    @Test
    public void testCustomFieldWrongVersion() {
        Long projectId = 123L;
        String projectKey = "TST";
        final PermissionManager mockPermissionManager = getPermissionManagerWithSysAdminAccess();

        ProjectManager mockProjectManager = mock(ProjectManager.class);
        when(mockProjectManager.getProjectObjByKey(projectKey)).thenReturn(new MockProject(projectId, projectKey));

        Plugin mockPlugin = mock(Plugin.class);
        when(mockPlugin.getPluginInformation()).thenReturn(getPluginInformation("1.4"));

        Plugin mockPlugin2 = mock(Plugin.class);
        when(mockPlugin2.getPluginInformation()).thenReturn(getPluginInformation("1.2"));

        final IssueManager mockIssueManager = mock(IssueManager.class);
        when(mockIssueManager.getIssueCountForProject(projectId)).thenReturn(0L);

        final VersionManager mockVersionManager = mock(VersionManager.class);
        when(mockVersionManager.getVersions(projectId)).thenReturn(emptyList());

        final ProjectComponentManager mockProjectComponentManager = mock(ProjectComponentManager.class);
        when(mockProjectComponentManager.findAllForProject(projectId)).thenReturn(emptyList());

        PluginAccessor mockPluginAccessor = mock(PluginAccessor.class);
        when(mockPluginAccessor.getPlugin("ABC")).thenReturn(mockPlugin);
        when(mockPluginAccessor.getPlugin("DEF")).thenReturn(mockPlugin2);
        when(mockPluginAccessor.getPlugin("XYZ")).thenReturn(null);

        ApplicationProperties mockApplicationProperties = mock(ApplicationProperties.class);
        when(mockApplicationProperties.getOption(JIRA_OPTION_ALLOWUNASSIGNED)).thenReturn(true);

        DefaultProjectImportService projectImportService = new DefaultProjectImportService(mockPermissionManager, null, mockProjectManager, null, mockIssueManager, mockVersionManager, mockProjectComponentManager, mockPluginAccessor, mockApplicationProperties, null, null, upgradeConstraints, eventPublisher, projectTypeValidator);

        JiraServiceContext jiraServiceContext = getContext();

        ExternalProject externalProject = new ExternalProject();
        externalProject.setId(projectId.toString());
        externalProject.setKey(projectKey);

        ExternalCustomField externalCustomField1 = new ExternalCustomField("10", "Stuff", "ABC:stuff");
        ExternalCustomFieldConfiguration customFieldConfiguration1 = new ExternalCustomFieldConfiguration(emptyList(), null, externalCustomField1, "321");
        ExternalCustomField externalCustomField2 = new ExternalCustomField("14", "More Stuff", "DEF:morestuff");
        ExternalCustomFieldConfiguration customFieldConfiguration2 = new ExternalCustomFieldConfiguration(emptyList(), null, externalCustomField2, "321");
        ExternalCustomField externalCustomField3 = new ExternalCustomField("12", "Other Stuff", "XYZ:otherstuff");
        ExternalCustomFieldConfiguration customFieldConfiguration3 = new ExternalCustomFieldConfiguration(emptyList(), null, externalCustomField3, "321");

        BackupProject backupProject = new BackupProjectImpl(externalProject, emptyList(), emptyList(), build(customFieldConfiguration1, customFieldConfiguration2, customFieldConfiguration3), ImmutableList.of(1L, 2L), 0, ImmutableMap.of());

        PluginVersion pluginVersion1 = new PluginVersionImpl("XYZ", "otherstuff", "1.0", new Date());
        PluginVersion pluginVersion2 = new PluginVersionImpl("ABC", "stuff", "1.0", new Date());
        PluginVersion pluginVersion3 = new PluginVersionImpl("DEF", "stuff", "1.2", new Date());
        BackupSystemInformation backupSystemInformation = new BackupSystemInformationImpl("456", "Prof", ImmutableList.of(pluginVersion1, pluginVersion2, pluginVersion3), true, emptyMap(), emptyEntitySet, 0);

        MessageSet messageSet = projectImportService.validateBackupProjectImportableSystemLevel(jiraServiceContext, backupProject, backupSystemInformation);
        assertThat(messageSet.getErrorMessages(), not(empty()));
        // Note that we should get a whinge on version of ABC, but nothing on XYZ which is missing from the current JIRA.
        assertThat(messageSet.getErrorMessages(), containsInAnyOrder(
                "The backup project 'null' requires custom field named 'Stuff' with full key 'ABC:stuff'. In the current instance of JIRA the plugin is at version '1.4', but in the backup it is at version '1.0'."
        ));
    }

    @Test
    public void testCustomFieldDoesNotExistInBackup() {
        Long projectId = 123L;
        String projectKey = "TST";
        final PermissionManager mockPermissionManager = getPermissionManagerWithSysAdminAccess();

        ProjectManager mockProjectManager = mock(ProjectManager.class);
        when(mockProjectManager.getProjectObjByKey(projectKey)).thenReturn(new MockProject(projectId, projectKey));

        final IssueManager mockIssueManager = mock(IssueManager.class);
        when(mockIssueManager.getIssueCountForProject(projectId)).thenReturn(0L);

        final VersionManager mockVersionManager = mock(VersionManager.class);
        when(mockVersionManager.getVersions(projectId)).thenReturn(emptyList());

        final ProjectComponentManager mockProjectComponentManager = mock(ProjectComponentManager.class);
        when(mockProjectComponentManager.findAllForProject(projectId)).thenReturn(emptyList());

        Plugin mockPlugin = mock(Plugin.class);
        when(mockPlugin.getPluginInformation()).thenReturn(getPluginInformation("1.4"));

        Plugin mockPlugin2 = mock(Plugin.class);
        when(mockPlugin2.getPluginInformation()).thenReturn(getPluginInformation("1.2"));

        PluginAccessor mockPluginAccessor = mock(PluginAccessor.class);
        when(mockPluginAccessor.getPlugin("ABC")).thenReturn(mockPlugin);
        when(mockPluginAccessor.getPlugin("DEF")).thenReturn(mockPlugin2);
        when(mockPluginAccessor.getPlugin("XYZ")).thenReturn(null);

        ApplicationProperties mockApplicationProperties = mock(ApplicationProperties.class);
        when(mockApplicationProperties.getOption(JIRA_OPTION_ALLOWUNASSIGNED)).thenReturn(true);

        DefaultProjectImportService projectImportService = new DefaultProjectImportService(mockPermissionManager, null, mockProjectManager, null, mockIssueManager, mockVersionManager, mockProjectComponentManager, mockPluginAccessor, mockApplicationProperties, null, null, upgradeConstraints, eventPublisher, projectTypeValidator);

        JiraServiceContext jiraServiceContext = getContext();

        ExternalProject externalProject = new ExternalProject();
        externalProject.setId(projectId.toString());
        externalProject.setKey(projectKey);

        ExternalCustomField externalCustomField1 = new ExternalCustomField("10", "Stuff", "ABC:stuff");
        ExternalCustomFieldConfiguration customFieldConfiguration1 = new ExternalCustomFieldConfiguration(emptyList(), null, externalCustomField1, "321");
        ExternalCustomField externalCustomField2 = new ExternalCustomField("14", "More Stuff", "DEF:morestuff");
        ExternalCustomFieldConfiguration customFieldConfiguration2 = new ExternalCustomFieldConfiguration(emptyList(), null, externalCustomField2, "321");
        ExternalCustomField externalCustomField3 = new ExternalCustomField("12", "Other Stuff", "XYZ:otherstuff");
        ExternalCustomFieldConfiguration customFieldConfiguration3 = new ExternalCustomFieldConfiguration(emptyList(), null, externalCustomField3, "321");

        BackupProject backupProject = new BackupProjectImpl(externalProject, emptyList(), emptyList(), ImmutableList.of(customFieldConfiguration1, customFieldConfiguration2, customFieldConfiguration3), ImmutableList.of(1L, 2L), 0, ImmutableMap.of());

        PluginVersion pluginVersion1 = new PluginVersionImpl("XYZ", "otherstuff", "1.0", new Date());
        PluginVersion pluginVersion3 = new PluginVersionImpl("DEF", "stuff", "1.2", new Date());
        BackupSystemInformation backupSystemInformation = new BackupSystemInformationImpl("456", "Prof", ImmutableList.of(pluginVersion1, pluginVersion3), true, emptyMap(), emptyEntitySet, 0);

        MessageSet messageSet = projectImportService.validateBackupProjectImportableSystemLevel(jiraServiceContext, backupProject, backupSystemInformation);
        assertThat(messageSet.getErrorMessages(), not(empty()));
        // Note that we should get a whinge on version of ABC, but nothing on XYZ which is missing from the current JIRA.
        assertThat(messageSet.getErrorMessages(), containsInAnyOrder(
                "The backup project 'null' requires custom field named 'Stuff' with full key 'ABC:stuff'. In the current instance of JIRA the plugin is at version '1.4', but this custom field was not installed in the backup data. You may want to create an XML backup with this version of the plugin installed."
        ));
    }

    @Test
    public void testValidateDoMappingHappyPath() throws IOException {
        final PermissionManager mockPermissionManager = getPermissionManagerWithSysAdminAccess();

        DefaultProjectImportService projectImportService = new DefaultProjectImportService(mockPermissionManager, null, null, null, null, null, null, null, null, null, null, upgradeConstraints, eventPublisher, projectTypeValidator);

        final File tempFile = createTempBackup();
        tempFile.deleteOnExit();
        JiraServiceContext jiraServiceContext = getContext();
        final BackupProject backupProject = new BackupProjectImpl(new ExternalProject(), emptyList(), emptyList(), emptyList(), emptyList(), 0, ImmutableMap.of());

        projectImportService.validateDoMapping(jiraServiceContext, new ProjectImportOptionsImpl(tempFile.getAbsolutePath(), null), backupProject, getBackupSystemInformationImpl());
        assertThat(jiraServiceContext.getErrorCollection(), isEmpty());
        //assertFalse(jiraServiceContext.getErrorCollection().hasAnyErrors());
    }

    @Test
    public void testValidateDoMappingNullBackupSystemInformation() throws IOException {
        final PermissionManager mockPermissionManager = getPermissionManagerWithSysAdminAccess();

        DefaultProjectImportService projectImportService = new DefaultProjectImportService(mockPermissionManager, null, null, null, null, null, null, null, null, null, null, upgradeConstraints, eventPublisher, projectTypeValidator);

        final File tempFile = createTempBackup();
        tempFile.deleteOnExit();
        JiraServiceContext jiraServiceContext = getContext();
        final BackupProject backupProject = new BackupProjectImpl(new ExternalProject(), emptyList(), emptyList(), emptyList(), emptyList(), 0, ImmutableMap.of());

        expectedException.expect(IllegalArgumentException.class);
        projectImportService.validateDoMapping(jiraServiceContext, new ProjectImportOptionsImpl(tempFile.getAbsolutePath(), null), backupProject, null);
    }

    @Test
    public void testValidateDoMappingNullProjImportOptions() throws IOException {
        DefaultProjectImportService projectImportService = new DefaultProjectImportService(null, null, null, null, null, null, null, null, null, null, null, upgradeConstraints, eventPublisher, projectTypeValidator);

        JiraServiceContext jiraServiceContext = getContext();
        final BackupProject backupProject = new BackupProjectImpl(new ExternalProject(), Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), 0, ImmutableMap.of());

        expectedException.expect(IllegalArgumentException.class);
        projectImportService.validateDoMapping(jiraServiceContext, null, backupProject, getBackupSystemInformationImpl());
    }

    private BackupSystemInformationImpl getBackupSystemInformationImpl() {
        return new BackupSystemInformationImpl("123", "Pro", Collections.emptyList(), true, Collections.emptyMap(), emptyEntitySet, 0);
    }

    @Test
    public void testValidateDoMappingNullImportPath() throws IOException {
        PermissionManager mockPermissionManager = getPermissionManagerWithSysAdminAccess();

        DefaultProjectImportService projectImportService = new DefaultProjectImportService(mockPermissionManager, null, null, null, null, null, null, null, null, null, null, upgradeConstraints, eventPublisher, projectTypeValidator);

        JiraServiceContext jiraServiceContext = getContext();
        final BackupProject backupProject = new BackupProjectImpl(new ExternalProject(), Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), 0, ImmutableMap.of());

        projectImportService.validateDoMapping(jiraServiceContext, new ProjectImportOptionsImpl(null, null), backupProject, getBackupSystemInformationImpl());

        assertThat(jiraServiceContext.getErrorCollection(), containsSystemError("You must provide a path to the JIRA backup zip file."));
    }

    @Test
    public void testValidateDoMappingInvalidImportFilePath() throws IOException {
        PermissionManager mockPermissionManager = getPermissionManagerWithSysAdminAccess();

        DefaultProjectImportService projectImportService = new DefaultProjectImportService(mockPermissionManager, null, null, null, null, null, null, null, null, null, null, upgradeConstraints, eventPublisher, projectTypeValidator);

        JiraServiceContext jiraServiceContext = getContext();
        final BackupProject backupProject = new BackupProjectImpl(new ExternalProject(), Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), 0, ImmutableMap.of());

        projectImportService.validateDoMapping(jiraServiceContext, new ProjectImportOptionsImpl("/SomeStupidPath/Whatever.xml", null), backupProject, getBackupSystemInformationImpl());
        assertThat(jiraServiceContext.getErrorCollection(), containsSystemError("The path to the JIRA backup zip file is not valid."));
    }

    @Test
    public void testValidateDoMappingNullBackupProject() throws IOException {
        PermissionManager mockPermissionManager = getPermissionManagerWithSysAdminAccess();

        DefaultProjectImportService projectImportService = new DefaultProjectImportService(mockPermissionManager, null, null, null, null, null, null, null, null, null, null, upgradeConstraints, eventPublisher, projectTypeValidator);

        JiraServiceContext jiraServiceContext = getContext();
        final File tempFile = createTempBackup();
        tempFile.deleteOnExit();

        projectImportService.validateDoMapping(jiraServiceContext, new ProjectImportOptionsImpl(tempFile.getAbsolutePath(), null), null, getBackupSystemInformationImpl());
        assertThat(jiraServiceContext.getErrorCollection(), containsSystemError("You must provide a backup project to import."));
    }

    @Test
    public void testValidateDoMappingNoPermission() throws IOException {
        PermissionManager mockPermissionManager = getPermissionManagerWithoutSysAdminAccess();

        DefaultProjectImportService projectImportService = new DefaultProjectImportService(mockPermissionManager, null, null, null, null, null, null, null, null, null, null, upgradeConstraints, eventPublisher, projectTypeValidator);

        final File tempFile = createTempBackup();
        tempFile.deleteOnExit();
        JiraServiceContext jiraServiceContext = getContext();
        final BackupProject backupProject = new BackupProjectImpl(new ExternalProject(), Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), 0, ImmutableMap.of());

        projectImportService.validateDoMapping(jiraServiceContext, new ProjectImportOptionsImpl(tempFile.getAbsolutePath(), null), backupProject, getBackupSystemInformationImpl());
        assertThat(jiraServiceContext.getErrorCollection(), containsSystemError("You must be a JIRA System Administrator to perform a project import."));
    }

    @Test
    public void testDoMappingNoPermission() throws IOException {
        final PermissionManager mockPermissionManager = getPermissionManagerWithoutSysAdminAccess();

        DefaultProjectImportService projectImportService = new DefaultProjectImportService(mockPermissionManager, null, null, null, null, null, null, null, null, null, null, upgradeConstraints, eventPublisher, projectTypeValidator);

        final File tempFile = createTempBackup();
        tempFile.deleteOnExit();
        JiraServiceContext jiraServiceContext = getContext();
        final BackupProject backupProject = new BackupProjectImpl(new ExternalProject(), emptyList(), emptyList(), emptyList(), emptyList(), 0, ImmutableMap.of());

        ProjectImportTemporaryFiles temporaryFiles = new MockProjectImportTemporaryFiles("TST");
        ProjectImportData projectImportData = new ProjectImportDataImpl(projectImportMapper, temporaryFiles, aoImportTemporaryFiles, 0, 0, 0, 0, 0, 0);
        projectImportService.doMapping(jiraServiceContext, new ProjectImportOptionsImpl(tempFile.getAbsolutePath(), null), projectImportData, backupProject, getBackupSystemInformationImpl(), null);

        assertThat(
                jiraServiceContext.getErrorCollection(),
                containsSystemError("You must be a JIRA System Administrator to perform a project import."));
    }

    @Test
    public void testDoMappingNullProjectImportOption() throws Exception {
        DefaultProjectImportService projectImportService = new DefaultProjectImportService(null, null, null, null, null, null, null, null, null, null, null, upgradeConstraints, eventPublisher, projectTypeValidator);
        JiraServiceContext jiraServiceContext = getContext();
        final BackupProject backupProject = new BackupProjectImpl(new ExternalProject(), Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), 0, ImmutableMap.of());
        ProjectImportData projectImportData = new ProjectImportDataImpl(projectImportMapper, null, aoImportTemporaryFiles, 0, 0, 0, 0, 0, 0);

        expectedException.expect(IllegalArgumentException.class);
        projectImportService.doMapping(jiraServiceContext, null, projectImportData, backupProject, getBackupSystemInformationImpl(), null);
    }

    @Test
    public void testDoMappingNullBackupProject() throws Exception {
        DefaultProjectImportService projectImportService = new DefaultProjectImportService(null, null, null, null, null, null, null, null, null, null, null, upgradeConstraints, eventPublisher, projectTypeValidator);
        JiraServiceContext jiraServiceContext = getContext();
        final ProjectImportOptionsImpl projectImportOptions = new ProjectImportOptionsImpl("/some/path", "/attach/path");
        ProjectImportData projectImportData = new ProjectImportDataImpl(projectImportMapper, null, aoImportTemporaryFiles, 0, 0, 0, 0, 0, 0);

        expectedException.expect(IllegalArgumentException.class);
        projectImportService.doMapping(jiraServiceContext, projectImportOptions, projectImportData, null, getBackupSystemInformationImpl(), null);
    }

    @Test
    public void testDoMappingNullBackupSystemInformation() throws Exception {
        DefaultProjectImportService projectImportService = new DefaultProjectImportService(null, null, null, null, null, null, null, null, null, null, null, upgradeConstraints, eventPublisher, projectTypeValidator);
        JiraServiceContext jiraServiceContext = getContext();
        ProjectImportData projectImportData = new ProjectImportDataImpl(projectImportMapper, null, aoImportTemporaryFiles, 0, 0, 0, 0, 0, 0);
        final BackupProject backupProject = new BackupProjectImpl(new ExternalProject(), Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), 0, ImmutableMap.of());

        expectedException.expect(IllegalArgumentException.class);
        projectImportService.doMapping(jiraServiceContext, new ProjectImportOptionsImpl(null, null), projectImportData, backupProject, null, null);
    }

    @Test
    public void testDoMappingIOException() throws IOException, SAXException {
        final PermissionManager mockPermissionManager = getPermissionManagerWithSysAdminAccess();
        // Mock out the manager
        final ProjectImportManager mockProjectImportManager = mock(ProjectImportManager.class);
        when(mockProjectImportManager.getProjectImportData(any(), any(), any(), any())).thenThrow(new IOException("File exists - not!"));

        DefaultProjectImportService projectImportService = new DefaultProjectImportService(mockPermissionManager, mockProjectImportManager, null, null, null, null, null, null, null, null, null, upgradeConstraints, eventPublisher, projectTypeValidator);

        JiraServiceContext jiraServiceContext = getContext();

        ExternalProject externalProject = new ExternalProject();
        externalProject.setKey("TST");
        BackupProject backupProject = new BackupProjectImpl(externalProject, emptyList(), emptyList(), emptyList(), emptyList(), 0, ImmutableMap.of());
        projectImportService.getProjectImportData(jiraServiceContext, new ProjectImportOptionsImpl("/ged/njk.xml", null), backupProject, getBackupSystemInformationImpl(), null);
        assertThat(
                jiraServiceContext.getErrorCollection(),
                containsSystemError("There was a problem accessing the backup XML file at /ged/njk.xml."));
    }

    @Test
    public void testDoMappingSAXException() throws IOException, SAXException {
        final PermissionManager mockPermissionManager = getPermissionManagerWithSysAdminAccess();

        // Mock out the manager
        final ProjectImportManager mockProjectImportManager = mock(ProjectImportManager.class);
        when(mockProjectImportManager.getProjectImportData(any(), any(), any(), any())).thenThrow(new SAXException("Parse Exception."));

        DefaultProjectImportService projectImportService = new DefaultProjectImportService(mockPermissionManager, mockProjectImportManager, null, null, null, null, null, null, null, null, null, upgradeConstraints, eventPublisher, projectTypeValidator);

        JiraServiceContext jiraServiceContext = getContext();

        ExternalProject externalProject = new ExternalProject();
        externalProject.setKey("TST");
        BackupProject backupProject = new BackupProjectImpl(externalProject, emptyList(), emptyList(), emptyList(), emptyList(), 0, ImmutableMap.of());
        projectImportService.getProjectImportData(jiraServiceContext, new ProjectImportOptionsImpl("/ged/njk.xml", null), backupProject, getBackupSystemInformationImpl(), null);
        assertThat(
                jiraServiceContext.getErrorCollection(),
                containsSystemError("There was a problem parsing the backup XML file at /ged/njk.xml: Parse Exception."));
    }

    @Test
    public void testGetProjectImportNoPermission() throws IOException {
        final PermissionManager mockPermissionManager = getPermissionManagerWithoutSysAdminAccess();

        DefaultProjectImportService projectImportService = new DefaultProjectImportService(mockPermissionManager, null, null, null, null, null, null, null, null, null, null, upgradeConstraints, eventPublisher, projectTypeValidator);

        JiraServiceContext jiraServiceContext = getContext();
        final BackupProject backupProject = new BackupProjectImpl(new ExternalProject(), emptyList(), emptyList(), emptyList(), emptyList(), 0, ImmutableMap.of());

        projectImportService.getProjectImportData(jiraServiceContext, new ProjectImportOptionsImpl("/blah", null), backupProject, getBackupSystemInformationImpl(), null);
        assertThat(
                jiraServiceContext.getErrorCollection(),
                containsSystemError("You must be a JIRA System Administrator to perform a project import."));
    }

    @Test
    public void testGetProjectImportDataNullBackupProject() throws Exception {
        DefaultProjectImportService projectImportService = new DefaultProjectImportService(null, null, null, null, null, null, null, null, null, null, null, upgradeConstraints, eventPublisher, projectTypeValidator);

        JiraServiceContext serviceContext = getContext();

        final ProjectImportOptionsImpl projectImportOptions = new ProjectImportOptionsImpl("/some/path", "/attach/path");
        final BackupSystemInformationImpl backupSystemInformation = getBackupSystemInformationImpl();

        expectedException.expect(IllegalArgumentException.class);
        projectImportService.getProjectImportData(serviceContext, projectImportOptions, null, backupSystemInformation, null);
    }

    @Test
    public void testGetProjectImportDataNullProjectImportOptions() throws Exception {
        DefaultProjectImportService projectImportService = new DefaultProjectImportService(null, null, null, null, null, null, null, null, null, null, null, upgradeConstraints, eventPublisher, projectTypeValidator);

        JiraServiceContext serviceContext = getContext();

        ExternalProject externalProject = new ExternalProject();
        externalProject.setKey("TST");
        BackupProject backupProject = new BackupProjectImpl(externalProject, Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), 0, ImmutableMap.of());
        final BackupSystemInformationImpl backupSystemInformation = getBackupSystemInformationImpl();

        expectedException.expect(IllegalArgumentException.class);
        projectImportService.getProjectImportData(serviceContext, null, backupProject, backupSystemInformation, null);
    }

    @Test
    public void testGetProjectImportDataNullBackupSystemInformation() throws Exception {
        DefaultProjectImportService projectImportService = new DefaultProjectImportService(null, null, null, null, null, null, null, null, null, null, null, upgradeConstraints, eventPublisher, projectTypeValidator);

        JiraServiceContext serviceContext = getContext();

        ExternalProject externalProject = new ExternalProject();
        externalProject.setKey("TST");
        BackupProject backupProject = new BackupProjectImpl(externalProject, Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), 0, ImmutableMap.of());
        final ProjectImportOptionsImpl projectImportOptions = new ProjectImportOptionsImpl("/some/path", "/attach/path");

        expectedException.expect(IllegalArgumentException.class);
        projectImportService.getProjectImportData(serviceContext, projectImportOptions, backupProject, null, null);
    }

    @Test
    public void testGetProjectImportDataHappyPath() throws Exception {
        ExternalProject externalProject = new ExternalProject();
        externalProject.setKey("TST");
        BackupProject backupProject = new BackupProjectImpl(externalProject, emptyList(), emptyList(), emptyList(), emptyList(), 0, ImmutableMap.of());

        ProjectImportManager mockProjectImportManager = mock(ProjectImportManager.class);

        final ProjectImportOptionsImpl projectImportOptions = new ProjectImportOptionsImpl("/some/path", "/attach/path");
        final ProjectImportDataImpl projectImportData = new ProjectImportDataImpl(projectImportMapper, null, aoImportTemporaryFiles, 0, 0, 0, 0, 0, 0);

        final BackupSystemInformationImpl backupSystemInformation = getBackupSystemInformationImpl();
        when(mockProjectImportManager.getProjectImportData(projectImportOptions, backupProject, backupSystemInformation, null)).thenReturn(projectImportData);
        final PermissionManager mockPermissionManager = getPermissionManagerWithSysAdminAccess();
        DefaultProjectImportService projectImportService = new DefaultProjectImportService(mockPermissionManager, mockProjectImportManager, null, null, null, null, null, null, null, null, null, upgradeConstraints, eventPublisher, projectTypeValidator);
        JiraServiceContext serviceContext = getContext();

        final ProjectImportData result = projectImportService.getProjectImportData(serviceContext, projectImportOptions, backupProject, backupSystemInformation, null);

        assertEquals(projectImportData, result);
    }

    @Test
    public void testDoMappingHappyPath() throws IOException, SAXException {
        ExternalProject externalProject = new ExternalProject();
        externalProject.setKey("TST");
        BackupProject backupProject = new BackupProjectImpl(externalProject, emptyList(), emptyList(), emptyList(), emptyList(), 0, ImmutableMap.of());

        final ProjectImportManager mockProjectImportManager = mock(ProjectImportManager.class);
        InOrder inOrder = Mockito.inOrder(mockProjectImportManager);

        final ProjectImportOptionsImpl projectImportOptions = new ProjectImportOptionsImpl("/some/path", "/attach/path");
        final ProjectImportDataImpl projectImportData = new ProjectImportDataImpl(projectImportMapper, null, aoImportTemporaryFiles, 0, 0, 0, 0, 0, 0) {
            @Override
            public String getPathToEntityXml(final String entityName) {
                return null;
            }
        };
        final MappingResult initialMappingResult = new MappingResult();

        final BackupSystemInformationImpl backupSystemInformation = getBackupSystemInformationImpl();

        initialMappingResult.setIssueTypeMessageSet(new MessageSetImpl());
        initialMappingResult.setCustomFieldMessageSet(new MessageSetImpl());
        initialMappingResult.setPriorityMessageSet(new MessageSetImpl());
        initialMappingResult.setStatusMessageSet(new MessageSetImpl());
        initialMappingResult.setResolutionMessageSet(new MessageSetImpl());
        initialMappingResult.setGroupMessageSet(new MessageSetImpl());
        initialMappingResult.setIssueLinkTypeMessageSet(new MessageSetImpl());
        initialMappingResult.setUserMessageSet(new MessageSetImpl());
        initialMappingResult.setProjectRoleMessageSet(new MessageSetImpl());
        initialMappingResult.setProjectRoleActorMessageSet(new MessageSetImpl());
        initialMappingResult.setFileAttachmentMessageSet(new MessageSetImpl());
        initialMappingResult.setIssueSecurityLevelMessageSet(new MessageSetImpl());
        initialMappingResult.setTextFieldLengthExceedingLimitMessageSet(new MessageSetImpl());

        final PermissionManager mockPermissionManager = getPermissionManagerWithSysAdminAccess();

        DefaultProjectImportService projectImportService = new DefaultProjectImportService(mockPermissionManager, mockProjectImportManager, null, null, null, null, null, null, null, null, null, upgradeConstraints, eventPublisher, projectTypeValidator) {
            MappingResult buildMappingResult() {
                return initialMappingResult;
            }
        };

        JiraServiceContext serviceContext = getContext();

        final MappingResult mappingResult = projectImportService.doMapping(serviceContext, projectImportOptions, projectImportData, backupProject, backupSystemInformation, null);

        assertTrue(mappingResult.canImport());

        inOrder.verify(mockProjectImportManager).autoMapAndValidateIssueTypes(Mockito.eq(projectImportData), refEq(initialMappingResult), eq(backupProject), refEq(serviceContext.getI18nBean()));
        inOrder.verify(mockProjectImportManager).autoMapAndValidateCustomFields(eq(projectImportData), refEq(initialMappingResult), eq(backupProject), refEq(serviceContext.getI18nBean()));
        inOrder.verify(mockProjectImportManager).autoMapSystemFields(eq(projectImportData), eq(backupProject));
        inOrder.verify(mockProjectImportManager).autoMapProjectRoles(eq(projectImportData));
        inOrder.verify(mockProjectImportManager).autoMapCustomFieldOptions(eq(projectImportData), eq(backupProject));
        inOrder.verify(mockProjectImportManager).validateCustomFieldValues(eq(projectImportData), refEq(initialMappingResult), eq(backupProject), Mockito.isNull(TaskProgressProcessor.class), refEq(serviceContext.getI18nBean()));
        inOrder.verify(mockProjectImportManager).validateSystemFields(eq(projectImportData), refEq(initialMappingResult), eq(projectImportOptions), eq(backupProject), Mockito.isNull(TaskProgressInterval.class), refEq(serviceContext.getI18nBean()));
        inOrder.verify(mockProjectImportManager).validateFileAttachments(eq(projectImportOptions), eq(projectImportData), refEq(initialMappingResult), eq(backupProject), eq(backupSystemInformation), Mockito.isNull(TaskProgressProcessor.class), refEq(serviceContext.getI18nBean()));
    }

    @Test
    public void testDoMappingIssueTypeInvalid() throws IOException, SAXException {
        ExternalProject externalProject = new ExternalProject();
        externalProject.setKey("TST");
        BackupProject backupProject = new BackupProjectImpl(externalProject, emptyList(), emptyList(), emptyList(), emptyList(), 0, ImmutableMap.of());

        final ProjectImportManager mockProjectImportManager = mock(ProjectImportManager.class);

        final ProjectImportDataImpl importData = new ProjectImportDataImpl(projectImportMapper, null, aoImportTemporaryFiles, 0, 0, 0, 0, 0, 0);
        final MappingResult initialMappingResult = new MappingResult();

        final ProjectImportOptionsImpl projectImportOptions = new ProjectImportOptionsImpl("/some/path", null);
        final BackupSystemInformationImpl backupSystemImpl = getBackupSystemInformationImpl();


        MessageSet issueTypeMessageSet = new MessageSetImpl();
        issueTypeMessageSet.addErrorMessage("I am an error");
        initialMappingResult.setIssueTypeMessageSet(issueTypeMessageSet);

        final PermissionManager mockPermissionManager = getPermissionManagerWithSysAdminAccess();
        DefaultProjectImportService projectImportService = new DefaultProjectImportService(mockPermissionManager, mockProjectImportManager, null, null, null, null, null, null, null, null, null, upgradeConstraints, eventPublisher, projectTypeValidator) {
            MappingResult buildMappingResult() {
                return initialMappingResult;
            }
        };

        JiraServiceContext serviceContext = getContext();
        final MappingResult mappingResult = projectImportService.doMapping(serviceContext, projectImportOptions, importData, backupProject, backupSystemImpl, null);

        assertThat(mappingResult.getIssueTypeMessageSet().getErrorMessages(), not(empty()));
        assertThat(serviceContext.getErrorCollection(), containsSystemError("The data mappings have produced errors, you can not import this project until all errors have been resolved. See below for details."));

        verify(mockProjectImportManager).autoMapAndValidateIssueTypes(eq(importData), refEq(initialMappingResult), eq(backupProject), refEq(serviceContext.getI18nBean()));
    }

    @Test
    public void testDoMappingCustomFieldsInvalid() throws IOException, SAXException {
        ExternalProject externalProject = new ExternalProject();
        externalProject.setKey("TST");
        BackupProject backupProject = new BackupProjectImpl(externalProject, emptyList(), emptyList(), emptyList(), emptyList(), 0, ImmutableMap.of());

        final ProjectImportManager mockProjectImportManager = mock(ProjectImportManager.class);
        InOrder inOrder = Mockito.inOrder(mockProjectImportManager);

        final ProjectImportDataImpl projectImportData = new ProjectImportDataImpl(projectImportMapper, null, aoImportTemporaryFiles, 0, 0, 0, 0, 0, 0);
        final MappingResult initialMappingResult = new MappingResult();

        final ProjectImportOptionsImpl projectImportOptions = new ProjectImportOptionsImpl("/some/path", null);
        final BackupSystemInformationImpl backupSystemImpl = getBackupSystemInformationImpl();

        initialMappingResult.setIssueTypeMessageSet(new MessageSetImpl());
        initialMappingResult.setCustomFieldMessageSet(new MessageSetImpl());
        MessageSet customFieldMessageSet = new MessageSetImpl();
        customFieldMessageSet.addErrorMessage("I am an error");
        initialMappingResult.setCustomFieldMessageSet(customFieldMessageSet);

        final PermissionManager mockPermissionManager = getPermissionManagerWithSysAdminAccess();

        DefaultProjectImportService projectImportService = new DefaultProjectImportService(mockPermissionManager, mockProjectImportManager, null, null, null, null, null, null, null, null, null, upgradeConstraints, eventPublisher, projectTypeValidator) {
            MappingResult buildMappingResult() {
                return initialMappingResult;
            }
        };

        JiraServiceContext serviceContext = getContext();

        final MappingResult mappingResult = projectImportService.doMapping(serviceContext, projectImportOptions, projectImportData, backupProject, backupSystemImpl, null);

        assertThat(mappingResult.getCustomFieldMessageSet().getErrorMessages(), not(empty()));

        inOrder.verify(mockProjectImportManager).autoMapAndValidateIssueTypes(eq(projectImportData), refEq(initialMappingResult), eq(backupProject), refEq(serviceContext.getI18nBean()));
        inOrder.verify(mockProjectImportManager).autoMapAndValidateCustomFields(eq(projectImportData), refEq(initialMappingResult), eq(backupProject), refEq(serviceContext.getI18nBean()));
    }

    @Test
    public void testDoMappingCustomFieldsInvalidCheckMessageSetForValues() throws IOException, SAXException {
        ExternalProject externalProject = new ExternalProject();
        externalProject.setKey("TST");
        BackupProject backupProject = new BackupProjectImpl(externalProject, emptyList(), emptyList(), emptyList(), emptyList(), 0, ImmutableMap.of());

        final ProjectImportManager mockProjectImportManager = mock(ProjectImportManager.class);
        InOrder inOrder = Mockito.inOrder(mockProjectImportManager);

        final ProjectImportDataImpl importData = new ProjectImportDataImpl(projectImportMapper, null, aoImportTemporaryFiles, 0, 0, 0, 0, 0, 0);
        final MappingResult initialMappingResult = new MappingResult();

        final ProjectImportOptionsImpl projectImportOptions = new ProjectImportOptionsImpl("/some/path", null);
        final BackupSystemInformationImpl backupSystemImpl = getBackupSystemInformationImpl();

        initialMappingResult.setIssueTypeMessageSet(new MessageSetImpl());
        initialMappingResult.setCustomFieldMessageSet(new MessageSetImpl());
        MessageSet customFieldMessageSet = new MessageSetImpl();
        customFieldMessageSet.addErrorMessage("I am an error");
        initialMappingResult.setCustomFieldMessageSet(customFieldMessageSet);

        // We want to have some required custom fields so that they get added with null message sets
        importData.getProjectImportMapper().getCustomFieldMapper().flagValueAsRequired("23", "12");
        importData.getProjectImportMapper().getCustomFieldMapper().flagValueAsRequired("25", "12");

        final PermissionManager mockPermissionManager = getPermissionManagerWithSysAdminAccess();

        DefaultProjectImportService projectImportService = new DefaultProjectImportService(mockPermissionManager, mockProjectImportManager, null, null, null, null, null, null, null, null, null, upgradeConstraints, eventPublisher, projectTypeValidator) {
            MappingResult buildMappingResult() {
                return initialMappingResult;
            }
        };

        JiraServiceContext serviceContext = getContext();

        final MappingResult mappingResult = projectImportService.doMapping(serviceContext, projectImportOptions, importData, backupProject, backupSystemImpl, null);

        assertThat(mappingResult.getCustomFieldValueMessageSets(), allOf(
                hasEntry("23", null),
                hasEntry("25", null)
        ));
        assertThat(mappingResult.getCustomFieldValueMessageSets().keySet(), hasSize(2));

        assertThat(mappingResult.getCustomFieldMessageSet().getErrorMessages(), not(empty()));
        inOrder.verify(mockProjectImportManager).autoMapAndValidateIssueTypes(eq(importData), refEq(initialMappingResult), eq(backupProject), refEq(serviceContext.getI18nBean()));
        inOrder.verify(mockProjectImportManager).autoMapAndValidateCustomFields(eq(importData), refEq(initialMappingResult), eq(backupProject), refEq(serviceContext.getI18nBean()));

    }

    @Test
    public void testDoMappingCustomFieldsValuesInvalid() throws IOException, SAXException {
        ExternalProject externalProject = new ExternalProject();
        externalProject.setKey("TST");
        BackupProject backupProject = new BackupProjectImpl(externalProject, emptyList(), emptyList(), emptyList(), emptyList(), 0, ImmutableMap.of());

        final ProjectImportManager mockProjectImportManager = mock(ProjectImportManager.class);
        InOrder inOrder = Mockito.inOrder(mockProjectImportManager);

        final ProjectImportDataImpl projectImportData = new ProjectImportDataImpl(projectImportMapper, null, aoImportTemporaryFiles, 0, 0, 0, 0, 0, 0) {
            @Override
            public String getPathToEntityXml(final String entityName) {
                return null;
            }
        };
        final MappingResult initialMappingResult = new MappingResult();

        final ProjectImportOptionsImpl projectImportOptions = new ProjectImportOptionsImpl("/some/path", null);
        final BackupSystemInformationImpl backupSystemImpl = getBackupSystemInformationImpl();

        initialMappingResult.setIssueTypeMessageSet(new MessageSetImpl());
        initialMappingResult.setCustomFieldMessageSet(new MessageSetImpl());
        initialMappingResult.setPriorityMessageSet(new MessageSetImpl());
        initialMappingResult.setStatusMessageSet(new MessageSetImpl());
        initialMappingResult.setResolutionMessageSet(new MessageSetImpl());

        MessageSet customFieldValueMesageSet = new MessageSetImpl();
        customFieldValueMesageSet.addErrorMessage("I am an error");
        HashMap<String, MessageSet> customFieldValuesErrors = new HashMap<>();
        customFieldValuesErrors.put("customFieldName", customFieldValueMesageSet);
        initialMappingResult.setCustomFieldValueMessageSets(customFieldValuesErrors);

        final PermissionManager mockPermissionManager = getPermissionManagerWithSysAdminAccess();

        DefaultProjectImportService projectImportService = new DefaultProjectImportService(mockPermissionManager, mockProjectImportManager, null, null, null, null, null, null, null, null, null, upgradeConstraints, eventPublisher, projectTypeValidator) {
            MappingResult buildMappingResult() {
                return initialMappingResult;
            }
        };

        JiraServiceContext serviceContext = getContext();

        final MappingResult mappingResult = projectImportService.doMapping(serviceContext, projectImportOptions, projectImportData, backupProject, backupSystemImpl, null);

        assertThat(mappingResult.getCustomFieldValueMessageSets().entrySet(), not(empty()));

        inOrder.verify(mockProjectImportManager).autoMapAndValidateIssueTypes(eq(projectImportData), refEq(initialMappingResult), eq(backupProject), any());
        inOrder.verify(mockProjectImportManager).autoMapAndValidateCustomFields(eq(projectImportData), refEq(initialMappingResult), eq(backupProject), any());
        inOrder.verify(mockProjectImportManager).autoMapSystemFields(eq(projectImportData), eq(backupProject));
        inOrder.verify(mockProjectImportManager).autoMapProjectRoles(eq(projectImportData));
        inOrder.verify(mockProjectImportManager).autoMapCustomFieldOptions(eq(projectImportData), eq(backupProject));
        inOrder.verify(mockProjectImportManager).validateCustomFieldValues(eq(projectImportData), refEq(initialMappingResult), eq(backupProject), Mockito.isNull(TaskProgressProcessor.class), any());
        inOrder.verify(mockProjectImportManager).validateSystemFields(eq(projectImportData), refEq(initialMappingResult), eq(projectImportOptions), eq(backupProject), Mockito.isNull(TaskProgressInterval.class), any());
    }

    @Test
    public void testDoMappingCustomFieldsValuesSaxException() throws IOException, SAXException {
        ExternalProject externalProject = new ExternalProject();
        externalProject.setKey("TST");
        BackupProject backupProject = new BackupProjectImpl(externalProject, emptyList(), emptyList(), emptyList(), emptyList(), 0, ImmutableMap.of());

        final ProjectImportManager mockProjectImportManager = mock(ProjectImportManager.class);
        InOrder inOrder = Mockito.inOrder(mockProjectImportManager);

        ProjectImportTemporaryFiles temporaryFiles = new MockProjectImportTemporaryFiles("TST");
        final ProjectImportDataImpl projectImportData = new ProjectImportDataImpl(projectImportMapper, temporaryFiles, aoImportTemporaryFiles, 0, 0, 0, 0, 0, 0);
        final MappingResult initialMappingResult = new MappingResult();

        final ProjectImportOptionsImpl projectImportOptions = new ProjectImportOptionsImpl("/some/path", null);
        final BackupSystemInformationImpl backupSystemImpl = getBackupSystemInformationImpl();

        initialMappingResult.setIssueTypeMessageSet(new MessageSetImpl());
        initialMappingResult.setCustomFieldMessageSet(new MessageSetImpl());
        initialMappingResult.setPriorityMessageSet(new MessageSetImpl());
        initialMappingResult.setStatusMessageSet(new MessageSetImpl());
        initialMappingResult.setResolutionMessageSet(new MessageSetImpl());

        final String exceptionMessage = "I am an exception";
        Mockito.doThrow(new SAXException(exceptionMessage)).when(mockProjectImportManager).validateCustomFieldValues(any(), any(), any(), any(), any());

        final PermissionManager mockPermissionManager = getPermissionManagerWithSysAdminAccess();

        DefaultProjectImportService projectImportService = new DefaultProjectImportService(mockPermissionManager, mockProjectImportManager, null, null, null, null, null, null, null, null, null, upgradeConstraints, eventPublisher, projectTypeValidator) {
            MappingResult buildMappingResult() {
                return initialMappingResult;
            }
        };

        JiraServiceContext serviceContext = getContext();

        final MappingResult mappingResult = projectImportService.doMapping(serviceContext, projectImportOptions, projectImportData, backupProject, backupSystemImpl, null);

        assertNull(mappingResult);
        assertThat(serviceContext.getErrorCollection(), containsSystemError(
                String.format(
                        "There was a SAX parsing problem accessing the custom field value XML file at %s, %s.",
                        temporaryFiles.getEntityXmlFile("CustomFieldValue").getAbsolutePath(),
                        exceptionMessage)));

        inOrder.verify(mockProjectImportManager).autoMapAndValidateIssueTypes(eq(projectImportData), refEq(initialMappingResult), eq(backupProject), any());
        inOrder.verify(mockProjectImportManager).autoMapAndValidateCustomFields(eq(projectImportData), refEq(initialMappingResult), eq(backupProject), any());
        inOrder.verify(mockProjectImportManager).autoMapSystemFields(eq(projectImportData), eq(backupProject));
        inOrder.verify(mockProjectImportManager).autoMapProjectRoles(eq(projectImportData));
        inOrder.verify(mockProjectImportManager).autoMapCustomFieldOptions(eq(projectImportData), eq(backupProject));
        inOrder.verify(mockProjectImportManager).validateCustomFieldValues(eq(projectImportData), refEq(initialMappingResult), eq(backupProject), Mockito.isNull(TaskProgressProcessor.class), any());
    }

    @Test
    public void testDoMappingCustomFieldsValuesIOException() throws IOException, SAXException {
        ExternalProject externalProject = new ExternalProject();
        externalProject.setKey("TST");
        BackupProject backupProject = new BackupProjectImpl(externalProject, emptyList(), emptyList(), emptyList(), emptyList(), 0, ImmutableMap.of());

        final ProjectImportManager mockProjectImportManager = mock(ProjectImportManager.class);

        ProjectImportTemporaryFiles temporaryFiles = new MockProjectImportTemporaryFiles("TST");
        final ProjectImportDataImpl projectImportData = new ProjectImportDataImpl(projectImportMapper, temporaryFiles, aoImportTemporaryFiles, 0, 0, 0, 0, 0, 0);
        final MappingResult initialMappingResult = new MappingResult();

        final ProjectImportOptionsImpl projectImportOptions = new ProjectImportOptionsImpl("/some/path", null);
        final BackupSystemInformationImpl backupSystemImpl = getBackupSystemInformationImpl();

        initialMappingResult.setIssueTypeMessageSet(new MessageSetImpl());
        initialMappingResult.setCustomFieldMessageSet(new MessageSetImpl());
        initialMappingResult.setPriorityMessageSet(new MessageSetImpl());
        initialMappingResult.setStatusMessageSet(new MessageSetImpl());
        initialMappingResult.setResolutionMessageSet(new MessageSetImpl());

        final String exceptionMessage = "I am an exception";
        Mockito.doThrow(new IOException(exceptionMessage)).when(mockProjectImportManager).validateCustomFieldValues(any(), any(), any(), any(), any());

        final PermissionManager mockPermissionManager = getPermissionManagerWithSysAdminAccess();

        DefaultProjectImportService projectImportService = new DefaultProjectImportService(mockPermissionManager, mockProjectImportManager, null, null, null, null, null, null, null, null, null, upgradeConstraints, eventPublisher, projectTypeValidator) {
            MappingResult buildMappingResult() {
                return initialMappingResult;
            }
        };
        JiraServiceContext serviceContext = getContext();

        final MappingResult mappingResult = projectImportService.doMapping(serviceContext, projectImportOptions, projectImportData, backupProject, backupSystemImpl, null);

        assertNull(mappingResult);
        assertThat(serviceContext.getErrorCollection(), containsSystemError(
                String.format(
                        "There was a problem accessing the custom field value XML file at %s.",
                        temporaryFiles.getEntityXmlFile("CustomFieldValue").getAbsolutePath())));
    }

    @Test
    public void testDoMappingFileAttachmentsIOException() throws IOException, SAXException {
        ExternalProject externalProject = new ExternalProject();
        externalProject.setKey("TST");
        BackupProject backupProject = new BackupProjectImpl(externalProject, emptyList(), emptyList(), emptyList(), emptyList(), 0, ImmutableMap.of());

        final ProjectImportManager mockProjectImportManager = mock(ProjectImportManager.class);

        ProjectImportTemporaryFiles temporaryFiles = new MockProjectImportTemporaryFiles("TST");
        final ProjectImportDataImpl projectImportData = new ProjectImportDataImpl(projectImportMapper, temporaryFiles, aoImportTemporaryFiles, 0, 0, 0, 0, 0, 0);
        final MappingResult initialMappingResult = new MappingResult();

        final ProjectImportOptionsImpl projectImportOptions = new ProjectImportOptionsImpl("/some/path", "/attach/path");
        final BackupSystemInformationImpl backupSystemImpl = getBackupSystemInformationImpl();

        initialMappingResult.setIssueTypeMessageSet(new MessageSetImpl());
        initialMappingResult.setCustomFieldMessageSet(new MessageSetImpl());
        initialMappingResult.setPriorityMessageSet(new MessageSetImpl());
        initialMappingResult.setStatusMessageSet(new MessageSetImpl());
        initialMappingResult.setResolutionMessageSet(new MessageSetImpl());

        Mockito.doThrow(new IOException("I am an exception")).when(mockProjectImportManager).validateFileAttachments(any(), any(), any(), any(), any(), Mockito.isNull(TaskProgressProcessor.class), any());

        final PermissionManager mockPermissionManager = getPermissionManagerWithSysAdminAccess();

        DefaultProjectImportService projectImportService = new DefaultProjectImportService(mockPermissionManager, mockProjectImportManager, null, null, null, null, null, null, null, null, null, upgradeConstraints, eventPublisher, projectTypeValidator) {
            MappingResult buildMappingResult() {
                return initialMappingResult;
            }
        };

        JiraServiceContext serviceContext = getContext();

        final MappingResult mappingResult = projectImportService.doMapping(serviceContext, projectImportOptions, projectImportData, backupProject, backupSystemImpl, null);

        assertNull(mappingResult);
        assertThat(serviceContext.getErrorCollection(), containsSystemError(
                String.format(
                        "There was a problem accessing the file attachment XML file at %s.",
                        temporaryFiles.getEntityXmlFile("FileAttachment").getAbsolutePath())));
    }

    @Test
    public void testDoMappingFileAttachmentsSaxException() throws IOException, SAXException {
        ExternalProject externalProject = new ExternalProject();
        externalProject.setKey("TST");
        BackupProject backupProject = new BackupProjectImpl(externalProject, emptyList(), emptyList(), emptyList(), emptyList(), 0, ImmutableMap.of());

        final ProjectImportManager mockProjectImportManager = mock(ProjectImportManager.class);

        ProjectImportTemporaryFiles temporaryFiles = new MockProjectImportTemporaryFiles("TST");
        final ProjectImportDataImpl projectImportData = new ProjectImportDataImpl(projectImportMapper, temporaryFiles, aoImportTemporaryFiles, 0, 0, 0, 0, 0, 0);
        final MappingResult initialMappingResult = new MappingResult();

        final ProjectImportOptionsImpl projectImportOptions = new ProjectImportOptionsImpl("/some/path", "/attach/path");
        final BackupSystemInformationImpl backupSystemImpl = getBackupSystemInformationImpl();

        initialMappingResult.setIssueTypeMessageSet(new MessageSetImpl());
        initialMappingResult.setCustomFieldMessageSet(new MessageSetImpl());
        initialMappingResult.setPriorityMessageSet(new MessageSetImpl());
        initialMappingResult.setStatusMessageSet(new MessageSetImpl());
        initialMappingResult.setResolutionMessageSet(new MessageSetImpl());

        final String exceptionMessage = "I am an exception";
        doThrow(new SAXException(exceptionMessage)).when(mockProjectImportManager).validateFileAttachments(any(), any(), any(), any(), any(), any(), any());

        final PermissionManager mockPermissionManager = getPermissionManagerWithSysAdminAccess();

        DefaultProjectImportService projectImportService = new DefaultProjectImportService(mockPermissionManager, mockProjectImportManager, null, null, null, null, null, null, null, null, null, upgradeConstraints, eventPublisher, projectTypeValidator) {
            MappingResult buildMappingResult() {
                return initialMappingResult;
            }
        };

        JiraServiceContext serviceContext = getContext();

        final MappingResult mappingResult = projectImportService.doMapping(serviceContext, projectImportOptions, projectImportData, backupProject, backupSystemImpl, null);

        assertNull(mappingResult);
        assertThat(serviceContext.getErrorCollection(), containsSystemError(
                String.format(
                        "There was a SAX parsing problem accessing the custom field value XML file at %s, %s.",
                        temporaryFiles.getEntityXmlFile("FileAttachment").getAbsolutePath(),
                        exceptionMessage
                )));

        assertTrue(serviceContext.getErrorCollection().hasAnyErrors());
    }

    private PluginInformation getPluginInformation(final String version) {
        PluginInformation pluginInformation = new PluginInformation();
        pluginInformation.setVersion(version);
        return pluginInformation;
    }

    @Test
    public void testDoMappingNo() throws IOException, SAXException {
        ExternalProject externalProject = new ExternalProject();
        externalProject.setKey("TST");
        BackupProject backupProject = new BackupProjectImpl(externalProject, emptyList(), emptyList(), emptyList(), emptyList(), 0, ImmutableMap.of());

        final ProjectImportManager mockProjectImportManager = mock(ProjectImportManager.class);
        InOrder inOrder = Mockito.inOrder(mockProjectImportManager);

        final ProjectImportOptionsImpl projectImportOptions = new ProjectImportOptionsImpl("/some/path", null);
        final ProjectImportDataImpl projectImportData = new ProjectImportDataImpl(projectImportMapper, null, aoImportTemporaryFiles, 0, 0, 0, 0, 0, 0) {
            @Override
            public String getPathToEntityXml(final String entityName) {
                return null;
            }
        };
        final MappingResult initialMappingResult = new MappingResult();

        final BackupSystemInformationImpl backupSystemImpl = getBackupSystemInformationImpl();

        initialMappingResult.setIssueTypeMessageSet(new MessageSetImpl());
        initialMappingResult.setCustomFieldMessageSet(new MessageSetImpl());
        initialMappingResult.setPriorityMessageSet(new MessageSetImpl());
        initialMappingResult.setStatusMessageSet(new MessageSetImpl());
        initialMappingResult.setResolutionMessageSet(new MessageSetImpl());
        initialMappingResult.setGroupMessageSet(new MessageSetImpl());
        initialMappingResult.setIssueLinkTypeMessageSet(new MessageSetImpl());
        initialMappingResult.setUserMessageSet(new MessageSetImpl());
        initialMappingResult.setProjectRoleMessageSet(new MessageSetImpl());
        initialMappingResult.setProjectRoleActorMessageSet(new MessageSetImpl());
        initialMappingResult.setIssueSecurityLevelMessageSet(new MessageSetImpl());
        initialMappingResult.setTextFieldLengthExceedingLimitMessageSet(new MessageSetImpl());

        final PermissionManager mockPermissionManager = getPermissionManagerWithSysAdminAccess();

        DefaultProjectImportService projectImportService = new DefaultProjectImportService(mockPermissionManager, mockProjectImportManager, null, null, null, null, null, null, null, null, null, upgradeConstraints, eventPublisher, projectTypeValidator) {
            MappingResult buildMappingResult() {
                return initialMappingResult;
            }
        };

        JiraServiceContext serviceContext = getContext();

        final MappingResult mappingResult = projectImportService.doMapping(serviceContext, projectImportOptions, projectImportData, backupProject, backupSystemImpl, null);

        assertTrue(mappingResult.canImport());

        inOrder.verify(mockProjectImportManager).autoMapAndValidateIssueTypes(Mockito.eq(projectImportData), refEq(initialMappingResult), eq(backupProject), refEq(serviceContext.getI18nBean()));
        inOrder.verify(mockProjectImportManager).autoMapAndValidateCustomFields(eq(projectImportData), refEq(initialMappingResult), eq(backupProject), refEq(serviceContext.getI18nBean()));
        inOrder.verify(mockProjectImportManager).autoMapSystemFields(eq(projectImportData), eq(backupProject));
        inOrder.verify(mockProjectImportManager).autoMapProjectRoles(eq(projectImportData));
        inOrder.verify(mockProjectImportManager).autoMapCustomFieldOptions(eq(projectImportData), eq(backupProject));
        inOrder.verify(mockProjectImportManager).validateCustomFieldValues(eq(projectImportData), refEq(initialMappingResult), eq(backupProject), Mockito.isNull(TaskProgressProcessor.class), refEq(serviceContext.getI18nBean()));
        inOrder.verify(mockProjectImportManager).validateSystemFields(eq(projectImportData), refEq(initialMappingResult), eq(projectImportOptions), eq(backupProject), Mockito.isNull(TaskProgressInterval.class), refEq(serviceContext.getI18nBean()));
    }

    @Test
    public void testCreateValidationMessageListAttachmentsNotIncluded() throws IOException {
        DefaultProjectImportService defaultProjectImportService = new DefaultProjectImportService(null, null, null, null, null, null, null, null, null, null, null, upgradeConstraints, eventPublisher, projectTypeValidator);
        final ProjectImportDataImpl projectImportData = new ProjectImportDataImpl(projectImportMapper, null, aoImportTemporaryFiles, 0, 0, 0, 0, 0, 0);
        final MappingResult mappingResult = new MappingResult();
        // Make out we have validated Issue Types with no problems
        mappingResult.setIssueTypeMessageSet(new MessageSetImpl());
        // Make Custom Fields have errors and warnings
        final MessageSetImpl customFieldMessageSet = new MessageSetImpl();
        customFieldMessageSet.addErrorMessage("Custom Field 'knob' has green dots.");
        customFieldMessageSet.addWarningMessage("Eat hot lead cowbuy.");
        customFieldMessageSet.addWarningMessage("You Suck");
        mappingResult.setCustomFieldMessageSet(customFieldMessageSet);

        // Add some custom field values
        final MessageSetImpl fredMessageSet = new MessageSetImpl();
        fredMessageSet.addErrorMessage("Not happy Jan!");
        mappingResult.setCustomFieldValueMessageSets(MapBuilder.<String, MessageSet>build("12", fredMessageSet));
        projectImportData.getProjectImportMapper().getCustomFieldMapper().registerOldValue("12", "Fred");

        defaultProjectImportService.createValidationMessageList(mappingResult, projectImportData, new MockI18nBean());

        // Now assert the list:
        List<ValidationMessage> messages = mappingResult.getSystemFieldsMessageList();
        Iterator<ValidationMessage> iter = messages.iterator();
        // Issue Type
        ValidationMessage validationMessage = iter.next();
        assertEquals("Issue Type", validationMessage.getDisplayName());
        assertFalse(validationMessage.getMessageSet().hasAnyMessages());
        // Custom Field
        validationMessage = iter.next();
        assertEquals("Custom Field Configuration", validationMessage.getDisplayName());
        assertTrue(validationMessage.getMessageSet().hasAnyErrors());
        assertTrue(validationMessage.getMessageSet().hasAnyWarnings());
        // Status
        validationMessage = iter.next();
        assertEquals("Status", validationMessage.getDisplayName());
        assertFalse(validationMessage.isValidated());
        // Priority
        validationMessage = iter.next();
        assertEquals("Priority", validationMessage.getDisplayName());
        assertFalse(validationMessage.isValidated());
        // Resolution
        validationMessage = iter.next();
        assertEquals("Resolution", validationMessage.getDisplayName());
        assertFalse(validationMessage.isValidated());
        // Users
        validationMessage = iter.next();
        assertEquals("Users", validationMessage.getDisplayName());
        assertFalse(validationMessage.isValidated());
        // ProjectRole
        validationMessage = iter.next();
        assertEquals("Project Role", validationMessage.getDisplayName());
        assertFalse(validationMessage.isValidated());
        // ProjectRoleActor
        validationMessage = iter.next();
        assertEquals("Project Role Membership", validationMessage.getDisplayName());
        assertFalse(validationMessage.isValidated());
        // Group
        validationMessage = iter.next();
        assertEquals("Group", validationMessage.getDisplayName());
        assertFalse(validationMessage.isValidated());
        // Issue Link Type
        validationMessage = iter.next();
        assertEquals("Issue Link Type", validationMessage.getDisplayName());
        assertFalse(validationMessage.isValidated());
        // Issue Security Level
        validationMessage = iter.next();
        assertEquals("Issue Security Level", validationMessage.getDisplayName());
        assertFalse(validationMessage.isValidated());
        // File Attachments
        validationMessage = iter.next();
        assertEquals("Attachments", validationMessage.getDisplayName());
        assertFalse(validationMessage.isValidated());
        // System Text Field Length
        validationMessage = iter.next();
        assertEquals("Text fields size", validationMessage.getDisplayName());
        assertFalse(validationMessage.isValidated());
        // Assert end of list
        assertFalse(iter.hasNext());

        List<ValidationMessage> custMessage = mappingResult.getCustomFieldsMessageList();
        // Custom Field Values for Fred
        validationMessage = custMessage.iterator().next();
        assertEquals("Fred", validationMessage.getDisplayName());
        assertTrue(validationMessage.isValidated());
        assertEquals("Not happy Jan!", validationMessage.getMessageSet().getErrorMessages().iterator().next());
    }

    @Test
    public void testCreateValidationMessageList() throws Exception {
        DefaultProjectImportService defaultProjectImportService = new DefaultProjectImportService(null, null, null, null, null, null, null, null, null, null, null, upgradeConstraints, eventPublisher, projectTypeValidator);
        final ProjectImportDataImpl projectImportData = new ProjectImportDataImpl(projectImportMapper, null, aoImportTemporaryFiles, 0, 0, 0, 0, 0, 0);
        final MappingResult mappingResult = new MappingResult();
        // Make out we have validated Issue Types with no problems
        mappingResult.setIssueTypeMessageSet(new MessageSetImpl());
        // Make Custom Fields have errors and warnings
        final MessageSetImpl customFieldMessageSet = new MessageSetImpl();
        customFieldMessageSet.addErrorMessage("Custom Field 'knob' has green dots.");
        customFieldMessageSet.addWarningMessage("Eat hot lead cowbuy.");
        customFieldMessageSet.addWarningMessage("You Suck");
        mappingResult.setCustomFieldMessageSet(customFieldMessageSet);

        // Add some custom field values
        final MessageSetImpl fredMessageSet = new MessageSetImpl();
        fredMessageSet.addErrorMessage("Not happy Jan!");
        mappingResult.setCustomFieldValueMessageSets(MapBuilder.<String, MessageSet>build("12", fredMessageSet));
        projectImportData.getProjectImportMapper().getCustomFieldMapper().registerOldValue("12", "Fred");

        defaultProjectImportService.createValidationMessageList(mappingResult, projectImportData, new MockI18nBean());

        // Now assert the list:
        List<ValidationMessage> messages = mappingResult.getSystemFieldsMessageList();
        Iterator<ValidationMessage> iter = messages.iterator();
        // Issue Type
        ValidationMessage validationMessage = iter.next();
        assertEquals("Issue Type", validationMessage.getDisplayName());
        assertFalse(validationMessage.getMessageSet().hasAnyMessages());
        // Custom Field
        validationMessage = iter.next();
        assertEquals("Custom Field Configuration", validationMessage.getDisplayName());
        assertTrue(validationMessage.getMessageSet().hasAnyErrors());
        assertTrue(validationMessage.getMessageSet().hasAnyWarnings());
        // Status
        validationMessage = iter.next();
        assertEquals("Status", validationMessage.getDisplayName());
        assertFalse(validationMessage.isValidated());
        // Priority
        validationMessage = iter.next();
        assertEquals("Priority", validationMessage.getDisplayName());
        assertFalse(validationMessage.isValidated());
        // Resolution
        validationMessage = iter.next();
        assertEquals("Resolution", validationMessage.getDisplayName());
        assertFalse(validationMessage.isValidated());
        // Users
        validationMessage = iter.next();
        assertEquals("Users", validationMessage.getDisplayName());
        assertFalse(validationMessage.isValidated());
        // ProjectRole
        validationMessage = iter.next();
        assertEquals("Project Role", validationMessage.getDisplayName());
        assertFalse(validationMessage.isValidated());
        // ProjectRoleActor
        validationMessage = iter.next();
        assertEquals("Project Role Membership", validationMessage.getDisplayName());
        assertFalse(validationMessage.isValidated());
        // Group
        validationMessage = iter.next();
        assertEquals("Group", validationMessage.getDisplayName());
        assertFalse(validationMessage.isValidated());
        // Issue Link Type
        validationMessage = iter.next();
        assertEquals("Issue Link Type", validationMessage.getDisplayName());
        assertFalse(validationMessage.isValidated());
        // Issue Security Level
        validationMessage = iter.next();
        assertEquals("Issue Security Level", validationMessage.getDisplayName());
        assertFalse(validationMessage.isValidated());
        // File Attachments
        validationMessage = iter.next();
        assertEquals("Attachments", validationMessage.getDisplayName());
        assertFalse(validationMessage.isValidated());
        // System Text Field Length
        validationMessage = iter.next();
        assertEquals("Text fields size", validationMessage.getDisplayName());
        assertFalse(validationMessage.isValidated());

        List<ValidationMessage> custMessage = mappingResult.getCustomFieldsMessageList();
        // Custom Field Values for Fred
        validationMessage = custMessage.iterator().next();
        assertEquals("Fred", validationMessage.getDisplayName());
        assertTrue(validationMessage.isValidated());
        assertEquals("Not happy Jan!", validationMessage.getMessageSet().getErrorMessages().iterator().next());

        // Assert end of list
        assertFalse(iter.hasNext());
    }

    @Test
    public void testDoImportHappyPath() throws ExternalException, IndexException, IOException, SAXException {
        final MockJiraServiceContext mockJiraServiceContext = new MockJiraServiceContext();
        final ProjectImportResultsImpl projectImportResults = new ProjectImportResultsImpl(0, 0, 0, 0, mockJiraServiceContext.getI18nBean());

        ExternalProject externalProject = new ExternalProject();
        externalProject.setKey("TST");
        BackupProject backupProject = new BackupProjectImpl(externalProject, Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), 0, ImmutableMap.of());

        ProjectImportOptions projectImportOptions = new ProjectImportOptionsImpl("/path", "/attach/path");

        final ProjectImportManager mockProjectImportManager = mock(ProjectImportManager.class);
        InOrder inOrder = Mockito.inOrder(mockProjectImportManager);

        ProjectImportTemporaryFiles temporaryFiles = new MockProjectImportTemporaryFiles("TST");
        final ProjectImportDataImpl importData = new ProjectImportDataImpl(projectImportMapper, temporaryFiles, aoImportTemporaryFiles, 0, 0, 0, 0, 0, 0);
        final BackupSystemInformationImpl backupSystemInfo = getBackupSystemInformationImpl();


        final PermissionManager permissionManager = getPermissionManagerWithSysAdminAccess();
        final UserManager userManager = mock(UserManager.class);
        when(userManager.hasWritableDirectory()).thenReturn(true);

        DefaultProjectImportService defaultProjectImportService = new DefaultProjectImportService(permissionManager, mockProjectImportManager, null, userManager, null, null, null, null, null, null, null, upgradeConstraints, eventPublisher, projectTypeValidator) {
            ProjectImportResults getInitialImportResults(final ProjectImportData projectImportData, final I18nHelper i18n, final int usersToCreate) {
                return projectImportResults;
            }
        };

        defaultProjectImportService.doImport(mockJiraServiceContext, projectImportOptions, backupProject, backupSystemInfo, importData, null);

        assertTrue(projectImportResults.isImportCompleted());
        inOrder.verify(mockProjectImportManager).createMissingUsers(projectImportMapper.getUserMapper(), projectImportResults, null);
        inOrder.verify(mockProjectImportManager).importProject(projectImportOptions, projectImportMapper, backupProject, projectImportResults, null);
        inOrder.verify(mockProjectImportManager).doImport(projectImportOptions, importData, backupProject, backupSystemInfo, projectImportResults, null, mockJiraServiceContext.getI18nBean(), mockJiraServiceContext.getLoggedInUser());
    }

    @Test
    public void testDoImportNullProjectImportOption() throws Exception {
        ExternalProject externalProject = new ExternalProject();
        externalProject.setKey("TST");
        BackupProject backupProject = new BackupProjectImpl(externalProject, emptyList(), emptyList(), emptyList(), emptyList(), 0, ImmutableMap.of());
        DefaultProjectImportService defaultProjectImportService = new DefaultProjectImportService(null, null, null, null, null, null, null, null, null, null, null, upgradeConstraints, eventPublisher, projectTypeValidator);

        expectedException.expect(IllegalArgumentException.class);
        defaultProjectImportService.doImport(new MockJiraServiceContext(), null, backupProject, getBackupSystemInformationImpl(), new ProjectImportDataImpl(projectImportMapper, null, aoImportTemporaryFiles, 0, 0, 0, 0, 0, 0), null);
    }

    @Test
    public void testDoImportNullBackupSystemInformation() throws Exception {
        ExternalProject externalProject = new ExternalProject();
        externalProject.setKey("TST");
        BackupProject backupProject = new BackupProjectImpl(externalProject, Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), 0, ImmutableMap.of());
        DefaultProjectImportService defaultProjectImportService = new DefaultProjectImportService(null, null, null, null, null, null, null, null, null, null, null, upgradeConstraints, eventPublisher, projectTypeValidator);

        expectedException.expect(IllegalArgumentException.class);
        defaultProjectImportService.doImport(new MockJiraServiceContext(), new ProjectImportOptionsImpl("", ""), backupProject, null, new ProjectImportDataImpl(projectImportMapper, null, aoImportTemporaryFiles, 0, 0, 0, 0, 0, 0), null);
    }

    @Test
    public void testDoImportNoPermission() throws ExternalException, IndexException, IOException, SAXException {
        ExternalProject externalProject = new ExternalProject();
        externalProject.setKey("TST");
        BackupProject backupProject = new BackupProjectImpl(externalProject, Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), 0, ImmutableMap.of());

        final ProjectImportManager mockProjectImportManager = mock(ProjectImportManager.class);
        final PermissionManager permissionManager = getPermissionManagerWithoutSysAdminAccess();
        final UserManager userManager = mock(UserManager.class);
        when(userManager.hasWritableDirectory()).thenReturn(true);

        DefaultProjectImportService defaultProjectImportService = new DefaultProjectImportService(permissionManager, mockProjectImportManager, null, userManager, null, null, null, null, null, null, null, upgradeConstraints, eventPublisher, projectTypeValidator);

        JiraServiceContext jiraServiceContext = getContext();
        defaultProjectImportService.doImport(jiraServiceContext, new ProjectImportOptionsImpl(null, null), backupProject, getBackupSystemInformationImpl(), new ProjectImportDataImpl(projectImportMapper, null, aoImportTemporaryFiles, 0, 0, 0, 0, 0, 0), null);

        assertThat(
                jiraServiceContext.getErrorCollection(),
                containsSystemError("You must be a JIRA System Administrator to perform a project import."));
    }

    @Test
    public void testDoImportErrorCreatingProject() throws ExternalException, IndexException, IOException, SAXException {
        final MockJiraServiceContext jiraServiceContext = new MockJiraServiceContext();
        final ProjectImportResultsImpl projectImportResults = new ProjectImportResultsImpl(0, 0, 0, 0, jiraServiceContext.getI18nBean());

        ProjectImportOptionsImpl projectImportOptions = new ProjectImportOptionsImpl(null, null);

        ExternalProject externalProject = new ExternalProject();
        externalProject.setKey("TST");
        BackupProject backupProject = new BackupProjectImpl(externalProject, Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), 0, ImmutableMap.of());

        final ProjectImportManager mockProjectImportManager = mock(ProjectImportManager.class);
        Mockito.doThrow(new AbortImportException()).when(mockProjectImportManager).importProject(any(), any(), any(), any(), any());
        final PermissionManager permissionManager = getPermissionManagerWithSysAdminAccess();
        final UserManager userManager = mock(UserManager.class);
        when(userManager.hasWritableDirectory()).thenReturn(true);

        DefaultProjectImportService defaultProjectImportService = new DefaultProjectImportService(permissionManager, mockProjectImportManager, null, userManager, null, null, null, null, null, null, null, upgradeConstraints, eventPublisher, projectTypeValidator) {
            ProjectImportResults getInitialImportResults(final ProjectImportData projectImportData, final I18nHelper i18n, final int usersToCreate) {
                return projectImportResults;
            }
        };

        ProjectImportTemporaryFiles temporaryFiles = new MockProjectImportTemporaryFiles("TST");
        defaultProjectImportService.doImport(jiraServiceContext, projectImportOptions, backupProject, getBackupSystemInformationImpl(), new ProjectImportDataImpl(projectImportMapper, temporaryFiles, aoImportTemporaryFiles, 0, 0, 0, 0, 0, 0), null);

        assertThat(jiraServiceContext.getErrorCollection().getErrorMessages(), containsInAnyOrder(
                "There was a problem creating/updating the project and its details.",
                "The import was aborted because there were too many errors. Some errors are listed below. For full details about the errors please see your logs. Please note that some elements have been created. You may want to consult your logs to see what caused the errors, delete the project, and perform the import again."
        ));

        assertFalse(projectImportResults.isImportCompleted());
    }

    @Test
    public void testDoImportIOException() throws ExternalException, IndexException, IOException, SAXException {
        final MockJiraServiceContext jiraServiceContext = new MockJiraServiceContext();
        final ProjectImportResultsImpl projectImportResults = new ProjectImportResultsImpl(0, 0, 0, 0, jiraServiceContext.getI18nBean());
        ExternalProject externalProject = new ExternalProject();
        externalProject.setKey("TST");
        BackupProject backupProject = new BackupProjectImpl(externalProject, Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), 0, ImmutableMap.of());

        ProjectImportOptions projectImportOptions = new ProjectImportOptionsImpl("/path", "/attach/path");

        final ProjectImportManager mockProjectImportManager = mock(ProjectImportManager.class);

        final BackupSystemInformationImpl backupSystemInfo = getBackupSystemInformationImpl();
        ProjectImportTemporaryFiles temporaryFiles = new MockProjectImportTemporaryFiles("TST");
        final ProjectImportDataImpl projectImportData = new ProjectImportDataImpl(projectImportMapper, temporaryFiles, aoImportTemporaryFiles, 0, 0, 0, 0, 0, 0);

        doThrow(new IOException("Whoops")).when(mockProjectImportManager).doImport(any(), any(), any(), any(), any(), any(), any(), any());

        final PermissionManager permissionManager = getPermissionManagerWithSysAdminAccess();
        final UserManager userManager = mock(UserManager.class);
        when(userManager.hasWritableDirectory()).thenReturn(true);

        DefaultProjectImportService defaultProjectImportService = new DefaultProjectImportService(permissionManager, mockProjectImportManager, null, userManager, null, null, null, null, null, null, null, upgradeConstraints, eventPublisher, projectTypeValidator) {
            ProjectImportResults getInitialImportResults(final ProjectImportData projectImportData, final I18nHelper i18n, final int usersToCreate) {
                return projectImportResults;
            }
        };

        defaultProjectImportService.doImport(jiraServiceContext, projectImportOptions, backupProject, backupSystemInfo, projectImportData, null);

        assertThat(
                jiraServiceContext.getErrorCollection(),
                containsSystemError("There was a problem accessing the partitioned XML files: Whoops."));
        assertFalse(projectImportResults.isImportCompleted());
    }

    @Test
    public void testDoImportSAXException() throws ExternalException, IndexException, IOException, SAXException {
        MockJiraServiceContext jiraServiceContext = new MockJiraServiceContext();
        final ProjectImportResultsImpl projectImportResults = new ProjectImportResultsImpl(0, 0, 0, 0, jiraServiceContext.getI18nBean());
        ExternalProject externalProject = new ExternalProject();
        externalProject.setKey("TST");
        BackupProject backupProject = new BackupProjectImpl(externalProject, Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), 0, ImmutableMap.of());

        ProjectImportOptions projectImportOptions = new ProjectImportOptionsImpl("/path", "/attach/path");


        final ProjectImportManager mockProjectImportManager = mock(ProjectImportManager.class);
        final BackupSystemInformationImpl backupSystemInfo = getBackupSystemInformationImpl();
        ProjectImportTemporaryFiles temporaryFiles = new MockProjectImportTemporaryFiles
                ("TST");
        doThrow(new SAXException("Whoops")).when(mockProjectImportManager).doImport(any(), any(), any(), any(), any(), any(), any(), any());

        final PermissionManager permissionManager = getPermissionManagerWithSysAdminAccess();
        final UserManager userManager = mock(UserManager.class);
        when(userManager.hasWritableDirectory()).thenReturn(false);

        DefaultProjectImportService defaultProjectImportService = new DefaultProjectImportService(permissionManager, mockProjectImportManager, null, userManager, null, null, null, null, null, null, null, upgradeConstraints, eventPublisher, projectTypeValidator) {
            ProjectImportResults getInitialImportResults(final ProjectImportData projectImportData, final I18nHelper i18n, final int usersToCreate) {
                return projectImportResults;
            }
        };

        defaultProjectImportService.doImport(jiraServiceContext, projectImportOptions, backupProject, backupSystemInfo, new ProjectImportDataImpl(projectImportMapper, temporaryFiles, aoImportTemporaryFiles, 0, 0, 0, 0, 0, 0), null);

        assertThat(
                jiraServiceContext.getErrorCollection(),
                containsSystemError("There was a SAX parsing problem accessing the partitioned XML files: Whoops."));
        assertFalse(projectImportResults.isImportCompleted());
    }

    @Test
    public void testDoImportIndexException() throws ExternalException, IndexException, IOException, SAXException {
        MockJiraServiceContext jiraServiceContext = new MockJiraServiceContext();
        final ProjectImportResultsImpl projectImportResults = new ProjectImportResultsImpl(0, 0, 0, 0, jiraServiceContext.getI18nBean());
        ExternalProject externalProject = new ExternalProject();
        externalProject.setKey("TST");
        BackupProject backupProject = new BackupProjectImpl(externalProject, Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), 0, ImmutableMap.of());

        ProjectImportOptions projectImportOptions = new ProjectImportOptionsImpl("/path", "/attach/path");

        final ProjectImportManager mockProjectImportManager = mock(ProjectImportManager.class);

        final BackupSystemInformationImpl backupSystemInfo = getBackupSystemInformationImpl();
        ProjectImportTemporaryFiles temporaryFiles = new MockProjectImportTemporaryFiles("TST");
        doThrow(new IndexException("Whoops")).when(mockProjectImportManager).doImport(any(), any(), any(), any(), any(), any(), any(), any());

        final PermissionManager permissionManager = getPermissionManagerWithSysAdminAccess();
        final UserManager userManager = mock(UserManager.class);
        when(userManager.hasWritableDirectory()).thenReturn(false);

        DefaultProjectImportService defaultProjectImportService = new DefaultProjectImportService(permissionManager, mockProjectImportManager, null, userManager, null, null, null, null, null, null, null, upgradeConstraints, eventPublisher, projectTypeValidator) {
            ProjectImportResults getInitialImportResults(final ProjectImportData projectImportData, final I18nHelper i18n, final int usersToCreate) {
                return projectImportResults;
            }
        };

        defaultProjectImportService.doImport(jiraServiceContext, projectImportOptions, backupProject, backupSystemInfo, new ProjectImportDataImpl(projectImportMapper, temporaryFiles, aoImportTemporaryFiles, 0, 0, 0, 0, 0, 0), null);

        assertThat(
                jiraServiceContext.getErrorCollection(),
                containsSystemError("There was a problem reIndexing the newly imported project. Please manually perform a full system reindex: Whoops."));
        assertFalse(projectImportResults.isImportCompleted());
    }

    @Test
    public void testDoImportAbortImportxceptionFromDoImport()
            throws ExternalException, IndexException, IOException, SAXException {
        MockJiraServiceContext jiraServiceContext = new MockJiraServiceContext();
        final ProjectImportResultsImpl projectImportResults = new ProjectImportResultsImpl(0, 0, 0, 0, jiraServiceContext.getI18nBean());
        ExternalProject externalProject = new ExternalProject();
        externalProject.setKey("TST");
        BackupProject backupProject = new BackupProjectImpl(externalProject, emptyList(), emptyList(), emptyList(), emptyList(), 0, ImmutableMap.of());

        ProjectImportOptions projectImportOptions = new ProjectImportOptionsImpl("/path", "/attach/path");

        final ProjectImportManager mockProjectImportManager = mock(ProjectImportManager.class);

        final BackupSystemInformationImpl backupSystemInfo = getBackupSystemInformationImpl();
        ProjectImportTemporaryFiles temporaryFiles = new MockProjectImportTemporaryFiles("TST");
        doThrow(new AbortImportException()).when(mockProjectImportManager).doImport(any(), any(), any(), any(), any(), any(), any(), any());

        final PermissionManager permissionManager = getPermissionManagerWithSysAdminAccess();
        final UserManager userManager = mock(UserManager.class);
        when(userManager.hasWritableDirectory()).thenReturn(false);

        DefaultProjectImportService defaultProjectImportService = new DefaultProjectImportService(permissionManager, mockProjectImportManager, null, userManager, null, null, null, null, null, null, null, upgradeConstraints, eventPublisher, projectTypeValidator) {
            ProjectImportResults getInitialImportResults(final ProjectImportData projectImportData, final I18nHelper i18n, final int usersToCreate) {
                return projectImportResults;
            }
        };

        defaultProjectImportService.doImport(jiraServiceContext, projectImportOptions, backupProject, backupSystemInfo, new ProjectImportDataImpl(projectImportMapper, temporaryFiles, aoImportTemporaryFiles, 0, 0, 0, 0, 0, 0), null);

        assertFalse(projectImportResults.isImportCompleted());
        assertThat(
                jiraServiceContext.getErrorCollection(),
                containsSystemError("The import was aborted because there were too many errors. Some errors are listed below. For full details about the errors please see your logs. Please note that some elements have been created. You may want to consult your logs to see what caused the errors, delete the project, and perform the import again."));
    }

    @Test
    public void testDoImportAbortImportxceptionFromCreateUsers()
            throws ExternalException, IndexException, IOException, SAXException {
        MockJiraServiceContext jiraServiceContext = new MockJiraServiceContext();
        final ProjectImportResultsImpl projectImportResults = new ProjectImportResultsImpl(0, 0, 0, 0, jiraServiceContext.getI18nBean());
        ExternalProject externalProject = new ExternalProject();
        externalProject.setKey("TST");
        BackupProject backupProject = new BackupProjectImpl(externalProject, emptyList(), emptyList(), emptyList(), emptyList(), 0, ImmutableMap.of());

        ProjectImportOptions projectImportOptions = new ProjectImportOptionsImpl("/path", "/attach/path");

        final ProjectImportManager mockProjectImportManager = mock(ProjectImportManager.class);

        final BackupSystemInformationImpl backupSystemInfo = getBackupSystemInformationImpl();
        Mockito.doThrow(new AbortImportException()).when(mockProjectImportManager).createMissingUsers(any(), any(), any());

        final PermissionManager permissionManager = getPermissionManagerWithSysAdminAccess();
        final UserManager userManager = mock(UserManager.class);
        when(userManager.hasWritableDirectory()).thenReturn(true);

        DefaultProjectImportService defaultProjectImportService = new DefaultProjectImportService(permissionManager, mockProjectImportManager, null, userManager, null, null, null, null, null, null, null, upgradeConstraints, eventPublisher, projectTypeValidator) {
            ProjectImportResults getInitialImportResults(final ProjectImportData projectImportData, final I18nHelper i18n, final int usersToCreate) {
                return projectImportResults;
            }
        };

        ProjectImportTemporaryFiles temporaryFiles = new MockProjectImportTemporaryFiles("TST");
        defaultProjectImportService.doImport(jiraServiceContext, projectImportOptions, backupProject, backupSystemInfo, new ProjectImportDataImpl(projectImportMapper, temporaryFiles, aoImportTemporaryFiles, 0, 0, 0, 0, 0, 0), null);

        assertFalse(projectImportResults.isImportCompleted());
        assertThat(
                jiraServiceContext.getErrorCollection(),
                containsSystemError("The import was aborted because there were too many errors. Some errors are listed below. For full details about the errors please see your logs. Please note that some elements have been created. You may want to consult your logs to see what caused the errors, delete the project, and perform the import again."));
    }

    private File createTempBackup() throws IOException {
        final File zipFile = File.createTempFile("JIRABackup", "zip");
        final ZipArchiveOutputStream os = new ZipArchiveOutputStream(zipFile);
        os.putArchiveEntry(new ZipArchiveEntry("entities.xml"));
        os.closeArchiveEntry();
        os.putArchiveEntry(new ZipArchiveEntry("activeobjects.xml"));
        os.closeArchiveEntry();
        os.close();

        return zipFile;
    }

    @Nonnull
    private File createTempDirectory() throws IOException {
        File baseDir = new File(System.getProperty("java.io.tmpdir"));
        String name = "Test" + System.currentTimeMillis();
        File dir = new File(baseDir, name);
        dir.mkdir();
        return dir;
    }

    private PermissionManager getPermissionManagerWithSysAdminAccess() {
        PermissionManager mockPermissionManager = mock(PermissionManager.class);
        when(mockPermissionManager.hasPermission(Mockito.eq(Permissions.SYSTEM_ADMIN), Mockito.any())).thenReturn(true);
        return mockPermissionManager;
    }

    private PermissionManager getPermissionManagerWithoutSysAdminAccess() {
        PermissionManager mockPermissionManager = mock(PermissionManager.class);
        when(mockPermissionManager.hasPermission(Mockito.eq(Permissions.SYSTEM_ADMIN), Mockito.any())).thenReturn(false);
        return mockPermissionManager;
    }

    private JiraServiceContext getContext() {
        final MockI18nBean i18nBean = new MockI18nBean();
        JiraServiceContext jiraServiceContext = new JiraServiceContextImpl(mockUser, new SimpleErrorCollection()) {
            @Override
            public I18nHelper getI18nBean() {
                return i18nBean;
            }
        };
        return jiraServiceContext;
    }

    private JiraServiceContext getContextWithI18HelperReturningKey() {
        I18nHelper helper = mock(I18nHelper.class);
        when(helper.getText(Mockito.any())).then(invocation -> invocation.getArguments()[0]);

        JiraServiceContext jiraServiceContext = new JiraServiceContextImpl(mockUser, new SimpleErrorCollection()) {
            @Override
            public I18nHelper getI18nBean() {
                return helper;
            }
        };
        return jiraServiceContext;
    }

}
