package com.atlassian.jira.web.action.admin;

import com.atlassian.jira.application.JiraApplication;
import com.atlassian.jira.application.MockApplication;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.project.type.JiraApplicationAdapter;
import com.atlassian.jira.project.type.ProjectType;
import com.atlassian.jira.project.type.ProjectTypeKey;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.web.action.RedirectSanitiser;
import com.atlassian.jira.web.landingpage.LandingPageRedirectManager;
import com.atlassian.webresource.api.assembler.PageBuilderService;
import com.atlassian.webresource.api.assembler.RequiredResources;
import com.atlassian.webresource.api.assembler.WebResourceAssembler;
import com.google.common.collect.ImmutableList;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.mockito.Mock;

import java.util.Optional;

import static java.util.Collections.emptyList;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

public class TestLandingPage {
    @Rule
    public RuleChain mockitoMocksInContainer = MockitoMocksInContainer.forTest(this);

    @Mock
    private LandingPageRedirectManager landingPageRedirectManager;
    @Mock
    private PageBuilderService pageBuilderService;
    @Mock
    private JiraApplicationAdapter jiraApplicationAdapter;
    @Mock
    private WebResourceAssembler webResourceAssembler;
    @Mock
    private RequiredResources requiredResources;
    @Mock
    private ApplicationUser user;

    @AvailableInContainer
    @Mock
    private JiraAuthenticationContext jiraAuthenticationContext;

    @AvailableInContainer
    @Mock
    private RedirectSanitiser redirectSanitiser;

    private LandingPage landingPage;


    @Before
    public void setUp() throws Exception {
        landingPage = spy(new LandingPage(landingPageRedirectManager, pageBuilderService, jiraApplicationAdapter));

        when(pageBuilderService.assembler()).thenReturn(webResourceAssembler);
        when(webResourceAssembler.resources()).thenReturn(requiredResources);
        when(requiredResources.requireContext(anyString())).thenReturn(requiredResources);
        when(jiraAuthenticationContext.getUser()).thenReturn(user);
    }

    @Test
    public void doExecuteReturnsSuccess() throws Exception {
        when(landingPageRedirectManager.redirectUrl(user)).thenReturn(Optional.empty());

        assertThat(landingPage.doExecute(), equalTo("success"));
    }

    @Test
    public void doExecuteReturnsRedirectPage() throws Exception {
        when(landingPageRedirectManager.redirectUrl(user)).thenReturn(Optional.of("redirectUrl"));
        doReturn("redirect:redirectUrl").when(landingPage).getRedirect("redirectUrl");

        assertThat(landingPage.doExecute(), equalTo("redirect:redirectUrl"));
    }

    @Test
    public void handleSelectedProductWillUseDefaultsWhenThereIsNoProductSet() throws Exception {
        landingPage.handleSelectedProduct();

        assertThat(landingPage.getProductName(), equalTo("JIRA"));
        assertThat(landingPage.getProjectTypeKey(), equalTo(""));
    }

    @Test
    public void handleSelectedProductWillChooseCorrectProduct() throws Exception {
        landingPage.setProduct("test-product");
        when(jiraApplicationAdapter.getAccessibleJiraApplications()).thenReturn(
                ImmutableList.of(new JiraApplication(new MockApplication("test-product-other"),
                                ImmutableList.of(projectType("test.product.2"))),
                        new JiraApplication(new MockApplication("test-product"),
                                ImmutableList.of(projectType("test.product")))));

        landingPage.handleSelectedProduct();

        assertThat(landingPage.getProductName(), equalTo("test-product-name"));
        assertThat(landingPage.getProjectTypeKey(), equalTo("test.product"));
    }

    @Test
    public void handleSelectedProductWillChooseCorrectProductWithEmptyProjectTypesList() throws Exception {
        landingPage.setProduct("test-product");
        when(jiraApplicationAdapter.getAccessibleJiraApplications()).thenReturn(
                ImmutableList.of(new JiraApplication(new MockApplication("test-product"), emptyList())));

        landingPage.handleSelectedProduct();

        assertThat(landingPage.getProductName(), equalTo("test-product-name"));
        assertThat(landingPage.getProjectTypeKey(), equalTo(""));
    }

    @Test
    public void handleSelectedProductWillUseDefaultsForEmptyApplicationsList() throws Exception {
        landingPage.setProduct("test-product");
        when(jiraApplicationAdapter.getAccessibleJiraApplications()).thenReturn(emptyList());

        landingPage.handleSelectedProduct();

        assertThat(landingPage.getProductName(), equalTo("JIRA"));
        assertThat(landingPage.getProjectTypeKey(), equalTo(""));
    }

    @Test
    public void handleSelectedProductWillUseDefaultsForUnknownApplicationKey() throws Exception {
        landingPage.setProduct("test-product-unknown");
        when(jiraApplicationAdapter.getAccessibleJiraApplications()).thenReturn(
                ImmutableList.of(new JiraApplication(new MockApplication("test-product"), emptyList())));

        landingPage.handleSelectedProduct();

        assertThat(landingPage.getProductName(), equalTo("JIRA"));
        assertThat(landingPage.getProjectTypeKey(), equalTo(""));
    }


    @Test
    public void handleSelectedProductWillUseDefaultsForInvalidApplicationKey() throws Exception {
        landingPage.setProduct("123invalid");

        landingPage.handleSelectedProduct();

        assertThat(landingPage.getProductName(), equalTo("JIRA"));
        assertThat(landingPage.getProjectTypeKey(), equalTo(""));
    }

    private static ProjectType projectType(final String key) {
        return new ProjectType(new ProjectTypeKey(key), "", "", "", 0);
    }

}
