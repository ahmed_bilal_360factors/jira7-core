package com.atlassian.jira.tenancy;

import com.atlassian.fugue.Option;
import com.atlassian.jira.bc.dataimport.ImportCompletedEvent;
import com.atlassian.jira.bc.dataimport.ImportStartedEvent;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.util.ComponentLocator;
import com.atlassian.tenancy.api.Tenant;
import com.atlassian.tenancy.api.TenantContext;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.ServletContext;

import static com.atlassian.fugue.Option.none;
import static com.atlassian.jira.matchers.IterableMatchers.emptyIterable;
import static org.hamcrest.Matchers.contains;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestJiraTenantAccessor {
    @Rule
    public final RuleChain mocksInContainer = MockitoMocksInContainer.forTest(this);

    @Mock
    private TenantContext mockTenantContext;
    @Mock
    private ServletContext mockServletContext;
    @Mock
    private ComponentLocator mockComponentLocator;
    @Mock
    @AvailableInContainer
    private TenancyCondition mockTenancyCondition;
    @Mock
    @AvailableInContainer
    private ApplicationProperties applicationProperties;

    private Tenant tenant = new JiraTenantImpl("tenant");
    private DefaultJiraTenantAccessor tenantAccessor;

    @Before
    public void setupMocks() {
        when(mockComponentLocator.getComponent(TenancyCondition.class)).thenReturn(mockTenancyCondition);
        when(mockTenancyCondition.isEnabled()).thenReturn(true);
        tenantAccessor = new DefaultJiraTenantAccessor(mockTenantContext, mockComponentLocator);
    }

    @Test
    public void testAddTenant() throws Exception {
        tenantAccessor = new DefaultJiraTenantAccessor(mockTenantContext, mockComponentLocator);
        tenantAccessor.addTenant(tenant);
        assertThat(tenantAccessor.getAvailableTenants(), contains(tenant));
    }

    @Test
    public void testTenantIsNotAddedWhenApplicationPropertiesBlank() throws Exception {
        tenantAccessor = new DefaultJiraTenantAccessor(mockTenantContext, mockComponentLocator);
        assertThat(tenantAccessor.getAvailableTenants(), emptyIterable(Tenant.class));
    }

    @Test(expected = IllegalStateException.class)
    public void testIllegalStateExceptionIsThrownDuringImport() throws Exception {

        tenantAccessor.onImportStarted(new ImportStartedEvent(none()));
        tenantAccessor.addTenant(tenant);
    }

    @Test
    public void testAfterImport() throws Exception {
        tenantAccessor.onImportStarted(new ImportStartedEvent(none()));
        tenantAccessor.onImportCompleted(new ImportCompletedEvent(true, Option.<Long>none()));
        tenantAccessor.addTenant(tenant);
        assertThat(tenantAccessor.getAvailableTenants(), contains(tenant));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIllegalArgumentExceptionIsThrownWhenAlreadyTenanted() throws Exception {
        tenantAccessor.addTenant(tenant);
        tenantAccessor.addTenant(tenant);
    }

    @Test
    public void testAlwaysTenantedInBtf() throws Exception {
        when(mockTenancyCondition.isEnabled()).thenReturn(false);
        assertThat(tenantAccessor.getAvailableTenants(), contains(new JiraTenantImpl("system")));
    }
}
