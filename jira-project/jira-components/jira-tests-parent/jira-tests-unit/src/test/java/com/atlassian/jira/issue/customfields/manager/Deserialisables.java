package com.atlassian.jira.issue.customfields.manager;

import java.util.LinkedList;
import java.util.List;

/**
 * This is used for testing the deserialisation security for xstream.
 *
 * @since v7.1
 */
public class Deserialisables {
    public static class Alpha {
        public int a = 0;
        public List<String> myList = new LinkedList<>();

        public void Alpha() {
        }

        public void add(String s) {
            myList.add(s);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Alpha alpha = (Alpha) o;

            if (a != alpha.a) return false;
            return !(myList != null ? !myList.equals(alpha.myList) : alpha.myList != null);
        }
    }

}
