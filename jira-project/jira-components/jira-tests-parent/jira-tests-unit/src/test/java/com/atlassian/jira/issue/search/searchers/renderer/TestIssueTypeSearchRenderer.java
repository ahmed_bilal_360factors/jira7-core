package com.atlassian.jira.issue.search.searchers.renderer;

import com.atlassian.collectors.CollectorsUtil;
import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.config.MockSubTaskManager;
import com.atlassian.jira.config.SubTaskManager;
import com.atlassian.jira.issue.fields.option.Option;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.issue.issuetype.MockIssueType;
import com.atlassian.jira.issue.search.SearchContext;
import com.atlassian.jira.issue.search.SearchContextImpl;
import com.atlassian.jira.issue.search.constants.SystemSearchConstants;
import com.atlassian.jira.issue.transport.FieldValuesHolder;
import com.atlassian.jira.issue.transport.impl.FieldValuesHolderImpl;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoContainer;
import com.atlassian.jira.mock.MockConstantsManager;
import com.atlassian.jira.mock.MockProjectManager;
import com.atlassian.jira.mock.i18n.MockI18nHelper;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.util.I18nHelper;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.function.Function;
import java.util.stream.LongStream;

import static com.atlassian.jira.config.ConstantsManager.ALL_STANDARD_ISSUE_TYPES;
import static com.atlassian.jira.config.ConstantsManager.ALL_SUB_TASK_ISSUE_TYPES;
import static com.atlassian.jira.issue.search.searchers.renderer.IssueTypeSearchRenderer.OPTION_CSS_CLASSES;
import static com.atlassian.jira.issue.search.searchers.renderer.IssueTypeSearchRenderer.SELECTED_OPTIONS;
import static com.atlassian.jira.issue.search.searchers.renderer.IssueTypeSearchRenderer.SELECTED_OPTION_IDS;
import static com.atlassian.jira.issue.search.searchers.renderer.IssueTypeSearchRenderer.SPECIAL_OPTIONS;
import static com.atlassian.jira.issue.search.searchers.renderer.IssueTypeSearchRenderer.STANDARD_OPTIONS;
import static com.atlassian.jira.issue.search.searchers.renderer.IssueTypeSearchRenderer.SUBTASK_OPTIONS;
import static com.atlassian.jira.issue.search.searchers.renderer.IssueTypeSearchRenderer.VALID_OPTION_IDS;
import static com.atlassian.jira.permission.ProjectPermissions.BROWSE_PROJECTS;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.when;

/**
 * @since v5.2
 */
public class TestIssueTypeSearchRenderer {
    @Rule
    public MockitoContainer mockitoContainer = new MockitoContainer(this);

    @AvailableInContainer(interfaceClass = ConstantsManager.class)
    MockConstantsManager constantsMgr = new MockConstantsManager();

    @AvailableInContainer(interfaceClass = ProjectManager.class)
    MockProjectManager projectMgr = new MockProjectManager();

    @AvailableInContainer
    @Mock
    private I18nHelper.BeanFactory i18nFactory;

    @AvailableInContainer
    private SubTaskManager subTaskMgr = new MockSubTaskManager();

    @Mock
    private PermissionManager permissionMgr;

    private Map<String, Object> velocityParameters = new HashMap<>(64);

    private SearchContext searchContext;
    private Fixture searchRenderer;

    private ApplicationUser searcher = new MockApplicationUser("searcher", "Searcher");

    private static final Map<String, IssueType> ISSUE_TYPES = ImmutableMap.of(
            "bug", new MockIssueType("bug", "Bug"),
            "aaa", new MockIssueType("aaa", "Story"),
            "zzz", new MockIssueType("zzz", "Sub-Story", true),
            "task", new MockIssueType("task", "Task"),
            "subtask", new MockIssueType("subtask", "Sub-Task", true));

    // Mocked out answers for database queries
    // Actual database interaction verified by TestIssueTypeSearchRendererDb
    Set<String> visibleIssueTypeIds;
    Map<String, String> visibleIssueTypeIdsWithCssInfo;

    @Before
    public void setUp() {
        searchContext = new SearchContextImpl();
        when(i18nFactory.getInstance(searcher)).thenReturn(new MockI18nHelper());

        withProjectCount(3);
        ISSUE_TYPES.values().forEach(constantsMgr::addIssueType);
        when(permissionMgr.getProjects(BROWSE_PROJECTS, searcher)).thenAnswer(invocation -> {
            final List<Project> projects = new ArrayList<>(projectMgr.getProjectObjects());
            // Return the IDs in a stable order so we can predict the batching
            Collections.sort(projects, Comparator.comparing(Project::getId));
            return projects;
        });

        searchRenderer = new Fixture();
    }

    /**
     * The "All Sub-Task Issue Types" option should not be present if there are no sub-tasks in the search context,
     * so long as it isn't already selected.
     */
    @Test
    public void editWithNoSubtasksInContextExcludesThoseOptionsWhenNotSelected() {
        searchContext = new SearchContextImpl(null, Lists.newArrayList(1L, 2L, 3L), Lists.newArrayList("aaa", "task"));
        visibleIssueTypeIdsWithCssInfo = ImmutableMap.of(
                "bug", "42",
                "task", "42");

        edit("garbage", "bug");

        assertThat(parameter(SELECTED_OPTION_IDS), containsInAnyOrder("bug"));
        assertThat(parameter(VALID_OPTION_IDS), containsInAnyOrder(ALL_STANDARD_ISSUE_TYPES, "bug", "task"));
        assertThat(optionsToIds(SPECIAL_OPTIONS), contains(ALL_STANDARD_ISSUE_TYPES));
        assertThat(optionsToIds(STANDARD_OPTIONS), contains("bug", "task"));
        assertThat(optionsToIds(SUBTASK_OPTIONS), hasSize(0));
        assertOptionCssClasses();
        assertContextParams("Project 1, Project 2, Project 3", "Story, Task");
    }

    /**
     * The "All Sub-Task Issue Types" option should become invalid if there are no sub-tasks in the search context
     * and it is already selected.
     */
    @Test
    public void editWithNoSubtasksInContextMarksThemInvalidIfSelected() {
        searchContext = new SearchContextImpl(null, Lists.newArrayList(1L, 2L, 3L), Lists.newArrayList("aaa", "task"));
        visibleIssueTypeIdsWithCssInfo = ImmutableMap.of(
                "bug", "42",
                "task", "42");

        edit(ALL_SUB_TASK_ISSUE_TYPES, "garbage", "subtask", "bug");

        assertThat(parameter(SELECTED_OPTION_IDS), containsInAnyOrder(ALL_SUB_TASK_ISSUE_TYPES, "subtask", "bug"));
        assertThat(parameter(VALID_OPTION_IDS), containsInAnyOrder(ALL_STANDARD_ISSUE_TYPES, "bug", "task"));
        assertThat(optionsToIds(SPECIAL_OPTIONS), contains(ALL_STANDARD_ISSUE_TYPES, ALL_SUB_TASK_ISSUE_TYPES));
        assertThat(optionsToIds(STANDARD_OPTIONS), contains("bug", "task"));
        assertThat(optionsToIds(SUBTASK_OPTIONS), contains("subtask"));
        assertOptionCssClasses();
        assertContextParams("Project 1, Project 2, Project 3", "Story, Task");
    }

    @Test
    public void editWithSubtasksDisabledExcludesThoseOptionsWhenNotSelected() {
        subTaskMgr.disableSubTasks();
        visibleIssueTypeIdsWithCssInfo = ImmutableMap.of(
                "bug", "42",
                "task", "47 49",
                "subtask", "50",
                "zzz", "50 47");

        edit("task");

        assertThat(parameter(SELECTED_OPTION_IDS), contains("task"));
        assertThat(parameter(VALID_OPTION_IDS), containsInAnyOrder(ALL_STANDARD_ISSUE_TYPES, "bug", "task"));
        assertThat(optionsToIds(SPECIAL_OPTIONS), contains(ALL_STANDARD_ISSUE_TYPES));
        assertThat(optionsToIds(STANDARD_OPTIONS), contains("bug", "task"));
        assertThat(optionsToIds(SUBTASK_OPTIONS), hasSize(0));
        assertOptionCssClasses();
        assertContextParams("", "");
    }

    @Test
    public void editWithSubtasksDisabledMarksThemInvalidIfSelected() {
        subTaskMgr.disableSubTasks();
        visibleIssueTypeIdsWithCssInfo = ImmutableMap.of(
                "bug", "42",
                "task", "47 49",
                "subtask", "50",
                "zzz", "50 47");

        edit(ALL_SUB_TASK_ISSUE_TYPES, "task", "subtask");

        assertThat(parameter(SELECTED_OPTION_IDS), contains(ALL_SUB_TASK_ISSUE_TYPES, "task", "subtask"));
        assertThat(parameter(VALID_OPTION_IDS), containsInAnyOrder(ALL_STANDARD_ISSUE_TYPES, "bug", "task"));
        assertThat(optionsToIds(SPECIAL_OPTIONS), contains(ALL_STANDARD_ISSUE_TYPES, ALL_SUB_TASK_ISSUE_TYPES));
        assertThat(optionsToIds(STANDARD_OPTIONS), contains("bug", "task"));
        assertThat(optionsToIds(SUBTASK_OPTIONS), contains("subtask"));
        assertOptionCssClasses();
        assertContextParams("", "");
    }

    @Test
    public void editWithSubtasksEnabledAndInContextIncludesThoseOptions() {
        visibleIssueTypeIdsWithCssInfo = ImmutableMap.of(
                "bug", "42",
                "task", "47 49",
                "aaa", "52",
                "subtask", "50",
                "zzz", "50 47");

        edit(ALL_SUB_TASK_ISSUE_TYPES);
        assertThat(parameter(SELECTED_OPTION_IDS), contains(ALL_SUB_TASK_ISSUE_TYPES));
        assertThat(parameter(VALID_OPTION_IDS), containsInAnyOrder(ALL_STANDARD_ISSUE_TYPES, ALL_SUB_TASK_ISSUE_TYPES,
                "bug", "task", "aaa", "subtask", "zzz"));
        assertThat(optionsToIds(SPECIAL_OPTIONS), contains(ALL_STANDARD_ISSUE_TYPES, ALL_SUB_TASK_ISSUE_TYPES));
        assertThat(optionsToIds(STANDARD_OPTIONS), contains("bug", "aaa", "task"));
        assertThat(optionsToIds(SUBTASK_OPTIONS), contains("zzz", "subtask"));
        assertOptionCssClasses();
    }

    @Test
    public void editWithComplicatedCssOptions() {
        visibleIssueTypeIdsWithCssInfo = ImmutableMap.<String, String>builder()
                .put("bug", "42 45")
                .put("task", "42 43 44")
                .put("aaa", "41")
                .put("subtask", "49 42")
                .put("zzz", "46")
                .build();

        edit(ALL_STANDARD_ISSUE_TYPES, ALL_SUB_TASK_ISSUE_TYPES);

        assertThat(parameter(SELECTED_OPTION_IDS), contains(ALL_STANDARD_ISSUE_TYPES, ALL_SUB_TASK_ISSUE_TYPES));
        assertThat(parameter(VALID_OPTION_IDS), containsInAnyOrder(ALL_STANDARD_ISSUE_TYPES, ALL_SUB_TASK_ISSUE_TYPES,
                "bug", "task", "aaa", "subtask", "zzz"));
        assertThat(optionsToIds(SPECIAL_OPTIONS), contains(ALL_STANDARD_ISSUE_TYPES, ALL_SUB_TASK_ISSUE_TYPES));
        assertThat(optionsToIds(STANDARD_OPTIONS), contains("bug", "aaa", "task"));
        assertThat(optionsToIds(SUBTASK_OPTIONS), contains("zzz", "subtask"));

        assertOptionCssClasses();
    }

    @Test
    public void viewWithSubtasksEnabledAndInContextIncludesThoseOptions() {
        visibleIssueTypeIds = ImmutableSet.of("aaa", "task", "subtask");

        // bug - not included in the search context
        // garbage - can't be resolved to an existing issue type
        // task - not selected, but would test as valid if it were
        view(ALL_STANDARD_ISSUE_TYPES, ALL_SUB_TASK_ISSUE_TYPES, "subtask", "bug", "garbage", "aaa");

        assertThat(parameter(VALID_OPTION_IDS), containsInAnyOrder(ALL_STANDARD_ISSUE_TYPES, ALL_SUB_TASK_ISSUE_TYPES,
                "subtask", "task", "aaa"));
        assertThat(optionsToIds(SELECTED_OPTIONS), contains(ALL_STANDARD_ISSUE_TYPES, ALL_SUB_TASK_ISSUE_TYPES,
                "subtask", "bug", "aaa"));
        assertContextParams("", "");
    }

    /**
     * "All Sub-Task Issue Types" and any subtasks are invalid if subtasks are disabled.
     */
    @Test
    public void viewWithSubtasksDisabledExcludesThoseOptions() {
        subTaskMgr.disableSubTasks();
        visibleIssueTypeIds = ImmutableSet.of("aaa", "task", "subtask");

        view(ALL_STANDARD_ISSUE_TYPES, ALL_SUB_TASK_ISSUE_TYPES, "subtask", "bug", "garbage", "aaa");

        assertThat(parameter(VALID_OPTION_IDS), containsInAnyOrder(ALL_STANDARD_ISSUE_TYPES, "task", "aaa"));
        assertThat(optionsToIds(SELECTED_OPTIONS), contains(ALL_STANDARD_ISSUE_TYPES, ALL_SUB_TASK_ISSUE_TYPES,
                "subtask", "bug", "aaa"));
    }

    /**
     * "All Sub-Task Issue Types" is also invalid if there aren't any subtasks in the search context.
     */
    @Test
    public void viewWithNoSubtasksInContextExcludesThoseOptions() {
        searchContext = new SearchContextImpl(null, Lists.newArrayList(1L, 2L, 3L), Lists.newArrayList("aaa", "task"));
        visibleIssueTypeIds = ImmutableSet.of("aaa", "task");

        view(ALL_STANDARD_ISSUE_TYPES, ALL_SUB_TASK_ISSUE_TYPES, "subtask", "bug", "garbage", "aaa");

        assertThat(parameter(VALID_OPTION_IDS), containsInAnyOrder(ALL_STANDARD_ISSUE_TYPES, "task", "aaa"));
        assertThat(optionsToIds(SELECTED_OPTIONS), contains(ALL_STANDARD_ISSUE_TYPES, ALL_SUB_TASK_ISSUE_TYPES,
                "subtask", "bug", "aaa"));
        assertContextParams("Project 1, Project 2, Project 3", "Story, Task");
    }


    private void edit(String... selected) {
        searchRenderer.addEditParameters(fieldValuesHolder(selected), searchContext, searcher, velocityParameters);
    }

    private void view(String... selected) {
        searchRenderer.addViewParameters(fieldValuesHolder(selected), searchContext, searcher, velocityParameters);
    }

    private void assertContextParams(String contextProjectNames, String contextIssueTypeNames) {
        assertThat(parameter("contextProjectNames"), is(contextProjectNames));
        assertThat(parameter("contextIssueTypeNames"), is(contextIssueTypeNames));
    }

    private static FieldValuesHolder fieldValuesHolder(String... values) {
        final FieldValuesHolder fieldValuesHolder = new FieldValuesHolderImpl();
        fieldValuesHolder.put(SystemSearchConstants.forIssueType().getUrlParameter(), values);
        return fieldValuesHolder;
    }

    private void withProjectCount(int count) {
        projectMgr.removeAllProjects();
        LongStream.rangeClosed(1L, count)
                .mapToObj(id -> new MockProject(id, "PROJ" + id, "Project " + id))
                .forEach(projectMgr::addProject);
    }

    /**
     * Convenient accessor for velocity parameters with the value type inferred.
     */
    @SuppressWarnings("unchecked")
    private <T> T parameter(String key) {
        return (T) velocityParameters.get(key);
    }

    private List<String> optionsToIds(String key) {
        return map(parameter(key), Option::getId);
    }

    private static <T> List<T> map(Collection<Option> options, Function<Option, T> mapper) {
        return options.stream()
                .map(mapper)
                .collect(CollectorsUtil.toNewArrayListWithSizeOf(options));
    }

    void assertOptionCssClasses() {
        final Map<String, String> actual = parameter(OPTION_CSS_CLASSES);
        new CssVerifier(visibleIssueTypeIdsWithCssInfo).assertEquals(actual);
    }


    class Fixture extends IssueTypeSearchRenderer {
        public Fixture() {
            super(null, constantsMgr, null, permissionMgr, SystemSearchConstants.forIssueType(),
                    null, subTaskMgr, null, null);
        }

        @Override
        Set<String> getVisibleIssueTypeIds(Collection<Project> projects) {
            return new HashSet<>(visibleIssueTypeIds);
        }

        @Override
        Map<String, String> getVisibleIssueTypeIdsWithCssInfo(Collection<Project> projects) {
            return new HashMap<>(visibleIssueTypeIdsWithCssInfo);
        }
    }


    static class CssVerifier {
        private final Map<String, String> expected = new TreeMap<>();

        CssVerifier(Map<String, String> expected) {
            expected.forEach((issueTypeId, fieldConfigIds) -> this.expected.put(issueTypeId, sortIds(fieldConfigIds)));
        }

        void assertEquals(Map<String, String> optionCssClasses) {
            final Map<String, String> sortedCssClasses = new TreeMap<>();
            optionCssClasses.forEach((optionId, cssClasses) -> sortedCssClasses.put(optionId, sortIds(cssClasses)));
            if (!sortedCssClasses.equals(expected)) {
                fail("CSS Classes do not match.\n\tExp: " + expected + "\n\tGot: " + sortedCssClasses);
            }
        }

        private static String sortIds(String cssClasses) {
            final String[] ids = StringUtils.split(cssClasses, ' ');
            Arrays.sort(ids);
            return StringUtils.join(ids, ' ');
        }
    }
}