package com.atlassian.jira.util.collect;

import com.google.common.collect.Sets;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Test for {@link com.atlassian.jira.util.collect.IdentitySet}.
 *
 * @since v4.0
 */
public class TestIdentitySet {
    @Rule
    public final MockitoRule initMockito = MockitoJUnit.rule();
    @Mock
    private Set<IdentitySet.IdentityReference<Object>> identityReferences;

    @Test
    public void testNewSet() {
        final IdentitySet<Object> objectIdentitySet = IdentitySet.newSet();
        assertNotNull(objectIdentitySet);
        assertTrue(objectIdentitySet.isEmpty());
        assertFalse(objectIdentitySet.iterator().hasNext());
    }

    @Test
    public void testNewListOrderedSet() {
        final IdentitySet<Object> objectIdentitySet = IdentitySet.newListOrderedSet();
        assertNotNull(objectIdentitySet);
        assertTrue(objectIdentitySet.isEmpty());
        assertFalse(objectIdentitySet.iterator().hasNext());

        final List<Object> expectedObjects = CollectionBuilder.newBuilder(new Object(), new Object(), 1).asList();

        for (final Object object : expectedObjects) {
            assertTrue(objectIdentitySet.add(object));
        }

        assertEquals(expectedObjects.size(), objectIdentitySet.size());
        final Iterator<Object> actualIterator = objectIdentitySet.iterator();
        for (final Object object : expectedObjects) {
            assertSame(object, actualIterator.next());
        }
    }

    @Test
    public void testConstructor() throws Exception {
        try {
            new IdentitySet<Object>(null);
        } catch (final IllegalArgumentException expected) {
        }
    }

    @Test
    public void testClear() {
        final IdentitySet<Object> identitySet = new IdentitySet<Object>(identityReferences);
        identitySet.clear();
        verify(identityReferences).clear();
    }

    @Test
    public void testAdd() {
        final Object obj = new Object();

        when(identityReferences.add(new IdentitySet.IdentityReference<>(obj))).thenReturn(true);
        when(identityReferences.add(null)).thenReturn(false);

        final IdentitySet<Object> identitySet = new IdentitySet<Object>(identityReferences);

        assertTrue(identitySet.add(obj));
        assertFalse(identitySet.add(null));
    }

    @Test
    public void testRemove() {
        final Object obj = new Object();

        when(identityReferences.remove(new IdentitySet.IdentityReference<Object>(obj))).thenReturn(true);
        when(identityReferences.remove(null)).thenReturn(false);

        final IdentitySet<Object> identitySet = new IdentitySet<Object>(identityReferences);

        assertTrue(identitySet.remove(obj));
        assertFalse(identitySet.remove(null));
    }

    @Test
    public void testContainsAll() {
        final List<Integer> integers = CollectionBuilder.newBuilder(10, 12, 11, 23, 11).asList();

        when(identityReferences.containsAll(convert(integers))).thenReturn(true);
        when(identityReferences.containsAll(null)).thenReturn(false);

        final IdentitySet<Object> identitySet = new IdentitySet<Object>(identityReferences);

        assertTrue(identitySet.containsAll(integers));
        assertFalse(identitySet.containsAll(null));
    }

    @Test
    public void testAddAll() {
        final List<Double> doubles = CollectionBuilder.newBuilder(10.0, 12.1, 11.24, 23.23484, 11.3726423).asList();

        when(identityReferences.addAll(TestIdentitySet.<Object>convert(doubles))).thenReturn(false);
        when(identityReferences.addAll(null)).thenReturn(true);

        final IdentitySet<Object> identitySet = new IdentitySet<Object>(identityReferences);

        assertFalse(identitySet.addAll(doubles));
        assertTrue(identitySet.addAll(null));
    }

    @Test
    public void testRetainAll() {
        final List<Long> longs = asList(10L);

        when(identityReferences.retainAll(TestIdentitySet.<Object>convert(longs))).thenReturn(false);
        when(identityReferences.retainAll(null)).thenReturn(true);

        final IdentitySet<Object> identitySet = new IdentitySet<Object>(identityReferences);

        assertFalse(identitySet.retainAll(longs));
        assertTrue(identitySet.retainAll(null));
    }

    @Test
    public void testRemoveAll() {
        final List<Object> emptyObjects = Collections.emptyList();
        final List<Object> objects = asList(new Object(), new Object());

        when(identityReferences.removeAll(TestIdentitySet.convert(emptyObjects))).thenReturn(false);
        when(identityReferences.removeAll(null)).thenReturn(true);
        when(identityReferences.removeAll(convert(objects))).thenReturn(false);

        final IdentitySet<Object> identitySet = new IdentitySet<Object>(identityReferences);

        assertFalse(identitySet.removeAll(emptyObjects));
        assertTrue(identitySet.removeAll(null));
        assertFalse(identitySet.removeAll(objects));
    }

    @Test
    public void testSize() {
        when(identityReferences.size()).thenReturn(10);

        final IdentitySet<Object> identitySet = new IdentitySet<Object>(identityReferences);
        assertEquals(10, identitySet.size());
    }

    @Test
    public void testIsEmpty() {
        when(identityReferences.isEmpty()).thenReturn(false);
        final IdentitySet<Object> identitySet = new IdentitySet<>(identityReferences);
        assertFalse(identitySet.isEmpty());
    }

    @Test
    public void testIsNotEmpty() {
        when(identityReferences.isEmpty()).thenReturn(true);
        final IdentitySet<Object> identitySet = new IdentitySet<>(identityReferences);
        assertTrue(identitySet.isEmpty());
    }

    @Test
    public void testContains() {
        final Object obj = new Object();

        when(identityReferences.contains(new IdentitySet.IdentityReference<Object>(obj))).thenReturn(true);
        when(identityReferences.contains(new IdentitySet.IdentityReference<Object>(null))).thenReturn(false);

        final IdentitySet<Object> identitySet = new IdentitySet<Object>(identityReferences);

        assertTrue(identitySet.contains(obj));
        assertFalse(identitySet.contains(null));
    }

    @SuppressWarnings({"UnnecessaryBoxing"})
    @Test
    public void testIterator() throws Exception {
        final Double value = new Double(10.0d);

        final HashSet<IdentitySet.IdentityReference<Double>> valueSet = Sets.newHashSet(IdentitySet.IdentityReference.wrap(value));

        final IdentitySet<Double> identitySet = new IdentitySet<Double>(valueSet);
        final Iterator<Double> doubleIter = identitySet.iterator();
        assertTrue(doubleIter.hasNext());
        assertEquals(value, doubleIter.next());
        doubleIter.remove();
        assertFalse(doubleIter.hasNext());
    }

    @SuppressWarnings({"UnnecessaryBoxing"})
    @Test
    public void testRealTest() throws Exception {
        final Integer one1 = new Integer(1);
        final Integer one2 = new Integer(1);
        final Integer one3 = new Integer(1);
        final Integer one4 = new Integer(1);

        //check the initial state of the set
        final IdentitySet<Integer> testSet = IdentitySet.newListOrderedSet();
        assertNotNull(testSet);
        assertTrue(testSet.isEmpty());
        assertEquals(0, testSet.size());
        assertFalse(testSet.iterator().hasNext());
        assertFalse(testSet.contains(one1));
        assertFalse(testSet.containsAll(Collections.singletonList(one2)));
        assertTrue(testSet.containsAll(Collections.emptyList()));
        assertFalse(testSet.remove(one1));
        assertFalse(testSet.removeAll(asList(one1, one2, one3)));
        assertFalse(testSet.removeAll(Collections.emptyList()));
        assertFalse(testSet.retainAll(asList(one1, one2)));
        assertFalse(testSet.retainAll(Collections.emptyList()));
        assertFalse(testSet.remove(one2));

        //The set should now contain {one1}
        assertTrue(testSet.add(one1));
        assertFalse(testSet.isEmpty());
        assertEquals(1, testSet.size());
        assertTrue(testSet.iterator().hasNext());
        assertSame(one1, testSet.iterator().next());
        assertTrue(testSet.contains(one1));
        assertFalse(testSet.contains(one2));
        assertFalse(testSet.contains(null));
        assertTrue(testSet.containsAll(Collections.singletonList(one1)));
        assertFalse(testSet.containsAll(asList(one1, null)));
        assertFalse(testSet.removeAll(asList(one2, one3)));
        assertFalse(testSet.retainAll(asList(one1, null)));
        assertFalse(testSet.remove(one3));

        assertFalse(testSet.add(one1));

        //The set should now contain {one1, one2, one4}
        assertTrue(testSet.addAll(asList(one1, one2, one4)));
        assertFalse(testSet.addAll(asList(one2, one4)));
        assertFalse(testSet.add(one2));
        assertFalse(testSet.add(one4));
        assertFalse(testSet.isEmpty());
        assertEquals(3, testSet.size());

        List<Integer> expectedContent = asList(one1, one2, one4);
        Iterator<Integer> actualIterator = testSet.iterator();
        for (final Integer expectedInteger : expectedContent) {
            assertTrue(testSet.contains(expectedInteger));
            assertTrue(testSet.containsAll(Collections.singletonList(expectedInteger)));
            assertTrue(actualIterator.hasNext());
            assertSame(expectedInteger, actualIterator.next());
        }
        assertFalse(actualIterator.hasNext());
        assertTrue(testSet.containsAll(Collections.singletonList(one1)));
        assertTrue(testSet.containsAll(asList(one2, one1)));
        assertFalse(testSet.containsAll(asList(one3, one1, one2, one4)));
        assertFalse(testSet.removeAll(asList(one3, null, new Integer(1))));
        assertFalse(testSet.retainAll(asList(one1, one2, one3, one4)));
        assertFalse(testSet.remove(one3));

        //The set should not contain {one2, one4}.
        assertTrue(testSet.remove(one1));
        assertFalse(testSet.remove(one1));

        expectedContent = asList(one2, one4);
        actualIterator = testSet.iterator();
        for (final Integer expectedInteger : expectedContent) {
            assertTrue(testSet.contains(expectedInteger));
            assertTrue(testSet.containsAll(Collections.singletonList(expectedInteger)));
            assertTrue(actualIterator.hasNext());
            assertSame(expectedInteger, actualIterator.next());
        }

        //The set should not be empty.
        assertTrue(testSet.removeAll(asList(one1, one2, one3, one4)));
        assertTrue(testSet.isEmpty());
        assertFalse(testSet.iterator().hasNext());

        assertTrue(testSet.addAll(asList(one2, one3, one4)));

        expectedContent = asList(one2, one3);
        //The set should now be {one2, one3}.
        assertTrue(testSet.retainAll(asList(one1, one2, one3)));
        assertFalse(testSet.retainAll(asList(one1, one2, one3, new Double(20), new ArrayList<String>())));
        assertEquals(2, testSet.size());

        actualIterator = testSet.iterator();
        for (final Integer expectedInteger : expectedContent) {
            assertTrue(testSet.contains(expectedInteger));
            assertTrue(testSet.containsAll(Collections.singletonList(expectedInteger)));
            assertTrue(actualIterator.hasNext());
            assertSame(expectedInteger, actualIterator.next());
        }

        try {
            actualIterator.next();
            fail("Should not be able to iterate past the end of the set.");
        } catch (final NoSuchElementException ignored) {
        }

        actualIterator = testSet.iterator();
        try {
            actualIterator.remove();
            fail("Should not be able to remove.");
        } catch (final IllegalStateException ignored) {
        }

        final Iterator<Integer> expectedIterator = expectedContent.iterator();
        while (expectedIterator.hasNext()) {
            final Integer expectedRemovedElement = expectedIterator.next();
            expectedIterator.remove();

            assertTrue(actualIterator.hasNext());
            final Integer actualRemovedElement = actualIterator.next();
            actualIterator.remove();
            assertSame(expectedRemovedElement, actualRemovedElement);

            assertEquals(expectedContent.size(), testSet.size());
            assertTrue(testSet.containsAll(expectedContent));
        }

        assertTrue(testSet.isEmpty());
    }

    private static <C> Collection<IdentitySet.IdentityReference<C>> convert(final Collection<? extends C> collection) {
        final List<IdentitySet.IdentityReference<C>> list = new ArrayList<IdentitySet.IdentityReference<C>>(collection.size());
        for (final C c : collection) {
            list.add(new IdentitySet.IdentityReference<C>(c));
        }
        return list;
    }

    private static <T> List<T> asList(final T... values) {
        return new ArrayList<T>(Arrays.asList(values));
    }
}
