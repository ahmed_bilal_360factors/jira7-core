package com.atlassian.jira.web.servlet;

import com.atlassian.core.ofbiz.test.UtilsForTests;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.exception.AttachmentNotFoundException;
import com.atlassian.jira.exception.DataAccessException;
import com.atlassian.jira.issue.AttachmentManager;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.attachment.Attachment;
import com.atlassian.jira.issue.attachment.AttachmentConstants;
import com.atlassian.jira.mock.component.MockComponentWorker;
import com.atlassian.jira.mock.issue.MockIssue;
import com.atlassian.jira.mock.ofbiz.MockOfBizDelegator;
import com.atlassian.jira.ofbiz.FieldMap;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;

import static com.atlassian.jira.security.Permissions.BROWSE;
import static com.atlassian.jira.util.BrowserUtils.USER_AGENT_HEADER;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Tests the ViewAttachmentServlet class.
 */
public class TestViewAttachmentServlet {
    @Mock
    private AttachmentManager mockAttachmentManager;
    @Mock
    private HttpServletRequest mockRequest;
    @Mock
    private HttpServletResponse mockResponse;
    @Mock
    private IssueManager mockIssueManager;
    @Mock
    private JiraAuthenticationContext mockAuthenticationContext;
    @Mock
    private MimeSniffingKit mockMimeSniffingKit;
    @Mock
    private PermissionManager mockPermissionManager;
    @Mock
    private RequestDispatcher mockRequestDispatcher;
    @Mock
    private UserManager mockUserManager;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        new MockComponentWorker().init()
                .addMock(JiraAuthenticationContext.class, mockAuthenticationContext)
                .addMock(MimeSniffingKit.class, mockMimeSniffingKit)
                .addMock(OfBizDelegator.class, new MockOfBizDelegator())
                .addMock(PermissionManager.class, mockPermissionManager)
                .addMock(UserManager.class, mockUserManager);
    }

    @After
    public void tearDown() {
        ComponentAccessor.initialiseWorker(null);
    }

    @Test
    public void testHasPermissions() throws Exception {
        // Set up
        final Long issueId = 1L;

        //the issue that will be returned from the attachment
        final MutableIssue issue = new MockIssue();
        issue.setIssueTypeId(issueId.toString());

        //our trusty test user edwin
        final String username = "edwin";
        final ApplicationUser edwin = new MockApplicationUser(username);
        when(mockAuthenticationContext.getLoggedInUser()).thenReturn(edwin);
        when(mockPermissionManager.hasPermission(ProjectPermissions.BROWSE_PROJECTS, issue, edwin)).thenReturn(true);
        when(mockIssueManager.getIssueObject(issueId)).thenReturn(issue);

        final Attachment attachment = new Attachment(mockIssueManager, UtilsForTests.getTestEntity(AttachmentConstants.ATTACHMENT_ENTITY_NAME,
                FieldMap.build("issue", issueId)), null);
        final ViewAttachmentServlet viewAttachmentServlet = new PublicViewAttachmentServlet(attachment);

        // Invoke and check
        assertTrue(viewAttachmentServlet.loggedInUserHasPermissionToViewAttachment(attachment));
    }

    @Test
    public void testRedirectIfAnonUserHasNoPermissions() throws Exception {
        //set it up so the user has no permissions
        final ViewAttachmentServlet viewAttachmentServlet = new PublicViewAttachmentServlet(null) {
            @Override
            protected boolean loggedInUserHasPermissionToViewAttachment(final Attachment attachment) {
                return false;
            }
        };

        when(mockRequest.getContextPath()).thenReturn("someContext");
        when(mockRequest.getMethod()).thenReturn("GET");
        when(mockRequest.getPathInfo()).thenReturn("/attachments/something");
        when(mockRequest.getRemoteUser()).thenReturn("");

        when(mockAuthenticationContext.getLoggedInUser()).thenReturn(null);

        // Invoke
        viewAttachmentServlet.service(mockRequest, mockResponse);

        // Check
        verify(mockResponse).sendRedirect("someContext/login.jsp?user_role=");
    }

    @Test
    public void testRedirectIfUserHasNoPermissions() throws Exception {
        //set it up so the user has no permissions
        final ViewAttachmentServlet viewAttachmentServlet = new PublicViewAttachmentServlet(null) {
            @Override
            protected boolean loggedInUserHasPermissionToViewAttachment(final Attachment attachment) {
                return false;
            }
        };

        when(mockRequest.getContextPath()).thenReturn("someContext");
        when(mockRequest.getMethod()).thenReturn("GET");
        when(mockRequest.getPathInfo()).thenReturn("/attachments/something");
        when(mockRequest.getRemoteUser()).thenReturn("");
        when(mockRequest.getRequestDispatcher("/secure/views/securitybreach.jsp")).thenReturn(mockRequestDispatcher);

        when(mockAuthenticationContext.getLoggedInUser()).thenReturn(new MockApplicationUser("edwin"));

        // Invoke
        viewAttachmentServlet.service(mockRequest, mockResponse);

        // Check
        verify(mockRequestDispatcher).forward(mockRequest, mockResponse);
    }

    @Test
    public void testResponseHeaders() throws Exception {
        // Set up
        final Attachment attachment = new Attachment(null, UtilsForTests.getTestEntity(AttachmentConstants.ATTACHMENT_ENTITY_NAME,
                FieldMap.build("mimetype", "sampleMimeType", "filesize", 11L, "filename", "sampleFilename")), null);
        final PublicViewAttachmentServlet viewAttachmentServlet = new PublicViewAttachmentServlet(attachment);
        final String userAgent = "secretAgent";
        when(mockRequest.getHeader(USER_AGENT_HEADER)).thenReturn(userAgent);
        when(mockRequest.getPathInfo()).thenReturn("/attachments/something");
        when(mockRequest.getRemoteUser()).thenReturn("");

        // Invoke
        viewAttachmentServlet.setResponseHeaders(mockRequest, Optional.empty(), mockResponse);

        // Check
        verify(mockMimeSniffingKit).setAttachmentResponseHeaders(attachment, userAgent, mockResponse);
        verify(mockResponse).setContentType("sampleMimeType");
        verify(mockResponse).setContentLength(11);
        verify(mockResponse).setHeader("Accept-Ranges", "bytes");
        verify(mockResponse).setDateHeader("Expires", -1);
    }

    @Test
    public void testResponseHeadersRangeRequest() throws Exception {
        // Set up
        final Attachment attachment = new Attachment(null, UtilsForTests.getTestEntity(AttachmentConstants.ATTACHMENT_ENTITY_NAME,
                FieldMap.build("mimetype", "video/mpeg", "filesize", 11L, "filename", "sampleFilename")), null);
        final PublicViewAttachmentServlet viewAttachmentServlet = new PublicViewAttachmentServlet(attachment);
        final String userAgent = "secretAgent";
        when(mockRequest.getHeader(USER_AGENT_HEADER)).thenReturn(userAgent);
        when(mockRequest.getPathInfo()).thenReturn("/attachments/something");
        when(mockRequest.getRemoteUser()).thenReturn("");

        // Invoke
        viewAttachmentServlet.setResponseHeaders(mockRequest, Optional.of(new RangeResponse(48, 99, 100)), mockResponse);

        // Check
        verify(mockMimeSniffingKit).setAttachmentResponseHeaders(attachment, userAgent, mockResponse);
        verify(mockResponse).setContentType("video/mpeg");
        verify(mockResponse).setContentLength(52);
        verify(mockResponse).setStatus(HttpServletResponse.SC_PARTIAL_CONTENT);
        verify(mockResponse).setHeader("Accept-Ranges", "bytes");
        verify(mockResponse).setHeader("Content-Range", "bytes 48-99/100");
        verify(mockResponse).setDateHeader("Expires", -1);
    }

    @Test
    public void testDoGet_RangeInvalid() throws Exception {
        // Set up
        final Attachment attachment = new Attachment(null, UtilsForTests.getTestEntity(AttachmentConstants.ATTACHMENT_ENTITY_NAME,
                FieldMap.build("mimetype", "audio/mpeg3", "filesize", 11L, "filename", "sampleFilename")), null);
        final PublicViewAttachmentServlet viewAttachmentServlet = new PublicViewAttachmentServlet(attachment);

        when(mockRequest.getPathInfo()).thenReturn("/10100/alphabet.txt");
        when(mockRequest.getHeader("Range")).thenReturn("bytes=12-foo");

        // Invoke
        viewAttachmentServlet.doGet(mockRequest, mockResponse);

        // Check
        verify(mockResponse).sendError(400, "Malformed Range header");
    }

    @Test
    public void testDoGet_RangeNotSatisfiable() throws Exception {
        // Set up
        final Attachment attachment = new Attachment(null, UtilsForTests.getTestEntity(AttachmentConstants.ATTACHMENT_ENTITY_NAME,
                FieldMap.build("mimetype", "audio/mpeg3", "filesize", 500L, "filename", "sampleFilename")), null);
        final PublicViewAttachmentServlet viewAttachmentServlet = new PublicViewAttachmentServlet(attachment);

        when(mockRequest.getPathInfo()).thenReturn("/10100/alphabet.txt");
        when(mockRequest.getHeader("Range")).thenReturn("bytes=1024-");

        // Invoke
        viewAttachmentServlet.doGet(mockRequest, mockResponse);

        // Check
        verify(mockResponse).sendError(416, "Cannot return request range 'bytes=1024-'. Attachment has only 500 bytes");
        verify(mockResponse).setHeader("Accept-Ranges", "bytes");
        verify(mockResponse).setHeader("Content-Range", "bytes */500");
    }

    @Test
    public void testCacheControlHeadersShouldEncourageLongTermCaching() throws Exception {
        // Set up
        final Attachment attachment = new Attachment(null, UtilsForTests.getTestEntity(AttachmentConstants.ATTACHMENT_ENTITY_NAME,
                FieldMap.build("mimetype", "sampleMimeType", "filesize", 11L, "filename", "sampleFilename")), null);
        final PublicViewAttachmentServlet viewAttachmentServlet = new PublicViewAttachmentServlet(attachment);
        when(mockRequest.getPathInfo()).thenReturn("/attachments/something");
        when(mockRequest.getRemoteUser()).thenReturn("");

        // Invoke
        viewAttachmentServlet.setResponseHeaders(mockRequest, Optional.empty(), mockResponse);

        // Check
        verify(mockResponse).setHeader("Cache-control", "private, max-age=31536000");
        verify(mockResponse).setDateHeader("Expires", -1L); // forbid HTTP1.0 shared proxies from caching attachments
    }

    @Test(expected = InvalidAttachmentPathException.class)
    public void testInvalidAttachmentPath() throws Exception {
        // Set up
        final PublicViewAttachmentServlet viewAttachmentServlet = new PublicViewAttachmentServlet(null);
        when(mockRequest.getPathInfo()).thenReturn("abc");
        when(mockRequest.getRemoteUser()).thenReturn("");

        // Invoke and expect exception
        viewAttachmentServlet.setResponseHeaders(mockRequest, Optional.empty(), mockResponse);
    }

    @Test
    public void testAttachmentNotFoundWithStringId() throws Exception {
        ViewAttachmentServlet viewAttachmentServlet = new ViewAttachmentServlet();
        try {
            viewAttachmentServlet.getAttachment("/abc/");
            fail("AttachmentNotFoundException should have been thrown.");
        } catch (AttachmentNotFoundException e) {
            assertTrue(e.getMessage().contains("abc"));
        }
    }

    @Test
    public void testStreamRange() throws Exception {
        ViewAttachmentServlet viewAttachmentServlet = new ViewAttachmentServlet();
        ByteArrayOutputStream out;

        out = new ByteArrayOutputStream();
        viewAttachmentServlet.copyRange(new ByteArrayInputStream("0123456789".getBytes()), out, new RangeResponse(0, 1, 500));
        assertEquals("01", out.toString());

        out = new ByteArrayOutputStream();
        viewAttachmentServlet.copyRange(new ByteArrayInputStream("0123456789".getBytes()), out, new RangeResponse(1, 3, 500));
        assertEquals("123", out.toString());

        out = new ByteArrayOutputStream();
        viewAttachmentServlet.copyRange(new ByteArrayInputStream("0123456789".getBytes()), out, new RangeResponse(1, 4, 500));
        assertEquals("1234", out.toString());

        out = new ByteArrayOutputStream();
        viewAttachmentServlet.copyRange(new ByteArrayInputStream("0123456789".getBytes()), out, new RangeResponse(1, 5, 500));
        assertEquals("12345", out.toString());

        out = new ByteArrayOutputStream();
        viewAttachmentServlet.copyRange(new ByteArrayInputStream("0123456789".getBytes()), out, new RangeResponse(5, 7, 500));
        assertEquals("567", out.toString());

        out = new ByteArrayOutputStream();
        viewAttachmentServlet.copyRange(new ByteArrayInputStream("0123456789".getBytes()), out, new RangeResponse(2, null, 500));
        assertEquals("23456789", out.toString());

        out = new ByteArrayOutputStream();
        viewAttachmentServlet.copyRange(new ByteArrayInputStream("0123456789".getBytes()), out, new RangeResponse(3, 15, 500));
        assertEquals("3456789", out.toString());

        out = new ByteArrayOutputStream();
        viewAttachmentServlet.copyRange(new ByteArrayInputStream("0123456789".getBytes()), out, new RangeResponse(9, 15, 500));
        assertEquals("9", out.toString());
    }

    @Test
    public void testStreamRangeBig() throws Exception {
        // Intended to test boundary conditions of BUFFER size = 4096

        ViewAttachmentServlet viewAttachmentServlet = new ViewAttachmentServlet();
        ByteArrayOutputStream out;
        byte[] byteArray;
        AtomicBoolean bufferUsed = new AtomicBoolean(false);

        out = new ByteArrayOutputStream();
        viewAttachmentServlet.copyRange(getBigStream(20_000, bufferUsed), out, new RangeResponse(0, 4094, 5000));
        byteArray = out.toByteArray();
        assertEquals(4095, byteArray.length);
        assertEquals(0, byteArray[0]);
        assertEquals(0, byteArray[99]);
        assertEquals(1, byteArray[100]);
        assertEquals(1, byteArray[199]);
        assertEquals(39, byteArray[3999]);
        assertEquals(40, byteArray[4000]);
        assertEquals(40, byteArray[4094]);

        out = new ByteArrayOutputStream();
        viewAttachmentServlet.copyRange(getBigStream(20_000, bufferUsed), out, new RangeResponse(0, 4095, 5000));
        byteArray = out.toByteArray();
        assertEquals(4096, byteArray.length);
        assertEquals(0, byteArray[0]);
        assertEquals(0, byteArray[99]);
        assertEquals(1, byteArray[100]);
        assertEquals(1, byteArray[199]);
        assertEquals(39, byteArray[3999]);
        assertEquals(40, byteArray[4000]);
        assertEquals(40, byteArray[4094]);
        assertEquals(40, byteArray[4095]);

        out = new ByteArrayOutputStream();
        viewAttachmentServlet.copyRange(getBigStream(20_000, bufferUsed), out, new RangeResponse(0, 4096, 5000));
        byteArray = out.toByteArray();
        assertEquals(4097, byteArray.length);
        assertEquals(0, byteArray[0]);
        assertEquals(0, byteArray[99]);
        assertEquals(1, byteArray[100]);
        assertEquals(1, byteArray[199]);
        assertEquals(39, byteArray[3999]);
        assertEquals(40, byteArray[4000]);
        assertEquals(40, byteArray[4094]);
        assertEquals(40, byteArray[4095]);
        assertEquals(40, byteArray[4096]);

        assertTrue("Looks like we are not buffering any more?", bufferUsed.get());
    }

    private InputStream getBigStream(final int length, AtomicBoolean bufferUsed) {
        return new InputStream() {
            private int count = 0;

            @Override
            public int read() throws IOException {
                if (count >= length)
                    return -1;
                else {
                    return count++ / 100;
                }
            }

            @Override
            public int read(byte b[], int off, int len) throws IOException {
                bufferUsed.set(true);
                assertEquals("This test is for a buffer size of 4096 - you need to rewrite the test", 4096, b.length);
                return super.read(b, off, len);
            }
        };
    }

    private static class PublicViewAttachmentServlet extends ViewAttachmentServlet {
        private final Attachment attachment;

        private PublicViewAttachmentServlet(final Attachment attachment) {
            this.attachment = attachment;
        }

        @Override
        public Attachment getAttachment(final String query) {
            return attachment;
        }
    }
}
