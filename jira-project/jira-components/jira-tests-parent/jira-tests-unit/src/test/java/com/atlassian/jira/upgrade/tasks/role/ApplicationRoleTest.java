package com.atlassian.jira.upgrade.tasks.role;

import com.atlassian.jira.application.ApplicationKeys;
import com.google.common.collect.ImmutableSet;
import org.junit.Test;

import static com.atlassian.jira.upgrade.tasks.role.TestUtils.asGroups;
import static com.atlassian.jira.upgrade.tasks.role.TestUtils.newGroup;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;

public class ApplicationRoleTest {
    @Test(expected = IllegalArgumentException.class)
    public void forKeyRejectsNullKey() {
        //when
        ApplicationRole.forKey(null);

        //then exception.
    }

    @Test
    public void forKeyReturnsEmptyRoleForKey() {
        //when
        final ApplicationRole role = ApplicationRole.forKey(ApplicationKeys.CORE);

        //then
        assertThat(role, ApplicationRoleMatcher.forKey(ApplicationKeys.CORE));
    }

    @Test
    public void withGroupsSetsGroupsAndDefaults() {
        //when
        final ApplicationRole role = ApplicationRole.forKey(ApplicationKeys.CORE)
                .withGroups(asGroups("one"), asGroups("one"));

        //then
        assertThat(role, ApplicationRoleMatcher.forKey(ApplicationKeys.CORE).withGroups("oNe").withDefaultGroups("onE"));
    }

    @Test
    public void withGroupsAllowsDefaultsToBeSubestOfGroups() {
        //when
        final ApplicationRole role = ApplicationRole.forKey(ApplicationKeys.CORE)
                .withGroups(asGroups("one", "two"), asGroups("one"));

        //then
        assertThat(role, ApplicationRoleMatcher.forKey(ApplicationKeys.CORE)
                .withGroups("one", "two").withDefaultGroups("one"));
    }

    @Test(expected = MigrationFailedException.class)
    public void withGroupsErrorsOutWhenDefaultsNotSubsetOfGroups() {
        //when
        ApplicationRole.forKey(ApplicationKeys.CORE)
                .withGroups(asGroups("one"), asGroups("one", "two"));

        //then exception
    }

    @Test
    public void addDefaultGroupAddsGroupAndSetsAsDefault() {
        //when
        final ApplicationRole role = ApplicationRole.forKey(ApplicationKeys.CORE)
                .withGroups(asGroups("one"), asGroups("one")).addGroupAsDefault(newGroup("two"));

        //then
        assertThat(role, ApplicationRoleMatcher.forKey(ApplicationKeys.CORE)
                .withGroups("one", "two").withDefaultGroups("one", "two"));
    }

    @Test
    public void addDefaultGroupSetsGroupAsDefault() {
        //when
        final ApplicationRole role = ApplicationRole.forKey(ApplicationKeys.CORE)
                .withGroups(asGroups("one"), ImmutableSet.of()).addGroupAsDefault(newGroup("one"));

        //then
        assertThat(role, ApplicationRoleMatcher.forKey(ApplicationKeys.CORE)
                .withGroups("one").withDefaultGroups("one"));
    }

    @Test
    public void addDefaultGroupIsNoOpWhenGroupAlreadyDefault() {
        //when
        final ApplicationRole role = ApplicationRole.forKey(ApplicationKeys.CORE)
                .withGroups(asGroups("one"), asGroups("one")).addGroupAsDefault(newGroup("one"));

        //then
        assertThat(role, ApplicationRoleMatcher.forKey(ApplicationKeys.CORE)
                .withGroups("one").withDefaultGroups("one"));
    }

    @Test
    public void addGroupAddsNewGroup() {
        //when
        final ApplicationRole role = ApplicationRole.forKey(ApplicationKeys.CORE)
                .withGroups(ImmutableSet.of(), ImmutableSet.of()).addGroup(newGroup("one"));

        //then
        assertThat(role, ApplicationRoleMatcher.forKey(ApplicationKeys.CORE)
                .withGroups("one"));
    }

    @Test
    public void addGroupIsNoOpWhenGroupAlreadyExists() {
        //when
        final ApplicationRole role = ApplicationRole.forKey(ApplicationKeys.CORE)
                .withGroups(asGroups("one"), ImmutableSet.of()).addGroup(newGroup("one"));

        //then
        assertThat(role, ApplicationRoleMatcher.forKey(ApplicationKeys.CORE)
                .withGroups("one"));
    }

    @Test
    public void addGroupIsNoOpWhenGroupAlreadyExistsAndIsDefault() {
        //when
        final ApplicationRole role = ApplicationRole.forKey(ApplicationKeys.CORE)
                .withGroups(asGroups("one"), asGroups("one")).addGroup(newGroup("one"));

        //then
        assertThat(role, ApplicationRoleMatcher.forKey(ApplicationKeys.CORE)
                .withGroups("one").withDefaultGroups("one"));
    }

    @Test
    public void removeFromDefaultsIsNoOpIfGroupNotPresent() {
        //when
        final ApplicationRole role = ApplicationRole.forKey(ApplicationKeys.CORE)
                .withGroups(asGroups("one"), asGroups("one")).removeFromDefaults(newGroup("two"));

        //then
        assertThat(role, ApplicationRoleMatcher.forKey(ApplicationKeys.CORE)
                .withGroups("one").withDefaultGroups("one"));
    }

    @Test
    public void removeFromDefaultsIsNoOpIfGroupNotDefault() {
        //when
        final ApplicationRole role = ApplicationRole.forKey(ApplicationKeys.CORE)
                .withGroups(asGroups("one"), ImmutableSet.of()).removeFromDefaults(newGroup("one"));

        //then
        assertThat(role, ApplicationRoleMatcher.forKey(ApplicationKeys.CORE).withGroups("one"));
    }

    @Test
    public void removeFromDefaultsRemovesGroupFromDefaults() {
        //when
        final ApplicationRole role = ApplicationRole.forKey(ApplicationKeys.CORE)
                .withGroups(asGroups("one"), asGroups("one")).removeFromDefaults(newGroup("one"));

        //then
        assertThat(role, ApplicationRoleMatcher.forKey(ApplicationKeys.CORE).withGroups("one"));
    }

    @Test
    public void equalityChecksKey() {
        //given
        final ApplicationRole coreRole = ApplicationRole.forKey(ApplicationKeys.CORE)
                .addGroupAsDefault(newGroup("core"));
        final ApplicationRole coreRole2 = ApplicationRole.forKey(ApplicationKeys.CORE)
                .addGroupAsDefault(newGroup("core"));
        final ApplicationRole softwareRole = ApplicationRole.forKey(ApplicationKeys.SOFTWARE)
                .addGroupAsDefault(newGroup("core"));

        //then
        assertThat(coreRole, equalTo(coreRole2));
        assertThat(coreRole2, equalTo(coreRole));
        assertThat(coreRole.hashCode(), equalTo(coreRole2.hashCode()));

        assertThat(coreRole, equalTo(coreRole));
        assertThat(coreRole.hashCode(), equalTo(coreRole.hashCode()));
        assertThat(coreRole2, equalTo(coreRole2));
        assertThat(coreRole2.hashCode(), equalTo(coreRole2.hashCode()));

        assertThat(coreRole, not(equalTo(softwareRole)));
        assertThat(softwareRole, not(equalTo(coreRole)));

        assertThat(coreRole2, not(equalTo(softwareRole)));
        assertThat(softwareRole, not(equalTo(coreRole2)));
    }

    @Test
    public void equalityChecksGroups() {
        //given
        final ApplicationRole coreRole = ApplicationRole.forKey(ApplicationKeys.CORE)
                .withGroups(asGroups("one"), ImmutableSet.of());
        final ApplicationRole coreRole2 = ApplicationRole.forKey(ApplicationKeys.CORE)
                .withGroups(asGroups("one"), ImmutableSet.of());
        final ApplicationRole differentRole = ApplicationRole.forKey(ApplicationKeys.CORE)
                .withGroups(asGroups("two"), ImmutableSet.of());

        //then
        assertThat(coreRole, equalTo(coreRole2));
        assertThat(coreRole2, equalTo(coreRole));
        assertThat(coreRole.hashCode(), equalTo(coreRole2.hashCode()));

        assertThat(coreRole, equalTo(coreRole));
        assertThat(coreRole.hashCode(), equalTo(coreRole.hashCode()));
        assertThat(coreRole2, equalTo(coreRole2));
        assertThat(coreRole2.hashCode(), equalTo(coreRole2.hashCode()));

        assertThat(coreRole, not(equalTo(differentRole)));
        assertThat(differentRole, not(equalTo(coreRole)));

        assertThat(coreRole2, not(equalTo(differentRole)));
        assertThat(differentRole, not(equalTo(coreRole2)));
    }

    @Test
    public void equalsChecksDefaultGroups() {
        //given
        final ApplicationRole coreRole = ApplicationRole.forKey(ApplicationKeys.CORE)
                .withGroups(asGroups("one"), asGroups("one"));
        final ApplicationRole coreRole2 = ApplicationRole.forKey(ApplicationKeys.CORE)
                .withGroups(asGroups("one"), asGroups("one"));
        final ApplicationRole differentRole = ApplicationRole.forKey(ApplicationKeys.CORE)
                .withGroups(asGroups("one"), ImmutableSet.of());

        //then
        assertThat(coreRole, equalTo(coreRole2));
        assertThat(coreRole2, equalTo(coreRole));
        assertThat(coreRole.hashCode(), equalTo(coreRole2.hashCode()));

        assertThat(coreRole, equalTo(coreRole));
        assertThat(coreRole.hashCode(), equalTo(coreRole.hashCode()));
        assertThat(coreRole2, equalTo(coreRole2));
        assertThat(coreRole2.hashCode(), equalTo(coreRole2.hashCode()));

        assertThat(coreRole, not(equalTo(differentRole)));
        assertThat(differentRole, not(equalTo(coreRole)));

        assertThat(coreRole2, not(equalTo(differentRole)));
        assertThat(differentRole, not(equalTo(coreRole2)));
    }
}