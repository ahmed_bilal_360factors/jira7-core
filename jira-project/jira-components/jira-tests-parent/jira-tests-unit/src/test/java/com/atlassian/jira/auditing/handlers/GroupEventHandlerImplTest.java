package com.atlassian.jira.auditing.handlers;

import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.event.group.GroupMembershipCreatedEvent;
import com.atlassian.crowd.event.group.GroupMembershipDeletedEvent;
import com.atlassian.crowd.model.membership.MembershipType;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.local.runner.ListeningMockitoRunner;
import com.atlassian.jira.mock.component.MockComponentWorker;
import com.atlassian.jira.util.I18nHelper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.util.Locale;

import static org.mockito.Mockito.when;

/**
 * This test is a sanity check if we have all possible
 * MembershipTypes covered in our group handler.
 * In tests we loop though all MembershipTypes to check
 * if all of them are expected.
 *
 * @since v6.2
 */
@RunWith(ListeningMockitoRunner.class)
public class GroupEventHandlerImplTest {
    @Mock
    I18nHelper.BeanFactory beanFactory;

    @Mock
    I18nHelper i18nHelper;

    @Mock
    Directory directory;


    @Before
    public void setUp() throws Exception {
        final MockComponentWorker componentAccessorWorker = new MockComponentWorker();
        componentAccessorWorker.registerMock(I18nHelper.BeanFactory.class, beanFactory);
        ComponentAccessor.initialiseWorker(componentAccessorWorker);

        when(beanFactory.getInstance(Locale.ENGLISH)).thenReturn(i18nHelper);
    }

    @Test
    public void checkIfWeServeAllMembershipTypesWhenCreatingMembership() {
        for (MembershipType membershipType : MembershipType.values()) {
            final GroupMembershipCreatedEvent groupMembershipCreatedEvent = Mockito.mock(GroupMembershipCreatedEvent.class);
            when(groupMembershipCreatedEvent.getMembershipType()).thenReturn(membershipType);
            when(groupMembershipCreatedEvent.getDirectory()).thenReturn(directory);
            new GroupEventHandlerImpl().onGroupMembershipCreatedEvent(groupMembershipCreatedEvent);
        }
    }

    @Test
    public void checkIfWeServeAllMembershipTypesWhenDeletingMembership() {
        for (MembershipType membershipType : MembershipType.values()) {
            final GroupMembershipDeletedEvent groupMembershipDeletedEvent = Mockito.mock(GroupMembershipDeletedEvent.class);
            when(groupMembershipDeletedEvent.getMembershipType()).thenReturn(membershipType);
            when(groupMembershipDeletedEvent.getDirectory()).thenReturn(directory);
            new GroupEventHandlerImpl().onGroupMembershipDeletedEvent(groupMembershipDeletedEvent);
        }
    }


}
