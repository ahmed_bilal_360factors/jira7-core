package com.atlassian.jira.web.action.util.navigator;

import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.mock.servlet.MockHttpServletResponse;
import com.atlassian.jira.web.SessionKeys;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.mockito.Mock;
import org.mockito.Mockito;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * Simple test for {@link TestIssueNavigatorType}.
 *
 * @since v4.0
 */
public class TestIssueNavigatorType {
    @Rule
    public final RuleChain mockitoMocks = MockitoMocksInContainer.forTest(this);

    @Mock
    private HttpServletRequest mockHttpServletRequest;

    @Test
    public void testGetFromSession() {
        final Cookie cookie = new Cookie(SessionKeys.ISSUE_NAVIGATOR_TYPE, IssueNavigatorType.ADVANCED.name());
        Mockito.when(mockHttpServletRequest.getCookies()).thenReturn(new Cookie[]{cookie});

        assertEquals(IssueNavigatorType.ADVANCED, IssueNavigatorType.getFromCookie(mockHttpServletRequest));
    }

    @Test
    public void testGetFromSessionNoCookie() {
        Mockito.when(mockHttpServletRequest.getCookies()).thenReturn(new Cookie[]{});

        assertEquals(IssueNavigatorType.SIMPLE, IssueNavigatorType.getFromCookie(mockHttpServletRequest));
    }

    @Test
    public void testSetInSession() {
        final AtomicInteger count = new AtomicInteger(0);
        final AtomicBoolean advancedCalled = new AtomicBoolean(false);
        final AtomicBoolean simpleCalled = new AtomicBoolean(false);
        final MockHttpServletResponse mockHttpServletResponse = new MockHttpServletResponse() {
            @Override
            public void addCookie(final Cookie cookie) {
                if (count.get() == 0) {
                    advancedCalled.set(true);
                    assertEquals(IssueNavigatorType.ADVANCED.name(), cookie.getValue());
                    count.incrementAndGet();
                } else {
                    simpleCalled.set(true);
                    assertEquals(IssueNavigatorType.SIMPLE.name(), cookie.getValue());
                }
            }
        };

        //Make sure the tab is correctly updated in the session.
        IssueNavigatorType.setInCookie(mockHttpServletResponse, IssueNavigatorType.ADVANCED);

        //Make sure the tab is correctly updated in the session.
        IssueNavigatorType.setInCookie(mockHttpServletResponse, IssueNavigatorType.SIMPLE);

        assertTrue(advancedCalled.get());
        assertTrue(simpleCalled.get());
    }

    @Test
    public void testClearSession() {
        final Cookie cookie = new Cookie(SessionKeys.ISSUE_NAVIGATOR_TYPE, IssueNavigatorType.ADVANCED.name());
        Mockito.when(mockHttpServletRequest.getCookies()).thenReturn(new Cookie[]{cookie});

        //Make sure the tab is removed from the session.
        IssueNavigatorType.clearCookie(mockHttpServletRequest);
        assertNull(cookie.getValue());
    }

    @Test
    public void testGetTabForString() {
        assertEquals(IssueNavigatorType.ADVANCED, IssueNavigatorType.getTypeFromString("AdVaNcEd"));
        assertEquals(IssueNavigatorType.ADVANCED, IssueNavigatorType.getTypeFromString("advanced"));
        assertEquals(IssueNavigatorType.ADVANCED, IssueNavigatorType.getTypeFromString("ADVANCED"));

        assertEquals(IssueNavigatorType.SIMPLE, IssueNavigatorType.getTypeFromString("SiMpLe"));
        assertEquals(IssueNavigatorType.SIMPLE, IssueNavigatorType.getTypeFromString("simple"));
        assertEquals(IssueNavigatorType.SIMPLE, IssueNavigatorType.getTypeFromString("SIMPLE"));
    }

    /**
     * Should return null if the show
     */
    @Test
    public void testGetTabForStringBad() {
        assertNull(IssueNavigatorType.getTypeFromString("shos"));
        assertNull(IssueNavigatorType.getTypeFromString("shossw"));
        assertNull(IssueNavigatorType.getTypeFromString("shows"));
    }
}
