package com.atlassian.jira.application;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.cache.CacheManager;
import com.atlassian.cache.memory.MemoryCacheManager;
import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.crowd.embedded.spi.DirectoryDao;
import com.atlassian.crowd.exception.CrowdException;
import com.atlassian.crowd.manager.directory.SynchronisationStatusManager;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.config.CoreFeatures;
import com.atlassian.jira.config.database.DatabaseConfig;
import com.atlassian.jira.config.database.JndiDatasource;
import com.atlassian.jira.config.database.MockDatabaseConfigurationManager;
import com.atlassian.jira.crowd.embedded.ofbiz.InternalMembershipDao;
import com.atlassian.jira.crowd.embedded.ofbiz.MockDirectory;
import com.atlassian.jira.crowd.embedded.ofbiz.OfBizUserDao;
import com.atlassian.jira.crowd.embedded.ofbiz.db.DefaultOfBizTransactionManager;
import com.atlassian.jira.event.ClearCacheEvent;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.license.JiraLicenseManager;
import com.atlassian.jira.license.LicenseCountService;
import com.atlassian.jira.license.LicenseDetails;
import com.atlassian.jira.license.MockLicensedApplications;
import com.atlassian.jira.mock.MockFeatureManager;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.groups.MockGroupManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.ApplicationUsers;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.MockCrowdService;
import com.atlassian.jira.user.MockGroup;
import com.atlassian.jira.user.MockUser;
import com.atlassian.jira.user.util.DisabledRecoveryMode;
import com.atlassian.jira.user.util.MockUserManager;
import com.atlassian.jira.user.util.UserManager;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Sets;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import static com.atlassian.extras.common.LicensePropertiesConstants.UNLIMITED_USERS;
import static java.util.Collections.singleton;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyCollectionOf;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Tests {@link com.atlassian.jira.application.DefaultApplicationRoleManager#isAnyRoleLimitExceeded()}.
 *
 * @since 7.0
 */
public class DefaultApplicationRoleManagerUserLimitTest {
    private final ApplicationKey role1 = ApplicationKey.valueOf("com.atlassian.product.one");
    private final ApplicationKey role2 = ApplicationKey.valueOf("com.atlassian.product.two");
    private final ApplicationKey role3 = ApplicationKey.valueOf("com.atlassian.product.three");

    @Rule
    public MockitoContainer container = MockitoMocksInContainer.rule(this);

    @AvailableInContainer
    @Mock
    private LicenseCountService licenseCount;
    @Mock
    private JiraLicenseManager licenseManager;
    @Mock
    private EventPublisher eventPublisher;
    @Mock
    @AvailableInContainer
    private GlobalPermissionManager globalPermissionManager;
    @Mock
    private InternalMembershipDao internalMembershipDao;
    @Mock
    private DirectoryDao directoryDao;
    @Mock
    private SynchronisationStatusManager crowdSyncStatusManager;

    private final CacheManager cacheManager = new MemoryCacheManager();
    private final MockApplicationRoleStore store = new MockApplicationRoleStore();
    private final MockApplicationRoleDefinitions definitions = new MockApplicationRoleDefinitions();
    private final MockFeatureManager featureManager = new MockFeatureManager();
    private final MockGroupManager groupManager = new MockGroupManager();
    private MockCrowdService crowdService = new MockCrowdService();
    private MockUserManager userManager;
    private OfBizUserDao ofBizUserDao;

    private DefaultApplicationRoleManager manager;

    @Before
    public void setup() {
        final MockDatabaseConfigurationManager databaseConfigurationManager = new MockDatabaseConfigurationManager();
        databaseConfigurationManager.setDatabaseConfiguration(new DatabaseConfig("postgres", "jira", new JndiDatasource("jira")));
        ofBizUserDao = new OfBizUserDao(null, directoryDao, null, null, null, cacheManager, null, null, new DefaultOfBizTransactionManager(), databaseConfigurationManager) {
            @Override
            public boolean useFullCache() {
                return false;
            }

            @Override
            public void processUsers(Consumer userProcessor) {
                userManager.getAllUsers().forEach(appUser -> userProcessor.accept(appUser.getDirectoryUser()));
            }
        };

        featureManager.enable(CoreFeatures.LICENSE_ROLES_ENABLED);
        userManager = new MockUserManager(crowdService);
        userManager.alwaysReturnUsers();
        userManager.useCrowdServiceToGetUsers();
        container.getMockWorker().addMock(UserManager.class, userManager);

        manager = new DefaultApplicationRoleManager(cacheManager, store, definitions, groupManager, licenseManager,
                new DisabledRecoveryMode(), crowdService, eventPublisher,
                internalMembershipDao, directoryDao, ofBizUserDao, new DefaultOfBizTransactionManager(),
                crowdSyncStatusManager, featureManager);

        when(directoryDao.findAll()).thenReturn(ImmutableList.of(new MockDirectory(MockUser.MOCK_DIRECTORY_ID)));
        when(internalMembershipDao.findGroupChildrenOfGroups(anyLong(), anyCollectionOf(String.class))).thenReturn(Collections.emptyList());
        when(internalMembershipDao.findUserChildrenOfGroups(anyLong(), anyCollectionOf(String.class))).thenAnswer(invocation ->
        {
            @SuppressWarnings("unchecked") Collection<String> groupNames = invocation.getArgumentAt(1, Collection.class);
            List<Group> groups = groupNames.stream().map(MockGroup::new).collect(Collectors.toList());
            return groupManager.getUserNamesInGroups(groups);

        });

        definitions.addDefined(ApplicationKeys.CORE.value(), "CORE");
        definitions.addLicensed(ApplicationKeys.CORE.value(), "CORE");
    }

    @Test
    public void roleLimitIsNotExceededWhenNotInstalled() {
        given(installedApplications(role1),
                seatsGrantedByLicense(role1, 1),
                currentUserCountByRole(role1, 1));

        assertFalse(manager.isRoleLimitExceeded(role3));
    }

    @Test
    public void roleLimitIsNotExceededWhenUnlimitedUsers() {
        given(installedApplications(role1),
                seatsGrantedByLicense(role1, UNLIMITED_USERS),
                currentUserCountByRole(role1, 10000));

        assertFalse(manager.isRoleLimitExceeded(role1));
    }

    @Test
    public void roleLimitIsNotExceededWhenUserCountLessThanLicense() {
        given(installedApplications(role1),
                seatsGrantedByLicense(role1, 10),
                currentUserCountByRole(role1, 9));

        assertFalse(manager.isRoleLimitExceeded(role1));
    }

    @Test
    public void roleLimitIsNotExceededWhenUserCountEqualToLicense() {
        given(installedApplications(role1),
                seatsGrantedByLicense(role1, 10),
                currentUserCountByRole(role1, 10));

        assertFalse(manager.isRoleLimitExceeded(role1));
    }

    @Test
    public void roleLimitIsExceededWhenUserCountGreaterThanLicense() {
        given(installedApplications(role1),
                seatsGrantedByLicense(role1, 10),
                currentUserCountByRole(role1, 11));

        assertTrue(manager.isRoleLimitExceeded(role1));
    }

    @Test
    public void instanceRoleLimitIsNotExceededWhenAllRolesHaveSeatsRemaining() {
        given(installedApplications(role1, role2, role3),
                seatsGrantedByLicense(role1, 11, role2, 22, role3, 33),
                currentUserCountByRole(role1, 10, role2, 20, role3, 30));

        assertFalse(manager.isAnyRoleLimitExceeded());
    }

    @Test
    public void instanceRoleLimitIsNotExceededWhenRolesAreEqualToSeats() {
        given(installedApplications(role1, role2, role3),
                seatsGrantedByLicense(role1, 11, role2, 22, role3, 33),
                currentUserCountByRole(role1, 11, role2, 22, role3, 33));

        assertFalse(manager.isAnyRoleLimitExceeded());
    }

    @Test
    public void instanceRoleLimitIsNotExceededWhenAllRolesAreUnlimited() {
        given(installedApplications(role1, role2, role3),
                seatsGrantedByLicense(role1, UNLIMITED_USERS, role2, UNLIMITED_USERS, role3, UNLIMITED_USERS),
                currentUserCountByRole(role1, 1000, role2, 1000, role3, 1000));

        assertFalse(manager.isAnyRoleLimitExceeded());
    }

    @Test
    public void instanceRoleLimitIsNotExceededWhenSomeRolesAreUnlimited() {
        given(installedApplications(role1, role2, role3),
                seatsGrantedByLicense(role1, UNLIMITED_USERS, role2, 10, role3, 10),
                currentUserCountByRole(role1, 1000, role2, 1, role3, 1));

        assertFalse(manager.isAnyRoleLimitExceeded());
    }

    @Test
    public void instanceRoleLimitIsExceededWhenAnyRoleExceeded() {
        given(installedApplications(role1, role2, role3),
                seatsGrantedByLicense(role1, 10, role2, 10, role3, 10),
                currentUserCountByRole(role1, 11, role2, 9, role3, 9));

        assertTrue(manager.isAnyRoleLimitExceeded());
    }

    @Test
    public void instanceRoleLimitIsExceededWhenAllRolesExceeded() {
        given(installedApplications(role1, role2, role3),
                seatsGrantedByLicense(role1, 10, role2, 10, role3, 10),
                currentUserCountByRole(role1, 11, role2, 11, role3, 11));

        assertTrue(manager.isAnyRoleLimitExceeded());
    }

    @Test
    public void instanceRoleLimitIsExceededWhenRoleIsExceededEvenWhenNotInstalled() {
        given(installedApplications(role1),
                seatsGrantedByLicense(role1, 10, role2, 10, role3, 10),
                currentUserCountByRole(role1, 1, role2, 1000, role3, 1000));

        assertTrue(manager.isAnyRoleLimitExceeded());
    }

    @Test
    public void instanceRoleLimitIsNotExceededWhenThereAreRolesButNoApplicationsInstalled() {
        given(installedApplications(/* none */),
                seatsGrantedByLicense(role1, 10, role2, 10, role3, 10),
                currentUserCountByRole(role1, 1));

        assertFalse(manager.isAnyRoleLimitExceeded());
    }

    @Test
    public void instanceRoleLimitIsNotExceededWhenRoleIsInstalledButNotUsed() {
        given(installedApplications(role1, role2),
                seatsGrantedByLicense(role1, 10, role2, 10),
                currentUserCountByRole(role1, 1));

        assertFalse(manager.isAnyRoleLimitExceeded());
    }

    @Test
    public void instanceRoleLimitIsNotExceededWhenAnInstalledRoleHasNoSeatsGranted() {
        given(installedApplications(role1, role2),
                seatsGrantedByLicense(role2, 10, role3, 10),
                currentUserCountByRole(role2, 1, role3, 1));

        assertFalse(manager.isAnyRoleLimitExceeded());
    }

    @Test
    public void instanceRoleLimitIsNotExceededWhenAllRolesInstalledHaveNoSeatsGranted() {
        given(installedApplications(role1, role2),
                seatsGrantedByLicense(role3, 10),
                currentUserCountByRole(role3, 1));

        assertFalse(manager.isAnyRoleLimitExceeded());
    }

    @Test
    public void userLimitIsNotExceededWhenUsersRolesAreNotExceeded()
            throws CrowdException {
        given(installedApplications(role1, role2, role3),
                seatsGrantedByLicense(role1, 10, role2, 10, role3, 10),
                currentUserCountByRole(role1, 1, role2, 1, role3, 1));

        ApplicationUser alice = new MockApplicationUser("Alice");
        crowdService.addUser(alice);
        addUserToRole(alice, role1);
        addUserToRole(alice, role2);

        assertEquals(2, manager.getUserCount(role1));
        assertEquals(2, manager.getUserCount(role2));
        assertEquals(1, manager.getUserCount(role3));

        assertFalse(manager.hasExceededAllRoles(alice));
    }

    @Test
    public void userLimitIsExceededWhenUsersRolesAreExceeded()
            throws CrowdException {
        given(installedApplications(role1, role2, role3),
                seatsGrantedByLicense(role1, 10, role2, 10, role3, 10),
                currentUserCountByRole(role1, 10, role2, 10, role3, 10));

        assertFalse(manager.isAnyRoleLimitExceeded());

        ApplicationUser alice = new MockApplicationUser("Alice");
        crowdService.addUser(alice);
        addUserToRole(alice, role1);
        addUserToRole(alice, role2);

        assertEquals(11, manager.getUserCount(role1));
        assertEquals(11, manager.getUserCount(role2));
        assertEquals(10, manager.getUserCount(role3));

        assertTrue(manager.hasExceededAllRoles(alice));
    }

    @Test
    public void userLimitIsNotExceededWhenNotAllUsersRolesAreExceeded()
            throws CrowdException {
        given(installedApplications(role1, role2, role3),
                seatsGrantedByLicense(role1, 10, role2, 10, role3, 10),
                currentUserCountByRole(role1, 5, role2, 10, role3, 10));

        assertFalse(manager.isAnyRoleLimitExceeded());

        ApplicationUser alice = new MockApplicationUser("Alice");
        crowdService.addUser(alice);
        addUserToRole(alice, role1);
        addUserToRole(alice, role2);
        addUserToRole(alice, role3);

        assertEquals(6, manager.getUserCount(role1));
        assertEquals(11, manager.getUserCount(role2));
        assertEquals(11, manager.getUserCount(role3));

        assertFalse(manager.hasExceededAllRoles(alice));
    }

    @Test
    public void userLimitIsExceededWhenUserHasNoRoles() {
        given(installedApplications(role1, role2, role3),
                seatsGrantedByLicense(role1, 10, role2, 10, role3, 10),
                currentUserCountByRole(role1, 5, role2, 10, role3, 10));

        assertTrue(manager.hasExceededAllRoles(new MockApplicationUser("Alice")));
    }

    private void given(final Iterable<ApplicationKey> installedApplications,
                       final Map<ApplicationKey, Integer> seatsGrantedByLicense,
                       final Map<ApplicationKey, Integer> currentUserCountByRole) {
        // Add all the installed application roles.
        for (ApplicationKey key : installedApplications) {
            definitions.addDefined(key.value(), key.value());
        }

        for (ApplicationKey key : seatsGrantedByLicense.keySet()) {
            definitions.addLicensed(key.value(), key.value());
        }

        // Add the license to JIRA.
        final LicenseDetails mock = mock(LicenseDetails.class);
        when(licenseManager.getLicenses()).thenReturn(singleton(mock));
        when(mock.getLicensedApplications()).thenReturn(new MockLicensedApplications(seatsGrantedByLicense));
        when(licenseManager.getAllLicensedApplicationKeys()).thenReturn(Sets.newHashSet(seatsGrantedByLicense.keySet()));

        // Add the users to JIRA.
        for (Map.Entry<ApplicationKey, Integer> entry : currentUserCountByRole.entrySet()) {
            final ApplicationKey id = entry.getKey();
            final Integer count = entry.getValue();
            String group = "group-" + id.value();

            groupManager.addGroup(group);

            store.save(new MockApplicationRole().key(id).name(id.value()).groupNames(group));

            for (int i = 0; i < count; i++) {
                String user = String.format("user-of-%s-%d", group, i);
                groupManager.addMember(group, user);
                try {
                    MockApplicationUser crowdUser = new MockApplicationUser(user);
                    MockGroup crowdGroup = new MockGroup(group);
                    crowdService.addUser(crowdUser);
                    crowdService.addUserToGroup(crowdUser, crowdGroup);
                } catch (CrowdException e) {
                    throw new RuntimeException(e);
                }
            }
        }
    }

    private void addUserToRole(ApplicationUser user, ApplicationKey role) {
        Group firstGroup = manager.getRole(role).get().getGroups().iterator().next();
        groupManager.addUserToGroup(user, firstGroup);
        crowdService.addUserToGroup(user, firstGroup);

        // give the cache a poke
        manager.onClearCache(ClearCacheEvent.INSTANCE);
    }

    private static List<ApplicationKey> installedApplications(ApplicationKey... ids) {
        return ImmutableList.copyOf(ids);
    }

    private static Map<ApplicationKey, Integer> seatsGrantedByLicense(final ApplicationKey role1, final int count1,
                                                                      final ApplicationKey role2, final int count2, final ApplicationKey role3, final int count3) {
        return ImmutableMap.of(role1, count1, role2, count2, role3, count3);
    }

    private static Map<ApplicationKey, Integer> seatsGrantedByLicense(final ApplicationKey role1, final int count1,
                                                                      final ApplicationKey role2, final int count2) {
        return ImmutableMap.of(role1, count1, role2, count2);
    }

    private static Map<ApplicationKey, Integer> seatsGrantedByLicense(final ApplicationKey role, final int count) {
        return ImmutableMap.of(role, count);
    }

    private static Map<ApplicationKey, Integer> currentUserCountByRole(final ApplicationKey role1, final int count1,
                                                                       final ApplicationKey role2, final int count2, final ApplicationKey role3, final int count3) {
        return ImmutableMap.of(role1, count1, role2, count2, role3, count3);
    }

    private static Map<ApplicationKey, Integer> currentUserCountByRole(final ApplicationKey role1, final int count1,
                                                                       final ApplicationKey role2, final int count2) {
        return ImmutableMap.of(role1, count1, role2, count2);
    }

    private static Map<ApplicationKey, Integer> currentUserCountByRole(final ApplicationKey role, final int count) {
        return ImmutableMap.of(role, count);
    }

    private MockUser setupMockUser(String name, Group... groups) {
        MockUser user = new MockUser(name);
        crowdService.addUser(user, null);

        for (Group group : groups) {
            crowdService.addUserToGroup(user, group);
            groupManager.addUserToGroup(ApplicationUsers.from(user), group);
        }
        return user;
    }

    private Group setupGroup(String name) {
        final Group group = new MockGroup(name);
        crowdService.addGroup(group);
        return group;
    }
}
