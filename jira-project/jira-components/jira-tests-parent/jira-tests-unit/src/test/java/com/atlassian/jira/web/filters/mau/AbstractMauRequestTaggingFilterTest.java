package com.atlassian.jira.web.filters.mau;

import java.io.IOException;
import java.util.Optional;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletRequest;

import com.atlassian.jira.event.mau.MauApplicationKey;
import com.atlassian.jira.event.mau.MauEventService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class AbstractMauRequestTaggingFilterTest {
    @Mock
    private HttpServletRequest request;

    @Mock
    private HttpServletResponse response;

    @Mock
    private FilterChain filterChain;

    @Mock
    private MauEventService mauEventService;

    private AbstractMauRequestTaggingFilter taggingFilter;

    @Before
    public void setUp() {
        taggingFilter = new ExampleMauRequestTaggingFilter(mauEventService);
    }

    @Test
    public void testTaggingFilterSetsApplicationKeyAsFamily() throws ServletException, IOException {
        taggingFilter.doFilter(request, response, filterChain);
        verify(mauEventService, times(1)).setApplicationForThread(any(MauApplicationKey.class));
    }

    @Test
    public void testTaggingFilterDoesNothingIfCannotGetMauEventService() throws ServletException, IOException {
        taggingFilter = new ExampleMauRequestTaggingFilter(null);
        taggingFilter.doFilter(request, response, filterChain);
        verify(mauEventService, never()).setApplicationForThread(any(MauApplicationKey.class));
    }

    @Test
    public void testTaggingFilterCallsNextFilterInChain()  throws ServletException, IOException {
        taggingFilter.doFilter(request, response, filterChain);
        verify(filterChain, times(1)).doFilter(eq(request), eq(response));
    }

    @Test
    public void testTaggingFilterCallsNextFilterInChainDespiteException()  throws ServletException, IOException {
        taggingFilter = new ExampleMauRequestTaggingFilter(mauEventService) {
            @Override
            public void tagRequest(final MauEventService mauEventService, final HttpServletRequest request) {
                throw new RuntimeException("Catastrophic failure..");
            }
        };

        taggingFilter.doFilter(request, response, filterChain);
        verify(filterChain, times(1)).doFilter(eq(request), eq(response));
    }

    private static class ExampleMauRequestTaggingFilter extends AbstractMauRequestTaggingFilter {
        private MauApplicationKey applicationKey = MauApplicationKey.family();
        private MauEventService mauEventService;

        ExampleMauRequestTaggingFilter(final MauEventService mauEventService) {
            this.mauEventService = mauEventService;
        }

        @Override
        public void tagRequest(final MauEventService mauEventService, final HttpServletRequest request)
        {
            mauEventService.setApplicationForThread(applicationKey);
        }

        @Override
        Optional<MauEventService> getMauEventService() {
            return Optional.ofNullable(mauEventService);
        }
    }
}