package com.atlassian.jira.tenancy;

import com.atlassian.jira.bc.ServiceResult;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockComponentContainer;
import com.atlassian.jira.util.johnson.DefaultJohnsonProvider;
import com.atlassian.jira.util.johnson.JohnsonProvider;
import com.atlassian.johnson.Johnson;
import com.atlassian.tenancy.api.Tenant;
import com.google.common.collect.Lists;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @since v7.1
 */
@RunWith(MockitoJUnitRunner.class)
public class TestDefaultTenantManager {
    @Rule
    public MockComponentContainer container = new MockComponentContainer(this);
    @Mock
    ApplicationProperties applicationProperties;
    @Mock
    TenancyCondition tenancyCondition;
    @Mock
    JiraTenantAccessor jiraTenantAccessor;
    @Rule
    public ExpectedException thrown = ExpectedException.none();
    @AvailableInContainer
    JohnsonProvider johnsonProvider = new DefaultJohnsonProvider();

    private TenantManager tenantManager;

    @Before
    public void setup() {
        Johnson.initialize("test-johnson-config.xml");
        tenantManager = new DefaultTenantManager(applicationProperties, tenancyCondition, jiraTenantAccessor);
    }

    @After
    public void tearDown() {
        Johnson.terminate();
    }

    @Test
    public void taskAreRunImmediatelyIfTenancyConditionDisabled() {
        when(tenancyCondition.isEnabled()).thenReturn(false);
        final Runnable runMe = mock(Runnable.class);
        tenantManager.doNowOrWhenTenantArrives(runMe, "description");
        verify(runMe).run();
    }

    @Test
    public void taskAreDelayedIfNoTenant() {
        when(tenancyCondition.isEnabled()).thenReturn(true);
        when(jiraTenantAccessor.getAvailableTenants()).thenReturn(Collections.emptyList());
        final Runnable runMe = mock(Runnable.class);
        tenantManager.doNowOrWhenTenantArrives(runMe, "description");
        verify(runMe, never()).run();
    }

    @Test
    public void voidTaskOnlyRunsOneEvenIfTenantArrivedIsHitTwice() {
        when(tenancyCondition.isEnabled()).thenReturn(true);
        when(jiraTenantAccessor.getAvailableTenants()).thenReturn(Collections.emptyList());
        final Runnable runMe = mock(Runnable.class);
        tenantManager.doNowOrWhenTenantArrives(runMe, "description");
        ServiceResult result1 = tenantManager.tenantArrived();
        ServiceResult result2 = tenantManager.tenantArrived();
        verify(runMe, times(1)).run();
        assertTrue("tenantArrived did not succed when expected to", result1.isValid());
        assertFalse("tenantArrived succeded on second execution when it should fail", result2.isValid());
        assertEquals("Instance is already tenanted, ignoring tenant arrival notification.",
                result2.getErrorCollection().getErrorMessages().iterator().next());
    }

    @Test
    public void taskRunsImmediatelyIfTenancyConditionEnabledAndTenantAlreadyArrived() {
        when(tenancyCondition.isEnabled()).thenReturn(true);
        tenantManager.tenantArrived();
        when(jiraTenantAccessor.getAvailableTenants()).thenReturn(Lists.newArrayList(mock(Tenant.class)));
        final Runnable runMe = mock(Runnable.class);
        tenantManager.doNowOrWhenTenantArrives(runMe, "description");
        verify(runMe).run();
    }

    @Test
    public void delayedTaskAreRunWhenTenantArrives() {
        when(tenancyCondition.isEnabled()).thenReturn(true);
        when(jiraTenantAccessor.getAvailableTenants()).thenReturn(Collections.emptyList());
        final Runnable runMe = mock(Runnable.class);
        tenantManager.doNowOrWhenTenantArrives(runMe, "description");
        ServiceResult result = tenantManager.tenantArrived();
        verify(runMe).run();
        assertTrue("Tenant arrived failed when expected to succeed", result.isValid());
        assertFalse("Unexpected errors found " + result.getErrorCollection().getErrors(),
                result.getErrorCollection().hasAnyErrors());
    }

    @Test
    public void isTenantedWhenJiraBaseUrlIsSet() throws Exception {
        when(tenancyCondition.isEnabled()).thenReturn(true);
        when(applicationProperties.getDefaultBackedText(APKeys.JIRA_BASEURL)).thenReturn("http://baseurl/jira");
        tenantManager.afterInstantiation();
        verify(jiraTenantAccessor).addTenant(anyObject());
    }

    @Test
    public void taskExceptionsArePropagatedWhenTenancyDisabled() {
        when(tenancyCondition.isEnabled()).thenReturn(false);
        final Runnable runMe = mock(Runnable.class);
        Exception e = new RuntimeException("Task failed!");
        doThrow(e).when(runMe).run();
        thrown.expect(equalTo(e));
        tenantManager.doNowOrWhenTenantArrives(runMe, "description");
        verify(runMe).run();
    }

    @Test
    public void taskExceptionsAreLoggedIntoJohnsonWhenTenancyEnabled() {
        when(tenancyCondition.isEnabled()).thenReturn(true);
        when(jiraTenantAccessor.getAvailableTenants()).thenReturn(Collections.emptyList());

        final Runnable runMe = mock(Runnable.class);
        Exception e = new RuntimeException("Task failed!");
        doThrow(e).when(runMe).run();

        tenantManager.doNowOrWhenTenantArrives(runMe, "description");
        ServiceResult result = tenantManager.tenantArrived();
        verify(runMe).run();
        assertEquals("Unexpected exception when tenant arrived. This JIRA instance will not be able to recover. Please check the logs for details.",
                result.getErrorCollection().getErrorMessages().iterator().next());
    }

    @Test
    public void tenantArrivedFailsIfTenancyNotEnabled() {
        when(tenancyCondition.isEnabled()).thenReturn(false);
        ServiceResult result = tenantManager.tenantArrived();
        assertFalse("Tenant arrived should fail if tenancy is disabled", result.isValid());
        assertEquals("Instance tenanting is NOT enabled, ignoring instance tenanting request.",
                result.getErrorCollection().getErrorMessages().iterator().next());
    }

    @Test
    public void taskShouldNotBeSetTwice() {
        when(tenancyCondition.isEnabled()).thenReturn(true);
        when(jiraTenantAccessor.getAvailableTenants()).thenReturn(Collections.emptyList());
        final Runnable runMe = mock(Runnable.class);
        tenantManager.doNowOrWhenTenantArrives(runMe, "description");
        thrown.expect(IllegalStateException.class);
        tenantManager.doNowOrWhenTenantArrives(runMe, "description");
    }
}
