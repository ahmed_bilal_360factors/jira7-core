package com.atlassian.jira.imports.project.handler;

import com.atlassian.jira.exception.ParseException;
import com.atlassian.jira.imports.project.taskprogress.EntityTypeTaskProgressProcessor;
import com.atlassian.jira.imports.project.taskprogress.TaskProgressProcessor;
import com.atlassian.jira.imports.project.taskprogress.ThrottlingTaskProgressProcessor;
import com.atlassian.jira.junit.rules.InitMockitoMocks;
import com.atlassian.jira.task.TaskProgressSink;
import com.atlassian.jira.web.bean.MockI18nBean;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.TestRule;
import org.mockito.Mockito;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.io.StringReader;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @since v3.13
 */
public class TestChainedOfBizSaxHandler {
    @Rule
    public TestRule initMockitoMocks = new InitMockitoMocks(this);

    @Rule
    public ExpectedException exception = ExpectedException.none();

    public static final String DESCRIPTION = "description\n" + "\n" + "that \n" + "\n" + "spans \n" + "\n" + "many \n" + "\n" + "lines";

    public static final String ENVIRONMENT = "environment\n" + "\n" + "that \n" + "\n" + "spans \n" + "\n" + "many \n" + "\n" + "lines";

    public static final String XML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" + "<entity-engine-xml>\n" + "    <Action id=\"10000\" issue=\"10000\" author=\"admin\" type=\"comment\" created=\"2008-01-10 16:16:40.55\" updateauthor=\"admin\" updated=\"2008-01-10 16:16:40.55\">\n" + "        <body><![CDATA[I am a comment\n" + "]]></body>\n" + "    </Action>" + "    <Issue id=\"10000\" key=\"MKY-1\" project=\"10001\" reporter=\"fred\" assignee=\"admin\" type=\"1\" summary=\"test\" priority=\"3\" status=\"1\" created=\"2008-01-07 16:24:43.46\" updated=\"2008-01-24 16:03:39.836\" votes=\"0\" timeestimate=\"0\" timespent=\"72000\" workflowId=\"10000\" security=\"10001\">\n" + "        <description><![CDATA[" + DESCRIPTION + "]]></description>\n" + "        <environment><![CDATA[" + ENVIRONMENT + "]]></environment>\n"

            + "    </Issue>" + "<Project id=\"10001\" name=\"monkey\" lead=\"admin\" description=\"project for monkeys\" key=\"MKY\" counter=\"1\" assigneetype=\"2\"/>" + "</entity-engine-xml>";

    @Test
    public void testHandlerWithRealXml() throws ParserConfigurationException, SAXException, IOException {
        final ChainedOfBizSaxHandler handler = new ChainedOfBizSaxHandler();
        final SAXParserFactory factory = SAXParserFactory.newInstance();
        final SAXParser saxParser = factory.newSAXParser();
        final InputSource inputSource = new InputSource(new StringReader(XML));
        // Scan the XML
        saxParser.parse(inputSource, handler);

        // Validate we got what we were after
        assertEquals(3, handler.getEntityCount());
    }

    @Test
    public void testHandlerWithRealXmlAndTaskSink() throws ParserConfigurationException, SAXException, IOException {
        final AtomicBoolean actionCalled = new AtomicBoolean(false);
        final AtomicBoolean issueCalled = new AtomicBoolean(false);
        final AtomicBoolean projectCalled = new AtomicBoolean(false);
        final TaskProgressSink taskProgressSink = (taskProgress, currentSubTask, message) -> {
            if ("Processing Action".equals(currentSubTask)) {
                actionCalled.set(true);
                assertEquals(0, taskProgress);
            }
            if ("Processing Issue".equals(currentSubTask)) {
                issueCalled.set(true);
                assertEquals(33, taskProgress);
            }
            if ("Processing Project".equals(currentSubTask)) {
                projectCalled.set(true);
                assertEquals(66, taskProgress);
            }
        };
        final TaskProgressProcessor taskProgressProcessor = new EntityTypeTaskProgressProcessor(3, taskProgressSink, new MockI18nBean());
        final ThrottlingTaskProgressProcessor projectWriterMock = mock(ThrottlingTaskProgressProcessor.class);
        Mockito.doAnswer(invocation -> {
            taskProgressProcessor.processTaskProgress(
                    invocation.getArgumentAt(0, String.class),
                    invocation.getArgumentAt(1, Integer.class),
                    invocation.getArgumentAt(2, Long.class),
                    invocation.getArgumentAt(3, Long.class));
            return new Object();
        }).when(projectWriterMock).processTaskProgress(any(), anyInt(), anyLong(), anyLong());
        final ChainedOfBizSaxHandler handler = new ChainedOfBizSaxHandler(projectWriterMock);
        final SAXParserFactory factory = SAXParserFactory.newInstance();
        final SAXParser saxParser = factory.newSAXParser();
        final InputSource inputSource = new InputSource(new StringReader(XML));
        // Scan the XML
        saxParser.parse(inputSource, handler);

        // Validate we got what we were after
        assertEquals(3, handler.getEntityCount());

        // Validate we got what we were after from the make progress
        assertTrue(actionCalled.get());
        assertTrue(issueCalled.get());
        assertTrue(projectCalled.get());
    }

    @Test
    public void testHandlerNoRoot() throws SAXException {
        final ChainedOfBizSaxHandler handler = new ChainedOfBizSaxHandler();

        // Try to start an element before we reach the root
        exception.expect(SAXException.class);
        handler.startElement(null, null, "Issue", null);

        // Try to end an element before we reach the root
        exception.expect(SAXException.class);
        handler.endElement(null, null, "Issue");

        final Attributes mockTopLevelAttributes = mock(Attributes.class);
        when(mockTopLevelAttributes.getLength()).thenReturn(0, 1);

        exception.expect(SAXException.class);
        handler.startDocument();
        handler.startElement(null, null, "entity-engine-xml", null);
        handler.startElement(null, null, "TestEntity", mockTopLevelAttributes);
        handler.endDocument();
    }

    @Test
    public void testInEndElementWithoutStartElement() {
        final ChainedOfBizSaxHandler handler = new ChainedOfBizSaxHandler() {
            protected void handleEntity(final String entityName, final Map attributes) throws ParseException {
            }
        };

        try {
            handler.startDocument();
            handler.startElement(null, null, "entity-engine-xml", null);
            handler.endElement(null, null, "TestEntity");
            fail("Should throw a SAX exception for bad closing");
        } catch (final SAXException e) {
            // expected
        }
    }

    @Test
    public void testHandleEntityThrowsParseException() {
        final ChainedOfBizSaxHandler handler = new ChainedOfBizSaxHandler();
        handler.registerHandler(new ImportOfBizEntityHandler() {
            public void handleEntity(final String entityName, final Map<String, String> attributes)
                    throws ParseException {
                throw new ParseException("Test message");
            }

            public void startDocument() {
            }

            public void endDocument() {
            }
        });

        try {
            final Attributes mockTopLevelAttributes = mock(Attributes.class);

            when(mockTopLevelAttributes.getLength()).thenReturn(0, 1);

            handler.startDocument();
            handler.startElement(null, null, "entity-engine-xml", null);
            handler.startElement(null, null, "TestEntity", mockTopLevelAttributes);
            handler.endElement(null, null, "TestEntity");
            fail("Should throw a SAX exception from handler");
        } catch (final SAXException e) {
            // expected
            assertTrue(e.getException() instanceof ParseException);
        }
    }

    @Test
    public void testHandlerDelegatesRealXML()
            throws ParseException, ParserConfigurationException, SAXException, IOException {
        final ChainedOfBizSaxHandler chainedOfBizSaxHandler = new ChainedOfBizSaxHandler();
        final TestHandler testHandler1 = new TestHandler();
        final TestHandler testHandler2 = new TestHandler();
        chainedOfBizSaxHandler.registerHandler(testHandler1);
        chainedOfBizSaxHandler.registerHandler(testHandler2);

        final SAXParserFactory factory = SAXParserFactory.newInstance();
        final SAXParser saxParser = factory.newSAXParser();
        final InputSource inputSource = new InputSource(new StringReader(TestChainedOfBizSaxHandler.XML));
        // Scan the XML
        saxParser.parse(inputSource, chainedOfBizSaxHandler);

        // Validate we got what we were after
        assertEquals(3, testHandler1.entityMapAndValues.size());
        assertTrue(testHandler1.entityMapAndValues.containsKey("Action"));
        assertTrue(testHandler1.entityMapAndValues.containsKey("Issue"));
        assertTrue(testHandler1.entityMapAndValues.containsKey("Project"));
        assertEquals(8, ((Map) testHandler1.entityMapAndValues.get("Action")).size());
        assertEquals(18, ((Map) testHandler1.entityMapAndValues.get("Issue")).size());
        assertEquals(TestChainedOfBizSaxHandler.DESCRIPTION, ((Map) testHandler1.entityMapAndValues.get("Issue")).get("description"));
        assertEquals(TestChainedOfBizSaxHandler.ENVIRONMENT, ((Map) testHandler1.entityMapAndValues.get("Issue")).get("environment"));

        assertEquals(3, testHandler2.entityMapAndValues.size());
        assertTrue(testHandler2.entityMapAndValues.containsKey("Action"));
        assertTrue(testHandler2.entityMapAndValues.containsKey("Issue"));
        assertTrue(testHandler2.entityMapAndValues.containsKey("Project"));
        assertEquals(8, ((Map) testHandler2.entityMapAndValues.get("Action")).size());
        assertEquals(18, ((Map) testHandler2.entityMapAndValues.get("Issue")).size());
        assertEquals(TestChainedOfBizSaxHandler.DESCRIPTION, ((Map) testHandler2.entityMapAndValues.get("Issue")).get("description"));
        assertEquals(TestChainedOfBizSaxHandler.ENVIRONMENT, ((Map) testHandler2.entityMapAndValues.get("Issue")).get("environment"));
    }

    private class TestHandler implements ImportOfBizEntityHandler {
        public Map entityMapAndValues = new HashMap();

        public void handleEntity(final String entityName, final Map<String, String> attributes) throws ParseException {
            entityMapAndValues.put(entityName, attributes);
        }

        public void startDocument() {
        }

        public void endDocument() {
        }
    }
}
