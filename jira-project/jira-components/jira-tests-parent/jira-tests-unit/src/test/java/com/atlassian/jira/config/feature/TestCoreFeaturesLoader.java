package com.atlassian.jira.config.feature;

import com.atlassian.fugue.Option;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.config.properties.JiraProperties;
import com.atlassian.jira.io.ClassPathResourceLoader;
import com.atlassian.jira.io.ResourceLoader;
import com.atlassian.jira.junit.rules.Log4jLogger;
import com.atlassian.jira.util.RuntimeIOException;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.elements.ResourceDescriptor;
import com.google.common.collect.ImmutableList;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.MethodRule;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.URL;
import java.util.Properties;

import static com.atlassian.jira.config.feature.CoreFeaturesLoader.CORE_FEATURES_RESOURCE;
import static com.atlassian.jira.config.feature.CoreFeaturesLoader.FEATURE_RESOURCE_TYPE;
import static com.atlassian.jira.matchers.OptionMatchers.some;
import static com.atlassian.sal.api.features.DarkFeatureManager.DARKFEATURES_PROPERTIES_FILE_PROPERTY;
import static com.atlassian.sal.api.features.DarkFeatureManager.DARKFEATURES_PROPERTIES_FILE_PROPERTY_DEFAULT;
import static java.lang.String.format;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.anything;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.junit.internal.matchers.ThrowableMessageMatcher.hasMessage;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Tests for feature loader.
 */
public class TestCoreFeaturesLoader {

    public static final String PROPERTY_NAME = "myProperty";
    public static final String PLUGIN_RESOURCE_LOCATION = "justLocation";
    private static final String SYSTEM_PROPERTY_NAME = FeatureManager.SYSTEM_PROPERTY_PREFIX + PROPERTY_NAME;
    @Rule
    public final MethodRule initMockito = MockitoJUnit.rule();
    @Rule
    public final ExpectedException expectedException = ExpectedException.none();
    @Rule
    public final Log4jLogger log4jLogger = new Log4jLogger();
    @Mock
    private JiraProperties jiraSystemProperties;
    @Mock
    private ResourceLoader resourceLoader;
    @Mock
    private PluginAccessor pluginAccessor;

    @InjectMocks
    private CoreFeaturesLoader coreFeaturesLoader;

    @Before
    public void setUp() throws Exception {
        when(jiraSystemProperties.getProperties()).thenReturn(new Properties());
    }

    @Test
    public void shouldLoadCorePropertiesAndOverrideSalAndCoreWithSystem() throws Exception {
        prepareSalProperty(PROPERTY_NAME, "salValue");
        prepareCoreProperty(PROPERTY_NAME, "coreValue");
        prepareSystemProperty(SYSTEM_PROPERTY_NAME, "systemValue");

        final Properties properties = coreFeaturesLoader.loadCoreProperties();

        assertThat(properties, hasEntry("myProperty", "systemValue"));
    }


    @Test
    public void shouldLoadCorePropertiesAndOverrideSalWithCore() throws Exception {
        prepareSalProperty(PROPERTY_NAME, "salValue");
        prepareCoreProperty(PROPERTY_NAME, "coreValue");

        final Properties properties = coreFeaturesLoader.loadCoreProperties();

        assertThat(properties, hasEntry("myProperty", "coreValue"));
    }


    @Test
    public void shouldLoadPropertiesFromSalCoreAndSystemIndependently() throws Exception {
        prepareSalProperty(PROPERTY_NAME + "1", "salValue");
        prepareCoreProperty(PROPERTY_NAME + "2", "coreValue");
        prepareSystemProperty(SYSTEM_PROPERTY_NAME + "3", "systemValue");

        final Properties properties = coreFeaturesLoader.loadCoreProperties();

        assertThat(properties, hasEntry(PROPERTY_NAME + "1", "salValue"));
        assertThat(properties, hasEntry(PROPERTY_NAME + "2", "coreValue"));
        assertThat(properties, hasEntry(PROPERTY_NAME + "3", "systemValue"));
    }

    @Test
    public void shouldLoadCorePropertiesIgnoreIfSalPropertiesFilesIsMissing() throws Exception {
        prepareCoreProperty(PROPERTY_NAME, "coreValue");

        final Properties properties = coreFeaturesLoader.loadCoreProperties();

        assertThat(properties, hasEntry("myProperty", "coreValue"));
    }

    @Test
    public void shouldLoadCorePropertiesFromSystemOnlyWithSpecificPrefix() throws Exception {
        //core properties are required so lets init something
        prepareEmptyCoreProperties();
        prepareSystemProperty("wrong" + SYSTEM_PROPERTY_NAME + "4", "systemValue");

        final Properties properties = coreFeaturesLoader.loadCoreProperties();

        //noinspection unchecked
        assertThat(properties.entrySet(), Matchers.empty());
    }

    @Test
    public void shouldThrowExceptionWhenCorePropertiesCannotBeLoaded() throws Exception {
        expectedException.expect(IllegalStateException.class);
        expectedException.expectMessage(format("Resource %s not found", CORE_FEATURES_RESOURCE));

        coreFeaturesLoader.loadCoreProperties();
    }

    @Test
    public void shouldLoadAllPluginFeatureResourcesAsProperties() throws Exception {
        final Plugin pluginWithFeatureResources1 = getPluginWithResources(inputStreamForProperties("key1", "value1"));
        final Plugin pluginWithoutFeatureResources = mock(Plugin.class);
        when(pluginWithoutFeatureResources.getResourceDescriptors()).thenReturn(ImmutableList.of(mock(ResourceDescriptor.class)));

        final Plugin pluginWithFeatureResources2 = getPluginWithResources(inputStreamForProperties("key2", "value2"));
        when(pluginAccessor.getEnabledPlugins()).thenReturn(ImmutableList.of(pluginWithFeatureResources1, pluginWithoutFeatureResources, pluginWithFeatureResources2));
        final Iterable<Properties> properties = coreFeaturesLoader.loadPluginsFeatureProperties();

        assertThat(properties, hasItems(
                        properties("key1", "value1"),
                        properties("key2", "value2"))
        );
    }

    @Test
    public void shouldThrowIllegalStateExceptionWhenAnyOfPluginResourcesCannotBeLoaded() throws Exception {
        final Plugin plugin = getPluginWithResources(inputStreamForProperties("key1", "value1"));
        final Plugin badPlugin = getPluginWithResources(Option.none());
        when(pluginAccessor.getEnabledPlugins()).thenReturn(ImmutableList.of(plugin, badPlugin));

        expectedException.expect(IllegalStateException.class);
        expectedException.expectMessage(format("Resource %s not found", PLUGIN_RESOURCE_LOCATION));

        coreFeaturesLoader.loadPluginsFeatureProperties();
    }

    @Test
    public void shouldThrowRuntimeIoExceptionWhenLoadingOfAnyPluginResourcesThrowsIOException() throws Exception {
        final Plugin plugin = getPluginWithResources(inputStreamForProperties("key1", "value1"));
        final InputStream inputStream = mock(InputStream.class);
        when(inputStream.read(any())).thenThrow(new IOException("you shall not read"));
        final Plugin badPlugin = getPluginWithResources(Option.some(inputStream));
        when(pluginAccessor.getEnabledPlugins()).thenReturn(ImmutableList.of(plugin, badPlugin));

        expectedException.expect(RuntimeIOException.class);
        expectedException.expectMessage("Unable to load properties from justLocation");
        expectedException.expectCause(
                allOf(
                        instanceOf(IOException.class),
                        hasMessage(equalTo("you shall not read")))
        );

        coreFeaturesLoader.loadPluginsFeatureProperties();
    }

    @Test
    public void shouldReturnTrueForHasFeatureResourcesWhenPluginDefinesFeatureResources() throws Exception {
        final Plugin plugin = getPluginWithResources(inputStreamForProperties(PROPERTY_NAME, "value"));
        when(pluginAccessor.getEnabledPlugins()).thenReturn(ImmutableList.of(plugin));

        final boolean hasFeatureResources = coreFeaturesLoader.hasFeatureResources(plugin);

        assertThat("Plugins defines feature resources", hasFeatureResources, is(true));

    }

    @Test
    public void shouldReturnFalseForHasFeatureResourcesWhenPluginDoesNotDefineFeatureResources() throws Exception {
        final Plugin plugin = mock(Plugin.class);
        when(plugin.getResourceDescriptors()).thenReturn(ImmutableList.of());
        when(pluginAccessor.getEnabledPlugins()).thenReturn(ImmutableList.of(plugin));

        final boolean hasFeatureResources = coreFeaturesLoader.hasFeatureResources(plugin);

        assertThat("Plugins defines feature resources", hasFeatureResources, is(false));
    }

    @Test
    public void shouldLogAllLoadedSystemPropertiesAsTrace() {
        prepareEmptyCoreProperties();
        prepareSystemProperty(SYSTEM_PROPERTY_NAME, "value");

        coreFeaturesLoader.loadCoreProperties();

        assertThat(log4jLogger.getMessage(),
                containsString(
                        format("Feature '%s' is set to 'value' by system properties", PROPERTY_NAME)));
    }

    @Test
    public void jiraFeaturesPropertiesFileShouldBeProperlyLocatedInResources() {
        final ClassPathResourceLoader loader = new ClassPathResourceLoader();

        final Option<URL> resource = loader.getResource(CORE_FEATURES_RESOURCE);

        assertThat(resource, some(anything()));
    }

    private Plugin getPluginWithResources(final Option<InputStream> streamOption) {
        final Plugin plugin = mock(Plugin.class);
        final ResourceDescriptor resourceDescriptor = mock(ResourceDescriptor.class);
        when(resourceDescriptor.getType()).thenReturn(FEATURE_RESOURCE_TYPE);
        when(resourceDescriptor.getLocation()).thenReturn(PLUGIN_RESOURCE_LOCATION);
        when(plugin.getResourceDescriptors()).thenReturn(ImmutableList.of(resourceDescriptor));
        when(resourceLoader.getResourceAsStream(plugin, PLUGIN_RESOURCE_LOCATION)).thenReturn(streamOption);
        return plugin;
    }

    private Properties properties(final String propertyName, final String propertyValue) {
        final Properties properties = new Properties();
        properties.setProperty(propertyName, propertyValue);
        return properties;
    }

    private Option<InputStream> inputStreamForProperties(final String propertyName, final String propertyValue) {
        final Properties properties = properties(propertyName, propertyValue);
        final StringWriter stringWriter = new StringWriter();
        try {
            properties.store(stringWriter, null);
        } catch (final IOException e) {
            fail("Writing to memory should never fail");
        }
        return Option.some(new ByteArrayInputStream(stringWriter.getBuffer().toString().getBytes()));
    }

    private void prepareSystemProperty(final String propertyName, final String propertyValue) {
        when(jiraSystemProperties.getProperties()).thenReturn(properties(propertyName, propertyValue));
        when(jiraSystemProperties.getProperty(propertyName)).thenReturn(propertyValue);
    }

    private void prepareSalProperty(final String propertyName, final String propertyValue) {
        when(jiraSystemProperties.getProperty(DARKFEATURES_PROPERTIES_FILE_PROPERTY, DARKFEATURES_PROPERTIES_FILE_PROPERTY_DEFAULT))
                .thenReturn("propertyName");
        when(resourceLoader.getResourceAsStream("propertyName"))
                .thenReturn(inputStreamForProperties(propertyName, propertyValue));
    }

    private void prepareCoreProperty(final String propertyName, final String propertyValue) {
        when(resourceLoader.getResourceAsStream(CORE_FEATURES_RESOURCE))
                .thenReturn(inputStreamForProperties(propertyName, propertyValue));
    }

    private void prepareEmptyCoreProperties() {
        when(resourceLoader.getResourceAsStream(CORE_FEATURES_RESOURCE))
                .thenReturn(Option.some(new ByteArrayInputStream(new byte[0])));
    }
}