package com.atlassian.jira.bc.project.version;

import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.junit.rules.InitMockitoMocks;
import com.atlassian.jira.project.version.VersionManager;
import com.atlassian.jira.security.PermissionManager;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;

/**
 * Tests the {@link DeleteVersionValidator} construction. Note that the actual validation is already being tested in the
 * {@link TestDefaultVersionService}.
 */
public class TestDeleteVersionValidator {
    @Rule
    public InitMockitoMocks mocks = new InitMockitoMocks(this);
    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Mock
    VersionManager mockVersionManager;
    @Mock
    JiraServiceContext context;
    @Mock
    PermissionManager permissionManager;

    @Test
    public void constructorThrowsExceptionOnNullContext() {
        exception.expect(IllegalArgumentException.class);
        new DeleteVersionValidator(null, mockVersionManager, permissionManager);
    }

    @Test
    public void constructorThrowsExceptionOnNullVersionManager() {
        exception.expect(IllegalArgumentException.class);
        new DeleteVersionValidator(context, null, permissionManager);
    }

    @Test
    public void constructorThrowsExceptionOnNullPermissionManager() {
        exception.expect(IllegalArgumentException.class);
        new DeleteVersionValidator(context, mockVersionManager, null);
    }

    @Test
    public void constructorWorksForNonNullParameters() {
        new DeleteVersionValidator(context, mockVersionManager, permissionManager);
    }
}

