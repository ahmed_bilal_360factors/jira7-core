package com.atlassian.jira.issue.fields.csv;

import com.atlassian.jira.issue.export.FieldExportParts;
import com.atlassian.jira.issue.fields.CreatorSystemField;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.mock.issue.MockIssue;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.NoopI18nHelper;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

/**
 * @since v6.5
 */
public class CreatorSystemFieldCsvExportTest {
    private static final String CREATOR_USERNAME = "Creator username";

    @Rule
    public final RuleChain mockito = MockitoMocksInContainer.forTest(this);

    @Mock
    private MockIssue issue;
    @Mock
    private ApplicationUser creator;

    @Mock
    private JiraAuthenticationContext authenticationContext;

    @InjectMocks
    private CreatorSystemField systemFieldUnderTest;

    @Before
    public void setUp() throws Exception {
        when(creator.getName()).thenReturn(CREATOR_USERNAME);

        when(authenticationContext.getI18nHelper()).thenReturn(new NoopI18nHelper());
    }

    @Test
    public void testCsvRepresentationForExpectedTypeAndValues() {
        when(issue.getCreator()).thenReturn(creator);

        final FieldExportParts representation = systemFieldUnderTest.getRepresentationFromIssue(issue);

        assertThat(representation.getParts(), hasSize(1));
        assertThat(representation.getParts().get(0).getValues()::iterator, contains(CREATOR_USERNAME));
    }

    @Test
    public void testCsvRepresentationWhenThereIsNoCreator() {
        when(issue.getCreator()).thenReturn(null);

        final FieldExportParts representation = systemFieldUnderTest.getRepresentationFromIssue(issue);

        assertThat(representation.getParts(), hasSize(1));
        assertThat(representation.getParts().get(0).getValues()::iterator, contains(""));
    }
}
