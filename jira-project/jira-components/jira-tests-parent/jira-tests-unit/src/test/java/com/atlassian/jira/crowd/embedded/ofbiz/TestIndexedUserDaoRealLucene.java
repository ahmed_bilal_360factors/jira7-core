package com.atlassian.jira.crowd.embedded.ofbiz;

import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.embedded.api.SearchRestriction;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.search.query.entity.UserQuery;
import com.atlassian.crowd.search.query.entity.restriction.BooleanRestriction;
import com.atlassian.crowd.search.query.entity.restriction.BooleanRestrictionImpl;
import com.atlassian.crowd.search.query.entity.restriction.MatchMode;
import com.atlassian.crowd.search.query.entity.restriction.NullRestrictionImpl;
import com.atlassian.crowd.search.query.entity.restriction.TermRestriction;
import com.atlassian.crowd.search.query.entity.restriction.constants.UserTermKeys;
import com.atlassian.crowd.util.BatchResult;
import com.atlassian.jira.bc.user.search.DirectoryUserIndexer;
import com.atlassian.jira.bc.user.search.UserIndexer;
import com.atlassian.jira.bc.user.search.UserNameAnalyzer;
import com.atlassian.jira.cluster.ClusterMessagingService;
import com.atlassian.jira.crowd.embedded.EventuallyConsistentQuery;
import com.atlassian.jira.crowd.embedded.lucene.CrowdQueryTranslator;
import com.atlassian.jira.crowd.embedded.ofbiz.db.DefaultOfBizTransactionManager;
import com.atlassian.jira.crowd.embedded.ofbiz.db.OfBizTransactionManager;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.RAMDirectory;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.mockito.Mock;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import static com.atlassian.jira.crowd.embedded.ofbiz.OfBizUserFactory.makeUser;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.emptyCollectionOf;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.when;

/**
 * Use a real Lucene implementation to test queries from indexed user DAO.
 */
public class TestIndexedUserDaoRealLucene {

    private static final long MAIN_DIRECTORY_ID = 1L;

    @Rule
    public RuleChain ruleChain = MockitoMocksInContainer.forTest(this);

    @Mock
    private ExtendedUserDao baseUserDao;

    @Mock
    private ClusterMessagingService clusterMessagingService;

    private IndexedUserDao userDao;

    private List<OfBizUser> users;

    @Before
    public void setUp() throws Exception {

        users = Lists.newArrayList(
                makeUser(MAIN_DIRECTORY_ID, 1L, "jgalah", "John Galah", "galah@galahsrus.org", true),
                makeUser(MAIN_DIRECTORY_ID, 2L, "acockatoo", "Amy Cockatoo", "amy@galahsrus.org", true),
                makeUser(MAIN_DIRECTORY_ID, 3L, "penny", "Penelope Ibis", "pennyibis@fullofgalahs.org", true),
                makeUser(MAIN_DIRECTORY_ID, 4L, "raven", "Amy Raven", "araven@example.com", false),
                makeUser(MAIN_DIRECTORY_ID, 5L, "amy", "Amy", "amy@example.com", false)
        );

        doAnswer(invocation -> feedUsers(invocation.getArgumentAt(0, Consumer.class)))
                .when(baseUserDao).processUsers(any());
        when(baseUserDao.add(any(com.atlassian.crowd.model.user.User.class), any(PasswordCredential.class)))
                .thenAnswer(invocation -> invocation.getArgumentAt(0, com.atlassian.crowd.model.user.User.class));
        doAnswer(invocation -> users.remove(invocation.getArgumentAt(0, OfBizUser.class)))
                .when(baseUserDao).remove(any());
        when(baseUserDao.removeAllUsers(anyLong(), any(Set.class)))
                .thenAnswer(invocation -> {
                    Set<String> userNames = invocation.getArgumentAt(1, Set.class);
                    if (userNames == null) {
                        return new BatchResult<String>(0);
                    }
                    users.removeIf(user -> userNames.contains(user.getName()));
                    return batchResult(ImmutableList.copyOf(userNames), Collections.emptyList());
                });
        when(baseUserDao.findById(anyLong()))
                .thenAnswer(invocation -> users.stream().filter(user -> user.getId() == invocation.getArgumentAt(0, Long.class)).findFirst().orElse(null));
        when(baseUserDao.findByNameOrNull(anyLong(), anyString()))
                .thenAnswer(invocation -> users.stream().filter(user -> user.getName().equals(invocation.getArgumentAt(1, String.class))).findFirst().orElse(null));

        OfBizTransactionManager ofBizTransactionManager = new DefaultOfBizTransactionManager();
        Directory luceneDirectory = new RAMDirectory();
        Analyzer luceneAnalyzer = new UserNameAnalyzer();
        UserIndexer indexer = new DirectoryUserIndexer(luceneDirectory, luceneAnalyzer);
        CrowdQueryTranslator translator = new CrowdQueryTranslator(luceneAnalyzer);
        userDao = new IndexedUserDao(baseUserDao, indexer, translator, clusterMessagingService, ofBizTransactionManager, 1000);

        userDao.flushCache();
    }

    private Object feedUsers(Consumer<com.atlassian.crowd.model.user.User> consumer) {
        users.forEach(consumer);
        return null;
    }

    @Test
    public void findAllUsers() {
        SearchRestriction search = NullRestrictionImpl.INSTANCE;

        List<User> results = search(query(search));

        assertThat(toUserNames(results), contains("acockatoo", "amy", "jgalah", "penny", "raven"));
    }

    private List<User> search(EntityQuery<User> query) {
        return userDao.search(MAIN_DIRECTORY_ID, query);
    }

    private EntityQuery<User> query(SearchRestriction search) {
        return query(search, 0, 100);
    }

    private EntityQuery<User> query(SearchRestriction search, int startIndex, int maxResults) {
        return new EventuallyConsistentQuery<>(new UserQuery<>(User.class, search, startIndex, maxResults));
    }

    @Test
    public void findUserByName() {
        SearchRestriction search = new TermRestriction<>(UserTermKeys.DISPLAY_NAME, MatchMode.STARTS_WITH, "galah");

        List<User> results = search(query(search));

        assertThat(toUserNames(results), contains("jgalah"));
    }

    @Test
    public void findByEmail() {
        SearchRestriction search = new TermRestriction<>(UserTermKeys.EMAIL, MatchMode.STARTS_WITH, "pennyibis");

        List<User> results = search(query(search));

        assertThat(toUserNames(results), contains("penny"));
    }

    @Test
    public void findOnlyActiveUsers() {
        SearchRestriction search = new TermRestriction<>(UserTermKeys.ACTIVE, MatchMode.EXACTLY_MATCHES, true);

        List<User> results = search(query(search));

        assertThat(toUserNames(results), contains("acockatoo", "jgalah", "penny"));
    }

    @Test
    public void findOnlyInactiveUsers() {
        SearchRestriction search = new TermRestriction<>(UserTermKeys.ACTIVE, MatchMode.EXACTLY_MATCHES, false);

        List<User> results = search(query(search));

        assertThat(toUserNames(results), contains("amy", "raven"));
    }

    /**
     * Two search criteria.
     */
    @Test
    public void findActiveUsersWithName() {
        SearchRestriction search = new BooleanRestrictionImpl(
                BooleanRestriction.BooleanLogic.AND,
                new TermRestriction<>(UserTermKeys.DISPLAY_NAME, MatchMode.STARTS_WITH, "amy"),
                new TermRestriction<>(UserTermKeys.ACTIVE, MatchMode.EXACTLY_MATCHES, true)
        );

        List<User> results = search(query(search));

        assertThat(toUserNames(results), contains("acockatoo"));
    }

    /**
     * Asking for an exact match of a field does not match for tokens in that field.  e.g. 'amy' does not match 'amy smith'
     */
    @Test
    public void exactlyMatchesDoesNotIncludeSmallerMatches() {
        SearchRestriction search = new TermRestriction<>(UserTermKeys.DISPLAY_NAME, MatchMode.EXACTLY_MATCHES, "amy");

        List<User> results = search(query(search));

        assertThat(toUserNames(results), contains("amy"));
    }

    @Test
    public void exactlyMatchesIsCaseInsensitive() {
        SearchRestriction search = new TermRestriction<>(UserTermKeys.DISPLAY_NAME, MatchMode.EXACTLY_MATCHES, "Amy");

        List<User> results = search(query(search));

        assertThat(toUserNames(results), contains("amy"));
    }

    @Test
    public void startsWithDoesIncludeSmallerMatches() {
        SearchRestriction search = new TermRestriction<>(UserTermKeys.DISPLAY_NAME, MatchMode.STARTS_WITH, "amy");

        List<User> results = search(query(search));

        assertThat(toUserNames(results), contains("acockatoo", "amy", "raven"));
    }

    @Test
    public void searchWithLimit() {
        SearchRestriction search = NullRestrictionImpl.INSTANCE;

        List<User> results = search(query(search, 0, 2));

        assertThat(toUserNames(results), contains("acockatoo", "amy"));
    }

    @Test
    public void searchWithOffsetAndLimit() {
        SearchRestriction search = NullRestrictionImpl.INSTANCE;

        List<User> results = search(query(search, 1, 2));

        assertThat(toUserNames(results), contains("amy", "jgalah"));
    }

    /**
     * Search with offset and a result limit that is higher than the actual number of results we get back.
     */
    @Test
    public void searchWithOffsetAndHighLimit() {
        SearchRestriction search = NullRestrictionImpl.INSTANCE;

        List<User> results = search(query(search, 1, 1000));

        assertThat(toUserNames(results), contains("amy", "jgalah", "penny", "raven"));
    }

    @Test
    public void searchWithHighOffset() {
        SearchRestriction search = NullRestrictionImpl.INSTANCE;

        List<User> results = search(query(search, 1000, 1000));

        assertThat(results, emptyCollectionOf(User.class));
    }

    @Test
    public void deleteUserWithMultipleTokensInUserName() throws Exception {

        com.atlassian.crowd.model.user.User user = makeUser(1L, 20L, "user@galah", "Token User", "user@galah.com", true);
        userDao.add(user, new PasswordCredential("galah"));

        //Verify user is indexed
        SearchRestriction search = new TermRestriction<>(UserTermKeys.DISPLAY_NAME, MatchMode.STARTS_WITH, "Token");
        List<User> results1 = search(query(search, 0, 100));
        assertThat(toUserNames(results1), contains("user@galah"));

        //Now delete the user
        userDao.remove(user);

        //Verify search no longer picks up user
        List<User> results2 = search(query(search, 0, 100));
        assertThat(toUserNames(results2), empty());
    }

    /**
     * Simulate OfBizUserDao's delete veto and make sure if the user isn't really deleted in the base DAO that the
     * indexed DAO is appropriately updated.
     */
    @Test
    public void vetoedDeleteIsReflectedInLuceneIndex() throws Exception {

        //Veto all deletes, make users inactive instead like OfBizUserDao might do
        doAnswer(invocation -> {
            OfBizUser user = invocation.getArgumentAt(0, OfBizUser.class);
            int index = users.indexOf(user);
            users.set(index, convertToInactiveUser(user));
            return null;
        }).when(baseUserDao).remove(any());

        userDao.remove(users.get(1)); //Amy Cockatoo

        SearchRestriction search = new TermRestriction<>(UserTermKeys.DISPLAY_NAME, MatchMode.STARTS_WITH, "Cockatoo");
        List<User> results = search(query(search, 0, 100));
        assertThat(toUserNames(results), contains("acockatoo"));
    }

    /**
     * Similar to previous test but with bulk delete operation.
     */
    @Test
    public void vetoedBulkDeleteIsReflectedInLuceneIndex() throws Exception {

        //Veto all deletes, make users inactive instead like OfBizUserDao might do
        when(baseUserDao.removeAllUsers(anyLong(), any(Set.class)))
                .thenAnswer(invocation -> {
                    Set<String> userNames = invocation.getArgumentAt(1, Set.class);
                    for (String userName : userNames) {
                        OfBizUser user = users.stream().filter(u -> u.getName().equals(userName)).findFirst().get();
                        int index = users.indexOf(user);
                        users.set(index, convertToInactiveUser(user));
                    }
                    return batchResult(ImmutableList.copyOf(userNames), Collections.emptyList());
                });
        userDao.removeAllUsers(MAIN_DIRECTORY_ID, ImmutableSet.of("acockatoo"));

        SearchRestriction search = new TermRestriction<>(UserTermKeys.DISPLAY_NAME, MatchMode.STARTS_WITH, "Cockatoo");
        List<User> results = search(query(search, 0, 100));
        assertThat(toUserNames(results), contains("acockatoo"));
    }

    private OfBizUser convertToInactiveUser(OfBizUser user) {
        return makeUser(user.getDirectoryId(), user.getId(), user.getName(), user.getDisplayName(),
                user.getEmailAddress(), false);
    }

    private static List<String> toUserNames(List<? extends User> users) {
        return users.stream().map(User::getName).collect(Collectors.toList());
    }

    private static <T> BatchResult<T> batchResult(List<T> successes, List<T> failures) {
        BatchResult<T> br = new BatchResult<>(successes.size() + failures.size());
        br.addSuccesses(successes);
        br.addFailures(failures);
        return br;
    }
}
