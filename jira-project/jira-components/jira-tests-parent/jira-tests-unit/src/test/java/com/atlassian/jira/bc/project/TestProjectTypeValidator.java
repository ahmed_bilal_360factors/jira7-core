package com.atlassian.jira.bc.project;

import com.atlassian.fugue.Option;
import com.atlassian.jira.project.type.ProjectType;
import com.atlassian.jira.project.type.ProjectTypeKey;
import com.atlassian.jira.project.type.ProjectTypeManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static com.atlassian.fugue.Option.some;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestProjectTypeValidator {
    private static final ApplicationUser USER = new MockApplicationUser("user");

    @Mock
    private ProjectTypeManager projectTypeManager;

    private ProjectTypeKey projectTypeKey = new ProjectTypeKey("key");

    private ProjectTypeValidator validator;

    @Before
    public void setUp() {
        validator = new ProjectTypeValidator(projectTypeManager);
    }

    @Test
    public void nullProjectTpeIsInvalidIfTheValidationDarkFeatureIsEnabled() {
        projectTypeIsAccessible();

        boolean isValid = validator.isValid(USER, null);

        assertThat(isValid, is(false));
    }

    @Test
    public void projectTypeIsInvalidWhenProjectTypeIsNotAccessibleByTheUser() {
        projectTypeIsNotAccessible();

        boolean isValid = validator.isValid(USER, projectTypeKey);

        assertThat(isValid, is(false));
    }

    @Test
    public void projectTypeIsValidWhenProjectTypeIsAccessibleByTheUser() {
        projectTypeIsAccessible();

        boolean isValid = validator.isValid(USER, projectTypeKey);

        assertThat(isValid, is(true));
    }

    private void projectTypeIsNotAccessible() {
        when(projectTypeManager.getAccessibleProjectType(projectTypeKey)).thenReturn(Option.<ProjectType>none());
        when(projectTypeManager.getByKey(projectTypeKey)).thenReturn(Option.<ProjectType>none());
    }

    private void projectTypeIsAccessible() {
        when(projectTypeManager.getAccessibleProjectType(projectTypeKey)).thenReturn(some(anyProjectType()));
        when(projectTypeManager.getByKey(projectTypeKey)).thenReturn(some(anyProjectType()));
    }

    private ProjectType anyProjectType() {
        ProjectTypeKey projectTypeKey = new ProjectTypeKey("key");
        return new ProjectType(projectTypeKey, null, null, null, 0);
    }
}
