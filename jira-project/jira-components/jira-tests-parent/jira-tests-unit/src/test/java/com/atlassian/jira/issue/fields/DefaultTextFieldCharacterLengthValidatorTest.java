package com.atlassian.jira.issue.fields;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

public class DefaultTextFieldCharacterLengthValidatorTest {
    private TextFieldLimitProvider textFieldLimitProvider = Mockito.mock(TextFieldLimitProvider.class);

    private TextFieldCharacterLengthValidator validator;

    @Before
    public void setUp() {
        validator = new DefaultTextFieldCharacterLengthValidator(textFieldLimitProvider);
    }

    @Test
    public void textIsNotTooLongForNullString() throws Exception {
        expectMockTextFieldLimitProviderReturnsLimit(10L);

        assertFalse(validator.isTextTooLong(null));
    }

    @Test
    public void textIsNotTooLongForEmptyString() throws Exception {
        expectMockTextFieldLimitProviderReturnsLimit(10L);

        assertFalse(validator.isTextTooLong(""));
    }

    @Test
    public void textIsNotTooLongForShortText() throws Exception {
        expectMockTextFieldLimitProviderReturnsLimit(20L);

        assertFalse(validator.isTextTooLong("some short text"));
    }

    @Test
    public void textIsNotTooLongForTextLengthAtLimit() throws Exception {
        final String text = "this text must not be longer";
        expectMockTextFieldLimitProviderReturnsLimit(text.length());

        assertFalse(validator.isTextTooLong(text));
    }

    @Test
    public void textIsNotTooLongWhenLimitIsOff() throws Exception {
        expectMockTextFieldLimitProviderReturnsLimit(0L);

        assertFalse(validator.isTextTooLong("some very long text that would normally be too long"));
    }

    @Test
    public void textIsTooLongForTextExceedingLengthByOne() throws Exception {
        final String text = "this text must not be longer";
        expectMockTextFieldLimitProviderReturnsLimit(text.length() - 1);

        assertTrue(validator.isTextTooLong(text));
    }

    @Test
    public void textIsTooLongForFarTooLongText() throws Exception {
        expectMockTextFieldLimitProviderReturnsLimit(5L);

        assertTrue(validator.isTextTooLong("this text is really too long"));
    }

    private void expectMockTextFieldLimitProviderReturnsLimit(final long numberOfCharacters) {
        when(textFieldLimitProvider.getTextFieldLimit()).thenReturn(numberOfCharacters);
    }

    @Test
    public void textIsNotTooLongForZeroTextLength() throws Exception {
        expectMockTextFieldLimitProviderReturnsLimit(10L);

        assertFalse(validator.isTextTooLong(0));
    }

    @Test
    public void textIsNotTooLongForLengthSmallerThanMax() throws Exception {
        expectMockTextFieldLimitProviderReturnsLimit(20L);

        assertFalse(validator.isTextTooLong(18));
    }

    @Test
    public void textIsNotTooLongWhenLimitIsOff2() throws Exception {
        expectMockTextFieldLimitProviderReturnsLimit(0);

        assertFalse(validator.isTextTooLong(18));
    }

    @Test
    public void textIsTooLongForLengthGreaterThanMax() throws Exception {
        expectMockTextFieldLimitProviderReturnsLimit(30L);

        assertTrue(validator.isTextTooLong(31));
    }
}
