package com.atlassian.jira.issue;

import com.atlassian.jira.web.ExecutingHttpRequest;
import com.atlassian.jira.web.SessionKeys;
import com.atlassian.jira.web.action.issue.TemporaryAttachmentsMonitor;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.CoreMatchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.same;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Test for {@link com.atlassian.jira.issue.DefaultTemporaryAttachmentsMonitorLocator}.
 *
 * @since v4.3
 */
public class TestDefaultTemporaryAttachmentsMonitorLocator {
    @Rule
    public MethodRule initMockito = MockitoJUnit.rule();

    @Mock
    private HttpServletRequest httpServletRequest;
    @Mock
    private HttpSession httpSession;

    @Before
    public void setup() {
        ExecutingHttpRequest.set(httpServletRequest, null);
    }

    @After
    public void tearDown() {
        httpServletRequest = null;
        httpSession = null;

        ExecutingHttpRequest.clear();
    }

    @Test
    public void testGetForRequestNoCreateNoSession() throws Exception {
        when(httpServletRequest.getSession(false)).thenReturn(null);

        final DefaultTemporaryAttachmentsMonitorLocator locator = new DefaultTemporaryAttachmentsMonitorLocator();
        assertThat(locator.get(false), nullValue());
    }

    @Test
    public void testGetForRequestNoCreateSessionNoValue() throws Exception {
        when(httpServletRequest.getSession(false)).thenReturn(httpSession);
        when(httpSession.getAttribute(SessionKeys.TEMP_ATTACHMENTS)).thenReturn(null);

        final DefaultTemporaryAttachmentsMonitorLocator locator = new DefaultTemporaryAttachmentsMonitorLocator();
        assertThat(locator.get(false), nullValue());
    }

    @Test
    public void testGetForRequestNoCreateSessionValue() throws Exception {
        final TemporaryAttachmentsMonitor monitor = mock(TemporaryAttachmentsMonitor.class);

        when(httpServletRequest.getSession(false)).thenReturn(httpSession);
        when(httpSession.getAttribute(SessionKeys.TEMP_ATTACHMENTS)).thenReturn(monitor);

        final DefaultTemporaryAttachmentsMonitorLocator locator = new DefaultTemporaryAttachmentsMonitorLocator();
        assertThat(locator.get(false), sameInstance(monitor));
    }

    @Test
    public void testGetForRequestCreateSessionNoValue() throws Exception {
        when(httpServletRequest.getSession(true)).thenReturn(httpSession);
        when(httpSession.getAttribute(SessionKeys.TEMP_ATTACHMENTS)).thenReturn(null);

        final DefaultTemporaryAttachmentsMonitorLocator locator = new DefaultTemporaryAttachmentsMonitorLocator();
        TemporaryAttachmentsMonitor monitor = locator.get(true);
        assertThat(monitor, notNullValue());
        verify(httpSession).setAttribute(eq(SessionKeys.TEMP_ATTACHMENTS), same(monitor));
    }

    @Test
    public void testGetForRequestCreateSessionValue() throws Exception {
        final TemporaryAttachmentsMonitor monitor = mock(TemporaryAttachmentsMonitor.class);

        when(httpServletRequest.getSession(true)).thenReturn(httpSession);
        when(httpSession.getAttribute(SessionKeys.TEMP_ATTACHMENTS)).thenReturn(monitor);

        final DefaultTemporaryAttachmentsMonitorLocator locator = new DefaultTemporaryAttachmentsMonitorLocator();
        assertThat(locator.get(true), sameInstance(monitor));
    }

    @Test
    public void testGetForRequestNoRequest() throws Exception {
        ExecutingHttpRequest.set(null, null);

        final DefaultTemporaryAttachmentsMonitorLocator locator = new DefaultTemporaryAttachmentsMonitorLocator();
        assertThat(locator.get(true), nullValue(TemporaryAttachmentsMonitor.class));
    }
}
