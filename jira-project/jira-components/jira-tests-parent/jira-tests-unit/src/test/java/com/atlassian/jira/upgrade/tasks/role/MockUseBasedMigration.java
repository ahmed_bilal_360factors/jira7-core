package com.atlassian.jira.upgrade.tasks.role;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.crowd.embedded.api.Group;

/**
 * @since v7.0
 */
public class MockUseBasedMigration extends UseBasedMigration {
    private Group group;

    public MockUseBasedMigration(final Group group) {
        this.group = group;
    }

    @Override
    MigrationState addUsePermissionToRoles(final MigrationState state, final Iterable<ApplicationKey> keys) {
        MigrationState result = state;
        for (ApplicationKey key : keys) {
            result = result.changeApplicationRole(key, role -> role.addGroup(group));
        }
        return result;
    }
}
