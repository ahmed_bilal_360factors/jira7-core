package com.atlassian.jira.issue.customfields.searchers;

import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.datetime.DateTimeFormatterFactoryStub;
import com.atlassian.jira.issue.customfields.searchers.transformer.CustomFieldInputHelper;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.jql.operand.JqlOperandResolver;
import com.atlassian.jira.jql.util.JqlDateSupport;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.template.VelocityTemplatingEngine;
import com.atlassian.jira.timezone.TimeZoneManager;
import com.atlassian.jira.util.velocity.VelocityRequestContextFactory;
import com.atlassian.jira.web.FieldVisibilityManager;
import com.atlassian.jira.web.action.util.CalendarLanguageUtil;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestDateTimeRangeSearcher {
    @Test
    public void getSorterDoesNotReturnNull() {
        final DateTimeRangeSearcher dateTimeRangeSearcher = new DateTimeRangeSearcher(
                mock(JiraAuthenticationContext.class),
                mock(JqlOperandResolver.class),
                mock(VelocityRequestContextFactory.class),
                mock(ApplicationProperties.class),
                mock(VelocityTemplatingEngine.class),
                mock(CalendarLanguageUtil.class),
                mock(JqlDateSupport.class),
                mock(CustomFieldInputHelper.class),
                mock(TimeZoneManager.class),
                new DateTimeFormatterFactoryStub(),
                mock(FieldVisibilityManager.class)
        );

        final CustomField mockDateTimeField = mock(CustomField.class);
        when(mockDateTimeField.getId()).thenReturn("foo");

        assertNotNull(dateTimeRangeSearcher.getSorter(mockDateTimeField));
    }
}
