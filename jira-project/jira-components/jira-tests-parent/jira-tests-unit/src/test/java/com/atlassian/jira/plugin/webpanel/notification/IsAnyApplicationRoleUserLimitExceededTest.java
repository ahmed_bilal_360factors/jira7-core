package com.atlassian.jira.plugin.webpanel.notification;

import com.atlassian.jira.application.ApplicationRoleManager;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class IsAnyApplicationRoleUserLimitExceededTest {
    @Mock
    private ApplicationRoleManager applicationRoleManager;

    @Test
    public void shouldDisplayIfAnyApplicationRoleIsExceeded() {
        when(applicationRoleManager.isAnyRoleLimitExceeded()).thenReturn(true);
        IsAnyApplicationRoleUserLimitExceeded condition = new IsAnyApplicationRoleUserLimitExceeded(applicationRoleManager);

        assertTrue(condition.shouldDisplay(null));
    }

    @Test
    public void shouldNotDisplayIfApplicationRolesAreNotExceeded() {
        when(applicationRoleManager.isAnyRoleLimitExceeded()).thenReturn(false);
        IsAnyApplicationRoleUserLimitExceeded condition = new IsAnyApplicationRoleUserLimitExceeded(applicationRoleManager);

        assertFalse(condition.shouldDisplay(null));
    }
}