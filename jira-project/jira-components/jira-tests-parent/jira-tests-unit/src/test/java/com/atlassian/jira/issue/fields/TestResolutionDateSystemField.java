package com.atlassian.jira.issue.fields;

import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.template.VelocityTemplatingEngine;
import org.apache.velocity.exception.VelocityException;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.sql.Timestamp;
import java.util.Map;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestResolutionDateSystemField {
    @Rule
    public MockitoRule mockito = MockitoJUnit.rule();

    @Mock
    ColumnViewDateTimeHelper mockColumnViewDateTimeHelper;

    @Mock
    VelocityTemplatingEngine templatingEngine;

    @Mock
    ApplicationProperties applicationProperties;

    @Mock
    JiraAuthenticationContext authContext;

    @Mock
    FieldLayoutItem fieldLayoutItem;

    @Mock
    Map displayParams;

    @Test
    public void testGetColumnViewHtmlNoDate() throws VelocityException {
        final Timestamp testDate = new Timestamp(123);

        final Issue mockIssue = mock(Issue.class);
        when(mockIssue.getResolutionDate()).thenReturn(testDate);

        final ResolutionDateSystemField field = new ResolutionDateSystemField(templatingEngine, applicationProperties, authContext, null, mockColumnViewDateTimeHelper, null, null);

        when(mockColumnViewDateTimeHelper.render(field, fieldLayoutItem, displayParams, mockIssue, testDate)).thenReturn("Nuthing");

        final String columnViewHtml = field.getColumnViewHtml(fieldLayoutItem, displayParams, mockIssue);
        assertThat(columnViewHtml, equalTo("Nuthing"));
    }

    @Test
    public void testGetColumnViewHtml() throws VelocityException {
        final Timestamp resDate = new Timestamp(1);

        final Issue mockIssue = mock(Issue.class);
        when(mockIssue.getResolutionDate()).thenReturn(resDate);

        final ResolutionDateSystemField field = new ResolutionDateSystemField(templatingEngine, applicationProperties, authContext, null, mockColumnViewDateTimeHelper, null, null);

        when(mockColumnViewDateTimeHelper.render(field, fieldLayoutItem, displayParams, mockIssue, resDate)).thenReturn("12/10/08");

        final String columnViewHtml = field.getColumnViewHtml(fieldLayoutItem, displayParams, mockIssue);
        assertThat(columnViewHtml, equalTo("12/10/08"));
    }

}
