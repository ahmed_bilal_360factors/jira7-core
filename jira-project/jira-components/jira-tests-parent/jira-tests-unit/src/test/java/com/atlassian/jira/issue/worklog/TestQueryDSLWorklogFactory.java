package com.atlassian.jira.issue.worklog;

import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.mock.issue.MockIssue;
import com.atlassian.jira.model.querydsl.QProjectRole;
import com.atlassian.jira.model.querydsl.QWorklog;
import com.atlassian.jira.security.roles.QueryDSLProjectRoleFactory;
import com.querydsl.core.Tuple;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.sql.Timestamp;
import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestQueryDSLWorklogFactory {
    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    private static final Long WORKLOG_ID = 10l;
    private static final String WORKLOG_AUTHOR = "author";
    private static final String WORKLOG_UPDATE_AUTHOR = "update_author";
    private static final String WORKLOG_COMMENT_BODY = "worklog body";
    private static final Timestamp WORKLOG_START_DATE = new Timestamp(System.currentTimeMillis());
    private static final String WORKLOG_GROUPLEVEL = "grouplevel";
    private static final Long WORKLOG_ROLE_LEVEL = 20l;
    private static final Long WORKLOG_TIMEWORKED = 3600l;
    private static final Timestamp WORKLOG_CREATED = new Timestamp(System.currentTimeMillis());
    private static final Timestamp WORKLOG_UPDATED = new Timestamp(System.currentTimeMillis());
    private static final Long WORKLOG_ISSUE_ID = 20l;
    private static final String PROJECT_ROLE_NAME = "project role name";
    private static final String PROJECT_ROLE_DESCRIPTION = "description";

    @Mock
    private IssueManager issueManager;

    private QueryDSLWorklogFactory queryDSLWorklogFactory;
    private MockIssue issue = new MockIssue();

    @Before
    public void setUp() throws Exception {
        queryDSLWorklogFactory = new QueryDSLWorklogFactory(issueManager, new QueryDSLProjectRoleFactory());
        when(issueManager.getIssueObject(WORKLOG_ISSUE_ID)).thenReturn(issue);
    }

    @Test
    public void testCreatingWorklogFromTupleWithProjectRole() {
        final Worklog worklog = queryDSLWorklogFactory.createWorklog(mockTuple());

        assertEquals(WORKLOG_AUTHOR, worklog.getAuthorKey());
        assertEquals(WORKLOG_UPDATE_AUTHOR, worklog.getUpdateAuthorKey());
        assertEquals(WORKLOG_COMMENT_BODY, worklog.getComment());
        assertEquals(WORKLOG_ID, worklog.getId());
        assertEquals(WORKLOG_GROUPLEVEL, worklog.getGroupLevel());
        assertEquals(WORKLOG_ROLE_LEVEL, worklog.getRoleLevelId());
        assertEquals(new Date(WORKLOG_UPDATED.getTime()), worklog.getUpdated());
        assertEquals(new Date(WORKLOG_CREATED.getTime()), worklog.getCreated());
        assertEquals(issue, worklog.getIssue());
        assertEquals(WORKLOG_TIMEWORKED, worklog.getTimeSpent());
    }

    private static Tuple mockTuple() {
        final Tuple tuple = mock(Tuple.class);
        when(tuple.get(QWorklog.WORKLOG.id)).thenReturn(WORKLOG_ID);
        when(tuple.get(QWorklog.WORKLOG.author)).thenReturn(WORKLOG_AUTHOR);
        when(tuple.get(QWorklog.WORKLOG.updateauthor)).thenReturn(WORKLOG_UPDATE_AUTHOR);
        when(tuple.get(QWorklog.WORKLOG.body)).thenReturn(WORKLOG_COMMENT_BODY);
        when(tuple.get(QWorklog.WORKLOG.startdate)).thenReturn(WORKLOG_START_DATE);
        when(tuple.get(QWorklog.WORKLOG.grouplevel)).thenReturn(WORKLOG_GROUPLEVEL);
        when(tuple.get(QWorklog.WORKLOG.rolelevel)).thenReturn(WORKLOG_ROLE_LEVEL);
        when(tuple.get(QWorklog.WORKLOG.timeworked)).thenReturn(WORKLOG_TIMEWORKED);
        when(tuple.get(QWorklog.WORKLOG.created)).thenReturn(WORKLOG_CREATED);
        when(tuple.get(QWorklog.WORKLOG.updated)).thenReturn(WORKLOG_UPDATED);
        when(tuple.get(QWorklog.WORKLOG.issue)).thenReturn(WORKLOG_ISSUE_ID);

        when(tuple.get(QProjectRole.PROJECT_ROLE.id)).thenReturn(WORKLOG_ROLE_LEVEL);
        when(tuple.get(QProjectRole.PROJECT_ROLE.name)).thenReturn(PROJECT_ROLE_NAME);
        when(tuple.get(QProjectRole.PROJECT_ROLE.name)).thenReturn(PROJECT_ROLE_DESCRIPTION);
        return tuple;
    }
}
