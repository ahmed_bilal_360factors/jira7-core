package com.atlassian.jira.issue.fields.renderer.wiki;

import com.atlassian.jira.JiraTestUtil;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.util.velocity.VelocityRequestContext;
import com.atlassian.jira.util.velocity.VelocityRequestContextFactory;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;


public class TestJiraRendererConfiguration {
    @Rule
    public final MethodRule initMockito = MockitoJUnit.rule();
    @Mock
    private ApplicationProperties mockApplicationProperties;
    @Mock
    private VelocityRequestContextFactory mockVelocityRequestContextFactory;
    @Mock
    private VelocityRequestContext mockVelocityRequestContext;
    private JiraRendererConfiguration jiraRendererConfig;

    private final static String BASE_HTTP_URL = "http://localhost:8080/jira";
    private final static String BASE_HTTPS_URL_NOCONTEXT = "https://localhost:8443";

    @Before
    public void setUp() {
        jiraRendererConfig = new JiraRendererConfiguration(mockApplicationProperties, mockVelocityRequestContextFactory);
        JiraTestUtil.resetRequestAndResponse();
    }

    @Test
    public void getWebAppContextPathShouldReturnContextPath() {
        when(mockVelocityRequestContextFactory.getJiraVelocityRequestContext()).thenReturn(mockVelocityRequestContext);
        when(mockVelocityRequestContext.getBaseUrl()).thenReturn(BASE_HTTP_URL);

        assertThat("Base  path should be base url", jiraRendererConfig.getWebAppContextPath(), is(BASE_HTTP_URL));
    }

    @Test
    public void getWebAppContextPathShouldReturnCanonicalPathWithNoContext() {
        when(mockVelocityRequestContextFactory.getJiraVelocityRequestContext()).thenReturn(mockVelocityRequestContext);
        when(mockVelocityRequestContext.getBaseUrl()).thenReturn("");
        when(mockVelocityRequestContext.getCanonicalBaseUrl()).thenReturn(BASE_HTTPS_URL_NOCONTEXT);

        assertThat("Base  path should be canonical", jiraRendererConfig.getWebAppContextPath(), is(BASE_HTTPS_URL_NOCONTEXT));
    }
}
