package com.atlassian.jira.issue.export.customfield.layout;

import com.atlassian.jira.issue.export.FieldExportParts;
import com.atlassian.jira.issue.export.FieldExportPartsBuilder;
import com.atlassian.jira.issue.fields.Field;
import com.google.common.collect.ImmutableList;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


public class TestIssueSearchCsvExportLayoutBuilder {

    private IssueSearchCsvExportLayoutBuilder builder;

    @Mock
    Field field;

    @Before
    public void setUp() {
        builder = new IssueSearchCsvExportLayoutBuilder();
    }

    @Test
    public void testRegistersTheMaximumColumnsForAFieldPart() {
        final FieldExportParts singleColumnField = FieldExportPartsBuilder.buildSinglePartRepresentation("id", "label", "value");
        final FieldExportParts multiColumnField = FieldExportPartsBuilder.buildSinglePartRepresentation("id", "label", Stream.of("value1", "value2"));

        builder.registerFieldRepresentation(field, singleColumnField);
        builder.registerFieldRepresentation(field, multiColumnField);

        final IssueSearchCsvExportLayout layout = builder.build();
        assertThat(layout.getFieldLayouts(), hasSize(1));
        assertThat(layout.getFieldLayouts().get(0).getSubFields(), hasSize(1));
        assertThat(layout.getFieldLayouts().get(0).getSubFields().get(0).getColumnCount(), is(2));
    }

    /**
     * If someone was to create an item with label: "firstlabel" and then the same item with a different label it would
     * ignore any subsequent label and use the first one. This shouldn't be done anyway.
     */
    @Test
    public void testStoresTheFirstLabelForTheFieldPart() {
        final FieldExportParts firstFieldRepresentation = FieldExportPartsBuilder.buildSinglePartRepresentation("id", "firstlabel", "value");
        final FieldExportParts secondFieldRepresentation = FieldExportPartsBuilder.buildSinglePartRepresentation("id", "secondlabel", "value");

        builder.registerFieldRepresentation(field, firstFieldRepresentation);
        builder.registerFieldRepresentation(field, secondFieldRepresentation);

        final IssueSearchCsvExportLayout layout = builder.build();
        assertThat(layout.getFieldLayouts(), hasSize(1));
        assertThat(layout.getFieldLayouts().get(0).getSubFields(), hasSize(1));
        assertThat(layout.getFieldLayouts().get(0).getSubFields().get(0).getColumnHeader(), equalTo("firstlabel"));
    }

    @Test
    public void testPreserveGivenFieldOrder() {
        final List<Field> expectedFields = ImmutableList.of(
                mockField("first"),
                mockField("second"),
                mockField("third")
        );

        for (final Field field : expectedFields) {
            builder.registerFieldRepresentation(
                    field,
                    FieldExportPartsBuilder.buildSinglePartRepresentation("id", "firstlabel", "value")
            );
        }

        final List<Field> actualFields = builder.build().getFieldLayouts().stream()
                .map(IssueSearchCsvExportLayout.FieldLayout::getField)
                .collect(Collectors.toList());

        assertThat(actualFields, contains(expectedFields.toArray()));
    }

    private static Field mockField(String name) {
        final Field field = mock(Field.class);
        when(field.getName()).thenReturn(name);
        when(field.toString()).thenReturn(String.format("Mock Field \"%s\"", name));
        return field;
    }
}
