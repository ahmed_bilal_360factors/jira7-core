package com.atlassian.jira.bc.dataimport;

import com.atlassian.jira.upgrade.UpgradeConstraints;
import com.atlassian.jira.util.BuildUtilsInfo;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


public class ConstrainedBuildUtilsInfoTest {

    private UpgradeConstraints constraints;
    private BuildUtilsInfo delegateBuildUtils;
    private ConstrainedBuildUtilsInfo constrainedBuildUtilsInfo;

    @Before
    public void setup() {
        constraints = mock(UpgradeConstraints.class);
        delegateBuildUtils = mock(BuildUtilsInfo.class);
        constrainedBuildUtilsInfo = new ConstrainedBuildUtilsInfo(constraints, delegateBuildUtils);
    }

    @Test
    public void applicationNumberIsTakenFromUpgradeConstraints() {
        when(constraints.getTargetDatabaseBuildNumber()).thenReturn(70013);
        assertThat(constrainedBuildUtilsInfo.getApplicationBuildNumber(), is(70013));
    }

    @Test
    public void currentBuildNumberIsTakenFromUpgradeConstraints() {
        when(constraints.getTargetDatabaseBuildNumber()).thenReturn(70013);
        assertThat(constrainedBuildUtilsInfo.getCurrentBuildNumber(), is("70013"));
    }

    @Test
    public void otherImportantMethodsAreDelegatedToUnderlyingBuildUtilsInfo() {
        when(delegateBuildUtils.getMinimumUpgradableBuildNumber()).thenReturn("6400");
        when(delegateBuildUtils.getVersion()).thenReturn("7.0.1");
        assertThat(constrainedBuildUtilsInfo.getMinimumUpgradableBuildNumber(), is("6400"));
        assertThat(constrainedBuildUtilsInfo.getVersion(), is("7.0.1"));
    }
}