package com.atlassian.jira.web.action.setup;

import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.mock.MockFeatureManager;
import com.atlassian.jira.onboarding.FirstUseFlow;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.web.landingpage.LandingPageRedirectManager;
import com.atlassian.jira.web.landingpage.StaticUrlRedirect;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.mockito.Mock;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class TestSetupCompleteRedirectHelper {
    @Rule
    public RuleChain mocksInContainer = MockitoMocksInContainer.forTest(this);

    @Mock
    private ApplicationUser user;

    @Mock
    private FirstUseFlow firstUseFlow;

    @AvailableInContainer
    private LandingPageRedirectManager redirectManager = new LandingPageRedirectManager(new MockFeatureManager());

    private SetupCompleteRedirectHelper redirectHelper;

    @Before
    public void setUp() {
        redirectHelper = new SetupCompleteRedirectHelper();
    }

    @Test
    public void shouldRedirectToUrlProvidedByOnboardingFirst() {
        final String redirectUrl = "/some/fancy/url";

        redirectManager.registerRedirect(new StaticUrlRedirect(redirectUrl, (user) -> true), 0);
        assertThat(redirectHelper.getRedirectUrl(user), is(redirectUrl));
    }

    @Test
    public void shouldRedirectToDashboardIfOnboardingDoesntProvideUrl() {
        assertThat(redirectHelper.getRedirectUrl(user), is("/secure/Dashboard.jspa"));
    }
}
