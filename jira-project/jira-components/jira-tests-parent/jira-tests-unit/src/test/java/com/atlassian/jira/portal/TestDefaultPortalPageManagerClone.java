package com.atlassian.jira.portal;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.fugue.Option;
import com.atlassian.gadgets.dashboard.Color;
import com.atlassian.gadgets.dashboard.Layout;
import com.atlassian.gadgets.plugins.PluginGadgetSpec;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.dashboard.permission.GadgetPermissionManager;
import com.atlassian.jira.instrumentation.InstrumentationListenerManager;
import com.atlassian.jira.instrumentation.InstrumentationLogger;
import com.atlassian.jira.portal.events.DashboardCreated;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.sharing.ShareManager;
import com.atlassian.jira.sharing.SharePermission;
import com.atlassian.jira.sharing.SharePermissionImpl;
import com.atlassian.jira.sharing.index.MockSharedEntityIndexer;
import com.atlassian.jira.sharing.type.GlobalShareType;
import com.atlassian.jira.sharing.type.GroupShareType;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.plugin.ModuleCompleteKey;
import com.atlassian.plugin.Plugin;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Maps;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import java.net.URI;
import java.util.Collections;
import java.util.Map;

import static com.atlassian.fugue.Option.some;
import static com.atlassian.gadgets.Vote.ALLOW;
import static com.atlassian.gadgets.Vote.DENY;
import static com.atlassian.gadgets.dashboard.Color.color1;
import static com.atlassian.jira.portal.PortalPage.portalPage;
import static com.atlassian.jira.sharing.SharedEntity.SharePermissions;
import static com.atlassian.jira.sharing.SharedEntity.SharePermissions.PRIVATE;
import static com.atlassian.jira.util.collect.CollectionBuilder.newBuilder;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyMapOf;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Unit tests for {@link com.atlassian.jira.issue.search.DefaultSearchRequestManager}
 *
 * @since v3.13
 */
@RunWith(MockitoJUnitRunner.class)
public class TestDefaultPortalPageManagerClone {
    private static final MockApplicationUser USER = new MockApplicationUser("admin");
    private static final Long PORTLET_ID = 666L;
    private static final String DASHBOARD = "dashboard";
    private static final String OWEN = "user";
    private static final Long PAGE1_ID = 10030L;
    private static final Map<String, String> PREFS = ImmutableMap.of("pref1", "value1", "pref2", "value2");
    private static final Option<URI> XML_URI = Option.some(URI.create("http://www.google.com"));
    private static final Option<ModuleCompleteKey> MODULE_KEY = Option.some(new ModuleCompleteKey("com.atlassian.gadgets.test:complete.key"));
    private static final SharePermissions SHARE_PERMISSIONS = new SharePermissions(ImmutableSet.<SharePermission>of(
            new SharePermissionImpl(GroupShareType.TYPE, "jira-user", null),
            new SharePermissionImpl(GlobalShareType.TYPE, null, null)
    ));
    private final PortalPage defaultPage = PortalPage.id(PAGE1_ID).name("one").description("one description").owner(USER).build();
    private final PortalPage portalPageWithPortlets = PortalPage.id(PORTLET_ID).name(DASHBOARD).description(DASHBOARD).owner(new MockApplicationUser(OWEN)).layout(Layout.AA).version(0L).build();

    @Mock
    private PortalPageStore portalPageStore;
    @Mock
    private ShareManager shareManager;
    @Mock
    private PortletConfigurationManager portletConfigurationManager;
    @Mock
    private InstrumentationListenerManager instrumentationListenerManager;
    @Mock
    private EventPublisher eventPublisher;
    @Mock
    private JiraAuthenticationContext authenticationContext;
    @Mock
    private GadgetPermissionManager gadgetPermissionManager;
    @Mock
    private Plugin plugin;

    private PluginGadgetSpec pluginGadgetSpec;
    private DefaultPortalPageManager portalPageManager;

    @Before
    public void setUp() throws Exception {
        when(portalPageStore.create(any(PortalPage.class))).thenAnswer(new Answer<PortalPage>() {
            @Override
            public PortalPage answer(final InvocationOnMock invocationOnMock) throws Throwable {
                return (PortalPage) invocationOnMock.getArguments()[0];
            }
        });
        portalPageManager = new DefaultPortalPageManager(shareManager, portalPageStore, portletConfigurationManager, new MockSharedEntityIndexer(), eventPublisher,
                authenticationContext) {
            @Override
            GadgetPermissionManager getGadgetPermissionManager() {
                return gadgetPermissionManager;
            }
        };
        pluginGadgetSpec = PluginGadgetSpec.builder()
                .plugin(plugin)
                .location("location")
                .params(Maps.<String, String>newHashMap())
                .moduleKey("gadget")
                .build();
    }

    @Test(expected = IllegalArgumentException.class)
    public void test_createWithClone_NullPortalPage() throws Exception {
        final PortalPageManager portalPageManager = this.portalPageManager;
        portalPageManager.createBasedOnClone(USER, null, defaultPage);
    }

    @Test(expected = IllegalArgumentException.class)
    public void test_createWithClone_NullPortalPageClone() throws Exception {
        final PortalPageManager portalPageManager = this.portalPageManager;
        portalPageManager.createBasedOnClone((ApplicationUser) null, defaultPage, null);
    }

    @Test
    public void test_createWithClone_NoPerms() throws Exception {
        final PortalPage portalPage = portalPage(defaultPage).permissions(PRIVATE).build();
        when(portletConfigurationManager.getByPortalPage(portalPageWithPortlets.getId())).thenReturn(Collections.<PortletConfiguration>emptyList());
        when(shareManager.updateSharePermissions(portalPage)).thenReturn(PRIVATE);

        final PortalPage clonedPage = this.portalPageManager.createBasedOnClone(USER, defaultPage, portalPageWithPortlets);
        assertNotNull(clonedPage);
        assertEquals(defaultPage, clonedPage);
        assertTrue(clonedPage.getPermissions().isPrivate());
    }

    @Test
    public void test_createWithClone_hasPerms() throws Exception {
        final ApplicationUser user = mock(ApplicationUser.class);
        final PortalPage portalPage = portalPage(defaultPage).permissions(SHARE_PERMISSIONS).build();

        when(portletConfigurationManager.getByPortalPage(portalPageWithPortlets.getId())).thenReturn(Collections.<PortletConfiguration>emptyList());
        when(shareManager.updateSharePermissions(portalPage)).thenReturn(SHARE_PERMISSIONS);
        when(authenticationContext.getUser()).thenReturn(user);

        final PortalPage clonedPage = portalPageManager.createBasedOnClone(USER, portalPage, portalPageWithPortlets);
        assertNotNull(clonedPage);
        assertEquals(portalPage, clonedPage);
        assertEquals(SHARE_PERMISSIONS, clonedPage.getPermissions());
        verify(eventPublisher).publish(new DashboardCreated(clonedPage, user));
    }

    @Test
    public void test_createWithClone_gadgets_hasPerms() throws Exception {
        final PortletConfiguration gadget = new PortletConfigurationImpl(PORTLET_ID, PAGE1_ID, 1, 1, XML_URI, color1, PREFS, MODULE_KEY);
        final PortalPage portalPage = portalPage(defaultPage).permissions(SHARE_PERMISSIONS).build();

        when(portletConfigurationManager.getByPortalPage(portalPageWithPortlets.getId())).thenReturn(ImmutableList.of(gadget));
        when(shareManager.updateSharePermissions(portalPage)).thenReturn(SHARE_PERMISSIONS);
        when(gadgetPermissionManager.getPluginGadgetSpec(XML_URI.get())).thenReturn(some(pluginGadgetSpec));
        when(gadgetPermissionManager.voteOn(pluginGadgetSpec, USER)).thenReturn(ALLOW);

        portalPageManager.createBasedOnClone(USER, portalPage, portalPageWithPortlets);
        verify(portletConfigurationManager).addDashBoardItem(PAGE1_ID, 1, 1, XML_URI, color1, PREFS, MODULE_KEY);
    }

    @Test
    public void test_createWithClone_gadgets_hasPerms_hasNotUriSpec() throws Exception {
        final PortletConfiguration gadget = new PortletConfigurationImpl(PORTLET_ID, PAGE1_ID, 1, 1, Option.<URI>none(), color1, PREFS, MODULE_KEY);
        final PortalPage portalPage = portalPage(defaultPage).permissions(SHARE_PERMISSIONS).build();

        when(portletConfigurationManager.getByPortalPage(portalPageWithPortlets.getId())).thenReturn(newBuilder(gadget).asList());
        when(shareManager.updateSharePermissions(portalPage)).thenReturn(SHARE_PERMISSIONS);
        when(gadgetPermissionManager.getPluginGadgetSpec(null)).thenReturn(Option.<PluginGadgetSpec>none());

        portalPageManager.createBasedOnClone(USER, portalPage, portalPageWithPortlets);
        verify(portletConfigurationManager).addDashBoardItem(PAGE1_ID, 1, 1, Option.<URI>none(), color1, PREFS, MODULE_KEY);
    }

    @Test
    public void test_createWithClone_gadgets_hasPerms_hasNotAnyDefinition() throws Exception {
        final PortletConfiguration gadget = new PortletConfigurationImpl(PORTLET_ID, PAGE1_ID, 1, 1, Option.<URI>none(), color1, PREFS, Option.<ModuleCompleteKey>none());
        final PortalPage portalPage = portalPage(defaultPage).permissions(SHARE_PERMISSIONS).build();

        when(portletConfigurationManager.getByPortalPage(portalPageWithPortlets.getId())).thenReturn(newBuilder(gadget).asList());
        when(shareManager.updateSharePermissions(portalPage)).thenReturn(SHARE_PERMISSIONS);
        when(gadgetPermissionManager.getPluginGadgetSpec(null)).thenReturn(Option.<PluginGadgetSpec>none());

        portalPageManager.createBasedOnClone(USER, portalPage, portalPageWithPortlets);
        verify(portletConfigurationManager, never()).addDashBoardItem(anyLong(), anyInt(), anyInt(), Matchers.<Option<URI>>any(),
                any(Color.class), anyMapOf(String.class, String.class), Matchers.<Option<ModuleCompleteKey>>any());
    }

    @Test
    public void test_createWithClone_gadgets_noPerms() throws Exception {
        final PortletConfiguration gadget = new PortletConfigurationImpl(PORTLET_ID, 123L, 1, 1, XML_URI, color1, PREFS, Option.<ModuleCompleteKey>none());
        final PortalPage portalPage = portalPage(defaultPage).permissions(SHARE_PERMISSIONS).build();

        when(portletConfigurationManager.getByPortalPage(portalPageWithPortlets.getId())).thenReturn(newBuilder(gadget).asList());
        when(shareManager.updateSharePermissions(portalPage)).thenReturn(SHARE_PERMISSIONS);
        when(gadgetPermissionManager.getPluginGadgetSpec(XML_URI.get())).thenReturn(some(pluginGadgetSpec));
        when(gadgetPermissionManager.voteOn(pluginGadgetSpec, USER)).thenReturn(DENY);

        portalPageManager.createBasedOnClone(USER, portalPage, portalPageWithPortlets);
        verify(portletConfigurationManager, never()).addDashBoardItem(anyLong(), anyInt(), anyInt(), Matchers.<Option<URI>>any(),
                any(Color.class), anyMapOf(String.class, String.class), Matchers.<Option<ModuleCompleteKey>>any());
    }
}
