package com.atlassian.jira.web.action.setup;

import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.util.BuildUtilsInfo;
import com.atlassian.jira.util.JiraProductInformation;
import com.atlassian.jira.web.HttpServletVariables;
import com.atlassian.license.SIDManager;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.hamcrest.FeatureMatcher;
import org.hamcrest.Matcher;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.util.List;

import static com.atlassian.collectors.CollectorsUtil.toImmutableListWithSizeOf;
import static com.atlassian.jira.matchers.FeatureMatchers.hasFeature;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItem;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

public class MyAtlassianComRedirectTest {

    public static final String SID = "1234-5678-90AB-CDEF";
    public static final String VERSION = "7.8.9";
    public static final int BUILD_NUMBER = 78900;
    public static final String PRODUCT = "jira-world-domination";
    @Rule
    public MockitoRule jUnit = MockitoJUnit.rule();
    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Mock
    private JiraProductInformation jiraProductInformation;

    @Mock
    private BuildUtilsInfo buildUtilsInfo;

    @Mock
    private SIDManager sidManager;

    @Mock
    private ApplicationProperties applicationProperties;

    @Mock
    private HttpServletVariables servletVariables;

    @Mock
    private HttpServletRequest httpServletRequest;

    @InjectMocks
    private MyAtlassianComRedirect macHelper;

    @Before
    public void prepareBaseUrl() throws Exception {
        when(servletVariables.getHttpRequest()).thenReturn(httpServletRequest);

        when(httpServletRequest.getScheme()).thenReturn("https");
        when(httpServletRequest.getServerName()).thenReturn("testhost");
        when(httpServletRequest.getServerPort()).thenReturn(6666);
        when(httpServletRequest.getContextPath()).thenReturn("/jira-app/");
    }

    @Before
    public void prepareJiraData() throws Exception {
        when(sidManager.generateSID()).thenReturn(SID);
        when(buildUtilsInfo.getVersion()).thenReturn(VERSION);
        when(buildUtilsInfo.getApplicationBuildNumber()).thenReturn(BUILD_NUMBER);
        when(jiraProductInformation.getLicenseProductKey()).thenReturn(PRODUCT);
    }


    @Test
    public void resultingUrlShouldNotHaveDoubleSlashAfterContextPathIfProvidedCallbackStartsWithSlash() throws Exception {
        final String result = macHelper.newRedirectWithCallbackTo("/myAction.jspa").buildWithPostReturnParameter("license");
        final URI uri = new URI(result);
        final List<NameValuePair> params = URLEncodedUtils.parse(uri, "UTF-8");

        assertThat(params, hasItem(withKeyAndValue("licensefieldname", "license")));
        assertThat(params, hasItem(withKeyAndValue("callback", "https://testhost:6666/jira-app/myAction.jspa")));
        assertLinkHasExpectedValues(uri);
    }

    @Test
    public void resultingUrlShouldHaveSlashAfterContextPathIfProvidedCallbackDoesNotStartsWithSlashNorContextStartsWithSlash() throws Exception {
        when(httpServletRequest.getContextPath()).thenReturn("/jira-app");

        final String result = macHelper.newRedirectWithCallbackTo("myAction.jspa").buildWithPostReturnParameter("license");
        final URI uri = new URI(result);
        final List<NameValuePair> params = URLEncodedUtils.parse(uri, "UTF-8");

        assertThat(params, hasItem(withKeyAndValue("licensefieldname", "license")));
        assertThat(params, hasItem(withKeyAndValue("callback", "https://testhost:6666/jira-app/myAction.jspa")));
        assertLinkHasExpectedValues(uri);
    }

    @Test
    public void shouldHanldeVeryShortStringsOnInput() throws Exception {
        when(httpServletRequest.getContextPath()).thenReturn("/");

        final String result = macHelper.newRedirectWithCallbackTo("/").buildWithPostReturnParameter("l");
        final URI uri = new URI(result);
        final List<NameValuePair> params = URLEncodedUtils.parse(uri, "UTF-8");

        assertThat(params, hasItem(withKeyAndValue("licensefieldname", "l")));
        assertThat(params, hasItem(withKeyAndValue("callback", "https://testhost:6666/")));
        assertLinkHasExpectedValues(uri);
    }

    @Test
    public void shouldThrowExceptionOnNullCallback() throws Exception {
        exception.expect(IllegalArgumentException.class);
        macHelper.newRedirectWithCallbackTo(null);
    }

    @Test
    public void shouldThrowExceptionOnEmptyCallback() throws Exception {
        exception.expect(IllegalArgumentException.class);
        macHelper.newRedirectWithCallbackTo("");
    }

    @Test
    public void shouldThrowExceptionOnNullDataAttribute() throws Exception {
        exception.expect(IllegalArgumentException.class);
        macHelper.newRedirectWithCallbackTo("myAction.jspa").buildWithPostReturnParameter(null);
    }

    @Test
    public void shouldThrowExceptionOnEmptyDataAttribute() throws Exception {
        exception.expect(IllegalArgumentException.class);
        macHelper.newRedirectWithCallbackTo("myAction.jspa").buildWithPostReturnParameter("");
    }

    private void assertLinkHasExpectedValues(final URI uri) {
        final List<NameValuePair> params = URLEncodedUtils.parse(uri, "UTF-8");
        assertThat(uri.getScheme(), equalTo("https"));
        assertThat(uri.getHost(), equalTo("www.atlassian.com"));
        assertThat(uri.getPath(), equalTo("/ex/GenerateLicense.jspa"));
        assertThat(params.stream().map(NameValuePair::getName).collect(toImmutableListWithSizeOf(params)),
                containsInAnyOrder("utm_nooverride", "ref", "product", "version", "build", "sid", "licensefieldname", "callback"));

        assertThat(params, hasItem(withKeyAndValue("utm_nooverride", "1")));
        assertThat(params, hasItem(withKeyAndValue("ref", "prod")));
        assertThat(params, hasItem(withKeyAndValue("sid", SID)));
        assertThat(params, hasItem(withKeyAndValue("version", VERSION)));
        assertThat(params, hasItem(withKeyAndValue("build", Integer.toString(BUILD_NUMBER))));
        assertThat(params, hasItem(withKeyAndValue("product", PRODUCT)));
    }

    private Matcher<NameValuePair> withKeyAndValue(final String key, final String value) {
        return allOf(hasKeyThat(equalTo(key)), hasValueThat(equalTo(value)));
    }

    private FeatureMatcher<NameValuePair, String> hasKeyThat(final Matcher<String> matcher) {
        return hasFeature(NameValuePair::getName, matcher, "has Key", "key");
    }

    private FeatureMatcher<NameValuePair, String> hasValueThat(final Matcher<String> matcher) {
        return hasFeature(NameValuePair::getValue, matcher, "has Value", "value");
    }

}