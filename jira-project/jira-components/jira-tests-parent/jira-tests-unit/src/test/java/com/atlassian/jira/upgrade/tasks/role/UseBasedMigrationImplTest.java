package com.atlassian.jira.upgrade.tasks.role;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.jira.application.ApplicationKeys;
import com.atlassian.jira.auditing.AssociatedItem;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Set;

import static com.atlassian.jira.upgrade.tasks.role.TestUtils.asGroups;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

/**
 * @since 7.0
 */
public class UseBasedMigrationImplTest {
    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @Mock
    private GlobalPermissionDao globalPermissionDao;

    @Test
    public void addsGroupsFromAdminSysadminAndUseToApplicationsNoOverlap() {
        //given
        final Set<Group> useGroups = asGroups("use-only");
        final Set<Group> adminGroups = asGroups("admin-only");
        final Set<ApplicationKey> roles = ImmutableSet.of(ApplicationKeys.CORE);
        final MigrationState state = TestUtils.emptyState();

        when(globalPermissionDao.groupsWithUsePermission()).thenReturn(useGroups);
        when(globalPermissionDao.groupsWithAdminPermission()).thenReturn(adminGroups);

        final UseBasedMigrationImpl migration = new UseBasedMigrationImpl(globalPermissionDao);

        //when
        final MigrationState migratedState = migration.addUsePermissionToRoles(state, roles);

        //then
        final ApplicationRole coreRole = ApplicationRole.forKey(ApplicationKeys.CORE)
                .addGroups(adminGroups).addGroupsAsDefault(useGroups);
        assertThat(migratedState, new MigrationStateMatcher()
                .roles(coreRole)
                .log(
                        new AuditEntry(UseBasedMigrationImpl.class, "Migrated groups to jira-core: 2",
                                "Group with USE permission migrated to a role to ensure continued access"
                                        + " for associated users.",
                                AssociatedItem.Type.APPLICATION_ROLE,
                                "jira-core", true,
                                new MigrationChangedValue("admin-only", "USE", "jira-core"),
                                new MigrationChangedValue("use-only", "USE", "jira-core (default)"))));
    }

    @Test
    public void addsGroupsFromAdminSysadminAndUseToApplicationsWithOverlap() {
        //given
        final Set<Group> useGroups = asGroups("use-only", "common-group");
        final Set<Group> adminGroups = asGroups("admin-only", "common-group");
        final Set<Group> useOnlyGroups = Sets.difference(useGroups, adminGroups);
        final Set<ApplicationKey> roles = ImmutableSet.of(ApplicationKeys.SERVICE_DESK, ApplicationKeys.SOFTWARE);
        final MigrationState state = TestUtils.emptyState();

        when(globalPermissionDao.groupsWithAdminPermission()).thenReturn(adminGroups);
        when(globalPermissionDao.groupsWithUsePermission()).thenReturn(useGroups);

        final UseBasedMigrationImpl migration = new UseBasedMigrationImpl(globalPermissionDao);

        //when
        MigrationState migratedState = migration.addUsePermissionToRoles(state, roles);

        //then
        ApplicationRole coreRole = ApplicationRole.forKey(ApplicationKeys.SERVICE_DESK)
                .addGroups(adminGroups).addGroupsAsDefault(useOnlyGroups);
        ApplicationRole softwareRole = ApplicationRole.forKey(ApplicationKeys.SOFTWARE)
                .addGroups(adminGroups).addGroupsAsDefault(useOnlyGroups);

        final AuditEntry sdEntry = new AuditEntry(UseBasedMigrationImpl.class, "Migrated groups to jira-servicedesk: 3",
                "Group with USE permission migrated to a role to ensure continued access for associated users.",
                AssociatedItem.Type.APPLICATION_ROLE,
                "jira-servicedesk", true,
                new MigrationChangedValue("common-group", "USE", "jira-servicedesk"),
                new MigrationChangedValue("admin-only", "USE", "jira-servicedesk"),
                new MigrationChangedValue("use-only", "USE", "jira-servicedesk (default)"));
        final AuditEntry swEntry = new AuditEntry(UseBasedMigrationImpl.class, "Migrated groups to jira-software: 3",
                "Group with USE permission migrated to a role to ensure continued access for associated users.",
                AssociatedItem.Type.APPLICATION_ROLE,
                "jira-software", true,
                new MigrationChangedValue("common-group", "USE", "jira-software"),
                new MigrationChangedValue("admin-only", "USE", "jira-software"),
                new MigrationChangedValue("use-only", "USE", "jira-software (default)"));
        assertThat(migratedState, new MigrationStateMatcher()
                .roles(coreRole, softwareRole)
                .logs(sdEntry, swEntry));
    }

    @Test
    public void isNoOpWhenNoApplicationsPassed() {
        //given
        final Set<Group> useGroups = asGroups("use-only", "common-group");
        final Set<ApplicationKey> roles = ImmutableSet.of();

        when(globalPermissionDao.groupsWithUsePermission()).thenReturn(useGroups);

        final UseBasedMigrationImpl migration = new UseBasedMigrationImpl(globalPermissionDao);

        //when
        MigrationState migratedState = migration.addUsePermissionToRoles(TestUtils.emptyState(), roles);

        //then
        assertThat(migratedState, new MigrationStateMatcher().roles());
    }
}