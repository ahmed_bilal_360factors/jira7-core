package com.atlassian.jira.user;

import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.util.collect.CollectionBuilder;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isNull;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Tests for the {@link com.atlassian.jira.user.DefaultUserQueryHistoryManager}
 *
 * @since v4.0
 */
public class TestDefaultUserQueryHistoryManager {
    @Rule
    public final MockitoRule initMockito = MockitoJUnit.rule();
    @Mock
    private UserHistoryManager historyManager;
    @Mock
    private ApplicationProperties applicationProperties;
    private DefaultUserQueryHistoryManager queryHistoryManager;

    private ApplicationUser user = new MockApplicationUser("admin");

    @Before
    public void setUp() throws Exception {
        queryHistoryManager = new DefaultUserQueryHistoryManager(historyManager, applicationProperties);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testAddQueryNullQuery() throws Exception {
        queryHistoryManager.addQueryToHistory(user, null);
    }

    @Test
    public void testAddQueryNullUser() throws Exception {
        final String query = "project = \"homosapien\"";
        when(historyManager.getHistory(eq(UserHistoryItem.JQL_QUERY), isNull(ApplicationUser.class)))
                .thenReturn(CollectionBuilder.newBuilder(new UserHistoryItem(UserHistoryItem.JQL_QUERY, String.valueOf(query), query)).asList());

        when(applicationProperties.getDefaultBackedString("jira.max.JQLQuery.history.items")).thenReturn("10");

        queryHistoryManager.addQueryToHistory(null, query);

        verify(historyManager).addItemToHistory(UserHistoryItem.JQL_QUERY, null, String.valueOf(query.hashCode()), query);

        final List<UserHistoryItem> history = queryHistoryManager.getUserQueryHistory(null);
        assertEquals(1, history.size());
        assertEquals(query, history.get(0).getData());
    }

    @Test
    public void testAddQueries() throws Exception {
        final String query1 = "project = \"homosapien\"";
        final String query2 = "project = \"monkeys\"";
        historyManager.addItemToHistory(UserHistoryItem.JQL_QUERY, user, String.valueOf(query1.hashCode()), query1);
        historyManager.addItemToHistory(UserHistoryItem.JQL_QUERY, user, String.valueOf(query2.hashCode()), query2);

        when(historyManager.getHistory(UserHistoryItem.JQL_QUERY, user)).thenReturn(CollectionBuilder.newBuilder(new UserHistoryItem(UserHistoryItem.JQL_QUERY, String.valueOf(query2.hashCode()), query2), new UserHistoryItem(UserHistoryItem.JQL_QUERY, String.valueOf(query1.hashCode()), query1)).asList());
        when(applicationProperties.getDefaultBackedString("jira.max.JQLQuery.history.items")).thenReturn("10");

        queryHistoryManager.addQueryToHistory(user, query1);
        queryHistoryManager.addQueryToHistory(user, query2);

        final List<UserHistoryItem> history = queryHistoryManager.getUserQueryHistory(user);
        assertEquals(2, history.size());
        assertEquals(query2, history.get(0).getData());
        assertEquals(query1, history.get(1).getData());
    }

    @Test
    public void testAddMaximumQueries() {
        String[] queries = new String[25];
        for (int i = 0; i < queries.length; i++) {
            queries[i] = new String("query " + i);
            historyManager.addItemToHistory(UserHistoryItem.JQL_QUERY, user, String.valueOf(queries[i].hashCode()), queries[i]);
        }

        List<UserHistoryItem> historyItems = new ArrayList<>();

        for (int i = 0; i < queries.length; i++) {
            String query = queries[i];
            historyItems.add(new UserHistoryItem(UserHistoryItem.JQL_QUERY, String.valueOf(query.hashCode()), query));
        }

        when(historyManager.getHistory(UserHistoryItem.JQL_QUERY, user)).thenReturn(historyItems);
        when(applicationProperties.getDefaultBackedString("jira.max.JQLQuery.history.items")).thenReturn("10");

        for (int i = 0; i < queries.length; i++) {
            queries[i] = new String("query " + i);
            queryHistoryManager.addQueryToHistory(user, queries[i]);
        }

        final List<UserHistoryItem> history = queryHistoryManager.getUserQueryHistory(user);
        assertEquals(10, history.size());
        for (int i = 0; i < 10; i++) {
            assertEquals(queries[i], history.get(i).getData());
        }
    }

}
