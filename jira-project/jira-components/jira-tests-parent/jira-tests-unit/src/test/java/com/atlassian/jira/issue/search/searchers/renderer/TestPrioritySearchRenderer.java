package com.atlassian.jira.issue.search.searchers.renderer;

import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.issue.priority.Priority;
import com.google.common.collect.ImmutableList;
import org.hamcrest.Matchers;
import org.junit.Test;

import java.util.Collection;

import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestPrioritySearchRenderer {
    @Test
    public void testGetSelectListOptions() throws Exception {
        final ConstantsManager mockConstantsManager = mock(ConstantsManager.class);
        final ImmutableList<Priority> priorities = ImmutableList.of();
        when(mockConstantsManager.getPriorityObjects()).thenReturn(priorities);
        final PrioritySearchRenderer searchRenderer = new PrioritySearchRenderer("test", mockConstantsManager, null, null, null, null);

        final Collection<Priority> selectListOptions = searchRenderer.getSelectListOptions(null);

        assertThat(selectListOptions, Matchers.sameInstance(priorities));

    }
}
