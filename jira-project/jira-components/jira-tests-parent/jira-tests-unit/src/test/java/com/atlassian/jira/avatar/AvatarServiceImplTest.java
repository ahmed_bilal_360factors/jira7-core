package com.atlassian.jira.avatar;

import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.config.util.EncodingConfiguration;
import com.atlassian.jira.icon.IconType;
import com.atlassian.jira.mock.MockAvatar;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.UserPropertyManager;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.velocity.VelocityRequestContext;
import com.atlassian.jira.util.velocity.VelocityRequestContextFactory;
import com.atlassian.mock.propertyset.MockPropertySet;
import com.opensymphony.module.propertyset.PropertySet;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.net.URI;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

/**
 * Unit tests for AvatarServiceImplTest.
 *
 * @since v4.3
 */
@RunWith(MockitoJUnitRunner.class)
public class AvatarServiceImplTest {
    private static final String BASE_URL = "http://jira.atlassian.com";
    private static final String LIBRAVATAR_API = "http://cdn.libravatar.org/avatar/";
    private static final String FRED_HASH = "6255165076a5e31273cbda50bb9f9636";
    private static final String SMALL_GRAVATAR_PARAMS = "?d=mm&s=16";
    private static final String FRED_SMALL_GRAVATAR_URL = "http://www.gravatar.com/avatar/" + FRED_HASH + SMALL_GRAVATAR_PARAMS;
    private static final String FRED_LIBRAVATAR_URL = LIBRAVATAR_API + FRED_HASH + SMALL_GRAVATAR_PARAMS;
    private static final long defaultProjectAvatarId = 10L;

    @Mock
    private AvatarManager avatarManager;
    @Mock
    private UserManager userManager;
    @Mock
    private UserPropertyManager userPropertyManager;
    @Mock
    private VelocityRequestContext context;
    @Mock
    private VelocityRequestContextFactory factory;
    @Mock
    private ApplicationProperties applicationProperties;
    @Mock
    private GravatarSettings gravatarSettings;
    @Mock
    private EncodingConfiguration encodingConfiguration;

    private AvatarServiceImpl avatarService;

    @Before
    public void setup() {
        when(applicationProperties.getEncoding()).thenReturn("utf-8");
        when(factory.getJiraVelocityRequestContext()).thenReturn(context);
        when(avatarManager.getDefaultAvatarId(IconType.PROJECT_ICON_TYPE)).thenReturn(defaultProjectAvatarId);
        avatarService = new AvatarServiceImpl(userManager, avatarManager, userPropertyManager, factory,
                applicationProperties, gravatarSettings, encodingConfiguration);
    }

    @Test
    public void shouldReturnCorrectProjectDefaultAvatarURL() throws Exception {
        long defaultProjectAvatarId = 10L;

        when(context.getCanonicalBaseUrl()).thenReturn(BASE_URL);
        when(context.getBaseUrl()).thenReturn("/jira");

        URI url = avatarService.getProjectDefaultAvatarURL(Avatar.Size.SMALL);
        assertThat(url.toString(), equalTo("/jira/secure/projectavatar?size=xsmall&avatarId=" + defaultProjectAvatarId));
    }

    @Test
    public void anonymousNotHasCustomAvatar() throws Exception {
        final MockApplicationUser satoshiNakamoto = new MockApplicationUser("SatoshiNakamoto");
        MockPropertySet ps = new MockPropertySet();
        when(userPropertyManager.getPropertySet(satoshiNakamoto)).thenReturn(ps);

        assertThat(avatarService.hasCustomUserAvatar(null, (ApplicationUser) null), equalTo(false));
        assertThat(avatarService.hasCustomUserAvatar(null, satoshiNakamoto), equalTo(false));
        assertThat(avatarService.hasCustomUserAvatar(satoshiNakamoto, satoshiNakamoto), equalTo(false));
    }

    @Test
    public void externalAvatarIsCustom() throws Exception {
        final MockApplicationUser satoshiNakamoto = new MockApplicationUser("SatoshiNakamoto");
        MockPropertySet ps = new MockPropertySet();
        when(userPropertyManager.getPropertySet(satoshiNakamoto)).thenReturn(ps);
        when(gravatarSettings.isAllowGravatars()).thenReturn(true);
        assertThat(avatarService.hasCustomUserAvatar(satoshiNakamoto, satoshiNakamoto), equalTo(true));
    }

    @Test
    public void anonymousApplicationUserAvatarRequest() throws Exception {
        final ApplicationUser user = null;
        final Avatar mockAv = Mockito.mock(Avatar.class);
        when(avatarManager.getById(123L)).thenReturn(mockAv);
        final Avatar avatar = avatarService.getAvatar(user, user);
        assertThat(avatar, equalTo(avatar));
    }

    @Test
    public void anonymousUserAvatarRequest() throws Exception {
        final Avatar anon = Mockito.mock(Avatar.class);
        when(userManager.getUserByName("Froboz")).thenReturn(null);
        when(avatarManager.getAnonymousAvatarId()).thenReturn(123L);
        when(avatarManager.getById(123L)).thenReturn(anon);
        final Avatar avatar = avatarService.getAvatar(null, "Froboz");
        assertThat(avatar, equalTo(anon));
    }

    @Test
    public void gravatarShouldUseCustomApiUrlIfAvailable() throws Exception {
        final ApplicationUser knownUser = new MockApplicationUser("Fred", "Wilson", "fred@example.com");
        final PropertySet propertySet = new MockPropertySet();
        final Avatar whateverAvatar = new MockAvatar(123L, null, "image/png", IconType.USER_ICON_TYPE, knownUser.getUsername(), true);

        when(userManager.getUserByKey("fred@example.com")).thenReturn(knownUser);
        when(userManager.isUserExisting(knownUser)).thenReturn(true);
        when(userPropertyManager.getPropertySet(knownUser)).thenReturn(propertySet);
        when(gravatarSettings.isAllowGravatars()).thenReturn(true);
        when(gravatarSettings.getCustomApiAddress()).thenReturn(LIBRAVATAR_API);
        when(avatarManager.getDefaultAvatarId(IconType.USER_ICON_TYPE)).thenReturn(whateverAvatar.getId());

        String url = avatarService.getGravatarAvatarURL(knownUser, Avatar.Size.SMALL).get().toString();
        assertThat(url, equalTo(FRED_LIBRAVATAR_URL));
    }

    @Test
    public void gravatarShouldReturnCorrectUrlForFredAtExampleCom() throws Exception {
        final ApplicationUser knownUser = new MockApplicationUser("Fred", "Wilson", "fred@example.com");
        final Avatar whateverAvatar = new MockAvatar(123L, null, "image/png", IconType.USER_ICON_TYPE, knownUser.getUsername(), true);

        when(userManager.getUserByKey("fred@example.com")).thenReturn(knownUser);
        when(userManager.isUserExisting(knownUser)).thenReturn(true);
        when(userPropertyManager.getPropertySet(knownUser)).thenReturn(new MockPropertySet());
        when(gravatarSettings.isAllowGravatars()).thenReturn(true);
        when(avatarManager.getDefaultAvatarId(IconType.USER_ICON_TYPE)).thenReturn(whateverAvatar.getId());

        String url = avatarService.getGravatarAvatarURL(knownUser, Avatar.Size.SMALL).get().toString();
        assertThat(url, equalTo(FRED_SMALL_GRAVATAR_URL));
    }

    @Test
    public void getAvatarShouldReturnDefaultAvatarForUserWithNoConfiguredAvatar() throws Exception {
        long defaultAvatarId = 42L;
        String knownUsername = "known_user";
        ApplicationUser knownUser = new MockApplicationUser(knownUsername);
        Avatar defaultAvatar = new MockAvatar(defaultAvatarId, null, "image/png", IconType.USER_ICON_TYPE, null, true);

        when(userManager.getUserByName(knownUsername)).thenReturn(knownUser);
        when(userManager.isUserExisting(knownUser)).thenReturn(true);
        when(userPropertyManager.getPropertySet(knownUser)).thenReturn(new MockPropertySet());
        when(gravatarSettings.isAllowGravatars()).thenReturn(true);
        when(avatarManager.getDefaultAvatar(IconType.USER_ICON_TYPE)).thenReturn(defaultAvatar);

        Avatar avatar = avatarService.getAvatar(knownUser, knownUsername);
        assertThat(avatar, equalTo(defaultAvatar));
    }

    @Test
    public void serviceReturnsCanonicalAvatarUrl() throws Exception {
        final String knownUsername = "known_user";
        final ApplicationUser knownUser = new MockApplicationUser(knownUsername);

        when(context.getCanonicalBaseUrl()).thenReturn(BASE_URL);

        URI url = avatarService.getAvatarAbsoluteURL(knownUser, knownUsername, Avatar.Size.SMALL);
        assertThat(url.toString(), equalTo(BASE_URL + "/secure/useravatar?size=xsmall"));
    }

    @Test
    public void gravatarShouldReturnDefaultAvatarWhenEmailAddressIsNull() throws Exception {
        final String knownUsername = "known_user";
        final ApplicationUser knownUser = new MockApplicationUser(knownUsername);
        when(context.getCanonicalBaseUrl()).thenReturn(BASE_URL);
        when(gravatarSettings.isAllowGravatars()).thenReturn(true);

        URI url = avatarService.getAvatarAbsoluteURL(knownUser, knownUsername, Avatar.Size.SMALL);
        assertThat(url.toString(), equalTo(BASE_URL + "/secure/useravatar?size=xsmall"));
    }

    @Test
    public void gravatarShouldReturnCorrectUrlForFredAtExampleComInMixedCase() throws Exception {
        final ApplicationUser knownUser = new MockApplicationUser("Fred", "Wilson", "FrEd@eXamPle.CoM");
        final Avatar whateverAvatar = new MockAvatar(123L, null, "image/png", IconType.USER_ICON_TYPE, knownUser.getUsername(), true);

        when(userPropertyManager.getPropertySet(knownUser)).thenReturn(new MockPropertySet());
        when(gravatarSettings.isAllowGravatars()).thenReturn(true);
        when(avatarManager.getDefaultAvatarId(IconType.USER_ICON_TYPE)).thenReturn(whateverAvatar.getId());

        String url = avatarService.getGravatarAvatarURL(knownUser, Avatar.Size.SMALL).get().toString();
        assertThat(url, equalTo(FRED_SMALL_GRAVATAR_URL));
    }

}
