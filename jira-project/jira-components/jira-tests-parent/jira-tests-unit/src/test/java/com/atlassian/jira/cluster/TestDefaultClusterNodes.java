package com.atlassian.jira.cluster;

import com.atlassian.jira.util.BuildUtilsInfo;
import com.atlassian.jira.util.JiraUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.Instant;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @since v7.0.6
 */
@RunWith(MockitoJUnitRunner.class)
public class TestDefaultClusterNodes {

    private DefaultClusterNodes clusterNodes;

    @Mock
    private ClusterNodeProperties clusterNodeProperties;
    @Mock
    private OfBizClusterNodeStore ofBizClusterNodeStore;
    @Mock
    private BuildUtilsInfo buildUtilsInfo;

    @Test
    public void testGetHostnameSetInProperties() {
        when(clusterNodeProperties.getProperty(DefaultEhCacheConfigurationFactory.EHCACHE_LISTENER_HOSTNAME)).thenReturn("atlassian-test");

        invokeConstructor();

        assertThat(clusterNodes.buildHostname(), is("atlassian-test"));
    }

    @Test
    public void testNoHostnameInProperties() {
        when(clusterNodeProperties.getProperty(DefaultEhCacheConfigurationFactory.EHCACHE_LISTENER_HOSTNAME)).thenReturn(null);

        invokeConstructor();

        assertThat(clusterNodes.buildHostname(), is(JiraUtils.getHostname()));
    }

    @Test
    public void testUncleanShutdownAndRestart() {
        final Node node = newNode();
        mockSelf(node);
        mockDatabaseNode(node);

        invokeConstructor();
        clusterNodes.current();

        verifyNeverUpdated();
        verifyNeverCreated();
    }

    @Test
    public void testUncleanShutdownAndRestartOnNewBuildNumber() {
        final Node dbNode = newNode();
        final Node appNode = newBuildNumber(dbNode, 73002L);
        mockSelf(appNode);
        mockDatabaseNode(dbNode);

        invokeConstructor();
        clusterNodes.current();

        verifyUpdated(appNode);
        verifyNeverCreated();
    }

    @Test
    public void testUncleanShutdownAndRestartOnNewVersion() {
        final Node dbNode = newNode();
        final Node appNode = newVersion(dbNode, "7.3.1-SNAPSHOT");
        mockSelf(appNode);
        mockDatabaseNode(dbNode);

        invokeConstructor();
        clusterNodes.current();

        verifyUpdated(appNode);
        verifyNeverCreated();
    }

    private void verifyNeverUpdated() {
        verify(ofBizClusterNodeStore, never()).updateNode(any(), any(), any(), any(), any(), any());
    }

    private void verifyNeverCreated() {
        verify(ofBizClusterNodeStore, never()).createNode(any(), any(), any(), any(), any(), any());
    }

    private void verifyUpdated(final Node appNode) {
        verify(ofBizClusterNodeStore, times(1)).updateNode(appNode.getNodeId(), appNode.getState(), appNode.getIp(), appNode.getCacheListenerPort(), appNode.getNodeBuildNumber(), appNode.getNodeVersion());
    }

    private static Node newBuildNumber(final Node node, final long buildNumber) {
        return new Node(node.getNodeId(), node.getState(), node.getTimestamp(), node.getIp(), node.getCacheListenerPort(), buildNumber, node.getNodeVersion());
    }

    private static Node newVersion(final Node node, final String version) {
        return new Node(node.getNodeId(), node.getState(), node.getTimestamp(), node.getIp(), node.getCacheListenerPort(), node.getNodeBuildNumber(), version);
    }

    private static Node newNode() {
        return new Node("node-1", Node.NodeState.ACTIVE, Instant.now().toEpochMilli(), "127.0.0.1", 40001L, 73001L, "7.3.0-SNAPSHOT");
    }

    private void mockDatabaseNode(final Node node) {
        when(ofBizClusterNodeStore.getNode(node.getNodeId())).thenReturn(node);
    }

    private void mockSelf(final Node node) {
        when(clusterNodeProperties.getNodeId()).thenReturn(node.getNodeId());
        when(buildUtilsInfo.getVersion()).thenReturn(node.getNodeVersion());
        when(buildUtilsInfo.getApplicationBuildNumber()).thenReturn(Math.toIntExact(node.getNodeBuildNumber()));
        when(clusterNodeProperties.getProperty(DefaultEhCacheConfigurationFactory.EHCACHE_LISTENER_HOSTNAME)).thenReturn(node.getIp());
    }

    private void invokeConstructor() {
        clusterNodes = new DefaultClusterNodes(clusterNodeProperties, ofBizClusterNodeStore, buildUtilsInfo);
    }
}
