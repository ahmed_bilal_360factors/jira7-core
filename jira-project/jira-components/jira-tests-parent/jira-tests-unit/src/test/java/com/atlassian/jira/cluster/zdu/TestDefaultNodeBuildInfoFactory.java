package com.atlassian.jira.cluster.zdu;

import com.atlassian.jira.cluster.Node;
import com.atlassian.jira.model.querydsl.ClusterUpgradeStateDTO;
import com.atlassian.jira.util.BuildUtilsInfo;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestDefaultNodeBuildInfoFactory {

    @Mock
    private BuildUtilsInfo buildUtilsInfo;

    private DefaultNodeBuildInfoFactory factory;

    @Before
    public void setUp() {
        factory = new DefaultNodeBuildInfoFactory(buildUtilsInfo);
    }

    @Test
    public void createsInfoFromNode() {
        NodeBuildInfo nodeBuildInfo = factory.create(new Node("nodeId", Node.NodeState.ACTIVE, 0L, "", 0L, 12345L, "1.2.3"));

        assertThat(nodeBuildInfo.getBuildNumber(), is(12345L));
    }
    @Test
    public void createsInfoFromClusterUpgradeStateDTO() {
        NodeBuildInfo nodeBuildInfo = factory.create(new ClusterUpgradeStateDTO(0L, 0L, 12345L, "1.2.3", "", 1L));

        assertThat(nodeBuildInfo.getBuildNumber(), is(12345L));
    }
    @Test
    public void createsInfoFromParameters() {
        NodeBuildInfo nodeBuildInfo = factory.create(12345L, "1.2.3");

        assertThat(nodeBuildInfo.getBuildNumber(), is(12345L));
    }
    @Test
    public void createsInfoFromBuildUtilsInfo() {
        when(buildUtilsInfo.getApplicationBuildNumber()).thenReturn(12345);
        when(buildUtilsInfo.getVersion()).thenReturn("1.2.3");

        NodeBuildInfo nodeBuildInfo = factory.currentApplicationInfo();

        assertThat(nodeBuildInfo.getBuildNumber(), is(12345L));
    }

    @Test
    public void createdNodeBuildInfoHonorsEquality() {
        when(buildUtilsInfo.getApplicationBuildNumber()).thenReturn(12345);
        when(buildUtilsInfo.getVersion()).thenReturn("1.2.3");

        NodeBuildInfo nodeBuildInfo1 = factory.currentApplicationInfo();
        NodeBuildInfo nodeBuildInfo2 = factory.create(12345L, "1.2.3");

        assertThat(nodeBuildInfo1, is(nodeBuildInfo2));
    }

    @Test
    public void createdNodeBuildInfoHonorsInequalityInBuildNumber() {
        when(buildUtilsInfo.getApplicationBuildNumber()).thenReturn(54321);
        when(buildUtilsInfo.getVersion()).thenReturn("1.2.3");

        NodeBuildInfo nodeBuildInfo1 = factory.currentApplicationInfo();
        NodeBuildInfo nodeBuildInfo2 = factory.create(12345L, "1.2.3");

        assertThat(nodeBuildInfo1, not(is(nodeBuildInfo2)));
    }

    @Test
    public void createdNodeBuildInfoHonorsInequalityInVersion() {
        when(buildUtilsInfo.getApplicationBuildNumber()).thenReturn(12345);
        when(buildUtilsInfo.getVersion()).thenReturn("3.2.1");

        NodeBuildInfo nodeBuildInfo1 = factory.currentApplicationInfo();
        NodeBuildInfo nodeBuildInfo2 = factory.create(12345L, "1.2.3");

        assertThat(nodeBuildInfo1, not(is(nodeBuildInfo2)));
    }
}
