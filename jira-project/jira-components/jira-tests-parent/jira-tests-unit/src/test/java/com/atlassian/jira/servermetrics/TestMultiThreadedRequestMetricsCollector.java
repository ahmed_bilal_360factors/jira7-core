package com.atlassian.jira.servermetrics;

import com.atlassian.jira.application.install.ThrowableFunctions;
import com.atlassian.jira.matchers.FeatureMatchers;
import com.atlassian.jira.matchers.OptionalMatchers;
import com.google.common.base.Ticker;
import org.hamcrest.Matchers;
import org.junit.Test;

import javax.annotation.concurrent.NotThreadSafe;
import java.time.Duration;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static com.atlassian.jira.matchers.OptionalMatchers.none;
import static com.atlassian.jira.matchers.OptionalMatchers.some;
import static org.hamcrest.Matchers.any;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.core.Every.everyItem;
import static org.junit.Assert.assertThat;

@SuppressWarnings("Duplicates")
public class TestMultiThreadedRequestMetricsCollector {

    public static final String CHECKPOINT_1 = "checkpoint1";
    public static final String CHECKPOINT_2 = "checkpoint2";
    private static final String CHECKPOINT_3 = "checkpoint3";
    public static final String ACTIVITY_1 = "activity1";
    public static final String ACTIVITY_2 = "activity2";

    @NotThreadSafe
    class MockTicker extends Ticker {

        private volatile Duration currentValue = Duration.ZERO;

        public void set(Duration value) {
            currentValue = value;
        }

        public void elapse(Duration value) {
            currentValue = currentValue.plus(value);
        }

        @Override
        public long read() {
            return currentValue.toNanos();
        }
    }

    private final MockTicker ticker = new MockTicker();
    private final MultiThreadedRequestMetricsCollector testObj = new MultiThreadedRequestMetricsCollector(ticker);

    @Test
    public void inNotStartedThreadShouldNotCollectInformation() {
        ticker.elapse(Duration.ofMillis(1));
        testObj.checkpointReached("testCheckpoint");
        final Optional<TimingInformation> result = testObj.finishCollectionInCurrentThread();

        assertThat(result, is(none()));
    }

    @Test
    public void collectCheckpointInCurrentThread() {
        testObj.startCollectionInCurrentThread();
        testObj.checkpointReached(CHECKPOINT_1);
        ticker.elapse(Duration.ofMillis(1));
        testObj.checkpointReached(CHECKPOINT_2);
        ticker.elapse(Duration.ofMillis(2));
        testObj.checkpointReached(CHECKPOINT_3);
        ticker.elapse(Duration.ofMillis(3));

        final Optional<TimingInformation> result = testObj.finishCollectionInCurrentThread();

        assertThat(result,
                is(some(Matchers.allOf(
                        FeatureMatchers.hasFeature(
                                TimingInformation::getTotalTime,
                                equalTo(Duration.ofMillis(6)),
                                "TimingInformation totalTime",
                                "totalTime"),
                        FeatureMatchers.hasFeature(
                                TimingInformation::getTimingEventList,
                                contains(
                                        new CheckpointTiming(CHECKPOINT_1, Duration.ZERO),
                                        new CheckpointTiming(CHECKPOINT_2, Duration.ofMillis(1)),
                                        new CheckpointTiming(CHECKPOINT_3, Duration.ofMillis(3))),
                                "TimingInformation timingEventList",
                                "timingEventList"
                        ),
                        FeatureMatchers.hasFeature(
                                TimingInformation::getActivityDurations,
                                empty(),
                                "TimingInformation timingEventList",
                                "timingEventList"
                        )
                ))));
    }

    @Test
    public void collectCheckpointInCurrentThreadOnce() {
        testObj.startCollectionInCurrentThread();
        testObj.checkpointReached(CHECKPOINT_1);
        ticker.elapse(Duration.ofMillis(1));
        testObj.checkpointReached(CHECKPOINT_2);
        ticker.elapse(Duration.ofMillis(2));
        testObj.checkpointReachedOnce(CHECKPOINT_1);
        ticker.elapse(Duration.ofMillis(3));

        final Optional<TimingInformation> result = testObj.finishCollectionInCurrentThread();

        assertThat(result,
                is(some(Matchers.allOf(
                        FeatureMatchers.hasFeature(
                                TimingInformation::getTotalTime,
                                equalTo(Duration.ofMillis(6)),
                                "TimingInformation totalTime",
                                "totalTime"),
                        FeatureMatchers.hasFeature(
                                TimingInformation::getTimingEventList,
                                contains(
                                        new CheckpointTiming(CHECKPOINT_1, Duration.ZERO),
                                        new CheckpointTiming(CHECKPOINT_2, Duration.ofMillis(1))),
                                "TimingInformation timingEventList",
                                "timingEventList"
                        )
                ))));
    }

    @Test
    public void collectCheckpointInCurrentThreadOverride() {
        testObj.startCollectionInCurrentThread();
        testObj.checkpointReached(CHECKPOINT_1);
        ticker.elapse(Duration.ofMillis(1));
        testObj.checkpointReached(CHECKPOINT_2);
        ticker.elapse(Duration.ofMillis(2));
        testObj.checkpointReachedOverride(CHECKPOINT_1);
        ticker.elapse(Duration.ofMillis(3));

        final Optional<TimingInformation> result = testObj.finishCollectionInCurrentThread();

        assertThat(result,
                is(some(Matchers.allOf(
                        FeatureMatchers.hasFeature(
                                TimingInformation::getTotalTime,
                                equalTo(Duration.ofMillis(6)),
                                "TimingInformation totalTime",
                                "totalTime"),
                        FeatureMatchers.hasFeature(
                                TimingInformation::getTimingEventList,
                                contains(
                                        new CheckpointTiming(CHECKPOINT_2, Duration.ofMillis(1)),
                                        new CheckpointTiming(CHECKPOINT_1, Duration.ofMillis(3))),
                                "TimingInformation timingEventList",
                                "timingEventList"
                        )
                ))));
    }


    @Test
    public void whenRequestAreNestedLastYieldsResult() {
        testObj.startCollectionInCurrentThread();
        testObj.checkpointReached(CHECKPOINT_1);
        ticker.elapse(Duration.ofMillis(1));
        testObj.startCollectionInCurrentThread();
        testObj.checkpointReached(CHECKPOINT_2);
        ticker.elapse(Duration.ofMillis(2));
        final Optional<TimingInformation> nestedResult = testObj.finishCollectionInCurrentThread();
        testObj.checkpointReached(CHECKPOINT_3);
        ticker.elapse(Duration.ofMillis(3));
        final Optional<TimingInformation> finalResult = testObj.finishCollectionInCurrentThread();


        assertThat(nestedResult,
                is(OptionalMatchers.none()));

        assertThat(finalResult,
                is(some(Matchers.allOf(
                        FeatureMatchers.hasFeature(
                                TimingInformation::getTotalTime,
                                equalTo(Duration.ofMillis(6)),
                                "TimingInformation totalTime",
                                "totalTime"),
                        FeatureMatchers.hasFeature(
                                TimingInformation::getTimingEventList,
                                contains(
                                        new CheckpointTiming(CHECKPOINT_1, Duration.ZERO),
                                        new CheckpointTiming(CHECKPOINT_2, Duration.ofMillis(1)),
                                        new CheckpointTiming(CHECKPOINT_3, Duration.ofMillis(3))),
                                "TimingInformation timingEventList",
                                "timingEventList"
                        )
                ))));
    }

    @Test
    public void shouldNotMixCheckpointInEachThread() throws InterruptedException {
        int numThreads = 16;
        final List<String> perThreadCheckpointNames = IntStream.range(0, numThreads)
                .mapToObj(num -> "thread-" + num)
                .collect(Collectors.toList());
        final List<Callable<Optional<TimingInformation>>> executors = perThreadCheckpointNames.stream()
                .map(checkpointName -> (Callable<Optional<TimingInformation>>) () -> {
                            testObj.startCollectionInCurrentThread();

                            testObj.checkpointReached(checkpointName);

                            return testObj.finishCollectionInCurrentThread();
                        }
                )
                .collect(Collectors.toList());

        // when
        final ExecutorService fixedThreadPool = Executors.newFixedThreadPool(numThreads);
        try {
            final List<Optional<TimingInformation>> threadTimingResults = fixedThreadPool
                    .invokeAll(executors)
                    .stream()
                    .map(ThrowableFunctions.uncheckFunction(future -> future.get(500, TimeUnit.MILLISECONDS)))
                    .collect(Collectors.toList());

            // then
            assertThat(threadTimingResults, everyItem(some(any(TimingInformation.class))));

            final List<String> checkpointNames = threadTimingResults
                    .stream()
                    .map(Optional::get)
                    .flatMap(timingInformation -> timingInformation.getTimingEventList().stream())
                    .map(CheckpointTiming::getCheckpointName)
                    .collect(Collectors.toList());

            assertThat(
                    checkpointNames,
                    equalTo(perThreadCheckpointNames));
        } finally {
            fixedThreadPool.shutdown();
        }
    }

    @Test
    public void collectActivityInformation() {
        testObj.startCollectionInCurrentThread();
        testObj.addTimeSpentInActivity(ACTIVITY_1, Duration.ofMillis(1));
        testObj.addTimeSpentInActivity(ACTIVITY_1, Duration.ofMillis(2));
        testObj.addTimeSpentInActivity(ACTIVITY_2, Duration.ofMillis(4));

        final Optional<TimingInformation> result = testObj.finishCollectionInCurrentThread();

        assertThat(result,
                is(some(Matchers.allOf(
                        FeatureMatchers.hasFeature(
                                TimingInformation::getActivityDurations,
                                contains(
                                        new CheckpointTiming(ACTIVITY_1, Duration.ofMillis(3)),
                                        new CheckpointTiming(ACTIVITY_2, Duration.ofMillis(4))),
                                "TimingInformation activityDurations",
                                "activityDurations"
                        )
                ))));
    }
}