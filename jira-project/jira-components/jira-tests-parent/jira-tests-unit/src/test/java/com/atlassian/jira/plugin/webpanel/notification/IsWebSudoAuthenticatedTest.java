package com.atlassian.jira.plugin.webpanel.notification;

import com.atlassian.jira.junit.rules.InitMockitoMocks;
import com.atlassian.jira.mock.security.MockSimpleAuthenticationContext;
import com.atlassian.jira.security.websudo.InternalWebSudoManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.web.HttpServletVariables;
import com.atlassian.jira.web.MockHttpServletVariables;
import com.google.common.collect.ImmutableMap;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

public class IsWebSudoAuthenticatedTest {
    @Rule
    public InitMockitoMocks mockitoMocks = new InitMockitoMocks(this);

    @Mock
    private InternalWebSudoManager webSudoManager;
    private HttpServletVariables variables = new MockHttpServletVariables();
    private MockSimpleAuthenticationContext ctx = MockSimpleAuthenticationContext.createNoopContext(new MockApplicationUser("admin"));
    private IsWebSudoAuthenticated isWebSudoAuthenticated;

    @Before
    public void setUp() throws Exception {
        isWebSudoAuthenticated = new IsWebSudoAuthenticated(webSudoManager, ctx, variables);
    }

    @Test
    public void shouldNotDisplayWhenWebSudoDisabled() {
        //given
        when(webSudoManager.isEnabled()).thenReturn(false);
        when(webSudoManager.hasValidSession(Mockito.any())).thenReturn(true);

        //when
        boolean allowed = isWebSudoAuthenticated.shouldDisplay(ImmutableMap.of());

        //then
        assertThat(allowed, Matchers.equalTo(false));
    }

    @Test
    public void shouldNotDisplayWhenNoUser() {
        //given
        when(webSudoManager.isEnabled()).thenReturn(true);
        isWebSudoAuthenticated = new IsWebSudoAuthenticated(webSudoManager,
                MockSimpleAuthenticationContext.createNoopContext((ApplicationUser) null), variables);

        //when
        boolean allowed = isWebSudoAuthenticated.shouldDisplay(ImmutableMap.of());

        //then
        assertThat(allowed, Matchers.equalTo(false));

    }

    @Test
    public void shouldNotDisplayWhenNoValidWebSudo() {
        //given
        when(webSudoManager.isEnabled()).thenReturn(true);
        when(webSudoManager.hasValidSession(variables.getHttpSession())).thenReturn(false);

        //when
        boolean allowed = isWebSudoAuthenticated.shouldDisplay(ImmutableMap.of());

        //then
        assertThat(allowed, Matchers.equalTo(false));
    }

    @Test
    public void shouldDisplayWhenValidWebSudo() {
        //given
        when(webSudoManager.isEnabled()).thenReturn(true);
        when(webSudoManager.hasValidSession(variables.getHttpSession())).thenReturn(true);

        //when
        boolean allowed = isWebSudoAuthenticated.shouldDisplay(ImmutableMap.of());

        //then
        assertThat(allowed, Matchers.equalTo(true));
    }
}