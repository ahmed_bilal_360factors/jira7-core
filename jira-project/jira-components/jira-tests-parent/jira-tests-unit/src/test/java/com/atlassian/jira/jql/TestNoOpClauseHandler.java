package com.atlassian.jira.jql;

import com.atlassian.jira.JiraDataTypes;
import com.atlassian.jira.issue.search.ClauseNames;
import com.atlassian.jira.jql.context.ClauseContextImpl;
import com.atlassian.jira.jql.permission.ClausePermissionHandler;
import com.atlassian.jira.jql.query.QueryFactoryResult;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.MessageSet;
import com.atlassian.jira.util.MessageSetImpl;
import com.atlassian.jira.web.bean.MockI18nBean;
import com.atlassian.query.clause.TerminalClauseImpl;
import com.atlassian.query.operator.Operator;
import org.apache.lucene.search.BooleanQuery;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import static org.hamcrest.Matchers.empty;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestNoOpClauseHandler {
    private static final String FIELD_ID = "fieldId";
    @Rule
    public final MethodRule initMockito = MockitoJUnit.rule();
    @Mock
    private ClausePermissionHandler clausePermissionHandler;
    private ClauseNames names = new ClauseNames("primaryName", "secondaryName");
    private NoOpClauseHandler noOpClauseHandler;

    @Before
    public void setUp() throws Exception {
        this.noOpClauseHandler = new NoOpClauseHandler(clausePermissionHandler, FIELD_ID, names, "jira.jql.validation.no.such.field") {
            @Override
            I18nHelper getI18n(final ApplicationUser user) {
                return new MockI18nBean();
            }
        };
    }

    @Test
    public void returnsFalseQueries() throws Exception {
        //noinspection ConstantConditions
        assertThat(noOpClauseHandler.getFactory().getQuery(null, null), is(new QueryFactoryResult(new BooleanQuery())));
    }

    @Test
    public void returnsCorrectValidationMessage() throws Exception {
        final MessageSet messageSet = new MessageSetImpl();
        messageSet.addErrorMessage("Field 'dude' does not exist or you do not have permission to view it.");
        assertThat(noOpClauseHandler.getValidator().validate(null, new TerminalClauseImpl("dude", Operator.EQUALS, "sweet")), is(messageSet));
    }

    @Test
    public void returnsCorrectContext() throws Exception {
        assertThat(noOpClauseHandler.getClauseContextFactory().getClauseContext(null, new TerminalClauseImpl("dude", Operator.EQUALS, "sweet")),
                is(ClauseContextImpl.createGlobalClauseContext()));
    }

    @Test
    public void returnsCorrectPermission() throws Exception {
        when(clausePermissionHandler.hasPermissionToUseClause(null)).thenReturn(true);

        assertThat(noOpClauseHandler.getPermissionHandler().hasPermissionToUseClause(null), is(true));
    }

    @Test
    public void clauseInformationIsCorrect() throws Exception {
        assertThat(noOpClauseHandler.getInformation().getFieldId(), is(FIELD_ID));
        assertThat(noOpClauseHandler.getInformation().getIndexField(), is(FIELD_ID));
        assertThat(noOpClauseHandler.getInformation().getJqlClauseNames(), is(names));
        assertThat(noOpClauseHandler.getInformation().getSupportedOperators(), empty());
        assertThat(noOpClauseHandler.getInformation().getDataType(), is(JiraDataTypes.ALL));
    }

}
