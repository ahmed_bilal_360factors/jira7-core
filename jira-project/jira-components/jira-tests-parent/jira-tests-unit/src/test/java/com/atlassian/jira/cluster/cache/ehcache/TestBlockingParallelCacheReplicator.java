package com.atlassian.jira.cluster.cache.ehcache;

import com.google.common.collect.ImmutableList;
import net.sf.ehcache.Ehcache;
import net.sf.ehcache.Element;
import net.sf.ehcache.distribution.CachePeer;
import org.hamcrest.CoreMatchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.mockito.stubbing.Answer;

import java.rmi.RemoteException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

import static java.util.concurrent.TimeUnit.SECONDS;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestBlockingParallelCacheReplicator {

    private static final int NUMBER_OF_PEERS = 3;

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @Mock
    private Ehcache cache;

    @Mock
    private net.sf.ehcache.CacheManager cacheManager;

    @Mock
    private net.sf.ehcache.distribution.CacheManagerPeerProvider cacheManagerPeerProvider;

    @Mock
    private CachePeer peer1;

    @Mock
    private CachePeer peer2;

    @Mock
    private CachePeer peer3;

    private BlockingParallelCacheReplicator sut;

    private final ExecutorService executorService = Executors.newFixedThreadPool(NUMBER_OF_PEERS);
    private final CountDownLatch latch = new CountDownLatch(NUMBER_OF_PEERS);
    private final AtomicInteger count = new AtomicInteger(0);
    private final AtomicInteger exceptionCount = new AtomicInteger(0);
    private final Element element = new Element("key", "value", 1);

    @Before
    public void setUp() throws Exception {
        sut = new BlockingParallelCacheReplicator(true, true, true, true, true, this.executorService);

        when(cache.getCacheManager()).thenReturn(cacheManager);
        when(cacheManager.getCacheManagerPeerProvider("RMI")).thenReturn(cacheManagerPeerProvider);
        when(cacheManagerPeerProvider.listRemoteCachePeers(cache)).thenReturn(ImmutableList.of(peer1, peer2, peer3));
    }

    @After
    public void tearDown() throws Exception {
        executorService.shutdown();
        executorService.awaitTermination(10, SECONDS);
    }


    @Test(timeout = 10_000)
    public void testConcurrentReplicationOfRemove() throws Exception {
        when(peer1.remove("key")).thenAnswer(waitForOthersAndYield());
        when(peer2.remove("key")).thenAnswer(waitForOthersAndYield());
        when(peer3.remove("key")).thenAnswer(waitForOthersAndYield());

        sut.notifyElementRemoved(cache, element);

        assertThat(NUMBER_OF_PEERS, CoreMatchers.equalTo(count.get()));
        verify(peer1).remove("key");
        verify(peer2).remove("key");
        verify(peer3).remove("key");
    }


    @Test(timeout = 10_000)
    public void testConcurrentReplicationOfRemoveAll() throws Exception {
        doAnswer(waitForOthersAndYield()).when(peer1).removeAll();
        doAnswer(waitForOthersAndYield()).when(peer2).removeAll();
        doAnswer(waitForOthersAndYield()).when(peer3).removeAll();

        sut.notifyRemoveAll(cache);

        assertThat(NUMBER_OF_PEERS, CoreMatchers.equalTo(count.get()));
        verify(peer1).removeAll();
        verify(peer2).removeAll();
        verify(peer3).removeAll();
    }

    @Test(timeout = 10_000)
    public void testConcurrentReplicationOfPut() throws Exception {
        doAnswer(waitForOthersAndYield()).when(peer1).put(element);
        doAnswer(waitForOthersAndYield()).when(peer2).put(element);
        doAnswer(waitForOthersAndYield()).when(peer3).put(element);

        sut.notifyElementPut(cache, element);

        assertThat(NUMBER_OF_PEERS, CoreMatchers.equalTo(count.get()));
        verify(peer1).put(element);
        verify(peer2).put(element);
        verify(peer3).put(element);
    }

    @Test
    public void replicationShouldContinueWhenFailsOnAnyNode() throws RemoteException {
        final RuntimeException ex = new RuntimeException("something-went-wrong");
        when(peer1.remove("key")).thenThrow(ex);
        when(peer2.remove("key")).thenReturn(true);
        when(peer3.remove("key")).thenReturn(true);

        final BlockingParallelCacheReplicator sutSpy = spy(sut);
        sutSpy.notifyElementRemoved(cache, element);

        verify(peer1).remove("key");
        verify(peer2).remove("key");
        verify(peer3).remove("key");

        verify(sutSpy).onReplicationError(eq(cache), anyObject(), eq(ex), eq("remove"));
    }

    private Answer<Void> waitForOthersAndYield() {
        return invocation -> {
            latch.countDown();
            latch.await();
            Thread.yield();
            count.incrementAndGet();
            return null;
        };
    }
}