package com.atlassian.jira.web.component.jql;

import com.atlassian.jira.JiraDataTypes;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.Field;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.fields.NavigableField;
import com.atlassian.jira.issue.search.ClauseNames;
import com.atlassian.jira.issue.search.constants.SimpleFieldSearchConstants;
import com.atlassian.jira.issue.search.managers.SearchHandlerManager;
import com.atlassian.jira.jql.ClauseHandler;
import com.atlassian.jira.jql.ClauseInformation;
import com.atlassian.jira.jql.ValueGeneratingClauseHandler;
import com.atlassian.jira.jql.context.ClauseContextFactory;
import com.atlassian.jira.jql.operand.FunctionOperandHandler;
import com.atlassian.jira.jql.operand.registry.JqlFunctionHandlerRegistry;
import com.atlassian.jira.jql.operator.OperatorClasses;
import com.atlassian.jira.jql.permission.ClausePermissionHandler;
import com.atlassian.jira.jql.query.ClauseQueryFactory;
import com.atlassian.jira.jql.util.JqlStringSupport;
import com.atlassian.jira.jql.validator.ClauseValidator;
import com.atlassian.jira.jql.values.ClauseValuesGenerator;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.plugin.jql.function.JqlFunction;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.collect.CollectionBuilder;
import com.atlassian.jira.util.json.JSONArray;
import com.atlassian.jira.util.json.JSONObject;
import com.atlassian.query.operand.FunctionOperand;
import com.atlassian.query.operator.Operator;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.mockito.Mock;

import java.util.Collections;
import java.util.Locale;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestDefaultAutoCompleteJsonGenerator {
    @Rule
    public final TestRule initMocks = MockitoMocksInContainer.forTest(this);

    @Mock
    JqlFunction badJqlFunction;
    @Mock
    JqlFunction badJqlFunction2;
    @Mock
    I18nHelper i18nHelper;

    @Mock
    private SearchHandlerManager searchHandlerManager;
    @Mock
    private JqlStringSupport jqlStringSupport;
    @Mock
    private FieldManager fieldManager;
    @Mock
    private JqlFunctionHandlerRegistry jqlFunctionHandlerRegistry;
    private DefaultAutoCompleteJsonGenerator jsonGenerator;

    @Before
    public void setUp() throws Exception {

        jsonGenerator = new DefaultAutoCompleteJsonGenerator(searchHandlerManager, jqlStringSupport, fieldManager, jqlFunctionHandlerRegistry) {
            @Override
            String htmlEncode(final String string) {
                return string;
            }
        };

    }

    @Test
    public void testGetVisibleFieldNamesJsonHappyPath() throws Exception {
        final ClauseInformation information = mock(ClauseInformation.class);
        when(information.getJqlClauseNames()).thenReturn(new ClauseNames("field2"));
        when(information.getFieldId()).thenReturn("id2");
        when(information.getSupportedOperators()).thenReturn(OperatorClasses.EMPTY_ONLY_OPERATORS);
        when(information.getDataType()).thenReturn(JiraDataTypes.DATE);

        final ClauseHandler clauseHandler = mock(ClauseHandler.class);
        when(clauseHandler.getInformation()).thenReturn(information);

        when(searchHandlerManager.getVisibleClauseHandlers(null)).thenReturn(CollectionBuilder.newBuilder(new MockValuesGeneratingClauseHandler("field1"), clauseHandler, new MockValuesGeneratingClauseHandler("cf[12345]", "Custom Field Name")).asList());

        when(jqlStringSupport.encodeFieldName("Custom Field Name")).thenReturn("Custom Field Name");
        when(jqlStringSupport.encodeFieldName("field1")).thenReturn("field1");
        when(jqlStringSupport.encodeFieldName("field2")).thenReturn("field2");

        final NavigableField navigableField = mock(NavigableField.class);
        final Field field = mock(Field.class);
        final CustomField customField = mock(CustomField.class);
        when(customField.getUntranslatedName()).thenReturn("Custom Field Name");

        when(fieldManager.getField("field1")).thenReturn(navigableField);
        when(fieldManager.getField("id2")).thenReturn(field);
        when(fieldManager.getField("cf[12345]")).thenReturn(customField);

        final String json = jsonGenerator.getVisibleFieldNamesJson(null, Locale.ENGLISH);

        JSONArray expectedOperators = new JSONArray();
        for (Operator oper : OperatorClasses.EMPTY_ONLY_OPERATORS) {
            expectedOperators.put(oper.getDisplayString());
        }
        JSONArray expectedJsonArr = new JSONArray();

        JSONArray allTypes = new JSONArray();
        allTypes.put("java.lang.Object");
        JSONArray dateTypes = new JSONArray();
        dateTypes.put("java.util.Date");

        JSONObject expectedJsonObj3 = new JSONObject();
        expectedJsonObj3.put("value", "Custom Field Name");
        expectedJsonObj3.put("displayName", "Custom Field Name - cf[12345]");
        expectedJsonObj3.put("auto", "true");
        expectedJsonObj3.put("orderable", "true");
        expectedJsonObj3.put("searchable", "true");
        expectedJsonObj3.put("cfid", "cf[12345]");
        expectedJsonObj3.put("operators", new JSONArray());
        expectedJsonObj3.put("types", allTypes);
        expectedJsonArr.put(expectedJsonObj3);

        JSONObject expectedJsonObj1 = new JSONObject();
        expectedJsonObj1.put("value", "field1");
        expectedJsonObj1.put("displayName", "field1");
        expectedJsonObj1.put("auto", "true");
        expectedJsonObj1.put("orderable", "true");
        expectedJsonObj1.put("searchable", "true");
        expectedJsonObj1.put("operators", new JSONArray());
        expectedJsonObj1.put("types", allTypes);
        expectedJsonArr.put(expectedJsonObj1);

        JSONObject expectedJsonObj2 = new JSONObject();
        expectedJsonObj2.put("value", "field2");
        expectedJsonObj2.put("displayName", "field2");
        expectedJsonObj2.put("searchable", "true");
        expectedJsonObj2.put("operators", expectedOperators);
        expectedJsonObj2.put("types", dateTypes);
        expectedJsonArr.put(expectedJsonObj2);

        assertEquals(expectedJsonArr.toString(), json);
    }

    @Test
    public void testGetVisibleFieldNamesJsonCustomFieldWithOnlyId() throws Exception {
        final ClauseInformation information = mock(ClauseInformation.class);
        when(information.getJqlClauseNames()).thenReturn(new ClauseNames("field2"));
        when(information.getFieldId()).thenReturn("id2");
        when(information.getSupportedOperators()).thenReturn(OperatorClasses.EMPTY_ONLY_OPERATORS);
        when(information.getDataType()).thenReturn(JiraDataTypes.DATE);

        final ClauseHandler clauseHandler = mock(ClauseHandler.class);
        when(clauseHandler.getInformation()).thenReturn(information);

        when(searchHandlerManager.getVisibleClauseHandlers(null)).thenReturn(CollectionBuilder.newBuilder(new MockValuesGeneratingClauseHandler("field1"), clauseHandler, new MockValuesGeneratingClauseHandler("cf[12345]")).asList());

        when(jqlStringSupport.encodeFieldName("cf[12345]")).thenReturn("cf[12345]");
        when(jqlStringSupport.encodeFieldName("field1")).thenReturn("field1");
        when(jqlStringSupport.encodeFieldName("field2")).thenReturn("field2");


        final NavigableField navigableField = mock(NavigableField.class);
        final Field field = mock(Field.class);
        final CustomField customField = mock(CustomField.class);
        when(customField.getUntranslatedName()).thenReturn("Custom Field Name");

        when(fieldManager.getField("field1")).thenReturn(navigableField);
        when(fieldManager.getField("id2")).thenReturn(field);
        when(fieldManager.getField("cf[12345]")).thenReturn(customField);

        final String json = jsonGenerator.getVisibleFieldNamesJson(null, Locale.ENGLISH);

        JSONArray expectedOperators = new JSONArray();
        for (Operator oper : OperatorClasses.EMPTY_ONLY_OPERATORS) {
            expectedOperators.put(oper.getDisplayString());
        }

        JSONArray allTypes = new JSONArray();
        allTypes.put("java.lang.Object");
        JSONArray dateTypes = new JSONArray();
        dateTypes.put("java.util.Date");

        JSONArray expectedJsonArr = new JSONArray();

        JSONObject expectedJsonObj3 = new JSONObject();
        expectedJsonObj3.put("value", "cf[12345]");
        expectedJsonObj3.put("displayName", "Custom Field Name - cf[12345]");
        expectedJsonObj3.put("auto", "true");
        expectedJsonObj3.put("orderable", "true");
        expectedJsonObj3.put("searchable", "true");
        expectedJsonObj3.put("operators", new JSONArray());
        expectedJsonObj3.put("types", allTypes);
        expectedJsonArr.put(expectedJsonObj3);

        JSONObject expectedJsonObj1 = new JSONObject();
        expectedJsonObj1.put("value", "field1");
        expectedJsonObj1.put("displayName", "field1");
        expectedJsonObj1.put("auto", "true");
        expectedJsonObj1.put("orderable", "true");
        expectedJsonObj1.put("searchable", "true");
        expectedJsonObj1.put("operators", new JSONArray());
        expectedJsonObj1.put("types", allTypes);
        expectedJsonArr.put(expectedJsonObj1);

        JSONObject expectedJsonObj2 = new JSONObject();
        expectedJsonObj2.put("value", "field2");
        expectedJsonObj2.put("displayName", "field2");
        expectedJsonObj2.put("searchable", "true");
        expectedJsonObj2.put("operators", expectedOperators);
        expectedJsonObj2.put("types", dateTypes);
        expectedJsonArr.put(expectedJsonObj2);

        assertEquals(expectedJsonArr.toString(), json);
    }

    @Test
    public void testGetVisibleFieldNamesJsonCustomFieldConflictsWithOtherField() throws Exception {
        final ClauseInformation information = mock(ClauseInformation.class);
        when(information.getJqlClauseNames()).thenReturn(new ClauseNames("field2"));
        when(information.getFieldId()).thenReturn("id2");
        when(information.getSupportedOperators()).thenReturn(OperatorClasses.EMPTY_ONLY_OPERATORS);
        when(information.getDataType()).thenReturn(JiraDataTypes.DATE);

        final ClauseHandler clauseHandler = mock(ClauseHandler.class);
        when(clauseHandler.getInformation()).thenReturn(information);

        when(searchHandlerManager.getVisibleClauseHandlers(null)).thenReturn(CollectionBuilder.newBuilder(new MockValuesGeneratingClauseHandler("field1"), clauseHandler, new MockValuesGeneratingClauseHandler("cf[12345]", "field1")).asList());

        when(jqlStringSupport.encodeFieldName("field1")).thenReturn("field1");
        when(jqlStringSupport.encodeFieldName("cf[12345]")).thenReturn("cf[12345]");
        when(jqlStringSupport.encodeFieldName("field2")).thenReturn("field2");

        final NavigableField navigableField = mock(NavigableField.class);
        final Field field = mock(Field.class);
        final CustomField customField = mock(CustomField.class);
        when(customField.getUntranslatedName()).thenReturn("field1");

        when(fieldManager.getField("field1")).thenReturn(navigableField);
        when(fieldManager.getField("id2")).thenReturn(field);
        when(fieldManager.getField("cf[12345]")).thenReturn(customField);

        final String json = jsonGenerator.getVisibleFieldNamesJson(null, Locale.ENGLISH);

        JSONArray expectedOperators = new JSONArray();
        for (Operator oper : OperatorClasses.EMPTY_ONLY_OPERATORS) {
            expectedOperators.put(oper.getDisplayString());
        }
        JSONArray allTypes = new JSONArray();
        allTypes.put("java.lang.Object");
        JSONArray dateTypes = new JSONArray();
        dateTypes.put("java.util.Date");

        JSONArray expectedJsonArr = new JSONArray();

        JSONObject expectedJsonObj1 = new JSONObject();
        expectedJsonObj1.put("value", "field1");
        expectedJsonObj1.put("displayName", "field1");
        expectedJsonObj1.put("auto", "true");
        expectedJsonObj1.put("orderable", "true");
        expectedJsonObj1.put("searchable", "true");
        expectedJsonObj1.put("operators", new JSONArray());
        expectedJsonObj1.put("types", allTypes);
        expectedJsonArr.put(expectedJsonObj1);

        JSONObject expectedJsonObj3 = new JSONObject();
        expectedJsonObj3.put("value", "cf[12345]");
        expectedJsonObj3.put("displayName", "field1 - cf[12345]");
        expectedJsonObj3.put("auto", "true");
        expectedJsonObj3.put("orderable", "true");
        expectedJsonObj3.put("searchable", "true");
        expectedJsonObj3.put("operators", new JSONArray());
        expectedJsonObj3.put("types", allTypes);
        expectedJsonArr.put(expectedJsonObj3);

        JSONObject expectedJsonObj2 = new JSONObject();
        expectedJsonObj2.put("value", "field2");
        expectedJsonObj2.put("displayName", "field2");
        expectedJsonObj2.put("searchable", "true");
        expectedJsonObj2.put("operators", expectedOperators);
        expectedJsonObj2.put("types", dateTypes);
        expectedJsonArr.put(expectedJsonObj2);

        assertEquals(expectedJsonArr.toString(), json);
    }

    @Test
    public void testGetVisibleFieldNamesJsonCustomFieldConflictsWithOtherCustomField() throws Exception {
        final ClauseInformation information = mock(ClauseInformation.class);
        when(information.getJqlClauseNames()).thenReturn(new ClauseNames("field2"));
        when(information.getFieldId()).thenReturn("id2");
        when(information.getSupportedOperators()).thenReturn(OperatorClasses.EMPTY_ONLY_OPERATORS);
        when(information.getDataType()).thenReturn(JiraDataTypes.DATE);

        final ClauseHandler clauseHandler = mock(ClauseHandler.class);
        when(clauseHandler.getInformation()).thenReturn(information);

        when(searchHandlerManager.getVisibleClauseHandlers(null)).thenReturn(CollectionBuilder.newBuilder(new MockValuesGeneratingClauseHandler("field1"), clauseHandler, new MockValuesGeneratingClauseHandler("cf[12345]", "Cust Field Conflicts"), new MockValuesGeneratingClauseHandler("cf[54321]", "Cust Field Conflicts")).asList());

        when(jqlStringSupport.encodeFieldName("cf[12345]")).thenReturn("cf[12345]");
        when(jqlStringSupport.encodeFieldName("cf[54321]")).thenReturn("cf[54321]");
        when(jqlStringSupport.encodeFieldName("field1")).thenReturn("field1");
        when(jqlStringSupport.encodeFieldName("field2")).thenReturn("field2");

        final NavigableField navigableField = mock(NavigableField.class);
        final Field field = mock(Field.class);
        final CustomField customField = mock(CustomField.class);
        when(customField.getUntranslatedName()).thenReturn("Cust Field Conflicts");

        when(fieldManager.getField("field1")).thenReturn(navigableField);
        when(fieldManager.getField("id2")).thenReturn(field);
        when(fieldManager.getField("cf[12345]")).thenReturn(customField);
        when(fieldManager.getField("cf[54321]")).thenReturn(customField);


        final String json = jsonGenerator.getVisibleFieldNamesJson(null, Locale.ENGLISH);

        JSONArray allTypes = new JSONArray();
        allTypes.put("java.lang.Object");
        JSONArray dateTypes = new JSONArray();
        dateTypes.put("java.util.Date");

        JSONArray expectedOperators = new JSONArray();
        for (Operator oper : OperatorClasses.EMPTY_ONLY_OPERATORS) {
            expectedOperators.put(oper.getDisplayString());
        }
        JSONArray expectedJsonArr = new JSONArray();

        JSONObject expectedJsonObj3 = new JSONObject();
        expectedJsonObj3.put("value", "cf[12345]");
        expectedJsonObj3.put("displayName", "Cust Field Conflicts - cf[12345]");
        expectedJsonObj3.put("auto", "true");
        expectedJsonObj3.put("orderable", "true");
        expectedJsonObj3.put("searchable", "true");
        expectedJsonObj3.put("operators", new JSONArray());
        expectedJsonObj3.put("types", allTypes);
        expectedJsonArr.put(expectedJsonObj3);

        JSONObject expectedJsonObj4 = new JSONObject();
        expectedJsonObj4.put("value", "cf[54321]");
        expectedJsonObj4.put("displayName", "Cust Field Conflicts - cf[54321]");
        expectedJsonObj4.put("auto", "true");
        expectedJsonObj4.put("orderable", "true");
        expectedJsonObj4.put("searchable", "true");
        expectedJsonObj4.put("operators", new JSONArray());
        expectedJsonObj4.put("types", allTypes);
        expectedJsonArr.put(expectedJsonObj4);

        JSONObject expectedJsonObj1 = new JSONObject();
        expectedJsonObj1.put("value", "field1");
        expectedJsonObj1.put("displayName", "field1");
        expectedJsonObj1.put("auto", "true");
        expectedJsonObj1.put("orderable", "true");
        expectedJsonObj1.put("searchable", "true");
        expectedJsonObj1.put("operators", new JSONArray());
        expectedJsonObj1.put("types", allTypes);
        expectedJsonArr.put(expectedJsonObj1);

        JSONObject expectedJsonObj2 = new JSONObject();
        expectedJsonObj2.put("value", "field2");
        expectedJsonObj2.put("displayName", "field2");
        expectedJsonObj2.put("searchable", "true");
        expectedJsonObj2.put("operators", expectedOperators);
        expectedJsonObj2.put("types", dateTypes);
        expectedJsonArr.put(expectedJsonObj2);

        assertEquals(expectedJsonArr.toString(), json);
    }

    @Test
    public void testGetVisibleFunctionNamesJson() throws Exception {
        // Make sure we test the exclusions
        when(jqlFunctionHandlerRegistry.getAllFunctionNames()).thenReturn(CollectionBuilder.newBuilder("afunc", "cfunc", "bfunc", "currentUser").asMutableList());

        final JqlFunction jqlFunction = mock(JqlFunction.class);
        when(jqlFunction.getMinimumNumberOfExpectedArguments()).thenReturn(0).thenReturn(1).thenReturn(2);
        when(jqlFunction.getDataType()).thenReturn(JiraDataTypes.ALL).thenReturn(JiraDataTypes.DATE).thenReturn(JiraDataTypes.ISSUE);


        final FunctionOperandHandler functionOperandHandler = mock(FunctionOperandHandler.class);
        when(functionOperandHandler.getJqlFunction()).thenReturn(jqlFunction);
        when(functionOperandHandler.isList()).thenReturn(true).thenReturn(false).thenReturn(false);

        when(jqlFunctionHandlerRegistry.getOperandHandler(new FunctionOperand("afunc"))).thenReturn(functionOperandHandler);
        when(jqlFunctionHandlerRegistry.getOperandHandler(new FunctionOperand("bfunc"))).thenReturn(functionOperandHandler);
        when(jqlFunctionHandlerRegistry.getOperandHandler(new FunctionOperand("cfunc"))).thenReturn(functionOperandHandler);

        when(jqlStringSupport.encodeFunctionName("afunc")).thenReturn("afunc");
        when(jqlStringSupport.encodeFunctionName("bfunc")).thenReturn("bfunc");
        when(jqlStringSupport.encodeFunctionName("cfunc")).thenReturn("cfunc");

        JSONArray allTypes = new JSONArray();
        allTypes.put("java.lang.Object");
        JSONArray dateTypes = new JSONArray();
        dateTypes.put("java.util.Date");
        JSONArray issueTypes = new JSONArray();
        issueTypes.put("com.atlassian.jira.issue.Issue");

        final String json = jsonGenerator.getVisibleFunctionNamesJson(null, Locale.ENGLISH);

        JSONArray expectedJsonArr = new JSONArray();
        JSONObject expectedJsonObj1 = new JSONObject();
        expectedJsonObj1.put("value", "afunc()");
        expectedJsonObj1.put("displayName", "afunc()");
        expectedJsonObj1.put("isList", "true");
        expectedJsonObj1.put("types", allTypes);
        expectedJsonArr.put(expectedJsonObj1);

        JSONObject expectedJsonObj2 = new JSONObject();
        expectedJsonObj2.put("value", "bfunc(\"\")");
        expectedJsonObj2.put("displayName", "bfunc(\"\")");
        expectedJsonObj2.put("types", dateTypes);
        expectedJsonArr.put(expectedJsonObj2);

        JSONObject expectedJsonObj3 = new JSONObject();
        expectedJsonObj3.put("value", "cfunc(\"\", \"\")");
        expectedJsonObj3.put("displayName", "cfunc(\"\", \"\")");
        expectedJsonObj3.put("types", issueTypes);
        expectedJsonArr.put(expectedJsonObj3);

        assertEquals(expectedJsonArr.toString(), json);
    }

    @Test
    public void testGetVisibleFunctionNamesJsonSurvivesPluginThrowingExceptions() throws Exception {
        when(jqlFunctionHandlerRegistry.getAllFunctionNames()).thenReturn(CollectionBuilder.newBuilder("afunc", "badfunc", "cfunc", "badfunc2", "bfunc").asMutableList());

        when(badJqlFunction.getMinimumNumberOfExpectedArguments()).thenThrow(new RuntimeException("Misbehaving plugin exception"));
        when(badJqlFunction2.isList()).thenThrow(new RuntimeException("Misbehaving plugin exception"));

        final JqlFunction goodJqlFunction = mock(JqlFunction.class);
        when(goodJqlFunction.getMinimumNumberOfExpectedArguments()).thenReturn(0).thenReturn(1).thenReturn(2);
        when(goodJqlFunction.getDataType()).thenReturn(JiraDataTypes.ALL).thenReturn(JiraDataTypes.DATE).thenReturn(JiraDataTypes.ISSUE);

        final FunctionOperandHandler goodFunctionOperandHandler = mock(FunctionOperandHandler.class);
        when(goodFunctionOperandHandler.getJqlFunction()).thenReturn(goodJqlFunction);
        when(goodFunctionOperandHandler.isList()).thenReturn(true).thenReturn(false).thenReturn(false);

        final FunctionOperandHandler badFunctionOperandHandler = new FunctionOperandHandler(badJqlFunction, i18nHelper);
        final FunctionOperandHandler badFunctionOperandHandler2 = new FunctionOperandHandler(badJqlFunction2, i18nHelper);

        when(jqlFunctionHandlerRegistry.getOperandHandler(new FunctionOperand("afunc"))).thenReturn(goodFunctionOperandHandler);
        when(jqlFunctionHandlerRegistry.getOperandHandler(new FunctionOperand("badfunc"))).thenReturn(badFunctionOperandHandler);
        when(jqlFunctionHandlerRegistry.getOperandHandler(new FunctionOperand("bfunc"))).thenReturn(goodFunctionOperandHandler);
        when(jqlFunctionHandlerRegistry.getOperandHandler(new FunctionOperand("badfunc2"))).thenReturn(badFunctionOperandHandler2);
        when(jqlFunctionHandlerRegistry.getOperandHandler(new FunctionOperand("cfunc"))).thenReturn(goodFunctionOperandHandler);

        when(jqlStringSupport.encodeFunctionName("afunc")).thenReturn("afunc");
        when(jqlStringSupport.encodeFunctionName("bfunc")).thenReturn("bfunc");
        when(jqlStringSupport.encodeFunctionName("badfunc2")).thenReturn("badfunc2");
        when(jqlStringSupport.encodeFunctionName("cfunc")).thenReturn("cfunc");

        JSONArray allTypes = new JSONArray();
        allTypes.put("java.lang.Object");
        JSONArray dateTypes = new JSONArray();
        dateTypes.put("java.util.Date");
        JSONArray issueTypes = new JSONArray();
        issueTypes.put("com.atlassian.jira.issue.Issue");

        final String json = jsonGenerator.getVisibleFunctionNamesJson(null, Locale.ENGLISH);

        JSONArray expectedJsonArr = new JSONArray();
        JSONObject expectedJsonObj1 = new JSONObject();
        expectedJsonObj1.put("value", "afunc()");
        expectedJsonObj1.put("displayName", "afunc()");
        expectedJsonObj1.put("isList", "true");
        expectedJsonObj1.put("types", allTypes);
        expectedJsonArr.put(expectedJsonObj1);

        JSONObject expectedJsonObj2 = new JSONObject();
        expectedJsonObj2.put("value", "badfunc2()");
        expectedJsonObj2.put("displayName", "badfunc2()");
        expectedJsonObj2.put("types", Collections.emptyList());
        expectedJsonArr.put(expectedJsonObj2);

        JSONObject expectedJsonObj3 = new JSONObject();
        expectedJsonObj3.put("value", "bfunc(\"\")");
        expectedJsonObj3.put("displayName", "bfunc(\"\")");
        expectedJsonObj3.put("types", dateTypes);
        expectedJsonArr.put(expectedJsonObj3);

        JSONObject expectedJsonObj4 = new JSONObject();
        expectedJsonObj4.put("value", "cfunc(\"\", \"\")");
        expectedJsonObj4.put("displayName", "cfunc(\"\", \"\")");
        expectedJsonObj4.put("types", issueTypes);
        expectedJsonArr.put(expectedJsonObj4);

        assertEquals(expectedJsonArr.toString(), json);
    }

    private class MockValuesGeneratingClauseHandler implements ClauseHandler, ValueGeneratingClauseHandler {
        private ClauseInformation clauseInformation;

        private MockValuesGeneratingClauseHandler(final String... fieldId) {
            final ClauseNames names = new ClauseNames(fieldId[0], fieldId);
            this.clauseInformation = new SimpleFieldSearchConstants(fieldId[0], names, fieldId[0], fieldId[0], fieldId[0], Collections.<Operator>emptySet(), JiraDataTypes.ALL);
        }

        public ClauseInformation getInformation() {
            return this.clauseInformation;
        }

        public ClauseQueryFactory getFactory() {
            return null;
        }

        public ClauseValidator getValidator() {
            return null;
        }

        public ClausePermissionHandler getPermissionHandler() {
            return null;
        }

        public ClauseContextFactory getClauseContextFactory() {
            return null;
        }

        public ClauseValuesGenerator getClauseValuesGenerator() {
            return null;
        }
    }
}
