package com.atlassian.jira.plugin.userformat.descriptors;

import com.atlassian.jira.plugin.userformat.UserFormatModuleDescriptor;
import com.atlassian.jira.plugin.util.ModuleDescriptors;
import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.PluginAccessor;
import com.google.common.collect.Ordering;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.List;

import static com.google.common.collect.ImmutableList.of;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.emptyIterable;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Holds unit tests for {@link DefaultUserFormatModuleDescriptors}
 *
 * @since v4.4
 */
public class TestDefaultUserFormatModuleDescriptors {
    @Rule
    public MockitoRule mocks = MockitoJUnit.rule();

    @Mock
    private PluginAccessor mockPluginAccesor;

    @Mock
    private ModuleDescriptors.Orderings mockModuleDescriptorOrderings;

    @Test
    public void forTypeShouldReturnAnEmptyIterableIfThereAreNoUserFormatModuleDescriptorsForTheSpecifiedType() {
        final UserFormatModuleDescriptor moduleDescriptor1 = mock(UserFormatModuleDescriptor.class);
        when(moduleDescriptor1.getType()).thenReturn("test-user-format-type");

        final UserFormatModuleDescriptor moduleDescriptor2 = mock(UserFormatModuleDescriptor.class);
        when(moduleDescriptor2.getType()).thenReturn("test-user-format-type");

        when(mockPluginAccesor.getEnabledModuleDescriptorsByClass(UserFormatModuleDescriptor.class)).thenReturn(of(moduleDescriptor1, moduleDescriptor2));

        final DefaultUserFormatModuleDescriptors userFormatModuleDescriptors =
                new DefaultUserFormatModuleDescriptors(mockPluginAccesor, mockModuleDescriptorOrderings);

        final Iterable<UserFormatModuleDescriptor> descriptorsForTypeResult =
                userFormatModuleDescriptors.forType("non-existing-user-format-type");

        assertThat(descriptorsForTypeResult, emptyIterable());
    }

    @Test
    public void forTypeShouldReturnAllTheUserFormatModuleDescriptorsThatCanHandleTheSpecifiedTypeWhenThereIsMoreThanOneOfThem() {
        final UserFormatModuleDescriptor moduleDescriptor1 = mock(UserFormatModuleDescriptor.class);
        when(moduleDescriptor1.getType()).thenReturn("test-user-format-type");

        final UserFormatModuleDescriptor moduleDescriptor2 = mock(UserFormatModuleDescriptor.class);
        when(moduleDescriptor2.getType()).thenReturn("test-user-format-type");

        final UserFormatModuleDescriptor moduleDescriptor3 = mock(UserFormatModuleDescriptor.class);
        when(moduleDescriptor3.getType()).thenReturn("another-test-user-format-type");

        when(mockPluginAccesor.getEnabledModuleDescriptorsByClass(UserFormatModuleDescriptor.class)).thenReturn(of(moduleDescriptor1, moduleDescriptor2, moduleDescriptor3));

        final DefaultUserFormatModuleDescriptors userFormatModuleDescriptors =
                new DefaultUserFormatModuleDescriptors(mockPluginAccesor, mockModuleDescriptorOrderings);

        final Iterable<UserFormatModuleDescriptor> descriptorsForTypeResult =
                userFormatModuleDescriptors.forType("test-user-format-type");

        assertThat(descriptorsForTypeResult, containsInAnyOrder(moduleDescriptor1, moduleDescriptor2));
    }

    @Test
    public void forTypeShouldReturnOnlyOneUserFormatModuleDescriptorsForTheSpecifiedTypeWhenThereIsOnlyOneDescriptorThatCanHandleThatType() {
        final UserFormatModuleDescriptor moduleDescriptor1 = mock(UserFormatModuleDescriptor.class);
        when(moduleDescriptor1.getType()).thenReturn("test-user-format-type");

        final UserFormatModuleDescriptor moduleDescriptor2 = mock(UserFormatModuleDescriptor.class);
        when(moduleDescriptor2.getType()).thenReturn("test-user-format-type");

        final UserFormatModuleDescriptor moduleDescriptor3 = mock(UserFormatModuleDescriptor.class);
        when(moduleDescriptor3.getType()).thenReturn("another-test-user-format-type");

        when(mockPluginAccesor.getEnabledModuleDescriptorsByClass(UserFormatModuleDescriptor.class)).thenReturn(of(moduleDescriptor1, moduleDescriptor2, moduleDescriptor3));

        final DefaultUserFormatModuleDescriptors userFormatModuleDescriptors =
                new DefaultUserFormatModuleDescriptors(mockPluginAccesor, mockModuleDescriptorOrderings);

        final Iterable<UserFormatModuleDescriptor> descriptorsForTypeResult =
                userFormatModuleDescriptors.forType("another-test-user-format-type");

        assertThat(descriptorsForTypeResult, containsInAnyOrder(moduleDescriptor3));
    }

    @Test
    public void defaultForATypeShouldBeASystemModuleDescriptorWhenThereIsASystemDescriptorAndANonSystemDescriptorThatCanHandleThatType() {
        final UserFormatModuleDescriptor systemModuleDescriptor = mock(UserFormatModuleDescriptor.class);
        when(systemModuleDescriptor.getType()).thenReturn("test-user-format-type");

        final UserFormatModuleDescriptor userInstalledModuleDescriptor = mock(UserFormatModuleDescriptor.class);
        when(userInstalledModuleDescriptor.getType()).thenReturn("test-user-format-type");

        final UserFormatModuleDescriptor anotherModuleDescriptor = mock(UserFormatModuleDescriptor.class);
        when(anotherModuleDescriptor.getType()).thenReturn("another-test-user-format-type");

        when(mockPluginAccesor.getEnabledModuleDescriptorsByClass(UserFormatModuleDescriptor.class)).thenReturn(of(systemModuleDescriptor, userInstalledModuleDescriptor, anotherModuleDescriptor));

        when(mockModuleDescriptorOrderings.byOrigin()).thenReturn(buildStubOrderingFromListPosition
                (
                        of(systemModuleDescriptor, userInstalledModuleDescriptor)
                ));

        // Natural Order should not matter in this case.
        when(mockModuleDescriptorOrderings.natural()).thenReturn(buildStubAlwaysEqualOrdering());

        final DefaultUserFormatModuleDescriptors userFormatModuleDescriptors =
                new DefaultUserFormatModuleDescriptors(mockPluginAccesor, mockModuleDescriptorOrderings);

        final UserFormatModuleDescriptor defaultDescriptorsForType =
                userFormatModuleDescriptors.defaultFor("test-user-format-type");

        assertThat(defaultDescriptorsForType, equalTo(systemModuleDescriptor));
    }

    @Test
    public void defaultForATypeShouldBeTheModuleDescriptorThatIsLowerAccordingToNaturalOrderingWhenTheDescriptorsThatCanHandleThatTypeHaveTheSameOrigin() {
        final UserFormatModuleDescriptor abcModuleDescriptor = mock(UserFormatModuleDescriptor.class);
        when(abcModuleDescriptor.getType()).thenReturn("test-user-format-type");

        final UserFormatModuleDescriptor xyzInstalledModuleDescriptor = mock(UserFormatModuleDescriptor.class);
        when(xyzInstalledModuleDescriptor.getType()).thenReturn("test-user-format-type");

        final UserFormatModuleDescriptor anotherModuleDescriptor = mock(UserFormatModuleDescriptor.class);
        when(anotherModuleDescriptor.getType()).thenReturn("another-test-user-format-type");

        when(mockPluginAccesor.getEnabledModuleDescriptorsByClass(UserFormatModuleDescriptor.class)).thenReturn(of(abcModuleDescriptor, xyzInstalledModuleDescriptor, anotherModuleDescriptor));

        // They all come from the same origin
        when(mockModuleDescriptorOrderings.byOrigin()).thenReturn(buildStubAlwaysEqualOrdering());

        when(mockModuleDescriptorOrderings.natural()).thenReturn(buildStubOrderingFromListPosition
                (
                        of(abcModuleDescriptor, xyzInstalledModuleDescriptor)
                ));

        final DefaultUserFormatModuleDescriptors userFormatModuleDescriptors =
                new DefaultUserFormatModuleDescriptors(mockPluginAccesor, mockModuleDescriptorOrderings);

        final UserFormatModuleDescriptor defaultDescriptorsForType =
                userFormatModuleDescriptors.defaultFor("test-user-format-type");

        assertThat(defaultDescriptorsForType, equalTo(abcModuleDescriptor));
    }

    private Ordering<ModuleDescriptor> buildStubOrderingFromListPosition(final List<ModuleDescriptor> moduleDescriptorList) {
        return new Ordering<ModuleDescriptor>() {
            @Override
            public int compare(ModuleDescriptor o1, ModuleDescriptor o2) {
                if (moduleDescriptorList.indexOf(o1) < moduleDescriptorList.indexOf(o2)) {
                    return -1;
                } else if (moduleDescriptorList.indexOf(o1) > moduleDescriptorList.indexOf(o2)) {
                    return 1;
                } else {
                    return 0;
                }
            }
        };
    }

    /**
     * A dummy ordering that considers all objects to be equal.
     *
     * @return Always returns zero.
     */
    private Ordering<ModuleDescriptor> buildStubAlwaysEqualOrdering() {
        return new Ordering<ModuleDescriptor>() {
            @Override
            public int compare(ModuleDescriptor o1, ModuleDescriptor o2) {
                return 0;
            }
        };
    }
}
