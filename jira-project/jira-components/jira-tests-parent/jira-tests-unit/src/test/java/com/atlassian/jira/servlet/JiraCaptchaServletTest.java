package com.atlassian.jira.servlet;

import com.atlassian.jira.junit.rules.MockitoContainer;
import com.atlassian.jira.mock.servlet.MockHttpServletRequest;
import com.atlassian.jira.mock.servlet.MockHttpServletResponse;
import com.atlassian.jira.mock.servlet.MockServletOutputStream;
import com.octo.captcha.service.CaptchaServiceException;
import com.octo.captcha.service.image.ImageCaptchaService;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.StringWriter;

import static java.util.Locale.CANADA;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


public class JiraCaptchaServletTest {
    private static final String SESSION_ID = "MySession";
    private static final byte[] PASS = {'P', 'a', 's', 's'};

    @Rule
    public MockitoContainer mockitoContainer = new MockitoContainer(this);
    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Mock
    HttpSession session;
    @Mock
    BufferedImage image;
    @Mock
    ImageCaptchaService imageCaptchaService;

    MockHttpServletRequest request;
    JiraCaptchaServlet fixture;

    @Before
    public void setUp() {
        request = new MockHttpServletRequest(session);

        request.setLocale(CANADA);
        when(session.getId()).thenReturn(SESSION_ID);
        when(imageCaptchaService.getImageChallengeForID(SESSION_ID, CANADA)).thenReturn(image);

        fixture = new JiraCaptchaServlet() {
            @Override
            void writeJpegImage(ByteArrayOutputStream os, BufferedImage bufferedImage) throws IOException {
                assertThat(bufferedImage, sameInstance(image));
                os.write(PASS);
            }
        };
    }

    private JiraCaptchaService initCaptcha() {
        final JiraCaptchaService captcha = mock(JiraCaptchaService.class);
        when(captcha.getImageCaptchaService()).thenReturn(imageCaptchaService);
        mockitoContainer.getMockComponentContainer().addMock(JiraCaptchaService.class, captcha);
        return captcha;
    }

    @Test
    public void getThrowsExceptionIfCaptchaIsUnavailable() {
        thrown.expect(IllegalStateException.class);
        fixture.doGet(request, new MockHttpServletResponse());
    }

    @Test
    public void getWritesImageIfNothingElseGoesWrong() {
        initCaptcha();
        final StringWriter out = new StringWriter();
        final MockHttpServletResponse response = new MockHttpServletResponse(new MockServletOutputStream(out));

        fixture.doGet(request, response);

        assertThat(out.toString(), is("Pass"));
    }

    @Test
    public void respondsWith404ForIllegalArgumentException() {
        initCaptcha();
        when(imageCaptchaService.getImageChallengeForID(SESSION_ID, CANADA)).thenThrow(new IllegalArgumentException());
        final MockHttpServletResponse response = new MockHttpServletResponse();

        fixture.doGet(request, response);

        assertThat(response.getStatus(), is(HttpServletResponse.SC_NOT_FOUND));
    }

    @Test
    public void respondsWith500ForCaptchaServiceException() {
        initCaptcha();
        when(imageCaptchaService.getImageChallengeForID(SESSION_ID, CANADA)).thenThrow(new CaptchaServiceException("ouch!"));
        final MockHttpServletResponse response = new MockHttpServletResponse();

        fixture.doGet(request, response);

        assertThat(response.getStatus(), is(HttpServletResponse.SC_INTERNAL_SERVER_ERROR));
    }
}