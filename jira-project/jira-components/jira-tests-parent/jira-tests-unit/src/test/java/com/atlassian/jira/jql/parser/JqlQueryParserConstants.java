package com.atlassian.jira.jql.parser;

public class JqlQueryParserConstants {
    //Set of characters that are reserved but not currently used within JQL.
    public static final String ILLEGAL_CHARS_STRING = "{}*/%+$#@?;";
}
