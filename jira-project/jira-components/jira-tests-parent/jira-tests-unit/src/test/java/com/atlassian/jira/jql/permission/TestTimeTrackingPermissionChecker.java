package com.atlassian.jira.jql.permission;

import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.IssueFieldConstants;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestTimeTrackingPermissionChecker {
    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @Mock
    private ApplicationProperties applicationProperties;
    @Mock
    private FieldClausePermissionChecker.Factory fieldClausePermissionCheckerFactory;
    @Mock
    private FieldClausePermissionChecker fieldClausePermissionChecker;

    @Before
    public void setUp() throws Exception {
        when(fieldClausePermissionCheckerFactory.createPermissionChecker(IssueFieldConstants.TIMETRACKING))
                .thenReturn(fieldClausePermissionChecker);
    }

    @Test
    public void testHasPermissionToUseClauseTimeTrackingDisabled() throws Exception {
        when(applicationProperties.getOption(APKeys.JIRA_OPTION_TIMETRACKING)).thenReturn(false);
        when(fieldClausePermissionChecker.hasPermissionToUseClause(null)).thenReturn(true);

        final TimeTrackingPermissionChecker checker = new TimeTrackingPermissionChecker(fieldClausePermissionCheckerFactory, applicationProperties);
        assertFalse(checker.hasPermissionToUseClause(null));
    }

    @Test
    public void testHasPermissionToUseClauseTimeTrackingHidden() throws Exception {
        when(applicationProperties.getOption(APKeys.JIRA_OPTION_TIMETRACKING)).thenReturn(true);
        when(fieldClausePermissionChecker.hasPermissionToUseClause(null)).thenReturn(false);

        final TimeTrackingPermissionChecker checker = new TimeTrackingPermissionChecker(fieldClausePermissionCheckerFactory, applicationProperties);
        assertFalse(checker.hasPermissionToUseClause(null));
    }

    @Test
    public void testHasPermissionToUseClauseHappyPath() throws Exception {
        when(applicationProperties.getOption(APKeys.JIRA_OPTION_TIMETRACKING)).thenReturn(true);
        when(fieldClausePermissionChecker.hasPermissionToUseClause(null)).thenReturn(true);

        final TimeTrackingPermissionChecker checker = new TimeTrackingPermissionChecker(fieldClausePermissionCheckerFactory, applicationProperties);
        assertTrue(checker.hasPermissionToUseClause(null));
    }
}
