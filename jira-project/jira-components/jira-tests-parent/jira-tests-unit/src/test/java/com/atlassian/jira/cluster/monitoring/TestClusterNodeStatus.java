package com.atlassian.jira.cluster.monitoring;

import com.atlassian.jira.cluster.zdu.ClusterUpgradeStateManager;
import com.atlassian.jira.cluster.zdu.UpgradeState;
import com.atlassian.jira.util.BuildUtilsInfo;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestClusterNodeStatus {
    @Mock
    private BuildUtilsInfo buildUtilsInfo;

    @Mock
    private ClusterUpgradeStateManager clusterUpgradeStateManager;

    private ClusterNodeStatus clusterNodeStatus;

    @Before
    public void setUp() {
        clusterNodeStatus = new ClusterNodeStatus(buildUtilsInfo, clusterUpgradeStateManager);
    }

    @Test
    public void getsRightVersionNumber() {
        when(buildUtilsInfo.getVersion()).thenReturn("7.3.0");

        assertThat(clusterNodeStatus.getNodeVersion(), is("7.3.0"));
    }

    @Test
    public void getsUpgradeStateAsString() {
        when(clusterUpgradeStateManager.getUpgradeState()).thenReturn(UpgradeState.STABLE);

        assertThat(clusterNodeStatus.getClusterUpgradeState(), is("STABLE"));
    }
}
