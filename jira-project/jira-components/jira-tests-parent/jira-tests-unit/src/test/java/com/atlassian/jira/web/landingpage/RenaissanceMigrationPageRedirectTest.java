package com.atlassian.jira.web.landingpage;

import com.atlassian.fugue.Option;
import com.atlassian.jira.bc.dataimport.ImportCompletedEvent;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.license.RenaissanceMigrationStatus;
import com.atlassian.jira.mock.MockApplicationProperties;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.user.MockApplicationUser;
import org.junit.Before;
import org.junit.Test;

import java.util.Optional;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class RenaissanceMigrationPageRedirectTest {
    private static final String MIGRATION_SUMMARY_URL = "/secure/MigrationSummary.jspa";

    private RenaissanceMigrationPageRedirect pageRedirect;
    private ApplicationProperties applicationProperties;
    private GlobalPermissionManager globalPermissionManager;
    private FeatureManager featureManager;
    private RenaissanceMigrationStatus renaissanceMigrationStatus;

    private LandingPageRedirectManager landingPageRedirectManager;

    @Before
    public void setup() {
        featureManager = mock(FeatureManager.class);
        globalPermissionManager = mock(GlobalPermissionManager.class);
        landingPageRedirectManager = mock(LandingPageRedirectManager.class);
        renaissanceMigrationStatus = mock(RenaissanceMigrationStatus.class);
        applicationProperties = new MockApplicationProperties();

        pageRedirect = new RenaissanceMigrationPageRedirect(applicationProperties, landingPageRedirectManager,
                globalPermissionManager, renaissanceMigrationStatus);
    }

    @Test
    public void redirectsToMigrationSummaryIfItHasntBeenShownAndUserHasAdminPermission() {
        MockApplicationUser user = new MockApplicationUser("user1");
        when(globalPermissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, user)).thenReturn(true);
        when(renaissanceMigrationStatus.hasMigrationRun()).thenReturn(true);
        applicationProperties.setOption(RenaissanceMigrationPageRedirect.PROPERTY_POST_MIGRATION_PAGE_DISPLAYED, false);

        assertThat(pageRedirect.url(user), is(Optional.of(MIGRATION_SUMMARY_URL)));
    }

    @Test
    public void noRedirectIfUserIsNotAdmin() {
        MockApplicationUser user = new MockApplicationUser("user1");
        when(globalPermissionManager.hasPermission(eq(GlobalPermissionKey.ADMINISTER), any())).thenReturn(true);
        when(globalPermissionManager.hasPermission(eq(GlobalPermissionKey.ADMINISTER), eq(user))).thenReturn(false);
        when(renaissanceMigrationStatus.hasMigrationRun()).thenReturn(true);

        applicationProperties.setOption(RenaissanceMigrationPageRedirect.PROPERTY_POST_MIGRATION_PAGE_DISPLAYED, false);

        assertThat(pageRedirect.url(user), is(Optional.empty()));
    }

    @Test
    public void noRedirectIfUserIsNull() {
        when(globalPermissionManager.hasPermission(eq(GlobalPermissionKey.ADMINISTER), any())).thenReturn(true);
        when(renaissanceMigrationStatus.hasMigrationRun()).thenReturn(true);

        applicationProperties.setOption(RenaissanceMigrationPageRedirect.PROPERTY_POST_MIGRATION_PAGE_DISPLAYED, false);

        assertThat(pageRedirect.url(null), is(Optional.empty()));
    }


    @Test
    public void noRedirectIfMigrationSummaryHasBeenDisplayed() {
        applicationProperties.setOption(RenaissanceMigrationPageRedirect.PROPERTY_POST_MIGRATION_PAGE_DISPLAYED, true);
        when(globalPermissionManager.hasPermission(eq(GlobalPermissionKey.ADMINISTER), any())).thenReturn(true);
        when(renaissanceMigrationStatus.hasMigrationRun()).thenReturn(true);

        assertThat(pageRedirect.url(new MockApplicationUser("user1")), is(Optional.empty()));
    }

    @Test
    public void noRedirectIfMigrationHasntRun() {
        MockApplicationUser user = new MockApplicationUser("user1");
        when(globalPermissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, user)).thenReturn(true);
        when(renaissanceMigrationStatus.hasMigrationRun()).thenReturn(false);
        applicationProperties.setOption(RenaissanceMigrationPageRedirect.PROPERTY_POST_MIGRATION_PAGE_DISPLAYED, false);

        assertThat(pageRedirect.url(new MockApplicationUser("user1")), is(Optional.empty()));
    }

    @Test
    public void registersRedirectOnStartIfItHasntBeenDisplayed() {
        applicationProperties.setOption(RenaissanceMigrationPageRedirect.PROPERTY_POST_MIGRATION_PAGE_DISPLAYED, false);
        when(renaissanceMigrationStatus.hasMigrationRun()).thenReturn(true);

        pageRedirect.start();
        verify(landingPageRedirectManager).registerRedirect(pageRedirect, 1);
    }

    @Test
    public void registersRedirectOnImportCompleted() throws Exception {
        applicationProperties.setOption(RenaissanceMigrationPageRedirect.PROPERTY_POST_MIGRATION_PAGE_DISPLAYED, false);
        when(renaissanceMigrationStatus.hasMigrationRun()).thenReturn(true);

        pageRedirect.onImportCompletedEvent(new ImportCompletedEvent(true, Option.none()));
        verify(landingPageRedirectManager).registerRedirect(pageRedirect, 1);
    }


    @Test
    public void doesNotRegisterRedirectOnStartIfSummaryHasBeenDisplayed() throws Exception {
        applicationProperties.setOption(RenaissanceMigrationPageRedirect.PROPERTY_POST_MIGRATION_PAGE_DISPLAYED, true);
        when(renaissanceMigrationStatus.hasMigrationRun()).thenReturn(true);

        pageRedirect.start();
        verify(landingPageRedirectManager, never()).registerRedirect(pageRedirect, 1);
    }

    @Test
    public void registersRedirectOnStartIfMigrationHasntRun() throws Exception {
        applicationProperties.setOption(RenaissanceMigrationPageRedirect.PROPERTY_POST_MIGRATION_PAGE_DISPLAYED, false);
        when(renaissanceMigrationStatus.hasMigrationRun()).thenReturn(false);

        pageRedirect.start();
        verify(landingPageRedirectManager).registerRedirect(pageRedirect, 1);
    }

}