package com.atlassian.jira.onboarding;

import com.atlassian.jira.bc.security.login.LoginInfo;
import com.atlassian.jira.bc.security.login.LoginService;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.config.properties.PropertiesManager;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserKeyStore;
import com.atlassian.jira.web.HttpServletVariables;
import com.opensymphony.module.propertyset.PropertySet;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestUserCheckerImpl {
    private UserChecker userChecker;

    @Mock
    private UserKeyStore userKeyStore;
    @Mock
    private LoginService loginService;
    @Mock
    private LoginInfo loginInfo;
    @Mock
    private FeatureManager featureManager;
    @Mock
    private GlobalPermissionManager permissionManager;
    @Mock
    private PropertiesManager propertiesManager;
    @Mock
    private HttpServletVariables httpServletVariables;
    @Mock
    private PropertySet propertySet;
    @Mock
    private ApplicationUser user;

    @Before
    public void init() {
        when(propertiesManager.getPropertySet()).thenReturn(propertySet);
        when(loginService.getLoginInfo(anyString())).thenReturn(loginInfo);
        when(userKeyStore.getIdForUserKey(anyString())).thenReturn(1000L);
        userChecker = new UserCheckerImpl(userKeyStore, loginService, featureManager, permissionManager, propertiesManager, httpServletVariables);
    }

    @Test
    public void shouldNotReturnTrueForServerSysAdmin() {
        assertThat("should return false when instance is Server", userChecker.isOnDemandSysAdmin(user), is(false));
    }

    @Test
    public void shouldReturnTrueAsFirstLoginWhenBelowIdThresholdAndLoginCountIsOne() {
        when(loginInfo.getLoginCount()).thenReturn(1L);
        assertThat("should return true when user id is below threshold AND loginCount is exactly one", userChecker.firstTimeLoggingIn(user), is(true));
    }

    @Test
    public void shouldReturnTrueAsFirstLoginWhenAboveIdThresholdAndLoginCountIsOne() {
        when(propertySet.exists(anyString())).thenReturn(true);
        when(propertySet.getString(anyString())).thenReturn("500");
        when(loginInfo.getLoginCount()).thenReturn(1L);
        assertThat("should return true when user id is above threshold AND loginCount is exactly one", userChecker.firstTimeLoggingIn(user), is(true));
    }

    @Test
    public void shouldReturnFalseAsFirstLoginWhenBelowIdThresholdAndLoginCountIsNull() {
        when(loginInfo.getLoginCount()).thenReturn(null);
        assertThat("should return false when user id is below threshold AND loginCount is null", userChecker.firstTimeLoggingIn(user), is(false));
    }

    @Test
    public void shouldReturnTrueAsFirstLoginWhenAboveIdThresholdAndLoginCountIsNull() {
        when(propertySet.exists(anyString())).thenReturn(true);
        when(propertySet.getString(anyString())).thenReturn("500");
        when(loginInfo.getLoginCount()).thenReturn(null);
        assertThat("should return true when user id is above threshold AND loginCount is null", userChecker.firstTimeLoggingIn(user), is(true));
    }

    @Test
    public void shouldReturnFalseAsFirstLoginWhenBelowIdThresholdAndLoginCountIsMoreThanOne() {
        when(loginInfo.getLoginCount()).thenReturn(42L);
        assertThat("should return false when user id is below threshold AND loginCount is more than one", userChecker.firstTimeLoggingIn(user), is(false));
    }

    @Test
    public void shouldReturnFalseAsFirstLoginWhenAboveIdThresholdAndLoginCountIsMoreThanOne() {
        when(propertySet.exists(anyString())).thenReturn(true);
        when(propertySet.getString(anyString())).thenReturn("500");
        when(loginInfo.getLoginCount()).thenReturn(42L);
        assertThat("should return false when user id is above threshold AND loginCount is more than one", userChecker.firstTimeLoggingIn(user), is(false));
    }

    @Test
    public void shouldReturnImpersonationFalseWhenCookieIsNotPresent() {
        // returns an empty Cookie array
        final HttpServletRequest mockHttpRequest = mock(HttpServletRequest.class);
        when(mockHttpRequest.getCookies()).thenReturn(new Cookie[]{});
        when(httpServletVariables.getHttpRequest()).thenReturn(mockHttpRequest);
        when(user.getUsername()).thenReturn("anything");

        assertThat("if impersonation cookie is not present, impersonation should be false", userChecker.isImpersonationActive(user), is(false));
    }

    @Test
    public void shouldReturnTrueWhenImpersonationCookieIsPresentAndItsValueMatchesUserCurrentlyLoggedIn() {
        Cookie impersonationCookie = new Cookie("um.user.impersonated.username", "username-currently-logged-in");
        final HttpServletRequest mockHttpRequest = mock(HttpServletRequest.class);
        when(mockHttpRequest.getCookies()).thenReturn(new Cookie[]{impersonationCookie});
        when(httpServletVariables.getHttpRequest()).thenReturn(mockHttpRequest);
        when(user.getUsername()).thenReturn("username-currently-logged-in");

        assertThat("if impersonation cookie is present AND current user matches the cookie value, impersonation should be true", userChecker.isImpersonationActive(user), is(true));
    }

    @Test
    public void shouldReturnTrueWhenImpersonationCookieIsPresentAndItsValueMatchesUserCurrentlyLoggedInWithSpaceInUsername() {
        Cookie impersonationCookie = new Cookie("um.user.impersonated.username", "username%20currently-logged-in");
        final HttpServletRequest mockHttpRequest = mock(HttpServletRequest.class);
        when(mockHttpRequest.getCookies()).thenReturn(new Cookie[]{impersonationCookie});
        when(httpServletVariables.getHttpRequest()).thenReturn(mockHttpRequest);
        when(user.getUsername()).thenReturn("username currently-logged-in");

        assertThat("if impersonation cookie is present AND current user with space in username matches the cookie value, impersonation should be true", userChecker.isImpersonationActive(user), is(true));
    }

    @Test
    public void shouldReturnFalseWhenImpersonationCookieIsPresentAndButItsValueDoesNotMatchUserCurrentlyLoggedIn() {
        Cookie impersonationCookie = new Cookie("um.user.impersonated.username", "username-currently-logged-in");
        final HttpServletRequest mockHttpRequest = mock(HttpServletRequest.class);
        when(mockHttpRequest.getCookies()).thenReturn(new Cookie[]{impersonationCookie});
        when(httpServletVariables.getHttpRequest()).thenReturn(mockHttpRequest);
        when(user.getUsername()).thenReturn("another-username-logged-in");

        assertThat("if impersonation cookie is present AND current user does not match the cookie value, impersonation should be false", userChecker.isImpersonationActive(user), is(false));
    }
}
