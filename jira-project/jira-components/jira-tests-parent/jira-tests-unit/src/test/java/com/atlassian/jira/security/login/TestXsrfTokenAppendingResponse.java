package com.atlassian.jira.security.login;

import com.atlassian.jira.security.xsrf.XsrfTokenGenerator;
import com.atlassian.jira.util.UrlBuilder;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.containsString;
import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @since v4.1.1
 */
public class TestXsrfTokenAppendingResponse {
    private final static String URL = "url";

    @Rule
    public final MethodRule initMockito = MockitoJUnit.rule();

    @Mock
    private HttpServletRequest mockBaseRequest;
    @Mock
    private HttpServletResponse mockBaseResponse;
    private XsrfTokenAppendingResponse xsrfTokenAppendingResponse;


    @Before
    public void setUp() throws Exception {
        xsrfTokenAppendingResponse = new XsrfTokenAppendingResponse(mockBaseRequest, mockBaseResponse) {
            @Override
            UrlBuilder createUrlBuilder(final String url) {
                return new UrlBuilder(url, "UTF-8", false);
            }
        };
    }

    @Test
    public void delegatesToBaseIfTokenIsNull() throws Exception {
        when(mockBaseRequest.getParameter(XsrfTokenGenerator.TOKEN_WEB_PARAMETER_KEY)).thenReturn(null);

        xsrfTokenAppendingResponse.sendRedirect(URL);

        verify(mockBaseResponse).sendRedirect(URL);
    }

    @Test
    public void appendsTokenToUrlIfThereIsATokenInTheResponse() throws Exception {
        String tokenValue = "token-value";
        when(mockBaseRequest.getParameter(XsrfTokenGenerator.TOKEN_WEB_PARAMETER_KEY)).thenReturn(tokenValue);

        xsrfTokenAppendingResponse.sendRedirect(URL);

        verify(mockBaseResponse).sendRedirect(argThat(allOf(containsString(URL), containsString(XsrfTokenGenerator.TOKEN_WEB_PARAMETER_KEY + "=" + tokenValue))));
    }
}
