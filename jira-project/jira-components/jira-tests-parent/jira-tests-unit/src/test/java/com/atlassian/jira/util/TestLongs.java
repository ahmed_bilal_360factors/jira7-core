package com.atlassian.jira.util;

import com.google.common.base.Optional;
import org.junit.Test;

import static java.lang.Long.valueOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TestLongs {
    @Test
    public void aLongInAStringCanBeReturnedAsALongObject() {
        final String value = "123";

        final Optional<Long> result = Longs.toLong(value);

        assertTrue(result.isPresent());
        assertEquals(valueOf(123l), result.get());
    }

    @Test
    public void aStringThatDoesNotRepresentALongIsDeemedIncorrect() {
        final String value = "invalid";

        final Optional<Long> result = Longs.toLong(value);

        assertFalse(result.isPresent());
    }

    @Test
    public void aNullStringIsDeemedIncorrect() {
        final String value = null;

        final Optional<Long> result = Longs.toLong(value);

        assertFalse(result.isPresent());
    }
}
