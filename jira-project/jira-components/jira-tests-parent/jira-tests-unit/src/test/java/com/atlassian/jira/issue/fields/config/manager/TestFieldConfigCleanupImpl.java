package com.atlassian.jira.issue.fields.config.manager;

import com.atlassian.jira.issue.customfields.manager.GenericConfigManager;
import com.atlassian.jira.issue.customfields.manager.OptionsManager;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.issue.fields.option.OptionSetManager;
import com.atlassian.jira.util.ComponentLocator;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestFieldConfigCleanupImpl {
    @Rule
    public final MethodRule initMockito = MockitoJUnit.rule();

    @Mock
    private OptionSetManager optionSetManager;
    @Mock
    private GenericConfigManager genericConfigManager;
    @Mock
    private ComponentLocator componentLocator;
    @Mock
    private OptionsManager optionsManager;
    @Mock
    private FieldConfig fieldConfig;

    @Test
    public void testRemoveAdditionalData() throws Exception {
        // given:
        when(fieldConfig.getId()).thenReturn(123L);
        when(componentLocator.getComponentInstanceOfType(OptionsManager.class)).thenReturn(optionsManager);
        final FieldConfigCleanupImpl cleanup = new FieldConfigCleanupImpl(optionSetManager, genericConfigManager, componentLocator);

        // when:
        cleanup.removeAdditionalData(fieldConfig);

        // then:
        verify(optionSetManager).removeOptionSet(fieldConfig);
        verify(genericConfigManager).remove("DefaultValue", "123");
        verify(optionsManager).removeCustomFieldConfigOptions(fieldConfig);
    }
}
