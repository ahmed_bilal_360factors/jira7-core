package com.atlassian.jira.config.feature;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.config.CoreFeatures;
import com.atlassian.jira.event.ClearCacheEvent;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import org.hamcrest.core.IsNot;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Properties;

import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class DefaultInstanceFeatureManagerTest {
    private static final String CORE_ENABLED_FEATURE_KEY = "enabled-feature";
    private static final String CORE_DISABLED_FEATURE_KEY = "disabled-feature";

    @Mock
    private FeaturesLoader featuresLoader;

    @Mock
    private EventPublisher eventPublisher;

    private DefaultInstanceFeatureManager tested;

    @Before
    public void setup() {
        when(featuresLoader.loadCoreProperties()).thenReturn(new Properties());
        tested = new DefaultInstanceFeatureManager(featuresLoader, eventPublisher);
    }

    @Test
    public void pluginFeaturesShouldOverrideCore() throws Exception {
        final Properties pluginProperties = new Properties();
        pluginProperties.put(CORE_ENABLED_FEATURE_KEY, "true");
        pluginProperties.put(CORE_DISABLED_FEATURE_KEY, "true");
        when(featuresLoader.loadPluginsFeatureProperties()).
                thenReturn(ImmutableList.of(pluginProperties));
        final Properties coreProperties = new Properties();
        coreProperties.put(CORE_ENABLED_FEATURE_KEY, "true");
        coreProperties.put(CORE_DISABLED_FEATURE_KEY, "false");
        when(featuresLoader.loadCoreProperties()).thenReturn(coreProperties);

        final DefaultInstanceFeatureManager tested = new DefaultInstanceFeatureManager(featuresLoader, eventPublisher);
        tested.start();

        assertThat(tested.isInstanceFeatureEnabled(CORE_ENABLED_FEATURE_KEY), is(true));
        assertThat(tested.isInstanceFeatureEnabled(CORE_DISABLED_FEATURE_KEY), is(true));
    }

    @Test
    public void clearCacheEventsShouldResetTheFeatureManagerState() throws Exception {
        final String PLUGIN_FEAT = "my.plugin.feature";
        final Properties properties = new Properties();
        properties.setProperty(PLUGIN_FEAT, "true");

        when(featuresLoader.loadPluginsFeatureProperties())
                .thenReturn(ImmutableList.of(properties))
                .thenReturn(ImmutableList.of());
        tested.afterInstantiation();
        tested.start();

        assertThat(tested.getEnabledFeatureKeys(), hasItem(PLUGIN_FEAT));

        final ArgumentCaptor<FeaturesMapHolder> captor = ArgumentCaptor.forClass(FeaturesMapHolder.class);
        verify(eventPublisher).register(captor.capture());

        captor.getValue().onClearCache(ClearCacheEvent.INSTANCE);
        assertThat(tested.getEnabledFeatureKeys(), IsNot.not(hasItem(PLUGIN_FEAT)));
    }

    @Test
    public void shouldHandleExistingAndNotExistingFeatureKeys() {
        when(featuresLoader.loadCoreProperties()).thenReturn(fromMap(
                "key1", "true",
                "key2", "value2"
        ));

        assertTrue(tested.isInstanceFeatureEnabled("key1"));
        assertFalse("Value different than 'true' should be treated as false", tested.isInstanceFeatureEnabled("key2"));
        assertFalse("Not existing value should be treated as false", tested.isInstanceFeatureEnabled("key3"));
    }

    @Test
    public void shouldHandleNonStringsAsFalse() {
        when(featuresLoader.loadCoreProperties()).thenReturn(fromMap(
                "key1", new Object(),
                "key2", Boolean.TRUE  // yes, that as well
        ));
        assertFalse("Non strings should be treated as false", tested.isInstanceFeatureEnabled("key1"));
        assertFalse("Non strings should be treated as false", tested.isInstanceFeatureEnabled("key2"));
    }

    @Test
    public void shouldReturnFalseForOnDemand() {
        when(featuresLoader.loadCoreProperties()).thenReturn(new Properties());
        assertFalse(tested.isInstanceFeatureEnabled(CoreFeatures.ON_DEMAND.featureKey()));
        assertFalse(tested.isOnDemand());
    }

    private Properties fromMap(final Object k1, final Object v1, final Object k2, final Object v2) {
        final Properties props = new Properties();
        props.putAll(ImmutableMap.of(k1, v1, k2, v2));
        return props;
    }
}