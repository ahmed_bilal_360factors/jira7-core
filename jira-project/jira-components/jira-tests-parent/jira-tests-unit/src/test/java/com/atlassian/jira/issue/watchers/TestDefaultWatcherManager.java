package com.atlassian.jira.issue.watchers;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.association.UserAssociationStore;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.database.QueryDslAccessor;
import com.atlassian.jira.event.issue.IssueWatcherAddedEvent;
import com.atlassian.jira.event.issue.IssueWatcherDeletedEvent;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueFactory;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.index.IssueIndexManager;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.mock.issue.MockIssue;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.mockito.Mock;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static java.util.stream.Collectors.toList;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestDefaultWatcherManager {
    @Rule
    public RuleChain mockAllTheThings = MockitoMocksInContainer.forTest(this);

    @Mock
    @AvailableInContainer
    public UserAssociationStore userAssociationStore;

    @Mock
    @AvailableInContainer
    public ApplicationProperties applicationProperties;

    @Mock
    @AvailableInContainer
    public IssueIndexManager indexManager;

    @Mock
    @AvailableInContainer
    public UserManager userManager;

    @Mock
    @AvailableInContainer
    public IssueFactory issueFactory;

    @Mock
    @AvailableInContainer
    public EventPublisher eventPublisher;

    @Mock
    @AvailableInContainer
    public IssueManager issueManager;

    @Mock
    @AvailableInContainer
    private QueryDslAccessor queryDslAccessor;

    private DefaultWatcherManager watcherManager;

    private final MockIssue issue = new MockIssue(1, "DEMO-1");
    private final MockIssue issue2 = new MockIssue(2, "DEMO-2");
    private final ApplicationUser user = new MockApplicationUser("bob");

    @Before
    public void setUp() throws Exception {
        Map<Long, List<ApplicationUser>> watchers = new HashMap<>();

        when(issueManager.getIssueObject(1L)).thenReturn(issue);
        when(issueManager.getIssueObject(2L)).thenReturn(issue2);

        when(userManager.getUserByKeyEvenWhenUnknown(anyString())).thenReturn(new MockApplicationUser("bob"));

        doAnswer(invocation -> {
            final ApplicationUser user1 = (ApplicationUser) invocation.getArguments()[1];
            final Issue issue1 = (Issue) invocation.getArguments()[2];
            watchers.put(issue1.getId(), Lists.newArrayList(user1));
            return null;
        }).when(userAssociationStore).createAssociation(eq(DefaultWatcherManager.ASSOCIATION_TYPE), any(ApplicationUser.class), any(Issue.class));

        when(userAssociationStore.getUserkeysFromIssue(eq(DefaultWatcherManager.ASSOCIATION_TYPE), any(Long.class))).thenAnswer(invocation -> {
            final Long issueId = (Long) invocation.getArguments()[1];
            final List<ApplicationUser> users = watchers.get(issueId);
            return users != null ? users.stream().map(ApplicationUser::getKey).collect(toList()) : ImmutableList.<ApplicationUser>of();
        });

        doAnswer(invocation -> {
            final ApplicationUser user1 = (ApplicationUser) invocation.getArguments()[1];
            for (List<ApplicationUser> list : watchers.values()) {
                list.remove(user1);
            }
            return null;
        }).when(userAssociationStore).removeUserAssociationsFromUser(eq(DefaultWatcherManager.ASSOCIATION_TYPE), any(ApplicationUser.class), eq("Issue"));

        this.watcherManager = new DefaultWatcherManager(userAssociationStore, applicationProperties, indexManager,
                issueFactory, eventPublisher, issueManager, userManager, queryDslAccessor);
    }

    @Test
    public void testAddUserToWatchList() {
        watcherManager.startWatching(user, issue);

        assertThat(watcherManager.getWatchers(issue, Locale.ENGLISH), hasSize(1));
        assertThat(watcherManager.getWatchers(issue, Locale.ENGLISH), hasItems(user));
    }

    @Test
    public void testVerifyWatchUnsortedReturnsData() {
        watcherManager.startWatching(user, issue);
        assertThat(watcherManager.getWatchersUnsorted(issue), hasItems(user));

        watcherManager.removeAllWatchesForUser(user);
        assertThat(watcherManager.getWatchersUnsorted(issue), hasSize(0));
    }

    @Test
    public void testRemoveAllWatchesForUser() {
        watcherManager.startWatching(user, issue);
        watcherManager.startWatching(user, issue2);

        assertThat(watcherManager.getWatchers(issue, Locale.ENGLISH), hasItems(user));
        assertThat(watcherManager.getWatchers(issue2, Locale.ENGLISH), hasItems(user));

        verify(eventPublisher, times(2)).publish(any(IssueWatcherAddedEvent.class));

        watcherManager.removeAllWatchesForUser(user);
        assertThat(watcherManager.getWatchers(issue, Locale.ENGLISH), hasSize(0));
        assertThat(watcherManager.getWatchers(issue2, Locale.ENGLISH), hasSize(0));

        verify(eventPublisher, times(2)).publish(any(IssueWatcherDeletedEvent.class));
    }
}
