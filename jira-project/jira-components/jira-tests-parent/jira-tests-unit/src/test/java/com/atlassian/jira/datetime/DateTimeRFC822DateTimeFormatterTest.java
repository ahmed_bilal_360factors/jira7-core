package com.atlassian.jira.datetime;

import org.joda.time.DateTimeZone;
import org.junit.Before;
import org.junit.Test;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

public class DateTimeRFC822DateTimeFormatterTest {
    /**
     * Test date -> 2011-04-28T12:43:34.618+1000
     */
    private static final Date APR_28 = new Date(1303958614618L);

    private static final DateTimeZone JIRA_TZ = DateTimeZone.forID("Australia/Sydney");
    private static final Locale JIRA_LOCALE = Locale.ENGLISH;

    private DateTimeFormatterFactoryStub dateTimeFormatterFactory;

    @Test
    public void parseEnglishRegardlessOfLocale() throws Exception {
        // Japan locale
        DateTimeFormatter formatter = dateTimeFormatterFactory.formatter()
                .withLocale(Locale.JAPAN)
                .withStyle(DateTimeStyle.RSS_RFC822_DATE_TIME);

        GregorianCalendar calendar = new GregorianCalendar(JIRA_TZ.toTimeZone());
        calendar.setTimeInMillis(0);
        calendar.set(2010, Calendar.OCTOBER, 17, 10, 0, 0);

        // Parses only English regardless of locale, when RFC 822 Style
        assertThat(formatter.parse("Sun, 17 Oct 2010 00:00:00 +0100"), is(calendar.getTime()));
    }

    @Test
    public void formattedInEnglishRegardlessOfLocale() throws Exception {
        DateTimeFormatter formatter = dateTimeFormatterFactory.formatter()
                .withLocale(Locale.JAPAN)
                .withStyle(DateTimeStyle.RSS_RFC822_DATE_TIME);

        // Formats always in English regardless of locale, when RFC 822 Style
        assertThat(formatter.format(APR_28), equalTo("Thu, 28 Apr 2011 12:43:34 +1000"));
    }

    @Before
    public void setUpFactoryStub() throws Exception {
        dateTimeFormatterFactory = new DateTimeFormatterFactoryStub();
        dateTimeFormatterFactory.style(DateTimeStyle.DATE);
        dateTimeFormatterFactory.relativeDates(false);
        dateTimeFormatterFactory.jiraTimeZone(JIRA_TZ);
        dateTimeFormatterFactory.jiraLocale(JIRA_LOCALE);
    }

}