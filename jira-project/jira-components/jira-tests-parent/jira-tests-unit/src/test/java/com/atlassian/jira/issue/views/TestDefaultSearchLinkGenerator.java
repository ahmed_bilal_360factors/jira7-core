package com.atlassian.jira.issue.views;

import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.bc.project.component.MockProjectComponent;
import com.atlassian.jira.bc.project.component.ProjectComponent;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.mock.project.MockVersion;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.util.velocity.MockVelocityRequestContextFactory;
import com.atlassian.jira.util.velocity.VelocityRequestContextFactory;
import com.atlassian.query.Query;
import com.atlassian.query.clause.Clause;
import com.atlassian.query.operand.SingleValueOperand;
import com.atlassian.query.operator.Operator;
import com.atlassian.sal.api.features.DarkFeatureManager;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasProperty;
import static org.mockito.Matchers.argThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

public class TestDefaultSearchLinkGenerator {
    @Rule
    public MockitoContainer container = MockitoMocksInContainer.rule(this);

    @Mock
    private JiraAuthenticationContext authenticationContext;
    @AvailableInContainer
    @Mock
    private SearchService searchService;
    @Mock
    private ProjectManager projectManager;
    @AvailableInContainer
    @Mock
    private DarkFeatureManager darkFeatureManager;
    @Mock
    private Query query;

    private final ApplicationUser USER = new MockApplicationUser("admin");
    private final Project PROJECT = new MockProject(123L, "PKEY", "New Project");
    private final Version VERSION = new MockVersion(111L, "New Version", PROJECT);
    private final ProjectComponent COMPONENT = new MockProjectComponent(222L, "New Component", PROJECT.getId());
    private final String baseUrl = "http://jira.com/base";
    private final VelocityRequestContextFactory velocityRequestContextFactory = new MockVelocityRequestContextFactory(baseUrl);

    private DefaultSearchLinkGenerator defaultSearchLinkGenerator;

    @Before
    public void setUp() throws Exception {
        when(projectManager.getProjectObj(PROJECT.getId())).thenReturn(PROJECT);
        when(authenticationContext.getUser()).thenReturn(USER);

        container.getMockWorker().getMockApplicationProperties().setEncoding("UTF-8");

        defaultSearchLinkGenerator = new DefaultSearchLinkGenerator(projectManager, authenticationContext, velocityRequestContextFactory);
    }

    @Test
    public void getComponentSearchLinkShouldReturnEmptyStringWhenNullComponentIsPassed() throws Exception {
        assertThat(defaultSearchLinkGenerator.getComponentSearchLink(null), equalTo(""));
    }

    @Test
    public void getComponentSearchLinkShouldReturnProperSearchLinkForValidComponent() throws Exception {
        final String url = "/issues/?jql=project+%3D+PKEY+AND+component+%3D+%22New Component%22";
        when(searchService.getIssueSearchPath(eq(USER), argThat(queryWithSimpleClauses(
                queryClause("project", PROJECT.getKey()),
                queryClause("component", COMPONENT.getName())
        )))).thenReturn(url);

        assertThat(
                defaultSearchLinkGenerator.getComponentSearchLink(COMPONENT),
                containsString(baseUrl + url)
        );
    }

    @Test
    public void getFixVersionSearchLinkShouldReturnEmptyStringWhenNullVersionIsPassed() throws Exception {
        assertThat(defaultSearchLinkGenerator.getFixVersionSearchLink(null), equalTo(""));
    }

    @Test
    public void getFixVersionSearchLinkShouldReturnProperSearchLinkForValidVersion() throws Exception {
        String url = "/issues/?jql=project+%3D+PKEY+AND+fixVersion+%3D+%22New Version%22";
        when(searchService.getIssueSearchPath(eq(USER), argThat(queryWithSimpleClauses(
                queryClause("project", PROJECT.getKey()),
                queryClause("fixVersion", VERSION.getName())
        )))).thenReturn(url);

        assertThat(
                defaultSearchLinkGenerator.getFixVersionSearchLink(VERSION),
                containsString(baseUrl + url)
        );
    }

    @SuppressWarnings("unchecked")
    private Matcher<Clause> queryClause(final String name, final String value) {
        return Matchers.<Clause>allOf(
                hasProperty("name", equalTo(name)),
                hasProperty("operator", equalTo(Operator.EQUALS)),
                hasProperty("operand", equalTo(new SingleValueOperand(value)))
        );
    }

    private Matcher<SearchService.IssueSearchParameters> queryWithSimpleClauses(final Matcher<Clause>... matchers) {
        return new TypeSafeMatcher<SearchService.IssueSearchParameters>() {
            @Override
            public void describeTo(final Description description) {
                description.appendText("SearchService.IssueSearchParameters.query");
            }

            @Override
            protected boolean matchesSafely(final SearchService.IssueSearchParameters item) {
                return hasProperty("whereClause", hasProperty("clauses", containsInAnyOrder(matchers))).matches(item.query());
            }
        };
    }
}
