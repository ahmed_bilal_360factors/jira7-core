package com.atlassian.jira.project.util;

import com.atlassian.cache.memory.MemoryCacheManager;
import com.atlassian.jira.junit.rules.MockitoContainer;
import com.atlassian.jira.util.collect.MapBuilder;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;

import java.util.Collections;

import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

public class TestCachingProjectKeyStore {

    private static final Long TEST_PROJECT_ID = 100001L;
    private static final String TEST_PROJECT_KEY = "TEST";

    private CachingProjectKeyStore store;

    @Rule
    public MockitoContainer container = new MockitoContainer(this);

    @Mock
    private ProjectKeyStore delegate;


    @Before
    public void setUp() {
        store = new CachingProjectKeyStore(delegate, new MemoryCacheManager());
    }

    @Test
    public void addProjectKeyInvalidatesCache() {
        when(delegate.getAllProjectKeys()).thenReturn(Collections.emptyMap())
                .thenReturn(MapBuilder.build(TEST_PROJECT_KEY, TEST_PROJECT_ID));
        // Ensure misses are cached.
        assertThat(store.getProjectId(TEST_PROJECT_KEY), nullValue());
        assertThat(store.getProjectId(TEST_PROJECT_KEY), nullValue());

        store.addProjectKey(TEST_PROJECT_ID, TEST_PROJECT_KEY);

        assertEquals(TEST_PROJECT_ID, store.getProjectId(TEST_PROJECT_KEY));
    }

    @Test
    public void deleteProjectKeysInvalidatesCache() {
        when(delegate.getAllProjectKeys()).thenReturn(MapBuilder.build(TEST_PROJECT_KEY, TEST_PROJECT_ID))
                .thenReturn(Collections.emptyMap());
        assertEquals(TEST_PROJECT_ID, store.getProjectId(TEST_PROJECT_KEY));

        store.deleteProjectKeys(TEST_PROJECT_ID);

        assertThat(store.getProjectId(TEST_PROJECT_KEY), nullValue());
    }
}
