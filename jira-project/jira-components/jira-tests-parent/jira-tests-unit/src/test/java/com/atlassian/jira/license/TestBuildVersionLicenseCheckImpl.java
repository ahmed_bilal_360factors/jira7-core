package com.atlassian.jira.license;

import com.atlassian.application.api.Application;
import com.atlassian.application.api.ApplicationKey;
import com.atlassian.fugue.Option;
import com.atlassian.jira.datetime.DateTimeFormatter;
import com.atlassian.jira.util.BuildUtilsInfo;
import com.atlassian.jira.util.I18nHelper;
import com.google.common.collect.ImmutableList;
import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Date;
import java.util.List;
import java.util.Random;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertFalse;
import static org.mockito.Answers.RETURNS_MOCKS;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

/**
 * Tests {@link BuildVersionLicenseCheckImpl}.
 *
 * @since 7.0
 */
public class TestBuildVersionLicenseCheckImpl {
    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @Mock(answer = RETURNS_MOCKS)
    private DateTimeFormatter dateTimeFormatter;
    @Mock
    private Jira6xServiceDeskPluginLicenseSupplier serviceDeskLicense;
    @Mock
    private BuildUtilsInfo buildUtils;
    @Mock
    private I18nHelper i18n;
    @Mock
    private LicenseDetails lic1, lic2, lic3;
    @Mock
    private Application app1, app2, app3;
    @Mock
    private LicenseMaintenancePredicate predicate;
    private Random random = new Random();

    @Before
    public void setUp() {
        when(serviceDeskLicense.get()).thenReturn(Option.none());
        when(buildUtils.getCurrentBuildDate()).thenReturn(new Date());

        addApplicationToLicense(lic1, "app-key-one");
        addApplicationToLicense(lic2, "app-key-two");
        addApplicationToLicense(lic3, "app-key-three");

        when(predicate.test(Mockito.any())).thenReturn(true);
        when(predicate.negate()).thenReturn(t -> !predicate.test(t));
    }

    /**
     * Creates an application with given key and licensed by lic. The application will be considered to be installed.
     * The license will also be valid for CORE application.
     */
    private void addApplicationToLicense(LicenseDetails lic, String key) {
        Date arbitraryDate = new Date(random.nextInt());
        // only needed to avoid NPEs
        when(lic.getMaintenanceExpiryDate()).thenReturn(arbitraryDate);
        when(lic.getApplicationDescription()).thenReturn(key);
        final ApplicationKey appKey = ApplicationKey.valueOf(key);
        when(lic.getLicensedApplications()).thenReturn(new MockLicensedApplications(appKey));
    }

    @Test
    public void licenseCheckFailsWhenNoLicenseSet() {
        final BuildVersionLicenseCheck licenseCheck = checkFor(ImmutableList.of(), false);
        MatcherAssert.assertThat(licenseCheck.evaluate().isPass(), equalTo(false));
        MatcherAssert.assertThat(licenseCheck.evaluateWithoutGracePeriod().isPass(), equalTo(false));
        verifyNoMoreInteractions(lic1, lic2, lic3);
    }

    @Test
    public void buildVersionCheckPassesWhenAllLicensesWithinMaintenance() {
        //given
        when(predicate.test(Mockito.any())).thenReturn(true);
        final BuildVersionLicenseCheck licenseCheck = checkFor(ImmutableList.of(lic1, lic2, lic3));

        //when
        LicenseCheck.Result result = licenseCheck.evaluate();

        //then
        MatcherAssert.assertThat(result.isPass(), equalTo(true));
        MatcherAssert.assertThat(result.getFailedLicenses(), Matchers.empty());
    }

    @Test
    public void buildVersionCheckFailsUnlessAllLicensesWithinMaintenance() {
        //given
        when(predicate.test(lic3)).thenReturn(false);
        final BuildVersionLicenseCheck licenseCheck = checkFor(ImmutableList.of(lic1, lic2, lic3));

        //when
        LicenseCheck.Result result = licenseCheck.evaluate();

        //then
        assertFalse(result.isPass());
        MatcherAssert.assertThat(result.getFailedLicenses(), contains(lic3));
    }

    @Test
    public void buildVersionCheckFailsIfNoLicensesWithinMaintenance() {
        //given
        when(predicate.test(any())).thenReturn(false);
        final BuildVersionLicenseCheck licenseCheck = checkFor(ImmutableList.of(lic1, lic2, lic3));

        //when
        LicenseCheck.Result result = licenseCheck.evaluate();

        //then
        assertFalse(result.isPass());
        MatcherAssert.assertThat(result.getFailedLicenses(), containsInAnyOrder(lic1, lic2, lic3));
    }

    @Test
    public void subscriptionLicensePassesIfAllLicensesAreELAsEvenWhenOutOfMaintenance() {
        //given
        when(lic1.isEnterpriseLicenseAgreement()).thenReturn(true);
        when(lic2.isEnterpriseLicenseAgreement()).thenReturn(true);
        when(lic3.isEnterpriseLicenseAgreement()).thenReturn(true);
        final BuildVersionLicenseCheck licenseCheck = checkFor(ImmutableList.of(lic1, lic2, lic3));

        //when
        LicenseCheck.Result result = licenseCheck.evaluate();

        //then
        MatcherAssert.assertThat(result.isPass(), equalTo(true));
        MatcherAssert.assertThat(result.getFailedLicenses(), Matchers.<LicenseDetails>empty());
    }

    @Test
    public void subscriptionLicenseCheckRevertsToMaintenanceExpiryCheckIfNotAllAreELAs() {
        //given
        when(predicate.test(lic1)).thenReturn(false);
        when(predicate.test(lic2)).thenReturn(false);

        when(lic1.isEnterpriseLicenseAgreement()).thenReturn(true);
        when(lic2.isEnterpriseLicenseAgreement()).thenReturn(false);
        when(lic3.isEnterpriseLicenseAgreement()).thenReturn(false);

        final BuildVersionLicenseCheck licenseCheck = checkFor(ImmutableList.of(lic1, lic2, lic3));

        //when
        LicenseCheck.Result result = licenseCheck.evaluate();

        //then
        MatcherAssert.assertThat(result.isPass(), equalTo(false));
        MatcherAssert.assertThat(result.getFailedLicenses(), contains(lic1, lic2));
    }

    @Test
    public void buildVersionCheckPassesIfLicensesTooOldButUserHasAcceptedGracePeriod() {
        //given
        when(predicate.test(lic1)).thenReturn(false);
        when(predicate.test(lic2)).thenReturn(false);
        final BuildVersionLicenseCheck licenseCheck = checkFor(ImmutableList.of(lic1, lic2, lic3), true);

        //when
        LicenseCheck.Result result = licenseCheck.evaluate();

        //then
        MatcherAssert.assertThat(result.isPass(), equalTo(true));
        MatcherAssert.assertThat(result.getFailedLicenses(), Matchers.empty());
    }

    @Test
    public void acceptingGracePeriodMakesCheckPassEvenWhenOutOfMaintenance() {
        when(predicate.test(lic1)).thenReturn(false);
        when(predicate.test(lic3)).thenReturn(false);
        BuildVersionLicenseCheck licenseCheck = checkFor(ImmutableList.of(lic1, lic2, lic3));

        LicenseCheck.Result result = licenseCheck.evaluate();
        MatcherAssert.assertThat(result.isPass(), equalTo(false));
        MatcherAssert.assertThat(result.getFailedLicenses(), containsInAnyOrder(lic1, lic3));

        licenseCheck = checkFor(ImmutableList.of(lic1, lic2, lic3), true);
        MatcherAssert.assertThat(licenseCheck.evaluate().isPass(), equalTo(true));

        // check again without grace period
        LicenseCheck.Result resultWithoutGrace = licenseCheck.evaluateWithoutGracePeriod();
        MatcherAssert.assertThat(resultWithoutGrace.isPass(), equalTo(false));
        MatcherAssert.assertThat(result.getFailedLicenses(), containsInAnyOrder(lic1, lic3));
    }

    @Test
    public void buildVersionCheckWithoutGracePeriodPassesWhenAllLicensesWithinMaintenance() {
        //given
        final BuildVersionLicenseCheck licenseCheck = checkFor(ImmutableList.of(lic1, lic2, lic3));

        //when
        LicenseCheck.Result result = licenseCheck.evaluateWithoutGracePeriod();

        //then
        MatcherAssert.assertThat(result.isPass(), equalTo(true));
        MatcherAssert.assertThat(result.getFailedLicenses(), Matchers.empty());
    }

    @Test
    public void buildVersionCheckFailsForAnyV1License() {
        //given
        when(lic3.getLicenseVersion()).thenReturn(1);

        final BuildVersionLicenseCheck licenseCheck = checkFor(ImmutableList.of(lic1, lic2, lic3));

        //when
        LicenseCheck.Result result = licenseCheck.evaluate();

        //then
        MatcherAssert.assertThat(result.isPass(), equalTo(false));
        MatcherAssert.assertThat(result.getFailedLicenses(), contains(lic3));
    }

    private BuildVersionLicenseCheckImpl checkFor(final List<LicenseDetails> details) {
        return checkFor(details, false);
    }

    private BuildVersionLicenseCheckImpl checkFor(final List<LicenseDetails> details,
                                                  boolean hasLicenseTooOldForBuildConfirmationBeenDoneDone) {
        JiraLicenseManager licenseManager = Mockito.mock(JiraLicenseManager.class);
        when(licenseManager.getLicenses()).thenReturn(details);
        when(licenseManager.hasLicenseTooOldForBuildConfirmationBeenDone())
                .thenReturn(hasLicenseTooOldForBuildConfirmationBeenDoneDone);

        return new BuildVersionLicenseCheckImpl(licenseManager, buildUtils, i18n, dateTimeFormatter, predicate);
    }

}
