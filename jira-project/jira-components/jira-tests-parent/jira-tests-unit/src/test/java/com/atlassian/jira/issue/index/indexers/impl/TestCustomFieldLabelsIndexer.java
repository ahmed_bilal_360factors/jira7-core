package com.atlassian.jira.issue.index.indexers.impl;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.label.Label;
import com.atlassian.jira.mock.issue.MockIssue;
import com.atlassian.jira.web.FieldVisibilityManager;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.junit.Test;

import static com.atlassian.jira.util.collect.CollectionBuilder.newBuilder;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestCustomFieldLabelsIndexer {
    @Test
    public void testIndex() {
        final CustomField mockCustomField = mock(CustomField.class);
        final FieldVisibilityManager mockFieldVisibilityManager = mock(FieldVisibilityManager.class);
        final Issue mockIssue = new MockIssue(10000L);

        when(mockCustomField.getValue(mockIssue)).thenReturn(newBuilder(new Label(null, null, "blah"), new Label(null, null, "HUGE")).asListOrderedSet());
        when(mockCustomField.getId()).thenReturn("customfield_10000");

        final CustomFieldLabelsIndexer labelsIndexer = new CustomFieldLabelsIndexer(mockFieldVisibilityManager, mockCustomField);

        final Document doc = new Document();
        labelsIndexer.addDocumentFieldsSearchable(doc, mockIssue);

        final Field[] fields = doc.getFields("customfield_10000");
        assertEquals("blah", fields[0].stringValue());
        assertEquals("HUGE", fields[1].stringValue());

        final Field[] fieldsFolded = doc.getFields("customfield_10000_folded");
        assertEquals("blah", fieldsFolded[0].stringValue());
        assertEquals("huge", fieldsFolded[1].stringValue());
    }

    @Test
    public void testIndexEmpty() {
        final CustomField mockCustomField = mock(CustomField.class);
        final FieldVisibilityManager mockFieldVisibilityManager = mock(FieldVisibilityManager.class);
        final Issue mockIssue = new MockIssue(10000L);

        when(mockCustomField.getValue(mockIssue)).thenReturn(null);
        when(mockCustomField.getId()).thenReturn("customfield_10000");

        final CustomFieldLabelsIndexer labelsIndexer = new CustomFieldLabelsIndexer(mockFieldVisibilityManager, mockCustomField);

        final Document doc = new Document();
        labelsIndexer.addDocumentFieldsSearchable(doc, mockIssue);

        assertEquals("<EMPTY>", doc.getField("customfield_10000_folded").stringValue());
        assertNull(doc.getField("customfield_10000"));
    }

    @Test
    public void testIndexNotSearchable() {
        final CustomField mockCustomField = mock(CustomField.class);
        final FieldVisibilityManager mockFieldVisibilityManager = mock(FieldVisibilityManager.class);
        final Issue mockIssue = new MockIssue(10000L);

        when(mockCustomField.getValue(mockIssue)).thenReturn(newBuilder(new Label(null, null, "blah"), new Label(null, null, "HUGE")).asListOrderedSet());
        when(mockCustomField.getId()).thenReturn("customfield_10000");

        final CustomFieldLabelsIndexer labelsIndexer = new CustomFieldLabelsIndexer(mockFieldVisibilityManager, mockCustomField);

        final Document doc = new Document();
        labelsIndexer.addDocumentFieldsNotSearchable(doc, mockIssue);

        final Field[] fields = doc.getFields("customfield_10000");
        assertEquals("blah", fields[0].stringValue());
        assertEquals("HUGE", fields[1].stringValue());

        assertNull(doc.getField("customfield_10000_folded"));
    }
}
