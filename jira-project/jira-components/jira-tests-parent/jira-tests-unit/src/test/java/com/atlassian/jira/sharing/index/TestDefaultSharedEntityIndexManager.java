package com.atlassian.jira.sharing.index;

import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.index.Index;
import com.atlassian.jira.index.MockResult;
import com.atlassian.jira.index.MultiThreadedIndexingConfiguration;
import com.atlassian.jira.issue.index.IndexException;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.issue.search.SearchRequestManager;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.mock.MockApplicationProperties;
import com.atlassian.jira.portal.PortalPage;
import com.atlassian.jira.portal.PortalPageManager;
import com.atlassian.jira.sharing.IndexableSharedEntity;
import com.atlassian.jira.sharing.SharedEntity;
import com.atlassian.jira.task.context.Contexts;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.util.FileFactory;
import com.atlassian.jira.util.collect.MockCloseableIterable;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.util.Collection;

import static com.atlassian.jira.config.properties.APKeys.JiraIndexConfiguration.SharedEntity.MIN_BATCH_SIZE;
import static com.atlassian.jira.issue.search.SearchRequest.ENTITY_TYPE;
import static com.google.common.collect.Lists.newArrayList;
import static java.util.Collections.singletonList;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * A test case for DefaultSharedEntityIndexManager
 *
 * @since v3.13
 */
public class TestDefaultSharedEntityIndexManager {
    @Rule
    public TestRule initInContainer = MockitoMocksInContainer.forTest(this);

    @Mock
    private SharedEntityIndexer indexer;
    @Mock
    private SearchRequestManager searchRequestManager;
    @Mock
    private PortalPageManager portalPageManager;
    @Mock
    private FileFactory fileFactory;
    @Mock
    private ApplicationProperties applicationProperties;

    @InjectMocks
    private DefaultSharedEntityIndexManager sharedEntityIndexManager;

    @Before
    public void setUp() throws Exception {
        final ApplicationUser owner = new MockApplicationUser("ownername");
        final PortalPage portalPage = PortalPage.name("name").description("desc").owner(owner).build();
        final Index.Result indexResult = new MockResult();
        final SharedEntity indexableSearchRequest = new IndexableSharedEntity<>(1L, "name", "desc", SearchRequest.ENTITY_TYPE, owner, 0L);

        when(applicationProperties.getDefaultBackedString(MIN_BATCH_SIZE)).thenReturn("50");
        when(portalPageManager.getAllIndexableSharedEntities()).thenReturn(new MockCloseableIterable<>(singletonList(portalPage)));
        when(searchRequestManager.getAllIndexableSharedEntities()).thenReturn(new MockCloseableIterable<>(singletonList(indexableSearchRequest)));
        when(indexer.index(indexableSearchRequest, false)).thenReturn(indexResult);
        when(indexer.index(portalPage, false)).thenReturn(indexResult);

//        sharedEntityIndexManager = new DefaultSharedEntityIndexManager(indexer, searchRequestManager, portalPageManager, fileFactory, applicationProperties);
    }

    @Test
    public void testReIndexAll() throws IndexException {
        final long time = sharedEntityIndexManager.reIndexAll(Contexts.nullContext());
        assertThat(time, greaterThanOrEqualTo(0L));
    }

    @Test
    public void testReIndexAllThrowsIllegalArgForNullEvent() throws IndexException {
        try {
            sharedEntityIndexManager.reIndexAll(null);
            fail("IllegalArg expected");
        } catch (final IllegalArgumentException ignore) {
        }
    }

    @Test
    public void testActivate() throws Exception {
        sharedEntityIndexManager.activate(Contexts.nullContext());
    }

    @Test
    public void testActivateThrowsIllegalArgForNullEvent() throws Exception {
        try {
            sharedEntityIndexManager.activate(null);
            fail("IllegalArg expected");
        } catch (final IllegalArgumentException ignore) {
        }
    }

    @Test
    public void testShutdown() {
        final SharedEntityIndexer indexer = mock(SharedEntityIndexer.class);
        indexer.shutdown(SearchRequest.ENTITY_TYPE);
        indexer.shutdown(PortalPage.ENTITY_TYPE);

        sharedEntityIndexManager.shutdown();
    }

    @Test
    public void testDeactivate() throws Exception {
        final SharedEntityIndexer indexer = mock(SharedEntityIndexer.class);

        when(indexer.clear(ENTITY_TYPE)).thenReturn("testing");
        when(indexer.clear(PortalPage.ENTITY_TYPE)).thenReturn("testing");

        sharedEntityIndexManager.deactivate();
    }

    @Test
    public void testOptimize() throws Exception {
        when(indexer.optimize(ENTITY_TYPE)).thenReturn(5L);
        when(indexer.optimize(PortalPage.ENTITY_TYPE)).thenReturn(2L);

        final long actualTime = sharedEntityIndexManager.optimize();
        assertThat(actualTime, is(7L));
    }

    @Test
    public void testIsIndexingEnabled() {
        assertThat(sharedEntityIndexManager.isIndexingEnabled(), is(true));
    }

    @Test
    public void testMultiThreadedConfigurationDefaults() {
        final MultiThreadedIndexingConfiguration multiThreadedIndexingConfiguration = new DefaultSharedEntityIndexManager.PropertiesAdapter(new MockApplicationProperties());
        assertThat(multiThreadedIndexingConfiguration.minimumBatchSize(), is(50));
        assertThat(multiThreadedIndexingConfiguration.maximumQueueSize(), is(1000));
        assertThat(multiThreadedIndexingConfiguration.noOfThreads(), is(10));
    }

    @Test
    public void testMultiThreadedConfigurationCustom() {
        final MockApplicationProperties applicationProperties = new MockApplicationProperties();
        applicationProperties.setString(APKeys.JiraIndexConfiguration.SharedEntity.MIN_BATCH_SIZE, "1");
        applicationProperties.setString(APKeys.JiraIndexConfiguration.SharedEntity.MAX_QUEUE_SIZE, "2");
        applicationProperties.setString(APKeys.JiraIndexConfiguration.SharedEntity.THREADS, "3");

        final MultiThreadedIndexingConfiguration multiThreadedIndexingConfiguration = new DefaultSharedEntityIndexManager.PropertiesAdapter(applicationProperties);
        assertThat(multiThreadedIndexingConfiguration.minimumBatchSize(), is(1));
        assertThat(multiThreadedIndexingConfiguration.maximumQueueSize(), is(2));
        assertThat(multiThreadedIndexingConfiguration.noOfThreads(), is(3));
    }

    @Test
    public void testGetAllIndexPaths() {
        final Collection<String> expectedList = newArrayList("This should be returned");
        when(indexer.getAllIndexPaths()).thenReturn(expectedList);

        final Collection<String> actualList = sharedEntityIndexManager.getAllIndexPaths();
        assertThat(actualList, sameInstance(expectedList));
    }
}
