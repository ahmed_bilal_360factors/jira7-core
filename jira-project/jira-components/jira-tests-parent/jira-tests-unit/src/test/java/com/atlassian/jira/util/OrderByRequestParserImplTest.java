package com.atlassian.jira.util;

import com.atlassian.fugue.Either;
import com.atlassian.jira.mock.i18n.MockI18nHelper;
import org.hamcrest.Matcher;
import org.junit.Test;

import static com.atlassian.jira.util.OrderByRequest.asc;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

public class OrderByRequestParserImplTest {

    private final OrderByRequestParser parser = new OrderByRequestParserImpl(new MockI18nHelper());

    @Test
    public void testParsingQueryWithExactEnumValue() {
        assertThat(parser.parse("name", Fields.class).right().get(), equalTo(asc(Fields.name)));
        assertThat(parser.parse("+name", Fields.class).right().get(), equalTo(asc(Fields.name)));
        assertThat(parser.parse("-name", Fields.class).right().get(), equalTo(OrderByRequest.desc(Fields.name)));
    }

    @Test
    public void valueToParseIsTrimmedBeforeParsingFromBothSides() {
        assertThat(parser.parse("  -  name  ", Fields.class), (Matcher) equalTo(Either.right(OrderByRequest.desc(Fields.name))));
    }

    @Test
    public void parsingIsCaseInsensitive() {
        assertThat(parser.parse("NAME", Fields.class).right().get(), equalTo(asc(Fields.name)));
        assertThat(parser.parse("-NAME", Fields.class).right().get(), equalTo(OrderByRequest.desc(Fields.name)));
    }

    @Test
    public void parseMethodFailsIfNoEnumValueIsMatched() {
        assertThat(parser.parse("invalidField", Fields.class).left().get(), equalTo(error("util.errors.order.query.parse [invalidField] [[id, name, description]]")));
        assertThat(parser.parse("invalidField+", Fields.class).left().get(), equalTo(error("util.errors.order.query.parse [invalidField+] [[id, name, description]]")));
        assertThat(parser.parse("/name", Fields.class).left().get(), equalTo(error("util.errors.order.query.parse [/name] [[id, name, description]]")));
        assertThat(parser.parse("++name", Fields.class).left().get(), equalTo(error("util.errors.order.query.parse [+name] [[id, name, description]]")));
        assertThat(parser.parse("+", Fields.class).left().get(), equalTo(error("util.errors.order.query.parse [] [[id, name, description]]")));
        assertThat(parser.parse("-", Fields.class).left().get(), equalTo(error("util.errors.order.query.parse [] [[id, name, description]]")));
        assertThat(parser.parse("", Fields.class).left().get(), equalTo(error("util.errors.order.query.parse [] [[id, name, description]]")));
    }

    @Test
    public void parseMethodFailsGracefullyWhenNullStringIsPassed() {
        assertThat(parser.parse(null, Fields.class).left().get(), equalTo(error("util.errors.order.query.parse [] [[id, name, description]]")));
    }

    private ErrorCollection error(final String errorMessage) {
        return new SimpleErrorCollection(errorMessage, ErrorCollection.Reason.VALIDATION_FAILED);
    }
}

