package com.atlassian.jira.bc.project;

import com.atlassian.beehive.ClusterLockService;
import com.atlassian.jira.association.NodeAssociationStore;
import com.atlassian.jira.avatar.AvatarManager;
import com.atlassian.jira.bc.project.component.ProjectComponentManager;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.event.project.ProjectEventManager;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.fields.screen.issuetype.IssueTypeScreenSchemeManager;
import com.atlassian.jira.issue.security.IssueSecuritySchemeManager;
import com.atlassian.jira.notification.NotificationSchemeManager;
import com.atlassian.jira.permission.PermissionSchemeManager;
import com.atlassian.jira.project.AssigneeTypes;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectCreateNotifier;
import com.atlassian.jira.project.ProjectCreatedData;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.project.template.ProjectTemplateKey;
import com.atlassian.jira.project.template.ProjectTemplateManager;
import com.atlassian.jira.project.type.ProjectTypeKey;
import com.atlassian.jira.project.type.ProjectTypeUpdatedNotifier;
import com.atlassian.jira.project.util.ProjectKeyStore;
import com.atlassian.jira.project.version.VersionManager;
import com.atlassian.jira.scheme.SchemeFactory;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.sharing.SharePermissionDeleteUtils;
import com.atlassian.jira.task.TaskManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.jira.workflow.WorkflowManager;
import com.atlassian.jira.workflow.WorkflowSchemeManager;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Optional;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestDefaultProjectServiceNotificationsOnProjectCreate {
    private static final String PROJECT_TEMPLATE = "template";
    private static final long EXISTING_PROJECT_ID = 10000L;

    @Mock
    private ProjectManager projectManager;
    @Mock
    private ProjectCreateNotifier notifier;

    private ProjectServiceExposingDeleteMethods service;

    @Before
    public void setUp() {
        service = new ProjectServiceExposingDeleteMethods(projectManager, notifier);
    }

    @Test
    public void allHandlersAreNotifiedAboutANewProjectBeingCreated() {
        Project createdProject = mockCreatedProject();
        ProjectCreatedData projectCreatedData = projectCreationData(createdProject, PROJECT_TEMPLATE);
        when(notifier.notifyAllHandlers(projectCreatedData)).thenReturn(true);

        service.createProject(createProjectValidationResultWith(PROJECT_TEMPLATE));

        verify(notifier).notifyAllHandlers(projectCreatedData);
    }

    @Test
    public void allHandlersAreNotifiedWithCorrectCreationDataType() {
        when(projectManager.getProjectObj(EXISTING_PROJECT_ID)).thenReturn(new MockProject(EXISTING_PROJECT_ID));
        Project createdProject = mockCreatedProject();
        ProjectCreatedData projectCreatedData = projectCreationData(createdProject, EXISTING_PROJECT_ID);
        when(notifier.notifyAllHandlers(projectCreatedData)).thenReturn(true);

        service.createProject(createProjectValidationResultWith(EXISTING_PROJECT_ID));

        verify(notifier).notifyAllHandlers(projectCreatedData);
    }

    @Test
    public void newProjectIsDeletedIfOneOfTheHandlersFails() {
        Project createdProject = mockCreatedProject();
        ProjectCreatedData projectCreatedData = projectCreationData(createdProject, PROJECT_TEMPLATE);
        when(notifier.notifyAllHandlers(projectCreatedData)).thenReturn(false);

        try {
            service.createProject(createProjectValidationResultWith(PROJECT_TEMPLATE));
            fail("An exception was expected to be thrown");
        } catch (Exception ex) {
            // do nothing, we are expecting this
        }
        assertThatCreatedProjectWasDeleted(createdProject);
    }

    private void assertThatCreatedProjectWasDeleted(final Project createdProject) {
        assertTrue(service.projectWasDeleted(createdProject));
    }

    private Project mockCreatedProject() {
        Project createdProject = projectWithKey("ANY");
        when(projectManager.createProject(any(ApplicationUser.class), any(ProjectCreationData.class))).thenReturn(createdProject);
        return createdProject;
    }

    private ProjectService.CreateProjectValidationResult createProjectValidationResultWith(String projectTemplateKey) {
        ApplicationUser user = new MockApplicationUser("user");
        ProjectCreationData creationData = new ProjectCreationData.Builder()
                .withKey("TEST")
                .withLead(new MockApplicationUser("lead"))
                .withAssigneeType(AssigneeTypes.PROJECT_LEAD)
                .withProjectTemplateKey(projectTemplateKey)
                .build();

        return new ProjectService.CreateProjectValidationResult(new SimpleErrorCollection(), user, creationData);
    }

    private ProjectService.CreateProjectValidationResult createProjectValidationResultWith(Long existingProjectId) {
        ApplicationUser user = new MockApplicationUser("user");
        ProjectCreationData creationData = new ProjectCreationData.Builder()
                .withKey("TEST")
                .withLead(new MockApplicationUser("lead"))
                .withAssigneeType(AssigneeTypes.PROJECT_LEAD)
                .withType(new ProjectTypeKey("business"))
                .build();

        return new ProjectService.CreateProjectValidationResult(new SimpleErrorCollection(), user, creationData, Optional.of(existingProjectId));
    }

    private Project projectWithKey(String key) {
        Project project = mock(Project.class);
        when(project.getKey()).thenReturn(key);
        return project;
    }

    private ProjectCreatedData projectCreationData(Project project, String projectTemplateKey) {
        return new ProjectCreatedData.Builder()
                .withProject(project)
                .withProjectTemplateKey(new ProjectTemplateKey(projectTemplateKey))
                .build();
    }

    private ProjectCreatedData projectCreationData(Project project, Long existingProjectId) {
        return new ProjectCreatedData.Builder()
                .withProject(project)
                .withExistingProjectId(existingProjectId)
                .build();
    }

    private class ProjectServiceExposingDeleteMethods extends DefaultProjectService {
        private String deletedProjectKey;

        public ProjectServiceExposingDeleteMethods(ProjectManager projectManager, ProjectCreateNotifier notifier) {
            super(
                    mock(JiraAuthenticationContext.class),
                    projectManager,
                    mock(ApplicationProperties.class),
                    mock(PermissionManager.class),
                    mock(GlobalPermissionManager.class),
                    mock(PermissionSchemeManager.class),
                    mock(NotificationSchemeManager.class),
                    mock(IssueSecuritySchemeManager.class),
                    mock(SchemeFactory.class),
                    mock(WorkflowSchemeManager.class),
                    mock(IssueTypeScreenSchemeManager.class),
                    mock(CustomFieldManager.class),
                    mock(NodeAssociationStore.class),
                    mock(VersionManager.class),
                    mock(ProjectComponentManager.class),
                    mock(SharePermissionDeleteUtils.class),
                    mock(AvatarManager.class),
                    mock(I18nHelper.BeanFactory.class),
                    mock(WorkflowManager.class),
                    mock(UserManager.class),
                    mock(ProjectEventManager.class),
                    mock(ProjectKeyStore.class),
                    mock(ProjectTypeValidator.class),
                    notifier,
                    mock(ProjectTypeUpdatedNotifier.class),
                    mock(ClusterLockService.class),
                    mock(ProjectTemplateManager.class),
                    mock(ProjectSchemeAssociationManager.class),
                    mock(TaskManager.class),
                    mock(IssueManager.class)
            );
        }

        @Override
        public DeleteProjectResult deleteProject(ApplicationUser user, DeleteProjectValidationResult deleteProjectValidationResult) {
            deletedProjectKey = deleteProjectValidationResult.getProject().getKey();
            return null;
        }

        public boolean projectWasDeleted(Project project) {
            return project.getKey().equals(deletedProjectKey);
        }
    }
}
