package com.atlassian.jira.upgrade.tasks.role;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.crowd.embedded.api.Group;
import com.google.common.collect.Maps;
import org.hamcrest.Description;
import org.hamcrest.TypeSafeDiagnosingMatcher;

import java.util.Map;
import java.util.stream.Collectors;

/**
 * @since v7.0
 */
class ApplicationRolesMatcher extends TypeSafeDiagnosingMatcher<ApplicationRoles> {
    private Map<ApplicationKey, ApplicationRoleMatcher> matchers = Maps.newHashMap();

    @Override
    protected boolean matchesSafely(final ApplicationRoles item, final Description mismatchDescription) {
        final Map<ApplicationKey, ApplicationRole> roles = item.asMap();
        if (matchers.keySet().equals(roles.keySet())) {
            for (Map.Entry<ApplicationKey, ApplicationRoleMatcher> entry : matchers.entrySet()) {
                final ApplicationKey key = entry.getKey();
                final ApplicationRoleMatcher roleMatcher = entry.getValue();

                final ApplicationRole role = roles.get(key);
                if (!roleMatcher.matches(role)) {
                    describeItem(item, mismatchDescription);
                    return false;
                }
            }
            return true;
        } else {
            describeItem(item, mismatchDescription);
            return false;
        }
    }

    private void describeItem(ApplicationRoles item, Description mismatchDescription) {
        mismatchDescription.appendText(item.asMap().entrySet()
                .stream()
                .map(entry -> String.format("%s: %s", entry.getKey(), ApplicationRoleMatcher.toString(entry.getValue())))
                .collect(Collectors.joining(", ")));
    }

    @Override
    public void describeTo(final Description description) {
        description.appendText(matchers.entrySet()
                .stream()
                .map(entry -> String.format("%s: %s", entry.getKey(), entry.getValue()))
                .collect(Collectors.joining(", ")));
    }

    ApplicationRoleMatcher roleMatcher(ApplicationKey key) {
        return matchers.computeIfAbsent(key, ApplicationRoleMatcher::forKey);
    }

    ApplicationRolesMatcher role(ApplicationKey key, Iterable<Group> groups, Iterable<Group> defaults) {
        roleMatcher(key).withGroups(groups).withDefaultGroups(defaults);
        return this;
    }

    ApplicationRolesMatcher role(ApplicationKey key) {
        roleMatcher(key);
        return this;
    }
}
