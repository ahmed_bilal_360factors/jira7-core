package com.atlassian.jira.issue.fields.renderer.wiki.links;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.plugin.contentlinkresolver.ContentLinkResolverDescriptor;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.renderer.links.ContentLinkResolver;
import com.atlassian.renderer.links.GenericLinkParser;
import com.atlassian.renderer.links.Link;
import com.atlassian.renderer.links.UnresolvedLink;
import com.atlassian.renderer.links.UrlLink;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestJiraLinkResolver {
    @Rule
    public MockitoRule mockito = MockitoJUnit.rule();

    @Mock
    private PluginAccessor mockPluginAccessor;

    @Mock
    private EventPublisher mockEventPublisher;

    private JiraLinkResolver tested;

    @Before
    public void setUpResolver() {
        tested = new JiraLinkResolver(mockPluginAccessor, mockEventPublisher);
    }

    @Test
    public void testConstructorRegistersProvider() {
        final EventPublisher eventPublisher = mock(EventPublisher.class);
        new JiraLinkResolver(mockPluginAccessor, eventPublisher);
        verify(eventPublisher).register(Matchers.argThat(org.hamcrest.Matchers.instanceOf(JiraLinkResolver.LinkResolverProvider.class)));
    }

    @Test
    public void testBuildContentLinkResolvers() {
        final MockContentLinkResolver r1 = new MockContentLinkResolver(null);
        final MockContentLinkResolver r2 = new MockContentLinkResolver(null);
        final MockContentLinkResolver r3 = new MockContentLinkResolver(null);

        List<ContentLinkResolverDescriptor> originalDescriptors = Lists.newArrayList(
                newContentLinkResolverDescriptor(null, r1, 10),
                newContentLinkResolverDescriptor(null, r2, 20),
                newContentLinkResolverDescriptor(null, r3, 30)
        );

        setUpMockAccessor(originalDescriptors);
        List<ContentLinkResolver> result = tested.getLinkResolverProvider().getLinkResolvers();
        assertNotNull(result);
        assertEquals(r1, result.get(0));
        assertEquals(r2, result.get(1));
        assertEquals(r3, result.get(2));
    }

    @Test
    public void testBuildContentLinkResolversOutOfOrder() {
        final MockContentLinkResolver r1 = new MockContentLinkResolver(null);
        final MockContentLinkResolver r2 = new MockContentLinkResolver(null);
        final MockContentLinkResolver r3 = new MockContentLinkResolver(null);

        List<ContentLinkResolverDescriptor> originalDescriptors = Lists.newArrayList(
                newContentLinkResolverDescriptor(null, r1, 20),
                newContentLinkResolverDescriptor(null, r2, 10),
                newContentLinkResolverDescriptor(null, r3, 30)
        );

        setUpMockAccessor(originalDescriptors);
        List<ContentLinkResolver> result = tested.getLinkResolverProvider().getLinkResolvers();
        assertNotNull(result);
        assertEquals(r2, result.get(0));
        assertEquals(r1, result.get(1));
        assertEquals(r3, result.get(2));
    }

    @Test
    public void testCreateLink() {
        MockLink mockLink = new MockLink(new GenericLinkParser("[foo|bar]"));
        final MockContentLinkResolver r1 = new MockContentLinkResolver(mockLink);
        setUpMockAccessor(ImmutableList.of(newContentLinkResolverDescriptor(null, r1, 10)));
        Link link = tested.createLink(null, "[foo|bar]");
        assertEquals(mockLink, link);
    }

    @Test
    public void testCreateLinkWithResolverFallThrough() {
        MockLink mockLink = new MockLink(new GenericLinkParser("[foo|bar]"));
        final MockContentLinkResolver r1 = new MockContentLinkResolver(null);
        final MockContentLinkResolver r2 = new MockContentLinkResolver(mockLink);
        setUpMockAccessor(ImmutableList.of(
                newContentLinkResolverDescriptor(null, r1, 10),
                newContentLinkResolverDescriptor(null, r2, 20)
        ));
        Link link = tested.createLink(null, "[foo|bar]");
        assertEquals(mockLink, link);
    }

    @Test
    public void testCreateLinkNoResolversFound() {
        final MockContentLinkResolver r1 = new MockContentLinkResolver(null);
        setUpMockAccessor(ImmutableList.of(newContentLinkResolverDescriptor(null, r1, 10)));
        Link link = tested.createLink(null, "[foo|bar]");
        assertNotNull(link);
        assertTrue("Unexpected link instance: " + link.getClass().getName(), link instanceof UnresolvedLink);
    }

    @Test
    public void testCreateLinkUrlLink() {
        final MockContentLinkResolver r1 = new MockContentLinkResolver(null);
        setUpMockAccessor(ImmutableList.of(newContentLinkResolverDescriptor(null, r1, 10)));
        Link link = tested.createLink(null, "http://jira.atlassian.com/");
        assertNotNull(link);
        assertTrue("Unexpected link instance: " + link.getClass().getName(), link instanceof UrlLink);
    }

    private void setUpMockAccessor(List<ContentLinkResolverDescriptor> originalDescriptors) {
        when(mockPluginAccessor.getEnabledModuleDescriptorsByClass(ContentLinkResolverDescriptor.class)).thenReturn(originalDescriptors);
    }

    private ContentLinkResolverDescriptor newContentLinkResolverDescriptor(final JiraAuthenticationContext authContext,
                                                                           final MockContentLinkResolver resolver, final int order) {
        return new ContentLinkResolverDescriptor(authContext, ModuleFactory.LEGACY_MODULE_FACTORY) {
            @Override
            public int getOrder() {
                return order;
            }

            @Override
            public ContentLinkResolver getModule() {
                return resolver;
            }
        };
    }

}
