package com.atlassian.jira.license;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.cache.CacheManager;
import com.atlassian.cache.memory.MemoryCacheManager;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.extras.api.LicenseException;
import com.atlassian.fugue.Option;
import com.atlassian.jira.application.ApplicationConfigurationHelper;
import com.atlassian.jira.auditing.AuditingManager;
import com.atlassian.jira.auditing.handlers.SystemAuditEventHandler;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.event.ClearCacheEvent;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.mock.MockApplicationProperties;
import com.atlassian.license.SIDManager;
import com.google.common.collect.ImmutableList;
import org.hamcrest.Description;
import org.hamcrest.Matchers;
import org.hamcrest.TypeSafeDiagnosingMatcher;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.util.Objects;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Function;

import static com.atlassian.application.api.ApplicationKey.valueOf;
import static com.atlassian.jira.matchers.OptionMatchers.none;
import static com.atlassian.jira.matchers.OptionMatchers.some;
import static org.hamcrest.CoreMatchers.anyOf;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Tests {@link JiraLicenseManager}.
 *
 * @since v6.3
 */
public class TestJiraLicenseManagerImpl {
    @Rule
    public MockitoContainer mockitoContainer = MockitoMocksInContainer.rule(this);

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    private ApplicationProperties applicationProperties = new MockApplicationProperties();
    @Mock
    private SIDManager sidManager;
    @Mock
    private EventPublisher eventPublisher;
    private MockMultiLicenseStore multiLicenseStore = Mockito.spy(new MockMultiLicenseStore());
    private MockLicenseDetailsFactory licenseDetailsFactory = Mockito.spy(new MockLicenseDetailsFactory());
    private JiraLicenseManagerImpl jiraLicenseManager;
    private CacheManager cacheManager = new MemoryCacheManager();

    @AvailableInContainer
    @Mock
    private AuditingManager auditingManager;

    @AvailableInContainer
    @Mock
    private SystemAuditEventHandler systemAuditEventHandler;

    @AvailableInContainer
    @Mock
    private BuildVersionLicenseCheck buildVersionLicenseCheck;

    @AvailableInContainer
    @Mock
    private ApplicationConfigurationHelper applicationConfigurationHelper;

    @Before
    public void setUp() {
        jiraLicenseManager = new JiraLicenseManagerImpl(sidManager,
                eventPublisher,
                multiLicenseStore,
                licenseDetailsFactory,
                applicationProperties,
                cacheManager);
        mockitoContainer.getMockWorker().registerMock(ApplicationConfigurationHelper.class, applicationConfigurationHelper);
    }

    @Test
    public void getServerIdWithExistingServerIdReturnsExistingId() {
        //given
        final String expectedServerId = "A server ID";
        multiLicenseStore.storeServerId(expectedServerId);

        //when
        final String actualSID = jiraLicenseManager.getServerId();

        //then
        assertThat(actualSID, equalTo(expectedServerId));
        assertThat(multiLicenseStore.retrieveServerId(), equalTo(expectedServerId));
    }

    @Test
    public void getServerIdWithNonExistingServerIdCreatesAndReturnsNewId() {
        //given
        final String expectedServerId = "A server ID";
        when(sidManager.generateSID()).thenReturn(expectedServerId);

        //when
        final String actual = jiraLicenseManager.getServerId();

        //then
        assertThat(actual, equalTo(expectedServerId));
        assertThat(multiLicenseStore.retrieveServerId(), equalTo(expectedServerId));
    }

    @Test
    public void gettingANewLicenseByKeyReturnsNewLicense() {
        //given
        final String newLicense = "newLicense";
        multiLicenseStore.store("license1", "license2", "license3");
        licenseDetailsFactory.byDefaultReturnLicense();

        //when
        final LicenseDetails license = jiraLicenseManager.getLicense(newLicense);

        //then
        assertThat(license.getLicenseString(), equalTo(newLicense));
    }

    @Test
    public void gettingOldLicenseByKeyReturnsExistingCachedLicense() {
        //given
        final String existingLicense = "existingLicense";
        multiLicenseStore.store(existingLicense, "license2", "license3");
        licenseDetailsFactory.byDefaultReturnLicense();

        //when
        final LicenseDetails license = jiraLicenseManager.getLicense(existingLicense);
        final LicenseDetails cachedReference = jiraLicenseManager.getLicense(existingLicense);

        //then
        assertThat(license.getLicenseString(), equalTo(existingLicense));
        assertThat(license, Matchers.sameInstance(cachedReference));
    }

    @Test
    public void gettingInvalidLicenseStringThrowsException() {
        //given
        expectedException.expect(LicenseException.class);

        //when
        jiraLicenseManager.getLicense("bad license");

        //then exception
    }

    @Test
    public void isLicensedLooksAtLicenses() {
        //given
        final ApplicationKey app1 = valueOf("appone");
        final ApplicationKey app2 = valueOf("apptwo");
        final ApplicationKey app3 = valueOf("appthree");
        final String license1 = "license1";
        final String license2 = "license2";

        multiLicenseStore.store(license1, license2);
        licenseDetailsFactory.addLicense(license1).setLicensedApplications(app1);
        licenseDetailsFactory.addLicense(license2).setLicensedApplications(app2);

        //then
        assertThat(jiraLicenseManager.isLicensed(app1), equalTo(true));
        assertThat(jiraLicenseManager.isLicensed(app3), equalTo(false));
    }

    @Test
    public void isDecodeableSimplyDelegates() {
        //given
        when(licenseDetailsFactory.isDecodeable("abc")).thenReturn(false);

        //when
        final boolean decodeable = jiraLicenseManager.isDecodeable("abc");

        assertFalse(decodeable);
        verify(licenseDetailsFactory).isDecodeable("abc");
    }

    @Test
    public void setLicenseRejectsLicenseThatDoesNotDecode() {
        //given
        licenseDetailsFactory.byDefaultThrowError();
        expectedException.expect(IllegalArgumentException.class);

        //when
        jiraLicenseManager.setLicense("someLicense");

        //then exception
    }

    @Test
    public void setLicenseReplacesConflictingLicenses() {
        //given
        final ApplicationKey app1 = valueOf("appone");
        final ApplicationKey app2 = valueOf("apptwo");
        final String keepLicense = "license1";
        final String replaceLicense = "license2";
        final String newLicense = "license3";

        multiLicenseStore.store(keepLicense, replaceLicense);
        licenseDetailsFactory.addLicense(keepLicense).setLicensedApplications(app1);
        licenseDetailsFactory.addLicense(replaceLicense).setLicensedApplications(app2);
        licenseDetailsFactory.addLicense(newLicense).setLicensedApplications(app2);

        //when
        final LicenseDetails details = jiraLicenseManager.setLicense(newLicense);

        //then
        assertThat(details, is(license(newLicense)));
        assertThat(multiLicenseStore.retrieve(), containsInAnyOrder(keepLicense, newLicense));
        verify(eventPublisher).publish(argThat(isChangedEvent()
                .withNewLicense(newLicense)
                .withOldLicense(replaceLicense)));
    }

    @Test
    public void setLicenseAddsNewLicenses() {
        //given
        final ApplicationKey app1 = valueOf("appone");
        final ApplicationKey app2 = valueOf("apptwo");
        final String keepLicense = "license1";
        final String newLicense = "license2";

        multiLicenseStore.store(keepLicense);
        licenseDetailsFactory.addLicense(keepLicense).setLicensedApplications(app1);
        licenseDetailsFactory.addLicense(newLicense).setLicensedApplications(app2);

        //when
        final LicenseDetails details = jiraLicenseManager.setLicense(newLicense);

        //then
        assertThat(details, is(license(newLicense)));
        assertThat(multiLicenseStore.retrieve(), containsInAnyOrder(keepLicense, newLicense));
        verify(eventPublisher).publish(argThat(isChangedEvent()
                .withNewLicense(newLicense)));
    }

    @Test
    public void setLicenseDoesNotDoBuildVersionCheckWhenLicenseNotExtended() {
        //given
        final ApplicationKey app1 = valueOf("appone");
        final String license1 = "license1";
        final String license2 = "license2";

        multiLicenseStore.store(license1);
        licenseDetailsFactory.addLicense(license1).setLicensedApplications(app1);
        licenseDetailsFactory.addLicense(license2).setLicensedApplications(app1);

        //when
        final LicenseDetails details = jiraLicenseManager.setLicense(license2);

        //then
        assertThat(details, is(license(license2)));
        assertThat(multiLicenseStore.retrieve(), contains(license2));
        verify(eventPublisher).publish(argThat(isChangedEvent()
                .withNewLicense(license2)
                .withOldLicense(license1)));

        verify(multiLicenseStore, never()).resetOldBuildConfirmation();
    }

    @Test
    public void setLicenseResetsBuildVersionCheckIfLicenseIsValidAndCurrentLicenseExtended() {
        //given
        final ApplicationKey app1 = valueOf("appone");
        final ApplicationKey app2 = valueOf("apptwo");
        final String license1 = "license1";
        final String license2 = "license2";

        multiLicenseStore.store(license1);
        licenseDetailsFactory.addLicense(license1).setLicensedApplications(app1);
        licenseDetailsFactory.addLicense(license2).setLicensedApplications(app2);

        extendedCurrentLicense();
        when(buildVersionLicenseCheck.evaluateWithoutGracePeriod()).thenReturn(LicenseCheck.PASS);

        //when
        final LicenseDetails details = jiraLicenseManager.setLicense(license2);

        //then

        assertThat(details, is(license(license2)));
        assertThat(multiLicenseStore.retrieve(), contains(license2, license1));
        verify(eventPublisher).publish(argThat(isChangedEvent()
                .withNewLicense(license2)));
        verify(multiLicenseStore).resetOldBuildConfirmation();
    }

    @Test
    public void updateLicenseResetsBuildVersionCheckIfAllLicensesAreValid() {
        //given
        final ApplicationKey app1 = ApplicationKey.valueOf("appone");
        final ApplicationKey app2 = ApplicationKey.valueOf("apptwo");
        final String license1 = "license1";
        final String license2 = "license2";

        multiLicenseStore.store(license1, license2);
        licenseDetailsFactory.addLicense(license1).setLicensedApplications(app1);
        licenseDetailsFactory.addLicense(license2).setLicensedApplications(app2);

        extendedCurrentLicense();
        when(buildVersionLicenseCheck.evaluate()).thenReturn(LicenseCheck.PASS);

        //when
        jiraLicenseManager.removeLicense(app2);

        //then
        verify(multiLicenseStore).resetOldBuildConfirmation();
    }

    @Test
    public void setLicenseDoesNotResetsBuildVersionCheckIfLicenseIsInvalid() {
        //given
        final String license1 = "license1";
        final String license2 = "license2";

        licenseDetailsFactory.addLicense(license1);
        licenseDetailsFactory.addLicense(license2);
        multiLicenseStore.store(license1);

        extendedCurrentLicense();
        when(buildVersionLicenseCheck.evaluateWithoutGracePeriod()).thenReturn(LicenseCheck.FAIL);

        //when
        final LicenseDetails details = jiraLicenseManager.setLicense(license2);

        //then
        assertThat(details, is(license(license2)));
        assertThat(multiLicenseStore.retrieve(), contains(license2, license1));
        verify(eventPublisher).publish(argThat(isChangedEvent()
                .withNewLicense(license2)));
        verify(multiLicenseStore, never()).resetOldBuildConfirmation();
    }

    @Test
    public void setLicenseResetsTheCache() {
        assertSetLicenseClearsCache(jiraLicenseManager::setLicense);
    }

    @Test
    public void setLicenseNoEventRejectsInvalidLicense() {

        //given
        licenseDetailsFactory.byDefaultThrowError();
        expectedException.expect(IllegalArgumentException.class);

        //when
        jiraLicenseManager.setLicenseNoEvent("someLicense");

        //then exception
    }

    @Test
    public void setLicenseNoEventTriggersAuditLogOnServer() {

        //given
        final ApplicationKey app = valueOf("app");
        final String oldLicense = "license1";
        final String newLicense = "license2";

        multiLicenseStore.store(oldLicense);
        licenseDetailsFactory.addLicense(oldLicense).setLicensedApplications(app);
        licenseDetailsFactory.addLicense(newLicense).setLicensedApplications(app);

        //when
        final LicenseDetails details = jiraLicenseManager.setLicenseNoEvent(newLicense);

        //then
        assertThat(details, is(license(newLicense)));
        assertThat(multiLicenseStore.retrieve(), contains(newLicense));
        verify(eventPublisher, never()).publish(any());
        verify(systemAuditEventHandler).onLicenseChangedEvent(argThat(isChangedEvent()
                .withNewLicense(newLicense)
                .withOldLicense(oldLicense)));
    }

    @Test
    public void setLicenseNoEventResetsTheCache() {
        assertSetLicenseClearsCache(jiraLicenseManager::setLicenseNoEvent);
    }

    @Test
    public void confirmProceedUnderEvalTriggersEvalAndFiresEvent() {
        //given
        final String evalUser = "admin";
        final MockLicenseDetails badLicense = licenseDetailsFactory.addLicense("badLicense");
        final ImmutableList<MockLicenseDetails> badLicenses =
                ImmutableList.of(badLicense);
        when(buildVersionLicenseCheck.evaluateWithoutGracePeriod())
                .thenReturn(new LicenseCheck.Failure(badLicenses, "something"));

        //when
        jiraLicenseManager.confirmProceedUnderEvaluationTerms(evalUser);

        //then
        assertThat(multiLicenseStore.getEvalUser(), is(evalUser));
        final ArgumentCaptor<ConfirmEvaluationLicenseEvent> eventArgumentCaptor =
                ArgumentCaptor.forClass(ConfirmEvaluationLicenseEvent.class);
        verify(eventPublisher).publish(eventArgumentCaptor.capture());
        final ConfirmEvaluationLicenseEvent event = eventArgumentCaptor.getValue();
        assertThat(event.getUserWhoConfirmed(), is(evalUser));
        assertThat(event.getExpiredLicenses(), contains(badLicense));
    }

    @Test
    public void hasLicenseTooOldForBuildConfirmationBeenDoneIsFalseWhenNotDone() {
        //then
        assertThat(jiraLicenseManager.hasLicenseTooOldForBuildConfirmationBeenDone(), is(false));
    }

    @Test
    public void hasLicenseTooOldForBuildConfirmationBeenDoneIsTrueWhenDone() {
        //when
        extendedCurrentLicense();

        //then
        assertThat(jiraLicenseManager.hasLicenseTooOldForBuildConfirmationBeenDone(), is(true));
    }

    @Test
    public void getLicensesReturnsAllLicensesReturnsAllLicenses() {
        //given
        final String license1 = "license1";
        final String license2 = "license2";
        licenseDetailsFactory.byDefaultReturnLicense();
        multiLicenseStore.store(license1, license2);

        //when
        final Iterable<LicenseDetails> licenses = jiraLicenseManager.getLicenses();

        //then
        assertThat(licenses, containsInAnyOrder(license(license1), license(license2)));
    }

    @Test
    public void getLicensesIsCached() {
        final String license1 = "license1";
        final String license2 = "license2";
        licenseDetailsFactory.byDefaultReturnLicense();
        multiLicenseStore.store(license1, license2);

        //when
        final Iterable<LicenseDetails> licenses1 = jiraLicenseManager.getLicenses();
        final Iterable<LicenseDetails> licenses2 = jiraLicenseManager.getLicenses();

        //then
        assertThat(licenses1, containsInAnyOrder(license(license1), license(license2)));
        assertThat(licenses2, containsInAnyOrder(license(license1), license(license2)));

        //only called once. The first call is cached.
        verify(multiLicenseStore, times(1)).retrieve();
        verify(licenseDetailsFactory, times(1)).getLicense(license1);
    }

    @Test
    public void getAllLicensedApplicationKeysReturnsAllKeysAndIsCached() {
        final ApplicationKey one = valueOf("one");
        final ApplicationKey two = valueOf("two");
        final ApplicationKey three = valueOf("three");
        final MockLicenseDetails license1 = licenseDetailsFactory.addLicense("license1")
                .setLicensedApplications(one, two);
        final MockLicenseDetails license2 = licenseDetailsFactory.addLicense("license2")
                .setLicensedApplications(three);
        multiLicenseStore.storeDetails(license1, license2);

        //when
        final Set<ApplicationKey> keys1 = jiraLicenseManager.getAllLicensedApplicationKeys();
        final Set<ApplicationKey> keys2 = jiraLicenseManager.getAllLicensedApplicationKeys();

        //then
        assertThat(keys1, containsInAnyOrder(one, two, three));
        assertThat(keys2, containsInAnyOrder(one, two, three));

        //only called once. The first call is cached.
        verify(multiLicenseStore, times(1)).retrieve();
        verify(licenseDetailsFactory, times(1)).getLicense(license1.getLicenseString());
        verify(licenseDetailsFactory, times(1)).getLicense(license2.getLicenseString());
    }

    @Test
    public void getSupportEntitlementNumbersReturnsAllSensAndIsCached() {
        final String sen1 = "zxy";
        final String sen2 = "abc";
        final MockLicenseDetails license1 = licenseDetailsFactory.addLicense("license1")
                .setSupportEntitlementNumber(sen1);
        final MockLicenseDetails license2 = licenseDetailsFactory.addLicense("license2")
                .setSupportEntitlementNumber(sen2);
        final MockLicenseDetails noSen = licenseDetailsFactory.addLicense("license3")
                .setSupportEntitlementNumber(null);
        final MockLicenseDetails emptySen = licenseDetailsFactory.addLicense("license4")
                .setSupportEntitlementNumber("");
        final MockLicenseDetails spaceSen = licenseDetailsFactory.addLicense("license5")
                .setSupportEntitlementNumber("  ");
        multiLicenseStore.storeDetails(license1, license2, noSen, emptySen, spaceSen);

        //when
        final Set<String> sens1 = jiraLicenseManager.getSupportEntitlementNumbers();
        final Set<String> sens2 = jiraLicenseManager.getSupportEntitlementNumbers();

        //then

        //the order of the sens is important.
        assertThat(sens1, contains(sen2, sen1));
        assertThat(sens2, contains(sen2, sen1));

        //only called once. The first call is cached.
        verify(multiLicenseStore, times(1)).retrieve();
        verify(licenseDetailsFactory, times(1)).getLicense(license1.getLicenseString());
        verify(licenseDetailsFactory, times(1)).getLicense(license2.getLicenseString());
    }

    @Test
    public void getLicenseByKeyReturnsNoneWhenLicenseDoesNotExist() {

        //given
        final ApplicationKey appone = valueOf("appone");
        final ApplicationKey apptwo = valueOf("apptwo");
        final ApplicationKey appthree = valueOf("appthree");

        final MockLicenseDetails license1 = licenseDetailsFactory.addLicense("license1")
                .setLicensedApplications(appone);
        final MockLicenseDetails license2 = licenseDetailsFactory.addLicense("license2")
                .setLicensedApplications(apptwo);
        multiLicenseStore.storeDetails(license1, license2);

        //when
        final Option<LicenseDetails> result = jiraLicenseManager.getLicense(appthree);

        //then
        assertThat(result, none());
    }

    @Test
    public void getLicenseByKeyReturnsSomeWhenLicenseExists() {
        //given
        final ApplicationKey appone = valueOf("appone");
        final ApplicationKey apptwo = valueOf("apptwo");
        final ApplicationKey appthree = valueOf("appthree");

        final MockLicenseDetails license1 = licenseDetailsFactory.addLicense("license1")
                .setLicensedApplications(appone);
        final MockLicenseDetails license2 = licenseDetailsFactory.addLicense("license2")
                .setLicensedApplications(apptwo, appthree);
        multiLicenseStore.storeDetails(license1, license2);

        //when
        final Option<LicenseDetails> result = jiraLicenseManager.getLicense(appthree);

        //then
        assertThat(result, some(license(license2)));
    }

    @Test
    public void getLicenseByKeyIsCached() {
        //given
        final ApplicationKey appone = valueOf("appone");
        final ApplicationKey apptwo = valueOf("apptwo");

        final MockLicenseDetails license1 = licenseDetailsFactory.addLicense("license1")
                .setLicensedApplications(appone);
        multiLicenseStore.storeDetails(license1);

        //when
        final Option<LicenseDetails> someResult = jiraLicenseManager.getLicense(appone);
        final Option<LicenseDetails> someResult2 = jiraLicenseManager.getLicense(appone);
        final Option<LicenseDetails> none = jiraLicenseManager.getLicense(apptwo);
        final Option<LicenseDetails> none2 = jiraLicenseManager.getLicense(apptwo);

        //then
        assertThat(someResult, some(license(license1)));
        assertThat(someResult2, some(license(license1)));
        assertThat(none, none());
        assertThat(none2, none());
        verify(multiLicenseStore, times(1)).retrieve();
        verify(licenseDetailsFactory, times(1)).getLicense(license1.getLicenseString());
    }

    @Test
    public void clearAndSetLicenseFailsWithInvalidLicense() {
        //given
        licenseDetailsFactory.byDefaultThrowError();
        expectedException.expect(IllegalArgumentException.class);

        //when
        jiraLicenseManager.clearAndSetLicense("badString");

        //then exception
    }

    @Test
    public void clearAndSetResetsAllLicensesSetsAllLicenses() {
        //given
        final String newLicenseString = "newLicense";
        final MockLicenseDetails existing = licenseDetailsFactory.addLicense("license1");
        licenseDetailsFactory.addLicense(newLicenseString);
        multiLicenseStore.storeDetails(existing);

        //when
        jiraLicenseManager.clearAndSetLicense(newLicenseString);

        //then
        assertThat(multiLicenseStore.retrieve(), containsInAnyOrder(newLicenseString));
        verify(eventPublisher).publish(argThat(isChangedEvent()
                .withNewLicense(newLicenseString)));
    }

    @Test
    public void clearAndSetClearsCache() {
        assertClearAndSetLicenseClearsCache(str ->
        {
            jiraLicenseManager.clearAndSetLicense(str);
            return new MockLicenseDetails().setLicenseString(str);
        });
    }

    @Test
    public void clearAndSetShouldPassPreviousLicenseWhenUpgradingWithAssociatedNewKey_server() {
        //given
        final ApplicationKey sharedApp = ApplicationKey.valueOf("sharedApp");
        final ApplicationKey newApp = ApplicationKey.valueOf("newApp");

        final MockLicenseDetails existingLicenseWithSharedApp = licenseDetailsFactory.addLicense("oldLicense")
                .setLicensedApplications(sharedApp);
        multiLicenseStore.storeDetails(existingLicenseWithSharedApp);

        final MockLicenseDetails newLicenseWithSharedApp = licenseDetailsFactory.addLicense("newLicenseWithSharedApp")
                .setLicensedApplications(sharedApp, newApp);

        //when
        jiraLicenseManager.clearAndSetLicense(newLicenseWithSharedApp.getLicenseString());

        //then
        final ArgumentCaptor<LicenseChangedEvent> eventArgumentCaptor =
                ArgumentCaptor.forClass(LicenseChangedEvent.class);
        verify(eventPublisher).publish(eventArgumentCaptor.capture());
        final LicenseChangedEvent event = eventArgumentCaptor.getValue();

        assertThat(event.getPreviousLicenseDetails(), equalTo(Option.option(existingLicenseWithSharedApp)));
        assertThat(event.getNewLicenseDetails(), equalTo(Option.option(newLicenseWithSharedApp)));
        assertTrue(event.isLicenseUpdated());
    }

    @Test
    public void clearAndSetShouldPassPreviousLicenseWhenNewLicenseIsTheSame_server() {
        //given
        final ApplicationKey app = ApplicationKey.valueOf("app");

        final MockLicenseDetails license = licenseDetailsFactory.addLicense("license")
                .setLicensedApplications(app);
        multiLicenseStore.storeDetails(license);

        //when
        jiraLicenseManager.clearAndSetLicense(license.getLicenseString());

        //then
        final ArgumentCaptor<LicenseChangedEvent> eventArgumentCaptor =
                ArgumentCaptor.forClass(LicenseChangedEvent.class);
        verify(eventPublisher).publish(eventArgumentCaptor.capture());
        final LicenseChangedEvent event = eventArgumentCaptor.getValue();

        assertThat(event.getPreviousLicenseDetails(), equalTo(Option.option(license)));
        assertThat(event.getNewLicenseDetails(), equalTo(Option.option(license)));
        assertTrue(event.isLicenseUpdated());
    }

    @Test
    public void clearAndSetShouldNotPassPreviousLicenseWhenUpgradingNotAssociatedWithNewKey_server() {
        //given
        final ApplicationKey notSharedApp = ApplicationKey.valueOf("notSharedApp");
        final ApplicationKey newApp = ApplicationKey.valueOf("sharedApp");

        final MockLicenseDetails oldLicense = licenseDetailsFactory.addLicense("license")
                .setLicensedApplications(notSharedApp);
        multiLicenseStore.storeDetails(oldLicense);

        final MockLicenseDetails newLicense = licenseDetailsFactory.addLicense("newLicense")
                .setLicensedApplications(newApp);

        //when
        jiraLicenseManager.clearAndSetLicense(newLicense.getLicenseString());

        //then
        final ArgumentCaptor<LicenseChangedEvent> eventArgumentCaptor =
                ArgumentCaptor.forClass(LicenseChangedEvent.class);
        verify(eventPublisher).publish(eventArgumentCaptor.capture());
        final LicenseChangedEvent event = eventArgumentCaptor.getValue();

        assertThat(event.getPreviousLicenseDetails(), equalTo(Option.none()));
        assertThat(event.getNewLicenseDetails(), equalTo(Option.option(newLicense)));
        assertFalse(event.isLicenseUpdated());
    }

    @Test
    public void clearAndSetShouldPassPreviousLicenseWhenDowngradingWithAssociatedNewKey_server() {
        //given
        final ApplicationKey notSharedApp = ApplicationKey.valueOf("notSharedApp");
        final ApplicationKey sharedApp = ApplicationKey.valueOf("sharedApp");

        final MockLicenseDetails oldLicense = licenseDetailsFactory.addLicense("license1")
                .setLicensedApplications(notSharedApp);
        final MockLicenseDetails existingLicenseWithSharedApp = licenseDetailsFactory.addLicense("license2")
                .setLicensedApplications(sharedApp);
        multiLicenseStore.storeDetails(oldLicense, existingLicenseWithSharedApp);

        final MockLicenseDetails newLicenseWithSharedApp = licenseDetailsFactory.addLicense("newLicenseWithSharedApp")
                .setLicensedApplications(sharedApp);

        //when
        jiraLicenseManager.clearAndSetLicense(newLicenseWithSharedApp.getLicenseString());

        //then
        final ArgumentCaptor<LicenseChangedEvent> eventArgumentCaptor =
                ArgumentCaptor.forClass(LicenseChangedEvent.class);
        verify(eventPublisher).publish(eventArgumentCaptor.capture());
        final LicenseChangedEvent event = eventArgumentCaptor.getValue();

        assertThat(event.getPreviousLicenseDetails(), equalTo(Option.option(existingLicenseWithSharedApp)));
        assertThat(event.getNewLicenseDetails(), equalTo(Option.option(newLicenseWithSharedApp)));
        assertTrue(event.isLicenseUpdated());
    }

    @Test
    public void clearAndSetShouldPassAnyAssociatedPreviousLicenseWhenDowngradingWithMultipleAssociatedNewKey_server() {
        //given
        final ApplicationKey notSharedApp = ApplicationKey.valueOf("notSharedApp");
        final ApplicationKey sharedApp1 = ApplicationKey.valueOf("sharedApp-one");
        final ApplicationKey sharedApp2 = ApplicationKey.valueOf("sharedApp-two");

        final MockLicenseDetails oldLicense = licenseDetailsFactory.addLicense("license1")
                .setLicensedApplications(notSharedApp);
        final MockLicenseDetails existingLicenseWithSharedApp1 = licenseDetailsFactory.addLicense("license2")
                .setLicensedApplications(sharedApp1);
        final MockLicenseDetails existingLicenseWithSharedApp2 = licenseDetailsFactory.addLicense("license3")
                .setLicensedApplications(sharedApp2);
        multiLicenseStore.storeDetails(oldLicense, existingLicenseWithSharedApp1, existingLicenseWithSharedApp2);

        final MockLicenseDetails newLicenseWithSharedApps = licenseDetailsFactory.addLicense("newLicenseWithSharedApps")
                .setLicensedApplications(sharedApp1, sharedApp2);

        //when
        jiraLicenseManager.clearAndSetLicense(newLicenseWithSharedApps.getLicenseString());

        //then
        final ArgumentCaptor<LicenseChangedEvent> eventArgumentCaptor =
                ArgumentCaptor.forClass(LicenseChangedEvent.class);
        verify(eventPublisher).publish(eventArgumentCaptor.capture());
        final LicenseChangedEvent event = eventArgumentCaptor.getValue();

        assertThat(event.getPreviousLicenseDetails(),
                anyOf(equalTo(Option.option(existingLicenseWithSharedApp1)),
                        equalTo(Option.option(existingLicenseWithSharedApp2))));

        assertThat(event.getNewLicenseDetails(), equalTo(Option.option(newLicenseWithSharedApps)));
        assertTrue(event.isLicenseUpdated());
    }

    @Test
    public void clearAndSetShouldNotPassPreviousLicenseWhenDowngradingWithNotAssociatedNewKey_server() {
        //given
        final ApplicationKey notSharedApp1 = ApplicationKey.valueOf("notSharedApp-one");
        final ApplicationKey notSharedApp2 = ApplicationKey.valueOf("notSharedApp-two");
        final ApplicationKey newApp = ApplicationKey.valueOf("newApp");

        final MockLicenseDetails oldLicense1 = licenseDetailsFactory.addLicense("license1")
                .setLicensedApplications(notSharedApp1);
        final MockLicenseDetails oldLicense2 = licenseDetailsFactory.addLicense("license2")
                .setLicensedApplications(notSharedApp2);
        multiLicenseStore.storeDetails(oldLicense1, oldLicense2);

        final MockLicenseDetails newLicense = licenseDetailsFactory.addLicense("newLicense")
                .setLicensedApplications(newApp);

        //when
        jiraLicenseManager.clearAndSetLicense(newLicense.getLicenseString());

        //then
        final ArgumentCaptor<LicenseChangedEvent> eventArgumentCaptor =
                ArgumentCaptor.forClass(LicenseChangedEvent.class);
        verify(eventPublisher).publish(eventArgumentCaptor.capture());
        final LicenseChangedEvent event = eventArgumentCaptor.getValue();

        assertThat(event.getPreviousLicenseDetails(), equalTo(Option.none()));
        assertThat(event.getNewLicenseDetails(), equalTo(Option.option(newLicense)));
        assertFalse(event.isLicenseUpdated());
    }

    @Test
    public void clearAndSetShouldPassEmptyOptionWhenNoPreviousLicense() {
        //given
        multiLicenseStore.clear();
        final ApplicationKey newApp = ApplicationKey.valueOf("newApp");
        final MockLicenseDetails newLicense = licenseDetailsFactory.addLicense("newLicense")
                .setLicensedApplications(newApp);

        //when
        jiraLicenseManager.clearAndSetLicense(newLicense.getLicenseString());

        //then
        final ArgumentCaptor<LicenseChangedEvent> eventArgumentCaptor =
                ArgumentCaptor.forClass(LicenseChangedEvent.class);
        verify(eventPublisher).publish(eventArgumentCaptor.capture());
        final LicenseChangedEvent event = eventArgumentCaptor.getValue();

        assertThat(event.getPreviousLicenseDetails(), equalTo(Option.none()));
        assertThat(event.getNewLicenseDetails(), equalTo(Option.option(newLicense)));
        assertFalse(event.isLicenseUpdated());
    }

    @Test
    public void clearAndSetNoEventLicenseFailsWithInvalidLicense() {
        //given
        licenseDetailsFactory.byDefaultThrowError();
        expectedException.expect(IllegalArgumentException.class);

        //when
        jiraLicenseManager.clearAndSetLicenseNoEvent("badString");

        //then exception
    }

    @Test
    public void clearAndSetNoEventLicenseSetsAllLicenses() {
        //given
        final String newLicenseString = "newLicense";
        final MockLicenseDetails existing = licenseDetailsFactory.addLicense("license1");
        final MockLicenseDetails existing2 = licenseDetailsFactory.addLicense("license2");
        licenseDetailsFactory.addLicense(newLicenseString);
        multiLicenseStore.storeDetails(existing, existing2);

        //when
        final LicenseDetails licenseDetails = jiraLicenseManager.clearAndSetLicenseNoEvent(newLicenseString);

        //then
        assertThat(licenseDetails, is(license(newLicenseString)));
        assertThat(multiLicenseStore.retrieve(), containsInAnyOrder(newLicenseString));
        verify(eventPublisher, never()).publish(any());
    }

    @Test
    public void clearAndSetNoEventClearsCache() {
        assertClearAndSetLicenseClearsCache(jiraLicenseManager::clearAndSetLicenseNoEvent);
    }

    @Test
    public void removeLicenseErrorsOutWhenTryingToRemoveLastLicense() {
        //given
        final ApplicationKey app = valueOf("app");
        final MockLicenseDetails existing = licenseDetailsFactory.addLicense("license1")
                .setLicensedApplications(app);
        multiLicenseStore.storeDetails(existing);
        expectedException.expect(IllegalStateException.class);

        //when
        jiraLicenseManager.removeLicense(app);

        //then exception expected.
    }

    @Test
    public void removeLicenseRemovesCorrectLicense() {
        //given
        final ApplicationKey app = valueOf("app");
        final ApplicationKey appToRemove = valueOf("remove");
        final MockLicenseDetails keepDetails = licenseDetailsFactory.addLicense("license1")
                .setLicensedApplications(app);
        final MockLicenseDetails removeDetails = licenseDetailsFactory.addLicense("removeLicense")
                .setLicensedApplications(appToRemove);
        multiLicenseStore.storeDetails(keepDetails, removeDetails);

        //when
        jiraLicenseManager.removeLicense(appToRemove);

        //then
        assertThat(multiLicenseStore.retrieve(), contains(keepDetails.getLicenseString()));
        verify(eventPublisher).publish(argThat(isChangedEvent().withOldLicense(removeDetails)));
    }

    @Test
    public void removeLicenseIgnoresRequestToRemoveLicenseThatDoesNotExist() {
        //given
        final ApplicationKey app = valueOf("app");
        final ApplicationKey appToRemove = valueOf("remove");
        final MockLicenseDetails keepDetails = licenseDetailsFactory.addLicense("license1")
                .setLicensedApplications(app);
        multiLicenseStore.storeDetails(keepDetails);

        //when
        jiraLicenseManager.removeLicense(appToRemove);

        //then
        assertThat(multiLicenseStore.retrieve(), contains(keepDetails.getLicenseString()));
        verify(eventPublisher, never()).publish(any());
    }

    @Test
    public void removeLicenseClearsCache() {
        assertRemoveLicenseClearsCache(details ->
        {
            final ApplicationKey toDelete = details.getLicensedApplications().getKeys().iterator().next();
            jiraLicenseManager.removeLicense(toDelete);
            return null;
        });
    }

    @Test
    public void removeLicensesErrorsOutWhenTryingToRemoveLastLicense() {
        //given
        final MockLicenseDetails remove1 = licenseDetailsFactory.addLicense("license1");
        final MockLicenseDetails remove2 = licenseDetailsFactory.addLicense("license2");
        multiLicenseStore.storeDetails(remove1, remove2);
        expectedException.expect(IllegalStateException.class);

        //when
        jiraLicenseManager.removeLicenses(ImmutableList.of(remove1, remove2));

        //then exception expected.
    }

    @Test
    public void removeLicensesRemovesCorrectLicenses() {
        //given
        final MockLicenseDetails remove1 = licenseDetailsFactory.addLicense("license1");
        final MockLicenseDetails remove2 = licenseDetailsFactory.addLicense("license2");
        final MockLicenseDetails leave1 = licenseDetailsFactory.addLicense("license3");
        final MockLicenseDetails ignored = licenseDetailsFactory.addLicense("license4");
        multiLicenseStore.storeDetails(remove1, remove2, leave1);

        //when
        jiraLicenseManager.removeLicenses(ImmutableList.of(remove1, remove2, ignored));

        //then
        assertThat(multiLicenseStore.retrieve(), containsInAnyOrder(leave1.getLicenseString()));
        verify(eventPublisher).publish(argThat(isChangedEvent().withOldLicense(remove1)));
        verify(eventPublisher).publish(argThat(isChangedEvent().withOldLicense(remove2)));
        verify(eventPublisher, never()).publish(argThat(isChangedEvent().withOldLicense(ignored)));
        verify(eventPublisher, never()).publish(argThat(isChangedEvent().withOldLicense(leave1)));
    }

    @Test
    public void removeLicensesClearsCache() {
        assertRemoveLicenseClearsCache(details ->
        {
            jiraLicenseManager.removeLicenses(ImmutableList.of(details));
            return null;
        });
    }

    @Test
    public void isLicenseSetReturnsTrueWhenThereAreLicenses() {
        //given
        licenseDetailsFactory.byDefaultReturnLicense();
        multiLicenseStore.store("license1", "license2", "license3");

        //then
        assertTrue(jiraLicenseManager.isLicenseSet());
    }

    @Test
    public void isLicenseSetReturnsFalseWhenThereAreNoLicenses() {
        //given
        multiLicenseStore.store();

        //then
        assertFalse(jiraLicenseManager.isLicenseSet());
    }

    @Test
    public void onClearCacheClearsTheCache() {
        assertClearsCache(() -> jiraLicenseManager.onCacheClear(ClearCacheEvent.INSTANCE));
    }

    @Test
    public void testThatConsumerCalledOnClearCache() {
        final int[] consumerCallCount = {3};
        jiraLicenseManager.subscribeToClearCache(Void -> consumerCallCount[0]++);
        jiraLicenseManager.onCacheClear(ClearCacheEvent.INSTANCE);
        assertThat(consumerCallCount[0], is(4));
    }

    @Test
    public void testThatWhenConsumerUnSubscriberItsNotCalledOnClearCache() {
        final int[] consumerCallCount = {3};
        final Consumer<Void> countConsumer = Void -> consumerCallCount[0]++;
        jiraLicenseManager.subscribeToClearCache(countConsumer);
        jiraLicenseManager.onCacheClear(ClearCacheEvent.INSTANCE);
        assertThat(consumerCallCount[0], is(4));
        jiraLicenseManager.unSubscribeFromClearCache(countConsumer);
        jiraLicenseManager.onCacheClear(ClearCacheEvent.INSTANCE);
        assertThat(consumerCallCount[0], is(4));
    }

    @Test
    public void testThatWhenConsumerUnSubscriberMultipleTimesIstNoop() {
        final int[] consumerCallCount = {3};
        final Consumer<Void> countConsumer = Void -> consumerCallCount[0]++;
        jiraLicenseManager.subscribeToClearCache(countConsumer);
        jiraLicenseManager.unSubscribeFromClearCache(countConsumer);
        jiraLicenseManager.unSubscribeFromClearCache(countConsumer);
        jiraLicenseManager.onCacheClear(ClearCacheEvent.INSTANCE);
        assertThat(consumerCallCount[0], is(3));
    }

    @Test
    public void testThatConsumerOnlyCalledOnceWhenRegisteredMultipleTimes() {
        final int[] consumerCallCount = {3};
        final Consumer<Void> countConsumer = Void -> consumerCallCount[0]++;
        jiraLicenseManager.subscribeToClearCache(countConsumer);
        jiraLicenseManager.subscribeToClearCache(countConsumer);
        jiraLicenseManager.onCacheClear(ClearCacheEvent.INSTANCE);
        assertThat(consumerCallCount[0], is(4));
    }

    @Test
    public void clearCacheClearsTheCache() {
        assertClearsCache(jiraLicenseManager::clearCache);
    }

    private TestJiraLicenseManagerImpl extendedCurrentLicense() {
        applicationProperties.setOption(APKeys.JIRA_CONFIRMED_INSTALL_WITH_OLD_LICENSE, true);
        return this;
    }

    private void assertSetLicenseClearsCache(Function<String, LicenseDetails> save) {
        //given
        final ApplicationKey software = valueOf("one");
        final ApplicationKey core = valueOf("two");
        final String softwareSen = "abc";
        final String coreSen = "xyz";
        final MockLicenseDetails softwareDetails = licenseDetailsFactory
                .addLicense("software")
                .setSupportEntitlementNumber(softwareSen)
                .setLicensedApplications(software);
        final MockLicenseDetails coreDetails = licenseDetailsFactory
                .addLicense("core")
                .setSupportEntitlementNumber(coreSen)
                .setLicensedApplications(core);
        multiLicenseStore.storeDetails(softwareDetails);

        assertThat(jiraLicenseManager.getLicenses(), contains(is(license(softwareDetails))));
        assertThat(jiraLicenseManager.getSupportEntitlementNumbers(), contains(softwareSen));
        assertThat(jiraLicenseManager.getLicense(software), some(license(softwareDetails)));
        assertThat(jiraLicenseManager.getLicense(core), none());
        assertThat(jiraLicenseManager.getAllLicensedApplicationKeys(), contains(software));

        //when
        final LicenseDetails licenseDetails = save.apply(coreDetails.getLicenseString());

        //then
        assertThat(licenseDetails, is(license(coreDetails)));
        assertThat(jiraLicenseManager.getLicenses(), containsInAnyOrder(license(softwareDetails), license(coreDetails)));
        assertThat(jiraLicenseManager.getSupportEntitlementNumbers(), contains(softwareSen, coreSen));
        assertThat(jiraLicenseManager.getLicense(software), some(license(softwareDetails)));
        assertThat(jiraLicenseManager.getLicense(core), some(license(coreDetails)));
        assertThat(jiraLicenseManager.getAllLicensedApplicationKeys(), containsInAnyOrder(software, core));
    }

    private void assertClearsCache(Runnable clear) {
        //given
        final ApplicationKey software = valueOf("one");
        final ApplicationKey core = valueOf("two");
        final String softwareSen = "abc";
        final MockLicenseDetails softwareDetails = licenseDetailsFactory
                .addLicense("software")
                .setSupportEntitlementNumber(softwareSen)
                .setLicensedApplications(software);
        multiLicenseStore.storeDetails(softwareDetails);

        assertThat(jiraLicenseManager.getLicenses(), contains(is(license(softwareDetails))));
        assertThat(jiraLicenseManager.getSupportEntitlementNumbers(), contains(softwareSen));
        assertThat(jiraLicenseManager.getLicense(software), some(license(softwareDetails)));
        assertThat(jiraLicenseManager.getLicense(core), none());
        assertThat(jiraLicenseManager.getAllLicensedApplicationKeys(), contains(software));

        verify(licenseDetailsFactory, times(1)).getLicense(softwareDetails.getLicenseString());
        verify(multiLicenseStore, times(1)).retrieve();

        //when
        clear.run();

        //then
        assertThat(jiraLicenseManager.getLicenses(), contains(is(license(softwareDetails))));
        assertThat(jiraLicenseManager.getSupportEntitlementNumbers(), contains(softwareSen));
        assertThat(jiraLicenseManager.getLicense(software), some(license(softwareDetails)));
        assertThat(jiraLicenseManager.getLicense(core), none());
        assertThat(jiraLicenseManager.getAllLicensedApplicationKeys(), contains(software));

        verify(licenseDetailsFactory, times(2)).getLicense(softwareDetails.getLicenseString());
        verify(multiLicenseStore, times(2)).retrieve();
    }

    private void assertRemoveLicenseClearsCache(Function<LicenseDetails, Void> remove) {
        //given
        final ApplicationKey software = valueOf("one");
        final ApplicationKey core = valueOf("two");
        final String softwareSen = "abc";
        final String coreSen = "xyz";
        final MockLicenseDetails softwareDetails = licenseDetailsFactory
                .addLicense("software")
                .setSupportEntitlementNumber(softwareSen)
                .setLicensedApplications(software);
        final MockLicenseDetails coreDetails = licenseDetailsFactory
                .addLicense("core")
                .setSupportEntitlementNumber(coreSen)
                .setLicensedApplications(core);
        multiLicenseStore.storeDetails(softwareDetails, coreDetails);

        assertThat(jiraLicenseManager.getLicenses(), containsInAnyOrder(license(softwareDetails), license(coreDetails)));
        assertThat(jiraLicenseManager.getSupportEntitlementNumbers(), contains(softwareSen, coreSen));
        assertThat(jiraLicenseManager.getLicense(software), some(license(softwareDetails)));
        assertThat(jiraLicenseManager.getLicense(core), some(license(coreDetails)));
        assertThat(jiraLicenseManager.getAllLicensedApplicationKeys(), containsInAnyOrder(software, core));

        //when
        remove.apply(coreDetails);

        //then
        assertThat(jiraLicenseManager.getLicenses(), contains(license(softwareDetails)));
        assertThat(jiraLicenseManager.getSupportEntitlementNumbers(), contains(softwareSen));
        assertThat(jiraLicenseManager.getLicense(software), some(license(softwareDetails)));
        assertThat(jiraLicenseManager.getLicense(core), none());
        assertThat(jiraLicenseManager.getAllLicensedApplicationKeys(), contains(software));
    }

    private void assertClearAndSetLicenseClearsCache(Function<String, LicenseDetails> save) {
        //given
        final ApplicationKey software = valueOf("one");
        final ApplicationKey core = valueOf("two");
        final String softwareSen = "abc";
        final String coreSen = "xyz";
        final MockLicenseDetails softwareDetails = licenseDetailsFactory
                .addLicense("software")
                .setSupportEntitlementNumber(softwareSen)
                .setLicensedApplications(software);
        final MockLicenseDetails coreDetails = licenseDetailsFactory
                .addLicense("core")
                .setSupportEntitlementNumber(coreSen)
                .setLicensedApplications(core);
        multiLicenseStore.storeDetails(softwareDetails);

        assertThat(jiraLicenseManager.getLicenses(), contains(is(license(softwareDetails))));
        assertThat(jiraLicenseManager.getSupportEntitlementNumbers(), contains(softwareSen));
        assertThat(jiraLicenseManager.getLicense(software), some(license(softwareDetails)));
        assertThat(jiraLicenseManager.getLicense(core), none());
        assertThat(jiraLicenseManager.getAllLicensedApplicationKeys(), contains(software));

        //when
        final LicenseDetails licenseDetails = save.apply(coreDetails.getLicenseString());

        //then
        assertThat(licenseDetails, is(license(coreDetails)));
        assertThat(jiraLicenseManager.getLicenses(), contains(license(coreDetails)));
        assertThat(jiraLicenseManager.getSupportEntitlementNumbers(), contains(coreSen));
        assertThat(jiraLicenseManager.getLicense(software), none());
        assertThat(jiraLicenseManager.getLicense(core), some(license(coreDetails)));
        assertThat(jiraLicenseManager.getAllLicensedApplicationKeys(), containsInAnyOrder(core));
    }

    private static LicenseDetailsMatcher license(String license) {
        return new LicenseDetailsMatcher(license);
    }

    private static LicenseDetailsMatcher license(LicenseDetails license) {
        return license(license.getLicenseString());
    }

    private static LicenseChangedEventMatcher isChangedEvent() {
        return new LicenseChangedEventMatcher();
    }

    private static class LicenseDetailsMatcher extends TypeSafeDiagnosingMatcher<LicenseDetails> {
        private final String license;

        private LicenseDetailsMatcher(final String license) {
            this.license = license;
        }

        @Override
        protected boolean matchesSafely(final LicenseDetails item, final Description mismatchDescription) {
            if (Objects.equals(item.getLicenseString(), license)) {
                return true;
            } else {
                mismatchDescription.appendText(String.format("License(%s)", item.getLicenseString()));
                return false;
            }
        }

        @Override
        public void describeTo(final Description description) {
            description.appendText(String.format("License(%s)", license));
        }
    }

    private static class LicenseChangedEventMatcher extends TypeSafeDiagnosingMatcher<LicenseChangedEvent> {
        private Option<String> oldLicense = Option.none();
        private Option<String> newLicense = Option.none();

        private LicenseChangedEventMatcher() {
        }

        @Override
        protected boolean matchesSafely(final LicenseChangedEvent item, final Description mismatchDescription) {
            final Option<String> actualOld = item.getPreviousLicenseDetails().map(LicenseDetails::getLicenseString);
            final Option<String> actualNew = item.getNewLicenseDetails().map(LicenseDetails::getLicenseString);
            if (Objects.equals(oldLicense, actualOld) && Objects.equals(newLicense, actualNew)) {
                return true;
            } else {
                mismatchDescription.appendText(String.format("LicenseChangedEvent(old: %s, new: %s)",
                        actualOld.getOrNull(), actualNew.getOrNull()));
                return false;
            }
        }

        private LicenseChangedEventMatcher withOldLicense(String oldLicense) {
            this.oldLicense = Option.some(oldLicense);
            return this;
        }

        private LicenseChangedEventMatcher withOldLicense(LicenseDetails oldLicense) {
            return withOldLicense(oldLicense.getLicenseString());
        }

        private LicenseChangedEventMatcher withNewLicense(String newLicense) {
            this.newLicense = Option.some(newLicense);
            return this;
        }

        @Override
        public void describeTo(final Description description) {
            description.appendText(String.format("LicenseChangedEvent(old: %s, new: %s)",
                    oldLicense.getOrNull(), newLicense.getOrNull()));
        }
    }
}
