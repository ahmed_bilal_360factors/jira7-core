package com.atlassian.jira.config;

import com.atlassian.cache.memory.MemoryCacheManager;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.database.MockDbConnectionManager;
import com.atlassian.jira.database.ResultRow;
import com.atlassian.jira.exception.StoreException;
import com.atlassian.jira.issue.IssueConstant;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.issue.issuetype.IssueTypeImpl;
import com.atlassian.jira.issue.priority.PriorityImpl;
import com.atlassian.jira.issue.resolution.Resolution;
import com.atlassian.jira.issue.resolution.ResolutionImpl;
import com.atlassian.jira.issue.status.ClearStatusCacheEvent;
import com.atlassian.jira.issue.status.Status;
import com.atlassian.jira.issue.status.StatusImpl;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoContainer;
import com.atlassian.jira.mock.ofbiz.MockGenericValue;
import com.atlassian.jira.mock.ofbiz.MockOfBizDelegator;
import com.atlassian.jira.ofbiz.FieldMap;
import com.google.common.base.Function;
import com.google.common.base.Supplier;
import com.google.common.collect.ImmutableList;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.ofbiz.core.entity.GenericEntityException;
import org.ofbiz.core.entity.GenericValue;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;

@SuppressWarnings("deprecation")
public class TestDefaultConstantsManager {
    private ConstantsManager defaultConstantsManager;

    @AvailableInContainer
    private MockOfBizDelegator mockOfBizDelegator = new MockOfBizDelegator();

    @Mock
    @AvailableInContainer
    private SubTaskManager subTaskManager;

    MockIssueConstantFactory issueConstantFactory = new MockIssueConstantFactory(mockOfBizDelegator);

    @Mock
    private EventPublisher eventPublisher;
    @Rule
    public MockitoContainer mockitoContainer = new MockitoContainer(this);

    private MockDbConnectionManager dbConnectionManager = new MockDbConnectionManager();

    @Before
    public void setUp() throws Exception {
        defaultConstantsManager = new DefaultConstantsManager(null, mockOfBizDelegator, dbConnectionManager,
                issueConstantFactory, new MemoryCacheManager(), eventPublisher);
    }

    protected GenericValue testGetGenericOperations(final String entityName, String sql, Supplier<Collection<? extends IssueConstant>> entitySupplier, Runnable entityRefresher, final Function<GenericValue, IssueConstant> constantCreator) {
        return testGetGenericOperations(entityName, sql, entitySupplier, entityRefresher, constantCreator, null);
    }

    protected GenericValue testGetGenericOperations(String entityName, String sql, Supplier<Collection<? extends IssueConstant>> entitySupplier, Runnable entityRefresher, final Function<GenericValue, IssueConstant> constantCreator, String style) {
        FieldMap fields1 = FieldMap.build("id", "1", "sequence", 1L, "name", "High");
        FieldMap fields2 = FieldMap.build("id", "2", "sequence", 2L, "name", "Low");
        FieldMap fields3 = FieldMap.build("id", "3", "sequence", 3L, "name", "Another");
        if ("IssueType".equals(entityName)) {
            fields1.add("style", style);
            fields2.add("style", style);
            fields3.add("style", style);
        }
        fields1.add("description", null);
        fields2.add("description", null);
        fields3.add("description", null);
        fields1.add("iconurl", null);
        fields2.add("iconurl", null);
        fields3.add("iconurl", null);
        if ("IssueType".equals(entityName)) {
            fields1.add("avatar", null);
            fields2.add("avatar", null);
            fields3.add("avatar", null);
        }
        if ("Priority".equals(entityName)) {
            fields1.add("statusColor", null);
            fields2.add("statusColor", null);
            fields3.add("statusColor", null);
        }

        dbConnectionManager.setQueryResults(sql, toQueryResult(fields1, fields2));

        GenericValue value1 = new MockGenericValue(entityName, fields1);
        GenericValue value2 = new MockGenericValue(entityName, fields2);
        IssueConstant constant1 = constantCreator.apply(value1);
        IssueConstant constant2 = constantCreator.apply(value2);

        final Collection<? extends IssueConstant> constants = entitySupplier.get();

        assertThat("Manager should contain all registered values (cache is empty)", constants, containsInAnyOrder(constant1, constant2));

        GenericValue value3 = new MockGenericValue(entityName, fields3);
        IssueConstant constant3 = constantCreator.apply(value3);
        dbConnectionManager.setQueryResults(sql, toQueryResult(fields1, fields2, fields3));
        assertThat("Manager should contain only cached values", entitySupplier.get(), containsInAnyOrder(constant1, constant2));

        entityRefresher.run();

        assertThat("Manager should contain all registered values (cache is refreshed)", entitySupplier.get(), containsInAnyOrder(constant1, constant2, constant3));

        return value1;
    }

    private Iterable<ResultRow> toQueryResult(final Map... fieldMaps) {
        final ImmutableList.Builder<ResultRow> results = ImmutableList.builder();
        for (Map fieldMap : fieldMaps) {
            List orderedValues = new ArrayList();
            // We extract the values from the field map in the correct order, but only if the key is present.
            addIfPresent(fieldMap, orderedValues, "id");
            addIfPresent(fieldMap, orderedValues, "sequence");
            addIfPresent(fieldMap, orderedValues, "name");
            addIfPresent(fieldMap, orderedValues, "style");
            addIfPresent(fieldMap, orderedValues, "description");
            addIfPresent(fieldMap, orderedValues, "iconurl");
            addIfPresent(fieldMap, orderedValues, "avatar");
            addIfPresent(fieldMap, orderedValues, "statusColor");
            results.add(new ResultRow(orderedValues.toArray()));
        }
        return results.build();
    }

    private void addIfPresent(final Map fieldMap, final List orderedValues, final String key) {
        if (fieldMap.containsKey(key)) {
            orderedValues.add(fieldMap.get(key));
        }
    }

    @Test
    public void testGetPriorities() {
        String sql = "select PRIORITY.id, PRIORITY.sequence, PRIORITY.pname, PRIORITY.description, PRIORITY.iconurl, PRIORITY.status_color\n"
                + "from priority PRIORITY\n"
                + "order by PRIORITY.sequence asc";

        final Function<GenericValue, IssueConstant> priorityCreator = issueConstantFactory::createPriority;
        final Supplier<Collection<? extends IssueConstant>> prioritiesSupplier = defaultConstantsManager::getPriorities;
        final Runnable prioritiesRefresher = defaultConstantsManager::refreshPriorities;

        GenericValue priority1 = testGetGenericOperations("Priority", sql, prioritiesSupplier, prioritiesRefresher, priorityCreator);

        assertEquals(new PriorityImpl(priority1, null, null, null), defaultConstantsManager.getPriorityObject("1"));
    }

    @Test
    public void testGetResolutions() {
        String sql = "select RESOLUTION.id, RESOLUTION.sequence, RESOLUTION.pname, RESOLUTION.description, RESOLUTION.iconurl\n"
                + "from resolution RESOLUTION\n"
                + "order by RESOLUTION.sequence asc";

        final Function<GenericValue, IssueConstant> resolutionCreator = issueConstantFactory::createResolution;
        final Supplier<Collection<? extends IssueConstant>> resolutionsSupplier = defaultConstantsManager::getResolutions;
        final Runnable prioritiesRefresher = defaultConstantsManager::refreshResolutions;
        GenericValue resolution1 = testGetGenericOperations("Resolution", sql, resolutionsSupplier, prioritiesRefresher, resolutionCreator);

        assertEquals(new ResolutionImpl(resolution1, null, null, null), defaultConstantsManager.getResolutionObject("1"));
    }

    @Test
    public void testGetIssueTypes() {
        String sql = "select ISSUE_TYPE.id, ISSUE_TYPE.sequence, ISSUE_TYPE.pname, ISSUE_TYPE.pstyle, ISSUE_TYPE.description, ISSUE_TYPE.iconurl, ISSUE_TYPE.avatar\n"
                + "from issuetype ISSUE_TYPE\n"
                + "order by ISSUE_TYPE.pstyle asc, ISSUE_TYPE.sequence asc";

        final Function<GenericValue, IssueConstant> issuetypeCreator = issueConstantFactory::createIssueType;
        final Supplier<Collection<? extends IssueConstant>> issueTypesSupplier = defaultConstantsManager::getRegularIssueTypeObjects;
        final Runnable issueTypesRefresher = defaultConstantsManager::refreshIssueTypes;

        GenericValue issueType1 = testGetGenericOperations("IssueType", sql, issueTypesSupplier, issueTypesRefresher, issuetypeCreator, null);

        assertThat(defaultConstantsManager.getSubTaskIssueTypeObjects(), Matchers.<IssueType>empty());

        assertEquals(new IssueTypeImpl(issueType1, null, null, null, null), defaultConstantsManager.getIssueTypeObject("1"));
    }


    @Test
    public void testGetConstantsByNameIgnoresCase() {
        String sql = "select RESOLUTION.id, RESOLUTION.sequence, RESOLUTION.pname, RESOLUTION.description, RESOLUTION.iconurl\n"
                + "from resolution RESOLUTION\n"
                + "order by RESOLUTION.sequence asc";

        FieldMap fieldMap = FieldMap.build("id", "1", "sequence", 1L, "name", "High", "description", null, "iconurl", null);
        GenericValue gv = new MockGenericValue("Resolution", fieldMap);
        Resolution resolution = new ResolutionImpl(gv, null, null, null);

        dbConnectionManager.setQueryResults(sql, toQueryResult(fieldMap));

        assertEquals(resolution, defaultConstantsManager.getConstantByNameIgnoreCase("Resolution", "hiGH"));
    }

    @Test
    public void testGetStatusByName() {
        String sql = "select STATUS.id, STATUS.sequence, STATUS.pname, STATUS.description, STATUS.iconurl, STATUS.statuscategory\n"
                + "from issuestatus STATUS\n"
                + "order by STATUS.sequence asc";

        FieldMap fieldMap = FieldMap.build("id", "7", "sequence", 1L, "name", "TestStatus_1", "description", null, "iconurl", null, "statuscategory", null);
        GenericValue gv1 = new MockGenericValue("Status", fieldMap);
        Status status1 = new StatusImpl(gv1, null, null, null, null);

        dbConnectionManager.setQueryResults(sql, toQueryResult(fieldMap));

        Status testStatus1 = defaultConstantsManager.getStatusByName("TestStatus_1");
        assertThat(testStatus1, is(status1));
    }

    @Test
    public void refreshStatusesPublishesEvent() {
        defaultConstantsManager.refreshStatuses();
        verify(eventPublisher).publish(eq(ClearStatusCacheEvent.INSTANCE));
    }

    @Test
    public void testGetStatusByNameIgnoreCase() {
        String sql = "select STATUS.id, STATUS.sequence, STATUS.pname, STATUS.description, STATUS.iconurl, STATUS.statuscategory\n"
                + "from issuestatus STATUS\n"
                + "order by STATUS.sequence asc";
        FieldMap fieldMap = FieldMap.build("id", "7", "sequence", 1L, "name", "TestStatus_1", "statuscategory", null, "description", null, "iconurl", null);
        GenericValue gv1 = new MockGenericValue("Status", fieldMap);
        Status status1 = new StatusImpl(gv1, null, null, null, null);
        dbConnectionManager.setQueryResults(sql, toQueryResult(fieldMap));

        assertThat(defaultConstantsManager.getStatusByNameIgnoreCase("TestStatus_1"), is(status1));
        assertThat(defaultConstantsManager.getStatusByNameIgnoreCase("tEststatus_1"), is(status1));
    }

    @Test
    public void testStoreIssueTypes() throws StoreException {
        FieldMap fieldMap1 = FieldMap.build("id", "1", "sequence", 1L, "name", "High", "style", "dummyIssueType");
        GenericValue val1 = new MockGenericValue("IssueType", fieldMap1);
        FieldMap fieldMap2 = FieldMap.build("id", "2", "sequence", 2L, "name", "Medium", "style", "dummyIssueType");
        GenericValue val2 = new MockGenericValue("IssueType", fieldMap2);
        FieldMap fieldMap3 = FieldMap.build("id", "3", "sequence", 3L, "name", "Low", "style", "dummyIssueType");
        GenericValue val3 = new MockGenericValue("IssueType", fieldMap3);

        final List<GenericValue> list = ImmutableList.of(val1, val2, val3);

        final String name = "Very High";
        final String name2 = "Very Medium";
        final String name3 = "Very Low";
        val1.set("name", name);
        val2.set("name", name2);
        val3.set("name", name3);

        defaultConstantsManager.storeIssueTypes(list);

        String sql = "select ISSUE_TYPE.id, ISSUE_TYPE.sequence, ISSUE_TYPE.pname, ISSUE_TYPE.pstyle, ISSUE_TYPE.description, ISSUE_TYPE.iconurl, ISSUE_TYPE.avatar\n"
                + "from issuetype ISSUE_TYPE\n"
                + "order by ISSUE_TYPE.pstyle asc, ISSUE_TYPE.sequence asc";

        fieldMap1.put("name", name);
        fieldMap2.put("name", name2);
        fieldMap3.put("name", name3);
        dbConnectionManager.setQueryResults(sql, toQueryResult(fieldMap1, fieldMap2, fieldMap3));

        final Collection<IssueType> issueTypes = defaultConstantsManager.getRegularIssueTypeObjects();
        assertEquals(3, issueTypes.size());
        final Iterator<IssueType> iterator = issueTypes.iterator();
        IssueType subTaskIssueType = iterator.next();
        assertEquals(name, subTaskIssueType.getName());
        subTaskIssueType = iterator.next();
        assertEquals(name2, subTaskIssueType.getName());
        subTaskIssueType = iterator.next();
        assertEquals(name3, subTaskIssueType.getName());
    }

    @Test
    public void testUpdateIssueTypeNullId() throws StoreException, GenericEntityException {
        try {
            defaultConstantsManager.updateIssueType(null, null, null, null, null, (String) null);
            fail("IllegalArgumentException should have been thrown.");
        } catch (IllegalArgumentException e) {
            assertEquals("Id cannot be null.", e.getMessage());
        }
    }

    @Test
    public void testUpdateIssueTypeIssueTypeDoesNotExist() throws StoreException, GenericEntityException {
        final String updateSQL = "update issuetype\n"
                + "set pname = NULL, sequence = NULL, pstyle = NULL, description = NULL, iconurl = NULL\n"
                + "where issuetype.id = '1'";
        dbConnectionManager.setUpdateResults(updateSQL, 0);

        String id = "1";
        try {
            defaultConstantsManager.updateIssueType(id, null, null, null, null, (String) null);
            fail("IllegalArgumentException should have been thrown.");
        } catch (IllegalArgumentException e) {
            assertEquals("Issue Type with id '" + id + "' does not exist.", e.getMessage());
        }
    }

    @Test
    public void testUpdateIssueType() throws StoreException, GenericEntityException {
        String id = "1";
        String name = "issue type name";
        Long sequence = 1L;
        String description = "issue type description";
        String iconurl = "issue type icon url";
        String style = "issue type style";

        final String updateSQL = "update issuetype\n"
                + "set pname = 'issue type name', sequence = 1, pstyle = 'issue type style', description = 'issue type description', iconurl = 'issue type icon url'\n"
                + "where issuetype.id = '1'";
        dbConnectionManager.setUpdateResults(updateSQL, 1);

        // update the issue via the ConstantsManager
        defaultConstantsManager.updateIssueType(id, name, sequence, style, description, iconurl);

    }

    @Test
    public void shouldUpdateIssueTypeSetAvatarId() throws StoreException, GenericEntityException {
        String id = "1";
        String name = "issue type name";
        Long sequence = 1L;
        String description = "issue type description";
        Long avatarId = 123l;
        String style = "issue type style";

        final String updateSQL = "update issuetype\n"
                + "set pname = 'issue type name', sequence = 1, pstyle = 'issue type style', description = 'issue type description', avatar = 123\n"
                + "where issuetype.id = '1'";
        dbConnectionManager.setUpdateResults(updateSQL, 1);

        // update the issue via the ConstantsManager
        defaultConstantsManager.updateIssueType(id, name, sequence, style, description, avatarId);
    }

    @Test
    public void testGetSubTaskIssueTypes() {
        final Supplier<Collection<? extends IssueConstant>> issueTypesSupplier = defaultConstantsManager::getSubTaskIssueTypeObjects;
        final Runnable issueTypesRefresher = defaultConstantsManager::refreshIssueTypes;
        String sql = "select ISSUE_TYPE.id, ISSUE_TYPE.sequence, ISSUE_TYPE.pname, ISSUE_TYPE.pstyle, ISSUE_TYPE.description, ISSUE_TYPE.iconurl, ISSUE_TYPE.avatar\n"
                + "from issuetype ISSUE_TYPE\n"
                + "order by ISSUE_TYPE.pstyle asc, ISSUE_TYPE.sequence asc";

        final Function<GenericValue, IssueConstant> issueTypeCreator = issueConstantFactory::createIssueType;

        GenericValue issueType1 = testGetGenericOperations("IssueType", sql, issueTypesSupplier, issueTypesRefresher, issueTypeCreator, "jira_subtask");

        assertThat(defaultConstantsManager.getRegularIssueTypeObjects(), Matchers.<IssueType>empty());

        assertEquals(new IssueTypeImpl(issueType1, null, null, null, null), defaultConstantsManager.getIssueTypeObject("1"));
    }

    @Test
    public void testGetMixedSubtaskAndRegularIssueTypes() {
        FieldMap fieldMap1 = FieldMap.build("id", "1", "sequence", 1L, "name", "bug", "style", null, "iconurl", null, "description", null, "avatar", null);
        GenericValue val1 = new MockGenericValue("IssueType", fieldMap1);
        final IssueType issueType1 = issueConstantFactory.createIssueType(val1);
        FieldMap fieldMap2 = FieldMap.build("id", "2", "sequence", 2L, "name", "Improvement", "style", null, "iconurl", null, "description", null, "avatar", null);
        GenericValue val2 = new MockGenericValue("IssueType", fieldMap2);
        final IssueType issueType2 = issueConstantFactory.createIssueType(val2);
        FieldMap fieldMap3 = FieldMap.build("id", "3", "sequence", 3L, "name", "Subtask", "style", "jira_subtask", "iconurl", null, "description", null, "avatar", null);
        GenericValue val3 = new MockGenericValue("IssueType", fieldMap3);
        final IssueType issueType3 = issueConstantFactory.createIssueType(val3);

        String sql = "select ISSUE_TYPE.id, ISSUE_TYPE.sequence, ISSUE_TYPE.pname, ISSUE_TYPE.pstyle, ISSUE_TYPE.description, ISSUE_TYPE.iconurl, ISSUE_TYPE.avatar\n"
                + "from issuetype ISSUE_TYPE\n"
                + "order by ISSUE_TYPE.pstyle asc, ISSUE_TYPE.sequence asc";

        dbConnectionManager.setQueryResults(sql, toQueryResult(fieldMap1, fieldMap2, fieldMap3));

        assertThat("Manager should contain all registered values (cache is refreshed)", (defaultConstantsManager.getRegularIssueTypeObjects()),
                containsInAnyOrder(issueType1, issueType2));
        assertThat("Manager should contain all registered values (cache is refreshed)", defaultConstantsManager.getSubTaskIssueTypeObjects(),
                containsInAnyOrder(issueType3));
    }

    @Test
    public void testExpandIssueTypeIds() {
        FieldMap fieldMap1 = FieldMap.build("id", "11", "sequence", 1L, "name", "A", "style", null);
        GenericValue val1 = new MockGenericValue("IssueType", fieldMap1);
        final IssueType issueType1 = issueConstantFactory.createIssueType(val1);
        FieldMap fieldMap2 = FieldMap.build("id", "12", "sequence", 2L, "name", "Subtask1", "style", "jira_subtask");
        GenericValue val2 = new MockGenericValue("IssueType", fieldMap2);
        final IssueType issueType2 = issueConstantFactory.createIssueType(val2);
        FieldMap fieldMap3 = FieldMap.build("id", "13", "sequence", 3L, "name", "Subtask2", "style", "jira_subtask");
        GenericValue val3 = new MockGenericValue("IssueType", fieldMap3);
        final IssueType issueType3 = issueConstantFactory.createIssueType(val3);
        FieldMap fieldMap4 = FieldMap.build("id", "14", "sequence", 4L, "name", "Bug", "style", null);
        GenericValue val4 = new MockGenericValue("IssueType", fieldMap4);
        final IssueType issueType4 = issueConstantFactory.createIssueType(val4);

        String sql = "select ISSUE_TYPE.id, ISSUE_TYPE.sequence, ISSUE_TYPE.pname, ISSUE_TYPE.pstyle, ISSUE_TYPE.description, ISSUE_TYPE.iconurl, ISSUE_TYPE.avatar\n"
                + "from issuetype ISSUE_TYPE\n"
                + "order by ISSUE_TYPE.pstyle asc, ISSUE_TYPE.sequence asc";

        dbConnectionManager.setQueryResults(sql, toQueryResult(fieldMap1, fieldMap2, fieldMap3, fieldMap4));

        List<String> expectedIds = ImmutableList.of("1", "2", "3");
        assertEquals(expectedIds, defaultConstantsManager.expandIssueTypeIds(expectedIds));

        final List<String> randomIdsAndStandard = ImmutableList.of("1", "2", ConstantsManager.ALL_STANDARD_ISSUE_TYPES, "4");
        assertThat(defaultConstantsManager.expandIssueTypeIds(randomIdsAndStandard), containsInAnyOrder("11", "14"));

        final List<String> randomIdsAndSubtask = ImmutableList.of("34", "12", ConstantsManager.ALL_SUB_TASK_ISSUE_TYPES, "41");
        assertThat(defaultConstantsManager.expandIssueTypeIds(randomIdsAndSubtask), containsInAnyOrder("12", "13"));

        final List<String> randomIdsAndAll = ImmutableList.of("34", "12", ConstantsManager.ALL_ISSUE_TYPES, "41");
        assertThat(defaultConstantsManager.expandIssueTypeIds(randomIdsAndAll), containsInAnyOrder("11", "12", "13", "14"));

    }

}
