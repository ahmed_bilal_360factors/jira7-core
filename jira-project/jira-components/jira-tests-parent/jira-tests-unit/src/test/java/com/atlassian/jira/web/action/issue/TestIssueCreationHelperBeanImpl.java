package com.atlassian.jira.web.action.issue;

import com.atlassian.jira.bc.MockJiraServiceContext;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueFieldConstants;
import com.atlassian.jira.issue.IssueInputParameters;
import com.atlassian.jira.issue.IssueInputParametersImpl;
import com.atlassian.jira.issue.customfields.OperationContextImpl;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem;
import com.atlassian.jira.issue.fields.screen.FieldScreenRenderer;
import com.atlassian.jira.issue.fields.screen.FieldScreenRendererFactory;
import com.atlassian.jira.license.CreateIssueLicenseCheck;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.jira.web.bean.MockI18nBean;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Maps;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.HashMap;
import java.util.List;

import static com.atlassian.jira.license.LicenseCheck.PASS;
import static org.junit.Assert.assertFalse;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Matchers.anyMap;
import static org.mockito.Matchers.anyMapOf;
import static org.mockito.Mockito.RETURNS_DEEP_STUBS;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Tests {@link com.atlassian.jira.web.action.issue.IssueCreationHelperBeanImpl}.
 */
@RunWith(MockitoJUnitRunner.class)
public class TestIssueCreationHelperBeanImpl {
    @Mock
    private FieldManager fieldManager;
    @Mock
    private FieldScreenRendererFactory fieldScreenRendererFactory;
    @Mock
    CreateIssueLicenseCheck licenseCheck;
    @Mock
    JiraAuthenticationContext authenticationContext;
    MockI18nBean i18n = new MockI18nBean();

    private IssueCreationHelperBeanImpl helperBean;

    @Before
    public void setUp() throws Exception {
        helperBean = new IssueCreationHelperBeanImpl(fieldManager, fieldScreenRendererFactory, licenseCheck, authenticationContext);
    }

    @Test
    public void testValidateLicenseHappy() {
        when(licenseCheck.evaluateWithUser(null)).thenReturn(PASS);
        SimpleErrorCollection errorCollection = new SimpleErrorCollection();

        helperBean.validateLicense(errorCollection, i18n);
        assertFalse(errorCollection.hasAnyErrors());
    }

    @Test
    public void shouldPopulateFromDefaultsWhenSkippingScreenCheckIsDisabled() {
        //prepare fields
        FieldScreenRenderer fsr = mock(FieldScreenRenderer.class, RETURNS_DEEP_STUBS);
        FieldLayoutItem fli1 = createFieldLayoutItem(fsr, "field1", false);
        FieldLayoutItem fli2 = createFieldLayoutItem(fsr, "field2", false);
        when(fsr.getFieldLayout().getVisibleLayoutItems(any(Project.class), anyListOf(String.class))).thenReturn(ImmutableList.of(fli1, fli2));

        //prepare test issue
        Issue issue = mock(Issue.class, RETURNS_DEEP_STUBS);

        //prepare input params
        IssueInputParameters issueInput = new IssueInputParametersImpl();
        assertFalse("skipScreenCheck should be false by default", issueInput.skipScreenCheck());
        final HashMap<String, Object> fieldValuesHolder = Maps.newHashMap();

        helperBean.validateCreateIssueFields(new MockJiraServiceContext("user"), ImmutableList.of("field1", "field2"), issue, fsr, new OperationContextImpl(null, fieldValuesHolder), issueInput, i18n);

        verify(fli1.getOrderableField()).populateDefaults(fieldValuesHolder, issue);
        verify(fli2.getOrderableField()).populateDefaults(fieldValuesHolder, issue);
        verify(fli1.getOrderableField(), never()).populateFromParams(anyMapOf(String.class, Object.class), anyMapOf(String.class, String[].class));
        verify(fli2.getOrderableField(), never()).populateFromParams(anyMapOf(String.class, Object.class), anyMapOf(String.class, String[].class));
    }


    @Test
    public void shouldPopulateFromParamsWhenSkippigIsEnabled() {
        //prepare fields
        FieldScreenRenderer fsr = mock(FieldScreenRenderer.class, RETURNS_DEEP_STUBS);
        FieldLayoutItem fli1 = createFieldLayoutItem(fsr, "field1", false);
        FieldLayoutItem fli2 = createFieldLayoutItem(fsr, "field2", false);
        when(fsr.getFieldLayout().getVisibleLayoutItems(any(Project.class), anyListOf(String.class))).thenReturn(ImmutableList.of(fli1, fli2));

        Issue issue = mock(Issue.class, RETURNS_DEEP_STUBS);

        //prepare input params
        IssueInputParameters issueInput = new IssueInputParametersImpl();
        issueInput.setSkipScreenCheck(true);
        final HashMap<String, Object> fieldValuesHolder = Maps.newHashMap();

        helperBean.validateCreateIssueFields(new MockJiraServiceContext("user"), ImmutableList.of("field1", "field2"), issue, fsr, new OperationContextImpl(null, fieldValuesHolder), issueInput, i18n);

        verify(fli1.getOrderableField(), never()).populateDefaults(anyMapOf(String.class, Object.class), any(Issue.class));
        verify(fli2.getOrderableField(), never()).populateDefaults(anyMapOf(String.class, Object.class), any(Issue.class));
        verify(fli1.getOrderableField()).populateFromParams(fieldValuesHolder, issueInput.getActionParameters());
        verify(fli2.getOrderableField()).populateFromParams(fieldValuesHolder, issueInput.getActionParameters());
    }

    @Test
    public void createIssueDoesNotPopulateResolutionAndResolutionDateWithDefaultValues() {
        ImmutableList<String> fieldConstants = ImmutableList.of(
                IssueFieldConstants.DUE_DATE,
                IssueFieldConstants.RESOLUTION,
                IssueFieldConstants.RESOLUTION_DATE
        );

        Issue issue = mock(Issue.class);

        final FieldScreenRenderer fieldScreenRenderer = mock(FieldScreenRenderer.class, RETURNS_DEEP_STUBS);

        FieldLayoutItem dueDateItem = createFieldLayoutItem(fieldScreenRenderer, IssueFieldConstants.DUE_DATE, false);
        FieldLayoutItem resolutionItem = createFieldLayoutItem(fieldScreenRenderer, IssueFieldConstants.RESOLUTION, false);
        FieldLayoutItem resolutionDateItem = createFieldLayoutItem(fieldScreenRenderer, IssueFieldConstants.RESOLUTION_DATE, false);

        List < FieldLayoutItem > fieldLayoutItems = ImmutableList.of(dueDateItem, resolutionItem, resolutionDateItem);

        when(fieldScreenRenderer.getFieldLayout().getVisibleLayoutItems(any(Project.class), anyListOf(String.class))).thenReturn(fieldLayoutItems);

        final HashMap<String,Object> fieldValuesHolder = Maps.newHashMap();

        //prepare input params
        IssueInputParameters issueInput = new IssueInputParametersImpl();
        issueInput.setApplyDefaultValuesWhenParameterNotProvided(true);

        helperBean.validateCreateIssueFields(
                new MockJiraServiceContext("user"),
                fieldConstants,
                issue,
                fieldScreenRenderer,
                new OperationContextImpl(null, fieldValuesHolder),
                issueInput,
                i18n);

        verify(dueDateItem.getOrderableField(), atLeastOnce()).populateDefaults(anyMap(), any(Issue.class));
        verify(resolutionItem.getOrderableField(), never()).populateDefaults(anyMap(), any(Issue.class));
        verify(resolutionDateItem.getOrderableField(), never()).populateDefaults(anyMap(), any(Issue.class));
    }

    private static FieldLayoutItem createFieldLayoutItem(final FieldScreenRenderer fsr, final String fieldId, final boolean isVisible) {
        FieldLayoutItem fli1 = mock(FieldLayoutItem.class, RETURNS_DEEP_STUBS);
        when(fli1.getOrderableField().getId()).thenReturn(fieldId);
        when(fsr.getFieldScreenRenderLayoutItem(fli1.getOrderableField()).isShow(any(Issue.class))).thenReturn(isVisible);
        return fli1;
    }
}
