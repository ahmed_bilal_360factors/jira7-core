package com.atlassian.jira.plugin.webpanel.notification;

import com.atlassian.jira.application.ApplicationKeys;
import com.atlassian.jira.application.ApplicationRole;
import com.atlassian.jira.application.ApplicationRoleManager;
import com.atlassian.jira.application.MockApplicationRole;
import com.atlassian.jira.util.BaseUrl;
import com.google.common.collect.ImmutableSet;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Map;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserLimitExceededContextProviderTest {
    private static final ApplicationRole ROLE_SOFTWARE = new MockApplicationRole(ApplicationKeys.SOFTWARE);
    private static final ApplicationRole ROLE_SERVICE_DESK = new MockApplicationRole(ApplicationKeys.SERVICE_DESK);
    private static final String BASE_URL = "/base";

    @Mock
    private ApplicationRoleManager applicationManager;
    @Mock
    private BaseUrl baseUrl;

    private UserLimitExceededContextProvider contextProvider;


    @Before
    public void setup() {
        when(applicationManager.getRoles()).thenReturn(ImmutableSet.of(ROLE_SOFTWARE, ROLE_SERVICE_DESK));
        when(baseUrl.getBaseUrl()).thenReturn(BASE_URL);
        when(applicationManager.isAnyRoleLimitExceeded()).thenReturn(true);

        contextProvider = new UserLimitExceededContextProvider(applicationManager, baseUrl);
    }

    @Test
    public void justOneIsTrueIfOnlyOneAppRoleIsExceededAndAppRolesAreEnabled() {
        when(applicationManager.isRoleLimitExceeded(eq(ApplicationKeys.SOFTWARE))).thenReturn(true);

        final Map<String, Object> context = contextProvider.getContextMap(null);
        assertThat(context.get("justOne"), is(true));
        assertThat(context.get("baseUrl"), is("/base"));
    }

    @Test
    public void roleKeyAndRoleNamePresentIfOnlyOneAppRoleIsExceededAndAppRolesAreEnabled() {
        when(applicationManager.isRoleLimitExceeded(eq(ApplicationKeys.SOFTWARE))).thenReturn(true);

        final Map<String, Object> context = contextProvider.getContextMap(null);
        assertThat(context.get("roleName"), is(ROLE_SOFTWARE.getName()));
        assertThat(context.get("roleKey"), is(ApplicationKeys.SOFTWARE.value()));
    }

    @Test
    public void justOneIsFalseIfMoreThanOneAppRoleIsExceededAndAppRolesAreEnabled() {
        when(applicationManager.isRoleLimitExceeded(ApplicationKeys.SOFTWARE)).thenReturn(true);
        when(applicationManager.isRoleLimitExceeded(ApplicationKeys.SERVICE_DESK)).thenReturn(true);

        final Map<String, Object> context = contextProvider.getContextMap(null);
        assertThat(context.get("justOne"), is(false));
        assertThat(context.get("baseUrl"), is("/base"));
    }

    @Test(expected = IllegalStateException.class)
    public void throwsExceptionIfNoRoleIsExceeded() {
        when(applicationManager.isAnyRoleLimitExceeded()).thenReturn(false);

        contextProvider.getContextMap(null);
    }
}