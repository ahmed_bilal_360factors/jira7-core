package com.atlassian.jira.upgrade.tasks;

import com.atlassian.jira.config.LocaleManager;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.customfields.CustomFieldUtils;
import com.atlassian.jira.issue.fields.layout.column.ColumnLayoutManager;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutManager;
import com.atlassian.jira.issue.fields.screen.FieldScreenSchemeManager;
import com.atlassian.jira.issue.fields.screen.FieldScreenStore;
import com.atlassian.jira.issue.label.CachingLabelStore;
import com.atlassian.jira.issue.search.CachingSearchRequestStore;
import com.atlassian.jira.jql.parser.DefaultJqlQueryParser;
import com.atlassian.jira.jql.util.JqlStringSupportImpl;
import com.atlassian.jira.mock.ofbiz.MockGenericValue;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.ofbiz.OfBizListIterator;
import com.atlassian.jira.portal.CachingPortletConfigurationStore;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.collect.MapBuilder;
import com.atlassian.jira.web.bean.MockI18nBean;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnit;
import org.ofbiz.core.entity.EntityCondition;
import org.ofbiz.core.entity.EntityOperator;
import org.ofbiz.core.entity.GenericEntityException;
import org.ofbiz.core.entity.GenericModelException;
import org.ofbiz.core.entity.GenericValue;

import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

import static com.google.common.collect.ImmutableMap.of;
import static com.google.common.collect.Maps.newHashMap;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

public class TestUpgradeTask_Build552 {
    @Mock
    private OfBizDelegator mockOfBizDelegator;
    @Mock
    private LocaleManager mockLocaleManager;
    @Mock
    private I18nHelper.BeanFactory mockBeanFactory;
    @Mock
    private CustomFieldManager mockCustomFieldManager;
    @Mock
    private CachingLabelStore cachingLabelStore;
    @Mock
    private CachingSearchRequestStore cachingSearchRequestStore;
    @Mock
    private CachingPortletConfigurationStore cachingPortletConfigurationStore;

    @Rule
    public final MethodRule initMockito = MockitoJUnit.rule();

    @Test
    public void testDoUpgradeNoFields() throws Exception {
        final UpgradeTask_Build552 upgradeTask = new UpgradeTask_Build552(mockOfBizDelegator, null, null, null, null, null, null, null, null, null, null, null, null);
        upgradeTask.doUpgrade(false);

        verify(mockOfBizDelegator).findByAnd("CustomField",
                ImmutableMap.of("customfieldtypekey", "com.atlassian.jira.plugin.labels:labels"));
    }

    @Test
    public void testDoUpgrade() throws Exception {
        final AtomicBoolean migrateDataCalled = new AtomicBoolean(false);
        final AtomicBoolean updateSRCalled = new AtomicBoolean(false);
        final AtomicBoolean updateNavigatorColumns = new AtomicBoolean(false);
        final AtomicBoolean updateGadgets = new AtomicBoolean(false);
        final AtomicBoolean updateFieldScreens = new AtomicBoolean(false);

        final GenericValue customFieldGv1 = new MockGenericValue("CustomField", ImmutableMap.of("name", "Labels", "id", 12232L));
        final GenericValue customFieldGv2 = new MockGenericValue("CustomField", ImmutableMap.of("name", "Epic", "id", 10001L));
        when(mockOfBizDelegator.findByAnd("CustomField",
                ImmutableMap.of("customfieldtypekey", "com.atlassian.jira.plugin.labels:labels"))).
                thenReturn(ImmutableList.of(customFieldGv1, customFieldGv2));
        when(mockLocaleManager.getInstalledLocales()).thenReturn(ImmutableSet.of(Locale.ENGLISH));
        when(mockBeanFactory.getInstance(Locale.ENGLISH)).thenReturn(new MockI18nBean());

        final UpgradeTask_Build552 upgradeTask = new UpgradeTask_Build552(mockOfBizDelegator, mockLocaleManager, null, null, null, mockCustomFieldManager, mockBeanFactory, null, null, null, cachingLabelStore, cachingSearchRequestStore, cachingPortletConfigurationStore) {
            //these methods will be tested in isolation!

            @Override
            void migrateCustomFieldData(final List<Long> fieldsToConvertToSystem, final List<Long> customFieldIds) {
                migrateDataCalled.set(true);
                assertEquals(1, fieldsToConvertToSystem.size());
                assertTrue(fieldsToConvertToSystem.contains(12232L));
                assertEquals(2, customFieldIds.size());
                assertTrue(customFieldIds.contains(12232L));
                assertTrue(customFieldIds.contains(10001L));
            }

            @Override
            void updateSearchRequests() throws GenericEntityException {
                updateSRCalled.set(true);
            }

            @Override
            void updateIssueNavigatorColumns(final List<Long> fieldsToConvertToSystem) {
                updateNavigatorColumns.set(true);
                assertEquals(1, fieldsToConvertToSystem.size());
                assertTrue(fieldsToConvertToSystem.contains(12232L));
            }

            @Override
            void updateGadgetConfigurations(final List<Long> fieldsToConvertToSystem) throws GenericEntityException {
                updateGadgets.set(true);
                assertEquals(1, fieldsToConvertToSystem.size());
                assertTrue(fieldsToConvertToSystem.contains(12232L));
            }

            @Override
            void updateFieldScreensWithSystemField(final List<Long> fieldsToConvertToSystem) {
                updateFieldScreens.set(true);
                assertEquals(1, fieldsToConvertToSystem.size());
                assertTrue(fieldsToConvertToSystem.contains(12232L));
            }
        };
        upgradeTask.doUpgrade(false);

        assertTrue(migrateDataCalled.get());
        assertTrue(updateSRCalled.get());
        assertTrue(updateNavigatorColumns.get());
        assertTrue(updateGadgets.get());
        assertTrue(updateFieldScreens.get());

        verify(mockOfBizDelegator).bulkUpdateByAnd("CustomField",
                ImmutableMap.of
                        (
                                "customfieldtypekey", "com.atlassian.jira.plugin.system.customfieldtypes:labels",
                                "customfieldsearcherkey", "com.atlassian.jira.plugin.system.customfieldtypes:labelsearcher"
                        ),
                ImmutableMap.of("customfieldtypekey", "com.atlassian.jira.plugin.labels:labels"));
        verify(mockOfBizDelegator).removeByOr("CustomFieldValue", "customfield", ImmutableList.of(12232L, 10001L));

        verify(mockCustomFieldManager).removeCustomFieldPossiblyLeavingOrphanedData(12232L);
        verify(mockCustomFieldManager).refresh();
        verify(cachingLabelStore).onClearCache(null);
    }

    @Test
    public void testMigrateData() {
        final GenericValue customFieldGv1 = new MockGenericValue("CustomFieldValue",
                ImmutableMap.of("issue", 22001L, "customfield", 12232L, "textvalue", "this are some test labels"));
        final GenericValue customFieldGv2 = new MockGenericValue("CustomFieldValue",
                ImmutableMap.of("issue", 21003L, "customfield", 10001L, "textvalue", "heres another label"));
        final GenericValue customFieldGv3 = new MockGenericValue("CustomFieldValue",
                MapBuilder.build("issue", 21023L, "customfield", 10001L, "textvalue", null));

        final OfBizListIterator listIterator = mock(OfBizListIterator.class);
        when(listIterator.next()).thenReturn(customFieldGv1).thenReturn(customFieldGv2).thenReturn(customFieldGv3).thenReturn(null);

        when(mockOfBizDelegator.findListIteratorByCondition("CustomFieldValue", new MockEntityExpr("customfield", EntityOperator.IN,
                ImmutableList.of(12232L, 10001L)), null, ImmutableList.of("issue", "customfield", "textvalue"), null, null)).
                thenReturn(listIterator);


        final UpgradeTask_Build552 upgradeTask = new UpgradeTask_Build552(mockOfBizDelegator, null, null, null, null, null, null, null, null, null, null, null, null);
        upgradeTask.migrateCustomFieldData(ImmutableList.of(12232L), ImmutableList.of(12232L, 10001L));

        verify(listIterator).close();

        //these obviously wont return null, but for the purposes of this test we don't care what they return.
        InOrder inOrder = Mockito.inOrder(mockOfBizDelegator);
        inOrder.verify(mockOfBizDelegator).createValue("Label", ImmutableMap.<String, Object>of("issue", 22001L, "label", "this"));
        inOrder.verify(mockOfBizDelegator).createValue("Label", ImmutableMap.<String, Object>of("issue", 22001L, "label", "are"));
        inOrder.verify(mockOfBizDelegator).createValue("Label", ImmutableMap.<String, Object>of("issue", 22001L, "label", "some"));
        inOrder.verify(mockOfBizDelegator).createValue("Label", ImmutableMap.<String, Object>of("issue", 22001L, "label", "test"));
        inOrder.verify(mockOfBizDelegator).createValue("Label", ImmutableMap.<String, Object>of("issue", 22001L, "label", "labels"));
        inOrder.verify(mockOfBizDelegator).createValue("Label", ImmutableMap.<String, Object>of("issue", 21003L, "fieldid", 10001L, "label", "heres"));
        inOrder.verify(mockOfBizDelegator).createValue("Label", ImmutableMap.<String, Object>of("issue", 21003L, "fieldid", 10001L, "label", "another"));
        inOrder.verify(mockOfBizDelegator).createValue("Label", ImmutableMap.<String, Object>of("issue", 21003L, "fieldid", 10001L, "label", "label"));
    }

    @Test
    public void testUpdateSearchRequests() throws GenericEntityException {
        final GenericValue srGv1 = new MockGenericValue("SearchRequest", ImmutableMap.of("id", 33001L));
        final GenericValue srGv2 = new MockGenericValue("SearchRequest", ImmutableMap.of("id", 33003L));
        final GenericValue srGv3 = new MockGenericValue("SearchRequest", ImmutableMap.of("id", 33023L));
        final GenericValue srGvNull = new MockGenericValue("SearchRequest", ImmutableMap.of("id", 33033L));
        final GenericValue fullSrGv1 = new MyMockGenericValue("SearchRequest", ImmutableMap.of("id", 33001L, "request", ""));
        final GenericValue fullSrGv2 = new MyMockGenericValue("SearchRequest",
                ImmutableMap.of("id", 33003L, "request", "project = MKY and cf[10003] in (poo, bear)"));
        final GenericValue fullSrGv3 = new MyMockGenericValue("SearchRequest",
                ImmutableMap.of("id", 33023L, "request", "Epic != donkeys"));
        final GenericValue fullSrGvNull = new MyMockGenericValue("SearchRequest",
                MapBuilder.build("id", 33033L, "request", null));
        when(mockOfBizDelegator.findByCondition("SearchRequest", null, ImmutableList.of("id"))).thenReturn(ImmutableList.of(srGv1, srGv2, srGv3, srGvNull));

        when(mockOfBizDelegator.findByCondition("SearchRequest", new MockEntityExpr("id", EntityOperator.IN, ImmutableList.of(33001L, 33003L, 33023L, 33033L)), null)).
                thenReturn(ImmutableList.of(fullSrGv1, fullSrGv2, fullSrGv3, fullSrGvNull));

        final DefaultJqlQueryParser jqlQueryParser = new DefaultJqlQueryParser();
        final UpgradeTask_Build552 upgradeTask = new UpgradeTask_Build552(mockOfBizDelegator, null, null,
                jqlQueryParser, new JqlStringSupportImpl(jqlQueryParser), null, null, null, null, null, null, cachingSearchRequestStore, null) {
            @Override
            Map<String, String> getSubstitutions(final List<GenericValue> fieldsToConvertToSystemGvs) {
                return ImmutableMap.of("cf[10003]", "labels", "Epic", "labels");
            }
        };

        upgradeTask.updateSearchRequests();
        assertFalse(((MyMockGenericValue) fullSrGv1).isStored());
        assertTrue(((MyMockGenericValue) fullSrGv2).isStored());
        assertEquals("project = MKY AND labels in (poo, bear)", ((MyMockGenericValue) fullSrGv2).getNewJql());
        assertTrue(((MyMockGenericValue) fullSrGv3).isStored());
        assertEquals("labels != donkeys", ((MyMockGenericValue) fullSrGv3).getNewJql());
        assertFalse(((MyMockGenericValue) fullSrGvNull).isStored());

        verify(cachingSearchRequestStore).onClearCache(null);
    }

    @Test
    public void testUpdateGadgetConfigurations() throws GenericEntityException {
        final GenericValue pcGv1 = new MyMockGenericValue("PortletConfiguration", ImmutableMap.of("id", 12023L));
        final GenericValue pcGv2 = new MyMockGenericValue("PortletConfiguration", ImmutableMap.of("id", 12027L));

        when(mockOfBizDelegator.findByLike("PortletConfiguration",
                ImmutableMap.of("gadgetXml", "rest/gadgets/1.0/g/com.atlassian.jira.plugin.labels:labels-gadget/templates/plugins/labels/gadget/labels-gadget.xml"))).
                thenReturn(ImmutableList.of(pcGv1, pcGv2));

        final EntityCondition entityCondition = new MockEntityConditionList(ImmutableList.of(new MockEntityExpr("portletconfiguration", EntityOperator.IN, ImmutableList.of(12023L, 12027L)),
                new MockEntityExpr("userprefkey", EntityOperator.EQUALS, "fieldId")), EntityOperator.AND);

        final GenericValue userPrefGv1 = new MyMockGenericValue("GadgetUserPreference", newHashMap(of("userprefvalue", "customfield_10002")));
        final GenericValue userPrefGv2 = new MyMockGenericValue("GadgetUserPreference", ImmutableMap.of("userprefvalue", "someotherfield"));
        final GenericValue userPrefGv3 = new MyMockGenericValue("GadgetUserPreference", ImmutableMap.of("userprefvalue", "customfield_10202"));

        when(mockOfBizDelegator.findByCondition("GadgetUserPreference", entityCondition, null)).thenReturn(ImmutableList.of(userPrefGv1, userPrefGv2, userPrefGv3));


        final UpgradeTask_Build552 upgradeTask = new UpgradeTask_Build552(mockOfBizDelegator, null, null, null, null, null, null, null, null, null, null, null, cachingPortletConfigurationStore);

        upgradeTask.updateGadgetConfigurations(ImmutableList.of(10002L));

        assertTrue(((MyMockGenericValue) userPrefGv1).isStored());
        assertEquals("labels", ((MyMockGenericValue) userPrefGv1).getNewUserPrefValue());
        assertFalse(((MyMockGenericValue) userPrefGv2).isStored());
        assertFalse(((MyMockGenericValue) userPrefGv3).isStored());

        verify(cachingPortletConfigurationStore).onClearCache(null);

        verify(mockOfBizDelegator).bulkUpdateByPrimaryKey("PortletConfiguration",
                ImmutableMap.of("gadgetXml", "rest/gadgets/1.0/g/com.atlassian.jira.gadgets:labels-gadget/gadgets/labels-gadget.xml"),
                ImmutableList.of(12023L, 12027L));

    }

    @Test
    public void testUpdateGadgetConfigurationsEmpty() throws GenericEntityException {
        final UpgradeTask_Build552 upgradeTask = new UpgradeTask_Build552(mockOfBizDelegator, null, null, null, null, null, null, null, null, null, null, null, null);
        upgradeTask.updateGadgetConfigurations(ImmutableList.of(10002L));

        verify(mockOfBizDelegator).findByLike("PortletConfiguration",
                ImmutableMap.of("gadgetXml", "rest/gadgets/1.0/g/com.atlassian.jira.plugin.labels:labels-gadget/templates/plugins/labels/gadget/labels-gadget.xml"));
    }

    @Test
    public void testUpgradeIssueNavigatorColumns() throws Exception {
        final OfBizListIterator mockIterator = mock(OfBizListIterator.class);
        when(mockOfBizDelegator.findListIteratorByCondition("ColumnLayoutItem", new MockEntityExpr("fieldidentifier", EntityOperator.IN, ImmutableList.of(
                "customfield_10020", "customfield_10030")), null, ImmutableList.of("id", "columnlayout"), ImmutableList.of("columnlayout ASC",
                "horizontalposition ASC"), null)).thenReturn(mockIterator);

        // first columnlayoutitem is an instance of a single custom field in a column layout
        // this columnlayoutitem will be simply changed to use the system field
        final MockGenericValue resLayoutItem1 = new MockGenericValue("ColumnLayoutItem", ImmutableMap.of("id", 100L, "columnlayout", 2000L));

        // next 3 columnlayoutitems are an instance of 3 separate custom fields (all resolution date) in the same column layout
        // the first of these will be simply changed to use the system field
        // the other 2 will be removed
        final MockGenericValue resLayoutItem2 = new MockGenericValue("ColumnLayoutItem", ImmutableMap.of("id", 101L, "columnlayout", 2010L));
        final MockGenericValue resLayoutItem3 = new MockGenericValue("ColumnLayoutItem", ImmutableMap.of("id", 102L, "columnlayout", 2010L));
        final MockGenericValue resLayoutItem4 = new MockGenericValue("ColumnLayoutItem", ImmutableMap.of("id", 103L, "columnlayout", 2010L));

        when(mockIterator.next())
                .thenReturn(resLayoutItem1)
                .thenReturn(resLayoutItem2)
                .thenReturn(resLayoutItem3)
                .thenReturn(resLayoutItem4)
                .thenReturn(null);


        final ColumnLayoutManager columnLayoutManager = mock(ColumnLayoutManager.class);

        final UpgradeTask_Build552 upgradeTask = new UpgradeTask_Build552(mockOfBizDelegator, null, columnLayoutManager, null, null, null, null, null, null, null, null, null, null);
        upgradeTask.updateIssueNavigatorColumns(ImmutableList.of(10020L, 10030L));

        verify(mockIterator).close();
        verify(columnLayoutManager).refresh();

        verify(mockOfBizDelegator).removeByOr("ColumnLayoutItem", "id", ImmutableList.of(102L, 103L));
        verify(mockOfBizDelegator).bulkUpdateByPrimaryKey("ColumnLayoutItem", ImmutableMap.of("fieldidentifier", "labels"), ImmutableList.of(100L, 101L));

    }

    @Test
    public void testUpgradeIssueNavigatorColumnsWithNoLayoutsToUpdate() throws Exception {

        final OfBizListIterator mockIterator = mock(OfBizListIterator.class);
        when(mockOfBizDelegator.findListIteratorByCondition("ColumnLayoutItem", new MockEntityExpr("fieldidentifier", EntityOperator.IN, ImmutableList.of(
                "customfield_10020", "customfield_10030")), null, ImmutableList.of("id", "columnlayout"), ImmutableList.of("columnlayout ASC",
                "horizontalposition ASC"), null)).thenReturn(mockIterator);

        when(mockIterator.next()).thenReturn(null);

        final ColumnLayoutManager columnLayoutManager = mock(ColumnLayoutManager.class);
        columnLayoutManager.refresh();

        final UpgradeTask_Build552 upgradeTask = new UpgradeTask_Build552(mockOfBizDelegator, null, columnLayoutManager, null, null, null, null, null, null, null, null, null, null);
        upgradeTask.updateIssueNavigatorColumns(ImmutableList.of(10020L, 10030L));

        verify(mockIterator).close();
        verify(mockOfBizDelegator).findListIteratorByCondition("ColumnLayoutItem", new MockEntityExpr("fieldidentifier", EntityOperator.IN, ImmutableList.of(
                "customfield_10020", "customfield_10030")), null, ImmutableList.of("id", "columnlayout"), ImmutableList.of("columnlayout ASC",
                "horizontalposition ASC"), null);
        verifyNoMoreInteractions(mockOfBizDelegator);
    }

    @Test
    public void testGetSubstitutions() {
        final UpgradeTask_Build552 upgradeTask = new UpgradeTask_Build552(mockOfBizDelegator, null, null, null, null, null, null, null, null, null, null, null, null);
        final GenericValue cfGv1 = new MockGenericValue("CustomField", ImmutableMap.of("id", 10023L, "name", "fooBar"));
        final GenericValue cfGv2 = new MockGenericValue("CustomField", ImmutableMap.of("id", 10423L, "name", "Epic"));
        final Map<String, String> substitutions = upgradeTask.getSubstitutions(ImmutableList.of(cfGv1, cfGv2));
        assertEquals(4, substitutions.size());
        assertEquals("labels", substitutions.get("cf[10023]"));
        assertEquals("labels", substitutions.get("fooBar"));
        assertEquals("labels", substitutions.get("cf[10423]"));
        assertEquals("labels", substitutions.get("Epic"));
    }

    @Test
    public void testUpdateFieldScreensWithSystemField() throws GenericModelException {
        final FieldScreenSchemeManager mockFieldScreenSchemeManager = mock(FieldScreenSchemeManager.class);
        final FieldLayoutManager mockFieldLayoutManager = mock(FieldLayoutManager.class);
        final FieldScreenStore mockFieldScreenStore = mock(FieldScreenStore.class);

        final GenericValue cfGv1 = new MockGenericValue("FieldLayoutItem", ImmutableMap.of("id", 10023L, "fieldlayout", 10000L));
        final GenericValue cfGv2 = new MockGenericValue("FieldLayoutItem", ImmutableMap.of("id", 10013L, "fieldlayout", 10000L));
        final GenericValue cfGv3 = new MockGenericValue("FieldLayoutItem", ImmutableMap.of("id", 10098L, "fieldlayout", 10232L));
        when(mockOfBizDelegator.findByAnd("FieldLayoutItem", ImmutableMap.of("fieldidentifier", "labels"))).thenReturn(ImmutableList.of(cfGv1, cfGv2, cfGv3));

        final GenericValue cfGv4 = new MockGenericValue("FieldScreenLayoutItem", ImmutableMap.of("id", 10043L, "fieldscreentab", 10000L));
        final GenericValue cfGv5 = new MockGenericValue("FieldScreenLayoutItem", ImmutableMap.of("id", 10053L, "fieldscreentab", 10000L));
        final GenericValue cfGv6 = new MockGenericValue("FieldScreenLayoutItem", ImmutableMap.of("id", 10198L, "fieldscreentab", 10000L));
        when(mockOfBizDelegator.findByAnd("FieldScreenLayoutItem", ImmutableMap.of("fieldidentifier", "labels"))).thenReturn(ImmutableList.of(cfGv4, cfGv5, cfGv6));

        final UpgradeTask_Build552 upgradeTask = new UpgradeTask_Build552(mockOfBizDelegator, null, null, null, null, null, null, mockFieldScreenSchemeManager, mockFieldLayoutManager, mockFieldScreenStore, null, null, null);
        upgradeTask.updateFieldScreensWithSystemField(ImmutableList.of(10020L, 10030L));

        verify(mockFieldScreenSchemeManager).refresh();
        verify(mockFieldLayoutManager).refresh();
        verify(mockFieldScreenStore).refresh();

        verify(mockOfBizDelegator).bulkUpdateByAnd("FieldLayoutItem",
                ImmutableMap.of("fieldidentifier", "labels"),
                ImmutableMap.of("fieldidentifier", CustomFieldUtils.CUSTOM_FIELD_PREFIX + 10020L));
        verify(mockOfBizDelegator).bulkUpdateByAnd("FieldScreenLayoutItem",
                ImmutableMap.of("fieldidentifier", "labels"),
                ImmutableMap.of("fieldidentifier", CustomFieldUtils.CUSTOM_FIELD_PREFIX + 10020L));

        verify(mockOfBizDelegator).bulkUpdateByAnd("FieldLayoutItem",
                ImmutableMap.of("fieldidentifier", "labels"),
                ImmutableMap.of("fieldidentifier", CustomFieldUtils.CUSTOM_FIELD_PREFIX + 10030L));
        verify(mockOfBizDelegator).bulkUpdateByAnd("FieldScreenLayoutItem",
                ImmutableMap.of("fieldidentifier", "labels"),
                ImmutableMap.of("fieldidentifier", CustomFieldUtils.CUSTOM_FIELD_PREFIX + 10030L));
        verify(mockOfBizDelegator).removeByOr("FieldLayoutItem", "id", ImmutableList.of(10013L));
        verify(mockOfBizDelegator).removeByOr("FieldScreenLayoutItem", "id", ImmutableList.of(10053L, 10198L));
    }

    @Test
    public void testMetaData() throws Exception {
        final UpgradeTask_Build552 upgradeTask = new UpgradeTask_Build552(null, null, null, null, null, null, null, null, null, null, null, null, null);
        assertEquals(552, upgradeTask.getBuildNumber());
        assertEquals("Converts label custom fields to the new label system field.", upgradeTask.getShortDescription());
    }

    static class MyMockGenericValue extends MockGenericValue {
        private boolean stored = false;
        private String newJql = null;
        private String newUserPrefValue = null;

        public MyMockGenericValue(final String entityName, final Map fields) {
            super(entityName, fields);
        }

        @Override
        public void setString(final String s, final String s1) {
            super.setString(s, s1);
            if (s.equals("request")) {
                newJql = s1;
            } else if (s.equals("userprefvalue")) {
                newUserPrefValue = s1;
            }
        }

        @Override
        public void store() throws GenericEntityException {
            stored = true;
        }

        public String getNewJql() {
            return newJql;
        }

        public String getNewUserPrefValue() {
            return newUserPrefValue;
        }

        @Override
        public boolean isStored() {
            return stored;
        }
    }
}
