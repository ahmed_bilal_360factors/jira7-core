package com.atlassian.jira.issue.fields.option;

import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.database.MockQueryDslAccessor;
import com.atlassian.jira.database.ResultRowBuilder;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.issue.issuetype.MockIssueType;
import com.atlassian.jira.junit.rules.MockitoContainer;
import com.google.common.collect.ImmutableList;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;

import java.util.Collection;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.BiFunction;
import java.util.stream.IntStream;

import static org.hamcrest.Matchers.contains;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

public class OptionSetManagerImplTest {
    @Rule
    public MockitoContainer mockitoContainer = new MockitoContainer(this);

    @Mock
    private ConstantsManager constantsManager;

    @Mock
    private FieldConfig config;

    private MockQueryDslAccessor queryDslAccessor;
    private OptionSetManagerImpl fixture;

    @Before
    public void setUp() {
        queryDslAccessor = new MockQueryDslAccessor();
        final AtomicLong counter = new AtomicLong(1000L);
        when(queryDslAccessor.getMockDelegatorInterface().getNextSeqId(anyString()))
                .thenAnswer(invocation -> counter.getAndIncrement());

        when(config.getId()).thenReturn(42L);
        when(config.getFieldId()).thenReturn("IssueType");

        IntStream.range(1, 8).forEach(this::expectIssueType);

        this.fixture = new OptionSetManagerImpl(constantsManager, queryDslAccessor);
    }

    @After
    public void tearDown() {
        queryDslAccessor.assertAllExpectedStatementsWereRun();

        constantsManager = null;
        queryDslAccessor = null;
        config = null;
        fixture = null;
    }

    @Test
    public void testGetOptionsForConfig() {
        // Issue Type 99 doesn't actually exist and should get filtered out of the result
        expectSelect("6", "2", "99", "3");

        final OptionSet result = fixture.getOptionsForConfig(config);
        assertThat(result.getOptionIds(), contains("6", "2", "3"));
    }

    @Test
    public void testCreateOptionSet() {
        createUpdate(fixture::createOptionSet);
    }

    @Test
    public void testUpdateOptionSet() {
        createUpdate(fixture::updateOptionSet);
    }

    // Create and update have identical semantics.  Don't blame me; I didn't write them!
    private void createUpdate(BiFunction<FieldConfig, Collection<String>, OptionSet> fn) {
        expectDelete();
        expectInsert(1000L, 0L, "4");
        expectInsert(1001L, 1L, "1");
        expectInsert(1002L, 2L, "3");
        expectSelect("6", "7", "5");

        final OptionSet result = fn.apply(config, ImmutableList.of("4", "1", "3"));
        assertThat(result.getOptionIds(), contains("6", "7", "5"));
    }

    @Test
    public void testRemoveOptionSet() {
        expectDelete();

        fixture.removeOptionSet(config);
    }

    private void expectInsert(Long id, long seq, String optionId) {
        queryDslAccessor.setUpdateResults("insert into optionconfiguration (fieldconfig, fieldid, sequence, optionid, id)\n" +
                "values (42, 'IssueType', " + seq + ", '" + optionId + "', " + id + ')', 1);
    }

    private void expectDelete() {
        queryDslAccessor.setUpdateResults("delete from optionconfiguration\n" +
                "where optionconfiguration.fieldid = 'IssueType' and optionconfiguration.fieldconfig = 42", 1);
    }

    private void expectSelect(String... optionIds) {
        final ResultRowBuilder rows = new ResultRowBuilder();
        for (String optionId : optionIds) {
            rows.addRow(optionId);
        }
        queryDslAccessor.setQueryResults("select OPTION_CONFIGURATION.optionid\n" +
                "from optionconfiguration OPTION_CONFIGURATION\n" +
                "where OPTION_CONFIGURATION.fieldid = 'IssueType' and OPTION_CONFIGURATION.fieldconfig = 42\n" +
                "order by OPTION_CONFIGURATION.sequence asc", rows);
    }

    private void expectIssueType(long id) {
        when(constantsManager.getConstantObject("IssueType", String.valueOf(id)))
                .thenReturn(new MockIssueType(id, "IssueType" + id));
    }
}