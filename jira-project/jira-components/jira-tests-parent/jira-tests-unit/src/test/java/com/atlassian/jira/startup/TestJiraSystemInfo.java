package com.atlassian.jira.startup;

import com.atlassian.jira.config.properties.JiraProperties;
import com.atlassian.jira.junit.rules.InitMockitoMocks;
import com.atlassian.jira.mock.servlet.MockServletContext;
import com.atlassian.jira.ofbiz.OfBizConnectionFactory;
import com.atlassian.jira.util.MockBuildUtilsInfo;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.hamcrest.TypeSafeDiagnosingMatcher;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;

import java.util.Set;
import java.util.regex.Pattern;

import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

public class TestJiraSystemInfo {
    private static final Set<String> PATH_RELATED_KEYS = ImmutableSet.of("sun.boot.class.path",
            "com.ibm.oti.vm.bootstrap.library.path", "java.library.path",
            "java.endorsed.dirs", "java.ext.dirs", "java.class.path");

    private static final Set<String> IGNORE_THESE_KEYS = ImmutableSet.<String>builder()
            .addAll(PATH_RELATED_KEYS)
            .add("line.separator")
            .add("path.separator")
            .add("file.separator")
            .build();

    @Rule
    public InitMockitoMocks mocks = new InitMockitoMocks(this);

    private MockBuildUtilsInfo buildInfo = new MockBuildUtilsInfo();
    private FormattedLogMsg logMsg = new FormattedLogMsg();

    @Mock
    private OfBizConnectionFactory connectionFactory;

    @Mock
    private JiraProperties jiraProperties;

    private JiraSystemInfo info;

    @Before
    public void setup() {
        info = new JiraSystemInfo(logMsg, buildInfo, connectionFactory, jiraProperties);
    }

    @Test
    public void testObtainBasicInfo() throws Exception {
        MockServletContext context = new MockServletContext();
        info.obtainBasicInfo(context);

        String logText = logMsg.toString();

        assertTrue(logText.contains("JIRA Build"));
        assertTrue(logText.contains("Build Date"));
        assertTrue(logText.contains("JIRA Installation Type"));
        assertTrue(logText.contains("Application Server"));
        assertTrue(logText.contains("Java Version"));
        assertTrue(logText.contains("Current Working Director"));
        assertTrue(logText.contains("Maximum Allowable Memory"));
        assertTrue(logText.contains("Total Memory"));
        assertTrue(logText.contains("Free Memory"));
        assertTrue(logText.contains("Used Memory"));
        // We don't assert that Memory pools exist because some JVMs may not expose this data
        assertTrue(logText.contains("JVM Input Arguments"));
        // We don't assert that Applied Patches exist because some JVMs may not expose this data
    }

    @Test
    public void testObtainSystemPropertiesRendersValues() throws Exception {
        //given
        when(jiraProperties.getSanitisedProperties()).thenReturn(ImmutableMap.of("one", "oneValue", "two", "twoValue"));

        //when
        info.obtainSystemProperties();
        String logText = logMsg.toString();

        //then
        assertThat(logText, Matchers.containsString("Java System Properties"));
        assertThat(logText, containsPattern(valuePattern("one", "oneValue")));
        assertThat(logText, containsPattern(valuePattern("two", "twoValue")));
    }

    @Test
    public void testObtainSystemPropertiesIgnoresPathsAndUninterestingProperties() throws Exception {
        //given
        final String badValue = "<BAD>";
        ImmutableMap.Builder<String, String> builder = ImmutableMap.builder();
        for (String key : IGNORE_THESE_KEYS) {
            builder.put(key, badValue);
        }
        builder.put("one", "oneValue");
        builder.put("two", "twoValue");

        when(jiraProperties.getSanitisedProperties()).thenReturn(builder.build());

        //when
        info.obtainSystemProperties();
        String logText = logMsg.toString();

        //then
        assertThat(logText, Matchers.containsString("Java System Properties"));
        assertThat(logText, containsPattern(valuePattern("one", "oneValue")));
        assertThat(logText, containsPattern(valuePattern("two", "twoValue")));
        assertThat(logText, Matchers.not(Matchers.containsString(badValue)));
    }

    @Test
    public void testObtainSystemPropertiesOnlyShowsPathProperties() throws Exception {
        //given
        final String badValue = "<BAD>";
        ImmutableMap.Builder<String, String> builder = ImmutableMap.builder();
        for (String key : PATH_RELATED_KEYS) {
            builder.put(key, key + "Value");
        }
        builder.put("one", badValue);
        builder.put("two", badValue);

        when(jiraProperties.getSanitisedProperties()).thenReturn(builder.build());

        //when
        info.obtainSystemPathProperties();
        String logText = logMsg.toString();

        //then
        assertThat(logText, Matchers.containsString("Java Class Paths"));
        for (String key : PATH_RELATED_KEYS) {
            assertThat(logText, containsPattern(valuePattern(key, key + "Value")));
        }
        assertThat(logText, Matchers.not(Matchers.containsString(badValue)));
    }

    private static Pattern valuePattern(String key, String value) {
        // Matches output of the form "key        : value"
        return Pattern.compile(Pattern.quote(key) + "\\s*:\\s*" + Pattern.quote(value));
    }

    private static Matcher<String> containsPattern(Pattern pattern) {
        return new TypeSafeDiagnosingMatcher<String>() {
            @Override
            protected boolean matchesSafely(final String s, final Description description) {
                if (pattern.matcher(s).find()) {
                    return true;
                } else {
                    description.appendText(s);
                    return false;
                }
            }

            @Override
            public void describeTo(final Description description) {
                description.appendText(String.format("Regex '%s' to be found.", pattern.toString()));
            }
        };
    }
}
