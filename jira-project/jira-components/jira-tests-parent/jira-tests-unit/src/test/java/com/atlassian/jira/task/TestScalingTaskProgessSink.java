package com.atlassian.jira.task;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.MethodRule;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnit;

public class TestScalingTaskProgessSink {
    @Rule
    public final ExpectedException exception = ExpectedException.none();
    @Rule
    public final MethodRule initMockito = MockitoJUnit.rule();

    private static final String SUB_TASK_A = "SubTaskA";
    private static final String MESSAGE_A = "MessageA";

    @Mock
    private TaskProgressSink mockSink;
    private InOrder inOrder;

    @Before
    public void createInProgressVerifier() {
        inOrder = Mockito.inOrder(mockSink);
    }

    @Test
    public void testScalingTaskProgessSinkConstructorNullSink() {
        exception.expect(IllegalArgumentException.class);
        new ScalingTaskProgessSink(10, 90, null);
    }

    @Test
    public void testScalingTaskProgessSinkConstructorActualStartGreaterThanEnd() {
        exception.expect(IllegalArgumentException.class);
        new ScalingTaskProgessSink(101, 90, TaskProgressSink.NULL_SINK);
    }

    @Test
    public void testScalingTaskProgessSinkConstructorVirtualStartGreaterThanEnd() {
        exception.expect(IllegalArgumentException.class);
        new ScalingTaskProgessSink(0, 100, 101, 100, TaskProgressSink.NULL_SINK);
    }

    @Test
    public void testMakeProgressSimplePercentage() {
        TaskProgressSink scaleSink = new ScalingTaskProgessSink(0, 10, mockSink);
        scaleSink.makeProgress(0, SUB_TASK_A, MESSAGE_A);
        scaleSink.makeProgress(50, SUB_TASK_A, null);
        scaleSink.makeProgress(100, null, null);
        scaleSink.makeProgress(-1, null, MESSAGE_A);
        scaleSink.makeProgress(20000, null, null);

        inOrder.verify(mockSink).makeProgress(0, SUB_TASK_A, MESSAGE_A);
        inOrder.verify(mockSink).makeProgress(5, SUB_TASK_A, null);
        inOrder.verify(mockSink).makeProgress(10, null, null);
        inOrder.verify(mockSink).makeProgress(0, null, MESSAGE_A);
        inOrder.verify(mockSink).makeProgress(10, null, null);
    }

    @Test
    public void testMakeProgress() {
        TaskProgressSink scaleSink = new ScalingTaskProgessSink(90, 100, mockSink);
        scaleSink.makeProgress(0, SUB_TASK_A, MESSAGE_A);
        scaleSink.makeProgress(50, SUB_TASK_A, null);
        scaleSink.makeProgress(100, null, null);

        inOrder.verify(mockSink).makeProgress(90, SUB_TASK_A, MESSAGE_A);
        inOrder.verify(mockSink).makeProgress(95, SUB_TASK_A, null);
        inOrder.verify(mockSink).makeProgress(100, null, null);
    }

    @Test
    public void testMakeProgressScaleDown() {
        TaskProgressSink scaleSink = new ScalingTaskProgessSink(27, 27, mockSink);
        scaleSink.makeProgress(0, SUB_TASK_A, MESSAGE_A);
        scaleSink.makeProgress(50, SUB_TASK_A, null);
        scaleSink.makeProgress(100, null, null);

        inOrder.verify(mockSink).makeProgress(27, SUB_TASK_A, MESSAGE_A);
        inOrder.verify(mockSink).makeProgress(27, SUB_TASK_A, null);
        inOrder.verify(mockSink).makeProgress(27, null, null);
    }

    @Test
    public void testMakeProgressNegativeValues() {
        TaskProgressSink scaleSink = new ScalingTaskProgessSink(-12, -8, mockSink);
        scaleSink.makeProgress(0, SUB_TASK_A, MESSAGE_A);
        scaleSink.makeProgress(25, SUB_TASK_A, null);
        scaleSink.makeProgress(50, null, null);

        inOrder.verify(mockSink).makeProgress(-12, SUB_TASK_A, MESSAGE_A);
        inOrder.verify(mockSink).makeProgress(-11, SUB_TASK_A, null);
        inOrder.verify(mockSink).makeProgress(-10, null, null);
    }

    @Test
    public void testMakeProgressScaleUp() {
        TaskProgressSink scaleSink = new ScalingTaskProgessSink(200, 400, 0, 10, mockSink);
        scaleSink.makeProgress(0, SUB_TASK_A, MESSAGE_A);
        scaleSink.makeProgress(5, SUB_TASK_A, null);
        scaleSink.makeProgress(10, null, null);

        inOrder.verify(mockSink).makeProgress(200, SUB_TASK_A, MESSAGE_A);
        inOrder.verify(mockSink).makeProgress(300, SUB_TASK_A, null);
        inOrder.verify(mockSink).makeProgress(400, null, null);
    }

    @Test
    public void testMakeProgressZeroRanges() {
        TaskProgressSink scaleSink = new ScalingTaskProgessSink(27, 27, 100, 100, mockSink);
        scaleSink.makeProgress(100, SUB_TASK_A, MESSAGE_A);
        scaleSink.makeProgress(100, SUB_TASK_A, null);
        scaleSink.makeProgress(100, null, null);

        inOrder.verify(mockSink).makeProgress(27, SUB_TASK_A, MESSAGE_A);
        inOrder.verify(mockSink).makeProgress(27, SUB_TASK_A, null);
        inOrder.verify(mockSink).makeProgress(27, null, null);
    }

    @Test
    public void testMakeProgressWorkWithNegative() {
        TaskProgressSink scaleSink = new ScalingTaskProgessSink(-20, -10, 0, 20, mockSink);
        scaleSink.makeProgress(0, SUB_TASK_A, MESSAGE_A);
        scaleSink.makeProgress(10, SUB_TASK_A, null);
        scaleSink.makeProgress(18, null, null);
        scaleSink.makeProgress(Long.MIN_VALUE, SUB_TASK_A, null);
        scaleSink.makeProgress(Long.MAX_VALUE, null, MESSAGE_A);

        inOrder.verify(mockSink).makeProgress(-20, SUB_TASK_A, MESSAGE_A);
        inOrder.verify(mockSink).makeProgress(-15, SUB_TASK_A, null);
        inOrder.verify(mockSink).makeProgress(-11, null, null);
        inOrder.verify(mockSink).makeProgress(-20, SUB_TASK_A, null);
        inOrder.verify(mockSink).makeProgress(-10, null, MESSAGE_A);
    }
}
