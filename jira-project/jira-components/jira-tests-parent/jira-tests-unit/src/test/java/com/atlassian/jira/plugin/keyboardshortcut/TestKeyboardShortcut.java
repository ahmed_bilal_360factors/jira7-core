package com.atlassian.jira.plugin.keyboardshortcut;

import com.atlassian.jira.util.I18nHelper;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import static com.atlassian.jira.plugin.keyboardshortcut.KeyboardShortcutManager.Context;
import static com.atlassian.jira.plugin.keyboardshortcut.KeyboardShortcutManager.Operation;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

public class TestKeyboardShortcut {
    @Rule
    public MockitoRule mockito = MockitoJUnit.rule();

    @Mock
    private I18nHelper mockI18nHelper;

    @Test
    public void testPrettyShortcutOneLetter() {
        final Set<List<String>> keys = new LinkedHashSet<List<String>>();
        keys.add(Arrays.asList("g"));
        KeyboardShortcut shortcut = new KeyboardShortcut("", Context.global, Operation.click, "", 10, keys, null, false);
        String prettyString = shortcut.getPrettyShortcut(mockI18nHelper);
        assertEquals("<kbd>g</kbd>", prettyString);
    }

    @Test
    public void testPrettyShortcutOneLetterThreeKeys() {
        when(mockI18nHelper.getText("common.words.or")).thenReturn("or");

        final Set<List<String>> keys = new LinkedHashSet<List<String>>();
        keys.add(Arrays.asList("g"));
        keys.add(Arrays.asList("h"));
        keys.add(Arrays.asList("d"));
        KeyboardShortcut shortcut = new KeyboardShortcut("", Context.global, Operation.click, "", 10, keys, null, false);
        String prettyString = shortcut.getPrettyShortcut(mockI18nHelper);
        assertEquals("<kbd>g</kbd> or <kbd>h</kbd> or <kbd>d</kbd>", prettyString);
    }

    @Test
    public void testPrettyShortcutTwoLettersTwoKeys() {
        when(mockI18nHelper.getText("keyboard.shortcuts.two.keys", "<kbd>g</kbd>", "<kbd>h</kbd>")).thenReturn("<kbd>g</kbd> then <kbd>h</kbd>");
        when(mockI18nHelper.getText("common.words.or")).thenReturn("or");
        when(mockI18nHelper.getText("keyboard.shortcuts.two.keys", "<kbd>g</kbd>", "<kbd>d</kbd>")).thenReturn("<kbd>g</kbd> then <kbd>d</kbd>");

        final Set<List<String>> keys = new LinkedHashSet<List<String>>();
        keys.add(Arrays.asList("g", "h"));
        keys.add(Arrays.asList("g", "d"));
        KeyboardShortcut shortcut = new KeyboardShortcut("", Context.global, Operation.click, "", 10, keys, null, false);
        String prettyString = shortcut.getPrettyShortcut(mockI18nHelper);
        assertEquals("<kbd>g</kbd> then <kbd>h</kbd> or <kbd>g</kbd> then <kbd>d</kbd>", prettyString);
    }

    @Test
    public void testPrettyShortcutThreeLetters() {
        when(mockI18nHelper.getText("keyboard.shortcuts.three.keys", "<kbd>g</kbd>", "<kbd>h</kbd>", "<kbd>c</kbd>")).
                thenReturn("<kbd>g</kbd> then <kbd>h</kbd> then <kbd>c</kbd>");

        final Set<List<String>> keys = new LinkedHashSet<List<String>>();
        keys.add(Arrays.asList("g", "h", "c"));
        KeyboardShortcut shortcut = new KeyboardShortcut("", Context.global, Operation.click, "", 10, keys, null, false);
        String prettyString = shortcut.getPrettyShortcut(mockI18nHelper);
        assertEquals("<kbd>g</kbd> then <kbd>h</kbd> then <kbd>c</kbd>", prettyString);
    }

}
