package com.atlassian.jira.upgrade;

import com.atlassian.beehive.ClusterLock;
import com.atlassian.beehive.ClusterLockService;
import com.atlassian.jira.index.request.ReindexRequestType;
import com.atlassian.upgrade.api.UpgradeContext;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import java.util.Set;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

public class TestClusterLockingUpgradeService {

    private static final String UPGRADE_FRAMEWORK_LOCKED_ERROR = "Could not acquire the lock for running upgrades";

    @Rule
    public MethodRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private ClusterLockService clusterLockService;

    @Mock
    private ClusterLock clusterLock;

    @Mock
    private LicenseCheckingUpgradeService licenseCheckingUpgradeService;

    @Mock
    private Set<ReindexRequestType> reindexRequestTypes;

    @Mock
    private UpgradeContext upgradeContext;

    @Captor
    private ArgumentCaptor<Set<ReindexRequestType>> requestTypesArgumentCaptor;

    private ClusterLockingUpgradeService clusterLockingUpgradeService;

    @Before
    public void setUp() {
        clusterLockingUpgradeService = new ClusterLockingUpgradeService(
                clusterLockService,
                licenseCheckingUpgradeService
        );

        when(clusterLockService.getLockForName(any())).thenReturn(clusterLock);
        when(clusterLock.tryLock()).thenReturn(true);
    }

    @Test
    public void shouldExecuteUpgrades() {
        final UpgradeResult expectedUpgradeResult = new UpgradeResult();
        when(licenseCheckingUpgradeService.runUpgrades(eq(reindexRequestTypes), any(UpgradeContext.class))).thenReturn(expectedUpgradeResult);

        final UpgradeResult upgradeResult = clusterLockingUpgradeService.runUpgrades(
                reindexRequestTypes,
                upgradeContext
        );

        assertThat(upgradeResult.successful(), equalTo(true));
        assertThat(upgradeResult, equalTo(expectedUpgradeResult));
    }

    @Test
    public void shouldFailIfAlreadyLocked() {
        when(clusterLock.tryLock()).thenReturn(false);

        final UpgradeResult upgradeResult = clusterLockingUpgradeService.runUpgrades(
                reindexRequestTypes,
                upgradeContext
        );

        assertThat(upgradeResult.successful(), equalTo(false));
        assertThat(upgradeResult.getErrors(), contains(UPGRADE_FRAMEWORK_LOCKED_ERROR));
    }
}