package com.atlassian.jira.startup;

import com.atlassian.jira.matchers.johnson.JohnsonEventMatchers;
import com.atlassian.jira.mock.servlet.MockServletContext;
import com.atlassian.jira.studio.startup.MockStartupHooks;
import com.atlassian.jira.studio.startup.StudioStartupHooks;
import com.atlassian.jira.util.johnson.JohnsonProvider;
import com.atlassian.johnson.DefaultJohnsonEventContainer;
import com.atlassian.johnson.Johnson;
import com.atlassian.johnson.JohnsonEventContainer;
import com.atlassian.johnson.event.Event;
import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import javax.servlet.ServletContextEvent;

import static com.atlassian.jira.matchers.IterableMatchers.hasItemsThat;
import static com.atlassian.jira.matchers.IterableMatchers.iterableWithSize;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Test case for {@link com.atlassian.jira.startup.LauncherContextListener}.
 *
 * @since v5.1.6
 */
public class TestLauncherContextListener {
    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private MockServletContext context;
    @Mock
    private JohnsonProvider johnsonProvider;

    @Before
    public void before() {
        context = new MockServletContext();
        Johnson.initialize("test-johnson-config.xml");
    }

    @After
    public void resetStudioStartupHooks() {
        MockStartupHooks.resetHooks();
        Johnson.terminate();
    }

    @Test
    public void shouldNotEscapeRuntimeExceptionsOnStartup() {
        StudioStartupHooks hooks = mock(StudioStartupHooks.class);
        doThrow(new RuntimeException()).when(hooks).beforeJiraStart();
        MockStartupHooks.setHooks(hooks);
        DefaultJohnsonEventContainer eventContainer = new DefaultJohnsonEventContainer();
        when(johnsonProvider.getContainer()).thenReturn(eventContainer);

        LauncherContextListener launcherContextListener = new LauncherContextListener(johnsonProvider);
        launcherContextListener.contextInitialized(new ServletContextEvent(context));

        assertFatalJohnsonEvent(eventContainer);

    }

    @Test(expected = Error.class)
    public void shouldLetEscapeErrorOnStartup() {
        StudioStartupHooks hooks = mock(StudioStartupHooks.class);
        doThrow(new Error()).when(hooks).beforeJiraStart();
        MockStartupHooks.setHooks(hooks);
        LauncherContextListener launcherContextListener = new LauncherContextListener(johnsonProvider);
        launcherContextListener.contextInitialized(new ServletContextEvent(context));
    }

    private void assertFatalJohnsonEvent(JohnsonEventContainer eventContainer) {
        assertThat(getEvents(eventContainer), Matchers.allOf(
                iterableWithSize(1, Event.class),
                hasItemsThat(Event.class, Matchers.allOf(
                        JohnsonEventMatchers.hasKey("startup-unexpected"),
                        JohnsonEventMatchers.containsDescription("Unexpected exception during JIRA startup. "
                                + "This JIRA instance will not be able to recover."),
                        JohnsonEventMatchers.hasLevel("fatal")
                ))));
        assertTrue("hasEvents should be consistent", eventContainer.hasEvents());
    }

    @SuppressWarnings("unchecked")
    private Iterable<Event> getEvents(JohnsonEventContainer eventContainer) {
        return eventContainer.getEvents();
    }
}
