package com.atlassian.jira.upgrade.tasks;

import com.atlassian.core.ofbiz.test.UtilsForTests;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.mock.config.properties.MockPropertiesManager;
import com.atlassian.jira.mock.ofbiz.MockOfBizDelegator;
import com.atlassian.jira.ofbiz.FieldMap;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.module.propertyset.memory.MemoryPropertySet;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.ofbiz.core.entity.GenericValue;

import java.util.Collection;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.when;

public class TestUpgradeTask_Build605 {
    @Rule
    public MockitoContainer mockitoContainer = MockitoMocksInContainer.rule(this);

    @AvailableInContainer
    private OfBizDelegator ofBizDelegator = new MockOfBizDelegator();

    @Mock
    private MockPropertiesManager mockPropertiesManager;

    private PropertySet propertySet;
    private UpgradeTask_Build605 upgradeTask;

    @Before
    public void setUp() throws Exception {
        propertySet = new MemoryPropertySet();
        propertySet.init(null, null);
        when(mockPropertiesManager.getPropertySet()).thenReturn(propertySet);
        upgradeTask = new UpgradeTask_Build605(mockPropertiesManager);
    }

    @Test
    public void testRemoveListenerWhenExists() throws Exception {
        UtilsForTests.getTestEntity("ListenerConfig", FieldMap.build("name", "Issue Cache Listener", "clazz",
                UpgradeTask_Build605.ISSUE_CACHE_LISTENER_CLASS));
        UtilsForTests.getTestEntity("ListenerConfig", FieldMap.build("name", "Some Other Listener", "clazz",
                "foo.bar.Listener"));
        upgradeTask.doUpgrade(false);
        assertListenerDoesntExist(UpgradeTask_Build605.ISSUE_CACHE_LISTENER_CLASS);
        assertListenerExists("foo.bar.Listener");
    }

    @Test
    public void testUpgradeRemoveOptionsWhenSet() throws Exception {
        mockPropertiesManager.getPropertySet().setBoolean(UpgradeTask_Build605.JIRA_OPTION_CACHE_ISSUES, false);
        mockPropertiesManager.getPropertySet().setBoolean(UpgradeTask_Build605.JIRA_OPTION_CACHE_PERMISSIONS, false);
        mockPropertiesManager.getPropertySet().setBoolean(UpgradeTask_Build605.JIRA_OPTION_CACHE_PROJECTS, false);
        upgradeTask.doUpgrade(false);
        assertFalse(mockPropertiesManager.getPropertySet().exists(UpgradeTask_Build605.JIRA_OPTION_CACHE_ISSUES));
        assertFalse(mockPropertiesManager.getPropertySet().exists(UpgradeTask_Build605.JIRA_OPTION_CACHE_PERMISSIONS));
        assertFalse(mockPropertiesManager.getPropertySet().exists(UpgradeTask_Build605.JIRA_OPTION_CACHE_PROJECTS));
    }

    @Test
    public void testUpgradeRemoveOptionsWhenNotSet() throws Exception {
        // Make sure no exceptions are thrown if the options aren't set
        upgradeTask.doUpgrade(false);
        assertFalse(mockPropertiesManager.getPropertySet().exists(UpgradeTask_Build605.JIRA_OPTION_CACHE_ISSUES));
        assertFalse(mockPropertiesManager.getPropertySet().exists(UpgradeTask_Build605.JIRA_OPTION_CACHE_PERMISSIONS));
        assertFalse(mockPropertiesManager.getPropertySet().exists(UpgradeTask_Build605.JIRA_OPTION_CACHE_PROJECTS));
    }

    private void assertListenerDoesntExist(String clazz) throws Exception {
        final Collection<GenericValue> listenerConfigs = ofBizDelegator.findAll("ListenerConfig");
        if (listenerConfigs != null) {
            for (GenericValue gv : listenerConfigs) {
                assertFalse("Listener " + clazz + " exists", clazz.equals(gv.getString("clazz")));
            }
        }

    }

    private void assertListenerExists(String clazz) throws Exception {
        final Collection<GenericValue> listenerConfigs = ofBizDelegator.findAll("ListenerConfig");
        if (listenerConfigs != null) {
            for (GenericValue gv : listenerConfigs) {
                if (clazz.equals(gv.getString("clazz"))) {
                    return;
                }
            }
        }
        fail("Listener " + clazz + " doesn't exist");
    }
}
