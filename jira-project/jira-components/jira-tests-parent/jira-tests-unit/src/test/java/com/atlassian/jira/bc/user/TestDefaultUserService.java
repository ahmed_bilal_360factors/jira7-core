package com.atlassian.jira.bc.user;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.crowd.embedded.api.OperationType;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.fugue.Option;
import com.atlassian.jira.application.ApplicationRole;
import com.atlassian.jira.application.ApplicationRoleManager;
import com.atlassian.jira.application.MockApplicationRole;
import com.atlassian.jira.bc.ServiceResultImpl;
import com.atlassian.jira.bc.license.JiraLicenseService;
import com.atlassian.jira.exception.AddException;
import com.atlassian.jira.exception.CreateException;
import com.atlassian.jira.exception.PermissionException;
import com.atlassian.jira.exception.RemoveException;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.mock.MockPermissionManager;
import com.atlassian.jira.mock.security.MockSimpleAuthenticationContext;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.plugin.user.PasswordPolicyManager;
import com.atlassian.jira.plugin.user.PreDeleteUserErrorsManager;
import com.atlassian.jira.plugin.user.WebErrorMessage;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.MockGroup;
import com.atlassian.jira.user.MockUserDeleteVeto;
import com.atlassian.jira.user.UserDeleteVeto;
import com.atlassian.jira.user.util.MockUserManager;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.user.util.UserUtil;
import com.atlassian.jira.user.util.UserUtil.PasswordResetToken;
import com.atlassian.jira.util.ComponentLocator;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.GlobalPermissionGroupAssociationUtil;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.jira.util.SimpleWarningCollection;
import com.atlassian.jira.web.bean.MockI18nBean;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.event.PluginEventManager;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import org.apache.commons.lang3.StringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.mockito.Mock;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.atlassian.jira.matchers.ErrorCollectionMatcher.hasNoErrors;
import static java.util.Arrays.asList;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Clean tests for DefaultUserService.
 *
 * @since v5.0
 */
public class TestDefaultUserService {
    @Rule
    public final RuleChain mockContainer = MockitoMocksInContainer.forTest(this);
    @Mock
    private PermissionManager permissionManager;
    @Mock
    private PreDeleteUserErrorsManager preDeleteUserErrorsManager;
    @Mock
    private PasswordPolicyManager passwordPolicyManager;
    @Mock
    private UserUtil userUtil;
    @Mock
    private CreateUserApplicationHelper applicationHelper;
    @Mock
    private ApplicationRoleManager applicationRoleManager;
    @Mock
    @AvailableInContainer
    private PluginAccessor pluginAccessor;
    @Mock
    @AvailableInContainer
    private PluginEventManager pluginEventManager;
    @Mock
    @AvailableInContainer
    private EventPublisher eventPublisher;
    private UserDeleteVeto userDeleteVeto = new MockUserDeleteVeto();
    private I18nHelper.BeanFactory i18nFactory = new MockI18nBean.MockI18nBeanFactory();
    private MockUserManager userManager = new MockUserManager();
    private ApplicationUser admin = new MockApplicationUser("Admin");
    @AvailableInContainer
    private JiraAuthenticationContext jiraAuthenticationContext = new MockSimpleAuthenticationContext(null);
    private UserValidationHelper validationHelper;
    @Mock
    private GlobalPermissionGroupAssociationUtil globalPermissionGroupAssociationUtil;
    @Mock
    private GlobalPermissionManager globalPermissionManager;
    private final ApplicationUser TEST_USER = new MockApplicationUser("testuser");

    private UserService defaultUserService;

    private final ApplicationKey TEST_APPLICATION_KEY = ApplicationKey.valueOf("testApplicationKey");
    private final ApplicationUser NON_ADMIN_USER = new MockApplicationUser("nonAdminUser");
    private static final String NO_PERMISSION_TO_ADD_USER_TO_APPLICATION_ERROR = "You do not have permission to add users to applications.";
    private static final String NO_PERMISSION_TO_REMOVE_USER_TO_APPLICATION_ERROR = "You do not have permission to remove users to applications.";

    @Before
    public void setUp() throws Exception {
        validationHelper = new UserValidationHelper(i18nFactory, jiraAuthenticationContext, permissionManager,
                userManager, passwordPolicyManager);
        userManager.addUser(admin);

        when(preDeleteUserErrorsManager.getWarnings(any(ApplicationUser.class))).thenReturn(ImmutableList.<WebErrorMessage>of());
        when(passwordPolicyManager.checkPolicy(any(ApplicationUser.class), anyString(), anyString())).thenReturn(ImmutableList.<WebErrorMessage>of());
        when(passwordPolicyManager.checkPolicy(anyString(), anyString(), anyString(), anyString())).thenReturn(ImmutableList.<WebErrorMessage>of());
        mockGlobalPermission(Permissions.ADMINISTER, admin);
        when(permissionManager.hasPermission(Permissions.ADMINISTER, NON_ADMIN_USER)).thenReturn(false);
        this.defaultUserService = newDefaultUserService();
    }

    @After
    public void tearDown() throws Exception {
        permissionManager = null;
        preDeleteUserErrorsManager = null;
        userUtil = null;
        i18nFactory = null;
        userManager = null;
        admin = null;
        jiraAuthenticationContext = null;
    }

    public DefaultUserService newDefaultUserService() {
        return new DefaultUserService(userUtil, userDeleteVeto, permissionManager, userManager,
                i18nFactory, jiraAuthenticationContext, null, preDeleteUserErrorsManager,
                applicationHelper, applicationRoleManager, validationHelper, globalPermissionGroupAssociationUtil, globalPermissionManager);
    }

    public DefaultUserService newUserServiceWithAuthenticationContext(JiraAuthenticationContext authenticationContext) {
        return new DefaultUserService(userUtil, userDeleteVeto, permissionManager, userManager,
                i18nFactory, authenticationContext, null, preDeleteUserErrorsManager,
                applicationHelper, applicationRoleManager, validationHelper, globalPermissionGroupAssociationUtil, globalPermissionManager);
    }

    private ApplicationUser createUser(final String username, final String password, final String emailAddress, final String displayName)
            throws CreateException, PermissionException {

        final GlobalPermissionManager globalPermissionManager = mock(GlobalPermissionManager.class);
        final ComponentLocator componentLocator = mock(ComponentLocator.class);
        final JiraLicenseService jiraLicenseService = mock(JiraLicenseService.class);

        when(globalPermissionManager.getGroupsWithPermission(anyInt())).thenReturn(ImmutableList.<Group>of());
        when(componentLocator.getComponentInstanceOfType(JiraLicenseService.class)).thenReturn(jiraLicenseService);
        final PasswordResetToken passwordResetToken = mock(PasswordResetToken.class);
        when(passwordResetToken.getExpiryHours()).thenReturn(24);
        when(passwordResetToken.getToken()).thenReturn("TOKEN");
        when(userUtil.generatePasswordResetToken(anyObject())).thenReturn(passwordResetToken);

        final UserService userService = newDefaultUserService();
        UserService.CreateUserValidationResult validationResult = new UserService.CreateUserValidationResult(username, password, emailAddress, displayName);
        assertNoErrors(validationResult);

        final ApplicationUser user = userService.createUser(validationResult);

        assertThat(user.getName(), is(username));
        assertThat(user.getEmailAddress(), is(emailAddress));
        assertThat(user.getDisplayName(), is(displayName));

        return userManager.getUser(username);
    }

    @Test
    public void testValidateCreateJiraUserForAdminNoPermission() {
        final UserService userService = newDefaultUserService();
        final ApplicationUser fred = new MockApplicationUser("Fred");
        userManager.addUser(fred);

        final UserService.CreateUserValidationResult result = userService.validateCreateUserForAdmin(
                fred, "fflintstone", "mypassword", "mypassword", "fred@flintstone.com", "Fred Flintstone");
        assertEquals(asList("You do not have permission to create a user."), result.getErrorCollection().getErrorMessages());
        assertFalse("Should not be a valid result", result.isValid());
    }

    @Test
    public void testValidateCreateJiraUserForSetupOk() {
        final UserService userService = newDefaultUserService();
        final ApplicationUser fred = new MockApplicationUser("Fred");
        userManager.addUser(fred);

        final UserService.CreateUserValidationResult result = userService.validateCreateUserForSignupOrSetup(fred,
                "fflintstone", "mypassword", "mypassword", "fred@flintstone.com", "Fred Flintstone");
        assertNoErrors(result);
        assertEquals("fflintstone", result.getUsername());
        assertEquals("mypassword", result.getPassword());
        assertEquals("Fred Flintstone", result.getFullname());
        assertEquals("fred@flintstone.com", result.getEmail());
    }

    @Test
    public void testValidateCreateJiraUserForAdminExternalAdmin() {
        final UserService userService = newDefaultUserService();
        userManager.setWritableDirectory(false);

        final UserService.CreateUserValidationResult result = userService.validateCreateUserForAdmin(
                admin, "fflintstone", "mypassword", "mypassword", "fred@flintstone.com", "Fred Flintstone");

        assertEquals(asList("Cannot add user, all the user directories are read-only."), result.getErrorCollection().getErrorMessages());
        assertTrue("Should have errors", result.getErrorCollection().hasAnyErrors());
        assertFalse("Should not be valid", result.isValid());
    }

    @Test
    public void testValidateCreateJiraUserForSignUpNoExtMgmnt() {
        final UserService userService = newDefaultUserService();
        userManager.setWritableDirectory(false);

        final UserService.CreateUserValidationResult result = userService.validateCreateUserForSignup(
                admin, "fflintstone", "mypassword", "mypassword", "fred@flintstone.com", "Fred Flintstone");

        assertEquals(asList("Cannot add user, all the user directories are read-only."), result.getErrorCollection().getErrorMessages());
        assertTrue("Should have errors", result.getErrorCollection().hasAnyErrors());
        assertFalse("Should not be valid", result.isValid());
    }

    @Test
    public void testValidateCreateJiraUserForSetupNoPassword() {
        final UserService userService = newDefaultUserService();
        final UserService.CreateUserValidationResult result = userService.validateCreateUserForSignupOrSetup(
                admin, "fflintstone", "", "", "fred@flintstone.com", "Fred Flintstone");
        assertEquals("You must specify a password and a confirmation password.", result.getErrorCollection().getErrors().get("password"));
        assertTrue("Should have errors", result.getErrorCollection().hasAnyErrors());
        assertFalse("Should not be valid", result.isValid());
    }

    @Test
    public void testValidateCreateJiraUserForAdmin() {
        final UserService userService = newDefaultUserService();
        final UserService.CreateUserValidationResult result = userService.validateCreateUserForAdmin(
                admin, "fflintstone", null, null, "fred@flintstone.com", "Fred Flintstone");
        assertTrue(result.isValid());
        assertFalse(result.getErrorCollection().hasAnyErrors());
    }

    @Test
    public void testValidateCreateJiraUserForAdmin_nameDirectory() {
        final UserService userService = newDefaultUserService();
        final UserService.CreateUserValidationResult result = userService.validateCreateUserForAdmin(
                admin, "fflintstone", null, null, "fred@flintstone.com", "Fred Flintstone", 1L);
        assertNoErrors(result);
    }

    @Test
    public void testValidateCreateJiraUserForAdminNoPermissionNoPassword() {
        final UserService userService = newDefaultUserService();
        final ApplicationUser fred = new MockApplicationUser("Fred");
        userManager.addUser(fred);
        final UserService.CreateUserValidationResult result = userService.validateCreateUserForAdmin(
                fred, "fflintstone", null, null, "fred@flintstone.com", "Fred Flintstone");

        assertEquals(asList("You do not have permission to create a user."), result.getErrorCollection().getErrorMessages());
        assertTrue("Should have errors", result.getErrorCollection().hasAnyErrors());
        assertFalse("Should not be valid", result.isValid());
    }

    @Test
    public void testValidateCreateJiraUserForAdminNoPermissionNoExtMgmnt() {
        final UserService userService = newDefaultUserService();
        userManager.setWritableDirectory(false);
        final UserService.CreateUserValidationResult result = userService.validateCreateUserForAdmin(
                admin, "fflintstone", null, null, "fred@flintstone.com", "Fred Flintstone");

        assertEquals(asList("Cannot add user, all the user directories are read-only."), result.getErrorCollection().getErrorMessages());
        assertFalse("Should not be valid", result.isValid());
        assertTrue("Should have errors", result.getErrorCollection().hasAnyErrors());
    }

    private void checkCreateUserValues(final String username, final String password, final String confirm, final String email, final String fullname, final String errorI18n, final String errorField) {
        final I18nHelper i18n = new MockI18nBean();
        final UserService userService = newDefaultUserService();
        final String expectedMessage = i18n.getText(errorI18n);
        final UserService.CreateUserValidationResult result = userService.validateCreateUserForAdmin(
                admin, username, password, confirm, email, fullname);
        assertEquals(expectedMessage, result.getErrorCollection().getErrors().get(errorField));
        assertFalse(result.isValid());
        assertTrue(result.getErrorCollection().hasAnyErrors());
    }

    @Test
    public void testValidateCreateJiraUserPasswordNotMatch() {
        checkCreateUserValues("fflintstone", "mypassword", "mypassword2", "fred@flintstone.com", "Fred Flintstone",
                "signup.error.password.mustmatch", "confirm");
    }

    @Test
    public void testValidateCreateJiraUserNoEmail() {
        checkCreateUserValues("fflintstone", "mypassword", "mypassword", null, "Fred Flintstone", "signup.error.email.required", "email");
    }

    @Test
    public void testValidateCreateJiraUserEmailExceeds255() {
        checkCreateUserValues("testuser543", "mypassword", "mypassword", StringUtils.repeat("a", 256), "Fred Flintstone", "signup.error.email.greater.than.max.chars", "email");
    }

    @Test
    public void testValidateCreateJiraUserWrongEmail() {
        checkCreateUserValues("fflintstone", "mypassword", "mypassword", "fred", "Fred Flintstone", "signup.error.email.valid", "email");
    }

    @Test
    public void testValidateCreateJiraUserNoUsername() {
        checkCreateUserValues("", "mypassword", "mypassword", "fred@flintstone.com", "Fred Flintstone", "signup.error.username.required", "username");
    }

    @Test
    public void testValidateCreateJiraUserInvalidUsername1() {
        checkCreateUserValues("fred<bad", "mypassword", "mypassword", "fred@flintstone.com", "Fred Flintstone", "signup.error.username.invalid.chars", "username");
    }

    @Test
    public void testValidateCreateJiraUserInvalidUsername2() {
        checkCreateUserValues("fred>bad", "mypassword", "mypassword", "fred@flintstone.com", "Fred Flintstone", "signup.error.username.invalid.chars", "username");
    }

    @Test
    public void testValidateCreateJiraUserInvalidUsername3() {
        checkCreateUserValues("fred&bad", "mypassword", "mypassword", "fred@flintstone.com", "Fred Flintstone", "signup.error.username.invalid.chars", "username");
    }

    @Test
    public void testValidateCreateJiraUserUsernameExceeds255() {
        checkCreateUserValues(StringUtils.repeat("a", 256), "mypassword", "mypassword", "fred@flintstone.com", "Fred Flintstone", "signup.error.username.greater.than.max.chars", "username");
    }

    @Test
    public void testValidateCreateJiraUserExists() {
        userManager.addUser(new MockApplicationUser("DupeMe"));
        checkCreateUserValues("DupeMe", "mypassword", "mypassword", "fred@flintstone.com", "Fred Flintstone", "signup.error.username.exists",
                "username");
    }

    @Test
    public void testValidateCreateJiraUserNoFullname() {
        checkCreateUserValues("fflintstone", "mypassword", "mypassword", "fred@flintstone.com", null, "signup.error.fullname.required", "fullname");
    }

    @Test
    public void testValidateCreateJiraUserFullnameExceeds255() {
        checkCreateUserValues("testuser654", "mypassword", "mypassword", "fred@flintstone.com", StringUtils.repeat("a", 256), "signup.error.full.name.greater.than.max.chars", "fullname");
    }

    @Test
    public void testValidateDeleteUserNoPermission() throws Exception {
        final ApplicationUser dude = new MockApplicationUser("DudeUser");
        final ApplicationUser fred = new MockApplicationUser("Fred");
        userManager.addUser(dude);
        userManager.addUser(fred);

        DefaultUserService userService = newDefaultUserService();
        UserService.DeleteUserValidationResult validationResult = userService.validateDeleteUser((ApplicationUser) null, "fred");

        final List<String> expectedErrors = asList("You do not have the permission to remove users.");
        assertEquals(expectedErrors, validationResult.getErrorCollection().getErrorMessages());
        assertTrue("Should have errors", validationResult.getErrorCollection().hasAnyErrors());
    }

    @Test
    public void testValidateDeleteUserNull() throws Exception {
        DefaultUserService userService = newDefaultUserService();
        UserService.DeleteUserValidationResult validationResult = userService.validateDeleteUser((ApplicationUser) null, (String) null);

        // This error gets returned on the username field, not the general error messages
        assertEquals("Username for delete can not be null or empty.", validationResult.getErrorCollection().getErrors().get("username"));
        assertEquals(Collections.<String>emptyList(), validationResult.getErrorCollection().getErrorMessages());
        assertTrue("Should have errors", validationResult.getErrorCollection().hasAnyErrors());
        assertFalse("Should not be valid", validationResult.isValid());
    }

    @Test
    public void testValidateDeleteUserEmpty() throws Exception {
        DefaultUserService userService = newDefaultUserService();
        UserService.DeleteUserValidationResult validationResult = userService.validateDeleteUser((ApplicationUser) null, "");

        // This error gets returned on the username field, not the general error messages
        assertEquals("Username for delete can not be null or empty.", validationResult.getErrorCollection().getErrors().get("username"));
        assertEquals(Collections.<String>emptyList(), validationResult.getErrorCollection().getErrorMessages());
        assertTrue("Should have errors", validationResult.getErrorCollection().hasAnyErrors());
        assertFalse("Should not be valid", validationResult.isValid());
    }

    @Test
    public void testValidateDeleteUserDeleteSelf() throws Exception {
        final MockApplicationUser fred1 = new MockApplicationUser("Fred");
        final MockApplicationUser fred2 = new MockApplicationUser("fReD");
        userManager.addUser(fred1);
        userManager.addUser(fred2);

        permissionManager = new MockPermissionManager(true);
        DefaultUserService userService = newDefaultUserService();
        UserService.DeleteUserValidationResult validationResult = userService.validateDeleteUser(fred1, fred2);

        final List<String> expectedErrors = asList("You cannot delete the currently logged in user.");
        assertEquals(expectedErrors, validationResult.getErrorCollection().getErrorMessages());
        assertTrue("Should have errors", validationResult.getErrorCollection().hasAnyErrors());
    }

    @Test
    public void testValidateDeleteUserNotExist() throws Exception {
        permissionManager = new MockPermissionManager(true);
        DefaultUserService userService = newDefaultUserService();
        UserService.DeleteUserValidationResult validationResult = userService.validateDeleteUser(new MockApplicationUser("dude"), "fred");

        final List<String> expectedErrors = asList("This user does not exist please select a user from the user browser.");
        assertEquals(expectedErrors, validationResult.getErrorCollection().getErrorMessages());
        assertTrue("Should have errors", validationResult.getErrorCollection().hasAnyErrors());
    }

    @Test
    public void testValidateDeleteUserReadOnly() throws Exception {
        userManager.addUser(new MockApplicationUser("fred"));
        userManager.setWritableDirectory(false);

        permissionManager = new MockPermissionManager(true);
        DefaultUserService userService = newDefaultUserService();
        UserService.DeleteUserValidationResult validationResult = userService.validateDeleteUser(new MockApplicationUser("dude"), "fred");

        final List<String> expectedErrors = asList("Cannot delete user, the user directory is read-only.");
        assertEquals(expectedErrors, validationResult.getErrorCollection().getErrorMessages());
        assertTrue("Should have errors", validationResult.getErrorCollection().hasAnyErrors());
    }

    @Test
    public void testValidateDeleteUserForeignKeysButInMultipleDirectories() throws Exception {
        final UserManager userManager = mock(UserManager.class);
        final ApplicationUser admin = new MockApplicationUser("Admin");
        final ApplicationUser fred = new MockApplicationUser("Fred");
        when(userManager.isUserExisting(fred)).thenReturn(true);
        when(userManager.canUpdateUser(fred)).thenReturn(true);
        when(userManager.getUserState(fred)).thenReturn(UserManager.UserState.NORMAL_USER_WITH_SHADOW);

        DefaultUserService userService = new DefaultUserService(userUtil, null, new MockPermissionManager(true),
                userManager, i18nFactory, null, null, preDeleteUserErrorsManager,
                applicationHelper, null, validationHelper, globalPermissionGroupAssociationUtil, globalPermissionManager);
        UserService.DeleteUserValidationResult validationResult = userService.validateDeleteUser(admin, fred);

        // When the user is in multiple directories, these shouldn't get checked at all
        // (This also checks the old User.class forms, as Matchers.any doesn't do type checks)
        verify(userUtil, never()).getNumberOfReportedIssuesIgnoreSecurity(any(ApplicationUser.class), any(ApplicationUser.class));
        verify(userUtil, never()).getNumberOfAssignedIssuesIgnoreSecurity(any(ApplicationUser.class), any(ApplicationUser.class));
        verify(userUtil, never()).getProjectsLeadBy(any(ApplicationUser.class));

        assertNoErrors(validationResult);
    }

    @Test
    public void testValidateDeleteUserForeignKeys() throws Exception {
        final ApplicationUser admin = new MockApplicationUser("Admin");
        final ApplicationUser fred = new MockApplicationUser("Fred");
        userManager.addUser(admin);
        userManager.addUser(fred);
        when(userUtil.getNumberOfReportedIssuesIgnoreSecurity(admin, fred)).thenReturn(387L);
        when(userUtil.getNumberOfAssignedIssuesIgnoreSecurity(admin, fred)).thenReturn(26L);
        when(userUtil.getProjectsLeadBy(fred)).thenReturn(Collections.<Project>singleton(new MockProject(10003L)));

        permissionManager = new MockPermissionManager(true);
        userDeleteVeto = new MockUserDeleteVeto().setDefaultCommentCount(12);
        DefaultUserService userService = newDefaultUserService();
        UserService.DeleteUserValidationResult validationResult = userService.validateDeleteUser(admin, "fred");

        final List<String> expectedErrors = asList(
                "Cannot delete user 'Fred' because 387 issues were reported by this person.",
                "Cannot delete user 'Fred' because 26 issues are currently assigned to this person.",
                "Cannot delete user 'Fred' because they have made 12 comments.",
                "Cannot delete user 'Fred' because they are currently the project lead on 1 projects.");
        assertEquals(expectedErrors, validationResult.getErrorCollection().getErrorMessages());
        assertTrue("Should have errors", validationResult.getErrorCollection().hasAnyErrors());
    }

    @Test
    public void testValidateDeleteUserNonSysAdminAttemptingToDeleteSysAdmin() throws Exception {
        final MockApplicationUser fred = new MockApplicationUser("Fred");
        userManager.addUser(fred);
        mockGlobalPermission(Permissions.SYSTEM_ADMIN, fred);

        DefaultUserService userService = newDefaultUserService();
        UserService.DeleteUserValidationResult validationResult = userService.validateDeleteUser(admin, "frEd");

        final List<String> expectedErrors = asList("As a user with JIRA Administrators permission, you cannot delete users with JIRA System Administrators permission.");
        assertEquals(expectedErrors, validationResult.getErrorCollection().getErrorMessages());
        assertTrue("Should have errors", validationResult.getErrorCollection().hasAnyErrors());
    }

    @Test
    public void testValidateDeleteUserHappyHappyJoyJoy() throws Exception {
        userManager.addUser(new MockApplicationUser("fred"));

        DefaultUserService userService = newDefaultUserService();
        UserService.DeleteUserValidationResult validationResult = userService.validateDeleteUser(new MockApplicationUser("admin"), "fred");

        assertNoErrors(validationResult);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testRemoveUserNullResult() {
        final UserService userService = newDefaultUserService();
        userService.removeUser(new MockApplicationUser("admin"), null);
    }

    @Test(expected = IllegalStateException.class)
    public void testRemoveUserResultWithError() {
        final ErrorCollection errors = new SimpleErrorCollection();
        errors.addErrorMessage("error");

        final UserService userService = newDefaultUserService();
        final UserService.DeleteUserValidationResult result = new UserService.DeleteUserValidationResult(errors);
        userService.removeUser(new MockApplicationUser("admin"), result);
    }

    @Test
    public void testRemoveUser() throws Exception {
        final MockApplicationUser loggedInUser = new MockApplicationUser("admin");
        final MockApplicationUser fred = new MockApplicationUser("Fred");
        userManager.addUser(fred);

        DefaultUserService userService = newDefaultUserService();
        UserService.DeleteUserValidationResult validationResult = userService.validateDeleteUser(loggedInUser, "fred");
        assertNoErrors(validationResult);

        userService.removeUser(loggedInUser, validationResult);
        verify(userUtil).removeUser(loggedInUser, fred);
    }

    private void assertNoErrors(ServiceResultImpl validationResult) {
        assertThat(validationResult.getErrorCollection(), hasNoErrors());
        assertTrue("Should be valid", validationResult.isValid());
    }

    @Test
    public void testCreateUserNullResult() throws PermissionException, CreateException {
        final UserService userService = newDefaultUserService();
        try {
            userService.createUser(null);
            fail("Should not be able to create a user with a null validation result.");
        } catch (final IllegalArgumentException e) {
            assertEquals("You can not create a user, validation result should not be null!", e.getMessage());
        }
    }

    @Test
    public void testCreateUserInvalidResult() throws PermissionException, CreateException {
        final ErrorCollection errors = new SimpleErrorCollection();
        errors.addErrorMessage("Something went wrong");
        UserService.CreateUserRequest userRequest = UserService.CreateUserRequest.withUserDetails(null, "charlie", null, null, null);
        final UserService.CreateUserValidationResult result = new UserService.CreateUserValidationResult(userRequest,
                Collections.emptySet(), errors, Collections.emptyList(), new SimpleWarningCollection());
        final UserService userService = newDefaultUserService();

        try {
            userService.createUser(result);
            fail("Should not be able to create a user with an invalid validation result.");
        } catch (final CreateException e) {
            assertEquals("Validation failed, user charlie cannot be created", e.getMessage());
        }
    }

    @Test
    public void testCreateJiraUserWithPassword() throws PermissionException, CreateException {
        createUser("fflintstone", "mypassword", "fred@flintstone.com", "Fred Flintstone");
    }

    @Test
    public void testCreateJiraUserWithNullPassword() throws PermissionException, CreateException {
        // We don't use it anywhere in JIRA, but it should simply generate a random password for wilma.
        // Since that's done inside UserUtil, we don't need to verify that here.
        createUser("wflintstone", null, "wilma@flintstone.com", "Wilma Flintstone");
    }

    private void mockGlobalPermission(int permission, ApplicationUser user) {
        when(permissionManager.hasPermission(permission, user)).thenReturn(true);
    }

    @Test
    public void validationShouldFailWhenNonAdminTryToAddUserToApplication() {
        final UserService.AddUserToApplicationValidationResult result = defaultUserService.validateAddUserToApplication(NON_ADMIN_USER, TEST_USER, TEST_APPLICATION_KEY);

        assertFalse("Should not be a valid result", result.isValid());
        assertEquals(asList(NO_PERMISSION_TO_ADD_USER_TO_APPLICATION_ERROR), result.getErrorCollection().getErrorMessages());
    }

    @Test
    public void addUserToApplicationValidationShouldFailForAnonymousUser() {
        final UserService.AddUserToApplicationValidationResult result = defaultUserService.validateAddUserToApplication(null, TEST_USER, TEST_APPLICATION_KEY);

        assertFalse("Should not be a valid result", result.isValid());
        assertEquals(asList(NO_PERMISSION_TO_ADD_USER_TO_APPLICATION_ERROR), result.getErrorCollection().getErrorMessages());
    }

    @Test
    public void addUserToApplicationValidationShouldFailWhenAuthenticationContextIsNull() {
        final UserService.AddUserToApplicationValidationResult result = newUserServiceWithAuthenticationContext(null).validateAddUserToApplication(TEST_USER, TEST_APPLICATION_KEY);

        assertFalse("Should not be a valid result", result.isValid());
        assertEquals(asList(NO_PERMISSION_TO_ADD_USER_TO_APPLICATION_ERROR), result.getErrorCollection().getErrorMessages());
    }

    @Test
    public void addUserToApplicationValidationShouldFailWhenLoggedInUserIsNotAdmin() {
        JiraAuthenticationContext authenticationContext = new MockSimpleAuthenticationContext(NON_ADMIN_USER);
        final UserService.AddUserToApplicationValidationResult result = newUserServiceWithAuthenticationContext(authenticationContext).validateAddUserToApplication(TEST_USER, TEST_APPLICATION_KEY);

        assertFalse("Should not be a valid result", result.isValid());
        assertEquals(asList(NO_PERMISSION_TO_ADD_USER_TO_APPLICATION_ERROR), result.getErrorCollection().getErrorMessages());
    }

    @Test
    public void addUserToApplicationShouldNotAllowToAddWhenApplicationKeyIsInValid() {
        final UserService userService = newDefaultUserService();

        when(applicationHelper.validateApplicationKeys(TEST_USER, ImmutableSet.of(TEST_APPLICATION_KEY))).thenReturn(ImmutableList.of("My error message"));

        final UserService.AddUserToApplicationValidationResult result = userService.validateAddUserToApplication(admin, TEST_USER, TEST_APPLICATION_KEY);

        assertEquals(asList("My error message"), result.getErrorCollection().getErrorMessages());
        assertFalse("Should not be a valid result", result.isValid());
    }


    @Test
    public void addUserToApplicationWithValidResultShouldBeSuccessful() throws AddException, PermissionException {
        final UserService userService = newDefaultUserService();

        final Set<Group> defaultGroups = ImmutableSet.of(new MockGroup("group 1"), new MockGroup("group 2"));

        when(applicationRoleManager.getDefaultGroups(TEST_APPLICATION_KEY)).thenReturn(defaultGroups);

        final UserService.AddUserToApplicationValidationResult result = userService.validateAddUserToApplication(admin, TEST_USER, TEST_APPLICATION_KEY);
        userService.addUserToApplication(result);

        assertTrue("Should be a valid result", result.isValid());
        verify(userUtil).addUserToGroups(defaultGroups, TEST_USER);
    }

    @Test
    public void validationShouldFailWhenNonAdminTryToRemoveUserToApplication() {
        final UserService.RemoveUserFromApplicationValidationResult result = defaultUserService.validateRemoveUserFromApplication(NON_ADMIN_USER, TEST_USER, TEST_APPLICATION_KEY);

        assertFalse("Should not be a valid result", result.isValid());
        assertEquals(asList(NO_PERMISSION_TO_REMOVE_USER_TO_APPLICATION_ERROR), result.getErrorCollection().getErrorMessages());
    }

    @Test
    public void removeUserToApplicationValidationShouldFailForAnonymousUser() {
        final UserService.RemoveUserFromApplicationValidationResult result = defaultUserService.validateRemoveUserFromApplication(null, TEST_USER, TEST_APPLICATION_KEY);

        assertFalse("Should not be a valid result", result.isValid());
        assertEquals(asList(NO_PERMISSION_TO_REMOVE_USER_TO_APPLICATION_ERROR), result.getErrorCollection().getErrorMessages());
    }

    @Test
    public void removeUserToApplicationValidationShouldFailWhenAuthenticationContextIsNull() {
        final UserService.RemoveUserFromApplicationValidationResult result = newUserServiceWithAuthenticationContext(null).validateRemoveUserFromApplication(TEST_USER, TEST_APPLICATION_KEY);

        assertFalse("Should not be a valid result", result.isValid());
        assertEquals(asList(NO_PERMISSION_TO_REMOVE_USER_TO_APPLICATION_ERROR), result.getErrorCollection().getErrorMessages());
    }

    @Test
    public void removeUserToApplicationValidationShouldFailWhenLoggedInUserIsNotAdmin() {
        JiraAuthenticationContext authenticationContext = new MockSimpleAuthenticationContext(NON_ADMIN_USER);
        final UserService.RemoveUserFromApplicationValidationResult result = newUserServiceWithAuthenticationContext(authenticationContext).validateRemoveUserFromApplication(TEST_USER, TEST_APPLICATION_KEY);

        assertFalse("Should not be a valid result", result.isValid());
        assertEquals(asList(NO_PERMISSION_TO_REMOVE_USER_TO_APPLICATION_ERROR), result.getErrorCollection().getErrorMessages());
    }


    @Test
    public void removeUserFromUndefinedApplicationShouldProduceAnInvalidResult() {
        final UserService userService = newDefaultUserService();

        final UserService.RemoveUserFromApplicationValidationResult result = userService.validateRemoveUserFromApplication(admin, TEST_USER, ApplicationKey.valueOf("Undefined-Key"));

        assertEquals(asList("There is not an application defined by application key: Undefined-Key."), result.getErrorCollection().getErrorMessages());
        assertFalse("Should not be a valid result", result.isValid());
    }

    @Test
    public void removeUserFromApplicationWhenUserLivesInFullyReadOnlyDirectoryShouldProduceAnInvalidResult() {
        final long directoryId = 1l;
        final Directory directory = mock(Directory.class);
        when(directory.getName()).thenReturn("testdirectory");
        when(directory.getAllowedOperations()).thenReturn(new HashSet<OperationType>());

        final UserManager userManager = mock(UserManager.class);
        when(userManager.getDirectory(directoryId)).thenReturn(directory);

        final ApplicationRoleManager applicationRoleManager = mock(ApplicationRoleManager.class);
        final DefaultUserService userService = new DefaultUserService(null, null, permissionManager, userManager, i18nFactory,
                jiraAuthenticationContext, null, null, null, applicationRoleManager, validationHelper, globalPermissionGroupAssociationUtil, globalPermissionManager);

        final UserService.RemoveUserFromApplicationValidationResult result = userService.validateRemoveUserFromApplication(admin, TEST_USER, ApplicationKey.valueOf("Undefined-Key"));

        assertTrue(result.getErrorCollection().getErrorMessages().contains("The underlying directory 'testdirectory' is fully read-only."));
        assertFalse("Should not be a valid result", result.isValid());
    }

    @Test
    public void removeUserFromApplicationWithValidResultShouldBeSuccessful() throws RemoveException, PermissionException {
        final UserService userService = newDefaultUserService();

        final String groupName1 = "group 1";
        final String groupName2 = "group 2";
        final Group group1 = new MockGroup(groupName1);
        final Group group2 = new MockGroup(groupName2);

        final Set<String> groupNames = new HashSet<String>();
        groupNames.add(groupName1);
        groupNames.add(groupName2);

        final ApplicationKey applicationKey = ApplicationKey.valueOf("Undefined-Key");
        final ApplicationRole applicationRole = new MockApplicationRole().name("Test Application").groupNames(groupNames);

        when(applicationRoleManager.getRole(applicationKey)).thenReturn(Option.<ApplicationRole>some(applicationRole));
        when(applicationRoleManager.hasSeatsAvailable(applicationKey, 1)).thenReturn(true);

        when(userUtil.getGroup(groupName1)).thenReturn(group1);
        when(userUtil.getGroup(groupName2)).thenReturn(group2);

        final UserService.RemoveUserFromApplicationValidationResult result = userService.validateRemoveUserFromApplication(admin, TEST_USER, applicationKey);
        userService.removeUserFromApplication(result);

        assertTrue("Should be a valid result", result.isValid());
        verify(userUtil).removeUserFromGroup(group1, TEST_USER);
        verify(userUtil).removeUserFromGroup(group2, TEST_USER);
    }

    @Test
    public void removeSysAdminFromApplicationShouldProduceAnInvalidResult() throws RemoveException, PermissionException {
        final UserService userService = newDefaultUserService();
        final ApplicationUser adminUser = new MockApplicationUser("adminuser");

        final String groupName1 = "group 1";
        final Group group1 = new MockGroup(groupName1);

        final Set<String> groupNames = new HashSet<String>();
        groupNames.add(groupName1);

        final ApplicationKey applicationKey = ApplicationKey.valueOf("Undefined-Key");
        final ApplicationRole applicationRole = new MockApplicationRole().name("Test Application").groupNames(groupNames);

        when(applicationRoleManager.getRole(applicationKey)).thenReturn(Option.<ApplicationRole>some(applicationRole));
        when(applicationRoleManager.hasSeatsAvailable(applicationKey, 1)).thenReturn(true);

        when(userUtil.getGroup(groupName1)).thenReturn(group1);

        when(globalPermissionManager.hasPermission(GlobalPermissionKey.SYSTEM_ADMIN, adminUser)).thenReturn(true);
        when(permissionManager.hasPermission(Permissions.ADMINISTER, adminUser)).thenReturn(true);
        when(globalPermissionGroupAssociationUtil.isRemovingAllMySysAdminGroups(eq(groupNames), eq(adminUser))).thenReturn(true);

        final UserService.RemoveUserFromApplicationValidationResult result = userService.validateRemoveUserFromApplication(adminUser, adminUser, applicationKey);

        assertTrue(result.getErrorCollection().getErrorMessages().contains("You cannot remove yourself from this application, because by doing so you will lose the admin logon access to JIRA."));
        assertFalse("Should not be a valid result", result.isValid());
    }

    @Test
    public void removeAdminFromApplicationShouldProduceAnInvalidResult() throws RemoveException, PermissionException {
        final UserService userService = newDefaultUserService();

        final String groupName1 = "group 1";
        final Group group1 = new MockGroup(groupName1);

        final Set<String> groupNames = new HashSet<String>();
        groupNames.add(groupName1);

        final ApplicationKey applicationKey = ApplicationKey.valueOf("Undefined-Key");
        final ApplicationRole applicationRole = new MockApplicationRole().name("Test Application").groupNames(groupNames);

        when(applicationRoleManager.getRole(applicationKey)).thenReturn(Option.<ApplicationRole>some(applicationRole));
        when(applicationRoleManager.hasSeatsAvailable(applicationKey, 1)).thenReturn(true);

        when(userUtil.getGroup(groupName1)).thenReturn(group1);

        when(globalPermissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, admin)).thenReturn(true);
        when(globalPermissionGroupAssociationUtil.isRemovingAllMyAdminGroups(eq(groupNames), eq(admin))).thenReturn(true);

        final UserService.RemoveUserFromApplicationValidationResult result = userService.validateRemoveUserFromApplication(admin, admin, applicationKey);

        assertTrue(result.getErrorCollection().getErrorMessages().contains("You cannot remove yourself from this application, because by doing so you will lose the admin logon access to JIRA."));
        assertFalse("Should not be a valid result", result.isValid());
    }
}
