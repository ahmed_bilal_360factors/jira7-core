package com.atlassian.jira.bc.dataimport;

import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.database.DbConnectionManager;
import com.atlassian.jira.database.MockDbConnectionManager;
import com.atlassian.jira.database.ResultRowBuilder;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoContainer;
import com.atlassian.jira.upgrade.DowngradeException;
import com.atlassian.jira.upgrade.UpgradeVersionHistoryManager;
import com.atlassian.jira.util.BuildUtilsInfo;
import com.atlassian.jira.util.MockBuildUtilsInfo;
import org.junit.After;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

/**
 * @since v6.4.6
 */
public class TestDowngradeWorker {
    @Rule
    public MockitoContainer mockitoContainer = new MockitoContainer(this);

    @AvailableInContainer(interfaceClass = DbConnectionManager.class)
    MockDbConnectionManager mockDbConnectionManager = new MockDbConnectionManager();

    @Mock
    private UpgradeVersionHistoryManager upgradeVersionHistoryManager;

    @After
    public void tearDown() {
        mockDbConnectionManager.assertAllExpectedStatementsWereRun();
    }

    @Test
    public void testFindDowngradeTasks() throws DowngradeException {
        mockFullUpgradeHistory();
        List<Integer> downgradeTasks;

        // Test Downgrade to 400
        BuildUtilsInfo buildUtilsInfo = new MockBuildUtilsInfo().withApplicationBuildNumber(400);
        downgradeTasks = new DowngradeWorker(buildUtilsInfo, mockDbConnectionManager, null, null, upgradeVersionHistoryManager).findDowngradeTasksToRun();
        // Nothing to downgrade - no upgrade tasks have run since build 400
        assertThat("No downgrade tasks", downgradeTasks, hasSize(0));

        // Test Downgrade to 371
        buildUtilsInfo = new MockBuildUtilsInfo().withApplicationBuildNumber(371);
        downgradeTasks = new DowngradeWorker(buildUtilsInfo, mockDbConnectionManager, null, null, upgradeVersionHistoryManager).findDowngradeTasksToRun();
        // Nothing to downgrade - all upgrade tasks are reversible with a NO-OP (373 has not actually run yet)
        assertThat("No downgrade tasks", downgradeTasks, hasSize(0));

        // Test Downgrade to 360
        buildUtilsInfo = new MockBuildUtilsInfo().withApplicationBuildNumber(360);
        downgradeTasks = new DowngradeWorker(buildUtilsInfo, mockDbConnectionManager, null, null, upgradeVersionHistoryManager).findDowngradeTasksToRun();
        // Need to reverse 371
        assertThat(downgradeTasks, contains(371));

        // Test Downgrade to 342
        buildUtilsInfo = new MockBuildUtilsInfo().withApplicationBuildNumber(342);
        downgradeTasks = new DowngradeWorker(buildUtilsInfo, mockDbConnectionManager, null, null, upgradeVersionHistoryManager).findDowngradeTasksToRun();
        // Need to reverse 371. 343 is a no-op
        assertThat(downgradeTasks, contains(371));

        // Test Downgrade to 340
        buildUtilsInfo = new MockBuildUtilsInfo().withApplicationBuildNumber(340);
        downgradeTasks = new DowngradeWorker(buildUtilsInfo, mockDbConnectionManager, null, null, upgradeVersionHistoryManager).findDowngradeTasksToRun();
        // Need to reverse 371, 340 does not get reversed
        assertThat(downgradeTasks, contains(371));

        // Test Downgrade to 339
        buildUtilsInfo = new MockBuildUtilsInfo().withApplicationBuildNumber(339);
        downgradeTasks = new DowngradeWorker(buildUtilsInfo, mockDbConnectionManager, null, null, upgradeVersionHistoryManager).findDowngradeTasksToRun();
        // Need to reverse 371, 340
        assertThat(downgradeTasks, contains(371, 340));

        // Test Downgrade to 320
        buildUtilsInfo = new MockBuildUtilsInfo().withApplicationBuildNumber(320);
        downgradeTasks = new DowngradeWorker(buildUtilsInfo, mockDbConnectionManager, null, null, upgradeVersionHistoryManager).findDowngradeTasksToRun();
        // Need to reverse 371, 340
        assertThat(downgradeTasks, contains(371, 340));

        // Test Downgrade to 301
        buildUtilsInfo = new MockBuildUtilsInfo().withApplicationBuildNumber(301);
        downgradeTasks = new DowngradeWorker(buildUtilsInfo, mockDbConnectionManager, null, null, upgradeVersionHistoryManager).findDowngradeTasksToRun();
        // Nothing to downgrade - all upgrade tasks are reversible with a NO-OP
        assertThat(downgradeTasks, contains(371, 340));

        // Test Downgrade to 300
        buildUtilsInfo = new MockBuildUtilsInfo().withApplicationBuildNumber(300);
        downgradeTasks = new DowngradeWorker(buildUtilsInfo, mockDbConnectionManager, null, null, upgradeVersionHistoryManager).findDowngradeTasksToRun();
        // 301 is included because downgradetaskrequired is NULL
        assertThat(downgradeTasks, contains(371, 340, 301));

        // Test Downgrade to 200
        buildUtilsInfo = new MockBuildUtilsInfo().withApplicationBuildNumber(200);
        downgradeTasks = new DowngradeWorker(buildUtilsInfo, mockDbConnectionManager, null, null, upgradeVersionHistoryManager).findDowngradeTasksToRun();
        // 301 + 300 are included because downgradetaskrequired is NULL
        assertThat(downgradeTasks, contains(371, 340, 301, 300));
    }

    @Test
    public void testFindDowngradeTasksFail_InsufficientHistory() throws DowngradeException {
        mockUpgradeHistory(new ResultRowBuilder()
                .addRow(17L, null, "371", "complete", "Y")
                .addRow(18L, null, "372", "complete", "N")
                .addRow(19L, null, "373", "complete", "N"));

        // Test Downgrade to 360
        BuildUtilsInfo buildUtilsInfo = new MockBuildUtilsInfo().withApplicationBuildNumber(360);
        try {
            new DowngradeWorker(buildUtilsInfo, mockDbConnectionManager, null, null, upgradeVersionHistoryManager).findDowngradeTasksToRun();
            fail("DowngradeException expected");
        } catch (DowngradeException ex) {
            assertEquals("Unable to downgrade data to build number 360 because it does not have sufficient upgrade task history.", ex.getMessage());
        }
    }

    @Test
    public void testFindDowngradeTasksUnknownState() throws DowngradeException {
        mockUpgradeHistory(new ResultRowBuilder()
                .addRow(17L, null, "371", "complete", "Y")
                .addRow(18L, null, "372", "started", "Y")
                .addRow(19L, null, "373", "complete", "N"));

        // Test Downgrade to 371
        BuildUtilsInfo buildUtilsInfo = new MockBuildUtilsInfo().withApplicationBuildNumber(371);
        try {
            new DowngradeWorker(buildUtilsInfo, mockDbConnectionManager, null, null, upgradeVersionHistoryManager).findDowngradeTasksToRun();
            fail("DowngradeException expected");
        } catch (DowngradeException ex) {
            assertEquals("Unable to downgrade data because upgrade 372 is in an unknown state 'started'.", ex.getMessage());
        }
    }

    @Test
    public void testFindDowngradeTasksUnknownState_Ignored() throws Exception {
        mockUpgradeHistory(new ResultRowBuilder()
                        .addRow(17L, null, "371", "complete", "Y")
                        .addRow(18L, null, "372", "started", "N")
                        .addRow(19L, null, "373", "complete", "Y")
        );

        // Test Downgrade to 371
        BuildUtilsInfo buildUtilsInfo = new MockBuildUtilsInfo().withApplicationBuildNumber(371);
        final List<Integer> downgradeTasksToRun = new DowngradeWorker(buildUtilsInfo, mockDbConnectionManager, null, null, upgradeVersionHistoryManager).findDowngradeTasksToRun();
        // We just need to run 373: 372 is in an unknown state but we don't care because it does not required downgrade task.
        assertEquals(Collections.singletonList(373), downgradeTasksToRun);
    }

    @Test
    public void testUndoUpgradeHistory() throws Exception {
        mockFullUpgradeHistory();
        mockDeleteHistory(371, 372, 373, 374);
        mockDeleteVersionHistory(371, 373, 374);  // nothing found for 372, so it is skipped
        final ApplicationProperties applicationProperties = mock(ApplicationProperties.class);

        mockDbConnectionManager.setQueryResults("select UPGRADE_VERSION_HISTORY.id, UPGRADE_VERSION_HISTORY.timeperformed, " +
                        "UPGRADE_VERSION_HISTORY.targetbuild, UPGRADE_VERSION_HISTORY.targetversion\n" +
                        "from upgradeversionhistory UPGRADE_VERSION_HISTORY",
                new ResultRowBuilder()
                        .addRow(42L, null, "340", null)
                        .addRow(43L, null, "343", null)
                        .addRow(44L, null, "371", null)
                        .addRow(45L, null, "371", null)
                        .addRow(46L, null, "373", null)
                        .addRow(47L, null, "374", null));


        BuildUtilsInfo buildUtilsInfo = new MockBuildUtilsInfo().withApplicationBuildNumber(371);
        final DowngradeWorker worker = new DowngradeWorker(buildUtilsInfo, mockDbConnectionManager, null, applicationProperties, upgradeVersionHistoryManager);
        worker.undoUpgradeHistory(371);

        verify(applicationProperties).setString(APKeys.JIRA_PATCHED_VERSION, "370");
        verifyNoMoreInteractions(applicationProperties);
    }

    private void mockUpgradeHistory(ResultRowBuilder expectedResults) {
        mockDbConnectionManager.setQueryResults(
                "select UPGRADE_HISTORY.id, UPGRADE_HISTORY.upgradeclass, UPGRADE_HISTORY.targetbuild, UPGRADE_HISTORY.status, UPGRADE_HISTORY.downgradetaskrequired\n"
                        + "from upgradehistory UPGRADE_HISTORY",
                expectedResults);
    }

    private void mockFullUpgradeHistory() {
        mockUpgradeHistory(new ResultRowBuilder()
                // Really old data has NULL for targetbuild
                .addRow(1L, null, null, "complete", null)
                .addRow(11L, null, "300", "complete", null)
                .addRow(12L, null, "301", "complete", null)
                .addRow(13L, null, "302", "complete", "N")
                .addRow(14L, null, "340", "complete", "Y")
                .addRow(15L, null, "342", "complete", "N")
                .addRow(16L, null, "343", "complete", "N")
                .addRow(17L, null, "371", "complete", "Y")
                .addRow(18L, null, "372", "complete", "N")
                .addRow(19L, null, "373", "pending", "Y")
                .addRow(20L, null, "374", "complete", "N"));
    }

    private void mockDeleteHistory(long... buildNumbers) {
        Arrays.stream(buildNumbers).forEach(buildNumber -> mockDbConnectionManager.setUpdateResults(
                "delete from upgradehistory\n" +
                        "where upgradehistory.targetbuild = '" + buildNumber + '\'',
                1));
    }

    private void mockDeleteVersionHistory(long... buildNumbers) {
        Arrays.stream(buildNumbers).forEach(buildNumber -> mockDbConnectionManager.setUpdateResults(
                "delete from upgradeversionhistory\n" +
                        "where upgradeversionhistory.targetbuild = '" + buildNumber + '\'',
                1));
    }
}
