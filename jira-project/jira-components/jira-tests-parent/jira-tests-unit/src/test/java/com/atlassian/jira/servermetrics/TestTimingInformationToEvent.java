package com.atlassian.jira.servermetrics;

import com.google.common.collect.ImmutableList;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import javax.servlet.http.HttpServletRequest;
import java.time.Duration;
import java.util.Optional;

import static com.atlassian.jira.matchers.FeatureMatchers.hasFeature;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.nullValue;
import static org.mockito.Mockito.when;

public class TestTimingInformationToEvent {

    private static final String CORRELATION_ID = "correlation-id";
    private static final long SAMPLE_REQ_TIME = 11L;
    private static final long SAMPLE_USER_TIME = 12L;
    private static final long SAMPLE_CPU_TIME = 13L;
    private static final long SAMPLE_GC_COUNT = 15L;
    private static final long SAMPLE_GC_DURATION = 14L;

    @Rule
    public MockitoRule mocks = MockitoJUnit.rule();
    @Mock
    private HttpServletRequest request;

    private final TimingInformationToEvent testObj = new TimingInformationToEvent();

    @Test
    public void shouldTransformTimingToEvent() {
        when(request.getServletPath()).thenReturn("/servlet");
        when(request.getPathInfo()).thenReturn("pathInfo");
        when(request.getAttribute(TimingInformationToEvent.B3_TRACE_ID)).thenReturn(CORRELATION_ID);
        final TimingInformation sampleTimingInformation = new TimingInformation(
                ImmutableList.of(
                        new CheckpointTiming("checkpoint1", Duration.ofMillis(1L)),
                        new CheckpointTiming("checkpoint2", Duration.ofMillis(10L))
                ),
                ImmutableList.of(
                        new CheckpointTiming("activity1", Duration.ofMillis(2L)),
                        new CheckpointTiming("activity2", Duration.ofMillis(20L))
                ),
                Duration.ofMillis(SAMPLE_REQ_TIME),
                Duration.ofMillis(SAMPLE_USER_TIME),
                Duration.ofMillis(SAMPLE_CPU_TIME),
                Duration.ofMillis(SAMPLE_GC_DURATION),
                SAMPLE_GC_COUNT
        );
        final RequestMetricsEvent statEvent = testObj.createStatEvent(sampleTimingInformation, request, Optional.empty());

        Assert.assertThat(statEvent,
                hasFeature(
                        RequestMetricsEvent::getKey,
                        nullValue(),
                        "key", "key"));
        Assert.assertThat(statEvent,
                hasFeature(
                        RequestMetricsEvent::getRequestCorrelationId,
                        equalTo(CORRELATION_ID),
                        "requestCorrelationId", "requestCorrelationId"));
        Assert.assertThat(statEvent,
                hasFeature(
                        RequestMetricsEvent::getReqTime,
                        equalTo(SAMPLE_REQ_TIME),
                        "getReqTime", "getReqTime"));
        Assert.assertThat(statEvent,
                hasFeature(
                        RequestMetricsEvent::getRequestUserTime,
                        equalTo(SAMPLE_USER_TIME),
                        "getRequestUserTime", "getRequestUserTime"));
        Assert.assertThat(statEvent,
                hasFeature(
                        RequestMetricsEvent::getRequestCpuTime,
                        equalTo(SAMPLE_CPU_TIME),
                        "getRequestCpuTime", "getRequestCpuTime"));
        Assert.assertThat(statEvent,
                hasFeature(
                        RequestMetricsEvent::getRequestGCCount,
                        equalTo(SAMPLE_GC_COUNT),
                        "getRequestGCCount", "getRequestGCCount"));
        Assert.assertThat(statEvent,
                hasFeature(
                        RequestMetricsEvent::getRequestGCDuration,
                        equalTo(SAMPLE_GC_DURATION),
                        "getRequestGCDuration", "getRequestGCDuration"));
        Assert.assertThat(statEvent,
                hasFeature(
                        RequestMetricsEvent::getTimingEventKeys,
                        equalTo("checkpoint1,checkpoint2"),
                        "getTimingEventKeys", "getTimingEventKeys"));
        Assert.assertThat(statEvent,
                hasFeature(
                        RequestMetricsEvent::getTimingEventMillis,
                        equalTo("1,10"),
                        "getTimingEventMillis", "getTimingEventMillis"));
        Assert.assertThat(statEvent,
                hasFeature(
                        RequestMetricsEvent::getTimingEvents,
                        allOf(hasEntry("checkpoint1", 1L), hasEntry("checkpoint2", 10L)),
                        "getTimingEvents", "getTimingEvents"));
        Assert.assertThat(statEvent,
                hasFeature(
                        RequestMetricsEvent::getActivityTimings, Matchers.allOf(
                        hasEntry("activity1", 2L),
                        hasEntry("activity2", 20L)), "getTimingEvents", "getTimingEvents"));
        Assert.assertThat(statEvent,
                hasFeature(
                        RequestMetricsEvent::getUrl,
                        equalTo("/servlet/pathInfo"),
                        "getTimingEventMillis", "getTimingEventMillis"));
    }
}