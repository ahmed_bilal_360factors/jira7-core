package com.atlassian.jira.board;

import com.atlassian.jira.bc.ServiceOutcome;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.bc.project.ProjectAction;
import com.atlassian.jira.bc.project.ProjectService;
import com.atlassian.jira.bc.project.ProjectService.GetProjectResult;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.matchers.ErrorCollectionMatcher;
import com.atlassian.jira.mock.component.MockComponentWorker;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.ofbiz.OfBizFactory;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.ErrorCollections;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.ListOrderedMessageSetImpl;
import com.atlassian.jira.util.MessageSet;
import com.atlassian.jira.web.bean.MockI18nBean;
import com.atlassian.query.Query;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;
import java.util.Optional;

import static com.google.common.collect.Lists.newArrayList;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestBoardServiceImpl {
    private static final ApplicationUser USER_CAN_NOT_EDIT_PROJECT = new MockApplicationUser("nonAdmin");
    private static final ApplicationUser USER_CAN_EDIT_PROJECT = new MockApplicationUser("admin");
    private static final ApplicationUser USER_CAN_CREATE_SHARED_OBJECTS = new MockApplicationUser("admin");
    private static final ApplicationUser USER_CAN_NOT_CREATE_SHARED_OBJECTS = new MockApplicationUser("Jerry");
    private static final ApplicationUser USER_CAN_VIEW_PROJECT = new MockApplicationUser("john");
    private static final ApplicationUser USER_CAN_NOT_VIEW_PROJECT = new MockApplicationUser("Jerry");
    private static final ApplicationUser USER_CAN_VIEW_AT_LEAST_ONE_PROJECT = new MockApplicationUser("Alex");
    private static final ApplicationUser USER_CAN_NOT_VIEW_ANY_PROJECT = new MockApplicationUser("ben");
    private static final String NON_PARSABLE_JQL = "non parsable jql";
    private static final String INVALID_JQL = "issueType=nonexist";
    private static final String VALID_JQL = "valid jql";
    private static final Long PROJECT_ID = 1l;
    private static final String NON_PARSABLE_JQL_MSG = "non parsable jql";
    private static final String NO_PROJECT_PERMISSION_MSG = "no required project permission";
    private static final String INVALID_JQL_MSG = "Your jql is invalid";
    private static final BoardId BOARD_ID = new BoardId(1l);
    private static final BoardId NONEXISTENT_BOARD_ID = new BoardId(3l);
    public static final String PROJECT_JQL = "project=1";

    @Mock
    private BoardManager boardManager;
    private Board expectedBoard;
    @Mock
    private GlobalPermissionManager globalPermissionManager;
    private I18nHelper.BeanFactory i18nFactory = new MockI18nBean.MockI18nBeanFactory();
    @Mock
    private SearchService searchService;
    @Mock
    private Query query;
    @Mock
    private ProjectService projectService;
    @Mock
    private GetProjectResult validProjectResult;
    @Mock
    private GetProjectResult invalidProjectResult;
    private List<Board> expectedBoards;
    @Mock
    private PermissionManager permissionManager;

    private BoardServiceImpl boardService;

    @Before
    public void setUp() {
        boardService = new BoardServiceImpl(boardManager, globalPermissionManager, i18nFactory, searchService, projectService, permissionManager);
        setupSearchService();
        setUpGlobalPermission();
        setupProjectPermission();
        setupBoardManager();
    }

    @Test
    public void userWithoutEditProjectPermissionShouldGetErrorWhenCreatingDefaultBoard() {
        ServiceOutcome<Board> board = boardService.createDefaultBoardForProject(USER_CAN_NOT_EDIT_PROJECT, PROJECT_ID);

        assertThat(board.getErrorCollection().getErrorMessages().contains(NO_PROJECT_PERMISSION_MSG), is(true));
        assertNull(board.get());
    }

    @Test
    public void shouldGetErrorIfJqlParseFailsWhenCreatingDefaultBoard() {
        mockJqlParseFailWithMessage(USER_CAN_EDIT_PROJECT, PROJECT_JQL, NON_PARSABLE_JQL_MSG);

        ServiceOutcome<Board> board = boardService.createDefaultBoardForProject(USER_CAN_EDIT_PROJECT, PROJECT_ID);

        assertThat(board.getErrorCollection().getErrorMessages().contains(NON_PARSABLE_JQL_MSG), is(true));
        assertNull(board.get());
    }

    @Test
    public void shouldNotValidateJqlWhenCreatingDefaultBoard() {
        mockJqlParsePass(USER_CAN_EDIT_PROJECT, PROJECT_JQL);
        mockJqlValidateFail(USER_CAN_EDIT_PROJECT, INVALID_JQL_MSG);

        ServiceOutcome<Board> board = boardService.createDefaultBoardForProject(USER_CAN_CREATE_SHARED_OBJECTS, PROJECT_ID);

        assertThat(board.getErrorCollection().hasAnyErrors(), is(false));
        assertEquals(board.get(), expectedBoard);
    }

    @Test
    public void defaultBoardShouldBeCreatedProperlyGivenRightPermission() {
        mockJqlParsePass(USER_CAN_EDIT_PROJECT, PROJECT_JQL);
        mockJqlValidatePass(USER_CAN_EDIT_PROJECT);

        ServiceOutcome<Board> board = boardService.createDefaultBoardForProject(USER_CAN_EDIT_PROJECT, PROJECT_ID);

        assertThat(board.getErrorCollection().hasAnyErrors(), is(false));
        assertEquals(board.get(), expectedBoard);
    }

    @Test
    public void userWithoutCreateSharedObjectPermissionShouldGetErrorWhenCreatingBoard() {
        ServiceOutcome<Board> board = boardService.createBoard(USER_CAN_NOT_CREATE_SHARED_OBJECTS, boardCreateData(VALID_JQL));

        assertThat(board.getErrorCollection().getErrorMessages().contains("You must have create shared objects rights in order to perform this operation."), is(true));
    }

    @Test
    public void shouldGetErrorIfJqlParseFailsWhenCreatingBoard() {
        mockJqlParseFailWithMessage(USER_CAN_CREATE_SHARED_OBJECTS, NON_PARSABLE_JQL, NON_PARSABLE_JQL_MSG);

        ServiceOutcome<Board> board = boardService.createBoard(USER_CAN_CREATE_SHARED_OBJECTS, boardCreateData(NON_PARSABLE_JQL));

        assertThat(board.getErrorCollection().getErrorMessages().contains(NON_PARSABLE_JQL_MSG), is(true));
        assertNull(board.get());
    }

    @Test
    public void shouldGetErrorIfQueryValidationFailsWhenCreatingBoard() {
        mockJqlParsePass(USER_CAN_CREATE_SHARED_OBJECTS, INVALID_JQL);
        mockJqlValidateFail(USER_CAN_CREATE_SHARED_OBJECTS, INVALID_JQL_MSG);

        ServiceOutcome<Board> board = boardService.createBoard(USER_CAN_CREATE_SHARED_OBJECTS, boardCreateData(INVALID_JQL));

        assertThat(board.getErrorCollection().getErrorMessages().contains(INVALID_JQL_MSG), is(true));
        assertNull(board.get());
    }

    @Test
    public void boardShouldBeCreatedProperlyIfUserHasRightPermissionAndJqlIsValid() {

        mockJqlParsePass(USER_CAN_CREATE_SHARED_OBJECTS, VALID_JQL);
        mockJqlValidatePass(USER_CAN_CREATE_SHARED_OBJECTS);

        ServiceOutcome<Board> board = boardService.createBoard(USER_CAN_CREATE_SHARED_OBJECTS, boardCreateData(VALID_JQL));

        assertThat(board.getErrorCollection().hasAnyErrors(), is(false));
        assertEquals(board.get(), expectedBoard);
    }

    @Test
    public void userWithViewProjectPermissionCanGetBoardForProject() {
        ServiceOutcome<List<Board>> boards = boardService.getBoardsForProject(USER_CAN_VIEW_PROJECT, PROJECT_ID);

        assertThat(boards.getErrorCollection().hasAnyErrors(), is(false));
        assertEquals(boards.get(), expectedBoards);
    }

    @Test
    public void userWithoutViewProjectPermissionCanNotGetBoardForProject() {
        ServiceOutcome<List<Board>> boards = boardService.getBoardsForProject(USER_CAN_NOT_VIEW_PROJECT, PROJECT_ID);

        assertThat(boards.getErrorCollection().getErrorMessages().contains(NO_PROJECT_PERMISSION_MSG), is(true));
        assertNull(boards.get());
    }

    @Test
    public void userShouldHavePermissionToViewAtLeastOneProjectCanGetBoard() {
        ServiceOutcome<Board> board = boardService.getBoard(USER_CAN_VIEW_AT_LEAST_ONE_PROJECT, BOARD_ID);

        assertThat(board.getErrorCollection().hasAnyErrors(), is(false));
        assertEquals(board.get(), expectedBoard);
    }

    @Test
    public void userWithoutPermissionToViewAtLeastOneProjectShouldGetErrorWhenGetBoard() {
        ServiceOutcome<Board> board = boardService.getBoard(USER_CAN_NOT_VIEW_ANY_PROJECT, BOARD_ID);

        assertThat(board.getErrorCollection().getErrorMessages().contains("You don't have permission to view this board."), is(true));
        assertNull(board.get());
    }


    @Test
    public void shouldReturnCorrectErrorMessageWhenBoardCannotBeFound() {
        ServiceOutcome<Board> board = boardService.getBoard(USER_CAN_VIEW_AT_LEAST_ONE_PROJECT, NONEXISTENT_BOARD_ID);

        assertThat(board.getErrorCollection().getErrorMessages().contains("A board with id '3' could not be found."), is(true));
        assertNull(board.get());
    }

    @Test
    public void userWithCreateSharedObjectPermissionCanDeleteBoard() {
        ServiceOutcome<Boolean> deleted = boardService.deleteBoard(USER_CAN_CREATE_SHARED_OBJECTS, BOARD_ID);

        assertThat(deleted.getErrorCollection().hasAnyErrors(), is(false));
        assertTrue(deleted.get());
    }

    @Test
    public void userWithoutCreateSharedObjectPermissionCanNotDeleteBoard() {
        ServiceOutcome<Boolean> deleted = boardService.deleteBoard(USER_CAN_NOT_CREATE_SHARED_OBJECTS, BOARD_ID);

        assertThat(deleted.getErrorCollection().getErrorMessages().contains("You must have create shared objects rights in order to perform this operation."), is(true));
        assertNull(deleted.get());
    }

    @Test
    public void userWithoutPermissionToViewProjectCannotSeeIfItHasAnyBoards() {
        ServiceOutcome<Boolean> hasBoard = boardService.hasBoardInProject(USER_CAN_NOT_VIEW_PROJECT, PROJECT_ID);

        assertThat(hasBoard.isValid(), is(false));
        assertThat(hasBoard.getErrorCollection(), ErrorCollectionMatcher.hasErrorMessage(NO_PROJECT_PERMISSION_MSG));
    }

    @Test
    public void userWithPermissionToViewProjectCanSeeIfItHasAnyBoards() {
        ServiceOutcome<Boolean> hasBoard = boardService.hasBoardInProject(USER_CAN_VIEW_PROJECT, PROJECT_ID);

        assertThat(hasBoard.isValid(), is(true));
        assertThat(hasBoard.get(), is(false));
    }

    private void mockJqlValidateFail(ApplicationUser user, String message) {
        when(searchService.validateQuery(user, query)).thenReturn(messageSet(message));
    }

    private void mockJqlValidatePass(ApplicationUser user) {
        when(searchService.validateQuery(user, query)).thenReturn(messageSet(null));
    }

    private void mockJqlParseFailWithMessage(ApplicationUser user, String jql, String message) {
        when(searchService.parseQuery(user, jql)).thenReturn(parseResult(null, message));
    }

    private void mockJqlParsePass(ApplicationUser user, String jql) {
        when(searchService.parseQuery(user, jql)).thenReturn(parseResult(query, null));
    }

    private BoardCreationData boardCreateData(String jql) {
        return new BoardCreationData.Builder().jql(jql).build();
    }

    private SearchService.ParseResult parseResult(Query query, String error) {
        return new SearchService.ParseResult(query, messageSet(error));
    }

    private MessageSet messageSet(String message) {
        MessageSet validateMessage = new ListOrderedMessageSetImpl();
        if (message != null) {
            validateMessage.addErrorMessage(message);
        }
        return validateMessage;
    }

    private void setupSearchService() {
        when(searchService.getJqlString(any(Query.class))).thenReturn(PROJECT_JQL);
        final MockComponentWorker componentAccessorWorker = new MockComponentWorker();
        componentAccessorWorker.registerMock(OfBizDelegator.class, OfBizFactory.getOfBizDelegator());
        ComponentAccessor.initialiseWorker(componentAccessorWorker);
    }

    private void setUpGlobalPermission() {
        when(globalPermissionManager.hasPermission(GlobalPermissionKey.CREATE_SHARED_OBJECTS, USER_CAN_CREATE_SHARED_OBJECTS)).thenReturn(true);
        when(globalPermissionManager.hasPermission(GlobalPermissionKey.CREATE_SHARED_OBJECTS, USER_CAN_NOT_CREATE_SHARED_OBJECTS)).thenReturn(false);
    }

    private void setupProjectPermission() {
        when(validProjectResult.isValid()).thenReturn(true);
        when(invalidProjectResult.isValid()).thenReturn(false);
        when(invalidProjectResult.getErrorCollection()).thenReturn(ErrorCollections.create(NO_PROJECT_PERMISSION_MSG, ErrorCollection.Reason.FORBIDDEN));

        when(projectService.getProjectByIdForAction(USER_CAN_EDIT_PROJECT, PROJECT_ID, ProjectAction.EDIT_PROJECT_CONFIG)).thenReturn(validProjectResult);
        when(projectService.getProjectByIdForAction(USER_CAN_NOT_EDIT_PROJECT, PROJECT_ID, ProjectAction.EDIT_PROJECT_CONFIG)).thenReturn(invalidProjectResult);


        when(projectService.getProjectByIdForAction(USER_CAN_VIEW_PROJECT, PROJECT_ID, ProjectAction.VIEW_PROJECT)).thenReturn(validProjectResult);
        when(projectService.getProjectByIdForAction(USER_CAN_NOT_VIEW_PROJECT, PROJECT_ID, ProjectAction.VIEW_PROJECT)).thenReturn(invalidProjectResult);

        when(permissionManager.hasProjects(ProjectPermissions.BROWSE_PROJECTS, USER_CAN_VIEW_AT_LEAST_ONE_PROJECT)).thenReturn(true);
    }

    private void setupBoardManager() {
        expectedBoard = new Board(new BoardId(1l), VALID_JQL);
        when(boardManager.createBoard(any(BoardCreationData.class))).thenReturn(expectedBoard);
        expectedBoards = newArrayList(expectedBoard);
        when(boardManager.getBoardsForProject(PROJECT_ID)).thenReturn(expectedBoards);
        when(boardManager.deleteBoard(BOARD_ID)).thenReturn(true);
        when(boardManager.getBoard(BOARD_ID)).thenReturn(Optional.of(expectedBoard));
        when(boardManager.getBoard(NONEXISTENT_BOARD_ID)).thenReturn(Optional.empty());
    }
}
