package com.atlassian.jira.plugin.icon;

import com.atlassian.jira.icon.IconType;
import com.atlassian.plugin.PluginAccessor;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertNotNull;

@RunWith(MockitoJUnitRunner.class)
public class TestIconTypeFactory {
    private static final String TEST_TYPE_NAME1 = "typeName1";
    private static final String TEST_TYPE_NAME2 = "typeName2";
    private static final String TEST_FILE_NAME1 = "ben.svg";
    private static final String TEST_FILE_NAME2 = "santa.svg";
    private static final String UNKNOWN_TYPE_NAME = "unknownName";

    @Mock
    private PluginAccessor pluginAccessor;

    @Autowired
    private IconTypeDefinitionFactory iconTypeFactory;

    @Before
    public void before() {
        iconTypeFactory = new IconTypeDefinitionFactoryImpl(pluginAccessor);
    }

    private List<IconTypeModuleDescriptor> getGoodList() {
        IconTypeDefinition iconType1 = mock(IconTypeDefinition.class);
        when(iconType1.getKey()).thenReturn(TEST_TYPE_NAME1);

        IconTypeModuleDescriptor iconTypeModuleDescriptor1 = mock(IconTypeModuleDescriptor.class);
        when(iconTypeModuleDescriptor1.getModule()).thenReturn(iconType1);
        when(iconTypeModuleDescriptor1.getDefaultFilename()).thenReturn(TEST_FILE_NAME1);

        IconTypeDefinition iconType2 = mock(IconTypeDefinition.class);
        when(iconType2.getKey()).thenReturn(TEST_TYPE_NAME2);

        IconTypeModuleDescriptor iconTypeModuleDescriptor2 = mock(IconTypeModuleDescriptor.class);
        when(iconTypeModuleDescriptor2.getModule()).thenReturn(iconType2);
        when(iconTypeModuleDescriptor2.getDefaultFilename()).thenReturn(TEST_FILE_NAME2);

        List<IconTypeModuleDescriptor> iconTypeModuleDescriptorList = new ArrayList<IconTypeModuleDescriptor>();
        iconTypeModuleDescriptorList.add(iconTypeModuleDescriptor1);
        iconTypeModuleDescriptorList.add(iconTypeModuleDescriptor2);

        return iconTypeModuleDescriptorList;
    }

    private void setupGoodIconTypeModuleDescriptorList() {
        List<IconTypeModuleDescriptor> iconTypeModuleDescriptorList = getGoodList();
        when(pluginAccessor.getEnabledModuleDescriptorsByClass(IconTypeModuleDescriptor.class)).thenReturn(iconTypeModuleDescriptorList);
    }

    @Test
    public void testGetDefinition() {
        setupGoodIconTypeModuleDescriptorList();

        IconTypeDefinition definition = iconTypeFactory.getDefinition(IconType.of(TEST_TYPE_NAME1));
        assertEquals(TEST_TYPE_NAME1, definition.getKey());

        definition = iconTypeFactory.getDefinition(IconType.of(TEST_TYPE_NAME2));
        assertEquals(TEST_TYPE_NAME2, definition.getKey());

        IconTypeDefinition bad = iconTypeFactory.getDefinition(IconType.of(UNKNOWN_TYPE_NAME));
        assertNull(bad);
    }

    @Test
    public void testGetDefaultSystemIconFilename() {
        setupGoodIconTypeModuleDescriptorList();

        String filename = iconTypeFactory.getDefaultSystemIconFilename(IconType.of(TEST_TYPE_NAME1));
        assertEquals(TEST_FILE_NAME1, filename);

        filename = iconTypeFactory.getDefaultSystemIconFilename(IconType.of(TEST_TYPE_NAME2));
        assertEquals(TEST_FILE_NAME2, filename);

    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetDefaultSystemIconFilenameInvalid() {
        setupGoodIconTypeModuleDescriptorList();

        String filename = iconTypeFactory.getDefaultSystemIconFilename(IconType.of(UNKNOWN_TYPE_NAME));
    }
}
