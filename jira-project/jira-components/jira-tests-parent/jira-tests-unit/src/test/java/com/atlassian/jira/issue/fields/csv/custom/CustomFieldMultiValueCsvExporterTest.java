package com.atlassian.jira.issue.fields.csv.custom;

import com.atlassian.jira.issue.customfields.impl.LabelsCFType;

/**
 * This represents a Custom Field Type which exports multiple values. E.g. the {@link LabelsCFType} can export multiple
 * values for the different labels. If the label retrieved is null it should return an empty list for the values of this.
 * @param <T>
 */
public abstract class CustomFieldMultiValueCsvExporterTest<T> extends CustomFieldCsvExporterTest<T> {
    @Override
    public void testExportWithNullValue() {
        whenFieldValueIs(null);
        assertExportedValue();
    }
}
