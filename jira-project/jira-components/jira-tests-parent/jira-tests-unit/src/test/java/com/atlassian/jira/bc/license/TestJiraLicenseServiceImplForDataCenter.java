package com.atlassian.jira.bc.license;

import com.atlassian.application.api.Application;
import com.atlassian.application.api.ApplicationKey;
import com.atlassian.application.api.ApplicationManager;
import com.atlassian.fugue.Option;
import com.atlassian.jira.cluster.ClusterManager;
import com.atlassian.jira.help.MockHelpUrl;
import com.atlassian.jira.help.MockHelpUrls;
import com.atlassian.jira.license.JiraLicenseManager;
import com.atlassian.jira.license.LicenseDetails;
import com.atlassian.jira.license.MockLicenseDetailsBuilder;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.NoopI18nHelper;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.HashSet;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.when;

/**
 * Tests {@link com.atlassian.jira.bc.license.JiraLicenseServiceImpl}.
 */
@RunWith(MockitoJUnitRunner.class)
public class TestJiraLicenseServiceImplForDataCenter {
    @Mock
    private JiraLicenseManager licenseManager;
    @Mock
    private ClusterManager clusterManager;
    @Mock
    private ApplicationManager applicationManager;
    @Mock
    private UserManager userManager;

    private I18nHelper i18nHelper = new NoopI18nHelper();

    private JiraLicenseServiceImpl licenseService;
    private MockApplicationRoleTestHelper mockLicHelper;
    private MockHelpUrls helpUrls = new MockHelpUrls();
    private MockHelpUrl mockHelpUrl = new MockHelpUrl();

    @Before
    public void setup() {
        mockLicHelper = new MockApplicationRoleTestHelper();
        licenseService = new JiraLicenseServiceImpl(licenseManager, clusterManager, applicationManager, userManager, helpUrls);
        when(licenseManager.getLicenses()).thenReturn(Sets.<LicenseDetails>newHashSet());

        mockHelpUrl.setUrl("test-url");
        mockHelpUrl.setKey("license.compatibility");
        helpUrls.addUrl(mockHelpUrl);
    }

    private static final boolean DATA_CENTER = true;
    private static final boolean NON_DATA_CENTER = false;

    //  Data Center lic in NON Data Center instance, admin will configure data center later.
    //
    //  JIRA Instance:
    //  -- Data Center has not been configured.
    //  -- Only JIRA Software Application installed.
    //
    //  Action:
    //  -- Admin replaces existing JIRA Software Data Centre License
    //     with a new JIRA Software Data Centre License.
    //
    //  Summary:
    //  -- Data Center license replaces Data Center license non Data Center instance
    @Test
    public void shouldAllowDataCenterLicenseInNonDataCenterInstanceWhenSingleApp() {
        //Given
        configuredAsDataCenterInstance(false);
        installApplication(mockLicHelper.SOFTWARE_APPLICATION);
        licenseApplication(newLicense("SOFT_DC", DATA_CENTER, mockLicHelper.SOFTWARE_APP_KEY));

        LicenseDetails anotherSoftwareDataCenterLicense = newLicense("SOFT_DC_2", DATA_CENTER,
                mockLicHelper.SOFTWARE_APP_KEY);
        when(licenseManager.getLicense("SOFT_DC_2")).thenReturn(anotherSoftwareDataCenterLicense);

        //When
        final JiraLicenseService.ValidationResult validationResultClustered =
                licenseService.validateApplicationLicense(i18nHelper, "SOFT_DC_2");

        assertValidationResultContainsNoError(validationResultClustered);
    }

    //  License renewal:
    //  License is being replaced.
    //
    //  JIRA Instance:
    //  -- Configured as Data Center instance.
    //  -- Only JIRA Software Application installed.
    //
    //  Action:
    //  -- Admin replaces existing JIRA Software Data Centre License
    //     with a new JIRA Software Data Centre License.
    //
    //  Summary:
    //  -- Data Center license replaces Data Center license Data Center instance.
    @Test
    public void shouldAllowRenewal() {
        //Given
        configuredAsDataCenterInstance(true);
        installApplication(mockLicHelper.SOFTWARE_APPLICATION);
        licenseApplication(newLicense("SOFT_DC", DATA_CENTER, mockLicHelper.SOFTWARE_APP_KEY));

        LicenseDetails anotherSoftwareDataCenterLicense = newLicense("SOFT_DC_2", DATA_CENTER,
                mockLicHelper.SOFTWARE_APP_KEY);
        when(licenseManager.getLicense("SOFT_DC_2")).thenReturn(anotherSoftwareDataCenterLicense);

        //When
        final JiraLicenseService.ValidationResult validationResultClustered =
                licenseService.validateApplicationLicense(i18nHelper, "SOFT_DC_2");

        assertValidationResultContainsNoError(validationResultClustered);
    }

    //  Additional Application Data Center License:
    //  Customer installed other JIRA Application on Data Center instance.
    //
    //  JIRA Instance:
    //  -- Configured as Data Center instance.
    //  -- Has JIRA Software Application installed and Data Center license.
    //  -- Has JIRA Portfolio Application installed.
    //
    //  Action:
    //  -- Admin add JIRA Portfolio Data Centre License.
    //
    //  Summary:
    //  -- Has Data Center license adds Data Center license in Data Center instance.
    @Test
    public void shouldAllowAdditionalLicense() {
        //Given
        configuredAsDataCenterInstance(true);
        installApplication(mockLicHelper.SOFTWARE_APPLICATION);
        licenseApplication(newLicense("SOFT_DC", DATA_CENTER, mockLicHelper.SOFTWARE_APP_KEY));
        installApplication(mockLicHelper.PORTFOLIO_APPLICATION);

        LicenseDetails portfolioDataCenterLicense = newLicense("PORT_DC", DATA_CENTER,
                mockLicHelper.PORTFOLIO_APP_KEY);
        when(licenseManager.getLicense("PORT_DC")).thenReturn(portfolioDataCenterLicense);

        //When
        final JiraLicenseService.ValidationResult validationResultClustered =
                licenseService.validateApplicationLicense(i18nHelper, "PORT_DC");

        assertValidationResultContainsNoError(validationResultClustered);
    }

    //  Downgrade instance and license:
    //
    //  JIRA Instance:
    //  -- Configured as Data Center instance.
    //  -- Only JIRA Software Application installed
    //
    //  Action:
    //  -- Admin un configure instance as Data Center instance.
    //  -- Customer replaces their existing JIRA Software Data Centre License
    //     with a new JIRA Software License that is not for Data Centre.
    //
    //  Summary:
    //  -- Non Data Center license replaces Data Center license in non Data Center instance
    @Test
    public void shouldAllowDowngrade() {
        //Given
        configuredAsDataCenterInstance(true);
        installApplication(mockLicHelper.SOFTWARE_APPLICATION);
        licenseApplication(newLicense("SOFT_DC", DATA_CENTER, mockLicHelper.SOFTWARE_APP_KEY));

        LicenseDetails nonDataCenterSoftwareLicense = newLicense("SOFT_NDC_2", NON_DATA_CENTER,
                mockLicHelper.SOFTWARE_APP_KEY);
        when(licenseManager.getLicense("SOFT_NDC_2")).thenReturn(nonDataCenterSoftwareLicense);

        //When
        configuredAsDataCenterInstance(false);
        final JiraLicenseService.ValidationResult validationResultNotClustered =
                licenseService.validateApplicationLicense(i18nHelper, "SOFT_NDC_2");

        //Then:
        assertValidationResultContainsNoError(validationResultNotClustered);
    }

    //  Additional Application License of different type:
    //  Customer purchased wrong license.
    //
    //  JIRA Instance:
    //  -- Configured as Data Center instance.
    //  -- Has JIRA Software Application installed and Data Center license.
    //  -- Has JIRA Portfolio Application installed.
    //
    //  Action:
    //  -- Admin add JIRA Portfolio License (NOT a Data Center license).
    //
    //  Summary:
    //  -- Has Data Center license adds non Data Center license
    //  -- Data Center instance = FAIL - Data Center expected
    //  -- Non Data Center instance = FAIL -  Data Center mix
    @Test
    public void shouldNotAllowMixingOfNonDataCenterLicenseInDataCenterInstance() {
        //Given
        configuredAsDataCenterInstance(true);
        installApplication(mockLicHelper.SOFTWARE_APPLICATION);
        licenseApplication(newLicense("SOFT_DC", DATA_CENTER, mockLicHelper.SOFTWARE_APP_KEY));
        installApplication(mockLicHelper.PORTFOLIO_APPLICATION);

        LicenseDetails nonDataCenterPortfolioLicense = newLicense("PORT_NDC", NON_DATA_CENTER,
                mockLicHelper.PORTFOLIO_APP_KEY);
        when(licenseManager.getLicense("PORT_NDC")).thenReturn(nonDataCenterPortfolioLicense);

        //When
        final JiraLicenseService.ValidationResult validationResultClustered =
                licenseService.validateApplicationLicense(i18nHelper, "PORT_NDC");

        assertValidationResultContainsSingleError(validationResultClustered, "jira.license.validation.not.datacenter{[<a href=\"test-url\">, </a>]}");

        configuredAsDataCenterInstance(false);
        final JiraLicenseService.ValidationResult validationResultNonClustered =
                licenseService.validateApplicationLicense(i18nHelper, "PORT_NDC");

        assertValidationResultContainsSingleError(validationResultNonClustered, "jira.license.validation.datacenter.mix{[<a href=\"test-url\">, </a>]}");
    }

    //  Additional Application License of different type:
    //  Customer purchased wrong license.
    //
    //  JIRA Instance:
    //  -- Not configured as Data Center instance.
    //  -- Has JIRA Software Application installed and NON Data Center license.
    //  -- Has JIRA Portfolio Application installed.
    //
    //  Action:
    //  -- Admin add JIRA Portfolio Data Center License.
    //
    //  Summary:
    //  -- Has non Data Center license adds Data Center license
    //  -- Data Center instance = FAIL - Data Center mix
    //  -- Non Data Center instance = FAIL - Data Center mix
    @Test
    public void shouldNotAllowMixingOfDataCenterLicenseInNonDataCenterInstance() {
        //Given
        configuredAsDataCenterInstance(false);
        installApplication(mockLicHelper.SOFTWARE_APPLICATION);
        licenseApplication(newLicense("SOFT_NDC", NON_DATA_CENTER, mockLicHelper.SOFTWARE_APP_KEY));
        installApplication(mockLicHelper.PORTFOLIO_APPLICATION);

        LicenseDetails dataCenterPortfolioLicense = newLicense("PORT_DC", DATA_CENTER,
                mockLicHelper.PORTFOLIO_APP_KEY);
        when(licenseManager.getLicense("PORT_DC")).thenReturn(dataCenterPortfolioLicense);

        configuredAsDataCenterInstance(false);
        final JiraLicenseService.ValidationResult validationResultNonClustered =
                licenseService.validateApplicationLicense(i18nHelper, "PORT_DC");

        assertValidationResultContainsSingleError(validationResultNonClustered, "jira.license.validation.datacenter.mix{[<a href=\"test-url\">, </a>]}");

        configuredAsDataCenterInstance(true);
        //When
        final JiraLicenseService.ValidationResult validationResultClustered =
                licenseService.validateApplicationLicense(i18nHelper, "PORT_DC");

        //Admin should first go and remove non Data Center license then add Data Center license
        assertValidationResultContainsSingleError(validationResultClustered, "jira.license.validation.not.datacenter.mix{[<a href=\"test-url\">, </a>]}");
    }

    //  Progressive upgrade:
    //
    //  JIRA Instance:
    //  -- Not configured as Data Center instance.
    //  -- Has JIRA Software Application installed and NON Data Center license.
    //
    //  Action:
    //  -- Admin replaces JIRA Software license with JIRA Software Data Center license
    //
    //  Summary:
    //  -- Data Center license replaces non Data Center license same app
    //  -- Non Data Center instance = Work, admin is progressive upgrade enters license first then configures.
    @Test
    public void shouldAllowProgressiveUpgrade() {
        //Given
        configuredAsDataCenterInstance(false);
        installApplication(mockLicHelper.SOFTWARE_APPLICATION);
        licenseApplication(newLicense("SOFT_NDC", NON_DATA_CENTER, mockLicHelper.SOFTWARE_APP_KEY));

        LicenseDetails dataCenterSoftwareLicense = newLicense("SOFT_DC", DATA_CENTER,
                mockLicHelper.SOFTWARE_APP_KEY);
        when(licenseManager.getLicense("SOFT_DC")).thenReturn(dataCenterSoftwareLicense);

        final JiraLicenseService.ValidationResult validationResultNonClustered =
                licenseService.validateApplicationLicense(i18nHelper, "SOFT_DC");

        assertValidationResultContainsNoError(validationResultNonClustered);
    }

    //  Upgrade:
    //
    //  JIRA Instance:
    //  -- Not configured as Data Center instance.
    //  -- Has JIRA Software Application installed and NON Data Center license.
    //
    //  Action:
    //  -- Admin configured as Data Center instance.
    //  -- At startup instance would lockout.
    //  -- Admin replaces JIRA Software license with JIRA Software Data Center license
    //
    //  Summary:
    //  -- Data Center license replaces Non Data Center license same application.
    @Test
    public void shouldAllowUpgrade() {
        //Given
        configuredAsDataCenterInstance(false);
        installApplication(mockLicHelper.SOFTWARE_APPLICATION);
        licenseApplication(newLicense("SOFT_NDC", NON_DATA_CENTER, mockLicHelper.SOFTWARE_APP_KEY));

        LicenseDetails dataCenterSoftwareLicense = newLicense("SOFT_DC", DATA_CENTER,
                mockLicHelper.SOFTWARE_APP_KEY);
        when(licenseManager.getLicense("SOFT_DC")).thenReturn(dataCenterSoftwareLicense);

        configuredAsDataCenterInstance(true);
        final JiraLicenseService.ValidationResult validationResultNonClustered =
                licenseService.validateApplicationLicense(i18nHelper, "SOFT_DC");

        assertValidationResultContainsNoError(validationResultNonClustered);
    }

    //  Admin renews to correct license:
    //
    //  JIRA Instance:
    //  -- Not configured as Data Center instance.
    //  -- Has JIRA Software Application installed with JIRA Software Data Center license.
    //
    //  Action:
    //  -- Admin adds JIRA Software license (NON Data Center).
    //
    //  Summary:
    //  -- Non Data Center license replaces Data Center License in non Data Center instance
    @Test
    public void shouldAllowReplacementInNonDataCenterInstance() {
        //Given
        configuredAsDataCenterInstance(false);
        installApplication(mockLicHelper.SOFTWARE_APPLICATION);
        licenseApplication(newLicense("SOFT_DC", DATA_CENTER, mockLicHelper.SOFTWARE_APP_KEY));

        LicenseDetails nonDataCenterSoftwareLicense = newLicense("SOFT_NDC_2", NON_DATA_CENTER,
                mockLicHelper.SOFTWARE_APP_KEY);
        when(licenseManager.getLicense("SOFT_NDC_2")).thenReturn(nonDataCenterSoftwareLicense);

        //When
        configuredAsDataCenterInstance(false);
        final JiraLicenseService.ValidationResult validationResultNotClustered =
                licenseService.validateApplicationLicense(i18nHelper, "SOFT_NDC_2");

        //Then:
        assertValidationResultContainsNoError(validationResultNotClustered);
    }

    //  Setup new instance:
    //
    //  JIRA Instance:
    //  -- Not configured as Data Center instance.
    //  -- Has JIRA Software Application installed.
    //
    //  Action:
    //  -- Admin adds JIRA Software license at startup.
    //
    //  Summary:
    //  -- Non Data Center Instance + Non Data Center License
    @Test
    public void shouldAllowNonDataCenterLicenseWithNonClusterInstance() {
        configuredAsDataCenterInstance(false);
        installApplication(mockLicHelper.SOFTWARE_APPLICATION);

        LicenseDetails nonDataCenterSoftwareLicense = newLicense("SOFT_NDC_2", NON_DATA_CENTER,
                mockLicHelper.SOFTWARE_APP_KEY);
        when(licenseManager.getLicense("SOFT_NDC_2")).thenReturn(nonDataCenterSoftwareLicense);

        //When
        configuredAsDataCenterInstance(false);
        final JiraLicenseService.ValidationResult validationResultNotClustered =
                licenseService.validateApplicationLicense(i18nHelper, "SOFT_NDC_2");

        //Then:
        assertValidationResultContainsNoError(validationResultNotClustered);
    }

    //  Setup new instance with data center license:
    //
    //  JIRA Instance:
    //  -- Not configured as Data Center instance.
    //  -- Has JIRA Software Application installed.
    //
    //  Action:
    //  -- Admin adds JIRA Software Data Center license at startup.
    //
    //  Summary:
    //  -- Non Data Center Instance + Data Center License
    @Test
    public void shouldAllowDataCenterLicenseWithNonClusterInstance() {
        configuredAsDataCenterInstance(false);
        installApplication(mockLicHelper.SOFTWARE_APPLICATION);

        LicenseDetails dataCenterSoftwareLicense = newLicense("SOFT_DC", DATA_CENTER,
                mockLicHelper.SOFTWARE_APP_KEY);
        when(licenseManager.getLicense("SOFT_DC")).thenReturn(dataCenterSoftwareLicense);

        //When
        final JiraLicenseService.ValidationResult validationResultNotClustered =
                licenseService.validateApplicationLicense(i18nHelper, "SOFT_DC");

        //Then:
        assertValidationResultContainsNoError(validationResultNotClustered);
    }

    //  Setup new data center configured instance with NON data center license:
    //
    //  JIRA Instance:
    //  -- Configured as Data Center instance.
    //  -- Has JIRA Software Application installed.
    //
    //  Action:
    //  -- Admin adds JIRA Software license at startup (not a Data Center license).
    //
    //  Summary:
    //  -- Data Center Instance + Non Data Center License
    @Test
    public void shouldFailValidationNondatacenterLicenseForConfiguredDataCenterInstance() {
        //Given:
        configuredAsDataCenterInstance(true);
        installApplication(mockLicHelper.SOFTWARE_APPLICATION);

        LicenseDetails nonDataCenterSoftwareLicense = newLicense("SOFT_NDC_2", NON_DATA_CENTER,
                mockLicHelper.SOFTWARE_APP_KEY);
        when(licenseManager.getLicense("SOFT_NDC_2")).thenReturn(nonDataCenterSoftwareLicense);

        //When
        final JiraLicenseService.ValidationResult validationResultClustered =
                licenseService.validateApplicationLicense(i18nHelper, "SOFT_NDC_2");

        assertValidationResultContainsSingleError(validationResultClustered, "jira.license.validation.not.datacenter{[<a href=\"test-url\">, </a>]}");
    }

    //  Downgrade 1:
    //  Customer insert non Data Center license with other Data Center licenses.
    //
    //  JIRA Instance:
    //  -- Has JIRA Software Application installed with Data Centre license.
    //  -- Has JIRA Portfolio Application installed with Data Centre license
    //
    //  Action:
    //  -- Admin only replaces JIRA Software Data Center license with non data center license.
    @Test
    public void shouldNotAllowLicenseDowngradeFromDataCenterToNonDataCenterWhenNotReplacingAllLicenses() {
        //Given
        configuredAsDataCenterInstance(true);
        installApplication(mockLicHelper.SOFTWARE_APPLICATION);
        licenseApplication(newLicense("SOFT_DC", DATA_CENTER, mockLicHelper.SOFTWARE_APP_KEY));
        installApplication(mockLicHelper.PORTFOLIO_APPLICATION);
        licenseApplication(newLicense("PORT_DC", DATA_CENTER, mockLicHelper.PORTFOLIO_APP_KEY));

        LicenseDetails nonDataCenterLicensesSoftwareOnly = newElaLicense("SOFT_NDC", NON_DATA_CENTER,
                mockLicHelper.SOFTWARE_APP_KEY);
        when(licenseManager.getLicense("SOFT_NDC")).thenReturn(nonDataCenterLicensesSoftwareOnly);

        //When
        configuredAsDataCenterInstance(false);
        final JiraLicenseService.ValidationResult validationResultNotClustered =
                licenseService.validateApplicationLicense(i18nHelper, "SOFT_NDC");

        //Then
        assertValidationResultContainsSingleError(validationResultNotClustered, "jira.license.validation.datacenter.mix{[<a href=\"test-url\">, </a>]}");

        configuredAsDataCenterInstance(true);
        final JiraLicenseService.ValidationResult validationResultClustered =
                licenseService.validateApplicationLicense(i18nHelper, "SOFT_NDC");

        //Then
        assertValidationResultContainsSingleError(validationResultClustered, "jira.license.validation.not.datacenter{[<a href=\"test-url\">, </a>]}");
    }

    //  Downgrade 2:
    //  Customer replaces Data Center license with ELA Non Data Center license. All licenses are being replaced.
    //
    //  JIRA Instance:
    //  -- Has JIRA Software Application installed with Data Centre license.
    //  -- Has JIRA Portfolio Application installed with Data Centre license
    //
    //  Action:
    //  -- Admin un configures JIRA instance as data center instance (now non data center instance)
    //  -- Admin replaces all Data Center license with ELA non data center license.
    @Test
    public void shouldAllowLicenseDowngradeFromDataCenterToNonDataCenterWhenNotConfiguredAsDataCenterInstanceAndReplaceAllLicenses() {
        //Given
        configuredAsDataCenterInstance(true);
        installApplication(mockLicHelper.SOFTWARE_APPLICATION);
        licenseApplication(newLicense("SOFT_DC", DATA_CENTER, mockLicHelper.SOFTWARE_APP_KEY));
        installApplication(mockLicHelper.PORTFOLIO_APPLICATION);
        licenseApplication(newLicense("PORT_DC", DATA_CENTER, mockLicHelper.PORTFOLIO_APP_KEY));

        LicenseDetails nonDataCenterElaLicenses = newElaLicense("ELA_NON_DC_LIC", NON_DATA_CENTER,
                mockLicHelper.SOFTWARE_APP_KEY, mockLicHelper.PORTFOLIO_APP_KEY);
        when(licenseManager.getLicense("ELA_NON_DC_LIC")).thenReturn(nonDataCenterElaLicenses);

        //When
        configuredAsDataCenterInstance(false);
        final JiraLicenseService.ValidationResult validationResultNotClustered =
                licenseService.validateApplicationLicense(i18nHelper, "ELA_NON_DC_LIC");

        //Then
        assertValidationResultContainsNoError(validationResultNotClustered);
    }

    //  Downgrade 3:
    //  Customer replaces Data Center license with ELA Non Data Center license. All licenses are being replaced.
    //  This instance is still configured as data center instance.
    //
    //  JIRA Instance:
    //  -- Has JIRA Software Application installed with Data Centre license.
    //  -- Has JIRA Portfolio Application installed with Data Centre license
    //
    //  Action:
    //  -- Admin replaces all Data Center license with ELA non data center license. But this is still configured as
    //     data center instance.
    @Test
    public void shouldNotAllowLicenseDowngradeFromDataCenterToNonDataCenterWhenConfiguredAsDataCenterInstance() {
        //Given
        configuredAsDataCenterInstance(true);
        installApplication(mockLicHelper.SOFTWARE_APPLICATION);
        licenseApplication(newLicense("SOFT_DC", DATA_CENTER, mockLicHelper.SOFTWARE_APP_KEY));
        installApplication(mockLicHelper.PORTFOLIO_APPLICATION);
        licenseApplication(newLicense("PORT_DC", DATA_CENTER, mockLicHelper.PORTFOLIO_APP_KEY));

        LicenseDetails nonDataCenterElaLicenses = newElaLicense("ELA_NON_DC_LIC", NON_DATA_CENTER,
                mockLicHelper.SOFTWARE_APP_KEY, mockLicHelper.PORTFOLIO_APP_KEY);
        when(licenseManager.getLicense("ELA_NON_DC_LIC")).thenReturn(nonDataCenterElaLicenses);

        //When
        final JiraLicenseService.ValidationResult validationResultClustered =
                licenseService.validateApplicationLicense(i18nHelper, "ELA_NON_DC_LIC");

        //Then
        assertValidationResultContainsSingleError(validationResultClustered, "jira.license.validation.not.datacenter{[<a href=\"test-url\">, </a>]}");
    }

    //  Upgrade 1:
    //  Customer replaces NON Data Center license with ELA Data Center license. All licenses are being replaced.
    //
    //  JIRA Instance:
    //  -- Not configured as Data Center instance.
    //  -- Has JIRA Software Application installed with NON Data Centre license.
    //  -- Has JIRA Portfolio Application installed with NON Data Centre license
    //
    //  Action:
    //  -- Admin replaces all NON Data Center license with ELA Data Center license.
    @Test
    public void shouldAllowUpgradeForELAWhenAllLicenseReplaced() {
        //Given
        configuredAsDataCenterInstance(false);
        installApplication(mockLicHelper.SOFTWARE_APPLICATION);
        licenseApplication(newLicense("SOFT_NDC", NON_DATA_CENTER, mockLicHelper.SOFTWARE_APP_KEY));
        installApplication(mockLicHelper.PORTFOLIO_APPLICATION);
        licenseApplication(newLicense("PORT_NDC", NON_DATA_CENTER, mockLicHelper.PORTFOLIO_APP_KEY));

        LicenseDetails elaDataCenterLicense = newElaLicense("ELA_DC", DATA_CENTER,
                mockLicHelper.SOFTWARE_APP_KEY, mockLicHelper.PORTFOLIO_APP_KEY);
        when(licenseManager.getLicense("ELA_DC")).thenReturn(elaDataCenterLicense);

        //When
        configuredAsDataCenterInstance(false);
        final JiraLicenseService.ValidationResult validationResultNotClustered =
                licenseService.validateApplicationLicense(i18nHelper, "ELA_DC");

        //Then
        assertValidationResultContainsNoError(validationResultNotClustered);

        //When
        configuredAsDataCenterInstance(true);
        final JiraLicenseService.ValidationResult validationResultClustered =
                licenseService.validateApplicationLicense(i18nHelper, "ELA_DC");

        //Then
        assertValidationResultContainsNoError(validationResultClustered);
    }

    //  Upgrade 2:
    //  Customer replaces NON Data Center license with ELA Data Center license.
    //  Only some licenses are being replaced leaving it with a mix.
    //
    //  JIRA Instance:
    //  -- Not configured as Data Center instance.
    //  -- Has JIRA Software Application installed with NON Data Centre license.
    //  -- Has JIRA Portfolio Application installed with NON Data Centre license
    //  -- Has JIRA Other Application installed with NON Data Centre license
    //
    //  Action:
    //  -- Admin replaces some NON Data Center license with ELA Data Center license.
    @Test
    public void shouldNotAllowUpgradeWhenPartialLicenseReplaces() {
        //Given
        configuredAsDataCenterInstance(false);
        installApplication(mockLicHelper.SOFTWARE_APPLICATION);
        licenseApplication(newLicense("SOFT_NDC", NON_DATA_CENTER, mockLicHelper.SOFTWARE_APP_KEY));
        installApplication(mockLicHelper.PORTFOLIO_APPLICATION);
        licenseApplication(newLicense("PORT_NDC", NON_DATA_CENTER, mockLicHelper.PORTFOLIO_APP_KEY));
        installApplication(mockLicHelper.OTHER_APPLICATION);
        licenseApplication(newLicense("mockLicHelper.OTHER_NDC", NON_DATA_CENTER, mockLicHelper.OTHER_APP_KEY));

        LicenseDetails elaDataCenterLicense = newElaLicense("ELA_DC", DATA_CENTER,
                mockLicHelper.SOFTWARE_APP_KEY, mockLicHelper.PORTFOLIO_APP_KEY);
        when(licenseManager.getLicense("ELA_DC")).thenReturn(elaDataCenterLicense);

        //When
        configuredAsDataCenterInstance(false);
        final JiraLicenseService.ValidationResult validationResultNotClustered =
                licenseService.validateApplicationLicense(i18nHelper, "ELA_DC");

        //Then
        assertValidationResultContainsSingleError(validationResultNotClustered, "jira.license.validation.datacenter.mix{[<a href=\"test-url\">, </a>]}");

        //When
        configuredAsDataCenterInstance(true);
        final JiraLicenseService.ValidationResult validationResultClustered =
                licenseService.validateApplicationLicense(i18nHelper, "ELA_DC");

        //Then
        assertValidationResultContainsSingleError(validationResultClustered, "jira.license.validation.not.datacenter.mix{[<a href=\"test-url\">, </a>]}");
    }

    /*
    --------------------------------------------------------------------------------------------------------------------
        TEST SETUP
    --------------------------------------------------------------------------------------------------------------------
        Section that contains all methods used to setup test
     */

    private final HashSet<LicenseDetails> licensedOnly = Sets.newHashSet();
    private final List<LicenseDetails> installedAppsAndLicense = Lists.newArrayList();

    private void licenseApplication(final LicenseDetails licenseDetails) {
        installedAppsAndLicense.add(licenseDetails);
        licensedOnly.add(licenseDetails);
        when(licenseService.getLicenses()).thenReturn(licensedOnly);
        when(licenseManager.getLicenses()).thenReturn(installedAppsAndLicense);
    }

    private final List<Application> installedApplications = Lists.newArrayList();

    private void installApplication(final Application application) {
        installedApplications.add(application);
        when(applicationManager.getApplication(application.getKey())).thenReturn(Option.<Application>some(application));
        when(applicationManager.getApplications()).thenReturn(installedApplications);
        when(application.buildDate()).thenReturn(DateTime.now());
    }

    private void configuredAsDataCenterInstance(boolean shouldConfigureAsDataCenter) {
        when(clusterManager.isClustered()).thenReturn(shouldConfigureAsDataCenter);
    }

    /*
    --------------------------------------------------------------------------------------------------------------------
        ASSERTS
    --------------------------------------------------------------------------------------------------------------------
        Section that contains all assertion helper methods
     */

    void assertValidationResultContainsSingleError(final JiraLicenseService.ValidationResult validationResult, String errorKey) {
        assertTrue("Does not contain errorKey:" + errorKey, validationResult.getErrorCollection().hasAnyErrors());
        final String errorKeyFound = validationResult.getErrorCollection().getErrors().values().iterator().next();
        assertEquals(errorKey, errorKeyFound);
        assertEquals(1, validationResult.getErrorCollection().getErrors().values().size());
    }

    void assertValidationResultContainsNoError(final JiraLicenseService.ValidationResult validationResult) {
        if (validationResult.getErrorCollection().hasAnyErrors()) {
            fail("Found unexpected error: " + validationResult.getErrorCollection().getErrors().values().iterator().next());
        }
        assertFalse(validationResult.getErrorCollection().hasAnyErrors());
    }

    /*
    --------------------------------------------------------------------------------------------------------------------
        MOCK OBJECT CREATION
    --------------------------------------------------------------------------------------------------------------------
        Section that contains methods used to create mock objects.
     */

    private LicenseDetails newElaLicense(final String licenseString, final boolean isDataCenter, final ApplicationKey... keys) {
        return newLicense(licenseString, isDataCenter, keys);
    }

    private LicenseDetails newLicense(final String licenseString, final boolean isDataCenter, final ApplicationKey... keys) {
        MockLicenseDetailsBuilder mockLicenseDetailsBuilder = new MockLicenseDetailsBuilder();
        mockLicenseDetailsBuilder.setRawLicense(licenseString);
        mockLicenseDetailsBuilder.setAsDataCenter(isDataCenter);
        for (ApplicationKey key : keys) {
            mockLicenseDetailsBuilder.setApplicationRoleWithLimit(key, 10);
        }
        return mockLicenseDetailsBuilder.build();
    }
}