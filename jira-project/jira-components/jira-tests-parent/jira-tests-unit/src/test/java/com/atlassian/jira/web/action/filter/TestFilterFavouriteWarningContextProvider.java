package com.atlassian.jira.web.action.filter;

import com.atlassian.jira.bc.favourites.FavouritesService;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Map;

import static com.atlassian.jira.bc.filter.FilterDeletionWarningViewProviderImpl.CONTEXT_KEY_SEARCH_REQUEST;
import static com.atlassian.jira.web.action.filter.FilterFavouriteWarningContextProvider.CONTEXT_KEY_FAVOURITE_COUNT;
import static com.google.common.collect.Maps.newHashMap;
import static org.hamcrest.collection.IsMapContaining.hasEntry;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestFilterFavouriteWarningContextProvider {
    @Mock
    private FavouritesService favouritesService;

    @Mock
    private JiraAuthenticationContext jiraAuthenticationContext;

    @Mock
    private SearchRequest searchRequest;

    @Mock
    private ApplicationUser user;
    
    @InjectMocks
    private FilterFavouriteWarningContextProvider filterFavouriteWarningContextProvider;
    
    private Map<String, Object> contextMap;

    @Before
    public void setUp() throws Exception {
        when(jiraAuthenticationContext.getLoggedInUser()).thenReturn(user);

        contextMap = newHashMap();
        contextMap.put(CONTEXT_KEY_SEARCH_REQUEST, searchRequest);
    }

    private void givenTotalFavouriteCount(int count) {
        when(searchRequest.getFavouriteCount()).thenReturn((long) count);
    }

    private void givenCurrentUserHasFavourited(boolean hasFavourited) {
        when(favouritesService.isFavourite(eq(user), eq(searchRequest))).thenReturn(hasFavourited);
    }

    @Test
    public void testWarningForOneSubscription() throws Exception {
        givenTotalFavouriteCount(1);
        givenCurrentUserHasFavourited(false);

        Map<String, Object> resultMap = filterFavouriteWarningContextProvider.getContextMap(contextMap);

        assertThat(resultMap, hasEntry(CONTEXT_KEY_FAVOURITE_COUNT, 1L));
    }

    @Test
    public void testWarningForManySubscriptions() throws Exception {
        givenTotalFavouriteCount(5);
        givenCurrentUserHasFavourited(false);

        Map<String, Object> resultMap = filterFavouriteWarningContextProvider.getContextMap(contextMap);

        assertThat(resultMap, hasEntry(CONTEXT_KEY_FAVOURITE_COUNT, 5L));
    }

    @Test
    public void testWarningForManySubscriptionsIncludingCurrentUser() throws Exception {
        givenTotalFavouriteCount(5);
        givenCurrentUserHasFavourited(true);

        Map<String, Object> resultMap = filterFavouriteWarningContextProvider.getContextMap(contextMap);

        assertThat(resultMap, hasEntry(CONTEXT_KEY_FAVOURITE_COUNT, 4L));
    }

    @Test
    public void testReturnsNoWarningForNoSubscriptions() throws Exception {
        givenTotalFavouriteCount(0);
        givenCurrentUserHasFavourited(false);

        Map<String, Object> resultMap = filterFavouriteWarningContextProvider.getContextMap(contextMap);

        assertThat(resultMap, hasEntry(CONTEXT_KEY_FAVOURITE_COUNT, 0L));
    }
}