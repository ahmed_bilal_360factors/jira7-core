package com.atlassian.jira.project.template.descriptor;

import com.atlassian.jira.plugin.webfragment.descriptors.ConditionDescriptorFactory;
import com.atlassian.jira.project.template.module.DemoProjectModule;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.elements.ResourceDescriptor;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugin.web.Condition;
import com.atlassian.plugin.webresource.UrlMode;
import com.atlassian.plugin.webresource.WebResourceUrlProvider;
import org.dom4j.Element;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.net.URL;
import java.util.Optional;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.when;

public class DemoProjectModuleDescriptorTest {
    private static final String REFERENCE_PLUGIN = "<demo-project key=\"demo-project\" weight=\"125\">\n" +
            "<label key=\"reference.demo.project.data.label\"/>\n" +
            "<description key=\"reference.demo.project.data.description\"/>\n" +
            "<longDescription key=\"reference.demo.project.data.description.long\"/>\n" +
            "<icon location=\"images/icon.jpg\" content-type=\"image/jpg\" />\n" +
            "<backgroundIcon location=\"images/background.jpg\" content-type=\"image/jpg\" />\n" +
            "<importFile location=\"location/to/sample/data.json\"/>\n" +
            "<projectTemplate key=\"gh-scrum-template\"/>\n" +
            "<projectType key=\"software\"/>\n" +
            "</demo-project>";

    @Rule
    public MockitoRule mockito = MockitoJUnit.rule();

    @Mock
    private ModuleFactory moduleFactory;
    @Mock
    private ResourceDescriptorFactory resourceDescriptorFactory;
    @Mock
    private WebResourceUrlProvider urlProvider;
    @Mock
    private ConditionDescriptorFactory conditionDescriptorFactory;


    @Mock
    private Plugin plugin;

    private DemoProjectModuleDescriptor descriptor;
    private DemoProjectElementBuilder elementBuilder;


    @Before
    public void setUp() throws Exception {
        //by default return some mocked resource descriptor (required for icons)
        ResourceDescriptor mockResourceDescriptor = mock(ResourceDescriptor.class);
        when(resourceDescriptorFactory.makeResourceDescriptorNode(Mockito.anyString(), Mockito.anyString(), Mockito.anyString())).thenReturn(mockResourceDescriptor);

        descriptor = new DemoProjectModuleDescriptor(moduleFactory, resourceDescriptorFactory, urlProvider, conditionDescriptorFactory);
        elementBuilder = DemoProjectElementBuilder.fromReference(REFERENCE_PLUGIN);
    }

    @Test
    public void shouldProperlyParseBasicParametersFromXml() {
        descriptor.init(plugin, elementBuilder.getXmlElement());
        final DemoProjectModule module = descriptor.getModule();

        assertEquals(125, (int) module.getWeight());
        assertEquals("demo-project", module.getKey());
        assertEquals("reference.demo.project.data.label", module.getLabelKey());
        assertEquals("reference.demo.project.data.description", module.getDescriptionKey());
        assertEquals("reference.demo.project.data.description.long", module.getLongDescriptionKey().get());
        assertEquals("gh-scrum-template", module.getProjectTemplateKey().get());
        assertEquals("software", module.getProjectTypeKey().get());
    }

    @Test
    public void shouldReturnURLForJSONResource() throws Exception {
        URL url = new URL("http://example.com/some/specific/url");
        when(plugin.getResource("location/to/sample/data.json")).thenReturn(url);

        descriptor.init(plugin, elementBuilder.getXmlElement());
        final DemoProjectModule module = descriptor.getModule();

        assertSame(url, module.getImportFile());
    }

    @Test
    public void shouldReturnNonesAndDefaultsWhenNoDataGiven() {
        descriptor.init(plugin, elementBuilder.weight(null)
                .longDescriptionKey(null)
                .projectTemplateKey(null)
                .projectTypeKey(null)
                .backgroundIconLocation(null)
                .getXmlElement()
        );
        final DemoProjectModule module = descriptor.getModule();

        assertEquals(100, (int) module.getWeight());
        assertEquals(Optional.empty(), module.getLongDescriptionKey());
        assertEquals(Optional.empty(), module.getProjectTemplateKey());
        assertEquals(Optional.empty(), module.getProjectTypeKey());
        assertEquals(Optional.empty(), module.getBackgroundIconUrl());
    }

    @Test
    public void shouldCreateResourceDescriptorsBasedOnResourceName() {
        reset(resourceDescriptorFactory);

        ResourceDescriptor mockIcon = mock(ResourceDescriptor.class);
        when(mockIcon.getName()).thenReturn("images/icon.jpg");
        ResourceDescriptor mockBackgroundIcon = mock(ResourceDescriptor.class);
        when(mockBackgroundIcon.getName()).thenReturn("images/background.jpg");

        when(resourceDescriptorFactory.makeResourceDescriptorNode("icon.jpg", "images/icon.jpg", "image/jpg")).thenReturn(mockIcon);
        when(resourceDescriptorFactory.makeResourceDescriptorNode("background.jpg", "images/background.jpg", "image/jpg")).thenReturn(mockBackgroundIcon);
        when(urlProvider.getStaticPluginResourceUrl("null:demo-project", "images/icon.jpg", UrlMode.AUTO)).thenReturn("images/icon.jpg");
        when(urlProvider.getStaticPluginResourceUrl("null:demo-project", "images/background.jpg", UrlMode.AUTO)).thenReturn("images/background.jpg");

        descriptor.init(plugin, elementBuilder.getXmlElement());
        final DemoProjectModule module = descriptor.getModule();

        assertThat(descriptor.getResourceDescriptors(), containsInAnyOrder(mockBackgroundIcon, mockIcon));
        assertEquals("images/icon.jpg", module.getIconUrl());
        assertEquals("images/background.jpg", module.getBackgroundIconUrl().get());
    }

    @Test
    public void shouldContainOnlyIconResouceWhenBackgroundNotSpecified() {
        reset(resourceDescriptorFactory);

        ResourceDescriptor mockIcon = mock(ResourceDescriptor.class);
        when(resourceDescriptorFactory.makeResourceDescriptorNode("icon.jpg", "images/icon.jpg", "image/jpg")).thenReturn(mockIcon);

        descriptor.init(plugin, elementBuilder.backgroundIconLocation(null).getXmlElement());

        assertThat(descriptor.getResourceDescriptors(), contains(mockIcon));
    }

    @Test(expected = NullPointerException.class)
    public void shouldNotAllowToCreateModuleWithoutIconSet() {
        descriptor.init(plugin, elementBuilder.iconLocation(null).getXmlElement());
    }

    @Test(expected = NullPointerException.class)
    public void shouldNotAllowToCreateModuleWithoutImportFile() {
        descriptor.init(plugin, elementBuilder.importFileLocation(null).getXmlElement());
    }

    @Test
    public void shouldCreateConditionBasedOnElementsConditionTagAfterEnabled() {
        final Condition condition = mock(Condition.class);

        final Element xmlElement = elementBuilder.getXmlElement();
        when(conditionDescriptorFactory.retrieveCondition(plugin, xmlElement)).thenReturn(condition);
        descriptor.init(plugin, xmlElement);
        descriptor.enabled();

        assertSame(condition, descriptor.getCondition());
    }

    @Test
    public void shouldReturnNullConditionWhenNotInitialized() {
        assertNull(descriptor.getCondition());
    }
}