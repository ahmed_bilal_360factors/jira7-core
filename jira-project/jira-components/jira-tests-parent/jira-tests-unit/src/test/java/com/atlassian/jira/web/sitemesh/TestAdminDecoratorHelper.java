package com.atlassian.jira.web.sitemesh;

import com.atlassian.jira.bc.project.ProjectAction;
import com.atlassian.jira.bc.project.ProjectService;
import com.atlassian.jira.junit.rules.MockHttp;
import com.atlassian.jira.mock.security.MockSimpleAuthenticationContext;
import com.atlassian.jira.plugin.webfragment.SimpleLinkManager;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.plugin.webfragment.model.SimpleLink;
import com.atlassian.jira.plugin.webfragment.model.SimpleLinkImpl;
import com.atlassian.jira.plugin.webfragment.model.SimpleLinkSection;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.template.soy.SoyTemplateRendererProvider;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.NoopI18nHelper;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.jira.util.collect.MapBuilder;
import com.atlassian.plugin.web.WebInterfaceManager;
import com.atlassian.plugin.web.model.WebPanel;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Iterables;
import org.apache.commons.lang.StringUtils;
import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.collection.IsIterableContainingInOrder;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnit;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;

import static com.google.common.collect.Lists.newArrayList;
import static org.apache.commons.lang.StringUtils.isEmpty;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyMap;
import static org.mockito.Matchers.argThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @since v4.4
 */
public class TestAdminDecoratorHelper {
    public static final String MAIN_SIDEBAR_LOCATION = "atl.jira.proj.config.main-sidebar";
    @Rule
    public final MethodRule initMockito = MockitoJUnit.rule();
    @Rule
    public MockHttp.DefaultMocks mockHttp = MockHttp.withDefaultMocks();
    @Mock
    private WebInterfaceManager webInterfaceManager;
    @Mock
    private ProjectService projectService;
    @Mock
    private SimpleLinkManager simpleLinkManager;
    @Mock
    private SoyTemplateRendererProvider soyTemplateProvider;
    @Mock
    private AdminDecoratorSectionLinkItemHelper adminDecoratorSectionLinkItemHelper;

    private MockApplicationUser user;
    private JiraAuthenticationContext authenticationContext;

    @Before
    public void setUp() throws Exception {
        user = new MockApplicationUser("bbain");
        authenticationContext = new MockSimpleAuthenticationContext(user, Locale.ENGLISH, new NoopI18nHelper());
        when(adminDecoratorSectionLinkItemHelper.findSectionLink(any(SimpleLinkSection.class), eq(user), any(JiraHelper.class))).thenReturn(Optional.empty());
    }

    @Test
    public void getHeadersNoProjectKey() throws Exception {
        List<String> panelContents = Arrays.asList("panel1", "panel2");

        Map<String, Object> expectedContext = MapBuilder.<String, Object>build("admin.active.section", "some.section", "admin.active.tab",
                "blah", "adminNavigationPrimary", new ArrayList<Map<String, Object>>());

        List<WebPanel> webPanels = newArrayList(Iterables.transform(panelContents, content -> mockWebPanel(content, expectedContext)));

        when(webInterfaceManager.getDisplayableWebPanels("system.admin.decorator.header", Collections.<String, Object>emptyMap()))
                .thenReturn(webPanels);

        when(simpleLinkManager.getSectionsForLocation(eq("system.admin.top"), eq(user), isA(JiraHelper.class)))
                .thenReturn(Collections.<SimpleLinkSection>emptyList());

        AdminDecoratorHelper helper = new AdminDecoratorHelper(webInterfaceManager, projectService,
                authenticationContext, simpleLinkManager, soyTemplateProvider, adminDecoratorSectionLinkItemHelper);

        helper.setCurrentSection("some.section");
        helper.setCurrentTab("blah");

        assertFalse(helper.hasKey());

        final List<AdminDecoratorHelper.Header> actualHeaders = helper.getHeaders();
        List<String> result = newArrayList(Iterables.transform(actualHeaders, AdminDecoratorHelper.Header::getHtml));

        assertThat(result,
                IsIterableContainingInOrder.contains(panelContents.toArray()));

        assertTrue(helper.isHasHeader());
        String combined = StringUtils.join(panelContents, "");
        assertEquals(combined, helper.getHeaderHtml());
    }

    @Test
    public void getHeadersWithoutProject() throws Exception {
        String key = "ABC";
        when(projectService.getProjectByKeyForAction(user, key, ProjectAction.EDIT_PROJECT_CONFIG))
                .thenReturn(new ProjectService.GetProjectResult(error("Not Found")));

        when(webInterfaceManager.getDisplayableWebPanels("system.admin.decorator.header", Collections.<String, Object>emptyMap()))
                .thenReturn(Collections.<WebPanel>emptyList());

        when(simpleLinkManager.getSectionsForLocation(eq("system.admin.top"), eq(user), isA(JiraHelper.class)))
                .thenReturn(Collections.<SimpleLinkSection>emptyList());

        AdminDecoratorHelper helper = new AdminDecoratorHelper(webInterfaceManager, projectService,
                authenticationContext, simpleLinkManager, soyTemplateProvider, adminDecoratorSectionLinkItemHelper);

        helper.setProject(key);

        assertTrue(helper.hasKey());
        assertEquals(Collections.<AdminDecoratorHelper.Header>emptyList(), helper.getHeaders());
        assertEquals(Collections.<AdminDecoratorHelper.Header>emptyList(), helper.getHeaders());

        assertFalse(helper.isHasHeader());
        assertTrue(isEmpty(helper.getHeaderHtml()));
    }

    @Test
    public void getHeadersWithDefaultLinkForMainSectionUsesCorrectWebItemUrl() throws Exception {
        AdminDecoratorHelper helper = new AdminDecoratorHelper(webInterfaceManager, projectService,
                authenticationContext, simpleLinkManager, soyTemplateProvider, adminDecoratorSectionLinkItemHelper);

        SimpleLinkSection mainSection = mock(SimpleLinkSection.class);
        String mainSectionId = "mainSectionId";
        when(mainSection.getId()).thenReturn(mainSectionId);

        SimpleLinkSection subSection = mock(SimpleLinkSection.class);
        when(subSection.getId()).thenReturn("subSectionId");
        SimpleLink relatedLink = mock(SimpleLink.class);
        String relatedLinkUrl = "relatedLinkUrl";
        when(relatedLink.getUrl()).thenReturn(relatedLinkUrl);

        when(simpleLinkManager.getSectionsForLocation(eq("system.admin.top"), eq(user), isA(JiraHelper.class)))
                .thenReturn(ImmutableList.of(mainSection));

        when(simpleLinkManager.getSectionsForLocation(eq(mainSectionId), eq(user), isA(JiraHelper.class)))
                .thenReturn(ImmutableList.of(subSection));

        when(simpleLinkManager.getLinksForSection(eq(subSection.getId()), eq(user), isA(JiraHelper.class))).thenReturn(ImmutableList.of(relatedLink));

        final WebPanel mockWebPanel = mock(WebPanel.class);

        when(webInterfaceManager.getDisplayableWebPanels("system.admin.decorator.header", Collections.<String, Object>emptyMap()))
                .thenReturn(ImmutableList.of(mockWebPanel));

        // getHtml is required to get hold of the context map
        helper.getHeaders().forEach(AdminDecoratorHelper.Header::getHtml);

        verify(mockWebPanel).getHtml(argThat(adminNavigationPrimaryWithLink(relatedLinkUrl)));
    }

    private Matcher<Map<String, Object>> adminNavigationPrimaryWithLink(final String url) {
        return new BaseMatcher<Map<String, Object>>() {
            @Override
            public boolean matches(final Object item) {
                Map<String, Object> map = (Map<String, Object>) item;
                List adminNavigationPrimary = (List) map.get("adminNavigationPrimary");
                if (adminNavigationPrimary != null && adminNavigationPrimary.size() > 0) {
                    Map<String, Object> adminNavigationPrimaryMap = (Map<String, Object>) adminNavigationPrimary.get(0);
                    return url.equals(adminNavigationPrimaryMap.get("link"));
                }
                return false;
            }

            @Override
            public void describeTo(final Description description) {
                description.appendText("<expected first item in 'adminNavigationPrimary' list to be a map with link=").appendText(url).appendText(">");
            }
        };
    }

    @Test
    public void getHeadersProject() throws Exception {
        String key = "ABC";
        List<String> panelContents = Arrays.asList("panel1", "panel2");

        Project project = new MockProject(101010L, key);
        Map<String, Object> expectedContext = new HashMap<String, Object>();
        expectedContext.put("project", project);
        expectedContext.put("admin.active.section", null);
        expectedContext.put("admin.active.tab", "blah");
        expectedContext.put("adminNavigationPrimary", new ArrayList<Map<String, Object>>());
        expectedContext.put("adminNavigationSecondary", new ArrayList<Map<String, Object>>());

        when(projectService.getProjectByKeyForAction(user, key, ProjectAction.EDIT_PROJECT_CONFIG))
                .thenReturn(new ProjectService.GetProjectResult(ok(), project));

        List<WebPanel> webPanels = newArrayList(Iterables.transform(panelContents, content -> mockWebPanel(content, expectedContext)));

        when(webInterfaceManager.getDisplayableWebPanels("atl.jira.proj.config.header", Collections.emptyMap()))
                .thenReturn(webPanels);

        AdminDecoratorHelper helper = new AdminDecoratorHelper(webInterfaceManager, projectService,
                authenticationContext, simpleLinkManager, soyTemplateProvider, adminDecoratorSectionLinkItemHelper) {
            protected String encode(String string) {
                return string;
            }
        };

        helper.setProject(key);
        assertTrue(helper.hasKey());

        helper.setCurrentTab("blah");

        List<AdminDecoratorHelper.Header> actualHeaders = helper.getHeaders();
        List<String> result = newArrayList(Iterables.transform(actualHeaders, AdminDecoratorHelper.Header::getHtml));

        Assert.assertThat(result,
                IsIterableContainingInOrder.contains(panelContents.toArray()));

        assertTrue(helper.isHasHeader());
        String combined = StringUtils.join(panelContents, "");
        assertEquals(combined, helper.getHeaderHtml());

        //The last result should have been cached.
        assertSame(actualHeaders, helper.getHeaders());
    }

    @Test
    public void shouldReturnEmptySidebarHtmlWhenNoWebPanelIsFound() {
        final AdminDecoratorHelper adminDecoratorHelper = new AdminDecoratorHelper(webInterfaceManager, projectService, authenticationContext, simpleLinkManager, soyTemplateProvider, adminDecoratorSectionLinkItemHelper);
        final String sidebarHtml = adminDecoratorHelper.getSidebarHtml();

        assertThat("empty HTML was returned from an empty list of web-panels", sidebarHtml, is(""));
    }

    @Test
    public void shouldReturnCorrectSidebarHtmlWhenSimpleAndSingleWebPanelIsPresent() {
        final String PANEL_HTML = "This is a very basic <strong>web-panel</strong>.";

        List<String> panelContents = Arrays.asList(PANEL_HTML);
        List<WebPanel> webPanels = newArrayList(Iterables.transform(panelContents, content -> mockWebPanel(content)));
        when(webInterfaceManager.getDisplayableWebPanels(eq(MAIN_SIDEBAR_LOCATION), anyMap())).thenReturn(webPanels);

        final AdminDecoratorHelper adminDecoratorHelper = new AdminDecoratorHelper(webInterfaceManager, projectService, authenticationContext, simpleLinkManager, soyTemplateProvider, adminDecoratorSectionLinkItemHelper);
        final String sidebarHtml = adminDecoratorHelper.getSidebarHtml();

        assertThat("correct HTML was returned", sidebarHtml, is(PANEL_HTML));
    }

    @Test
    public void shouldReturnSidebarHtmlOnlyFromTheFirstWebPanel() {
        List<String> panelContents = Arrays.asList("first web-panel", "second web-panel (should be ignored)");
        List<WebPanel> webPanels = newArrayList(Iterables.transform(panelContents, content -> mockWebPanel(content)));
        when(webInterfaceManager.getDisplayableWebPanels(eq(MAIN_SIDEBAR_LOCATION), anyMap())).thenReturn(webPanels);

        final AdminDecoratorHelper adminDecoratorHelper = new AdminDecoratorHelper(webInterfaceManager, projectService, authenticationContext, simpleLinkManager, soyTemplateProvider, adminDecoratorSectionLinkItemHelper);
        final String sidebarHtml = adminDecoratorHelper.getSidebarHtml();

        assertThat("HTML from the first web-panel was returned", sidebarHtml, is("first web-panel"));
    }

    @Test
    public void shouldMarkLinkAsLabsOnlyWhenTrueValueIsProvided() {
        final Function<Map<String, String>, SimpleLink> newLink = (params) -> {
            SimpleLink link = mock(SimpleLink.class);
            when(link.getUrl()).thenReturn("http://some/url/is/required");
            when(link.getParams()).thenReturn(params);
            return link;
        };

        SimpleLink linkWithoutParams = newLink.apply(null);                                             //0
        SimpleLink linkWithSomeUnknownLabsValue = newLink.apply(ImmutableMap.of("labs", "test123"));    //1
        SimpleLink linkWithFalseValue = newLink.apply(ImmutableMap.of("labs", "false"));                //2
        SimpleLink linkWithTrueLabsValue = newLink.apply(ImmutableMap.of("labs", "true"));              //3

        when(simpleLinkManager.getLinksForSection(Mockito.eq("myParentSection/mySection"), Mockito.same(user), Mockito.any(JiraHelper.class)))
                .thenReturn(ImmutableList.of(linkWithoutParams, linkWithSomeUnknownLabsValue, linkWithFalseValue, linkWithTrueLabsValue));
        final AdminDecoratorHelper adminDecoratorHelper = new AdminDecoratorHelper(webInterfaceManager, projectService, authenticationContext, simpleLinkManager, soyTemplateProvider, adminDecoratorSectionLinkItemHelper);

        final List<Map<String, Object>> maps = adminDecoratorHelper.makeLinksForSection("myParentSection", "mySection");
        assertFalse((boolean)maps.get(0).get("isLabs"));
        assertFalse((boolean)maps.get(1).get("isLabs"));
        assertFalse((boolean)maps.get(2).get("isLabs"));
        assertTrue((boolean)maps.get(3).get("isLabs"));
    }

    // useful when you simply want the HTML back, regardless of the context map
    private WebPanel mockWebPanel(final String panelContent) {
        WebPanel webPanel = mock(WebPanel.class);
        when(webPanel.getHtml(anyMap())).thenReturn(panelContent);
        return webPanel;
    }

    private WebPanel mockWebPanel(final String panelContent, final Map<String, Object> expectedContext) {
        WebPanel webPanel = mock(WebPanel.class);
        when(webPanel.getHtml(expectedContext)).thenReturn(panelContent);
        return webPanel;
    }

    private ErrorCollection ok() {
        return new SimpleErrorCollection();
    }

    private ErrorCollection error(String... msgs) {
        SimpleErrorCollection simpleErrorCollection = new SimpleErrorCollection();
        simpleErrorCollection.addErrorMessages(Arrays.asList(msgs));
        return simpleErrorCollection;
    }
}
