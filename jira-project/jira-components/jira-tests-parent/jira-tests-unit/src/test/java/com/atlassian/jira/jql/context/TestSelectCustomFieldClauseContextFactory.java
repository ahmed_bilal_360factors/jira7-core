package com.atlassian.jira.jql.context;

import com.atlassian.jira.action.issue.customfields.option.MockOption;
import com.atlassian.jira.issue.customfields.option.Option;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.config.FieldConfigScheme;
import com.atlassian.jira.jql.operand.JqlOperandResolver;
import com.atlassian.jira.jql.operand.QueryLiteral;
import com.atlassian.jira.jql.util.JqlSelectOptionsUtil;
import com.atlassian.jira.jql.validator.AlwaysValidOperatorUsageValidator;
import com.atlassian.jira.jql.validator.MockJqlOperandResolver;
import com.atlassian.jira.jql.validator.OperatorUsageValidator;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.collect.CollectionBuilder;
import com.atlassian.query.clause.TerminalClause;
import com.atlassian.query.clause.TerminalClauseImpl;
import com.atlassian.query.operand.EmptyOperand;
import com.atlassian.query.operand.MultiValueOperand;
import com.atlassian.query.operand.Operand;
import com.atlassian.query.operand.SingleValueOperand;
import com.atlassian.query.operator.Operator;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import static com.atlassian.jira.jql.operand.SimpleLiteralFactory.createLiteral;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestSelectCustomFieldClauseContextFactory {
    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @Mock
    private CustomField customField;
    @Mock
    private JqlSelectOptionsUtil jqlSelectOptionsUtil;
    @Mock
    private FieldConfigSchemeClauseContextUtil fieldConfigSchemeClauseContextUtil;
    @Mock
    private ContextSetUtil contextSetUtil;

    private final ApplicationUser theUser = null;
    private JqlOperandResolver jqlOperandResolver = MockJqlOperandResolver.createSimpleSupport();
    private OperatorUsageValidator operatorUsageValidator = new AlwaysValidOperatorUsageValidator();

    @Test
    public void testBadOperator() throws Exception {
        final Operand operand = new SingleValueOperand("one");
        final TerminalClause clause = new TerminalClauseImpl("blah", Operator.LESS_THAN, operand);

        final ClauseContext clauseContext1 = new ClauseContextImpl();
        final ClauseContext clauseContext2 = createProjectContext(1);
        final ClauseContext clauseContext3 = createProjectContext(6);

        final FieldConfigScheme scheme1 = mock(FieldConfigScheme.class);
        final FieldConfigScheme scheme2 = mock(FieldConfigScheme.class);

        when(customField.getConfigurationSchemes()).thenReturn(CollectionBuilder.<FieldConfigScheme>newBuilder(scheme1, scheme2).asList());

        when(fieldConfigSchemeClauseContextUtil.getContextForConfigScheme(theUser, scheme1)).thenReturn(clauseContext2);
        when(scheme1.isGlobal()).thenReturn(false);
        when(contextSetUtil.union(CollectionBuilder.<ClauseContext>newBuilder(clauseContext1, clauseContext2).asSet())).thenReturn(clauseContext3);

        when(fieldConfigSchemeClauseContextUtil.getContextForConfigScheme(theUser, scheme2)).thenReturn(clauseContext3);
        when(scheme2.isGlobal()).thenReturn(false);
        when(contextSetUtil.union(CollectionBuilder.<ClauseContext>newBuilder(clauseContext3, clauseContext3).asSet())).thenReturn(clauseContext3);

        final SelectCustomFieldClauseContextFactory factory = new SelectCustomFieldClauseContextFactory(customField, contextSetUtil, jqlSelectOptionsUtil, fieldConfigSchemeClauseContextUtil, jqlOperandResolver, operatorUsageValidator);
        final ClauseContext result = factory.getClauseContext(theUser, clause);

        assertEquals(clauseContext3, result);
        verify(fieldConfigSchemeClauseContextUtil).getContextForConfigScheme(theUser, scheme1);
        verify(contextSetUtil).union(CollectionBuilder.<ClauseContext>newBuilder(clauseContext1, clauseContext2).asSet());
        verify(fieldConfigSchemeClauseContextUtil).getContextForConfigScheme(theUser, scheme2);
        verify(contextSetUtil).union(CollectionBuilder.<ClauseContext>newBuilder(clauseContext3, clauseContext3).asSet());

    }

    @Test
    public void testGetInvalidUsage() throws Exception {
        final Operand operand = new SingleValueOperand("one");
        final TerminalClause clause = new TerminalClauseImpl("blah", Operator.EQUALS, operand);

        final ClauseContext clauseContext1 = new ClauseContextImpl();
        final ClauseContext clauseContext2 = createProjectContext(1);
        final ClauseContext clauseContext3 = createProjectContext(6);

        final FieldConfigScheme scheme1 = mock(FieldConfigScheme.class);
        final FieldConfigScheme scheme2 = mock(FieldConfigScheme.class);

        operatorUsageValidator = mock(OperatorUsageValidator.class);
        when(operatorUsageValidator.check(theUser, clause)).thenReturn(false);

        when(customField.getConfigurationSchemes()).thenReturn(CollectionBuilder.<FieldConfigScheme>newBuilder(scheme1, scheme2).asList());

        when(fieldConfigSchemeClauseContextUtil.getContextForConfigScheme(theUser, scheme1)).thenReturn(clauseContext2);
        when(scheme1.isGlobal()).thenReturn(false);
        when(contextSetUtil.union(CollectionBuilder.<ClauseContext>newBuilder(clauseContext1, clauseContext2).asSet())).thenReturn(clauseContext3);

        when(fieldConfigSchemeClauseContextUtil.getContextForConfigScheme(theUser, scheme2)).thenReturn(clauseContext3);
        when(scheme2.isGlobal()).thenReturn(false);
        when(contextSetUtil.union(CollectionBuilder.<ClauseContext>newBuilder(clauseContext3, clauseContext3).asSet())).thenReturn(clauseContext3);

        final SelectCustomFieldClauseContextFactory factory = new SelectCustomFieldClauseContextFactory(customField, contextSetUtil, jqlSelectOptionsUtil, fieldConfigSchemeClauseContextUtil, jqlOperandResolver, operatorUsageValidator);
        final ClauseContext result = factory.getClauseContext(theUser, clause);

        assertEquals(clauseContext3, result);
        verify(fieldConfigSchemeClauseContextUtil).getContextForConfigScheme(theUser, scheme1);
        verify(contextSetUtil).union(CollectionBuilder.<ClauseContext>newBuilder(clauseContext1, clauseContext2).asSet());
        verify(fieldConfigSchemeClauseContextUtil).getContextForConfigScheme(theUser, scheme2);
        verify(contextSetUtil).union(CollectionBuilder.<ClauseContext>newBuilder(clauseContext3, clauseContext3).asSet());
    }

    @Test
    public void testGetClauseContextNoSchemes() throws Exception {
        final QueryLiteral literal = createLiteral(10L);
        final TerminalClause clause = new TerminalClauseImpl("blah", Operator.EQUALS, new MultiValueOperand(literal));
        when(customField.getConfigurationSchemes()).thenReturn(Collections.<FieldConfigScheme>emptyList());
        final SelectCustomFieldClauseContextFactory factory = new SelectCustomFieldClauseContextFactory(customField, contextSetUtil, jqlSelectOptionsUtil, fieldConfigSchemeClauseContextUtil, jqlOperandResolver, operatorUsageValidator);

        final ClauseContext result = factory.getClauseContext(theUser, clause);

        assertGlobalContext(result);
    }

    @Test
    public void testGetClauseContextNullSchemes() throws Exception {
        final QueryLiteral literal = createLiteral(10L);
        final TerminalClause clause = new TerminalClauseImpl("blah", Operator.EQUALS, new MultiValueOperand(literal));
        when(customField.getConfigurationSchemes()).thenReturn(null);
        final SelectCustomFieldClauseContextFactory factory = new SelectCustomFieldClauseContextFactory(customField, contextSetUtil, jqlSelectOptionsUtil, fieldConfigSchemeClauseContextUtil, jqlOperandResolver, operatorUsageValidator);

        final ClauseContext result = factory.getClauseContext(theUser, clause);

        assertGlobalContext(result);
    }

    @Test
    public void testGetClauseContextOneOptionEmptyOneNotEmpty() throws Exception {
        final QueryLiteral literalPositive = createLiteral(10L);
        final QueryLiteral literalEmpty = new QueryLiteral(EmptyOperand.EMPTY);
        final TerminalClause clause = new TerminalClauseImpl("blah", Operator.EQUALS, new MultiValueOperand(literalPositive, literalEmpty));
        final MockOption option = new MockOption(null, null, null, null, null, 10L);

        final ClauseContext clauseContext1 = new ClauseContextImpl();
        final ClauseContext clauseContext2 = createProjectContext(1);
        final ClauseContext clauseContext3 = createProjectContext(2);

        final FieldConfigScheme scheme = mock(FieldConfigScheme.class);
        when(scheme.isGlobal()).thenReturn(false);

        when(customField.getConfigurationSchemes()).thenReturn(CollectionBuilder.<FieldConfigScheme>newBuilder(scheme).asList());

        when(jqlSelectOptionsUtil.getOptions(customField, literalPositive, false)).thenReturn(Collections.<Option>singletonList(option));
        when(jqlSelectOptionsUtil.getOptions(customField, literalEmpty, false)).thenReturn(Collections.<Option>singletonList(null));

        when(jqlSelectOptionsUtil.getOptionsForScheme(scheme)).thenReturn(Collections.<Option>singletonList(option));

        when(fieldConfigSchemeClauseContextUtil.getContextForConfigScheme(theUser, scheme)).thenReturn(clauseContext2);
        when(contextSetUtil.union(CollectionBuilder.<ClauseContext>newBuilder(clauseContext1, clauseContext2).asSet())).thenReturn(clauseContext3);

        final SelectCustomFieldClauseContextFactory factory = new SelectCustomFieldClauseContextFactory(customField, contextSetUtil, jqlSelectOptionsUtil, fieldConfigSchemeClauseContextUtil, jqlOperandResolver, operatorUsageValidator);

        final ClauseContext result = factory.getClauseContext(theUser, clause);

        assertEquals(clauseContext3, result);
    }

    @Test
    public void testGetClauseContextNoLiterals() throws Exception {
        final QueryLiteral literalPositive = createLiteral(10L);
        final QueryLiteral literalEmpty = new QueryLiteral();
        final TerminalClause clause = new TerminalClauseImpl("blah", Operator.EQUALS, new MultiValueOperand(literalPositive, literalEmpty));

        final ClauseContext clauseContext1 = new ClauseContextImpl();
        final ClauseContext clauseContext2 = createProjectContext(1);
        final ClauseContext clauseContext3 = createProjectContext(2);

        final FieldConfigScheme scheme = mock(FieldConfigScheme.class);
        when(scheme.isGlobal()).thenReturn(false);

        jqlOperandResolver = mock(JqlOperandResolver.class);
        when(jqlOperandResolver.getValues(theUser, clause.getOperand(), clause)).thenReturn(Collections.<QueryLiteral>emptyList());

        when(customField.getConfigurationSchemes()).thenReturn(CollectionBuilder.<FieldConfigScheme>newBuilder(scheme).asList());
        when(fieldConfigSchemeClauseContextUtil.getContextForConfigScheme(theUser, scheme)).thenReturn(clauseContext2);
        when(contextSetUtil.union(CollectionBuilder.<ClauseContext>newBuilder(clauseContext1, clauseContext2).asSet())).thenReturn(clauseContext3);

        final SelectCustomFieldClauseContextFactory factory = new SelectCustomFieldClauseContextFactory(customField, contextSetUtil, jqlSelectOptionsUtil, fieldConfigSchemeClauseContextUtil, jqlOperandResolver, operatorUsageValidator);

        final ClauseContext result = factory.getClauseContext(theUser, clause);

        assertEquals(clauseContext3, result);
    }


    @Test
    public void testGetClauseContextOptionsEquals() throws Exception {
        final String value = "one";
        final SingleValueOperand operand = new SingleValueOperand(value);
        final TerminalClause clause = new TerminalClauseImpl("blah", Operator.EQUALS, operand);
        final QueryLiteral literal = new QueryLiteral(operand, value);

        final MockOption option1 = new MockOption(null, null, null, null, null, 10L);
        final MockOption option2 = new MockOption(null, null, null, null, null, 20L);
        final MockOption option3 = new MockOption(null, null, null, null, null, 30L);

        final ClauseContext clauseContext1 = new ClauseContextImpl();
        final ClauseContext clauseContext2 = createProjectContext(1);
        final ClauseContext clauseContext3 = createProjectContext(6);

        final FieldConfigScheme scheme1 = mock(FieldConfigScheme.class);
        final FieldConfigScheme scheme2 = mock(FieldConfigScheme.class);

        when(customField.getConfigurationSchemes()).thenReturn(CollectionBuilder.<FieldConfigScheme>newBuilder(scheme1, scheme2).asList());

        when(jqlSelectOptionsUtil.getOptions(customField, literal, false)).thenReturn(Arrays.<Option>asList(option1, option2));

        when(jqlSelectOptionsUtil.getOptionsForScheme(scheme1)).thenReturn(Collections.singletonList(option1));
        when(fieldConfigSchemeClauseContextUtil.getContextForConfigScheme(theUser, scheme1)).thenReturn(clauseContext2);
        when(scheme1.isGlobal()).thenReturn(false);
        when(contextSetUtil.union(CollectionBuilder.<ClauseContext>newBuilder(clauseContext1, clauseContext2).asSet())).thenReturn(clauseContext3);

        //This scheme should be ignored.
        when(jqlSelectOptionsUtil.getOptionsForScheme(scheme2)).thenReturn(Collections.singletonList(option3));

        final SelectCustomFieldClauseContextFactory factory = new SelectCustomFieldClauseContextFactory(customField, contextSetUtil, jqlSelectOptionsUtil, fieldConfigSchemeClauseContextUtil, jqlOperandResolver, operatorUsageValidator);
        final ClauseContext result = factory.getClauseContext(theUser, clause);

        assertEquals(clauseContext3, result);
    }

    @Test
    public void testGetClauseContextOptionsNotEquals() throws Exception {
        final MultiValueOperand operand = new MultiValueOperand("one", "two");
        final TerminalClause clause = new TerminalClauseImpl("blah", Operator.NOT_EQUALS, operand);
        final QueryLiteral literal1 = new QueryLiteral(operand, "one");
        final QueryLiteral literal2 = new QueryLiteral(operand, "two");

        final MockOption option1 = new MockOption(null, null, null, null, null, 10L);
        final MockOption option3 = new MockOption(null, null, null, null, null, 30L);

        final ClauseContext clauseContext1 = new ClauseContextImpl();
        final ClauseContext clauseContext2 = createProjectContext(1);
        final ClauseContext clauseContext3 = createProjectContext(6);

        final FieldConfigScheme scheme1 = mock(FieldConfigScheme.class);
        final FieldConfigScheme scheme2 = mock(FieldConfigScheme.class);

        //This should be ignored.
        when(customField.getConfigurationSchemes()).thenReturn(CollectionBuilder.<FieldConfigScheme>newBuilder(scheme1, scheme2).asList());
        when(jqlSelectOptionsUtil.getOptions(customField, literal1, false)).thenReturn(Collections.singletonList(option1));
        when(jqlSelectOptionsUtil.getOptions(customField, literal2, false)).thenReturn(Collections.singletonList(option1));

        when(jqlSelectOptionsUtil.getOptionsForScheme(scheme1)).thenReturn(Collections.singletonList(option1));

        when(jqlSelectOptionsUtil.getOptionsForScheme(scheme2)).thenReturn(Collections.singletonList(option3));
        when(fieldConfigSchemeClauseContextUtil.getContextForConfigScheme(theUser, scheme2)).thenReturn(clauseContext2);
        when(scheme2.isGlobal()).thenReturn(false);
        when(contextSetUtil.union(CollectionBuilder.<ClauseContext>newBuilder(clauseContext1, clauseContext2).asSet())).thenReturn(clauseContext3);

        final SelectCustomFieldClauseContextFactory factory = new SelectCustomFieldClauseContextFactory(customField, contextSetUtil, jqlSelectOptionsUtil, fieldConfigSchemeClauseContextUtil, jqlOperandResolver, operatorUsageValidator);
        final ClauseContext result = factory.getClauseContext(theUser, clause);

        assertEquals(clauseContext3, result);
    }

    @Test
    public void testGetClauseContextOptionsGlobalContext() throws Exception {
        final String value = "one";
        final SingleValueOperand operand = new SingleValueOperand(value);
        final QueryLiteral literal = new QueryLiteral(operand, value);
        final TerminalClause clause = new TerminalClauseImpl("blah", Operator.NOT_EQUALS, operand);
        final MockOption option1 = new MockOption(null, null, null, null, null, 10L);
        final MockOption option3 = new MockOption(null, null, null, null, null, 30L);

        final ClauseContext clauseContext1 = new ClauseContextImpl();
        final ClauseContext clauseContext2 = createProjectContext(1);
        final ClauseContext clauseContext3 = createProjectContext(6);

        final FieldConfigScheme scheme1 = mock(FieldConfigScheme.class);
        final FieldConfigScheme scheme2 = mock(FieldConfigScheme.class);

        when(customField.getConfigurationSchemes()).thenReturn(CollectionBuilder.<FieldConfigScheme>newBuilder(scheme1, scheme2).asList());
        when(jqlSelectOptionsUtil.getOptions(customField, literal, false)).thenReturn(Collections.singletonList(option1));

        when(jqlSelectOptionsUtil.getOptionsForScheme(scheme1)).thenReturn(Collections.singletonList(option3));
        when(scheme1.isGlobal()).thenReturn(false);
        when(fieldConfigSchemeClauseContextUtil.getContextForConfigScheme(theUser, scheme1)).thenReturn(clauseContext2);
        when(contextSetUtil.union(CollectionBuilder.<ClauseContext>newBuilder(clauseContext1, clauseContext2).asSet())).thenReturn(clauseContext3);

        when(jqlSelectOptionsUtil.getOptionsForScheme(scheme2)).thenReturn(Collections.singletonList(option3));
        when(scheme2.isGlobal()).thenReturn(true);

        final SelectCustomFieldClauseContextFactory factory = new SelectCustomFieldClauseContextFactory(customField, contextSetUtil, jqlSelectOptionsUtil, fieldConfigSchemeClauseContextUtil, jqlOperandResolver, operatorUsageValidator);
        final ClauseContext result = factory.getClauseContext(theUser, clause);

        assertGlobalContext(result);
        verify(contextSetUtil).union(CollectionBuilder.<ClauseContext>newBuilder(clauseContext1, clauseContext2).asSet());
    }

    @Test
    public void testNoLiterals() throws Exception {
        final String value = "one";
        final SingleValueOperand operand = new SingleValueOperand(value);
        final QueryLiteral literal = new QueryLiteral(operand, value);
        final TerminalClause clause = new TerminalClauseImpl("blah", Operator.NOT_EQUALS, operand);

        final FieldConfigScheme scheme = mock(FieldConfigScheme.class);

        when(customField.getConfigurationSchemes()).thenReturn(CollectionBuilder.<FieldConfigScheme>newBuilder(scheme, scheme).asList());
        when(jqlSelectOptionsUtil.getOptions(customField, literal, false)).thenReturn(Collections.<Option>emptyList());
        when(scheme.isGlobal()).thenReturn(true);

        final SelectCustomFieldClauseContextFactory factory = new SelectCustomFieldClauseContextFactory(customField, contextSetUtil, jqlSelectOptionsUtil,
                fieldConfigSchemeClauseContextUtil, jqlOperandResolver, operatorUsageValidator);

        assertGlobalContext(factory.getClauseContext(theUser, clause));
    }

    @Test
    public void testNullLiterals() throws Exception {
        final String value = "one";
        final SingleValueOperand operand = new SingleValueOperand(value);
        final QueryLiteral literal = new QueryLiteral(operand, value);
        final TerminalClause clause = new TerminalClauseImpl("blah", Operator.NOT_EQUALS, operand);

        final FieldConfigScheme scheme = mock(FieldConfigScheme.class);

        when(customField.getConfigurationSchemes()).thenReturn(CollectionBuilder.<FieldConfigScheme>newBuilder(scheme, scheme).asList());
        when(jqlSelectOptionsUtil.getOptions(customField, literal, false)).thenReturn(CollectionBuilder.<Option>newBuilder((Option) null).asMutableList());
        when(scheme.isGlobal()).thenReturn(true);

        final SelectCustomFieldClauseContextFactory factory = new SelectCustomFieldClauseContextFactory(customField, contextSetUtil, jqlSelectOptionsUtil,
                fieldConfigSchemeClauseContextUtil, jqlOperandResolver, operatorUsageValidator);

        assertGlobalContext(factory.getClauseContext(theUser, clause));
    }

    private static void assertGlobalContext(final ClauseContext result) {
        assertEquals(ClauseContextImpl.createGlobalClauseContext(), result);
    }

    private ClauseContext createProjectContext(final int... projects) {
        final Set<ProjectIssueTypeContext> itc = new HashSet<>();
        for (final int project : projects) {
            itc.add(new ProjectIssueTypeContextImpl(new ProjectContextImpl((long) project), AllIssueTypesContext.getInstance()));
        }
        return new ClauseContextImpl(itc);
    }
}
