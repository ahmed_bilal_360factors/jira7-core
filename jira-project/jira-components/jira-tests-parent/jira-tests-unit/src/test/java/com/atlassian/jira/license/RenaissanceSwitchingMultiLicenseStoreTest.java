package com.atlassian.jira.license;

import com.google.common.collect.ImmutableList;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class RenaissanceSwitchingMultiLicenseStoreTest {
    private static final ImmutableList<String> LEGACY_LICENSES = ImmutableList.of("legacy");
    private static final ImmutableList<String> PROD_LICENSES = ImmutableList.of("production");
    private static final String LEGACY_ID = "legacyId";
    private static final String PRODUCTION_ID = "productionId";

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();
    @Mock
    private MultiLicenseStore production;
    @Mock
    private MultiLicenseStore legacy;
    private RenaissanceSwitchingMultiLicenseStore store;
    private RenaissanceMigrationStatus predicate = new MockRenaissanceMigrationStatus();

    @Before
    public void setUp() {
        when(legacy.retrieve()).thenReturn(LEGACY_LICENSES);
        when(production.retrieve()).thenReturn(PROD_LICENSES);
        when(legacy.retrieveServerId()).thenReturn(LEGACY_ID);
        when(production.retrieveServerId()).thenReturn(PRODUCTION_ID);

        store = new RenaissanceSwitchingMultiLicenseStore(legacy, production, predicate);
    }

    @Test
    public void retrieveUsesLegacyStoreWhenMigrationNotRun() {
        //when
        final Iterable<String> result = store.retrieve();

        //then
        assertThat(result, is(LEGACY_LICENSES));
    }

    @Test
    public void retrieveUsesProductStoreWhenMigrationHasRun() {
        //when
        final Iterable<String> resultLegacy = store.retrieve();
        //then
        assertThat(resultLegacy, is(LEGACY_LICENSES));

        //given
        predicate.markDone();
        //when
        final Iterable<String> resultProduction = store.retrieve();
        //then
        assertThat(resultProduction, is(PROD_LICENSES));
    }

    @Test
    public void storeUsesLegacyStoreWhenMigrationNotRun() {
        //when
        store.store(LEGACY_LICENSES);

        //then
        verify(legacy).store(LEGACY_LICENSES);
        verify(production, never()).store(any());
    }

    @Test
    public void storeUsesProductStoreWhenMigrationHasRun() {
        //given
        predicate.markDone();
        //when
        store.store(PROD_LICENSES);
        //then
        verify(production).store(PROD_LICENSES);
        verify(legacy, never()).store(any());
    }

    @Test
    public void retrieveServerIdUsesLegacyStoreWhenMigrationNotRun() {
        //when
        final String result = store.retrieveServerId();

        //then
        assertThat(result, is(LEGACY_ID));
    }

    @Test
    public void retrieveServerIdUsesProductStoreWhenMigrationHasRun() {
        //when
        final String resultLegacy = store.retrieveServerId();
        //then
        assertThat(resultLegacy, is(LEGACY_ID));

        //given
        predicate.markDone();
        //when
        final String result = store.retrieveServerId();
        //then
        assertThat(result, is(PRODUCTION_ID));
    }

    @Test
    public void storeServerIdUsesLegacyStoreWhenMigrationNotRun() {
        //when
        store.storeServerId(LEGACY_ID);

        //then
        verify(legacy).storeServerId(LEGACY_ID);
        verify(production, never()).storeServerId(any());
    }

    @Test
    public void storeServerIdUsesProductStoreWhenMigrationHasRun() {
        //given
        predicate.markDone();
        //when
        store.storeServerId(PRODUCTION_ID);
        //then
        verify(production).storeServerId(PRODUCTION_ID);
        verify(legacy, never()).storeServerId(any());
    }

    @Test
    public void storeServerIdUsesProductStoreInCloud() {
        //given
        predicate.markDone();
        //when
        store.storeServerId(PRODUCTION_ID);
        //then
        verify(production).storeServerId(PRODUCTION_ID);
        verify(legacy, never()).storeServerId(any());
    }

    @Test
    public void resetOldBuildConfirmationUsesLegacyStoreWhenMigrationNotRun() {
        //when
        store.resetOldBuildConfirmation();

        //then
        verify(legacy).resetOldBuildConfirmation();
        verify(production, never()).resetOldBuildConfirmation();
    }

    @Test
    public void resetOldBuildConfirmationUsesProductStoreWhenMigrationHasRun() {
        //given
        predicate.markDone();
        //when
        store.resetOldBuildConfirmation();
        //then
        verify(production).resetOldBuildConfirmation();
        verify(legacy, never()).resetOldBuildConfirmation();
    }

    @Test
    public void resetOldBuildConfirmationUsesProductStoreInCloud() {
        //given
        predicate.markDone();
        //when
        store.resetOldBuildConfirmation();
        //then
        verify(production).resetOldBuildConfirmation();
        verify(legacy, never()).resetOldBuildConfirmation();
    }

    @Test
    public void confirmProceedUnderEvaluationTermsUsesLegacyStoreWhenMigrationNotRun() {
        //when
        final String admin = "admin";
        store.confirmProceedUnderEvaluationTerms(admin);

        //then
        verify(legacy).confirmProceedUnderEvaluationTerms(admin);
        verify(production, never()).confirmProceedUnderEvaluationTerms(any());
    }

    @Test
    public void confirmProceedUnderEvaluationTermsUsesProductStoreWhenMigrationHasRun() {
        //given
        final String admin = "admin";
        predicate.markDone();
        //when
        store.confirmProceedUnderEvaluationTerms(admin);
        //then
        verify(production).confirmProceedUnderEvaluationTerms(admin);
        verify(legacy, never()).confirmProceedUnderEvaluationTerms(any());
    }

    @Test
    public void confirmProceedUnderEvaluationTermsUsesProductStoreInCloud() {
        //given
        final String admin = "admin";
        predicate.markDone();
        //when
        store.confirmProceedUnderEvaluationTerms(admin);
        //then
        verify(production).confirmProceedUnderEvaluationTerms(admin);
        verify(legacy, never()).confirmProceedUnderEvaluationTerms(any());
    }

    @Test
    public void clearUsesLegacyStoreWhenMigrationNotRun() {
        //when
        store.clear();

        //then
        verify(legacy).clear();
        verify(production, never()).clear();
    }

    @Test
    public void clearUsesProductStoreWhenMigrationHasRun() {
        //given
        predicate.markDone();
        //when
        store.clear();
        //then
        verify(production).clear();
        verify(legacy, never()).clear();
    }

    @Test
    public void clearUsesProductStoreInCloud() {
        //given
        predicate.markDone();
        //when
        store.clear();
        //then
        verify(production).clear();
        verify(legacy, never()).clear();
    }
}