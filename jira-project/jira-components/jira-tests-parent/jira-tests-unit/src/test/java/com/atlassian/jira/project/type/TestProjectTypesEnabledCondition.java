package com.atlassian.jira.project.type;

import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;

@RunWith(MockitoJUnitRunner.class)
public class TestProjectTypesEnabledCondition {
    private static final ApplicationUser USER = new MockApplicationUser("user");

    @Mock
    private ProjectTypesDarkFeature darkFeature;

    private ProjectTypesEnabledCondition condition;

    @Before
    public void setUp() {
        condition = new ProjectTypesEnabledCondition();
    }

    @Test
    public void shouldDisplayReturnsTrue() {
        assertTrue(condition.shouldDisplay(USER, anyJiraHelper()));
    }

    private JiraHelper anyJiraHelper() {
        return mock(JiraHelper.class);
    }
}
