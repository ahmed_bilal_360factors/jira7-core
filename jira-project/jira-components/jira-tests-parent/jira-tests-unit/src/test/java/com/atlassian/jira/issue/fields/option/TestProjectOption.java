package com.atlassian.jira.issue.fields.option;

import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TestProjectOption {
    @Test
    public void testTransform() throws Exception {
        List<MockProject> projects = Arrays.asList(new MockProject(1L, "ABC", "Abra"), new MockProject(2L, "DOG", "Dabra"));

        final List<ProjectOption> projectOptions = ProjectOption.transform(projects);
        assertEquals(2, projectOptions.size());
        assertEquals("1", projectOptions.get(0).getId());
        assertEquals("2", projectOptions.get(1).getId());
        assertEquals("Abra", projectOptions.get(0).getName());
        assertEquals("Dabra", projectOptions.get(1).getName());
    }

    @Test
    public void testTransformEmptyList() throws Exception {
        List<Project> projects = Collections.emptyList();

        final List<ProjectOption> projectOptions = ProjectOption.transform(projects);
        assertTrue(projectOptions.isEmpty());
    }

}
