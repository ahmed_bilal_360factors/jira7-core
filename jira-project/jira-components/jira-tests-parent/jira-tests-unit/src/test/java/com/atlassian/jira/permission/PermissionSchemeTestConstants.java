package com.atlassian.jira.permission;

import com.atlassian.jira.permission.data.PermissionGrantImpl;
import com.atlassian.jira.permission.data.PermissionSchemeImpl;
import com.atlassian.jira.scheme.Scheme;
import com.atlassian.jira.scheme.SchemeEntity;
import com.google.common.collect.BiMap;
import com.google.common.collect.ImmutableBiMap;
import com.google.common.collect.ImmutableList;

import java.util.Collections;

import static com.atlassian.jira.permission.PermissionHolder.holder;

final class PermissionSchemeTestConstants {
    public static final PermissionScheme PERMISSION_SCHEME_1 = new PermissionSchemeImpl(
            42L,
            "name",
            "description",
            ImmutableList.<PermissionGrant>of(
                    new PermissionGrantImpl(
                            420L,
                            holder(JiraPermissionHolderType.PROJECT_ROLE, "100"),
                            ProjectPermissions.ADMINISTER_PROJECTS
                    ),
                    new PermissionGrantImpl(
                            421L,
                            holder(JiraPermissionHolderType.ANYONE),
                            ProjectPermissions.CREATE_ISSUES))
    );

    public static final String USERNAME = "username";
    public static final String USERKEY = "userkey";

    public static final PermissionScheme PERMISSION_SCHEME_WITH_USER_NAME = new PermissionSchemeImpl(
            44L,
            "name",
            "description",
            ImmutableList.<PermissionGrant>of(
                    new PermissionGrantImpl(
                            420L,
                            holder(JiraPermissionHolderType.USER, USERNAME),
                            ProjectPermissions.ADMINISTER_PROJECTS
                    ),
                    new PermissionGrantImpl(
                            421L,
                            holder(JiraPermissionHolderType.ANYONE),
                            ProjectPermissions.CREATE_ISSUES))
    );

    public static final PermissionScheme PERMISSION_SCHEME_WITH_USER_KEY = new PermissionSchemeImpl(
            45L,
            "name",
            "description",
            ImmutableList.<PermissionGrant>of(
                    new PermissionGrantImpl(
                            420L,
                            holder(JiraPermissionHolderType.USER, USERKEY),
                            ProjectPermissions.ADMINISTER_PROJECTS
                    ),
                    new PermissionGrantImpl(
                            421L,
                            holder(JiraPermissionHolderType.ANYONE),
                            ProjectPermissions.CREATE_ISSUES))
    );

    public static final PermissionSchemeInput PERMISSION_SCHEME_WITH_USER_NAME_DATA = PermissionSchemeInput.builder(PERMISSION_SCHEME_WITH_USER_NAME).build();
    public static final PermissionSchemeInput PERMISSION_SCHEME_WITH_USER_KEY_DATA = PermissionSchemeInput.builder(PERMISSION_SCHEME_WITH_USER_KEY).build();

    public static final PermissionSchemeInput PERMISSION_SCHEME_1_DATA = PermissionSchemeInput.builder(PERMISSION_SCHEME_1).build();

    public static final PermissionScheme EMPTY_PERMISSION_SCHEME = new PermissionSchemeImpl(43L, "empty", "empty permission scheme");

    public static final PermissionSchemeInput EMPTY_PERMISSION_SCHEME_DATA = PermissionSchemeInput.builder(EMPTY_PERMISSION_SCHEME).build();

    public static final Scheme SCHEME_1 = new Scheme(
            42L,
            "permission",
            "name",
            "description",
            ImmutableList.of(
                    new SchemeEntity(420L, "projectrole", "100", ProjectPermissions.ADMINISTER_PROJECTS, null, 42L),
                    new SchemeEntity(421L, "group", null, ProjectPermissions.CREATE_ISSUES, null, 42L)

            ));

    public static final Scheme EMPTY_SCHEME = new Scheme(
            43L,
            "permission",
            "empty",
            "empty permission scheme",
            Collections.<SchemeEntity>emptyList()
    );

    public static BiMap<Scheme, PermissionScheme> schemeEquivalency = ImmutableBiMap.of(
            SCHEME_1, PERMISSION_SCHEME_1,
            EMPTY_SCHEME, EMPTY_PERMISSION_SCHEME
    );

    public static Iterable<PermissionSchemeInput> SCHEME_DATAS = ImmutableList.of(PERMISSION_SCHEME_1_DATA, EMPTY_PERMISSION_SCHEME_DATA);
}
