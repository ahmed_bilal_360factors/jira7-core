package com.atlassian.jira.issue.fields.csv.custom;

import com.atlassian.jira.issue.customfields.impl.AbstractSingleFieldType;
import com.atlassian.jira.issue.customfields.impl.URLCFType;

public class URLCFTypeCsvExportTest extends TextCustomFieldCsvExportTest {
    @Override
    protected AbstractSingleFieldType<String> createField() {
        return new URLCFType(null, null, null, null);
    }
}
