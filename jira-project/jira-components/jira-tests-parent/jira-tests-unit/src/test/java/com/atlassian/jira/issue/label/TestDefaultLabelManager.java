package com.atlassian.jira.issue.label;

import com.atlassian.jira.event.type.EventType;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.IssueFieldConstants;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.history.ChangeItemBean;
import com.atlassian.jira.issue.label.suggestions.LabelSuggester;
import com.atlassian.jira.issue.search.SearchProvider;
import com.atlassian.jira.issue.util.IssueUpdateBean;
import com.atlassian.jira.issue.util.IssueUpdater;
import com.atlassian.jira.mock.issue.MockIssue;
import com.atlassian.jira.mock.ofbiz.MockGenericValue;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.ArgumentMatcher;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;

import static org.hamcrest.collection.IsEmptyCollection.empty;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.when;

/**
 * Test case for {@link DefaultLabelManager}.
 *
 * @since v4.2
 */
public class TestDefaultLabelManager {
    private static final String DEFAULT_LABEL_TEXT = "foo";
    private static final long ISSUE_ID = 1L;
    private static final long CUSTOM_FIELD_ID = ISSUE_ID;
    private ApplicationUser user = new MockApplicationUser("admin");

    private static final Set<String> LABELS_STRINGS = Sets.newHashSet(DEFAULT_LABEL_TEXT);

    private static final MockIssue MOCK_ISSUE = newMockIssueBackedByGV(ISSUE_ID);

    private static final AtomicLong IDS = new AtomicLong();

    @Rule
    public ExpectedException expectedException = org.junit.rules.ExpectedException.none();

    @Rule
    public MockitoRule initializeMocksRule = MockitoJUnit.rule();

    @Mock
    LabelStore labelStore;

    @Mock
    IssueManager issueManager;

    @Mock
    IssueUpdater issueUpdater;

    @Mock
    LabelSuggester labelSuggester;

    @Mock
    CustomFieldManager customFieldManager;

    @Mock
    SearchProvider searchProvider;

    DefaultLabelManager labelManager;

    private static MockIssue newMockIssueBackedByGV(final Long id) {
        final MockIssue answer = new MockIssue();
        final MockGenericValue backing = new MockGenericValue("Issue");
        backing.set("id", id);
        backing.set(IssueFieldConstants.ISSUE_KEY, id);
        answer.setGenericValue(backing);
        return answer;
    }

    @Before
    public void setUp() throws Exception {
        labelManager = getLabelManagerWithMockedCustomFieldManager();
    }

    private DefaultLabelManager getLabelManagerWithMockedCustomFieldManager() {
        return new DefaultLabelManager(labelStore, issueManager, issueUpdater, labelSuggester) {
            @Override
            protected CustomFieldManager getFieldManager() {
                return customFieldManager;
            }
        };
    }

    @Test
    public void testGetSystemFieldLabels() throws Exception {
        final Set<Label> expectedLabels = labelSetFor(DEFAULT_LABEL_TEXT);
        when(labelStore.getLabels(ISSUE_ID, null)).thenReturn(expectedLabels);

        assertEquals(expectedLabels, labelManager.getLabels(ISSUE_ID));
        Mockito.verify(issueUpdater, never()).doUpdate(anyObject(), anyBoolean());
    }


    @Test
    public void testGetCustomFieldLabels() throws Exception {
        final Set<Label> expectedLabels = labelSetFor(CUSTOM_FIELD_ID, DEFAULT_LABEL_TEXT);
        when(labelStore.getLabels(ISSUE_ID, CUSTOM_FIELD_ID)).thenReturn(expectedLabels);

        assertEquals(expectedLabels, labelManager.getLabels(ISSUE_ID, CUSTOM_FIELD_ID));
        Mockito.verify(issueUpdater, never()).doUpdate(anyObject(), anyBoolean());
    }

    @Test
    public void testOnlySetSystemFieldLabels() {
        final Set<Label> expectedReturn = labelSetFor(LABELS_STRINGS, null);

        when(issueManager.getIssueObject(ISSUE_ID)).thenReturn(MOCK_ISSUE);
        when(labelStore.getLabels(ISSUE_ID, null)).thenReturn(Sets.newHashSet());
        when(labelStore.setLabels(ISSUE_ID, null, LABELS_STRINGS)).thenReturn(expectedReturn);
        when(issueManager.getIssueObject(ISSUE_ID)).thenReturn(newMockIssueWithLabels(DEFAULT_LABEL_TEXT));

        assertEquals(expectedReturn, labelManager.setLabels(null, ISSUE_ID, LABELS_STRINGS, false, true));
        Mockito.verify(issueUpdater).doUpdate(argThat(mockitoIssueUpdateBeanMatcher("", DEFAULT_LABEL_TEXT, IssueFieldConstants.LABELS, false, ChangeItemBean.STATIC_FIELD)), Matchers.eq(false));
    }

    @Test
    public void testOnlySetCustomFieldLabels() throws Exception {
        final Set<Label> expectedReturn = labelSetFor(LABELS_STRINGS, CUSTOM_FIELD_ID);
        when(labelStore.getLabels(ISSUE_ID, CUSTOM_FIELD_ID)).thenReturn(Sets.newHashSet());
        when(labelStore.setLabels(ISSUE_ID, CUSTOM_FIELD_ID, LABELS_STRINGS)).thenReturn(expectedReturn);
        when(issueManager.getIssueObject(ISSUE_ID)).thenReturn(newMockIssueWithLabels(DEFAULT_LABEL_TEXT));

        final CustomField answer = Mockito.mock(CustomField.class);
        when(answer.getIdAsLong()).thenReturn(CUSTOM_FIELD_ID);
        when(answer.getName()).thenReturn("customfield");
        when(customFieldManager.getCustomFieldObject(CUSTOM_FIELD_ID)).thenReturn(answer);

        assertEquals(expectedReturn, labelManager.setLabels(null, ISSUE_ID, CUSTOM_FIELD_ID, LABELS_STRINGS, false, true));
        Mockito.verify(issueUpdater).doUpdate(argThat(mockitoIssueUpdateBeanMatcher("", DEFAULT_LABEL_TEXT, "customfield", false, ChangeItemBean.CUSTOM_FIELD)), Matchers.eq(false));
    }

    @Test
    public void testSetMultipleLabels() throws Exception {
        final Set<String> oldLabels = ImmutableSet.of("some", "another");
        final Set<String> newLabels = ImmutableSet.of("one", "two", "three");
        final Set<Label> expectedLabels = labelSetFor(newLabels, null);

        when(issueManager.getIssueObject(ISSUE_ID)).thenReturn(newMockIssueWithLabels(oldLabels, null));
        when(labelStore.getLabels(ISSUE_ID, null)).thenReturn(labelSetFor(oldLabels, null));
        when(labelStore.setLabels(ISSUE_ID, null, newLabels)).thenReturn(expectedLabels);

        assertEquals(expectedLabels, labelManager.setLabels(null, ISSUE_ID, newLabels, false, true));
        Mockito.verify(issueUpdater).doUpdate(argThat(mockitoIssueUpdateBeanMatcher("another some", "one three two", IssueFieldConstants.LABELS, false, ChangeItemBean.STATIC_FIELD)), Matchers.eq(false));
    }


    @Test
    public void testAddSystemFieldLabel() throws Exception {
        final Label expectedLabel = defaultSystemLabel();
        when(labelStore.getLabels(ISSUE_ID, null)).thenReturn(labelSetFor(Sets.newHashSet(), null));
        when(labelStore.addLabel(ISSUE_ID, null, DEFAULT_LABEL_TEXT)).thenReturn(expectedLabel);
        when(issueManager.getIssueObject(ISSUE_ID)).thenReturn(newMockIssueWithLabels(DEFAULT_LABEL_TEXT));

        assertEquals(expectedLabel, labelManager.addLabel(null, ISSUE_ID, DEFAULT_LABEL_TEXT, false));
        Mockito.verify(issueUpdater).doUpdate(argThat(mockitoIssueUpdateBeanMatcher("", DEFAULT_LABEL_TEXT, IssueFieldConstants.LABELS, false, ChangeItemBean.STATIC_FIELD)), Matchers.eq(false));
    }

    @Test
    public void testAddCustomFieldLabel() throws Exception {
        final Label expectedLabel = defaultCustomFieldLabel(CUSTOM_FIELD_ID);
        when(labelStore.getLabels(ISSUE_ID, null)).thenReturn(labelSetFor(Sets.newHashSet(), CUSTOM_FIELD_ID));
        when(labelStore.addLabel(ISSUE_ID, CUSTOM_FIELD_ID, DEFAULT_LABEL_TEXT)).thenReturn(expectedLabel);
        when(issueManager.getIssueObject(ISSUE_ID)).thenReturn(newMockIssueWithLabels(DEFAULT_LABEL_TEXT));

        final CustomField answer = Mockito.mock(CustomField.class);
        when(answer.getIdAsLong()).thenReturn(CUSTOM_FIELD_ID);
        when(answer.getName()).thenReturn("customfield");
        when(customFieldManager.getCustomFieldObject(CUSTOM_FIELD_ID)).thenReturn(answer);

        final Label actual = labelManager.addLabel(null, ISSUE_ID, CUSTOM_FIELD_ID, DEFAULT_LABEL_TEXT, false);
        assertEquals(expectedLabel, actual);
    }


    @Test
    public void testNullLabelRejected() throws Exception {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("label should not be null!");
        labelManager.addLabel(null, ISSUE_ID, null, false);
    }

    @Test
    public void testBlankLabelRejected() throws Exception {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Labels cannot be blank!");
        labelManager.addLabel(null, ISSUE_ID, " ", false);
    }

    @Test
    public void testTooLongLabelRejected() throws Exception {
        final StringBuilder sb = new StringBuilder();
        for (int i = 0; i <= LabelParser.MAX_LABEL_LENGTH; i++) {
            sb.append("x");
        }
        final String tooLongLabel = sb.toString();
        assertTrue(tooLongLabel.length() > LabelParser.MAX_LABEL_LENGTH);

        expectedException.expect(IllegalArgumentException.class);
        labelManager.addLabel(null, ISSUE_ID, tooLongLabel, false);
    }

    @Test
    public void testSuggestedLabels() throws Exception {
        final DefaultLabelManager labelManager = getLabelManagerWithMockedCustomFieldManager();
        final ImmutableSet<Label> issueLabels = ImmutableSet.of(new Label(1000L, 1000L, "dudette"));
        when(labelStore.getLabels(ISSUE_ID, null)).thenReturn(issueLabels);

        final Set<String> suggestedLabels = labelManager.getSuggestedLabels(user, ISSUE_ID, "dude");

        //no hits should be returned.
        assertThat(suggestedLabels, empty());

        Mockito.verify(labelSuggester).getSuggestedLabels("dude", ISSUE_ID, issueLabels, user);
        Mockito.verify(issueUpdater, never()).doUpdate(anyObject(), anyBoolean());
    }

    @Test
    public void testSetSameLabelsDoesNotCauseUpdate() throws Exception {
        final Set<Label> expectedLabels = labelSetFor("some", "another");
        when(labelStore.getLabels(ISSUE_ID, null)).thenReturn(expectedLabels);
        when(issueManager.getIssueObject(ISSUE_ID)).thenReturn(newMockIssueWithLabels("some", "another"));

        assertEquals(expectedLabels, labelManager.setLabels(null, ISSUE_ID, asSet("some", "another"), false, true));
        Mockito.verify(issueUpdater, never()).doUpdate(anyObject(), anyBoolean());
    }

    @Test
    public void testSetSameCustomFieldLabelsDoesNotCauseUpdate() throws Exception {
        final Set<Label> expectedLabels = labelSetFor(CUSTOM_FIELD_ID, "some", "another");
        when(labelStore.getLabels(ISSUE_ID, CUSTOM_FIELD_ID)).thenReturn(expectedLabels);
        when(issueManager.getIssueObject(ISSUE_ID)).thenReturn(newMockIssueWithLabels("some", "another"));

        assertEquals(expectedLabels, labelManager.setLabels(null, ISSUE_ID, CUSTOM_FIELD_ID, asSet("some", "another"), false, true));
        Mockito.verify(issueUpdater, never()).doUpdate(anyObject(), anyBoolean());
    }

    @Test
    public void testAddSameLabelDoesNotCauseUpdate() throws Exception {
        when(labelStore.getLabels(ISSUE_ID, null)).thenReturn(labelSetFor("somelabel", "another", "yetanother"));
        when(issueManager.getIssueObject(ISSUE_ID)).thenReturn(newMockIssueWithLabels("somelabel", "another", "yetanother"));

        final Label actual = labelManager.addLabel(null, ISSUE_ID, "somelabel", false);

        assertEquals("somelabel", actual.getLabel());
        assertNotNull(actual.getId());
        assertTrue(actual.getId() > 0);

        Mockito.verify(issueUpdater, never()).doUpdate(anyObject(), anyBoolean());
    }

    @Test
    public void testAddSameCustomFieldLabelDoesNotCauseUpdate() throws Exception {
        when(labelStore.getLabels(ISSUE_ID, CUSTOM_FIELD_ID)).thenReturn(labelSetFor("somelabel", "another", "yetanother"));
        when(issueManager.getIssueObject(ISSUE_ID)).thenReturn(newMockIssueWithLabels("somelabel", "another", "yetanother"));

        final Label actual = labelManager.addLabel(null, ISSUE_ID, CUSTOM_FIELD_ID, "somelabel", false);

        assertEquals("somelabel", actual.getLabel());
        assertNotNull(actual.getId());
        assertTrue(actual.getId() > 0);

        Mockito.verify(issueUpdater, never()).doUpdate(anyObject(), anyBoolean());
    }

    @Test
    public void testSendNotificationFlagForUpdate() throws Exception {
        when(labelStore.getLabels(ISSUE_ID, null)).thenReturn(Sets.newHashSet());

        final Label expectedLabel = defaultSystemLabel();
        when(labelStore.addLabel(ISSUE_ID, null, DEFAULT_LABEL_TEXT)).thenReturn(expectedLabel);
        when(issueManager.getIssueObject(ISSUE_ID)).thenReturn(MOCK_ISSUE);

        assertEquals(expectedLabel, labelManager.addLabel(null, ISSUE_ID, DEFAULT_LABEL_TEXT, true));

        Mockito.verify(issueUpdater).
                doUpdate(argThat(mockitoIssueUpdateBeanMatcher("", DEFAULT_LABEL_TEXT, IssueFieldConstants.LABELS, true, ChangeItemBean.STATIC_FIELD)), Matchers.eq(false));
    }

    @Test
    public void testSetLabelsTriggersIssueUpdateForCorrectField() throws Exception {
        final Set<Label> expectedLabels = labelSetFor(CUSTOM_FIELD_ID, "somevalue");

        when(labelStore.getLabels(ISSUE_ID, CUSTOM_FIELD_ID)).thenReturn(Sets.newHashSet());
        when(labelStore.setLabels(ISSUE_ID, CUSTOM_FIELD_ID, Sets.newHashSet("somevalue"))).thenReturn(expectedLabels);
        when(issueManager.getIssueObject(ISSUE_ID)).thenReturn(MOCK_ISSUE);

        final CustomField answer = Mockito.mock(CustomField.class);
        when(answer.getIdAsLong()).thenReturn(CUSTOM_FIELD_ID);
        when(answer.getName()).thenReturn("thefield");
        when(customFieldManager.getCustomFieldObject(CUSTOM_FIELD_ID)).thenReturn(answer);

        final Set<Label> actual = labelManager.setLabels(null, ISSUE_ID, CUSTOM_FIELD_ID, asSet("somevalue"), false, true);

        assertEquals(expectedLabels, actual);
        Mockito.verify(issueUpdater).doUpdate(argThat(mockitoIssueUpdateBeanMatcher("", "somevalue", "thefield", false, ChangeItemBean.CUSTOM_FIELD)), Matchers.eq(false));

    }

    private ArgumentMatcher<IssueUpdateBean> mockitoIssueUpdateBeanMatcher(final String oldLabelsList, final String newLabelsList,
                                                                           final String fieldName, final boolean sendNotification, final String fieldType) {
        return new ArgumentMatcher<IssueUpdateBean>() {
            @Override
            public boolean matches(final Object argument) {
                final IssueUpdateBean actual = (IssueUpdateBean) argument;
                assertEquals(EventType.ISSUE_UPDATED_ID, actual.getEventTypeId());
                assertEquals(sendNotification, actual.isSendMail());
                assertEquals(1, actual.getChangeItems().size());

                final ChangeItemBean labelsChange = (ChangeItemBean) actual.getChangeItems().iterator().next();
                assertEquals(fieldType, labelsChange.getFieldType());
                assertEquals(fieldName, labelsChange.getField());
                assertEquals(oldLabelsList, labelsChange.getFromString());
                assertEquals(newLabelsList, labelsChange.getToString());
                return true;
            }
        };
    }

    private MutableIssue newMockIssueWithLabels(final String... labels) {
        return newMockIssueWithLabels(asSet(labels), null);
    }

    private MutableIssue newMockIssueWithLabels(final Set<String> labels, final Long customFieldId) {
        final MockIssue answer = newMockIssueBackedByGV(ISSUE_ID);
        answer.setLabels(labelSetFor(labels, customFieldId));
        return answer;
    }

    private Set<Label> labelSetFor(final String... labels) {
        return labelSetFor(asSet(labels), null);
    }

    private Set<Label> labelSetFor(final Long customFieldId, final String... labels) {
        return labelSetFor(asSet(labels), customFieldId);
    }

    private Set<Label> labelSetFor(final Set<String> labels, final Long customFieldId) {
        final Set<Label> answer = new HashSet<Label>();
        for (final String stringLabel : labels) {
            answer.add(labelFor(stringLabel, customFieldId));
        }
        return answer;
    }

    private <T> Set<T> asSet(final T... elems) {
        return Sets.newHashSet(elems);
    }

    private Label labelFor(final String labelString, final Long customFieldId) {
        return new Label(IDS.incrementAndGet(), ISSUE_ID, customFieldId, labelString);
    }

    private Label defaultSystemLabel() {
        return labelFor(DEFAULT_LABEL_TEXT, null);
    }

    private Label defaultCustomFieldLabel(final Long customFieldId) {
        return labelFor(DEFAULT_LABEL_TEXT, customFieldId);
    }

}
