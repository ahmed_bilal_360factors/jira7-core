package com.atlassian.jira.issue.customfields.view;


import com.atlassian.jira.issue.fields.CustomField;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.mockobjects.dynamic.Mock;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TestCustomFieldParamsImpl {
    Mock customFieldMock;
    CustomFieldParamsImpl params;

    @Before
    public void setUp() {
        customFieldMock = new Mock(CustomField.class);
        customFieldMock.expectAndReturn("getId", "customField_123");

        params = new CustomFieldParamsImpl((CustomField) customFieldMock.proxy());
    }

    @Test
    public void testGetDefaultParameterQueryString() {
        params.addValue(null, ImmutableList.of("Margaret"));
        assertEquals("customField_123=Margaret", params.getQueryString());
    }

    @Test
    public void testGetQueryStringIgnoresNulls() {
        params.addValue(null, null);
        assertEquals("", params.getQueryString());
    }

    @Test
    public void testGetSingleParameterQueryString() {
        params.addValue("name", ImmutableList.of("Bob"));
        assertEquals("customField_123:name=Bob", params.getQueryString());
    }

    @Test
    public void testGetMultiParameterQueryString() {
        params.addValue("name", ImmutableList.of("Angela", "Duncan"));
        assertEquals("customField_123:name=Angela&customField_123:name=Duncan", params.getQueryString());
    }

    @Test
    public void testGetMultiDifferentParameterQueryString() {
        params.addValue("name", ImmutableList.of("Angela", "Duncan"));
        params.addValue("age", ImmutableList.of("21"));
        params.addValue(null, ImmutableList.of("Bob"));
        String param1 = "customField_123:age=21";
        String param2 = "customField_123=Bob";
        String param3 = "customField_123:name=Angela";
        String param4 = "customField_123:name=Duncan";

        assertEquals((param1 + "&" + param2 + "&" + param3 + "&" + param4).length(), params.getQueryString().length());
        assertTrue(params.getQueryString().indexOf(param1) != -1);
        assertTrue(params.getQueryString().indexOf(param2) != -1);
        assertTrue(params.getQueryString().indexOf(param3) != -1);
        assertTrue(params.getQueryString().indexOf(param4) != -1);
    }

    @Test
    public void testEquals() {
        CustomField customField1 = Mockito.mock(CustomField.class);
        CustomField customField2 = Mockito.mock(CustomField.class);

        // both custom fields are null params same
        CustomFieldParamsImpl customFieldParams1 = new CustomFieldParamsImpl();
        CustomFieldParamsImpl customFieldParams2 = new CustomFieldParamsImpl();
        assertTrue(customFieldParams1.equals(customFieldParams2));

        // both custom fields are null params different
        customFieldParams1.addValue("name", Lists.<String>newArrayList("BOB"));
        assertFalse(customFieldParams1.equals(customFieldParams2));

        // first null, second not null
        customFieldParams2.setCustomField(customField1);
        assertFalse(customFieldParams1.equals(customFieldParams2));

        // first not null, second null
        customFieldParams1.setCustomField(customField1);
        customFieldParams2.setCustomField(null);
        assertFalse(customFieldParams1.equals(customFieldParams2));

        // both not null + same, params diff
        customFieldParams2.setCustomField(customField1);
        assertFalse(customFieldParams1.equals(customFieldParams2));

        // both not null, params same
        customFieldParams2.addValue("name", Lists.<String>newArrayList("BOB"));
        assertTrue(customFieldParams1.equals(customFieldParams2));

        // actually check customFields are different
        customFieldParams2.setCustomField(customField2);
        assertFalse(customFieldParams1.equals(customFieldParams2));

        assertTrue(customFieldParams1.equals(customFieldParams1));
    }
}
