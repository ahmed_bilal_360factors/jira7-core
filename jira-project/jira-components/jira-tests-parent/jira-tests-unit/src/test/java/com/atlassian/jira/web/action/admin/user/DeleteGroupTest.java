package com.atlassian.jira.web.action.admin.user;

import com.atlassian.collectors.CollectorsUtil;
import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.jira.application.ApplicationRole;
import com.atlassian.jira.application.ApplicationRoleManager;
import com.atlassian.jira.application.MockApplicationRole;
import com.atlassian.jira.bc.filter.SearchRequestService;
import com.atlassian.jira.bc.group.GroupService;
import com.atlassian.jira.bc.issue.worklog.TimeTrackingConfiguration;
import com.atlassian.jira.issue.AttachmentManager;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.subscription.SubscriptionManager;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.project.version.VersionManager;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.MockGroup;
import com.atlassian.jira.user.UserIssueHistoryManager;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

import static com.google.common.collect.Sets.newHashSet;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

/**
 * Tests {@link DeleteGroup}.
 *
 * @since 7.0
 */
@RunWith(MockitoJUnitRunner.class)
public class DeleteGroupTest {
    @Mock
    private IssueManager issueManager;
    @Mock
    private CustomFieldManager customFieldManager;
    @Mock
    private AttachmentManager attachmentManager;
    @Mock
    private ProjectManager projectManager;
    @Mock
    private PermissionManager permissionManager;
    @Mock
    private VersionManager versionManager;
    @Mock
    private SubscriptionManager subscriptionManager;
    @Mock
    private SearchRequestService searchRequestService;
    @Mock
    private GroupService groupService;
    @Mock
    private UserIssueHistoryManager userHistoryManager;
    @Mock
    private TimeTrackingConfiguration timeTrackingConfiguration;
    @Mock
    private GroupManager groupManager;
    @Mock
    private ApplicationRoleManager roleManager;

    private final ApplicationRole core = new MockApplicationRole().key("JC").name("core").groupNames("users", "developers", "grunts");
    private final ApplicationRole software = new MockApplicationRole().key("JForSW").name("software").groupNames("developers", "grunts");
    private final ApplicationRole servicedesk = new MockApplicationRole().key("JForSD").name("servicedesk").groupNames("agents", "grunts");

    @Before
    public void setupRoles() {
        Set<ApplicationUser> grunts = users("matt", "alex", "chris", "brenden", "becc");
        Set<ApplicationUser> developers = users("matt", "alex", "chris", "brenden", "eric");
        Set<ApplicationUser> agents = users("eric", "dave");
        Set<ApplicationUser> users = union(grunts, developers, agents);

        final Group gruntGroup = group("grunts");
        final Group developerGroup = group("developers");
        final Group agentsGroup = group("agents");
        final Group usersGroup = group("users");

        for (Group group : ImmutableList.of(gruntGroup, developerGroup, agentsGroup, usersGroup)) {
            when(groupManager.getGroup(group.getName())).thenReturn(group);
        }

        when(groupManager.getUsersInGroup(gruntGroup)).thenReturn(grunts);
        when(groupManager.getUsersInGroup(developerGroup)).thenReturn(developers);
        when(groupManager.getUsersInGroup(agentsGroup)).thenReturn(agents);
        when(groupManager.getUsersInGroup(usersGroup)).thenReturn(users);

        when(roleManager.getRolesForGroup(usersGroup)).thenReturn(set(core));
        when(roleManager.getRolesForGroup(developerGroup)).thenReturn(set(core, software));
        when(roleManager.getRolesForGroup(gruntGroup)).thenReturn(set(core, software, servicedesk));
        when(roleManager.getRolesForGroup(agentsGroup)).thenReturn(set(servicedesk));

        when(roleManager.getRoles()).thenReturn(set(core, software, servicedesk));
    }

    @Test
    public void getRolesForGroup() {
        DeleteGroup action = createAction();

        action.setName("non-existent-group");
        assertTrue(action.getRolesForGroup().isEmpty());

        action = createAction();
        action.setName("users");
        assertThat(action.getRolesForGroup(), equalTo(list(core)));

        action = createAction();
        action.setName("agents");
        assertThat(action.getRolesForGroup(), equalTo(list(servicedesk)));

        action = createAction();
        action.setName("developers");
        assertThat(action.getRolesForGroup(), equalTo(list(core, software)));

        action = createAction();
        action.setName("grunts");
        assertThat(action.getRolesForGroup(), equalTo(list(core, servicedesk, software)));
    }

    @Test
    public void getApplicationUsersAffected() {
        DeleteGroup action = createAction();

        action.setName("non-existent-group");
        assertThat(action.getApplicationUsersAffected(), equalTo(0));

        action = createAction();
        action.setName("users");
        assertThat(action.getApplicationUsersAffected(), equalTo(1));

        action = createAction();
        action.setName("agents");
        assertThat(action.getApplicationUsersAffected(), equalTo(2));

        action = createAction();
        action.setName("developers");
        assertThat(action.getApplicationUsersAffected(), equalTo(1));

        action = createAction();
        action.setName("grunts");
        assertThat(action.getApplicationUsersAffected(), equalTo(5));
    }

    private Group group(final String group) {
        return new MockGroup(group);
    }

    @SafeVarargs
    private static <T> List<T> list(T... things) {
        return ImmutableList.copyOf(things);
    }

    @SafeVarargs
    private static <T> Set<T> set(T... things) {
        return ImmutableSet.copyOf(things);
    }

    private static Set<ApplicationUser> users(String... things) {
        return Arrays.stream(things).map(MockApplicationUser::new).collect(CollectorsUtil.toImmutableSet());
    }

    @SafeVarargs
    private static <T> Set<T> union(Set<T>... groups) {
        Set<T> union = newHashSet(groups[0]);
        for (int i = 1; i < groups.length; i++) {
            union.addAll(groups[i]);
        }
        return union;
    }

    private DeleteGroup createAction() {
        return new DeleteGroup(
                issueManager, customFieldManager, attachmentManager, projectManager, permissionManager, versionManager,
                subscriptionManager, searchRequestService, groupService, userHistoryManager, timeTrackingConfiguration,
                groupManager, roleManager);
    }
}
