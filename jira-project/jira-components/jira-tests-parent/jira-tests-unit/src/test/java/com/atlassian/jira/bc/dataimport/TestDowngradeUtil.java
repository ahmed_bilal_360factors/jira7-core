package com.atlassian.jira.bc.dataimport;

import com.atlassian.jira.model.querydsl.UpgradeHistoryDTO;
import com.atlassian.jira.upgrade.DowngradeException;
import com.atlassian.jira.upgrade.DowngradeTask;
import com.atlassian.jira.upgrade.MissingDowngradeTaskException;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class TestDowngradeUtil {
    @Test
    public void findDowngradeTasksToRun() throws DowngradeException {
        final List<UpgradeHistoryDTO> historyItems = Arrays.asList(
                new UpgradeHistoryDTO(1L, "UpgradeTask300", "300", "complete", "Y"),
                new UpgradeHistoryDTO(2L, "UpgradeTask302", "302", "complete", "N"),
                new UpgradeHistoryDTO(3L, "UpgradeTask303", "303", "pending", "Y"),
                new UpgradeHistoryDTO(4L, "UpgradeTask307", "307", "complete", "Y")
        );
        final List<Integer> actual = DowngradeUtil.findDowngradeTasksToRun(historyItems, 300);
        final List<Integer> expected = Collections.singletonList(307);
        assertEquals(expected, actual);
    }

    @Test
    public void verifyTasksExist() throws DowngradeException {
        final List<Integer> downgradeTaskNumbers = Arrays.asList(400, 401, 402);
        final Map<Integer, DowngradeTask> downgradeTaskMap = new HashMap<Integer, DowngradeTask>();
        downgradeTaskMap.put(400, null);
        downgradeTaskMap.put(401, null);
        downgradeTaskMap.put(402, null);

        // all required tasks exist
        DowngradeUtil.verifyTasksExist(downgradeTaskNumbers, downgradeTaskMap);

        // remove one
        downgradeTaskMap.remove(401);
        try {
            DowngradeUtil.verifyTasksExist(downgradeTaskNumbers, downgradeTaskMap);
            fail("DowngradeException expected");
        } catch (MissingDowngradeTaskException e) {
            assertEquals("Cannot downgrade data - missing downgrade task 401", e.getMessage());
        }
    }
}
