package com.atlassian.jira.jql.validator;

import com.atlassian.jira.jql.operand.JqlOperandResolver;
import com.atlassian.jira.jql.operand.QueryLiteral;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.MessageSet;
import com.atlassian.jira.util.collect.CollectionBuilder;
import com.atlassian.jira.web.bean.MockI18nBean;
import com.atlassian.query.clause.TerminalClauseImpl;
import com.atlassian.query.operand.SingleValueOperand;
import com.atlassian.query.operator.Operator;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


public class TestLabelsValidator {

    @Test
    public void testValidate() {
        final ApplicationUser user = new MockApplicationUser("admin");

        final TerminalClauseImpl terminalClause = new TerminalClauseImpl("labels", Operator.EQUALS, "goodLabel");

        final JqlOperandResolver mockResolver = mock(JqlOperandResolver.class);
        when(mockResolver.getValues(user, terminalClause.getOperand(), terminalClause)).
                thenReturn(CollectionBuilder.newBuilder(new QueryLiteral(new SingleValueOperand(""), "goodLabel")).asList());


        final LabelsValidator validator = new LabelsValidator(mockResolver) {
            @Override
            I18nHelper getI18n(final ApplicationUser user) {
                return new MockI18nBean();
            }
        };

        final MessageSet messageSet = validator.validate(user, terminalClause);

        assertTrue(messageSet.getErrorMessages().isEmpty());
        assertTrue(messageSet.getWarningMessages().isEmpty());
    }

    @Test
    public void testValidateLabelTooLong() {
        ApplicationUser user = new MockApplicationUser("admin");

        final TerminalClauseImpl terminalClause = new TerminalClauseImpl("labels", Operator.EQUALS, "reallylonglabelreallylonglabelreallylonglabelreallylonglabelreallylonglabelreallylonglabelreallylonglabelreallylonglabelreallylonglabelreallylonglabelreallylonglabelreallylonglabelreallylonglabelreallylonglabelreallylonglabelreallylonglabelreallylonglabelr");

        final JqlOperandResolver mockResolver = mock(JqlOperandResolver.class);
        when(mockResolver.getValues(user, terminalClause.getOperand(), terminalClause)).
                thenReturn(CollectionBuilder.newBuilder(new QueryLiteral(new SingleValueOperand(""), "reallylonglabelreallylonglabelreallylonglabelreallylonglabelreallylonglabelreallylonglabelreallylonglabelreallylonglabelreallylonglabelreallylonglabelreallylonglabelreallylonglabelreallylonglabelreallylonglabelreallylonglabelreallylonglabelreallylonglabelr")).asList());


        final LabelsValidator validator = new LabelsValidator(mockResolver) {
            @Override
            I18nHelper getI18n(final ApplicationUser user) {
                return new MockI18nBean();
            }
        };

        final MessageSet messageSet = validator.validate(user, terminalClause);

        assertFalse(messageSet.getErrorMessages().isEmpty());
        assertTrue(messageSet.getWarningMessages().isEmpty());
    }

    @Test
    public void testValidateLabelInvalidChars() {
        ApplicationUser user = new MockApplicationUser("admin");

        final TerminalClauseImpl terminalClause = new TerminalClauseImpl("labels", Operator.EQUALS, "B AD");

        final JqlOperandResolver mockResolver = mock(JqlOperandResolver.class);
        when(mockResolver.getValues(user, terminalClause.getOperand(), terminalClause)).
                thenReturn(CollectionBuilder.newBuilder(new QueryLiteral(new SingleValueOperand(""), "B AD")).asList());


        final LabelsValidator validator = new LabelsValidator(mockResolver) {
            @Override
            I18nHelper getI18n(final ApplicationUser user) {
                return new MockI18nBean();
            }
        };

        final MessageSet messageSet = validator.validate(user, terminalClause);

        assertFalse(messageSet.getErrorMessages().isEmpty());
        assertTrue(messageSet.getWarningMessages().isEmpty());
    }

    @Test
    public void testValidateInvalidOperator() {
        ApplicationUser user = new MockApplicationUser("admin");

        final TerminalClauseImpl terminalClause = new TerminalClauseImpl("labels", Operator.GREATER_THAN, "goodlabel");

        final JqlOperandResolver mockResolver = mock(JqlOperandResolver.class);


        final LabelsValidator validator = new LabelsValidator(mockResolver) {
            @Override
            I18nHelper getI18n(final ApplicationUser user) {
                return new MockI18nBean();
            }
        };

        final MessageSet messageSet = validator.validate(user, terminalClause);

        assertFalse(messageSet.getErrorMessages().isEmpty());
        assertTrue(messageSet.getWarningMessages().isEmpty());
    }
}
