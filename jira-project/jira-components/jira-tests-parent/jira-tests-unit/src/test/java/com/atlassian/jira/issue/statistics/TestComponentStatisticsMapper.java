package com.atlassian.jira.issue.statistics;

import com.atlassian.jira.bc.project.component.ProjectComponent;
import com.atlassian.jira.bc.project.component.ProjectComponentManager;
import com.atlassian.jira.issue.IssueFieldConstants;
import com.atlassian.jira.issue.search.MockJqlSearchRequest;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.query.Query;
import com.atlassian.query.QueryImpl;
import com.atlassian.query.clause.AndClause;
import com.atlassian.query.clause.TerminalClauseImpl;
import com.atlassian.query.operand.EmptyOperand;
import com.atlassian.query.operator.Operator;
import org.hamcrest.Matchers;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.mockito.Mock;
import org.ofbiz.core.entity.GenericValue;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

public class TestComponentStatisticsMapper {
    @Rule
    public TestRule container = MockitoMocksInContainer.rule(this);

    @Mock
    @AvailableInContainer
    private ProjectComponentManager projectComponentManager;

    @Mock
    @AvailableInContainer
    private ProjectManager projectManager;

    @Test
    public void testEquals() {
        final ComponentStatisticsMapper mapper = new ComponentStatisticsMapper();
        assertThat(mapper, equalTo(mapper));
        assertEquals(mapper.hashCode(), mapper.hashCode());

        final ComponentStatisticsMapper mapper2 = new ComponentStatisticsMapper();
        assertThat(mapper2, equalTo(mapper));
        assertEquals(mapper.hashCode(), mapper2.hashCode());

        assertThat(mapper, Matchers.not(equalTo(null)));
        assertThat(mapper, Matchers.not(equalTo(new Object())));
        assertThat(mapper, Matchers.not(equalTo(new IssueTypeStatisticsMapper(null))));
    }


    private static final long PROJECT_ID = 13L;
    private static final long COMPONENT_ID = 555L;
    private static final String CLAUSE_NAME = "component";

    @Test
    public void testGetUrlSuffixForSomeComponent() throws Exception {
        final Project project = new MockProject(PROJECT_ID, "PR");
        final ProjectComponent component = new MockComponent(COMPONENT_ID, "New Component 555", project.getId());

        when(projectComponentManager.find(COMPONENT_ID)).thenReturn(component);
        when(projectManager.getProjectObj(PROJECT_ID)).thenReturn(project);

        final ComponentStatisticsMapper mapper = new ComponentStatisticsMapper();

        final TerminalClauseImpl projectClause = new TerminalClauseImpl(IssueFieldConstants.PROJECT, Operator.EQUALS, PROJECT_ID);
        final TerminalClauseImpl issueTypeClause = new TerminalClauseImpl(IssueFieldConstants.ISSUE_TYPE, Operator.EQUALS, "Bug");
        final AndClause totalExistingClauses = new AndClause(projectClause, issueTypeClause);

        final TerminalClauseImpl myComponentClause = new TerminalClauseImpl(CLAUSE_NAME, Operator.EQUALS, "New Component 555");
        final TerminalClauseImpl myComponentProjectClause = new TerminalClauseImpl(IssueFieldConstants.PROJECT, Operator.EQUALS, PROJECT_ID);

        final Query query = new QueryImpl(totalExistingClauses);
        final SearchRequest sr = new MockJqlSearchRequest(10000L, query);

        final SearchRequest urlSuffix = mapper.getSearchUrlSuffix(component, sr);
        final String modifiedClauses = urlSuffix.getQuery().getWhereClause().toString();

        assertThat(modifiedClauses, Matchers.containsString(projectClause.toString()));
        assertThat(modifiedClauses, Matchers.containsString(issueTypeClause.toString()));
        assertThat(modifiedClauses, Matchers.containsString(myComponentClause.toString()));
        assertThat(modifiedClauses, Matchers.containsString(myComponentProjectClause.toString()));
    }

    @Test
    public void testGetUrlSuffixForNullComponent() throws Exception {
        final ComponentStatisticsMapper mapper = new ComponentStatisticsMapper();

        final TerminalClauseImpl projectClause = new TerminalClauseImpl(IssueFieldConstants.PROJECT, Operator.EQUALS, PROJECT_ID);
        final TerminalClauseImpl issueTypeClause = new TerminalClauseImpl(IssueFieldConstants.ISSUE_TYPE, Operator.EQUALS, "Bug");
        final AndClause totalExistingClauses = new AndClause(projectClause, issueTypeClause);

        final TerminalClauseImpl myComponentClause = new TerminalClauseImpl(CLAUSE_NAME, Operator.IS, EmptyOperand.EMPTY);

        final Query query = new QueryImpl(totalExistingClauses);
        final SearchRequest sr = new MockJqlSearchRequest(10000L, query);

        final SearchRequest urlSuffix = mapper.getSearchUrlSuffix(null, sr);
        final String modifiedClauses = urlSuffix.getQuery().getWhereClause().toString();

        assertThat(modifiedClauses, Matchers.containsString(projectClause.toString()));
        assertThat(modifiedClauses, Matchers.containsString(issueTypeClause.toString()));
        assertThat(modifiedClauses, Matchers.containsString(myComponentClause.toString()));
    }

    @Test
    public void testGetUrlSuffixForNullSearchRequest() throws Exception {
        final ComponentStatisticsMapper mapper = new ComponentStatisticsMapper();

        assertNull(mapper.getSearchUrlSuffix(null, null));
    }

    static class MockComponent implements ProjectComponent {
        private final Long id;
        private final String name;
        private final Long projectId;

        MockComponent(final Long id, final String name, final Long projectId) {
            this.id = id;
            this.name = name;
            this.projectId = projectId;
        }

        public String getName() {
            return name;
        }

        public Long getId() {
            return id;
        }

        public Long getProjectId() {
            return projectId;
        }

        public String getDescription() {
            return null;
        }

        public String getLead() {
            return null;
        }

        @Override
        public ApplicationUser getComponentLead() {
            throw new UnsupportedOperationException("Not implemented");
        }

        public long getAssigneeType() {
            return 0;
        }

        public GenericValue getGenericValue() {
            return null;
        }
    }
}
