package com.atlassian.jira.issue.statistics;

import com.atlassian.jira.issue.customfields.searchers.transformer.CustomFieldInputHelper;
import com.atlassian.jira.issue.customfields.statistics.GroupPickerStatisticsMapper;
import com.atlassian.jira.issue.customfields.statistics.ProjectSelectStatisticsMapper;
import com.atlassian.jira.issue.customfields.statistics.TextStatisticsMapper;
import com.atlassian.jira.issue.customfields.statistics.UserPickerStatisticsMapper;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.search.ClauseNames;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.jql.builder.JqlClauseBuilderFactory;
import com.atlassian.jira.jql.builder.JqlClauseBuilderFactoryImpl;
import com.atlassian.jira.jql.util.JqlDateSupportImpl;
import com.atlassian.jira.junit.rules.MockitoContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.mock.project.MockVersion;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.project.version.VersionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.query.Query;
import com.atlassian.query.QueryImpl;
import com.atlassian.query.clause.AndClause;
import com.atlassian.query.clause.TerminalClauseImpl;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;

import static com.atlassian.query.operator.Operator.EQUALS;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestCustomFieldVersionStatisticsMapper {
    @Rule
    public final MockitoContainer mockInContainer = MockitoMocksInContainer.rule(this);

    @Mock
    private VersionManager versionManager;
    @Mock
    private CustomField customField;
    @Mock
    private CustomFieldInputHelper customFieldInputHelper;
    @Mock
    private JiraAuthenticationContext authenticationContext;

    @Before
    public void setUp() {
        mockInContainer.getMockComponentContainer().addMock(
                JqlClauseBuilderFactory.class,
                new JqlClauseBuilderFactoryImpl(new JqlDateSupportImpl(null))
        );
    }

    @Test
    public void testEquals() throws Exception {
        when(customField.getIdAsLong()).thenReturn(10001l);
        when(customField.getId()).thenReturn("customfield_10001");
        when(customField.getIdAsLong()).thenReturn(10001l);
        when(customField.getId()).thenReturn("customfield_10001");
        when(customField.getClauseNames()).thenReturn(new ClauseNames("test"));

        final CustomField customField1 = mock(CustomField.class);
        when(customField1.getIdAsLong()).thenReturn(10002l);
        when(customField1.getId()).thenReturn("customfield_10002");

        final CustomFieldVersionStatisticsMapper userStatisticsMapper = new CustomFieldVersionStatisticsMapper(customField, versionManager, authenticationContext, customFieldInputHelper, false);

        assertThat(userStatisticsMapper, equalTo(userStatisticsMapper));

        final CustomFieldVersionStatisticsMapper userStatisticsMapper1 = new CustomFieldVersionStatisticsMapper(customField, versionManager, authenticationContext, customFieldInputHelper, false);
        // As the mappers are using the same custom field they should be equal

        assertThat(userStatisticsMapper1, equalTo(userStatisticsMapper));
        assertThat(userStatisticsMapper1.hashCode(), equalTo(userStatisticsMapper.hashCode()));

        final CustomFieldVersionStatisticsMapper userStatisticsMapper2 = new CustomFieldVersionStatisticsMapper(customField1, versionManager, authenticationContext, customFieldInputHelper, false);

        // As the mappers are using different custom field they should *not* be equal
        assertThat(userStatisticsMapper2, not(equalTo(userStatisticsMapper)));
        assertThat(userStatisticsMapper2.hashCode(), not(equalTo(userStatisticsMapper.hashCode())));

        assertThat(userStatisticsMapper, not(equalTo(null)));
        assertThat(userStatisticsMapper, not(equalTo(new Object())));
        assertThat(userStatisticsMapper, not(equalTo(new IssueKeyStatisticsMapper())));
        assertThat(userStatisticsMapper, not(equalTo(new IssueTypeStatisticsMapper(null))));
        assertThat(userStatisticsMapper, not(equalTo(new UserPickerStatisticsMapper(null, null, null))));
        assertThat(userStatisticsMapper, not(equalTo(new TextStatisticsMapper(null))));
        assertThat(userStatisticsMapper, not(equalTo(new ProjectSelectStatisticsMapper(null, null))));
        assertThat(userStatisticsMapper, not(equalTo(new GroupPickerStatisticsMapper(customField, null, authenticationContext, customFieldInputHelper))));
    }

    @Test
    public void testGetUrlSuffixForUniqueCustomFieldName() throws Exception {
        final SearchRequest searchRequest = mock(SearchRequest.class);
        when(searchRequest.getQuery()).thenReturn(new QueryImpl());

        final ApplicationUser user = new MockApplicationUser("fred");
        when(authenticationContext.getUser()).thenReturn(user);

        when(customField.getIdAsLong()).thenReturn(10001l);
        when(customField.getId()).thenReturn("customfield_10001");
        final ClauseNames clauseNames = new ClauseNames("cf[10001]");
        when(customField.getClauseNames()).thenReturn(clauseNames);
        when(customField.getName()).thenReturn("My Custom CF");

        when(customFieldInputHelper.getUniqueClauseName(user, "cf[10001]", "My Custom CF")).thenReturn("My Custom CF");

        final CustomFieldVersionStatisticsMapper versionStatisticsMapper = new CustomFieldVersionStatisticsMapper(customField, versionManager, authenticationContext, customFieldInputHelper, false);

        final Version version = new MockVersion(123l, "Version 1.0", new MockProject(1000l, "DUMPTY"));

        final SearchRequest modifiedSearchRequest = versionStatisticsMapper.getSearchUrlSuffix(version, searchRequest);

        final Query query = modifiedSearchRequest.getQuery();

        final TerminalClauseImpl clause1 = new TerminalClauseImpl("project", EQUALS, "DUMPTY");
        final TerminalClauseImpl clause2 = new TerminalClauseImpl("My Custom CF", EQUALS, "Version 1.0");

        assertEquals(new QueryImpl(new AndClause(clause1, clause2)), query);
    }

    @Test
    public void testGetUrlSuffixForNonUniqueCustomFieldName() throws Exception {
        final SearchRequest searchRequest = mock(SearchRequest.class);
        when(searchRequest.getQuery()).thenReturn(new QueryImpl());

        final ApplicationUser user = new MockApplicationUser("fred");
        when(authenticationContext.getUser()).thenReturn(user);
        when(customField.getId()).thenReturn("customfield_10001");
        when(customField.getIdAsLong()).thenReturn(10001l);
        final ClauseNames clauseNames = new ClauseNames("cf[10001]");
        when(customField.getClauseNames()).thenReturn(clauseNames);
        when(customField.getName()).thenReturn("My Custom CF");

        when(customFieldInputHelper.getUniqueClauseName(user, "cf[10001]", "My Custom CF")).thenReturn("cf[10001]");

        final CustomFieldVersionStatisticsMapper versionStatisticsMapper = new CustomFieldVersionStatisticsMapper(customField, versionManager, authenticationContext, customFieldInputHelper, false);

        final Version version = new MockVersion(123l, "Version 1.0", new MockProject(1000l, "DUMPTY"));
        final SearchRequest modifiedSearchRequest = versionStatisticsMapper.getSearchUrlSuffix(version, searchRequest);

        final Query query = modifiedSearchRequest.getQuery();

        final TerminalClauseImpl clause1 = new TerminalClauseImpl("project", EQUALS, "DUMPTY");
        final TerminalClauseImpl clause2 = new TerminalClauseImpl("cf[10001]", EQUALS, "Version 1.0");
        assertEquals(new QueryImpl(new AndClause(clause1, clause2)), query);
    }

    @Test
    public void testGetValueFromLuceneField() {
        final Version version = new MockVersion(10020L, "Version 1.0", new MockProject(1000l, "DUMPTY"));
        when(versionManager.getVersion(10020L)).thenReturn(version);
        when(customField.getId()).thenReturn("customfield_10001");
        when(customField.getIdAsLong()).thenReturn(10001l);

        final CustomFieldVersionStatisticsMapper versionStatisticsMapper = new CustomFieldVersionStatisticsMapper(customField, versionManager, null, null, false);

        final Object nullReturn = versionStatisticsMapper.getValueFromLuceneField(null);
        assertNull(nullReturn);
        final Object anotherNull = versionStatisticsMapper.getValueFromLuceneField("-1");
        assertNull(anotherNull);
        final Object returnedVersion = versionStatisticsMapper.getValueFromLuceneField("10020");
        assertEquals(version, returnedVersion);
    }
}
