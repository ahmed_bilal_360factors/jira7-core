package com.atlassian.jira.upgrade.tasks.role;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.crowd.embedded.api.Group;
import com.google.common.collect.ImmutableSet;
import org.hamcrest.Description;
import org.hamcrest.TypeSafeDiagnosingMatcher;

import java.util.Set;

import static com.atlassian.jira.upgrade.tasks.role.TestUtils.asGroups;

/**
 * @since 7.0
 */
class ApplicationRoleMatcher extends TypeSafeDiagnosingMatcher<ApplicationRole> {
    private ApplicationKey key;
    private Set<Group> groups = ImmutableSet.of();
    private Set<Group> defaultGroups = ImmutableSet.of();

    public ApplicationRoleMatcher(final ApplicationKey key) {
        this.key = key;
    }

    static ApplicationRoleMatcher forKey(ApplicationKey key) {
        return new ApplicationRoleMatcher(key);
    }

    ApplicationRoleMatcher withGroups(String... groups) {
        this.groups = asGroups(groups);
        return this;
    }

    ApplicationRoleMatcher withGroups(Iterable<Group> groups) {
        this.groups = ImmutableSet.copyOf(groups);
        return this;
    }

    ApplicationRoleMatcher withDefaultGroups(String... groups) {
        this.defaultGroups = asGroups(groups);
        return this;
    }

    ApplicationRoleMatcher withDefaultGroups(Iterable<Group> groups) {
        this.defaultGroups = ImmutableSet.copyOf(groups);
        return this;
    }

    @Override
    protected boolean matchesSafely(final ApplicationRole item, final Description mismatchDescription) {
        if (item.key().equals(key) && item.groups().equals(groups) && item.defaultGroups().equals(defaultGroups)) {
            return true;
        } else {
            mismatchDescription.appendText(toString(item));
            return false;
        }
    }

    @Override
    public void describeTo(final Description description) {
        description.appendText(toString());
    }

    @Override
    public String toString() {
        return String.format("AppRole [%s, groups: %s, defaultGroups: %s]", key, groups, defaultGroups);
    }

    static String toString(ApplicationRole item) {
        return String.format("AppRole [%s, groups: %s, defaultGroups: %s]",
                item.key(), item.groups(), item.defaultGroups());
    }
}
