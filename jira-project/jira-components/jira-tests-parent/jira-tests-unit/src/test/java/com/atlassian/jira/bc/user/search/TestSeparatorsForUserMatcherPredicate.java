package com.atlassian.jira.bc.user.search;

import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.google.common.collect.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(Parameterized.class)
public class TestSeparatorsForUserMatcherPredicate {
    String separator;

    public TestSeparatorsForUserMatcherPredicate(String separator) {
        this.separator = separator;
    }

    @Parameters(name = "{0}")
    public static Object[] data() {
        return Lists.newArrayList(UserSearchUtilities.SEPARATORS).toArray();
    }

    @Test
    public void testSeparator_name() {
        final ApplicationUser testUser = new MockApplicationUser("Tester" + separator + "End", "FirstTest", "email");

        UserMatcherPredicate matcher = new UserMatcherPredicate("End", true);
        assertTrue(matcher.apply(testUser));
    }

    @Test
    public void testSeparator_name_other_levels() {
        final ApplicationUser testUser = new MockApplicationUser("Tester" + separator + "Into" + separator + "End", "FirstTest", "email");

        assertTrue(new UserMatcherPredicate("Into", true).apply(testUser));
        assertTrue(new UserMatcherPredicate("End", true).apply(testUser));
        assertTrue(new UserMatcherPredicate("Into" + separator + "End", true).apply(testUser));
        assertFalse(new UserMatcherPredicate("r" + separator + "Into", true).apply(testUser));
    }

    @Test
    public void testSeparator_display_name() {
        final ApplicationUser testUser = new MockApplicationUser("Tester", "FirstTest" + separator + "End", "email");

        UserMatcherPredicate matcher = new UserMatcherPredicate("End", true);
        assertTrue(matcher.apply(testUser));
    }

    @Test
    public void testSeparator_display_name_other_levels() {
        final ApplicationUser testUser = new MockApplicationUser("Tester", "FirstTest" + separator + "Into" + separator + "End", "email");

        assertTrue(new UserMatcherPredicate("Into", true).apply(testUser));
        assertTrue(new UserMatcherPredicate("End", true).apply(testUser));
        assertTrue(new UserMatcherPredicate("Into" + separator + "End", true).apply(testUser));
        assertFalse(new UserMatcherPredicate("t" + separator + "Into", true).apply(testUser));
    }

    @Test
    public void testSeparator_email() {
        final ApplicationUser testUser = new MockApplicationUser("Tester", "FirstTest", "email" + separator + "End");

        UserMatcherPredicate matcher = new UserMatcherPredicate("End", true);
        assertTrue(matcher.apply(testUser));
    }

    @Test
    public void testSeparator_email_other_levels() {
        final ApplicationUser testUser = new MockApplicationUser("Tester", "FirstTest", "email" + separator + "Into" + separator + "End");

        assertTrue(new UserMatcherPredicate("Into", true).apply(testUser));
        assertTrue(new UserMatcherPredicate("End", true).apply(testUser));
        assertTrue(new UserMatcherPredicate("Into" + separator + "End", true).apply(testUser));
        assertFalse(new UserMatcherPredicate("mail" + separator + "Into", true).apply(testUser));
    }

}
