package com.atlassian.jira.dashboard;

import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

public class TestJiraDirectoryPermissionService {
    @Rule
    public final MethodRule initMockito = MockitoJUnit.rule();

    @Mock
    private PermissionManager mockPermissionManager;
    @Mock
    private UserManager mockUserManager;
    @Mock
    private ApplicationUser admin;
    @Mock
    private ApplicationUser fred;

    @Test
    public void testCanConfigureDirectory() throws Exception {
        when(mockUserManager.getUserObject(null)).thenReturn(null);
        when(mockUserManager.getUserObject("admin")).thenReturn(admin);
        when(mockUserManager.getUserObject("fred")).thenReturn(fred);

        when(mockPermissionManager.hasPermission(Permissions.SYSTEM_ADMIN, (ApplicationUser) null)).thenReturn(false);
        when(mockPermissionManager.hasPermission(Permissions.SYSTEM_ADMIN, admin)).thenReturn(true);
        when(mockPermissionManager.hasPermission(Permissions.SYSTEM_ADMIN, fred)).thenReturn(false);

        JiraDirectoryPermissionService permissionService = new JiraDirectoryPermissionService(mockPermissionManager, mockUserManager);

        assertFalse(permissionService.canConfigureDirectory(null));
        assertTrue(permissionService.canConfigureDirectory("admin"));
        assertFalse(permissionService.canConfigureDirectory("fred"));
    }
}
