package com.atlassian.jira.bc.user.search;

import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import static org.junit.Assert.assertFalse;

/**
 * @see TestSeparatorsForUserMatcherPredicate
 * <p>
 * Testing that if a separator not supported is searched, it does return no results
 */
@RunWith(Parameterized.class)
public class TestInvalidSeparatorsForUserMatcherPredicate {
    String separator;

    public TestInvalidSeparatorsForUserMatcherPredicate(String separator) {
        this.separator = separator;
    }

    @Parameters(name = "{0}")
    public static Object[] data() {
        return new Object[]{"+", "="};
    }

    @Test
    public void testSeparator_name() {
        final ApplicationUser testUser = new MockApplicationUser("Tester" + separator + "End", "FirstTest", "email");

        UserMatcherPredicate matcher = new UserMatcherPredicate("End", true);
        assertFalse(matcher.apply(testUser));
    }

    @Test
    public void testSeparator_display_name() {
        final ApplicationUser testUser = new MockApplicationUser("Tester", "FirstTest" + separator + "End", "email");

        UserMatcherPredicate matcher = new UserMatcherPredicate("End", true);
        assertFalse(matcher.apply(testUser));
    }

    @Test
    public void testSeparator_email() {
        final ApplicationUser testUser = new MockApplicationUser("Tester", "FirstTest", "email" + separator + "End");

        UserMatcherPredicate matcher = new UserMatcherPredicate("End", true);
        assertFalse(matcher.apply(testUser));
    }

}
