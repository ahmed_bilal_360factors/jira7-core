package com.atlassian.jira.issue.search.searchers.renderer;

import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.search.SearchContext;
import com.atlassian.jira.issue.status.MockStatus;
import com.atlassian.jira.issue.status.Status;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.template.VelocityTemplatingEngine;
import com.atlassian.jira.util.velocity.VelocityRequestContextFactory;
import com.atlassian.jira.web.FieldVisibilityManager;
import com.atlassian.jira.workflow.JiraWorkflow;
import com.atlassian.jira.workflow.WorkflowException;
import com.atlassian.jira.workflow.WorkflowManager;
import com.google.common.collect.ImmutableList;
import org.hamcrest.Matchers;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import java.util.Collection;

import static org.hamcrest.Matchers.empty;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestStatusSearchRenderer {
    @Rule
    public MethodRule initMockito = MockitoJUnit.rule();
    @Mock
    private FieldVisibilityManager fieldVisibilityManager;
    @Mock
    private VelocityTemplatingEngine templatingEngine;
    @Mock
    private ApplicationProperties applicationProperties;
    @Mock
    private VelocityRequestContextFactory velocityRequestContextFactory;
    @Mock
    private ProjectManager projectManager;
    @Mock
    private WorkflowManager workflowManager;
    @Mock
    private ConstantsManager constantsManager;
    @Mock
    private SearchContext searchContext;
    @InjectMocks
    private StatusSearchRenderer renderer;

    @Test
    public void testSelectListOptionsNullProjectIdsAndNoWorkFlows() throws Exception {
        when(workflowManager.getActiveWorkflows()).thenReturn(ImmutableList.of());

        when(searchContext.getProjectIds()).thenReturn(null);

        final Collection<Status> selectListOptions = renderer.getSelectListOptions(searchContext);
        assertThat(selectListOptions, empty());
    }

    @Test
    public void testSelectListOptionsNullProjectIdsAndWorkFlowsException() throws Exception {
        final ImmutableList<Status> statuses = ImmutableList.of();
        when(constantsManager.getStatusObjects()).thenReturn(statuses);
        when(workflowManager.getActiveWorkflows()).thenThrow(new WorkflowException("bugger"));
        when(searchContext.getProjectIds()).thenReturn(null);

        final Collection<Status> selectListOptions = renderer.getSelectListOptions(searchContext);

        assertThat(selectListOptions, Matchers.sameInstance(statuses));
    }

    @Test
    public void testGetSelectListOptionsProjectAndIssueTypeDefined() throws Exception {
        final JiraWorkflow workflow = mock(JiraWorkflow.class);
        final Long pid = 10L;
        final String tid = "20";
        final MockProject project = new MockProject(pid);
        final MockStatus status1 = new MockStatus("1", "Status 1");
        status1.setSequence(1L);
        final MockStatus status2 = new MockStatus("2", "Status 2");
        status2.setSequence(2L);

        when(searchContext.getProjectIds()).thenReturn(ImmutableList.of(pid));
        when(searchContext.getIssueTypeIds()).thenReturn(ImmutableList.of(tid));
        when(projectManager.getProjectObj(pid)).thenReturn(project);
        when(workflowManager.getWorkflow(pid, tid)).thenReturn(workflow);
        when(workflow.getLinkedStatusObjects()).thenReturn(ImmutableList.of(status2, status1));

        final Collection<Status> selectListOptions = renderer.getSelectListOptions(searchContext);
        assertThat(selectListOptions, Matchers.hasItems(status1, status2));
    }

    @Test
    public void testGetSelectListOptionsProjectDefined() throws Exception {
        final JiraWorkflow workflow = mock(JiraWorkflow.class);
        final Long pid = 10L;
        final String tid = "20";
        final MockProject project = new MockProject(pid);

        final MockStatus status1 = new MockStatus("1", "Status 1");
        status1.setSequence(1L);
        final MockStatus status2 = new MockStatus("2", "Status 2");
        status2.setSequence(2L);

        when(searchContext.getProjectIds()).thenReturn(ImmutableList.of(pid));
        when(searchContext.getIssueTypeIds()).thenReturn(ImmutableList.of());
        when(constantsManager.getAllIssueTypeIds()).thenReturn(ImmutableList.of(tid));
        when(projectManager.getProjectObj(pid)).thenReturn(project);
        when(workflowManager.getWorkflow(pid, tid)).thenReturn(workflow);
        when(workflow.getLinkedStatusObjects()).thenReturn(ImmutableList.of(status2, status1));

        final Collection<Status> selectListOptions = renderer.getSelectListOptions(searchContext);

        assertThat(selectListOptions, Matchers.hasItems(status1, status2));
    }

    @Test
    public void testGetSelectListOptionsProjectDefinedButDoesntExist() throws Exception {
        final Long pid = 10L;
        final String tid = "20";
        when(searchContext.getProjectIds()).thenReturn(ImmutableList.of(pid));
        when(searchContext.getIssueTypeIds()).thenReturn(ImmutableList.of());
        when(constantsManager.getAllIssueTypeIds()).thenReturn(ImmutableList.of(tid));
        when(projectManager.getProjectObj(pid)).thenReturn(null);


        final Collection<Status> selectListOptions = renderer.getSelectListOptions(searchContext);
        assertThat(selectListOptions, empty());
    }

    @Test
    public void testGetSelectListOptionsNoProjectDefined() throws Exception {
        final JiraWorkflow workflow = mock(JiraWorkflow.class);
        final MockStatus status1 = new MockStatus("1", "Status 1");
        status1.setSequence(1L);
        final MockStatus status2 = new MockStatus("2", "Status 2");
        status2.setSequence(2L);
        when(searchContext.getProjectIds()).thenReturn(ImmutableList.of());
        when(searchContext.getIssueTypeIds()).thenReturn(ImmutableList.of());
        when(workflowManager.getActiveWorkflows()).thenReturn(ImmutableList.of(workflow));
        when(workflow.getLinkedStatusObjects()).thenReturn(ImmutableList.of(status2, status1));


        final Collection<Status> selectListOptions = renderer.getSelectListOptions(searchContext);

        assertThat(selectListOptions, Matchers.hasItems(status1, status2));
    }
}
