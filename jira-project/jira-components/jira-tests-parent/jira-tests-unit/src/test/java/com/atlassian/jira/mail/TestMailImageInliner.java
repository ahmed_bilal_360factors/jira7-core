package com.atlassian.jira.mail;

import com.atlassian.jira.avatar.AvatarManager;
import com.atlassian.jira.avatar.AvatarService;
import com.atlassian.jira.avatar.AvatarTranscoder;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.AttachmentManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.attachment.Attachment;
import com.atlassian.jira.issue.thumbnail.ThumbnailManager;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.io.InputStreamConsumer;
import com.atlassian.jira.web.ServletContextProviderListener;
import com.google.common.collect.ImmutableList;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collection;
import javax.mail.BodyPart;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;

import static com.google.common.collect.Iterables.size;
import static org.apache.commons.lang.StringUtils.countMatches;
import static org.apache.commons.lang.StringUtils.substringBetween;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalToIgnoringCase;
import static org.hamcrest.Matchers.equalToIgnoringWhiteSpace;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.core.Is.is;
import static org.mockito.AdditionalMatchers.not;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.startsWith;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;

@RunWith(MockitoJUnitRunner.class)
public class TestMailImageInliner {

    @Mock
    private AvatarService avatarService;

    @Mock
    private AvatarTranscoder avatarTranscoder;

    @Mock
    private UserManager userManager;

    @Mock
    private AvatarManager avatarManager;

    @Mock
    private ApplicationProperties applicationProperties;

    @Mock
    private AttachmentManager attachmentManager;

    @Mock
    private ThumbnailManager thumbnailManager;

    @Mock
    private ServletContext servletContext;

    @Mock
    private Attachment attachment1;

    @Mock
    private Attachment attachment2;

    @Mock
    private Issue issue;

    @InjectMocks
    private MailImageInlinerImpl mailImageInliner;

    @Before
    public void setup() {
        new ServletContextProviderListener().contextInitialized(
                new ServletContextEvent(servletContext)
        );

        doReturn("http://localhost:2990/").when(applicationProperties).getString(APKeys.JIRA_BASEURL);
        doReturn("img/png").when(servletContext).getMimeType(anyString());
        doReturn(IOUtils.toInputStream("this is a test")).when(servletContext).getResourceAsStream(anyString());

        doReturn(10506L).when(attachment1).getId();
        doReturn(10507L).when(attachment2).getId();
    }

    @Test
    public void testInlineHtmlImagesWithNoAttachmentsAndContext() throws MalformedURLException {
        doReturn(new URL("http:")).when(servletContext).getResource(not(startsWith("cid:")));

        final MailImageInliner.InlinedEmailBody inlinedEmailBody = mailImageInliner.inlineImages(html());

        assertThat(inlinedEmailBody, notNullValue());

        final String newHtml = inlinedEmailBody.getHtml();
        assertThat(countMatches(newHtml, "src=\"http://localhost:2990/servicedesk/customershim/secure/thumbnail/10507/_thumb_10507.png\""), is(0));
        assertThat(countMatches(newHtml, "src=\"http://localhost:2990/servicedesk/customershim/secure/attachment/10506/10506_2015-02-14+15.21.17.jpg\""), is(0));

        assertThat(countMatches(newHtml, "src=\"cid:jira-generated-image-static-_thumb_10507-"), is(1));
        assertThat(countMatches(newHtml, "src=\"cid:jira-generated-image-static-10506_2015-02-14+15.21.17-"), is(1));

        assertThat(StringUtils.countMatches(newHtml, "<img class=\"logo\" src=\"cid:jsd-content-id-11eb42ef-399d-4062-a6a8-0af9a5a8454c\""), is(1));

        final Iterable<BodyPart> bodyParts = inlinedEmailBody.getBodyParts();
        assertThat(bodyParts, notNullValue());
        assertThat(size(bodyParts), is(2));

        verifyZeroInteractions(thumbnailManager, attachmentManager);
    }

    @Test
    public void testInlineHtmlImagesWithNoAttachmentsAndNotContext() throws MalformedURLException {
        doReturn(null).when(servletContext).getResource(not(startsWith("cid:")));

        final MailImageInliner.InlinedEmailBody inlinedEmailBody = mailImageInliner.inlineImages(html());

        assertThat(inlinedEmailBody, notNullValue());

        final String newHtml = inlinedEmailBody.getHtml();
        assertThat(countMatches(newHtml, "src=\"cid:jira-generated-image-static-_thumb_10507-"), is(0));
        assertThat(countMatches(newHtml, "src=\"cid:jira-generated-image-static-10506_2015-02-14+15.21.17-"), is(0));

        assertThat(countMatches(newHtml, "src=\"http://localhost:2990/servicedesk/customershim/secure/thumbnail/10507/_thumb_10507.png\""), is(1));
        assertThat(countMatches(newHtml, "src=\"http://localhost:2990/servicedesk/customershim/secure/attachment/10506/10506_2015-02-14+15.21.17.jpg\""), is(1));

        assertThat(StringUtils.countMatches(newHtml, "<img class=\"logo\" src=\"cid:jsd-content-id-11eb42ef-399d-4062-a6a8-0af9a5a8454c\""), is(1));

        final Iterable<BodyPart> bodyParts = inlinedEmailBody.getBodyParts();
        assertThat(bodyParts, notNullValue());
        assertThat(size(bodyParts), is(0));

        verifyZeroInteractions(thumbnailManager, attachmentManager);
    }

    @Test
    public void testInlineHtmlImagesWithAttachmentsAndNoContext() throws IOException {
        doReturn(null).when(servletContext).getResource(not(startsWith("cid:")));

        final Collection<Attachment> issueAttachments = ImmutableList.of(
                attachment1,
                attachment2
        );

        doReturn(issueAttachments).when(issue).getAttachments();

        final MailImageInliner.InlinedEmailBody inlinedEmailBody = mailImageInliner.inlineImages(html(), issue);

        assertThat(inlinedEmailBody, notNullValue());

        final String newHtml = inlinedEmailBody.getHtml();

        assertThat(countMatches(newHtml, "<a id=\"10507_thumb\" href=\"http://localhost:2990/servicedesk/customershim/secure/attachment/10507/10507_2015-02-14+11.32.24.jpg\" title=\"2015-02-14 11.32.24.jpg\"> <img src=\"cid:jira-generated-image-static-"), is(1));
        assertThat(countMatches(newHtml, "<span class=\"image-wrap\" style=\"\"> <img src=\"cid:jira-generated-image-static-"), is(1));

        assertThat(countMatches(newHtml, "src=\"http://localhost:2990/servicedesk/customershim/secure/thumbnail/10507/_thumb_10507.png\""), is(0));
        assertThat(countMatches(newHtml, "src=\"http://localhost:2990/servicedesk/customershim/secure/attachment/10506/10506_2015-02-14+15.21.17.jpg\""), is(0));

        assertThat(countMatches(newHtml, "<img class=\"logo\" src=\"cid:jsd-content-id-11eb42ef-399d-4062-a6a8-0af9a5a8454c\""), is(1));

        // make sure the new html is not unknowingly changed in any way
        assertThat(newHtml, Matchers.startsWith("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">"));

        final Iterable<BodyPart> bodyParts = inlinedEmailBody.getBodyParts();
        assertThat(bodyParts, notNullValue());
        assertThat(size(bodyParts), is(2));

        verify(attachmentManager).streamAttachmentContent(eq(attachment1), any(InputStreamConsumer.class));
        verify(thumbnailManager).streamThumbnailContent(eq(attachment2), any(InputStreamConsumer.class));

        verifyNoMoreInteractions(attachmentManager, thumbnailManager);
    }

    @Test
    public void testInlineHtmlImagesWithAttachmentsAndContext() throws IOException {
        doReturn(new URL("http:")).when(servletContext).getResource(startsWith("servicedesk/customershim/secure/attachment/10506/10506_2015-02-14+15.21.17.jpg"));

        final Collection<Attachment> issueAttachments = ImmutableList.of(
                attachment1,
                attachment2
        );

        doReturn(issueAttachments).when(issue).getAttachments();

        final MailImageInliner.InlinedEmailBody inlinedEmailBody = mailImageInliner.inlineImages(html(), issue);

        assertThat(inlinedEmailBody, notNullValue());

        final String newHtml = inlinedEmailBody.getHtml();

        assertThat(countMatches(newHtml, "<a id=\"10507_thumb\" href=\"http://localhost:2990/servicedesk/customershim/secure/attachment/10507/10507_2015-02-14+11.32.24.jpg\" title=\"2015-02-14 11.32.24.jpg\"> <img src=\"cid:jira-generated-image-static-"), is(1));
        assertThat(countMatches(newHtml, "src=\"cid:jira-generated-image-static-10506_2015-02-14+15.21.17-"), is(1));

        assertThat(countMatches(newHtml, "src=\"http://localhost:2990/servicedesk/customershim/secure/thumbnail/10507/_thumb_10507.png\""), is(0));
        assertThat(countMatches(newHtml, "src=\"http://localhost:2990/servicedesk/customershim/secure/attachment/10506/10506_2015-02-14+15.21.17.jpg\""), is(0));

        assertThat(countMatches(newHtml, "<img class=\"logo\" src=\"cid:jsd-content-id-11eb42ef-399d-4062-a6a8-0af9a5a8454c\""), is(1));

        // make sure the new html is not unknowingly changed in any way
        assertThat(newHtml, Matchers.startsWith("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">"));

        final Iterable<BodyPart> bodyParts = inlinedEmailBody.getBodyParts();
        assertThat(bodyParts, notNullValue());
        assertThat(size(bodyParts), is(2));

        verify(thumbnailManager).streamThumbnailContent(eq(attachment2), any(InputStreamConsumer.class));

        verifyNoMoreInteractions(thumbnailManager);
        verifyZeroInteractions(attachmentManager);
    }

    @Test
    public void testCacheSameImageAttachmentsAndNoContext() throws IOException {
        doReturn(null).when(servletContext).getResource(anyString());

        final Collection<Attachment> issueAttachments = ImmutableList.of(
                attachment1,
                attachment2
        );

        doReturn(issueAttachments).when(issue).getAttachments();

        final String htmlWithSameImage = "<html>\n" +
                " <body>\n" +
                "  <p>1<img src=\"http://localhost:2990/servicedesk/customershim/secure/thumbnail/10507/_thumb_10507.png\"></p>\n" +
                "  <p>2<img src=\"http://localhost:2990/servicedesk/customershim/secure/thumbnail/10507/_thumb_10507.png\"></p>\n" +
                "  <p>3<img src=\"http://localhost:2990/servicedesk/customershim/secure/attachment/10507/full_file.png\"></p>\n" +
                " </body>\n" +
                "</html>";

        final MailImageInliner.InlinedEmailBody inlinedEmailBody = mailImageInliner.inlineImages(htmlWithSameImage, issue);

        assertThat(inlinedEmailBody, notNullValue());

        final String newHtml = inlinedEmailBody.getHtml();

        final String path1 = substringBetween(newHtml, "1<img src=\"", "\"");
        final String path2 = substringBetween(newHtml, "2<img src=\"", "\"");
        final String path3 = substringBetween(newHtml, "3<img src=\"", "\"");

        assertThat(path1, notNullValue());
        assertThat(path2, notNullValue());
        assertThat(path3, notNullValue());

        assertThat(path1, Matchers.startsWith("cid:"));
        assertThat(path2, Matchers.startsWith("cid:"));
        assertThat(path3, Matchers.startsWith("cid:"));

        assertThat(path1, is(path2));
        assertThat(path2, Matchers.not(is(path3)));

        assertThat(countMatches(newHtml, "src=\"http://localhost:2990/servicedesk/customershim/secure/thumbnail/10507/_thumb_10507.png\""), is(0));
        assertThat(countMatches(newHtml, "src=\"http://localhost:2990/servicedesk/customershim/secure/attachment/10507/full_file.png\""), is(0));

        final Iterable<BodyPart> bodyParts = inlinedEmailBody.getBodyParts();
        assertThat(bodyParts, notNullValue());
        assertThat(size(bodyParts), is(2));
    }

    @Test
    public void testCheckParentForThumbnailAndNoContext() throws IOException {
        doReturn(null).when(servletContext).getResource(anyString());

        final Collection<Attachment> issueAttachments = ImmutableList.of(
                attachment1,
                attachment2
        );

        doReturn(issueAttachments).when(issue).getAttachments();

        // link id tells us still a thumbnail
        final String htmlWithThumnailBadHtml = "<html>\n" +
                " <body>\n" +
                "<a id=\"10507_thumb\"\n" +
                " href=\"http://localhost:2990/servicedesk/customershim/secure/attachment/10507/10507_2015-02-14+11.32.24.jpg\"\n" +
                " title=\"2015-02-14 11.32.24.jpg\">\n" +
                " <img src=\"http://localhost:2990/servicedesk/customershim/secure/attachment/10507/_thumb_10507.png\" style=\"border: 0px solid black\"/></a>\n" +
                " </body>\n" +
                "</html>";

        final MailImageInliner.InlinedEmailBody inlinedEmailBody = mailImageInliner.inlineImages(htmlWithThumnailBadHtml, issue);

        assertThat(inlinedEmailBody, notNullValue());

        final String newHtml = inlinedEmailBody.getHtml();

        assertThat(countMatches(newHtml, "<a id=\"10507_thumb\" href=\"http://localhost:2990/servicedesk/customershim/secure/attachment/10507/10507_2015-02-14+11.32.24.jpg\" title=\"2015-02-14 11.32.24.jpg\"> <img src=\"cid:jira-generated-image-static-"), is(1));

        assertThat(countMatches(newHtml, "<img src=\"http://localhost:2990/servicedesk/customershim/secure/attachment/10507/_thumb_10507.png\""), is(0));

        final Iterable<BodyPart> bodyParts = inlinedEmailBody.getBodyParts();
        assertThat(bodyParts, notNullValue());
        assertThat(size(bodyParts), is(1));

        verify(thumbnailManager).streamThumbnailContent(eq(attachment2), any(InputStreamConsumer.class));

        verifyNoMoreInteractions(thumbnailManager);
        verifyZeroInteractions(attachmentManager);
    }

    @Test
    public void testLeaveUnknownPathsAlone() throws IOException {
        doReturn(null).when(servletContext).getResource(anyString());

        final String htmlUnknownPath = "<html> <head></head> <body> <img src=\"http://localhost:2990/servicedesk/customershim/secure/unknown/10507/full_file.png\"> </body> </html>";

        final MailImageInliner.InlinedEmailBody inlinedEmailBody = mailImageInliner.inlineImages(htmlUnknownPath, issue);

        assertThat(inlinedEmailBody, notNullValue());

        final String newHtml = inlinedEmailBody.getHtml();

        assertThat(newHtml, equalToIgnoringWhiteSpace(htmlUnknownPath));

        final Iterable<BodyPart> bodyParts = inlinedEmailBody.getBodyParts();
        assertThat(bodyParts, notNullValue());
        assertThat(size(bodyParts), is(0));
    }

    @Test
    public void testFindIdCorrectly() throws IOException {
        doReturn(null).when(servletContext).getResource(anyString());

        final Collection<Attachment> issueAttachments = ImmutableList.of(
                attachment1,
                attachment2
        );

        doReturn(issueAttachments).when(issue).getAttachments();

        final String htmlPath = "<html> <head></head> <body> <img src=\"http://localhost:2990/jira/secure/thumbnail/this/secure/thumbnail/10507/full_file.png\" /> </body> </html>";

        final MailImageInliner.InlinedEmailBody inlinedEmailBody = mailImageInliner.inlineImages(htmlPath, issue);

        assertThat(inlinedEmailBody, notNullValue());

        final String newHtml = inlinedEmailBody.getHtml();

        assertThat(countMatches(newHtml, "<img src=\"cid:jira-generated-image-static-"), is(1));
        assertThat(countMatches(newHtml, "<img src=\"http://localhost:2990/jira/secure/thumbnail/this/secure/thumbnail/10507/full_file.png"), is(0));

        final Iterable<BodyPart> bodyParts = inlinedEmailBody.getBodyParts();
        assertThat(bodyParts, notNullValue());
        assertThat(size(bodyParts), is(1));

        verify(thumbnailManager).streamThumbnailContent(eq(attachment2), any(InputStreamConsumer.class));

        verifyNoMoreInteractions(thumbnailManager);
        verifyZeroInteractions(attachmentManager);
    }

    @Test
    public void testDoNothingWithImagesNoFromInstance() throws MalformedURLException {
        doReturn("http://you:2990/jira").when(applicationProperties).getString(APKeys.JIRA_BASEURL);

        doReturn(null).when(servletContext).getResource(anyString());

        final Collection<Attachment> issueAttachments = ImmutableList.of(
                attachment1,
                attachment2
        );

        doReturn(issueAttachments).when(issue).getAttachments();

        final String htmlPath = "<html> <head></head> <body> <img src=\"http://localhost:2990/jira/secure/thumbnail/10507/full_file.png\" /> </body> </html>";

        final MailImageInliner.InlinedEmailBody inlinedEmailBody = mailImageInliner.inlineImages(htmlPath, issue);

        assertThat(inlinedEmailBody, notNullValue());

        final String newHtml = inlinedEmailBody.getHtml();

        assertThat(countMatches(newHtml, "<img src=\"cid:jira-generated-image-static-"), is(0));
        assertThat(countMatches(newHtml, "<img src=\"http://localhost:2990/jira/secure/thumbnail/10507/full_file.png"), is(1));

        final Iterable<BodyPart> bodyParts = inlinedEmailBody.getBodyParts();
        assertThat(bodyParts, notNullValue());
        assertThat(size(bodyParts), is(0));

        verifyZeroInteractions(attachmentManager, thumbnailManager);
    }

    @Test
    public void testProduceCleanHtmlWhenNoAttachments() {
        final String notHtml = "invalid - what is this?";

        final MailImageInliner.InlinedEmailBody inlinedEmailBody = mailImageInliner.inlineImages(notHtml);

        assertThat(inlinedEmailBody, notNullValue());

        final String newHtml = inlinedEmailBody.getHtml();

        final String expectCleanerHtml = "<html>\n" +
                " <head></head>\n" +
                " <body>\n" +
                "  invalid - what is this?\n" +
                " </body>\n" +
                "</html>";

        assertThat(newHtml, equalToIgnoringCase(expectCleanerHtml));
        assertThat(size(inlinedEmailBody.getBodyParts()), is(0));
    }

    @Test
    public void testProduceCleanHtmlWhenAttachments() {
        final String notHtml = "invalid - what is this?";


        final Collection<Attachment> issueAttachments = ImmutableList.of(
                attachment1,
                attachment2
        );

        doReturn(issueAttachments).when(issue).getAttachments();

        final MailImageInliner.InlinedEmailBody inlinedEmailBody = mailImageInliner.inlineImages(notHtml, issue);

        assertThat(inlinedEmailBody, notNullValue());

        final String newHtml = inlinedEmailBody.getHtml();

        final String expectCleanerHtml = "<html>\n" +
                " <head></head>\n" +
                " <body>\n" +
                "  invalid - what is this? \n" +
                " </body>\n" +
                "</html>";

        assertThat(newHtml, equalToIgnoringWhiteSpace(expectCleanerHtml));
        assertThat(size(inlinedEmailBody.getBodyParts()), is(0));
    }

    private String html() {
        return "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\n" +
                "<html xmlns=\"http://www.w3.org/1999/xhtml\">\n" +
                "<head>\n" +
                "    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"/>\n" +
                "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"/>\n" +
                "    <base href=\"http://localhost:2990\"/>\n" +
                "</head>\n" +
                "<body class=\"sd-conversational\"\n" +
                "      style=\"font-size: 16px; font-family: sans-serif; background-color: #f5f5f5; background-color: transparent; font-family: Arial, FreeSans, Helvetica, sans-serif; font-size: inherit\">\n" +
                "<div class=\"sd-email-custommarker\"\n" +
                "     style=\"padding-top: 10px; padding-bottom: 10px; text-align: center; color: #707070; font-size: 12px; color: #999; font-size: 12px; padding-top: 0; padding-bottom: 12px; text-align: left\">\n" +
                "    =E2=80=94Write replies above=E2=80=94\n" +
                "</div>\n" +
                "<table class=\"email-content-table\" style=\"width: 100%; font-family: sans-serif; width: 100%\">\n" +
                "    <tbody>\n" +
                "    <tr>\n" +
                "        <td>\n" +
                "            <p style=\"margin: 0 0 1.350em\"></p>\n" +
                "            <p style=\"margin: 0 0 1.350em\">Hi System Administrator,</p>=20\n" +
                "            <p style=\"margin: 0 0 1.350em\"><span class=\"image-wrap\" style=\"\"><a id=\"10507_thumb\"\n" +
                "                                                                                href=\"http://localhost:2990/servicedesk/customershim/secure/attachment/10507/10507_2015-02-14+11.32.24.jpg\"\n" +
                "                                                                                title=\"2015-02-14 11.32.24.jpg\">\n" +
                "                   <img src=\"http://localhost:2990/servicedesk/customershim/secure/thumbnail/10507/_thumb_10507.png\" style=\"border: 0px solid black\"/></a></span></p>\n" +
                "            <p style=\"margin: 0 0 1.350em\"><span class=\"image-wrap\" style=\"\">\n" +
                "                   <img src=\"http://localhost:2990/servicedesk/customershim/secure/attachment/10506/10506_2015-02-14+15.21.17.jpg\" style=\"border: 0px solid black\">" +
                "               </span></p>=20\n" +
                "            <p style=\"margin: 0 0 1.350em\">=E2=80=93 Administrator</p>\n" +
                "            <p class=\"additional-information\"\n" +
                "               style=\"color:#999; font-size: 12px; line-height: 1.700; padding-top: 12px; margin: 0 0 1.350em\">You can\n" +
                "                reply directly to this email to add any further comments or attachments.<br/>See request details and\n" +
                "                updates for <a class=\"blue-link\"\n" +
                "                               href=\"http://localhost:2990/servicedesk/customer/portal/1/DESK-3?sda_source=notification-email\"\n" +
                "                               style=\"color: #00f; text-decoration: none\"> #DESK-3</a> - &quot;ppp&quot;</p>\n" +
                "        </td>\n" +
                "    </tr>\n" +
                "    <tr>\n" +
                "        <td class=\"sd-conversational-foot-note\" style=\"padding-top: 32px; color: #999; font-size: 12px\">\n" +
                "            <hr style=\"height: 1px; color: #999; background-color: #999; border: none\"/>\n" +
                "            <img class=\"logo\" src=\"cid:jsd-content-id-11eb42ef-399d-4062-a6a8-0af9a5a8454c\" height=\"30\" alt=\"\" style=\"height: 30px; padding-top: 5px; padding-bottom: 5px\"/>\n" +
                "            <p class=\"footer-with-logo\" style=\"margin-top: 0; margin: 0 0 1.350em\">Help sent you this message, <i>powered\n" +
                "                by</i> <a class=\"gray-link\" href=\"http://www.atlassian.com/software/jira/service-desk\"\n" +
                "                          style=\"color: #999; text-decoration: underline\">JIRA Service Desk</a>\n" +
                "            </p>\n" +
                "        </td>\n" +
                "    </tr>\n" +
                "    </tbody>\n" +
                "</table>\n" +
                "</body>\n" +
                "</html>";
    }

}