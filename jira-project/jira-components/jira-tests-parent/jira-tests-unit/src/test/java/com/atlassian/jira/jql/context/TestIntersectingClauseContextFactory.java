package com.atlassian.jira.jql.context;

import com.atlassian.jira.util.collect.CollectionBuilder;
import com.atlassian.query.clause.TerminalClause;
import com.atlassian.query.clause.TerminalClauseImpl;
import com.atlassian.query.operator.Operator;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.Collections;

import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestIntersectingClauseContextFactory {
    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void testSingleSub() throws Exception {
        final ClauseContextFactory sub1 = mock(ClauseContextFactory.class);
        final TerminalClause aClause = new TerminalClauseImpl("blarg", Operator.NOT_EQUALS, "blarg");

        final ClauseContext context = mock(ClauseContext.class);
        when(sub1.getClauseContext(null, aClause)).thenReturn(context);

        final ContextSetUtil contextSetUtil = mock(ContextSetUtil.class);
        when(contextSetUtil.intersect(CollectionBuilder.newBuilder(context).asSet())).thenReturn(context);

        final IntersectingClauseContextFactory factory = new IntersectingClauseContextFactory(contextSetUtil, CollectionBuilder.newBuilder(sub1).asCollection());

        assertThat(factory.getClauseContext(null, aClause), sameInstance(context));
    }

    @Test
    public void testTwoSubs() throws Exception {
        final ClauseContextFactory sub1 = mock(ClauseContextFactory.class);
        final ClauseContextFactory sub2 = mock(ClauseContextFactory.class);
        final TerminalClause aClause = new TerminalClauseImpl("blarg", Operator.NOT_EQUALS, "blarg");

        final ClauseContext context1 = mock(ClauseContext.class);
        final ClauseContext context2 = mock(ClauseContext.class);
        when(sub1.getClauseContext(null, aClause)).thenReturn(context1);
        when(sub2.getClauseContext(null, aClause)).thenReturn(context2);

        final ContextSetUtil contextSetUtil = mock(ContextSetUtil.class);
        when(contextSetUtil.intersect(CollectionBuilder.newBuilder(context1, context2).asSet())).thenReturn(context1);

        final IntersectingClauseContextFactory factory = new IntersectingClauseContextFactory(contextSetUtil, CollectionBuilder.newBuilder(sub1, sub2).asCollection());


        factory.getClauseContext(null, aClause);
    }

    @Test
    public void testNoSubs() throws Exception {
        final TerminalClause aClause = new TerminalClauseImpl("blarg", Operator.NOT_EQUALS, "blarg");

        final ContextSetUtil contextSetUtil = mock(ContextSetUtil.class);

        final IntersectingClauseContextFactory factory = new IntersectingClauseContextFactory(contextSetUtil, Collections.<ClauseContextFactory>emptyList());

        factory.getClauseContext(null, aClause);

        verify(contextSetUtil).intersect(CollectionBuilder.<ClauseContext>newBuilder().asSet());
    }

    @Test
    public void shouldThrowIAEWhenNullsArePassedToConstructor() throws Exception {
        thrown.expect(IllegalArgumentException.class);
        new IntersectingClauseContextFactory(null, null);
    }

    @Test
    public void shouldThrowIAEWhenNullSubfactoriesPassedToConstructor() throws Exception {
        final ContextSetUtil contextSetUtil = mock(ContextSetUtil.class);

        thrown.expect(IllegalArgumentException.class);
        new IntersectingClauseContextFactory(contextSetUtil, CollectionBuilder.<ClauseContextFactory>newBuilder(null, null).asCollection());
    }
}
