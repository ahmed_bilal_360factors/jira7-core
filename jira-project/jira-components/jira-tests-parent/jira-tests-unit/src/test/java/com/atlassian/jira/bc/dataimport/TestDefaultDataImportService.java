package com.atlassian.jira.bc.dataimport;

import com.atlassian.activeobjects.spi.Backup;
import com.atlassian.core.util.ClassLoaderUtils;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.fugue.Option;
import com.atlassian.jira.JiraTestUtil;
import com.atlassian.jira.bc.ServiceResultImpl;
import com.atlassian.jira.bc.license.JiraLicenseService;
import com.atlassian.jira.bc.license.JiraLicenseUpdaterService;
import com.atlassian.jira.cluster.ClusterManager;
import com.atlassian.jira.cluster.Node;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.JiraProperties;
import com.atlassian.jira.config.properties.MockJiraProperties;
import com.atlassian.jira.config.util.AttachmentPathManager;
import com.atlassian.jira.config.util.IndexPathManager;
import com.atlassian.jira.config.util.JiraHome;
import com.atlassian.jira.event.JiraEventExecutorFactory;
import com.atlassian.jira.index.request.ReindexRequestManager;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.mock.MockApplicationProperties;
import com.atlassian.jira.mock.ofbiz.MockGenericValue;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.service.ServiceManager;
import com.atlassian.jira.task.TaskManager;
import com.atlassian.jira.task.TaskProgressSink;
import com.atlassian.jira.task.context.Context;
import com.atlassian.jira.upgrade.ConsistencyChecker;
import com.atlassian.jira.upgrade.UpgradeConstraints;
import com.atlassian.jira.upgrade.UpgradeResult;
import com.atlassian.jira.upgrade.UpgradeService;
import com.atlassian.jira.upgrade.UpgradeVersionHistoryManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.util.DirectorySynchroniserBarrier;
import com.atlassian.jira.util.BuildUtilsInfo;
import com.atlassian.jira.util.ComponentFactory;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.jira.util.collect.CollectionBuilder;
import com.atlassian.jira.util.index.IndexLifecycleManager;
import com.atlassian.jira.web.bean.MockI18nBean;
import com.atlassian.jira.web.util.ExternalLinkUtil;
import com.atlassian.mail.queue.MailQueue;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.sal.api.upgrade.PluginUpgradeManager;
import com.atlassian.scheduler.core.LifecycleAwareSchedulerService;
import com.google.common.collect.ImmutableList;
import org.hamcrest.Description;
import org.hamcrest.Matchers;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.junit.rules.TemporaryFolder;
import org.mockito.Answers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.ofbiz.core.entity.model.ModelEntity;
import org.ofbiz.core.entity.model.ModelField;
import org.ofbiz.core.entity.model.ModelReader;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Collection;
import java.util.Collections;
import java.util.Set;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;

import static com.atlassian.jira.bc.dataimport.DataImportEvent.ImportType.CLOUD;
import static com.atlassian.jira.bc.dataimport.DataImportEvent.ImportType.SERVER;
import static com.atlassian.jira.bc.dataimport.DataImportEvent.ImportType.UNKNOWN;
import static com.atlassian.jira.bc.dataimport.DataImportOSPropertyValidator.DataImportProperties;
import static com.atlassian.jira.bc.dataimport.DataImportService.ImportError.FAILED_VALIDATION;
import static com.atlassian.jira.bc.dataimport.DataImportService.ImportResult;
import static com.atlassian.jira.bc.dataimport.DataImportService.ImportValidationResult;
import static com.google.common.collect.Sets.newHashSet;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.argThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.notNull;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestDefaultDataImportService {
    @Rule
    public RuleChain mockAllTheThings = MockitoMocksInContainer.forTest(this);

    @Rule
    public TemporaryFolder tempFolder = new TemporaryFolder();

    private final MockGenericValue mockGv = new MockGenericValue("someentity");
    @Mock
    private MailQueue mockMailQueue;
    @Mock
    private IndexPathManager indexPathManager;
    @Mock
    private AttachmentPathManager attachmentPathManager;
    @Mock
    private ExternalLinkUtil mockExternalLinkUtil;
    @Mock
    private JiraLicenseUpdaterService jiraLicenseService;
    @Mock
    private BuildUtilsInfo buildUtilsInfo;
    private I18nHelper.BeanFactory beanFactory = new MockI18nBean.MockI18nBeanFactory();
    @Mock
    private JiraHome mockJiraHome;
    @Mock
    private OfbizImportHandler ofbizImportHandler;
    @Mock
    private GlobalPermissionManager permissionManager;
    @Mock
    private TaskManager mockTaskManager;
    @Mock
    private ServiceManager mockServiceManager;
    @Mock
    private PluginEventManager pluginEventManager;
    @Mock
    private ComponentFactory componentFactory;
    @Mock
    private DirectorySynchroniserBarrier directorySynchroniserBarrier;
    @Mock
    private JiraEventExecutorFactory executorFactory;
    @Mock
    private EventPublisher eventPublisher;
    @Mock
    private JiraLicenseService.ValidationResult validationResult;
    @Mock
    private ReindexRequestManager reindexRequestManager;
    @Mock
    private FeatureManager featureManager;
    @Mock
    private DataImportPropertiesValidationService dataImportPropertiesValidationService;
    private OfbizImportHandlerFactory ofbizImportHandlerFactory;
    @Mock
    @AvailableInContainer
    public LifecycleAwareSchedulerService schedulerService;

    @Mock(answer = Answers.RETURNS_MOCKS)
    @AvailableInContainer
    public OfBizDelegator ofBizDelegator;

    @Mock
    @AvailableInContainer
    public ConsistencyChecker consistencyChecker;

    @Mock
    @AvailableInContainer
    public UpgradeService upgradeService;

    @Mock
    @AvailableInContainer
    public UpgradeVersionHistoryManager upgradeVersionHistoryManager;

    @Mock
    @AvailableInContainer
    public IndexLifecycleManager indexManager;

    @Mock
    @AvailableInContainer
    public Backup backup;

    @Mock
    @AvailableInContainer
    public ModelReader modelReader;

    @Mock
    private UpgradeConstraints upgradeConstraints;

    @Mock
    private ClusterManager clusterManager;

    private ApplicationUser currentUser = new MockApplicationUser("admin");
    private MockApplicationProperties applicationProperties;
    private MockDataImportDependencies dependencies;
    private final JiraProperties jiraProperties = new MockJiraProperties();
    private PluginUpgradeManager pluginUpgradeManager = Collections::emptyList;

    @Before
    public void setUpTest() throws Exception {
        dependencies = new MockDataImportDependencies(consistencyChecker, pluginEventManager, pluginUpgradeManager, upgradeService);

        //Directory sync always works.
        when(componentFactory.createObject(DirectorySynchroniserBarrier.class)).thenReturn(directorySynchroniserBarrier);
        when(directorySynchroniserBarrier.await(20, TimeUnit.SECONDS)).thenReturn(true);

        //Setup directory state
        final File jiraAttachmentsDir = tempFolder.newFolder("jira-attachments");
        final File jiraIndexesDir = tempFolder.newFolder("jira-indexes");
        when(attachmentPathManager.getDefaultAttachmentPath()).thenReturn(jiraAttachmentsDir.getAbsolutePath());
        when(indexPathManager.getDefaultIndexRootPath()).thenReturn(jiraIndexesDir.getAbsolutePath());

        //Setup application properties.
        applicationProperties = new MockApplicationProperties();
        applicationProperties.setString(APKeys.JIRA_SETUP, "setup");

        //Give current user admin access.
        when(permissionManager.hasPermission(GlobalPermissionKey.SYSTEM_ADMIN, currentUser)).thenReturn(true);

        //Upgrade by default just works by default.
        when(upgradeService.runUpgrades()).thenReturn(UpgradeResult.OK);

        //By default the licenses are valid.
        when(validationResult.getErrorCollection()).thenReturn(new SimpleErrorCollection());
        when(jiraLicenseService.validate(Mockito.any(), Mockito.any(String.class)))
                .thenReturn(validationResult);
        when(jiraLicenseService.validate(Mockito.any(), Mockito.<Iterable<String>>any()))
                .thenReturn(ImmutableList.of(validationResult));

        //Task manager shutdown cleanly.
        when(mockTaskManager.shutdownAndWait(5)).thenReturn(true);

        //Reindex works by default.
        when(indexManager.size()).thenReturn(5);
        when(indexManager.activate(notNull(Context.class))).thenReturn(1L);

        //Allow some elements to be deleted.
        when(ofBizDelegator.getModelReader()).thenReturn(modelReader);
        when(modelReader.getEntityNames()).thenReturn(CollectionBuilder.list("Issue", "User"));
        when(modelReader.getModelEntity("Issue")).thenReturn(new ModelEntity());
        when(ofBizDelegator.removeByAnd("Issue", Collections.emptyMap())).thenReturn(10);
        when(modelReader.getModelEntity("User")).thenReturn(new ModelEntity());
        when(ofBizDelegator.removeByAnd("User", Collections.emptyMap())).thenReturn(5);

        //Allow database values to be saved. This is magic GV that represents all our saving.
        when(ofBizDelegator.makeValue(any(String.class))).thenReturn(mockGv);

        //some default builds numbers.
        when(buildUtilsInfo.getApplicationBuildNumber()).thenReturn(99000);
        when(buildUtilsInfo.getMinimumUpgradableBuildNumber()).thenReturn("0");

        when(upgradeConstraints.shouldRunTask(anyString())).thenReturn(true);
        when(upgradeConstraints.getTargetDatabaseBuildNumber())
                .thenAnswer(params -> buildUtilsInfo.getApplicationBuildNumber());

        when(dataImportPropertiesValidationService.validate(any(DataImportParams.class), any(DataImportProperties.class))).thenReturn(new ServiceResultImpl(new SimpleErrorCollection()));

        ofbizImportHandlerFactory = spy(new OfbizImportHandlerFactory(ofBizDelegator, indexPathManager, attachmentPathManager, featureManager));
    }

    @Test
    public void testExecuteGoodVersion() throws Exception {
        final String filePath = getDataFilePath("jira-export-test.xml");
        final DataImportParams params = new DataImportParams.Builder(filePath).build();
        executeTest(params, true, false, DataImportService.ImportError.NONE);
        verifyFinalSteps();
    }

    @Test
    public void fileThatDoesNotNeedEscapingSuceedsWhenEscapingIsUsed() throws Exception {
        final String filePath = getDataFilePath("jira-export-test.xml");
        final DataImportParams params = new DataImportParams.Builder(filePath).build();
        executeTest(params, true, true, DataImportService.ImportError.NONE);
        verifyFinalSteps();
    }

    @Test
    public void fileThatNeedsEscapingSuceedsWhenEscapingIsUsed() throws Exception {
        final String filePath = getDataFilePath("jira-export-test-needs-escaping.xml");
        final DataImportParams params = new DataImportParams.Builder(filePath).build();
        executeTest(params, true, true, DataImportService.ImportError.NONE);
        verifyFinalSteps();
    }

    @Test
    public void fileThatNeedsEscapingFailsWhenEscapingIsNotUsed() throws Exception {
        final String filePath = getDataFilePath("jira-export-test-needs-escaping.xml");
        final DataImportParams params = new DataImportParams.Builder(filePath).build();

        applicationProperties.setOption(APKeys.JIRA_IMPORT_CLEAN_XML, false);
        final DefaultDataImportService service = createImportService();

        final ImportValidationResult validationResult = service.validateImport(currentUser, params);
        final ImportResult importResult = service.doImport(currentUser, validationResult, TaskProgressSink.NULL_SINK);

        assertEquals(false, importResult.isValid());
        assertEquals(DataImportService.ImportError.NONE, importResult.getImportError());

        //create() should not have been called on our GVs
        assertFalse(mockGv.isCreated());
        assertFalse(dependencies.globalRefreshCalled);
        assertFalse(dependencies.addRecoveryMappingCalled);
    }

    @Test
    public void testExecuteQuickImport() throws Exception {
        //after the first parse check the build number.
        final String filePath = getDataFilePath("jira-export-test.xml");
        final DataImportParams params = new DataImportParams.Builder(filePath).setQuickImport(true).build();
        executeTest(params, true, false, DataImportService.ImportError.NONE);
        verifyFinalSteps();
    }

    @Test
    public void testExecuteImportWithUpdateTasksReindex() throws Exception {
        final String filePath = getDataFilePath("jira-export-test.xml");
        final DataImportParams params = new DataImportParams.Builder(filePath).setQuickImport(true).build();
        //Finally everything's mocked out.  Run the import!
        executeTest(params, true, false, DataImportService.ImportError.NONE);
        verifyFinalSteps();
    }

    @Test
    public void testNoPermission() throws Exception {
        when(permissionManager.hasPermission(eq(GlobalPermissionKey.SYSTEM_ADMIN), eq(currentUser))).thenReturn(false);

        try {
            final String filePath = getDataFilePath("jira-export-test.xml");
            final DataImportParams params = new DataImportParams.Builder(filePath).build();
            executeTest(params, false, false, DataImportService.ImportError.NONE);
            fail("Calling doImport with invalid validation result should have thrown an exception!");
        } catch (IllegalStateException e) {
            //yay
        }
    }

    @Test
    public void testNoFileProvided() throws Exception {
        try {
            final DataImportParams params = new DataImportParams.Builder("").build();
            executeTest(params, false, false, DataImportService.ImportError.NONE);
            fail("Calling doImport with invalid validation result should have thrown an exception!");
        } catch (IllegalStateException e) {
            //yay
        }
    }

    @Test
    public void testSetupImportWhenAlreadySetup() throws Exception {
        applicationProperties.setString(APKeys.JIRA_SETUP, "true");

        try {
            final DataImportParams params = new DataImportParams.Builder("").setupImport().build();
            executeTest(params, false, false, DataImportService.ImportError.NONE);
            fail("Calling doImport with invalid validation result should have thrown an exception!");
        } catch (IllegalStateException e) {
            //yay
        }
    }

    @Test
    public void testFileNonExistent() throws Exception {
        when(mockJiraHome.getImportDirectory()).thenReturn(new File("somewhere"));

        try {
            final DataImportParams params = new DataImportParams.Builder("idontexisthopefully.txt").build();
            executeTest(params, false, false, DataImportService.ImportError.NONE);
            fail("Calling doImport with invalid validation result should have thrown an exception!");
        } catch (IllegalStateException e) {
            //yay
        }
    }

    @Test(expected = IllegalStateException.class)
    public void testUnsafeFileNonExistent() throws Exception {
        when(mockJiraHome.getImportDirectory()).thenReturn(new File("somewhere"));

        final DataImportParams params = new DataImportParams.Builder(null)
                .setUnsafeJiraBackup(new File("idontexist.really.really.not")).build();
        executeTest(params, false, false, DataImportService.ImportError.NONE);
    }

    @Test(expected = IllegalStateException.class)
    public void testUnsafeAOFileNonExistent() throws Exception {
        final File file = tempFolder.newFile("testUnsafeAOFileNonExistent.txt");

        final DataImportParams params = new DataImportParams.Builder(null)
                .setUnsafeJiraBackup(file)
                .setUnsafeAOBackup(new File("I.really.really.don't.exist.and.if.i.did.it.would.be.very.unlucky"))
                .build();
        executeTest(params, false, false, DataImportService.ImportError.NONE);
    }

    @Test
    public void testGetJiraBackupFilesWithFileNameAndNoAOFile() throws IOException {
        String f = getDataFilePath("jira-export-test.xml");

        final DefaultDataImportService defaultDataImportService = createImportService();
        final DataImportParams params = new DataImportParams.Builder("jira-export-test.xml").build();
        final File backupFile = defaultDataImportService.getJiraBackupFile(params);
        final File aoBackupFile = defaultDataImportService.getAOBackupFile(params);

        final File expectedFile = new File(f).getCanonicalFile();
        assertEquals(expectedFile, backupFile.getCanonicalFile());
        assertEquals(expectedFile, aoBackupFile.getCanonicalFile());
    }

    @Test
    public void testNoAO() throws Exception {
        when(mockJiraHome.getImportDirectory()).thenReturn(new File("somewhere"));

        backup = null;

        try {
            final String filePath = getDataFilePath("jira-export-test.xml");
            final DataImportParams params = new DataImportParams.Builder(filePath).build();
            executeTest(params, false, false, DataImportService.ImportError.NONE);
            fail("Calling doImport with invalid validation result should have thrown an exception!");
        } catch (IllegalStateException e) {
            //yay
        }
    }

    @Test(expected = IllegalStateException.class)
    public void testInvalidLicenseProvided() throws Exception {
        final String badLicense = "thisisnotavalidlicensestring";
        when(jiraLicenseService.validate(any(), eq(badLicense))).thenReturn(validationResult);
        final ErrorCollection errors = new SimpleErrorCollection();
        errors.addErrorMessage("Not a valid license");
        when(validationResult.getErrorCollection()).thenReturn(errors);

        final String filePath = getDataFilePath("jira-export-test.xml");
        final DataImportParams params = new DataImportParams.Builder(filePath).setLicenseString(badLicense).build();
        executeTest(params, false, false, DataImportService.ImportError.NONE);
        fail("Calling doImport with invalid validation result should have thrown an exception!");
    }

    @Test
    public void testInvalidImportedLicense() throws Exception {
        final ErrorCollection errors = new SimpleErrorCollection();
        errors.addError("license", "ErrorInLicense");
        when(validationResult.getErrorCollection()).thenReturn(errors);
        when(jiraLicenseService.validate(Mockito.any(I18nHelper.class), anyIterable()))
                .thenReturn(ImmutableList.of(validationResult));

        final String filePath = getDataFilePath("jira-export-test.xml");
        final DataImportParams params = new DataImportParams.Builder(filePath).build();
        ImportResult importResult = executeTest(params, false, false, DataImportService.ImportError.NONE);
        assertEquals(errors, importResult.getErrorCollection());
    }

    @Test
    public void testBackupLicenseNotCheckedWhenInStartupMode() throws Exception {
        //given
        final ErrorCollection errors = new SimpleErrorCollection();
        errors.addError("license", "ErrorInLicense");
        when(validationResult.getErrorCollection()).thenReturn(errors);
        when(jiraLicenseService.validate(Mockito.any(), anyIterable()))
                .thenReturn(ImmutableList.of(validationResult));
        final String filePath = getDataFilePath("jira-export-test.xml");

        //when
        final DataImportParams params = new DataImportParams.Builder(filePath).setStartupDataOnly().build();

        //then
        executeTest(params, true, false, DataImportService.ImportError.NONE);
        verifyFinalSteps();
        verify(jiraLicenseService, never()).validate(any(), anyIterable());
    }

    @Test
    public void testPassedLicenseCheckedWhenInStartupMode() throws Exception {
        //given
        final String licenseString = "license";
        final String filePath = getDataFilePath("jira-export-test.xml");

        final DataImportParams params = new DataImportParams.Builder(filePath)
                .setStartupDataOnly()
                .setLicenseString(licenseString)
                .build();

        //when
        executeTest(params, true, false, DataImportService.ImportError.NONE);

        //then
        verify(jiraLicenseService, atLeastOnce()).validate(any(), eq(licenseString));
        verifyFinalSteps();
    }

    @Test
    public void jiraShouldNotAllowDowngradeFromNonDowngradableVersion() throws Exception {
        final ModelEntity modelEntity = new ModelEntity();
        modelEntity.addField(makeModelField("upgradeclass"));
        modelEntity.addField(makeModelField("targetbuild"));
        modelEntity.addField(makeModelField("status"));
        //This is called during the first parse of the XML file.  At this stage nothing should have been created yet!
        final MockGenericValue mockGv = new MockGenericValue("UpgradeHistory", modelEntity, null);
        when(ofBizDelegator.makeValue(any(String.class))).thenReturn(mockGv);

        //after the first parse check the build number.
        when(buildUtilsInfo.getApplicationBuildNumber()).thenReturn(800);
        when(buildUtilsInfo.getMinimumUpgradableBuildNumber()).thenReturn("1");

        final String filePath = getDataFilePath("jira-export-test-too-new.xml");
        final DataImportParams params = new DataImportParams.Builder(filePath).setNoLicenseCheck().build();
        executeTest(params, false, false, DataImportService.ImportError.NONE);
    }

    @Test
    public void jiraShouldNotAllowImportIfVersionIsConstrainedAndDowngradeIsNotPossible() throws Exception {
        final ModelEntity modelEntity = new ModelEntity();
        modelEntity.addField(makeModelField("upgradeclass"));
        modelEntity.addField(makeModelField("targetbuild"));
        modelEntity.addField(makeModelField("status"));
        //This is called during the first parse of the XML file.  At this stage nothing should have been created yet!
        final MockGenericValue mockGv = new MockGenericValue("UpgradeHistory", modelEntity, null);
        when(ofBizDelegator.makeValue(any(String.class))).thenReturn(mockGv);

        //after the first parse check the build number.
        when(buildUtilsInfo.getApplicationBuildNumber()).thenReturn(99001);
        when(upgradeConstraints.getTargetDatabaseBuildNumber()).thenReturn(99000);
        when(buildUtilsInfo.getMinimumUpgradableBuildNumber()).thenReturn("1");

        final String filePath = getDataFilePath("jira-export-test-too-new.xml");
        final DataImportParams params = new DataImportParams.Builder(filePath).setNoLicenseCheck().build();
        executeTest(params, false, false, DataImportService.ImportError.NONE);
    }

    @Test
    public void jiraShouldAllowDowngradeFromDowngradableBuildNumber_promptForConfirmation() throws Exception {
        final ModelEntity modelEntity = new ModelEntity();
        modelEntity.addField(makeModelField("upgradeclass"));
        modelEntity.addField(makeModelField("targetbuild"));
        modelEntity.addField(makeModelField("status"));
        //This is called during the first parse of the XML file.  At this stage nothing should have been created yet!
        final MockGenericValue mockGv = new MockGenericValue("UpgradeHistory", modelEntity, null);
        when(ofBizDelegator.makeValue(any(String.class))).thenReturn(mockGv);

        //after the first parse check the build number.
        when(buildUtilsInfo.getApplicationBuildNumber()).thenReturn(800);
        when(buildUtilsInfo.getMinimumUpgradableBuildNumber()).thenReturn("1");

        final String filePath = getDataFilePath("jira-export-test-too-new-noop-downgrade.xml");
        final DataImportParams params = new DataImportParams.Builder(filePath).setNoLicenseCheck().build();

        // We have passed validation - but we need the admin to confirm that they are happy to downgrade:
        executeTest(params, false, false, DataImportService.ImportError.DOWNGRADE_FROM_ONDEMAND);
    }

    @Test
    public void testExecuteBuildNumberTooOldInXml() throws Exception {
        //This is called during the first parse of the XML file.  At this stage nothing should have been created yet!
        final MockGenericValue mockGv = new MockGenericValue("someentity");
        when(ofBizDelegator.makeValue(any(String.class))).thenReturn(mockGv);

        //after the first parse check the build number.
        when(buildUtilsInfo.getCurrentBuildNumber()).thenReturn("400");
        when(buildUtilsInfo.getMinimumUpgradableBuildNumber()).thenReturn("18");

        when(mockExternalLinkUtil.getProperty("external.link.jira.confluence.upgrade.guide.for.old.versions"))
                .thenReturn("http://www.atlassian.com");

        final String filePath = getDataFilePath("jira-export-test-too-old.xml");
        final DataImportParams params = new DataImportParams.Builder(filePath).build();
        executeTest(params, false, false, DataImportService.ImportError.NONE);
    }

    @Test
    public void testExecuteBuildNumberMissing() throws Exception {
        //after the first parse check the build number.
        when(buildUtilsInfo.getCurrentBuildNumber()).thenReturn("1");
        when(buildUtilsInfo.getMinimumUpgradableBuildNumber()).thenReturn("0");

        final String filePath = getDataFilePath("jira-export-test-no-build-number.xml");
        final DataImportParams params = new DataImportParams.Builder(filePath).build();

        //Finally everything's mocked out.  Run the import!
        executeTest(params, true, false, DataImportService.ImportError.NONE);
        verifyFinalSteps();
    }

    @Test
    public void testImportBackupWithDate() throws Exception {
        applicationProperties.setOption(APKeys.JIRA_IMPORT_CLEAN_XML, false);

        final String filePath = getDataFilePath("jira-export-test-with-time.xml");
        final DataImportParams params = new DataImportParams.Builder(filePath).build();
        final DefaultDataImportService importService = createImportService();
        final ImportValidationResult validationResult = importService.validateImport(currentUser, params);

        importService.doImport(currentUser, validationResult, TaskProgressSink.NULL_SINK);

        verify(eventPublisher).publish(argThat(new ImportEventMatcher(1l, ImportStartedEvent.class)));
        verify(eventPublisher).publish(argThat(new ImportEventMatcher(1l, ImportCompletedEvent.class)));
        verifyFinalSteps();
    }

    @Test
    public void jiraShouldAllowDowngradeinSingleActiveNodeCluster() throws Exception {
        when(clusterManager.isClustered()).thenReturn(true);
        when(clusterManager.getNodeId()).thenReturn("node1");
        when(clusterManager.findLiveNodes()).thenReturn(ImmutableList.of(new Node("node1", Node.NodeState.ACTIVE)));

        final String filePath = getDataFilePath("jira-export-test.xml");
        final DataImportParams params = new DataImportParams.Builder(filePath).build();

        executeTest(params, true, false, DataImportService.ImportError.NONE);
        verifyFinalSteps();
    }

    @Test
    public void jiraShouldDisallowDowngradeinActiveCluster() throws Exception {
        //called during validation!
        when(clusterManager.isClustered()).thenReturn(true);
        when(clusterManager.getNodeId()).thenReturn("node1");
        when(clusterManager.findLiveNodes()).thenReturn(ImmutableList.of(
                new Node("node1", Node.NodeState.ACTIVE),
                new Node("node2", Node.NodeState.ACTIVE)));

        final String filePath = getDataFilePath("jira-export-test.xml");
        final DataImportParams params = new DataImportParams.Builder(filePath).build();

        //Finally everything's mocked out.  Run the import!
        applicationProperties.setOption(APKeys.JIRA_IMPORT_CLEAN_XML, false);
        final DefaultDataImportService service = createImportService();
        final ImportValidationResult validationResult = service.validateImport(currentUser, params);
        assertThat(validationResult.isValid(), is(false));
        assertThat(validationResult.getErrorCollection().getErrorMessages(),
                Matchers.contains("This JIRA cluster has other active nodes. Please shutdown other active nodes",
                        "You are currently attached to node 'node1'. Nodes 'node2' are also active.",
                        "It may take up to five minutes for shutdown nodes to be removed from the cluster."));
    }

    @Test
    public void shouldAlwaysSpecifyPropertyKeysToRecordWhenCreatingOfbizImportHandler() throws Exception {
        Set<String> propertyKeysToRecord = newHashSet("property.key.1", "property.key.2");
        when(dataImportPropertiesValidationService.getPropertyKeysToRecord()).thenReturn(propertyKeysToRecord);

        final String filePath = getDataFilePath("jira-export-test.xml");
        final DataImportParams params = new DataImportParams.Builder(filePath).build();
        executeTest(params, true, false, DataImportService.ImportError.NONE);
        verifyFinalSteps();

        verify(ofbizImportHandlerFactory).create(anyBoolean(), eq(propertyKeysToRecord), any(Executor.class));
    }

    @Test
    public void shouldPreventImportWhenPropertiesValidationFailed() throws Exception {
        when(dataImportPropertiesValidationService.validate(any(DataImportParams.class), any(DataImportProperties.class)))
                .thenReturn(new ServiceResultImpl(new SimpleErrorCollection("Validation error", ErrorCollection.Reason.PRECONDITION_FAILED)));
        DefaultDataImportService service = spy(createImportService());
        String filePath = getDataFilePath("jira-export-test.xml");
        DataImportParams params = new DataImportParams.Builder(filePath).build();
        ImportValidationResult importValidationResult = new ImportValidationResult(new SimpleErrorCollection(), params);

        ImportResult importResult = service.doImport(currentUser, importValidationResult, TaskProgressSink.NULL_SINK);

        assertThat(importResult.isValid(), is(false));
        assertThat(importResult.getImportError(), is(FAILED_VALIDATION));

        Collection<String> validationErrorMessages = importResult.getValidationErrorMessages();
        assertThat(validationErrorMessages.size(), is(1));
        assertThat(validationErrorMessages, hasItem("Validation error"));
    }

    @Test
    public void testImportTypeBuildNumberBumpedCloud() {
        final String buildNumber = "1000012";
        final DefaultDataImportService dataImportService = spy(createImportService());
        doReturn(buildNumber).when(ofbizImportHandler).getBuildNumber();

        final DataImportEvent.ImportType importType = dataImportService.importVersion(ofbizImportHandler);

        assertEquals(CLOUD, importType);
    }

    @Test
    public void testImportTypeBuildNumberBumpedServer() {
        final String buildNumber = "73000";
        final DefaultDataImportService dataImportService = spy(createImportService());
        doReturn(buildNumber).when(ofbizImportHandler).getBuildNumber();

        final DataImportEvent.ImportType importType = dataImportService.importVersion(ofbizImportHandler);

        assertEquals(SERVER, importType);
    }

    @Test
    public void testImportTypeBeforeBuildNumberBumpedUndefined() {
        final String buildNumber = "70107";
        final DefaultDataImportService dataImportService = spy(createImportService());
        doReturn(buildNumber).when(ofbizImportHandler).getBuildNumber();

        final DataImportEvent.ImportType importType = dataImportService.importVersion(ofbizImportHandler);

        assertEquals(UNKNOWN, importType);
    }

    @Test
    public void testImportTypeNullBuildNumber() {
        final DefaultDataImportService dataImportService = spy(createImportService());
        doReturn(null).when(ofbizImportHandler).getBuildNumber();

        final DataImportEvent.ImportType importType = dataImportService.importVersion(ofbizImportHandler);

        assertEquals(UNKNOWN, importType);
    }

    private ImportResult executeTest(final DataImportParams params,
                                                       final boolean success,
                                                       final boolean escapeIllegalCharacters,
                                                       DataImportService.ImportError specificError) throws Exception {
        applicationProperties.setOption(APKeys.JIRA_IMPORT_CLEAN_XML, escapeIllegalCharacters);

        final DefaultDataImportService service = createImportService();

        final ImportValidationResult validationResult = service.validateImport(currentUser, params);
        final ImportResult importResult = service.doImport(currentUser, validationResult, TaskProgressSink.NULL_SINK);

        assertEquals(success, importResult.isValid());
        assertEquals(specificError, importResult.getImportError());
        return importResult;
    }

    private DefaultDataImportService createImportService() {
        return new DefaultDataImportService(dependencies, permissionManager,
                mockJiraHome, jiraLicenseService, beanFactory, ofBizDelegator,
                mockExternalLinkUtil, applicationProperties, buildUtilsInfo,
                mockTaskManager, mockServiceManager, mockMailQueue, componentFactory, null, executorFactory,
                jiraProperties, reindexRequestManager, clusterManager, upgradeConstraints,
                dataImportPropertiesValidationService, ofbizImportHandlerFactory, upgradeVersionHistoryManager) {
            @Override
            Backup getAOBackup() {
                return backup;
            }

            @Override
            protected EventPublisher getEventPublisher() {
                return eventPublisher;
            }
        };
    }

    private String getDataFilePath(String dataFileName) {
        // let's do some funky URL stuff to find the real path of this file
        final URL url = ClassLoaderUtils.getResource(JiraTestUtil.TESTS_BASE + "/action/admin/" + dataFileName, TestDefaultDataImportService.class);
        final File f = new File(url.getPath());

        when(mockJiraHome.getImportDirectory()).thenReturn(new File(f.getParent()));
        return f.getAbsolutePath();
    }

    private ModelField makeModelField(final String fieldName) {
        final ModelField modelField = new ModelField();
        modelField.setName(fieldName);
        return modelField;
    }

    static class MockDataImportDependencies extends DataImportProductionDependencies {
        private boolean globalRefreshCalled = false;
        private boolean addRecoveryMappingCalled = false;
        private final ConsistencyChecker consistencyChecker;
        private final PluginEventManager pluginEventManager;
        private final PluginUpgradeManager pluginUpgradeManager;
        private final UpgradeService upgradeService;

        MockDataImportDependencies(
                ConsistencyChecker consistencyChecker, PluginEventManager pluginEventManager,
                PluginUpgradeManager pluginUpgradeManager, UpgradeService upgradeService
        ) {
            this.consistencyChecker = consistencyChecker;
            this.pluginEventManager = pluginEventManager;
            this.pluginUpgradeManager = pluginUpgradeManager;
            this.upgradeService = upgradeService;
        }

        @Override
        void globalRefresh(boolean quickImport) {
            globalRefreshCalled = true;
        }

        @Override
        ConsistencyChecker getConsistencyChecker() {
            return consistencyChecker;
        }

        @Override
        PluginEventManager getPluginEventManager() {
            return pluginEventManager;
        }

        @Override
        PluginUpgradeManager getPluginUpgradeManager() {
            return pluginUpgradeManager;
        }

        @Override
        void addRecoveryMapping() {
            addRecoveryMappingCalled = true;
        }

        @Override
        UpgradeService getUpgradeService(boolean setupMode) {
            return upgradeService;
        }
    }

    private static class ImportEventMatcher extends TypeSafeMatcher<DataImportEvent> {
        private final Long expectedXmlExportTime;

        ImportEventMatcher(final Long expectedXmlExportTime, Class<? extends DataImportEvent> clazz) {
            super(clazz);
            this.expectedXmlExportTime = expectedXmlExportTime;
        }

        @Override
        protected boolean matchesSafely(final DataImportEvent o) {
            final Option<Long> exportXmlTime = o.getXmlExportTime();
            return exportXmlTime.isDefined() && exportXmlTime.get().equals(expectedXmlExportTime);
        }

        @Override
        public void describeTo(final Description description) {
            description.appendText("Exported xml file doesn't contain date attribute");
        }
    }

    private void verifyFinalSteps() {
        //create() should have been called on our GVs
        assertTrue(mockGv.isCreated());
        //the world should have been rebuilt!
        assertTrue(dependencies.globalRefreshCalled);
        //recovery admin should have been called if necessary.
        assertTrue(dependencies.addRecoveryMappingCalled);
    }

    private static <T> Iterable<T> anyIterable() {
        return any();
    }
}
