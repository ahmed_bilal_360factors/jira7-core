package com.atlassian.jira.issue.statistics;

import java.util.Map;
import java.util.Optional;
import java.util.function.Function;

import com.atlassian.jira.bc.project.component.ProjectComponent;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.web.bean.StatisticMapWrapper;
import com.atlassian.query.Query;

import com.google.common.collect.Maps;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsMapContaining.hasEntry;
import static org.hamcrest.collection.IsMapContaining.hasKey;
import static org.hamcrest.core.Is.is;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestStatisticsManagerImpl {
    @Mock
    private JiraAuthenticationContext jiraAuthenticationContext;
    @Mock
    private ProjectManager projectManager;
    @Mock
    private FilterStatisticsValuesGenerator filterStatisticsGenerator;

    StatisticsManagerImpl statisticsManagerImpl;
    private ProjectComponent comp0;
    private ProjectComponent comp1;
    private ProjectComponent comp2;
    private ProjectComponent comp3;

    private StatisticMapWrapper statisticMapWrapper;
    private Project proj0;
    private Project proj1;
    private ProjectComponent compWithBrokenProject;

    @Before
    public void setUp() throws Exception {
        statisticMapWrapper = mock(StatisticMapWrapper.class);
        final Map<ProjectComponent, Integer> stats = Maps.newHashMap();

        proj0 = mock(Project.class);
        proj1 = mock(Project.class);
        when(projectManager.getProjectObj(eq(0L))).thenReturn(proj0);
        when(projectManager.getProjectObj(eq(1L))).thenReturn(proj1);
        when(projectManager.getProjectObj(eq(2L))).thenReturn(null);
        
        comp0 = mock(ProjectComponent.class);
        comp1 = mock(ProjectComponent.class);
        when(comp0.getProjectId()).thenReturn(0L);
        when(comp1.getProjectId()).thenReturn(0L);
        
        comp2 = mock(ProjectComponent.class);
        comp3 = mock(ProjectComponent.class);
        when(comp2.getProjectId()).thenReturn(1L);
        when(comp3.getProjectId()).thenReturn(1L);
        
        compWithBrokenProject = mock(ProjectComponent.class);
        when(compWithBrokenProject.getProjectId()).thenReturn(2L);
        
        stats.put(comp0, 10);
        stats.put(comp1, 11);
        stats.put(comp2, 12);
        stats.put(comp3, null);
        
        // We expect null projects and null components to be filtered out.
        stats.put(null, 23);
        stats.put(compWithBrokenProject, 0);

        when(statisticMapWrapper.getStatistics()).thenReturn(stats);
        when(statisticMapWrapper.keySet()).thenReturn(stats.keySet());
    }

    @Test
    public void testGetProjectsTransformsCorrectly() throws Exception {
        statisticsManagerImpl = mockStatisticManagerImpl(statisticMapWrapper);
        
        final Optional query = Optional.of(mock(Query.class));
        final Function<ProjectComponent, Long> objToProjectIdMapper = comp -> comp.getProjectId();
        final String objectType = "component";

        Map<Project, Map<ProjectComponent, Integer>> result = statisticsManagerImpl.getProjectsWithItemsWithIssueCount(query, objToProjectIdMapper, objectType);
        
        assertThat(result, hasKey(proj0));
        assertThat(result, hasKey(proj1));
        
        assertThat(result.get(proj0), hasEntry(comp0, 10));
        assertThat(result.get(proj0), hasEntry(comp1, 11));
        assertThat(result.get(proj0).size(), is(2));
        
        assertThat(result.get(proj1), hasEntry(comp2, 12));
        assertThat(result.get(proj1), hasEntry(comp3, null));
        assertThat(result.get(proj1).size(), is(2));
    }
    
    
    private StatisticsManagerImpl mockStatisticManagerImpl(StatisticMapWrapper<Object, Integer> returnValue) {
        return new StatisticsManagerImpl(jiraAuthenticationContext, projectManager, filterStatisticsGenerator) {
            @Override
            public StatisticMapWrapper<Object, Integer> getObjectsResultingFrom(final Optional<Query> query, final String statsObject) {
                return returnValue;
            }
        };
    }
}