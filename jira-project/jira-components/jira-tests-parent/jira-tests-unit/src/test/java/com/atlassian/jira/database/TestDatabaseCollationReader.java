package com.atlassian.jira.database;

import com.atlassian.jira.config.database.DatabaseConfig;
import com.atlassian.jira.config.database.Datasource;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.RETURNS_DEEP_STUBS;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestDatabaseCollationReader {

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private Connection connection;

    private enum DatasourceType {
        JNDI, JDBC
    }

    @Test
    public void testFindCollation() throws Exception {
        String databaseType = "postgres72";
        String expectedCollation = "C";
        DatasourceType datasourceType = DatasourceType.JDBC;
        setUpConnection(expectedCollation, databaseType, false);

        String collation = DatabaseCollationReader.findCollation(connection, getDatabaseConfiguration(databaseType, datasourceType));

        assertEquals(expectedCollation, collation);
    }

    @Test
    public void testFindCollationForHsqlReturnsNull() throws Exception {
        String databaseType = "hsql";
        String expectedCollation = null;
        DatasourceType datasourceType = DatasourceType.JDBC;
        setUpConnection(expectedCollation, databaseType, false);

        String collation = DatabaseCollationReader.findCollation(connection, getDatabaseConfiguration(databaseType, datasourceType));

        assertEquals(expectedCollation, collation);
    }

    @Test
    public void testFindCollationForUnknownDatabaseReturnsNull() throws Exception {
        String databaseType = "unknown";
        String expectedCollation = null;
        DatasourceType datasourceType = DatasourceType.JDBC;
        setUpConnection(expectedCollation, databaseType, false);

        String collation = DatabaseCollationReader.findCollation(connection, getDatabaseConfiguration(databaseType, datasourceType));

        assertEquals(expectedCollation, collation);
    }

    @Test(expected = SQLException.class)
    public void testFindCollationThrowsSqlExceptionWhenQueryFails() throws Exception {
        String databaseType = "postgres72";
        String expectedCollation = "C";
        DatasourceType datasourceType = DatasourceType.JDBC;
        setUpConnection(expectedCollation, databaseType, true);

        DatabaseCollationReader.findCollation(connection, getDatabaseConfiguration(databaseType, datasourceType));
    }

    @Test
    public void testFindCollationWorksWithJndiDatasource() throws Exception {
        String databaseType = "postgres72";
        String expectedCollation = "C";
        DatasourceType datasourceType = DatasourceType.JNDI;
        setUpConnection(expectedCollation, databaseType, false);

        String collation = DatabaseCollationReader.findCollation(connection, getDatabaseConfiguration(databaseType, datasourceType));

        assertEquals(expectedCollation, collation);
    }

    @Test
    public void testNoSqlStringSubstitution() {
        String[] queries = DatabaseCollationReader.getCollationQueries();

        for (String query : queries) {
            // Simple SQL injection check. Checks that no %s substitution shenanigans are going on
            assertThat(query, not(containsString("%")));
        }
    }

    private void setUpConnection(String expectedCollation, String databaseType, boolean collationQueryFails) {
        try {
            ResultSet mockResult = mock(ResultSet.class);
            when(mockResult.getString(1)).thenReturn(expectedCollation);

            when(connection.getMetaData().getURL()).thenReturn(getJdbcUri(databaseType));
            if (collationQueryFails) {
                when(connection.prepareStatement(anyString()).executeQuery()).thenThrow(new SQLException());
            } else {
                when(connection.prepareStatement(anyString()).executeQuery()).thenReturn(mockResult);
            }

        } catch (SQLException e) {
            // Swallow mock exceptions
        }
    }

    private DatabaseConfig getDatabaseConfiguration(String databaseType, DatasourceType datasourceType) {
        Datasource datasource = mock(Datasource.class, RETURNS_DEEP_STUBS);
        if (datasourceType == DatasourceType.JDBC) {
            when(datasource.getDatasource(anyString(), anyString(), anyString()).getJdbcDatasource().getDriverClassName()).thenReturn(getDriverClassName(databaseType));
            when(datasource.getDatasource(anyString(), anyString(), anyString()).getJdbcDatasource().getUri()).thenReturn(getJdbcUri(databaseType));
        } else if (datasourceType == DatasourceType.JNDI) {
            when(datasource.getDatasource(anyString(), anyString(), anyString()).getJdbcDatasource()).thenReturn(null);
            when(datasource.getDatasource(anyString(), anyString(), anyString()).getJdbcDatasource()).thenReturn(null);
        }

        return new DatabaseConfig(databaseType, "schema", datasource);
    }

    private String getJdbcUri(String databaseType) {
        if (databaseType.contains("postgres")) {
            return "jdbc:postgresql://localhost:5432/jiradb";
        } else if (databaseType.contains("hsql")) {
            return "jdbc:hsqldb:somewhere/database/jiradb";
        } else {
            return "unknown";
        }
    }

    private String getDriverClassName(String databaseType) {
        if (databaseType.contains("postgres")) {
            return "org.postgresql.Driver";
        } else if (databaseType.contains("hsql")) {
            return "org.hsqldb.jdbc.JDBCDriver";
        } else {
            return "unknown";
        }
    }
}