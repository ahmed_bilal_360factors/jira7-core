package com.atlassian.jira.bc.license;

import com.atlassian.application.api.Application;
import com.atlassian.application.api.ApplicationKey;
import com.atlassian.application.api.ApplicationManager;
import com.atlassian.extras.api.LicenseException;
import com.atlassian.extras.api.LicenseType;
import com.atlassian.jira.bc.license.JiraLicenseService.ValidationResult;
import com.atlassian.jira.cluster.ClusterManager;
import com.atlassian.jira.help.MockHelpUrl;
import com.atlassian.jira.help.MockHelpUrls;
import com.atlassian.jira.license.JiraLicenseManager;
import com.atlassian.jira.license.LicenseDetails;
import com.atlassian.jira.license.LicensedApplications;
import com.atlassian.jira.license.MockLicenseDetailsBuilder;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.NoopI18nHelper;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSortedSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.annotation.Nonnull;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.SortedSet;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Tests {@link JiraLicenseServiceImpl}.
 */
@RunWith(MockitoJUnitRunner.class)
public class TestJiraLicenseServiceImpl {
    private static final String BAD_LIC_STRING = "somebadstring";
    private static final String GOOD_LIC_STRING = "adam goodes";
    private static final String GOOD_NEW_LIC_STRING = "new license";

    @Mock
    private JiraLicenseManager licenseManager;
    @Mock
    private LicenseDetails licenseDetails;
    @Mock
    private ClusterManager clusterManager;
    @Mock
    private ApplicationManager applicationManager;
    @Mock
    private UserManager userManager;

    private MockHelpUrls helpUrls = new MockHelpUrls();
    private MockHelpUrl mockHelpUrl = new MockHelpUrl();

    private I18nHelper i18nHelper = new NoopI18nHelper();

    private JiraLicenseServiceImpl licenseService;
    private MockApplicationRoleTestHelper mockLicHelper;

    @Before
    public void setup() {
        mockLicHelper = new MockApplicationRoleTestHelper();
        licenseService = new JiraLicenseServiceImpl(licenseManager, clusterManager, applicationManager, userManager, helpUrls);
        when(applicationManager.getApplications()).thenReturn(ImmutableList.<Application>of());

        mockHelpUrl.setUrl("test-url");
        mockHelpUrl.setKey("license.compatibility");
        helpUrls.addUrl(mockHelpUrl);
    }

    @Test(expected = IllegalStateException.class)
    public void testSetLicense_NullValidationResult() {
        licenseService.setLicense(null);
    }

    @Test
    public void testSetLicense_ValidationResultHasNoErrorCollection() {
        ValidationResult validationResult = mock(ValidationResult.class);
        when(validationResult.getErrorCollection()).thenReturn(null);
        try {
            licenseService.setLicense(validationResult);
            fail("Should have barfed");
        } catch (IllegalStateException ignored) {
        }
        verify(validationResult).getErrorCollection();
    }

    @Test
    public void testSetLicense_ValidationResultHasErrorCollectionWithErrors() {
        SimpleErrorCollection errorCollection = new SimpleErrorCollection();
        errorCollection.addError("shite", "happens");

        ValidationResult validationResult = mock(ValidationResult.class);
        when(validationResult.getErrorCollection()).thenReturn(errorCollection);
        try {
            licenseService.setLicense(validationResult);
            fail("Should have barfed");
        } catch (IllegalStateException ignored) {
        }

        verify(validationResult, times(2)).getErrorCollection();
    }

    @Test
    public void testSetLicense_HappyPath() {
        when(licenseManager.setLicense(GOOD_LIC_STRING)).thenReturn(licenseDetails);

        ValidationResult validationResult = mock(ValidationResult.class);
        when(validationResult.getErrorCollection()).thenReturn(new SimpleErrorCollection());
        when(validationResult.getLicenseString()).thenReturn(GOOD_LIC_STRING);

        LicenseDetails actualLicenseDetails = licenseService.setLicense(validationResult);
        assertSame(licenseDetails, actualLicenseDetails);
        verify(licenseManager).setLicense(GOOD_LIC_STRING);
        verify(validationResult).getLicenseString();
    }

    @Test
    public void validateLicensesMethodShouldAlwaysCheckAllLicenses() {
        LicensedApplications licensedApplications = mock(LicensedApplications.class);
        when(licensedApplications.getUserLimit(anyObject())).thenReturn(3);
        when(licensedApplications.getKeys()).thenReturn(Sets.newHashSet(ApplicationKey.valueOf("jira-software")));
        List<String> licenses = ImmutableList.of("valid", "invalid", "valid again");
        when(licenseManager.getLicenses()).thenReturn(Collections.emptyList());
        when(licenseManager.getLicense("invalid")).thenThrow(LicenseException.class);

        LicenseDetails valid = mock(LicenseDetails.class);
        when(valid.getLicenseString()).thenReturn("valid");
        when(valid.getLicenseVersion()).thenReturn(2);
        when(valid.getLicensedApplications()).thenReturn(licensedApplications);
        when(licenseManager.getLicense(("valid"))).thenReturn(valid);

        LicenseDetails validAgain = mock(LicenseDetails.class);
        when(validAgain.getLicenseString()).thenReturn("valid again");
        when(validAgain.getLicenseVersion()).thenReturn(2);
        when(validAgain.getLicensedApplications()).thenReturn(licensedApplications);
        when(licenseManager.getLicense(("valid again"))).thenReturn(validAgain);

        Iterable<ValidationResult> actual = licenseService.validate(i18nHelper, licenses);

        assertEquals(licenses.size(), Iterables.size(actual));

        Iterator<ValidationResult> iterator = actual.iterator();
        assertFalse("valid license", iterator.next().getErrorCollection().hasAnyErrors());
        assertTrue("invalid license", iterator.next().getErrorCollection().hasAnyErrors());
        assertFalse("second valid license", iterator.next().getErrorCollection().hasAnyErrors());
    }

    @Test
    public void emptyValidateLicensesShouldFail() {
        Iterable<ValidationResult> actual = licenseService.validate(i18nHelper, ImmutableList.<String>of());
        assertEquals("results should contain one element", 1, Iterables.size(actual));
        assertTrue("results should contain errors", actual.iterator().next().getErrorCollection().hasAnyErrors());

    }

    @Test
    public void ourValidationResultShouldReturnCorrectTotalUserCount() {
        // Given
        int totalUserCount = 7;
        when(userManager.getTotalUserCount()).thenReturn(totalUserCount);
        when(licenseManager.getLicense(GOOD_LIC_STRING)).thenReturn(licenseDetails);

        // When
        final ValidationResult validationResult1 = licenseService.validate(i18nHelper, GOOD_LIC_STRING);
        final ValidationResult validationResult2 = licenseService.validate(null, GOOD_LIC_STRING, i18nHelper);
        final ValidationResult validationResult3 = licenseService.validateApplicationLicense(i18nHelper, GOOD_LIC_STRING);
        Iterable<ValidationResult> validationResults = licenseService.validate(i18nHelper, ImmutableList.<String>of());

        // Then
        assertThat("ValidationResult should get value from UserManager", validationResult1.getTotalUserCount(), is(totalUserCount));
        assertThat("ValidationResult should get value from UserManager", validationResult2.getTotalUserCount(), is(totalUserCount));
        assertThat("ValidationResult should get value from UserManager", validationResult3.getTotalUserCount(), is(totalUserCount));
        for (ValidationResult result : validationResults) {
            assertThat("ValidationResult should get value from UserManager", result.getTotalUserCount(), is(totalUserCount));
        }
    }

    @Test
    public void testValidate_InvalidString() {
        when(licenseManager.isDecodeable(BAD_LIC_STRING)).thenReturn(false);

        final ValidationResult validationResult = licenseService.validateApplicationLicense(i18nHelper, BAD_LIC_STRING);
        assertValidationResult(validationResult, "jira.license.validation.invalid.product.key{[]}");
    }

    @Test
    public void testValidate_NoApplicationsAndNoSoftware() {
        LicensedApplications licensedApplications = mock(LicensedApplications.class);
        when(licenseDetails.getLicensedApplications()).thenReturn(licensedApplications);
        when(licensedApplications.getKeys()).thenReturn(Collections.emptySet());

        final ValidationResult validationResult = licenseService.validateApplicationLicense(i18nHelper, BAD_LIC_STRING);
        assertValidationResult(validationResult, "jira.license.validation.invalid.product.key{[]}");
    }

    @Test
    public void testGetServerId() throws Exception {
        final String serverId = "A server ID";
        when(licenseManager.getServerId()).thenReturn(serverId);
        assertEquals(serverId, licenseService.getServerId());
    }

    @Test
    public void shouldFailValidationInvalidLicenseProductKey() {
        final ValidationResult validationResult = licenseService.validate(null, GOOD_LIC_STRING, i18nHelper);
        assertValidationResultContainsSingleError(validationResult, "jira.license.validation.invalid.product.key{[]}");
    }

    @Test
    public void shouldFailValidationInvalidLicenseString() {
        final ValidationResult validationResult = licenseService.validate(mockLicHelper.PORTFOLIO_APP_KEY, "", i18nHelper);
        assertValidationResultContainsSingleError(validationResult, "jira.license.validation.invalid.license.key{[]}");
    }

    @Test
    public void shouldFailValidationUnableToDecode() {
        //noinspection unchecked
        when(licenseManager.getLicense(GOOD_LIC_STRING)).thenThrow(com.atlassian.extras.decoder.api.LicenseDecoderNotFoundException.class);
        final ValidationResult validationResult = licenseService.validate(mockLicHelper.PORTFOLIO_APP_KEY, GOOD_LIC_STRING, i18nHelper);
        assertValidationResultContainsSingleError(validationResult, "jira.license.validation.invalid.license.key{[]}");
    }

    @Test
    public void shouldFailValidationInvalidLicenceString() {
        when(licenseManager.getLicense(BAD_LIC_STRING)).thenThrow(LicenseException.class);
        final ValidationResult validationResult = licenseService.validate(mockLicHelper.PORTFOLIO_APP_KEY, BAD_LIC_STRING, i18nHelper);
        assertValidationResultContainsSingleError(validationResult, "jira.license.validation.invalid.license.key{[]}");
    }

    @Test
    public void shouldFailValidationExpiredLicense() {
        //Given:
        LicenseDetails mockServiceDeskLicenseDetails = new MockLicenseDetailsBuilder()
                .setRawLicense(GOOD_LIC_STRING)
                .setApplicationRoleWithLimit(mockLicHelper.PORTFOLIO_APP_KEY, 11)
                .build();

        //When:
        when(licenseManager.getLicenses()).thenReturn(Lists.newArrayList(mockServiceDeskLicenseDetails));
        when(licenseManager.getLicense(GOOD_LIC_STRING)).thenReturn(mockServiceDeskLicenseDetails);
        when(licenseManager.getLicense(GOOD_LIC_STRING).isExpired()).thenReturn(Boolean.TRUE);

        //Then:
        final ValidationResult validationResult = licenseService.validate(mockLicHelper.PORTFOLIO_APP_KEY, GOOD_LIC_STRING, i18nHelper);
        assertValidationResultContainsSingleError(validationResult, "jira.license.validation.expired{[]}");
    }

    @Test
    public void shouldFailValidationInvalidLicenceVersion() {
        //Given:
        LicenseDetails mockServiceDeskLicenseDetails = new MockLicenseDetailsBuilder()
                .setRawLicense(GOOD_LIC_STRING)
                .setApplicationRoleWithLimit(mockLicHelper.PORTFOLIO_APP_KEY, 11)
                .build();

        //When:
        when(licenseManager.getLicenses()).thenReturn(Lists.newArrayList(mockServiceDeskLicenseDetails));
        when(licenseManager.getLicense(GOOD_LIC_STRING)).thenReturn(mockServiceDeskLicenseDetails);
        when(mockServiceDeskLicenseDetails.getLicenseVersion()).thenReturn(1);

        //Then:
        final ValidationResult validationResult = licenseService.validate(mockLicHelper.PORTFOLIO_APP_KEY, GOOD_LIC_STRING, i18nHelper);
        assertValidationResultContainsSingleError(validationResult, "setup.error.invalidlicensekey.v1.license.version{[]}");
    }

    @Test
    public void shouldFailValidationLicenseInvalidUserLimit() {
        //Given:
        LicenseDetails mockSoftwareLicenseDetails = new MockLicenseDetailsBuilder()
                .setRawLicense(GOOD_LIC_STRING)
                .setApplicationRoleWithLimit(mockLicHelper.SOFTWARE_APP_KEY, 0)
                .setLicenseType(LicenseType.COMMERCIAL)
                .build();

        //When:
        when(licenseManager.getLicenses()).thenReturn(Lists.newArrayList(mockSoftwareLicenseDetails));
        when(licenseManager.getLicense(GOOD_LIC_STRING)).thenReturn(mockSoftwareLicenseDetails);

        //Then:
        final ValidationResult validationResult = licenseService.validate(mockLicHelper.SOFTWARE_APP_KEY, GOOD_LIC_STRING, i18nHelper);
        assertValidationResultContainsSingleError(validationResult, "jira.license.validation.product.user.limit.invalid{[]}");
    }

    @Test
    public void shouldFailLicenseValidationWhenNoApplicationsInLicense() {
        final ValidationResult validationResult = licenseService.validateApplicationLicense(i18nHelper, BAD_LIC_STRING);
        assertValidationResultContainsSingleError(validationResult, "jira.license.validation.invalid.product.key{[]}");
    }

    @Test
    public void shouldSucceedIfLicenseIsForAnyInstalledApplication() {
        mockLicHelper.setupApplications(applicationManager, mockLicHelper.PORTFOLIO_APPLICATION, mockLicHelper.SOFTWARE_APPLICATION);

        //Given:
        LicenseDetails mockLicenseDetails = new MockLicenseDetailsBuilder()
                .setRawLicense(GOOD_LIC_STRING)
                .setApplicationRoleWithLimit(mockLicHelper.PORTFOLIO_APP_KEY, 11)
                .build();

        //When:
        when(licenseManager.getLicenses()).thenReturn(Lists.newArrayList(mockLicenseDetails));
        when(licenseManager.getLicense(GOOD_LIC_STRING)).thenReturn(mockLicenseDetails);

        final ValidationResult validationResult = licenseService.validateApplicationLicense(i18nHelper, GOOD_LIC_STRING);

        //Then:
        assertValidationSucceeded(validationResult);
    }

    @Test
    public void shouldSuccessIfLicenseDoesNotContainGiveeApplication() {
        mockLicHelper.setupApplications(applicationManager, mockLicHelper.PORTFOLIO_APPLICATION);

        //Given:
        LicenseDetails mockLicenseDetails = new MockLicenseDetailsBuilder()
                .setRawLicense(GOOD_LIC_STRING)
                .setApplicationRoleWithLimit(mockLicHelper.PORTFOLIO_APP_KEY, 11)
                .build();

        //When:
        when(licenseManager.getLicenses()).thenReturn(Lists.newArrayList(mockLicenseDetails));
        when(licenseManager.getLicense(GOOD_LIC_STRING)).thenReturn(mockLicenseDetails);

        final ValidationResult validationResult = licenseService.validate(mockLicHelper.SOFTWARE_APP_KEY, GOOD_LIC_STRING, i18nHelper);

        //Then:
        assertValidationSucceeded(validationResult);
    }

    @Test
    public void shouldFailLicenseValidationIfMaintenanceExpDateInValidForBuild() {
        //Given
        //Install Application
        mockLicHelper.setupApplications(applicationManager, mockLicHelper.PORTFOLIO_APPLICATION);

        //Setup license
        LicenseDetails license = new MockLicenseDetailsBuilder()
                .setRawLicense(BAD_LIC_STRING)
                .setApplicationRoleWithLimit(mockLicHelper.PORTFOLIO_APP_KEY, 11)
                .isMaintenanceValidForBuildDate(false)
                .build();
        when(licenseManager.getLicenses()).thenReturn(Lists.newArrayList(license));
        when(licenseManager.getLicense(BAD_LIC_STRING)).thenReturn(license);
        when(licenseManager.isDecodeable(BAD_LIC_STRING)).thenReturn(true);

        //Setup expected error message
        LicenseDetails.LicenseStatusMessage mockStatusMessage = mock(LicenseDetails.LicenseStatusMessage.class);
        when(mockStatusMessage.hasAnyMessages()).thenReturn(true);
        when(mockStatusMessage.getAllMessages(anyString())).thenReturn("ERROR");
        when(license.getMaintenanceMessage(Matchers.<I18nHelper>any(), anyString())).thenReturn(mockStatusMessage);

        //When
        ValidationResult validationResult = licenseService.validate(mockLicHelper.PORTFOLIO_APP_KEY, BAD_LIC_STRING, i18nHelper);
        //Then
        assertValidationResultContainsSingleError(validationResult, "ERROR");
    }

    @Test
    public void shouldSucceedIfLicenseTypeIsUpgradedToDifferentTypeFromNonStarter() {
        mockLicHelper.setupApplications(applicationManager, mockLicHelper.PORTFOLIO_APPLICATION, mockLicHelper.SOFTWARE_APPLICATION);

        LicenseDetails existingLicenseDetails = new MockLicenseDetailsBuilder()
                .setRawLicense(GOOD_LIC_STRING)
                .setLicenseType(LicenseType.NON_PROFIT)
                .setApplicationRoleWithLimit(mockLicHelper.SOFTWARE_APP_KEY, 11)
                .setApplicationRoleWithLimit(mockLicHelper.PORTFOLIO_APP_KEY, 11)
                .build();

        when(licenseManager.getLicenses()).thenReturn(Lists.newArrayList(existingLicenseDetails));

        LicenseDetails newLicenseDetails = new MockLicenseDetailsBuilder()
                .setRawLicense(GOOD_NEW_LIC_STRING)
                .setLicenseType(LicenseType.COMMERCIAL)
                .setApplicationRoleWithLimit(mockLicHelper.SOFTWARE_APP_KEY, 11)
                .build();

        when(licenseManager.getLicense(GOOD_NEW_LIC_STRING)).thenReturn(newLicenseDetails);

        final ValidationResult validationResult = licenseService.validateApplicationLicense(i18nHelper, GOOD_NEW_LIC_STRING);

        assertValidationSucceeded(validationResult);
    }

    @Test
    public void shouldFailIfThereIsOnlyOneExistingLicenseAndNewOneIsForADifferentProduct() {
        mockLicHelper.setupApplications(applicationManager, mockLicHelper.PORTFOLIO_APPLICATION, mockLicHelper.SOFTWARE_APPLICATION);

        LicenseDetails existingLicenseDetails = new MockLicenseDetailsBuilder()
                .setRawLicense(GOOD_LIC_STRING)
                .setLicenseType(LicenseType.NON_PROFIT)
                .setApplicationRoleWithLimit(mockLicHelper.CORE_APP_KEY, 11)
                .build();

        when(licenseManager.getLicenses()).thenReturn(Lists.newArrayList(existingLicenseDetails));

        LicenseDetails newLicenseDetails = new MockLicenseDetailsBuilder()
                .setRawLicense(GOOD_NEW_LIC_STRING)
                .setLicenseType(LicenseType.COMMERCIAL)
                .setApplicationRoleWithLimit(mockLicHelper.SOFTWARE_APP_KEY, 11)
                .build();

        when(licenseManager.getLicense(GOOD_NEW_LIC_STRING)).thenReturn(newLicenseDetails);

        final ValidationResult validationResult = licenseService.validateApplicationLicense(i18nHelper, GOOD_NEW_LIC_STRING);

        assertValidationResultContainsSingleError(validationResult, "jira.license.validation.does.not.match.existing.license{[COMMERCIAL, NON_PROFIT, <a href=\"test-url\">, </a>]}");
    }

    @Test
    public void shouldFailIfThereAreManyExistingNonStarterLicenses() {
        mockLicHelper.setupApplications(applicationManager, mockLicHelper.PORTFOLIO_APPLICATION, mockLicHelper.SOFTWARE_APPLICATION);

        LicenseDetails existingLicenseDetails1 = new MockLicenseDetailsBuilder()
                .setRawLicense(GOOD_LIC_STRING)
                .setLicenseType(LicenseType.NON_PROFIT)
                .setApplicationRoleWithLimit(mockLicHelper.SOFTWARE_APP_KEY, 11)
                .build();

        LicenseDetails existingLicenseDetails2 = new MockLicenseDetailsBuilder()
                .setRawLicense("license 2")
                .setLicenseType(LicenseType.NON_PROFIT)
                .setApplicationRoleWithLimit(mockLicHelper.SOFTWARE_APP_KEY, 11)
                .build();

        when(licenseManager.getLicenses()).thenReturn(Lists.newArrayList(existingLicenseDetails1, existingLicenseDetails2));

        LicenseDetails newLicenseDetails = new MockLicenseDetailsBuilder()
                .setRawLicense(GOOD_NEW_LIC_STRING)
                .setLicenseType(LicenseType.COMMERCIAL)
                .setApplicationRoleWithLimit(mockLicHelper.SOFTWARE_APP_KEY, 11)
                .build();

        when(licenseManager.getLicense(GOOD_NEW_LIC_STRING)).thenReturn(newLicenseDetails);

        final ValidationResult validationResult = licenseService.validateApplicationLicense(i18nHelper, GOOD_NEW_LIC_STRING);

        assertValidationResultContainsSingleError(validationResult, "jira.license.validation.does.not.match.existing.license{[COMMERCIAL, NON_PROFIT, <a href=\"test-url\">, </a>]}");
    }

    @Test
    public void retrievingSupportEntitlementNumberIsSourcingFromLicenseManager() {
        when(licenseManager.getSupportEntitlementNumbers()).thenReturn(ImmutableSortedSet.of("3", "2", "1"));
        SortedSet<String> sens = licenseService.getSupportEntitlementNumbers();
        assertTrue("there are 3 sens", sens.size() == 3);
        Iterator<String> senItem = sens.iterator();
        assertThat("First element is 1", senItem.next(), is("1"));
        assertThat("First element is 2", senItem.next(), is("2"));
        assertThat("First element is 3", senItem.next(), is("3"));
    }

    @Test
    public void validationOfTypeMismatchedLicenseShouldStillSucceedIfNewLicenseIsEvaluation() {
        mockLicHelper.setupApplications(applicationManager, mockLicHelper.SOFTWARE_APPLICATION);

        LicenseDetails existingLicenseDetails = new MockLicenseDetailsBuilder()
                .setRawLicense(GOOD_LIC_STRING)
                .setLicenseType(LicenseType.ACADEMIC)
                .setApplicationRoleWithLimit(mockLicHelper.SOFTWARE_APP_KEY, 11)
                .build();

        when(licenseManager.getLicenses()).thenReturn(Lists.newArrayList(existingLicenseDetails));

        LicenseDetails newLicenseDetails = new MockLicenseDetailsBuilder()
                .setRawLicense(GOOD_NEW_LIC_STRING)
                .setLicenseType(LicenseType.COMMERCIAL) // Evaluation licenses are of type COMMERCIAL
                .setApplicationRoleWithLimit(mockLicHelper.CORE_APP_KEY, 11)
                .setAsEvaluationLicense(true)
                .build();

        when(licenseManager.getLicense(GOOD_NEW_LIC_STRING)).thenReturn(newLicenseDetails);

        final ValidationResult validationResult = licenseService.validateApplicationLicense(i18nHelper, GOOD_NEW_LIC_STRING);

        assertValidationSucceeded(validationResult);
    }

    @Test
    public void validationOfEvaluationLicenseWithoutDataCenterShouldFailIfExistingLicenseHasDataCenter() {
        mockLicHelper.setupApplications(applicationManager, mockLicHelper.SOFTWARE_APPLICATION);

        LicenseDetails existingLicenseDetails = new MockLicenseDetailsBuilder()
                .setRawLicense(GOOD_LIC_STRING)
                .setLicenseType(LicenseType.ACADEMIC)
                .setApplicationRoleWithLimit(mockLicHelper.SOFTWARE_APP_KEY, 11)
                .setAsDataCenter(true)
                .build();

        when(licenseManager.getLicenses()).thenReturn(Lists.newArrayList(existingLicenseDetails));

        LicenseDetails newLicenseDetails = new MockLicenseDetailsBuilder()
                .setRawLicense(GOOD_NEW_LIC_STRING)
                .setLicenseType(LicenseType.COMMERCIAL) // Evaluation licenses are of type COMMERCIAL
                .setApplicationRoleWithLimit(mockLicHelper.CORE_APP_KEY, 11)
                .setAsEvaluationLicense(true)
                .build();

        when(licenseManager.getLicense(GOOD_NEW_LIC_STRING)).thenReturn(newLicenseDetails);

        final ValidationResult validationResult = licenseService.validateApplicationLicense(i18nHelper, GOOD_NEW_LIC_STRING);

        assertValidationResultContainsSingleError(validationResult, "jira.license.validation.datacenter.mix{[<a href=\"test-url\">, </a>]}");
    }

    @Test
    public void validationOfTypeMismatchedLicenseShouldStillSucceedIfNewLicenseIsStarter() {
        mockLicHelper.setupApplications(applicationManager, mockLicHelper.SOFTWARE_APPLICATION);

        LicenseDetails existingLicenseDetails = new MockLicenseDetailsBuilder()
                .setRawLicense(GOOD_LIC_STRING)
                .setLicenseType(LicenseType.ACADEMIC)
                .setApplicationRoleWithLimit(mockLicHelper.SOFTWARE_APP_KEY, 110)
                .build();

        when(licenseManager.getLicenses()).thenReturn(Lists.newArrayList(existingLicenseDetails));

        LicenseDetails newLicenseDetails = new MockLicenseDetailsBuilder()
                .setRawLicense(GOOD_NEW_LIC_STRING)
                .setLicenseType(LicenseType.STARTER)
                .setApplicationRoleWithLimit(mockLicHelper.CORE_APP_KEY, 10)
                .build();

        when(licenseManager.getLicense(GOOD_NEW_LIC_STRING)).thenReturn(newLicenseDetails);

        final ValidationResult validationResult = licenseService.validateApplicationLicense(i18nHelper, GOOD_NEW_LIC_STRING);

        assertValidationSucceeded(validationResult);
    }

    @Test
    public void licenseTypeValidationShouldFailWhenMixingPaidAndNotPaidTypes_nonStarterNonEvaluation() {
        mockLicHelper.setupApplications(applicationManager, mockLicHelper.SOFTWARE_APPLICATION);

        LicenseDetails existingLicenseDetails = new MockLicenseDetailsBuilder()
                .setRawLicense(GOOD_LIC_STRING)
                .setLicenseType(LicenseType.NON_PROFIT)
                .setApplicationRoleWithLimit(mockLicHelper.SOFTWARE_APP_KEY, 11)
                .build();

        when(licenseManager.getLicenses()).thenReturn(Lists.newArrayList(existingLicenseDetails));

        LicenseDetails newLicenseDetails = new MockLicenseDetailsBuilder()
                .setRawLicense(GOOD_NEW_LIC_STRING)
                .setLicenseType(LicenseType.COMMERCIAL)
                .setApplicationRoleWithLimit(mockLicHelper.CORE_APP_KEY, 11)
                .setAsEvaluationLicense(false)
                .build();

        when(licenseManager.getLicense(GOOD_NEW_LIC_STRING)).thenReturn(newLicenseDetails);

        final ValidationResult validationResult = licenseService.validateApplicationLicense(i18nHelper, GOOD_NEW_LIC_STRING);

        assertValidationResultContainsSingleError(validationResult, "jira.license.validation.does.not.match.existing.license{[COMMERCIAL, NON_PROFIT, <a href=\"test-url\">, </a>]}");
    }

    @Test
    public void licenseTypeValidationShouldSucceedWhenMixingPaidTypes() {
        mockLicHelper.setupApplications(applicationManager, mockLicHelper.SOFTWARE_APPLICATION);

        LicenseDetails existingLicenseDetails = new MockLicenseDetailsBuilder()
                .setRawLicense(GOOD_LIC_STRING)
                .setLicenseType(LicenseType.ACADEMIC)
                .setApplicationRoleWithLimit(mockLicHelper.SOFTWARE_APP_KEY, 11)
                .build();

        when(licenseManager.getLicenses()).thenReturn(Lists.newArrayList(existingLicenseDetails));

        LicenseDetails newLicenseDetails = new MockLicenseDetailsBuilder()
                .setRawLicense(GOOD_NEW_LIC_STRING)
                .setLicenseType(LicenseType.COMMERCIAL)
                .setApplicationRoleWithLimit(mockLicHelper.CORE_APP_KEY, 11)
                .setAsEvaluationLicense(false)
                .build();

        when(licenseManager.getLicense(GOOD_NEW_LIC_STRING)).thenReturn(newLicenseDetails);

        final ValidationResult validationResult = licenseService.validateApplicationLicense(i18nHelper, GOOD_NEW_LIC_STRING);

        assertFalse(validationResult.getErrorCollection().hasAnyErrors());
    }

    @Test
    public void licenseTypeValidationShouldSucceedWhenMixingNotPaidTypes() {
        mockLicHelper.setupApplications(applicationManager, mockLicHelper.SOFTWARE_APPLICATION);

        LicenseDetails existingLicenseDetails = new MockLicenseDetailsBuilder()
                .setRawLicense(GOOD_LIC_STRING)
                .setLicenseType(LicenseType.NON_PROFIT)
                .setApplicationRoleWithLimit(mockLicHelper.SOFTWARE_APP_KEY, 11)
                .build();

        when(licenseManager.getLicenses()).thenReturn(Lists.newArrayList(existingLicenseDetails));

        LicenseDetails newLicenseDetails = new MockLicenseDetailsBuilder()
                .setRawLicense(GOOD_NEW_LIC_STRING)
                .setLicenseType(LicenseType.COMMUNITY)
                .setApplicationRoleWithLimit(mockLicHelper.CORE_APP_KEY, 11)
                .setAsEvaluationLicense(false)
                .build();

        when(licenseManager.getLicense(GOOD_NEW_LIC_STRING)).thenReturn(newLicenseDetails);

        final ValidationResult validationResult = licenseService.validateApplicationLicense(i18nHelper, GOOD_NEW_LIC_STRING);

        assertFalse(validationResult.getErrorCollection().hasAnyErrors());
    }

    private void assertValidationResult(final ValidationResult validationResult, final String expectedMsg) {
        assertNotNull(validationResult);
        assertTrue(validationResult.getErrorCollection().hasAnyErrors());
        assertEquals(expectedMsg, validationResult.getErrorCollection().getErrors().get("license"));
    }

    private void assertValidationSucceeded(@Nonnull final ValidationResult validationResult) {
        assertFalse(validationResult.getErrorCollection().hasAnyErrors());
    }


    private void assertValidationResultContainsSingleError(final ValidationResult validationResult, String errorKey) {
        final String errorKeyFound = validationResult.getErrorCollection().getErrors().values().iterator().next();
        assertEquals(errorKey, errorKeyFound);
        assertEquals(1, validationResult.getErrorCollection().getErrors().values().size());
    }

}
