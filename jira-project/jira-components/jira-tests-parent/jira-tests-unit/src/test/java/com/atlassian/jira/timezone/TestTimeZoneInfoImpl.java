package com.atlassian.jira.timezone;

import com.atlassian.jira.mock.i18n.MockI18nHelper;
import org.junit.Before;
import org.junit.Test;

import java.util.TimeZone;

import static org.junit.Assert.assertEquals;

public class TestTimeZoneInfoImpl {
    private MockI18nHelper i18nHelper;

    @Before
    public void setUp() throws Exception {
        i18nHelper = new MockI18nHelper();
    }

    @Test
    public void shouldGetActualTimeZoneCityAndReplaceSlashesWithDotsWhenTimezoneIsJira() throws Exception {
        final TimeZone actualTimeZone = TimeZone.getTimeZone("Australia/Sydney");
        final TimeZoneInfoImpl timeZoneInfo = new TimeZoneInfoImpl(TimeZoneService.JIRA, "Eastern Standard Time", actualTimeZone, i18nHelper, "notImportant");

        assertEquals("timezone.zone.australia.sydney", timeZoneInfo.getCity());
    }

    @Test
    public void shouldReturnTimezoneIdWhenIsNotSystemOrJira() {
        final TimeZone notImportantTimezone = null;
        final TimeZoneInfoImpl timeZoneInfo = new TimeZoneInfoImpl("timezone.id", "Eastern Standard Time", notImportantTimezone, i18nHelper, "notImportant");

        assertEquals("timezone.zone.timezone.id", timeZoneInfo.getCity());
    }
}
