package com.atlassian.jira.security.type;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.fugue.Option;
import com.atlassian.jira.application.ApplicationAuthorizationService;
import com.atlassian.jira.application.ApplicationRole;
import com.atlassian.jira.application.ApplicationRoleManager;
import com.atlassian.jira.application.MockApplicationRole;
import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.bc.MockJiraServiceContext;
import com.atlassian.jira.mock.i18n.MockI18nHelper;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.MockGroup;
import com.atlassian.jira.user.util.UserUtil;
import com.atlassian.jira.util.collect.MapBuilder;
import com.google.common.collect.Sets;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import static com.atlassian.application.api.ApplicationKey.valueOf;
import static com.atlassian.fugue.Iterables.first;
import static com.atlassian.fugue.Option.none;
import static com.atlassian.fugue.Option.some;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestApplicationRoleSecurityType {
    @Rule
    public final MethodRule initMockito = MockitoJUnit.rule();

    private static final Project PROJECT = new MockProject();
    private static final ApplicationUser FRED = new MockApplicationUser("fred");
    private static final String SOFTWARE_KEY = "jira-software";
    private static final Group DEV_GROUP = new MockGroup("jira-dev");
    private static final Group SW_GROUP = new MockGroup("jira-software-users");
    private static final ApplicationRole SOFTWARE_ROLE = new MockApplicationRole(valueOf(SOFTWARE_KEY)).name("JIRA Software").groups(DEV_GROUP, SW_GROUP);
    private static final String ANY_ROLE = null;

    @Mock
    private JiraAuthenticationContext authenticationContext;
    @Mock
    private ApplicationAuthorizationService applicationAuthorizationService;
    @Mock
    private ApplicationRoleManager applicationRoleManager;
    @Mock
    private UserUtil userUtil;

    private ApplicationRoleSecurityType securityType;
    private JiraServiceContext serviceContext;

    @Before
    public void setUp() throws Exception {
        serviceContext = new MockJiraServiceContext(FRED, new MockI18nHelper());
        when(authenticationContext.getI18nHelper()).thenReturn(new MockI18nHelper());

        securityType = new ApplicationRoleSecurityType(authenticationContext,
                applicationAuthorizationService, applicationRoleManager, userUtil);
    }

    @Test
    public void argumentDisplayReturnsAnyWhenNoRoleSelected() {
        String argumentDisplay = securityType.getArgumentDisplay("");

        assertThat(argumentDisplay, is("admin.permission.types.application.role.any"));
    }

    @Test
    public void argumentDisplayReturnsRoleName() {
        when(applicationRoleManager.getRole(valueOf(SOFTWARE_KEY))).thenReturn(some(SOFTWARE_ROLE));

        String argumentDisplay = securityType.getArgumentDisplay(SOFTWARE_KEY);

        assertThat(argumentDisplay, is("JIRA Software"));
    }

    @Test
    public void argumentDisplayDoesNotThrowExceptionForInvalidApplicationKey() {
        String argumentDisplay = securityType.getArgumentDisplay("invalid_key_$@#%");

        assertThat(argumentDisplay, is("invalid_key_$@#%"));
    }

    @Test
    public void argumentDisplayReturnsArgumentWhenRoleUnknown() {
        when(applicationRoleManager.getRole(valueOf(SOFTWARE_KEY))).thenReturn(none());

        String argumentDisplay = securityType.getArgumentDisplay(SOFTWARE_KEY);

        assertThat(argumentDisplay, is(SOFTWARE_KEY));
    }

    @Test(expected = IllegalArgumentException.class)
    public void permissionCheckThrowsExceptionForAnonymousUser() {
        securityType.hasPermission(PROJECT, ANY_ROLE, null, true);
    }

    @Test
    public void permissionCheckPassesForAnyApplicationRoleWithUser() {
        boolean hasPermission = securityType.hasPermission(PROJECT, ANY_ROLE, FRED, true);

        assertTrue(hasPermission);
    }

    @Test
    public void permissionCheckFailsIfUserNotPartOfRole() {
        when(applicationAuthorizationService.canUseApplication(FRED, valueOf(SOFTWARE_KEY))).thenReturn(false);

        boolean hasPermission = securityType.hasPermission(PROJECT, SOFTWARE_KEY, FRED, true);

        assertFalse(hasPermission);
    }

    @Test
    public void permissionCheckPassesIfUserPartOfRole() {
        when(applicationAuthorizationService.canUseApplication(FRED, valueOf(SOFTWARE_KEY))).thenReturn(true);

        boolean hasPermission = securityType.hasPermission(PROJECT, SOFTWARE_KEY, FRED, true);

        assertTrue(hasPermission);
    }

    @Test
    public void validationPassesForAnyRole() {
        securityType.doValidation("applicationRole", MapBuilder.build("applicationRole", ANY_ROLE), serviceContext);

        assertFalse(serviceContext.getErrorCollection().hasAnyErrors());
    }

    @Test
    public void validationFailsForInvalidRole() {
        when(applicationRoleManager.getRole(valueOf("invalidRole"))).thenReturn(none());

        securityType.doValidation("applicationRole", MapBuilder.build("applicationRole", "invalidRole"), serviceContext);

        assertTrue(serviceContext.getErrorCollection().hasAnyErrors());
        assertThat(first(serviceContext.getErrorCollection().getErrorMessages()).get(), is("admin.permissions.errors.please.select.application.role"));
    }

    @Test
    public void validationPassesForValidRole() {
        when(applicationRoleManager.getRole(valueOf(SOFTWARE_KEY))).thenReturn(Option.option(SOFTWARE_ROLE));

        securityType.doValidation("applicationRole", MapBuilder.build("applicationRole", SOFTWARE_KEY), serviceContext);

        assertFalse(serviceContext.getErrorCollection().hasAnyErrors());
    }

    @Test
    public void getUsersReturnsAllUsersForAnyRole() {
        securityType.getUsers(null, ANY_ROLE);

        verify(userUtil, times(1)).getAllApplicationUsers();
    }

    @Test
    public void getUsersReturnsAllUsersForInGroupsDefinedForRole() {
        when(applicationRoleManager.getRole(ApplicationKey.valueOf(SOFTWARE_KEY))).thenReturn(Option.option(SOFTWARE_ROLE));

        securityType.getUsers(null, SOFTWARE_KEY);

        verify(userUtil, times(1)).getAllUsersInGroups(eq(Sets.newHashSet(SW_GROUP, DEV_GROUP)));
    }

    @Test
    public void getApplicationRolesFetchesThemFromTheApplicationRoleManager() {
        securityType.getApplicationRoles();

        verify(applicationRoleManager, times(1)).getRoles();
    }
}
