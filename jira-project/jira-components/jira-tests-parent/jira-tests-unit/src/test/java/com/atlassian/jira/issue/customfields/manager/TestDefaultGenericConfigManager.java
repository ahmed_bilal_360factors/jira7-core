package com.atlassian.jira.issue.customfields.manager;

import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.config.properties.JiraProperties;
import com.atlassian.jira.issue.customfields.converters.DatePickerConverter;
import com.atlassian.jira.issue.customfields.view.CustomFieldParamsImpl;
import com.atlassian.jira.mock.ofbiz.MockGenericValue;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.util.collect.MapBuilder;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.thoughtworks.xstream.security.ForbiddenClassException;
import org.apache.commons.collections.comparators.ComparableComparator;
import org.apache.commons.collections.comparators.TransformingComparator;
import org.apache.commons.collections.functors.InvokerTransformer;
import org.apache.xalan.xsltc.trax.TemplatesImpl;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.ofbiz.core.entity.GenericEntityException;
import org.ofbiz.core.entity.GenericValue;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static java.lang.Double.compare;
import static org.hamcrest.Matchers.is;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.util.AssertionErrors.assertEquals;
import static org.springframework.test.util.AssertionErrors.assertTrue;
import static org.springframework.test.util.MatcherAssertionErrors.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class TestDefaultGenericConfigManager {

    private static final String JIRA_CONFIG_STOP_FILTERING = "enable.generic.config.serialization.whitelistdisabled";
    private static final String KEY = "Bravo";
    private static final String DATA_TYPE = "Alpha";
    private static Deserialisables.Alpha goldenAlpha;

    private final static String MAP_XML = "<map>" +
            "   <entry>" +
            "       <string>one</string>" +
            "       <string>1.0</string>" +
            "   </entry>" +
            "   <entry>" +
            "       <string>two</string>" +
            "       <string>2.0</string>" +
            "   </entry>" +
            "   <entry>" +
            "       <string>three</string>" +
            "       <string>3.0</string>" +
            "   </entry>" +
            "</map>";

    private final static String SET_XML = "<set>" +
            "<long>1</long>" +
            "<long>2</long>" +
            "<long>3</long>" +
            "<long>4</long>" +
            "<long>5</long>" +
            "</set>";

    private final static String LIST_XML =
            "<list>" +
                    "  <long>1</long>" +
                    "  <long>2</long>" +
                    "  <long>3</long>" +
                    "  <long>4</long>" +
                    "  <long>5</long>" +
                    "</list>";

    private final String SERIALISED_ALPHA =
            "<com.atlassian.jira.issue.customfields.manager.Deserialisables_-Alpha>\n" +
                    "  <a>3</a>\n" +
                    "  <myList class=\"linked-list\">\n" +
                    "      <string>one</string>\n" +
                    "      <string>two</string>\n" +
                    "  </myList>\n" +
                    "</com.atlassian.jira.issue.customfields.manager.Deserialisables_-Alpha>";

    private final String UNMODIFIABLE_LIST =
            "<java.util.Collections_-UnmodifiableRandomAccessList resolves-to=\"java.util.Collections$UnmodifiableList\">\n" +
                    "  <c class=\"list\">\n" +
                    "    <string>one</string>\n" +
                    "    <string>two</string>\n" +
                    "    <string>three</string>\n" +
                    "    <string>two</string>\n" +
                    "    <string>one</string>\n" +
                    "  </c>\n" +
                    "  <list reference=\"../c\"/>\n" +
                    "</java.util.Collections_-UnmodifiableRandomAccessList>";

    private final String TRANSFORMING_COMPARATOR =
            "<org.apache.commons.collections.comparators.TransformingComparator>\n" +
                    "  <decorated class=\"org.apache.commons.collections.comparators.ComparableComparator\"/>\n" +
                    "  <transformer class=\"org.apache.commons.collections.functors.CloneTransformer\"/>\n" +
                    "</org.apache.commons.collections.comparators.TransformingComparator>";

    private final String INVOKER_TRANSFORMER =
            "<org.apache.commons.collections.functors.InvokerTransformer>\n" +
                    "  <iMethodName>touch</iMethodName>\n" +
                    "  <iParamTypes/>\n" +
                    "  <iArgs/>\n" +
                    "</org.apache.commons.collections.functors.InvokerTransformer>";

    private final String TEMPLATES_IMPL =
            "<org.apache.xalan.xsltc.trax.TemplatesImpl serialization=\"custom\">\n" +
                    "  <org.apache.xalan.xsltc.trax.TemplatesImpl>\n" +
                    "    <default>\n" +
                    "      <__indentNumber>0</__indentNumber>\n" +
                    "      <__transletIndex>-1</__transletIndex>\n" +
                    "    </default>\n" +
                    "    <boolean>false</boolean>\n" +
                    "  </org.apache.xalan.xsltc.trax.TemplatesImpl>\n" +
                    "</org.apache.xalan.xsltc.trax.TemplatesImpl>";

    private final String CUSTOM_FIELD_PARAMETERS = "<com.atlassian.jira.issue.customfields.view.CustomFieldParamsImpl>\n" +
            "  <parameterMap/>\n" +
            "</com.atlassian.jira.issue.customfields.view.CustomFieldParamsImpl>";

    private final String collections3Payload = "<java.util.PriorityQueue serialization=\"custom\">\n" +
            "  <unserializable-parents/>\n" +
            "  <java.util.PriorityQueue>\n" +
            "    <default>\n" +
            "      <size>2</size>\n" +
            "      <comparator class=\"org.apache.commons.collections.comparators.TransformingComparator\">\n" +
            "        <decorated class=\"org.apache.commons.collections.comparators.ComparableComparator\"/>\n" +
            "        <transformer class=\"org.apache.commons.collections.functors.InvokerTransformer\">\n" +
            "          <iMethodName>newTransformer</iMethodName>\n" +
            "          <iParamTypes/>\n" +
            "          <iArgs/>\n" +
            "        </transformer>\n" +
            "      </comparator>\n" +
            "    </default>\n" +
            "    <int>3</int>\n" +
            "    <com.sun.org.apache.xalan.internal.xsltc.trax.TemplatesImpl serialization=\"custom\">\n" +
            "      <com.sun.org.apache.xalan.internal.xsltc.trax.TemplatesImpl>\n" +
            "        <default>\n" +
            "          <__name>Pwnr</__name>\n" +
            "          <__bytecodes>\n" +
            "            <byte-array>yv66vgAAADEAOAoAAwAiBwA2BwAlBwAmAQAQc2VyaWFsVmVyc2lvblVJRAEAAUoBAA1Db25zdGFu\n"
            + "dFZhbHVlBa0gk/OR3e8+AQAGPGluaXQ+AQADKClWAQAEQ29kZQEAD0xpbmVOdW1iZXJUYWJsZQEA\n"
            + "EkxvY2FsVmFyaWFibGVUYWJsZQEABHRoaXMBABNTdHViVHJhbnNsZXRQYXlsb2FkAQAMSW5uZXJD\n"
            + "bGFzc2VzAQA1THlzb3NlcmlhbC9wYXlsb2Fkcy91dGlsL0dhZGdldHMkU3R1YlRyYW5zbGV0UGF5\n"
            + "bG9hZDsBAAl0cmFuc2Zvcm0BAHIoTGNvbS9zdW4vb3JnL2FwYWNoZS94YWxhbi9pbnRlcm5hbC94\n"
            + "c2x0Yy9ET007W0xjb20vc3VuL29yZy9hcGFjaGUveG1sL2ludGVybmFsL3NlcmlhbGl6ZXIvU2Vy\n"
            + "aWFsaXphdGlvbkhhbmRsZXI7KVYBAAhkb2N1bWVudAEALUxjb20vc3VuL29yZy9hcGFjaGUveGFs\n"
            + "YW4vaW50ZXJuYWwveHNsdGMvRE9NOwEACGhhbmRsZXJzAQBCW0xjb20vc3VuL29yZy9hcGFjaGUv\n"
            + "eG1sL2ludGVybmFsL3NlcmlhbGl6ZXIvU2VyaWFsaXphdGlvbkhhbmRsZXI7AQAKRXhjZXB0aW9u\n"
            + "cwcAJwEApihMY29tL3N1bi9vcmcvYXBhY2hlL3hhbGFuL2ludGVybmFsL3hzbHRjL0RPTTtMY29t\n"
            + "L3N1bi9vcmcvYXBhY2hlL3htbC9pbnRlcm5hbC9kdG0vRFRNQXhpc0l0ZXJhdG9yO0xjb20vc3Vu\n"
            + "L29yZy9hcGFjaGUveG1sL2ludGVybmFsL3NlcmlhbGl6ZXIvU2VyaWFsaXphdGlvbkhhbmRsZXI7\n"
            + "KVYBAAhpdGVyYXRvcgEANUxjb20vc3VuL29yZy9hcGFjaGUveG1sL2ludGVybmFsL2R0bS9EVE1B\n"
            + "eGlzSXRlcmF0b3I7AQAHaGFuZGxlcgEAQUxjb20vc3VuL29yZy9hcGFjaGUveG1sL2ludGVybmFs\n"
            + "L3NlcmlhbGl6ZXIvU2VyaWFsaXphdGlvbkhhbmRsZXI7AQAKU291cmNlRmlsZQEADEdhZGdldHMu\n"
            + "amF2YQwACgALBwAoAQAzeXNvc2VyaWFsL3BheWxvYWRzL3V0aWwvR2FkZ2V0cyRTdHViVHJhbnNs\n"
            + "ZXRQYXlsb2FkAQBAY29tL3N1bi9vcmcvYXBhY2hlL3hhbGFuL2ludGVybmFsL3hzbHRjL3J1bnRp\n"
            + "bWUvQWJzdHJhY3RUcmFuc2xldAEAFGphdmEvaW8vU2VyaWFsaXphYmxlAQA5Y29tL3N1bi9vcmcv\n"
            + "YXBhY2hlL3hhbGFuL2ludGVybmFsL3hzbHRjL1RyYW5zbGV0RXhjZXB0aW9uAQAfeXNvc2VyaWFs\n"
            + "L3BheWxvYWRzL3V0aWwvR2FkZ2V0cwEACDxjbGluaXQ+AQARamF2YS9sYW5nL1J1bnRpbWUHACoB\n"
            + "AApnZXRSdW50aW1lAQAVKClMamF2YS9sYW5nL1J1bnRpbWU7DAAsAC0KACsALgEAFHRvdWNoIC90\n"
            + "bXAvWVNPU0VSSUFMCAAwAQAEZXhlYwEAJyhMamF2YS9sYW5nL1N0cmluZzspTGphdmEvbGFuZy9Q\n"
            + "cm9jZXNzOwwAMgAzCgArADQBAB15c29zZXJpYWwvUHduZXI1NjQwOTQyNDY5NTE0MQEAH0x5c29z\n"
            + "ZXJpYWwvUHduZXI1NjQwOTQyNDY5NTE0MTsAIQACAAMAAQAEAAEAGgAFAAYAAQAHAAAAAgAIAAQA\n"
            + "AQAKAAsAAQAMAAAALwABAAEAAAAFKrcAAbEAAAACAA0AAAAGAAEAAAAdAA4AAAAMAAEAAAAFAA8A\n"
            + "NwAAAAEAEwAUAAIADAAAAD8AAAADAAAAAbEAAAACAA0AAAAGAAEAAAAgAA4AAAAgAAMAAAABAA8A\n"
            + "NwAAAAAAAQAVABYAAQAAAAEAFwAYAAIAGQAAAAQAAQAaAAEAEwAbAAIADAAAAEkAAAAEAAAAAbEA\n"
            + "AAACAA0AAAAGAAEAAAAjAA4AAAAqAAQAAAABAA8ANwAAAAAAAQAVABYAAQAAAAEAHAAdAAIAAAAB\n"
            + "AB4AHwADABkAAAAEAAEAGgAIACkACwABAAwAAAAbAAMAAgAAAA+nAAMBTLgALxIxtgA1V7EAAAAA\n"
            + "AAIAIAAAAAIAIQARAAAACgABAAIAIwAQAAk=</byte-array>\n"
            + "            <byte-array>yv66vgAAADEAGwoAAwAVBwAXBwAYBwAZAQAQc2VyaWFsVmVyc2lvblVJRAEAAUoBAA1Db25zdGFu\n"
            + "dFZhbHVlBXHmae48bUcYAQAGPGluaXQ+AQADKClWAQAEQ29kZQEAD0xpbmVOdW1iZXJUYWJsZQEA\n"
            + "EkxvY2FsVmFyaWFibGVUYWJsZQEABHRoaXMBAANGb28BAAxJbm5lckNsYXNzZXMBACVMeXNvc2Vy\n"
            + "aWFsL3BheWxvYWRzL3V0aWwvR2FkZ2V0cyRGb287AQAKU291cmNlRmlsZQEADEdhZGdldHMuamF2\n"
            + "YQwACgALBwAaAQAjeXNvc2VyaWFsL3BheWxvYWRzL3V0aWwvR2FkZ2V0cyRGb28BABBqYXZhL2xh\n"
            + "bmcvT2JqZWN0AQAUamF2YS9pby9TZXJpYWxpemFibGUBAB95c29zZXJpYWwvcGF5bG9hZHMvdXRp\n"
            + "bC9HYWRnZXRzACEAAgADAAEABAABABoABQAGAAEABwAAAAIACAABAAEACgALAAEADAAAAC8AAQAB\n"
            + "AAAABSq3AAGxAAAAAgANAAAABgABAAAAJwAOAAAADAABAAAABQAPABIAAAACABMAAAACABQAEQAA\n"
            + "AAoAAQACABYAEAAJ</byte-array>" +
            "          </__bytecodes>\n" +
            "          <__transletIndex>-1</__transletIndex>\n" +
            "          <__indentNumber>0</__indentNumber>\n" +
            "        </default>\n" +
            "        <boolean>false</boolean>\n" +
            "      </com.sun.org.apache.xalan.internal.xsltc.trax.TemplatesImpl>\n" +
            "    </com.sun.org.apache.xalan.internal.xsltc.trax.TemplatesImpl>\n" +
            "    <int>1</int>\n" +
            "  </java.util.PriorityQueue>\n" +
            "</java.util.PriorityQueue>";

    // Note the following two constants are setup to pass the correct intention to the setupTest() second parameter.
    private final boolean FILTERING_ENABLED = false;
    private final boolean FILTERING_DISABLED = true;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Mock
    OfBizDelegator delegator;

    @Mock
    FeatureManager featureManager;

    @Mock
    JiraProperties jiraProperties;

    DefaultGenericConfigManager itemUnderTest;

    @Before
    public void setup() {
        goldenAlpha = new Deserialisables.Alpha();
        goldenAlpha.add("one");
        goldenAlpha.add("two");
        goldenAlpha.a = 3;
    }

    @Test
    public void btfBlocksTransformingComparator() {
        expectedException.expect(ForbiddenClassException.class);
        expectedException.expectMessage(TransformingComparator.class.getCanonicalName());
        setupTest(TRANSFORMING_COMPARATOR, FILTERING_ENABLED);
        itemUnderTest.retrieve(DATA_TYPE, KEY);
    }

    @Test
    public void btfBlocksCompareComparator() {
        expectedException.expect(ForbiddenClassException.class);
//        expectedException.expectMessage(ComparableComparator.class.getCanonicalName());
        setupTest("<org.apache.commons.collections.comparators.ComparableComparator/>", FILTERING_ENABLED);
        itemUnderTest.retrieve(DATA_TYPE, KEY);
    }

    @Test
    public void btfBlocksInvokerTransfrormer() {
        expectedException.expect(ForbiddenClassException.class);
        expectedException.expectMessage(InvokerTransformer.class.getCanonicalName());
        setupTest(INVOKER_TRANSFORMER, FILTERING_ENABLED);
        itemUnderTest.retrieve(DATA_TYPE, KEY);
    }

    @Test
    public void btfBlocksInvokerTemplatesImpl() {
        expectedException.expect(ForbiddenClassException.class);
        expectedException.expectMessage(TemplatesImpl.class.getCanonicalName());
        setupTest(TEMPLATES_IMPL, FILTERING_ENABLED);
        itemUnderTest.retrieve(DATA_TYPE, KEY);
    }

    @Test
    public void btfDeserialisesAnythingElse() {
        setupTest(SERIALISED_ALPHA, FILTERING_ENABLED);
        Deserialisables.Alpha result = (Deserialisables.Alpha) itemUnderTest.retrieve(DATA_TYPE, KEY);
        assertEquals("String is deserialised", result, goldenAlpha);
    }

    @Test
    public void testUpdateToDefaultDate() throws GenericEntityException {
        GenericValue gv = mock(GenericValue.class);
        itemUnderTest = new DefaultGenericConfigManager(delegator, featureManager, jiraProperties);

        when(delegator.findByAnd(eq(DefaultGenericConfigManager.ENTITY_TABLE_NAME),
                eq(new ImmutableMap.Builder<String, String>()
                        .put("datakey", "10101")
                        .put("datatype", "DefaultValue")
                        .build()))).thenReturn(Lists.newArrayList(gv));

        itemUnderTest.update("DefaultValue", "10101", DatePickerConverter.USE_NOW_DATE);

        verify(gv).setString(eq(DefaultGenericConfigManager.ENTITY_XML_VALUE),
                eq("<sql-timestamp>0001-01-01 00:00:00.0</sql-timestamp>"));
        verify(gv).store();
    }

    @Test
    public void testRetrieveDefaultDate() {
        GenericValue gv = mock(GenericValue.class);
        when(delegator.findByAnd(eq(DefaultGenericConfigManager.ENTITY_TABLE_NAME),
                eq(new ImmutableMap.Builder<String, String>()
                        .put("datatype", "DefaultValue")
                        .put("datakey", "10101")
                        .build()))).thenReturn(Lists.newArrayList(gv));

        when(gv.getString(DefaultGenericConfigManager.ENTITY_XML_VALUE))
                .thenReturn("<sql-timestamp>0001-01-01 00:00:00.0</sql-timestamp>");

        itemUnderTest = new DefaultGenericConfigManager(delegator, featureManager, jiraProperties);
        
        Date retrieved = (Date)itemUnderTest.retrieve("DefaultValue", "10101");
        Date expected = DatePickerConverter.USE_NOW_DATE;
        assertEquals("Retrieved date should be USE_NOW_DATE", expected, retrieved);
    }

    @Test
    public void testUpdateToCustomDate() throws GenericEntityException {
        GenericValue gv = mock(GenericValue.class);
        itemUnderTest = new DefaultGenericConfigManager(delegator, featureManager, jiraProperties);

        when(delegator.findByAnd(eq(DefaultGenericConfigManager.ENTITY_TABLE_NAME),
                eq(new ImmutableMap.Builder<String, String>()
                        .put("datakey", "10101")
                        .put("datatype", "DefaultValue")
                        .build()))).thenReturn(Lists.newArrayList(gv));

        itemUnderTest.update("DefaultValue", "10101",
                new Timestamp((new GregorianCalendar(2016, 1, 24)).getTime().getTime()));

        verify(gv).setString(eq(DefaultGenericConfigManager.ENTITY_XML_VALUE),
                eq("<sql-timestamp>2016-02-24 00:00:00.0</sql-timestamp>"));
        verify(gv).store();
    }

    @Test
    public void testRetrieveCustomDate() {
        GenericValue gv = mock(GenericValue.class);
        when(delegator.findByAnd(eq(DefaultGenericConfigManager.ENTITY_TABLE_NAME),
                eq(new ImmutableMap.Builder<String, String>()
                        .put("datatype", "DefaultValue")
                        .put("datakey", "10101")
                        .build()))).thenReturn(Lists.newArrayList(gv));

        when(gv.getString(DefaultGenericConfigManager.ENTITY_XML_VALUE))
                .thenReturn("<sql-timestamp>2016-02-24 00:00:00.0</sql-timestamp>");

        itemUnderTest = new DefaultGenericConfigManager(delegator, featureManager, jiraProperties);

        Date retrieved = (Date)itemUnderTest.retrieve("DefaultValue", "10101");
        Calendar expected = new GregorianCalendar(2016, 1, 24);
        assertEquals("Retrieved date should be 24/Feb/2016", expected.getTime(), retrieved);
    }

    private void setupTest(String xmlValue, boolean disableFiltering) {
        when(jiraProperties.getBoolean(JIRA_CONFIG_STOP_FILTERING)).thenReturn(disableFiltering);
        itemUnderTest = new DefaultGenericConfigManager(delegator, featureManager, jiraProperties);
        GenericValue gv = new MockGenericValue("DefaultValue",
                MapBuilder.<String, String>newBuilder().add(GenericConfigManager.ENTITY_XML_VALUE, xmlValue).toHashMap());
        when(delegator.findByAnd(eq(GenericConfigManager.ENTITY_TABLE_NAME), Matchers.<Map<String, String>>any())).thenReturn(ImmutableList.of(gv));
    }
}
