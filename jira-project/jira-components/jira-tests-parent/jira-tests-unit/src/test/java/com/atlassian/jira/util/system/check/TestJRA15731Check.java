package com.atlassian.jira.util.system.check;

import com.atlassian.jira.ofbiz.OfBizConnectionFactory;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.SQLException;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

/**
 * Test case for {@link JRA15731Check}.
 *
 * @since v4.4
 */
public class TestJRA15731Check {
    @Rule
    public MockitoRule mocks = MockitoJUnit.rule();

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Mock
    private OfBizConnectionFactory mockFactory;

    @Mock
    private Connection mockConnection;

    @Mock
    private DatabaseMetaData mockMetaData;

    @Before
    public void initMocks() throws Exception {
        when(mockFactory.getConnection()).thenReturn(mockConnection);
    }

    @Test
    public void shouldDetermineMySql() throws Exception {
        when(mockConnection.getMetaData()).thenReturn(mockMetaData);
        when(mockMetaData.getDatabaseProductName()).thenReturn("MySQL");
        JRA15731Check tested = new JRA15731Check(mockFactory);

        assertTrue(tested.isMySQL());

        Mockito.verify(mockConnection).close();
    }


    @Test
    public void shouldDetermineNotMySqlIfConnectionThrowsSqlException() throws Exception {
        when(mockConnection.getMetaData()).thenThrow(SQLException.class);
        JRA15731Check tested = new JRA15731Check(mockFactory);

        assertFalse(tested.isMySQL());

        Mockito.verify(mockConnection).close();
    }

    @Test
    public void shouldDetermineNotMySqlIfMetaDataThrowsSqlException() throws Exception {
        when(mockConnection.getMetaData()).thenReturn(mockMetaData);
        when(mockMetaData.getDatabaseProductName()).thenThrow(new SQLException());
        JRA15731Check tested = new JRA15731Check(mockFactory);

        assertFalse(tested.isMySQL());

        Mockito.verify(mockConnection).close();
    }


    @Test
    public void shouldDetermineNotMySqlIfConnectionThrowsRuntimeException() throws Exception {
        when(mockConnection.getMetaData()).thenThrow(RuntimeException.class);

        expectedException.expect(RuntimeException.class);
        try {
            new JRA15731Check(mockFactory).getWarningMessage();
        } catch (RuntimeException x) {
            Mockito.verify(mockConnection).close();
            throw x;
        }
    }
}
