package com.atlassian.jira.upgrade.tasks;

import com.atlassian.jira.issue.fields.screen.FieldScreen;
import com.atlassian.jira.issue.fields.screen.FieldScreenLayoutItem;
import com.atlassian.jira.issue.fields.screen.FieldScreenScheme;
import com.atlassian.jira.issue.fields.screen.FieldScreenSchemeManager;
import com.atlassian.jira.issue.fields.screen.FieldScreenTab;
import com.atlassian.jira.junit.rules.InitMockitoMocks;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;

import static com.google.common.collect.ImmutableList.of;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestUpgradeTask_Build551 {
    public static final long DEFAULT_SCHEME_ID = 1L;
    public static final String LABELS_FIELD_ID = "labels";

    @Rule
    public InitMockitoMocks initMocks = new InitMockitoMocks(this);

    @Mock
    private FieldScreenTab someDefaultTab;
    @Mock
    private FieldScreenSchemeManager mockFieldScreenSchemeManager;

    private UpgradeTask_Build551 upgradeTask;

    @Before
    public void setUp() {
        upgradeTask = new UpgradeTask_Build551(mockFieldScreenSchemeManager);
    }

    @Test
    public void shouldNotBreakWhenThereAreNoScreenSchemes() throws Exception {
        upgradeTask.doUpgrade(false);
    }

    @Test
    public void shouldNotAddWhenThereIsNoDefaultScreenScheme() throws Exception {
        FieldScreenScheme scheme1 = givenScreenSchemeWithDefaultScreen(4L, givenFieldScreenWithDefaultTab(someDefaultTab));
        FieldScreenScheme scheme2 = givenScreenSchemeWithDefaultScreen(10L, givenFieldScreenWithDefaultTab(someDefaultTab));
        when(mockFieldScreenSchemeManager.getFieldScreenSchemes()).thenReturn(of(scheme1, scheme2));

        upgradeTask.doUpgrade(false);

        verify(someDefaultTab, never()).addFieldScreenLayoutItem(LABELS_FIELD_ID);
    }

    @Test
    public void shouldNotAddWhenThereIsNoDefaultScreen() throws Exception {
        FieldScreenScheme scheme1 = givenScreenSchemeWithDefaultScreen(4L, givenFieldScreenWithDefaultTab(someDefaultTab));
        FieldScreenScheme defaultButWithoutScreen = givenScreenSchemeWithDefaultScreen(DEFAULT_SCHEME_ID, null);
        when(mockFieldScreenSchemeManager.getFieldScreenSchemes()).thenReturn(of(scheme1, defaultButWithoutScreen));

        upgradeTask.doUpgrade(false);

        verify(someDefaultTab, never()).addFieldScreenLayoutItem(LABELS_FIELD_ID);
    }

    @Test
    public void shouldNotAddWhenThereAreLabelsAlreadyOnScreen() throws Exception {
        FieldScreenLayoutItem item = mock(FieldScreenLayoutItem.class);
        when(someDefaultTab.getFieldScreenLayoutItem(LABELS_FIELD_ID)).thenReturn(item);

        FieldScreenScheme schemeWithLabelsOnScreen = givenScreenSchemeWithDefaultScreen(DEFAULT_SCHEME_ID, givenFieldScreenWithDefaultTab(someDefaultTab));

        when(mockFieldScreenSchemeManager.getFieldScreenSchemes()).thenReturn(of(schemeWithLabelsOnScreen));

        verify(someDefaultTab, never()).addFieldScreenLayoutItem(LABELS_FIELD_ID);
    }


    @Test
    public void shouldAddLabelsToTheFirstTabWhenIsNotConfigured() throws Exception {
        final FieldScreenScheme mockDefaultScheme = givenScreenSchemeWithDefaultScreen(DEFAULT_SCHEME_ID, givenFieldScreenWithDefaultTab(someDefaultTab));

        when(mockFieldScreenSchemeManager.getFieldScreenSchemes()).thenReturn(of(mockDefaultScheme));

        upgradeTask.doUpgrade(false);

        verify(someDefaultTab).addFieldScreenLayoutItem(LABELS_FIELD_ID);
    }

    @Test
    public void testMetaData() {
        assertEquals(551, upgradeTask.getBuildNumber());
        assertEquals("Adds the new Labels system field to the Default Screen of the default field configuration.", upgradeTask.getShortDescription());
    }

    private FieldScreenScheme givenScreenSchemeWithDefaultScreen(Long id, FieldScreen defaultScreen) {
        FieldScreenScheme scheme = mock(FieldScreenScheme.class);
        when(scheme.getId()).thenReturn(id);
        if (defaultScreen != null) {
            when(scheme.getFieldScreen(null)).thenReturn(defaultScreen);
        }
        return scheme;
    }

    private FieldScreen givenFieldScreenWithDefaultTab(FieldScreenTab defaultTab) {
        FieldScreen fieldScreen = mock(FieldScreen.class);
        when(fieldScreen.getTabs()).thenReturn(of(defaultTab));
        when(fieldScreen.getTab(0)).thenReturn(defaultTab);
        return fieldScreen;
    }

}
