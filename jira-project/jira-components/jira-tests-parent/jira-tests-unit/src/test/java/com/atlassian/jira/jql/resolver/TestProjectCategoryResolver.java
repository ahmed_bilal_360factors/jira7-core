package com.atlassian.jira.jql.resolver;

import com.atlassian.jira.jql.operand.QueryLiteral;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectCategory;
import com.atlassian.jira.project.ProjectCategoryImpl;
import com.atlassian.jira.project.ProjectManager;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Collection;
import java.util.Collections;

import static com.atlassian.jira.jql.operand.SimpleLiteralFactory.createLiteral;
import static com.google.common.collect.ImmutableList.of;
import static java.util.Collections.singleton;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 * @since v4.0
 */
public class TestProjectCategoryResolver {
    @Rule
    public MockitoRule mocks = MockitoJUnit.rule();

    @Mock
    private ProjectManager projectManager;

    @Test
    public void testGetProjectCategoryUsingStringHappyPath() throws Exception {
        final ProjectCategory expectedCategory = createMockProjectCategory(1L, "TheName");

        Mockito.when(projectManager.getProjectCategoryObjectByNameIgnoreCase("TheName")).thenReturn(expectedCategory);


        final ProjectCategoryResolver resolver = new ProjectCategoryResolver(projectManager);

        final ProjectCategory category = resolver.getProjectCategory(createLiteral("TheName"));
        assertEquals(expectedCategory, category);
    }

    @Test
    public void testGetProjectCategoryUsingStringNameDoesntExistIsntId() throws Exception {
        Mockito.when(projectManager.getProjectCategoryObjectByNameIgnoreCase("TheName")).thenReturn(null);


        final ProjectCategoryResolver resolver = new ProjectCategoryResolver(projectManager);

        final ProjectCategory category = resolver.getProjectCategory(createLiteral("TheName"));
        assertNull(category);
    }

    @Test
    public void testGetProjectCategoryUsingStringNameDoesntExistIsIdAndExists() throws Exception {
        final ProjectCategory expectedCategory = createMockProjectCategory(1L, "TheName");

        Mockito.when(projectManager.getProjectCategoryObjectByNameIgnoreCase("1")).thenReturn(null);
        Mockito.when(projectManager.getProjectCategoryObject(1L)).thenReturn(expectedCategory);


        final ProjectCategoryResolver resolver = new ProjectCategoryResolver(projectManager);

        final ProjectCategory category = resolver.getProjectCategory(createLiteral("1"));
        assertEquals(expectedCategory, category);
    }

    @Test
    public void testGetProjectCategoryUsingStringNameDoesntExistIsIdDoesntExist() throws Exception {
        Mockito.when(projectManager.getProjectCategoryObjectByNameIgnoreCase("1")).thenReturn(null);
        Mockito.when(projectManager.getProjectCategoryObject(1L)).thenReturn(null);

        final ProjectCategoryResolver resolver = new ProjectCategoryResolver(projectManager);

        final ProjectCategory category = resolver.getProjectCategory(createLiteral("1"));
        assertNull(category);
    }

    @Test
    public void testGetProjectCategoryUsingEmptyLiteral() throws Exception {
        final ProjectCategoryResolver resolver = new ProjectCategoryResolver(projectManager);

        final ProjectCategory category = resolver.getProjectCategory(new QueryLiteral());
        assertNull(category);
    }

    @Test
    public void testGetProjectCategoryUsingLongHappyPath() throws Exception {
        final ProjectCategory expectedCategory = createMockProjectCategory(1L, "TheName");

        Mockito.when(projectManager.getProjectCategoryObject(1L)).thenReturn(expectedCategory);


        final ProjectCategoryResolver resolver = new ProjectCategoryResolver(projectManager);

        final ProjectCategory category = resolver.getProjectCategory(createLiteral(1L));
        assertEquals(expectedCategory, category);
    }

    @Test
    public void testGetProjectCategoryUsingLongIdDoesntExistNameDoes() throws Exception {
        final ProjectCategory expectedCategory = createMockProjectCategory(1L, "1");

        Mockito.when(projectManager.getProjectCategoryObject(1L)).thenReturn(null);
        Mockito.when(projectManager.getProjectCategoryObjectByNameIgnoreCase("1")).thenReturn(expectedCategory);


        final ProjectCategoryResolver resolver = new ProjectCategoryResolver(projectManager);

        final ProjectCategory category = resolver.getProjectCategory(createLiteral(1L));
        assertEquals(expectedCategory, category);
    }

    @Test
    public void testGetProjectCategoryUsingLongIdDoesntExistNameDoesnt() throws Exception {
        Mockito.when(projectManager.getProjectCategoryObject(1L)).thenReturn(null);
        Mockito.when(projectManager.getProjectCategoryObjectByNameIgnoreCase("1")).thenReturn(null);

        final ProjectCategoryResolver resolver = new ProjectCategoryResolver(projectManager);

        final ProjectCategory category = resolver.getProjectCategory(createLiteral(1L));
        assertNull(category);
    }

    @Test
    public void testGetProjectsForCategoryEmptyLiteral() throws Exception {
        final Collection<Project> expectedProjects = of(new MockProject(555L));
        Mockito.when(projectManager.getProjectObjectsWithNoCategory()).thenReturn(expectedProjects);


        final ProjectCategoryResolver resolver = new ProjectCategoryResolver(projectManager);

        final Collection<Project> projects = resolver.getProjectsForCategory(new QueryLiteral());
        assertEquals(expectedProjects, projects);
    }

    @Test
    public void testGetProjectsForCategoryLiteralResolves() throws Exception {
        final QueryLiteral inputLiteral = createLiteral(2L);
        final Collection<Project> expectedProjects = singleton(new MockProject(555L));
        final ProjectCategory expectedCategory = createMockProjectCategory(2L, "Cat");

        Mockito.when(projectManager.getProjectsFromProjectCategory(expectedCategory)).thenReturn(expectedProjects);


        final ProjectCategoryResolver resolver = new ProjectCategoryResolver(projectManager) {
            @Override
            public ProjectCategory getProjectCategory(final QueryLiteral literal) {
                assertEquals(inputLiteral, literal);
                return expectedCategory;
            }
        };

        final Collection<Project> projects = resolver.getProjectsForCategory(inputLiteral);
        assertEquals(expectedProjects, projects);
    }

    @Test
    public void testGetProjectsForCategoryLiteralDoesntResolve() throws Exception {
        final QueryLiteral inputLiteral = createLiteral(2L);
        final Collection<Project> expectedProjects = Collections.emptySet();


        final ProjectCategoryResolver resolver = new ProjectCategoryResolver(projectManager) {
            @Override
            public ProjectCategory getProjectCategory(final QueryLiteral literal) {
                assertEquals(inputLiteral, literal);
                return null;
            }
        };

        final Collection<Project> projects = resolver.getProjectsForCategory(inputLiteral);
        assertEquals(expectedProjects, projects);
    }

    private ProjectCategory createMockProjectCategory(final Long id, final String name) {
        return new ProjectCategoryImpl(id, name, null);
    }
}
