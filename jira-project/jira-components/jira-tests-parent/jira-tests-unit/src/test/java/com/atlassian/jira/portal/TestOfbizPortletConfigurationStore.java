package com.atlassian.jira.portal;

import com.atlassian.fugue.Option;
import com.atlassian.gadgets.dashboard.Color;
import com.atlassian.jira.entity.property.EntityProperty;
import com.atlassian.jira.entity.property.EntityPropertyImpl;
import com.atlassian.jira.entity.property.EntityPropertyType;
import com.atlassian.jira.entity.property.JsonEntityPropertyManager;
import com.atlassian.jira.exception.DataAccessException;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoContainer;
import com.atlassian.jira.mock.ofbiz.MockGenericValue;
import com.atlassian.jira.mock.ofbiz.MockOfBizDelegator;
import com.atlassian.jira.portal.OfbizPortletConfigurationStore.Columns;
import com.atlassian.jira.util.Consumer;
import com.atlassian.jira.util.collect.EnclosedIterable;
import com.atlassian.jira.util.collect.MapBuilder;
import com.atlassian.plugin.ModuleCompleteKey;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.ofbiz.core.entity.GenericValue;

import javax.annotation.Nonnull;
import java.net.URI;
import java.util.List;
import java.util.Map;

import static com.atlassian.jira.portal.OfbizPortletConfigurationStore.TABLE;
import static com.atlassian.jira.portal.OfbizPortletConfigurationStore.USER_PREFERENCES_TABLE;
import static com.google.common.collect.Lists.newArrayList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

public class TestOfbizPortletConfigurationStore {

    public static final long PORTLET_ID = 20010L;
    private static final Long PAGE1_ID = 10030L;
    private static final Long PAGE2_ID = 10040L;
    private static final Map<String, String> USER_PREFS = ImmutableMap.of("pref1", "value1", "pref2", "value2");
    private static final Map<String, String> EMPTY_PREFS = ImmutableMap.of();
    private static final Option<URI> XML_URI = Option.some(URI.create("http://www.google.com"));
    private static final Option<ModuleCompleteKey> MODULE_KEY = Option.some(new ModuleCompleteKey("com.atlassian.gadgets.test:complete.key"));

    @Rule
    public MockitoContainer mockitoContainer = new MockitoContainer(this);

    @AvailableInContainer(instantiateMe = true)
    private MockOfBizDelegator delegator;

    private final JsonEntityPropertyManager jsonEntityPropertyManager = mock(JsonEntityPropertyManager.class);

    private OfbizPortletConfigurationStore portletConfigurationStore;

    @Before
    public void setUp() throws Exception {
        portletConfigurationStore = new OfbizPortletConfigurationStore(delegator, jsonEntityPropertyManager) {
            @Override
            protected void removePropertySet(final GenericValue gv) {
                // this method normally uses some public static method which makes deleting close to impossible to test
                // we don't care what happens here
            }
        };
    }

    @Test
    public void testAddGadget() {
        portletConfigurationStore.addGadget(PAGE1_ID, 10025L, 3, 4, XML_URI.get(), Color.color3, USER_PREFS);

        final PortletConfiguration portletConfiguration = portletConfigurationStore.getByPortletId(10025L);
        assertThat(portletConfiguration, has(equalTo(10025L), PAGE1_ID, 3, 4, XML_URI, Color.color3, USER_PREFS, Option.<ModuleCompleteKey>none()));
    }

    @Test
    public void testAddGadgetWithoutId() {
        final PortletConfiguration newConfiguration = portletConfigurationStore.addGadget(PAGE1_ID, null, 3, 4, XML_URI.get(), Color.color5, USER_PREFS);
        final PortletConfiguration configurationFromStore = portletConfigurationStore.getByPortletId(newConfiguration.getId());
        assertThat(configurationFromStore, has(notNullValue(), PAGE1_ID, 3, 4, XML_URI, Color.color5, USER_PREFS, Option.<ModuleCompleteKey>none()));
    }

    @Test
    public void testAddDashboardItem() {
        final PortletConfiguration portletConfiguration = portletConfigurationStore.addDashboardItem(
                PAGE1_ID, 10025L, 3, 4, XML_URI, Color.color3, EMPTY_PREFS, MODULE_KEY);
        assertThat(portletConfiguration, has(equalTo(10025L), PAGE1_ID, 3, 4, XML_URI, Color.color3, EMPTY_PREFS, MODULE_KEY));
    }

    @Test
    public void testAddDashboardItemWithoutId() {
        final PortletConfiguration portletConfiguration = portletConfigurationStore
                .addDashboardItem(PAGE1_ID, null, 3, 4, XML_URI, Color.color3, EMPTY_PREFS, MODULE_KEY);
        assertThat(portletConfiguration, has(notNullValue(), PAGE1_ID, 3, 4, XML_URI, Color.color3, EMPTY_PREFS, MODULE_KEY));
    }

    @Test
    public void testStoreOpenSocialDashboardItem() {
        final PortletConfiguration portletConfiguration = portletConfigurationStore
                .addDashboardItem(PAGE1_ID, 10025L, 3, 4, XML_URI, Color.color3, USER_PREFS, Option.<ModuleCompleteKey>none());

        final Map<String, String> newPrefs = ImmutableMap.of("pref3", "value3", "pref4", "value4");
        portletConfiguration.setColor(Color.color1);
        portletConfiguration.setColumn(1);
        portletConfiguration.setRow(2);
        portletConfiguration.setDashboardPageId(PAGE2_ID);
        portletConfiguration.setUserPrefs(newPrefs);
        portletConfigurationStore.store(portletConfiguration);

        final PortletConfiguration updatedPortlet = portletConfigurationStore.getByPortletId(10025L);
        assertThat(updatedPortlet, has(notNullValue(), PAGE2_ID, 1, 2, XML_URI, Color.color1, newPrefs, Option.<ModuleCompleteKey>none()));
    }

    @Test
    public void testStoreLocalDashboardItem() {
        final PortletConfiguration portletConfiguration = portletConfigurationStore
                .addDashboardItem(PAGE1_ID, PORTLET_ID, 3, 4, Option.<URI>none(), Color.color3, EMPTY_PREFS, MODULE_KEY);

        final Map<String, String> newPrefs = ImmutableMap.of("pref3", "value3", "pref4", "value4");
        assumePropertiesAreStored(PORTLET_ID, newPrefs);
        portletConfiguration.setColor(Color.color1);
        portletConfiguration.setColumn(1);
        portletConfiguration.setRow(2);
        portletConfiguration.setDashboardPageId(PAGE2_ID);
        portletConfiguration.setUserPrefs(newPrefs);
        portletConfigurationStore.store(portletConfiguration);

        final PortletConfiguration updatedPortlet = portletConfigurationStore.getByPortletId(PORTLET_ID);
        assertThat(updatedPortlet, has(is(PORTLET_ID), PAGE2_ID, 1, 2, Option.<URI>none(), Color.color1, newPrefs, MODULE_KEY));
        verify(jsonEntityPropertyManager).put(EntityPropertyType.DASHBOARD_ITEM_PROPERTY.getDbEntityName(), PORTLET_ID, "pref3", "\"value3\"");
        verify(jsonEntityPropertyManager).put(EntityPropertyType.DASHBOARD_ITEM_PROPERTY.getDbEntityName(), PORTLET_ID, "pref4", "\"value4\"");
        verify(jsonEntityPropertyManager, atLeastOnce()).deleteByEntity(EntityPropertyType.DASHBOARD_ITEM_PROPERTY.getDbEntityName(), PORTLET_ID);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testStoreItemWhichHasNotBeenAdded() {
        PortletConfiguration portletConfig = new PortletConfigurationImpl(10026L, PAGE2_ID, 1, 2, Option.<URI>none(),
                Color.color7, EMPTY_PREFS, Option.<ModuleCompleteKey>none());
        portletConfigurationStore.store(portletConfig);
    }

    @Test
    public void testGetAllPortletConfigurations() {
        portletConfigurationStore.addDashboardItem(PAGE1_ID, PORTLET_ID, 3, 4, Option.<URI>none(), Color.color5, EMPTY_PREFS, MODULE_KEY);
        portletConfigurationStore.addDashboardItem(PAGE2_ID, null, 1, 1, XML_URI, Color.color2, USER_PREFS, Option.<ModuleCompleteKey>none());

        final Iterable<PortletConfiguration> portlets = toIterable((portletConfigurationStore.getAllPortletConfigurations()));

        assertThat(portlets, Matchers.<PortletConfiguration>contains(
                has(equalTo(PORTLET_ID), PAGE1_ID, 3, 4, Option.<URI>none(), Color.color5, EMPTY_PREFS, MODULE_KEY),
                has(notNullValue(), PAGE2_ID, 1, 1, XML_URI, Color.color2, USER_PREFS, Option.<ModuleCompleteKey>none())
        ));
    }

    @Test
    public void testLoadingDashboardItemPropertiesForLocalDashboardItemWithReplacementUri() {
        portletConfigurationStore.addDashboardItem(PAGE2_ID, PORTLET_ID, 1, 1, XML_URI, Color.color2, USER_PREFS, MODULE_KEY);

        final Map<String, String> properties = ImmutableMap.of("prop1", "1", "prop2", "2", "porp3", "3");
        assumePropertiesAreStored(PORTLET_ID, properties);

        PortletConfiguration dashboardItem = portletConfigurationStore.getByPortletId(PORTLET_ID);

        Map<String, String> loadedPrefs = dashboardItem.getUserPrefs();

        assertThat(loadedPrefs, is(USER_PREFS));
    }

    @Test
    public void testLoadingDashboardItemPropertiesForLocalDashboardItemWithoutReplacementUri() {
        portletConfigurationStore.addDashboardItem(PAGE2_ID, PORTLET_ID, 1, 1, Option.<URI>none(), Color.color2, USER_PREFS, MODULE_KEY);

        final Map<String, String> properties = ImmutableMap.of("prop1", "1", "prop2", "2", "porp3", "3");
        assumePropertiesAreStored(PORTLET_ID, properties);

        PortletConfiguration dashboardItem = portletConfigurationStore.getByPortletId(PORTLET_ID);
        Map<String, String> loadedPrefs = dashboardItem.getUserPrefs();

        assertThat(loadedPrefs, is(properties));
    }

    private void assumePropertiesAreStored(Long itemId, Map<String, String> properties) {
        when(jsonEntityPropertyManager.findKeys(EntityPropertyType.DASHBOARD_ITEM_PROPERTY.getDbEntityName(), itemId)).thenReturn(newArrayList(properties.keySet()));
        for (Map.Entry<String, String> propertyEntry : properties.entrySet()) {
            EntityProperty property = EntityPropertyImpl.forCreate(
                    EntityPropertyType.DASHBOARD_ITEM_PROPERTY.getDbEntityName(),
                    itemId,
                    propertyEntry.getKey(),
                    "\"" + propertyEntry.getValue() + "\"");

            when(jsonEntityPropertyManager.get(EntityPropertyType.DASHBOARD_ITEM_PROPERTY.getDbEntityName(), itemId, propertyEntry.getKey())).thenReturn(property);
        }
    }

    @Test
    public void testUpdateGadgetColor() {
        portletConfigurationStore.addDashboardItem(PAGE1_ID, PORTLET_ID, 3, 4, XML_URI, Color.color5, EMPTY_PREFS, MODULE_KEY);
        portletConfigurationStore.updateGadgetColor(PORTLET_ID, Color.color3);

        final PortletConfiguration updatedPc = portletConfigurationStore.getByPortletId(PORTLET_ID);
        assertThat(Color.color3, is(updatedPc.getColor()));
    }

    @Test(expected = DataAccessException.class)
    public void testUpdateGadgetColorWhenGadgetDoesNotExist() {
        portletConfigurationStore.updateGadgetColor(-999L, Color.color2);
    }

    @Test
    public void testUpdateGadgetPosition() {
        portletConfigurationStore.addDashboardItem(PAGE1_ID, PORTLET_ID, 3, 4, XML_URI, Color.color5, EMPTY_PREFS, MODULE_KEY);
        portletConfigurationStore.updateGadgetPosition(PORTLET_ID, 2, 5, PAGE2_ID);

        final PortletConfiguration movedPc = portletConfigurationStore.getByPortletId(PORTLET_ID);
        assertThat(movedPc.getDashboardPageId(), is(PAGE2_ID));
        assertThat(movedPc.getRow(), is(2));
        assertThat(movedPc.getColumn(), is(5));

        final List<PortletConfiguration> page1Gadgets = portletConfigurationStore.getByPortalPage(PAGE1_ID);
        assertThat(page1Gadgets, Matchers.<PortletConfiguration>empty());

        final List<PortletConfiguration> page2Gadgets = portletConfigurationStore.getByPortalPage(PAGE2_ID);
        assertThat(page2Gadgets, contains(
                has(equalTo(PORTLET_ID), PAGE2_ID, 5, 2, XML_URI, Color.color5, EMPTY_PREFS, MODULE_KEY)
        ));
    }

    @Test(expected = DataAccessException.class)
    public void testUpdateGadgetPositionWhenGadgetDoesNotExist() {
        portletConfigurationStore.updateGadgetPosition(-999L, 0, 0, PAGE1_ID);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testUpdateUserPrefsForGadgetWhichDoesNotExist() {
        portletConfigurationStore.updateUserPrefs(666l, ImmutableMap.of("key", "value"));
    }

    @Test
    public void testUpdateUserPrefsForOpenSocialGadget() {
        delegator.setGenericValues(ImmutableList.of(
                new MockGenericValue(TABLE, ImmutableMap.<String, Object>of(Columns.ID, PORTLET_ID, Columns.GADGET_XML, XML_URI.get())),
                new MockGenericValue(USER_PREFERENCES_TABLE,
                        ImmutableMap.<String, Object>of(OfbizPortletConfigurationStore.UserPreferenceColumns.PORTLETID, PORTLET_ID)
                )
        ));

        portletConfigurationStore.updateUserPrefs(PORTLET_ID, ImmutableMap.of("key", "value"));
        final ImmutableMap<String, Long> prefsFields = ImmutableMap.of(OfbizPortletConfigurationStore.UserPreferenceColumns.PORTLETID, PORTLET_ID);
        final List<GenericValue> userPrefs = delegator.findByAnd(USER_PREFERENCES_TABLE, prefsFields);
        assertThat(userPrefs, hasSize(1));
        final GenericValue userPref = userPrefs.get(0);
        assertThat(userPref.get(OfbizPortletConfigurationStore.UserPreferenceColumns.KEY), is((Object) "key"));
        assertThat(userPref.get(OfbizPortletConfigurationStore.UserPreferenceColumns.VALUE), is((Object) "value"));
    }

    @Test
    public void testUpdateUserPrefsForLocalDashboardItem() {
        delegator.setGenericValues(ImmutableList.of(
                new MockGenericValue(TABLE, ImmutableMap.<String, Object>of(Columns.ID, PORTLET_ID, Columns.MODULE_KEY, MODULE_KEY.get()))
        ));

        portletConfigurationStore.updateUserPrefs(PORTLET_ID, ImmutableMap.of("key", "value"));
        verify(jsonEntityPropertyManager).put(EntityPropertyType.DASHBOARD_ITEM_PROPERTY.getDbEntityName(), PORTLET_ID, "key", "\"value\"");
        verify(jsonEntityPropertyManager).deleteByEntity(EntityPropertyType.DASHBOARD_ITEM_PROPERTY.getDbEntityName(), PORTLET_ID);
        verifyNoMoreInteractions(jsonEntityPropertyManager);
    }

    //test special case for Oracle (JRA-18125) where empty strings come back as null.
    @Test
    public void testGetUserPrefs() {
        List<GenericValue> results = ImmutableList.<GenericValue>of(
                new MockGenericValue("GadgetUserPreference", ImmutableMap.of("portletconfiguration", 10000L, "userprefkey", "key1", "userprefvalue", "val1")),
                new MockGenericValue("GadgetUserPreference", MapBuilder.<String, Object>newBuilder().add("portletconfiguration", 10000L).add("userprefkey", "key2").add("userprefvalue", null).toMap())
        );

        delegator.setGenericValues(results);

        final Map<String, String> prefs = portletConfigurationStore.getUserPreferences(10000L, Option.<URI>none(), Option.<ModuleCompleteKey>none());

        assertEquals("val1", prefs.get("key1"));
        assertNotNull(prefs.get("key2"));
        assertEquals("", prefs.get("key2"));
    }

    @Test
    public void testDeletingOpenSocialGadget() throws Exception {
        delegator.setGenericValues(ImmutableList.of(
                new MockGenericValue(TABLE, PORTLET_ID),
                new MockGenericValue(USER_PREFERENCES_TABLE,
                        ImmutableMap.<String, Object>of(OfbizPortletConfigurationStore.UserPreferenceColumns.PORTLETID, PORTLET_ID)
                )
        ));

        portletConfigurationStore.delete(new PortletConfigurationImpl(PORTLET_ID, 2L, 0, 0, XML_URI, Color.color1, EMPTY_PREFS, Option.<ModuleCompleteKey>none()));
        assertThat(delegator.getCount(TABLE), is(0l));
        assertThat(delegator.getCount(USER_PREFERENCES_TABLE), is(0l));
    }

    @Test
    public void testDeletingOpenSocialDashboardItem() throws Exception {
        delegator.setGenericValues(ImmutableList.of(
                new MockGenericValue(TABLE, PORTLET_ID),
                new MockGenericValue(USER_PREFERENCES_TABLE,
                        ImmutableMap.<String, Object>of(OfbizPortletConfigurationStore.UserPreferenceColumns.PORTLETID, PORTLET_ID)
                )
        ));

        portletConfigurationStore.delete(new PortletConfigurationImpl(PORTLET_ID, 2L, 0, 0, XML_URI, Color.color1, EMPTY_PREFS, MODULE_KEY));
        assertThat(delegator.getCount(TABLE), is(0l));
        assertThat(delegator.getCount(USER_PREFERENCES_TABLE), is(0l));
    }

    @Test
    public void dashboardItemPropertiesAreRemovedWhenDeletingLocalDashboardItem() throws Exception {
        delegator.setGenericValues(ImmutableList.of(
                new MockGenericValue(TABLE, PORTLET_ID)
        ));

        portletConfigurationStore.delete(new PortletConfigurationImpl(PORTLET_ID, 2L, 0, 0, Option.<URI>none(), Color.color1, EMPTY_PREFS, MODULE_KEY));

        verify(jsonEntityPropertyManager).deleteByEntity(EntityPropertyType.DASHBOARD_ITEM_PROPERTY.getDbEntityName(), PORTLET_ID);
        assertThat(delegator.getCount(TABLE), is(0l));
    }

    private Matcher<PortletConfiguration> has(final Matcher<? super Long> idMatcher, final Long dashboardPageId,
                                              final Integer column, final Integer row, final Option<URI> openSocialSpecUri, final Color color,
                                              final Map<String, String> userPrefs, final Option<ModuleCompleteKey> completeModuleKey) {
        return Matchers.<PortletConfiguration>allOf(
                hasProperty("id", idMatcher),
                hasProperty("dashboardPageId", equalTo(dashboardPageId)),
                hasProperty("column", equalTo(column)),
                hasProperty("row", equalTo(row)),
                hasProperty("color", equalTo(color)),
                hasProperty("userPrefs", equalTo(userPrefs)),
                hasProperty("openSocialSpecUri", equalTo(openSocialSpecUri)),
                hasProperty("completeModuleKey", equalTo(completeModuleKey))
        );
    }

    private Iterable<PortletConfiguration> toIterable(final EnclosedIterable<PortletConfiguration> enclosedIterable) {
        final ImmutableList.Builder<PortletConfiguration> builder = ImmutableList.builder();
        enclosedIterable.foreach(new Consumer<PortletConfiguration>() {
            @Override
            public void consume(@Nonnull final PortletConfiguration element) {
                builder.add(element);
            }
        });
        return builder.build();
    }
}
