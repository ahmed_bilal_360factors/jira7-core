package com.atlassian.jira.upgrade.tasks.role;

import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.jira.test.util.lic.License;
import com.atlassian.jira.test.util.lic.core.CoreLicenses;
import com.atlassian.jira.test.util.lic.software.SoftwareLicenses;
import com.google.common.collect.ImmutableList;
import org.junit.Before;
import org.junit.Test;

import static com.atlassian.jira.application.ApplicationKeys.CORE;
import static com.atlassian.jira.application.ApplicationKeys.SOFTWARE;
import static com.atlassian.jira.upgrade.tasks.role.TestUtils.emptyState;
import static com.atlassian.jira.upgrade.tasks.role.TestUtils.newGroup;
import static com.atlassian.jira.upgrade.tasks.role.TestUtils.toLicense;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.is;

public class Move6XUsePermissionOverToCoreOrSoftwareApplicationsTest {
    private Group otherGroup = newGroup("otherGroup");
    private UseBasedMigration userBasedMigration = new MockUseBasedMigration(otherGroup);
    private Move6xUsePermissionOverToCoreAndSoftwareApplications task;

    @Before
    public void setUp() {
        task = new Move6xUsePermissionOverToCoreAndSoftwareApplications(userBasedMigration);
    }

    @Test
    public void migrateMovesUsersIntoCoreAndSoftwareRoleIfThereIsNoLicense() {
        //given
        final ApplicationRole core = ApplicationRole.forKey(CORE).addGroup(otherGroup);
        final ApplicationRole software = ApplicationRole.forKey(SOFTWARE).addGroup(otherGroup);
        ApplicationRoles roles = new ApplicationRoles(ImmutableList.of(software, core));
        final MigrationState state = TestUtils.emptyState();

        //when
        final MigrationState migrate = task.migrate(state, false);

        //then
        assertThat(migrate, new MigrationStateMatcher()
                .roles(roles)
                .anyLog());
    }

    @Test
    public void migrateMovesUsersIntoCoreAndSoftwareRoleIfThereIsSoftwareLicense() {
        //given
        final MigrationState state = stateWithLicenses(SoftwareLicenses.LICENSE_SOFTWARE);
        final ApplicationRole core = ApplicationRole.forKey(CORE).addGroup(otherGroup);
        final ApplicationRole software = ApplicationRole.forKey(SOFTWARE).addGroup(otherGroup);
        ApplicationRoles roles = new ApplicationRoles(ImmutableList.of(software, core));

        //when
        final MigrationState migrate = task.migrate(state, false);

        //then
        assertThat(migrate, new MigrationStateMatcher()
                .licenses(SoftwareLicenses.LICENSE_SOFTWARE)
                .roles(roles)
                .anyLog());
    }

    @Test
    public void migrateMovesUsersIntoCoreAndSoftwareRoleIfThereIsCoreLicense() {
        //given
        final MigrationState state = stateWithLicenses(CoreLicenses.LICENSE_CORE);
        final ApplicationRole core = ApplicationRole.forKey(CORE).addGroup(otherGroup);
        final ApplicationRole software = ApplicationRole.forKey(SOFTWARE).addGroup(otherGroup);
        ApplicationRoles roles = new ApplicationRoles(ImmutableList.of(software, core));

        //when
        final MigrationState migrate = task.migrate(state, false);

        //then

        assertThat(migrate, new MigrationStateMatcher()
                .licenses(CoreLicenses.LICENSE_CORE)
                .roles(roles)
                .anyLog());
    }

    @Test
    public void migrateMovesUsersIntoCoreAndSoftwareRoleIfThereIsSoftwareAndCoreLicense() {
        //given
        final MigrationState state = stateWithLicenses(SoftwareLicenses.LICENSE_SOFTWARE, CoreLicenses.LICENSE_CORE);

        final ApplicationRole core = ApplicationRole.forKey(CORE).addGroup(otherGroup);
        final ApplicationRole software = ApplicationRole.forKey(SOFTWARE).addGroup(otherGroup);
        ApplicationRoles roles = new ApplicationRoles(ImmutableList.of(software, core));

        //when
        final MigrationState migrate = task.migrate(state, false);

        //then
        assertThat(migrate, new MigrationStateMatcher()
                .licenses(SoftwareLicenses.LICENSE_SOFTWARE, CoreLicenses.LICENSE_CORE)
                .roles(roles)
                .anyLog());

    }


    @Test
    public void doesNotAddAfterSaveTask() {
        final MigrationState result = task.migrate(emptyState(), false);

        assertThat(result.afterSaveTasks(), is(empty()));
    }

    private MigrationState stateWithLicenses(com.atlassian.jira.test.util.lic.License... licenses) {
        MigrationState state = TestUtils.emptyState();
        for (License license : licenses) {
            state = state.changeLicenses(l -> l.addLicense(toLicense(license)));
        }
        return state;
    }
}