package com.atlassian.jira.upgrade;

import com.atlassian.jira.config.properties.PropertiesManager;
import com.atlassian.jira.junit.rules.MockitoContainer;
import com.opensymphony.module.propertyset.PropertySet;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;

import java.util.Optional;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.internal.verification.VerificationModeFactory.times;

public class TestLegacyUpgradeTaskBuildNumberStorageImpl {
    private final static String PROPERTY_KEY = "test";

    @Rule
    public MockitoContainer mockitoContainer = new MockitoContainer(this);

    @Mock
    private PropertiesManager propertiesManager;

    @Mock
    private PropertySet propertySet;

    private LegacyUpgradeTaskBuildNumberStorageImpl legacyUpgradeTaskBuildNumberStorageImpl;

    @Before
    public void setUp() throws Exception {
        legacyUpgradeTaskBuildNumberStorageImpl = new LegacyUpgradeTaskBuildNumberStorageImpl(propertiesManager);

        when(propertiesManager.getPropertySet()).thenReturn(propertySet);
        when(propertySet.exists(eq(PROPERTY_KEY))).thenReturn(true);
    }

    @Test(expected = NullPointerException.class)
    public void getIntegerPluginSettingThrowsExceptionWhenPropertySetDoesNotExist() {
        when(propertiesManager.getPropertySet()).thenReturn(null);

        legacyUpgradeTaskBuildNumberStorageImpl.getIntegerPluginSetting(PROPERTY_KEY);
    }

    @Test(expected = IllegalArgumentException.class)
    public void getIntegerPluginSettingThrowsExceptionWhenKeyIsNull() {
        legacyUpgradeTaskBuildNumberStorageImpl.getIntegerPluginSetting(null);
    }

    @Test
    public void getIntegerPluginSettingReturnsEmptyWhenPropertyDoesNotExist() {
        when(propertySet.exists(eq(PROPERTY_KEY))).thenReturn(false);

        Optional<Integer> setting = legacyUpgradeTaskBuildNumberStorageImpl.getIntegerPluginSetting(PROPERTY_KEY);
        assertThat(setting.isPresent(), equalTo(false));
    }

    @Test
    public void getIntegerPluginSettingReturnsReturnsParsedValueWhenPropertyIsAString() {
        when(propertySet.getType(anyString())).thenReturn(PropertySet.STRING);
        when(propertySet.getString(anyString())).thenReturn("1");

        Optional<Integer> setting = legacyUpgradeTaskBuildNumberStorageImpl.getIntegerPluginSetting(PROPERTY_KEY);
        assertThat(setting.isPresent(), equalTo(true));
        assertThat(setting.orElse(-1), equalTo(1));
    }

    @Test
    public void getIntegerPluginSettingReturnsReturnsParsedValueWhenPropertyIsText() {
        when(propertySet.getType(anyString())).thenReturn(PropertySet.TEXT);
        when(propertySet.getText(anyString())).thenReturn("1");

        Optional<Integer> setting = legacyUpgradeTaskBuildNumberStorageImpl.getIntegerPluginSetting(PROPERTY_KEY);
        assertThat(setting.isPresent(), equalTo(true));
        assertThat(setting.orElse(-1), equalTo(1));
    }

    @Test
    public void getIntegerPluginSettingReturnsEmptyWhenPropertyIsUnknown() {
        when(propertySet.getType(anyString())).thenReturn(PropertySet.BOOLEAN);

        Optional<Integer> setting = legacyUpgradeTaskBuildNumberStorageImpl.getIntegerPluginSetting(PROPERTY_KEY);
        assertThat(setting.isPresent(), equalTo(false));
    }

    @Test(expected = NumberFormatException.class)
    public void getIntegerPluginSettingThrowsExceptionIfIntegerParsingFails() {
        when(propertySet.getType(anyString())).thenReturn(PropertySet.TEXT);
        when(propertySet.getText(anyString())).thenReturn("hello");

        legacyUpgradeTaskBuildNumberStorageImpl.getIntegerPluginSetting(PROPERTY_KEY);
    }

    @Test(expected = NullPointerException.class)
    public void putIntegerPluginSettingThrowsExceptionWhenPropertySetDoesNotExist() {
        when(propertiesManager.getPropertySet()).thenReturn(null);

        legacyUpgradeTaskBuildNumberStorageImpl.putIntegerPluginSetting(PROPERTY_KEY, 1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void putIntegerPluginSettingThrowsExceptionIfKeyIsNull() {
        legacyUpgradeTaskBuildNumberStorageImpl.putIntegerPluginSetting(null, 1);
    }

    @Test
    public void putIntegerPluginSettingRemovesAnyPreExistingValueForKey() {
        when(propertySet.exists(eq(PROPERTY_KEY))).thenReturn(true);

        legacyUpgradeTaskBuildNumberStorageImpl.putIntegerPluginSetting(PROPERTY_KEY, 1);

        verify(propertySet, times(1)).remove(eq(PROPERTY_KEY));
        verify(propertySet, times(1)).setString(eq(PROPERTY_KEY), eq("1"));
    }

    @Test
    public void putIntegerPluginSettingDoesNotRemoveAnyKeyThatDoesNotPreviouslyExist() {
        when(propertySet.exists(eq(PROPERTY_KEY))).thenReturn(false);

        legacyUpgradeTaskBuildNumberStorageImpl.putIntegerPluginSetting(PROPERTY_KEY, 1);

        verify(propertySet, times(0)).remove(eq(PROPERTY_KEY));
        verify(propertySet, times(1)).setString(eq(PROPERTY_KEY), eq("1"));
    }
}