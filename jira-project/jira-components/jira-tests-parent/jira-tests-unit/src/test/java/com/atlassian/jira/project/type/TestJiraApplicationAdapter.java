package com.atlassian.jira.project.type;

import com.atlassian.application.api.Application;
import com.atlassian.application.api.ApplicationKey;
import com.atlassian.application.api.ApplicationManager;
import com.atlassian.application.api.PlatformApplication;
import com.atlassian.application.host.plugin.PluginApplicationMetaData;
import com.atlassian.application.host.plugin.PluginApplicationMetaDataManager;
import com.atlassian.fugue.Option;
import com.atlassian.jira.application.ApplicationAuthorizationService;
import com.atlassian.jira.application.ApplicationRoleDefinitions;
import com.atlassian.jira.application.JiraApplication;
import com.atlassian.jira.application.JiraPluginApplicationMetaData;
import com.atlassian.jira.application.MockApplicationRoleDefinition;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static com.atlassian.fugue.Option.some;
import static com.atlassian.jira.project.type.JiraApplicationAdapter.BUSINESS_TYPE;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsIterableWithSize.iterableWithSize;
import static org.hamcrest.core.Is.is;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestJiraApplicationAdapter {
    private static final ApplicationUser USER = new MockApplicationUser("user");

    private static final PlatformApplication PLATFORM_APP = platformApp();

    private static final ProjectType PROJECT_TYPE_1 = projectType("one", "one-desc", "one-icon", "one-color");
    private static final ProjectType PROJECT_TYPE_2 = projectType("two", "two-desc", "two-icon", "two-color");
    private static final ProjectType PROJECT_TYPE_3 = projectType("three", "three-desc", "three-icon", "three-color");

    @Mock
    private ApplicationManager applicationManager;
    @Mock
    private PluginApplicationMetaDataManager metaDataManager;
    @Mock
    private ApplicationAuthorizationService applicationAuthorizationService;
    @Mock
    private ApplicationRoleDefinitions applicationRoleDefinitions;

    private JiraApplicationAdapter adapter;

    @Before
    public void setUp() {
        adapter = new JiraApplicationAdapter(applicationManager, metaDataManager, applicationAuthorizationService, applicationRoleDefinitions);
        when(applicationManager.getPlatform()).thenReturn(PLATFORM_APP);
        when(applicationManager.getApplications()).thenReturn(emptyList());
        when(metaDataManager.getApplications()).thenReturn(emptyList());
    }

    @Test
    public void jiraApplicationForPlatformApplicationGetsABusinessProjectTypeAdded() {
        List<JiraApplication> applications = Lists.newArrayList(adapter.getJiraApplications());

        assertOnlyPlatformApplicationIsReturned(applications);
    }

    @Test
    public void noJiraApplicationsAreReturnedIfThereAreNoApplicationsDefined() {
        when(applicationManager.getApplications()).thenReturn(new ArrayList<>());

        List<JiraApplication> applications = Lists.newArrayList(adapter.getJiraApplications());

        assertOnlyPlatformApplicationIsReturned(applications);
    }

    @Test
    public void noJiraApplicationsAreReturnedIfThereAreApplicationsButNoApplicationMetadata() {
        Application app1 = appWithKey("firstapp");
        Application app2 = appWithKey("secondapp");

        when(applicationManager.getApplications()).thenReturn(asList(app1, app2));
        when(metaDataManager.getApplication(any(ApplicationKey.class))).thenReturn(Option.<PluginApplicationMetaData>none());

        List<JiraApplication> applications = Lists.newArrayList(adapter.getJiraApplications());

        assertOnlyPlatformApplicationIsReturned(applications);
    }

    @Test
    public void applicationsAreCorrectlyMappedToTheirCorrespondingJiraApplications() {
        Application app1 = appWithKey("firstapp");
        Application app2 = appWithKey("secondapp");
        when(applicationManager.getApplications()).thenReturn(asList(app1, app2));

        PluginApplicationMetaData metadataApp1 = appMetadataFor(app1.getKey().value(), asList(PROJECT_TYPE_1, PROJECT_TYPE_2));
        PluginApplicationMetaData metadataApp2 = appMetadataFor(app2.getKey().value(), singletonList(PROJECT_TYPE_3));
        when(metaDataManager.getApplication(ApplicationKey.valueOf("firstapp"))).thenReturn(some(metadataApp1));
        when(metaDataManager.getApplication(ApplicationKey.valueOf("secondapp"))).thenReturn(some(metadataApp2));

        List<JiraApplication> applications = Lists.newArrayList(adapter.getJiraApplications());
        assertThat(applications.size(), is(3));
        assertIsJiraApplicationForPlatform(applications.get(0));
        assertJiraApplicationHas(applications.get(1), app1.getKey(), PROJECT_TYPE_1, PROJECT_TYPE_2);
        assertJiraApplicationHas(applications.get(2), app2.getKey(), PROJECT_TYPE_3);
    }

    @Test
    public void platformApplicationIsAdaptedExactlyOnceEvenWhenItAlsoAppearsOnTheListOfApplicationsProvidedByTheApplicationManager() {
        PlatformApplication platformApp = platformApp();
        when(applicationManager.getPlatform()).thenReturn(platformApp);

        Application normalApp = appWithKey("normalapp");
        when(applicationManager.getApplications()).thenReturn(asList(platformApp, normalApp));

        PluginApplicationMetaData metadataNormalApp = appMetadataFor(normalApp.getKey().value(), asList(PROJECT_TYPE_1, PROJECT_TYPE_2));
        when(metaDataManager.getApplication(ApplicationKey.valueOf("normalapp"))).thenReturn(some(metadataNormalApp));

        List<JiraApplication> applications = Lists.newArrayList(adapter.getJiraApplications());
        assertThat(applications.size(), is(2));
        assertIsJiraApplicationForPlatform(applications.get(0));
        assertJiraApplicationHas(applications.get(1), normalApp.getKey(), PROJECT_TYPE_1, PROJECT_TYPE_2);
    }

    @Test
    public void nonAccessibleApplicationsAreFilteredOutWhenRequestingAccessibleApplications() {
        Application accessibleApp = appWithKey("accessible");
        Application nonAccessibleApp = appWithKey("nonaccessible");
        when(applicationRoleDefinitions.getLicensed()).thenReturn(emptyList());
        when(applicationManager.getApplications()).thenReturn(asList(accessibleApp, nonAccessibleApp));

        PluginApplicationMetaData metadataNormalApp = appMetadataFor(accessibleApp.getKey().value(), singletonList(PROJECT_TYPE_1));
        PluginApplicationMetaData metadataNonAccessibleApp = appMetadataFor(accessibleApp.getKey().value(), singletonList(PROJECT_TYPE_2));
        when(metaDataManager.getApplication(ApplicationKey.valueOf("accessible"))).thenReturn(some(metadataNormalApp));
        when(metaDataManager.getApplication(ApplicationKey.valueOf("nonaccessible"))).thenReturn(some(metadataNonAccessibleApp));

        when(applicationAuthorizationService.isApplicationInstalledAndLicensed(ApplicationKey.valueOf("accessible"))).thenReturn(true);
        when(applicationAuthorizationService.isApplicationInstalledAndLicensed(ApplicationKey.valueOf("nonaccessible"))).thenReturn(false);

        List<JiraApplication> applications = Lists.newArrayList(adapter.getAccessibleJiraApplications());
        assertThat(applications.size(), is(1));
        assertJiraApplicationHas(applications.get(0), accessibleApp.getKey(), PROJECT_TYPE_1);
    }

    @Test
    public void coreShouldBeAccessibleIfAtLeastOneOtherApplicationIsLicensed() {
        Application nonAccessibleApp = appWithKey("nonaccessible");
        when(applicationRoleDefinitions.getLicensed()).thenReturn(singletonList(new MockApplicationRoleDefinition("jira-software")));
        when(applicationManager.getApplications()).thenReturn(singletonList(nonAccessibleApp));

        PluginApplicationMetaData metadataNonAccessibleApp = appMetadataFor(nonAccessibleApp.getKey().value(), singletonList(PROJECT_TYPE_2));
        when(metaDataManager.getApplication(ApplicationKey.valueOf("nonaccessible"))).thenReturn(some(metadataNonAccessibleApp));

        when(applicationAuthorizationService.isApplicationInstalledAndLicensed(ApplicationKey.valueOf("nonaccessible"))).thenReturn(false);

        List<JiraApplication> applications = Lists.newArrayList(adapter.getAccessibleJiraApplications());
        assertThat(applications.size(), is(1));
        assertIsJiraApplicationForPlatform(applications.get(0));
    }

    @Test
    public void nonAccessibleApplicationsAreFilteredOutWhenRequestingAccessibleApplicationsForAnUser() {
        Application accessibleApp = appWithKey("accessible");
        Application nonAccessibleApp = appWithKey("nonaccessible");
        when(applicationManager.getApplications()).thenReturn(asList(accessibleApp, nonAccessibleApp));

        PluginApplicationMetaData metadataNormalApp = appMetadataFor(accessibleApp.getKey().value(), singletonList(PROJECT_TYPE_1));
        PluginApplicationMetaData metadataNonAccessibleApp = appMetadataFor(accessibleApp.getKey().value(), singletonList(PROJECT_TYPE_2));
        when(metaDataManager.getApplication(ApplicationKey.valueOf("accessible"))).thenReturn(some(metadataNormalApp));
        when(metaDataManager.getApplication(ApplicationKey.valueOf("nonaccessible"))).thenReturn(some(metadataNonAccessibleApp));

        when(applicationAuthorizationService.canUseApplication(USER, ApplicationKey.valueOf("accessible"))).thenReturn(true);
        when(applicationAuthorizationService.canUseApplication(USER, ApplicationKey.valueOf("nonaccessible"))).thenReturn(false);

        List<JiraApplication> applications = Lists.newArrayList(adapter.getAccessibleJiraApplications(USER));
        assertThat(applications.size(), is(1));
        assertJiraApplicationHas(applications.get(0), accessibleApp.getKey(), PROJECT_TYPE_1);
    }

    private void assertJiraApplicationHas(JiraApplication app, ApplicationKey expectedKey, ProjectType... expectedProjectTypes) {
        assertApplicationHasKey(app, expectedKey);

        List<ProjectType> projectTypes = Lists.newArrayList(app.getProjectTypes());
        assertThat(projectTypes.size(), is(expectedProjectTypes.length));

        for (ProjectType expectedProjectType : expectedProjectTypes) {
            assertThat(projectTypes.contains(expectedProjectType), is(true));
        }
    }

    private static PlatformApplication platformApp() {
        PlatformApplication platformApp = mock(PlatformApplication.class);
        when(platformApp.getKey()).thenReturn(ApplicationKey.valueOf("platform"));
        return platformApp;
    }

    private void assertApplicationHasKey(JiraApplication application, ApplicationKey key) {
        assertThat(application.getKey(), is(key));
    }

    private Application appWithKey(String key) {
        Application application = mock(Application.class);
        when(application.getKey()).thenReturn(ApplicationKey.valueOf(key));
        return application;
    }

    private static ProjectType projectType(ProjectTypeKey key, String desc, String icon, String color) {
        return new ProjectType(key, desc, icon, color, 0);
    }

    private static ProjectType projectType(String key, String desc, String icon, String color) {
        return projectType(new ProjectTypeKey(key), desc, icon, color);
    }

    private PluginApplicationMetaData appMetadataFor(String appKey, List<ProjectType> projectTypesForApp) {
        JiraPluginApplicationMetaData metaData = mock(JiraPluginApplicationMetaData.class);
        when(metaData.getKey()).thenReturn(ApplicationKey.valueOf(appKey));
        when(metaData.getProjectTypes()).thenReturn(projectTypesForApp);
        return metaData;
    }

    private void assertOnlyPlatformApplicationIsReturned(List<JiraApplication> applications) {
        assertThat(applications, iterableWithSize(1));
        assertIsJiraApplicationForPlatform(applications.get(0));
    }

    private void assertIsJiraApplicationForPlatform(JiraApplication application) {
        assertJiraApplicationHas(application, PLATFORM_APP.getKey(), BUSINESS_TYPE);
    }
}
