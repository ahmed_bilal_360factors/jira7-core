package com.atlassian.jira.issue.fields.rest.json;

import com.atlassian.jira.avatar.Avatar;
import com.atlassian.jira.avatar.AvatarService;
import com.atlassian.jira.issue.MockIssueConstant;
import com.atlassian.jira.issue.fields.option.IssueConstantOption;
import com.atlassian.jira.issue.fields.option.Option;
import com.atlassian.jira.issue.fields.rest.json.beans.SuggestionBean;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoContainer;
import com.atlassian.jira.mock.security.MockAuthenticationContext;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.BaseUrl;
import com.google.common.collect.ImmutableList;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.when;

public class TestSuggestionBeanFactory {
    private static final String PROJECT_AVATAR_URL = "http://www.example.com/avatar";
    private static final String JIRA_BASE_URL = "http://www.jira.com/jira/";
    private static final List<Project> PROJECTS = ImmutableList.<Project>of(
            new MockProject(1L, "TST", "test"),
            new MockProject(2L, "PR", "project")
    );

    @Rule
    public MockitoContainer mockitoContainer = new MockitoContainer(this);

    @AvailableInContainer
    public JiraAuthenticationContext jiraAuthenticationContext = new MockAuthenticationContext(null);

    @Mock
    private BaseUrl baseUrl;

    @Mock
    private AvatarService avatarService;

    private SuggestionBeanFactory suggestionBeanFactory;

    @Before
    public void setup() throws URISyntaxException {
        when(avatarService.getProjectAvatarAbsoluteURL(isA(Project.class), isA(Avatar.Size.class)))
                .thenReturn(new URI(PROJECT_AVATAR_URL));

        when(baseUrl.getBaseUri()).thenReturn(new URI(JIRA_BASE_URL));

        this.suggestionBeanFactory = new SuggestionBeanFactory(avatarService, baseUrl);
    }

    @Test
    public void shouldCreateSuggestionsFromProjects() {
        final Collection<SuggestionBean> result = this.suggestionBeanFactory.projectSuggestions(PROJECTS, Optional.of(2L));

        assertThat(result, containsInAnyOrder(
                suggestionBeanMatcher(projectSuggestionName(PROJECTS.get(0)), PROJECTS.get(0).getId().toString(), PROJECT_AVATAR_URL, false),
                suggestionBeanMatcher(projectSuggestionName(PROJECTS.get(1)), PROJECTS.get(1).getId().toString(), PROJECT_AVATAR_URL, true)
        ));
    }

    @Test
    public void shouldCreateSuggestionsFromOptions() {
        final MockIssueConstant issueConstant1 = new MockIssueConstant("1", "First issue constant");
        issueConstant1.setIconUrl("path/to/image"); //path to image file
        final MockIssueConstant issueConstant2 = new MockIssueConstant("2", "Second issue constant");
        issueConstant2.setIconUrl("http://example.com/image"); //absolute url
        final MockIssueConstant issueConstant3 = new MockIssueConstant("3", "Third issue constant");
        issueConstant3.setIconUrl("/absolute/path/to/image"); //path to image file

        final List<Option> options = ImmutableList.<Option>of(
                new IssueConstantOption(issueConstant1),
                new IssueConstantOption(issueConstant2),
                new IssueConstantOption(issueConstant3)
        );

        final Collection<SuggestionBean> result = this.suggestionBeanFactory.optionSuggestions(options, Optional.of("2"));

        final String issueConstant1IconUrl = JIRA_BASE_URL + issueConstant1.getIconUrl();
        final String issueConstant3IconUrl = JIRA_BASE_URL + issueConstant3.getIconUrl().substring(1);

        assertThat(result, containsInAnyOrder(
                suggestionBeanMatcher(issueConstant1.getName(), issueConstant1.getId(), issueConstant1IconUrl, false),
                suggestionBeanMatcher(issueConstant2.getName(), issueConstant2.getId(), issueConstant2.getIconUrl(), true),
                suggestionBeanMatcher(issueConstant3.getName(), issueConstant3.getId(), issueConstant3IconUrl, false)
        ));
    }

    private static String projectSuggestionName(Project p) {
        return String.format("%s (%s)", p.getName(), p.getKey());
    }

    private static Matcher<SuggestionBean> suggestionBeanMatcher(final String label, final String value,
                                                                 final String icon, final boolean selected) {
        return Matchers.allOf(
                Matchers.hasProperty("label", Matchers.equalTo(label)),
                Matchers.hasProperty("value", Matchers.equalTo(value)),
                Matchers.hasProperty("icon", Matchers.equalTo(icon)),
                Matchers.hasProperty("selected", Matchers.equalTo(selected))
        );
    }

    @SafeVarargs
    private static <T> Matcher<Iterable<? extends T>> containsInAnyOrder(Matcher<? super T>... matchers) {
        return Matchers.containsInAnyOrder(matchers);
    }
}
