package com.atlassian.jira.upgrade.tasks.role;

import com.atlassian.core.util.Clock;
import com.atlassian.jira.license.RenaissanceMigrationStatus;
import com.atlassian.jira.web.util.ExternalLinkUtil;
import com.atlassian.jira.web.util.MockExternalLinkUtil;
import org.hamcrest.Matchers;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.slf4j.Logger;

import java.sql.Date;
import java.time.LocalDate;

import static com.atlassian.jira.test.util.lic.core.CoreLicenses.LICENSE_CORE;
import static com.atlassian.jira.test.util.lic.sd.ServiceDeskLicenses.LICENSE_SERVICE_DESK_ABP;
import static com.atlassian.jira.test.util.lic.software.SoftwareLicenses.LICENSE_SOFTWARE;
import static com.atlassian.jira.upgrade.tasks.role.TestUtils.emptyState;
import static com.atlassian.jira.upgrade.tasks.role.TestUtils.licensesState;
import static com.atlassian.jira.upgrade.tasks.role.TestUtils.toLicense;
import static com.google.common.collect.ImmutableList.of;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class RenaissanceMigrationImplTest {
    private static final long ZERO_TIMER = 1000;
    @Rule
    public MockitoRule mockitoMocks = MockitoJUnit.rule();

    @Mock
    private MigrationStateDao migrationStateDao;

    @Mock
    private MigrationValidator migrationValidator;

    @Mock
    private RenaissanceMigrationStatus predicate;

    @Mock
    private Clock mockClock;

    @Mock
    private Logger loggerMock;

    private ExternalLinkUtil util = new MockExternalLinkUtil().addLink("external.link.jira.support.site", "support");

    @Test
    public void migrateCorrectlyUpdatesJira() {
        //given
        when(migrationStateDao.get()).thenReturn(emptyState());
        final AddLicenseMigrationTask task1 = new AddLicenseMigrationTask(LICENSE_SERVICE_DESK_ABP);
        final AddLicenseMigrationTask task2 = new AddLicenseMigrationTask(LICENSE_SOFTWARE);
        final RenaissanceMigration migration = new RenaissanceMigrationImpl(migrationStateDao, util,
                of(task1, task2), migrationValidator, predicate, mockClock, loggerMock);
        when(mockClock.getCurrentDate()).thenReturn(Date.valueOf(LocalDate.now()));
        //when
        migration.migrate();

        //then
        verify(migrationStateDao)
                .put(argThat(new MigrationStateMatcher().licenses(LICENSE_SERVICE_DESK_ABP, LICENSE_SOFTWARE)));
        assertThat(task1.cleanupCalled, Matchers.is(true));
        assertThat(task2.cleanupCalled, Matchers.is(true));
        verify(predicate).markDone();
    }

    @Test
    public void migrateCorrectlyUpdatesJiraWhenUserSuppliesLicense() {
        //given
        when(migrationStateDao.get()).thenReturn(licensesState(LICENSE_CORE));
        final AddLicenseMigrationTask task1 = new AddLicenseMigrationTask(LICENSE_SERVICE_DESK_ABP);
        final AddLicenseMigrationTask task2 = new AddLicenseMigrationTask(LICENSE_SOFTWARE);
        final RenaissanceMigration migration = new RenaissanceMigrationImpl(migrationStateDao, util,
                of(task1, task2), migrationValidator, predicate, mockClock, loggerMock);
        when(mockClock.getCurrentDate()).thenReturn(Date.valueOf(LocalDate.now()));

        //when
        migration.migrate();

        //then
        verify(migrationStateDao).put(argThat(new MigrationStateMatcher().licenses(LICENSE_CORE)));
        assertThat(task1.cleanupCalled, Matchers.is(true));
        assertThat(task2.cleanupCalled, Matchers.is(true));
        verify(predicate).markDone();
    }

    @Test
    public void migrateThrowsOnErrorAndDoesNotUpdateJira() {
        //given
        when(migrationStateDao.get()).thenReturn(emptyState());
        final AddLicenseMigrationTask licenseTask = new AddLicenseMigrationTask(LICENSE_SERVICE_DESK_ABP);
        final RenaissanceMigration migration
                = new RenaissanceMigrationImpl(migrationStateDao, util,
                of(licenseTask, new ErrorMigrationTask()), migrationValidator, predicate, mockClock, loggerMock);
        when(mockClock.getCurrentDate()).thenReturn(Date.valueOf(LocalDate.now()));

        try {
            //when
            migration.migrate();

            //then
            fail("Migration did not fail.");
        } catch (MigrationFailedException expected) {
            //very good.
        }

        verify(migrationStateDao, never()).put(any());
        verify(predicate, never()).markDone();
        assertThat(licenseTask.cleanupCalled, Matchers.is(false));
    }

    @Test
    public void migrateAfterSaveErrorFailsOnFirstError() {
        //given
        when(migrationStateDao.get()).thenReturn(emptyState());
        final AddLicenseMigrationTask licenseTask = new AddLicenseMigrationTask(LICENSE_SERVICE_DESK_ABP);
        final AddLicenseMigrationTask licenseTask2 = new AddLicenseMigrationTask(LICENSE_SOFTWARE);
        final RenaissanceMigration migration
                = new RenaissanceMigrationImpl(migrationStateDao, util,
                of(licenseTask, new AfterSaveErrorMigrationTask(), licenseTask2), migrationValidator, predicate,
                mockClock, loggerMock);
        when(mockClock.getCurrentDate()).thenReturn(Date.valueOf(LocalDate.now()));

        try {
            //when
            migration.migrate();

            //then
            fail("Migration did not fail.");
        } catch (MigrationFailedException expected) {
            //very good.
        }

        //State should be saved before afterSave is called.
        verify(migrationStateDao)
                .put(argThat(new MigrationStateMatcher().licenses(LICENSE_SERVICE_DESK_ABP, LICENSE_SOFTWARE)));

        //Cleanup worked on first task.
        assertThat(licenseTask.cleanupCalled, Matchers.is(true));
        //But not on the second.
        assertThat(licenseTask2.cleanupCalled, Matchers.is(false));
        //This also skips the version store.
        verify(predicate, never()).markDone();
    }

    @Test
    public void timingOfMigrationTasksIsCorrect() {
        //given
        when(migrationStateDao.get()).thenReturn(emptyState());
        final The20msTask firstTask = new The20msTask();
        final The100msTask secondTask = new The100msTask();
        final RenaissanceMigration migration
                = new RenaissanceMigrationImpl(migrationStateDao, util,
                of(firstTask, secondTask), new MockValidator40ms(), predicate, mockClock, loggerMock);
        when(mockClock.getCurrentDate()).thenReturn(new Date(ZERO_TIMER), new Date(ZERO_TIMER + 20),
                new Date(ZERO_TIMER + 20), new Date(ZERO_TIMER + 120),
                new Date(ZERO_TIMER + 120), new Date(ZERO_TIMER + 160));

        //when
        migration.executeTasksAndValidate();

        //then
        verify(loggerMock).info("Elapsed time of 20ms for migration task: " + firstTask.getClass().getSimpleName());
        verify(loggerMock).info("Elapsed time of 100ms for migration task: " + secondTask.getClass().getSimpleName());
        verify(loggerMock).info("Elapsed time of 100ms for migration task: " + secondTask.getClass().getSimpleName());
    }

    private static class AddLicenseMigrationTask extends MigrationTask {
        private final License license;
        private boolean cleanupCalled;

        AddLicenseMigrationTask(com.atlassian.jira.test.util.lic.License license) {
            this.license = toLicense(license);
        }

        @Override
        MigrationState migrate(final MigrationState state, final boolean licenseSuppliedByUser) {
            if (!licenseSuppliedByUser) {
                return state.changeLicenses(licenses -> licenses.addLicense(license))
                        .withAfterSaveTask(() -> cleanupCalled = true);
            } else {
                return state.withAfterSaveTask(() -> cleanupCalled = true);
            }
        }
    }

    private static class ErrorMigrationTask extends MigrationTask {
        @Override
        MigrationState migrate(final MigrationState state, final boolean licenseSuppliedByUser) {
            throw new MigrationFailedException("Test failure.");
        }
    }

    private static class AfterSaveErrorMigrationTask extends MigrationTask {
        @Override
        MigrationState migrate(final MigrationState state, final boolean licenseSuppliedByUser) {
            return state.withAfterSaveTask(this::throwException);
        }

        void throwException() {
            throw new MigrationFailedException("Migration has failed.");
        }
    }

    private static class The20msTask extends MigrationTask {

        @Override
        MigrationState migrate(final MigrationState state, final boolean licenseSuppliedByUser) {
            return state;
        }
    }

    private static class The100msTask extends MigrationTask {

        @Override
        MigrationState migrate(final MigrationState state, final boolean licenseSuppliedByUser) {
            return state;
        }
    }

    private static class MockValidator40ms extends MigrationValidator {

        @Override
        void validate(final MigrationState original, final MigrationState resulting) {
            return; //do nothing
        }
    }
}