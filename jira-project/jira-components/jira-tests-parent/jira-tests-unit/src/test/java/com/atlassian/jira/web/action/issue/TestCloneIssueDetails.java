package com.atlassian.jira.web.action.issue;

import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.bc.issue.IssueService;
import com.atlassian.jira.config.SubTaskManager;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationPropertiesImpl;
import com.atlassian.jira.exception.RemoveException;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueFactory;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.TemporaryAttachmentsMonitorLocator;
import com.atlassian.jira.issue.customfields.CustomFieldType;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.fields.MockCustomField;
import com.atlassian.jira.issue.fields.screen.FieldScreenRendererFactory;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.issue.link.IssueLinkType;
import com.atlassian.jira.issue.link.IssueLinkTypeImpl;
import com.atlassian.jira.issue.link.IssueLinkTypeManager;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.license.CreateIssueLicenseCheck;
import com.atlassian.jira.mock.MockPermissionManager;
import com.atlassian.jira.mock.i18n.MockI18nHelper;
import com.atlassian.jira.mock.ofbiz.MockGenericValue;
import com.atlassian.jira.ofbiz.FieldMap;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.task.TaskManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.web.bean.TaskDescriptorBean;
import com.atlassian.jira.workflow.JiraWorkflow;
import com.atlassian.jira.workflow.WorkflowManager;
import com.google.common.collect.Lists;
import com.opensymphony.workflow.loader.ActionDescriptor;
import com.opensymphony.workflow.loader.WorkflowDescriptor;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.ofbiz.core.entity.GenericEntityException;
import org.ofbiz.core.entity.GenericValue;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.atlassian.jira.issue.customfields.CloneOptionConfiguration;
import static com.atlassian.jira.license.LicenseCheck.PASS;
import static com.atlassian.jira.web.action.issue.CloneIssueDetails.CustomFieldCloneOption;
import static java.util.Arrays.asList;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestCloneIssueDetails {
    @Rule
    public RuleChain ruleChain = MockitoMocksInContainer.forTest(this);

    private GenericValue originalIssue;
    private Map<String, Object> issueParamsMap;
    private GenericValue componentGV;

    private String[] components;
    private GenericValue cloneLink;

    @Mock
    @AvailableInContainer
    private IssueLinkTypeManager mockIssueLinkTypeManager;
    @Mock
    @AvailableInContainer
    private SubTaskManager mockSubTaskManager;
    @Mock
    @AvailableInContainer
    private PermissionManager myPermissionManager;
    @Mock
    private IssueManager mockIssueManager;
    @Mock
    @AvailableInContainer
    private EventPublisher eventPublisher;
    @Mock
    @AvailableInContainer
    private ApplicationPropertiesImpl applicationProperties;
    @Mock
    @AvailableInContainer
    private TemporaryAttachmentsMonitorLocator temporaryAttachmentsMonitorLocator;
    @Mock
    private IssueService mockIssueService;
    @Mock
    private IssueFactory mockIssueFactory;
    @Mock
    private TaskManager taskManager;
    @Mock
    private TaskDescriptorBean.Factory taskBeanFactory;
    @Mock
    @AvailableInContainer
    private CustomFieldManager mockCustomFieldManager;
    @Mock
    @AvailableInContainer
    private WorkflowManager mockWorkflowManager;
    @Mock
    @AvailableInContainer
    private Logger mockLogger;
    @Mock
    @AvailableInContainer
    private JiraAuthenticationContext authContext;

    @Before
    public void setUp() throws Exception {
        // Tests fail due to unambiguous definition of IssueIndexManager in VersionManager wihtout a refreshed container
        myPermissionManager = new MyPermissionManager();
        applicationProperties = new MyApplicationProperties("Clones");

        cloneLink = new MockGenericValue("IssueLinkType", FieldMap.build("linkname", "Clones", "inward", "clones", "outward", "is cloned by"));

        issueParamsMap = new HashMap<>();
        issueParamsMap.put("project", new Long(1));
        issueParamsMap.put("type", "test-type");
        issueParamsMap.put("environment", "test-env");
        issueParamsMap.put("description", "test-desc");
        issueParamsMap.put("reporter", "test-reporter");
        issueParamsMap.put("assignee", "test-assignee");
        issueParamsMap.put("summary", "test-summary");
        issueParamsMap.put("priority", "test-priority");
        issueParamsMap.put("security", new Long(1));
        issueParamsMap.put("key", "TST-1");
        issueParamsMap.put("id", new Long(1));

        originalIssue = new MockGenericValue("Issue", issueParamsMap);

        componentGV = new MockGenericValue("Component", FieldMap.build("id", new Long(1)));
        components = new String[]{componentGV.getString("id")};

        when(authContext.getI18nHelper()).thenReturn(new MockI18nHelper());
    }

    @Test
    public void testDisplayWarningWithCloneName() throws GenericEntityException {
        when(mockIssueLinkTypeManager.getIssueLinkTypesByName(eq("Clones"))).thenReturn(Collections.emptyList());
        applicationProperties = new MyApplicationProperties("Clones");
        CloneIssueDetails cloneIssueDetails = getCloneIssueDetails(applicationProperties);
        Assert.assertTrue(cloneIssueDetails.isDisplayCloneLinkWarning());
    }

    @Test
    public void testDisplayWarningWithoutCloneName() throws GenericEntityException {
        applicationProperties = new MyApplicationProperties("");
        CloneIssueDetails cloneIssueDetails = getCloneIssueDetails(applicationProperties);
        Assert.assertFalse(cloneIssueDetails.isDisplayCloneLinkWarning());
    }

    @Test
    public void testDisplayWarningWithCloneNameThatExists() throws GenericEntityException {
        when(mockIssueLinkTypeManager.getIssueLinkTypesByName(eq("Clones"))).thenReturn(Lists.<IssueLinkType>newArrayList(new IssueLinkTypeImpl(cloneLink)));
        applicationProperties = new MyApplicationProperties("Clones");
        CloneIssueDetails cloneIssueDetails = getCloneIssueDetails(applicationProperties);
        Assert.assertFalse(cloneIssueDetails.isDisplayCloneLinkWarning());
    }

    @Test
    public void shouldReturnAllCloneOptionsToBeDisplayed() {
        CloneIssueDetails cloneIssueDetails = spy(getCloneIssueDetails(null));
        cloneIssueDetails.setOriginalIssue(originalIssueWithNeededInformationToBeCloned());

        CustomField cf1 = givenACustomFieldWithId("10000");
        CustomField cf2 = givenACustomFieldWithId("10001");
        CustomField cf3 = givenACustomFieldWithId("10002");
        CustomField cf4 = givenACustomFieldWithId("10003");
        CustomField cf5 = givenACustomFieldWithId("10004");
        givenCustomFieldsWithDisplayableCloneOption(cf3, cf5);
        when(cloneIssueDetails.getCustomFields(cloneIssueDetails.getOriginalIssue())).thenReturn(asList(cf1, cf2, cf3, cf4, cf5));

        List<CustomFieldCloneOption> cloneOptions = cloneIssueDetails.getCustomFieldCloneOptions();
        assertThat(cloneOptions.size(), is(2));
        assertThat(cloneOptions.get(0).getId(), equalTo(cf3.getId()));
        assertThat(cloneOptions.get(1).getId(), equalTo(cf5.getId()));
    }

    private CustomField givenACustomFieldWithId(String customFieldId) {
        CustomFieldType cfType = mock(CustomFieldType.class);
        when(cfType.getCloneOptionConfiguration(any(CustomField.class), any(Issue.class))).thenReturn(CloneOptionConfiguration.DO_NOT_DISPLAY);

        return new MockCustomField(customFieldId, "Custom field " + customFieldId, cfType);
    }

    private void givenCustomFieldsWithDisplayableCloneOption(CustomField... customFields) {
        for (CustomField cf : customFields) {
            CustomFieldType cfType = cf.getCustomFieldType();
            when(cfType.getCloneOptionConfiguration(any(CustomField.class), any(Issue.class))).thenReturn(CloneOptionConfiguration.withOptionLabel("Clone " + cf.getId()));
        }
    }

    private void givenFailureToCreateNewIssueForCloning() {
        JiraWorkflow mockJiraWorkflow = mock(JiraWorkflow.class);
        WorkflowDescriptor mockWorkflowDescriptor = mock(WorkflowDescriptor.class);
        List<ActionDescriptor> mockInitialActions = new ArrayList<ActionDescriptor>(1);
        mockInitialActions.add(mock(ActionDescriptor.class));
        when(mockWorkflowDescriptor.getInitialActions()).thenReturn(mockInitialActions);
        when(mockJiraWorkflow.getDescriptor()).thenReturn(mockWorkflowDescriptor);
        when(mockWorkflowManager.getWorkflow(anyLong(), anyString())).thenReturn(mockJiraWorkflow);

        IssueService.IssueResult createIssueForCloningResult = mock(IssueService.IssueResult.class);
        when(createIssueForCloningResult.isValid()).thenReturn(false);
        ErrorCollection mockError = failureToCreateIssueForCloningError();
        when(createIssueForCloningResult.getErrorCollection()).thenReturn(mockError);
        when(mockIssueService.create(any(ApplicationUser.class), any(IssueService.CreateValidationResult.class), anyString())).thenReturn(createIssueForCloningResult);
    }

    private static ErrorCollection failureToCreateIssueForCloningError() {
        ErrorCollection failureToCreateIssueForCloningError = mock(ErrorCollection.class);
        when(failureToCreateIssueForCloningError.getErrorMessages()).thenReturn(asList("This operation is intentionally forced to be fail"));

        Map<String, String> forcedToBeFailError = new HashMap<String, String>();
        forcedToBeFailError.put("forced.to.be.fail.error", "This operation is intentionally forced to be fail");
        when(failureToCreateIssueForCloningError.getErrors()).thenReturn(forcedToBeFailError);
        when(failureToCreateIssueForCloningError.getReasons()).thenReturn(Collections.emptySet());
        return failureToCreateIssueForCloningError;
    }

    private Issue originalIssueWithNeededInformationToBeCloned() {
        Issue originalIssueToBeCloned = mock(MutableIssue.class);
        when(originalIssueToBeCloned.getOriginalEstimate()).thenReturn(1L);
        when(originalIssueToBeCloned.getFixVersions()).thenReturn(Collections.emptyList());
        when(originalIssueToBeCloned.getAffectedVersions()).thenReturn(Collections.emptyList());
        when(originalIssueToBeCloned.getId()).thenReturn(1L);

        Project originalProjectObject = mock(Project.class);
        when(originalProjectObject.getId()).thenReturn(1L);
        when(originalIssueToBeCloned.getProjectObject()).thenReturn(originalProjectObject);

        IssueType originalIssueType = mock(IssueType.class);
        when(originalIssueType.getId()).thenReturn("issue1");
        when(originalIssueToBeCloned.getIssueTypeObject()).thenReturn(originalIssueType);
        when(mockIssueFactory.cloneIssue(any(Issue.class))).thenReturn((MutableIssue) originalIssueToBeCloned);
        when(mockIssueFactory.getIssue(any(GenericValue.class))).thenReturn((MutableIssue) originalIssueToBeCloned);
        return originalIssueToBeCloned;
    }

    // ---- Helper Methods & Classes----
    private CloneIssueDetails getCloneIssueDetails(ApplicationPropertiesImpl applicationProperties) {
        CloneIssueDetails cloneIssueDetails;
        IssueCreationHelperBean issueCreationHelperBean = getMockIssueCreationHelperBean();
        if (applicationProperties != null)
            cloneIssueDetails = new CloneIssueDetails(applicationProperties, myPermissionManager, null, mockIssueLinkTypeManager, mockSubTaskManager, null, null, issueCreationHelperBean, mockIssueFactory, mockIssueService, temporaryAttachmentsMonitorLocator, taskManager, taskBeanFactory);
        else
            cloneIssueDetails = new CloneIssueDetails(null, myPermissionManager, null, mockIssueLinkTypeManager, mockSubTaskManager, null, null, issueCreationHelperBean, mockIssueFactory, mockIssueService, temporaryAttachmentsMonitorLocator, taskManager, taskBeanFactory);

        return cloneIssueDetails;
    }

    // Mock Application Properties for tests
    private static class MyApplicationProperties extends ApplicationPropertiesImpl {
        private final String cloneLinkTypeName;

        public MyApplicationProperties(String cloneLinkTypeName) {
            super(null);
            this.cloneLinkTypeName = cloneLinkTypeName;
        }

        public String getDefaultBackedString(String name) {
            if (APKeys.JIRA_CLONE_LINKTYPE_NAME.equals(name))
                return cloneLinkTypeName;

            return null;
        }
    }

    private class MyPermissionManager extends MockPermissionManager {

        public MyPermissionManager() {
            super(true);
        }

        public boolean hasPermission(int permissionsId, ApplicationUser u) {
            return true;
        }

        public boolean hasPermission(int permissionsId, Issue entity, ApplicationUser u) {
            return true;
        }

        public boolean hasPermission(int permissionsId, Project project, ApplicationUser user) {
            return true;
        }

        public boolean hasPermission(int permissionsId, Project project, ApplicationUser user, boolean issueCreation) {
            return true;
        }

        public void removeGroupPermissions(String group) throws RemoveException {

        }

        public boolean hasProjects(int permissionId, ApplicationUser user) {
            return false;
        }

        public Collection<Group> getAllGroups(int permissionId, Project project) {
            return null;
        }
    }

    private IssueCreationHelperBean getMockIssueCreationHelperBean() {
        CreateIssueLicenseCheck licenseCheck = mock(CreateIssueLicenseCheck.class);
        when(licenseCheck.evaluate()).thenReturn(PASS);

        return new IssueCreationHelperBeanImpl(
                mock(FieldManager.class), mock(FieldScreenRendererFactory.class), licenseCheck, authContext);
    }
}
