package com.atlassian.jira.web.action.util;

import com.atlassian.jira.mock.i18n.MockI18nHelper;
import com.atlassian.jira.mock.security.MockSimpleAuthenticationContext;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.plugin.webresource.WebResourceManager;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import java.util.Locale;

import static org.mockito.Mockito.verify;

/**
 * Test for {@link com.atlassian.jira.web.action.util.FieldsResourceIncluderImpl}.
 *
 * @since v4.2
 */
public class TestFieldsResourceIncluderImpl {
    @Rule
    public final MethodRule initMockito = MockitoJUnit.rule();

    @Mock
    CalendarResourceIncluder includer;
    @Mock
    WebResourceManager wrm;

    @Test
    public void testIncludeResourcesForCurrentUser() {
        final Locale expectedLocale = Locale.CANADA_FRENCH;
        final JiraAuthenticationContext ctx = new MockSimpleAuthenticationContext((ApplicationUser) null, Locale.ENGLISH, new MockI18nHelper(expectedLocale));
        final FieldsResourceIncluderImpl fieldsIncluder = new FieldsResourceIncluderImpl(ctx, wrm, includer);

        fieldsIncluder.includeFieldResourcesForCurrentUser();

        verify(wrm).requireResource("jira.webresources:jira-fields");
        verify(includer).includeForLocale(expectedLocale);
    }
}
