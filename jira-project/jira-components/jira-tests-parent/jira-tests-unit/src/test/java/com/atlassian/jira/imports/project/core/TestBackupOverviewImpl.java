package com.atlassian.jira.imports.project.core;

import com.atlassian.core.util.collection.EasyList;
import com.atlassian.jira.external.beans.ExternalProject;
import com.google.common.collect.Sets;
import org.junit.Test;

import java.util.Collections;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * @since v3.13
 */
public class TestBackupOverviewImpl {
    @Test
    public void testIllegalConstructorArguments() {
        try {
            new BackupOverviewImpl(null, null);
            fail();
        } catch (IllegalArgumentException e) {
            // We want this to happen
        }

        try {
            final BackupProject project = new BackupProjectImpl(new ExternalProject(), Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), 0, Collections.emptyMap());
            new BackupOverviewImpl(null, EasyList.build(project));
            fail();
        } catch (IllegalArgumentException e) {
            // We want this to happen
        }

    }

    @Test
    public void testGetProject() {
        final ExternalProject project = new ExternalProject();
        project.setKey("TST");
        final BackupProject backupProject = new BackupProjectImpl(project, Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), 0, Collections.emptyMap());
        BackupSystemInformation backupSystemInformation = new BackupSystemInformationImpl("123", "Enterprise", Collections.emptyList(), true, Collections.emptyMap(), Sets.<String>newHashSet(), 0);
        final BackupOverviewImpl backupOverview = new BackupOverviewImpl(backupSystemInformation, EasyList.build(backupProject));

        assertEquals(backupProject, backupOverview.getProject("TST"));
    }

    @Test
    public void testGetProjects() {
        final ExternalProject project = new ExternalProject();
        project.setKey("TST");
        final BackupProject backupProject = new BackupProjectImpl(project, Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), 0, Collections.emptyMap());
        BackupSystemInformation backupSystemInformation = new BackupSystemInformationImpl("123", "Enterprise", Collections.emptyList(), true, Collections.emptyMap(), Sets.<String>newHashSet(), 0);
        final BackupOverviewImpl backupOverview = new BackupOverviewImpl(backupSystemInformation, EasyList.build(backupProject));

        assertEquals(1, backupOverview.getProjects().size());
    }

    @Test
    public void testGetProjectsIsOrdered() {
        final ExternalProject project1 = new ExternalProject();
        project1.setKey("TST");
        project1.setName("Test");
        final ExternalProject project2 = new ExternalProject();
        project2.setKey("ANA");
        project2.setName("Another");
        final BackupProject backupProject1 = new BackupProjectImpl(project1, Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), 0, Collections.emptyMap());
        final BackupProject backupProject2 = new BackupProjectImpl(project2, Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), 0, Collections.emptyMap());
        BackupSystemInformation backupSystemInformation = new BackupSystemInformationImpl("123", "Enterprise", Collections.emptyList(), true, Collections.emptyMap(), Sets.<String>newHashSet(), 0);
        final BackupOverviewImpl backupOverview = new BackupOverviewImpl(backupSystemInformation, EasyList.build(backupProject1, backupProject2));

        assertEquals(2, backupOverview.getProjects().size());
        assertEquals(backupProject2, backupOverview.getProjects().get(0));
        assertEquals(backupProject1, backupOverview.getProjects().get(1));
    }

    @Test
    public void testGetSystemInformation() {
        final ExternalProject project1 = new ExternalProject();
        project1.setKey("TST");
        project1.setName("Test");
        final BackupProject backupProject1 = new BackupProjectImpl(project1, Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), 0, Collections.emptyMap());
        BackupSystemInformation backupSystemInformation = new BackupSystemInformationImpl("123", "Enterprise", Collections.emptyList(), true, Collections.emptyMap(), Sets.<String>newHashSet(), 0);
        final BackupOverviewImpl backupOverview = new BackupOverviewImpl(backupSystemInformation, EasyList.build(backupProject1));

        assertEquals(backupSystemInformation, backupOverview.getBackupSystemInformation());
        assertEquals(1, backupOverview.getProjects().size());
        assertEquals(backupProject1, backupOverview.getProjects().get(0));
    }
}
