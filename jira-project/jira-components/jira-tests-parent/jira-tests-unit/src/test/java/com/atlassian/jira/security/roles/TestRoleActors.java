package com.atlassian.jira.security.roles;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class TestRoleActors {
    @Test
    public void testDefaultRoleActorsAddRoleActor() {
        ProjectRole role = new ProjectRoleImpl("name", "description");
        RoleActor mockRoleActor1 = new MockRoleActor(ProjectRoleActor.GROUP_ROLE_ACTOR_TYPE, "group1");
        assertEquals(mockRoleActor1, new MockRoleActor(ProjectRoleActor.GROUP_ROLE_ACTOR_TYPE, "group1"));
        DefaultRoleActors roleActors = new DefaultRoleActorsImpl(role.getId(), mockRoleActor1);
        assertNotNull(roleActors.getRoleActors());
        assertEquals(1, roleActors.getRoleActors().size());
        assertTrue(roleActors.getRoleActors().contains(new MockRoleActor(ProjectRoleActor.GROUP_ROLE_ACTOR_TYPE, "group1")));
        MockRoleActor mockRoleActor2 = new MockRoleActor(ProjectRoleActor.USER_ROLE_ACTOR_TYPE, "user1");
        roleActors = roleActors.addRoleActor(mockRoleActor2);
        assertNotNull(roleActors.getRoleActors());
        assertEquals(2, roleActors.getRoleActors().size());
        assertTrue(roleActors.getRoleActors().contains(new MockRoleActor(ProjectRoleActor.GROUP_ROLE_ACTOR_TYPE, "group1")));
        assertTrue(roleActors.getRoleActors().contains(new MockRoleActor(ProjectRoleActor.USER_ROLE_ACTOR_TYPE, "user1")));
    }
}
