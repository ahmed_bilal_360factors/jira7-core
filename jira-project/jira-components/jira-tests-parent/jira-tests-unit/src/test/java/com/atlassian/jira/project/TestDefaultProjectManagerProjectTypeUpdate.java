package com.atlassian.jira.project;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.association.NodeAssociationStore;
import com.atlassian.jira.avatar.AvatarManager;
import com.atlassian.jira.bc.project.ProjectTypeValidator;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.config.properties.PropertiesManager;
import com.atlassian.jira.database.DbConnectionManager;
import com.atlassian.jira.database.MockDbConnectionManager;
import com.atlassian.jira.entity.property.JsonEntityPropertyManager;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.mock.component.MockComponentWorker;
import com.atlassian.jira.mock.ofbiz.MockGenericValue;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.project.type.ProjectTypeKey;
import com.atlassian.jira.project.util.ProjectKeyStore;
import com.atlassian.jira.security.roles.ProjectRoleManager;
import com.atlassian.jira.transaction.TransactionSupport;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.ofbiz.core.entity.GenericValue;

import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestDefaultProjectManagerProjectTypeUpdate {
    private static final long PROJECT_ID = 1;
    private static final long ANY_ID = 2;
    private static final Project PROJECT = new MockProject(PROJECT_ID);
    private static final Project UPDATED_PROJECT = new MockProject(ANY_ID);
    private static final ApplicationUser USER = new MockApplicationUser("user");

    @Mock
    private OfBizDelegator ofBizDelegator;
    @Mock
    private ProjectFactory projectFactory;
    @Mock
    private ProjectTypeValidator projectTypeValidator;

    private MockDbConnectionManager dbConnectionManager;

    private DefaultProjectManager projectManager;

    @Before
    public void setUp() {
        ComponentAccessor.initialiseWorker(new MockComponentWorker());
        dbConnectionManager = new MockDbConnectionManager();
        projectManager = newProjectManager(ofBizDelegator, dbConnectionManager, projectFactory, projectTypeValidator);
    }

    @Test(expected = IllegalStateException.class)
    public void projectTypeMustBeValid() {
        ProjectTypeKey type = invalidProjectType("type");

        projectManager.updateProjectType(USER, PROJECT, type);
    }

    @Test
    public void updatesTheProjectTypeWithTheExpectedValue() {
        ProjectTypeKey type = validProjectType("type");
        dbConnectionManager.setUpdateResults(updateTypeSqlFor("type", PROJECT_ID), 1);

        projectManager.updateProjectType(USER, PROJECT, type);

        dbConnectionManager.assertAllExpectedStatementsWereRun();
    }

    @Test
    public void returnsTheUpdatedProject() {
        ProjectTypeKey type = validProjectType("type");
        GenericValue projectGenericValue = projectGenericValueWithType(type.getKey());
        when(ofBizDelegator.findById("Project", PROJECT_ID)).thenReturn(projectGenericValue);
        when(projectFactory.getProject(projectGenericValue)).thenReturn(UPDATED_PROJECT);
        dbConnectionManager.setUpdateResults(updateTypeSqlFor("type", PROJECT_ID), 1);

        Project project = projectManager.updateProjectType(USER, PROJECT, type);

        assertThat(project, is(UPDATED_PROJECT));
    }

    private String updateTypeSqlFor(String projectType, long projectId) {
        return "update project\nset projecttype = '" + projectType + "'\nwhere project.id = " + projectId;
    }

    private MockGenericValue projectGenericValueWithType(String type) {
        Map<String, Object> fields = new HashMap<>();
        fields.put("projecttype", type);
        return new MockGenericValue("Project", fields);
    }

    private ProjectTypeKey invalidProjectType(String type) {
        ProjectTypeKey typeKey = new ProjectTypeKey(type);
        when(projectTypeValidator.isValid(USER, typeKey)).thenReturn(false);
        return typeKey;
    }

    private ProjectTypeKey validProjectType(String type) {
        ProjectTypeKey typeKey = new ProjectTypeKey(type);
        when(projectTypeValidator.isValid(USER, typeKey)).thenReturn(true);
        return typeKey;
    }

    private DefaultProjectManager newProjectManager(OfBizDelegator ofBizDelegator, DbConnectionManager connectionManager, ProjectFactory projectFactory, ProjectTypeValidator projectTypeValidator) {
        return new DefaultProjectManager(
                ofBizDelegator,
                dbConnectionManager,
                mock(NodeAssociationStore.class),
                projectFactory,
                mock(ProjectRoleManager.class),
                mock(IssueManager.class),
                mock(UserManager.class),
                mock(ProjectCategoryStore.class),
                mock(ApplicationProperties.class),
                mock(ProjectKeyStore.class),
                mock(TransactionSupport.class),
                mock(PropertiesManager.class),
                mock(JsonEntityPropertyManager.class),
                mock(EventPublisher.class),
                projectTypeValidator,
                mock(AvatarManager.class)
        );
    }
}
