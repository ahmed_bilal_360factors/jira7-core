package com.atlassian.jira.permission;

import com.atlassian.fugue.Option;
import com.atlassian.jira.scheme.Scheme;
import com.atlassian.jira.scheme.SchemeEntity;
import com.atlassian.jira.security.plugin.ProjectPermissionKey;
import com.atlassian.jira.security.roles.ProjectRoleManager;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Map;

import static com.atlassian.fugue.Option.some;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

@RunWith(MockitoJUnitRunner.class)
public final class PermissionSchemeRepresentationConverterTest {
    @Mock
    private ProjectRoleManager projectRoleManager;

    @InjectMocks
    PermissionSchemeRepresentationConverter factory;

    @Test
    public void testConvertingToScheme() {
        for (Map.Entry<Scheme, PermissionScheme> schemeEntry : PermissionSchemeTestConstants.schemeEquivalency.entrySet()) {
            assertThat(factory.scheme(schemeEntry.getValue()), equalTo(schemeEntry.getKey()));
        }
    }

    @Test
    public void testConvertingFromScheme() {
        for (Map.Entry<Scheme, PermissionScheme> schemeEntry : PermissionSchemeTestConstants.schemeEquivalency.entrySet()) {
            assertThat(factory.permissionScheme(schemeEntry.getKey()), equalTo(schemeEntry.getValue()));
        }
    }

    @Test
    public void convertingSchemeEntityWithInvalidNumericProjectPermissionKeyResultsInNone() {
        SchemeEntity entity = new SchemeEntity(42L, "type", "param", 444, null, 1L);
        assertThat(factory.permissionGrant(entity), equalTo(Option.<PermissionGrant>none()));
    }

    @Test
    public void nonBuiltInHolderTypeFromPluginsIsConverted() {
        SchemeEntity entityFromDb = new SchemeEntity(42L, "typeFromPlugin", "param", "permissionKeyFromPlugin", null, 1L);
        PermissionGrant converted = factory.permissionGrant(entityFromDb).get();
        assertThat(converted.getHolder().getType().getKey(), equalTo("typeFromPlugin"));
        assertThat(converted.getHolder().getParameter(), equalTo(some("param")));
        assertThat(converted.getPermission(), equalTo(new ProjectPermissionKey("permissionKeyFromPlugin")));
    }
}
