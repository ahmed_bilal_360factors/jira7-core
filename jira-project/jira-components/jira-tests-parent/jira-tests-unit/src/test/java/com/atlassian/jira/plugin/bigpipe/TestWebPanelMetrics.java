package com.atlassian.jira.plugin.bigpipe;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.mock.MockFeatureManager;
import com.atlassian.jira.mock.plugin.MockPlugin;
import com.atlassian.jira.mock.servlet.MockHttpServletRequest;
import com.atlassian.jira.mock.servlet.MockHttpServletResponse;
import com.atlassian.jira.servermetrics.ServerMetricsDetailCollector;
import com.atlassian.jira.util.Supplier;
import com.atlassian.jira.web.ExecutingHttpRequest;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.hostcontainer.HostContainer;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugin.web.WebInterfaceManager;
import com.atlassian.plugin.web.model.ResourceTemplateWebPanel;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.Matchers.empty;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.when;

import static org.junit.Assert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;

public class TestWebPanelMetrics {
    @Rule
    public MockitoContainer mockitoMocksInContainer = MockitoMocksInContainer.rule(this);

    private final HostContainer hostContainer = ComponentAccessor::getComponent;

    @Mock
    private ModuleFactory moduleFactory;

    @Mock
    private WebInterfaceManager webInterfaceManager;

    @Mock
    private BigPipeService bigPipeService;

    @AvailableInContainer
    @Mock
    private PluginAccessor pluginAccessor;

    private MockFeatureManager featureManager = new MockFeatureManager();

    @Mock
    private ServerMetricsDetailCollector detailCollector;

    private MockPlugin plugin = new MyMockPlugin();

    @Before
    public void setUpContainer() {
        ResourceTemplateWebPanel resourceTemplateWebPanel = new ResourceTemplateWebPanel(pluginAccessor);
        mockitoMocksInContainer.getMockWorker().addMock(ResourceTemplateWebPanel.class, resourceTemplateWebPanel);

        //Consume supplied content immediately when bigpipe service pipeContent() is called
        doAnswer(args -> args.getArgumentAt(2, Supplier.class).get())
                .when(bigPipeService).pipeContent(anyString(), any(), any());

        //Enable web panel metrics
        featureManager.enable(() -> BigPipeWebPanelModuleDescriptor.FEATURE_WEB_PANEL_METRICS);
    }

    @Before
    public void setUpExecutingHttpRequest() {
        ExecutingHttpRequest.set(new MockHttpServletRequest(), new MockHttpServletResponse());
    }

    @After
    public void tearDownExecutingHttpRequest() {
        ExecutingHttpRequest.clear();
    }

    @Test
    public void metricsCollectedBigpipeDisabled()
            throws Exception {
        featureManager.disable(() -> BigPipeService.FEATURE_BIG_PIPE);
        when(bigPipeService.isBigPipeEnabled()).thenReturn(false);

        List<String> checkpoints = new ArrayList<>();
        doAnswer(args -> checkpoints.add(args.getArgumentAt(0, String.class)))
                .when(detailCollector).checkpointReached(anyString());

        String descriptorXml =
                "      <web-panel key=\"mypanel\" location=\"atl.jira.view.issue.left.context\">\n"
                        + "        <resource name=\"view\" type=\"velocity\" location=\"viewissue/activityblock.vm\"/>\n"
                        + "        <resource name=\"bigPipe\" type=\"velocity\" location=\"viewissue/placeholder.vm\">\n"
                        + "            <param name=\"bigPipeId\" value=\"my-pipe-id\"/>\n"
                        + "        </resource>\n"
                        + "    </web-panel>";

        Element descriptorElement = DocumentHelper.parseText(descriptorXml).getRootElement();

        BigPipeWebPanelModuleDescriptor descriptor = new BigPipeWebPanelModuleDescriptor(hostContainer, moduleFactory, webInterfaceManager, bigPipeService, featureManager, detailCollector);
        descriptor.init(plugin, descriptorElement);
        descriptor.enabled();
        descriptor.getModule().getHtml(Collections.emptyMap());

        assertThat(checkpoints, contains("panel.mypanel.start", "panel.mypanel.finish"));
    }

    @Test
    public void metricsCollectedBigpipeEnabledForBigpipePanel()
            throws Exception {
        featureManager.enable(() -> BigPipeService.FEATURE_BIG_PIPE);
        when(bigPipeService.isBigPipeEnabled()).thenReturn(true);

        List<String> checkpoints = new ArrayList<>();
        doAnswer(args -> checkpoints.add(args.getArgumentAt(0, String.class)))
                .when(detailCollector).checkpointReached(anyString());

        String descriptorXml =
                "      <web-panel key=\"mypanel\" location=\"atl.jira.view.issue.left.context\">\n"
                        + "        <resource name=\"view\" type=\"velocity\" location=\"viewissue/activityblock.vm\"/>\n"
                        + "        <resource name=\"bigPipe\" type=\"velocity\" location=\"viewissue/placeholder.vm\">\n"
                        + "            <param name=\"bigPipeId\" value=\"my-pipe-id\"/>\n"
                        + "        </resource>\n"
                        + "    </web-panel>";

        Element descriptorElement = DocumentHelper.parseText(descriptorXml).getRootElement();

        BigPipeWebPanelModuleDescriptor descriptor = new BigPipeWebPanelModuleDescriptor(hostContainer, moduleFactory,
                webInterfaceManager, bigPipeService, featureManager, detailCollector);
        descriptor.init(plugin, descriptorElement);
        descriptor.enabled();
        descriptor.getModule().getHtml(Collections.emptyMap());

        //These happen out of order because we are mocking bigpipe service and generating content immediately
        assertThat(checkpoints, containsInAnyOrder("placeholder.panel.mypanel.start", "placeholder.panel.mypanel.finish",
                "content.panel.mypanel.start", "content.panel.mypanel.finish"));
    }

    @Test
    public void metricsCollectedBigpipeEnabledForOrdinaryPanel()
            throws Exception {
        featureManager.enable(() -> BigPipeService.FEATURE_BIG_PIPE);
        when(bigPipeService.isBigPipeEnabled()).thenReturn(true);

        List<String> checkpoints = new ArrayList<>();
        doAnswer(args -> checkpoints.add(args.getArgumentAt(0, String.class)))
                .when(detailCollector).checkpointReached(anyString());

        String descriptorXml =
                "      <web-panel key=\"mypanel\" location=\"atl.jira.view.issue.left.context\">\n"
                        + "        <resource name=\"view\" type=\"velocity\" location=\"viewissue/activityblock.vm\"/>\n"
                        + "    </web-panel>";

        Element descriptorElement = DocumentHelper.parseText(descriptorXml).getRootElement();

        BigPipeWebPanelModuleDescriptor descriptor = new BigPipeWebPanelModuleDescriptor(hostContainer, moduleFactory,
                webInterfaceManager, bigPipeService, featureManager, detailCollector);
        descriptor.init(plugin, descriptorElement);
        descriptor.enabled();
        descriptor.getModule().getHtml(Collections.emptyMap());

        assertThat(checkpoints, contains("panel.mypanel.start", "panel.mypanel.finish"));
    }

    @Test
    public void noMetricsCollectedWhenFeatureDisabled()
        throws Exception {

        featureManager.disable(() -> BigPipeService.FEATURE_BIG_PIPE);
        featureManager.disable(() -> BigPipeWebPanelModuleDescriptor.FEATURE_WEB_PANEL_METRICS);
        when(bigPipeService.isBigPipeEnabled()).thenReturn(false);

        List<String> checkpoints = new ArrayList<>();
        doAnswer(args -> checkpoints.add(args.getArgumentAt(0, String.class)))
                .when(detailCollector).checkpointReached(anyString());

        String descriptorXml =
                "      <web-panel key=\"mypanel\" location=\"atl.jira.view.issue.left.context\">\n"
                        + "        <resource name=\"view\" type=\"velocity\" location=\"viewissue/activityblock.vm\"/>\n"
                        + "    </web-panel>";

        Element descriptorElement = DocumentHelper.parseText(descriptorXml).getRootElement();

        BigPipeWebPanelModuleDescriptor descriptor = new BigPipeWebPanelModuleDescriptor(hostContainer, moduleFactory, webInterfaceManager, bigPipeService, featureManager, detailCollector);
        descriptor.init(plugin, descriptorElement);
        descriptor.enabled();
        descriptor.getModule().getHtml(Collections.emptyMap());

        assertThat(checkpoints, empty());
    }

    /**
     * For testing BigPipe we only look up the default priority configurer in the JIRA-standard classloader so we can
     * implement loadClass to do this.
     */
    private static class MyMockPlugin extends MockPlugin {
        @SuppressWarnings("unchecked") //No way to make this safe given API design
        @Override
        public <T> Class<T> loadClass(String clazz, Class<?> callingClass) throws ClassNotFoundException {
            return (Class) Class.forName(clazz, true, callingClass.getClassLoader());
        }
    }
}
