package com.atlassian.jira.jql.operand;

import com.atlassian.jira.jql.query.QueryCreationContext;
import com.atlassian.jira.jql.query.QueryCreationContextImpl;
import com.atlassian.jira.jql.validator.MockJqlOperandResolver;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.MessageSet;
import com.atlassian.jira.util.MessageSetImpl;
import com.atlassian.jira.util.collect.CollectionBuilder;
import com.atlassian.query.clause.TerminalClause;
import com.atlassian.query.clause.TerminalClauseImpl;
import com.atlassian.query.operand.MultiValueOperand;
import com.atlassian.query.operand.Operand;
import com.atlassian.query.operand.SingleValueOperand;
import com.atlassian.query.operator.Operator;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.List;

import static com.atlassian.jira.jql.operand.SimpleLiteralFactory.createLiteral;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestMultiValueOperandHandler {
    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    private String field = "field";
    private ApplicationUser theUser = null;
    private QueryCreationContext queryCreationContext = new QueryCreationContextImpl(theUser);

    @Mock
    private JqlOperandResolver jqlOperandResolver;

    @Test
    public void testValidateWithError() throws Exception {
        final SingleValueOperand singleValueOperand1 = new SingleValueOperand("test");
        final SingleValueOperand singleValueOperand2 = new SingleValueOperand(12L);
        MultiValueOperand multiValueOperand = new MultiValueOperand(CollectionBuilder.newBuilder(singleValueOperand1, singleValueOperand2).asList());
        TerminalClause clause = new TerminalClauseImpl(field, Operator.EQUALS, multiValueOperand);

        final MessageSetImpl messageSet1 = new MessageSetImpl();
        messageSet1.addErrorMessage("Test Error 1");
        final MessageSetImpl messageSet2 = new MessageSetImpl();
        messageSet2.addErrorMessage("Test Error 2");

        when(jqlOperandResolver.validate(theUser, singleValueOperand1, clause)).thenReturn(messageSet1);
        when(jqlOperandResolver.validate(theUser, singleValueOperand2, clause)).thenReturn(messageSet2);

        MultiValueOperandHandler multiValueOperandHandler = new MultiValueOperandHandler(jqlOperandResolver);
        final MessageSet resultMessageSet = multiValueOperandHandler.validate(theUser, multiValueOperand, clause);

        assertTrue(resultMessageSet.hasAnyMessages());
        assertTrue(resultMessageSet.hasAnyErrors());
        assertFalse(resultMessageSet.hasAnyWarnings());
        assertTrue(resultMessageSet.getErrorMessages().contains("Test Error 1"));
        assertTrue(resultMessageSet.getErrorMessages().contains("Test Error 2"));
    }

    @Test
    public void testValidateHappyPath() throws Exception {
        final SingleValueOperand singleValueOperand1 = new SingleValueOperand("test");
        final SingleValueOperand singleValueOperand2 = new SingleValueOperand(12L);
        MultiValueOperand multiValueOperand = new MultiValueOperand(CollectionBuilder.newBuilder(singleValueOperand1, singleValueOperand2).asList());
        TerminalClause clause = new TerminalClauseImpl(field, Operator.EQUALS, multiValueOperand);

        final MessageSetImpl messageSet1 = new MessageSetImpl();
        final MessageSetImpl messageSet2 = new MessageSetImpl();

        when(jqlOperandResolver.validate(theUser, singleValueOperand1, clause)).thenReturn(messageSet1);
        when(jqlOperandResolver.validate(theUser, singleValueOperand2, clause)).thenReturn(messageSet2);

        MultiValueOperandHandler multiValueOperandHandler = new MultiValueOperandHandler(jqlOperandResolver);
        final MessageSet resultMessageSet = multiValueOperandHandler.validate(theUser, multiValueOperand, clause);

        assertFalse(resultMessageSet.hasAnyMessages());
        verify(jqlOperandResolver).validate(theUser, singleValueOperand1, clause);
        verify(jqlOperandResolver).validate(theUser, singleValueOperand2, clause);
    }

    @Test
    public void testGetValuesStrings() {
        MultiValueOperand multiValueOperand = new MultiValueOperand("fred", "jane", "jimi", "123");
        TerminalClause clause = new TerminalClauseImpl(field, Operator.EQUALS, multiValueOperand);

        MultiValueOperandHandler mockMultiValueOperandHandler = new MultiValueOperandHandler(MockJqlOperandResolver.createSimpleSupport());
        List<QueryLiteral> values = mockMultiValueOperandHandler.getValues(queryCreationContext, multiValueOperand, clause);

        final List<QueryLiteral> expectedList = CollectionBuilder.newBuilder(createLiteral("fred"), createLiteral("jane"), createLiteral("jimi"), createLiteral("123")).asList();
        assertEquals(expectedList, values);
    }

    @Test
    public void testGetValuesLongs() {
        MultiValueOperand multiValueOperand = new MultiValueOperand(11L, 1L, 0L, 9999L);
        TerminalClause clause = new TerminalClauseImpl(field, Operator.EQUALS, multiValueOperand);

        MultiValueOperandHandler mockMultiValueOperandHandler = new MultiValueOperandHandler(MockJqlOperandResolver.createSimpleSupport());
        List<QueryLiteral> values = mockMultiValueOperandHandler.getValues(queryCreationContext, multiValueOperand, clause);

        final List<QueryLiteral> expectedList = CollectionBuilder.newBuilder(createLiteral(11L), createLiteral(1L), createLiteral(0L), createLiteral(9999L)).asList();

        assertEquals(expectedList, values);
    }

    @Test
    public void testGetValuesMixture() {
        final SingleValueOperand operandStringy = new SingleValueOperand("stringy");
        final SingleValueOperand operand2010 = new SingleValueOperand(2010L);
        final MultiValueOperand operandMulti1 = new MultiValueOperand(CollectionBuilder.newBuilder(new SingleValueOperand("substring svo"), new SingleValueOperand(333L)).asList());
        final MultiValueOperand operandMulti2 = new MultiValueOperand("sublist", "another");
        List<? extends Operand> operandValues = CollectionBuilder.newBuilder(operandStringy, operand2010, operandMulti1, operandMulti2).asList();

        MultiValueOperand multiValueOperand = new MultiValueOperand(operandValues);
        TerminalClause clause = new TerminalClauseImpl(field, Operator.EQUALS, multiValueOperand);

        MultiValueOperandHandler multiValueOperandHandler = new MultiValueOperandHandler(MockJqlOperandResolver.createSimpleSupport());
        List<QueryLiteral> values = multiValueOperandHandler.getValues(queryCreationContext, multiValueOperand, clause);

        final List<QueryLiteral> expectedList = CollectionBuilder.newBuilder(createLiteral("stringy"), createLiteral(2010L), createLiteral("substring svo"), createLiteral(333L), createLiteral("sublist"), createLiteral("another")).asList();
        assertEquals("sublists should be flattened out into one big list", expectedList, values);
    }

    @Test
    public void testIsList() throws Exception {
        MultiValueOperandHandler multiValueOperandHandler = new MultiValueOperandHandler(jqlOperandResolver);
        assertTrue(multiValueOperandHandler.isList());
    }

    @Test
    public void testIsEmpty() throws Exception {
        MultiValueOperandHandler multiValueOperandHandler = new MultiValueOperandHandler(jqlOperandResolver);
        assertFalse(multiValueOperandHandler.isEmpty());
    }

    @Test
    public void testIsFunction() throws Exception {
        MultiValueOperandHandler multiValueOperandHandler = new MultiValueOperandHandler(jqlOperandResolver);
        assertFalse(multiValueOperandHandler.isFunction());
    }
}
