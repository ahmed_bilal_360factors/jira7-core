package com.atlassian.jira.license;

import com.atlassian.extras.api.jira.JiraLicense;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.datetime.DateTimeFormatter;
import com.atlassian.jira.util.BuildUtilsInfo;
import com.atlassian.jira.util.ConstantClock;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.web.util.ExternalLinkUtil;
import com.google.common.collect.ImmutableSet;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Date;

import static com.atlassian.jira.application.ApplicationKeys.CORE;
import static com.atlassian.jira.application.ApplicationKeys.SERVICE_DESK;
import static com.atlassian.jira.application.ApplicationKeys.SOFTWARE;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.when;

/**
 * @since 7.0
 */
@RunWith(MockitoJUnitRunner.class)
public class TestDefaultLicenseDetailsApplications {
    DefaultLicenseDetails licenseDetails;
    @Mock
    LicensedApplications licensedApplications;
    @Mock
    JiraLicense mockLicense;
    @Mock
    ApplicationProperties applicationProperties;
    @Mock
    ExternalLinkUtil externalLinkUtil;
    @Mock
    BuildUtilsInfo buildUtilsInfo;
    @Mock
    I18nHelper.BeanFactory i18nFactory;
    @Mock
    DateTimeFormatter dateTimeFormatter;

    @Before
    public void init() {
        licenseDetails = new DefaultLicenseDetails(
                new JiraProductLicense(licensedApplications, mockLicense),
                "license_text", applicationProperties, externalLinkUtil, buildUtilsInfo, i18nFactory, dateTimeFormatter,
                new ConstantClock(new Date().getTime()));

        when(licensedApplications.getKeys()).thenReturn(ImmutableSet.of(SERVICE_DESK, CORE));
    }

    @Test
    public void getApplicationsReturnsUnderlyingLicensesApplications() {
        assertThat(licenseDetails.getLicensedApplications().getKeys(), equalTo(ImmutableSet.of(SERVICE_DESK, CORE)));
    }

    @Test
    public void hasApplicationReflectsUnderlyingLicensesApplications() {
        assertThat(licenseDetails.hasApplication(SERVICE_DESK), equalTo(true));
        assertThat(licenseDetails.hasApplication(SOFTWARE), equalTo(false));
        assertThat(licenseDetails.hasApplication(CORE), equalTo(true));
    }
}
