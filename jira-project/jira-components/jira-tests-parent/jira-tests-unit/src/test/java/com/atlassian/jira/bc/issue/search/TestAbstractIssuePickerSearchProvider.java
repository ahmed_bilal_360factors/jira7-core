package com.atlassian.jira.bc.issue.search;

import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.bc.JiraServiceContextImpl;
import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.index.LuceneVersion;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.index.analyzer.EnglishAnalyzer;
import com.atlassian.jira.issue.index.analyzer.TokenFilters;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.issue.search.SearchProvider;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.issue.search.SearchResults;
import com.atlassian.jira.issue.search.util.LuceneQueryModifier;
import com.atlassian.jira.mock.issue.MockIssue;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.jira.web.bean.PagerFilter;
import com.atlassian.query.QueryImpl;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.MatchAllDocsQuery;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TermQuery;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Test for {@link com.atlassian.jira.bc.issue.search.AbstractIssuePickerSearchProvider}.
 *
 * @since v4.2
 */
public class TestAbstractIssuePickerSearchProvider {
    private static final String NO_FILTER_LUCENCEQSTR = "(key:WEB* key:SEARCH*) ((keynumpart:WEB* keynumpart:SEARCH*)^1.5) (summary:web* summary:search*) (summary:web summary:search) key:WEB SEARCH^2.0 keynumpart:WEB SEARCH^1.8";
    private static final String WEB_SEARCH = "web search";

    public static final EnglishAnalyzer ENGLISH_ANALYZER = new EnglishAnalyzer(
            LuceneVersion.get(), false, TokenFilters.English.Stemming.aggressive(),
            TokenFilters.English.StopWordRemoval.defaultSet());

    private JiraServiceContext jiraServiceContext;

    private final AbstractIssuePickerSearchProvider provider = new SimpleProvider();

    @Before
    public void setUp() throws Exception {
        jiraServiceContext = new JiraServiceContextImpl(null, new SimpleErrorCollection());
        provider.summaryAnalyzer = ENGLISH_ANALYZER;
    }

    @Test
    public void testCreateQueryNoQuery() {
        org.apache.lucene.search.Query query = provider.createQuery(null, null, null);
        assertNull(query);
    }

    @Test
    public void testCreateQuerySingleTerm() {
        final Set<String> keyTerms = new LinkedHashSet<>();
        final Set<String> summaryTerms = new LinkedHashSet<>();
        org.apache.lucene.search.Query query = provider.createQuery("web", keyTerms, summaryTerms);
        assertNotNull(query);
        assertThat(query.toString(), equalTo("(key:WEB*) ((keynumpart:WEB*)^1.5) (summary:web*) (summary:web) key:WEB^2.0 keynumpart:WEB^1.8"));
        assertThat(keyTerms, equalTo(Collections.singleton("WEB")));
        assertThat(summaryTerms, equalTo(Collections.singleton("web")));
    }

    @Test
    public void testCreateQueryMultipleTerms() {
        final Set<String> keyTerms = new LinkedHashSet<>();
        final Set<String> summaryTerms = new LinkedHashSet<>();

        org.apache.lucene.search.Query query = provider.createQuery(WEB_SEARCH, keyTerms, summaryTerms);
        assertNotNull(query);
        assertThat(query.toString(), equalTo(NO_FILTER_LUCENCEQSTR));

        assertCollectionEquals(Arrays.asList("WEB", "SEARCH"), keyTerms);
        assertCollectionEquals(Arrays.asList("web", "search"), summaryTerms);
    }

    @Test
    public void testCreateNullFilterQuery() {
        IssuePickerSearchService.IssuePickerParameters params = new IssuePickerSearchService.IssuePickerParameters(null, null, null, null, true, true, 50);
        org.apache.lucene.search.Query query = provider.addFilterToQuery(params, null);
        assertNull(query);
    }

    @Test
    public void testCreateEmptyFilterQuery() {
        IssuePickerSearchService.IssuePickerParameters params = new IssuePickerSearchService.IssuePickerParameters(null, null, null, null, true, true, 50);
        BooleanQuery origQuery = new BooleanQuery();
        org.apache.lucene.search.Query query = provider.addFilterToQuery(params, origQuery);
        assertNotNull(query);
        assertThat(query.toString(), equalTo("+()"));
    }

    @Test
    public void testCreateEmptyFilterQueryWithNonEmptyOrig() {
        IssuePickerSearchService.IssuePickerParameters params = new IssuePickerSearchService.IssuePickerParameters(null, null, null, null, true, true, 50);
        BooleanQuery origQuery = new BooleanQuery();
        origQuery.add(new TermQuery(new Term("key", "KEY")), BooleanClause.Occur.MUST);
        org.apache.lucene.search.Query query = provider.addFilterToQuery(params, origQuery);
        assertNotNull(query);
        assertThat(query.toString(), equalTo("+(+key:KEY)"));
    }

    @Test
    public void testCreateFilterQueryWithNonEmptyOrigAndIssue() {
        Issue issue = getMockIssueWithParent("JRA-123", "JRA-123 Summary", null);
        IssuePickerSearchService.IssuePickerParameters params = new IssuePickerSearchService.IssuePickerParameters(null, null, issue, null, true, true, 50);
        BooleanQuery origQuery = new BooleanQuery();
        origQuery.add(new TermQuery(new Term("key", "KEY")), BooleanClause.Occur.MUST);
        org.apache.lucene.search.Query query = provider.addFilterToQuery(params, origQuery);
        assertNotNull(query);
        assertThat(query.toString(), equalTo("+(+key:KEY) -key:JRA-123"));
    }

    @Test
    public void testCreateFilterQueryWithNonEmptyOrigAndProject() {
        Project project = new MockProject(123L);
        Issue issue = getMockIssueWithParent("JRA-123", "JRA-123 Summary", null);
        IssuePickerSearchService.IssuePickerParameters params = new IssuePickerSearchService.IssuePickerParameters(null, null, issue, project, true, true, 50);
        BooleanQuery origQuery = new BooleanQuery();
        origQuery.add(new TermQuery(new Term("key", "KEY")), BooleanClause.Occur.MUST);
        org.apache.lucene.search.Query query = provider.addFilterToQuery(params, origQuery);
        assertNotNull(query);
        assertThat(query.toString(), equalTo("+(+key:KEY) -key:JRA-123 +projid:123"));
    }

    @Test
    public void testCreateFilterQueryWithNonEmptyOrigAndshowParentFalse() {
        Project project = new MockProject(123L);
        Issue issue = getMockIssueWithParent("JRA-123", "JRA-123 Summary", getMockIssue("JRA-321", "PArent Issue"));
        IssuePickerSearchService.IssuePickerParameters params = new IssuePickerSearchService.IssuePickerParameters(null, null, issue, project, true, false, 50);
        BooleanQuery origQuery = new BooleanQuery();
        origQuery.add(new TermQuery(new Term("key", "KEY")), BooleanClause.Occur.MUST);
        org.apache.lucene.search.Query query = provider.addFilterToQuery(params, origQuery);
        assertNotNull(query);
        assertThat(query.toString(), equalTo("+(+key:KEY) -key:JRA-123 +projid:123 -key:JRA-321"));
    }

    @Test
    public void testCreateFilterQueryWithNonEmptyOrigAndshowSubsFalse() {
        IssueType it1 = mock(IssueType.class);
        when(it1.getId()).thenReturn("1234");
        IssueType it2 = mock(IssueType.class);
        when(it2.getId()).thenReturn("9876");

        ConstantsManager cMgr = mock(ConstantsManager.class);
        when(cMgr.getRegularIssueTypeObjects()).thenReturn(Arrays.asList(it1, it2));

        AbstractIssuePickerSearchProvider provider = new SimpleProvider(null, cMgr, null);

        Issue issue = getMockIssueWithParent("JRA-123", "JRA-123 Summary", getMockIssue("JRA-321", "PArent Issue"));
        IssuePickerSearchService.IssuePickerParameters params = new IssuePickerSearchService.IssuePickerParameters(null, null, issue, null, false, true, 50);
        BooleanQuery origQuery = new BooleanQuery();
        origQuery.add(new TermQuery(new Term("key", "KEY")), BooleanClause.Occur.MUST);
        org.apache.lucene.search.Query query = provider.addFilterToQuery(params, origQuery);
        assertNotNull(query);
        assertThat(query.toString(), equalTo("+(+key:KEY) -key:JRA-123 +(type:1234 type:9876)"));
    }

    @Test
    public void testGetIssues() throws Exception {
        final int maxCount = 20;

        //The Query created from the "query" given by the user.
        final Query createQuery = new TermQuery(new Term("a", "b"));

        //The query created after the filter terms have been added.
        final BooleanQuery createQueryWithFilter = new BooleanQuery();
        createQueryWithFilter.add(createQuery, BooleanClause.Occur.MUST);
        createQueryWithFilter.add(new TermQuery(new Term("c", "c")), BooleanClause.Occur.MUST);

        //The query created after passed through the modifier to add the "MATCH ALL" term.
        final BooleanQuery modifiedQuery = new BooleanQuery();
        modifiedQuery.add(new MatchAllDocsQuery(), BooleanClause.Occur.MUST);
        modifiedQuery.add(createQueryWithFilter, BooleanClause.Occur.MUST);

        final List<String> keyTermsExpected = Arrays.asList("a", "b");
        final List<String> summaryTermsExpected = Arrays.asList("c", "d");

        //The list of issues to return.
        final List<Issue> issueList = new ArrayList<>();
        issueList.add(getMockIssue("JRA-123", "JRA-123 Summary"));
        issueList.add(getMockIssue("JRA-124", "JRA-124 Summary"));
        issueList.add(getMockIssue("JRA-125", "JRA-125 Summary"));

        //The parameters we will be testing.
        final IssuePickerSearchService.IssuePickerParameters testParams = new IssuePickerSearchService.IssuePickerParameters(WEB_SEARCH, null, null, null, true, true, maxCount);

        //The results returned from the search service.
        final SearchResults searchResults = new SearchResults(issueList, new PagerFilter<>(maxCount));

        //Prime the modifier with expected behaviour.
        final LuceneQueryModifier modifier = mock(LuceneQueryModifier.class);
        when(modifier.getModifiedQuery(createQueryWithFilter)).thenReturn(modifiedQuery);

        //Prime the search provider with expected behaviour.
        final SearchProvider mockSearchProvider = mock(SearchProvider.class);
        ArgumentCaptor<PagerFilter> pager = ArgumentCaptor.forClass(PagerFilter.class);
        when(mockSearchProvider.search(eq(new QueryImpl()), isNull(ApplicationUser.class), pager.capture(), eq(modifiedQuery))).thenReturn(searchResults);

        AbstractIssuePickerSearchProvider provider = new SimpleProvider(mockSearchProvider, null, modifier) {
            @Override
            Query createQuery(final String query, final Collection<String> keyTerms, final Collection<String> summaryTerms) {
                assertThat(query, equalTo(WEB_SEARCH));

                keyTerms.addAll(keyTermsExpected);
                summaryTerms.addAll(summaryTermsExpected);
                return createQuery;
            }

            @Override
            Query addFilterToQuery(final IssuePickerSearchService.IssuePickerParameters issuePickerParams, final Query filterQuery) {
                assertThat(filterQuery, equalTo(createQuery));
                return createQueryWithFilter;
            }
        };

        // we have to use this as the default one we should use is DB dependant
        provider.summaryAnalyzer = ENGLISH_ANALYZER;

        IssuePickerResults results = provider.getResults(jiraServiceContext, testParams, maxCount);

        assertNotNull(results);
        assertThat(provider.getLabelKey(), equalTo(results.getLabel()));
        assertThat(pager.getValue().getMax(), equalTo(20));
        assertNotNull(results.getIssues());
        assertCollectionEquals(issueList, results.getIssues());
        assertCollectionEquals(keyTermsExpected, results.getKeyTerms());
        assertCollectionEquals(summaryTermsExpected, results.getSummaryTerms());
    }

    private <T> void assertCollectionEquals(Collection<T> expected, Collection<? extends T> actual) {
        assertThat(actual, contains(expected.toArray()));
    }

    private Issue getMockIssue(final String key, final String summary) {
        return new MockIssue(999L) {
            public String getKey() {
                return key;
            }

            public String getSummary() {
                return summary;
            }
        };
    }

    private Issue getMockIssueWithParent(final String key, final String summary, final Issue parent) {
        return new MockIssue(999L) {
            public String getKey() {
                return key;
            }

            public String getSummary() {
                return summary;
            }

            public Issue getParentObject() {
                return parent;
            }
        };
    }

    private static class SimpleProvider extends AbstractIssuePickerSearchProvider {
        private SimpleProvider(final SearchProvider searchProvider, final ConstantsManager constantsManager, final LuceneQueryModifier modifier) {
            super(searchProvider, constantsManager, modifier);
        }

        private SimpleProvider() {
            super(null, null, null);
        }

        @Override
        protected String getLabelKey() {
            return "label.ley";
        }

        @Override
        protected String getId() {
            return "label.id";
        }

        @Override
        protected SearchRequest getRequest(final IssuePickerSearchService.IssuePickerParameters issuePickerParams) {
            return new SearchRequest(new QueryImpl());
        }

        public boolean handlesParameters(final ApplicationUser searcher, final IssuePickerSearchService.IssuePickerParameters issuePickerParams) {
            return false;
        }
    }
}
