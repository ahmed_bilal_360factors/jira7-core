package com.atlassian.jira.issue.security;

import com.atlassian.jira.bc.ServiceResult;
import com.atlassian.jira.mock.i18n.MockI18nHelper;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.I18nHelper;
import com.google.common.collect.ImmutableMap;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestIssueSecuritySchemeService {
    @Mock
    private GlobalPermissionManager globalPermissionManager;
    @Mock
    private IssueSecurityLevelManager issueSecurityLevelManager;
    @Mock
    private IssueSecuritySchemeManager issueSecuritySchemeManager;
    @Mock
    private PermissionManager permissionManager;
    @Mock
    private ApplicationUser user;
    @Mock
    private ProjectManager projectManager;

    private I18nHelper i18n = new MockI18nHelper();
    private IssueSecuritySchemeService service;

    @Before
    public void setUp() throws Exception {
        when(globalPermissionManager.hasPermission(eq(GlobalPermissionKey.ADMINISTER), any(ApplicationUser.class))).thenReturn(true);
        service = new IssueSecuritySchemeServiceImpl(issueSecuritySchemeManager, issueSecurityLevelManager, globalPermissionManager, permissionManager, i18n, projectManager);
    }

    @Test
    public void testGetIssueSecurityLevelsReturnsManagerResults() throws Exception {
        when(issueSecuritySchemeManager.getIssueSecurityLevelScheme(0l)).thenReturn(new IssueSecurityLevelScheme(0l, "", "", null));
        List<IssueSecurityLevel> expected = Collections.emptyList();
        when(issueSecurityLevelManager.getIssueSecurityLevels(0)).thenReturn(expected);

        List<IssueSecurityLevel> result = service.getIssueSecurityLevels(user, 0).get();

        assertThat(result, hasSize(0));
    }

    @Test
    public void testGetIssueSecurityLevelSchemeReturnsManagerResults() throws Exception {
        IssueSecurityLevelScheme expected = new IssueSecurityLevelScheme(0l, "", "", null);
        when(issueSecuritySchemeManager.getIssueSecurityLevelScheme(0l)).thenReturn(expected);

        IssueSecurityLevelScheme result = service.getIssueSecurityLevelScheme(user, 0).get();

        assertThat(result, equalTo(expected));
    }

    @Test
    public void testGetIssueSecurityLevelSchemesReturnsManagerResults() throws Exception {
        Collection<IssueSecurityLevelScheme> expected = Collections.emptyList();
        when(issueSecuritySchemeManager.getIssueSecurityLevelSchemes()).thenReturn(expected);

        Collection<IssueSecurityLevelScheme> result = service.getIssueSecurityLevelSchemes(user).get();

        assertThat(result, hasSize(0));
    }

    @Test
    public void testGetPermissionsForSecurityLevelReturnsManagerResults() throws Exception {
        when(issueSecurityLevelManager.getSecurityLevel(0)).thenReturn(new IssueSecurityLevelImpl(0l, "", "", 0l));

        List<IssueSecurityLevelPermission> expected = Collections.emptyList();
        when(issueSecuritySchemeManager.getPermissionsBySecurityLevel(0l)).thenReturn(expected);

        Collection<IssueSecurityLevelPermission> result = service.getPermissionsByIssueSecurityLevel(user, 0).get();

        assertThat(result, hasSize(0));
    }

    @Test
    public void testGetSecurityLevelReturnsManagerResults() throws Exception {
        IssueSecurityLevel expected = new IssueSecurityLevelImpl(0l, "", "", 0l);
        when(issueSecurityLevelManager.getSecurityLevel(0)).thenReturn(expected);

        IssueSecurityLevel result = service.getIssueSecurityLevel(user, 0).get();

        assertThat(result, equalTo(expected));
    }

    @Test
    public void testAssignSchemeToProjectReturnsManagerResults() throws Exception {
        Project p = mock(Project.class);
        when(projectManager.getProjectObj(1L)).thenReturn(p);
        when(issueSecuritySchemeManager.assignSchemeToProject(p, 1L, ImmutableMap.of())).thenReturn("return url");
        List<IssueSecurityLevel> expected = Collections.emptyList();
        when(issueSecurityLevelManager.getIssueSecurityLevels(0)).thenReturn(expected);

        String result = service.assignSchemeToProject(user, 1L, 1L, ImmutableMap.of()).get();
        assertThat(result, is("return url"));
    }

    @Test
    public void testAnyCallWhenNotAdminYieldsForbidden() {
        when(globalPermissionManager.hasPermission(eq(GlobalPermissionKey.ADMINISTER), any(ApplicationUser.class))).thenReturn(false);
        assertGotForbidden(service.getIssueSecurityLevels(user, 0));
        assertGotForbidden(service.getIssueSecurityLevelScheme(user, 0));
        assertGotForbidden(service.getIssueSecurityLevelSchemes(user));
        assertGotForbidden(service.getPermissionsByIssueSecurityLevel(user, 0));
        assertGotForbidden(service.getIssueSecurityLevel(user, 0));

        Project p = mock(Project.class);
        when(projectManager.getProjectObj(1L)).thenReturn(p);
        assertGotForbidden(service.assignSchemeToProject(user, 1L, 1L, ImmutableMap.of()));
    }

    @Test
    public void testAnyCallWhenNotAdminButProjectAdminDoesNotYieldForbidden() {
        when(globalPermissionManager.hasPermission(eq(GlobalPermissionKey.ADMINISTER), any(ApplicationUser.class))).thenReturn(false);

        Project p = mock(Project.class);
        when(permissionManager.getProjects(eq(ProjectPermissions.ADMINISTER_PROJECTS), any(ApplicationUser.class))).thenReturn(Arrays.asList(p));
        when(issueSecuritySchemeManager.getProjectsUsingScheme(0)).thenReturn(Arrays.asList(p));

        assertNotForbidden(service.getIssueSecurityLevelScheme(user, 0));
        assertNotForbidden(service.getIssueSecurityLevels(user, 0));
        assertNotForbidden(service.assignSchemeToProject(user, 1L, 1L, ImmutableMap.of()));
    }

    private void assertNotForbidden(ServiceResult result) {
        assertFalse(result.getErrorCollection().getReasons().contains(ErrorCollection.Reason.FORBIDDEN));
    }

    private void assertGotForbidden(ServiceResult result) {
        assertTrue(result.getErrorCollection().getReasons().contains(ErrorCollection.Reason.FORBIDDEN));
    }
}
