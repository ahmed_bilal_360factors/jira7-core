package com.atlassian.jira.upgrade.tasks.role;

import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.jira.CachingComponent;
import com.atlassian.jira.application.ApplicationKeys;
import com.atlassian.jira.application.ApplicationRoleManager;
import com.atlassian.jira.mock.ofbiz.MockOfBizDelegator;
import com.atlassian.jira.ofbiz.FieldMap;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.junit.Test;
import org.ofbiz.core.entity.GenericValue;

import javax.annotation.Nullable;
import java.util.Set;
import java.util.stream.Collectors;

import static com.atlassian.jira.application.ApplicationKeys.CORE;
import static com.atlassian.jira.application.ApplicationKeys.SOFTWARE;
import static com.atlassian.jira.upgrade.tasks.role.TestUtils.asGroups;
import static com.atlassian.jira.upgrade.tasks.role.TestUtils.newGroup;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.withSettings;

/**
 * @since 7.0
 */
public class ApplicationRolesDaoImplTest {
    private MockOfBizDelegator delegator = new MockOfBizDelegator();

    private ApplicationRoleManager applicationRoleManager = mock(ApplicationRoleManager.class,
            withSettings().extraInterfaces(CachingComponent.class));

    private ApplicationRolesDaoImpl dao = new ApplicationRolesDaoImpl(delegator, applicationRoleManager);

    @Test
    public void getParsesGoodDatabaseCorrectly() {
        //given
        ApplicationRole coreUsers = ApplicationRole.forKey(CORE).addGroup(newGroup("core-users"));
        ApplicationRole swUsers = ApplicationRole.forKey(SOFTWARE)
                .addGroupAsDefault(newGroup("software-users"))
                .addGroup(newGroup("jira-users"));

        setRoles(coreUsers, swUsers);

        //when
        final ApplicationRoles applicationRoles = dao.get();

        //then
        assertThat(applicationRoles, new ApplicationRolesMatcher()
                .role(ApplicationKeys.CORE, asGroups("core-users"), asGroups())
                .role(ApplicationKeys.SOFTWARE, asGroups("software-users", "jira-users"), asGroups("software-users")));
    }

    @Test
    public void getIngoresRowsWithInvalidApplicationKey() {
        //given
        ApplicationRole coreUsers = ApplicationRole.forKey(CORE).addGroup(newGroup("core-users"));
        setRoles(coreUsers);
        new Row("&Bad_Role_Key&*", "group", true).save(delegator);

        //when
        final ApplicationRoles applicationRoles = dao.get();

        //then
        assertThat(applicationRoles, new ApplicationRolesMatcher()
                .role(ApplicationKeys.CORE, asGroups("core-users"), ImmutableSet.of()));
    }

    @Test
    public void getIgnoresRowsWithInvalidGroups() {
        //given
        ApplicationRole coreUsers = ApplicationRole.forKey(CORE).addGroup(newGroup("core-users"));
        setRoles(coreUsers);
        new Row(ApplicationKeys.CORE.value(), null, null).save(delegator);

        //when
        final ApplicationRoles applicationRoles = dao.get();

        //then
        assertThat(applicationRoles, new ApplicationRolesMatcher()
                .role(ApplicationKeys.CORE, asGroups("core-users"), ImmutableSet.of()));
    }

    @Test
    public void getConsidersNullDefaultColumnsAsFalse() {
        //given
        ApplicationRole coreUsers = ApplicationRole.forKey(CORE).addGroupAsDefault(newGroup("core-users"));
        setRoles(coreUsers);
        new Row(ApplicationKeys.CORE.value(), "jack-users", null).save(delegator);

        //when
        final ApplicationRoles applicationRoles = dao.get();

        //then
        assertThat(applicationRoles, new ApplicationRolesMatcher().role(ApplicationKeys.CORE,
                asGroups("core-users", "jack-users"), asGroups("core-users")));
    }

    @Test
    public void putAddsRowsForNewRole() {
        //given
        ApplicationRole softwareUsers = ApplicationRole.forKey(SOFTWARE)
                .addGroupAsDefault(newGroup("software-users"))
                .addGroup(newGroup("jira-users"));

        //when
        dao.put(new ApplicationRoles(ImmutableList.of(softwareUsers)));

        //then
        Set<Row> expectedRows = ImmutableSet.of(new Row(ApplicationKeys.SOFTWARE.value(), "software-users", true),
                new Row(ApplicationKeys.SOFTWARE.value(), "jira-users", false));
        assertThat(getRoleRows(), equalTo(expectedRows));
        assertCacheCleared();
    }

    @Test
    public void putAddsNewGroupIdentifierInLowerCase() {
        //given
        ApplicationRole softwareUsers = ApplicationRole.forKey(SOFTWARE)
                .addGroupAsDefault(newGroup("Software-users"))
                .addGroup(newGroup("JIRA-users"));

        //when
        dao.put(new ApplicationRoles(ImmutableList.of(softwareUsers)));

        //then
        Set<Row> expectedRows = ImmutableSet.of(new Row(ApplicationKeys.SOFTWARE.value(), "software-users", true),
                new Row(ApplicationKeys.SOFTWARE.value(), "jira-users", false));
        assertThat(getRoleRows(), equalTo(expectedRows));
    }

    @Test
    public void putCorrectsCaseOfAlreadyStoredGroupIfItIsIncorrect() {
        //given
        setRoles(ApplicationRole.forKey(SOFTWARE)
                .addGroup(newGroup("JIRA-UseRS"))
                .addGroup(newGroup("software-UseRS")));

        ApplicationRole softwareUsers = ApplicationRole.forKey(SOFTWARE)
                .addGroupAsDefault(newGroup("software-users"))
                .addGroup(newGroup("jira-users"));

        //when
        dao.put(new ApplicationRoles(ImmutableList.of(softwareUsers)));

        //then
        Set<Row> expectedRows = ImmutableSet.of(new Row(ApplicationKeys.SOFTWARE.value(), "software-users", true),
                new Row(ApplicationKeys.SOFTWARE.value(), "jira-users", false));
        assertThat(getRoleRows(), equalTo(expectedRows));
    }


    @Test
    public void putCanAddGroup() {
        //given
        setRole(ApplicationRole.forKey(SOFTWARE)
                .addGroupAsDefault(newGroup("software-users"))
                .addGroup(newGroup("jira-users")));
        ApplicationRole updatedSoftware = ApplicationRole.forKey(SOFTWARE)
                .addGroupAsDefault(newGroup("software-users"))
                .addGroup(newGroup("jira-users"))
                .addGroup(newGroup("jira-developers"));

        //when
        dao.put(new ApplicationRoles(ImmutableList.of(updatedSoftware)));

        //then
        assertThat(getRoleRows(), equalTo(Row.toRows(updatedSoftware)));
        assertCacheCleared();
    }

    @Test
    public void putCanAddDefaultGroup() {
        //given
        setRole(ApplicationRole.forKey(SOFTWARE)
                .addGroupAsDefault(newGroup("software-users"))
                .addGroup(newGroup("jira-users")));
        ApplicationRole updatedSoftware = ApplicationRole.forKey(SOFTWARE)
                .addGroupAsDefault(newGroup("software-users"))
                .addGroup(newGroup("jira-users"))
                .addGroupAsDefault(newGroup("jira-developers"));

        //when
        dao.put(new ApplicationRoles(ImmutableList.of(updatedSoftware)));

        //then
        assertThat(getRoleRows(), equalTo(Row.toRows(updatedSoftware)));
        assertCacheCleared();
    }

    @Test
    public void putCanMakeGroupDefault() {
        //given
        setRole(ApplicationRole.forKey(SOFTWARE)
                .addGroupAsDefault(newGroup("software-users"))
                .addGroup(newGroup("jira-users")));
        ApplicationRole updatedSoftware = ApplicationRole.forKey(SOFTWARE)
                .addGroupAsDefault(newGroup("software-users"))
                .addGroupAsDefault(newGroup("jira-users"));

        //when
        dao.put(new ApplicationRoles(ImmutableList.of(updatedSoftware)));

        //then
        assertThat(getRoleRows(), equalTo(Row.toRows(updatedSoftware)));
        assertCacheCleared();
    }

    @Test
    public void putCanMakeGroupDefaultEvenWhenColumnIsNull() {
        //given
        new Row(ApplicationKeys.SOFTWARE.value(), "jira-users", null).save(delegator);
        ApplicationRole updatedSoftware = ApplicationRole.forKey(SOFTWARE).addGroupAsDefault(newGroup("jira-users"));

        //when
        dao.put(new ApplicationRoles(ImmutableList.of(updatedSoftware)));

        //then
        assertThat(getRoleRows(), equalTo(Row.toRows(updatedSoftware)));
        assertCacheCleared();
    }

    @Test
    public void putWillSetNullDefaultColumnToFalseWhenRowNotDefault() {
        //given
        new Row(ApplicationKeys.SOFTWARE.value(), "jira-users", null).save(delegator);
        ApplicationRole updatedSoftware = ApplicationRole.forKey(SOFTWARE).addGroup(newGroup("jira-users"));

        //when
        dao.put(new ApplicationRoles(ImmutableList.of(updatedSoftware)));

        //then
        assertThat(getRoleRows(), equalTo(Row.toRows(updatedSoftware)));
        assertCacheCleared();
    }

    @Test
    public void putAbleToRemoveRows() {
        //given
        setRole(ApplicationRole.forKey(SOFTWARE).addGroupAsDefault(newGroup("software-users"))
                .addGroupAsDefault(newGroup("jira-users")));
        ApplicationRole updatedSoftware = ApplicationRole.forKey(SOFTWARE)
                .addGroupAsDefault(newGroup("jira-users"))
                .addGroupAsDefault(newGroup("jira-developers"));

        //when
        dao.put(new ApplicationRoles(ImmutableList.of(updatedSoftware)));

        //then
        assertThat(getRoleRows(), equalTo(Row.toRows(updatedSoftware)));
        assertCacheCleared();
    }

    @Test
    public void putCleansOutRowsWithNullGroups() {
        //given
        final ApplicationRole softwareRole = ApplicationRole.forKey(SOFTWARE)
                .addGroupAsDefault(newGroup("software-users"))
                .addGroupAsDefault(newGroup("jira-users"));
        setRole(softwareRole);
        new Row(ApplicationKeys.SOFTWARE.value(), null, true).save(delegator);

        //when
        dao.put(new ApplicationRoles(ImmutableList.of(softwareRole)));

        //then
        assertThat(getRoleRows(), equalTo(Row.toRows(softwareRole)));
        assertCacheCleared();
    }

    @Test
    public void putLeavesInvalidUnrelatedRowsAlone() {
        //given
        final Row invalidRow = new Row("&_Bad_ID*", null, true);
        final ApplicationRole softwareRole = ApplicationRole.forKey(SOFTWARE)
                .addGroupAsDefault(newGroup("software-users"))
                .addGroupAsDefault(newGroup("jira-users"));
        setRole(softwareRole);
        invalidRow.save(delegator);

        //when
        dao.put(new ApplicationRoles(ImmutableList.of(softwareRole)));

        //then
        assertThat(getRoleRows(), equalTo(Sets.union(Row.toRows(softwareRole), ImmutableSet.of(invalidRow))));
        assertCacheCleared();
    }

    @Test
    public void putErrorsOutWhenTryingToDeleteApplicationRole() {
        //given
        final ApplicationRole coreRole = ApplicationRole.forKey(CORE).addGroupAsDefault(newGroup("something"));
        final ApplicationRole softwareRole = ApplicationRole.forKey(SOFTWARE)
                .addGroupAsDefault(newGroup("software-users"))
                .addGroupAsDefault(newGroup("jira-users"));

        setRoles(softwareRole, coreRole);

        try {
            //when
            dao.put(new ApplicationRoles(ImmutableList.of(softwareRole)));

            fail("Expecting an error during migration.");
        } catch (MigrationFailedException ignored) {
            //good.
        }

        //Cache should be cleared even on error to ensure that partial write is shadowed by cache.
        assertCacheCleared();
    }

    @Test
    public void putWorksWithMultipleRoles() {
        //given
        setRole(ApplicationRole.forKey(CORE).addGroupAsDefault(newGroup("something")));
        setRole(ApplicationRole.forKey(SOFTWARE)
                .addGroupAsDefault(newGroup("software-users"))
                .addGroupAsDefault(newGroup("jira-users")));

        ApplicationRole coreRole = ApplicationRole.forKey(CORE)
                .addGroupAsDefault(newGroup("jira-core-users"))
                .addGroup(newGroup("jira-users"));

        ApplicationRole softwareRole = ApplicationRole.forKey(SOFTWARE).addGroupAsDefault(newGroup("jira-software-users"));

        //when
        dao.put(new ApplicationRoles(ImmutableList.of(softwareRole, coreRole)));

        //then.
        assertThat(getRoleRows(), equalTo(Sets.union(Row.toRows(softwareRole), Row.toRows(coreRole))));
        assertCacheCleared();
    }

    @Test(expected = IllegalArgumentException.class)
    public void cotrFailsIfApplicationRoleManagerNotACache() {
        new ApplicationRolesDaoImpl(delegator, mock(ApplicationRoleManager.class));
    }

    private void assertCacheCleared() {
        final CachingComponent cachingComponent = (CachingComponent) this.applicationRoleManager;
        verify(cachingComponent).clearCache();
    }

    private void setRoles(ApplicationRole... roles) {
        for (ApplicationRole role : roles) {
            setRole(role);
        }
    }

    private void setRole(ApplicationRole role) {
        final Set<Group> defaults = role.defaultGroups();
        for (Group group : role.groups()) {
            new Row(role.key().value(), group.getName(), defaults.contains(group)).save(delegator);
        }
    }

    private Set<Row> getRoleRows() {
        return Row.findAll(delegator);
    }

    private static class Row {
        private static final String APPLICATION_ROLE_ENTITY = "LicenseRoleGroup";

        private static final String NAME = "licenseRoleName";
        private static final String GROUP_ID = "groupId";
        private static final String DEFAULT = "primaryGroup";

        @Nullable
        private final String key;
        @Nullable
        private final String groupName;
        @Nullable
        private Boolean isDefault;

        private Row(@Nullable final String key, @Nullable final String groupName, @Nullable final Boolean isDefault) {
            this.key = key;
            this.groupName = groupName;
            this.isDefault = isDefault;
        }

        private Row(GenericValue value) {
            this(value.getString(NAME), value.getString(GROUP_ID), value.getBoolean(DEFAULT));
        }

        private GenericValue save(OfBizDelegator delegator) {
            return delegator.createValue(APPLICATION_ROLE_ENTITY, FieldMap.build(NAME, key, GROUP_ID, groupName,
                    DEFAULT, isDefault));
        }

        @Override
        public boolean equals(@Nullable final Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }

            final Row row = (Row) o;

            if (key != null ? !key.equals(row.key) : row.key != null) {
                return false;
            }
            if (groupName != null ? !groupName.equals(row.groupName) : row.groupName != null) {
                return false;
            }
            return !(isDefault != null ? !isDefault.equals(row.isDefault) : row.isDefault != null);

        }

        @Override
        public int hashCode() {
            int result = key != null ? key.hashCode() : 0;
            result = 31 * result + (groupName != null ? groupName.hashCode() : 0);
            result = 31 * result + (isDefault != null ? isDefault.hashCode() : 0);
            return result;
        }

        @Override
        public String toString() {
            return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
                    .append("key", key)
                    .append("groupName", groupName)
                    .append("isDefault", isDefault)
                    .toString();
        }

        private static Set<Row> findAll(OfBizDelegator delegator) {
            return delegator.findAll(APPLICATION_ROLE_ENTITY).stream().map(Row::new).collect(Collectors.toSet());
        }

        private static Set<Row> toRows(ApplicationRole role) {
            final Set<Group> defaults = role.defaultGroups();
            return role.groups().stream().map(g -> new Row(role.key().value(), g.getName(),
                    defaults.contains(g))).collect(Collectors.toSet());
        }
    }
}