package com.atlassian.jira.upgrade.tasks;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.database.MockDbConnectionManager;
import com.atlassian.jira.database.ResultRow;
import com.atlassian.jira.mock.component.MockComponentWorker;
import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableList;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class TestUpgradeTask_Build70025 {
    private final MockDbConnectionManager dbConnectionManager = new MockDbConnectionManager();

    private final UpgradeTask_Build70025 upgradeTask = new UpgradeTask_Build70025(dbConnectionManager);

    private final ImmutableList<Long> propertyIds = ImmutableList.of(10001L, 10002L, 10003L);

    private final Long MIN_ID = 10000L;
    private final int MAX_ROWS_IN_BATCH = 900;
    private final int EXPECTED_TESTING_ROWS = 915;

    @Before
    public void setUp() {
        ComponentAccessor.initialiseWorker(new MockComponentWorker());
    }

    @Test
    public void shouldExecuteCorrectDeleteQueryOnceWhenRowsLessThanBatchSize() throws Exception {
        queryForMinusOneUserLocalePropertyIds(propertyIds);
        queryForDeleteMinusOneUserLocaleProperty(propertyIds);

        upgradeTask.doUpgrade(false);

        dbConnectionManager.assertAllExpectedStatementsWereRun();
    }

    @Test
    public void shouldExecuteCorrectDeleteQueryOnceWhenRowsEqualBatchSize() throws Exception {
        List<Long> resultRows = getPropertyIdsByBatchSize(MAX_ROWS_IN_BATCH);

        queryForMinusOneUserLocalePropertyIds(resultRows);
        queryForDeleteMinusOneUserLocaleProperty(resultRows);

        upgradeTask.doUpgrade(false);

        dbConnectionManager.assertAllExpectedStatementsWereRun();
    }

    @Test
    public void shouldNotExecuteDeleteQueryWhenNoRowsFound() throws Exception {
        queryForMinusOneUserLocalePropertyIds(new ArrayList<>());

        upgradeTask.doUpgrade(false);

        dbConnectionManager.assertAllExpectedStatementsWereRun();
    }

    @Test
    public void shouldSplitMultipleQueryWhenRowsMoreThanBatchSize() throws Exception {
        List<Long> resultRows = getPropertyIdsByBatchSize(EXPECTED_TESTING_ROWS);

        queryForMinusOneUserLocalePropertyIds(resultRows);

        List<Long> firstIdsList = new ArrayList<>();
        for (int i = 1; i <= MAX_ROWS_IN_BATCH; i++) {
            Long id = MIN_ID + i;
            firstIdsList.add(id);
        }
        List<Long> secondIdsList = new ArrayList<>();
        for (int i = MAX_ROWS_IN_BATCH + 1; i < EXPECTED_TESTING_ROWS; i++) {
            Long id = MIN_ID + i;
            secondIdsList.add(id);
        }

        queryForDeleteMinusOneUserLocaleProperty(firstIdsList);
        queryForDeleteMinusOneUserLocaleProperty(secondIdsList);

        upgradeTask.doUpgrade(false);

        dbConnectionManager.assertAllExpectedStatementsWereRun();
    }

    private List<Long> getPropertyIdsByBatchSize(int batchSize) {
        List<Long> ids = new ArrayList<>();
        for (int i = 1; i < batchSize; i++) {
            Long id = MIN_ID + i;
            ids.add(id);
        }
        return ids;
    }

    private void queryForMinusOneUserLocalePropertyIds(List<Long> propertyIds) {
        List<ResultRow> resultRows = propertyIds.stream().map(ResultRow::new).collect(Collectors.toList());
        dbConnectionManager.setQueryResults("select O_S_PROPERTY_STRING.id\n" +
                "from propertyentry O_S_PROPERTY_ENTRY\n" +
                "join propertystring O_S_PROPERTY_STRING\n" +
                "on O_S_PROPERTY_ENTRY.id = O_S_PROPERTY_STRING.id\n" +
                "where O_S_PROPERTY_ENTRY.property_key = 'jira.user.locale' and O_S_PROPERTY_STRING.propertyvalue like '-1' escape '\\'", resultRows);
    }

    private void queryForDeleteMinusOneUserLocaleProperty(List<Long> ids) {
        final String elements = Joiner.on(", ").join(ids);
        final String in = "(" + elements + ")";

        dbConnectionManager.setUpdateResults("delete from propertystring\n" +
                "where propertystring.id in " + in, 1);

        dbConnectionManager.setUpdateResults("delete from propertyentry\n" +
                "where propertyentry.id in " + in, 1);
    }
}