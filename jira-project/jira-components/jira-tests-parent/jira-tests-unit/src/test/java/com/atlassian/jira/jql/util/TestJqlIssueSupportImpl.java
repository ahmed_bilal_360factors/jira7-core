package com.atlassian.jira.jql.util;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.mock.issue.MockIssue;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Collections;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

/**
 * Test for {@link JqlIssueSupportImpl}.
 *
 * @since v4.0
 */
public class TestJqlIssueSupportImpl {
    @Rule
    public MockitoRule mockito = MockitoJUnit.rule();
    @Mock
    private IssueManager issueManager;
    @Mock
    private PermissionManager permissionManager;
    @InjectMocks
    private JqlIssueSupportImpl keySupport;

    @Test
    public void testGetIssuesEmptyKey() throws Exception {
        assertTrue(keySupport.getIssues(null, null).isEmpty());
        assertTrue(keySupport.getIssues("", null).isEmpty());
    }

    @Test
    public void testGetIssuesDoesNotExistKey() throws Exception {
        final String key = "key";
        when(issueManager.getIssueByKeyIgnoreCase(key)).thenReturn(null);

        assertTrue(keySupport.getIssues(key, null).isEmpty());

    }

    @Test
    public void testGetIssuesNoPermissionsKey() throws Exception {
        final String key = "key";
        final MockIssue issue = new MockIssue(89);

        when(issueManager.getIssueByKeyIgnoreCase(key)).thenReturn(issue);

        when(permissionManager.hasPermission(Permissions.BROWSE, issue, (ApplicationUser) null)).thenReturn(false);

        assertTrue(keySupport.getIssues(key, null).isEmpty());

    }

    @Test
    public void testGetIssuesSkipCheck() throws Exception {
        final String key = "key";
        final MockIssue issue = new MockIssue(89);

        when(issueManager.getIssueByKeyIgnoreCase(key)).thenReturn(issue);

        assertEquals(Collections.<Issue>singletonList(issue), keySupport.getIssues(key));
    }

    @Test
    public void testGetIssuesHappyPathKey() throws Exception {
        final ApplicationUser user = new MockApplicationUser("test");
        final String key = "key";
        final MockIssue issue = new MockIssue(89);

        when(issueManager.getIssueByKeyIgnoreCase(key)).thenReturn(issue);

        when(permissionManager.hasPermission(Permissions.BROWSE, issue, user)).thenReturn(true);

        assertEquals(Collections.<Issue>singletonList(issue), keySupport.getIssues(key, user));
    }


    @Test
    public void testGetIssueDoesNotExistId() throws Exception {
        final long id = 10;
        when(issueManager.getIssueObject(id)).thenReturn(null);

        assertNull(keySupport.getIssue(id, (ApplicationUser) null));
    }

    @Test
    public void testGetIssueNoPermissionsId() throws Exception {
        final long id = 11;
        final MockIssue issue = new MockIssue(id);

        when(issueManager.getIssueObject(id)).thenReturn(issue);

        when(permissionManager.hasPermission(Permissions.BROWSE, issue, (ApplicationUser) null)).thenReturn(false);

        assertNull(keySupport.getIssue(id, (ApplicationUser) null));
    }

    @Test
    public void testGetIssueSkipCheck() throws Exception {
        final long id = 12;
        final MockIssue issue = new MockIssue(id);

        when(issueManager.getIssueObject(id)).thenReturn(issue);

        assertSame(issue, keySupport.getIssue(id));
    }

    @Test
    public void testGetIssueHappyPathId() throws Exception {
        final ApplicationUser user = new MockApplicationUser("test");
        final long id = 12;
        final MockIssue issue = new MockIssue(id);

        when(issueManager.getIssueObject(id)).thenReturn(issue);

        when(permissionManager.hasPermission(Permissions.BROWSE, issue, user)).thenReturn(true);

        assertSame(issue, keySupport.getIssue(id, user));
    }
}
