package com.atlassian.jira.issue.fields;

import com.atlassian.jira.bc.ServiceOutcome;
import com.atlassian.jira.bc.project.ProjectService;
import com.atlassian.jira.issue.fields.layout.field.FieldLayout;
import com.atlassian.jira.issue.fields.layout.field.MockFieldLayout;
import com.atlassian.jira.issue.fields.layout.field.MockFieldLayoutManager;
import com.atlassian.jira.mock.issue.fields.layout.field.MockFieldConfigurationScheme;
import com.atlassian.jira.mock.security.MockSimpleAuthenticationContext;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.util.NoopI18nHelper;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import com.google.common.collect.Sets;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import static com.atlassian.jira.bc.project.ProjectAction.EDIT_PROJECT_CONFIG;
import static java.util.Collections.singleton;
import static java.util.Collections.singletonList;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.eq;

/**
 * @since v4.4
 */
public class TestDefaultProjectFieldLayoutSchemeHelper {
    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    private JiraAuthenticationContext authenticationContext;
    private MockFieldLayoutManager fieldLayoutManager;
    private MockApplicationUser user;
    @Mock
    private ProjectService projectService;

    @Mock
    ServiceOutcome<List<Project>> serviceOutcome;

    @Before
    public void setUp() throws Exception {
        user = new MockApplicationUser("mtan");
        authenticationContext = new MockSimpleAuthenticationContext(user, Locale.ENGLISH, new NoopI18nHelper());
        fieldLayoutManager = new MockFieldLayoutManager();
    }

    @After
    public void tearDown() {
        user = null;
        authenticationContext = null;
        fieldLayoutManager = null;
    }

    @Test
    public void testGetActiveFieldLayoutsWithInvalidServiceResult() {
        authenticationContext.setLoggedInUser(user);

        Mockito.when(serviceOutcome.isValid()).thenReturn(false);
        Mockito.when(projectService.getAllProjectsForAction(eq(user), eq(EDIT_PROJECT_CONFIG))).thenReturn(serviceOutcome);

        final FieldLayout fieldLayout = new MockFieldLayout();

        final ProjectFieldLayoutSchemeHelper projectFieldsContextProvider = new DefaultProjectFieldLayoutSchemeHelper(projectService,
                fieldLayoutManager, authenticationContext);

        final Multimap<FieldLayout, Project> activeFieldLayouts = projectFieldsContextProvider.getProjectsForFieldLayouts(Sets.<FieldLayout>newHashSet(fieldLayout));

        assertEquals(0, activeFieldLayouts.keySet().size());

        final List<Project> projectsForFieldLayout = projectFieldsContextProvider.getProjectsForFieldLayout(fieldLayout);

        assertEquals(0, projectsForFieldLayout.size());
    }

    @Test
    public void testGetActiveFieldLayoutsWithValidServiceResult() {
        authenticationContext.setLoggedInUser(user);

        final MockProject sameFieldLayout = new MockProject(888L, "aaa")
                .setIssueTypes("Bug", "Task");
        final MockProject differentFieldLayout = new MockProject(777L, "aaa")
                .setIssueTypes("Bug", "Task");

        final MockFieldLayout fieldLayout = new MockFieldLayout()
                .setDefault(false)
                .setDescription("description")
                .setId(999L)
                .setName("fieldLayout");

        fieldLayoutManager.setFieldLayout(sameFieldLayout, "Bug", fieldLayout);

        Mockito.when(serviceOutcome.isValid()).thenReturn(true);
        Mockito.when(serviceOutcome.getReturnedValue()).thenReturn(Lists.<Project>newArrayList(
                sameFieldLayout, differentFieldLayout
        ));

        Mockito.when(projectService.getAllProjectsForAction(eq(user), eq(EDIT_PROJECT_CONFIG))).thenReturn(serviceOutcome);

        final ProjectFieldLayoutSchemeHelper projectFieldLayoutSchemeHelper = new DefaultProjectFieldLayoutSchemeHelper(projectService,
                fieldLayoutManager, authenticationContext);

        final Multimap<FieldLayout, Project> activeFieldLayouts = projectFieldLayoutSchemeHelper
                .getProjectsForFieldLayouts(Sets.<FieldLayout>newHashSet(fieldLayout));

        assertEquals(1, activeFieldLayouts.keySet().size());
        assertEquals(singleton(sameFieldLayout), activeFieldLayouts.get(fieldLayout));

        final List<Project> projectsForFieldLayout = projectFieldLayoutSchemeHelper
                .getProjectsForFieldLayout(fieldLayout);

        assertEquals(1, projectsForFieldLayout.size());
        assertEquals(singletonList(sameFieldLayout), projectsForFieldLayout);
    }

    @Test
    public void testGetActiveFieldLayoutsWithValidServiceResultButNoMatchingFieldLayouts() {
        authenticationContext.setLoggedInUser(user);

        final MockProject differentFieldLayout = new MockProject(777L, "aaa")
                .setIssueTypes("Bug", "Task");

        final MockFieldLayout fieldLayout = new MockFieldLayout()
                .setDefault(false)
                .setDescription("description")
                .setId(999L)
                .setName("fieldLayout");

        Mockito.when(serviceOutcome.isValid()).thenReturn(true);
        Mockito.when(serviceOutcome.getReturnedValue()).thenReturn(Lists.<Project>newArrayList(
                differentFieldLayout
        ));

        Mockito.when(projectService.getAllProjectsForAction(eq(user), eq(EDIT_PROJECT_CONFIG))).thenReturn(serviceOutcome);

        final ProjectFieldLayoutSchemeHelper projectFieldLayoutSchemeHelper = new DefaultProjectFieldLayoutSchemeHelper(projectService,
                fieldLayoutManager, authenticationContext);

        final Multimap<FieldLayout, Project> activeFieldLayouts = projectFieldLayoutSchemeHelper
                .getProjectsForFieldLayouts(Sets.<FieldLayout>newHashSet(fieldLayout));

        assertEquals(0, activeFieldLayouts.keySet().size());

        final List<Project> projectsForFieldLayout = projectFieldLayoutSchemeHelper
                .getProjectsForFieldLayout(fieldLayout);

        assertEquals(0, projectsForFieldLayout.size());
    }

    @Test
    public void testGetActiveFieldLayoutsWithValidServiceResultAndNoProjects() {
        authenticationContext.setLoggedInUser(user);

        final MockFieldLayout fieldLayout = new MockFieldLayout()
                .setDefault(false)
                .setDescription("description")
                .setId(999L)
                .setName("fieldLayout");

        Mockito.when(serviceOutcome.isValid()).thenReturn(true);
        Mockito.when(serviceOutcome.getReturnedValue()).thenReturn(Collections.<Project>emptyList());

        Mockito.when(projectService.getAllProjectsForAction(eq(user), eq(EDIT_PROJECT_CONFIG))).thenReturn(serviceOutcome);

        final ProjectFieldLayoutSchemeHelper projectFieldLayoutSchemeHelper = new DefaultProjectFieldLayoutSchemeHelper(projectService,
                fieldLayoutManager, authenticationContext);

        final Multimap<FieldLayout, Project> activeFieldLayouts = projectFieldLayoutSchemeHelper
                .getProjectsForFieldLayouts(Sets.<FieldLayout>newHashSet(fieldLayout));

        assertEquals(0, activeFieldLayouts.keySet().size());

        final List<Project> projectsForFieldLayout = projectFieldLayoutSchemeHelper
                .getProjectsForFieldLayout(fieldLayout);

        assertEquals(0, projectsForFieldLayout.size());
    }

    @Test
    public void testGetProjectsForScheme() {
        final MockProject project1 = new MockProject(101928282L, "ONE");
        final MockProject project2 = new MockProject(35438590L, "TWO");

        final MockFieldConfigurationScheme configScheme = new MockFieldConfigurationScheme().setId(1010101L).setName("SchemeName");
        final MockFieldConfigurationScheme otherConfigScheme = new MockFieldConfigurationScheme().setId(1010102L).setName("OtherSchemeName");

        fieldLayoutManager.setFieldConfigurationScheme(1010101L, configScheme)
                .setFieldConfigurationScheme(project1, configScheme)
                .setFieldConfigurationScheme(project2, otherConfigScheme);


        Mockito.when(serviceOutcome.isValid()).thenReturn(true);
        Mockito.when(serviceOutcome.getReturnedValue()).thenReturn(Lists.<Project>newArrayList(project1));

        Mockito.when(projectService.getAllProjectsForAction(eq(user), eq(EDIT_PROJECT_CONFIG))).thenReturn(serviceOutcome);

        final ProjectFieldLayoutSchemeHelper projectFieldLayoutSchemeHelper = new DefaultProjectFieldLayoutSchemeHelper(projectService,
                fieldLayoutManager, authenticationContext);

        final List<Project> projects = projectFieldLayoutSchemeHelper.getProjectsForScheme(configScheme.getId());
        assertEquals(Arrays.<Project>asList(project1), projects);
    }


}
