package com.atlassian.jira.imports.project.handler;

import com.atlassian.jira.external.beans.ExternalLink;
import com.atlassian.jira.imports.project.ProjectImportPersister;
import com.atlassian.jira.imports.project.core.BackupSystemInformation;
import com.atlassian.jira.imports.project.core.EntityRepresentation;
import com.atlassian.jira.imports.project.core.ProjectImportResults;
import com.atlassian.jira.imports.project.mapper.ProjectImportMapper;
import com.atlassian.jira.imports.project.mapper.SimpleProjectImportIdMapper;
import com.atlassian.jira.imports.project.parser.IssueLinkParser;
import com.atlassian.jira.imports.project.transformer.IssueLinkTransformer;
import com.atlassian.jira.junit.rules.InitMockitoMocks;
import com.atlassian.jira.mock.i18n.MockI18nHelper;
import com.atlassian.jira.user.ApplicationUser;
import com.google.common.collect.ImmutableMap;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;

import java.util.Map;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

/**
 * @since v3.13
 */
public class TestIssueLinkPersisterHandler {
    public static final String ENTITY_NAME = "IssueLink";
    public static final String SOURCE_ID = "Source_ID";
    public static final String TRANSFORMED_SOURCE_ID = "Transformed_Source_ID";
    public static final String SOURCE_MAPPED_ID = "Source_Mapped_ID";
    public static final String DESTINATION_ID = "Destination_ID";
    public static final String TRANSFORMED_DESTINATION_ID = "Transformed_Destination_ID";
    public static final String DESTINATION_MAPPED_ID = "Destination_Mapped_ID";
    public static final String TRANSFORMED_LINK_TYPE = "Transformed_Link_Type";

    @Rule
    public InitMockitoMocks initMocks = new InitMockitoMocks(this);

    @Mock
    private Map<String, String> linkAttributes;
    @Mock
    private ExternalLink parsedLink;
    @Mock
    private ExternalLink transformedLink;

    @Mock
    private IssueLinkParser issueLinkParser;
    @Mock
    private IssueLinkTransformer issueLinkTransformer;
    @Mock
    private ProjectImportPersister projectImportPersister;
    @Mock
    private ProjectImportMapper projectImportMapper;
    @Mock
    private ProjectImportResults projectImportResults;
    @Mock
    private SimpleProjectImportIdMapper issueMapper;
    @Mock
    private BackupSystemInformation backupSystemInformation;
    @Mock
    private ApplicationUser importAuthor;

    private IssueLinkPersisterHandler issueLinkPersisterHandler;

    @Before
    public void setUp() throws Exception {

        when(projectImportMapper.getIssueMapper()).thenReturn(issueMapper);
        when(projectImportResults.getI18n()).thenReturn(new MockI18nHelper());

        //happy path:

        //parse succeeds
        when(issueLinkParser.parse(linkAttributes)).thenReturn(parsedLink);
        when(parsedLink.getSourceId()).thenReturn(SOURCE_ID);
        when(parsedLink.getDestinationId()).thenReturn(DESTINATION_ID);

        //transformation succeeds
        when(issueLinkTransformer.transform(projectImportMapper, parsedLink)).thenReturn(transformedLink);
        when(transformedLink.getSourceId()).thenReturn(TRANSFORMED_SOURCE_ID);
        when(transformedLink.getDestinationId()).thenReturn(TRANSFORMED_DESTINATION_ID);
        when(transformedLink.getLinkType()).thenReturn(TRANSFORMED_LINK_TYPE);

        //issues are inside current project
        when(issueMapper.getMappedId(SOURCE_ID)).thenReturn(SOURCE_MAPPED_ID);
        when(issueMapper.getMappedId(DESTINATION_ID)).thenReturn(DESTINATION_MAPPED_ID);

        issueLinkPersisterHandler = new IssueLinkPersisterHandler(projectImportPersister, projectImportMapper, projectImportResults, backupSystemInformation, issueLinkTransformer, issueLinkParser, new ExecutorForTests(), importAuthor);
    }

    @Test
    public void shouldNotDoAnythingOnUnknownEntity() throws Exception {
        //if we would like to do anything with unknown entity most likely we would get obvious NPE due to not initialized components
        IssueLinkPersisterHandler issueLinkPersisterHandler = new IssueLinkPersisterHandler(null, null, null, null, null, null, null, null);
        issueLinkPersisterHandler.handleEntity("SomeRubbish", ImmutableMap.of());

    }

    @Test
    public void shouldNotDoAnythingWhenParsedLinkHasSourceAndDestinationMissing() throws Exception {
        //like in shouldNotDoAnythingOnUnknownEntity - no NPE  = no action performed
        when(parsedLink.getSourceId()).thenReturn(null);
        IssueLinkPersisterHandler issueLinkPersisterHandler = new IssueLinkPersisterHandler(null, null, null, null, null, issueLinkParser, null, null);

        issueLinkPersisterHandler.handleEntity(ENTITY_NAME, linkAttributes);
    }

    @Test
    public void shouldNotPersistWhenTransformationFails() throws Exception {
        when(issueLinkTransformer.transform(projectImportMapper, parsedLink)).thenReturn(null);

        issueLinkPersisterHandler.handleEntity(ENTITY_NAME, linkAttributes);

        verifyZeroInteractions(projectImportPersister);
    }

    @Test
    public void shouldTryToCreateEntityBasedOnRepresentationOfValidLink() throws Exception {
        EntityRepresentation er = mock(EntityRepresentation.class);
        when(issueLinkParser.getEntityRepresentation(transformedLink)).thenReturn(er);

        issueLinkPersisterHandler.handleEntity(ENTITY_NAME, linkAttributes);
        verify(projectImportPersister).createEntity(er);
        verifyZeroInteractions(projectImportPersister);
    }

    @Test
    public void shouldAddErrorWhenEntityWasNotCreated() throws Exception {
        when(projectImportPersister.createEntity(any(EntityRepresentation.class))).thenReturn(null);
        when(backupSystemInformation.getIssueKeyForId(SOURCE_ID)).thenReturn("Source-key");
        when(backupSystemInformation.getIssueKeyForId(DESTINATION_ID)).thenReturn("Destination-key");

        issueLinkPersisterHandler.handleEntity(ENTITY_NAME, linkAttributes);

        verify(projectImportResults).addError("admin.errors.project.import.issue.link.error [Source-key] [Destination-key]");
    }

    @Test
    public void shouldShouldCreateChangeItemForSourceIssueWhenSourceIssueLivesOutsideProject() throws Exception {
        when(issueMapper.getMappedId(SOURCE_ID)).thenReturn(null);
        when(backupSystemInformation.getIssueKeyForId(DESTINATION_ID)).thenReturn("localDestinationIssueId");

        when(projectImportPersister.createChangeItemForIssueLinkIfNeeded(TRANSFORMED_SOURCE_ID, TRANSFORMED_LINK_TYPE, "localDestinationIssueId", true, importAuthor)).thenReturn("localIssueKey");

        issueLinkPersisterHandler.handleEntity(ENTITY_NAME, linkAttributes);

        verify(issueMapper).mapValue("localIssueKey", "localIssueKey");
    }

    @Test
    public void shouldShouldCreateChangeItemForDestinationIssueWhenDestinationIssueLivesOutsideProject()
            throws Exception {
        when(issueMapper.getMappedId(DESTINATION_ID)).thenReturn(null);
        when(backupSystemInformation.getIssueKeyForId(SOURCE_ID)).thenReturn("localSourceIssueId");

        when(projectImportPersister.createChangeItemForIssueLinkIfNeeded(TRANSFORMED_DESTINATION_ID, TRANSFORMED_LINK_TYPE, "localSourceIssueId", false, importAuthor)).thenReturn("localIssueKey");

        issueLinkPersisterHandler.handleEntity(ENTITY_NAME, linkAttributes);

        verify(issueMapper).mapValue("localIssueKey", "localIssueKey");
    }

}
