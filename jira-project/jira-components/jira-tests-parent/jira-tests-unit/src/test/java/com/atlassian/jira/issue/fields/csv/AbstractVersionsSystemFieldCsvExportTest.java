package com.atlassian.jira.issue.fields.csv;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.export.ExportableSystemField;
import com.atlassian.jira.issue.export.FieldExportParts;
import com.atlassian.jira.issue.fields.AffectedVersionsSystemField;
import com.atlassian.jira.issue.fields.FixVersionsSystemField;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.mock.project.MockVersion;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.NoopI18nHelper;
import com.google.common.collect.ImmutableList;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.mockito.Mock;

import java.util.Collection;
import java.util.List;
import java.util.Locale;

import static java.util.stream.Collectors.toList;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public abstract class AbstractVersionsSystemFieldCsvExportTest {
    protected abstract ExportableSystemField createField();

    protected abstract void setUpIssueWithValue(Issue issue, Collection<Version> versions);

    @Rule
    public final RuleChain mockito = MockitoMocksInContainer.forTest(this);

    @Mock
    static private JiraAuthenticationContext authenticationContext;

    @Before
    public void setUp() throws Exception {
        when(authenticationContext.getI18nHelper()).thenReturn(new NoopI18nHelper());
        when(authenticationContext.getLocale()).thenReturn(Locale.ENGLISH);
    }

    @Test
    public void allVersionsAreReturnedAsSeperateValues() {
        List<Version> versions = ImmutableList.of(
                MockVersion.named("version1"), MockVersion.named("version2"), MockVersion.named("version3")
        );
        Issue issue = mock(Issue.class);
        setUpIssueWithValue(issue, versions);
        ExportableSystemField field = createField();
        FieldExportParts representation = field.getRepresentationFromIssue(issue);
        assertThat(representation.getParts(), hasSize(1));
        assertThat(representation.getParts().get(0).getValues()::iterator, contains("version1", "version2", "version3"));
    }

    @Test
    public void allVersionsAreReturnedAsSortedValues() {
        List<Version> versions = ImmutableList.of(
                MockVersion.named("a"), MockVersion.named("c"), MockVersion.named("b")
        );
        Issue issue = mock(Issue.class);
        setUpIssueWithValue(issue, versions);
        ExportableSystemField field = createField();
        FieldExportParts representation = field.getRepresentationFromIssue(issue);
        assertThat(representation.getParts().get(0).getValues()::iterator, contains("a", "b", "c"));
    }

    @Test
    public void noValueIsReturnedWhenVersionsCollectionIsNull() {
        Issue issue = mock(Issue.class);
        setUpIssueWithValue(issue, null);
        ExportableSystemField field = createField();
        FieldExportParts representation = field.getRepresentationFromIssue(issue);
        assertThat(representation.getParts(), hasSize(1));
        assertThat(representation.getParts().get(0).getValues().collect(toList()), hasSize(0));
    }

    public static class FixVersionsSystemFieldCsvExportTest extends AbstractVersionsSystemFieldCsvExportTest {

        @Override
        protected ExportableSystemField createField() {
            return new FixVersionsSystemField(null, null, null, null, authenticationContext, null, null, null, null, null, null);
        }

        @Override
        protected void setUpIssueWithValue(final Issue issue, final Collection<Version> versions) {
            when(issue.getFixVersions()).thenReturn(versions);
        }
    }

    public static class AffectedVersionsSystemFieldCsvExportTest extends AbstractVersionsSystemFieldCsvExportTest {
        @Override
        protected ExportableSystemField createField() {
            return new AffectedVersionsSystemField(null, null, null, null, authenticationContext, null, null, null, null, null, null);
        }

        @Override
        protected void setUpIssueWithValue(final Issue issue, final Collection<Version> versions) {
            when(issue.getAffectedVersions()).thenReturn(versions);
        }
    }
}
