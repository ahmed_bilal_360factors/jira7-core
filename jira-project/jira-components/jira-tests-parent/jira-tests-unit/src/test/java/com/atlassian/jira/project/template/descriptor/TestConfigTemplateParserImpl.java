package com.atlassian.jira.project.template.descriptor;

import com.atlassian.jira.project.template.hook.ConfigTemplate;
import com.atlassian.jira.project.template.hook.IssueTypeSchemeTemplate;
import com.atlassian.jira.project.template.hook.IssueTypeScreenSchemeTemplate;
import com.atlassian.jira.project.template.hook.IssueTypeTemplate;
import com.atlassian.jira.project.template.hook.ScreenSchemeTemplate;
import com.atlassian.jira.project.template.hook.ScreenTabTemplate;
import com.atlassian.jira.project.template.hook.ScreenTemplate;
import com.atlassian.jira.project.template.hook.WorkflowSchemeTemplate;
import com.atlassian.jira.project.template.hook.WorkflowTemplate;
import com.atlassian.plugin.Plugin;
import org.hamcrest.Matchers;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import java.net.URL;
import java.util.List;
import java.util.Optional;

import static java.util.Arrays.asList;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.mockito.Mockito.when;

public class TestConfigTemplateParserImpl {
    @Rule
    public MethodRule initMockito = MockitoJUnit.rule();

    private static final String CONFIG_FILE = "/project-templates/test-config.json";
    private static final String ISSUE_TYPE1_ICON = "/project-templates/issueType1.png";
    private static final String ISSUE_TYPE1_AVATAR = "genericissue.svg";
    private static final String ISSUE_TYPE2_ICON = "/project-templates/issueType2.png";
    private static final String ISSUE_TYPE3_AVATAR = "epic.svg";
    private static final String WORKFLOW_BUNDLE1 = "/project-templates/wf1.jwb";
    private static final String WORKFLOW_BUNDLE2 = "/project-templates/wf2.jwb";

    @Mock
    private Plugin plugin;

    private ConfigTemplateParserImpl configTemplateParser = new ConfigTemplateParserImpl();

    @Test
    public void configIsParsedCorrectly() {
        URL configFileUrl = getResource(CONFIG_FILE);
        URL issueType1IconUrl = getResource(ISSUE_TYPE1_ICON);
        URL issueType2IconUrl = getResource(ISSUE_TYPE2_ICON);
        URL wfBundle1Url = getResource(WORKFLOW_BUNDLE1);
        URL wfBundle2Url = getResource(WORKFLOW_BUNDLE2);

        when(plugin.getResource(CONFIG_FILE)).thenReturn(configFileUrl);
        when(plugin.getResource(ISSUE_TYPE1_ICON)).thenReturn(issueType1IconUrl);
        when(plugin.getResource(ISSUE_TYPE2_ICON)).thenReturn(issueType2IconUrl);
        when(plugin.getResource(WORKFLOW_BUNDLE1)).thenReturn(wfBundle1Url);
        when(plugin.getResource(WORKFLOW_BUNDLE2)).thenReturn(wfBundle2Url);

        ConfigTemplate configTemplate = configTemplateParser.parse(CONFIG_FILE, plugin);

        assertIssueTypeScheme(configTemplate, issueType1IconUrl, issueType2IconUrl);
        assertIssueTypeScreenScheme(configTemplate);
        assertWorkflowScheme(configTemplate, wfBundle1Url, wfBundle2Url);
    }

    private URL getResource(String location) {
        return getClass().getResource(location);
    }

    private void assertIssueTypeScheme(ConfigTemplate configTemplate, URL issueType1IconUrl, URL issueType2IconUrl) {
        assertThat(configTemplate.issueTypeSchemeTemplate().isPresent(), is(true));
        IssueTypeSchemeTemplate issueTypeSchemeTemplate = configTemplate.issueTypeSchemeTemplate().get();
        assertThat(issueTypeSchemeTemplate.name(), equalTo("issue.type.scheme.name"));
        assertThat(issueTypeSchemeTemplate.description(), equalTo("issue.type.scheme.desc"));
        assertThat(issueTypeSchemeTemplate.defaultIssueType().isPresent(), is(true));
        assertThat(issueTypeSchemeTemplate.defaultIssueType().get(), equalTo("ISSUETYPE1"));

        List<IssueTypeTemplate> issueTypeTemplates = issueTypeSchemeTemplate.issueTypeTemplates();
        assertThat(issueTypeTemplates, hasSize(3));

        IssueTypeTemplate issueTypeTemplate1 = issueTypeTemplates.get(0);
        assertThat(issueTypeTemplate1.name(), equalTo("issue.type.1.name"));
        assertThat(issueTypeTemplate1.description(), equalTo("issue.type.1.desc"));
        assertThat(issueTypeTemplate1.isSubtask(), is(false));
        assertThat(issueTypeTemplate1.workflow().isPresent(), is(false));
        assertThat(issueTypeTemplate1.iconPath(), equalTo(ISSUE_TYPE1_ICON));
        assertThat(issueTypeTemplate1.iconUrl(), equalTo(issueType1IconUrl));
        assertThat(issueTypeTemplate1.avatar(), equalTo(Optional.of(ISSUE_TYPE1_AVATAR)));
        assertThat(issueTypeTemplate1.screenScheme().isPresent(), is(false));

        IssueTypeTemplate issueTypeTemplate2 = issueTypeTemplates.get(1);
        assertThat(issueTypeTemplate2.name(), equalTo("issue.type.2.name"));
        assertThat(issueTypeTemplate2.description(), equalTo("issue.type.2.desc"));
        assertThat(issueTypeTemplate2.isSubtask(), is(true));
        assertThat(issueTypeTemplate2.workflow().isPresent(), is(true));
        assertThat(issueTypeTemplate2.workflow().get(), equalTo("WF2"));
        assertThat(issueTypeTemplate2.iconPath(), equalTo(ISSUE_TYPE2_ICON));
        assertThat(issueTypeTemplate2.iconUrl(), equalTo(issueType2IconUrl));
        assertThat(issueTypeTemplate2.avatar().isPresent(), is(false));
        assertThat(issueTypeTemplate2.screenScheme().isPresent(), is(true));
        assertThat(issueTypeTemplate2.screenScheme().get(), equalTo("SCREENSCHEME2"));

        IssueTypeTemplate issueTypeTemplate3 = issueTypeTemplates.get(2);
        assertThat(issueTypeTemplate3.name(), equalTo("issue.type.3.name"));
        assertThat(issueTypeTemplate3.description(), equalTo("issue.type.3.desc"));
        assertThat(issueTypeTemplate3.isSubtask(), is(false));
        assertThat(issueTypeTemplate3.workflow().isPresent(), is(false));
        assertThat(issueTypeTemplate3.iconPath(), equalTo(null));
        assertThat(issueTypeTemplate3.iconUrl(), equalTo(null));
        assertThat(issueTypeTemplate3.avatar(), equalTo(Optional.of(ISSUE_TYPE3_AVATAR)));
        assertThat(issueTypeTemplate3.screenScheme().isPresent(), is(false));
    }

    private void assertIssueTypeScreenScheme(ConfigTemplate configTemplate) {
        assertThat(configTemplate.issueTypeSchemeTemplate().isPresent(), is(true));
        IssueTypeScreenSchemeTemplate issueTypeScreenSchemeTemplate = configTemplate.issueTypeScreenSchemeTemplate().get();
        assertThat(issueTypeScreenSchemeTemplate.name(), equalTo("issue.type.screen.scheme.name"));
        assertThat(issueTypeScreenSchemeTemplate.description(), equalTo("issue.type.screen.scheme.desc"));
        assertThat(issueTypeScreenSchemeTemplate.defaultScreenScheme(), equalTo("SCREENSCHEME1"));

        List<ScreenTemplate> screenTemplates = issueTypeScreenSchemeTemplate.screenTemplates();
        assertThat(screenTemplates, hasSize(2));

        ScreenTemplate screenTemplate1 = screenTemplates.get(0);
        assertThat(screenTemplate1.name(), equalTo("screen.1.name"));
        assertThat(screenTemplate1.description(), equalTo("screen.1.desc"));

        List<ScreenTabTemplate> screenTemplate1Tabs = screenTemplate1.tabs();
        assertThat(screenTemplates, hasSize(2));

        ScreenTabTemplate screenTemplate1Tab1 = screenTemplate1Tabs.get(0);
        assertThat(screenTemplate1Tab1.name(), equalTo("screen.1.tab.1.name"));
        assertThat(screenTemplate1Tab1.fields(), equalTo(asList("field1", "field2")));

        ScreenTabTemplate screenTemplate1Tab2 = screenTemplate1Tabs.get(1);
        assertThat(screenTemplate1Tab2.name(), equalTo("screen.1.tab.2.name"));
        assertThat(screenTemplate1Tab2.fields(), Matchers.<String>emptyIterable());

        ScreenTemplate screenTemplate2 = screenTemplates.get(1);
        assertThat(screenTemplate2.name(), equalTo("screen.2.name"));
        assertThat(screenTemplate2.description(), equalTo("screen.2.desc"));
        assertThat(screenTemplate2.tabs(), Matchers.<ScreenTabTemplate>emptyIterable());

        List<ScreenSchemeTemplate> screenSchemeTemplates = issueTypeScreenSchemeTemplate.screenSchemeTemplates();
        assertThat(screenSchemeTemplates, hasSize(2));

        ScreenSchemeTemplate screenSchemeTemplate1 = screenSchemeTemplates.get(0);
        assertThat(screenSchemeTemplate1.name(), equalTo("screen.scheme.1.name"));
        assertThat(screenSchemeTemplate1.description(), equalTo("screen.scheme.1.desc"));
        assertThat(screenSchemeTemplate1.defaultScreen(), equalTo("SCREEN1"));
        assertThat(screenSchemeTemplate1.createScreen().isPresent(), is(false));
        assertThat(screenSchemeTemplate1.editScreen().isPresent(), is(false));
        assertThat(screenSchemeTemplate1.viewScreen().isPresent(), is(false));

        ScreenSchemeTemplate screenSchemeTemplate2 = screenSchemeTemplates.get(1);
        assertThat(screenSchemeTemplate2.name(), equalTo("screen.scheme.2.name"));
        assertThat(screenSchemeTemplate2.description(), equalTo("screen.scheme.2.desc"));
        assertThat(screenSchemeTemplate2.defaultScreen(), equalTo("SCREEN1"));
        assertThat(screenSchemeTemplate2.createScreen().isPresent(), is(true));
        assertThat(screenSchemeTemplate2.createScreen().get(), equalTo("SCREEN1"));
        assertThat(screenSchemeTemplate2.editScreen().isPresent(), is(true));
        assertThat(screenSchemeTemplate2.editScreen().get(), equalTo("SCREEN2"));
        assertThat(screenSchemeTemplate2.viewScreen().isPresent(), is(true));
        assertThat(screenSchemeTemplate2.viewScreen().get(), equalTo("SCREEN2"));
    }

    private void assertWorkflowScheme(ConfigTemplate configTemplate, URL wfBundle1Url, URL wfBundle2Url) {
        assertThat(configTemplate.workflowSchemeTemplate().isPresent(), is(true));
        WorkflowSchemeTemplate workflowSchemeTemplate = configTemplate.workflowSchemeTemplate().get();
        assertThat(workflowSchemeTemplate.name(), equalTo("workflow.scheme.name"));
        assertThat(workflowSchemeTemplate.description(), equalTo("workflow.scheme.desc"));
        assertThat(workflowSchemeTemplate.defaultWorkflow().isPresent(), is(true));
        assertThat(workflowSchemeTemplate.defaultWorkflow().get(), equalTo("WF1"));

        List<WorkflowTemplate> workflowTemplates = workflowSchemeTemplate.workflowTemplates();
        assertThat(workflowTemplates, hasSize(2));

        WorkflowTemplate workflowTemplate1 = workflowTemplates.get(0);
        assertThat(workflowTemplate1.name(), equalTo("workflow.1.name"));
        assertThat(workflowTemplate1.bundlePath(), equalTo(WORKFLOW_BUNDLE1));
        assertThat(workflowTemplate1.bundleUrl(), equalTo(wfBundle1Url));

        WorkflowTemplate workflowTemplate2 = workflowTemplates.get(1);
        assertThat(workflowTemplate2.name(), equalTo("workflow.2.name"));
        assertThat(workflowTemplate2.bundlePath(), equalTo(WORKFLOW_BUNDLE2));
        assertThat(workflowTemplate2.bundleUrl(), equalTo(wfBundle2Url));
    }

    @Test(expected = IllegalArgumentException.class)
    public void throwsIllegalArgumentExceptionIfConfigCannotBeFound() {
        when(plugin.getResource("/test-config.json")).thenReturn(null);

        configTemplateParser.parse("/test-config.json", plugin);
    }
}
