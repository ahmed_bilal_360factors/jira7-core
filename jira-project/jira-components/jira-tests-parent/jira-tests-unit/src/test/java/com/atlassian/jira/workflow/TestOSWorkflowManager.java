package com.atlassian.jira.workflow;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueFactory;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.IssueTextFieldCharacterLengthValidator;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.index.IssueIndexManager;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.matchers.FeatureMatchers;
import com.atlassian.jira.mock.issue.MockIssue;
import com.atlassian.jira.mock.ofbiz.MockGenericValue;
import com.atlassian.jira.mock.security.MockSimpleAuthenticationContext;
import com.atlassian.jira.ofbiz.FieldMap;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.transaction.MockTransactionSupport;
import com.atlassian.jira.transaction.TransactionSupport;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.AnswerWith;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.NoopI18nHelper;
import com.atlassian.jira.web.bean.MockI18nBean;
import com.google.common.collect.ImmutableSet;
import com.opensymphony.workflow.FactoryException;
import com.opensymphony.workflow.Workflow;
import com.opensymphony.workflow.config.Configuration;
import com.opensymphony.workflow.loader.DescriptorFactory;
import com.opensymphony.workflow.loader.WorkflowDescriptor;
import org.hamcrest.FeatureMatcher;
import org.hamcrest.Matcher;
import org.hamcrest.collection.IsMapContaining;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.RuleChain;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.ArgumentCaptor;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.ofbiz.core.entity.GenericValue;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

import static com.atlassian.jira.matchers.MapMatchers.hasKeyThat;
import static java.util.Collections.emptySet;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.argThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.same;
import static org.mockito.Mockito.RETURNS_MOCKS;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

/**
 * This tests the OSWorkflowManager. Currently just tests migrateStatus changing of the updated date
 */
@RunWith(MockitoJUnitRunner.class)
public class TestOSWorkflowManager {
    private static final String ID_2 = "2";
    private static final String ID_1 = "1";

    @Rule
    public final RuleChain mockitoMocks = MockitoMocksInContainer.forTest(this);
    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Mock
    private Configuration configuration;
    @Mock
    private DraftWorkflowStore draftWorkflowStore;
    @Mock
    private EventPublisher eventPublisher;
    @Mock
    private IssueTextFieldCharacterLengthValidator textFieldCharacterLengthValidator;
    @Mock
    private JiraWorkflow mockJiraWorkflow;
    @Mock
    private JiraDraftWorkflow mockJiraDraftWorkflow;
    @Mock
    private ConfigurableJiraWorkflow configurableJiraWorkflow;
    @Mock
    private WorkflowDescriptor descriptor;

    @Mock
    @AvailableInContainer
    private UserManager userManager;
    @Mock
    @AvailableInContainer
    private WorkflowSchemeManager workflowSchemeManager;
    @Mock(answer = Answers.RETURNS_MOCKS)
    @AvailableInContainer
    private ProjectManager projectManager;
    @Mock(answer = Answers.RETURNS_MOCKS)
    @AvailableInContainer
    private ConstantsManager constantsManager;
    @Mock
    @AvailableInContainer
    private IssueIndexManager issueIndexManager;
    @AvailableInContainer
    private TransactionSupport transactionSupport = new MockTransactionSupport();
    @Mock
    @AvailableInContainer
    private IssueManager issueManager;
    @Mock
    @AvailableInContainer
    private IssueFactory issueFactory;


    private JiraAuthenticationContext ctx;
    private ApplicationUser applicationUser = new MockApplicationUser("userkey", "testuser", "Testy Tester", "test@test.com");

    private static final Integer ACTION = 123;
    private static final Long WORKFLOW_ID = 1l;
    private static final Long ISSUE_ID = 2l;
    private static final Long PROJECT_ID = 3l;
    private static final String PROJECT_KEY = "ISS";
    Project project;
    MutableIssue mutableIssue;
    ApplicationUser assignee;

    private OSWorkflowManager osWorkflowManager;

    @Before
    public void setUp() throws Exception {
        ctx = new MockSimpleAuthenticationContext(new MockApplicationUser("Something"), Locale.ENGLISH, new NoopI18nHelper());
        osWorkflowManager = new OSWorkflowManager(configuration, draftWorkflowStore, eventPublisher, null, null, ctx, textFieldCharacterLengthValidator);
        when(userManager.getUserByName(applicationUser.getUsername())).thenReturn(applicationUser);

        project = new MockProject(PROJECT_ID, PROJECT_KEY);
        mutableIssue = new MockIssue(ISSUE_ID.intValue(), "ISS-1");
        assignee = new MockApplicationUser("assigneeId", "assigneeKey", "some description", "test@test.com");
        mutableIssue.setProjectObject(project);
        mutableIssue.setWorkflowId(WORKFLOW_ID);
        mutableIssue.setAssignee(assignee);
    }

    @Test
    public void testMigrateIssueToWorkflowSetUpdatedDate() {
        final MockGenericValue issueGV = updateIssue(ID_1, ID_2);

        assertNotNull(issueGV.get("updated"));
        assertEquals(ID_2, issueGV.getString("status"));
    }

    @Test
    public void testMigrateIssueToWorkflowDoNotSetUpdatedDate() {
        final MockGenericValue issueGV = updateIssue(ID_1, ID_1);

        assertNull(issueGV.get("updated"));
        assertEquals(ID_1, issueGV.getString("status"));
    }

    @Test
    public void testSaveWorkflowWithLastAuthorAndDate() throws WorkflowException {
        final Map metaAttributes = new HashMap();
        when(descriptor.getMetaAttributes()).thenReturn(metaAttributes);

        when(mockJiraWorkflow.getDescriptor()).thenReturn(descriptor);
        when(mockJiraWorkflow.getName()).thenReturn("testWorkflow");

        osWorkflowManager = new OSWorkflowManager(configuration, null, eventPublisher, null, null, ctx, textFieldCharacterLengthValidator) {
            @Override
            public boolean isActive(final JiraWorkflow workflow) throws WorkflowException {
                return false;
            }
        };

        osWorkflowManager.updateWorkflow("testuser", mockJiraWorkflow);

        assertEquals(2, metaAttributes.size());
        assertEquals(applicationUser.getKey(), metaAttributes.get(JiraWorkflow.JIRA_META_UPDATE_AUTHOR_KEY));
        assertNotNull(metaAttributes.get(JiraWorkflow.JIRA_META_UPDATED_DATE));
        verify(mockJiraWorkflow).reset();
    }

    @Test
    public void testSaveWorkflowWithoutAuditTrail() throws Exception {
        when(mockJiraWorkflow.getDescriptor()).thenReturn(descriptor);
        when(mockJiraWorkflow.getName()).thenReturn("testWorkflow");

        osWorkflowManager = new OSWorkflowManager(configuration, null, eventPublisher, null, null, ctx, textFieldCharacterLengthValidator) {
            @Override
            public boolean isActive(final JiraWorkflow workflow) throws WorkflowException {
                return false;
            }
        };
        osWorkflowManager.saveWorkflowWithoutAudit(mockJiraWorkflow);
        verify(configuration).saveWorkflow("testWorkflow", descriptor, true);
        verify(mockJiraWorkflow).reset();
    }

    @Test
    public void testSaveWorkflowWithoutAuditTrailAndActiveWorkflow() throws Exception {
        when(mockJiraWorkflow.getDescriptor()).thenReturn(descriptor);
        when(mockJiraWorkflow.getName()).thenReturn("testWorkflow");

        osWorkflowManager = new OSWorkflowManager(configuration, null, eventPublisher, null, null, ctx, textFieldCharacterLengthValidator) {
            @Override
            public boolean isActive(final JiraWorkflow workflow) throws WorkflowException {
                return true;
            }
        };
        osWorkflowManager.saveWorkflowWithoutAudit(mockJiraWorkflow);
        verify(configuration).saveWorkflow("testWorkflow", descriptor, true);
        verify(mockJiraWorkflow).reset();
    }

    @Test
    public void testSaveWorkflowWithoutAuditTrailAndDraftWorkflow() throws WorkflowException {
        when(mockJiraWorkflow.isDraftWorkflow()).thenReturn(true);
        when(mockJiraWorkflow.getName()).thenReturn("Test");

        osWorkflowManager = new OSWorkflowManager(configuration, draftWorkflowStore, eventPublisher, null, null, ctx, textFieldCharacterLengthValidator) {
            @Override
            public boolean isActive(final JiraWorkflow workflow) throws WorkflowException {
                return true;
            }
        };
        osWorkflowManager.saveWorkflowWithoutAudit(mockJiraWorkflow);
        verify(draftWorkflowStore).updateDraftWorkflowWithoutAudit("Test", mockJiraWorkflow);
    }

    @Test
    public void testGetDraftWorkflow() {
        osWorkflowManager = new OSWorkflowManager(configuration, draftWorkflowStore, eventPublisher, null, null, ctx, textFieldCharacterLengthValidator) {
            @Override
            public JiraWorkflow getWorkflow(final String name) {
                return mockJiraWorkflow;
            }
        };
        osWorkflowManager.getDraftWorkflow("testworkflow");
        verify(draftWorkflowStore).getDraftWorkflow("testworkflow");
    }

    @Test
    public void testGetDraftWorkflowWithNullParentWorkflow() {
        // we don't want this to touch the store at all:
        draftWorkflowStore = mock(DraftWorkflowStore.class, AnswerWith.throwAssertionError());
        osWorkflowManager = new OSWorkflowManager(configuration, draftWorkflowStore, eventPublisher, null, null, ctx, textFieldCharacterLengthValidator) {
            @Override
            public JiraWorkflow getWorkflow(final String name) {
                return null;
            }
        };
        exception.expect(IllegalArgumentException.class);
        osWorkflowManager.getDraftWorkflow("testworkflow");
    }

    @Test
    public void testCreateDraftWorkflow() throws WorkflowException {
        when(mockJiraWorkflow.isActive()).thenReturn(true);
        osWorkflowManager = new OSWorkflowManager(configuration, draftWorkflowStore, eventPublisher, null, null, ctx, textFieldCharacterLengthValidator) {
            @Override
            public JiraWorkflow getWorkflow(final String name) {
                return mockJiraWorkflow;
            }
        };
        osWorkflowManager.createDraftWorkflow("testuser", "testworkflow");
        verify(draftWorkflowStore).createDraftWorkflow(applicationUser, mockJiraWorkflow);
    }

    @Test
    public void testCreateDraftWorkflowWithNoParentWorkflow() throws WorkflowException {
        osWorkflowManager = new OSWorkflowManager(configuration, draftWorkflowStore, eventPublisher, null, null, ctx, textFieldCharacterLengthValidator) {
            @Override
            public JiraWorkflow getWorkflow(final String name) {
                return null;
            }
        };
        exception.expect(Exception.class);
        exception.expectMessage("You can not create a draft workflow from a parent that does not exist.");
        osWorkflowManager.createDraftWorkflow("testuser", "testworkflow");
    }

    @Test
    public void testCreateDraftWorkflowWithInActiveParentWorkflow() throws WorkflowException {
        when(mockJiraWorkflow.isActive()).thenReturn(false);
        osWorkflowManager = new OSWorkflowManager(configuration, draftWorkflowStore, eventPublisher, null, null, ctx, textFieldCharacterLengthValidator) {
            @Override
            public JiraWorkflow getWorkflow(final String name) {
                return mockJiraWorkflow;
            }
        };

        exception.expect(RuntimeException.class);
        exception.expectMessage("You can not create a draft workflow from a parent workflow that is not active.");
        osWorkflowManager.createDraftWorkflow("testuser", "testworkflow");
    }

    @Test
    public void testCreateDraftWorkflowWithNullUsername() {
        osWorkflowManager = new OSWorkflowManager(configuration, null, eventPublisher, null, null, ctx, textFieldCharacterLengthValidator);
        exception.expect(IllegalArgumentException.class);
        osWorkflowManager.createDraftWorkflow((String) null, "testworkflow");
    }

    @Test
    public void testDeleteWorkflowDeletesAssociatedDraftWorkflow() throws Exception {
        when(mockJiraWorkflow.getName()).thenReturn("testworkflow");

        final AtomicBoolean deleteCalled = new AtomicBoolean(false);
        when(configuration.removeWorkflow("testworkflow")).thenReturn(true);
        osWorkflowManager = new OSWorkflowManager(configuration, null, eventPublisher, null, null, ctx, textFieldCharacterLengthValidator) {
            @Override
            public boolean deleteDraftWorkflow(final String parentWorkflowName) throws IllegalArgumentException {
                deleteCalled.set(true);
                return true;
            }

            @Override
            public boolean isActive(final JiraWorkflow workflow) throws WorkflowException {
                return false;
            }
        };

        when(workflowSchemeManager.getSchemesForWorkflowIncludingDrafts(mockJiraWorkflow))
                .thenReturn(Collections.<WorkflowScheme>emptyList());

        osWorkflowManager.deleteWorkflow(mockJiraWorkflow);

        // Assert that we called to the deleteDraftWorkflow() method
        assertTrue(deleteCalled.get());

        Collection<WorkflowScheme> schemes = Arrays.<WorkflowScheme>asList(new MockAssignableWorkflowScheme(10101938L, "Test"));
        when(workflowSchemeManager.getSchemesForWorkflowIncludingDrafts(mockJiraWorkflow)).thenReturn(schemes);
        exception.expect(WorkflowException.class);
        osWorkflowManager.deleteWorkflow(mockJiraWorkflow);
    }

    @Test
    public void testDeleteDraftWorkflow() throws Exception {
        MockJiraWorkflow mockDraft = new MockJiraWorkflow();
        mockDraft.setName("testworkflow");
        mockDraft.setWorkflowDescriptor(descriptor);

        when(draftWorkflowStore.getDraftWorkflow("testworkflow")).thenReturn(mockDraft);
        when(draftWorkflowStore.deleteDraftWorkflow("testworkflow")).thenReturn(true);

        when(configuration.getWorkflow("testworkflow")).thenReturn(descriptor);

        osWorkflowManager = new OSWorkflowManager(configuration, draftWorkflowStore, eventPublisher, null, null, ctx, textFieldCharacterLengthValidator);

        final boolean result = osWorkflowManager.deleteDraftWorkflow("testworkflow");
        assertThat(result, is(true));
    }

    @Test
    public void testDeleteDraftWorkflowWithNullParentName() {
        osWorkflowManager = new OSWorkflowManager(configuration, null, eventPublisher, null, null, ctx, textFieldCharacterLengthValidator);
        exception.expect(IllegalArgumentException.class);
        osWorkflowManager.deleteDraftWorkflow(null);
    }

    @Test
    public void testUpdateDraftWorkflow() {
        when(mockJiraDraftWorkflow.getDescriptor()).thenReturn(descriptor);
        when(mockJiraDraftWorkflow.isDraftWorkflow()).thenReturn(true);
        when(mockJiraDraftWorkflow.getName()).thenReturn("parentWorkflow");

        osWorkflowManager = new OSWorkflowManager(configuration, draftWorkflowStore, eventPublisher, null, null, ctx, textFieldCharacterLengthValidator) {
            @Override
            public JiraWorkflow getWorkflow(final String name) {
                return mockJiraWorkflow;
            }
        };
        osWorkflowManager.updateWorkflow("testuser", mockJiraDraftWorkflow);
        verify(draftWorkflowStore).updateDraftWorkflow(applicationUser, "parentWorkflow", mockJiraDraftWorkflow);
    }

    @Test
    public void testUpdateWorkflowNullWorkflow() {
        osWorkflowManager = new OSWorkflowManager(configuration, null, eventPublisher, null, null, ctx, textFieldCharacterLengthValidator);
        exception.expect(IllegalArgumentException.class);
        exception.expectMessage("Can not update a workflow with a null workflow/descriptor.");
        osWorkflowManager.updateWorkflow("testuser", null);
    }

    @Test
    public void testUpdateDraftWorkflowWithNonExistentParentWorkflow() {
        when(mockJiraDraftWorkflow.getDescriptor()).thenReturn(descriptor);
        when(mockJiraDraftWorkflow.isDraftWorkflow()).thenReturn(true);
        when(mockJiraDraftWorkflow.getName()).thenReturn("parentWorkflow");

        osWorkflowManager = new OSWorkflowManager(configuration, null, eventPublisher, null, null, ctx, textFieldCharacterLengthValidator) {
            @Override
            public JiraWorkflow getWorkflow(final String name) {
                return null;
            }
        };

        exception.expect(IllegalStateException.class);
        exception.expectMessage("You can not update a draft workflow for a parent that does not exist.");
        osWorkflowManager.updateWorkflow("testuser", mockJiraDraftWorkflow);
    }

    @Test
    public void testUpdateWorkflow() throws Exception {
        when(configuration.saveWorkflow("parentWorkflow", descriptor, true)).thenReturn(true);

        when(mockJiraWorkflow.getDescriptor()).thenReturn(descriptor);
        when(mockJiraWorkflow.isDraftWorkflow()).thenReturn(false);
        when(mockJiraWorkflow.getName()).thenReturn("parentWorkflow");
        when(mockJiraWorkflow.isSystemWorkflow()).thenReturn(false);
        when(mockJiraWorkflow.isActive()).thenReturn(false);

        osWorkflowManager = new OSWorkflowManager(configuration, draftWorkflowStore, eventPublisher, null, null, ctx, textFieldCharacterLengthValidator) {
            @Override
            public JiraWorkflow getWorkflow(final String name) {
                return null;
            }
        };
        osWorkflowManager.updateWorkflow("testuser", mockJiraWorkflow);
        verify(configuration).saveWorkflow("parentWorkflow", descriptor, true);
        verifyZeroInteractions(draftWorkflowStore);
    }

    @Test
    public void testUpdateSystemWorkflow() throws Exception {
        when(configuration.saveWorkflow("parentWorkflow", descriptor, true)).thenReturn(true);

        when(mockJiraWorkflow.getDescriptor()).thenReturn(descriptor);
        when(mockJiraWorkflow.isDraftWorkflow()).thenReturn(false);
        when(mockJiraWorkflow.getName()).thenReturn("parentWorkflow");
        when(mockJiraWorkflow.isSystemWorkflow()).thenReturn(true);
        when(mockJiraWorkflow.isActive()).thenReturn(false);

        final DraftWorkflowStore mockDraftWorkflowStore = mock(DraftWorkflowStore.class);
        when(mockDraftWorkflowStore.deleteDraftWorkflow("parentWorkflow")).thenReturn(true);

        osWorkflowManager = new OSWorkflowManager(configuration, mockDraftWorkflowStore, eventPublisher, null, null, ctx, textFieldCharacterLengthValidator) {
            @Override
            public JiraWorkflow getWorkflow(final String name) {
                return null;
            }
        };
        exception.expect(WorkflowException.class);
        exception.expectMessage("Cannot change the system workflow.");
        osWorkflowManager.updateWorkflow("testuser", mockJiraWorkflow);
    }

    @Test
    public void testUpdateActiveWorkflow() throws Exception {
        when(configuration.saveWorkflow("parentWorkflow", descriptor, true)).thenReturn(true);

        when(mockJiraWorkflow.getDescriptor()).thenReturn(descriptor);
        when(mockJiraWorkflow.isDraftWorkflow()).thenReturn(false);
        when(mockJiraWorkflow.getName()).thenReturn("parentWorkflow");
        when(mockJiraWorkflow.isSystemWorkflow()).thenReturn(false);
        when(mockJiraWorkflow.isActive()).thenReturn(true);

        final DraftWorkflowStore mockDraftWorkflowStore = mock(DraftWorkflowStore.class);
        when(mockDraftWorkflowStore.deleteDraftWorkflow("parentWorkflow")).thenReturn(true);

        osWorkflowManager = new OSWorkflowManager(configuration, mockDraftWorkflowStore, eventPublisher, null, null, ctx, textFieldCharacterLengthValidator) {
            @Override
            public JiraWorkflow getWorkflow(final String name) {
                return null;
            }
        };

        exception.expect(WorkflowException.class);
        exception.expectMessage("Cannot save an active workflow.");
        osWorkflowManager.updateWorkflow("testuser", mockJiraWorkflow);
    }

    @Test
    public void testUpdateDraftWorkflowWithNullWorkflowDescriptor() {
        final JiraDraftWorkflow mockJiraDraftWorkflow = mock(JiraDraftWorkflow.class);
        when(mockJiraDraftWorkflow.getDescriptor()).thenReturn(null);

        osWorkflowManager = new OSWorkflowManager(configuration, null, eventPublisher, null, null, ctx, textFieldCharacterLengthValidator);
        exception.expect(IllegalArgumentException.class);
        exception.expectMessage("Can not update a workflow with a null workflow/descriptor.");
        osWorkflowManager.updateWorkflow("testuser", mockJiraDraftWorkflow);
    }

    @Test
    public void testUpdateDraftWorkflowNullUsername() {
        osWorkflowManager = new OSWorkflowManager(configuration, null, eventPublisher, null, null, ctx, textFieldCharacterLengthValidator);
        exception.expect(IllegalArgumentException.class);
        exception.expectMessage("Can not update a workflow with a null username.");
        osWorkflowManager.updateWorkflow((String) null, null);
    }

    private MockGenericValue updateIssue(final String oldStatus, final String newStatus) {
        final Map issueFields = FieldMap.build("status", oldStatus);
        final MockGenericValue issueGV = new MockGenericValue("Issue", issueFields);

        final Map statusFields = FieldMap.build("id", newStatus);
        final MockGenericValue statusGV = new MockGenericValue("Status", statusFields);

        osWorkflowManager = new OSWorkflowManager(configuration, null, eventPublisher, null, null, ctx, textFieldCharacterLengthValidator);
        osWorkflowManager.updateIssueStatusAndUpdatedDate(issueGV, statusGV);
        return issueGV;
    }

    @Test
    public void testGetWorkflowNullName() throws Exception {
        // Create a mock Configuration
        when(configuration.getWorkflow(null)).thenThrow(new IllegalArgumentException());
        final OSWorkflowManager osWorkflowManager = new OSWorkflowManager(configuration, null, eventPublisher, null, null, ctx, textFieldCharacterLengthValidator);

        exception.expect(IllegalArgumentException.class);
        osWorkflowManager.getWorkflow((String) null);
    }

    @Test
    public void testGetDefaultWorkflow() throws Exception {
        when(configuration.getWorkflow("jira")).thenReturn(descriptor);
        final OSWorkflowManager osWorkflowManager = new OSWorkflowManager(configuration, null, eventPublisher, null, null, ctx, textFieldCharacterLengthValidator);

        final JiraWorkflow workflow = osWorkflowManager.getWorkflow("jira");

        assertTrue(workflow instanceof DefaultJiraWorkflow);
        assertEquals(descriptor, workflow.getDescriptor());
    }

    @Test
    public void testGetWorkflow() throws Exception {
        when(configuration.getWorkflow("someWorkflow")).thenReturn(descriptor);
        final OSWorkflowManager osWorkflowManager = new OSWorkflowManager(configuration, null, eventPublisher, null, null, ctx, textFieldCharacterLengthValidator);

        final JiraWorkflow workflow = osWorkflowManager.getWorkflow("someWorkflow");

        assertTrue(workflow instanceof ConfigurableJiraWorkflow);
        assertEquals(descriptor, workflow.getDescriptor());
    }

    @Test
    public void testGetWorflowClone() throws Exception {
        when(configuration.getWorkflow("someWorkflow")).thenReturn(descriptor);

        final OSWorkflowManager osWorkflowManager = new OSWorkflowManager(configuration, null, eventPublisher, null, null, ctx, textFieldCharacterLengthValidator) {
            //package level protected for testing.
            @Override
            WorkflowDescriptor cloneDescriptor(final WorkflowDescriptor workflowDescriptor) throws FactoryException {
                return new DescriptorFactory().createWorkflowDescriptor();
            }
        };

        final JiraWorkflow workflow = osWorkflowManager.getWorkflowClone("someWorkflow");
        assertTrue(workflow instanceof ConfigurableJiraWorkflow);
        assertNotSame(descriptor, workflow.getDescriptor());
        assertFalse(workflow.getDescriptor() instanceof ImmutableWorkflowDescriptor);
    }

    @Test
    public void testGetDefaultWorkflowClone() throws Exception {
        when(configuration.getWorkflow("jira")).thenReturn(descriptor);

        final OSWorkflowManager osWorkflowManager = new OSWorkflowManager(configuration, null, eventPublisher, null, null, ctx, textFieldCharacterLengthValidator);

        final JiraWorkflow workflow = osWorkflowManager.getWorkflowClone("jira");
        assertTrue(workflow instanceof DefaultJiraWorkflow);
        assertEquals(descriptor, workflow.getDescriptor());
    }

    @Test
    public void testOverwriteActiveWorkflowWithNoDraftWorkflow() {
        when(draftWorkflowStore.getDraftWorkflow("atestworkflow")).thenReturn(null);
        final OSWorkflowManager osWorkflowManager = new OSWorkflowManager(null, draftWorkflowStore, eventPublisher, null, null, ctx, textFieldCharacterLengthValidator);
        exception.expect(WorkflowException.class);
        exception.expectMessage("No draft workflow named 'atestworkflow'");
        osWorkflowManager.overwriteActiveWorkflow("merlin", "atestworkflow");
    }

    @Test
    public void testOverwriteActiveWorkflow() throws Exception {
        final WorkflowDescriptor originalDescriptor = new DescriptorFactory().createWorkflowDescriptor();
        final JiraWorkflow jiraWorkflow = new JiraDraftWorkflow("someworkflow", null, originalDescriptor);

        when(draftWorkflowStore.getDraftWorkflow("atestworkflow")).thenReturn(jiraWorkflow);
        when(draftWorkflowStore.deleteDraftWorkflow("atestworkflow")).thenReturn(true);

        when(configuration.getWorkflow("atestworkflow")).thenReturn(originalDescriptor);
        when(configuration.saveWorkflow(eq("atestworkflow"), argThat(isMutatedInstanceOf(originalDescriptor)), eq(true))).thenReturn(true);

        final OSWorkflowManager osWorkflowManager = new OSWorkflowManager(configuration, draftWorkflowStore, eventPublisher, null, null, ctx, textFieldCharacterLengthValidator);

        osWorkflowManager.overwriteActiveWorkflow(applicationUser.getUsername(), "atestworkflow");
        verify(configuration).saveWorkflow(eq("atestworkflow"), same(originalDescriptor), eq(true));
        verify(draftWorkflowStore).deleteDraftWorkflow("atestworkflow");
    }

    @Test
    public void testOverwriteActiveWorkflowSaveReturnsFalse() throws Exception {
        final WorkflowDescriptor originalDescriptor = new DescriptorFactory().createWorkflowDescriptor();
        final JiraWorkflow jiraWorkflow = new JiraDraftWorkflow(null, null, originalDescriptor);
        when(draftWorkflowStore.getDraftWorkflow("atestworkflow")).thenReturn(jiraWorkflow);

        when(configuration.getWorkflow("atestworkflow")).thenReturn(originalDescriptor);

        final OSWorkflowManager osWorkflowManager = new OSWorkflowManager(configuration, draftWorkflowStore, eventPublisher, null, null, ctx, textFieldCharacterLengthValidator);
        exception.expect(WorkflowException.class);
        exception.expectMessage("Workflow 'atestworkflow' could not be overwritten!");
        osWorkflowManager.overwriteActiveWorkflow(applicationUser.getUsername(), "atestworkflow");
    }

    @Test
    public void testCopyWorkflow() throws Exception {
        final OSWorkflowManager workflowManager = new OSWorkflowManager(configuration, null, eventPublisher, null, null, ctx, textFieldCharacterLengthValidator) {
            //package level protected for testing.
            @Override
            WorkflowDescriptor cloneDescriptor(final WorkflowDescriptor workflowDescriptor) throws FactoryException {
                return workflowDescriptor;
            }
        };
        final MockJiraWorkflow mockJiraWorkflow = new MockJiraWorkflow();
        mockJiraWorkflow.setWorkflowDescriptor(descriptor);
        workflowManager.copyWorkflow("testuser", "Copy of Workflow", "Workflow Desc", mockJiraWorkflow);
        //basically all we can check is that saveWorkflow was called with the right params.
        verify(configuration).saveWorkflow("Copy of Workflow", descriptor, true);
    }

    @Test
    public void testCopyWorkflowNullDescription() throws Exception {
        final OSWorkflowManager workflowManager = new OSWorkflowManager(configuration, null, eventPublisher, null, null, ctx, textFieldCharacterLengthValidator) {
            //package level protected for testing.
            @Override
            WorkflowDescriptor cloneDescriptor(final WorkflowDescriptor workflowDescriptor) throws FactoryException {
                return workflowDescriptor;
            }
        };
        final MockJiraWorkflow mockJiraWorkflow = new MockJiraWorkflow();
        mockJiraWorkflow.setWorkflowDescriptor(descriptor);
        workflowManager.copyWorkflow("testuser", "Copy of Workflow", null, mockJiraWorkflow);
        //basically all we can check is that saveWorkflow was called with the right params.
        verify(configuration).saveWorkflow("Copy of Workflow", descriptor, true);
    }

    @Test
    public void testEditWorkflowWithNoChanges() throws WorkflowException {
        when(configurableJiraWorkflow.getDescription()).thenReturn("sameDescription");

        final OSWorkflowManager osWorkflowManager = new OSWorkflowManager(configuration, draftWorkflowStore, eventPublisher, null, null, ctx, textFieldCharacterLengthValidator) {
            @Override
            public JiraWorkflow getWorkflowClone(final String name) {
                return configurableJiraWorkflow;
            }
        };
        final JiraWorkflow mockCurrentWorkflow = mock(JiraWorkflow.class);
        when(mockCurrentWorkflow.getName()).thenReturn("workflow1");
        when(mockCurrentWorkflow.isDraftWorkflow()).thenReturn(false);

        osWorkflowManager.updateWorkflowNameAndDescription("testuser", mockCurrentWorkflow, "workflow1", "sameDescription");

        verifyZeroInteractions(draftWorkflowStore);
        verifyZeroInteractions(configuration);
    }

    @Test
    public void testEditWorkflowNameOfDraftWorfklow() throws WorkflowException {
        final OSWorkflowManager osWorkflowManager = new OSWorkflowManager(configuration, draftWorkflowStore, eventPublisher, null, null, ctx, textFieldCharacterLengthValidator);
        final JiraWorkflow mockCurrentWorkflow = mock(JiraWorkflow.class);
        when(mockCurrentWorkflow.getName()).thenReturn("workflow1");
        when(mockCurrentWorkflow.isDraftWorkflow()).thenReturn(true);
        when(mockCurrentWorkflow.getDescription()).thenReturn("sameDescription");

        osWorkflowManager.updateWorkflowNameAndDescription("testuser", mockCurrentWorkflow, "newWorkflowName", "sameDescription");

        verifyZeroInteractions(draftWorkflowStore);
        verifyZeroInteractions(configuration);
    }

    @Test
    public void testEditWorkflowNameNullWorkflow() {
        final OSWorkflowManager osWorkflowManager = new OSWorkflowManager(null, null, eventPublisher, null, null, ctx, textFieldCharacterLengthValidator);
        exception.expect(IllegalArgumentException.class);
        osWorkflowManager.updateWorkflowNameAndDescription("testuser", null, "newWorkflowName", "sameDescription");
    }

    @Test
    public void testEditWorkflowDescriptionOnly() throws WorkflowException {
        when(configurableJiraWorkflow.getDescription()).thenReturn("sameDescription");
        when(configurableJiraWorkflow.getDescriptor()).thenReturn(descriptor);

        final OSWorkflowManager osWorkflowManager = new OSWorkflowManager(configuration, draftWorkflowStore, eventPublisher, null, null, ctx, textFieldCharacterLengthValidator) {
            @Override
            public JiraWorkflow getWorkflowClone(final String name) {

                return configurableJiraWorkflow;
            }

            @Override
            public void updateWorkflow(final ApplicationUser username, final JiraWorkflow workflow) {
                assertEquals(configurableJiraWorkflow, workflow);
            }
        };
        final JiraWorkflow mockCurrentWorkflow = mock(JiraWorkflow.class);
        when(mockCurrentWorkflow.getName()).thenReturn("workflow1");
        when(mockCurrentWorkflow.isDraftWorkflow()).thenReturn(false);

        osWorkflowManager.updateWorkflowNameAndDescription("testuser", mockCurrentWorkflow, "workflow1", "anotherDescription");

        // no interactions, because we mocked the updateWorkflow method on SUT.
        verifyZeroInteractions(draftWorkflowStore);
        verifyZeroInteractions(configuration);
    }

    @Test
    public void testEditWorkflowNameOnly() throws Exception {
        when(configurableJiraWorkflow.getDescription()).thenReturn("sameDescription");
        when(configurableJiraWorkflow.getDescriptor()).thenReturn(descriptor);

        final OSWorkflowManager osWorkflowManager = new OSWorkflowManager(configuration, draftWorkflowStore, eventPublisher, null, null, ctx, textFieldCharacterLengthValidator) {
            @Override
            public JiraWorkflow getWorkflowClone(final String name) {
                return configurableJiraWorkflow;
            }

            @Override
            public void updateWorkflow(final String username, final JiraWorkflow workflow) {
                assertEquals(configurableJiraWorkflow, workflow);
            }
        };
        final JiraWorkflow mockCurrentWorkflow = mock(JiraWorkflow.class);
        when(mockCurrentWorkflow.getName()).thenReturn("workflow1");
        when(mockCurrentWorkflow.isDraftWorkflow()).thenReturn(false);
        osWorkflowManager.updateWorkflowNameAndDescription("testuser", mockCurrentWorkflow, "anotherWorkflow",
                "sameDescription");

        verify(draftWorkflowStore).getDraftWorkflow("workflow1");
        verify(configuration).removeWorkflow("workflow1");
        verify(configuration).saveWorkflow("anotherWorkflow", descriptor, true);
        verify(workflowSchemeManager).updateSchemesForRenamedWorkflow("workflow1", "anotherWorkflow");
    }

    @Test
    public void testEditWorkflowNameAndDescription() throws Exception {
        when(configurableJiraWorkflow.getDescription()).thenReturn("sameDescription");
        when(configurableJiraWorkflow.getDescriptor()).thenReturn(descriptor);

        final OSWorkflowManager osWorkflowManager = new OSWorkflowManager(configuration, draftWorkflowStore, eventPublisher, null, null, ctx, textFieldCharacterLengthValidator) {
            @Override
            public JiraWorkflow getWorkflowClone(final String name) {
                return configurableJiraWorkflow;
            }

            @Override
            public void updateWorkflow(final ApplicationUser username, final JiraWorkflow workflow) {
                assertEquals(configurableJiraWorkflow, workflow);
            }
        };
        final JiraWorkflow mockCurrentWorkflow = mock(JiraWorkflow.class);
        when(mockCurrentWorkflow.getName()).thenReturn("workflow1");
        when(mockCurrentWorkflow.isDraftWorkflow()).thenReturn(false);

        osWorkflowManager.updateWorkflowNameAndDescription("testuser", mockCurrentWorkflow, "anotherWorkflow",
                "anotherDescription");

        verify(workflowSchemeManager).updateSchemesForRenamedWorkflow("workflow1", "anotherWorkflow");
        verify(configuration).removeWorkflow("workflow1");
        verify(configuration).saveWorkflow("anotherWorkflow", descriptor, true);
        verify(draftWorkflowStore).getDraftWorkflow("workflow1");
    }

    @Test
    public void testEditWorkflowNameWithDraftWorkflow() throws Exception {
        final WorkflowDescriptor descriptor = new DescriptorFactory().createWorkflowDescriptor();

        when(mockJiraDraftWorkflow.getDescriptor()).thenReturn(new DescriptorFactory().createWorkflowDescriptor());

        when(draftWorkflowStore.getDraftWorkflow("workflow1")).thenReturn(mockJiraDraftWorkflow);
        when(draftWorkflowStore.deleteDraftWorkflow("workflow1")).thenReturn(true);

        when(configurableJiraWorkflow.getDescription()).thenReturn("sameDescription");
        when(configurableJiraWorkflow.getDescriptor()).thenReturn(descriptor);


        final OSWorkflowManager osWorkflowManager = new OSWorkflowManager(configuration, draftWorkflowStore, eventPublisher, null, null, ctx, textFieldCharacterLengthValidator) {
            @Override
            public JiraWorkflow getWorkflowClone(final String name) {
                return configurableJiraWorkflow;
            }

            @Override
            public void updateWorkflow(final ApplicationUser username, final JiraWorkflow workflow) {
                assertEquals(configurableJiraWorkflow, workflow);
            }
        };
        final JiraWorkflow mockCurrentWorkflow = mock(JiraWorkflow.class);
        when(mockCurrentWorkflow.getName()).thenReturn("workflow1");
        when(mockCurrentWorkflow.isDraftWorkflow()).thenReturn(false);
        osWorkflowManager.updateWorkflowNameAndDescription("testuser", mockCurrentWorkflow, "anotherWorkflow",
                "sameDescription");

        verify(draftWorkflowStore).createDraftWorkflow(eq(applicationUser), any(JiraWorkflow.class));
        verify(configuration).removeWorkflow("workflow1");
        verify(configuration).saveWorkflow("anotherWorkflow", descriptor, true);
        verify(workflowSchemeManager).updateSchemesForRenamedWorkflow("workflow1", "anotherWorkflow");
    }

    @Test
    public void testCopyAndDeleteDraftWorkflowsEmptyInput() {
        final OSWorkflowManager osWorkflowManager = new OSWorkflowManager(null, null, eventPublisher, null, null, ctx, textFieldCharacterLengthValidator);
        // these shouldn't throw any errors.
        osWorkflowManager.copyAndDeleteDraftWorkflows(null, null);
        osWorkflowManager.copyAndDeleteDraftWorkflows(null, emptySet());
    }

    @Test
    public void testCopyAndDeleteDraftWorkflows() {
        final JiraWorkflow mockJiraWorkflowActive = mock(JiraWorkflow.class);
        when(mockJiraWorkflowActive.getName()).thenReturn("I'm active!");
        when(mockJiraWorkflowActive.isActive()).thenReturn(true);

        final JiraWorkflow mockJiraWorkflowNoDraft = mock(JiraWorkflow.class);
        when(mockJiraWorkflowNoDraft.getName()).thenReturn("I've got no draft!");
        when(mockJiraWorkflowNoDraft.isActive()).thenReturn(false);

        final JiraWorkflow mockJiraWorkflowActiveWithDraft = mock(JiraWorkflow.class);
        when(mockJiraWorkflowActiveWithDraft.getName()).thenReturn("Yeah Baby! I've got it all!");
        when(mockJiraWorkflowActiveWithDraft.isActive()).thenReturn(false);

        final Set<JiraWorkflow> workflowsToCopy = ImmutableSet.of(mockJiraWorkflowActive, mockJiraWorkflowNoDraft, mockJiraWorkflowActiveWithDraft);

        final AtomicBoolean copyWorkflowCalled = new AtomicBoolean();
        copyWorkflowCalled.set(false);
        final AtomicBoolean deleteDraftWorkflowCalled = new AtomicBoolean();
        deleteDraftWorkflowCalled.set(false);

        final OSWorkflowManager osWorkflowManager = new OSWorkflowManager(null, null, eventPublisher, null, null, ctx, textFieldCharacterLengthValidator) {
            @Override
            public JiraWorkflow getDraftWorkflow(final String parentWorkflowName) throws IllegalArgumentException {
                final JiraWorkflow mockDraftWorkflow = mock(JiraWorkflow.class);
                when(mockDraftWorkflow.isDraftWorkflow()).thenReturn(true);
                switch (parentWorkflowName) {
                    case "I'm active!":
                        return mockDraftWorkflow;
                    case "I've got no draft!":
                        return null;
                    case "Yeah Baby! I've got it all!":
                        when(mockDraftWorkflow.getDescription()).thenReturn("My description is cool.");
                        return mockDraftWorkflow;
                }
                fail("Unexpected call to getDraftWorkflow with name '" + parentWorkflowName + "'");
                return null;
            }

            @Override
            public JiraWorkflow copyWorkflow(final String username, final String clonedWorkflowName, final String clonedWorkflowDescription, final JiraWorkflow workflowToClone) {
                assertEquals("fakeuser", username);
                assertEquals("Copy of Yeah Baby! I've got it all!", clonedWorkflowName);
                assertEquals(
                        "My description is cool. (This copy was automatically generated from a draft, when workflow 'Yeah Baby! I've got it all!' was made inactive.)",
                        clonedWorkflowDescription);
                assertNotNull(workflowToClone);
                copyWorkflowCalled.set(true);
                return null;
            }

            @Override
            public boolean deleteDraftWorkflow(final String parentWorkflowName) throws IllegalArgumentException {
                assertEquals("Yeah Baby! I've got it all!", parentWorkflowName);
                deleteDraftWorkflowCalled.set(true);
                return true;
            }

            @Override
            I18nHelper getI18nBean(final ApplicationUser user) {
                return new MockI18nBean();
            }

            @Override
            String getClonedWorkflowName(final String parentWorkflowName, ApplicationUser user) {
                return "Copy of " + parentWorkflowName;
            }
        };

        final ApplicationUser remoteUser = new MockApplicationUser("fakeuser");
        osWorkflowManager.copyAndDeleteDraftWorkflows(remoteUser, workflowsToCopy);

        assertTrue(copyWorkflowCalled.get());
        assertTrue(deleteDraftWorkflowCalled.get());
    }

    @Test
    public void isEditableByDefault() {
        osWorkflowManager = spy(osWorkflowManager);
        Issue issue = mock(Issue.class, RETURNS_MOCKS);
        when(workflowSchemeManager.getWorkflowName(Matchers.<Project>any(), Matchers.<String>any())).thenReturn("Workflow");

        doReturn(mockJiraWorkflow).when(osWorkflowManager).getWorkflow(anyLong(), anyString());

        assertTrue("issue should be editable by default", osWorkflowManager.isEditable(issue));
    }

    @Test
    public void doWorkflowActionProperlyFillsInputsMapWithOriginalAssigneeId()
            throws com.opensymphony.workflow.WorkflowException {
        final WorkflowProgressAware workflowProgressAware = mock(WorkflowProgressAware.class);
        when(workflowProgressAware.getIssue()).thenReturn(mutableIssue);
        when(workflowProgressAware.getRemoteUser()).thenReturn(applicationUser);
        when(workflowProgressAware.getAction()).thenReturn(ACTION);
        when(workflowProgressAware.getProject()).thenReturn(project);
        final Workflow workflow = mock(Workflow.class);
        final IssueTextFieldCharacterLengthValidator.ValidationResult validationResultSuccess = mock(IssueTextFieldCharacterLengthValidator.ValidationResult.class);
        when(validationResultSuccess.isValid()).thenReturn(true);
        when(textFieldCharacterLengthValidator.validateModifiedFields(mutableIssue)).thenReturn(validationResultSuccess);
        osWorkflowManager = new OSWorkflowManager(configuration, draftWorkflowStore, eventPublisher, null, null, ctx, textFieldCharacterLengthValidator) {
            @Override
            public Workflow makeWorkflow(final ApplicationUser user) {
                return workflow;
            }
        };
        final GenericValue mutableIssueGV = mutableIssue.getGenericValue();
        when(issueManager.getIssue(ISSUE_ID)).thenReturn(mutableIssueGV);
        when(issueFactory.getIssue(mutableIssueGV)).thenReturn(mutableIssue);

        osWorkflowManager.doWorkflowAction(workflowProgressAware);

        final ArgumentCaptor<Map> mapCaptor = ArgumentCaptor.forClass(Map.class);
        verify(workflow, times(1)).doAction(Mockito.eq(WORKFLOW_ID), Mockito.eq(ACTION), mapCaptor.capture());
        final Map<Object, Object> capturedMap = mapCaptor.getValue();
        assertThat("originalAssigneeId should be prefilled", capturedMap, new IsMapContaining<Object, Object>(equalTo("originalAssigneeId"), equalTo(assignee.getKey())));
    }

    private Matcher<WorkflowDescriptor> isMutatedInstanceOf(final WorkflowDescriptor originalDescriptor) {
        //check the descriptor is the same instance as from the draft workflow.  Also check the updated user and time
        //stamp meta tags were added.
        return allOf(
                sameInstance(originalDescriptor),
                hasMetaAttributesThat(hasEntry(JiraWorkflow.JIRA_META_UPDATE_AUTHOR_KEY, applicationUser.getKey())),
                hasMetaAttributesThat(hasKeyThat(equalTo("jira.updated.date")))
        );
    }

    private FeatureMatcher<WorkflowDescriptor, Map<? extends String, ?>> hasMetaAttributesThat(final Matcher<Map<? extends String, ?>> mapMatcher) {
        return FeatureMatchers.hasFeature((workflowDescriptor) -> (Map<? extends String, ?>) workflowDescriptor.getMetaAttributes(), mapMatcher, "Descriptor with mata attributes that", "Meta attributes");
    }
}
