package com.atlassian.jira.issue.customfields.searchers;

import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.datetime.DateTimeFormatterFactory;
import com.atlassian.jira.issue.customfields.searchers.transformer.CustomFieldInputHelper;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.jql.operand.JqlOperandResolver;
import com.atlassian.jira.jql.util.JqlLocalDateSupport;
import com.atlassian.jira.mock.component.MockComponentWorker;
import com.atlassian.jira.template.VelocityTemplatingEngine;
import com.atlassian.jira.util.velocity.VelocityRequestContextFactory;
import com.atlassian.jira.web.FieldVisibilityManager;
import com.atlassian.jira.web.action.util.CalendarLanguageUtil;
import com.atlassian.velocity.VelocityManager;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestDateRangeSearcher {
    @Test
    public void getSorterDoesNotReturnNull() {
        new MockComponentWorker().init()
                .addMock(VelocityTemplatingEngine.class, mock(VelocityTemplatingEngine.class));

        final DateRangeSearcher dateRangeSearcher = new DateRangeSearcher(
                mock(JqlOperandResolver.class),
                mock(JqlLocalDateSupport.class),
                mock(CustomFieldInputHelper.class),
                mock(DateTimeFormatterFactory.class),
                mock(VelocityRequestContextFactory.class),
                mock(ApplicationProperties.class),
                mock(VelocityManager.class),
                mock(CalendarLanguageUtil.class),
                mock(FieldVisibilityManager.class)
        );

        final CustomField mockDateField = mock(CustomField.class);
        when(mockDateField.getId()).thenReturn("foo");

        assertNotNull(dateRangeSearcher.getSorter(mockDateField));
    }
}
