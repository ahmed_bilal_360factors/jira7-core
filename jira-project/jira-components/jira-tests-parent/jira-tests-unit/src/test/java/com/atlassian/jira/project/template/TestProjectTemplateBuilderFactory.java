package com.atlassian.jira.project.template;

import com.atlassian.plugin.webresource.WebResourceUrlProvider;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;

public class TestProjectTemplateBuilderFactory {
    @Test
    public void returnsANewBuilder() {
        ProjectTemplateBuilder projectTemplateBuilder = new ProjectTemplateBuilderFactory(mock(WebResourceUrlProvider.class)).newBuilder();

        assertNotNull(projectTemplateBuilder);
    }
}
