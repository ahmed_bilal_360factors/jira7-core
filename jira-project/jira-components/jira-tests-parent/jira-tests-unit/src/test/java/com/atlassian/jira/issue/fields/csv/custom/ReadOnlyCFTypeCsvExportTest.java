package com.atlassian.jira.issue.fields.csv.custom;

import com.atlassian.jira.issue.customfields.CustomFieldType;
import com.atlassian.jira.issue.customfields.impl.ReadOnlyCFType;

public class ReadOnlyCFTypeCsvExportTest extends TextCustomFieldCsvExportTest {
    @Override
    protected CustomFieldType<String, ?> createField() {
        return new ReadOnlyCFType(null, null, null, null);
    }
}
