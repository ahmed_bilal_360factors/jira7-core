package com.atlassian.jira.web.action.user;

import com.atlassian.jira.JiraApplicationContext;
import com.atlassian.jira.JiraTestUtil;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.JiraWebActionSupportDependencies;
import com.atlassian.jira.junit.rules.MockitoContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.mail.CssInliner;
import com.atlassian.jira.mail.TemplateContext;
import com.atlassian.jira.mail.TemplateContextFactory;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.template.TemplateSource;
import com.atlassian.jira.template.VelocityTemplatingEngine;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockUserLocaleStore;
import com.atlassian.jira.user.UserLocaleStore;
import com.atlassian.jira.user.UserPropertyManager;
import com.atlassian.jira.user.util.UserUtil;
import com.atlassian.jira.util.JiraContactHelper;
import com.atlassian.jira.util.collect.MapBuilder;
import com.atlassian.mail.queue.MailQueue;
import com.atlassian.mail.queue.MailQueueItem;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.mockito.Answers;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;

import java.util.Locale;
import java.util.Map;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestContactAdministrators {
    @Rule
    public final MockitoContainer mockContainer = MockitoMocksInContainer.rule(this);
    @Rule
    public TestRule webActionSupport = JiraWebActionSupportDependencies.build(mockContainer);

    @Mock
    MailQueue mailQueue;
    @Mock
    JiraContactHelper jiraContactHelper;
    @Mock
    UserUtil userUtil;
    @Mock
    @AvailableInContainer
    TemplateContextFactory templateContextFactory;
    @Mock(answer = Answers.RETURNS_MOCKS)
    @AvailableInContainer
    UserPropertyManager userPropertyManager;
    @Mock
    @AvailableInContainer
    VelocityTemplatingEngine velocityTemplatingEngine;
    @Mock
    @AvailableInContainer
    ApplicationProperties applicationProperties;
    @Mock
    VelocityTemplatingEngine.RenderRequest renderRequest;
    @Mock
    @AvailableInContainer
    CssInliner cssInliner;
    @AvailableInContainer
    @Mock(answer = Answers.RETURNS_MOCKS)
    JiraAuthenticationContext authenticationContext;
    @AvailableInContainer
    UserLocaleStore localeStore = new MockUserLocaleStore(Locale.ENGLISH);
    @Mock(answer = Answers.RETURNS_MOCKS)
    @AvailableInContainer
    JiraApplicationContext applicationContext;


    @Test
    public void checkSendingEmail() throws Exception {

        when(jiraContactHelper.isAdministratorContactFormEnabled()).thenReturn(true);

        ApplicationUser activeAdmin = mock(ApplicationUser.class);
        when(activeAdmin.getEmailAddress()).thenReturn("activeAdmin@atlassian.com");
        when(activeAdmin.isActive()).thenReturn(Boolean.TRUE);

        ApplicationUser inActiveAdmin = mock(ApplicationUser.class);
        when(inActiveAdmin.getEmailAddress()).thenReturn("inActiveAdmin@atlassian.com");
        when(inActiveAdmin.isActive()).thenReturn(Boolean.FALSE);

        when(userUtil.getJiraAdministrators()).thenReturn(Lists.newArrayList(activeAdmin, inActiveAdmin));

        TemplateContext templateContext = mock(TemplateContext.class);
        when(templateContextFactory.getTemplateContext(any(Locale.class))).thenReturn(templateContext);

        Map<String, Object> context = MapBuilder.<String, Object>newBuilder().add("testing", "yes").toMutableMap();
        when(templateContext.getTemplateParams()).thenReturn(context);

        //use a mock servlet response
        JiraTestUtil.setupExpectedRedirect("/secure/MyJiraHome.jspa");

        when(velocityTemplatingEngine.render(any(TemplateSource.class))).thenReturn(renderRequest);
        ArgumentCaptor<Map> argumentCaptor = ArgumentCaptor.forClass(Map.class);
        when(renderRequest.applying(argumentCaptor.capture())).thenReturn(renderRequest);

        ContactAdministrators contact = new ContactAdministrators(null, mailQueue, userUtil, userPropertyManager, jiraContactHelper);
        contact.setDetails("");
        contact.setSubject("SomeSubject");
        contact.doExecute();

        verify(mailQueue, times(1)).addItem(any(MailQueueItem.class));
    }

}
