package com.atlassian.jira.issue.label;

import com.atlassian.jira.issue.label.OfBizLabelStore.Columns;
import com.atlassian.jira.mock.ofbiz.MockGenericValue;
import com.atlassian.jira.mock.ofbiz.MockOfBizDelegator;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.util.collect.CollectionBuilder;
import com.atlassian.jira.util.collect.MapBuilder;
import com.google.common.collect.ImmutableSet;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.stubbing.OngoingStubbing;
import org.ofbiz.core.entity.GenericValue;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

/**
 * @since v4.2
 */
public class TestOfBizLabelStore {
    private static final Long ISSUE_1_ID = 10000L;
    private static final Long ISSUE_2_ID = 10001L;
    private static final Long CUSTOM_FIELD_1_ID = null;
    private static final Long CUSTOM_FIELD_2_ID = 20001L;

    private static enum Data {
        FOO(1L, ISSUE_1_ID, CUSTOM_FIELD_1_ID, "foo"),
        BAR(2L, ISSUE_1_ID, CUSTOM_FIELD_1_ID, "bar"),
        BAR_UPPERCASE(MockOfBizDelegator.STARTING_ID, ISSUE_1_ID, CUSTOM_FIELD_1_ID, BAR.labelString.toUpperCase()),
        QUX(3L, ISSUE_2_ID, CUSTOM_FIELD_2_ID, "qux");

        long labelId;
        long issueId;
        Long customFieldId;
        String labelString;
        GenericValue genericValue;
        Label label;

        Data(long labelId, long issueId, Long customFieldId, String labelString) {
            this.labelId = labelId;
            this.issueId = issueId;
            this.customFieldId = customFieldId;
            this.labelString = labelString;
            this.genericValue = new MockGenericValue(OfBizLabelStore.TABLE, MapBuilder.<String, Object>newBuilder()
                    .add(Columns.ID, labelId)
                    .add(Columns.ISSUE_ID, issueId)
                    .add(Columns.CUSTOM_FIELD_ID, customFieldId)
                    .add(Columns.LABEL, labelString)
                    .toMap());
            this.label = new Label(labelId, issueId, customFieldId, labelString);
        }
    }

    @Rule
    public final MethodRule initMockito = MockitoJUnit.rule();

    @Mock
    private OfBizDelegator mockDelegate;

    @InjectMocks
    private OfBizLabelStore store;

    @Test
    public void testSortsResults() {
        expectFindAllLabels(mockDelegate)
                .thenReturn(Arrays.asList(Data.FOO.genericValue, Data.BAR.genericValue));

        assertEquals(ImmutableSet.of(Data.BAR.label, Data.FOO.label),
                store.getLabels(ISSUE_1_ID, CUSTOM_FIELD_1_ID)
        );
    }

    @Test
    public void testAddNewLabel() {
        expectFindLabel(mockDelegate, Data.BAR.labelString)
                .thenReturn(Collections.<GenericValue>emptyList());
        expectAddLabel(mockDelegate, Data.BAR.labelString)
                .thenReturn(Data.BAR.genericValue);

        assertEquals(Data.BAR.label, store.addLabel(Data.BAR.issueId, Data.BAR.customFieldId, Data.BAR.labelString));
    }

    @Test
    public void testBlankLabelsIgnored() {
        mockDelegate = new MockOfBizDelegator(
                Collections.<GenericValue>emptyList(),
                Arrays.asList(Data.BAR_UPPERCASE.genericValue)
        );
        store = new OfBizLabelStore(mockDelegate);

        assertEquals(
                Collections.singleton(Data.BAR_UPPERCASE.label),
                store.setLabels(ISSUE_1_ID, CUSTOM_FIELD_1_ID, CollectionBuilder.newBuilder(Data.BAR_UPPERCASE.labelString, "", " ").asSet())
        );
        ((MockOfBizDelegator) mockDelegate).verifyAll();
    }

    @Test
    public void testAddIsCaseSensitive() {
        mockDelegate = new MockOfBizDelegator(
                Arrays.asList(Data.FOO.genericValue, Data.BAR.genericValue),
                Arrays.asList(Data.FOO.genericValue, Data.BAR.genericValue, Data.BAR_UPPERCASE.genericValue)
        );
        store = new OfBizLabelStore(mockDelegate);

        assertEquals(Data.BAR_UPPERCASE.labelString, store.addLabel(Data.BAR_UPPERCASE.issueId, Data.BAR_UPPERCASE.customFieldId, Data.BAR_UPPERCASE.labelString).getLabel());
        ((MockOfBizDelegator) mockDelegate).verifyAll();
    }

    @Test
    public void testAddExistingLabelNoop() {
        expectFindLabel(mockDelegate, Data.BAR.labelString)
                .thenReturn(Arrays.asList(Data.BAR.genericValue));

        assertEquals(Data.BAR.label, store.addLabel(Data.BAR.issueId, Data.BAR.customFieldId, Data.BAR.labelString));
    }

    @Test
    public void testRemoveLabelsForAnIssue() {
        mockDelegate = new MockOfBizDelegator(
                Arrays.asList(Data.FOO.genericValue, Data.BAR.genericValue, Data.QUX.genericValue),
                Arrays.asList(Data.QUX.genericValue)
        );
        store = new OfBizLabelStore(mockDelegate);

        assertEquals(Collections.<Label>emptySet(), store.setLabels(ISSUE_1_ID, CUSTOM_FIELD_1_ID, Collections.<String>emptySet()));
        ((MockOfBizDelegator) mockDelegate).verifyAll();
    }

    @Test
    public void testRemoveNonexistentLabel() {
        mockDelegate = new MockOfBizDelegator(
                Arrays.asList(Data.FOO.genericValue, Data.BAR.genericValue),
                Arrays.asList(Data.FOO.genericValue, Data.BAR.genericValue)
        );
        store = new OfBizLabelStore(mockDelegate);

        final Long nonexistentLabelId = 3L;
        assertThat(Data.FOO.labelId, not(equalTo(nonexistentLabelId)));
        assertThat(Data.BAR.labelId, not(equalTo(nonexistentLabelId)));
        store.removeLabel(nonexistentLabelId, ISSUE_1_ID, CUSTOM_FIELD_1_ID);
        ((MockOfBizDelegator) mockDelegate).verifyAll();
    }

    @Test
    public void testRemoveOnlyUsesLabelId() {
        mockDelegate = new MockOfBizDelegator(
                Arrays.asList(Data.FOO.genericValue, Data.BAR.genericValue),
                Arrays.asList(Data.FOO.genericValue)
        );
        store = new OfBizLabelStore(mockDelegate);

        Long nonexistentIssueId = 3L;
        Long nonexistentCustomFieldId = 10000L;
        assertThat(Data.BAR.issueId, not(equalTo(nonexistentIssueId)));
        assertThat(Data.BAR.customFieldId, not(equalTo(nonexistentCustomFieldId)));

        store.removeLabel(Data.BAR.labelId, nonexistentIssueId, nonexistentCustomFieldId);
        ((MockOfBizDelegator) mockDelegate).verifyAll();
    }

    private static OngoingStubbing<List<GenericValue>> expectFindAllLabels(OfBizDelegator mockDelegate) {
        return when(mockDelegate.findByAnd(OfBizLabelStore.TABLE, MapBuilder.<String, Object>newBuilder()
                        .add(Columns.ISSUE_ID, ISSUE_1_ID)
                        .add(Columns.CUSTOM_FIELD_ID, CUSTOM_FIELD_1_ID)
                        .toMap()
        ));
    }

    private static OngoingStubbing<List<GenericValue>> expectFindLabel(OfBizDelegator mockDelegate, String label) {
        return when(mockDelegate.findByAnd(OfBizLabelStore.TABLE, MapBuilder.<String, Object>newBuilder()
                        .add(Columns.ISSUE_ID, ISSUE_1_ID)
                        .add(Columns.CUSTOM_FIELD_ID, CUSTOM_FIELD_1_ID)
                        .add(Columns.LABEL, label)
                        .toMap()
        ));
    }

    private static OngoingStubbing<GenericValue> expectAddLabel(OfBizDelegator mockDelegate, String label) {
        return when(mockDelegate.createValue(OfBizLabelStore.TABLE, MapBuilder.<String, Object>newBuilder()
                        .add(Columns.ISSUE_ID, ISSUE_1_ID)
                        .add(Columns.CUSTOM_FIELD_ID, CUSTOM_FIELD_1_ID)
                        .add(Columns.LABEL, label)
                        .toMap()
        ));
    }
}
