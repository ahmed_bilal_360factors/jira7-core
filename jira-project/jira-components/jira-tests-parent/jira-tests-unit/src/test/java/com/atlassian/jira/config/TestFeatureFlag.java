package com.atlassian.jira.config;


import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TestFeatureFlag {
    @Test
    public void testCreateFeatureFlagWithDefaultFalseStatus() {
        FeatureFlag featureFlag = FeatureFlag.featureFlag("feature");
        assertTrue(featureFlag.featureKey().equals("feature"));
        assertFalse(featureFlag.isOnByDefault());
        assertTrue(featureFlag.enabledFeatureKey().equals("feature" + FeatureFlag.POSTFIX_ENABLED));
        assertTrue(featureFlag.disabledFeatureKey().equals("feature" + FeatureFlag.POSTFIX_DISABLED));
    }

    @Test
    public void testCreateFeatureFlagWithStatus() {
        FeatureFlag featureFlag = FeatureFlag.featureFlag("feature").onByDefault();
        assertTrue(featureFlag.featureKey().equals("feature"));
        assertTrue(featureFlag.isOnByDefault());
        assertTrue(featureFlag.enabledFeatureKey().equals("feature" + FeatureFlag.POSTFIX_ENABLED));
        assertTrue(featureFlag.disabledFeatureKey().equals("feature" + FeatureFlag.POSTFIX_DISABLED));
    }
}
