package com.atlassian.jira.help;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.fugue.Option;
import com.atlassian.jira.util.JiraProductInformation;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

public class BootstrapApplicationHelpSpaceProviderTest {
    @Rule
    public MockitoRule initMockito = MockitoJUnit.rule();

    @Mock
    private JiraProductInformation jiraProductInformation;

    @InjectMocks
    BoostrapApplicationHelpSpaceProvider helpSpaceProvider;

    @Test
    public void getHelpSpaceURIReadsApplicationKeyFromJiraProductInformation() throws Exception {
        final String spaceURI = "some-spaceURI-072";
        when(jiraProductInformation.getSetupHelpApplicationHelpSpace()).thenReturn(spaceURI);

        assertThat(helpSpaceProvider.getHelpSpace(ApplicationKey.valueOf("some-key")), is(Option.some(spaceURI)));
    }

    @Test
    public void getHelpSpaceURIEffectivelyIgnoresApplicationKey() throws Exception {
        final String spaceURI = "SOMESPACEURI";
        when(jiraProductInformation.getSetupHelpApplicationHelpSpace()).thenReturn(spaceURI);

        assertThat(helpSpaceProvider.getHelpSpace(null), is(Option.some(spaceURI)));
    }

}