package com.atlassian.jira.io;

import com.atlassian.fugue.Option;
import com.atlassian.jira.matchers.OptionMatchers;
import com.atlassian.plugin.Plugin;
import org.apache.commons.io.IOUtils;
import org.hamcrest.Matchers;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import java.io.File;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

import static com.atlassian.jira.matchers.OptionMatchers.some;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestClassPathResourceLoader {
    public static final String FILE_NAME = "TestClassLoaderCoreResources.txt";
    private static final String EXISTING_RESOURCE = "/com/atlassian/jira/io/" + FILE_NAME;
    private static final String NON_EXISTING_RESOURCE = "this resource should never exist ;)";
    private final ClassPathResourceLoader loader = new ClassPathResourceLoader();
    @Rule
    public final MethodRule initMockito = MockitoJUnit.rule();
    @Mock
    private Plugin plugin;

    @Test
    public void shoutReturnStreamForExistingResource() throws Exception {
        final Option<InputStream> resource = loader.getResourceAsStream(EXISTING_RESOURCE);
        try {
            assertThat(resource, some(Matchers.any(InputStream.class)));
            assertThat(IOUtils.toString(resource.get()), equalTo("YOU LOADED ME?"));
        } finally {
            IOUtils.closeQuietly(resource.getOrNull());
        }
    }

    @Test
    public void shoutReturnUrlForExistingResource() throws Exception {
        final Option<URL> resource = loader.getResource(EXISTING_RESOURCE);
        assertThat(resource, some(Matchers.any(URL.class)));
        assertThat(new File(resource.get().getFile()).getName(), equalTo(FILE_NAME));
    }

    @Test
    public void shoutReturnNoneStreamForNotExistingResource() throws Exception {
        final Option<URL> resource = loader.getResource(NON_EXISTING_RESOURCE);
        assertThat(resource, OptionMatchers.none(URL.class));
    }

    @Test
    public void shoutReturnNoneUrlForNotExistingResource() throws Exception {
        final Option<InputStream> resource = loader.getResourceAsStream(NON_EXISTING_RESOURCE);
        assertThat(resource, OptionMatchers.none(InputStream.class));
    }

    @Test
    public void shouldUserPluginToLoadItsResourcesAsStream() {
        final InputStream mockStream = mock(InputStream.class);
        when(plugin.getResourceAsStream(EXISTING_RESOURCE)).thenReturn(mockStream);

        final Option<InputStream> resource = loader.getResourceAsStream(plugin, EXISTING_RESOURCE);

        assertThat(resource, some(mockStream));
    }

    @Test
    public void shouldUserPluginToLoadItsResources() throws MalformedURLException {
        final URL url = new URL("file:///aaa");
        when(plugin.getResource(EXISTING_RESOURCE)).thenReturn(url);

        final Option<URL> resource = loader.getResource(plugin, EXISTING_RESOURCE);

        assertThat(resource.map(URL::toExternalForm), some(url.toExternalForm()));
    }
}