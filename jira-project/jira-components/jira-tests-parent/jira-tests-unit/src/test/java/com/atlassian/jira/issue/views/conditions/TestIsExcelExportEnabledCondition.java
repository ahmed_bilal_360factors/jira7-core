package com.atlassian.jira.issue.views.conditions;

import java.util.Map;

import com.atlassian.jira.config.FeatureManager;

import com.atlassian.jira.config.properties.ApplicationProperties;
import com.google.common.collect.ImmutableMap;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

public class TestIsExcelExportEnabledCondition {

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private ApplicationProperties applicationProperties;

    private IsExcelExportEnabledCondition condition;
    private Map<String, Object> context = ImmutableMap.of();

    @Before
    public void setUp() {
        condition = new IsExcelExportEnabledCondition(applicationProperties);
    }

    @Test
    public void shouldDisplayWhenApplicationPropertyIsEnabled() {
        when(applicationProperties.getOption(IsExcelExportEnabledCondition.FEATURE_EXCEL_EXPORT_ENABLED)).thenReturn(true);
        assertThat(condition.shouldDisplay(context), equalTo(true));
    }

    @Test
    public void shouldNotDisplayApplicationPropertyIsDisabled() {
        when(applicationProperties.getOption(IsExcelExportEnabledCondition.FEATURE_EXCEL_EXPORT_ENABLED)).thenReturn(false);
        assertThat(condition.shouldDisplay(context), equalTo(false));
    }
}
