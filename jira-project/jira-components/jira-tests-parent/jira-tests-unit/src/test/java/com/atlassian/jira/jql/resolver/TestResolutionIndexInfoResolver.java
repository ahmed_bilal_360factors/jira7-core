package com.atlassian.jira.jql.resolver;

import com.atlassian.jira.issue.resolution.Resolution;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.when;

/**
 * Unit test for {@link com.atlassian.jira.jql.resolver.ResolutionIndexInfoResolver}.
 *
 * @since v4.0
 */
public class TestResolutionIndexInfoResolver {
    @Rule
    public MethodRule initMockito = MockitoJUnit.rule();

    @Mock
    private NameResolver<Resolution> mockNameResolver;

    @Test(expected = RuntimeException.class)
    public void testConstructor() {
        new ResolutionIndexInfoResolver(null);
        fail("epxected problemo constructing from nulls"); // throws AssertionError, not RuntimeException
    }

    @Test
    public void testGetIndexedValuesStringUnresolved() {
        ResolutionIndexInfoResolver resolver = new ResolutionIndexInfoResolver(mockNameResolver);

        List<String> expectedIds = Collections.singletonList("-1");
        assertEquals(expectedIds, resolver.getIndexedValues("unresolved"));
    }

    @Test
    public void testGetIndexedValuesStringUnresolvedQuoted() {
        when(mockNameResolver.getIdsFromName("unresolved"))
                .thenReturn(Collections.singletonList("123"));

        ResolutionIndexInfoResolver resolver = new ResolutionIndexInfoResolver(mockNameResolver);

        List<String> expectedIds = Collections.singletonList("123");
        assertEquals(expectedIds, resolver.getIndexedValues("'unresolved'"));
    }

    @Test
    public void testGetIndexedValuesStringSomethingElse() {
        when(mockNameResolver.getIdsFromName("somethingelse"))
                .thenReturn(Collections.singletonList("123"));

        ResolutionIndexInfoResolver resolver = new ResolutionIndexInfoResolver(mockNameResolver);

        List<String> expectedIds = Collections.singletonList("123");
        assertEquals(expectedIds, resolver.getIndexedValues("somethingelse"));
    }

    @Test
    public void testCleanOperand() throws Exception {
        assertEquals("fixed", ResolutionIndexInfoResolver.cleanOperand("fixed"));
        assertEquals("'fixed'", ResolutionIndexInfoResolver.cleanOperand("'fixed'"));
        assertEquals("''fixed''", ResolutionIndexInfoResolver.cleanOperand("''fixed''"));
        assertEquals("\"fixed\"", ResolutionIndexInfoResolver.cleanOperand("\"fixed\""));
        assertEquals("\"\"fixed\"\"", ResolutionIndexInfoResolver.cleanOperand("\"\"fixed\"\""));

        assertEquals("UNRESolved", ResolutionIndexInfoResolver.cleanOperand("UNRESolved"));
        assertEquals("UNRESolved", ResolutionIndexInfoResolver.cleanOperand("'UNRESolved'"));
        assertEquals("'UNRESolved'", ResolutionIndexInfoResolver.cleanOperand("''UNRESolved''"));
        assertEquals("UNRESolved", ResolutionIndexInfoResolver.cleanOperand("\"UNRESolved\""));
        assertEquals("\"UNRESolved\"", ResolutionIndexInfoResolver.cleanOperand("\"\"UNRESolved\"\""));
        assertEquals("'UNRESolved", ResolutionIndexInfoResolver.cleanOperand("''UNRESolved'"));

        assertEquals(" 'UNRESolved'", ResolutionIndexInfoResolver.cleanOperand(" 'UNRESolved'"));
        assertEquals("s 'UNRESolved'", ResolutionIndexInfoResolver.cleanOperand("s 'UNRESolved'"));
        assertEquals("s 's'UNRESolved's'", ResolutionIndexInfoResolver.cleanOperand("s 's'UNRESolved's'"));
    }
}
