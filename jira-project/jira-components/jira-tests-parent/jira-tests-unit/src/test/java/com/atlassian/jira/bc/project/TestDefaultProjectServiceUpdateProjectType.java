package com.atlassian.jira.bc.project;

import com.atlassian.beehive.ClusterLock;
import com.atlassian.beehive.ClusterLockService;
import com.atlassian.fugue.Either;
import com.atlassian.jira.association.NodeAssociationStore;
import com.atlassian.jira.avatar.AvatarManager;
import com.atlassian.jira.bc.project.component.ProjectComponentManager;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.event.project.ProjectEventManager;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.fields.screen.issuetype.IssueTypeScreenSchemeManager;
import com.atlassian.jira.issue.security.IssueSecuritySchemeManager;
import com.atlassian.jira.matchers.ErrorCollectionMatcher;
import com.atlassian.jira.notification.NotificationSchemeManager;
import com.atlassian.jira.permission.PermissionSchemeManager;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectCreateNotifier;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.project.template.ProjectTemplateManager;
import com.atlassian.jira.project.type.ProjectTypeKey;
import com.atlassian.jira.project.type.ProjectTypeUpdatedNotifier;
import com.atlassian.jira.project.util.ProjectKeyStore;
import com.atlassian.jira.project.version.VersionManager;
import com.atlassian.jira.scheme.SchemeFactory;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.sharing.SharePermissionDeleteUtils;
import com.atlassian.jira.task.TaskManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.NoopI18nFactory;
import com.atlassian.jira.workflow.WorkflowManager;
import com.atlassian.jira.workflow.WorkflowSchemeManager;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestDefaultProjectServiceUpdateProjectType {
    private static final ApplicationUser USER = new MockApplicationUser("user");
    private static final ProjectTypeKey OLD_PROJECT_TYPE = new ProjectTypeKey("old-type");
    private static final ProjectTypeKey NEW_PROJECT_TYPE = new ProjectTypeKey("new-type");
    private static final String PROJECT_KEY = "KEY";

    private static final Project PROJECT = new MockProject(1, PROJECT_KEY, OLD_PROJECT_TYPE);
    private static final Project UPDATED_PROJECT = new MockProject(2, PROJECT_KEY, NEW_PROJECT_TYPE);

    @Mock
    private ProjectManager projectManager;
    @Mock
    private ProjectEventManager projectEventManager;
    @Mock
    private ProjectTypeValidator projectTypeValidator;
    @Mock
    private ProjectTypeUpdatedNotifier notifier;
    @Mock
    private ClusterLockService lockService;

    private ProjectService projectService;

    @Before
    public void setUp() {
        projectService = new ProjectService(
                projectManager,
                projectEventManager,
                new NoopI18nFactory(),
                projectTypeValidator,
                notifier,
                lockService
        );
        mockClusterLock();
    }

    @Test
    public void returnsAnErrorIfTheUserDoesNotHavePermissionToUpdateTheProjectType() {
        projectService.userDoesNotHavePermissionToUpdateProjectType(USER, PROJECT);
        when(projectTypeValidator.isValid(USER, NEW_PROJECT_TYPE)).thenReturn(true);

        Either<Project, ErrorCollection> result = projectService.updateProjectType(USER, PROJECT, NEW_PROJECT_TYPE);

        ErrorCollection errorCollection = result.right().get();
        assertErrorMessageWasAdded(errorCollection, "admin.errors.project.type.update.no.permission{[]}");
        assertReasonWasAdded(errorCollection, ErrorCollection.Reason.FORBIDDEN);
    }

    @Test
    public void returnsAnErrorIfTheNewProjectTypeIsNotValid() {
        projectService.userHasPermissionToUpdateProjectType(USER, PROJECT);
        when(projectTypeValidator.isValid(USER, NEW_PROJECT_TYPE)).thenReturn(false);

        Either<Project, ErrorCollection> result = projectService.updateProjectType(USER, PROJECT, NEW_PROJECT_TYPE);

        ErrorCollection errorCollection = result.right().get();
        assertThat(errorCollection, ErrorCollectionMatcher.hasError(ProjectService.PROJECT_TYPE, "admin.errors.invalid.project.type{[]}"));
        assertReasonWasAdded(errorCollection, ErrorCollection.Reason.VALIDATION_FAILED);
    }

    @Test
    public void updatesTheProjectTypeOnTheDatabase() {
        projectService.userHasPermissionToUpdateProjectType(USER, PROJECT);
        when(projectTypeValidator.isValid(USER, NEW_PROJECT_TYPE)).thenReturn(true);
        when(projectManager.updateProjectType(USER, PROJECT, NEW_PROJECT_TYPE)).thenReturn(UPDATED_PROJECT);
        when(notifier.notifyAllHandlers(USER, UPDATED_PROJECT, OLD_PROJECT_TYPE, NEW_PROJECT_TYPE)).thenReturn(true);

        Either<Project, ErrorCollection> result = projectService.updateProjectType(USER, PROJECT, NEW_PROJECT_TYPE);

        assertThat(result.left().get(), is(UPDATED_PROJECT));
    }

    @Test
    public void notifiesAllHandlersOfAProjectTypeUpdate() {
        projectService.userHasPermissionToUpdateProjectType(USER, PROJECT);
        when(projectTypeValidator.isValid(USER, NEW_PROJECT_TYPE)).thenReturn(true);
        when(projectManager.updateProjectType(USER, PROJECT, NEW_PROJECT_TYPE)).thenReturn(UPDATED_PROJECT);

        projectService.updateProjectType(USER, PROJECT, NEW_PROJECT_TYPE);

        verify(notifier, times(1)).notifyAllHandlers(USER, UPDATED_PROJECT, OLD_PROJECT_TYPE, NEW_PROJECT_TYPE);
    }

    @Test
    public void rollsBackTheProjectUpdateIfOneOfTheHandlersReturnsAnError() {
        projectService.userHasPermissionToUpdateProjectType(USER, PROJECT);
        when(projectTypeValidator.isValid(USER, NEW_PROJECT_TYPE)).thenReturn(true);
        when(projectManager.updateProjectType(USER, PROJECT, NEW_PROJECT_TYPE)).thenReturn(UPDATED_PROJECT);

        when(notifier.notifyAllHandlers(USER, UPDATED_PROJECT, OLD_PROJECT_TYPE, NEW_PROJECT_TYPE)).thenReturn(false);

        Either<Project, ErrorCollection> result = projectService.updateProjectType(USER, PROJECT, NEW_PROJECT_TYPE);

        verify(projectManager).updateProjectType(USER, UPDATED_PROJECT, OLD_PROJECT_TYPE);
        assertThat(result.right().isDefined(), is(true));
    }

    private void assertErrorMessageWasAdded(ErrorCollection errorCollection, String expectedError) {
        assertTrue("Expected some errors", errorCollection.hasAnyErrors());
        assertThat(errorCollection.getErrorMessages(), Matchers.containsInAnyOrder(expectedError));
    }

    private void assertReasonWasAdded(ErrorCollection errorCollection, ErrorCollection.Reason reason) {
        assertThat(errorCollection.getReasons().contains(reason), is(true));
    }

    private void mockClusterLock() {
        when(lockService.getLockForName(anyString())).thenReturn(mock(ClusterLock.class));
    }

    private static class ProjectService extends DefaultProjectService {
        private ApplicationUser userToCheck;
        private Project projectToCheck;
        private boolean hasPermission;

        public ProjectService(ProjectManager projectManager, ProjectEventManager projectEventManager, I18nHelper.BeanFactory i18nBeanFactory, ProjectTypeValidator projectTypeValidator, ProjectTypeUpdatedNotifier notifier, ClusterLockService clusterLockService) {
            super(
                    mock(JiraAuthenticationContext.class),
                    projectManager,
                    mock(ApplicationProperties.class),
                    mock(PermissionManager.class),
                    mock(GlobalPermissionManager.class),
                    mock(PermissionSchemeManager.class),
                    mock(NotificationSchemeManager.class),
                    mock(IssueSecuritySchemeManager.class),
                    mock(SchemeFactory.class),
                    mock(WorkflowSchemeManager.class),
                    mock(IssueTypeScreenSchemeManager.class),
                    mock(CustomFieldManager.class),
                    mock(NodeAssociationStore.class),
                    mock(VersionManager.class),
                    mock(ProjectComponentManager.class),
                    mock(SharePermissionDeleteUtils.class),
                    mock(AvatarManager.class),
                    i18nBeanFactory,
                    mock(WorkflowManager.class),
                    mock(UserManager.class),
                    projectEventManager,
                    mock(ProjectKeyStore.class),
                    projectTypeValidator,
                    mock(ProjectCreateNotifier.class),
                    notifier,
                    clusterLockService,
                    mock(ProjectTemplateManager.class),
                    mock(ProjectSchemeAssociationManager.class),
                    mock(TaskManager.class),
                    mock(IssueManager.class)
            );
        }

        public void userDoesNotHavePermissionToUpdateProjectType(ApplicationUser user, Project project) {
            userToCheck = user;
            projectToCheck = project;
            hasPermission = false;
        }

        public void userHasPermissionToUpdateProjectType(ApplicationUser user, Project project) {
            userToCheck = user;
            projectToCheck = project;
            hasPermission = true;
        }

        @Override
        boolean checkActionPermission(ApplicationUser user, Project project, ProjectAction action) {
            if (action.equals(ProjectAction.EDIT_PROJECT_CONFIG) && user.equals(userToCheck) && project.getKey().equals(projectToCheck.getKey())) {
                return hasPermission;
            }
            return false;
        }
    }
}
