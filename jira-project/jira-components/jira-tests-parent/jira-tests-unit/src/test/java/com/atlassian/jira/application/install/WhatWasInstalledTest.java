package com.atlassian.jira.application.install;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static com.atlassian.jira.application.install.ThrowableFunctions.uncheckFunction;
import static java.lang.String.format;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 */
public class WhatWasInstalledTest {
    private static final String APPLICATION_NAME = "some-application";
    private static final String PLUGIN_FILE_BASE = "some.plugin.file.%d.jar";

    @Rule
    public TemporaryFolder temporaryFolder = new TemporaryFolder();

    @Rule
    public MockitoRule mocks = MockitoJUnit.rule();

    @Mock
    ApplicationInstallationEnvironment applicationInstallationEnvironment;

    @Mock
    BundlesVersionDiscovery bundlesVersionDiscovery;

    @Mock
    WhatWasInstalledInApplicationFactory whatWasInstalledInApplicationFactory;
    @InjectMocks
    WhatWasInstalled testObj;
    private File informationFolder;
    private File[] sourceFiles;
    private BundlesVersionDiscovery.PluginIdentification[] pluginIdentifications;

    @Before
    public void setup() throws IOException {
        informationFolder = temporaryFolder.newFolder("install-info");
        when(applicationInstallationEnvironment.getInstallInformationDir()).thenReturn(informationFolder);
        when(bundlesVersionDiscovery.getBundleNameAndVersion(Mockito.any(File.class))).thenAnswer(
                invocation -> {
                    final File file = (File) invocation.getArguments()[0];

                    return Optional.ofNullable(getPluginIdentificationFromFilename(file.getName()));
                }
        );

        final List<String> mockPluginFilenames = IntStream.range(0, 5)
                .mapToObj(value -> format(PLUGIN_FILE_BASE, value))
                .collect(Collectors.toList());
        sourceFiles = mockPluginFilenames.stream()
                .map(uncheckFunction((ThrowableFunctions.ThrowingFunction<String, File, IOException>) temporaryFolder::newFile))
                .toArray(File[]::new);
        pluginIdentifications = mockPluginFilenames.stream()
                .map(WhatWasInstalledTest::getPluginIdentificationFromFilename)
                .toArray(BundlesVersionDiscovery.PluginIdentification[]::new);

    }

    @Test
    public void shouldLoadConfigurationFileAndVerifyItAgainstCurrentPlugins() throws IOException {
        // given
        final WhatWasInstalledInApplication whatWasInstalledInApplication = Mockito.mock(WhatWasInstalledInApplication.class);
        when(whatWasInstalledInApplication.wereBundlesInstalled(Mockito.any())).thenReturn(false);
        final File applicationConfigFile = new File(informationFolder, String.format(WhatWasInstalled.APP_INSTALL_INFO_FILE_BASE, APPLICATION_NAME));
        when(whatWasInstalledInApplicationFactory.load(applicationConfigFile)).thenReturn(whatWasInstalledInApplication);

        // when
        final boolean wasApplicationSetInstalled = testObj.wasApplicationSourceInstalled(new ApplicationSource(APPLICATION_NAME, sourceFiles));

        // then
        assertThat(wasApplicationSetInstalled, is(false));
        verify(whatWasInstalledInApplication).wereBundlesInstalled(
                argThatMatchesMockPluginIds());
    }

    @Test
    public void shouldStoreObtainedPluginIdentificationFromPluginFiles() throws IOException {
        // given
        final WhatWasInstalledInApplication whatWasInstalledInApplication = Mockito.mock(WhatWasInstalledInApplication.class);
        final ReversibleFileOperations reversibleFileOperations = new ReversibleFileOperations();

        when(whatWasInstalledInApplicationFactory.load(argThatMatchesMockPluginIds())).thenReturn(whatWasInstalledInApplication);

        // when
        testObj.storeInstalledApplicationSource(new ApplicationSource(APPLICATION_NAME, sourceFiles), reversibleFileOperations);

        // then
        final File applicationConfigFile = new File(informationFolder, String.format(WhatWasInstalled.APP_INSTALL_INFO_FILE_BASE, APPLICATION_NAME));
        verify(whatWasInstalledInApplicationFactory).store(
                Mockito.eq(applicationConfigFile),
                Mockito.refEq(whatWasInstalledInApplication),
                Mockito.refEq(reversibleFileOperations)
        );
    }

    private static BundlesVersionDiscovery.PluginIdentification getPluginIdentificationFromFilename(final String fileName) {
        return new BundlesVersionDiscovery.PluginIdentification(
                fileName,
                String.valueOf(fileName.hashCode() % 10)
        );
    }

    @SuppressWarnings("unchecked")
    private Collection<BundlesVersionDiscovery.PluginIdentification> argThatMatchesMockPluginIds() {
        return (Collection<BundlesVersionDiscovery.PluginIdentification>) argThat(contains(
                pluginIdentifications
        ));
    }
}