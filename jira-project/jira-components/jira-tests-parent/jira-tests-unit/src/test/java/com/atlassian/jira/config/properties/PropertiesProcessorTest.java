package com.atlassian.jira.config.properties;

import com.atlassian.jira.matchers.MapMatchers;
import com.google.common.collect.ImmutableMap;
import org.hamcrest.Matchers;
import org.junit.Test;

import java.util.Map;
import java.util.Properties;
import java.util.regex.Pattern;

import static org.hamcrest.MatcherAssert.assertThat;

public class PropertiesProcessorTest {
    @Test
    public void sanitiseReplacesMapValues() {
        //given
        Properties props = new Properties();
        props.put("something", 6);
        props.put("somethingElse", 1819L);
        props.put("noMatchsomething", true);

        final Pattern pattern = Pattern.compile("^some");
        final String replace = "***";

        //when
        final Map<String, String> output = new PropertiesProcessor(props).sanitise(pattern, replace).toMap();

        //then
        assertThat(output, Matchers.equalTo(ImmutableMap.of("something", replace,
                "somethingElse", replace, "noMatchsomething", "true")));
    }

    @Test
    public void sortActuallyDoesOrder() {
        //given
        Properties props = new Properties();
        props.put("XYZ", 1);
        props.put("ABC", 2);
        props.put("xy", 3);

        //when
        final Map<String, String> output = new PropertiesProcessor(props).sort().toMap();

        //then
        assertThat(output, MapMatchers.orderedMap("ABC", "2", "xy", "3", "XYZ", "1"));
    }

    @Test
    public void sanitisePasswordReplacesValues() {
        //given
        Properties props = new Properties();
        props.put("atlassian.recovery.password", 6);
        props.put("atlassian.recovery.PASSWORD", 6);
        props.put("somethingElse", 1819L);

        final String replace = "****";

        //when
        final Map<String, String> output = new PropertiesProcessor(props).sanitisePassword().toMap();

        //then
        assertThat(output, Matchers.equalTo(ImmutableMap.of("atlassian.recovery.password", replace,
                "atlassian.recovery.PASSWORD", replace,
                "somethingElse", "1819")));
    }
}