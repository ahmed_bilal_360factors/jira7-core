package com.atlassian.jira.upgrade.tasks.role;

import com.atlassian.jira.auditing.AssociatedItem;
import com.atlassian.jira.auditing.ChangedValueImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class AuditEntryTest {
    @Test
    public void toLogMessageShouldTolerateNullChangedObjectAndEmptyChangedValues() {
        AuditEntry auditEntry = new AuditEntry(AuditEntryTest.class,
                "my summary", "a happy description",
                AssociatedItem.Type.APPLICATION_ROLE);
        String logMessage = auditEntry.toLogMessage();
        assertEquals("my summary - a happy description, type: APPLICATION_ROLE", logMessage);
    }

    @Test
    public void toLogMessageShouldIncludeDescriptionChangedObjectsAndChangedValues() {
        final String changedObject = "a changed object";
        AuditEntry auditEntry = new AuditEntry(AuditEntryTest.class,
                "my summary",
                "a happy  description",
                AssociatedItem.Type.APPLICATION_ROLE,
                changedObject, false,
                new ChangedValueImpl("gertrude-group", "admin-perm", "admin-perm, fighter-role"),
                new ChangedValueImpl("margaret-group", "use-perm", "fighter-role"));
        String logMessage = auditEntry.toLogMessage();
        assertEquals("my summary - a happy  description - a changed object - (gertrude-group: admin-perm --> admin-perm"
                + ", fighter-role), (margaret-group: use-perm --> fighter-role), type: APPLICATION_ROLE", logMessage);
    }

}
