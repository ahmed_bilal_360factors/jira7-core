package com.atlassian.jira.project.template.hook;

import com.atlassian.jira.project.template.descriptor.ConfigTemplateParser;
import com.atlassian.jira.project.template.descriptor.ProjectTemplateModuleDescriptor;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginException;
import com.atlassian.plugin.module.ModuleFactory;
import org.junit.Test;

import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestAddProjectModuleImpl {
    @Test
    public void hasConfigTemplateReturnsFalseIfNoConfigFileIsPassed() {
        AddProjectModuleImpl module = moduleWithNoConfigFile();

        assertThat(module.hasConfigTemplate(), is(false));
    }

    @Test
    public void hasConfigTemplateReturnsTrueIfAConfigFileIsPassed() {
        AddProjectModuleImpl module = moduleWithAnyConfigFile();

        assertThat(module.hasConfigTemplate(), is(true));
    }

    @Test
    public void configTemplateIsCorrectlyParsedWhenAConfigFileIsPassed() {
        ConfigTemplate expectedConfigTemplate = configTemplateWithNoIssueTypeScheme();
        ConfigTemplateParser parser = configParserReturningTemplate(expectedConfigTemplate);

        AddProjectModuleImpl module = moduleWithConfigTemplateParser(parser);

        assertThat(module.configTemplate(), is(expectedConfigTemplate));
    }

    @Test
    public void configTemplateIsNotParsedWhenNoConfigFileIsPassed() {
        AddProjectModuleImpl module = moduleWithNoConfigFile();

        assertThat(module.configTemplate(), is(nullValue()));
    }

    @Test
    public void hasProjectCreateHookReturnsFalseIfNoHookClassIsPassed() {
        AddProjectModuleImpl module = moduleWithNoHookClass();

        assertThat(module.hasProjectCreateHook(), is(false));
    }

    @Test
    public void hasProjectCreateHookReturnsTrueIfAHookClassIsPassed() {
        AddProjectModuleImpl module = moduleWithAHookClass();

        assertThat(module.hasProjectCreateHook(), is(true));
    }

    @Test
    public void addProjectHookReturnsNullIfThereIsNoHookClass() {
        AddProjectModuleImpl module = moduleWithNoHookClass();

        assertThat(module.addProjectHook(), is(nullValue()));
    }

    @Test(expected = PluginException.class)
    public void addProjectHookReturnsExceptionIfTheClassedPassedAsHookIsNotAnAddProjectHook() {
        AddProjectModuleImpl module = moduleWithHookOfWrongClass();

        module.addProjectHook();
    }

    @Test
    public void addProjectHookCorrectlyInstantiatesTheAddProjectHookGivenACorrectClass() {
        AddProjectHook hook = mock(AddProjectHook.class);
        AddProjectModuleImpl module = moduleWithParsedHook(hook);

        AddProjectHook addProjectHook = module.addProjectHook();

        assertThat(addProjectHook, is(hook));
    }

    private AddProjectModuleImpl moduleWithNoConfigFile() {
        return new AddProjectModuleImpl(
                mock(ModuleFactory.class),
                mock(ProjectTemplateModuleDescriptor.class),
                "add-project-hook-class",
                null,
                anyConfigParser()
        );
    }

    private AddProjectModuleImpl moduleWithAnyConfigFile() {
        ConfigTemplateParser parser = mock(ConfigTemplateParser.class);
        String configFile = "config-file";
        ConfigTemplate configTemplate = configTemplateWithNoIssueTypeScheme();
        when(parser.parse(eq(configFile), any(Plugin.class))).thenReturn(configTemplate);

        return new AddProjectModuleImpl(
                mock(ModuleFactory.class),
                mock(ProjectTemplateModuleDescriptor.class),
                "add-project-hook-class",
                configFile,
                parser
        );
    }

    private AddProjectModuleImpl moduleWithConfigTemplateParser(ConfigTemplateParser parser) {
        return new AddProjectModuleImpl(
                mock(ModuleFactory.class),
                mock(ProjectTemplateModuleDescriptor.class),
                "add-project-hook-class",
                "any-file",
                parser
        );
    }

    private AddProjectModuleImpl moduleWithNoHookClass() {
        return new AddProjectModuleImpl(
                mock(ModuleFactory.class),
                mock(ProjectTemplateModuleDescriptor.class),
                null,
                "config-file",
                anyConfigParser()
        );
    }

    private AddProjectModuleImpl moduleWithAHookClass() {
        return new AddProjectModuleImpl(
                mock(ModuleFactory.class),
                mock(ProjectTemplateModuleDescriptor.class),
                "add-project-hook-class",
                "config-file",
                anyConfigParser()
        );
    }

    private AddProjectModuleImpl moduleWithHookOfWrongClass() {
        ModuleFactory moduleFactory = mock(ModuleFactory.class);
        ProjectTemplateModuleDescriptor moduleDescriptor = mock(ProjectTemplateModuleDescriptor.class);
        String hookOfWrongClass = "";
        doReturn(hookOfWrongClass).when(moduleFactory).createModule("add-project-hook-class", moduleDescriptor);

        return new AddProjectModuleImpl(
                moduleFactory,
                moduleDescriptor,
                "add-project-hook-class",
                "config-file",
                anyConfigParser()
        );
    }

    private AddProjectModuleImpl moduleWithParsedHook(AddProjectHook hook) {
        ModuleFactory moduleFactory = mock(ModuleFactory.class);
        ProjectTemplateModuleDescriptor moduleDescriptor = mock(ProjectTemplateModuleDescriptor.class);
        doReturn(hook).when(moduleFactory).createModule("add-project-hook-class", moduleDescriptor);

        return new AddProjectModuleImpl(
                moduleFactory,
                moduleDescriptor,
                "add-project-hook-class",
                "config-file",
                anyConfigParser()
        );
    }

    private ConfigTemplateParser anyConfigParser() {
        ConfigTemplate configTemplate = configTemplateWithNoIssueTypeScheme();
        return configParserReturningTemplate(configTemplate);
    }

    private ConfigTemplateParser configParserReturningTemplate(ConfigTemplate template) {
        ConfigTemplateParser parser = mock(ConfigTemplateParser.class);
        when(parser.parse(any(String.class), any(Plugin.class))).thenReturn(template);
        return parser;
    }

    private ConfigTemplate configTemplateWithNoIssueTypeScheme() {
        ConfigTemplate configTemplate = mock(ConfigTemplate.class);
        when(configTemplate.issueTypeSchemeTemplate()).thenReturn(Optional.empty());
        return configTemplate;
    }
}
