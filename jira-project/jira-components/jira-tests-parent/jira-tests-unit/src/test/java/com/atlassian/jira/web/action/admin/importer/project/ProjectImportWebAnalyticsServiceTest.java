package com.atlassian.jira.web.action.admin.importer.project;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ProjectImportWebAnalyticsServiceTest {
    @Mock
    private EventPublisher eventPublisher;

    @Mock
    private JiraWebActionSupport jiraWebActionSupport;

    @InjectMocks
    private ProjectImportWebAnalyticsService projectImportWebAnalyticsService;

    @Test
    public void testPublishAnalyticsEvent() {
        when(jiraWebActionSupport.getCommandName()).thenReturn("myaction");
        projectImportWebAnalyticsService.publishAnalyticsEvent(jiraWebActionSupport);
        ArgumentCaptor<ProjectImportWebAnalyticsEvent> eventCaptor = ArgumentCaptor.forClass(ProjectImportWebAnalyticsEvent.class);
        verify(eventPublisher).publish(eventCaptor.capture());
        assertThat(eventCaptor.getValue().getCommand(), is("myaction"));
    }

}
