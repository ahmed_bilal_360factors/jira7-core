package com.atlassian.jira.jql.query;

import com.atlassian.jira.issue.vote.DefaultVoteManager;
import com.atlassian.jira.jql.operand.JqlOperandResolver;
import com.atlassian.jira.jql.util.VotesIndexValueConverter;
import com.atlassian.jira.jql.validator.MockJqlOperandResolver;
import com.atlassian.query.clause.TerminalClauseImpl;
import com.atlassian.query.operator.Operator;
import org.apache.lucene.search.BooleanQuery;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestVotesClauseQueryFactory {
    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @Mock
    private DefaultVoteManager defaultVoteManager;
    @Mock
    private MockJqlOperandResolver jqlOperandResolver;
    @Mock
    private VotesIndexValueConverter votesIndexValueConverter;
    @Mock
    private GenericClauseQueryFactory genericClauseQueryFactory;

    @Test
    public void testVotingDisabled() throws Exception {
        when(defaultVoteManager.isVotingEnabled()).thenReturn(false);
        final VotesClauseQueryFactory factory = new VotesClauseQueryFactory(jqlOperandResolver, votesIndexValueConverter, defaultVoteManager);
        final TerminalClauseImpl clause = new TerminalClauseImpl("a", Operator.EQUALS, "clause");

        final QueryFactoryResult result = factory.getQuery(null, clause);

        assertEquals(QueryFactoryResult.createFalseResult(), result);
        verify(defaultVoteManager).isVotingEnabled();
    }

    @Test
    public void testVotingEnabled() throws Exception {
        final TerminalClauseImpl clause = new TerminalClauseImpl("a", Operator.EQUALS, "clause");
        when(defaultVoteManager.isVotingEnabled()).thenReturn(true);
        when(genericClauseQueryFactory.getQuery(null, clause)).thenReturn(new QueryFactoryResult(new BooleanQuery()));
        final VotesClauseQueryFactory factory = new VotesClauseQueryFactory(jqlOperandResolver, votesIndexValueConverter, defaultVoteManager) {
            @Override
            GenericClauseQueryFactory createGenericClauseFactory(final JqlOperandResolver operandResolver, final List<OperatorSpecificQueryFactory> operatorFactories) {
                return genericClauseQueryFactory;
            }
        };

        factory.getQuery(null, clause);

        verify(defaultVoteManager).isVotingEnabled();
        verify(genericClauseQueryFactory).getQuery(null, clause);
    }
}
