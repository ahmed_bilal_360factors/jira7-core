package com.atlassian.jira.jql.permission;

import com.atlassian.jira.issue.vote.VoteManager;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestVotePermissionChecker {
    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @Mock
    private VoteManager voteManager;

    @Test
    public void testHasPermissionToUseClauseVotingDisabled() throws Exception {
        when(voteManager.isVotingEnabled()).thenReturn(false);

        final VotePermissionChecker checker = new VotePermissionChecker(voteManager);
        assertFalse(checker.hasPermissionToUseClause(null));
    }

    @Test
    public void testHasPermissionToUseClauseVotingEnabled() throws Exception {
        when(voteManager.isVotingEnabled()).thenReturn(true);

        final VotePermissionChecker checker = new VotePermissionChecker(voteManager);
        assertTrue(checker.hasPermissionToUseClause(null));
    }
}
