package com.atlassian.jira.issue.customfields.converters;

import com.atlassian.jira.datetime.DateTimeFormatterFactoryStub;
import com.atlassian.jira.issue.customfields.impl.FieldValidationException;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.DateFieldFormat;
import com.atlassian.jira.util.DateFieldFormatImpl;
import com.atlassian.jira.util.I18nHelper;
import org.junit.Before;
import org.junit.Test;

import java.util.Calendar;
import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestDatePickerConverterImpl {

    private DatePickerConverter converter;

    @Before
    public void setUp() {
        JiraAuthenticationContext authenticationContext = mock(JiraAuthenticationContext.class);
        I18nHelper i18nHelper = mock(I18nHelper.class);
        when(i18nHelper.getText(anyString(), anyString())).thenReturn("");
        when(authenticationContext.getI18nHelper()).thenReturn(i18nHelper);
        DateFieldFormat dateFieldFormat = new DateFieldFormatImpl(new DateTimeFormatterFactoryStub());
        converter = new DatePickerConverterImpl(authenticationContext, dateFieldFormat);
    }

    @Test
    public void testConvertDate() {
        Date date = new Date(converter.getTimestamp("10/Dec/2000").getTime());
        assertDate(10, Calendar.DECEMBER, 2000, date);
    }

    @Test(expected = FieldValidationException.class)
    public void testConvertDateBeyondLimit() {
        converter.getTimestamp("10/Dec/20000");
    }

    public void assertDate(int d, int m, int y, Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        assertEquals(d, cal.get(Calendar.DAY_OF_MONTH));
        assertEquals(m, cal.get(Calendar.MONTH));
        assertEquals(y, cal.get(Calendar.YEAR));
    }
}
