package com.atlassian.jira.license;

import com.atlassian.application.api.ApplicationKey;
import org.junit.Test;

import static com.atlassian.jira.license.Licenses.LICENSE_ELA_4_ROLES;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Test that validates the JiraProductLicenseManager and its ability to decode a license string into a
 * JiraProductLicense.
 *
 * @since v7.0
 */
public class JiraProductLicenseManagerTest {
    @Test
    public void shouldReturnLicensedApplications() {
        //When
        final JiraProductLicense productLicense = getJiraProductLicense(LICENSE_ELA_4_ROLES.getLicenseString());

        //Then
        assertNotNull(productLicense.getApplications());
        assertLicenseIsForApplication(productLicense, "jira-servicedesk");
        assertLicenseIsForApplication(productLicense, "jira-func-test");
        assertLicenseIsForApplication(productLicense, "jira-software");
        assertLicenseIsForApplication(productLicense, "jira-core");
        assertLicenseIsNotForApplication(productLicense, "madeup");
    }

    @Test
    public void shouldDelegate() {
        //When
        final JiraProductLicense productLicense = getJiraProductLicense(LICENSE_ELA_4_ROLES.getLicenseString());

        //Then
        assertEquals("JIRA Enterprise: Commercial Server", productLicense.getDescription());
    }

    private void assertLicenseIsNotForApplication(final JiraProductLicense productLicense, final String applicationKey) {
        final ApplicationKey key = ApplicationKey.valueOf(applicationKey);
        assertFalse(productLicense.getApplications().getKeys().contains(key));
    }

    private void assertLicenseIsForApplication(final JiraProductLicense productLicense, final String applicationKey) {
        final ApplicationKey key = ApplicationKey.valueOf(applicationKey);
        assertTrue(productLicense.getApplications().getKeys().contains(key));
    }

    private JiraProductLicense getJiraProductLicense(final String licenseKey) {
        return LicenseDetailsFactoryImpl.JiraProductLicenseManager.INSTANCE.getProductLicense(licenseKey);
    }
}
