package com.atlassian.jira.application;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.jira.test.util.lic.ApplicationLicenseConstants;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class UndefinedApplicationRoleNameTest {
    @Before
    public void setUp() {
    }

    @Test
    public void getNameForCore() {
        UndefinedApplicationRoleName descriptor =
                UndefinedApplicationRoleName.of(ApplicationKey.valueOf(ApplicationLicenseConstants.CORE_KEY));
        assertThat(descriptor.getName(), equalTo("JIRA Core"));
    }

    @Test
    public void getNameForSoftware() {
        UndefinedApplicationRoleName descriptor =
                UndefinedApplicationRoleName.of(ApplicationKey.valueOf(ApplicationLicenseConstants.SOFTWARE_KEY));
        assertThat(descriptor.getName(), equalTo("JIRA Software"));
    }

    @Test
    public void getNameForServiceDesk() {
        UndefinedApplicationRoleName descriptor =
                UndefinedApplicationRoleName.of(ApplicationKey.valueOf(
                        ApplicationLicenseConstants.SERVICE_DESK_KEY));
        assertThat(descriptor.getName(), equalTo("JIRA Service Desk"));
    }


    @Test
    public void shouldReturnExpectedDisplayNameForUninstalledApplications() {
        assertExpectedDisplayNameForUninstalledApplication("jira-other", "JIRA Other");
        assertExpectedDisplayNameForUninstalledApplication("Jira-other.one.-is", "JIRA Other One Is");
        assertExpectedDisplayNameForUninstalledApplication("jiRa-other..one.-is---i", "JIRA Other One Is I");
    }

    public void assertExpectedDisplayNameForUninstalledApplication(final String applicationKey,
                                                                   final String expectedDisplayName) {
        ApplicationKey appKey = ApplicationKey.valueOf(applicationKey);
        UndefinedApplicationRoleName descriptor = UndefinedApplicationRoleName.of(appKey);
        final String displayName = descriptor.getName();
        assertEquals(expectedDisplayName, displayName);
    }
}
