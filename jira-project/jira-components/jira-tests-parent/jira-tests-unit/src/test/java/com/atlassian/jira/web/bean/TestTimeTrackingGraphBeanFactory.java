package com.atlassian.jira.web.bean;

import com.atlassian.jira.issue.util.AggregateTimeTrackingBean;
import com.atlassian.jira.mock.issue.MockIssue;
import com.atlassian.jira.util.JiraDurationUtils;
import com.atlassian.jira.util.NoopI18nHelper;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Locale;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

/**
 * Test for {@link TimeTrackingGraphBeanFactoryImpl}.
 *
 * @since v4.1
 */
public class TestTimeTrackingGraphBeanFactory {
    @Rule
    public MockitoRule mockito = MockitoJUnit.rule();
    @Mock
    private JiraDurationUtils durationUtils;

    private static final String SPENT_SHORT = "spentShort";
    private static final String ORIG_SHORT = "origShort";
    private static final String ESTIMATE_SHORT = "estimateShort";

    private static final String SPENT_LONG = "spentLong";
    private static final String ORIG_LONG = "origLong";
    private static final String ESTIMATE_LONG = "estimateLong";

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotBeAbleToPassNullInConstructor() throws Exception {
        new TimeTrackingGraphBeanFactoryImpl(null);
    }

    @Test
    public void testCreateBeanFromIssueShort() throws Exception {
        final Long timespent = 300L;
        final Long originalEstimate = 20L;
        final Long remainingEstimate = 10L;

        MockIssue issue = new MockIssue(7);
        issue.setEstimate(remainingEstimate);
        issue.setOriginalEstimate(originalEstimate);
        issue.setTimeSpent(timespent);

        final NoopI18nHelper helper = new NoopI18nHelper();

        when(durationUtils.getShortFormattedDuration(timespent, Locale.ENGLISH)).thenReturn(SPENT_SHORT);
        when(durationUtils.getShortFormattedDuration(originalEstimate, Locale.ENGLISH)).thenReturn(ORIG_SHORT);
        when(durationUtils.getShortFormattedDuration(remainingEstimate, Locale.ENGLISH)).thenReturn(ESTIMATE_SHORT);

        when(durationUtils.getFormattedDuration(timespent, Locale.ENGLISH)).thenReturn(SPENT_LONG);
        when(durationUtils.getFormattedDuration(originalEstimate, Locale.ENGLISH)).thenReturn(ORIG_LONG);
        when(durationUtils.getFormattedDuration(remainingEstimate, Locale.ENGLISH)).thenReturn(ESTIMATE_LONG);

        final TimeTrackingGraphBeanFactory factory = new TimeTrackingGraphBeanFactoryImpl(durationUtils);
        final TimeTrackingGraphBean bean = factory.createBean(issue, TimeTrackingGraphBeanFactoryImpl.Style.SHORT, helper);

        assertEquals((long) timespent, bean.getTimeSpent());
        assertEquals((long) originalEstimate, bean.getOriginalEstimate());
        assertEquals((long) remainingEstimate, bean.getRemainingEstimate());

        assertEquals(SPENT_SHORT, bean.getTimeSpentStr());
        assertEquals(ORIG_SHORT, bean.getOriginalEstimateStr());
        assertEquals(ESTIMATE_SHORT, bean.getRemainingEstimateStr());

        assertTrue(bean.getTimeSpentTooltip().contains(SPENT_LONG));
        assertTrue(bean.getOriginalEstimateTooltip().contains(ORIG_LONG));
        assertTrue(bean.getRemainingEstimateTooltip().contains(ESTIMATE_LONG));
    }

    @Test
    public void testCreateBeanFromIssueNormal() throws Exception {
        final Long timespent = 300L;
        final Long originalEstimate = 20L;
        final Long remainingEstimate = 10L;

        MockIssue issue = new MockIssue(7);
        issue.setEstimate(remainingEstimate);
        issue.setOriginalEstimate(originalEstimate);
        issue.setTimeSpent(timespent);

        final NoopI18nHelper helper = new NoopI18nHelper();

        when(durationUtils.getFormattedDuration(timespent, Locale.ENGLISH)).thenReturn(SPENT_LONG);
        when(durationUtils.getFormattedDuration(originalEstimate, Locale.ENGLISH)).thenReturn(ORIG_LONG);
        when(durationUtils.getFormattedDuration(remainingEstimate, Locale.ENGLISH)).thenReturn(ESTIMATE_LONG);

        final TimeTrackingGraphBeanFactory factory = new TimeTrackingGraphBeanFactoryImpl(durationUtils);
        final TimeTrackingGraphBean bean = factory.createBean(issue, TimeTrackingGraphBeanFactoryImpl.Style.NORMAL, helper);

        assertEquals((long) timespent, bean.getTimeSpent());
        assertEquals((long) originalEstimate, bean.getOriginalEstimate());
        assertEquals((long) remainingEstimate, bean.getRemainingEstimate());

        assertEquals(SPENT_LONG, bean.getTimeSpentStr());
        assertEquals(ORIG_LONG, bean.getOriginalEstimateStr());
        assertEquals(ESTIMATE_LONG, bean.getRemainingEstimateStr());

        assertTrue(bean.getTimeSpentTooltip().contains(SPENT_LONG));
        assertTrue(bean.getOriginalEstimateTooltip().contains(ORIG_LONG));
        assertTrue(bean.getRemainingEstimateTooltip().contains(ESTIMATE_LONG));
    }

    @Test
    public void testCreateBeanFromAggregateBeanShort() throws Exception {
        final Long timespent = 300L;
        final Long originalEstimate = 20L;
        final Long remainingEstimate = 10L;

        final AggregateTimeTrackingBean aggregate = new AggregateTimeTrackingBean(originalEstimate, remainingEstimate, timespent, 1);
        final NoopI18nHelper helper = new NoopI18nHelper();

        when(durationUtils.getShortFormattedDuration(timespent, Locale.ENGLISH)).thenReturn(SPENT_SHORT);
        when(durationUtils.getShortFormattedDuration(originalEstimate, Locale.ENGLISH)).thenReturn(ORIG_SHORT);
        when(durationUtils.getShortFormattedDuration(remainingEstimate, Locale.ENGLISH)).thenReturn(ESTIMATE_SHORT);

        when(durationUtils.getFormattedDuration(timespent, Locale.ENGLISH)).thenReturn(SPENT_LONG);
        when(durationUtils.getFormattedDuration(originalEstimate, Locale.ENGLISH)).thenReturn(ORIG_LONG);
        when(durationUtils.getFormattedDuration(remainingEstimate, Locale.ENGLISH)).thenReturn(ESTIMATE_LONG);

        final TimeTrackingGraphBeanFactory factory = new TimeTrackingGraphBeanFactoryImpl(durationUtils);
        final TimeTrackingGraphBean bean = factory.createBean(aggregate, TimeTrackingGraphBeanFactoryImpl.Style.SHORT, helper);

        assertEquals((long) timespent, bean.getTimeSpent());
        assertEquals((long) originalEstimate, bean.getOriginalEstimate());
        assertEquals((long) remainingEstimate, bean.getRemainingEstimate());

        assertEquals(SPENT_SHORT, bean.getTimeSpentStr());
        assertEquals(ORIG_SHORT, bean.getOriginalEstimateStr());
        assertEquals(ESTIMATE_SHORT, bean.getRemainingEstimateStr());

        assertTrue(bean.getTimeSpentTooltip().contains(SPENT_LONG));
        assertTrue(bean.getOriginalEstimateTooltip().contains(ORIG_LONG));
        assertTrue(bean.getRemainingEstimateTooltip().contains(ESTIMATE_LONG));
    }

    @Test
    public void testCreateBeanFromAggregateBeanNormal() throws Exception {
        final Long timespent = 300L;
        final Long originalEstimate = 20L;
        final Long remainingEstimate = 10L;

        final AggregateTimeTrackingBean aggregate = new AggregateTimeTrackingBean(originalEstimate, remainingEstimate, timespent, 1);
        final NoopI18nHelper helper = new NoopI18nHelper();

        when(durationUtils.getFormattedDuration(timespent, Locale.ENGLISH)).thenReturn(SPENT_LONG);
        when(durationUtils.getFormattedDuration(originalEstimate, Locale.ENGLISH)).thenReturn(ORIG_LONG);
        when(durationUtils.getFormattedDuration(remainingEstimate, Locale.ENGLISH)).thenReturn(ESTIMATE_LONG);

        final TimeTrackingGraphBeanFactory factory = new TimeTrackingGraphBeanFactoryImpl(durationUtils);
        final TimeTrackingGraphBean bean = factory.createBean(aggregate, TimeTrackingGraphBeanFactoryImpl.Style.NORMAL, helper);

        assertEquals((long) timespent, bean.getTimeSpent());
        assertEquals((long) originalEstimate, bean.getOriginalEstimate());
        assertEquals((long) remainingEstimate, bean.getRemainingEstimate());

        assertEquals(SPENT_LONG, bean.getTimeSpentStr());
        assertEquals(ORIG_LONG, bean.getOriginalEstimateStr());
        assertEquals(ESTIMATE_LONG, bean.getRemainingEstimateStr());

        assertTrue(bean.getTimeSpentTooltip().contains(SPENT_LONG));
        assertTrue(bean.getOriginalEstimateTooltip().contains(ORIG_LONG));
        assertTrue(bean.getRemainingEstimateTooltip().contains(ESTIMATE_LONG));
    }
}
