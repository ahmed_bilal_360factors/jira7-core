package com.atlassian.jira.issue.fields.config.manager;

import com.atlassian.collectors.CollectorsUtil;
import com.atlassian.jira.exception.DataAccessException;
import com.atlassian.jira.issue.context.persistence.FieldConfigContextPersister;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.issue.fields.config.FieldConfigScheme;
import com.atlassian.jira.issue.fields.config.persistence.FieldConfigSchemePersister;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.MethodRule;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import java.util.List;
import java.util.stream.Stream;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SuppressWarnings("deprecation")
public class TestFieldConfigSchemeManagerImpl {
    @Rule
    public final MethodRule initMockito = MockitoJUnit.rule();
    @Rule
    public final ExpectedException exception = ExpectedException.none();


    @Mock
    private FieldConfigContextPersister fieldConfigContextPersister;
    @Mock
    private FieldConfigSchemePersister fieldConfigSchemePersister;
    @Mock
    private FieldConfig fieldConfig;
    @Mock
    private FieldConfigScheme configScheme;
    @Mock
    private FieldConfigManager fieldConfigManager;
    @Mock
    private IssueType issueType;

    @InjectMocks
    private FieldConfigSchemeManagerImpl configSchemeManager;

    @Test
    public void testGetConfigSchemeForFieldConfig() throws Exception {
        when(fieldConfigSchemePersister.getConfigSchemeForFieldConfig(fieldConfig)).thenReturn(configScheme);
        assertSame(configScheme, configSchemeManager.getConfigSchemeForFieldConfig(fieldConfig));
    }

    @Test
    public void testGetConfigSchemeForFieldConfigDataAccessException() throws Exception {
        when(fieldConfigSchemePersister.getConfigSchemeForFieldConfig(fieldConfig)).thenThrow(new DataAccessException("blarg"));
        assertNull(configSchemeManager.getConfigSchemeForFieldConfig(fieldConfig));
    }

    @Test
    public void testNullArgumentToGetInvalidFieldConfigSchemesForIssueTypeRemoval() {
        // given:
        final FieldConfigSchemeManagerImpl fcsm = new FieldConfigSchemeManagerImpl(null, null, null);

        // expect:
        exception.expect(IllegalArgumentException.class);
        // once:
        fcsm.getInvalidFieldConfigSchemesForIssueTypeRemoval(null);
    }

    @Test
    public void testNullArgumentToRemoveInvalidFieldConfigSchemesForIssueType() {
        // given:
        final FieldConfigSchemeManagerImpl fcsm = new FieldConfigSchemeManagerImpl(null, null, null);

        // expect:
        exception.expect(IllegalArgumentException.class);
        // once:
        fcsm.removeInvalidFieldConfigSchemesForIssueType(null);
    }

    @Test
    public void testCallThrough() {
        final List<FieldConfigScheme> expectedList = ImmutableList.of(mock(FieldConfigScheme.class));

        when(issueType.getId()).thenReturn("123");
        when(fieldConfigSchemePersister.getInvalidFieldConfigSchemeAfterIssueTypeRemoval(issueType)).thenReturn(expectedList);

        assertSame(expectedList, configSchemeManager.getInvalidFieldConfigSchemesForIssueTypeRemoval(issueType));
    }

    @Test
    public void testRemoveInvalidFieldConfigSchemesForIssueType() {
        // given:
        when(issueType.getId()).thenReturn("123");
        final List<FieldConfigScheme> fcsList = Stream.of(1001, 1002, 1003)
                .map(this::getFieldConfigScheme)
                .collect(CollectorsUtil.toImmutableList());

        for (final FieldConfigScheme fcs : fcsList) {
            when(fieldConfigSchemePersister.getFieldConfigScheme(fcs.getId())).thenReturn(fcs);
        }
        when(fieldConfigSchemePersister.getInvalidFieldConfigSchemeAfterIssueTypeRemoval(issueType)).thenReturn(fcsList);

        // when:
        configSchemeManager.removeInvalidFieldConfigSchemesForIssueType(issueType);

        // then:
        for (final FieldConfigScheme fcs : fcsList) {
            verify(fieldConfigContextPersister).removeContextsForConfigScheme(fcs);
            verify(fieldConfigManager).removeConfigsForConfigScheme(fcs.getId());
            verify(fieldConfigSchemePersister).remove(fcs.getId());
        }
        verify(fieldConfigSchemePersister).removeByIssueType(issueType);
    }

    @Test
    public void testRemoveInvalidFieldConfigSchemesForCustomField() {
        final long id234 = 234L;
        final long id567 = 567L;
        final FieldConfigScheme fcs1 = getFieldConfigScheme(id234);
        final FieldConfigScheme fcs2 = getFieldConfigScheme(id567);

        when(fieldConfigSchemePersister.getConfigSchemeIdsForCustomFieldId("customfield_10000")).thenReturn(Lists.newArrayList(id234, id567));
        when(fieldConfigSchemePersister.getFieldConfigScheme(id234)).thenReturn(fcs1);
        when(fieldConfigSchemePersister.getFieldConfigScheme(id567)).thenReturn(fcs2);

        configSchemeManager.removeInvalidFieldConfigSchemesForCustomField("customfield_10000");

        verify(fieldConfigContextPersister).removeContextsForConfigScheme(fcs1);
        verify(fieldConfigContextPersister).removeContextsForConfigScheme(fcs2);
        verify(fieldConfigSchemePersister).remove(id234);
        verify(fieldConfigSchemePersister).remove(id567);
        verify(fieldConfigManager).removeConfigsForConfigScheme(id234);
        verify(fieldConfigManager).removeConfigsForConfigScheme(id567);

    }

    private FieldConfigScheme getFieldConfigScheme(final long id) {
        return new FieldConfigScheme.Builder().setName("Name" + id).setDescription("Desc" + id).setId(id).toFieldConfigScheme();
    }
}
