package com.atlassian.jira.web.action.admin.issuetypes;

import com.atlassian.jira.bc.ServiceOutcomeImpl;
import com.atlassian.jira.bc.project.ProjectAction;
import com.atlassian.jira.bc.project.ProjectService;
import com.atlassian.jira.issue.fields.config.MockFieldConfigScheme;
import com.atlassian.jira.issue.fields.config.manager.IssueTypeSchemeManager;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.mock.security.MockSimpleAuthenticationContext;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectFactory;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.google.common.collect.Sets;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.mockito.Mock;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static java.util.Collections.singletonList;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

/**
 * @since v4.4
 */
public class TestProjectIssueTypeSchemeHelper {
    @Rule
    public final RuleChain mockitoMocks = MockitoMocksInContainer.forTest(this);

    @Mock
    private ProjectService projectService;
    @Mock
    private IssueTypeSchemeManager issueTypeSchemeManager;
    @Mock
    private ProjectFactory projectFactory;
    @Mock
    private PermissionManager permissionManager;

    private MockSimpleAuthenticationContext authContext;
    private MockApplicationUser user;

    @Before
    public void setUp() throws Exception {
        user = new MockApplicationUser("bbain");
        authContext = new MockSimpleAuthenticationContext(user);
    }

    @Test
    public void testGetProjectsNonDefault() throws Exception {
        final MockProject project1 = new MockProject(101928282L, "ONE");
        final MockProject project2 = new MockProject(35438590L, "TWO");

        final MockFieldConfigScheme configScheme = new MockFieldConfigScheme().setId(1010101L).setName("SchemeName");
        configScheme.addAssociatedProjects(project1).addAssociatedProjects(project2);

        when(issueTypeSchemeManager.isDefaultIssueTypeScheme(configScheme)).thenReturn(false);

        final ProjectIssueTypeSchemeHelper helper = createHelper(Sets.<Project>newHashSet(project1));

        final List<Project> projects = helper.getProjectsUsingScheme(configScheme);
        assertEquals(singletonList(project1), projects);
    }

    @Test
    public void testGetProjectsDefault() throws Exception {
        final MockProject project1 = new MockProject(101928282L, "ONE");
        final MockProject project2 = new MockProject(35438590L, "TWO");

        final MockFieldConfigScheme configScheme1 = new MockFieldConfigScheme().setId(1010101L).setName("SchemeName");
        final MockFieldConfigScheme configScheme2 = new MockFieldConfigScheme().setId(1010102L).setName("SchemeName2");

        when(issueTypeSchemeManager.isDefaultIssueTypeScheme(configScheme1)).thenReturn(true);
        when(projectService.getAllProjectsForAction(user, ProjectAction.EDIT_PROJECT_CONFIG))
                .thenReturn(new ServiceOutcomeImpl<>(new SimpleErrorCollection(), Arrays.<Project>asList(project1, project2)));

        when(issueTypeSchemeManager.getConfigScheme(project1)).thenReturn(configScheme2);
        when(issueTypeSchemeManager.getConfigScheme(project2)).thenReturn(configScheme1);

        final ProjectIssueTypeSchemeHelper helper = createHelper(Collections.<Project>emptySet());

        final List<Project> projects = helper.getProjectsUsingScheme(configScheme1);
        assertEquals(singletonList(project2), projects);
    }

    @Test
    public void testGetProjectsDefaultWithErrors() throws Exception {
        final MockProject project1 = new MockProject(101928282L, "ONE");
        final MockProject project2 = new MockProject(35438590L, "TWO");

        final MockFieldConfigScheme configScheme1 = new MockFieldConfigScheme().setId(1010101L).setName("SchemeName");

        when(issueTypeSchemeManager.isDefaultIssueTypeScheme(configScheme1)).thenReturn(true);
        final SimpleErrorCollection errorCollection = new SimpleErrorCollection();
        errorCollection.addErrorMessage("errorMessage");
        when(projectService.getAllProjectsForAction(user, ProjectAction.EDIT_PROJECT_CONFIG))
                .thenReturn(new ServiceOutcomeImpl<>(errorCollection, Arrays.<Project>asList(project1, project2)));

        final ProjectIssueTypeSchemeHelper helper = createHelper(Collections.<Project>emptySet());

        final List<Project> projects = helper.getProjectsUsingScheme(configScheme1);
        assertEquals(Collections.<Project>emptyList(), projects);
    }

    private ProjectIssueTypeSchemeHelper createHelper(final Collection<? extends Project> allowedProjects) {
        return new ProjectIssueTypeSchemeHelper(projectService, issueTypeSchemeManager, authContext, projectFactory, permissionManager) {
            @Override
            boolean hasEditPermission(final ApplicationUser user, final Project project) {
                return allowedProjects.contains(project);
            }
        };
    }
}
