package com.atlassian.jira.issue.fields.config.manager;

import com.atlassian.jira.issue.context.IssueContext;
import com.atlassian.jira.issue.context.persistence.FieldConfigContextPersister;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.MockCustomField;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.issue.fields.config.FieldConfigImpl;
import com.atlassian.jira.issue.fields.config.FieldConfigScheme;
import com.atlassian.jira.issue.fields.config.persistence.FieldConfigSchemePersister;
import com.atlassian.jira.issue.issuetype.MockIssueType;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.LinkedHashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestFieldConfigSchemeManagerImplMockito {
    private static final String CUSTOM_FIELD_ID = "customfield_10100";
    public static final long PID = 1000L;

    @Mock
    private FieldConfigSchemePersister schemePersister;
    @Mock
    private FieldConfigContextPersister contextPersister;
    @Mock
    private FieldConfigManager configManager;
    @Mock
    private FieldConfigScheme fieldConfigScheme;
    @Mock
    private IssueContext issueContext;
    private CustomField customField = new MockCustomField(CUSTOM_FIELD_ID, "MockCF", null);
    private FieldConfigImpl firstFieldConfig = new FieldConfigImpl(10001L, "CONFIG_1", null, null, null);
    private FieldConfigImpl secondFieldConfig = new FieldConfigImpl(10002L, "CONFIG_2", null, null, null);

    private FieldConfigSchemeManagerImpl fieldConfigSchemeManager;

    @Before
    public void setUp() {
        when(issueContext.getProjectId()).thenReturn(PID);
        when(contextPersister.getRelevantConfigSchemeId(PID, CUSTOM_FIELD_ID)).thenReturn(10L);
        when(schemePersister.getFieldConfigScheme(10L)).thenReturn(fieldConfigScheme);

        fieldConfigSchemeManager = new FieldConfigSchemeManagerImpl(schemePersister, contextPersister, configManager);
    }

    @Test
    public void getRelevantConfigShouldReturnConfigHasKeyNull() {
        Map<String, FieldConfig> fieldConfigMap = mockFieldConfigMap(null);
        when(fieldConfigScheme.getConfigs()).thenReturn(fieldConfigMap);

        assertEquals(secondFieldConfig, fieldConfigSchemeManager.getRelevantConfig(issueContext, customField));
    }

    @Test
    public void getRelevantConfigShouldReturnFirstConfigEvenIssueTypeIsNull() {
        Map<String, FieldConfig> fieldConfigMap = mockFieldConfigMap("2");
        when(issueContext.getIssueTypeObject()).thenReturn(null);
        when(fieldConfigScheme.getConfigs()).thenReturn(fieldConfigMap);

        assertEquals(firstFieldConfig, fieldConfigSchemeManager.getRelevantConfig(issueContext, customField));
    }

    @Test
    public void getRelevantConfigShouldReturnConfigHasKeyEqualIssueTypeId() {
        Map<String, FieldConfig> fieldConfigMap = mockFieldConfigMap("2");
        when(issueContext.getIssueTypeObject()).thenReturn(new MockIssueType("2", "Issue"));
        when(fieldConfigScheme.getConfigs()).thenReturn(fieldConfigMap);

        assertEquals(secondFieldConfig, fieldConfigSchemeManager.getRelevantConfig(issueContext, customField));
    }

    private Map<String, FieldConfig> mockFieldConfigMap(final String secondFieldConfigKey) {
        Map<String, FieldConfig> fieldConfigMap = new LinkedHashMap<>();
        fieldConfigMap.put("1", firstFieldConfig);
        fieldConfigMap.put(secondFieldConfigKey, secondFieldConfig);
        return fieldConfigMap;
    }
}