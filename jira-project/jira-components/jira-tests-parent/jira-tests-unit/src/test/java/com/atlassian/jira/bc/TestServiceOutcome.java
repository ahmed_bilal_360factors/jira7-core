package com.atlassian.jira.bc;

import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.WarningCollection;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.function.Function;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestServiceOutcome {

    private static final long SUCCESS_VALUE = 1234;

    private static final String ERROR_RESULT = "errorResult";
    private static final String SUCCESS_RESULT = "successResult";

    @Mock private ErrorCollection errorCollection;
    @Mock private Function<ErrorCollection, String> onFailure;
    @Mock private Function<Long, String> onSuccess;

    @Before
    public void setUp() {
        when(onSuccess.apply(SUCCESS_VALUE)).thenReturn(SUCCESS_RESULT);
        when(onFailure.apply(errorCollection)).thenReturn(ERROR_RESULT);
    }

    @Test
    public void foldShouldApplyOnSuccessFunctionToValidResult() {
        // Set up
        final ServiceOutcome<Long> serviceOutcome = new StubServiceOutcome(true);

        // Invoke
        final String result = serviceOutcome.fold(onSuccess, onFailure);

        // Check
        assertThat(result, is(SUCCESS_RESULT));
        verifyZeroInteractions(onFailure);
    }

    @Test
    public void foldShouldApplyOnErrorFunctionToInvalidResult() {
        // Set up
        final ServiceOutcome<Long> serviceOutcome = new StubServiceOutcome(false);

        // Invoke
        final String result = serviceOutcome.fold(onSuccess, onFailure);

        // Check
        assertThat(result, is(ERROR_RESULT));
        verifyZeroInteractions(onSuccess);
    }

    /**
     * Using this stub in this test shields us from implementation details in the production code.
     */
    private class StubServiceOutcome implements ServiceOutcome<Long> {

        private final boolean valid;

        private StubServiceOutcome(final boolean valid) {
            this.valid = valid;
        }

        @Override
        public Long getReturnedValue() {
            return get();
        }

        @Override
        public Long get() {
            return SUCCESS_VALUE;
        }

        @Override
        public boolean isValid() {
            return valid;
        }

        @Override
        public ErrorCollection getErrorCollection() {
            return errorCollection;
        }

        @Override
        public WarningCollection getWarningCollection() {
            throw new UnsupportedOperationException("Not implemented");
        }
    }
}
