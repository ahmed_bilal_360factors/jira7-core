package com.atlassian.jira.plugin.webfragment.conditions;

import com.atlassian.jira.config.properties.ApplicationProperties;
import org.junit.Test;

import static com.atlassian.jira.config.properties.APKeys.JIRA_OPTION_ISSUELINKING;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestLinkingEnabledCondition {
    @Test
    public void testTrueWhenLinkingEnabled() {
        final ApplicationProperties applicationProperties = mock(ApplicationProperties.class);

        when(applicationProperties.getOption(JIRA_OPTION_ISSUELINKING)).thenReturn(true);

        final LinkingEnabledCondition condition = new LinkingEnabledCondition(applicationProperties);

        assertTrue(condition.shouldDisplay(null, null));
    }

    @Test
    public void testFalseWhenLinkingDisabled() {
        final ApplicationProperties applicationProperties = mock(ApplicationProperties.class);

        when(applicationProperties.getOption(JIRA_OPTION_ISSUELINKING)).thenReturn(false);

        final LinkingEnabledCondition condition = new LinkingEnabledCondition(applicationProperties);

        assertFalse(condition.shouldDisplay(null, null));
    }

}
