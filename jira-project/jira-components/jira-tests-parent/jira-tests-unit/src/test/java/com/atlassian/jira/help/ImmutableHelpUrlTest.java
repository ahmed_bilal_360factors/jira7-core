package com.atlassian.jira.help;

import org.junit.Test;

import static org.junit.Assert.assertThat;

/**
 * @since v6.2.4
 */
public class ImmutableHelpUrlTest {
    @Test
    public void ableToConstructUrl() {
        ImmutableHelpUrl url = new ImmutableHelpUrl("key", "url", "title", "alt", "description", true);
        HelpUrlMatcher matcher = new HelpUrlMatcher().url("url").title("title").key("key").alt("alt").description("description").local(true);

        assertThat(url, matcher);
    }

    //HIROL-62
    @Test
    public void ableToTakeStrangeArguments() {
        ImmutableHelpUrl url = new ImmutableHelpUrl("      key", "   url    ", "\ntitle\n\n", "        ", "   desc    ", true);
        HelpUrlMatcher matcher = new HelpUrlMatcher()
                .url("   url    ")
                .title("\ntitle\n\n")
                .key("      key")
                .alt("        ")
                .description("   desc    ")
                .local(true);
        assertThat(url, matcher);
    }
}
