package com.atlassian.jira.i18n;

import com.atlassian.jira.plugin.language.AppendTextTransform;
import com.atlassian.jira.plugin.language.PrependTextTransform;
import com.atlassian.jira.plugin.language.TranslationTransform;
import com.atlassian.jira.util.resourcebundle.I18NResourceBundleLoader;
import com.atlassian.jira.web.bean.MockI18nTranslationMode;
import com.atlassian.jira.web.bean.i18n.MockTranslationStoreFactory;
import com.google.common.collect.ImmutableMap;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Mockito.when;

/**
 * @since v6.2.3
 */
public class TestBackingI18nFactoryImpl {
    @Rule
    public MockitoRule rule = MockitoJUnit.rule();
    private final MockTranslationStoreFactory storeFactory = new MockTranslationStoreFactory();
    private final MockI18nTranslationMode translationMode = new MockI18nTranslationMode();
    @Mock
    private I18NResourceBundleLoader resourceLoader;
    private BackingI18nFactoryImpl i18nFactory;

    @Test
    public void createReturnsBackingBeanInCorrectLocale() {
        when(resourceLoader.load(Locale.GERMANY)).thenReturn(ImmutableMap.of("german", "yes"));

        i18nFactory = new BackingI18nFactoryImpl(storeFactory, resourceLoader, translationMode);

        final BackingI18n backingI18n = i18nFactory.create(Locale.GERMANY, Collections.<TranslationTransform>emptyList());

        assertThat(backingI18n.getLocale(), equalTo(Locale.GERMANY));
        assertThat(backingI18n.getText("german"), equalTo("yes"));
    }

    @Test
    public void createReturnsBackingBeanWithTranslations() {
        final List<TranslationTransform> translationTransforms = Arrays.asList(new PrependTextTransform("Start"),
                new AppendTextTransform("End"));

        when(resourceLoader.load(Locale.ITALY)).thenReturn(ImmutableMap.of("italy", "beer"));

        i18nFactory = new BackingI18nFactoryImpl(storeFactory, resourceLoader, translationMode);

        final BackingI18n backingI18n = i18nFactory.create(Locale.ITALY, translationTransforms);

        assertThat(backingI18n.getText("italy"), equalTo("StartbeerEnd"));
    }

    @Test
    public void createsI18nWithEmptyValues() {
        when(resourceLoader.load(Locale.FRENCH)).thenReturn(ImmutableMap.of("abc.key", ""));
        i18nFactory = new BackingI18nFactoryImpl(storeFactory, resourceLoader, translationMode);

        final BackingI18n backingI18n = i18nFactory.create(Locale.FRENCH, Collections.<TranslationTransform>emptyList());

        assertThat(backingI18n.getText("abc.key"), equalTo(""));
        assertThat(backingI18n.getLocale(), equalTo(Locale.FRENCH));
    }
}
