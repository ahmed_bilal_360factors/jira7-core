package com.atlassian.jira.instrumentation;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.config.FeatureFlag;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.config.properties.JiraProperties;
import com.atlassian.jira.config.properties.MockJiraProperties;
import com.atlassian.jira.util.concurrent.SynchronousExecutorService;
import com.google.common.collect.ImmutableMap;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.Arrays;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class DefaultInstrumentationLoggerTest {
    @Mock
    private FeatureManager featureManager;
    @Mock
    private EventPublisher eventPublisher;
    private DefaultInstrumentationLogger instrumentationLogger;

    @Before
    public void setUpMocks() {
        JiraProperties mockJiraProperties = new MockJiraProperties();
        mockJiraProperties.setProperty(DefaultInstrumentationLogger.JIRA_INSTRUMENTATION_LAAS, "true");
        when(featureManager.isEnabled(isA(FeatureFlag.class))).thenReturn(Boolean.TRUE);

        this.instrumentationLogger = new DefaultInstrumentationLogger(mockJiraProperties, featureManager, eventPublisher);
    }

    @Test
    public void testJsonable() {
        LogEntry entry = new LogEntry("traceId", "/path", "param1=val", ImmutableMap.of());

        String json = instrumentationLogger.getJsonString(Optional.of("spanId"), Optional.of("parentSpanId"), entry, Optional.empty(), Optional.empty());
        JsonElement tree = new JsonParser().parse(json);
        JsonObject baseObj = tree.getAsJsonObject();
        assertNotNull(baseObj);

        JsonObject extObj = baseObj.getAsJsonObject("ext");
        assertNotNull(extObj);
        assertEquals(extObj.getAsJsonPrimitive("spanId").getAsString(), "spanId");
        assertEquals(extObj.getAsJsonPrimitive("parentSpanId").getAsString(), "parentSpanId");

        JsonObject instrumentation = extObj.getAsJsonObject("instrumentation");
        assertNotNull(instrumentation);
        assertEquals(instrumentation.get("queryString").getAsString(), "param1=val");
        instrumentationLogger.cleanMdc();
    }

    @Test
    public void testMissingSpanId() {
        LogEntry entry = new LogEntry("traceId", "/path","param1=value1", ImmutableMap.of());

        String json = instrumentationLogger.getJsonString(Optional.empty(), Optional.empty(), entry, Optional.empty(), Optional.empty());

        JsonElement tree = new JsonParser().parse(json);
        JsonObject baseObj = tree.getAsJsonObject();
        assertNotNull(baseObj);

        JsonObject extObj = baseObj.getAsJsonObject("ext");
        assertNotNull(extObj);
        assertNull(extObj.getAsJsonPrimitive("spanId"));
        assertNull(extObj.getAsJsonPrimitive("parentSpanId"));

        instrumentationLogger.cleanMdc();
    }

    @Test
    public void testRequestTimeUnset() {
        LogEntry entry = new LogEntry("traceId", "/path","param1=value1", ImmutableMap.of());

        String json = instrumentationLogger.getJsonString(Optional.empty(), Optional.empty(), entry, Optional.empty(), Optional.empty());

        JsonElement tree = new JsonParser().parse(json);
        JsonObject baseObj = tree.getAsJsonObject();
        assertNotNull(baseObj);

        JsonObject extObj = baseObj.getAsJsonObject("ext");
        assertNotNull(extObj);

        assertEquals(extObj.getAsJsonPrimitive(DefaultInstrumentationLogger.REQUEST_EXECUTION_TIME).getAsLong(), -1L);

        instrumentationLogger.cleanMdc();
    }

    @Test
    public void testRequestTimeSet() {
        LogEntry entry = new LogEntry("traceId", "path", "param1=val1", ImmutableMap.of());
        String json = instrumentationLogger.getJsonString(Optional.empty(), Optional.empty(), entry, Optional.of(1L), Optional.empty());

        JsonElement tree = new JsonParser().parse(json);
        JsonObject baseObj = tree.getAsJsonObject();
        assertNotNull(baseObj);

        JsonObject extObj = baseObj.getAsJsonObject("ext");
        assertNotNull(extObj);

        assertNotNull(extObj.getAsJsonPrimitive(DefaultInstrumentationLogger.REQUEST_EXECUTION_TIME));
        assertEquals(extObj.getAsJsonPrimitive(DefaultInstrumentationLogger.REQUEST_EXECUTION_TIME).getAsLong(), 1L);

        instrumentationLogger.cleanMdc();
    }

    @Test
    public void testLaasFeatureNotEnabled() throws InterruptedException {
        ReflectionTestUtils.setField(instrumentationLogger, "pool", new SynchronousExecutorService());

        Statistics statistics = mock(Statistics.class);
        when(statistics.getLoggingKey()).thenReturn("key");

        when(featureManager.isEnabled(isA(FeatureFlag.class))).thenReturn(Boolean.FALSE);
        instrumentationLogger.save("traceId", "path", Arrays.asList(statistics), Optional.of(1L));

        verify(featureManager).isEnabled(isA(FeatureFlag.class));
    }

}
