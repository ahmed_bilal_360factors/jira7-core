package com.atlassian.jira.mock.web.pagebuilder;

import com.atlassian.jira.web.pagebuilder.Decorator;
import com.atlassian.jira.web.pagebuilder.DecoratorListener;
import com.atlassian.jira.web.pagebuilder.JiraPageBuilderService;
import com.atlassian.jira.web.pagebuilder.PageBuilder;
import com.atlassian.jira.web.pagebuilder.PageBuilderServiceSpi;
import com.atlassian.jira.web.pagebuilder.PageBuilderSpi;
import com.atlassian.plugin.webresource.WebResourceIntegration;
import com.atlassian.plugin.webresource.prebake.PrebakeWebResourceAssemblerFactory;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;

/**
 * A mock for DefaultJiraPageBuilderService that is able to return a PageBuilder that has its
 * <code>setDecorator()</code> method cause <code>DecoratorListener.onSetDecorator()</code> to be called.
 */
public class MockDefaultJiraPageBuilderService extends com.atlassian.plugin.webresource.assembler.DefaultPageBuilderService implements JiraPageBuilderService, PageBuilderServiceSpi {
    private PageBuilder pageBuilder;
    private PageBuilderSpi pageBuilderSpi;
    private DecoratorListener decoratorListener;
    private HttpServletRequest request;

    public MockDefaultJiraPageBuilderService() {
        this(mock(WebResourceIntegration.class), mock(PrebakeWebResourceAssemblerFactory.class));
    }

    public MockDefaultJiraPageBuilderService(
            WebResourceIntegration webResourceIntegration, PrebakeWebResourceAssemblerFactory webResourceAssemblerFactory) {
        super(webResourceIntegration, webResourceAssemblerFactory);
    }

    @Override
    public void initForRequest(final HttpServletRequest request, final HttpServletResponse response,
                               final DecoratorListener decoratorListener, final ServletContext servletContext) {
        if (this.request != null) {
            throw new IllegalStateException("This mock only supports one request at a time.");
        }
        this.request = request;
        this.decoratorListener = decoratorListener;
    }

    @Override
    public void clearForRequest() {
        request = null;
        decoratorListener = null;
        pageBuilder = null;
        pageBuilderSpi = null;
    }

    @Override
    public PageBuilder get() {
        if (pageBuilder == null) {
            final DecoratorListener listener = this.decoratorListener;
            pageBuilder = mock(PageBuilder.class);
            // When pageBuilder.setDecorator(...) is called, call decoratorListener.onSetDecorator().
            doAnswer(invocation -> {
                listener.onDecoratorSet();
                return null;
            }).when(pageBuilder).setDecorator(any(Decorator.class));
        }
        return pageBuilder;
    }

    @Override
    public PageBuilderSpi getSpi() {
        if (pageBuilderSpi == null) {
            pageBuilderSpi = mock(PageBuilderSpi.class);
        }
        return pageBuilderSpi;
    }
}
