package com.atlassian.jira.jql.query;

import com.atlassian.jira.jql.validator.OperatorUsageValidator;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.query.clause.TerminalClause;
import com.atlassian.query.clause.TerminalClauseImpl;
import com.atlassian.query.operand.MultiValueOperand;
import com.atlassian.query.operator.Operator;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.TermQuery;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Test for {@link com.atlassian.jira.jql.query.ValidatingDecoratorQueryFactory}.
 *
 * @since v4.0
 */
public class TestValidatingDecoratorQueryFactory {
    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @Mock
    private OperatorUsageValidator usageValidator;
    @Mock
    private ClauseQueryFactory delegate;

    @InjectMocks
    ValidatingDecoratorQueryFactory queryFactory;

    @Test
    public void testValidQuery() throws Exception {
        final TerminalClause clause = new TerminalClauseImpl("5757", Operator.LIKE, new MultiValueOperand(19L, 282L, 4L));
        final QueryFactoryResult delegateResult = new QueryFactoryResult(new TermQuery(new Term("a", "b")));
        final QueryCreationContext creationContext = new QueryCreationContextImpl((ApplicationUser) null);

        when(usageValidator.check(null, clause)).thenReturn(true);
        when(delegate.getQuery(creationContext, clause)).thenReturn(delegateResult);

        assertEquals(delegateResult, queryFactory.getQuery(creationContext, clause));

        verify(usageValidator).check(null, clause);
    }

    @Test
    public void testInvalidQuery() throws Exception {
        final TerminalClause clause = new TerminalClauseImpl("5757", Operator.LIKE, new MultiValueOperand(19L, 282L, 4L));
        final QueryCreationContext creationContext = new QueryCreationContextImpl((ApplicationUser) null);

        when(usageValidator.check(null, clause)).thenReturn(false);

        assertEquals(QueryFactoryResult.createFalseResult(), queryFactory.getQuery(creationContext, clause));

        verify(usageValidator).check(null, clause);
    }
}
