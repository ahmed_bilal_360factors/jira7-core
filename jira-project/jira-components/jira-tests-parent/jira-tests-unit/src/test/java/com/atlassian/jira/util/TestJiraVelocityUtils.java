package com.atlassian.jira.util;

import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockComponentContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.timezone.TimeZoneResolver;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.ofbiz.core.entity.GenericEntityException;

import java.util.Locale;
import java.util.TimeZone;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.when;


public class TestJiraVelocityUtils {
    private static final String lSep = System.getProperty("line.separator");

    @Rule
    public final RuleChain mockitoMocks = MockitoMocksInContainer.forTest(this);

    @Rule public MockComponentContainer container = new MockComponentContainer(this);

    @Mock
    private FieldManager mockFieldManager;

    @Mock
    private JiraAuthenticationContext jiraAuthenticationContext;

    @Mock
    @AvailableInContainer
    private TimeZoneResolver timeZoneResolver;

    @InjectMocks
    JiraVelocityHelper helper;

    @Test
    public void testQuote() {
        assertEquals("", helper.quote(""));
        assertEquals("> foo", helper.quote("foo"));
        assertEquals("> foo" + lSep + "> bar", helper.quote("foo\nbar"));
        assertEquals("> foo" + lSep + "> bar" + lSep + "> baz", helper.quote("foo\nbar\nbaz"));
        assertEquals("> foo bar", helper.quote("foo bar"));
    }

    @Test
    public void testPrintChangelog() throws GenericEntityException {
        // Write me!
    }

    @Test
    public void testFirstDayOfWeekFrench() {
        when(jiraAuthenticationContext.getLocale()).thenReturn(Locale.FRANCE);
        assertEquals(2, new JiraVelocityUtils.LazyCalendar(jiraAuthenticationContext, null).getFirstDayOfWeek());
    }

    @Test
    public void testFirstDayOfWeekYank() {
        when(jiraAuthenticationContext.getLocale()).thenReturn(Locale.US);
        assertEquals(1, new JiraVelocityUtils.LazyCalendar(jiraAuthenticationContext, null).getFirstDayOfWeek());
    }

    @Test
    public void testFirstDayOfWeekOz() {
        when(jiraAuthenticationContext.getLocale()).thenReturn(new Locale("en", "AU"));
        assertEquals(1, new JiraVelocityUtils.LazyCalendar(jiraAuthenticationContext, null).getFirstDayOfWeek());
    }

    @Test
    public void testDateTimeInCurrentUsersTimezone() {
        // for a user in TZ -1, the current user date should be yesterday's date
        when(timeZoneResolver.getUserTimeZone(anyObject())).thenReturn(TimeZone.getTimeZone("GMT-1:00"));
        assertThat(new JiraVelocityUtils.LazyCalendar(jiraAuthenticationContext, null).dateTimeInCurrentUsersTimezone(0), is("1969/12/31 23:00:00"));
        // for a user in TZ +1, the current user date should be today's date
        when(timeZoneResolver.getUserTimeZone(anyObject())).thenReturn(TimeZone.getTimeZone("GMT+1:00"));
        assertThat(new JiraVelocityUtils.LazyCalendar(jiraAuthenticationContext, null).dateTimeInCurrentUsersTimezone(0), is("1970/01/01 01:00:00"));
    }
}
