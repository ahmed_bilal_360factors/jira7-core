package com.atlassian.jira.upgrade.tasks.role;

import com.atlassian.fugue.Option;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.license.Jira6xServiceDeskPluginEncodedLicenseSupplier;
import com.atlassian.jira.matchers.OptionMatchers;
import com.google.common.collect.ImmutableList;
import org.junit.Before;
import org.junit.Test;

import java.util.Collections;

import static com.atlassian.jira.test.util.lic.core.CoreLicenses.LICENSE_CORE;
import static com.atlassian.jira.test.util.lic.sd.ServiceDeskLicenses.LICENSE_SERVICE_DESK_ABP;
import static com.atlassian.jira.test.util.lic.sd.ServiceDeskLicenses.LICENSE_SERVICE_DESK_TBP;
import static com.atlassian.jira.test.util.lic.software.SoftwareLicenses.LICENSE_SOFTWARE;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class Jira6xServiceDeskLicenseProviderImplTest {
    private Jira6xServiceDeskPluginEncodedLicenseSupplier sdLicenseSupplier;
    private LicenseDao licenseDao;

    @Before
    public void setup() {
        sdLicenseSupplier = mock(Jira6xServiceDeskPluginEncodedLicenseSupplier.class);
        when(sdLicenseSupplier.getCurrentOrUpgrade()).thenReturn(Option.none());

        licenseDao = mock(LicenseDao.class);
        when(licenseDao.get6xLicense()).thenReturn(Option.none());
        when(licenseDao.getLicenses()).thenReturn(new Licenses(Collections.emptyList()));
    }

    @Test
    public void retrievesLicenseFromPluginStoreIfBehindTheFirewall() {
        Jira6xServiceDeskLicenseProviderImpl provider = licenseProvider();
        final String licenseString = LICENSE_SERVICE_DESK_ABP.getLicenseString();
        when(sdLicenseSupplier.getCurrentOrUpgrade()).thenReturn(Option.some(licenseString));

        assertThat(provider.serviceDeskLicense(), OptionMatchers.some(new License(licenseString)));
    }

    @Test
    public void retrievesLicenseFromOldStoreIfThereIsNotOneInPluginStoreWhenBehindTheFirewall() {
        final String licenseString = LICENSE_SERVICE_DESK_ABP.getLicenseString();
        Jira6xServiceDeskLicenseProviderImpl provider = licenseProvider();
        when(licenseDao.get6xLicense()).thenReturn(Option.some(new License(licenseString)));
        when(sdLicenseSupplier.getCurrentOrUpgrade()).thenReturn(Option.none());

        assertThat(provider.serviceDeskLicense().map(License::licenseString), OptionMatchers.some(licenseString));
    }

    @Test
    public void retrievesLicenseFromPluginStoreWhenBehindTheFirewallAndThereIsServiceDeskLicenseInOldStore() {
        Jira6xServiceDeskLicenseProviderImpl provider = licenseProvider();
        when(licenseDao.get6xLicense()).thenReturn(Option.some(new License(LICENSE_SERVICE_DESK_ABP.getLicenseString())));
        when(sdLicenseSupplier.getCurrentOrUpgrade()).thenReturn(Option.some(LICENSE_SERVICE_DESK_TBP.getLicenseString()));

        assertThat(provider.serviceDeskLicense().map(License::licenseString),
                OptionMatchers.some(LICENSE_SERVICE_DESK_TBP.getLicenseString()));
    }

    private Jira6xServiceDeskLicenseProviderImpl licenseProvider() {
        return new Jira6xServiceDeskLicenseProviderImpl(sdLicenseSupplier, licenseDao);
    }
}