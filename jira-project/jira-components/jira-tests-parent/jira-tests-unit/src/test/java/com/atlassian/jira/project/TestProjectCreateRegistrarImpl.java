package com.atlassian.jira.project;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static com.atlassian.jira.project.ProjectCreateRegistrarImpl.APPLY_PROJECT_TEMPLATE_HANDLER_ID;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestProjectCreateRegistrarImpl {
    private static final ProjectCreatedData PROJECT_CREATED_DATA = anyProjectProjectCreatedData();

    private ProjectCreateRegistrarImpl registrar;

    @Before
    public void setUp() {
        registrar = new ProjectCreateRegistrarReturningHandlersInOrder();
    }

    @Test
    public void aRegisteredHandlerReceivesANotificationWhenAProjectIsCreated() throws Exception {
        ProjectCreateHandler handler = handlerWithId("id");

        register(handler);
        notifyAllHandlers();

        assertHandlerWasNotifiedOnce(handler);
    }

    @Test
    public void severalHandlersCanBeRegistered() throws Exception {
        ProjectCreateHandler firstHandler = handlerWithId("first");
        ProjectCreateHandler secondHandler = handlerWithId("second");

        register(firstHandler);
        register(secondHandler);
        notifyAllHandlers();

        assertHandlerWasNotifiedOnce(firstHandler);
        assertHandlerWasNotifiedOnce(secondHandler);
    }

    @Test
    public void theHandlerToApplyAProjectTemplateIsTheFirstOneThatGetsNotified() throws Exception {
        ProjectCreateHandler firstHandler = handlerWithId("first");
        ProjectCreateHandler secondHandler = handlerWithId("second");
        ProjectCreateHandler applyProjectTemplateHandler = handlerWithId(APPLY_PROJECT_TEMPLATE_HANDLER_ID);

        InOrder inOrder = inOrder(firstHandler, secondHandler, applyProjectTemplateHandler);

        register(firstHandler);
        register(secondHandler);
        register(applyProjectTemplateHandler);
        notifyAllHandlers();

        assertHandlerWasNotifiedOnce(inOrder, applyProjectTemplateHandler);
        assertHandlerWasNotifiedOnce(inOrder, firstHandler);
        assertHandlerWasNotifiedOnce(inOrder, secondHandler);
    }

    @Test
    public void aRegisteredHandlerCanNotBeRegisteredAgain() throws Exception {
        ProjectCreateHandler handler = handlerWithId("id");

        register(handler);
        register(handler);
        notifyAllHandlers();

        assertHandlerWasNotifiedOnce(handler);
    }

    @Test
    public void unregisteringAHandlerThatHasNotBeenRegisteredIsANoOp() throws Exception {
        ProjectCreateHandler firstHandler = handlerWithId("first");
        ProjectCreateHandler secondHandler = handlerWithId("second");

        register(firstHandler);
        unregister(secondHandler);
        notifyAllHandlers();

        assertHandlerWasNotifiedOnce(firstHandler);
    }

    @Test
    public void aHandlerThatHasBeenUnregisteredDoesNotReceiveMoreNotificationsWhenAProjectIsCreated() throws Exception {
        ProjectCreateHandler handler = handlerWithId("id");

        register(handler);
        unregister(handler);
        notifyAllHandlers();

        assertHandlerWasNotNotified(handler);
    }

    @Test
    public void notifyWithNoHandlersIsANoOp() {
        boolean result = notifyAllHandlers();

        assertThat(result, is(true));
    }

    @Test
    public void whenAHandlerReturnsErrorsAllHandlersThatWereExecutedAreRolledBack() throws Exception {
        ProjectCreateHandler firstHandler = handlerWithId("first");
        ProjectCreateHandler secondHandler = handlerWithId("second");
        ProjectCreateHandler thirdHandler = handlerThrowingException("third");

        register(firstHandler);
        register(secondHandler);
        register(thirdHandler);
        notifyAllHandlers();

        assertHandlerWasNotifiedOnce(firstHandler);
        assertHandlerWasRolledBack(firstHandler);

        assertHandlerWasNotifiedOnce(secondHandler);
        assertHandlerWasRolledBack(secondHandler);

        assertHandlerWasNotifiedOnce(thirdHandler);
        assertHandlerWasRolledBack(thirdHandler);
    }

    @Test
    public void whenAHandlerReturnsErrorsHandlersThatHaveNotBeenNotifiedYetAreNotRolledBack() throws Exception {
        ProjectCreateHandler firstHandler = handlerWithId("first");
        ProjectCreateHandler secondHandler = handlerThrowingException("second");
        ProjectCreateHandler thirdHandler = handlerWithId("third");

        register(firstHandler);
        register(secondHandler);
        register(thirdHandler);
        notifyAllHandlers();

        assertHandlerWasNotifiedOnce(firstHandler);
        assertHandlerWasRolledBack(firstHandler);

        assertHandlerWasNotifiedOnce(secondHandler);
        assertHandlerWasRolledBack(secondHandler);

        assertHandlerWasNotNotified(thirdHandler);
        assertHandlerWasNotRolledBack(thirdHandler);
    }

    @Test
    public void whenAHandlerReturnsErrorsTheRegistrarReturnsFalse() throws Exception {
        ProjectCreateHandler handler = handlerThrowingException("first");

        register(handler);
        boolean result = notifyAllHandlers();

        assertThat(result, is(false));
    }

    private void assertHandlerWasNotifiedOnce(ProjectCreateHandler handler) throws Exception {
        verify(handler, times(1)).onProjectCreated(PROJECT_CREATED_DATA);
    }

    private void assertHandlerWasNotifiedOnce(InOrder inOrder, ProjectCreateHandler handler) throws Exception {
        inOrder.verify(handler, times(1)).onProjectCreated(PROJECT_CREATED_DATA);
    }

    private void assertHandlerWasNotNotified(ProjectCreateHandler handler) throws Exception {
        verify(handler, never()).onProjectCreated(PROJECT_CREATED_DATA);
    }

    private void assertHandlerWasRolledBack(ProjectCreateHandler handler) {
        verify(handler, times(1)).onProjectCreationRolledBack(PROJECT_CREATED_DATA);
    }

    private void assertHandlerWasNotRolledBack(ProjectCreateHandler handler) {
        verify(handler, never()).onProjectCreationRolledBack(PROJECT_CREATED_DATA);
    }

    private static ProjectCreatedData anyProjectProjectCreatedData() {
        return new ProjectCreatedData.Builder().withProject(mock(Project.class)).build();
    }

    private boolean notifyAllHandlers() {
        return registrar.notifyAllHandlers(PROJECT_CREATED_DATA);
    }

    private void register(ProjectCreateHandler handler) {
        registrar.register(handler);
    }

    private void unregister(ProjectCreateHandler handler) {
        registrar.unregister(handler);
    }

    private ProjectCreateHandler handlerWithId(String id) throws Exception {
        ProjectCreateHandler handler = mock(ProjectCreateHandler.class);
        when(handler.getHandlerId()).thenReturn(id);
        return handler;
    }

    private ProjectCreateHandler handlerThrowingException(String id) throws Exception {
        ProjectCreateHandler handler = mock(ProjectCreateHandler.class);
        when(handler.getHandlerId()).thenReturn(id);
        doThrow(new RuntimeException()).when(handler).onProjectCreated(PROJECT_CREATED_DATA);
        return handler;
    }

    private static class ProjectCreateRegistrarReturningHandlersInOrder extends ProjectCreateRegistrarImpl {
        private List<ProjectCreateHandler> handlersList = new ArrayList<>();

        @Override
        protected Collection<ProjectCreateHandler> getHandlers() {
            return handlersList;
        }

        @Override
        public void register(final ProjectCreateHandler handlerToAdd) {
            if (!handlersList.contains(handlerToAdd)) {
                handlersList.add(handlerToAdd);
            }
            super.register(handlerToAdd);
        }

        @Override
        public void unregister(final ProjectCreateHandler handlerToRemove) {
            handlersList.remove(handlerToRemove);
            super.unregister(handlerToRemove);
        }
    }
}
