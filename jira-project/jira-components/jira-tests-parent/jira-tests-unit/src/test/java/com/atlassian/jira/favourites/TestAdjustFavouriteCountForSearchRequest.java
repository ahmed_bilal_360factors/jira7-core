package com.atlassian.jira.favourites;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.index.MockResult;
import com.atlassian.jira.instrumentation.InstrumentationListenerManager;
import com.atlassian.jira.instrumentation.InstrumentationLogger;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.issue.fields.layout.column.ColumnLayoutManager;
import com.atlassian.jira.issue.search.DefaultSearchRequestManager;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.issue.search.SearchRequestManager;
import com.atlassian.jira.issue.search.SearchRequestStore;
import com.atlassian.jira.issue.subscription.SubscriptionManager;
import com.atlassian.jira.junit.rules.InitMockitoMocks;
import com.atlassian.jira.sharing.ShareManager;
import com.atlassian.jira.sharing.index.MockSharedEntityIndexer;
import com.atlassian.jira.sharing.index.SharedEntityIndexer;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.util.UserUtil;
import com.atlassian.query.QueryImpl;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.junit.rules.TestRule;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import static com.atlassian.jira.sharing.SharedEntity.SharePermissions.PRIVATE;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestAdjustFavouriteCountForSearchRequest {
    @Rule
    public MethodRule initMockitoMocks = MockitoJUnit.rule();

    private final ApplicationUser user = new MockApplicationUser("admin");

    @Mock
    private SearchRequestStore searchRequestStore;
    @Mock
    private ShareManager shareManager;
    @Mock
    private SubscriptionManager subscriptionManager;
    @Mock
    private ColumnLayoutManager columnLayoutManager;
    @Mock
    private SearchService searchService;
    @Mock
    private UserUtil userUtil;
    @Mock
    private SharedEntityIndexer sharedEntityIndexer;
    @Mock
    private EventPublisher eventPublisher;
    @Mock
    private FeatureManager featureManager;
    @Mock
    private InstrumentationListenerManager instrumentationListenerManager;
    @Mock
    private InstrumentationLogger instrumentationLogger;

    @InjectMocks
    private DefaultSearchRequestManager manager;

    @Test
    public void searchRequestStoreMethodIsCalledWhenAdjustingFavoriteCount() {
        final SearchRequest searchRequest = new SearchRequest(new QueryImpl(), user, "test", "test desc", 999L, 0L);
        when(searchRequestStore.adjustFavouriteCount(searchRequest.getId(), 1)).thenReturn(searchRequest);
        when(shareManager.getSharePermissions(searchRequest)).thenReturn(PRIVATE);
        when(sharedEntityIndexer.index(searchRequest)).thenReturn(new MockResult());

        manager.adjustFavouriteCount(searchRequest, 1);

        verify(searchRequestStore).adjustFavouriteCount(searchRequest.getId(), 1);
    }
}
