package com.atlassian.jira.issue.search.searchers.renderer;

import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.issue.resolution.Resolution;
import com.google.common.collect.ImmutableList;
import org.hamcrest.Matchers;
import org.junit.Test;

import java.util.Collection;

import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestResolutionSearchRenderer {
    @Test
    public void testGetSelectListOptions() throws Exception {
        final ConstantsManager mockConstantsManager = mock(ConstantsManager.class);
        final ImmutableList<Resolution> resolutionObjects = ImmutableList.of();
        when(mockConstantsManager.getResolutionObjects()).thenReturn(resolutionObjects);
        final ResolutionSearchRenderer searchRenderer = new ResolutionSearchRenderer("test", mockConstantsManager, null, null, null, null);
        final Collection<Resolution> options = searchRenderer.getSelectListOptions(null);

        assertThat(options, Matchers.sameInstance(options));
    }
}
