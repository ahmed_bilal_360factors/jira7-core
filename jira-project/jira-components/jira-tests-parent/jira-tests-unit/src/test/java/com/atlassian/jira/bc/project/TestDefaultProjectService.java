package com.atlassian.jira.bc.project;

import com.atlassian.beehive.ClusterLockService;
import com.atlassian.jira.association.NodeAssociationStore;
import com.atlassian.jira.avatar.Avatar;
import com.atlassian.jira.avatar.AvatarManager;
import com.atlassian.jira.bc.EntityNotFoundException;
import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.bc.MockJiraServiceContext;
import com.atlassian.jira.bc.ServiceOutcome;
import com.atlassian.jira.bc.ServiceOutcomeImpl;
import com.atlassian.jira.bc.project.component.MockProjectComponent;
import com.atlassian.jira.bc.project.component.ProjectComponent;
import com.atlassian.jira.bc.project.component.ProjectComponentManager;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.entity.Entity;
import com.atlassian.jira.event.project.ProjectEventManager;
import com.atlassian.jira.exception.DataAccessException;
import com.atlassian.jira.exception.RemoveException;
import com.atlassian.jira.icon.IconType;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.fields.screen.issuetype.IssueTypeScreenScheme;
import com.atlassian.jira.issue.fields.screen.issuetype.IssueTypeScreenSchemeManager;
import com.atlassian.jira.issue.security.IssueSecurityLevelScheme;
import com.atlassian.jira.issue.security.IssueSecuritySchemeManager;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.mock.MockAvatar;
import com.atlassian.jira.mock.MockProjectManager;
import com.atlassian.jira.mock.ofbiz.MockGenericValue;
import com.atlassian.jira.mock.project.MockVersion;
import com.atlassian.jira.notification.NotificationSchemeManager;
import com.atlassian.jira.ofbiz.FieldMap;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.permission.PermissionSchemeManager;
import com.atlassian.jira.project.AssigneeTypes;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectCategoryImpl;
import com.atlassian.jira.project.ProjectCreateNotifier;
import com.atlassian.jira.project.ProjectCreatedData;
import com.atlassian.jira.project.ProjectImpl;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.project.UpdateProjectParameters;
import com.atlassian.jira.project.template.ProjectTemplate;
import com.atlassian.jira.project.template.ProjectTemplateKey;
import com.atlassian.jira.project.template.ProjectTemplateManager;
import com.atlassian.jira.project.type.ProjectTypeKey;
import com.atlassian.jira.project.type.ProjectTypeUpdatedNotifier;
import com.atlassian.jira.project.util.ProjectKeyStore;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.project.version.VersionManager;
import com.atlassian.jira.scheme.Scheme;
import com.atlassian.jira.scheme.SchemeFactory;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.MockGlobalPermissionManager;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.sharing.SharePermissionDeleteUtils;
import com.atlassian.jira.task.TaskManager;
import com.atlassian.jira.task.context.Context;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.util.MockUserManager;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.ErrorCollections;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.NoopI18nFactory;
import com.atlassian.jira.util.NoopI18nHelper;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.jira.web.action.RequestSourceType;
import com.atlassian.jira.workflow.JiraWorkflow;
import com.atlassian.jira.workflow.WorkflowManager;
import com.atlassian.jira.workflow.WorkflowSchemeManager;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.ofbiz.core.entity.GenericEntityException;
import org.ofbiz.core.entity.GenericValue;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import static com.atlassian.jira.permission.ProjectPermissions.ADMINISTER_PROJECTS;
import static com.atlassian.jira.permission.ProjectPermissions.BROWSE_PROJECTS;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestDefaultProjectService {
    private I18nHelper.BeanFactory i18nFactory = new NoopI18nFactory();

    @Rule
    public final RuleChain mockitoMocks = MockitoMocksInContainer.forTest(this);

    @Mock
    JiraAuthenticationContext jiraAuthenticationContext;
    @AvailableInContainer
    @Mock
    AvatarManager avatarManager;
    @Mock
    PermissionManager permissionManager;
    @Mock
    ApplicationProperties applicationProperties;
    @Mock
    ProjectManager projectManager;
    @Mock
    PermissionSchemeManager permissionSchemeManager;
    @Mock
    NotificationSchemeManager notificationSchemeManager;
    @Mock
    IssueSecuritySchemeManager issueSecuritySchemeManager;
    @Mock
    IssueTypeScreenSchemeManager issueTypeScreenSchemeManager;
    @Mock
    WorkflowSchemeManager workflowSchemeManager;
    @Mock
    ProjectEventManager projectEventManager;
    @Mock
    SchemeFactory schemeFactory;
    @Mock
    CustomFieldManager customFieldManager;
    @Mock
    WorkflowManager workflowManager;
    @Mock
    NodeAssociationStore nodeAssociationStore;
    @Mock
    VersionManager versionManager;
    @Mock
    ProjectComponentManager projectComponentManager;
    @Mock
    SharePermissionDeleteUtils sharePermissionDeleteUtils;
    @Mock
    ProjectKeyStore projectKeyStore;
    @Mock
    ProjectTypeUpdatedNotifier projectTypeUpdatedNotifier;
    @Mock
    ProjectSchemeAssociationManager projectSchemeAssociationManager;
    @Mock
    ProjectTemplateManager projectTemplateManager;
    @AvailableInContainer
    GlobalPermissionManager globalPermissionManager = MockGlobalPermissionManager.withSystemGlobalPermissions();
    ProjectTypeValidator projectTypeValidator;

    @AvailableInContainer
    MockUserManager userManager = new MockUserManager();
    ApplicationUser admin;

    @Before
    public void setup() {
        // prepare the admin user
        admin = new MockApplicationUser("adminKey", "AdMiN");
        userManager.addUser(admin);
        when(permissionManager.hasPermission(Permissions.ADMINISTER, admin)).thenReturn(true);
        when(globalPermissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, admin)).thenReturn(true);

        // prepare mocks for the project name/key that we'll try to create
        when(projectManager.getProjectObjByName("projectName")).thenReturn(null);
        when(projectManager.getProjectObjByKey("KEY")).thenReturn(null);
        when(applicationProperties.getDefaultBackedString(APKeys.JIRA_PROJECTKEY_MAX_LENGTH)).thenReturn("10");

        // default max length
        when(applicationProperties.getDefaultBackedString(APKeys.JIRA_PROJECTNAME_MAX_LENGTH)).thenReturn("80");

        when(projectKeyStore.getProjectId(anyString())).thenReturn(null);

        when(avatarManager.getDefaultAvatar(any(IconType.class))).thenReturn(mock(Avatar.class));

        projectTypeValidator = projectTypeValidatorMarkingAnyProjectTypeAsValid();
    }

    @Test
    public void testRetrieveProjectByIdProjectDoesntExist() {
        when(projectManager.getProjectObj(1L)).thenReturn(null);

        DefaultProjectService projectService = new MyProjectService();

        final ProjectService.GetProjectResult projectResult = projectService.getProjectById((ApplicationUser) null, 1L);
        assertNotNull(projectResult);
        assertNull(projectResult.getProject());
        assertThat(projectResult.getErrorCollection().getErrorMessages(), hasItem(NoopI18nHelper.makeTranslation("admin.errors.project.not.found.for.id", 1L)));
    }

    @Test
    public void testRetrieveProjectByIdNoBrowsePermission() {
        final Project mockProject = new ProjectImpl(null);
        MockApplicationUser user = new MockApplicationUser("luser");

        when(permissionManager.hasPermission(BROWSE_PROJECTS, mockProject, user)).thenReturn(false);
        when(projectManager.getProjectObj(1L)).thenReturn(mockProject);

        DefaultProjectService projectService = new MyProjectService();

        final ProjectService.GetProjectResult projectResult = projectService.getProjectById(user, 1L);
        assertNotNull(projectResult);
        assertNull(projectResult.getProject());
        assertThat(projectResult.getErrorCollection().getErrorMessages(), hasItem(NoopI18nHelper.makeTranslation("admin.errors.project.not.found.for.id", 1L)));
    }

    @Test
    public void testRetrieveProjectByIdNoUserProvided() {
        final Project mockProject = new ProjectImpl(null);
        MockApplicationUser user = new MockApplicationUser("luser");

        when(permissionManager.hasPermission(BROWSE_PROJECTS, mockProject, user)).thenReturn(false);
        when(projectManager.getProjectObj(1L)).thenReturn(mockProject);
        when(jiraAuthenticationContext.getUser()).thenReturn(user);

        DefaultProjectService projectService = new MyProjectService();

        final ProjectService.GetProjectResult projectResult = projectService.getProjectById(1L);
        assertNotNull("A project result is returned", projectResult);
        assertNull("No project is returned", projectResult.getProject());
        assertThat(projectResult.getErrorCollection().getErrorMessages(), hasItem(NoopI18nHelper.makeTranslation("admin.errors.project.not.found.for.id", 1L)));
    }

    @Test
    public void testRetrieveProjectByIdCurrentUserNoBrowsePermission() {
        final Project mockProject = new ProjectImpl(null);
        MockApplicationUser user = new MockApplicationUser("luser");

        when(permissionManager.hasPermission(BROWSE_PROJECTS, mockProject, user)).thenReturn(false);
        when(projectManager.getProjectObj(1L)).thenReturn(mockProject);
        when(jiraAuthenticationContext.getUser()).thenReturn(user);

        DefaultProjectService projectService = new MyProjectService();

        final ProjectService.GetProjectResult projectResult = projectService.getProjectById(1L);
        assertNotNull("A project result is returned", projectResult);
        assertNull("No project is returned", projectResult.getProject());
        assertThat(projectResult.getErrorCollection().getErrorMessages(), hasItem(NoopI18nHelper.makeTranslation("admin.errors.project.not.found.for.id", 1L)));
    }

    @Test
    public void testRetrieveProjectByKeyNoUserProvided() {
        final Project mockProject = new ProjectImpl(null);
        MockApplicationUser user = new MockApplicationUser("luser");

        when(permissionManager.hasPermission(BROWSE_PROJECTS, mockProject, user)).thenReturn(false);
        when(projectManager.getProjectObjByKey("KEY")).thenReturn(mockProject);
        when(jiraAuthenticationContext.getUser()).thenReturn(user);

        DefaultProjectService projectService = new MyProjectService();

        final ProjectService.GetProjectResult projectResult = projectService.getProjectByKey("KEY");
        assertNotNull("A project result is returned", projectResult);
        assertNull("No project is returned", projectResult.getProject());
        assertThat(projectResult.getErrorCollection().getErrorMessages(), hasItem(NoopI18nHelper.makeTranslation("admin.errors.project.not.found.for.key", "KEY")));
    }

    @Test
    public void testRetrieveProjectByKeyCurrentUserNoBrowsePermission() {
        final Project mockProject = new ProjectImpl(null);
        MockApplicationUser user = new MockApplicationUser("luser");

        when(permissionManager.hasPermission(BROWSE_PROJECTS, mockProject, user)).thenReturn(false);
        when(projectManager.getProjectObjByKey("KEY")).thenReturn(mockProject);
        when(jiraAuthenticationContext.getUser()).thenReturn(user);

        DefaultProjectService projectService = new MyProjectService();

        final ProjectService.GetProjectResult projectResult = projectService.getProjectByKey("KEY");
        assertNotNull("A project result is returned", projectResult);
        assertNull("No project is returned", projectResult.getProject());
        assertThat(projectResult.getErrorCollection().getErrorMessages(), hasItem(NoopI18nHelper.makeTranslation("admin.errors.project.not.found.for.key", "KEY")));
    }

    @Test
    public void testRetrieveProjectById() {
        final Project mockProject = new ProjectImpl(null);

        when(permissionManager.hasPermission(BROWSE_PROJECTS, mockProject, admin)).thenReturn(true);
        when(projectManager.getProjectObj(1L)).thenReturn(mockProject);

        DefaultProjectService projectService = new MyProjectService();

        final ProjectService.GetProjectResult projectResult = projectService.getProjectById(admin, 1L);
        assertNotNull(projectResult);
        assertEquals(mockProject, projectResult.getProject());
        assertFalse(projectResult.getErrorCollection().hasAnyErrors());
    }

    @Test
    public void testRetrieveProjectByKeyAndActionProjectDoesntExist() {
        String projectKey = "projectKey";

        when(projectManager.getProjectObjByKey(projectKey)).thenReturn(null);

        DefaultProjectService projectService = new MyProjectService();


        final ProjectService.GetProjectResult projectResult = projectService.getProjectByKeyForAction((ApplicationUser) null, projectKey, ProjectAction.VIEW_ISSUES);
        assertNotNull(projectResult);
        assertNull(projectResult.getProject());
        assertEquals(NoopI18nHelper.makeTranslation("admin.errors.project.not.found.for.key", projectKey),
                projectResult.getErrorCollection().getErrorMessages().iterator().next());
    }

    @Test
    public void testRetrieveProjectByKeyAndActionPermissions() {
        final Project project1 = new MockProject(1818L, "ONE");
        final Project project2 = new MockProject(1819L, "TW0");
        final MockApplicationUser expectedUser = new MockApplicationUser("mockUser");

        when(projectManager.getProjectObjByKey(project1.getKey())).thenReturn(project1);
        when(projectManager.getProjectObjByKey(project2.getKey())).thenReturn(project2);

        // user can BROWSE but not PROJECT admin for project1
        when(permissionManager.hasPermission(BROWSE_PROJECTS, project1, expectedUser)).thenReturn(true);
        when(permissionManager.hasPermission(ADMINISTER_PROJECTS, project1, expectedUser)).thenReturn(false);

        // anon can BROWSE project2
        when(permissionManager.hasPermission(BROWSE_PROJECTS, project2, null)).thenReturn(true);

        DefaultProjectService projectService = new MyProjectService();


        ProjectService.GetProjectResult projectResult = projectService.getProjectByKeyForAction(expectedUser, project1.getKey(), ProjectAction.EDIT_PROJECT_CONFIG);
        assertNotNull(projectResult);
        assertNull(projectResult.getProject());
        assertEquals(NoopI18nHelper.makeTranslation("admin.errors.project.no.config.permission"),
                projectResult.getErrorCollection().getErrorMessages().iterator().next());

        projectResult = projectService.getProjectByKeyForAction((ApplicationUser) null, project2.getKey(), ProjectAction.VIEW_PROJECT);
        assertNotNull(projectResult);
        assertEquals(project2, projectResult.getProject());
        assertTrue(projectResult.isValid());
    }

    @Test
    public void testRetrieveProjectKeyId() {
        final String expectedKey = "KEY";
        final ProjectService.GetProjectResult expectedResult =
                new ProjectService.GetProjectResult(new SimpleErrorCollection());

        final DefaultProjectService projectService = new MyProjectService() {
            @Override
            public GetProjectResult getProjectByKeyForAction(ApplicationUser user, String key, ProjectAction action) {
                assertEquals(admin, user);
                assertEquals(expectedKey, key);
                assertSame(ProjectAction.VIEW_ISSUES, action);

                return expectedResult;
            }
        };

        ProjectService.GetProjectResult actualResult = projectService.getProjectByKey(admin, expectedKey);
        assertSame(expectedResult, actualResult);
    }

    @Test
    public void testRetrieveProjectsForUserWithAction() throws Exception {
        final Project mockProject1 = new MockProject(11781L, "ABC");
        final Project mockProject2 = new MockProject(171718L, "EX");
        final List<Project> projects = Arrays.asList(mockProject1, mockProject2);
        final List<Project> checkedProjects = Lists.newArrayList();

        when(projectManager.getProjectObjects()).thenReturn(projects);

        DefaultProjectService projectService = new MyProjectService() {
            @Override
            boolean checkActionPermission(ApplicationUser user, Project project, ProjectAction action) {
                checkedProjects.add(project);
                assertEquals(admin, user);
                assertEquals(ProjectAction.EDIT_PROJECT_CONFIG, action);
                return project.equals(mockProject1);
            }
        };

        final List<Project> expectedList = Lists.newArrayList(mockProject1);
        final ServiceOutcome<List<Project>> outcome = projectService.getAllProjectsForAction(admin, ProjectAction.EDIT_PROJECT_CONFIG);
        assertTrue(outcome.isValid());
        assertEquals(expectedList, outcome.getReturnedValue());
        assertEquals(projects, checkedProjects);
    }

    @Test
    public void testRetrieveProjectsForUser() throws Exception {
        final Project mockProject1 = createProjectObj("ABC");
        final Project mockProject2 = createProjectObj("EX");
        final List<Project> projectArrayList = Lists.newArrayList(mockProject1, mockProject2);

        DefaultProjectService projectService = new MyProjectService() {
            @Override
            public ServiceOutcome<List<Project>> getAllProjectsForAction(ApplicationUser user, ProjectAction action) {
                assertEquals(admin, user);
                assertEquals(ProjectAction.VIEW_ISSUES, action);
                return ServiceOutcomeImpl.ok(projectArrayList);
            }
        };

        final ServiceOutcome<List<Project>> outcome = projectService.getAllProjects(admin);
        assertEquals(projectArrayList, outcome.getReturnedValue());
    }

    public static Project createProjectObj(String projectKey) {
        return new MockProject(18181L, projectKey);
    }

    @Test
    public void testValidateCreateProjectNoPermission() {
        when(permissionManager.hasPermission(Permissions.ADMINISTER, admin)).thenReturn(false);
        when(globalPermissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, admin)).thenReturn(false);

        DefaultProjectService projectService = new MyProjectService();
        ProjectService.CreateProjectValidationResult projectResult = projectService.validateCreateProject(
                admin,
                new ProjectCreationData.Builder()
                        .withName("projectName")
                        .withKey("invalidKey")
                        .withDescription("description")
                        .withLead(admin)
                        .build()
        );

        assertNotNull(projectResult);
        assertFalse(projectResult.isValid());
        assertThat(projectResult.getErrorCollection().getErrorMessages(), hasItem(NoopI18nHelper.makeTranslation("admin.projects.service.error.no.admin.permission")));
    }

    @Test
    public void testValidateCreateProjectBasedOnExistingProjectNoPermission() {
        when(globalPermissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, admin)).thenReturn(false);

        DefaultProjectService projectService = new MyProjectService();
        ProjectService.CreateProjectValidationResult projectResult = projectService.validateCreateProjectBasedOnExistingProject(
                admin,
                10000L,
                new ProjectCreationData.Builder()
                        .withName("projectName")
                        .withKey("invalidKey")
                        .withDescription("description")
                        .withLead(admin)
                        .build()
        );

        assertNotNull(projectResult);
        assertFalse(projectResult.isValid());
        assertThat(projectResult.getErrorCollection().getErrorMessages(), hasItem(NoopI18nHelper.makeTranslation("admin.projects.service.error.no.admin.permission")));
    }

    @Test
    public void testValidateCreateProjectBasedOnExistingThatDoesntExist() {
        long existingProjectId = 10000L;
        when(globalPermissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, admin)).thenReturn(true);
        when(projectManager.getProjectObj(existingProjectId)).thenReturn(null);

        DefaultProjectService projectService = new MyProjectService();
        ProjectService.CreateProjectValidationResult projectResult = projectService.validateCreateProjectBasedOnExistingProject(
                admin,
                existingProjectId,
                new ProjectCreationData.Builder()
                        .withName("projectName")
                        .withKey("invalidKey")
                        .withDescription("description")
                        .withLead(admin)
                        .build()
        );

        assertNotNull(projectResult);
        assertFalse(projectResult.isValid());
        assertThat(projectResult.getErrorCollection().getErrorMessages(), hasItem(NoopI18nHelper.makeTranslation("admin.errors.project.not.found.for.id", existingProjectId)));
    }

    @Test
    public void testValidateUpdateProjectNoPermissionToView() {
        final GenericValue mockProjectGV = new MockGenericValue("Project",
                FieldMap.build("id", 1000L, "key", "HSP", "name", "homosapien"));
        final Project mockProject = new ProjectImpl(mockProjectGV);

        when(projectManager.getProjectObjByKey("HSP")).thenReturn(mockProject);
        when(permissionManager.hasPermission(Permissions.BROWSE, admin)).thenReturn(false);
        when(permissionManager.hasPermission(Permissions.ADMINISTER, admin)).thenReturn(false);
        when(permissionManager.hasPermission(ADMINISTER_PROJECTS, mockProject, admin)).thenReturn(false);

        DefaultProjectService projectService = new MyProjectService();

        final ProjectService.UpdateProjectValidationResult projectResult = projectService.validateUpdateProject(admin,
                "projectName", "HSP", "description", "admin", null, null);

        assertNotNull(projectResult);
        assertFalse(projectResult.isValid());
        assertThat(projectResult.getErrorCollection().getErrorMessages(), hasItem(NoopI18nHelper.makeTranslation("admin.errors.project.not.found.for.key", "HSP")));
    }

    @Test
    public void testValidateUpdateProjectNoPermissionToEdit() {
        final GenericValue mockProjectGV = new MockGenericValue("Project",
                FieldMap.build("id", 1000L, "key", "HSP", "name", "homosapien"));
        final Project mockProject = new ProjectImpl(mockProjectGV);

        when(permissionManager.hasPermission(Permissions.ADMINISTER, admin)).thenReturn(false);
        when(permissionManager.hasPermission(BROWSE_PROJECTS, mockProject, admin)).thenReturn(true);
        when(permissionManager.hasPermission(ADMINISTER_PROJECTS, mockProject, admin)).thenReturn(false);
        when(projectManager.getProjectObjByKey("HSP")).thenReturn(mockProject);

        DefaultProjectService projectService = new MyProjectService();

        final ProjectService.UpdateProjectValidationResult projectResult = projectService.validateUpdateProject(admin,
                "projectName", "HSP", "description", "admin", null, null);

        assertNotNull(projectResult);
        assertFalse(projectResult.isValid());
        assertThat(projectResult.getErrorCollection().getErrorMessages(), hasItem(NoopI18nHelper.makeTranslation("admin.errors.project.no.config.permission")));
    }

    @Test
    public void testValidateCreateProjectNullValues() {
        when(projectManager.getProjectObjByKey(null)).thenReturn(null);
        when(applicationProperties.getDefaultBackedString(APKeys.JIRA_PROJECTKEY_WARNING)).thenReturn("You must specify a valid project key.");
        when(projectTemplateManager.getProjectTemplate(any())).thenReturn(Optional.empty());
        DefaultProjectService projectService = new MyProjectService()
                .setProjectKeyValid(false)
                .setReserveKeyword(false)
                .setUserExists(false);

        final ProjectService.CreateProjectValidationResult projectResult = projectService.validateCreateProject(
                admin,
                new ProjectCreationData.Builder().build()
        );

        assertNotNull(projectResult);
        assertFalse(projectResult.isValid());
        assertTrue(projectResult.getErrorCollection().hasAnyErrors());
        assertEquals(NoopI18nHelper.makeTranslation("admin.errors.must.specify.a.valid.project.name"),
                projectResult.getErrorCollection().getErrors().get("projectName"));
        assertEquals(NoopI18nHelper.makeTranslation("You must specify a valid project key."), projectResult.getErrorCollection().getErrors().get("projectKey"));
    }

    @Test
    public void testValidateUpdateProjectNullValues() {
        when(projectManager.getProjectObjByKey(null)).thenReturn(null);

        DefaultProjectService projectService = new MyProjectService()
                .setProjectKeyValid(false)
                .setReserveKeyword(false)
                .setUserExists(false);

        final ProjectService.UpdateProjectValidationResult projectResult = projectService.validateUpdateProject(admin,
                null, null, null, (String) null, null, null);

        assertNotNull(projectResult);
        assertFalse(projectResult.isValid());
        assertTrue(projectResult.getErrorCollection().hasAnyErrors());
        assertThat(projectResult.getErrorCollection().getErrorMessages(), hasItem(NoopI18nHelper.makeTranslation("admin.errors.project.not.found.for.key", (Object) null)));
    }

    @Test
    public void testValidateCreateProjectAlreadyExists() {
        final Project mockProject = new MockProject(12L, "KEY", "Already here");

        when(projectManager.getProjectObjByName("projectName")).thenReturn(mockProject);
        when(projectManager.getProjectObjByKey("KEY")).thenReturn(mockProject);
        when(applicationProperties.getDefaultBackedString(APKeys.JIRA_PROJECTKEY_MAX_LENGTH)).thenReturn("10");
        when(projectTemplateManager.getProjectTemplate(any())).thenReturn(Optional.empty());

        DefaultProjectService projectService = new MyProjectService();
        final ProjectService.CreateProjectValidationResult projectResult = projectService.validateCreateProject(
                admin,
                new ProjectCreationData.Builder()
                        .withName("projectName")
                        .withKey("KEY")
                        .withDescription("description")
                        .withLead(admin)
                        .build()
        );

        assertNotNull(projectResult);
        assertFalse(projectResult.isValid());
        assertTrue(projectResult.getErrorCollection().hasAnyErrors());
        assertEquals(NoopI18nHelper.makeTranslation("admin.errors.project.with.that.name.already.exists"),
                projectResult.getErrorCollection().getErrors().get("projectName"));
        assertEquals(NoopI18nHelper.makeTranslation("admin.errors.project.with.that.key.already.exists", "Already here"),
                projectResult.getErrorCollection().getErrors().get("projectKey"));
    }

    @Test
    public void testValidateUpdateProjectNotExists() {
        when(projectManager.getProjectObjByKey("KEY")).thenReturn(null);

        DefaultProjectService projectService = new MyProjectService()
                .setProjectKeyValid(true)
                .setReserveKeyword(false)
                .setUserExists(true);

        final ProjectService.UpdateProjectValidationResult projectResult = projectService.validateUpdateProject(admin,
                "projectName", "KEY", "description", "admin", null, null);

        assertNotNull(projectResult);
        assertFalse(projectResult.isValid());
        assertTrue(projectResult.getErrorCollection().hasAnyErrors());
        assertThat(projectResult.getErrorCollection().getErrorMessages(), hasItem(NoopI18nHelper.makeTranslation("admin.errors.project.not.found.for.key", "KEY")));
    }

    @Test
    public void testValidateCreateProjectAvatarDoesNotExists() {
        when(projectTemplateManager.getProjectTemplate(any())).thenReturn(Optional.empty());

        DefaultProjectService projectService = new MyProjectService()
                .setProjectKeyValid(true)
                .setReserveKeyword(false)
                .setUserExists(true);

        final ProjectService.CreateProjectValidationResult projectResult = projectService.validateCreateProject(
                admin,
                new ProjectCreationData.Builder()
                        .withName("projectName")
                        .withKey("KEY")
                        .withDescription("description")
                        .withLead(admin)
                        .withAvatarId(72L)
                        .build()
        );

        assertThat(projectResult.getErrorCollection().getErrorMessages(), contains(NoopI18nHelper.makeTranslation("admin.errors.avatar.does.not.exist", "72")));
    }

    @Test
    public void testValidateUpdateProjectLeadNotExist() {
        final Project mockProject = new ProjectImpl(null) {
            public String getKey() {
                return "KEY";
            }

            public Long getId() {
                return 1L;
            }
        };
        when(projectManager.getProjectObj(1L)).thenReturn(mockProject);
        when(projectManager.getProjectObjByKey("KEY")).thenReturn(mockProject);
        when(projectManager.getProjectObjByName("KEY")).thenReturn(mockProject);
        when(applicationProperties.getDefaultBackedString(APKeys.JIRA_PROJECTNAME_MAX_LENGTH)).thenReturn("80");

        DefaultProjectService projectService = new MyProjectService()
                .setProjectKeyValid(true)
                .setReserveKeyword(false)
                .setUserExists(false);

        final ProjectService.UpdateProjectValidationResult projectResult = projectService.validateUpdateProject(admin,
                "projectName", "KEY", "description", "non-existing-lead", null, null);

        assertNotNull(projectResult);
        assertFalse(projectResult.isValid());
        assertTrue(projectResult.getErrorCollection().hasAnyErrors());
        assertThat(projectResult.getErrorCollection().getErrors().get("projectLead"), equalTo(NoopI18nHelper.makeTranslation("admin.errors.must.specify.project.lead")));
    }

    @Test
    public void testValidateUpdateProjectWithInvalidProjectType() {
        final Project mockProject = new ProjectImpl(null) {
            public String getKey() {
                return "KEY";
            }

            public Long getId() {
                return 1L;
            }
        };
        when(projectManager.getProjectObj(1L)).thenReturn(mockProject);
        when(projectManager.getProjectObjByKey("KEY")).thenReturn(mockProject);
        when(projectManager.getProjectObjByName("KEY")).thenReturn(mockProject);
        when(projectTypeValidator.isValid(eq(admin), any(ProjectTypeKey.class))).thenReturn(false);

        DefaultProjectService projectService = new MyProjectService()
                .setProjectKeyValid(true)
                .setReserveKeyword(false)
                .setUserExists(false);

        ProjectService.UpdateProjectRequest request = new ProjectService.UpdateProjectRequest(mockProject);

        request.projectType("ProjectType");

        final ProjectService.UpdateProjectValidationResult projectResult = projectService.validateUpdateProject(admin, request);

        assertThat(projectResult.isValid(), is(false));

        assertThat(projectResult.getErrorCollection().getErrors().containsKey("projectType"), is(true));
    }

    @Test
    public void testValidateCreateProjectLongNameUrlAndKey() {
        String longProjectKey = StringUtils.repeat("B", 11);

        when(projectManager.getProjectObjByKey(longProjectKey)).thenReturn(null);
        when(applicationProperties.getDefaultBackedString(APKeys.JIRA_PROJECTKEY_MAX_LENGTH)).thenReturn("10");
        when(projectTemplateManager.getProjectTemplate(any())).thenReturn(Optional.empty());

        DefaultProjectService projectService = new MyProjectService()
                .setProjectKeyValid(true)
                .setReserveKeyword(false)
                .setUserExists(true);

        final ProjectService.CreateProjectValidationResult projectResult = projectService.validateCreateProject(
                admin,
                new ProjectCreationData.Builder()
                        .withName(StringUtils.repeat("A", 81))
                        .withKey(longProjectKey)
                        .withDescription("description")
                        .withLead(admin)
                        .withUrl(StringUtils.repeat("C", 256))
                        .build()
        );

        assertNotNull(projectResult);
        assertFalse(projectResult.isValid());
        assertTrue(projectResult.getErrorCollection().hasAnyErrors());
        assertEquals(3, projectResult.getErrorCollection().getErrors().size());
        assertEquals(NoopI18nHelper.makeTranslation("admin.errors.project.url.too.long"),
                projectResult.getErrorCollection().getErrors().get("projectUrl"));
        assertEquals(NoopI18nHelper.makeTranslation("admin.errors.project.name.too.long", "80"),
                projectResult.getErrorCollection().getErrors().get("projectName"));
        assertEquals(NoopI18nHelper.makeTranslation("admin.errors.project.key.too.long", "10"),
                projectResult.getErrorCollection().getErrors().get("projectKey"));
    }

    @Test
    public void testValidateCreateProjectWithNonDefaultNameLength() {
        String longProjectName = StringUtils.repeat("A", 11);

        when(applicationProperties.getDefaultBackedString(APKeys.JIRA_PROJECTKEY_MAX_LENGTH)).thenReturn("10");
        when(applicationProperties.getDefaultBackedString(APKeys.JIRA_PROJECTNAME_MAX_LENGTH)).thenReturn("10");
        when(projectTemplateManager.getProjectTemplate(any())).thenReturn(Optional.empty());

        DefaultProjectService projectService = new MyProjectService();

        final ProjectService.CreateProjectValidationResult projectResult = projectService.validateCreateProject(
                admin,
                new ProjectCreationData.Builder()
                        .withName(longProjectName)
                        .withKey("KEY")
                        .withDescription("description")
                        .withLead(admin)
                        .withUrl(StringUtils.repeat("C", 200))
                        .build()
        );

        assertNotNull(projectResult);
        assertFalse(projectResult.isValid());
        assertTrue(projectResult.getErrorCollection().hasAnyErrors());
        assertEquals(2, projectResult.getErrorCollection().getErrors().size());
        assertEquals(NoopI18nHelper.makeTranslation("admin.errors.url.specified.is.not.valid"),
                projectResult.getErrorCollection().getErrors().get("projectUrl"));
        assertEquals(NoopI18nHelper.makeTranslation("admin.errors.project.name.too.long", "10"),
                projectResult.getErrorCollection().getErrors().get("projectName"));
    }

    @Test
    public void testValidateCreateProjectWithTooShortName() {
        String longProjectName = StringUtils.repeat("A", 1);

        when(applicationProperties.getDefaultBackedString(APKeys.JIRA_PROJECTKEY_MAX_LENGTH)).thenReturn("10");
        when(applicationProperties.getDefaultBackedString(APKeys.JIRA_PROJECTNAME_MAX_LENGTH)).thenReturn("10");
        when(projectTemplateManager.getProjectTemplate(any())).thenReturn(Optional.empty());

        DefaultProjectService projectService = new MyProjectService();

        final ProjectService.CreateProjectValidationResult projectResult = projectService.validateCreateProject(
                admin,
                new ProjectCreationData.Builder()
                        .withName(longProjectName)
                        .withKey("KEY")
                        .withDescription("description")
                        .withLead(admin)
                        .withUrl(StringUtils.repeat("C", 200))
                        .build()
        );

        assertNotNull(projectResult);
        assertFalse(projectResult.isValid());
        assertTrue(projectResult.getErrorCollection().hasAnyErrors());
        assertEquals(2, projectResult.getErrorCollection().getErrors().size());
        assertEquals(NoopI18nHelper.makeTranslation("admin.errors.url.specified.is.not.valid"),
                projectResult.getErrorCollection().getErrors().get("projectUrl"));
        assertEquals(NoopI18nHelper.makeTranslation("admin.errors.project.name.too.short", "2"),
                projectResult.getErrorCollection().getErrors().get("projectName"));
    }

    @Test
    public void testValidateCreateProjectWithNonDefaultKeyLength() {
        String longProjectKey = StringUtils.repeat("B", 6);
        String validProjectName = StringUtils.repeat("A", 10);

        when(projectManager.getProjectObjByName(validProjectName)).thenReturn(null);
        when(projectManager.getProjectObjByKey(longProjectKey)).thenReturn(null);
        when(applicationProperties.getDefaultBackedString(APKeys.JIRA_PROJECTKEY_MAX_LENGTH)).thenReturn("5");
        when(projectTemplateManager.getProjectTemplate(any())).thenReturn(Optional.empty());

        DefaultProjectService projectService = new MyProjectService();

        final ProjectService.CreateProjectValidationResult projectResult = projectService.validateCreateProject(
                admin,
                new ProjectCreationData.Builder()
                        .withName(validProjectName)
                        .withKey(longProjectKey)
                        .withDescription("description")
                        .withLead(admin)
                        .withUrl(StringUtils.repeat("C", 200))
                        .build()
        );

        assertNotNull(projectResult);
        assertFalse(projectResult.isValid());
        assertTrue(projectResult.getErrorCollection().hasAnyErrors());
        assertEquals(2, projectResult.getErrorCollection().getErrors().size());
        assertEquals(NoopI18nHelper.makeTranslation("admin.errors.url.specified.is.not.valid"),
                projectResult.getErrorCollection().getErrors().get("projectUrl"));
        assertEquals(NoopI18nHelper.makeTranslation("admin.errors.project.key.too.long", "5"),
                projectResult.getErrorCollection().getErrors().get("projectKey"));
    }

    @Test
    public void testValidateCreateProjectWithNoProjectTypeRetainsPassedTemplateAndInfersTypeFromTemplate() {
        final String projectKey = "KEY";
        final String projectName = "My Project";
        final ProjectTemplateKey projectTemplateKey = new ProjectTemplateKey("com.atlassian.jira-core-project-templates:jira-core-project-management");
        final ProjectTypeKey projectTypeKey = new ProjectTypeKey("business");
        final ProjectTemplate projectTemplate = new ProjectTemplate(projectTemplateKey, projectTypeKey, 0, null, null, null, null, null, null, null);
        final ProjectCreationData projectCreationData = new ProjectCreationData.Builder()
                .withKey(projectKey)
                .withName(projectName)
                .withLead(admin)
                .withProjectTemplateKey(projectTemplateKey.getKey())
                .build();

        when(projectTemplateManager.getProjectTemplate(projectTemplateKey)).thenReturn(Optional.of(projectTemplate));

        final DefaultProjectService projectService = new MyProjectService();
        final ProjectService.CreateProjectValidationResult createProjectValidationResult = projectService.validateCreateProject(admin, projectCreationData);

        assertThat(createProjectValidationResult.getErrorCollection().hasAnyErrors(), is(false));
        assertThat(createProjectValidationResult.getProjectCreationData().getProjectTypeKey(), is(projectTypeKey));
        assertThat(createProjectValidationResult.getProjectCreationData().getProjectTemplateKey(), is(projectTemplateKey));
    }

    @Test
    public void testValidateCreateProjectWithNoProjectTemplateRetainsPassedTypeAndTemplate() {
        final String projectKey = "KEY";
        final String projectName = "My Project";
        final ProjectTypeKey projectTypeKey = new ProjectTypeKey("business");
        final ProjectCreationData projectCreationData = new ProjectCreationData.Builder()
                .withKey(projectKey)
                .withName(projectName)
                .withLead(admin)
                .withType(projectTypeKey)
                .build();

        final DefaultProjectService projectService = new MyProjectService();
        final ProjectService.CreateProjectValidationResult createProjectValidationResult = projectService.validateCreateProject(admin, projectCreationData);

        assertThat(createProjectValidationResult.getErrorCollection().hasAnyErrors(), is(false));
        assertThat(createProjectValidationResult.getProjectCreationData().getProjectTemplateKey(), nullValue());
        assertThat(createProjectValidationResult.getProjectCreationData().getProjectTypeKey(), is(projectTypeKey));
    }

    @Test
    public void testValidateCreateProjectWithNoProjectTypeOrTemplateReturnsInvalidTypeError() {
        final String projectKey = "KEY";
        final String projectName = "My Project";
        final ProjectCreationData projectCreationData = new ProjectCreationData.Builder()
                .withKey(projectKey)
                .withName(projectName)
                .withLead(admin)
                .build();

        when(projectTemplateManager.getProjectTemplate(null)).thenReturn(Optional.empty());
        when(projectTypeValidator.isValid(admin, null)).thenReturn(false);

        final DefaultProjectService projectService = new MyProjectService();
        final ProjectService.CreateProjectValidationResult createProjectValidationResult = projectService.validateCreateProject(admin, projectCreationData);

        assertThat(createProjectValidationResult.getErrorCollection().hasAnyErrors(), is(true));
        assertThat(createProjectValidationResult.getErrorCollection().getErrors().size(), is(1));
        assertThat(createProjectValidationResult.getErrorCollection().getErrors().get(ProjectService.PROJECT_TYPE), is(NoopI18nHelper.makeTranslation("admin.errors.invalid.project.type")));
    }

    @Test
    public void testValidateCreateProjectWithMatchingProjectTypeAndTemplateRetainsPassedTypeAndTemplate() {
        final String projectKey = "KEY";
        final String projectName = "My Project";
        final ProjectTemplateKey projectTemplateKey = new ProjectTemplateKey("com.atlassian.jira-core-project-templates:jira-core-project-management");
        final ProjectTypeKey projectTypeKey = new ProjectTypeKey("business");
        final ProjectTemplate projectTemplate = new ProjectTemplate(projectTemplateKey, projectTypeKey, 0, null, null, null, null, null, null, null);
        final ProjectCreationData projectCreationData = new ProjectCreationData.Builder()
                .withKey(projectKey)
                .withName(projectName)
                .withLead(admin)
                .withType(projectTypeKey)
                .withProjectTemplateKey(projectTemplateKey.getKey())
                .build();

        when(projectTemplateManager.getProjectTemplate(projectTemplateKey)).thenReturn(Optional.of(projectTemplate));

        final DefaultProjectService projectService = new MyProjectService();
        final ProjectService.CreateProjectValidationResult createProjectValidationResult = projectService.validateCreateProject(admin, projectCreationData);

        assertThat(createProjectValidationResult.getErrorCollection().hasAnyErrors(), is(false));
        assertThat(createProjectValidationResult.getProjectCreationData().getProjectTypeKey(), is(projectTypeKey));
        assertThat(createProjectValidationResult.getProjectCreationData().getProjectTemplateKey(), is(projectTemplateKey));
    }

    @Test
    public void testValidateCreateProjectWithInvalidTemplateReturnsInvalidTemplateError() {
        final String projectKey = "KEY";
        final String projectName = "My Project";
        final ProjectTemplateKey projectTemplateKey = new ProjectTemplateKey("com.atlassian.jira-core-project-templates:jira-core-project-management");
        final ProjectTypeKey projectTypeKey = new ProjectTypeKey("business");
        final ProjectCreationData projectCreationData = new ProjectCreationData.Builder()
                .withKey(projectKey)
                .withName(projectName)
                .withLead(admin)
                .withType(projectTypeKey)
                .withProjectTemplateKey(projectTemplateKey.getKey())
                .build();

        when(projectTemplateManager.getProjectTemplate(projectTemplateKey)).thenReturn(Optional.empty());

        final DefaultProjectService projectService = new MyProjectService();
        final ProjectService.CreateProjectValidationResult createProjectValidationResult = projectService.validateCreateProject(admin, projectCreationData);

        assertThat(createProjectValidationResult.getErrorCollection().hasAnyErrors(), is(true));
        assertThat(createProjectValidationResult.getErrorCollection().getErrorMessages().size(), is(1));
        assertThat(createProjectValidationResult.getErrorCollection().getErrorMessages(), hasItem(NoopI18nHelper.makeTranslation("admin.errors.invalid.project.template")));
    }

    @Test
    public void testValidateCreateProjectWithoutMatchingProjectTypeAndTemplateReturnsTemplateTypeMismatchError() {
        final String projectKey = "KEY";
        final String projectName = "My Project";
        final ProjectTemplateKey projectTemplateKey = new ProjectTemplateKey("com.atlassian.jira-core-project-templates:jira-core-project-management");
        final ProjectTypeKey projectTypeKey = new ProjectTypeKey("business");
        final ProjectTypeKey differentProjectTypeKey = new ProjectTypeKey("software");
        final ProjectTemplate projectTemplate = new ProjectTemplate(projectTemplateKey, projectTypeKey, 0, null, null, null, null, null, null, null);
        final ProjectCreationData projectCreationData = new ProjectCreationData.Builder()
                .withKey(projectKey)
                .withName(projectName)
                .withLead(admin)
                .withType(differentProjectTypeKey)
                .withProjectTemplateKey(projectTemplateKey.getKey())
                .build();

        when(projectTemplateManager.getProjectTemplate(projectTemplateKey)).thenReturn(Optional.of(projectTemplate));

        final DefaultProjectService projectService = new MyProjectService();
        final ProjectService.CreateProjectValidationResult createProjectValidationResult = projectService.validateCreateProject(admin, projectCreationData);

        assertThat(createProjectValidationResult.getErrorCollection().hasAnyErrors(), is(true));
        assertThat(createProjectValidationResult.getErrorCollection().getErrorMessages().size(), is(1));
        assertThat(createProjectValidationResult.getErrorCollection().getErrorMessages(), hasItem(NoopI18nHelper.makeTranslation("admin.errors.project.template.type.mismatch")));
    }

    @Test
    public void testValidateUpdateProjectInvalidUrl() {
        final Project mockProject = new ProjectImpl(null) {
            public String getKey() {
                return "KEY";
            }

            public Long getId() {
                return 12L;
            }
        };

        when(projectManager.getProjectObj(12L)).thenReturn(mockProject);
        when(projectManager.getProjectObjByKey("KEY")).thenReturn(mockProject);
        when(projectManager.getProjectObjByName("projectName")).thenReturn(mockProject);
        when(applicationProperties.getDefaultBackedString(APKeys.JIRA_PROJECTNAME_MAX_LENGTH)).thenReturn("80");

        DefaultProjectService projectService = new MyProjectService()
                .setProjectKeyValid(true)
                .setReserveKeyword(false)
                .setUserExists(true);

        final ProjectService.UpdateProjectValidationResult projectResult = projectService.validateUpdateProject(admin,
                "projectName", "KEY", "description", "admin", "invalidUrl", null);

        assertNotNull(projectResult);
        assertFalse(projectResult.isValid());
        assertTrue(projectResult.getErrorCollection().hasAnyErrors());
        assertThat(projectResult.getErrorCollection().getErrors().get("projectUrl"), equalTo(NoopI18nHelper.makeTranslation("admin.errors.url.specified.is.not.valid")));
    }

    @Test
    public void testValidateUpdateProjectKeyUsedByDifferentProject() {
        ProjectManager projectManager = mock(ProjectManager.class);
        final ApplicationUser user = new MockApplicationUser("admin");
        final ApplicationProperties applicationProperties = mock(ApplicationProperties.class);

        final Project mockProject = new MockProject(11L, "KEY");
        final Project abcProject = new MockProject(12L, "ABC");

        userManager.addUser(user);
        when(permissionManager.hasPermission(eq(Permissions.ADMINISTER), any(ApplicationUser.class))).thenReturn(true);
        when(projectManager.getProjectObj(11L)).thenReturn(mockProject);
        when(projectManager.getProjectObjByKey("KEY")).thenReturn(mockProject);
        when(projectManager.getProjectObj(12L)).thenReturn(abcProject);
        when(projectManager.getProjectObjByKey("ABC")).thenReturn(abcProject);
        when(projectManager.getProjectObjByName("projectName")).thenReturn(mockProject);
        when(applicationProperties.getDefaultBackedString(APKeys.JIRA_PROJECTNAME_MAX_LENGTH)).thenReturn("80");

        DefaultProjectService projectService = new DefaultProjectService(null, projectManager, applicationProperties, permissionManager, globalPermissionManager, null,
                null, null, null, null, null, null, null, null, null, null, null, i18nFactory, null, userManager, null, null, null, null, null, null, null, null, null, null) {
            boolean isProjectKeyValid(final String key) {
                return true;
            }

            boolean isReservedKeyword(final String key) {
                return false;
            }

            boolean checkUserExists(final String user) {
                return true;
            }
        };

        final ProjectService.UpdateProjectValidationResult projectResult = projectService.validateUpdateProject(user, mockProject,
                "projectName", "ABC", "description", user, null, null, null);

        assertNotNull(projectResult);
        assertThat(projectResult.isValid(), equalTo(false));
        assertTrue(projectResult.getErrorCollection().hasAnyErrors());
        assertThat(projectResult.getErrorCollection().getErrors().get("projectKey"), equalTo(NoopI18nHelper.makeTranslation("admin.errors.project.with.that.key.already.exists", "ABC")));
    }

    @Test
    public void testValidateUpdateProjectKeyByProjectAdmin() {
        ProjectManager projectManager = mock(ProjectManager.class);
        final ApplicationUser user = new MockApplicationUser("projectadmin");
        final ApplicationProperties applicationProperties = mock(ApplicationProperties.class);

        final Project abcProject = new MockProject(12L, "ABC");

        userManager.addUser(user);
        //when(userManager.getUserByName("projectadmin")).thenReturn(user);
        when(permissionManager.hasPermission(eq(ADMINISTER_PROJECTS), any(Project.class), any(ApplicationUser.class))).thenReturn(true);
        when(projectManager.getProjectObj(abcProject.getId())).thenReturn(abcProject);
        when(projectManager.getProjectObjByKey("ABC")).thenReturn(abcProject);
        when(projectManager.getProjectObjByName("projectName")).thenReturn(abcProject);
        when(applicationProperties.getDefaultBackedString(APKeys.JIRA_PROJECTNAME_MAX_LENGTH)).thenReturn("80");

        DefaultProjectService projectService = new DefaultProjectService(null, projectManager, applicationProperties, permissionManager, globalPermissionManager, null,
                null, null, null, null, null, null, null, null, null, null, null, i18nFactory, null, userManager, null, null, null, null, null, null, null, null, null, null) {
            boolean isProjectKeyValid(final String key) {
                return true;
            }

            boolean isReservedKeyword(final String key) {
                return false;
            }

            boolean checkUserExists(final String user) {
                return true;
            }
        };

        final ProjectService.UpdateProjectValidationResult projectResult = projectService.validateUpdateProject(user, abcProject,
                "projectName", "NEWABC", "description", user, null, null, null);

        assertNotNull(projectResult);
        assertThat(projectResult.isValid(), equalTo(false));
        assertTrue("there should be errors", projectResult.getErrorCollection().hasAnyErrors());
        assertThat(projectResult.getErrorCollection().getErrorMessages(),
                hasItem(NoopI18nHelper.makeTranslation("admin.projects.service.error.no.admin.permission.key")));
    }

    @Test
    public void testValidateUpdateProjectKeyUsedByTheSameProject() {
        ProjectManager projectManager = mock(ProjectManager.class);
        final ApplicationUser user = new MockApplicationUser("admin");
        final ApplicationProperties applicationProperties = mock(ApplicationProperties.class);

        final MockProject mockProject = new MockProject(11L, "KEY");
        mockProject.setLead(user);
        mockProject.setAvatar(new MockAvatar(1L, null, null, IconType.USER_ICON_TYPE, null, false));

        when(permissionManager.hasPermission(eq(Permissions.ADMINISTER), any(ApplicationUser.class))).thenReturn(true);
        when(projectManager.getProjectObj(11L)).thenReturn(mockProject);
        when(projectManager.getProjectObjByKey("KEY")).thenReturn(mockProject);
        when(projectManager.getProjectObjByKey("ABC")).thenReturn(mockProject);
        when(projectManager.getProjectObjByName("projectName")).thenReturn(mockProject);
        when(applicationProperties.getDefaultBackedString(APKeys.JIRA_PROJECTNAME_MAX_LENGTH)).thenReturn("80");

        DefaultProjectService projectService = new DefaultProjectService(null, projectManager, applicationProperties, permissionManager, globalPermissionManager, null,
                null, null, null, null, null, null, null, null, null, null, null, i18nFactory, null, userManager, null, null, null, null, null, null, null, null, null, null) {
            boolean isProjectKeyValid(final String key) {
                return true;
            }

            boolean isReservedKeyword(final String key) {
                return false;
            }
        };

        final ProjectService.UpdateProjectValidationResult projectResult = projectService.validateUpdateProject(user, mockProject,
                "projectName", "ABC", "description", new MockApplicationUser("admin"), null, null, null);

        assertNotNull(projectResult);
        assertThat(projectResult.isValid(), equalTo(true));
    }

    @Test
    public void testValidateUpdateProjectLongNameAndUrl() {
        final String projectKey = "KEY";
        final Project mockProject = new ProjectImpl(null) {
            @Override
            public String getKey() {
                return projectKey;
            }

            @Override
            public Long getId() {
                return 12L;
            }
        };

        when(projectManager.getProjectObj(mockProject.getId())).thenReturn(mockProject);
        when(projectManager.getProjectObjByKey(projectKey)).thenReturn(mockProject);

        DefaultProjectService projectService = new MyProjectService() {
            @Override
            public int getMaximumNameLength() {
                return 80;
            }
        }.setUserExists(true);

        final ProjectService.UpdateProjectValidationResult projectResult = projectService.validateUpdateProject(admin,
                StringUtils.repeat("N", 151), projectKey, "description", "admin", StringUtils.repeat("U", 256), null);

        assertNotNull(projectResult);
        assertFalse(projectResult.isValid());
        assertTrue(projectResult.getErrorCollection().hasAnyErrors());
        assertEquals(2, projectResult.getErrorCollection().getErrors().size());
        assertThat(projectResult.getErrorCollection().getErrors().get("projectUrl"), equalTo(NoopI18nHelper.makeTranslation("admin.errors.project.url.too.long")));
        assertThat(projectResult.getErrorCollection().getErrors().get("projectName"), equalTo(NoopI18nHelper.makeTranslation("admin.errors.project.name.too.long", projectService.getMaximumNameLength())));
    }

    @Test
    public void defaultAssigneeShouldBeUnassignedIfGlobalAllowUnassignedSettingIsOn() {
        projectManager = new MockProjectManager();
        when(applicationProperties.getOption(APKeys.JIRA_OPTION_ALLOWUNASSIGNED)).thenReturn(true);
        when(projectTemplateManager.getProjectTemplate(any())).thenReturn(Optional.empty());

        DefaultProjectService projectService = new MyProjectService();

        final ProjectService.CreateProjectValidationResult projectResult = projectService.validateCreateProject(
                admin,
                new ProjectCreationData.Builder()
                        .withName("projectName")
                        .withKey("KEY")
                        .withDescription("description")
                        .withLead(admin)
                        .build()
        );

        assertTrue(projectResult.isValid());
        assertThat("assignee type should be null in validation result", projectResult.getProjectCreationData().getAssigneeType(), equalTo(null));

        Project createdProject = projectService.createProject(projectResult);
        assertThat(createdProject.getAssigneeType(), equalTo(AssigneeTypes.UNASSIGNED));
    }

    @Test
    public void defaultAssigneeShouldBeProjectLeadIfGlobalAllowUnassignedSettingIsOff() {
        projectManager = new MockProjectManager();
        when(applicationProperties.getOption(APKeys.JIRA_OPTION_ALLOWUNASSIGNED)).thenReturn(false);
        when(projectTemplateManager.getProjectTemplate(any())).thenReturn(Optional.empty());

        DefaultProjectService projectService = new MyProjectService();

        final ProjectService.CreateProjectValidationResult projectResult = projectService.validateCreateProject(
                admin,
                new ProjectCreationData.Builder()
                        .withName("projectName")
                        .withKey("KEY")
                        .withDescription("description")
                        .withLead(admin)
                        .build()
        );

        assertTrue(projectResult.isValid());
        assertThat("assignee type should be null in validation result", projectResult.getProjectCreationData().getAssigneeType(), equalTo(null));

        Project createdProject = projectService.createProject(projectResult);
        assertThat(createdProject.getAssigneeType(), equalTo(AssigneeTypes.PROJECT_LEAD));
    }

    @Test
    public void testValidateUpdateProjectAssigneeType() {
        final Project mockProject = new MockProject(1L, "KEY");

        when(projectManager.getProjectObj(1L)).thenReturn(mockProject);
        when(projectManager.getProjectObjByKey("KEY")).thenReturn(mockProject);
        when(projectManager.getProjectObjByName("projectName")).thenReturn(mockProject);
        when(applicationProperties.getDefaultBackedString(APKeys.JIRA_PROJECTNAME_MAX_LENGTH)).thenReturn("80");

        DefaultProjectService projectService = new MyProjectService()
                .setProjectKeyValid(true)
                .setReserveKeyword(false)
                .setUserExists(true);

        final ProjectService.UpdateProjectValidationResult projectResult = projectService.validateUpdateProject(admin,
                "projectName", "KEY", "description", "admin", null, new Long(-1));

        assertNotNull(projectResult);
        assertFalse(projectResult.isValid());
        assertTrue(projectResult.getErrorCollection().hasAnyErrors());
        assertThat(projectResult.getErrorCollection().getErrorMessages(), hasItem(NoopI18nHelper.makeTranslation("admin.errors.invalid.default.assignee")));
    }

    @Test
    public void testValidateSchemesNoPermission() {
        when(permissionManager.hasPermission(Permissions.ADMINISTER, admin)).thenReturn(false);

        DefaultProjectService projectService = new MyProjectService();

        final ProjectService.UpdateProjectSchemesValidationResult result = projectService
                .validateUpdateProjectSchemes(admin, 1L, 1L, 1L);
        assertNotNull(result);
        assertFalse(result.isValid());
        assertTrue(result.getErrorCollection().hasAnyErrors());
        assertThat(result.getErrorCollection().getErrorMessages(), hasItem(NoopI18nHelper.makeTranslation("admin.projects.service.error.no.admin.permission")));
    }

    @Test
    public void testValidateSchemesNullSchemes() throws Exception {

        DefaultProjectService projectService = new MyProjectService();

        final ProjectService.UpdateProjectSchemesValidationResult projectResult = projectService
                .validateUpdateProjectSchemes(admin,
                        null, null, null);

        assertNotNull(projectResult);
        assertTrue(projectResult.isValid());
    }

    @Test
    public void testValidateSchemesMinusOne() throws Exception {
        DefaultProjectService projectService = new MyProjectService();

        final Long schemeId = -1L;
        final ProjectService.UpdateProjectSchemesValidationResult projectResult = projectService
                .validateUpdateProjectSchemes(admin,
                        schemeId, schemeId, schemeId);

        assertNotNull(projectResult);
        assertTrue(projectResult.isValid());
    }

    @Test
    public void testValidateSchemesNotExistEnterprise() throws Exception {
        final Long schemeId = 1L;
        when(permissionSchemeManager.getScheme(schemeId)).thenReturn(null);
        when(notificationSchemeManager.getScheme(schemeId)).thenReturn(null);
        when(issueSecuritySchemeManager.getIssueSecurityLevelScheme(schemeId)).thenReturn(null);

        DefaultProjectService projectService = new MyProjectService();

        final ProjectService.UpdateProjectSchemesValidationResult projectResult = projectService
                .validateUpdateProjectSchemes(admin,
                        schemeId, schemeId, schemeId);

        assertNotNull(projectResult);
        assertFalse(projectResult.isValid());
        assertTrue(projectResult.getErrorCollection().hasAnyErrors());

        ErrorCollection errors = projectResult.getErrorCollection();
        assertTrue(errors.hasAnyErrors());
        assertThat(errors.getErrorMessages(), hasItem(NoopI18nHelper.makeTranslation("admin.errors.project.validation.permission.scheme.not.retrieved")));
        assertThat(errors.getErrorMessages(), hasItem(NoopI18nHelper.makeTranslation("admin.errors.project.validation.notification.scheme.not.retrieved")));
        assertThat(errors.getErrorMessages(), hasItem(NoopI18nHelper.makeTranslation("admin.errors.project.validation.issuesecurity.scheme.not.retrieved")));
    }

    @Test
    public void testValidateSchemesExistEnterprise() throws Exception {
        final Long schemeId = 1L;
        MockGenericValue permissionScheme = new MockGenericValue("permissionScheme", new HashMap());
        MockGenericValue notificationScheme = new MockGenericValue("notificationScheme", new HashMap());

        when(permissionSchemeManager.getScheme(schemeId)).thenReturn(permissionScheme);
        when(notificationSchemeManager.getScheme(schemeId)).thenReturn(notificationScheme);
        when(issueSecuritySchemeManager.getIssueSecurityLevelScheme(schemeId)).thenReturn(new IssueSecurityLevelScheme(12L, "blah", "", null));

        DefaultProjectService projectService = new MyProjectService();

        final ProjectService.UpdateProjectSchemesValidationResult projectResult = projectService.validateUpdateProjectSchemes(admin, schemeId, schemeId, schemeId);

        assertNotNull(projectResult);
        assertTrue(projectResult.isValid());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateProjectNullResult() throws Exception {
        DefaultProjectService projectService = new MyProjectService();

        projectService.createProject(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testUpdateProjectNullResult() throws Exception {
        DefaultProjectService projectService = new MyProjectService();

        projectService.updateProject(null);
    }

    @Test(expected = IllegalStateException.class)
    public void testCreateProjectErrorResult() throws Exception {
        DefaultProjectService projectService = new MyProjectService();

        SimpleErrorCollection errorCollection = new SimpleErrorCollection();
        errorCollection.addError("field", "error");
        ProjectService.CreateProjectValidationResult result = new ProjectService.CreateProjectValidationResult(errorCollection);

        projectService.createProject(result);
    }

    @Test(expected = IllegalStateException.class)
    public void testUpdateProjectErrorResult() throws Exception {
        DefaultProjectService projectService = new MyProjectService();

        SimpleErrorCollection errorCollection = new SimpleErrorCollection();
        errorCollection.addError("field", "error");
        ProjectService.UpdateProjectValidationResult result = new ProjectService.UpdateProjectValidationResult(errorCollection);

        projectService.updateProject(result);
    }

    @Test
    public void testCreateProjectSuccess() throws Exception {
        final Project mockProject = new ProjectImpl(null);
        String projectType = "type";

        when(projectManager.createProject(admin, new ProjectCreationData.Builder()
                .withName("projectName")
                .withKey("KEY")
                .withType(projectType)
                .withLead(admin)
                .withAssigneeType(AssigneeTypes.PROJECT_LEAD)
                .build()
        )).thenReturn(mockProject);


        SimpleErrorCollection errorCollection = new SimpleErrorCollection();
        ProjectService.CreateProjectValidationResult result = new ProjectService.CreateProjectValidationResult(errorCollection, admin,
                new ProjectCreationData.Builder().withName("projectName").withKey("KEY").withLead(admin).withType(projectType).build()
        );

        ProjectService projectService = new MyProjectService();
        Project project = projectService.createProject(result);

        assertNotNull(project);
        verify(projectEventManager, times(1)).dispatchProjectCreated(isA(ApplicationUser.class), isA(Project.class));
        verify(projectSchemeAssociationManager, times(1)).associateDefaultSchemesWithProject(project);
        verify(workflowSchemeManager, times(1)).clearWorkflowCache();
    }

    @Test(expected = IllegalStateException.class)
    public void testCreateProjectBasedOnExistingWithoutExisingProject() throws Exception {
        when(projectManager.getProjectObj(10200L)).thenReturn(null);
        DefaultProjectService projectService = new MyProjectService();

        SimpleErrorCollection errorCollection = new SimpleErrorCollection();
        errorCollection.addError("field", "error");
        ProjectService.CreateProjectValidationResult result = new ProjectService.CreateProjectValidationResult(
                errorCollection, admin, new ProjectCreationData.Builder().build(), Optional.of(10200L));

        projectService.createProject(result);
    }

    @Test
    public void testCreateProjectBasedOnExistingSuccess() throws Exception {
        final ProjectCategoryImpl existingProjectCategory = new ProjectCategoryImpl(10010L, "Sample Category", "");
        final MockProject existingProject = new MockProject(10000L);
        existingProject.setProjectCategory(existingProjectCategory);

        final Project newProject = new ProjectImpl(null);
        final String projectType = "type";

        when(projectManager.getProjectObj(10000L)).thenReturn(existingProject);
        when(projectManager.createProject(admin, new ProjectCreationData.Builder()
                .withName("projectName")
                .withKey("KEY")
                .withType(projectType)
                .withLead(admin)
                .withAssigneeType(AssigneeTypes.PROJECT_LEAD)
                .build()
        )).thenReturn(newProject);


        SimpleErrorCollection errorCollection = new SimpleErrorCollection();
        ProjectService.CreateProjectValidationResult result = new ProjectService.CreateProjectValidationResult(errorCollection, admin,
                new ProjectCreationData.Builder().withName("projectName").withKey("KEY").withLead(admin).withType(projectType).build(), Optional.of(10000L)
        );

        ProjectService projectService = new MyProjectService();
        Project project = projectService.createProject(result);

        assertNotNull(project);
        verify(projectManager, times(1)).setProjectCategory(eq(newProject), eq(existingProjectCategory));
        verify(projectEventManager, times(1)).dispatchProjectCreated(isA(ApplicationUser.class), isA(Project.class));
        verify(projectSchemeAssociationManager, times(1)).associateSchemesOfExistingProjectWithNewProject(project, existingProject);
        verify(workflowSchemeManager, times(1)).clearWorkflowCache();
    }

    @Test
    public void testUpdateProjectSuccess() throws Exception {
        final Project mockProject = new ProjectImpl(null) {
            public String getKey() {
                return "KEY";
            }

            public Long getId() {
                return 12L;
            }
        };

        when(projectManager.updateProject(any(UpdateProjectParameters.class))).thenReturn(mockProject);

        SimpleErrorCollection errorCollection = new SimpleErrorCollection();
        ProjectService.UpdateProjectValidationResult result = new ProjectService.UpdateProjectValidationResult(errorCollection,
                "projectName", "KEY", null, "admin", null, null, null, mockProject, false, admin);

        DefaultProjectService projectService = new MyProjectService();
        Project project = projectService.updateProject(result);
        assertNotNull(project);
        verify(projectEventManager, times(1)).dispatchProjectUpdated(isA(ApplicationUser.class), isA(Project.class), isA(Project.class), eq(RequestSourceType.PAGE));
    }

    @Test
    public void testUpdateProjectRevertOnNotifyTypeChangeFailure() {
        final GenericValue mockProjectGV = new MockGenericValue("Project", FieldMap.build("key", "HSP", "name", "homosapien", "id", 10000L, "lead", admin.getUsername(), "projecttype", "original"));
        Project mockProject = new ProjectImpl(mockProjectGV);

        final GenericValue newMockProjectGv = new MockGenericValue(mockProjectGV);
        newMockProjectGv.put("projecttype", "another");
        Project changedProject = new ProjectImpl(newMockProjectGv);

        when(projectManager.updateProject(any(UpdateProjectParameters.class))).thenReturn(changedProject);
        when(projectTypeUpdatedNotifier.notifyAllHandlers(eq(admin), eq(mockProject), any(ProjectTypeKey.class), any(ProjectTypeKey.class))).thenReturn(false);
        ProjectService.UpdateProjectRequest request = new ProjectService.UpdateProjectRequest(mockProject);
        when(projectTypeValidator.isValid(eq(admin), any(ProjectTypeKey.class))).thenReturn(true);
        request.projectType("another");

        ProjectService.UpdateProjectValidationResult result = new ProjectService.UpdateProjectValidationResult(ErrorCollections.empty(), mockProject, admin, request);

        DefaultProjectService projectService = new MyProjectService();
        projectService.updateProject(result);
        verify(projectManager, times(2)).updateProject(isA(UpdateProjectParameters.class));
    }

    @Test
    public void testUpdateSchemesNullResult() {
        DefaultProjectService projectService = new MyProjectService();

        final Project mockProject = new ProjectImpl(null);
        try {
            projectService.updateProjectSchemes(null, mockProject);
            fail();
        } catch (IllegalArgumentException e) {
            //
        }
        verify(projectEventManager, times(0)).dispatchProjectUpdated(isA(ApplicationUser.class), isA(Project.class), isA(Project.class), any(RequestSourceType.class));
    }

    @Test
    public void testUpdateSchemesErrorResult() {
        DefaultProjectService projectService = new MyProjectService();

        final Project mockProject = new ProjectImpl(null);
        SimpleErrorCollection errorCollection = new SimpleErrorCollection();
        errorCollection.addError("field", "error");
        ProjectService.UpdateProjectSchemesValidationResult result = new ProjectService.UpdateProjectSchemesValidationResult(
                errorCollection);

        try {
            projectService.updateProjectSchemes(result, mockProject);
            fail();
        } catch (IllegalStateException e) {
            //
        }
        verify(projectEventManager, times(0)).dispatchProjectUpdated(isA(ApplicationUser.class), isA(Project.class), isA(Project.class), any(RequestSourceType.class));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testUpdateSchemesNullProject() {
        DefaultProjectService projectService = new MyProjectService();
        SimpleErrorCollection errorCollection = new SimpleErrorCollection();

        ProjectService.UpdateProjectSchemesValidationResult result = new ProjectService.UpdateProjectSchemesValidationResult(
                errorCollection);

        projectService.updateProjectSchemes(result, null);
    }

    @Test
    public void testUpdateSchemesSuccessEnterprise() throws Exception {
        final Project mockProject = new ProjectImpl(null);
        final Long schemeId = 1L;
        MockGenericValue permissionScheme = new MockGenericValue("permissionScheme", new HashMap());
        MockGenericValue notificationScheme = new MockGenericValue("notificationScheme", new HashMap());

        notificationSchemeManager.removeSchemesFromProject(mockProject);
        when(notificationSchemeManager.getScheme(schemeId)).thenReturn(notificationScheme);
        schemeFactory.getScheme(notificationScheme);
        final Scheme nScheme = new Scheme();
        notificationSchemeManager.addSchemeToProject(mockProject, nScheme);

        permissionSchemeManager.removeSchemesFromProject(mockProject);
        when(permissionSchemeManager.getScheme(schemeId)).thenReturn(permissionScheme);
        schemeFactory.getScheme(permissionScheme);
        final Scheme pScheme = new Scheme();
        permissionSchemeManager.addSchemeToProject(mockProject, pScheme);

        issueSecuritySchemeManager.setSchemeForProject(mockProject, schemeId);

        DefaultProjectService projectService = new MyProjectService();

        ErrorCollection errorCollection = new SimpleErrorCollection();
        ProjectService.UpdateProjectSchemesValidationResult schemesResult
                = new ProjectService.UpdateProjectSchemesValidationResult(errorCollection, schemeId, schemeId, schemeId);
        projectService.updateProjectSchemes(schemesResult, mockProject);
    }

    @Test
    public void testValidateDeleteProject() {
        final Project mockProject = new ProjectImpl(null);

        when(projectManager.getProjectObjByKey("HSP")).thenReturn(mockProject);

        DefaultProjectService projectService = new MyProjectService();

        final ProjectService.DeleteProjectValidationResult result = projectService.validateDeleteProject(admin, "HSP");

        assertTrue(result.isValid());
        assertFalse(result.getErrorCollection().hasAnyErrors());
        assertEquals(mockProject, result.getProject());
    }

    @Test
    public void testValidateDeleteProjectNoPermission() {
        when(globalPermissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, admin)).thenReturn(false);

        DefaultProjectService projectService = new MyProjectService();

        final ProjectService.DeleteProjectValidationResult result = projectService.validateDeleteProject(admin, "HSP");

        assertFalse(result.isValid());

        assertThat(result.getErrorCollection().getErrorMessages(), hasItem(NoopI18nHelper.makeTranslation("admin.projects.service.error.no.admin.permission")));
    }

    @Test
    public void testValidateDeleteProjectNoProject() {
        DefaultProjectService projectService = new MyProjectService() {
            public GetProjectResult getProjectByKeyForAction(final ApplicationUser user, final String key, ProjectAction projectAction) {
                ErrorCollection errors = new SimpleErrorCollection();
                errors.addErrorMessage("Error retrieving project.");
                return new GetProjectResult(errors);
            }
        };

        final ProjectService.DeleteProjectValidationResult result = projectService.validateDeleteProject(admin, "HSP");

        assertFalse(result.isValid());
        assertEquals("Error retrieving project.", result.getErrorCollection().getErrorMessages().iterator().next());
    }

    @Test
    public void testDeleteProjectNullResult() {
        DefaultProjectService projectService = new MyProjectService();

        try {
            projectService.deleteProject(admin, null);
            fail("Should have thrown exception");
        } catch (IllegalArgumentException e) {
            assertEquals("You can not delete a project with a null validation result.", e.getMessage());
        }
    }

    @Test
    public void testDeleteProjectInvalidResult() {
        ErrorCollection errors = new SimpleErrorCollection();
        errors.addErrorMessage("Something bad happend");
        ProjectService.DeleteProjectValidationResult result = new ProjectService.DeleteProjectValidationResult(errors, null);

        DefaultProjectService projectService = new MyProjectService();

        try {
            projectService.deleteProject(admin, result);
            fail("Should have thrown exception");
        } catch (IllegalStateException e) {
            assertEquals("You can not delete a project with an invalid validation result.", e.getMessage());
        }
    }

    @Test
    public void testDeleteProjectRemoveIssuesException() throws RemoveException {
        Project mockProject = new ProjectImpl(null);
        doThrow(new RemoveException("Error deleting issues")).when(projectManager).removeProjectIssues(eq(mockProject), any(Context.class));

        DefaultProjectService projectService = new MyProjectService();

        ProjectService.DeleteProjectValidationResult result =
                new ProjectService.DeleteProjectValidationResult(new SimpleErrorCollection(), mockProject);
        final ProjectService.DeleteProjectResult projectResult = projectService.deleteProject(admin, result);

        assertFalse(projectResult.isValid());
        assertThat(projectResult.getErrorCollection().getErrorMessages(), hasItem(NoopI18nHelper.makeTranslation("admin.errors.project.exception.removing", "Error deleting issues")));
        verify(projectEventManager, times(0)).dispatchProjectDeleted(isA(ApplicationUser.class), isA(Project.class));
    }

    @Test
    public void testDeleteProjectRemoveAssociationsThrowsException() throws RemoveException, GenericEntityException {
        final GenericValue mockProjectGV = new MockGenericValue("Project", FieldMap.build("key", "HSP", "name", "homosapien", "id", 10000L));
        Project mockProject = new ProjectImpl(mockProjectGV);

        projectManager.removeProjectIssues(mockProject);

        customFieldManager.removeProjectAssociations(mockProject);

        final IssueTypeScreenScheme issueTypeScreenScheme = Mockito.mock(IssueTypeScreenScheme.class);

        when(issueTypeScreenSchemeManager.getIssueTypeScreenScheme(mockProject)).thenReturn(issueTypeScreenScheme);
        issueTypeScreenSchemeManager.removeSchemeAssociation(mockProject, issueTypeScreenScheme);

        Mockito.doThrow(new DataAccessException("Error removing associations"))
                .when(nodeAssociationStore).removeAllAssociationsFromSource(Entity.Name.PROJECT, mockProject.getId());


        DefaultProjectService projectService = new MyProjectService();

        final ProjectService.DeleteProjectValidationResult result =
                new ProjectService.DeleteProjectValidationResult(new SimpleErrorCollection(), mockProject);
        final ProjectService.DeleteProjectResult projectResult = projectService.deleteProject(admin, result);

        assertFalse(projectResult.isValid());
        assertThat
                (
                        projectResult.getErrorCollection().getErrorMessages(),
                        hasItem
                                (
                                        NoopI18nHelper.makeTranslation
                                                (
                                                        "admin.errors.project.exception.removing",
                                                        "Error removing associations")
                                )
                );

        verify(projectEventManager, times(0)).dispatchProjectDeleted(isA(ApplicationUser.class), isA(Project.class));
    }

    @Test
    public void testDeleteProjectNoVersionsNorComponents()
            throws RemoveException, GenericEntityException, EntityNotFoundException {
        GenericValue mockProjectGV = new MockGenericValue("Project", ImmutableMap.of("key", "HSP", "name", "homosapien", "id", new Long(10000)));
        Project mockProject = new ProjectImpl(mockProjectGV);

        projectManager.removeProjectIssues(mockProject);

        customFieldManager.removeProjectAssociations(mockProject);

        IssueTypeScreenScheme mockIssueTypeScreenScheme = mock(IssueTypeScreenScheme.class);

        when(issueTypeScreenSchemeManager.getIssueTypeScreenScheme(mockProjectGV)).thenReturn(mockIssueTypeScreenScheme);
        issueTypeScreenSchemeManager.removeSchemeAssociation(mockProjectGV, mockIssueTypeScreenScheme);

        nodeAssociationStore.removeAssociationsFromSource(mockProjectGV);

        when(versionManager.getVersions(10000L)).thenReturn(Collections.emptyList());

        when(projectComponentManager.findAllForProject(10000L)).thenReturn(Collections.emptyList());


        projectManager.removeProject(mockProject);

        projectManager.refresh();

        when(workflowSchemeManager.getSchemeFor(mockProject)).thenReturn(null);
        when(workflowSchemeManager.cleanUpSchemeDraft(mockProject, admin)).thenReturn(null);
        workflowSchemeManager.clearWorkflowCache();


        DefaultProjectService projectService = new MyProjectService();

        ProjectService.DeleteProjectValidationResult result =
                new ProjectService.DeleteProjectValidationResult(new SimpleErrorCollection(), mockProject);
        final ProjectService.DeleteProjectResult projectResult = projectService.deleteProject(admin, result);

        assertTrue(projectResult.isValid());

        verify(sharePermissionDeleteUtils).deleteProjectSharePermissions(10000L);
        verify(projectEventManager, times(1)).dispatchProjectDeleted(isA(ApplicationUser.class), isA(Project.class));
    }

    @Test
    public void testDeleteProject() throws RemoveException, GenericEntityException, EntityNotFoundException {
        final ApplicationUser user = new MockApplicationUser("admin");

        final GenericValue mockProjectGV = new MockGenericValue("Project", ImmutableMap.of("key", "HSP", "name", "homosapien", "id", 10000L));
        Project mockProject = new ProjectImpl(mockProjectGV);

        final IssueTypeScreenScheme mockIssueTypeScreenScheme = Mockito.mock(IssueTypeScreenScheme.class);

        when(issueTypeScreenSchemeManager.getIssueTypeScreenScheme(mockProject)).thenReturn(mockIssueTypeScreenScheme);

        final Version mockVersion = new MockVersion(1000L, "the-version-to-delete");

        when(versionManager.getVersions(10000L)).thenReturn(ImmutableList.of(mockVersion));

        final ProjectComponent mockProjectComponent = new MockProjectComponent(12L, "the-component-to-delete");

        when(projectComponentManager.findAllForProject(10000L)).thenReturn(ImmutableList.of(mockProjectComponent));

        when(workflowSchemeManager.getSchemeFor(mockProject)).thenReturn(null);

        final List<JiraWorkflow> workflowsUsedByProject = Collections.emptyList();
        when(workflowManager.getWorkflowsFromScheme((Scheme) null)).thenReturn(workflowsUsedByProject);

        final DefaultProjectService projectService = new MyProjectService();

        ProjectService.DeleteProjectValidationResult result =
                new ProjectService.DeleteProjectValidationResult(new SimpleErrorCollection(), mockProject);
        final ProjectService.DeleteProjectResult projectResult = projectService.deleteProject(user, result);

        assertTrue(projectResult.isValid());

        Mockito.verify(projectManager).removeProjectIssues(eq(mockProject), any(Context.class));
        Mockito.verify(projectManager).removeProject(mockProject);
        Mockito.verify(projectManager).refresh();

        Mockito.verify(customFieldManager).removeProjectAssociations(mockProject);

        Mockito.verify(issueTypeScreenSchemeManager).removeSchemeAssociation(mockProject, mockIssueTypeScreenScheme);

        Mockito.verify(nodeAssociationStore).removeAllAssociationsFromSource(Entity.Name.PROJECT, mockProject.getId());

        Mockito.verify(versionManager).deleteAllVersions(mockProject.getId());
        Mockito.verify(projectComponentManager).deleteAllComponents(mockProject.getId());

        Mockito.verify(sharePermissionDeleteUtils).deleteProjectSharePermissions(10000L);
        Mockito.verify(workflowSchemeManager).clearWorkflowCache();
        Mockito.verify(workflowManager).copyAndDeleteDraftsForInactiveWorkflowsIn(user, workflowsUsedByProject);
        verify(projectEventManager, times(1)).dispatchProjectDeleted(isA(ApplicationUser.class), isA(Project.class));
    }

    private ProjectTypeValidator projectTypeValidatorMarkingAnyProjectTypeAsValid() {
        ProjectTypeValidator validator = mock(ProjectTypeValidator.class);
        when(validator.isValid(any(ApplicationUser.class), any(ProjectTypeKey.class))).thenReturn(true);
        return validator;
    }

    private ProjectCreateNotifier createProjectNotifier() {
        ProjectCreateNotifier notifier = mock(ProjectCreateNotifier.class);
        when(notifier.notifyAllHandlers(any(ProjectCreatedData.class))).thenReturn(true);
        return notifier;
    }

    /**
     * Inner project service class to get around some nasty calls to static methods in DefaultProjectService.
     */
    class MyProjectService extends DefaultProjectService {
        private boolean projectKeyValid = true;
        private boolean reserveKeyword = false;
        private boolean userExists = true;

        public MyProjectService() {
            super(TestDefaultProjectService.this.jiraAuthenticationContext,
                    TestDefaultProjectService.this.projectManager,
                    TestDefaultProjectService.this.applicationProperties,
                    TestDefaultProjectService.this.permissionManager,
                    globalPermissionManager, permissionSchemeManager,
                    notificationSchemeManager,
                    issueSecuritySchemeManager,
                    schemeFactory,
                    workflowSchemeManager,
                    issueTypeScreenSchemeManager,
                    customFieldManager,
                    nodeAssociationStore,
                    versionManager,
                    projectComponentManager,
                    sharePermissionDeleteUtils,
                    avatarManager,
                    TestDefaultProjectService.this.i18nFactory,
                    workflowManager,
                    TestDefaultProjectService.this.userManager,
                    TestDefaultProjectService.this.projectEventManager,
                    projectKeyStore,
                    projectTypeValidator,
                    createProjectNotifier(),
                    projectTypeUpdatedNotifier,
                    mock(ClusterLockService.class),
                    projectTemplateManager,
                    projectSchemeAssociationManager,
                    mock(TaskManager.class),
                    mock(IssueManager.class)
            );
        }

        public boolean isProjectKeyValid(final String key) {
            return projectKeyValid;
        }

        public MyProjectService setProjectKeyValid(final boolean projectKeyValid) {
            this.projectKeyValid = projectKeyValid;
            return this;
        }

        public boolean isReservedKeyword(final String key) {
            return reserveKeyword;
        }

        public MyProjectService setReserveKeyword(final boolean reserveKeyword) {
            this.reserveKeyword = reserveKeyword;
            return this;
        }

        public boolean checkUserExists(final String user) {
            return userExists;
        }

        public MyProjectService setUserExists(final boolean userExists) {
            this.userExists = userExists;
            return this;
        }

        @Override
        protected JiraServiceContext getServiceContext(final ApplicationUser user, final ErrorCollection errorCollection) {
            return new MockJiraServiceContext(user, errorCollection);
        }
    }
}
