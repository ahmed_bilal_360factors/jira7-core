package com.atlassian.jira.mock;

import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchProvider;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.issue.search.SearchResults;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.web.bean.PagerFilter;
import com.atlassian.query.Query;
import org.apache.lucene.search.Collector;

import java.util.Collections;
import java.util.List;

public class MockSearchProvider implements SearchProvider {
    List results = Collections.emptyList();

    public MockSearchProvider() {
    }

    @Override
    public SearchResults search(Query query, ApplicationUser searcher, PagerFilter pager) throws SearchException {
        return new SearchResults(results, new PagerFilter());
    }

    @Override
    public long searchCount(Query query, ApplicationUser searcher) throws SearchException {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public long searchCountOverrideSecurity(Query query, ApplicationUser searcher) throws SearchException {
        return 0;
    }

    @Override
    public void search(Query query, ApplicationUser searcher, Collector collector) throws SearchException {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public void search(Query query, ApplicationUser searcher, Collector collector, org.apache.lucene.search.Query andQuery)
            throws SearchException {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public void searchOverrideSecurity(Query query, ApplicationUser searcher, Collector collector)
            throws SearchException {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public void searchAndSort(Query query, ApplicationUser searcher, Collector collector, PagerFilter pager)
            throws SearchException {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public void searchAndSortOverrideSecurity(Query query, ApplicationUser searcher, Collector collector, PagerFilter pager)
            throws SearchException {
        throw new UnsupportedOperationException("Not implemented");
    }

    public long searchCountIgnorePermissions(SearchRequest request, ApplicationUser searchUser) throws SearchException {
        throw new UnsupportedOperationException();
    }

    public void setResults(List results) {
        this.results = results;
    }

    @Override
    public SearchResults search(Query query, ApplicationUser searcher, PagerFilter pager, org.apache.lucene.search.Query andQuery)
            throws SearchException {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public SearchResults searchOverrideSecurity(Query query, ApplicationUser searcher, PagerFilter pager, org.apache.lucene.search.Query andQuery)
            throws SearchException {
        throw new UnsupportedOperationException("Not implemented");
    }

}
