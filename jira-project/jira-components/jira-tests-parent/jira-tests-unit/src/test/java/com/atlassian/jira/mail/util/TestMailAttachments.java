package com.atlassian.jira.mail.util;

import com.atlassian.jira.issue.AttachmentManager;
import com.atlassian.jira.issue.attachment.Attachment;
import com.atlassian.jira.issue.thumbnail.ThumbnailManager;
import com.atlassian.jira.junit.rules.MockitoContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;

import javax.activation.DataHandler;
import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.internet.MimeBodyPart;
import java.io.IOException;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

public class TestMailAttachments {
    @Rule
    public MockitoContainer container = MockitoMocksInContainer.rule(this);

    @Mock
    private Attachment attachment;
    @Mock
    private AttachmentManager attachmentManager;
    @Mock
    private ThumbnailManager thumbnailManager;
    @Mock
    private DataHandler dataHandler;

    @Test
    public void shouldCreateMailAttachmentByStreamingAttachmentFromAttachmentManager() throws IOException, MessagingException {
        final MailAttachment streamedAttachment = MailAttachments.newMailAttachmentByStreamingFromAttachmentManager(attachment, attachmentManager);
        when(attachmentManager.streamAttachmentContent(eq(attachment), any())).thenReturn(dataHandler);

        assertBodyPartWasBuiltCorrectly(streamedAttachment.buildBodyPart());
    }

    @Test
    public void shouldCreateMailAttachmentByStreamingThumbnailFromThumbnailManager() throws IOException, MessagingException {
        final MailAttachment thumbnail = MailAttachments.newMailAttachmentByStreamingFromThumbnailManager(attachment, thumbnailManager);
        when(thumbnailManager.streamThumbnailContent(eq(attachment), any())).thenReturn(dataHandler);

        assertBodyPartWasBuiltCorrectly(thumbnail.buildBodyPart());
    }

    private void assertBodyPartWasBuiltCorrectly(BodyPart bodyPart) throws MessagingException {
        assertThat(bodyPart.getDataHandler(), is(dataHandler));
        assertThat(bodyPart.getDisposition(), is(MimeBodyPart.INLINE));
    }
}