package com.atlassian.jira.issue.fields.csv.custom;

import com.atlassian.jira.datetime.DateTimeFormatterFactoryStub;
import com.atlassian.jira.issue.customfields.impl.AbstractSingleFieldType;
import com.atlassian.jira.issue.customfields.impl.DateTimeCFType;
import org.junit.Test;

import java.util.Date;

public class DateTimeCFTypeCsvExportTest extends CustomFieldSingleValueCsvExporterTest<Date> {
    @Override
    protected AbstractSingleFieldType<Date> createField() {
        return new DateTimeCFType(null, null, new DateTimeFormatterFactoryStub(), null, null, null, csvDateFormatter);
    }

    @Test
    public void dateIsFormattedWithCsvDateFormatter() {
        whenFieldValueIs(now.toDate());
        assertExportedValue(csvDateFormatter.formatDateTime(now.toDate()));
    }
}
