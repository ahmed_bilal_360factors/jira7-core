package com.atlassian.jira.imports.project.util;

import com.atlassian.jira.util.TempDirectoryUtil;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Mock implementation of ProjectImportTemporaryFiles, which doesn't actually create
 *
 * @since v3.13
 */
public class MockProjectImportTemporaryFiles implements ProjectImportTemporaryFiles {
    private String projectKey;

    public MockProjectImportTemporaryFiles(String projectKey) {
        this.projectKey = projectKey;
    }

    public File getParentDirectory() {
        return new File(TempDirectoryUtil.getSystemTempDir(), "ProjectImport" + projectKey + "0000");
    }

    public File getEntityXmlFile(String entityName) {
        return new File(getParentDirectory(), entityName + ".xml");
    }

    @Override
    public PrintWriter getWriter(final String entity) throws IOException {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public String getEncoding() {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public void closeWriters() {
        throw new UnsupportedOperationException("Not implemented");
    }

    public void deleteTempFiles() {
        // nothing to do.
    }
}
