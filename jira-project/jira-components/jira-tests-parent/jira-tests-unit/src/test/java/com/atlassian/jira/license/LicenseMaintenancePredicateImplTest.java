package com.atlassian.jira.license;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.jira.application.MockApplication;
import com.atlassian.jira.application.MockApplicationManager;
import com.atlassian.jira.util.MockBuildUtilsInfo;
import com.google.common.collect.Sets;
import org.hamcrest.Matchers;
import org.junit.Test;

import java.time.Period;
import java.util.Date;
import java.util.Set;

import static org.hamcrest.MatcherAssert.assertThat;

public class LicenseMaintenancePredicateImplTest {
    @Test
    public void returnsTrueWhenApplicationsAndCoreValid() {
        //given
        final PredicateRunner runner = new PredicateRunner()
                .addApplicationInMaintenance("one")
                .addApplicationInMaintenance("two")
                .coreInMaintenance();

        //when
        final boolean result = runner.run();

        //then
        assertThat(result, Matchers.equalTo(true));
    }

    @Test
    public void returnsFalseWhenApplicationsAreValidAndCoreIsInvalid() {
        //given
        final PredicateRunner runner = new PredicateRunner()
                .addApplicationInMaintenance("one")
                .addApplicationInMaintenance("two")
                .coreOutOfMaintenance();

        //when
        final boolean result = runner.run();

        //then
        assertThat(result, Matchers.equalTo(false));
    }

    @Test
    public void returnsFalseWhenAnyApplicationIsInvalid() {
        //given
        final PredicateRunner runner = new PredicateRunner()
                .addApplicationInMaintenance("one")
                .addApplicationOutOfMaintenance("two")
                .coreInMaintenance();

        //when
        final boolean result = runner.run();

        //then
        assertThat(result, Matchers.equalTo(false));
    }

    @Test
    public void ignoresGhostApplicationWhenApplicationsAndCoreAreValid() {
        //given
        final PredicateRunner runner = new PredicateRunner()
                .addGhostApplication("one")
                .addApplicationInMaintenance("two")
                .coreInMaintenance();

        //when
        final boolean result = runner.run();

        //then
        assertThat(result, Matchers.equalTo(true));
    }

    @Test
    public void checksGhostApplicationAgainstCore() {
        //given
        final PredicateRunner runner = new PredicateRunner()
                .addGhostApplication("one")
                .coreOutOfMaintenance();

        //when
        final boolean result = runner.run();

        //then
        assertThat(result, Matchers.equalTo(false));
    }

    private static class PredicateRunner {
        private final Date today = new Date();
        private final Date tomorrow = Date.from(today.toInstant().plus(Period.ofDays(1)));
        private final Date yesterday = Date.from(today.toInstant().minus(Period.ofDays(1)));

        private Set<ApplicationKey> keys = Sets.newHashSet();
        private MockApplicationManager mockApplicationManager = new MockApplicationManager();
        private MockBuildUtilsInfo info = new MockBuildUtilsInfo().setBuildDate(yesterday);

        private PredicateRunner coreInMaintenance() {
            info.setBuildDate(yesterday);
            return this;
        }

        private PredicateRunner coreOutOfMaintenance() {
            info.setBuildDate(tomorrow);
            return this;
        }

        private PredicateRunner addApplicationInMaintenance(String key) {
            addApplication(key).buildDate(yesterday);
            return this;
        }

        private PredicateRunner addApplicationOutOfMaintenance(String key) {
            addApplication(key).buildDate(tomorrow);
            return this;
        }

        private PredicateRunner addGhostApplication(String key) {
            final ApplicationKey keyObject = ApplicationKey.valueOf(key);
            keys.add(keyObject);

            return this;
        }

        private MockApplication addApplication(String key) {
            final ApplicationKey keyObject = ApplicationKey.valueOf(key);
            keys.add(keyObject);
            return mockApplicationManager.addApplication(keyObject);
        }

        public boolean run() {
            final MockLicenseDetails details = new MockLicenseDetails()
                    .setLicensedApplications(new MockLicensedApplications(keys))
                    .setMaintenanceDate(today);

            final LicenseMaintenancePredicateImpl predicate
                    = new LicenseMaintenancePredicateImpl(mockApplicationManager, info);

            return predicate.test(details);
        }

    }
}