package com.atlassian.jira.upgrade;

import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestDelayedUpgradeService {

    @Rule
    public MethodRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private UpgradeScheduler upgradeScheduler;

    @Mock
    private ApplicationProperties applicationProperties;

    private DelayedUpgradeService delayedUpgradeService;

    @Before
    public void setUp() {
        delayedUpgradeService = new DelayedUpgradeService(upgradeScheduler, applicationProperties);
    }

    @Test
    public void shouldNotScheduleUpgradesWhenDisabled() {
        when(applicationProperties.getOption(eq(APKeys.JIRA_UPGRADE_FORCE_MANUAL_SCHEDULE))).thenReturn(true);

        final UpgradeResult upgradeResult = delayedUpgradeService.scheduleUpgrades();

        assertThat(upgradeResult, equalTo(UpgradeResult.OK));
        verify(upgradeScheduler, never()).scheduleUpgrades(anyInt());
    }

    @Test
    public void shouldScheduleUpgradesWithDefaultDelay() {
        when(upgradeScheduler.scheduleUpgrades(anyInt())).thenReturn(UpgradeResult.OK);

        final UpgradeResult upgradeResult = delayedUpgradeService.scheduleUpgrades();

        assertThat(upgradeResult, equalTo(UpgradeResult.OK));
        verify(upgradeScheduler).scheduleUpgrades(1);
    }
}