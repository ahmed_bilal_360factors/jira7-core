package com.atlassian.jira.plugin.jql.function;

import com.atlassian.jira.JiraDataTypes;
import com.atlassian.jira.jql.operand.QueryLiteral;
import com.atlassian.jira.jql.query.QueryCreationContext;
import com.atlassian.jira.jql.query.QueryCreationContextImpl;
import com.atlassian.jira.mock.plugin.jql.operand.MockJqlFunctionModuleDescriptor;
import com.atlassian.jira.permission.MockProjectPermission;
import com.atlassian.jira.permission.ProjectPermission;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.plugin.ProjectPermissionKey;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.util.UserUtil;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.MessageSet;
import com.atlassian.query.clause.TerminalClause;
import com.atlassian.query.operand.FunctionOperand;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Collections;
import java.util.List;

import static com.atlassian.jira.permission.ProjectPermissions.BROWSE_PROJECTS;
import static java.util.Locale.ENGLISH;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestProjectsWhereUserHasPermissionFunction {
    @Rule
    public MockitoRule mockito = MockitoJUnit.rule();

    @Mock
    private UserUtil userUtil;
    @Mock
    private PermissionManager permissionManager;
    @Mock
    private I18nHelper.BeanFactory i18nHelperFactory;
    @Mock
    private I18nHelper i18nHelper;

    private ApplicationUser theUser;
    private QueryCreationContext queryCreationContext;
    private TerminalClause terminalClause = null;
    private Project project1;
    private Project project2;
    private Project project3;
    private Project project4;
    private ImmutableList<Project> allProjects;

    @Before
    public void setUp() throws Exception {
        theUser = new MockApplicationUser("fred");
        queryCreationContext = new QueryCreationContextImpl(theUser);

        project1 = new MockProject(21l, "c1");
        project2 = new MockProject(22l, "c2");
        project3 = new MockProject(23l, "c3");
        project4 = new MockProject(24l, "c4");

        when(i18nHelperFactory.getInstance(ENGLISH)).thenReturn(i18nHelper);

        allProjects = ImmutableList.of(project1, project2, project3, project4);
    }

    @Test
    public void testDataType() throws Exception {

        ProjectsWhereUserHasPermissionFunction projectsWhereUserHasPermissionFunction = new ProjectsWhereUserHasPermissionFunction(permissionManager, userUtil, i18nHelperFactory);

        assertEquals(JiraDataTypes.PROJECT, projectsWhereUserHasPermissionFunction.getDataType());
    }

    @Test
    public void testValidateWrongArgs() throws Exception {
        ProjectsWhereUserHasPermissionFunction projectsWhereUserHasPermissionFunction = new ProjectsWhereUserHasPermissionFunction(permissionManager, userUtil, i18nHelperFactory);
        projectsWhereUserHasPermissionFunction.init(MockJqlFunctionModuleDescriptor.create("projectsWhereUserHasPermission", true));

        MessageSet messageSet = projectsWhereUserHasPermissionFunction.validate(new MockApplicationUser("bob"), new FunctionOperand("projectsWhereUserHasPermission"), terminalClause);
        assertTrue(messageSet.hasAnyErrors());
        assertEquals(1, messageSet.getErrorMessages().size());
        assertEquals("Function 'projectsWhereUserHasPermission' expected '1' arguments but received '0'.", messageSet.getErrorMessages().iterator().next());

        messageSet = projectsWhereUserHasPermissionFunction.validate(new MockApplicationUser("bob"), new FunctionOperand("projectsWhereUserHasPermission", "badArg1", "badArg2", "badArg3"), terminalClause);
        assertTrue(messageSet.hasAnyErrors());
        assertEquals(1, messageSet.getErrorMessages().size());
        assertEquals("Function 'projectsWhereUserHasPermission' expected '1' arguments but received '3'.", messageSet.getErrorMessages().iterator().next());

        when(permissionManager.getAllProjectPermissions()).thenReturn(Collections.<ProjectPermission>emptyList());

        messageSet = projectsWhereUserHasPermissionFunction.validate(new MockApplicationUser("bob"), new FunctionOperand("projectsWhereUserHasPermission", "BadPermission"), terminalClause);
        assertEquals(1, messageSet.getErrorMessages().size());
        assertEquals("Function 'projectsWhereUserHasPermission' can not generate a list of projects for permission 'BadPermission'; the permission does not exist.", messageSet.getErrorMessages().iterator().next());
    }

    @Test
    public void testValidateHappyPath() throws Exception {
        ProjectsWhereUserHasPermissionFunction projectsWhereUserHasPermissionFunction = new ProjectsWhereUserHasPermissionFunction(permissionManager, userUtil, i18nHelperFactory);
        projectsWhereUserHasPermissionFunction.init(MockJqlFunctionModuleDescriptor.create("projectsWhereUserHasPermission", true));

        when(permissionManager.getAllProjectPermissions()).thenReturn(Lists.<ProjectPermission>newArrayList(new MockProjectPermission("key", "nameKey", null, null)));
        when(i18nHelperFactory.getInstance(ENGLISH)).thenReturn(i18nHelper);
        when(i18nHelper.getText("nameKey")).thenReturn("View Development Tools");

        // No user name supplied
        MessageSet messageSet = projectsWhereUserHasPermissionFunction.validate(new MockApplicationUser("bob"), new FunctionOperand("projectsWhereUserHasPermission", "View Development Tools"), terminalClause);
        assertFalse(messageSet.hasAnyErrors());
    }

    @Test
    public void testValidateAnonymous() {
        ProjectsWhereUserHasPermissionFunction projectsWhereUserHasPermissionFunction = new ProjectsWhereUserHasPermissionFunction(permissionManager, userUtil, i18nHelperFactory);
        projectsWhereUserHasPermissionFunction.init(MockJqlFunctionModuleDescriptor.create("projectsWhereUserHasPermission", true));

        when(permissionManager.getAllProjectPermissions()).thenReturn(Lists.<ProjectPermission>newArrayList(new MockProjectPermission("key", "nameKey", null, null)));
        when(i18nHelperFactory.getInstance(ENGLISH)).thenReturn(i18nHelper);
        when(i18nHelper.getText("nameKey")).thenReturn("View Development Tools");

        final MessageSet messageSet = projectsWhereUserHasPermissionFunction.validate(null, new FunctionOperand("projectsWhereUserHasPermission", "View Development Tools"), terminalClause);
        assertTrue(messageSet.hasAnyErrors());
        assertEquals(1, messageSet.getErrorMessages().size());
        assertEquals("Function 'projectsWhereUserHasPermission' cannot be called as anonymous user.", messageSet.getErrorMessages().iterator().next());
    }

    @Test
    public void testGetValuesHappyPath() throws Exception {
        ProjectsWhereUserHasPermissionFunction projectsWhereUserHasPermissionFunction = new ProjectsWhereUserHasPermissionFunction(permissionManager, userUtil, i18nHelperFactory);
        projectsWhereUserHasPermissionFunction.init(MockJqlFunctionModuleDescriptor.create("projectsWhereUserHasPermission", true));

        when(permissionManager.getProjects(new ProjectPermissionKey("key"), theUser)).thenReturn(allProjects);
        when(permissionManager.getAllProjectPermissions()).thenReturn(Lists.<ProjectPermission>newArrayList(new MockProjectPermission("key", "nameKey", null, null)));
        when(i18nHelper.getText("nameKey")).thenReturn("View Development Tools");
        when(userUtil.getUserByName("fred")).thenReturn(theUser);

        setUpBrowsePermissionForProjects(allProjects);

        List<QueryLiteral> list = projectsWhereUserHasPermissionFunction.getValues(queryCreationContext, new FunctionOperand("projectsWhereUserHasPermission", "View Development Tools"), terminalClause);
        assertEquals(4, list.size());
        assertEquals(new Long(21), list.get(0).getLongValue());
        assertEquals(new Long(22), list.get(1).getLongValue());
        assertEquals(new Long(23), list.get(2).getLongValue());
        assertEquals(new Long(24), list.get(3).getLongValue());

        // No permissions on projects 22 & 23
        setUpBrowsePermissionForProjects(ImmutableList.of(project1, project4));

        list = projectsWhereUserHasPermissionFunction.getValues(queryCreationContext, new FunctionOperand("projectsWhereUserHasPermission", "View Development Tools"), terminalClause);
        assertEquals(2, list.size());
        assertEquals(new Long(21), list.get(0).getLongValue());
        assertEquals(new Long(24), list.get(1).getLongValue());
    }

    @Test
    public void testGetValuesAnonymous() {
        List<Project> projectsList2 = ImmutableList.of(project1, project2);

        ProjectsWhereUserHasPermissionFunction projectsWhereUserHasPermissionFunction = new ProjectsWhereUserHasPermissionFunction(permissionManager, userUtil, i18nHelperFactory);
        projectsWhereUserHasPermissionFunction.init(MockJqlFunctionModuleDescriptor.create("projectsWhereUserHasPermission", true));

        setUpBrowsePermissionForProjects(projectsList2);

        List<QueryLiteral> list = projectsWhereUserHasPermissionFunction.getValues(new QueryCreationContextImpl((ApplicationUser) null), new FunctionOperand("projectsWhereUserHasPermission", "View Development Tools"), terminalClause);
        assertTrue(list.isEmpty());

        setUpBrowsePermissionForProjects(ImmutableList.of());

        list = projectsWhereUserHasPermissionFunction.getValues(new QueryCreationContextImpl((ApplicationUser) null), new FunctionOperand("projectsWhereUserHasPermission", "View Development Tools"), terminalClause);
        assertTrue(list.isEmpty());
    }

    @Test
    public void testGetMinimumNumberOfExpectedArguments() throws Exception {
        ProjectsWhereUserHasPermissionFunction projectsWhereUserHasPermissionFunction = new ProjectsWhereUserHasPermissionFunction(permissionManager, userUtil, i18nHelperFactory);
        projectsWhereUserHasPermissionFunction.init(MockJqlFunctionModuleDescriptor.create("projectsWhereUserHasPermission", true));

        assertEquals(1, projectsWhereUserHasPermissionFunction.getMinimumNumberOfExpectedArguments());
    }

    private void setUpBrowsePermissionForProjects(List<Project> projects) {
        for (Project project : allProjects) {
            final boolean contains = projects.contains(project);
            when(permissionManager.hasPermission(BROWSE_PROJECTS, project, theUser)).thenReturn(contains);
        }
    }

}
