package com.atlassian.jira.plugin.util;

import com.atlassian.jira.plugin.language.LanguageModuleDescriptor;
import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.elements.ResourceDescriptor;
import com.google.common.collect.ImmutableList;
import org.junit.Test;

import java.util.function.Predicate;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


public class TestModuleDescriptorPredicates {

    @Test
    public void shouldReturnPredicateToTestPresenceOfModuleDescriptor() {
        final Plugin plugin = mock(Plugin.class);
        final LanguageModuleDescriptor languageModuleDescriptor = mock(LanguageModuleDescriptor.class);
        when(plugin.getModuleDescriptors()).thenReturn(ImmutableList.of(languageModuleDescriptor));

        final Predicate<Plugin> pluginWithModuleDescriptor =
                ModuleDescriptorPredicates.isPluginWithModuleDescriptor(LanguageModuleDescriptor.class);
        final boolean result = pluginWithModuleDescriptor.test(plugin);

        assertEquals(true, result);
    }

    @Test
    public void shouldReturnPredicateToTestAbsenceOfModuleDescriptor() {
        final Plugin plugin = mock(Plugin.class);
        final ModuleDescriptor moduleDescriptor = mock(ModuleDescriptor.class);
        when(plugin.getModuleDescriptors()).thenReturn(ImmutableList.of(moduleDescriptor));

        final Predicate<Plugin> pluginWithModuleDescriptor =
                ModuleDescriptorPredicates.isPluginWithModuleDescriptor(LanguageModuleDescriptor.class);
        final boolean result = pluginWithModuleDescriptor.test(plugin);

        assertEquals(false, result);
    }


    @Test
    public void shouldReturnPredicateToTestPresenceOfResourceTypeOnPlugin() {
        final Plugin plugin = mock(Plugin.class);

        final ResourceDescriptor resourceDescriptor = mock(ResourceDescriptor.class);
        when(resourceDescriptor.getType()).thenReturn("type");
        when(plugin.getResourceDescriptors()).thenReturn(ImmutableList.of(resourceDescriptor));


        final Predicate<Plugin> pluginWithResourceType = ModuleDescriptorPredicates.isPluginWithResourceType("type");
        final boolean result = pluginWithResourceType.test(plugin);

        assertEquals(true, result);

    }

    @Test
    public void shouldReturnPredicateToTestAbsenceOfResourceTypeOnPlugin() {
        final Plugin plugin = mock(Plugin.class);

        final ResourceDescriptor resourceDescriptor = mock(ResourceDescriptor.class);
        when(resourceDescriptor.getType()).thenReturn("not a type");
        when(plugin.getResourceDescriptors()).thenReturn(ImmutableList.of(resourceDescriptor));


        final Predicate<Plugin> pluginWithResourceType = ModuleDescriptorPredicates.isPluginWithResourceType("type");
        final boolean result = pluginWithResourceType.test(plugin);

        assertEquals(false, result);

    }

    @Test
    public void shouldReturnPredicateToTestPresenceOfResourceTypeOnModuleDescriptors() {
        final Plugin plugin = mock(Plugin.class);
        final ModuleDescriptor moduleDescriptor = mock(ModuleDescriptor.class);
        when(plugin.getModuleDescriptors()).thenReturn(ImmutableList.of(moduleDescriptor));
        final ResourceDescriptor resourceDescriptor = mock(ResourceDescriptor.class);
        when(resourceDescriptor.getType()).thenReturn("type");
        when(moduleDescriptor.getResourceDescriptors()).thenReturn(ImmutableList.of(resourceDescriptor));

        final Predicate<Plugin> pluginWithResourceType = ModuleDescriptorPredicates.isPluginWithResourceType("type");
        final boolean result = pluginWithResourceType.test(plugin);

        assertEquals(true, result);

    }
}