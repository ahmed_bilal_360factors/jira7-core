package com.atlassian.jira.issue.fields.csv;

import com.atlassian.jira.bc.ServiceOutcome;
import com.atlassian.jira.bc.issue.watcher.WatcherService;
import com.atlassian.jira.bc.issue.watcher.WatchingDisabledException;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.export.FieldExportParts;
import com.atlassian.jira.issue.fields.WatchesSystemField;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.lang.Pair;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static java.util.stream.Collectors.toList;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

public class WatchesSystemFieldCsvExportTest {
    @Rule
    public final RuleChain mockito = MockitoMocksInContainer.forTest(this);

    private static final String WATCHER1_USERNAME = "Watcher1 username";
    private static final String WATCHER2_USERNAME = "Watcher2 username";

    @Mock
    private ApplicationUser watcher1;

    @Mock
    private ApplicationUser watcher2;

    @Mock
    private Issue issue;

    @Mock
    @AvailableInContainer
    private WatcherService watcherService;

    @Mock
    private ServiceOutcome<Pair<Integer, List<ApplicationUser>>> serviceOutcome;


    @Mock
    private JiraAuthenticationContext authenticationContext;

    @Mock
    private I18nHelper i18nHelper;

    @InjectMocks
    private WatchesSystemField field;

    @Before
    public void setUp() {
        when(authenticationContext.getI18nHelper()).thenReturn(i18nHelper);
        when(authenticationContext.getLocale()).thenReturn(Locale.ENGLISH);

        when(watcher1.getName()).thenReturn(WATCHER1_USERNAME);
        when(watcher2.getName()).thenReturn(WATCHER2_USERNAME);
        when(watcherService.isWatchingEnabled()).thenReturn(true);
        when(watcherService.getWatchers(any(Issue.class), any(ApplicationUser.class))).thenReturn(serviceOutcome);
    }

    @Test
    public void testCsvRepresentationForExpectedTypeAndValues() {
        final List<ApplicationUser> watchers = new ArrayList<>(2);
        watchers.add(watcher1);
        watchers.add(watcher2);

        when(serviceOutcome.get()).thenReturn(Pair.of(2, watchers));

        final FieldExportParts representation = field.getRepresentationFromIssue(issue);

        assertThat(representation.getParts(), hasSize(1));
        assertThat(representation.getParts().get(0).getValues()::iterator, contains(WATCHER1_USERNAME, WATCHER2_USERNAME));
    }

    @Test
    public void testCsvRepresentationAreSortedValues() {
        final List<ApplicationUser> watchers = new ArrayList<>(2);
        watchers.add(watcher2);
        watchers.add(watcher1);

        when(serviceOutcome.get()).thenReturn(Pair.of(2, watchers));

        final FieldExportParts representation = field.getRepresentationFromIssue(issue);

        assertThat(representation.getParts(), hasSize(1));
        assertThat(representation.getParts().get(0).getValues()::iterator, contains(WATCHER1_USERNAME, WATCHER2_USERNAME));
    }

    @Test
    public void testCsvRepresentationWhenThereAreNoWatchers() {
        when(serviceOutcome.get()).thenReturn(Pair.of(0, new ArrayList<>()));

        final FieldExportParts representation = field.getRepresentationFromIssue(issue);

        assertThat(representation.getParts(), hasSize(1));
        assertThat(representation.getParts().get(0).getValues().collect(toList()), hasSize(0));
    }

    @Test
    public void testCsvRepresentationWhenWatchingIsDisabled() {
        when(watcherService.isWatchingEnabled()).thenReturn(false);
        when(watcherService.getWatchers(any(Issue.class), any(ApplicationUser.class))).thenThrow(new WatchingDisabledException());

        final FieldExportParts representation = field.getRepresentationFromIssue(issue);

        assertThat(representation.getParts(), hasSize(0));
    }
}
