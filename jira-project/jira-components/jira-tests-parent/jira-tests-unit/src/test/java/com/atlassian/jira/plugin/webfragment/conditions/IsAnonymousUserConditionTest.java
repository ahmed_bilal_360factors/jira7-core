package com.atlassian.jira.plugin.webfragment.conditions;

import com.atlassian.jira.security.JiraAuthenticationContext;
import org.junit.Test;

import java.util.Collections;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class IsAnonymousUserConditionTest {
    @Test
    public void shouldDisplayIfUserIsNotLoggedIn() {
        JiraAuthenticationContext context = mock(JiraAuthenticationContext.class);
        when(context.isLoggedInUser()).thenReturn(false);
        IsAnonymousUserCondition condition = new IsAnonymousUserCondition(context);

        assertTrue(condition.shouldDisplay(Collections.emptyMap()));
    }

    @Test
    public void shouldNotDisplayIfUserIsLoggedIn() {
        JiraAuthenticationContext context = mock(JiraAuthenticationContext.class);
        when(context.isLoggedInUser()).thenReturn(true);
        IsAnonymousUserCondition condition = new IsAnonymousUserCondition(context);

        assertFalse(condition.shouldDisplay(Collections.emptyMap()));
    }
}