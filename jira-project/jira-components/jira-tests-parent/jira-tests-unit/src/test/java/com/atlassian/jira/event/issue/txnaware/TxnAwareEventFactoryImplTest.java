package com.atlassian.jira.event.issue.txnaware;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.event.issue.IssueChangedEvent;
import com.atlassian.jira.event.issue.IssueChangedEventImpl;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.changehistory.ChangeHistory;
import com.atlassian.jira.issue.changehistory.ChangeHistoryManager;
import com.atlassian.jira.issue.comments.Comment;
import com.atlassian.jira.issue.history.ChangeItemBean;
import com.atlassian.jira.transaction.RequestLocalTransactionRunnableQueueFactory;
import com.atlassian.jira.transaction.RunnablesQueue;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.ComponentLocator;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.sql.Timestamp;
import java.util.Collection;

import static java.util.Optional.ofNullable;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TxnAwareEventFactoryImplTest {

    @Mock
    EventPublisher eventPublisher;

    @Mock
    RequestLocalTransactionRunnableQueueFactory requestLocalTransactionRunnableQueueFactory;

    @Mock
    RunnablesQueue runnablesQueue;

    @Mock
    ComponentLocator componentLocator;

    @Mock
    IssueManager issueManager;

    @Mock
    MutableIssue issue;

    @Mock
    ChangeHistory changeHistory;

    @Mock
    ChangeHistoryManager changeHistoryManager;

    TxnAwareEventFactory factory;

    @Before
    public void before() {
        when(requestLocalTransactionRunnableQueueFactory.getRunnablesQueue()).thenReturn(runnablesQueue);
        doAnswer(invocation -> {
            Runnable runnable = (Runnable) invocation.getArguments()[0];
            runnable.run();
            return null;
        }).when(runnablesQueue).offer(any());

        when(issueManager.getIssueObject(anyLong())).thenReturn(issue);

        when(changeHistoryManager.getChangeHistoryById(6L)).thenReturn(changeHistory);
        when(changeHistory.getChangeItemBeans()).thenReturn(Lists.newArrayList(mock(ChangeItemBean.class)));
        when(changeHistory.getTimePerformed()).thenReturn(new Timestamp(System.currentTimeMillis()));

        when(componentLocator.getComponent(IssueManager.class)).thenReturn(issueManager);
        when(componentLocator.getComponent(ChangeHistoryManager.class)).thenReturn(changeHistoryManager);

        factory = new TxnAwareEventFactoryImpl(eventPublisher, requestLocalTransactionRunnableQueueFactory, componentLocator);
    }

    @Test
    public void eventPublishedBecauseOfChanges() {
        prepareEventPublisherAssert(null, changeHistory.getChangeItemBeans(), null);
        factory.issueChangedEventOnCommit(5, null, null, 6L);
        verify(eventPublisher, times(1)).publish(any());
    }

    @Test
    public void eventPublishedCommentAndFieldChanges() {
        ApplicationUser user = mock(ApplicationUser.class);
        Comment comment = mock(Comment.class);
        prepareEventPublisherAssert(user, changeHistory.getChangeItemBeans(), comment);
        factory.issueChangedEventOnCommit(5, user, comment, 6L);
        verify(eventPublisher, times(1)).publish(any());
    }

    private void prepareEventPublisherAssert(ApplicationUser author, Collection<ChangeItemBean> changes, Comment comment) {
        IssueChangedEvent event = new IssueChangedEventImpl(issue, ofNullable(author), changes, ofNullable(comment), changeHistory.getTimePerformed());
        doAnswer(invocation -> {
            assertThat(invocation.getArguments()[0], is(event));
            return null;
        }).when(eventPublisher).publish(any());
    }
}
