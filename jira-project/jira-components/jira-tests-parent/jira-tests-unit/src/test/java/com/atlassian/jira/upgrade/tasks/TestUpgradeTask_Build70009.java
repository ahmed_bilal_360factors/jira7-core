package com.atlassian.jira.upgrade.tasks;

import com.atlassian.jira.entity.EntityEngine;
import com.atlassian.jira.entity.EntityEngineImpl;
import com.atlassian.jira.entity.Select;
import com.atlassian.jira.mock.ofbiz.MockOfBizDelegator;
import com.atlassian.jira.ofbiz.FieldMap;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.service.ServiceManager;
import com.atlassian.jira.service.services.DebugService;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import org.junit.Test;
import org.ofbiz.core.entity.GenericValue;

import java.util.Map;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;

/**
 * @since v7.0.0
 */
public class TestUpgradeTask_Build70009 {
    private static final String CFG_SERVICE_NAME = "name";
    private static final String CFG_DELAY_MILLIS = "time";
    private static final String CFG_CRON_EXPRESSION = "cronExpression";
    private static final String CFG_CLASS = "clazz";

    @Test
    public void testDoUpgrade() throws Exception {
        final MockOfBizDelegator delegator = new MockOfBizDelegator();
        final EntityEngine entityEngine = new EntityEngineImpl(delegator);
        final ServiceManager serviceManager = mock(ServiceManager.class);
        final UpgradeTask_Build70009 upgrade = new UpgradeTask_Build70009(entityEngine, serviceManager);

        createServiceConfig(delegator, "Service 1", "0 0/60 * * * ? *");
        createServiceConfig(delegator, "Service 2", "0 40 9 1W,15W 1-12 ? 2010-2050");
        createServiceConfig(delegator, "Service 3", "Fred");

        upgrade.doUpgrade(false);

        assertServiceCronExpressions(entityEngine, ImmutableMap.of(
                "Service 1", "0 0 * * * ? *",
                "Service 2", "0 40 9 1,15 1-12 ? 2010-2050",
                "Service 3", "Fred"));
    }

    private static Long createServiceConfig(final OfBizDelegator delegator, final String name, final String cronExpr) {
        final GenericValue gv = delegator.createValue("ServiceConfig", FieldMap.build(
                CFG_SERVICE_NAME, name,
                CFG_DELAY_MILLIS, TimeUnit.HOURS.toMicros(1L),
                CFG_CLASS, DebugService.class.getName(),
                CFG_CRON_EXPRESSION, cronExpr));
        return gv.getLong("id");
    }

    private static void assertServiceCronExpressions(final EntityEngine entityEngine, final Map<String, String> expectedConfigs) {
        final Map<String, String> actualConfigs = Maps.newHashMapWithExpectedSize(expectedConfigs.size());
        Select.columns(CFG_SERVICE_NAME, CFG_CRON_EXPRESSION)
                .from("ServiceConfig")
                .runWith(entityEngine)
                .visitWith(gv -> actualConfigs.put(gv.getString(CFG_SERVICE_NAME), gv.getString(CFG_CRON_EXPRESSION)));
        assertEquals(expectedConfigs, actualConfigs);
    }
}
