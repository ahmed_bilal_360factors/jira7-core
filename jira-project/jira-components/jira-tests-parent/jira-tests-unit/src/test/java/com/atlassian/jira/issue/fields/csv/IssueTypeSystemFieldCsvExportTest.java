package com.atlassian.jira.issue.fields.csv;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.export.FieldExportParts;
import com.atlassian.jira.issue.fields.IssueTypeSystemField;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.I18nHelper;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

/**
 * @since v6.5
 */
public class IssueTypeSystemFieldCsvExportTest {
    private static final String ISSUE_TYPE_NAME = "issue type name";

    @Rule
    public final RuleChain mockito = MockitoMocksInContainer.forTest(this);

    @Mock
    private IssueType issueType;

    @Mock
    private Issue issue;

    @Mock
    private JiraAuthenticationContext authenticationContext;

    @Mock
    private I18nHelper i18nHelper;


    @InjectMocks
    private IssueTypeSystemField field;

    @Before
    public void setUp() {
        when(authenticationContext.getI18nHelper()).thenReturn(i18nHelper);

        when(issueType.getNameTranslation()).thenReturn(ISSUE_TYPE_NAME);
    }

    @Test
    public void testCsvRepresentationForExpectedTypeAndValues() {
        when(issue.getIssueType()).thenReturn(issueType);

        final FieldExportParts representation = field.getRepresentationFromIssue(issue);
        assertThat(representation.getParts(), hasSize(1));
        assertThat(representation.getParts().get(0).getValues()::iterator, contains(ISSUE_TYPE_NAME));
    }
}
