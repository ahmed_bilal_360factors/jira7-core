package com.atlassian.jira.plugin.webresource;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.hostcontainer.HostContainer;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugin.webresource.WebResourceModuleDescriptor;
import com.atlassian.plugin.webresource.WebResourceTransformation;
import org.dom4j.DocumentHelper;
import org.junit.Test;
import org.mockito.Mockito;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;

public class TestJiraLookAndFeelWebResourceModuleDescriptor {

    @Test
    public void testAutomaticallyAddsTransformerAndContextToWebResource() throws Exception {
        ModuleFactory mf = Mockito.mock(ModuleFactory.class);
        HostContainer hc = Mockito.mock(HostContainer.class);

        WebResourceModuleDescriptor md1 = new JiraLookAndFeelWebResourceModuleDescriptor(mf, hc);
        ModuleDescriptor md2 = new JiraLookAndFeelWebResourceModuleDescriptor(mf, hc);

        md1.init(Mockito.mock(Plugin.class), DocumentHelper.createElement("lookandfeel-webresource").addAttribute("key","test"));
        md2.init(Mockito.mock(Plugin.class), DocumentHelper.createElement("lookandfeel-webresource").addAttribute("key","test2"));

        assertThat(md1.getContexts(), contains(equalTo("null:test"), equalTo("jira.global.look-and-feel")));
        assertThat(md1.getTransformations().size(), equalTo(1));

        WebResourceTransformation wrt = md1.getTransformations().get(0);
        assertThat(wrt.matches("less"), equalTo(true));
        assertThat(wrt.toString(), equalTo("WebResourceTransformation with [lessTransformer] transformers"));
    }
}
