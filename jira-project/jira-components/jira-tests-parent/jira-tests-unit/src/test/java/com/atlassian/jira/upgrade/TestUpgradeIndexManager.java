package com.atlassian.jira.upgrade;

import com.atlassian.jira.config.ReindexMessageManager;
import com.atlassian.jira.index.request.ReindexRequest;
import com.atlassian.jira.index.request.ReindexRequestManager;
import com.atlassian.jira.index.request.ReindexRequestType;
import com.atlassian.jira.index.request.ReindexRequestTypes;
import com.atlassian.jira.index.request.ReindexStatus;
import com.google.common.collect.Sets;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import java.util.Collections;
import java.util.EnumSet;
import java.util.stream.Collectors;

import static com.atlassian.jira.index.request.ReindexRequestTypes.allAllowed;
import static com.atlassian.jira.index.request.ReindexRequestTypes.onlyImmediate;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestUpgradeIndexManager {

    @Rule
    public MethodRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private ReindexRequestManager reindexRequestManager;

    @Mock
    private ReindexMessageManager reindexMessageManager;

    private UpgradeIndexManager upgradeIndexManager;

    @Before
    public void setUp() {
        upgradeIndexManager = new UpgradeIndexManager(reindexRequestManager, reindexMessageManager);
    }

    @Test
    public void shouldNotRequestReindexIfNotAllowed() {

        when(reindexRequestManager.isReindexRequested(onlyImmediate())).thenReturn(false);

        final boolean indexingResult = upgradeIndexManager.runReindexIfNeededAndAllowed(ReindexRequestTypes.noneAllowed());

        assertThat(indexingResult, equalTo(true));
        verify(reindexMessageManager, never()).pushRawMessage(eq(null), eq("admin.upgrade.reindex.deferred"));
        verify(reindexRequestManager, times(1)).isReindexRequested(onlyImmediate());
        verify(reindexRequestManager, never()).processPendingRequests(anyBoolean(), any(), anyBoolean());
    }

    @Test
    public void shouldNotRequestReindexWhenAnotherReindexIsPending() {

        when(reindexRequestManager.isReindexRequested(onlyImmediate())).thenReturn(true);

        final boolean indexingResult = upgradeIndexManager.runReindexIfNeededAndAllowed(ReindexRequestTypes.noneAllowed());

        assertThat(indexingResult, equalTo(true));
        verify(reindexMessageManager, times(1)).pushRawMessage(eq(null), eq("admin.upgrade.reindex.deferred"));
        verify(reindexRequestManager, times(1)).isReindexRequested(onlyImmediate());
        verify(reindexRequestManager, never()).processPendingRequests(anyBoolean(), any(), anyBoolean());
    }

    @Test
    public void shouldRequestReindex() {

        when(reindexRequestManager.isReindexRequested(onlyImmediate())).thenReturn(true);
        final ReindexRequest reindexRequestSucceeded = new ReindexRequest(null, ReindexRequestType.IMMEDIATE, 0L, null, null, null, ReindexStatus.COMPLETE, Collections.emptySet(), Collections.emptySet());
        when(reindexRequestManager.processPendingRequests(anyBoolean(), any(), anyBoolean())).thenReturn(Sets.newHashSet(reindexRequestSucceeded));

        final boolean indexingResult = upgradeIndexManager.runReindexIfNeededAndAllowed(onlyImmediate());

        assertThat(indexingResult, equalTo(true));
        verify(reindexRequestManager, times(1)).processPendingRequests(anyBoolean(), eq(onlyImmediate()), anyBoolean());
    }

    @Test
    public void shouldRequestReindexForAllTypes() {

        when(reindexRequestManager.isReindexRequested(allAllowed())).thenReturn(true);

        final boolean indexingResult = upgradeIndexManager.runReindexIfNeededAndAllowed(allAllowed());

        assertThat(indexingResult, equalTo(true));
        verify(reindexRequestManager, times(1)).isReindexRequested(eq(allAllowed()));
        verify(reindexRequestManager, times(1)).processPendingRequests(anyBoolean(), eq(allAllowed()), anyBoolean());
    }

    @Test
    public void shouldFailWhenReindexFailed() {

        when(reindexRequestManager.isReindexRequested(onlyImmediate())).thenReturn(true);
        final ReindexRequest reindexRequestFailed = new ReindexRequest(null, ReindexRequestType.IMMEDIATE, 0L, null, null, null, ReindexStatus.FAILED, Collections.emptySet(), Collections.emptySet());
        when(reindexRequestManager.processPendingRequests(anyBoolean(), any(), anyBoolean())).thenReturn(Sets.newHashSet(reindexRequestFailed));

        final boolean indexingResult = upgradeIndexManager.runReindexIfNeededAndAllowed(onlyImmediate());

        assertThat(indexingResult, equalTo(false));
        verify(reindexRequestManager, times(1)).processPendingRequests(anyBoolean(), eq(onlyImmediate()), anyBoolean());
    }

    @Test
    public void shouldFailWhenForegroundReindexFailedForAllTypes() {
        when(reindexRequestManager.isReindexRequested(allAllowed())).thenReturn(true);
        when(reindexRequestManager.isReindexRequested(onlyImmediate())).thenReturn(true);
        final ReindexRequest reindexRequestFailed = new ReindexRequest(null, ReindexRequestType.IMMEDIATE, 0L, null, null, null, ReindexStatus.FAILED, Collections.emptySet(), Collections.emptySet());
        when(reindexRequestManager.processPendingRequests(anyBoolean(), any(), anyBoolean())).thenReturn(Sets.newHashSet(reindexRequestFailed));

        final boolean indexingResult = upgradeIndexManager.runReindexIfNeededAndAllowed(allAllowed());

        assertThat(indexingResult, equalTo(false));
        verify(reindexRequestManager, times(1)).processPendingRequests(anyBoolean(), eq(allAllowed()), anyBoolean());
    }

    @Test
    public void shouldNotReindexWhenNoReindexRequests() {

        when(reindexRequestManager.isReindexRequested(allAllowed())).thenReturn(false);

        final boolean indexingResult = upgradeIndexManager.runReindexIfNeededAndAllowed(allAllowed());

        assertThat(indexingResult, equalTo(true));
        verify(reindexMessageManager, never()).pushRawMessage(eq(null), eq("admin.upgrade.reindex.deferred"));
        verify(reindexRequestManager, times(1)).isReindexRequested(allAllowed());
        verify(reindexRequestManager, never()).processPendingRequests(anyBoolean(), any(), anyBoolean());
    }

    @Test
    public void shouldRunReindexInBackgroundIfImmediateReindexNotAllowed() {

        final EnumSet<ReindexRequestType> notImmediateReindexRequestTypes = EnumSet.copyOf(allAllowed().stream()
                .filter(reindexRequestType -> !reindexRequestType.equals(ReindexRequestType.IMMEDIATE))
                .collect(Collectors.toList()));

        when(reindexRequestManager.isReindexRequested(notImmediateReindexRequestTypes)).thenReturn(true);

        final boolean indexingResult = upgradeIndexManager.runReindexIfNeededAndAllowed(notImmediateReindexRequestTypes);

        assertThat(indexingResult, equalTo(true));
        verify(reindexRequestManager, times(1)).isReindexRequested(notImmediateReindexRequestTypes);
        verify(reindexRequestManager, times(1)).processPendingRequests(false, notImmediateReindexRequestTypes, true);
    }

    @Test
    public void shouldRunReindexInBackgroundIfImmediateReindexAllowedButNotRequested() {

        when(reindexRequestManager.isReindexRequested(allAllowed())).thenReturn(true);
        when(reindexRequestManager.isReindexRequested(onlyImmediate())).thenReturn(false);

        final boolean indexingResult = upgradeIndexManager.runReindexIfNeededAndAllowed(allAllowed());

        assertThat(indexingResult, equalTo(true));
        verify(reindexRequestManager, times(1)).processPendingRequests(false, allAllowed(), true);
    }

    @Test
    public void shouldRunReindexInForegoundIfImmediateReindexAllowedAndRequested() {

        when(reindexRequestManager.isReindexRequested(allAllowed())).thenReturn(true);
        when(reindexRequestManager.isReindexRequested(onlyImmediate())).thenReturn(true);

        final boolean indexingResult = upgradeIndexManager.runReindexIfNeededAndAllowed(allAllowed());

        assertThat(indexingResult, equalTo(true));
        verify(reindexRequestManager, times(1)).processPendingRequests(true, allAllowed(), false);
    }

}