package com.atlassian.jira.ofbiz;

import com.google.common.collect.Lists;
import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.Mockito;
import org.ofbiz.core.entity.jdbc.interceptors.connection.ConnectionPoolState;
import org.ofbiz.core.entity.jdbc.interceptors.connection.SQLConnectionInterceptor;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;

/**
 * @since v4.0
 */
public class ChainedSQLInterceptorTest {
    @Test
    public void testChaining() {
        CountingSqlInterceptor interceptor1 = new CountingSqlInterceptor();
        CountingSqlInterceptor interceptor2 = new CountingSqlInterceptor();

        ChainedSQLInterceptor.Builder builder = new ChainedSQLInterceptor.Builder();

        final ChainedSQLInterceptor chainedInterceptor = builder.add(interceptor1).add(interceptor2).build();
        assertNotNull(chainedInterceptor);

        chainedInterceptor.beforeExecution(null, null, null);
        chainedInterceptor.beforeExecution(null, null, null);
        chainedInterceptor.beforeExecution(null, null, null);

        chainedInterceptor.afterSuccessfulExecution(null, null, null, null, 0);
        chainedInterceptor.afterSuccessfulExecution(null, null, null, null, 0);

        chainedInterceptor.onException(null, null, null, null);

        assertEquals(3, interceptor1.beforeExecutionCallCount.get());
        assertEquals(2, interceptor1.afterSuccessfulExecutionCallCount.get());
        assertEquals(1, interceptor1.onExceptionCallCount.get());

        assertEquals(3, interceptor2.beforeExecutionCallCount.get());
        assertEquals(2, interceptor2.afterSuccessfulExecutionCallCount.get());
        assertEquals(1, interceptor2.onExceptionCallCount.get());
    }

    @Test
    public void testCallOrdering() {
        final ChainedSQLInterceptor.Builder builder = new ChainedSQLInterceptor.Builder();
        LinkedList<SQLConnectionInterceptor> interceptors = Lists.newLinkedList();

        for (int i = 0; i < 3; i++) {
            final SQLConnectionInterceptor interceptor = mock(SQLConnectionInterceptor.class);
            interceptors.add(interceptor);
            builder.add(interceptor);
        }

        InOrder inOrder = Mockito.inOrder(interceptors.toArray());

        final ChainedSQLInterceptor chainedInterceptor = builder.build();
        chainedInterceptor.beforeExecution(null, null, null);
        chainedInterceptor.afterSuccessfulExecution(null, null, null, null, 0);
        chainedInterceptor.onException(null, null, null, null);

        interceptors.forEach(i -> inOrder.verify(i).beforeExecution(null, null, null));
        interceptors.descendingIterator().forEachRemaining(i -> inOrder.verify(i).afterSuccessfulExecution(null, null, null, null, 0));
        interceptors.descendingIterator().forEachRemaining(i -> inOrder.verify(i).onException(null, null, null, null));
    }


    private static class CountingSqlInterceptor implements SQLConnectionInterceptor {
        final AtomicInteger beforeExecutionCallCount = new AtomicInteger();
        final AtomicInteger afterSuccessfulExecutionCallCount = new AtomicInteger();
        final AtomicInteger onExceptionCallCount = new AtomicInteger();

        @Override
        public void onConnectionTaken(Connection connection, ConnectionPoolState connectionPoolState) {
        }

        @Override
        public void onConnectionReplaced(Connection connection, ConnectionPoolState connectionPoolState) {
        }

        public void beforeExecution(final String sqlString, final List<String> parameterValues, final Statement statement) {
            beforeExecutionCallCount.incrementAndGet();
        }

        public void afterSuccessfulExecution(final String sqlString, final List<String> parameterValues, final Statement statement, final ResultSet resultSet, final int rowsUpdated) {
            afterSuccessfulExecutionCallCount.incrementAndGet();
        }

        public void onException(final String sqlString, final List<String> parameterValues, final Statement statement, final SQLException sqlException) {
            onExceptionCallCount.incrementAndGet();
        }
    }
}
