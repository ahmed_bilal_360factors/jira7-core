package com.atlassian.jira.auditing.handlers;

import com.atlassian.jira.auditing.AuditingCategory;
import com.atlassian.jira.auditing.RecordRequest;
import com.atlassian.jira.cluster.zdu.JiraUpgradeApprovedEvent;
import com.atlassian.jira.cluster.zdu.JiraUpgradeCancelledEvent;
import com.atlassian.jira.cluster.zdu.JiraUpgradeFinishedEvent;
import com.atlassian.jira.cluster.zdu.JiraUpgradeStartedEvent;
import com.atlassian.jira.cluster.zdu.NodeBuildInfo;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.mock.cluster.zdu.MockNodeBuildInfo;
import com.atlassian.jira.mock.i18n.MockI18nHelper;
import com.atlassian.jira.util.I18nHelper;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Locale;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestJiraUpgradeEventHandlerImpl {
    private static final MockNodeBuildInfo NODE_BUILD_INFO_FROM = new MockNodeBuildInfo(12345L, "1.2.3");
    private static final NodeBuildInfo NODE_BUILD_INFO_TO = new MockNodeBuildInfo(12345L, "1.2.4");

    @Rule
    public RuleChain mockitoMocksInContainer = MockitoMocksInContainer.forTest(this);

    @Mock
    @AvailableInContainer
    private MockI18nHelper.BeanFactory beanFactory;

    @Mock
    private I18nHelper i18nHelper;

    private JiraUpgradeEventHandlerImpl handler;

    @Before
    public void setUp() {
        handler = new JiraUpgradeEventHandlerImpl(beanFactory);
        when(beanFactory.getInstance(any(Locale.class))).thenReturn(i18nHelper);
        when(i18nHelper.getText("admin.systeminfo.version")).thenReturn("version");
        when(i18nHelper.getText("admin.systeminfo.build.number")).thenReturn("build number");
    }

    @Test
    @SuppressWarnings("unchecked")
    public void startedEvent() {
        when(i18nHelper.getText("jira.auditing.upgrade.started")).thenReturn("upgrade started");
        when(i18nHelper.getText("jira.auditing.upgrade.started.description")).thenReturn("upgrade started description");

        RecordRequest recordRequest = handler.onStartedEvent(new JiraUpgradeStartedEvent(NODE_BUILD_INFO_FROM));

        verifyCategory(recordRequest);
        verifySummary(recordRequest, "upgrade started");
        verifyDescription(recordRequest, "upgrade started description");
        assertThat(recordRequest.getChangedValues(), contains(
                allOf(
                        hasProperty("name", is("version")),
                        hasProperty("from", is(nullValue())),
                        hasProperty("to", is("1.2.3"))
                ),
                allOf(
                        hasProperty("name", is("build number")),
                        hasProperty("from", is(nullValue())),
                        hasProperty("to", is("12345"))
                )
        ));
    }

    @Test
    @SuppressWarnings("unchecked")
    public void cancelledEvent() {
        when(i18nHelper.getText("jira.auditing.upgrade.cancelled")).thenReturn("upgrade cancelled");
        when(i18nHelper.getText("jira.auditing.upgrade.cancelled.description")).thenReturn("upgrade cancelled description");

        RecordRequest recordRequest = handler.onCancelledEvent(new JiraUpgradeCancelledEvent(NODE_BUILD_INFO_FROM));

        verifyCategory(recordRequest);
        verifySummary(recordRequest, "upgrade cancelled");
        verifyDescription(recordRequest, "upgrade cancelled description");
        assertThat(recordRequest.getChangedValues(), contains(
                allOf(
                        hasProperty("name", is("version")),
                        hasProperty("from", is(nullValue())),
                        hasProperty("to", is("1.2.3"))
                ),
                allOf(
                        hasProperty("name", is("build number")),
                        hasProperty("from", is(nullValue())),
                        hasProperty("to", is("12345"))
                )
        ));
    }

    @Test
    @SuppressWarnings("unchecked")
    public void approvedEvent() {
        when(i18nHelper.getText("jira.auditing.upgrade.approved")).thenReturn("upgrade approved");
        when(i18nHelper.getText("jira.auditing.upgrade.approved.description")).thenReturn("upgrade approved description");

        RecordRequest recordRequest = handler.onApprovedEvent(new JiraUpgradeApprovedEvent(NODE_BUILD_INFO_FROM, NODE_BUILD_INFO_TO));

        verifyCategory(recordRequest);
        verifySummary(recordRequest, "upgrade approved");
        verifyDescription(recordRequest, "upgrade approved description");
        assertThat(recordRequest.getChangedValues(), contains(
                allOf(
                        hasProperty("name", is("version")),
                        hasProperty("from", is("1.2.3")),
                        hasProperty("to", is("1.2.4"))
                )
        ));
    }

    @Test
    public void finishedEvent() {
        when(i18nHelper.getText("jira.auditing.upgrade.finished")).thenReturn("upgrade finished");
        when(i18nHelper.getText("jira.auditing.upgrade.finished.description")).thenReturn("upgrade finished description");

        RecordRequest recordRequest = handler.onFinishedEvent(new JiraUpgradeFinishedEvent(NODE_BUILD_INFO_FROM, NODE_BUILD_INFO_TO));

        verifyCategory(recordRequest);
        verifySummary(recordRequest, "upgrade finished");
        verifyDescription(recordRequest, "upgrade finished description");
        assertThat(recordRequest.getChangedValues(), contains(
                allOf(
                        hasProperty("name", is("version")),
                        hasProperty("from", is("1.2.3")),
                        hasProperty("to", is("1.2.4"))
                )
        ));
    }

    private void verifySummary(final RecordRequest recordRequest, final String summary) {
        assertThat(recordRequest.getSummary(), is(summary));
    }

    private void verifyCategory(final RecordRequest recordRequest) {
        assertThat(recordRequest.getCategory(), is(AuditingCategory.SYSTEM));
    }

    private void verifyDescription(final RecordRequest recordRequest, final String description) {
        assertThat(recordRequest.getDescription(), is(description));
    }
}
