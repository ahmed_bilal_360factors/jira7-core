package com.atlassian.jira.bc.project;

import com.atlassian.beehive.ClusterLockService;
import com.atlassian.jira.association.NodeAssociationStore;
import com.atlassian.jira.avatar.AvatarManager;
import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.bc.project.component.ProjectComponentManager;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.event.project.ProjectEventManager;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.fields.screen.issuetype.IssueTypeScreenSchemeManager;
import com.atlassian.jira.issue.security.IssueSecuritySchemeManager;
import com.atlassian.jira.notification.NotificationSchemeManager;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.permission.PermissionSchemeManager;
import com.atlassian.jira.project.ProjectCreateNotifier;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.project.template.ProjectTemplate;
import com.atlassian.jira.project.template.ProjectTemplateKey;
import com.atlassian.jira.project.template.ProjectTemplateManager;
import com.atlassian.jira.project.type.ProjectTypeKey;
import com.atlassian.jira.project.type.ProjectTypeUpdatedNotifier;
import com.atlassian.jira.project.util.ProjectKeyStore;
import com.atlassian.jira.project.version.VersionManager;
import com.atlassian.jira.scheme.SchemeFactory;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.sharing.SharePermissionDeleteUtils;
import com.atlassian.jira.task.TaskManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.jira.workflow.WorkflowManager;
import com.atlassian.jira.workflow.WorkflowSchemeManager;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Optional;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Tests the validation of the parameters that occurs before a project is created
 */
@RunWith(MockitoJUnitRunner.class)
public class TestDefaultProjectServiceValidateCreateProject {
    private static final Long ASSIGNEE_TYPE = 1L;
    private static final Long AVATAR_ID = 2L;

    @Mock
    private I18nHelper.BeanFactory i18nFactory;
    @Mock
    private GlobalPermissionManager globalPermissionManager;
    @Mock
    private ProjectTemplateManager projectTemplateManager;
    @Mock
    private ApplicationUser user;

    private ProjectCreationValidator validator;

    @Before
    public void setUp() {
        mockI18N();

        validator = new ProjectCreationValidator(globalPermissionManager, i18nFactory, projectTemplateManager);
        validator.setServiceContext(contextWith(new SimpleErrorCollection()));
    }

    @Test
    public void returnsAForbiddenErrorIfTheUserIsNotAnAdmin() {
        when(globalPermissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, user)).thenReturn(false);

        ProjectService.CreateProjectValidationResult result = validator.validateCreateProject(
                user,
                new ProjectCreationData.Builder().build()
        );

        assertTrue(result.getErrorCollection().getReasons().contains(ErrorCollection.Reason.FORBIDDEN));
    }

    @Test
    public void returnsAnErrorIfNotAllOfTheProjectCreationDataIsValid() {
        when(globalPermissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, user)).thenReturn(true);
        when(projectTemplateManager.getProjectTemplate(null)).thenReturn(Optional.empty());

        validator.projectDataIsNotValid();
        ProjectService.CreateProjectValidationResult result = validator.validateCreateProject(
                user,
                new ProjectCreationData.Builder().build()
        );

        assertTrue(result.getErrorCollection().hasAnyErrors());
    }

    @Test
    public void returnsAValidationResultWithoutErrorsIfAllOfTheProjectCreationDataIsValid() {
        when(globalPermissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, user)).thenReturn(true);

        validator.projectDataIsValid();
        ProjectService.CreateProjectValidationResult result = validator.validateCreateProject(
                user,
                new ProjectCreationData.Builder()
                        .withName("name")
                        .withKey("key")
                        .withDescription("description")
                        .withType("projectType")
                        .withLead(new MockApplicationUser("leadName"))
                        .withUrl("url")
                        .withAssigneeType(ASSIGNEE_TYPE)
                        .withAvatarId(AVATAR_ID)
                        .build()
        );

        ProjectCreationData projectCreationData = result.getProjectCreationData();
        assertFalse(result.getErrorCollection().hasAnyErrors());
        assertThat(projectCreationData.getName(), is("name"));
        assertThat(projectCreationData.getKey(), is("key"));
        assertThat(projectCreationData.getDescription(), is("description"));
        assertThat(projectCreationData.getUrl(), is("url"));
        assertThat(projectCreationData.getAssigneeType(), is(1L));
        assertThat(projectCreationData.getAvatarId(), is(2L));
        assertThat(projectCreationData.getProjectTypeKey(), is(new ProjectTypeKey("projectType")));
    }

    @Test
    public void usesTheProjectTypeSpecifiedOnTheProjectCreationDataIfTheSpecifiedProjectTemplateDoesNotExist() {
        when(globalPermissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, user)).thenReturn(true);
        validator.projectDataIsValid();

        when(projectTemplateManager.getProjectTemplate(new ProjectTemplateKey("projectTemplateKey"))).thenReturn(Optional.empty());

        ProjectService.CreateProjectValidationResult result = validator.validateCreateProject(
                user,
                new ProjectCreationData.Builder()
                        .withType("projectType")
                        .withProjectTemplateKey("projectTemplateKey")
                        .build()
        );

        ProjectCreationData projectCreationData = result.getProjectCreationData();
        assertThat(projectCreationData.getProjectTypeKey().getKey(), is("projectType"));
    }

    private void mockI18N() {
        I18nHelper i18nHelper = mock(I18nHelper.class);
        when(i18nFactory.getInstance(any(ApplicationUser.class))).thenReturn(i18nHelper);
    }

    private JiraServiceContext contextWith(final SimpleErrorCollection errorCollection) {
        JiraServiceContext serviceContext = mock(JiraServiceContext.class);
        when(serviceContext.getErrorCollection()).thenReturn(errorCollection);
        return serviceContext;
    }

    private ProjectTemplate templateWithProjectType(String projectType) {
        ProjectTemplate projectTemplate = mock(ProjectTemplate.class);
        when(projectTemplate.getProjectTypeKey()).thenReturn(new ProjectTypeKey(projectType));
        return projectTemplate;
    }

    private static class ProjectCreationValidator extends DefaultProjectService {
        private boolean isProjectDataValid;
        private JiraServiceContext serviceContext;

        public ProjectCreationValidator(
                GlobalPermissionManager globalPermissionManager,
                I18nHelper.BeanFactory i18nFactory,
                ProjectTemplateManager projectTemplateManager) {
            super(
                    mock(JiraAuthenticationContext.class),
                    mock(ProjectManager.class),
                    mock(ApplicationProperties.class),
                    mock(PermissionManager.class),
                    globalPermissionManager,
                    mock(PermissionSchemeManager.class),
                    mock(NotificationSchemeManager.class),
                    mock(IssueSecuritySchemeManager.class),
                    mock(SchemeFactory.class),
                    mock(WorkflowSchemeManager.class),
                    mock(IssueTypeScreenSchemeManager.class),
                    mock(CustomFieldManager.class),
                    mock(NodeAssociationStore.class),
                    mock(VersionManager.class),
                    mock(ProjectComponentManager.class),
                    mock(SharePermissionDeleteUtils.class),
                    mock(AvatarManager.class),
                    i18nFactory,
                    mock(WorkflowManager.class),
                    mock(UserManager.class),
                    mock(ProjectEventManager.class),
                    mock(ProjectKeyStore.class),
                    mock(ProjectTypeValidator.class),
                    mock(ProjectCreateNotifier.class),
                    mock(ProjectTypeUpdatedNotifier.class),
                    mock(ClusterLockService.class),
                    projectTemplateManager,
                    mock(ProjectSchemeAssociationManager.class),
                    mock(TaskManager.class),
                    mock(IssueManager.class)
            );
        }

        public void projectDataIsValid() {
            isProjectDataValid = true;
        }

        public void projectDataIsNotValid() {
            isProjectDataValid = false;
        }

        public void setServiceContext(JiraServiceContext serviceContext) {
            this.serviceContext = serviceContext;
        }

        @Override
        public boolean isValidAllProjectData(JiraServiceContext serviceContext, ProjectCreationData projectCreationData) {
            if (!isProjectDataValid) {
                serviceContext.getErrorCollection().addError("some-field", "some-error");
            }
            return isProjectDataValid;
        }

        @Override
        protected JiraServiceContext getServiceContext(final ApplicationUser user, final ErrorCollection errorCollection) {
            return serviceContext;
        }
    }
}
