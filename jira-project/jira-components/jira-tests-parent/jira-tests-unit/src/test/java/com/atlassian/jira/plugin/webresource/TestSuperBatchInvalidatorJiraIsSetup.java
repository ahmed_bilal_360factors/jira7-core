package com.atlassian.jira.plugin.webresource;

import com.atlassian.jira.bc.dataimport.DataImportFinishedEvent;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.plugin.event.events.PluginFrameworkStartedEvent;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestSuperBatchInvalidatorJiraIsSetup {
    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @Mock
    private PluginFrameworkStartedEvent pluginFrameworkStartedEvent;

    @Mock
    private DataImportFinishedEvent dataImportFinishedEvent;

    @Mock
    private ApplicationProperties applicationProperties;

    private SuperBatchInvalidator superBatchInvalidator;

    @Before
    public void setUp() {
        superBatchInvalidator = new SuperBatchInvalidator(applicationProperties, null);
    }

    @Test
    public void testSuperBatchVersionNotIncrementedIfJiraSetup() {
        when(applicationProperties.getString(APKeys.JIRA_SETUP)).thenReturn("true");

        superBatchInvalidator.pluginFrameworkStarted(pluginFrameworkStartedEvent);
        verify(applicationProperties, never()).setString(any(String.class), any(String.class));

        superBatchInvalidator.dataImportFinished(dataImportFinishedEvent);
        verify(applicationProperties, never()).setString(any(String.class), any(String.class));
    }
}