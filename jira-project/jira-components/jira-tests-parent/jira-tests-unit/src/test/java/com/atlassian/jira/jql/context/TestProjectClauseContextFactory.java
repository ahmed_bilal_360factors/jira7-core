package com.atlassian.jira.jql.context;

import com.atlassian.jira.jql.operand.JqlOperandResolver;
import com.atlassian.jira.jql.resolver.NameResolver;
import com.atlassian.jira.jql.resolver.ProjectResolver;
import com.atlassian.jira.jql.validator.MockJqlOperandResolver;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.query.clause.TerminalClauseImpl;
import com.atlassian.query.operand.EmptyOperand;
import com.atlassian.query.operand.MultiValueOperand;
import com.atlassian.query.operand.SingleValueOperand;
import com.atlassian.query.operator.Operator;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import org.junit.Before;
import org.junit.Test;

import static com.atlassian.jira.jql.operand.SimpleLiteralFactory.createLiteral;
import static com.atlassian.jira.permission.ProjectPermissions.BROWSE_PROJECTS;
import static java.util.Collections.singleton;
import static java.util.Collections.singletonList;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestProjectClauseContextFactory {
    private JqlOperandResolver jqlOperandResolver;
    private NameResolver<Project> projectResolver;
    private PermissionManager permissionManager;
    private final ApplicationUser theUser = null;

    @Before
    public void setUp() {
        jqlOperandResolver = MockJqlOperandResolver.createSimpleSupport();
        projectResolver = mock(NameResolver.class);
        permissionManager = mock(PermissionManager.class);
    }

    @Test
    public void testGetClauseContextBadOperator() {
        final TerminalClauseImpl projectClause = new TerminalClauseImpl("project", Operator.GREATER_THAN, 12345L);
        final ProjectClauseContextFactory clauseContextFactory = new ProjectClauseContextFactory(jqlOperandResolver, projectResolver, permissionManager);
        final ClauseContext expectedContext = ClauseContextImpl.createGlobalClauseContext();

        final ClauseContext clauseContext = clauseContextFactory.getClauseContext(theUser, projectClause);

        assertEquals(expectedContext, clauseContext);
    }

    @Test
    public void testGetClauseContextForEmpty() {
        final TerminalClauseImpl projectClause = new TerminalClauseImpl("project", Operator.EQUALS, EmptyOperand.EMPTY);
        final ProjectClauseContextFactory clauseContextFactory = new ProjectClauseContextFactory(jqlOperandResolver, projectResolver, permissionManager);
        final ClauseContext expectedContext = ClauseContextImpl.createGlobalClauseContext();

        final ClauseContext clauseContext = clauseContextFactory.getClauseContext(theUser, projectClause);

        assertEquals(expectedContext, clauseContext);
    }

    @Test
    public void testGetClauseContextForNoVisibleProjects() {
        final MockProject mockProject = new MockProject(12345L, "TST", "Test Project");
        final TerminalClauseImpl projectClause = new TerminalClauseImpl("project", Operator.EQUALS, 12345L);
        final PermissionManager permissionManager = mock(PermissionManager.class);
        final ProjectResolver projectResolver = mock(ProjectResolver.class);
        when(projectResolver.get(eq(12345L))).thenReturn(mockProject);

        final ProjectClauseContextFactory clauseContextFactory = new ProjectClauseContextFactory(MockJqlOperandResolver.createSimpleSupport(), projectResolver, permissionManager);
        final ClauseContext expectedContext = ClauseContextImpl.createGlobalClauseContext();
        final ClauseContext clauseContext = clauseContextFactory.getClauseContext(theUser, projectClause);

        assertEquals(expectedContext, clauseContext);
    }

    @Test
    public void testGetClauseContextForNullLiterals() {
        final TerminalClauseImpl projectClause = new TerminalClauseImpl("project", Operator.EQUALS, 12345L);
        final JqlOperandResolver jqlOperandResolver = mock(JqlOperandResolver.class);

        final ProjectClauseContextFactory clauseContextFactory = new ProjectClauseContextFactory(jqlOperandResolver, projectResolver, permissionManager);
        final ClauseContext expectedContext = ClauseContextImpl.createGlobalClauseContext();
        final ClauseContext clauseContext = clauseContextFactory.getClauseContext(theUser, projectClause);

        assertEquals(expectedContext, clauseContext);
    }

    @Test
    public void testGetClauseContextForOneVisibleProject() {
        final TerminalClauseImpl projectClause = new TerminalClauseImpl("project", Operator.EQUALS, 12345L);
        final MockProject mockProject = new MockProject(12345L, "TST", "Test Project");
        final JqlOperandResolver jqlOperandResolver = mock(JqlOperandResolver.class);
        final PermissionManager permissionManager = mock(PermissionManager.class);
        final ProjectResolver projectResolver = mock(ProjectResolver.class);
        when(jqlOperandResolver.getValues(theUser, projectClause.getOperand(), projectClause)).thenReturn(singletonList(createLiteral(12345L)));
        when(permissionManager.getProjects(BROWSE_PROJECTS, null)).thenReturn(singletonList(mockProject));
        when(projectResolver.get(eq(12345L))).thenReturn(mockProject);

        final ProjectClauseContextFactory clauseContextFactory = new ProjectClauseContextFactory(jqlOperandResolver, projectResolver, permissionManager);
        final ClauseContext expectedContext = new ClauseContextImpl(singleton(new ProjectIssueTypeContextImpl(new ProjectContextImpl(12345L), AllIssueTypesContext.INSTANCE)));
        final ClauseContext clauseContext = clauseContextFactory.getClauseContext(theUser, projectClause);

        assertEquals(expectedContext, clauseContext);
    }

    @Test
    public void testGetClauseContextForOneVisibleProjectMultpleSpecified() {
        final TerminalClauseImpl projectClause = new TerminalClauseImpl("project", 12345L, 54321L);
        final MockProject mockProject1 = new MockProject(12345L, "TST", "Test Project");
        final MockProject mockProject2 = new MockProject(54321L, "ANA", "Another Project");
        final JqlOperandResolver jqlOperandResolver = mock(JqlOperandResolver.class);
        final PermissionManager permissionManager = mock(PermissionManager.class);
        final ProjectResolver projectResolver = mock(ProjectResolver.class);
        when(jqlOperandResolver.getValues(theUser, projectClause.getOperand(), projectClause)).thenReturn(ImmutableList.of(createLiteral(12345L), createLiteral(54321L)));
        when(permissionManager.getProjects(BROWSE_PROJECTS, null)).thenReturn(singletonList(mockProject1));
        when(projectResolver.get(eq(12345L))).thenReturn(mockProject1);
        when(projectResolver.get(eq(54321L))).thenReturn(mockProject2);

        final ProjectClauseContextFactory clauseContextFactory = new ProjectClauseContextFactory(jqlOperandResolver, projectResolver, permissionManager);
        final ClauseContext expectedContext = new ClauseContextImpl(ImmutableSet.of(new ProjectIssueTypeContextImpl(new ProjectContextImpl(12345L), AllIssueTypesContext.INSTANCE)));
        final ClauseContext clauseContext = clauseContextFactory.getClauseContext(theUser, projectClause);

        assertEquals(expectedContext, clauseContext);
    }

    @Test
    public void testGetClauseContextForTwoVisibleProjectsSameName() {
        final TerminalClauseImpl projectClause = new TerminalClauseImpl("project", Operator.EQUALS, "Test Project");
        final MockProject mockProject1 = new MockProject(12345L, "TST", "Test Project");
        final MockProject mockProject2 = new MockProject(54321L, "ANA", "Test Project");
        final JqlOperandResolver jqlOperandResolver = mock(JqlOperandResolver.class);
        final PermissionManager permissionManager = mock(PermissionManager.class);
        final ProjectResolver projectResolver = mock(ProjectResolver.class);
        when(jqlOperandResolver.getValues(theUser, projectClause.getOperand(), projectClause)).thenReturn(ImmutableList.of(createLiteral("Test Project")));
        when(permissionManager.getProjects(BROWSE_PROJECTS, null)).thenReturn(ImmutableList.of(mockProject1, mockProject2));
        when(projectResolver.getIdsFromName(anyString())).thenReturn(ImmutableList.of("12345", "54321"));

        final ProjectClauseContextFactory clauseContextFactory = new ProjectClauseContextFactory(jqlOperandResolver, projectResolver, permissionManager);
        final ClauseContext expectedContext = new ClauseContextImpl(ImmutableSet.of(
                new ProjectIssueTypeContextImpl(new ProjectContextImpl(12345L), AllIssueTypesContext.INSTANCE),
                new ProjectIssueTypeContextImpl(new ProjectContextImpl(54321L), AllIssueTypesContext.INSTANCE)));
        final ClauseContext clauseContext = clauseContextFactory.getClauseContext(theUser, projectClause);

        assertEquals(expectedContext, clauseContext);
    }

    @Test
    public void testGetClauseContextForTwoVisibleProjectsNotEquals() {
        final TerminalClauseImpl projectClause = new TerminalClauseImpl("project", Operator.NOT_EQUALS, 54321L);
        final MockProject mockProject1 = new MockProject(12345L, "TST", "Test Project");
        final MockProject mockProject2 = new MockProject(54321L, "ANA", "Test Project");
        final JqlOperandResolver jqlOperandResolver = mock(JqlOperandResolver.class);
        final PermissionManager permissionManager = mock(PermissionManager.class);
        final ProjectResolver projectResolver = mock(ProjectResolver.class);
        when(jqlOperandResolver.getValues(theUser, projectClause.getOperand(), projectClause)).thenReturn(ImmutableList.of(createLiteral(54321L)));
        when(permissionManager.getProjects(BROWSE_PROJECTS, null)).thenReturn(ImmutableList.of(mockProject1, mockProject2));
        when(projectResolver.get(eq(54321L))).thenReturn(mockProject2);

        final ProjectClauseContextFactory clauseContextFactory = new ProjectClauseContextFactory(jqlOperandResolver, projectResolver, permissionManager);
        final ClauseContext expectedContext = new ClauseContextImpl(ImmutableSet.of(
                new ProjectIssueTypeContextImpl(new ProjectContextImpl(12345L), AllIssueTypesContext.INSTANCE)));
        final ClauseContext clauseContext = clauseContextFactory.getClauseContext(theUser, projectClause);

        assertEquals(expectedContext, clauseContext);
    }

    @Test
    public void testGetClauseContextForOneVisibleProjectNoTypes() {
        final TerminalClauseImpl projectClause = new TerminalClauseImpl("project", Operator.EQUALS, 12345L);
        final MockProject mockProject = new MockProject(12345L, "TST", "Test Project");
        final JqlOperandResolver jqlOperandResolver = mock(JqlOperandResolver.class);
        final PermissionManager permissionManager = mock(PermissionManager.class);
        final ProjectResolver projectResolver = mock(ProjectResolver.class);
        when(jqlOperandResolver.getValues(theUser, projectClause.getOperand(), projectClause)).thenReturn(singletonList(createLiteral(12345L)));
        when(permissionManager.getProjects(BROWSE_PROJECTS, null)).thenReturn(singletonList(mockProject));
        when(projectResolver.get(eq(12345L))).thenReturn(mockProject);

        final ProjectClauseContextFactory clauseContextFactory = new ProjectClauseContextFactory(jqlOperandResolver, projectResolver, permissionManager);
        final ClauseContext expectedContext = new ClauseContextImpl(ImmutableSet.of(
                new ProjectIssueTypeContextImpl(new ProjectContextImpl(mockProject.getId()), AllIssueTypesContext.INSTANCE)));
        final ClauseContext clauseContext = clauseContextFactory.getClauseContext(theUser, projectClause);

        assertEquals(expectedContext, clauseContext);
    }

    @Test
    public void testGetClauseContextEmptyInList() {
        final TerminalClauseImpl projectClause = new TerminalClauseImpl("project", Operator.IN, new MultiValueOperand(new EmptyOperand()));
        final JqlOperandResolver jqlOperandResolver = MockJqlOperandResolver.createSimpleSupport();
        final ProjectClauseContextFactory clauseContextFactory = new ProjectClauseContextFactory(jqlOperandResolver, projectResolver, permissionManager);

        final ClauseContext expectedContext = ClauseContextImpl.createGlobalClauseContext();
        final ClauseContext clauseContext = clauseContextFactory.getClauseContext(theUser, projectClause);

        assertEquals(expectedContext, clauseContext);
    }

    @Test
    public void testGetClauseContextEmptyInWithValues() {
        final TerminalClauseImpl projectClause = new TerminalClauseImpl("project", Operator.IN, new MultiValueOperand(new EmptyOperand(), new SingleValueOperand(12345L)));
        final MockProject mockProject = new MockProject(12345L, "TST", "Test Project");
        final JqlOperandResolver jqlOperandResolver = MockJqlOperandResolver.createSimpleSupport();
        final PermissionManager permissionManager = mock(PermissionManager.class);
        final ProjectResolver projectResolver = mock(ProjectResolver.class);
        when(permissionManager.getProjects(BROWSE_PROJECTS, null)).thenReturn(singletonList(mockProject));
        when(projectResolver.get(eq(12345L))).thenReturn(mockProject);

        final ProjectClauseContextFactory clauseContextFactory = new ProjectClauseContextFactory(jqlOperandResolver, projectResolver, permissionManager);
        final ClauseContext expectedContext = new ClauseContextImpl(ImmutableSet.of(
                new ProjectIssueTypeContextImpl(new ProjectContextImpl(12345L), AllIssueTypesContext.INSTANCE)));
        final ClauseContext clauseContext = clauseContextFactory.getClauseContext(theUser, projectClause);

        assertEquals(expectedContext, clauseContext);
    }

    @Test
    public void testGetClauseContextEmptyInListNegation() {
        final TerminalClauseImpl projectClause = new TerminalClauseImpl("project", Operator.NOT_IN, new MultiValueOperand(new EmptyOperand()));
        final JqlOperandResolver jqlOperandResolver = MockJqlOperandResolver.createSimpleSupport();
        final ProjectClauseContextFactory clauseContextFactory = new ProjectClauseContextFactory(jqlOperandResolver, projectResolver, permissionManager);

        final ClauseContext expectedContext = ClauseContextImpl.createGlobalClauseContext();
        final ClauseContext clauseContext = clauseContextFactory.getClauseContext(theUser, projectClause);

        assertEquals(expectedContext, clauseContext);
    }

    @Test
    public void testGetClauseContextEmptyInWithValuesNegation() {
        final TerminalClauseImpl projectClause = new TerminalClauseImpl("project", Operator.NOT_IN, new MultiValueOperand(new EmptyOperand(), new SingleValueOperand(12345L)));
        final MockProject mockProject = new MockProject(12345L, "TST", "Test Project");
        final MockProject mockProject2 = new MockProject(12346L, "BAD", "BAD Project");
        final JqlOperandResolver jqlOperandResolver = MockJqlOperandResolver.createSimpleSupport();
        final PermissionManager permissionManager = mock(PermissionManager.class);
        final ProjectResolver projectResolver = mock(ProjectResolver.class);
        when(permissionManager.getProjects(BROWSE_PROJECTS, null)).thenReturn(ImmutableList.of(mockProject, mockProject2));
        when(projectResolver.get(eq(mockProject.getId()))).thenReturn(mockProject);
        when(projectResolver.get(eq(mockProject2.getId()))).thenReturn(mockProject2);

        final ProjectClauseContextFactory clauseContextFactory = new ProjectClauseContextFactory(jqlOperandResolver, projectResolver, permissionManager);
        final ClauseContext expectedContext = new ClauseContextImpl(ImmutableSet.of(
                new ProjectIssueTypeContextImpl(new ProjectContextImpl(12346L), AllIssueTypesContext.INSTANCE)));
        final ClauseContext clauseContext = clauseContextFactory.getClauseContext(theUser, projectClause);

        assertEquals(expectedContext, clauseContext);
    }

    @Test
    public void testGetClauseContextAllValuesNegation() {
        final TerminalClauseImpl projectClause = new TerminalClauseImpl("project", Operator.NOT_IN, new MultiValueOperand(new SingleValueOperand(12345L), new SingleValueOperand(12346L)));
        final MockProject mockProject = new MockProject(12345L, "TST", "Test Project");
        final MockProject mockProject2 = new MockProject(12346L, "BAD", "BAD Project");
        final JqlOperandResolver jqlOperandResolver = MockJqlOperandResolver.createSimpleSupport();
        final PermissionManager permissionManager = mock(PermissionManager.class);
        final ProjectResolver projectResolver = mock(ProjectResolver.class);
        when(permissionManager.getProjects(BROWSE_PROJECTS, null)).thenReturn(ImmutableList.of(mockProject, mockProject2));
        when(projectResolver.get(eq(mockProject.getId()))).thenReturn(mockProject);
        when(projectResolver.get(eq(mockProject2.getId()))).thenReturn(mockProject2);

        final ProjectClauseContextFactory clauseContextFactory = new ProjectClauseContextFactory(jqlOperandResolver, projectResolver, permissionManager);
        final ClauseContext expectedContext = ClauseContextImpl.createGlobalClauseContext();
        final ClauseContext clauseContext = clauseContextFactory.getClauseContext(theUser, projectClause);
        assertEquals(expectedContext, clauseContext);
    }
}
