package com.atlassian.jira.startup;

import com.atlassian.jira.cluster.ClusterManager;
import com.atlassian.jira.cluster.ClusterServicesManager;
import com.atlassian.jira.cluster.monitoring.ClusterMonitoringBeansRegistrar;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @since v7.3
 */
@RunWith(MockitoJUnitRunner.class)
public class TestClusteringLauncher {
    private static final String CLUSTER_MONITORING_DARK_FEATURE = "jira.zdu.jmx-monitoring";

    @Rule
    public final RuleChain mockContainer = MockitoMocksInContainer.forTest(this);

    @Mock
    @AvailableInContainer
    private FeatureManager featureManager;

    @Mock
    @AvailableInContainer
    private ClusterManager clusterManager;

    @Mock
    @AvailableInContainer
    private ClusterServicesManager clusterServicesManager;

    @Mock
    private ClusterMonitoringBeansRegistrar clusterMonitoringBeansRegistrar;

    private ClusteringLauncher clusteringLauncher;

    @Before
    public void setUp() {
        clusteringLauncher = new ClusteringLauncher(clusterMonitoringBeansRegistrar);
    }

    @Test
    public void clusterMonitoringServerIsDisabledWithoutDarkFeature() {
        disableClusterMonitoring();

        clusteringLauncher.start();

        verify(clusterMonitoringBeansRegistrar, never()).registerClusterMonitoringMBeans();
    }

    @Test
    public void clusterMonitoringServerIsEnabledByDarkFeature() {
        enableClusterMonitoring();

        clusteringLauncher.start();

        verify(clusterMonitoringBeansRegistrar, times(1)).registerClusterMonitoringMBeans();
    }

    @Test
    public void clusterMonitoringIsShutDown() {
        enableClusterMonitoring();

        clusteringLauncher.start();
        clusteringLauncher.stop();

        verify(clusterMonitoringBeansRegistrar, times(1)).unregisterClusterMonitorMBeans();
    }

    @Test
    public void clusterMonitoringIsNotShutDownWhenNotStarted() {
        disableClusterMonitoring();

        clusteringLauncher.start();
        clusteringLauncher.stop();

        verify(clusterMonitoringBeansRegistrar, times(0)).unregisterClusterMonitorMBeans();
    }

    private void enableClusterMonitoring() {
        when(featureManager.isEnabled(CLUSTER_MONITORING_DARK_FEATURE)).thenReturn(true);
    }

    private void disableClusterMonitoring() {
        when(featureManager.isEnabled(CLUSTER_MONITORING_DARK_FEATURE)).thenReturn(false);
    }
}
