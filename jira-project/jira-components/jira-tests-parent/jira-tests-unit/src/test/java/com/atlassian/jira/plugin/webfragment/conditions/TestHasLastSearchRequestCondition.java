package com.atlassian.jira.plugin.webfragment.conditions;

import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.util.velocity.VelocityRequestContext;
import com.atlassian.jira.util.velocity.VelocityRequestContextFactory;
import com.atlassian.jira.util.velocity.VelocityRequestSession;
import com.atlassian.jira.web.session.SessionSearchObjectManagerFactory;
import com.atlassian.jira.web.session.SessionSearchRequestManager;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestHasLastSearchRequestCondition {
    @Rule
    public final MethodRule initMockito = MockitoJUnit.rule();

    @Mock
    private VelocityRequestContextFactory requestContextFactory;
    @Mock
    private SessionSearchObjectManagerFactory sessionSearchObjectManagerFactory;
    @Mock
    private SessionSearchRequestManager sessionSearchRequestManager;
    @Mock
    private VelocityRequestContext requestContext;
    @Mock
    private VelocityRequestSession requestSession;

    @InjectMocks
    private HasLastSearchRequestCondition condition;

    @Test
    public void testNullcontext() {
        when(requestContextFactory.getJiraVelocityRequestContext()).thenReturn(null);

        assertFalse(condition.shouldDisplay(null, null));
    }

    @Test
    public void testNullSession() {
        when(requestContextFactory.getJiraVelocityRequestContext()).thenReturn(requestContext);
        when(requestContext.getSession()).thenReturn(null);

        assertFalse(condition.shouldDisplay(null, null));
    }

    @Test
    public void testNoFilter() {
        when(requestContextFactory.getJiraVelocityRequestContext()).thenReturn(requestContext);
        when(requestContext.getSession()).thenReturn(requestSession);
        when(sessionSearchObjectManagerFactory.createSearchRequestManager(requestSession)).thenReturn(sessionSearchRequestManager);
        when(sessionSearchRequestManager.getCurrentObject()).thenReturn(null);

        assertFalse(condition.shouldDisplay(null, null));
    }

    @Test
    public void testHasFilter() {
        final SearchRequest sr = new SearchRequest();

        when(requestContextFactory.getJiraVelocityRequestContext()).thenReturn(requestContext);
        when(requestContext.getSession()).thenReturn(requestSession);
        when(sessionSearchObjectManagerFactory.createSearchRequestManager(requestSession)).thenReturn(sessionSearchRequestManager);
        when(sessionSearchRequestManager.getCurrentObject()).thenReturn(sr);

        assertTrue(condition.shouldDisplay(null, null));
    }
}