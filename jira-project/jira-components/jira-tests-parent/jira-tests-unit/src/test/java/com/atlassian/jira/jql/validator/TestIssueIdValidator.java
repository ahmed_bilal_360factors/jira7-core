package com.atlassian.jira.jql.validator;

import com.atlassian.jira.issue.util.MovedIssueKeyStore;
import com.atlassian.jira.jql.operand.JqlOperandResolver;
import com.atlassian.jira.jql.operand.QueryLiteral;
import com.atlassian.jira.jql.util.JqlIssueKeySupport;
import com.atlassian.jira.jql.util.JqlIssueSupport;
import com.atlassian.jira.junit.rules.InitMockitoMocks;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.MessageSet;
import com.atlassian.jira.util.MessageSetImpl;
import com.atlassian.jira.util.NoopI18nFactory;
import com.atlassian.query.clause.TerminalClause;
import com.atlassian.query.clause.TerminalClauseImpl;
import com.atlassian.query.operand.FunctionOperand;
import com.atlassian.query.operand.Operand;
import com.atlassian.query.operand.SingleValueOperand;
import com.atlassian.query.operator.Operator;
import com.google.common.collect.Lists;
import org.hamcrest.Matchers;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.mockito.Mock;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.atlassian.jira.jql.operand.SimpleLiteralFactory.createLiteral;
import static com.atlassian.jira.util.NoopI18nHelper.makeTranslation;
import static com.atlassian.query.operand.EmptyOperand.EMPTY;
import static com.atlassian.query.operator.Operator.EQUALS;
import static com.atlassian.query.operator.Operator.IS;
import static com.google.common.collect.Sets.newHashSet;
import static java.util.Collections.singleton;
import static java.util.Collections.singletonList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

/**
 * Test for {@link IssueIdValidator}.
 *
 * @since v4.0
 */
public class TestIssueIdValidator {
    @Rule
    public TestRule initMockitoMocks = new InitMockitoMocks(this);

    @Mock
    private JqlOperandResolver resolver;
    @Mock
    private JqlIssueKeySupport keySupport;
    @Mock
    private JqlIssueSupport issueSupport;
    private I18nHelper.BeanFactory i18nFactory = new NoopI18nFactory();
    @Mock
    private SupportedOperatorsValidator supportedOperatorsValidator;
    @Mock
    private MovedIssueValidator movedIssueValidator;
    @Mock
    private MovedIssueKeyStore movedIssueKeyStore;

    private ApplicationUser theUser = null;

    @Test
    public void testValidateUnsupportedOperator() throws Exception {
        final String expectedError = "error message";

        final IssueIdValidator idValidator = getIssueIdValidator(expectedError);
        final MessageSet messageSet =
                idValidator.validate(theUser, new TerminalClauseImpl("issueKey", Operator.LIKE, "PKY-134"));
        assertTrue(messageSet.hasAnyErrors());
        assertTrue(messageSet.getErrorMessages().contains(expectedError));
    }

    private IssueIdValidator getIssueIdValidator() {
        return new IssueIdValidator(resolver, keySupport, issueSupport, i18nFactory, new MockSupportedOperatorsValidator(), movedIssueValidator);
    }

    private IssueIdValidator getIssueIdValidator(final String expectedError) {
        return new IssueIdValidator(resolver, keySupport, issueSupport, i18nFactory, new MockSupportedOperatorsValidator(expectedError), movedIssueValidator);
    }

    @Test
    public void testValidateNullLiterals() throws Exception {
        final String issueKey = "PK-1287";
        final String fieldName = "key";
        final SingleValueOperand operand = new SingleValueOperand(issueKey);
        final TerminalClauseImpl clause = new TerminalClauseImpl(fieldName, Operator.EQUALS, operand);


        when(resolver.getValues(theUser, operand, clause)).thenReturn(null);

        IssueIdValidator validator = getIssueIdValidator();

        final MessageSet messageSet = validator.validate(theUser, clause);

        assertNotNull(messageSet);
        assertEquals(Collections.<String>emptySet(), messageSet.getErrorMessages());
        assertEquals(Collections.<String>emptySet(), messageSet.getWarningMessages());
    }

    @Test
    public void testValidateEmptyLiterals() throws Exception {
        final String fieldName = "key";
        final Operand operand = EMPTY;
        final TerminalClauseImpl clause = new TerminalClauseImpl(fieldName, IS, operand);

        when(resolver.getValues(theUser, operand, clause)).thenReturn(singletonList(new QueryLiteral()));
        when(resolver.isFunctionOperand(operand)).thenReturn(false);

        IssueIdValidator validator = getIssueIdValidator();
        final MessageSet messageSet = validator.validate(theUser, clause);

        final Set<String> expectedErrors =
                singleton(makeTranslation("jira.jql.clause.field.does.not.support.empty", fieldName));

        assertThat(messageSet, Matchers.allOf(Matchers.hasProperty("errorMessages", Matchers.equalTo(expectedErrors)),
                Matchers.hasProperty("warningMessages", Matchers.emptyIterable())));
    }

    @Test
    public void testValidateBadKey() throws Exception {
        final String issueKey = "PK-1287";
        final String fieldName = "key";
        final SingleValueOperand operand = new SingleValueOperand(issueKey);
        final TerminalClauseImpl clause = new TerminalClauseImpl(fieldName, EQUALS, operand);

        when(resolver.getValues(theUser, operand, clause)).thenReturn(singletonList(createLiteral(issueKey)));
        when(resolver.isFunctionOperand(operand)).thenReturn(false);
        when(keySupport.isValidIssueKey(issueKey)).thenReturn(false);
        when(issueSupport.getKeysOfMissingIssues(newHashSet(issueKey))).thenReturn(newHashSet(issueKey));

        final IssueIdValidator validator = getIssueIdValidator();
        final MessageSet messageSet = validator.validate(theUser, clause);

        final Set<String> expectedErrors = singleton(
                makeTranslation("jira.jql.clause.issuekey.invalidissuekey", issueKey, fieldName));

        assertThat(messageSet, Matchers.allOf(Matchers.hasProperty("errorMessages", Matchers.equalTo(expectedErrors)),
                Matchers.hasProperty("warningMessages", Matchers.emptyIterable())));
    }

    @Test
    public void testValidateBadKeyFunction() throws Exception {
        final String issueKey = "PK-1287";
        final String fieldName = "key";
        final String funcName = "qwerty";
        final Operand operand = new FunctionOperand(funcName);
        final TerminalClauseImpl clause = new TerminalClauseImpl(fieldName, EQUALS, operand);

        when(resolver.getValues(theUser, operand, clause)).thenReturn(singletonList(new QueryLiteral(operand, issueKey)));
        when(resolver.isFunctionOperand(operand)).thenReturn(true);
        when(keySupport.isValidIssueKey(issueKey)).thenReturn(false);
        when(issueSupport.getKeysOfMissingIssues(newHashSet(issueKey))).thenReturn(newHashSet(issueKey));

        final IssueIdValidator validator = getIssueIdValidator();
        final MessageSet messageSet = validator.validate(theUser, clause);

        final Set<String> expectedErrors = singleton(
                makeTranslation("jira.jql.clause.issuekey.invalidissuekey.from.func", funcName, fieldName));

        assertThat(messageSet, Matchers.allOf(Matchers.hasProperty("errorMessages", Matchers.equalTo(expectedErrors)),
                Matchers.hasProperty("warningMessages", Matchers.emptyIterable())));
    }

    @Test
    public void testValidateNoIssue() throws Exception {
        final String issueKey = "PK-1287";
        final String fieldName = "key";
        final Operand operand = new SingleValueOperand(issueKey);
        final TerminalClauseImpl clause = new TerminalClauseImpl(fieldName, EQUALS, operand);

        when(resolver.getValues(theUser, operand, clause)).thenReturn(singletonList(createLiteral(issueKey)));
        when(resolver.isFunctionOperand(operand)).thenReturn(false);
        when(keySupport.isValidIssueKey(issueKey)).thenReturn(true);
        when(issueSupport.getKeysOfMissingIssues(newHashSet(issueKey))).thenReturn(newHashSet(issueKey));

        final IssueIdValidator validator = getIssueIdValidator();
        final MessageSet messageSet = validator.validate(theUser, clause);

        final Set<String> expectedErrors =
                singleton(makeTranslation("jira.jql.clause.issuekey.noissue", issueKey, fieldName));

        assertThat(messageSet, Matchers.allOf(Matchers.hasProperty("errorMessages", Matchers.equalTo(expectedErrors)),
                Matchers.hasProperty("warningMessages", Matchers.emptyIterable())));
    }

    @Test
    public void testValidateNoIssueFunction() throws Exception {
        final String issueKey = "PK-1287";
        final String fieldName = "key";
        final String funcName = "qwerty";

        final Operand operand = new FunctionOperand(funcName);
        final TerminalClauseImpl clause = new TerminalClauseImpl(fieldName, EQUALS, operand);

        when(resolver.getValues(theUser, operand, clause)).thenReturn(singletonList(new QueryLiteral(operand, issueKey)));
        when(resolver.isFunctionOperand(operand)).thenReturn(true);
        when(keySupport.isValidIssueKey(issueKey)).thenReturn(true);
        when(issueSupport.getKeysOfMissingIssues(newHashSet(issueKey))).thenReturn(newHashSet(issueKey));

        final IssueIdValidator validator = getIssueIdValidator();
        final MessageSet messageSet = validator.validate(theUser, clause);

        final Set<String> expectedErrors = singleton(
                makeTranslation("jira.jql.clause.issuekey.noissue.from.func", funcName, fieldName));

        assertThat(messageSet, Matchers.allOf(Matchers.hasProperty("errorMessages", Matchers.equalTo(expectedErrors)),
                Matchers.hasProperty("warningMessages", Matchers.emptyIterable())));
    }

    @Test
    public void testValidateHappyPathWithValues() throws Exception {
        final String issueKey = "PK-1287";
        final String fieldName = "key";
        final Operand operand = new SingleValueOperand(issueKey);
        final TerminalClauseImpl clause = new TerminalClauseImpl(fieldName, EQUALS, operand);

        when(resolver.getValues(theUser, operand, clause)).thenReturn(singletonList(createLiteral(issueKey)));
        when(issueSupport.getKeysOfMissingIssues(newHashSet(issueKey))).thenReturn(new HashSet<String>());
        when(movedIssueValidator.validate(null, newHashSet(issueKey), clause)).thenReturn(new MessageSetImpl());

        final IssueIdValidator validator = getIssueIdValidator();
        final MessageSet messageSet = validator.validate(theUser, clause);

        assertThat(messageSet, Matchers.allOf(Matchers.hasProperty("errorMessages", Matchers.emptyIterable()),
                Matchers.hasProperty("warningMessages", Matchers.emptyIterable())));
    }

    @Test
    public void testValidateHappyPathWithMultipleValues() throws Exception {
        final String issueKey = "PK-1287";
        final String fieldName = "key";
        final Operand operand = new SingleValueOperand(issueKey);
        final TerminalClauseImpl clause = new TerminalClauseImpl(fieldName, Operator.EQUALS, operand);

        final List<QueryLiteral> values = new ArrayList<>();
        final Set<String> keys = new HashSet<>();
        for (int i = 0; i < 10; i++) {
            String key = "PK-1287" + i;
            keys.add(key);
            values.add(createLiteral(key));
        }
        when(resolver.getValues(theUser, operand, clause)).thenReturn(values);
        when(issueSupport.getKeysOfMissingIssues(newHashSet(issueKey))).thenReturn(new HashSet<String>());
        when(movedIssueValidator.validate(null, newHashSet(issueKey), clause)).thenReturn(new MessageSetImpl());

        final IssueIdValidator validator = getIssueIdValidator();
        final MessageSet messageSet = validator.validate(theUser, clause);

        assertThat(messageSet, Matchers.allOf(Matchers.hasProperty("errorMessages", Matchers.emptyIterable()),
                Matchers.hasProperty("warningMessages", Matchers.emptyIterable())));
    }

    @Test
    public void testValidateHappyPathWithHeapsOfValues() throws Exception {
        final String issueKey = "PK-1287";
        final String fieldName = "key";
        final Operand operand = new SingleValueOperand(issueKey);
        final TerminalClauseImpl clause = new TerminalClauseImpl(fieldName, Operator.EQUALS, operand);

        final List<QueryLiteral> values = new ArrayList<>();
        final Set<String> keys = new HashSet<>();
        for (int i = 0; i < 100; i++) {
            String key = "PK-1287" + i;
            keys.add(key);
            values.add(createLiteral(key));
        }
        when(resolver.getValues(theUser, operand, clause)).thenReturn(values);
        when(issueSupport.getKeysOfMissingIssues(newHashSet(issueKey))).thenReturn(new HashSet<String>());
        when(movedIssueValidator.validate(null, newHashSet(issueKey), clause)).thenReturn(new MessageSetImpl());

        final IssueIdValidator validator = getIssueIdValidator();
        final MessageSet messageSet = validator.validate(theUser, clause);

        assertThat(messageSet, Matchers.allOf(Matchers.hasProperty("errorMessages", Matchers.emptyIterable()),
                Matchers.hasProperty("warningMessages", Matchers.emptyIterable())));
    }

    @Test
    public void testValidateHappyPathWithIdValues() throws Exception {
        final long id = 9292;
        final String fieldName = "key";
        final Operand operand = new SingleValueOperand(id);
        final TerminalClauseImpl clause = new TerminalClauseImpl(fieldName, EQUALS, operand);

        when(resolver.getValues(theUser, operand, clause)).thenReturn(singletonList(createLiteral(id)));
        when(issueSupport.getIdsOfMissingIssues(newHashSet(id))).thenReturn(new HashSet<Long>());

        final IssueIdValidator validator = getIssueIdValidator();
        final MessageSet messageSet = validator.validate(theUser, clause);

        assertThat(messageSet, Matchers.allOf(Matchers.hasProperty("errorMessages", Matchers.emptyIterable()),
                Matchers.hasProperty("warningMessages", Matchers.emptyIterable())));
    }

    @Test
    public void testValidateHappyPathWithoutValues() throws Exception {
        final String issueKey = "PK-1287";
        final String fieldName = "key";
        final Operand operand = new SingleValueOperand(issueKey);
        final TerminalClauseImpl clause = new TerminalClauseImpl(fieldName, EQUALS, operand);

        when(resolver.getValues(theUser, operand, clause)).thenReturn(Lists.newArrayList());

        final IssueIdValidator validator = getIssueIdValidator();
        final MessageSet messageSet = validator.validate(theUser, clause);

        assertThat(messageSet, Matchers.allOf(Matchers.hasProperty("errorMessages", Matchers.emptyIterable()),
                Matchers.hasProperty("warningMessages", Matchers.emptyIterable())));
    }

    @Test
    public void testValidateInvalidId() throws Exception {
        final long id = 1287;
        final String fieldName = "name";
        final Operand operand = new SingleValueOperand(id);
        final TerminalClauseImpl clause = new TerminalClauseImpl(fieldName, EQUALS, operand);

        when(resolver.getValues(theUser, operand, clause)).thenReturn(singletonList(createLiteral(id)));
        when(resolver.isFunctionOperand(operand)).thenReturn(false);
        when(issueSupport.getIdsOfMissingIssues(newHashSet(id))).thenReturn(newHashSet(id));

        final IssueIdValidator validator = getIssueIdValidator();
        final MessageSet messageSet = validator.validate(theUser, clause);

        assertNotNull(messageSet);
        assertEquals(singleton("jira.jql.clause.no.value.for.id{[name, 1287]}"),
                messageSet.getErrorMessages());
        assertThat(messageSet.getWarningMessages(), Matchers.emptyIterable());
    }

    @Test
    public void testValidateWithMultipleValuesAndInvalid() throws Exception {
        final String issueKey = "PK-1287";
        final String fieldName = "key";
        final Operand operand = new SingleValueOperand(issueKey);
        final TerminalClauseImpl clause = new TerminalClauseImpl(fieldName, Operator.EQUALS, operand);

        final List<QueryLiteral> values = new ArrayList<>();
        final Set<String> keys = new HashSet<>();
        for (int i = 0; i < 10; i++) {
            String key = "PK-1287" + i;
            keys.add(key);
            values.add(createLiteral(key));
        }
        when(resolver.getValues(theUser, operand, clause)).thenReturn(values);
        when(issueSupport.getKeysOfMissingIssues(keys)).thenReturn(newHashSet(issueKey));
        when(movedIssueValidator.validate(null, keys, clause)).thenReturn(new MessageSetImpl());
        when(keySupport.isValidIssueKey(issueKey)).thenReturn(true);
        final IssueIdValidator validator = getIssueIdValidator();
        final MessageSet messageSet = validator.validate(theUser, clause);

        assertNotNull(messageSet);
        assertEquals(singleton("jira.jql.clause.issuekey.noissue{[PK-1287, key]}"),
                messageSet.getErrorMessages());
        assertThat(messageSet.getWarningMessages(), Matchers.emptyIterable());
    }

    @Test
    public void testValidateWithFunctionValuesAndInvalid() throws Exception {
        final String issueKey = "PK-1287";
        final String fieldName = "key";
        final Operand operand = new SingleValueOperand(issueKey);
        final TerminalClauseImpl clause = new TerminalClauseImpl(fieldName, Operator.EQUALS, operand);

        final List<QueryLiteral> values = new ArrayList<>();
        final Set<String> keys = new HashSet<>();
        for (int i = 0; i < 10; i++) {
            String key = "PK-1287" + i;
            keys.add(key);
            values.add(createLiteral(key));
        }
        when(resolver.getValues(theUser, operand, clause)).thenReturn(values);
        when(resolver.isFunctionOperand(operand)).thenReturn(true);
        when(issueSupport.getKeysOfMissingIssues(keys)).thenReturn(newHashSet(issueKey));
        when(movedIssueValidator.validate(null, keys, clause)).thenReturn(new MessageSetImpl());
        when(keySupport.isValidIssueKey(issueKey)).thenReturn(true);
        final IssueIdValidator validator = getIssueIdValidator();
        final MessageSet messageSet = validator.validate(theUser, clause);

        assertNotNull(messageSet);
        assertThat(messageSet, Matchers.allOf(Matchers.hasProperty("errorMessages", Matchers.emptyIterable()),
                Matchers.hasProperty("warningMessages", Matchers.emptyIterable())));
    }

    @Test
    public void testValidateInvalidIdFromFunc() throws Exception {
        final long id = 1288;
        final String fieldName = "qwerty";
        final Operand operand = new FunctionOperand("funcName");
        final TerminalClauseImpl clause = new TerminalClauseImpl(fieldName, EQUALS, operand);

        when(resolver.getValues(theUser, operand, clause)).thenReturn(singletonList(new QueryLiteral(operand, id)));
        when(resolver.isFunctionOperand(operand)).thenReturn(true);
        when(issueSupport.getIdsOfMissingIssues(newHashSet(id))).thenReturn(newHashSet(id));

        final IssueIdValidator validator = getIssueIdValidator();
        final MessageSet messageSet = validator.validate(theUser, clause);

        assertNotNull(messageSet);
        assertEquals(singleton("jira.jql.clause.no.value.for.name.from.function{[funcName, qwerty]}"),
                messageSet.getErrorMessages());
        assertThat(messageSet.getWarningMessages(), Matchers.emptyIterable());
    }

    private static class MockSupportedOperatorsValidator extends SupportedOperatorsValidator {
        private final String message;

        public MockSupportedOperatorsValidator() {
            this(null);
        }

        public MockSupportedOperatorsValidator(final String message) {
            this.message = message;
        }

        @Override
        public MessageSet validate(final ApplicationUser searcher, final TerminalClause terminalClause) {
            MessageSet set = new MessageSetImpl();
            if (message != null) {
                set.addErrorMessage(message);
            }
            return set;
        }
    }
}
