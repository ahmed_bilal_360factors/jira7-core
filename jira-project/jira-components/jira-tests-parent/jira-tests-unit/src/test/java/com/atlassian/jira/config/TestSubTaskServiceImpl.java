package com.atlassian.jira.config;

import com.atlassian.jira.bc.ServiceResult;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.link.IssueLink;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoContainer;
import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.MockUserLocaleStore;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.NoopI18nFactory;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;

import java.util.Arrays;
import java.util.Locale;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 *
 */
public class TestSubTaskServiceImpl {
    @Rule
    public MockitoContainer mockitoContainer = new MockitoContainer(this);

    @Mock
    private SubTaskManager manager;

    @Mock
    private PermissionManager permissionManager;

    @Mock
    private FeatureManager featureManager;

    @AvailableInContainer
    MockUserLocaleStore store = new MockUserLocaleStore(Locale.ENGLISH);
    @AvailableInContainer
    private I18nHelper.BeanFactory beanFactory = new NoopI18nFactory();

    private SubTaskService service;

    @Before
    public void setUp() throws Exception {
        service = new SubTaskServiceImpl(manager, permissionManager, featureManager);
    }

    @Test
    public void subTaskSuccessfullyMovedInNormalCircumstances() {

        when(manager.getSubTaskIssueLinks(anyLong())).thenReturn(Arrays.asList(
                mock(IssueLink.class),
                mock(IssueLink.class),
                mock(IssueLink.class)));

        when(permissionManager.hasPermission(eq(ProjectPermissions.EDIT_ISSUES), any(Issue.class), any(ApplicationUser.class))).thenReturn(true);

        ServiceResult result = service.moveSubTask(mock(ApplicationUser.class), mock(Issue.class), 0L, 0L);

        assertThat(result.isValid(), is(true));
    }

    @Test
    public void errorWhenUserWithoutPermissionToEditIssue() {
        ApplicationUser userWithLocale = new MockApplicationUser("userWithLocale");
        store.setLocale(userWithLocale, Locale.ENGLISH);

        when(permissionManager.hasPermission(eq(ProjectPermissions.EDIT_ISSUES), any(Issue.class), any(ApplicationUser.class))).thenReturn(false);

        ServiceResult result = service.moveSubTask(userWithLocale, mock(Issue.class), 0L, 0L);

        assertThat(result.getErrorCollection().getReasons(), Matchers.contains(ErrorCollection.Reason.FORBIDDEN));
    }
}