package com.atlassian.jira.imports.project.validation;

import com.atlassian.jira.external.beans.ExternalProjectRoleActor;
import com.atlassian.jira.external.beans.ExternalUser;
import com.atlassian.jira.imports.project.core.ProjectImportOptionsImpl;
import com.atlassian.jira.imports.project.mapper.ProjectImportMapper;
import com.atlassian.jira.imports.project.mapper.ProjectImportMapperImpl;
import com.atlassian.jira.imports.project.mapper.ProjectRoleActorMapper;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.user.util.MockUserManager;
import com.atlassian.jira.user.util.UserUtil;
import com.atlassian.jira.util.MessageSet;
import com.atlassian.jira.util.MessageSetAssert;
import com.atlassian.jira.web.bean.MockI18nBean;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import static com.atlassian.jira.security.roles.ProjectRoleActor.GROUP_ROLE_ACTOR_TYPE;
import static com.atlassian.jira.security.roles.ProjectRoleActor.USER_ROLE_ACTOR_TYPE;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @since v3.13
 */
public class TestProjectRoleActorMapperValidatorImpl {
    @Rule
    public MethodRule initMockito = MockitoJUnit.rule();

    @Mock
    UserUtil userUtil;

    @Test
    public void testValidateUnknownRoleType() throws Exception {
        final ProjectImportMapper projectImportMapper = new ProjectImportMapperImpl(null, null);

        projectImportMapper.getProjectRoleActorMapper().flagValueActorAsInUse(new ExternalProjectRoleActor("1", "2", "12", "Dog", "Rover"));
        projectImportMapper.getProjectRoleMapper().registerOldValue("12", "Dudes");

        final ProjectRoleActorMapperValidatorImpl validator = new ProjectRoleActorMapperValidatorImpl(null);
        final MessageSet messageSet = validator.validateProjectRoleActors(new MockI18nBean(), projectImportMapper, new ProjectImportOptionsImpl("", "", true));
        MessageSetAssert.assert1Warning(messageSet, "Project role 'Dudes' contains an actor 'Rover' of unknown role type 'Dog'. This actor will not be added to the project role.");
    }

    @Test
    public void testValidateWithDontUpdateDetailsOption() throws Exception {
        final ProjectImportMapper projectImportMapper = new ProjectImportMapperImpl(null, null);

        projectImportMapper.getProjectRoleActorMapper().flagValueActorAsInUse(new ExternalProjectRoleActor("1", "2", "12", "Dog", "Rover"));
        projectImportMapper.getProjectRoleMapper().registerOldValue("12", "Dudes");

        final ProjectRoleActorMapperValidatorImpl validator = new ProjectRoleActorMapperValidatorImpl(null);
        // If the USer chooses "Don't update project details, then we don't change the role memberships,and therefore we have no validation.
        final MessageSet messageSet = validator.validateProjectRoleActors(new MockI18nBean(), projectImportMapper, new ProjectImportOptionsImpl("", "", false));
        MessageSetAssert.assertNoMessages(messageSet);
    }

    @Test
    public void testValidateMissingGroup() throws Exception {
        final GroupManager mockGroupManager = mock(GroupManager.class);
        when(mockGroupManager.groupExists("goodies")).thenReturn(true);
        when(mockGroupManager.groupExists("baddies")).thenReturn(false);

        final ProjectImportMapper projectImportMapper = new ProjectImportMapperImpl(null, mockGroupManager);

        final ProjectRoleActorMapper projectRoleActorMapper = projectImportMapper.getProjectRoleActorMapper();
        projectRoleActorMapper.flagValueActorAsInUse(new ExternalProjectRoleActor("1", "2", "12", GROUP_ROLE_ACTOR_TYPE, "goodies"));
        projectRoleActorMapper.flagValueActorAsInUse(new ExternalProjectRoleActor("1", "2", "12", GROUP_ROLE_ACTOR_TYPE, "baddies"));

        projectImportMapper.getProjectRoleMapper().registerOldValue("12", "Dudes");

        final ProjectRoleActorMapperValidatorImpl validator = new ProjectRoleActorMapperValidatorImpl(null);
        final MessageSet messageSet = validator.validateProjectRoleActors(new MockI18nBean(), projectImportMapper, new ProjectImportOptionsImpl("", "", true));
        MessageSetAssert.assert1Warning(messageSet, "Project role 'Dudes' contains a group 'baddies' that doesn't exist in the current system. This group will not be added to the project role membership.");
    }

    @Test
    public void testValidateMissingUsersExternalUserManagement() throws Exception {
        final ExternalUser peter = new ExternalUser("peterKey", "peter", "Peter", "", "");

        when(userUtil.userExists("peter")).thenReturn(true);
        when(userUtil.userExists("paul")).thenReturn(false);

        final ProjectImportMapper projectImportMapper = new ProjectImportMapperImpl(userUtil, null);
        projectImportMapper.getUserMapper().registerOldValue(peter);

        final ProjectRoleActorMapper projectRoleActorMapper = projectImportMapper.getProjectRoleActorMapper();
        projectRoleActorMapper.flagValueActorAsInUse(new ExternalProjectRoleActor("1", "2", "12", USER_ROLE_ACTOR_TYPE, "peterKey"));
        projectRoleActorMapper.flagValueActorAsInUse(new ExternalProjectRoleActor("1", "2", "12", USER_ROLE_ACTOR_TYPE, "paul"));

        projectImportMapper.getProjectRoleMapper().registerOldValue("12", "Dudes");

        final MockUserManager userManager = new MockUserManager();
        final ProjectRoleActorMapperValidatorImpl validator = new ProjectRoleActorMapperValidatorImpl(userManager);
        final MessageSet messageSet = validator.validateProjectRoleActors(new MockI18nBean(), projectImportMapper, new ProjectImportOptionsImpl("", "", true));
        MessageSetAssert.assert1Warning(messageSet, "Project role 'Dudes' contains a user 'paul' that doesn't exist in the current system. This user will not be added to the project role membership.");
    }

    @Test
    public void testValidateMissingUsersJiraUserManagement() throws Exception {
        when(userUtil.userExists("peter")).thenReturn(true);
        when(userUtil.userExists("paul")).thenReturn(false);
        when(userUtil.userExists("mary")).thenReturn(false);

        final ProjectImportMapper projectImportMapper = new ProjectImportMapperImpl(userUtil, null);

        final ProjectRoleActorMapper projectRoleActorMapper = projectImportMapper.getProjectRoleActorMapper();
        projectRoleActorMapper.flagValueActorAsInUse(new ExternalProjectRoleActor("1", "2", "12", USER_ROLE_ACTOR_TYPE, "peterKey"));
        projectRoleActorMapper.flagValueActorAsInUse(new ExternalProjectRoleActor("1", "2", "12", USER_ROLE_ACTOR_TYPE, "paulKey"));
        projectRoleActorMapper.flagValueActorAsInUse(new ExternalProjectRoleActor("1", "2", "12", USER_ROLE_ACTOR_TYPE, "mary"));

        projectImportMapper.getProjectRoleMapper().registerOldValue("12", "Dudes");

        // Make it that paul can be auto-created.
        final ExternalUser paul = new ExternalUser("paulKey", "paul", "Paul", "", "");
        final ExternalUser peter = new ExternalUser("peterKey", "peter", "Peter", "", "");
        projectImportMapper.getUserMapper().registerOldValue(paul);
        projectImportMapper.getUserMapper().registerOldValue(peter);

        final ProjectRoleActorMapperValidatorImpl validator = new ProjectRoleActorMapperValidatorImpl(new MockUserManager());
        final MessageSet messageSet = validator.validateProjectRoleActors(new MockI18nBean(), projectImportMapper, new ProjectImportOptionsImpl("", "", true));
        MessageSetAssert.assert1Warning(messageSet, "Project role 'Dudes' contains a user 'mary' that doesn't exist in the current system. This user will not be added to the project role membership.");
    }
}
