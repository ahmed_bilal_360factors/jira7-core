package com.atlassian.jira.web.servlet;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.MethodRule;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.CharArrayWriter;
import java.io.IOException;

import static org.hamcrest.Matchers.isA;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

public class TestBufferingRequestDispatcher {
    @Rule
    public final MethodRule initMockito = MockitoJUnit.rule();
    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpServletResponse response;
    @InjectMocks
    private BufferingRequestDispatcher bufferingRequestDispatcher;

    @Test
    public void testInclude() throws Exception {
        when(request.getRequestDispatcher("path")).thenReturn(new RequestDispatcher() {
            public void forward(ServletRequest request, ServletResponse response) {
                throw new UnsupportedOperationException();
            }

            public void include(ServletRequest request, ServletResponse response) throws IOException {
                response.getWriter().write("output");
            }
        });
        CharArrayWriter result = bufferingRequestDispatcher.include("path");

        assertEquals("output", result.toString());
    }

    @Test
    public void testNoOutputStream() throws Exception {
        final String exceptionMessage = "TestBufferingRequestDispatcher.testNoOutputStream()";
        when(request.getRequestDispatcher("path")).thenReturn(new RequestDispatcher() {
            public void forward(ServletRequest request, ServletResponse response) {
                throw new UnsupportedOperationException();
            }

            public void include(ServletRequest request, ServletResponse response) throws IOException {
                try {
                    response.getOutputStream();
                } catch (UnsupportedOperationException exception) {
                    throw new UnsupportedOperationException(exceptionMessage, exception);
                }
            }
        });
        exception.expect(UnsupportedOperationException.class);
        exception.expectMessage(exceptionMessage);
        exception.expectCause(isA(UnsupportedOperationException.class));
        bufferingRequestDispatcher.include("path");
    }
}
