package com.atlassian.jira.issue.fields;

import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.issue.RendererManager;
import com.atlassian.jira.issue.fields.config.manager.FieldConfigSchemeManager;
import com.atlassian.jira.jql.context.FieldConfigSchemeClauseContextUtil;
import com.atlassian.jira.mock.component.MockComponentWorker;
import com.atlassian.jira.mock.ofbiz.MockGenericValue;
import com.atlassian.jira.mock.ofbiz.MockOfBizDelegator;
import com.atlassian.jira.model.querydsl.CustomFieldDTO;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.plugin.customfield.CustomFieldSearcherModuleDescriptors;
import com.atlassian.jira.plugin.customfield.CustomFieldTypeModuleDescriptors;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.web.action.admin.translation.TranslationManager;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Answers;
import org.mockito.Mock;
import org.ofbiz.core.entity.GenericValue;
import org.ofbiz.core.entity.model.ModelEntity;

import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestDefaultCustomFieldFactory {
    private DefaultCustomFieldFactory factory;

    OfBizDelegator ofBizDelegator = new MockOfBizDelegator();

    @Before
    public void setUp() {
        new MockComponentWorker().init();
        factory = createFactory();
    }

    @Test(expected = NullPointerException.class)
    public void createDoesNotAcceptANullGenericValue() {
        factory.create((GenericValue) null);
    }

    @Test(expected = NullPointerException.class)
    public void createDoesNotAcceptANullCustomFieldDTO() {
        factory.create((CustomFieldDTO) null);
    }

    @Test
    public void createWithGVInstantiatesAnInstanceCorrectly() {
        GenericValue genericValue = mockGenericValue();

        CustomField customField = factory.create(genericValue);

        assertThat(customField.getGenericValue().get("id"), is(genericValue.get("id")));
    }

    @Test
    public void createWithDTOInstantiatesAnInstanceCorrectly() {
        CustomFieldDTO customFieldDTO = mock(CustomFieldDTO.class);

        CustomField customField = factory.create(customFieldDTO);

        assertThat(customField.getIdAsLong(), is(customFieldDTO.getId()));
    }

    private GenericValue mockGenericValue() {
        String anyEntityName = "customField";
        Map<String, Object> map = new HashMap<>();
        map.put("id", 1L);

        ModelEntity modelEntity = mock(ModelEntity.class);
        when(modelEntity.getEntityName()).thenReturn(anyEntityName);

        return new MockGenericValue(anyEntityName, modelEntity, map);
    }

    private CustomField customFieldWith(GenericValue genericValue) {
        CustomField customField = mock(CustomField.class);
        when(customField.getGenericValue()).thenReturn(genericValue);
        return customField;
    }

    private DefaultCustomFieldFactory createFactory() {
        return new DefaultCustomFieldFactory(
                mock(JiraAuthenticationContext.class),
                mock(FieldConfigSchemeManager.class),
                mock(PermissionManager.class),
                mock(RendererManager.class),
                mock(FieldConfigSchemeClauseContextUtil.class),
                mock(CustomFieldDescription.class),
                mock(FeatureManager.class),
                mock(TranslationManager.class),
                mock(CustomFieldScopeFactory.class),
                mock(CustomFieldTypeModuleDescriptors.class),
                mock(CustomFieldSearcherModuleDescriptors.class),
                ofBizDelegator);
    }
}
