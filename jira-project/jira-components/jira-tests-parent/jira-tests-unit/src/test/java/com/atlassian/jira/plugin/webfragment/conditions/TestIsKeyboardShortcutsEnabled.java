package com.atlassian.jira.plugin.webfragment.conditions;

import com.atlassian.core.user.preferences.Preferences;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.preferences.UserPreferencesManager;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

public class TestIsKeyboardShortcutsEnabled {
    private static final String SHORTCUTS_DISABLED_KEY = "user.keyboard.shortcuts.disabled";
    @Rule
    public MockitoRule mockito = MockitoJUnit.rule();
    @Mock
    private Preferences preferences;
    @Mock
    private UserPreferencesManager mockUserPreferencesManager;

    private ApplicationUser user = new MockApplicationUser("admin");

    @Test
    public void testShouldDisplayAnonymous() {
        IsKeyboardShortcutsEnabledCondition condition = new IsKeyboardShortcutsEnabledCondition(null);
        final boolean result = condition.shouldDisplay(null, null);
        assertTrue(result);
    }

    @Test
    public void testShouldDisplayEnabled() {
        when(preferences.getBoolean(SHORTCUTS_DISABLED_KEY)).thenReturn(false);
        when(mockUserPreferencesManager.getPreferences(user)).thenReturn(preferences);

        IsKeyboardShortcutsEnabledCondition condition = new IsKeyboardShortcutsEnabledCondition(mockUserPreferencesManager);
        final boolean result = condition.shouldDisplay(user, null);
        assertTrue(result);
    }

    @Test
    public void testShouldDisplayDisabled() {
        when(preferences.getBoolean(SHORTCUTS_DISABLED_KEY)).thenReturn(true);
        when(mockUserPreferencesManager.getPreferences(user)).thenReturn(preferences);

        IsKeyboardShortcutsEnabledCondition condition = new IsKeyboardShortcutsEnabledCondition(mockUserPreferencesManager);
        final boolean result = condition.shouldDisplay(user, null);
        assertFalse(result);
    }
}
