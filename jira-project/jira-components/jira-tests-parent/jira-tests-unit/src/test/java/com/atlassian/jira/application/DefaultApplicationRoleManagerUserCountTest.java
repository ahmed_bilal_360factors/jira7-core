package com.atlassian.jira.application;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.cache.memory.MemoryCacheManager;
import com.atlassian.crowd.directory.RemoteDirectory;
import com.atlassian.collectors.CollectorsUtil;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.DirectoryType;
import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.crowd.embedded.spi.DirectoryDao;
import com.atlassian.crowd.event.application.ApplicationDirectoryOrderUpdatedEvent;
import com.atlassian.crowd.event.directory.DirectoryUpdatedEvent;
import com.atlassian.crowd.event.directory.RemoteDirectorySynchronisationFailedEvent;
import com.atlassian.crowd.event.directory.RemoteDirectorySynchronisedEvent;
import com.atlassian.crowd.event.group.GroupCreatedEvent;
import com.atlassian.crowd.event.group.GroupDeletedEvent;
import com.atlassian.crowd.event.group.GroupMembershipDeletedEvent;
import com.atlassian.crowd.event.group.GroupMembershipsCreatedEvent;
import com.atlassian.crowd.event.group.GroupUpdatedEvent;
import com.atlassian.crowd.event.user.AutoUserUpdatedEvent;
import com.atlassian.crowd.event.user.UserDeletedEvent;
import com.atlassian.crowd.event.user.UserEditedEvent;
import com.atlassian.crowd.manager.directory.SynchronisationStatusManager;
import com.atlassian.crowd.model.application.Application;
import com.atlassian.crowd.model.directory.ImmutableDirectory;
import com.atlassian.crowd.model.user.User;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.fugue.Option;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.config.database.DatabaseConfig;
import com.atlassian.jira.config.database.JndiDatasource;
import com.atlassian.jira.config.database.MockDatabaseConfigurationManager;
import com.atlassian.jira.crowd.embedded.ofbiz.InternalMembershipDao;
import com.atlassian.jira.crowd.embedded.ofbiz.MockDirectory;
import com.atlassian.jira.crowd.embedded.ofbiz.OfBizUserDao;
import com.atlassian.jira.crowd.embedded.ofbiz.db.DefaultOfBizTransactionManager;
import com.atlassian.jira.event.ClearCacheEvent;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.license.JiraLicenseManager;
import com.atlassian.jira.license.LicenseChangedEvent;
import com.atlassian.jira.license.LicenseDetails;
import com.atlassian.jira.license.MockLicenseDetails;
import com.atlassian.jira.license.MockLicensedApplications;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.groups.MockGroupManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.ApplicationUsers;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.MockCrowdService;
import com.atlassian.jira.user.MockGroup;
import com.atlassian.jira.user.MockUser;
import com.atlassian.jira.user.util.DisabledRecoveryMode;
import com.atlassian.jira.user.util.MockUserManager;
import com.atlassian.jira.user.util.UserManager;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Answers;
import org.mockito.Mock;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static com.atlassian.application.api.ApplicationKey.valueOf;
import static com.atlassian.extras.common.LicensePropertiesConstants.UNLIMITED_USERS;
import static com.atlassian.jira.config.CoreFeatures.LICENSE_ROLES_ENABLED;
import static com.atlassian.jira.test.util.lic.ApplicationLicenseConstants.SOFTWARE_KEY;
import static com.google.common.collect.ImmutableMap.of;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyCollectionOf;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

public class DefaultApplicationRoleManagerUserCountTest {
    @Rule
    public MockitoContainer container = MockitoMocksInContainer.rule(this);

    private static final Group OHS_GROUP = new MockGroup("ohs.group");
    private static final Group CORE_GROUP = new MockGroup("core.group");
    private static final Group SOFTWARE_GROUP = new MockGroup("software.group");
    private static final int USERS_ACTIVE_UNIQUE_IN_CORE_AND_SOFTWARE = 4;
    private static final int USERS_ACTIVE_IN_SOFTWARE = 3;
    private static final int USERS_ACTIVE_IN_SOFTWARE_AND_OHS = 7;
    private static final int USERS_ACTIVE_IN_CORE_AND_OHS = 6;
    private static final int USERS_ACTIVE_IN_CORE_AND_OHS_UNIQUE_TO_SOFTWARE = 5;
    private static final int USERS_ACTIVE_IN_UNDEFINED_OR_NULL_APP_ROLE = 0;
    private static final int UNIQUE_USERS_IN_ALL_GROUPS = 8;

    private final ApplicationKey applicationKeySoftware = ApplicationKey.valueOf(SOFTWARE_KEY);
    private final ApplicationKey applicationKeyUndefined = ApplicationKey.valueOf("undefined");
    private final ApplicationKey applicationKeyCore = ApplicationKeys.CORE;

    private final MockGroupManager groupManager = spy(new MockGroupManager());
    private final MemoryCacheManager cacheManager = new MemoryCacheManager();
    private final MockCrowdService crowdService = new MockCrowdService();
    private final MockApplicationRoleStore store = new MockApplicationRoleStore();
    private final MockApplicationRoleDefinitions definitions = new MockApplicationRoleDefinitions();
    private DefaultApplicationRoleManager manager;
    private MockUserManager userManager;

    @Mock
    private JiraLicenseManager licenseManager;
    @Mock
    private FeatureManager featureManager;
    @Mock
    private EventPublisher eventPublisher;
    @Mock
    private RemoteDirectory remoteDirectory;
    @Mock
    private InternalMembershipDao internalMembershipDao;
    @Mock
    private DirectoryDao directoryDao;
    @Mock
    private com.atlassian.crowd.model.group.Group group;
    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private SynchronisationStatusManager crowdSyncStatusManager;

    private OfBizUserDao ofBizUserDao;
    private Directory directory;

    @Mock
    @AvailableInContainer
    private GlobalPermissionManager globalPermissionManager;

    private ApplicationKey testAppKey = ApplicationKey.valueOf("com.test.application.one");
    private Set<LicenseDetails> licenses = new HashSet<>();

    @Before
    public void before() {
        final MockDatabaseConfigurationManager databaseConfigurationManager = new MockDatabaseConfigurationManager();
        databaseConfigurationManager.setDatabaseConfiguration(new DatabaseConfig("postgres", "jira", new JndiDatasource("jira")));
        ofBizUserDao = spy(new OfBizUserDao(null, directoryDao, null, null, null, cacheManager, null, null, new DefaultOfBizTransactionManager(), databaseConfigurationManager) {
            @Override
            public boolean useFullCache() {
                return false;
            }

            @Override
            public void processUsers(Consumer userProcessor) {
                userManager.getAllUsers().forEach(appUser -> userProcessor.accept(appUser.getDirectoryUser()));
            }
        });

        userManager = new MockUserManager(crowdService);
        userManager.alwaysReturnUsers();
        userManager.useCrowdServiceToGetUsers();
        container.getMockWorker().addMock(UserManager.class, userManager);

        when(licenseManager.getLicenses()).thenReturn(licenses);

        manager = new DefaultApplicationRoleManager(cacheManager, store, definitions, groupManager,
                licenseManager, new DisabledRecoveryMode(), crowdService, eventPublisher,
                internalMembershipDao, directoryDao, ofBizUserDao, new DefaultOfBizTransactionManager(),
                crowdSyncStatusManager, featureManager);

        //Necessary pre-condition to all cache flush test to ensure the two groups have different populations.
        assertThat("Two groups have different populations",
                USERS_ACTIVE_UNIQUE_IN_CORE_AND_SOFTWARE, greaterThan(USERS_ACTIVE_IN_SOFTWARE));

        final MockUser johnInactiveUser = new MockUser("NoJohn");
        johnInactiveUser.setActive(false);
        final ApplicationUser johnInactive = ApplicationUsers.from(johnInactiveUser);
        final MockApplicationUser john = new MockApplicationUser("John");
        final MockApplicationUser stephani = new MockApplicationUser("Stephani");
        final MockApplicationUser george = new MockApplicationUser("George");
        final MockApplicationUser helen = new MockApplicationUser("Helen");
        final MockApplicationUser jim = new MockApplicationUser("Jim");
        final MockApplicationUser sophie = new MockApplicationUser("Sophie");
        final MockApplicationUser peter = new MockApplicationUser("Peter");
        final MockApplicationUser alex = new MockApplicationUser("Alex");

        for (ApplicationUser user : ImmutableList.of(johnInactive, john, stephani, george, helen, jim, sophie, peter, alex)) {
            crowdService.addUser(user.getDirectoryUser(), null);
        }

        for (Group group : ImmutableList.of(CORE_GROUP, SOFTWARE_GROUP, OHS_GROUP)) {
            crowdService.addGroup(group);
        }

        for (ApplicationUser user : ImmutableList.of(john, stephani, johnInactive, george)) {
            groupManager.addUserToGroup(user, SOFTWARE_GROUP);
            crowdService.addUserToGroup(user, SOFTWARE_GROUP);
        }

        for (ApplicationUser user : ImmutableList.of(john, helen)) {
            groupManager.addUserToGroup(user, CORE_GROUP);
            crowdService.addUserToGroup(user, CORE_GROUP);
        }

        for (ApplicationUser user : ImmutableList.of(jim, sophie, johnInactive, peter, alex)) {
            groupManager.addUserToGroup(user, OHS_GROUP);
            crowdService.addUserToGroup(user, OHS_GROUP);
        }

        definitions.addDefined(applicationKeySoftware.value(), "Name");
        definitions.addDefined(applicationKeyCore.value(), "Core");
        definitions.addLicensed(applicationKeySoftware.value(), "Name");
        definitions.addLicensed(applicationKeyCore.value(), "Core");

        setupLicense(of(applicationKeySoftware, 100));

        when(directoryDao.findAll()).thenReturn(ImmutableList.of(new MockDirectory(MockUser.MOCK_DIRECTORY_ID), new MockDirectory(2L)));

        when(internalMembershipDao.findGroupChildrenOfGroups(anyLong(), anyCollectionOf(String.class))).thenReturn(Collections.emptyList());

        when(internalMembershipDao.findUserChildrenOfGroups(anyLong(), anyCollectionOf(String.class))).thenAnswer(invocation ->
        {
            long directoryId = invocation.getArgumentAt(0, Long.class);
            @SuppressWarnings("unchecked") Collection<String> groupNames = invocation.getArgumentAt(1, Collection.class);
            Set<com.atlassian.crowd.embedded.api.User> users = new LinkedHashSet<>();
            for (String groupName : groupNames) {
                users.addAll(
                        groupManager.getUsersInGroup(groupName).stream()
                                .filter(user -> user.getDirectoryId() == directoryId)
                                .map(ApplicationUser::getDirectoryUser)
                                .collect(Collectors.toList()));
            }

            return users.stream().map(com.atlassian.crowd.embedded.api.User::getName).collect(Collectors.toList());
        });

        directory = ImmutableDirectory.builder("1", DirectoryType.DELEGATING, "").setId(1L).build();
    }

    @Test
    public void getUserCountForExistingRoleIdWithOneGroupMustReturnNumberOfUsersInGroup() {
        // Given: Software role has software group only.
        store.save(applicationKeySoftware, SOFTWARE_GROUP.getName());

        //When
        int activeSoftwareUsers = manager.getUserCount(applicationKeySoftware);

        // Expect to have 3 users (one is inactive)
        assertThat("Active Users with software group only",
                activeSoftwareUsers, equalTo(USERS_ACTIVE_IN_SOFTWARE));
    }

    @Test
    public void getUserCountForExistingRoleIdWith2GroupsMustReturnSuperSetSize() {
        // software application role has core and software groups
        // software group has one inactive and 3 active
        // core group has one duplicate user with software and total of 2 active
        store.save(applicationKeySoftware, SOFTWARE_GROUP, CORE_GROUP);

        //When
        int activeSoftwareUsers = manager.getUserCount(applicationKeySoftware);

        // Expect to have 4 users (extra one from core)
        assertThat("Active Users after Cache clear",
                activeSoftwareUsers, equalTo(USERS_ACTIVE_UNIQUE_IN_CORE_AND_SOFTWARE));
    }

    @Test
    public void getRemainingSeatsWhenThereAreSeatsAvailableMustReturnPositiveNumber() {
        // Given: There is an application role with 4 seats, and 3 are occupied
        setupLicense(of(applicationKeySoftware, 4));
        store.save(applicationKeySoftware, SOFTWARE_GROUP);

        // When: asking for the roles remaining seats
        int remainingSeats = manager.getRemainingSeats(applicationKeySoftware);

        // Then: expect to have 1 seats available
        assertThat("There is 1 available seat",
                remainingSeats, equalTo(1));
    }

    @Test
    public void roleLimitShouldNotBeExceedIfLicenseRemoved() {
        setupLicense(of(applicationKeySoftware, USERS_ACTIVE_IN_SOFTWARE - 1));
        store.save(applicationKeySoftware, SOFTWARE_GROUP);
        final boolean roleLimitExceeded = manager.isRoleLimitExceeded(applicationKeySoftware);
        removeLicense(applicationKeySoftware);

        final boolean roleLimitExceededAfterDelete = manager.isRoleLimitExceeded(applicationKeySoftware);
        assertTrue(roleLimitExceeded);
        assertFalse("Role limit shouldn't be exceeded after license deleted", roleLimitExceededAfterDelete);
    }

    @Test
    public void getRemainingSeatsWhenLicenseIsRemovedShouldReturZero() {
        setupLicense(of(applicationKeySoftware, 4));
        store.save(applicationKeySoftware, SOFTWARE_GROUP);

        final int userCount = manager.getUserCount(applicationKeySoftware);

        removeLicense(applicationKeySoftware);

        final int usersCountAfterLicenseRemoved = manager.getUserCount(applicationKeySoftware);

        assertThat(userCount, equalTo(USERS_ACTIVE_IN_SOFTWARE));
        assertThat(usersCountAfterLicenseRemoved, equalTo(0));
    }

    @Test
    public void getRemainingSeatsWhenThereAreZero() {
        // Given: There is an application role with 3 seats, and 3 are occupied
        setupLicense(of(applicationKeySoftware, 3));
        store.save(applicationKeySoftware, SOFTWARE_GROUP);

        // When: asking for the roles remaining seats
        int remainingSeats = manager.getRemainingSeats(applicationKeySoftware);

        // Then: expect to have no seats are available
        assertThat("There are no available seats",
                remainingSeats, equalTo(0));
    }

    @Test
    public void getRemainingSeatsWhenThereAreFewerSeatsAvailable() {
        // Given: There is an application role with 2 seats, and 3 are occupied
        setupLicense(of(applicationKeySoftware, 2));
        store.save(applicationKeySoftware, SOFTWARE_GROUP);

        // When: asking for the roles remaining seats
        int remainingSeats = manager.getRemainingSeats(applicationKeySoftware);

        // Then: expect to have 0 seats available as we have busted the license limit
        assertThat("There are no seats available",
                remainingSeats, equalTo(0));
    }

    @Test
    public void activatingUserFlushesCache() {
        assertClearCache(() -> manager.onUserUpdated(
                new UserEditedEvent(null, directory, activeUser("fred"), disabledUser("fred"))));
    }

    @Test
    public void nullChangedUserFlushesCache() {
        assertClearCache(() -> manager.onUserUpdated(
                new UserEditedEvent(null, directory, activeUser("fred"), disabledUser("fred"))));
    }

    @Test
    public void autoActivatingUserFlushesCache() {
        assertClearCache(() -> manager.onUserUpdated(
                new AutoUserUpdatedEvent(null, directory, activeUser("fred"), disabledUser("fred"))));
    }

    @Test
    public void nullAutoChangedUserFlushesCache() {
        assertClearCache(() -> manager.onUserUpdated(
                new AutoUserUpdatedEvent(null, directory, activeUser("fred"), disabledUser("fred"))));
    }

    @Test
    public void deactivatingUserFlushesCache() {
        assertClearCache(() -> manager.onUserUpdated(
                new UserEditedEvent(null, directory, disabledUser("fred"), activeUser("fred"))));
    }

    @Test
    public void autoDeactivatingUserFlushesCache() {
        assertClearCache(() -> manager.onUserUpdated(
                new AutoUserUpdatedEvent(null, directory, disabledUser("fred"), activeUser("fred"))));
    }

    @Test
    public void leavingUserActiveDoesNotFlushCache() {
        assertNoClearCache(() -> manager.onUserUpdated(
                new UserEditedEvent(null, directory, activeUser("fred"), activeUser("fred"))));
    }

    @Test
    public void leavingUserInActiveDoesNotFlushCache() {
        assertNoClearCache(() -> manager.onUserUpdated(
                new UserEditedEvent(null, directory, disabledUser("fred"), disabledUser("fred"))));
    }

    @Test
    public void usersPerLicenseCacheFlushOnClearCacheEvent() {
        assertClearCache(() ->
        {
            // Send ClearCache event
            manager.onClearCache(ClearCacheEvent.INSTANCE);
        });
    }

    @Test
    public void usersPerLicenseCacheFlushedOnGroupCreated() {
        assertClearCache(() -> manager.onGroupCreated(new GroupCreatedEvent(null, directory, group)));
    }

    @Test
    public void usersPerLicenseCacheFlushedOnGroupUpdated() {
        assertClearCache(() -> manager.onGroupUpdated(new GroupUpdatedEvent(null, directory, group)));
    }

    @Test
    public void usersPerLicenseCacheFlushedOnGroupDeleted() {
        assertClearCache(() -> manager.onGroupDeleted(new GroupDeletedEvent(null, directory, null)));
    }

    @Test
    public void usersPerLicenseCacheNotFlushedOnGroupCreatedWhileSync() {
        startSync();
        assertNoClearCache(() -> manager.onGroupCreated(new GroupCreatedEvent(null, directory, group)));
    }

    @Test
    public void usersPerLicenseCacheFlushedOnGroupCreatedWhileSyncIfFeatureDisabled() {
        when(featureManager.isEnabled("com.atlassian.jira.ldap.nonblocking.sync.disabled")).thenReturn(true);

        startSync();
        assertClearCache(() -> manager.onGroupCreated(new GroupCreatedEvent(null, directory, group)));
    }

    @Test
    public void usersPerLicenseCacheNotFlushedOnGroupUpdatedWhileSync() {
        startSync();
        assertNoClearCache(() -> manager.onGroupUpdated(new GroupUpdatedEvent(null, directory, group)));
    }

    @Test
    public void usersPerLicenseCacheNotFlushedOnGroupDeletedWhileSync() {
        startSync();
        assertNoClearCache(() -> manager.onGroupDeleted(new GroupDeletedEvent(null, directory, null)));
    }

    @Test
    public void usersPerLicenseCacheFlushedOnMembershipCreated() {
        assertClearCache(() -> manager.onGroupMembershipsCreated(new GroupMembershipsCreatedEvent(null, directory, ImmutableSet.of("group"), null, null)));
    }

    @Test
    public void usersPerLicenseCacheFlushedOnMembershipDeleted() {
        assertClearCache(() -> manager.onGroupMembershipDeleted(new GroupMembershipDeletedEvent(null, directory, null, null, null)));
    }

    @Test
    public void usersPerLicenseCacheNotFlushedOnMembershipCreatedWhileSync() {
        startSync();
        assertNoClearCache(() -> manager.onGroupMembershipsCreated(new GroupMembershipsCreatedEvent(null, directory, ImmutableSet.of("group"), null, null)));
    }

    @Test
    public void usersPerLicenseCacheFlushedOnMembershipCreatedWhileOtherDirectoryIsInSync() {
        Directory d2 = ImmutableDirectory.builder("2", DirectoryType.DELEGATING, "").setId(2L).build();

        when(crowdSyncStatusManager.getDirectorySynchronisationInformation(eq(directory)).isSynchronising()).thenReturn(true);
        when(crowdSyncStatusManager.getDirectorySynchronisationInformation(eq(d2)).isSynchronising()).thenReturn(false);

        assertNoClearCache(() -> manager.onGroupMembershipsCreated(new GroupMembershipsCreatedEvent(null, directory, ImmutableSet.of("group"), null, null)));
        assertClearCache(() -> manager.onGroupMembershipsCreated(new GroupMembershipsCreatedEvent(null, d2, ImmutableSet.of("group"), null, null)));
    }

    @Test
    public void usersPerLicenseCacheNotFlushedOnMembershipDeletedWhileSync() {
        startSync();
        assertNoClearCache(() -> manager.onGroupMembershipDeleted(new GroupMembershipDeletedEvent(null, directory, null, null, null)));
    }

    @Test
    public void usersPerLicenseCacheFlushedOnUserDeleted() {
        assertClearCache(() -> manager.onUserDeleted(new UserDeletedEvent(null, directory, "fred")));
    }

    @Test
    public void usersPerLicenseCacheNotFlushedOnUserDeletedWhileSync() {
        startSync();
        assertNoClearCache(() -> manager.onUserDeleted(new UserDeletedEvent(null, directory, "fred")));
    }

    @Test
    public void usersPerLicenseCacheFlushedOnLicenseChangedEvent() {
        assertClearCache(() -> manager.onLicenseChanged(new LicenseChangedEvent(Option.none(), Option.none())));
    }

    @Test
    public void usersPerLicenseCacheFlushedOnDirectoryReorder() {
        assertClearCache(() -> manager.onDirectoryReorder(new ApplicationDirectoryOrderUpdatedEvent(mock(Application.class), mock(Directory.class))));
    }

    @Test
    public void usersPerLicenseCacheFlushedOnDirectoryUpdated() {
        assertClearCache(() -> manager.onDirectoryUpdated(new DirectoryUpdatedEvent(null, directory)));
    }

    @Test
    public void usersPerLicenseCacheFlushedOnSyncFinished() {
        assertClearCache(() -> manager.onSyncFinished(new RemoteDirectorySynchronisedEvent(null, remoteDirectory, 0L)));
    }

    @Test
    public void usersPerLicenseCacheFlushedOnSyncFailed() {
        assertClearCache(() -> manager.onSyncFinished(new RemoteDirectorySynchronisationFailedEvent(null, remoteDirectory, 0L)));
    }

    @Test
    public void getUserCountWhenLicenseUndefinedMustReturnZero() {
        int activeSoftwareUsers = manager.getUserCount(applicationKeyUndefined);

        assertThat("Active Users after Cache clear",
                activeSoftwareUsers, equalTo(USERS_ACTIVE_IN_UNDEFINED_OR_NULL_APP_ROLE));
    }

    @Test(expected = IllegalArgumentException.class)
    public void getUserCountForNullRoleId() {
        manager.getUserCount(null);
    }

    @Test
    public void getUserCountWithWithGroupsContainingSameUsersNotDoubleCounted() {
        // software application role has OHS and software groups
        // software group has one inactive and 3 active
        // core group has total of 2 active and no duplicates to software.
        store.save(applicationKeySoftware, SOFTWARE_GROUP, OHS_GROUP);

        //When
        int activeSoftwareUsers = manager.getUserCount(applicationKeySoftware);

        // Expect to have 5 users 3 Software + 2 OHS
        assertThat("Active Users for OHS and Core",
                activeSoftwareUsers, equalTo(USERS_ACTIVE_IN_SOFTWARE_AND_OHS));
    }

    @Test
    public void usersPerLicenseCacheFlushedWhenRemovingGroupFromRoles() {
        assertClearCache(() -> manager.removeGroupFromRoles(OHS_GROUP));
    }

    @Test
    public void usersPerLicenseCacheFlushedWhenSavingToRole() {
        // When: Software role has software group only.
        store.save(applicationKeySoftware, SOFTWARE_GROUP);

        int activeSoftwareUsers = manager.getUserCount(applicationKeySoftware);
        int billableUsers = manager.totalBillableUsers();

        // Expect to have 3 users (one is inactive)
        assertThat("Active Users with software group only",
                activeSoftwareUsers, equalTo(USERS_ACTIVE_IN_SOFTWARE));

        assertThat("Active Users with software group only",
                billableUsers, equalTo(USERS_ACTIVE_IN_SOFTWARE));

        // Add core group to software application role
        store.save(applicationKeySoftware, SOFTWARE_GROUP, CORE_GROUP);

        //Verify that cache was not flushed.
        activeSoftwareUsers = manager.getUserCount(applicationKeySoftware);
        billableUsers = manager.totalBillableUsers();

        // Expect to have 3 users (extra one from core)
        assertThat("Active Users after Cache clear",
                activeSoftwareUsers, equalTo(USERS_ACTIVE_IN_SOFTWARE));

        assertThat("Billable Users after Cache clear",
                billableUsers, equalTo(USERS_ACTIVE_IN_SOFTWARE));

        manager.setRole(new MockApplicationRole().key(applicationKeySoftware));

        activeSoftwareUsers = manager.getUserCount(applicationKeySoftware);
        billableUsers = manager.totalBillableUsers();

        assertThat("Active Users after Cache clear", activeSoftwareUsers, equalTo(0));
        assertThat("Billable Users after Cache clear", billableUsers, equalTo(0));
    }

    @Test
    public void usersForCoreLicenseAreNotDoubleCountedWithOtherActiveRoles() {
        definitions.addDefined(applicationKeySoftware.value(), "Software definition");
        definitions.addLicensed(applicationKeySoftware.value(), "Software definition");
        definitions.addDefined(applicationKeyCore.value(), "Core definition");
        definitions.addLicensed(applicationKeyCore.value(), "Core definition");

        store.save(applicationKeySoftware, SOFTWARE_GROUP);
        store.save(applicationKeyCore, SOFTWARE_GROUP, OHS_GROUP, CORE_GROUP);

        //Preconditions:
        assertThat("Software group has different amount of users from combined OHS and Core",
                USERS_ACTIVE_IN_SOFTWARE,
                not(equalTo(USERS_ACTIVE_IN_CORE_AND_OHS)));

        //then:
        assertThat("Cache did not refresh role1 active users still Software",
                manager.getUserCount(applicationKeySoftware),
                equalTo(USERS_ACTIVE_IN_SOFTWARE));

        assertThat("Cache did not refresh role1 active users still Software",
                manager.getUserCount(applicationKeyCore),
                equalTo(USERS_ACTIVE_IN_CORE_AND_OHS_UNIQUE_TO_SOFTWARE));

        definitions.removeDefinition(applicationKeySoftware);
        definitions.removeLicensed(applicationKeySoftware);

        manager.onClearCache(ClearCacheEvent.INSTANCE);

        assertThat("Cache did not refresh role1 active users still Software",
                manager.getUserCount(applicationKeyCore),
                equalTo(UNIQUE_USERS_IN_ALL_GROUPS));
    }

    @Test
    public void usersForCoreAreCountedWhenOtherRoleIsExceeded() {
        when(featureManager.isEnabled(LICENSE_ROLES_ENABLED)).thenReturn(true);
        definitions.addDefined(applicationKeySoftware.value(), "Software definition");
        definitions.addLicensed(applicationKeySoftware.value(), "Software definition");

        definitions.addDefined(applicationKeyCore.value(), "Core definition");
        definitions.addLicensed(applicationKeyCore.value(), "Core definition");

        // SOFTWARE_GROUP: john, stephani, johnInactive, george
        // CORE_GROUP:  john, helen
        store.save(applicationKeySoftware, SOFTWARE_GROUP);
        store.save(applicationKeyCore, CORE_GROUP);

        // Only helen is taking a CORE seat, john is taking a Software seat instead
        assertEquals("Users that belong to a non exceeded role other than CORE should not take a CORE seat",
                1, manager.getUserCount(applicationKeyCore));

        // Software license exceeds so john should start taking a CORE seat
        setupLicense(of(applicationKeySoftware, 2));
        manager.clearCache();

        assertEquals("Users that belong to an exceeded role other than CORE should take a CORE seat",
                2, manager.getUserCount(applicationKeyCore));
    }

    @Test
    public void usersForCoreAreNotCountedIfNotAllOtherRolesExceeded() {
        when(featureManager.isEnabled(LICENSE_ROLES_ENABLED)).thenReturn(true);
        final ApplicationKey applicationKeySD = ApplicationKeys.SERVICE_DESK;

        definitions.addDefined(applicationKeySoftware.value(), "Software definition");
        definitions.addLicensed(applicationKeySoftware.value(), "Software definition");
        definitions.addDefined(applicationKeyCore.value(), "Core definition");
        definitions.addLicensed(applicationKeyCore.value(), "Core definition");
        definitions.addDefined(applicationKeySD.value(), "SD definition");
        definitions.addLicensed(applicationKeySD.value(), "SD definition");

        // SOFTWARE_GROUP: john, stephani, johnInactive, george
        // CORE_GROUP:  john, helen
        setupLicense(of(applicationKeySoftware, 2)); // Software is exceeded
        setupLicense(of(applicationKeySD, 5)); // SD is not exceeded
        store.save(applicationKeySoftware, SOFTWARE_GROUP);
        store.save(applicationKeySD, SOFTWARE_GROUP);
        store.save(applicationKeyCore, CORE_GROUP);

        // John can still use his SD seat so won't count towards CORE limit
        assertEquals("Users that belong to an non exceeded role other than CORE should not take a CORE seat",
                1, manager.getUserCount(applicationKeyCore));

        // Software license exceeds too so john should start taking a CORE seat
        setupLicense(of(applicationKeySD, 2)); // SD is now exceeded
        manager.clearCache();

        assertEquals("Users that belong only to exceeded roles apart from CORE should take a CORE seat",
                2, manager.getUserCount(applicationKeyCore));
    }

    @Test
    public void shouldHaveSeatsAvailableInRole() {
        // Given: There is a application role with 5 seats, and 3 are occupied
        setupLicense(of(applicationKeySoftware, 5));

        //When:
        store.save(applicationKeySoftware, SOFTWARE_GROUP);

        // Then: expect to have no seats are available
        assertTrue(manager.hasSeatsAvailable(applicationKeySoftware, 1));
        assertTrue(manager.hasSeatsAvailable(applicationKeySoftware, 2));
        assertFalse(manager.hasSeatsAvailable(applicationKeySoftware, 3));
    }

    @Test
    public void shouldNotHaveAnySeatsAvailableForUnlicensedRole() {
        assertFalse(manager.hasSeatsAvailable(valueOf("com.none"), 3));
    }

    @Test(expected = IllegalArgumentException.class)
    public void haveAnySeatsAvailableForNegativeNumberFails() {
        manager.hasSeatsAvailable(valueOf("com.none"), -1);
    }

    @Test
    public void shouldHaveSeatAvailableInUnlimitedRole() {
        //Given
        setupLicense(of(applicationKeySoftware, UNLIMITED_USERS));

        //When:
        store.save(applicationKeySoftware, SOFTWARE_GROUP);

        //Then:
        assertTrue(manager.hasSeatsAvailable(applicationKeySoftware, 999999999));
    }

    @Test
    public void getUserCountDoesNotCountInactiveUsers()
            throws Exception {
        MockApplicationUser user1 = new MockApplicationUser("Bob");
        user1.setActive(false);
        ((MockUser) user1.getDirectoryUser()).setActive(false);
        ApplicationUser user2 = new MockApplicationUser("Alice");
        final Group group1 = new MockGroup("group1");
        final Group group2 = new MockGroup("group2");
        crowdService.addUser(user1);
        crowdService.addUser(user2);
        groupManager.addUserToGroup(user1, group1);
        crowdService.addUserToGroup(user1, group1);
        groupManager.addUserToGroup(user2, group2);
        crowdService.addUserToGroup(user2, group2);

        setupApplication(testAppKey, 5, ImmutableSet.of(group1, group2));
        when(licenseManager.getAllLicensedApplicationKeys()).thenReturn(ImmutableSet.of(testAppKey));

        assertEquals("Should count regular user", 1, manager.getUserCount(testAppKey));
    }

    @Test
    public void billableUserCountDoesNotDoubleCountShadowedUsers() {
        final Group group1 = new MockGroup("group1");
        final Group group2 = new MockGroup("group2");
        MockUser userDir1 = setupMockUser("Bob", MockUser.MOCK_DIRECTORY_ID, group1);
        MockUser userDir2 = setupMockUser("Bob", 2L, group2);

        ApplicationRole role = setupApplication(testAppKey, 5, ImmutableSet.of(group1, group2));
        when(licenseManager.getAllLicensedApplicationKeys()).thenReturn(ImmutableSet.of(testAppKey));

        assertThat("Should count shadowed users once.", manager.totalBillableUsers(), equalTo(1));
    }

    @Test
    public void billableUserCountDoesNotCountShadowedUserInWrongGroup() {
        final Group group1 = new MockGroup("group1");
        final Group group2 = new MockGroup("group2");
        MockUser userDir1 = setupMockUser("Bob", MockUser.MOCK_DIRECTORY_ID, group1);
        MockUser userDir2 = setupMockUser("Bob", 2L, group2);

        ApplicationRole role = setupApplication(testAppKey, 5, ImmutableSet.of(group2));
        when(licenseManager.getAllLicensedApplicationKeys()).thenReturn(ImmutableSet.of(testAppKey));

        assertThat("Should not count shadowed user.", manager.totalBillableUsers(), equalTo(0));
    }

    private ApplicationRole setupApplication(ApplicationKey applicationId, int numSeats, Set<Group> groups) {
        //Make sure the application is licensed.
        definitions.addLicensed(new MockApplicationRoleDefinition(applicationId.value()));
        final MockLicenseDetails details = new MockLicenseDetails();
        details.setLicensedApplications(new MockLicensedApplications(ImmutableMap.of(applicationId, numSeats)));
        boolean newLicenseWasAdded = licenses.add(details);
        assert newLicenseWasAdded;

        // create the role & store
        ApplicationRole role = new MockApplicationRole(applicationId)
                .defaultGroups(groups)
                .groups(groups);

        store.save(role);

        return role;
    }

    private void startSync() {
        when(crowdSyncStatusManager.getDirectorySynchronisationInformation(any(Directory.class)).isSynchronising()).thenReturn(true);
    }

    private void assertClearCache(Runnable clearCache) {
        // When: Software role has software group only.
        store.save(applicationKeySoftware, SOFTWARE_GROUP);

        int activeSoftwareUsers = manager.getUserCount(applicationKeySoftware);
        int billableUsers = manager.totalBillableUsers();

        // Expect to have 3 users (one is inactive and thus not counted)
        assertThat("Active Users with software group only",
                activeSoftwareUsers, equalTo(USERS_ACTIVE_IN_SOFTWARE));

        assertThat("Billable Users are from software group only",
                billableUsers, equalTo(USERS_ACTIVE_IN_SOFTWARE));

        for (ApplicationUser applicationUser : groupManager.getUsersInGroup(CORE_GROUP)) {
            groupManager.addUserToGroup(applicationUser, SOFTWARE_GROUP);
            crowdService.addUserToGroup(applicationUser, SOFTWARE_GROUP);
        }

        //Verify that cache was not flushed.
        activeSoftwareUsers = manager.getUserCount(applicationKeySoftware);
        billableUsers = manager.totalBillableUsers();

        // Expect to still have 3 users
        assertThat("Active Users without Cache clear",
                activeSoftwareUsers, equalTo(USERS_ACTIVE_IN_SOFTWARE));

        assertThat("Billable Users without Cache clear",
                billableUsers, equalTo(USERS_ACTIVE_IN_SOFTWARE));

        clearCache.run();

        activeSoftwareUsers = manager.getUserCount(applicationKeySoftware);
        billableUsers = manager.totalBillableUsers();

        // Expect to have 4 users (extra one came from core)
        assertThat("Active Users after Cache clear",
                activeSoftwareUsers, equalTo(USERS_ACTIVE_UNIQUE_IN_CORE_AND_SOFTWARE));

        assertThat("Billable Users after Cache clear",
                billableUsers, equalTo(USERS_ACTIVE_UNIQUE_IN_CORE_AND_SOFTWARE));
    }

    private void assertNoClearCache(Runnable clearCache) {
        // When: Software role has software group only.
        store.save(applicationKeySoftware, SOFTWARE_GROUP);

        int activeSoftwareUsers = manager.getUserCount(applicationKeySoftware);
        int billableUsers = manager.totalBillableUsers();

        // Expect to have 3 users (one is inactive and thus not counted)
        assertThat("Active Users with software group only",
                activeSoftwareUsers, equalTo(USERS_ACTIVE_IN_SOFTWARE));

        assertThat("Billable Users are only from software group here",
                billableUsers, equalTo(USERS_ACTIVE_IN_SOFTWARE));

        for (ApplicationUser applicationUser : groupManager.getUsersInGroup(CORE_GROUP)) {
            groupManager.addUserToGroup(applicationUser, SOFTWARE_GROUP);
        }

        //Verify that cache was not flushed.
        activeSoftwareUsers = manager.getUserCount(applicationKeySoftware);
        billableUsers = manager.totalBillableUsers();

        // Expect to still have 3 users
        assertThat("Active Users without Cache clear",
                activeSoftwareUsers, equalTo(USERS_ACTIVE_IN_SOFTWARE));

        assertThat("Billable Users without Cache clear",
                billableUsers, equalTo(USERS_ACTIVE_IN_SOFTWARE));

        clearCache.run();

        activeSoftwareUsers = manager.getUserCount(applicationKeySoftware);
        billableUsers = manager.totalBillableUsers();

        // Expect to still have 3 users
        assertThat("Active Users after Cache clear",
                activeSoftwareUsers, equalTo(USERS_ACTIVE_IN_SOFTWARE));

        assertThat("Billable Users after Cache clear",
                billableUsers, equalTo(USERS_ACTIVE_IN_SOFTWARE));
    }

    private void setupLicense(final Map<ApplicationKey, Integer> count) {
        MockLicenseDetails details = new MockLicenseDetails();
        details.setLicensedApplications(new MockLicensedApplications(count));
        when(licenseManager.getLicenses()).thenReturn(ImmutableList.of(details));
        when(licenseManager.getAllLicensedApplicationKeys()).thenReturn(count.keySet());
    }

    private MockUser setupMockUser(String name, long directoryId, boolean active, Group... groups) {
        MockUser user = new MockUser(name);
        user.setDirectoryId(directoryId);
        user.setActive(active);
        crowdService.addUser(user, null);

        for (Group group : groups) {
            crowdService.addUserToGroup(user, group);
            groupManager.addUserToGroup(ApplicationUsers.from(user), group);
        }
        return user;
    }

    private MockUser setupMockUser(String name, long directoryId, Group... groups) {
        return setupMockUser(name, directoryId, true, groups);
    }

    private MockUser setupMockUser(String name, Group... groups) {
        return setupMockUser(name, MockUser.MOCK_DIRECTORY_ID, groups);
    }

    private static User activeUser(String user) {
        return new MockModelUser(user, true);
    }

    private static User disabledUser(String user) {
        return new MockModelUser(user, false);
    }

    private void removeLicense(ApplicationKey applicationKey) {
        definitions.removeLicensed(applicationKey);
        final List<LicenseDetails> licenses = StreamSupport.stream(licenseManager.getLicenses().spliterator(), false)
                .filter(licenseDetails -> !licenseDetails.getLicensedApplications().getKeys().contains(applicationKey))
                .collect(CollectorsUtil.toImmutableList());

        when(licenseManager.getLicenses()).thenReturn(licenses::iterator);

        final Set<ApplicationKey> allLicensedApplicationKeys = licenseManager.getAllLicensedApplicationKeys();
        final Set<ApplicationKey> licensedApplicationKeys = allLicensedApplicationKeys.stream()
                .filter(appKey -> !appKey.equals(applicationKey))
                .collect(CollectorsUtil.toImmutableSet());

        when(allLicensedApplicationKeys).thenReturn(licensedApplicationKeys);
        manager.onClearCache(ClearCacheEvent.INSTANCE);
    }

    private static class MockModelUser implements User {
        private final String name;
        private final boolean active;

        private MockModelUser(final String name, final boolean active) {
            this.name = name;
            this.active = active;
        }

        @Override
        public String getFirstName() {
            return name;
        }

        @Override
        public String getLastName() {
            return name;
        }

        @Override
        public String getExternalId() {
            return name;
        }

        @Override
        public long getDirectoryId() {
            return 0L;
        }

        @Override
        public boolean isActive() {
            return active;
        }

        @Override
        public String getEmailAddress() {
            return name + "@test.com";
        }

        @Override
        public String getDisplayName() {
            return name;
        }

        @Override
        public int compareTo(final com.atlassian.crowd.embedded.api.User user) {
            return name.compareTo(user.getName());
        }

        @Override
        public String getName() {
            return name;
        }
    }
}
