package com.atlassian.jira.notification;

import com.atlassian.jira.bc.ServiceOutcome;
import com.atlassian.jira.event.type.EventType;
import com.atlassian.jira.event.type.EventTypeManager;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.mock.i18n.MockI18nHelper;
import com.atlassian.jira.notification.type.NotificationType;
import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.scheme.Scheme;
import com.atlassian.jira.scheme.SchemeEntity;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.security.roles.ProjectRoleManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.Page;
import com.atlassian.jira.util.PageMatcher;
import com.atlassian.jira.util.PageRequests;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static com.atlassian.jira.notification.type.NotificationType.GROUP;
import static com.atlassian.jira.notification.type.NotificationType.PROJECT_ROLE;
import static com.atlassian.jira.notification.type.NotificationType.SINGLE_USER;
import static com.google.common.collect.Iterables.concat;
import static com.google.common.collect.Lists.newArrayList;
import static java.util.stream.IntStream.range;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class DefaultNotificationSchemeServiceTest {
    @Mock
    private NotificationSchemeManager notificationSchemeManager;
    @Mock
    private EventTypeManager eventTypeManager;
    @Mock
    private UserManager userManager;
    @Mock
    private GroupManager groupManager;
    @Mock
    private ProjectRoleManager projectRoleManager;
    @Mock
    private CustomFieldManager customFieldManager;
    @Mock
    private PermissionManager permissionManager;
    @Mock
    private GlobalPermissionManager globalPermissionManager;
    @Mock
    private ProjectManager projectManager;
    @Mock
    private ApplicationUser user;
    private NotificationSchemeService schemeService;

    @Before
    public void setUp() throws Exception {
        schemeService = new DefaultNotificationSchemeService(notificationSchemeManager,
                eventTypeManager, userManager, groupManager, projectRoleManager, customFieldManager,
                permissionManager, globalPermissionManager, new MockI18nHelper(), projectManager);
    }

    @Test
    public void testGettingNotificationSchemeWithoutPermissionsToAdministerProjectAndGlobalAdminPermissions() {
        long schemeId = 10l;
        createNotificationSchemeMock(schemeId, 0l, "", NotificationType.GROUP, user, false);

        ServiceOutcome<NotificationScheme> notificationScheme = schemeService.getNotificationScheme(user, schemeId);

        assertInvalidServiceOutcome(notificationScheme, ErrorCollection.Reason.NOT_FOUND);
    }

    @Test
    public void testGettingNotificationWithoutAuthentication() {
        ServiceOutcome<NotificationScheme> notificationScheme = schemeService.getNotificationScheme(null, 0l);

        assertInvalidServiceOutcome(notificationScheme, ErrorCollection.Reason.NOT_LOGGED_IN);
    }

    @Test
    public void testGettingNotificationSchemeWhichDoesntExist() {
        long schemeId = 10l;
        when(notificationSchemeManager.getSchemeObject(schemeId)).thenReturn(null);
        ServiceOutcome<NotificationScheme> notificationScheme = schemeService.getNotificationScheme(user, 0l);

        assertInvalidServiceOutcome(notificationScheme, ErrorCollection.Reason.NOT_FOUND);
    }

    @Test
    public void testGettingNotificationScheme() {
        long schemeId = 10l;
        long eventId = 1l;
        String groupname = "groupname";

        createNotificationSchemeMock(schemeId, eventId, groupname, GROUP, user, true);

        ServiceOutcome<NotificationScheme> serviceOutcome = schemeService.getNotificationScheme(user, schemeId);
        assertTrue(serviceOutcome.isValid());

        NotificationScheme notificationScheme = serviceOutcome.getReturnedValue();

        assertThat(notificationScheme, notificationSchemeMatcher(hasItem(eventNotificationMatcher(eventId, Matchers.allOf(
                Matchers.hasProperty("notificationType", is(GROUP)),
                Matchers.hasProperty("parameter", is(groupname))
        )))));
    }

    @Test
    public void testGettingNotificationSchemeWithProjectRole() {
        long schemeId = 10l;
        long eventId = 1l;
        String projectRoleId = "1";

        createNotificationSchemeMock(schemeId, eventId, projectRoleId, PROJECT_ROLE, user, true);

        ServiceOutcome<NotificationScheme> serviceOutcome = schemeService.getNotificationScheme(user, schemeId);
        assertTrue(serviceOutcome.isValid());

        NotificationScheme notificationScheme = serviceOutcome.getReturnedValue();

        assertThat(notificationScheme, notificationSchemeMatcher(hasItem(eventNotificationMatcher(eventId, Matchers.allOf(
                Matchers.hasProperty("notificationType", is(PROJECT_ROLE)),
                Matchers.hasProperty("parameter", is(projectRoleId))
        )))));
    }

    @Test
    public void testGettingNotificationSchemesPage() {
        long eventId = 5l;
        List<Scheme> schemes = mockNotificationSchemeList(range(0, 100), eventId, "param", SINGLE_USER, user, true);
        when(notificationSchemeManager.getSchemeObjects()).thenReturn(schemes);

        Page<NotificationScheme> schemePage = schemeService.getNotificationSchemes(user, PageRequests.request(0L, 10));

        Matcher<EventNotifications> eventNotificationsMatcher = eventNotificationMatcher(eventId, Matchers.allOf(
                Matchers.hasProperty("notificationType", is(SINGLE_USER)),
                Matchers.hasProperty("parameter", is("param"))
        ));

        assertThat(schemePage, PageMatcher.matcher(is(0l), is(100l), is(10), contains(range(0, 10)
                .mapToObj(i -> notificationSchemeMatcher(hasItem(eventNotificationsMatcher)))
                .collect(Collectors.toList()))));
    }

    @Test
    public void testGettingSchemePageWithTooHighOffsetResultsInEmptyPage() {
        long eventId = 5l;
        List<Scheme> schemes = mockNotificationSchemeList(range(0, 100), eventId, "param", SINGLE_USER, user, true);
        when(notificationSchemeManager.getSchemeObjects()).thenReturn(schemes);

        Page<NotificationScheme> schemesPage = schemeService.getNotificationSchemes(user, PageRequests.request(100L, 10));

        assertThat(schemesPage, PageMatcher.matcher(is(100l), is(100l), is(0), Matchers.<NotificationScheme>hasSize(0)));
    }

    @Test
    public void testGettingSchemePageWithoutPermissionsToSomeSchemes() {
        long visibleEventId = 5l;
        List<Scheme> schemesToWhichUserHasPermissions = mockNotificationSchemeList(range(0, 10), visibleEventId, "havePermissions", GROUP, user, true);
        List<Scheme> schemesToWhichUserDoesntHavePermissions = mockNotificationSchemeList(range(0, 10), 6l, "param", SINGLE_USER, user, false);
        when(notificationSchemeManager.getSchemeObjects()).thenReturn(newArrayList(concat(schemesToWhichUserDoesntHavePermissions, schemesToWhichUserHasPermissions)));

        Page<NotificationScheme> schemePage = schemeService.getNotificationSchemes(user, PageRequests.request(0l, 50));

        Matcher<EventNotifications> eventNotificationsMatcher = eventNotificationMatcher(visibleEventId, Matchers.allOf(
                Matchers.hasProperty("notificationType", is(GROUP)),
                Matchers.hasProperty("parameter", is("havePermissions"))
        ));

        Matcher<Iterable<? extends NotificationScheme>> pageValuesMatcher = contains(range(0, 10)
                .mapToObj(i -> notificationSchemeMatcher(hasItem(eventNotificationsMatcher)))
                .collect(Collectors.toList()));

        assertThat(schemePage, PageMatcher.matcher(is(0l), is(10l), is(10), pageValuesMatcher));
    }

    @Test
    public void testGettingOnlyMaximumNumberOfItemsInPage() {
        long eventId = 5l;
        List<Scheme> schemes = mockNotificationSchemeList(range(0, 100), eventId, "havePermissions", GROUP, user, true);
        when(notificationSchemeManager.getSchemeObjects()).thenReturn(schemes);

        Page<NotificationScheme> schemePage = schemeService.getNotificationSchemes(user, PageRequests.request(0l, 100));

        assertThat(schemePage, PageMatcher.matcher(is(0l), is(100l), is(DefaultNotificationSchemeService.MAX_PAGE_RESULT), Matchers.<NotificationScheme>hasSize(DefaultNotificationSchemeService.MAX_PAGE_RESULT)));
    }

    @Test
    public void testGettingNotificationSchemeWhenNoneAssociatedToTheProject() {
        final ServiceOutcome<NotificationScheme> notificationScheme = schemeService.getNotificationScheme(user, 10l);

        assertFalse("Notification Scheme Service Outcome is not valid", notificationScheme.isValid());
        assertEquals(ErrorCollection.Reason.NOT_FOUND, ErrorCollection.Reason.getWorstReason(notificationScheme.getErrorCollection().getReasons()));
    }

    private List<Scheme> mockNotificationSchemeList(IntStream range,
                                                    long eventId,
                                                    String param,
                                                    NotificationType type,
                                                    ApplicationUser user,
                                                    boolean hasPermissions) {
        return range
                .mapToObj(i -> createNotificationSchemeMock(i, eventId, param, type, user, hasPermissions))
                .collect(Collectors.toList());
    }

    private Scheme createNotificationSchemeMock(final long schemeId,
                                                final long eventId,
                                                final String parameter,
                                                final NotificationType notificationType,
                                                final ApplicationUser user,
                                                final boolean hasPermissions) {
        Scheme scheme = mock(Scheme.class);
        Project project = mock(Project.class);

        when(notificationSchemeManager.getSchemeObject(schemeId)).thenReturn(scheme);
        when(scheme.getEntities()).thenReturn(newArrayList(new SchemeEntity(-1l, notificationType.dbCode(), parameter, eventId, null, schemeId)));
        when(notificationSchemeManager.getProjects(scheme)).thenReturn(newArrayList(project));
        when(permissionManager.hasPermission(ProjectPermissions.ADMINISTER_PROJECTS, project, user)).thenReturn(hasPermissions);

        EventType eventType = new EventType(eventId, "name", "description", null);
        when(eventTypeManager.getEventType(eventId)).thenReturn(eventType);
        return scheme;
    }

    private Matcher<NotificationScheme> notificationSchemeMatcher(final Matcher<Iterable<? super EventNotifications>> notificationsMatcher) {
        return new TypeSafeMatcher<NotificationScheme>() {
            @Override
            protected boolean matchesSafely(final NotificationScheme item) {
                return notificationsMatcher.matches(item.getEventNotifications());
            }

            @Override
            public void describeTo(final Description description) {
                description.appendText("Notification scheme ").appendValue(notificationsMatcher);
            }
        };
    }

    private Matcher<EventNotifications> eventNotificationMatcher(final Long eventId,
                                                                 final Matcher<Notification> notificationMatcher) {
        return new TypeSafeMatcher<EventNotifications>() {
            @Override
            protected boolean matchesSafely(final EventNotifications eventNotifications) {
                return Objects.equals(eventNotifications.getEventType().getId(), eventId)
                        && hasItem(notificationMatcher).matches(eventNotifications.getNotifications());
            }

            @Override
            public void describeTo(final Description description) {
                description.appendText("Notification for event")
                        .appendValue(eventId);
            }
        };
    }

    private void assertInvalidServiceOutcome(final ServiceOutcome<NotificationScheme> notificationScheme, final ErrorCollection.Reason reason) {
        assertFalse(notificationScheme.isValid());
        assertThat(notificationScheme.getErrorCollection().getReasons(), hasItem(reason));
    }
}