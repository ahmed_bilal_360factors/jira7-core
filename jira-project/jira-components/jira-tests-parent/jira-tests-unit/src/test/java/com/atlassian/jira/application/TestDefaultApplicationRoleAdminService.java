package com.atlassian.jira.application;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.fugue.Option;
import com.atlassian.jira.bc.ServiceOutcome;
import com.atlassian.jira.bc.ServiceOutcomeMatchers;
import com.atlassian.jira.bc.ValueMatcher;
import com.atlassian.jira.license.JiraLicenseManager;
import com.atlassian.jira.license.LicenseDetails;
import com.atlassian.jira.license.LicensedApplications;
import com.atlassian.jira.mock.security.MockSimpleAuthenticationContext;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.groups.MockGroupManager;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.MockGroup;
import com.atlassian.jira.util.ErrorCollection;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Set;

import static com.atlassian.jira.util.NoopI18nHelper.makeTranslation;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestDefaultApplicationRoleAdminService {
    private static final MockApplicationUser TEST_USER = new MockApplicationUser("User");
    private static final ApplicationKey TEST_APPLICATION_KEY = ApplicationKey.valueOf("Role");
    private static final ApplicationKey NONE_APPLICATION_KEY = ApplicationKey.valueOf("None");

    private static final Group GROUP_1 = new MockGroup("Group 1");
    private static final Group GROUP_2 = new MockGroup("Group 2");
    private static final Group GROUP_3 = new MockGroup("Group 3");
    private static final Group GROUP_BAD = new MockGroup("Bad Group");

    private static final ApplicationRole TEST_ROLE = new MockApplicationRole()
            .key(TEST_APPLICATION_KEY)
            .groups(GROUP_1, GROUP_2, GROUP_3);

    private MockGroupManager groupManager = new MockGroupManager();
    @Mock
    private ApplicationRoleManager applicationRoleManager;
    @Mock
    private GlobalPermissionManager permissionManager;
    @Mock
    private JiraLicenseManager jiraLicenseManager;
    @Mock
    private LicenseDetails licenseDetails;
    @Mock
    private LicensedApplications licensedApplications;

    private JiraAuthenticationContext context =
            MockSimpleAuthenticationContext.createNoopContext(TEST_USER);

    private DefaultApplicationRoleAdminService adminService;

    @Before
    public void setUp() {
        groupManager.addGroup(GROUP_1).addGroup(GROUP_2).addGroup(GROUP_3);
        adminService = new DefaultApplicationRoleAdminService(groupManager, applicationRoleManager, context,
                permissionManager);

        //User is an admin.
        when(permissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, TEST_USER))
                .thenReturn(true);

        when(applicationRoleManager.getRole(TEST_APPLICATION_KEY)).thenReturn(Option.some(TEST_ROLE));
        when(applicationRoleManager.getRole(NONE_APPLICATION_KEY)).thenReturn(Option.none(ApplicationRole.class));
        when(applicationRoleManager.isRoleInstalledAndLicensed(TEST_APPLICATION_KEY)).thenReturn(true);
    }

    @Test
    public void getRolesFailsWithForbiddenForNonUsers() {
        when(permissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, TEST_USER))
                .thenReturn(false);

        assertForbidden(adminService.getRoles());
    }

    @Test
    public void getRolesReturnsRolesCorrectly() {
        final ApplicationRole role1 = new MockApplicationRole()
                .key("idOne")
                .name("name1")
                .groups(GROUP_1, GROUP_2)
                .defaultGroups(GROUP_1);

        final ApplicationRole role2 = new MockApplicationRole()
                .key("idTwo")
                .name("name2");

        when(applicationRoleManager.getRoles())
                .thenReturn(Sets.newHashSet(role1, role2));

        final ServiceOutcome<Set<ApplicationRole>> roles = adminService.getRoles();
        final ValueMatcher<Set<ApplicationRole>> matcher = ServiceOutcomeMatchers.equalTo(
                Matchers.containsInAnyOrder(new ApplicationRoleMatcher().merge(role1), new ApplicationRoleMatcher().merge(role2)));

        assertThat(roles, matcher);
    }

    @Test
    public void getRoleFailsWithForbiddenForNonUsers() {
        when(permissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, TEST_USER))
                .thenReturn(false);

        final ServiceOutcome<ApplicationRole> outsome = adminService.getRole(ApplicationKey.valueOf("w"));
        assertForbidden(outsome);
    }

    @Test
    public void getRoleReturnsRoleThatExistsAndFiltersInvalidGroups() {
        final ServiceOutcome<ApplicationRole> role = adminService.getRole(TEST_APPLICATION_KEY);

        //Need the explict Type for JDK6.
        assertThat(role, ServiceOutcomeMatchers.equalTo(new ApplicationRoleMatcher().merge(TEST_ROLE)));
    }

    @Test
    public void getRoleReturnsErrorOnRoleThatDoesNotExist() {
        final ServiceOutcome<ApplicationRole> role = adminService.getRole(NONE_APPLICATION_KEY);
        assertRoleDoesNotExist(NONE_APPLICATION_KEY, role);
    }

    @Test
    public void setRoleFailsForNonAdmin() {
        //User is an admin.
        when(permissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, TEST_USER))
                .thenReturn(false);

        assertForbidden(adminService.setRole(TEST_ROLE));
    }

    @Test
    public void setRoleFailsWhenRoleDoesNotExist() {
        final MockApplicationRole iDontExist = new MockApplicationRole().key(NONE_APPLICATION_KEY);
        final ServiceOutcome<ApplicationRole> outcome = adminService.setRole(iDontExist);
        assertRoleDoesNotExist(NONE_APPLICATION_KEY, outcome);
    }

    @Test
    public void setRoleFailsWhenGroupsDontExist() {
        ApplicationRole expectedRole = TEST_ROLE.withGroups(groups(GROUP_BAD), groups());

        //Save the groups.
        final ServiceOutcome<ApplicationRole> outcome = adminService.setRole(expectedRole);

        assertThat(outcome, ServiceOutcomeMatchers.errorMatcher()
                .addReason(ErrorCollection.Reason.VALIDATION_FAILED)
                .addError("groups", makeTranslation("application.role.service.group.does.not.exist", GROUP_BAD.getName())));
    }

    @Test
    public void setRoleSavesWhenGroupsExist() {
        ApplicationRole expectedRole = TEST_ROLE.withGroups(groups(GROUP_1, GROUP_2), groups());
        final ApplicationRoleMatcher expectedMatcher = new ApplicationRoleMatcher().merge(expectedRole);

        //All groups exist.
        when(applicationRoleManager.setRole(Mockito.argThat(expectedMatcher))).thenReturn(expectedRole);

        //Save the groups.
        final ServiceOutcome<ApplicationRole> outcome = adminService.setRole(expectedRole);

        //Have the groups been saved.
        verify(applicationRoleManager).setRole(Mockito.argThat(expectedMatcher));
        assertThat(outcome, ServiceOutcomeMatchers.equalTo(expectedMatcher));
    }

    @Test
    public void setRolesFailsForNonAdmin() {
        //User is an admin.
        when(permissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, TEST_USER))
                .thenReturn(false);

        assertForbidden(adminService.setRoles(Lists.newArrayList(TEST_ROLE)));
    }

    @Test
    public void setRolesFailsWhenRoleDoesNotExist() {
        final MockApplicationRole iDontExist = new MockApplicationRole().key(NONE_APPLICATION_KEY);
        final ServiceOutcome<Set<ApplicationRole>> outcome = adminService.setRoles(Lists.newArrayList(iDontExist));
        assertRoleDoesNotExist(NONE_APPLICATION_KEY, outcome);
    }

    @Test
    public void setRolesFailsWhenGroupsDontExist() {
        ApplicationRole expectedRole = TEST_ROLE.withGroups(groups(GROUP_BAD), groups());

        //Save the groups.
        final ServiceOutcome<Set<ApplicationRole>> outcome = adminService.setRoles(Lists.newArrayList(expectedRole));

        assertThat(outcome, ServiceOutcomeMatchers.errorMatcher()
                .addReason(ErrorCollection.Reason.VALIDATION_FAILED)
                .addError("groups", makeTranslation("application.role.service.group.does.not.exist", GROUP_BAD.getName())));
    }

    private void assertRoleDoesNotExist(final ApplicationKey key, final ServiceOutcome<?> role) {
        assertThat(role, ServiceOutcomeMatchers.errorMatcher()
                .addErrorMessage(makeTranslation("application.role.service.role.does.not.exist", key.value()))
                .addReason(ErrorCollection.Reason.NOT_FOUND));
    }

    private void assertForbidden(final ServiceOutcome<?> outcome) {
        assertThat(outcome, ServiceOutcomeMatchers.errorMatcher()
                .addErrorMessage(makeTranslation("application.role.service.permission.denied"))
                .addReason(ErrorCollection.Reason.FORBIDDEN));
    }

    private static Set<Group> groups(Group... groups) {
        return ImmutableSet.copyOf(groups);
    }

    private static Set<Group> groups() {
        return ImmutableSet.of();
    }
}
