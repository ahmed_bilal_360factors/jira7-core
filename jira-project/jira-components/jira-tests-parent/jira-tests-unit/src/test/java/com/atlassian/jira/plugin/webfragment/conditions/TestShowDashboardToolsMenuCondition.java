package com.atlassian.jira.plugin.webfragment.conditions;

import com.atlassian.gadgets.dashboard.DashboardId;
import com.atlassian.gadgets.dashboard.spi.DashboardPermissionService;
import com.atlassian.jira.bc.portal.PortalPageService;
import com.atlassian.jira.junit.rules.InitMockitoMocks;
import com.atlassian.jira.portal.PortalPage;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.util.UserUtil;
import com.google.common.collect.ImmutableMap;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.util.Map;

import static com.atlassian.gadgets.dashboard.DashboardId.valueOf;
import static com.atlassian.jira.portal.PortalPage.name;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

public class TestShowDashboardToolsMenuCondition {
    public static final Long ID = 10000l;
    public static final DashboardId DASHBOARD_ID = valueOf(Long.toString(ID));

    @Rule
    public TestRule initMocks = new InitMockitoMocks(this);

    @Mock
    private UserUtil userUtil;
    @Mock
    private PortalPageService portalPageService;
    @Mock
    private DashboardPermissionService permissionService;
    @InjectMocks
    private ShowDashboardToolsMenuCondition showDashboardToolsMenuCondition;

    @Before
    public void setUp() {
        final PortalPage mockPortalPage = name("Some page").id(ID).build();
        when(portalPageService.getSystemDefaultPortalPage()).thenReturn(mockPortalPage);
        when(userUtil.getUserByName("admin")).thenReturn(new MockApplicationUser("admin"));
    }

    @Test
    public void testShouldDisplayNonDefaultDashboard() {
        final Map<String, Object> context = ImmutableMap.of("username", "admin", "dashboardId", valueOf(Long.toString(10020)));
        final boolean display = showDashboardToolsMenuCondition.shouldDisplay(context);

        assertTrue(display);
    }

    @Test
    public void testShouldDisplayNonDefaultDashboardNotLoggedIn() {
        final Map<String, Object> context = ImmutableMap.of("dashboardId", valueOf(Long.toString(10020)));
        final boolean display = showDashboardToolsMenuCondition.shouldDisplay(context);

        assertFalse(display);
    }

    @Test
    public void testShouldDisplayDefaultDashboardInAdminSection() {
        when(permissionService.isWritableBy(DASHBOARD_ID, "admin")).thenReturn(true);

        final Map<String, Object> context = ImmutableMap.of("username", "admin", "dashboardId", DASHBOARD_ID);
        final boolean display = showDashboardToolsMenuCondition.shouldDisplay(context);

        assertFalse(display);
    }

    @Test
    public void testShouldDisplayDefaultDashboardOnHomeScreen() {
        when(permissionService.isWritableBy(DASHBOARD_ID, "admin")).thenReturn(false);

        final Map<String, Object> context = ImmutableMap.of("username", "admin", "dashboardId", DASHBOARD_ID);
        final boolean display = showDashboardToolsMenuCondition.shouldDisplay(context);
        assertTrue(display);
    }
}
