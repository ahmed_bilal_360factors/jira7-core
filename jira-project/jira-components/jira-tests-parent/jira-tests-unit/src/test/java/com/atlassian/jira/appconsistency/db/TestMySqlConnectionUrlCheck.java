package com.atlassian.jira.appconsistency.db;

import com.atlassian.jira.junit.rules.Log4jLogger;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

public class TestMySqlConnectionUrlCheck {
    @Rule
    public MethodRule useMockito = MockitoJUnit.rule();
    @Rule
    public final Log4jLogger log4jLogger = new Log4jLogger();

    @Mock
    private MySqlConnectionUrlCheck.ConfigurationState configurationState;

    private MySqlConnectionUrlCheck mySqlConnectionUrlCheck;

    @Before
    public void before() throws Exception {
        mySqlConnectionUrlCheck = spy(new MySqlConnectionUrlCheck(configurationState));
        doReturn("").when(mySqlConnectionUrlCheck).getDocumentationLink();
    }

    @Test
    public void testShouldBeOkIfUriUnavailable() throws  Exception {
        when(configurationState.isConfigurationAvailable()).thenReturn(true);
        when(configurationState.isMySql()).thenReturn(true);
        when(configurationState.getConnectionUri()).thenReturn(Optional.empty());

        final boolean isOk = mySqlConnectionUrlCheck.isOk();

        assertTrue(isOk);
    }

    @Test
    public void testShouldBeOkIfConfigurationUnavailable() throws Exception {
        when(configurationState.isConfigurationAvailable()).thenReturn(false);

        final boolean isOk = mySqlConnectionUrlCheck.isOk();

        assertTrue(isOk);
    }

    @Test
    public void testShouldBeOkForNotMySqlDatabase() throws Exception {
        when(configurationState.isConfigurationAvailable()).thenReturn(true);
        when(configurationState.isMySql()).thenReturn(false);

        final boolean isOk = mySqlConnectionUrlCheck.isOk();

        assertTrue(isOk);
    }

    @Test
    public void testShouldBeOkIfConfigurationIsOK() throws Exception {
        when(configurationState.isConfigurationAvailable()).thenReturn(true);
        when(configurationState.isMySql()).thenReturn(true);
        when(configurationState.isUriMySql57Compatible(anyString())).thenReturn(true);
        when(configurationState.getConnectionUri()).thenReturn(Optional.of(""));

        final boolean isOk = mySqlConnectionUrlCheck.isOk();

        assertTrue(isOk);
    }

    @Test
    public void testShouldWarnIfConfigurationIsOutdatedButWorks() throws Exception {
        when(configurationState.isConfigurationAvailable()).thenReturn(true);
        when(configurationState.isMySql()).thenReturn(true);
        when(configurationState.isUriMySql57Compatible(anyString())).thenReturn(false);
        when(configurationState.getConnectionUri()).thenReturn(Optional.of(""));
        doReturn(true).when(mySqlConnectionUrlCheck).canWorkWithCurrentConfiguration();

        final boolean isOk = mySqlConnectionUrlCheck.isOk();

        assertTrue(isOk);
        assertThat(log4jLogger.getMessage(),
                containsString(mySqlConnectionUrlCheck.getFaultDescription()));
    }

    @Test
    public void testShouldFailIfConfigurationDoesntWork() throws Exception {
        when(configurationState.isConfigurationAvailable()).thenReturn(true);
        when(configurationState.isMySql()).thenReturn(true);
        when(configurationState.isUriMySql57Compatible(anyString())).thenReturn(false);
        when(configurationState.getConnectionUri()).thenReturn(Optional.of(""));
        doReturn(false).when(mySqlConnectionUrlCheck).canWorkWithCurrentConfiguration();

        final boolean isOk = mySqlConnectionUrlCheck.isOk();

        assertFalse(isOk);
    }

    @Test
    public void testCorrectUri() {
        final String correctUri = "jdbc:mysql://localhost:5577/jira?useUnicode=true&characterEncoding=UTF8&sessionVariables=default_storage_engine=InnoDB";
        final MySqlConnectionUrlCheck.ConfigurationState configurationState = new MySqlConnectionUrlCheck.ConfigurationState(null);

        final boolean isCorrect = configurationState.isUriMySql57Compatible(correctUri);

        assertTrue(isCorrect);
    }

    @Test
    public void testIncorrectUri() {
        final String correctUri = "jdbc:mysql://localhost:5577/jira?useUnicode=true&characterEncoding=UTF8&sessionVariables=storage_engine=InnoDB";
        final MySqlConnectionUrlCheck.ConfigurationState configurationState = new MySqlConnectionUrlCheck.ConfigurationState(null);

        final boolean isCorrect = configurationState.isUriMySql57Compatible(correctUri);

        assertFalse(isCorrect);
    }

    @Test
    public void testMissingParameterInUri() {
        final String correctUri = "jdbc:mysql://localhost:5577/jira?useUnicode=true&characterEncoding=UTF8";
        final MySqlConnectionUrlCheck.ConfigurationState configurationState = new MySqlConnectionUrlCheck.ConfigurationState(null);

        final boolean isCorrect = configurationState.isUriMySql57Compatible(correctUri);

        assertFalse(isCorrect);
    }
}