package com.atlassian.jira.upgrade;

import com.atlassian.core.util.ClassLoaderUtils;
import com.atlassian.jira.JiraUnitTestProperties;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import electric.xml.Element;
import electric.xml.Elements;
import electric.xml.ParseException;
import org.apache.commons.io.IOUtils;
import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;
import java.io.File;
import java.io.InputStream;
import java.nio.file.Path;
import java.util.SortedSet;
import java.util.TreeSet;

import static com.atlassian.jira.matchers.FileMatchers.isFile;
import static org.junit.Assert.assertThat;

/**
 * Check that the version of the latest upgrade task in the upgrades.xml is less than or equal to the jira.build.number
 * in the distribution and root poms
 *
 * @since 6.4
 */
public class TestUpgradeVersionNumbers {
    @Rule
    public RuleChain ruleChain = MockitoMocksInContainer.forTest(this);

    private static final String UPGRADES_XML = "upgrades.xml";
    private final File parentPom;
    private final File distributionPom;
    private final Integer latestBuildNo;
    private final DocumentBuilder documentBuilder;
    private final XPathFactory xPathFactory = XPathFactory.newInstance();

    public TestUpgradeVersionNumbers() throws ParserConfigurationException {
        final Path rootPath = JiraUnitTestProperties.getProjectRootPath();
        parentPom = rootPath.resolve("pom.xml").toFile();
        distributionPom = rootPath.resolve("jira-distribution/pom.xml").toFile();
        latestBuildNo = getUpgradeTasks().last();
        documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
    }

    @Test
    public void testLatestUpgradeTaskAgainstDistPom() {
        String distVersionNo = getJiraVersionNumberFromPom(distributionPom);
        assertThat(latestBuildNo, BuildNumberMatcher.lessThatEqualTo(distVersionNo));
    }

    @Test
    public void testLatestUpgradeTaskAgainstRootPom() {
        String rootVersionNo = getJiraVersionNumberFromPom(parentPom);
        assertThat(latestBuildNo, BuildNumberMatcher.lessThatEqualTo(rootVersionNo));
    }

    private String getJiraVersionNumberFromPom(final File pomFile) {
        assertThat(pomFile, isFile());
        try {
            final XPathExpression xpath = xPathFactory.newXPath().compile("/project/properties");

            final Document doc = documentBuilder.parse(pomFile);
            doc.getDocumentElement().normalize();
            final Node propsNode = (Node) xpath.evaluate(doc, XPathConstants.NODE);
            final NodeList props = propsNode.getChildNodes();

            for (int i = 0; i < props.getLength(); i++) {
                final Node node = props.item(i);
                if (node.getNodeType() == Node.ELEMENT_NODE && node.getNodeName().equals("jira.build.number")) {
                    return node.getTextContent();
                }
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return null;
    }

    private SortedSet<Integer> getUpgradeTasks() {
        SortedSet<Integer> sortedTasks = new TreeSet<>();

        final InputStream is = ClassLoaderUtils.getResourceAsStream(UPGRADES_XML, this.getClass());
        try {
            final electric.xml.Document doc = new electric.xml.Document(is);
            final Element root = doc.getRoot();
            final Elements actions = root.getElements("upgrade");

            while (actions.hasMoreElements()) {
                final Element action = (Element) actions.nextElement();
                final Integer buildNo = Integer.parseInt(action.getAttribute("build"));

                sortedTasks.add(buildNo);
            }
        } catch (ParseException e) {
            throw new RuntimeException(e);
        } finally {
            IOUtils.closeQuietly(is);
        }
        return sortedTasks;
    }

    public static class BuildNumberMatcher {
        @Factory
        public static Matcher<Integer> lessThatEqualTo(final String buildNo) {
            return new TypeSafeMatcher<Integer>() {
                @Override
                protected boolean matchesSafely(final Integer item) {
                    return item <= Integer.parseInt(buildNo);
                }

                @Override
                public void describeTo(final Description description) {
                    description.appendValue(buildNo);
                }
            };
        }
    }
}
