package com.atlassian.jira.security.auth;

import com.atlassian.jira.application.ApplicationRoleManager;
import com.atlassian.jira.mock.servlet.MockHttpServletRequest;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.plugin.webwork.WebworkPluginSecurityServiceHelper;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.plugin.PluginAccessor;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Sets;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

/**
 * Tests {@link com.atlassian.jira.security.auth.AuthorisationManagerImpl}.
 */
@RunWith(MockitoJUnitRunner.class)
public class AuthorisationManagerImplTest {
    @Mock
    PluginAccessor pluginAccessor;
    @Mock
    ApplicationRoleManager applicationRoleManager;
    @Mock
    PermissionManager permissionManager;
    @Mock
    WebworkPluginSecurityServiceHelper webworkPluginSecurityServiceHelper;
    @Mock
    GlobalPermissionManager globalPermissions;

    private AuthorisationManagerImpl authorisationManager;
    private ApplicationUser userGood;
    private ApplicationUser userBad;
    private ApplicationUser userAdmin;
    private MockHttpServletRequest httpServletRequest;
    @Mock
    private AuthorisationModuleDescriptor positiveAuth;
    @Mock
    private AuthorisationModuleDescriptor negativeAuth;
    @Mock
    private AuthorisationModuleDescriptor abstainAuth;
    @Mock
    private AuthorisationModuleDescriptor exceptionAuth;

    @Before
    public void setUp() throws Exception {
        userGood = new MockApplicationUser("userGood");
        userBad = new MockApplicationUser("userBad");
        userAdmin = new MockApplicationUser("userAdmin");
        httpServletRequest = new MockHttpServletRequest();

        authorisationManager = new AuthorisationManagerImpl(permissionManager, pluginAccessor,
                webworkPluginSecurityServiceHelper, applicationRoleManager, globalPermissions);

        when(exceptionAuth.getModule()).thenReturn(new Authorisation() {
            @Override
            public Decision authoriseForLogin(@Nonnull ApplicationUser user, HttpServletRequest httpServletRequest) {
                throw new RuntimeException("authoriseForLogin!");
            }

            @Override
            public Set<String> getRequiredRoles(HttpServletRequest httpServletRequest) {
                throw new RuntimeException("getRequiredRoles!");
            }

            @Override
            public Decision authoriseForRole(@Nullable ApplicationUser user, HttpServletRequest httpServletRequest, String role) {
                throw new RuntimeException("authoriseForRole!");
            }
        });


        when(applicationRoleManager.hasAnyRole(userGood)).thenReturn(true);
        when(permissionManager.hasPermission(Permissions.SYSTEM_ADMIN, userGood)).thenReturn(true);
        when(globalPermissions.hasPermission(GlobalPermissionKey.SYSTEM_ADMIN, userGood)).thenReturn(true);

        when(applicationRoleManager.hasAnyRole(userAdmin)).thenReturn(false);
        when(permissionManager.hasPermission(Permissions.ADMINISTER, userAdmin)).thenReturn(true);
        when(globalPermissions.hasPermission(GlobalPermissionKey.ADMINISTER, userAdmin)).thenReturn(true);

        when(applicationRoleManager.hasAnyRole(userBad)).thenReturn(false);
        when(permissionManager.hasPermission(Permissions.ADMINISTER, userBad)).thenReturn(false);
        when(globalPermissions.hasPermission(GlobalPermissionKey.ADMINISTER, userBad)).thenReturn(false);


        when(positiveAuth.getModule()).thenReturn(new Authorisation() {
            @Override
            public Decision authoriseForLogin(@Nonnull ApplicationUser user, HttpServletRequest httpServletRequest) {
                return Decision.GRANTED;
            }

            @Override
            public Set<String> getRequiredRoles(HttpServletRequest httpServletRequest) {
                return Sets.newHashSet("positive");
            }

            @Override
            public Decision authoriseForRole(@Nullable ApplicationUser user, HttpServletRequest httpServletRequest, String role) {
                if ("positive".equals(role)) {
                    return Decision.GRANTED;
                }
                return Decision.ABSTAIN;
            }
        });
        when(negativeAuth.getModule()).thenReturn(new Authorisation() {
            @Override
            public Decision authoriseForLogin(@Nonnull ApplicationUser user, HttpServletRequest httpServletRequest) {
                return Decision.DENIED;
            }

            @Override
            public Set<String> getRequiredRoles(HttpServletRequest httpServletRequest) {
                return Sets.newHashSet();
            }

            @Override
            public Decision authoriseForRole(@Nullable ApplicationUser user, HttpServletRequest httpServletRequest, String role) {
                return Decision.DENIED;
            }
        });
        when(abstainAuth.getModule()).thenReturn(new Authorisation() {
            @Override
            public Decision authoriseForLogin(@Nonnull ApplicationUser user, HttpServletRequest httpServletRequest) {
                return Decision.ABSTAIN;
            }

            @Override
            public Set<String> getRequiredRoles(HttpServletRequest httpServletRequest) {
                return Sets.newHashSet();
            }

            @Override
            public Decision authoriseForRole(@Nullable ApplicationUser user, HttpServletRequest httpServletRequest, String role) {
                return Decision.ABSTAIN;
            }
        });
    }

    @Test
    public void testBasicJiraAuthorisationNoPlugins() throws Exception {
        assertTrue(authorisationManager.authoriseForLogin(userGood, httpServletRequest));
        assertTrue(authorisationManager.authoriseForLogin(userAdmin, httpServletRequest));
        assertFalse(authorisationManager.authoriseForLogin(userBad, httpServletRequest));

        assertTrue(authorisationManager.hasUserAccessToJIRA(userGood));
        assertTrue(authorisationManager.hasUserAccessToJIRA(userAdmin));
        assertFalse(authorisationManager.hasUserAccessToJIRA(userBad));
    }

    @Test
    public void administratorsCanAlwaysLogin() throws Exception {
        when(globalPermissions.hasPermission(GlobalPermissionKey.ADMINISTER, userAdmin))
                .thenReturn(true);

        assertTrue(authorisationManager.authoriseForLogin(userAdmin, httpServletRequest));
        assertTrue(authorisationManager.hasUserAccessToJIRA(userAdmin));
    }

    @Test
    public void systemAdministratorsCanAlwaysLogin() throws Exception {
        when(globalPermissions.hasPermission(GlobalPermissionKey.ADMINISTER, userAdmin))
                .thenReturn(true);

        assertTrue(authorisationManager.authoriseForLogin(userAdmin, httpServletRequest));
        assertTrue(authorisationManager.hasUserAccessToJIRA(userAdmin));
    }

    @Test
    public void usersCanLoginWhenAssociatedWithRoles() throws Exception {
        assertTrue(authorisationManager.authoriseForLogin(userGood, httpServletRequest));
        assertTrue(authorisationManager.hasUserAccessToJIRA(userGood));
    }

    @Test
    public void usersCanNotLoginWhenNotAssociatedWithRoles() throws Exception {
        assertFalse(authorisationManager.authoriseForLogin(userBad, httpServletRequest));
        assertFalse(authorisationManager.hasUserAccessToJIRA(userBad));
    }

    @Test
    public void testPluginProvidedAbstainAuthorisation() throws Exception {
        when(pluginAccessor.getEnabledModuleDescriptorsByClass(AuthorisationModuleDescriptor.class)).thenReturn(
                ImmutableList.of(abstainAuth, abstainAuth, abstainAuth));
        assertTrue(authorisationManager.authoriseForLogin(userGood, httpServletRequest));
        assertTrue(authorisationManager.authoriseForLogin(userAdmin, httpServletRequest));
        assertFalse(authorisationManager.authoriseForLogin(userBad, httpServletRequest));
    }

    @Test
    public void testPluginProvidedNegativeAuthorisation() throws Exception {
        final List<AuthorisationModuleDescriptor> authorisationList = new ArrayList<>();
        when(pluginAccessor.getEnabledModuleDescriptorsByClass(AuthorisationModuleDescriptor.class)).thenReturn(authorisationList);

        // abstain does nothing
        authorisationList.add(abstainAuth);
        assertFalse(authorisationManager.authoriseForLogin(userBad, httpServletRequest));

        // a neg auth is in play
        authorisationList.add(negativeAuth);
        assertFalse(authorisationManager.authoriseForLogin(userBad, httpServletRequest));

        // abstain does nothing
        authorisationList.add(abstainAuth);
        assertFalse(authorisationManager.authoriseForLogin(userBad, httpServletRequest));

        // a positive auth here is too late
        authorisationList.add(positiveAuth);
        assertFalse(authorisationManager.authoriseForLogin(userBad, httpServletRequest));

    }

    @Test
    public void testPluginProvidedPositiveAuthorisation() throws Exception {
        final List<AuthorisationModuleDescriptor> authorisationList = new ArrayList<>();
        when(pluginAccessor.getEnabledModuleDescriptorsByClass(AuthorisationModuleDescriptor.class)).thenReturn(authorisationList);
        // abstain does nothing
        authorisationList.add(abstainAuth);
        assertFalse(authorisationManager.authoriseForLogin(userBad, httpServletRequest));

        // abstain does nothing
        authorisationList.add(abstainAuth);
        assertFalse(authorisationManager.authoriseForLogin(userBad, httpServletRequest));

        // a positive auth here is a yes
        authorisationList.add(positiveAuth);
        assertTrue(authorisationManager.authoriseForLogin(userBad, httpServletRequest));

        // a neg auth here doesnt matter
        authorisationList.add(negativeAuth);
        assertTrue(authorisationManager.authoriseForLogin(userBad, httpServletRequest));

    }

    @Test
    public void testPluginAbstainersHaveNoEffect() throws Exception {
        // abstain always is neutral
        when(pluginAccessor.getEnabledModuleDescriptorsByClass(AuthorisationModuleDescriptor.class)).thenReturn(
                ImmutableList.of(abstainAuth, abstainAuth, abstainAuth));

        assertFalse(authorisationManager.authoriseForLogin(userBad, httpServletRequest));
        assertTrue(authorisationManager.authoriseForLogin(userAdmin, httpServletRequest));
        assertTrue(authorisationManager.authoriseForLogin(userGood, httpServletRequest));
    }

    @Test
    public void testPluginExceptionsHaveNoEffect() throws Exception {
        // abstain always is neutral
        when(pluginAccessor.getEnabledModuleDescriptorsByClass(AuthorisationModuleDescriptor.class)).thenReturn(
                ImmutableList.of(exceptionAuth, abstainAuth, exceptionAuth));

        assertFalse(authorisationManager.authoriseForLogin(userBad, httpServletRequest));
        assertTrue(authorisationManager.authoriseForLogin(userAdmin, httpServletRequest));
        assertTrue(authorisationManager.authoriseForLogin(userGood, httpServletRequest));
    }

    @Test
    public void testWebworkRolesAreReturned() {

        HashSet<String> webworkRoles = Sets.newHashSet("webworksupplied", "webworkprovided");

        when(webworkPluginSecurityServiceHelper.getRequiredRoles(httpServletRequest)).thenReturn(webworkRoles);
        when(pluginAccessor.getEnabledModuleDescriptorsByClass(AuthorisationModuleDescriptor.class)).thenReturn(
                ImmutableList.of(positiveAuth, negativeAuth, abstainAuth));

        Set<String> actual = authorisationManager.getRequiredRoles(httpServletRequest);
        assertEquals(actual.size(), 3);
        for (String webworkRole : webworkRoles) {
            assertTrue(actual.contains(webworkRole));
        }
        assertTrue(actual.contains("positive"));
    }

    @Test
    public void testExceptionsFromPluginsAreignored() {

        when(pluginAccessor.getEnabledModuleDescriptorsByClass(AuthorisationModuleDescriptor.class)).thenReturn(
                ImmutableList.of(positiveAuth, negativeAuth, abstainAuth, exceptionAuth));

        Set<String> actual = authorisationManager.getRequiredRoles(httpServletRequest);
        assertEquals(actual.size(), 1);
        assertEquals(actual.iterator().next(), "positive");
    }


    @Test
    public void testHasJIRARole_FAIL() {
        when(permissionManager.hasPermission(Permissions.USE, userBad)).thenReturn(false);
        assertFalse(authorisationManager.authoriseForRole(userBad, httpServletRequest, "use"));
    }

    @Test
    public void testHasJIRARole_FAIL_RubbishRoleName() {
        assertFalse(authorisationManager.authoriseForRole(userBad, httpServletRequest, "rubbish"));
    }

    @Test
    public void testHasJIRARole_OK() {
        when(permissionManager.hasPermission(Permissions.USE, userGood)).thenReturn(true);
        assertTrue(authorisationManager.authoriseForRole(userGood, httpServletRequest, "use"));
    }

    @Test
    public void testPluginProvidedRoleChecks() {
        when(pluginAccessor.getEnabledModuleDescriptorsByClass(AuthorisationModuleDescriptor.class)).thenReturn(
                ImmutableList.of(positiveAuth));

        assertTrue(authorisationManager.authoriseForRole(userGood, httpServletRequest, "positive"));
    }

    @Test
    public void testPluginProvidedRoleChecksBeatJIRA() {
        when(pluginAccessor.getEnabledModuleDescriptorsByClass(AuthorisationModuleDescriptor.class)).thenReturn(
                ImmutableList.of(positiveAuth));

        assertTrue(authorisationManager.authoriseForRole(userBad, httpServletRequest, "positive"));
    }

    @Test
    public void testPluginProvidedRoleCheckExceptionsHaveNoEffect() {
        when(pluginAccessor.getEnabledModuleDescriptorsByClass(AuthorisationModuleDescriptor.class)).thenReturn(
                ImmutableList.of(exceptionAuth));
        when(pluginAccessor.getEnabledModuleDescriptorsByClass(AuthorisationModuleDescriptor.class)).thenReturn(
                ImmutableList.of(positiveAuth));

        assertTrue(authorisationManager.authoriseForRole(userGood, httpServletRequest, "positive"));
    }

}
