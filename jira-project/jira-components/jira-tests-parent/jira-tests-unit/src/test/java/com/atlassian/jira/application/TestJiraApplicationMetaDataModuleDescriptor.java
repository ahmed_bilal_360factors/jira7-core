package com.atlassian.jira.application;

import com.atlassian.jira.project.type.ProjectType;
import com.atlassian.jira.project.type.ProjectTypeKey;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginInformation;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.PluginState;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugin.osgi.factory.OsgiPlugin;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.osgi.framework.Bundle;

import javax.annotation.Nullable;
import java.io.StringReader;
import java.util.Hashtable;
import java.util.Iterator;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestJiraApplicationMetaDataModuleDescriptor {
    @Test
    public void moduleDescriptorContainsNoProjectTypesIfXMLDoesNotHaveProjectTypesTag() {
        String application =
                "<application key=\"completeModule\" name=\"ModuleDescriptorName\">\n" +
                        "<applicationKey>com.atlassian.jira.platform</applicationKey>\n" +
                        "<applicationName>Test Product</applicationName>\n" +
                        "<applicationDescriptionKey>some.key</applicationDescriptionKey>\n" +
                        "<userCountKey>user.count.key</userCountKey>\n" +
                        "<defaultGroup>test-group</defaultGroup>\n" +
                        "</application>";

        JiraPluginApplicationMetaData metadata = parse(application);

        assertThat(metadata.getProjectTypes(), Matchers.<ProjectType>iterableWithSize(0));
    }

    @Test
    public void moduleDescriptorContainsNoProjectTypesIfXMLContainsEmptyProjectTypesTag() {
        String application =
                "<application key=\"completeModule\" name=\"ModuleDescriptorName\">\n" +
                        "<applicationKey>com.atlassian.jira.platform</applicationKey>\n" +
                        "<applicationName>Test Product</applicationName>\n" +
                        "<applicationDescriptionKey>some.key</applicationDescriptionKey>\n" +
                        "<userCountKey>user.count.key</userCountKey>\n" +
                        "<defaultGroup>test-group</defaultGroup>\n" +
                        "<projectTypes></projectTypes>\n" +
                        "</application>";

        JiraPluginApplicationMetaData metadata = parse(application);

        assertThat(metadata.getProjectTypes(), Matchers.<ProjectType>iterableWithSize(0));
    }

    @Test
    public void moduleDescriptorParsesSingleProjectTypeCorrectly() {
        String application =
                "<application key=\"completeModule\" name=\"ModuleDescriptorName\">\n" +
                        "<applicationKey>com.atlassian.jira.platform</applicationKey>\n" +
                        "<applicationName>Test Product</applicationName>\n" +
                        "<applicationDescriptionKey>some.key</applicationDescriptionKey>\n" +
                        "<userCountKey>user.count.key</userCountKey>\n" +
                        "<defaultGroup>test-group</defaultGroup>\n" +
                        "<projectTypes>\n" +
                        "<projectType>\n" +
                        "<key>key</key>\n" +
                        "<descriptionI18nKey>descriptionI18nKey</descriptionI18nKey>\n" +
                        "<icon>icon</icon>\n" +
                        "<color>color</color>\n" +
                        "<weight>10</weight>\n" +
                        "</projectType>\n" +
                        "</projectTypes>\n" +
                        "</application>";

        JiraPluginApplicationMetaData metadata = parse(application);

        assertThat(metadata.getProjectTypes(), Matchers.<ProjectType>iterableWithSize(1));
        ProjectType projectType = metadata.getProjectTypes().iterator().next();
        assertProjectTypeIsCorrect(projectType, "key", "descriptionI18nKey", "icon", "color", 10);
    }

    @Test(expected = PluginParseException.class)
    public void shouldThrowExceptionWhenWeightIsNotNumeric() {
        String application =
                "<application key=\"completeModule\" name=\"ModuleDescriptorName\">\n" +
                        "<applicationKey>com.atlassian.jira.platform</applicationKey>\n" +
                        "<applicationName>Test Product</applicationName>\n" +
                        "<applicationDescriptionKey>some.key</applicationDescriptionKey>\n" +
                        "<userCountKey>user.count.key</userCountKey>\n" +
                        "<defaultGroup>test-group</defaultGroup>\n" +
                        "<projectTypes>\n" +
                        "<projectType>\n" +
                        "<key>key</key>\n" +
                        "<descriptionI18nKey>descriptionI18nKey</descriptionI18nKey>\n" +
                        "<icon>icon</icon>\n" +
                        "<color>color</color>\n" +
                        "<weight>dfsdf10dfsf</weight>\n" +
                        "</projectType>\n" +
                        "</projectTypes>\n" +
                        "</application>";

        parse(application);
    }

    @Test
    public void moduleDescriptorParsesSeveralProjectTypesCorrectly() {
        String application =
                "<application key=\"completeModule\" name=\"ModuleDescriptorName\">\n" +
                        "<applicationKey>com.atlassian.jira.platform</applicationKey>\n" +
                        "<applicationName>Test Product</applicationName>\n" +
                        "<applicationDescriptionKey>some.key</applicationDescriptionKey>\n" +
                        "<userCountKey>user.count.key</userCountKey>\n" +
                        "<defaultGroup>test-group</defaultGroup>\n" +
                        "<projectTypes>\n" +
                        "<projectType>\n" +
                        "<key>key</key>\n" +
                        "<descriptionI18nKey>descriptionI18nKey</descriptionI18nKey>\n" +
                        "<icon>icon</icon>\n" +
                        "<color>color</color>\n" +
                        "<weight>10</weight>\n" +
                        "</projectType>\n" +
                        "<projectType>\n" +
                        "<key>key2</key>\n" +
                        "<descriptionI18nKey>description2</descriptionI18nKey>\n" +
                        "<icon>icon2</icon>\n" +
                        "<color>color2</color>\n" +
                        "<weight>5</weight>\n" +
                        "</projectType>\n" +
                        "</projectTypes>\n" +
                        "</application>";

        JiraPluginApplicationMetaData metadata = parse(application);

        assertThat(metadata.getProjectTypes(), Matchers.<ProjectType>iterableWithSize(2));

        Iterator<ProjectType> iterator = metadata.getProjectTypes().iterator();
        ProjectType projectType1 = iterator.next();
        ProjectType projectType2 = iterator.next();

        assertProjectTypeIsCorrect(projectType1, "key", "descriptionI18nKey", "icon", "color", 10);
        assertProjectTypeIsCorrect(projectType2, "key2", "description2", "icon2", "color2", 5);
    }

    @Test(expected = PluginParseException.class)
    public void moduleDescriptorThrowsAnExceptionIfAProjectTypeDoesNotHaveAKey() {
        String application =
                "<application key=\"completeModule\" name=\"ModuleDescriptorName\">\n" +
                        "<applicationKey>com.atlassian.jira.platform</applicationKey>\n" +
                        "<applicationName>Test Product</applicationName>\n" +
                        "<applicationDescriptionKey>some.key</applicationDescriptionKey>\n" +
                        "<userCountKey>user.count.key</userCountKey>\n" +
                        "<defaultGroup>test-group</defaultGroup>\n" +
                        "<projectTypes>\n" +
                        "<projectType>\n" +
                        "<descriptionI18nKey>descriptionI18nKey</descriptionI18nKey>\n" +
                        "<icon>icon</icon>\n" +
                        "<color>color</color>\n" +
                        "</projectType>\n" +
                        "</projectTypes>\n" +
                        "</application>";

        parse(application);
    }

    @Test(expected = PluginParseException.class)
    public void moduleDescriptorThrowsAnExceptionIfAProjectTypeDoesNotHaveADescriptionKey() {
        String application =
                "<application key=\"completeModule\" name=\"ModuleDescriptorName\">\n" +
                        "<applicationKey>com.atlassian.jira.platform</applicationKey>\n" +
                        "<applicationName>Test Product</applicationName>\n" +
                        "<applicationDescriptionKey>some.key</applicationDescriptionKey>\n" +
                        "<userCountKey>user.count.key</userCountKey>\n" +
                        "<defaultGroup>test-group</defaultGroup>\n" +
                        "<projectTypes>\n" +
                        "<projectType>\n" +
                        "<key>key</key>\n" +
                        "<icon>icon</icon>\n" +
                        "<color>color</color>\n" +
                        "</projectType>\n" +
                        "</projectTypes>\n" +
                        "</application>";

        parse(application);
    }

    @Test(expected = PluginParseException.class)
    public void moduleDescriptorThrowsAnExceptionIfAProjectTypeDoesNotHaveAnIcon() {
        String application =
                "<application key=\"completeModule\" name=\"ModuleDescriptorName\">\n" +
                        "<applicationKey>com.atlassian.jira.platform</applicationKey>\n" +
                        "<applicationName>Test Product</applicationName>\n" +
                        "<applicationDescriptionKey>some.key</applicationDescriptionKey>\n" +
                        "<userCountKey>user.count.key</userCountKey>\n" +
                        "<defaultGroup>test-group</defaultGroup>\n" +
                        "<projectTypes>\n" +
                        "<projectType>\n" +
                        "<key>key</key>\n" +
                        "<descriptionI18nKey>descriptionI18nKey</descriptionI18nKey>\n" +
                        "<color>color</color>\n" +
                        "</projectType>\n" +
                        "</projectTypes>\n" +
                        "</application>";

        parse(application);
    }

    @Test(expected = PluginParseException.class)
    public void moduleDescriptorThrowsAnExceptionIfAProjectTypeDoesNotHaveAColor() {
        String application =
                "<application key=\"completeModule\" name=\"ModuleDescriptorName\">\n" +
                        "<applicationKey>com.atlassian.jira.platform</applicationKey>\n" +
                        "<applicationName>Test Product</applicationName>\n" +
                        "<applicationDescriptionKey>some.key</applicationDescriptionKey>\n" +
                        "<userCountKey>user.count.key</userCountKey>\n" +
                        "<defaultGroup>test-group</defaultGroup>\n" +
                        "<projectTypes>\n" +
                        "<projectType>\n" +
                        "<key>key</key>\n" +
                        "<descriptionI18nKey>descriptionI18nKey</descriptionI18nKey>\n" +
                        "<icon>icon</icon>\n" +
                        "</projectType>\n" +
                        "</projectTypes>\n" +
                        "</application>";

        parse(application);
    }

    private JiraPluginApplicationMetaData parse(final String domString) {
        Element applicationElement = readDom(domString).getRootElement();

        Plugin plugin = createOsgiPlugin(applicationElement.elementText("applicationKey"), "2012-01-12T12:00:12+1000");
        ModuleFactory factory = mock(ModuleFactory.class);
        JiraApplicationMetaDataModuleDescriptor descriptor = new JiraApplicationMetaDataModuleDescriptor(factory);

        descriptor.init(plugin, applicationElement);
        return descriptor.getModule();
    }

    private Document readDom(String xml) {
        try {
            final SAXReader reader = new SAXReader();
            return reader.read(new StringReader(xml));
        } catch (DocumentException e) {
            throw new RuntimeException(e);
        }
    }

    private void assertProjectTypeIsCorrect(ProjectType projectType, String key, String descriptionI18nKey, String icon, String color, final int weight) {
        assertThat(projectType.getKey(), is(new ProjectTypeKey(key)));
        assertThat(projectType.getDescriptionI18nKey(), is(descriptionI18nKey));
        assertThat(projectType.getIcon(), is(icon));
        assertThat(projectType.getColor(), is(color));
        assertThat(projectType.getWeight(), is(weight));
    }

    static OsgiPlugin createOsgiPlugin(final String key, @Nullable final String date) {
        final OsgiPlugin plugin = mock(OsgiPlugin.class);
        final Bundle bundle = mock(Bundle.class);
        final PluginInformation info = mock(PluginInformation.class);

        final Hashtable<String, String> hashtable = new Hashtable<String, String>();
        if (date != null) {
            hashtable.put("Atlassian-Build-Date", date);
        }

        when(info.getVersion()).thenReturn("1.0");
        when(bundle.getHeaders()).thenReturn(hashtable);
        when(plugin.getBundle()).thenReturn(bundle);
        when(plugin.getKey()).thenReturn(key);
        when(plugin.getPluginInformation()).thenReturn(info);
        when(plugin.getPluginState()).thenReturn(PluginState.ENABLED);

        return plugin;
    }
}
