package com.atlassian.jira.upgrade.tasks.role;

import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.crowd.embedded.impl.ImmutableGroup;
import com.atlassian.crowd.embedded.impl.ImmutableUser;
import com.atlassian.jira.user.MockCrowdService;
import org.hamcrest.Description;
import org.hamcrest.TypeSafeDiagnosingMatcher;
import org.junit.Before;
import org.junit.Test;

import javax.annotation.Nullable;
import java.util.Set;

import static com.atlassian.jira.upgrade.tasks.role.TestUtils.asGroups;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


public class MigrationGroupServiceImplTest {
    private static final User USER_JOHN = user("john");
    private static final User USER_CARL = user("carl");
    private static final User USER_ADMIN = user("admin");

    private static final Group GROUP_ADMINS = group("admins");
    private static final Group GROUP_SOMEGROUP = group("somegroup");
    private static final Group GROUP_USERS_ADMINS = group("userAdmins");
    private static final Group GROUP_USERS = group("users");
    private static final Group GROUP_SD_AGENT = group("sd_agent");

    private MockCrowdService crowdService;
    private MigrationGroupService groupService;

    @Before
    public void setup() {
        final GlobalPermissionDao globalPermissionDao = mock(GlobalPermissionDao.class);
        crowdService = new MockCrowdService();
        groupService = new MigrationGroupServiceImpl(globalPermissionDao, crowdService);

        when(globalPermissionDao.groupsWithAdminPermission()).thenReturn(asGroups("admins", "userAdmins"));
        when(globalPermissionDao.groupsWithUsePermission()).thenReturn(asGroups("userAdmins", "users"));

        crowdService.addUser(USER_JOHN, null);
        crowdService.addUser(USER_CARL, null);
        crowdService.addUser(USER_ADMIN, null);

        crowdService.addGroup(GROUP_ADMINS);
        crowdService.addGroup(GROUP_USERS_ADMINS);
        crowdService.addGroup(GROUP_USERS);
        crowdService.addGroup(GROUP_SD_AGENT);
        crowdService.addGroup(GROUP_SOMEGROUP);
    }

    @Test
    public void returnsUsersInTheGroups() {
        crowdService.addUserToGroup(USER_CARL, GROUP_USERS);
        crowdService.addUserToGroup(USER_JOHN, GROUP_USERS);
        crowdService.addUserToGroup(USER_ADMIN, GROUP_ADMINS);

        assertThat(groupService.getUsersInGroup(GROUP_USERS), containsInAnyOrder(
                userMatches(USER_CARL),
                userMatches(USER_JOHN)
        ));
        assertThat(groupService.getUsersInGroup(GROUP_ADMINS), contains(
                userMatches(USER_ADMIN)
        ));
    }

    @Test
    public void doesNotReturnDisabledUsersInTheGroups() {
        crowdService.addUserToGroup(USER_CARL, GROUP_USERS);
        crowdService.addUserToGroup(USER_JOHN, GROUP_USERS);
        crowdService.addUserToGroup(ImmutableUser.newUser().active(false).name("fired").toUser(), GROUP_USERS);

        assertThat(groupService.getUsersInGroup(GROUP_USERS), containsInAnyOrder(
                userMatches(USER_CARL),
                userMatches(USER_JOHN)
        ));
    }

    @Test
    public void usersInNestedGroupsAreReturnedAndPermissionsAreCarriedOver() {
        crowdService.addUserToGroup(USER_ADMIN, GROUP_ADMINS);
        crowdService.addUserToGroup(USER_CARL, GROUP_SOMEGROUP);
        crowdService.addGroupToGroup(GROUP_SOMEGROUP, GROUP_ADMINS);

        final Set<UserWithPermissions> usersInGroup = groupService.getUsersInGroup(GROUP_ADMINS);
        assertThat(usersInGroup, containsInAnyOrder(
                userMatches(USER_CARL, false, true),
                userMatches(USER_ADMIN, false, true)
        ));

    }

    @Test
    public void usersHaveUseAndAdminPermissionPopulated() {
        crowdService.addUserToGroup(USER_CARL, GROUP_USERS);
        crowdService.addUserToGroup(USER_ADMIN, GROUP_ADMINS);

        assertThat(groupService.getUsersInGroup(GROUP_USERS), contains(
                userMatches(USER_CARL, true, false)
        ));
        assertThat(groupService.getUsersInGroup(GROUP_ADMINS), contains(
                userMatches(USER_ADMIN, false, true)
        ));
    }

    @Test
    public void userBelongingToGroupWithAdminAndUsePermissionHasThosePermissionsSet() {
        crowdService.addUserToGroup(USER_CARL, GROUP_USERS_ADMINS);

        assertThat(groupService.getUsersInGroup(GROUP_USERS_ADMINS), contains(
                userMatches(USER_CARL, true, true)
        ));
    }

    @Test
    public void userBelongingToGroupsWithAdminAndOtherWithUseHasThosePermissionsSet() {
        crowdService.addUserToGroup(USER_CARL, GROUP_USERS);
        crowdService.addUserToGroup(USER_CARL, GROUP_ADMINS);

        assertThat(groupService.getUsersInGroup(GROUP_USERS), contains(
                userMatches(USER_CARL, true, true)
        ));
        assertThat(groupService.getUsersInGroup(GROUP_ADMINS), contains(
                userMatches(USER_CARL, true, true)
        ));
    }

    @Test
    public void userBelongingToGroupWithNoUseAndAdminDoesNotHaveThemSet() {
        crowdService.addUserToGroup(USER_CARL, GROUP_SD_AGENT);

        assertThat(groupService.getUsersInGroup(GROUP_SD_AGENT), contains(
                userMatches(USER_CARL, false, false)
        ));
    }


    private static User user(String name) {
        return new ImmutableUser(1, name, name, name + "@test.org", true);
    }


    private static Group group(String name) {
        return new ImmutableGroup(name);
    }


    private static UserWithPermissionsMatcher userMatches(User user, boolean hasUse, boolean hasAdmin) {
        return new UserWithPermissionsMatcher(user.getName(), hasUse, hasAdmin);
    }

    private static UserWithPermissionsMatcher userMatches(User user) {
        return new UserWithPermissionsMatcher(user.getName(), null, null);
    }

    public static class UserWithPermissionsMatcher extends TypeSafeDiagnosingMatcher<UserWithPermissions> {
        private final String userName;
        private final Boolean hasUse;
        private final Boolean hasAdmin;

        public UserWithPermissionsMatcher(final String userName, @Nullable final Boolean hasUse,
                                          @Nullable final Boolean hasAdmin) {
            this.userName = userName;
            this.hasUse = hasUse;
            this.hasAdmin = hasAdmin;
        }

        @Override
        protected boolean matchesSafely(UserWithPermissions userWithPermissions, final Description mismatchDescription) {
            if (!userName.equalsIgnoreCase(userWithPermissions.getUser().getName())) {
                mismatchDescription.appendText("userName=").appendValue(userWithPermissions.getUser().getName());
                return false;
            }
            if (hasUse != null && userWithPermissions.hasUsePermission() != hasUse) {
                mismatchDescription.appendText("hasUse=").appendValue(userWithPermissions.hasUsePermission());
                return false;
            }
            if (hasAdmin != null && userWithPermissions.hasAdminPermission() != hasAdmin) {
                mismatchDescription.appendText("hasAdmin=").appendValue(userWithPermissions.hasAdminPermission());
                return false;
            }
            return true;
        }

        @Override
        public void describeTo(Description description) {
            description.appendText("[userName=").appendValue(userName);
            if (hasUse != null) {
                description.appendText(", hasUse=").appendValue(hasUse);
            }
            if (hasAdmin != null) {
                description.appendText(", hasAdmin=").appendValue(hasAdmin);
            }
            description.appendText("]");
        }

    }

}