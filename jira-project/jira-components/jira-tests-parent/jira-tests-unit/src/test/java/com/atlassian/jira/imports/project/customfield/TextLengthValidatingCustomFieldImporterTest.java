package com.atlassian.jira.imports.project.customfield;

import com.atlassian.jira.imports.project.mapper.ProjectImportMapper;
import com.atlassian.jira.imports.project.mapper.ProjectImportMapperImpl;
import com.atlassian.jira.issue.fields.TextFieldCharacterLengthValidator;
import com.atlassian.jira.util.I18nHelper;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static com.atlassian.jira.util.MessageSetAssert.assert1ErrorNoWarnings;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

public class TextLengthValidatingCustomFieldImporterTest {
    public static final String DUMMY_TEXT = "La de dah";
    private ProjectImportMapper projectImportMapper;

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @Mock
    private TextFieldCharacterLengthValidator validator;

    @Mock
    private I18nHelper i18nHelper;

    @InjectMocks
    private TextLengthValidatingCustomFieldImporter customFieldImporter;

    @Before
    public void setUp() {
        projectImportMapper = new ProjectImportMapperImpl(null, null);
    }

    @Test
    public void canMapImportStringValueWhenValueShorterThanLimit() {
        assertThat(customFieldImporter.canMapImportValue(projectImportMapper, getExternalCustomFieldStringValue("some text"), null, null), nullValue());
    }

    @Test
    public void testCannotMapImportStringValueWhenValueTooLong() {
        final String expectedErrorMessage = "This will be expected error message";
        when(i18nHelper.getText(eq("admin.errors.project.import.field.text.too.long"), anyString(), anyString())).thenReturn(expectedErrorMessage);
        when(validator.isTextTooLong(anyInt())).thenReturn(true);

        assert1ErrorNoWarnings(customFieldImporter.canMapImportValue(projectImportMapper, getExternalCustomFieldStringValue("some text"), null, i18nHelper), expectedErrorMessage);
    }

    @Test
    public void getMappedImportStringValueReturnsCustomFieldValue() {
        // Test with null original value
        assertThat(customFieldImporter.getMappedImportValue(new ProjectImportMapperImpl(null, null), getExternalCustomFieldStringValue(null), null).getValue(), nullValue());
        // Test with empty original value
        assertThat(customFieldImporter.getMappedImportValue(new ProjectImportMapperImpl(null, null), getExternalCustomFieldStringValue(""), null).getValue(), equalTo(""));
        // Test with non-empty original value
        assertThat(customFieldImporter.getMappedImportValue(new ProjectImportMapperImpl(null, null), getExternalCustomFieldStringValue(DUMMY_TEXT), null).getValue(), equalTo(DUMMY_TEXT));
    }

    @Test
    public void canMapImportTextValueWhenValueShorterThanLimit() {
        assertThat(customFieldImporter.canMapImportValue(projectImportMapper, getExternalCustomFieldTextValue("some text"), null, null), nullValue());
    }

    @Test
    public void testCannotMapImportTextValueWhenValueTooLong() {
        final String expectedErrorMessage = "This will be expected error message";
        when(i18nHelper.getText(eq("admin.errors.project.import.field.text.too.long"), anyString(), anyString())).thenReturn(expectedErrorMessage);
        when(validator.isTextTooLong(anyInt())).thenReturn(true);

        assert1ErrorNoWarnings(customFieldImporter.canMapImportValue(projectImportMapper, getExternalCustomFieldTextValue("some text"), null, i18nHelper), expectedErrorMessage);
    }

    @Test
    public void getMappedImportTextValueReturnsCustomFieldValue() {
        // Test with null original value
        assertThat(customFieldImporter.getMappedImportValue(new ProjectImportMapperImpl(null, null), getExternalCustomFieldTextValue(null), null).getValue(), nullValue());
        // Test with empty original value
        assertThat(customFieldImporter.getMappedImportValue(new ProjectImportMapperImpl(null, null), getExternalCustomFieldTextValue(""), null).getValue(), equalTo(""));
        // Test with non-empty original value
        assertThat(customFieldImporter.getMappedImportValue(new ProjectImportMapperImpl(null, null), getExternalCustomFieldTextValue(DUMMY_TEXT), null).getValue(), equalTo(DUMMY_TEXT));
    }

    @Test
    public void canMapImportNumberValue() {
        final ExternalCustomFieldValueImpl externalCustomFieldValue = new ExternalCustomFieldValueImpl("123456", "123", "8888");
        externalCustomFieldValue.setNumberValue("9089");

        assertThat(customFieldImporter.canMapImportValue(projectImportMapper, externalCustomFieldValue, null, null), nullValue());
    }

    @Test
    public void canMapImportDateValue() {
        final ExternalCustomFieldValueImpl externalCustomFieldValue = new ExternalCustomFieldValueImpl("123456", "123", "8888");
        externalCustomFieldValue.setDateValue("02/11/99");

        assertThat(customFieldImporter.canMapImportValue(projectImportMapper, externalCustomFieldValue, null, null), nullValue());
    }

    private ExternalCustomFieldValue getExternalCustomFieldStringValue(final String value) {
        final ExternalCustomFieldValueImpl externalCustomFieldValue = new ExternalCustomFieldValueImpl("123456", "123", "8888");
        externalCustomFieldValue.setStringValue(value);
        return externalCustomFieldValue;
    }

    private ExternalCustomFieldValue getExternalCustomFieldTextValue(final String value) {
        final ExternalCustomFieldValueImpl externalCustomFieldValue = new ExternalCustomFieldValueImpl("123456", "123", "8888");
        externalCustomFieldValue.setTextValue(value);
        return externalCustomFieldValue;
    }

}