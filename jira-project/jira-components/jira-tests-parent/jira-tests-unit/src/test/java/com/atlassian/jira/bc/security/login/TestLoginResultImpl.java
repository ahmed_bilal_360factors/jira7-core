package com.atlassian.jira.bc.security.login;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;

/**
 */
public class TestLoginResultImpl {
    @Test
    public void testConstruction() {
        LoginInfo loginInfo = LoginInfoImpl.builder()
                .setLastLoginTime(1L)
                .setPreviousLoginTime(2L)
                .setLastFailedLoginTime(3L)
                .setLoginCount(4L)
                .setCurrentFailedLoginCount(5L)
                .setTotalFailedLoginCount(5L)
                .setMaxAuthenticationAttemptsAllowed(6L)
                .setElevatedSecurityCheckRequired(true)
                .build();

        final LoginResultImpl loginResult = new LoginResultImpl(LoginReason.OK, loginInfo, "userName");
        assertEquals(LoginReason.OK, loginResult.getReason());
        assertEquals("userName", loginResult.getUserName());
        assertSame(loginInfo, loginResult.getLoginInfo());
    }
}
