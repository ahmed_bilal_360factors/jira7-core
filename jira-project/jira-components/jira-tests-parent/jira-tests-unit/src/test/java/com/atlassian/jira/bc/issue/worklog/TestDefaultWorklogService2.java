package com.atlassian.jira.bc.issue.worklog;

import com.atlassian.core.util.InvalidDurationException;
import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.bc.MockJiraServiceContext;
import com.atlassian.jira.bc.issue.util.VisibilityValidator;
import com.atlassian.jira.bc.issue.visibility.Visibility;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.worklog.Worklog;
import com.atlassian.jira.issue.worklog.WorklogImpl2;
import com.atlassian.jira.issue.worklog.WorklogManager;
import com.atlassian.jira.junit.rules.InitMockitoMocks;
import com.atlassian.jira.mock.MockApplicationProperties;
import com.atlassian.jira.mock.issue.MockIssue;
import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.security.roles.ProjectRoleManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.ErrorCollectionAssert;
import com.atlassian.jira.util.JiraDurationUtils;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.util.Date;
import java.util.Locale;

import static com.atlassian.jira.bc.issue.worklog.WorklogInputParametersImpl.issue;
import static com.atlassian.jira.permission.ProjectPermissions.WORK_ON_ISSUES;
import static com.atlassian.jira.util.ErrorCollectionAssert.assert1ErrorMessage;
import static java.util.Locale.ENGLISH;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * This TestCase unit tests DefaultWorklogService without using JiraMockTestCase.
 *
 * @since v3.13
 */
public class TestDefaultWorklogService2 {
    @Rule
    public TestRule initMockitoMocks = new InitMockitoMocks(this);

    @Mock
    private WorklogManager worklogManager;
    @Mock
    private PermissionManager permissionManager;
    @Mock
    private VisibilityValidator visibilityValidator;
    @Mock
    private ProjectRoleManager projectRoleManager;
    @Mock
    private IssueManager issueManager;
    @Mock
    private TimeTrackingConfiguration timeTrackingConfiguration;
    @Mock
    private JiraDurationUtils jiraDurationUtils;
    @Mock
    private GroupManager groupManager;

    @InjectMocks
    private DefaultWorklogService defaultWorklogService;

    private static final long HOUR = 3600;

    @Before
    public void setUp() throws Exception {
        when(jiraDurationUtils.parseDuration(Mockito.any(), Mockito.any())).thenReturn(1l);
    }

    @Test
    public void testValidateCreateNoPermission() {
        JiraServiceContext jiraServiceContext = new MockJiraServiceContext("wburroughs");
        when(timeTrackingConfiguration.enabled()).thenReturn(false);

        WorklogInputParameters params = WorklogInputParametersImpl
                .issue(new MockIssue())
                .timeSpent("4h")
                .build();
        assertNull(defaultWorklogService.validateCreate(jiraServiceContext, params));
        ErrorCollectionAssert.assert1ErrorMessage(jiraServiceContext.getErrorCollection(), "Can not perform the requested operation, time tracking is disabled in JIRA.");
    }

    @Test
    public void testValidateCreateHappyPath() {
        final String authorKey = "wburroughs";
        JiraServiceContext jiraServiceContext = new MockJiraServiceContext(authorKey);

        final Worklog worklog = mock(Worklog.class);
        final MockIssue issue = new MockIssue();

        when(visibilityValidator.isValidVisibilityData(Mockito.<JiraServiceContext>any(), Mockito.<String>any(), Mockito.<Issue>any(), Mockito.<Visibility>any())).thenReturn(true);
        when(timeTrackingConfiguration.enabled()).thenReturn(true);
        when(issueManager.isEditable(issue)).thenReturn(true);
        when(permissionManager.hasPermission(eq(ProjectPermissions.WORK_ON_ISSUES), eq(issue), Mockito.any())).thenReturn(true);

        WorklogInputParameters params = WorklogInputParametersImpl
                .issue(issue)
                .timeSpent("4h")
                .startDate(new Date())
                .build();

        final WorklogResult worklogResult = defaultWorklogService.validateCreate(jiraServiceContext, params);

        assertNotNull(worklogResult);
        assertEquals(issue, worklogResult.getWorklog().getIssue());
        assertTrue(worklogResult.isEditableCheckRequired());

        assertFalse(jiraServiceContext.getErrorCollection().hasAnyErrors());
    }

    @Test
    public void testValidateCreateHappyPathEditableCheckNotRequired() {
        JiraServiceContext jiraServiceContext = new MockJiraServiceContext("wburroughs");

        final Worklog worklog = mock(Worklog.class);
        final VisibilityValidator visibilityValidator = mock(VisibilityValidator.class);
        when(visibilityValidator.isValidVisibilityData(Mockito.<JiraServiceContext>any(), Mockito.<String>any(),
                Mockito.<Issue>any(), Mockito.<Visibility>any())).thenReturn(true);

        DefaultWorklogService defaultWorklogService = new DefaultWorklogService(null, null, visibilityValidator, null, null, null, null, null) {
            @Override
            public boolean hasPermissionToCreate(final JiraServiceContext jiraServiceContext, final Issue issue, final boolean isEditableCheckRequired) {
                return true;
            }

            @Override
            protected Worklog validateParamsAndCreateWorklog(final JiraServiceContext jiraServiceContext, final Issue issue,
                                                             final ApplicationUser author, final Visibility visibility, final String timeSpent, final Date startDate, final Long worklogId, final String comment, final Date created, final Date updated, final ApplicationUser updateAuthor, final String errorFieldPrefix) {
                return worklog;
            }
        };

        WorklogInputParameters params = WorklogInputParametersImpl
                .issue(new MockIssue())
                .timeSpent("4h")
                .editableCheckRequired(false)
                .build();

        final WorklogResult worklogResult = defaultWorklogService.validateCreate(jiraServiceContext, params);
        assertNotNull(worklogResult);
        assertEquals(worklog, worklogResult.getWorklog());
        assertFalse(worklogResult.isEditableCheckRequired());

        assertFalse(jiraServiceContext.getErrorCollection().hasAnyErrors());
    }

    @Test
    public void testValidateCreateWithManuallyAdjustedEstimateNoPermission() throws InvalidDurationException {
        JiraServiceContext jiraServiceContext = new MockJiraServiceContext("wburroughs");
        JiraDurationUtils jiraDurationUtils = mock(JiraDurationUtils.class);
        when(jiraDurationUtils.parseDuration("2h", ENGLISH)).thenReturn(10L);

        DefaultWorklogService defaultWorklogService = new DefaultWorklogService(null, null, null, null, null, null, jiraDurationUtils, null) {
            public WorklogResult validateCreate(final JiraServiceContext jiraServiceContext, final WorklogInputParameters params) {
                jiraServiceContext.getErrorCollection().addErrorMessage("You don't have permission to do this.");
                return null;
            }
        };

        WorklogAdjustmentAmountInputParameters params =
                issue(new MockIssue())
                        .timeSpent("4h")
                        .adjustmentAmount("2h")
                        .buildAdjustmentAmount();
        WorklogResult result = defaultWorklogService.validateCreateWithManuallyAdjustedEstimate(jiraServiceContext, params);

        // Now we expect to have failed because of the permission failure
        assertNull(result);
        assert1ErrorMessage(jiraServiceContext.getErrorCollection(), "You don't have permission to do this.");
    }

    @Test
    public void testValidateCreateWithManuallyAdjustedEstimateNullAdjustmentAmount() {
        JiraServiceContext jiraServiceContext = new MockJiraServiceContext("wburroughs");

        DefaultWorklogService defaultWorklogService = new DefaultWorklogService(null, null, null, null, null, null, null, null) {
            public WorklogResult validateCreate(final JiraServiceContext jiraServiceContext, final WorklogInputParameters params) {
                return WorklogResultFactory.createAdjustmentAmount(new WorklogImpl2(null, null, null, null, new Date(), null, null, 3600L, null), null);
            }
        };

        WorklogAdjustmentAmountInputParameters params = WorklogInputParametersImpl
                .issue(new MockIssue())
                .timeSpent("4h")
                .buildAdjustmentAmount();
        WorklogResult result = defaultWorklogService.validateCreateWithManuallyAdjustedEstimate(jiraServiceContext, params);

        // Now we expect to have failed because of the missing amount
        assertNull(result);
        ErrorCollectionAssert.assertFieldError(jiraServiceContext.getErrorCollection(), "adjustmentAmount", "You must supply a valid amount of time to adjust the estimate by.");
    }

    @Test
    public void testValidateCreateWithManuallyAdjustedEstimateEmptyAdjustmentAmount() {
        JiraServiceContext jiraServiceContext = new MockJiraServiceContext("wburroughs");

        DefaultWorklogService defaultWorklogService = new DefaultWorklogService(null, null, null, null, null, null, null, null) {
            public WorklogResult validateCreate(final JiraServiceContext jiraServiceContext, final WorklogInputParameters params) {
                return WorklogResultFactory.createAdjustmentAmount(new WorklogImpl2(null, null, null, null, new Date(), null, null, 3600L, null), null);
            }
        };

        WorklogAdjustmentAmountInputParameters params = WorklogInputParametersImpl
                .issue(new MockIssue())
                .timeSpent("4h")
                .adjustmentAmount("")
                .buildAdjustmentAmount();
        WorklogResult result = defaultWorklogService.validateCreateWithManuallyAdjustedEstimate(jiraServiceContext, params);

        // Now we expect to have failed because of the missing amount
        assertNull(result);
        ErrorCollectionAssert.assertFieldError(jiraServiceContext.getErrorCollection(), "adjustmentAmount", "You must supply a valid amount of time to adjust the estimate by.");
    }

    @Test
    public void testValidateCreateWithManuallyAdjustedEstimateNullWorklogResult() throws InvalidDurationException {
        JiraServiceContext jiraServiceContext = new MockJiraServiceContext("wburroughs");
        JiraDurationUtils jiraDurationUtils = mock(JiraDurationUtils.class);
        when(jiraDurationUtils.parseDuration("2h", ENGLISH)).thenReturn(10L);

        DefaultWorklogService defaultWorklogService = new DefaultWorklogService(null, null, null, null, null, null, jiraDurationUtils, null) {
            public WorklogResult validateCreate(final JiraServiceContext jiraServiceContext, final WorklogInputParameters params) {
                // as this validate returns null, so too will the call to validateCreateWithManuallyAdjustedEstimate
                return null;
            }
        };

        WorklogAdjustmentAmountInputParameters params =
                issue(new MockIssue())
                        .adjustmentAmount("2h")
                        .buildAdjustmentAmount();

        assertNull(defaultWorklogService.validateCreateWithManuallyAdjustedEstimate(jiraServiceContext, params));
    }

    @Test
    public void testValidateCreateWithManuallyAdjustedEstimateBadAdjustmentAmount() throws InvalidDurationException {
        JiraServiceContext jiraServiceContext = new MockJiraServiceContext("wburroughs");
        JiraDurationUtils jiraDurationUtils = mock(JiraDurationUtils.class);
        doThrow(new InvalidDurationException()).when(jiraDurationUtils).parseDuration("two hours", Locale.ENGLISH);

        DefaultWorklogService defaultWorklogService = new DefaultWorklogService(null, null, null, null, null, null, jiraDurationUtils, null) {
            public WorklogResult validateCreate(final JiraServiceContext jiraServiceContext, final WorklogInputParameters params) {
                return WorklogResultFactory.createAdjustmentAmount(new WorklogImpl2(null, null, null, null, new Date(), null, null, 3600L, null), null);
            }
        };

        WorklogAdjustmentAmountInputParameters params = WorklogInputParametersImpl
                .issue(new MockIssue())
                .timeSpent("4h")
                .adjustmentAmount("two hours")
                .buildAdjustmentAmount();
        WorklogResult result = defaultWorklogService.validateCreateWithManuallyAdjustedEstimate(jiraServiceContext, params);

        // Now we expect to have failed because of the not parsable amount
        assertNull(result);
        ErrorCollectionAssert.assertFieldError(jiraServiceContext.getErrorCollection(), "adjustmentAmount", "Invalid time entered for adjusting the estimate.");
    }

    @Test
    public void testValidateCreateWithManuallyAdjustedEstimateHappyPath() throws InvalidDurationException {
        JiraServiceContext jiraServiceContext = new MockJiraServiceContext("wburroughs");
        JiraDurationUtils jiraDurationUtils = mock(JiraDurationUtils.class);

        when(jiraDurationUtils.parseDuration("2h", Locale.ENGLISH)).thenReturn(7200L);

        MockApplicationProperties applicationProperites = new MockApplicationProperties();
        applicationProperites.setString("jira.timetracking.hours.per.day", "8");
        applicationProperites.setString("jira.timetracking.days.per.week", "5");
        applicationProperites.setString("jira.timetracking.default.unit", "minute");

        final TimeTrackingConfiguration trackingConfiguration = new TimeTrackingConfiguration.PropertiesAdaptor(applicationProperites);

        DefaultWorklogService defaultWorklogService = new DefaultWorklogService(null, null, null, null, null, trackingConfiguration, jiraDurationUtils, null) {
            public WorklogResult validateCreate(final JiraServiceContext jiraServiceContext, final WorklogInputParameters params) {
                return WorklogResultFactory.createAdjustmentAmount(new WorklogImpl2(null, null, null, null, new Date(), null, null, 3600L, null), null);
            }
        };

        WorklogAdjustmentAmountInputParameters params = WorklogInputParametersImpl
                .issue(new MockIssue())
                .timeSpent("4h")
                .adjustmentAmount("2h")
                .buildAdjustmentAmount();
        WorklogAdjustmentAmountResult result = defaultWorklogService.validateCreateWithManuallyAdjustedEstimate(jiraServiceContext, params);

        // Now we expect to have passed.
        assertNotNull(result.getWorklog());
        assertEquals(7200, result.getAdjustmentAmount().longValue());
        assertFalse(jiraServiceContext.getErrorCollection().hasAnyErrors());
    }

    @Test
    public void testCreateAndAutoAdjustRemainingEstimateNullWorklogResult() {
        JiraServiceContext jiraServiceContext = new MockJiraServiceContext("wburroughs");

        DefaultWorklogService defaultWorklogService = new DefaultWorklogService(null, null, null, null, null, null, null, null);

        assertNull(defaultWorklogService.createAndAutoAdjustRemainingEstimate(jiraServiceContext, null, false));
        ErrorCollectionAssert.assert1ErrorMessage(jiraServiceContext.getErrorCollection(), "Worklog must not be null.");
    }

    @Test
    public void testCreateAndAutoAdjustRemainingEstimateNullWorklog() {
        JiraServiceContext jiraServiceContext = new MockJiraServiceContext("wburroughs");

        DefaultWorklogService defaultWorklogService = new DefaultWorklogService(null, null, null, null, null, null, null, null);

        WorklogResult result = WorklogResultFactory.create(null);

        assertNull(defaultWorklogService.createAndAutoAdjustRemainingEstimate(jiraServiceContext, result, false));
        ErrorCollectionAssert.assert1ErrorMessage(jiraServiceContext.getErrorCollection(), "Worklog must not be null.");
    }

    @Test
    public void testCreateAndAutoAdjustRemainingEstimateNullIssue() {
        JiraServiceContext jiraServiceContext = new MockJiraServiceContext("wburroughs");

        DefaultWorklogService defaultWorklogService = new DefaultWorklogService(null, null, null, null, null, null, null, null);

        Worklog worklog = mock(Worklog.class);
        WorklogResult result = WorklogResultFactory.create(worklog);

        assertNull(defaultWorklogService.createAndAutoAdjustRemainingEstimate(jiraServiceContext, result, false));
        ErrorCollectionAssert.assert1ErrorMessage(jiraServiceContext.getErrorCollection(), "Can not modify a worklog without an issue specified.");
    }

    @Test
    public void testCreateAndAutoAdjustRemainingEstimateNoPermission() {
        JiraServiceContext jiraServiceContext = new MockJiraServiceContext("wburroughs");

        DefaultWorklogService defaultWorklogService = new DefaultWorklogService(null, null, null, null, null, null, null, null) {
            @Override
            public boolean hasPermissionToCreate(final JiraServiceContext jiraServiceContext, final Issue issue, final boolean isEditableCheckRequired) {
                jiraServiceContext.getErrorCollection().addErrorMessage("You don't have permission to do this.");
                return false;
            }
        };
        final MockIssue issue = new MockIssue();
        // current estimate is 10h
        issue.setEstimate(new Long(36000));
        // We have done 2 hours
        Worklog worklog = new WorklogImpl2(issue, null, null, null, null, null, null, 7200L, null);

        WorklogResult result = WorklogResultFactory.create(worklog);
        assertNull(defaultWorklogService.createAndAutoAdjustRemainingEstimate(jiraServiceContext, result, false));
        ErrorCollectionAssert.assert1ErrorMessage(jiraServiceContext.getErrorCollection(), "You don't have permission to do this.");
    }

    //------------------------------------------------------------------------------------------------------------------
    //  createWithManuallyAdjustedEstimate
    //------------------------------------------------------------------------------------------------------------------

    @Test
    public void testCreateWithManuallyAdjustedEstimate() {
        JiraServiceContext jiraServiceContext = new MockJiraServiceContext("wburroughs");
        final MutableBoolean me = new MutableBoolean();

        DefaultWorklogService defaultWorklogService = new DefaultWorklogService(null, null, null, null, null, null, null, null) {
            // Create is public and should be tested elsewhere.
            protected Worklog create(final JiraServiceContext jiraServiceContext, final WorklogResult worklogResult, final Long newEstimate, final boolean dispatchEvent) {
                // Check that the new Estimate is 4h
                if (newEstimate != 4 * 3600) {
                    fail("Tried to create Worklog with wrong new Estimate.");
                }
                me.called = true;
                return worklogResult.getWorklog();
            }
        };
        final MockIssue issue = new MockIssue();
        // current estimate is 10h
        issue.setEstimate(new Long(36000));
        // We have done 2 hours
        Worklog worklog = new WorklogImpl2(issue, null, null, null, null, null, null, 7200L, null);
        // but ask to reduce by 6h
        WorklogAdjustmentAmountResult result = WorklogResultFactory.createAdjustmentAmount(worklog, 6 * HOUR);
        defaultWorklogService.createWithManuallyAdjustedEstimate(jiraServiceContext, result, false);

        // Now we expect to have passed.
        //assertNotNull(worklog);
        assertFalse(jiraServiceContext.getErrorCollection().hasAnyErrors());
        // Make sure we called our create.
        assertTrue(me.called);
    }

    /**
     * This test will test what happens when we ask to manually reduce the estimate but the current estimate is
     * undefined (null).
     */
    @Test
    public void testCreateWithManuallyAdjustedEstimateNoCurrentEstimate() {
        JiraServiceContext jiraServiceContext = new MockJiraServiceContext("wburroughs");
        final MutableBoolean me = new MutableBoolean();

        DefaultWorklogService defaultWorklogService = new DefaultWorklogService(null, null, null, null, null, null, null, null) {
            // Create is public and should be tested elsewhere.
            protected Worklog create(final JiraServiceContext jiraServiceContext, final WorklogResult worklogResult, final Long newEstimate, final boolean dispatchEvent) {
                // Check that the new Estimate is 0h
                if (newEstimate != 0) {
                    fail("Tried to create Worklog with wrong new Estimate.");
                }
                me.called = true;
                return worklogResult.getWorklog();
            }
        };
        final MockIssue issue = new MockIssue();
        issue.setEstimate(null);
        // We have done 2 hours
        Worklog worklog = new WorklogImpl2(issue, null, null, null, null, null, null, 7200L, null);
        // but ask to reduce by 6h
        WorklogAdjustmentAmountResult result = WorklogResultFactory.createAdjustmentAmount(worklog, 6 * HOUR);
        defaultWorklogService.createWithManuallyAdjustedEstimate(jiraServiceContext, result, false);

        // Now we expect to have passed.
        //assertNotNull(worklog);
        assertFalse(jiraServiceContext.getErrorCollection().hasAnyErrors());
        // Make sure we called our create.
        assertTrue(me.called);
    }

    @Test
    public void testCreateWithManuallyAdjustedEstimateNullIssue() {
        JiraServiceContext jiraServiceContext = new MockJiraServiceContext("wburroughs");
        DefaultWorklogService defaultWorklogService = new DefaultWorklogService(null, null, null, null, null, null, null, null);
        Worklog worklog = new WorklogImpl2(null, null, null, null, null, null, null, 7200L, null);
        WorklogAdjustmentAmountResult result = WorklogResultFactory.createAdjustmentAmount(worklog, 6 * HOUR);
        assertNull(defaultWorklogService.createWithManuallyAdjustedEstimate(jiraServiceContext, result, false));
        assertTrue(jiraServiceContext.getErrorCollection().hasAnyErrors());
    }

    @Test
    public void testCreateWithManuallyAdjustedEstimateNullWorklog() {
        JiraServiceContext jiraServiceContext = new MockJiraServiceContext("wburroughs");
        DefaultWorklogService defaultWorklogService = new DefaultWorklogService(null, null, null, null, null, null, null, null);
        WorklogAdjustmentAmountResult result = WorklogResultFactory.createAdjustmentAmount((Worklog) null, 6 * HOUR);
        assertNull(defaultWorklogService.createWithManuallyAdjustedEstimate(jiraServiceContext, result, false));
        assertTrue(jiraServiceContext.getErrorCollection().hasAnyErrors());
    }

    @Test
    public void testCreateWithManuallyAdjustedEstimateNullWorklogResult() {
        JiraServiceContext jiraServiceContext = new MockJiraServiceContext("wburroughs");
        DefaultWorklogService defaultWorklogService = new DefaultWorklogService(null, null, null, null, null, null, null, null);
        assertNull(defaultWorklogService.createWithManuallyAdjustedEstimate(jiraServiceContext, null, false));
        assertTrue(jiraServiceContext.getErrorCollection().hasAnyErrors());
    }

    //------------------------------------------------------------------------------------------------------------------
    //  validateDeleteWithManuallyAdjustedEstimate
    //------------------------------------------------------------------------------------------------------------------

    @Test
    public void testValidateDeleteWithManuallyAdjustedEstimateNoPermission() {
        JiraServiceContext jiraServiceContext = new MockJiraServiceContext("wburroughs");

        DefaultWorklogService defaultWorklogService = new DefaultWorklogService(null, null, null, null, null, null, null, null) {
            public WorklogResult validateDelete(JiraServiceContext jiraServiceContext, Long worklogId) {
                jiraServiceContext.getErrorCollection().addErrorMessage("You don't have permission to do this.");
                return null;
            }
        };
        WorklogResult result = defaultWorklogService.validateDeleteWithManuallyAdjustedEstimate(jiraServiceContext, null, "4h");

        // Now we expect to have failed because of the permission failure
        assertNull(result);
        ErrorCollectionAssert.assert1ErrorMessage(jiraServiceContext.getErrorCollection(), "You don't have permission to do this.");
    }

    @Test
    public void testValidateDeleteWithManuallyAdjustedEstimateNullAdjustmentAmount() {
        JiraServiceContext jiraServiceContext = new MockJiraServiceContext("wburroughs");

        DefaultWorklogService defaultWorklogService = new DefaultWorklogService(null, null, null, null, null, null, null, null) {
            public WorklogResult validateDelete(JiraServiceContext jiraServiceContext, Long worklogId) {
                return WorklogResultFactory.create(new WorklogImpl2(null, null, null, null, new Date(), null, null, 3600L, null));
            }
        };
        WorklogResult result = defaultWorklogService.validateDeleteWithManuallyAdjustedEstimate(jiraServiceContext, null, null);

        // Now we expect to have failed because of the missing amount
        assertNull(result);
        ErrorCollectionAssert.assertFieldError(jiraServiceContext.getErrorCollection(), "adjustmentAmount", "You must supply a valid amount of time to adjust the estimate by.");
    }

    @Test
    public void testValidateDeleteWithManuallyAdjustedEstimateEmptyAdjustmentAmount() {
        JiraServiceContext jiraServiceContext = new MockJiraServiceContext("wburroughs");

        DefaultWorklogService defaultWorklogService = new DefaultWorklogService(null, null, null, null, null, null, null, null) {
            public WorklogResult validateDelete(JiraServiceContext jiraServiceContext, Long worklogId) {
                return WorklogResultFactory.create(new WorklogImpl2(null, null, null, null, new Date(), null, null, 3600L, null));
            }
        };
        WorklogResult result = defaultWorklogService.validateDeleteWithManuallyAdjustedEstimate(jiraServiceContext, null, "");

        // Now we expect to have failed because of the missing amount
        assertNull(result);
        ErrorCollectionAssert.assertFieldError(jiraServiceContext.getErrorCollection(), "adjustmentAmount", "You must supply a valid amount of time to adjust the estimate by.");
    }

    @Test
    public void testValidateDeleteWithManuallyAdjustedEstimateBadAdjustmentAmount() throws InvalidDurationException {
        JiraServiceContext jiraServiceContext = new MockJiraServiceContext("wburroughs");
        JiraDurationUtils jiraDurationUtils = mock(JiraDurationUtils.class);
        doThrow(new InvalidDurationException()).when(jiraDurationUtils).parseDuration("I can't parse!", Locale.ENGLISH);

        DefaultWorklogService defaultWorklogService = new DefaultWorklogService(null, null, null, null, null, null, jiraDurationUtils, null) {
            public WorklogResult validateDelete(JiraServiceContext jiraServiceContext, Long worklogId) {
                return WorklogResultFactory.create(new WorklogImpl2(null, null, null, null, new Date(), null, null, 3600L, null));
            }
        };
        WorklogResult result = defaultWorklogService.validateDeleteWithManuallyAdjustedEstimate(jiraServiceContext, null, "I can't parse!");

        // Now we expect to have failed because of the bad amount
        assertNull(result);
        ErrorCollectionAssert.assertFieldError(jiraServiceContext.getErrorCollection(), "adjustmentAmount", "Invalid time entered for adjusting the estimate.");
    }

    @Test
    public void testValidateDeleteWithManuallyAdjustedEstimateHappyPath() throws InvalidDurationException {
        JiraServiceContext jiraServiceContext = new MockJiraServiceContext("wburroughs");
        JiraDurationUtils jiraDurationUtils = mock(JiraDurationUtils.class);
        when(jiraDurationUtils.parseDuration("4h", Locale.ENGLISH)).thenReturn(14400L);

        MockApplicationProperties applicationProperites = new MockApplicationProperties();
        applicationProperites.setString("jira.timetracking.hours.per.day", "8");
        applicationProperites.setString("jira.timetracking.days.per.week", "5");
        applicationProperites.setString("jira.timetracking.default.unit", "minute");

        final TimeTrackingConfiguration trackingConfiguration = new TimeTrackingConfiguration.PropertiesAdaptor(applicationProperites);

        DefaultWorklogService defaultWorklogService = new DefaultWorklogService(null, null, null, null, null, trackingConfiguration, jiraDurationUtils, null) {
            public WorklogResult validateDelete(JiraServiceContext jiraServiceContext, Long worklogId) {
                return WorklogResultFactory.create(new WorklogImpl2(null, null, null, null, new Date(), null, null, 3600L, null));
            }
        };
        WorklogAdjustmentAmountResult result = defaultWorklogService.validateDeleteWithManuallyAdjustedEstimate(jiraServiceContext, null, "4h");

        // Now we expect to have passed.
        assertNotNull(result.getWorklog());
        assertEquals(4 * HOUR, result.getAdjustmentAmount().longValue());
        assertFalse(jiraServiceContext.getErrorCollection().hasAnyErrors());
    }

    //------------------------------------------------------------------------------------------------------------------
    //  deleteWithManuallyAdjustedEstimate
    //------------------------------------------------------------------------------------------------------------------

    @Test
    public void testDeleteWithManuallyAdjustedEstimateHappyPath() {
        JiraServiceContext jiraServiceContext = new MockJiraServiceContext("wburroughs");
        final MutableBoolean me = new MutableBoolean();

        DefaultWorklogService defaultWorklogService = new DefaultWorklogService(null, null, null, null, null, null, null, null) {
            // Create is public and should be tested elsewhere.
            protected boolean delete(final JiraServiceContext jiraServiceContext, final WorklogResult worklog, final Long newEstimate, final boolean dispatchEvent) {
                // Check that the new Estimate is 10h
                if (newEstimate != 10 * HOUR) {
                    fail("Tried to create Worklog with wrong new Estimate.");
                }
                me.called = true;
                return true;
            }
        };
        final MockIssue issue = new MockIssue();
        // current estimate is 4h
        issue.setEstimate(new Long(4 * HOUR));
        // Remove worklog where we did 2 hours
        Worklog worklog = new WorklogImpl2(issue, null, null, null, null, null, null, 2 * HOUR, null);
        // but ask to increase by 6h
        defaultWorklogService.deleteWithManuallyAdjustedEstimate(jiraServiceContext, WorklogResultFactory.createAdjustmentAmount(worklog, 6 * HOUR), false);

        // Now we expect to have passed.
        //assertNotNull(worklog);
        assertFalse(jiraServiceContext.getErrorCollection().hasAnyErrors());
        // Make sure we called our create.
        assertTrue(me.called);
    }

    /**
     * This test will test what happens when we ask to manually increase the estimate but the current estimate is
     * undefined (null).
     */
    @Test
    public void testDeleteWithManuallyAdjustedEstimateNoCurrentEstimate() {
        JiraServiceContext jiraServiceContext = new MockJiraServiceContext("wburroughs");
        final MutableBoolean me = new MutableBoolean();

        DefaultWorklogService defaultWorklogService = new DefaultWorklogService(null, null, null, null, null, null, null, null) {
            // Create is public and should be tested elsewhere.
            protected boolean delete(final JiraServiceContext jiraServiceContext, final WorklogResult worklog, final Long newEstimate, final boolean dispatchEvent) {
                // Check that the new Estimate is 6h
                if (newEstimate != 6 * HOUR) {
                    fail("Tried to create Worklog with wrong new Estimate.");
                }
                me.called = true;
                return true;
            }
        };
        final MockIssue issue = new MockIssue();
        issue.setEstimate(null);
        // Remove worklog where we did 2 hours
        Worklog worklog = new WorklogImpl2(issue, null, null, null, null, null, null, 2 * HOUR, null);
        // but ask to increase by 6h
        defaultWorklogService.deleteWithManuallyAdjustedEstimate(jiraServiceContext, WorklogResultFactory.createAdjustmentAmount(worklog, 6 * HOUR), false);

        // Now we expect to have passed.
        //assertNotNull(worklog);
        assertFalse(jiraServiceContext.getErrorCollection().hasAnyErrors());
        // Make sure we called our create.
        assertTrue(me.called);
    }

    @Test
    public void testDeleteWithManuallyAdjustedEstimateNullWorklogResult() {
        JiraServiceContext jiraServiceContext = new MockJiraServiceContext("wburroughs");

        assertFalse(defaultWorklogService.deleteWithManuallyAdjustedEstimate(jiraServiceContext, null, false));
        assertTrue(jiraServiceContext.getErrorCollection().hasAnyErrors());
    }

    @Test
    public void testDeleteWithManuallyAdjustedEstimateNullWorklog() {
        JiraServiceContext jiraServiceContext = new MockJiraServiceContext("wburroughs");

        assertFalse(defaultWorklogService.deleteWithManuallyAdjustedEstimate(jiraServiceContext, WorklogResultFactory.createAdjustmentAmount((Worklog) null, null), false));
        assertTrue(jiraServiceContext.getErrorCollection().hasAnyErrors());
    }

    @Test
    public void testDeleteWithManuallyAdjustedEstimateNullIssue() {
        JiraServiceContext jiraServiceContext = new MockJiraServiceContext("wburroughs");

        Worklog worklog = new WorklogImpl2(null, null, null, null, null, null, null, 2 * HOUR, null);

        assertFalse(defaultWorklogService.deleteWithManuallyAdjustedEstimate(jiraServiceContext, WorklogResultFactory.createAdjustmentAmount(worklog, null), false));
        assertTrue(jiraServiceContext.getErrorCollection().hasAnyErrors());
    }

    //------------------------------------------------------------------------------------------------------------------
    //  Autoadjust
    //------------------------------------------------------------------------------------------------------------------
    @Test
    public void testReduceEstimate() {
        DefaultWorklogService defaultWorklogService = new DefaultWorklogService(null, null, null, null, null, null, null, null);
        MockIssue mockIssue = new MockIssue();

        mockIssue.setEstimate(null);
        Long newEstimate = defaultWorklogService.reduceEstimate(mockIssue, 10L);
        assertEquals(new Long(0), newEstimate);

        mockIssue.setEstimate(new Long(40));
        newEstimate = defaultWorklogService.reduceEstimate(mockIssue, 10L);
        assertEquals(new Long(30), newEstimate);

        mockIssue.setEstimate(new Long(5));
        newEstimate = defaultWorklogService.reduceEstimate(mockIssue, 10L);
        assertEquals(new Long(0), newEstimate);
    }

    @Test
    public void testIncreaseEstimate() {
        DefaultWorklogService defaultWorklogService = new DefaultWorklogService(null, null, null, null, null, null, null, null);
        MockIssue mockIssue = new MockIssue();

        mockIssue.setEstimate(null);
        long newEstimate = defaultWorklogService.increaseEstimate(mockIssue, 10L);
        assertEquals(10L, newEstimate);

        mockIssue.setEstimate(40L);
        newEstimate = defaultWorklogService.increaseEstimate(mockIssue, 10L);
        assertEquals(50L, newEstimate);

        // This is a bullshit situation that should not occur, but we have coded against it, so I test it.
        mockIssue.setEstimate(-25L);
        newEstimate = defaultWorklogService.increaseEstimate(mockIssue, 10L);
        assertEquals(0L, newEstimate);
    }

    @Test
    public void testHasPermissionToCreateTimeTrackDisabled() throws Exception {
        JiraServiceContext jiraServiceContext = new MockJiraServiceContext("wburroughs");
        MockIssue issue = new MockIssue();

        when(timeTrackingConfiguration.enabled()).thenReturn(false);

        assertFalse(defaultWorklogService.hasPermissionToCreate(jiraServiceContext, issue, true));
        assert1ErrorMessage(jiraServiceContext.getErrorCollection(), "Can not perform the requested operation, time tracking is disabled in JIRA.");
    }

    @Test
    public void testHasPermissionToCreateIssueNull() throws Exception {
        JiraServiceContext jiraServiceContext = new MockJiraServiceContext("wburroughs");

        when(timeTrackingConfiguration.enabled()).thenReturn(true);

        assertFalse(defaultWorklogService.hasPermissionToCreate(jiraServiceContext, null, true));
        assert1ErrorMessage(jiraServiceContext.getErrorCollection(), "Can not modify a worklog without an issue specified.");
    }

    @Test
    public void testHasPermissionToCreateEditableCheckRequiredAndNotEditable() throws Exception {
        JiraServiceContext jiraServiceContext = new MockJiraServiceContext("wburroughs");

        MockIssue issue = new MockIssue();

        when(timeTrackingConfiguration.enabled()).thenReturn(true);
        when(issueManager.isEditable(issue)).thenReturn(false);

        assertFalse(defaultWorklogService.hasPermissionToCreate(jiraServiceContext, issue, true));

        assert1ErrorMessage(jiraServiceContext.getErrorCollection(), "You can not edit the issue as it is in a non-editable workflow state.");
    }

    @Test
    public void testHasPermissionToCreateEditableCheckNotRequiredAndNotEditable() throws Exception {
        JiraServiceContext jiraServiceContext = new MockJiraServiceContext("wburroughs");

        MockIssue issue = new MockIssue();

        when(timeTrackingConfiguration.enabled()).thenReturn(true);
        when(permissionManager.hasPermission(WORK_ON_ISSUES, issue, jiraServiceContext.getLoggedInApplicationUser())).thenReturn(true);

        assertTrue(defaultWorklogService.hasPermissionToCreate(jiraServiceContext, issue, false));
        assertFalse(jiraServiceContext.getErrorCollection().hasAnyErrors());
    }

    @Test
    public void testHasPermissionToCreateUserHasNoPermission() throws Exception {
        JiraServiceContext jiraServiceContext = new MockJiraServiceContext("wburroughs", "Willy B");
        MockIssue issue = new MockIssue();

        when(timeTrackingConfiguration.enabled()).thenReturn(true);
        when(issueManager.isEditable(issue)).thenReturn(true);
        when(permissionManager.hasPermission(WORK_ON_ISSUES, issue, jiraServiceContext.getLoggedInApplicationUser())).thenReturn(false);

        assertFalse(defaultWorklogService.hasPermissionToCreate(jiraServiceContext, issue, true));
        assert1ErrorMessage(jiraServiceContext.getErrorCollection(), "Willy B, you do not have the permission to associate a worklog to this issue.");
    }

    @Test
    public void testHasPermissionToCreateAnonymousUserHasNoPermission() throws Exception {
        JiraServiceContext jiraServiceContext = new MockJiraServiceContext((ApplicationUser) null);

        MockIssue issue = new MockIssue();

        when(timeTrackingConfiguration.enabled()).thenReturn(true);
        when(issueManager.isEditable(issue)).thenReturn(true);
        when(permissionManager.hasPermission(WORK_ON_ISSUES, issue, jiraServiceContext.getLoggedInApplicationUser())).thenReturn(false);

        assertFalse(defaultWorklogService.hasPermissionToCreate(jiraServiceContext, issue, true));
        assert1ErrorMessage(jiraServiceContext.getErrorCollection(), "You do not have the permission to associate a worklog to this issue.");
    }

    //------------------------------------------------------------------------------------------------------------------
    //  Helpers
    //------------------------------------------------------------------------------------------------------------------

    private class MutableBoolean {
        public boolean called = false;
    }
}
