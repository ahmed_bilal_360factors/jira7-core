package com.atlassian.jira.issue.label;

import com.atlassian.cache.memory.MemoryCacheManager;
import com.atlassian.jira.event.ClearCacheEvent;
import com.google.common.collect.ImmutableSet;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import java.util.Collections;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

/**
 * @since v4.2
 */
public class TestCachingLabelStore {
    private final static long ISSUE_ID = 10000L;
    private final Label LABEL_BAR = new Label(1L, ISSUE_ID, null, "bar");
    private final Label LABEL_FOO = new Label(2L, ISSUE_ID, null, "foo");

    @Rule
    public final MethodRule initMockito = MockitoJUnit.rule();

    @Mock
    private OfBizLabelStore mockStore;
    private CachingLabelStore cachingStore;

    @Before
    public void setUp() throws Exception {
        cachingStore = new CachingLabelStore(mockStore, new MemoryCacheManager());
    }

    @Test
    public void testGetLabelsReadsCachedValue() {
        assertEquals(Collections.<Label>emptySet(), cachingStore.getLabels(ISSUE_ID, null));
        verify(mockStore).getLabels(ISSUE_ID, null);

        // Cache hit.
        assertEquals(Collections.<Label>emptySet(), cachingStore.getLabels(ISSUE_ID, null));
        verifyNoMoreInteractions(mockStore);
    }

    @Test
    public void testSetLabelsModifiesStore() {
        final Set<Label> labels = ImmutableSet.of(LABEL_BAR, LABEL_FOO);

        // Prime cache.
        assertEquals(Collections.<Label>emptySet(), cachingStore.getLabels(ISSUE_ID, null));
        verify(mockStore).getLabels(ISSUE_ID, null);

        when(mockStore.setLabels(ISSUE_ID, null, ImmutableSet.of(LABEL_BAR.getLabel(), LABEL_FOO.getLabel()))).thenReturn(labels);
        when(mockStore.getLabels(ISSUE_ID, null)).thenReturn(labels);
        assertEquals(labels, cachingStore.setLabels(ISSUE_ID, null, ImmutableSet.of(LABEL_BAR.getLabel(), LABEL_FOO.getLabel())));
        verify(mockStore).setLabels(ISSUE_ID, null, ImmutableSet.of(LABEL_BAR.getLabel(), LABEL_FOO.getLabel()));
        verifyNoMoreInteractions(mockStore);

        // Read invalidated value, should load from backing store.
        assertEquals(labels, cachingStore.getLabels(ISSUE_ID, null));
        verify(mockStore, times(2)).getLabels(ISSUE_ID, null);

        // Read cached value
        assertEquals(labels, cachingStore.getLabels(ISSUE_ID, null));
        verifyNoMoreInteractions(mockStore);
    }

    @Test
    public void testAddLabelModifiesStoreAndInvalidatesCache() {
        when(mockStore.addLabel(ISSUE_ID, null, LABEL_BAR.getLabel())).thenReturn(LABEL_BAR);

        // Prime cache.
        assertEquals(Collections.<Label>emptySet(), cachingStore.getLabels(ISSUE_ID, null));

        // Invalidate:
        assertEquals(LABEL_BAR, cachingStore.addLabel(ISSUE_ID, null, LABEL_BAR.getLabel()));
        when(mockStore.getLabels(ISSUE_ID, null)).thenReturn(Collections.singleton(LABEL_BAR));

        // Cache miss.
        assertEquals(Collections.singleton(LABEL_BAR), cachingStore.getLabels(ISSUE_ID, null));
    }

    @Test
    public void testRemoveLabelModifiesStoreAndInvalidatesCache() {
        // Prime cache.
        when(mockStore.getLabels(ISSUE_ID, null)).thenReturn(Collections.singleton(LABEL_BAR));
        assertEquals(Collections.singleton(LABEL_BAR), cachingStore.getLabels(ISSUE_ID, null));

        // Remove label.
        cachingStore.removeLabel(LABEL_BAR.getId(), ISSUE_ID, null);
        verify(mockStore).removeLabel(LABEL_BAR.getId(), ISSUE_ID, null);
        when(mockStore.getLabels(ISSUE_ID, null)).thenReturn(Collections.<Label>emptySet());

        // Cache miss.
        assertEquals(Collections.<Label>emptySet(), cachingStore.getLabels(ISSUE_ID, null));
    }

    @Test
    public void testRespondsToClearCacheEvent() {
        assertEquals(Collections.<Label>emptySet(), cachingStore.getLabels(ISSUE_ID, null));
        cachingStore.onClearCache(ClearCacheEvent.INSTANCE);

        // Cache miss.
        assertEquals(Collections.<Label>emptySet(), cachingStore.getLabels(ISSUE_ID, null));
        verify(mockStore, times(2)).getLabels(ISSUE_ID, null);
    }
}
