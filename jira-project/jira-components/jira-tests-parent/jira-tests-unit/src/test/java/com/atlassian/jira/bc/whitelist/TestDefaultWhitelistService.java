package com.atlassian.jira.bc.whitelist;

import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.bc.JiraServiceContextImpl;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.mock.component.MockComponentWorker;
import com.atlassian.jira.mock.i18n.MockI18nHelper;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.UserKeyService;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.SimpleErrorCollection;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.when;

public class TestDefaultWhitelistService {
    private ApplicationUser admin;

    @Rule
    public final MethodRule initMockito = MockitoJUnit.rule();

    @Mock
    private PermissionManager permissionManager;
    @Mock
    private I18nHelper.BeanFactory beanFactory;
    @Mock
    private UserKeyService mockUserKeyService;

    @Before
    public void setUp() throws Exception {
        admin = new MockApplicationUser("admin");
        ComponentAccessor.initialiseWorker(new MockComponentWorker());
    }

    @Test
    public void testCheckInvalidPermissions() {
        when(permissionManager.hasPermission(Permissions.SYSTEM_ADMIN, (ApplicationUser) admin)).thenReturn(true);

        DefaultWhitelistService whitelistService = new DefaultWhitelistService(permissionManager, null, beanFactory);
        final SimpleErrorCollection errorCollection = new SimpleErrorCollection();
        assertFalse(whitelistService.checkInvalidPermissions(new JiraServiceContextImpl(admin, errorCollection)));
        assertFalse(errorCollection.hasAnyErrors());
    }

    @Test
    public void testCheckInvalidPermissionsNonSysadmin() {
        when(permissionManager.hasPermission(Permissions.SYSTEM_ADMIN, (ApplicationUser) admin)).thenReturn(false);
        when(beanFactory.getInstance((ApplicationUser) admin)).thenReturn(new MockI18nHelper());

        DefaultWhitelistService whitelistService = new DefaultWhitelistService(permissionManager, null, beanFactory);
        final SimpleErrorCollection errorCollection = new SimpleErrorCollection();
        assertTrue(whitelistService.checkInvalidPermissions(new JiraServiceContextImpl(admin, errorCollection)));
        assertTrue(errorCollection.hasAnyErrors());
    }

    @Test
    public void testMethodsCheckPermissions() {
        final DefaultWhitelistService whitelistService = new DefaultWhitelistService(null, null, null) {
            @Override
            boolean checkInvalidPermissions(JiraServiceContext context) {
                context.getErrorCollection().addErrorMessage("FAILED PERMISSION CHECK!");
                return true;
            }
        };

        WhitelistService.WhitelistResult result = whitelistService.getRules(getContext());
        assertFalse(result.isValid());

        final WhitelistService.WhitelistUpdateValidationResult validationResult = whitelistService.validateUpdateRules(getContext(), Collections.<String>emptyList(), false);
        assertFalse(validationResult.isValid());

        try {
            whitelistService.updateRules(validationResult);
            fail("should have thrown exception!");
        } catch (Exception e) {
            //yay!
        }
    }

    @Test
    public void testValidPatterns() {
        when(permissionManager.hasPermission(Permissions.SYSTEM_ADMIN, (ApplicationUser) admin)).thenReturn(true);

        final SimpleErrorCollection errorCollection = new SimpleErrorCollection();
        JiraServiceContext context = new JiraServiceContextImpl(admin, errorCollection);

        WhitelistService whitelistService = new DefaultWhitelistService(permissionManager, null, null);

        List<String> rules = new ArrayList<String>();
        rules.add("a series of innocent looking words");
        rules.add("/a [f]ancy reg(e|e)x");
        rules.add("a [f]ancy reg(e|e)x looking thing that actually has to be escaped");
        rules.add("=http://www.zombo.com");
        rules.add("http[s://hosting.gmodules.com/ig/gadgets/file/112581010116074801021/hamster.xml");
        rules.add("/http://hosting.gmodules.com/ig/gadgets/file/112581010116074801021/hamster.xml");

        WhitelistService.WhitelistUpdateValidationResult whitelistUpdateValidationResult;
        whitelistUpdateValidationResult = whitelistService.validateUpdateRules(context, rules, false);

        assertTrue(whitelistUpdateValidationResult.isValid());
        assertFalse(errorCollection.hasAnyErrors());
    }

    @Test
    public void testInvalidPatterns() {
        when(permissionManager.hasPermission(Permissions.SYSTEM_ADMIN, (ApplicationUser) admin)).thenReturn(true);
        when(beanFactory.getInstance((ApplicationUser) admin)).thenReturn(new MockI18nHelper());

        final SimpleErrorCollection errorCollection = new SimpleErrorCollection();
        JiraServiceContext context = new JiraServiceContextImpl(admin, errorCollection);

        WhitelistService whitelistService = new DefaultWhitelistService(permissionManager, null, beanFactory);

        List<String> rules = new ArrayList<String>();
        rules.add("/http[s://hosting.gmodules.com/ig/gadgets/file/112581010116074801021/hamster.xml");

        WhitelistService.WhitelistUpdateValidationResult whitelistUpdateValidationResult;
        whitelistUpdateValidationResult = whitelistService.validateUpdateRules(context, rules, false);

        assertFalse(whitelistUpdateValidationResult.isValid());
        assertTrue(errorCollection.hasAnyErrors());
    }

    private JiraServiceContext getContext() {
        final SimpleErrorCollection errorCollection = new SimpleErrorCollection();
        return new JiraServiceContextImpl(admin, errorCollection);
    }
}
