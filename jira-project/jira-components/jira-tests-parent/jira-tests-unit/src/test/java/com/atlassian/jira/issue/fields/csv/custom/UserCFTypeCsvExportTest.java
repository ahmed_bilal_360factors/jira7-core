package com.atlassian.jira.issue.fields.csv.custom;

import com.atlassian.jira.issue.customfields.impl.AbstractSingleFieldType;
import com.atlassian.jira.issue.customfields.impl.UserCFType;
import com.atlassian.jira.template.soy.SoyTemplateRendererProvider;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import org.junit.Test;

import static org.mockito.Mockito.mock;

public class UserCFTypeCsvExportTest extends CustomFieldSingleValueCsvExporterTest<ApplicationUser> {
    @Override
    protected AbstractSingleFieldType<ApplicationUser> createField() {
        return new UserCFType(null, null, null, null, null, null, null, mock(SoyTemplateRendererProvider.class), null, null,
                null, null, null, null, null, null);
    }

    @Test
    public void usernameIsExported() {
        whenFieldValueIs(new MockApplicationUser("geralt"));
        assertExportedValue("geralt");
    }
}
