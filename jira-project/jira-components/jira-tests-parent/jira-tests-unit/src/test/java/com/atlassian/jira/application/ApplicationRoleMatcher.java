package com.atlassian.jira.application;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.jira.user.MockGroup;
import com.google.common.base.Objects;
import com.google.common.collect.Sets;
import org.hamcrest.Description;
import org.hamcrest.TypeSafeDiagnosingMatcher;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static com.atlassian.jira.application.ApplicationRoleDefinitions.ApplicationRoleDefinition;
import static java.util.Arrays.asList;

public class ApplicationRoleMatcher extends TypeSafeDiagnosingMatcher<ApplicationRole> {
    private String name;
    private Set<Group> groups = Sets.newHashSet();
    private Set<Group> defaultGroups = Sets.newHashSet();
    private ApplicationKey id;
    private int seats;
    private boolean selectedByDefault;

    public static ApplicationRoleMatcher forCore() {
        return new ApplicationRoleMatcher().key(ApplicationKeys.CORE).name("JIRA Core");
    }

    public ApplicationRoleMatcher() {
    }

    public ApplicationRoleMatcher merge(ApplicationRoleDefinition definition) {
        key(definition.key());
        name(definition.name());
        return this;
    }

    public ApplicationRoleMatcher merge(ApplicationRole role) {
        key(role.getKey());
        name(role.getName());
        groups(role.getGroups());
        defaultGroups(role.getDefaultGroups());
        seats(role.getNumberOfSeats());
        selectedByDefault(role.isSelectedByDefault());

        return this;
    }

    public ApplicationRoleMatcher key(String id) {
        this.id = ApplicationKey.valueOf(id);
        return this;
    }

    public ApplicationRoleMatcher key(ApplicationKey id) {
        this.id = id;
        return this;
    }

    public ApplicationRoleMatcher name(String name) {
        this.name = name;
        return this;
    }

    public ApplicationRoleMatcher groups(Group... groups) {
        this.groups(asList(groups));
        return this;
    }

    public ApplicationRoleMatcher groupNames(final String... names) {
        return groups(toGroups(names));
    }

    public ApplicationRoleMatcher groups(Iterable<Group> groups) {
        this.groups = Sets.newHashSet(groups);
        return this;
    }

    public ApplicationRoleMatcher defaultGroups(Group... defaultGroups) {
        this.defaultGroups = Sets.newHashSet(defaultGroups);
        return this;
    }

    public ApplicationRoleMatcher defaultGroupNames(String... defaultGroups) {
        return defaultGroups(toGroups(defaultGroups));
    }

    public ApplicationRoleMatcher defaultGroups(Iterable<Group> defaultGroups) {
        this.defaultGroups = Sets.newHashSet(defaultGroups);
        return this;
    }

    public ApplicationRoleMatcher seats(int seats) {
        this.seats = seats;
        return this;
    }

    public ApplicationRoleMatcher selectedByDefault(boolean selectedByDefault) {
        this.selectedByDefault = selectedByDefault;
        return this;
    }

    @Override
    protected boolean matchesSafely(final ApplicationRole item, final Description mismatchDescription) {
        final boolean matchName = Objects.equal(name, item.getName());
        if (!matchName) {
            mismatchDescription.appendText(String.format("Failed to match name: %s %n", item.getName()));
        }
        final boolean matchGroup = Objects.equal(groups, item.getGroups());
        if (!matchGroup) {
            mismatchDescription.appendText(String.format("Failed to match groups: %s %n", item.getGroups()));
        }
        final boolean matchKey = Objects.equal(id, item.getKey());
        if (!matchKey) {
            mismatchDescription.appendText(String.format("Failed to match key: %s %n", item.getKey()));
        }
        final boolean matchDefaultGroups = Objects.equal(defaultGroups, item.getDefaultGroups());
        if (!matchDefaultGroups) {
            mismatchDescription.appendText(String.format("Failed to match defaultGroups: %s %n", item.getDefaultGroups()));
        }
        final boolean matchNumberOfSeats = seats == item.getNumberOfSeats();
        if (!matchNumberOfSeats) {
            mismatchDescription.appendText(String.format("Failed to match seats: %s %n", item.getNumberOfSeats()));
        }
        final boolean matchSelectedByDefault = selectedByDefault == item.isSelectedByDefault();
        if (!matchSelectedByDefault) {
            mismatchDescription.appendText(String.format("Failed to match selected by default: %s %n", item.isSelectedByDefault()));
        }

        if (matchName
                && matchGroup
                && matchKey
                && matchDefaultGroups
                && matchNumberOfSeats
                && matchSelectedByDefault) {
            return true;
        }

        mismatchDescription.appendText(String.format("[name: %s, groups: %s, id: %s, defaultGroups: %s, seats: %s, selectedByDefault: %b]",
                item.getName(), item.getGroups(), item.getKey(), item.getDefaultGroups(), item.getNumberOfSeats(), item.isSelectedByDefault()));

        return false;
    }


    @Override
    public void describeTo(final Description description) {
        description.appendText(String.format("[name: %s, groups: %s, id: %s, defaultGroups: %s, seats: %s, selectedByDefault: %b]",
                name, groups, id, defaultGroups, seats, selectedByDefault));
    }

    private static List<Group> toGroups(final String... names) {
        return Arrays.stream(names).map(MockGroup::new).collect(Collectors.toList());
    }
}
