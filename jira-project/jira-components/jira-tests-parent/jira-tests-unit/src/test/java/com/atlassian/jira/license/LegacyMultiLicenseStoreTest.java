package com.atlassian.jira.license;

import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.mock.MockApplicationProperties;
import com.atlassian.jira.mock.MockFeatureManager;
import com.atlassian.jira.test.util.lic.core.CoreLicenses;
import com.atlassian.jira.test.util.lic.sd.ServiceDeskLicenses;
import com.atlassian.jira.test.util.lic.software.SoftwareLicenses;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static com.atlassian.jira.license.Jira6xServiceDeskPluginEncodedLicenseSupplier.UPM_SERVICE_DESK_LICENSE_DELETED_KEY;
import static com.atlassian.jira.license.Jira6xServiceDeskPluginEncodedLicenseSupplier.UPM_SERVICE_DESK_LICENSE_KEY;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class LegacyMultiLicenseStoreTest {
    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @Mock
    private MultiLicenseStore delegateStore;
    private MockApplicationProperties properties = new MockApplicationProperties();
    private MockFeatureManager featureManager = new MockFeatureManager();
    private LegacyMultiLicenseStore licenseStore;

    @Before
    public void setUp() throws Exception {
        licenseStore = new LegacyMultiLicenseStore(delegateStore, properties);
    }

    @Test
    public void retrieveReturnsDelegateLicensesWithNoOtherLicensesAround() {
        //given
        final String serviceDesk = ServiceDeskLicenses.LICENSE_SERVICE_DESK_RBP.getLicenseString();
        final String core = CoreLicenses.LICENSE_CORE.getLicenseString();
        when(delegateStore.retrieve()).thenReturn(ImmutableList.of(serviceDesk, core));

        //when
        final Iterable<String> retrieve = licenseStore.retrieve();

        //then
        assertThat(retrieve, containsInAnyOrder(serviceDesk, core));
    }

    @Test(expected = IllegalStateException.class)
    public void retrieveThrowsAnErrorOnDuplicateKeysInStore() {
        //given
        final String serviceDesk = ServiceDeskLicenses.LICENSE_SERVICE_DESK_RBP.getLicenseString();
        final String serviceDesk2 = ServiceDeskLicenses.LICENSE_SERVICE_DESK_ABP.getLicenseString();
        when(delegateStore.retrieve()).thenReturn(ImmutableList.of(serviceDesk, serviceDesk2));

        //when
        licenseStore.retrieve();

        //then
        //throw an exception.
    }

    @Test
    public void retrieveReturnsOldLicenseIfStoreEmpty() {
        //given
        final String software = SoftwareLicenses.LICENSE_SOFTWARE.getLicenseString();
        when(delegateStore.retrieve()).thenReturn(ImmutableList.of());
        properties.setText(APKeys.JIRA_LICENSE, software);

        //when
        Iterable<String> result = licenseStore.retrieve();

        //then
        assertThat(result, containsInAnyOrder(software));
    }

    @Test
    public void retrieveIgnoresReturnsOldLicenseIfStoreNotEmpty() {
        //given
        final String core = CoreLicenses.LICENSE_CORE.getLicenseString();
        final String software = SoftwareLicenses.LICENSE_SOFTWARE.getLicenseString();

        when(delegateStore.retrieve()).thenReturn(ImmutableList.of(core));
        properties.setText(APKeys.JIRA_LICENSE, software);

        //when
        Iterable<String> result = licenseStore.retrieve();

        //then
        assertThat(result, containsInAnyOrder(core));
    }

    @Test
    public void retrieveMergesInOldServiceDeskWhenAvailable() {
        //given
        final String core = CoreLicenses.LICENSE_CORE.getLicenseString();
        final String software = SoftwareLicenses.LICENSE_SOFTWARE.getLicenseString();
        final String serviceDesk = ServiceDeskLicenses.LICENSE_SERVICE_DESK_RBP.getLicenseString();

        when(delegateStore.retrieve()).thenReturn(ImmutableList.of(core));
        properties.setText(APKeys.JIRA_LICENSE, software);
        properties.setText(UPM_SERVICE_DESK_LICENSE_KEY, serviceDesk);

        //when
        Iterable<String> result = licenseStore.retrieve();

        //then
        assertThat(result, containsInAnyOrder(core, serviceDesk));
    }

    @Test
    public void retrieveIgnoresOldServiceDeskLicenseWhenItWouldClash() {
        //given
        final String serviceDesk = ServiceDeskLicenses.LICENSE_SERVICE_DESK_RBP.getLicenseString();
        final String serviceDeskOld = ServiceDeskLicenses.LICENSE_SERVICE_DESK_ABP.getLicenseString();

        when(delegateStore.retrieve()).thenReturn(ImmutableList.of(serviceDesk));
        properties.setText(UPM_SERVICE_DESK_LICENSE_KEY, serviceDeskOld);

        //when
        Iterable<String> result = licenseStore.retrieve();

        //then
        assertThat(result, containsInAnyOrder(serviceDesk));
    }

    @Test
    public void retrieveIgnoresOldServiceDeskLicenseIfItsNotServiceDesk() {
        //given
        final String core = CoreLicenses.LICENSE_CORE.getLicenseString();
        final String software = SoftwareLicenses.LICENSE_SOFTWARE.getLicenseString();

        when(delegateStore.retrieve()).thenReturn(ImmutableList.of(core));
        properties.setText(UPM_SERVICE_DESK_LICENSE_KEY, software);

        //when
        Iterable<String> result = licenseStore.retrieve();

        //then
        assertThat(result, containsInAnyOrder(core));
    }

    @Test(expected = IllegalArgumentException.class)
    public void storeRejectsEmptyList() {
        //when
        licenseStore.store(ImmutableList.of());

        //then throw an error
    }

    @Test(expected = IllegalArgumentException.class)
    public void storeRejectsListWithNullValues() {
        //given
        final String core = CoreLicenses.LICENSE_CORE.getLicenseString();

        //when
        licenseStore.store(Lists.newArrayList(null, core));

        //then throw an error
    }

    @Test
    public void storeDelegates() {
        //given
        final String core = CoreLicenses.LICENSE_CORE.getLicenseString();
        final String software = SoftwareLicenses.LICENSE_SOFTWARE.getLicenseString();
        final ImmutableList<String> licenses = ImmutableList.of(core, software);

        //when
        licenseStore.store(licenses);

        //then
        verify(delegateStore).store(licenses);
    }

    @Test
    public void storeMovesOldSdLicenseToUpgradeLocation() {
        //given
        final String core = CoreLicenses.LICENSE_CORE.getLicenseString();
        final String software = SoftwareLicenses.LICENSE_SOFTWARE.getLicenseString();
        final String serviceDesk = ServiceDeskLicenses.LICENSE_SERVICE_DESK_ABP.getLicenseString();
        final ImmutableList<String> licenses = ImmutableList.of(core, software);

        properties.setText(UPM_SERVICE_DESK_LICENSE_KEY, serviceDesk);

        //when
        licenseStore.store(licenses);

        //verify
        verify(delegateStore).store(licenses);
        assertThat(properties.getText(UPM_SERVICE_DESK_LICENSE_DELETED_KEY), is(serviceDesk));
        assertThat(properties.getText(UPM_SERVICE_DESK_LICENSE_KEY), nullValue());
    }

    @Test
    public void storeRemovesJira6xLicense() {
        //given
        final String core = CoreLicenses.LICENSE_CORE.getLicenseString();
        final String software = SoftwareLicenses.LICENSE_SOFTWARE.getLicenseString();
        final String serviceDesk = ServiceDeskLicenses.LICENSE_SERVICE_DESK_ABP.getLicenseString();
        final ImmutableList<String> licenses = ImmutableList.of(core, serviceDesk);

        properties.setText(APKeys.JIRA_LICENSE, software);

        //when
        licenseStore.store(licenses);

        //verify
        verify(delegateStore).store(licenses);
        assertThat(properties.getText(APKeys.JIRA_LICENSE), nullValue());
    }

    @Test
    public void retrieveServerIdDelegates() {
        //given
        String serverId = "serverId";
        when(delegateStore.retrieveServerId()).thenReturn(serverId);

        //when
        final String id = licenseStore.retrieveServerId();

        //then
        assertThat(id, Matchers.equalTo(serverId));
        verify(delegateStore).retrieveServerId();
    }

    @Test
    public void storeServerIdDelegates() {
        //given
        String serverId = "serverId";

        //when
        licenseStore.storeServerId(serverId);

        //then
        verify(delegateStore).storeServerId(serverId);
    }

    @Test
    public void resetOldBuildConfirmationDelegates() {
        //when
        licenseStore.resetOldBuildConfirmation();

        //then
        verify(delegateStore).resetOldBuildConfirmation();
    }

    @Test
    public void confirmProceedUnderEvaluationTermsDelegates() {
        //given
        String user = "user";

        //when
        licenseStore.confirmProceedUnderEvaluationTerms(user);

        //then
        verify(delegateStore).confirmProceedUnderEvaluationTerms(user);
    }

    @Test
    public void clearFlushesTheOldAndNewLicenses() {
        //given
        final String core = CoreLicenses.LICENSE_CORE.getLicenseString();
        final String serviceDesk = ServiceDeskLicenses.LICENSE_SERVICE_DESK_ABP.getLicenseString();

        properties.setText(UPM_SERVICE_DESK_LICENSE_KEY, serviceDesk);
        properties.setText(APKeys.JIRA_LICENSE, core);

        //when
        licenseStore.clear();

        //then
        verify(delegateStore).clear();
        assertThat(properties.getText(APKeys.JIRA_LICENSE), nullValue());
        assertThat(properties.getText(UPM_SERVICE_DESK_LICENSE_KEY), nullValue());
        assertThat(properties.getText(UPM_SERVICE_DESK_LICENSE_DELETED_KEY), is(serviceDesk));
    }
}