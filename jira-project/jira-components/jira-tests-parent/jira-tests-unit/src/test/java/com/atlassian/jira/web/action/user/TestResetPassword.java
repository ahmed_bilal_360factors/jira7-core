package com.atlassian.jira.web.action.user;


import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.bc.security.login.LoginInfo;
import com.atlassian.jira.bc.security.login.LoginService;
import com.atlassian.jira.event.user.OnboardingEvent;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.plugin.user.PasswordPolicyManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserUtil;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.web.action.admin.user.PasswordChangeService;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;
import webwork.action.ResultException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestResetPassword {

    @Rule
    public final RuleChain mockContainer = MockitoMocksInContainer.forTest(this);

    @Mock
    private UserUtil userUtil;

    @Mock
    private LoginInfo loginInfo;

    @Mock
    private LoginService loginService;

    @Mock
    private EventPublisher eventPublisher;

    @Mock
    private PasswordPolicyManager passwordPolicyManager;

    @Mock
    private ApplicationUser user;

    @Mock
    private UserUtil.PasswordResetTokenValidation resetValidation;

    @Mock
    private PasswordChangeService passwordChangeService;

    @Mock
    @AvailableInContainer
    private JiraAuthenticationContext mockJiraAuthenticationContext;

    @Mock
    private I18nHelper mockI18nHelper;

    private static final String RESET_PASSWORD_VALIDATION_PROPERTY = "resetpassword.error.invalid.user.or.token";


    private ResetPassword resetPassword;

    @Before
    public void setUp() throws Exception {
        resetPassword = new ResetPassword(userUtil, loginService, eventPublisher, passwordPolicyManager, passwordChangeService);

        when(mockJiraAuthenticationContext.getI18nHelper()).thenReturn(mockI18nHelper);

        when(mockI18nHelper.getText(anyString())).thenAnswer(new Answer<Object>() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                return invocation.getArguments()[0];
            }
        });
    }

    @Test
    public void whenUserNotFoundErrorReturned() throws ResultException {
        when(userUtil.getUser(anyString())).thenReturn(null);

        resetPassword.validate();

        assertTrue(resetPassword.isUserInvalid());
        assertEquals(1, resetPassword.getErrorMessages().size());
        assertEquals(RESET_PASSWORD_VALIDATION_PROPERTY, getError());

    }

    @Test
    public void whenTokenIsValidErrorReturned() throws ResultException {
        final String invalidToken = "invalid";

        when(userUtil.getUser(anyString())).thenReturn(user);

        when(userUtil.validatePasswordResetToken(eq(user), eq(invalidToken))).thenReturn(resetValidation);
        when(resetValidation.getStatus()).thenReturn(UserUtil.PasswordResetTokenValidation.Status.UNEQUAL);

        resetPassword.setToken(invalidToken);

        resetPassword.validate();

        assertTrue(resetPassword.isTokenInvalid());
        assertEquals(RESET_PASSWORD_VALIDATION_PROPERTY, getError());
    }


    @Test
    public void whenTokenIsExpiredErrorReturned() throws ResultException {
        final String expiredToken = "expired";
        when(userUtil.getUser(anyString())).thenReturn(user);

        when(userUtil.validatePasswordResetToken(eq(user), eq(expiredToken))).thenReturn(resetValidation);
        when(resetValidation.getStatus()).thenReturn(UserUtil.PasswordResetTokenValidation.Status.EXPIRED);

        resetPassword.setToken(expiredToken);

        resetPassword.validate();

        assertTrue(resetPassword.isTokenTimedOut());
        assertEquals(1, resetPassword.getErrorMessages().size());
        assertEquals(RESET_PASSWORD_VALIDATION_PROPERTY, getError());
    }

    @Test
    public void whenOnboardingUserHitsPageSendOnboardingEmailClickEvent() throws ResultException {
        final String validToken = "valid";
        when(userUtil.getUser(anyString())).thenReturn(user);

        when(loginInfo.getLoginCount()).thenReturn(0L);
        when(loginService.getLoginInfo(anyString())).thenReturn(loginInfo);

        when(userUtil.validatePasswordResetToken(eq(user), eq(validToken))).thenReturn(resetValidation);
        when(resetValidation.getStatus()).thenReturn(UserUtil.PasswordResetTokenValidation.Status.OK);

        resetPassword.setToken(validToken);
        resetPassword.validate();

        resetPassword.doDefault();

        ArgumentCaptor<OnboardingEvent> argumentCaptor = ArgumentCaptor.forClass(OnboardingEvent.class);

        verify(eventPublisher, times(1)).publish(argumentCaptor.capture());
        assertEquals("onboarding.email.click", argumentCaptor.getValue().buildEventName());
    }

    @Test
    public void whenOnboardingUserHitsPageWithExpiredLinkSendOnboardingEmailLinkExpiredEvent() throws ResultException {
        final String validToken = "expired";
        when(userUtil.getUser(anyString())).thenReturn(user);

        when(loginInfo.getLoginCount()).thenReturn(0L);
        when(loginService.getLoginInfo(anyString())).thenReturn(loginInfo);

        when(userUtil.validatePasswordResetToken(eq(user), eq(validToken))).thenReturn(resetValidation);
        when(resetValidation.getStatus()).thenReturn(UserUtil.PasswordResetTokenValidation.Status.EXPIRED);

        resetPassword.setToken(validToken);
        resetPassword.validate();

        resetPassword.doDefault();

        ArgumentCaptor<OnboardingEvent> argumentCaptor = ArgumentCaptor.forClass(OnboardingEvent.class);

        verify(eventPublisher, times(1)).publish(argumentCaptor.capture());
        assertEquals("onboarding.email.linkExpired", argumentCaptor.getValue().buildEventName());
    }

    private String getError() {
        return resetPassword.getErrorMessages().iterator().next();
    }
}
