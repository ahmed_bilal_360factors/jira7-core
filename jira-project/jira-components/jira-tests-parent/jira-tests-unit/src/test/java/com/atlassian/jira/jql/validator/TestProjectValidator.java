package com.atlassian.jira.jql.validator;

import com.atlassian.jira.jql.operand.JqlOperandResolver;
import com.atlassian.jira.jql.resolver.ProjectResolver;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.util.MessageSetImpl;
import com.atlassian.query.clause.TerminalClauseImpl;
import com.atlassian.query.operator.Operator;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.empty;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestProjectValidator {
    @Rule
    public final MethodRule initMockito = MockitoJUnit.rule();

    @Mock
    private SupportedOperatorsValidator mockSupportedOperatorsValidator;
    @Mock
    private RawValuesExistValidator mockRawValuesExistValidator;

    @Test
    public void testRawValuesDelegateNotCalledWithOperatorProblem() throws Exception {
        final TerminalClauseImpl clause = new TerminalClauseImpl("test", Operator.GREATER_THAN, 12L);
        final MessageSetImpl messageSet = new MessageSetImpl();
        messageSet.addErrorMessage("blah blah");

        when(mockSupportedOperatorsValidator.validate(null, clause)).thenReturn(messageSet);

        final ProjectValidator projectValidator = getProjectValidator();

        assertSame(messageSet, projectValidator.validate(null, clause));
    }

    @Test
    public void testRawValuesDelegateNotCalledWithValueProblem() throws Exception {
        final TerminalClauseImpl clause = new TerminalClauseImpl("test", Operator.GREATER_THAN, 12L);
        final MessageSetImpl messageSet = new MessageSetImpl();
        messageSet.addErrorMessage("blah blah");

        when(mockSupportedOperatorsValidator.validate(null, clause)).thenReturn(new MessageSetImpl());
        when(mockRawValuesExistValidator.validate(null, clause)).thenReturn(messageSet);

        final ProjectValidator projectValidator = getProjectValidator();

        assertEquals(messageSet, projectValidator.validate(null, clause));
    }

    @Test
    public void testRawValuesDelegateCalledWithNoOperatorProblem() throws Exception {
        final TerminalClauseImpl clause = new TerminalClauseImpl("test", Operator.GREATER_THAN, 12L);

        when(mockSupportedOperatorsValidator.validate(null, clause)).thenReturn(new MessageSetImpl());
        when(mockRawValuesExistValidator.validate(null, clause)).thenReturn(new MessageSetImpl());

        final ProjectValidator projectValidator = getProjectValidator();

        assertThat(projectValidator.validate(null, clause).getErrorMessages(), empty());
        verify(mockSupportedOperatorsValidator).validate(null, clause);
        verify(mockRawValuesExistValidator).validate(null, clause);
    }

    private ProjectValidator getProjectValidator() {
        return new ProjectValidator(null, null, null, null, null) {
            @Override
            SupportedOperatorsValidator getSupportedOperatorsValidator() {
                return mockSupportedOperatorsValidator;
            }

            @Override
            ValuesExistValidator getValuesValidator(final ProjectResolver projectResolver, final JqlOperandResolver operandSupport, final PermissionManager permissionManager, final ProjectManager projectManager) {
                return mockRawValuesExistValidator;
            }
        };
    }

}
