package com.atlassian.jira.application;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.jira.user.MockGroup;
import com.google.common.collect.ImmutableSet;
import org.junit.Test;

import javax.annotation.Nonnull;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static com.atlassian.extras.common.LicensePropertiesConstants.UNLIMITED_USERS;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertTrue;

public class AbstractApplicationRoleTest {
    @Test(expected = IllegalArgumentException.class)
    public void cotrFailsWithNullGroups() {
        new TestingApplicationRole("id", "name", null, groups(), false, false);
    }

    @Test(expected = IllegalArgumentException.class)
    public void cotrFailsWithNullInGroups() {
        new TestingApplicationRole("id", "name", groups("jkl", null), groups(), false, false);
    }

    @Test(expected = IllegalArgumentException.class)
    public void cotrFailsWithNullInDefaultGroups() {
        new TestingApplicationRole("id", "name", groups("one"), groups("one", null), false, false);
    }

    @Test(expected = IllegalArgumentException.class)
    public void cotrFailsWithNullDefaultGroups() {
        new TestingApplicationRole("id", "name", groups(), null, false, false);
    }

    @Test(expected = IllegalArgumentException.class)
    public void cotrFailsWithDefaultGroupNotListedInGroups() {
        new TestingApplicationRole("id", "name", groups("one", "two"), groups("one", "two", "three"), false, false);
    }

    @Test
    public void equalsOnlyListensToId() {
        List<ApplicationRole> roles = Arrays.<ApplicationRole>asList(
                new TestingApplicationRole("id", "name", groups("one"), groups(), false, false),
                new TestingApplicationRole("id", "name", groups("one"), groups(), true, false),
                new TestingApplicationRole("id", "name", groups("two", "thee"), groups(), false, false),
                new TestingApplicationRole("id", "name", groups("two", "thee"), groups(), true, false),
                new TestingApplicationRole("id", "name", groups("two", "three"), groups("three"), false, false),
                new TestingApplicationRole("id", "otherName", groups("two", "three"), groups("three"), false, false));

        for (ApplicationRole left : roles) {
            for (ApplicationRole right : roles) {
                assertThat(left.equals(right), equalTo(true));
            }
        }

        List<ApplicationRole> otherRoles = Arrays.<ApplicationRole>asList(
                new TestingApplicationRole("idTwo", "name", groups("one"), groups(), false, false),
                new TestingApplicationRole("idTwo", "name", groups("one"), groups(), true, false),
                new TestingApplicationRole("idTwo", "name", groups("two", "thee"), groups(), false, false),
                new TestingApplicationRole("idTwo", "name", groups("two", "thee"), groups(), true, false),
                new TestingApplicationRole("idTwo", "name", groups("two", "three"), groups("three"), false, false),
                new TestingApplicationRole("idTwo", "otherName", groups("two", "three"), groups("three"), false, false));

        for (ApplicationRole left : roles) {
            for (ApplicationRole right : otherRoles) {
                assertThat(left.equals(right), equalTo(false));
            }
        }
    }

    @Test
    public void hashListensToId() {
        List<ApplicationRole> roles = Arrays.<ApplicationRole>asList(
                new TestingApplicationRole("id", "name", groups("one"), groups(), false, false),
                new TestingApplicationRole("id", "name", groups("one"), groups(), true, false),
                new TestingApplicationRole("id", "name", groups("two", "thee"), groups(), false, false),
                new TestingApplicationRole("id", "name", groups("two", "thee"), groups(), true, false),
                new TestingApplicationRole("id", "name", groups("two", "three"), groups("three"), false, false),
                new TestingApplicationRole("id", "otherName", groups("two", "three"), groups("three"), false, false));

        for (ApplicationRole left : roles) {
            for (ApplicationRole right : roles) {
                assertThat(left.hashCode() == right.hashCode(), equalTo(true));
            }
        }
    }

    @Test
    public void getGroupsReturnsGroups() {
        assertThat(new TestingApplicationRole("id", "name", groups("one"), groups(), false, false).getGroups(), contains(group("one")));
        assertThat(new TestingApplicationRole("id", "name", groups("one", "two"), groups(), false, false).getGroups(),
                containsInAnyOrder(group("one"), group("two")));
    }

    @Test
    public void getDefaultGroupsReturnsTheDefaults() {
        assertTrue(new TestingApplicationRole("id", "name", groups("one"), groups(), false, false).getDefaultGroups().isEmpty());
        assertThat(new TestingApplicationRole("id", "name", groups("one", "two"), groups("two"), false, false).getDefaultGroups(),
                containsInAnyOrder(group("two")));
        assertThat(new TestingApplicationRole("id", "name", groups("one", "two"), groups("one", "two"), false, false).getDefaultGroups(),
                containsInAnyOrder(group("two"), group("one")));
    }

    @Test
    public void getSelectedByDefaultReturnsDefault() {
        assertThat(new TestingApplicationRole("id", "name", groups(), groups(), true, false).isSelectedByDefault(), equalTo(true));
        assertThat(new TestingApplicationRole("id", "name", groups(), groups(), false, false).isSelectedByDefault(), equalTo(false));
    }

    @Test
    public void getDefinedReturnsDefined() {
        assertThat(new TestingApplicationRole("id", "name", groups(), groups(), false, true).isDefined(), equalTo(true));
        assertThat(new TestingApplicationRole("id", "name", groups(), groups(), false, false).isDefined(), equalTo(false));
    }

    private static Group group(String group) {
        return new MockGroup(group);
    }

    private static Set<Group> groups(String... groups) {
        return Arrays.stream(groups)
                .map(name -> name != null ? new MockGroup(name) : null).collect(Collectors.toSet());
    }

    private static Set<Group> groups() {
        return ImmutableSet.of();
    }

    private static class TestingApplicationRole extends AbstractApplicationRole {
        private final String name;
        private final ApplicationKey key;
        private boolean defined;

        private TestingApplicationRole(final String key, final String name, Iterable<Group> groups,
                                       Iterable<Group> defaultGroups, final boolean selectedByDefault, final boolean defined) {
            super(groups, defaultGroups, UNLIMITED_USERS, selectedByDefault);
            this.key = ApplicationKey.valueOf(key);
            this.name = name;
            this.defined = defined;
        }

        @Nonnull
        @Override
        public ApplicationKey getKey() {
            return key;
        }

        @Nonnull
        @Override
        public String getName() {
            return name;
        }

        @Nonnull
        @Override
        public ApplicationRole withGroups(@Nonnull final Iterable<Group> groups, @Nonnull final Iterable<Group> defaultGroups) {
            throw new UnsupportedOperationException("Not implemented");
        }

        @Override
        public ApplicationRole withSelectedByDefault(final boolean selectedByDefault) {
            throw new UnsupportedOperationException("Not implemented");
        }

        @Override
        public boolean isPlatform() {
            return false;
        }

        @Override
        public boolean isDefined() {
            return this.defined;
        }
    }
}