package com.atlassian.jira.upgrade.util;

import com.atlassian.jira.mock.ofbiz.MockOfBizDelegator;
import org.junit.Test;
import org.ofbiz.core.entity.GenericValue;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class BuildNumberDaoTest {
    @Test
    public void insertUpgradeVersionHistory() throws Exception {
        BuildNumberDao buildNumberDao = new BuildNumberDao(null, null);
        final MockOfBizDelegator mockOfBizDelegator = new MockOfBizDelegator();
        assertEquals(0, mockOfBizDelegator.findAll("UpgradeVersionHistory").size());

        buildNumberDao.insertUpgradeVersionHistory(mockOfBizDelegator, "64021", "6.4.9");
        final List<GenericValue> upgradeVersionHistory = mockOfBizDelegator.findAll("UpgradeVersionHistory");
        assertEquals(1, upgradeVersionHistory.size());
        assertEquals("64021", upgradeVersionHistory.get(0).getString("targetbuild"));
        assertEquals("6.4.9", upgradeVersionHistory.get(0).getString("targetversion"));
    }
}
