package com.atlassian.jira.upgrade;

import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class DowngradeTaskFileParserTest {
    @Test
    public void testHappyPath() throws Exception {
        DowngradeTaskFileParser parser = new DowngradeTaskFileParser();

        String xml = "<!--\n"
                + "Details of all the downgrade tasks for JIRA.\n"
                + "\n"
                + "A downgrade task is an explicit reversal of an upgrade task.\n"
                + "The \"build number\" for a downgrade task is the number of the upgrade task it reverses.\n"
                + "eg downgrade task 117 would reverse upgrade task 117 and move the current build number to 116.\n"
                + "\n"
                + "Not all upgrade tasks have explicit downgrade tasks - more often the reversal is a NO-OP.\n"
                + "The UpgradeHistory DB table stores a history of all upgrades that have run along with a flag of whether the reversal\n"
                + "is a no-op or if it requires a downgrade task.\n"
                + "\n"
                + "See also the \"upgrades.xml\" file.\n"
                + "-->\n"
                + "\n"
                + "<downgrades>\n"
                + "    <downgrade>\n"
                + "        <class>com.atlassian.jira.upgrade.tasks.DowngradeTask_Build64012</class>\n"
                + "    </downgrade>\n"
                + "    <downgrade>\n"
                + "        <class>com.atlassian.jira.upgrade.tasks.DowngradeTask_Build64014</class>\n"
                + "    </downgrade>\n"
                + "    <downgrade>\n"
                + "        <class>com.atlassian.jira.upgrade.tasks.DowngradeTask_Build64015</class>\n"
                + "    </downgrade>\n"
                + "    <downgrade>\n"
                + "        <class>com.atlassian.jira.upgrade.tasks.DowngradeTask_Build64016</class>\n"
                + "    </downgrade>\n"
                + "</downgrades>";

        final Collection<String> classNames = parser.parse(new ByteArrayInputStream(xml.getBytes()));
        final List<String> expected = Arrays.asList(
                "com.atlassian.jira.upgrade.tasks.DowngradeTask_Build64012",
                "com.atlassian.jira.upgrade.tasks.DowngradeTask_Build64014",
                "com.atlassian.jira.upgrade.tasks.DowngradeTask_Build64015",
                "com.atlassian.jira.upgrade.tasks.DowngradeTask_Build64016"
        );
        assertEquals(expected, classNames);
    }

}
