package com.atlassian.jira.upgrade.tasks.role;

import com.atlassian.jira.auditing.AssociatedItem;
import com.atlassian.jira.license.Jira6xServiceDeskPluginEncodedLicenseSupplier;
import com.atlassian.jira.test.util.lic.core.CoreLicenses;
import com.atlassian.jira.test.util.lic.sd.ServiceDeskLicenses;
import com.atlassian.jira.test.util.lic.software.SoftwareLicenses;
import com.google.common.collect.ImmutableList;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static com.atlassian.fugue.Option.none;
import static com.atlassian.fugue.Option.some;
import static com.atlassian.jira.test.util.lic.sd.ServiceDeskLicenses.LICENSE_SERVICE_DESK_RBP;
import static java.util.stream.Collectors.toList;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class Move6xServiceDeskLicenseTo70StoreTest {
    private static final String SERVICE_DESK_LICENSE = ServiceDeskLicenses.LICENSE_SERVICE_DESK_ABP.getLicenseString();
    private static final String SOFTWARE_LICENSE = SoftwareLicenses.LICENSE_SOFTWARE.getLicenseString();
    private static final String CORE_LICENSE = CoreLicenses.LICENSE_CORE.getLicenseString();

    private Jira6xServiceDeskPluginEncodedLicenseSupplier serviceDeskLicenseSupplier;
    private Move6xServiceDeskLicenseTo70Store migrationTask;

    @Before
    public void setUp() {
        serviceDeskLicenseSupplier = mock(Jira6xServiceDeskPluginEncodedLicenseSupplier.class);
        migrationTask = new Move6xServiceDeskLicenseTo70Store(serviceDeskLicenseSupplier);
    }

    @Test
    public void migratesServiceDeskABPLicenseWhenNotAlreadyPresent() {
        returnServiceDeskLicense(SERVICE_DESK_LICENSE);

        final MigrationState result = migrationTask.migrate(migrationState(), false);

        assertThat(result, new MigrationStateMatcher()
                .licenses(SERVICE_DESK_LICENSE)
                .log(new AuditEntry(Move6xServiceDeskLicenseTo70Store.class,
                        "Moved Service Desk license to JIRA license store",
                        "The existing Service Desk plugin license was moved to the JIRA license store to function as" +
                                " a license for the JIRA Service Desk application.",
                        AssociatedItem.Type.LICENSE,
                        "SEN-3320766", false
                )));
    }

    @Test
    public void migratesServiceDeskRBPLicenseWhenNotAlreadyPresent() {
        returnServiceDeskLicense(LICENSE_SERVICE_DESK_RBP.getLicenseString());

        final MigrationState result = migrationTask.migrate(migrationState(), false);

        assertThat(result, new MigrationStateMatcher()
                .licenses(LICENSE_SERVICE_DESK_RBP.getLicenseString())
                .log(new AuditEntry(Move6xServiceDeskLicenseTo70Store.class,
                                "Moved Service Desk license to JIRA license store",
                                "The existing Service Desk plugin license was moved to the JIRA license store to function as" +
                                        " a license for the JIRA Service Desk application.",
                                AssociatedItem.Type.LICENSE,
                                "SEN-4075780", false)
                ));
    }

    @Test
    public void doesNotMigrateServiceDeskLicenseIfItIsAlreadyPresent() {
        returnServiceDeskLicense(SERVICE_DESK_LICENSE);

        // Already has service desk plugin license in multi license store
        final MigrationState initialState = migrationState(SOFTWARE_LICENSE, SERVICE_DESK_LICENSE);
        final MigrationState result = migrationTask.migrate(initialState, false);

        assertThat(result, new MigrationStateMatcher()
                .licenses(SOFTWARE_LICENSE, SERVICE_DESK_LICENSE)
                .log(new AuditEntry(Move6xServiceDeskLicenseTo70Store.class,
                        "Not moving old Service Desk license - new license present already",
                        "An existing JIRA Service Desk license is installed already so will be used in" +
                                " preference of the old-style plugin license. ",
                        AssociatedItem.Type.LICENSE, null, false)));
    }

    @Test
    public void doesNotMigrateIfThereIsNoServiceDeskLicenseInUPMLicStore() {
        when(serviceDeskLicenseSupplier.get()).thenReturn(none());

        final MigrationState initialState = migrationState(SOFTWARE_LICENSE);
        final MigrationState result = migrationTask.migrate(initialState, false);

        assertThat(result, new MigrationStateMatcher().licenses(SOFTWARE_LICENSE));
    }

    @Test
    public void doesNotMigrateIfSDLicenseInUPMStoreIsNotServiceDeskLicense() {
        when(serviceDeskLicenseSupplier.get()).thenReturn(some(CORE_LICENSE));

        final MigrationState initialState = migrationState(SOFTWARE_LICENSE);
        final MigrationState result = migrationTask.migrate(initialState, false);

        assertThat(result, new MigrationStateMatcher().licenses(SOFTWARE_LICENSE));
    }

    @Test
    public void noMigratesServiceDeskLicenseWhenUserSuppliedLicense() {
        returnServiceDeskLicense(SERVICE_DESK_LICENSE);

        final MigrationState initialState = migrationState(SOFTWARE_LICENSE, CORE_LICENSE);
        final MigrationState result = migrationTask.migrate(initialState, true);

        assertThat(result,
                new MigrationStateMatcher()
                        .licenses(SOFTWARE_LICENSE, CORE_LICENSE)
                        .log(new AuditEntry(Move6xServiceDeskLicenseTo70Store.class,
                                "Not moving old Service Desk license - license supplied by user",
                                "The person importing JIRA has supplied a license to use; any existing Service Desk license" +
                                        " installed in your system will be ignored in preference of the new license.",
                                AssociatedItem.Type.LICENSE, null, false
                        )));
    }

    private MigrationState migrationState(String... encodedLicenses) {
        final List<License> licenses = Arrays.asList(encodedLicenses).stream().map(License::new).collect(toList());
        return new MigrationState(new Licenses(licenses), TestUtils.emptyRoles(), ImmutableList.of(),
                new MigrationLogImpl());
    }

    private void returnServiceDeskLicense(String encodedLicense) {
        when(serviceDeskLicenseSupplier.get()).thenReturn(some(encodedLicense));
    }
}