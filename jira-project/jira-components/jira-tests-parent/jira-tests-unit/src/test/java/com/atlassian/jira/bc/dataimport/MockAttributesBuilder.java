package com.atlassian.jira.bc.dataimport;

import com.google.common.collect.Maps;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.xml.sax.Attributes;

import java.util.Map;

/**
 * Builder for attributes
 *
 * @since v6.1
 */
class MockAttributesBuilder {
    private static final String ID = "id";

    private final Map<String, String> map = Maps.newHashMap();

    MockAttributesBuilder id(String id) {
        map.put(ID, id);
        return this;
    }

    MockAttributesBuilder attr(String key, String value) {
        map.put(key, value);
        return this;
    }

    MockAttributesBuilder attr(final Map<String, String> values) {
        map.putAll(values);
        return this;
    }

    Attributes build() {
        final Attributes mock = Mockito.mock(Attributes.class);
        Mockito.when(mock.getValue(Mockito.anyString())).thenAnswer(new Answer<Object>() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                final Object[] args = invocation.getArguments();
                final String key = (String) args[0];
                return map.get(key);
            }
        });
        return mock;
    }

}
