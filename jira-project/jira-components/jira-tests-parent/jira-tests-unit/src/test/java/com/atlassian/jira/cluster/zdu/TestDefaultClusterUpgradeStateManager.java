package com.atlassian.jira.cluster.zdu;

import com.atlassian.beehive.ClusterLock;
import com.atlassian.beehive.ClusterLockService;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.cluster.ClusterInfo;
import com.atlassian.jira.cluster.ClusterManager;
import com.atlassian.jira.cluster.MessageHandlerService;
import com.atlassian.jira.cluster.Node;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.event.JiraDelayedUpgradeCompletedEvent;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.mock.cluster.zdu.MockNodeBuildInfo;
import com.atlassian.jira.model.querydsl.ClusterUpgradeStateDTO;
import com.atlassian.jira.upgrade.UpgradeService;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;
import java.util.Optional;
import java.util.Set;

import static com.atlassian.jira.cluster.Node.NodeState.ACTIVE;
import static com.atlassian.jira.cluster.zdu.ClusterUpgradeStateManager.CLUSTER_UPGRADE_STATE_CHANGED;
import static com.atlassian.jira.cluster.zdu.UpgradeState.MIXED;
import static com.atlassian.jira.cluster.zdu.UpgradeState.READY_TO_RUN_UPGRADE_TASKS;
import static com.atlassian.jira.cluster.zdu.UpgradeState.READY_TO_UPGRADE;
import static com.atlassian.jira.cluster.zdu.UpgradeState.RUNNING_UPGRADE_TASKS;
import static com.atlassian.jira.cluster.zdu.UpgradeState.STABLE;
import static java.lang.String.valueOf;
import static java.util.stream.Collectors.toSet;
import static java.util.stream.IntStream.range;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestDefaultClusterUpgradeStateManager {
    private static final String CLUSTER_UPGRADE_STATE_DARK_FEATURE = "jira.zdu.cluster-upgrade-state";

    private static final NodeBuildInfo BUILD_NUMBER_FROM = new MockNodeBuildInfo(72000, "7.2.0");
    private static final NodeBuildInfo BUILD_NUMBER_TO = new MockNodeBuildInfo(73000, "7.3.0");

    @Rule
    public final RuleChain mockitoMocks = MockitoMocksInContainer.forTest(this);

    @Mock
    private ClusterManager clusterManager;

    @Mock
    private ClusterLockService clusterLockService;

    @Mock
    private ClusterUpgradeStateDao clusterUpgradeStateDao;

    @Mock
    private EventPublisher eventPublisher;

    @Mock
    private ClusterInfo clusterInfo;

    @Mock
    private UpgradeService upgradeService;

    @Mock
    private FeatureManager featureManager;

    @Mock
    private MessageHandlerService messageHandlerService;

    @Mock
    private ClusterLock clusterLock;

    @Mock
    private NodeBuildInfoFactory nodeBuildInfoFactory;

    private DefaultClusterUpgradeStateManager clusterUpgradeStateManager;

    @Before
    public void setUp() {
        when(clusterLockService.getLockForName(anyString())).thenReturn(clusterLock);
        when(featureManager.isEnabled(CLUSTER_UPGRADE_STATE_DARK_FEATURE)).thenReturn(true);
        when(clusterUpgradeStateDao.getCurrent()).thenReturn(Optional.empty());
        when(nodeBuildInfoFactory.create(any(Node.class))).thenAnswer(call -> {
            Node node = call.getArgumentAt(0, Node.class);
            return new MockNodeBuildInfo(node.getNodeBuildNumber(), node.getNodeVersion());
        });
        when(nodeBuildInfoFactory.create(any(ClusterUpgradeStateDTO.class))).thenAnswer(call -> {
            ClusterUpgradeStateDTO dto = call.getArgumentAt(0, ClusterUpgradeStateDTO.class);
            return new MockNodeBuildInfo(dto.getClusterBuildNumber(), dto.getClusterVersion());
        });
        doAnswer(invocation -> {
            mockClusterUpgradeState(invocation.getArgumentAt(0, NodeBuildInfo.class), invocation.getArgumentAt(1, UpgradeState.class));
            return null;
        }).when(clusterUpgradeStateDao).writeState(any(), any());
        clusterUpgradeStateManager = new DefaultClusterUpgradeStateManager(
                clusterManager,
                clusterLockService,
                clusterUpgradeStateDao,
                clusterInfo,
                eventPublisher,
                featureManager,
                messageHandlerService,
                nodeBuildInfoFactory
        );
    }

    private ClusterUpgradeStateDTO clusterUpgradeState(NodeBuildInfo buildNumber, UpgradeState upgradeState) {
        return new ClusterUpgradeStateDTO(1L, 12345L, buildNumber.getBuildNumber(), buildNumber.getVersion(), upgradeState.name(), 1L);
    }

    @Test
    public void testUpdateClusterStateWithNoPreviousState() {
        mockApplicationBuildInfo(BUILD_NUMBER_FROM);

        clusterUpgradeStateManager.startUpgrade();

        verifyUpgradeStateWritten(BUILD_NUMBER_FROM, READY_TO_UPGRADE);
        verifyClusterNotified(READY_TO_UPGRADE);
        verifyEventPublished(JiraUpgradeStartedEvent.class);
    }

    @Test
    public void testApproveUpgrade() {
        mockClusterUpgradeState(BUILD_NUMBER_TO, READY_TO_RUN_UPGRADE_TASKS);

        clusterUpgradeStateManager.approveUpgrade();

        verifyEventPublished(JiraUpgradeApprovedEvent.class);
    }

    @Test
    public void runUpgradeFromEvent() {
        mockClusterUpgradeState(BUILD_NUMBER_FROM, READY_TO_RUN_UPGRADE_TASKS);
        mockApplicationBuildInfo(BUILD_NUMBER_TO);

        clusterUpgradeStateManager.runUpgrade();

        verifyUpgradeStateWritten(BUILD_NUMBER_FROM, RUNNING_UPGRADE_TASKS);
        verifyClusterNotified(RUNNING_UPGRADE_TASKS);
    }

    @Test
    public void upgradeTasksFinished() {
        mockClusterUpgradeState(BUILD_NUMBER_FROM, RUNNING_UPGRADE_TASKS);
        mockApplicationBuildInfo(BUILD_NUMBER_TO);

        clusterUpgradeStateManager.onJiraUpgradeCompleted(new JiraDelayedUpgradeCompletedEvent(valueOf(BUILD_NUMBER_TO.getBuildNumber())));

        verifyUpgradeStateWritten(BUILD_NUMBER_TO, STABLE);
        verifyClusterNotified(STABLE);
        verifyEventPublished(JiraUpgradeFinishedEvent.class);
    }

    @Test
    public void upgradeCompletedEventDoesNotTriggerEventsWhenInIncorrectState() {
        mockClusterUpgradeState(BUILD_NUMBER_FROM, STABLE);
        mockApplicationBuildInfo(BUILD_NUMBER_FROM);

        clusterUpgradeStateManager.onJiraUpgradeCompleted(new JiraDelayedUpgradeCompletedEvent(valueOf(BUILD_NUMBER_FROM.getBuildNumber())));

        verifyNoUpgradeStateWritten();
        verifyClusterNotNotified();
        verifyNoEventPublished();
    }

    @Test
    public void noClusterNodeEntriesDoesNotTransitionClustered() {
        mockClusterUpgradeState(BUILD_NUMBER_FROM, READY_TO_UPGRADE);
        when(clusterManager.findLiveNodes()).thenReturn(Collections.emptySet());
        when(clusterInfo.isClustered()).thenReturn(true);

        clusterUpgradeStateManager.updateState(null);

        verifyNoUpgradeStateWritten();
    }

    @Test
    public void noClusterNodeEntriesGoesReadyToRunUpgradeTasksNonClustered() {
        mockClusterUpgradeState(BUILD_NUMBER_FROM, READY_TO_UPGRADE);
        mockApplicationBuildInfo(BUILD_NUMBER_TO);
        when(clusterManager.findLiveNodes()).thenReturn(Collections.emptySet());
        when(clusterInfo.isClustered()).thenReturn(false);

        clusterUpgradeStateManager.updateState(null);

        verifyUpgradeStateWritten(BUILD_NUMBER_FROM, READY_TO_RUN_UPGRADE_TASKS);
        verifyClusterNotified(READY_TO_RUN_UPGRADE_TASKS);
    }

    @Test
    public void noClusterNodeEntriesStaysReadyToUpgradeNonClustered() {
        mockClusterUpgradeState(BUILD_NUMBER_FROM, READY_TO_UPGRADE);
        mockApplicationBuildInfo(BUILD_NUMBER_FROM);
        when(clusterManager.findLiveNodes()).thenReturn(Collections.emptySet());
        when(clusterInfo.isClustered()).thenReturn(false);

        clusterUpgradeStateManager.updateState(null);

        verifyNoUpgradeStateWritten();
    }

    @Test
    public void startupInReadyToUpgrade() {
        mockClusterUpgradeState(BUILD_NUMBER_FROM, READY_TO_UPGRADE);
        mockClusterNodes(BUILD_NUMBER_FROM);

        clusterUpgradeStateManager.updateState(null);

        verifyNoUpgradeStateWritten();
        verifyClusterNotNotified();
    }

    @Test
    public void readyToUpgradeToMixed() {
        mockClusterUpgradeState(BUILD_NUMBER_FROM, READY_TO_UPGRADE);
        mockClusterNodes(BUILD_NUMBER_FROM, BUILD_NUMBER_TO);

        clusterUpgradeStateManager.updateState(null);

        verifyUpgradeStateWritten(BUILD_NUMBER_FROM, MIXED);
        verifyClusterNotified(MIXED);
    }

    @Test
    public void mixedToReadyToUpgrade() {
        mockClusterUpgradeState(BUILD_NUMBER_FROM, MIXED);
        mockClusterNodes(BUILD_NUMBER_FROM, BUILD_NUMBER_FROM);

        clusterUpgradeStateManager.updateState(null);

        verifyUpgradeStateWritten(BUILD_NUMBER_FROM, READY_TO_UPGRADE);
        verifyClusterNotified(READY_TO_UPGRADE);
    }

    @Test
    public void mixedToReadyToRunUpgradeTasks() {
        mockClusterUpgradeState(BUILD_NUMBER_FROM, MIXED);
        mockClusterNodes(BUILD_NUMBER_TO, BUILD_NUMBER_TO);

        clusterUpgradeStateManager.updateState(null);

        verifyUpgradeStateWritten(BUILD_NUMBER_FROM, READY_TO_RUN_UPGRADE_TASKS);
        verifyClusterNotified(READY_TO_RUN_UPGRADE_TASKS);
    }

    @Test
    public void readyToRunUpgradeTasksToMixed() {
        mockClusterUpgradeState(BUILD_NUMBER_FROM, READY_TO_RUN_UPGRADE_TASKS);
        mockClusterNodes(BUILD_NUMBER_FROM, BUILD_NUMBER_TO);

        clusterUpgradeStateManager.updateState(null);

        verifyUpgradeStateWritten(BUILD_NUMBER_FROM, MIXED);
        verifyClusterNotified(MIXED);
    }

    @Test
    public void readyToUpgradeToReadyToRunUpgradeTasks() {
        mockClusterUpgradeState(BUILD_NUMBER_FROM, READY_TO_UPGRADE);
        mockClusterNodes(BUILD_NUMBER_TO, BUILD_NUMBER_TO);

        clusterUpgradeStateManager.updateState(null);

        verifyUpgradeStateWritten(BUILD_NUMBER_FROM, READY_TO_RUN_UPGRADE_TASKS);
        verifyClusterNotified(READY_TO_RUN_UPGRADE_TASKS);
    }

    @Test
    public void readyToRunUpgradeTasksToReadyToUpgrade() {
        mockClusterUpgradeState(BUILD_NUMBER_FROM, READY_TO_RUN_UPGRADE_TASKS);
        mockClusterNodes(BUILD_NUMBER_FROM, BUILD_NUMBER_FROM);

        clusterUpgradeStateManager.updateState(null);

        verifyUpgradeStateWritten(BUILD_NUMBER_FROM, READY_TO_UPGRADE);
        verifyClusterNotified(READY_TO_UPGRADE);
    }

    @Test
    public void clusterUpgradeStateIsAlwaysStableWithoutDarkFeature() {
        mockClusterUpgradeState(BUILD_NUMBER_TO, MIXED);
        mockApplicationBuildInfo(BUILD_NUMBER_FROM);
        when(featureManager.isEnabled(CLUSTER_UPGRADE_STATE_DARK_FEATURE)).thenReturn(false);

        NodeBuildInfo clusterBuildNumber = clusterUpgradeStateManager.getClusterBuildInfo();

        assertThat(clusterBuildNumber, is(BUILD_NUMBER_FROM));
    }

    @Test
    public void clusterBuildNumberMatchesBuildInfoWithoutDarkFeature() {
        mockClusterUpgradeState(BUILD_NUMBER_TO, MIXED);
        mockApplicationBuildInfo(BUILD_NUMBER_FROM);
        when(featureManager.isEnabled(CLUSTER_UPGRADE_STATE_DARK_FEATURE)).thenReturn(true);

        NodeBuildInfo clusterBuildNumber = clusterUpgradeStateManager.getClusterBuildInfo();

        assertThat(clusterBuildNumber, is(BUILD_NUMBER_TO));
    }

    @Test
    public void upgradesAreNotHandledByClusterWhenDarkFeatureIsOff() {
        when(featureManager.isEnabled(eq(CLUSTER_UPGRADE_STATE_DARK_FEATURE))).thenReturn(false);
        when(clusterUpgradeStateDao.getCurrent()).thenReturn(Optional.of(clusterUpgradeState(BUILD_NUMBER_FROM, STABLE)));

        assertThat(clusterUpgradeStateManager.areDelayedUpgradesHandledByCluster(), equalTo(false));
    }

    @Test
    public void upgradesAreNotHandledByClusterWhenDarkFeatureIsOffAndNotInStableState() {
        when(featureManager.isEnabled(eq(CLUSTER_UPGRADE_STATE_DARK_FEATURE))).thenReturn(false);
        when(clusterUpgradeStateDao.getCurrent()).thenReturn(Optional.of(clusterUpgradeState(BUILD_NUMBER_FROM, READY_TO_UPGRADE)));

        assertThat(clusterUpgradeStateManager.areDelayedUpgradesHandledByCluster(), equalTo(false));
    }

    @Test
    public void upgradesAreNotHandledByClusterWhenClusterInStableState() {
        when(featureManager.isEnabled(eq(CLUSTER_UPGRADE_STATE_DARK_FEATURE))).thenReturn(true);
        when(clusterUpgradeStateDao.getCurrent()).thenReturn(Optional.of(clusterUpgradeState(BUILD_NUMBER_FROM, STABLE)));

        assertThat(clusterUpgradeStateManager.areDelayedUpgradesHandledByCluster(), equalTo(false));
    }

    @Test
    public void upgradesAreHandledByCluster() {
        when(featureManager.isEnabled(eq(CLUSTER_UPGRADE_STATE_DARK_FEATURE))).thenReturn(true);
        when(clusterUpgradeStateDao.getCurrent()).thenReturn(Optional.of(clusterUpgradeState(BUILD_NUMBER_FROM, READY_TO_UPGRADE)));

        assertThat(clusterUpgradeStateManager.areDelayedUpgradesHandledByCluster(), equalTo(true));
    }

    @Test
    public void getStateReturnsCorrectStateWhenNodesTimeOutToUpgrade() {
        mockClusterUpgradeState(BUILD_NUMBER_FROM, MIXED);
        mockClusterNodes(BUILD_NUMBER_TO);

        UpgradeState upgradeState = clusterUpgradeStateManager.getUpgradeState();

        verifyUpgradeStateWritten(BUILD_NUMBER_FROM, READY_TO_RUN_UPGRADE_TASKS);
        assertThat(upgradeState, is(READY_TO_RUN_UPGRADE_TASKS));
        verifyClusterNotified(READY_TO_RUN_UPGRADE_TASKS);
        verifyLiveNodesRefreshed();
    }

    @Test
    public void getStateReturnsCorrectStateWhenNodesTimeOutToRollback() {
        mockClusterUpgradeState(BUILD_NUMBER_FROM, MIXED);
        mockClusterNodes(BUILD_NUMBER_FROM);

        UpgradeState upgradeState = clusterUpgradeStateManager.getUpgradeState();

        verifyUpgradeStateWritten(BUILD_NUMBER_FROM, READY_TO_UPGRADE);
        assertThat(upgradeState, is(READY_TO_UPGRADE));
        verifyClusterNotified(READY_TO_UPGRADE);
        verifyLiveNodesRefreshed();
    }

    @Test
    public void canCancelUpgradeWhenNodesTimeOut() {
        mockClusterUpgradeState(BUILD_NUMBER_FROM, MIXED);
        mockClusterNodes(BUILD_NUMBER_FROM);

        clusterUpgradeStateManager.cancelUpgrade();

        verifyUpgradeStateWritten(BUILD_NUMBER_FROM, READY_TO_UPGRADE);
        verifyClusterNotified(READY_TO_UPGRADE);
        verifyUpgradeStateWritten(BUILD_NUMBER_FROM, STABLE);
        verifyEventPublished(JiraUpgradeCancelledEvent.class);
        verifyLiveNodesRefreshed();
    }

    @Test
    public void canApproveUpgradeWhenNodesTimeOut() {
        mockClusterUpgradeState(BUILD_NUMBER_FROM, MIXED);
        mockClusterNodes(BUILD_NUMBER_TO);

        clusterUpgradeStateManager.approveUpgrade();

        verifyUpgradeStateWritten(BUILD_NUMBER_FROM, READY_TO_RUN_UPGRADE_TASKS);
        verifyEventPublished(JiraUpgradeApprovedEvent.class);
        verifyClusterNotified(READY_TO_RUN_UPGRADE_TASKS);
        verifyLiveNodesRefreshed();
    }

    private void mockClusterUpgradeState(NodeBuildInfo buildNumber, UpgradeState upgradeState) {
        when(clusterUpgradeStateDao.getCurrent()).thenReturn(Optional.of(clusterUpgradeState(buildNumber, upgradeState)));
    }

    private void mockClusterNodes(final NodeBuildInfo...buildNumbers) {
        Set<Node> nodes = range(0, buildNumbers.length)
                .mapToObj(i -> new Node("node" + i, ACTIVE, 1L, "ip", 1L, buildNumbers[i].getBuildNumber(), buildNumbers[i].getVersion()))
                .collect(toSet());
        when(clusterManager.findLiveNodes()).thenReturn(nodes);
    }

    private void mockApplicationBuildInfo(final NodeBuildInfo buildInfo) {
        when(nodeBuildInfoFactory.currentApplicationInfo()).thenReturn(buildInfo);
    }

    private void verifyLiveNodesRefreshed() {
        InOrder inOrder = inOrder(clusterManager);
        inOrder.verify(clusterManager, times(1)).refreshLiveNodes();
        inOrder.verify(clusterManager, times(1)).findLiveNodes();
    }

    private void verifyUpgradeStateWritten(final NodeBuildInfo buildNumberFrom, final UpgradeState state) {
        verify(clusterUpgradeStateDao, times(1)).writeState(buildNumberFrom, state);
    }

    private void verifyClusterNotified(final UpgradeState upgradeState) {
        InOrder inOrder = inOrder(clusterLock, messageHandlerService);
        inOrder.verify(clusterLock, times(1)).unlock();
        inOrder.verify(messageHandlerService, times(1)).sendRemote(CLUSTER_UPGRADE_STATE_CHANGED, upgradeState.toString());
    }

    private void verifyNoUpgradeStateWritten() {
        verify(clusterUpgradeStateDao, never()).writeState(any(), any());
    }

    private void verifyClusterNotNotified() {
        verify(messageHandlerService, never()).sendRemote(any(), any());
    }
    private void verifyNoEventPublished() {
        verify(eventPublisher, never()).publish(any());
    }

    private void verifyEventPublished(final Class<?> eventClass) {
        InOrder inOrder = inOrder(clusterLock, eventPublisher);
        inOrder.verify(clusterLock, times(1)).unlock();
        inOrder.verify(eventPublisher, times(1)).publish(any(eventClass));
    }
}
