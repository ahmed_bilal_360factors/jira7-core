package com.atlassian.jira.issue.search.searchers.util;

import com.atlassian.jira.issue.search.searchers.transformer.FieldFlagOperandRegistry;
import com.atlassian.jira.jql.operand.JqlOperandResolver;
import com.atlassian.jira.jql.resolver.IndexInfoResolver;
import com.atlassian.jira.jql.resolver.NameResolver;
import com.atlassian.jira.mock.project.MockVersion;
import com.atlassian.jira.project.version.Version;
import com.atlassian.query.operand.SingleValueOperand;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestVersionIndexedInputHelper {
    @Rule
    public final MethodRule initMockito = MockitoJUnit.rule();

    @Mock
    private NameResolver<Version> versionResolver;
    @Mock
    private IndexInfoResolver<Version> versionIndexInfoResolver;
    @Mock
    private JqlOperandResolver operandResolver;
    @Mock
    private FieldFlagOperandRegistry fieldFlagOperandRegistry;

    @InjectMocks
    private VersionIndexedInputHelper helper;

    @Test
    public void testCreateSingleValueOperandFromIdIsntNumber() throws Exception {
        assertEquals(new SingleValueOperand("test"), helper.createSingleValueOperandFromId("test"));
    }

    @Test
    public void testCreateSingleValueOperandFromIdIsNotAVersionNumber() throws Exception {
        assertEquals(new SingleValueOperand(123l), helper.createSingleValueOperandFromId("123"));
        verify(versionResolver).get(123l);
    }

    @Test
    public void testCreateSingleValueOperandFromIdIsAVersion() throws Exception {
        when(versionResolver.get(123l)).thenReturn(new MockVersion(123l, "Version 1"));
        assertEquals(new SingleValueOperand("Version 1"), helper.createSingleValueOperandFromId("123"));
    }

}
