package com.atlassian.jira.imports.project.customfield;

import com.atlassian.jira.imports.project.mapper.ProjectImportMapper;
import com.atlassian.jira.imports.project.mapper.ProjectImportMapperImpl;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @since v3.13
 */
public class TestVersionCustomFieldImporter {
    private ProjectImportMapper projectImportMapper;

    @Before
    public void setUp() {
        projectImportMapper = new ProjectImportMapperImpl(null, null);
    }

    @Test
    public void testCanMapImportValue() throws Exception {
        final VersionCustomFieldImporter versionCustomFieldImporter = new VersionCustomFieldImporter();
        assertNull(versionCustomFieldImporter.canMapImportValue(null, null, null, null));
    }

    @Test
    public void testGetMappedImportValue() throws Exception {
        projectImportMapper.getVersionMapper().mapValue("12", "14");

        final ExternalCustomFieldValueImpl externalCustomFieldValue = new ExternalCustomFieldValueImpl("1", "2", "3");
        externalCustomFieldValue.setStringValue("12");

        final VersionCustomFieldImporter versionCustomFieldImporter = new VersionCustomFieldImporter();

        final ProjectCustomFieldImporter.MappedCustomFieldValue importValue = versionCustomFieldImporter.getMappedImportValue(projectImportMapper, externalCustomFieldValue, null);
        assertEquals("14", importValue.getValue());
    }

    @Test
    public void testGetMappedImportValueNoMapping() throws Exception {
        // given
        final CustomField mockCustomField = mock(CustomField.class);
        when(mockCustomField.getName()).thenReturn("Test Custom Field");

        final FieldConfig mockFieldConfig = mock(FieldConfig.class);
        when(mockFieldConfig.getCustomField()).thenReturn(mockCustomField);

        final ExternalCustomFieldValueImpl externalCustomFieldValue = new ExternalCustomFieldValueImpl("1", "2", "3");
        externalCustomFieldValue.setStringValue("12.0");

        // when
        final VersionCustomFieldImporter versionCustomFieldImporter = new VersionCustomFieldImporter();
        final ProjectCustomFieldImporter.MappedCustomFieldValue importValue = versionCustomFieldImporter.getMappedImportValue(projectImportMapper, externalCustomFieldValue, mockFieldConfig);

        // then
        assertNull(importValue.getValue());
    }
}
