package com.atlassian.jira.upgrade;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.bc.license.JiraLicenseService;
import com.atlassian.jira.bc.license.JiraLicenseService.ValidationResult;
import com.atlassian.jira.index.request.ReindexRequestType;
import com.atlassian.jira.license.BuildVersionLicenseCheck;
import com.atlassian.jira.license.LicenseCheck;
import com.atlassian.jira.license.LicenseDetails;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.upgrade.util.BuildNumberDao;
import com.atlassian.jira.util.BuildUtilsInfo;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.upgrade.api.UpgradeContext;
import com.atlassian.upgrade.core.UpgradeTaskManager;
import com.atlassian.upgrade.core.dao.UpgradeTaskHistoryDao;
import com.google.common.collect.ImmutableList;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import java.util.Collections;
import java.util.Locale;
import java.util.Set;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anySet;
import static org.mockito.Matchers.anySetOf;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestLicenseCheckingUpgradeService {

    private static final String BUILD_VERSION = "7.1-SNAPSHOT";
    private static final String UPGRADE_FRAMEWORK_FAILED_UPGRADE_ERROR =
            "The upgrade task manager has returned a non-successful result from running the upgrades";

    @Rule
    public MethodRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private UpgradeTaskHistoryDao upgradeTaskHistoryDao;

    @Mock
    private UpgradeTaskManager upgradeTaskManager;

    @Mock
    private BuildNumberDao buildNumberDao;

    @Mock
    private BuildUtilsInfo buildUtilsInfo;

    @Mock
    private UpgradeVersionHistoryManager upgradeVersionHistoryManager;

    @Mock
    private UpgradeIndexManager upgradeIndexManager;

    @Mock
    private Set<ReindexRequestType> reindexRequestTypes;

    @Mock
    private UpgradeContext upgradeContext;

    @Mock
    private JiraLicenseService jiraLicenseService;

    @Mock
    private BuildVersionLicenseCheck buildVersionLicenseCheck;

    @Mock
    private LicenseDetails licenseDetails;

    @Mock
    private I18nHelper.BeanFactory i18nHelperFactory;

    @Mock
    private EventPublisher eventPublisher;

    @Mock
    private OfBizDelegator ofBizDelegator;

    @Captor
    private ArgumentCaptor<Set<ReindexRequestType>> requestTypesArgumentCaptor;

    private LicenseCheckingUpgradeService licenseCheckingUpgradeService;

    @Before
    public void setUp() {
        licenseCheckingUpgradeService = new LicenseCheckingUpgradeService(
                upgradeTaskHistoryDao,
                upgradeTaskManager,
                buildNumberDao,
                buildUtilsInfo,
                upgradeVersionHistoryManager,
                upgradeIndexManager,
                jiraLicenseService,
                buildVersionLicenseCheck,
                i18nHelperFactory,
                eventPublisher,
                ofBizDelegator
        );
        when(upgradeIndexManager.runReindexIfNeededAndAllowed(anySet())).thenReturn(true);
        when(buildUtilsInfo.getVersion()).thenReturn(BUILD_VERSION);
    }

    @Test
    public void shouldFailWhenLicenseIsTooOld() {
        when(jiraLicenseService.isLicenseSet()).thenReturn(true);
        when(jiraLicenseService.getLicenses()).thenReturn(ImmutableList.of(licenseDetails));
        when(buildVersionLicenseCheck.evaluate()).thenReturn(BuildVersionLicenseCheck.FAIL);

        final UpgradeResult upgradeResult = licenseCheckingUpgradeService.runUpgrades(
                reindexRequestTypes,
                upgradeContext
        );

        assertThat(upgradeResult.successful(), equalTo(false));
        assertThat(upgradeResult.getErrors(), hasSize(1));
    }

    @Test
    public void shouldFailWhenLicenseNotValid() {
        when(jiraLicenseService.isLicenseSet()).thenReturn(true);
        when(jiraLicenseService.getLicenses()).thenReturn(ImmutableList.of(licenseDetails));
        when(buildVersionLicenseCheck.evaluate()).thenReturn(BuildVersionLicenseCheck.PASS);
        final String failureMessage = "License didn't validate, gg";
        final ValidationResult licenseValidationResult = mock(ValidationResult.class);
        final SimpleErrorCollection errorCollection = new SimpleErrorCollection();
        errorCollection.addErrorMessage(failureMessage);
        when(licenseValidationResult.getErrorCollection()).thenReturn(errorCollection);

        when(i18nHelperFactory.getInstance(any(Locale.class))).thenReturn(null);
        when(jiraLicenseService.validate(any(I18nHelper.class))).thenReturn(ImmutableList.of(licenseValidationResult));

        final UpgradeResult upgradeResult = licenseCheckingUpgradeService.runUpgrades(
                reindexRequestTypes,
                upgradeContext
        );

        assertThat(upgradeResult.successful(), equalTo(false));
        assertThat(upgradeResult.getErrors(), contains(failureMessage));
    }

    @Test
    public void shouldCompleteWhenLicenseValid() {

        newUpgradeManagerRunUpgradesIsSuccessful(5, 6);
        when(jiraLicenseService.isLicenseSet()).thenReturn(true);
        when(jiraLicenseService.getLicenses()).thenReturn(ImmutableList.of(licenseDetails));
        when(jiraLicenseService.validate(any())).thenReturn(Collections.emptyList());
        when(buildVersionLicenseCheck.evaluate()).thenReturn(LicenseCheck.PASS);

        final UpgradeResult upgradeResult = licenseCheckingUpgradeService.runUpgrades(
                reindexRequestTypes,
                upgradeContext
        );

        assertThat(upgradeResult.successful(), equalTo(true));
    }

    @Test
    public void shouldCompleteWhenReindexIsAllowed() {

        newUpgradeManagerRunUpgradesIsSuccessful(5, 6);

        final UpgradeResult upgradeResult = licenseCheckingUpgradeService.runUpgrades(
                reindexRequestTypes,
                upgradeContext
        );

        assertThat(upgradeResult.successful(), equalTo(true));
        verify(upgradeIndexManager).runReindexIfNeededAndAllowed(requestTypesArgumentCaptor.capture());
        assertThat(requestTypesArgumentCaptor.getValue(), equalTo(reindexRequestTypes));
    }

    @Test
    public void shouldFailIfFrameworkFailsToUpgrade() {
        when(upgradeTaskManager.upgradeHostApp(any(UpgradeContext.class))).thenReturn(false);
        when(upgradeTaskHistoryDao.getDatabaseBuildNumber(anyString())).thenReturn(5, 5);

        final UpgradeResult upgradeResult = licenseCheckingUpgradeService.runUpgrades(
                reindexRequestTypes,
                upgradeContext
        );

        assertThat(upgradeResult.successful(), equalTo(false));
        assertThat(upgradeResult.getErrors(), contains(UPGRADE_FRAMEWORK_FAILED_UPGRADE_ERROR));
    }

    @Test
    public void shouldUpdateBuildNumberAndVersion() {

        newUpgradeManagerRunUpgradesIsSuccessful(5, 6);
        when(buildUtilsInfo.getCurrentBuildNumber()).thenReturn("6");

        final UpgradeResult upgradeResult = licenseCheckingUpgradeService.runUpgrades(
                reindexRequestTypes,
                upgradeContext
        );

        assertThat(upgradeResult.successful(), Matchers.equalTo(true));
        verify(buildNumberDao, times(1)).setDatabaseBuildNumber(eq("6"));
        verify(buildNumberDao, times(1)).setJiraVersion(eq(BUILD_VERSION));
        verify(buildNumberDao, times(1)).setMinimumDowngradeVersion();
        verify(upgradeVersionHistoryManager, times(1)).addUpgradeVersionHistory(eq(6), eq(BUILD_VERSION));
    }

    @Test
    public void shouldUpdateBuildNumberWhenUpgradeTasksAreMissing() {

        newUpgradeManagerRunUpgradesIsSuccessful(5, 6);
        when(buildUtilsInfo.getCurrentBuildNumber()).thenReturn("10");

        final UpgradeResult upgradeResult = licenseCheckingUpgradeService.runUpgrades(
                reindexRequestTypes,
                upgradeContext
        );

        assertThat(upgradeResult.successful(), Matchers.equalTo(true));
        verify(buildNumberDao, times(1)).setDatabaseBuildNumber(eq("10"));
        verify(buildNumberDao, times(1)).setJiraVersion(eq(BUILD_VERSION));
        verify(buildNumberDao, times(1)).setMinimumDowngradeVersion();
        verify(upgradeVersionHistoryManager, times(1)).addUpgradeVersionHistory(eq(6), eq(BUILD_VERSION));
    }

    @Test
    public void shouldUpdateBuildNumberIfNoUpgradeTasks() {
        when(upgradeTaskManager.upgradeHostApp(any(UpgradeContext.class))).thenReturn(true);
        when(buildUtilsInfo.getCurrentBuildNumber()).thenReturn("5");

        final UpgradeResult upgradeResult = licenseCheckingUpgradeService.runUpgrades(
                reindexRequestTypes,
                upgradeContext
        );

        assertThat(upgradeResult.successful(), Matchers.equalTo(true));
        verify(buildNumberDao, times(1)).setDatabaseBuildNumber(eq("5"));
        verify(buildNumberDao, times(1)).setJiraVersion(eq(BUILD_VERSION));
        verify(buildNumberDao, times(1)).setMinimumDowngradeVersion();
        verify(upgradeVersionHistoryManager, never()).addUpgradeVersionHistory(anyInt(), anyString());
    }

    @Test
    public void shouldUpdateBuildNumberAndVersionEvenWhenFails() {
        when(upgradeTaskManager.upgradeHostApp(any(UpgradeContext.class))).thenReturn(false);
        when(upgradeTaskHistoryDao.getDatabaseBuildNumber(anyString())).thenReturn(5, 6);

        final UpgradeResult upgradeResult = licenseCheckingUpgradeService.runUpgrades(
                reindexRequestTypes,
                upgradeContext
        );

        assertThat(upgradeResult.successful(), Matchers.equalTo(false));
        verify(buildNumberDao, times(1)).setDatabaseBuildNumber(eq("6"));
        verify(buildNumberDao, times(1)).setJiraVersion(eq(BUILD_VERSION));
        verify(buildNumberDao, times(1)).setMinimumDowngradeVersion();
        verify(upgradeVersionHistoryManager, never()).addUpgradeVersionHistory(anyInt(), anyString());
        verify(buildUtilsInfo, never()).getCurrentBuildNumber();
    }

    @Test
    public void shouldFailOnException() {
        when(upgradeTaskManager.upgradeHostApp(any(UpgradeContext.class))).thenThrow(new RuntimeException("Exception"));

        final UpgradeResult upgradeResult = licenseCheckingUpgradeService.runUpgrades(
                reindexRequestTypes,
                upgradeContext
        );

        assertThat(upgradeResult.successful(), Matchers.equalTo(false));
    }

    @Test
    public void shouldUpdateVersionHistoryEvenIfReindexingFailed() {
        when(upgradeTaskManager.upgradeHostApp(any(UpgradeContext.class))).thenReturn(true);
        when(upgradeTaskHistoryDao.getDatabaseBuildNumber(anyString())).thenReturn(5, 6);
        when(buildUtilsInfo.getCurrentBuildNumber()).thenReturn("6");
        when(upgradeIndexManager.runReindexIfNeededAndAllowed(anySetOf(ReindexRequestType.class))).thenReturn(false);

        final UpgradeResult upgradeResult = licenseCheckingUpgradeService.runUpgrades(
                reindexRequestTypes,
                upgradeContext
        );

        assertThat(upgradeResult.successful(), Matchers.equalTo(false));
        verify(buildNumberDao, times(1)).setDatabaseBuildNumber(eq("6"));
        verify(buildNumberDao, times(1)).setJiraVersion(eq(BUILD_VERSION));
        verify(buildNumberDao, times(1)).setMinimumDowngradeVersion();
        verify(upgradeVersionHistoryManager, times(1)).addUpgradeVersionHistory(anyInt(), anyString());
    }

    private void newUpgradeManagerRunUpgradesIsSuccessful(final int from, final int to) {
        when(upgradeTaskManager.upgradeHostApp(any(UpgradeContext.class))).thenReturn(true);
        when(upgradeTaskHistoryDao.getDatabaseBuildNumber(anyString())).thenReturn(from, to);
    }
}