package com.atlassian.jira.issue.fields.csv;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.export.FieldExportParts;
import com.atlassian.jira.issue.fields.OriginalEstimateSystemField;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;

import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.I18nHelper;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.mockito.Mock;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

public class OriginalEstimateSystemFieldCsvExportTest {
    @Rule
    public final RuleChain mockito = MockitoMocksInContainer.forTest(this);

    @Mock
    private JiraAuthenticationContext authenticationContext;

    @Mock
    private I18nHelper i18nHelper;

    @Before
    public void setUpAuthenticationContext() throws Exception {
        when(authenticationContext.getI18nHelper()).thenReturn(i18nHelper);
    }

    private static final Long ORIGINAL_ESTIMATE = 500L;

    @Mock
    private Issue issue;

    private OriginalEstimateSystemField field;

    @Before
    public void setUp() {
        field = new OriginalEstimateSystemField(null, null, authenticationContext);
    }

    @Test
    public void testCsvRepresentationForExpectedTypeAndValues() {
        when(issue.getOriginalEstimate()).thenReturn(ORIGINAL_ESTIMATE);

        final FieldExportParts representation = field.getRepresentationFromIssue(issue);
        assertThat(representation.getParts(), hasSize(1));
        assertThat(representation.getParts().get(0).getValues()::iterator, contains(String.valueOf(ORIGINAL_ESTIMATE)));
    }

    @Test
    public void testCsvRepresentationWhenOriginalEstimateIsNull() {
        when(issue.getOriginalEstimate()).thenReturn(null);

        final FieldExportParts representation = field.getRepresentationFromIssue(issue);
        assertThat(representation.getParts(), hasSize(1));
        assertThat(representation.getParts().get(0).getValues()::iterator, contains(""));
    }
}
