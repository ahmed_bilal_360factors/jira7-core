package com.atlassian.jira.jql.util;

import com.atlassian.jira.issue.customfields.converters.DoubleConverter;
import com.atlassian.jira.issue.customfields.impl.FieldValidationException;
import com.atlassian.jira.jql.operand.QueryLiteral;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static com.atlassian.jira.jql.operand.SimpleLiteralFactory.createLiteral;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestNumberIndexValueConverter {
    @Rule
    public final MockitoRule initMockito = MockitoJUnit.rule();
    @Mock
    private DoubleConverter doubleConverter;
    private NumberIndexValueConverter valueConverter;

    @Before
    public void setUp() throws Exception {
        valueConverter = new NumberIndexValueConverter(doubleConverter);
    }

    @Test
    public void testConvertToIndexValueEmpty() throws Exception {
        assertNull(valueConverter.convertToIndexValue(new QueryLiteral()));
    }

    @Test
    public void testConvertToIndexValueValid() throws Exception {
        when(doubleConverter.getStringForLucene("10")).thenReturn("10.000");

        assertEquals("10.000", valueConverter.convertToIndexValue(createLiteral(10L)));
    }

    @Test
    public void testConvertToIndexValueInvalid() throws Exception {
        Mockito.doThrow(new FieldValidationException("blah")).when(doubleConverter).getStringForLucene("10.A");

        NumberIndexValueConverter numberIndexValueConverter = new NumberIndexValueConverter(doubleConverter);
        assertNull(numberIndexValueConverter.convertToIndexValue(createLiteral("10.A")));
    }
}
