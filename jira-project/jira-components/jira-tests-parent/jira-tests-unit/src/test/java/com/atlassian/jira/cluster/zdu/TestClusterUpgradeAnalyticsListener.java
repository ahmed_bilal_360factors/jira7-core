package com.atlassian.jira.cluster.zdu;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.cluster.ClusterManager;
import com.atlassian.jira.cluster.Node;
import com.atlassian.jira.cluster.zdu.analytics.ClusterUpgradeAnalyticsListener;
import com.atlassian.jira.cluster.zdu.analytics.NodeStartedAnalytics;
import com.atlassian.jira.cluster.zdu.analytics.NodeStoppedAnalytics;
import com.atlassian.jira.cluster.zdu.analytics.UpgradeApprovedAnalytics;
import com.atlassian.jira.cluster.zdu.analytics.UpgradeCancelledAnalytics;
import com.atlassian.jira.cluster.zdu.analytics.UpgradeCompletedAnalytics;
import com.atlassian.jira.cluster.zdu.analytics.UpgradeStartedAnalytics;
import com.atlassian.jira.service.services.analytics.start.JiraStartAnalyticEvent;
import com.atlassian.jira.service.services.analytics.stop.JiraStopAnalyticEvent;
import com.atlassian.jira.util.BuildUtilsInfo;
import java.util.Set;
import java.util.UUID;
import java.util.stream.IntStream;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static com.atlassian.collectors.CollectorsUtil.toImmutableSet;
import static com.atlassian.jira.cluster.zdu.UpgradeState.READY_TO_UPGRADE;
import static com.atlassian.jira.cluster.zdu.UpgradeState.STABLE;
import static org.hamcrest.CoreMatchers.any;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNot.not;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @since v7.3
 */
@RunWith(MockitoJUnitRunner.class)
public class TestClusterUpgradeAnalyticsListener {
    @Mock
    private EventPublisher eventPublisher;

    @Mock
    private ClusterUpgradeStateManager clusterUpgradeStateManager;

    @Mock
    private ClusterManager clusterManager;

    @Mock
    private BuildUtilsInfo buildUtilsInfo;

    private ClusterUpgradeAnalyticsListener clusterUpgradeAnalyticsListener;

    @Before
    public void setUp() {
        clusterUpgradeAnalyticsListener = new ClusterUpgradeAnalyticsListener(
                eventPublisher,
                clusterUpgradeStateManager,
                clusterManager,
                buildUtilsInfo);
    }

    @Test
    public void triggersAnEventWhenJiraStartsUp() {
        mockUpgradeState(73001, STABLE);
        mockDatabaseBuildNumber(73000);
        mockNodeId("node1");
        JiraStartAnalyticEvent jiraStartedEvent = mock(JiraStartAnalyticEvent.class);

        clusterUpgradeAnalyticsListener.onNodeStarted(jiraStartedEvent);

        NodeStartedAnalytics analytics = verifyPublishedEvent(NodeStartedAnalytics.class);
        assertThat("the original node id is obscured", analytics.getNodeId(), is(not("node1")));
        assertThat("the sent node id is a UUID", isUUID(analytics.getNodeId()), is(true));
        assertThat(analytics.getDatabaseBuildNumber(), is(73000));
        assertThat(analytics.getNodeBuildNumber(), is(73001L));
        assertThat("cluster state is a string", analytics.getClusterState(), is("STABLE"));
    }

    @Test
    public void safeNullNodeIdAtStartup() {
        mockUpgradeState(73001, STABLE);
        mockDatabaseBuildNumber(73000);
        mockNodeId(null);   // This can happen outside of clustered mode.
        JiraStartAnalyticEvent jiraStartedEvent = mock(JiraStartAnalyticEvent.class);

        clusterUpgradeAnalyticsListener.onNodeStarted(jiraStartedEvent);

        verifyNotPublished(NodeStartedAnalytics.class);
    }

    @Test
    public void triggersAnEventWhenJiraStops() {
        mockUpgradeState(73001, STABLE);
        mockDatabaseBuildNumber(73000);
        mockNodeId("node1");
        JiraStopAnalyticEvent jiraStoppedEvent = mock(JiraStopAnalyticEvent.class);

        clusterUpgradeAnalyticsListener.onNodeStopped(jiraStoppedEvent);

        NodeStoppedAnalytics analytics = verifyPublishedEvent(NodeStoppedAnalytics.class);
        assertThat("the original node id is obscured", analytics.getNodeId(), is(not("node1")));
        assertThat("the sent node id is a UUID", isUUID(analytics.getNodeId()), is(true));
        assertThat(analytics.getDatabaseBuildNumber(), is(73000));
        assertThat(analytics.getNodeBuildNumber(), is(73001L));
        assertThat("cluster state is a string", analytics.getClusterState(), is("STABLE"));
    }

    @Test
    public void safeNullNodeIdAtShutdown() {
        mockUpgradeState(73001, STABLE);
        mockDatabaseBuildNumber(73000);
        mockNodeId(null);   // This can happen outside of clustered mode.
        JiraStopAnalyticEvent jiraStoppedEvent = mock(JiraStopAnalyticEvent.class);

        clusterUpgradeAnalyticsListener.onNodeStopped(jiraStoppedEvent);

        verifyNotPublished(NodeStoppedAnalytics.class);
    }

    @Test
    public void triggersAnEventWhenAnUpgradeBegins() {
        NodeBuildInfo nodeBuildInfo = mockUpgradeState(73000, READY_TO_UPGRADE);
        mockClusterSize(2);
        JiraUpgradeStartedEvent jiraUpgradeStartedEvent = new JiraUpgradeStartedEvent(nodeBuildInfo);

        clusterUpgradeAnalyticsListener.onUpgradeStarted(jiraUpgradeStartedEvent);

        UpgradeStartedAnalytics analytics = verifyPublishedEvent(UpgradeStartedAnalytics.class);
        assertThat(analytics.getNodeBuildNumber(), is(73000L));
        assertThat(analytics.getNodeCount(), is(2));
    }

    @Test
    public void triggersAnEventWhenAnUpgradeIsApproved() {
        NodeBuildInfo fromBuildInfo = mockNodeBuildInfo(73000);
        NodeBuildInfo toBuildInfo = mockNodeBuildInfo(73001);
        mockClusterSize(2);
        JiraUpgradeApprovedEvent jiraUpgradeApprovedEvent = new JiraUpgradeApprovedEvent(fromBuildInfo, toBuildInfo);

        clusterUpgradeAnalyticsListener.onUpgradeApproved(jiraUpgradeApprovedEvent);

        UpgradeApprovedAnalytics analytics = verifyPublishedEvent(UpgradeApprovedAnalytics.class);
        assertThat(analytics.getFromBuildNumber(), is(73000L));
        assertThat(analytics.getToBuildNumber(), is(73001L));
        assertThat(analytics.getNodeCount(), is(2));
    }

    @Test
    public void triggersAnEventWhenAnUpgradeIsComplete() {
        NodeBuildInfo fromBuildInfo = mockNodeBuildInfo(73000);
        NodeBuildInfo toBuildInfo = mockNodeBuildInfo(73001);
        mockClusterSize(2);
        JiraUpgradeFinishedEvent jiraUpgradeFinishedEvent = new JiraUpgradeFinishedEvent(fromBuildInfo, toBuildInfo);

        clusterUpgradeAnalyticsListener.onUpgradeFinished(jiraUpgradeFinishedEvent);

        UpgradeCompletedAnalytics analytics = verifyPublishedEvent(UpgradeCompletedAnalytics.class);
        assertThat(analytics.getFromBuildNumber(), is(73000L));
        assertThat(analytics.getToBuildNumber(), is(73001L));
        assertThat(analytics.getNodeCount(), is(2));
    }

    @Test
    public void triggersAnEventWhenAnUpgradeIsCancelled() {
        NodeBuildInfo nodeBuildInfo = mockUpgradeState(73001, READY_TO_UPGRADE);
        mockDatabaseBuildNumber(73000);
        mockClusterSize(2);
        JiraUpgradeCancelledEvent jiraUpgradeCancelledEvent = new JiraUpgradeCancelledEvent(nodeBuildInfo);

        clusterUpgradeAnalyticsListener.onUpgradeCancelled(jiraUpgradeCancelledEvent);

        UpgradeCancelledAnalytics analytics = verifyPublishedEvent(UpgradeCancelledAnalytics.class);
        assertThat(analytics.getDatabaseBuildNumber(), is(73000));
        assertThat(analytics.getNodeBuildNumber(), is(73001L));
        assertThat(analytics.getNodeCount(), is(2));
    }

    private NodeBuildInfo mockUpgradeState(long nodeBuildNumber, UpgradeState state) {
        NodeBuildInfo nodeBuildInfo = mockNodeBuildInfo(nodeBuildNumber);
        when(clusterUpgradeStateManager.getClusterBuildInfo()).thenReturn(nodeBuildInfo);
        when(clusterUpgradeStateManager.getUpgradeState()).thenReturn(state);
        return nodeBuildInfo;
    }

    private NodeBuildInfo mockNodeBuildInfo(long nodeBuildNumber) {
        NodeBuildInfo nodeBuildInfo = mock(NodeBuildInfo.class);
        when(nodeBuildInfo.getBuildNumber()).thenReturn(nodeBuildNumber);
        return nodeBuildInfo;
    }

    private void mockDatabaseBuildNumber(int databaseBuildNumber) {
        when(buildUtilsInfo.getDatabaseBuildNumber()).thenReturn(databaseBuildNumber);
    }

    private void mockNodeId(String nodeId) {
        when(clusterManager.getNodeId()).thenReturn(nodeId);
    }

    private void mockClusterSize(int numberOfNodes) {
        final Set<Node> mockedNodes = IntStream.rangeClosed(1, numberOfNodes)
                .mapToObj(i -> mock(Node.class))
                .collect(toImmutableSet());
        when(clusterManager.findLiveNodes()).thenReturn(mockedNodes);
    }

    private <T> T verifyPublishedEvent(Class<T> eventClass) {
        ArgumentCaptor<T> analytics = ArgumentCaptor.forClass(eventClass);
        verify(eventPublisher, times(1)).publish(analytics.capture());
        return analytics.getValue();
    }

    private void verifyNotPublished(Class<?> eventClass) {
        verify(eventPublisher, never()).publish(any(eventClass));
    }

    private boolean isUUID(String value) {
        try {
            UUID.fromString(value);
            return true;
        } catch (IllegalArgumentException e) {
            return false;
        }
    }
}
