package com.atlassian.jira.issue.export;

import org.junit.Before;
import org.junit.Test;

import java.util.stream.Stream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.collection.IsIterableContainingInOrder.contains;


public class TestFieldExportPartsBuilder {

    FieldExportPartsBuilder builder;

    @Before
    public void setUp() {
        builder = new FieldExportPartsBuilder();
    }

    @Test(expected = IllegalStateException.class)
    public void testAddingSameIdForItemThrowsException() {
        final String id = "id";
        builder.addItem(id, "label", "value");
        builder.addItem(id, "label", "value");
    }

    @Test
    public void testAddingItemsKeepsTheOrdering() {
        builder.addItem("first", "label", "value");
        builder.addItem("second", "label", "value");

        FieldExportParts parts = builder.build();
        assertThat(parts.getParts(), hasSize(2));
        assertThat(parts.getParts().get(0).getId(), equalTo("first"));
        assertThat(parts.getParts().get(1).getId(), equalTo("second"));
    }

    @Test
    public void testBuildingSinglePartRepresentationWithSingleValue() {
        FieldExportParts parts = FieldExportPartsBuilder.buildSinglePartRepresentation("id", "label", "value");

        assertThat(parts.getParts(), hasSize(1));
        assertThat(parts.getParts().get(0).getId(), equalTo("id"));
        assertThat(parts.getParts().get(0).getItemLabel(), equalTo("label"));
        assertThat(parts.getParts().get(0).getValues()::iterator, contains("value"));
    }


    @Test
    public void testBuildingSinglePartRepresentationWithMultipleValue() {
        FieldExportParts parts = FieldExportPartsBuilder.buildSinglePartRepresentation("id", "label", Stream.of("value", "value2"));

        assertThat(parts.getParts(), hasSize(1));
        assertThat(parts.getParts().get(0).getId(), equalTo("id"));
        assertThat(parts.getParts().get(0).getItemLabel(), equalTo("label"));
        assertThat(parts.getParts().get(0).getValues()::iterator, contains("value", "value2"));
    }

    @Test
    public void testEmptyRepresentation() {
        FieldExportPartsBuilder builder = new FieldExportPartsBuilder();
        FieldExportParts parts = builder.build();
        assertThat(parts.getParts(), hasSize(0));
    }
}