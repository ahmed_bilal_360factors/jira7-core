package com.atlassian.jira.bc.user.search;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.crowd.embedded.ofbiz.OfBizUser;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.search.MatchAllDocsQuery;
import org.apache.lucene.search.Sort;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.RAMDirectory;
import org.hamcrest.Matchers;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.List;

import static com.atlassian.jira.crowd.embedded.ofbiz.OfBizUserFactory.makeUser;
import static org.junit.Assert.assertThat;

public class TestDirectoryUserIndexer {

    private final Directory luceneDirectory = new RAMDirectory();
    private final Analyzer luceneAnalyzer = new UserNameAnalyzer();
    private final DirectoryUserIndexer sut = new DirectoryUserIndexer(luceneDirectory, luceneAnalyzer);

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    // RUN-1008 user names with capital letters to make sure deindexing works for usernames that are not lower case only
    private final OfBizUser fred = makeUser(1, 1, "Fred", "Fred Anderson", "anderson@example.com", true);
    private final OfBizUser jack = makeUser(1, 2, "Jack", "Jack Smith", "smith@example.com", true);

    @Test
    public void testAddedUsersAreSearchable() throws Exception {
        sut.replaceAllUsers((consumer) -> {
            consumer.accept(fred);
            consumer.accept(jack);
        });

        assertThat(searchAll(sut), Matchers.containsInAnyOrder(fred, jack));
    }

    @Test
    public void testDeindexedUserNotPresent() throws Exception {
        sut.replaceAllUsers((consumer) -> {
            consumer.accept(fred);
            consumer.accept(jack);
        });

        sut.deindex(new UserId(jack.getName(), jack.getDirectoryId()));

        assertThat(searchAll(sut), Matchers.containsInAnyOrder(fred));
    }

    @Test
    public void testUserDeindexedByIdNotPresent() throws Exception {
        sut.replaceAllUsers((consumer) -> {
            consumer.accept(fred);
            consumer.accept(jack);
        });

        sut.deindexById(jack.getId());

        assertThat(searchAll(sut), Matchers.containsInAnyOrder(fred));
    }

    private List<User> searchAll(final DirectoryUserIndexer userIndexer) {
        return userIndexer.search(new MatchAllDocsQuery(), 0, 10, Sort.INDEXORDER);
    }


}