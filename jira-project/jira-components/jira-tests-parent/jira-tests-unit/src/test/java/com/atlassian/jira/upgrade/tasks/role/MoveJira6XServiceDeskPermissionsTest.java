package com.atlassian.jira.upgrade.tasks.role;

import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.fugue.Option;
import com.atlassian.jira.application.ApplicationKeys;
import com.google.common.collect.ImmutableList;
import org.hamcrest.MatcherAssert;
import org.junit.Before;
import org.junit.Test;

import static com.atlassian.fugue.Option.none;
import static com.atlassian.fugue.Option.some;
import static com.atlassian.jira.test.util.lic.sd.ServiceDeskLicenses.LICENSE_SERVICE_DESK_ABP;
import static com.atlassian.jira.test.util.lic.sd.ServiceDeskLicenses.LICENSE_SERVICE_DESK_ABP_ACADEMIC;
import static com.atlassian.jira.test.util.lic.sd.ServiceDeskLicenses.LICENSE_SERVICE_DESK_RBP;
import static com.atlassian.jira.test.util.lic.sd.ServiceDeskLicenses.LICENSE_SERVICE_DESK_TBP;
import static com.atlassian.jira.upgrade.tasks.role.TestUtils.emptyState;
import static com.atlassian.jira.upgrade.tasks.role.TestUtils.newGroup;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

public class MoveJira6XServiceDeskPermissionsTest {

    private ApplicationRole serviceDeskRole;
    private MoveJira6xServiceDeskPermissions task;
    private MoveJira6xTBPServiceDeskPermissions tbpTask;
    private MoveJira6xABPServiceDeskPermissions abpTask;
    private Jira6xServiceDeskLicenseProvider jira6xServiceDeskLicenseProvider;
    private ServiceDeskPropertySetDao serviceDeskPropertySetDao;

    @Before
    public void setup() {
        serviceDeskPropertySetDao = mock(ServiceDeskPropertySetDao.class);
        jira6xServiceDeskLicenseProvider = mock(Jira6xServiceDeskLicenseProvider.class);

        tbpTask = mock(MoveJira6xTBPServiceDeskPermissions.class);
        when(tbpTask.migrate(any(MigrationState.class), anyBoolean())).thenReturn(emptyState());

        abpTask = mock(MoveJira6xABPServiceDeskPermissions.class);
        when(abpTask.migrate(any(MigrationState.class), anyBoolean())).thenReturn(emptyState());

        task = new MoveJira6xServiceDeskPermissions(jira6xServiceDeskLicenseProvider, serviceDeskPropertySetDao, tbpTask, abpTask);

        serviceDeskRole = ApplicationRole.forKey(ApplicationKeys.SERVICE_DESK);
    }

    @Test
    public void executesTbpTaskIfThereIsTierBasedPricingLicenseInThePluginStore() {
        Group group1 = newGroup("group1");
        serviceDeskRole = serviceDeskRole.addGroup(group1);

        when(jira6xServiceDeskLicenseProvider.serviceDeskLicense())
                .thenReturn(some(TestUtils.toLicense(LICENSE_SERVICE_DESK_TBP)));
        MigrationState tbpState = emptyState()
                .changeApplicationRole(ApplicationKeys.SERVICE_DESK, appRole -> appRole.addGroup(group1));
        when(tbpTask.migrate(any(MigrationState.class), anyBoolean())).thenReturn(tbpState);

        final MigrationState initialState = emptyState();
        MigrationState migratedState = task.migrate(initialState, false);

        MatcherAssert.assertThat(migratedState, new MigrationStateMatcher()
                .roles(new ApplicationRoles(ImmutableList.of(serviceDeskRole)))
                .anyLog());
        verify(tbpTask).migrate(initialState, false);
    }

    @Test
    public void executesAbpTaskIfThereIsAgentBasedPricingLicenseInThePluginStore() {
        Group group1 = newGroup("group1");
        serviceDeskRole = serviceDeskRole.addGroup(group1);
        when(jira6xServiceDeskLicenseProvider.serviceDeskLicense())
                .thenReturn(some(TestUtils.toLicense(LICENSE_SERVICE_DESK_ABP_ACADEMIC)));

        final MigrationState initialState = emptyState();
        MigrationState abpState = initialState
                .changeApplicationRole(ApplicationKeys.SERVICE_DESK, appRole -> appRole.addGroup(group1));
        when(abpTask.migrate(any(MigrationState.class), anyBoolean())).thenReturn(abpState);


        MigrationState migrate = task.migrate(initialState, true);
        MatcherAssert.assertThat(migrate, new MigrationStateMatcher()
                .roles(new ApplicationRoles(ImmutableList.of(serviceDeskRole)))
                .anyLog());
        verify(abpTask).migrate(initialState, true);
    }

    @Test
    public void executesAbpTaskIfThereIsRoleBasedPricingLicenseInThePluginStore() {
        // given
        Group group1 = newGroup("group1");
        serviceDeskRole = serviceDeskRole.addGroup(group1);

        when(jira6xServiceDeskLicenseProvider.serviceDeskLicense())
                .thenReturn(some(TestUtils.toLicense(LICENSE_SERVICE_DESK_RBP)));

        final MigrationState initialState = emptyState();
        MigrationState abpState = initialState
                .changeApplicationRole(ApplicationKeys.SERVICE_DESK, appRole -> appRole.addGroup(group1));
        when(abpTask.migrate(any(MigrationState.class), anyBoolean())).thenReturn(abpState);

        MigrationState migrate = task.migrate(initialState, false);
        // then
        MatcherAssert.assertThat(migrate, new MigrationStateMatcher()
                .roles(new ApplicationRoles(ImmutableList.of(serviceDeskRole)))
                .anyLog());

        verify(abpTask).migrate(initialState, false);
    }

    @Test
    public void doesNotMigrateIfThereIsNoLicense() {
        when(jira6xServiceDeskLicenseProvider.serviceDeskLicense()).thenReturn(Option.none());

        assertThat(task.migrate(emptyState(), false), new MigrationStateMatcher());
        verifyZeroInteractions(tbpTask);
    }

    @Test
    public void afterSaveTaskSetsSDPropertyForTBPLicense() {
        when(jira6xServiceDeskLicenseProvider.serviceDeskLicense())
                .thenReturn(some(TestUtils.toLicense(LICENSE_SERVICE_DESK_TBP)));

        final MigrationState result = task.migrate(emptyState(), false);
        result.afterSaveTasks().forEach(Runnable::run);

        verify(serviceDeskPropertySetDao)
                .writeStringProperty("com.atlassian.servicedesk.renaissance.migration.type", "TBP");
    }

    @Test
    public void afterSaveSetsSDPropertyForABPLicense() {
        when(jira6xServiceDeskLicenseProvider.serviceDeskLicense())
                .thenReturn(some(TestUtils.toLicense(LICENSE_SERVICE_DESK_ABP)));

        final MigrationState result = task.migrate(emptyState(), false);
        result.afterSaveTasks().forEach(Runnable::run);

        verify(serviceDeskPropertySetDao)
                .writeStringProperty("com.atlassian.servicedesk.renaissance.migration.type", "ABP");
    }

    @Test
    public void afterSaveSetsSDPropertyIfThereIsNoLicense() {
        when(jira6xServiceDeskLicenseProvider.serviceDeskLicense()).thenReturn(none());

        final MigrationState result = task.migrate(emptyState(), false);
        result.afterSaveTasks().forEach(Runnable::run);

        verify(serviceDeskPropertySetDao)
                .writeStringProperty("com.atlassian.servicedesk.renaissance.migration.type", "none");
    }
}