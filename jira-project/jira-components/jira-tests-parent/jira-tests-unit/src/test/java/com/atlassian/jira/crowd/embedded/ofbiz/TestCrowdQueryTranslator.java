package com.atlassian.jira.crowd.embedded.ofbiz;

import com.atlassian.crowd.embedded.api.SearchRestriction;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.search.query.entity.UserQuery;
import com.atlassian.crowd.search.query.entity.restriction.BooleanRestriction;
import com.atlassian.crowd.search.query.entity.restriction.BooleanRestrictionImpl;
import com.atlassian.crowd.search.query.entity.restriction.MatchMode;
import com.atlassian.crowd.search.query.entity.restriction.NullRestrictionImpl;
import com.atlassian.crowd.search.query.entity.restriction.TermRestriction;
import com.atlassian.crowd.search.query.entity.restriction.constants.UserTermKeys;
import com.atlassian.jira.bc.user.search.UserNameAnalyzer;
import com.atlassian.jira.crowd.embedded.lucene.CrowdQueryTranslator;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.search.Query;
import org.junit.Before;
import org.junit.Test;

import java.util.Collections;
import java.util.Optional;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class TestCrowdQueryTranslator {

    private CrowdQueryTranslator translator;

    @Before
    public void setUp() {
        Analyzer analyzer = new UserNameAnalyzer();
        translator = new CrowdQueryTranslator(analyzer);
    }

    /**
     * Ensure that a Crowd query that matches all users gets translated into a Lucene 'match all' query.
     */
    @Test
    public void translateMatchAllQueryTranslatesIntoEquivalentLuceneQuery() {
        SearchRestriction search = NullRestrictionImpl.INSTANCE;
        EntityQuery<User> query = new UserQuery<>(User.class, search, 0, 10);

        Optional<Query> result = translator.translateQuery(query);

        assertThat(result.get().toString(), is("*:*"));
    }

    /**
     * Ensures a Crowd query that finds all active users translates into a Lucene query that performs the appropriate
     * search.
     */
    @Test
    public void translateActiveUsersQueryTranslatesIntoEquivalentLuceneQuery() {
        SearchRestriction search = new TermRestriction<>(UserTermKeys.ACTIVE, true);
        EntityQuery<User> query = new UserQuery<>(User.class, search, 0, 10);

        Optional<Query> result = translator.translateQuery(query);

        assertThat(result.get().toString(), is("active:true"));
    }

    /**
     * Ensure a Crowd query that finds all inactive users translates into a Lucene query that performs the appropriate
     * search.
     */
    @Test
    public void translateInactiveUsersQueryTranslatesIntoEquivalentLuceneQuery() {
        SearchRestriction search = new TermRestriction<>(UserTermKeys.ACTIVE, false);
        EntityQuery<User> query = new UserQuery<>(User.class, search, 0, 10);

        Optional<Query> result = translator.translateQuery(query);

        assertThat(result.get().toString(), is("active:false"));
    }

    /**
     * Ensure a Crowd query with an AND term translates into the equivalent Lucene query that performs the
     * appropriate search.
     */
    @Test
    public void translateAndQueryTranslatesIntoEquivalentLuceneQuery() {
        SearchRestriction search = new BooleanRestrictionImpl(
                                        BooleanRestriction.BooleanLogic.AND,
                                        new TermRestriction<>(UserTermKeys.ACTIVE, true),
                                        new TermRestriction<>(UserTermKeys.FIRST_NAME, "galah"));
        EntityQuery<User> query = new UserQuery<>(User.class, search, 0, 10);

        Optional<Query> result = translator.translateQuery(query);

        assertThat(result.get().toString(), is("+active:true +firstName:galah"));
    }

    /**
     * Ensure a Crowd query with an OR term translates into the equivalent Lucene query that performs the
     * appropriate search.
     */
    @Test
    public void translateOrQueryTranslatesIntoEquivalentLuceneQuery() {
        SearchRestriction search = new BooleanRestrictionImpl(BooleanRestriction.BooleanLogic.OR,
                new TermRestriction<>(UserTermKeys.ACTIVE, true),
                new TermRestriction<>(UserTermKeys.FIRST_NAME, "galah"));
        EntityQuery<User> query = new UserQuery<>(User.class, search, 0, 10);

        Optional<Query> result = translator.translateQuery(query);

        assertThat(result.get().toString(), is("active:true firstName:galah"));
    }

    /**
     * Ensure a Crowd query to match a field value translates into the equivalent Lucene query that performs
     * appropriate search.
     */
    @Test
    public void translateNameEqualsQueryTranslatesIntoEquivalentLuceneQuery() {
        SearchRestriction search = new TermRestriction<>(UserTermKeys.LAST_NAME, "galah");
        EntityQuery<User> query = new UserQuery<>(User.class, search, 0, 10);

        Optional<Query> result = translator.translateQuery(query);

        assertThat(result.get().toString(), is("lastName:galah"));
    }

    /**
     * Multi token exact matches should match the whole string and not each individual token.
     */
    @Test
    public void translateNameEqualsQueryTranslatesIntoEquivalentLuceneQueryMultiToken() {
        SearchRestriction search = new TermRestriction<>(UserTermKeys.DISPLAY_NAME, MatchMode.EXACTLY_MATCHES, "galah cockatoo");
        EntityQuery<User> query = new UserQuery<>(User.class, search, 0, 10);

        Optional<Query> result = translator.translateQuery(query);

        //Internally that's a one token value
        assertThat(result.get().toString(), is("exact_displayName:galah cockatoo"));
    }

    /**
     * Ensure a Crowd query for a starts-with substring match translates into the appropriate Lucene query.
     */
    @Test
    public void translateNameStartsWithQueryTranslatesIntoEquivalentLuceneQuery() {
        SearchRestriction search = new TermRestriction<>(UserTermKeys.DISPLAY_NAME, MatchMode.STARTS_WITH, "galah");
        EntityQuery<User> query = new UserQuery<>(User.class, search, 0, 10);

        Optional<Query> result = translator.translateQuery(query);

        assertThat(result.get().toString(), is("displayName:galah*"));
    }

    /**
     * Multi token starts-with substring matches should use substring-matching on individual tokens in the
     * Lucene query.
     */
    @Test
    public void translateNameStartsWithQueryTranslatesIntoEquivalentLuceneQueryMultiToken() {
        SearchRestriction search = new TermRestriction<>(UserTermKeys.LAST_NAME, MatchMode.STARTS_WITH, "galah cockatoo");
        EntityQuery<User> query = new UserQuery<>(User.class, search, 0, 10);

        Optional<Query> result = translator.translateQuery(query);

        assertThat(result.get().toString(), is("+lastName:galah* +lastName:cockatoo*"));
    }

    @Test
    public void translateNestedBooleanQueryIntoEquivalentLuceneQueryMultiToken() {
        SearchRestriction search = new BooleanRestrictionImpl(
                BooleanRestriction.BooleanLogic.OR,
                new TermRestriction<>(UserTermKeys.ACTIVE, true),
                new BooleanRestrictionImpl(
                        BooleanRestriction.BooleanLogic.AND,
                        new TermRestriction<>(UserTermKeys.FIRST_NAME, "galah"),
                        new TermRestriction<>(UserTermKeys.LAST_NAME,  MatchMode.STARTS_WITH, "cocka")));
        EntityQuery<User> query = new UserQuery<>(User.class, search, 0, 10);

        Optional<Query> result = translator.translateQuery(query);

        assertThat(result.get().toString(), is("active:true (+firstName:galah +lastName:cocka*)"));
    }

    /**
     * Do not allow performing contains substring matches in Lucene.
     */
    @Test
    public void shouldRefuseSubstringMatches() {
        SearchRestriction search = new TermRestriction<>(UserTermKeys.LAST_NAME, MatchMode.CONTAINS, "galah");
        EntityQuery<User> query = new UserQuery<>(User.class, search, 0, 10);

        Optional<Query> result = translator.translateQuery(query);

        assertThat(result.isPresent(), is(false));
    }
}
