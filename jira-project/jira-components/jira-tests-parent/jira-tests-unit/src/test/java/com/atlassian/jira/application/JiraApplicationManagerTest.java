package com.atlassian.jira.application;

import com.atlassian.application.api.ApplicationAccess;
import com.atlassian.application.api.PlatformApplication;
import com.atlassian.application.host.ApplicationConfigurationManager;
import com.atlassian.application.host.plugin.PluginApplicationMetaDataManager;
import com.atlassian.fugue.Option;
import com.atlassian.jira.mock.security.MockSimpleAuthenticationContext;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.util.MockBuildUtilsInfo;
import com.atlassian.jira.util.NoopI18nHelper;
import com.atlassian.sal.api.license.SingleProductLicenseDetailsView;
import org.hamcrest.Matchers;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.net.URI;
import java.util.Date;

import static com.atlassian.jira.matchers.OptionMatchers.none;
import static com.atlassian.jira.matchers.OptionMatchers.some;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;

public class JiraApplicationManagerTest {
    private static final String DOC_VERSION = "070";

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @Mock
    private PluginApplicationMetaDataManager manager;
    private MockSimpleAuthenticationContext ctx
            = MockSimpleAuthenticationContext.createNoopContext(new MockApplicationUser("one"));
    private MockBuildUtilsInfo info = new MockBuildUtilsInfo().setBuildDate(new Date()).setVersion("20.0").setDocVersion(DOC_VERSION);
    private MockLicenseLocator licenseLocator = new MockLicenseLocator();
    private MockApplicationAccessFactory applicationAccessFactory = new MockApplicationAccessFactory();
    private JiraApplicationManager applicationManager;
    @Mock
    private ApplicationConfigurationManager appConfigManager;

    @Before
    public void setup() {
        applicationManager = new JiraApplicationManager(manager, info, ctx, licenseLocator,
                applicationAccessFactory, appConfigManager);
    }

    @Test
    public void jiraPlatformMakesSense() {
        final PlatformApplication platform = applicationManager.getPlatform();
        assertThat(platform.getName(), equalTo("JIRA Core"));
        assertThat(platform.getConfigurationURI(), none());
        assertThat(platform.getPostInstallURI(), some(URI.create("/secure/admin/user/AddUser!default.jspa?selectedApplications=jira-core")));
        assertThat(platform.getPostUpdateURI(), none());
        assertThat(platform.getUserCountDescription(Option.some(10)),
                equalTo(NoopI18nHelper.makeTranslation("jira.core.user.count", 10)));
        assertThat(platform.getUserCountDescription(Option.none(Integer.class)),
                equalTo(NoopI18nHelper.makeTranslation("jira.core.user.count", -1)));
        assertThat(platform.getKey(), equalTo(ApplicationKeys.CORE));
        assertThat(platform.getDescription(), equalTo(NoopI18nHelper.makeTranslation("jira.core.description")));
        assertThat(platform.getDefaultGroup(), equalTo("jira-core-users"));
        assertThat(platform.buildDate(), equalTo(new DateTime(info.getCurrentBuildDate())));
        assertThat(platform.getVersion(), equalTo(info.getVersion()));
        assertThat(platform.getProductHelpCloudSpaceURI(), equalTo(Option.some(URI.create("JIRACORECLOUD"))));
        assertThat(platform.getProductHelpServerSpaceURI(), equalTo(Option.some(URI.create("jcore-docs-" + DOC_VERSION))));
    }

    @Test
    public void jiraPlatformLicenseReturnedWhenAvailable() {
        final SingleProductLicenseDetailsView license = licenseLocator.add(ApplicationKeys.CORE);
        final PlatformApplication platform = applicationManager.getPlatform();
        assertThat(platform.getLicense(), some(sameInstance(license)));
    }

    @Test
    public void jiraPlatformLicenseNotReturnedWhenNotAvailable() {
        final PlatformApplication platform = applicationManager.getPlatform();
        assertThat(platform.getLicense(), none());
    }

    @Test
    public void jiraPlatformAccessDelegatesToFactory() {
        final ApplicationAccess access = applicationManager.getPlatform().getAccess();
        assertThat(access, Matchers.instanceOf(MockApplicationAccess.class));

        MockApplicationAccess mockApplicationAccess = (MockApplicationAccess) access;
        assertThat(mockApplicationAccess.getApplicationKey(), equalTo(ApplicationKeys.CORE));
        assertThat(mockApplicationAccess.buildDate().toDate(), equalTo(info.getCurrentBuildDate()));
    }
}