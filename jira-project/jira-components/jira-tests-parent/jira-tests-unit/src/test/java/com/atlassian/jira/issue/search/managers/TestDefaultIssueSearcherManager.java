package com.atlassian.jira.issue.search.managers;

import com.atlassian.jira.issue.search.SearchContext;
import com.atlassian.jira.issue.search.searchers.IssueSearcher;
import com.atlassian.jira.issue.search.searchers.SearcherGroup;
import com.atlassian.jira.issue.search.searchers.SearcherGroupType;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.collect.CollectionBuilder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Test for {@link com.atlassian.jira.issue.search.managers.TestDefaultIssueSearcherManager}.
 *
 * @since v4.0
 */
public class TestDefaultIssueSearcherManager {
    @Rule
    public final MethodRule initMockito = MockitoJUnit.rule();

    @Mock
    private SearchContext context;

    @Mock
    private SearchHandlerManager manager;

    @InjectMocks
    private DefaultIssueSearcherManager issueSearcherManager;

    @Test
    public void testConstructor() throws Exception {
        try {
            new DefaultIssueSearcherManager(null);
            fail("Shoudl not be able to pass in a null manager.");
        } catch (IllegalArgumentException expected) {
        }
    }

    @Test
    public void testGetSearchers() throws Exception {
        final List<IssueSearcher<?>> expectedSearchers = Collections.emptyList();

        when(manager.getSearchers((ApplicationUser) null, context)).thenReturn(expectedSearchers);
        when(manager.getSearchers((ApplicationUser) null, null)).thenReturn(null);

        assertEquals(expectedSearchers, issueSearcherManager.getSearchers(null, context));
        assertNull(issueSearcherManager.getSearchers(null, null));
    }

    @Test
    public void testGetSearcherGroups() {
        final SearcherGroup group = new SearcherGroup(SearcherGroupType.DATE, Collections.<IssueSearcher<?>>emptyList());
        final SearcherGroup group2 = new SearcherGroup(SearcherGroupType.ISSUE, Collections.<IssueSearcher<?>>emptyList());

        when(manager.getSearcherGroups()).thenReturn(Collections.singletonList(group)).thenReturn(CollectionBuilder.newBuilder(group, group2).asList());

        assertEquals(Collections.singletonList(group), issueSearcherManager.getSearcherGroups());
        assertEquals(CollectionBuilder.newBuilder(group, group2).asList(), issueSearcherManager.getSearcherGroups());
    }

    @Test
    public void testGetAllSearchers() throws Exception {
        final List<IssueSearcher<?>> expectedSearchers = Collections.emptyList();

        when(manager.getAllSearchers()).thenReturn(expectedSearchers);
        when(manager.getSearchers((ApplicationUser) null, null)).thenReturn(null);

        assertEquals(expectedSearchers, issueSearcherManager.getAllSearchers());
        assertNull(issueSearcherManager.getSearchers(null, null));
    }

    @Test
    public void testGetSearcher() {
        final String searcherId = "myid";
        final IssueSearcher<?> expectedSearcher = mock(IssueSearcher.class);

        when(manager.getSearcher(searcherId)).thenReturn((IssueSearcher) expectedSearcher);
        when(manager.getSearcher(null)).thenReturn(null);

        assertEquals(expectedSearcher, issueSearcherManager.getSearcher(searcherId));
        assertNull(issueSearcherManager.getSearcher(null));
    }

    @Test
    public void testRefresh() throws Exception {
        issueSearcherManager.refresh();

        verify(manager).refresh();
    }
}
