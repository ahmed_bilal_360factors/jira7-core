package com.atlassian.jira.config.feature;

import com.atlassian.jira.config.FeatureFlag;
import com.atlassian.jira.config.FeatureFlagDefinitions;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.junit.Test;

import java.util.Set;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

@SuppressWarnings("UnusedDeclaration")
public class FeatureFlagParserTest {
    FeatureFlagDefinitions FLAGS = new FeatureFlagDefinitions();

    FeatureFlag emptyAndOff = FLAGS.featureFlag("emptyAndOff").offByDefault().define();
    FeatureFlag specifiedFalse = FLAGS.featureFlag("specifiedFalse").offByDefault().define();
    FeatureFlag specifiedOff = FLAGS.featureFlag("specifiedOff").offByDefault().define();
    FeatureFlag specifiedTrue = FLAGS.featureFlag("specifiedTrue").onByDefault().define();
    FeatureFlag specifiedOn = FLAGS.featureFlag("specifiedOn").onByDefault().define();
    FeatureFlag badDefaulting = FLAGS.featureFlag("badDefaulting").offByDefault().define();


    @Test
    public void testParseFlags() throws Exception {
        String xml = ""
                + "<featureflags>"
                + ""
                + "    <flag key='emptyAndOff' />"
                + "    <flag key='specifiedFalse' default='false' />"
                + "    <flag key='specifiedOff' default='off' />"
                + "    <flag key='specifiedTrue' default='true' />"
                + "    <flag key='specifiedOn' default='true' />"
                + "    <flag key='badDefaulting' defaultWrong='true' />"
                + ""
                + "    <flag/>"
                + "    <flag default='true' />"
                + "    <flag keyx='ignored' default='on' />"
                + "    <ignored key='ignored' default='on' />"
                + ""
                + "</featureflags>";
        Document document = DocumentHelper.parseText(xml);

        Set<FeatureFlag> featureFlags = FeatureFlagParser.parseFlags(document.getRootElement());
        assertThat(featureFlags, equalTo(FLAGS.getFeatureFlags()));
    }

    @Test
    public void testParseNoFlags() throws Exception {
        String xml = ""
                + "<featureflags>"
                + "</featureflags>";
        Document document = DocumentHelper.parseText(xml);

        Set<FeatureFlag> featureFlags = FeatureFlagParser.parseFlags(document.getRootElement());
        assertTrue(featureFlags.isEmpty());
    }
}