package com.atlassian.jira.application.install;

import com.atlassian.jira.imports.project.LogVerifier;
import com.google.common.collect.ImmutableList;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

import static com.atlassian.jira.imports.project.LogVerifier.exception;
import static com.atlassian.jira.imports.project.LogVerifier.level;
import static com.atlassian.jira.imports.project.LogVerifier.renderedMessage;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

public class WhatWasInstalledInApplicationTest {
    @Rule
    public TemporaryFolder temporaryFolder = new TemporaryFolder();
    private static final List<BundlesVersionDiscovery.PluginIdentification> PLUGIN_IDENTIFICATIONS = ImmutableList.of(
            new BundlesVersionDiscovery.PluginIdentification("plugin-a", "1.0"),
            new BundlesVersionDiscovery.PluginIdentification("plugin-b", "0.1")
    );
    private static final List<BundlesVersionDiscovery.PluginIdentification> PLUGIN_IDENTIFICATIONS2 = ImmutableList.of(
            new BundlesVersionDiscovery.PluginIdentification("plugin-a", "2.0"),
            new BundlesVersionDiscovery.PluginIdentification("plugin-b", "0.1.2")
    );

    WhatWasInstalledInApplicationFactory whatWasInstalledInApplicationFactory = new WhatWasInstalledInApplicationFactory();

    @Test
    public void applicationInstalledInfoShouldReturnNotInstalledWhenLoadedFromNonExistingFile() throws IOException {
        // given
        final WhatWasInstalledInApplication whatWasInstalled = whatWasInstalledInApplicationFactory.load(new File(temporaryFolder.getRoot(), "im-not-here.properties"));

        // when
        final boolean wereBundlesInstalled = whatWasInstalled.wereBundlesInstalled(PLUGIN_IDENTIFICATIONS);

        // then
        assertThat(wereBundlesInstalled, is(false));
    }

    @Test
    public void infoObjectCreatedFromGivenIdentificationShouldReturnItIsInstalled() {
        // given
        final WhatWasInstalledInApplication whatWasInstalled = whatWasInstalledInApplicationFactory.load(PLUGIN_IDENTIFICATIONS);

        // when
        final boolean wereBundlesInstalled = whatWasInstalled.wereBundlesInstalled(PLUGIN_IDENTIFICATIONS);

        // then
        assertThat(wereBundlesInstalled, is(true));
    }

    @Test
    public void whenNewPluginIsAddedShouldInformThatBundlesWereNotInstalled() {
        // given
        final WhatWasInstalledInApplication whatWasInstalled = whatWasInstalledInApplicationFactory.load(PLUGIN_IDENTIFICATIONS);

        // when
        final ImmutableList<BundlesVersionDiscovery.PluginIdentification> withNewPlugin = ImmutableList.<BundlesVersionDiscovery.PluginIdentification>builder()
                .addAll(PLUGIN_IDENTIFICATIONS)
                .add(new BundlesVersionDiscovery.PluginIdentification("plugin-c", "1.0"))
                .build();
        final boolean wereBundlesInstalled = whatWasInstalled.wereBundlesInstalled(withNewPlugin);

        // then
        assertThat(wereBundlesInstalled, is(false));
    }

    @Test
    public void newVersionsOfPluginShouldInformThatBundlesWereNotInstalled() {
        // given
        final WhatWasInstalledInApplication whatWasInstalled = whatWasInstalledInApplicationFactory.load(PLUGIN_IDENTIFICATIONS);

        // when
        final boolean wereBundlesInstalled = whatWasInstalled.wereBundlesInstalled(PLUGIN_IDENTIFICATIONS2);

        // then
        assertThat(wereBundlesInstalled, is(false));
    }

    @Test
    public void whenPluginIsOnlyRemovedShouldInformThatBundlesIsStillInstalled() {
        // given
        final WhatWasInstalledInApplication whatWasInstalled = whatWasInstalledInApplicationFactory.load(PLUGIN_IDENTIFICATIONS);

        // when
        final ImmutableList<BundlesVersionDiscovery.PluginIdentification> subsetOfPlugins =
                ImmutableList.of(PLUGIN_IDENTIFICATIONS.get(0));
        final boolean wereBundlesInstalled = whatWasInstalled.wereBundlesInstalled(subsetOfPlugins);

        // then
        assertThat(wereBundlesInstalled, is(true));
    }

    @Test
    public void storedAndLoadedInfoShouldKeepInformation() throws IOException {
        // given
        final WhatWasInstalledInApplication originalInstance = whatWasInstalledInApplicationFactory.load(PLUGIN_IDENTIFICATIONS);
        final File configFile = new File(new File(temporaryFolder.getRoot(), "subfolder"), "config.properties");
        final ReversibleFileOperations reversibleFileOperations = new ReversibleFileOperations();
        whatWasInstalledInApplicationFactory.store(configFile, originalInstance, reversibleFileOperations);
        reversibleFileOperations.commit();
        final WhatWasInstalledInApplication loadedInstance = whatWasInstalledInApplicationFactory.load(configFile);

        // when
        final boolean wereBundlesInstalled = loadedInstance.wereBundlesInstalled(PLUGIN_IDENTIFICATIONS);

        // then
        assertThat(wereBundlesInstalled, is(true));
    }

    @Test
    public void newVersionOfPluginShoulOverwriteOldInfo() throws IOException {
        // given
        final WhatWasInstalledInApplication originalInstance = whatWasInstalledInApplicationFactory.load(PLUGIN_IDENTIFICATIONS);
        final File configFile = temporaryFolder.newFile();
        final ReversibleFileOperations reversibleFileOperations = new ReversibleFileOperations();
        whatWasInstalledInApplicationFactory.store(configFile, originalInstance, reversibleFileOperations);
        reversibleFileOperations.commit();
        final WhatWasInstalledInApplication v2Instance = whatWasInstalledInApplicationFactory.load(PLUGIN_IDENTIFICATIONS2);

        // when
        whatWasInstalledInApplicationFactory.store(configFile, v2Instance, reversibleFileOperations);
        reversibleFileOperations.commit();
        final WhatWasInstalledInApplication loadedInstance = whatWasInstalledInApplicationFactory.load(configFile);

        // then
        final boolean wereV1BundlesInstalled = loadedInstance.wereBundlesInstalled(PLUGIN_IDENTIFICATIONS);
        final boolean wereV2BundlesInstalled = loadedInstance.wereBundlesInstalled(PLUGIN_IDENTIFICATIONS2);
        assertThat(wereV1BundlesInstalled, is(false));
        assertThat(wereV2BundlesInstalled, is(true));
    }

    @Test
    public void revertingStoreOperationShouldKeepOriginalInfo() throws IOException {
        // given
        final WhatWasInstalledInApplication originalInstance = whatWasInstalledInApplicationFactory.load(PLUGIN_IDENTIFICATIONS);
        final File configFile = temporaryFolder.newFile();
        final ReversibleFileOperations reversibleFileOperations = new ReversibleFileOperations();
        whatWasInstalledInApplicationFactory.store(configFile, originalInstance, reversibleFileOperations);
        reversibleFileOperations.commit();
        final WhatWasInstalledInApplication v2Instance = whatWasInstalledInApplicationFactory.load(PLUGIN_IDENTIFICATIONS2);

        // when
        whatWasInstalledInApplicationFactory.store(configFile, v2Instance, reversibleFileOperations);
        reversibleFileOperations.close(); // last operation not commited
        final WhatWasInstalledInApplication loadedInstance = whatWasInstalledInApplicationFactory.load(configFile);

        // then
        final boolean wereV1BundlesInstalled = loadedInstance.wereBundlesInstalled(PLUGIN_IDENTIFICATIONS);
        final boolean wereV2BundlesInstalled = loadedInstance.wereBundlesInstalled(PLUGIN_IDENTIFICATIONS2);
        assertThat(wereV1BundlesInstalled, is(true));
        assertThat(wereV2BundlesInstalled, is(false));

    }

    @Test
    public void invalidFilesShouldActAsIfApplicationIsNotInstalled() throws Exception {
        // given
        final File configFile = temporaryFolder.newFile();
        Files.write(configFile.toPath(), new byte[]{'=', 1, 2, 3, 4, 5, 6});

        try (LogVerifier logVerifier = LogVerifier.catchLogMessages(Logger.getLogger(WhatWasInstalledInApplicationFactory.class))) {
            // when
            whatWasInstalledInApplicationFactory.load(configFile);

            // then
            final String expectedErrorMesage = String.format(WhatWasInstalledInApplicationFactory.CANNOT_READ_PROPS_MESSAGE, configFile.getAbsolutePath());
            logVerifier.verifyEventLogged(allOf(
                    level(equalTo(Level.ERROR)),
                    exception(notNullValue()),
                    renderedMessage(equalTo(expectedErrorMesage))
            ));
        }
    }
}