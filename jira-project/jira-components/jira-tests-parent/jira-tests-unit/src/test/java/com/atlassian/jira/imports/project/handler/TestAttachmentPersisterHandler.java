package com.atlassian.jira.imports.project.handler;

import com.atlassian.jira.exception.ParseException;
import com.atlassian.jira.external.beans.ExternalAttachment;
import com.atlassian.jira.external.beans.ExternalProject;
import com.atlassian.jira.imports.project.ProjectImportPersister;
import com.atlassian.jira.imports.project.core.BackupProject;
import com.atlassian.jira.imports.project.core.BackupProjectImpl;
import com.atlassian.jira.imports.project.core.BackupSystemInformation;
import com.atlassian.jira.imports.project.core.ProjectImportOptions;
import com.atlassian.jira.imports.project.core.ProjectImportResults;
import com.atlassian.jira.imports.project.core.ProjectImportResultsImpl;
import com.atlassian.jira.imports.project.mapper.ProjectImportMapper;
import com.atlassian.jira.imports.project.parser.AttachmentParser;
import com.atlassian.jira.imports.project.transformer.AttachmentTransformer;
import com.atlassian.jira.issue.attachment.Attachment;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.web.bean.MockI18nBean;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.mockito.Mock;

import java.io.File;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.collection.IsEmptyCollection.empty;
import static org.hamcrest.collection.IsIterableContainingInOrder.contains;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @since v3.13
 */
public class TestAttachmentPersisterHandler {

    private static final String ISSUE_ID = "12";
    private static final String ISSUE_KEY = "TST-1";

    private ExternalAttachment externalAttachment;
    private ProjectImportMapper projectImportMapper;

    private ExternalProject oldProject;
    private BackupProject backupProject;
    private ProjectImportResults projectImportResults;
    private ProjectImportOptions projectImportOptions;

    @Mock
    private ProjectImportPersister projectImportPersister;

    @Mock
    private BackupSystemInformation backupSystemInformation;

    @Mock
    private AttachmentTransformer attachmentTransformer;
    @Mock
    private AttachmentParser attachmentParser;
    @Rule
    public final RuleChain chain = MockitoMocksInContainer.forTest(this);

    @Before
    public void setUp() throws Exception {
        externalAttachment = new ExternalAttachment();
        externalAttachment.setIssueId(ISSUE_ID);

        oldProject = new ExternalProject();
        oldProject.setKey("TST");

        backupProject = new BackupProjectImpl(oldProject, ImmutableList.of(), ImmutableList.of(), ImmutableList.of(), ImmutableList.of(), 0, ImmutableMap.of());

        when(backupSystemInformation.getIssueKeyForId(ISSUE_ID)).thenReturn(ISSUE_KEY);

        projectImportResults = new ProjectImportResultsImpl(0, 0, 0, 0, new MockI18nBean());

        projectImportOptions = mock(ProjectImportOptions.class);
        when(projectImportOptions.getPathToBackup()).thenReturn("/");
        when(projectImportOptions.getAttachmentPath()).thenReturn("/");
    }

    @Test
    public void shouldCreateAttachmentAndNotReturnErrorsWhenAttachmentImported() throws Exception {
        final Attachment mockAttachment = mock(Attachment.class);
        when(projectImportPersister.createAttachment(externalAttachment)).thenReturn(mockAttachment);
        when(attachmentTransformer.transform(projectImportMapper, externalAttachment)).thenReturn(externalAttachment);
        mockAttachmentParser(true);

        AttachmentPersisterHandler attachmentPersisterHandler = createAttachmentPersisterHandler();
        attachmentPersisterHandler.startDocument();
        attachmentPersisterHandler.handleEntity(AttachmentParser.ATTACHMENT_ENTITY_NAME, null);
        attachmentPersisterHandler.handleEntity("SOME_ENTITY", null);

        assertEquals(1, projectImportResults.getAttachmentsCreatedCount());
        assertThat(projectImportResults.getErrors(), is(empty()));
    }

    @Test
    public void shouldNotCreateAttachmentWhenAttachmentNotImported() throws Exception {
        externalAttachment.setFileName("my.file");
        when(projectImportPersister.createAttachment(externalAttachment)).thenReturn(null);
        when(attachmentTransformer.transform(projectImportMapper, externalAttachment)).thenReturn(externalAttachment);
        mockAttachmentParser(true);

        AttachmentPersisterHandler attachmentPersisterHandler = createAttachmentPersisterHandler();
        attachmentPersisterHandler.handleEntity(AttachmentParser.ATTACHMENT_ENTITY_NAME, null);
        attachmentPersisterHandler.handleEntity("SOME_ENTITY", null);

        assertEquals(0, projectImportResults.getAttachmentsCreatedCount());
        assertThat(projectImportResults.getErrors(), contains("There was a problem saving attachment 'my.file' for issue 'TST-1'."));
    }

    @Test
    public void shouldNotReturnErrorsWhenIssueIdNull() throws Exception {
        externalAttachment.setIssueId(null);
        when(attachmentParser.parse(null)).thenReturn(externalAttachment);

        AttachmentPersisterHandler attachmentPersisterHandler = createAttachmentPersisterHandler();
        attachmentPersisterHandler.handleEntity(AttachmentParser.ATTACHMENT_ENTITY_NAME, null);
        attachmentPersisterHandler.handleEntity("SOME_ENTITY", null);

        assertEquals(0, projectImportResults.getAttachmentsCreatedCount());
        assertThat(projectImportResults.getErrors(), is(empty()));
    }

    @Test
    public void shouldNotReturnErrorsWhenNoAttachmentPathSpecified() throws Exception {
        when(projectImportOptions.getAttachmentPath()).thenReturn(null);

        AttachmentPersisterHandler attachmentPersisterHandler = createAttachmentPersisterHandler();
        attachmentPersisterHandler.handleEntity(AttachmentParser.ATTACHMENT_ENTITY_NAME, null);

        assertThat(projectImportResults.getErrors(), is(empty()));
    }

    @Test
    public void shouldNotThrowExceptionWhenWrongEntityType() throws Exception {
        AttachmentPersisterHandler attachmentPersisterHandler = createAttachmentPersisterHandler();
        attachmentPersisterHandler.handleEntity("SOME_ENTITY", null);

        assertEquals(0, projectImportResults.getAttachmentsCreatedCount());
        assertThat(projectImportResults.getErrors(), is(empty()));
    }

    @Test
    public void shouldNotCreateAttachmentWhenFileNotExists() throws Exception {
        mockAttachmentParser(false);

        AttachmentPersisterHandler attachmentPersisterHandler = createAttachmentPersisterHandler();
        attachmentPersisterHandler.handleEntity(AttachmentParser.ATTACHMENT_ENTITY_NAME, null);

        assertEquals(0, projectImportResults.getAttachmentsCreatedCount());
        assertThat(projectImportResults.getErrors(), is(empty()));
    }

    private AttachmentPersisterHandler createAttachmentPersisterHandler() {
        return new AttachmentPersisterHandler(projectImportPersister, projectImportOptions, projectImportMapper,
                backupProject, backupSystemInformation, projectImportResults, new ExecutorForTests(), attachmentParser,
                attachmentTransformer);
    }

    private void mockAttachmentParser(final boolean exists) throws ParseException {
        when(attachmentParser.isUsingOriginalKeyPath(oldProject)).thenReturn(false);
        when(attachmentParser.parse(null)).thenReturn(externalAttachment);
        when(attachmentParser.getAttachmentFile(externalAttachment, oldProject, ISSUE_KEY)).thenReturn(mockFile("/some/file.txt", exists));
    }

    private File mockFile(final String pathname, final boolean exists) {
        return new File(pathname) {
            @Override
            public boolean exists() {
                return exists;
            }
        };
    }

}
