package com.atlassian.jira.issue.fields;

import com.atlassian.jira.action.issue.customfields.MockCustomFieldType;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.RendererManager;
import com.atlassian.jira.issue.context.IssueContext;
import com.atlassian.jira.issue.context.IssueContextImpl;
import com.atlassian.jira.issue.customfields.CustomFieldSearcher;
import com.atlassian.jira.issue.customfields.CustomFieldType;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.issue.fields.config.FieldConfigImpl;
import com.atlassian.jira.issue.fields.config.FieldConfigScheme;
import com.atlassian.jira.issue.fields.config.manager.FieldConfigSchemeManager;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.jql.context.FieldConfigSchemeClauseContextUtil;
import com.atlassian.jira.mock.component.MockComponentWorker;
import com.atlassian.jira.mock.ofbiz.MockOfBizDelegator;
import com.atlassian.jira.model.querydsl.CustomFieldDTO;
import com.atlassian.jira.plugin.customfield.CustomFieldSearcherModuleDescriptors;
import com.atlassian.jira.plugin.customfield.CustomFieldTypeModuleDescriptors;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.web.action.admin.translation.TranslationManager;
import com.atlassian.utt.mock.FlipFlop;
import com.atlassian.utt.mock.MoreAnswers;
import com.google.common.collect.ImmutableList;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.ofbiz.core.entity.GenericValue;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static com.atlassian.fugue.Option.none;
import static com.atlassian.fugue.Option.some;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

public class TestImmutableCustomField {
    @Rule
    public MockitoRule mockito = MockitoJUnit.rule();

    @Mock
    private CustomFieldDescription customFieldDescription;

    private MockOfBizDelegator ofBizDelegator = new MockOfBizDelegator();

    @Before
    public void setUp() {
        new MockComponentWorker().init();
    }

    @Test
    public void testFieldConfigComparison() {
        final FieldConfig fieldConfigX = new FieldConfigImpl(1L, null, null, null, null);
        final FieldConfig fieldConfigY = new FieldConfigImpl(2L, null, null, null, null);

        assertEquals(ImmutableCustomField.areDifferent(null, null), false);
        assertEquals(ImmutableCustomField.areDifferent(fieldConfigX, null), true);
        assertEquals(ImmutableCustomField.areDifferent(null, fieldConfigX), true);
        assertEquals(ImmutableCustomField.areDifferent(fieldConfigX, fieldConfigX), false);
        assertEquals(ImmutableCustomField.areDifferent(fieldConfigX, fieldConfigY), true);
        assertEquals(ImmutableCustomField.areDifferent(fieldConfigY, fieldConfigX), true);
    }

    @Test
    public void testExceptionDuringSearcherInit() throws Exception {
        final String customFieldKey = "mockkey";

        final CustomFieldDTO customFieldDTO = CustomFieldDTO.builder()
                .id(1L)
                .customfieldsearcherkey(customFieldKey)
                .build();

        final CustomFieldSearcher searcher = mock(CustomFieldSearcher.class);
        doThrow(new RuntimeException()).when(searcher).init(any(CustomField.class));

        final CustomFieldManager customFieldManager = mock(CustomFieldManager.class);
        when(customFieldManager.getCustomFieldSearcher(customFieldKey)).thenReturn(searcher);

        final FieldConfigSchemeClauseContextUtil contextUtil = null;
        final JiraAuthenticationContext authenticationContext = null;
        final FieldConfigSchemeManager fieldConfigSchemeManager = null;
        final PermissionManager permissionManager = null;
        final RendererManager rendererManager = null;
        final FeatureManager featureManager = null;
        final TranslationManager translationManager = null;
        final CustomFieldScopeFactory scopeFactory = null;
        final CustomFieldTypeModuleDescriptors customFieldTypeModuleDescriptors = null;
        final CustomFieldSearcherModuleDescriptors customFieldSearcherModuleDescriptors = mock(CustomFieldSearcherModuleDescriptors.class);
        when(customFieldSearcherModuleDescriptors.getCustomFieldSearcher(anyString())).thenReturn(some(searcher));

        final ImmutableCustomField customField = new ImmutableCustomField(
                customFieldDTO,
                authenticationContext,
                fieldConfigSchemeManager,
                permissionManager,
                rendererManager,
                contextUtil,
                customFieldDescription,
                featureManager,
                translationManager,
                scopeFactory,
                customFieldTypeModuleDescriptors,
                customFieldSearcherModuleDescriptors,
                ofBizDelegator
        );

        // we should swallow the exception from the searcher and return a null searcher instead.
        assertNull(customField.getCustomFieldSearcher());
    }

    @Test
    public void testExceptionDuringSearcherInitIsNotCached() throws Exception {
        final String customFieldKey = "mockkey";

        final CustomFieldDTO customFieldDTO = CustomFieldDTO.builder()
                .id(1L)
                .customfieldsearcherkey(customFieldKey)
                .build();

        final CustomFieldSearcher searcher = mock(CustomFieldSearcher.class);

        FlipFlop<Void> flipFlop = MoreAnswers.flipFlop(invocation -> {
            throw new RuntimeException();
        }, invocation -> searcher);

        doAnswer(flipFlop).when(searcher).init(any(CustomField.class));

        final CustomFieldSearcherModuleDescriptors customFieldSearcherModuleDescriptors = mock(CustomFieldSearcherModuleDescriptors.class);
        when(customFieldSearcherModuleDescriptors.getCustomFieldSearcher(anyString())).thenReturn(some(searcher));

        final ImmutableCustomField customField = new ImmutableCustomField(
                customFieldDTO,
                mock(JiraAuthenticationContext.class),
                mock(FieldConfigSchemeManager.class),
                mock(PermissionManager.class),
                mock(RendererManager.class),
                mock(FieldConfigSchemeClauseContextUtil.class),
                mock(CustomFieldDescription.class),
                mock(FeatureManager.class),
                mock(TranslationManager.class),
                mock(CustomFieldScopeFactory.class),
                mock(CustomFieldTypeModuleDescriptors.class),
                customFieldSearcherModuleDescriptors,
                ofBizDelegator
        );

        if (customField.getCustomFieldSearcher() == null) {
            //expect to happen
            flipFlop.flip();
        }

        assertThat(customField.getCustomFieldSearcher(), is(searcher));
        verify(customFieldSearcherModuleDescriptors, times(2)).getCustomFieldSearcher(anyString());

        // when there is a value, it is returned from cache
        assertThat(customField.getCustomFieldSearcher(), is(searcher));
        verify(customFieldSearcherModuleDescriptors, times(2)).getCustomFieldSearcher(anyString());
    }

    @Test
    public void testExceptionGettingCustomFieldSearcherIsNotCached() throws Exception {
        final String customFieldKey = "mockkey";

        final CustomFieldDTO customFieldDTO = CustomFieldDTO.builder()
                .id(1L)
                .customfieldsearcherkey(customFieldKey)
                .build();

        final CustomFieldSearcher customFieldSearcher = mock(CustomFieldSearcher.class);

        final CustomFieldSearcherModuleDescriptors customFieldSearcherModuleDescriptors = mock(CustomFieldSearcherModuleDescriptors.class);

        FlipFlop<Void> flipFlop = MoreAnswers.flipFlop(invocation -> none(), invocation -> some(customFieldSearcher));
        doAnswer(flipFlop).when(customFieldSearcherModuleDescriptors).getCustomFieldSearcher(anyString());

        final ImmutableCustomField customField = new ImmutableCustomField(
                customFieldDTO,
                mock(JiraAuthenticationContext.class),
                mock(FieldConfigSchemeManager.class),
                mock(PermissionManager.class),
                mock(RendererManager.class),
                mock(FieldConfigSchemeClauseContextUtil.class),
                mock(CustomFieldDescription.class),
                mock(FeatureManager.class),
                mock(TranslationManager.class),
                mock(CustomFieldScopeFactory.class),
                mock(CustomFieldTypeModuleDescriptors.class),
                customFieldSearcherModuleDescriptors,
                ofBizDelegator
        );

        if (customField.getCustomFieldSearcher() == null) {
            //expect to happen
            flipFlop.flip();
        }

        assertThat(customField.getCustomFieldSearcher(), is(customFieldSearcher));
        verify(customFieldSearcherModuleDescriptors, times(2)).getCustomFieldSearcher(anyString());

        // when there is a value, it is returned from cache
        assertThat(customField.getCustomFieldSearcher(), is(customFieldSearcher));
        verify(customFieldSearcherModuleDescriptors, times(2)).getCustomFieldSearcher(anyString());
    }

    @Test
    public void testBlankCustomFieldSearcherKeyIsCheckedOnceAndCached() throws Exception {
        final String customFieldKey = "";

        final CustomFieldDTO customFieldDTO = CustomFieldDTO.builder()
                .id(1L)
                .customfieldsearcherkey(customFieldKey)
                .build();

        final CustomFieldSearcherModuleDescriptors customFieldSearcherModuleDescriptors = mock(CustomFieldSearcherModuleDescriptors.class);

        final ImmutableCustomField customField = new ImmutableCustomField(
                customFieldDTO,
                mock(JiraAuthenticationContext.class),
                mock(FieldConfigSchemeManager.class),
                mock(PermissionManager.class),
                mock(RendererManager.class),
                mock(FieldConfigSchemeClauseContextUtil.class),
                mock(CustomFieldDescription.class),
                mock(FeatureManager.class),
                mock(TranslationManager.class),
                mock(CustomFieldScopeFactory.class),
                mock(CustomFieldTypeModuleDescriptors.class),
                customFieldSearcherModuleDescriptors,
                ofBizDelegator
        );

        assertThat(customField.getCustomFieldSearcher(), nullValue());
        assertThat(customField.getCustomFieldSearcher(), nullValue());

        verifyZeroInteractions(customFieldSearcherModuleDescriptors);
    }

    @Test
    public void testExceptionGettingCustomFieldTypeIsNotCached() throws Exception {
        final String customFieldKey = "mockkey";

        final CustomFieldDTO customFieldDTO = CustomFieldDTO.builder()
                .id(1L)
                .customfieldtypekey(customFieldKey)
                .build();

        final CustomFieldType customFieldType = new MockCustomFieldType();

        final CustomFieldTypeModuleDescriptors customFieldTypeModuleDescriptors = mock(CustomFieldTypeModuleDescriptors.class);

        FlipFlop<Void> flipFlop = MoreAnswers.flipFlop(invocation -> none(), invocation -> some(customFieldType));
        doAnswer(flipFlop).when(customFieldTypeModuleDescriptors).getCustomFieldType(anyString());

        final ImmutableCustomField customField = new ImmutableCustomField(
                customFieldDTO,
                mock(JiraAuthenticationContext.class),
                mock(FieldConfigSchemeManager.class),
                mock(PermissionManager.class),
                mock(RendererManager.class),
                mock(FieldConfigSchemeClauseContextUtil.class),
                mock(CustomFieldDescription.class),
                mock(FeatureManager.class),
                mock(TranslationManager.class),
                mock(CustomFieldScopeFactory.class),
                customFieldTypeModuleDescriptors,
                mock(CustomFieldSearcherModuleDescriptors.class),
                ofBizDelegator
        );

        if (customField.getCustomFieldType() == null) {
            //expect to happen
            flipFlop.flip();
        }

        assertThat(customField.getCustomFieldType(), is(customFieldType));
        verify(customFieldTypeModuleDescriptors, times(2)).getCustomFieldType(anyString());

        // when there is a value, it is returned from cache
        assertThat(customField.getCustomFieldType(), is(customFieldType));
        verify(customFieldTypeModuleDescriptors, times(2)).getCustomFieldType(anyString());
    }

    @Test
    public void testBlankCustomFieldTypeKeyIsCheckedOnceAndCached() throws Exception {
        final String customFieldKey = "";

        final CustomFieldDTO customFieldDTO = CustomFieldDTO.builder()
                .id(1L)
                .customfieldtypekey(customFieldKey)
                .build();

        final CustomFieldTypeModuleDescriptors customFieldTypeModuleDescriptors = mock(CustomFieldTypeModuleDescriptors.class);

        final ImmutableCustomField customField = new ImmutableCustomField(
                customFieldDTO,
                mock(JiraAuthenticationContext.class),
                mock(FieldConfigSchemeManager.class),
                mock(PermissionManager.class),
                mock(RendererManager.class),
                mock(FieldConfigSchemeClauseContextUtil.class),
                mock(CustomFieldDescription.class),
                mock(FeatureManager.class),
                mock(TranslationManager.class),
                mock(CustomFieldScopeFactory.class),
                customFieldTypeModuleDescriptors,
                mock(CustomFieldSearcherModuleDescriptors.class),
                ofBizDelegator
        );

        assertThat(customField.getCustomFieldType(), nullValue());
        assertThat(customField.getCustomFieldType(), nullValue());

        verifyZeroInteractions(customFieldTypeModuleDescriptors);
    }

    @Test
    public void testIssueComparatorEquals() {
        final CustomField mockCustomField = mock(CustomField.class);

        final ImmutableCustomField.CustomFieldIssueSortComparator comparator = new ImmutableCustomField.CustomFieldIssueSortComparator(mockCustomField);
        assertEquals(comparator, comparator);

        final String cfId = "customfield_2000";
        when(mockCustomField.getId()).thenReturn(cfId);

        assertEquals(comparator.hashCode(), comparator.hashCode());
        assertEquals(cfId.hashCode(), comparator.hashCode());
        verify(mockCustomField, atLeastOnce()).getId();

        final CustomField mockCustomField2 = mock(CustomField.class);
        when(mockCustomField2.getId()).thenReturn(cfId);

        final ImmutableCustomField.CustomFieldIssueSortComparator comparator2 = new ImmutableCustomField.CustomFieldIssueSortComparator(mockCustomField2);
        assertEquals(comparator, comparator2);
        verify(mockCustomField2).getId();
        assertEquals(comparator.hashCode(), comparator2.hashCode());

        final CustomField mockCustomField3 = mock(CustomField.class);
        final String anotherCfId = "customfield_1001";
        when(mockCustomField3.getId()).thenReturn(anotherCfId);

        final ImmutableCustomField.CustomFieldIssueSortComparator comparator3 = new ImmutableCustomField.CustomFieldIssueSortComparator(mockCustomField3);
        assertFalse(comparator.equals(comparator3));
        assertFalse(comparator2.equals(comparator3));
        verify(mockCustomField3, atLeastOnce()).getId();

        assertFalse(comparator.hashCode() == comparator3.hashCode());
        assertFalse(comparator2.hashCode() == comparator3.hashCode());

        assertEquals(anotherCfId.hashCode(), comparator3.hashCode());
    }

    @Test
    public void isInScopeForSearchGracefullyHandlesANullProjectPassedIn() {
        final CustomFieldScope scope = mock(CustomFieldScope.class);

        final ImmutableCustomField customField = customFieldWithScope(scope);
        customField.isInScopeForSearch(null, anyIssueTypeIds());

        verify(scope, atLeastOnce()).isIncludedIn(any(IssueContext.class));
    }

    @Test
    public void isInScopeForSearchGracefullyHandlesANullListOfIssueTypesPassedIn() {
        final CustomFieldScope scope = mock(CustomFieldScope.class);

        final ImmutableCustomField customField = customFieldWithScope(scope);
        customField.isInScopeForSearch(anyProject(), null);

        verify(scope, atLeastOnce()).isIncludedIn(any(IssueContext.class));
    }

    @Test
    public void isInScopeForSearchReturnsFalseIfItIsNotInScopeOfAtLeastOneIssueContext() {
        final CustomFieldScope scope = mock(CustomFieldScope.class);
        when(scope.isIncludedIn(any(IssueContext.class))).thenReturn(false);

        final ImmutableCustomField customField = customFieldWithScope(scope);
        final boolean inScope = customField.isInScopeForSearch(anyProject(), anyIssueTypeIds());

        assertThat(inScope, is(false));
    }

    @Test
    public void isInScopeForSearchReturnsTrueIfItIsInScopeOfAtLeastOneIssueContext() {
        final CustomFieldScope scope = mock(CustomFieldScope.class);
        final ImmutableCustomField customField = customFieldWithScope(scope);

        final Long projectId = 1L;
        final List<String> issueTypeIds = Arrays.asList("bug", "task");
        final List<IssueContext> issueContexts = asList(
                new IssueContextImpl(projectId, "bug"),
                new IssueContextImpl(projectId, "task")
        );

        when(scope.isIncludedIn(issueContexts.get(0))).thenReturn(false);
        when(scope.isIncludedIn(issueContexts.get(1))).thenReturn(true);

        final boolean inScope = customField.isInScopeForSearch(projectWithId(projectId), issueTypeIds);

        assertThat(inScope, is(true));
    }

    @Test
    public void getDefaultValueReturnsNullWhenTheRelevantConfigForTheGivenIssueIsNull() {
        final FieldConfigSchemeManager fieldConfigSchemeManager = mock(FieldConfigSchemeManager.class);
        final ImmutableCustomField customField = customFieldWith(fieldConfigSchemeManager);

        final Issue issue = mock(Issue.class);
        when(fieldConfigSchemeManager.getRelevantConfig(issue, customField)).thenReturn(null);

        final Object defaultValue = customField.getDefaultValue(issue);

        assertThat(defaultValue, is(nullValue()));
    }

    @Test
    public void getJsonDefaultValueReturnsNullWhenTheRelevantConfigForTheGivenIssueContextIsNull() {
        final FieldConfigSchemeManager fieldConfigSchemeManager = mock(FieldConfigSchemeManager.class);
        final CustomFieldTypeModuleDescriptors customFieldTypeModuleDescriptors = mock(CustomFieldTypeModuleDescriptors.class);
        final ImmutableCustomField customField = customFieldWith(fieldConfigSchemeManager, customFieldTypeModuleDescriptors);

        final IssueContext issueContext = mock(IssueContext.class);
        when(fieldConfigSchemeManager.getRelevantConfig(issueContext, customField)).thenReturn(null);
        when(customFieldTypeModuleDescriptors.getCustomFieldType(anyString())).thenReturn(some(mock(CustomFieldType.class)));

        final Object defaultValue = customField.getJsonDefaultValue(issueContext);

        assertThat(defaultValue, is(nullValue()));
    }

    @Test
    public void mutatingTheGVwillNotChangeCustomFieldValue() {
        final CustomFieldDTO dto = CustomFieldDTO.builder()
                .id(1L)
                .name("foo")
                .build();
        final ImmutableCustomField icf = new ImmutableCustomField(
                dto,
                mock(JiraAuthenticationContext.class),
                mock(FieldConfigSchemeManager.class),
                mock(PermissionManager.class),
                mock(RendererManager.class),
                mock(FieldConfigSchemeClauseContextUtil.class),
                mock(CustomFieldDescription.class),
                mock(FeatureManager.class),
                mock(TranslationManager.class),
                mock(CustomFieldScopeFactory.class),
                mock(CustomFieldTypeModuleDescriptors.class),
                mock(CustomFieldSearcherModuleDescriptors.class),
                ofBizDelegator
        );

        GenericValue gv = icf.getGenericValue();
        gv.set("name", "bar");
        assertThat(gv.get("name"), is("bar"));
        assertThat(icf.getName(), is("foo"));
    }

    @Test
    public void shouldGetIssueTypesWithNullValuesForGlobalSchemes() {
        final FieldConfigSchemeManager fieldConfigSchemeManager = mock(FieldConfigSchemeManager.class);
        final ImmutableCustomField customField = customFieldWith(fieldConfigSchemeManager);
        final FieldConfigScheme fieldConfigScheme = mock(FieldConfigScheme.class);
        final FieldConfigScheme fieldConfigSchemeContainingNulls = mock(FieldConfigScheme.class);
        final IssueType issueType = mock(IssueType.class);

        when(fieldConfigScheme.getAssociatedIssueTypes()).thenReturn(ImmutableList.of(issueType));
        when(fieldConfigSchemeContainingNulls.getAssociatedIssueTypes()).thenReturn(Collections.singleton(null));
        when(fieldConfigSchemeManager.getConfigSchemesForField(customField)).thenReturn(ImmutableList.of(fieldConfigScheme, fieldConfigSchemeContainingNulls));
        final List<IssueType> associatedIssueTypes = customField.getAssociatedIssueTypes();

        assertThat(associatedIssueTypes, containsInAnyOrder(issueType, null));
    }

    private ImmutableCustomField customFieldWith(final FieldConfigSchemeManager fieldConfigSchemeManager, final CustomFieldTypeModuleDescriptors customFieldTypeModuleDescriptors) {
        return new ImmutableCustomField(
                anyCustomFieldDTO(),
                mock(JiraAuthenticationContext.class),
                fieldConfigSchemeManager,
                mock(PermissionManager.class),
                mock(RendererManager.class),
                mock(FieldConfigSchemeClauseContextUtil.class),
                mock(CustomFieldDescription.class),
                mock(FeatureManager.class),
                mock(TranslationManager.class),
                mock(CustomFieldScopeFactory.class),
                customFieldTypeModuleDescriptors,
                mock(CustomFieldSearcherModuleDescriptors.class),
                ofBizDelegator
        );
    }

    private ImmutableCustomField customFieldWith(final FieldConfigSchemeManager fieldConfigSchemeManager) {
        return new ImmutableCustomField(
                anyCustomFieldDTO(),
                mock(JiraAuthenticationContext.class),
                fieldConfigSchemeManager,
                mock(PermissionManager.class),
                mock(RendererManager.class),
                mock(FieldConfigSchemeClauseContextUtil.class),
                mock(CustomFieldDescription.class),
                mock(FeatureManager.class),
                mock(TranslationManager.class),
                mock(CustomFieldScopeFactory.class),
                mock(CustomFieldTypeModuleDescriptors.class),
                mock(CustomFieldSearcherModuleDescriptors.class),
                ofBizDelegator
        );
    }

    private List<IssueContext> asList(final IssueContext... issueContexts) {
        return Arrays.asList(issueContexts);
    }

    private Project projectWithId(final Long id) {
        final Project project = mock(Project.class);
        when(project.getId()).thenReturn(id);
        return project;
    }

    private Project anyProject() {
        return projectWithId(null);
    }

    private List<String> anyIssueTypeIds() {
        return Arrays.asList("bug", "task");
    }

    private static CustomFieldDTO anyCustomFieldDTO() {
        return mock(CustomFieldDTO.class);
    }

    private ImmutableCustomField customFieldWithScope(final CustomFieldScope scope) {
        final CustomFieldScopeFactory scopeFactory = mock(CustomFieldScopeFactory.class);
        final ImmutableCustomField customField = new ImmutableCustomField(
                anyCustomFieldDTO(),
                mock(JiraAuthenticationContext.class),
                mock(FieldConfigSchemeManager.class),
                mock(PermissionManager.class),
                mock(RendererManager.class),
                mock(FieldConfigSchemeClauseContextUtil.class),
                mock(CustomFieldDescription.class),
                mock(FeatureManager.class),
                mock(TranslationManager.class),
                scopeFactory,
                mock(CustomFieldTypeModuleDescriptors.class),
                mock(CustomFieldSearcherModuleDescriptors.class),
                ofBizDelegator
        );

        when(scopeFactory.createFor(customField)).thenReturn(scope);

        return customField;
    }
}
