package com.atlassian.jira.help;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.jira.application.ApplicationRole;
import com.atlassian.jira.application.ApplicationRoleManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.google.common.collect.ImmutableSet;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Arrays;
import java.util.Collection;
import java.util.Locale;
import java.util.Set;
import java.util.stream.Collectors;

import static com.atlassian.jira.application.ApplicationKeys.CORE;
import static com.atlassian.jira.application.ApplicationKeys.SERVICE_DESK;
import static com.atlassian.jira.application.ApplicationKeys.SOFTWARE;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(Parameterized.class)
public class DefaultHelpUrlsApplicationKeyProviderTest {

    private static final TestUser ANON = TestUser.ANONYMOUS;
    private static final TestUser USER = TestUser.LOGGED_IN;

    private static final Object[][] testcases = new Object[][]{
            {USER, hasAccessTo(), getsHelpFrom(CORE)},
            {ANON, hasAccessTo(), getsHelpFrom(CORE)},
            {USER, hasAccessTo(), getsHelpFrom(CORE)},
            {ANON, hasAccessTo(), getsHelpFrom(CORE)},
            {USER, hasAccessTo(CORE), getsHelpFrom(CORE)},
            {ANON, hasAccessTo(CORE), getsHelpFrom(CORE)},
            {USER, hasAccessTo(SOFTWARE), getsHelpFrom(SOFTWARE)},
            {ANON, hasAccessTo(SOFTWARE), getsHelpFrom(CORE)},
            {USER, hasAccessTo(SERVICE_DESK), getsHelpFrom(SERVICE_DESK)},
            {ANON, hasAccessTo(SERVICE_DESK), getsHelpFrom(CORE)},
            {USER, hasAccessTo(SERVICE_DESK, SOFTWARE), getsHelpFrom(CORE)},
            {ANON, hasAccessTo(SERVICE_DESK, SOFTWARE), getsHelpFrom(CORE)},
            {USER, hasAccessTo(CORE, SOFTWARE), getsHelpFrom(SOFTWARE)},
            {ANON, hasAccessTo(CORE, SOFTWARE), getsHelpFrom(CORE)},
            {USER, hasAccessTo(CORE, SERVICE_DESK), getsHelpFrom(SERVICE_DESK)},
            {ANON, hasAccessTo(CORE, SERVICE_DESK), getsHelpFrom(CORE)},
            {USER, hasAccessTo(CORE, SERVICE_DESK, SOFTWARE), getsHelpFrom(CORE)},
            {ANON, hasAccessTo(CORE, SERVICE_DESK, SOFTWARE), getsHelpFrom(CORE)},
    };

    private static ImmutableSet<ApplicationKey> hasAccessTo(final ApplicationKey... keys) {
        return ImmutableSet.copyOf(Arrays.asList(keys));
    }

    private final TestUser user;
    private final Set<ApplicationKey> userRolesApplicationKeys;
    private final ApplicationKey expectedKey;

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @Mock
    ApplicationRoleManager applicationRoleManager;

    @Mock
    JiraAuthenticationContext ctx;

    @Mock
    ApplicationUser mockUser;

    @InjectMocks
    private DefaultHelpUrlsApplicationKeyProvider provider;

    @Before
    public void setUp() {
        when(ctx.getLocale()).thenReturn(Locale.GERMAN);
    }

    public DefaultHelpUrlsApplicationKeyProviderTest(TestUser user,
                                                     Set<ApplicationKey> userRolesApplicationKeys, ApplicationKey expectedKey) {
        this.user = user;
        this.userRolesApplicationKeys = userRolesApplicationKeys;
        this.expectedKey = expectedKey;
    }

    @Parameterized.Parameters(name = "JIRA {0} and user is {1} and user has application access to {2} the help should be taken from {3}")
    public static Collection input() {
        return Arrays.asList(testcases);
    }

    @Test
    public void testApplicationKey() {
        when(ctx.getLoggedInUser()).thenReturn(user == USER.LOGGED_IN ? mockUser : null);
        whenSpecifiedRolesAvailableForUser();

        assertThat(provider.getApplicationKeyForUser(), is(expectedKey));
    }

    private void whenSpecifiedRolesAvailableForUser() {
        final Set<ApplicationRole> roles = userRolesApplicationKeys.stream().map(key ->
        {
            ApplicationRole role = mock(ApplicationRole.class);
            when(role.getKey()).thenReturn(key);
            when(role.isDefined()).thenReturn(true);
            return role;
        }).collect(Collectors.toSet());
        switch (user) {
            case LOGGED_IN:
                when(applicationRoleManager.getRolesForUser(mockUser)).thenReturn(roles);
                break;
            case ANONYMOUS:
                when(applicationRoleManager.getRoles()).thenReturn(roles);
                break;
        }
    }

    private static ApplicationKey getsHelpFrom(ApplicationKey key) {
        return key;
    }

    private enum TestUser {
        ANONYMOUS(false), LOGGED_IN(true);

        private final boolean loggedIn;

        TestUser(final boolean loggedIn) {
            this.loggedIn = loggedIn;
        }

        @Override
        public String toString() {
            return (loggedIn ? "logged in" : "anonymous");
        }
    }
}