package com.atlassian.jira.web.util;

import com.atlassian.application.api.PlatformApplication;
import com.atlassian.application.host.ApplicationConfigurationManager;
import com.atlassian.application.host.plugin.MockPluginApplicationMetaDataManager;
import com.atlassian.fugue.Option;
import com.atlassian.jira.application.JiraApplicationManager;
import com.atlassian.jira.application.MockApplicationAccessFactory;
import com.atlassian.jira.application.MockLicenseLocator;
import com.atlassian.jira.help.HelpUrl;
import com.atlassian.jira.help.HelpUrls;
import com.atlassian.jira.help.HelpUrlsParser;
import com.atlassian.jira.help.InitialHelpUrlsParserBuilderFactory;
import com.atlassian.jira.help.MockLocalHelpUrls;
import com.atlassian.jira.mock.MockFeatureManager;
import com.atlassian.jira.mock.security.MockAuthenticationContext;
import com.atlassian.jira.util.BuildUtilsInfoImpl;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.output.NullOutputStream;
import org.apache.commons.lang3.ArrayUtils;
import org.joda.time.Period;
import org.joda.time.format.PeriodFormatter;
import org.joda.time.format.PeriodFormatterBuilder;
import org.mockito.Mockito;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import static com.google.common.collect.ImmutableList.copyOf;

/**
 * Utility class for checking that the JIRA help links are not broken.
 * Only <strong>Core</strong> help urls are currently supported.
 * <p>
 * Accepts two optional parameters:
 * <li>
 * <ul>{@code ondemand} - to check ondemand keys (and ondemand url-prefix and help space)</ul>
 * <ul>{@code legacy} - to parse legacy help paths file used in 'dark ages' mode</ul>
 * </li>
 */
public class HelpUrlsChecker {
    public static final String HELP_PATHS_LEGACY_PROPERTIES = "help-paths.properties";
    public static final String HELP_PATHS_USER_PROPERTIES = "help-paths-user.properties";
    public static final String HELP_PATHS_ADMIN_PROPERTIES = "help-paths-admin.properties";
    private final boolean parseLegacyFile;

    public HelpUrlsChecker(final boolean parseLegacyFile) {
        this.parseLegacyFile = parseLegacyFile;
    }

    // run this from IDEA to check all help links
    public static void main(String[] args) throws Exception {
        new HelpUrlsChecker(ArrayUtils.contains(args, "legacy")).checkHelpUtilLinks();
    }

    /**
     * Thread local HttpClient.
     */
    private final ThreadLocal<HttpClient> httpClient = new ThreadLocal<HttpClient>() {
        @Override
        protected HttpClient initialValue() {
            return new HttpClient();
        }
    };

    /**
     * Number of threads to use for checking links. Y U NO ASYNC IO?!
     */
    private final int nThreads = 20;

    /**
     * Thread pool to use for link checking.
     */
    private ExecutorService threadPool = Executors.newFixedThreadPool(nThreads);

    public void checkHelpUtilLinks() throws Exception {
        final long start = System.currentTimeMillis();

        HelpUrls helpUtil = load();
        List<CheckResult> allLinks = followLinksAndReportErrors(helpUtil);
        Collection<CheckResult> brokenLinks = copyOf(Iterables.filter(allLinks, new FailedCheck()));

        // kill threads before exiting
        threadPool.shutdown();
        System.err.flush();
        System.out.flush();

        Period elapsed = new Period(System.currentTimeMillis() - start);
        System.out.format("Found %d broken links in %s (checked %d in total).\n", brokenLinks.size(), formatter().print(elapsed), allLinks.size());
        System.exit(brokenLinks.isEmpty() ? 0 : 1);
    }

    private List<CheckResult> followLinksAndReportErrors(final HelpUrls helpUtil) throws Exception {
        List<CheckResult> followedLinks = Lists.newArrayList();
        List<Future<CheckResult>> urlChecks = checkLinksInThreadPool(helpUtil);
        for (Future<CheckResult> urlCheck : urlChecks) {
            CheckResult result = urlCheck.get();
            if (result.status != 200) {
                System.err.format("%s returned %d -> [%s]\n", result.helpPath.getKey(), result.status, result.helpPath.getUrl());
            }

            followedLinks.add(result);
        }

        return followedLinks;
    }

    private List<Future<CheckResult>> checkLinksInThreadPool(final HelpUrls urls) {
        final List<Future<CheckResult>> futures = Lists.newArrayList();

        // schedule a job for each link
        for (final String key : urls.getUrlKeys()) {
            futures.add(threadPool.submit(() -> {
                HelpUrl helpLink = urls.getUrl(key);
                GetMethod get = null;
                try {
                    get = new GetMethod(helpLink.getUrl());
                    int resp = httpClient.get().executeMethod(get);
                    IOUtils.copy(get.getResponseBodyAsStream(), NullOutputStream.NULL_OUTPUT_STREAM);

                    return new CheckResult(resp, helpLink);
                } catch (Exception e) {
                    return new CheckResult(-1, helpLink);
                } finally {
                    if (get != null) {
                        get.releaseConnection();
                    }
                }
            }));
        }

        return futures;
    }

    private HelpUrls load() {
        InitialHelpUrlsParserBuilderFactory factory =
                new InitialHelpUrlsParserBuilderFactory(new MockFeatureManager(), new MockLocalHelpUrls(), new BuildUtilsInfoImpl());
        HelpUrlsParser parser = factory.newBuilder()
                .applicationHelpSpace(Option.option(getPlatformHelpSpace().get().toString()))
                .build();

        return parseLegacyFile ? parser.parse(loadProperties(HELP_PATHS_LEGACY_PROPERTIES)) : parser.parse(loadProperties(HELP_PATHS_USER_PROPERTIES), loadProperties(HELP_PATHS_ADMIN_PROPERTIES));
    }

    private Option<URI> getPlatformHelpSpace() {
        // yup, it's all done only to avoid the platform help space URI being hardcoded here
        PlatformApplication platform = new JiraApplicationManager(new MockPluginApplicationMetaDataManager(), new BuildUtilsInfoImpl(), new MockAuthenticationContext(null),
                new MockLicenseLocator(), new MockApplicationAccessFactory(), Mockito.mock(ApplicationConfigurationManager.class)).getPlatform();
        return platform.getProductHelpServerSpaceURI();
    }

    private Map<String, String> loadProperties(final String fileName) {
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(fileName);
        if (inputStream == null) {
            throw new RuntimeException("Unable to find " + fileName);
        }

        try {
            Properties properties = new Properties();
            properties.load(inputStream);

            Map<String, String> result = Maps.newHashMap();
            for (String propertyName : properties.stringPropertyNames()) {
                result.put(propertyName, properties.getProperty(propertyName));
            }
            return result;
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            IOUtils.closeQuietly(inputStream);
        }
    }

    private PeriodFormatter formatter() {
        return new PeriodFormatterBuilder()
                .appendHours().appendSuffix("h")
                .appendMinutes().appendSuffix("m")
                .appendSeconds().appendSuffix("s")
                .toFormatter();
    }

    private static class CheckResult {
        public final int status;
        public final HelpUrl helpPath;

        private CheckResult(final int status, final HelpUrl helpPath) {
            this.status = status;
            this.helpPath = helpPath;
        }
    }

    private static class FailedCheck implements Predicate<CheckResult> {
        @Override
        public boolean apply(CheckResult result) {
            return result.status != 200;
        }
    }
}
