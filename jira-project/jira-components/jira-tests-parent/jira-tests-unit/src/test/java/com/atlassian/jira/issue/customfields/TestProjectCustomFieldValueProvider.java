package com.atlassian.jira.issue.customfields;

import com.atlassian.jira.issue.customfields.converters.ProjectConverter;
import com.atlassian.jira.issue.customfields.impl.FieldValidationException;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.transport.FieldValuesHolder;
import com.atlassian.jira.issue.transport.impl.FieldValuesHolderImpl;
import com.atlassian.jira.project.MockProject;
import com.google.common.collect.ImmutableList;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import java.util.Collections;

import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestProjectCustomFieldValueProvider {
    @Rule
    public final MethodRule initMockito = MockitoJUnit.rule();

    @Mock
    private ProjectConverter projectConverter;
    @Mock
    private CustomField customField;
    @Mock
    private CustomFieldType customFieldType;
    private FieldValuesHolder holder = new FieldValuesHolderImpl();
    private ProjectCustomFieldValueProvider valueProvider;

    @Before
    public void setUp() {
        when(customField.getCustomFieldType()).thenReturn(customFieldType);
        valueProvider = new ProjectCustomFieldValueProvider(projectConverter);
    }

    @Test
    public void getStringValueNoProjectIsNull() throws Exception {
        final ProjectCustomFieldValueProvider mockValueProvider = new ProjectCustomFieldValueProvider(projectConverter) {
            @Override
            public com.atlassian.jira.project.Project getValue(final CustomField customField, final FieldValuesHolder fieldValuesHolder) {
                return null;
            }
        };

        assertThat(mockValueProvider.getStringValue(customField, holder), nullValue());
    }

    @Test
    public void getStringValue() throws Exception {
        final ProjectCustomFieldValueProvider mockValueProvider = new ProjectCustomFieldValueProvider(projectConverter) {
            @Override
            public com.atlassian.jira.project.Project getValue(final CustomField customField, final FieldValuesHolder fieldValuesHolder) {
                return new MockProject(12345L);
            }
        };

        assertThat(mockValueProvider.getStringValue(customField, holder), is("12345"));
    }

    @Test
    public void getValueSingleStringIdExists() throws Exception {
        MockProject project = new MockProject();
        when(customFieldType.getStringValueFromCustomFieldParams(null)).thenReturn("id");
        when(projectConverter.getProject("id")).thenReturn(project);

        assertThat(valueProvider.getValue(customField, holder), is(project));
    }

    @Test
    public void getValueListIdExists() throws Exception {
        MockProject project = new MockProject();
        when(customFieldType.getStringValueFromCustomFieldParams(null)).thenReturn(ImmutableList.of("id", "id1"));
        when(projectConverter.getProject("id")).thenReturn(project);

        assertThat(valueProvider.getValue(customField, holder), is(project));
    }

    @Test
    public void getValueListIdNotStringIsNull() throws Exception {
        when(customFieldType.getStringValueFromCustomFieldParams(null)).thenReturn(ImmutableList.of(10L, "id1"));

        assertThat(valueProvider.getValue(customField, holder), nullValue());
    }

    @Test
    public void getValueEmptyListIsNull() throws Exception {
        when(customFieldType.getStringValueFromCustomFieldParams(null)).thenReturn(Collections.emptyList());

        assertThat(valueProvider.getValue(customField, holder), nullValue());
    }

    @Test
    public void getValueSingleIdNotStringIsNull() throws Exception {
        final CustomFieldType customFieldType = mock(CustomFieldType.class);
        when(customFieldType.getStringValueFromCustomFieldParams(null)).thenReturn(10L);

        assertThat(valueProvider.getValue(customField, holder), nullValue());
    }

    @Test
    public void getValueSingleStringIdDoesNotExistsExceptionIsNull() throws Exception {
        when(customFieldType.getStringValueFromCustomFieldParams(null)).thenReturn("id");
        when(projectConverter.getProject("id")).thenThrow(new FieldValidationException("blarg!"));

        assertThat(valueProvider.getValue(customField, holder), nullValue());
    }

    @Test
    public void getValueSingleStringIdDoesNotExistsIsNull() throws Exception {
        when(customFieldType.getStringValueFromCustomFieldParams(null)).thenReturn("id");
        when(projectConverter.getProject("id")).thenReturn(null);

        assertThat(valueProvider.getValue(customField, holder), nullValue());
    }

}
