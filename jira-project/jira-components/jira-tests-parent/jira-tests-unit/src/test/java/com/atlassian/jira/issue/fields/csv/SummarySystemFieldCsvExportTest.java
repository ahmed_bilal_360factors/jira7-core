package com.atlassian.jira.issue.fields.csv;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.export.FieldExportParts;
import com.atlassian.jira.issue.fields.SummarySystemField;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;

import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.NoopI18nHelper;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.mockito.Mock;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

public class SummarySystemFieldCsvExportTest {
    @Rule
    public final RuleChain mockito = MockitoMocksInContainer.forTest(this);

    private static final String SUMMARY = "Issue summary";


    @Mock
    @AvailableInContainer
    private JiraAuthenticationContext authenticationContext;

    private I18nHelper i18nHelper = new NoopI18nHelper();

    @Mock
    private Issue issue;

    private SummarySystemField field;

    @Before
    public void setUp() {
        when(authenticationContext.getI18nHelper()).thenReturn(i18nHelper);

        field = new SummarySystemField(null, null, authenticationContext, null, null, null);
    }

    @Test
    public void testCsvRepresentationForExpectedTypeAndValues() {
        when(issue.getSummary()).thenReturn(SUMMARY);

        final FieldExportParts representation = field.getRepresentationFromIssue(issue);
        assertThat(representation.getParts(), hasSize(1));
        assertThat(representation.getParts().get(0).getValues()::iterator, contains(SUMMARY));
    }
}
