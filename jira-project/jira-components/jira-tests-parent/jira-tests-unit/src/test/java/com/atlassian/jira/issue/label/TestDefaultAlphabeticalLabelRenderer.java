package com.atlassian.jira.issue.label;

import com.atlassian.jira.action.issue.customfields.MockCustomFieldType;
import com.atlassian.jira.issue.fields.Field;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.fields.MockCustomField;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.template.VelocityTemplatingEngine;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.JiraVelocityUtils;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatcher;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Map;
import java.util.Set;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyMapOf;
import static org.mockito.Matchers.argThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestDefaultAlphabeticalLabelRenderer {
    private static final long PROJECT_ID = 10000L;
    private static final String FIELD_ID = "someFieldId";
    private static final String EXPECTED_HTML = "theHtml";
    private static final String FIELD_NAME = "someName";
    private static final int LABEL_COUNT = 5;
    private static final String CUSTOM_FIELD_ID = "customfield_10000";
    private static final String PROJECT_NAME = "someProjectName";

    @Mock
    private AlphabeticalLabelGroupingService alphabeticalLabelGroupingService;
    @Mock
    private JiraVelocityUtils velocityUtils;
    @Mock
    private FieldManager fieldManager;
    @Mock
    private ProjectManager projectManager;
    @Mock
    private LabelUtil labelUtil;
    @Mock
    private I18nHelper i18nHelper;
    @Mock
    private JiraAuthenticationContext authenticationContext;
    @Mock
    private I18nHelper.BeanFactory beanFactory;
    @Mock
    private VelocityTemplatingEngine velocityTemplatingEngine;
    @Mock
    private VelocityTemplatingEngine.RenderRequest renderRequest;

    private AlphabeticalLabelGroupingSupport alphabeticalLabelGroupingSupport;
    private ApplicationUser user = new MockApplicationUser("someUser");
    private DefaultAlphabeticalLabelRenderer defaultAlphabeticalLabelRenderer;

    @Before
    public void setUp() {
        Set<String> labels = Sets.newHashSet("label1", "label2", "label3", "label4", "label5");
        alphabeticalLabelGroupingSupport = new AlphabeticalLabelGroupingSupport(labels);

        defaultAlphabeticalLabelRenderer = new DefaultAlphabeticalLabelRenderer(
                alphabeticalLabelGroupingService,
                velocityTemplatingEngine,
                authenticationContext,
                fieldManager,
                projectManager,
                labelUtil,
                beanFactory
        ) {
            @Override
            Map<String, Object> getDefaultVelocityParams() {
                return Maps.newHashMap();
            }
        };
    }

    @Test
    public void shouldApplyCorrectArgumentsWhenOtherFieldsExistAndIsCustomField() {
        assertFieldWasCorrectForIsCustomFieldAndOtherFieldsExist(getMockCustomField(), true, true);
    }

    @Test
    public void shouldApplyCorrectArgumentsWhenOtherFieldsDoNotExistAndIsCustomField() {
        assertFieldWasCorrectForIsCustomFieldAndOtherFieldsExist(getMockNormalField(), false, true);
    }

    @Test
    public void shouldApplyCorrectArgumentsWhenOtherFieldsExistAndIsNotCustomField() {
        assertFieldWasCorrectForIsCustomFieldAndOtherFieldsExist(getMockCustomField(), true, false);
    }

    @Test
    public void shouldApplyCorrectArgumentsWhenOtherFieldsDoNotExistAndIsNotCustomField() {
        assertFieldWasCorrectForIsCustomFieldAndOtherFieldsExist(getMockNormalField(), false, false);
    }

    private Project getMockProject() {
        return new MockProject(PROJECT_ID, PROJECT_NAME);
    }

    private Field getMockNormalField() {
        return new MockCustomField(FIELD_ID, FIELD_NAME, new MockCustomFieldType("someKey", "someFieldTypeName"));
    }

    private Field getMockCustomField() {
        return new MockCustomField(CUSTOM_FIELD_ID, FIELD_NAME, new MockCustomFieldType("someKey", "someFieldTypeName"));
    }

    private void assertFieldWasCorrectForIsCustomFieldAndOtherFieldsExist(Field field, Boolean isCustomField, Boolean otherFieldsExist) {
        Project project = getMockProject();
        setUpStandardMocks(project, field);

        String html = defaultAlphabeticalLabelRenderer.getHtml(user, project.getId(), field.getId(), otherFieldsExist);

        assertThat(html, is(EXPECTED_HTML));
        verify(renderRequest).applying(argThat(
                        argumentMatcherWithValues(field.getId(), field.getName(), isCustomField, project.getId(), project.getName(), LABEL_COUNT, otherFieldsExist))
        );
    }

    private ArgumentMatcher<Map<String, Object>> argumentMatcherWithValues(
            String fieldId,
            String fieldName,
            Boolean isCustomField,
            Long projectId,
            String projectName,
            Integer labelCount,
            Boolean otherFieldsExist) {
        return new ArgumentMatcher<Map<String, Object>>() {
            @Override
            public boolean matches(final Object o) {
                Map<String, Object> params = (Map<String, Object>) o;
                assertThat(params.get("isCustomField"), is(isCustomField));

                Field field = (Field) params.get("field");
                assertThat(field.getId(), is(fieldId));
                assertThat(field.getName(), is(fieldName));

                Project project = (Project) params.get("project");
                assertThat(project.getId(), is(projectId));
                assertThat(project.getName(), is(projectName));

                assertThat((Integer) params.get("labelCount"), is(labelCount));
                assertThat((Boolean) params.get("isOtherFieldsExist"), is(otherFieldsExist));

                return true;
            }
        };
    }

    private void setUpStandardMocks(Project project, Field field) {
        when(fieldManager.getField(field.getId())).thenReturn(field);
        when(projectManager.getProjectObj(project.getId())).thenReturn(project);
        when(alphabeticalLabelGroupingService.getAlphabeticallyGroupedLabels(any(), eq(project.getId()), eq(field.getId())))
                .thenReturn(alphabeticalLabelGroupingSupport);
        when(beanFactory.getInstance(user)).thenReturn(i18nHelper);
        when(velocityTemplatingEngine.render(any())).thenReturn(renderRequest);
        when(renderRequest.applying(anyMapOf(String.class, Object.class))).thenReturn(renderRequest);
        when(renderRequest.asHtml()).thenReturn(EXPECTED_HTML);
    }
}
