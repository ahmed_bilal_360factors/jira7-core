package com.atlassian.jira.security.xsrf;

import com.atlassian.jira.util.ComponentLocator;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import webwork.action.ActionSupport;
import webwork.action.CommandDriven;
import webwork.config.util.ActionInfo;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

/**
 * @since v4.1
 */
public class TestDefaultXsrfInvocationChecker {
    @Rule
    public MockitoRule mockito = MockitoJUnit.rule();
    @Mock
    private ComponentLocator componentLocator;
    @Mock
    private XsrfDefaults xsrfDefaults;
    @Mock
    private XsrfTokenGenerator xsrfTokenGenerator;
    @Mock
    private ActionInfo actionInfo;
    @Mock
    private HttpServletRequest httpServletRequest;
    private Map<String, Object> actionParameters;
    private MockJiraWebActionSupport actionDoDefault;
    private MockJiraWebActionSupport actionDoExecute;
    private MockJiraWebActionSupport actionNullCommand;
    private MockJiraWebActionSupport actionNoSuchMethod;

    private DefaultXsrfInvocationChecker checker;

    private static final HashMap EMPTY_PARAMETERS = new HashMap();

    private class MockJiraWebActionSupport extends JiraWebActionSupport implements CommandDriven {
        private MockJiraWebActionSupport(String command) {
            this.command = command;
        }

        public String doDefault() {
            return "ok";
        }

        @RequiresXsrfCheck
        protected String doExecute() {
            return "ok";
        }

        @Override
        protected <T> T getComponentInstanceOfType(Class<T> clazz) {
            return null;
        }
    }

    private class BackendAction extends ActionSupport {
    }

    @Before
    public void setUp() throws Exception {
        when(componentLocator.getComponentInstanceOfType(XsrfDefaults.class)).thenReturn(xsrfDefaults);
        when(componentLocator.getComponentInstanceOfType(XsrfTokenGenerator.class)).thenReturn(xsrfTokenGenerator);

        actionParameters = new HashMap<>();
        actionDoDefault = new MockJiraWebActionSupport("default");
        actionDoExecute = new MockJiraWebActionSupport("execute");
        actionNullCommand = new MockJiraWebActionSupport(null);
        actionNoSuchMethod = new MockJiraWebActionSupport("nosuchmethod");

        checker = newXsrfInvocationChecker();
    }

    @Test
    public void testNoCheckNeeded_ActionGlobalXsrfProtectionIsOff() {
        when(httpServletRequest.getHeader("X-Atlassian-Token")).thenReturn(null);
        when(xsrfDefaults.isXsrfProtectionEnabled()).thenReturn(false);

        checker = newXsrfInvocationChecker();
        assertTrue(checker.checkActionInvocation(actionDoDefault, actionParameters).isValid());
    }

    @Test
    public void testNoCheckNeeded_WebRequestGlobalXsrfProtectionIsOff() {
        when(httpServletRequest.getParameterMap()).thenReturn(EMPTY_PARAMETERS);
        when(httpServletRequest.getHeader("X-Atlassian-Token")).thenReturn(null);
        when(xsrfDefaults.isXsrfProtectionEnabled()).thenReturn(false);

        checker = newXsrfInvocationChecker();
        assertTrue(checker.checkWebRequestInvocation(httpServletRequest).isValid());
    }

    @Test
    public void testNoCheckNeeded_BackendAction() {
        when(httpServletRequest.getHeader("X-Atlassian-Token")).thenReturn(null);
        when(xsrfDefaults.isXsrfProtectionEnabled()).thenReturn(true);

        checker = newXsrfInvocationChecker();

        final BackendAction backendAction = new BackendAction();
        assertTrue(checker.checkActionInvocation(backendAction, actionParameters).isValid());
    }

    @Test
    public void testNoCheckNeeded_ActionCommandWithoutXsrfAnnotation() {
        when(httpServletRequest.getHeader("X-Atlassian-Token")).thenReturn(null);
        when(xsrfDefaults.isXsrfProtectionEnabled()).thenReturn(true);

        checker = newXsrfInvocationChecker();
        assertTrue(checker.checkActionInvocation(actionDoDefault, actionParameters).isValid());
    }

    @Test
    public void testNoCheckNeeded_ActionHasOverrideHeader() {
        when(httpServletRequest.getHeader("X-Atlassian-Token")).thenReturn("no-check");

        checker = newXsrfInvocationChecker();
        assertTrue(checker.checkActionInvocation(actionDoDefault, actionParameters).isValid());
    }

    @Test
    public void testNoCheckNeeded_WebRequestHasOverrideHeader() {
        when(httpServletRequest.getParameterMap()).thenReturn(EMPTY_PARAMETERS);
        when(httpServletRequest.getHeader("X-Atlassian-Token")).thenReturn("no-check");

        checker = newXsrfInvocationChecker();
        XsrfCheckResult checkResult = checker.checkWebRequestInvocation(httpServletRequest);
        assertTrue(checkResult.isValid());
        assertFalse(checkResult.isRequired());
    }

    @Test
    public void testNoCheckNeeded_ActionNoCommandMethodDefined() {
        when(httpServletRequest.getHeader("X-Atlassian-Token")).thenReturn(null);
        when(xsrfDefaults.isXsrfProtectionEnabled()).thenReturn(true);

        checker = newXsrfInvocationChecker();
        XsrfCheckResult xsrfCheckResult = checker.checkActionInvocation(actionNoSuchMethod, actionParameters);
        assertTrue(xsrfCheckResult.isValid());
        assertFalse(xsrfCheckResult.isRequired());
    }

    @Test
    public void testCheckNeeded_ActionWithoutToken() {
        when(httpServletRequest.getHeader("X-Atlassian-Token")).thenReturn(null);
        when(xsrfDefaults.isXsrfProtectionEnabled()).thenReturn(true);
        when(xsrfTokenGenerator.validateToken(httpServletRequest, null)).thenReturn(false);
        when(xsrfTokenGenerator.generatedByAuthenticatedUser(null)).thenReturn(false);

        checker = newXsrfInvocationChecker();
        assertFalse(checker.checkActionInvocation(actionDoExecute, actionParameters).isValid());
    }

    @Test
    public void testCheckNeeded_WebRequestWithoutToken() {
        when(httpServletRequest.getParameterMap()).thenReturn(EMPTY_PARAMETERS);
        when(httpServletRequest.getHeader("X-Atlassian-Token")).thenReturn(null);

        when(xsrfDefaults.isXsrfProtectionEnabled()).thenReturn(true);

        when(xsrfTokenGenerator.validateToken(httpServletRequest, null)).thenReturn(false);
        when(xsrfTokenGenerator.generatedByAuthenticatedUser(null)).thenReturn(false);

        checker = newXsrfInvocationChecker();
        XsrfCheckResult checkResult = checker.checkWebRequestInvocation(httpServletRequest);
        assertFalse(checkResult.isValid());
        assertTrue(checkResult.isRequired());
    }

    @Test
    public void testCheckNeeded_ActionWithBadToken() {
        when(httpServletRequest.getHeader("X-Atlassian-Token")).thenReturn(null);
        when(xsrfDefaults.isXsrfProtectionEnabled()).thenReturn(true);

        actionParameters.put(XsrfTokenGenerator.TOKEN_WEB_PARAMETER_KEY, new String[]{"abc123"});


        when(xsrfTokenGenerator.validateToken(httpServletRequest, "abc123")).thenReturn(false);
        when(xsrfTokenGenerator.generatedByAuthenticatedUser("abc123")).thenReturn(false);

        checker = newXsrfInvocationChecker();
        XsrfCheckResult checkResult = checker.checkActionInvocation(actionDoExecute, actionParameters);
        assertFalse(checkResult.isValid());
        assertTrue(checkResult.isRequired());
        assertFalse(checkResult.isGeneratedForAuthenticatedUser());
    }

    @Test
    public void testCheckNeeded_WebRequestWithBadToken() {
        Map<String, String[]> parametersWithToken = new HashMap<>();
        parametersWithToken.put(XsrfTokenGenerator.TOKEN_WEB_PARAMETER_KEY, new String[]{"abc123"});

        when(httpServletRequest.getParameterMap()).thenReturn(parametersWithToken);
        when(httpServletRequest.getHeader("X-Atlassian-Token")).thenReturn(null);
        when(xsrfDefaults.isXsrfProtectionEnabled()).thenReturn(true);

        when(xsrfTokenGenerator.validateToken(httpServletRequest, "abc123")).thenReturn(false);
        when(xsrfTokenGenerator.generatedByAuthenticatedUser("abc123")).thenReturn(false);

        checker = newXsrfInvocationChecker();
        assertFalse(checker.checkWebRequestInvocation(httpServletRequest).isValid());
    }

    @Test
    public void testCheckNeeded_ActionWithGoodToken() {
        when(httpServletRequest.getHeader("X-Atlassian-Token")).thenReturn(null);
        when(xsrfDefaults.isXsrfProtectionEnabled()).thenReturn(true);

        actionParameters.put(XsrfTokenGenerator.TOKEN_WEB_PARAMETER_KEY, new String[]{"abc123"});

        when(xsrfTokenGenerator.validateToken(httpServletRequest, "abc123")).thenReturn(true);
        when(xsrfTokenGenerator.generatedByAuthenticatedUser("abc123")).thenReturn(true);

        checker = newXsrfInvocationChecker();
        XsrfCheckResult checkResult = checker.checkActionInvocation(actionDoExecute, actionParameters);
        assertTrue(checkResult.isValid());
        assertTrue(checkResult.isRequired());
        assertTrue(checkResult.isGeneratedForAuthenticatedUser());
    }

    @Test
    public void testCheckNeeded_WebRequestWithGoodToken() {
        Map<String, String[]> parametersWithToken = new HashMap<>();
        parametersWithToken.put(XsrfTokenGenerator.TOKEN_WEB_PARAMETER_KEY, new String[]{"abc123"});

        when(httpServletRequest.getParameterMap()).thenReturn(parametersWithToken);
        when(httpServletRequest.getHeader("X-Atlassian-Token")).thenReturn(null);
        when(xsrfDefaults.isXsrfProtectionEnabled()).thenReturn(true);

        when(xsrfTokenGenerator.validateToken(httpServletRequest, "abc123")).thenReturn(true);
        when(xsrfTokenGenerator.generatedByAuthenticatedUser("abc123")).thenReturn(false);

        checker = newXsrfInvocationChecker();
        assertTrue(checker.checkWebRequestInvocation(httpServletRequest).isValid());
    }

    @Test
    public void testCheckNeeded_ActionWithGoodTokenButNullCommand() {
        when(httpServletRequest.getHeader("X-Atlassian-Token")).thenReturn(null);
        when(xsrfDefaults.isXsrfProtectionEnabled()).thenReturn(true);

        actionParameters.put(XsrfTokenGenerator.TOKEN_WEB_PARAMETER_KEY, new String[]{"abc123"});

        when(xsrfTokenGenerator.validateToken(httpServletRequest, "abc123")).thenReturn(true);
        when(xsrfTokenGenerator.generatedByAuthenticatedUser("abc123")).thenReturn(false);

        checker = newXsrfInvocationChecker();
        XsrfCheckResult checkResult = checker.checkActionInvocation(actionNullCommand, actionParameters);
        assertTrue(checkResult.isValid());
        assertTrue(checkResult.isRequired());
        assertFalse(checkResult.isGeneratedForAuthenticatedUser());
    }

    private DefaultXsrfInvocationChecker newXsrfInvocationChecker() {
        return new DefaultXsrfInvocationChecker(componentLocator) {
            @Override
            ActionInfo getActionInfo(final String magicKey) {
                return actionInfo;
            }

            @Override
            HttpServletRequest getActionHttpRequest() {
                return httpServletRequest;
            }
        };
    }
}
