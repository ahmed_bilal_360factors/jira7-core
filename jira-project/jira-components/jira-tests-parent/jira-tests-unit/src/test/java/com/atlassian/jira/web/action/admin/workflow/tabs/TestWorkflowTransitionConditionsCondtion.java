package com.atlassian.jira.web.action.admin.workflow.tabs;

import com.atlassian.jira.workflow.JiraWorkflow;
import com.opensymphony.workflow.loader.ActionDescriptor;
import org.hamcrest.CoreMatchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.HashMap;
import java.util.Map;

import static com.atlassian.jira.web.action.admin.workflow.tabs.WorkflowTransitionContextUtils.TRANSITION_KEY;
import static com.atlassian.jira.web.action.admin.workflow.tabs.WorkflowTransitionContextUtils.WORKFLOW_KEY;
import static java.util.Collections.emptyMap;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

/**
 * This unit test is deliberately misspelled to match the production code,
 * for easier lookup by the IDE.
 */
@RunWith(MockitoJUnitRunner.class)
public class TestWorkflowTransitionConditionsCondtion {

    @Mock
    private ActionDescriptor actionDescriptor;

    @Mock
    private JiraWorkflow workflow;

    private Map<String, Object> context;

    private WorkflowTransitionConditionsCondtion condition;

    @Before
    public void setUp() {
        condition = new WorkflowTransitionConditionsCondtion();
        context = new HashMap<>();
    }

    @Test
    public void shouldDisplayForNonInitialTransition() {
        // Set up
        context.put(TRANSITION_KEY, actionDescriptor);
        context.put(WORKFLOW_KEY, workflow);
        when(workflow.isInitialAction(actionDescriptor)).thenReturn(false);

        // Invoke and check
        assertThat(condition.shouldDisplay(context), CoreMatchers.is(true));
    }

    @Test
    public void shouldNotDisplayForInitialTransition() {
        // Set up
        context.put(TRANSITION_KEY, actionDescriptor);
        context.put(WORKFLOW_KEY, workflow);
        when(workflow.isInitialAction(actionDescriptor)).thenReturn(true);

        // Invoke and check
        assertThat(condition.shouldDisplay(context), CoreMatchers.is(false));
    }

    @Test
    public void shouldNotDisplayWhenContextHasNoWorkflow() {
        // Set up
        context.put(TRANSITION_KEY, actionDescriptor);
        when(workflow.isInitialAction(actionDescriptor)).thenReturn(false);

        // Invoke and check
        assertThat(condition.shouldDisplay(context), CoreMatchers.is(false));
    }

    @Test
    public void shouldNotDisplayWhenContextHasNoTransition() {
        // Set up
        context.put(WORKFLOW_KEY, workflow);
        when(workflow.isInitialAction(actionDescriptor)).thenReturn(false);

        // Invoke and check
        assertThat(condition.shouldDisplay(context), CoreMatchers.is(false));
    }

    @Test
    public void shouldNotDisplayWhenContextHasNoTransitionOrWorkflow() {
        // Set up
        context.put("irrelevantKey1", actionDescriptor);
        context.put("irrelevantKey2", workflow);
        when(workflow.isInitialAction(actionDescriptor)).thenReturn(false);

        // Invoke and check
        assertThat(condition.shouldDisplay(context), CoreMatchers.is(false));
    }

    @Test
    public void shouldNotDisplayIfContextIsEmpty() {
        // Invoke
        final boolean shouldDisplay = condition.shouldDisplay(emptyMap());

        // Check
        assertThat(shouldDisplay, is(false));
    }
}
