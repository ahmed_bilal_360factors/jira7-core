package com.atlassian.jira.dashboard;

import com.atlassian.jira.bc.whitelist.WhitelistService;
import com.atlassian.jira.util.velocity.SimpleVelocityRequestContext;
import com.atlassian.jira.util.velocity.VelocityRequestContext;
import com.atlassian.jira.util.velocity.VelocityRequestContextFactory;
import org.junit.Test;

import java.net.URI;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestJiraWhitelist {
    @Test
    public void testAllows() throws Exception {
        final WhitelistService mockWhitelistService = mock(WhitelistService.class);
        final VelocityRequestContextFactory mockVelocityRequestContextFactory = mock(VelocityRequestContextFactory.class);

        final VelocityRequestContext context = new SimpleVelocityRequestContext("/", "http://localhost:8090/", null, null);

        when(mockVelocityRequestContextFactory.getJiraVelocityRequestContext()).thenReturn(context);
        when(mockWhitelistService.isAllowed(URI.create("HTTP://www.Atlassian.com/gadgets/marketing.xml"))).thenReturn(true);
        when(mockWhitelistService.isAllowed(URI.create("HTTPs://www.Atlassian.com/gadgets/marketing.xml"))).thenReturn(true);
        when(mockWhitelistService.isAllowed(URI.create("localhost:8090/gadget/blah.xml"))).thenReturn(false);
        when(mockWhitelistService.isAllowed(URI.create("www.atlassian.com/gadget/blah.xml"))).thenReturn(false);
        when(mockWhitelistService.isAllowed(URI.create("http://www.google.com/"))).thenReturn(false);

        JiraWhitelist whitelist = new JiraWhitelist(mockWhitelistService, mockVelocityRequestContextFactory);
        assertTrue(whitelist.allows(URI.create("HTTP://www.Atlassian.com/gadgets/marketing.xml")));
        assertTrue(whitelist.allows(URI.create("HTTPs://www.Atlassian.com/gadgets/marketing.xml")));
        assertTrue(whitelist.allows(URI.create("http://localhost:8090/gadgets/marketing.xml")));
        assertFalse(whitelist.allows(URI.create("localhost:8090/gadget/blah.xml")));
        assertFalse(whitelist.allows(URI.create("www.atlassian.com/gadget/blah.xml")));
        assertFalse(whitelist.allows(URI.create("http://www.google.com/")));
    }
}
