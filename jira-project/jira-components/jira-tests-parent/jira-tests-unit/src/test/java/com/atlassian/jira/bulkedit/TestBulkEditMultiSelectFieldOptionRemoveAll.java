package com.atlassian.jira.bulkedit;

import com.atlassian.jira.issue.IssueFieldConstants;
import com.atlassian.jira.issue.fields.LongIdsValueHolder;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import org.hamcrest.Matchers;
import org.junit.Rule;
import org.junit.Test;

import static com.google.common.collect.ImmutableList.of;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

/**
 * @since v6.4
 */
public class TestBulkEditMultiSelectFieldOptionRemoveAll {
    private static final String LABEL1 = "Label1";
    private static final String LABEL2 = "Label2";
    private static final String LABEL3 = "Label3";
    private static final String LABEL4 = "Label4";
    private static final String VALUE1 = "Value1";
    private static final String VALUE2 = "Value2";
    private static final String VALUE3 = "Value3";
    private static final String COMPONENTS_PREFIX = LongIdsValueHolder.NEW_VALUE_PREFIX;
    private static final String VERSIONS_PREFIX = LongIdsValueHolder.NEW_VALUE_PREFIX;

    @Rule
    public BulkEditTestHelper helper = new BulkEditTestHelper() {
        @Override
        public BulkEditMultiSelectFieldOption instantiateFieldOption() {
            return new BulkEditMultiSelectFieldOptionRemoveAll();
        }
    };

    @Test
    public void testGetFieldValuesMapForComponents() throws Exception {
        LongIdsValueHolder componentsFromIssue = new LongIdsValueHolder(of(10001L, 10002L, 10003L));
        LongIdsValueHolder componentsFromBulkEdit = new LongIdsValueHolder(of(10003L, 10004L));

        helper.mockFieldValuesFromIssue(IssueFieldConstants.COMPONENTS, componentsFromIssue);
        helper.setValuesFromBulkEdit(IssueFieldConstants.COMPONENTS, componentsFromBulkEdit);

        assertThat(helper.getCollectionFromValuesMap(IssueFieldConstants.COMPONENTS), Matchers.emptyIterable());
    }

    @Test
    public void testGetFieldValuesMapForVersions() throws Exception {
        LongIdsValueHolder versionsFromIssue = new LongIdsValueHolder(of(10001L, 10002L, 10003L));
        LongIdsValueHolder versionsFromBulkEdit = new LongIdsValueHolder(of(10002L, 10003L));
        versionsFromBulkEdit.setValuesToAdd(ImmutableSet.of("Version1"));

        helper.mockFieldValuesFromIssue(IssueFieldConstants.FIX_FOR_VERSIONS, versionsFromIssue);
        helper.setValuesFromBulkEdit(IssueFieldConstants.FIX_FOR_VERSIONS, versionsFromBulkEdit);

        assertThat(helper.getCollectionFromValuesMap(IssueFieldConstants.FIX_FOR_VERSIONS), Matchers.emptyIterable());
    }

    @Test
    public void testGetFieldValuesMapForLabels() throws Exception {
        helper.mockFieldValuesFromIssue(IssueFieldConstants.LABELS, ImmutableSet.of(LABEL1, LABEL2, LABEL3, LABEL4));

        //it has to be mutable set - is modified under the hood ;/
        helper.setValuesFromBulkEdit(IssueFieldConstants.LABELS, Sets.newHashSet(LABEL2, LABEL3));

        assertThat(helper.getCollectionFromValuesMap(IssueFieldConstants.LABELS), Matchers.emptyIterable());
    }

    @Test
    public void testValidateOperationForComponents() throws Exception {
        LongIdsValueHolder componentsFromBulkEdit = new LongIdsValueHolder(ImmutableList.<Long>of());
        componentsFromBulkEdit.setValuesToAdd(ImmutableSet.of(COMPONENTS_PREFIX + VALUE1));

        helper.setValuesFromBulkEdit(IssueFieldConstants.COMPONENTS, componentsFromBulkEdit);

        assertTrue(helper.validate());
    }

    @Test
    public void testValidateOperationForVersions() throws Exception {
        helper.setValuesFromBulkEdit(IssueFieldConstants.AFFECTED_VERSIONS, new LongIdsValueHolder(of(10001L)));

        assertTrue(helper.validate());
    }

    @Test
    public void testValidateOperationForVersionsNoValueToAdd() throws Exception {
        helper.setValuesFromBulkEdit(IssueFieldConstants.AFFECTED_VERSIONS, new LongIdsValueHolder(ImmutableList.<Long>of()));

        assertTrue(helper.validate());
    }

    @Test
    public void testValidateOperationLabels() throws Exception {
        helper.setValuesFromBulkEdit(IssueFieldConstants.LABELS, ImmutableSet.of(LABEL1));

        assertTrue(helper.validate());
    }

    @Test
    public void testValidateOperationLabelsNoValueToAdd() throws Exception {
        helper.setValuesFromBulkEdit(IssueFieldConstants.LABELS, ImmutableSet.of());

        assertTrue(helper.validate());
    }

    @Test
    public void testGetFieldValuesToAddForComponents() throws Exception {
        LongIdsValueHolder componentsFromBulkEdit = new LongIdsValueHolder(of(10003L, 10004L));
        componentsFromBulkEdit.setValuesToAdd(ImmutableSet.of(COMPONENTS_PREFIX + VALUE1, COMPONENTS_PREFIX + VALUE2, COMPONENTS_PREFIX + VALUE3));

        helper.setValuesFromBulkEdit(IssueFieldConstants.COMPONENTS, componentsFromBulkEdit);

        assertThat(helper.getValuesToAdd(), Matchers.isEmptyString());
    }

    @Test
    public void testGetFieldValuesToAddForVersions() throws Exception {
        LongIdsValueHolder versionsFromBulkEdit = new LongIdsValueHolder(of(10003L, 10004L));
        versionsFromBulkEdit.setValuesToAdd(ImmutableSet.of(VERSIONS_PREFIX + VALUE1, VERSIONS_PREFIX + VALUE2, VERSIONS_PREFIX + VALUE3));

        helper.setValuesFromBulkEdit(IssueFieldConstants.FIX_FOR_VERSIONS, versionsFromBulkEdit);

        assertThat(helper.getValuesToAdd(), Matchers.isEmptyString());
    }

    @Test
    public void testGetFieldValuesToAddForLabels() throws Exception {
        helper.setValuesFromBulkEdit(IssueFieldConstants.LABELS, ImmutableSet.of(LABEL1, LABEL2, LABEL3));

        assertThat(helper.getValuesToAdd(), Matchers.isEmptyString());
    }
}
