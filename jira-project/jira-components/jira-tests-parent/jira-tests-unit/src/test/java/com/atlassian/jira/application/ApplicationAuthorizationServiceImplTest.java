package com.atlassian.jira.application;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.fugue.Option;
import com.atlassian.jira.license.JiraLicenseManager;
import com.atlassian.jira.license.LicenseDetails;
import com.atlassian.jira.license.LicenseMaintenancePredicate;
import com.atlassian.jira.license.LicensedApplications;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.MockGroup;
import com.google.common.collect.ImmutableSet;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.EnumSet;
import java.util.Set;

import static com.atlassian.application.api.ApplicationAccess.AccessError;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

public class ApplicationAuthorizationServiceImplTest {
    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    private static final MockApplicationUser TEST_USER = new MockApplicationUser("User");
    private static final ApplicationKey TEST_APPLICATION_KEY = ApplicationKey.valueOf("Role");
    private static final Group GROUP = new MockGroup("Group 1");

    @Mock
    private ApplicationRoleManager applicationRoleManager;
    @Mock
    private GroupManager groupManager;
    @Mock
    private LicenseDetails licenseDetails;
    @Mock
    private LicensedApplications licensedApplications;
    @Mock
    private LicenseMaintenancePredicate predicate;
    @Mock
    private JiraLicenseManager jiraLicenseManager;

    private ApplicationAuthorizationServiceImpl roleAuthorizationService;

    @Before
    public void setUp() {
        roleAuthorizationService = new ApplicationAuthorizationServiceImpl(applicationRoleManager, jiraLicenseManager,
                predicate);
        when(groupManager.getGroup(GROUP.getName())).thenReturn(GROUP);
    }

    @Test
    public void checkIsRoleInstalled() {
        when(applicationRoleManager.isRoleInstalledAndLicensed(TEST_APPLICATION_KEY)).thenReturn(true);

        assertThat(roleAuthorizationService.isApplicationInstalledAndLicensed(TEST_APPLICATION_KEY), Matchers.equalTo(true));
        assertThat(roleAuthorizationService.isApplicationInstalledAndLicensed(ApplicationKey.valueOf("random")), Matchers.equalTo(false));
    }

    @Test
    public void userCannotUseRoleWhenRoleInvalid() {
        when(jiraLicenseManager.getLicense(TEST_APPLICATION_KEY)).thenReturn(Option.some(licenseDetails));
        when(predicate.test(licenseDetails)).thenReturn(true);
        when(applicationRoleManager.getRole(TEST_APPLICATION_KEY))
                .thenReturn(Option.none(ApplicationRole.class));

        boolean authenticated = roleAuthorizationService.canUseApplication(TEST_USER, TEST_APPLICATION_KEY);
        assertThat(authenticated, Matchers.equalTo(false));
    }

    @Test
    public void userCannotUseRoleWhenNotInGroup() {
        when(jiraLicenseManager.getLicense(TEST_APPLICATION_KEY)).thenReturn(Option.some(licenseDetails));
        when(predicate.test(licenseDetails)).thenReturn(true);
        when(applicationRoleManager.userHasRole(TEST_USER, TEST_APPLICATION_KEY)).thenReturn(false);

        boolean authenticated = roleAuthorizationService.canUseApplication(TEST_USER, TEST_APPLICATION_KEY);
        assertThat(authenticated, Matchers.equalTo(false));
    }

    @Test
    public void userCanUseRoleWhenInGroup() {
        when(jiraLicenseManager.getLicense(TEST_APPLICATION_KEY)).thenReturn(Option.some(licenseDetails));
        when(predicate.test(licenseDetails)).thenReturn(true);
        when(applicationRoleManager.userHasRole(TEST_USER, TEST_APPLICATION_KEY)).thenReturn(true);

        boolean authenticated = roleAuthorizationService.canUseApplication(TEST_USER, TEST_APPLICATION_KEY);
        assertThat(authenticated, Matchers.equalTo(true));
    }

    @Test
    public void userCanUseRoleWhenAppNotInstalledButLicensed() {
        when(jiraLicenseManager.getLicense(TEST_APPLICATION_KEY)).thenReturn(Option.some(licenseDetails));
        when(predicate.test(licenseDetails)).thenReturn(true);
        when(applicationRoleManager.userHasRole(TEST_USER, TEST_APPLICATION_KEY)).thenReturn(true);

        boolean authenticated = roleAuthorizationService.canUseApplication(TEST_USER, TEST_APPLICATION_KEY);
        assertThat(authenticated, Matchers.equalTo(true));
    }

    @Test
    public void userCanUseCoreRoleWhenOtherAppLicensed() {
        when(jiraLicenseManager.getLicense(ApplicationKeys.CORE)).thenReturn(Option.none());
        when(jiraLicenseManager.getLicense(TEST_APPLICATION_KEY)).thenReturn(Option.some(licenseDetails));
        when(predicate.test(licenseDetails)).thenReturn(true);
        when(applicationRoleManager.userHasRole(TEST_USER, TEST_APPLICATION_KEY)).thenReturn(true);
        when(jiraLicenseManager.getAllLicensedApplicationKeys()).thenReturn(ImmutableSet.of(TEST_APPLICATION_KEY));

        boolean authenticated = roleAuthorizationService.canUseApplication(TEST_USER, ApplicationKeys.CORE);
        assertThat(authenticated, Matchers.equalTo(true));
    }

    @Test
    public void userCannotUseCoreRoleWhenAllAppsHaveAccessErrors() {
        when(jiraLicenseManager.getLicense(ApplicationKeys.CORE)).thenReturn(Option.none());
        when(jiraLicenseManager.getLicense(TEST_APPLICATION_KEY)).thenReturn(Option.none());
        when(jiraLicenseManager.getAllLicensedApplicationKeys()).thenReturn(ImmutableSet.of(TEST_APPLICATION_KEY));

        boolean authenticated = roleAuthorizationService.canUseApplication(TEST_USER, ApplicationKeys.CORE);
        assertThat(authenticated, Matchers.equalTo(false));
    }

    @Test
    public void isLicensedShouldDelegateDirectlyToJiraLicenseManager() {
        roleAuthorizationService.isApplicationInstalledAndLicensed(TEST_APPLICATION_KEY);

        verify(applicationRoleManager).isRoleInstalledAndLicensed(TEST_APPLICATION_KEY);
        verifyNoMoreInteractions(groupManager, applicationRoleManager, licenseDetails, licensedApplications);
    }

    @Test
    public void countActiveUsersShallDelegateToRoleManager() {
        when(applicationRoleManager.getUserCount(TEST_APPLICATION_KEY)).thenReturn(20);

        assertThat("There shall be 20 users for the test application role",
                roleAuthorizationService.getUserCount(TEST_APPLICATION_KEY), Matchers.equalTo(20));
    }

    @Test
    public void isExceededDelegatesToManager() {
        when(applicationRoleManager.isRoleLimitExceeded(TEST_APPLICATION_KEY)).thenReturn(true);
        assertThat(roleAuthorizationService.isExceeded(TEST_APPLICATION_KEY), Matchers.equalTo(true));
    }

    @Test
    public void unlicensedAppsShouldGiveUnlicensedError() {
        when(jiraLicenseManager.getLicense(TEST_APPLICATION_KEY)).thenReturn(Option.none());
        Set<AccessError> expectedErrors = EnumSet.of(AccessError.UNLICENSED);

        assertThat("Unlicensed app should return unlicensed error",
                roleAuthorizationService.getLicensingAccessErrors(TEST_APPLICATION_KEY), Matchers.equalTo(expectedErrors));
    }

    @Test
    public void cannotUseUnlicensedApps() {
        when(jiraLicenseManager.getLicense(TEST_APPLICATION_KEY)).thenReturn(Option.none(LicenseDetails.class));
        assertThat("Should not be able to use unlicensed apps",
                roleAuthorizationService.hasNoLicensingAccessErrors(TEST_APPLICATION_KEY), Matchers.equalTo(false));
    }

    @Test
    public void expiredAppsShouldGiveExpiredAccessError() {
        when(jiraLicenseManager.getLicense(TEST_APPLICATION_KEY)).thenReturn(Option.some(licenseDetails));
        when(predicate.test(licenseDetails)).thenReturn(true);
        when(licenseDetails.isExpired()).thenReturn(true);
        Set<AccessError> expectedError = EnumSet.of(AccessError.EXPIRED);

        assertThat("Expired app should return an expired AccessError",
                roleAuthorizationService.getLicensingAccessErrors(TEST_APPLICATION_KEY), Matchers.equalTo(expectedError));
    }

    @Test
    public void versionMismatchedAppShouldGiveVersionMismatchAccessError() {
        when(jiraLicenseManager.getLicense(TEST_APPLICATION_KEY)).thenReturn(Option.some(licenseDetails));
        when(predicate.test(licenseDetails)).thenReturn(false);
        Set<AccessError> expectedError = EnumSet.of(AccessError.VERSION_MISMATCH);

        assertThat("Out of maintenance app should return version mismatch AccessError",
                roleAuthorizationService.getLicensingAccessErrors(TEST_APPLICATION_KEY), Matchers.equalTo(expectedError));
    }

    @Test
    public void userExceededAppShouldGiveUsersExceededAccessError() {
        when(jiraLicenseManager.getLicense(TEST_APPLICATION_KEY)).thenReturn(Option.some(licenseDetails));
        when(predicate.test(licenseDetails)).thenReturn(true);
        when(applicationRoleManager.isRoleLimitExceeded(TEST_APPLICATION_KEY)).thenReturn(true);
        Set<AccessError> expectedError = EnumSet.of(AccessError.USERS_EXCEEDED);

        assertThat("App with users exceeded should return a users exceeded AccessError",
                roleAuthorizationService.getLicensingAccessErrors(TEST_APPLICATION_KEY), Matchers.equalTo(expectedError));
    }

    @Test
    public void usersWhoAreNotMembersOfAnyGroupsShouldGiveAccessError_checkingDefinedNonCoreRole() {
        when(jiraLicenseManager.getLicense(TEST_APPLICATION_KEY)).thenReturn(Option.some(licenseDetails));
        when(predicate.test(licenseDetails)).thenReturn(true);
        when(applicationRoleManager.userHasRole(TEST_USER, TEST_APPLICATION_KEY)).thenReturn(false);

        Set<AccessError> expectedError = EnumSet.of(AccessError.NO_ACCESS);

        assertThat("Users that are not members of any role's groups do not have access to app",
                roleAuthorizationService.getAccessErrors(TEST_USER, TEST_APPLICATION_KEY), Matchers.equalTo(expectedError));
    }

    @Test
    public void usersWhoAreNotMembersOfAnyGroupsShouldGiveAccessError_checkingCore() {
        when(jiraLicenseManager.getLicense(ApplicationKeys.CORE)).thenReturn(Option.some(licenseDetails));
        when(predicate.test(licenseDetails)).thenReturn(true);
        when(applicationRoleManager.userHasRole(TEST_USER, ApplicationKeys.CORE)).thenReturn(false);

        when(jiraLicenseManager.getLicense(TEST_APPLICATION_KEY)).thenReturn(Option.some(licenseDetails));
        when(applicationRoleManager.userHasRole(TEST_USER, TEST_APPLICATION_KEY)).thenReturn(false);
        when(jiraLicenseManager.getAllLicensedApplicationKeys()).thenReturn(ImmutableSet.of(TEST_APPLICATION_KEY));

        Set<AccessError> expectedError = EnumSet.of(AccessError.NO_ACCESS);

        assertThat("Users that are not members of any role's groups do not have access to core app",
                roleAuthorizationService.getAccessErrors(TEST_USER, ApplicationKeys.CORE), Matchers.equalTo(expectedError));
    }

    @Test
    public void shouldGetMultipleAccessErrorsWhenExpiredVersionMismatchedUsersExceededAndAnonUsers() {
        when(jiraLicenseManager.getLicense(TEST_APPLICATION_KEY)).thenReturn(Option.some(licenseDetails));
        when(licenseDetails.isExpired()).thenReturn(true);
        when(predicate.test(licenseDetails)).thenReturn(false);
        when(applicationRoleManager.isRoleLimitExceeded(TEST_APPLICATION_KEY)).thenReturn(true);
        ApplicationUser anonUser = null;

        Set<AccessError> expectedError = EnumSet.of(AccessError.EXPIRED,
                AccessError.VERSION_MISMATCH, AccessError.USERS_EXCEEDED, AccessError.NO_ACCESS);

        assertThat("Should get multiple access errors when expired, version mismatched, users exceeded, and anonymous user",
                roleAuthorizationService.getAccessErrors(anonUser, TEST_APPLICATION_KEY), Matchers.equalTo(expectedError));
    }

    @Test
    public void shouldNotFindAccessErrorsWhenThereAreNone() {
        when(jiraLicenseManager.getLicense(ApplicationKeys.CORE)).thenReturn(Option.some(licenseDetails));
        when(licenseDetails.isExpired()).thenReturn(false);
        when(predicate.test(licenseDetails)).thenReturn(true);
        when(applicationRoleManager.isRoleLimitExceeded(ApplicationKeys.CORE)).thenReturn(false);
        when(applicationRoleManager.userHasRole(TEST_USER, ApplicationKeys.CORE)).thenReturn(true);

        Set<AccessError> expectedError = EnumSet.noneOf(AccessError.class);

        assertThat("Should not find any access errors when there are none",
                roleAuthorizationService.getAccessErrors(TEST_USER, ApplicationKeys.CORE), Matchers.equalTo(expectedError));
    }

    @Test
    public void shouldGiveUnlicensedErrorWhenOnlyUnlicensedCorePresent() {
        when(jiraLicenseManager.getLicense(ApplicationKeys.CORE)).thenReturn(Option.none(LicenseDetails.class));
        Set<AccessError> expectedErrors = EnumSet.of(AccessError.UNLICENSED);

        assertThat("Unlicensed core (only) should return unlicensed error",
                roleAuthorizationService.getLicensingAccessErrors(ApplicationKeys.CORE), Matchers.equalTo(expectedErrors));
    }

    @Test
    public void unlicensedCoreWithLicensedAppShouldReturnNoErrors() {
        when(jiraLicenseManager.getLicense(ApplicationKeys.CORE)).thenReturn(Option.none(LicenseDetails.class));
        when(jiraLicenseManager.getLicense(TEST_APPLICATION_KEY)).thenReturn(Option.some(licenseDetails));
        when(licenseDetails.isExpired()).thenReturn(false);
        when(predicate.test(licenseDetails)).thenReturn(true);
        when(applicationRoleManager.isRoleLimitExceeded(TEST_APPLICATION_KEY)).thenReturn(false);
        when(applicationRoleManager.userHasRole(TEST_USER, TEST_APPLICATION_KEY)).thenReturn(true);
        when(jiraLicenseManager.getAllLicensedApplicationKeys()).thenReturn(ImmutableSet.of(TEST_APPLICATION_KEY));

        Set<AccessError> expectedError = EnumSet.noneOf(AccessError.class);

        assertThat("Should not find any access errors when checking core but there is a licensed app present",
                roleAuthorizationService.getAccessErrors(TEST_USER, ApplicationKeys.CORE), Matchers.equalTo(expectedError));
    }

    @Test
    public void unlicensedCoreWithAnotherAppWithLicenseErrorsShouldReturnUnlicensed() {
        when(jiraLicenseManager.getLicense(ApplicationKeys.CORE)).thenReturn(Option.none());
        when(jiraLicenseManager.getLicense(TEST_APPLICATION_KEY)).thenReturn(Option.some(licenseDetails));
        when(licenseDetails.isExpired()).thenReturn(true);

        Set<AccessError> expectedErrors = EnumSet.of(AccessError.UNLICENSED);

        assertThat("Should get unlicensed error from an unlicensed core app and another app with license errors",
                roleAuthorizationService.getLicensingAccessErrors(ApplicationKeys.CORE), Matchers.equalTo(expectedErrors));
    }

    @Test
    public void unlicensedCoreWithAnotherAppWithUserAccessErrorsShouldReturnUnlicensedError() {
        when(jiraLicenseManager.getLicense(ApplicationKeys.CORE)).thenReturn(Option.none());
        when(applicationRoleManager.userHasRole(TEST_USER, ApplicationKeys.CORE)).thenReturn(true);

        when(jiraLicenseManager.getAllLicensedApplicationKeys()).thenReturn(ImmutableSet.of(TEST_APPLICATION_KEY));
        when(jiraLicenseManager.getLicense(TEST_APPLICATION_KEY)).thenReturn(Option.some(licenseDetails));
        when(licenseDetails.isExpired()).thenReturn(false);
        when(predicate.test(licenseDetails)).thenReturn(true);
        when(applicationRoleManager.isRoleLimitExceeded(TEST_APPLICATION_KEY)).thenReturn(false);
        when(applicationRoleManager.userHasRole(TEST_USER, TEST_APPLICATION_KEY)).thenReturn(false);

        Set<AccessError> expectedErrors = EnumSet.of(AccessError.UNLICENSED);

        assertThat("Should get unlicensed error from an unlicensed core app and another app with user access errors",
                roleAuthorizationService.getAccessErrors(TEST_USER, ApplicationKeys.CORE), Matchers.equalTo(expectedErrors));
    }
}
