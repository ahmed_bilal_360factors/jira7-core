package com.atlassian.jira.plugin.util.orderings;

import com.atlassian.jira.mock.plugin.MockPlugin;
import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginInformation;
import com.atlassian.plugin.metadata.PluginMetadataManager;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Responsible for holding tests for {@link ByOriginModuleDescriptorOrdering}
 *
 * @since v4.4
 */
public class TestByOriginModuleDescriptorOrdering {
    PluginMetadataManager mockPluginMetadataManager;

    @Before
    public void setUpMockComponents() {
        mockPluginMetadataManager = mock(PluginMetadataManager.class);
    }

    @Test
    public void descriptorsComingFromASystemProvidedPluginShouldBeComparedAsEqual() {
        final ModuleDescriptor systemProvidedModuleDescriptor1 = mock(ModuleDescriptor.class);
        final ModuleDescriptor systemProvidedModuleDescriptor2 = mock(ModuleDescriptor.class);

        when(systemProvidedModuleDescriptor1.getPlugin()).thenReturn(createEmptyMockPlugin());
        when(systemProvidedModuleDescriptor2.getPlugin()).thenReturn(createEmptyMockPlugin());

        expectAnyPluginToBeSystemProvided();

        final ByOriginModuleDescriptorOrdering byOriginModuleDescriptorOrdering =
                new ByOriginModuleDescriptorOrdering(mockPluginMetadataManager);

        assertTrue(byOriginModuleDescriptorOrdering.compare(systemProvidedModuleDescriptor1, systemProvidedModuleDescriptor2) == 0);
        assertTrue(byOriginModuleDescriptorOrdering.compare(systemProvidedModuleDescriptor2, systemProvidedModuleDescriptor1) == 0);
    }

    @Test
    public void descriptorsComingFromAUserInstalledPluginShouldBeComparedAsEqual() {
        final ModuleDescriptor userInstalledModuleDescriptor1 = mock(ModuleDescriptor.class);
        final ModuleDescriptor userInstalledModuleDescriptor2 = mock(ModuleDescriptor.class);

        when(userInstalledModuleDescriptor1.getPlugin()).thenReturn(createEmptyMockPlugin());
        when(userInstalledModuleDescriptor2.getPlugin()).thenReturn(createEmptyMockPlugin());

        expectAnyPluginToBeUserInstalled();

        final ByOriginModuleDescriptorOrdering byOriginModuleDescriptorOrdering =
                new ByOriginModuleDescriptorOrdering(mockPluginMetadataManager);

        assertTrue(byOriginModuleDescriptorOrdering.compare(userInstalledModuleDescriptor1, userInstalledModuleDescriptor2) == 0);
        assertTrue(byOriginModuleDescriptorOrdering.compare(userInstalledModuleDescriptor2, userInstalledModuleDescriptor1) == 0);
    }

    @Test
    public void descriptorsComingFromAUserInstalledPluginShouldBeGreaterThanDescriptorsComingFromASystemProvidedPlugin() {
        final ModuleDescriptor userInstalledModuleDescriptor = mock(ModuleDescriptor.class);
        final Plugin userInstalledPlugin =
                new MockPlugin("User Installed Plugin", "user-installed-plugin", new PluginInformation());
        when(userInstalledModuleDescriptor.getPlugin()).thenReturn(userInstalledPlugin);

        final ModuleDescriptor systemProvidedModuleDescriptor = mock(ModuleDescriptor.class);
        final Plugin systemProvidedPlugin =
                new MockPlugin("System Provided Plugin", "system-provided-plugin", new PluginInformation());
        when(systemProvidedModuleDescriptor.getPlugin()).thenReturn(systemProvidedPlugin);

        when(mockPluginMetadataManager.isUserInstalled(userInstalledPlugin)).thenReturn(true);
        when(mockPluginMetadataManager.isUserInstalled(systemProvidedPlugin)).thenReturn(false);

        final ByOriginModuleDescriptorOrdering byOriginModuleDescriptorOrdering =
                new ByOriginModuleDescriptorOrdering(mockPluginMetadataManager);

        assertTrue(byOriginModuleDescriptorOrdering.compare(userInstalledModuleDescriptor, systemProvidedModuleDescriptor) >= 1);
        assertTrue(byOriginModuleDescriptorOrdering.compare(systemProvidedModuleDescriptor, userInstalledModuleDescriptor) <= -1);
    }

    private void expectAnyPluginToBeSystemProvided() {
        when(mockPluginMetadataManager.isUserInstalled(any())).thenReturn(false);
    }

    private void expectAnyPluginToBeUserInstalled() {
        when(mockPluginMetadataManager.isUserInstalled(any())).thenReturn(true);
    }

    private Plugin createEmptyMockPlugin() {
        return new MockPlugin("Empty Mock Plugin", "mock-plugin", new PluginInformation());
    }
}
