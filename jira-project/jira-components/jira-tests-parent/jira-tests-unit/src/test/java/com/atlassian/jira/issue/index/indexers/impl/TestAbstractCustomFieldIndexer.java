package com.atlassian.jira.issue.index.indexers.impl;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.web.FieldVisibilityManager;
import org.apache.lucene.document.Document;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.concurrent.atomic.AtomicBoolean;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestAbstractCustomFieldIndexer {
    @Rule
    public MockitoRule mockito = MockitoJUnit.rule();
    @Mock
    private CustomField customField;
    @Mock
    private FieldVisibilityManager fieldVisibilityManager;

    @Test
    public void testIsRelevantForIssue() throws Exception {
        when(customField.isRelevantForIssueContext(null)).thenReturn(true);

        final TestCustomFieldIndexer indexer = new TestCustomFieldIndexer(fieldVisibilityManager, customField);
        assertTrue(indexer.isRelevantForIssue(null));
    }

    @Test
    public void testIsFieldVisibleAndInScope() throws Exception {
        when(customField.getId()).thenReturn("blah");
        when(fieldVisibilityManager.isFieldVisible("blah", null)).thenReturn(true);

        final AtomicBoolean called = new AtomicBoolean(false);
        final TestCustomFieldIndexer indexer = new TestCustomFieldIndexer(fieldVisibilityManager, customField) {
            @Override
            protected boolean isRelevantForIssue(final Issue issue) {
                called.set(true);
                return true;
            }
        };
        assertTrue(indexer.isFieldVisibleAndInScope(null));
        assertTrue(called.get());
    }

    @Test
    public void testIsFieldVisibleAndInScopeNotVisible() throws Exception {
        when(customField.getId()).thenReturn("blah");
        when(fieldVisibilityManager.isFieldVisible("blah", null)).thenReturn(false);

        final AtomicBoolean called = new AtomicBoolean(false);
        final TestCustomFieldIndexer indexer = new TestCustomFieldIndexer(fieldVisibilityManager, customField) {
            @Override
            protected boolean isRelevantForIssue(final Issue issue) {
                called.set(true);
                return true;
            }
        };
        assertFalse(indexer.isFieldVisibleAndInScope(null));
        // not called because first check is false
        assertFalse(called.get());
    }

    @Test
    public void testGetId() throws Exception {
        when(customField.getId()).thenReturn("blah");

        final TestCustomFieldIndexer indexer = new TestCustomFieldIndexer(fieldVisibilityManager, customField);
        assertEquals("blah", indexer.getId());
    }

    @Test
    public void testDocumentFieldId() throws Exception {
        when(customField.getId()).thenReturn("blah");

        final TestCustomFieldIndexer indexer = new TestCustomFieldIndexer(fieldVisibilityManager, customField);
        assertEquals("blah", indexer.getDocumentFieldId());
    }

    @Test
    public void testAddIndexSearchable() throws Exception {
        when(customField.getId()).thenReturn("blah");
        when(fieldVisibilityManager.isFieldVisible("blah", null)).thenReturn(true);

        final TestCustomFieldIndexer indexer = new TestCustomFieldIndexer(fieldVisibilityManager, customField) {
            @Override
            protected boolean isRelevantForIssue(final Issue issue) {
                return true;
            }
        };

        indexer.addIndex(null, null);
        assertTrue(indexer.addSearchableCalled);
    }

    @Test
    public void testAddIndexNotSearchable() throws Exception {
        when(customField.getId()).thenReturn("blah");
        when(fieldVisibilityManager.isFieldVisible("blah", null)).thenReturn(false);

        final TestCustomFieldIndexer indexer = new TestCustomFieldIndexer(fieldVisibilityManager, customField) {
            @Override
            protected boolean isRelevantForIssue(final Issue issue) {
                return true;
            }
        };

        indexer.addIndex(null, null);
        assertTrue(indexer.addNotSearchableCalled);
    }

    static class TestCustomFieldIndexer extends AbstractCustomFieldIndexer {
        public boolean addSearchableCalled = false;
        public boolean addNotSearchableCalled = false;

        protected TestCustomFieldIndexer(final FieldVisibilityManager fieldVisibilityManager, final CustomField customField) {
            super(fieldVisibilityManager, customField);
        }

        public void addDocumentFieldsSearchable(final Document doc, final Issue issue) {
            this.addSearchableCalled = true;
        }

        public void addDocumentFieldsNotSearchable(final Document doc, final Issue issue) {
            this.addNotSearchableCalled = true;
        }
    }
}
