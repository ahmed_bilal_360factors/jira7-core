package com.atlassian.core.util;

import com.atlassian.inception.JiraUpgradeHelper;
import org.junit.Test;

import java.io.IOException;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

/**
 * This test verifies the expectation the upgrader(installer) has on the format of setenv.sh and setenv.bat. When
 * installer permorms the upgrade it transferes the max/min/pemgen memory and other settings to the upgraded
 * installation. It relies on the paricular format of the file to extract those values so if the format is changed the
 * upgrader will have to change. IF THIS TEST BREAKES PLEASE VERIFY THAT UPGRADER STILL WORKS WHEN FIXING IT.
 *
 * @since v4.4
 */
public class TestDistributionEnvForUpgrade {
    private static final JiraUpgradeHelper WINDOWS = new TestJiraUpgradeHelper(true);
    private static final JiraUpgradeHelper NON_WINDOWS = new TestJiraUpgradeHelper(false);

    @Test
    public void minMemoryOnAllSystemsIs384m() throws IOException {
        testAllSystems(upgradeHelper -> assertThat(upgradeHelper.getJvmMinMemory(), equalTo("384m")));
    }

    @Test
    public void maxMemoryOnAllSystemsIs768m() throws IOException {
        testAllSystems(upgradeHelper -> assertThat(upgradeHelper.getJvmMaxMemory(), equalTo("768m")));
    }

    @Test
    public void supportArgsOnAllSystemsIsEmpty() throws IOException {
        testAllSystems(upgradeHelper -> assertThat(upgradeHelper.getJvmSupportArgs(), equalTo("")));
    }

    private void testAllSystems(TestAction test) {
        test.runWithUpgradeHelper(WINDOWS);
        test.runWithUpgradeHelper(NON_WINDOWS);
    }

    private static class TestJiraUpgradeHelper extends JiraUpgradeHelper {
        private final boolean windows;

        public TestJiraUpgradeHelper(boolean windows) {
            super("../../../jira-distribution/jira-standalone-distribution-base/src/main/tomcat/");
            this.windows = windows;
        }

        @Override
        public boolean isWindows() {
            return windows;
        }
    }

    private interface TestAction {
        void runWithUpgradeHelper(JiraUpgradeHelper upgradeHelper);
    }
}
