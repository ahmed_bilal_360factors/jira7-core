package com.atlassian.jira.security;

import com.atlassian.collectors.CollectorsUtil;
import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.fugue.Option;
import com.atlassian.jira.application.ApplicationRoleManager;
import com.atlassian.jira.exception.RemoveException;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.matchers.OptionMatchers;
import com.atlassian.jira.mock.issue.MockIssue;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.permission.MockProjectPermission;
import com.atlassian.jira.permission.ProjectPermission;
import com.atlassian.jira.permission.ProjectPermissionCategory;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectCategory;
import com.atlassian.jira.project.ProjectCategoryImpl;
import com.atlassian.jira.security.plugin.ProjectPermissionKey;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.MockGroup;
import com.google.common.collect.ImmutableList;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.mockito.Mock;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;

import static com.atlassian.jira.permission.ProjectPermissions.ADD_COMMENTS;
import static com.atlassian.jira.permission.ProjectPermissions.ADMINISTER_PROJECTS;
import static com.atlassian.jira.permission.ProjectPermissions.ASSIGNABLE_USER;
import static com.atlassian.jira.permission.ProjectPermissions.ASSIGN_ISSUES;
import static com.atlassian.jira.permission.ProjectPermissions.BROWSE_PROJECTS;
import static com.atlassian.jira.permission.ProjectPermissions.CLOSE_ISSUES;
import static com.atlassian.jira.permission.ProjectPermissions.CREATE_ATTACHMENTS;
import static com.atlassian.jira.permission.ProjectPermissions.CREATE_ISSUES;
import static com.atlassian.jira.permission.ProjectPermissions.DELETE_ALL_ATTACHMENTS;
import static com.atlassian.jira.permission.ProjectPermissions.DELETE_ALL_COMMENTS;
import static com.atlassian.jira.permission.ProjectPermissions.DELETE_ALL_WORKLOGS;
import static com.atlassian.jira.permission.ProjectPermissions.DELETE_ISSUES;
import static com.atlassian.jira.permission.ProjectPermissions.DELETE_OWN_ATTACHMENTS;
import static com.atlassian.jira.permission.ProjectPermissions.DELETE_OWN_COMMENTS;
import static com.atlassian.jira.permission.ProjectPermissions.DELETE_OWN_WORKLOGS;
import static com.atlassian.jira.permission.ProjectPermissions.EDIT_ALL_COMMENTS;
import static com.atlassian.jira.permission.ProjectPermissions.EDIT_ALL_WORKLOGS;
import static com.atlassian.jira.permission.ProjectPermissions.EDIT_ISSUES;
import static com.atlassian.jira.permission.ProjectPermissions.EDIT_OWN_COMMENTS;
import static com.atlassian.jira.permission.ProjectPermissions.EDIT_OWN_WORKLOGS;
import static com.atlassian.jira.permission.ProjectPermissions.LINK_ISSUES;
import static com.atlassian.jira.permission.ProjectPermissions.MANAGE_WATCHERS;
import static com.atlassian.jira.permission.ProjectPermissions.MODIFY_REPORTER;
import static com.atlassian.jira.permission.ProjectPermissions.MOVE_ISSUES;
import static com.atlassian.jira.permission.ProjectPermissions.RESOLVE_ISSUES;
import static com.atlassian.jira.permission.ProjectPermissions.SCHEDULE_ISSUES;
import static com.atlassian.jira.permission.ProjectPermissions.SET_ISSUE_SECURITY;
import static com.atlassian.jira.permission.ProjectPermissions.TRANSITION_ISSUES;
import static com.atlassian.jira.permission.ProjectPermissions.VIEW_DEV_TOOLS;
import static com.atlassian.jira.permission.ProjectPermissions.VIEW_READONLY_WORKFLOW;
import static com.atlassian.jira.permission.ProjectPermissions.VIEW_VOTERS_AND_WATCHERS;
import static com.atlassian.jira.permission.ProjectPermissions.WORK_ON_ISSUES;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

/**
 * @since v7.0
 */
public class ApplicationRequiredPermissionManagerTest {
    private static List<ProjectPermissionKey> SYSTEM_PERMISSIONS = ImmutableList.of(ADMINISTER_PROJECTS,
            BROWSE_PROJECTS,
            VIEW_DEV_TOOLS,
            VIEW_READONLY_WORKFLOW,
            CREATE_ISSUES,
            EDIT_ISSUES,
            TRANSITION_ISSUES,
            SCHEDULE_ISSUES,
            MOVE_ISSUES,
            ASSIGN_ISSUES,
            ASSIGNABLE_USER,
            RESOLVE_ISSUES,
            CLOSE_ISSUES,
            MODIFY_REPORTER,
            DELETE_ISSUES,
            LINK_ISSUES,
            SET_ISSUE_SECURITY,
            VIEW_VOTERS_AND_WATCHERS,
            MANAGE_WATCHERS,
            ADD_COMMENTS,
            EDIT_ALL_COMMENTS,
            EDIT_OWN_COMMENTS,
            DELETE_ALL_COMMENTS,
            DELETE_OWN_COMMENTS,
            CREATE_ATTACHMENTS,
            DELETE_ALL_ATTACHMENTS,
            DELETE_OWN_ATTACHMENTS,
            WORK_ON_ISSUES,
            EDIT_OWN_WORKLOGS,
            EDIT_ALL_WORKLOGS,
            DELETE_OWN_WORKLOGS,
            DELETE_ALL_WORKLOGS);

    @Rule
    public RuleChain rule = MockitoMocksInContainer.forTest(this);

    @Mock
    @AvailableInContainer
    private ApplicationRoleManager applicationRoleManager;
    @Mock
    @AvailableInContainer
    private GlobalPermissionManager globalPermissions;

    @Mock
    private PermissionManager delegatePermissionManager;

    private PermissionManager permissionManager;
    private final MockApplicationUser adminWithoutApplication = new MockApplicationUser("adminWithoutApplication");
    private final MockApplicationUser adminWithApplicationAndPermission = new MockApplicationUser("adminWithApplication");
    private final MockApplicationUser adminWithApplicationAndNoPermission = new MockApplicationUser("adminWithNoPermission");
    private final MockApplicationUser anonymousUser = null;
    private final MockApplicationUser userWithoutApplicationRole = new MockApplicationUser("userWithoutApplicationRole");
    private final MockProject project = new MockProject(10101L, "ProjectKey", "TestProject");
    private final MockIssue issue = new MockIssue();
    private final List<Project> projects = ImmutableList.of(project);
    private final ProjectCategory category = new ProjectCategoryImpl(10L, "Category", "TestCategory");

    @Before
    public void setup() {
        permissionManager = new ApplicationRequiredPermissionManager(delegatePermissionManager);

        when(applicationRoleManager.hasAnyRole(adminWithoutApplication)).thenReturn(false);
        when(applicationRoleManager.hasAnyRole(adminWithApplicationAndNoPermission)).thenReturn(true);
        when(applicationRoleManager.hasAnyRole(adminWithApplicationAndPermission)).thenReturn(true);
        when(applicationRoleManager.hasAnyRole(userWithoutApplicationRole)).thenReturn(false);

        when(globalPermissions.hasPermission(GlobalPermissionKey.ADMINISTER, adminWithoutApplication)).thenReturn(true);
        when(globalPermissions.hasPermission(GlobalPermissionKey.ADMINISTER, adminWithApplicationAndNoPermission)).thenReturn(true);
        when(globalPermissions.hasPermission(GlobalPermissionKey.ADMINISTER, adminWithApplicationAndPermission)).thenReturn(true);
        when(globalPermissions.hasPermission(GlobalPermissionKey.ADMINISTER, userWithoutApplicationRole)).thenReturn(false);

        when(delegatePermissionManager.hasProjectWidePermission(any(), any(), any()))
                .thenReturn(ProjectWidePermission.NO_ISSUES);

        giveAllAccessTo(adminWithoutApplication);
        giveAllAccessTo(adminWithApplicationAndPermission);
        giveAllAccessTo(anonymousUser);
        giveAllAccessTo(userWithoutApplicationRole);
    }

    @Test
    public void getAllProjectPermissionsDelegates() {
        //given
        final List<ProjectPermission> expected = permissions("one", "two", "three");
        when(delegatePermissionManager.getAllProjectPermissions()).thenReturn(expected);

        //when
        final Collection<ProjectPermission> actual = permissionManager.getAllProjectPermissions();

        //then
        assertThat(actual, Matchers.equalTo(expected));
    }

    @Test
    public void getProjectPermissionsDelegates() {
        //given
        final List<ProjectPermission> expected = permissions("one", "two", "three");
        when(delegatePermissionManager.getProjectPermissions(ProjectPermissionCategory.ISSUES))
                .thenReturn(expected);

        //when
        final Collection<ProjectPermission> actual = permissionManager.getProjectPermissions(ProjectPermissionCategory.ISSUES);

        //then
        assertThat(actual, Matchers.equalTo(expected));
    }

    @Test
    public void getProjectPermissionDelegates() {
        //given
        final MockProjectPermission permission = permission("one");
        when(delegatePermissionManager.getProjectPermission(ADD_COMMENTS)).thenReturn(Option.some(permission));

        //when
        final Option<ProjectPermission> actual = permissionManager.getProjectPermission(ADD_COMMENTS);

        //then
        assertThat(actual, OptionMatchers.some(permission));
    }

    @Test
    public void hasGlobalPermissionDelegates() {
        //given.
        final int id = Permissions.ADMINISTER;
        when(permissionManager.hasPermission(id, adminWithoutApplication)).thenReturn(true);

        //when
        boolean hasPermission = permissionManager.hasPermission(id, adminWithoutApplication);

        //then
        assertThat(hasPermission, is(true));
    }

    @Test
    public void adminWithoutApplicationNeverHasProjectPermission() {
        assertPermissions(adminWithoutApplication, false);

        //no call should have been delegated.
        verifyNoMoreInteractions(delegatePermissionManager);
    }

    @Test
    public void anonymousWithoutApplicationDelegatesToPermissionManager() {
        assertPermissions(anonymousUser, true);
    }

    @Test
    public void usersWithoutApplicationDelegatesToPermissionManager() {
        assertPermissions(userWithoutApplicationRole, true);
    }

    @Test
    public void adminWithApplicationAndNoPermissionHasNoProjectPermission() {
        assertPermissions(adminWithApplicationAndNoPermission, false);
    }

    @Test
    public void adminWithApplicationAndPermissionHasProjectPermission() {
        assertPermissions(adminWithApplicationAndPermission, true);
    }

    @Test
    public void adminWithoutApplicationHasNoProjects() {
        assertProjects(adminWithoutApplication, false);

        //no call should have been delegated.
        verifyNoMoreInteractions(delegatePermissionManager);
    }

    @Test
    public void anonymousWithoutApplicationDelegatesProjectPermissions() {
        assertProjects(anonymousUser, true);
    }

    @Test
    public void usersWithoutApplicationDelegatesProjectPermissions() {
        assertProjects(userWithoutApplicationRole, true);
    }

    @Test
    public void adminWithApplicationAndPermissionHasNoProjects() {
        assertProjects(adminWithApplicationAndNoPermission, false);
    }

    @Test
    public void adminWithApplicationAndPermissionHasProjects() {
        assertProjects(adminWithApplicationAndPermission, true);
    }

    @Test
    public void removeGroupPermissionDelegates() throws RemoveException {
        //given
        String group = "GroupName";

        //when
        permissionManager.removeGroupPermissions(group);

        //then
        verify(delegatePermissionManager).removeGroupPermissions(group);
    }

    @Test
    public void removeUserPermissions() throws RemoveException {
        //given
        final MockApplicationUser user = new MockApplicationUser("jack");

        //when
        permissionManager.removeUserPermissions(user);

        //then
        verify(delegatePermissionManager).removeUserPermissions(user);
    }

    @Test
    public void getAllGroupsDelegates() {
        //given
        final List<Group> groups = groups("one", "two", "three");
        when(delegatePermissionManager.getAllGroups(Permissions.ADMINISTER, project)).thenReturn(groups);

        //when
        final Collection<Group> allGroups = permissionManager.getAllGroups(Permissions.ADMINISTER, project);

        //then
        assertThat(allGroups, equalTo(allGroups));
    }

    private void assertProjects(final ApplicationUser user, final boolean hasProject) {
        final List<Project> projects = hasProject ? this.projects : ImmutableList.of();

        assertForEachPermissionKey(key -> permissionManager.hasProjects(key, user), equalTo(hasProject));
        assertForEachPermissionId(id -> permissionManager.hasProjects(id, user), equalTo(hasProject));

        assertForEachPermissionKey(key -> permissionManager.getProjects(key, user), equalTo(projects));
        assertForEachPermissionId(id -> permissionManager.getProjects(id, user), equalTo(projects));

        assertForEachPermissionKey(key -> permissionManager.getProjects(key, user, category), equalTo(projects));
        assertForEachPermissionId(id -> permissionManager.getProjects(id, user, category), equalTo(projects));
    }

    private void assertPermissions(final ApplicationUser user, final boolean allowed) {
        assertForEachPermissionKey(key -> permissionManager.hasPermission(key, issue, user), is(allowed));
        assertForEachPermissionId(id -> permissionManager.hasPermission(id, issue, user), is(allowed));

        final ProjectWidePermission permission = allowed ? ProjectWidePermission.ALL_ISSUES : ProjectWidePermission.NO_ISSUES;
        assertForEachPermissionKey(key -> permissionManager.hasProjectWidePermission(key, project, user), is(permission));

        assertForEachPermissionKey(key -> permissionManager.hasPermission(key, project, user), is(allowed));
        assertForEachPermissionId(id -> permissionManager.hasPermission(id, project, user), is(allowed));

        assertForEachPermissionKey(key -> permissionManager.hasPermission(key, project, user, true), is(allowed));
        assertForEachPermissionId(id -> permissionManager.hasPermission(id, project, user, true), is(allowed));

        assertForEachPermissionKey(key -> permissionManager.hasPermission(key, project, user, allowed), is(allowed));
        assertForEachPermissionId(id -> permissionManager.hasPermission(id, project, user, allowed), is(allowed));
    }

    private void forEachKey(Consumer<ProjectPermissionKey> callback) {
        SYSTEM_PERMISSIONS.forEach(callback);
    }

    private void forEachId(Consumer<Integer> callback) {
        SYSTEM_PERMISSIONS.forEach(key -> callback.accept(Permissions.getType(key.permissionKey())));
    }

    private <T> void assertForEachPermissionKey(Function<ProjectPermissionKey, T> callback, Matcher<? super T> matcher) {
        forEachKey(key ->
        {
            //when
            T value = callback.apply(key);

            //then
            assertThat(String.format("Failed for key: %s", key), value, matcher);
        });
    }

    private <T> void assertForEachPermissionId(Function<Integer, T> callback, Matcher<? super T> matcher) {
        forEachId(id ->
        {
            //when
            T value = callback.apply(id);

            //then
            assertThat(String.format("Failed for id: %d (%s)", id, Permissions.getShortName(id)), value, matcher);
        });

    }

    private static List<ProjectPermission> permissions(String... names) {
        return Arrays.stream(names)
                .map(ApplicationRequiredPermissionManagerTest::permission)
                .collect(CollectorsUtil.toImmutableList());
    }

    private static MockProjectPermission permission(final String name) {
        return new MockProjectPermission(name, name, name, ProjectPermissionCategory.ISSUES);
    }

    private static List<Group> groups(String... groups) {
        return Arrays.stream(groups)
                .map(MockGroup::new)
                .collect(CollectorsUtil.toImmutableList());
    }

    private void giveAllAccessTo(ApplicationUser user) {
        when(delegatePermissionManager.hasPermission(anyInt(), eq(issue), eq(user))).thenReturn(true);
        when(delegatePermissionManager.hasPermission(any(ProjectPermissionKey.class), eq(issue), eq(user))).thenReturn(true);
        when(delegatePermissionManager.hasPermission(anyInt(), eq(project), eq(user))).thenReturn(true);
        when(delegatePermissionManager.hasPermission(any(ProjectPermissionKey.class), eq(project), eq(user))).thenReturn(true);
        when(delegatePermissionManager.hasPermission(anyInt(), eq(project), eq(user), anyBoolean())).thenReturn(true);
        when(delegatePermissionManager.hasPermission(any(ProjectPermissionKey.class), eq(project), eq(user), anyBoolean())).thenReturn(true);
        when(delegatePermissionManager.hasProjects(anyInt(), eq(user))).thenReturn(true);
        when(delegatePermissionManager.hasProjects(any(ProjectPermissionKey.class), eq(user))).thenReturn(true);

        when(delegatePermissionManager.getProjects(anyInt(), eq(user), any())).thenReturn(projects);
        when(delegatePermissionManager.getProjects(any(ProjectPermissionKey.class), eq(user), any())).thenReturn(projects);

        when(delegatePermissionManager.getProjects(anyInt(), eq(user))).thenReturn(projects);
        when(delegatePermissionManager.getProjects(any(ProjectPermissionKey.class), eq(user))).thenReturn(projects);

        when(delegatePermissionManager.hasProjectWidePermission(any(), any(), eq(user))).thenReturn(ProjectWidePermission.ALL_ISSUES);
    }
}
