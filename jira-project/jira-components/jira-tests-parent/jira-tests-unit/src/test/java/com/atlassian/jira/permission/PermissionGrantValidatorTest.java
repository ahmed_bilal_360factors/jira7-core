package com.atlassian.jira.permission;

import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.matchers.ErrorCollectionMatcher;
import com.atlassian.jira.mock.i18n.MockI18nHelper;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.security.plugin.ProjectPermissionKey;
import com.atlassian.jira.security.plugin.ProjectPermissionTypesManager;
import com.atlassian.jira.security.roles.ProjectRole;
import com.atlassian.jira.security.roles.ProjectRoleManager;
import com.atlassian.jira.security.type.SecurityType;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.I18nHelper;
import com.google.common.collect.ImmutableMap;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import static com.atlassian.jira.matchers.ErrorCollectionMatchers.containsFieldError;
import static com.atlassian.jira.matchers.ErrorCollectionMatchers.containsSystemError;
import static com.atlassian.jira.matchers.ErrorCollectionMatchers.isEmpty;
import static com.atlassian.jira.permission.JiraPermissionHolderType.ANYONE;
import static com.atlassian.jira.permission.JiraPermissionHolderType.ASSIGNEE;
import static com.atlassian.jira.permission.JiraPermissionHolderType.GROUP;
import static com.atlassian.jira.permission.JiraPermissionHolderType.GROUP_CUSTOM_FIELD;
import static com.atlassian.jira.permission.JiraPermissionHolderType.PROJECT_LEAD;
import static com.atlassian.jira.permission.JiraPermissionHolderType.PROJECT_ROLE;
import static com.atlassian.jira.permission.JiraPermissionHolderType.REPORTER;
import static com.atlassian.jira.permission.JiraPermissionHolderType.USER;
import static com.atlassian.jira.permission.JiraPermissionHolderType.USER_CUSTOM_FIELD;
import static com.atlassian.jira.permission.PermissionGrantInput.newGrant;
import static com.atlassian.jira.permission.PermissionHolder.holder;
import static com.atlassian.jira.permission.ProjectPermissions.ADMINISTER_PROJECTS;
import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyMap;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public final class PermissionGrantValidatorTest {
    @Mock
    private PermissionTypeManager permissionTypeManager;
    @Mock
    private ProjectPermissionTypesManager projectPermissionsManager;
    @Mock
    private GroupManager groupManager;
    @Mock
    private CustomFieldManager customFieldManager;
    @Mock
    private ProjectRoleManager projectRoleManager;
    @Mock
    private UserManager userManager;

    private I18nHelper i18nHelper = new MockI18nHelper();

    private PermissionGrantValidator validator;
    @Mock
    private ApplicationUser user;

    private final SecurityType securityType = mock(SecurityType.class);

    @Before
    public void setUp() throws Exception {
        validator = new PermissionGrantValidator(permissionTypeManager, projectPermissionsManager, i18nHelper, groupManager, customFieldManager, projectRoleManager, userManager);
        when(permissionTypeManager.getSchemeType(anyString())).thenReturn(securityType);
        when(securityType.isValidForPermission(any(ProjectPermissionKey.class))).thenReturn(TRUE);
        when(projectPermissionsManager.exists(any(ProjectPermissionKey.class))).thenReturn(TRUE);
    }

    @Test
    public void errorIfGroupDoesNotExist() {
        ErrorCollection errorCollection = validator.validateGrant(user, grantWithHolder(GROUP, "non-existent-group"));
        assertThat(errorCollection, containsFieldError("holder.parameter", "admin.schemes.permissions.validation.group.does.not.exist [non-existent-group]"));
    }

    @Test
    public void errorIfProjectRoleDoesNotExist() {
        ErrorCollection errorCollection = validator.validateGrant(user, grantWithHolder(PROJECT_ROLE, "42"));
        assertThat(errorCollection, containsFieldError("holder.parameter", "admin.schemes.permissions.validation.project.role.does.not.exist [42]"));
    }

    @Test
    public void errorIfProjectRoleParameterIsNotANumberExist() {
        ErrorCollection errorCollection = validator.validateGrant(user, grantWithHolder(PROJECT_ROLE, "1.2"));
        assertThat(errorCollection, containsFieldError("holder.parameter", "admin.schemes.permissions.validation.project.role.id.must.be.number"));
    }

    @Test
    public void errorIfUserDoesNotExist() {
        ErrorCollection errorCollection = validator.validateGrant(user, grantWithHolder(USER, "fred"));
        assertThat(errorCollection, containsFieldError("holder.parameter", "admin.schemes.permissions.validation.user.does.not.exist [fred]"));
    }

    @Test
    public void errorIfUserCustomFieldDoesNotExist() {
        ErrorCollection errorCollection = validator.validateGrant(user, grantWithHolder(USER_CUSTOM_FIELD, "a"));
        assertThat(errorCollection, containsFieldError("holder.parameter", "admin.schemes.permissions.validation.cf.does.not.exist [a]"));
    }

    @Test
    public void errorIGroupCustomFieldDoesNotExist() {
        ErrorCollection errorCollection = validator.validateGrant(user, grantWithHolder(GROUP_CUSTOM_FIELD, "a"));
        assertThat(errorCollection, containsFieldError("holder.parameter", "admin.schemes.permissions.validation.cf.does.not.exist [a]"));
    }

    @Test
    public void noErrorWhenCorrectholder() {
        when(userManager.getUserByName("admin")).thenReturn(mock(ApplicationUser.class));
        when(groupManager.groupExists("jira-developers")).thenReturn(TRUE);
        when(customFieldManager.exists("CF-42")).thenReturn(TRUE);
        when(projectRoleManager.getProjectRole(42L)).thenReturn(mock(ProjectRole.class));

        PermissionGrantInput[] correctEntities = {
                grantWithHolder(ASSIGNEE), grantWithHolder(ANYONE), grantWithHolder(PROJECT_LEAD), grantWithHolder(REPORTER),
                grantWithHolder(GROUP, "jira-developers"), grantWithHolder(USER, "admin"), grantWithHolder(PROJECT_ROLE, "42"),
                grantWithHolder(GROUP_CUSTOM_FIELD, "CF-42"), grantWithHolder(USER_CUSTOM_FIELD, "CF-42")
        };

        for (PermissionGrantInput entity : correctEntities) {
            assertThat("there should be no errors for " + entity.getHolder(), validator.validateGrant(user, entity), isEmpty());
        }
    }

    @Test
    public void errorWhenUnrecognizedPermission() {
        ProjectPermissionKey nonExistentPermission = new ProjectPermissionKey("non-existent-permission");

        when(projectPermissionsManager.exists(nonExistentPermission)).thenReturn(FALSE);

        PermissionGrantInput entity = newGrant(PermissionHolder.holder(ASSIGNEE), nonExistentPermission);

        assertThat(validator.validateGrant(user, entity), containsFieldError("permission", "admin.schemes.permissions.validation.permission.unrecognized [non-existent-permission]"));
    }

    @Test
    public void errorsFromSchemeTypesAreHonored() {
        doAnswer(new Answer<Object>() {
            @Override
            public Object answer(final InvocationOnMock invocation) throws Throwable {
                JiraServiceContext context = (JiraServiceContext) invocation.getArguments()[2];
                context.getErrorCollection().addError("err", "err");
                return null;
            }
        }).when(securityType).doValidation(anyString(), anyMap(), any(JiraServiceContext.class));

        ErrorCollection errorCollection = validator.validateGrant(user, grantWithHolder(ASSIGNEE));
        assertThat(errorCollection, containsFieldError("err", "err"));
    }

    @Test
    public void typeAndPermissionCompatibilityIsChecked() {
        when(securityType.isValidForPermission(any(ProjectPermissionKey.class))).thenReturn(false);

        ErrorCollection errorCollection = validator.validateGrant(user, grantWithHolder(ASSIGNEE));
        assertThat(errorCollection, containsSystemError("admin.permissions.errors.invalid.combination [ADMINISTER_PROJECTS] [assignee]"));
    }

    @Test
    public void userNameIsValidatedForTypeUser() {
        when(securityType.isValidForPermission(any(ProjectPermissionKey.class))).thenReturn(false);

        ErrorCollection errorCollection = validator.validateGrant(user, grantWithHolder(USER, "userName"));
        assertThat(errorCollection, ErrorCollectionMatcher.hasError("holder.parameter", "admin.schemes.permissions.validation.user.does.not.exist [userName]", ErrorCollection.Reason.VALIDATION_FAILED));
    }

    @Test
    public void userNameIsValidatedForSchemeTypeUser() throws IllegalAccessException, InstantiationException {
        final String userName = "userName";

        when(securityType.isValidForPermission(any(ProjectPermissionKey.class))).thenReturn(true);
        doAnswer(invocation -> {
            JiraServiceContext context = (JiraServiceContext) invocation.getArguments()[2];
            context.getErrorCollection().addError("errSec", "errSec");
            return null;
        }).when(securityType).doValidation(eq(USER.getKey()), eq(ImmutableMap.of(USER.getKey(), userName)), any(JiraServiceContext.class));

        // This enable us to pass the first user name validation
        when(userManager.getUserByName(userName)).thenReturn(new MockApplicationUser(userName));
        ErrorCollection errorCollection = validator.validateGrant(user, grantWithHolder(USER, userName));

        // This verifies that the correct arguments was passed to the user SecurityType for validation
        assertThat(errorCollection, ErrorCollectionMatcher.hasError("errSec", "errSec"));
    }

    private PermissionGrantInput grantWithHolder(PermissionHolderType type) {
        return grantWithHolder(type, null);

    }

    private PermissionGrantInput grantWithHolder(PermissionHolderType type, String parameter) {
        return newGrant(holder(type, parameter), ADMINISTER_PROJECTS);
    }
}
