package com.atlassian.jira.upgrade.tasks.role;

import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.fugue.Option;
import com.atlassian.jira.application.ApplicationKeys;
import com.atlassian.jira.test.util.lic.core.CoreLicenses;
import com.atlassian.jira.test.util.lic.sd.ServiceDeskLicenses;
import com.atlassian.jira.test.util.lic.software.SoftwareLicenses;
import com.atlassian.jira.upgrade.tasks.role.MigrationValidatorImpl.MigrationValidationFailedException;
import com.atlassian.jira.user.MockGroup;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Set;

import static com.atlassian.jira.upgrade.tasks.role.TestUtils.toLicense;
import static org.hamcrest.core.StringContains.containsString;
import static org.mockito.Mockito.when;


public class MigrationValidatorImplUseTest {
    private static final MockGroup ADMIN_ONE = new MockGroup("AdminOne");
    private static final MockGroup ADMIN_TWO = new MockGroup("AdminTwo");
    private static final MockGroup ADMIN_THREE = new MockGroup("AdminThree");
    private static final MockGroup SOFTWARE_ONE = new MockGroup("SoftwareOne");
    private static final MockGroup SOFTWARE_TWO = new MockGroup("SoftwareTwo");
    private static final MockGroup SOFTWARE_THREE = new MockGroup("SoftwareThree");
    private static final MockGroup OTHER_GROUP = new MockGroup("Other");
    final License swLicense = toLicense(SoftwareLicenses.LICENSE_SOFTWARE);
    final License coreLicense = toLicense(CoreLicenses.LICENSE_CORE);
    final License sdLicense = toLicense(ServiceDeskLicenses.LICENSE_SERVICE_DESK_TBP);
    final License sdAbpLicense = toLicense(ServiceDeskLicenses.LICENSE_SERVICE_DESK_ABP);
    final Licenses twoLicenses = new Licenses(ImmutableSet.of(swLicense, coreLicense));
    final Licenses threeLicenses = new Licenses(ImmutableSet.of(swLicense, coreLicense, sdLicense));

    @Mock
    private GlobalPermissionDao globalPermissionDao;
    @Mock
    Jira6xServiceDeskLicenseProvider licenseSupplier;
    @Mock
    MigrationGroupService migrationGroupService;

    private final Set<Group> adminGroups = ImmutableSet.of(ADMIN_ONE, ADMIN_TWO, ADMIN_THREE);
    private final Set<Group> useGroups = ImmutableSet.of(SOFTWARE_ONE, SOFTWARE_TWO);
    private final Set<Group> useAndAdminGroups = ImmutableSet.of(SOFTWARE_ONE, SOFTWARE_TWO, ADMIN_ONE, ADMIN_TWO, ADMIN_THREE);
    private MigrationValidator validator;

    @Rule
    public ExpectedException thrown = ExpectedException.none();
    @Rule
    public MockitoRule initMocks = MockitoJUnit.rule();

    @Before
    public void setUp() throws Exception {
        validator = new MigrationValidatorImpl(this.globalPermissionDao, licenseSupplier, migrationGroupService);
        when(globalPermissionDao.groupsWithAdminPermission()).thenReturn(adminGroups);
        when(globalPermissionDao.groupsWithUsePermission()).thenReturn(useAndAdminGroups);

    }

    @Test
    public void validationShouldPassWhenSoftwareAndCoreHaveCorrectGroups() {
        // given
        final ApplicationRole softwareRole = ApplicationRole.forKey(ApplicationKeys.SOFTWARE)
                .withGroups(useAndAdminGroups, useGroups);
        final ApplicationRole coreRole = ApplicationRole.forKey(ApplicationKeys.CORE)
                .withGroups(useAndAdminGroups, useGroups);

        MigrationState state = migrationStateOf(twoLicenses, new ApplicationRoles(ImmutableList.of(softwareRole, coreRole)));

        // when
        validator.validate(TestUtils.emptyState(), state);
        // then
        // there is no exception thrown
    }

    @Test
    public void validationShouldPassWhenSoftwareCoreAndServiceDeskHaveCorrectGroups() {
        // given
        final ApplicationRole softwareRole = ApplicationRole.forKey(ApplicationKeys.SOFTWARE)
                .withGroups(useAndAdminGroups, useGroups);
        final ApplicationRole coreRole = ApplicationRole.forKey(ApplicationKeys.CORE)
                .withGroups(useAndAdminGroups, useGroups);
        final ApplicationRole sdRole = ApplicationRole.forKey(ApplicationKeys.SERVICE_DESK)
                .withGroups(useAndAdminGroups, useGroups);

        MigrationState state = migrationStateOf(threeLicenses,
                new ApplicationRoles(ImmutableList.of(softwareRole, coreRole, sdRole)));
        when(licenseSupplier.serviceDeskLicense()).thenReturn(Option.some(sdLicense));

        // when
        validator.validate(TestUtils.emptyState(), state);
        // then
        // there is no exception thrown
    }

    @Test
    public void validationShouldFailWhenExtraGroupIsFoundInRole() {
        // given
        thrown.expect(MigrationValidationFailedException.class);
        thrown.expectMessage(containsString(SOFTWARE_THREE.getName()));

        final ImmutableSet<Group> extraGroups = ImmutableSet.of(SOFTWARE_ONE, SOFTWARE_TWO, SOFTWARE_THREE);
        final ApplicationRole softwareRole = ApplicationRole.forKey(ApplicationKeys.SOFTWARE)
                .withGroups(Sets.union(extraGroups, adminGroups), useGroups);
        final ApplicationRole coreRole = ApplicationRole.forKey(ApplicationKeys.CORE)
                .withGroups(useAndAdminGroups, useGroups);

        MigrationState state = migrationStateOf(twoLicenses, new ApplicationRoles(ImmutableList.of(softwareRole, coreRole)));

        // when
        validator.validate(TestUtils.emptyState(), state);
        // then
        // there is no exception thrown
    }

    @Test
    public void validationShouldPassWhenExtraGroupIsInPreviousState() {
        // given
        final ApplicationRole initialSoftwareRole = ApplicationRole.forKey(ApplicationKeys.SOFTWARE)
                .withGroups(ImmutableSet.of(SOFTWARE_ONE, SOFTWARE_TWO, OTHER_GROUP), ImmutableSet.of());
        final ApplicationRole softwareRole = ApplicationRole.forKey(ApplicationKeys.SOFTWARE)
                .withGroups(Sets.union(useAndAdminGroups, ImmutableSet.of(OTHER_GROUP)), useGroups);
        final ApplicationRole coreRole = ApplicationRole.forKey(ApplicationKeys.CORE)
                .withGroups(useAndAdminGroups, useGroups);
        MigrationState originalState = migrationStateOf(twoLicenses, new ApplicationRoles(ImmutableList.of(initialSoftwareRole)));
        MigrationState state = migrationStateOf(twoLicenses, new ApplicationRoles(ImmutableList.of(softwareRole, coreRole)));

        // when
        validator.validate(originalState, state);
        // then
        // there is no exception thrown
    }

    @Test
    public void validationShouldPassNoAppsWhenNoUseAndAdminGroupsFound() {
        // given

        when(licenseSupplier.serviceDeskLicense()).thenReturn(Option.some(sdAbpLicense));
        when(globalPermissionDao.groupsWithAdminPermission()).thenReturn(ImmutableSet.of());
        when(globalPermissionDao.groupsWithUsePermission()).thenReturn(ImmutableSet.of());


        MigrationState state = migrationStateOf(threeLicenses,
                new ApplicationRoles(ImmutableList.of()));

        // when
        validator.validate(TestUtils.emptyState(), state);
        // then
        // there is no exception thrown
    }

    @Test
    public void validationShouldFailWhenServiceDeskHasMoreGroups() {
        thrown.expect(MigrationValidationFailedException.class);
        thrown.expectMessage(containsString(SOFTWARE_THREE.getName()));
        // given

        when(licenseSupplier.serviceDeskLicense()).thenReturn(Option.some(sdLicense));
        final ImmutableSet<Group> extraGroups = ImmutableSet.of(SOFTWARE_ONE, SOFTWARE_TWO, SOFTWARE_THREE);
        final ApplicationRole softwareRole = ApplicationRole.forKey(ApplicationKeys.SOFTWARE)
                .withGroups(useAndAdminGroups, useGroups);
        final ApplicationRole coreRole = ApplicationRole.forKey(ApplicationKeys.CORE)
                .withGroups(useAndAdminGroups, useGroups);
        final ApplicationRole sdRole = ApplicationRole.forKey(ApplicationKeys.SERVICE_DESK)
                .withGroups(extraGroups, useGroups);

        MigrationState state = migrationStateOf(threeLicenses,
                new ApplicationRoles(ImmutableList.of(softwareRole, coreRole, sdRole)));

        // when
        validator.validate(TestUtils.emptyState(), state);
        // then
        // there is no exception thrown
    }

    private static MigrationState migrationStateOf(Licenses licenses, ApplicationRoles roles) {
        return new MigrationState(licenses, roles, ImmutableList.<Runnable>of(), new MigrationLogImpl());
    }
}