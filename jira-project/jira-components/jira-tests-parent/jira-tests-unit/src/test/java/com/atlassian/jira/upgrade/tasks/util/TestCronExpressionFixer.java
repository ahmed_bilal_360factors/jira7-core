package com.atlassian.jira.upgrade.tasks.util;


import com.atlassian.jira.upgrade.tasks.util.CronExpressionFixer.Result;
import com.atlassian.scheduler.cron.CronSyntaxException;
import com.atlassian.scheduler.cron.ErrorCode;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import org.junit.Test;

import java.util.Map;
import java.util.Optional;
import java.util.Set;

import static com.atlassian.jira.upgrade.tasks.util.CronExpressionFixer.repairCronExpression;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

/**
 * @since v7.0.0
 */
public class TestCronExpressionFixer {
    private static final Map<String, String> FIXABLE_CRON_EXPRESSIONS = ImmutableMap.<String, String>builder()
            .put("0 0/60 8-16 ? * MON-FRI", "0 0 8-16 ? * MON-FRI")
            .put("0 0/60 8-18 ? * MON-FRI", "0 0 8-18 ? * MON-FRI")
            .put("0 0/60 * * * ?", "0 0 * * * ?")
            .put("0 0/120 8-18 ? * MON-FRI", "0 0 8-18 ? * MON-FRI")
            .put("0 0/1000000000 * * * ? *", "0 0 * * * ? *")
            .put("0 0/120 7-17 ? * MON-FRI", "0 0 7-17 ? * MON-FRI")
            .put("0 0/150 * * * ? *", "0 0 * * * ? *")
            .put("0 0/60 * ? * * *", "0 0 * ? * * *")
            .put("0 0 0/72 * * ?", "0 0 0 * * ?")
            .put("0 0/0 23-8 ? * MON-FRI *", "0 0 23-8 ? * MON-FRI *")
            .put("0 0/60 * 1/1 * ? *", "0 0 * 1/1 * ? *")
            .put("0 0/60 7-22 * * ? *", "0 0 7-22 * * ? *")
            .put("0 0/60 * ? * MON-FRI", "0 0 * ? * MON-FRI")
            .put("0 40 9 1W,15W 1-12 ? 2010-2050", "0 40 9 1,15 1-12 ? 2010-2050")
            .put("0 40 9 1W,15WWWWWWWWWWWWWWWWWWWWW 1-12 ? 2010-2050", "0 40 9 1,15 1-12 ? 2010-2050")
            .put("0 40 9 1W,15 1-12 ? 2010-2050", "0 40 9 1,15 1-12 ? 2010-2050")
            .put("0 40 9 1,15W 1-12 ? 2010-2050", "0 40 9 1,15 1-12 ? 2010-2050")
            .put("0 0/2 8-18 ? * MON-FRI *?", "0 0/2 8-18 ? * MON-FRI *")
            .build();

    private static final Map<String, ErrorCode> UNFIXABLE_CRON_EXPRESSIONS = ImmutableMap.<String, ErrorCode>builder()
            .put("0 0/2 8-18 ? * MXN-FRI *?", ErrorCode.INVALID_NAME_DAY_OF_WEEK)
            .put("0/120 0/60 1/50 1/40,15W 1/14,3/14 ?*", ErrorCode.INVALID_STEP_SECOND_OR_MINUTE)
            .put("m000000!!!", ErrorCode.INVALID_NAME)
            .build();

    private static final Set<String> NO_ERRORS = ImmutableSet.<String>builder()
            .add(" \t 0 0/15 6-23 ? * SUN-THU")
            .build();

    @Test
    public void fixableCronExpressionsShouldGetFixed() {
        FIXABLE_CRON_EXPRESSIONS.forEach(TestCronExpressionFixer::assertSuccess);
    }

    @Test
    public void unfixableCronExpressionsShouldNotGetFixed() {
        UNFIXABLE_CRON_EXPRESSIONS.forEach(TestCronExpressionFixer::assertFailure);
    }

    @Test
    public void parseGoodExpressions() throws CronSyntaxException {
        NO_ERRORS.forEach(TestCronExpressionFixer::assertUnchanged);
    }


    private static void assertSuccess(final String oldCronExpression, final String newCronExpression) {
        assertThat(oldCronExpression, repairCronExpression(oldCronExpression), is(Result.success(newCronExpression)));
    }

    private static void assertFailure(final String oldCronExpression, final ErrorCode errorCode) {
        final Result result = repairCronExpression(oldCronExpression);
        final Optional<CronSyntaxException> exception = result.getCronSyntaxException();
        if (!exception.isPresent() || exception.get().getErrorCode() != errorCode) {
            fail("Expected a failing result with error code " + errorCode + "; got " + result.toString());
        }
    }

    private static void assertUnchanged(final String oldCronExpression) {
        assertThat(oldCronExpression, repairCronExpression(oldCronExpression), is(Result.ALREADY_VALID));
    }
}
