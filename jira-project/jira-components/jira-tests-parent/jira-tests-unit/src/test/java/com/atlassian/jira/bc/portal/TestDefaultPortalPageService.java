package com.atlassian.jira.bc.portal;

import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.bc.JiraServiceContextImpl;
import com.atlassian.jira.bc.MockJiraServiceContext;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.exception.PermissionException;
import com.atlassian.jira.favourites.FavouritesManager;
import com.atlassian.jira.mock.component.MockComponentWorker;
import com.atlassian.jira.mock.i18n.MockI18nHelper;
import com.atlassian.jira.portal.PortalPage;
import com.atlassian.jira.portal.PortalPageManager;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.sharing.SharePermission;
import com.atlassian.jira.sharing.SharePermissionImpl;
import com.atlassian.jira.sharing.ShareTypeValidatorUtils;
import com.atlassian.jira.sharing.SharedEntity;
import com.atlassian.jira.sharing.type.GlobalShareType;
import com.atlassian.jira.sharing.type.ShareType;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.util.UserUtil;
import com.atlassian.jira.util.I18nHelper;
import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.atlassian.jira.matchers.ErrorCollectionMatchers.containsFieldError;
import static com.atlassian.jira.matchers.ErrorCollectionMatchers.containsSystemError;
import static com.atlassian.jira.matchers.ErrorCollectionMatchers.isEmpty;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.Matchers.sameInstance;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestDefaultPortalPageService {
    @Rule
    public final MethodRule initMockito = MockitoJUnit.rule();
    @Mock
    protected PortalPageManager portalPageManager;
    @Mock
    protected FavouritesManager<PortalPage> favouritesManager;
    @Mock
    protected ShareTypeValidatorUtils shareTypeValidatorUtils;
    @Mock
    protected PermissionManager permissionManager;
    @Mock
    protected UserUtil userUtil;
    protected ApplicationUser user;
    protected PortalPage portalPage1;
    protected PortalPage portalPage2;
    protected PortalPage portalPage3;
    protected PortalPage portalPageClone;
    protected PortalPage portalPageSystemDefault;
    protected PortalPage portalPageNotOwnedByUser;
    protected static final Long CLONE_ID = (long) 666;
    protected ApplicationUser notTheOwner;

    protected PortalPageService createPortalPageService() {
        return new DefaultPortalPageService(portalPageManager, shareTypeValidatorUtils, favouritesManager, permissionManager, userUtil);
    }

    protected JiraServiceContext createContext(final ApplicationUser user) {
        return new MockJiraServiceContext(user) {
            public I18nHelper getI18nBean() {
                return new MockI18nHelper();
            }
        };
    }

    protected JiraServiceContext createContext() {
        return createContext(user);
    }

    @Before
    public void setUp() throws Exception {
        user = new MockApplicationUser("admin");
        notTheOwner = new MockApplicationUser("notTheOwner");

        portalPage1 = PortalPage.id(1L).name("one").description("one description").owner(user).build();
        portalPage2 = PortalPage.id(2L).name("two").description("two description").owner(user).build();
        portalPage3 = PortalPage.id(3L).name("three").description("three description").owner(user).build();
        portalPageClone = PortalPage.id(CLONE_ID).name("clone").description("clone description").owner(user).build();
        portalPageSystemDefault = PortalPage.id(CLONE_ID).name("clone").description("clone description").systemDashboard().build();

        final ApplicationUser notUser = new MockApplicationUser("notMe");
        portalPageNotOwnedByUser = PortalPage.id(CLONE_ID).name("notownedbyuser").description("").owner(notUser).build();

        ComponentAccessor.initialiseWorker(new MockComponentWorker());
    }

    @Test
    public void favouritePortalPagesReturnsAllowedPages() {
        final List<Long> favouriteIds = Arrays.asList(portalPage3.getId(), portalPage2.getId(), portalPage1.getId()); // return is reverse order to test sorting

        when(favouritesManager.getFavouriteIds(user, PortalPage.ENTITY_TYPE)).thenReturn(favouriteIds);
        when(portalPageManager.getPortalPage(user, portalPage3.getId())).thenReturn(portalPage3);
        when(portalPageManager.getPortalPage(user, portalPage2.getId())).thenReturn(portalPage2);
        when(portalPageManager.getPortalPage(user, portalPage1.getId())).thenReturn(null);

        final PortalPageService service = createPortalPageService();

        final Collection<PortalPage> results = service.getFavouritePortalPages(user);

        assertThat(results, containsInAnyOrder(idEquals(portalPage2), idEquals(portalPage3)));
    }

    /**
     * Matcher for {@code PortalPage} that matches using {@code getId()} method.
     *
     * @param expectedPage expected page
     * @return matcher
     */
    private static Matcher<PortalPage> idEquals(final PortalPage expectedPage) {
        return new BaseMatcher<PortalPage>() {
            @Override
            public void describeTo(Description description) {
                description.appendValue(expectedPage);
            }

            @Override
            public boolean matches(final Object item) {
                if (!(item instanceof PortalPage)) {
                    return false;
                }
                final PortalPage resultPage = (PortalPage) item;
                return expectedPage.getId().equals(resultPage.getId());
            }
        };
    }

    @Test
    public void favouritePortalPagesReturnsEmptyCollectionForNullUser() {
        final PortalPageService service = createPortalPageService();

        final Collection<PortalPage> results = service.getFavouritePortalPages(null);

        assertThat(results, empty());
    }

    @Test
    public void favouritePortalPagesReturnsEmptyCollectionWhenNoFavourites() {
        when(favouritesManager.getFavouriteIds(user, PortalPage.ENTITY_TYPE)).thenReturn(Collections.<Long>emptyList());

        final PortalPageService service = createPortalPageService();

        final Collection<PortalPage> results = service.getFavouritePortalPages(user);

        assertThat(results, empty());
    }

    /**
     * Check that {@link DefaultPortalPageService#isFavourite(ApplicationUser, com.atlassian.jira.portal.PortalPage)} always delegate through to the favourites manager.
     *
     * @throws com.atlassian.jira.exception.PermissionException just re-throw for an error for test failure.
     */
    @Test
    public void isFavouriteReturnsTrueWhenPageIsFavoritedByUser() throws PermissionException {
        when(favouritesManager.isFavourite(user, portalPage1)).thenReturn(true);
        when(favouritesManager.isFavourite(user, portalPage2)).thenReturn(false);

        final PortalPageService service = createPortalPageService();

        assertThat(service.isFavourite(user, portalPage1), is(true));
        assertThat(service.isFavourite(user, portalPage2), is(false));
    }

    /**
     * Check that {@link DefaultPortalPageService#isFavourite(ApplicationUser, com.atlassian.jira.portal.PortalPage)} always returns false when the user is anonymous.
     */
    @Test
    public void isFavouriteReturnsFalseForAnonymousUser() {
        final PortalPageService service = createPortalPageService();

        assertThat(service.isFavourite(null, portalPage2), is(false));
        assertThat(service.isFavourite(null, portalPage3), is(false));
    }

    /**
     * Check that {@link DefaultPortalPageService#isFavourite(ApplicationUser, com.atlassian.jira.portal.PortalPage)} always returns false on exception.
     *
     * @throws com.atlassian.jira.exception.PermissionException just re-throw for an error for test failure.
     */
    @Test
    public void isFavouriteReturnsFalseOnException() throws PermissionException {
        when(favouritesManager.isFavourite(user, portalPage1)).thenThrow(new PermissionException("testIsFavouriteWithException"));

        final PortalPageService service = createPortalPageService();

        assertThat(service.isFavourite(user, portalPage1), is(false));
    }

    @Test
    public void ownedPortalPagesReturnsCorrectPages() {
        when(portalPageManager.getAllOwnedPortalPages(user)).thenReturn(Arrays.asList(portalPage3, portalPage2));

        final PortalPageService service = createPortalPageService();

        final Collection<PortalPage> results = service.getOwnedPortalPages(user);

        assertThat(results, containsInAnyOrder(idEquals(portalPage2), idEquals(portalPage3)));
    }

    @Test
    public void deleteAllPagesForUser() {
        when(portalPageManager.getAllOwnedPortalPages(user)).thenReturn(Arrays.asList(portalPage1, portalPage2, portalPage3));
        final PortalPageService service = createPortalPageService();

        service.deleteAllPortalPagesForUser(user);

        verify(portalPageManager).delete(portalPage1.getId());
        verify(favouritesManager).removeFavouritesForEntityDelete(portalPage1);

        verify(portalPageManager).delete(portalPage2.getId());
        verify(favouritesManager).removeFavouritesForEntityDelete(portalPage2);

        verify(portalPageManager).delete(portalPage3.getId());
        verify(favouritesManager).removeFavouritesForEntityDelete(portalPage3);

        verify(favouritesManager).removeFavouritesForUser(user, PortalPage.ENTITY_TYPE);
    }

    @Test
    public void deletePortalPage() {
        when(portalPageManager.getPortalPageById(portalPage1.getId())).thenReturn(portalPage1);
        when(portalPageManager.getPortalPage(user, portalPage1.getId())).thenReturn(portalPage1);
        final PortalPageService service = createPortalPageService();
        final JiraServiceContext ctx = createContext();

        service.deletePortalPage(ctx, portalPage1.getId());

        assertThat(ctx.getErrorCollection(), isEmpty());

        verify(portalPageManager).delete(portalPage1.getId());
        verify(favouritesManager).removeFavouritesForEntityDelete(portalPage1);
    }

    @Test
    public void deletePortalPageNoPortalPageReturnsErrorMessage() {
        final Long portalPageToDeleteId = 1L;

        when(portalPageManager.getPortalPageById(portalPage1.getId())).thenReturn(null);

        final PortalPageService service = createPortalPageService();
        final JiraServiceContext ctx = createContext();

        service.deletePortalPage(ctx, portalPageToDeleteId);

        assertThat(ctx.getErrorCollection(), containsSystemError("admin.errors.portalpages.nonexistent"));
    }

    @Test
    public void deletePortalPageWhenNotAuthorReturnsErrorMessage() {
        final PortalPage pageOwnedByAnother = PortalPage.name("name").description("desc").owner(new MockApplicationUser("other")).build();
        when(portalPageManager.getPortalPageById(portalPage1.getId())).thenReturn(pageOwnedByAnother);

        final PortalPageService service = createPortalPageService();

        final JiraServiceContext ctx = createContext();

        service.deletePortalPage(ctx, portalPage1.getId());

        assertThat(ctx.getErrorCollection(), containsSystemError("admin.errors.portalpages.must.be.owner"));
    }

    @Test
    public void deletePortalPageAsAnonymousReturnsErrorMessage() {
        final PortalPageService service = createPortalPageService();

        final JiraServiceContext ctx = createContext(null);

        service.deletePortalPage(ctx, portalPage1.getId());

        assertThat(ctx.getErrorCollection(), containsSystemError("admin.errors.portalpages.owned.anonymous.user"));
    }

    @Test
    public void deletePortalPageNotReturnedFromManagerReturnsErrorMessage() {
        when(portalPageManager.getPortalPageById(portalPage1.getId())).thenReturn(portalPage1);
        when(portalPageManager.getPortalPage(user, portalPage1.getId())).thenReturn(null);

        final PortalPageService service = createPortalPageService();

        final JiraServiceContext ctx = createContext();

        service.deletePortalPage(ctx, portalPage1.getId());

        assertThat(ctx.getErrorCollection(), containsSystemError("admin.errors.portalpages.nonexistent"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void deletePortalPageThrowsExceptionOnNullPortalPageId() {
        final PortalPageService service = createPortalPageService();
        final JiraServiceContext ctx = createContext();
        service.deletePortalPage(ctx, null);
    }

    @Test
    public void deleteCantDeleteSystemDefaultPage() throws Exception {
        when(portalPageManager.getPortalPageById(portalPageSystemDefault.getId())).thenReturn(portalPageSystemDefault);

        final PortalPageService service = createPortalPageService();
        final JiraServiceContext serviceContext = createContext();

        service.deletePortalPage(serviceContext, portalPageSystemDefault.getId());

        assertThat(serviceContext.getErrorCollection(), containsSystemError("admin.errors.portalpages.not.delete.system.default"));
    }

    @Test
    public void getPortalPageReturnsNoErrors() {
        when(portalPageManager.getPortalPage(user, portalPage1.getId())).thenReturn(portalPage1);

        final PortalPageService service = createPortalPageService();
        final JiraServiceContext ctx = createContext();
        final PortalPage result = service.getPortalPage(ctx, portalPage1.getId());

        assertThat(result, is(portalPage1));
        assertThat(ctx.getErrorCollection(), isEmpty());
    }

    @Test
    public void getPortalPageDoesntExistReturnsErrors() {
        final Long nonExistentId = 303L;

        when(portalPageManager.getPortalPage(user, nonExistentId)).thenReturn(null);

        final PortalPageService searchRequestService = createPortalPageService();
        final JiraServiceContext ctx = createContext();

        final PortalPage portalPage = searchRequestService.getPortalPage(ctx, nonExistentId);

        assertThat("where the hell did this come from?", portalPage, nullValue());
        assertThat(ctx.getErrorCollection(), containsSystemError("admin.errors.portalpages.no.access"));
    }

    @Test
    public void getPortalPageAnonymousUser() {
        when(portalPageManager.getPortalPage(null, portalPage1.getId())).thenReturn(portalPage1);

        final PortalPageService service = createPortalPageService();
        final JiraServiceContext ctx = createContext(null);
        final PortalPage result = service.getPortalPage(ctx, portalPage1.getId());

        assertThat(result, is(portalPage1));
        assertThat(ctx.getErrorCollection(), isEmpty());
    }

    @Test
    public void getPortalPageAnonymousUserDoesntExistReturnsErrorMessage() {
        final Long nonExistentId = 303L;

        when(portalPageManager.getPortalPage(null, nonExistentId)).thenReturn(null);

        final PortalPageService searchRequestService = createPortalPageService();
        final JiraServiceContext ctx = createContext(null);

        searchRequestService.getPortalPage(ctx, nonExistentId);

        assertThat(ctx.getErrorCollection(), containsSystemError("admin.errors.portalpages.no.access"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void getPortalPageThrowsExceptionOnPortalPageId() {
        final PortalPageService service = createPortalPageService();
        final JiraServiceContext ctx = new JiraServiceContextImpl(user);
        service.getPortalPage(ctx, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void getPortalPageThrowsExceptionOnNullCtx() {
        final PortalPageService service = createPortalPageService();
        service.getPortalPage(null, 1L);
    }

    @Test
    public void getNonPrivatePortalPageReturnsAll() {
        final SharedEntity.SharePermissions permissions = new SharedEntity.SharePermissions(Collections.singleton(new SharePermissionImpl(GlobalShareType.TYPE, null, null)));
        portalPage1 = PortalPage.portalPage(portalPage1).permissions(permissions).build();
        portalPage2 = PortalPage.portalPage(portalPage2).permissions(permissions).build();

        final List<PortalPage> portalPages = Arrays.asList(portalPage2, portalPage1, portalPage3);
        when(portalPageManager.getAllOwnedPortalPages(user)).thenReturn(portalPages);

        final PortalPageService service = createPortalPageService();

        final Collection<PortalPage> results = service.getNonPrivatePortalPages(user);

        assertThat(results, containsInAnyOrder(portalPage1, portalPage2));
    }

    @Test
    public void getNonPrivatePortalPagesWithPrivatePagesOnlyReturnsEmptyCollection() {
        final List<PortalPage> portalPages = Arrays.asList(portalPage1, portalPage2);

        when(portalPageManager.getAllOwnedPortalPages(user)).thenReturn(portalPages);

        final PortalPageService service = createPortalPageService();

        final Collection<PortalPage> results = service.getNonPrivatePortalPages(user);

        assertThat(results, empty());
    }

    @Test
    public void getNonPrivatePortalPagesAllBut1PrivateReturnsSinglePage() {

        final Set<SharePermission> permissions = Collections.<SharePermission>singleton(new SharePermissionImpl(GlobalShareType.TYPE, null, null));

        portalPage3 = PortalPage.portalPage(portalPage3).permissions(new SharedEntity.SharePermissions(permissions)).build();

        when(portalPageManager.getAllOwnedPortalPages(user)).thenReturn(Arrays.asList(portalPage1, portalPage2, portalPage3));

        final PortalPageService service = createPortalPageService();

        final Collection<PortalPage> results = service.getNonPrivatePortalPages(user);

        assertThat(results, containsInAnyOrder(portalPage3));
    }

    @Test
    public void getNonPrivatePortalPagesNullUserReturnsEmptyCollection() {
        final PortalPageService service = createPortalPageService();

        final Collection<PortalPage> results = service.getNonPrivatePortalPages(null);

        assertThat(results, empty());
    }

    @Test
    public void getPortalPagesFavouritedByOthersIsEmpty() {
        when(portalPageManager.getAllOwnedPortalPages(user)).thenReturn(Collections.<PortalPage>emptyList());

        final PortalPageService service = createPortalPageService();
        final Collection<PortalPage> result = service.getPortalPagesFavouritedByOthers(user);

        assertThat(result, empty());
    }

    @Test
    public void getPortalPagesFavouritedByOthers() {
        portalPage1 = PortalPage.id(1L).name("one").description("one description").owner(user).favouriteCount(3L).build();
        portalPage2 = PortalPage.id(2L).name("two").description("two description").owner(user).favouriteCount(3L).build();
        portalPage3 = PortalPage.id(3L).name("three").description("three description").owner(user).favouriteCount(3L).build();

        final SharedEntity.SharePermissions permissions = new SharedEntity.SharePermissions(Collections.singleton(new SharePermissionImpl(GlobalShareType.TYPE, null, null)));

        portalPage1 = PortalPage.portalPage(portalPage1).permissions(permissions).build();
        portalPage2 = PortalPage.portalPage(portalPage2).permissions(permissions).build();
        portalPage3 = PortalPage.portalPage(portalPage3).permissions(permissions).build();

        when(portalPageManager.getAllOwnedPortalPages(user)).thenReturn(Arrays.asList(portalPage2, portalPage3, portalPage1));

        when(favouritesManager.getFavouriteIds(user, PortalPage.ENTITY_TYPE)).thenReturn(Arrays.asList(1L, 2L, 3L));

        final PortalPageService service = createPortalPageService();
        final Collection<PortalPage> result = service.getPortalPagesFavouritedByOthers(user);

        assertThat(result, containsInAnyOrder(idEquals(portalPage1), idEquals(portalPage2), idEquals(portalPage3)));
    }

    @Test
    public void getPortalPagesFavouritedByOthersPortalPaged() {
        portalPage1 = PortalPage.id(1L).name("one").description("one description").owner(user).favouriteCount(3L).build();
        portalPage2 = PortalPage.id(2L).name("two").description("two description").owner(user).favouriteCount(2L).build();
        portalPage3 = PortalPage.id(3L).name("three").description("three description").owner(user).favouriteCount(0L).build();

        final Set<SharePermission> permissions = Collections.<SharePermission>singleton(new SharePermissionImpl(GlobalShareType.TYPE, null, null));

        portalPage2 = PortalPage.portalPage(portalPage2).permissions(new SharedEntity.SharePermissions(permissions)).build();

        when(portalPageManager.getAllOwnedPortalPages(user)).thenReturn(Arrays.asList(portalPage1, portalPage2, portalPage3));

        when(favouritesManager.getFavouriteIds(user, PortalPage.ENTITY_TYPE)).thenReturn(Collections.singletonList(1L));

        final PortalPageService service = createPortalPageService();
        final Collection<PortalPage> result = service.getPortalPagesFavouritedByOthers(user);

        assertThat(result, containsInAnyOrder(portalPage2));
    }

    @Test
    public void getPortalPagesFavouritedByOthersMorePortalPaged() {
        portalPage1 = PortalPage.id(1L).name("one").description("one description").owner(user).favouriteCount(2L).build();
        portalPage2 = PortalPage.id(2L).name("two").description("two description").owner(user).favouriteCount(1L).build();
        portalPage3 = PortalPage.id(3L).name("three").description("three description").owner(user).favouriteCount(3L).build();

        final HashSet<SharePermission> permissions = new HashSet<SharePermission>();
        permissions.add(new SharePermissionImpl(GlobalShareType.TYPE, null, null));

        portalPage1 = PortalPage.portalPage(portalPage1).permissions(new SharedEntity.SharePermissions(permissions)).build();
        portalPage3 = PortalPage.portalPage(portalPage3).permissions(new SharedEntity.SharePermissions(permissions)).build();

        final List<PortalPage> portalPages = Arrays.asList(portalPage2, portalPage1, portalPage3);

        when(portalPageManager.getAllOwnedPortalPages(user)).thenReturn(portalPages);
        when(favouritesManager.getFavouriteIds(user, PortalPage.ENTITY_TYPE)).thenReturn(Arrays.asList(1L, 2L));

        final PortalPageService service = createPortalPageService();
        final Collection<PortalPage> result = service.getPortalPagesFavouritedByOthers(user);

        assertThat(result, containsInAnyOrder(idEquals(portalPage1), idEquals(portalPage3)));
    }

    @Test(expected = IllegalArgumentException.class)
    public void validatePortalPageForUpdateWithNullCtxThrowsException() {
        final PortalPageService service = createPortalPageService();
        service.validateForUpdate(null, portalPage1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void validatePortalPageForUpdateWithNullDataThrowsException() {
        final JiraServiceContext ctx = new JiraServiceContextImpl(user);
        final PortalPageService service = createPortalPageService();
        service.validateForUpdate(ctx, null);
    }

    @Test
    public void validatePortalPageForUpdateWithNullUserReturnsErrors() {
        final JiraServiceContext ctx = createContext(null);

        portalPage1 = PortalPage.portalPage(portalPage1).permissions(SharedEntity.SharePermissions.PRIVATE).build();

        final PortalPageService service = createPortalPageService();
        service.validateForUpdate(ctx, portalPage1);

        assertThat(ctx.getErrorCollection(), containsSystemError("admin.errors.portalpages.owned.anonymous.user"));
    }

    @Test
    public void validatePortalPageForUpdateWithEmptyNameReturnsErrors() {
        final JiraServiceContext ctx = createContext();
        final Set<SharePermission> permissions = Collections.<SharePermission>singleton(new SharePermissionImpl(new ShareType.Name("exampleType"), null, null));

        portalPage1 = PortalPage.portalPage(portalPage1).name("").permissions(new SharedEntity.SharePermissions(permissions)).build();

        final PortalPageService service = createPortalPageService();
        service.validateForUpdate(ctx, portalPage1);

        assertThat(ctx.getErrorCollection(), containsFieldError("portalPageName", "admin.errors.portalpages.must.specify.name"));
    }

    @Test
    public void validatePortalPageForUpdateNotInDatabaseReturnsErrors() {
        final JiraServiceContext ctx = createContext();

        portalPage1 = PortalPage.portalPage(portalPage1).permissions(SharedEntity.SharePermissions.PRIVATE).build();

        when(portalPageManager.getPortalPageById(portalPage1.getId())).thenReturn(null);

        final PortalPageService service = createPortalPageService();
        service.validateForUpdate(ctx, portalPage1);

        assertThat(ctx.getErrorCollection(), containsSystemError("admin.errors.portalpages.not.saved"));
    }

    @Test
    public void validatePortalPageForUpdateNotOwnerReturnsErrorMessage() {
        final ApplicationUser owner = new MockApplicationUser("user");
        portalPage1 = PortalPage.id(1L).name("one").description("one description").owner(owner).build();

        final JiraServiceContext ctx = createContext();

        portalPage1 = PortalPage.portalPage(portalPage1).permissions(SharedEntity.SharePermissions.PRIVATE).build();

        final PortalPageService service = createPortalPageService();
        service.validateForUpdate(ctx, portalPage1);

        assertThat(ctx.getErrorCollection(), containsSystemError("admin.errors.portalpages.must.be.owner"));
    }

    @Test
    public void validatePortalPageForUpdateWithValidPortalPageReturnsNoErrors() {
        final JiraServiceContext ctx = new JiraServiceContextImpl(user);

        when(portalPageManager.getPortalPageById(portalPage1.getId())).thenReturn(portalPage1);

        when(portalPageManager.getPortalPageByName(user, portalPage1.getName())).thenReturn(null);

        portalPage1 = PortalPage.portalPage(portalPage1).permissions(SharedEntity.SharePermissions.PRIVATE).build();

        final PortalPageService service = createPortalPageService();
        service.validateForUpdate(ctx, portalPage1);

        assertThat(ctx.getErrorCollection(), isEmpty());
    }

    @Test
    public void validatePortalPageForUpdateWithBadNameAndSavedReturnsErrors() {
        final JiraServiceContext ctx = createContext();

        PortalPage newRequest = PortalPage.id(1L).name("testValidatePortalPageWithBadNameAndSaved").owner(user).build();
        final PortalPage oldRequest = PortalPage.id(2L).name("crapName").owner(user).build();

        when(portalPageManager.getPortalPageById(newRequest.getId())).thenReturn(newRequest);

        when(portalPageManager.getPortalPageByName(user, newRequest.getName())).thenReturn(oldRequest);

        newRequest = PortalPage.portalPage(newRequest).permissions(SharedEntity.SharePermissions.PRIVATE).build();

        final PortalPageService service = createPortalPageService();
        service.validateForUpdate(ctx, newRequest);

        assertThat(ctx.getErrorCollection(), containsFieldError("portalPageName", "admin.errors.portalpages.same.name"));
    }

    @Test
    public void validatePortalPageForUpdateUpdateNameReturnsNoErrors() {
        final JiraServiceContext ctx = createContext();

        final PortalPage oldRequest = PortalPage.id(1L).name("crapName").owner(user).build();

        when(portalPageManager.getPortalPageById(portalPage1.getId())).thenReturn(portalPage1);
        when(portalPageManager.getPortalPageByName(user, portalPage1.getName())).thenReturn(oldRequest);

        portalPage1 = PortalPage.portalPage(portalPage1).permissions(SharedEntity.SharePermissions.PRIVATE).build();

        final PortalPageService service = createPortalPageService();
        service.validateForUpdate(ctx, portalPage1);

        assertThat(ctx.getErrorCollection(), isEmpty());
    }

    @Test
    public void validateForUpdateSystemDefaultPageNoPermissionReturnsErrors() {
        final JiraServiceContext ctx = createContext();

        when(permissionManager.hasPermission(Permissions.ADMINISTER, user)).thenReturn(false);

        final PortalPageService service = createPortalPageService();
        final boolean ok = service.validateForUpdate(ctx, portalPageSystemDefault);

        assertThat(ok, is(false));
        assertThat(ctx.getErrorCollection(), containsSystemError("admin.errors.portalpages.must.be.admin.change.sysdefault"));
    }

    @Test
    public void validateForUpdateSystemDefaultPageAllOK() {
        final JiraServiceContext ctx = createContext();

        portalPageSystemDefault = PortalPage.portalPage(portalPageSystemDefault).permissions(SharedEntity.SharePermissions.GLOBAL).build();

        when(permissionManager.hasPermission(Permissions.ADMINISTER, user)).thenReturn(true);

        final PortalPageService service = createPortalPageService();
        final boolean ok = service.validateForUpdate(ctx, portalPageSystemDefault);

        assertThat(ok, is(true));
        assertThat(ctx.getErrorCollection(), isEmpty());
    }

    @Test(expected = IllegalArgumentException.class)
    public void validatePortalPageForCreateWithNullCtxThrowsException() {
        final PortalPageService service = createPortalPageService();
        service.validateForCreate(null, portalPage1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void validatePortalPageForCreateWithNullRequestThrowsException() {
        final JiraServiceContext ctx = new JiraServiceContextImpl(user);
        final PortalPageService service = createPortalPageService();
        service.validateForCreate(ctx, null);
    }

    @Test
    public void validatePortalPageForCreateWithNullUserReturnsErrorMessage() {
        final JiraServiceContext ctx = createContext(null);

        portalPage1 = PortalPage.portalPage(portalPage1).permissions(SharedEntity.SharePermissions.PRIVATE).build();

        final PortalPageService service = createPortalPageService();
        service.validateForCreate(ctx, portalPage1);

        assertThat(ctx.getErrorCollection(), containsSystemError("admin.errors.portalpages.owned.anonymous.user"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void validateForDeleteWithNullCtxThrowsException() {
        final PortalPageService service = createPortalPageService();
        service.validateForDelete(null, portalPage1.getId());
    }

    @Test(expected = IllegalArgumentException.class)
    public void validateForDeleteWithNullIdThrowsException() {
        final PortalPageService service = createPortalPageService();
        service.validateForDelete(createContext(), null);
    }

    @Test
    public void validateForDeleteWithAnonymousUserReturnsErrorMessage() {
        final PortalPageService service = createPortalPageService();
        final JiraServiceContext serviceContext = createContext(null);

        service.validateForDelete(serviceContext, portalPage1.getId());

        assertThat(serviceContext.getErrorCollection(), containsSystemError("admin.errors.portalpages.owned.anonymous.user"));
    }

    @Test
    public void validateForDeleteNonExistingPortalPageReturnsErrorMessage() {
        when(portalPageManager.getPortalPageById(portalPage1.getId())).thenReturn(null);

        final PortalPageService service = createPortalPageService();
        final JiraServiceContext serviceContext = createContext();

        service.validateForDelete(serviceContext, portalPage1.getId());

        assertThat(serviceContext.getErrorCollection(), containsSystemError("admin.errors.portalpages.nonexistent"));
    }

    @Test
    public void validateForDeleteCantDeleteSystemDefaultPageReturnsErrorMessage() {
        when(portalPageManager.getPortalPageById(portalPageSystemDefault.getId())).thenReturn(portalPageSystemDefault);

        final PortalPageService service = createPortalPageService();
        final JiraServiceContext serviceContext = createContext();

        service.validateForDelete(serviceContext, portalPageSystemDefault.getId());

        assertThat(serviceContext.getErrorCollection(), containsSystemError("admin.errors.portalpages.not.delete.system.default"));
    }

    @Test
    public void validateForDeleteNonOwnedReturnsErrorMessage() {
        final ApplicationUser otherUser = new MockApplicationUser("otherUser");
        final PortalPage otherPage = PortalPage.name("name").description("desc").owner(otherUser).build();

        when(portalPageManager.getPortalPageById(portalPage1.getId())).thenReturn(otherPage);

        final PortalPageService service = createPortalPageService();
        final JiraServiceContext serviceContext = createContext();

        service.validateForDelete(serviceContext, portalPage1.getId());

        assertThat(serviceContext.getErrorCollection(), containsSystemError("admin.errors.portalpages.must.be.owner"));
    }

    @Test
    public void validateForDeleteReturnsNoErrors() {
        when(portalPageManager.getPortalPageById(portalPage1.getId())).thenReturn(portalPage1);

        final PortalPageService service = createPortalPageService();
        final JiraServiceContext serviceContext = createContext();

        service.validateForDelete(serviceContext, portalPage1.getId());

        assertThat(serviceContext.getErrorCollection(), isEmpty());
    }

    @Test(expected = IllegalArgumentException.class)
    public void createPortalPageNullCtxThrowsException() {
        final PortalPageService service = createPortalPageService();
        service.createPortalPage(null, portalPage1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void createPortalPageNullPortalPageThrowsException() {
        final PortalPageService service = createPortalPageService();
        service.createPortalPage(createContext(), null);
    }

    @Test
    public void createPortalPageAsAnonymousReturnsErrorMessage() {
        final JiraServiceContext context = createContext(null);

        final PortalPageService service = createPortalPageService();

        service.createPortalPage(context, portalPage1);

        assertThat(context.getErrorCollection(), containsSystemError("admin.errors.portalpages.owned.anonymous.user"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void createPortalPageFavouriteWithNullCtxThrowsException() {
        final PortalPageService service = createPortalPageService();
        service.createPortalPage(null, portalPage1, true);
    }

    @Test(expected = IllegalArgumentException.class)
    public void createPortalPageFavouriteWithNullPortalPageThrowsException() {
        final PortalPageService service = createPortalPageService();
        service.createPortalPage(createContext(), null, true);
    }

    @Test
    public void createPortalPageFavouriteAsAnonymousReturnsErrorMessage() {
        final JiraServiceContext context = createContext(null);

        final PortalPageService service = createPortalPageService();

        service.createPortalPage(context, portalPage1, true);

        assertThat(context.getErrorCollection(), containsSystemError("admin.errors.portalpages.owned.anonymous.user"));
    }

    /*
    * ===================================================================================== CREATE CLONE
    * =====================================================================================
    */

    @Test(expected = IllegalArgumentException.class)
    public void createClonePortalPageNullCtxThrowsException() {
        final PortalPageService service = createPortalPageService();
        service.createPortalPageByClone(null, portalPage1, CLONE_ID, false);
    }

    @Test(expected = IllegalArgumentException.class)
    public void createClonePortalPageNullPortalPageThrowsException() {
        final PortalPageService service = createPortalPageService();
        service.createPortalPageByClone(createContext(), null, CLONE_ID, false);
    }

    @Test(expected = IllegalArgumentException.class)
    public void createClonePortalPageNullCloneIdThrowsException() {
        final PortalPageService service = createPortalPageService();
        service.createPortalPageByClone(createContext(), portalPage1, null, false);
    }

    @Test
    public void createClonePortalPageAsAnonymousReturnsErrorMessage() {
        final JiraServiceContext context = createContext(null);

        final PortalPageService service = createPortalPageService();

        service.createPortalPageByClone(context, portalPage1, CLONE_ID, false);

        assertThat(context.getErrorCollection(), containsSystemError("admin.errors.portalpages.owned.anonymous.user"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void createClonePortalPageFavouriteWithNullCtxThrowsException() {
        final PortalPageService service = createPortalPageService();
        service.createPortalPageByClone(null, portalPage1, CLONE_ID, true);
    }

    @Test(expected = IllegalArgumentException.class)
    public void createClonePortalPageFavouriteWithNullPortalPageThrowsException() {
        final PortalPageService service = createPortalPageService();
        service.createPortalPageByClone(createContext(), null, CLONE_ID, true);
    }

    @Test
    public void createClonePortalPageFavouriteAsAnonymousReturnsErrorMessage() {
        final JiraServiceContext context = createContext(null);

        final PortalPageService service = createPortalPageService();

        service.createPortalPageByClone(context, portalPage1, CLONE_ID, true);

        assertThat(context.getErrorCollection(), containsSystemError("admin.errors.portalpages.owned.anonymous.user"));
    }

    @Test
    public void updatePortalPageReturnsSamePortalPageInstance() {
        final JiraServiceContext ctx = createContext();

        when(portalPageManager.getPortalPageById(portalPage1.getId())).thenReturn(portalPage1);
        when(portalPageManager.getPortalPageByName(user, portalPage1.getName())).thenReturn(null);
        when(portalPageManager.update(portalPage1)).thenReturn(portalPage2);

        final PortalPageService service = createPortalPageService();

        final PortalPage result = service.updatePortalPage(ctx, portalPage1, false);

        assertThat(result, sameInstance(portalPage2));
        assertThat(ctx.getErrorCollection(), isEmpty());

        verify(favouritesManager).removeFavourite(user, portalPage2);
    }

    @Test
    public void updateSystemDefaultPortalPageReturnsSamePortalPageInstance() throws Exception {
        final JiraServiceContext ctx = createContext();

        portalPageSystemDefault = PortalPage.portalPage(portalPageSystemDefault).permissions(SharedEntity.SharePermissions.GLOBAL).build();

        when(permissionManager.hasPermission(Permissions.ADMINISTER, user)).thenReturn(true);
        when(portalPageManager.update(portalPageSystemDefault)).thenReturn(portalPage2);

        final PortalPageService service = createPortalPageService();

        final PortalPage result = service.updatePortalPage(ctx, portalPageSystemDefault, false);

        assertThat(result, sameInstance(portalPage2));
        assertThat(ctx.getErrorCollection(), isEmpty());

        verify(favouritesManager).removeFavourite(user, portalPage2);
    }

    @Test(expected = IllegalArgumentException.class)
    public void updatePortalPageNullCtxThrowsException() {
        final PortalPageService service = createPortalPageService();
        service.updatePortalPage(null, portalPage1, false);
    }

    @Test(expected = IllegalArgumentException.class)
    public void updatePortalPageNullPortalPageThrowsException() {
        final PortalPageService service = createPortalPageService();
        service.updatePortalPage(createContext(), null, false);
    }

    @Test
    public void updatePortalPageAsAnonymousReturnsErrorMessage() {
        final JiraServiceContext context = createContext(null);

        final PortalPageService service = createPortalPageService();

        service.updatePortalPage(context, portalPage1, false);

        assertThat(context.getErrorCollection(), containsSystemError("admin.errors.portalpages.owned.anonymous.user"));
    }

    @Test
    public void updatePortalPageNotNewOwnerReturnsErrorMessage() {
        final ApplicationUser notMe = new MockApplicationUser("notMe");
        final JiraServiceContext context = createContext(notMe);

        final PortalPageService service = createPortalPageService();

        service.updatePortalPage(context, portalPage1, false);

        assertThat(context.getErrorCollection(), containsSystemError("admin.errors.portalpages.must.be.owner"));
    }

    @Test
    public void updatePortalPageNotInDbReturnsErrorMessage() {
        final JiraServiceContext context = createContext();

        when(portalPageManager.getPortalPageById(portalPage1.getId())).thenReturn(null);

        final PortalPageService service = createPortalPageService();

        service.updatePortalPage(context, portalPage1, false);

        assertThat(context.getErrorCollection(), containsSystemError("admin.errors.portalpages.not.saved"));
    }

    @Test
    public void updatePortalPageNotOwnerInDbReturnsErrorMessage() {
        final JiraServiceContext context = createContext();

        when(portalPageManager.getPortalPageById(portalPage1.getId())).thenReturn(portalPageNotOwnedByUser);

        final PortalPageService service = createPortalPageService();

        service.updatePortalPage(context, portalPage1, false);

        assertThat(context.getErrorCollection(), containsSystemError("admin.errors.portalpages.must.be.owner"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void updatePortalPageFavouriteWithNullCtxThrowsException() {
        final PortalPageService service = createPortalPageService();
        service.updatePortalPage(null, portalPage1, true);
    }

    @Test(expected = IllegalArgumentException.class)
    public void updatePortalPageFavouriteWithNullPortalPageThrowsException() {
        final PortalPageService service = createPortalPageService();
        service.updatePortalPage(createContext(), null, true);
    }

    @Test
    public void updatePortalPageFavouriteNotExistingReturnsErrorMessage() {
        final JiraServiceContext context = createContext();

        when(portalPageManager.getPortalPageById(portalPage2.getId())).thenReturn(null);

        final PortalPageService service = createPortalPageService();

        service.updatePortalPage(context, portalPage2, true);

        assertThat(context.getErrorCollection(), containsSystemError("admin.errors.portalpages.not.saved"));
    }

    @Test
    public void updatePortalPageFavouriteAsAnonymousReturnsErrorMessage() {
        final JiraServiceContext context = createContext(null);

        final PortalPageService service = createPortalPageService();

        service.updatePortalPage(context, portalPage1, true);

        assertThat(context.getErrorCollection(), containsSystemError("admin.errors.portalpages.owned.anonymous.user"));
    }

    @Test
    public void updatePortalPageFavouriteNotNewOwnerReturnsErrorMessage() {
        MockApplicationUser notMe = new MockApplicationUser("notMe");
        final JiraServiceContext context = createContext(notMe);

        final PortalPageService service = createPortalPageService();

        service.updatePortalPage(context, portalPage1, true);

        assertThat(context.getErrorCollection(), containsSystemError("admin.errors.portalpages.must.be.owner"));
    }

    @Test
    public void updatePortalPageFavouriteNotInDbReturnsErrorMessage() {
        final JiraServiceContext context = createContext();

        when(portalPageManager.getPortalPageById(portalPage1.getId())).thenReturn(null);

        final PortalPageService service = createPortalPageService();

        service.updatePortalPage(context, portalPage1, false);

        assertThat(context.getErrorCollection(), containsSystemError("admin.errors.portalpages.not.saved"));
    }

    @Test
    public void updatePortalPageFavouriteWithAddFavourite() throws PermissionException {
        final JiraServiceContext ctx = createContext();

        when(portalPageManager.getPortalPageById(portalPage1.getId())).thenReturn(portalPage1);
        when(portalPageManager.getPortalPageByName(user, portalPage1.getName())).thenReturn(null);
        when(portalPageManager.update(portalPage1)).thenReturn(portalPage1);

        final PortalPageService service = createPortalPageService();
        final PortalPage resultPortalPage = service.updatePortalPage(ctx, portalPage1, true);

        assertThat(resultPortalPage, sameInstance(portalPage1));
        assertThat(ctx.getErrorCollection(), isEmpty());

        verify(favouritesManager).addFavourite(user, portalPage1);
    }

    @Test
    public void updatePortalPageFavouriteWithAddFavouriteErrorReturnsErrorMessage() throws PermissionException {
        final JiraServiceContext ctx = createContext();

        when(portalPageManager.getPortalPageById(portalPage1.getId())).thenReturn(portalPage1);
        when(portalPageManager.getPortalPageByName(user, portalPage1.getName())).thenReturn(null);
        when(portalPageManager.update(portalPage1)).thenReturn(portalPage1);

        doThrow(new PermissionException("ahhhh")).when(favouritesManager).addFavourite(user, portalPage1);

        final PortalPageService service = createPortalPageService();
        final PortalPage resultPortalPage = service.updatePortalPage(ctx, portalPage1, true);

        assertThat(resultPortalPage, sameInstance(portalPage1));
        assertThat(ctx.getErrorCollection(), not(isEmpty()));
    }

    @Test
    public void updatePortalPageFavouriteWithRemoveFavourite() throws PermissionException {
        final JiraServiceContext ctx = createContext();

        when(portalPageManager.getPortalPageById(portalPage1.getId())).thenReturn(portalPage1);
        when(portalPageManager.getPortalPageByName(user, portalPage1.getName())).thenReturn(null);
        when(portalPageManager.update(portalPage1)).thenReturn(portalPage1);

        final PortalPageService service = createPortalPageService();
        final PortalPage resultPortalPage = service.updatePortalPage(ctx, portalPage1, false);

        assertThat(resultPortalPage, sameInstance(portalPage1));
        assertThat(ctx.getErrorCollection(), isEmpty());

        verify(favouritesManager).removeFavourite(user, portalPage1);
    }

    @Test
    public void getSystemDefaultPage() throws Exception {
        when(portalPageManager.getSystemDefaultPortalPage()).thenReturn(portalPage1);

        final PortalPageService service = createPortalPageService();
        final PortalPage systemDefault = service.getSystemDefaultPortalPage();

        assertThat(systemDefault, is(portalPage1));
    }

    @Test
    public void validateForChangePortalPageSequenceAnonymousReturnsErrorMessage() {
        final JiraServiceContext ctx = createContext(null);

        final PortalPageService pps = createPortalPageService();

        final boolean actualResult = pps.validateForChangePortalPageSequence(ctx, portalPage1.getId());

        assertThat(actualResult, is(false));
        assertThat(ctx.getErrorCollection(), containsSystemError("admin.errors.portalpages.owned.anonymous.user"));
    }

    @Test
    public void validateForChangePortalPageSequencePageNotExistReturnsErrorMessage() {
        final JiraServiceContext ctx = createContext();

        when(portalPageManager.getPortalPage(user, portalPage2.getId())).thenReturn(null);

        final PortalPageService pps = createPortalPageService();
        final boolean actualResult = pps.validateForChangePortalPageSequence(ctx, portalPage2.getId());

        assertThat(actualResult, is(false));
        assertThat(ctx.getErrorCollection(), containsSystemError("admin.errors.portalpages.no.access"));
    }

    @Test
    public void validateForChangePortalPageSequenceNotFavouriteReturnsErrorMessage() throws PermissionException {
        final JiraServiceContext ctx = createContext(notTheOwner);

        when(portalPageManager.getPortalPage(notTheOwner, portalPage2.getId())).thenReturn(portalPage2);
        when(favouritesManager.isFavourite(notTheOwner, portalPage2)).thenReturn(false);

        final PortalPageService pps = createPortalPageService();
        final boolean actualResult = pps.validateForChangePortalPageSequence(ctx, portalPage2.getId());

        assertThat(actualResult, is(false));
        assertThat(ctx.getErrorCollection(), containsSystemError("admin.errors.portalpages.not.favourite"));
    }

    @Test
    public void validateForChangePortalPageSequenceFavouriteErrorReturnsErrorMessage() throws PermissionException {
        final JiraServiceContext ctx = createContext(notTheOwner);

        when(portalPageManager.getPortalPage(notTheOwner, portalPage2.getId())).thenReturn(portalPage2);
        when(favouritesManager.isFavourite(notTheOwner, portalPage2)).thenThrow(new PermissionException("some exception"));

        final PortalPageService pps = createPortalPageService();
        final boolean actualResult = pps.validateForChangePortalPageSequence(ctx, portalPage2.getId());

        assertThat(actualResult, is(false));
        assertThat(ctx.getErrorCollection(), containsSystemError("admin.errors.portalpages.not.favourite"));
    }

    @Test
    public void validateForChangePortalPageSequenceGood() throws PermissionException {
        final JiraServiceContext ctx = createContext(notTheOwner);

        when(portalPageManager.getPortalPage(notTheOwner, portalPage2.getId())).thenReturn(portalPage2);
        when(favouritesManager.isFavourite(notTheOwner, portalPage2)).thenReturn(true);

        final PortalPageService pps = createPortalPageService();
        final boolean actualResult = pps.validateForChangePortalPageSequence(ctx, portalPage2.getId());

        assertThat(actualResult, is(true));
        assertThat(ctx.getErrorCollection(), isEmpty());
    }

    // INCREASE
    //
    @Test
    public void increaseSequenceExisting() throws Exception {
        sequenceNotExisting(new IncreaseClosure());
    }

    @Test
    public void increaseSequenceAsAnonymous() throws Exception {
        sequenceAsAnonymous(new IncreaseClosure());
    }

    @Test
    public void increaseSequenceNotFavourite() throws Exception {
        sequenceNotFavourite(new IncreaseClosure());
    }

    @Test
    public void increaseSequenceOK() throws Exception {
        sequenceOK(new OkIncreaseClosure());
    }//

    // DECREASE
    //
    @Test
    public void decreaseSequenceExisting() throws Exception {
        sequenceNotExisting(new DecreaseClosure());
    }

    @Test
    public void decreaseSequenceAsAnonymous() throws Exception {
        sequenceAsAnonymous(new DecreaseClosure());
    }

    @Test
    public void decreaseSequenceNotFavourite() throws Exception {
        sequenceNotFavourite(new DecreaseClosure());
    }

    @Test
    public void decreaseSequenceOK() throws Exception {
        sequenceOK(new OkDecreaseClosure());
    }//

    // MOVE TO START
    //
    @Test
    public void moveToStartSequenceExisting() throws Exception {
        sequenceNotExisting(new StartClosure());
    }

    @Test
    public void moveToStartSequenceAsAnonymous() throws Exception {
        sequenceAsAnonymous(new StartClosure());
    }

    @Test
    public void moveToStartSequenceNotFavourite() throws Exception {
        sequenceNotFavourite(new StartClosure());
    }

    @Test
    public void moveToStartSequenceOK() throws Exception {
        sequenceOK(new OkStartClosure());
    }//

    // MOVE TO END
    //
    @Test
    public void moveToEndSequenceExisting() throws Exception {
        sequenceNotExisting(new EndClosure());
    }

    @Test
    public void moveToEndSequenceAsAnonymous() throws Exception {
        sequenceAsAnonymous(new EndClosure());
    }

    @Test
    public void moveToEndSequenceNotFavourite() throws Exception {
        sequenceNotFavourite(new EndClosure());
    }

    @Test
    public void moveToEndSequenceOK() throws Exception {
        sequenceOK(new OkEndClosure());
    }

    private void sequenceNotExisting(final ReorderCommand reorderCommand) {
        final JiraServiceContext context = createContext();

        when(portalPageManager.getPortalPage(user, portalPage2.getId())).thenReturn(null);

        final PortalPageService service = createPortalPageService();

        reorderCommand.execute(service, context, portalPage2);

        assertThat(context.getErrorCollection(), containsSystemError("admin.errors.portalpages.no.access"));
    }

    private void sequenceAsAnonymous(final ReorderCommand reorderCommand) {
        final JiraServiceContext context = createContext(null);

        final PortalPageService service = createPortalPageService();

        reorderCommand.execute(service, context, portalPage1);

        assertThat(context.getErrorCollection(), containsSystemError("admin.errors.portalpages.owned.anonymous.user"));
    }

    private void sequenceNotFavourite(final ReorderCommand reorderCommand) throws PermissionException {
        final JiraServiceContext context = createContext(notTheOwner);

        when(portalPageManager.getPortalPage(notTheOwner, portalPage1.getId())).thenReturn(portalPage1);

        when(favouritesManager.isFavourite(notTheOwner, portalPage1)).thenReturn(false);

        final PortalPageService service = createPortalPageService();

        reorderCommand.execute(service, context, portalPage1);

        assertThat(context.getErrorCollection(), containsSystemError("admin.errors.portalpages.not.favourite"));
    }

    private void sequenceOK(final OkReorderCommand reorderCommand) throws Exception {
        final JiraServiceContext context = createContext();

        when(portalPageManager.getPortalPage(user, portalPage1.getId())).thenReturn(portalPage1);
        when(favouritesManager.isFavourite(user, portalPage1)).thenReturn(true);

        reorderCommand.setupMockFavManager(favouritesManager);

        final PortalPageService service = createPortalPageService();

        reorderCommand.execute(service, context, portalPage1);

        assertThat(context.getErrorCollection(), isEmpty());
    }

    //what used to be professional edition unit tests
    @Test
    public void validateForUpdateSystemDefaultPageNotGloballySharedReturnsErrorMessage() {
        final JiraServiceContext ctx = createContext();

        when(permissionManager.hasPermission(Permissions.ADMINISTER, user)).thenReturn(true);

        final PortalPageService service = createPortalPageService();
        final boolean ok = service.validateForUpdate(ctx, portalPageSystemDefault);

        assertThat(ok, is(false));
        assertThat(ctx.getErrorCollection(), containsSystemError("admin.errors.portalpages.sysdefault.must.be.public"));
    }

    @Test
    public void validatePortalPageForCreateWithOtherUserReturnsErrorMessage() {
        final ApplicationUser otherUser = new MockApplicationUser("other");
        portalPage1 = PortalPage.id(1L).name("one").description("one description").owner(otherUser).build();

        final JiraServiceContext ctx = createContext();

        portalPage1 = PortalPage.portalPage(portalPage1).permissions(SharedEntity.SharePermissions.PRIVATE).build();

        when(portalPageManager.getPortalPageByName(user, portalPage1.getName())).thenReturn(null);

        final PortalPageService service = createPortalPageService();
        service.validateForCreate(ctx, portalPage1);

        assertThat(ctx.getErrorCollection(), containsSystemError("admin.errors.portalpages.must.be.owner"));
    }

    @Test
    public void validatePortalPageForCreateWithEmptyNameReturnsErrorMessage() {
        final JiraServiceContext ctx = new MockJiraServiceContext(user);
        PortalPage portalPage = PortalPage.name("").owner(user).build();
        final Set<SharePermission> permissions = Collections.<SharePermission>singleton(new SharePermissionImpl(new ShareType.Name("exampleType"), null, null));
        portalPage = PortalPage.portalPage(portalPage).permissions(new SharedEntity.SharePermissions(permissions)).build();

        final PortalPageService service = createPortalPageService();
        service.validateForCreate(ctx, portalPage);

        assertThat(ctx.getErrorCollection(), containsFieldError("portalPageName", ctx.getI18nBean().getText("admin.errors.portalpages.must.specify.name")));
    }

    @Test
    public void validatePortalPageForCreateWithValidPortalPage() {
        final JiraServiceContext ctx = new MockJiraServiceContext(user);

        when(portalPageManager.getPortalPageByName(user, portalPage1.getName())).thenReturn(null);

        final PortalPageService service = createPortalPageService();
        service.validateForCreate(ctx, portalPage1);

        assertThat(ctx.getErrorCollection(), isEmpty());
    }

    @Test
    public void validatePortalPageForCreateWithBadNameAndNotSavedReturnsErrorMessage() {
        final JiraServiceContext ctx = new MockJiraServiceContext(user);
        final PortalPage notSavedPortalPage = PortalPage.name("NotSaved").owner(user).build();

        when(portalPageManager.getPortalPageByName(user, notSavedPortalPage.getName())).thenReturn(notSavedPortalPage);

        final PortalPageService service = createPortalPageService();
        service.validateForCreate(ctx, notSavedPortalPage);

        assertThat(ctx.getErrorCollection(), containsFieldError("portalPageName", ctx.getI18nBean().getText("admin.errors.portalpages.same.name")));
    }

    @Test
    public void validatePortalPageForCreateWithBadNameAndSavedReturnsErrorMessage() {
        final JiraServiceContext ctx = new MockJiraServiceContext(user);

        final PortalPage newRequest = PortalPage.id(1L).name("testValidatePortalPageWithBadNameAndSaved").owner(user).build();
        final PortalPage oldRequest = PortalPage.id(2L).name("crapName").owner(user).build();

        when(portalPageManager.getPortalPageByName(user, newRequest.getName())).thenReturn(oldRequest);

        final PortalPageService service = createPortalPageService();
        service.validateForCreate(ctx, newRequest);

        assertThat(ctx.getErrorCollection(), containsFieldError("portalPageName", ctx.getI18nBean().getText("admin.errors.portalpages.same.name")));
    }

    @Test
    public void validatePortalPageForCreateWithPortalPageOfSameIdReturnsErrorMessage() {
        final JiraServiceContext ctx = new MockJiraServiceContext(user);

        final PortalPage newRequest = PortalPage.id(1L).name("testValidatePortalPageWithBadNameAndSaved").owner(user).build();
        final PortalPage oldRequest = PortalPage.id(2L).name("crapName").owner(user).build();

        when(portalPageManager.getPortalPageByName(user, newRequest.getName())).thenReturn(oldRequest);

        final PortalPageService service = createPortalPageService();
        service.validateForCreate(ctx, newRequest);

        assertThat(ctx.getErrorCollection(), containsFieldError("portalPageName", ctx.getI18nBean().getText("admin.errors.portalpages.same.name")));
    }

    @Test
    public void createPortalPage() {
        final JiraServiceContext ctx = createContext();

        when(portalPageManager.create(portalPage3)).thenReturn(portalPage2);

        final PortalPageService service = createPortalPageService();
        final PortalPage result = service.createPortalPage(ctx, portalPage3);

        assertThat(result, sameInstance(portalPage2));
        assertThat(ctx.getErrorCollection(), isEmpty());
    }

    @Test
    public void createPortalPageNotAsOwnerReturnsErrorMessage() {
        MockApplicationUser otherUser = new MockApplicationUser("otherUser");
        final JiraServiceContext context = createContext(otherUser);

        final PortalPageService service = createPortalPageService();

        service.createPortalPage(context, portalPage1);

        assertThat(context.getErrorCollection(), containsSystemError("admin.errors.portalpages.must.be.owner"));
    }

    @Test
    public void createPortalPageFavouriteNotAsOwnerReturnsErrorMessage() {
        MockApplicationUser otherUser = new MockApplicationUser("otherUser");
        final JiraServiceContext context = createContext(otherUser);

        final PortalPageService service = createPortalPageService();

        service.createPortalPage(context, portalPage1, true);

        assertThat(context.getErrorCollection(), containsSystemError("admin.errors.portalpages.must.be.owner"));
    }

    @Test
    public void createPortalPageFavouriteWithAddFavourite() throws PermissionException {
        final JiraServiceContext ctx = createContext();

        when(portalPageManager.create(portalPage3)).thenReturn(portalPage2);

        favouritesManager.addFavourite(user, portalPage2);

        final PortalPageService service = createPortalPageService();
        final PortalPage resultPortalPage = service.createPortalPage(ctx, portalPage3, true);

        assertThat(resultPortalPage, sameInstance(portalPage2));
        assertThat(ctx.getErrorCollection(), isEmpty());
    }

    @Test
    public void createPortalPageFavouriteWithAddFavouriteError() throws PermissionException {
        final JiraServiceContext ctx = createContext();

        when(portalPageManager.create(portalPage3)).thenReturn(portalPage2);

        doThrow(new PermissionException("ahhhh")).when(favouritesManager).addFavourite(user, portalPage2);

        final PortalPageService service = createPortalPageService();
        final PortalPage resultPortalPage = service.createPortalPage(ctx, portalPage3, true);

        assertThat(resultPortalPage, sameInstance(portalPage2));
        assertThat(ctx.getErrorCollection(), not(isEmpty()));
    }

    @Test
    public void createClonePortalPage() {
        final JiraServiceContext ctx = createContext();

        when(portalPageManager.getPortalPageByName(ctx.getLoggedInApplicationUser(), portalPage3.getName())).thenReturn(null);
        when(portalPageManager.getPortalPage(ctx.getLoggedInApplicationUser(), CLONE_ID)).thenReturn(portalPageClone);
        when(portalPageManager.createBasedOnClone(ctx.getLoggedInApplicationUser(), portalPage3, portalPageClone)).thenReturn(portalPage2);

        final PortalPageService service = createPortalPageService();
        final PortalPage result = service.createPortalPageByClone(ctx, portalPage3, CLONE_ID, false);

        assertThat(result, sameInstance(portalPage2));
        assertThat(ctx.getErrorCollection(), isEmpty());
    }

    @Test
    public void createClonePortalPageNotAsOwnerReturnsErrorMessage() {
        final ApplicationUser otherUser = new MockApplicationUser("otherUser");
        final JiraServiceContext context = createContext(otherUser);

        when(portalPageManager.getPortalPageByName(otherUser, portalPage1.getName())).thenReturn(null);

        final PortalPageService service = createPortalPageService();

        service.createPortalPageByClone(context, portalPage1, CLONE_ID, false);

        assertThat(context.getErrorCollection(), containsSystemError("admin.errors.portalpages.must.be.owner"));
    }

    @Test
    public void createClonePortalPageFavouriteNotAsOwnerReturnsErrorMessage() {
        final ApplicationUser otherUser = new MockApplicationUser("otherUser");
        final JiraServiceContext context = createContext(otherUser);

        when(portalPageManager.getPortalPageByName(otherUser, portalPage1.getName())).thenReturn(null);

        final PortalPageService service = createPortalPageService();

        service.createPortalPageByClone(context, portalPage1, CLONE_ID, true);

        assertThat(context.getErrorCollection(), containsSystemError("admin.errors.portalpages.must.be.owner"));
    }

    @Test
    public void createClonePortalPageFavouriteWithAddFavourite() throws PermissionException {
        final JiraServiceContext ctx = createContext();

        when(portalPageManager.getPortalPageByName(user, portalPage3.getName())).thenReturn(null);
        when(portalPageManager.getPortalPage(user, CLONE_ID)).thenReturn(portalPageClone);
        when(portalPageManager.createBasedOnClone(user, portalPage3, portalPageClone)).thenReturn(portalPage2);

        favouritesManager.addFavourite(user, portalPage2);

        final PortalPageService service = createPortalPageService();
        final PortalPage resultPortalPage = service.createPortalPageByClone(ctx, portalPage3, CLONE_ID, true);

        assertThat(resultPortalPage, sameInstance(portalPage2));
        assertThat(ctx.getErrorCollection(), isEmpty());
    }

    @Test
    public void createClonePortalPageFavouriteWithAddFavouriteError() throws PermissionException {
        final JiraServiceContext ctx = createContext();

        when(portalPageManager.getPortalPageByName(user, portalPage3.getName())).thenReturn(null);
        when(portalPageManager.getPortalPage(user, CLONE_ID)).thenReturn(portalPageClone);
        when(portalPageManager.createBasedOnClone(user, portalPage3, portalPageClone)).thenReturn(portalPage2);
        doThrow(new PermissionException("ahhhh")).when(favouritesManager).addFavourite(user, portalPage2);

        final PortalPageService service = createPortalPageService();
        final PortalPage resultPortalPage = service.createPortalPageByClone(ctx, portalPage3, CLONE_ID, true);

        assertThat(resultPortalPage, sameInstance(portalPage2));
        assertThat(ctx.getErrorCollection(), not(isEmpty()));
    }

    @Test
    public void validateClonePageDoesNotExistReturnsErrorMessage() throws Exception {
        final JiraServiceContext ctx = createContext();

        when(portalPageManager.getPortalPageByName(user, portalPage3.getName())).thenReturn(null);
        when(portalPageManager.getPortalPage(user, CLONE_ID)).thenReturn(null);

        final PortalPageService service = createPortalPageService();
        final boolean ok = service.validateForCreatePortalPageByClone(ctx, portalPage3, CLONE_ID);

        assertThat(ok, is(false));
        assertThat(ctx.getErrorCollection(), containsSystemError("admin.errors.portalpages.clone.does.not.exist"));
    }

    @Test
    public void validateClonePageMultipleCloningAllowedOnProAndEnt() throws Exception {
        final JiraServiceContext ctx = createContext();

        when(portalPageManager.getPortalPageByName(user, portalPage3.getName())).thenReturn(null);
        when(portalPageManager.getPortalPage(user, CLONE_ID)).thenReturn(portalPageClone);

        final PortalPageService service = createPortalPageService();
        final boolean ok = service.validateForCreatePortalPageByClone(ctx, portalPage3, CLONE_ID);

        assertThat(ok, is(true));
        assertThat(ctx.getErrorCollection(), isEmpty());
    }

    @Test
    public void validatePortalPageForCreateLongDescriptionReturnsErrorMessage() {
        final JiraServiceContext ctx = createContext(user);

        portalPage1 = PortalPage.portalPage(portalPage1).description("Really long descriptionReally long descriptionReally long descriptionReally long descriptionReally long descriptionReally long descriptionReally long descriptionReally long descriptionReally long descriptionReally long descriptionReally long descriptionRea").build();
        when(portalPageManager.getPortalPageByName(user, portalPage1.getName())).thenReturn(null);

        final PortalPageService service = createPortalPageService();
        service.validateForCreate(ctx, portalPage1);

        assertThat(ctx.getErrorCollection(), containsFieldError("portalPageDescription", "admin.errors.portalpages.description.too.long"));
    }

    @Test
    public void validatePortalPageForUpdateLongDescriptionReturnsErrorMessage() {
        final JiraServiceContext ctx = createContext(user);

        portalPage1 = PortalPage.portalPage(portalPage1).description("Really long descriptionReally long descriptionReally long descriptionReally long descriptionReally long descriptionReally long descriptionReally long descriptionReally long descriptionReally long descriptionReally long descriptionReally long descriptionRea").build();
        when(portalPageManager.getPortalPageById(1L)).thenReturn(portalPage1);
        when(portalPageManager.getPortalPageByName(user, portalPage1.getName())).thenReturn(null);

        final PortalPageService service = createPortalPageService();
        service.validateForUpdate(ctx, portalPage1);

        assertThat(ctx.getErrorCollection(), containsFieldError("portalPageDescription", "admin.errors.portalpages.description.too.long"));
    }

    interface ReorderCommand {
        void execute(PortalPageService service, JiraServiceContext context, PortalPage portalPage);
    }

    interface OkReorderCommand extends ReorderCommand {
        void setupMockFavManager(FavouritesManager<PortalPage> favouritesManager) throws Exception;
    }

    private static class IncreaseClosure implements ReorderCommand {
        public void execute(final PortalPageService service, final JiraServiceContext context, final PortalPage portalPage) {
            service.increasePortalPageSequence(context, portalPage.getId());
        }
    }

    private static class DecreaseClosure implements ReorderCommand {
        public void execute(final PortalPageService service, final JiraServiceContext context, final PortalPage portalPage) {
            service.decreasePortalPageSequence(context, portalPage.getId());
        }
    }

    private static class StartClosure implements ReorderCommand {
        public void execute(final PortalPageService service, final JiraServiceContext context, final PortalPage portalPage) {
            service.moveToStartPortalPageSequence(context, portalPage.getId());
        }
    }

    private static class EndClosure implements ReorderCommand {
        public void execute(final PortalPageService service, final JiraServiceContext context, final PortalPage portalPage) {
            service.moveToEndPortalPageSequence(context, portalPage.getId());
        }
    }

    private class OkIncreaseClosure extends IncreaseClosure implements OkReorderCommand {
        public void setupMockFavManager(final FavouritesManager<PortalPage> favouritesManager)
                throws Exception {
            favouritesManager.increaseFavouriteSequence(user, portalPage1);
        }
    }

    private class OkDecreaseClosure extends DecreaseClosure implements OkReorderCommand {
        public void setupMockFavManager(final FavouritesManager<PortalPage> favouritesManager)
                throws Exception {
            favouritesManager.decreaseFavouriteSequence(user, portalPage1);
        }
    }

    private class OkStartClosure extends StartClosure implements OkReorderCommand {
        public void setupMockFavManager(final FavouritesManager<PortalPage> favouritesManager)
                throws Exception {
            favouritesManager.moveToStartFavouriteSequence(user, portalPage1);
        }
    }

    private class OkEndClosure extends EndClosure implements OkReorderCommand {
        public void setupMockFavManager(final FavouritesManager<PortalPage> favouritesManager)
                throws Exception {
            favouritesManager.moveToEndFavouriteSequence(user, portalPage1);
        }
    }
}
