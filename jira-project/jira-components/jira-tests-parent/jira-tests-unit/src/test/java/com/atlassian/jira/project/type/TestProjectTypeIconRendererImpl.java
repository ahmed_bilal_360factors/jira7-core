package com.atlassian.jira.project.type;

import com.atlassian.fugue.Option;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Optional;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestProjectTypeIconRendererImpl {
    private static final String INACCESSIBLE_ICON = "inaccessible-icon";

    @Mock
    private ProjectTypeManager projectTypeManager;

    private ProjectTypeIconRendererImpl renderer;

    @Before
    public void setUp() {
        renderer = new ProjectTypeIconRendererImpl(projectTypeManager);
        mockInaccessibleTypeIcon();
    }

    @Test
    public void returnsTheInaccessibleProjectTypeIconIfThePassedKeyIsNull() {
        Optional<String> iconToRender = renderer.getIconToRender(null);

        assertThat(iconToRender.get(), is(INACCESSIBLE_ICON));
    }

    @Test
    public void returnsTheInaccessibleProjectTypeIconIfTheKeyIsEmpty() {
        Optional<String> iconToRender = renderer.getIconToRender(new ProjectTypeKey(null));

        assertThat(iconToRender.get(), is(INACCESSIBLE_ICON));
    }

    @Test
    public void returnsTheProjectTypeIconIfItIsInstalled() {
        ProjectTypeKey key = new ProjectTypeKey("key");
        projectTypeWithKeyIsInstalledAndHasIcon(key, "expected-icon");

        Optional<String> iconToRender = renderer.getIconToRender(key);

        assertThat(iconToRender.get(), is("expected-icon"));
    }

    @Test
    public void returnsTheInaccessibleProjectTypeIfItIsNotInstalled() {
        ProjectTypeKey key = new ProjectTypeKey("key");
        projectTypeWithKeyIsUninstalled(key);

        Optional<String> iconToRender = renderer.getIconToRender(key);

        assertThat(iconToRender.get(), is(INACCESSIBLE_ICON));
    }

    private ProjectTypeKey anyKey() {
        return mock(ProjectTypeKey.class);
    }

    private void mockInaccessibleTypeIcon() {
        when(projectTypeManager.getInaccessibleProjectType()).thenReturn(projectTypeWithIcon(INACCESSIBLE_ICON));
    }

    private void projectTypeWithKeyIsUninstalled(ProjectTypeKey key) {
        when(projectTypeManager.getByKey(key)).thenReturn(Option.none());
    }

    private void projectTypeWithKeyIsInstalledAndHasIcon(ProjectTypeKey key, String icon) {
        when(projectTypeManager.getByKey(key)).thenReturn(Option.some(projectTypeWithIcon(icon)));
    }

    private ProjectType projectTypeWithIcon(final String icon) {
        return new ProjectType(new ProjectTypeKey("key"), "desc", icon, "color", 1);
    }
}
