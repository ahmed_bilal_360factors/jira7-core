package com.atlassian.jira.portal;

import com.atlassian.jira.junit.rules.MockitoContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.sharing.SharedEntityColumn;
import com.atlassian.jira.sharing.index.SharedEntityIndexer;
import com.atlassian.jira.sharing.search.SharedEntitySearchParameters;
import com.atlassian.jira.sharing.search.SharedEntitySearchParametersBuilder;
import com.atlassian.jira.sharing.search.SharedEntitySearchResult;
import com.atlassian.jira.sharing.search.SharedEntitySearcher;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.util.collect.MockCloseableIterable;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.util.Collections;
import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

/**
 * Test the searching methods of the {@link com.atlassian.jira.portal.PortalPageManager}.
 *
 * @since v3.13
 */
public class TestDefaultPortalPageManagerSearching {
    @Rule
    public final MockitoContainer initMockito = MockitoMocksInContainer.rule(this);

    @Mock
    private SharedEntitySearcher searcher;
    @Mock
    private SharedEntityIndexer indexer;

    private MockApplicationUser user;
    private PortalPage portalPage1;
    private PortalPage portalPage2;

    @InjectMocks
    private DefaultPortalPageManager portalPageManager;

    @Before
    public void setUp() throws Exception {
        user = new MockApplicationUser("testSearchAsUser");

        portalPage1 = PortalPage.id(1L).name("one").description("one description").owner(user).build();
        portalPage2 = PortalPage.id(2L).name("two").description("two description").owner(user).build();

        when(indexer.getSearcher(PortalPage.ENTITY_TYPE)).thenReturn(searcher);
    }

    /**
     * Check what happens when null search parameters are passed.
     */
    @Test(expected = IllegalArgumentException.class)
    public void searchNullParametersThrowsException() {
        portalPageManager.search(null, (ApplicationUser) null, 0, 10);
    }

    /**
     * Check what happens when zero width is passed.
     */
    @Test(expected = IllegalArgumentException.class)
    public void searchZeroWidthThrowsException() {
        portalPageManager.search(new SharedEntitySearchParametersBuilder().toSearchParameters(), (ApplicationUser) null, 0, 0);
    }

    /**
     * Check what happens on illegal offset
     */
    @Test(expected = IllegalArgumentException.class)
    public void searchIllegalOffsetThrowsException() {
        portalPageManager.search(new SharedEntitySearchParametersBuilder().toSearchParameters(), (ApplicationUser) null, 0, -1);
    }

    /**
     * Execute the search as a user.
     */
    @Test
    public void searchAsUser() {
        _testSearch(user, Lists.newArrayList(portalPage1, portalPage2));
    }

    /**
     * Execute the search as a user and expect no results.
     */
    @Test
    public void searchAsUserWithNoResults() {
        _testSearch(user, Collections.emptyList());
    }

    /**
     * Execute the search as the anonymous user.
     */
    @Test
    public void searchAsAnonymous() {
        _testSearch(null, Lists.newArrayList(portalPage2));
    }

    private void _testSearch(final ApplicationUser user, final List expectedPages) {
        final SharedEntitySearchParameters searchParameters = new SharedEntitySearchParametersBuilder().setName("searchTest").
                setSortColumn(SharedEntityColumn.ID, false).toSearchParameters();
        final SharedEntitySearchResult expectedResult = new SharedEntitySearchResult(new MockCloseableIterable(expectedPages), true, expectedPages.size() + 100);

        when(searcher.search(searchParameters, user, 0, 100)).thenReturn(expectedResult);

        // run the search.
        final SharedEntitySearchResult actualResult = portalPageManager.search(searchParameters, user, 0, 100);

        // make sure the result is as expected.
        assertThat(actualResult.hasMoreResults(), is(expectedResult.hasMoreResults()));
        assertThat(actualResult.getResults(), is(expectedResult.getResults()));
    }

}
