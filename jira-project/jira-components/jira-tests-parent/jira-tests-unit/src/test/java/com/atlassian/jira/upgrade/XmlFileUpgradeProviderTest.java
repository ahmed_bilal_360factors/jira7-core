package com.atlassian.jira.upgrade;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;

public class XmlFileUpgradeProviderTest {
    @Test(expected = RuntimeException.class)
    public void upgradeFileCannotBeReadThrowsException() {
        final XmlFileUpgradeProvider xmlFileUpgradeProvider = new XmlFileUpgradeProvider("upgrades_not_found.xml");

        xmlFileUpgradeProvider.getUpgradeTasks();
    }

    @Test(expected = RuntimeException.class)
    public void incorrectlyParsedUpgradeInputStreamThrowsException() {
        final XmlFileUpgradeProvider xmlFileUpgradeProvider =
                new XmlFileUpgradeProvider("com/atlassian/jira/upgrade/tasks/not-an-xml-file.txt");

        xmlFileUpgradeProvider.getUpgradeTasks();
    }

    @Test
    public void upgradesFileWithNoUpgradesReturnsEmptyCollection() {
        final XmlFileUpgradeProvider xmlFileUpgradeProvider =
                new XmlFileUpgradeProvider("com/atlassian/jira/upgrade/tasks/no-upgrade-tasks.xml");

        assertThat(xmlFileUpgradeProvider.getUpgradeTasks(), hasSize(0));
    }


    @Test(expected = RuntimeException.class)
    public void missingUpgradesClassesAreNotLoadedFromTheXmlFile() {
        final XmlFileUpgradeProvider xmlFileUpgradeProvider =
                new XmlFileUpgradeProvider("com/atlassian/jira/upgrade/tasks/upgrade-tasks-with-missing.xml");

        xmlFileUpgradeProvider.getUpgradeTasks();
    }

    @Test
    public void upgradesAreLoadedFromTheXmlFile() {
        final XmlFileUpgradeProvider xmlFileUpgradeProvider =
                new XmlFileUpgradeProvider("com/atlassian/jira/upgrade/tasks/upgrade-tasks.xml");

        assertThat(xmlFileUpgradeProvider.getUpgradeTasks(), hasSize(2));
    }

    @Test
    public void upgradesCanBeLimitedToABuildNumber() {
        final XmlFileUpgradeProvider xmlFileUpgradeProvider =
                new XmlFileUpgradeProvider("com/atlassian/jira/upgrade/tasks/upgrade-tasks.xml");

        assertThat(xmlFileUpgradeProvider.getUpgradeTasksBoundByBuild(19999L), hasSize(1));
    }

    @Test
    public void upgradesLimitIsInclusive() {
        final XmlFileUpgradeProvider xmlFileUpgradeProvider =
                new XmlFileUpgradeProvider("com/atlassian/jira/upgrade/tasks/upgrade-tasks.xml");

        assertThat(xmlFileUpgradeProvider.getUpgradeTasksBoundByBuild(20000L), hasSize(2));
    }
}
