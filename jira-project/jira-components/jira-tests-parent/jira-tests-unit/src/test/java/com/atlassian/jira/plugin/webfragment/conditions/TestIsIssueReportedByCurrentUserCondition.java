package com.atlassian.jira.plugin.webfragment.conditions;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

public class TestIsIssueReportedByCurrentUserCondition {
    @Rule
    public final MethodRule initMockito = MockitoJUnit.rule();

    @Mock
    private Issue issue;

    @Test
    public void testTrue() {
        final ApplicationUser fred = new MockApplicationUser("fred");
        when(issue.getReporterId()).thenReturn("fred");

        final IsIssueReportedByCurrentUserCondition condition = new IsIssueReportedByCurrentUserCondition();

        assertTrue(condition.shouldDisplay(fred, issue, null));
    }

    @Test
    public void testNullReporter() {
        final ApplicationUser fred = new MockApplicationUser("fred");
        when(issue.getReporterId()).thenReturn(null);

        final IsIssueReportedByCurrentUserCondition condition = new IsIssueReportedByCurrentUserCondition();

        assertFalse(condition.shouldDisplay(fred, issue, null));
    }

    @Test
    public void testNullCurrent() {
        when(issue.getReporterId()).thenReturn("fred");

        final IsIssueReportedByCurrentUserCondition condition = new IsIssueReportedByCurrentUserCondition();

        assertFalse(condition.shouldDisplay(null, issue, null));
    }

    @Test
    public void testFalse() {
        final ApplicationUser fred = new MockApplicationUser("fred");
        when(issue.getReporterId()).thenReturn("admin");

        final IsIssueReportedByCurrentUserCondition condition = new IsIssueReportedByCurrentUserCondition();

        assertFalse(condition.shouldDisplay(fred, issue, null));
    }

}
