package com.atlassian.jira.issue.fields.renderer.wiki.embedded;

import com.atlassian.core.ofbiz.test.UtilsForTests;
import com.atlassian.core.util.thumbnail.Thumbnail;
import com.atlassian.jira.issue.AttachmentManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.attachment.Attachment;
import com.atlassian.jira.issue.fields.renderer.wiki.AtlassianWikiRenderer;
import com.atlassian.jira.issue.fields.renderer.wiki.JiraIconManager;
import com.atlassian.jira.issue.thumbnail.ThumbnailManager;
import com.atlassian.jira.issue.thumbnail.ThumbnailedImage;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.mock.component.MockComponentWorker;
import com.atlassian.jira.mock.ofbiz.MockOfBizDelegator;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.RendererConfiguration;
import com.atlassian.renderer.embedded.EmbeddedResourceRenderer;
import com.atlassian.renderer.v2.Renderer;
import com.atlassian.renderer.v2.V2LinkRenderer;
import com.atlassian.renderer.v2.V2Renderer;
import com.atlassian.renderer.v2.V2RendererFacade;
import com.atlassian.renderer.v2.V2SubRenderer;
import com.atlassian.renderer.v2.components.EmbeddedImageRendererComponent;
import com.atlassian.renderer.v2.components.EmbeddedObjectRendererComponent;
import com.atlassian.renderer.v2.components.EmbeddedUnembeddableRendererComponent;
import com.atlassian.renderer.v2.components.TokenRendererComponent;
import com.google.common.collect.ImmutableMap;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.mockito.Mock;
import org.ofbiz.core.entity.GenericValue;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Tests the renderer that handles embedding attached images within the markup.
 */
public class TestWikiEmbeddedResourceRenderer {

    @Rule
    public RuleChain mockitoMocksInContainer = MockitoMocksInContainer.forTest(this);

    @Mock
    private PluginAccessor pluginAccessor;

    @Mock
    @AvailableInContainer
    private JiraAuthenticationContext jiraAuthenticationContext;

    @Mock
    @AvailableInContainer
    private AttachmentManager attachmentManager;

    @Mock
    @AvailableInContainer
    private OfBizDelegator ofBizDelegator = new MockOfBizDelegator();

    private RenderContext renderContext;
    private V2RendererFacade renderer;

    private Attachment attachment;

    @Before
    public void onTestSetUp() throws Exception {
        new MockComponentWorker().addMock(OfBizDelegator.class, new MockOfBizDelegator())
                .addMock(AttachmentManager.class, attachmentManager)
                .init();

        ThumbnailManager thumbnailManager = mock(ThumbnailManager.class);

        attachment = newAttachment(1L, "testfile.jpg");
        String attachmentFileName = attachment.getFilename();
        Long attachemntId = attachment.getId();
        when(attachmentManager.getAttachment(attachment.getId())).thenReturn(attachment);

        final Issue issue = newIssue(attachment);
        renderContext = newRenderContext(issue);

        Thumbnail thumbnail = mock(Thumbnail.class);
        when(thumbnail.getAttachmentId()).thenReturn(attachemntId);
        when(thumbnail.getFilename()).thenReturn(attachmentFileName);

        ThumbnailedImage thumbnailedImage = mock(ThumbnailedImage.class);
        when(thumbnailedImage.getAttachmentId()).thenReturn(attachemntId);
        when(thumbnailedImage.getFilename()).thenReturn(attachmentFileName);
        String thumbnailURL = getThumbnailURL(attachment);
        when(thumbnailedImage.getImageURL()).thenReturn(thumbnailURL);

        when(thumbnailManager.isThumbnailable(attachment)).thenReturn(true);
        when(thumbnailManager.toThumbnailedImage(thumbnail)).thenReturn(thumbnailedImage);
        when(thumbnailManager.getThumbnail(attachment)).thenReturn(thumbnail);

        renderer = newRenderer(attachmentManager, thumbnailManager, jiraAuthenticationContext);
    }

    private V2RendererFacade newRenderer(AttachmentManager attachmentManager,
                                         ThumbnailManager thumbnailManager, JiraAuthenticationContext authenticationContext) {
        RendererConfiguration rendererConfiguration = mock(RendererConfiguration.class);

        EmbeddedResourceRenderer embeddedRenderer = new JiraEmbeddedResourceRenderer(new RendererAttachmentManager(attachmentManager,
                thumbnailManager, authenticationContext));

        V2SubRenderer v2SubRenderer = new V2SubRenderer();
        Renderer renderer = new V2Renderer(Arrays.asList(new EmbeddedImageRendererComponent(), new EmbeddedObjectRendererComponent(),
                new EmbeddedUnembeddableRendererComponent(), new TokenRendererComponent(v2SubRenderer)));
        v2SubRenderer.setRenderer(renderer);
        V2LinkRenderer linkRenderer = new V2LinkRenderer(v2SubRenderer, new JiraIconManager(), rendererConfiguration);
        return new V2RendererFacade(rendererConfiguration, linkRenderer, embeddedRenderer, renderer);
    }

    private Attachment newAttachment(long id, String filename) {
        Attachment result = mock(Attachment.class);
        when(result.getId()).thenReturn(id);
        when(result.getFilename()).thenReturn(filename);
        return result;
    }

    private Issue newIssue(Attachment attachment) {
        GenericValue issueOpen = UtilsForTests.getTestEntity("Issue",
                ImmutableMap.of("id", 1L, "key", "TST-1", "summary", "summary", "security", 1L));

        Issue result = mock(Issue.class);
        when(result.getKey()).thenReturn("TST-1");
        when(result.getAttachments()).thenReturn(Arrays.asList(attachment));
        when(result.getGenericValue()).thenReturn(issueOpen);
        return result;
    }

    private RenderContext newRenderContext(Issue issue) {
        RenderContext result = new RenderContext();
        result.setSiteRoot("http://localhost:8080");
        result.setCharacterEncoding("UTF-8");
        result.addParam(AtlassianWikiRenderer.ISSUE_CONTEXT_KEY, issue);
        return result;
    }

    @Test
    public void testEmbeddedImageAttachment() {
        assertEquals(getExpectedEmbeddedImageLink(renderContext, attachment),
                renderer.convertWikiToXHtml(renderContext, "!" + attachment.getFilename() + "!"));
    }

    @Test
    public void testEmbeddedImageExternal() {
        String externalLink = "http://www.google.com.au/intl/en_au/images/logo.gif";
        assertEquals(getExpectedEmbeddedExternalImageLink(externalLink),
                renderer.convertWikiToXHtml(renderContext, "!" + externalLink + "!"));
    }

    @Test
    public void testExternalLinkSuccessWithNoIssue() {
        String externalLink = "http://www.google.com.au/intl/en_au/images/logo.gif";
        assertEquals(getExpectedEmbeddedExternalImageLink(externalLink),
                renderer.convertWikiToXHtml(renderContext, "!" + externalLink + "!"));
    }

    @Test
    public void testInternalLinkFailureWithNoIssue() {
        assertEquals(getExpectedEmbeddedLinkFailure(attachment),
                renderer.convertWikiToXHtml(new RenderContext(), "!" + attachment.getFilename() + "!"));
    }

    @Test
    public void testInternalLinkFailureWithNoAttachment() {
        String filename = "unexisting.jpg";
        assertEquals(getExpectedEmbeddedLinkFailureNoFile(filename),
                renderer.convertWikiToXHtml(renderContext, "!" + filename + "!"));
    }

    @Test
    public void testEmbeddedImageThumbnailAttachment() {
        assertEquals(getExpectedImageThumbnailLink(renderContext, attachment),
                renderer.convertWikiToXHtml(renderContext, "!" + attachment.getFilename() + "|thumbnail!"));
    }

    private String getExpectedEmbeddedImageLink(RenderContext renderContext, Attachment attachment) {
        return "<span class=\"image-wrap\" style=\"\"><img src=\"" + renderContext.getSiteRoot()
                + "/secure/attachment/" + attachment.getId() + '/' + attachment.getId() + '_'
                + attachment.getFilename() + "\" style=\"border: 0px solid black\" /></span>";
    }

    private String getExpectedEmbeddedExternalImageLink(String externalLink) {
        return "<span class=\"image-wrap\" style=\"\"><img src=\"" + externalLink
                + "\" style=\"border: 0px solid black\" /></span>";
    }

    private String getExpectedEmbeddedLinkFailure(Attachment attachment) {
        return "<span class=\"error\">No usable issue stored in the context, unable to resolve filename &#39;"
                + attachment.getFilename() + "&#39;</span>";
    }

    private String getExpectedEmbeddedLinkFailureNoFile(String filename) {
        return "<span class=\"error\">Unable to render embedded object: File (" + filename + ") not found.</span>";
    }

    private String getExpectedImageThumbnailLink(RenderContext renderContext, Attachment attachment) {
        return "<span class=\"image-wrap\" style=\"\"><a id=\"1_thumb\" href=\"" + renderContext.getSiteRoot()
                + "/secure/attachment/" + attachment.getId() + '/' + attachment.getId() + '_'
                + attachment.getFilename() + "\" title=\"" + attachment.getFilename() + "\""
                + " file-preview-type=\"image\" file-preview-id=\""+ attachment.getId() +"\" file-preview-title=\"" + attachment.getFilename() + "\""
                + "><img src=\""
                + renderContext.getSiteRoot() + "/secure/thumbnail/" + attachment.getId() + '/' + attachment.getId()
                + '_' + attachment.getFilename() + "\" style=\"border: 0px solid black\" /></a></span>";
    }

    private String getThumbnailURL(Attachment attachment) {
        return "http://localhost:8080/secure/thumbnail/" + attachment.getId() + '/' + attachment.getId() + '_'
                + attachment.getFilename();
    }

}
