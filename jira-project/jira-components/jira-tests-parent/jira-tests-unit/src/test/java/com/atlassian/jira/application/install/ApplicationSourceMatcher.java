package com.atlassian.jira.application.install;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeDiagnosingMatcher;

import java.io.File;

import static org.mockito.Matchers.argThat;

/**
 * @since v6.5
 */
class ApplicationSourceMatcher extends TypeSafeDiagnosingMatcher<ApplicationSource> {
    private final Matcher<String> productNameMatcher;
    private final Matcher<File[]> bundlesMatcher;

    ApplicationSourceMatcher(final Matcher<String> productNameMatcher, final Matcher<File[]> bundlesMatcher) {
        this.productNameMatcher = productNameMatcher;
        this.bundlesMatcher = bundlesMatcher;
    }

    @Override
    protected boolean matchesSafely(final ApplicationSource item, final Description mismatchDescription) {
        if (!productNameMatcher.matches(item.getApplicationSourceName())) {
            mismatchDescription.appendText("name ");
            productNameMatcher.describeMismatch(item, mismatchDescription);
            return false;
        }
        if (!bundlesMatcher.matches(item.getApplicationBundles())) {
            mismatchDescription.appendText("files ");
            bundlesMatcher.describeMismatch(item, mismatchDescription);
            return false;
        }

        return true;
    }

    @Override
    public void describeTo(final Description description) {
        description
                .appendText("ApplicationSource with name that ")
                .appendDescriptionOf(productNameMatcher)
                .appendText("and files that ")
                .appendDescriptionOf(bundlesMatcher);
    }

    static ApplicationSource appSource(final Matcher<String> productNameMatcher, final Matcher<File[]> bundlesMatcher) {
        return argThat(new ApplicationSourceMatcher(productNameMatcher, bundlesMatcher));
    }
}
