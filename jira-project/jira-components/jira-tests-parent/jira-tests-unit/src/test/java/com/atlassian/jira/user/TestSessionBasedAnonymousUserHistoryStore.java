package com.atlassian.jira.user;

import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.user.util.MockUserManager;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.collect.CollectionBuilder;
import com.atlassian.jira.util.velocity.VelocityRequestContext;
import com.atlassian.jira.util.velocity.VelocityRequestContextFactory;
import com.atlassian.jira.util.velocity.VelocityRequestSession;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.List;
import java.util.Set;

import static java.util.Collections.emptyList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestSessionBasedAnonymousUserHistoryStore {
    @Rule
    public final MockitoRule initMockito = MockitoJUnit.rule();
    @Mock
    private UserHistoryStore delegateStore;
    @Mock
    private ApplicationProperties applicationProperties;
    @Mock
    private VelocityRequestContextFactory velocityRequestContextFactory;
    @Mock
    private VelocityRequestContext velocityRequestContext;
    @Mock
    private VelocityRequestSession session;
    private ApplicationUser user = new DelegatingApplicationUser(1L, "admin", new MockUser("admin"));
    private SessionBasedAnonymousUserHistoryStore store;

    @Before
    public void setUp() throws Exception {
        final UserManager userManager = new MockUserManager();

        store = new SessionBasedAnonymousUserHistoryStore(delegateStore, applicationProperties, userManager, velocityRequestContextFactory);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCantAddNullValues() {
        store.addHistoryItem(user, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGettingHistoryOfNullType() {
        store.getHistory(null, user);
    }

    @Test
    public void testAddHistoryNoSessionNoUser() {
        final UserHistoryItem item = new UserHistoryItem(UserHistoryItem.ISSUE, "123");

        when(velocityRequestContextFactory.getJiraVelocityRequestContext()).thenReturn(velocityRequestContext);
        when(velocityRequestContext.getSession()).thenReturn(null);

        store.addHistoryItem(null, item);

        verify(velocityRequestContext).getSession();
        verify(velocityRequestContextFactory).getJiraVelocityRequestContext();
    }

    @Test
    public void testAddHistoryNoSession() {
        final UserHistoryItem item = new UserHistoryItem(UserHistoryItem.ISSUE, "123");

        when(velocityRequestContextFactory.getJiraVelocityRequestContext()).thenReturn(velocityRequestContext);
        when(velocityRequestContext.getSession()).thenReturn(null);

        store.addHistoryItem(user, item);

        verify(delegateStore).addHistoryItem(user, item);
    }

    @Test
    public void testAddHistoryNoUserNoHistory() {
        final UserHistoryItem item = new UserHistoryItem(UserHistoryItem.ISSUE, "123");

        String sessionKey = SessionBasedAnonymousUserHistoryStore.SESSION_PREFIX + UserHistoryItem.ISSUE.getName();
        when(velocityRequestContextFactory.getJiraVelocityRequestContext()).thenReturn(velocityRequestContext);
        when(velocityRequestContext.getSession()).thenReturn(session);
        when(session.getId()).thenReturn("sessionId");
        when(session.getAttribute(sessionKey)).thenReturn(null);

        store.addHistoryItem(null, item);

        verify(session).setAttribute(sessionKey, CollectionBuilder.newBuilder(item).asList());
    }

    @Test
    public void testAddHistoryNoHistory() {
        final UserHistoryItem item = new UserHistoryItem(UserHistoryItem.ISSUE, "123");

        when(velocityRequestContextFactory.getJiraVelocityRequestContext()).thenReturn(velocityRequestContext);
        when(velocityRequestContext.getSession()).thenReturn(null);
        when(session.getId()).thenReturn("sessionId");

        final String sessionKey = SessionBasedAnonymousUserHistoryStore.SESSION_PREFIX + UserHistoryItem.ISSUE.getName();

        when(session.getAttribute(sessionKey)).thenReturn(null);

        store.addHistoryItem(user, item);

        verify(delegateStore).addHistoryItem(user, item);
    }

    @Test
    public void testAddHistoryEmptyHistory() {
        final UserHistoryItem item = new UserHistoryItem(UserHistoryItem.ISSUE, "123");

        when(velocityRequestContextFactory.getJiraVelocityRequestContext()).thenReturn(velocityRequestContext);
        when(velocityRequestContext.getSession()).thenReturn(null);
        when(session.getId()).thenReturn("sessionId");

        final String sessionKey = SessionBasedAnonymousUserHistoryStore.SESSION_PREFIX + UserHistoryItem.ISSUE.getName();
        when(session.getAttribute(sessionKey)).thenReturn(CollectionBuilder.newBuilder().asList());

        store.addHistoryItem(user, item);

        verify(delegateStore).addHistoryItem(user, item);
    }

    @Test
    public void testAddNewHistoryNoUser() {
        final UserHistoryItem item = new UserHistoryItem(UserHistoryItem.ISSUE, "123");
        final UserHistoryItem item2 = new UserHistoryItem(UserHistoryItem.ISSUE, "1234");
        final UserHistoryItem item3 = new UserHistoryItem(UserHistoryItem.ISSUE, "1235");
        final UserHistoryItem item4 = new UserHistoryItem(UserHistoryItem.ISSUE, "1236");

        final List<UserHistoryItem> history = CollectionBuilder.newBuilder(item2, item3, item4).asMutableList();
        final List<UserHistoryItem> expectedHistory = CollectionBuilder.newBuilder(item, item2, item3, item4).asMutableList();

        when(velocityRequestContextFactory.getJiraVelocityRequestContext()).thenReturn(velocityRequestContext);
        when(velocityRequestContext.getSession()).thenReturn(session);
        when(session.getId()).thenReturn("sessionId");

        final String sessionKey = SessionBasedAnonymousUserHistoryStore.SESSION_PREFIX + UserHistoryItem.ISSUE.getName();
        when(session.getAttribute(sessionKey)).thenReturn(history);
        when(applicationProperties.getDefaultBackedString("jira.max.Issue.history.items")).thenReturn("10");

        store.addHistoryItem(null, item);

        assertEquals(expectedHistory, history);
    }

    @Test
    public void testAddNewHistoryNoUserExpireOld() {
        final UserHistoryItem item = new UserHistoryItem(UserHistoryItem.ISSUE, "123");
        final UserHistoryItem item2 = new UserHistoryItem(UserHistoryItem.ISSUE, "1234");
        final UserHistoryItem item3 = new UserHistoryItem(UserHistoryItem.ISSUE, "1235");
        final UserHistoryItem item4 = new UserHistoryItem(UserHistoryItem.ISSUE, "1236");

        final List<UserHistoryItem> history = CollectionBuilder.newBuilder(item2, item3, item4).asMutableList();
        final List<UserHistoryItem> expectedHistory = CollectionBuilder.newBuilder(item, item2).asMutableList();

        when(velocityRequestContextFactory.getJiraVelocityRequestContext()).thenReturn(velocityRequestContext);
        when(velocityRequestContext.getSession()).thenReturn(session);
        when(session.getId()).thenReturn("sessionId");

        final String sessionKey = SessionBasedAnonymousUserHistoryStore.SESSION_PREFIX + UserHistoryItem.ISSUE.getName();
        when(session.getAttribute(sessionKey)).thenReturn(history);
        when(applicationProperties.getDefaultBackedString("jira.max.Issue.history.items")).thenReturn("2");

        store.addHistoryItem(null, item);

        assertEquals(expectedHistory, history);
    }

    @Test
    public void testAddExistingHistory() {
        final UserHistoryItem item = new UserHistoryItem(UserHistoryItem.ISSUE, "123", 2);
        final UserHistoryItem newItem = new UserHistoryItem(UserHistoryItem.ISSUE, "123", 3);
        final UserHistoryItem item2 = new UserHistoryItem(UserHistoryItem.ISSUE, "1234");
        final UserHistoryItem item3 = new UserHistoryItem(UserHistoryItem.ISSUE, "1235");
        final UserHistoryItem item4 = new UserHistoryItem(UserHistoryItem.ISSUE, "1236");

        final List<UserHistoryItem> history = CollectionBuilder.newBuilder(item2, item3, item, item4).asMutableList();
        final List<UserHistoryItem> expectedHistory = CollectionBuilder.newBuilder(newItem, item2, item3, item4).asMutableList();

        when(velocityRequestContextFactory.getJiraVelocityRequestContext()).thenReturn(velocityRequestContext);
        when(velocityRequestContext.getSession()).thenReturn(session);
        when(session.getId()).thenReturn("sessionId");

        final String sessionKey = SessionBasedAnonymousUserHistoryStore.SESSION_PREFIX + UserHistoryItem.ISSUE.getName();
        when(session.getAttribute(sessionKey)).thenReturn(history);

        // even though ther are more items, we don't expire as we are only moving an existing item
        store.addHistoryItem(null, newItem);

        assertEquals(expectedHistory, history);
    }

    @Test
    public void testAddHistoryUserJustLoggedIn() {
        final UserHistoryItem item = new UserHistoryItem(UserHistoryItem.ISSUE, "123", 2);
        final UserHistoryItem item2 = new UserHistoryItem(UserHistoryItem.ISSUE, "1234");
        final UserHistoryItem item3 = new UserHistoryItem(UserHistoryItem.ISSUE, "1235");
        final UserHistoryItem item4 = new UserHistoryItem(UserHistoryItem.ISSUE, "1236");

        final List<UserHistoryItem> history = CollectionBuilder.newBuilder(item2, item3, item4).asMutableList();

        when(velocityRequestContextFactory.getJiraVelocityRequestContext()).thenReturn(velocityRequestContext);
        when(velocityRequestContext.getSession()).thenReturn(session);
        when(session.getId()).thenReturn("sessionId");

        String sessionKey = SessionBasedAnonymousUserHistoryStore.SESSION_PREFIX + UserHistoryItem.ISSUE.getName();
        when(session.getAttribute(sessionKey)).thenReturn(history);

        session.removeAttribute(sessionKey);

        store.addHistoryItem(user, item);

        verify(delegateStore).addHistoryItem(user, item4);
        verify(delegateStore).addHistoryItem(user, item3);
        verify(delegateStore).addHistoryItem(user, item2);
        verify(delegateStore).addHistoryItem(user, item);
    }

    @Test
    public void testGetHistoryNoSessionNoUser() {
        when(velocityRequestContextFactory.getJiraVelocityRequestContext()).thenReturn(velocityRequestContext);
        when(velocityRequestContext.getSession()).thenReturn(null);

        assertTrue(store.getHistory(UserHistoryItem.ISSUE, (ApplicationUser) null).isEmpty());
    }

    @Test
    public void testGetHistoryNoSession() {
        final UserHistoryItem item = new UserHistoryItem(UserHistoryItem.ISSUE, "123", 2);
        final UserHistoryItem item2 = new UserHistoryItem(UserHistoryItem.ISSUE, "1234");
        final UserHistoryItem item3 = new UserHistoryItem(UserHistoryItem.ISSUE, "1235");
        final UserHistoryItem item4 = new UserHistoryItem(UserHistoryItem.ISSUE, "1236");

        final List<UserHistoryItem> history = CollectionBuilder.newBuilder(item, item2, item3, item4).asMutableList();

        when(velocityRequestContextFactory.getJiraVelocityRequestContext()).thenReturn(velocityRequestContext);
        when(velocityRequestContext.getSession()).thenReturn(null);
        when(delegateStore.getHistory(UserHistoryItem.ISSUE, user)).thenReturn(history);

        assertEquals(history, store.getHistory(UserHistoryItem.ISSUE, user));
    }

    @Test
    public void testGetHistoryNoUserNoHistory() {
        final List<UserHistoryItem> history = CollectionBuilder.<UserHistoryItem>newBuilder().asMutableList();

        when(velocityRequestContextFactory.getJiraVelocityRequestContext()).thenReturn(velocityRequestContext);
        when(velocityRequestContext.getSession()).thenReturn(null);
        when(session.getId()).thenReturn("sessionId");

        String sessionKey = SessionBasedAnonymousUserHistoryStore.SESSION_PREFIX + UserHistoryItem.ISSUE.getName();
        when(session.getAttribute(sessionKey)).thenReturn(null);

        assertEquals(history, store.getHistory(UserHistoryItem.ISSUE, (ApplicationUser) null));
    }

    @Test
    public void testGetHistoryNoUserEmptyHistory() {
        final List<UserHistoryItem> history = CollectionBuilder.<UserHistoryItem>newBuilder().asMutableList();

        when(velocityRequestContextFactory.getJiraVelocityRequestContext()).thenReturn(velocityRequestContext);
        when(velocityRequestContext.getSession()).thenReturn(null);
        when(session.getId()).thenReturn("sessionId");

        final String sessionKey = SessionBasedAnonymousUserHistoryStore.SESSION_PREFIX + UserHistoryItem.ISSUE.getName();
        when(session.getAttribute(sessionKey)).thenReturn(history);

        assertEquals(history, store.getHistory(UserHistoryItem.ISSUE, (ApplicationUser) null));
    }

    @Test
    public void testGetHistoryNoUser() {
        final UserHistoryItem item = new UserHistoryItem(UserHistoryItem.ISSUE, "123", 2);
        final UserHistoryItem item2 = new UserHistoryItem(UserHistoryItem.ISSUE, "1234");
        final UserHistoryItem item3 = new UserHistoryItem(UserHistoryItem.ISSUE, "1235");
        final UserHistoryItem item4 = new UserHistoryItem(UserHistoryItem.ISSUE, "1236");

        final List<UserHistoryItem> history = CollectionBuilder.newBuilder(item, item2, item3, item4).asMutableList();

        when(velocityRequestContextFactory.getJiraVelocityRequestContext()).thenReturn(velocityRequestContext);
        when(velocityRequestContext.getSession()).thenReturn(session);
        when(session.getId()).thenReturn("sessionId");

        final String sessionKey = SessionBasedAnonymousUserHistoryStore.SESSION_PREFIX + UserHistoryItem.ISSUE.getName();
        when(session.getAttribute(sessionKey)).thenReturn(history);

        assertEquals(history, store.getHistory(UserHistoryItem.ISSUE, (ApplicationUser) null));
    }

    @Test
    public void testGetHistoryNoHistoryInSession() {
        final UserHistoryItem item = new UserHistoryItem(UserHistoryItem.ISSUE, "123", 2);
        final UserHistoryItem item2 = new UserHistoryItem(UserHistoryItem.ISSUE, "1234");
        final UserHistoryItem item3 = new UserHistoryItem(UserHistoryItem.ISSUE, "1235");
        final UserHistoryItem item4 = new UserHistoryItem(UserHistoryItem.ISSUE, "1236");
        final List<UserHistoryItem> history = CollectionBuilder.newBuilder(item, item2, item3, item4).asMutableList();

        when(velocityRequestContextFactory.getJiraVelocityRequestContext()).thenReturn(velocityRequestContext);
        when(velocityRequestContext.getSession()).thenReturn(null);
        when(session.getId()).thenReturn("sessionId");

        final String sessionKey = SessionBasedAnonymousUserHistoryStore.SESSION_PREFIX + UserHistoryItem.ISSUE.getName();
        when(session.getAttribute(sessionKey)).thenReturn(null);
        when(delegateStore.getHistory(UserHistoryItem.ISSUE, user)).thenReturn(history);

        assertEquals(history, store.getHistory(UserHistoryItem.ISSUE, user));
    }

    @Test
    public void testGetHistoryEmptyHistoryInSession() {
        final UserHistoryItem item = new UserHistoryItem(UserHistoryItem.ISSUE, "123", 2);
        final UserHistoryItem item2 = new UserHistoryItem(UserHistoryItem.ISSUE, "1234");
        final UserHistoryItem item3 = new UserHistoryItem(UserHistoryItem.ISSUE, "1235");
        final UserHistoryItem item4 = new UserHistoryItem(UserHistoryItem.ISSUE, "1236");

        final List<UserHistoryItem> history = CollectionBuilder.newBuilder(item, item2, item3, item4).asMutableList();
        final List<UserHistoryItem> emptyHistory = CollectionBuilder.<UserHistoryItem>newBuilder().asMutableList();

        when(velocityRequestContextFactory.getJiraVelocityRequestContext()).thenReturn(velocityRequestContext);
        when(velocityRequestContext.getSession()).thenReturn(session);
        when(session.getId()).thenReturn("sessionId");

        final String sessionKey = SessionBasedAnonymousUserHistoryStore.SESSION_PREFIX + UserHistoryItem.ISSUE.getName();
        when(session.getAttribute(sessionKey)).thenReturn(emptyHistory);
        when(delegateStore.getHistory(UserHistoryItem.ISSUE, user)).thenReturn(history);

        assertEquals(history, store.getHistory(UserHistoryItem.ISSUE, user));
    }

    @Test
    public void testGetHistoryUserJustLoggedIn() {
        final UserHistoryItem item = new UserHistoryItem(UserHistoryItem.ISSUE, "123", 2);
        final UserHistoryItem item2 = new UserHistoryItem(UserHistoryItem.ISSUE, "1234");
        final UserHistoryItem item3 = new UserHistoryItem(UserHistoryItem.ISSUE, "1235");
        final UserHistoryItem item4 = new UserHistoryItem(UserHistoryItem.ISSUE, "1236");

        final List<UserHistoryItem> history = CollectionBuilder.newBuilder(item2, item3, item4).asMutableList();
        final List<UserHistoryItem> newHistory = CollectionBuilder.newBuilder(item2, item3, item4, item).asMutableList();

        when(velocityRequestContextFactory.getJiraVelocityRequestContext()).thenReturn(velocityRequestContext);
        when(velocityRequestContext.getSession()).thenReturn(session);
        when(session.getId()).thenReturn("sessionId");

        final String sessionKey = SessionBasedAnonymousUserHistoryStore.SESSION_PREFIX + UserHistoryItem.ISSUE.getName();
        when(session.getAttribute(sessionKey)).thenReturn(history);

        session.removeAttribute(sessionKey);

        when(delegateStore.getHistory(UserHistoryItem.ISSUE, user)).thenReturn(newHistory);

        assertEquals(newHistory, store.getHistory(UserHistoryItem.ISSUE, user));

        verify(delegateStore).addHistoryItem(user, item4);
        verify(delegateStore).addHistoryItem(user, item3);
        verify(delegateStore).addHistoryItem(user, item2);
    }

    @Test
    public void testGetHistoryErrorThrown() {
        final List<UserHistoryItem> history = emptyList();

        when(velocityRequestContextFactory.getJiraVelocityRequestContext()).thenReturn(velocityRequestContext);
        when(velocityRequestContext.getSession()).thenReturn(null);
        when(session.getId()).thenReturn("sessionId");

        final String sessionKey = SessionBasedAnonymousUserHistoryStore.SESSION_PREFIX + UserHistoryItem.ISSUE.getName();
        when(session.getAttribute(sessionKey)).thenThrow(new IllegalArgumentException());

        assertEquals(history, store.getHistory(UserHistoryItem.ISSUE, user));
    }

    @Test
    public void testRemoveHistoryNullUser() {
        final Set<UserHistoryItem.Type> typesRemoved = CollectionBuilder.<UserHistoryItem.Type>newBuilder().asSet();

        assertEquals(typesRemoved, store.removeHistoryForUser(null));
    }

    @Test
    public void testRemoveHistory() {
        final Set<UserHistoryItem.Type> typesRemoved = CollectionBuilder.<UserHistoryItem.Type>newBuilder(UserHistoryItem.ISSUE,
                UserHistoryItem.PROJECT).asSet();

        when(delegateStore.removeHistoryForUser(user)).thenReturn(typesRemoved);

        assertEquals(typesRemoved, store.removeHistoryForUser(user));
    }
}
