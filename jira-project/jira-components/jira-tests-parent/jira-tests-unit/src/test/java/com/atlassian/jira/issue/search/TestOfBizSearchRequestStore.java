package com.atlassian.jira.issue.search;

import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.jql.parser.JqlQueryParser;
import com.atlassian.jira.junit.rules.InitMockitoMocks;
import com.atlassian.jira.mock.component.MockComponentWorker;
import com.atlassian.jira.mock.ofbiz.MockOfBizDelegator;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.util.MockUserManager;
import com.atlassian.jira.user.util.UserManager;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.mockito.Mock;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.emptyIterable;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;

/**
 * @since v5.2
 */
public class TestOfBizSearchRequestStore {
    @Rule
    public TestRule initMockitoMocks = new InitMockitoMocks(this);

    private MockUserManager userManager = new MockUserManager();
    private OfBizDelegator mockOfBizDelegator = new MockOfBizDelegator();

    @Mock
    private SearchService searchService;

    @Mock
    private JqlQueryParser jqlQueryParser;

    private OfBizSearchRequestStore ofBizSearchRequestStore;

    @Before
    public void setUp() throws Exception {
        new MockComponentWorker().addMock(UserManager.class, userManager).init();
        ofBizSearchRequestStore = new OfBizSearchRequestStore(mockOfBizDelegator, jqlQueryParser, searchService, userManager);
    }

    @Test
    public void testVisitAll() throws Exception {
        ofBizSearchRequestStore.create(searchRequest("My Search Request"));
        ofBizSearchRequestStore.create(searchRequest("Another Search Request"));

        final Set<String> requestNames = new HashSet<>();
        ofBizSearchRequestStore.visitAll(element -> requestNames.add(element.getName()));

        assertThat(requestNames, contains("My Search Request", "Another Search Request"));
    }

    @Test
    public void testFindByNameIgnoreCase() throws Exception {
        ofBizSearchRequestStore.create(searchRequest("My Search Request"));
        ofBizSearchRequestStore.create(searchRequest("Another Search Request"));

        List<SearchRequest> results = ofBizSearchRequestStore.findByNameIgnoreCase("my search");
        assertThat(results, emptyIterable());

        results = ofBizSearchRequestStore.findByNameIgnoreCase("my search Request");
        assertThat(results, contains(hasProperty("name", equalTo("My Search Request"))));
    }

    @Test
    public void testUpdatingNameInSearchRequest() throws Exception {

        SearchRequest searchRequest = ofBizSearchRequestStore.create(searchRequest("My Search Request"));
        searchRequest.setName("Blah Blah");
        ofBizSearchRequestStore.update(searchRequest);

        assertThat(ofBizSearchRequestStore.findByNameIgnoreCase("My Search Request"), hasSize(0));
        assertThat(ofBizSearchRequestStore.findByNameIgnoreCase("blah BLAH"), hasSize(1));
    }

    private SearchRequest searchRequest(String name) {
        return new SearchRequest(null, new MockApplicationUser("fred"), name, "SearchRequest for " + name);
    }
}
