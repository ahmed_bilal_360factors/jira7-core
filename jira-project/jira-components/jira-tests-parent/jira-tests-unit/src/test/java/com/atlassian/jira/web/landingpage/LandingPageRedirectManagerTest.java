package com.atlassian.jira.web.landingpage;

import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.user.MockApplicationUser;
import org.junit.Before;
import org.junit.Test;

import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;

import static org.hamcrest.CoreMatchers.anyOf;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


public class LandingPageRedirectManagerTest {
    private static final MockApplicationUser USER = new MockApplicationUser("user1");

    private PageRedirect redirect1;
    private PageRedirect redirect2;
    private PageRedirect redirect3;

    private AtomicBoolean redirect1Active;
    private AtomicBoolean redirect2Active;
    private AtomicBoolean redirect3Active;

    private FeatureManager featureManager;
    private LandingPageRedirectManager manager;

    @Before
    public void setup() {
        featureManager = mock(FeatureManager.class);
        manager = new LandingPageRedirectManager(featureManager);

        redirect1Active = new AtomicBoolean(true);
        redirect2Active = new AtomicBoolean(true);
        redirect3Active = new AtomicBoolean(true);
        redirect1 = new StaticUrlRedirect("/redir1", (user) -> redirect1Active.get());
        redirect2 = new StaticUrlRedirect("/redir2", (user) -> redirect2Active.get());
        redirect3 = new StaticUrlRedirect("/redir3", (user) -> redirect3Active.get());
    }

    @Test
    public void noRedirectIfNothingHasBeenRegistered() {
        final Optional<String> redirect = manager.redirectUrl(USER);
        assertThat(redirect.isPresent(), is(false));
    }

    @Test
    public void redirectToUrlIfRedirectIsActive() {
        manager.registerRedirect(redirect1, 5);
        redirect1Active.set(true);

        final Optional<String> redirect = manager.redirectUrl(USER);
        assertThat(redirect, is(Optional.of("/redir1")));
    }

    @Test
    public void noRedirectIfDarkFeatureIsEnabled() {
        when(featureManager.isEnabled(LandingPageRedirectManager.DARK_FEATURE_DISABLE_LANDING_PAGE_REDIRECT_FLAG))
                .thenReturn(true);
        manager.registerRedirect(redirect1, 5);
        redirect1Active.set(true);

        final Optional<String> redirect = manager.redirectUrl(USER);
        assertThat(redirect, is(Optional.empty()));
    }

    @Test
    public void choosesRedirectWithHighestPriorityIfManyAreActive() {
        manager.registerRedirect(redirect1, 5);
        manager.registerRedirect(redirect3, 0);
        manager.registerRedirect(redirect2, 1);

        final Optional<String> redirect = manager.redirectUrl(USER);
        assertThat(redirect, is(Optional.of("/redir3")));
    }

    @Test
    public void choosesUrlOnlyFromActiveRedirects() {
        manager.registerRedirect(redirect1, 5);
        manager.registerRedirect(redirect3, 0);
        manager.registerRedirect(redirect2, 1);

        redirect1Active.set(true);
        redirect2Active.set(false);
        redirect3Active.set(false);

        assertThat(manager.redirectUrl(USER), is(Optional.of("/redir1")));

        redirect1Active.set(true);
        redirect2Active.set(true);
        redirect3Active.set(false);
        assertThat(manager.redirectUrl(USER), is(Optional.of("/redir2")));

        redirect1Active.set(false);
        redirect2Active.set(true);
        redirect3Active.set(true);
        assertThat(manager.redirectUrl(USER), is(Optional.of("/redir3")));

        redirect1Active.set(false);
        redirect2Active.set(false);
        redirect3Active.set(false);
        assertThat(manager.redirectUrl(USER), is(Optional.empty()));
    }

    @Test
    public void unregisteresRedirects() {
        manager.registerRedirect(redirect1, 5);
        manager.registerRedirect(redirect3, 1);
        manager.registerRedirect(redirect2, 1);
        manager.registerRedirect(redirect2, 10);

        redirect3Active.set(false);

        assertThat(manager.unregisterRedirect(redirect2), is(true));
        assertThat(manager.redirectUrl(USER), is(Optional.of("/redir1")));
        assertThat(manager.unregisterRedirect(redirect2), is(false));

        assertThat(manager.unregisterRedirect(redirect1), is(true));
        assertThat(manager.redirectUrl(USER), is(Optional.empty()));
        assertThat(manager.unregisterRedirect(redirect1), is(false));

        assertThat(manager.unregisterRedirect(redirect3), is(true));
        assertThat(manager.redirectUrl(USER), is(Optional.empty()));
        assertThat(manager.unregisterRedirect(redirect3), is(false));
    }

    @Test
    public void cannotRegisterDuplicateRedirects() {
        manager.registerRedirect(redirect2, 1);
        manager.registerRedirect(redirect2, 5);
        manager.registerRedirect(redirect2, 10);

        assertThat(manager.redirectUrl(USER), is(Optional.of("/redir2")));
        assertThat(manager.unregisterRedirect(redirect2), is(true));
        assertThat(manager.redirectUrl(USER), is(Optional.empty()));
        assertThat(manager.unregisterRedirect(redirect2), is(false));
    }

    @Test
    public void redirectsWithSamePriorityAreAllowed() {
        PageRedirect redirect4 = new StaticUrlRedirect("/redir4", (user) -> true);

        manager.registerRedirect(redirect2, 1);
        manager.registerRedirect(redirect4, 2);
        manager.registerRedirect(redirect1, 1);
        manager.registerRedirect(redirect3, 1);


        assertThat(manager.redirectUrl(USER), anyOf(
                is(Optional.of("/redir1")),
                is(Optional.of("/redir2")),
                is(Optional.of("/redir3")
                )));

        assertTrue(manager.unregisterRedirect(redirect2));
        assertThat(manager.redirectUrl(USER), anyOf(
                is(Optional.of("/redir1")),
                is(Optional.of("/redir3")
                )));

        assertTrue(manager.unregisterRedirect(redirect1));
        assertThat(manager.redirectUrl(USER), is(Optional.of("/redir3")));

        assertTrue(manager.unregisterRedirect(redirect3));
        assertThat(manager.redirectUrl(USER), is(Optional.of("/redir4")));
    }


}