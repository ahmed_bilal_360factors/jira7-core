package com.atlassian.jira.project.template;

import com.atlassian.fugue.Pair;
import com.atlassian.jira.project.template.descriptor.DemoProjectModuleDescriptor;
import com.atlassian.jira.project.template.module.DefaultDemoProjectModule;
import com.atlassian.jira.project.template.module.DemoProjectModule;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.web.Condition;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class DefaultDemoProjectManagerTest {
    @Rule
    public MockitoRule mockito = MockitoJUnit.rule();

    @Mock
    private PluginAccessor pluginAccessor;

    private DefaultDemoProjectManager manager;

    @Before
    public void setUp() {
        manager = new DefaultDemoProjectManager(pluginAccessor);
    }

    @Test
    public void shouldReturnEnabledDemoModules() {
        List<Pair<DemoProjectModuleDescriptor, DemoProjectModule>> pairs = ImmutableList.of(
                moduleWithDescriptor(true),
                moduleWithDescriptor(true),
                moduleWithDescriptor(true)
        );

        when(pluginAccessor.getEnabledModuleDescriptorsByClass(DemoProjectModuleDescriptor.class)).thenReturn(descriptors(pairs));

        assertEquals(manager.getDemoProjects(), modules(pairs));
    }

    @Test
    public void shouldNotReturnModulesConditionallyDisabled() {
        Pair<DemoProjectModuleDescriptor, DemoProjectModule> enabledModule = moduleWithDescriptor(true);
        Pair<DemoProjectModuleDescriptor, DemoProjectModule> disabledModule = moduleWithDescriptor(false);
        List<Pair<DemoProjectModuleDescriptor, DemoProjectModule>> pairs = ImmutableList.of(enabledModule, disabledModule);

        when(pluginAccessor.getEnabledModuleDescriptorsByClass(DemoProjectModuleDescriptor.class)).thenReturn(descriptors(pairs));

        assertEquals(manager.getDemoProjects(), modules(ImmutableList.of(enabledModule)));
    }

    private static List<DemoProjectModuleDescriptor> descriptors(List<Pair<DemoProjectModuleDescriptor, DemoProjectModule>> pairs) {
        return pairs.stream().map(Pair::left).collect(Collectors.toList());
    }

    private static List<DemoProjectModule> modules(List<Pair<DemoProjectModuleDescriptor, DemoProjectModule>> pairs) {
        return pairs.stream().map(Pair::right).collect(Collectors.toList());
    }

    private Pair<DemoProjectModuleDescriptor, DemoProjectModule> moduleWithDescriptor(boolean isEnabled) {
        DemoProjectModuleDescriptor descriptor = mock(DemoProjectModuleDescriptor.class);
        DemoProjectModule module = mock(DefaultDemoProjectModule.class);
        when(descriptor.getModule()).thenReturn(module);

        Condition condition = mock(Condition.class);
        when(descriptor.getCondition()).thenReturn(condition);
        when(condition.shouldDisplay(ImmutableMap.of())).thenReturn(isEnabled);

        return Pair.pair(descriptor, module);
    }

}