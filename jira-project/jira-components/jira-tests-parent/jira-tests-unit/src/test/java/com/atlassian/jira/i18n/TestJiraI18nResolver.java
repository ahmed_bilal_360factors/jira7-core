package com.atlassian.jira.i18n;

import com.atlassian.jira.junit.rules.InitMockitoMocks;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.I18nHelper;
import com.google.common.collect.ImmutableMap;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;

import java.util.Locale;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;
import static org.mockito.Matchers.startsWith;
import static org.mockito.Mockito.when;

public class TestJiraI18nResolver {
    @Rule
    public InitMockitoMocks mocks = new InitMockitoMocks(this);

    @Mock
    JiraAuthenticationContext jiraAuthenticationContext;
    @Mock
    I18nHelper.BeanFactory beanFactory;
    @Mock
    I18nHelper i18nHelper;

    @Before
    public void setUp() throws Exception {
        when(jiraAuthenticationContext.getLocale()).thenReturn(Locale.getDefault());
        when(beanFactory.getInstance(Locale.getDefault())).thenReturn(i18nHelper);
    }

    @Test
    public void testGetAllTranslationsForPrefix() throws Exception {
        final Map<String, String> translations = new ImmutableMap.Builder<String, String>()
                .put("key1", "value1").put("key2", "value2").put("key3", "value3").build();

        when(i18nHelper.getKeysForPrefix("key")).thenReturn(translations.keySet());
        when(i18nHelper.getUnescapedText(startsWith("key")))
                .thenAnswer(invocation -> translations.get(invocation.getArguments()[0].toString()));

        final JiraI18nResolver i18nResolver = new JiraI18nResolver(jiraAuthenticationContext, beanFactory);
        final Map<String, String> actual = i18nResolver.getAllTranslationsForPrefix("key");

        assertNotSame(actual, translations);
        assertEquals(actual, translations);
    }

    @Test
    public void testResolveText() throws Exception {
        final String[] arguments = new String[]{"p1", "p2", "p3"};
        when(i18nHelper.getText("question", arguments)).thenReturn("answer");

        final JiraI18nResolver i18nResolver = new JiraI18nResolver(jiraAuthenticationContext, beanFactory);
        final String answer = i18nResolver.resolveText("question", arguments);

        assertEquals("answer", answer);
    }

    @Test
    public void testRawText() throws Exception {
        when(i18nHelper.getUnescapedText("question")).thenReturn("answer");

        final JiraI18nResolver i18nResolver = new JiraI18nResolver(jiraAuthenticationContext, beanFactory);
        final String answer = i18nResolver.getRawText("question");

        assertEquals("answer", answer);
    }
}
