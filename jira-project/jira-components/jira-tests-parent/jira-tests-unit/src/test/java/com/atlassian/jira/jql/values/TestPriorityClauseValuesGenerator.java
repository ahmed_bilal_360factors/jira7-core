package com.atlassian.jira.jql.values;

import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.issue.priority.MockPriority;
import com.atlassian.jira.issue.priority.Priority;
import com.google.common.collect.ImmutableList;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestPriorityClauseValuesGenerator {
    @Rule
    public MockitoRule mockito = MockitoJUnit.rule();
    @Mock
    private ConstantsManager constantsManager;
    @InjectMocks
    private PriorityClauseValuesGenerator valuesGenerator;

    @Test
    public void testGetAllConstants() throws Exception {
        final MockPriority priority1 = new MockPriority("1", "Aa it");
        final MockPriority priority2 = new MockPriority("2", "A it");
        final MockPriority priority3 = new MockPriority("3", "B it");
        final MockPriority priority4 = new MockPriority("4", "C it");

        final ImmutableList<Priority> allPrioritiesList = ImmutableList.of(priority4, priority3, priority2, priority1);
        when(constantsManager.getPriorityObjects()).thenReturn(allPrioritiesList);

        assertEquals(allPrioritiesList, valuesGenerator.getAllConstants());
    }
}
