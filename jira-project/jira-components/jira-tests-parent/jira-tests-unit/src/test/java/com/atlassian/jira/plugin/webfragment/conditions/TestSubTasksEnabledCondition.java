package com.atlassian.jira.plugin.webfragment.conditions;

import com.atlassian.jira.config.SubTaskManager;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestSubTasksEnabledCondition {
    @Test
    public void testTrueWhenSubTasksEnabled() {
        final SubTaskManager subTaskManager = mock(SubTaskManager.class);

        when(subTaskManager.isSubTasksEnabled()).thenReturn(true);

        final SubTasksEnabledCondition condition = new SubTasksEnabledCondition(subTaskManager);

        assertTrue(condition.shouldDisplay(null, null));
    }

    @Test
    public void testFalseWhenSubTasksDisabled() {
        final SubTaskManager subTaskManager = mock(SubTaskManager.class);

        when(subTaskManager.isSubTasksEnabled()).thenReturn(false);

        final SubTasksEnabledCondition condition = new SubTasksEnabledCondition(subTaskManager);

        assertFalse(condition.shouldDisplay(null, null));
    }

}
