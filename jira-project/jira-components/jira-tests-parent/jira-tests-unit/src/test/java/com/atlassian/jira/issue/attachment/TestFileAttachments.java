package com.atlassian.jira.issue.attachment;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @since v6.3
 */
public class TestFileAttachments {
    @Rule
    public TemporaryFolder tmpFolder = new TemporaryFolder();

    @Test
    public void computeIssueKeyForOriginalProjectKeyWorksForIssueWithCorrectProjectKey() {
        String projectKey = "PRJ";
        String issueKey = "PRJ-100";
        assertEquals(issueKey, FileAttachments.computeIssueKeyForOriginalProjectKey(projectKey, issueKey));
    }

    @Test
    public void computeIssueKeyForOriginalProjectKeyWorksForIssueWithDifferentProjectKey() {
        String projectKey = "PRX";
        String issueKey = "PRJ-100";
        assertEquals("PRX-100", FileAttachments.computeIssueKeyForOriginalProjectKey(projectKey, issueKey));
    }

    @Test
    public void getAttachmentFileHolderUsingProjectBucketIssueFileFormat() throws Exception {
        File rootDir = tmpFolder.getRoot();
        String projectKey = "PRJ";
        String issueKey = "PRJ-100";
        Attachment attachment = mock(Attachment.class);
        when(attachment.getId()).thenReturn(1L);
        when(attachment.getFilename()).thenReturn("Foo.txt");
        AttachmentKey key = AttachmentKeys.from(projectKey, issueKey, attachment);
        tmpFolder.newFolder(projectKey, "10000", issueKey);
        File expectedFile = tmpFolder.newFile(projectKey + "/10000/" + issueKey + "/1");
        File result = FileAttachments.getAttachmentFileHolder(key, rootDir);
        assertEquals(expectedFile, result);
    }

    @Test
    public void getAttachmentFileHolderProjectBucketIssueFileFormatWithIdBiggerThanInitialBucket()
            throws Exception {
        File rootDir = tmpFolder.getRoot();
        String projectKey = "PRJ";
        String issueKey = "PRJ-10001";
        Attachment attachment = mock(Attachment.class);
        when(attachment.getId()).thenReturn(1L);
        when(attachment.getFilename()).thenReturn("Foo.txt");
        AttachmentKey key = AttachmentKeys.from(projectKey, issueKey, attachment);
        tmpFolder.newFolder(projectKey, "20000", issueKey);
        File expectedFile = tmpFolder.newFile(projectKey + "/20000/" + issueKey + "/1");
        File result = FileAttachments.getAttachmentFileHolder(key, rootDir);
        assertEquals(expectedFile, result);
    }

    @Test
    public void getAttachmentFileHolderProjectIssueFileFormat() throws Exception {
        File rootDir = tmpFolder.getRoot();
        String projectKey = "PRJ";
        String issueKey = "PRJ-100";
        Attachment attachment = mock(Attachment.class);
        when(attachment.getId()).thenReturn(1L);
        when(attachment.getFilename()).thenReturn("Foo.txt");
        AttachmentKey key = AttachmentKeys.from(projectKey, issueKey, attachment);
        tmpFolder.newFolder(projectKey, issueKey);
        File expectedFile = tmpFolder.newFile(projectKey + "/" + issueKey + "/1");
        File result = FileAttachments.getAttachmentFileHolder(key, rootDir);
        assertEquals(expectedFile, result);
    }

    @Test
    public void getAttachmentFileHolderLegacyFileFormat() throws Exception {
        File rootDir = tmpFolder.getRoot();
        String projectKey = "PRJ";
        String issueKey = "PRJ-100";
        Attachment attachment = mock(Attachment.class);
        when(attachment.getId()).thenReturn(1L);
        when(attachment.getFilename()).thenReturn("Foo.txt");
        AttachmentKey key = AttachmentKeys.from(projectKey, issueKey, attachment);
        tmpFolder.newFolder(projectKey, issueKey);
        File expectedFile = tmpFolder.newFile(projectKey + "/" + issueKey + "/1_Foo.txt");
        File result = FileAttachments.getAttachmentFileHolder(key, rootDir);
        assertEquals(expectedFile, result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void computeIssueBucketWhenIssueKeyIsImproper() throws Exception {
        String improperIssueKey = "PRJ/100";
        FileAttachments.computeIssueBucketDir(improperIssueKey);
    }

    @Test(expected = IllegalArgumentException.class)
    public void computeIssueBucketWhenIssueKeyIsEmpty() throws Exception {
        String improperIssueKey = "";
        FileAttachments.computeIssueBucketDir(improperIssueKey);
    }

    @Test(expected = NullPointerException.class)
    public void computeIssueBucketWhenIssueKeyIsNull() throws Exception {
        String improperIssueKey = null;
        FileAttachments.computeIssueBucketDir(improperIssueKey);
    }

    @Test
    public void computeIssueBucketWithValidIssueKey() throws Exception {
        String issueKey = "PRJ-1";
        String expectedBucket = "10000";
        String result = FileAttachments.computeIssueBucketDir(issueKey);
        assertEquals(expectedBucket, result);
    }

    @Test
    public void computeIssueBucketWithIssueKeyLargerThanInitialBucket() throws Exception {
        String issueKey = "PRJ-10001";
        String expectedBucket = "20000";
        String result = FileAttachments.computeIssueBucketDir(issueKey);
        assertEquals(expectedBucket, result);
    }

    @Test
    public void computeIssueBucketWithIssueKeyEqualToBucketSize() throws Exception {
        String issueKey = "PRJ-10000";
        String expectedBucket = "10000";
        String result = FileAttachments.computeIssueBucketDir(issueKey);
        assertEquals(expectedBucket, result);
    }
}
