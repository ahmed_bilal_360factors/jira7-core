package com.atlassian.jira.soy;

import com.atlassian.jira.help.HelpUrl;
import com.atlassian.jira.help.MockHelpUrl;
import com.atlassian.jira.help.MockHelpUrls;
import com.atlassian.soy.renderer.JsExpression;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import javax.annotation.Nonnull;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;

public class AbstractHelpFunctionTest {

    private MockHelpUrls helpUrls;
    private AbstractHelpFunction abstractHelpFunction;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void setUp() {
        helpUrls = new MockHelpUrls();
        helpUrls.addUrl(new MockHelpUrl().setAlt("myAlt").setKey("key1"));
        helpUrls.addUrl(new MockHelpUrl().setAlt("myAlt2").setKey("key2"));
        helpUrls.addUrl(new MockHelpUrl().setKey("key4"));
        abstractHelpFunction = new AbstractHelpFunction(helpUrls) {
            @Nonnull
            @Override
            String getHelpValue(HelpUrl helpUrl) {
                return helpUrl.getAlt();
            }

            @Override
            public String getName() {
                return "testFunction";
            }
        };
    }

    @Test
    public void testGenerateClientSideValueIsBeingEscaped() {
        JsExpression expr = abstractHelpFunction.generate(new JsExpression("'key1'"));
        assertThat(expr.getText(), equalTo("\"myAlt\""));
    }

    @Test
    public void testGenerateClientSideDefinedButValueNotSet() {
        JsExpression expr = abstractHelpFunction.generate(new JsExpression("'key4'"));
        assertThat(expr.getText(), equalTo("\"null\""));
    }

    @Test
    public void testGenerateInvalidArguments() {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Wrong number of arguments (1 expected)");
        abstractHelpFunction.generate();
    }

    @Test
    public void testGenerateRequireStringLiteral() {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("The help key should be a string literal");
        abstractHelpFunction.generate(new JsExpression("key1"));
    }

    @Test
    public void testGenerateClientSideNoSuchKey() {
        JsExpression expr = abstractHelpFunction.generate(new JsExpression("'keyMadeUp'"));
        assertThat(expr.getText(), equalTo("\"alt.default\""));
    }

    @Test
    public void testApplyServerSideReturnsStringUnchanged() {
        String value = abstractHelpFunction.apply("key2");
        assertThat(value, equalTo("myAlt2"));
    }

    @Test
    public void testApplyInvalidArguments() {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Wrong number of arguments (1 expected)");
        abstractHelpFunction.apply();
    }

    @Test
    public void testApplyRequireString() {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("The help key should be a String");
        abstractHelpFunction.apply(new Object());
    }

    @Test
    public void testApplyServerSideNoSuchKey() {
        String value = abstractHelpFunction.apply("keyMadeUp");
        assertThat(value, equalTo("alt.default"));
    }

    @Test
    public void testApplyServerSideDefinedButValueNotSet() {
        String value = abstractHelpFunction.apply("key4");
        assertNull(value);
    }

}
