package com.atlassian.jira.plugin.renderercomponent;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.v2.SubRenderer;
import com.atlassian.renderer.v2.components.RendererComponent;
import com.atlassian.renderer.v2.components.block.BlockRenderer;
import com.atlassian.renderer.v2.components.block.BlockRendererComponent;
import com.atlassian.renderer.v2.components.block.LineWalker;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.hamcrest.Matchers;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @since v3.12
 */
public class TestBlockRendererComponentFactory {
    @Rule
    public MockitoRule mocks = MockitoJUnit.rule();

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @org.mockito.Mock
    private RendererComponentFactoryDescriptor mockRendererComponentFactoryDescriptor;

    @Test
    public void testInitMissingParam() {
        when(mockRendererComponentFactoryDescriptor.getListParams()).thenReturn(Maps.newHashMap());
        when(mockRendererComponentFactoryDescriptor.getCompleteKey()).thenReturn("pluginkey");

        final BlockRendererComponentFactory blockRendererComponentFactory = new BlockRendererComponentFactory(null);

        expectedException.expect(PluginParseException.class);
        expectedException.expectMessage(Matchers.containsString("Missing required list-param"));
        expectedException.expectMessage(Matchers.containsString("pluginkey"));
        blockRendererComponentFactory.init(mockRendererComponentFactoryDescriptor);
    }

    @Test
    public void testInitInvalidClass() throws ClassNotFoundException {
        final Plugin mockPlugin = mock(Plugin.class);

        final Map<String, List<String>> listParams = Maps.newHashMap();
        final List<String> blockRenderers = Lists.newArrayList();
        blockRenderers.add("com.test.invalid.class");
        listParams.put("blockrenderers", blockRenderers);

        when(mockRendererComponentFactoryDescriptor.getListParams()).thenReturn(listParams);
        when(mockRendererComponentFactoryDescriptor.getPlugin()).thenReturn(mockPlugin);
        when(mockRendererComponentFactoryDescriptor.getCompleteKey()).thenReturn("pluginkey");

        final BlockRendererComponentFactory blockRendererComponentFactory = new BlockRendererComponentFactory(null);

        when(
                mockPlugin.loadClass(
                        eq("com.test.invalid.class"),
                        any(Class.class)))
                .thenThrow((Class) ClassNotFoundException.class);

        expectedException.expect(PluginParseException.class);
        expectedException.expectMessage(Matchers.containsString("Could not load block renderer class"));
        expectedException.expectMessage(Matchers.containsString("pluginkey"));
        expectedException.expectMessage(Matchers.containsString("com.test.invalid.class"));
        blockRendererComponentFactory.init(mockRendererComponentFactoryDescriptor);
    }

    @Test
    public void testInitSuccessful() throws PluginParseException, ClassNotFoundException {
        final Plugin mockPlugin = mock(Plugin.class);

        final Map<String, List<String>> listParams = Maps.newHashMap();
        final List<String> blockRenderers = Lists.newArrayList();
        blockRenderers.add(TestBlockRenderer.class.getName());
        listParams.put("blockrenderers", blockRenderers);

        when(mockRendererComponentFactoryDescriptor.getListParams()).thenReturn(listParams);
        when(mockRendererComponentFactoryDescriptor.getPlugin()).thenReturn(mockPlugin);
        when(mockRendererComponentFactoryDescriptor.getCompleteKey()).thenReturn("pluginkey");

        final BlockRendererComponentFactory blockRendererComponentFactory = new BlockRendererComponentFactory(null);

        when(mockPlugin.<TestBlockRenderer>loadClass(TestBlockRenderer.class.getName(), blockRendererComponentFactory.getClass())).
                thenReturn(TestBlockRenderer.class);

        blockRendererComponentFactory.init(mockRendererComponentFactoryDescriptor);
        final RendererComponent rendererComponent = blockRendererComponentFactory.getRendererComponent();
        //noinspection unchecked
        assertThat(rendererComponent, Matchers.isA((Class) BlockRendererComponent.class));
    }

    public static final class TestBlockRenderer implements BlockRenderer {
        public String renderNextBlock(final String arg0, final LineWalker arg1, final RenderContext arg2, final SubRenderer arg3) {
            return null;
        }
    }
}
