package com.atlassian.jira.util.cache;

import com.atlassian.jira.util.cache.WeakInterner.InternReference;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.hamcrest.TypeSafeMatcher;
import org.junit.After;
import org.junit.Test;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static com.atlassian.jira.util.cache.WeakInterner.newWeakInterner;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;

/**
 * @since v6.3
 */
@SuppressWarnings({"RedundantStringConstructorCall", "QuestionableName", "ConstantConditions", "unchecked"})
public class TestWeakInterner {
    private WeakInterner<String> interner = newWeakInterner();

    @After
    public void tearDown() {
        interner = null;
    }

    @Test
    public void testSimpleInterning() {
        final String hello1 = new String("hello");
        final String hello2 = new String("hello");
        final String hello3 = new String("hello");
        final String world1 = new String("world");
        final String world2 = new String("world");
        final String world3 = new String("world");

        assertInternerState(interner);
        assertThat(interner.intern(hello1), sameInstance(hello1));
        assertInternerState(interner, live(hello1));
        assertThat(interner.internOrNull(hello2), sameInstance(hello1));
        assertThat(interner.intern(hello1), sameInstance(hello1));
        assertInternerState(interner, live(hello1));
        assertThat(interner.internOrNull(world2), sameInstance(world2));
        assertInternerState(interner, live(hello1), live(world2));
        assertThat(interner.intern(world1), sameInstance(world2));

        assertThat(interner.intern(world2), sameInstance(world2));
        assertThat(interner.internOrNull(world3), sameInstance(world2));
        assertThat(interner.intern(hello3), sameInstance(hello1));
        assertInternerState(interner, live(hello1), live(world2));
    }

    @Test
    public void testWeaknessWithReferenceProperlyEnqueued() {
        checkWeakness(true);
    }

    @Test
    public void testWeaknessWithReferenceNotYetEnqueued() {
        checkWeakness(false);
    }

    private void checkWeakness(boolean enqueue) {
        String s1 = new String("xyzzy");
        String s2 = new String("xyzzy");
        String s3 = new String("xyzzy");
        String s4 = new String("xyzzy");
        String world = new String("world");

        assertInternerState(interner);
        assertThat(interner.intern(s1), sameInstance(s1));
        assertInternerState(interner, live(s1));
        final InternReference<String> ref = interner.store.keySet().iterator().next();
        assertThat(interner.intern(s4), sameInstance(s1));
        assertInternerState(interner, live(s1));
        assertThat(interner.intern(world), sameInstance(world));
        assertInternerState(interner, live(s1), live(world));

        //noinspection UnusedAssignment
        s4 = null;

        assertThat(interner.internOrNull(s3), sameInstance(s1));
        assertInternerState(interner, live(s1), live(world));

        // Simulate the GC; keep s1 ref to make sure the real GC can't get there first
        ref.clear();
        if (enqueue) {
            assertThat(ref.enqueue(), is(true));
        }

        // We should still have a key with the cleared weak reference
        assertInternerState(interner, dead(s3), live(world));

        assertThat("Interning after the original is cleared should intern the new value", interner.internOrNull(s2), sameInstance(s2));
        assertThat(interner.intern(s3), sameInstance(s2));

        if (enqueue) {
            // The ideal case, where the dead reference was enqueued by the GC before we called intern, so cleanUp() was
            // able to remove it
            assertInternerState(interner, live(s2), live(world));
        } else {
            // The case where the reference has been cleared but not yet enqueued when we see it
            assertInternerState(interner, live(s2), dead(s2), live(world));
        }
    }

    @Test
    public void testInternOrNullWithNullArg() {
        assertThat(interner.internOrNull(null), nullValue());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInternWithNullArg() {
        interner.intern(null);
    }

    private static <T> void assertInternerState(WeakInterner<T> interner, Matcher<InternReference<T>>... keyMatchers) {
        assertThat(interner.store.keySet(), Matchers.containsInAnyOrder(keyMatchers));
    }

    // Matcher that expects the instance given to be the interned value
    private static <T> RefMatcher<T> live(@Nonnull T expectedValue) {
        return new RefMatcher<T>(expectedValue, expectedValue.hashCode());
    }

    // Matcher that expects an interned entry with the same hash as the prototype value, but with its ref cleared
    private static <T> RefMatcher<T> dead(@Nonnull T prototypeValue) {
        return new RefMatcher<T>(null, prototypeValue.hashCode());
    }

    static class RefMatcher<T> extends TypeSafeMatcher<InternReference<T>> {
        final T expectedValue;
        final int expectedHashCode;

        RefMatcher(@Nullable final T expectedValue, final int expectedHashCode) {
            this.expectedValue = expectedValue;
            this.expectedHashCode = expectedHashCode;
        }

        @Override
        @SuppressWarnings("ObjectEquality")  // identity test is intentional
        protected boolean matchesSafely(final InternReference<T> item) {
            return item != null && item.hashCode() == expectedHashCode && expectedValue == item.get();
        }

        @Override
        public void describeTo(final Description description) {
            description.appendText("InternReference@(any)[hash=" + expectedHashCode + ",referent=" + expectedValue);
        }
    }
}