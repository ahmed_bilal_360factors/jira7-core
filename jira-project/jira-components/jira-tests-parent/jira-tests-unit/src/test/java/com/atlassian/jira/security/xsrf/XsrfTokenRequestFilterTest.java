package com.atlassian.jira.security.xsrf;

import com.atlassian.jira.mock.servlet.MockFilterChain;
import com.atlassian.jira.mock.servlet.MockHttpServletRequest;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.IOException;
import java.security.Principal;
import javax.servlet.ServletException;

import static org.mockito.Mockito.mock;

public class XsrfTokenRequestFilterTest {

    @Test
    public void testDoFilter() throws IOException, ServletException {
        final MockFilterChain filterChain = new MockFilterChain();
        final MockHttpServletRequest request = new MockHttpServletRequest();

        final XsrfTokenGenerator xsrfTokenGenerator = mock(XsrfTokenGenerator.class);

        Mockito.when(xsrfTokenGenerator.generateToken(request)).thenReturn("abc1234");

        final XsrfTokenAdditionRequestFilter filter = new XsrfTokenAdditionRequestFilter() {

            @Override
            XsrfTokenGenerator getXsrfTokenGenerator() {
                return xsrfTokenGenerator;
            }
        };

        filter.doFilter(request, null, filterChain);
    }

    @Test
    public void testDoFilter_NoPrincipal() throws IOException, ServletException {
        final MockFilterChain filterChain = new MockFilterChain();
        final MockHttpServletRequest request = new MockHttpServletRequest() {
            @Override
            public Principal getUserPrincipal() {
                return null;
            }
        };

        final XsrfTokenGenerator xsrfTokenGenerator = mock(XsrfTokenGenerator.class);

        Mockito.when(xsrfTokenGenerator.generateToken(request)).thenReturn("abc1234");

        final XsrfTokenAdditionRequestFilter filter = new XsrfTokenAdditionRequestFilter() {

            @Override
            XsrfTokenGenerator getXsrfTokenGenerator() {
                return xsrfTokenGenerator;
            }
        };

        filter.doFilter(request, null, filterChain);
    }
}
