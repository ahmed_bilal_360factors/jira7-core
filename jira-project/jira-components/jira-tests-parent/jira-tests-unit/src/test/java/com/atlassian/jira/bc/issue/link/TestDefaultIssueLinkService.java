package com.atlassian.jira.bc.issue.link;

import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.link.IssueLinkManager;
import com.atlassian.jira.issue.link.IssueLinkType;
import com.atlassian.jira.issue.link.IssueLinkTypeManager;
import com.atlassian.jira.issue.link.LinkCollection;
import com.atlassian.jira.issue.link.LinkCollectionImpl;
import com.atlassian.jira.issue.link.MockIssueLinkType;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.mock.MockApplicationProperties;
import com.atlassian.jira.mock.i18n.MockI18nHelper;
import com.atlassian.jira.mock.issue.MockIssue;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.UserHistoryManager;
import com.atlassian.jira.util.I18nHelper;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.mockito.Mock;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.atlassian.jira.security.Permissions.BROWSE;
import static com.atlassian.jira.security.Permissions.LINK_ISSUE;
import static com.google.common.collect.Lists.newArrayList;
import static java.util.Collections.emptyList;
import static java.util.Collections.emptyMap;
import static java.util.Collections.emptySet;
import static org.hamcrest.Matchers.hasItem;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

public class TestDefaultIssueLinkService {
    @Rule
    public TestRule container = MockitoMocksInContainer.forTest(this);

    private static final String ABC_1 = "ABC-1";
    private static final Long ISSUE_ID = 1L;

    @Mock
    private IssueManager issueManager;
    @Mock
    private PermissionManager permissionManager;

    private I18nHelper.BeanFactory beanFactory = new MockI18nHelper().factory();
    private MockApplicationUser user = new MockApplicationUser("fred");
    private MockI18nHelper i18nHelper;
    private MockIssue issue = new MockIssue(ISSUE_ID);
    @Mock
    private IssueLinkTypeManager issueLinkTypeManager;
    @Mock
    private IssueLinkManager issueLinkManager;
    @Mock
    private ApplicationProperties applicationProperties = new MockApplicationProperties();
    @Mock
    private UserHistoryManager userHistoryManager;

    private DefaultIssueLinkService linkService;

    @Before
    public void setUp() throws Exception {
        linkService = new DefaultIssueLinkService(issueLinkTypeManager, issueManager, permissionManager, beanFactory, issueLinkManager, userHistoryManager);
    }

    private void setupIssueMocks(final MockIssue issue, final int permissionsId, final boolean hasPermission) {
        when(issueManager.getIssueObject(ABC_1)).thenReturn(issue);
        if (issue != null) {
            when(permissionManager.hasPermission(permissionsId, this.issue, user)).thenReturn(hasPermission);
        }
    }

    private ArrayList<IssueLinkType> setupDefaultLinkTypes() {
        return Lists.<IssueLinkType>newArrayList(
                new MockIssueLinkType(1, "Duplicate", "duplicates", "duplicated by", "freestyling"),
                new MockIssueLinkType(1, "Blocker", "blocks", "blocked by", "freestyling"),
                new MockIssueLinkType(1, "Subtask", "subtask", "subtasked by", "jira_subtaks")// <<-- this makes it a system link.  weird eh?
        );
    }

    @Test
    public void testAddValidation_NoIssuePermission() {
        setupIssueMocks(issue, LINK_ISSUE, false);

        when(issueLinkTypeManager.getIssueLinkTypes()).thenReturn(setupDefaultLinkTypes());

        IssueLinkService.AddIssueLinkValidationResult result = linkService.validateAddIssueLinks(user, issue, "duplicates", newArrayList(ABC_1));
        Collection<String> errorMessages = result.getErrorCollection().getErrorMessages();
        assertThat(errorMessages, hasItem("issuelinking.service.error.issue.no.permission [null]"));
    }

    @Test
    public void testAddValidation_NoLinkTypeDefined() {
        setupIssueMocks(null, LINK_ISSUE, true);

        when(issueLinkTypeManager.getIssueLinkTypes()).thenReturn(setupDefaultLinkTypes());

        IssueLinkService.AddIssueLinkValidationResult result = linkService.validateAddIssueLinks(user, issue, "nonexistentlink", newArrayList(ABC_1));
        assertThat(result.getErrorCollection().getErrorMessages(), hasItem("issuelinking.service.error.invalid.link.name [nonexistentlink]"));
    }

    @Test
    public void testAddValidation_SystemLinkType() {
        setupIssueMocks(issue, LINK_ISSUE, true);

        when(issueLinkTypeManager.getIssueLinkTypes()).thenReturn(setupDefaultLinkTypes());

        IssueLinkService.AddIssueLinkValidationResult result = linkService.validateAddIssueLinks(user, issue, "subtask", newArrayList(ABC_1));
        Collection<String> errorMessages = result.getErrorCollection().getErrorMessages();
        assertThat(errorMessages, hasItem("issuelinking.service.error.invalid.link.type [Subtask]"));
    }

    @Test
    public void testAddValidation_NoKeysDefined() {
        setupIssueMocks(issue, LINK_ISSUE, true);

        when(issueLinkTypeManager.getIssueLinkTypes()).thenReturn(setupDefaultLinkTypes());

        IssueLinkService.AddIssueLinkValidationResult result = linkService.validateAddIssueLinks(user, issue, "blocks", emptyList());
        assertTrue(result.getErrorCollection().getErrorMessages().contains("issuelinking.service.error.must.provide.issue.links"));
    }

    @Test
    public void testAddValidation() {
        setupIssueMocks(issue, LINK_ISSUE, true);

        when(issueLinkTypeManager.getIssueLinkTypes()).thenReturn(setupDefaultLinkTypes());

        ArrayList<String> linkKeys = newArrayList(ABC_1);
        IssueLinkService.AddIssueLinkValidationResult result = linkService.validateAddIssueLinks(user, issue, "blocks", linkKeys);

        assertTrue(result.isValid());

        assertEquals(ISSUE_ID, result.getIssue().getId());
        assertEquals(user, result.getUser());
        assertEquals("blocks", result.getLinkName());
        assertEquals(linkKeys, result.getLinkKeys());
    }

    @Test
    public void testGetLinks_NoPermission() {
        setupIssueMocks(issue, Permissions.BROWSE, false);

        IssueLinkService.IssueLinkResult result = linkService.getIssueLinks(user, issue);

        assertEquals(false, result.isValid());
        assertThat(result.getErrorCollection().getErrorMessages(), hasItem("issuelinking.service.error.issue.no.permission [null]"));
    }


    @Test
    public void testGetLinks() {
        setupIssueMocks(issue, BROWSE, true);

        Map<String, List<Issue>> outward = emptyMap();
        Map<String, List<Issue>> inward = emptyMap();
        Set<IssueLinkType> linkTypes = emptySet();
        Long id = issue.getId();
        LinkCollection linkCollection = new LinkCollectionImpl(id, linkTypes, outward, inward, user, applicationProperties);

        when(issueLinkManager.getLinkCollection(issue, user, true)).thenReturn(linkCollection);

        IssueLinkService.IssueLinkResult result = linkService.getIssueLinks(user, issue);

        assertTrue(result.isValid());
        assertNotNull(result.getLinkCollection());
    }

    @Test
    public void testGetEvenSystemLinks() {
        setupIssueMocks(issue, BROWSE, true);

        Map<String, List<Issue>> outward = emptyMap();
        Map<String, List<Issue>> inward = emptyMap();
        Set<IssueLinkType> linkTypes = emptySet();
        Long id = issue.getId();
        LinkCollection linkCollection = new LinkCollectionImpl(id, linkTypes, outward, inward, user, applicationProperties);

        // we expect the delegate call to the manager to preserve the system link flag
        when(issueLinkManager.getLinkCollection(issue, user, false)).thenReturn(linkCollection);

        IssueLinkService.IssueLinkResult result = linkService.getIssueLinks(user, issue, false);

        assertEquals(true, result.isValid());
        assertNotNull(result.getLinkCollection());
    }
}
