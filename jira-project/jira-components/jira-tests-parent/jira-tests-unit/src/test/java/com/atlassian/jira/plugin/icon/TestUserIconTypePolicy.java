package com.atlassian.jira.plugin.icon;

import com.atlassian.jira.avatar.Avatar;
import com.atlassian.jira.icon.IconOwningObjectId;
import com.atlassian.jira.icon.IconType;
import com.atlassian.jira.junit.rules.MockitoContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.mock.MockAvatar;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestUserIconTypePolicy {
    @Rule
    public MockitoContainer mockitoContainer = MockitoMocksInContainer.rule(this);

    @Mock
    private GlobalPermissionManager globalPermissionManager;
    @Mock
    private PermissionManager permissionManager;
    @Mock
    private UserManager userManager;

    @InjectMocks
    private UserIconTypePolicy testObj;

    @Before
    public void before() {
        mockitoContainer.getMockComponentContainer().addMockComponent(UserManager.class, userManager);
    }

    @Test
    public void testUserCanView() throws Exception {
        ApplicationUser user = mock(ApplicationUser.class);
        Avatar systemAvatar = new MockAvatar(123L, "abc.gif", "text/plain", IconType.USER_ICON_TYPE, "hellO", false);

        boolean result = testObj.userCanView(user, systemAvatar);
        assertTrue(result);
    }

    @Test
    public void testUserCanDeleteSelf() throws Exception {
        ApplicationUser user = new MockApplicationUser("hello");
        Avatar systemAvatar = new MockAvatar(123L, "abc.gif", "text/plain", IconType.USER_ICON_TYPE, "hello", false);
        when(userManager.getUserByKey("hello")).thenReturn(user);

        boolean result = testObj.userCanDelete(user, systemAvatar);
        assertTrue(result);
    }

    @Test
    public void testUserCanDeleteOther() throws Exception {
        ApplicationUser user1 = new MockApplicationUser("user");
        ApplicationUser user2 = new MockApplicationUser("user2");
        Avatar systemAvatar = new MockAvatar(123L, "abc.gif", "text/plain", IconType.USER_ICON_TYPE, "user", false);
        when(userManager.getUserByKey("hello")).thenReturn(user1);

        boolean result = testObj.userCanDelete(user2, systemAvatar);
        assertFalse(result);
    }

    @Test
    public void testUserCanCreateForSelf() throws Exception {
        ApplicationUser user1 = new MockApplicationUser("user");
        when(userManager.getUserByKey("user")).thenReturn(user1);

        boolean result = testObj.userCanCreateFor(user1, new IconOwningObjectId("user"));

        assertTrue(result);
    }

    @Test
    public void testUserCanCreateForOther() throws Exception {
        ApplicationUser user1 = new MockApplicationUser("user");
        ApplicationUser user2 = new MockApplicationUser("user2");

        when(userManager.getUserByKey("user")).thenReturn(user1);

        boolean result = testObj.userCanCreateFor(user2, new IconOwningObjectId("user"));

        assertFalse(result);
    }
}