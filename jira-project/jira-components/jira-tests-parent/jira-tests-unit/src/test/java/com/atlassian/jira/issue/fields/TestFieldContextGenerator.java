package com.atlassian.jira.issue.fields;

import com.atlassian.jira.jql.context.ClauseContext;
import com.atlassian.jira.jql.context.ClauseContextImpl;
import com.atlassian.jira.jql.context.ProjectIssueTypeContext;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.web.FieldVisibilityManager;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

/**
 * Unit test for {@link com.atlassian.jira.issue.fields.FieldContextGenerator}.
 *
 * @since v4.0
 */
public class TestFieldContextGenerator {
    @Rule
    public MockitoRule mockito = MockitoJUnit.rule();

    @Mock
    private FieldVisibilityManager visibilityManager;


    private List<Project> projects;
    private MockProject project1;
    private MockProject project2;
    private MockProject project3;

    @Before
    public void setUp() throws Exception {
        project1 = new MockProject();
        project1.setId(1L);
        project2 = new MockProject();
        project2.setId(2L);
        project3 = new MockProject();
        project3.setId(3L);
        projects = Arrays.<Project>asList(project1, project2, project3);

    }

    @Test
    public void testMultipleProjectsAllVisible() throws Exception {
        final FieldContextGenerator contextGenerator = new FieldContextGenerator(visibilityManager);
        final ClauseContext expectedContext = ClauseContextImpl.createGlobalClauseContext();
        final ClauseContext clauseContext = contextGenerator.generateClauseContext(projects, "test");
        assertEquals(expectedContext, clauseContext);
    }

    @Test
    public void testMultipleProjectsOneNotVisible() throws Exception {
        when(visibilityManager.isFieldHiddenInAllSchemes(project1.getId(), "test")).thenReturn(true);

        final FieldContextGenerator contextGenerator = new FieldContextGenerator(visibilityManager);
        final ClauseContext expectedContext = ClauseContextImpl.createGlobalClauseContext();
        final ClauseContext clauseContext = contextGenerator.generateClauseContext(projects, "test");
        assertEquals(expectedContext, clauseContext);
    }

    @Test
    public void testMultipleProjectsAllNotVisible() throws Exception {
        when(visibilityManager.isFieldHiddenInAllSchemes(project1.getId(), "test")).thenReturn(true);
        when(visibilityManager.isFieldHiddenInAllSchemes(project2.getId(), "test")).thenReturn(true);
        when(visibilityManager.isFieldHiddenInAllSchemes(project3.getId(), "test")).thenReturn(true);

        final FieldContextGenerator contextGenerator = new FieldContextGenerator(visibilityManager);
        final ClauseContext expectedContext = new ClauseContextImpl(Collections.<ProjectIssueTypeContext>emptySet());
        final ClauseContext clauseContext = contextGenerator.generateClauseContext(projects, "test");
        assertEquals(expectedContext, clauseContext);
    }

}
