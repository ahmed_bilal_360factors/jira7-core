package com.atlassian.jira.issue.index;

import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.config.util.IndexPathManager;
import com.atlassian.jira.config.util.JiraHome;
import com.atlassian.jira.config.util.MockJiraHome;
import com.atlassian.jira.mock.MockApplicationProperties;
import com.atlassian.jira.util.PathUtils;
import org.junit.Test;

import java.io.File;
import java.util.Optional;

import static org.junit.Assert.assertEquals;

public class TestIndexPathPropertiesAdaptor {

    private final String tempIndexDir = System.getProperty("java.io.tmpdir") + "/index/" + getClass().getName();

    @Test
    public void testIndexRootPath() {
        ApplicationProperties mockApplicationProperties = mockIndexPropertiesWithDefaults();
        IndexPathManager.PropertiesAdaptor indexPathManager = getPathManager(mockApplicationProperties);

        assertEquals(tempIndexDir, indexPathManager.getIndexRootPath());
    }

    @Test
    public void testIndexRootPathUsingDefault() {
        final ApplicationProperties mockApplicationProperties = mockIndexProperties(Optional.of(true), Optional.empty());
        final IndexPathManager.PropertiesAdaptor indexPathManager = getPathManager(mockApplicationProperties);

        assertEquals(new File("/jira-local-home/caches/indexes").getAbsolutePath(), indexPathManager.getIndexRootPath());
    }

    @Test
    public void testIndexRootPathMissingConfig() {
        final ApplicationProperties mockApplicationProperties = mockIndexProperties(Optional.of(false), Optional.empty());
        final IndexPathManager.PropertiesAdaptor indexPathManager = getPathManager(mockApplicationProperties);

        assertEquals(new File("/jira-local-home/caches/indexes").getAbsolutePath(), indexPathManager.getIndexRootPath());
    }

    @Test
    public void testDefaultIndexRootPath() {
        final ApplicationProperties mockApplicationProperties = mockIndexPropertiesWithDefaults();
        final IndexPathManager.PropertiesAdaptor indexPathManager = getPathManager(mockApplicationProperties);

        assertEquals(new File("/jira-local-home/caches/indexes").getAbsolutePath(), indexPathManager.getDefaultIndexRootPath());
    }

    @Test
    public void testIssueIndexPath() {
        final ApplicationProperties mockApplicationProperties = mockIndexPropertiesWithDefaults();
        final IndexPathManager.PropertiesAdaptor indexPathManager = getPathManager(mockApplicationProperties);

        assertEquals(tempIndexDir + File.separator + "issues", indexPathManager.getIssueIndexPath());
    }

    @Test
    public void testCommentIndexPath() {
        final ApplicationProperties mockApplicationProperties = mockIndexPropertiesWithDefaults();
        final IndexPathManager.PropertiesAdaptor indexPathManager = getPathManager(mockApplicationProperties);

        assertEquals(tempIndexDir + File.separator + "comments", indexPathManager.getCommentIndexPath());
    }

    @Test
    public void testPluginIndexRootPath() {
        final ApplicationProperties mockApplicationProperties = mockIndexPropertiesWithDefaults();
        final IndexPathManager.PropertiesAdaptor indexPathManager = getPathManager(mockApplicationProperties);

        assertEquals(tempIndexDir + File.separator + "plugins", indexPathManager.getPluginIndexRootPath());
    }

    @Test
    public void testAppendFileSeparatorChar() {
        assertEquals("dir" + File.separator, PathUtils.appendFileSeparator("dir"));
        assertEquals("dir" + File.separator, PathUtils.appendFileSeparator("dir" + File.separator));
        assertEquals(File.separator, PathUtils.appendFileSeparator(""));
    }

    private ApplicationProperties mockIndexPropertiesWithDefaults() {
        return mockIndexProperties(Optional.of(false), Optional.of(tempIndexDir));
    }

    private ApplicationProperties mockIndexProperties(final Optional<Boolean> useDefaultDirectory, final Optional<String> jiraPathIndex) {
        final ApplicationProperties mockApplicationProperties = new MockApplicationProperties();
        useDefaultDirectory.ifPresent(p -> mockApplicationProperties.setOption(APKeys.JIRA_PATH_INDEX_USE_DEFAULT_DIRECTORY, p));
        jiraPathIndex.ifPresent(p -> mockApplicationProperties.setString(APKeys.JIRA_PATH_INDEX, p));

        return mockApplicationProperties;
    }

    private IndexPathManager.PropertiesAdaptor getPathManager(final ApplicationProperties applicationProperties) {
        final JiraHome mockJiraHome = new MockJiraHome("/jira-local-home/", "jira-shared-home");
        final IndexPathManager.PropertiesAdaptor indexPathManager = new IndexPathManager.PropertiesAdaptor(applicationProperties, mockJiraHome);

        return indexPathManager;
    }
}