package com.atlassian.jira.event.type;

import com.atlassian.cache.CacheManager;
import com.atlassian.cache.memory.MemoryCacheManager;
import com.atlassian.jira.database.MockQueryDslAccessor;
import com.atlassian.jira.database.ResultRow;
import com.atlassian.jira.junit.rules.MockitoContainer;
import com.atlassian.jira.mock.ofbiz.MockOfBizDelegator;
import com.atlassian.jira.notification.NotificationSchemeManager;
import com.atlassian.jira.workflow.WorkflowManager;
import com.google.common.collect.ImmutableList;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;

import static org.mockito.Mockito.when;

/**
 * {@link DefaultEventTypeManager} did not have unit test until JIRA 7.2. We needed to touch it during
 * Vertigo migration, which we used it to migrate to QueryDQL as part of the migration.
 *
 * I have added tests for {@link EventType} CRUD methods as they were the only ones I had to touch.
 *
 * @since 7.2
 */
public class TestDefaultEventTypeManager {
    private static final Long TEST_EVENTTYPE_ID = 666L;

    private final CacheManager cacheManager = new MemoryCacheManager();
    private MockQueryDslAccessor queryDslAccessor;
    private MockOfBizDelegator ofBizDelegator;

    @Rule
    public MockitoContainer container = new MockitoContainer(this);

    @Mock
    private WorkflowManager workflowManager;
    @Mock
    private NotificationSchemeManager notificationSchemeManager;

    private DefaultEventTypeManager eventTypeManager;

    @Before
    public void setUp() throws Exception {
        queryDslAccessor = new MockQueryDslAccessor();
        ofBizDelegator = new MockOfBizDelegator();

        eventTypeManager = new DefaultEventTypeManager(ofBizDelegator, queryDslAccessor, workflowManager,
                notificationSchemeManager, cacheManager);
    }

    private String encode(final String str) {
        return (str == null)?"NULL":"'" + str + "'";
    }

    private String encode(final Long str) {
        return (str == null)?"NULL":"" + str + "";
    }

    private void expectInsert(final String name, final String description, final Long templateId) {
        final String expectedSql = "insert into jiraeventtype (name, description, template_id, id)\n"
                + "values (" + encode(name) + ", " + encode(description) + ", " + encode(templateId) + ", 666)";

        queryDslAccessor.setUpdateResults(expectedSql, 1);
        when(queryDslAccessor.getMockDelegatorInterface().getNextSeqId("EventType")).thenReturn(TEST_EVENTTYPE_ID);
    }

    private void expectDelete() {
        final String expectedSql = "delete from jiraeventtype\n" + "where jiraeventtype.id = 666";
        queryDslAccessor.setUpdateResults(expectedSql, 1);
    }

    private void expectUpdate(final String name, final String description, final Long templateId) {
        final String expectedSql = "update jiraeventtype\n"
                + "set name = " + encode(name) + ", "
                + "description = " + encode(description) + ", "
                + "template_id = " + encode(templateId)
                + "\nwhere jiraeventtype.id = 666";

        queryDslAccessor.setUpdateResults(expectedSql, 1);
    }

    private void expectSelectByAll(final Long id, final String name, final String description, final Long templateId) {
        final String expectedSql = "select EVENT_TYPE.id, EVENT_TYPE.template_id, EVENT_TYPE.name, EVENT_TYPE.description, EVENT_TYPE.event_type\n" +
                "from jiraeventtype EVENT_TYPE";

        final ResultRow resultRow = new ResultRow(id, templateId, name, description, null);
        queryDslAccessor.setQueryResults(expectedSql, ImmutableList.of(resultRow));
    }

    private void expectSelectById(final Long id, final String name, final String description, final Long templateId) {
        final String expectedSql = "select EVENT_TYPE.id, EVENT_TYPE.template_id, EVENT_TYPE.name, EVENT_TYPE.description, EVENT_TYPE.event_type\n" +
                "from jiraeventtype EVENT_TYPE\nwhere EVENT_TYPE.id = " + id + "\nlimit 1";

        final ResultRow resultRow = new ResultRow(id, templateId, name, description, null);
        queryDslAccessor.setQueryResults(expectedSql, ImmutableList.of(resultRow));
    }

    @Test
    public void testGetEventType() throws Exception {
        expectSelectByAll(666L, "NAME", "DESCRIPTION", 1L);
        expectSelectById(555L, "NAME2", "DESCRIPTION2", 2L);
        eventTypeManager.getEventType(666L);
        eventTypeManager.getEventType(555L);
    }

    @Test
    public void testAddEventTypeIgnoresPassedArgumentIdWhenInserting() throws Exception {
        expectInsert("NAME", "DESCRIPTION", 1L);
        eventTypeManager.addEventType(new EventType(-1L, "NAME", "DESCRIPTION", 1L));
    }

    @Test
    public void testAddEventTypeWorks() throws Exception {
        expectInsert("NAME", "DESCRIPTION", 1L);
        eventTypeManager.addEventType(new EventType("NAME", "DESCRIPTION", 1L));
    }

    @Test
    public void testAddEventTypeWorksWhenNoNamePassed() throws Exception {
        expectInsert(null, "DESCRIPTION", 1L);
        eventTypeManager.addEventType(new EventType(null, "DESCRIPTION", 1L));
    }

    @Test
    public void testAddEventTypeWorksWhenNoDescriptionPassed() throws Exception {
        expectInsert("NAME", null, 1L);
        eventTypeManager.addEventType(new EventType("NAME", null, 1L));
    }

    @Test
    public void testAddEventTypeWorksWhenNoTemplateIdPassed() throws Exception {
        expectInsert("NAME", "DESCRIPTION", null);
        eventTypeManager.addEventType(new EventType("NAME", "DESCRIPTION", null));
    }

    @Test
    public void testEditEventType() throws Exception {
        expectUpdate("NAME", "DESCRIPTION", 1L);
        eventTypeManager.editEventType(TEST_EVENTTYPE_ID, "NAME", "DESCRIPTION", 1L);
    }

    @Test
    public void testDeleteEventType() throws Exception {
        expectDelete();
        eventTypeManager.deleteEventType(TEST_EVENTTYPE_ID);
    }
}
