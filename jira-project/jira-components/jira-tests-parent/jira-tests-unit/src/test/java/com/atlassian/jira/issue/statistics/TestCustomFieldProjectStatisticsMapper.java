package com.atlassian.jira.issue.statistics;

import com.atlassian.jira.issue.customfields.searchers.transformer.CustomFieldInputHelper;
import com.atlassian.jira.issue.customfields.statistics.GroupPickerStatisticsMapper;
import com.atlassian.jira.issue.customfields.statistics.ProjectSelectStatisticsMapper;
import com.atlassian.jira.issue.customfields.statistics.TextStatisticsMapper;
import com.atlassian.jira.issue.customfields.statistics.UserPickerStatisticsMapper;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.search.ClauseNames;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.junit.rules.MockitoContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.query.Query;
import com.atlassian.query.QueryImpl;
import com.atlassian.query.clause.TerminalClauseImpl;
import com.atlassian.query.operator.Operator;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestCustomFieldProjectStatisticsMapper {
    @Rule
    public MockitoContainer mockito = MockitoMocksInContainer.rule(this);

    @Mock
    private ProjectManager projectManager;
    @Mock
    private CustomField customField;
    @Mock
    private CustomFieldInputHelper customFieldInputHelper;
    @Mock
    private JiraAuthenticationContext authenticationContext;

    @Test
    public void testEquals() throws Exception {
        final ClauseNames clauseNames = new ClauseNames("cf[10001]");
        when(customField.getClauseNames()).thenReturn(clauseNames);
        when(customField.getId()).thenReturn("customfield_10001");
        when(customField.getClauseNames()).thenReturn(clauseNames);
        when(customField.getId()).thenReturn("customfield_10001");
        when(customField.getClauseNames()).thenReturn(clauseNames);

        final CustomField customField1 = mock(CustomField.class);
        final ClauseNames clauseNames1 = new ClauseNames("cf[10002]");
        when(customField1.getClauseNames()).thenReturn(clauseNames1);
        when(customField1.getId()).thenReturn("customfield_10002");

        CustomFieldProjectStatisticsMapper projectStatisticsMapper = new CustomFieldProjectStatisticsMapper(projectManager, customField, customFieldInputHelper, authenticationContext);

        assertTrue(projectStatisticsMapper.equals(projectStatisticsMapper));
        assertEquals(projectStatisticsMapper.hashCode(), projectStatisticsMapper.hashCode());

        CustomFieldProjectStatisticsMapper projectStatisticsMapper1 = new CustomFieldProjectStatisticsMapper(projectManager, customField, customFieldInputHelper, authenticationContext);
        // As the mappers are using the same custom field they should be equal

        assertTrue(projectStatisticsMapper.equals(projectStatisticsMapper1));
        assertEquals(projectStatisticsMapper.hashCode(), projectStatisticsMapper1.hashCode());

        CustomFieldProjectStatisticsMapper projectStatisticsMapper2 = new CustomFieldProjectStatisticsMapper(projectManager, customField1, customFieldInputHelper, authenticationContext);

        // As the mappers are using different custom field they should *not* be equal
        assertFalse(projectStatisticsMapper.equals(projectStatisticsMapper2));
        assertFalse(projectStatisticsMapper.hashCode() == projectStatisticsMapper2.hashCode());

        assertFalse(projectStatisticsMapper.equals(null));
        assertFalse(projectStatisticsMapper.equals(null));
        assertFalse(projectStatisticsMapper.equals(new Object()));
        assertFalse(projectStatisticsMapper.equals(new IssueKeyStatisticsMapper()));
        assertFalse(projectStatisticsMapper.equals(new IssueTypeStatisticsMapper(null)));
        assertFalse(projectStatisticsMapper.equals(new UserPickerStatisticsMapper(null, null, null)));
        assertFalse(projectStatisticsMapper.equals(new TextStatisticsMapper(null)));
        assertFalse(projectStatisticsMapper.equals(new ProjectSelectStatisticsMapper(null, null)));
        assertFalse(projectStatisticsMapper.equals(new GroupPickerStatisticsMapper(customField, null, authenticationContext, customFieldInputHelper)));
    }

    @Test
    public void testGetUrlSuffixForUniqueCustomFieldName() throws Exception {
        final SearchRequest searchRequest = mock(SearchRequest.class);
        when(searchRequest.getQuery()).thenReturn(new QueryImpl());

        final MockProject project = new MockProject(123, "PR");
        when(projectManager.getProjectObj(123l)).thenReturn(project);

        final ApplicationUser user = new MockApplicationUser("fred");
        when(authenticationContext.getUser()).thenReturn(user);
        when(customField.getId()).thenReturn("customfield_10001");
        final ClauseNames clauseNames = new ClauseNames("cf[10001]");
        when(customField.getClauseNames()).thenReturn(clauseNames);
        when(customField.getClauseNames()).thenReturn(clauseNames);
        when(customField.getName()).thenReturn("My Custom CF");

        when(customFieldInputHelper.getUniqueClauseName(user, "cf[10001]", "My Custom CF")).thenReturn("My Custom CF");

        CustomFieldProjectStatisticsMapper projectStatisticsMapper = new CustomFieldProjectStatisticsMapper(projectManager, customField, customFieldInputHelper, authenticationContext);

        final SearchRequest modifiedSearchRequest = projectStatisticsMapper.getSearchUrlSuffix(project, searchRequest);

        final Query query = modifiedSearchRequest.getQuery();

        assertEquals(new QueryImpl(new TerminalClauseImpl("My Custom CF", Operator.EQUALS, "PR")), query);
    }

    @Test
    public void testGetUrlSuffixForNonUniqueCustomFieldName() throws Exception {
        final SearchRequest searchRequest = mock(SearchRequest.class);
        when(searchRequest.getQuery()).thenReturn(new QueryImpl());

        final MockProject project = new MockProject(123, "PR");
        when(projectManager.getProjectObj(123l)).thenReturn(project);

        final ApplicationUser user = new MockApplicationUser("fred");
        when(authenticationContext.getUser()).thenReturn(user);
        when(customField.getId()).thenReturn("customfield_10001");
        final ClauseNames clauseNames = new ClauseNames("cf[10001]");
        when(customField.getClauseNames()).thenReturn(clauseNames);
        when(customField.getName()).thenReturn("My Custom CF");

        when(customFieldInputHelper.getUniqueClauseName(user, "cf[10001]", "My Custom CF")).thenReturn("cf[10001]");

        CustomFieldProjectStatisticsMapper projectStatisticsMapper = new CustomFieldProjectStatisticsMapper(projectManager, customField, customFieldInputHelper, authenticationContext);

        final SearchRequest modifiedSearchRequest = projectStatisticsMapper.getSearchUrlSuffix(project, searchRequest);

        final Query query = modifiedSearchRequest.getQuery();

        assertEquals(new QueryImpl(new TerminalClauseImpl("cf[10001]", Operator.EQUALS, "PR")), query);
    }

}
