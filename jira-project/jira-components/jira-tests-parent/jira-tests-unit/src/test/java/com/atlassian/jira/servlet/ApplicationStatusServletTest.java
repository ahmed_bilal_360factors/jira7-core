package com.atlassian.jira.servlet;

import com.atlassian.jira.ComponentManager;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoContainer;
import com.atlassian.jira.mock.servlet.MockHttpServletRequest;
import com.atlassian.jira.mock.servlet.MockHttpServletResponse;
import com.atlassian.jira.util.johnson.DefaultJohnsonProvider;
import com.atlassian.jira.util.johnson.JohnsonProvider;
import com.atlassian.jira.util.json.JSONObject;
import com.atlassian.jira.util.system.status.ApplicationState;
import com.atlassian.johnson.Johnson;
import com.atlassian.johnson.event.Event;
import com.atlassian.johnson.event.EventLevel;
import com.atlassian.johnson.event.EventType;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.stubbing.Answer;

import java.io.PrintWriter;
import java.io.StringWriter;

import static javax.servlet.http.HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
import static javax.servlet.http.HttpServletResponse.SC_OK;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;

public class ApplicationStatusServletTest {
    @Rule
    public MockitoContainer mockitoContainer = new MockitoContainer(this);

    @AvailableInContainer
    private JohnsonProvider johnsonProvider = new DefaultJohnsonProvider();

    @Mock
    ComponentManager.State state;
    boolean isUserCacheInitialized;

    @Before
    public void setUp() {
        Johnson.initialize("test-johnson-config.xml");
    }

    @After
    public void tearDown() {
        Johnson.terminate();
    }


    @Test
    public void runningIfSetUp() {
        givenSetUp();
        givenStarted();
        isUserCacheInitialized = true;

        assertResponse(SC_OK, ApplicationState.RUNNING);
    }

    @Test
    public void maintenanceIfSetUpButThereAreWarnings() {
        givenJohnsonWarning();
        givenSetUp();
        givenStarted();
        isUserCacheInitialized = true;

        assertResponse(SC_OK, ApplicationState.MAINTENANCE);
    }

    @Test
    public void startingIfSetUpButUserCacheIsNotReady() {
        givenSetUp();
        givenStarted();

        assertResponse(SC_OK, ApplicationState.STARTING);
    }

    @Test
    public void errorIfJohnsonFatalEvenIfSetUp() {
        givenJohnsonFatal();
        givenSetUp();
        givenStarted();
        assertResponse(SC_INTERNAL_SERVER_ERROR, ApplicationState.ERROR);
    }

    @Test
    public void firstRunIfStartedButNotSetUp() {
        givenStarted();

        assertResponse(SC_OK, ApplicationState.FIRST_RUN);
    }

    @Test
    public void firstRunIfStartedButNotSetUpEvenIfThereAreWarnings() {
        givenJohnsonWarning();
        givenStarted();

        assertResponse(SC_OK, ApplicationState.FIRST_RUN);
    }

    @Test
    public void respondsWithErrorForJohnsonFatal() {
        givenJohnsonFatal();
        givenSetUp();
        givenStarted();

        assertResponse(SC_INTERNAL_SERVER_ERROR, ApplicationState.ERROR);
    }

    @Test
    public void respondsWithErrorForJohnsonFatalWithFirstRun() {
        givenJohnsonFatal();
        givenSetUp();

        assertResponse(SC_INTERNAL_SERVER_ERROR, ApplicationState.ERROR);
    }

    @Test
    public void respondsWithErrorForJohnsonFatalWithWarning() {
        givenJohnsonWarning();
        givenJohnsonFatal();

        assertResponse(SC_INTERNAL_SERVER_ERROR, ApplicationState.ERROR);
    }


    private void assertResponse(int expectedStatus, ApplicationState expectedState) {
        final StringWriter sw = new StringWriter();
        try (PrintWriter pw = new PrintWriter(sw)) {
            final MockHttpServletRequest request = new MockHttpServletRequest();
            final MockHttpServletResponse response = new MockHttpServletResponse(pw);
            new Fixture().doGet(request, response);
            pw.flush();
            final String out = sw.toString();

            assertEquals(out, expectedStatus, response.getStatus());
            assertEquals(out, "application/json", response.getContentType());
            assertEquals(out, expectedState.name(), new JSONObject(out).getString("state"));
        } catch (Exception e) {
            throw new AssertionError(e);
        }
    }

    private static void givenSetUp() {
        ComponentAccessor.getApplicationProperties().setString(APKeys.JIRA_SETUP, "true");
    }

    private void givenStarted() {
        state = mock(ComponentManager.State.class, (Answer<Boolean>) (invocation -> true));
    }

    private void givenJohnsonWarning() {
        final Event event = new Event(EventType.get("clustering"), "Clustering Warning", EventLevel.get("warning"));
        johnsonProvider.getContainer().addEvent(event);
    }

    private void givenJohnsonFatal() {
        final Event event = new Event(EventType.get("clustering"), "Clustering Fatal", EventLevel.get("fatal"));
        johnsonProvider.getContainer().addEvent(event);
    }


    class Fixture extends ApplicationStatusServlet {
        @Override
        protected ComponentManager.State getState() {
            return state;
        }

        @Override
        protected boolean isUserCacheInitialized() {
            return isUserCacheInitialized;
        }
    }
}