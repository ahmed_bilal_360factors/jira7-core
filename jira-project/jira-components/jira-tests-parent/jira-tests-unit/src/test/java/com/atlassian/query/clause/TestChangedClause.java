package com.atlassian.query.clause;

import com.atlassian.query.history.TerminalHistoryPredicate;
import com.atlassian.query.operand.SingleValueOperand;
import com.atlassian.query.operator.Operator;
import org.junit.Test;

import java.util.concurrent.atomic.AtomicBoolean;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * @since v4.0
 */
public class TestChangedClause {

    @Test
    public void testNeverContainsSubClauses() throws Exception {
        assertTrue(new ChangedClauseImpl("testField", Operator.CHANGED, null).getClauses().isEmpty());
    }

    @Test
    public void testToString() throws Exception {
        final ChangedClauseImpl clause = new ChangedClauseImpl("testField", Operator.CHANGED, null);
        assertEquals("{testField changed}", clause.toString());
    }

    @Test
    public void testVisit() throws Exception {
        final AtomicBoolean visitCalled = new AtomicBoolean(false);
        final ClauseVisitor<Void> visitor = new ClauseVisitor<Void>() {
            public Void visit(final AndClause andClause) {
                return failVisitor();
            }

            public Void visit(final NotClause notClause) {
                return failVisitor();
            }

            public Void visit(final OrClause orClause) {
                return failVisitor();
            }

            public Void visit(final TerminalClause clause) {
                return failVisitor();
            }

            @Override
            public Void visit(final WasClause clause) {
                return failVisitor();
            }

            @Override
            public Void visit(final ChangedClause clause) {
                visitCalled.set(true);
                return null;
            }
        };
        new ChangedClauseImpl("testField", Operator.CHANGED, null).accept(visitor);
        assertTrue(visitCalled.get());
    }

    /**
     * JRA-36813: Regression test for this bug.
     */
    @Test
    public void testToStringSeparatesOperandAndPredicateBySpace() {
        final ChangedClauseImpl changedClause = new ChangedClauseImpl("FieldX", Operator.CHANGED,
                new TerminalHistoryPredicate(Operator.ON, new SingleValueOperand(3500000L)));
        assertEquals("{FieldX changed on 3500000}", changedClause.toString());
    }

    private Void failVisitor() {
        fail("Should not be called");
        return null;
    }

}
