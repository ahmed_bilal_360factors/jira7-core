package com.atlassian.jira.action.version;

import com.atlassian.cache.memory.MemoryCacheManager;
import com.atlassian.jira.bc.project.version.VersionBuilderImpl;
import com.atlassian.jira.mock.MockProjectManager;
import com.atlassian.jira.mock.project.MockVersion;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.project.version.CachingVersionStore;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.project.version.VersionImpl;
import com.atlassian.jira.project.version.VersionStore;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import org.hamcrest.collection.IsIterableContainingInOrder;
import org.hamcrest.collection.IsIterableWithSize;
import org.junit.Before;
import org.junit.Test;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.List;

import static java.util.stream.Collectors.toList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

@SuppressWarnings({"deprecation"})
public class TestCachingVersionStore {
    private MemoryVersionStore underlyingStore;
    private CachingVersionStore cachingVersionStore;
    private MockProjectManager projectManager;


    @Before
    public void setUp() throws Exception {
        projectManager = new MockProjectManager();
        underlyingStore = new MemoryVersionStore(projectManager);
        cachingVersionStore = new CachingVersionStore(underlyingStore, new MemoryCacheManager());
    }

    @Test
    public void testGetVersion() throws Exception {
        Version version1 = new MockVersion(1, "v1.0");
        Version version2 = new MockVersion(2, "v1.5");
        Version version3 = new MockVersion(3, "v2.0");

        cachingVersionStore.storeVersion(version1);
        cachingVersionStore.storeVersion(version2);

        assertThat(cachingVersionStore.getAllVersions(), IsIterableWithSize.<Version>iterableWithSize(2));
        assertEquals(version1, Iterables.get(cachingVersionStore.getAllVersions(), 0));
        assertEquals(version1, cachingVersionStore.getVersion(new Long(1)));
        assertEquals(version2, Iterables.get(cachingVersionStore.getAllVersions(), 1));
        assertEquals(version2, cachingVersionStore.getVersion(new Long(2)));

        cachingVersionStore.storeVersion(version3);

        assertThat(cachingVersionStore.getAllVersions(), IsIterableWithSize.<Version>iterableWithSize(3));
        assertEquals(version1, Iterables.get(cachingVersionStore.getAllVersions(), 0));
        assertEquals(version1, cachingVersionStore.getVersion(new Long(1)));
        assertEquals(version2, Iterables.get(cachingVersionStore.getAllVersions(), 1));
        assertEquals(version2, cachingVersionStore.getVersion(new Long(2)));
        assertEquals(version3, Iterables.get(cachingVersionStore.getAllVersions(), 2));
        assertEquals(version3, cachingVersionStore.getVersion(new Long(3)));
    }

    @Test
    public void testCreateAndDeleteModifiesUnderlyingVersionCorrectly() {
        Version version = new VersionImpl(1L, 1L, "version1");
        Version version2 = new VersionImpl(1L, 2L, "version2");

        cachingVersionStore.createVersion(version);
        cachingVersionStore.createVersion(version2);

        //assert created
        assertThat(underlyingStore.getAllVersions(), IsIterableWithSize.<Version>iterableWithSize(2));
        assertThat(underlyingStore.getAllVersions(), IsIterableContainingInOrder.contains(version, version2));

        assertEquals(underlyingStore.getAllVersions(), cachingVersionStore.getAllVersions());

        assertGetVersion(version, (long) 1);
        assertGetVersion(version2, (long) 2);
        assertGetVersion(null, (long) 3);

        cachingVersionStore.deleteVersion(version);
        cachingVersionStore.deleteVersion(version2);

        assertThat(underlyingStore.getAllVersions(), IsIterableWithSize.<Version>iterableWithSize(0));
        assertEquals(underlyingStore.getAllVersions(), cachingVersionStore.getAllVersions());
        assertGetVersion(null, (long) 1);
        assertGetVersion(null, (long) 2);

    }

    @Test
    public void testCreateAndDeleteAllModifiesUnderlyingVersionCorrectly() {
        Project project = new MockProject(1L);
        Project project2 = new MockProject(2L);
        projectManager.addProject(project);
        projectManager.addProject(project2);
        Version version = new VersionImpl(1L, 1L, "version1");
        Version version2 = new VersionImpl(1L, 2L, "version2");
        Version version3 = new VersionImpl(2L, 3L, "version3");

        cachingVersionStore.createVersion(version);
        cachingVersionStore.createVersion(version2);
        cachingVersionStore.createVersion(version3);
        //assert created
        assertThat(underlyingStore.getAllVersions(), IsIterableWithSize.<Version>iterableWithSize(3));
        assertThat(underlyingStore.getAllVersions(), IsIterableContainingInOrder.contains(version, version2, version3));

        assertEquals(underlyingStore.getAllVersions(), cachingVersionStore.getAllVersions());

        assertGetVersion(version, (long) 1);
        assertGetVersion(version2, (long) 2);
        assertGetVersion(version3, (long) 3);
        assertGetVersion(null, (long) 4);

        cachingVersionStore.deleteAllVersions(project.getId());

        assertThat(underlyingStore.getAllVersions(), IsIterableWithSize.<Version>iterableWithSize(1));
        assertThat(underlyingStore.getAllVersions(), IsIterableContainingInOrder.contains(version3));

        assertEquals(underlyingStore.getAllVersions(), cachingVersionStore.getAllVersions());

        assertGetVersion(version3, (long) 3);
        assertGetVersion(null, (long) 1);
        assertGetVersion(null, (long) 2);
    }

    private void assertGetVersion(Version version, Long id) {
        assertEquals(version, underlyingStore.getVersion(id));
        assertEquals(version, cachingVersionStore.getVersion(id));
    }

    private static class MemoryVersionStore implements VersionStore {

        private List<Version> versions = Lists.newArrayList();
        private final ProjectManager projectManager;

        private MemoryVersionStore(final ProjectManager projectManager) {
            this.projectManager = projectManager;
        }

        @Nonnull
        public Iterable<Version> getAllVersions() {
            List<Version> versionGVs = Lists.newArrayListWithCapacity(versions.size());
            for (final Version version1 : versions) {
                versionGVs.add(version1);
            }
            return versionGVs;
        }

        @Nonnull
        public Version createVersion(@Nonnull Version version) {
            versions.add(new VersionBuilderImpl(version).build());
            return version;
        }

        public void storeVersion(@Nonnull Version version) {
            versions.remove(version);
            versions.add(version);
        }

        public void storeVersions(@Nonnull final Collection<Version> versions) {
            for (Version version : versions) {
                if (version != null) {
                    storeVersion(version);
                }
            }
        }

        public Version getVersion(Long id) {
            for (final Version version : versions) {
                if (id.equals(version.getId())) {
                    return version;
                }
            }
            return null;
        }

        public void deleteVersion(@Nonnull Version version) {
            versions.remove(version);
        }

        @Override
        public void deleteAllVersions(@Nonnull final Long projectId) {
            versions = versions.stream()
                    .filter(v -> !v.getProjectId().equals(projectId))
                    .collect(toList());
        }

        @Nonnull
        @Override
        public Iterable<Version> getVersionsByName(String name) {
            throw new UnsupportedOperationException();
        }

        @Nonnull
        @Override
        public Iterable<Version> getVersionsByProject(Long projectId) {
            throw new UnsupportedOperationException();
        }
    }
}
