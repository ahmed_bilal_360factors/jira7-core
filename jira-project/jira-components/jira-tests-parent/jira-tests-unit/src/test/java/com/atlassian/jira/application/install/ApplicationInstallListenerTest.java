package com.atlassian.jira.application.install;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.plugin.event.events.PluginFrameworkStartingEvent;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class ApplicationInstallListenerTest {
    @Rule
    public MockitoRule mocks = MockitoJUnit.rule();

    @InjectMocks
    public ApplicationInstallListener testObj;

    @Mock
    public EventPublisher eventPublisher;

    @Mock
    public ApplicationInstaller installer;

    @Test
    public void shouldPerformInstallationOnlyOnceIfPluginSystemStartsMultipleTimes() {
        // when
        testObj.onPluginSystemStarting(mock(PluginFrameworkStartingEvent.class));
        testObj.onPluginSystemStarting(mock(PluginFrameworkStartingEvent.class));

        //then
        verify(installer, times(1)).installApplications();
    }
}