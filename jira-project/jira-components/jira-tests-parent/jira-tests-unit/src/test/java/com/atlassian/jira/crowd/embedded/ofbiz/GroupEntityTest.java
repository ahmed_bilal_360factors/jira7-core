package com.atlassian.jira.crowd.embedded.ofbiz;

import com.atlassian.crowd.model.group.Group;
import com.atlassian.crowd.model.group.GroupType;
import com.atlassian.crowd.search.query.entity.restriction.constants.GroupTermKeys;
import com.atlassian.gzipfilter.org.apache.commons.lang.BooleanUtils;
import com.atlassian.gzipfilter.org.apache.commons.lang.StringUtils;
import org.junit.Test;

import java.sql.Timestamp;
import java.util.Map;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 * @since v6.0
 */
public class GroupEntityTest {

    @Test
    public void crowdGroupTermKeyName() throws Exception {
        assertThat(GroupEntity.isSystemField(GroupTermKeys.NAME.getPropertyName()), is(true));
        assertThat(GroupEntity.getLowercaseFieldNameFor(GroupTermKeys.NAME.getPropertyName()), is(GroupEntity.LOWER_NAME));
    }

    @Test
    public void descriptionsLongerThan255CharsAreTruncated() {
        String testDescription = StringUtils.rightPad("test", GroupEntity.MAX_DESCRIPTION_LENGTH + 1, "*");
        String truncated = GroupEntity.truncateDescriptionIfRequired(testDescription);
        assertThat(truncated, is(StringUtils.rightPad("test", GroupEntity.MAX_DESCRIPTION_LENGTH, "*")));
    }

    @Test
    public void shortDescriptionNotTruncated() {
        String testDescription = StringUtils.rightPad("test", GroupEntity.MAX_DESCRIPTION_LENGTH - 1, "*");
        String notTruncated = GroupEntity.truncateDescriptionIfRequired(testDescription);
        assertThat(notTruncated, is(testDescription));
    }

    @Test
    public void testGetDataWithLongDescription() {
        Group group = new MockGroup("name", GroupType.GROUP, true, StringUtils.rightPad("Test", GroupEntity.MAX_DESCRIPTION_LENGTH + 1, "*"), 1L);

        Map<String, Object> map = GroupEntity.getData(group, new Timestamp(System.currentTimeMillis()), new Timestamp(System.currentTimeMillis()), true);
        assertThat(map.get(GroupEntity.NAME), is("name"));
        assertThat(map.get(GroupEntity.DESCRIPTION), is(StringUtils.rightPad("Test", GroupEntity.MAX_DESCRIPTION_LENGTH, "*")));
        assertThat(map.get(GroupEntity.LOWER_DESCRIPTION), is(StringUtils.rightPad("test", GroupEntity.MAX_DESCRIPTION_LENGTH, "*")));
        assertThat(map.get(GroupEntity.ACTIVE), is(BooleanUtils.toInteger(true)));
        assertThat(map.get(GroupEntity.TYPE), is(GroupType.GROUP.name()));

    }

    @Test
    public void getDataWithShortDescription() {
        Group group = new MockGroup("name", GroupType.GROUP, true, StringUtils.rightPad("Test", GroupEntity.MAX_DESCRIPTION_LENGTH, "*"), 1L);

        Map<String, Object> map = GroupEntity.getData(group, new Timestamp(System.currentTimeMillis()), new Timestamp(System.currentTimeMillis()), true);
        assertThat(map.get(GroupEntity.NAME), is("name"));
        assertThat(map.get(GroupEntity.DESCRIPTION), is(StringUtils.rightPad("Test", GroupEntity.MAX_DESCRIPTION_LENGTH, "*")));
        assertThat(map.get(GroupEntity.LOWER_DESCRIPTION), is(StringUtils.rightPad("test", GroupEntity.MAX_DESCRIPTION_LENGTH, "*")));
        assertThat(map.get(GroupEntity.ACTIVE), is(BooleanUtils.toInteger(true)));
        assertThat(map.get(GroupEntity.TYPE), is(GroupType.GROUP.name()));
    }

    @Test
    public void whenLowerCasedDescriptionsIncreaseLength() {

        // Change the locale - in this locale \u00cc when lower cased increases the length of the string.
        String tempLanguage = System.getProperty("crowd.identifier.language");
        System.setProperty("crowd.identifier.language", "lt");

        try {
            // This str, when toLowered, will create a tring 757 chars long.
            Group group = new MockGroup("name", GroupType.GROUP, true, StringUtils.rightPad("Test", GroupEntity.MAX_DESCRIPTION_LENGTH, "\u00cc"), 1L);
            Map<String, Object> map = GroupEntity.getData(group, new Timestamp(System.currentTimeMillis()), new Timestamp(System.currentTimeMillis()), true);
            assertThat(((String) map.get(GroupEntity.DESCRIPTION)).length(), is(GroupEntity.MAX_DESCRIPTION_LENGTH));
            assertThat(((String) map.get(GroupEntity.LOWER_DESCRIPTION)).length(), is(GroupEntity.MAX_DESCRIPTION_LENGTH));
        } finally {
            // Clean up our locale setting, just in case...
            if (tempLanguage != null) {
                System.setProperty("crowd.identifier.language", tempLanguage);
            } else {
                System.clearProperty("crowd.identifier.language");
            }
        }

    }
}
