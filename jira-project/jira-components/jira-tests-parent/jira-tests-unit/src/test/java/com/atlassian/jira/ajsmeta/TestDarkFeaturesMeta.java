package com.atlassian.jira.ajsmeta;

import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.plugin.profile.DarkFeatures;
import com.atlassian.jira.util.json.JSONArray;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import java.util.Collections;
import java.util.List;
import java.util.Set;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

public class TestDarkFeaturesMeta {
    @Rule
    public MethodRule initMockito = MockitoJUnit.rule();

    @Mock
    private FeatureManager featureManager;

    @Test
    public void checkJavascriptSyntaxWithNoFeaturesEnabled() throws Exception {
        mockDarkFeatures(Collections.<String>emptySet());
        assertThat(createDarkFeaturesMeta().getContent(), equalTo("[]"));
    }

    @Test
    public void checkJavascriptSyntaxWithOneFeatureEnabled() throws Exception {
        mockDarkFeatures(Sets.newHashSet("feat_1"));
        assertThat(createDarkFeaturesMeta().getContent(), equalTo("[\"feat_1\"]"));
    }

    @Test
    public void checkJavascriptSyntaxWithSeveralFeaturesEnabled() throws Exception {
        mockDarkFeatures(ImmutableSet.of("feat_1", "feat_2"));
        final String content = createDarkFeaturesMeta().getContent();

        final JSONArray jsonContent = new JSONArray(content);
        assertThat(jsonContent.length(), equalTo(2));
        List<String> arrayContent = ImmutableList.of(jsonContent.getString(0), jsonContent.getString(1));
        assertThat(arrayContent, containsInAnyOrder("feat_1", "feat_2"));
    }

    private void mockDarkFeatures(Set<String> stringSet) {
        DarkFeatures darkFeatures = new DarkFeatures(stringSet, Collections.<String>emptySet(), Collections.<String>emptySet());
        when(featureManager.getDarkFeatures()).thenReturn(darkFeatures);
    }

    private DarkFeaturesMeta createDarkFeaturesMeta() {
        return new DarkFeaturesMeta(featureManager);
    }
}
