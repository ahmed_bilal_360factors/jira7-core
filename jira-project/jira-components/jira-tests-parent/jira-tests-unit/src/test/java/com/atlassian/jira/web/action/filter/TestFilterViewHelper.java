package com.atlassian.jira.web.action.filter;

import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.bc.MockJiraServiceContext;
import com.atlassian.jira.bc.filter.SearchRequestService;
import com.atlassian.jira.mock.security.MockSimpleAuthenticationContext;
import com.atlassian.jira.sharing.search.SharedEntitySearchParameters;
import com.atlassian.jira.sharing.type.ShareTypeFactory;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;

/**
 * Some tests for {@link com.atlassian.jira.web.action.filter.TestFilterViewHelper}.
 *
 * @since v3.13
 */
public class TestFilterViewHelper {
    @Rule
    public final MethodRule initMockito = MockitoJUnit.rule();

    @Mock
    private ShareTypeFactory factory;
    @Mock
    private SearchRequestService searchRequestService;
    private ApplicationUser user = new MockApplicationUser("TestFilterViewHelper");

    /**
     * Test to ensure that the SearchRequest service is actually called.
     */
    @Test
    public void serviceIsCalled() {
        final JiraServiceContext jiraServiceContext = new MockJiraServiceContext(user);
        final FilterViewHelper helper = new FilterViewHelper(factory, new MockSimpleAuthenticationContext(user), "applicationContext", "actionUrl", searchRequestService);

        helper.search(jiraServiceContext);

        verify(searchRequestService).search(eq(jiraServiceContext), any(SharedEntitySearchParameters.class), anyInt(), anyInt());

    }
}
