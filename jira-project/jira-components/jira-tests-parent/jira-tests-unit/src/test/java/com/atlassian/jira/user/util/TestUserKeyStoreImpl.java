package com.atlassian.jira.user.util;

import com.atlassian.cache.memory.MemoryCacheManager;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.event.internal.AsynchronousAbleEventDispatcher;
import com.atlassian.event.internal.DirectEventExecutorFactory;
import com.atlassian.event.internal.EventPublisherImpl;
import com.atlassian.event.internal.EventThreadPoolConfigurationImpl;
import com.atlassian.event.internal.ListenerHandlerConfigurationImpl;
import com.atlassian.jira.entity.ApplicationUserEntityFactory;
import com.atlassian.jira.entity.EntityEngine;
import com.atlassian.jira.entity.EntityEngineImpl;
import com.atlassian.jira.event.ClearCacheEvent;
import com.atlassian.jira.mock.ofbiz.MockGenericValue;
import com.atlassian.jira.mock.ofbiz.MockOfBizDelegator;
import com.atlassian.jira.user.ApplicationUserEntity;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.ofbiz.core.entity.DelegatorInterface;
import org.ofbiz.core.entity.GenericValue;

import java.util.Collections;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;

import static com.atlassian.jira.entity.Entity.APPLICATION_USER;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

/**
 * Unit test of UserKeyStoreImpl.
 *
 * @since 6.2
 */
public class TestUserKeyStoreImpl {
    @Rule
    public MockitoRule initMockitoMocks = MockitoJUnit.rule();

    private static final AtomicLong SEQUENCE = new AtomicLong(10000L);

    private MockOfBizDelegator ofBizDelegator;
    private EntityEngine entityEngine;

    @Mock
    private DelegatorInterface delegatorInterface;

    private EventPublisher eventPublisher = new EventPublisherImpl(new AsynchronousAbleEventDispatcher(new DirectEventExecutorFactory(new EventThreadPoolConfigurationImpl())), new ListenerHandlerConfigurationImpl());

    private UserKeyStore keyStore;

    @Before
    public void setUp() {
        when(delegatorInterface.getNextSeqId(anyString())).then(invocationOnMock -> SEQUENCE.getAndIncrement());

        ofBizDelegator = new MockOfBizDelegator(ImmutableList.of(
                user(1, "admin", "adminn"),
                user(2, "fred", "fredn")
        ), Collections.emptyList());
        entityEngine = new EntityEngineImpl(ofBizDelegator);

        keyStore = new UserKeyStoreImpl(entityEngine, ofBizDelegator, delegatorInterface, eventPublisher, new MemoryCacheManager());
    }

    @Test
    public void cacheIsPrepopulatedOnObjectCreation() {
        ofBizDelegator.clear();

        // we have prepopulated cache so clearing the database should not prevent us from getting results
        assertUserIsStoredInCache(1, "admin", "adminn");
        assertUserIsStoredInCache(2, "fred", "fredn");
    }

    @Test
    public void correctUsernameIsRetrievedAfterRenaming() {
        keyStore.renameUser("adminn", "admin2");
        assertUserIsStoredInCache(1, "admin", "admin2");
    }

    @Test
    public void removeByKeyRemovesUserFromAllCaches() {
        keyStore.removeByKey("admin");
        assertUserIsNotStoredInCache(1, "admin", "adminn");
    }

    @Test
    public void gettingByUsernamePerformsToLowerCaseTransformation() {
        assertThat(keyStore.getKeyForUsername("AdminN"), equalTo("admin"));
    }

    @Test
    public void renamingUserPerformsToLowerCaseTransformation() {
        keyStore.renameUser("AdminN", "AdminUsername");
        assertThat(keyStore.getKeyForUsername("adminusername"), equalTo("admin"));
    }

    @Test
    public void ensureUniqueUsernameDoesNotDoAnythingIfMappingAlreadyExists() {
        final String key = keyStore.ensureUniqueKeyForNewUser("adminn");
        assertThat(key, equalTo("admin"));
        assertUserIsStoredInCache(1L, "admin", "adminn");
    }

    @Test
    public void ensureUniqueUsernameCreatesKeyEqualToUsernameIfMappingForUsernameDoesNotExistButKeyIsFree() {
        final String key = keyStore.ensureUniqueKeyForNewUser("Name");
        assertThat(key, equalTo("name"));
        assertUserIsStoredInCache(MockOfBizDelegator.STARTING_ID, "name", "name");
    }

    @Test
    public void ensureUniqueUsernameCreatesUniqueKeyIfKeyEqualToUsernameIsTaken() {
        final String key = keyStore.ensureUniqueKeyForNewUser("admin");
        final long currentSequence = SEQUENCE.get() - 1;
        assertThat(key, equalTo("ID" + currentSequence));
        assertUserIsStoredInCache(currentSequence, key, "admin");
    }

    @Test
    public void cacheIsClearedOnClearCacheEvent() {
        ofBizDelegator.clear();
        eventPublisher.publish(new ClearCacheEvent(Collections.emptyMap()));
        assertUserIsNotStoredInCache(1, "admin", "adminn");
        assertUserIsNotStoredInCache(2, "fred", "fredn");
    }

    private void assertUserIsStoredInCache(final long id, final String key, final String name) {
        assertThat(keyStore.getIdForUserKey(key), equalTo(id));
        assertThat(keyStore.getKeyForUsername(name), equalTo(key));
        assertThat(keyStore.getUsernameForKey(key), equalTo(name));
        assertThat(keyStore.getUserForId(id).get(), equalTo(new ApplicationUserEntity(id, key, name)));
        assertThat(keyStore.getUserForKey(key).get(), equalTo(new ApplicationUserEntity(id, key, name)));
        assertThat(keyStore.getUserForUsername(name).get(), equalTo(new ApplicationUserEntity(id, key, name)));
    }

    private void assertUserIsNotStoredInCache(final long id, final String key, final String name) {
        assertThat(keyStore.getIdForUserKey(key), nullValue());
        assertThat(keyStore.getKeyForUsername(name), nullValue());
        assertThat(keyStore.getUsernameForKey(key), nullValue());
        assertThat(keyStore.getUserForId(id), equalTo(Optional.empty()));
        assertThat(keyStore.getUserForKey(key), equalTo(Optional.empty()));
        assertThat(keyStore.getUserForUsername(name), equalTo(Optional.empty()));
    }

    private GenericValue user(final long id, final String key, final String username) {
        return new MockGenericValue(APPLICATION_USER.getEntityName(), ImmutableMap.of(
                ApplicationUserEntityFactory.ID, id,
                ApplicationUserEntityFactory.USER_KEY, key,
                ApplicationUserEntityFactory.LOWER_USER_NAME, username
        ));
    }
}
