package com.atlassian.jira.sharing.type;

import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.mock.MockProjectRoleManager;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectFactory;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.roles.MockRoleActor;
import com.atlassian.jira.security.roles.ProjectRole;
import com.atlassian.jira.security.roles.ProjectRoleManager;
import com.atlassian.jira.security.roles.RoleActorFactory;
import com.atlassian.jira.sharing.SharePermissionImpl;
import com.atlassian.jira.sharing.search.ProjectShareTypeSearchParameter;
import com.atlassian.jira.sharing.search.ShareTypeSearchParameter;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.google.common.collect.Lists;
import org.apache.lucene.document.Field;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.Query;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.TestRule;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static com.atlassian.core.util.collection.EasyList.build;
import static com.atlassian.jira.permission.ProjectPermissions.BROWSE_PROJECTS;
import static com.google.common.collect.ImmutableSet.of;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

/**
 * A test for ProjectShareQueryFactory
 *
 * @since v3.13
 */
public class TestProjectShareQueryFactory {
    @Rule
    public TestRule container = MockitoMocksInContainer.forTest(this);
    @Rule
    public ExpectedException exception = ExpectedException.none();

    private static final ApplicationUser USER = new MockApplicationUser("admin");

    private static final Long PROJECT_ID_123 = 123l;
    private static final Long ROLE_ID_456 = 456l;
    private static final Project PROJECT_1 = new MockProject(PROJECT_ID_123, "AA", "AA project");

    private static final ProjectRole ROLE_1 = new MockProjectRoleManager.MockProjectRole(ROLE_ID_456, "Role1", "Role1Desc");
    @Mock
    private ProjectManager projectManager;
    @Mock
    private PermissionManager permissionManager;
    @Mock
    private RoleActorFactory roleActorFactory;
    @Mock
    private ProjectRoleManager projectRoleManager;
    @Mock
    private ProjectFactory projectFactory;
    @InjectMocks
    private ProjectShareQueryFactory queryFactory;

    private void basicProjectMocks(boolean hasProjectPermission) {
        when(projectManager.getProjects()).thenReturn(Lists.newArrayList(PROJECT_1));
        when(permissionManager.hasPermission(BROWSE_PROJECTS, PROJECT_1, USER)).thenReturn(hasProjectPermission);
        when(roleActorFactory.getAllRoleActorsForUser(USER)).thenReturn(of(new MockRoleActor(ROLE_ID_456, PROJECT_ID_123, "doesntmatter", "USER")));
    }

    @Test
    public void testGetTerms() {
        basicProjectMocks(true);

        Term[] terms = queryFactory.getTerms(USER);
        assertEquals(2, terms.length);
        assertEquals("shareTypeProject", terms[0].field());
        assertEquals("123:456", terms[0].text());
        assertEquals("shareTypeProject", terms[1].field());
        assertEquals("123", terms[1].text());
    }

    @Test
    public void testGetTermsWithoutProjectPermission() {
        basicProjectMocks(false);

        Term[] terms = queryFactory.getTerms(USER);
        assertEquals(1, terms.length);
        assertEquals("shareTypeProject", terms[0].field());
        assertEquals("123:456", terms[0].text());
    }

    @Test
    public void testGetQuery() {
        // search when we have a project and role id
        ShareTypeSearchParameter searchParameter = new ProjectShareTypeSearchParameter(PROJECT_ID_123, ROLE_ID_456);
        Query query = queryFactory.getQuery(searchParameter);
        assertNotNull(query);
        assertEquals("shareTypeProject:123:456", query.toString());

        // search when we have just a project id
        searchParameter = new ProjectShareTypeSearchParameter(PROJECT_ID_123);
        query = queryFactory.getQuery(searchParameter);
        assertNotNull(query);
        assertEquals("shareTypeProject:123 shareTypeProject:123:*", query.toString());
    }

    @Test
    public void testGetQuery_WithUser_ProjectAndRole() {
        when(projectManager.getProjectObj(PROJECT_ID_123)).thenReturn(PROJECT_1);
        when(permissionManager.hasPermission(BROWSE_PROJECTS, PROJECT_1, USER)).thenReturn(true);
        when(projectRoleManager.getProjectRole(ROLE_ID_456)).thenReturn(ROLE_1);
        when(projectRoleManager.isUserInProjectRole(new MockApplicationUser(USER.getName(), USER.getDisplayName(), USER.getEmailAddress()), ROLE_1, PROJECT_1)).thenReturn(true);

        // search when we have a project and role id
        ShareTypeSearchParameter searchParameter = new ProjectShareTypeSearchParameter(PROJECT_ID_123, ROLE_ID_456);
        Query query = queryFactory.getQuery(searchParameter, USER);
        assertNotNull(query);
        assertEquals("shareTypeProject:123:456", query.toString());
    }

    @Test
    public void testGetQuery_WithUser_ProjectOnly() {
        when(projectManager.getProjectObj(PROJECT_ID_123)).thenReturn(PROJECT_1);
        when(permissionManager.hasPermission(BROWSE_PROJECTS, PROJECT_1, USER)).thenReturn(true);
        when(projectManager.getProjectObj(PROJECT_ID_123)).thenReturn(PROJECT_1);
        when(projectRoleManager.getProjectRoles(new MockApplicationUser(USER.getName(), USER.getDisplayName(), USER.getEmailAddress()), PROJECT_1)).thenReturn(build(ROLE_1));

        // search when we have a project only
        ShareTypeSearchParameter searchParameter = new ProjectShareTypeSearchParameter(PROJECT_ID_123);
        Query query = queryFactory.getQuery(searchParameter, USER);
        assertNotNull(query);
        assertEquals("shareTypeProject:123 shareTypeProject:123:456", query.toString());
    }


    @Test
    public void testGetQuery_WithNullUser() {
        when(projectManager.getProjectObj(PROJECT_ID_123)).thenReturn(PROJECT_1);
        when(permissionManager.hasPermission(BROWSE_PROJECTS, PROJECT_1, null)).thenReturn(false);

        // search when we have a project and role id
        ShareTypeSearchParameter searchParameter = new ProjectShareTypeSearchParameter(PROJECT_ID_123, ROLE_ID_456);

        exception.expect(IllegalStateException.class);
        queryFactory.getQuery(searchParameter, (ApplicationUser) null);
    }

    @Test
    public void testGetField() {
        final SharePermissionImpl projectSharePermission = new SharePermissionImpl(new ShareType.Name("project"), String.valueOf(PROJECT_ID_123), String.valueOf(ROLE_ID_456));

        Field field = queryFactory.getField(null, projectSharePermission);

        assertNotNull(field);
        assertEquals("shareTypeProject", field.name());
        assertTrue(field.isStored());
        assertTrue(field.isIndexed());
        assertEquals("123:456", field.stringValue());
    }
}
