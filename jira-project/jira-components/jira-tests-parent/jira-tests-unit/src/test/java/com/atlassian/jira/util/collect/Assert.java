package com.atlassian.jira.util.collect;

import static org.junit.Assert.fail;

class Assert {
    static void assertUnsupportedOperation(final Runnable runnable) {
        try {
            runnable.run();
            fail("UnsupportedOperationException expected");
        } catch (final UnsupportedOperationException expected) {
        }
    }

    private Assert() {
    }
}
