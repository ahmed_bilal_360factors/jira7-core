package com.atlassian.jira.plugin.webfragment.conditions;

import com.atlassian.jira.bc.issue.attachment.AttachmentService;
import com.atlassian.jira.issue.Issue;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

public class TestCanAttachFileToIssueCondition {
    @Rule
    public MockitoRule mockito = MockitoJUnit.rule();

    @Mock
    private Issue issue;
    @Mock
    private AttachmentService attachmentService;

    @Test
    public void testTrue() {
        when(attachmentService.canCreateAttachments(Mockito.any(), Mockito.same(issue))).thenReturn(true);

        final CanAttachFileToIssueCondition condition = new CanAttachFileToIssueCondition(attachmentService);

        assertTrue(condition.shouldDisplay(null, issue, null));
    }

    @Test
    public void testFalse() {
        when(attachmentService.canCreateAttachments(Mockito.any(), Mockito.same(issue))).thenReturn(false);

        final CanAttachFileToIssueCondition condition = new CanAttachFileToIssueCondition(attachmentService);

        assertFalse(condition.shouldDisplay(null, issue, null));

    }
}
