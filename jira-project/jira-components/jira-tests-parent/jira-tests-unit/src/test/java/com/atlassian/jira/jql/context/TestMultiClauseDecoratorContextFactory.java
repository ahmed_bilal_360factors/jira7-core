package com.atlassian.jira.jql.context;

import com.atlassian.jira.jql.operand.JqlOperandResolver;
import com.atlassian.jira.jql.validator.OperatorUsageValidator;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.collect.CollectionBuilder;
import com.atlassian.query.clause.TerminalClause;
import com.atlassian.query.clause.TerminalClauseImpl;
import com.atlassian.query.operand.EmptyOperand;
import com.atlassian.query.operand.MultiValueOperand;
import com.atlassian.query.operand.SingleValueOperand;
import com.atlassian.query.operator.Operator;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import static com.atlassian.jira.jql.operand.SimpleLiteralFactory.createLiteral;
import static com.atlassian.jira.jql.validator.MockJqlOperandResolver.createSimpleSupport;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Test for {@link com.atlassian.jira.jql.context.MultiClauseDecoratorContextFactory}.
 *
 * @since v4.0
 */
public class TestMultiClauseDecoratorContextFactory {
    @Rule
    public final MethodRule initMockito = MockitoJUnit.rule();

    @Mock
    private ClauseContextFactory delegate;
    @Mock
    private JqlOperandResolver jqlOperandResolver;
    @Mock
    private ContextSetUtil contextSetUtil;
    @Mock
    private OperatorUsageValidator operatorUsageValidator;

    @InjectMocks
    private MultiClauseDecoratorContextFactory factory;
    @InjectMocks
    private MultiClauseDecoratorContextFactory.Factory factoryFactory;

    @Test
    public void testGetClauseContextNoLiterals() throws Exception {
        final TerminalClause terminal = new TerminalClauseImpl("one", Operator.IN, EmptyOperand.EMPTY);

        final ClauseContext clauseContext = factory.getClauseContext(null, terminal);
        assertEquals(ClauseContextImpl.createGlobalClauseContext(), clauseContext);
        verify(jqlOperandResolver).getValues((ApplicationUser) null, terminal.getOperand(), terminal);
    }

    @Test
    public void testGetClauseContextNotList() throws Exception {
        final ClauseContext expectedResult = createIssueTypeContext(1, 2, 56);
        final SingleValueOperand operand = new SingleValueOperand("one");
        final TerminalClause terminal = new TerminalClauseImpl("one", Operator.NOT_IN, operand);

        when(jqlOperandResolver.getValues((ApplicationUser) null, operand, terminal))
                .thenReturn(Collections.singletonList(createLiteral(1L)));
        when(jqlOperandResolver.isListOperand(operand)).thenReturn(false);

        when(delegate.getClauseContext(null, terminal)).thenReturn(expectedResult);

        final ClauseContext clauseContext = factory.getClauseContext(null, terminal);
        assertEquals(expectedResult, clauseContext);
    }

    @Test
    public void testGetClauseContextBadOperator() throws Exception {
        final ClauseContext expectedResult = createIssueTypeContext(1, 2, 56);

        final TerminalClause terminal = new TerminalClauseImpl("one", Operator.LIKE, new MultiValueOperand("one"));

        when(delegate.getClauseContext(null, terminal)).thenReturn(expectedResult);

        final MultiClauseDecoratorContextFactory factory =
                new MultiClauseDecoratorContextFactory(createSimpleSupport(), delegate, contextSetUtil);

        final ClauseContext clauseContext = factory.getClauseContext(null, terminal);
        assertEquals(expectedResult, clauseContext);
    }

    @Test
    public void testGetClauseContextIn() throws Exception {
        final ClauseContext clauseCtx1 = createIssueTypeContext(1, 2, 56);
        final ClauseContext expectedResult = createIssueTypeContext(573832222);
        final Set<ClauseContext> unionedCtxs =
                CollectionBuilder.<ClauseContext>newBuilder(clauseCtx1).asSet();

        final TerminalClause terminal = new TerminalClauseImpl("one", Operator.IN, new MultiValueOperand("one", "two"));

        when(delegate.getClauseContext(null,
                new TerminalClauseImpl("one", Operator.EQUALS, new MultiValueOperand("one", "two")))).thenReturn(
                clauseCtx1);

        when(contextSetUtil.union(unionedCtxs)).thenReturn(expectedResult);

        factory = new MultiClauseDecoratorContextFactory(createSimpleSupport(), delegate, contextSetUtil);

        final ClauseContext clauseContext = factory.getClauseContext(null, terminal);
        assertEquals(expectedResult, clauseContext);
    }

    @Test
    public void testGetClauseContextNotIn() throws Exception {
        final ClauseContext clauseCtx1 = createIssueTypeContext(4835490, 35354, 54353);
        final ClauseContext expectedResult = createIssueTypeContext(22);
        final Set<ClauseContext> unionedCtxs =
                CollectionBuilder.<ClauseContext>newBuilder(clauseCtx1).asSet();

        final TerminalClause terminal = new TerminalClauseImpl("one", Operator.NOT_IN,
                new MultiValueOperand(
                        new SingleValueOperand("one"),
                        new SingleValueOperand("two"),
                        EmptyOperand.EMPTY));

        when(delegate.getClauseContext(null,
                new TerminalClauseImpl("one", Operator.NOT_EQUALS,
                        new MultiValueOperand(new SingleValueOperand("one"), new SingleValueOperand("two"),
                                EmptyOperand.EMPTY)))).thenReturn(
                clauseCtx1);

        when(contextSetUtil.intersect(unionedCtxs)).thenReturn(expectedResult);

        factory = new MultiClauseDecoratorContextFactory(createSimpleSupport(), delegate, contextSetUtil);

        final ClauseContext clauseContext = factory.getClauseContext(null, terminal);
        assertEquals(expectedResult, clauseContext);
    }

    @Test
    public void testFactoryNoValidate() throws Exception {
        final ClauseContextFactory clauseContextFactory = factoryFactory.create(delegate, false);
        assertTrue(clauseContextFactory instanceof MultiClauseDecoratorContextFactory);
    }

    @Test
    public void testFactoryValidate() throws Exception {
        final ClauseContextFactory clauseContextFactory = factoryFactory.create(mock(ClauseContextFactory.class), true);
        assertTrue(clauseContextFactory instanceof ValidatingDecoratorContextFactory);
    }

    @Test
    public void testFactoryDefault() throws Exception {
        final ClauseContextFactory clauseContextFactory = factoryFactory.create(mock(ClauseContextFactory.class));
        assertTrue(clauseContextFactory instanceof ValidatingDecoratorContextFactory);
    }

    private static ClauseContext createIssueTypeContext(int... types) {
        Set<ProjectIssueTypeContext> ctxs = new HashSet<ProjectIssueTypeContext>();
        for (int type : types) {
            ctxs.add(new ProjectIssueTypeContextImpl(AllProjectsContext.INSTANCE,
                    new IssueTypeContextImpl(String.valueOf(type))));
        }

        return new ClauseContextImpl(ctxs);
    }
}
