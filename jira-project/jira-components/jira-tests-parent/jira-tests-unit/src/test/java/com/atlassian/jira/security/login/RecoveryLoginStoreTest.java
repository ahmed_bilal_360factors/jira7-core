package com.atlassian.jira.security.login;

import com.atlassian.core.util.Clock;
import com.atlassian.jira.bc.security.login.LoginInfo;
import com.atlassian.jira.bc.security.login.LoginInfoImpl;
import com.atlassian.jira.junit.rules.MockitoContainer;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.util.RecoveryMode;
import com.atlassian.jira.user.util.StaticRecoveryMode;
import com.atlassian.jira.util.ConstantClock;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Mockito.only;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;


public class RecoveryLoginStoreTest {
    private static final ApplicationUser RECOVERY_USER = new MockApplicationUser("recovery_user");
    private static final ApplicationUser NORMAL_USER = new MockApplicationUser("normal_user");
    private static final long NOW = 11037383L;

    @Rule
    public MockitoContainer initMockitoMocks = new MockitoContainer(this);

    @Mock
    private LoginStore delegate;
    private Clock clock = new ConstantClock(NOW);
    private RecoveryMode mode = new StaticRecoveryMode(RECOVERY_USER.getName());
    private RecoveryLoginStore loginStore;

    @Before
    public void before() {
        loginStore = new RecoveryLoginStore(mode, delegate, clock);
    }

    @Test
    public void getLoginInfoForRecoveryReturnsEmptyLoginInfo() {
        //when
        final LoginInfo actualLoginInfo = loginStore.getLoginInfo(RECOVERY_USER);

        //then
        assertThat(actualLoginInfo, equalTo(LoginInfoImpl.builder().build()));
        verifyZeroInteractions(delegate);
    }

    @Test
    public void getLoginInfoDelegatesForNormalUser() {
        //given
        final LoginInfo loginInfo = LoginInfoImpl.builder().setLoginCount(10L).build();
        when(delegate.getLoginInfo(NORMAL_USER)).thenReturn(loginInfo);

        //when
        final LoginInfo actualLoginInfo = loginStore.getLoginInfo(NORMAL_USER);

        //then
        assertThat(actualLoginInfo, equalTo(loginInfo));
    }

    @Test
    public void recordLoginAttemptSetsCorrectLoginInfoForSuccessfulLogin() {
        //when
        final LoginInfo origInfo = loginStore.getLoginInfo(RECOVERY_USER);
        loginStore.recordLoginAttempt(RECOVERY_USER, true);

        //then
        final LoginInfo expectedLoginInfo = LoginInfoImpl.builder(origInfo)
                .succeededAt(NOW)
                .build();

        assertThat(loginStore.getLoginInfo(RECOVERY_USER), equalTo(expectedLoginInfo));
        verifyZeroInteractions(delegate);
    }

    @Test
    public void recordLoginAttemptSetsCorrectLoginInfoForFailedLogin() {
        //when
        final LoginInfo origInfo = loginStore.getLoginInfo(RECOVERY_USER);
        loginStore.recordLoginAttempt(RECOVERY_USER, false);

        //then
        final LoginInfo expectedLoginInfo = LoginInfoImpl.builder(origInfo)
                .failedAt(NOW)
                .build();

        assertThat(loginStore.getLoginInfo(RECOVERY_USER), equalTo(expectedLoginInfo));
        verifyZeroInteractions(delegate);
    }

    @Test
    public void recordLoginAttemptDelegatesForNormalUser() {
        //given
        final LoginInfo loginInfo = LoginInfoImpl.builder().setLoginCount(11L).build();
        when(delegate.recordLoginAttempt(NORMAL_USER, true)).thenReturn(loginInfo);

        //when
        final LoginInfo actualLoginInfo = loginStore.recordLoginAttempt(NORMAL_USER, true);

        //then
        assertThat(actualLoginInfo, equalTo(loginInfo));
    }

    @Test
    public void getMaxAuthenticationAttemptsAllowedDelegates() {
        //given
        when(delegate.getMaxAuthenticationAttemptsAllowed()).thenReturn(5L);

        //when
        final long count = loginStore.getMaxAuthenticationAttemptsAllowed();

        //then
        assertThat(count, equalTo(5L));
    }

    @Test
    public void resetFailedLoginCountSetCountForRecoveryUser() {
        //when
        final LoginInfo origInfo = loginStore.getLoginInfo(RECOVERY_USER);
        loginStore.resetFailedLoginCount(RECOVERY_USER);

        //then
        final LoginInfo expectedLoginInfo = LoginInfoImpl.builder(origInfo).setLoginCount(0L).build();
        assertThat(loginStore.getLoginInfo(RECOVERY_USER), equalTo(expectedLoginInfo));
        verifyZeroInteractions(delegate);
    }

    @Test
    public void resetFailedLoginCountDelegatesForNormalUser() {
        //when
        loginStore.resetFailedLoginCount(NORMAL_USER);

        //then
        verify(delegate, only()).resetFailedLoginCount(NORMAL_USER);
    }
}
