package com.atlassian.jira.jql.validator;

import com.atlassian.jira.jql.operand.JqlOperandResolver;
import com.atlassian.jira.jql.resolver.ProjectIndexInfoResolver;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.collect.CollectionBuilder;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import static com.atlassian.jira.permission.ProjectPermissions.BROWSE_PROJECTS;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestProjectValuesExistValidator {
    @Rule
    public MockitoRule mockito = MockitoJUnit.rule();
    @Mock
    private JqlOperandResolver operandResolver;
    @Mock
    private ProjectIndexInfoResolver indexInfoResolver;
    @Mock
    private PermissionManager permissionManager;
    @Mock
    private ProjectManager projectManager;
    @Mock
    private I18nHelper.BeanFactory beanFactory;
    @InjectMocks
    private ProjectValuesExistValidator validator;

    @Test
    public void testProjectExistsAndHasPermission() throws Exception {
        final String key = "proj";
        final Long id = 10L;

        MockProject project = new MockProject(id, key);

        when(indexInfoResolver.getIndexedValues(key)).thenReturn(Collections.singletonList(id.toString()));
        when(projectManager.getProjectObj(id)).thenReturn(project);
        when(permissionManager.hasPermission(BROWSE_PROJECTS, project, null)).thenReturn(true);

        assertTrue(validator.stringValueExists(null, key));
    }

    @Test
    public void testProjectExistsAndHasNoPermission() throws Exception {
        final String key = "proj";
        final Long id = 10L;

        MockProject project = new MockProject(id, key);

        when(indexInfoResolver.getIndexedValues(key)).thenReturn(Collections.singletonList(id.toString()));
        when(projectManager.getProjectObj(id)).thenReturn(project);
        when(permissionManager.hasPermission(BROWSE_PROJECTS, project, null)).thenReturn(false);

        assertFalse(validator.stringValueExists(null, key));
    }

    @Test
    public void testProjectDoesntExist() throws Exception {
        final String key = "proj";

        when(indexInfoResolver.getIndexedValues(key)).thenReturn(Collections.emptyList());

        assertFalse(validator.stringValueExists(null, key));

    }

    @Test
    public void testTwoProjectsExistsAndOneHasPermission() throws Exception {
        final String name = "proj";
        final Long id1 = 10L;
        final Long id2 = 20L;

        MockProject project1 = new MockProject(id1, "key", name);
        MockProject project2 = new MockProject(id2, "key", name);

        when(indexInfoResolver.getIndexedValues(name)).thenReturn(CollectionBuilder.newBuilder(id1.toString(), id2.toString()).asList());
        when(projectManager.getProjectObj(id1)).thenReturn(project1);
        when(permissionManager.hasPermission(BROWSE_PROJECTS, project1, null)).thenReturn(false);
        when(projectManager.getProjectObj(id2)).thenReturn(project2);
        when(permissionManager.hasPermission(BROWSE_PROJECTS, project2, null)).thenReturn(true);

        assertTrue(validator.stringValueExists(null, name));
    }

    @Test
    public void testLongValueExist() throws Exception {
        Long id = 10L;
        when(indexInfoResolver.getIndexedValues(id)).thenReturn(Collections.emptyList());
        when(indexInfoResolver.getIndexedValues(id)).thenReturn(Collections.emptyList());

        final AtomicInteger called = new AtomicInteger(0);
        ProjectValuesExistValidator validator = new ProjectValuesExistValidator(operandResolver, indexInfoResolver, permissionManager, projectManager, beanFactory) {
            @Override
            boolean projectExists(final ApplicationUser searcher, final List<String> ids) {
                return called.incrementAndGet() == 1;
            }
        };

        assertTrue(validator.longValueExist(null, id));
        assertFalse(validator.longValueExist(null, id));

        assertEquals(2, called.get());
    }

}
