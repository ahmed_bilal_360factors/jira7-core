package com.atlassian.jira.jql.query;

import com.atlassian.jira.issue.search.MockSearchRequest;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.jql.operand.JqlOperandResolver;
import com.atlassian.jira.jql.operand.QueryLiteral;
import com.atlassian.jira.jql.operator.OperatorClasses;
import com.atlassian.jira.jql.resolver.SavedFilterResolver;
import com.atlassian.jira.jql.validator.SavedFilterCycleDetector;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.collect.CollectionBuilder;
import com.atlassian.query.clause.TerminalClause;
import com.atlassian.query.clause.TerminalClauseImpl;
import com.atlassian.query.operand.MultiValueOperand;
import com.atlassian.query.operand.SingleValueOperand;
import com.atlassian.query.operator.Operator;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.MatchAllDocsQuery;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TermQuery;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import static com.atlassian.jira.jql.operand.SimpleLiteralFactory.createLiteral;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */

public class TestSavedFilterClauseQueryFactory {
    @Rule
    public final MethodRule initMockito = MockitoJUnit.rule();

    @Mock
    private SavedFilterResolver savedFilterResolver;
    @Mock
    private JqlOperandResolver jqlOperandResolver;
    @Mock
    private SavedFilterCycleDetector savedFilterCycleDetector;
    private ApplicationUser theUser = null;
    private QueryCreationContext queryCreationContext;
    @Mock
    private QueryVisitor queryVisitor;
    private boolean overrideSecurity = false;

    @Before
    public void setUp() throws Exception {
        queryCreationContext = new QueryCreationContextImpl(theUser);
    }

    @Test
    public void testGetQueryBadOperators() throws Exception {
        final SavedFilterClauseQueryFactory factory = new SavedFilterClauseQueryFactory(savedFilterResolver, jqlOperandResolver, null, queryVisitor);

        final Operator[] operators = Operator.values();
        for (Operator operator : operators) {
            if (!OperatorClasses.EQUALITY_OPERATORS.contains(operator)) {
                assertEquals(QueryFactoryResult.createFalseResult(), factory.getQuery(queryCreationContext, new TerminalClauseImpl("savedFilter", operator, "blah")));
            }
        }
    }

    @Test
    public void testGetQueryNoFiltersFound() throws Exception {
        final SingleValueOperand filter1Operand = new SingleValueOperand("filter1");
        final SingleValueOperand filter2Operand = new SingleValueOperand("filter2");
        final SingleValueOperand filter3Operand = new SingleValueOperand(123L);
        final List<QueryLiteral> queryLiterals = CollectionBuilder.newBuilder(createLiteral("filter1"), createLiteral("filter2"), createLiteral(123L)).asList();

        final MultiValueOperand operand = new MultiValueOperand(CollectionBuilder.newBuilder(filter1Operand, filter2Operand, filter3Operand).asList());
        TerminalClause clause = new TerminalClauseImpl("test", Operator.IN, operand);

        when(savedFilterResolver.getSearchRequest(theUser, queryLiterals)).thenReturn(Collections.emptyList());

        when(jqlOperandResolver.getValues(queryCreationContext, operand, clause)).thenReturn(queryLiterals);
        final SavedFilterClauseQueryFactory filterClauseQueryFactory = new SavedFilterClauseQueryFactory(savedFilterResolver, jqlOperandResolver, null, queryVisitor);
        final QueryFactoryResult query = filterClauseQueryFactory.getQuery(queryCreationContext, clause);

        assertEquals(new BooleanQuery(), query.getLuceneQuery());
        assertFalse(query.mustNotOccur());
    }

    @Test
    public void testGetQueryNoFiltersFoundOverrideSecurity() throws Exception {
        overrideSecurity = true;
        queryCreationContext = new QueryCreationContextImpl(theUser, overrideSecurity);

        final SingleValueOperand filter1Operand = new SingleValueOperand("filter1");
        final SingleValueOperand filter2Operand = new SingleValueOperand("filter2");
        final SingleValueOperand filter3Operand = new SingleValueOperand(123L);
        final List<QueryLiteral> queryLiterals = CollectionBuilder.newBuilder(createLiteral("filter1"), createLiteral("filter2"), createLiteral(123L)).asList();

        final MultiValueOperand operand = new MultiValueOperand(CollectionBuilder.newBuilder(filter1Operand, filter2Operand, filter3Operand).asList());
        TerminalClause clause = new TerminalClauseImpl("test", Operator.IN, operand);

        when(savedFilterResolver.getSearchRequestOverrideSecurity(queryLiterals)).thenReturn(Collections.<SearchRequest>emptyList());
        when(jqlOperandResolver.getValues(queryCreationContext, operand, clause)).thenReturn(queryLiterals);

        final SavedFilterClauseQueryFactory filterClauseQueryFactory = new SavedFilterClauseQueryFactory(savedFilterResolver, jqlOperandResolver, null, queryVisitor);
        final QueryFactoryResult query = filterClauseQueryFactory.getQuery(queryCreationContext, clause);

        assertEquals(new BooleanQuery(), query.getLuceneQuery());
        assertFalse(query.mustNotOccur());
    }

    @Test
    public void testGetQueryOneFilterFound() throws Exception {
        final SingleValueOperand filter1Operand = new SingleValueOperand("filter1");
        final SingleValueOperand filter2Operand = new SingleValueOperand("filter2");
        final SingleValueOperand filter3Operand = new SingleValueOperand(123L);
        final List<QueryLiteral> queryLiterals = CollectionBuilder.newBuilder(createLiteral("filter1"), createLiteral("filter2"), createLiteral(123L)).asList();

        final MultiValueOperand operand = new MultiValueOperand(CollectionBuilder.newBuilder(filter1Operand, filter2Operand, filter3Operand).asList());
        TerminalClause clause = new TerminalClauseImpl("test", Operator.IN, operand);

        final MockSearchRequest foundFilter = new MockSearchRequest("dude");
        when(savedFilterResolver.getSearchRequest(theUser, queryLiterals)).thenReturn(Collections.singletonList(foundFilter));

        when(savedFilterCycleDetector.containsSavedFilterReference(theUser, overrideSecurity, foundFilter, null)).thenReturn(false);

        when(jqlOperandResolver.getValues(queryCreationContext, operand, clause)).thenReturn(queryLiterals);

        final TermQuery termQuery = new TermQuery(new Term("blah", "blee"));
        final SavedFilterClauseQueryFactory filterClauseQueryFactory = new SavedFilterClauseQueryFactory(savedFilterResolver, jqlOperandResolver, savedFilterCycleDetector, queryVisitor) {
            @Override
            Query getQueryFromSavedFilter(final QueryCreationContext queryCreationContext, final SearchRequest savedFilter) {
                assertEquals(foundFilter, savedFilter);
                return termQuery;
            }
        };
        final QueryFactoryResult query = filterClauseQueryFactory.getQuery(queryCreationContext, clause);

        assertEquals(termQuery, query.getLuceneQuery());
        assertFalse(query.mustNotOccur());
    }

    @Test
    public void testGetQueryMultipleFiltersFound() throws Exception {
        final SingleValueOperand filter1Operand = new SingleValueOperand("filter1");
        final SingleValueOperand filter2Operand = new SingleValueOperand("filter2");
        final SingleValueOperand filter3Operand = new SingleValueOperand(123L);
        final List<QueryLiteral> queryLiterals = CollectionBuilder.newBuilder(createLiteral("filter1"), createLiteral("filter2"), createLiteral(123L)).asList();

        final MultiValueOperand operand = new MultiValueOperand(CollectionBuilder.newBuilder(filter1Operand, filter2Operand, filter3Operand).asList());
        TerminalClause clause = new TerminalClauseImpl("test", Operator.IN, operand);

        final MockSearchRequest foundFilter1 = new MockSearchRequest("dude1");
        final MockSearchRequest foundFilter2 = new MockSearchRequest("dude2");
        final MockSearchRequest foundFilter3 = new MockSearchRequest("dude3");
        when(savedFilterResolver.getSearchRequest(theUser, queryLiterals))
                .thenReturn(CollectionBuilder.<SearchRequest>newBuilder(foundFilter1, foundFilter2, foundFilter3).asList());

        when(savedFilterCycleDetector.containsSavedFilterReference(theUser, overrideSecurity, foundFilter1, null)).thenReturn(false);
        when(savedFilterCycleDetector.containsSavedFilterReference(theUser, overrideSecurity, foundFilter2, null)).thenReturn(false);
        when(savedFilterCycleDetector.containsSavedFilterReference(theUser, overrideSecurity, foundFilter3, null)).thenReturn(false);

        when(jqlOperandResolver.getValues(queryCreationContext, operand, clause)).thenReturn(queryLiterals);

        final AtomicInteger callCount = new AtomicInteger(0);
        final TermQuery termQuery = new TermQuery(new Term("blah", "blee"));
        final SavedFilterClauseQueryFactory filterClauseQueryFactory = new SavedFilterClauseQueryFactory(savedFilterResolver, jqlOperandResolver, savedFilterCycleDetector, queryVisitor) {
            @Override
            Query getQueryFromSavedFilter(final QueryCreationContext queryCreationContext, final SearchRequest savedFilter) {
                callCount.incrementAndGet();
                return termQuery;
            }
        };
        final QueryFactoryResult query = filterClauseQueryFactory.getQuery(queryCreationContext, clause);

        BooleanQuery expectedQuery = new BooleanQuery();
        expectedQuery.add(termQuery, BooleanClause.Occur.SHOULD);
        expectedQuery.add(termQuery, BooleanClause.Occur.SHOULD);
        expectedQuery.add(termQuery, BooleanClause.Occur.SHOULD);

        assertEquals(expectedQuery, query.getLuceneQuery());
        assertFalse(query.mustNotOccur());
        assertEquals(3, callCount.get());
    }

    @Test
    public void testGetQueryOneFilterFoundWithNot() throws Exception {
        final SingleValueOperand filter1Operand = new SingleValueOperand("filter1");
        final SingleValueOperand filter2Operand = new SingleValueOperand("filter2");
        final SingleValueOperand filter3Operand = new SingleValueOperand(123L);
        final List<QueryLiteral> queryLiterals = CollectionBuilder.newBuilder(createLiteral("filter1"), createLiteral("filter2"), createLiteral(123L)).asList();

        final MultiValueOperand operand = new MultiValueOperand(CollectionBuilder.newBuilder(filter1Operand, filter2Operand, filter3Operand).asList());
        TerminalClause clause = new TerminalClauseImpl("test", Operator.NOT_IN, operand);

        final MockSearchRequest foundFilter = new MockSearchRequest("dude");
        when(savedFilterResolver.getSearchRequest(theUser, queryLiterals)).thenReturn(Collections.singletonList(foundFilter));

        when(savedFilterCycleDetector.containsSavedFilterReference(theUser, overrideSecurity, foundFilter, null)).thenReturn(false);

        when(jqlOperandResolver.getValues(queryCreationContext, operand, clause)).thenReturn(queryLiterals);

        final TermQuery termQuery = new TermQuery(new Term("blah", "blee"));
        final SavedFilterClauseQueryFactory filterClauseQueryFactory = new SavedFilterClauseQueryFactory(savedFilterResolver, jqlOperandResolver, savedFilterCycleDetector, queryVisitor) {
            @Override
            Query getQueryFromSavedFilter(final QueryCreationContext queryCreationContext, final SearchRequest savedFilter) {
                assertEquals(foundFilter, savedFilter);
                return termQuery;
            }
        };
        final QueryFactoryResult query = filterClauseQueryFactory.getQuery(queryCreationContext, clause);

        assertEquals(termQuery, query.getLuceneQuery());
        assertTrue(query.mustNotOccur());
    }

    @Test
    public void testGetQueryMultipleFiltersFoundWithNot() throws Exception {
        final SingleValueOperand filter1Operand = new SingleValueOperand("filter1");
        final SingleValueOperand filter2Operand = new SingleValueOperand("filter2");
        final SingleValueOperand filter3Operand = new SingleValueOperand(123L);
        final List<QueryLiteral> queryLiterals = CollectionBuilder.newBuilder(createLiteral("filter1"), createLiteral("filter2"), createLiteral(123L)).asList();

        final MultiValueOperand operand = new MultiValueOperand(CollectionBuilder.newBuilder(filter1Operand, filter2Operand, filter3Operand).asList());
        TerminalClause clause = new TerminalClauseImpl("test", Operator.NOT_IN, operand);

        final MockSearchRequest foundFilter1 = new MockSearchRequest("dude1");
        final MockSearchRequest foundFilter2 = new MockSearchRequest("dude2");
        final MockSearchRequest foundFilter3 = new MockSearchRequest("dude3");
        when(savedFilterResolver.getSearchRequest(theUser, queryLiterals))
                .thenReturn(CollectionBuilder.<SearchRequest>newBuilder(foundFilter1, foundFilter2, foundFilter3).asList());

        when(savedFilterCycleDetector.containsSavedFilterReference(theUser, overrideSecurity, foundFilter1, null)).thenReturn(false);
        when(savedFilterCycleDetector.containsSavedFilterReference(theUser, overrideSecurity, foundFilter2, null)).thenReturn(false);
        when(savedFilterCycleDetector.containsSavedFilterReference(theUser, overrideSecurity, foundFilter3, null)).thenReturn(false);

        when(jqlOperandResolver.getValues(queryCreationContext, operand, clause)).thenReturn(queryLiterals);

        final AtomicInteger callCount = new AtomicInteger(0);
        final TermQuery termQuery = new TermQuery(new Term("blah", "blee"));
        final SavedFilterClauseQueryFactory filterClauseQueryFactory = new SavedFilterClauseQueryFactory(savedFilterResolver, jqlOperandResolver, savedFilterCycleDetector, queryVisitor) {
            @Override
            Query getQueryFromSavedFilter(final QueryCreationContext queryCreationContext, final SearchRequest savedFilter) {
                callCount.incrementAndGet();
                return termQuery;
            }
        };
        final QueryFactoryResult query = filterClauseQueryFactory.getQuery(queryCreationContext, clause);

        BooleanQuery expectedQuery = new BooleanQuery();
        expectedQuery.add(termQuery, BooleanClause.Occur.SHOULD);
        expectedQuery.add(termQuery, BooleanClause.Occur.SHOULD);
        expectedQuery.add(termQuery, BooleanClause.Occur.SHOULD);

        assertEquals(expectedQuery, query.getLuceneQuery());
        assertTrue(query.mustNotOccur());
        assertEquals(3, callCount.get());
    }

    @Test
    public void testGetQueryMultipleFiltersFoundWithNotOneCausesCycle() throws Exception {
        final SingleValueOperand filter1Operand = new SingleValueOperand("filter1");
        final SingleValueOperand filter2Operand = new SingleValueOperand("filter2");
        final SingleValueOperand filter3Operand = new SingleValueOperand(123L);
        final List<QueryLiteral> queryLiterals = CollectionBuilder.newBuilder(createLiteral("filter1"), createLiteral("filter2"), createLiteral(123L)).asList();

        final MultiValueOperand operand = new MultiValueOperand(CollectionBuilder.newBuilder(filter1Operand, filter2Operand, filter3Operand).asList());
        TerminalClause clause = new TerminalClauseImpl("test", Operator.NOT_IN, operand);

        final MockSearchRequest foundFilter1 = new MockSearchRequest("dude1");
        final MockSearchRequest foundFilter2 = new MockSearchRequest("dude2");
        final MockSearchRequest foundFilter3 = new MockSearchRequest("dude3");
        when(savedFilterResolver.getSearchRequest(theUser, queryLiterals))
                .thenReturn(CollectionBuilder.<SearchRequest>newBuilder(foundFilter1, foundFilter2, foundFilter3).asList());

        when(savedFilterCycleDetector.containsSavedFilterReference(theUser, overrideSecurity, foundFilter1, null)).thenReturn(false);
        when(savedFilterCycleDetector.containsSavedFilterReference(theUser, overrideSecurity, foundFilter2, null)).thenReturn(false);
        when(savedFilterCycleDetector.containsSavedFilterReference(theUser, overrideSecurity, foundFilter3, null)).thenReturn(true);

        when(jqlOperandResolver.getValues(queryCreationContext, operand, clause)).thenReturn(queryLiterals);

        final AtomicInteger callCount = new AtomicInteger(0);
        final TermQuery termQuery = new TermQuery(new Term("blah", "blee"));
        final SavedFilterClauseQueryFactory filterClauseQueryFactory = new SavedFilterClauseQueryFactory(savedFilterResolver, jqlOperandResolver, savedFilterCycleDetector, queryVisitor) {
            @Override
            Query getQueryFromSavedFilter(final QueryCreationContext queryCreationContext, final SearchRequest savedFilter) {
                callCount.incrementAndGet();
                return termQuery;
            }
        };
        final QueryFactoryResult query = filterClauseQueryFactory.getQuery(queryCreationContext, clause);

        BooleanQuery expectedQuery = new BooleanQuery();

        assertEquals(expectedQuery, query.getLuceneQuery());
        assertFalse(query.mustNotOccur());
        assertEquals(2, callCount.get());
    }

    @Test
    public void testGetQueryFromSavedFilterNullWhereClause() throws Exception {
        SavedFilterClauseQueryFactory queryFactory = new SavedFilterClauseQueryFactory(null, null, null, null);

        assertEquals(new MatchAllDocsQuery(), queryFactory.getQueryFromSavedFilter(queryCreationContext, new SearchRequest()));
    }
}
