package com.atlassian.jira.plugin.dataprovider;

import com.atlassian.jira.mock.security.MockSimpleAuthenticationContext;
import com.atlassian.jira.security.AdminIssueLockoutFlagManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.util.MockComponentLocator;
import com.atlassian.json.marshal.Jsonable;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.when;

public class AdminIssueLockoutFlagDataProviderTest {
    @Rule
    public MockitoRule mockito = MockitoJUnit.rule();

    private MockApplicationUser user = new MockApplicationUser("brenden");
    private JiraAuthenticationContext context = MockSimpleAuthenticationContext.createNoopContext(user);
    private MockComponentLocator componentLocator = new MockComponentLocator();
    @Mock
    private AdminIssueLockoutFlagManager noAccess;
    private AdminIssueLockoutFlagDataProvider adminIssueLockoutFlagDataProvider;

    @Before
    public void setUp() throws Exception {
        componentLocator.register(AdminIssueLockoutFlagManager.class, noAccess);
        componentLocator.register(JiraAuthenticationContext.class, context);

        adminIssueLockoutFlagDataProvider = new AdminIssueLockoutFlagDataProvider(componentLocator);

        when(noAccess.isAdminWithoutIssuePermission(user)).thenReturn(false);
    }


    @Test
    public void getReturnsEmptyAdminHasProjectAccess() {
        //given
        when(noAccess.isAdminWithoutIssuePermission(user)).thenReturn(false);

        //when
        final Map<String, Object> output = toMap(adminIssueLockoutFlagDataProvider.get());

        //then
        assertThat(output.isEmpty(), Matchers.is(true));
    }

    @Test
    public void getReturnsNoAccessWhenAdminHaveNoAccess() {
        //given
        when(noAccess.isAdminWithoutIssuePermission(user)).thenReturn(true);

        //when
        final Map<String, Object> output = toMap(adminIssueLockoutFlagDataProvider.get());

        //then
        assertThat(output, Matchers.hasEntry("noprojects", true));
        assertThat(output, Matchers.hasEntry("manageAccessUrl", "secure/admin/ApplicationAccess.jspa"));
        assertThat(output, Matchers.hasEntry("flagId", AdminIssueLockoutFlagManager.FLAG));
    }

    private Map<String, Object> toMap(Jsonable jsonable) {
        try {
            final StringWriter writer = new StringWriter();
            jsonable.write(writer);

            Gson gson = new Gson();
            final TypeToken mapType = new TypeToken<Map<String, Object>>() {
            };
            return gson.fromJson(writer.toString(), mapType.getType());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}