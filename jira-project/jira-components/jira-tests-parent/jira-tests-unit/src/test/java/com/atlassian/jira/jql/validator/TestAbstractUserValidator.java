package com.atlassian.jira.jql.validator;

import com.atlassian.jira.jql.operand.JqlOperandResolver;
import com.atlassian.jira.jql.resolver.UserResolver;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.MessageSet;
import com.atlassian.jira.util.MessageSetImpl;
import com.atlassian.query.clause.TerminalClauseImpl;
import com.atlassian.query.operator.Operator;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isNull;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestAbstractUserValidator {
    @Rule
    public final MethodRule initMockito = MockitoJUnit.rule();

    @Mock
    private JqlOperandResolver jqlOperandResolver;

    @Mock
    DataValuesExistValidator dataValuesExistValidator;

    @Mock
    SupportedOperatorsValidator supportedOperatorsValidator;

    @Test
    public void dataValuesExistValidatorNotCalledWithOperatorProblem() throws Exception {
        final TerminalClauseImpl clause = new TerminalClauseImpl("test", Operator.GREATER_THAN, 12L);

        final MessageSet messageSet = new MessageSetImpl();
        messageSet.addErrorMessage("blah blah");
        when(supportedOperatorsValidator.validate(isNull(ApplicationUser.class), eq(clause))).thenReturn(messageSet);

        final AbstractUserValidator userValidator = new MockUserValidator(null, jqlOperandResolver) {
            @Override
            SupportedOperatorsValidator getSupportedOperatorsValidator() {
                return supportedOperatorsValidator;
            }

            @Override
            DataValuesExistValidator getDataValuesValidator(final UserResolver userResolver, final JqlOperandResolver operandSupport, final I18nHelper.BeanFactory beanFactory) {
                return dataValuesExistValidator;
            }
        };
        userValidator.validate(null, clause);

        verify(dataValuesExistValidator, never()).validate(isNull(ApplicationUser.class), eq(clause));
    }

    @Test
    public void dataValuesExistValidatorCalledWithNoOperatorProblem() throws Exception {
        final TerminalClauseImpl clause = new TerminalClauseImpl("test", Operator.GREATER_THAN, 12L);

        when(dataValuesExistValidator.validate(isNull(ApplicationUser.class), eq(clause))).thenReturn(new MessageSetImpl());
        when(supportedOperatorsValidator.validate(isNull(ApplicationUser.class), eq(clause))).thenReturn(new MessageSetImpl());

        final AbstractUserValidator userValidator = new MockUserValidator(null, jqlOperandResolver) {
            @Override
            SupportedOperatorsValidator getSupportedOperatorsValidator() {
                return supportedOperatorsValidator;
            }

            @Override
            DataValuesExistValidator getDataValuesValidator(final UserResolver userResolver, final JqlOperandResolver operandSupport, final I18nHelper.BeanFactory beanFactory) {
                return dataValuesExistValidator;
            }
        };

        userValidator.validate(null, clause);

        verify(dataValuesExistValidator).validate(isNull(ApplicationUser.class), eq(clause));
    }

    class MockUserValidator extends AbstractUserValidator {
        public MockUserValidator(UserResolver userResolver, JqlOperandResolver operandResolver) {
            super(userResolver, operandResolver, null);
        }
    }
}
