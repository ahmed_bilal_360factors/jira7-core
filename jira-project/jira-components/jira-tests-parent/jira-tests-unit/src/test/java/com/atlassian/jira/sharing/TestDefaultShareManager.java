package com.atlassian.jira.sharing;

import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.mock.component.MockComponentWorker;
import com.atlassian.jira.sharing.SharedEntity.SharePermissions;
import com.atlassian.jira.sharing.type.GlobalShareType;
import com.atlassian.jira.sharing.type.ShareType;
import com.atlassian.jira.sharing.type.ShareTypeFactory;
import com.atlassian.jira.sharing.type.ShareTypePermissionChecker;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Sets;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.only;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Test for {@link com.atlassian.jira.sharing.TestDefaultShareManager}.
 *
 * @since v3.13
 */

public class TestDefaultShareManager {
    private static final SharePermission GLOBAL_PERM = new SharePermissionImpl(new ShareType.Name("global"), null, null);
    private static final SharePermission GROUP_PERM = new SharePermissionImpl(new ShareType.Name("group"), "jira-user", null);
    private SharedEntity SEARCH_ENTITY_1;
    private MockApplicationUser user;
    private MockApplicationUser other;

    @Mock
    private ShareType shareType;

    @Mock
    private ShareTypePermissionChecker checker;

    @Mock
    private SharePermissionStore sharePermissionStore;

    @Mock
    ShareTypeFactory shareTypeFactory;

    @Mock
    SharePermissionReindexer sharePermissionReindexer;

    @Rule
    public MockitoRule initializeMocksRule = MockitoJUnit.rule();

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Before
    public void setUp() throws Exception {
        new MockComponentWorker().init();
        user = new MockApplicationUser("admin");
        other = new MockApplicationUser("other");
        SEARCH_ENTITY_1 = new StupidEntity(new Long(1), SearchRequest.ENTITY_TYPE, user);
    }

    @Test
    public void shouldNotBeAbleToInstantiateAShareManagerWithANullStore() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Can not instantiate a ShareManager with a 'null' SharePermissionStore");
        new DefaultShareManager(null, mock(ShareTypeFactory.class), mock(SharePermissionReindexer.class));
    }

    @Test
    public void shouldNotBeAbleToInstantiateAShareManagerWithANullFactory() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Can not instantiate a ShareManager with a 'null' ShareTypeFactory");
        new DefaultShareManager(mock(SharePermissionStore.class), null, mock(SharePermissionReindexer.class));
    }

    @Test
    public void shouldNotBeAbleToInstantiateAShareManagerWithNullPermissionReindexer() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Can not instantiate a ShareManager with a 'null' SharePermissionReindexer");
        new DefaultShareManager(mock(SharePermissionStore.class), mock(ShareTypeFactory.class), null);
    }

    @Test
    public void testGetSharePermissions() {
        final SharePermissions expectedReturnedSet = new SharePermissions(Collections.singleton(TestDefaultShareManager.GLOBAL_PERM));
        when(sharePermissionStore.getSharePermissions(eq(SEARCH_ENTITY_1))).thenReturn(expectedReturnedSet);

        final DefaultShareManager mgr = new DefaultShareManager(sharePermissionStore, shareTypeFactory, sharePermissionReindexer);
        final SharePermissions actualReturnedSet = mgr.getSharePermissions(SEARCH_ENTITY_1);

        assertNotNull(actualReturnedSet);
        assertEquals(expectedReturnedSet, actualReturnedSet);
    }

    @Test
    public void testUpdateSharePermissionsWithNullPermissions() {
        when(sharePermissionStore.deleteSharePermissions(eq(SEARCH_ENTITY_1))).thenReturn(10);

        final DefaultShareManager mgr = new DefaultShareManager(sharePermissionStore, shareTypeFactory, sharePermissionReindexer);
        final SharePermissions actualReturnedSet = mgr.updateSharePermissions(SEARCH_ENTITY_1);

        assertNotNull(actualReturnedSet);
        assertThat(actualReturnedSet.getPermissionSet(), empty());
    }

    @Test
    public void testUpdateSharePermissions() {
        final SharePermissions expectedPermissions = new SharePermissions(new HashSet(ImmutableList.of(TestDefaultShareManager.GLOBAL_PERM)));
        final SharedEntity entity = new StupidEntity(1L, SearchRequest.ENTITY_TYPE, user, expectedPermissions);

        when(sharePermissionStore.storeSharePermissions(eq(entity))).thenReturn(expectedPermissions);

        final DefaultShareManager mgr = new DefaultShareManager(sharePermissionStore, shareTypeFactory, sharePermissionReindexer);
        final SharePermissions actualReturnedSet = mgr.updateSharePermissions(entity);

        assertNotNull(actualReturnedSet);
        assertEquals(expectedPermissions, actualReturnedSet);
    }

    @Test
    public void testHasPermissionNullEntity() {
        final DefaultShareManager mgr = new DefaultShareManager(sharePermissionStore, shareTypeFactory, sharePermissionReindexer);
        expectedException.expect(IllegalArgumentException.class);
        mgr.isSharedWith(user, null);
    }

    @Test
    public void testHasPermissionUserIsOwner() {
        final DefaultShareManager mgr = new DefaultShareManager(sharePermissionStore, shareTypeFactory, sharePermissionReindexer);
        final boolean result = mgr.isSharedWith(user, SEARCH_ENTITY_1);
        assertThat(result, is(true));
    }

    @Test
    public void testHasPermissionUserIsNotOwnerNoPermissions() {
        final SharePermissions expectedReturnedSet = SharePermissions.PRIVATE;
        when(sharePermissionStore.getSharePermissions(eq(SEARCH_ENTITY_1))).thenReturn(expectedReturnedSet);

        final DefaultShareManager mgr = new DefaultShareManager(sharePermissionStore, shareTypeFactory, sharePermissionReindexer);
        assertThat(mgr.isSharedWith(other, SEARCH_ENTITY_1), is(false));
    }

    @Test
    public void testHasPermissionUserIsNotOwnerNullPermissions() {
        when(sharePermissionStore.getSharePermissions(eq(SEARCH_ENTITY_1))).thenReturn(null);

        final DefaultShareManager mgr = new DefaultShareManager(sharePermissionStore, shareTypeFactory, sharePermissionReindexer);
        assertThat(mgr.isSharedWith(other, SEARCH_ENTITY_1), is(false));
    }

    @Test
    public void testHasPermissionUserIsNotOwnerHasPermission() {
        final SharePermissions expectedPermissions = SharePermissions.GLOBAL;
        when(sharePermissionStore.getSharePermissions(eq(SEARCH_ENTITY_1))).thenReturn(expectedPermissions);
        when(checker.hasPermission(other, TestDefaultShareManager.GLOBAL_PERM)).thenReturn(true);
        when(shareType.getPermissionsChecker()).thenReturn(checker);
        when(shareTypeFactory.getShareType(GlobalShareType.TYPE)).thenReturn(shareType);

        final DefaultShareManager mgr = new DefaultShareManager(sharePermissionStore, shareTypeFactory, sharePermissionReindexer);
        assertThat(mgr.isSharedWith(other, SEARCH_ENTITY_1), is(true));
    }

    @Test
    public void testHasPermissionUserIsNotOwnerNoPermission() {
        final SharePermissions expectedPermissions = SharePermissions.GLOBAL;

        when(sharePermissionStore.getSharePermissions(eq(SEARCH_ENTITY_1))).thenReturn(expectedPermissions);
        when(shareTypeFactory.getShareType(GlobalShareType.TYPE)).thenReturn(shareType);
        when(shareType.getPermissionsChecker()).thenReturn(checker);
        when(checker.hasPermission(other, TestDefaultShareManager.GLOBAL_PERM)).thenReturn(false);

        final DefaultShareManager mgr = new DefaultShareManager(sharePermissionStore, shareTypeFactory, sharePermissionReindexer);

        assertThat(mgr.isSharedWith(other, SEARCH_ENTITY_1), is(false));
    }

    @Test
    public void testHasPermissionUserIsNotOwnerSomePermissions() {
        // Check shot circuit check
        final Set<SharePermission> perms = Sets.newLinkedHashSet();
        perms.addAll(ImmutableList.of(TestDefaultShareManager.GLOBAL_PERM, TestDefaultShareManager.GROUP_PERM));
        final SharePermissions expectedPermissions = new SharePermissions(perms);

        when(sharePermissionStore.getSharePermissions(eq(SEARCH_ENTITY_1))).thenReturn(expectedPermissions);
        when(shareTypeFactory.getShareType(GlobalShareType.TYPE)).thenReturn(shareType);
        when(shareType.getPermissionsChecker()).thenReturn(checker);
        when(shareTypeFactory.getShareType(GlobalShareType.TYPE)).thenReturn(shareType);
        when(checker.hasPermission(other, TestDefaultShareManager.GLOBAL_PERM)).thenReturn(true);

        final DefaultShareManager mgr = new DefaultShareManager(sharePermissionStore, shareTypeFactory, sharePermissionReindexer);

        assertThat(mgr.isSharedWith(other, SEARCH_ENTITY_1), is(true));
    }

    @Test
    public void testDeleteSharePermissionsLike() throws Exception {
        when(sharePermissionStore.deleteSharePermissionsLike(TestDefaultShareManager.GROUP_PERM)).thenReturn(1);

        final DefaultShareManager mgr = new DefaultShareManager(sharePermissionStore, shareTypeFactory, sharePermissionReindexer);
        mgr.deleteSharePermissionsLike(TestDefaultShareManager.GROUP_PERM);

        verify(sharePermissionStore, only()).deleteSharePermissionsLike(TestDefaultShareManager.GROUP_PERM);
    }

    private static class StupidEntity implements SharedEntity {
        private final Long id;
        private final SharedEntity.TypeDescriptor entityType;
        private final ApplicationUser owner;
        private final SharePermissions sharePermissions;

        StupidEntity(final Long id, final TypeDescriptor entityType, final ApplicationUser owner) {
            this.id = id;
            this.entityType = entityType;
            this.owner = owner;
            sharePermissions = SharePermissions.PRIVATE;
        }

        StupidEntity(final Long id, final TypeDescriptor entityType, final ApplicationUser owner, final SharePermissions sharePermissions) {
            this.id = id;
            this.entityType = entityType;
            this.owner = owner;
            this.sharePermissions = sharePermissions;
        }

        public Long getId() {
            return id;
        }

        public SharedEntity.TypeDescriptor getEntityType() {
            return entityType;
        }

        public String getOwnerUserName() {
            return owner.getName();
        }

        public String getName() {
            throw new UnsupportedOperationException();
        }

        public String getDescription() {
            throw new UnsupportedOperationException();
        }

        @Override
        public ApplicationUser getOwner() {
            return owner;
        }

        public SharePermissions getPermissions() {
            return sharePermissions;
        }

        public Long getFavouriteCount() {
            return null;
        }
    }
}
