package com.atlassian.jira.sharing;

import com.atlassian.jira.sharing.type.GroupShareType;
import com.atlassian.jira.sharing.type.ProjectShareType;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import static org.mockito.Mockito.verify;

/**
 * @since v3.13
 */
public class TestSharePermissionDeleteUtils {
    private static final String GROUPNAME1 = "groupname1";
    private static final SharePermissionImpl GROUP_PERMISSIONS = new SharePermissionImpl(GroupShareType.TYPE, GROUPNAME1, null);
    private static final Long ROLE_ID = new Long(1);
    private static final Long PROJECT_ID = new Long(99);

    @Rule
    public final MethodRule initMockito = MockitoJUnit.rule();
    @Mock
    private ShareManager shareManager;

    private SharePermissionDeleteUtils deleteUtils;

    @Before
    public void setUp() {
        deleteUtils = new SharePermissionDeleteUtils(shareManager);
    }

    @Test(expected = IllegalArgumentException.class)
    public void deleteGroupPermissionsThrowsExceptionOnNull() {
        SharePermissionDeleteUtils deleteUtils = new SharePermissionDeleteUtils(null);
        deleteUtils.deleteGroupPermissions(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void deleteRoleSharePermissionsThrowsExceptionOnNull() {
        SharePermissionDeleteUtils deleteUtils = new SharePermissionDeleteUtils(null);
        deleteUtils.deleteRoleSharePermissions(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void deleteProjectSharePermissionsThrowsExceptionOnNull() {
        SharePermissionDeleteUtils deleteUtils = new SharePermissionDeleteUtils(null);
        deleteUtils.deleteProjectSharePermissions(null);
    }

    @Test
    public void deleteGroupPermissions() throws Exception {
        deleteUtils.deleteGroupPermissions(GROUPNAME1);

        verify(shareManager).deleteSharePermissionsLike(GROUP_PERMISSIONS);
    }

    @Test
    public void deleteRoleSharePermissions() throws Exception {
        SharePermission roleSharePermission = new SharePermissionImpl(ProjectShareType.TYPE, ROLE_ID.toString());

        deleteUtils.deleteRoleSharePermissions(ROLE_ID);

        verify(shareManager).deleteSharePermissionsLike(roleSharePermission);
    }

    @Test
    public void deleteProjectSharePermissions() throws Exception {
        SharePermission permission = new SharePermissionImpl(ProjectShareType.TYPE, PROJECT_ID.toString(), null);

        deleteUtils.deleteProjectSharePermissions(PROJECT_ID);

        verify(shareManager).deleteSharePermissionsLike(permission);
    }
}
