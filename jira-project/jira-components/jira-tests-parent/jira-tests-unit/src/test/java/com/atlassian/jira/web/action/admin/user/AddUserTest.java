package com.atlassian.jira.web.action.admin.user;

import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.exception.runtime.GroupNotFoundException;
import com.atlassian.jira.application.ApplicationKeys;
import com.atlassian.jira.application.ApplicationRole;
import com.atlassian.jira.application.ApplicationRoleManager;
import com.atlassian.jira.bc.user.CreateUserApplicationHelper;
import com.atlassian.jira.bc.user.UserService;
import com.atlassian.jira.crowd.embedded.ofbiz.MockDirectory;
import com.atlassian.jira.event.MockEventPublisher;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.JiraWebActionSupportDependencies;
import com.atlassian.jira.junit.rules.MockHttp;
import com.atlassian.jira.junit.rules.MockitoContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.mock.MockApplicationProperties;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.plugin.web.WebInterfaceManager;
import com.atlassian.plugin.web.descriptors.WebPanelModuleDescriptor;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;
import java.util.Optional;

import static com.atlassian.jira.mock.security.MockSimpleAuthenticationContext.createNoopContext;
import static com.atlassian.jira.web.action.admin.user.AddUser.APPLICATION_ACCESS_EXTENSION_WEB_PANEL_LOCATION;
import static java.util.Collections.emptyMap;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AddUserTest {

    private static final long DIRECTORY_ID = 1L;
    private static final String DIRECTORY_NAME = "Ldap";
    private static final String USERNAME = "user1";
    private static final String X_ATLASSIAN_DIALOG_MSG_HTML = "X-Atlassian-Dialog-Msg-Html";
    private static final String X_ATLASSIAN_DIALOG_CONTROL = "X-Atlassian-Dialog-Control";

    @Rule
    public MockHttp.DefaultMocks httpContext = MockHttp.withDefaultMocks();
    @Rule
    public final MockitoContainer mockContainer = MockitoMocksInContainer.rule(this);
    @Rule
    public TestRule webActionSupport = JiraWebActionSupportDependencies.build(mockContainer);

    @AvailableInContainer
    private JiraAuthenticationContext authenticationContext = createNoopContext(new MockApplicationUser("User"));

    @AvailableInContainer
    private MockApplicationProperties applicationProperties = new MockApplicationProperties();

    @Mock
    UserService userService;

    @Mock
    UserManager userManager;

    @Mock
    WebInterfaceManager webInterfaceManager;

    @Mock
    CreateUserApplicationHelper createUserApplicationHelper;

    @Mock
    ApplicationRoleManager roleManager;

    private AddUser action;

    @Before
    public void setup() {
        when(userManager.getDirectory(DIRECTORY_ID)).thenReturn(new MockDirectory(DIRECTORY_ID, DIRECTORY_NAME));
        when(userManager.getDefaultCreateDirectory()).thenReturn(Optional.of(new MockDirectory(DIRECTORY_ID, DIRECTORY_NAME)));
        applicationProperties.setEncoding("UTF-8");

        action = new AddUser(userService, userManager, webInterfaceManager, new MockEventPublisher(), createUserApplicationHelper, roleManager);
        action.setDirectoryId(DIRECTORY_ID);
        action.setSendEmail(false);
        action.setUsername(USERNAME);
    }

    @Test
    public void showsWarningMessageAndRedirectsToViewUserWhenUserCouldNotBeAddedToTheGroup() throws Exception {
        action.setInline(true);
        when(userService.createUser(any(UserService.CreateUserValidationResult.class)))
                .thenThrow(new GroupNotFoundException("test-group"));

        action.doExecute();
        assertThat(httpContext.mockResponse().getHeader(X_ATLASSIAN_DIALOG_MSG_HTML),
                is("admin.warn.user.create.no.group{[user1, Group &lt;test-group&gt; does not exist, Ldap]}"));
        assertThat(httpContext.mockResponse().getHeader(X_ATLASSIAN_DIALOG_CONTROL),
                is("redirect:ViewUser.jspa?name=" + USERNAME));
    }

    @Test
    public void createUsersListShouldBeEscaped() {
        action.setCreatedUser(new String[]{"<script>alert('1');%&!</script>"});

        final List<String> createdUsersList = action.createdUsers();

        assertThat(createdUsersList, contains("createdUser=%3Cscript%3Ealert%28%271%27%29%3B%25%26%21%3C%2Fscript%3E"));
    }

    @Test
    public void shouldDoDefaultUpdateSelectedApplicationWhenTheyAreNull() {
        when(roleManager.getDefaultApplicationKeys()).thenReturn(ImmutableSet.of(ApplicationKeys.CORE));
        action.setSelectedApplications(null);

        action.doDefault();

        assertThat(action.getSelectedApplicationKeys(), contains(ApplicationKeys.CORE));
    }

    @Test
    public void shouldDoDefaultNotUpdateSelectedApplicationWhenTheyAreNotNull() {
        when(roleManager.getDefaultApplicationKeys()).thenReturn(ImmutableSet.of(ApplicationKeys.CORE));
        action.setSelectedApplications(new String[]{ApplicationKeys.SERVICE_DESK.toString()});

        action.doDefault();

        assertThat(action.getSelectedApplicationKeys(), contains(ApplicationKeys.SERVICE_DESK));
    }

    @Test
    public void shouldIncludeDirectoryIdToRedirectUrlWhenDirectoryWasSpecified() {
        action.setCreateAnother(true);
        action.setDirectoryId(4567L);
        final ImmutableList<Directory> directories = ImmutableList.of(mock(Directory.class), mock(Directory.class));
        when(userManager.getWritableDirectories()).thenReturn(directories);

        action.doExecute();

        assertThat(httpContext.mockResponse().getRedirect(), containsString("directoryId=4567"));
    }

    @Test
    public void shouldNotIncludeDirectoryWhenThereIsOnlyOneDirectory() {
        action.setCreateAnother(true);
        action.setDirectoryId(4567L);
        final ImmutableList<Directory> directories = ImmutableList.of(mock(Directory.class));
        when(userManager.getWritableDirectories()).thenReturn(directories);

        action.doExecute();

        assertThat(httpContext.mockResponse().getRedirect(), not(containsString("directoryId=")));
    }

    @Test
    public void shouldTakeIntoAccountExternalApplicationsWhenDeterminingNumberOfApplications() {
        when(roleManager.getRoles()).thenReturn(ImmutableSet.of(mock(ApplicationRole.class)));
        when(webInterfaceManager.getDisplayableWebPanelDescriptors(APPLICATION_ACCESS_EXTENSION_WEB_PANEL_LOCATION, emptyMap()))
                .thenReturn(ImmutableList.of(mock(WebPanelModuleDescriptor.class)));

        assertThat(action.hasOnlyOneApplication(), is(false));
    }

    @Test
    public void shouldReturnCancelUrlWithCreatedUsersFlagWhenAtLeastOneUserWasCreated() {
        action.setCreatedUser(new String[]{"kevin"});

        assertEquals("UserBrowser.jspa?createdUser=kevin#userCreatedFlag", action.getCancelUrl());
    }

    @Test
    public void shouldNotReturnUserCreatedFlagInCancelUrlWhenNoUserCreated() {
        action.setCreatedUser(new String[]{});

        assertEquals("UserBrowser.jspa?", action.getCancelUrl());
    }
}
