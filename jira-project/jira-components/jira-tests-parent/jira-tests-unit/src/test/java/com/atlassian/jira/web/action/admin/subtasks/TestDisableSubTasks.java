package com.atlassian.jira.web.action.admin.subtasks;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.SubTaskManager;
import com.atlassian.jira.junit.rules.JiraWebActionSupportDependencies;
import com.atlassian.jira.junit.rules.MockitoContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.mockobjects.servlet.MockHttpServletResponse;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.mockito.Mock;
import webwork.action.Action;

import java.util.Collections;
import java.util.Set;

import static com.atlassian.jira.JiraTestUtil.setupExpectedRedirect;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

/**
 * Unit test of {@link DisableSubTasks}.
 *
 * @since 6.2
 */
public class TestDisableSubTasks {
    private DisableSubTasks disableSubTasks;
    @Mock
    private SubTaskManager mockSubTaskManager;

    @Rule
    public final MockitoContainer mockContainer = MockitoMocksInContainer.rule(this);
    @Rule
    public TestRule webActionSupport = JiraWebActionSupportDependencies.build(mockContainer);

    @Before
    public void setUp() {
        disableSubTasks = new DisableSubTasks(mockSubTaskManager);
    }

    @After
    public void tearDownWorker() {
        ComponentAccessor.initialiseWorker(null);
    }

    @Test
    public void testDoDefaultNoSubTasks() throws Exception {
        final MockHttpServletResponse expectedRedirect = setupExpectedRedirect("ManageSubTasks.jspa");
        when(mockSubTaskManager.getAllSubTaskIssueIds()).thenReturn(Collections.<Long>emptySet());
        assertEquals(Action.NONE, disableSubTasks.doDefault());
        expectedRedirect.verify();
    }

    @Test
    public void testDoDefaultSubTasksExist() throws Exception {
        final Set<Long> issueIds = Collections.singleton(1000L);
        when(mockSubTaskManager.getAllSubTaskIssueIds()).thenReturn(issueIds);
        assertEquals(Action.INPUT, disableSubTasks.doDefault());
        assertEquals(issueIds.size(), disableSubTasks.getSubTaskCount());
    }

    @Test
    public void testDoExecute() throws Exception {
        final MockHttpServletResponse expectedRedirect = setupExpectedRedirect("ManageSubTasks.jspa");
        assertEquals(Action.NONE, disableSubTasks.execute());
        expectedRedirect.verify();
    }
}
