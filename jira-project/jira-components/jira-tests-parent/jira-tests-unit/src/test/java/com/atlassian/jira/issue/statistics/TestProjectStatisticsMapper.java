package com.atlassian.jira.issue.statistics;

import com.atlassian.jira.issue.index.DocumentConstants;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import org.junit.Test;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestProjectStatisticsMapper {
    private final String clauseName = "name";

    @Test
    public void testGetDocumentConstant() {
        final ProjectStatisticsMapper sorter = new ProjectStatisticsMapper(null);
        assertEquals(DocumentConstants.PROJECT_ID, sorter.getDocumentConstant());
    }

    @Test
    public void testGetDocumentConstantCustom() {
        final ProjectStatisticsMapper sorter = new ProjectStatisticsMapper(null, clauseName, "abc");
        assertEquals("abc", sorter.getDocumentConstant());
    }

    @Test
    public void testEqualsWithDefaultDocumentConstant() {
        final ProjectStatisticsMapper sorter = new ProjectStatisticsMapper(null);
        assertEquals(sorter, sorter);
        assertEquals(sorter.hashCode(), sorter.hashCode());

        final ProjectManager mockProjectManager = mock(ProjectManager.class);

        final ProjectStatisticsMapper sorter2 = new ProjectStatisticsMapper(mockProjectManager);

        assertEquals(sorter, sorter2);
        assertEquals(sorter.hashCode(), sorter2.hashCode());
    }

    @Test
    public void testEqualsWithCustomDocumentConstant() {
        final String documentConstant = "abc";
        final ProjectStatisticsMapper sorter = new ProjectStatisticsMapper(null, clauseName, documentConstant);
        assertEquals(sorter, sorter);
        assertEquals(sorter.hashCode(), sorter.hashCode());

        final ProjectManager mockProjectManager = mock(ProjectManager.class);

        // Ensure we have a new string object
        final ProjectStatisticsMapper sorter2 = new ProjectStatisticsMapper(mockProjectManager, clauseName, "abc");
        assertEquals(sorter, sorter2);
        assertEquals(sorter.hashCode(), sorter2.hashCode());

        assertThat(sorter, not(equalTo(new ProjectStatisticsMapper(null, clauseName, "def"))));
        assertThat(sorter, not(equalTo(new ProjectStatisticsMapper(mockProjectManager, clauseName, "def"))));

        assertThat(sorter, not(equalTo(null)));
        assertThat(sorter, not(equalTo(new Object())));

        // Ensure a different sorter with the same document constant is not equal
        assertThat(sorter, not(equalTo(new TextFieldSorter(documentConstant))));
        assertThat(sorter, not(equalTo(new IssueKeyStatisticsMapper())));
    }

    @Test
    public void testGetValueFromLuceneField() {
        final ProjectManager mockProjectManager = mock(ProjectManager.class);

        final Project project = new MockProject(23232L);
        when(mockProjectManager.getProjectObj(23232L)).thenReturn(project);

        final ProjectStatisticsMapper projectStatsMapper = new ProjectStatisticsMapper(mockProjectManager, null, null);
        final Object nullResult = projectStatsMapper.getValueFromLuceneField(null);
        assertNull(nullResult);
        final Object anotherNullResult = projectStatsMapper.getValueFromLuceneField("-1");
        assertNull(anotherNullResult);

        final Object projectObject = projectStatsMapper.getValueFromLuceneField("23232");
        assertEquals(project, projectObject);
    }

}
