package com.atlassian.jira.mention;

import com.atlassian.jira.bc.ServiceOutcome;
import com.atlassian.jira.bc.issue.comment.CommentService;
import com.atlassian.jira.bc.issue.vote.VoteService;
import com.atlassian.jira.bc.issue.watcher.WatcherService;
import com.atlassian.jira.bc.user.search.UserSearchParams;
import com.atlassian.jira.bc.user.search.UserSearchService;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.comments.Comment;
import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.IssueInvolvement;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.UserIssueRelevance;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.lang.Pair;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSortedSet;
import org.junit.Before;
import org.junit.Test;

import java.time.ZonedDateTime;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import static java.util.Collections.emptyList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.lessThan;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.argThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestSortedMentionableUserSearcher {
    private CommentService commentService;
    private WatcherService watcherService;
    private UserSearchService userSearchService;
    private PermissionManager permissionManager;

    private final int maxResults = 5;
    private ApplicationUser authUser;
    private Issue issue;
    private ServiceOutcome<Collection<ApplicationUser>> votersResponse;
    private ServiceOutcome<Pair<Integer, List<ApplicationUser>>> watchersResponse;

    private MentionableUserSearcher mentionableUserSearch;

    @Before
    public void setup() {
        permissionManager = mock(PermissionManager.class);
        final I18nHelper i18n = mock(I18nHelper.class);
        commentService = mock(CommentService.class);
        watcherService = mock(WatcherService.class);
        final VoteService voteService = mock(VoteService.class);
        userSearchService = mock(UserSearchService.class);

        mentionableUserSearch = new SortedMentionableUserSearcher(i18n, permissionManager, commentService, watcherService, voteService, userSearchService);

        authUser = mock(ApplicationUser.class);
        issue = mock(Issue.class);
        votersResponse = mock(ServiceOutcome.class);
        watchersResponse = mock(ServiceOutcome.class);

        when(i18n.getLocale()).thenReturn(Locale.ENGLISH);
        when(authUser.getUsername()).thenReturn("loggedInUser");
        when(voteService.viewVoters(issue, authUser)).thenReturn(votersResponse);
        when(votersResponse.isValid()).thenReturn(true);
        when(watchersResponse.isValid()).thenReturn(true);
        when(votersResponse.getReturnedValue()).thenReturn(ImmutableList.of());
        when(watchersResponse.getReturnedValue()).thenReturn(Pair.of(0, ImmutableList.of()));
        when(watcherService.isWatchingEnabled()).thenReturn(true);
        when(i18n.getText(anyString())).then(returnsFirstArg());
        when(permissionManager.hasPermission(eq(ProjectPermissions.BROWSE_PROJECTS), eq(issue), any(ApplicationUser.class))).thenReturn(true);
        when(permissionManager.hasPermission(eq(Permissions.USER_PICKER), eq(authUser))).thenReturn(true);
        when(watcherService.getWatchers(issue, authUser)).thenReturn(watchersResponse);
    }

    @Test
    public void userWithoutBrowseUsersPermissionReturnsEmptyList() {
        when(permissionManager.hasPermission(eq(Permissions.USER_PICKER), eq(authUser))).thenReturn(false);

        final List<UserIssueRelevance> result = mentionableUserSearch.findRelatedUsersToMention("someFilter", issue, authUser, maxResults);

        assertThat(result, is(emptyList()));
    }

    @Test
    public void invalidIssueReturnsEmptyList() {
        final List<UserIssueRelevance> result = mentionableUserSearch.findRelatedUsersToMention("someFilter", null, authUser, maxResults);

        assertThat(result, is(emptyList()));
    }

    @Test
    public void watchersDisabledDoesntBreakAllTheThings() {
        when(watcherService.isWatchingEnabled()).thenReturn(false);
        when(issue.getAssignee()).thenReturn(genUser("Michael Jordan"));
        when(issue.getReporter()).thenReturn(genUser("Mike Wazowski"));
        when(votersResponse.getReturnedValue()).thenReturn(ImmutableList.of(genUser("Mickey Mouse")));

        final List<UserIssueRelevance> result = mentionableUserSearch.findRelatedUsersToMention("mi", issue, authUser, maxResults);
        final List<ApplicationUser> users = result.stream()
                .map(UserIssueRelevance::getUser)
                .collect(Collectors.toList());

        assertThat(result.size(), is(3));
        assertThat(users, hasItem(genUser("Michael Jordan")));
        assertThat(users, hasItem(genUser("Mike Wazowski")));
        assertThat(users, hasItem(genUser("Mickey Mouse")));
    }

    @Test
    public void votersDisabledDoesntBreakAllTheThings() {
        when(issue.getAssignee()).thenReturn(genUser("Michael Jordan"));
        when(issue.getReporter()).thenReturn(genUser("Mike Wazowski"));
        when(watchersResponse.isValid()).thenReturn(true);
        when(watchersResponse.getReturnedValue()).thenReturn(Pair.of(1, ImmutableList.of(genUser("Mickey Mouse"))));
        when(votersResponse.isValid()).thenReturn(false);

        final List<UserIssueRelevance> result = mentionableUserSearch.findRelatedUsersToMention("mi", issue, authUser, maxResults);
        final List<ApplicationUser> users = result.stream()
                .map(UserIssueRelevance::getUser)
                .collect(Collectors.toList());

        assertThat(result.size(), is(3));
        assertThat(users, hasItem(genUser("Michael Jordan")));
        assertThat(users, hasItem(genUser("Mike Wazowski")));
        assertThat(users, hasItem(genUser("Mickey Mouse")));
    }

    @Test
    public void searchByRoleReturnsUser() {
        when(issue.getAssignee()).thenReturn(genUser("Sir Pompous III"));
        when(watchersResponse.isValid()).thenReturn(true);
        when(votersResponse.isValid()).thenReturn(false);

        //Search for the i18n key as the i18nHelper has been mocked to return that as the text value
        final List<UserIssueRelevance> result = mentionableUserSearch.findRelatedUsersToMention("rest.mentionableuser.assi", issue, authUser, maxResults);
        final List<ApplicationUser> users = result.stream()
                .map(UserIssueRelevance::getUser)
                .collect(Collectors.toList());

        assertThat(result.size(), is(1));
        assertThat(users, hasItem(genUser("Sir Pompous III")));
    }

    /**
     * In some languages, roles could start with the same letters. Ensure we return all matching roles.
     */
    @Test
    public void findsMultipleUserByRoleIfRolesOverlap() {
        when(issue.getAssignee()).thenReturn(genUser("Michael Clarke"));
        when(issue.getReporter()).thenReturn(genUser("Ricky Ponting"));

        //Search for the i18n key as the i18nHelper has been mocked to return that as the text value. This will match all roles.
        final List<UserIssueRelevance> result = mentionableUserSearch.findRelatedUsersToMention("rest.mentionableuser", issue, authUser, maxResults);
        final List<ApplicationUser> users = result.stream()
                .map(UserIssueRelevance::getUser)
                .collect(Collectors.toList());

        assertThat(result.size(), is(2));
        assertThat(users, hasItem(genUser("Michael Clarke")));
        assertThat(users, hasItem(genUser("Ricky Ponting")));
    }

    @Test
    public void searchForCommentersFindsAllCommentersInOrder() {
        final Comment comment1 = mock(Comment.class);
        final Comment comment2 = mock(Comment.class);
        final Comment comment3 = mock(Comment.class);
        final Comment comment4 = mock(Comment.class);
        final Comment comment5 = mock(Comment.class);
        when(commentService.getCommentsForUser(authUser, issue)).thenReturn(ImmutableList.of(comment1, comment2, comment3, comment4, comment5));

        final ZonedDateTime now = ZonedDateTime.now();
        when(comment1.getAuthorApplicationUser()).thenReturn(genUser("Michael Jordan"));
        when(comment1.getCreated()).thenReturn(toDate(now.minusDays(1)));
        when(comment2.getAuthorApplicationUser()).thenReturn(genUser("Mike Wazowski"));
        when(comment2.getCreated()).thenReturn(toDate(now.minusDays(2)));
        when(comment3.getAuthorApplicationUser()).thenReturn(genUser("Mickey Mouse"));
        when(comment3.getCreated()).thenReturn(toDate(now.minusDays(3)));
        when(comment4.getAuthorApplicationUser()).thenReturn(genUser("Mike Wazowski"));
        when(comment4.getCreated()).thenReturn(toDate(now.minusDays(4)));
        when(comment5.getAuthorApplicationUser()).thenReturn(genUser("Michael Jordan"));
        when(comment5.getCreated()).thenReturn(toDate(now.minusDays(5)));

        //Search for the i18n key as the i18nHelper has been mocked to return that as the text value. This will match commenter.
        final List<UserIssueRelevance> result = mentionableUserSearch.findRelatedUsersToMention("mi", issue, authUser, maxResults);
        final List<ApplicationUser> users = result.stream()
                .map(UserIssueRelevance::getUser)
                .collect(Collectors.toList());

        assertThat(result.size(), is(3));
        assertThat(users.get(0), is(genUser("Michael Jordan")));
        assertThat(users.get(1), is(genUser("Mike Wazowski")));
        assertThat(users.get(2), is(genUser("Mickey Mouse")));
        assertThat(result.get(0).compareTo(result.get(1)), lessThan(0));
        assertThat(result.get(1).compareTo(result.get(2)), lessThan(0));
    }

    @Test
    public void searchByRoleReturnsAllRolesForUser() {
        final String searchFilter = "rest.mentionableuser.assignee"; //Search for the i18n key as the i18nHelper has been mocked to return that as the text value. This will match all roles.
        final ApplicationUser user = genUser("Bart");
        when(issue.getAssignee()).thenReturn(user);
        when(issue.getReporter()).thenReturn(user);

        final Comment comment = mock(Comment.class);
        when(commentService.getCommentsForUser(authUser, issue)).thenReturn(ImmutableList.of(comment));
        when(comment.getAuthorApplicationUser()).thenReturn(user);
        when(comment.getCreated()).thenReturn(new Date());

        final List<UserIssueRelevance> result = mentionableUserSearch.findRelatedUsersToMention(searchFilter, issue, authUser, maxResults);
        final List<ApplicationUser> users = result.stream()
                .map(UserIssueRelevance::getUser)
                .collect(Collectors.toList());

        assertThat(result.size(), is(1));
        assertThat(users, hasItem(user));
        assertThat(result.get(0).getIssueInvolvements(), is(ImmutableSortedSet.of(IssueInvolvement.ASSIGNEE, IssueInvolvement.COMMENTER, IssueInvolvement.REPORTER)));
    }

    @Test
    public void searchByNameReturnsAllRolesForUser() {
        final String searchFilter = "Bart";
        final ApplicationUser user = genUser(searchFilter);
        when(issue.getAssignee()).thenReturn(user);
        when(issue.getReporter()).thenReturn(user);

        final Comment comment = mock(Comment.class);
        when(commentService.getCommentsForUser(authUser, issue)).thenReturn(ImmutableList.of(comment));
        when(comment.getAuthorApplicationUser()).thenReturn(user);
        when(comment.getCreated()).thenReturn(new Date());

        final List<UserIssueRelevance> result = mentionableUserSearch.findRelatedUsersToMention(searchFilter, issue, authUser, maxResults);
        final List<ApplicationUser> users = result.stream()
                .map(UserIssueRelevance::getUser)
                .collect(Collectors.toList());

        assertThat(result.size(), is(1));
        assertThat(users, hasItem(user));
        assertThat(result.get(0).getIssueInvolvements(), is(ImmutableSortedSet.of(IssueInvolvement.ASSIGNEE, IssueInvolvement.COMMENTER, IssueInvolvement.REPORTER)));
    }

    @Test
    public void searchByMultiWordNameForInvolvedUserReturnsUser() {
        when(issue.getAssignee()).thenReturn(genUser("Mike Cannon-Brooks"));
        when(issue.getReporter()).thenReturn(genUser("Steven Bradbury"));

        //Search for the i18n key as the i18nHelper has been mocked to return that as the text value
        final List<UserIssueRelevance> result = mentionableUserSearch.findRelatedUsersToMention("br", issue, authUser, maxResults);
        final List<ApplicationUser> users = result.stream()
                .map(UserIssueRelevance::getUser)
                .collect(Collectors.toList());

        assertThat(result.size(), is(2));
        assertThat(users, hasItem(genUser("Mike Cannon-Brooks")));
        assertThat(users, hasItem(genUser("Steven Bradbury")));
    }

    @Test
    public void grabsRandomUsersToFillRemainingNumberWithNoOverlap() {
        final String userPrefix = "user_";
        when(issue.getAssignee()).thenReturn(genUser(userPrefix + "assignee"));
        when(issue.getReporter()).thenReturn(genUser(userPrefix + "reporter"));

        when(userSearchService.findUsers(eq(userPrefix), any(UserSearchParams.class))).thenReturn(ImmutableList.of(
                genUser(userPrefix + "1"),
                genUser(userPrefix + "2"),
                genUser(userPrefix + "3"),
                genUser(userPrefix + "4"),
                genUser(userPrefix + "5")
        ));

        final List<UserIssueRelevance> result = mentionableUserSearch.findRelatedUsersToMention(userPrefix, issue, authUser, maxResults);
        final List<ApplicationUser> users = result.stream()
                .map(UserIssueRelevance::getUser)
                .collect(Collectors.toList());

        assertThat(result.size(), is(5));
        assertThat(users, hasItem(genUser(userPrefix + "assignee")));
        assertThat(users, hasItem(genUser(userPrefix + "reporter")));
    }

    @Test
    public void grabsRandomUsersToFillRemainingNumberWithSomeOverlap() {
        final String userPrefix = "user_";
        when(issue.getAssignee()).thenReturn(genUser(userPrefix + "assignee"));
        when(issue.getReporter()).thenReturn(genUser(userPrefix + "reporter"));

        when(userSearchService.findUsers(eq(userPrefix), any(UserSearchParams.class))).thenReturn(ImmutableList.of(
                genUser(userPrefix + "assignee"),
                genUser(userPrefix + "reporter"),
                genUser(userPrefix + "3"),
                genUser(userPrefix + "4")
        ));

        final List<UserIssueRelevance> result = mentionableUserSearch.findRelatedUsersToMention(userPrefix, issue, authUser, maxResults);
        final List<ApplicationUser> users = result.stream()
                .map(UserIssueRelevance::getUser)
                .collect(Collectors.toList());

        assertThat(result.size(), is(4));
        assertThat(users, hasItem(genUser(userPrefix + "assignee")));
        assertThat(users, hasItem(genUser(userPrefix + "reporter")));
    }

    @Test
    public void doesNotGrabsRandomUsersToFillRemainingNumberIfSufficientSpecificUsers() {
        final String userPrefix = "user_";
        when(issue.getAssignee()).thenReturn(genUser(userPrefix + "assignee"));
        when(issue.getReporter()).thenReturn(genUser(userPrefix + "reporter"));

        mentionableUserSearch.findRelatedUsersToMention(userPrefix, issue, authUser, 2);
        verify(userSearchService, never()).findUsers(anyString(), any(UserSearchParams.class));
    }

    @Test
    public void sortsByInvolvementThenLatestCommentTimeThenUsername() {
        final String userPrefix = "user_";
        when(issue.getAssignee()).thenReturn(genUser(userPrefix + "assignee"));
        when(issue.getReporter()).thenReturn(genUser(userPrefix + "reporter"));
        final Comment comment1 = mock(Comment.class);
        final Comment comment2 = mock(Comment.class);
        final Comment comment3 = mock(Comment.class);
        final Comment comment4 = mock(Comment.class);
        when(commentService.getCommentsForUser(authUser, issue)).thenReturn(ImmutableList.of(comment1, comment2, comment3, comment4));

        final ZonedDateTime now = ZonedDateTime.now();
        when(comment1.getAuthorApplicationUser()).thenReturn(genUser(userPrefix + "commenter_1", "B", "a@a.a"));
        when(comment1.getCreated()).thenReturn(toDate(now.minusDays(1)));
        when(comment2.getAuthorApplicationUser()).thenReturn(genUser(userPrefix + "commenter_2", "F", "a@a.a"));
        when(comment2.getCreated()).thenReturn(toDate(now.minusDays(3)));
        when(comment3.getAuthorApplicationUser()).thenReturn(genUser(userPrefix + "commenter_3", "D", "a@a.a"));
        when(comment3.getCreated()).thenReturn(toDate(now.minusDays(3)));
        when(comment4.getAuthorApplicationUser()).thenReturn(genUser(userPrefix + "commenter_4", "C", "a@a.a"));
        when(comment4.getCreated()).thenReturn(toDate(now.minusDays(2)));

        when(userSearchService.findUsers(eq(userPrefix), any(UserSearchParams.class))).thenReturn(ImmutableList.of(
                genUser(userPrefix + "1", "C", "a@a.a"),
                genUser(userPrefix + "2", "D", "a@a.a"),
                genUser(userPrefix + "3", "B", "a@a.a"),
                genUser(userPrefix + "4", "A", "a@a.a"),
                genUser(userPrefix + "5", "E", "a@a.a")
        ));

        final List<UserIssueRelevance> result = mentionableUserSearch.findRelatedUsersToMention(userPrefix, issue, authUser, 10);
        final List<ApplicationUser> users = result.stream()
                .map(UserIssueRelevance::getUser)
                .collect(Collectors.toList());

        assertThat(result.size(), is(10));
        assertThat(users.get(0), is(genUser(userPrefix + "assignee")));
        assertThat(users.get(1), is(genUser(userPrefix + "reporter")));
        assertThat(users.get(2), is(genUser(userPrefix + "commenter_1")));
        assertThat(users.get(3), is(genUser(userPrefix + "commenter_4")));
        assertThat(users.get(4), is(genUser(userPrefix + "commenter_3")));
        assertThat(users.get(5), is(genUser(userPrefix + "commenter_2")));
        assertThat(users.get(6), is(genUser(userPrefix + "4")));
        assertThat(users.get(7), is(genUser(userPrefix + "3")));
        assertThat(users.get(8), is(genUser(userPrefix + "1")));
        assertThat(users.get(9), is(genUser(userPrefix + "2")));
    }

    @Test
    public void dropsCommentsWithNoAuthor() {
        final String userPrefix = "user_";
        when(issue.getAssignee()).thenReturn(genUser(userPrefix + "assignee"));
        when(issue.getReporter()).thenReturn(genUser(userPrefix + "reporter"));
        final Comment comment1 = mock(Comment.class);
        final Comment comment2 = mock(Comment.class);
        final Comment comment3 = mock(Comment.class);
        final Comment comment4 = mock(Comment.class);
        when(commentService.getCommentsForUser(authUser, issue)).thenReturn(ImmutableList.of(comment1, comment2, comment3, comment4));

        final ZonedDateTime now = ZonedDateTime.now();
        when(comment1.getAuthorApplicationUser()).thenReturn(genUser(userPrefix + "commenter_1"));
        when(comment1.getCreated()).thenReturn(toDate(now.minusDays(1)));
        when(comment2.getCreated()).thenReturn(toDate(now.minusDays(2)));
        when(comment3.getCreated()).thenReturn(toDate(now.minusDays(3)));
        when(comment4.getAuthorApplicationUser()).thenReturn(genUser(userPrefix + "commenter_4"));
        when(comment4.getCreated()).thenReturn(toDate(now.minusDays(3)));

        when(userSearchService.findUsers(eq(userPrefix), any(UserSearchParams.class))).thenReturn(ImmutableList.of(
                genUser(userPrefix + "1"),
                genUser(userPrefix + "2"),
                genUser(userPrefix + "3"),
                genUser(userPrefix + "4"),
                genUser(userPrefix + "5")
        ));

        final List<UserIssueRelevance> result = mentionableUserSearch.findRelatedUsersToMention(userPrefix, issue, authUser, 5);
        final List<ApplicationUser> users = result.stream()
                .map(UserIssueRelevance::getUser)
                .collect(Collectors.toList());

        assertThat(result.size(), is(5));
        assertThat(users.get(0), is(genUser(userPrefix + "assignee")));
        assertThat(users.get(1), is(genUser(userPrefix + "reporter")));
        assertThat(users.get(2), is(genUser(userPrefix + "commenter_1")));
        assertThat(users.get(3), is(genUser(userPrefix + "commenter_4")));
    }

    @Test
    public void sortsCorrectlyWhenInvolvedUsersSizeGreaterThanMaxResults() {
        final String userPrefix = "user_";
        when(issue.getAssignee()).thenReturn(genUser(userPrefix + "assignee"));
        when(issue.getReporter()).thenReturn(genUser(userPrefix + "reporter"));
        final Comment comment1 = mock(Comment.class);
        final Comment comment2 = mock(Comment.class);
        final Comment comment3 = mock(Comment.class);
        final Comment comment4 = mock(Comment.class);
        when(commentService.getCommentsForUser(authUser, issue)).thenReturn(ImmutableList.of(comment1, comment2, comment3, comment4));

        final ZonedDateTime now = ZonedDateTime.now();
        when(comment1.getAuthorApplicationUser()).thenReturn(genUser(userPrefix + "commenter_1", "B", "a@a.a"));
        when(comment1.getCreated()).thenReturn(toDate(now.minusDays(1)));
        when(comment2.getAuthorApplicationUser()).thenReturn(genUser(userPrefix + "commenter_2", "F", "a@a.a"));
        when(comment2.getCreated()).thenReturn(toDate(now.minusDays(3)));
        when(comment3.getAuthorApplicationUser()).thenReturn(genUser(userPrefix + "commenter_3", "D", "a@a.a"));
        when(comment3.getCreated()).thenReturn(toDate(now.minusDays(3)));
        when(comment4.getAuthorApplicationUser()).thenReturn(genUser(userPrefix + "commenter_4", "C", "a@a.a"));
        when(comment4.getCreated()).thenReturn(toDate(now.minusDays(2)));

        final List<UserIssueRelevance> result = mentionableUserSearch.findRelatedUsersToMention(userPrefix, issue, authUser, 5);
        final List<ApplicationUser> users = result.stream()
                .map(UserIssueRelevance::getUser)
                .collect(Collectors.toList());

        assertThat(result.size(), is(5));
        assertThat(users.get(0), is(genUser(userPrefix + "assignee")));
        assertThat(users.get(1), is(genUser(userPrefix + "reporter")));
        assertThat(users.get(2), is(genUser(userPrefix + "commenter_1")));
        assertThat(users.get(3), is(genUser(userPrefix + "commenter_4")));
        assertThat(users.get(4), is(genUser(userPrefix + "commenter_3")));
    }

    @Test
    public void returnsAllUsersSortedByRelevancyIfEmptyStringPassedAsQuery() {
        when(issue.getAssignee()).thenReturn(genUser("assignee"));
        when(issue.getReporter()).thenReturn(genUser("reporter"));
        final Comment comment1 = mock(Comment.class);
        final Comment comment2 = mock(Comment.class);
        when(commentService.getCommentsForUser(authUser, issue)).thenReturn(ImmutableList.of(comment1, comment2));

        final ZonedDateTime now = ZonedDateTime.now();
        when(comment1.getAuthorApplicationUser()).thenReturn(genUser("commenter_1"));
        when(comment1.getCreated()).thenReturn(toDate(now.minusDays(1)));
        when(comment2.getAuthorApplicationUser()).thenReturn(genUser("commenter_2"));
        when(comment2.getCreated()).thenReturn(toDate(now.minusDays(2)));

        when(userSearchService.findUsers(eq(""), any(UserSearchParams.class))).thenReturn(ImmutableList.of(
                genUser("user_1"),
                genUser("user_2"),
                genUser("user_3"),
                genUser("user_4"),
                genUser("user_5")
        ));

        final List<UserIssueRelevance> result = mentionableUserSearch.findRelatedUsersToMention("", issue, authUser, 5);
        final List<ApplicationUser> users = result.stream()
                .map(UserIssueRelevance::getUser)
                .collect(Collectors.toList());

        assertThat(result.size(), is(5));
        assertThat(users.get(0), is(genUser("assignee")));
        assertThat(users.get(1), is(genUser("reporter")));
        assertThat(users.get(2), is(genUser("commenter_1")));
        assertThat(users.get(3), is(genUser("commenter_2")));
    }

    // This is testing an internal implementation detail, and it's a pretty
    // hacky test but hopefully it will prevent people from making what looks
    // like an obvious improvement but is in-fact a subtle bug.
    @Test
    public void returnsMaxResultsEvenIfUserSearchResultsOverlapWithInvolvedUsers() {
        when(issue.getAssignee()).thenReturn(genUser("assignee"));
        when(issue.getReporter()).thenReturn(genUser("reporter"));
        final Comment comment1 = mock(Comment.class);
        final Comment comment2 = mock(Comment.class);
        when(commentService.getCommentsForUser(authUser, issue)).thenReturn(ImmutableList.of(comment1, comment2));

        final ZonedDateTime now = ZonedDateTime.now();
        when(comment1.getAuthorApplicationUser()).thenReturn(genUser("commenter_1"));
        when(comment1.getCreated()).thenReturn(toDate(now.minusDays(1)));
        when(comment2.getAuthorApplicationUser()).thenReturn(genUser("commenter_2"));
        when(comment2.getCreated()).thenReturn(toDate(now.minusDays(2)));

        when(userSearchService.findUsers(eq(""), argThat(hasProperty("maxResults", equalTo(1))))).thenReturn(ImmutableList.of(
                genUser("assignee")
        ));

        when(userSearchService.findUsers(eq(""), argThat(hasProperty("maxResults", equalTo(5))))).thenReturn(ImmutableList.of(
                genUser("assignee"),
                genUser("reporter"),
                genUser("commenter_1"),
                genUser("commenter_2"),
                genUser("user_1")
        ));

        final List<UserIssueRelevance> result = mentionableUserSearch.findRelatedUsersToMention("", issue, authUser, 5);

        assertThat(result.size(), is(5));
    }

    private ApplicationUser genUser(final String username) {
        return new MockApplicationUser(username);
    }

    private ApplicationUser genUser(final String username, final String displayName, final String email) {
        return new MockApplicationUser(username, displayName, email);
    }

    private Date toDate(final ZonedDateTime ZonedDateTime) {
        return Date.from(ZonedDateTime.toInstant());
    }
}
