package com.atlassian.jira.issue.views.util.csv;

import java.text.SimpleDateFormat;
import java.util.Locale;

import com.atlassian.jira.datetime.DateTimeFormatterFactory;
import com.atlassian.jira.datetime.DateTimeFormatterFactoryStub;
import com.atlassian.jira.junit.rules.InitMockitoMocks;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.mockito.InjectMocks;
import org.mockito.Spy;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

public class CsvDateFormatterImplTest {
    @Rule
    public TestRule initMockito = new InitMockitoMocks(this);

    private static final String DATE_FORMAT = "dd/MMM/yy hh:mm a";
    private static final DateTime NOW = DateTime.now();
    private final static DateTimeZone tzNewYork = DateTimeZone.forID("America/New_York");
    private final static DateTimeZone tzSydney = DateTimeZone.forID("Australia/Sydney");

    @Spy
    private final DateTimeFormatterFactory formatterFactory = new DateTimeFormatterFactoryStub();

    @InjectMocks
    private CsvDateFormatterImpl formatter;

    @Test
    public void datesAreAbsolute() {
        final SimpleDateFormat expectedFormat = new SimpleDateFormat(DATE_FORMAT);

        assertThat(formatter.formatDateTime(NOW.toDate()), equalTo(expectedFormat.format(NOW.toDate())));
        assertThat(formatter.formatDate(NOW.toDate()), equalTo(expectedFormat.format(NOW.toDate())));
    }

    @Test
    public void datesAreInUserLocale() {
        final Locale localeUser = getUserLocale();
        final SimpleDateFormat expectedFormat = new SimpleDateFormat(DATE_FORMAT, localeUser);

        final DateTimeFormatterFactory factory = new DateTimeFormatterFactoryStub().userLocale(localeUser);
        final CsvDateFormatter formatter = new CsvDateFormatterImpl(factory);

        assertThat(formatter.formatDateTime(NOW.toDate()), equalTo(expectedFormat.format(NOW.toDate())));
        assertThat(formatter.formatDate(NOW.toDate()), equalTo(expectedFormat.format(NOW.toDate())));
    }

    @Test
    public void datesAreInUserTimezone() {
        final DateTimeZone tzUser = getUserTimezone();
        final SimpleDateFormat expectedFormat = new SimpleDateFormat(DATE_FORMAT);
        expectedFormat.setTimeZone(tzUser.toTimeZone());

        final DateTimeFormatterFactory factory = new DateTimeFormatterFactoryStub().userTimeZone(tzUser);
        final CsvDateFormatter formatter = new CsvDateFormatterImpl(factory);

        assertThat(formatter.formatDateTime(NOW.toDate()), equalTo(expectedFormat.format(NOW.toDate())));
    }

    @Test
    public void dateWithoutTimeIsInSystemTimezone() {
        final DateTimeZone tzUser = getUserTimezone();
        final SimpleDateFormat expectedFormat = new SimpleDateFormat(DATE_FORMAT);

        final DateTimeFormatterFactory factory = new DateTimeFormatterFactoryStub().userTimeZone(tzUser);
        final CsvDateFormatter formatter = new CsvDateFormatterImpl(factory);

        assertThat(formatter.formatDate(NOW.toDate()), equalTo(expectedFormat.format(NOW.toDate())));
    }

    @Test
    public void nullIsReturnedForNullDate() {
        assertThat(formatter.formatDate(null), equalTo(null));
    }

    private DateTimeZone getUserTimezone() {
        return DateTimeZone.getDefault().equals(tzNewYork) ? tzSydney : tzNewYork;
    }

    private Locale getUserLocale() {
        return Locale.getDefault().equals(Locale.ENGLISH) ? Locale.JAPANESE : Locale.ENGLISH;
    }
}
