package com.atlassian.jira.appconsistency.db;

import com.atlassian.jira.config.database.DatabaseConfig;
import com.atlassian.jira.config.database.DatabaseConfigurationManager;
import com.atlassian.jira.config.database.Datasource;
import com.atlassian.jira.config.properties.JiraProperties;
import com.atlassian.jira.ofbiz.OfBizConnectionFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.contains;
import static org.mockito.Mockito.RETURNS_DEEP_STUBS;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestCollationCheck {

    // These values are used to create the CollationCheck
    private String collation;
    private String databaseType;
    private boolean collationQueryFails;

    @Mock
    private OfBizConnectionFactory connectionFactory;
    @Mock
    private DatabaseConfigurationManager dbConfigurationManager;

    @Before
    public void before() {
        collation = null;
        databaseType = null;
        collationQueryFails = false;
    }

    @Test
    public void collationNotSupportedShowsWarning() {
        databaseType = "postgres72";
        collation = "en_AU.UTF-8";
        CollationCheck collationCheck = getCollationCheck();

        boolean isOk = collationCheck.isOk();

        assertTrue(isOk);
        assertWarning(collationCheck, "You are using an unsupported postgres72 collation: en_AU.UTF-8");
    }

    @Test
    public void collationSupportedShowsNoWarning() {
        databaseType = "postgres72";
        collation = "POSIX";
        CollationCheck collationCheck = getCollationCheck();

        boolean isOk = collationCheck.isOk();

        assertTrue(isOk);
        assertNoWarning(collationCheck);
    }

    @Test
    public void usingHsqlShowsNoWarning() {
        databaseType = "hsql";
        collation = null;
        CollationCheck collationCheck = getCollationCheck();

        boolean isOk = collationCheck.isOk();

        assertTrue(isOk);
        assertNoWarning(collationCheck);
    }

    @Test
    public void usingUnknownDatabaseShowsWarning() {
        databaseType = "unknown database";
        collation = null;
        CollationCheck collationCheck = getCollationCheck();

        boolean isOk = collationCheck.isOk();

        assertTrue(isOk);
        assertWarning(collationCheck, "Your database is not supported");
    }

    @Test
    public void readingCollationFailsShowsWarning() {
        databaseType = "postgres72";
        collationQueryFails = true;
        CollationCheck collationCheck = getCollationCheck();

        boolean isOk = collationCheck.isOk();

        assertTrue(isOk);
        assertWarning(collationCheck, "collation could not be read");
    }

    private CollationCheck getCollationCheck() {
        try {
            Connection connection = mock(Connection.class, RETURNS_DEEP_STUBS);
            when(connectionFactory.getConnection()).thenReturn(connection);

            if (collationQueryFails) {
                when(connection.prepareStatement(anyString())).thenThrow(new SQLException());
            } else {
                when(connection.getMetaData().getURL()).thenReturn("jdbc:postgresql://localhost:5432/jiradb");
                PreparedStatement preparedStatement = mock(PreparedStatement.class);
                when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);
                ResultSet resultSet = mock(ResultSet.class);
                when(resultSet.getString(1)).thenReturn(collation);
                when(preparedStatement.executeQuery()).thenReturn(resultSet);
            }

            DatabaseConfig databaseConfig = new DatabaseConfig(databaseType, "schema", mock(Datasource.class));
            when(dbConfigurationManager.getDatabaseConfiguration()).thenReturn(databaseConfig);

            JiraProperties jiraProperties = mock(JiraProperties.class);
            when(jiraProperties.getProperty("line.separator")).thenReturn("\n");

            final CollationCheck.WarningLogger mockWarningLogger = mock(CollationCheck.WarningLogger.class);

            CollationCheck collationCheck = new CollationCheck(dbConfigurationManager, connectionFactory, jiraProperties) {
                @Override
                WarningLogger getWarningLogger() {
                    return mockWarningLogger;
                }
            };

            return collationCheck;
        } catch (Exception e) {
            // Swallow mock exceptions
            return null;
        }
    }

    private void assertWarning(final CollationCheck collationCheck, String warningMessage) {
        verify(collationCheck.getWarningLogger(), times(1)).showWarning(contains(warningMessage));
    }

    private void assertNoWarning(CollationCheck collationCheck) {
        verify(collationCheck.getWarningLogger(), never()).showWarning(anyString());
    }
}

