package com.atlassian.jira.upgrade.util;

import org.apache.log4j.AppenderSkeleton;
import org.apache.log4j.Level;
import org.apache.log4j.spi.LoggingEvent;

import java.util.ArrayList;
import java.util.List;

/**
 * A log appender to be able to check if certain messages have been logged
 *
 * @since v7.0
 */
public class TestLogAppender extends AppenderSkeleton {
    private final List<LoggingEvent> log = new ArrayList<LoggingEvent>();

    @Override
    public boolean requiresLayout() {
        return false;
    }

    @Override
    protected void append(final LoggingEvent loggingEvent) {
        log.add(loggingEvent);
    }

    @Override
    public void close() {
    }

    public List<LoggingEvent> getLog() {
        return log;
    }

    public boolean contains(String msg, Level level) {
        for (LoggingEvent logEntry : log) {
            if (logEntry.getLevel().equals(level) && logEntry.getMessage().toString().contains(msg)) {
                return true;
            }
        }
        return false;
    }
}
