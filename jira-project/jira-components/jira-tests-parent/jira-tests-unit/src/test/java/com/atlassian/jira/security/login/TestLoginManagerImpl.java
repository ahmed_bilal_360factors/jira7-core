package com.atlassian.jira.security.login;

import com.atlassian.crowd.embedded.api.CrowdService;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.bc.security.login.LoginInfo;
import com.atlassian.jira.bc.security.login.LoginInfoImpl;
import com.atlassian.jira.bc.security.login.LoginReason;
import com.atlassian.jira.bc.security.login.LoginResult;
import com.atlassian.jira.bc.security.login.LoginService;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.mock.component.MockComponentWorker;
import com.atlassian.jira.mock.servlet.MockHttpServletRequest;
import com.atlassian.jira.mock.servlet.MockHttpSession;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.auth.AuthorisationManager;
import com.atlassian.jira.servlet.JiraCaptchaService;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.util.MockUserManager;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.velocity.VelocityRequestContext;
import com.atlassian.jira.util.velocity.VelocityRequestContextFactory;
import com.atlassian.seraph.auth.Authenticator;
import com.atlassian.seraph.auth.AuthenticatorException;
import com.octo.captcha.service.CaptchaServiceException;
import com.octo.captcha.service.image.ImageCaptchaService;
import org.hamcrest.FeatureMatcher;
import org.hamcrest.Matcher;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static com.atlassian.jira.matchers.FeatureMatchers.hasFeature;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.isA;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.argThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.atMost;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

/**
 */
public class TestLoginManagerImpl {
    @Rule
    public RuleChain mockitoMocksInContainer = MockitoMocksInContainer.forTest(this);

    private static final String AUTHORISED_FAILURE = "com.atlassian.jira.security.login.LoginManager.AUTHORISED_FAILURE";
    private static final String AUTHORISING_USER_KEY = "com.atlassian.jira.security.login.LoginManager.AUTHORISING_USER_KEY";
    private static final String ELEVATED_SECURITY_FAILURE = "com.atlassian.jira.security.login.LoginManager.ELEVATED_SECURITY_FAILURE";
    private static final String OS_CAPTCHA = "os_captcha";

    MockApplicationUser fred = new MockApplicationUser("fred");
    MockApplicationUser fredAppUser = new MockApplicationUser("fred");
    @Mock
    private LoginStore loginStore;
    @Mock
    private AuthorisationManager authorisationManager;
    @Mock
    private HttpServletRequest httpServletRequest;
    @Mock
    private HttpServletResponse httpServletResponse;
    @Mock
    private LoginManagerImpl.StaticDependencies staticDependencies;
    @Mock
    private Authenticator authenticator;
    @Mock
    private CrowdService crowdService;
    @Mock
    private JiraCaptchaService jiraCaptchaService;
    @Mock
    private ImageCaptchaService imageCaptchaService;
    @Mock
    private VelocityRequestContextFactory velocityRequestContextFactory;
    @Mock
    private VelocityRequestContext velocityRequestContext;
    @Mock
    private EventPublisher eventPublisher;
    @Mock
    @AvailableInContainer
    private JiraAuthenticationContext jiraAuthenticationContext;
    @InjectMocks
    private LoginManagerImpl loginManager;

    @Before
    public void setUp() throws Exception {
        MockComponentWorker componentAccessorWorker = new MockComponentWorker();

        componentAccessorWorker
                .addMock(UserManager.class, new MockUserManager().alwaysReturnUsers())
                .addMock(JiraAuthenticationContext.class, jiraAuthenticationContext);

        ComponentAccessor.initialiseWorker(componentAccessorWorker);

        loginManager = new LoginManagerImpl(staticDependencies, loginStore, jiraAuthenticationContext, crowdService, jiraCaptchaService, velocityRequestContextFactory, eventPublisher, authorisationManager);
    }

    @Test
    public void testAuthoriseLogin_NoUsePermission() {
        when(authorisationManager.authoriseForLogin(fredAppUser, httpServletRequest)).thenReturn(false);

        assertFalse(loginManager.authoriseForLogin(fredAppUser, httpServletRequest));

        verify(httpServletRequest).removeAttribute(AUTHORISED_FAILURE);
        verify(httpServletRequest).setAttribute(AUTHORISING_USER_KEY, "fred");
        verify(httpServletRequest).setAttribute(AUTHORISED_FAILURE, true);
    }


    @Test
    public void testAuthoriseLogin_OK() {
        when(authorisationManager.authoriseForLogin(fredAppUser, httpServletRequest)).thenReturn(true);

        assertTrue(loginManager.authoriseForLogin(fredAppUser, httpServletRequest));

        verify(httpServletRequest).removeAttribute(AUTHORISED_FAILURE);
        verify(httpServletRequest).setAttribute(AUTHORISING_USER_KEY, "fred");
    }

    @Test
    public void testAuthoriseLogin_OK_admin() {
        when(authorisationManager.authoriseForLogin(fredAppUser, httpServletRequest)).thenReturn(true);

        assertTrue(loginManager.authoriseForLogin(fredAppUser, httpServletRequest));

        verify(httpServletRequest).removeAttribute(AUTHORISED_FAILURE);
        verify(httpServletRequest).setAttribute(AUTHORISING_USER_KEY, "fred");
    }

    @Test
    public void testGetLoginInfo() {
        LoginInfo expectedLoginInfo = makeLoginInfo();
        when(crowdService.getUser(fred.getName())).thenReturn(fred.getDirectoryUser());
        when(loginStore.getLoginInfo(fred)).thenReturn(expectedLoginInfo);
        when(loginStore.getMaxAuthenticationAttemptsAllowed()).thenReturn(3L);

        final LoginInfo loginInfo = loginManager.getLoginInfo(fred.getName());

        assertNotNull(loginInfo);
        assertEquals(new Long(123L), loginInfo.getLastLoginTime());
        assertEquals(new Long(456L), loginInfo.getPreviousLoginTime());
        assertEquals(new Long(789L), loginInfo.getLastFailedLoginTime());
        assertEquals(new Long(101112L), loginInfo.getLoginCount());
        assertEquals(new Long(131415L), loginInfo.getCurrentFailedLoginCount());
        assertEquals(new Long(171819L), loginInfo.getTotalFailedLoginCount());
    }

    @Test
    public void test_performElevatedSecurityCheck_NotNeeded() {
        LoginInfo expectedLoginInfo = makeLoginInfo(1L);

        when(loginStore.getLoginInfo(fred)).thenReturn(expectedLoginInfo);
        when(loginStore.getMaxAuthenticationAttemptsAllowed()).thenReturn(3L);
        when(crowdService.getUser(fred.getName())).thenReturn(fred.getDirectoryUser());

        assertTrue(loginManager.performElevatedSecurityCheck(httpServletRequest, "fred"));
        verify(httpServletRequest).removeAttribute(ELEVATED_SECURITY_FAILURE);
    }

    @Test
    public void test_performElevatedSecurityCheck_UserNotKnown() {
        when(crowdService.getUser(fred.getName())).thenReturn(null);

        assertTrue(loginManager.performElevatedSecurityCheck(httpServletRequest, "fred"));
        verify(httpServletRequest).removeAttribute(ELEVATED_SECURITY_FAILURE);
    }

    private void expect_getLoginInfo(final long currentFailedLoginCount, final long maxAttempts) {
        LoginInfo expectedLoginInfo = makeLoginInfo(currentFailedLoginCount);

        when(loginStore.getLoginInfo(fred)).thenReturn(expectedLoginInfo);
        when(loginStore.getMaxAuthenticationAttemptsAllowed()).thenReturn(maxAttempts);
        when(crowdService.getUser(fred.getName())).thenReturn(fred.getDirectoryUser());
    }

    @Test
    public void test_performElevatedSecurityCheck_Needed_ButFAIL() {
        when(httpServletRequest.getParameter(OS_CAPTCHA)).thenReturn("aseasyasabc");
        when(httpServletRequest.getSession(true)).thenReturn(new MockHttpSession());
        when(jiraCaptchaService.getImageCaptchaService()).thenReturn(imageCaptchaService);
        when(imageCaptchaService.validateResponseForID("session1234", "aseasyasabc")).thenReturn(false);
        expect_getLoginInfo(5L, 3L);

        assertFalse(loginManager.performElevatedSecurityCheck(httpServletRequest, "fred"));
        verify(httpServletRequest).removeAttribute(ELEVATED_SECURITY_FAILURE);
        verify(httpServletRequest).setAttribute(ELEVATED_SECURITY_FAILURE, true);
    }

    @Test
    public void test_performElevatedSecurityCheck_Needed_ButCAPTCHAThrowsUp() {
        when(httpServletRequest.getParameter(OS_CAPTCHA)).thenReturn("aseasyasabc");
        when(httpServletRequest.getSession(true)).thenReturn(new MockHttpSession());
        when(jiraCaptchaService.getImageCaptchaService()).thenReturn(imageCaptchaService);
        when(imageCaptchaService.validateResponseForID("session1234", "aseasyasabc")).thenThrow(new CaptchaServiceException("Expected exception"));

        expect_getLoginInfo(5L, 3L);

        assertFalse(loginManager.performElevatedSecurityCheck(httpServletRequest, "fred"));
        verify(httpServletRequest).removeAttribute(ELEVATED_SECURITY_FAILURE);
        verify(httpServletRequest).setAttribute(ELEVATED_SECURITY_FAILURE, true);
    }

    @Test
    public void test_performElevatedSecurityCheck_Needed_AndOK() {
        when(httpServletRequest.getParameter(OS_CAPTCHA)).thenReturn("aseasyasabc");
        when(httpServletRequest.getSession(true)).thenReturn(new MockHttpSession());
        when(jiraCaptchaService.getImageCaptchaService()).thenReturn(imageCaptchaService);
        when(imageCaptchaService.validateResponseForID("session1234", "aseasyasabc")).thenReturn(true);
        expect_getLoginInfo(5L, 3L);

        assertTrue(loginManager.performElevatedSecurityCheck(httpServletRequest, "fred"));
        verify(httpServletRequest).removeAttribute(ELEVATED_SECURITY_FAILURE);
    }

    @Test
    public void test_authenticate_And_ElevatedSecurityNeeded() {
        expect_getLoginInfo(5L, 3L);     // causes elevated security to be needed!

        when(loginStore.recordLoginAttempt(fred, false)).thenReturn(makeLoginInfo());
        final LoginResult loginResult = loginManager.authenticate(fred, "password");

        assertNotNull(loginResult);
        assertFalse(loginResult.isOK());
        assertEquals(LoginReason.AUTHENTICATION_DENIED, loginResult.getReason());
    }

    @Test
    public void test_authenticate_And_BadPassword() {
        expect_getLoginInfo(0L, 3L);
        when(staticDependencies.authenticate(fred, "badpassword")).thenReturn(false);
        when(loginStore.recordLoginAttempt(fred, false)).thenReturn(makeLoginInfo());

        final LoginResult loginResult = loginManager.authenticate(fred, "badpassword");

        assertNotNull(loginResult);
        assertFalse(loginResult.isOK());
        assertEquals(LoginReason.AUTHENTICATED_FAILED, loginResult.getReason());
    }

    @Test
    public void test_authenticate_And_OK() {
        expect_getLoginInfo(0L, 3L);
        when(staticDependencies.authenticate(fred, "password")).thenReturn(true);
        when(loginStore.recordLoginAttempt(fred, true)).thenReturn(makeLoginInfo());
        when(jiraAuthenticationContext.getLoggedInUser()).thenReturn(fred);

        final LoginResult loginResult = loginManager.authenticate(fred, "password");

        assertNotNull(loginResult);
        assertTrue(loginResult.isOK());
        assertEquals(LoginReason.OK, loginResult.getReason());
        verify(eventPublisher).publish(anyObject());
    }

    @Test
    public void test_OnLoginAttempt_UnknownUser() {
        when(crowdService.getUser("unknown")).thenReturn(null);

        assertNull(loginManager.onLoginAttempt(httpServletRequest, "unknown", false));
    }

    @Test
    public void test_OnLoginAttempt_Failure() {
        expect_getLoginInfo(2L, 5L);
        when(loginStore.recordLoginAttempt(fred, false)).thenReturn(makeLoginInfo());
        when(httpServletRequest.getAttribute(ELEVATED_SECURITY_FAILURE)).thenReturn(null);
        when(httpServletRequest.getAttribute(AUTHORISED_FAILURE)).thenReturn(null);
        when(httpServletRequest.getAttribute(ELEVATED_SECURITY_FAILURE)).thenReturn(null);

        final LoginInfo loginInfo = loginManager.onLoginAttempt(httpServletRequest, fred.getName(), false);

        Matcher<LoginReason> innerMatcher = equalTo(LoginReason.AUTHENTICATED_FAILED);
        verify(httpServletRequest).setAttribute(eq(LoginService.LOGIN_RESULT), argThat(allOf(
                isA(LoginResult.class), hasUsernameThat(equalTo(fred.getName())), hasReasonThat(innerMatcher)
        )));
        verifyZeroInteractions(eventPublisher);
        assertNotNull(loginInfo);
    }

    private FeatureMatcher<LoginResult, LoginReason> hasReasonThat(final Matcher<LoginReason> innerMatcher) {
        return hasFeature(LoginResult::getReason, innerMatcher, "LoginResult with reason that", "reason");
    }

    private FeatureMatcher<LoginResult, String> hasUsernameThat(final Matcher<String> usernameMatcher) {
        return hasFeature(LoginResult::getUserName, usernameMatcher, "LoginResult with username that", "username");
    }

    @Test
    public void test_OnLoginAttempt_Failure_butHadPreviouslyFailedElevatedCheck() {
        expect_getLoginInfo(2L, 5L);

        when(loginStore.recordLoginAttempt(fred, false)).thenReturn(makeLoginInfo());

        when(httpServletRequest.getAttribute(ELEVATED_SECURITY_FAILURE)).thenReturn(true);
        when(httpServletRequest.getAttribute(ELEVATED_SECURITY_FAILURE)).thenReturn(true);
        ArgumentCaptor<LoginResult> loginResultCapture = ArgumentCaptor.forClass(LoginResult.class);

        when(velocityRequestContextFactory.getJiraVelocityRequestContext()).thenReturn(velocityRequestContext);
        when(velocityRequestContext.getCanonicalBaseUrl()).thenReturn("http://localhost");

        final LoginInfo loginInfo = loginManager.onLoginAttempt(httpServletRequest, fred.getName(), false);
        Matcher<LoginReason> innerMatcher = equalTo(LoginReason.AUTHENTICATION_DENIED);
        verify(httpServletRequest).setAttribute(eq(LoginService.LOGIN_RESULT), argThat(allOf(
                isA(LoginResult.class), hasUsernameThat(equalTo(fred.getName())), hasReasonThat(innerMatcher)
        )));
        verifyZeroInteractions(eventPublisher);
        assertNotNull(loginInfo);
    }

    @Test
    public void test_OnLoginAttempt_Failure_butHadPreviouslyFailedAuthorisationCheck() {
        expect_getLoginInfo(2L, 5L);

        when(loginStore.recordLoginAttempt(fred, false)).thenReturn(makeLoginInfo());

        when(httpServletRequest.getAttribute(ELEVATED_SECURITY_FAILURE)).thenReturn(null);
        when(httpServletRequest.getAttribute(AUTHORISED_FAILURE)).thenReturn(true);
        when(httpServletRequest.getAttribute(ELEVATED_SECURITY_FAILURE)).thenReturn(null);
        ArgumentCaptor<LoginResult> loginResultCapture = ArgumentCaptor.forClass(LoginResult.class);

        final LoginInfo loginInfo = loginManager.onLoginAttempt(httpServletRequest, fred.getName(), false);
        Matcher<LoginReason> innerMatcher = equalTo(LoginReason.AUTHORISATION_FAILED);
        verify(httpServletRequest).setAttribute(eq(LoginService.LOGIN_RESULT), argThat(allOf(
                isA(LoginResult.class), hasUsernameThat(equalTo(fred.getName())), hasReasonThat(innerMatcher)
        )));
        verifyZeroInteractions(eventPublisher);
        assertNotNull(loginInfo);
    }

    @Test
    public void test_OnLoginAttempt_OK() {
        expect_getLoginInfo(2L, 5L);
        when(loginStore.recordLoginAttempt(fred, true)).thenReturn(makeLoginInfo());
        ArgumentCaptor<LoginResult> loginResultCapture = ArgumentCaptor.forClass(LoginResult.class);
        when(httpServletRequest.getAttribute(ELEVATED_SECURITY_FAILURE)).thenReturn(null);
        when(jiraAuthenticationContext.getLoggedInUser()).thenReturn(fred);

        final LoginInfo loginInfo = loginManager.onLoginAttempt(httpServletRequest, fred.getName(), true);
        Matcher<LoginReason> innerMatcher = equalTo(LoginReason.OK);

        verify(httpServletRequest, atMost(1)).getAttribute(ELEVATED_SECURITY_FAILURE);
        verify(httpServletRequest).setAttribute(eq(LoginService.LOGIN_RESULT), argThat(allOf(
                isA(LoginResult.class), hasUsernameThat(equalTo(fred.getName())), hasReasonThat(innerMatcher)
        )));
        verify(eventPublisher).publish(anyObject());
        assertNotNull(loginInfo);
    }

    @Test
    public void testLogout() throws AuthenticatorException {
        final MockHttpSession httpSession = new MockHttpSession();
        httpSession.setAttribute("somekey", "withsomevalue");
        assertNotNull(httpSession.getAttribute("somekey"));
        MockHttpServletRequest mockHttpServletRequest = new MockHttpServletRequest(httpSession);
        when(staticDependencies.getAuthenticator()).thenReturn(authenticator);
        when(authenticator.logout(mockHttpServletRequest, httpServletResponse)).thenReturn(true);
        when(jiraAuthenticationContext.getLoggedInUser()).thenReturn(fred);

        loginManager.logout(mockHttpServletRequest, httpServletResponse);

        assertNull(httpSession.getAttribute("somekey"));
        verify(jiraAuthenticationContext).clearLoggedInUser();
        verify(eventPublisher).publish(anyObject());
    }

    @Test
    public void testElevatedSecurityAlwaysShown_WhenZero() {
        when(loginStore.getMaxAuthenticationAttemptsAllowed()).thenReturn(0L);

        assertTrue(loginManager.isElevatedSecurityCheckAlwaysShown());
    }

    @Test
    public void testElevatedSecurityAlwaysShown_WhenGreaterZero() {
        when(loginStore.getMaxAuthenticationAttemptsAllowed()).thenReturn(3L);

        assertFalse(loginManager.isElevatedSecurityCheckAlwaysShown());
    }

    @Test
    public void test_resetFailedLoginCount() {
        loginManager.resetFailedLoginCount(fred);
        verify(loginStore).resetFailedLoginCount(fred);
    }

    private LoginInfo makeLoginInfo() {
        return makeLoginInfo(131415L);
    }

    private LoginInfo makeLoginInfo(final long currentFailedLoginCount) {
        return LoginInfoImpl.builder()
                .setLastLoginTime(123L)
                .setPreviousLoginTime(456L)
                .setLastFailedLoginTime(789L)
                .setLoginCount(101112L)
                .setCurrentFailedLoginCount(currentFailedLoginCount)
                .setTotalFailedLoginCount(171819L)
                .setMaxAuthenticationAttemptsAllowed(202122L)
                .setElevatedSecurityCheckRequired(false)
                .build();
    }

}
