package com.atlassian.jira.crowd.embedded.ofbiz;

import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.model.user.UserTemplateWithCredentialAndAttributes;
import com.atlassian.crowd.util.BatchResult;
import com.atlassian.jira.bc.user.search.UserIndexer;
import com.atlassian.jira.cluster.ClusterMessagingService;
import com.atlassian.jira.crowd.embedded.lucene.CrowdQueryTranslator;
import com.atlassian.jira.crowd.embedded.ofbiz.db.DefaultOfBizTransactionManager;
import com.atlassian.jira.crowd.embedded.ofbiz.db.OfBizTransactionManager;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.mockito.Mock;
import org.ofbiz.core.entity.GenericValue;

import java.security.Principal;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.empty;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anySet;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestIndexedUserDao {

    private static final long USER_ID_FOR_TESTING = 75L;

    @Rule
    public RuleChain ruleChain = MockitoMocksInContainer.forTest(this);

    private IndexedUserDao userDao;

    @Mock
    private ExtendedUserDao baseUserDao;

    @Mock
    private UserIndexer indexer;

    @Mock
    private CrowdQueryTranslator translator;

    @Mock
    private ClusterMessagingService clusterMessagingService;

    @Mock
    private OfBizTransactionManager ofBizTransactionManager;

    @Before
    public void setUp() throws Exception {
        int batchSize = 2;
        ofBizTransactionManager = new DefaultOfBizTransactionManager();
        userDao = new IndexedUserDao(baseUserDao, indexer, translator, clusterMessagingService, ofBizTransactionManager, batchSize);
        when(baseUserDao.findByNameOrNull(anyLong(), anyString()))
                .thenAnswer(invocation -> makeUser(invocation.getArgumentAt(0, Long.class), invocation.getArgumentAt(1, String.class)));
        when(baseUserDao.findByName(anyLong(), anyString()))
                .thenAnswer(invocation -> makeUser(invocation.getArgumentAt(0, Long.class), invocation.getArgumentAt(1, String.class)));
        when(baseUserDao.add(any(), any()))
                .thenAnswer(invocation -> invocation.getArgumentAt(0, User.class));
        when(baseUserDao.addAll(any()))
                .thenAnswer(invocation -> batchResult(ImmutableList.copyOf(invocation.getArgumentAt(0, Collection.class)), ImmutableList.of()));
        when(baseUserDao.removeAllUsers(anyLong(), anySet()))
                .thenAnswer(invocation -> batchResult(ImmutableList.copyOf(invocation.getArgumentAt(1, Collection.class)), ImmutableList.of()));
        when(baseUserDao.update(any()))
                .thenAnswer(invocation -> invocation.getArgumentAt(0, User.class));
        when(baseUserDao.rename(any(), anyString()))
                .thenAnswer(invocation -> makeUser(invocation.getArgumentAt(0, User.class).getDirectoryId(), invocation.getArgumentAt(1, String.class)));
    }

    @Test
    public void partitionSetTest5To2() {
        Set<String> s = ImmutableSet.of("one", "two", "three", "four", "five");
        List<? extends Set<String>> results =  IndexedUserDao.partitionSet(s, 2);
        assertThat(results, contains(ImmutableSet.of("one", "two"), ImmutableSet.of("three", "four"), ImmutableSet.of("five")));
    }

    @Test
    public void partitionSetTest4To2() {
        Set<String> s = ImmutableSet.of("one", "two", "three", "four");
        List<? extends Set<String>> results =  IndexedUserDao.partitionSet(s, 2);
        assertThat(results, contains(ImmutableSet.of("one", "two"), ImmutableSet.of("three", "four")));
    }

    @Test
    public void partitionSetTest2To3() {
        Set<String> s = ImmutableSet.of("one", "two");
        List<? extends Set<String>> results =  IndexedUserDao.partitionSet(s, 3);
        assertThat(results, contains(ImmutableSet.of("one", "two")));
    }

    @Test
    public void partitionSetTestEmpty() {
        Set<String> s = ImmutableSet.of();
        List<? extends Set<String>> results =  IndexedUserDao.partitionSet(s, 2);
        assertThat(results, empty());
    }

    private static <T> BatchResult<T> batchResult(List<T> successes, List<T> failures) {
        BatchResult<T> br = new BatchResult<>(successes.size() + failures.size());
        br.addSuccesses(successes);
        br.addFailures(failures);
        return br;
    }

    @Test
    public void combineBatchResultsTest() {
        List<BatchResult<String>> rs = ImmutableList.of(
                                        batchResult(ImmutableList.of("s1", "s2"), ImmutableList.of("f1")),
                                        batchResult(ImmutableList.of("s3"), ImmutableList.of()));

        BatchResult<String> combo = IndexedUserDao.combineBatchResults(rs);

        assertThat(combo.getSuccessfulEntities(), contains("s1", "s2", "s3"));
        assertThat(combo.getFailedEntities(), contains("f1"));
    }

    private OfBizUser makeUser(final long directoryId, final String username) {
        final GenericValue userAsGenericValue = mock(GenericValue.class);
        when(userAsGenericValue.getLong(UserEntity.DIRECTORY_ID)).thenReturn(directoryId);
        when(userAsGenericValue.getString(UserEntity.USER_NAME)).thenReturn(username);
        when(userAsGenericValue.getLong(UserEntity.USER_ID)).thenReturn(USER_ID_FOR_TESTING);
        return OfBizUser.from(userAsGenericValue);
    }

    @Test
    public void addAllSmallBatch() {
        Set<UserTemplateWithCredentialAndAttributes> users = ImmutableSet.of(
                new UserTemplateWithCredentialAndAttributes("galah", 1L, new PasswordCredential("password")));
        BatchResult<User> results = userDao.addAll(users);
        assertThat(results.getSuccessfulEntities(), hasSize(1));
        assertThat(results.getFailedEntities(), hasSize(0));

        List<String> successfulNames = results.getSuccessfulEntities().stream()
                .map(Principal::getName).collect(Collectors.toList());
        assertThat(successfulNames, contains("galah"));

        //One batch only
        verify(baseUserDao, times(1)).addAll(anySet());
    }

    @Test
    public void addAllLargeBatch() {
        Set<UserTemplateWithCredentialAndAttributes> users = ImmutableSet.of(
                new UserTemplateWithCredentialAndAttributes("galah", 1L, new PasswordCredential("password")),
                new UserTemplateWithCredentialAndAttributes("cockatoo", 1L, new PasswordCredential("password")),
                new UserTemplateWithCredentialAndAttributes("raven", 1L, new PasswordCredential("password")));
        BatchResult<User> results = userDao.addAll(users);
        assertThat(results.getSuccessfulEntities(), hasSize(3));
        assertThat(results.getFailedEntities(), hasSize(0));

        List<String> successfulNames = results.getSuccessfulEntities().stream()
                                        .map(Principal::getName).collect(Collectors.toList());
        assertThat(successfulNames, contains("galah", "cockatoo", "raven"));

        //Two batches, one with size two and one with size one
        verify(baseUserDao, times(2)).addAll(anySet());
    }

    @Test
    public void removeAllUsersSmallBatch() {
        Set<String> users = ImmutableSet.of("galah");
        BatchResult<String> results = userDao.removeAllUsers(1L, users);
        assertThat(results.getSuccessfulEntities(), contains("galah"));
        assertThat(results.getFailedEntities(), hasSize(0));

        //One batch only
        verify(baseUserDao, times(1)).removeAllUsers(anyLong(), anySet());
    }

    @Test
    public void removeAllUsersLargeBatch() {
        Set<String> users = ImmutableSet.of("galah", "cockatoo", "raven");
        BatchResult<String> results = userDao.removeAllUsers(1L, users);
        assertThat(results.getSuccessfulEntities(), contains("galah", "cockatoo", "raven"));
        assertThat(results.getFailedEntities(), hasSize(0));

        //Two batches, one with size two and one with size one
        verify(baseUserDao, times(2)).removeAllUsers(anyLong(), anySet());
    }

    @Test
    public void clusterMessagesAreSentForAdd() throws Exception {
        UserTemplateWithCredentialAndAttributes user = new UserTemplateWithCredentialAndAttributes("galah", 1L, new PasswordCredential("password"));
        userDao.add(user, user.getCredential());
        verify(clusterMessagingService).sendRemote(eq("jira.UserIndex"), eq("75"));
    }

    @Test
    public void clusterMessagesAreSentForBatchAdd() throws Exception {
        UserTemplateWithCredentialAndAttributes user = new UserTemplateWithCredentialAndAttributes("galah", 1L, new PasswordCredential("password"));
        userDao.addAll(Collections.singleton(user));
        verify(clusterMessagingService).sendRemote(eq("jira.UserIndex"), eq("75"));
    }

    @Test
    public void clusterMessagesAreSentForRemove() throws Exception {
        UserTemplateWithCredentialAndAttributes user = new UserTemplateWithCredentialAndAttributes("galah", 1L, new PasswordCredential("password"));
        userDao.remove(user);
        verify(clusterMessagingService).sendRemote(eq("jira.UserIndex"), eq("75"));
    }

    @Test
    public void clusterMessagesAreSentForBatchRemove() throws Exception {
        userDao.removeAllUsers(1L, Collections.singleton("galah"));
        verify(clusterMessagingService).sendRemote(eq("jira.UserIndex"), eq("75"));
    }

    @Test
    public void clusterMessagesAreSentForUpdate() throws Exception {
        UserTemplateWithCredentialAndAttributes user = new UserTemplateWithCredentialAndAttributes("galah", 1L, new PasswordCredential("password"));
        userDao.update(user);
        verify(clusterMessagingService).sendRemote(eq("jira.UserIndex"), eq("75"));
    }

}

