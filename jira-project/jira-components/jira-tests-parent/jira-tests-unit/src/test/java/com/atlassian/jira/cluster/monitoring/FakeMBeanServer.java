package com.atlassian.jira.cluster.monitoring;

import javax.management.Attribute;
import javax.management.AttributeList;
import javax.management.AttributeNotFoundException;
import javax.management.InstanceAlreadyExistsException;
import javax.management.InstanceNotFoundException;
import javax.management.IntrospectionException;
import javax.management.InvalidAttributeValueException;
import javax.management.ListenerNotFoundException;
import javax.management.MBeanException;
import javax.management.MBeanInfo;
import javax.management.MBeanRegistrationException;
import javax.management.MBeanServer;
import javax.management.NotCompliantMBeanException;
import javax.management.NotificationFilter;
import javax.management.NotificationListener;
import javax.management.ObjectInstance;
import javax.management.ObjectName;
import javax.management.OperationsException;
import javax.management.QueryExp;
import javax.management.ReflectionException;
import javax.management.loading.ClassLoaderRepository;
import java.io.ObjectInputStream;
import java.util.HashSet;
import java.util.Set;

/**
 * A stub implementation of MBeanServer for testing (the actual implementation class is final and a pain to test). This
 * implements only the methods we need for the cluster monitoring tests.
 *
 * @since v7.3
 */
class FakeMBeanServer implements MBeanServer {
    /**
     * Maintain a set of beans we've "registered" so that we can validate when a registration should and shouldn't
     * take place.
     */
    private Set<ObjectName> registeredBeans;

    public FakeMBeanServer() {
        registeredBeans = new HashSet<>();
    }

    @Override
    public ObjectInstance createMBean(String className, ObjectName name) throws ReflectionException, InstanceAlreadyExistsException, MBeanRegistrationException, NotCompliantMBeanException, MBeanRegistrationException {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public ObjectInstance createMBean(String className, ObjectName name, ObjectName loaderName) throws ReflectionException, InstanceAlreadyExistsException, MBeanRegistrationException, NotCompliantMBeanException, InstanceNotFoundException, MBeanRegistrationException {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public ObjectInstance createMBean(String className, ObjectName name, Object[] params, String[] signature) throws ReflectionException, InstanceAlreadyExistsException, MBeanRegistrationException, NotCompliantMBeanException, MBeanRegistrationException {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public ObjectInstance createMBean(String className, ObjectName name, ObjectName loaderName, Object[] params, String[] signature) throws ReflectionException, InstanceAlreadyExistsException, MBeanRegistrationException, NotCompliantMBeanException, InstanceNotFoundException, MBeanRegistrationException {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public ObjectInstance registerMBean(Object object, ObjectName name) throws InstanceAlreadyExistsException, MBeanRegistrationException, NotCompliantMBeanException {
        registeredBeans.add(name);
        return null;
    }

    @Override
    public void unregisterMBean(ObjectName name) throws InstanceNotFoundException, MBeanRegistrationException {
        if (!registeredBeans.remove(name)) {
            throw new InstanceNotFoundException();
        }
    }

    @Override
    public ObjectInstance getObjectInstance(ObjectName name) throws InstanceNotFoundException {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public Set<ObjectInstance> queryMBeans(ObjectName name, QueryExp query) {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public Set<ObjectName> queryNames(ObjectName name, QueryExp query) {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public boolean isRegistered(ObjectName name) {
        return registeredBeans.contains(name);
    }

    @Override
    public Integer getMBeanCount() {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public Object getAttribute(ObjectName name, String attribute) throws MBeanException, AttributeNotFoundException, InstanceNotFoundException, ReflectionException {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public AttributeList getAttributes(ObjectName name, String[] attributes) throws InstanceNotFoundException, ReflectionException {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public void setAttribute(ObjectName name, Attribute attribute) throws InstanceNotFoundException, AttributeNotFoundException, InvalidAttributeValueException, MBeanException, ReflectionException {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public AttributeList setAttributes(ObjectName name, AttributeList attributes) throws InstanceNotFoundException, ReflectionException {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public Object invoke(ObjectName name, String operationName, Object[] params, String[] signature) throws InstanceNotFoundException, MBeanException, ReflectionException {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public String getDefaultDomain() {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public String[] getDomains() {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public void addNotificationListener(ObjectName name, NotificationListener listener, NotificationFilter filter, Object handback) throws InstanceNotFoundException {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public void addNotificationListener(ObjectName name, ObjectName listener, NotificationFilter filter, Object handback) throws InstanceNotFoundException {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public void removeNotificationListener(ObjectName name, ObjectName listener) throws InstanceNotFoundException, ListenerNotFoundException {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public void removeNotificationListener(ObjectName name, ObjectName listener, NotificationFilter filter, Object handback) throws InstanceNotFoundException, ListenerNotFoundException {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public void removeNotificationListener(ObjectName name, NotificationListener listener) throws InstanceNotFoundException, ListenerNotFoundException {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public void removeNotificationListener(ObjectName name, NotificationListener listener, NotificationFilter filter, Object handback) throws InstanceNotFoundException, ListenerNotFoundException {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public MBeanInfo getMBeanInfo(ObjectName name) throws InstanceNotFoundException, IntrospectionException, ReflectionException {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public boolean isInstanceOf(ObjectName name, String className) throws InstanceNotFoundException {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public Object instantiate(String className) throws ReflectionException, MBeanException {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public Object instantiate(String className, ObjectName loaderName) throws ReflectionException, MBeanException, InstanceNotFoundException {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public Object instantiate(String className, Object[] params, String[] signature) throws ReflectionException, MBeanException {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public Object instantiate(String className, ObjectName loaderName, Object[] params, String[] signature) throws ReflectionException, MBeanException, InstanceNotFoundException {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public ObjectInputStream deserialize(ObjectName name, byte[] data) throws InstanceNotFoundException, OperationsException {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public ObjectInputStream deserialize(String className, byte[] data) throws OperationsException, ReflectionException {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public ObjectInputStream deserialize(String className, ObjectName loaderName, byte[] data) throws InstanceNotFoundException, OperationsException, ReflectionException {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public ClassLoader getClassLoaderFor(ObjectName mbeanName) throws InstanceNotFoundException {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public ClassLoader getClassLoader(ObjectName loaderName) throws InstanceNotFoundException {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public ClassLoaderRepository getClassLoaderRepository() {
        throw new UnsupportedOperationException("Not implemented");
    }
}
