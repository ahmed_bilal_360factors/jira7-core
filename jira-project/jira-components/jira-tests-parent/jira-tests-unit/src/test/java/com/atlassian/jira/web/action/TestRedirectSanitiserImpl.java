package com.atlassian.jira.web.action;

import com.atlassian.jira.util.velocity.MockVelocityRequestContextFactory;
import com.atlassian.jira.util.velocity.VelocityRequestContextFactory;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class TestRedirectSanitiserImpl {
    private static final String CANONICAL_BASE_URL = "http://issues.example.com/jira";
    private static final String CANONICAL_BASE_URL_WITH_DIFFERENT_PROTOCOL = "https://issues.example.com/jira";

    private RedirectSanitiser redirectSanitiser;

    @Before
    public void setup() {
        redirectSanitiser = createRedirectSanitiserWithBaseUrl(CANONICAL_BASE_URL);
    }

    @Test
    public void shouldBeAbleToRedirectNullUrl() {
        assertThat(redirectSanitiser.canRedirectTo(null), is(true));
    }

    @Test
    public void shouldNotBeAbleToRedirectToOffsiteUrls() {
        assertThat(redirectSanitiser.canRedirectTo("http://xyz.com/"), is(false));
        assertThat(redirectSanitiser.canRedirectTo("https://xyz.com/"), is(false));
        assertThat(redirectSanitiser.canRedirectTo("//xyz.com/"), is(false));
    }

    @Test
    public void shouldBeAbleToRedirectToAbsoluteUrlOfTheSameDomain() {
        assertThat(redirectSanitiser.canRedirectTo(CANONICAL_BASE_URL + "/foo/bar"), is(true));
    }

    @Test
    public void shouldNotBeAbleToRedirectToAbsoluteUrlOfTheSameDomainIfProtocolDiffers() {
        assertThat(redirectSanitiser.canRedirectTo(CANONICAL_BASE_URL_WITH_DIFFERENT_PROTOCOL + "/foo/bar"), is(false));
    }

    @Test
    public void shouldBeAbleToRedirectToSameDomainUsingRelativeUrl() {
        assertThat(redirectSanitiser.canRedirectTo("/browse/HSP-3"), is(true));
    }

    @Test
    public void shouldBeAbleToRedirectToSameDomainUsingNormalisedRelativeUrl() {
        assertThat(redirectSanitiser.canRedirectTo("Action.jspa?selectedId=4"), is(true));
    }

    @Test
    public void shouldBeAbleToRedirectToSameDomainUsingActionName() {
        assertThat(redirectSanitiser.canRedirectTo("./Action.jspa?selectedId=4"), is(true));
    }

    @Test
    public void shouldBeAbleToRedirectToIssueNavigator() {
        assertThat(redirectSanitiser.canRedirectTo("/issues/?jql=project+=+HSP"), is(true));
    }

    @Test
    public void shouldNotBeAbleToRedirectToAbsoluteUrlsWithNoScheme() {
        assertThat(redirectSanitiser.canRedirectTo("//www.google.com"), is(false));
    }

    @Test
    public void shouldNotBeAbleToRedirectToUrlsWhichContainOneOrMoreBackslashes() {
        assertThat(redirectSanitiser.canRedirectTo("/\\www.google.com"), is(false));
    }

    @Test
    public void shouldBeAbleRedirectToPermissionEditor() {
        assertThat(redirectSanitiser.canRedirectTo("EditPermissions!default.jspa?schemeId=23"), is(true));
    }

    @Test
    public void shouldNotBeAbleToRedirectToUrlsWhichContainColonInPath() {
        assertThat(redirectSanitiser.canRedirectTo("/foo:bar"), is(false));
    }

    @Test
    public void shouldBeAbleToRedirectWhenRequestContextUrlContainsNonLatinChars() {
        //Japanese domain name
        final String baseUrl = "http://\u306F\u3058\u3081\u3088\u3046.\u307F\u3093\u306A";
        final RedirectSanitiser redirectSanitiserWithoutBaseUrl = createRedirectSanitiserWithBaseUrl(baseUrl);
        assertThat(redirectSanitiserWithoutBaseUrl.canRedirectTo(baseUrl + "/foo/bar"), is(true));
    }

    @Test
    public void shouldNotBeAbleToRedirectToAbsoluteUrlWhenRequestContextUrlIsNotProvided() {
        final RedirectSanitiser redirectSanitiserWithoutBaseUrl =
                createRedirectSanitiserWithBaseUrl(null);
        assertThat(redirectSanitiserWithoutBaseUrl.canRedirectTo(CANONICAL_BASE_URL), is(false));
    }

    @Test
    public void shouldBeAbleToRedirectToRelativeUrlWhenRequestContextUrlIsNotProvided() {
        final RedirectSanitiser redirectSanitiserWithoutBaseUrl =
                createRedirectSanitiserWithBaseUrl(null);
        assertThat(redirectSanitiserWithoutBaseUrl.canRedirectTo("/browse/HSP-3"), is(true));
    }

    @Test
    public void shouldBeAbleToRedirectWhenRequestContextUrlIsIpAddress() {
        final String baseUrl = "http://131.103.28.11";
        final RedirectSanitiser redirectSanitiserWithoutBaseUrl = createRedirectSanitiserWithBaseUrl(baseUrl);
        assertThat(redirectSanitiserWithoutBaseUrl.canRedirectTo(baseUrl + "/foo/bar"), is(true));
    }

    @Test
    public void shouldBeAbleToRedirectWhenRequestContextUrlIsIp6Address() {
        final String baseUrl = "http://[2001:4860:4860::8888]";
        final RedirectSanitiser redirectSanitiserWithoutBaseUrl = createRedirectSanitiserWithBaseUrl(baseUrl);
        assertThat(redirectSanitiserWithoutBaseUrl.canRedirectTo(baseUrl + "/foo/bar"), is(true));
    }

    private static RedirectSanitiser createRedirectSanitiserWithBaseUrl(String baseUrl) {
        final VelocityRequestContextFactory mockVelocityRequestContextWithoutBaseUrl =
                new MockVelocityRequestContextFactory(baseUrl);
        return new RedirectSanitiserImpl(mockVelocityRequestContextWithoutBaseUrl);
    }
}
