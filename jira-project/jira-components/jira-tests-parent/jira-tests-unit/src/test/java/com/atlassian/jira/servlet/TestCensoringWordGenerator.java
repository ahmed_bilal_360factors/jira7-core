package com.atlassian.jira.servlet;

import com.octo.captcha.component.word.wordgenerator.WordGenerator;
import org.junit.Before;
import org.junit.Test;

import java.util.Locale;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Tests the behaviour of the {@link CensoringWordGenerator} class.
 *
 * @since v5.1
 */
public class TestCensoringWordGenerator {
    
    private WordGenerator censoredWordGenerator;

    @Before
    public void setUp() throws Exception {
        censoredWordGenerator = mock(WordGenerator.class);
        //Mocks censored word generator to produce three words, the first two of which are offensive
        //The words are not exactly the same as in offensive dictionary, so it checks partial application of censure
        when(censoredWordGenerator.getWord(anyInt())).thenReturn("fucker", "dickly", "noprob");
        when(censoredWordGenerator.getWord(anyInt(), Locale.class.cast(anyObject()))).thenReturn("fucker", "dickly", "noprob");
    }

    /**
     * Tests that the censoring word generator refuses to return offensive words.
     */
    @Test
    public void censoringOfOffensiveWords() {
        assertEquals("noprob", new CensoringWordGenerator(censoredWordGenerator, 5).getWord(6));
    }

    /**
     * Tests that the censoring word generator refuses to return offensive words
     * when a locale is specified.
     */
    @Test
    public void censoringOfOffensiveWordsInLocale() {
        assertEquals("noprob", new CensoringWordGenerator(censoredWordGenerator, 5).getWord(6, Locale.ENGLISH));
    }

    /**
     * Tests that, after 20 attempts to find a non-offensive word, we'll just accept the 21st word regardless.
     */
    @Test
    public void nicenessHasItsLimits() {
        assertEquals("dickly", new CensoringWordGenerator(censoredWordGenerator, 1).getWord(6));
    }

    @Test
    public void nicenessHasItsLimitsInLocale() {
        assertEquals("dickly", new CensoringWordGenerator(censoredWordGenerator, 1).getWord(6, Locale.ENGLISH));
    }
}
