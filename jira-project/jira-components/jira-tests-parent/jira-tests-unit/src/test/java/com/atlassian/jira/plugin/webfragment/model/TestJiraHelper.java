package com.atlassian.jira.plugin.webfragment.model;

import com.atlassian.jira.project.MockProject;
import com.mockobjects.servlet.MockHttpServletRequest;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class TestJiraHelper {

    @Test
    public void testDefaultConstructor() throws Exception {
        final JiraHelper helper = new JiraHelper();

        assertNull(helper.getRequest());
        assertNull(helper.getProjectObject());
    }

    @Test
    public void testConstructorWithRequest() throws Exception {
        MockHttpServletRequest request = new MockHttpServletRequest();
        final JiraHelper helper = new JiraHelper(request);

        assertEquals(request, helper.getRequest());
        assertNull(helper.getProjectObject());
    }

    @Test
    public void testConstructorWithRequestAndProject() throws Exception {
        final MockProject project = new MockProject(new Long(123), "JAVA", "Java Rulez");
        final MockHttpServletRequest request = new MockHttpServletRequest();
        final JiraHelper helper = new JiraHelper(request, project);

        assertEquals(request, helper.getRequest());
        assertEquals(project, helper.getProjectObject());
    }

    @Test
    public void testConstructorWithRequestAndProjectWithGV() throws Exception {
        MockProject project = new MockProject(new Long(123), "JAVA", "Java Rulez");
        MockHttpServletRequest request = new MockHttpServletRequest();
        final JiraHelper helper = new JiraHelper(request, project);

        assertEquals(request, helper.getRequest());
        assertEquals(project, helper.getProjectObject());
    }

    @Test
    public void testConstructorWithRequestAndNullGV() throws Exception {
        final MockHttpServletRequest request = new MockHttpServletRequest();
        final JiraHelper helper = new JiraHelper(request, null);

        assertEquals(request, helper.getRequest());
        assertNull(helper.getProjectObject());
    }

}
