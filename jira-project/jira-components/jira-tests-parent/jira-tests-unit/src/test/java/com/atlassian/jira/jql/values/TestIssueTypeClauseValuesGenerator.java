package com.atlassian.jira.jql.values;

import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.issue.issuetype.MockIssueType;
import com.google.common.collect.ImmutableList;
import org.hamcrest.Matchers;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestIssueTypeClauseValuesGenerator {
    @Rule
    public MockitoRule mockito = MockitoJUnit.rule();
    @Mock
    private ConstantsManager constantsManager;
    @InjectMocks
    private IssueTypeClauseValuesGenerator valuesGenerator;

    @Test
    public void testGetAllConstants() throws Exception {
        final MockIssueType type1 = new MockIssueType("1", "Aa it");
        final MockIssueType type2 = new MockIssueType("2", "A it");
        final MockIssueType type3 = new MockIssueType("3", "B it");
        final MockIssueType type4 = new MockIssueType("4", "C it");
        final ImmutableList<IssueType> allIssueTypes = ImmutableList.of(type4, type3, type2, type1);

        when(constantsManager.getAllIssueTypeObjects()).thenReturn(allIssueTypes);

        assertThat(valuesGenerator.getAllConstants(), Matchers.equalTo(allIssueTypes));
    }

}
