package com.atlassian.jira.util.system;

import com.atlassian.jdk.utilities.runtimeinformation.MemoryInformation;
import com.atlassian.jdk.utilities.runtimeinformation.RuntimeInformation;
import com.atlassian.jira.junit.rules.InitMockitoMocks;
import com.atlassian.jira.util.system.JiraRuntimeInformationFactory.SanitisedRuntimeInformation;
import com.google.common.collect.ImmutableList;
import org.hamcrest.Matchers;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;

import java.util.List;

import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class JiraRuntimeInformationFactoryTest {
    @Rule
    public InitMockitoMocks mockitoMocks = new InitMockitoMocks(this);

    @Mock
    private RuntimeInformation information;

    @Test
    public void getSanitisedJvmInputArgumentsSanitisesRecoveryPassword() {
        //given
        final List<String> arguments = ImmutableList.of("-Datlassian.recovery.password=recovery", "example", "other");
        final SanitisedRuntimeInformation runtimeInformation = new SanitisedRuntimeInformation(information, () -> arguments);

        //when
        final String str = runtimeInformation.getJvmInputArguments();

        //then
        assertThat(str, Matchers.equalTo("-Datlassian.recovery.password=**** example other"));
    }

    @Test
    public void getSanitisedTotalHeapMemoryDelegates() {
        //given
        final long total = 10_101_276L;
        when(information.getTotalHeapMemory()).thenReturn(total);
        final SanitisedRuntimeInformation runtimeInformation = new SanitisedRuntimeInformation(information, ImmutableList::of);

        //when
        final long totalHeapMemory = runtimeInformation.getTotalHeapMemory();

        //then
        assertThat(totalHeapMemory, Matchers.equalTo(total));
        verify(information).getTotalHeapMemory();
    }

    @Test
    public void getSanitisedTotalHeapMemoryUsedDelegates() {
        //given
        final long total = 4_637_984_534L;
        when(information.getTotalHeapMemoryUsed()).thenReturn(total);
        final SanitisedRuntimeInformation runtimeInformation = new SanitisedRuntimeInformation(information, ImmutableList::of);

        //when
        final long totalHeapMemory = runtimeInformation.getTotalHeapMemoryUsed();

        //then
        assertThat(totalHeapMemory, Matchers.equalTo(total));
        verify(information).getTotalHeapMemoryUsed();
    }

    @Test
    public void getSanitisedGetTotalPermGenMemory() {
        //given
        final long total = 56L;
        when(information.getTotalPermGenMemory()).thenReturn(total);
        final SanitisedRuntimeInformation runtimeInformation = new SanitisedRuntimeInformation(information, ImmutableList::of);

        //when
        final long totalPerm = runtimeInformation.getTotalPermGenMemory();

        //then
        assertThat(totalPerm, Matchers.equalTo(total));
        verify(information).getTotalPermGenMemory();
    }

    @Test
    public void getSanitisedGetTotalPermGenMemoryUsedDelegates() {
        //given
        final long total = 57L;
        when(information.getTotalPermGenMemoryUsed()).thenReturn(total);
        final SanitisedRuntimeInformation runtimeInformation = new SanitisedRuntimeInformation(information, ImmutableList::of);

        //when
        final long totalPerm = runtimeInformation.getTotalPermGenMemoryUsed();

        //then
        assertThat(totalPerm, Matchers.equalTo(total));
        verify(information).getTotalPermGenMemoryUsed();
    }

    @Test
    public void getSanitisedGetTotalNonHeapMemoryDelegates() {
        //given
        final long total = 67L;
        when(information.getTotalNonHeapMemory()).thenReturn(total);
        final SanitisedRuntimeInformation runtimeInformation = new SanitisedRuntimeInformation(information, ImmutableList::of);

        //when
        final long totalPerm = runtimeInformation.getTotalNonHeapMemory();

        //then
        assertThat(totalPerm, Matchers.equalTo(total));
        verify(information).getTotalNonHeapMemory();
    }

    @Test
    public void getSanitisedGetTotalNonHeapMemoryUsedDelegates() {
        //given
        final long total = 89L;
        when(information.getTotalNonHeapMemoryUsed()).thenReturn(total);
        final SanitisedRuntimeInformation runtimeInformation = new SanitisedRuntimeInformation(information, ImmutableList::of);

        //when
        final long totalPerm = runtimeInformation.getTotalNonHeapMemoryUsed();

        //then
        assertThat(totalPerm, Matchers.equalTo(total));
        verify(information).getTotalNonHeapMemoryUsed();
    }

    @Test
    public void getSanitisedMemoryInformationDelegates() {
        //given
        final MockMemoryInformation mockInfo = new MockMemoryInformation();
        when(information.getMemoryPoolInformation()).thenReturn(ImmutableList.of(mockInfo));
        final SanitisedRuntimeInformation runtimeInformation = new SanitisedRuntimeInformation(information, ImmutableList::of);

        //when
        final List<MemoryInformation> totalPerm = runtimeInformation.getMemoryPoolInformation();

        //then
        assertThat(totalPerm, Matchers.contains(mockInfo));
        verify(information).getMemoryPoolInformation();
    }

    @Test
    public void getRuntimeInformationIsSanitised() {
        final RuntimeInformation runtimeInformation = JiraRuntimeInformationFactory.getRuntimeInformation();
        assertThat(runtimeInformation, Matchers.instanceOf(SanitisedRuntimeInformation.class));
    }

    private static class MockMemoryInformation implements MemoryInformation {
        @Override
        public String getName() {
            return "Mock";
        }

        @Override
        public long getTotal() {
            return 100 * 1024 * 104;
        }

        @Override
        public long getUsed() {
            return 50 * 1024 * 1024;
        }

        @Override
        public long getFree() {
            return getTotal() - getUsed();
        }
    }
}