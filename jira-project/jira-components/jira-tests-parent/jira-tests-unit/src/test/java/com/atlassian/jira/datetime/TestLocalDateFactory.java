package com.atlassian.jira.datetime;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.Date;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

public class TestLocalDateFactory {
    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Test
    public void convertingDatesInCommonEraShouldSucceed() {
        assertThat(LocalDateFactory.from(new Date(0)), equalTo(new LocalDate(1970, 1, 1)));
        assertThat(LocalDateFactory.from(new Date(1275196032735L)), equalTo(new LocalDate(2010, 5, 30)));
        assertThat(LocalDateFactory.from(new Date(-11991058346L)), equalTo(new LocalDate(1969, 8, 15)));
        assertThat(LocalDateFactory.from(new Date(-62135769600000L)), equalTo(new LocalDate(1, 1, 1)));
    }

    @Test
    public void convertingDatesBCShouldThrowIllegalArgumentException() {
        exception.expect(IllegalArgumentException.class);
        exception.expectMessage("LocalDate only handles the Common Era - no BC dates are allowed.");
        LocalDateFactory.from(new Date(-62135856000000L)); // 31/12/0001 BC
    }
}
