package com.atlassian.jira.action.version;

import com.atlassian.jira.bc.project.version.VersionBuilderImpl;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.database.MockDbConnectionManager;
import com.atlassian.jira.entity.EntityEngineImpl;
import com.atlassian.jira.entity.VersionFactory;
import com.atlassian.jira.mock.component.MockComponentWorker;
import com.atlassian.jira.mock.ofbiz.MockGenericValue;
import com.atlassian.jira.mock.ofbiz.MockOfBizDelegator;
import com.atlassian.jira.ofbiz.FieldMap;
import com.atlassian.jira.project.version.OfBizVersionStore;
import com.atlassian.jira.project.version.Version;
import com.google.common.collect.ImmutableList;
import org.hamcrest.collection.IsIterableWithSize;
import org.junit.Before;
import org.junit.Test;
import org.ofbiz.core.entity.GenericValue;

import static com.google.common.collect.ImmutableList.of;
import static com.google.common.collect.Iterables.get;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

@SuppressWarnings("deprecation")
public class TestOfBizVersionStore {
    MockDbConnectionManager dbConnectionManager;
    VersionFactory versionFactory = new VersionFactory();
    GenericValue version1Gv = new MockGenericValue("Version", FieldMap.build("name", "version1", "id", (long) 1, "sequence", (long) 0));
    Version version1 = versionFactory.build(version1Gv);
    GenericValue version2Gv = new MockGenericValue("Version", FieldMap.build("name", "version2", "id", (long) 2, "sequence", (long) 1));
    Version version2 = versionFactory.build(version2Gv);

    @Before
    public void setUp() {
        dbConnectionManager = new MockDbConnectionManager();
    }

    @Test
    public void testGetAllVersionsReturnsCorrectDBValues() {
        MockOfBizDelegator delegator = new MockOfBizDelegator(
                of(version1Gv, version2Gv),
                of(version1Gv, version2Gv));
        OfBizVersionStore versionStore = new OfBizVersionStore(new EntityEngineImpl(delegator), dbConnectionManager);

        Iterable<Version> versions = versionStore.getAllVersions();
        assertThat(versions, IsIterableWithSize.<Version>iterableWithSize(2));
        assertEquals(version1, get(versions, 0));
        assertEquals(version2, get(versions, 1));
        delegator.verify();
    }

    @Test
    public void testGetAllVersionsReturnsEmptyListWhenNoVersions() {
        MockOfBizDelegator delegator = new MockOfBizDelegator(ImmutableList.<GenericValue>of(), ImmutableList.<GenericValue>of());
        OfBizVersionStore versionStore = new OfBizVersionStore(new EntityEngineImpl(delegator), dbConnectionManager);

        Iterable<Version> versions = versionStore.getAllVersions();
        assertThat(versions, IsIterableWithSize.<Version>iterableWithSize(0));
        delegator.verify();
    }

    @Test
    public void testCreateVersion() {
        final MockGenericValue versionGv = new MockGenericValue("Version", FieldMap.build("name", "version1"));
        MockOfBizDelegator delegator = new MockOfBizDelegator(ImmutableList.<GenericValue>of(), of(versionGv));
        OfBizVersionStore versionStore = new OfBizVersionStore(new EntityEngineImpl(delegator), dbConnectionManager);

        Version version = versionStore.createVersion(new VersionBuilderImpl().name("version1").build());
        assertEquals("version1", version.getName());
        delegator.verify();
    }

    @Test
    public void testGetVersionReturnsCorrectDatabaseVersion() {
        MockOfBizDelegator delegator = new MockOfBizDelegator(
                of(version1Gv, version2Gv),
                of(version1Gv, version2Gv));
        OfBizVersionStore versionStore = new OfBizVersionStore(new EntityEngineImpl(delegator), dbConnectionManager);

        Version version = versionStore.getVersion(new Long(1));
        assertEquals(version1, version);
    }

    @Test
    public void testGetVersionReturnsNullIfNoVersionFound() {
        MockOfBizDelegator delegator = new MockOfBizDelegator(ImmutableList.<GenericValue>of(), ImmutableList.<GenericValue>of());
        OfBizVersionStore versionStore = new OfBizVersionStore(new EntityEngineImpl(delegator), dbConnectionManager);

        Version version = versionStore.getVersion(new Long(1));
        assertEquals(null, version);
    }

    @Test
    public void testDeleteVersionRemovesVersionFromDatabase() {
        MockOfBizDelegator delegator = new MockOfBizDelegator(
                of(version1Gv, version2Gv),
                of(version2Gv));
        OfBizVersionStore versionStore = new OfBizVersionStore(new EntityEngineImpl(delegator), dbConnectionManager);

        versionStore.deleteVersion(version1);
        delegator.verify();
    }

    @Test
    public void testDeleteAllVersions() {
        ComponentAccessor.initialiseWorker(new MockComponentWorker());
        dbConnectionManager.setUpdateResults(
                "delete from projectversion\n" +
                        "where projectversion.project = 1", 0);

        final OfBizVersionStore versionStore = new OfBizVersionStore(null, dbConnectionManager);

        versionStore.deleteAllVersions(1L);

        dbConnectionManager.assertAllExpectedStatementsWereRun();

    }
}
