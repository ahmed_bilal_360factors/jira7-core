package com.atlassian.jira.web.action.admin.plugins;

import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.plugin.customfield.CustomFieldSearcherModuleDescriptor;
import com.atlassian.jira.plugin.customfield.CustomFieldTypeModuleDescriptor;
import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.mockito.Mock;

import java.util.Collections;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestPluginReindexHelperImpl {
    @Rule
    public final RuleChain mockitoMocks = MockitoMocksInContainer.forTest(this);

    @Mock
    private PluginAccessor pluginAccessor;

    @Test
    public void testDoesEnablingPluginModuleRequireMessageNull() throws Exception {
        final String key = "badKey";
        when(pluginAccessor.getEnabledPluginModule(key)).thenReturn(null);

        final PluginReindexHelperImpl helper = new PluginReindexHelperImpl(pluginAccessor);

        final boolean result = helper.doesEnablingPluginModuleRequireMessage(key);
        assertFalse(result);
    }

    @Test
    public void testDoesEnablingPluginModuleRequireMessageNotInterestingType() throws Exception {
        final ModuleDescriptor descriptor = mock(ModuleDescriptor.class);
        final String key = "notInteresting";
        when(pluginAccessor.getEnabledPluginModule(key)).thenReturn(descriptor);

        final PluginReindexHelperImpl helper = new PluginReindexHelperImpl(pluginAccessor);

        final boolean result = helper.doesEnablingPluginModuleRequireMessage(key);
        assertFalse(result);
    }

    @Test
    public void testDoesEnablingPluginModuleRequireMessageCustomFieldType() throws Exception {
        final ModuleDescriptor descriptor = mock(CustomFieldTypeModuleDescriptor.class);
        final String key = "customfieldtype";
        when(pluginAccessor.getEnabledPluginModule(key)).thenReturn(descriptor);

        final PluginReindexHelperImpl helper = new PluginReindexHelperImpl(pluginAccessor);

        final boolean result = helper.doesEnablingPluginModuleRequireMessage(key);
        assertTrue(result);
    }

    @Test
    public void testDoesEnablingPluginModuleRequireMessageCustomFieldSearcher() throws Exception {
        final ModuleDescriptor descriptor = mock(CustomFieldTypeModuleDescriptor.class);
        final String key = "customfieldsearcher";
        when(pluginAccessor.getEnabledPluginModule(key)).thenReturn(descriptor);

        final PluginReindexHelperImpl helper = new PluginReindexHelperImpl(pluginAccessor);

        final boolean result = helper.doesEnablingPluginModuleRequireMessage(key);
        assertTrue(result);
    }

    @Test
    public void testDoesEnablingPluginRequireMessageNull() throws Exception {
        final String key = "badKey";
        when(pluginAccessor.getEnabledPlugin(key)).thenReturn(null);

        final PluginReindexHelperImpl helper = new PluginReindexHelperImpl(pluginAccessor);

        final boolean result = helper.doesEnablingPluginRequireMessage(key);
        assertFalse(result);
    }

    @Test
    public void testDoesEnablingPluginRequireMessageEmptyDescriptors() throws Exception {
        final Plugin plugin = mock(Plugin.class);
        final String key = "key";
        when(pluginAccessor.getEnabledPlugin(key)).thenReturn(plugin);
        when(plugin.getModuleDescriptors()).thenReturn(Collections.<ModuleDescriptor<?>>emptyList());

        final PluginReindexHelperImpl helper = new PluginReindexHelperImpl(pluginAccessor);

        final boolean result = helper.doesEnablingPluginRequireMessage(key);
        assertFalse(result);
    }

    @Test
    public void testDoesEnablingPluginRequireMessageOneNotInterestingDescriptor() throws Exception {
        final ModuleDescriptor descriptor = mock(ModuleDescriptor.class);
        final Plugin plugin = mock(Plugin.class);
        final String key = "key";
        when(pluginAccessor.getEnabledPlugin(key)).thenReturn(plugin);
        when(plugin.getModuleDescriptors()).thenReturn(Collections.<ModuleDescriptor<?>>singleton(descriptor));

        final PluginReindexHelperImpl helper = new PluginReindexHelperImpl(pluginAccessor);

        final boolean result = helper.doesEnablingPluginRequireMessage(key);
        assertFalse(result);
    }

    @Test
    public void testDoesEnablingPluginRequireMessageOneInterestingDescriptor() throws Exception {
        final ModuleDescriptor descriptor = mock(CustomFieldTypeModuleDescriptor.class);
        final Plugin plugin = mock(Plugin.class);
        final String key = "key";
        when(pluginAccessor.getEnabledPlugin(key)).thenReturn(plugin);
        when(plugin.getModuleDescriptors()).thenReturn(Collections.<ModuleDescriptor<?>>singleton(descriptor));

        final PluginReindexHelperImpl helper = new PluginReindexHelperImpl(pluginAccessor);

        final boolean result = helper.doesEnablingPluginRequireMessage(key);
        assertTrue(result);
    }

    @Test
    public void testDoesEnablingPluginRequireMessageOtherInterestingDescriptor() throws Exception {
        final ModuleDescriptor descriptor = mock(CustomFieldSearcherModuleDescriptor.class);
        final Plugin plugin = mock(Plugin.class);
        final String key = "key";
        when(pluginAccessor.getEnabledPlugin(key)).thenReturn(plugin);
        when(plugin.getModuleDescriptors()).thenReturn(Collections.<ModuleDescriptor<?>>singleton(descriptor));

        final PluginReindexHelperImpl helper = new PluginReindexHelperImpl(pluginAccessor);

        final boolean result = helper.doesEnablingPluginRequireMessage(key);
        assertTrue(result);
    }
}
