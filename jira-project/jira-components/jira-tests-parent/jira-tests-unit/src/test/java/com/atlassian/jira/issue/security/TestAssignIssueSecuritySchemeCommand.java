package com.atlassian.jira.issue.security;

import com.atlassian.jira.database.MockQueryDslAccessor;
import com.atlassian.jira.database.ResultRow;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.index.IssueIndexingService;
import com.atlassian.jira.issue.util.IssueIdsIssueIterable;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.mock.i18n.MockI18nHelper;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.scheme.Scheme;
import com.atlassian.jira.task.TaskProgressSink;
import com.atlassian.jira.task.context.Context;
import com.atlassian.jira.util.I18nHelper;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.slf4j.Logger;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestAssignIssueSecuritySchemeCommand {
    @Rule
    public final RuleChain mockitoMocks = MockitoMocksInContainer.forTest(this);

    @Mock
    @AvailableInContainer
    private IssueSecurityLevelManager issueSecurityLevelManager;
    @Mock
    private IssueSecuritySchemeManager issueSecuritySchemeManager;

    private MockQueryDslAccessor queryDslAccessor = new MockQueryDslAccessor();
    @Mock
    private IssueIndexingService issueIndexingService;
    @Mock
    private IssueManager issueManager;
    @Mock
    private Logger log;
    private I18nHelper i18NHelper = new MockI18nHelper();
    private TaskProgressSink sink = TaskProgressSink.NULL_SINK;

    @Test
    public void testUpdateSchemeAndIssues() throws Exception {
        Project project = new MockProject(1, "HSP");
        Map<Long, Long> mapping = ImmutableMap.of(-1L, -1L, 1L, 11L, 2L, 12L);

        IssueSecurityLevel securityLevel1 = mock(IssueSecurityLevel.class);
        when(securityLevel1.getId()).thenReturn(1L);
        when(securityLevel1.getName()).thenReturn("Defcon 1");
        IssueSecurityLevel securityLevel2 = mock(IssueSecurityLevel.class);
        when(securityLevel2.getId()).thenReturn(2L);
        when(securityLevel2.getName()).thenReturn("Defcon 2");
        IssueSecurityLevel securityLevel11 = mock(IssueSecurityLevel.class);
        when(securityLevel11.getId()).thenReturn(11L);
        when(securityLevel11.getName()).thenReturn("Defcon 11");
        IssueSecurityLevel securityLevel12 = mock(IssueSecurityLevel.class);
        when(securityLevel12.getId()).thenReturn(12L);
        when(securityLevel12.getName()).thenReturn("Defcon 12");
        when(issueSecurityLevelManager.getSecurityLevel(1)).thenReturn(securityLevel1);
        when(issueSecurityLevelManager.getSecurityLevel(2)).thenReturn(securityLevel2);
        when(issueSecurityLevelManager.getSecurityLevel(11)).thenReturn(securityLevel11);
        when(issueSecurityLevelManager.getSecurityLevel(12)).thenReturn(securityLevel12);

        Scheme mockScheme = mock(Scheme.class);
        when(issueSecuritySchemeManager.getSchemeObject(10012L)).thenReturn(mockScheme);

        queryDslAccessor.setQueryEphemeralResults(
                "select ISSUE.id\n"
                        + "from jiraissue ISSUE\n"
                        + "where ISSUE.security = 1 and ISSUE.project = 1\n"
                        + "limit 100",
                ImmutableList.of(new ResultRow(1L), new ResultRow(2L), new ResultRow(3L)),
                ImmutableList.of());

        queryDslAccessor.setUpdateResults(
                "update jiraissue\n"
                        + "set security = 11\n"
                        + "where jiraissue.id in (1, 2, 3)", 3);

        queryDslAccessor.setQueryEphemeralResults(
                "select ISSUE.id\n"
                        + "from jiraissue ISSUE\n"
                        + "where ISSUE.security = 2 and ISSUE.project = 1\n"
                        + "limit 100",
                ImmutableList.of(new ResultRow(11L), new ResultRow(12L), new ResultRow(13L), new ResultRow(14L)),
                ImmutableList.of());

        queryDslAccessor.setUpdateResults(
                "update jiraissue\n"
                        + "set security = 12\n"
                        + "where jiraissue.id in (11, 12, 13, 14)", 4);

        AssignIssueSecuritySchemeCommand command = new AssignIssueSecuritySchemeCommand(project, 10012L, mapping, queryDslAccessor,
                issueSecuritySchemeManager, issueIndexingService, issueManager, log, i18NHelper);

        command.setTaskProgressSink(sink);
        command.call();

        queryDslAccessor.assertAllExpectedStatementsWereRun();

        verify(issueSecurityLevelManager).getIssueCount(1L, 1L);
        verify(issueSecurityLevelManager).getIssueCount(2L, 1L);

        ArgumentCaptor<IssueIdsIssueIterable> issueCapture = ArgumentCaptor.forClass(IssueIdsIssueIterable.class);
        verify(issueIndexingService, times(2)).reIndexIssues(issueCapture.capture(), any(Context.class));

        List<IssueIdsIssueIterable> issues = issueCapture.getAllValues();
        Iterator<IssueIdsIssueIterable> iterator = issues.iterator();
        IssueIdsIssueIterable issueBatch1 = iterator.next();
        assertThat(issueBatch1.size(), is(3));
        IssueIdsIssueIterable issueBatch2 = iterator.next();
        assertThat(issueBatch2.size(), is(4));

        verify(issueSecuritySchemeManager).removeSchemesFromProject(project);
        verify(issueSecuritySchemeManager).addSchemeToProject(project, mockScheme);

    }

    @Test
    public void testSetSchemeToNull() throws Exception {
        Project project = new MockProject(1, "HSP");
        Map<Long, Long> mapping = ImmutableMap.of(-1L, -1L, 1L, -1L, 2L, -1L);

        IssueSecurityLevel securityLevel1 = mock(IssueSecurityLevel.class);
        when(securityLevel1.getId()).thenReturn(1L);
        when(securityLevel1.getName()).thenReturn("Defcon 1");
        IssueSecurityLevel securityLevel2 = mock(IssueSecurityLevel.class);
        when(securityLevel2.getId()).thenReturn(2L);
        when(securityLevel2.getName()).thenReturn("Defcon 2");
        when(issueSecurityLevelManager.getSecurityLevel(1)).thenReturn(securityLevel1);
        when(issueSecurityLevelManager.getSecurityLevel(2)).thenReturn(securityLevel2);

        Scheme mockScheme = mock(Scheme.class);

        queryDslAccessor.setQueryEphemeralResults(
                "select ISSUE.id\n"
                        + "from jiraissue ISSUE\n"
                        + "where ISSUE.security = 1 and ISSUE.project = 1\n"
                        + "limit 100",
                ImmutableList.of(new ResultRow(1L), new ResultRow(2L), new ResultRow(3L)),
                ImmutableList.of());

        queryDslAccessor.setUpdateResults(
                "update jiraissue\n"
                        + "set security = NULL\n"
                        + "where jiraissue.id in (1, 2, 3)", 3);

        queryDslAccessor.setQueryEphemeralResults(
                "select ISSUE.id\n"
                        + "from jiraissue ISSUE\n"
                        + "where ISSUE.security = 2 and ISSUE.project = 1\n"
                        + "limit 100",
                ImmutableList.of(new ResultRow(11L), new ResultRow(12L), new ResultRow(13L), new ResultRow(14L)),
                ImmutableList.of());

        queryDslAccessor.setUpdateResults(
                "update jiraissue\n"
                        + "set security = NULL\n"
                        + "where jiraissue.id in (11, 12, 13, 14)", 4);

        AssignIssueSecuritySchemeCommand command = new AssignIssueSecuritySchemeCommand(project, null, mapping, queryDslAccessor,
                issueSecuritySchemeManager, issueIndexingService, issueManager, log, i18NHelper);

        command.setTaskProgressSink(sink);
        command.call();

        queryDslAccessor.assertAllExpectedStatementsWereRun();

        verify(issueSecurityLevelManager).getIssueCount(1L, 1L);
        verify(issueSecurityLevelManager).getIssueCount(2L, 1L);

        ArgumentCaptor<IssueIdsIssueIterable> issueCapture = ArgumentCaptor.forClass(IssueIdsIssueIterable.class);
        verify(issueIndexingService, times(2)).reIndexIssues(issueCapture.capture(), any(Context.class));

        List<IssueIdsIssueIterable> issues = issueCapture.getAllValues();
        Iterator<IssueIdsIssueIterable> iterator = issues.iterator();
        IssueIdsIssueIterable issueBatch1 = iterator.next();
        assertThat(issueBatch1.size(), is(3));
        IssueIdsIssueIterable issueBatch2 = iterator.next();
        assertThat(issueBatch2.size(), is(4));

        verify(issueSecuritySchemeManager).removeSchemesFromProject(project);
        verify(issueSecuritySchemeManager, never()).addSchemeToProject(project, mockScheme);
    }

    @Test
    public void testSetNewScheme() throws Exception {
        Project project = new MockProject(1, "HSP");
        Map<Long, Long> mapping = ImmutableMap.of(-1L, 11L);

        IssueSecurityLevel securityLevel11 = mock(IssueSecurityLevel.class);
        when(securityLevel11.getId()).thenReturn(11L);
        when(securityLevel11.getName()).thenReturn("Defcon 11");
        when(issueSecurityLevelManager.getSecurityLevel(11)).thenReturn(securityLevel11);

        Scheme mockScheme = mock(Scheme.class);
        when(issueSecuritySchemeManager.getSchemeObject(10012L)).thenReturn(mockScheme);

        queryDslAccessor.setQueryEphemeralResults(
                "select ISSUE.id\n"
                        + "from jiraissue ISSUE\n"
                        + "where ISSUE.security is null and ISSUE.project = 1\n"
                        + "limit 100",
                ImmutableList.of(new ResultRow(1L), new ResultRow(2L), new ResultRow(3L)),
                ImmutableList.of());

        queryDslAccessor.setUpdateResults(
                "update jiraissue\n"
                        + "set security = 11\n"
                        + "where jiraissue.id in (1, 2, 3)", 3);

        AssignIssueSecuritySchemeCommand command = new AssignIssueSecuritySchemeCommand(project, 10012L, mapping, queryDslAccessor,
                issueSecuritySchemeManager, issueIndexingService, issueManager, log, i18NHelper);

        command.setTaskProgressSink(sink);
        command.call();

        queryDslAccessor.assertAllExpectedStatementsWereRun();

        verify(issueSecurityLevelManager).getIssueCount(null, 1L);

        ArgumentCaptor<IssueIdsIssueIterable> issueCapture = ArgumentCaptor.forClass(IssueIdsIssueIterable.class);
        verify(issueIndexingService, times(1)).reIndexIssues(issueCapture.capture(), any(Context.class));

        List<IssueIdsIssueIterable> issues = issueCapture.getAllValues();
        Iterator<IssueIdsIssueIterable> iterator = issues.iterator();
        IssueIdsIssueIterable issueBatch1 = iterator.next();
        assertThat(issueBatch1.size(), is(3));

        verify(issueSecuritySchemeManager).removeSchemesFromProject(project);
        verify(issueSecuritySchemeManager).addSchemeToProject(project, mockScheme);
    }

    @Test
    public void testCallNoIssueWithSecurityLevelsInOldScheme() throws Exception {
        Project project = new MockProject(1, "HSP");
        Map<Long, Long> mapping = ImmutableMap.of(1L, 11L);

        IssueSecurityLevel securityLevel1 = mock(IssueSecurityLevel.class);
        when(securityLevel1.getId()).thenReturn(1L);
        when(securityLevel1.getName()).thenReturn("Defcon 1");
        IssueSecurityLevel securityLevel11 = mock(IssueSecurityLevel.class);
        when(securityLevel11.getId()).thenReturn(11L);
        when(securityLevel11.getName()).thenReturn("Defcon 11");
        when(issueSecurityLevelManager.getSecurityLevel(1)).thenReturn(securityLevel1);
        when(issueSecurityLevelManager.getSecurityLevel(11)).thenReturn(securityLevel11);
        when(issueSecurityLevelManager.getIssueCount(1L, 1L)).thenReturn(0L);
        when(issueSecurityLevelManager.getIssueCount(11L, 1L)).thenReturn(0L);

        Scheme mockScheme = mock(Scheme.class);
        when(issueSecuritySchemeManager.getSchemeObject(10012L)).thenReturn(mockScheme);

        queryDslAccessor.setQueryResults(
                "select ISSUE.id\n"
                        + "from jiraissue ISSUE\n"
                        + "where ISSUE.security = 1 and ISSUE.project = 1\n"
                        + "limit 100",
                ImmutableList.of());

        AssignIssueSecuritySchemeCommand command = new AssignIssueSecuritySchemeCommand(project, 10012L, mapping, queryDslAccessor,
                issueSecuritySchemeManager, issueIndexingService, issueManager, log, i18NHelper);

        command.setTaskProgressSink(sink);
        command.call();

        verify(issueSecurityLevelManager).getIssueCount(1L, 1L);

        ArgumentCaptor<IssueIdsIssueIterable> issueCapture = ArgumentCaptor.forClass(IssueIdsIssueIterable.class);
        verify(issueIndexingService, never()).reIndexIssues(issueCapture.capture(), any(Context.class));

        verify(issueSecuritySchemeManager).removeSchemesFromProject(project);
        verify(issueSecuritySchemeManager).addSchemeToProject(project, mockScheme);

        queryDslAccessor.assertAllExpectedStatementsWereRun();
    }

    @Test
    public void testSetSchemeToNullWithIncompleteMapping() throws Exception {
        // When a scheme is removed via the UI an empty oldToNew mapping is sent.
        // Check that we don't orphan issues by not clearing their security levels
        final Project project = new MockProject(1, "HSP");
        final Map<Long, Long> mapping = ImmutableMap.of();

        final IssueSecurityLevel securityLevel1 = mock(IssueSecurityLevel.class);
        when(securityLevel1.getId()).thenReturn(1L);
        when(securityLevel1.getName()).thenReturn("Defcon 1");
        when(issueSecurityLevelManager.getSecurityLevel(1)).thenReturn(securityLevel1);

        final Scheme mockScheme = mock(Scheme.class);
        when(mockScheme.getId()).thenReturn(1L);

        when(issueSecuritySchemeManager.getSchemeIdFor(project)).thenReturn(1L);
        when(issueSecurityLevelManager.getIssueSecurityLevels(1L)).thenReturn(ImmutableList.of(securityLevel1));

        queryDslAccessor.setQueryEphemeralResults(
                "select ISSUE.id\n"
                        + "from jiraissue ISSUE\n"
                        + "where ISSUE.security = 1 and ISSUE.project = 1\n"
                        + "limit 100",
                ImmutableList.of(new ResultRow(1L), new ResultRow(2L), new ResultRow(3L)),
                ImmutableList.of());

        queryDslAccessor.setUpdateResults(
                "update jiraissue\n"
                        + "set security = NULL\n"
                        + "where jiraissue.id in (1, 2, 3)", 3);

        final AssignIssueSecuritySchemeCommand command = new AssignIssueSecuritySchemeCommand(project, 1L, mapping, queryDslAccessor,
                issueSecuritySchemeManager, issueIndexingService, issueManager, log, i18NHelper);

        command.setTaskProgressSink(sink);
        command.call();

        queryDslAccessor.assertAllExpectedStatementsWereRun();

        verify(issueSecurityLevelManager).getIssueCount(1L, 1L);

        final ArgumentCaptor<IssueIdsIssueIterable> issueCapture = ArgumentCaptor.forClass(IssueIdsIssueIterable.class);
        verify(issueIndexingService, times(1)).reIndexIssues(issueCapture.capture(), any(Context.class));

        final List<IssueIdsIssueIterable> issues = issueCapture.getAllValues();
        final Iterator<IssueIdsIssueIterable> iterator = issues.iterator();
        final IssueIdsIssueIterable issueBatch1 = iterator.next();
        assertThat(issueBatch1.size(), is(3));

        verify(issueSecuritySchemeManager).removeSchemesFromProject(project);
        verify(issueSecuritySchemeManager, never()).addSchemeToProject(project, mockScheme);
    }
}
