package com.atlassian.jira.imports.project;

import com.atlassian.jira.association.NodeAssociationStore;
import com.atlassian.jira.association.UserAssociationStore;
import com.atlassian.jira.bc.project.component.ProjectComponent;
import com.atlassian.jira.bc.project.component.ProjectComponentImpl;
import com.atlassian.jira.bc.project.component.ProjectComponentManager;
import com.atlassian.jira.bc.user.MockUserService;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.exception.DataAccessException;
import com.atlassian.jira.exception.PermissionException;
import com.atlassian.jira.external.ExternalException;
import com.atlassian.jira.external.ExternalUtils;
import com.atlassian.jira.external.beans.ExternalAttachment;
import com.atlassian.jira.external.beans.ExternalComponent;
import com.atlassian.jira.external.beans.ExternalIssue;
import com.atlassian.jira.external.beans.ExternalIssueImpl;
import com.atlassian.jira.external.beans.ExternalNodeAssociation;
import com.atlassian.jira.external.beans.ExternalProject;
import com.atlassian.jira.external.beans.ExternalUser;
import com.atlassian.jira.external.beans.ExternalVersion;
import com.atlassian.jira.external.beans.ExternalVoter;
import com.atlassian.jira.external.beans.ExternalWatcher;
import com.atlassian.jira.imports.project.core.BackupProject;
import com.atlassian.jira.imports.project.core.BackupProjectImpl;
import com.atlassian.jira.imports.project.core.EntityRepresentation;
import com.atlassian.jira.imports.project.core.EntityRepresentationImpl;
import com.atlassian.jira.imports.project.mapper.ProjectImportMapper;
import com.atlassian.jira.imports.project.mapper.ProjectImportMapperImpl;
import com.atlassian.jira.imports.project.mapper.UserMapper;
import com.atlassian.jira.imports.project.taskprogress.TaskProgressInterval;
import com.atlassian.jira.issue.AttachmentManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueFactory;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.attachment.Attachment;
import com.atlassian.jira.issue.changehistory.ChangeHistoryManager;
import com.atlassian.jira.issue.history.ChangeItemBean;
import com.atlassian.jira.issue.index.IssueIndexManager;
import com.atlassian.jira.issue.link.IssueLinkType;
import com.atlassian.jira.issue.link.IssueLinkTypeManager;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.mock.MockProjectManager;
import com.atlassian.jira.mock.issue.MockIssue;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.onboarding.OnboardingStore;
import com.atlassian.jira.project.AssigneeTypes;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.project.version.VersionManager;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.task.TaskProgressSink;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.UserPropertyManager;
import com.atlassian.jira.user.util.MockUserManager;
import com.atlassian.jira.user.util.UserUtil;
import com.atlassian.jira.web.bean.MockI18nBean;
import com.atlassian.jira.web.util.AttachmentException;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.opensymphony.module.propertyset.memory.MemoryPropertySet;
import org.apache.log4j.Level;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.TestRule;
import org.mockito.Mock;
import org.ofbiz.core.entity.GenericEntityException;
import org.ofbiz.core.entity.GenericValue;
import org.ofbiz.core.entity.model.ModelEntity;
import org.ofbiz.core.entity.model.ModelField;

import java.io.File;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

import static com.atlassian.jira.imports.project.DefaultProjectImportPersister.ReindexTaskProgressProcessor;
import static com.atlassian.jira.imports.project.LogVerifier.catchLogMessages;
import static com.atlassian.jira.imports.project.LogVerifier.exception;
import static com.atlassian.jira.imports.project.LogVerifier.level;
import static com.atlassian.jira.imports.project.LogVerifier.renderedMessage;
import static com.atlassian.jira.imports.project.parser.UserAssociationParser.ASSOCIATION_TYPE_VOTE_ISSUE;
import static com.atlassian.jira.imports.project.parser.UserAssociationParser.ASSOCIATION_TYPE_WATCH_ISSUE;
import static com.atlassian.jira.issue.history.ChangeItemBean.STATIC_FIELD;
import static com.atlassian.jira.issue.util.IssueIterableIssueIdMatcher.issuesIdsThat;
import static com.atlassian.jira.project.AssigneeTypes.PROJECT_DEFAULT;
import static java.lang.String.valueOf;
import static java.util.Collections.emptyList;
import static java.util.Collections.emptyMap;
import static java.util.Collections.singletonList;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.Matchers.sameInstance;
import static org.hamcrest.Matchers.startsWith;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.argThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @since v3.13
 */
public class TestDefaultProjectImportPersister {
    @Rule
    public TestRule mocksInContainer = MockitoMocksInContainer.rule(this);

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Mock
    private ApplicationProperties applicationProperties;
    @Mock
    private IssueManager issueManager;
    @Mock
    @AvailableInContainer
    private OfBizDelegator ofBizDelegator;
    @Mock
    private NodeAssociationStore nodeAssociationStore;
    @Mock
    private ChangeHistoryManager changeHistoryManager;
    @Mock
    private UserAssociationStore userAssociationStore;
    @Mock
    private IssueIndexManager issueIndexManager;
    @Mock
    private IssueFactory issueFactory = mock(IssueFactory.class);
    @Mock
    private ExternalUtils externalUtils;
    @Mock
    private IssueLinkTypeManager issueLinkTypeManager;
    @Mock
    private AttachmentManager attachmentManager;
    @Mock
    private ProjectComponentManager projectComponentManager;
    @Mock
    private VersionManager versionManager;
    @Mock
    private UserPropertyManager userPropertyManager;
    @Mock
    private OnboardingStore onboardingStore;

    private MockProjectManager projectManager;
    private MockUserManager userManager;
    private MockUserService userService;
    private String defaultEmailSender;

    private DefaultProjectImportPersister projectImportPersister;

    @Before
    public void setUp() {
        defaultEmailSender = "monkey@baboon.com";
        projectManager = new MockProjectManager();
        userManager = new MockUserManager();
        userService = spy(new MockUserService(userManager)); // Only way to make it throw exceptions
        projectImportPersister = new DefaultProjectImportPersister(externalUtils, issueFactory, ofBizDelegator, issueIndexManager, issueManager,
                projectManager, versionManager, nodeAssociationStore, userAssociationStore, projectComponentManager, attachmentManager, changeHistoryManager,
                issueLinkTypeManager, userManager, userService, applicationProperties, userPropertyManager, onboardingStore) {
            @Override
            void setEmailSenderOnProject(final Project project, final String emailSender) {
                assertEquals(getDefaultEmailSender(), emailSender);
            }
        };
    }

    private String getDefaultEmailSender() {
        return defaultEmailSender;
    }

    @Test
    public void testUpdateProjectDetailsProjectGoneMissing() {
        // ExternalProject
        final ExternalProject externalProject = new ExternalProject();
        externalProject.setKey("MNK");
        externalProject.setAssigneeType("Monkey God");
        externalProject.setDescription("Great sage, equal of heaven.");
        externalProject.setEmailSender(defaultEmailSender);
        externalProject.setLead("Mr Bubbles");
        externalProject.setName("Monkey");
        externalProject.setUrl("http://www.monkey.com");

        // Mock ProjectManager
        when(projectManager.getProjectObjByKey("MNK")).thenReturn(null);
        expectedException.expect(IllegalStateException.class);
        projectImportPersister.updateProjectDetails(externalProject);
    }

    @Test
    public void testUpdateProjectDetails() {
        // ExternalProject
        final ExternalProject externalProject = new ExternalProject();
        externalProject.setKey("MKY");
        externalProject.setAssigneeType(String.valueOf(AssigneeTypes.PROJECT_LEAD));
        externalProject.setDescription("Great sage, equal of heaven.");
        externalProject.setEmailSender(defaultEmailSender);
        externalProject.setLead("Mr Bubbles");
        externalProject.setName("Monkey");
        externalProject.setUrl("http://www.monkey.com");

        projectManager.addProject(new MockProject(12, "MKY"));
        projectImportPersister.updateProjectDetails(externalProject);

        Project project = projectManager.getProjectObjByKey("MKY");

        assertEquals((Long) 12L, project.getId());
        assertEquals("MKY", project.getKey());
        assertEquals(AssigneeTypes.PROJECT_LEAD, project.getAssigneeType().longValue());
        assertEquals("Great sage, equal of heaven.", project.getDescription());
        assertEquals("Mr Bubbles", project.getLeadUserName());
        assertEquals("Monkey", project.getName());
        assertEquals("http://www.monkey.com", project.getUrl());
    }

    @Test
    public void testUpdateProjectDetailsNullAssigneeTypeUnassignedOff() throws Exception {
        _testUpdateProjectDetailsNullAssigneeType(false);
    }

    @Test
    public void testUpdateProjectDetailsNullAssigneeTypeUnassignedOn() throws Exception {
        _testUpdateProjectDetailsNullAssigneeType(true);
    }

    private void _testUpdateProjectDetailsNullAssigneeType(final boolean isUnassignedIssuesAllowed) {
        // ExternalProject
        final ExternalProject externalProject = new ExternalProject();
        externalProject.setKey("MKY");
        externalProject.setAssigneeType(null);
        externalProject.setDescription("Great sage, equal of heaven.");
        externalProject.setEmailSender(defaultEmailSender);
        externalProject.setLead("Mr Bubbles");
        externalProject.setName("Monkey");
        externalProject.setUrl("http://www.monkey.com");

        // Mock ProjectManager
        projectManager.addProject(new MockProject(12, "MKY"));
        when(applicationProperties.getOption(APKeys.JIRA_OPTION_ALLOWUNASSIGNED)).thenReturn(isUnassignedIssuesAllowed);

        projectImportPersister.updateProjectDetails(externalProject);
        // Assert update
        Project project = projectManager.getProjectObjByKey("MKY");
        assertEquals((Long) 12L, project.getId());
        assertEquals("MKY", project.getKey());
        if (isUnassignedIssuesAllowed) {
            assertEquals(AssigneeTypes.UNASSIGNED, project.getAssigneeType().longValue());
        } else {
            assertEquals(AssigneeTypes.PROJECT_LEAD, project.getAssigneeType().longValue());
        }
        assertEquals("Great sage, equal of heaven.", project.getDescription());
        assertEquals("Mr Bubbles", project.getLeadUserName());
        assertEquals("Monkey", project.getName());
        assertEquals("http://www.monkey.com", project.getUrl());
    }

    @Test
    public void testCreateUser() throws Exception {
        final ExternalUser externalUser = new ExternalUser("fred", "Freddy Kruger", "fk@you", "dude");
        externalUser.setKey("fred");
        externalUser.setPasswordHash("XYZ");
        externalUser.getUserPropertyMap().put("colour", "green");

        final MockApplicationUser fredUser = new MockApplicationUser("fred");

        MemoryPropertySet propertySet = new MemoryPropertySet();
        propertySet.init(null, null);

        final UserMapper userMapper = mock(UserMapper.class);
        when(userPropertyManager.getPropertySet(fredUser)).thenReturn(propertySet);

        projectImportPersister.createUser(userMapper, externalUser);
        final ApplicationUser createdUser = userManager.getUserByName("fred");

        assertNotNull(createdUser);
        assertEquals("Freddy Kruger", createdUser.getDisplayName());
        assertEquals("fk@you", createdUser.getEmailAddress());

        assertEquals("green", propertySet.getString("jira.meta.colour"));
    }

    @Test
    public void testCreateUserUserAlreadyExists() throws Exception {
        final MockApplicationUser fredUser = new MockApplicationUser("fred");
        userManager.addUser(fredUser);
        final UserMapper userMapper = mock(UserMapper.class);

        final ExternalUser user = new ExternalUser("fred", "Freddy Kruger", "fk@you", "dude");

        final boolean userCreated = projectImportPersister.createUser(userMapper, user);
        assertTrue(userCreated);
    }

    @Test
    public void testCreateUserUserNull() throws Exception {
        doThrow(new PermissionException()).when(userService).createUser(any());
        final UserMapper userMapper = mock(UserMapper.class);

        final ExternalUser user = new ExternalUser("fred", "Freddy Kruger", "fk@you", "dude");
        user.getUserPropertyMap().put("colour", "green");

        final boolean userCreated;
        try (LogVerifier logVerifier = LogVerifier.catchLogMessages(DefaultProjectImportPersister.log)) {
            userCreated = projectImportPersister.createUser(userMapper, user);
            logVerifier.verifyEventLogged(
                    allOf(
                            level(equalTo(Level.ERROR)),
                            renderedMessage(startsWith("An error occurred while trying to create user")),
                            exception(notNullValue())
                    )
            );
        }

        assertFalse(userCreated);
    }

    @Test
    public void testCreateComponents() throws Exception {
        final ExternalComponent component1 = new ExternalComponent();
        component1.setId("1");
        component1.setName("Component1");
        component1.setDescription("Component 1 Description");
        component1.setLead("admin");
        component1.setAssigneeType("2");

        final ExternalComponent component2 = new ExternalComponent();
        component2.setId("2");
        component2.setName("Component2");
        component2.setDescription("Component 2 Description");
        component2.setLead("admin");
        component2.setAssigneeType("2");

        final ExternalComponent component3 = new ExternalComponent();
        component3.setId("3");
        component3.setName("Component3");
        component3.setDescription("Component 3 Description");
        component3.setLead("admin");
        component3.setAssigneeType("2");

        final List<ExternalComponent> components = ImmutableList.of(component1, component2, component3);

        final ExternalProject project = new ExternalProject();
        project.setKey("TST");
        final BackupProject backupProject = new BackupProjectImpl(project, emptyList(),
                components, emptyList(), ImmutableList.of(12L, 14L), 0, ImmutableMap.of());

        addProject(12L, "TST");

        when(projectComponentManager.create("Component1", "Component 1 Description", "admin", 2, 12L)).
                thenReturn(new ProjectComponentImpl("", "", "", 2));
        when(projectComponentManager.create("Component2", "Component 2 Description", "admin", 2, 12L)).
                thenReturn(new ProjectComponentImpl("", "", "", 2));
        when(projectComponentManager.create("Component3", "Component 3 Description", "admin", 2, 12L)).
                thenReturn(new ProjectComponentImpl("", "", "", 2));

        final GroupManager groupManager = mock(GroupManager.class);
        final ProjectImportMapper projectImportMapper = new ProjectImportMapperImpl(mock(UserUtil.class), groupManager);
        final Map<String, ProjectComponent> newComponents = projectImportPersister.createComponents(backupProject, projectImportMapper);

        assertThat(newComponents.keySet(), containsInAnyOrder("1", "2", "3"));
    }

    private void addProject(Long id, String key) {
        final Project mockProject = mock(Project.class);
        when(mockProject.getId()).thenReturn(id);
        when(mockProject.getKey()).thenReturn(key);
        projectManager.addProject(mockProject);
    }

    @Test
    public void testCreateComponentsNoProject() throws Exception {
        final ExternalProject project = new ExternalProject();
        project.setKey("TST");
        final BackupProject backupProject = new BackupProjectImpl(project, emptyList(), emptyList(), emptyList(),
                ImmutableList.of(12L, 14L), 0, ImmutableMap.of());

        final GroupManager groupManager = mock(GroupManager.class);
        final ProjectImportMapper projectImportMapper = new ProjectImportMapperImpl(mock(UserUtil.class), groupManager);

        expectedException.expect(IllegalStateException.class);
        projectImportPersister.createComponents(backupProject, projectImportMapper);
    }

    @Test
    public void testCreateComponentsNoAssigneeType() throws Exception {
        final ExternalComponent component1 = new ExternalComponent();
        component1.setId("1");
        component1.setName("Component1");
        component1.setDescription("Component 1 Description");
        component1.setLead("admin");
        component1.setAssigneeType(null);

        final List<ExternalComponent> components = ImmutableList.of(component1);

        final ExternalProject project = new ExternalProject();
        project.setKey("TST");
        project.setId("TST");

        final BackupProject backupProject = new BackupProjectImpl(project, emptyList(), components, emptyList(),
                ImmutableList.of(12L, 14L), 0, ImmutableMap.of());

        addProject(12L, "TST");

        when(projectComponentManager.create("Component1", "Component 1 Description", "admin", PROJECT_DEFAULT, 12L)).
                thenReturn(new ProjectComponentImpl("", "", "", 0));

        final GroupManager groupManager = mock(GroupManager.class);
        final ProjectImportMapper projectImportMapper = new ProjectImportMapperImpl(mock(UserUtil.class), groupManager);

        final Map<String, ProjectComponent> newComponents = projectImportPersister.createComponents(backupProject, projectImportMapper);

        assertThat(newComponents.keySet(),
                containsInAnyOrder("1"));
        assertEquals(PROJECT_DEFAULT, newComponents.get("1").getAssigneeType());
    }

    @Test
    public void testCreateVersions() throws Exception {
        final ExternalVersion version1 = new ExternalVersion();
        version1.setId("1");
        version1.setName("Version1");
        version1.setDescription("Version 1 Description");
        final Date version1Date = new Date();
        version1.setReleaseDate(version1Date);
        version1.setSequence(5L);
        version1.setArchived(true);

        final ExternalVersion version2 = new ExternalVersion();
        version2.setId("2");
        version2.setName("Version2");
        version2.setDescription("Version 2 Description");
        final Date version2Date = new Date();
        version2.setReleaseDate(version2Date);
        version2.setSequence(1L);
        version2.setReleased(true);

        final ExternalVersion version3 = new ExternalVersion();
        version3.setId("3");
        version3.setName("Version3");
        version3.setDescription("Version 3 Description");
        final Date version3Date = new Date();
        version3.setReleaseDate(version3Date);
        version3.setSequence(4L);

        final List<ExternalVersion> versions = ImmutableList.of(version1, version2, version3);

        final ExternalProject project = new ExternalProject();
        project.setKey("TST");
        final BackupProject backupProject = new BackupProjectImpl(project, versions, emptyList(), emptyList(),
                ImmutableList.of(12L, 14L), 0, ImmutableMap.of());

        addProject(12L, "TST");

        when(versionManager.createVersion("Version2", version2Date, "Version 2 Description", 12L, null)).
                thenReturn(null);
        final List<Version> releaseVersions = singletonList(null);

        when(versionManager.createVersion("Version3", version3Date, "Version 3 Description", 12L, null)).
                thenReturn(null);
        when(versionManager.createVersion("Version1", version1Date, "Version 1 Description", 12L, null)).
                thenReturn(null);
        versionManager.archiveVersion(null, true);

        final Map<String, Version> newVersions = projectImportPersister.createVersions(backupProject);

        verify(versionManager).releaseVersions(releaseVersions, true);
        assertThat(newVersions.keySet(),
                containsInAnyOrder("1", "2", "3"));
    }

    @Test
    public void testCreateVersionsNoProject() {
        final ExternalProject project = new ExternalProject();
        project.setKey("TST");
        final BackupProject backupProject = new BackupProjectImpl(project, emptyList(), emptyList(), emptyList(),
                ImmutableList.of(12L, 14L), 0, ImmutableMap.of());


        expectedException.expect(IllegalStateException.class);
        projectImportPersister.createVersions(backupProject);
    }

    @Test
    public void testCreateProject() throws Exception {
        final ExternalProject project = new ExternalProject();
        project.setEmailSender(defaultEmailSender);
        final BackupProject backupProject = new BackupProjectImpl(project, emptyList(), emptyList(), emptyList(),
                ImmutableList.of(12L, 14L), 0, ImmutableMap.of());
        final Project mockProject = mock(Project.class);
        when(externalUtils.createProject(backupProject.getProject())).thenReturn(mockProject);

        assertEquals(mockProject, projectImportPersister.createProject(backupProject.getProject()));
    }

    @Test
    public void testCreateProjectNullEmailSender() throws Exception {
        defaultEmailSender = null;
        final ExternalProject project = new ExternalProject();
        final BackupProject backupProject = new BackupProjectImpl(project, emptyList(), emptyList(), emptyList(),
                ImmutableList.of(12L, 14L), 0, ImmutableMap.of());

        final Project mockProject = mock(Project.class);
        when(externalUtils.createProject(backupProject.getProject())).thenReturn(mockProject);

        assertEquals(mockProject, projectImportPersister.createProject(backupProject.getProject()));
    }

    @Test
    public void testCreateEntity() throws Exception {
        defaultEmailSender = null;
        final Map<String, String> fields = ImmutableMap.of("id", "123", "testfield", "something");
        final EntityRepresentation entityRepresentation = new EntityRepresentationImpl("TestEntity", fields);

        final ModelField mockModelField = mock(ModelField.class);
        when(mockModelField.getName()).thenReturn("testfield");

        final ModelField mockModelField2 = mock(ModelField.class);
        when(mockModelField2.getName()).thenReturn("notfound");

        final ModelEntity mockModelEntity = mock(ModelEntity.class);
        when(mockModelEntity.getFieldsIterator()).
                thenReturn(ImmutableList.of(mockModelField, mockModelField2).iterator());

        final GenericValue mockGenericValue = mock(GenericValue.class);
        when(mockGenericValue.getModelEntity()).thenReturn(mockModelEntity);
        when(mockGenericValue.getEntityName()).thenReturn("TestEntity");
        when(mockGenericValue.getAllFields()).thenReturn(ImmutableMap.<String, Object>copyOf(fields));
        when(mockGenericValue.getLong("id")).thenReturn(123L);

        when(ofBizDelegator.makeValue(entityRepresentation.getEntityName())).thenReturn(mockGenericValue);
        when(ofBizDelegator.createValue(entityRepresentation.getEntityName(), ImmutableMap.<String, Object>copyOf(fields))).
                thenReturn(mockGenericValue);

        assertEquals((Long) 123L, projectImportPersister.createEntity(entityRepresentation));
        verify(mockGenericValue).setString("testfield", "something");
    }

    @Test
    public void testCreateEntityDataAccessException() throws Exception {
        final Map<String, String> fields = ImmutableMap.of("id", "123", "testfield", "something");
        final EntityRepresentation entityRepresentation = new EntityRepresentationImpl("TestEntity", fields);

        final ModelField mockModelField = mock(ModelField.class);
        when(mockModelField.getName()).thenReturn("testfield");

        final ModelField mockModelField2 = mock(ModelField.class);
        when(mockModelField2.getName()).thenReturn("notfound");

        final ModelEntity mockModelEntity = mock(ModelEntity.class);
        when(mockModelEntity.getFieldsIterator()).
                thenReturn(ImmutableList.of(mockModelField, mockModelField2).iterator());

        final GenericValue mockGenericValue = mock(GenericValue.class);
        when(mockGenericValue.getModelEntity()).thenReturn(mockModelEntity);
        when(mockGenericValue.getEntityName()).thenReturn("TestEntity");
        when(mockGenericValue.getAllFields()).thenReturn((ImmutableMap.copyOf(fields)));

        when(ofBizDelegator.makeValue(entityRepresentation.getEntityName())).thenReturn(mockGenericValue);
        when(ofBizDelegator.createValue(entityRepresentation.getEntityName(),
                ImmutableMap.copyOf(fields))).thenThrow(new DataAccessException("Could not create thing."));

        assertNull(projectImportPersister.createEntity(entityRepresentation));
        verify(mockGenericValue).setString("testfield", "something");
    }

    @Test
    public void testCreateIssueExceptionThrown() throws Exception {
        final MutableIssue mockIssue = new MockIssue(12L);
        when(externalUtils.createIssue(mockIssue, "1", "2")).thenThrow(new ExternalException(new RuntimeException("blah")));
        when(issueFactory.getIssue()).thenReturn(mockIssue);

        final ExternalIssueImpl issue = new ExternalIssueImpl(null);
        issue.setStatus("1");
        issue.setResolution("2");
        issue.setProject("42");

        final Issue newIssue;
        try (LogVerifier logVerifier = catchLogMessages(DefaultProjectImportPersister.log)) {
            newIssue = projectImportPersister.createIssue(issue, null, null);
            logVerifier.verifyEventLogged(exception(instanceOf(ExternalException.class)));
        }
        assertNull(newIssue);

        // ensure that correct exception was dropped in call
        verify(issueFactory).getIssue();
    }

    @Test
    public void testCreateIssue() throws Exception {
        final Date importDate = new Date();

        final GenericValue mockGenericValue = mock(GenericValue.class);
        final MutableIssue mockIssue = new MockIssue(12L);
        final MutableIssue mockIssueForExternalIssue = new MockIssue(12L);
        final ExternalUtils externalUtils = mock(ExternalUtils.class);
        when(externalUtils.createIssue(mockIssueForExternalIssue, "1", "2")).thenReturn(mockGenericValue);

        final IssueFactory issueFactory = mock(IssueFactory.class);
        when(issueFactory.getIssue()).thenReturn(mockIssueForExternalIssue);
        when(issueFactory.getIssue(mockGenericValue)).thenReturn(mockIssue);

        final AtomicBoolean createChangeItemCalled = new AtomicBoolean(false);
        final DefaultProjectImportPersister projectImportPersister = new DefaultProjectImportPersister(externalUtils, issueFactory,
                null, null, null, null, null, null, null, null, null, null, null, null, null, applicationProperties, null, onboardingStore) {
            void createChangeItem(final ApplicationUser author, final Issue issue, final ChangeItemBean changeItem) {
                assertEquals("ProjectImport", changeItem.getField());
                assertEquals(STATIC_FIELD, changeItem.getFieldType());
                assertEquals("", changeItem.getFrom());
                assertEquals("", changeItem.getFromString());
                assertEquals(importDate.toString(), changeItem.getToString());
                assertEquals(valueOf(importDate.getTime()), changeItem.getTo());
                createChangeItemCalled.set(true);
            }
        };

        final ExternalIssueImpl issue = new ExternalIssueImpl(null);
        issue.setStatus("1");
        issue.setResolution("2");
        issue.setProject("42");

        final Issue newIssue = projectImportPersister.createIssue(issue, importDate, null);

        assertEquals(mockIssue, newIssue);
        assertTrue(createChangeItemCalled.get());
        verify(mockGenericValue).store();
    }

    @Test
    public void testUpdateIssueKeyDataAccessException() throws Exception {
        final ExternalIssue externalIssue = new ExternalIssueImpl(null);
        externalIssue.setKey("TST-1");

        final GenericValue mockGenericValue = mock(GenericValue.class);
        mockGenericValue.setString("key", "TST-1");
        doThrow(new GenericEntityException("This sucks!")).when(mockGenericValue).store();
        when(mockGenericValue.getLong("id")).thenReturn(12L);

        expectedException.expect(DataAccessException.class);
        expectedException.expectMessage("Unable to set the required key 'TST-1' in the Issue that we just created (id = '12').");
        projectImportPersister.updateIssueKey(externalIssue, mockGenericValue);

    }

    @Test
    public void testUpdateIssueKey() throws Exception {
        final ExternalIssue externalIssue = new ExternalIssueImpl(null);
        externalIssue.setKey("TST-1");

        final GenericValue mockGenericValue = mock(GenericValue.class);

        projectImportPersister.updateIssueKey(externalIssue, mockGenericValue);

        verify(mockGenericValue).setString("key", "TST-1");
        verify(mockGenericValue).store();
    }

    @Test
    public void testConvertExternalIssueToIssue() throws Exception {
        final ExternalIssue externalIssue = new ExternalIssueImpl("fred");
        externalIssue.setProject("11");
        externalIssue.setIssueType("7");
        externalIssue.setReporter("dylan");
        externalIssue.setAssignee("mark");
        externalIssue.setSummary("I am summary");
        externalIssue.setDescription("I am desc");
        externalIssue.setEnvironment("I am env");
        externalIssue.setPriority("3");
        externalIssue.setStatus("1");
        externalIssue.setResolution("5");

        externalIssue.setCreated(new Date());
        externalIssue.setUpdated(new Date());
        externalIssue.setDuedate(new Date());
        externalIssue.setResolutionDate(new Date());

        externalIssue.setVotes(3L);
        externalIssue.setOriginalEstimate(4L);
        externalIssue.setTimeSpent(2343L);
        externalIssue.setEstimate(5454L);
        externalIssue.setSecurityLevel("9");

        final MutableIssue mockMutableIssue = mock(MutableIssue.class);

        when(issueFactory.getIssue()).thenReturn(mockMutableIssue);

        projectImportPersister.createIssueForExternalIssue(externalIssue);

        verify(mockMutableIssue).setProjectId(new Long(externalIssue.getProject()));

        verify(mockMutableIssue).setIssueTypeId(externalIssue.getIssueType());

        verify(mockMutableIssue).setReporterId(externalIssue.getReporter());
        verify(mockMutableIssue).setAssigneeId(externalIssue.getAssignee());

        verify(mockMutableIssue).setSummary(externalIssue.getSummary());
        verify(mockMutableIssue).setDescription(externalIssue.getDescription());
        verify(mockMutableIssue).setEnvironment(externalIssue.getEnvironment());
        verify(mockMutableIssue).setPriorityId(externalIssue.getPriority());
        verify(mockMutableIssue).setResolutionId(externalIssue.getResolution());

        verify(mockMutableIssue).setCreated(new Timestamp(externalIssue.getCreated().getTime()));
        verify(mockMutableIssue).setUpdated(new Timestamp((externalIssue.getUpdated().getTime())));
        verify(mockMutableIssue).setDueDate(new Timestamp((externalIssue.getDuedate().getTime())));
        verify(mockMutableIssue).setResolutionDate(new Timestamp((externalIssue.getResolutionDate().getTime())));

        verify(mockMutableIssue).setVotes(externalIssue.getVotes());

        verify(mockMutableIssue).setOriginalEstimate(externalIssue.getOriginalEstimate());
        verify(mockMutableIssue).setTimeSpent(externalIssue.getTimeSpent());
        verify(mockMutableIssue).setEstimate(externalIssue.getEstimate());
        verify(mockMutableIssue).setSecurityLevelId(new Long(externalIssue.getSecurityLevel()));
    }

    @Test
    public void testReIndexProject() throws Exception {
        final MockI18nBean i18n = new MockI18nBean();

        when(issueIndexManager.reIndexIssues(any(), any())).thenReturn(2L);
        when(issueManager.getIssueObject((Long) any())).then(invocation -> {
            Long iId = (Long) invocation.getArguments()[0];
            return new MockIssue(iId);
        });

        final ProjectImportMapper projectImportMapper = new ProjectImportMapperImpl(null, null);
        projectImportMapper.getIssueMapper().mapValue("1", "101");
        projectImportMapper.getIssueMapper().mapValue("2", "102");

        projectImportPersister.reIndexProject(projectImportMapper, null, i18n);

        verify(issueIndexManager).reIndexIssues(argThat(issuesIdsThat(
                contains(101L, 102L)
        )), any());
    }

    @Test
    public void testCreateAssociation() throws Exception {
        final ExternalNodeAssociation nodeAssociation = new ExternalNodeAssociation("1", "Issue", "2", "Version", "AssType");

        projectImportPersister.createAssociation(nodeAssociation);

        verify(nodeAssociationStore).createAssociation(nodeAssociation.getSourceNodeEntity(), new Long(nodeAssociation.getSourceNodeId()),
                nodeAssociation.getSinkNodeEntity(), new Long(nodeAssociation.getSinkNodeId()), nodeAssociation.getAssociationType());
    }

    @Test
    public void testCreateVoter() throws Exception {
        final ExternalVoter externalVoter = new ExternalVoter();
        externalVoter.setIssueId("12");
        externalVoter.setVoter("admin");

        projectImportPersister.createVoter(externalVoter);
        verify(userAssociationStore).createAssociation(ASSOCIATION_TYPE_VOTE_ISSUE, "admin", "Issue", 12L);
    }

    @Test
    public void testCreateWatcher() throws Exception {
        final ExternalWatcher externalWatcher = new ExternalWatcher();
        externalWatcher.setIssueId("12");
        externalWatcher.setWatcher("admin");
        projectImportPersister.createWatcher(externalWatcher);

        verify(userAssociationStore).createAssociation(ASSOCIATION_TYPE_WATCH_ISSUE, "admin", "Issue", 12L);
    }

    @Test
    public void testCreateAttachment() throws Exception {
        final String adminKey = "admin";
        final String adminName = "adminName";
        final ExternalAttachment externalAttachment = new ExternalAttachment("12", "1212", "test.txt", new Date(), adminKey);
        externalAttachment.setAttachedFile(new File("/tmp"));

        final MockIssue mockIssue = new MockIssue(1212);
        final Attachment mockAttachment = mock(Attachment.class);
        when(attachmentManager.createAttachmentCopySourceFile(externalAttachment.getAttachedFile(), externalAttachment.getFileName(),
                DefaultProjectImportPersister.GENERIC_CONTENT_TYPE, adminName, mockIssue, emptyMap(),
                externalAttachment.getAttachedDate())).thenReturn(mockAttachment);
        when(issueManager.getIssueObject(1212L)).thenReturn(mockIssue);
        userManager.addUser(new MockApplicationUser(adminKey, adminName));

        final Attachment attachment = projectImportPersister.createAttachment(externalAttachment);

        assertThat(attachment, sameInstance(mockAttachment));
    }

    @Test
    public void testCreateAttachmentExceptionThrown() throws Exception {
        final String adminName = "adminName";
        final String adminKey = "admin";
        final ExternalAttachment externalAttachment = new ExternalAttachment("12", "1212", "test.txt", new Date(), adminKey);
        externalAttachment.setAttachedFile(new File("/tmp"));

        final MockIssue mockIssue = new MockIssue(1212);

        when(attachmentManager.createAttachmentCopySourceFile(externalAttachment.getAttachedFile(), externalAttachment.getFileName(),
                DefaultProjectImportPersister.GENERIC_CONTENT_TYPE, adminName, mockIssue, emptyMap(),
                externalAttachment.getAttachedDate())).thenThrow(new AttachmentException("blah"));
        when(issueManager.getIssueObject(1212L)).thenReturn(mockIssue);
        userManager.addUser(new MockApplicationUser(adminKey, adminName));

        final Attachment newAttachment;
        try (LogVerifier logVerifier = catchLogMessages(DefaultProjectImportPersister.log)) {
            newAttachment = projectImportPersister.createAttachment(externalAttachment);
            logVerifier.verifyEventLogged(exception(instanceOf(AttachmentException.class)));
        }
        assertThat(newAttachment, nullValue());
    }

    @Test
    public void testCreateAttachmentNullAttachment() throws Exception {
        expectedException.expect(IllegalArgumentException.class);
        projectImportPersister.createAttachment(null);
    }

    @Test
    public void testCreateAttachmentNullAttachmentFile() throws Exception {
        final ExternalAttachment externalAttachment = new ExternalAttachment("12", "1212", "test.txt", new Date(), "admin");

        expectedException.expect(IllegalArgumentException.class);
        projectImportPersister.createAttachment(externalAttachment);
    }

    @Test
    public void testCreateAttachmentNullIssueId() throws Exception {
        final ExternalAttachment externalAttachment = new ExternalAttachment("12", null, "test.txt", new Date(), "admin");
        externalAttachment.setAttachedFile(new File("/tmp"));

        expectedException.expect(IllegalArgumentException.class);
        projectImportPersister.createAttachment(externalAttachment);
    }

    @Test
    public void testCreateAttachmentNoSuchIssue() throws Exception {
        final ExternalAttachment externalAttachment = new ExternalAttachment("12", "1212", "test.txt", new Date(), "admin");
        externalAttachment.setAttachedFile(new File("/tmp"));

        when(issueManager.getIssueObject(1212L)).thenReturn(null);

        expectedException.expect(IllegalArgumentException.class);
        projectImportPersister.createAttachment(externalAttachment);
    }

    @Test
    public void testReindexTaskProgressProcessorNullProgress() throws Exception {
        final DefaultProjectImportPersister.ReindexTaskProgressProcessor taskProgressProcessor = new DefaultProjectImportPersister.ReindexTaskProgressProcessor(
                null, new MockI18nBean());
        // This should not throw an exception
        taskProgressProcessor.processTaskProgress(100);
    }

    @Test
    public void testReindexTaskProgressProcessor() throws Exception {
        // Mock TaskProgressSink
        final TaskProgressSink mockTaskProgressSink = mock(TaskProgressSink.class);
        mockTaskProgressSink.makeProgress(90, "Indexing", "Re-indexing is 50% complete.");

        final TaskProgressInterval progressInterval = new TaskProgressInterval(mockTaskProgressSink, 80, 100);
        final ReindexTaskProgressProcessor taskProgressProcessor = new ReindexTaskProgressProcessor(
                progressInterval, new MockI18nBean());
        taskProgressProcessor.processTaskProgress(50);

        // Verify Mock TaskProgressSink
    }

    @Test
    public void testCreateChangeItemForIssueLinkIfNeededNullIssue() throws Exception {
        final IssueManager mockIssueManager = mock(IssueManager.class);
        when(mockIssueManager.getIssueObject(12L)).thenReturn(null);

        final String changeItem = projectImportPersister.createChangeItemForIssueLinkIfNeeded("12", "1234", "TST-1", true, null);

        assertNull(changeItem);
    }

    @Test
    public void testCreateChangeItemForIssueLinkIfNeededDontCreateChangeItem() throws Exception {
        final MockIssue issue = new MockIssue();
        when(issueManager.getIssueObject(12L)).thenReturn(issue);
        final IssueLinkType mockIssueLinkType = mock(IssueLinkType.class);
        when(issueLinkTypeManager.getIssueLinkType(anyLong())).thenReturn(mockIssueLinkType);
        ChangeItemBean changeItem = mock(ChangeItemBean.class);
        when(changeItem.getFrom()).thenReturn("TST-1");
        when(changeHistoryManager.getChangeItemsForField(issue, "Link")).thenReturn(Arrays.asList(changeItem));
        when(ofBizDelegator.createValue(eq("ChangeGroup"), any())).thenReturn(mock(GenericValue.class));

        final String changedIssue = projectImportPersister.createChangeItemForIssueLinkIfNeeded("12", "1234", "TST-1", true, null);

        assertNull(changedIssue);
    }

    @Test
    public void testCreateChangeItemForIssueLinkIfNeededHappyPath() throws Exception {
        final MutableIssue mockMutableIssue = mock(MutableIssue.class);
        when(mockMutableIssue.getGenericValue()).thenReturn(mock(GenericValue.class));
        when(issueManager.getIssueObject(12L)).thenReturn(mockMutableIssue);
        final IssueLinkType mockIssueLinkType = mock(IssueLinkType.class);
        when(issueLinkTypeManager.getIssueLinkType(anyLong())).thenReturn(mockIssueLinkType);
        when(mockIssueLinkType.getOutward()).thenReturn("outward");
        when(changeHistoryManager.getChangeItemsForField(mockMutableIssue, "Link")).thenReturn(Arrays.asList());

        when(ofBizDelegator.createValue(eq("ChangeGroup"), any())).thenReturn(mock(GenericValue.class));

        final String changeItem = projectImportPersister.createChangeItemForIssueLinkIfNeeded("12", "1234", "TST-1", true, null);

        assertEquals("12", changeItem);
        verify(ofBizDelegator).createValue(eq("ChangeItem"), any());
        verify(mockMutableIssue).setUpdated(any());
        verify(mockMutableIssue).store();
    }

    @Test
    public void testCreateIssueLinkChangeItemLinkCreatedAndDeleted() throws Exception {
        final MockIssue issue = new MockIssue();
        final List<ChangeItemBean> changeItemsForIssue = ImmutableList.of(new ChangeItemBean("jira", "Link", "TST-1", null), new ChangeItemBean("jira", "Link", null,
                "TST-1"));
        when(changeHistoryManager.getChangeItemsForField(issue, "Link")).thenReturn(changeItemsForIssue);

        final boolean issueLinkChangeItemCreated = projectImportPersister.createIssueLinkChangeItem("TST-1", issue);
        assertTrue(issueLinkChangeItemCreated);
    }

    @Test
    public void testCreateIssueLinkChangeItemNoLinkChangeItems() throws Exception {
        final MockIssue issue = new MockIssue();

        final List<ChangeItemBean> changeItemsForIssue = emptyList();

        final ChangeHistoryManager changeHistoryManager = mock(ChangeHistoryManager.class);
        when(changeHistoryManager.getChangeItemsForField(issue, "Link")).thenReturn(changeItemsForIssue);

        final boolean issueLinkChangeItemCreated = projectImportPersister.createIssueLinkChangeItem("TST-1", issue);

        assertTrue(issueLinkChangeItemCreated);
    }

    @Test
    public void testCreateIssueLinkChangeItemDoesNotNeedCreating() throws Exception {
        final MockIssue issue = new MockIssue();

        final List<ChangeItemBean> changeItemsForIssue = singletonList(new ChangeItemBean("jira", "Link", "TST-1", null));

        when(changeHistoryManager.getChangeItemsForField(issue, "Link")).thenReturn(changeItemsForIssue);

        final boolean issueLinkChangeItemCreated = projectImportPersister.createIssueLinkChangeItem("TST-1", issue);

        assertFalse(issueLinkChangeItemCreated);
    }

    @Test
    public void testGetChangeItemBeanIsSource() throws Exception {
        final IssueLinkType mockIssueLinkType = mock(IssueLinkType.class);
        when(mockIssueLinkType.getOutward()).thenReturn("outward");

        when(issueLinkTypeManager.getIssueLinkType(12L)).thenReturn(mockIssueLinkType);

        final ChangeItemBean changeItemBean = projectImportPersister.getChangeItemBean("12", "TST-1", true);

        assertEquals("jira", changeItemBean.getFieldType());
        assertEquals("Link", changeItemBean.getField());
        assertNull(changeItemBean.getFrom());
        assertNull(changeItemBean.getFromString());
        assertEquals("TST-1", changeItemBean.getTo());
        assertEquals("This issue outward TST-1", changeItemBean.getToString());

    }

    @Test
    public void testGetChangeItemBeanIsDest() throws Exception {
        final IssueLinkType mockIssueLinkType = mock(IssueLinkType.class);
        when(mockIssueLinkType.getInward()).thenReturn("inward");
        when(issueLinkTypeManager.getIssueLinkType(12L)).thenReturn(mockIssueLinkType);

        final ChangeItemBean changeItemBean = projectImportPersister.getChangeItemBean("12", "TST-1", false);
        assertEquals("jira", changeItemBean.getFieldType());
        assertEquals("Link", changeItemBean.getField());
        assertNull(changeItemBean.getFrom());
        assertNull(changeItemBean.getFromString());
        assertEquals("TST-1", changeItemBean.getTo());
        assertEquals("This issue inward TST-1", changeItemBean.getToString());
    }
}
