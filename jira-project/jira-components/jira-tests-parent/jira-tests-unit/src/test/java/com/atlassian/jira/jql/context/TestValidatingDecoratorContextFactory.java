package com.atlassian.jira.jql.context;

import com.atlassian.jira.jql.validator.OperatorUsageValidator;
import com.atlassian.query.clause.TerminalClause;
import com.atlassian.query.clause.TerminalClauseImpl;
import com.atlassian.query.operator.Operator;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Collections;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Test for {@link com.atlassian.jira.jql.context.ValidatingDecoratorContextFactory}.
 *
 * @since v4.0
 */
public class TestValidatingDecoratorContextFactory {
    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @Mock
    private OperatorUsageValidator usageValidator;
    @Mock
    private ClauseContextFactory delegate;
    @InjectMocks
    private ValidatingDecoratorContextFactory factory;

    @Test
    public void testInvalidTerminalClause() throws Exception {
        final TerminalClause clause = new TerminalClauseImpl("one", Operator.GREATER_THAN, 2L);
        final ClauseContext context = ClauseContextImpl.createGlobalClauseContext();

        assertEquals(context, factory.getClauseContext(null, clause));

        verify(usageValidator).check(null, clause);
    }

    @Test
    public void testValidTerminalClause() throws Exception {
        final TerminalClause clause = new TerminalClauseImpl("one", Operator.GREATER_THAN, 2L);

        final ProjectIssueTypeContext projectCtx = new ProjectIssueTypeContextImpl(
                new ProjectContextImpl(10L),
                new IssueTypeContextImpl("47"));

        final ClauseContext expectedContext = new ClauseContextImpl(Collections.singleton(projectCtx));

        when(usageValidator.check(null, clause)).thenReturn(true);
        when(delegate.getClauseContext(null, clause)).thenReturn(expectedContext);

        assertEquals(expectedContext, factory.getClauseContext(null, clause));

        verify(usageValidator).check(null, clause);
        verify(delegate).getClauseContext(null, clause);
    }
}
