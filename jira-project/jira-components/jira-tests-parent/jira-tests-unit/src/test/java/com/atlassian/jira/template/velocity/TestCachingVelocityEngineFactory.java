package com.atlassian.jira.template.velocity;

import org.apache.velocity.app.VelocityEngine;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.sameInstance;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestCachingVelocityEngineFactory {

    @Rule
    public MockitoRule mockito = MockitoJUnit.rule();

    @Mock
    private VelocityEngineFactory velocityEngineFactory;

    @Mock
    private VelocityEngine velocityEngine;

    private CachingVelocityEngineFactory cachingVelocityEngineFactory;

    @Before
    public void setup() {
        cachingVelocityEngineFactory = new CachingVelocityEngineFactory(velocityEngineFactory);
        when(velocityEngineFactory.getEngine()).thenReturn(velocityEngine);
    }

    @Test
    public void shouldCallDelegatedFactoryOnlyOnce() {

        final VelocityEngine engine1 = cachingVelocityEngineFactory.getEngine();
        final VelocityEngine engine2 = cachingVelocityEngineFactory.getEngine();

        verify(velocityEngineFactory, times(1)).getEngine();
        assertThat(engine1, sameInstance(velocityEngine));
        assertThat(engine2, sameInstance(velocityEngine));
    }
}