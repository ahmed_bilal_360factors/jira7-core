package com.atlassian.jira.favourites;

import com.atlassian.jira.exception.PermissionException;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.sharing.ShareManager;
import com.atlassian.jira.sharing.SharedEntity;
import com.atlassian.jira.sharing.SharedEntityAccessor;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import org.hamcrest.FeatureMatcher;
import org.hamcrest.Matcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.RuleChain;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.Matchers.empty;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.argThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

public class TestDefaultFavouritesManager {
    @Rule
    public RuleChain mocks = MockitoMocksInContainer.forTest(this);
    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Mock
    private FavouritesStore store;
    @Mock
    private SharedEntityAccessor<SharedEntity> sharedEntityAccessor;
    @Mock
    private SharedEntityAccessor.Factory sharedEntityAccessorFactory;
    @Mock
    private ShareManager shareManager;

    @InjectMocks
    private DefaultFavouritesManager manager;

    private static final Long ENTITY_ID = 999L;
    private final ApplicationUser user = new MockApplicationUser("admin");
    private final SharedEntity entity = new SharedEntity.Identifier(TestDefaultFavouritesManager.ENTITY_ID, SearchRequest.ENTITY_TYPE, user);
    private final SharedEntity.TypeDescriptor<SharedEntity> entityType = entity.getEntityType();

    @Test
    public void testAddFavSuccess() throws PermissionException {
        // given:
        when(store.addFavourite(user, entity)).thenReturn(true);
        when(shareManager.isSharedWith(user, entity)).thenReturn(true);
        when(sharedEntityAccessorFactory.getSharedEntityAccessor(entityType)).thenReturn(sharedEntityAccessor);

        // when:
        manager.addFavourite(user, entity);

        // then:
        verify(sharedEntityAccessor).adjustFavouriteCount(entity, 1);
    }

    @Test
    public void testAddFavInPosFail() throws PermissionException {
        // given:
        when(store.addFavourite(user, entity)).thenReturn(false);
        when(shareManager.isSharedWith(user, entity)).thenReturn(true);

        // when:
        manager.addFavouriteInPosition(user, entity, 1);

        // then:
        verifyZeroInteractions(sharedEntityAccessor);
    }

    @Test
    public void testAddFavInPosNoPerm() throws Exception {
        // given:
        when(shareManager.isSharedWith(user, entity)).thenReturn(false);

        // expect:
        exception.expect(PermissionException.class);

        // once:
        manager.addFavouriteInPosition(user, entity, 1);
    }

    @Test
    public void testAddFavInPosNullEntityForAdd() throws PermissionException {
        // given:
        final DefaultFavouritesManager illConstructedManager = new DefaultFavouritesManager(null, null, null);

        // expect:
        exception.expect(IllegalArgumentException.class);

        // once:
        illConstructedManager.addFavouriteInPosition(user, null, 1);
    }

    @Test
    public void testFavInPosNullUserForAdd() throws PermissionException {
        // given:
        final DefaultFavouritesManager illConstructedManager = new DefaultFavouritesManager(null, null, null);

        // expect:
        exception.expect(IllegalArgumentException.class);

        // once:
        illConstructedManager.addFavouriteInPosition((ApplicationUser) null, entity, 1);
    }

    @Test
    public void testAddFavInPosHappyPath() throws Exception {
        // given:
        when(store.addFavourite(user, entity)).thenReturn(true);
        when(shareManager.isSharedWith(user, entity)).thenReturn(true);
        when(sharedEntityAccessorFactory.getSharedEntityAccessor(entityType)).thenReturn(sharedEntityAccessor);
        when(store.getFavouriteIds(user, entity.getEntityType())).thenReturn(Collections.emptyList());

        final AtomicBoolean reorderCalled = new AtomicBoolean(false);

        final FavouritesManager<SharedEntity> manager = new DefaultFavouritesManager(store, sharedEntityAccessorFactory, shareManager) {
            @Override
            void reorderFavourites(final ApplicationUser user, final SharedEntity entity, final DefaultFavouritesManager.FavouriteReordererCommand favouriteReordererCommand) {
                reorderCalled.set(true);
                assertTrue(favouriteReordererCommand instanceof InsertInPositionReorderCommand);
                assertEquals(1, ((InsertInPositionReorderCommand) favouriteReordererCommand).position);
            }
        };

        // when:
        manager.addFavouriteInPosition(user, entity, 1);

        // then:
        assertTrue(reorderCalled.get());
        verify(sharedEntityAccessor).adjustFavouriteCount(entity, 1);
    }

    @Test
    public void testInsertInPositionReorderCommand() throws Exception {
        // given:
        final List oldList = Lists.newArrayList("One", "Two", "Four", "Three");

        // when:
        final DefaultFavouritesManager.InsertInPositionReorderCommand positionReorderCommand = new DefaultFavouritesManager.InsertInPositionReorderCommand(
                2);
        positionReorderCommand.reorderFavourites(oldList, null);

        // then:
        assertEquals(oldList, ImmutableList.of("One", "Two", "Three", "Four"));
    }

    @Test
    public void testInsertInPositionReorderCommandNegativeNumber() throws Exception {
        // given:
        final List oldList = Lists.newArrayList("One", "Two", "Four", "Three");

        // when:
        final DefaultFavouritesManager.InsertInPositionReorderCommand positionReorderCommand = new DefaultFavouritesManager.InsertInPositionReorderCommand(
                -1);
        positionReorderCommand.reorderFavourites(oldList, null);

        // then:
        assertEquals(oldList, ImmutableList.of("One", "Two", "Four", "Three"));
    }

    @Test
    public void testInsertInPositionReorderCommandLargerNumberThanSize() throws Exception {
        // given:
        final List oldList = Lists.newArrayList("One", "Two", "Four", "Three");

        // when:
        final DefaultFavouritesManager.InsertInPositionReorderCommand positionReorderCommand = new DefaultFavouritesManager.InsertInPositionReorderCommand(
                70);
        positionReorderCommand.reorderFavourites(oldList, null);

        // then:
        assertEquals(oldList, ImmutableList.of("One", "Two", "Four", "Three"));
    }

    @Test
    public void testAddFavFail() throws PermissionException {
        // given:
        when(store.addFavourite(user, entity)).thenReturn(false);
        when(shareManager.isSharedWith(user, entity)).thenReturn(true);

        // when:
        manager.addFavourite(user, entity);

        // then:
        verifyZeroInteractions(sharedEntityAccessor);
    }

    @Test
    public void testAddFavNoPerm() throws Exception {
        // given:
        when(shareManager.isSharedWith(user, entity)).thenReturn(false);

        // expect:
        exception.expect(PermissionException.class);

        // once:
        manager.addFavourite(user, entity);
    }

    @Test
    public void testNullEntityForAdd() throws PermissionException {
        // given:
        final DefaultFavouritesManager illConstructedManager = new DefaultFavouritesManager(null, null, null);

        // expect:
        exception.expect(IllegalArgumentException.class);

        // once:
        illConstructedManager.addFavourite(user, null);
    }

    @Test
    public void testNullUserForAdd() throws PermissionException {
        // given:
        final DefaultFavouritesManager illConstructedManager = new DefaultFavouritesManager(null, null, null);

        // expect:
        exception.expect(IllegalArgumentException.class);

        // once:
        illConstructedManager.addFavourite((ApplicationUser) null, entity);
    }

    @Test
    public void testNullTypeForSharableEntityIdentifierType() {
        // expect:
        exception.expect(IllegalArgumentException.class);

        // once:
        new SharedEntity.Identifier(TestDefaultFavouritesManager.ENTITY_ID, null, (ApplicationUser) null);
    }

    @Test
    public void testNullIdForSharableEntityIdentifierType() {
        // expect:
        exception.expect(IllegalArgumentException.class);

        // once:
        new SharedEntity.Identifier(null, SearchRequest.ENTITY_TYPE, (ApplicationUser) null);
    }

    @Test
    public void testRemoveSuccess() {
        // given:
        when(store.removeFavourite(user, entity)).thenReturn(true);
        when(sharedEntityAccessorFactory.getSharedEntityAccessor(entityType)).thenReturn(sharedEntityAccessor);

        // when:
        manager.removeFavourite(user, entity);

        // then:
        verify(sharedEntityAccessor).adjustFavouriteCount(entity, -1);
    }

    @Test
    public void testRemoveFail() {
        // given:
        when(store.removeFavourite(user, entity)).thenReturn(false);

        // when:
        manager.removeFavourite(user, entity);

        // then:
        verifyZeroInteractions(sharedEntityAccessor);
    }

    @Test
    public void removeFavouritesForUser() {
        // given:
        final ImmutableList<Long> ids = ImmutableList.of(1L, 999L);
        when(store.getFavouriteIds(user, entityType)).thenReturn(ids);
        when(store.removeFavourite(eq(user), any(SharedEntity.class))).thenReturn(true);

        // when:
        manager.removeFavouritesForUser(user, entityType);

        // then:
        for (final long id : ids) {
            verify(store).removeFavourite(eq(user), argThat(allOf(
                    entityOfType(equalTo(entityType)),
                    entityWithId(equalTo(id))
            )));
        }
    }

    @Test
    public void removeFavouritesForUserNullUser() {
        // expect:
        exception.expect(IllegalArgumentException.class);

        // once:
        manager.removeFavouritesForUser((ApplicationUser) null, entity.getEntityType());
    }

    @Test
    public void removeFavouritesForUserNullEntity() {
        // expect:
        exception.expect(IllegalArgumentException.class);

        // once:
        manager.removeFavouritesForUser(user, null);
    }

    @Test
    public void testRemoveFavouritesForEntity() {
        // when
        manager.removeFavouritesForEntityDelete(entity);

        // then
        verify(store).removeFavouritesForEntity(entity);
    }

    @Test
    public void testRemoveFavouritesForEntityNullParam() {
        // expect:
        exception.expect(IllegalArgumentException.class);

        // once:
        manager.removeFavouritesForEntityDelete(null);
    }

    @Test
    public void testRemoveFavouritesForEntityNullEntityId() {
        // given:
        final SharedEntity sharedEntity = mock(SharedEntity.class);
        when(sharedEntity.getPermissions()).thenThrow(UnsupportedOperationException.class);

        // expect:
        exception.expect(IllegalArgumentException.class);

        // once:
        manager.removeFavouritesForEntityDelete(sharedEntity);
    }

    @Test
    public void testRemoveFavouritesForEntityNullEntityType() {
        // given:
        final SharedEntity sharedEntity = mock(SharedEntity.class);
        when(sharedEntity.getId()).thenReturn(1L);
        when(sharedEntity.getPermissions()).thenThrow(UnsupportedOperationException.class);

        // expect:
        exception.expect(IllegalArgumentException.class);
        // once:
        manager.removeFavouritesForEntityDelete(sharedEntity);
    }

    @Test
    public void testNullAllParamsForRemove() {
        // given:
        final DefaultFavouritesManager illConstructedManager = new DefaultFavouritesManager(null, null, null);

        // expect:
        exception.expect(IllegalArgumentException.class);

        // once:
        illConstructedManager.removeFavourite((ApplicationUser) null, entity);
    }

    @Test
    public void testIsFavSuccess() throws PermissionException {
        // given:
        when(shareManager.isSharedWith(user, entity)).thenReturn(true);
        when(store.isFavourite(user, entity)).thenReturn(true);

        // when:
        final boolean favourite = manager.isFavourite(user, entity);

        // then:
        assertTrue(favourite);
    }

    @Test
    public void testIsFavFail() throws PermissionException {
        // given:
        when(shareManager.isSharedWith(user, entity)).thenReturn(true);
        when(store.isFavourite(user, entity)).thenReturn(false);

        // when:
        final boolean favourite = manager.isFavourite(user, entity);

        // then:
        assertFalse(favourite);
    }

    @Test
    public void testIsFavNoPerm() throws Exception {
        // given:
        when(shareManager.isSharedWith(user, entity)).thenReturn(false);

        // expect:
        exception.expect(PermissionException.class);

        // once:
        manager.isFavourite(user, entity);
    }

    @Test
    public void testNullParamsForIs() throws PermissionException {
        // when:
        final DefaultFavouritesManager illConstructedManager = new DefaultFavouritesManager(null, null, null);

        // expect:
        exception.expect(IllegalArgumentException.class);

        // once:
        illConstructedManager.isFavourite(user, null);
    }

    @Test
    public void testGetFavouriteIdsSuccess() {
        // given:
        final List<Long> ids = ImmutableList.of(1L, 999L);
        when(store.getFavouriteIds(user, entityType)).thenReturn(ids);

        // when:
        final Collection<Long> actualIds = manager.getFavouriteIds(user, entity.getEntityType());

        // then:
        assertNotNull(actualIds);
        assertEquals(ids, actualIds);
    }

    @Test
    public void testGetFavouriteIdsNoneStored() {
        // given:
        when(store.getFavouriteIds(user, entityType)).thenReturn(Collections.emptyList());

        // when:
        final Collection<Long> ids = manager.getFavouriteIds(user, entity.getEntityType());

        // then:
        assertNotNull(ids);
        assertThat(ids, empty());
    }

    @Test
    public void testGetFavouriteIdsNullUserParam() {
        // expect:
        exception.expect(IllegalArgumentException.class);

        // once:
        manager.getFavouriteIds((ApplicationUser) null, entity.getEntityType());
    }

    @Test
    public void testGetFavouriteIdsNullEntityTypeParam() {
        // expect:
        exception.expect(IllegalArgumentException.class);

        // once:
        manager.getFavouriteIds(user, null);
    }

    public Matcher<SharedEntity> entityWithId(final Matcher<Long> idMatcher) {
        return new FeatureMatcher<SharedEntity, Long>(idMatcher, "Shared Entity with id", "entity with id") {
            @Override
            protected Long featureValueOf(final SharedEntity actual) {
                return actual.getId();
            }
        };
    }

    public Matcher<SharedEntity> entityOfType(final Matcher<SharedEntity.TypeDescriptor<SharedEntity>> typeMatcher) {
        return new FeatureMatcher<SharedEntity, SharedEntity.TypeDescriptor<SharedEntity>>(typeMatcher, "Shared Entity with type", "entity with type") {
            @Override
            protected SharedEntity.TypeDescriptor<SharedEntity> featureValueOf(final SharedEntity actual) {
                return actual.getEntityType();
            }
        };
    }


}
