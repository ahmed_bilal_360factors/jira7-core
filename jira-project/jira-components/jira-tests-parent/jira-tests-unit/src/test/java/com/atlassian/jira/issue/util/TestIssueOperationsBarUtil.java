package com.atlassian.jira.issue.util;

import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.plugin.webfragment.SimpleLinkManager;
import com.atlassian.jira.plugin.webfragment.model.SimpleLink;
import com.atlassian.jira.plugin.webfragment.model.SimpleLinkImpl;
import com.atlassian.jira.plugin.webfragment.model.SimpleLinkSection;
import com.atlassian.jira.plugin.webfragment.model.SimpleLinkSectionImpl;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.util.collect.CollectionBuilder;
import com.google.common.collect.ImmutableList;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.when;

public class TestIssueOperationsBarUtil {
    @Rule
    public MockitoRule mockito = MockitoJUnit.rule();

    private ApplicationUser user;
    @Mock
    private SimpleLinkManager linkManager;
    @Mock
    private PermissionManager permissionManager;
    @Mock
    private IssueManager issueManager;
    @Mock
    private ApplicationProperties appProps;

    @Before
    public void setUp() throws Exception {
        user = new MockApplicationUser("admin");
    }

    @Test
    public void testGetGroupsForEmpty() {
        when(linkManager.getSectionsForLocation("view.issue.opsbar", user, null)).thenReturn(Collections.<SimpleLinkSection>emptyList());

        final IssueOperationsBarUtil opsBarUtil = new IssueOperationsBarUtil(null, user, linkManager, appProps, issueManager, permissionManager, null);

        List<SimpleLinkSection> list = opsBarUtil.getGroups();
        assertTrue(list.isEmpty());

        // It should be cached now.  Shouldn't hit link manager
        list = opsBarUtil.getGroups();
        assertTrue(list.isEmpty());
    }

    @Test
    public void testGetGroupsForNonEmpty() {
        final List<SimpleLinkSection> groups = CollectionBuilder.list(createSection("1"), createSection("2"), createSection("3"));

        when(linkManager.getSectionsForLocation("view.issue.opsbar", user, null)).thenReturn(groups);

        final IssueOperationsBarUtil opsBarUtil = new IssueOperationsBarUtil(null, user, linkManager, appProps, issueManager, permissionManager, null);


        List<SimpleLinkSection> list = opsBarUtil.getGroups();
        assertEquals(groups, list);

        // It should be cached now.  Shouldn't hit link manager
        list = opsBarUtil.getGroups();
        assertEquals(groups, list);
    }

    @Test
    public void testGetLinkLabel() {
        final SimpleLinkImpl link1 = new SimpleLinkImpl("id", "label", "title", "", "", null, "", "");
        final SimpleLinkImpl link2 = new SimpleLinkImpl("id", "1234567890123456789012345", "title", "", "", null, "", "");
        final SimpleLinkImpl link3 = new SimpleLinkImpl("id", "12345678901234567890123456", "title", "", "", null, "", "");
        final SimpleLinkImpl link4 = new SimpleLinkImpl("id", "123456789012345678901234567890", "title", "", "", null, "", "");

        final IssueOperationsBarUtil opsBarUtil = new IssueOperationsBarUtil(null, user, linkManager, appProps, issueManager, permissionManager, null);

        assertEquals("label", opsBarUtil.getLabelForLink(link1));
        assertEquals("1234567890123456789012345", opsBarUtil.getLabelForLink(link2));
        assertEquals("1234567890123456789012...", opsBarUtil.getLabelForLink(link3));
        assertEquals("1234567890123456789012...", opsBarUtil.getLabelForLink(link4));
    }

    @Test
    public void testGetLinkTitle() {
        final SimpleLinkImpl link1 = new SimpleLinkImpl("id", "label", "title", "", "", null, "", "");
        final SimpleLinkImpl link2 = new SimpleLinkImpl("id", "1234567890123456789012345", null, "", "", null, "", "");
        final SimpleLinkImpl link3 = new SimpleLinkImpl("id", "12345678901234567890123456", "title", "", "", null, "", "");
        final SimpleLinkImpl link4 = new SimpleLinkImpl("id", "123456789012345678901234567890", null, "", "", null, "", "");

        final IssueOperationsBarUtil opsBarUtil = new IssueOperationsBarUtil(null, user, linkManager, appProps, issueManager, permissionManager, null);

        assertEquals("title", opsBarUtil.getTitleForLink(link1));
        assertEquals("", opsBarUtil.getTitleForLink(link2));
        assertEquals("title", opsBarUtil.getTitleForLink(link3));
        assertEquals("123456789012345678901234567890", opsBarUtil.getTitleForLink(link4));
    }

    @Test
    public void testGetPromotedLinksForGroup() {
        final SimpleLinkSection group = createSection("group");

        final SimpleLink link1 = createLink("1");
        final SimpleLink link2 = createLink("2");
        final SimpleLink link3 = createLink("3");
        final List<SimpleLink> links1 = CollectionBuilder.list(link1, link2, link3);

        final SimpleLinkSection section1 = createSection("1");
        final SimpleLinkSection section2 = createSection("2");
        final SimpleLinkSection section3 = createSection("3");
        final SimpleLinkSection section4 = createSection("4");
        final List<SimpleLinkSection> sections = CollectionBuilder.list(section1, section2, section3, section4);

        when(appProps.getDefaultBackedString("ops.bar.group.size.id-group")).thenReturn(null);
        when(appProps.getDefaultBackedString("ops.bar.group.size")).thenReturn(null);

        when(linkManager.getSectionsForLocation(group.getId(), user, null)).thenReturn(sections);
        when(linkManager.getLinksForSection(section1.getId(), user, null)).thenReturn(links1);

        final IssueOperationsBarUtil opsBarUtil = new IssueOperationsBarUtil(null, user, linkManager, appProps, issueManager, permissionManager, null);

        List<SimpleLink> list = opsBarUtil.getPromotedLinks(group);
        assertEquals(CollectionBuilder.list(link1), list);

        // It should be cached now.  Shouldn't hit link manager
        list = opsBarUtil.getPromotedLinks(group);
        assertEquals(CollectionBuilder.list(link1), list);
    }

    @Test
    public void testGetPromotedLinksForGroupMultipleSections() {
        final SimpleLinkSection group = createSection("group");

        final SimpleLink link1 = createLink("1");
        final SimpleLink link2 = createLink("2");
        final SimpleLink link3 = createLink("3");
        final List<SimpleLink> links1 = CollectionBuilder.list(link1, link2, link3);

        final SimpleLink link11 = createLink("11");
        final SimpleLink link12 = createLink("12");
        final SimpleLink link13 = createLink("13");
        final List<SimpleLink> links2 = CollectionBuilder.list(link11, link12, link13);

        final SimpleLinkSection section1 = createSection("1");
        final SimpleLinkSection section2 = createSection("2");
        final SimpleLinkSection section3 = createSection("3");
        final SimpleLinkSection section4 = createSection("4");
        final List<SimpleLinkSection> sections = CollectionBuilder.list(section1, section2, section3, section4);

        when(appProps.getDefaultBackedString("ops.bar.group.size.id-group")).thenReturn(null);
        when(appProps.getDefaultBackedString("ops.bar.group.size")).thenReturn("5");

        when(linkManager.getSectionsForLocation(group.getId(), user, null)).thenReturn(sections);
        when(linkManager.getLinksForSection(section1.getId(), user, null)).thenReturn(links1);
        when(linkManager.getLinksForSection(section2.getId(), user, null)).thenReturn(links2);

        final IssueOperationsBarUtil opsBarUtil = new IssueOperationsBarUtil(null, user, linkManager, appProps, issueManager, permissionManager, null);

        List<SimpleLink> list = opsBarUtil.getPromotedLinks(group);
        assertEquals(CollectionBuilder.list(link1, link2, link3, link11, link12), list);

        // It should be cached now.  Shouldn't hit link manager
        list = opsBarUtil.getPromotedLinks(group);
        assertEquals(CollectionBuilder.list(link1, link2, link3, link11, link12), list);
    }

    @Test
    public void testGetNonPromotedLinksForSectionNone() {
        final SimpleLinkSection group = createSection("group");

        final SimpleLink link1 = createLink("1");
        final SimpleLink link2 = createLink("2");
        final SimpleLink link3 = createLink("3");
        final List<SimpleLink> links1 = CollectionBuilder.newBuilder(link1, link2, link3).asMutableList();

        final SimpleLink link11 = createLink("11");
        final SimpleLink link12 = new SimpleLinkImpl("conjoined1", "conjoined1-label", "conjoined1-title", null,
                "conjoined", null, "url1", null);
        final SimpleLink link13 = createLink("13");
        final List<SimpleLink> links2 = CollectionBuilder.list(link11, link12, link13);

        final SimpleLinkSection section1 = createSection("1");
        final SimpleLinkSection section2 = createSection("2");
        final SimpleLinkSection section3 = createSection("3");
        final SimpleLinkSection section4 = createSection("4");
        final List<SimpleLinkSection> sections = CollectionBuilder.list(section1, section2, section3, section4);

        when(linkManager.getLinksForSection(section1.getId(), user, null)).thenReturn(links1);

        when(appProps.getDefaultBackedString("ops.bar.group.size.id-group")).thenReturn(null);
        // one less, but should return 5
        when(appProps.getDefaultBackedString("ops.bar.group.size")).thenReturn("4");

        when(linkManager.getSectionsForLocation(group.getId(), user, null)).thenReturn(sections);
        when(linkManager.getLinksForSection(section2.getId(), user, null)).thenReturn(links2);

        final IssueOperationsBarUtil opsBarUtil = new IssueOperationsBarUtil(null, user, linkManager, appProps, issueManager, permissionManager, null);

        List<SimpleLink> list = opsBarUtil.getNonPromotedLinksForSection(group, section1);
        assertEquals(0, list.size());

        // It should be cached now.  Shouldn't hit link manager
        list = opsBarUtil.getNonPromotedLinksForSection(group, section1);
        assertEquals(0, list.size());
    }

    @Test
    public void testGetNonPromotedLinksForSection() {
        final SimpleLinkSection group = createSection("group");

        final SimpleLink link1 = createLink("1");
        final SimpleLink link2 = createLink("2");
        final SimpleLink link3 = createLink("3");
        final List<SimpleLink> links1 = CollectionBuilder.newBuilder(link1, link2, link3).asMutableList();

        final SimpleLinkSection section1 = createSection("1");
        final SimpleLinkSection section2 = createSection("2");
        final SimpleLinkSection section3 = createSection("3");
        final SimpleLinkSection section4 = createSection("4");
        final List<SimpleLinkSection> sections = CollectionBuilder.list(section1, section2, section3, section4);

        when(linkManager.getLinksForSection(section1.getId(), user, null)).thenReturn(links1);

        when(appProps.getDefaultBackedString("ops.bar.group.size.id-group")).thenReturn("1");

        when(linkManager.getSectionsForLocation(group.getId(), user, null)).thenReturn(sections);

        final IssueOperationsBarUtil opsBarUtil = new IssueOperationsBarUtil(null, user, linkManager, appProps, issueManager, permissionManager, null);

        List<SimpleLink> list = opsBarUtil.getNonPromotedLinksForSection(group, section1);
        assertEquals(CollectionBuilder.list(link2, link3), list);

        // It should be cached now.  Shouldn't hit link manager
        list = opsBarUtil.getNonPromotedLinksForSection(group, section1);
        assertEquals(CollectionBuilder.list(link2, link3), list);
    }

    @Test
    public void testGetPrimaryOperationLinks() {
        testGetPrimaryOperationLinks(true, true, user, (actualLinks, editLink, commentLink, loginLink) -> assertEquals(ImmutableList.of(editLink, commentLink), actualLinks));

        testGetPrimaryOperationLinks(true, false, user, (actualLinks, editLink, commentLink, loginLink) -> assertEquals(ImmutableList.of(editLink), actualLinks));

        testGetPrimaryOperationLinks(false, true, user, (actualLinks, editLink, commentLink, loginLink) -> assertEquals(ImmutableList.of(commentLink), actualLinks));

        testGetPrimaryOperationLinks(false, false, null, (actualLinks, editLink, commentLink, loginLink) -> assertEquals(ImmutableList.of(loginLink), actualLinks));
    }

    private void testGetPrimaryOperationLinks(boolean expectedIsEditable, boolean expectedHasCommentPermission, ApplicationUser user, GetPrimaryOperationLinksAsserter asserter) {
        SimpleLink editLink = new SimpleLinkImpl("edit-issue", null, null, null, null, null, "", null);
        SimpleLink commentLink = new SimpleLinkImpl("comment-issue", null, null, null, null, null, "", null);
        final SimpleLink loginLink = new SimpleLinkImpl("ops-login-lnk", null, null, null, null, null, "", null);

        SimpleLink link1 = createLink("1");
        SimpleLink link2 = createLink("2");

        reset(linkManager, issueManager, permissionManager);

        ApplicationUser directoryUser = (user == null) ? null : user;

        if (!expectedIsEditable && !expectedHasCommentPermission) {
            when(linkManager.getSectionsForLocation("view.issue.opsbar", directoryUser, null)).thenReturn(ImmutableList.of());
        } else {
            when(linkManager.getLinksForSection("operations-top-level", directoryUser, null)).thenReturn(ImmutableList.of(editLink, commentLink, link1, link2));
        }

        when(issueManager.isEditable(null, directoryUser)).thenReturn(expectedIsEditable);
        when(permissionManager.hasPermission(Permissions.COMMENT_ISSUE, (Issue) null, user)).thenReturn(expectedHasCommentPermission);

        IssueOperationsBarUtil opsBarUtil = new IssueOperationsBarUtil(null, user, linkManager, appProps, issueManager, permissionManager, null) {
            @Override
            SimpleLink createLoginLink() {
                return loginLink;
            }
        };

        List<SimpleLink> links = opsBarUtil.getPrimaryOperationLinks(null);

        asserter.doAssert(links, editLink, commentLink, loginLink);
    }

    private interface GetPrimaryOperationLinksAsserter {
        void doAssert(List<SimpleLink> actualLinks, SimpleLink editLink, SimpleLink commentLink, SimpleLink loginLink);
    }

    private SimpleLink createLink(String id) {
        return new SimpleLinkImpl("id-" + id, "label-" + id, "title" + id, null, "style" + id, null, "a url -" + id, null);
    }

    private SimpleLinkSection createSection(String id) {
        return new SimpleLinkSectionImpl("id-" + id, "label-" + id, "title" + id, null, "style" + id, null);
    }
}
