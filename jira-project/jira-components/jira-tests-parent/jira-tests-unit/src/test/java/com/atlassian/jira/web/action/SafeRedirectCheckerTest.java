package com.atlassian.jira.web.action;

import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.util.velocity.VelocityRequestContext;
import com.atlassian.jira.util.velocity.VelocityRequestContextFactory;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

/**
 * Tests for SafeRedirectCheckerTest.
 *
 * @since v4.3
 */
public class SafeRedirectCheckerTest {
    private final String CANONICAL_BASE_URL = "http://jira.atlassian.com";

    private final String RELATIVE_URL = "./Action.jspa?selectedId=4";
    private final String BROWSE_URL = "/browse/HSP-3";
    private final String RELATIVE_NORMALIZED_URL = "Action.jspa?selectedId=4";
    private final String OFFSITE_HTTP_URL = "http://xyz.com/";
    private final String ISSUE_NAV_RELATIVE_URL = "/browse/TEST-1?jql=project+%3D+HSP";
    private final String ABSOLUTE_URL_NO_SCHEME = "//www.google.com";

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @Mock
    VelocityRequestContext reqContext;
    @Mock
    VelocityRequestContextFactory factory;
    @Mock
    ApplicationProperties applicationProperties;

    @Test
    public void testOffsiteRedirectIsNotAllowed() throws Exception {
        SafeRedirectChecker checker = new SafeRedirectChecker(new RedirectSanitiserImpl(factory));
        assertFalse(checker.canRedirectTo(OFFSITE_HTTP_URL));
    }

    @Test
    public void testRedirectToSameDomainUsingAbsoluteUrlShouldBeAllowed() throws Exception {
        SafeRedirectChecker checker = new SafeRedirectChecker(new RedirectSanitiserImpl(factory));
        assertTrue(checker.canRedirectTo(CANONICAL_BASE_URL + "/foo/bar"));
    }

    @Test
    public void testRedirectToSameDomainUsingRelativeUrlIsAllowed() throws Exception {
        SafeRedirectChecker checker = new SafeRedirectChecker(new RedirectSanitiserImpl(factory));
        assertTrue(checker.canRedirectTo(BROWSE_URL));
    }

    @Test
    public void testRedirectToSameDomainUsingNormalisedRelativeUrlIsAllowed() throws Exception {
        SafeRedirectChecker checker = new SafeRedirectChecker(new RedirectSanitiserImpl(factory));
        assertTrue(checker.canRedirectTo(RELATIVE_NORMALIZED_URL));
    }

    @Test
    public void testRedirectToSameDomainUsingActionNameIsAllowed() throws Exception {
        SafeRedirectChecker checker = new SafeRedirectChecker(new RedirectSanitiserImpl(factory));
        assertTrue(checker.canRedirectTo(RELATIVE_URL));
    }

    @Test
    public void testRedirectToAbsoluteUriUsingPort() throws Exception {
        SafeRedirectChecker checker = new SafeRedirectChecker(new RedirectSanitiserImpl(factory));
        assertTrue(checker.canRedirectTo(CANONICAL_BASE_URL + ":80/foo/bar"));
    }

    @Test
    public void testRedirectToIssueNavigatorShouldBeAllowed() throws Exception {
        SafeRedirectChecker checker = new SafeRedirectChecker(new RedirectSanitiserImpl(factory));
        assertTrue(checker.canRedirectTo(ISSUE_NAV_RELATIVE_URL));
    }

    @Test
    public void testRedirectToAbsoluteUrlsWithNoSchemeIsNotAllowed() {
        SafeRedirectChecker checker = new SafeRedirectChecker(new RedirectSanitiserImpl(factory));
        assertFalse(checker.canRedirectTo(ABSOLUTE_URL_NO_SCHEME));
    }

    @Before
    public void setUp() {
        when(reqContext.getCanonicalBaseUrl()).thenReturn(CANONICAL_BASE_URL);
        when(factory.getJiraVelocityRequestContext()).thenReturn(reqContext);
        when(applicationProperties.getEncoding()).thenReturn("UTF-8");
    }
}
