package com.atlassian.jira.avatar;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Unit tests for {@link Avatar.Size#fromString(String)} method.
 *
 * @since v6.0
 */
public class TestAvatarSizeFromString {
    @Test
    public void testEmptyParamReturnsDefaultSize() {
        assertEquals(Avatar.Size.defaultSize(), Avatar.Size.fromString(""));
    }

    @Test
    public void testUnmappableValueFallsBackToDefaultSize() {
        assertEquals(Avatar.Size.defaultSize(), Avatar.Size.fromString("trololololol"));
        assertEquals(Avatar.Size.defaultSize(), Avatar.Size.fromString("size"));
        assertEquals(Avatar.Size.defaultSize(), Avatar.Size.fromString("0"));
        assertEquals(Avatar.Size.defaultSize(), Avatar.Size.fromString("15"));
        assertEquals(Avatar.Size.defaultSize(), Avatar.Size.fromString("-32"));
        assertEquals(Avatar.Size.defaultSize(), Avatar.Size.fromString("16px"));
    }

    @Test
    public void testCanProvideIntegerValueForSize() {
        assertEquals(Avatar.Size.SMALL, Avatar.Size.fromString("16"));
    }

    @Test
    public void testXSmallReturns16px() {
        Avatar.Size size = Avatar.Size.fromString("xsmall");
        assertEquals(Avatar.Size.SMALL, size);
        assertEquals(16, size.getPixels());
    }

    @Test
    public void testSmallReturns24pxNot16px() {
        assertEquals(Avatar.Size.NORMAL, Avatar.Size.fromString("small"));
        assertEquals(24, Avatar.Size.fromString("small").getPixels());
        assertEquals(Avatar.Size.NORMAL, Avatar.Size.fromString("SMALL"));
    }

    @Test
    public void testTShirtSizeAbbreviationsReturnSensibleResults() {
        assertEquals("XS should be very small", Avatar.Size.SMALL, Avatar.Size.fromString("xs"));
        assertEquals("S should be small", Avatar.Size.NORMAL, Avatar.Size.fromString("s"));
        assertEquals("M should be a bit bigger than N", Avatar.Size.MEDIUM, Avatar.Size.fromString("m"));
        assertEquals("L should be large", Avatar.Size.LARGE, Avatar.Size.fromString("l"));
        assertEquals("XL should be a bit bigger than L", Avatar.Size.XLARGE, Avatar.Size.fromString("xl"));
        assertEquals("XXL should be much larger than L", Avatar.Size.XXLARGE, Avatar.Size.fromString("xxl"));
    }

    @Test
    public void testNormalSizeAbbreviation() {
        assertEquals("N should be the same as S", Avatar.Size.NORMAL, Avatar.Size.fromString("n"));
    }

    @Test
    public void testRetinaResolutions() {
        assertEquals(Avatar.Size.RETINA_XXLARGE, Avatar.Size.fromString("xxlarge@2x"));
        assertEquals(Avatar.Size.RETINA_XXXLARGE, Avatar.Size.fromString("xxxlarge@2x"));
        assertEquals(Avatar.Size.MEDIUM, Avatar.Size.fromString("medium"));
        assertEquals(Avatar.Size.XLARGE, Avatar.Size.fromString("medium@2x"));
        assertEquals(Avatar.Size.XXLARGE, Avatar.Size.fromString("medium@3x"));
    }
}
