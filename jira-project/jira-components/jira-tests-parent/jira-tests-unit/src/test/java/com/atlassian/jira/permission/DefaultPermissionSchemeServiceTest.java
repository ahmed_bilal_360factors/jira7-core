package com.atlassian.jira.permission;

import com.atlassian.jira.bc.ServiceOutcome;
import com.atlassian.jira.bc.ServiceOutcomeImpl;
import com.atlassian.jira.bc.ServiceResult;
import com.atlassian.jira.bc.project.ProjectAction;
import com.atlassian.jira.bc.project.ProjectService;
import com.atlassian.jira.mock.i18n.MockI18nHelper;
import com.atlassian.jira.permission.data.PermissionGrantImpl;
import com.atlassian.jira.permission.data.PermissionSchemeImpl;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.scheme.Scheme;
import com.atlassian.jira.scheme.SchemeEntity;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.UserKeyService;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.AnswerWith;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.ErrorCollection.Reason;
import com.atlassian.jira.util.I18nHelper;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.ofbiz.core.entity.GenericEntityException;
import org.ofbiz.core.entity.GenericValue;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import static com.atlassian.fugue.Iterables.first;
import static com.atlassian.jira.matchers.ErrorCollectionMatchers.containsFieldError;
import static com.atlassian.jira.matchers.ErrorCollectionMatchers.containsSystemError;
import static com.atlassian.jira.matchers.ErrorCollectionMatchers.isEmpty;
import static com.atlassian.jira.matchers.IterableMatchers.hasItems;
import static com.atlassian.jira.matchers.IterableMatchers.iterableWithSize;
import static com.atlassian.jira.permission.PermissionGrantInput.newGrant;
import static com.atlassian.jira.permission.PermissionHolder.holder;
import static com.atlassian.jira.permission.PermissionSchemeInput.builder;
import static com.atlassian.jira.permission.PermissionSchemeTestConstants.EMPTY_PERMISSION_SCHEME;
import static com.atlassian.jira.permission.PermissionSchemeTestConstants.EMPTY_SCHEME;
import static com.atlassian.jira.permission.PermissionSchemeTestConstants.PERMISSION_SCHEME_1;
import static com.atlassian.jira.permission.PermissionSchemeTestConstants.PERMISSION_SCHEME_1_DATA;
import static com.atlassian.jira.permission.PermissionSchemeTestConstants.SCHEME_1;
import static com.atlassian.jira.permission.PermissionSchemeTestConstants.USERKEY;
import static com.atlassian.jira.permission.PermissionSchemeTestConstants.USERNAME;
import static com.atlassian.jira.permission.PermissionSchemeTestConstants.PERMISSION_SCHEME_WITH_USER_KEY;
import static com.atlassian.jira.permission.PermissionSchemeTestConstants.PERMISSION_SCHEME_WITH_USER_KEY_DATA;
import static com.atlassian.jira.permission.PermissionSchemeTestConstants.PERMISSION_SCHEME_WITH_USER_NAME_DATA;
import static com.atlassian.jira.permission.ProjectPermissions.ADD_COMMENTS;
import static com.atlassian.jira.util.ErrorCollection.Reason.FORBIDDEN;
import static com.atlassian.jira.util.ErrorCollections.create;
import static com.atlassian.jira.util.ErrorCollections.empty;
import static com.atlassian.jira.util.ErrorCollections.validationError;
import static java.lang.Boolean.TRUE;
import static java.util.Collections.singleton;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyCollection;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.CALLS_REAL_METHODS;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public final class DefaultPermissionSchemeServiceTest {
    private static final AtomicLong SEQUENCE = new AtomicLong(10000L);

    private static final ApplicationUser ADMIN = new MockApplicationUser("admin");
    private static final ApplicationUser FRED = new MockApplicationUser("fred");
    private static final ErrorCollection VALIDATION_ERROR = validationError("some field", "some message");

    @Mock
    private PermissionSchemeManager permissionSchemeManager;
    @Mock
    private GlobalPermissionManager globalPermissions;
    @Mock
    private PermissionGrantValidator permissionGrantValidator;
    @Mock
    private ProjectService projectService;
    @Mock
    private UserKeyService userKeyService;
    @Mock
    private UserManager userManager;

    private I18nHelper i18n = new MockI18nHelper();

    private PermissionSchemeRepresentationConverter converter = new PermissionSchemeRepresentationConverter();

    private PermissionSchemeService service;

    @Before
    public void setUp() throws Exception {
        service = new DefaultPermissionSchemeService(permissionSchemeManager, globalPermissions, permissionGrantValidator, converter, projectService, i18n, userKeyService, userManager);

        when(globalPermissions.hasPermission(GlobalPermissionKey.ADMINISTER, ADMIN)).thenReturn(TRUE);
        when(globalPermissions.hasPermission(GlobalPermissionKey.ADMINISTER, FRED)).thenReturn(Boolean.FALSE);
        when(permissionGrantValidator.validateGrant(any(ApplicationUser.class), any(PermissionGrantInput.class))).thenReturn(empty());
        when(permissionGrantValidator.validateGrants(any(ApplicationUser.class), any(Iterable.class))).thenAnswer(CALLS_REAL_METHODS);
        when(permissionSchemeManager.createSchemeAndEntities(any(Scheme.class))).then(AnswerWith.firstParameterMapped(new Function<Scheme, Scheme>() {
            @Override
            public Scheme apply(final Scheme input) {
                return new Scheme(SEQUENCE.getAndIncrement(), input.getType(), input.getName(), input.getDescription(),
                        ImmutableList.copyOf(Iterables.transform(input.getEntities(), new Function<SchemeEntity, SchemeEntity>() {
                            @Override
                            public SchemeEntity apply(final SchemeEntity input) {
                                return new SchemeEntity(
                                        SEQUENCE.getAndIncrement(),
                                        input.getType(),
                                        input.getParameter(),
                                        input.getEntityTypeId(),
                                        input.getTemplateId(),
                                        input.getSchemeId()
                                );
                            }
                        })));
            }
        }));
        when(permissionSchemeManager.getDefaultSchemeObject()).thenReturn(new Scheme(0L, "permission", "Default permission scheme", Collections.<SchemeEntity>emptyList()));
        when(permissionSchemeManager.getProjects(any(Scheme.class))).thenReturn(Collections.<Project>emptyList());
        for (Scheme scheme : PermissionSchemeTestConstants.schemeEquivalency.keySet()) {
            when(permissionSchemeManager.getSchemeObject(scheme.getId())).thenReturn(scheme);
        }
        when(permissionSchemeManager.getSchemeObjects()).thenReturn(ImmutableList.copyOf(PermissionSchemeTestConstants.schemeEquivalency.keySet()));
    }

    @Test
    public void creatingPermissionSchemeFailsWhenInputIsInvalid() {
        when(permissionGrantValidator.validateGrant(ADMIN, PermissionSchemeTestConstants.PERMISSION_SCHEME_1_DATA.getPermissions().get(0))).thenReturn(VALIDATION_ERROR);
        assertCreationFails(VALIDATION_ERROR);
    }

    @Test
    public void creatingPermissionSchemeFailsWhenSchemeWithGivenNameAlreadyExists() throws GenericEntityException {
        when(permissionSchemeManager.schemeExists(PermissionSchemeTestConstants.PERMISSION_SCHEME_1.getName())).thenReturn(TRUE);
        assertCreationFails(create("admin.schemes.permissions.scheme.already.exists [" + PermissionSchemeTestConstants.PERMISSION_SCHEME_1.getName() + "]", Reason.VALIDATION_FAILED));
    }

    private void assertCreationFails(ErrorCollection expectedError) {
        ServiceOutcome<PermissionScheme> result = service.createPermissionScheme(ADMIN, PermissionSchemeTestConstants.PERMISSION_SCHEME_1_DATA);

        assertThat(result.getErrorCollection(), equalTo(expectedError));
        verify(permissionSchemeManager, times(0)).createSchemeAndEntities(any(Scheme.class));
        verify(permissionSchemeManager, times(0)).createSchemeObject(anyString(), anyString());
    }

    @Test
    public void creatingPermissionSchemeInvokesTheManager() throws GenericEntityException {
        for (PermissionSchemeInput permissionScheme : PermissionSchemeTestConstants.SCHEME_DATAS) {
            ServiceOutcome<PermissionScheme> result = service.createPermissionScheme(ADMIN, permissionScheme);

            assertThat(PermissionSchemeInput.builder(result.get()).build(), equalTo(permissionScheme));
            verify(permissionSchemeManager).createSchemeAndEntities(converter.scheme(permissionScheme));
        }
    }

    @Test
    public void nothingIsUpdatedWhenEntityToCreateIsInvalid() throws GenericEntityException {
        PermissionGrantInput invalidEntity = PermissionGrantInput.newGrant(holder(JiraPermissionHolderType.ASSIGNEE), ADD_COMMENTS);

        assumeSchemeIsStored(EMPTY_PERMISSION_SCHEME);

        when(permissionGrantValidator.validateGrant(ADMIN, invalidEntity)).thenReturn(VALIDATION_ERROR);

        service.updatePermissionScheme(ADMIN, 42L, builder(EMPTY_PERMISSION_SCHEME).addPermission(invalidEntity).build());

        verify(permissionSchemeManager, times(0)).updateScheme(any(Scheme.class));
    }

    @Test
    public void updateCreatesNewEntitiesDeletesOldOnesAndLeavesNotChangedAlone() throws GenericEntityException {
        PermissionScheme originalScheme = new PermissionSchemeImpl(
                3L, "nameOrg", "descOrg", ImmutableList.<PermissionGrant>of(
                new PermissionGrantImpl(1L, holder(JiraPermissionHolderType.ASSIGNEE), ProjectPermissions.ASSIGN_ISSUES),
                new PermissionGrantImpl(2L, holder(JiraPermissionHolderType.ANYONE), ProjectPermissions.ADMINISTER_PROJECTS)
        ));

        PermissionSchemeInput newScheme = builder("nameNew")
                .setName("nameNew")
                .setDescription("descNew")
                .setPermissions(ImmutableList.of(
                        newGrant(holder(JiraPermissionHolderType.ASSIGNEE), ProjectPermissions.ASSIGN_ISSUES),
                        newGrant(holder(JiraPermissionHolderType.PROJECT_LEAD), ProjectPermissions.ADMINISTER_PROJECTS)
                ))
                .build();

        GenericValue schemeGeneric = assumeSchemeIsStored(originalScheme);

        service.updatePermissionScheme(ADMIN, 3L, newScheme);
        verify(permissionSchemeManager).updateScheme(converter.scheme(new PermissionSchemeImpl(3L, "nameNew", "descNew")));
        verify(permissionSchemeManager).deleteEntities(argThat(iterableWithSize(1, Long.class)));
        verify(permissionSchemeManager).deleteEntities(argThat(hasItems(Long.class, 2L)));
        verify(permissionSchemeManager, times(1)).deleteEntities(anyCollection());
        verify(permissionSchemeManager).createSchemeEntity(schemeGeneric, converter.schemeEntity(newScheme.getPermissions().get(1), 3L));
        verify(permissionSchemeManager, times(1)).createSchemeEntity(any(GenericValue.class), any(SchemeEntity.class));
    }

    @Test
    public void errorIsReturnedWhenUpdatingSchemeWithNewInvalidGrant() throws GenericEntityException {
        PermissionGrantInput invalidGrant = PERMISSION_SCHEME_1_DATA.getPermissions().get(0);
        when(permissionGrantValidator.validateGrant(ADMIN, invalidGrant)).thenReturn(VALIDATION_ERROR);
        assumeSchemeIsStored(EMPTY_PERMISSION_SCHEME);
        assertThat(service.updatePermissionScheme(ADMIN, EMPTY_PERMISSION_SCHEME.getId(), builder(EMPTY_PERMISSION_SCHEME).addPermission(invalidGrant).build()).getErrorCollection(),
                containsFieldError("some field", "some message"));
    }

    @Test
    public void onlyNewlyCreatedGrantsAreValidated() throws GenericEntityException {
        PermissionGrantInput invalidGrant = PERMISSION_SCHEME_1_DATA.getPermissions().get(0);
        PermissionGrantInput validGrant = PERMISSION_SCHEME_1_DATA.getPermissions().get(1);
        when(permissionGrantValidator.validateGrant(ADMIN, invalidGrant)).thenReturn(VALIDATION_ERROR);

        PermissionScheme schemeWithInvalidGrant = new PermissionSchemeImpl(1L, "name", "desc", singleton(first(PERMISSION_SCHEME_1.getPermissions()).get()));
        assumeSchemeIsStored(schemeWithInvalidGrant);

        assertThat(service.updatePermissionScheme(ADMIN, schemeWithInvalidGrant.getId(), builder(schemeWithInvalidGrant).addPermission(validGrant).build()).getErrorCollection(),
                isEmpty());
    }

    @Test
    public void invalidGrantsCanBeRemoveWithAnUpdate() throws GenericEntityException {
        PermissionGrantInput invalidGrant = PERMISSION_SCHEME_1_DATA.getPermissions().get(0);
        PermissionGrantInput validGrant = PERMISSION_SCHEME_1_DATA.getPermissions().get(1);
        when(permissionGrantValidator.validateGrant(ADMIN, invalidGrant)).thenReturn(VALIDATION_ERROR);

        PermissionScheme schemeWithInvalidGrant = new PermissionSchemeImpl(1L, "name", "desc", singleton(first(PERMISSION_SCHEME_1.getPermissions()).get()));
        assumeSchemeIsStored(schemeWithInvalidGrant);

        assertThat(service.updatePermissionScheme(ADMIN, schemeWithInvalidGrant.getId(), builder(schemeWithInvalidGrant).setPermissions(singleton(validGrant)).build()).getErrorCollection(),
                isEmpty());
    }

    private GenericValue assumeSchemeIsStored(final PermissionScheme schemeInDb) throws GenericEntityException {
        GenericValue schemeGeneric = mock(GenericValue.class);
        when(permissionSchemeManager.getSchemeObject(schemeInDb.getId())).thenReturn(converter.scheme(schemeInDb));
        when(permissionSchemeManager.getScheme(schemeInDb.getId())).thenReturn(schemeGeneric);
        return schemeGeneric;
    }

    @Test
    public void errorIsReturnedWhenDeletingNonExistentScheme() {
        ServiceResult result = service.deletePermissionScheme(ADMIN, 555L);
        assertThat(result.getErrorCollection(), containsSystemError("admin.schemes.permissions.validation.scheme.does.not.exist [555]"));
    }

    @Test
    public void deletingSchemeInvokesTheManager() throws GenericEntityException {
        ServiceResult result = service.deletePermissionScheme(ADMIN, PermissionSchemeTestConstants.PERMISSION_SCHEME_1.getId());
        verify(permissionSchemeManager).deleteScheme(PermissionSchemeTestConstants.PERMISSION_SCHEME_1.getId());
        assertTrue(result.isValid());
    }

    @Test
    public void errorIsReturnedWhenTryingToDeleteDefaultPermissionScheme() throws GenericEntityException {
        when(permissionSchemeManager.getDefaultSchemeObject()).thenReturn(new Scheme(0L, "permission", "a", Collections.<SchemeEntity>emptyList()));
        ServiceResult result = service.deletePermissionScheme(ADMIN, 0L);
        assertThat(result.getErrorCollection(), containsSystemError("admin.schemes.permissions.cannot.delete.default.scheme"));
    }

    @Test
    public void errorIsReturnedWhenTryingToDeletePermissionSchemeWithAssociatedProjects() throws GenericEntityException {
        when(permissionSchemeManager.getProjects(PermissionSchemeTestConstants.schemeEquivalency.inverse().get(PermissionSchemeTestConstants.PERMISSION_SCHEME_1))).thenReturn(ImmutableList.of(mock(Project.class)));
        ServiceResult result = service.deletePermissionScheme(ADMIN, PermissionSchemeTestConstants.PERMISSION_SCHEME_1.getId());
        assertThat(result.getErrorCollection(), containsSystemError("admin.schemes.permissions.cannot.delete.with.projects [" + PermissionSchemeTestConstants.PERMISSION_SCHEME_1.getId() + "]"));
    }

    @Test
    public void errorIsReturnedWhenTryingToAssignNonExistentSchemeToProject() {
        when(projectService.getProjectByIdForAction(ADMIN, 42L, ProjectAction.VIEW_PROJECT)).thenReturn(new ProjectService.GetProjectResult(mock(Project.class)));
        ServiceResult result = service.assignPermissionSchemeToProject(ADMIN, 555L, 42L);
        assertThat(result.getErrorCollection(), containsSystemError("admin.schemes.permissions.validation.scheme.does.not.exist [555]"));
    }

    @Test
    public void errorIsReturnedWhenTryingToAssignSchemeToNonExistingProject() {
        when(projectService.getProjectByIdForAction(ADMIN, 42L, ProjectAction.VIEW_PROJECT)).thenReturn(new ProjectService.GetProjectResult(VALIDATION_ERROR));
        ServiceResult serviceResult = service.assignPermissionSchemeToProject(ADMIN, 1L, 42L);
        assertThat(serviceResult.getErrorCollection(), equalTo(VALIDATION_ERROR));
    }

    @Test
    public void schemeIsAssignedToProject() {
        Project project = new MockProject(42L);
        when(projectService.getProjectByIdForAction(ADMIN, project.getId(), ProjectAction.VIEW_PROJECT)).thenReturn(new ProjectService.GetProjectResult(project));
        ServiceResult serviceResult = service.assignPermissionSchemeToProject(ADMIN, PERMISSION_SCHEME_1.getId(), 42L);
        assertTrue(serviceResult.isValid());
        verify(permissionSchemeManager).removeSchemesFromProject(project);
        verify(permissionSchemeManager).addSchemeToProject(project, SCHEME_1);
    }

    @Test
    public void errorIsReturnedWhenTryingToGetSchemeAssignedToNonExistingProject() {
        when(projectService.getProjectByIdForAction(FRED, 42L, ProjectAction.EDIT_PROJECT_CONFIG)).thenReturn(new ProjectService.GetProjectResult(VALIDATION_ERROR));
        ServiceResult serviceResult = service.getSchemeAssignedToProject(FRED, 42L);
        assertThat(serviceResult.getErrorCollection(), equalTo(VALIDATION_ERROR));
    }

    @Test
    public void schemeAssignedToProjectIsReturned() {
        Project project = projectThatFredAdministers();
        when(permissionSchemeManager.getSchemeFor(project)).thenReturn(SCHEME_1);
        ServiceOutcome<PermissionScheme> result = service.getSchemeAssignedToProject(FRED, project.getId());
        assertThat(result.get(), Matchers.equalTo(PERMISSION_SCHEME_1));
    }

    @Test
    public void defaultSchemeIsReturnedIfNoSchemeIsAssignedToProject() {
        Project project = projectThatFredAdministers();
        when(permissionSchemeManager.getDefaultSchemeObject()).thenReturn(SCHEME_1);
        ServiceOutcome<PermissionScheme> result = service.getSchemeAssignedToProject(FRED, project.getId());
        assertThat(result.get(), Matchers.equalTo(PERMISSION_SCHEME_1));
    }

    @Test
    public void schemeIsReturnedIfUserHasAccessToAdministerProjectsUsingThisScheme() {
        Project project1 = projectThatFredAdministers();
        when(permissionSchemeManager.getProjects(SCHEME_1)).thenReturn(ImmutableList.of(project1));

        ServiceOutcome<PermissionScheme> result = service.getPermissionScheme(FRED, SCHEME_1.getId());
        assertTrue(result.isValid());
        assertThat(result.get(), equalTo(PERMISSION_SCHEME_1));
    }

    @Test
    public void schemeWithUserSecurityGrantShouldReturnUserNameNotUserKey() throws GenericEntityException {
        assumeSchemeIsStored(PERMISSION_SCHEME_WITH_USER_KEY);
        ApplicationUser user = new MockApplicationUser(USERNAME);
        when(userManager.getUserByKeyEvenWhenUnknown(USERKEY)).thenReturn(user);

        ServiceOutcome<PermissionScheme> result = service.getPermissionScheme(ADMIN, PERMISSION_SCHEME_WITH_USER_KEY.getId());

        assertTrue(result.isValid());
        assertThat(result.get().getPermissions().iterator().next().getHolder().getParameter().get(), equalTo(USERNAME));
    }

    @Test
    public void allSchemesUsedInProjectsTheUserAdministersAreReturned() {
        Project project1 = projectThatFredAdministers();
        Project project2 = projectThatFredAdministers();

        when(permissionSchemeManager.getProjects(SCHEME_1)).thenReturn(ImmutableList.of(project1));
        when(permissionSchemeManager.getProjects(EMPTY_SCHEME)).thenReturn(ImmutableList.of(project2));

        when(projectService.getAllProjectsForAction(FRED, ProjectAction.EDIT_PROJECT_CONFIG)).thenReturn(new ServiceOutcomeImpl<List<Project>>(empty(), ImmutableList.of(project1, project2)));

        ServiceOutcome<? extends List<PermissionScheme>> result = service.getPermissionSchemes(FRED);
        assertTrue(result.isValid());
        assertThat(result.get(), hasSize(2));
        assertThat(result.get(), Matchers.hasItems(PERMISSION_SCHEME_1, EMPTY_PERMISSION_SCHEME));
    }

    @Test
    public void nonAdminUserDoesNotHavePermissionToEditPermissionSchemes() {
        assertPermissionDenied(service.createPermissionScheme(FRED, PermissionSchemeTestConstants.PERMISSION_SCHEME_1_DATA));
        assertPermissionDenied(service.deletePermissionScheme(FRED, 42L));
        assertPermissionDenied(service.updatePermissionScheme(FRED, 42L, PermissionSchemeTestConstants.PERMISSION_SCHEME_1_DATA));
        assertPermissionDenied(service.assignPermissionSchemeToProject(FRED, 42L, 43L));
    }

    @Test
    public void creatingPermissionSchemePersistsUserkey() {
        when(userKeyService.getKeyForUsername(USERNAME)).thenReturn(USERKEY);
        ServiceOutcome<PermissionScheme> result = service.createPermissionScheme(ADMIN, PERMISSION_SCHEME_WITH_USER_NAME_DATA);

        assertThat("Created permission scheme should have userkey as a parameter rather than username",
                PermissionSchemeInput.builder(result.get()).build(), equalTo(PERMISSION_SCHEME_WITH_USER_KEY_DATA));
    }

    @Test
    public void creatingPermissionSchemeDoesNotLookUpUserkeyWhenNoUserGrant() {
        ServiceOutcome<PermissionScheme> result = service.createPermissionScheme(ADMIN, PERMISSION_SCHEME_1_DATA);

        assertTrue("The create should succeed", result.isValid());
        verify(userKeyService, times(0)).getKeyForUsername(USERNAME);
    }

    @Test
    public void updatingPermissionSchemePersistsUserkey() throws GenericEntityException {
        when(userKeyService.getKeyForUsername(USERNAME)).thenReturn(USERKEY);
        assumeSchemeIsStored(EMPTY_PERMISSION_SCHEME);

        ServiceOutcome<PermissionScheme> result = service.updatePermissionScheme(ADMIN, EMPTY_PERMISSION_SCHEME.getId(), PERMISSION_SCHEME_WITH_USER_NAME_DATA);

        assertTrue("The update should succeed", result.isValid());
        verify(userKeyService).getKeyForUsername(USERNAME);
    }

    @Test
    public void updatingPermissionSchemeDoesNotLookUpUserkeyWhenNoUserGrant() throws GenericEntityException {
        assumeSchemeIsStored(EMPTY_PERMISSION_SCHEME);

        ServiceOutcome<PermissionScheme> result = service.updatePermissionScheme(ADMIN, EMPTY_PERMISSION_SCHEME.getId(), PERMISSION_SCHEME_1_DATA);

        assertTrue("The update should succeed", result.isValid());
        verify(userKeyService, times(0)).getKeyForUsername(USERNAME);
    }

    private Project projectThatFredAdministers() {
        Project project = new MockProject(SEQUENCE.incrementAndGet(), "PROJ" + SEQUENCE.get());
        when(projectService.getProjectByIdForAction(FRED, project.getId(), ProjectAction.EDIT_PROJECT_CONFIG)).thenReturn(new ProjectService.GetProjectResult(project));
        return project;
    }

    private void assertPermissionDenied(final ServiceResult result) {
        assertThat(result.isValid(), equalTo(Boolean.FALSE));
        assertThat(result.getErrorCollection().getReasons(), hasSize(1));
        assertThat(result.getErrorCollection().getReasons(), hasItem(FORBIDDEN));
    }
}
