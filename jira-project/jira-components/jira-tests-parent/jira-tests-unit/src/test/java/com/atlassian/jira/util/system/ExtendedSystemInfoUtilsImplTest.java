package com.atlassian.jira.util.system;

import com.atlassian.jira.bc.license.JiraLicenseService;
import com.atlassian.jira.cache.HashRegistryCache;
import com.atlassian.jira.cluster.ClusterManager;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.LocaleManager;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.config.properties.JiraProperties;
import com.atlassian.jira.mock.component.MockComponentWorker;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.ofbiz.OfBizFactory;
import com.atlassian.jira.security.auth.trustedapps.TrustedApplicationService;
import com.atlassian.jira.service.ServiceManager;
import com.atlassian.jira.service.services.analytics.JiraStartStopAnalyticHelper;
import com.atlassian.jira.upgrade.UpgradeService;
import com.atlassian.jira.util.BuildUtilsInfo;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.plugin.PluginAccessor;
import com.google.common.collect.ImmutableMap;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.mockito.stubbing.Answer;

import java.util.Map;

import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class ExtendedSystemInfoUtilsImplTest {
    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private SystemInfoUtils systemInfoUtils;

    @Mock
    private ServiceManager serviceManager;

    @Mock
    private PluginAccessor accessor;

    @Mock
    private ApplicationProperties applicationProperties;

    @Mock
    private TrustedApplicationService trustedApplicationService;

    @Mock
    private OfBizDelegator delegator;

    @Mock
    private I18nHelper i18nHelper;

    @Mock
    private HashRegistryCache cache;

    @Mock
    private LocaleManager localeManager;

    @Mock
    private JiraLicenseService jiraLicenseService;

    @Mock
    private BuildUtilsInfo buildInfo;

    @Mock
    private UpgradeService upgradeService;

    @Mock
    private JiraProperties properties;

    @Mock
    private ClusterManager clusterManager;

    @Mock
    private JiraStartStopAnalyticHelper analyticsHelper;

    @InjectMocks
    private ExtendedSystemInfoUtilsImpl extendedSystemInfoUtils;

    @Test
    public void getSystemPropertiesFormattedUsesSanitisedProperties() {
        //given
        when(properties.getSanitisedProperties()).thenReturn(ImmutableMap.of("os.name", "Java",
                "example.path", "jack:peter", "other", "property"));

        //when
        final Map<String, String> formatted = extendedSystemInfoUtils.getSystemPropertiesFormatted("\n");

        //then
        assertThat(formatted, hasEntry("example.path", "jack:\npeter"));
        assertThat(formatted, hasEntry("other", "property"));
        assertThat(formatted, not(hasEntry("os.name", "Java")));

        verify(properties, never()).getProperties();
    }


    @Before
    public void setUp() throws Exception {
        when(i18nHelper.getText(anyString())).thenAnswer(new Answer<String>() {
            @Override
            public String answer(InvocationOnMock invocation) throws Throwable {
                Object[] args = invocation.getArguments();
                return (String) args[0];
            }
        });

        when(analyticsHelper.getOnStartUsageStats(true)).thenReturn(getJiraLifecycleAnalyticsServiceKeys());

        final MockComponentWorker componentAccessorWorker = new MockComponentWorker();
        componentAccessorWorker.registerMock(OfBizDelegator.class, OfBizFactory.getOfBizDelegator());
        ComponentAccessor.initialiseWorker(componentAccessorWorker);
    }

    @Test
    public void shouldAddThreeParamsToDatabaseStatsManually() {
        final Map<String, String> usageStats = extendedSystemInfoUtils.getUsageStats();
        assertEquals(16, usageStats.size());
        assertThat(usageStats.keySet(), Matchers.contains(
                "admin.systeminfo.attachments",
                "admin.systeminfo.comments",
                "admin.systeminfo.components",
                "admin.systeminfo.custom.fields",
                "admin.systeminfo.groups",
                "admin.systeminfo.issues",
                "admin.systeminfo.issuesecuritylevels",
                "admin.systeminfo.issuetypes",
                "admin.systeminfo.priorities",
                "admin.systeminfo.projects",
                "admin.systeminfo.resolutions",
                "admin.systeminfo.screens",
                "admin.systeminfo.screensschemes",
                "admin.systeminfo.statuses",
                "admin.systeminfo.users",
                "admin.systeminfo.versions"));

    }

    private Map<String, Object> getJiraLifecycleAnalyticsServiceKeys() {
        return ImmutableMap.<String, Object>builder()
                .put("issues", 0)
                .put("projects", 0)
                .put("comments", 0)
                .put("customfields", 0)
                .put("issuetypes", 0)
                .put("permissionschemes", 0)
                .put("status", 0)
                .put("resolutions", 0)
                .put("priorities", 0)
                .put("versions", 0)
                .put("components", 0)
                .put("issuesecuritylevels", 0)
                .put("screens", 0)
                .put("screensschemes", 0)
                .put("permission.scheme.avg", 0).build();

    }
}