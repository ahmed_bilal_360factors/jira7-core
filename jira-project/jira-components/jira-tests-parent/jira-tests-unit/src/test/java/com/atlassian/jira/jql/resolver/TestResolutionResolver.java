package com.atlassian.jira.jql.resolver;

import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.issue.resolution.Resolution;
import com.atlassian.jira.issue.resolution.ResolutionImpl;
import com.atlassian.jira.mock.ofbiz.MockGenericValue;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import java.util.Collections;

import static org.hamcrest.Matchers.empty;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestResolutionResolver {
    @Rule
    public MethodRule initMockito = MockitoJUnit.rule();
    @Mock
    private ConstantsManager mockConstantsManager;

    @Test
    public void testGetAll() throws Exception {
        when(mockConstantsManager.getResolutionObjects()).thenReturn(Collections.<Resolution>emptyList());

        ResolutionResolver resolutionResolver = new ResolutionResolver(mockConstantsManager);

        assertThat(resolutionResolver.getAll(), empty());
    }

    @Test
    public void testNameIsCorrect() throws Exception {
        when(mockConstantsManager.getConstantByNameIgnoreCase(ConstantsManager.RESOLUTION_CONSTANT_TYPE, "test"))
                .thenReturn(new ResolutionImpl(new MockGenericValue("blah"), null, null, null));

        ResolutionResolver resolutionResolver = new ResolutionResolver(mockConstantsManager);
        assertTrue("Name exists", resolutionResolver.nameExists("test"));
    }
}
