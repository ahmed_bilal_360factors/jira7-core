package com.atlassian.jira.bc;

import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.jira.util.SimpleWarningCollection;
import com.atlassian.jira.util.WarningCollection;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import org.apache.commons.lang3.SerializationUtils;
import org.junit.Test;

import java.io.IOException;
import java.io.NotSerializableException;
import java.io.ObjectOutputStream;

import static org.junit.Assert.assertThat;
import static org.hamcrest.Matchers.is;

public class TestServiceResultImpl {

    /**
     * Ensure ServiceResultImpl can actually be serialized.
     *
     * @throws org.apache.commons.lang3.SerializationException if serialization fails, e.g. if a component can't be serialized.
     */
    @Test
    public void serviceResultImplIsActuallySerializable() {
        ErrorCollection errorCollection = new UnserializableErrorCollection(); //Typically a non-serializable error collection, e.g. JiraWebActionSupport subclass
        WarningCollection warningCollection = new SimpleWarningCollection();
        ServiceResultImpl sr = new ServiceResultImpl(errorCollection, warningCollection);

        SerializationUtils.serialize(sr);
    }

    /**
     * Serialize, deserialize and verify errors and warnings are the same to verify writeReplace works.
     */
    @Test
    public void serializationRestoreWorks() {
        ErrorCollection errorCollection = new UnserializableErrorCollection(); //Typically a non-serializable error collection, e.g. JiraWebActionSupport subclass
        errorCollection.addError("myField", "my message");
        WarningCollection warningCollection = new SimpleWarningCollection();
        warningCollection.addWarning("my warning");
        ServiceResultImpl sr = new ServiceResultImpl(errorCollection, warningCollection);

        ServiceResultImpl deserialized = SerializationUtils.roundtrip(sr);

        assertThat(deserialized.getErrorCollection().getErrors(), is(ImmutableMap.of("myField", "my message")));
        assertThat(deserialized.getWarningCollection().getWarnings(), is(ImmutableList.of("my warning")));
    }

    private static class UnserializableErrorCollection extends SimpleErrorCollection {
        private void writeObject(ObjectOutputStream os) throws IOException {
            throw new NotSerializableException(UnserializableErrorCollection.class.getName());
        }
    }
}
