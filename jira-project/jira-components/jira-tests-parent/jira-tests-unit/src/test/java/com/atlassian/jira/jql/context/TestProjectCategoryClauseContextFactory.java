package com.atlassian.jira.jql.context;

import com.atlassian.jira.jql.operand.JqlOperandResolver;
import com.atlassian.jira.jql.operand.QueryLiteral;
import com.atlassian.jira.jql.resolver.ProjectCategoryResolver;
import com.atlassian.jira.jql.validator.MockJqlOperandResolver;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.collect.CollectionBuilder;
import com.atlassian.query.clause.TerminalClause;
import com.atlassian.query.clause.TerminalClauseImpl;
import com.atlassian.query.operand.MultiValueOperand;
import com.atlassian.query.operand.SingleValueOperand;
import com.atlassian.query.operator.Operator;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import java.util.Collection;

import static com.atlassian.jira.jql.operand.SimpleLiteralFactory.createLiteral;
import static com.atlassian.jira.permission.ProjectPermissions.BROWSE_PROJECTS;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestProjectCategoryClauseContextFactory {
    @Rule
    public MethodRule initMockito = MockitoJUnit.rule();

    @Mock
    private PermissionManager permissionManager;
    @Mock
    private ProjectCategoryResolver projectCategoryResolver;
    private JqlOperandResolver jqlOperandResolver;
    private Collection<Project> allVisibleProjects;
    private MockProject project1;
    private MockProject project2;
    private MockProject project3;
    private MockProject project4;
    private MockProject project5;
    private ApplicationUser theUser = null;


    @Before
    public void setUp() throws Exception {
        jqlOperandResolver = MockJqlOperandResolver.createSimpleSupport();

        project1 = new MockProject(1L);
        project2 = new MockProject(2L);
        project3 = new MockProject(3L);
        project4 = new MockProject(4L);
        project5 = new MockProject(5L);
        allVisibleProjects = CollectionBuilder.<Project>newBuilder(
                project1,
                project2,
                project3,
                project4
        ).asSet();
    }

    @After
    public void tearDown() throws Exception {

        permissionManager = null;
        projectCategoryResolver = null;
        jqlOperandResolver = null;

        project1 = null;
        project2 = null;
        project3 = null;
        project4 = null;
        project5 = null;

        allVisibleProjects = null;
    }

    @Test
    public void testBadOperator() throws Exception {
        final TerminalClause clause = new TerminalClauseImpl("category", Operator.LESS_THAN, "test");

        final ProjectCategoryClauseContextFactory factory = new ProjectCategoryClauseContextFactory(permissionManager, projectCategoryResolver, jqlOperandResolver);

        final ClauseContext result = factory.getClauseContext(theUser, clause);
        assertEquals(ClauseContextImpl.createGlobalClauseContext(), result);
    }

    @Test
    public void testNullLiterals() throws Exception {
        final SingleValueOperand operand = new SingleValueOperand("test");
        final TerminalClause clause = new TerminalClauseImpl("category", Operator.EQUALS, operand);

        when(permissionManager.getProjects(BROWSE_PROJECTS, null))
                .thenReturn(allVisibleProjects);

        jqlOperandResolver = mock(JqlOperandResolver.class);
        when(jqlOperandResolver.getValues(theUser, operand, clause))
                .thenReturn(null);

        final ProjectCategoryClauseContextFactory factory = new ProjectCategoryClauseContextFactory(permissionManager, projectCategoryResolver, jqlOperandResolver);

        final ClauseContext result = factory.getClauseContext(theUser, clause);
        assertEquals(ClauseContextImpl.createGlobalClauseContext(), result);
    }

    @Test
    public void testEquality() throws Exception {
        final MultiValueOperand operand = new MultiValueOperand(new QueryLiteral(), createLiteral(1L));
        final TerminalClause clause = new TerminalClauseImpl("category", Operator.IN, operand);

        when(permissionManager.getProjects(BROWSE_PROJECTS, null))
                .thenReturn(allVisibleProjects);

        when(projectCategoryResolver.getProjectsForCategory(new QueryLiteral()))
                .thenReturn(CollectionBuilder.<Project>newBuilder(project1, project5).asSet());

        when(projectCategoryResolver.getProjectsForCategory(createLiteral(1L)))
                .thenReturn(CollectionBuilder.<Project>newBuilder(project2, project3).asSet());

        final ProjectCategoryClauseContextFactory factory = new ProjectCategoryClauseContextFactory(permissionManager, projectCategoryResolver, jqlOperandResolver);

        final ClauseContext result = factory.getClauseContext(theUser, clause);
        final ClauseContext expectedContext = new ClauseContextImpl(
                CollectionBuilder.<ProjectIssueTypeContext>newBuilder(
                        new ProjectIssueTypeContextImpl(new ProjectContextImpl(1L), AllIssueTypesContext.INSTANCE),
                        new ProjectIssueTypeContextImpl(new ProjectContextImpl(2L), AllIssueTypesContext.INSTANCE),
                        new ProjectIssueTypeContextImpl(new ProjectContextImpl(3L), AllIssueTypesContext.INSTANCE)
                )
                        .asSet()
        );
        assertEquals(expectedContext, result);
    }

    @Test
    public void testEqualityNoResolvedProjects() throws Exception {
        final MultiValueOperand operand = new MultiValueOperand(createLiteral(1L));
        final TerminalClause clause = new TerminalClauseImpl("category", Operator.IN, operand);

        when(permissionManager.getProjects(BROWSE_PROJECTS, null))
                .thenReturn(allVisibleProjects);

        when(projectCategoryResolver.getProjectsForCategory(createLiteral(1L)))
                .thenReturn(CollectionBuilder.<Project>newBuilder().asSet());

        final ProjectCategoryClauseContextFactory factory = new ProjectCategoryClauseContextFactory(permissionManager, projectCategoryResolver, jqlOperandResolver);

        final ClauseContext result = factory.getClauseContext(theUser, clause);
        final ClauseContext expectedContext = ClauseContextImpl.createGlobalClauseContext();
        assertEquals(expectedContext, result);
    }

    @Test
    public void testInequalityNoResolvedProjects() throws Exception {
        final MultiValueOperand operand = new MultiValueOperand(createLiteral(1L));
        final TerminalClause clause = new TerminalClauseImpl("category", Operator.NOT_IN, operand);

        when(permissionManager.getProjects(BROWSE_PROJECTS, null))
                .thenReturn(allVisibleProjects);

        when(projectCategoryResolver.getProjectsForCategory(createLiteral(1L)))
                .thenReturn(CollectionBuilder.<Project>newBuilder().asSet());

        when(projectCategoryResolver.getProjectsForCategory(new QueryLiteral()))
                .thenReturn(CollectionBuilder.<Project>newBuilder().asSet());

        final ProjectCategoryClauseContextFactory factory = new ProjectCategoryClauseContextFactory(permissionManager, projectCategoryResolver, jqlOperandResolver);

        final ClauseContext result = factory.getClauseContext(theUser, clause);
        final ClauseContext expectedContext = ClauseContextImpl.createGlobalClauseContext();
        assertEquals(expectedContext, result);
    }

    @Test
    public void testInequalityWithEmpty() throws Exception {
        final MultiValueOperand operand = new MultiValueOperand(new QueryLiteral(), createLiteral(1L));
        final TerminalClause clause = new TerminalClauseImpl("category", Operator.NOT_IN, operand);

        when(permissionManager.getProjects(BROWSE_PROJECTS, null))
                .thenReturn(allVisibleProjects);

        when(projectCategoryResolver.getProjectsForCategory(new QueryLiteral()))
                .thenReturn(CollectionBuilder.<Project>newBuilder(project1, project5).asSet());

        when(projectCategoryResolver.getProjectsForCategory(createLiteral(1L)))
                .thenReturn(CollectionBuilder.<Project>newBuilder(project2, project3).asSet());

        final ProjectCategoryClauseContextFactory factory = new ProjectCategoryClauseContextFactory(permissionManager, projectCategoryResolver, jqlOperandResolver);

        final ClauseContext result = factory.getClauseContext(theUser, clause);
        final ClauseContext expectedContext = new ClauseContextImpl(
                CollectionBuilder.<ProjectIssueTypeContext>newBuilder(
                        new ProjectIssueTypeContextImpl(new ProjectContextImpl(4L), AllIssueTypesContext.INSTANCE)
                )
                        .asSet()
        );
        assertEquals(expectedContext, result);
    }

    @Test
    public void testInequalityWithoutEmpty() throws Exception {
        final MultiValueOperand operand = new MultiValueOperand(createLiteral(1L));
        final TerminalClause clause = new TerminalClauseImpl("category", Operator.NOT_IN, operand);

        when(permissionManager.getProjects(BROWSE_PROJECTS, null))
                .thenReturn(allVisibleProjects);

        when(projectCategoryResolver.getProjectsForCategory(createLiteral(1L)))
                .thenReturn(CollectionBuilder.<Project>newBuilder(project2, project3).asSet());

        when(projectCategoryResolver.getProjectsForCategory(new QueryLiteral()))
                .thenReturn(CollectionBuilder.<Project>newBuilder(project1, project5).asSet());

        final ProjectCategoryClauseContextFactory factory = new ProjectCategoryClauseContextFactory(permissionManager, projectCategoryResolver, jqlOperandResolver);

        final ClauseContext result = factory.getClauseContext(theUser, clause);
        final ClauseContext expectedContext = new ClauseContextImpl(
                CollectionBuilder.<ProjectIssueTypeContext>newBuilder(
                        new ProjectIssueTypeContextImpl(new ProjectContextImpl(4L), AllIssueTypesContext.INSTANCE)
                )
                        .asSet()
        );
        assertEquals(expectedContext, result);
    }
}
