package com.atlassian.jira.datetime;

import com.atlassian.core.util.Clock;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.google.common.collect.ImmutableMap;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

/**
 * Unit tests for TZConverter.
 */
public class DateTimeRelativeFormatterTest {
    /**
     * Sydney time zone.
     */
    private static final DateTimeZone SYDNEY_TZ = DateTimeZone.forTimeZone(TimeZone.getTimeZone("Australia/Sydney"));

    /**
     * Amsterdam time zone.
     */
    private static final DateTimeZone AMSTERDAM_TZ = DateTimeZone.forTimeZone(TimeZone.getTimeZone("Europe/Amsterdam"));

    /**
     * Sao Paulo time zone.
     */
    private static final DateTimeZone SAO_PAULO_TZ = DateTimeZone.forTimeZone(TimeZone.getTimeZone("America/Sao_Paulo"));

    /**
     * Locale used in test.
     */
    private static final Locale userLocale = Locale.UK;

    /**
     * This is the time that we use in most tests. It has time zone Sydney, because that is the time zone of the
     * server.
     */
    private final DateTime twoFiftyThreeAM_Sydney = new DateTime(2010, 11, 23, 2, 53, 0, 0, SYDNEY_TZ);

    /**
     * This is the same time as #twoFiftyThreeAM_Sydney, but shifted into the Amsterdam time zone.
     */
    private final DateTime twoFiftyThreeAM_Sydney_in_Amsterdam_TZ = new DateTime(twoFiftyThreeAM_Sydney, AMSTERDAM_TZ);

    /**
     * The class being tested.
     */
    private DateTimeRelativeFormatter formatterUnderTest;

    @Rule
    public final MethodRule initMockito = MockitoJUnit.rule();

    @Mock
    private ApplicationProperties applicationProperties;

    @Mock
    private DateTimeFormatterServiceProvider serviceProvider;

    @Mock
    private Clock clock;

    @Test
    public void issueCreatedAMinuteAgoShouldDisplayAsToday() throws Exception {
        final DateTime nov22InAmsterdam = new DateTime(2010, 11, 22, 16, 54, 0, 0, AMSTERDAM_TZ);
        setClockTo(nov22InAmsterdam);

        // now = Nov 22 in amsterdam, so Nov 22 is today
        String formatted = formatterUnderTest.format(twoFiftyThreeAM_Sydney_in_Amsterdam_TZ, userLocale);
        assertThat(formatted, equalTo("Today 04:53 PM"));
    }

    @Test
    public void issueCreatedYesterdayButLessThan24HoursAgoShouldDisplayAsYesterday() throws Exception {
        final DateTime nov23InAmsterdam = new DateTime(2010, 11, 23, 2, 1, 0, 0, AMSTERDAM_TZ);
        setClockTo(nov23InAmsterdam);

        // now = Nov 23 in amsterdam, so Nov 22 was yesterday
        String formatted = formatterUnderTest.format(twoFiftyThreeAM_Sydney_in_Amsterdam_TZ, userLocale);
        assertThat(formatted, equalTo("Yesterday 04:53 PM"));
    }

    @Test
    public void issueCreatedYesterdayAndOver24HoursAgoShouldDisplayAsYesterday() throws Exception {
        final DateTime nov23InAmsterdam = new DateTime(2010, 11, 23, 17, 1, 2, 3, AMSTERDAM_TZ);
        setClockTo(nov23InAmsterdam);

        // now = Nov 23 in amsterdam, so Nov 22 was yesterday
        String formatted = formatterUnderTest.format(twoFiftyThreeAM_Sydney_in_Amsterdam_TZ, userLocale);
        assertThat(formatted, equalTo("Yesterday 04:53 PM"));
    }

    @Test
    public void dstGapShouldBeHandledCorrectly() throws Exception {
        final DateTime sydneyTime = new DateTime(2010, 10, 17, 1, 0, 0, 0, SYDNEY_TZ);
        final DateTime sampaTime = new DateTime(sydneyTime, SAO_PAULO_TZ);

        // this is the day before a DST interval change thing
        final DateTime oct16InSaoPaulo = new DateTime(2010, 10, 16, 13, 0, 0, 0, SAO_PAULO_TZ);
        setClockTo(oct16InSaoPaulo);

        String formatted = formatterUnderTest.format(sampaTime, userLocale);
        assertThat(formatted, equalTo("Today 11:00 AM"));
    }

    @Before
    public void setUp() {
        prepareServiceProviderMock();
        prepareApplicationPropertiesMock();
        formatterUnderTest = new DateTimeRelativeFormatter(serviceProvider, new JodaFormatterSupplierStub(), applicationProperties, clock);
    }

    private void setClockTo(DateTime currentDate) {
        when(clock.getCurrentDate()).thenReturn(currentDate.toDate());
    }

    private void prepareServiceProviderMock() {
        // the supported date formats
        final ImmutableMap<String, String> dateFormats = ImmutableMap.of(
                APKeys.JIRA_LF_DATE_COMPLETE, "dd/MMM/yy hh:mm a", // complete
                APKeys.JIRA_LF_DATE_DAY, "EEEE hh:mm a",           // day of week + time
                APKeys.JIRA_LF_DATE_TIME, "hh:mm a"                // just time
        );

        when(serviceProvider.getUnescapedText("common.concepts.today")).thenReturn("Today {0}");
        when(serviceProvider.getUnescapedText("common.concepts.yesterday")).thenReturn("Yesterday {0}");
        for (Map.Entry<String, String> dateFormat : dateFormats.entrySet()) {
            String propertyKey = dateFormat.getKey();
            String pattern = dateFormat.getValue();

            when(serviceProvider.getDefaultBackedString(propertyKey)).thenReturn(pattern);
        }
    }

    private void prepareApplicationPropertiesMock() {
        when(applicationProperties.getOption(APKeys.JIRA_LF_DATE_RELATIVE)).thenReturn(true);
    }
}
