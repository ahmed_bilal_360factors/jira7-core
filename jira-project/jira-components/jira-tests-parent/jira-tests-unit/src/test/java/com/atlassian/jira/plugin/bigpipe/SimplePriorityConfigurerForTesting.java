package com.atlassian.jira.plugin.bigpipe;

import com.atlassian.plugin.elements.ResourceDescriptor;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Map;

public class SimplePriorityConfigurerForTesting implements BigPipePriorityConfigurer {
    static final int PRIORITY = 86;

    @Nullable
    @Override
    public Integer calculatePriority(@Nonnull ResourceDescriptor resource, @Nonnull Map<String, ?> context) {
        return PRIORITY;
    }
}
