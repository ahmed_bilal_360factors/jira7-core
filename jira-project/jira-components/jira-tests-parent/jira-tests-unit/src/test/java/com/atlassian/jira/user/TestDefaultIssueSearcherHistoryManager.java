package com.atlassian.jira.user;

import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.search.searchers.IssueSearcher;
import com.atlassian.jira.issue.search.searchers.information.SearcherInformation;
import com.atlassian.jira.util.collect.CollectionBuilder;
import org.hamcrest.Matchers;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.List;

import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @since v5.2
 */
public class TestDefaultIssueSearcherHistoryManager {
    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Mock
    private UserHistoryManager historyManager;
    @Mock
    private ApplicationProperties applicationProperties;
    @Mock
    private IssueSearcher searcher;
    @Mock
    private SearcherInformation searcherInformation;

    private final ApplicationUser user = new MockApplicationUser("admin");

    @InjectMocks
    private DefaultUserIssueSearcherHistoryManager issueSearcherHistoryManager;

    @Test
    public void testAddNullIssueSearcher() throws Exception {
        exception.expect(IllegalArgumentException.class);
        issueSearcherHistoryManager.addIssueSearcherToHistory(user, null);
    }

    @Test
    public void testAddProjectNullUser() {
        when(searcher.getSearchInformation()).thenReturn(searcherInformation);
        when(searcherInformation.getId()).thenReturn("123");

        issueSearcherHistoryManager.addIssueSearcherToHistory(null, searcher);

        verify(historyManager).addItemToHistory(UserHistoryItem.ISSUESEARCHER, null, "123");
    }

    @Test
    public void testAddIssueSearcher() {
        when(searcher.getSearchInformation()).thenReturn(searcherInformation);

        searcherInformation.getId();
        when(searcherInformation.getId()).thenReturn("123");


        issueSearcherHistoryManager.addIssueSearcherToHistory(user, searcher);

        verify(historyManager).addItemToHistory(UserHistoryItem.ISSUESEARCHER, user, "123");
    }

    @Test
    public void testGettingUsersCurrentIssueSearchers() {
        final UserHistoryItem item1 = new UserHistoryItem(UserHistoryItem.ISSUESEARCHER, "123");
        final UserHistoryItem item2 = new UserHistoryItem(UserHistoryItem.ISSUESEARCHER, "1234");
        final UserHistoryItem item3 = new UserHistoryItem(UserHistoryItem.ISSUESEARCHER, "1235");
        final UserHistoryItem item4 = new UserHistoryItem(UserHistoryItem.ISSUESEARCHER, "1236");

        when(applicationProperties.getDefaultBackedString("jira.max.Searcher.history.items")).thenReturn("10");

        final List<UserHistoryItem> list = CollectionBuilder.newBuilder(item1, item2, item3, item4).asList();
        when(historyManager.getHistory(UserHistoryItem.ISSUESEARCHER, user)).thenReturn(list);

        final List<UserHistoryItem> result = issueSearcherHistoryManager.getUserIssueSearcherHistory(user);
        assertThat(result, Matchers.hasSize(4));
    }

    @Test
    public void testGettingMaximumUsersCurrentIssueSearchers() {
        final UserHistoryItem item1 = new UserHistoryItem(UserHistoryItem.ISSUESEARCHER, "123");
        final UserHistoryItem item2 = new UserHistoryItem(UserHistoryItem.ISSUESEARCHER, "1234");
        final UserHistoryItem item3 = new UserHistoryItem(UserHistoryItem.ISSUESEARCHER, "1235");
        final UserHistoryItem item4 = new UserHistoryItem(UserHistoryItem.ISSUESEARCHER, "1236");

        when(applicationProperties.getDefaultBackedString("jira.max.Searcher.history.items")).thenReturn("2");

        final List<UserHistoryItem> list = CollectionBuilder.newBuilder(item1, item2, item3, item4).asList();
        when(historyManager.getHistory(UserHistoryItem.ISSUESEARCHER, user)).thenReturn(list);

        final List<UserHistoryItem> result = issueSearcherHistoryManager.getUserIssueSearcherHistory(user);
        assertThat(result, Matchers.hasSize(2));
    }
}
