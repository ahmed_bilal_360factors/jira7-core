package com.atlassian.jira.avatar;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.bc.project.ProjectAction;
import com.atlassian.jira.bc.project.ProjectService;
import com.atlassian.jira.cache.vcache.MockVCacheFactory;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.IssueTypeService;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.config.util.AbstractJiraHome;
import com.atlassian.jira.config.util.JiraHome;
import com.atlassian.jira.io.MediaConsumer;
import com.atlassian.jira.mock.MockAvatar;
import com.atlassian.jira.mock.component.MockComponentWorker;
import com.atlassian.jira.plugin.icon.DefaultSystemIconImageProvider;
import com.atlassian.jira.plugin.icon.IconTypeDefinition;
import com.atlassian.jira.icon.IconType;
import com.atlassian.jira.plugin.icon.IconTypeDefinitionFactory;
import com.atlassian.jira.plugin.icon.IconTypePolicy;
import com.atlassian.jira.plugin.icon.ProjectIconTypePolicy;
import com.atlassian.jira.plugin.icon.SystemIconImageProvider;
import com.atlassian.jira.plugin.icon.UserIconTypePolicy;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.Consumer;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.IOUtil;
import com.atlassian.jira.util.TempDirectoryUtil;
import com.google.common.collect.ImmutableList;
import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.annotation.Nonnull;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import static com.atlassian.jira.avatar.Avatar.Type.ISSUETYPE;
import static com.atlassian.jira.avatar.Avatar.Type.PROJECT;
import static com.atlassian.jira.avatar.Avatar.Type.USER;
import static com.atlassian.jira.permission.GlobalPermissionKey.ADMINISTER;
import static com.atlassian.jira.permission.ProjectPermissions.ADMINISTER_PROJECTS;
import static com.atlassian.jira.permission.ProjectPermissions.BROWSE_PROJECTS;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.isA;
import static org.mockito.Matchers.refEq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

/**
 * Unit test for {@link com.atlassian.jira.avatar.AvatarManagerImpl}.
 *
 * @since v4.0
 */
@RunWith(MockitoJUnitRunner.class)
public class TestAvatarManagerImpl {
    private static final ApplicationUser ANONYMOUS_APP_USER = null;
    private static final String FILENAME_STUB = "filename-stub";
    private static final String AVATAR_SUBJECT = "subject1";

    @Mock
    private AvatarStore avatarStore;
    @Mock
    private PermissionManager permissionManager;
    @Mock
    private EventPublisher eventPublisher;
    @Mock
    private GlobalPermissionManager globalPermissionManager;
    @Mock
    private IssueTypeService issueTypeService;
    @Mock
    private AvatarImageDataStorage imageDataStore;
    @Mock
    private AvatarTranscoder avatarTranscoder;
    @Mock
    private IconTypeDefinitionFactory iconTypeFactory;
    @Mock
    private DefaultSystemIconImageProvider systemIconImageProvider;
    @Mock
    private ProjectService projectService;
    @Mock
    private ProjectManager projectManager;

    private AvatarImageDataStorage avatarImageDataStorage;

    private AvatarManagerImpl avatarManager;

    private ApplicationUser fred = new MockApplicationUser("fred", "Fred", "Fred Flintstone", "fred@example.com");
    private ApplicationUser admin = new MockApplicationUser("admin", "Admin", "Admin I. Strator", "admin@example.com");
    private ApplicationUser projAdmin = new MockApplicationUser("projadmin", "ProjAdmin", "Project Admin", "projadmin@example.com");

    private AvatarTagger avatarTagger;

    @Before
    public void before() {
        avatarImageDataStorage = mock(AvatarImageDataStorage.class);
        avatarManager = new AvatarManagerImpl(
                avatarStore,
                null,
                null,
                avatarTagger,
                avatarImageDataStorage,
                permissionManager,
                eventPublisher,
                iconTypeFactory,
                new MockVCacheFactory(),
                avatarTranscoder);
    }

    @After
    public void tearDown() {
        fred = null;
        admin = null;
        projAdmin = null;
        avatarStore = null;
        avatarTagger = null;
    }

    @Test
    public void testGetById() {
        final Avatar avatar = new AvatarImpl(null, "foo", "mime/type", IconType.PROJECT_ICON_TYPE, "otto", false);
        when(avatarStore.getById(1001L)).thenReturn(avatar);

        assertThat(avatarManager.getById(1001L), is(avatar));
    }

    @Test
    public void testCreate() {
        final Avatar avatar1 = new AvatarImpl(null, "foo", "mime/type", IconType.PROJECT_ICON_TYPE, "otto", false);
        final Avatar avatar2 = new AvatarImpl(null, "foo2", "mime/type", IconType.PROJECT_ICON_TYPE, null, true);
        when(avatarStore.create(avatar1)).thenReturn(avatar2);

        assertThat(avatarManager.create(avatar1), is(avatar2));
    }

    @Test
    public void testCreateSad() throws IOException {
        final Avatar system = new AvatarImpl(null, "foo2", "mime/type", IconType.PROJECT_ICON_TYPE, null, true);

        try {
            avatarManager.create(system, null, null);
            fail("expected IllegalArgumentException");
        } catch (IllegalArgumentException expected) {
            assertThat(expected.getMessage().toLowerCase(), containsString("system avatars"));
        }
    }

    @Test
    public void shouldCreateInStoreNonsystemAvatarWithPassedParameters() throws IOException {
        // given
        final ArgumentCaptor<Avatar> avatarArgumentCaptor = ArgumentCaptor.forClass(Avatar.class);
        final Avatar avatar = mock(Avatar.class);
        when(avatarStore.create(any(Avatar.class))).thenReturn(avatar);
        when(imageDataStore.getNextFilenameStub()).thenReturn(FILENAME_STUB);

        AvatarManager avatarManager =
                new AvatarManagerImpl(
                        avatarStore,
                        null,
                        globalPermissionManager,
                        avatarTagger,
                        imageDataStore,
                        permissionManager,
                        eventPublisher,
                        iconTypeFactory,
                        new MockVCacheFactory(),
                        avatarTranscoder);

        final AvatarImageDataProvider imageDataSource = mock(AvatarImageDataProvider.class);

        // when
        avatarManager.create(ISSUETYPE, AVATAR_SUBJECT, imageDataSource);

        // then
        verify(avatarStore).create(avatarArgumentCaptor.capture());

        final Avatar value = avatarArgumentCaptor.getValue();
        assertThat(value.getId(), nullValue());
        assertThat(value.getAvatarType(), is(ISSUETYPE));
        assertThat(value.getContentType(), is("image/png"));
        assertThat(value.getOwner(), is(AVATAR_SUBJECT));
        assertThat(value.isSystemAvatar(), is(false));
        assertThat(value.getFileName(), is(FILENAME_STUB + ".png"));
    }

    @Test
    public void shouldPassNewlyCreatedAvatarToImageStore() throws IOException {
        // given
        final ArgumentCaptor<Avatar> avatarArgumentCaptor = ArgumentCaptor.forClass(Avatar.class);
        final AvatarImageDataStorage imageDataStore = mock(AvatarImageDataStorage.class);
        when(imageDataStore.getNextFilenameStub()).thenReturn(FILENAME_STUB);
        final Avatar newlyCreatedAvatar = mock(Avatar.class);
        when(avatarStore.create(any(Avatar.class))).thenReturn(newlyCreatedAvatar);

        AvatarManagerImpl avatarManager = new AvatarManagerImpl(
                avatarStore,
                null,
                null,
                avatarTagger,
                imageDataStore,
                permissionManager,
                eventPublisher,
                iconTypeFactory,
                new MockVCacheFactory(),
                avatarTranscoder);
        final AvatarImageDataProvider imageDataSource = mock(AvatarImageDataProvider.class);

        // when
        avatarManager.create(ISSUETYPE, AVATAR_SUBJECT, imageDataSource);

        // then
        verify(imageDataStore).storeAvatarFiles(refEq(newlyCreatedAvatar), refEq(imageDataSource));
    }

    @Test
    public void testUpdate() {
        final Avatar avatar = new AvatarImpl(99L, "foo", "mime/type", IconType.PROJECT_ICON_TYPE, "otto", false);

        avatarManager.update(avatar);

        verify(avatarStore).update(avatar);
    }

    @Test
    public void testDelete() {
        final Avatar avatar = new AvatarImpl(6543L, "foo", "mime/type", IconType.PROJECT_ICON_TYPE, "otto", false);
        when(avatarStore.getById(6543L)).thenReturn(avatar);
        when(avatarStore.delete(6543L)).thenReturn(true);

        final boolean result = avatarManager.delete(6543L, false);

        verify(avatarStore).getById(6543L);
        verify(avatarStore).delete(6543L);
        assertThat("Return value", result, is(true));
        verifyZeroInteractions(eventPublisher);
    }

    @Test
    public void testGetAllSystemAvatars() {
        final List<Avatar> systemAvatars = ImmutableList.<Avatar>of(
                AvatarImpl.createSystemAvatar("foo", "mime/type", IconType.PROJECT_ICON_TYPE),
                AvatarImpl.createSystemAvatar("foo2", "mime/type", IconType.PROJECT_ICON_TYPE));

        when(avatarStore.getAllSystemAvatars(IconType.PROJECT_ICON_TYPE)).thenReturn(systemAvatars);

        assertThat(avatarManager.getAllSystemAvatars(IconType.PROJECT_ICON_TYPE), equalTo(systemAvatars));
    }

    @Test
    public void testGetAllSystemAvatarsShouldFilterOutDemotedIcons() {
        final AvatarImpl avatar1 = AvatarImpl.createSystemAvatar("foo", "mime/type", IconType.USER_ICON_TYPE);
        final AvatarImpl avatar2 = AvatarImpl.createSystemAvatar("foo2", "mime/type", IconType.USER_ICON_TYPE);

        final List<Avatar> systemAvatars = ImmutableList.<Avatar>of(
                avatar1,
                avatar2,
                AvatarImpl.createSystemAvatar("Avatar-1.png", "image/png", IconType.USER_ICON_TYPE),
                AvatarImpl.createSystemAvatar("Avatar-unknown.png", "image/png", IconType.USER_ICON_TYPE));

        when(avatarStore.getAllSystemAvatars(IconType.USER_ICON_TYPE)).thenReturn(systemAvatars);

        assertThat(avatarManager.getAllSystemAvatars(USER), containsInAnyOrder(avatar1, avatar2));
    }

    @Test
    public void testGetCustomAvatarsForOwner() {
        final List<Avatar> customAvatars = ImmutableList.<Avatar>of(
                AvatarImpl.createSystemAvatar("foo", "mime/type", IconType.PROJECT_ICON_TYPE),
                AvatarImpl.createSystemAvatar("foo2", "mime/type", IconType.PROJECT_ICON_TYPE));
        when(avatarStore.getCustomAvatarsForOwner(IconType.PROJECT_ICON_TYPE, "skywalker")).thenReturn(customAvatars);

        assertThat(avatarManager.getCustomAvatarsForOwner(PROJECT, "skywalker"), equalTo(customAvatars));
    }

    @Test
    public void testCreateAvatarFile() throws IOException {
        final JiraHome jiraHome = new SimpleJiraHome("func_tests_jira_home");
        avatarImageDataStorage = new AvatarImageDataStorage(jiraHome, eventPublisher);
        final AvatarManagerImpl avatarManager = new AvatarManagerImpl(
                null,
                null,
                null,
                avatarTagger,
                avatarImageDataStorage,
                permissionManager,
                eventPublisher,
                iconTypeFactory,
                new MockVCacheFactory(),
                avatarTranscoder);
        final Avatar avatar = new AvatarImpl(123L, "filename", "image/png", IconType.PROJECT_ICON_TYPE, "owner", false);
        final File avatarFile = avatarManager.createAvatarFile(avatar, Avatar.Size.defaultSize());

        assertThat("isDirectory", avatarFile.isDirectory(), is(false));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateWithStream() throws IOException {
        final JiraHome jiraHome = new SimpleJiraHome("func_tests_jira_home");
        final ByteArrayInputStream imageData = new ByteArrayInputStream("foo".getBytes("UTF-8"));
        avatarManager.create(null, imageData, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateSystemAvatarWithStream() throws IOException {
        final ByteArrayInputStream imageData = new ByteArrayInputStream("foo".getBytes("UTF-8"));
        avatarManager.create(new AvatarImpl(null, "foo2", "mime/type", IconType.PROJECT_ICON_TYPE, "otto2", true), imageData, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateAvatarWithNullStream() throws IOException {
        avatarManager.create(new AvatarImpl(null, "foo2", "mime/type", IconType.PROJECT_ICON_TYPE, "otto2", false), null, null);
    }

    @Test
    public void testCreateCustomAvatarWithStream() throws IOException {
        final Avatar avatar = new AvatarImpl(null, "foo2", "mime/type", IconType.PROJECT_ICON_TYPE, "otto2", false);
        when(avatarStore.create(avatar)).thenReturn(avatar);

        final AvatarManager avatarManager = new AvatarManagerImpl(
                avatarStore,
                null,
                null,
                avatarTagger,
                avatarImageDataStorage,
                permissionManager,
                eventPublisher,
                iconTypeFactory,
                new MockVCacheFactory(),
                avatarTranscoder) {
            @Override
            File processImage(final Avatar created, final InputStream imageData, final Selection croppingSelection,
                              final Avatar.Size size) throws IOException {
                final File tempFile = File.createTempFile("nothing", ".empty");
                tempFile.deleteOnExit();
                return tempFile;
            }
        };

        final ByteArrayInputStream imageData = new ByteArrayInputStream("foo".getBytes("UTF-8"));
        final Avatar avatarCreated = avatarManager.create(avatar, imageData, null);
        IOUtil.shutdownStream(imageData);
        assertThat(avatarCreated, is(sameInstance(avatar)));
    }

    @Test
    public void testProcessAvatarDataWillGenerateImageWhenImageDoesntExist() throws IOException {
        final AtomicBoolean processImageCalled = new AtomicBoolean(false);
        final File avData = File.createTempFile("TestAvatarManagerImpl.java", "testProcessAvatarData");
        createFile(avData, "imageData");

        final AvatarImpl avatar = new AvatarImpl(10000L, "foo2", "mime/type", IconType.PROJECT_ICON_TYPE, "otto2", false);
        final Avatar.Size testSize = Avatar.Size.XLARGE;
        final List<String> checkedSizes = new ArrayList<>(4);

        AvatarManagerImpl avatarManager = new AvatarManagerImpl(
                avatarStore,
                null,
                null,
                avatarTagger,
                avatarImageDataStorage,
                permissionManager,
                eventPublisher,
                iconTypeFactory,
                new MockVCacheFactory(),
                avatarTranscoder) {
            @Override
            public File getAvatarFile(Avatar av, Avatar.Size size) {
                String sizeFlag = AvatarFilenames.getFilenameFlag(size);
                checkedSizes.add(sizeFlag);
                if (sizeFlag.equals(AvatarFilenames.getFilenameFlag(testSize))) {
                    return new File(sizeFlag) {
                        @Override
                        public boolean exists() {
                            return false;
                        }
                    };
                } else {
                    return avData;
                }
            }

            @Override
            File processImage(Avatar created, InputStream imageData, Selection croppingSelection, Avatar.Size size)
                    throws IOException {
                processImageCalled.set(true);
                final File tempFile = File.createTempFile("nothing", ".empty");
                tempFile.deleteOnExit();
                return tempFile;
            }
        };
        when(avatarStore.getByIdTagged(10000L)).thenReturn(avatar);

        final AtomicBoolean consumeCalled = new AtomicBoolean(false);
        final Consumer<InputStream> mockConsumer = new Consumer<InputStream>() {
            public void consume(@Nonnull final InputStream in) {
                consumeCalled.set(true);
            }
        };
        avatarManager.readAvatarData(avatar, Avatar.Size.XLARGE, mockConsumer);
        assertTrue("Our larger image is read from", consumeCalled.get());
        assertTrue("The image was processed to generate a new file", processImageCalled.get());
        assertThat(checkedSizes, hasItem(AvatarFilenames.getFilenameFlag(testSize)));
    }

    @Test
    public void testProcessAvatarData() throws IOException {
        final File avData = File.createTempFile("TestAvatarManagerImpl.java", "testProcessAvatarData");
        createFile(avData, "imageData");

        final AvatarImpl avatar = new AvatarImpl(10000L, "foo2", "mime/type", IconType.PROJECT_ICON_TYPE, "otto2", false);
        final AtomicBoolean getAvatarFileCalled = new AtomicBoolean(false);

        final AvatarManagerImpl avatarManager = new AvatarManagerImpl(
                avatarStore,
                null,
                null,
                avatarTagger,
                avatarImageDataStorage,
                permissionManager,
                eventPublisher,
                iconTypeFactory,
                new MockVCacheFactory(),
                avatarTranscoder) {
            @Override
            public File getAvatarFile(Avatar av, Avatar.Size size) {
                String sizeFlag = AvatarFilenames.getFilenameFlag(size);
                assertEquals(avatar, av);
                assertEquals("medium_", sizeFlag);
                getAvatarFileCalled.set(true);
                return avData;
            }
        };
        final AtomicBoolean consumeCalled = new AtomicBoolean(false);
        final Consumer<InputStream> mockConsumer = new Consumer<InputStream>() {
            public void consume(@Nonnull final InputStream in) {
                assertStreamEqualsString("imageData", in);
                consumeCalled.set(true);
            }
        };

        when(avatarStore.getByIdTagged(10000L)).thenReturn(avatar);

        avatarManager.readAvatarData(avatar, Avatar.Size.MEDIUM, mockConsumer);
        assertThat("consumeCalled", consumeCalled.get(), is(true));
    }

    @Test
    public void testProcessAvatarDataSystem() throws IOException {
        final InputStream bogus = new ByteArrayInputStream("imageData".getBytes());

        when(systemIconImageProvider.getSystemIconInputStream(isA(Avatar.class), isA(Avatar.Size.class))).thenReturn(bogus);

        final AvatarImpl avatar = new AvatarImpl(10000L, "foo2", "mime/type", IconType.PROJECT_ICON_TYPE, null, true);
        final AtomicBoolean consumeCalled = new AtomicBoolean(false);
        final Consumer<InputStream> mockConsumer = new Consumer<InputStream>() {
            public void consume(@Nonnull final InputStream in) {
                assertStreamEqualsString("imageData", in);
                consumeCalled.set(true);
            }
        };
        when(avatarStore.getByIdTagged(10000L)).thenReturn(avatar);
        IconTypeDefinition iconTypeDefinition = mock(IconTypeDefinition.class);
        when(iconTypeFactory.getDefinition(IconType.PROJECT_ICON_TYPE)).thenReturn(iconTypeDefinition);
        when(iconTypeDefinition.getSystemIconImageProvider()).thenReturn(systemIconImageProvider);

        avatarManager.readAvatarData(avatar, Avatar.Size.MEDIUM, mockConsumer);
        assertThat("consumeCalled", consumeCalled.get(), is(true));
    }

    @Test
    public void testProcessAvatarDataSystem_NotStrictRequestPng_CantProvide() throws IOException {
        final InputStream bogus = new ByteArrayInputStream("imageData".getBytes());
        when(systemIconImageProvider.getSystemIconInputStream(isA(Avatar.class), isA(Avatar.Size.class))).thenReturn(bogus);

        final AvatarImpl avatar = new AvatarImpl(10000L, "foo2", "mime/type", IconType.PROJECT_ICON_TYPE, null, true);
        final AtomicBoolean consumeCalled = new AtomicBoolean(false);
        final Consumer<InputStream> mockConsumer = new Consumer<InputStream>() {
            public void consume(@Nonnull final InputStream in) {
                assertStreamEqualsString("imageData", in);
                consumeCalled.set(true);
            }
        };
        when(avatarStore.getByIdTagged(10000L)).thenReturn(avatar);

        IconTypeDefinition iconTypeDefinition = mock(IconTypeDefinition.class);
        when(iconTypeFactory.getDefinition(IconType.PROJECT_ICON_TYPE)).thenReturn(iconTypeDefinition);
        when(iconTypeDefinition.getSystemIconImageProvider()).thenReturn(systemIconImageProvider);

        avatarManager.readAvatarData(avatar, Avatar.Size.MEDIUM, AvatarFormatPolicy.createPngFormatPolicy().withFallingBackToOriginalDataStrategy(), new MediaConsumer(mockConsumer));
        assertThat("consumeCalled", consumeCalled.get(), is(true));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testProcessAvatarDataSystem_StrictRequestPng_CantProvide() throws IOException {
        final InputStream bogus = new ByteArrayInputStream("imageData".getBytes());
        when(systemIconImageProvider.getSystemIconInputStream(isA(Avatar.class), isA(Avatar.Size.class))).thenReturn(bogus);

        final AvatarImpl avatar = new AvatarImpl(10000L, "foo2", "mime/type", IconType.PROJECT_ICON_TYPE, null, true);
        final AtomicBoolean consumeCalled = new AtomicBoolean(false);
        final Consumer<InputStream> mockConsumer = new Consumer<InputStream>() {
            public void consume(@Nonnull final InputStream in) {
                assertStreamEqualsString("imageData", in);
                consumeCalled.set(true);
            }
        };
        when(avatarStore.getByIdTagged(10000L)).thenReturn(avatar);

        avatarManager.readAvatarData(avatar, Avatar.Size.MEDIUM, AvatarFormatPolicy.createPngFormatPolicy().withRejectingFallbackStrategy(), new MediaConsumer(mockConsumer));
        assertThat("consumeNotCalled", consumeCalled.get(), is(false));
    }

    @Test
    public void testProcessAvatarDataSystem_ImageSizeDeprecated() throws IOException {
        final InputStream bogus = new ByteArrayInputStream("imageData".getBytes());
        when(systemIconImageProvider.getSystemIconInputStream(isA(Avatar.class), isA(Avatar.Size.class))).thenReturn(bogus);

        IconTypeDefinition iconTypeDefinition = mock(IconTypeDefinition.class);
        when(iconTypeDefinition.getSystemIconImageProvider()).thenReturn(systemIconImageProvider);
        when(iconTypeFactory.getDefinition(IconType.PROJECT_ICON_TYPE)).thenReturn(iconTypeDefinition);

        final AvatarImpl avatar = new AvatarImpl(10000L, "foo2", "mime/type", IconType.PROJECT_ICON_TYPE, null, true);
        final AtomicBoolean consumeCalled = new AtomicBoolean(false);
        final Consumer<InputStream> mockConsumer = new Consumer<InputStream>() {
            public void consume(@Nonnull final InputStream in) {
                assertStreamEqualsString("imageData", in);
                consumeCalled.set(true);
            }
        };
        when(avatarStore.getByIdTagged(10000L)).thenReturn(avatar);

        avatarManager.readAvatarData(avatar, AvatarManager.ImageSize.MEDIUM, mockConsumer);
        assertThat("consumeCalled", consumeCalled.get(), is(true));
    }

    @Test
    public void testProcessAvatarDataSystemTranscoded() throws IOException {
        final File avData = File.createTempFile("TestAvatarManagerImpl.java", "testProcessAvatarData");
        createFile(avData, "imageData2");

        InputStream inputStream = new FileInputStream(avData);

        final AvatarImpl avatar = new AvatarImpl(10000L, "foo2", AvatarManager.AVATAR_IMAGE_FORMAT_SVG.getContentType(), IconType.PROJECT_ICON_TYPE, null, true);

        final AtomicBoolean consumeCalled = new AtomicBoolean(false);
        final Consumer<InputStream> mockConsumer = in -> {
            assertStreamEqualsString("imageData2", in);
            consumeCalled.set(true);
        };

        when(avatarStore.getByIdTagged(10000L)).thenReturn(avatar);
        when(avatarTranscoder.getOrCreateRasterizedAvatarFile(avatar, AvatarManager.ImageSize.MEDIUM.getSize(), inputStream)).thenReturn(avData);

        IconTypeDefinition iconTypeDefinition = mock(IconTypeDefinition.class);
        when(iconTypeFactory.getDefinition(IconType.PROJECT_ICON_TYPE)).thenReturn(iconTypeDefinition);

        SystemIconImageProvider systemIconImageProvider = mock(SystemIconImageProvider.class);
        when(iconTypeDefinition.getSystemIconImageProvider()).thenReturn(systemIconImageProvider);

        when(systemIconImageProvider.getSystemIconInputStream(avatar, Avatar.Size.MEDIUM)).thenReturn(inputStream);

        avatarManager.readAvatarData(avatar, Avatar.Size.MEDIUM, AvatarFormatPolicy.createPngFormatPolicy().withFallingBackToOriginalDataStrategy(), new MediaConsumer(mockConsumer));
        assertThat("consumeCalled", consumeCalled.get(), is(true));
    }

    @Test
    public void testProcessAvatarDataSystemNotFound() throws IOException {
        final AvatarImpl avatar = new AvatarImpl(10000L, "foo2", "mime/type", IconType.PROJECT_ICON_TYPE, null, true);
        when(systemIconImageProvider.getSystemIconInputStream(isA(Avatar.class), isA(Avatar.Size.class))).thenReturn(null);
        when(avatarStore.getByIdTagged(10000L)).thenReturn(avatar);
        IconTypeDefinition iconTypeDefinition = mock(IconTypeDefinition.class);
        when(iconTypeDefinition.getSystemIconImageProvider()).thenReturn(systemIconImageProvider);
        when(iconTypeFactory.getDefinition(IconType.PROJECT_ICON_TYPE)).thenReturn(iconTypeDefinition);

        try {
            avatarManager.readAvatarData(avatar, Avatar.Size.MEDIUM, null);

            fail("expected IOException because the classpath avatar data was null");
        } catch (IOException yay) {
            assertThat(yay.getMessage(), containsString("File not found"));
        }
    }

    @Test
    public void testGetDefaultAvatarIdHappyPath() {
        reset (avatarStore);

        final long PROJECT_AVATAR_ID = 5555;
        final String PROJECT_DEFAULT_FILENAME = "ben.svg";
        Avatar avatar = new MockAvatar(PROJECT_AVATAR_ID, PROJECT_DEFAULT_FILENAME, (String) null, IconType.PROJECT_ICON_TYPE, null, true);
        List<Avatar> avatars = new ArrayList<Avatar>();
        avatars.add(avatar);
        when(avatarStore.getSystemAvatarsForFilename(IconType.PROJECT_ICON_TYPE, PROJECT_DEFAULT_FILENAME)).thenReturn(avatars);
        final ApplicationProperties mockApplicationProperties = mock(ApplicationProperties.class);
        when(iconTypeFactory.getDefaultSystemIconFilename(IconType.PROJECT_ICON_TYPE)).thenReturn(PROJECT_DEFAULT_FILENAME);

        final AvatarManagerImpl am = new AvatarManagerImpl(
                avatarStore,
                mockApplicationProperties,
                null,
                avatarTagger,
                avatarImageDataStorage,
                permissionManager,
                eventPublisher,
                iconTypeFactory,
                new MockVCacheFactory(),
                avatarTranscoder);
        final Long id = am.getDefaultAvatarId(Avatar.Type.PROJECT);
        assertEquals(new Long(5555), id);
    }

    @Test
    public void testGetDefaultAvatarIdCacheHit() {
        reset (avatarStore);

        final long PROJECT_AVATAR_ID = 5555;
        final String PROJECT_DEFAULT_FILENAME = "ben.svg";
        Avatar avatar = new MockAvatar(PROJECT_AVATAR_ID, PROJECT_DEFAULT_FILENAME, null, IconType.PROJECT_ICON_TYPE, null, true);
        List<Avatar> avatars = new ArrayList<Avatar>();
        avatars.add(avatar);
        when(avatarStore.getSystemAvatarsForFilename(IconType.PROJECT_ICON_TYPE, PROJECT_DEFAULT_FILENAME)).thenReturn(avatars);
        final ApplicationProperties mockApplicationProperties = mock(ApplicationProperties.class);
        when(iconTypeFactory.getDefaultSystemIconFilename(IconType.PROJECT_ICON_TYPE)).thenReturn(PROJECT_DEFAULT_FILENAME);

        final AvatarManagerImpl am = new AvatarManagerImpl(
                avatarStore,
                mockApplicationProperties,
                null,
                avatarTagger,
                avatarImageDataStorage,
                permissionManager,
                eventPublisher,
                iconTypeFactory,
                new MockVCacheFactory(),
                avatarTranscoder);
        Long id = am.getDefaultAvatarId(Avatar.Type.PROJECT);
        assertEquals(new Long(5555), id);
        // call it a second time
        id = am.getDefaultAvatarId(Avatar.Type.PROJECT);
        assertEquals(new Long(5555), id);
        // verify that we only talked to the store once
        verify(avatarStore, times(1)).getSystemAvatarsForFilename(IconType.PROJECT_ICON_TYPE, PROJECT_DEFAULT_FILENAME);
    }

    @Test
    public void testGetDefaultAvatarIdNoAvatars() {
        final String PROJECT_DEFAULT_FILENAME = "ben.svg";
        reset(avatarStore);
        List<Avatar> avatars = new ArrayList<Avatar>();
        when(avatarStore.getSystemAvatarsForFilename(IconType.PROJECT_ICON_TYPE, PROJECT_DEFAULT_FILENAME)).thenReturn(avatars);
        final ApplicationProperties mockApplicationProperties = mock(ApplicationProperties.class);
        when(iconTypeFactory.getDefaultSystemIconFilename(IconType.PROJECT_ICON_TYPE)).thenReturn(PROJECT_DEFAULT_FILENAME);
        when(mockApplicationProperties.getString(APKeys.JIRA_DEFAULT_AVATAR_ID)).thenReturn("111222");

        final AvatarManagerImpl am = new AvatarManagerImpl(
                avatarStore,
                mockApplicationProperties,
                null,
                avatarTagger,
                avatarImageDataStorage,
                permissionManager,
                eventPublisher,
                iconTypeFactory,
                new MockVCacheFactory(),
                avatarTranscoder);
        final Long id = am.getDefaultAvatarId(Avatar.Type.PROJECT);
        assertEquals(new Long(111222), id);
    }

    @Test
    public void testHasPermissionToViewProject() {
        final long project1Id = 10012L;
        final long projectId2 = 10022L;

        final Project project1 = new MockProject(project1Id);
        final Project project2 = new MockProject(projectId2);

        when(projectManager.getProjectObj(project1Id)).thenReturn(project1);
        when(projectManager.getProjectObj(projectId2)).thenReturn(project2);

        when(globalPermissionManager.hasPermission(ADMINISTER, admin)).thenReturn(true);
        when(permissionManager.hasPermission(Permissions.ADMINISTER, admin)).thenReturn(true);
        when(permissionManager.hasPermission(BROWSE_PROJECTS, project1, ANONYMOUS_APP_USER)).thenReturn(true);
        when(permissionManager.hasPermission(ADMINISTER_PROJECTS, project2, projAdmin)).thenReturn(true);

        IconTypeDefinition iconTypeDefinition = mock(IconTypeDefinition.class);
        when(iconTypeDefinition.getPolicy()).thenReturn(new ProjectIconTypePolicy(globalPermissionManager, permissionManager, projectService, projectManager));
        when(iconTypeFactory.getDefinition(IconType.PROJECT_ICON_TYPE)).thenReturn(iconTypeDefinition);

        assertThat(avatarManager, canView(null, PROJECT, "10012"));
        assertThat(avatarManager, not(canView(fred, PROJECT, "10022")));
        assertThat(avatarManager, not(canView(fred, PROJECT, "INVALIDPROJECTID")));
        assertThat(avatarManager, not(canView(fred, PROJECT, null)));
        assertThat(avatarManager, canView(admin, PROJECT, "10012"));
        assertThat(avatarManager, canView(admin, PROJECT, "10022"));
        assertThat(avatarManager, not(canView(projAdmin, PROJECT, "10012")));
        assertThat(avatarManager, canView(projAdmin, PROJECT, "10022"));
    }

    @Test
    public void testHasPermissionToViewUser() {
        final UserManager userManager = mock(UserManager.class);
        when(userManager.getUserByKey("admin")).thenReturn(admin);
        when(userManager.getUserByKey("fred")).thenReturn(fred);

        final PermissionManager permissionManager = mock(PermissionManager.class);
        when(permissionManager.hasPermission(1, fred)).thenReturn(true);
        when(permissionManager.hasPermission(1, ANONYMOUS_APP_USER)).thenReturn(false);

        new MockComponentWorker()
                .addMock(UserManager.class, userManager)
                .addMock(PermissionManager.class, permissionManager)
                .init();

        IconTypeDefinition iconTypeDefinition = mock(IconTypeDefinition.class);
        when(iconTypeDefinition.getPolicy()).thenReturn(new UserIconTypePolicy(globalPermissionManager, permissionManager));
        when(iconTypeFactory.getDefinition(IconType.USER_ICON_TYPE)).thenReturn(iconTypeDefinition);

        assertThat(avatarManager, not(canView(null, USER, "admin")));
        assertThat(avatarManager, canView(fred, USER, null));
        assertThat(avatarManager, canView(fred, USER, "fred"));
        assertThat(avatarManager, canView(fred, USER, "admin"));

        ComponentAccessor.initialiseWorker(null);
    }

    @Test
    public void testHasPermissionToEditUser() {
        final UserManager userManager = mock(UserManager.class);
        when(userManager.getUserByKey("admin")).thenReturn(admin);
        when(userManager.getUserByKey("fred")).thenReturn(fred);

        when(globalPermissionManager.hasPermission(ADMINISTER, admin)).thenReturn(true);

        new MockComponentWorker()
                .addMock(UserManager.class, userManager)
                .addMock(PermissionManager.class, permissionManager)
                .init();

        IconTypeDefinition iconTypeDefinition = mock(IconTypeDefinition.class);
        when(iconTypeFactory.getDefinition(IconType.USER_ICON_TYPE)).thenReturn(iconTypeDefinition);
        when(iconTypeDefinition.getPolicy()).thenReturn(new UserIconTypePolicy(globalPermissionManager, permissionManager));

        assertThat(avatarManager, not(canEdit(null, USER, "admin")));
        assertThat(avatarManager, not(canEdit(fred, USER, null)));
        assertThat(avatarManager, canEdit(fred, USER, "fred"));
        assertThat(avatarManager, not(canEdit(fred, USER, "admin")));
        assertThat(avatarManager, canEdit(admin, USER, "fred"));

        ComponentAccessor.initialiseWorker(null);
    }

    @Test
    public void testHasPermissionToEditProject() {
        final PermissionManager permissionManager = mock(PermissionManager.class);
        final ProjectManager projectManager = mock(ProjectManager.class);

        final long project1Id = 10012L;
        final long project2Id = 10022L;

        final Project project1 = new MockProject(project1Id);
        final Project project2 = new MockProject(project2Id);

        when(projectManager.getProjectObj(project1Id)).thenReturn(project1);
        when(projectManager.getProjectObj(project2Id)).thenReturn(project2);
        when(globalPermissionManager.hasPermission(ADMINISTER, admin)).thenReturn(true);
        when(permissionManager.hasPermission(ADMINISTER_PROJECTS, project2, projAdmin)).thenReturn(true);

        final AvatarManagerImpl avatarManager = new AvatarManagerImpl(
                null,
                null,
                globalPermissionManager,
                avatarTagger,
                avatarImageDataStorage,
                permissionManager,
                eventPublisher,
                iconTypeFactory,
                new MockVCacheFactory(),
                avatarTranscoder);

        ErrorCollection errorCollectionErrors = mock(ErrorCollection.class);
        when(errorCollectionErrors.hasAnyErrors()).thenReturn(true);
        ProjectService.GetProjectResult errorResult = new ProjectService.GetProjectResult(errorCollectionErrors);
        when(projectService.getProjectByIdForAction(fred, 10022L, ProjectAction.EDIT_PROJECT_CONFIG)).thenReturn(errorResult);
        when(projectService.getProjectByIdForAction(projAdmin, 10012L, ProjectAction.EDIT_PROJECT_CONFIG)).thenReturn(errorResult);

        ErrorCollection errorCollectionNoErrors = mock(ErrorCollection.class);
        when(errorCollectionNoErrors.hasAnyErrors()).thenReturn(false);
        ProjectService.GetProjectResult goodResult = new ProjectService.GetProjectResult(errorCollectionNoErrors);
        when(projectService.getProjectByIdForAction(admin, 10012L, ProjectAction.EDIT_PROJECT_CONFIG)).thenReturn(goodResult);
        when(projectService.getProjectByIdForAction(admin, 10022L, ProjectAction.EDIT_PROJECT_CONFIG)).thenReturn(goodResult);
        when(projectService.getProjectByIdForAction(projAdmin, 10022L, ProjectAction.EDIT_PROJECT_CONFIG)).thenReturn(goodResult);

        IconTypeDefinition iconTypeDefinition = mock(IconTypeDefinition.class);
        when(iconTypeFactory.getDefinition(IconType.PROJECT_ICON_TYPE)).thenReturn(iconTypeDefinition);
        IconTypePolicy iconTypePolicy = new ProjectIconTypePolicy(globalPermissionManager, permissionManager, projectService, projectManager);
        when(iconTypeDefinition.getPolicy()).thenReturn(iconTypePolicy);

        assertThat(avatarManager, not(canEdit(null, PROJECT, "10012")));
        assertThat(avatarManager, not(canEdit(fred, PROJECT, "10022")));
        assertThat(avatarManager, not(canEdit(fred, PROJECT, "INVALIDPROJECTID")));
        assertThat(avatarManager, not(canEdit(fred, PROJECT, null)));
        assertThat(avatarManager, canEdit(admin, PROJECT, "10012"));
        assertThat(avatarManager, canEdit(admin, PROJECT, "10022"));
        assertThat(avatarManager, not(canEdit(projAdmin, PROJECT, "10012")));
        assertThat(avatarManager, canEdit(projAdmin, PROJECT, "10022"));
    }

    @Test
    public void testGetAvatarDirectory() {
        final JiraHome jiraHome = new SimpleJiraHome("localHomePath", "sharedHomePath");
        avatarImageDataStorage = new AvatarImageDataStorage(jiraHome, eventPublisher);
        final AvatarManager avatarManager = new AvatarManagerImpl(
                null,
                null,
                null,
                avatarTagger,
                avatarImageDataStorage,
                permissionManager,
                eventPublisher,
                iconTypeFactory,
                new MockVCacheFactory(),
                avatarTranscoder);
        final File dir = avatarManager.getAvatarBaseDirectory();

        assertEquals(jiraHome.getHomePath() + "/data/avatars", dir.getAbsolutePath());
    }

    @Test
    public void testIsSvgContentType() {
        assertTrue(AvatarManagerImpl.isSvgContentType("image/svg+xml"));
        assertFalse(AvatarManagerImpl.isSvgContentType("image/png"));
        assertFalse(AvatarManagerImpl.isSvgContentType(""));
        assertFalse(AvatarManagerImpl.isSvgContentType(null));
    }

    @Test
    public void testIsAvatarTranscodeable() {
        assertFalse(AvatarManagerImpl.isAvatarTranscodeable(new AvatarImpl(null, "foo", "mime/type", IconType.PROJECT_ICON_TYPE, "otto", false)));
        assertFalse(AvatarManagerImpl.isAvatarTranscodeable(new AvatarImpl(null, "foo", "image/png", IconType.PROJECT_ICON_TYPE, "otto", false)));
        assertFalse(AvatarManagerImpl.isAvatarTranscodeable(new AvatarImpl(null, "foo", "image/png", IconType.PROJECT_ICON_TYPE, null, true)));
        assertFalse(AvatarManagerImpl.isAvatarTranscodeable(new AvatarImpl(null, "foo", "mime/type", IconType.PROJECT_ICON_TYPE, null, true)));
        assertFalse(AvatarManagerImpl.isAvatarTranscodeable(new AvatarImpl(null, "foo", "image/svg+xml", IconType.PROJECT_ICON_TYPE, "otto", false)));
        assertTrue(AvatarManagerImpl.isAvatarTranscodeable(new AvatarImpl(null, "foo", "image/svg+xml", IconType.PROJECT_ICON_TYPE, null, true)));
    }

    private void assertStreamEqualsString(final String expectedContents, final InputStream in) {
        try {
            assertEquals(expectedContents, IOUtil.toString(in));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static void createFile(File file, String contents) throws IOException {
        final FileWriter writer = new FileWriter(file);
        try {
            writer.append(contents);
        } finally {
            writer.close();
        }
    }

    private static class SimpleJiraHome extends AbstractJiraHome {
        private final String name;
        private final String localName;
        private File tempFile;

        private SimpleJiraHome(final String name) {
            this.name = name;
            this.localName = name;
        }

        private SimpleJiraHome(final String name, final String localName) {
            this.name = name;
            this.localName = localName;
        }

        @Nonnull
        public File getHome() {
            return getDir(name);
        }

        private File getDir(String aName) {
            if (tempFile != null) {
                return tempFile;
            }

            final File file = TempDirectoryUtil.createTempDirectory(aName);
            if (!file.exists()) {
                assertTrue(file.mkdir());
            }
            file.deleteOnExit();
            tempFile = file;
            return file;
        }

        @Nonnull
        @Override
        public File getLocalHome() {
            return getDir(localName);
        }
    }

    public static HasPermissionMatcher canEdit(final ApplicationUser user, final Avatar.Type type, final String target) {
        return new HasPermissionMatcher(user, type, target, true);
    }

    public static HasPermissionMatcher canView(final ApplicationUser user, final Avatar.Type type, final String target) {
        return new HasPermissionMatcher(user, type, target, false);
    }

    public static class HasPermissionMatcher extends TypeSafeMatcher<AvatarManager> {
        private final ApplicationUser user;
        private final Avatar.Type type;
        private final String target;
        private final boolean edit;

        HasPermissionMatcher(final ApplicationUser user, final Avatar.Type type, final String target, final boolean edit) {
            this.user = user;
            this.type = type;
            this.target = target;
            this.edit = edit;
        }

        @Override
        protected boolean matchesSafely(final AvatarManager avatarManager) {
            if (edit) {
                return avatarManager.hasPermissionToEdit(user, type, target);
            }
            return avatarManager.hasPermissionToView(user, type, target);
        }

        @Override
        public void describeTo(final Description description) {
            description.appendText("hasPermissionTo")
                    .appendText(edit ? "Edit" : "View")
                    .appendText("(user=")
                    .appendValue(user)
                    .appendText(",type=")
                    .appendValue(type)
                    .appendText(",target=")
                    .appendText(target)
                    .appendText(")");
        }
    }
}
