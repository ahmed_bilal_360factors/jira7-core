package com.atlassian.jira.service.services.analytics.start;


import com.atlassian.jira.cluster.ClusterManager;
import com.atlassian.jira.config.properties.JiraProperties;
import com.atlassian.jira.util.BuildUtilsInfo;
import com.atlassian.jira.util.system.SystemInfoUtils;
import com.atlassian.plugin.parsers.SafeModeCommandLineArguments;
import com.atlassian.plugin.parsers.SafeModeCommandLineArgumentsFactory;
import com.google.common.collect.ImmutableList;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Map;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestJiraSystemAnalyticTask {

    @Test
    public void testJiraSystemAnalytic() {
        BuildUtilsInfo info = mock(BuildUtilsInfo.class);
        SystemInfoUtils systemInfoUtils = mock(SystemInfoUtils.class);
        ClusterManager clusterManager = mock(ClusterManager.class);
        JiraProperties jiraProperties = mock(JiraProperties.class);
        SafeModeCommandLineArguments safeModeCommandLineArguments = mock(SafeModeCommandLineArguments.class);
        SafeModeCommandLineArgumentsFactory safeModeCommandLineArgumentsFactory = mock(SafeModeCommandLineArgumentsFactory.class);
        when(safeModeCommandLineArgumentsFactory.get()).thenReturn(safeModeCommandLineArguments);

        when(safeModeCommandLineArguments.isSafeMode()).thenReturn(true);
        when(safeModeCommandLineArguments.getDisabledPlugins()).thenReturn(Optional.of(new ArrayList<>()));

        JiraSystemAnalyticTask task = new JiraSystemAnalyticTask(info, systemInfoUtils, clusterManager, jiraProperties, safeModeCommandLineArgumentsFactory);

        when(info.getVersionNumbers()).thenReturn(new int[]{6, 3, 9});
        when(info.getCurrentBuildNumber()).thenReturn("6339");
        when(clusterManager.isClusterLicensed()).thenReturn(true);

        final Map<String, Object> analytics = task.getAnalytics();

        assertEquals(6, analytics.get("release.version"));
        assertEquals(3, analytics.get("major.version"));
        assertEquals(9, analytics.get("minor.version"));
        assertEquals("6339", analytics.get("build.number"));
        assertTrue((Boolean) analytics.get("license.dc"));
    }

    @Test
    public void testSafeModeAnalyticsWhenSafeModeOff() {
        BuildUtilsInfo info = mock(BuildUtilsInfo.class);
        SystemInfoUtils systemInfoUtils = mock(SystemInfoUtils.class);
        ClusterManager clusterManager = mock(ClusterManager.class);
        JiraProperties jiraProperties = mock(JiraProperties.class);
        SafeModeCommandLineArguments safeModeCommandLineArguments = mock(SafeModeCommandLineArguments.class);
        SafeModeCommandLineArgumentsFactory safeModeCommandLineArgumentsFactory = mock(SafeModeCommandLineArgumentsFactory.class);
        when(safeModeCommandLineArgumentsFactory.get()).thenReturn(safeModeCommandLineArguments);

        when(safeModeCommandLineArguments.isSafeMode()).thenReturn(false);
        when(safeModeCommandLineArguments.getDisabledPlugins()).thenReturn(Optional.empty());

        JiraSystemAnalyticTask task = new JiraSystemAnalyticTask(info, systemInfoUtils, clusterManager,
                jiraProperties, safeModeCommandLineArgumentsFactory);

        when(info.getVersionNumbers()).thenReturn(new int[]{6, 3, 9});
        when(info.getCurrentBuildNumber()).thenReturn("6339");
        when(clusterManager.isClusterLicensed()).thenReturn(true);

        final Map<String, Object> analytics = task.getAnalytics();

        assertEquals(6, analytics.get("release.version"));
        assertEquals(3, analytics.get("major.version"));
        assertEquals(9, analytics.get("minor.version"));
        assertEquals("6339", analytics.get("build.number"));
        assertTrue((Boolean) analytics.get("license.dc"));
        assertFalse((Boolean) analytics.get("plugins.nonsystemaddonsdisabledonstartup"));
    }

    @Test
    public void testSafeModeAnalyticsWhenDisablingAPlugin() {
        BuildUtilsInfo info = mock(BuildUtilsInfo.class);
        SystemInfoUtils systemInfoUtils = mock(SystemInfoUtils.class);
        ClusterManager clusterManager = mock(ClusterManager.class);
        JiraProperties jiraProperties = mock(JiraProperties.class);
        SafeModeCommandLineArguments safeModeCommandLineArguments = mock(SafeModeCommandLineArguments.class);
        SafeModeCommandLineArgumentsFactory safeModeCommandLineArgumentsFactory = mock(SafeModeCommandLineArgumentsFactory.class);
        when(safeModeCommandLineArgumentsFactory.get()).thenReturn(safeModeCommandLineArguments);

        when(safeModeCommandLineArguments.isSafeMode()).thenReturn(false);
        when(safeModeCommandLineArguments.getDisabledPlugins()).thenReturn(Optional.of(
                ImmutableList.of("com.atlassian.plugins.base-hipchat-integration-plugin","com.atlassian.foo.bar")));

        JiraSystemAnalyticTask task = new JiraSystemAnalyticTask(
                info, systemInfoUtils, clusterManager,
                jiraProperties, safeModeCommandLineArgumentsFactory);

        when(info.getVersionNumbers()).thenReturn(new int[]{6, 3, 9});
        when(info.getCurrentBuildNumber()).thenReturn("6339");
        when(clusterManager.isClusterLicensed()).thenReturn(true);

        final Map<String, Object> analytics = task.getAnalytics();


        assertEquals(6, analytics.get("release.version"));
        assertEquals(3, analytics.get("major.version"));
        assertEquals(9, analytics.get("minor.version"));
        assertEquals("6339", analytics.get("build.number"));
        assertTrue((Boolean) analytics.get("license.dc"));
        assertFalse((Boolean) analytics.get("plugins.nonsystemaddonsdisabledonstartup"));
        assertEquals("2897327710555512756:-7281550170772689005", analytics.get("plugins.addonsdisabledonstartup"));
    }

}
