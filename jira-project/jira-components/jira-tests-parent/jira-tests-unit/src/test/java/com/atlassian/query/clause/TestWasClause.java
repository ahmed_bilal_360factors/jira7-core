package com.atlassian.query.clause;

import com.atlassian.query.history.TerminalHistoryPredicate;
import com.atlassian.query.operand.SingleValueOperand;
import com.atlassian.query.operator.Operator;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.concurrent.atomic.AtomicBoolean;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * @since v4.0
 */
public class TestWasClause {

    private final SingleValueOperand singleValueOperand = new SingleValueOperand("test");

    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Test
    public void testNullPredicateConstructorArgument() throws Exception {
        assertNotNull(new WasClauseImpl("testField", Operator.WAS, singleValueOperand, null));
    }

    @Test
    public void testNullOperandConstructorArgument() throws Exception {
        exception.expect(Exception.class);
        new WasClauseImpl("testField", null, null, null);
    }

    @Test
    public void testNullFieldConstructorArgument() throws Exception {
        exception.expect(Exception.class);
        new WasClauseImpl(null, null, singleValueOperand, null);
    }

    @Test
    public void testNeverContainsSubClauses() throws Exception {
        assertTrue(new WasClauseImpl("testField", Operator.WAS, singleValueOperand, null).getClauses().isEmpty());
    }

    @Test
    public void testToString() throws Exception {
        final WasClauseImpl clause = new WasClauseImpl("testField", Operator.WAS, singleValueOperand, null);
        assertEquals("{testField was \"test\"}", clause.toString());
    }

    @Test
    public void testVisit() throws Exception {
        final AtomicBoolean visitCalled = new AtomicBoolean(false);
        final ClauseVisitor<Void> visitor = new ClauseVisitor<Void>() {
            public Void visit(final AndClause andClause) {
                return failVisitor();
            }

            public Void visit(final NotClause notClause) {
                return failVisitor();
            }

            public Void visit(final OrClause orClause) {
                return failVisitor();
            }

            public Void visit(final TerminalClause clause) {
                return failVisitor();
            }

            @Override
            public Void visit(final WasClause clause) {
                visitCalled.set(true);
                return null;
            }

            @Override
            public Void visit(final ChangedClause clause) {
                return failVisitor();
            }
        };
        new WasClauseImpl("testField", Operator.WAS, singleValueOperand, null).accept(visitor);
        assertTrue(visitCalled.get());
    }

    /**
     * JRA-36813: Regression test for this bug.
     */
    @Test
    public void testToStringSeparatesOperandAndPredicateBySpace() {
        final WasClauseImpl wasClause = new WasClauseImpl("status", Operator.WAS, new SingleValueOperand("closed"),
                new TerminalHistoryPredicate(Operator.ON, new SingleValueOperand(3500000L)));
        assertEquals("{status was \"closed\" on 3500000}", wasClause.toString());
    }

    private Void failVisitor() {
        fail("Should not be called");
        return null;
    }

}
