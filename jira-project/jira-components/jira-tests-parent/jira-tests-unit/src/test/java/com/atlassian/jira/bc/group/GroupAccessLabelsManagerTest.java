package com.atlassian.jira.bc.group;

import com.atlassian.jira.application.ApplicationRole;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.user.MockGroup;
import com.google.common.collect.ImmutableSet;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import java.util.Optional;
import java.util.Set;

import static com.google.common.collect.ImmutableList.of;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class GroupAccessLabelsManagerTest {
    public static final Optional<Long> DIRECTORY = Optional.of(125L);
    @Rule
    public final MethodRule initMockito = MockitoJUnit.rule();

    @Mock
    private GroupsToApplicationsSeatingHelper seatingHelper;
    @Mock
    private GlobalPermissionManager globalPermissionManager;
    @Mock
    private GroupRelationshipChecker relationshipChecker;

    @InjectMocks
    private GroupAccessLabelsManager manager;

    @Before
    public void setUp() {
        when(relationshipChecker.isGroupEqualOrNested(eq(DIRECTORY), anyString(), anyString()))
                .thenAnswer(GroupRelationshipCheckerTest.eqalityMockitoAnswer());
    }

    @Test
    public void shouldNotBeAdminWhenGroupsAreNotMatching() throws Exception {
        when(globalPermissionManager.getGroupsWithPermission(GlobalPermissionKey.ADMINISTER)).thenReturn(of(new MockGroup("tigers")));
        when(globalPermissionManager.getGroupsWithPermission(GlobalPermissionKey.SYSTEM_ADMIN)).thenReturn(of(new MockGroup("lions")));

        assertFalse(manager.getForGroup(new MockGroup("pumas"), DIRECTORY).isAdmin());
    }

    @Test
    public void shouldBeAdminWhenOneGroupsGivingAdminPermissionIsRelatedToThatGroup() throws Exception {
        when(globalPermissionManager.getGroupsWithPermission(GlobalPermissionKey.ADMINISTER)).thenReturn(of(new MockGroup("tigers"), new MockGroup("lions")));

        assertTrue(manager.getForGroup(new MockGroup("tigers"), DIRECTORY).isAdmin());
    }

    @Test
    public void shouldBeAdminWhenOneGroupsGivingSysadminPermissionIsRelatedToThatGroup() {
        when(globalPermissionManager.getGroupsWithPermission(GlobalPermissionKey.SYSTEM_ADMIN)).thenReturn(of(new MockGroup("tigers"), new MockGroup("lions")));

        assertTrue(manager.getForGroup(new MockGroup("tigers"), DIRECTORY).isAdmin());
    }

    @SuppressWarnings("unchecked")
    @Test
    public void shouldReturnAccessibleApplications() {
        Set<ApplicationRole> effectiveApplications = mock(Set.class);
        when(seatingHelper.findEffectiveApplicationsByGroups(DIRECTORY, ImmutableSet.of("lions"))).thenReturn(effectiveApplications);

        assertSame(effectiveApplications, manager.getForGroup(new MockGroup("lions"), DIRECTORY).getAccessibleApplications());
    }
}