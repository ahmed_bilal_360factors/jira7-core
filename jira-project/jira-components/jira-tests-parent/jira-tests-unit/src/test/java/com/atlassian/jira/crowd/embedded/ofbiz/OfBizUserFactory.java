package com.atlassian.jira.crowd.embedded.ofbiz;

import org.ofbiz.core.entity.GenericValue;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class OfBizUserFactory {
    public static OfBizUser makeUser(final long directoryId, final long userId, final String userName, final String displayName,
                              final String email, final boolean active) {
        final GenericValue userAsGenericValue = mock(GenericValue.class);
        when(userAsGenericValue.getLong(UserEntity.DIRECTORY_ID)).thenReturn(directoryId);
        when(userAsGenericValue.getString(UserEntity.USER_NAME)).thenReturn(userName);
        when(userAsGenericValue.getString(UserEntity.DISPLAY_NAME)).thenReturn(displayName);
        when(userAsGenericValue.getLong(UserEntity.USER_ID)).thenReturn(userId);
        when(userAsGenericValue.getString(UserEntity.EMAIL_ADDRESS)).thenReturn(email);
        when(userAsGenericValue.getString(UserEntity.EXTERNAL_ID)).thenReturn(String.valueOf(userId));
        when(userAsGenericValue.getInteger(UserEntity.ACTIVE)).thenReturn(active ? 1 : 0);
        return OfBizUser.from(userAsGenericValue);
    }
}
