package com.atlassian.jira.plugin.webfragment.conditions;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.resolution.Resolution;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestIsIssueUnresolvedCondition {
    @Rule
    public MockitoRule mockito = MockitoJUnit.rule();
    @Mock
    private Issue issue;
    private IsIssueUnresolvedCondition condition;


    @Before
    public void setUp() throws Exception {
        condition = new IsIssueUnresolvedCondition();
    }

    @Test
    public void testFalse() {
        final Resolution resolution = mock(Resolution.class);
        final ApplicationUser fred = new MockApplicationUser("fred");

        when(issue.getResolutionObject()).thenReturn(resolution);

        assertFalse(condition.shouldDisplay(fred, issue, null));
    }

    @Test
    public void testTrue() {
        when(issue.getResolutionObject()).thenReturn(null);

        assertTrue(condition.shouldDisplay(null, issue, null));
    }


}
