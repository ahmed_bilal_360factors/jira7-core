package com.atlassian.jira.issue.fields.csv;


import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.export.FieldExportParts;
import com.atlassian.jira.issue.fields.IssueLinksSystemField;
import com.atlassian.jira.issue.link.IssueLink;
import com.atlassian.jira.issue.link.IssueLinkManager;
import com.atlassian.jira.issue.link.IssueLinkType;
import com.atlassian.jira.issue.link.MockIssueLinkType;
import com.atlassian.jira.junit.rules.InitMockitoMocks;
import com.atlassian.jira.mock.issue.MockIssue;

import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.I18nHelper;
import com.google.common.collect.ImmutableList;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.util.List;
import java.util.stream.Collectors;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class IssueLinksSystemFieldCsvExportTest {
    @Rule
    public TestRule initMockito = new InitMockitoMocks(this);

    @Mock
    private IssueLinkManager issueLinkManager;

    @InjectMocks
    private IssueLinksSystemField field;

    @Mock
    private JiraAuthenticationContext authenticationContext;

    @Mock
    private I18nHelper i18nHelper;

    private final Issue issue = new MockIssue(1L);

    @Before
    public void setUp() {
        when(authenticationContext.getI18nHelper()).thenReturn(i18nHelper);
    }

    @Test
    public void allNonSystemLinksAreExported() {
        final IssueLinkType typeBlocks = linkType("blocks");
        final IssueLinkType typeRelated = linkType("related");
        final IssueLinkType typeSystem = sysLinkType("system link");

        final IssueLink link1 = link("HSP-1", typeBlocks);
        final IssueLink link2 = link("HSP-2", typeRelated);
        final IssueLink link3 = link("HSP-3", typeBlocks);
        final IssueLink link4 = link("HSP-4", typeSystem);

        when(issueLinkManager.getOutwardLinks(issue.getId())).thenReturn(ImmutableList.of(
                link1, link2, link3, link4
        ));

        final FieldExportParts representation = field.getRepresentationFromIssue(issue);
        final List<String> blockedIssues = representation.getPartWithId(typeBlocks.getName()).get().getValues().collect(Collectors.toList());
        final List<String> relatedIssues =  representation.getPartWithId(typeRelated.getName()).get().getValues().collect(Collectors.toList());

        assertThat(blockedIssues, contains("HSP-1", "HSP-3"));
        assertThat(relatedIssues, contains("HSP-2"));
        assertThat(!representation.getPartWithId(typeSystem.getName()).isPresent(), equalTo(true));
    }

    private IssueLink link(final String destIssueKey, final IssueLinkType linkType) {
        final IssueLink link = mock(IssueLink.class);

        when(link.getIssueLinkType()).thenReturn(linkType);
        when(link.getDestinationObject()).thenReturn(new MockIssue(0, destIssueKey));

        return link;
    }

    private IssueLinkType linkType(String name) {
        return new MockIssueLinkType(1, name, "outward", "inward", "style");
    }

    private IssueLinkType sysLinkType(final String name) {
        final IssueLinkType linkType = mock(IssueLinkType.class);

        when(linkType.getName()).thenReturn(name);
        when(linkType.isSystemLinkType()).thenReturn(true);

        return linkType;
    }
}
