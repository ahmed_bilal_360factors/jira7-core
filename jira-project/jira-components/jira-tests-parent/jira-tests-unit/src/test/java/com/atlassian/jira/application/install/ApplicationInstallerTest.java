package com.atlassian.jira.application.install;

import org.apache.commons.io.FileUtils;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.TemporaryFolder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.io.File;
import java.io.IOException;
import java.util.Optional;
import java.util.stream.IntStream;

import static com.atlassian.jira.application.install.ApplicationSourceMatcher.appSource;
import static com.atlassian.jira.matchers.FileMatchers.exists;
import static java.lang.String.format;
import static org.hamcrest.Matchers.arrayContainingInAnyOrder;
import static org.hamcrest.Matchers.arrayWithSize;
import static org.hamcrest.Matchers.emptyArray;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.notNull;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class ApplicationInstallerTest {
    private static final String ENCODING = "UTF-8";
    private static final String PRODUCT_SOURCE_FOLDER = "a-g-i-l-e";
    @Rule
    public TemporaryFolder temporaryFolder = new TemporaryFolder();

    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    @Rule
    public MockitoRule mocks = MockitoJUnit.rule();

    @Mock
    ApplicationInstallationEnvironment installationEnvironment;

    @Mock
    BundlesVersionDiscovery versionDiscovery;

    @Mock
    WhatWasInstalled whatWasInstalled;

    @InjectMocks
    ApplicationInstaller testObj;
    private File sourceFolder;
    private File destinationFolder;

    @Before
    public void setUp() throws IOException {
        sourceFolder = temporaryFolder.newFolder();
        destinationFolder = temporaryFolder.newFolder();

        when(installationEnvironment.getApplicationsSource()).thenReturn(sourceFolder);
        when(installationEnvironment.getApplicationsDestination()).thenReturn(destinationFolder);
    }

    private static File addFile(final File base, final String name, final String content) {
        try {
            base.mkdirs();
            final File pluginFile = new File(base, name);
            FileUtils.writeStringToFile(pluginFile, content, ENCODING);
            return pluginFile;
        } catch (final IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static void assertFileContentEquals(final File base, final String name, final String content) {
        try {
            final File pluginFile = new File(base, name);
            assertThat(pluginFile, exists());
            assertThat(FileUtils.readFileToString(pluginFile, ENCODING), equalTo(content));
        } catch (final IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void shouldMoveFilesFromSourceFoldersIntoDestinationFolder() throws IOException {
        final String pluginFileBase = "some.plugin.file.%d.jar";
        final String pluginContentBase = "plugin-%d";

        // given
        when(whatWasInstalled.wasApplicationSourceInstalled(any(ApplicationSource.class))).thenReturn(false);
        final File agileInstallationPlugins = new File(sourceFolder, PRODUCT_SOURCE_FOLDER);
        agileInstallationPlugins.mkdirs();
        IntStream.range(0, 5).forEach(value ->
                        addFile(agileInstallationPlugins, format(pluginFileBase, value), format(pluginContentBase, value))
        );
        when(versionDiscovery.getBundleNameAndVersion(any(File.class))).thenReturn(Optional.of(new BundlesVersionDiscovery.PluginIdentification("a", "0")));

        // when
        testObj.installApplications();

        // then
        FileUtils.forceDelete(sourceFolder);
        IntStream.range(0, 5).forEach(value ->
                        assertFileContentEquals(destinationFolder, format(pluginFileBase, value), format(pluginContentBase, value))
        );
    }

    @Test
    public void shouldNotInstallAnyFilesIfSetWasInstalledPreviously() throws IOException {
        final String pluginFileBase = "some.plugin.file.%d.jar";
        final String pluginContentBase = "plugin-%d";

        // given
        final File agileInstallationPlugins = new File(sourceFolder, PRODUCT_SOURCE_FOLDER);
        agileInstallationPlugins.mkdirs();
        final File[] sourceFiles = IntStream.range(0, 5).mapToObj(value ->
                        addFile(agileInstallationPlugins, format(pluginFileBase, value), format(pluginContentBase, value))
        ).toArray(File[]::new);
        when(versionDiscovery.getBundleNameAndVersion(any(File.class))).thenReturn(Optional.of(new BundlesVersionDiscovery.PluginIdentification("a", "0")));
        when(whatWasInstalled.wasApplicationSourceInstalled(
                appSource(
                        equalTo(PRODUCT_SOURCE_FOLDER),
                        arrayContainingInAnyOrder(sourceFiles)
                ))).thenReturn(true);

        // when
        testObj.installApplications();

        // then
        FileUtils.forceDelete(sourceFolder);
        assertThat(destinationFolder.listFiles(File::isFile), emptyArray());
    }

    @Test
    public void shouldStoreInformationAboutUpdatedPlugin() throws IOException {
        final String pluginFileBase = "some.plugin.file.%d.jar";
        final String pluginContentBase = "plugin-%d";

        // given
        final File agileInstallationPlugins = new File(sourceFolder, PRODUCT_SOURCE_FOLDER);
        agileInstallationPlugins.mkdirs();
        final File[] sourceFiles = IntStream.range(0, 5).mapToObj(value ->
                        addFile(agileInstallationPlugins, format(pluginFileBase, value), format(pluginContentBase, value))
        ).toArray(File[]::new);
        when(versionDiscovery.getBundleNameAndVersion(any(File.class))).thenReturn(Optional.of(new BundlesVersionDiscovery.PluginIdentification("a", "0")));
        when(whatWasInstalled.wasApplicationSourceInstalled(
                appSource(
                        equalTo(PRODUCT_SOURCE_FOLDER),
                        arrayContainingInAnyOrder(sourceFiles)
                ))).thenReturn(false);

        // when
        testObj.installApplications();

        // then
        verify(whatWasInstalled).storeInstalledApplicationSource(
                appSource(
                        equalTo(PRODUCT_SOURCE_FOLDER),
                        arrayContainingInAnyOrder(sourceFiles)
                ),
                notNull(ReversibleFileOperations.class));
    }

    @Test
    public void shouldNotInstallAnyFilesIfStoringInstalledApplicationThrows() throws IOException {
        final String pluginFileBase = "some.plugin.file.%d.jar";
        final String pluginContentBase = "plugin-%d";

        // given
        final File agileInstallationPlugins = new File(sourceFolder, PRODUCT_SOURCE_FOLDER);
        agileInstallationPlugins.mkdirs();
        final File[] sourceFiles = IntStream.range(0, 5).mapToObj(value ->
                        addFile(agileInstallationPlugins, format(pluginFileBase, value), format(pluginContentBase, value))
        ).toArray(File[]::new);
        when(versionDiscovery.getBundleNameAndVersion(any(File.class))).thenReturn(Optional.of(new BundlesVersionDiscovery.PluginIdentification("a", "0")));
        when(whatWasInstalled.wasApplicationSourceInstalled(
                appSource(
                        equalTo(PRODUCT_SOURCE_FOLDER),
                        arrayContainingInAnyOrder(sourceFiles)
                ))).thenReturn(false);
        doThrow(IllegalStateException.class).when(whatWasInstalled).storeInstalledApplicationSource(any(), any());

        try {
            // when
            testObj.installApplications();
            fail("installApplications should throw exceptions when storeInstalledApplicationSource throws exception");
        } catch (final IllegalStateException x) {
            // then
            assertThat(destinationFolder.listFiles(File::isFile), emptyArray());
        }
    }

    @Test
    public void shouldNotReplaceExistingFilesWithSameNameEvenIfVersionNewer() throws IOException {
        // it's mainly performance optimization
        // when jira is restarted it will see again the same files and will try to copy them again
        // this is explicit statement that file name must contain version
        final String pluginFilename = "a-plugin-file.jar";
        final String oldContent = "old-content";
        final String newContent = "new-content";

        // given
        when(whatWasInstalled.wasApplicationSourceInstalled(any(ApplicationSource.class))).thenReturn(false);
        final File agileInstallationPlugins = new File(sourceFolder, PRODUCT_SOURCE_FOLDER);
        final File oldFile = addFile(destinationFolder, pluginFilename, oldContent);
        final File newFile = addFile(agileInstallationPlugins, pluginFilename, newContent);
        when(versionDiscovery.getBundleNameAndVersion(oldFile))
                .thenReturn(Optional.of(new BundlesVersionDiscovery.PluginIdentification("plugin", "1")));
        when(versionDiscovery.getBundleNameAndVersion(newFile))
                .thenReturn(Optional.of(new BundlesVersionDiscovery.PluginIdentification("plugin", "2")));

        // when
        testObj.installApplications();

        // then
        FileUtils.forceDelete(sourceFolder);
        assertFileContentEquals(destinationFolder, pluginFilename, oldContent);
    }

    @Test
    public void shouldCleanAllPluginsAfterFailure() throws IOException {
        final String pluginFilename = "a-plugin-file.jar";
        final String failPluginFilename = "fail-plugin-file.jar";
        final String content = "old-content";

        // given
        when(whatWasInstalled.wasApplicationSourceInstalled(any(ApplicationSource.class))).thenReturn(false);
        final File agileInstallationPlugins = new File(sourceFolder, PRODUCT_SOURCE_FOLDER);
        final File file = addFile(agileInstallationPlugins, pluginFilename, content);
        final File failFile = addFile(agileInstallationPlugins, failPluginFilename, content);
        when(versionDiscovery.getBundleNameAndVersion(file))
                .thenReturn(Optional.of(new BundlesVersionDiscovery.PluginIdentification("plugin", "2")));
        when(versionDiscovery.getBundleNameAndVersion(failFile))
                .thenReturn(Optional.of(new BundlesVersionDiscovery.PluginIdentification("aplugin", "2")));
        failFile.setReadable(false);

        try {
            // when
            testObj.installApplications();
            fail("installApplications should throw exceptions when non-readable file is found");
        } catch (final Exception x) {
            // then
            assertThat(destinationFolder.list(), arrayWithSize(0));
        }
    }

    @Test
    public void shouldRemoveOldPluginVersionWhenInstallingNewer() throws IOException {
        final String newPluginFilename = "a-plugin-file.jar";
        final String oldPluginFilename = "a-old-plugin-file.jar";
        final String newContent = "new-content";
        final String oldContent = "old-content";

        // given
        when(whatWasInstalled.wasApplicationSourceInstalled(any(ApplicationSource.class))).thenReturn(false);
        final File agileInstallationPlugins = new File(sourceFolder, PRODUCT_SOURCE_FOLDER);
        final File newFile = addFile(agileInstallationPlugins, newPluginFilename, newContent);
        final File oldFile = addFile(destinationFolder, oldPluginFilename, oldContent);
        when(versionDiscovery.getBundleNameAndVersion(oldFile))
                .thenReturn(Optional.of(new BundlesVersionDiscovery.PluginIdentification("plugin", "1")));
        when(versionDiscovery.getBundleNameAndVersion(newFile))
                .thenReturn(Optional.of(new BundlesVersionDiscovery.PluginIdentification("plugin", "2")));

        // when
        testObj.installApplications();

        // then
        FileUtils.forceDelete(sourceFolder);
        assertThat(oldFile, not(exists()));
        assertFileContentEquals(destinationFolder, newPluginFilename, newContent);
    }

    @Test
    public void shouldKeepOldFileIfNewFileVersionIsOlder() throws IOException {
        final String newPluginFilename = "a-plugin-file.jar";
        final String oldPluginFilename = "a-old-plugin-file.jar";
        final String newContent = "new-content";
        final String oldContent = "old-content";

        // given
        when(whatWasInstalled.wasApplicationSourceInstalled(any(ApplicationSource.class))).thenReturn(false);
        final File agileInstallationPlugins = new File(sourceFolder, PRODUCT_SOURCE_FOLDER);
        final File newFile = addFile(agileInstallationPlugins, newPluginFilename, newContent);
        final File oldFile = addFile(destinationFolder, oldPluginFilename, oldContent);
        when(versionDiscovery.getBundleNameAndVersion(oldFile))
                .thenReturn(Optional.of(new BundlesVersionDiscovery.PluginIdentification("plugin", "2")));
        when(versionDiscovery.getBundleNameAndVersion(newFile))
                .thenReturn(Optional.of(new BundlesVersionDiscovery.PluginIdentification("plugin", "1")));

        // when
        testObj.installApplications();

        // then
        FileUtils.forceDelete(sourceFolder);
        assertThat(new File(destinationFolder, newPluginFilename), not(exists()));
        assertFileContentEquals(destinationFolder, oldPluginFilename, oldContent);
    }

    @Test
    public void shouldKeepUnrelatedPluginsIntact() throws IOException {
        final String newPluginFilename = "a-plugin-file.jar";
        final String oldPluginFilename = "a-old-plugin-file.jar";
        final String newContent = "new-content";
        final String oldContent = "old-content";

        // given
        when(whatWasInstalled.wasApplicationSourceInstalled(any(ApplicationSource.class))).thenReturn(false);
        final File agileInstallationPlugins = new File(sourceFolder, PRODUCT_SOURCE_FOLDER);
        final File newFile = addFile(agileInstallationPlugins, newPluginFilename, newContent);
        final File oldFile = addFile(destinationFolder, oldPluginFilename, oldContent);
        when(versionDiscovery.getBundleNameAndVersion(oldFile))
                .thenReturn(Optional.of(new BundlesVersionDiscovery.PluginIdentification("plugin", "1")));
        when(versionDiscovery.getBundleNameAndVersion(newFile))
                .thenReturn(Optional.of(new BundlesVersionDiscovery.PluginIdentification("plugin-2", "2")));

        // when
        testObj.installApplications();

        // then
        FileUtils.forceDelete(sourceFolder);
        assertFileContentEquals(destinationFolder, oldPluginFilename, oldContent);
        assertFileContentEquals(destinationFolder, newPluginFilename, newContent);
    }

    @Test
    public void shouldRestoreOldPluginsWhenFailureOccurs() throws IOException {
        final String newPlugin1Filename = "a-plugin-1-file.jar";
        final String newPlugin2Filename = "a-plugin-2-file.jar";
        final String oldPluginFilename = "a-old-plugin-file.jar";
        final String newContent = "new-content";
        final String oldContent = "old-content";

        // given
        when(whatWasInstalled.wasApplicationSourceInstalled(any(ApplicationSource.class))).thenReturn(false);
        final File agileInstallationPlugins = new File(sourceFolder, PRODUCT_SOURCE_FOLDER);
        final File newFile1 = addFile(agileInstallationPlugins, newPlugin1Filename, newContent);
        final File newFile2 = addFile(agileInstallationPlugins, newPlugin2Filename, newContent);
        final File oldFile = addFile(destinationFolder, oldPluginFilename, oldContent);
        when(versionDiscovery.getBundleNameAndVersion(oldFile))
                .thenReturn(Optional.of(new BundlesVersionDiscovery.PluginIdentification("plugin", "1")));
        when(versionDiscovery.getBundleNameAndVersion(newFile1))
                .thenReturn(Optional.of(new BundlesVersionDiscovery.PluginIdentification("plugin", "2")));
        when(versionDiscovery.getBundleNameAndVersion(newFile2))
                .thenReturn(Optional.of(new BundlesVersionDiscovery.PluginIdentification("other-plugin", "1")));
        newFile2.setReadable(false);

        // when
        try {
            testObj.installApplications();
            fail("installApplications should throw exceptions when non-readable file is found");
        } catch (final Exception x) {
            assertThat(new File(destinationFolder, newPlugin1Filename), not(exists()));
            assertThat(new File(destinationFolder, newPlugin2Filename), not(exists()));
            assertFileContentEquals(destinationFolder, oldPluginFilename, oldContent);
        }
    }

    @Test
    public void shouldUseNewerVersionIfTargetContainsMoreThanOne() throws IOException {
        final String newPluginVersion2Name = "a-plugin-1-file.jar";
        final String oldPluginVersion1Name = "a-old-plugin-file-1.jar";
        final String newPluginVersion3Name = "a-old-plugin-file-3.jar";

        // given
        when(whatWasInstalled.wasApplicationSourceInstalled(any(ApplicationSource.class))).thenReturn(false);
        final File agileInstallationPlugins = new File(sourceFolder, PRODUCT_SOURCE_FOLDER);
        final File newPluginV2File = addFile(agileInstallationPlugins, newPluginVersion2Name, "");
        final File oldPluginV1File = addFile(destinationFolder, oldPluginVersion1Name, "");
        final File oldPluginV3File = addFile(destinationFolder, newPluginVersion3Name, "");
        when(versionDiscovery.getBundleNameAndVersion(newPluginV2File))
                .thenReturn(Optional.of(new BundlesVersionDiscovery.PluginIdentification("plugin", "2")));
        when(versionDiscovery.getBundleNameAndVersion(oldPluginV1File))
                .thenReturn(Optional.of(new BundlesVersionDiscovery.PluginIdentification("plugin", "1")));
        when(versionDiscovery.getBundleNameAndVersion(oldPluginV3File))
                .thenReturn(Optional.of(new BundlesVersionDiscovery.PluginIdentification("plugin", "3")));

        // when
        testObj.installApplications();

        // then
        assertThat(new File(destinationFolder, newPluginVersion2Name), not(exists()));
        assertThat(oldPluginV1File, exists());
        assertThat(oldPluginV3File, exists());
    }

    @Test
    public void shouldOnlyConsiderJarFiles() throws IOException {
        final String newPlugin1Filename = "a-plugin-1-file.blah";
        final String newPlugin2Filename = "a-plugin-2-file.jar";
        final String newContent = "new-content";

        // given
        when(whatWasInstalled.wasApplicationSourceInstalled(any(ApplicationSource.class))).thenReturn(false);
        final File agileInstallationPlugins = new File(sourceFolder, PRODUCT_SOURCE_FOLDER);
        final File newFile1 = addFile(agileInstallationPlugins, newPlugin1Filename, newContent);
        final File newFile2 = addFile(agileInstallationPlugins, newPlugin2Filename, newContent);
        when(versionDiscovery.getBundleNameAndVersion(newFile1))
                .thenThrow(new RuntimeException("Don't know how to get version of blah file."));
        when(versionDiscovery.getBundleNameAndVersion(newFile2))
                .thenReturn(Optional.of(new BundlesVersionDiscovery.PluginIdentification("plugin", "2")));

        // when
        testObj.installApplications();

        // then
        assertThat(new File(destinationFolder, newPlugin2Filename), exists());
        assertThat(new File(destinationFolder, newPlugin1Filename), not(exists()));
    }

    @Test
    public void existingFilesWithoutManifestShouldNotPreventInstallation() throws IOException {
        final String oldPluginName = "a-plugin-1-file.jar";
        final String oldPlugin2Name = "a-plugin-old-2-file.jar";
        final String newPluginFilename = "a-plugin-2-file.jar";
        final String newContent = "new-content";
        final String oldContent = "old-content";

        // given
        when(whatWasInstalled.wasApplicationSourceInstalled(any(ApplicationSource.class))).thenReturn(false);
        final File agileInstallationPlugins = new File(sourceFolder, PRODUCT_SOURCE_FOLDER);
        final File oldPluginFile = addFile(destinationFolder, oldPluginName, oldContent);
        final File oldPlugin2File = addFile(destinationFolder, oldPlugin2Name, oldContent);
        final File newFile = addFile(agileInstallationPlugins, newPluginFilename, newContent);
        when(versionDiscovery.getBundleNameAndVersion(oldPluginFile))
                .thenReturn(Optional.empty());
        when(versionDiscovery.getBundleNameAndVersion(oldPlugin2File))
                .thenReturn(Optional.of(new BundlesVersionDiscovery.PluginIdentification("plugin", "1")));
        when(versionDiscovery.getBundleNameAndVersion(newFile))
                .thenReturn(Optional.of(new BundlesVersionDiscovery.PluginIdentification("plugin", "2")));

        // when
        testObj.installApplications();

        // then
        assertThat(new File(destinationFolder, newPluginFilename), exists());
        assertThat(oldPluginFile, exists());
        assertThat(oldPlugin2File, not(exists()));
        assertFileContentEquals(destinationFolder, newPluginFilename, newContent);
    }

    @Test
    public void providingNewJarWithoutCorrectManifestShouldThrowException() throws IOException {
        final String oldPluginName = "a-plugin-old-2-file.jar";
        final String newPluginFilename = "a-plugin-2-file.jar";

        // given
        when(whatWasInstalled.wasApplicationSourceInstalled(any(ApplicationSource.class))).thenReturn(false);
        final File agileInstallationPlugins = new File(sourceFolder, PRODUCT_SOURCE_FOLDER);
        final File oldPluginFile = addFile(destinationFolder, oldPluginName, "");
        final File newFile = addFile(agileInstallationPlugins, newPluginFilename, "");
        when(versionDiscovery.getBundleNameAndVersion(oldPluginFile))
                .thenReturn(Optional.of(new BundlesVersionDiscovery.PluginIdentification("plugin", "1")));
        when(versionDiscovery.getBundleNameAndVersion(newFile))
                .thenReturn(Optional.empty());

        // expect
        exceptionRule.expectMessage(Matchers.startsWith("Source jar doesn't provide version information"));

        // when
        testObj.installApplications();
    }
}