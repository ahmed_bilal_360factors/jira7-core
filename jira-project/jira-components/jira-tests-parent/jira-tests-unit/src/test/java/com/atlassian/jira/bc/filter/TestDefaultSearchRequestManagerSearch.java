package com.atlassian.jira.bc.filter;

import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.instrumentation.InstrumentationListenerManager;
import com.atlassian.jira.instrumentation.InstrumentationLogger;
import com.atlassian.jira.issue.fields.layout.column.ColumnLayoutManager;
import com.atlassian.jira.issue.search.DefaultSearchRequestManager;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.issue.search.SearchRequestStore;
import com.atlassian.jira.issue.subscription.SubscriptionManager;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.portal.PortalPage;
import com.atlassian.jira.sharing.ShareManager;
import com.atlassian.jira.sharing.index.SharedEntityIndexer;
import com.atlassian.jira.sharing.search.SharedEntitySearchParameters;
import com.atlassian.jira.sharing.search.SharedEntitySearchParametersBuilder;
import com.atlassian.jira.sharing.search.SharedEntitySearchResult;
import com.atlassian.jira.sharing.search.SharedEntitySearcher;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.util.UserUtil;
import com.atlassian.jira.util.collect.MockCloseableIterable;
import com.atlassian.query.QueryImpl;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.util.Collections;
import java.util.List;

import static com.atlassian.jira.sharing.SharedEntityColumn.ID;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.when;

/**
 * Test the {@link com.atlassian.jira.issue.search.DefaultSearchRequestManager} searching methods.
 *
 * @since v3.13
 */
public class TestDefaultSearchRequestManagerSearch {
    @Rule
    public TestRule container = MockitoMocksInContainer.forTest(this);

    private ApplicationUser user;
    private SearchRequest searchRequest2;
    private SearchRequest searchRequest3;

    @Mock
    private SharedEntitySearcher searcher;
    @Mock
    private SharedEntityIndexer indexer;
    @Mock
    private ColumnLayoutManager columnLayoutManager;
    @Mock
    private SubscriptionManager subscriptionManager;
    @Mock
    private ShareManager shareManager;
    @Mock
    private SearchRequestStore searchRequestStore;
    @Mock
    private SearchService searchService;
    @Mock
    private UserUtil userUtil;

    @InjectMocks
    private DefaultSearchRequestManager searchRequestManager;

    @Before
    public void setUp() {
        user = new MockApplicationUser("testSearchAsUser");

        searchRequest2 = new SearchRequest(new QueryImpl(), user, "two", "two description", 2L, 0L);
        searchRequest3 = new SearchRequest(new QueryImpl(), user, "three", "three description", 3L, 0L);

        when(indexer.getSearcher(PortalPage.ENTITY_TYPE)).thenReturn(searcher);
    }

    /**
     * Check what happens when null search parameters are passed.
     */
    @Test
    public void testSearchNullPrameters() {
        try {
            searchRequestManager.search(null, (ApplicationUser) null, 0, 10);
            fail("An exception should be thrown on null search parametes.");
        } catch (final IllegalArgumentException expected) {

        }
    }

    /**
     * Check what happens when zero width is passed.
     */
    @Test
    public void testSearchZeroWidth() {
        try {
            searchRequestManager.search(new SharedEntitySearchParametersBuilder().toSearchParameters(), (ApplicationUser) null, 0, 0);
            fail("An exception should be thrown on invalid width.");
        } catch (final IllegalArgumentException expected) {

        }
    }

    /**
     * Check what happens on illegal offset
     */
    @Test
    public void testSearchIllegalOffset() {
        try {
            searchRequestManager.search(new SharedEntitySearchParametersBuilder().toSearchParameters(), (ApplicationUser) null, 0, -1);
            fail("An exception should be thrown on invalid width.");
        } catch (final IllegalArgumentException expected) {

        }
    }

    /**
     * Execute the search as a user.
     */
    @Test
    public void testSearchAsUser() {
        _testSearch(user, Lists.newArrayList(searchRequest2, searchRequest3));
    }

    /**
     * Execute the search as a user and expect no results.
     */
    @Test
    public void testSearchAsUserWithNoResults() {
        _testSearch(user, Collections.emptyList());
    }

    /**
     * Execute the search as the anonymous user.
     */
    @Test
    public void testSearchAsAnonymous() {
        _testSearch(null, Lists.newArrayList(searchRequest2));
    }

    private void _testSearch(final ApplicationUser user, final List expectedPages) {
        final SharedEntitySearchParametersBuilder builder = new SharedEntitySearchParametersBuilder();
        builder.setName("searchTest");
        builder.setSortColumn(ID, false);

        final SharedEntitySearchParameters searchParameters = builder.toSearchParameters();
        final SharedEntitySearchResult expectedResult = new SharedEntitySearchResult(new MockCloseableIterable(expectedPages), true, 100);

        when(searcher.search(searchParameters, user, 0, 100)).thenReturn(expectedResult);
        when(indexer.getSearcher(Mockito.anyObject())).thenReturn(searcher);

        // run the search.
        final SharedEntitySearchResult actualResult = searchRequestManager.search(searchParameters, user, 0, 100);

        // make sure the result is as expected.
        assertEquals(expectedResult.hasMoreResults(), actualResult.hasMoreResults());
        assertEquals(expectedResult.getResults(), actualResult.getResults());
    }

}
