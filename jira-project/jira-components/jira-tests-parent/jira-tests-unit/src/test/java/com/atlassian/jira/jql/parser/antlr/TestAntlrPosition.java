package com.atlassian.jira.jql.parser.antlr;

import org.antlr.runtime.CharStream;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Simple test for the {@link com.atlassian.jira.jql.parser.antlr.AntlrPosition}.
 *
 * @since v4.0
 */
public class TestAntlrPosition {
    @Test
    public void testCotr() throws Exception {
        final int charPos = 79548;
        final int index = 5;
        final int line = 6;
        final int tokenType = 628273;

        final CharStream stream = mock(CharStream.class);
        when(stream.index()).thenReturn(index);
        when(stream.getLine()).thenReturn(line);
        when(stream.getCharPositionInLine()).thenReturn(charPos);

        final AntlrPosition positon = new AntlrPosition(tokenType, stream);
        assertEquals(tokenType, positon.getTokenType());
        assertEquals(index, positon.getIndex());
        assertEquals(line, positon.getLineNumber());
        assertEquals(charPos, positon.getCharNumber());
    }
}
