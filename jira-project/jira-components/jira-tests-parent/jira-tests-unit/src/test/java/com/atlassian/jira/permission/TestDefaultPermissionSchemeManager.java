package com.atlassian.jira.permission;

import com.atlassian.cache.memory.MemoryCacheManager;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.association.NodeAssociationStore;
import com.atlassian.jira.event.permission.PermissionAddedEvent;
import com.atlassian.jira.event.permission.PermissionSchemeDeletedEvent;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.mock.component.MockComponentWorker;
import com.atlassian.jira.mock.issue.MockIssue;
import com.atlassian.jira.mock.ofbiz.MockGenericValue;
import com.atlassian.jira.mock.ofbiz.MockOfBizDelegator;
import com.atlassian.jira.ofbiz.FieldMap;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.project.DefaultProjectFactory;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectFactory;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.scheme.Scheme;
import com.atlassian.jira.scheme.SchemeEntity;
import com.atlassian.jira.scheme.SchemeFactory;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.security.ProjectWidePermission;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.security.groups.MockGroupManager;
import com.atlassian.jira.security.plugin.ProjectPermissionKey;
import com.atlassian.jira.security.type.CurrentAssignee;
import com.atlassian.jira.security.type.GroupDropdown;
import com.atlassian.jira.security.type.SecurityType;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.MockGroup;
import com.atlassian.jira.util.collect.MapBuilder;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import org.hamcrest.collection.IsEmptyCollection;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.ofbiz.core.entity.GenericEntityException;
import org.ofbiz.core.entity.GenericValue;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.atlassian.jira.permission.DefaultPermissionSchemeManager.SCHEME_ENTITY_NAME;
import static com.atlassian.jira.permission.ProjectPermissions.ADMINISTER_PROJECTS;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static java.util.Collections.singletonMap;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentCaptor.forClass;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Tests the DefaultPermissionSchemeManager class.
 *
 * @since v3.12
 */
@SuppressWarnings("deprecation")
public class TestDefaultPermissionSchemeManager {
    private static final List<GenericValue> NO_SCHEMES = emptyList();

    @Mock
    private EventPublisher mockEventPublisher;
    @Mock
    private GroupManager mockGroupManager;
    @Mock
    private NodeAssociationStore mockNodeAssociationStore;
    @Mock
    private PermissionContextFactory mockPermissionContextFactory;
    @Mock
    private PermissionTypeManager mockPermissionTypeManager;
    @Mock
    private ProjectManager mockProjectManager;
    @Mock
    private SchemeFactory mockSchemeFactory;

    private MockOfBizDelegator mockOfBizDelegator;
    private DefaultPermissionSchemeManager permissionSchemeManager;

    @Before
    public void setUp() {
        mockOfBizDelegator = new MockOfBizDelegator();
        MockitoAnnotations.initMocks(this);
        new MockComponentWorker().init()
                .addMock(OfBizDelegator.class, new MockOfBizDelegator())
                .addMock(ProjectFactory.class, new DefaultProjectFactory());
        permissionSchemeManager = new DefaultPermissionSchemeManager(
                mockProjectManager, mockPermissionTypeManager, mockPermissionContextFactory, mockOfBizDelegator,
                mockSchemeFactory, mockNodeAssociationStore, mockGroupManager, mockEventPublisher, new MemoryCacheManager());
    }

    @Test
    public void getSchemeShouldReturnNullWhenSchemeIdIsUnknown() throws Exception {
        // Set up
        final long unknownId = Long.MAX_VALUE;

        // Invoke and check
        assertNull(permissionSchemeManager.getScheme(unknownId));
    }

    @Test
    public void getSchemeShouldReturnSchemeWhenItsIdIsKnown() throws Exception {
        // Set up
        final long knownId = Long.MAX_VALUE;
        mockOfBizDelegator.createValue(SCHEME_ENTITY_NAME, singletonMap("id", knownId));

        // Invoke and check
        assertEquals(knownId, permissionSchemeManager.getScheme(knownId).get("id"));
    }

    @Test
    public void gettingSchemeByNameShouldReturnNullWhenMatchingSchemesListIsNull() throws Exception {
        // Set up
        final String schemeName = "MyScheme";

        // Invoke and check
        assertNull(permissionSchemeManager.getScheme(schemeName));
    }

    @Test
    public void gettingSchemeByNameShouldReturnNullWhenMatchingSchemesListIsEmpty() throws Exception {
        // Set up
        final String schemeName = "MyScheme";

        // Invoke and check
        assertNull(permissionSchemeManager.getScheme(schemeName));
    }

    @Test
    public void gettingSchemeByNameShouldReturnThatScheme() throws Exception {
        // Set up
        final String schemeName = "MyScheme";
        mockOfBizDelegator.createValue(SCHEME_ENTITY_NAME, singletonMap("name", schemeName));

        // Invoke and check
        assertEquals(schemeName, permissionSchemeManager.getScheme(schemeName).get("name"));
    }

    @Test
    public void schemeShouldNotBeReportedToExistWhenItDoesNot() throws Exception {
        // Set up
        final String schemeName = "MyScheme";

        // Invoke and check
        assertFalse("Scheme should not exist", permissionSchemeManager.schemeExists(schemeName));
    }

    @Test
    public void schemeShouldBeReportedToExistWhenItDoes() throws Exception {
        // Set up
        final String schemeName = "MyScheme";
        mockOfBizDelegator.createValue(SCHEME_ENTITY_NAME, singletonMap("name", schemeName));

        // Invoke and check
        assertTrue("Scheme should exist", permissionSchemeManager.schemeExists(schemeName));
    }

    @Test
    public void shouldBeAbleToCreateSchemeThatDoesNotAlreadyExist() throws Exception {
        // Set up
        final String schemeName = "This Name";
        final String schemeDescription = "Description";
        final Map<String, Object> expected = ImmutableMap.<String, Object>of("name", schemeName, "description", schemeDescription);

        // Invoke
        final GenericValue createdScheme = permissionSchemeManager.createScheme(schemeName, schemeDescription);

        // Check
        assertEquals(expected, createdScheme.getFields(Lists.newArrayList("name", "description")));
    }

    @Test(expected = GenericEntityException.class)
    public void shouldNotBeAbleToCreateSchemeWithSameNameAsExistingScheme() throws Exception {
        // Set up
        final String schemeName = "someScheme";
        mockOfBizDelegator.createValue(SCHEME_ENTITY_NAME, ImmutableMap.of("name", schemeName));

        // Invoke
        permissionSchemeManager.createScheme(schemeName, "AnyDescription");
    }

    @Test
    public void shouldBeAbleToUpdateScheme() throws GenericEntityException {
        // Set up
        final GenericValue mockScheme = mock(GenericValue.class);

        // Invoke
        permissionSchemeManager.updateScheme(mockScheme);

        // Check
        verify(mockScheme).store();
    }

    @Test
    public void shouldBeAbleToDeleteScheme() throws GenericEntityException {
        // Set up
        final long schemeId = 123;
        GenericValue mockScheme = new MockGenericValue(SCHEME_ENTITY_NAME, schemeId);

        mockOfBizDelegator.createValue(SCHEME_ENTITY_NAME, singletonMap("id", schemeId));
        mockOfBizDelegator.addRelatedMap("ChildSchemePermissions", mockScheme);
        when(mockSchemeFactory.getScheme(eq(mockScheme))).thenReturn(mock(Scheme.class));

        // Invoke
        permissionSchemeManager.deleteScheme(schemeId);

        // Check
        verify(mockNodeAssociationStore).removeAssociationsFromSink(mockScheme);
        assertThat(mockOfBizDelegator.findByField(SCHEME_ENTITY_NAME, "id", schemeId), IsEmptyCollection.empty());
        assertThat(mockOfBizDelegator.getRelated("ChildSchemePermissions", mockScheme), IsEmptyCollection.empty());
        final ArgumentCaptor<PermissionSchemeDeletedEvent> eventCaptor =
                forClass(PermissionSchemeDeletedEvent.class);
        verify(mockEventPublisher).publish(eventCaptor.capture());
        assertEquals(schemeId, eventCaptor.getValue().getId().longValue());
    }

    @Test
    public void testHasSchemePermissionWithUserForProject() throws GenericEntityException {
        // Set up
        final ApplicationUser user = new MockApplicationUser("John");
        final Project project = new MockProject(7L);

        final SecurityType schemeType = mock(SecurityType.class);
        when(schemeType.isValidForPermission(ProjectPermissions.BROWSE_PROJECTS)).thenReturn(false);
        when(schemeType.isValidForPermission(ADMINISTER_PROJECTS)).thenReturn(true);
        when(schemeType.hasPermission(project, null, user, false)).thenReturn(true);

        final Map<String, SecurityType> types = singletonMap("typename", schemeType);

        final PermissionTypeManager mockPermissionTypeManager = mock(PermissionTypeManager.class);
        when(mockPermissionTypeManager.getTypes()).thenReturn(types);

        final PermissionSchemeEntry permissionSchemeEntry = new PermissionSchemeEntry(3L, 12L, "testPermKey", "typename", null);
        final DefaultPermissionSchemeManager defaultPermissionSchemeManager =
                createPermissionSchemeManager(mockPermissionTypeManager, singletonList(permissionSchemeEntry));

        assertFalse(defaultPermissionSchemeManager.hasSchemePermission(ProjectPermissions.BROWSE_PROJECTS, project, user, false));
        assertTrue(defaultPermissionSchemeManager.hasSchemePermission(ADMINISTER_PROJECTS, project, user, false));
    }

    @Test
    public void testIsGroupConfigured() throws GenericEntityException {
        assertFalse("Group should be configured", permissionSchemeManager.isGroupUsed("my-group"));
        mockOfBizDelegator.createValue("SchemePermissions", FieldMap.build("id", 10400, "scheme", 10000,
                "permission", null, "type", "group", "parameter", "my-group", "permission_key", "ADMINISTER_PROJECTS"));

        assertTrue("Group should be configured", permissionSchemeManager.isGroupUsed("my-group"));
    }

    @Test
    public void testHasSchemePermissionWithUserForIssue() throws GenericEntityException {
        // Set up
        final ApplicationUser user = new MockApplicationUser("John");
        final Issue issue = new MockIssue(7L);

        final SecurityType schemeType = mock(SecurityType.class);
        when(schemeType.isValidForPermission(ProjectPermissions.BROWSE_PROJECTS)).thenReturn(false);
        when(schemeType.isValidForPermission(ADMINISTER_PROJECTS)).thenReturn(true);
        when(schemeType.hasPermission(issue, null, user, false)).thenReturn(true);

        final Map<String, SecurityType> types = singletonMap("typename", schemeType);

        final PermissionTypeManager mockPermissionTypeManager = mock(PermissionTypeManager.class);
        when(mockPermissionTypeManager.getTypes()).thenReturn(types);

        final PermissionSchemeEntry permissionSchemeEntry = new PermissionSchemeEntry(3L, 12L, "testPermKey", "typename", null);
        final DefaultPermissionSchemeManager defaultPermissionSchemeManager =
                createPermissionSchemeManager(mockPermissionTypeManager, singletonList(permissionSchemeEntry));

        assertFalse(defaultPermissionSchemeManager.hasSchemePermission(ProjectPermissions.BROWSE_PROJECTS, issue, user, false));
        assertTrue(defaultPermissionSchemeManager.hasSchemePermission(ADMINISTER_PROJECTS, issue, user, false));
    }

    @Test
    public void testHasSchemePermissionWithUserAndNullSecurityTypesForProject() {
        // Set up
        final ApplicationUser user = new MockApplicationUser("John");
        final Project project = new MockProject(7L);

        final Map<String, SecurityType> types = singletonMap("typename", null);

        final PermissionTypeManager mockPermissionTypeManager = mock(PermissionTypeManager.class);
        when(mockPermissionTypeManager.getTypes()).thenReturn(types);

        final PermissionSchemeEntry permissionSchemeEntry = new PermissionSchemeEntry(3L, 12L, "testPermKey", "typename", null);
        final DefaultPermissionSchemeManager defaultPermissionSchemeManager = createPermissionSchemeManager(mockPermissionTypeManager, singletonList(permissionSchemeEntry));

        assertFalse(defaultPermissionSchemeManager.hasSchemePermission(ProjectPermissions.BROWSE_PROJECTS, project, user, false));
        assertFalse(defaultPermissionSchemeManager.hasSchemePermission(ADMINISTER_PROJECTS, project, user, false));
    }

    @Test
    public void testHasSchemePermissionWithUserAndNullSecurityTypesForIssue() {
        // Set up
        final ApplicationUser user = new MockApplicationUser("John");
        final Issue issue = new MockIssue(7L);

        final Map<String, SecurityType> types = singletonMap("typename", null);

        final PermissionTypeManager mockPermissionTypeManager = mock(PermissionTypeManager.class);
        when(mockPermissionTypeManager.getTypes()).thenReturn(types);

        final PermissionSchemeEntry permissionSchemeEntry = new PermissionSchemeEntry(3L, 12L, "testPermKey", "typename", null);
        final DefaultPermissionSchemeManager defaultPermissionSchemeManager = createPermissionSchemeManager(mockPermissionTypeManager, singletonList(permissionSchemeEntry));

        assertFalse(defaultPermissionSchemeManager.hasSchemePermission(ProjectPermissions.BROWSE_PROJECTS, issue, user, false));
        assertFalse(defaultPermissionSchemeManager.hasSchemePermission(ADMINISTER_PROJECTS, issue, user, false));
    }

    @Test
    public void testHasSchemePermissionWithAnonymousUserForProject() {
        final Project project = new MockProject(7L);

        // type 1 does not give permission
        final SecurityType schemeType1 = mock(SecurityType.class);
        when(schemeType1.isValidForPermission(ADMINISTER_PROJECTS)).thenReturn(true);
        when(schemeType1.hasPermission(project, null)).thenReturn(false);

        // type 2 does give permission
        final SecurityType schemeType2 = mock(SecurityType.class);
        when(schemeType2.isValidForPermission(ADMINISTER_PROJECTS)).thenReturn(true);
        when(schemeType2.hasPermission(project, null)).thenReturn(true);

        final Map<String, SecurityType> types = new HashMap<String, SecurityType>();
        types.put("type1", schemeType1);

        final PermissionTypeManager mockPermissionTypeManager = mock(PermissionTypeManager.class);
        when(mockPermissionTypeManager.getTypes()).thenReturn(types);

        final List<PermissionSchemeEntry> permissions = new ArrayList<PermissionSchemeEntry>();
        final PermissionSchemeEntry permissionSchemeEntry = new PermissionSchemeEntry(3L, 12L, "testPermKey", "type1", null);
        permissions.add(permissionSchemeEntry);
        final DefaultPermissionSchemeManager defaultPermissionSchemeManager =
                createPermissionSchemeManager(mockPermissionTypeManager, permissions);

        assertFalse(defaultPermissionSchemeManager.hasSchemePermission(ADMINISTER_PROJECTS, project, null, false));
        final PermissionSchemeEntry permissionSchemeEntry2 = new PermissionSchemeEntry(4L, 12L, "testPermKey", "type2", null);
        permissions.add(permissionSchemeEntry2);
        types.put("type2", schemeType2);
        assertTrue(defaultPermissionSchemeManager.hasSchemePermission(ADMINISTER_PROJECTS, project, null, false));
    }

    @Test
    public void testHasSchemePermissionWithAnonymousUserForIssue() {
        final Issue issue = new MockIssue(7L);

        // type 1 does not give permission
        final SecurityType schemeType1 = mock(SecurityType.class);
        when(schemeType1.isValidForPermission(ADMINISTER_PROJECTS)).thenReturn(true);
        when(schemeType1.hasPermission(issue, null)).thenReturn(false);

        // type 2 does give permission
        final SecurityType schemeType2 = mock(SecurityType.class);
        when(schemeType2.isValidForPermission(ADMINISTER_PROJECTS)).thenReturn(true);
        when(schemeType2.hasPermission(issue, null)).thenReturn(true);

        final Map<String, SecurityType> types = new HashMap<String, SecurityType>();
        types.put("type1", schemeType1);

        final PermissionTypeManager mockPermissionTypeManager = mock(PermissionTypeManager.class);
        when(mockPermissionTypeManager.getTypes()).thenReturn(types);

        final List<PermissionSchemeEntry> permissions = new ArrayList<PermissionSchemeEntry>();
        final PermissionSchemeEntry permissionSchemeEntry = new PermissionSchemeEntry(3L, 12L, "testPermKey", "type1", null);
        permissions.add(permissionSchemeEntry);
        final DefaultPermissionSchemeManager defaultPermissionSchemeManager =
                createPermissionSchemeManager(mockPermissionTypeManager, permissions);

        assertFalse(defaultPermissionSchemeManager.hasSchemePermission(ADMINISTER_PROJECTS, issue, null, false));
        final PermissionSchemeEntry permissionSchemeEntry2 = new PermissionSchemeEntry(4L, 12L, "testPermKey", "type2", null);
        permissions.add(permissionSchemeEntry2);
        types.put("type2", schemeType2);
        assertTrue(defaultPermissionSchemeManager.hasSchemePermission(ADMINISTER_PROJECTS, issue, null, false));
    }

    @Test
    public void creatingSchemeEntityWithProjectPermissionKeyPublishesPermissionAddedEvent() throws GenericEntityException {
        assertThatAdminPermissionAddedEventWasPublished(ADMINISTER_PROJECTS);
    }

    @Test
    public void creatingSchemeEntityWithStringPermissionKeyPublishesPermissionAddedEvent() throws GenericEntityException {
        assertThatAdminPermissionAddedEventWasPublished(ADMINISTER_PROJECTS.permissionKey());
    }

    @Test
    public void creatingSchemeEntityWithIntegerPermissionKeyPublishesPermissionAddedEvent() throws GenericEntityException {
        assertThatAdminPermissionAddedEventWasPublished(Permissions.PROJECT_ADMIN);
    }

    private void assertThatAdminPermissionAddedEventWasPublished(Object entityTypeId) throws GenericEntityException {
        createEntity(entityTypeId);

        ArgumentCaptor<PermissionAddedEvent> eventCaptor = forClass(PermissionAddedEvent.class);
        verify(mockEventPublisher).publish(eventCaptor.capture());
        PermissionAddedEvent event = eventCaptor.getValue();

        assertThat(event.getEntityTypeId(), equalTo((Object) ADMINISTER_PROJECTS));
        assertThat(event.getParameter(), equalTo("parameter"));
        assertThat(event.getSchemeId(), is(1L));
        assertThat(event.getType(), equalTo("type"));
    }

    @Test
    public void createSchemeEntityWithProjectPermissionKey() throws GenericEntityException {
        assertCreatedSchemeEntity(ADMINISTER_PROJECTS);
    }

    @Test
    public void createSchemeEntityWithStringPermissionKey() throws GenericEntityException {
        assertCreatedSchemeEntity(ADMINISTER_PROJECTS.permissionKey());
    }

    @Test
    public void createSchemeEntityWithIntegerPermissionKey() throws GenericEntityException {
        assertCreatedSchemeEntity(Permissions.PROJECT_ADMIN);
    }

    private void assertCreatedSchemeEntity(Object entityTypeId) throws GenericEntityException {
        GenericValue createdValue = createEntity(entityTypeId);

        assertThat(createdValue.getString("permissionKey"), equalTo(ADMINISTER_PROJECTS.permissionKey()));
        assertThat(createdValue.getLong("scheme"), is(1L));
        assertThat(createdValue.getString("type"), equalTo("type"));
    }

    private GenericValue createEntity(Object entityTypeId) throws GenericEntityException {
        return permissionSchemeManager.createSchemeEntity(new MockGenericValue("scheme", 1L),
                new SchemeEntity("type", "parameter", entityTypeId));
    }

    @Test
    public void testHasProjectWidePermission_NO_ISSUES() {
        // Set up the security types and Permission Scheme
        final PermissionTypeManager permissionTypeManager = buildPermissionTypeManager();
        final List<PermissionSchemeEntry> permissionSchemeEntries = Arrays.asList(
                permissionSchemeEntry(ProjectPermissions.TRANSITION_ISSUES.permissionKey(), "group", "tigers"),
                permissionSchemeEntry(ProjectPermissions.TRANSITION_ISSUES.permissionKey(), "group", "lions")
        );
        // Construct class under test
        DefaultPermissionSchemeManager defaultPermissionSchemeManager = createPermissionSchemeManager(permissionTypeManager, permissionSchemeEntries);
        // Create a mock GroupManager and add to ComponentAccessor
        new MockComponentWorker().addMock(GroupManager.class, new MockGroupManager()).init();

        final Project project = new MockProject(1L, "ABC");
        final ApplicationUser user = new MockApplicationUser("wendy");

        final ProjectWidePermission projectWidePermission = defaultPermissionSchemeManager.hasProjectWidePermission(ProjectPermissions.TRANSITION_ISSUES, project, user, false);
        // permissions are based on groups, but user does not belong to the groups
        assertEquals(ProjectWidePermission.NO_ISSUES, projectWidePermission);
    }

    @Test
    public void testHasProjectWidePermission_ISSUE_SPECIFIC() {
        // Set up the security types and Permission Scheme
        final PermissionTypeManager permissionTypeManager = buildPermissionTypeManager();
        final List<PermissionSchemeEntry> permissionSchemeEntries = Arrays.asList(
                permissionSchemeEntry(ProjectPermissions.TRANSITION_ISSUES.permissionKey(), "group", "tigers"),
                permissionSchemeEntry(ProjectPermissions.TRANSITION_ISSUES.permissionKey(), "assignee", null)
        );
        // Construct class under test
        DefaultPermissionSchemeManager defaultPermissionSchemeManager = createPermissionSchemeManager(permissionTypeManager, permissionSchemeEntries);
        // Create a mock GroupManager and add to ComponentAccessor
        new MockComponentWorker().addMock(GroupManager.class, new MockGroupManager()).init();

        final Project project = new MockProject(1L, "ABC");
        final ApplicationUser user = new MockApplicationUser("wendy");

        final ProjectWidePermission projectWidePermission = defaultPermissionSchemeManager.hasProjectWidePermission(ProjectPermissions.TRANSITION_ISSUES, project, user, false);
        // user does not belong to group tigers but might be assignee
        assertEquals(ProjectWidePermission.ISSUE_SPECIFIC, projectWidePermission);
    }

    @Test
    public void testHasProjectWidePermission_ALL_ISSUES() throws Exception {
        // Set up the security types and Permission Scheme
        final PermissionTypeManager permissionTypeManager = buildPermissionTypeManager();
        final List<PermissionSchemeEntry> permissionSchemeEntries = Arrays.asList(
                permissionSchemeEntry(ProjectPermissions.TRANSITION_ISSUES.permissionKey(), "group", "tigers"),
                permissionSchemeEntry(ProjectPermissions.TRANSITION_ISSUES.permissionKey(), "group", null)
        );
        // Construct class under test
        DefaultPermissionSchemeManager defaultPermissionSchemeManager = createPermissionSchemeManager(permissionTypeManager, permissionSchemeEntries);
        // Create a mock GroupManager and add to ComponentAccessor
        final MockGroupManager groupManager = new MockGroupManager();
        new MockComponentWorker().addMock(GroupManager.class, groupManager).init();

        final Project project = new MockProject(1L, "ABC");
        final ApplicationUser user = new MockApplicationUser("wendy");
        mockGroupManager.addUserToGroup(user, new MockGroup("tigers"));

        final ProjectWidePermission projectWidePermission = defaultPermissionSchemeManager.hasProjectWidePermission(ProjectPermissions.TRANSITION_ISSUES, project, user, false);
        // user does belong to group tigers
        assertEquals(ProjectWidePermission.ALL_ISSUES, projectWidePermission);
    }

    private PermissionSchemeEntry permissionSchemeEntry(final String permissionKey, final String type, final String parameter) {
        return new PermissionSchemeEntry(null, 12L, permissionKey, type, parameter);
    }

    private PermissionTypeManager buildPermissionTypeManager() {
        final PermissionTypeManager permissionTypeManager = new PermissionTypeManager();
        // Add in a couple of security types: one at issue level, one at project level
        permissionTypeManager.setSecurityTypes(MapBuilder.<String, SecurityType>newBuilder()
                        .add("group", new GroupDropdown(null))
                        .add("assignee", new CurrentAssignee(null))
                        .toMap()
        );
        return permissionTypeManager;
    }

    private DefaultPermissionSchemeManager createPermissionSchemeManager(
            final PermissionTypeManager permissionTypeManager, final List<PermissionSchemeEntry> permissionSchemeEntries) {
        return new DefaultPermissionSchemeManager(null, permissionTypeManager, null, null, null, null, null, null, new MemoryCacheManager()) {
            @Override
            public Long getSchemeIdFor(final Project project) {
                return 12L;
            }

            @Override
            public List<PermissionSchemeEntry> getPermissionSchemeEntries(long schemeId, @Nonnull final ProjectPermissionKey permissionKey) {
                return permissionSchemeEntries;
            }
        };
    }

    private DefaultPermissionSchemeManager createDefaultPermissionSchemeManager(
            final PermissionTypeManager permissionTypeManager, final List<GenericValue> permissions) {
        return new DefaultPermissionSchemeManager(null, permissionTypeManager, null, null, null, null, null, null, new MemoryCacheManager()) {
            public List<GenericValue> getEntities(GenericValue scheme, String permissionKey) {
                return permissions;
            }
        };
    }
}
