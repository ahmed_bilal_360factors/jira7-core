package com.atlassian.jira.plugin.userformat;

import com.atlassian.jira.plugin.profile.UserFormat;
import com.atlassian.jira.plugin.userformat.configuration.UserFormatTypeConfiguration;
import com.atlassian.jira.plugin.userformat.descriptors.UserFormatModuleDescriptors;
import com.atlassian.jira.user.UserKeyService;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @since v4.4
 */
public class TestDefaultUserFormats {
    @Rule
    public MockitoRule mockito = MockitoJUnit.rule();

    @Mock
    private UserFormatTypeConfiguration mockUserFormatTypeConfiguration;
    @Mock
    private UserFormatModuleDescriptors mockUserFormatModuleDescriptors;
    @Mock
    private UserKeyService userKeyService;
    private DefaultUserFormats defaultUserFormats;

    @Before
    public void setUp() throws Exception {
        defaultUserFormats = new DefaultUserFormats(mockUserFormatTypeConfiguration, mockUserFormatModuleDescriptors, userKeyService);
    }

    @Test
    public void forTypeShouldShouldReturnNullWhenTheTypeHasNoConfiguredFormatAndThereAreNoFormatsThatCanHandleIt() {

        when(mockUserFormatTypeConfiguration.containsType("a-non-configured-type")).thenReturn(false);
        when(mockUserFormatTypeConfiguration.getUserFormatKeyForType("a-non-configured-type")).thenReturn(null);
        when(mockUserFormatModuleDescriptors.defaultFor("a-non-configured-type")).thenReturn(null);

        final CachingUserFormat cachingUserFormat = (CachingUserFormat) defaultUserFormats.forType("a-non-configured-type");
        final UserFormat actualUserFormatForANonConfiguredType = cachingUserFormat.getDelegate();

        assertNull(actualUserFormatForANonConfiguredType);
    }

    @Test
    public void forTypeShouldReturnTheConfiguredUserFormatForThatTypeIfThereIsAnEntryForIt() {
        final UserFormatModuleDescriptor aConfiguredTypeModuleDescriptor = mock(UserFormatModuleDescriptor.class);
        final UserFormat expectedUserFormatForAConfiguredType = mock(UserFormat.class);
        when(aConfiguredTypeModuleDescriptor.getModule()).thenReturn(expectedUserFormatForAConfiguredType);

        when(mockUserFormatTypeConfiguration.containsType("a-configured-type")).thenReturn(true);
        when(mockUserFormatTypeConfiguration.getUserFormatKeyForType("a-configured-type")).thenReturn("a-configured-type-module-key");
        when(mockUserFormatModuleDescriptors.withKey("a-configured-type-module-key")).thenReturn(aConfiguredTypeModuleDescriptor);

        final CachingUserFormat cachingUserFormat = (CachingUserFormat) defaultUserFormats.forType("a-configured-type");
        final UserFormat actualUserFormatForAConfiguredType = cachingUserFormat.getDelegate();

        assertEquals(expectedUserFormatForAConfiguredType, actualUserFormatForAConfiguredType);
    }


    /**
     * When retrieving a user format for a type that has no configured format module descriptor a default should be
     * returned. A configuration entry should be set to the default format module descriptor that was found.
     */
    @Test
    public void forTypeShouldReturnTheDefaultFormatWhenThereIsNoConfigurationEntryAndThereIsAFormatThatCanHandleIt() {
        final UserFormatModuleDescriptor defaulModuleDescriptorForANonConfiguredType = mock(UserFormatModuleDescriptor.class);
        final UserFormat defaultUserFormatForANonConfiguredType = mock(UserFormat.class);
        when(defaulModuleDescriptorForANonConfiguredType.getModule()).thenReturn(defaultUserFormatForANonConfiguredType);
        when(defaulModuleDescriptorForANonConfiguredType.getCompleteKey()).thenReturn("default-module-descriptor-key");

        when(mockUserFormatTypeConfiguration.containsType("a-non-configured-type")).thenReturn(false);
        when(mockUserFormatTypeConfiguration.getUserFormatKeyForType("a-non-configured-type")).thenReturn(null);
        when(mockUserFormatModuleDescriptors.defaultFor("a-non-configured-type")).thenReturn(defaulModuleDescriptorForANonConfiguredType);

        mockUserFormatTypeConfiguration.setUserFormatKeyForType("a-non-configured-type", "default-module-descriptor-key");

        final CachingUserFormat cachingUserFormat = (CachingUserFormat) defaultUserFormats.forType("a-non-configured-type");
        final UserFormat actualUserFormatForANonConfiguredType = cachingUserFormat.getDelegate();

        assertEquals(defaultUserFormatForANonConfiguredType, actualUserFormatForANonConfiguredType);
    }
}
