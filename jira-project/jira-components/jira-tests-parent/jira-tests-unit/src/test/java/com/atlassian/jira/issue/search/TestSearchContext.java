package com.atlassian.jira.issue.search;

import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoContainer;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

/**
 * Tests for the SearchContext.
 */
public class TestSearchContext {
    @Rule
    public MockitoContainer mockitoContainer = new MockitoContainer(this);

    @AvailableInContainer
    @Mock
    private ConstantsManager constantsManager;

    private SearchContext searchContext1;
    private SearchContext searchContext2;

    @Before
    public void setUp() {
        searchContext1 = new SearchContextImpl();
        searchContext2 = new SearchContextImpl();
    }

    @Test
    public void testEquals() {
        assertThat("sc1 equal to sc2", searchContext1, equalTo(searchContext2));
        assertThat("sc2 equal to sc1", searchContext2, equalTo(searchContext1));
        assertThat("sc1 equal to sc1", searchContext1, equalTo(searchContext1));
        assertThat("sc2 equal to sc2", searchContext2, equalTo(searchContext2));
    }

    @Test
    public void testHashCode() {
        assertEquals("sc1.hashcode equal to sc2.hashcode", searchContext1.hashCode(), searchContext2.hashCode());
    }
}
