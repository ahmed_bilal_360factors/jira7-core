package com.atlassian.jira.web.filters;

import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.config.properties.JiraProperties;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.mock.servlet.MockFilterConfig;
import com.atlassian.jira.startup.InstantUpgradeManager;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

public class InstantUpgradeHoldingFilterTest {
    @Rule
    public MockitoContainer container = MockitoMocksInContainer.rule(this);
    
    @AvailableInContainer
    @Mock
    private InstantUpgradeManager upgradeManager;
    @Mock
    @AvailableInContainer
    private FeatureManager featureManager;
    @Mock
    @AvailableInContainer
    private JiraProperties jiraProperties;
    
    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpServletResponse response;
    @Mock
    private FilterChain filterChain;

    // use mockito spy to override behaviour of InstantUpgradeHoldingFilter.isFullContainerInitialised() method
    private InstantUpgradeHoldingFilter filter = spy(new InstantUpgradeHoldingFilter());

    @Before
    public void setUp() throws ServletException {
        MockFilterConfig mockFilterConfig = new MockFilterConfig("InstantUpgradeHoldingFilter");
        mockFilterConfig.addInitParam("permittedPaths","/something , /status , /boom");
        filter.init(mockFilterConfig);
        stubRequest("www.example.com", "/jira", "/path");
        when(request.getMethod()).thenReturn("GET");
        when(upgradeManager.getState()).thenReturn(InstantUpgradeManager.State.LATE_STARTUP);
        when(jiraProperties.getLong(eq("holding.filter.timeout.seconds"), anyLong())).thenReturn(60L);
        when(filter.isFullContainerInitialised()).thenReturn(true);
    }

    @Test
    public void shouldNotHoldIfJiraContextNotInitialised() throws IOException, ServletException, InterruptedException {
        when(filter.isFullContainerInitialised()).thenReturn(false);
        filter.doFilter(request, response, filterChain);
        verify(filterChain).doFilter(request, response);
        verify(upgradeManager, never()).waitTillFullyStarted(anyLong(), any(TimeUnit.class));
    }

    @Test
    public void shouldNotHoldIfInstantUpgradeIsDisabled() throws IOException, ServletException, InterruptedException {
        when(upgradeManager.getState()).thenReturn(InstantUpgradeManager.State.DISABLED);
        filter.doFilter(request, response, filterChain);
        verify(filterChain).doFilter(request, response);
        verify(upgradeManager, never()).waitTillFullyStarted(anyLong(), any(TimeUnit.class));
    }

    @Test
    public void shouldNotHoldApplicationStatus() throws IOException, ServletException, InterruptedException {
        stubRequest("www.example.com","/jira", "/status");
        filter.doFilter(request, response, filterChain);
        verify(filterChain).doFilter(request, response);
        verify(upgradeManager, never()).waitTillFullyStarted(anyLong(), any(TimeUnit.class));
    }

    @Test
    public void shouldHoldAndTimeout() throws IOException, ServletException, InterruptedException {
        stubRequest("www.example.com","/jira", "/secure/Dashboard.jspa");
        when(upgradeManager.waitTillFullyStarted(anyLong(), any(TimeUnit.class))).thenReturn(false); // simulate timeout
        filter.doFilter(request, response, filterChain);
        verify(upgradeManager).waitTillFullyStarted(60L, TimeUnit.SECONDS);
        verify(filterChain, never()).doFilter(request, response);
        verify(response).sendError(510);
    }

    @Test
    public void shouldHoldAndReleaseWhenFullyStarted() throws IOException, ServletException, InterruptedException {
        stubRequest("www.example.com","/jira", "/secure/Dashboard.jspa");
        when(upgradeManager.waitTillFullyStarted(anyLong(), any(TimeUnit.class))).thenReturn(true); // simulate full start
        filter.doFilter(request, response, filterChain);
        verify(upgradeManager).waitTillFullyStarted(60L, TimeUnit.SECONDS);
        verify(filterChain).doFilter(request, response);
    }

    @Test
    public void shouldHoldWhileLateStarting() throws InterruptedException, IOException, ServletException {
        CountDownLatch shouldFinishStarting = new CountDownLatch(1);
        CountDownLatch isWaitingTillFullyStarted = new CountDownLatch(1);
        doAnswer(invocationOnMock -> {
            isWaitingTillFullyStarted.countDown();
            shouldFinishStarting.await();
            return true;
        }).when(upgradeManager).waitTillFullyStarted(anyLong(), any(TimeUnit.class));
        Thread holdingFilter = new Thread() {
            public void run() {
                try {
                    if (!filter.isHoldingRequests()) {
                        isWaitingTillFullyStarted.countDown();
                        fail("Filter should be holding requests"); // To prevent test from hanging
                    }
                    filter.doFilter(request, response, filterChain);
                } catch (Exception e) {
                    e.printStackTrace();
                    fail(e.toString());
                }
            }
        };

        holdingFilter.start();
        
        isWaitingTillFullyStarted.await();
        verify(upgradeManager).waitTillFullyStarted(anyLong(), any(TimeUnit.class));
        verifyNoMoreInteractions(filterChain);

        shouldFinishStarting.countDown();
        holdingFilter.join();
        verify(filterChain).doFilter(request, response);
    }

    @Test
    public void errorIfItTimesOut() throws InterruptedException, IOException, ServletException {
        CountDownLatch isWaitingTillFullyStarted = new CountDownLatch(1);
        CountDownLatch jiraFinishedStarting = new CountDownLatch(1);
        
        doAnswer(invocationOnMock -> {
            isWaitingTillFullyStarted.countDown();
            jiraFinishedStarting.await();
            return false; // To indicate it has timed out
        }).when(upgradeManager).waitTillFullyStarted(anyLong(), any(TimeUnit.class));

        Thread holdingFilter = new Thread() {
            public void run() {
                try {
                    if (!filter.isHoldingRequests()) {
                        isWaitingTillFullyStarted.countDown();
                        fail("Filter should be holding requests"); // To prevent test from hanging
                    }
                    filter.doFilter(request, response, filterChain);
                } catch (Exception e) {
                    e.printStackTrace();
                    fail(e.toString());
                }
            }
        };
        
        holdingFilter.start();
        isWaitingTillFullyStarted.await();
        verify(upgradeManager).waitTillFullyStarted(anyLong(), any(TimeUnit.class));

        jiraFinishedStarting.countDown();
        holdingFilter.join();
        verifyNoMoreInteractions(filterChain);
        verify(response).sendError(510);
    }

    private void stubRequest(String host, String context, String path) {
        when(request.getContextPath()).thenReturn(context);
        when(request.getRequestURI()).thenReturn(context+path);
        when(request.getRequestURL()).thenReturn(new StringBuffer("http://"+host+context+path+"?key=value#fragment"));
    }
}