package com.atlassian.jira.jql.resolver;

import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.issue.priority.PriorityImpl;
import com.atlassian.jira.mock.ofbiz.MockGenericValue;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Collections;

import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestPriorityResolver {
    @Rule
    public MockitoRule mocks = MockitoJUnit.rule();

    @Mock
    private ConstantsManager mockConstantsManager;

    @Test
    public void testGetAll() throws Exception {
        when(mockConstantsManager.getPriorityObjects()).thenReturn(Collections.emptyList());

        final PriorityResolver priorityResolver = new PriorityResolver(mockConstantsManager);

        assertThat(priorityResolver.getAll(), empty());
    }

    @Test
    public void testNameIsCorrect() throws Exception {
        when(mockConstantsManager.getConstantByNameIgnoreCase(ConstantsManager.PRIORITY_CONSTANT_TYPE, "test"))
                .thenReturn(new PriorityImpl(new MockGenericValue("blah"), null, null, null));

        final PriorityResolver priorityResolver = new PriorityResolver(mockConstantsManager);
        assertThat(priorityResolver.nameExists("test"), is(true));
    }
}
