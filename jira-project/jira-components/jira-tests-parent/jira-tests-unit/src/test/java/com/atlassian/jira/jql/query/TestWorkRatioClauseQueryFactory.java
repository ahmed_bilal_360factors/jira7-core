package com.atlassian.jira.jql.query;

import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.jql.operand.JqlOperandResolver;
import com.atlassian.query.clause.TerminalClauseImpl;
import com.atlassian.query.operator.Operator;
import org.apache.lucene.search.Query;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestWorkRatioClauseQueryFactory {
    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @Mock
    private JqlOperandResolver jqlOperandResolver;
    @Mock
    private ApplicationProperties applicationProperties;
    @Mock
    private GenericClauseQueryFactory genericClauseQueryFactory;

    @Test
    public void testTimeTrackingDisabled() throws Exception {
        when(applicationProperties.getOption(APKeys.JIRA_OPTION_TIMETRACKING)).thenReturn(false);

        final WorkRatioClauseQueryFactory factory = new WorkRatioClauseQueryFactory(jqlOperandResolver, applicationProperties);
        final QueryFactoryResult result = factory.getQuery(null, new TerminalClauseImpl("clause", Operator.EQUALS, "a"));
        assertEquals(QueryFactoryResult.createFalseResult(), result);
    }

    @Test
    public void testTimeTrackingEnabled() throws Exception {
        final TerminalClauseImpl clause = new TerminalClauseImpl("clause", Operator.EQUALS, "a");
        when(applicationProperties.getOption(APKeys.JIRA_OPTION_TIMETRACKING)).thenReturn(true);
        final QueryFactoryResult queryFactoryResult = new QueryFactoryResult(mock(Query.class));
        when(genericClauseQueryFactory.getQuery(null, clause)).thenReturn(queryFactoryResult);
        final WorkRatioClauseQueryFactory factory = new WorkRatioClauseQueryFactory(jqlOperandResolver, applicationProperties) {
            @Override
            GenericClauseQueryFactory createGenericClauseQueryFactory(final JqlOperandResolver operandResolver, final List<OperatorSpecificQueryFactory> operatorFactories) {
                return genericClauseQueryFactory;
            }
        };

        assertSame(queryFactoryResult, factory.getQuery(null, clause));
    }
}
