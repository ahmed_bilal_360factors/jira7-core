package com.atlassian.jira.task;

import com.atlassian.jira.logging.QuietCountingLogger;
import org.apache.log4j.Logger;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * @since v3.13
 */

public class TestTimeBasedLogSink {
    private static final String MESSAGE_A = "MessageA";
    private static final String SUB_TASK_A = "SubTaskA";

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @Mock
    private TaskProgressSink mockSink;

    @Test
    public void testTimeBasedLogSink() {
        try {
            new TimeBasedLogSink(null, "Description", 1000, TaskProgressSink.NULL_SINK);
            fail("Should throw a NPE.");
        } catch (final RuntimeException e) {
            //expected.
        }

        try {
            new TimeBasedLogSink(createLogger(), null, 100, TaskProgressSink.NULL_SINK);
            fail("Should throw a NPE.");
        } catch (final RuntimeException e) {
            //expected.
        }

        try {
            new TimeBasedLogSink(createLogger(), "Description", -10, TaskProgressSink.NULL_SINK);
            fail("Should throw an IllegalArgumentException.");
        } catch (final RuntimeException e) {
            //expected.
        }

    }

    private Logger createLogger() {
        return Logger.getLogger(getClass());
    }

    @Test
    public void testMakeProgress() {
        final QuietCountingLogger quietCountingLogger = QuietCountingLogger.create(getClass().getName());

        final TimeBasedLogSink timeSink = new TimeBasedLogSink(quietCountingLogger, "Description", Long.MAX_VALUE, mockSink);
        timeSink.makeProgress(50, SUB_TASK_A, MESSAGE_A);
        timeSink.makeProgress(50, SUB_TASK_A, null);
        timeSink.makeProgress(51, null, MESSAGE_A);

        Mockito.verify(mockSink).makeProgress(50, SUB_TASK_A, MESSAGE_A);
        Mockito.verify(mockSink).makeProgress(50, SUB_TASK_A, null);
        Mockito.verify(mockSink).makeProgress(51, null, MESSAGE_A);

        assertEquals(2, quietCountingLogger.getCount());
    }

    /**
     * With a time difference of ZERO, all events should be logged.
     */

    @Test
    public void testMakeProgressTimeZero() {
        final QuietCountingLogger quietCountingLogger = QuietCountingLogger.create(getClass().getName());

        final TimeBasedLogSink timeSink = new TimeBasedLogSink(quietCountingLogger, "Description", 0, mockSink);
        timeSink.makeProgress(50, SUB_TASK_A, MESSAGE_A);
        timeSink.makeProgress(50, SUB_TASK_A, null);
        timeSink.makeProgress(51, null, MESSAGE_A);

        Mockito.verify(mockSink).makeProgress(50, SUB_TASK_A, MESSAGE_A);
        Mockito.verify(mockSink).makeProgress(50, SUB_TASK_A, null);
        Mockito.verify(mockSink).makeProgress(51, null, MESSAGE_A);

        assertEquals(3, quietCountingLogger.getCount());
    }
}
