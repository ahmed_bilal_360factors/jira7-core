package com.atlassian.jira.bc.filter;

import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.template.soy.SoyTemplateRendererProvider;
import com.atlassian.plugin.web.WebInterfaceManager;
import com.atlassian.plugin.web.model.WebPanel;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import org.hamcrest.Matcher;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;
import java.util.Map;

import static com.atlassian.jira.bc.filter.FilterDeletionWarningViewProviderImpl.TEMPLATE_NAME;
import static com.atlassian.jira.bc.filter.FilterDeletionWarningViewProviderImpl.TEMPLATES_RESOURCE_KEY;
import static com.atlassian.jira.bc.filter.FilterDeletionWarningViewProviderImpl.FILTER_DELETION_WARNING_PANEL_LOCATION;
import static com.google.common.collect.Lists.newArrayList;
import static java.util.Arrays.asList;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.collection.IsMapContaining.hasEntry;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.anyMap;
import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestFilterDeletionWarningViewProviderImpl {
    private static final String MOCK_SOY_TEMPLATE_EXPECTED_OUTPUT = "<div>warningmessages</div>";
    
    private FilterDeletionWarningViewProviderImpl filterDeletionWarningViewProvider;
    
    @Mock
    private SearchRequest searchRequest;
    
    @Mock
    private WebInterfaceManager webInterfaceManager;
    
    @Mock
    private SoyTemplateRenderer soyTemplateRenderer;

    @Mock
    private JiraAuthenticationContext jiraAuthenticationContext;
    
    private WebPanel webPanel1;
    private WebPanel webPanel2;
    @Mock
    private SoyTemplateRendererProvider soyTemplateRendererProvider;

    @Before
    public void setUp() throws Exception {
        when(soyTemplateRendererProvider.getRenderer()).thenReturn(soyTemplateRenderer);
        filterDeletionWarningViewProvider = new FilterDeletionWarningViewProviderImpl(webInterfaceManager, soyTemplateRendererProvider, jiraAuthenticationContext);

        webPanel1 = mock(WebPanel.class);
        webPanel2 = mock(WebPanel.class);

        when(webPanel1.getHtml(anyMap())).thenReturn("<p>HTML1</p>");
        when(webPanel2.getHtml(anyMap())).thenReturn("<p>HTML2</p>");

        when(soyTemplateRenderer.render(eq(TEMPLATES_RESOURCE_KEY), eq(TEMPLATE_NAME), anyMap()))
                .thenReturn(MOCK_SOY_TEMPLATE_EXPECTED_OUTPUT);
        
        when(webInterfaceManager.getDisplayableWebPanels(eq(FILTER_DELETION_WARNING_PANEL_LOCATION), anyMap())).thenReturn(newArrayList(webPanel1, webPanel2));
    }

    @Test
    public void testGetWarningHtml() throws Exception {
        String result = filterDeletionWarningViewProvider.getWarningHtml(searchRequest);

        assertSoyTemplateRendererCalled(asList("<p>HTML1</p>", "<p>HTML2</p>"));
        assertThat(result, is(MOCK_SOY_TEMPLATE_EXPECTED_OUTPUT));
    }

    @Test
    public void testGetWarningHtmlWithEmptyWebPanel() throws Exception {
        when(webPanel1.getHtml(anyMap())).thenReturn("");

        String result = filterDeletionWarningViewProvider.getWarningHtml(searchRequest);

        assertSoyTemplateRendererCalled(asList("<p>HTML2</p>"));
        assertThat(result, is(MOCK_SOY_TEMPLATE_EXPECTED_OUTPUT));
    }

    private void assertSoyTemplateRendererCalled(List<String> expectedWarnings) {
        Matcher<Map<String, Object>> containsCorrectHtml = (Matcher) hasEntry("warningMessages", expectedWarnings);
        Mockito.verify(soyTemplateRenderer).render(eq(TEMPLATES_RESOURCE_KEY), eq(TEMPLATE_NAME), argThat(containsCorrectHtml));
    }
}