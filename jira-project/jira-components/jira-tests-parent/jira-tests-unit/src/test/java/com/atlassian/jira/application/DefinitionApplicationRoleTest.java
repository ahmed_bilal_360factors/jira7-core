package com.atlassian.jira.application;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.user.MockGroup;
import com.google.common.collect.ImmutableSet;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.annotation.Nullable;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static com.atlassian.extras.common.LicensePropertiesConstants.UNLIMITED_USERS;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class DefinitionApplicationRoleTest {
    @Mock
    @AvailableInContainer
    ApplicationRoleDefinitions definitions;

    @Test(expected = IllegalArgumentException.class)
    public void constructorFailsWithDefinition() {
        new DefinitionApplicationRole(definitions, null, groups("one"), groups("one"), 0, false);
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructorFailsWithNullGroups() {
        new DefinitionApplicationRole(definitions, newDef("id", "name"), null, groups("one"), 0, false);
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructorFailsWithNullInGroups() {
        new DefinitionApplicationRole(definitions, newDef("id", "name"), groups("def"), groups("def", null), 0, false);
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructorFailsWithNullDefaultGroups() {
        new DefinitionApplicationRole(definitions, newDef("id", "name"), null, groups(), 0, false);
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructorFailsWithNullInDefaultGroups() {
        new DefinitionApplicationRole(definitions, newDef("id", "name"), groups("def", null), groups("def"), 0, false);
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructorFailsWithDefaultGroupNotListedInGroups() {
        new DefinitionApplicationRole(definitions, newDef("id", "name"), groups("one", "two"), groups("one", "three"), 0, false);
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructorFailsWithIllegalNumberOfLicenseSeats() {
        new DefinitionApplicationRole(definitions, newDef("id", "name"), groups("one", "two"), groups("one"), -22, false);
    }

    @Test
    public void equalsOnlyListensToId() {
        List<ApplicationRole> roles = Arrays.<ApplicationRole>asList(
                newRole(definitions, "id", "name", groups("one"), groups(), false),
                newRole(definitions, "id", "name", groups("one"), groups(), true),
                newRole(definitions, "id", "name", groups("two", "thee"), groups(), false),
                newRole(definitions, "id", "name", groups("two", "thee"), groups(), true),
                newRole(definitions, "id", "name", groups("two", "three"), groups("three"), false),
                newRole(definitions, "id", "otherName", groups("two", "three"), groups("three"), false));

        for (ApplicationRole left : roles) {
            for (ApplicationRole right : roles) {
                assertThat(left.equals(right), equalTo(true));
            }
        }

        List<ApplicationRole> otherRoles = Arrays.<ApplicationRole>asList(
                newRole(definitions, "id.abc", "name", groups("one"), groups(), false),
                newRole(definitions, "id.abc", "name", groups("one"), groups(), true),
                newRole(definitions, "id.abc", "name", groups("two", "thee"), groups(), false),
                newRole(definitions, "id.abc", "name", groups("two", "thee"), groups(), true),
                newRole(definitions, "id.abc", "name", groups("two", "three"), groups("three"), false),
                newRole(definitions, "id.abc", "otherName", groups("two", "three"), groups("three"), false));

        for (ApplicationRole left : roles) {
            for (ApplicationRole right : otherRoles) {
                assertThat(left.equals(right), equalTo(false));
            }
        }
    }

    @Test
    public void hashListensToId() {
        List<ApplicationRole> roles = Arrays.<ApplicationRole>asList(
                newRole(definitions, "id", "name", groups("one"), groups(), false),
                newRole(definitions, "id", "name", groups("one"), groups(), true),
                newRole(definitions, "id", "name", groups("two", "thee"), groups(), false),
                newRole(definitions, "id", "name", groups("two", "thee"), groups(), true),
                newRole(definitions, "id", "name", groups("two", "three"), groups("three"), false),
                newRole(definitions, "id", "otherName", groups("two", "three"), groups("three"), false));

        for (ApplicationRole left : roles) {
            for (ApplicationRole right : roles) {
                assertThat(left.hashCode() == right.hashCode(), equalTo(true));
            }
        }
    }

    @Test
    public void getGroupsReturnsGroups() {
        assertThat(newRole(definitions, "id", "name", groups("one"), groups(), false).getGroups(),
                contains(group("one")));
        assertThat(newRole(definitions, "id", "name", groups("one", "two"), groups(), false).getGroups(),
                containsInAnyOrder(group("one"), group("two")));
    }

    @Test
    public void getDefaultGroupsReturnsCorrectDefaults() {
        assertTrue(newRole(definitions, "id", "name", groups("one"), groups(), false).getDefaultGroups().isEmpty());
        assertThat(newRole(definitions, "id", "name", groups("one", "two"), groups("two"), false).getDefaultGroups(),
                containsInAnyOrder(group("two")));
        assertThat(newRole(definitions, "id", "name", groups("one", "two"), groups("two", "one"), false).getDefaultGroups(),
                containsInAnyOrder(group("two"), group("one")));
    }

    @Test
    public void getIdReturnsIdFromDef() {
        assertThat(newRole(definitions, "id", "name", groups("one"), groups(), false).getKey(), equalTo(ApplicationKey.valueOf("id")));
    }

    @Test
    public void getNameReturnsNameFromDef() {
        assertThat(newRole(definitions, "id", "name", groups("one"), groups(), false).getName(), equalTo("name"));
    }

    @Test
    public void getSelectedByDefaultReturnsSelectedByDefault() {
        assertThat(newRole(definitions, "id", "name", groups(), groups(), true).isSelectedByDefault(), equalTo(true));
        assertThat(newRole(definitions, "id", "name", groups(), groups(), false).isSelectedByDefault(), equalTo(false));
    }

    @Test
    public void getDefinedReturnsDefined() {
        MockApplicationRoleDefinition mockDef = newDef("id", "name");
        DefinitionApplicationRole mockRole = new DefinitionApplicationRole(definitions, mockDef, groups("one", "two"), groups("one"), 1, false);

        when(definitions.isDefined(mockDef.key())).thenReturn(true);
        assertThat(mockRole.isDefined(), equalTo(true));
    }

    @Test
    public void getDefinedReturnsUndefined() {
        MockApplicationRoleDefinition mockDef = newDef("id", "name");
        DefinitionApplicationRole mockRole = new DefinitionApplicationRole(definitions, mockDef, groups("one", "two"), groups("one"), 1, false);

        when(definitions.isDefined(mockDef.key())).thenReturn(true);
        assertThat(mockRole.isDefined(), equalTo(true));
    }

    @Test
    public void withGroupsUpdatesGroupInformation() {
        final ApplicationRole orig = newRole(definitions, "id", "name", groups("one"), groups(), false);
        final ApplicationRole newRole = orig.withGroups(groups("one", "two"), groups());

        assertThat(orig, new ApplicationRoleMatcher().key("id").name("name").groupNames("one").seats(UNLIMITED_USERS));
        assertThat(newRole, new ApplicationRoleMatcher().key("id").name("name").groupNames("one", "two").seats(UNLIMITED_USERS));
    }

    @Test
    public void withGroupsUpdatesDefaultGroupInformation() {
        final ApplicationRole orig = newRole(definitions, "id", "name", groups("one"), groups(), false);
        final ApplicationRole newRole = orig.withGroups(groups("one", "two"), groups("two"));

        assertThat(orig, new ApplicationRoleMatcher().key("id").name("name").groupNames("one").seats(UNLIMITED_USERS));
        assertThat(newRole, new ApplicationRoleMatcher()
                .key("id").name("name").groupNames("one", "two").defaultGroupNames("two").seats(UNLIMITED_USERS));

        final ApplicationRole noneRole = newRole.withGroups(groups("one", "two"), groups());

        assertThat(newRole, new ApplicationRoleMatcher().key("id").name("name")
                .groupNames("one", "two").defaultGroupNames("two").seats(UNLIMITED_USERS));

        assertThat(noneRole, new ApplicationRoleMatcher().key("id").name("name")
                .groupNames("one", "two").seats(UNLIMITED_USERS));
    }

    @Test
    public void withSelectedByDefaultUpdateSelectedByDefaultInformation() {
        final ApplicationRole orig = newRole(definitions, "id", "name", groups(), groups(), false);
        final ApplicationRole newRole = orig.withSelectedByDefault(true);

        assertThat(orig, new ApplicationRoleMatcher().key("id").name("name").seats(UNLIMITED_USERS));
        assertThat(newRole, new ApplicationRoleMatcher().key("id").name("name").selectedByDefault(true).seats(UNLIMITED_USERS));
    }

    @Test(expected = IllegalArgumentException.class)
    public void withGroupsFailsWithNullGroups() {
        final ApplicationRole idApplicationRole = newRole(definitions, "id", "name", groups("one"), groups("one"), false);
        idApplicationRole.withGroups(null, groups());
    }

    @Test(expected = IllegalArgumentException.class)
    public void withGroupsFailsWithNullInGroups() {
        final ApplicationRole idApplicationRole = newRole(definitions, "id", "name", groups("abc"), groups("abc"), false);
        idApplicationRole.withGroups(groups("abc", null), groups());
    }

    @Test(expected = IllegalArgumentException.class)
    public void withGroupsFailsWithNullDefaults() {
        final ApplicationRole idApplicationRole = newRole(definitions, "id", "name", groups(), groups(), false);
        idApplicationRole.withGroups(groups("abc"), null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void withGroupsFailsWithNullInDefaults() {
        final ApplicationRole idApplicationRole = newRole(definitions, "id", "name", groups(), groups(), false);
        idApplicationRole.withGroups(groups("abc"), groups("abc", null));
    }

    @Test(expected = IllegalArgumentException.class)
    public void withGroupsFailsWithDefaultGroupNotInRole() {
        final ApplicationRole idApplicationRole = newRole(definitions, "id", "name", groups("one"), groups("one"), false);
        idApplicationRole.withGroups(groups("abc"), groups("def"));
    }

    private static DefinitionApplicationRole newRole(ApplicationRoleDefinitions definitions, String id, String name, Iterable<Group> groups,
                                                     Iterable<Group> defaultGroups, boolean selectedByDefault) {
        return new DefinitionApplicationRole(definitions, newDef(id, name), groups, defaultGroups, UNLIMITED_USERS, selectedByDefault);
    }

    private static MockApplicationRoleDefinition newDef(String id, String name) {
        return new MockApplicationRoleDefinition(id, name);
    }

    private static Set<Group> groups(@Nullable String... groups) {
        if (groups == null) {
            return ImmutableSet.of();
        } else {
            return Arrays.stream(groups)
                    .map(DefinitionApplicationRoleTest::group)
                    .collect(Collectors.toSet());
        }
    }

    private static Group group(@Nullable final String name) {
        return name == null ? null : new MockGroup(name);
    }

    private static Iterable<Group> groups() {
        return ImmutableSet.of();
    }
}