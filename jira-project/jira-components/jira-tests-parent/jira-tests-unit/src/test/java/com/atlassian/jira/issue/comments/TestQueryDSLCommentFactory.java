package com.atlassian.jira.issue.comments;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.model.querydsl.QAction;
import com.atlassian.jira.model.querydsl.QProjectRole;
import com.atlassian.jira.security.roles.QueryDSLProjectRoleFactory;
import com.querydsl.core.Tuple;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.sql.Timestamp;
import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestQueryDSLCommentFactory {
    @Rule
    public MockitoRule rule = MockitoJUnit.rule();
    
    private QueryDSLCommentFactory queryDSLCommentFactory;

    private static final Long COMMENT_ID = 10l;
    private static final String COMMENT_AUTHOR = "author";
    private static final String COMMENT_UPDATE_AUTHOR = "update_author";
    private static final String COMMENT_BODY = "ACTION body";
    private static final String COMMENT_GROUPLEVEL = "grouplevel";
    private static final Long COMMENT_ROLE_LEVEL = 20l;
    private static final Timestamp COMMENT_CREATED = new Timestamp(System.currentTimeMillis());
    private static final Timestamp COMMENT_UPDATED = new Timestamp(System.currentTimeMillis());
    private static final Long COMMENT_ISSUE_ID = 20l;
    private static final String PROJECT_ROLE_NAME = "project role name";
    private static final String PROJECT_ROLE_DESCRIPTION = "description";

    @Before
    public void setUp() throws Exception {
        queryDSLCommentFactory = new QueryDSLCommentFactory(new QueryDSLProjectRoleFactory());
    }

    @Test
    public void testCreatingCommentFromTuple() throws Exception {
        Issue issue = mock(Issue.class);
        Comment comment = queryDSLCommentFactory.createComment(issue, mockTuple());

        assertEquals(COMMENT_ID, comment.getId());
        assertEquals(COMMENT_AUTHOR, comment.getAuthorKey());
        assertEquals(COMMENT_UPDATE_AUTHOR, comment.getUpdateAuthor());
        assertEquals(COMMENT_BODY, comment.getBody());
        assertEquals(new Date(COMMENT_CREATED.getTime()), comment.getCreated());
        assertEquals(new Date(COMMENT_UPDATED.getTime()), comment.getUpdated());
        assertEquals(COMMENT_ROLE_LEVEL, comment.getRoleLevelId());
        assertEquals(COMMENT_GROUPLEVEL, comment.getGroupLevel());

        assertEquals(PROJECT_ROLE_NAME, comment.getRoleLevel().getName());
        assertEquals(PROJECT_ROLE_DESCRIPTION, comment.getRoleLevel().getDescription());
    }

    private static Tuple mockTuple() {
        final Tuple tuple = mock(Tuple.class);
        when(tuple.get(QAction.ACTION.id)).thenReturn(COMMENT_ID);
        when(tuple.get(QAction.ACTION.author)).thenReturn(COMMENT_AUTHOR);
        when(tuple.get(QAction.ACTION.updateauthor)).thenReturn(COMMENT_UPDATE_AUTHOR);
        when(tuple.get(QAction.ACTION.body)).thenReturn(COMMENT_BODY);
        when(tuple.get(QAction.ACTION.level)).thenReturn(COMMENT_GROUPLEVEL);
        when(tuple.get(QAction.ACTION.rolelevel)).thenReturn(COMMENT_ROLE_LEVEL);
        when(tuple.get(QAction.ACTION.created)).thenReturn(COMMENT_CREATED);
        when(tuple.get(QAction.ACTION.updated)).thenReturn(COMMENT_UPDATED);
        when(tuple.get(QAction.ACTION.issue)).thenReturn(COMMENT_ISSUE_ID);

        when(tuple.get(QProjectRole.PROJECT_ROLE.id)).thenReturn(COMMENT_ROLE_LEVEL);
        when(tuple.get(QProjectRole.PROJECT_ROLE.name)).thenReturn(PROJECT_ROLE_NAME);
        when(tuple.get(QProjectRole.PROJECT_ROLE.description)).thenReturn(PROJECT_ROLE_DESCRIPTION);
        return tuple;
    }
}