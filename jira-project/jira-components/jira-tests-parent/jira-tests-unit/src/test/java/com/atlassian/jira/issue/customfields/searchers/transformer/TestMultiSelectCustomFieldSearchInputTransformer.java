package com.atlassian.jira.issue.customfields.searchers.transformer;

import com.atlassian.jira.action.issue.customfields.option.MockOption;
import com.atlassian.jira.bc.issue.search.QueryContextConverter;
import com.atlassian.jira.issue.customfields.option.Option;
import com.atlassian.jira.issue.customfields.view.CustomFieldParams;
import com.atlassian.jira.issue.customfields.view.CustomFieldParamsImpl;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.search.ClauseNames;
import com.atlassian.jira.issue.search.SearchContext;
import com.atlassian.jira.issue.transport.FieldValuesHolder;
import com.atlassian.jira.issue.transport.impl.FieldValuesHolderImpl;
import com.atlassian.jira.jql.operand.JqlOperandResolver;
import com.atlassian.jira.jql.operand.QueryLiteral;
import com.atlassian.jira.jql.util.JqlSelectOptionsUtil;
import com.atlassian.jira.jql.validator.MockJqlOperandResolver;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.collect.CollectionBuilder;
import com.atlassian.jira.util.collect.MapBuilder;
import com.atlassian.query.Query;
import com.atlassian.query.QueryImpl;
import com.atlassian.query.clause.Clause;
import com.atlassian.query.clause.TerminalClause;
import com.atlassian.query.clause.TerminalClauseImpl;
import com.atlassian.query.operand.EmptyOperand;
import com.atlassian.query.operand.MultiValueOperand;
import com.atlassian.query.operand.SingleValueOperand;
import com.atlassian.query.operator.Operator;
import com.google.common.collect.ImmutableList;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import static com.atlassian.jira.jql.operand.SimpleLiteralFactory.createLiteral;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestMultiSelectCustomFieldSearchInputTransformer {
    @Rule
    public final MethodRule initMockito = MockitoJUnit.rule();

    @Mock
    private CustomField customField;
    @Mock
    private JqlSelectOptionsUtil jqlSelectOptionsUtil;
    @Mock
    private SearchContext searchContext;
    @Mock
    private QueryContextConverter queryContextConverter;
    @Mock
    private CustomFieldInputHelper customFieldInputHelper;

    private ApplicationUser theUser = null;
    private JqlOperandResolver jqlOperandResolver;
    private final String url = "cf_100";
    private final ClauseNames clauseNames = new ClauseNames("cf[100]");

    private MultiSelectCustomFieldSearchInputTransformer transformer;

    @Before
    public void setUp() throws Exception {
        when(customField.getId()).thenReturn(url);
        jqlOperandResolver = MockJqlOperandResolver.createSimpleSupport();

        transformer = new MultiSelectCustomFieldSearchInputTransformer(url, clauseNames, customField, jqlOperandResolver, jqlSelectOptionsUtil, queryContextConverter, customFieldInputHelper);
    }

    @Test
    public void testGetSearchClauseNoValues() throws Exception {
        FieldValuesHolder holder = new FieldValuesHolderImpl();
        assertNull(transformer.getSearchClause(null, holder));
    }

    @Test
    public void testGetSearchClauseEmptyParams() throws Exception {
        final CustomFieldParamsImpl params = new CustomFieldParamsImpl();
        FieldValuesHolder holder = new FieldValuesHolderImpl(MapBuilder.newBuilder(url, params).toMap());
        assertNull(transformer.getSearchClause(null, holder));
    }

    @Test
    public void testGetSearchClauseParamsHasOnlyInvalidValues() throws Exception {
        final CustomFieldParamsImpl params = new CustomFieldParamsImpl(customField, CollectionBuilder.newBuilder("").asCollection());
        FieldValuesHolder holder = new FieldValuesHolderImpl(MapBuilder.newBuilder(url, params).toMap());
        assertNull(transformer.getSearchClause(null, holder));
    }

    @Test
    public void testGetSearchClauseParamsHappyPath() throws Exception {
        when(customField.getUntranslatedName()).thenReturn("ABC");
        when(customFieldInputHelper.getUniqueClauseName(theUser, clauseNames.getPrimaryName(), "ABC")).thenReturn(clauseNames.getPrimaryName());
        when(jqlSelectOptionsUtil.getOptionById(1000L)).thenReturn(MockOption._getMockParentOption());
        when(jqlSelectOptionsUtil.getOptionById(1001L)).thenReturn(MockOption._getMockParent2Option());

        final CustomFieldParamsImpl params = new CustomFieldParamsImpl(customField, CollectionBuilder.newBuilder("-1", "", "1000", "1001").asCollection());
        FieldValuesHolder holder = new FieldValuesHolderImpl(MapBuilder.newBuilder(url, params).toMap());

        final Clause result = transformer.getSearchClause(null, holder);
        final TerminalClause expectedResult = new TerminalClauseImpl(clauseNames.getPrimaryName(), Operator.IN, new MultiValueOperand("cars", "2"));
        assertEquals(expectedResult, result);
    }

    @Test
    public void testGetParamsFromSearchRequestNoWhereClause() throws Exception {
        Query query = new QueryImpl();
        assertNull(transformer.getParamsFromSearchRequest(null, query, searchContext));
    }

    @Test
    public void testGetParamsFromSearchRequestNoValues() throws Exception {
        Query query = new QueryImpl(new TerminalClauseImpl("blah", Operator.EQUALS, "blah"));
        assertNull(transformer.getParamsFromSearchRequest(null, query, searchContext));
    }

    @Test
    public void testGetParamsFromSearchRequestEmptyLiteral() throws Exception {
        // given:
        Query query = new QueryImpl(new TerminalClauseImpl(clauseNames.getPrimaryName(), Operator.IS, EmptyOperand.EMPTY));

        // when:
        final CustomFieldParams result = transformer.getParamsFromSearchRequest(null, query, searchContext);

        // then:
        assertNull(result);
    }

    @Test
    public void testGetParamsFromSearchRequestUnsupportedOperators() throws Exception {
        Query query1 = new QueryImpl(new TerminalClauseImpl(clauseNames.getPrimaryName(), Operator.IS_NOT, "x"));
        Query query2 = new QueryImpl(new TerminalClauseImpl(clauseNames.getPrimaryName(), Operator.NOT_EQUALS, "x"));
        Query query3 = new QueryImpl(new TerminalClauseImpl(clauseNames.getPrimaryName(), Operator.LESS_THAN_EQUALS, "x"));
        Query query4 = new QueryImpl(new TerminalClauseImpl(clauseNames.getPrimaryName(), Operator.NOT_IN, new MultiValueOperand("x", "y")));

        final MultiSelectCustomFieldSearchInputTransformer transformer = new MultiSelectCustomFieldSearchInputTransformer(url, clauseNames, customField, jqlOperandResolver, jqlSelectOptionsUtil, queryContextConverter, customFieldInputHelper);

        assertNull(transformer.getParamsFromSearchRequest(null, query1, searchContext));
        assertNull(transformer.getParamsFromSearchRequest(null, query2, searchContext));
        assertNull(transformer.getParamsFromSearchRequest(null, query3, searchContext));
        assertNull(transformer.getParamsFromSearchRequest(null, query4, searchContext));
    }

    @Test
    public void testGetParamsFromSearchRequestHappyPath() throws Exception {
        final QueryLiteral literal1 = createLiteral("value1");
        final QueryLiteral literal2 = createLiteral("value2");
        final Option option1 = new MockOption(null, null, null, "Value 1", null, 10L);
        final Option option2 = new MockOption(null, null, null, "Value 2", null, 20L);

        Query query = new QueryImpl(new TerminalClauseImpl(clauseNames.getPrimaryName(), Operator.IN, new MultiValueOperand(literal1, literal2)));

        when(jqlSelectOptionsUtil.getOptions(customField, literal1, false)).thenReturn(singletonList(option1));
        when(jqlSelectOptionsUtil.getOptions(customField, literal2, false)).thenReturn(ImmutableList.of(option1, option2));

        final CustomFieldParams result = transformer.getParamsFromSearchRequest(null, query, searchContext);
        final CustomFieldParams expectedResult = new CustomFieldParamsImpl(customField, CollectionBuilder.newBuilder("20", "10").asList());
        assertEquals(expectedResult, result);
    }

    @Test
    public void testGetParamsFromSearchRequestNullLiterals() throws Exception {
        final SingleValueOperand operand = new SingleValueOperand("value1");
        final TerminalClauseImpl clause = new TerminalClauseImpl(clauseNames.getPrimaryName(), Operator.IN, operand);
        Query query = new QueryImpl(clause);

        jqlOperandResolver = mock(JqlOperandResolver.class);

        final MultiSelectCustomFieldSearchInputTransformer transformer = new MultiSelectCustomFieldSearchInputTransformer(url, clauseNames, customField, jqlOperandResolver, jqlSelectOptionsUtil, queryContextConverter, customFieldInputHelper);

        final CustomFieldParams result = transformer.getParamsFromSearchRequest(theUser, query, searchContext);
        assertNull(result);
        verify(jqlOperandResolver).getValues(theUser, operand, clause);
    }

    @Test
    public void testGetParamsFromSearchRequestNoOptions() throws Exception {
        final Option option1 = new MockOption(null, null, null, "Value 1", null, 10L);

        final QueryLiteral literal1 = createLiteral("value1");
        final QueryLiteral literal2 = createLiteral("value2");

        Query query = new QueryImpl(new TerminalClauseImpl(clauseNames.getPrimaryName(), Operator.IN, new MultiValueOperand(literal1, literal2)));

        when(jqlSelectOptionsUtil.getOptions(customField, literal1, false)).thenReturn(singletonList(option1));
        when(jqlSelectOptionsUtil.getOptions(customField, literal2, false)).thenReturn(emptyList());

        final CustomFieldParams result = transformer.getParamsFromSearchRequest(null, query, searchContext);
        assertNull(result);
    }

}
