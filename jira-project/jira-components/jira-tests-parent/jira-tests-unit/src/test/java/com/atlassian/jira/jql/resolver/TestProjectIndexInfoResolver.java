package com.atlassian.jira.jql.resolver;

import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Collections;
import java.util.List;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.empty;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestProjectIndexInfoResolver {
    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Mock
    private NameResolver<Project> nameResolver;

    @InjectMocks
    private ProjectIndexInfoResolver projectIndexInfoResolver;

    @Test
    public void testGetIndexedValuesNullValue() throws Exception {
        exception.expect(IllegalArgumentException.class);

        projectIndexInfoResolver.getIndexedValues((String) null);
    }

    @Test
    public void testGetIndexedValuesNullProjectWithId() throws Exception {
        when(nameResolver.getIdsFromName("12345")).thenReturn(Collections.emptyList());
        when(nameResolver.get(12345L)).thenReturn(null);

        final List<String> list = projectIndexInfoResolver.getIndexedValues("12345");

        assertThat(list, empty());
    }

    @Test
    public void testGetIndexedValuesNullProjectWithString() throws Exception {
        when(nameResolver.getIdsFromName("12345")).thenReturn(Collections.emptyList());

        final List<String> list = projectIndexInfoResolver.getIndexedValues("TST");

        assertThat(list, empty());
    }

    @Test
    public void testGetIndexedValuesFoundById() throws Exception {
        final MockProject mockProject = new MockProject(12345, "TST");
        when(nameResolver.getIdsFromName("12345")).thenReturn(Collections.emptyList());
        when(nameResolver.get(12345L)).thenReturn(mockProject);

        final List<String> list = projectIndexInfoResolver.getIndexedValues("12345");

        assertThat(list, contains("12345"));
    }

    @Test
    public void testGetIndexedValuesFoundByString() throws Exception {
        when(nameResolver.getIdsFromName("TST")).thenReturn(Collections.singletonList("12345"));

        final List<String> list = projectIndexInfoResolver.getIndexedValues("TST");

        assertThat(list, contains("12345"));
    }

    @Test
    public void testGetIndexedValuesLongFoundById() throws Exception {
        final MockProject mockProject = new MockProject(12345, "TST");
        when(nameResolver.get(12345L)).thenReturn(mockProject);

        final List<String> list = projectIndexInfoResolver.getIndexedValues(12345L);

        assertThat(list, contains("12345"));
    }

    @Test
    public void testGetIndexedValuesLongNullValue() throws Exception {
        exception.expect(IllegalArgumentException.class);

        projectIndexInfoResolver.getIndexedValues((Long) null);
    }

    @Test
    public void testGetIndexedValuesLongFallBackToString() throws Exception {
        when(nameResolver.get(12345L)).thenReturn(null);
        when(nameResolver.getIdsFromName("12345")).thenReturn(Collections.singletonList("12345"));

        final List<String> list = projectIndexInfoResolver.getIndexedValues(12345L);

        assertThat(list, contains("12345"));
    }

    @Test
    public void testGetIndexedValue() throws Exception {
        final MockProject mockProject = new MockProject(12345, "TST");

        assertEquals("12345", projectIndexInfoResolver.getIndexedValue(mockProject));
    }

    @Test
    public void testGetIndexedValueNullProject() throws Exception {
        exception.expect(IllegalArgumentException.class);

        projectIndexInfoResolver.getIndexedValue(null);
    }
}
