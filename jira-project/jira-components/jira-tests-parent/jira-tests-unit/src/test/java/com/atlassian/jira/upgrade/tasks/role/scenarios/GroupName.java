package com.atlassian.jira.upgrade.tasks.role.scenarios;

import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.crowd.embedded.impl.ImmutableGroup;

import java.util.Locale;
import java.util.Objects;

import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * Represents a group name.
 *
 * @since v7.0
 */
abstract class GroupName {
    abstract String name();

    abstract boolean isAnyone();

    abstract Group toGroup();

    static GroupName valueOf(String name) {
        if (AnyoneGroupName.isAnyoneName(name)) {
            return AnyoneGroupName.INSTANCE;
        } else {
            return new ExplicitGroupName(name);
        }
    }

    @Override
    public final String toString() {
        return name();
    }

    private static final class AnyoneGroupName extends GroupName {
        private static final String ANYONE_GROUP = "<anyone>";
        private static final AnyoneGroupName INSTANCE = new AnyoneGroupName();

        @Override
        String name() {
            return null;
        }

        @Override
        boolean isAnyone() {
            return true;
        }

        @Override
        Group toGroup() {
            return null;
        }

        @Override
        public int hashCode() {
            return 31;
        }

        @Override
        public boolean equals(final Object obj) {
            return obj instanceof AnyoneGroupName;
        }

        private static boolean isAnyoneName(String name) {
            return name == null || ANYONE_GROUP.equalsIgnoreCase(name);
        }
    }

    private final static class ExplicitGroupName extends GroupName {
        private final String name;
        private final String key;

        public ExplicitGroupName(final String name) {
            this.name = notNull("name", name);
            this.key = name.toLowerCase(Locale.ENGLISH);
        }

        @Override
        String name() {
            return name;
        }

        @Override
        boolean isAnyone() {
            return false;
        }

        @Override
        Group toGroup() {
            return new ImmutableGroup(name);
        }

        @Override
        public boolean equals(final Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }
            final ExplicitGroupName that = (ExplicitGroupName) o;
            return Objects.equals(key, that.key);
        }

        @Override
        public int hashCode() {
            return Objects.hash(key);
        }
    }
}
