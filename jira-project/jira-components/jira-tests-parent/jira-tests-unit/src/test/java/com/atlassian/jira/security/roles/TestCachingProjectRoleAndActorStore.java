package com.atlassian.jira.security.roles;

import com.atlassian.cache.Cache;
import com.atlassian.cache.memory.MemoryCacheManager;
import com.atlassian.jira.junit.rules.MockitoContainer;
import com.atlassian.jira.mock.MockProjectRoleManager;
import com.atlassian.jira.security.roles.CachingProjectRoleAndActorStore.ProjectRoleActorsKey;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.collect.MapBuilder;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.internal.util.collections.Sets;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static com.atlassian.jira.mock.MockProjectRoleManager.DEFAULT_ROLE_TYPES;
import static com.atlassian.jira.mock.MockProjectRoleManager.PROJECT_ROLE_TYPE_1;
import static com.atlassian.jira.mock.MockProjectRoleManager.PROJECT_ROLE_TYPE_2;
import static com.atlassian.jira.mock.MockProjectRoleManager.PROJECT_ROLE_TYPE_3;
import static com.atlassian.jira.user.ApplicationUsers.toDirectoryUsers;
import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Sets.newHashSet;
import static java.util.Collections.emptyMap;
import static org.apache.commons.lang3.SerializationUtils.deserialize;
import static org.apache.commons.lang3.SerializationUtils.serialize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

public class TestCachingProjectRoleAndActorStore {
    private static final Comparator<ProjectRoleActorsKey> KEY_COMPARATOR = new ProjectRoleActorsKeyComparator();
    private static final Set<ApplicationUser> NO_USERS = ImmutableSet.of();
    private static final List<ProjectRole> NO_ROLES = ImmutableList.of();

    @Rule
    public MockitoContainer mockComponentContainer = new MockitoContainer(this);

    @Mock
    private ProjectRoleAndActorStore delegate;
    private CachingProjectRoleAndActorStore actorStore;

    @Before
    public void setUp() throws Exception {
        actorStore = new CachingProjectRoleAndActorStore(delegate, new MockProjectRoleManager.MockRoleActorFactory(),
                new MemoryCacheManager());
    }

    // Coverage note: The methods getAllProjectRoles, getProjectRole, and getProjectRoleByName are
    // verified implicitly in several of the other tests as part of checking cache clearing,
    // so they are not called out individually.

    @Test
    public void testAddProjectRole() {
        when(delegate.getAllProjectRoles())
                .thenReturn(NO_ROLES)
                .thenReturn(DEFAULT_ROLE_TYPES);

        // Make sure cache is effective
        assertEquals(NO_ROLES, actorStore.getAllProjectRoles());
        assertEquals(NO_ROLES, actorStore.getAllProjectRoles());
        verify(delegate).getAllProjectRoles();

        when(delegate.addProjectRole(PROJECT_ROLE_TYPE_1)).thenReturn(PROJECT_ROLE_TYPE_2);
        assertThat("Should return the value from the delegate, not our own object",
                actorStore.addProjectRole(PROJECT_ROLE_TYPE_1), is(PROJECT_ROLE_TYPE_2));

        // Cache should have cleared, causing second return from delegate to take effect
        assertEquals(DEFAULT_ROLE_TYPES, actorStore.getAllProjectRoles());
        assertEquals(DEFAULT_ROLE_TYPES, actorStore.getAllProjectRoles());

        verify(delegate).addProjectRole(PROJECT_ROLE_TYPE_1);
        verify(delegate, times(2)).getAllProjectRoles();
        verifyNoMoreInteractions(delegate);
    }

    @Test
    public void testUpdateProjectRole() {
        when(delegate.getAllProjectRoles())
                .thenReturn(NO_ROLES)
                .thenReturn(DEFAULT_ROLE_TYPES);

        // Make sure cache is effective
        assertEquals(NO_ROLES, actorStore.getAllProjectRoles());
        verify(delegate).getAllProjectRoles();

        assertEquals(NO_ROLES, actorStore.getAllProjectRoles());
        assertEquals(null, actorStore.getProjectRole(1L));
        assertEquals(null, actorStore.getProjectRole(2L));
        assertEquals(null, actorStore.getProjectRoleByName(PROJECT_ROLE_TYPE_3.getName()));
        verifyNoMoreInteractions(delegate);

        actorStore.updateProjectRole(PROJECT_ROLE_TYPE_1);

        // Cache should have cleared, causing second return from delegate to take effect
        assertEquals(DEFAULT_ROLE_TYPES, actorStore.getAllProjectRoles());
        assertEquals(DEFAULT_ROLE_TYPES, actorStore.getAllProjectRoles());
        assertEquals(PROJECT_ROLE_TYPE_1, actorStore.getProjectRole(1L));
        assertEquals(PROJECT_ROLE_TYPE_2, actorStore.getProjectRole(2L));
        assertEquals(PROJECT_ROLE_TYPE_3, actorStore.getProjectRoleByName(PROJECT_ROLE_TYPE_3.getName()));

        verify(delegate).updateProjectRole(PROJECT_ROLE_TYPE_1);
        verify(delegate, times(2)).getAllProjectRoles();
        verifyNoMoreInteractions(delegate);
    }

    // Delete also clears the default role actors and project role actors cache entries that
    // match the deleted role.  It's important that these cache interactions also get covered.
    @Test
    public void testDeleteProjectRole() {
        final ProjectRoleActors dra1 = mockDefaultRoleActors(1L);
        final ProjectRoleActors dra2 = mockDefaultRoleActors(2L);
        final ProjectRoleActors pra1 = mockProjectRoleActors(1L, 42L);
        final ProjectRoleActors pra2 = mockProjectRoleActors(1L, 43L);
        final ProjectRoleActors pra3 = mockProjectRoleActors(2L, 42L);

        final Collection<ProjectRole> afterDelete = ImmutableList.of(PROJECT_ROLE_TYPE_2, PROJECT_ROLE_TYPE_3);
        when(delegate.getAllProjectRoles())
                .thenReturn(DEFAULT_ROLE_TYPES)
                .thenReturn(afterDelete);
        when(delegate.getProjectRoleActorsByRoleId(1L)).thenReturn(Sets.newSet(pra1, pra2, dra1));
        when(delegate.getProjectRoleActorsByRoleId(2L)).thenReturn(Sets.newSet(pra3, dra2));

        // Load the cache
        assertEquals(DEFAULT_ROLE_TYPES, actorStore.getAllProjectRoles());
        assertThat(actorStore.getDefaultRoleActors(1L), hasRoleActors(dra1));
        assertThat(actorStore.getDefaultRoleActors(2L), hasRoleActors(dra2));
        assertThat(actorStore.getDefaultRoleActors(3L), notNullValue());
        assertThat(actorStore.getProjectRoleActors(1L, 42L), hasRoleActors(pra1));
        assertThat(actorStore.getProjectRoleActors(1L, 43L), hasRoleActors(pra2));
        assertThat(actorStore.getProjectRoleActors(1L, 44L), notNullValue());
        assertThat(actorStore.getProjectRoleActors(2L, 42L), hasRoleActors(pra3));
        assertThat(actorStore.getProjectRoleActors(2L, 43L), notNullValue());
        verify(delegate).getAllProjectRoles();
        verify(delegate).getProjectRoleActorsByRoleId(1L);
        verify(delegate).getProjectRoleActorsByRoleId(2L);
        verify(delegate).getProjectRoleActorsByRoleId(3L);
        verifyNoMoreInteractions(delegate);

        // Make sure caches have the expected contents and are effective
        assertThat(actorStore.projectRoleActorsCache.get(1L), is(MapBuilder.newBuilder(
                Optional.ofNullable((Long)null), actorStore.getProjectRoleActors(1L, null),
                Optional.of(42L), actorStore.getProjectRoleActors(1L, 42L),
                Optional.of(43L), actorStore.getProjectRoleActors(1L, 43L)
            ).toListOrderedMap()));
        assertThat(actorStore.projectRoleActorsCache.get(2L), is(MapBuilder.newBuilder(
                Optional.ofNullable((Long)null), actorStore.getProjectRoleActors(2L, null),
                Optional.of(42L), actorStore.getProjectRoleActors(2L, 42L)
        ).toMutableMap()));
        assertThat(actorStore.projectRoleActorsCache.get(3L), is(MapBuilder.emptyMap()));
        assertEquals(DEFAULT_ROLE_TYPES, actorStore.getAllProjectRoles());
        assertThat(actorStore.getDefaultRoleActors(1L), hasRoleActors(dra1));
        assertThat(actorStore.getDefaultRoleActors(2L), hasRoleActors(dra2));
        assertThat(actorStore.getDefaultRoleActors(3L), notNullValue());
        assertThat(actorStore.getProjectRoleActors(1L, 42L), hasRoleActors(pra1));
        assertThat(actorStore.getProjectRoleActors(1L, 43L), hasRoleActors(pra2));
        assertThat(actorStore.getProjectRoleActors(1L, 44L), notNullValue());
        assertThat(actorStore.getProjectRoleActors(2L, 42L), hasRoleActors(pra3));
        assertThat(actorStore.getProjectRoleActors(2L, 43L), notNullValue());
        verifyNoMoreInteractions(delegate);

        // Zap the role and make sure that the cached info was cleaned as expected
        actorStore.deleteProjectRole(PROJECT_ROLE_TYPE_1);
        when(delegate.getProjectRoleActorsByRoleId(1L)).thenReturn(Collections.emptySet()); //kind'a deleted stuff

        assertThat(actorStore.projectRoleActorsCache.get(1L), is(emptyMap()));
        assertThat(actorStore.projectRoleActorsCache.get(2L), is(MapBuilder.newBuilder(
                Optional.ofNullable((Long)null), actorStore.getProjectRoleActors(2L, null),
                Optional.of(42L), actorStore.getProjectRoleActors(2L, 42L)
        ).toMutableMap()));
        assertThat(actorStore.projectRoleActorsCache.get(3L), is(MapBuilder.emptyMap()));
    }

    private ProjectRoleActors mockDefaultRoleActors(final Long projectRoleId) {
        final ProjectRoleActors dra = mock(ProjectRoleActors.class);
        when(dra.getProjectRoleId()).thenReturn(projectRoleId);
        when(dra.getProjectId()).thenReturn(null);
        when(delegate.getDefaultRoleActors(projectRoleId)).thenReturn(dra).thenReturn(null);
        return dra;
    }

    private ProjectRoleActors mockProjectRoleActors(final Long projectRoleId, final Long projectId) {
        final ProjectRoleActors pra = mock(ProjectRoleActors.class);
        when(pra.getProjectRoleId()).thenReturn(projectRoleId);
        when(pra.getProjectId()).thenReturn(projectId);
        when(delegate.getProjectRoleActors(projectRoleId, projectId)).thenReturn(pra);
        return pra;
    }

    @Test
    public void testGetProjectRoleActors() throws Exception {
        final ProjectRole projectRoleType1 = MockProjectRoleManager.PROJECT_ROLE_TYPE_1;

        final Set<RoleActor> actors = newHashSet();
        final Long roleId = projectRoleType1.getId();
        actors.add(new MockProjectRoleManager.MockRoleActor(7L, roleId, null, NO_USERS, ProjectRoleActor.USER_ROLE_ACTOR_TYPE, "testuser"));
        final ProjectRoleActorsImpl projectRoleActors = new ProjectRoleActorsImpl(42L, roleId, actors);
        when(delegate.getProjectRoleActorsByRoleId(roleId)).thenReturn(Sets.newSet(projectRoleActors));

        assertThat(actorStore.getProjectRoleActors(roleId, 42L), hasRoleActors(projectRoleActors));

        //noinspection unchecked
        when(delegate.getProjectRoleActors(roleId, 42L)).thenThrow(AssertionError.class);

        assertThat(actorStore.getProjectRoleActors(roleId, 42L), hasRoleActors(projectRoleActors));
    }

    @Test
    public void testGetDefaultRoleActors() throws Exception {
        final ProjectRole projectRoleType1 = MockProjectRoleManager.PROJECT_ROLE_TYPE_1;

        final Set<RoleActor> actors = newHashSet();
        final Long roleId = projectRoleType1.getId();
        actors.add(new MockProjectRoleManager.MockRoleActor(7L, roleId, null, NO_USERS, ProjectRoleActor.USER_ROLE_ACTOR_TYPE, "testuser"));
        final ProjectRoleActors defaultRoleActors = new ProjectRoleActorsImpl(null, roleId, actors);
        when(delegate.getDefaultRoleActors(roleId)).thenReturn(defaultRoleActors);
        when(delegate.getProjectRoleActorsByRoleId(roleId)).thenReturn(Sets.newSet(defaultRoleActors));

        assertThat(actorStore.getDefaultRoleActors(roleId), hasRoleActors(defaultRoleActors));

        //noinspection unchecked
        when(delegate.getDefaultRoleActors(roleId)).thenThrow(AssertionError.class);

        assertThat(actorStore.getDefaultRoleActors(roleId), hasRoleActors(defaultRoleActors));
    }

    @Test
    public void testUpdateProjectRoleActors() throws RoleActorDoesNotExistException {
        final ProjectRole projectRoleType1 = MockProjectRoleManager.PROJECT_ROLE_TYPE_1;

        final Set<RoleActor> actors = newHashSet();
        final Long roleId = projectRoleType1.getId();
        actors.add(new MockProjectRoleManager.MockRoleActor(7L, roleId, null, NO_USERS, ProjectRoleActor.USER_ROLE_ACTOR_TYPE, "testuser"));
        final ProjectRoleActorsImpl projectRoleActors = new ProjectRoleActorsImpl(42L, roleId, actors);
        when(delegate.getProjectRoleActorsByRoleId(roleId)).thenReturn(Sets.newSet(projectRoleActors));

        // puts it in the cache
        final ProjectRoleActors before = actorStore.getProjectRoleActors(roleId, 42L);
        assertThat(before, hasRoleActors(projectRoleActors));
        verify(delegate).getProjectRoleActorsByRoleId(roleId);

        // clear the cache for this project role actor
        actorStore.updateProjectRoleActors(projectRoleActors);

        final ProjectRoleActors after = actorStore.getProjectRoleActors(roleId, 42L);
        assertThat(after, hasRoleActors(projectRoleActors));
        assertNotSame("The cached copy should be a new instance", before, after);
        verify(delegate, times(2)).getProjectRoleActorsByRoleId(roleId);
        verify(delegate).updateProjectRoleActors(projectRoleActors);
    }

    @Test
    public void testUpdateDefaultRoleActors() throws Exception {
        final ProjectRole projectRoleType1 = MockProjectRoleManager.PROJECT_ROLE_TYPE_1;

        final Set<RoleActor> actors = newHashSet();
        final Long roleId = projectRoleType1.getId();
        actors.add(new MockProjectRoleManager.MockRoleActor(7L, roleId, null, NO_USERS, ProjectRoleActor.USER_ROLE_ACTOR_TYPE, "testuser"));
        final ProjectRoleActors defaultRoleActors = new ProjectRoleActorsImpl(null, roleId, actors);
        when(delegate.getProjectRoleActorsByRoleId(roleId)).thenReturn(Sets.newSet(defaultRoleActors));

        // puts it in the cache
        final DefaultRoleActors before = actorStore.getDefaultRoleActors(roleId);
        assertThat(before, hasRoleActors(defaultRoleActors));
        verify(delegate).getProjectRoleActorsByRoleId(roleId);

        // clear the cache for this default role actor
        actorStore.updateDefaultRoleActors(defaultRoleActors);

        final DefaultRoleActors after = actorStore.getDefaultRoleActors(roleId);
        assertThat(after, hasRoleActors(defaultRoleActors));
        assertNotSame("The cached copy should be a new instance", before, after);
        verify(delegate, times(2)).getProjectRoleActorsByRoleId(roleId);
        verify(delegate).updateDefaultRoleActors(defaultRoleActors);
    }

    @Test
    public void projectRoleActorsKeyShouldBeSerializble() {
        // Set up
        final Serializable key = new ProjectRoleActorsKey(1L, 2L);

        // Invoke
        final Object roundTrippedKey = deserialize(serialize(key));

        // Check
        assertEquals(key, roundTrippedKey);
        assertNotSame(key, roundTrippedKey);
    }

    @Test(expected = IllegalArgumentException.class)
    public void getProjectRoleActorsThrowsExceptionOnNull() {
        actorStore.getProjectRoleActors(null, 1L);
    }

    @Test(expected = IllegalArgumentException.class)
    public void getDefaultProjectRoleActorsThrowsExceptionOnNull() {
        actorStore.getDefaultRoleActors(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void getProjectRoleActorsByRoleIdThrowsExceptionOnNull() {
        actorStore.getProjectRoleActorsByRoleId(null);
    }

    private static ProjectRoleActorsKey key(Long projectRoleId, Long projectId) {
        return new ProjectRoleActorsKey(projectRoleId, projectId);
    }

    private static <T extends Comparable<T>> List<T> sort(Collection<? extends T> c) {
        final List<T> list = newArrayList(c);
        Collections.sort(list);
        return list;
    }

    private static <T> List<T> sort(Collection<? extends T> c, Comparator<? super T> comparator) {
        final List<T> list = newArrayList(c);
        Collections.sort(list, comparator);
        return list;
    }

    static Matcher<Cache<ProjectRoleActorsKey, ?>> hasRoleActorKeys(ProjectRoleActorsKey... expected) {
        return hasRoleActorKeys(Arrays.asList(expected));
    }

    static Matcher<Cache<ProjectRoleActorsKey, ?>> hasRoleActorKeys(final Collection<ProjectRoleActorsKey> exp) {
        final List<ProjectRoleActorsKey> expected = sort(exp, KEY_COMPARATOR);
        return new BaseMatcher<Cache<ProjectRoleActorsKey, ?>>() {
            @Override
            public boolean matches(final Object o) {
                if (o instanceof Cache) {
                    final Collection<?> other = ((Cache<?, ?>) o).getKeys();
                    return expected.size() == other.size() && other.containsAll(expected);
                }
                return false;
            }

            @Override
            public void describeTo(final Description description) {
                description.appendText("has role actor keys ");
                description.appendValue(expected);
            }
        };
    }

    static Matcher<DefaultRoleActors> hasRoleActors(final DefaultRoleActors expected) {
        return new BaseMatcher<DefaultRoleActors>() {
            @Override
            public boolean matches(final Object o) {
                if (o == null || !(o instanceof DefaultRoleActors)) {
                    return false;
                }

                final DefaultRoleActors other = (DefaultRoleActors) o;
                if (!other.getProjectRoleId().equals(other.getProjectRoleId())) {
                    return false;
                }

                if (expected instanceof ProjectRoleActors) {
                    if (!(other instanceof ProjectRoleActors)) {
                        return false;
                    }
                    final ProjectRoleActors exp = (ProjectRoleActors) expected;
                    final ProjectRoleActors oth = (ProjectRoleActors) other;
                    if (exp.getProjectId() != null && !exp.getProjectId().equals(oth.getProjectId())) {
                        return false;
                    }
                }

                return sort(toDirectoryUsers(expected.getUsers())).equals(sort(toDirectoryUsers(other.getUsers())));
            }

            @Override
            public void describeTo(final Description description) {
                if (expected instanceof ProjectRoleActors) {
                    description.appendText("ProjectRoleActors[projectId=")
                            .appendValue(((ProjectRoleActors) expected).getProjectId())
                            .appendText(",");
                } else {
                    description.appendText("DefaultRoleActors[");
                }
                description.appendText("projectRoleId=").appendValue(expected.getProjectRoleId());
                description.appendText("]");
            }
        };
    }

    static class ProjectRoleActorsKeyComparator implements Comparator<ProjectRoleActorsKey> {

        @Override
        public int compare(final ProjectRoleActorsKey key1, final ProjectRoleActorsKey key2) {
            if (key1.getProjectRoleId() != key2.getProjectRoleId()) {
                return (key1.getProjectRoleId() < key2.getProjectRoleId()) ? -1 : 1;
            }
            if (key1.getProjectId() != key2.getProjectId()) {
                return (key1.getProjectId() < key2.getProjectId()) ? -1 : 1;
            }
            return 0;
        }
    }
}