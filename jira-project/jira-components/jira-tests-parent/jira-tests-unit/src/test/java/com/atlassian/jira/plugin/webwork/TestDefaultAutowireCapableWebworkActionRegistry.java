package com.atlassian.jira.plugin.webwork;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.module.ContainerManagedPlugin;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.withSettings;

public class TestDefaultAutowireCapableWebworkActionRegistry {

    private DefaultAutowireCapableWebworkActionRegistry registry;

    @Before
    public void setUp() throws Exception {
        registry = new DefaultAutowireCapableWebworkActionRegistry();
    }

    @Test
    public void testRegisterActionInvalidPlugin() {
        //a plugin that isn't AutowireCapable.
        final Plugin mockPlugin = mock(Plugin.class);
        final ModuleDescriptor<?> mockModuleDescriptor = mock(ModuleDescriptor.class);
        when(mockModuleDescriptor.getPlugin()).thenReturn(mockPlugin);
        try {
            registry.registerAction("some.action.class", mockModuleDescriptor);
            fail("Should only be able to register AutowireCapablePlugins");
        } catch (final IllegalArgumentException e) {
            //should have gotten here!
        }
    }

    @Test
    public void testRegisterAction() {
        final ModuleDescriptor<?> mockModuleDescriptor = mockAutowireCapablePlugin("someplugin:module");
        final ModuleDescriptor<?> mockModuleDescriptor2 = mockAutowireCapablePlugin("anotherplugin:module2");

        registry.registerAction("some.action.class", mockModuleDescriptor);
        registry.registerAction("some.action.class2", mockModuleDescriptor);
        registry.registerAction("other.plugin.action.class", mockModuleDescriptor2);

        assertTrue(registry.containsAction("some.action.class"));
        assertTrue(registry.containsAction("some.action.class2"));
        assertTrue(registry.containsAction("other.plugin.action.class"));
        assertFalse(registry.containsAction("other.plugin.action.class2"));

        final ContainerManagedPlugin plugin1 = registry.getPlugin("some.action.class");
        final ContainerManagedPlugin plugin2 = registry.getPlugin("some.action.class2");
        final ContainerManagedPlugin plugin3 = registry.getPlugin("other.plugin.action.class");
        final ContainerManagedPlugin plugin4 = registry.getPlugin("other.plugin.action.class2");

        assertEquals(mockModuleDescriptor.getPlugin(), plugin1);
        assertEquals(plugin1, plugin2);
        assertEquals(mockModuleDescriptor2.getPlugin(), plugin3);
        assertNull(plugin4);

        registry.unregisterPluginModule(mockModuleDescriptor);

        assertFalse(registry.containsAction("some.action.class"));
        assertFalse(registry.containsAction("some.action.class2"));
        assertTrue(registry.containsAction("other.plugin.action.class"));
        assertFalse(registry.containsAction("other.plugin.action.class2"));
    }

    @Test
    public void testRegisterNullPlugin() {
        final ModuleDescriptor<?> mockModuleDescriptor = mock(ModuleDescriptor.class);

        try {
            registry.registerAction(null, mockModuleDescriptor);
            fail("Should throw exception");
        } catch (final IllegalArgumentException e) {
            //can't register against null!
        }

        try {
            registry.registerAction("somethign", null);
            fail("Should throw exception");
        } catch (final IllegalArgumentException e) {
            //can't register against null!
        }
    }

    @Test
    public void testUnregisterNullPlugin() {
        final ModuleDescriptor<?> mockModuleDescriptor = mockAutowireCapablePlugin("some.other.action");

        registry.registerAction("some.action.class", mockModuleDescriptor);

        assertTrue(registry.containsAction("some.action.class"));
        try {
            registry.unregisterPluginModule(null);
            fail("Should have thrown exception");
        } catch (final IllegalArgumentException e) {
            //yay
        }
        assertTrue(registry.containsAction("some.action.class"));
    }

    @Test
    public void testUnregisterNonExistentPlugin() {
        final ModuleDescriptor<?> mockModuleDescriptor = mockAutowireCapablePlugin("someplugin:module");
        final ModuleDescriptor<?> mockModuleDescriptor2 = mockAutowireCapablePlugin("anotherplugin:module2");

        registry.registerAction("some.action.class", mockModuleDescriptor);

        assertTrue(registry.containsAction("some.action.class"));
        registry.unregisterPluginModule(mockModuleDescriptor2);
        assertTrue(registry.containsAction("some.action.class"));
    }

    @Test
    public void shouldCorrectlyRegisterAndUnregisterTwoActionsWithTheSameModuleKey() {
        final ModuleDescriptor<?> mockModuleDescriptor = mockAutowireCapablePlugin("someplugin:module");
        final ModuleDescriptor<?> mockModuleDescriptor2 = mockAutowireCapablePlugin("anotherplugin:module");

        registry.registerAction("some.action.class", mockModuleDescriptor);
        registry.registerAction("another.action.class", mockModuleDescriptor2);

        assertTrue(registry.containsAction("some.action.class"));
        assertTrue(registry.containsAction("another.action.class"));
        registry.unregisterPluginModule(mockModuleDescriptor);
        assertFalse(registry.containsAction("some.action.class"));
        assertTrue(registry.containsAction("another.action.class"));

        registry.registerAction("some.action.class", mockModuleDescriptor);
        assertTrue(registry.containsAction("some.action.class"));
        assertTrue(registry.containsAction("another.action.class"));
        registry.unregisterPluginModule(mockModuleDescriptor2);
        assertFalse(registry.containsAction("another.action.class"));
        assertTrue(registry.containsAction("some.action.class"));
        registry.unregisterPluginModule(mockModuleDescriptor);
        assertFalse(registry.containsAction("another.action.class"));
        assertFalse(registry.containsAction("some.action.class"));
    }

    private ModuleDescriptor<?> mockAutowireCapablePlugin(final String moduleKey) {
        final Plugin mockPlugin = mock(Plugin.class, withSettings().extraInterfaces(ContainerManagedPlugin.class));
        final ModuleDescriptor<?> mockModuleDescriptor = mock(ModuleDescriptor.class);
        when(mockModuleDescriptor.getPlugin()).thenReturn(mockPlugin);
        when(mockModuleDescriptor.getCompleteKey()).thenReturn(moduleKey);
        return mockModuleDescriptor;
    }
}
