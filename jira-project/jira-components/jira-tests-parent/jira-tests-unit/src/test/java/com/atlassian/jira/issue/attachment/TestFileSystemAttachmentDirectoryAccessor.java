package com.atlassian.jira.issue.attachment;

import com.atlassian.jira.config.util.AttachmentPathManager;
import com.atlassian.jira.config.util.JiraHome;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.rules.TestRule;
import org.mockito.Mock;

import java.io.File;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @since v6.3
 */
public class TestFileSystemAttachmentDirectoryAccessor {

    @Rule
    public TestRule container = MockitoMocksInContainer.forTest(this);

    @Rule
    public TemporaryFolder testFolder = new TemporaryFolder();

    @Mock
    private AttachmentPathManager attachmentPathManager;

    @Mock
    private ProjectManager projectManager;

    private FileSystemAttachmentDirectoryAccessor directoryAccessor;

    @Mock
    @AvailableInContainer
    private JiraHome jiraHome;
    @Rule
    public TemporaryFolder jiraTmpFolder = new TemporaryFolder();

    @Before
    public void setUp() {
        directoryAccessor = new FileSystemAttachmentDirectoryAccessor(projectManager, attachmentPathManager);
        when(attachmentPathManager.getAttachmentPath()).thenReturn(testFolder.getRoot().getAbsolutePath());
    }

    @Test
    public void getAttachmentDirectorShouldReturnCurrentKeyByDefault() {
        Project project = mock(Project.class);
        Issue issue = mock(Issue.class);
        when(issue.getKey()).thenReturn("MKY-5");
        when(issue.getProjectObject()).thenReturn(project);
        when(project.getKey()).thenReturn("MKY");
        when(projectManager.getProjectObjByKey("MKY")).thenReturn(project);
        when(project.getOriginalKey()).thenReturn("MKY");
        assertThat(directoryAccessor.getAttachmentDirectory(issue, false), equalTo(new File(testFolder.getRoot(), "MKY/10000/MKY-5")));
        assertThat(directoryAccessor.getThumbnailDirectory(issue), equalTo(new File(testFolder.getRoot(), "MKY/10000/MKY-5/thumbs")));
    }

    @Test
    public void getAttachmentDirectorShouldReturnCurrentKeyIfNoPriorDirectoryExists() {
        Project project = mock(Project.class);
        Issue issue = mock(Issue.class);
        when(issue.getKey()).thenReturn("MKY-5");
        when(issue.getProjectObject()).thenReturn(project);
        when(project.getKey()).thenReturn("MKY");
        when(project.getOriginalKey()).thenReturn("MKY");
        when(project.getId()).thenReturn(11l);

        when(projectManager.getProjectObjByKey("MKY")).thenReturn(project);
        assertThat(directoryAccessor.getAttachmentDirectory(issue, false), equalTo(new File(testFolder.getRoot(), "MKY/10000/MKY-5")));
        assertThat(directoryAccessor.getThumbnailDirectory(issue), equalTo(new File(testFolder.getRoot(), "MKY/10000/MKY-5/thumbs")));

    }

    @Test
    public void getAttachmentDirectorShouldReturnPreviousDirectoryIfItExists() {
        assertTrue(new File(testFolder.getRoot(), "OLD/OLD-5").mkdirs());

        Project project = mock(Project.class);
        Issue issue = mock(Issue.class);
        when(issue.getKey()).thenReturn("MKY-5");
        when(issue.getProjectObject()).thenReturn(project);
        when(project.getKey()).thenReturn("MKY");
        when(project.getOriginalKey()).thenReturn("OLD");
        when(project.getId()).thenReturn(11l);

        when(projectManager.getProjectObjByKey("MKY")).thenReturn(project);
        assertThat(directoryAccessor.getAttachmentDirectory(issue, false), equalTo(new File(testFolder.getRoot(), "OLD/10000/OLD-5")));
        assertThat(directoryAccessor.getThumbnailDirectory(issue), equalTo(new File(testFolder.getRoot(), "OLD/10000/OLD-5/thumbs")));
    }

    /**
     * We want to gather old attachments in one directory, even if it's the old one (with an old project key).
     */
    @Test
    public void getAttachmentDirectorShouldUsePreviousDirectoryIfRootExists() {
        assertTrue(new File(testFolder.getRoot(), "OLD").mkdirs());

        Project project = mock(Project.class);
        Issue issue = mock(Issue.class);
        when(issue.getKey()).thenReturn("MKY-5");
        when(issue.getProjectObject()).thenReturn(project);
        when(project.getKey()).thenReturn("MKY");
        when(project.getOriginalKey()).thenReturn("OLD");
        when(project.getId()).thenReturn(11l);

        when(projectManager.getProjectObjByKey("MKY")).thenReturn(project);
        assertThat(directoryAccessor.getAttachmentDirectory(issue, false), equalTo(new File(testFolder.getRoot(), "OLD/10000/OLD-5")));
        assertThat(directoryAccessor.getThumbnailDirectory(issue), equalTo(new File(testFolder.getRoot(), "OLD/10000/OLD-5/thumbs")));
    }


    @Test
    public void isHealthyReturnsHealthyIfRootDirectoryAndTmpDirectoryArePresent() throws Exception {
        when(attachmentPathManager.getAttachmentPath()).thenReturn(testFolder.getRoot().getAbsolutePath());
        when(jiraHome.getCachesDirectory()).thenReturn(jiraTmpFolder.newFolder());
        assertFalse(directoryAccessor.errors().isDefined());
    }

    @Test
    public void isHealthyReturnsUnhealthyIfRootDirectoryIsNotPresent() throws Exception {
        File invalid = new File("/tmp/jira-das-" + java.util.UUID.randomUUID().toString());
        when(attachmentPathManager.getAttachmentPath()).thenReturn(invalid.getAbsolutePath());
        when(jiraHome.getCachesDirectory()).thenReturn(jiraTmpFolder.newFolder());
        assertTrue(directoryAccessor.errors().isDefined());
    }

    @Test
    public void isHealthyReturnsUnhealthyIfTmpDirectoryIsNotPresent() throws Exception {
        File invalid = jiraTmpFolder.newFolder();
        invalid.setWritable(false);
        when(jiraHome.getCachesDirectory()).thenReturn(invalid);
        when(attachmentPathManager.getAttachmentPath()).thenReturn(testFolder.getRoot().getAbsolutePath());
        assertTrue(directoryAccessor.errors().isDefined());
    }
}
