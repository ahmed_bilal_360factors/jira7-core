package com.atlassian.jira.license;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.extras.api.LicenseType;
import com.atlassian.jira.cluster.ClusterManager;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.datetime.DateTimeFormatter;
import com.atlassian.jira.datetime.DateTimeFormatterFactoryStub;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.mock.MockApplicationProperties;
import com.atlassian.jira.mock.web.util.MockOutlookDate;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.BuildUtilsInfo;
import com.atlassian.jira.util.ConstantClock;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.NoopI18nFactory;
import com.atlassian.jira.util.NoopI18nHelper;
import com.atlassian.jira.web.util.ExternalLinkUtil;
import com.atlassian.jira.web.util.OutlookDate;
import com.google.common.collect.ImmutableSet;
import org.joda.time.format.DateTimeFormat;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import static com.atlassian.jira.license.DefaultLicenseDetails.DATACENTER_PROPERTY_NAME;
import static com.atlassian.jira.license.DefaultLicenseDetails.ENABLED;
import static com.atlassian.jira.license.DefaultLicenseDetails.ONDEMAND_PROPERTY_NAME;
import static com.atlassian.jira.license.DefaultLicenseDetails.STARTER_PROPERTY_NAME;
import static com.atlassian.jira.license.DefaultLicensedApplications.getApplicationLicensePropertyName;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

/**
 * Tests {@link DefaultLicenseDetails}; see also {@link TestDefaultLicenseDetailsApplications}.
 *
 * @see TestDefaultLicenseDetailsApplications
 * @since v6.3
 */
@RunWith(MockitoJUnitRunner.class)
public class TestDefaultLicenseDetails {
    @Rule
    public TestRule mockInContainer = MockitoMocksInContainer.forTest(this);

    private static final OutlookDate OUTLOOK_DATE = new MockOutlookDate(Locale.ENGLISH) {
        @Override
        @SuppressWarnings("deprecation")
        public String formatDMY(final Date date) {
            return date.toString();
        }
    };

    private static final String LICENSE_STRING = "Some license String";
    private static final long GRACE_PERIOD_IN_MILLIS = TimeUnit.DAYS.toMillis(30);
    private static final String NOT_ENABLED = "false";

    private long fiftyDaysAgoInMillis;
    private long tenDaysAgoInMillis;
    private long thirtyDaysAgoInMillis;
    private ApplicationProperties applicationProperties;
    private Date now;
    private Date tenDaysFromNow;
    private Date tenSecondsBeforeNow;
    private Date tenSecondsFromNow;
    private Date twentyFourDaysAgo;

    private I18nHelper mockI18nHelper;
    private LicenseDetails licenseDetails;
    private MockLicense mockLicense;
    private ApplicationUser fred;
    private final DateTimeFormatter dateTimeFormatter = new DateTimeFormatterFactoryStub().formatter();

    @Mock
    private ExternalLinkUtil externalLinkUtil;
    @Mock
    private LicensedApplications licensedApplications;
    @Mock
    private BuildUtilsInfo buildUtilsInfo;
    @Mock
    private UserManager userManager;
    @AvailableInContainer
    @Mock
    private LicenseMaintenancePredicate licenseMaintenancePredicate;
    @AvailableInContainer
    @Mock
    private ClusterManager clusterManager;

    @Before
    public void setUp() throws Exception {
        applicationProperties = new MockApplicationProperties();
        mockI18nHelper = new NoopI18nHelper(Locale.ENGLISH);

        now = new Date(1401162923372L);
        tenSecondsFromNow = new Date(now.getTime() + TimeUnit.SECONDS.toMillis(10));
        tenSecondsBeforeNow = new Date(now.getTime() - TimeUnit.SECONDS.toMillis(10));
        tenDaysFromNow = new Date(now.getTime() + TimeUnit.DAYS.toMillis(10));
        twentyFourDaysAgo = new Date(now.getTime() - TimeUnit.DAYS.toMillis(24));

        tenDaysAgoInMillis = now.getTime() - TimeUnit.DAYS.toMillis(10);
        thirtyDaysAgoInMillis = now.getTime() - TimeUnit.DAYS.toMillis(30);
        fiftyDaysAgoInMillis = now.getTime() - TimeUnit.DAYS.toMillis(50);

        mockLicense = new MockLicense();
        mockLicense.setLicenseType(LicenseType.COMMERCIAL);
        mockLicense.setMaintenanceExpired(false);
        mockLicense.setMaintenanceExpiryDate(tenSecondsFromNow);

        when(licenseMaintenancePredicate.test(any(LicenseDetails.class))).thenReturn(true);

        licenseDetails = new DefaultLicenseDetails(
                new JiraProductLicense(licensedApplications, mockLicense),
                LICENSE_STRING, applicationProperties, externalLinkUtil,
                buildUtilsInfo, new NoopI18nFactory(), dateTimeFormatter,
                new ConstantClock(now.getTime()));
    }

    private void assertExpiryMessageContains(final String text) {
        assertTrue(licenseDetails.getLicenseExpiryStatusMessage(fred).contains(text));
    }

    @Test
    public void testEquals() {
        LicenseDetails licenseDetails2 = new DefaultLicenseDetails(
                new JiraProductLicense(licensedApplications, mockLicense),
                LICENSE_STRING, applicationProperties, externalLinkUtil,
                buildUtilsInfo, new NoopI18nFactory(), dateTimeFormatter,
                new ConstantClock(now.getTime()));
        assertEquals(licenseDetails, licenseDetails2);
        assertEquals(licenseDetails.hashCode(), licenseDetails2.hashCode());
    }

    @Test
    public void testGetLicenseExpiryStatusMessageForExpiredEvaluation() {
        mockLicense.setEvaluation(true);
        mockLicense.setExpiryDate(now);
        mockLicense.setExpired(true);

        inGraceMode();

        assertExpiryMessageContains("admin.license.expired");
    }

    @Test
    public void testGetLicenseExpiryStatusMessageForNonExpiredEvaluation() {
        mockLicense.setEvaluation(true);
        mockLicense.setExpiryDate(tenSecondsFromNow);
        mockLicense.setExpired(false);
        assertExpiryMessageContains("admin.license.expiresin");
    }

    @Test
    public void testGetLicenseExpiryStatusMessageWhenTimestampedForOldLicenseOutsideTheGracePeriod() {
        inGraceMode();

        withExpiredLicense();
        setLicenseExtensionTimestamp(fiftyDaysAgoInMillis);// set the recorded extension timestamp to be outside the grace period

        assertExpiryMessageContains("admin.license.expired");
    }

    @Test
    public void testGetLicenseExpiryStatusMessageWhenTimestampedForOldLicenseWithinTheGracePeriod() {
        inGraceMode();
        withExpiredLicense();

        setLicenseExtensionTimestamp(tenDaysAgoInMillis); // set the recorded extension timestamp to be within the grace period

        assertExpiryMessageContains("admin.license.expiresin");
    }

    @Test
    public void testGetLicenseExpiryStatusMessageForLicenseWithNonExpiredMaintenance() {
        mockLicense.setMaintenanceExpired(false);

        assertExpiryMessageContains("admin.support.available.until");
    }

    @Test
    public void testGetLicenseExpiryStatusMessageForCommunityLicenseWithNonExpiredMaintenance() {
        mockLicense.setMaintenanceExpired(false);

        // other self renewable but supported licenses
        mockLicense.setLicenseType(LicenseType.COMMUNITY);
        assertExpiryMessageContains("admin.support.available.until");
    }

    @Test
    public void testGetLicenseExpiryStatusMessageForPersonalLicenseWithNonExpiredMaintenance() {
        mockLicense.setMaintenanceExpired(false);
        mockLicense.setMaintenanceExpiryDate(tenSecondsFromNow);
        // unsupported license
        mockLicense.setLicenseType(LicenseType.PERSONAL);
        assertExpiryMessageContains("admin.upgrades.available.until");
    }

    @Test
    public void testGetLicenseExpiryStatusMessageForLicenseWithExpiredMaintenance() {
        // finally, if expired, message should be null
        mockLicense.setMaintenanceExpired(true);
        mockLicense.setMaintenanceExpiryDate(tenSecondsBeforeNow);
        assertNull(licenseDetails.getLicenseExpiryStatusMessage(fred));
    }

    private void assertBriefMaintenanceMessageContains(final String text) {
        assertTrue(licenseDetails.getBriefMaintenanceStatusMessage(mockI18nHelper).contains(text));
    }

    @Test
    public void testGetBriefMaintenanceStatusMessageForExpiredEvaluationLicense() {
        mockLicense.setEvaluation(true);
        mockLicense.setExpired(true);
        assertBriefMaintenanceMessageContains("supported.expired");
    }

    @Test
    public void testGetBriefMaintenanceStatusMessageWithValidCommercialLicenseWithinMaintenanceTimeframe() {
        assertBriefMaintenanceMessageContains("supported.valid");
    }

    @Test
    public void testGetBriefMaintenanceStatusMessageWithOldLicenseOutsideOfGracePeriod() {

        inGraceMode();
        withExpiredLicense();
        setLicenseExtensionTimestamp(fiftyDaysAgoInMillis); // outside grace period

        assertBriefMaintenanceMessageContains("supported.expired");
    }

    @Test
    public void testGetBriefMaintenanceStatusMessageWithOldLicenseWithinGracePeriod() {
        inGraceMode();
        withExpiredLicense();
        setLicenseExtensionTimestamp(tenDaysAgoInMillis); // within grace period

        assertBriefMaintenanceMessageContains("supported.valid");
    }

    @Test
    public void testGetBriefMaintenanceStatusMessageForValidCommunityLicense() {
        mockLicense.setLicenseType(LicenseType.COMMUNITY);
        assertBriefMaintenanceMessageContains("supported.valid");
    }

    @Test
    public void testGetBriefMaintenanceStatusMessageForValidPersonalLicense() {
        mockLicense.setLicenseType(LicenseType.PERSONAL);
        assertBriefMaintenanceMessageContains("unsupported");
    }

    @Test
    public void testGetBriefMaintenanceStatusMessageForMaintenanceExpiredPersonalLicense() {
        mockLicense.setLicenseType(LicenseType.PERSONAL);
        mockLicense.setMaintenanceExpired(true);

        assertBriefMaintenanceMessageContains("unsupported");
    }

    @Test
    public void testGetBriefMaintenanceStatusMessageMaintenanceExpiredLicense() {
        mockLicense.setMaintenanceExpired(true);
        assertBriefMaintenanceMessageContains("supported.expired");
    }

    @Test
    public void testGetMaintenanceEndStringEvaluation() {
        mockLicense.setEvaluation(true);
        mockLicense.setExpiryDate(now);
        assertEquals(now.toString(), licenseDetails.getMaintenanceEndString(OUTLOOK_DATE));

    }

    @Test
    public void testGetMaintenanceEndStringOldNewBuild() {
        inGraceMode();
        withExpiredLicense();

        setLicenseExtensionTimestamp(tenSecondsBeforeNow.getTime());

        Date expected = new Date(tenSecondsBeforeNow.getTime() + GRACE_PERIOD_IN_MILLIS);
        assertEquals(expected.toString(), licenseDetails.getMaintenanceEndString(OUTLOOK_DATE));
    }

    @Test
    public void testGetMaintenanceEndStringNotEvaluationNorOldNewBuild() {
        mockLicense.setMaintenanceExpiryDate(tenSecondsBeforeNow);
        assertEquals(tenSecondsBeforeNow.toString(), licenseDetails.getMaintenanceEndString(OUTLOOK_DATE));
    }

    @Test
    public void testGetLicenseStatusMessageEvaluation() {
        mockLicense.setEvaluation(true);
        mockLicense.setExpiryDate(now);
        mockLicense.setExpired(true);

        // expired
        assertTrue(licenseDetails.getLicenseStatusMessage(fred, "", userManager).contains("admin.license.evaluation.expired"));
    }

    @Test
    public void testGetLicenseStatusMessageEvaluationAlmostExpired() {
        // almost expired
        mockLicense.setEvaluation(true);
        mockLicense.setExpiryDate(tenSecondsFromNow);
        assertTrue(licenseDetails.getLicenseStatusMessage(fred, "", userManager).contains("admin.license.evaluation.almost.expired"));
    }

    @Test
    public void testGetLicenseStatusMessageEvaluationNotAlmostExpired() {
        // not expired
        mockLicense.setEvaluation(true);
        mockLicense.setExpiryDate(tenDaysFromNow);
        assertNull(licenseDetails.getLicenseStatusMessage(fred, "", userManager));
    }

    @Test
    public void testGetLicenseStatusMessageNewBuildOldLicenseForExpiredLicense() {
        inGraceMode();
        withExpiredLicense();
        // user: exists, license: commercial, expired: yes
        fred = new MockApplicationUser("fred", "Fred", "fred@example.com");
        mockLicense.setExpiryDate(tenSecondsBeforeNow);
        setLicenseExtensionTimestamp(fiftyDaysAgoInMillis);

        String message = licenseDetails.getLicenseStatusMessage(fred, "", userManager);

        assertTrue(message.contains("admin.license.nbol.current.version"));
        assertTrue(message.contains("admin.license.nbol.support.and.updates"));
        assertTrue(message.contains("admin.license.renew.for.support.and.updates"));
        assertTrue(message.contains("admin.license.nbol.evaluation.period.has.expired"));
    }

    @Test
    public void testGetLicenseStatusMessageNewBuildOldLicenseForNonExpiredPersonalLicense() {
        inGraceMode();
        withExpiredLicense();
        mockLicense.setLicenseType(LicenseType.PERSONAL);
        setLicenseExtensionTimestamp(tenDaysAgoInMillis);

        // user: doesn't exist, mockLicense: self renewable, expired: no
        String message = licenseDetails.getLicenseStatusMessage(fred, "", userManager);
        assertTrue(message.contains("admin.license.nbol.current.version.user.unknown"));
        assertTrue(message.contains("admin.license.nbol.updates.only"));
        assertTrue(message.contains("admin.license.renew.for.updates.only"));
        assertTrue(message.contains("admin.license.nbol.evaluation.period.will.expire.in"));
    }

    @Test
    public void testGetLicenseStatusMessageNewBuildOldLicenseForNonExpiredNonProfitLicense() {
        inGraceMode();
        withExpiredLicense();
        mockLicense.setLicenseType(LicenseType.NON_PROFIT);
        setLicenseExtensionTimestamp(tenDaysAgoInMillis);
        // user: doesnt exist, mockLicense: other/deprecated, expired: no
        String message = licenseDetails.getLicenseStatusMessage(fred, "", userManager);
        assertTrue(message.contains("admin.license.nbol.current.version.user.unknown"));
        assertTrue(message.contains("admin.license.nbol.updates.only"));
        assertTrue(message.contains("admin.license.renew.for.deprecated"));
        assertTrue(message.contains("admin.license.nbol.evaluation.period.will.expire.in"));
    }

    @Test
    public void testGetLicenseStatusMessageForMaintenanceExpiredLicense() throws Exception {
        mockLicense.setMaintenanceExpired(true);
        mockLicense.setMaintenanceExpiryDate(tenSecondsBeforeNow);

        String message = licenseDetails.getLicenseStatusMessage(fred, "", userManager);
        assertTrue(message.contains("admin.license.support.and.updates.has.ended"));
        assertTrue(message.contains("admin.license.renew.for.support.and.updates"));
    }

    @Test
    public void testGetLicenseStatusMessageNormalForMaintenanceExpiredPersonalLicense() throws Exception {
        mockLicense.setLicenseType(LicenseType.PERSONAL);
        mockLicense.setMaintenanceExpired(true);
        mockLicense.setMaintenanceExpiryDate(tenSecondsBeforeNow);

        String message = licenseDetails.getLicenseStatusMessage(fred, "", userManager);
        assertTrue(message.contains("admin.license.updates.only.has.ended"));
        assertTrue(message.contains("admin.license.renew.for.updates.only"));
    }

    @Test
    public void testGetLicenseStatusMessageNormalForMaintenanceExpiredNonProfitLicense() throws Exception {
        mockLicense.setLicenseType(LicenseType.NON_PROFIT);
        mockLicense.setMaintenanceExpired(true);
        mockLicense.setMaintenanceExpiryDate(tenSecondsBeforeNow);

        String message = licenseDetails.getLicenseStatusMessage(fred, "", userManager);
        assertTrue(message.contains("admin.license.updates.only.has.ended"));
        assertTrue(message.contains("admin.license.renew.for.deprecated"));
    }

    @Test
    public void testGetLicenseStatusMessageForAlmostMaintenanceExpiredLicense() throws Exception {
        mockLicense.setMaintenanceExpiryDate(tenSecondsFromNow);
        String message = licenseDetails.getLicenseStatusMessage(fred, "", userManager);
        assertTrue(message.contains("admin.license.support.and.updates.will.end"));
        assertTrue(message.contains("admin.license.renew.for.support.and.updates.after"));
    }

    @Test
    public void testGetLicenseStatusMessageForAlmostMaintenanceExpiredPersonalLicense() throws Exception {
        mockLicense.setLicenseType(LicenseType.PERSONAL);
        mockLicense.setMaintenanceExpiryDate(tenSecondsFromNow);
        String message = licenseDetails.getLicenseStatusMessage(fred, "", userManager);
        assertTrue(message.contains("admin.license.updates.only.will.end"));
        assertTrue(message.contains("admin.license.renew.for.updates.only.after"));
    }

    @Test
    public void testGetLicenseStatusMessageForAlmostMaintenanceExpiredNonProfitLicense() throws Exception {
        mockLicense.setLicenseType(LicenseType.NON_PROFIT);
        mockLicense.setMaintenanceExpiryDate(tenSecondsFromNow);

        String message = licenseDetails.getLicenseStatusMessage(fred, "", userManager);
        assertTrue(message.contains("admin.license.updates.only.will.end"));
        assertTrue(message.contains("admin.license.renew.for.deprecated.after"));
    }

    @Test
    public void testGetLicenseStatusMessageForMaintenanceDefinitelyNotExpired() throws Exception {
        mockLicense.setNumberOfDaysBeforeMaintenanceExpiry(50);
        assertNull(licenseDetails.getLicenseStatusMessage(fred, "", userManager));
    }

    @Test
    public void testIsEntitledToSupport() throws Exception {
        assertSupportEntitlement(LicenseType.ACADEMIC, true);
        assertSupportEntitlement(LicenseType.COMMUNITY, true);
        assertSupportEntitlement(LicenseType.COMMERCIAL, true);
        assertSupportEntitlement(LicenseType.DEMONSTRATION, false);
        assertSupportEntitlement(LicenseType.DEVELOPER, true);
        assertSupportEntitlement(LicenseType.HOSTED, true);
        assertSupportEntitlement(LicenseType.NON_PROFIT, false);
        assertSupportEntitlement(LicenseType.OPEN_SOURCE, true);
        assertSupportEntitlement(LicenseType.PERSONAL, false);
        assertSupportEntitlement(LicenseType.STARTER, true);
        assertSupportEntitlement(LicenseType.TESTING, false);
    }

    @Test
    public void testIsAlmostExpiredEvaluation() throws Exception {
        mockLicense.setEvaluation(true);
        mockLicense.setExpiryDate(tenSecondsFromNow);
        assertTrue(licenseDetails.isLicenseAlmostExpired());
    }

    @Test
    public void testIsNotAlmostExpiredEvaluation() throws Exception {
        mockLicense.setEvaluation(true);
        mockLicense.setExpiryDate(tenDaysFromNow);
        assertFalse(licenseDetails.isLicenseAlmostExpired());
    }

    @Test
    public void testIsAlmostExpiredNewBuildOldLicense() throws Exception {
        inGraceMode();
        withExpiredLicense();
        setLicenseExtensionTimestamp(twentyFourDaysAgo.getTime());
        assertTrue(licenseDetails.isLicenseAlmostExpired());
    }

    @Test
    public void testIsNotAlmostExpiredNewBuildOldLicense() throws Exception {
        mockLicense.setMaintenanceExpiryDate(tenSecondsBeforeNow);
        setLicenseExtensionTimestamp(tenDaysAgoInMillis);
        assertFalse(licenseDetails.isLicenseAlmostExpired());
    }

    @Test
    public void testGetLicenseVersion() throws Exception {
        mockLicense.setLicenseVersion(666);
        assertEquals(666, licenseDetails.getLicenseVersion());
    }

    private void assertSupportEntitlement(final LicenseType licenceType, final boolean expectedEntitlement) {
        mockLicense.setLicenseType(licenceType);
        assertEquals(licenceType.name(), expectedEntitlement, licenseDetails.isEntitledToSupport());
    }

    private void setLicenseExtensionTimestamp(final long timestamp) {
        applicationProperties.setString(APKeys.JIRA_CONFIRMED_INSTALL_WITH_OLD_LICENSE_TIMESTAMP, String.valueOf(timestamp));
    }

    @Test
    public void clusteringForScaleShouldBeOffByDefault() {
        assertFalse("Clustering for scale should default to off", licenseDetails.isDataCenter());
    }

    @Test
    public void clusteringForScaleShouldBeOnWhenPropertySetToTrue() {
        // Set up
        mockLicense.setProperty(DATACENTER_PROPERTY_NAME, ENABLED);

        // Invoke and check
        assertTrue("Clustering for scale should be on", licenseDetails.isDataCenter());
    }

    @Test
    public void clusteringForScaleShouldBeOffWhenPropertySetToFalse() {
        // Set up
        mockLicense.setProperty(DATACENTER_PROPERTY_NAME, "false");

        // Invoke and check
        assertFalse("Clustering for scale should be off", licenseDetails.isDataCenter());
    }

    @Test
    public void getDaysToExpiryOnEvaluationLicense() {
        mockLicense.setExpiryDate(tenDaysFromNow);
        mockLicense.setEvaluation(true);

        //License expires in exactly 10 days.
        assertEquals(10, licenseDetails.getDaysToLicenseExpiry());

        //License expires in exactly 10 days + 1 ms.
        mockLicense.setExpiryDate(new Date(tenDaysFromNow.getTime() + 1));
        assertEquals(10, licenseDetails.getDaysToLicenseExpiry());

        //License expires in exactly 10 days - 1 ms.
        mockLicense.setExpiryDate(new Date(tenDaysFromNow.getTime() - 1));
        assertEquals(9, licenseDetails.getDaysToLicenseExpiry());

        //License expires now.
        mockLicense.setExpiryDate(now);
        assertEquals(0, licenseDetails.getDaysToLicenseExpiry());

        //License expires 1ms into the future.
        mockLicense.setExpiryDate(new Date(now.getTime() + 1));
        assertEquals(0, licenseDetails.getDaysToLicenseExpiry());

        //License expired 1ms ago.
        mockLicense.setExpiryDate(new Date(now.getTime() - 1));
        assertEquals(-1, licenseDetails.getDaysToLicenseExpiry());

        //License expired a long time ago.
        mockLicense.setExpiryDate(twentyFourDaysAgo);
        mockLicense.setEvaluation(true);

        //License expired exactly 24 days ago.
        assertEquals(-24, licenseDetails.getDaysToLicenseExpiry());

        //License expired exactly 1ms later than 24 days ago.
        mockLicense.setExpiryDate(new Date(twentyFourDaysAgo.getTime() + 1));
        assertEquals(-24, licenseDetails.getDaysToLicenseExpiry());

        //License expired exactly 1ms earlier than 24 days ago.
        mockLicense.setExpiryDate(new Date(twentyFourDaysAgo.getTime() - 1));
        assertEquals(-25, licenseDetails.getDaysToLicenseExpiry());
    }

    @Test
    public void getDaysToExpiryOnLicenseExtension() {
        //Make sure JIRA is way out of maintenance.
        inGraceMode();
        withExpiredLicense();

        //We said continue under eval 10 days ago.
        setLicenseExtensionTimestamp(tenDaysAgoInMillis);
        //This leaves 20 days of eval left.
        assertEquals(20, licenseDetails.getDaysToLicenseExpiry());

        //We said continue under eval 10 days + 1 ms ago.
        setLicenseExtensionTimestamp(tenDaysAgoInMillis + 1);
        //This leaves 20 days of eval left.
        assertEquals(20, licenseDetails.getDaysToLicenseExpiry());

        //We said continue under eval 10 days - 1 ms ago.
        setLicenseExtensionTimestamp(tenDaysAgoInMillis - 1);
        //This leaves 19 days of eval left.
        assertEquals(19, licenseDetails.getDaysToLicenseExpiry());

        //We said continue under eval 30 days ago.
        setLicenseExtensionTimestamp(thirtyDaysAgoInMillis);
        //This leaves 0 days of eval left.
        assertEquals(0, licenseDetails.getDaysToLicenseExpiry());

        //We said continue under eval 30 + 1ms days ago.
        setLicenseExtensionTimestamp(thirtyDaysAgoInMillis + 1);
        //This leaves 0 days of eval left.
        assertEquals(0, licenseDetails.getDaysToLicenseExpiry());

        //We said continue under eval 30 - 1ms days ago.
        setLicenseExtensionTimestamp(thirtyDaysAgoInMillis - 1);
        //This leaves -1 days of eval left (i.e. it has expired)
        assertEquals(-1, licenseDetails.getDaysToLicenseExpiry());
    }

    @Test
    public void getDaysToExpiryOnPerpetualLicense() {
        mockLicense.setExpiryDate(null);
        mockLicense.setEvaluation(false);

        assertEquals(Integer.MAX_VALUE, licenseDetails.getDaysToLicenseExpiry());
    }

    @Test
    public void getDaysToMaintenanceExpiryOnPerpetualLicense() {
        mockLicense.setMaintenanceExpiryDate(null);

        assertEquals(Integer.MAX_VALUE, licenseDetails.getDaysToMaintenanceExpiry());
    }

    @Test
    public void getDaysToMaintenanceExpiryOnEvaluationLicense() {
        mockLicense.setMaintenanceExpiryDate(tenDaysFromNow);

        //License expires in exactly 10 days.
        assertEquals(10, licenseDetails.getDaysToMaintenanceExpiry());

        //License expires in exactly 10 days + 1 ms.
        mockLicense.setMaintenanceExpiryDate(new Date(tenDaysFromNow.getTime() + 1));
        assertEquals(10, licenseDetails.getDaysToMaintenanceExpiry());

        //License expires in exactly 10 days - 1 ms.
        mockLicense.setMaintenanceExpiryDate(new Date(tenDaysFromNow.getTime() - 1));
        assertEquals(9, licenseDetails.getDaysToMaintenanceExpiry());

        //License expires now.
        mockLicense.setMaintenanceExpiryDate(now);
        assertEquals(0, licenseDetails.getDaysToMaintenanceExpiry());

        //License expires 1ms into the future.
        mockLicense.setMaintenanceExpiryDate(new Date(now.getTime() + 1));
        assertEquals(0, licenseDetails.getDaysToMaintenanceExpiry());

        //License expired 1ms ago.
        mockLicense.setMaintenanceExpiryDate(new Date(now.getTime() - 1));
        assertEquals(-1, licenseDetails.getDaysToMaintenanceExpiry());

        //License expired a long time ago.
        mockLicense.setMaintenanceExpiryDate(twentyFourDaysAgo);

        //License expired exactly 24 days ago.
        assertEquals(-24, licenseDetails.getDaysToMaintenanceExpiry());

        //License expired exactly 24 days - 1ms ago (negative offset)
        mockLicense.setMaintenanceExpiryDate(new Date(twentyFourDaysAgo.getTime() + 1));
        assertEquals(-24, licenseDetails.getDaysToMaintenanceExpiry());

        //License expired exactly 24 days + 1ms ago (negative offset).
        mockLicense.setMaintenanceExpiryDate(new Date(twentyFourDaysAgo.getTime() - 1));
        assertEquals(-25, licenseDetails.getDaysToMaintenanceExpiry());
    }

    @Test
    public void licenseHasUnlimitedUsersWhenRunningInOnDemand() {
        mockLicense.setProperty(ONDEMAND_PROPERTY_NAME, null);
        mockLicense.setNumberOfUsers(54321);

        assertThat("Is not a OD license", false, equalTo(licenseDetails.isOnDemand()));
        assertThat("License has limited users", false, equalTo(licenseDetails.isUnlimitedNumberOfUsers()));

        mockLicense.setProperty(ONDEMAND_PROPERTY_NAME, ENABLED);

        assertThat("Is a OD license", true, equalTo(licenseDetails.isOnDemand()));
        assertThat("License has unlimited users in OD", true, equalTo(licenseDetails.isUnlimitedNumberOfUsers()));
    }

    @Test
    public void testThatLicenseWithMaintenanceExpiryDateBeforeBuildDateIsNotValid() {
        //Build date is after maintenance date = New build  old license, license not valid
        mockLicense.setMaintenanceExpiryDate(toDate("yyyy-MM-dd", "2014-11-22"));

        assertFalse(licenseDetails.isMaintenanceValidForBuildDate(toDate("yyyy-MM-dd", "2014-11-23")));
    }

    @Test
    public void testThatLicenseWithMaintenanceExpiryDateAfterBuildDateIsValid() {
        //Build date is before maintenance date = still in maintenance period, license valid
        mockLicense.setMaintenanceExpiryDate(toDate("yyyy-MM-dd", "2014-11-24"));

        assertTrue(licenseDetails.isMaintenanceValidForBuildDate(toDate("yyyy-MM-dd", "2014-11-23")));
    }

    @Test
    public void testThatLicenseWithMaintenanceExpiryDateSameAsBuildDateIsValid() {
        //Build date is same as maintenance date = still in maintenance period, license valid
        mockLicense.setMaintenanceExpiryDate(toDate("yyyy-MM-dd", "2014-11-23"));

        assertTrue(licenseDetails.isMaintenanceValidForBuildDate(toDate("yyyy-MM-dd", "2014-11-23")));
    }

    @Test
    public void isStarterShouldReturnTrueWhenFieldIsTrueInLicense_oneApplication() {
        ApplicationKey starterAppKey = ApplicationKey.valueOf("jira.product.test-app");
        mockLicense.setProperty(getApplicationLicensePropertyName(starterAppKey, STARTER_PROPERTY_NAME), ENABLED);
        when(licensedApplications.getKeys()).thenReturn(ImmutableSet.of(starterAppKey));

        assertTrue(licenseDetails.isStarter());
    }

    @Test
    public void isStarterShouldReturnTrueWhenFieldsAreTrueInLicense_multipleApplications() {
        ApplicationKey starterAppKey1 = ApplicationKey.valueOf("jira.product.test-app-one");
        ApplicationKey starterAppKey2 = ApplicationKey.valueOf("jira.product.test-app-two");
        mockLicense.setProperty(getApplicationLicensePropertyName(starterAppKey1, STARTER_PROPERTY_NAME), ENABLED);
        mockLicense.setProperty(getApplicationLicensePropertyName(starterAppKey2, STARTER_PROPERTY_NAME), ENABLED);
        when(licensedApplications.getKeys()).thenReturn(ImmutableSet.of(starterAppKey1, starterAppKey2));

        assertTrue(licenseDetails.isStarter());
    }

    @Test
    public void isStarterShouldReturnFalseWhenFieldNotInLicense() {
        assertFalse(licenseDetails.isStarter());
    }

    @Test
    public void isStarterShouldReturnFalseWhenFieldIsFalseInLicense_oneApplication() {
        ApplicationKey nonStarterAppKey = ApplicationKey.valueOf("jira.product.test-app");
        mockLicense.setProperty(getApplicationLicensePropertyName(nonStarterAppKey, STARTER_PROPERTY_NAME), NOT_ENABLED);
        when(licensedApplications.getKeys()).thenReturn(ImmutableSet.of(nonStarterAppKey));

        assertFalse(licenseDetails.isStarter());
    }

    @Test
    public void isStarterShouldReturnFalseWhenAFieldIsFalseInLicense_multipleApplications() {
        ApplicationKey starterAppKey = ApplicationKey.valueOf("jira.product.test-app-one");
        ApplicationKey nonStarterAppKey = ApplicationKey.valueOf("jira.product.test-app-two");
        mockLicense.setProperty(getApplicationLicensePropertyName(starterAppKey, STARTER_PROPERTY_NAME), ENABLED);
        mockLicense.setProperty(getApplicationLicensePropertyName(nonStarterAppKey, STARTER_PROPERTY_NAME), NOT_ENABLED);
        when(licensedApplications.getKeys()).thenReturn(ImmutableSet.of(starterAppKey, nonStarterAppKey));

        assertFalse(licenseDetails.isStarter());
    }

    @Test
    public void isStarterShouldReturnTrueWhenLicenseTypeIsStarter() {
        mockLicense.setLicenseType(LicenseType.STARTER);

        assertTrue(licenseDetails.isStarter());
    }

    @Test
    public void isNewBuildWithOldLicenseShouldReturnTrueIfAnyLicensesAreExpired() {
        inGraceMode();
        withExpiredLicense();

        DefaultLicenseDetails licenseDetails = new DefaultLicenseDetails(
                new JiraProductLicense(licensedApplications, mockLicense),
                LICENSE_STRING, applicationProperties, externalLinkUtil,
                buildUtilsInfo, new NoopI18nFactory(), dateTimeFormatter,
                new ConstantClock(now.getTime()));

        assertTrue(licenseDetails.isNewBuildWithOldLicense());
    }

    @Test
    public void isNewBuildWithOldLicenseShouldReturnFalseIfNotInCorrectMode() {
        withExpiredLicense();
        //explicitly set this because it's what we're testing. Yes, this is a little paranoid.
        applicationProperties.setOption(APKeys.JIRA_CONFIRMED_INSTALL_WITH_OLD_LICENSE, false);

        DefaultLicenseDetails licenseDetails = new DefaultLicenseDetails(
                new JiraProductLicense(licensedApplications, mockLicense),
                LICENSE_STRING, applicationProperties, externalLinkUtil,
                buildUtilsInfo, new NoopI18nFactory(), dateTimeFormatter,
                new ConstantClock(now.getTime()));

        assertFalse(licenseDetails.isNewBuildWithOldLicense());
    }

    @Test
    public void isNewBuildWithOldLicenseShouldReturnFalseIfNoLicensesAreExpired() {
        inGraceMode();
        when(licenseMaintenancePredicate.test(any())).thenReturn(true);

        DefaultLicenseDetails licenseDetails = new DefaultLicenseDetails(
                new JiraProductLicense(licensedApplications, mockLicense),
                LICENSE_STRING, applicationProperties, externalLinkUtil,
                buildUtilsInfo, new NoopI18nFactory(), dateTimeFormatter,
                new ConstantClock(now.getTime()));

        assertFalse(licenseDetails.isNewBuildWithOldLicense());
    }

    private void inGraceMode() {
        applicationProperties.setOption(APKeys.JIRA_CONFIRMED_INSTALL_WITH_OLD_LICENSE, true);
    }

    private void withExpiredLicense() {
        when(licenseMaintenancePredicate.test(any())).thenReturn(false);
    }

    private static Date toDate(final String dateFormat, final String dateString) {
        return DateTimeFormat.forPattern(dateFormat).parseDateTime(dateString).toDate();
    }

    @Test
    public void isPaidTypeShouldReturnTrueWhenTypeIsAcademicCommercialOrStarter() {
        // Academic
        mockLicense.setLicenseType(LicenseType.ACADEMIC);
        assertTrue(licenseDetails.isPaidType());

        // Commercial
        mockLicense.setLicenseType(LicenseType.COMMERCIAL);
        assertTrue(licenseDetails.isPaidType());

        // Starter
        mockLicense.setLicenseType(LicenseType.STARTER);
        assertTrue(licenseDetails.isPaidType());
    }

    @Test
    public void isPaidTypeShouldReturnFalseForNonPaidTypes() {
        // Developer
        mockLicense.setLicenseType(LicenseType.DEVELOPER);
        assertFalse(licenseDetails.isPaidType());

        // Community
        mockLicense.setLicenseType(LicenseType.COMMUNITY);
        assertFalse(licenseDetails.isPaidType());

        // Open source
        mockLicense.setLicenseType(LicenseType.OPEN_SOURCE);
        assertFalse(licenseDetails.isPaidType());
    }
}
