package com.atlassian.jira.jql.resolver;

import com.atlassian.jira.issue.security.IssueSecurityLevel;
import com.atlassian.jira.issue.security.IssueSecurityLevelImpl;
import com.atlassian.jira.issue.security.IssueSecurityLevelManager;
import com.atlassian.jira.jql.operand.QueryLiteral;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.google.common.collect.ImmutableList;
import org.hamcrest.Matchers;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.ofbiz.core.entity.GenericValue;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static com.atlassian.jira.jql.operand.SimpleLiteralFactory.createLiteral;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.only;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestIssueSecurityLevelResolver {
    public static final String THE_NAME = "TheName";
    @Rule
    public final MethodRule initMockito = MockitoJUnit.rule();

    @Mock
    private IssueSecurityLevelManager manager;
    private ApplicationUser theUser = new MockApplicationUser("fred");
    private boolean overrideSecurity = false;

    @Test
    public void getAllSecurityLevels() throws Exception {
        List<IssueSecurityLevel> expectedList = ImmutableList.of(createMockSecurityLevel(4L, "4"), createMockSecurityLevel(5L, "5"));

        when(manager.getAllSecurityLevelsForUser(theUser)).thenReturn(expectedList);

        IssueSecurityLevelResolver resolver = new IssueSecurityLevelResolver(manager);
        List<IssueSecurityLevel> actualList = resolver.getAllSecurityLevels(theUser);
        assertThat(actualList, is(expectedList));
    }

    @Test
    public void getIssueSecurityLevelByIdHappyPath() throws Exception {
        final IssueSecurityLevel expectedLevel = createMockSecurityLevel(1L, "1");
        final Collection<IssueSecurityLevel> levels = ImmutableList.of(
                createMockSecurityLevel(3L, "3"),
                createMockSecurityLevel(2L, "2"),
                expectedLevel
        );

        when(manager.getAllSecurityLevelsForUser(theUser)).thenReturn(levels);

        IssueSecurityLevelResolver resolver = new IssueSecurityLevelResolver(manager);
        final IssueSecurityLevel level = resolver.getIssueSecurityLevelById(theUser, overrideSecurity, 1L);
        assertThat(level, is(expectedLevel));
    }

    @Test
    public void getIssueSecurityLevelByIdOverrideSecurity() throws Exception {
        overrideSecurity = true;
        final IssueSecurityLevel expectedLevel = createMockSecurityLevel(1L, "1");
        final Collection<IssueSecurityLevel> levels = ImmutableList.of(
                createMockSecurityLevel(3L, "3"),
                createMockSecurityLevel(2L, "2"),
                expectedLevel
        );

        when(manager.getAllIssueSecurityLevels()).thenReturn(levels);

        IssueSecurityLevelResolver resolver = new IssueSecurityLevelResolver(manager);
        final IssueSecurityLevel level = resolver.getIssueSecurityLevelById(theUser, overrideSecurity, 1L);
        assertThat(level, is(expectedLevel));
    }

    @Test
    public void getIssueSecurityLevelByIdNotFound() throws Exception {
        final Collection<IssueSecurityLevel> levels = ImmutableList.of(
                createMockSecurityLevel(3L, "3"),
                createMockSecurityLevel(2L, "2"),
                createMockSecurityLevel(1L, "1")
        );

        when(manager.getAllSecurityLevelsForUser(theUser)).thenReturn(levels);

        IssueSecurityLevelResolver resolver = new IssueSecurityLevelResolver(manager);
        final IssueSecurityLevel level = resolver.getIssueSecurityLevelById(theUser, overrideSecurity, 4L);
        assertNull(level);
    }

    @Test
    public void getIssueSecurityLevelsByNameHappyPath() throws Exception {
        final Collection<IssueSecurityLevel> expectedLevels = ImmutableList.of(
                createMockSecurityLevel(3L, "3"),
                createMockSecurityLevel(2L, "2"),
                createMockSecurityLevel(1L, THE_NAME)
        );

        when(manager.getSecurityLevelsForUserByName(theUser, THE_NAME)).thenReturn(expectedLevels);

        IssueSecurityLevelResolver resolver = new IssueSecurityLevelResolver(manager);
        final Collection<IssueSecurityLevel> levels = resolver.getIssueSecurityLevelsByName(theUser, overrideSecurity, THE_NAME);
        assertThat(levels, is(expectedLevels));
    }

    @Test
    public void getIssueSecurityLevelsByNameOverrideSecurity() throws Exception {
        overrideSecurity = true;
        final Collection<IssueSecurityLevel> expectedLevels = ImmutableList.of(
                createMockSecurityLevel(3L, "3"),
                createMockSecurityLevel(2L, "2"),
                createMockSecurityLevel(1L, THE_NAME)
        );

        when(manager.getIssueSecurityLevelsByName(THE_NAME)).thenReturn(expectedLevels);

        IssueSecurityLevelResolver resolver = new IssueSecurityLevelResolver(manager);
        final Collection<IssueSecurityLevel> levels = resolver.getIssueSecurityLevelsByName(theUser, overrideSecurity, THE_NAME);
        assertThat(levels, is(expectedLevels));
    }

    @Test
    public void getIssueSecurityLevelsUsingStringHappyPath() throws Exception {
        final Collection<IssueSecurityLevel> expectedLevels = ImmutableList.of(
                createMockSecurityLevel(2L, THE_NAME),
                createMockSecurityLevel(1L, THE_NAME)
        );

        IssueSecurityLevelResolver resolver = getTestIssueSecurityLevelResolver(expectedLevels);
        final Collection<IssueSecurityLevel> levels = resolver.getIssueSecurityLevels(theUser, createLiteral(THE_NAME));
        assertThat(levels, is(expectedLevels));
    }

    @Test
    public void getIssueSecurityLevelsUsingStringOverrideSecurity() throws Exception {
        IssueSecurityLevelResolver resolver = new IssueSecurityLevelResolver(manager);

        resolver.getIssueSecurityLevelsOverrideSecurity(ImmutableList.of(createLiteral(THE_NAME)));

        verify(manager, only()).getIssueSecurityLevelsByName(THE_NAME);
    }

    @Test
    public void getIssueSecurityLevelsUsingStringNameDoesntExistIsntId() throws Exception {
        IssueSecurityLevelResolver resolver = getTestIssueSecurityLevelResolver((Collection) null);
        final Collection<IssueSecurityLevel> levels = resolver.getIssueSecurityLevels(theUser, createLiteral(THE_NAME));
        assertThat(levels, empty());
    }

    @Test
    public void getIssueSecurityLevelsUsingStringNameDoesntExistIsIdAndExists() throws Exception {
        final IssueSecurityLevel expectedLevel = createMockSecurityLevel(1L, THE_NAME);

        IssueSecurityLevelResolver resolver = getTestIssueSecurityLevelResolver(null, expectedLevel);
        final Collection<IssueSecurityLevel> levels = resolver.getIssueSecurityLevels(theUser, createLiteral("1"));
        assertThat(levels, Matchers.contains(expectedLevel));
    }

    @Test
    public void getIssueSecurityLevelsUsingStringNameDoesntExistIsIdDoesntExist() throws Exception {
        IssueSecurityLevelResolver resolver = getTestIssueSecurityLevelResolver(null, null);
        final Collection<IssueSecurityLevel> levels = resolver.getIssueSecurityLevels(theUser, createLiteral("1"));
        assertThat(levels, empty());
    }

    @Test
    public void getIssueSecurityLevelsUsingEmptyLiteral() throws Exception {
        final Collection<GenericValue> expectedLevels = Collections.singletonList(null);

        IssueSecurityLevelResolver resolver = new IssueSecurityLevelResolver(manager);

        final Collection<IssueSecurityLevel> levels = resolver.getIssueSecurityLevels(theUser, new QueryLiteral());
        assertThat(levels, is(expectedLevels));
    }

    @Test
    public void getIssueSecurityLevelsUsingLongHappyPath() throws Exception {
        final IssueSecurityLevel expectedLevel = createMockSecurityLevel(2L, THE_NAME);
        final Collection<IssueSecurityLevel> expectedLevels = ImmutableList.of(expectedLevel);

        IssueSecurityLevelResolver resolver = getTestIssueSecurityLevelResolver(expectedLevel);
        final Collection<IssueSecurityLevel> levels = resolver.getIssueSecurityLevels(theUser, createLiteral(2L));
        assertThat(levels, is(expectedLevels));
    }

    @Test
    public void getIssueSecurityLevelsUsingLongIdDoesntExistNameDoes() throws Exception {
        final IssueSecurityLevel expectedLevel = createMockSecurityLevel(2L, "2");
        final Collection<IssueSecurityLevel> expectedLevels = ImmutableList.of(expectedLevel);

        IssueSecurityLevelResolver resolver = getTestIssueSecurityLevelResolver(expectedLevels, null);
        final Collection<IssueSecurityLevel> levels = resolver.getIssueSecurityLevels(theUser, createLiteral(2L));
        assertThat(levels, is(expectedLevels));
    }

    @Test
    public void getIssueSecurityLevelsUsingLongIdDoesntExistNameDoesnt() throws Exception {
        IssueSecurityLevelResolver resolver = getTestIssueSecurityLevelResolver(null, null);
        final Collection<IssueSecurityLevel> levels = resolver.getIssueSecurityLevels(theUser, createLiteral(2L));
        assertThat(levels, empty());
    }

    private IssueSecurityLevel createMockSecurityLevel(final Long id, final String name) {
        return new IssueSecurityLevelImpl(id, name, null, null);
    }

    private IssueSecurityLevelResolver getTestIssueSecurityLevelResolver(final Collection<IssueSecurityLevel> issueSecurityLevelsByName) {
        return new IssueSecurityLevelResolver(manager) {
            @Override
            Collection<IssueSecurityLevel> getIssueSecurityLevelsByName(final ApplicationUser searcher, final boolean overrideSecurity, final String nameValue) {
                return issueSecurityLevelsByName;
            }
        };
    }

    private IssueSecurityLevelResolver getTestIssueSecurityLevelResolver(final IssueSecurityLevel issueSecurityLevelById) {
        return new IssueSecurityLevelResolver(manager) {
            @Override
            IssueSecurityLevel getIssueSecurityLevelById(final ApplicationUser searcher, final boolean overrideSecurity, final Long valueAsLong) {
                return issueSecurityLevelById;
            }
        };
    }

    private IssueSecurityLevelResolver getTestIssueSecurityLevelResolver(final Collection<IssueSecurityLevel> issueSecurityLevelsByName, final IssueSecurityLevel issueSecurityLevelById) {
        return new IssueSecurityLevelResolver(manager) {
            @Override
            IssueSecurityLevel getIssueSecurityLevelById(final ApplicationUser searcher, final boolean overrideSecurity, final Long valueAsLong) {
                return issueSecurityLevelById;
            }

            @Override
            Collection<IssueSecurityLevel> getIssueSecurityLevelsByName(final ApplicationUser searcher, final boolean overrideSecurity, final String nameValue) {
                return issueSecurityLevelsByName;
            }
        };
    }
}
