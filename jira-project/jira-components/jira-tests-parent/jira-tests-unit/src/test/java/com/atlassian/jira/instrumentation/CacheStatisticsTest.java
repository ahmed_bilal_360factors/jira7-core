package com.atlassian.jira.instrumentation;

import com.atlassian.vcache.internal.MetricLabel;
import com.google.common.collect.ImmutableMap;
import org.junit.Test;

import java.util.Map;

import static org.hamcrest.collection.IsMapContaining.hasEntry;
import static org.junit.Assert.assertThat;


public class CacheStatisticsTest {
    @Test
    public void conflictingKeysInOtherStatsAreRemovedFromFinalStatistics() {
        final Map<String, Object> stats = new CacheStatistics.CacheStatisticsBuilder()
                .withName("cache1")
                .withHits(1)
                .withMisses(1)
                .withOtherStats(ImmutableMap.of(
                        CacheMetricsKeys.HITS.key(), "5",
                        MetricLabel.NUMBER_OF_HITS.name(), "5"
                ))
                .build()
                .getStatsMap();

        assertThat(stats, hasEntry(CacheMetricsKeys.HITS.key(), 1L));
        assertThat(stats, hasEntry(MetricLabel.NUMBER_OF_HITS.name(), "5"));
    }
}