package com.atlassian.jira.sharing.type;

import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.bc.JiraServiceContextImpl;
import com.atlassian.jira.mock.MockProjectRoleManager;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.security.roles.ProjectRoleManager;
import com.atlassian.jira.sharing.SharePermission;
import com.atlassian.jira.sharing.SharePermissionImpl;
import com.atlassian.jira.sharing.search.PrivateShareTypeSearchParameter;
import com.atlassian.jira.sharing.search.ProjectShareTypeSearchParameter;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.jira.web.bean.MockI18nBean;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static com.atlassian.jira.permission.ProjectPermissions.BROWSE_PROJECTS;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

/**
 * Unit tests for {@link com.atlassian.jira.sharing.type.ProjectShareTypeValidator}
 *
 * @since v3.13
 */
public class TestProjectShareTypeValidator {
    @Rule
    public MockitoRule mockito = MockitoJUnit.rule();
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    private static final long MOCK_ROLE_ID = 20000;
    private static final long MOCK_PROJECT_ID = 10000;
    private static final MockProjectRoleManager.MockProjectRole MOCK_ROLE = new MockProjectRoleManager.MockProjectRole(MOCK_ROLE_ID, "Role1", "Role1");
    private static final MockProject MOCK_PROJECT = new MockProject(MOCK_PROJECT_ID, "PROJ", "PROJ");
    private static final SharePermissionImpl PROJECT_PERMISSION = new SharePermissionImpl(ProjectShareType.TYPE, "" + MOCK_PROJECT_ID, null);
    private static final SharePermissionImpl PROJECT_ROLE_PERMISSION = new SharePermissionImpl(ProjectShareType.TYPE, "" + MOCK_PROJECT_ID, "" + MOCK_ROLE_ID);

    @Mock
    protected PermissionManager permissionManager;
    @Mock
    protected ProjectManager projectManager;
    @Mock
    protected ProjectRoleManager projectRoleManager;

    private JiraServiceContext context;
    private ApplicationUser user;
    private ProjectShareTypeValidator projectShareTypeValidator;

    @Before
    public void setUp() throws Exception {
        user = new MockApplicationUser("test");
        context = createServiceContext(user);
        projectShareTypeValidator = new ProjectShareTypeValidator(permissionManager, projectManager, projectRoleManager);
    }

    @Test
    public void testCheckSharePermissionNullContext() {
        expectedException.expect(IllegalArgumentException.class);
        projectShareTypeValidator.checkSharePermission(null, PROJECT_PERMISSION);
    }

    @Test
    public void testCheckSharePermissionNullUserInContext() {
        JiraServiceContext nullUserContext = createServiceContext(null);

        expectedException.expect(IllegalArgumentException.class);
        projectShareTypeValidator.checkSharePermission(nullUserContext, PROJECT_PERMISSION);
    }

    @Test
    public void testCheckSharePermissionNullPermission() {
        expectedException.expect(IllegalArgumentException.class);
        projectShareTypeValidator.checkSharePermission(context, null);
    }

    @Test
    public void testCheckSharePermissionInvalidType() {
        final SharePermissionImpl invalidPermission = new SharePermissionImpl(GlobalShareType.TYPE, "coolgroup", null);

        expectedException.expect(IllegalArgumentException.class);
        projectShareTypeValidator.checkSharePermission(context, invalidPermission);
    }

    @Test
    public void testCheckSharePermissionNullParam1() {
        hasCreateObjectsPermission();

        final SharePermissionImpl nullParam1Permission = new SharePermissionImpl(ProjectShareType.TYPE, null, null);

        assertFalse(projectShareTypeValidator.checkSharePermission(context, nullParam1Permission));
        assertTrue(context.getErrorCollection().hasAnyErrors());
    }

    @Test
    public void testCheckSharePermissionInvalidParam1() {
        hasCreateObjectsPermission();

        final SharePermissionImpl nullParam1Permission = new SharePermissionImpl(ProjectShareType.TYPE, "abc", null);

        assertFalse(projectShareTypeValidator.checkSharePermission(context, nullParam1Permission));
        assertTrue(context.getErrorCollection().hasAnyErrors());
    }

    @Test
    public void testCheckSharePermissionNoPermission() {
        assertFalse(projectShareTypeValidator.checkSharePermission(context, PROJECT_PERMISSION));
        assertTrue(context.getErrorCollection().hasAnyErrors());
    }

    @Test
    public void testCheckSharePermissionProjectDoesNotExist() {
        hasCreateObjectsPermission();

        assertFalse(projectShareTypeValidator.checkSharePermission(context, PROJECT_PERMISSION));
        assertTrue(context.getErrorCollection().hasAnyErrors());
    }


    @Test
    public void testCheckSharePermissionNoBrowsePermission() {
        hasCreateObjectsPermission();
        hasProjectDefined();

        assertFalse(projectShareTypeValidator.checkSharePermission(context, PROJECT_PERMISSION));
        assertTrue(context.getErrorCollection().hasAnyErrors());
    }

    @Test
    public void testCheckSharePermissionWithBrowsePermission() {
        hasCreateObjectsPermission();
        hasBrowseProjectPermission();
        hasProjectDefined();

        assertTrue(projectShareTypeValidator.checkSharePermission(context, PROJECT_PERMISSION));
        assertFalse(context.getErrorCollection().hasAnyErrors());
    }


    @Test
    public void testCheckSharePermissionEmptyPerm2() {

        hasCreateObjectsPermission();
        hasProjectDefined();

        final SharePermission perm = new SharePermissionImpl(ProjectShareType.TYPE, "" + MOCK_PROJECT_ID, "");

        assertFalse(projectShareTypeValidator.checkSharePermission(context, perm));
        assertTrue(context.getErrorCollection().hasAnyErrors());
    }

    @Test
    public void testCheckSharePermissionInvalidPerm2() {
        hasCreateObjectsPermission();
        hasProjectDefined();

        final SharePermission perm = new SharePermissionImpl(ProjectShareType.TYPE, "" + MOCK_PROJECT_ID, "abc");

        assertFalse(projectShareTypeValidator.checkSharePermission(context, perm));
        assertTrue(context.getErrorCollection().hasAnyErrors());

    }

    @Test
    public void testCheckSharePermssionInvalidProjectRole() {
        hasCreateObjectsPermission();
        hasProjectDefined();

        assertFalse(projectShareTypeValidator.checkSharePermission(context, PROJECT_ROLE_PERMISSION));
        assertTrue(context.getErrorCollection().hasAnyErrors());
    }

    @Test
    public void testCheckSharePermssionWithProjectRoleNotInProject() {
        hasCreateObjectsPermission();
        hasProjectDefined();
        hasRoleDefined();
        hasBrowseProjectPermission();

        assertFalse(projectShareTypeValidator.checkSharePermission(context, PROJECT_ROLE_PERMISSION));
        assertTrue(context.getErrorCollection().hasAnyErrors());
    }

    @Test
    public void testCheckSharePermssionWithProjectRoleInProjectNoBrowse() {
        hasCreateObjectsPermission();
        hasProjectDefined();
        hasRoleDefined();

        assertFalse(projectShareTypeValidator.checkSharePermission(context, PROJECT_ROLE_PERMISSION));
        assertTrue(context.getErrorCollection().hasAnyErrors());
    }

    @Test
    public void testCheckSharePermssionWithProjectRoleInProject() {
        hasCreateObjectsPermission();
        hasProjectDefined();
        hasRoleDefined();
        hasBrowseProjectPermission();
        hasMockRoleInMockProject();

        assertTrue(projectShareTypeValidator.checkSharePermission(context, PROJECT_ROLE_PERMISSION));
        assertFalse(context.getErrorCollection().hasAnyErrors());
    }


    @Test
    public void testCheckSearchParameterInvalidProject() throws Exception {
        final Long roleId = null;

        boolean result = projectShareTypeValidator.checkSearchParameter(context, new ProjectShareTypeSearchParameter(MOCK_PROJECT_ID, roleId));
        assertFalse(result);

        final ErrorCollection errorCollection = context.getErrorCollection();
        assertTrue(errorCollection.hasAnyErrors());
        assertTrue(errorCollection.getErrors().get(ShareTypeValidator.ERROR_KEY).equals("common.sharing.searching.exception.project.does.not.exist"));
    }

    @Test
    public void testCheckSearchParameterInvalidRole() throws Exception {
        hasProjectDefined();

        boolean result = projectShareTypeValidator.checkSearchParameter(context, new ProjectShareTypeSearchParameter(MOCK_PROJECT_ID, MOCK_ROLE_ID));
        assertFalse(result);

        final ErrorCollection errorCollection = context.getErrorCollection();
        assertTrue(errorCollection.hasAnyErrors());
        assertTrue(errorCollection.getErrors().get(ShareTypeValidator.ERROR_KEY).equals("common.sharing.searching.exception.project.role.does.not.exist"));
    }

    @Test
    public void testCheckSearchParameterNotInRole() throws Exception {
        hasProjectDefined();
        hasRoleDefined();

        boolean result = projectShareTypeValidator.checkSearchParameter(context, new ProjectShareTypeSearchParameter(MOCK_PROJECT_ID, MOCK_ROLE_ID));
        assertFalse(result);

        final ErrorCollection errorCollection = context.getErrorCollection();
        assertTrue(errorCollection.hasAnyErrors());
        assertTrue(errorCollection.getErrors().get(ShareTypeValidator.ERROR_KEY).equals("common.sharing.searching.exception.user.not.in.project.role"));
    }

    @Test
    public void testCheckSearchParameterAllCool() throws Exception {
        hasProjectDefined();
        hasRoleDefined();
        hasMockRoleInMockProject();

        boolean result = projectShareTypeValidator.checkSearchParameter(context, new ProjectShareTypeSearchParameter(MOCK_PROJECT_ID, MOCK_ROLE_ID));
        assertTrue(result);

        final ErrorCollection errorCollection = context.getErrorCollection();
        assertFalse(errorCollection.hasAnyErrors());
    }


    @Test
    public void testCheckSearchParameterNullArgs() throws Exception {
        final Long projectId = null;
        final Long roleId = null;

        boolean result = projectShareTypeValidator.checkSearchParameter(context, new ProjectShareTypeSearchParameter(projectId, roleId));
        assertTrue(result);

        final ErrorCollection errorCollection = context.getErrorCollection();
        assertFalse(errorCollection.hasAnyErrors());
    }

    @Test
    public void testCheckSearchParameterBadArgs() throws Exception {

        ProjectShareTypeValidator projectShareTypeValidator = new ProjectShareTypeValidator(permissionManager, null, null);
        expectedException.expect(IllegalArgumentException.class);
        projectShareTypeValidator.checkSearchParameter(context, PrivateShareTypeSearchParameter.PRIVATE_PARAMETER);
    }

    private JiraServiceContext createServiceContext(ApplicationUser user) {
        return new JiraServiceContextImpl(user, new SimpleErrorCollection()) {
            public I18nHelper getI18nBean() {
                return new MockI18nBean() {
                    public String getText(final String key) {
                        return key;
                    }

                    public String getText(final String key, final String value1) {
                        return key;
                    }

                    public String getText(final String key, final String value1, final String value2) {
                        return key;
                    }

                    public String getText(final String key, final String value1, final String value2, final String value3) {
                        return key;
                    }

                    public String getText(final String key, final Object parameters) {
                        return key;
                    }
                };
            }
        };
    }

    private void hasProjectDefined() {
        when(projectManager.getProjectObj(MOCK_PROJECT_ID)).thenReturn(MOCK_PROJECT);
    }

    private void hasRoleDefined() {
        when(projectRoleManager.getProjectRole(MOCK_ROLE_ID)).thenReturn(MOCK_ROLE);
    }

    private void hasCreateObjectsPermission() {
        when(permissionManager.hasPermission(Permissions.CREATE_SHARED_OBJECTS, user)).thenReturn(true);
    }

    private void hasBrowseProjectPermission() {
        when(permissionManager.hasPermission(BROWSE_PROJECTS, MOCK_PROJECT, user)).thenReturn(true);
    }

    private void hasMockRoleInMockProject() {
        when(projectRoleManager.isUserInProjectRole(user, MOCK_ROLE, MOCK_PROJECT)).thenReturn(true);
    }


}
