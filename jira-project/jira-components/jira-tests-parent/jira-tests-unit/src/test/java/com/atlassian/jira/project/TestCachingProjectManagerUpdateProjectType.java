package com.atlassian.jira.project;

import com.atlassian.cache.CacheManager;
import com.atlassian.jira.association.NodeAssociationStore;
import com.atlassian.jira.bc.project.component.ProjectComponentManager;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.project.type.ProjectTypeKey;
import com.atlassian.jira.project.util.ProjectKeyStore;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestCachingProjectManagerUpdateProjectType {
    private static final Project PROJECT = new MockProject(1);
    private static final Project UPDATED_PROJECT = new MockProject(2);
    private static final ProjectTypeKey PROJECT_TYPE = new ProjectTypeKey("type");
    private static final ApplicationUser USER = new MockApplicationUser("user");

    @Mock
    private ProjectManager delegateProjectManager;

    private CachingProjectManagerSpy projectManager;

    @Before
    public void setUp() {
        projectManager = new CachingProjectManagerSpy(delegateProjectManager);
    }

    @Test
    public void delegatesToProjectManager() {
        when(delegateProjectManager.updateProjectType(USER, PROJECT, PROJECT_TYPE)).thenReturn(UPDATED_PROJECT);

        Project project = projectManager.updateProjectType(USER, PROJECT, PROJECT_TYPE);

        assertThat(project, is(UPDATED_PROJECT));
    }

    @Test
    public void updatesTheCache() {
        projectManager.updateProjectType(USER, PROJECT, PROJECT_TYPE);

        assertTrue(projectManager.cacheWasUpdated);
    }

    @Test
    public void updatesTheCacheEvenWhenThereIsAnExceptionBeingThrown() {
        when(delegateProjectManager.updateProjectType(USER, PROJECT, PROJECT_TYPE)).thenThrow(new RuntimeException());

        try {
            projectManager.updateProjectType(USER, PROJECT, PROJECT_TYPE);
            fail("A RuntimeException was expected");
        } catch (RuntimeException e) {
            assertTrue(projectManager.cacheWasUpdated);
        }
    }

    private class CachingProjectManagerSpy extends CachingProjectManager {
        public boolean cacheWasUpdated = false;

        public CachingProjectManagerSpy(ProjectManager delegateProjectManager) {
            super(
                    delegateProjectManager,
                    mock(ProjectComponentManager.class),
                    mock(ProjectFactory.class),
                    mock(UserManager.class),
                    mock(ApplicationProperties.class),
                    mock(ProjectKeyStore.class),
                    mock(CacheManager.class),
                    mock(NodeAssociationStore.class)
            );
        }

        @Override
        public void updateCache() {
            cacheWasUpdated = true;
        }
    }
}
