package com.atlassian.jira.jql.query;

import com.atlassian.jira.JiraDataTypes;
import com.atlassian.jira.issue.search.constants.SimpleFieldSearchConstants;
import com.atlassian.jira.jql.operand.JqlOperandResolver;
import com.atlassian.jira.jql.operand.QueryLiteral;
import com.atlassian.jira.jql.validator.MockJqlOperandResolver;
import com.atlassian.jira.junit.rules.InitMockitoMocks;
import com.atlassian.jira.util.collect.CollectionBuilder;
import com.atlassian.query.clause.TerminalClause;
import com.atlassian.query.clause.TerminalClauseImpl;
import com.atlassian.query.operand.EmptyOperand;
import com.atlassian.query.operand.MultiValueOperand;
import com.atlassian.query.operand.SingleValueOperand;
import com.atlassian.query.operator.Operator;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.mockito.Mock;

import java.util.Collections;
import java.util.List;

import static com.atlassian.jira.jql.operand.SimpleLiteralFactory.createLiteral;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestGenericClauseQueryFactory {
    @Rule
    public TestRule initMockito = new InitMockitoMocks(this);
    @Mock
    OperatorSpecificQueryFactory mockOperatorSpecificQueryFactory;
    @Mock
    OperatorSpecificQueryFactory mockOperatorSpecificQueryFactory2;

    @Test
    public void testGetQueryNoHandler() throws Exception {
        final SingleValueOperand operand = new SingleValueOperand("test");

        final GenericClauseQueryFactory clauseFactory = new GenericClauseQueryFactory(new SimpleFieldSearchConstants("testField", Collections.<Operator>emptySet(), JiraDataTypes.ALL), Collections.<OperatorSpecificQueryFactory>emptyList(), MockJqlOperandResolver.createSimpleSupport()) {
            @Override
            List<QueryLiteral> getRawValues(final QueryCreationContext queryCreationContext, final TerminalClause clause) {
                return TestGenericClauseQueryFactory.asList(createLiteral("1"), createLiteral("2"));
            }
        };

        final QueryFactoryResult result = clauseFactory.getQuery(null, new TerminalClauseImpl("testField", Operator.EQUALS, operand));
        assertEquals(QueryFactoryResult.createFalseResult(), result);
    }

    @Test
    public void testGetQueryNoOperandHandler() throws Exception {
        final SingleValueOperand operand = new SingleValueOperand("test");

        final JqlOperandResolver operandResolver = new MockJqlOperandResolver();

        final GenericClauseQueryFactory clauseFactory = new GenericClauseQueryFactory(new SimpleFieldSearchConstants("testField", Collections.<Operator>emptySet(), JiraDataTypes.ALL), Collections.<OperatorSpecificQueryFactory>emptyList(), operandResolver) {
            @Override
            List<QueryLiteral> getRawValues(final QueryCreationContext queryCreationContext, final TerminalClause clause) {
                return asList(createLiteral("1"), createLiteral("2"));
            }
        };

        final QueryFactoryResult result = clauseFactory.getQuery(null, new TerminalClauseImpl("testField", Operator.EQUALS, operand));
        assertEquals(QueryFactoryResult.createFalseResult(), result);
    }

    @Test
    public void testGetQueryListValues() throws Exception {
        final MultiValueOperand operand = new MultiValueOperand("test");

        final List<QueryLiteral> values = asList(createLiteral("1"), createLiteral("2"));

        when(mockOperatorSpecificQueryFactory.handlesOperator(Operator.EQUALS)).thenReturn(true);

        final GenericClauseQueryFactory clauseFactory = new GenericClauseQueryFactory(new SimpleFieldSearchConstants("testField", Collections.<Operator>emptySet(), JiraDataTypes.ALL), asList(mockOperatorSpecificQueryFactory2, mockOperatorSpecificQueryFactory), MockJqlOperandResolver.createSimpleSupport()) {
            @Override
            List<QueryLiteral> getRawValues(final QueryCreationContext queryCreationContext, final TerminalClause clause) {
                return values;
            }
        };

        clauseFactory.getQuery(null, new TerminalClauseImpl("testField", Operator.EQUALS, operand));

        verify(mockOperatorSpecificQueryFactory).handlesOperator(Operator.EQUALS);
        verify(mockOperatorSpecificQueryFactory).createQueryForMultipleValues("testField", Operator.EQUALS, values);
        verify(mockOperatorSpecificQueryFactory2).handlesOperator(Operator.EQUALS);
    }

    @Test
    public void testGetQuerySingleValue() throws Exception {
        final SingleValueOperand operand = new SingleValueOperand("test");

        final List<QueryLiteral> values = asList(createLiteral("1"), createLiteral("2"));

        when(mockOperatorSpecificQueryFactory.handlesOperator(Operator.EQUALS)).thenReturn(true);

        final GenericClauseQueryFactory clauseFactory = new GenericClauseQueryFactory(new SimpleFieldSearchConstants("testField", Collections.<Operator>emptySet(), JiraDataTypes.ALL), asList(mockOperatorSpecificQueryFactory), MockJqlOperandResolver.createSimpleSupport()) {
            @Override
            List<QueryLiteral> getRawValues(final QueryCreationContext queryCreationContext, final TerminalClause clause) {
                return values;
            }
        };

        clauseFactory.getQuery(null, new TerminalClauseImpl("testField", Operator.EQUALS, operand));

        verify(mockOperatorSpecificQueryFactory).handlesOperator(Operator.EQUALS);
        verify(mockOperatorSpecificQueryFactory).createQueryForSingleValue("testField", Operator.EQUALS, values);
    }

    @Test
    public void testGetQueryEmptyOperand() throws Exception {
        final EmptyOperand operand = new EmptyOperand();

        when(mockOperatorSpecificQueryFactory.handlesOperator(Operator.EQUALS)).thenReturn(true);

        final GenericClauseQueryFactory clauseFactory = new GenericClauseQueryFactory(new SimpleFieldSearchConstants("testField", Collections.<Operator>emptySet(), JiraDataTypes.ALL), asList(mockOperatorSpecificQueryFactory), MockJqlOperandResolver.createSimpleSupport());

        clauseFactory.getQuery(null, new TerminalClauseImpl("testField", Operator.EQUALS, operand));

        verify(mockOperatorSpecificQueryFactory).handlesOperator(Operator.EQUALS);
        verify(mockOperatorSpecificQueryFactory).createQueryForEmptyOperand("testField", Operator.EQUALS);
    }

    @SafeVarargs
    private static <T> List<T> asList(final T... elements) {
        return CollectionBuilder.newBuilder(elements).asList();
    }
}
