package com.atlassian.jira.project;

import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.mock.component.MockComponentWorker;
import com.atlassian.jira.mock.i18n.MockI18nHelper;
import com.atlassian.jira.project.type.BrowseProjectTypeManager;
import com.atlassian.jira.project.type.ProjectType;
import com.atlassian.jira.project.type.ProjectTypeKey;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.velocity.VelocityRequestContext;
import com.atlassian.jira.util.velocity.VelocityRequestContextFactory;
import com.atlassian.plugin.web.api.WebItem;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.atlassian.jira.project.ProjectTypesLinkFactory.ICON_PREFIX;
import static com.atlassian.jira.project.ProjectTypesLinkFactory.ICON_URL;
import static com.atlassian.jira.project.ProjectTypesLinkFactory.USER;
import static com.google.common.collect.Lists.newArrayList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class TestProjectTypesLinkFactory {
    private ProjectTypesLinkFactory projectTypesLinkFactory;
    @Mock
    private VelocityRequestContextFactory velocityRequestContextFactory;
    @Mock
    private ApplicationUser user;
    @Mock
    private BrowseProjectTypeManager browseProjectTypeManager;
    @Mock
    private VelocityRequestContext velocityRequestContext;
    @Mock
    private ApplicationProperties applicationProperties;
    @Mock
    private JiraAuthenticationContext authenticationContext;
    private static final String BUSINESS = "business";
    private static final String SOFTWARE = "software";
    private static final String SERVICE_DESK = "service_desk";
    private static final String BASE_URL = "http://localhost/jira";
    private static final int BUSINESS_WEIGHT = 10;
    private static final int SOFTWARE_WEIGHT = 20;
    private static final int SERVICE_DESK_WEIGHT = 30;

    @Before
    public void setUp() throws Exception {
        setUpEnv();
        when(velocityRequestContextFactory.getJiraVelocityRequestContext()).thenReturn(velocityRequestContext);
        when(browseProjectTypeManager.getAllProjectTypes(user)).thenReturn(projectTypes());
        when(velocityRequestContext.getBaseUrl()).thenReturn(BASE_URL);
        when(authenticationContext.getI18nHelper()).thenReturn(new MockI18nHelper());
        projectTypesLinkFactory = new ProjectTypesLinkFactory(velocityRequestContextFactory, browseProjectTypeManager, authenticationContext);
    }

    private void setUpEnv() {
        MockComponentWorker componentWorker = new MockComponentWorker();
        componentWorker.registerMock(ApplicationProperties.class, applicationProperties);
        when(applicationProperties.getEncoding()).thenReturn("UTF-8");
        componentWorker.init();
    }

    @Test
    public void shouldReturnAListOfProjectTypes() throws Exception {
        final Iterable<WebItem> iterableItems = projectTypesLinkFactory.getItems(context());
        assertNotNull(iterableItems);
        List<WebItem> list = newArrayList(iterableItems);
        assertThat(list.size(), is(3));
        assertWebItem(list.get(0), BUSINESS, BUSINESS_WEIGHT);
        assertWebItem(list.get(1), SOFTWARE, SOFTWARE_WEIGHT);
        assertWebItem(list.get(2), SERVICE_DESK, SERVICE_DESK_WEIGHT);
    }


    private void assertWebItem(WebItem webItem, String key, int weight) {
        assertThat(webItem.getTitle(), is(key));
        assertThat(webItem.getId(), is(ProjectTypesLinkFactory.ID_PREFIX + key));
        assertThat(webItem.getUrl(), is(expectedUrl(key)));
        assertThat(webItem.getParams().get(ICON_URL), is(ICON_PREFIX + expectedIcon(key)));
        assertThat(webItem.getSection(), is(ProjectTypesLinkFactory.MENU_SECTION));
        assertThat(webItem.getWeight(), is(weight));
    }

    private String expectedUrl(String key) {
        return "http://localhost/jira/secure/BrowseProjects.jspa?selectedCategory=all&selectedProjectType=" + key;
    }

    private String expectedIcon(String key) {
        return key + "_icon";
    }

    private List<ProjectType> projectTypes() {

        final List<ProjectType> projectTypes = newArrayList(projectType(BUSINESS, BUSINESS_WEIGHT),
                projectType(SOFTWARE, SOFTWARE_WEIGHT),
                projectType(SERVICE_DESK, SERVICE_DESK_WEIGHT));
        return projectTypes;
    }

    private ProjectType projectType(String key, int weight) {
        return new ProjectType(new ProjectTypeKey(key), key, expectedIcon(key), "color", weight);
    }

    private Map<String, Object> context() {
        final Map<String, Object> context = new HashMap<>();
        context.put(USER, user);
        return context;
    }

}