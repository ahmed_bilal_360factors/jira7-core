package com.atlassian.jira.web.servlet;

import com.atlassian.jira.avatar.Avatar;
import com.atlassian.jira.avatar.AvatarFormatPolicy;
import com.atlassian.jira.avatar.AvatarManager;
import com.atlassian.jira.io.MediaConsumer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.mock.MockAvatar;
import com.atlassian.jira.mock.servlet.MockServletOutputStream;
import com.atlassian.jira.icon.IconType;
import com.atlassian.jira.plugin.icon.IconTypeDefinitionFactory;
import com.atlassian.jira.plugin.icon.IssueTypeIconTypeDefinition;
import com.atlassian.jira.plugin.icon.IssueTypeIconTypePolicy;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.stubbing.Answer;

import javax.mail.internet.ContentType;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringWriter;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.refEq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.isA;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class ViewUniversalAvatarServletImplTest {
    public static final long AVATAR_ID = 3456l;
    public static final long PNG_AVATAR_ID = 3457l;
    public static final long SVG_AVATAR_ID = 3458l;
    public static final long NON_EXISTING_AVATAR_ID = 1111l;
    public static final long DEFAULT_AVATAR_ID = 3l;
    public static final String SAMPLE_OUTPUT = "sample output";
    public static final String DEFAULT_OUTPUT = "default output";
    public static final Class<? extends AvatarFormatPolicy> ORIGINAL_AVATAR_FORMAT_POLICY = AvatarFormatPolicy.createOriginalDataPolicy().getClass();
    public static final Class<? extends AvatarFormatPolicy> PNG_AVATAR_FORMAT_POLICY = AvatarFormatPolicy.createPngFormatPolicy().withRejectingFallbackStrategy().getClass();
    @Rule
    public final TestRule mockInContainer = MockitoMocksInContainer.forTest(this);

    @Mock
    JiraAuthenticationContext authenticationContext;
    @Mock
    private AvatarManager avatarManager;
    @Mock
    private IconTypeDefinitionFactory iconTypeFactory;

    @InjectMocks
    AvatarToStream avatarToStream;

    @Mock
    HttpServletRequest request;
    @Mock
    HttpServletResponse response;

    @Mock
    private IssueTypeIconTypePolicy issueTypeIconTypePolicy;
    @Mock
    private IconType issueTypeIconType;
    @Mock
    private IssueTypeIconTypeDefinition issueTypeIconTypeDef;

    ApplicationUser admin = new MockApplicationUser("admin");
    MockAvatar mockAvatar = new MockAvatar(AVATAR_ID, "acd.jpg", "image/jpg", Avatar.Type.ISSUETYPE, "23123", true);
    MockAvatar defaultAvatar = new MockAvatar(1, "default.jpg", "image/jpg", Avatar.Type.ISSUETYPE, "1", true);
    MockAvatar pngAvatar = new MockAvatar(1, "png.jpg", "image/png", Avatar.Type.ISSUETYPE, "1", false);
    MockAvatar svgAvatar = new MockAvatar(1, "svg.jpg", "image/svg+xml", Avatar.Type.ISSUETYPE, "1", true);

    ViewUniversalAvatarServletImpl testObj;

    @Before
    public void setUp() throws Exception {
        testObj = new ViewUniversalAvatarServletImpl(authenticationContext, avatarToStream, iconTypeFactory, avatarManager);

        when(issueTypeIconTypeDef.getPolicy()).thenReturn(issueTypeIconTypePolicy);
        when(issueTypeIconTypeDef.getKey()).thenReturn(IconType.ISSUE_TYPE_ICON_TYPE.getKey());

        when(authenticationContext.getUser()).thenReturn(admin);

        when(avatarManager.userCanView(isA(ApplicationUser.class), isA(Avatar.class))).thenReturn(true);

        when(avatarManager.getById(AVATAR_ID)).thenReturn(mockAvatar);
        when(avatarManager.getById(PNG_AVATAR_ID)).thenReturn(pngAvatar);
        when(avatarManager.getById(SVG_AVATAR_ID)).thenReturn(svgAvatar);
        when(avatarManager.getById(DEFAULT_AVATAR_ID)).thenReturn(defaultAvatar);
        when(avatarManager.getById(NON_EXISTING_AVATAR_ID)).thenReturn(null);
        when(avatarManager.getDefaultAvatarId(issueTypeIconType)).thenReturn(DEFAULT_AVATAR_ID);
        //when(issueTypeIconType.getDefaultId()).thenReturn(DEFAULT_AVATAR_ID);

        when(request.getParameter(ViewUniversalAvatarServletImpl.AVATAR_ID_PARAM)).
                thenReturn(String.valueOf(AVATAR_ID));
        when(request.getParameter(ViewUniversalAvatarServletImpl.AVATAR_TYPE_PARAM)).
                thenReturn(IconType.ISSUE_TYPE_ICON_TYPE.getKey());
    }

    @Test
    public void shouldRetrieveAvatarAndSendIt() throws Exception {
        //given
        returnSampleDataForAvatar(mockAvatar, DEFAULT_OUTPUT, new ContentType("image/jpg"));
        new CatchResponseOutput(response);

        // when
        testObj.doGet(request, response);

        // expect
        verify(response).setContentType("image/jpg");
        verify(avatarManager).readAvatarData(refEq(mockAvatar), any(Avatar.Size.class), isA(ORIGINAL_AVATAR_FORMAT_POLICY), any(MediaConsumer.class));
    }

    @Test
    public void shouldUseDefaultSizeWhenItsNotSetInParameter() throws Exception {
        //given
        returnSampleDataForAvatar(mockAvatar, DEFAULT_OUTPUT, new ContentType("image/jpg"));
        new CatchResponseOutput(response);

        // when
        testObj.doGet(request, response);

        // expect
        verify(response).setContentType("image/jpg");
        verify(avatarManager).readAvatarData(any(Avatar.class), refEq(Avatar.Size.defaultSize()), isA(ORIGINAL_AVATAR_FORMAT_POLICY), any(MediaConsumer.class));
    }

    @Test
    public void shouldUsePassedSize() throws Exception {
        // given
        returnSampleDataForAvatar(mockAvatar, DEFAULT_OUTPUT, new ContentType("image/jpg"));
        new CatchResponseOutput(response);

        when(request.getParameter(ViewUniversalAvatarServletImpl.AVATAR_SIZE_PARAM)).
                thenReturn(Avatar.Size.RETINA_XXLARGE.getParam());

        // when
        testObj.doGet(request, response);

        // expect
        verify(response).setContentType("image/jpg");
        verify(avatarManager).readAvatarData(any(Avatar.class), refEq(Avatar.Size.RETINA_XXLARGE), isA(ORIGINAL_AVATAR_FORMAT_POLICY), any(MediaConsumer.class));
    }

    @Test
    public void shouldUsePassedFormatIfAvatarPng() throws Exception {
        // given
        returnSampleDataForAvatar(pngAvatar, DEFAULT_OUTPUT, new ContentType("image/png"));
        new CatchResponseOutput(response);
        when(request.getParameter("format")).
                thenReturn("png");
        when(request.getParameter(ViewUniversalAvatarServletImpl.AVATAR_ID_PARAM)).
                thenReturn(String.valueOf(PNG_AVATAR_ID));

        // when
        testObj.doGet(request, response);

        // expect
        verify(response).setContentType("image/png");
        verify(avatarManager).readAvatarData(refEq(pngAvatar), refEq(Avatar.Size.LARGE), isA(PNG_AVATAR_FORMAT_POLICY), any(MediaConsumer.class));
    }

    @Test
    public void shouldUsePassedFormatIfAvatarSvgConvertible() throws Exception {
        // given
        returnSampleDataForAvatar(svgAvatar, DEFAULT_OUTPUT, new ContentType("image/png"));
        new CatchResponseOutput(response);

        when(request.getParameter("format")).
                thenReturn("png");
        when(request.getParameter(ViewUniversalAvatarServletImpl.AVATAR_ID_PARAM)).
                thenReturn(String.valueOf(SVG_AVATAR_ID));

        // when
        testObj.doGet(request, response);

        // expect
        verify(response).setContentType("image/png");
        verify(avatarManager).readAvatarData(refEq(svgAvatar), refEq(Avatar.Size.LARGE), isA(PNG_AVATAR_FORMAT_POLICY), any(MediaConsumer.class));
    }

    @Test
    public void shouldUseAvatarFormatIfPassedFormatIsNotSupported() throws Exception {
        // given
        returnSampleDataForAvatar(mockAvatar, DEFAULT_OUTPUT, new ContentType("image/jpg"));
        new CatchResponseOutput(response);

        when(request.getParameter("format")).
                thenReturn("png");
        when(request.getParameter(ViewUniversalAvatarServletImpl.AVATAR_ID_PARAM)).
                thenReturn(String.valueOf(AVATAR_ID));

        // when
        testObj.doGet(request, response);

        // expect
        verify(response).setContentType("image/jpg");
        verify(avatarManager).readAvatarData(refEq(mockAvatar), refEq(Avatar.Size.LARGE), isA(PNG_AVATAR_FORMAT_POLICY), any(MediaConsumer.class));
    }

    @Test
    public void shouldPassAvatarDataToResponseStream() throws Exception {
        returnSampleDataForAvatar(mockAvatar, SAMPLE_OUTPUT, new ContentType("image/png"));
        final CatchResponseOutput responseOutput = new CatchResponseOutput(response);

        // when
        testObj.doGet(request, response);

        // then
        String outputResult = responseOutput.getOutput();
        Assert.assertThat(
                outputResult,
                is(equalTo(SAMPLE_OUTPUT)));
    }

    private static class CatchResponseOutput {

        private final StringWriter resultWriter;

        public CatchResponseOutput(final HttpServletResponse mockResponse) throws IOException {
            resultWriter = new StringWriter();
            MockServletOutputStream resultStream = new MockServletOutputStream(resultWriter);
            when(mockResponse.getOutputStream()).thenReturn(resultStream);
        }

        String getOutput() {
            return resultWriter.getBuffer().toString();
        }
    }

    private void returnSampleDataForAvatar(final MockAvatar avatar, final String sampleData, final ContentType contentType) throws IOException {
        // AvatarManger send sample data to client
        final Answer sendSampleData = invocationOnMock -> {
            ByteArrayInputStream sample_data = new ByteArrayInputStream(sampleData.getBytes());
            MediaConsumer mediaConsumer = (MediaConsumer) invocationOnMock.getArguments()[3];
            mediaConsumer.consumeData(sample_data);
            mediaConsumer.consumeContentType(contentType.toString());
            return null;
        };
        doAnswer(sendSampleData).when(avatarManager).readAvatarData(
                refEq(avatar),
                any(Avatar.Size.class),
                any(AvatarFormatPolicy.class),
                any(MediaConsumer.class));
    }

    @Test
    public void shouldFailWithNotFoundWhenNonExistingAvatarIsPassed() throws Exception {
        // given
        returnSampleDataForAvatar(defaultAvatar, DEFAULT_OUTPUT, new ContentType("image/png"));
        when(avatarManager.getDefaultAvatar(IconType.ISSUE_TYPE_ICON_TYPE)).thenReturn(defaultAvatar);
        final CatchResponseOutput responseOutput = new CatchResponseOutput(response);

        HttpServletRequest nonExistingAvatarIdRequest = mock(HttpServletRequest.class);
        when(nonExistingAvatarIdRequest.getParameter(ViewUniversalAvatarServletImpl.AVATAR_ID_PARAM)).
                thenReturn(String.valueOf(NON_EXISTING_AVATAR_ID));
        when(nonExistingAvatarIdRequest.getParameter(ViewUniversalAvatarServletImpl.AVATAR_TYPE_PARAM)).
                thenReturn(String.valueOf(Avatar.Type.ISSUETYPE.getName()));

        // when
        testObj.doGet(nonExistingAvatarIdRequest, response);

        // then
        String outputResult = responseOutput.getOutput();
        Assert.assertThat(
                outputResult,
                is(equalTo(DEFAULT_OUTPUT)));
    }

    @Test
    public void shouldFailWithNotFoundWhenBadAvatarTypePassed() throws Exception {
        // given
        HttpServletRequest badAvatarTypeRequest = mock(HttpServletRequest.class);

        when(badAvatarTypeRequest.getParameter(ViewUniversalAvatarServletImpl.AVATAR_ID_PARAM)).
                thenReturn(String.valueOf(AVATAR_ID));
        when(badAvatarTypeRequest.getParameter(ViewUniversalAvatarServletImpl.AVATAR_TYPE_PARAM)).
                thenReturn("this-type-doesnt-exist");

        // when
        testObj.doGet(badAvatarTypeRequest, response);

        // expect
        verify(response).sendError(eq(HttpServletResponse.SC_NOT_FOUND), any(String.class));
    }

    @Test
    public void shouldFailWithNotFoundWhenBadAvatarIdPassed() throws Exception {
        // given
        HttpServletRequest badAvatarId = mock(HttpServletRequest.class);

        when(badAvatarId.getParameter(ViewUniversalAvatarServletImpl.AVATAR_ID_PARAM)).
                thenReturn("a");
        when(badAvatarId.getParameter(ViewUniversalAvatarServletImpl.AVATAR_TYPE_PARAM)).
                thenReturn("this-type-doesnt-exist");

        // when
        testObj.doGet(badAvatarId, response);

        // expect
        verify(response).sendError(eq(HttpServletResponse.SC_NOT_FOUND), any(String.class));
    }

    @Test
    public void shouldFailWithNotFoundWhenNoParametersPassed() throws Exception {
        // given
        HttpServletRequest badAvatarId = mock(HttpServletRequest.class);

        // when
        testObj.doGet(badAvatarId, response);

        // expect
        verify(response).sendError(eq(HttpServletResponse.SC_NOT_FOUND), any(String.class));
    }
}
