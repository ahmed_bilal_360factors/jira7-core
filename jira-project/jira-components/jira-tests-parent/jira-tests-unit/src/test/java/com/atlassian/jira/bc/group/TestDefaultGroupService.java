package com.atlassian.jira.bc.group;

import com.atlassian.crowd.embedded.api.CrowdService;
import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.search.query.membership.MembershipQuery;
import com.atlassian.jira.application.ApplicationKeys;
import com.atlassian.jira.application.ApplicationRole;
import com.atlassian.jira.application.ApplicationRoleManager;
import com.atlassian.jira.application.MockApplicationRole;
import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.bc.JiraServiceContextImpl;
import com.atlassian.jira.bc.group.GroupService.BulkEditGroupValidationResult;
import com.atlassian.jira.bc.projectroles.ProjectRoleService;
import com.atlassian.jira.exception.AddException;
import com.atlassian.jira.exception.PermissionException;
import com.atlassian.jira.issue.comments.CommentManager;
import com.atlassian.jira.issue.security.IssueSecurityLevelManager;
import com.atlassian.jira.issue.subscription.SubscriptionManager;
import com.atlassian.jira.issue.worklog.WorklogManager;
import com.atlassian.jira.notification.NotificationSchemeManager;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.security.type.GroupDropdown;
import com.atlassian.jira.sharing.SharePermissionDeleteUtils;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.MockGroup;
import com.atlassian.jira.user.util.UserUtil;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.GlobalPermissionGroupAssociationUtil;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.JiraContactHelper;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import org.apache.commons.collections.IteratorUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import static com.atlassian.jira.permission.GlobalPermissionKey.SYSTEM_ADMIN;
import static org.hamcrest.Matchers.contains;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.anyCollection;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


/**
 * Tests {@link DefaultGroupService}.
 *
 * @since v3.12
 */
@RunWith(MockitoJUnitRunner.class)
public class TestDefaultGroupService {
    static final String GROUP_NAME = "test group";
    static final Group GROUP = group(GROUP_NAME);
    static final ApplicationUser ANON_USER = null;

    // if you don't pass a mock user to JiraServiceContext then you'll usually find that eventually someone, somewhere
    // calls get default locale from the Component Manager which brings up the entire world.
    final ApplicationUser mockUser = new MockApplicationUser("test");

    @Mock
    WorklogManager worklogManager;
    @Mock
    GlobalPermissionManager globalPermManager;
    @Mock
    GlobalPermissionGroupAssociationUtil groupUtil;
    @Mock
    CommentManager commentManager;
    @Mock
    NotificationSchemeManager notificationSchemeManager;
    @Mock
    PermissionManager permissionManager;
    @Mock
    ProjectRoleService projectRoleService;
    @Mock
    IssueSecurityLevelManager issueSecurityManager;
    @Mock
    UserUtil userUtil;
    @Mock
    SharePermissionDeleteUtils deleteUtils;
    @Mock
    SubscriptionManager subscriptionManager;
    @Mock
    CrowdService crowdService;
    @Mock
    JiraContactHelper contactHelper;
    @Mock
    GroupManager groupManager;
    @Mock
    ApplicationRoleManager applicationRoleManager;
    @Mock
    I18nHelper i18n;
    @Mock
    GroupsToApplicationsSeatingHelper seatingHelper;

    DefaultGroupService groupService;
    ErrorCollection errorCollection;
    JiraServiceContext serviceContext;

    @Before
    public void setUp() {
        errorCollection = new SimpleErrorCollection();
        serviceContext = new JiraServiceContextImpl(mockUser, errorCollection, i18n);

        groupService = new DefaultGroupService(
                globalPermManager, groupUtil, commentManager, worklogManager,
                notificationSchemeManager, permissionManager, projectRoleService, issueSecurityManager, userUtil,
                deleteUtils, subscriptionManager, crowdService, contactHelper, groupManager, applicationRoleManager, seatingHelper);

        // always return a Group for any String group name
        when(crowdService.getGroup(anyString())).thenAnswer(invocation -> {
            // return a MockGroup for whatever String group name
            return new MockGroup((String) invocation.getArguments()[0]);
        });
    }

    @Test
    public void testGetCommentsAndWorklogsGuardedByGroupCount() {
        when(commentManager.getCountForCommentsRestrictedByGroup(GROUP_NAME)).thenReturn(1L);
        when(worklogManager.getCountForWorklogsRestrictedByGroup(GROUP_NAME)).thenReturn(2L);
        assertEquals(3, groupService.getCommentsAndWorklogsGuardedByGroupCount(GROUP_NAME));

        when(commentManager.getCountForCommentsRestrictedByGroup(GROUP_NAME)).thenReturn(3L);
        when(worklogManager.getCountForWorklogsRestrictedByGroup(GROUP_NAME)).thenReturn(4L);
        assertEquals(7, groupService.getCommentsAndWorklogsGuardedByGroupCount(GROUP_NAME));
    }


    @Test(expected = IllegalArgumentException.class)
    public void testGetCommentsAndWorklogsGuardedByGroupNullGroupName() {
        groupService.getCommentsAndWorklogsGuardedByGroupCount(null);
    }

    @Test
    public void testAreOnlyGroupsGrantingUserAdminPermissionsSysAdminRemovingSelf() {
        when(globalPermManager.hasPermission(Permissions.SYSTEM_ADMIN, mockUser)).thenReturn(true);
        when(groupUtil.isRemovingAllMySysAdminGroups(ImmutableSet.of(GROUP_NAME), mockUser)).thenReturn(true);

        assertTrue(groupService.areOnlyGroupsGrantingUserAdminPermissions(serviceContext, ImmutableSet.of(GROUP_NAME)));
        assertEquals(1, errorCollection.getErrorMessages().size());
        verify(i18n).getText("admin.errors.groups.cannot.delete.users.last.sys.admin.group");
    }

    @Test
    public void testAreOnlyGroupsGrantingUserAdminPermissionsSysAdminNotRemovingSelf() {
        when(globalPermManager.hasPermission(Permissions.SYSTEM_ADMIN, mockUser)).thenReturn(true);
        when(groupUtil.isRemovingAllMySysAdminGroups(ImmutableSet.of(GROUP_NAME), mockUser)).thenReturn(false);

        assertFalse(groupService.areOnlyGroupsGrantingUserAdminPermissions(serviceContext, ImmutableSet.of(GROUP_NAME)));
        assertEquals(0, errorCollection.getErrorMessages().size());
    }

    @Test
    public void testAreOnlyGroupsGrantingUserAdminPermissionsAdminRemovingSelf() {
        when(globalPermManager.hasPermission(Permissions.SYSTEM_ADMIN, mockUser)).thenReturn(false);
        when(groupUtil.isRemovingAllMyAdminGroups(ImmutableSet.of(GROUP_NAME), mockUser)).thenReturn(true);

        assertTrue(groupService.areOnlyGroupsGrantingUserAdminPermissions(serviceContext, ImmutableSet.of(GROUP_NAME)));
        assertEquals(1, errorCollection.getErrorMessages().size());
        verify(i18n).getText("admin.errors.groups.cannot.delete.users.last.admin.group");
    }

    @Test
    public void testAreOnlyGroupsGrantingUserAdminPermissionsAdminNotRemovingSelf() {
        when(globalPermManager.hasPermission(SYSTEM_ADMIN, ANON_USER)).thenReturn(false);
        when(groupUtil.isRemovingAllMySysAdminGroups(ImmutableSet.of(GROUP_NAME), mockUser)).thenReturn(false);

        assertFalse(groupService.areOnlyGroupsGrantingUserAdminPermissions(serviceContext, ImmutableList.of(GROUP_NAME)));
        assertEquals(0, errorCollection.getErrorMessages().size());
    }

    @Test
    public void testIsAdminDeletingSysAdminGroupAdminNotDeletingGroup() {
        when(globalPermManager.hasPermission(SYSTEM_ADMIN, ANON_USER)).thenReturn(false);
        when(globalPermManager.getGroupNamesWithPermission(SYSTEM_ADMIN)).thenReturn(ImmutableSet.of("admin group"));

        assertFalse(groupService.isAdminDeletingSysAdminGroup(serviceContext, GROUP_NAME));
        assertEquals(0, errorCollection.getErrorMessages().size());
    }

    @Test
    public void testIsAdminDeletingSysAdminGroupAdminDeletingGroup() {
        when(globalPermManager.hasPermission(Permissions.SYSTEM_ADMIN, (ApplicationUser) null)).thenReturn(false);
        when(globalPermManager.getGroupNames(Permissions.SYSTEM_ADMIN)).thenReturn(ImmutableSet.of(GROUP_NAME, "admin group"));

        assertTrue(groupService.isAdminDeletingSysAdminGroup(serviceContext, GROUP_NAME));
        assertEquals(1, errorCollection.getErrorMessages().size());
        verify(i18n).getText("admin.errors.groups.error.no.permission.to.remove.group");
    }

    @Test
    public void testIsAdminDeletingSysAdminGroupSysAdminDeletingGroup() {
        when(globalPermManager.hasPermission(Permissions.SYSTEM_ADMIN, (ApplicationUser) null)).thenReturn(true);

        assertFalse(groupService.isAdminDeletingSysAdminGroup(serviceContext, GROUP_NAME));
        assertEquals(0, errorCollection.getErrorMessages().size());
    }

    @Test
    public void testValidateDeleteHappyPath() {
        DefaultGroupService groupService = mockHappyPath();

        assertTrue(groupService.validateDelete(serviceContext, GROUP_NAME, "a swap group"));
    }

    @Test
    public void testValidateDeleteReturnsFalseWhenGroupNameIsNull() {
        DefaultGroupService groupService = spy(this.groupService);
        doReturn(false).when(groupService).isExternalUserManagementEnabled();
        doReturn(true).when(groupService).userHasAdminPermission(any(ApplicationUser.class));

        assertFalse(groupService.validateDelete(serviceContext, null, "SwapGroup"));
        assertEquals(1, errorCollection.getErrorMessages().size());
        verify(i18n).getText("admin.errors.groups.group.does.not.exist", null);
    }

    @Test
    public void testValidateDeleteIsOnlyGroupGrantingUserAdminPermissions() {
        DefaultGroupService groupService = spy(this.groupService);
        doReturn(false).when(groupService).isExternalUserManagementEnabled();
        doReturn(true).when(groupService).userHasAdminPermission(any(ApplicationUser.class));

        doReturn(false).when(groupService).areOnlyGroupsGrantingUserAdminPermissions(serviceContext, ImmutableList.of(GROUP_NAME));
        assertTrue(groupService.validateDelete(serviceContext, GROUP_NAME, "SwapGroup"));

        doReturn(true).when(groupService).areOnlyGroupsGrantingUserAdminPermissions(serviceContext, ImmutableList.of(GROUP_NAME));
        assertFalse(groupService.validateDelete(serviceContext, GROUP_NAME, "SwapGroup"));
    }

    @Test
    public void testValidateDeleteIsAdminDeletingSysAdminGroup() {
        DefaultGroupService groupService = spy(this.groupService);
        doReturn(false).when(groupService).isExternalUserManagementEnabled();
        doReturn(true).when(groupService).userHasAdminPermission(any(ApplicationUser.class));
        doReturn(false).when(groupService).areOnlyGroupsGrantingUserAdminPermissions(serviceContext, ImmutableList.of(GROUP_NAME));

        doReturn(false).when(groupService).isAdminDeletingSysAdminGroup(serviceContext, GROUP_NAME);
        assertTrue(groupService.validateDelete(serviceContext, GROUP_NAME, "SwapGroup"));

        doReturn(true).when(groupService).isAdminDeletingSysAdminGroup(serviceContext, GROUP_NAME);
        assertFalse(groupService.validateDelete(serviceContext, GROUP_NAME, "SwapGroup"));
    }

    @Test
    public void testValidateDeleteCommentsExistAndNoSwapGroup() {
        DefaultGroupService groupService = mockHappyPath();
        assertTrue(groupService.validateDelete(serviceContext, GROUP_NAME, null));

        doReturn(1L).when(groupService).getCommentsAndWorklogsGuardedByGroupCount(GROUP_NAME);
        assertFalse(groupService.validateDelete(serviceContext, GROUP_NAME, null));

        assertEquals(1, errorCollection.getErrorMessages().size());
        verify(i18n).getText("admin.errors.groups.must.specify.group.to.move.comments");
    }

    @Test
    public void testValidateDeleteSwapGroupSameAsDeleteGroup() {
        DefaultGroupService groupService = mockHappyPath();

        assertTrue(groupService.validateDelete(serviceContext, GROUP_NAME, null));
        assertFalse(errorCollection.hasAnyErrors());

        assertFalse(groupService.validateDelete(serviceContext, GROUP_NAME, GROUP_NAME));
        assertEquals(1, errorCollection.getErrorMessages().size());
        verify(i18n).getText("admin.errors.groups.cannot.swap.to.group.deleting");
    }

    @Test
    public void testValidateDeleteFailsWhenDeletingLastDefaultGroupForAnApplicationRole() {
        final String id = "Role";

        DefaultGroupService groupService = spy(this.groupService);
        doReturn(true).when(groupService).userHasAdminPermission(any(ApplicationUser.class));

        ApplicationRole role = mock(ApplicationRole.class);
        when(role.getDefaultGroups()).thenReturn(Collections.singleton(GROUP));
        when(role.getName()).thenReturn(id);
        when(applicationRoleManager.getRoles()).thenReturn(ImmutableSet.of(role));

        assertFalse(groupService.validateDelete(serviceContext, GROUP_NAME, null));
        assertEquals(1, errorCollection.getErrorMessages().size());
        verify(i18n).getText("admin.errors.groups.group.is.default.group.for.application", GROUP_NAME, id);
    }

    @Test
    public void testValidateDeleteWorksWhenDeletingNonLastDefaultGroupFromRole() {
        final String id = "Role";

        DefaultGroupService groupService = mockHappyPath();
        doReturn(true).when(groupService).userHasAdminPermission(any(ApplicationUser.class));

        ApplicationRole role = mock(ApplicationRole.class);
        when(role.getDefaultGroups()).thenReturn(ImmutableSet.of(GROUP, group("otherGroup")));
        when(role.getName()).thenReturn(id);
        when(applicationRoleManager.getRoles()).thenReturn(ImmutableSet.of(role));

        assertTrue(groupService.validateDelete(serviceContext, GROUP_NAME, null));
    }

    @Test
    public void testDeleteHappyPath() throws Exception {
        final MockGroup group = new MockGroup(GROUP_NAME);
        DefaultGroupService groupService = spy(this.groupService);
        doReturn(true).when(groupService).userHasAdminPermission(any(ApplicationUser.class));
        when(permissionManager.hasPermission(anyInt(), any(ApplicationUser.class))).thenReturn(true);

        assertTrue(groupService.delete(serviceContext, GROUP_NAME, "SwapGroup"));

        verify(projectRoleService).removeAllRoleActorsByNameAndType(any(ApplicationUser.class), anyString(), anyString(), any(ErrorCollection.class));
        verify(permissionManager).removeGroupPermissions(GROUP_NAME);
        verify(notificationSchemeManager).removeEntities(GroupDropdown.DESC, GROUP_NAME);
        verify(subscriptionManager).deleteSubscriptionsForGroup(group);
        verify(applicationRoleManager).removeGroupFromRoles(group);
        verify(deleteUtils).deleteGroupPermissions(GROUP_NAME);
        verify(groupService).updateCommentsAndWorklogs(any(ApplicationUser.class), anyString(), anyString());
        verify(groupService).removeGroup(any(Group.class));
        verify(groupService).clearIssueSecurityLevelCache();
    }

    @Test
    public void testDeleteNoAdminPerm() {
        when(permissionManager.hasPermission(anyInt(), any(ApplicationUser.class))).thenReturn(false);

        assertFalse(groupService.delete(serviceContext, GROUP_NAME, "SwapGroup"));
        assertEquals(1, errorCollection.getErrorMessages().size());
        verify(i18n).getText("admin.errors.groups.must.be.admin");
    }

    @Test
    public void testValidateDeleteNoAdminPerm() {
        when(permissionManager.hasPermission(anyInt(), any(ApplicationUser.class))).thenReturn(false);

        assertFalse(groupService.validateDelete(serviceContext, GROUP_NAME, "SwapGroup"));
        assertEquals(1, errorCollection.getErrorMessages().size());
        verify(i18n).getText("admin.errors.groups.must.be.admin");
    }

    @Test
    public void testClearIssueSecurityLevelCacheEnterprise() {
        groupService.clearIssueSecurityLevelCache();
        verify(issueSecurityManager).clearUsersLevels();
    }

    @Test
    public void testValidateGroupNamesExistHappy() {
        assertTrue(groupService.validateGroupNamesExist(ImmutableList.of(GROUP_NAME), errorCollection, i18n));
        assertFalse(errorCollection.hasAnyErrors());
    }

    /**
     * Tests we pass through to the bulk method.
     */
    @Test(expected = RuntimeException.class)
    public void testValidateAddUsersToGroupNullServiceContext() {
        groupService.validateAddUsersToGroup(null, null, null);
    }

    @Test(expected = RuntimeException.class)
    public void testValidateAddUsersToGroupNullGroupsToJoin() {
        groupService.validateAddUsersToGroup(serviceContext, null, null);
    }

    @Test
    public void testValidateAddUsersToGroupNonExistentGroups() {
        DefaultGroupService groupService = spy(this.groupService);
        doReturn(true).when(groupService).userHasAdminPermission(any(ApplicationUser.class));
        doReturn(false).when(groupService).validateGroupNamesExist(ImmutableList.of(GROUP_NAME), errorCollection, i18n);
        assertFalse(groupService.validateAddUsersToGroup(serviceContext, ImmutableList.of(GROUP_NAME), null).isSuccess());
    }

    @Test
    public void testValidateAddUsersToGroupNullUser() {
        final DefaultGroupService groupService = new DefaultGroupService(
                null, null, null, null, null, null, null, null, null, null, null, null, null, null, applicationRoleManager, null) {
            @Override
            boolean validateGroupNamesExist(final Collection groupNames, ErrorCollection errorCollection, final I18nHelper i18n) {
                return true;
            }

            @Override
            boolean isUserNull(final ApplicationUser user) {
                return true;
            }

            @Override
            ApplicationUser getUser(final String userName) {
                return null;
            }

            @Override
            boolean isExternalUserManagementEnabled() {
                return false;
            }

            @Override
            boolean userHasAdminPermission(final ApplicationUser user) {
                return true;
            }
        };

        assertFalse(groupService.validateAddUsersToGroup(
                serviceContext, ImmutableList.of("SomeGroup"), ImmutableList.of("joe user")).isSuccess());
        assertEquals(1, errorCollection.getErrorMessages().size());
        verify(i18n).getText("admin.bulkeditgroups.error.adding.invalid.user", "joe user");
    }

    @Test
    public void testValidateAddUsersToGroupExternalUserManagement() {
        when(contactHelper.getAdministratorContactMessage(i18n)).thenReturn("no soup for you!");

        final DefaultGroupService groupService = new DefaultGroupService(
                null, null, null, null, null, null, null, null, null, null, null, null,
                contactHelper, null, applicationRoleManager, null) {
            @Override
            boolean validateGroupNamesExist(final Collection groupNames, ErrorCollection errorCollection, final I18nHelper i18n) {
                return true;
            }

            @Override
            boolean isUserNull(final ApplicationUser user) {
                return false;
            }

            @Override
            boolean isExternalUserManagementEnabled() {
                return true;
            }

            @Override
            boolean userHasAdminPermission(final ApplicationUser user) {
                return true;
            }
        };

        assertFalse(groupService.validateAddUsersToGroup(serviceContext, ImmutableList.of("SomeGroup"), null).isSuccess());
        assertEquals(1, errorCollection.getErrorMessages().size());
        verify(i18n).getText("admin.bulkeditgroups.error.cannot.edit.user.groups.external.managment", "no soup for you!");
    }

    @Test
    public void testValidateAddUsersToGroupNonVisible() {
        final DefaultGroupService groupService = new DefaultGroupService(
                null, null, null, null, null, null, null, null, null, null, null, null, null, null, applicationRoleManager, null) {

            @Override
            boolean validateGroupNamesExist(final Collection groupNames, ErrorCollection errorCollection, final I18nHelper i18n) {
                return true;
            }

            @Override
            boolean isUserNull(final ApplicationUser user) {
                return false;
            }

            @Override
            boolean isExternalUserManagementEnabled() {
                return false;
            }

            @Override
            List getGroupNamesUserCanSee(final ApplicationUser currentUser) {
                return ImmutableList.of("SomeOtherGroup"); // only available group to join
            }

            @Override
            ApplicationUser getUser(final String userName) {
                return null;
            }

            @Override
            boolean userHasAdminPermission(final ApplicationUser user) {
                return true;
            }
        };
        final SimpleErrorCollection errors = new SimpleErrorCollection();
        JiraServiceContext serviceContext = new JiraServiceContextImpl(mockUser, errors, i18n);
        assertFalse(groupService.validateAddUsersToGroup(
                serviceContext, ImmutableList.of("SomeGroup"), ImmutableList.of("fred")).isSuccess());
        assertEquals(1, errors.getErrorMessages().size());
        verify(i18n).getText("admin.errors.cannot.join.user.groups.not.visible");
    }

    @Test
    public void testValidateAddUsersToGroupFailsWhenAlreadyAMember() {
        final DefaultGroupService groupService = new DefaultGroupService(
                null, null, null, null, null, null, null, null, null, null, null, null, null, null, applicationRoleManager, null) {

            @Override
            boolean validateGroupNamesExist(final Collection groupNames, ErrorCollection errorCollection, final I18nHelper i18n) {
                return true;
            }

            @Override
            boolean isUserNull(final ApplicationUser user) {
                return false;
            }

            @Override
            boolean isExternalUserManagementEnabled() {
                return false;
            }

            @Override
            List getGroupNamesUserCanSee(final ApplicationUser currentUser) {
                return ImmutableList.of("SomeOtherGroup", "SomeGroup");
            }

            @Override
            boolean validateUserIsNotInSelectedGroups(JiraServiceContext serviceContext, final Collection selectedGroupsNames, final ApplicationUser user) {
                return false;
            }

            @Override
            ApplicationUser getUser(final String userName) {
                return null;
            }

            @Override
            List<String> getUserGroups(final ApplicationUser user) {
                return ImmutableList.of("SomeGroup");
            }

            @Override
            boolean userHasAdminPermission(final ApplicationUser user) {
                return true;
            }
        };

        assertFalse(groupService.validateAddUsersToGroup(
                serviceContext, ImmutableList.of("SomeGroup"), ImmutableList.of("foo")).isSuccess());
    }

    @Test
    public void testValidateAddUsersToGroupHappy() {
        final AtomicBoolean validateGroupNamesExistCalled = new AtomicBoolean(false);
        final AtomicBoolean isUserNullCalled = new AtomicBoolean(false);
        final AtomicBoolean isExternalUserManagementEnabledCalled = new AtomicBoolean(false);
        final AtomicBoolean getNonMemberGroupsCalled = new AtomicBoolean(false);
        final AtomicBoolean validateUserIsNotInSelectedGroupsCalled = new AtomicBoolean(false);
        final DefaultGroupService groupService = new DefaultGroupService(
                globalPermManager, null, null, null, null, null, null, null, null, null, null, null, null, null, applicationRoleManager, seatingHelper) {

            @Override
            boolean validateGroupNamesExist(final Collection groupNames, ErrorCollection errorCollection, final I18nHelper i18n) {
                validateGroupNamesExistCalled.set(true);
                return true;
            }

            @Override
            boolean isUserNull(final ApplicationUser user) {
                isUserNullCalled.set(true);
                return false;
            }

            @Override
            boolean isExternalUserManagementEnabled() {
                isExternalUserManagementEnabledCalled.set(true);
                return false;
            }

            @Override
            List getGroupNamesUserCanSee(final ApplicationUser currentUser) {
                getNonMemberGroupsCalled.set(true);
                return ImmutableList.of("SomeOtherGroup", "SomeGroup");
            }

            @Override
            boolean validateUserIsNotInSelectedGroups(JiraServiceContext serviceContext, final Collection selectedGroupsNames, final ApplicationUser user) {
                validateUserIsNotInSelectedGroupsCalled.set(true);
                return true;
            }

            @Override
            ApplicationUser getUser(final String userName) {
                return null;
            }

            @Override
            boolean userHasAdminPermission(final ApplicationUser user) {
                return true;
            }

        };
        when(globalPermManager.getGroupNamesWithPermission(GlobalPermissionKey.USE)).thenReturn(Collections.<String>emptyList());

        final SimpleErrorCollection errors = new SimpleErrorCollection();
        JiraServiceContext serviceContext = new JiraServiceContextImpl(mockUser, errors, i18n);
        assertTrue(groupService.validateAddUsersToGroup(serviceContext, ImmutableList.of("SomeGroup"), ImmutableList.of("dude")).isSuccess());
        assertTrue(validateGroupNamesExistCalled.get());
        assertTrue(isUserNullCalled.get());
        assertTrue(isExternalUserManagementEnabledCalled.get());
        assertTrue(getNonMemberGroupsCalled.get());
        assertTrue(validateUserIsNotInSelectedGroupsCalled.get());
    }

    private DefaultGroupService mockDefaultGroupServiceForValidateAddUsersToGroupWillExceedLicenseLimit() {
        return new DefaultGroupService(
                globalPermManager, null, null, null, null, null, null, null, userUtil,
                null, null, crowdService, null, null, applicationRoleManager, seatingHelper) {
            @Override
            boolean validateGroupNamesExist(final Collection groupNames, ErrorCollection errorCollection, final I18nHelper i18n) {
                return true;
            }

            @Override
            boolean isUserNull(final ApplicationUser user) {
                return false;
            }

            @Override
            boolean isExternalUserManagementEnabled() {
                return false;
            }

            @Override
            List getGroupNamesUserCanSee(final ApplicationUser currentUser) {
                return ImmutableList.of("SomeOtherGroup", "SomeGroup");
            }

            @Override
            boolean validateUserIsNotInSelectedGroups(JiraServiceContext serviceContext, final Collection selectedGroupsNames, final ApplicationUser user) {
                return true;
            }

            @Override
            ApplicationUser getUser(final String userName) {
                return null;
            }

            @Override
            boolean userHasAdminPermission(final ApplicationUser user) {
                return true;
            }

        };
    }

    @Test
    public void testValidateAddUsersToGroupExceedOneApplicationLicenseLimit() {
        final ImmutableList<String> usersWeWantToAddToGroups = prepareUsersForSeatingTest();
        final DefaultGroupService groupService = stubGroupServiceForSeatingTest();

        when(i18n.getText("admin.errors.edit.group.membership.exceeded.application.limit", "jira-software")).thenReturn("single software warning");

        when(applicationRoleManager.hasSeatsAvailable(ApplicationKeys.CORE, 3)).thenReturn(true);
        when(applicationRoleManager.hasSeatsAvailable(ApplicationKeys.SOFTWARE, 2)).thenReturn(false);

        assertFalse(groupService.validateAddUsersToGroup(serviceContext, ImmutableList.of("SomeGroup"), usersWeWantToAddToGroups).isSuccess());
        assertThat(errorCollection.getErrorMessages(), contains("single software warning"));
    }

    @Test
    public void testValidateAddUsersToGroupExceedManyApplicationsLicenseLimit() {
        final ImmutableList<String> usersWeWantToAddToGroups = prepareUsersForSeatingTest();
        final DefaultGroupService groupService = stubGroupServiceForSeatingTest();

        when(i18n.getText("admin.errors.edit.group.membership.exceeded.multiple.applications.limit")).thenReturn("multiple app warning");

        //multiple license broken
        when(applicationRoleManager.hasSeatsAvailable(ApplicationKeys.CORE, 3)).thenReturn(false);
        when(applicationRoleManager.hasSeatsAvailable(ApplicationKeys.SOFTWARE, 2)).thenReturn(false);

        assertFalse(groupService.validateAddUsersToGroup(serviceContext, ImmutableList.of("SomeGroup"), usersWeWantToAddToGroups).isSuccess());
        assertThat(errorCollection.getErrorMessages(), contains("multiple app warning"));
    }

    /**
     * Mocks 3 users (emma, alana, will) and 2 applications (core, software). Also mocks up seating helper to respond for adding those 3 users to "SomeGroup":
     * <ul>
     * <li>Users <b>emma</b> and <b>will</b> will gain access to <b>software</b></li>
     * <li>Users <b>emma</b> and <b>will</b> and <b>alana</b> will gain access to <b>core</b></li>
     * </ul>
     *
     * @return usernames
     */
    private ImmutableList<String> prepareUsersForSeatingTest() {
        ApplicationUser emma = new MockApplicationUser("emma");
        when(userUtil.getUser("emma")).thenReturn(emma);

        ApplicationUser alana = new MockApplicationUser("alana");
        when(userUtil.getUser("alana")).thenReturn(alana);

        ApplicationUser will = new MockApplicationUser("will");
        when(userUtil.getUser("will")).thenReturn(will);

        ApplicationRole softwareRole = new MockApplicationRole(ApplicationKeys.SOFTWARE);
        ApplicationRole coreRole = new MockApplicationRole(ApplicationKeys.CORE);
        when(seatingHelper.rolesToBeAddedForSeatingCountPurpose(ImmutableSet.of(emma, will, alana), ImmutableSet.of("SomeGroup")))
                .thenReturn(ImmutableMap.of(
                        softwareRole, ImmutableSet.of(emma, will),
                        coreRole, ImmutableSet.of(alana, will, emma)
                ));
        return ImmutableList.of("alana", "emma", "will");
    }

    private DefaultGroupService stubGroupServiceForSeatingTest() {
        return new DefaultGroupService(
                null, null, null, null, null, null, null, null, userUtil,
                null, null, null, null, null, applicationRoleManager, seatingHelper) {
            @Override
            boolean validateGroupNamesExist(final Collection groupNames, ErrorCollection errorCollection, final I18nHelper i18n) {
                return true;
            }

            @Override
            boolean isExternalUserManagementEnabled() {
                return false;
            }

            @Override
            List getGroupNamesUserCanSee(final ApplicationUser currentUser) {
                return ImmutableList.of("SomeOtherGroup", "SomeGroup");
            }

            @Override
            boolean validateUserIsNotInSelectedGroups(JiraServiceContext serviceContext, final Collection selectedGroupsNames, final ApplicationUser user) {
                return true;
            }

            @Override
            boolean userHasAdminPermission(final ApplicationUser user) {
                return true;
            }
        };
    }

    @Test
    public void testValidateAddUsersToGroupWillNotExceedLicenseLimitWhenUserAlreadyHasAccess() {
        DefaultGroupService groupService = mockDefaultGroupServiceForValidateAddUsersToGroupWillExceedLicenseLimit();

        final String existingGroupName = "SomeGroup";
        final MockGroup groupThatExist = new MockGroup(existingGroupName);

        //When searching for users group return existing group - User has group
        when(crowdService.search(anyObject())).thenReturn(ImmutableList.of(groupThatExist));
        //Since user has access the count is zero
        when(userUtil.canActivateNumberOfUsers(eq(0))).thenReturn(true);

        when(crowdService.getGroup(existingGroupName)).thenReturn(groupThatExist);
        when(crowdService.getGroup("fakeGroup")).thenReturn(null);
        when(globalPermManager.getGroupNamesWithPermission(GlobalPermissionKey.USE)).thenReturn(Collections.singletonList(existingGroupName));

        final BulkEditGroupValidationResult bulkEditGroupValidationResult = groupService.validateAddUsersToGroup(serviceContext,
                ImmutableList.of(existingGroupName), ImmutableList.of("userName"));

        assertTrue(bulkEditGroupValidationResult.isSuccess());
    }

    private void mockAddUserToGroup(final String userWithAccess, final String groupWithAccess) {
        final MembershipQuery<Group> membershipQueryForUserWithAccess =
                QueryBuilder.queryFor(Group.class, EntityDescriptor.group()).parentsOf(EntityDescriptor.user()).withName(userWithAccess).returningAtMost(EntityQuery.ALL_RESULTS);
        when(crowdService.search(membershipQueryForUserWithAccess)).thenReturn(ImmutableSet.of(new MockGroup(groupWithAccess)));
    }

    @Test
    public void testGetGroupNamesUserCanSee() {
        when(groupUtil.getGroupNamesModifiableByCurrentUser(any(ApplicationUser.class), anyCollection()))
                .thenReturn(ImmutableList.of("FooGroup", "BarGroup"));

        final DefaultGroupService groupService = new DefaultGroupService(
                null, groupUtil, null, null, null, null, null, null, null,
                null, null, null, null, null, applicationRoleManager, null) {
            List<String> getAllGroupNames() {
                return null;
            }

        };
        final List<String> list = groupService.getGroupNamesUserCanSee(null);
        assertNotNull(list);
        assertEquals(2, list.size());
        assertTrue(list.contains("BarGroup"));
        assertTrue(list.contains("FooGroup"));
    }

    @Test
    public void testValidateUserIsNotInSelectedGroupsAlreadyInOnlyOne() {
        final DefaultGroupService groupService = new DefaultGroupService(
                null, null, null, null, null, null, null, null, null, null, null, null, null, null, applicationRoleManager, null) {
            List<String> getUserGroups(final ApplicationUser user) {
                return ImmutableList.of("AnyGroup");
            }
        };

        final ApplicationUser user = new MockApplicationUser("admin");
        assertFalse(groupService.validateUserIsNotInSelectedGroups(serviceContext, ImmutableList.of("AnyGroup"), user));
        assertTrue(errorCollection.hasAnyErrors());
        assertEquals(1, errorCollection.getErrorMessages().size());
        verify(i18n).getText("admin.bulkeditgroups.error.user.already.member.of.group", "admin", "AnyGroup");
    }

    @Test
    public void testValidateUserIsNotInSelectedGroupsAlreadyInAll() {
        final DefaultGroupService groupService = new DefaultGroupService(
                null, null, null, null, null, null, null, null, null, null, null, null, null, null, applicationRoleManager, null) {
            List<String> getUserGroups(final ApplicationUser user) {
                return ImmutableList.of("AnyGroup", GROUP_NAME);
            }
        };

        final ApplicationUser user = new MockApplicationUser("admin");
        assertFalse(groupService.validateUserIsNotInSelectedGroups(
                serviceContext, ImmutableList.of("AnyGroup", GROUP_NAME), user));

        assertTrue(errorCollection.hasAnyErrors());
        assertEquals(1, errorCollection.getErrorMessages().size());
        verify(i18n).getText("admin.bulkeditgroups.error.user.already.member.of.all", "admin");
    }

    @Test
    public void testValidateUserIsNotInSelectedGroupsHappy() {
        final DefaultGroupService groupService = new DefaultGroupService(
                null, null, null, null, null, null, null, null, null, null, null, null, null, null, applicationRoleManager, null) {
            List<String> getUserGroups(final ApplicationUser user) {
                return ImmutableList.of(GROUP_NAME);
            }
        };

        JiraServiceContext serviceContext = new JiraServiceContextImpl(mockUser, errorCollection, i18n);
        assertTrue(groupService.validateUserIsNotInSelectedGroups(serviceContext, ImmutableList.of("AnyGroup"), null));
        assertFalse(errorCollection.hasAnyErrors());
    }

    @Test
    public void testIsUserInGroupsInGroups() {
        final DefaultGroupService groupService = new DefaultGroupService(
                null, null, null, null, null, null, null, null, null, null, null, null, null, null, applicationRoleManager, null) {
            List<String> getUserGroups(final ApplicationUser user) {
                return ImmutableList.of(GROUP_NAME);
            }
        };
        assertTrue(groupService.isUserInGroups(null, Collections.singleton(GROUP_NAME)));
    }

    @Test
    public void testIsUserInGroupsNotInGroups() {
        final DefaultGroupService groupService = new DefaultGroupService(
                null, null, null, null, null, null, null, null, null, null, null, null, null, null, applicationRoleManager, null) {
            @Override
            List<String> getUserGroups(final ApplicationUser user) {
                return Collections.singletonList(GROUP_NAME);
            }
        };
        assertFalse(groupService.isUserInGroups(null, Collections.singleton("OtherGroup")));
    }

    @Test
    public void testValidateCanRemoveUserFromGroupsGroupNamesDontExist() {
        final DefaultGroupService groupService = new DefaultGroupService(
                null, null, null, null, null, null, null, null, null, null, null, null, null, null, applicationRoleManager, null) {
            @Override
            boolean validateGroupNamesExist(final Collection groupNames, ErrorCollection errorCollection, final I18nHelper i18n) {
                return false;
            }
        };

        assertFalse(groupService.validateCanRemoveUserFromGroups(serviceContext, null, null, null, true));
    }

    @Test
    public void testValidateCanRemoveUserFromGroupsIsAllGroupNotSelected() {
        final DefaultGroupService groupService = new DefaultGroupService(
                null, null, null, null, null, null, null, null, null, null, null, null, null, null, applicationRoleManager, null) {
            @Override
            boolean validateGroupNamesExist(final Collection groupNames, ErrorCollection errorCollection, final I18nHelper i18n) {
                return true;
            }
        };

        final ApplicationUser user = new MockApplicationUser("admin");
        assertFalse(groupService.validateCanRemoveUserFromGroups(serviceContext, user, ImmutableList.of("Group1"), ImmutableList.of("Group2"),
                false));
        assertTrue(errorCollection.hasAnyErrors());
        assertEquals(1, errorCollection.getErrorMessages().size());
        verify(i18n).getText("admin.bulkeditgroups.error.user.group.not.selected", "admin", "Group2");
    }

    @Test
    public void testValidateCanRemoveUserFromGroupsCurrentUserRemovingAdmin() {
        when(groupUtil.getSysAdminMemberGroups(any(ApplicationUser.class))).thenReturn(null);
        when(groupUtil.getAdminMemberGroups(any(ApplicationUser.class))).thenReturn(null);

        final DefaultGroupService groupService = new DefaultGroupService(
                null, groupUtil, null, null, null, null, null, null,
                null, null, null, null, null, groupManager, applicationRoleManager, null) {
            @Override
            boolean validateGroupNamesExist(Collection groupNames, ErrorCollection errorCollection, I18nHelper i18n) {
                return true;
            }

            @Override
            boolean areOnlyGroupsGrantingUserAdminPermissions(
                    JiraServiceContext serviceContext, Collection groupNames, String sysAdminErrorMessage,
                    Object sysAdminErrorParameters, String adminErrorMessage, Object adminErrorParameters) {
                return true;
            }
        };

        final ApplicationUser user = new MockApplicationUser("admin");

        assertFalse(groupService.validateCanRemoveUserFromGroups(
                serviceContext, user, ImmutableList.of("Group1"), ImmutableList.of("Group1"), true));
    }

    @Test
    public void testValidateCanRemoveUserFromGroupsUserCantSeeGroup() {

        final DefaultGroupService groupService = new DefaultGroupService(
                null, null, null, null, null, null, null, null, null, null, null, null, null, null, applicationRoleManager, null) {
            @Override
            boolean validateGroupNamesExist(final Collection groupNames, ErrorCollection errorCollection, final I18nHelper i18n) {
                return true;
            }

            @Override
            boolean areOnlyGroupsGrantingUserAdminPermissions(JiraServiceContext serviceContext, final Collection groupNames, final String sysAdminErrorMessage, final Object sysAdminErrorParameters, final String adminErrorMessage, final Object adminErrorParameters) {
                return false;
            }

            @Override
            List getGroupNamesUserCanSee(final ApplicationUser currentUser) {
                return Collections.emptyList();
            }
        };

        assertFalse(groupService.validateCanRemoveUserFromGroups(
                serviceContext, null, ImmutableList.of("Group1"), ImmutableList.of("Group1"), true));

        assertTrue(errorCollection.hasAnyErrors());
        assertEquals(1, errorCollection.getErrorMessages().size());
        verify(i18n).getText("admin.errors.cannot.leave.user.groups.not.visible");
    }

    @Test
    public void testValidateCanRemoveUserFromGroupsUserNotInGroupNotAll() {
        final ApplicationUser user = new MockApplicationUser("admin");

        final DefaultGroupService groupService = new DefaultGroupService(
                null, null, null, null, null, null, null, null, null, null, null, null, null, null, applicationRoleManager, null) {
            @Override
            boolean validateGroupNamesExist(final Collection groupNames, ErrorCollection errorCollection, final I18nHelper i18n) {
                return true;
            }

            @Override
            boolean areOnlyGroupsGrantingUserAdminPermissions(
                    JiraServiceContext serviceContext, final Collection<String> groupNames,
                    final String sysAdminErrorMessage, final Object sysAdminErrorParameters,
                    final String adminErrorMessage, final Object adminErrorParameters) {
                return false;
            }

            @Override
            List getGroupNamesUserCanSee(final ApplicationUser currentUser) {
                return ImmutableList.of("Group1");
            }

            @Override
            boolean isUserInGroups(final ApplicationUser user, final Set<String> groupNames) {
                return false;
            }
        };

        assertFalse(groupService.validateCanRemoveUserFromGroups(
                serviceContext, user, ImmutableList.of("Group1"), ImmutableList.of("Group1"), false));

        assertTrue(errorCollection.hasAnyErrors());
        assertEquals(1, errorCollection.getErrorMessages().size());
        verify(i18n).getText("admin.bulkeditgroups.error.user.not.member.of.group", "admin", "Group1");
    }

    @Test
    public void testValidateCanRemoveUserFromGroupsUserNotInGroupAllOneGroup() {
        final ApplicationUser user = new MockApplicationUser("admin");

        final DefaultGroupService groupService = new DefaultGroupService(
                null, null, null, null, null, null, null, null, null, null, null, null, null, null, applicationRoleManager, null) {
            @Override
            boolean validateGroupNamesExist(final Collection groupNames, ErrorCollection errorCollection, final I18nHelper i18n) {
                return true;
            }

            @Override
            boolean areOnlyGroupsGrantingUserAdminPermissions(JiraServiceContext serviceContext, final Collection groupNames, final String sysAdminErrorMessage, final Object sysAdminErrorParameters, final String adminErrorMessage, final Object adminErrorParameters) {
                return false;
            }

            @Override
            List getGroupNamesUserCanSee(final ApplicationUser currentUser) {
                return ImmutableList.of("Group1");
            }

            @Override
            boolean isUserInGroups(final ApplicationUser user, final Set /*<String>*/groupNames) {
                return false;
            }
        };

        assertFalse(groupService.validateCanRemoveUserFromGroups(
                serviceContext, user, ImmutableList.of("Group1"), ImmutableList.of("Group1"), true));

        assertTrue(errorCollection.hasAnyErrors());
        assertEquals(1, errorCollection.getErrorMessages().size());
        verify(i18n).getText("admin.bulkeditgroups.error.user.not.member.of.group", "admin", "Group1");
    }

    @Test
    public void testValidateCanRemoveUserFromGroupsUserNotInGroupAllNotInAll() {
        final ApplicationUser user = new MockApplicationUser("admin");

        final DefaultGroupService groupService = new DefaultGroupService(
                null, null, null, null, null, null, null, null, null, null, null, null, null, null, applicationRoleManager, null) {
            @Override
            boolean validateGroupNamesExist(final Collection groupNames, ErrorCollection errorCollection, final I18nHelper i18n) {
                return true;
            }

            @Override
            boolean areOnlyGroupsGrantingUserAdminPermissions(JiraServiceContext serviceContext, final Collection groupNames, final String sysAdminErrorMessage, final Object sysAdminErrorParameters, final String adminErrorMessage, final Object adminErrorParameters) {
                return false;
            }

            @Override
            List getGroupNamesUserCanSee(final ApplicationUser currentUser) {
                return ImmutableList.of("Group1", "Group2");
            }

            @Override
            boolean isUserInGroups(final ApplicationUser user, final Set /*<String>*/groupNames) {
                return false;
            }
        };

        assertFalse(groupService.validateCanRemoveUserFromGroups(
                serviceContext, user, ImmutableList.of("Group1"), ImmutableList.of("Group1", "Group2"), true));

        assertTrue(errorCollection.hasAnyErrors());
        assertEquals(1, errorCollection.getErrorMessages().size());
        verify(i18n).getText("admin.bulkeditgroups.error.user.not.member.of.all", "admin");
    }

    @Test
    public void testValidateCanRemoveUserFromGroupsHappyPath() {
        final ApplicationUser user = new MockApplicationUser("admin");

        final DefaultGroupService groupService = new DefaultGroupService(
                null, null, null, null, null, null, null, null, null, null, null, null, null, null, applicationRoleManager, null) {
            @Override
            boolean validateGroupNamesExist(final Collection groupNames, ErrorCollection errorCollection, final I18nHelper i18n) {
                return true;
            }

            @Override
            boolean areOnlyGroupsGrantingUserAdminPermissions(
                    JiraServiceContext serviceContext, final Collection<String> groupNames,
                    final String sysAdminErrorMessage, final Object sysAdminErrorParameters,
                    final String adminErrorMessage, final Object adminErrorParameters) {
                return false;
            }

            @Override
            List getGroupNamesUserCanSee(final ApplicationUser currentUser) {
                return ImmutableList.of("Group1", "Group2");
            }

            @Override
            boolean isUserInGroups(final ApplicationUser user, final Set<String> groupNames) {
                return true;
            }
        };

        assertTrue(groupService.validateCanRemoveUserFromGroups(
                serviceContext, user, ImmutableList.of("Group1"), ImmutableList.of("Group1", "Group2"), true));
    }

    @Test
    public void testValidateRemoveUsersFromGroupsNullMapper() {
        final DefaultGroupService groupService = new DefaultGroupService(
                null, null, null, null, null, null, null, null, null, null, null, null, null, null, applicationRoleManager, null);

        try {
            groupService.validateRemoveUsersFromGroups(serviceContext, null);
            fail("We should see an exception when a null mapper is passed in.");
        } catch (final Exception e) {
            // this should happen
            assertEquals("You must specify a non null mapper.", e.getMessage());
        }
    }

    @Test
    public void testValidateRemoveUsersFromGroupsExternalUserManagementEnabled() {
        when(contactHelper.getAdministratorContactMessage(any(I18nHelper.class))).thenReturn("no soup for you!");

        final DefaultGroupService groupService = new DefaultGroupService(
                null, null, null, null, null, null, null, null, null, null, null, null,
                contactHelper, null, applicationRoleManager, null) {
            @Override
            boolean isExternalUserManagementEnabled() {
                return true;
            }

            @Override
            boolean userHasAdminPermission(final ApplicationUser user) {
                return true;
            }
        };

        assertFalse(groupService.validateRemoveUsersFromGroups(serviceContext, new GroupRemoveChildMapper()));
        assertTrue(errorCollection.hasAnyErrors());
        assertEquals(1, errorCollection.getErrorMessages().size());
        verify(i18n).getText("admin.bulkeditgroups.error.cannot.edit.user.groups.external.managment", "no soup for you!");
    }

    @Test
    public void testValidateRemoveUsersFromGroupsUserIsNull() {
        final DefaultGroupService groupService = new DefaultGroupService(
                null, null, null, null, null, null, null, null, null, null, null, null, null, null, applicationRoleManager, null) {
            @Override
            boolean isExternalUserManagementEnabled() {
                return false;
            }

            @Override
            ApplicationUser getUser(final String userName) {
                return null;
            }

            @Override
            boolean isUserNull(final ApplicationUser user) {
                return true;
            }

            @Override
            boolean userHasAdminPermission(final ApplicationUser user) {
                return true;
            }
        };

        final GroupRemoveChildMapper mapper = new GroupRemoveChildMapper().register(null, "Group1");
        assertFalse(groupService.validateRemoveUsersFromGroups(serviceContext, mapper));
        assertTrue(errorCollection.hasAnyErrors());
        assertEquals(1, errorCollection.getErrorMessages().size());
        verify(i18n).getText("admin.bulkeditgroups.error.removing.invalid.user", null);
    }

    @Test
    public void testValidateRemoveUsersFromGroupsRemoveFromAll() {
        final AtomicInteger validateCanRemoveUserCount = new AtomicInteger(0);
        final DefaultGroupService groupService = new DefaultGroupService(
                null, null, null, null, null, null, null, null, null, null, null, null, null, null, applicationRoleManager, null) {
            @Override
            boolean isExternalUserManagementEnabled() {
                return false;
            }

            @Override
            ApplicationUser getUser(final String userName) {
                return null;
            }

            @Override
            boolean isUserNull(final ApplicationUser user) {
                return false;
            }

            @Override
            boolean validateCanRemoveUserFromGroups(
                    JiraServiceContext serviceContext, final ApplicationUser userToRemove,
                    final List allSelectedGroups, final List groupsToLeave, final boolean isAll) {
                validateCanRemoveUserCount.incrementAndGet();
                return false;
            }

            @Override
            boolean userHasAdminPermission(final ApplicationUser user) {
                return true;
            }
        };

        final GroupRemoveChildMapper mapper = new GroupRemoveChildMapper(ImmutableList.of("Group1"));
        mapper.register(null);
        assertFalse(groupService.validateRemoveUsersFromGroups(serviceContext, mapper));
        assertEquals(1, validateCanRemoveUserCount.get());
    }

    @Test
    public void testValidateRemoveUsersFromGroupsRemoveFromGroups() {
        final AtomicInteger validateCanRemoveUserCount = new AtomicInteger(0);
        final DefaultGroupService groupService = new DefaultGroupService(
                null, null, null, null, null, null, null, null, null, null, null, null, null, null, applicationRoleManager, null) {
            @Override
            boolean isExternalUserManagementEnabled() {
                return false;
            }

            @Override
            ApplicationUser getUser(final String userName) {
                return null;
            }

            @Override
            boolean isUserNull(final ApplicationUser user) {
                return false;
            }

            @Override
            boolean validateCanRemoveUserFromGroups(
                    JiraServiceContext serviceContext, final ApplicationUser userToRemove, final List allSelectedGroups,
                    final List groupsToLeave, final boolean isAll) {
                validateCanRemoveUserCount.incrementAndGet();
                return false;
            }

            @Override
            boolean userHasAdminPermission(final ApplicationUser user) {
                return true;
            }
        };

        final GroupRemoveChildMapper mapper = new GroupRemoveChildMapper(ImmutableList.of("Group1"));
        mapper.register(null, ImmutableList.of("Group1", "Group2"));
        assertFalse(groupService.validateRemoveUsersFromGroups(serviceContext, mapper));
        assertEquals(2, validateCanRemoveUserCount.get());
    }

    @Test
    public void testValidateRemoveUsersFromGroupsHappyPath() {
        final DefaultGroupService groupService = new DefaultGroupService(
                null, null, null, null, null, null, null, null, null, null, null, null, null, null, applicationRoleManager, null) {
            @Override
            boolean isExternalUserManagementEnabled() {
                return false;
            }

            @Override
            ApplicationUser getUser(final String userName) {
                return null;
            }

            @Override
            boolean isUserNull(final ApplicationUser user) {
                return false;
            }

            @Override
            boolean validateCanRemoveUserFromGroups(
                    JiraServiceContext serviceContext, final ApplicationUser userToRemove, final List allSelectedGroups,
                    final List groupsToLeave, final boolean isAll) {
                return true;
            }

            @Override
            boolean userHasAdminPermission(final ApplicationUser user) {
                return true;
            }
        };

        final GroupRemoveChildMapper mapper = new GroupRemoveChildMapper(ImmutableList.of("Group1"));
        mapper.register(null, ImmutableList.of("Group1", "Group2"));
        assertTrue(groupService.validateRemoveUsersFromGroups(serviceContext, mapper));
    }

    @Test
    public void testValidateRemoveUserFromGroupsNullGroupsToLeave() {
        final DefaultGroupService groupService = new DefaultGroupService(
                null, null, null, null, null, null, null, null, null, null, null, null, null, null, applicationRoleManager, null);

        try {
            groupService.validateRemoveUserFromGroups(serviceContext, null, null);
            fail();
        } catch (final Exception e) {
            assertEquals("You must specify a non null groupsToLeave.", e.getMessage());
        }
    }

    @Test
    public void testValidateRemoveUserFromGroupsHappyPath() {
        final DefaultGroupService groupService = new DefaultGroupService(
                null, null, null, null, null, null, null, null, null, null, null, null, null, null, applicationRoleManager, null) {
            @Override
            public boolean validateRemoveUsersFromGroups(
                    JiraServiceContext serviceContext, final GroupRemoveChildMapper mapper) {
                assertNotNull(mapper);
                final List groupsForUser = IteratorUtils.toList(mapper.getGroupsIterator("admin"));
                assertNotNull(groupsForUser);
                assertEquals(1, groupsForUser.size());
                assertEquals("Group1", groupsForUser.iterator().next());
                assertFalse(mapper.isRemoveFromAllSelected("admin"));
                return true;
            }
        };

        assertTrue(groupService.validateRemoveUserFromGroups(serviceContext, ImmutableList.of("Group1"), "admin"));
    }

    @Test
    public void testAddUsersToGroupsHappy() throws AddException, PermissionException {
        final DefaultGroupService groupService = new DefaultGroupService(
                null, null, null, null, null, null, null, null, userUtil,
                null, null, null, null, null, applicationRoleManager, null) {
            @Override
            List getGroupNamesUserCanSee(final ApplicationUser currentUser) {
                return ImmutableList.of("group1");
            }

            @Override
            ApplicationUser getUser(final String userName) {
                return null;
            }

            @Override
            Collection convertGroupNamesToGroups(final Collection<String> groupNames) {
                return Collections.singletonList(new MockGroup("group1"));
            }

            @Override
            boolean userHasAdminPermission(final ApplicationUser user) {
                return true;
            }
        };

        assertTrue(groupService.addUsersToGroups(serviceContext, ImmutableList.of("group1"), ImmutableList.of("user1")));
    }

    @Test
    public void testAddUsersToGroupsNullArgs() {
        final DefaultGroupService groupService = new DefaultGroupService(
                null, null, null, null, null, null, null, null, null, null, null, null, null, null, applicationRoleManager, null);

        try {
            groupService.addUsersToGroups(null, Collections.emptyList(), Collections.emptyList());
            fail("expected exception");
        } catch (final RuntimeException expected) {
        }

        try {
            final SimpleErrorCollection errors = new SimpleErrorCollection();
            JiraServiceContext serviceContext = new JiraServiceContextImpl(mockUser, errors, i18n);
            groupService.addUsersToGroups(serviceContext, null, Collections.emptyList());
            fail("expected exception");
        } catch (final RuntimeException expected) {
        }

        try {
            final SimpleErrorCollection errors = new SimpleErrorCollection();
            JiraServiceContext serviceContext = new JiraServiceContextImpl(mockUser, errors, i18n);
            groupService.addUsersToGroups(serviceContext, Collections.emptyList(), null);
            fail("expected exception");
        } catch (final RuntimeException expected) {
        }
    }

    @Test
    public void testAddUsersToGroupsNoPermission() {
        final DefaultGroupService groupService = new DefaultGroupService(
                null, null, null, null, null, null, null, null, null, null, null, null, null, null, applicationRoleManager, null) {
            @Override
            List getGroupNamesUserCanSee(final ApplicationUser currentUser) {
                return ImmutableList.of("group2");
            }

            @Override
            ApplicationUser getUser(final String userName) {
                return null;
            }

            @Override
            boolean userHasAdminPermission(final ApplicationUser user) {
                return true;
            }
        };

        assertFalse(groupService.addUsersToGroups(serviceContext, ImmutableList.of("group1"), ImmutableList.of("user1")));
        assertEquals(1, errorCollection.getErrorMessages().size());
        verify(i18n).getText("admin.errors.cannot.join.user.groups.not.visible");
    }

    @Test
    public void testAddUsersToGroupNotAdmin() {
        final DefaultGroupService groupService = new DefaultGroupService(
                null, null, null, null, null, null, null, null, null, null, null, null, null, null, applicationRoleManager, null) {
            @Override
            ApplicationUser getUser(final String userName) {
                return null;
            }

            @Override
            boolean userHasAdminPermission(final ApplicationUser user) {
                return false;
            }
        };

        assertFalse(groupService.addUsersToGroups(serviceContext, ImmutableList.of("group1"), ImmutableList.of("user1")));
        assertEquals(1, errorCollection.getErrorMessages().size());
        verify(i18n).getText("admin.errors.groups.must.be.admin");
    }

    @Test
    public void testRemoveUsersFromGroupNotAdmin() {
        final DefaultGroupService groupService = new DefaultGroupService(
                null, null, null, null, null, null, null, null, null, null, null, null, null, null, applicationRoleManager, null) {
            @Override
            ApplicationUser getUser(final String userName) {
                return null;
            }

            @Override
            boolean userHasAdminPermission(final ApplicationUser user) {
                return false;
            }
        };

        assertFalse(groupService.removeUsersFromGroups(serviceContext,
                new GroupRemoveChildMapper(ImmutableList.of("group1")).register("user1")));
        assertEquals(1, errorCollection.getErrorMessages().size());
        verify(i18n).getText("admin.errors.groups.must.be.admin");
    }

    @Test
    public void testValidateAddUsersToGroupNotAdmin() {
        final DefaultGroupService groupService = new DefaultGroupService(
                null, null, null, null, null, null, null, null, null, null, null, null, null, null, applicationRoleManager, null) {
            @Override
            ApplicationUser getUser(final String userName) {
                return null;
            }

            @Override
            boolean userHasAdminPermission(final ApplicationUser user) {
                return false;
            }
        };

        assertFalse(groupService.validateAddUsersToGroup(serviceContext, ImmutableList.of("group1"), ImmutableList.of("user1")).isSuccess());
        assertEquals(1, errorCollection.getErrorMessages().size());
        verify(i18n).getText("admin.errors.groups.must.be.admin");
    }

    @Test
    public void testValidateRemoveUsersFromGroupNotAdmin() {
        final DefaultGroupService groupService = new DefaultGroupService(
                null, null, null, null, null, null, null, null, null, null, null, null, null, null, applicationRoleManager, null) {
            @Override
            ApplicationUser getUser(final String userName) {
                return null;
            }

            @Override
            boolean userHasAdminPermission(final ApplicationUser user) {
                return false;
            }
        };

        assertFalse(groupService.validateRemoveUsersFromGroups(serviceContext,
                new GroupRemoveChildMapper(ImmutableList.of("group1")).register("user1")));
        assertEquals(1, errorCollection.getErrorMessages().size());
        verify(i18n).getText("admin.errors.groups.must.be.admin");
    }

    @Test
    public void testRemoveUsersFromGroupsHappy() {
        final DefaultGroupService groupService = new DefaultGroupService(
                null, null, null, null, null, null, null, null, userUtil,
                null, null, null, null, null, applicationRoleManager, null) {
            @Override
            List getGroupNamesUserCanSee(final ApplicationUser currentUser) {
                return ImmutableList.of("group1");
            }

            @Override
            ApplicationUser getUser(final String userName) {
                return null;
            }

            @Override
            Collection<Group> convertGroupNamesToGroups(final Collection groupNames) {
                return Collections.<Group>singletonList(new MockGroup("group1"));
            }

            @Override
            boolean userHasAdminPermission(final ApplicationUser user) {
                return true;
            }
        };

        final GroupRemoveChildMapper groupRemoveChildMapper = new GroupRemoveChildMapper();
        groupRemoveChildMapper.register("User1", "group1");
        assertTrue(groupService.removeUsersFromGroups(serviceContext, groupRemoveChildMapper));
    }

    @Test
    public void testRemoveUsersFromGroupsNullArgs() {
        final DefaultGroupService groupService = new DefaultGroupService(
                null, null, null, null, null, null, null, null, null, null, null, null, null, null, applicationRoleManager, null);

        try {
            groupService.removeUsersFromGroups(null, new GroupRemoveChildMapper());
            fail("expected exception");
        } catch (final RuntimeException expected) {
        }

        try {
            groupService.removeUsersFromGroups(serviceContext, null);
            fail("expected exception");
        } catch (final RuntimeException expected) {
        }
    }

    @Test
    public void testRemoveUsersFromGroupsNoPermission() {
        final DefaultGroupService groupService = new DefaultGroupService(
                null, null, null, null, null, null, null, null, null, null, null, null, null, null, applicationRoleManager, null) {
            @Override
            List getGroupNamesUserCanSee(final ApplicationUser currentUser) {
                return ImmutableList.of("group2");
            }

            @Override
            ApplicationUser getUser(final String userName) {
                return null;
            }

            @Override
            boolean userHasAdminPermission(final ApplicationUser user) {
                return true;
            }
        };

        final GroupRemoveChildMapper groupRemoveChildMapper = new GroupRemoveChildMapper();
        groupRemoveChildMapper.register("user1", "group1");

        assertFalse(groupService.removeUsersFromGroups(serviceContext, groupRemoveChildMapper));
        assertEquals(1, errorCollection.getErrorMessages().size());
        verify(i18n).getText("admin.errors.cannot.leave.user.groups.not.visible");
    }

    private DefaultGroupService mockHappyPath() {
        DefaultGroupService groupService = spy(this.groupService);
        doReturn(false).when(groupService).isExternalUserManagementEnabled();
        doReturn(true).when(groupService).userHasAdminPermission(any(ApplicationUser.class));
        doReturn(false).when(groupService).areOnlyGroupsGrantingUserAdminPermissions(serviceContext, ImmutableList.of(GROUP_NAME));
        doReturn(false).when(groupService).isAdminDeletingSysAdminGroup(serviceContext, GROUP_NAME);
        doReturn(0L).when(groupService).getCommentsAndWorklogsGuardedByGroupCount(GROUP_NAME);

        return groupService;
    }

    private static Group group(String name) {
        return new MockGroup(name);
    }
}
