package com.atlassian.jira.cluster.monitoring;

import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.upgrade.util.TestLogAppender;
import javax.management.MBeanRegistrationException;
import javax.management.MBeanServer;
import javax.management.ObjectName;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestClusterMonitoringBeansRegistrar {
    private static final String BEAN_NAME = "com.atlassian.jira.cluster.monitoring:type=ClusterNodeStatus";
    @Mock
    @AvailableInContainer
    private ClusterNodeStatus clusterNodeStatus;
    private MBeanServer mBeanServer;
    private ClusterMonitoringBeansRegistrar clusterMonitoringBeansRegistrar;
    private TestLogAppender log = new TestLogAppender();

    @Before
    public void setUp() {
        mBeanServer = spy(new FakeMBeanServer());
        clusterMonitoringBeansRegistrar = new ClusterMonitoringBeansRegistrar(mBeanServer);
        Logger.getRootLogger().addAppender(log);
    }

    @After
    public void tearDown() {
        Logger.getRootLogger().removeAppender(log);
    }

    @Test
    public void beansAreNotRegisteredTwice() throws Exception {
        clusterMonitoringBeansRegistrar.registerClusterMonitoringMBeans();
        clusterMonitoringBeansRegistrar.registerClusterMonitoringMBeans();

        verify(mBeanServer, times(1)).registerMBean(any(ClusterNodeStatus.class), eq(new ObjectName(BEAN_NAME)));
    }

    @Test
    public void registrationProblemsAreHandled() throws Exception {
        Throwable exception = new MBeanRegistrationException(new Exception());
        when(mBeanServer.registerMBean(any(ClusterNodeStatus.class), any(ObjectName.class))).thenThrow(exception);

        clusterMonitoringBeansRegistrar.registerClusterMonitoringMBeans();
        assertThat(log.contains("Unable to register monitoring bean", Level.WARN), is(true));
    }

    @Test
    public void beanIsUnregistered() throws Exception {
        clusterMonitoringBeansRegistrar.registerClusterMonitoringMBeans();
        clusterMonitoringBeansRegistrar.unregisterClusterMonitorMBeans();

        verify(mBeanServer, times(1)).unregisterMBean(eq(new ObjectName(BEAN_NAME)));
    }

    @Test
    public void unregisteringHandlesErrors() throws Exception {
        clusterMonitoringBeansRegistrar.unregisterClusterMonitorMBeans();

        verify(mBeanServer, times(1)).unregisterMBean(eq(new ObjectName(BEAN_NAME)));
        assertThat(log.contains("Unable to unregister monitoring bean", Level.ERROR), is(true));
    }
}
