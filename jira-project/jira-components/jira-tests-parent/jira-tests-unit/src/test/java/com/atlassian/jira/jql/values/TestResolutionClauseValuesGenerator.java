package com.atlassian.jira.jql.values;

import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.issue.IssueConstant;
import com.atlassian.jira.issue.resolution.MockResolution;
import com.atlassian.jira.issue.resolution.Resolution;
import com.google.common.collect.ImmutableList;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.List;

import static com.atlassian.jira.jql.values.ResolutionClauseValuesGenerator.quoteName;
import static org.hamcrest.Matchers.contains;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestResolutionClauseValuesGenerator {
    @Rule
    public MockitoRule mocks = MockitoJUnit.rule();

    @Mock
    private ConstantsManager constantsManager;

    @InjectMocks
    private ResolutionClauseValuesGenerator valuesGenerator;

    @Test
    public void testGetAllConstants() throws Exception {
        final MockResolution type1 = new MockResolution("1", "Aa it");
        final MockResolution type2 = new MockResolution("2", "A it");
        final MockResolution type3 = new MockResolution("3", "B it");
        final MockResolution type4 = new MockResolution("4", "C it");

        final List<Resolution> resolutions = ImmutableList.of(type4, type3, type2, type1);
        when(constantsManager.getResolutionObjects()).thenReturn(resolutions);

        final List<IssueConstant> allConstants = valuesGenerator.getAllConstants();

        assertThat(allConstants, contains(type4, type3, type2, type1));
    }

    @Test
    public void testGetAllConstantNames() throws Exception {
        final MockResolution type1 = new MockResolution("1", "Aa it");
        final MockResolution type2 = new MockResolution("2", "A it");
        final MockResolution type3 = new MockResolution("3", "B it");
        final MockResolution type4 = new MockResolution("4", "C it");

        final List<Resolution> resolutions = ImmutableList.of(type1, type2, type3, type4);
        when(constantsManager.getResolutionObjects()).thenReturn(resolutions);

        final List<String> result = valuesGenerator.getAllConstantNames();

        assertThat(result, contains("Aa it", "A it", "B it", "C it", "Unresolved"));
    }

    @Test
    public void testQuoteName() throws Exception {
        assertEquals("\"unresolved\"", quoteName("unresolved"));
        assertEquals("\"'unresolved\"", quoteName("'unresolved"));
        assertEquals("\"\"unresolved\"", quoteName("\"unresolved"));
        assertEquals("\"unresolved'\"", quoteName("unresolved'"));
        assertEquals("\"unresolved\"\"", quoteName("unresolved\""));
        assertEquals("\"'unresolved'\"", quoteName("'unresolved'"));
        assertEquals("\"\"unresolved\"\"", quoteName("\"unresolved\""));
    }
}
