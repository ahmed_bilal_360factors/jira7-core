package com.atlassian.jira.upgrade.tasks.role.scenarios;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.collectors.CollectorsUtil;
import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.crowd.embedded.impl.ImmutableGroup;
import com.atlassian.crowd.embedded.impl.ImmutableUser;
import com.atlassian.extras.api.LicenseException;
import com.atlassian.fugue.Option;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.license.JiraProductLicense;
import com.atlassian.jira.mock.MockApplicationProperties;
import com.atlassian.jira.mock.ofbiz.MockOfBizDelegator;
import com.atlassian.jira.ofbiz.FieldMap;
import com.atlassian.jira.user.MockCrowdService;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import org.ofbiz.core.entity.GenericValue;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.function.Predicate;

import static com.atlassian.jira.license.LicenseDetailsFactoryImpl.JiraProductLicenseManager.INSTANCE;

/**
 * Helper class useful for setting up the migration tests.
 */
public class MigrationTestHelper {
    private static final String UPM_SERVICE_DESK_LICENSE_KEY =
            "com.atlassian.upm.license.internal.impl.PluginSettingsPluginLicenseR339b1704e923b2280c224738f2d48ceb";
    private static final String UPM_SERVICE_DESK_LICENSE_DELETED_KEY = "deleted_" + UPM_SERVICE_DESK_LICENSE_KEY;
    private static final String MIGRATION_DONE_KEY = "renaissanceMigrationDone";

    public static <T> ImmutableList<T> appendElement(ImmutableList<T> list, T elem) {
        return ImmutableList.<T>builder().addAll(list).add(elem).build();
    }

    private final MockOfBizDelegator mockOfBizDelegator;
    private final MockApplicationProperties applicationProperties;
    private final MockCrowdService crowdService;

    public MigrationTestHelper(final MockOfBizDelegator mockOfBizDelegator,
                               final MockApplicationProperties applicationProperties, final MockCrowdService crowdService) {
        this.mockOfBizDelegator = mockOfBizDelegator;
        this.applicationProperties = applicationProperties;
        this.crowdService = crowdService;
    }

    public void givenJira6xServiceDeskLicenseInPluginStore(String encodedLicense) {
        applicationProperties.setText(UPM_SERVICE_DESK_LICENSE_KEY, encodedLicense);
    }

    public void givenJira6xServiceDeskLicenseInMigrationStore(String encodedLicense) {
        applicationProperties.setText(UPM_SERVICE_DESK_LICENSE_DELETED_KEY, encodedLicense);
    }

    public void givenJira6xLicense(String encodedLicense) {
        applicationProperties.setText(APKeys.JIRA_LICENSE, encodedLicense);
    }

    public void givenNoJira6xLicense() {
        applicationProperties.setText(APKeys.JIRA_LICENSE, null);
    }

    public void givenLicenseInMultiStore(String encodedLicense) {
        mockOfBizDelegator.createValue("ProductLicense", ImmutableMap.of("license", encodedLicense));
    }

    public void addGroupPermission(GroupName groupName, String permission) {
        if (groupName.isAnyone()) {
            mockOfBizDelegator.createValue("GlobalPermissionEntry", ImmutableMap.of("permission", permission));
        } else {
            crowdService.addGroup(groupName.toGroup());
            mockOfBizDelegator.createValue("GlobalPermissionEntry", ImmutableMap.of("group_id", groupName.name(), "permission", permission));
        }
    }

    public void addUserToGroup(String userName, String groupName, boolean disabled) {
        final ImmutableGroup group = new ImmutableGroup(groupName);
        crowdService.addGroup(group);
        final User user = ImmutableUser.newUser().name(userName).active(!disabled).toUser();
        crowdService.addUser(user, null);
        crowdService.addUserToGroup(user, group);
    }

    public Option<String> getEncodedLicenseForApplication(String applicationKey) {
        return getAllLicenses().stream()
                .filter(licenseContains(applicationKey))
                .findFirst()
                .map(Option::some)
                .orElseGet(Option::none);
    }

    private Predicate<String> licenseContains(final String key) {
        final ApplicationKey k = ApplicationKey.valueOf(key);
        return encodedLicense ->
        {
            try {
                final JiraProductLicense productLicense = INSTANCE.getProductLicense(encodedLicense);
                return productLicense.getApplications().getKeys().contains(k);
            } catch (LicenseException e) {
                return false;
            }
        };
    }

    public String getServiceDeskProperty(String key) {
        final FieldMap fieldMap = FieldMap.build(
                "entityName", "vp.properties",
                "entityId", 1,
                "type", 5,
                "propertyKey", key);

        final List<GenericValue> properties = mockOfBizDelegator.findByAnd("OSPropertyEntry", fieldMap);
        if (properties == null || properties.isEmpty()) {
            return null;
        }
        final GenericValue value = properties.get(0);
        final GenericValue stringProperty = mockOfBizDelegator.findById("OSPropertyString", value.getLong("id"));
        if (stringProperty == null) {
            return null;
        }

        return stringProperty.getString("value");
    }

    public List<String> getAllLicenses() {
        return mockOfBizDelegator.findAll("ProductLicense")
                .stream()
                .map(gv -> gv.getString("license"))
                .collect(CollectorsUtil.toImmutableList());
    }

    /**
     * Retrieves groups with all application roles and permissions. Should be used after migration is finished.
     *
     * @return groups with all roles and permissions.
     */
    public Map<GroupName, GroupWithAppRolesAndPermissions> getAllGroups() {
        ApplicationRoleIndex map = new ApplicationRoleIndex();
        mockOfBizDelegator.findAll("LicenseRoleGroup").forEach(gv ->
        {
            final GroupName groupId = GroupName.valueOf(gv.getString("groupId"));
            final String applicationId = gv.getString("licenseRoleName");
            final Boolean primary = gv.getBoolean("primaryGroup");

            map.withGroup(groupId, group ->
            {
                group = group.withApplication(applicationId);
                if (Boolean.TRUE.equals(primary)) {
                    group = group.withDefaultApplication(applicationId);
                }
                return group;
            });
        });

        mockOfBizDelegator.findAll("GlobalPermissionEntry").forEach(gv ->
        {
            final GroupName groupId = GroupName.valueOf(gv.getString("group_id"));
            final String permission = gv.getString("permission");

            map.withGroup(groupId, group -> group.withPermission(permission));
        });

        return map.asMap();
    }

    /**
     * Adds attribute to the group. If group does not exist it is added as well.
     *
     * @param groupName      name of the group
     * @param attributeName  name of the attribute
     * @param attributeValue value of the attribute
     */
    public void addGroupAttribute(final String groupName, final String attributeName, final String attributeValue) {
        Group group = crowdService.getGroup(groupName);
        if (group == null) {
            group = new ImmutableGroup(groupName);
            crowdService.addGroup(group);
        }
        crowdService.setGroupAttribute(group, attributeName, attributeValue);
    }

    public boolean getMigratedFlag() {
        return applicationProperties.getOption(MIGRATION_DONE_KEY);
    }

    private static class ApplicationRoleIndex {
        private Map<GroupName, GroupWithAppRolesAndPermissions> index = Maps.newHashMap();

        private ApplicationRoleIndex withGroup(GroupName name,
                                               Function<GroupWithAppRolesAndPermissions, GroupWithAppRolesAndPermissions> operation) {
            index.compute(name, (key, value) ->
            {
                if (value == null) {
                    value = GroupWithAppRolesAndPermissions.empty(key);
                }
                return operation.apply(value);
            });

            return this;
        }

        private Map<GroupName, GroupWithAppRolesAndPermissions> asMap() {
            return index;
        }
    }
}
