package com.atlassian.jira.upgrade.tasks.role.scenarios;


import com.atlassian.fugue.Option;
import com.atlassian.jira.upgrade.tasks.role.MigrationFailedException;
import com.atlassian.jira.upgrade.tasks.role.RenaissanceMigration;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.runtime.UsesModule;

import java.util.List;
import java.util.Map;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

@UsesModule(RenaissanceMigrationModule.class)
public class MigrationSteps {
    enum StoreType {
        PLUGIN {
            @Override
            void setLicense(final MigrationTestHelper helper, final String license) {
                helper.givenJira6xServiceDeskLicenseInPluginStore(license);
            }
        },
        MIGRATION {
            @Override
            void setLicense(final MigrationTestHelper helper, final String license) {
                helper.givenJira6xServiceDeskLicenseInMigrationStore(license);
            }
        };

        abstract void setLicense(MigrationTestHelper helper, String license);
    }

    private final RenaissanceMigration renaissanceMigration;
    private final MigrationTestHelper migrationTestHelper;

    private MigrationFailedException migrationException;
    private boolean expectFailure = false;

    public MigrationSteps(final RenaissanceMigration renaissanceMigration,
                          final MigrationTestHelper migrationTestHelper) {
        this.renaissanceMigration = renaissanceMigration;
        this.migrationTestHelper = migrationTestHelper;
    }

    /**
     * If scenario is annotated with @expectFailure then exception thrown by migration is is stored and can be examined
     * by then steps. If exception is thrown during migration and scenario is\ not annotated with failedMigration then
     * test will fail immediately making it easier to see the issue.
     */
    @Before("@failedMigration")
    public void failedMigration() {
        expectFailure = true;
    }

    @Given("^Service Desk license (\\S+) in (plugin|migration) store$")
    public void givenServiceDeskLicenseInLegacyStore(String licenseName, StoreType type) {
        type.setLicense(migrationTestHelper, getLicense(licenseName));
    }

    @Given("^JIRA license (\\S+) in old store$")
    public void givenJira6xLicense(String licenseName) throws Throwable {
        migrationTestHelper.givenJira6xLicense(getLicense(licenseName));
    }

    @Given("^no JIRA license in old store$")
    public void givenNoJira6xLicense() throws Throwable {
        migrationTestHelper.givenNoJira6xLicense();
    }

    @Given("^license (\\S+) in multistore$")
    public void givenLicenseInMultistore(MigrationLicenses license) throws Throwable {
        String encodedLicense = license.getEncodedLicense();
        if (encodedLicense != null) {
            migrationTestHelper.givenLicenseInMultiStore(encodedLicense);
        }
    }

    @Given("^(\\S+) license provided by user$")
    public void givenLicenseByUser(MigrationLicenses license) throws Throwable {
        givenLicenseInMultistore(license);
    }

    @Given("^following users:$")
    public void followingUsers(List<UserWithGroups> users) throws Throwable {
        for (UserWithGroups user : users) {
            user.getGroups().forEach(group -> migrationTestHelper.addUserToGroup(user.getUser(), group, user.isDisabled()));
        }
    }

    @Given("^following groups:$")
    public void givenGroups(List<GroupWithPermissions> groupsWithPermissions) {
        for (GroupWithPermissions groupsWithPermission : groupsWithPermissions) {
            groupsWithPermission.getPermissions().stream()
                    .forEach(permission -> migrationTestHelper.addGroupPermission(groupsWithPermission.getName(), permission));
        }
    }

    @Given("^group '(\\S+)' with attribute '(\\S+)' set to '(\\S+)'$")
    public void givenGroupWithAttribute(String groupName, String attributeName, String attributeValue) {
        migrationTestHelper.addGroupAttribute(groupName, attributeName, attributeValue);
    }

    @When("^migration runs$")
    public void whenMigrationRuns() {
        try {
            this.renaissanceMigration.migrate();
        } catch (MigrationFailedException e) {
            if (expectFailure) {
                this.migrationException = e;
            } else {
                throw e;
            }
        }
    }

    @Then("^groups have been migrated as follows:$")
    public void thenGroupsHaveBeenMigrated(List<GroupWithAppRolesAndPermissions> expectedGroups) {
        final Map<GroupName, GroupWithAppRolesAndPermissions> groupsInDb = migrationTestHelper.getAllGroups();

        for (GroupWithAppRolesAndPermissions expectedGroup : expectedGroups) {
            final GroupWithAppRolesAndPermissions groupInDb = groupsInDb.get(expectedGroup.getName());

            assertNotNull(String.format("Group '%s' not found in database after migration", expectedGroup.getName()), groupInDb);

            assertThat(String.format("Group '%s' does not have expected permissions", expectedGroup.getName()),
                    groupInDb.getPermissions(), containsInAnyOrder(expectedGroup.getPermissions().toArray()));

            assertThat(String.format("Group '%s' does not have expected applications", expectedGroup.getName()),
                    groupInDb.getApplications(), containsInAnyOrder(expectedGroup.getApplications().toArray()));

            assertThat(String.format("Group '%s' does not have expected default applications", expectedGroup.getName()),
                    groupInDb.getDefaultApplications(), containsInAnyOrder(expectedGroup.getDefaultApplications().toArray()));
        }
    }

    @Then("^there is license (\\S+) in multistore for (\\S+) application$")
    public void thenThereIsLicenseInMultistore(String licenseFile, String applicationKey) {
        final Option<String> licenseForApp = migrationTestHelper.getEncodedLicenseForApplication(applicationKey);
        assertTrue(String.format("License for application %s is not in the store", applicationKey), licenseForApp.isDefined());
        assertThat(String.format("License for application %s is different than expected", applicationKey),
                getLicense(licenseFile), is(licenseForApp.get()));
    }

    @Then("^there is no license for (\\S+) in multistore$")
    public void thenThereIsNoLicenseInMultistore(String applicationKey) {
        final Option<String> licenseForApp = migrationTestHelper.getEncodedLicenseForApplication(applicationKey);
        assertTrue(String.format("License for application %s exists", applicationKey), licenseForApp.isEmpty());
    }

    @Then("^license (\\S+) is not in multistore$")
    public void thenThereIsNoLicenseInMultistore(MigrationLicenses license) {
        if (license != MigrationLicenses.NONE) {
            final String encodedLicense = license.getEncodedLicense();
            assertThat(String.format("License %s in multistore.", license.name()),
                    migrationTestHelper.getAllLicenses(), not(hasItem(encodedLicense)));
        }
    }

    @Then("^there is no license in multistore$")
    public void thenThereIsNoLicenseInMultistore() {
        final List<?> licenses = migrationTestHelper.getAllLicenses();
        assertTrue(String.format("Expected no licenses in multistore but following were present: %s",
                licenses.toString()), licenses.isEmpty());
    }

    private String getLicense(final String licenseName) {
        try {
            return MigrationLicenses.valueOf(licenseName).getEncodedLicense();
        } catch (IllegalArgumentException e) {
            throw new RuntimeException("Invalid license: " + licenseName);
        }
    }


    @Then("^migration failed$")
    public void thenMigrationFailedWithError() {
        assertNotNull("Migration succeeded - expected it to fail", migrationException);
    }

    @Then("^Service Desk property '(\\S+)' is set to value '(\\S+)'$")
    public void thenServiceDeskPropertyIsSet(String key, String expectedValue) throws Throwable {
        final String actualValue = migrationTestHelper.getServiceDeskProperty(key);
        assertThat(String.format("Service Desk property %s has invalid value", key), actualValue, is(expectedValue));
    }

    @Then("^Renaissance migration flagged done$")
    public void thenMigrationIsFlagged() {
        final boolean migrationFlag = migrationTestHelper.getMigratedFlag();
        assertThat("Migration flag is set.", migrationFlag, is(true));
    }
}
