package com.atlassian.jira.imports.project.handler;

import com.atlassian.jira.entity.Entity;
import com.atlassian.jira.entity.property.EntityProperty;
import com.atlassian.jira.entity.property.EntityPropertyImpl;
import com.atlassian.jira.entity.property.EntityPropertyType;
import com.atlassian.jira.exception.ParseException;
import com.atlassian.jira.imports.project.ProjectImportPersister;
import com.atlassian.jira.imports.project.core.EntityRepresentation;
import com.atlassian.jira.imports.project.core.EntityRepresentationImpl;
import com.atlassian.jira.imports.project.core.ProjectImportResults;
import com.atlassian.jira.imports.project.mapper.SimpleProjectImportIdMapper;
import com.atlassian.jira.junit.rules.InitMockitoMocks;
import com.atlassian.jira.matchers.ReflectionEqualTo;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.Predicate;
import com.google.common.collect.Maps;
import org.hamcrest.CoreMatchers;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.sql.Timestamp;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.stream.Collectors;

import static com.atlassian.jira.imports.project.parser.EntityPropertyParser.ENTITY_PROPERTY_ENTITY_NAME;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @since v6.2
 */
public class TestEntityPropertiesPersisterHandler {
    private final Executor executor = new ExecutorForTests();
    @Rule
    public TestRule initMock = new InitMockitoMocks(this);
    @Mock
    private ProjectImportPersister projectImportPersister;
    @Mock
    private ProjectImportResults projectImportResults;
    @Mock
    private I18nHelper i18nHelper;
    @Mock
    private SimpleProjectImportIdMapper projectImportIdMapper;
    @Mock
    private SimpleProjectImportIdMapper issueImportIdMapper;
    @Mock
    private EntityPropertiesPersisterHandler propertiesPersisterHandler;

    @Before
    public void setUp() {
        propertiesPersisterHandler = new EntityPropertiesPersisterHandler(executor, projectImportResults, projectImportPersister, EntityPropertyType.ISSUE_PROPERTY, issueImportIdMapper);

    }

    @Test
    public void shouldTransformIssuePropertyWithNewIssueId() throws ParseException, AbortImportException {
        //having
        final Map<String, String> attributes = createNewEntityProperty(1L, EntityPropertyType.ISSUE_PROPERTY.getDbEntityName());
        final String newIssueId = "123";
        when(issueImportIdMapper.getMappedId(attributes.get(EntityProperty.ENTITY_ID))).
                thenReturn(String.valueOf(newIssueId));
        //when
        propertiesPersisterHandler.handleEntity(ENTITY_PROPERTY_ENTITY_NAME, attributes);
        //then
        final Map<String, String> expectedAttributes = Maps.newHashMap(attributes);
        expectedAttributes.put(EntityProperty.ENTITY_ID, newIssueId);

        verify(projectImportPersister).createEntity(Mockito.argThat(entityRepresentationMatcher(expectedAttributes)));
    }

    @Test
    public void shouldTransformProjectPropertyWithNewProjectId() throws ParseException, AbortImportException {
        //having
        propertiesPersisterHandler = new EntityPropertiesPersisterHandler(executor, projectImportResults, projectImportPersister, EntityPropertyType.PROJECT_PROPERTY, projectImportIdMapper);
        final Map<String, String> attributes = createNewEntityProperty(1L, EntityPropertyType.PROJECT_PROPERTY.getDbEntityName());
        final String newProjectId = "123";
        when(projectImportIdMapper.getMappedId(attributes.get(EntityProperty.ENTITY_ID))).thenReturn(newProjectId);
        //when
        propertiesPersisterHandler.handleEntity(ENTITY_PROPERTY_ENTITY_NAME, attributes);

        //then
        final Map<String, String> expectedAttributes = Maps.newHashMap(attributes);
        expectedAttributes.put(EntityProperty.ENTITY_ID, newProjectId);

        verify(projectImportPersister).createEntity(Mockito.argThat(entityRepresentationMatcher(expectedAttributes)));
    }

    @Test
    public void shouldNotTransformOtherTransformProjectAndIssueProperties() throws ParseException, AbortImportException {
        //having
        final Map<String, String> attributes = createNewEntityProperty(1L, EntityPropertyType.REMOTE_VERSION_LINK.getDbEntityName());

        //when
        propertiesPersisterHandler.handleEntity(ENTITY_PROPERTY_ENTITY_NAME, attributes);
        //then

        verify(projectImportPersister, never()).createEntity(any(EntityRepresentation.class));

    }

    @Test
    public void shouldNotTouchOtherEntitiesThatEntityProperties() throws ParseException, AbortImportException {
        //having
        final Map<String, String> attributes = createNewEntityProperty(1L, EntityPropertyType.ISSUE_PROPERTY.getDbEntityName());

        //when
        propertiesPersisterHandler.handleEntity(ENTITY_PROPERTY_ENTITY_NAME + "wrong", attributes);
        //then

        verify(projectImportPersister, never()).createEntity(any(EntityRepresentation.class));
    }

    @Test
    public void shouldHandleNotCreatedIssuePropertyWithReportingError() throws ParseException, AbortImportException {
        //having
        final Map<String, String> attributes = createNewEntityProperty(1L, EntityPropertyType.ISSUE_PROPERTY.getDbEntityName());
        final String newIssueId = "12";
        when(issueImportIdMapper.getMappedId(attributes.get(EntityProperty.ENTITY_ID))).
                thenReturn(newIssueId);
        when(projectImportPersister.createEntity(any(EntityRepresentation.class))).thenReturn(null);
        when(projectImportResults.getI18n()).thenReturn(i18nHelper);
        final String fancyErrorMessage = "fancyErrorMessage";
        when(i18nHelper.getText("admin.errors.project.import.entity.property.error",
                attributes.get(EntityProperty.ID),
                attributes.get(EntityProperty.ENTITY_NAME),
                attributes.get(EntityProperty.ENTITY_ID))).thenReturn(fancyErrorMessage);

        //when
        propertiesPersisterHandler.handleEntity(ENTITY_PROPERTY_ENTITY_NAME, attributes);
        //then
        verify(projectImportResults).addError(fancyErrorMessage);
    }

    @Test
    public void shouldNotMapPropertiesThatBelongToNotImportedProject() throws ParseException, AbortImportException {
        //having
        final Map<String, String> attributes = createNewEntityProperty(1L, EntityPropertyType.ISSUE_PROPERTY.getDbEntityName());

        //when
        propertiesPersisterHandler.handleEntity(ENTITY_PROPERTY_ENTITY_NAME, attributes);
        //then

        verify(projectImportPersister, never()).createEntity(any(EntityRepresentation.class));

    }

    @Test
    public void shouldThrowParseExceptionWhenFieldIsMissing() throws ParseException, AbortImportException {
        final Map<String, String> attributes = createNewEntityProperty(1L, EntityPropertyType.ISSUE_PROPERTY.getDbEntityName());

        //when
        for (final String keyToRemove : attributes.keySet()) {
            final Map<String, String> brokenAttributes = Maps.newHashMap(attributes);
            brokenAttributes.remove(keyToRemove);
            try {
                propertiesPersisterHandler.handleEntity(ENTITY_PROPERTY_ENTITY_NAME, brokenAttributes);
                Assert.fail("Expected ParseException for missing field " + keyToRemove);
            } catch (final ParseException exception) {
                Assert.assertThat(exception.getMessage(), CoreMatchers.is(CoreMatchers.containsString(keyToRemove)));
            }
        }
        //then
        verify(projectImportPersister, never()).createEntity(any(EntityRepresentation.class));
    }

    @Test
    public void shouldIgnoreEntityPropertyId() throws ParseException, AbortImportException {
        final Map<String, String> attributes = createNewEntityProperty(1L, EntityPropertyType.ISSUE_PROPERTY.getDbEntityName());
        final String newIssueId = "123";
        when(issueImportIdMapper.getMappedId(attributes.get(EntityProperty.ENTITY_ID)))
                .thenReturn(String.valueOf(newIssueId));

        //Emulate situation when there is already a property with the same id
        when(projectImportPersister.createEntity(any())).thenReturn(1L);
        when(projectImportPersister.createEntity(Matchers.argThat(entityRepresentationIdNullMatcher(
                value -> "1".equals(value.getEntityValues().get(EntityProperty.ID))))))
                .thenReturn(null);

        //when
        propertiesPersisterHandler.handleEntity(ENTITY_PROPERTY_ENTITY_NAME, attributes);

        //then
        verify(projectImportResults, never()).addError(any());

    }

    private Map<String, String> createNewEntityProperty(final Long id, final String entityName) {
        final Map<String, Object> fieldsMap = Entity.ENTITY_PROPERTY.fieldMapFrom(EntityPropertyImpl.existing(
                id, entityName, 1000L + id, "key" + id, "value" + id, new Timestamp(2000L + id), new Timestamp(3000L + id)
        ));
        return valuesToString(fieldsMap);
    }

    private Map<String, String> valuesToString(final Map<String, Object> arguments) {
        return Maps.transformValues(arguments, Object::toString);
    }

    private Matcher<EntityRepresentation> entityRepresentationMatcher(final Map<String, String> entityValues) {
        final Map<String, String> internal = entityValues.entrySet().stream()
                .filter(entry -> !EntityProperty.ID.equals(entry.getKey()))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
        return new ReflectionEqualTo<>(new EntityRepresentationImpl(ENTITY_PROPERTY_ENTITY_NAME, internal));
    }

    private Matcher<EntityRepresentation> entityRepresentationIdNullMatcher(
            final Predicate<EntityRepresentation> condition) {
        return new TypeSafeMatcher<EntityRepresentation>() {
            @Override
            protected boolean matchesSafely(final EntityRepresentation entityRepresentation) {
                return condition.evaluate(entityRepresentation);
            }

            @Override
            public void describeTo(final Description description) {
            }
        };

    }
}
