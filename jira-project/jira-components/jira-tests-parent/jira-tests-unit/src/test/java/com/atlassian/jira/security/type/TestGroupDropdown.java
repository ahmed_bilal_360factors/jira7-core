package com.atlassian.jira.security.type;

import com.atlassian.jira.issue.security.IssueSecurityLevel;
import com.atlassian.jira.issue.security.IssueSecurityLevelImpl;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.user.MockApplicationUser;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TermQuery;
import org.junit.Test;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;

public class TestGroupDropdown {
    private final GroupDropdown groupDropdown = new GroupDropdown(null);

    @Test
    public void testGetQueryWithNullProject() throws Exception {
        GroupDropdown groupDropdown = new GroupDropdown(null);
        Query query = groupDropdown.getQuery(new MockApplicationUser("fred"), null, "developers");

        assertNull(query);
    }

    @Test
    public void testGetQueryWithProjectOnly() throws Exception {
        GroupDropdown groupDropdown = new GroupDropdown(null);
        Query query = groupDropdown.getQuery(new MockApplicationUser("fred"), new MockProject(12), "developers");

        assertThat(query.toString(), equalTo("projid:12"));

        // expect a Term Query
        TermQuery actual = (TermQuery) query;
        assertThat(actual.getTerm().field(), equalTo("projid"));
        assertThat(actual.getTerm().text(), equalTo("12"));
    }

    @Test
    public void testGetQueryWithSecurityLevel() throws Exception {
        IssueSecurityLevel securityLevel = new IssueSecurityLevelImpl(10100L, "Blue", "", 20L);
        Query query = groupDropdown.getQuery(new MockApplicationUser("fred"), new MockProject(12, "ABC"), securityLevel, "developers");

        assertThat(query.toString(), equalTo("issue_security_level:10100"));

        // expect a Term Query
        TermQuery actual = (TermQuery) query;
        assertThat(actual.getTerm().field(), equalTo("issue_security_level"));
        assertThat(actual.getTerm().text(), equalTo("10100"));
    }
}
