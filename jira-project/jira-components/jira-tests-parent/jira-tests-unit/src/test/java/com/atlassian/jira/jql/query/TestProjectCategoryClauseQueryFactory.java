package com.atlassian.jira.jql.query;

import com.atlassian.jira.jql.operand.JqlOperandResolver;
import com.atlassian.jira.jql.operand.QueryLiteral;
import com.atlassian.jira.jql.resolver.ProjectCategoryResolver;
import com.atlassian.jira.jql.validator.MockJqlOperandResolver;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.collect.CollectionBuilder;
import com.atlassian.query.clause.TerminalClause;
import com.atlassian.query.clause.TerminalClauseImpl;
import com.atlassian.query.operand.MultiValueOperand;
import com.atlassian.query.operand.Operand;
import com.atlassian.query.operand.SingleValueOperand;
import com.atlassian.query.operator.Operator;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TermQuery;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static com.atlassian.jira.jql.operand.SimpleLiteralFactory.createLiteral;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestProjectCategoryClauseQueryFactory {
    @Rule
    public MockitoRule mockito = MockitoJUnit.rule();

    @Mock
    private ProjectCategoryResolver projectCategoryResolver;
    private JqlOperandResolver jqlOperandResolver;
    private ApplicationUser theUser = null;
    private QueryCreationContext queryCreationContext;

    @Before
    public void setUp() throws Exception {
        jqlOperandResolver = MockJqlOperandResolver.createSimpleSupport();
        queryCreationContext = new QueryCreationContextImpl(theUser);
    }

    @Test
    public void testBadOperator() throws Exception {
        final TerminalClause terminalClause = new TerminalClauseImpl("category", Operator.LIKE, "test");
        final ProjectCategoryClauseQueryFactory factory = new ProjectCategoryClauseQueryFactory(projectCategoryResolver, jqlOperandResolver);

        final QueryFactoryResult result = factory.getQuery(queryCreationContext, terminalClause);
        final Query expectedQuery = new BooleanQuery();
        assertEquals(expectedQuery, result.getLuceneQuery());
        assertFalse(result.mustNotOccur());
    }

    @Test
    public void testNullLiterals() throws Exception {
        final SingleValueOperand operand = new SingleValueOperand("test");
        final TerminalClause terminalClause = new TerminalClauseImpl("category", Operator.EQUALS, operand);

        jqlOperandResolver = mock(JqlOperandResolver.class);
        when(jqlOperandResolver.getValues(queryCreationContext, operand, terminalClause)).thenReturn(null);

        final ProjectCategoryClauseQueryFactory factory = new ProjectCategoryClauseQueryFactory(projectCategoryResolver, jqlOperandResolver);

        final QueryFactoryResult result = factory.getQuery(queryCreationContext, terminalClause);
        final Query expectedQuery = new BooleanQuery();
        assertEquals(expectedQuery, result.getLuceneQuery());
        assertFalse(result.mustNotOccur());
    }

    @Test
    public void testEqualitySingleValue() throws Exception {
        final Operand operand = new SingleValueOperand(createLiteral(1L));
        final TerminalClause terminalClause = new TerminalClauseImpl("category", Operator.EQUALS, operand);

        when(projectCategoryResolver.getProjectsForCategory(createLiteral(1L)))
                .thenReturn(CollectionBuilder.<Project>newBuilder(new MockProject(2L)).asSet());

        final ProjectCategoryClauseQueryFactory factory = new ProjectCategoryClauseQueryFactory(projectCategoryResolver, jqlOperandResolver);

        final QueryFactoryResult result = factory.getQuery(queryCreationContext, terminalClause);
        final Query expectedQuery = new TermQuery(new Term("projid", "2"));
        assertEquals(expectedQuery, result.getLuceneQuery());
        assertFalse(result.mustNotOccur());
    }

    @Test
    public void testEqualityMultiValue() throws Exception {
        final MultiValueOperand operand = new MultiValueOperand(new QueryLiteral(), createLiteral(1L), createLiteral("test"));
        final TerminalClause terminalClause = new TerminalClauseImpl("category", Operator.IN, operand);

        setUpCategoryResolver();

        final ProjectCategoryClauseQueryFactory factory = new ProjectCategoryClauseQueryFactory(projectCategoryResolver, jqlOperandResolver);

        final QueryFactoryResult result = factory.getQuery(queryCreationContext, terminalClause);
        final BooleanQuery expectedQuery = new BooleanQuery();
        expectedQuery.add(new TermQuery(new Term("projid", "1")), BooleanClause.Occur.SHOULD);
        expectedQuery.add(new TermQuery(new Term("projid", "2")), BooleanClause.Occur.SHOULD);
        expectedQuery.add(new TermQuery(new Term("projid", "3")), BooleanClause.Occur.SHOULD);
        assertEquals(expectedQuery, result.getLuceneQuery());
        assertFalse(result.mustNotOccur());
    }


    @Test
    public void testInequalityWithEmpty() throws Exception {
        final MultiValueOperand operand = new MultiValueOperand(new QueryLiteral(), createLiteral(1L), createLiteral("test"));
        final TerminalClause terminalClause = new TerminalClauseImpl("category", Operator.NOT_IN, operand);

        setUpCategoryResolver();

        final ProjectCategoryClauseQueryFactory factory = new ProjectCategoryClauseQueryFactory(projectCategoryResolver, jqlOperandResolver);

        final QueryFactoryResult result = factory.getQuery(queryCreationContext, terminalClause);
        final BooleanQuery expectedQuery = new BooleanQuery();
        expectedQuery.add(new TermQuery(new Term("projid", "1")), BooleanClause.Occur.SHOULD);
        expectedQuery.add(new TermQuery(new Term("projid", "2")), BooleanClause.Occur.SHOULD);
        expectedQuery.add(new TermQuery(new Term("projid", "3")), BooleanClause.Occur.SHOULD);
        assertEquals(expectedQuery, result.getLuceneQuery());
        assertTrue(result.mustNotOccur());
    }

    @Test
    public void testInequalityWithoutEmpty() throws Exception {
        final MultiValueOperand operand = new MultiValueOperand(createLiteral(1L), createLiteral("test"));
        final TerminalClause terminalClause = new TerminalClauseImpl("category", Operator.NOT_IN, operand);

        setUpCategoryResolver();

        final ProjectCategoryClauseQueryFactory factory = new ProjectCategoryClauseQueryFactory(projectCategoryResolver, jqlOperandResolver);

        final QueryFactoryResult result = factory.getQuery(queryCreationContext, terminalClause);
        final BooleanQuery expectedQuery = new BooleanQuery();
        expectedQuery.add(new TermQuery(new Term("projid", "2")), BooleanClause.Occur.SHOULD);
        expectedQuery.add(new TermQuery(new Term("projid", "3")), BooleanClause.Occur.SHOULD);
        expectedQuery.add(new TermQuery(new Term("projid", "1")), BooleanClause.Occur.SHOULD);
        assertEquals(expectedQuery, result.getLuceneQuery());
        assertTrue(result.mustNotOccur());
    }


    private void setUpCategoryResolver() {
        when(projectCategoryResolver.getProjectsForCategory(new QueryLiteral()))
                .thenReturn(CollectionBuilder.<Project>newBuilder(new MockProject(1L)).asSet());
        when(projectCategoryResolver.getProjectsForCategory(createLiteral(1L)))
                .thenReturn(CollectionBuilder.<Project>newBuilder(new MockProject(2L)).asSet());
        when(projectCategoryResolver.getProjectsForCategory(createLiteral("test")))
                .thenReturn(CollectionBuilder.<Project>newBuilder(new MockProject(3L)).asSet());
    }

}
