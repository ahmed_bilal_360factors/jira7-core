package com.atlassian.jira.scheme;

import com.atlassian.cache.CacheManager;
import com.atlassian.cache.memory.MemoryCacheManager;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.JiraTestUtil;
import com.atlassian.jira.association.NodeAssociationStore;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.JiraWebActionSupportDependencies;
import com.atlassian.jira.junit.rules.MockitoContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.mock.ofbiz.MockOfBizDelegator;
import com.atlassian.jira.notification.DefaultNotificationSchemeManager;
import com.atlassian.jira.notification.NotificationSchemeManager;
import com.atlassian.jira.notification.NotificationTypeManager;
import com.atlassian.jira.permission.DefaultPermissionSchemeManager;
import com.atlassian.jira.permission.PermissionContextFactory;
import com.atlassian.jira.permission.PermissionSchemeManager;
import com.atlassian.jira.permission.PermissionTypeManager;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.user.preferences.UserPreferencesManager;
import com.mockobjects.servlet.MockHttpServletResponse;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.mockito.Mock;
import org.ofbiz.core.entity.GenericValue;
import webwork.action.Action;

import static org.junit.Assert.assertEquals;

public class TestEditSchemeAction {
    @Rule
    public final MockitoContainer mockContainer = MockitoMocksInContainer.rule(this);
    @Rule
    public TestRule webActionSupport = JiraWebActionSupportDependencies.build(mockContainer);

    @Mock
    private ProjectManager projectManager;
    @Mock
    private PermissionTypeManager permissionTypeManager;
    @Mock
    private PermissionContextFactory permissionContextFactory;
    @Mock
    private SchemeFactory schemeFactory;
    @Mock
    private NodeAssociationStore nodeAssociationStore;
    @Mock
    private GroupManager groupManager;
    @Mock
    private EventPublisher eventPublisher;
    @Mock
    private NotificationTypeManager notificationTypeManager;
    @Mock
    private UserPreferencesManager userPreferencesManager;

    @AvailableInContainer
    private MockOfBizDelegator ofBizDelegator = new MockOfBizDelegator();

    private PermissionSchemeManager permSchemeManager;
    private NotificationSchemeManager notificationSchemeManager;

    @Before
    public void setUp()  {
        CacheManager cacheManager = new MemoryCacheManager();

        permSchemeManager= new DefaultPermissionSchemeManager(projectManager, permissionTypeManager,
                permissionContextFactory, ofBizDelegator, schemeFactory,
                nodeAssociationStore, groupManager, eventPublisher, cacheManager);
        notificationSchemeManager = new DefaultNotificationSchemeManager(projectManager, permissionTypeManager,
                permissionContextFactory, ofBizDelegator, schemeFactory,
                eventPublisher, notificationTypeManager, nodeAssociationStore,
                groupManager, userPreferencesManager, cacheManager);

        mockContainer.getMockWorker().addMock(PermissionSchemeManager.class,
                permSchemeManager);
        mockContainer.getMockWorker().addMock(NotificationSchemeManager.class,
                notificationSchemeManager);
    }

    @Test
    public void testEditPermissionScheme() throws Exception {
        MockHttpServletResponse response = JiraTestUtil.setupExpectedRedirect("ViewPermissionSchemes.jspa");

        com.atlassian.jira.web.action.admin.permission.EditScheme editScheme = new com.atlassian.jira.web.action.admin.permission.EditScheme();

        GenericValue scheme = permSchemeManager.createScheme("PScheme", "Test Desc");

        //set the scheme details
        editScheme.setName("New Name");
        editScheme.setDescription("New Description");

        //The scheme manager should be set correctly
        assertEquals(editScheme.getSchemeManager(), permSchemeManager);

        assertEquals(1, permSchemeManager.getSchemes().size());

        editScheme.setSchemeId(scheme.getLong("id"));

        //edit the scheme
        String result = editScheme.execute();

        //the new scheme should be there
        final GenericValue newScheme = permSchemeManager.getScheme(scheme.getLong("id"));
        assertEquals("New Name", newScheme.getString("name"));
        assertEquals("New Description", newScheme.getString("description"));

        //there should be no errors
        assertEquals(0, editScheme.getErrors().size());

        assertEquals(Action.NONE, result);

        response.verify();
    }

    @Test
    public void testEditNotificationScheme() throws Exception {
        MockHttpServletResponse response = JiraTestUtil.setupExpectedRedirect("ViewNotificationSchemes.jspa");

        com.atlassian.jira.web.action.admin.notification.EditScheme editScheme = new com.atlassian.jira.web.action.admin.notification.EditScheme();

        GenericValue scheme = notificationSchemeManager.createScheme("NScheme", "Test Desc");

        //set the scheme details
        editScheme.setName("New Name");
        editScheme.setDescription("New Description");

        //The scheme manager should be set correctly
        assertEquals(editScheme.getSchemeManager(), notificationSchemeManager);

        assertEquals(1, notificationSchemeManager.getSchemes().size());

        editScheme.setSchemeId(scheme.getLong("id"));

        //edit the scheme
        String result = editScheme.execute();

        //the new scheme should be there
        final GenericValue newScheme = notificationSchemeManager.getScheme(scheme.getLong("id"));
        assertEquals("New Name", newScheme.getString("name"));
        assertEquals("New Description", newScheme.getString("description"));

        //there should be no errors
        assertEquals(0, editScheme.getErrors().size());

        assertEquals(Action.NONE, result);

        response.verify();
    }
}
