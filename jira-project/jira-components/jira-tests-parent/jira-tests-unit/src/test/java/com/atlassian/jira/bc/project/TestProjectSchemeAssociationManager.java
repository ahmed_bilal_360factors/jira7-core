package com.atlassian.jira.bc.project;

import com.atlassian.jira.issue.context.JiraContextNode;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.fields.config.MockFieldConfigScheme;
import com.atlassian.jira.issue.fields.config.manager.FieldConfigSchemeManager;
import com.atlassian.jira.issue.fields.config.manager.IssueTypeSchemeManager;
import com.atlassian.jira.issue.fields.layout.field.FieldConfigurationScheme;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutManager;
import com.atlassian.jira.issue.fields.screen.issuetype.IssueTypeScreenSchemeManager;
import com.atlassian.jira.issue.fields.screen.issuetype.MockIssueTypeScreenScheme;
import com.atlassian.jira.mock.issue.fields.layout.field.MockFieldConfigurationScheme;
import com.atlassian.jira.permission.PermissionSchemeManager;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.scheme.Scheme;
import com.atlassian.jira.scheme.SchemeManagerFactory;
import com.atlassian.jira.workflow.WorkflowSchemeManager;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import java.util.List;

import static com.atlassian.jira.matchers.IterableMatchers.iterableWithSize;
import static com.google.common.collect.Lists.newArrayList;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.argThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestProjectSchemeAssociationManager {
    @Rule
    public final MethodRule initMockito = MockitoJUnit.rule();

    @Mock
    private SchemeManagerFactory schemeManagerFactory;
    @Mock
    private PermissionSchemeManager permissionSchemeManager;
    @Mock
    private WorkflowSchemeManager workflowSchemeManager;
    @Mock
    private IssueTypeSchemeManager issueTypeSchemeManager;
    @Mock
    private IssueTypeScreenSchemeManager issueTypeScreenSchemeManager;
    @Mock
    private FieldConfigSchemeManager fieldConfigSchemeManager;
    @Mock
    private FieldManager fieldManager;
    @Mock
    private ProjectManager projectManager;
    @Mock
    private FieldLayoutManager fieldLayoutManager;
    @Mock
    private Scheme scheme;

    private ProjectSchemeAssociationManager manager;

    private Project project = new MockProject(10000);
    private Project newProject = new MockProject(10001);
    private MockIssueTypeScreenScheme issueTypeScreenScheme = new MockIssueTypeScreenScheme();
    private MockFieldConfigScheme fieldConfigScheme = new MockFieldConfigScheme();
    private long fieldConfigSchemeId = 10232L;

    @Before
    public void setUp() throws Exception {
        fieldConfigScheme.addAssociatedProjects(project);
        when(schemeManagerFactory.getAllSchemeManagers()).thenReturn(newArrayList(permissionSchemeManager, workflowSchemeManager));

        manager = new ProjectSchemeAssociationManager(
                schemeManagerFactory,
                issueTypeSchemeManager,
                issueTypeScreenSchemeManager,
                fieldConfigSchemeManager,
                fieldManager,
                projectManager,
                fieldLayoutManager);
    }

    @Test
    public void associateDefaultSchemesCallsMethodForAllSchemeManagers() {
        manager.associateDefaultSchemesWithProject(project);

        verify(permissionSchemeManager).addDefaultSchemeToProject(eq(project));
        verify(workflowSchemeManager).addDefaultSchemeToProject(eq(project));
    }

    @Test
    public void associateSchemesOfExistingProjectDoesNothingWhenNoSchemesArePresentForProject() {
        mockProjectWithNoSchemes();

        manager.associateSchemesOfExistingProjectWithNewProject(newProject, project);

        verifyNoSchemesWereAssociated();
    }

    @Test
    public void associateSchemesOfExistingProjectLinksNewProjectToExistingSchemes() {
        mockExistingSchemes(false);

        manager.associateSchemesOfExistingProjectWithNewProject(newProject, project);

        verifySchemesAssociated();
        verify(fieldConfigSchemeManager, times(1)).updateFieldConfigScheme(eq(fieldConfigScheme), (List<JiraContextNode>) argThat(iterableWithSize(2, JiraContextNode.class)), any());
        verify(fieldManager, times(1)).refresh();
    }

    @Test
    public void associateSchemesOfExistingProjectWithDefaultIssueTypeSchemeDoesntDoAnything() {
        mockExistingSchemes(true);

        manager.associateSchemesOfExistingProjectWithNewProject(newProject, project);

        verifySchemesAssociated();
        verify(fieldConfigSchemeManager, never()).updateFieldConfigScheme(any(), any(), any());
        verify(fieldManager, never()).refresh();
    }

    private void mockProjectWithNoSchemes() {
        when(permissionSchemeManager.getSchemeFor(project)).thenReturn(null);
        when(workflowSchemeManager.getSchemeFor(project)).thenReturn(null);
        when(issueTypeSchemeManager.getConfigScheme(project)).thenReturn(null);
        when(issueTypeScreenSchemeManager.getIssueTypeScreenScheme(project)).thenReturn(null);
        when(fieldLayoutManager.getFieldConfigurationScheme(project)).thenReturn(null);
    }

    private void verifyNoSchemesWereAssociated() {
        verify(permissionSchemeManager, never()).addSchemeToProject(any(Project.class), any(Scheme.class));
        verify(workflowSchemeManager, never()).addSchemeToProject(any(Project.class), any(Scheme.class));
        verify(fieldConfigSchemeManager, never()).updateFieldConfigScheme(any(), any(), any());
        verify(issueTypeScreenSchemeManager, never()).addSchemeAssociation(any(Project.class), any());
        verify(fieldLayoutManager, never()).addSchemeAssociation(any(Project.class), any());
    }

    private void verifySchemesAssociated() {
        verify(permissionSchemeManager, times(1)).addSchemeToProject(eq(newProject), eq(scheme));
        verify(workflowSchemeManager, times(1)).addSchemeToProject(eq(newProject), eq(scheme));
        verify(issueTypeScreenSchemeManager, times(1)).addSchemeAssociation(eq(newProject), eq(issueTypeScreenScheme));
        verify(fieldLayoutManager, times(1)).addSchemeAssociation(eq(newProject), eq(fieldConfigSchemeId));
    }

    private void mockExistingSchemes(final boolean isDefaultIssueTypeScheme) {
        final FieldConfigurationScheme fieldConfigurationScheme = new MockFieldConfigurationScheme(fieldConfigSchemeId, "some Scheme", "");

        when(permissionSchemeManager.getSchemeFor(project)).thenReturn(scheme);
        when(workflowSchemeManager.getSchemeFor(project)).thenReturn(scheme);
        when(issueTypeSchemeManager.getConfigScheme(project)).thenReturn(fieldConfigScheme);
        when(issueTypeScreenSchemeManager.getIssueTypeScreenScheme(project)).thenReturn(issueTypeScreenScheme);
        when(fieldLayoutManager.getFieldConfigurationScheme(project)).thenReturn(fieldConfigurationScheme);
        when(issueTypeSchemeManager.isDefaultIssueTypeScheme(fieldConfigScheme)).thenReturn(isDefaultIssueTypeScheme);
    }
}