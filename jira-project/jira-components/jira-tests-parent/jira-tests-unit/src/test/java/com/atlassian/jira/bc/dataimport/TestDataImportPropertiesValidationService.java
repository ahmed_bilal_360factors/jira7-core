package com.atlassian.jira.bc.dataimport;

import com.atlassian.jira.bc.ServiceResult;
import com.atlassian.jira.bc.ServiceResultImpl;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.plugin.PluginAccessor;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import static com.atlassian.jira.bc.dataimport.DataImportOSPropertyValidator.DataImportProperties;
import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Sets.newHashSet;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.collection.IsEmptyCollection.empty;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith (MockitoJUnitRunner.class)
public class TestDataImportPropertiesValidationService {
    private static final String PROPERTY_KEY_1 = "property.key.1";
    private static final String PROPERTY_KEY_2 = "property.key.2";
    private static final String PROPERTY_KEY_3 = "property.key.3";

    private static final String VALIDATION_ERROR_1 = "error.message.1";
    private static final String VALIDATION_ERROR_2 = "error.message.2";
    private static final String VALIDATION_ERROR_3 = "error.message.3";

    @Mock
    private PluginAccessor pluginAccessor;

    @InjectMocks
    private DataImportPropertiesValidationService dataImportPropertiesValidationService;

    private DataImportParams dataImportParams;

    @Mock
    private DataImportProperties dataImportProperties;

    @Mock
    private DataImportOSPropertyValidator validator1;

    @Mock
    private DataImportOSPropertyValidator validator2;

    @Mock
    private DataImportOSPropertyValidator validator3;

    private List<DataImportOSPropertyValidatorModuleDescriptor> descriptors;

    @Before
    public void prepare() {
        dataImportParams = new DataImportParams.Builder("").build();

        descriptors = newArrayList();

        when(pluginAccessor.getEnabledModuleDescriptorsByClass(DataImportOSPropertyValidatorModuleDescriptor.class)).thenReturn(descriptors);

        when(validator1.getPropertyKeysToValidate()).thenReturn(newHashSet());
        when(validator1.validate(any(DataImportParams.class), any(DataImportProperties.class))).thenReturn(serviceResult(VALIDATION_ERROR_1, VALIDATION_ERROR_2));

        when(validator2.getPropertyKeysToValidate()).thenReturn(newHashSet(PROPERTY_KEY_1, PROPERTY_KEY_2));
        when(validator2.validate(any(DataImportParams.class), any(DataImportProperties.class))).thenReturn(serviceResult(VALIDATION_ERROR_3));

        when(validator3.getPropertyKeysToValidate()).thenReturn(newHashSet(PROPERTY_KEY_3));
        when(validator3.validate(any(DataImportParams.class), any(DataImportProperties.class))).thenReturn(serviceResult());
    }

    @Test
    public void shouldReturnAnEmptySetOfPropertyKeysToRecordWhenThereAreNoValidators() {
        Set<String> propertyKeysToRecord = dataImportPropertiesValidationService.getPropertyKeysToRecord();

        assertThat(propertyKeysToRecord, empty());
    }

    @Test
    public void shouldReturnAValidResultWhenThereAreNoValidators() {
        ServiceResult result = dataImportPropertiesValidationService.validate(dataImportParams, dataImportProperties);

        assertThat(result.isValid(), is(true));
    }

    @Test
    public void shouldReturnPropertyKeysRequiredByAllValidators() {
        givenADescriptorForAValidator(validator1);
        givenADescriptorForAValidator(validator2);
        givenADescriptorForAValidator(validator3);

        Set<String> propertyKeysToRecord = dataImportPropertiesValidationService.getPropertyKeysToRecord();

        assertThat(propertyKeysToRecord.size(), is(3));
        assertThat(propertyKeysToRecord, hasItems(PROPERTY_KEY_1, PROPERTY_KEY_2, PROPERTY_KEY_3));
    }

    @Test
    public void shouldReturnAllValidationErrorsProducedByAllValidators() {
        givenADescriptorForAValidator(validator1);
        givenADescriptorForAValidator(validator2);
        givenADescriptorForAValidator(validator3);

        ServiceResult serviceResult = dataImportPropertiesValidationService.validate(new DataImportParams.Builder("").build(), dataImportProperties);

        assertThat(serviceResult.isValid(), is(false));

        Collection<String> errorMessages = serviceResult.getErrorCollection().getErrorMessages();
        assertThat(errorMessages.size(), is(3));
        assertThat(errorMessages, hasItems(VALIDATION_ERROR_1, VALIDATION_ERROR_2, VALIDATION_ERROR_3));
    }

    @Test
    public void shouldReturnAnEmptySetOfPropertyKeysToRecordWhenValidatorsDoNotRequireAnyPropertyKeysToBeRecorded() {
        givenADescriptorForAValidator(validator1);

        Set<String> propertyKeysToRecord = dataImportPropertiesValidationService.getPropertyKeysToRecord();

        assertThat(propertyKeysToRecord, empty());
    }

    @Test
    public void shouldReturnAValidResultWhenValidatorsDoNotProduceAnyValidationErrors() {
        givenADescriptorForAValidator(validator3);

        ServiceResult serviceResult = dataImportPropertiesValidationService.validate(new DataImportParams.Builder("").build(), dataImportProperties);

        assertThat(serviceResult.isValid(), is(true));
    }

    private void givenADescriptorForAValidator(DataImportOSPropertyValidator validator) {
        DataImportOSPropertyValidatorModuleDescriptor descriptor = mock(DataImportOSPropertyValidatorModuleDescriptor.class);
        when(descriptor.getModule()).thenReturn(validator);
        descriptors.add(descriptor);
    }

    private ServiceResult serviceResult(String ...errorMessages) {
        SimpleErrorCollection errorCollection = new SimpleErrorCollection();
        errorCollection.addErrorMessages(newArrayList(errorMessages));
        return new ServiceResultImpl(errorCollection);
    }
}