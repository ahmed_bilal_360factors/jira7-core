package com.atlassian.jira.propertyset;

import com.atlassian.cache.memory.MemoryCacheManager;
import com.atlassian.jira.database.MockQueryDslAccessor;
import com.atlassian.jira.database.QueryDslAccessor;
import com.atlassian.jira.database.ResultRow;
import com.atlassian.jira.database.ResultRowBuilder;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoContainer;
import com.atlassian.jira.model.querydsl.JiraRelationalPathBase;
import com.atlassian.jira.propertyset.CachingOfBizPropertyEntryStore.CacheKey;
import com.atlassian.jira.propertyset.OfBizPropertyEntryStore.PropertyEntry;
import com.google.common.collect.ImmutableList;
import com.opensymphony.module.propertyset.PropertyImplementationException;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import javax.annotation.Nonnull;
import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

import static com.atlassian.jira.model.querydsl.QOSPropertyNumber.O_S_PROPERTY_NUMBER;
import static com.atlassian.jira.model.querydsl.QOSPropertyString.O_S_PROPERTY_STRING;
import static com.atlassian.jira.propertyset.CachingOfBizPropertyEntryStore.BATCH_SIZE;
import static com.opensymphony.module.propertyset.PropertySet.BOOLEAN;
import static com.opensymphony.module.propertyset.PropertySet.INT;
import static com.opensymphony.module.propertyset.PropertySet.LONG;
import static com.opensymphony.module.propertyset.PropertySet.STRING;
import static com.opensymphony.module.propertyset.PropertySet.TEXT;
import static org.apache.commons.lang.SerializationUtils.serialize;
import static org.apache.commons.lang3.SerializationUtils.deserialize;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.when;

/**
 * @since v6.2
 */
public class TestCachingOfBizPropertyEntryStore {
    private static final String ALL_IDS_AND_TYPES = "select O_S_PROPERTY_ENTRY.id, O_S_PROPERTY_ENTRY.propertytype\n" +
            "from propertyentry O_S_PROPERTY_ENTRY\n" +
            "where O_S_PROPERTY_ENTRY.entity_name = 'TestEntity1' and O_S_PROPERTY_ENTRY.entity_id = 1";
    private static final List<ResultRow> NO_ROWS = Collections.emptyList();

    private static final String TEST_ENTITY_1 = "TestEntity1";

    private static final Long TEST_ID_1 = 1L;

    private static final String TEST_KEY_1 = "TestKey1";
    private static final String TEST_KEY_2 = "TestKey2";

    private static final String VALUE1 = "Hello";

    @Rule
    public final MockitoContainer mockitoContainer = new MockitoContainer(this);

    @Rule
    public final ExpectedException thrown = ExpectedException.none();

    private final ConcurrentMap<CacheKey, AtomicInteger> loadCount = new ConcurrentHashMap<>(4);

    @AvailableInContainer(interfaceClass = QueryDslAccessor.class)
    private MockQueryDslAccessor queryDslAccessor = new MockQueryDslAccessor();

    @AvailableInContainer(interfaceClass = OfBizPropertyEntryStore.class)
    private CachingOfBizPropertyEntryStore fixture = new CachingOfBizPropertyEntryStore(queryDslAccessor,
            new MemoryCacheManager()) {
        @Override
        PropertySetData loadPropertySetData(@Nonnull CacheKey cacheKey) {
            loadCount.computeIfAbsent(cacheKey, key -> new AtomicInteger()).incrementAndGet();
            return super.loadPropertySetData(cacheKey);
        }
    };

    @Before
    public void setUp() {
        final AtomicLong counter = new AtomicLong(42L);
        when(queryDslAccessor.getMockDelegatorInterface().getNextSeqId("OSPropertyEntry"))
                .thenAnswer(x -> counter.getAndIncrement());
    }

    @After
    public void tearDown() {
        verifyDb();

        queryDslAccessor = null;
        fixture = null;
        loadCount.clear();
    }


    @Test
    public void getKeysSimpleCase() {
        mockKeysAndTypesQuery();
        assertThat(fixture.getKeys(TEST_ENTITY_1, TEST_ID_1), containsInAnyOrder(TEST_KEY_1, TEST_KEY_2));

        // Should be cached, now
        verifyDb();
        assertThat(fixture.getKeys(TEST_ENTITY_1, TEST_ID_1), containsInAnyOrder(TEST_KEY_1, TEST_KEY_2));
    }

    @Test
    public void getKeysOfType() {
        mockKeysAndTypesQuery();
        assertThat(fixture.getKeys(TEST_ENTITY_1, TEST_ID_1, TEXT), containsInAnyOrder(TEST_KEY_1));

        // Should be cached, now
        verifyDb();
        assertThat(fixture.getKeys(TEST_ENTITY_1, TEST_ID_1, STRING), containsInAnyOrder(TEST_KEY_2));
        assertThat(fixture.getKeys(TEST_ENTITY_1, TEST_ID_1, BOOLEAN), hasSize(0));
        assertThat(fixture.getKeys(TEST_ENTITY_1, TEST_ID_1, TEXT), containsInAnyOrder(TEST_KEY_1));
    }

    // TEXT values aren't bulk loaded, so this covers a different code path than the "normal" values test
    @Test
    public void duplicateKeysAndTextValuesAreResolvedUsingTheHighestId() {
        mockKeysAndTypesQuery(TEST_ENTITY_1, TEST_ID_1, new ResultRowBuilder()
                .addRow("dupe", TEXT)
                .addRow("dupe", STRING));

        // Highest ID was first, and it was a TEXT, so TEXT is what we should query, not STRING
        final String textQuery = textQuery("dupe");
        queryDslAccessor.setQueryResults(textQuery, new ResultRowBuilder()
                .addRow("hello")
                .addRow("world"));

        // Since the TEXT query uses ORDER BY ID DESC, the *first* value should win
        assertThat(fixture.getEntry(TEST_ENTITY_1, TEST_ID_1, "dupe"), isEntry(TEXT, "hello"));
    }

    @Test
    public void duplicateKeysAndNormalValuesAreResolvedUsingTheHighestId() {
        mockKeysAndTypesQuery(TEST_ENTITY_1, TEST_ID_1, new ResultRowBuilder()
                .addRow("dupe", STRING)
                .addRow("dupe", TEXT));

        // Highest ID was first, and it was a STRING, so STRING is what we should query, not TEXT
        final String stringQuery = valuesQuery(O_S_PROPERTY_STRING);
        queryDslAccessor.setQueryResults(stringQuery, new ResultRowBuilder()
                .addRow("dupe", "hello")
                .addRow("dupe", "world"));

        // Since the normal value query uses ORDER BY ID DESC, the *first* value should win
        assertThat(fixture.getEntry(TEST_ENTITY_1, TEST_ID_1, "dupe"), isEntry(STRING, "hello"));
    }

    @Test
    public void storeCanHandleNullValues() {
        mockKeysAndTypesQuery(TEST_ENTITY_1, TEST_ID_1, new ResultRowBuilder()
                .addRow("dupe", STRING)
                .addRow("dupe", TEXT));

        // Highest ID was first, and it was a STRING, so STRING is what we should query, not TEXT
        final String stringQuery = valuesQuery(O_S_PROPERTY_STRING);
        queryDslAccessor.setQueryResults(stringQuery, new ResultRowBuilder()
                .addRow("dupe", null)
                .addRow("dupe", "hello"));

        // Since the normal value query uses ORDER BY ID DESC, the *first* value should win
        assertThat(fixture.getEntry(TEST_ENTITY_1, TEST_ID_1, "dupe"), isEntry(STRING, null));
    }

    @Test
    public void getEntryWithCaching() {
        mockKeysAndTypesQuery(TEST_ENTITY_1, TEST_ID_1, new ResultRowBuilder()
                .addRow("string", STRING)
                .addRow("boolean", BOOLEAN)
                .addRow("int", INT)
                .addRow("zapped", LONG)
                .addRow("long", LONG)
                .addRow("text", TEXT));

        mockValuesQuery(O_S_PROPERTY_STRING, "string", "Hello");
        assertThat(fixture.getEntry(TEST_ENTITY_1, TEST_ID_1, "string"), isEntry(STRING, "Hello"));
        verifyDb();

        mockValuesQuery(O_S_PROPERTY_NUMBER, new ResultRowBuilder()
                .addRow("int", 42L)
                .addRow("boolean", 1L)
                .addRow("long", 2495L)
                .addRow("junk", 33L));

        assertThat(fixture.getEntry(TEST_ENTITY_1, TEST_ID_1, "int"), isEntry(INT, 42));
        verifyDb();

        // The first query for any of BOOLEAN, INT, or LONG loads all of those values, so these next two queries
        // queries should be served by the cache.
        assertThat(fixture.getEntry(TEST_ENTITY_1, TEST_ID_1, "boolean"), isEntry(BOOLEAN, true));
        assertThat(fixture.getEntry(TEST_ENTITY_1, TEST_ID_1, "long"), isEntry(LONG, 2495L));
        assertThat(fixture.getEntry(TEST_ENTITY_1, TEST_ID_1, "int"), isEntry(INT, 42));

        // We read a spurious value before, but that does not matter.  Since there was no entry for it before,
        // we do not attempt to map it to a value.
        assertThat(fixture.getEntry(TEST_ENTITY_1, TEST_ID_1, "junk"), nullValue());

        // We got an entry for this that should have been read in by now, but the value row either was already
        // missing or got deleted before we asked for it.
        assertThat(fixture.getEntry(TEST_ENTITY_1, TEST_ID_1, "zapped"), isEntry(LONG, null));

        final String textQuery = textQuery("text");

        queryDslAccessor.setQueryResults(textQuery, new ResultRowBuilder().addRow("hello"));
        assertThat(fixture.getEntry(TEST_ENTITY_1, TEST_ID_1, "text"), isEntry(TEXT, "hello"));
        verifyDb();

        // TEXT queries are cached, too
        assertThat(fixture.getEntry(TEST_ENTITY_1, TEST_ID_1, "text"), isEntry(TEXT, "hello"));
        verifyDb();
        assertThat(loadCount(TEST_ENTITY_1, TEST_ID_1), is(1));

        // But of course, if you reset the cache...
        fixture.refreshAll();
        mockKeysAndTypesQuery(TEST_ENTITY_1, TEST_ID_1, new ResultRowBuilder().addRow("text", TEXT));
        queryDslAccessor.setQueryResults(textQuery, new ResultRowBuilder().addRow("world"));
        assertThat(fixture.getEntry(TEST_ENTITY_1, TEST_ID_1, "text"), isEntry(TEXT, "world"));
        assertThat(loadCount(TEST_ENTITY_1, TEST_ID_1), is(2));
    }

    @Test
    public void removeNonExistingKey() {
        mockKeysAndTypesQuery();
        assertThat(fixture.getKeys(TEST_ENTITY_1, TEST_ID_1), containsInAnyOrder(TEST_KEY_1, TEST_KEY_2));
        assertThat(loadCount(TEST_ENTITY_1, TEST_ID_1), is(1));

        mockIdAndTypeQuery("DoesNotExist", NO_ROWS);

        fixture.removeEntry(TEST_ENTITY_1, TEST_ID_1, "DoesNotExist");
        verifyDb();

        assertThat(loadCount(TEST_ENTITY_1, TEST_ID_1), is(1));
        mockKeysAndTypesQuery();
        assertThat(fixture.getKeys(TEST_ENTITY_1, TEST_ID_1), containsInAnyOrder(TEST_KEY_1, TEST_KEY_2));
        assertThat(loadCount(TEST_ENTITY_1, TEST_ID_1), is(2));
    }

    @Test
    public void removeExistingKey() {
        // keys query is to make sure that remove clears the cache
        mockKeysAndTypesQuery();
        assertThat(fixture.getKeys(TEST_ENTITY_1, TEST_ID_1), containsInAnyOrder(TEST_KEY_1, TEST_KEY_2));
        assertThat(loadCount(TEST_ENTITY_1, TEST_ID_1), is(1));

        mockIdAndTypeQuery(TEST_KEY_1, 42L, TEXT);
        mockDeleteEntry(42L, TEXT, 1);
        mockDeleteValue("propertytext", 42L, 1);

        fixture.removeEntry(TEST_ENTITY_1, TEST_ID_1, TEST_KEY_1);

        verifyDb();

        mockKeysAndTypesQuery(new ResultRow(TEST_KEY_2, STRING));
        assertThat(fixture.getKeys(TEST_ENTITY_1, TEST_ID_1), containsInAnyOrder(TEST_KEY_2));
        assertThat(loadCount(TEST_ENTITY_1, TEST_ID_1), is(2));
    }

    @Test
    public void removeExistingKeyWithDuplicateEntries() {
        mockIdAndTypeQuery(TEST_KEY_1, new ResultRowBuilder()
                .addRow(43L, STRING)
                .addRow(42L, TEXT)
                .addRow(41L, TEXT)
                .addRow(40L, STRING));

        mockDeleteEntry(43L, 5, 1);

        // But it has to clean up the duplicates, too
        queryDslAccessor.setUpdateResults("delete from propertyentry\n" +
                        "where propertyentry.entity_name = 'TestEntity1' " +
                        "and propertyentry.entity_id = 1 " +
                        "and propertyentry.property_key = 'TestKey1' " +
                        "and propertyentry.id < 43",
                2);
        queryDslAccessor.setUpdateResults("delete from propertytext\n" +
                "where propertytext.id in (42, 41)", 2);
        queryDslAccessor.setUpdateResults("delete from propertystring\n" +
                "where propertystring.id in (43, 40)", 2);

        fixture.removeEntry(TEST_ENTITY_1, TEST_ID_1, TEST_KEY_1);
    }

    @Test
    public void removeExistingKeyWithFailedDelete() {
        mockKeysAndTypesQuery();
        assertThat(fixture.getKeys(TEST_ENTITY_1, TEST_ID_1), containsInAnyOrder(TEST_KEY_1, TEST_KEY_2));
        assertThat(loadCount(TEST_ENTITY_1, TEST_ID_1), is(1));

        mockIdAndTypeQuery(TEST_KEY_1, 42L, TEXT);
        mockDeleteEntry(42L, TEXT, 0);
        // Value doesn't get a remove because it is allegedly already gone

        try {
            fixture.removeEntry(TEST_ENTITY_1, TEST_ID_1, TEST_KEY_1);
            fail("Expected a PropertyImplementationException");
        } catch (PropertyImplementationException pie) {
            assertThat(pie.getMessage(), containsString("retries"));
        }

        verifyDb();

        mockKeysAndTypesQuery(new ResultRow(TEST_KEY_2, STRING));
        assertThat(fixture.getKeys(TEST_ENTITY_1, TEST_ID_1), containsInAnyOrder(TEST_KEY_2));
        assertThat(loadCount(TEST_ENTITY_1, TEST_ID_1), is(2));
    }

    @Test
    public void removePropertySetWithMultipleValues() {
        queryDslAccessor.setQueryResults(ALL_IDS_AND_TYPES, new ResultRowBuilder()
                .addRow(42L, STRING)
                .addRow(43L, STRING)
                .addRow(51L, LONG)
                .addRow(52L, LONG)
                .addRow(61L, TEXT)
                .addRow(74L, STRING));

        // Row count should not matter; we will delete all of the value entries regardless
        queryDslAccessor.setUpdateResults("delete from propertyentry\n" +
                "where propertyentry.entity_name = 'TestEntity1' and propertyentry.entity_id = 1", 0);

        queryDslAccessor.setUpdateResults("delete from propertystring\n" +
                "where propertystring.id in (42, 43, 74)", 0);
        queryDslAccessor.setUpdateResults("delete from propertynumber\n" +
                "where propertynumber.id in (51, 52)", 0);
        queryDslAccessor.setUpdateResults("delete from propertytext\n" +
                "where propertytext.id = 61", 0);

        fixture.removePropertySet(TEST_ENTITY_1, TEST_ID_1);
    }

    @Test
    public void removePropertySetWithBatching() {
        // If the property set has more than the batch limit values of a given type, then list should be split up.
        // But let's also include a few values of a different type.
        final ResultRowBuilder entries = new ResultRowBuilder()
                .addRow(10051L, LONG)
                .addRow(10052L, LONG)
                .addRow(10061L, TEXT);
        final long total = 2 * BATCH_SIZE + 3;
        for (long id = 1L; id <= total; ++id) {
            entries.addRow(id, STRING);
        }
        queryDslAccessor.setQueryResults(ALL_IDS_AND_TYPES, entries);

        // There's the entries themselves...
        queryDslAccessor.setUpdateResults("delete from propertyentry\n" +
                "where propertyentry.entity_name = 'TestEntity1' and propertyentry.entity_id = 1", 0);

        // And then there's the easy values...
        queryDslAccessor.setUpdateResults("delete from propertynumber\n" +
                "where propertynumber.id in (10051, 10052)", 0);
        queryDslAccessor.setUpdateResults("delete from propertytext\n" +
                "where propertytext.id = 10061", 0);

        // But then the trouble starts...
        queryDslAccessor.setUpdateResults("delete from propertystring\n" +
                "where propertystring.id " + in(1L, BATCH_SIZE), 0);
        queryDslAccessor.setUpdateResults("delete from propertystring\n" +
                "where propertystring.id " + in(BATCH_SIZE + 1L, 2 * BATCH_SIZE), 0);
        queryDslAccessor.setUpdateResults("delete from propertystring\n" +
                "where propertystring.id " + in(2 * BATCH_SIZE + 1L, 2 * BATCH_SIZE + 3L), 0);

        fixture.removePropertySet(TEST_ENTITY_1, TEST_ID_1);
    }

    @Test
    public void createNewProperty() {
        mockIdAndTypeQuery(TEST_KEY_1, NO_ROWS);

        queryDslAccessor.setUpdateResults("insert into propertyentry (entity_name, entity_id, property_key, propertytype, id)\n" +
                "values ('TestEntity1', 1, 'TestKey1', 6, 42)", 1);
        queryDslAccessor.setUpdateResults("insert into propertytext (id, propertyvalue)\n" +
                "values (42, 'Hello')", 1);
        fixture.setEntry(TEST_ENTITY_1, TEST_ID_1, TEST_KEY_1, TEXT, VALUE1);
    }

    @Test
    public void replaceExistingPropertyWithSameType() {
        mockIdAndTypeQuery(TEST_KEY_1, 42L, TEXT);

        queryDslAccessor.setUpdateResults("update propertytext\n" +
                "set propertyvalue = 'Hello'\n" +
                "where propertytext.id = 42", 1);

        fixture.setEntry(TEST_ENTITY_1, TEST_ID_1, TEST_KEY_1, TEXT, VALUE1);
    }

    @Test
    public void replaceExistingPropertyWithSameTypeButMissingValue() {
        mockIdAndTypeQuery(TEST_KEY_1, 42L, TEXT);

        queryDslAccessor.setUpdateResults("update propertytext\n" +
                        "set propertyvalue = 'Hello'\n" +
                        "where propertytext.id = 42",
                0);

        // If the update returns 0 rows, then we insert instead.
        // That should always succeed because we have locked entry in the DB is supposedly row locked.
        queryDslAccessor.setUpdateResults("insert into propertytext (id, propertyvalue)\n" +
                        "values (42, 'Hello')",
                1);

        fixture.setEntry(TEST_ENTITY_1, TEST_ID_1, TEST_KEY_1, TEXT, VALUE1);
    }

    @Test
    public void replaceExistingPropertyWithDifferentType() {
        mockIdAndTypeQuery(TEST_KEY_1, 42L, STRING);

        queryDslAccessor.setUpdateResults("update propertyentry\n" +
                "set propertytype = 6\n" +
                "where propertyentry.id = 42 and propertyentry.propertytype = 5", 1);
        queryDslAccessor.setUpdateResults("delete from propertystring\n" +
                "where propertystring.id = 42", 1);
        queryDslAccessor.setUpdateResults("insert into propertytext (id, propertyvalue)\n" +
                "values (42, 'Hello')", 1);

        fixture.setEntry(TEST_ENTITY_1, TEST_ID_1, TEST_KEY_1, TEXT, VALUE1);
    }

    @Test
    public void replaceExistingPropertyWithDifferentTypeLostFirstRace() {
        mockIdAndTypeQuery(TEST_KEY_1, 42L, STRING);

        final String updateEntryFromStringToText = "update propertyentry\n" +
                "set propertytype = 6\n" +
                "where propertyentry.id = 42 and propertyentry.propertytype = 5";
        queryDslAccessor.setUpdateResults(updateEntryFromStringToText, 0);
        queryDslAccessor.onSqlListener(updateEntryFromStringToText, () -> {
            // On the second attempt, show that the value is now a long and that this is what needs to get the update
            mockIdAndTypeQuery(TEST_KEY_1, 42L, LONG);
            queryDslAccessor.onSqlListener(updateEntryFromStringToText, () -> fail("update used stale info"));
            queryDslAccessor.setUpdateResults("update propertyentry\n" +
                    "set propertytype = 6\n" +
                    "where propertyentry.id = 42 and propertyentry.propertytype = 3", 1);
            queryDslAccessor.setUpdateResults("delete from propertynumber\n" +
                    "where propertynumber.id = 42", 1);
            queryDslAccessor.setUpdateResults("insert into propertytext (id, propertyvalue)\n" +
                    "values (42, 'Hello')", 1);
        });

        fixture.setEntry(TEST_ENTITY_1, TEST_ID_1, TEST_KEY_1, TEXT, VALUE1);
    }

    @Test
    public void replaceExistingPropertyWhenItHasDuplicateEntries() {
        mockIdAndTypeQuery(TEST_KEY_1, new ResultRowBuilder()
                .addRow(43L, STRING)
                .addRow(42L, STRING)
                .addRow(41L, TEXT)
                .addRow(40L, TEXT));

        // The update logic should UPDATE the type of the highest ID first to lock the entry
        queryDslAccessor.setUpdateResults("update propertyentry\n" +
                "set propertytype = 6\n" +
                "where propertyentry.id = 43 and propertyentry.propertytype = 5", 1);

        // Then DELETE/INSERT the value, since the type changed
        queryDslAccessor.setUpdateResults("delete from propertystring\n" +
                "where propertystring.id = 43", 1);
        queryDslAccessor.setUpdateResults("insert into propertytext (id, propertyvalue)\n" +
                "values (43, 'Hello')", 1);

        // Then DELETE the duplicate entries to get rid of them
        queryDslAccessor.setUpdateResults("delete from propertyentry\n" +
                "where propertyentry.entity_name = 'TestEntity1' " +
                "and propertyentry.entity_id = 1 " +
                "and propertyentry.property_key = 'TestKey1' " +
                "and propertyentry.id < 43", 2);

        // Then DELETE the duplicate values that match those entries
        queryDslAccessor.setUpdateResults("delete from propertystring\n" +
                "where propertystring.id = 42", 1);
        queryDslAccessor.setUpdateResults("delete from propertytext\n" +
                "where propertytext.id in (41, 40)", 2);

        fixture.setEntry(TEST_ENTITY_1, TEST_ID_1, TEST_KEY_1, TEXT, VALUE1);
    }

    @Test
    public void cacheKeyShouldBeSerializable() {
        final Serializable cacheKey = new CacheKey("foo", 123L);

        final Object roundTripped = deserialize(serialize(cacheKey));

        assertEquals(cacheKey, roundTripped);
        assertNotSame(cacheKey, roundTripped);
    }


    // The "keys and types" query is used for the initial read from the property set and (as you might guess)
    // populates the cached state with all of the property set's existing keys and their types.

    private void mockKeysAndTypesQuery() {
        mockKeysAndTypesQuery(TEST_ENTITY_1, TEST_ID_1, new ResultRowBuilder()
                .addRow(TEST_KEY_1, TEXT)
                .addRow(TEST_KEY_2, STRING));
    }

    private void mockKeysAndTypesQuery(ResultRow row) {
        mockKeysAndTypesQuery(TEST_ENTITY_1, TEST_ID_1, ImmutableList.of(row));
    }

    private void mockKeysAndTypesQuery(String entityName, long entityId, Iterable<ResultRow> rows) {
        queryDslAccessor.setQueryResults("select O_S_PROPERTY_ENTRY.property_key, O_S_PROPERTY_ENTRY.propertytype\n" +
                        "from propertyentry O_S_PROPERTY_ENTRY\n" +
                        "where O_S_PROPERTY_ENTRY.entity_name = '" + entityName +
                        "' and O_S_PROPERTY_ENTRY.entity_id = " + entityId +
                        "\norder by O_S_PROPERTY_ENTRY.id desc",
                rows);
    }

    // The id and type query is used to check the existence and type of a single entry in preparation for modifying
    // it, whether to set its value or delete it.
    private void mockIdAndTypeQuery(String propertyKey, Iterable<ResultRow> rows) {
        queryDslAccessor.setQueryResults(idAndTypeQuery(propertyKey), rows);
    }

    private void mockIdAndTypeQuery(String propertyKey, ResultRow row) {
        mockIdAndTypeQuery(propertyKey, ImmutableList.of(row));
    }

    private void mockIdAndTypeQuery(String propertyKey, long id, int type) {
        mockIdAndTypeQuery(propertyKey, new ResultRow(id, type));
    }

    private static String idAndTypeQuery(String propertyKey) {
        return "select O_S_PROPERTY_ENTRY.id, O_S_PROPERTY_ENTRY.propertytype\n" +
                "from propertyentry O_S_PROPERTY_ENTRY\n" +
                "where O_S_PROPERTY_ENTRY.entity_name = 'TestEntity1' " +
                "and O_S_PROPERTY_ENTRY.entity_id = 1 " +
                "and O_S_PROPERTY_ENTRY.property_key = '" + propertyKey + "'\n" +
                "order by O_S_PROPERTY_ENTRY.id desc\n" +
                "for update";
    }

    private void mockValuesQuery(JiraRelationalPathBase<?> valueTable, String key, Object value) {
        mockValuesQuery(valueTable, new ResultRowBuilder().addRow(key, value));
    }

    private void mockValuesQuery(JiraRelationalPathBase<?> valueTable, Iterable<ResultRow> rows) {
        queryDslAccessor.setQueryResults(valuesQuery(valueTable), rows);
    }

    private static String valuesQuery(JiraRelationalPathBase<?> valueTable) {
        final String tableName = valueTable.getTableName();
        final String tableAlias = valueTable.getMetadata().getName();
        return "select O_S_PROPERTY_ENTRY.property_key, " + tableAlias + ".propertyvalue\n" +
                "from propertyentry O_S_PROPERTY_ENTRY\n" +
                "join " + tableName + ' ' + tableAlias + '\n' +
                "on O_S_PROPERTY_ENTRY.id = " + tableAlias + ".id\n" +
                "where O_S_PROPERTY_ENTRY.entity_name = 'TestEntity1' and O_S_PROPERTY_ENTRY.entity_id = 1\n" +
                "order by O_S_PROPERTY_ENTRY.id desc";
    }

    private static String textQuery(String key) {
        return "select O_S_PROPERTY_TEXT.propertyvalue\n" +
                "from propertyentry O_S_PROPERTY_ENTRY\n" +
                "join propertytext O_S_PROPERTY_TEXT\n" +
                "on O_S_PROPERTY_ENTRY.id = O_S_PROPERTY_TEXT.id\n" +
                "where O_S_PROPERTY_ENTRY.entity_name = 'TestEntity1' " +
                "and O_S_PROPERTY_ENTRY.entity_id = 1 " +
                "and O_S_PROPERTY_ENTRY.property_key = '" + key + "'\n" +
                "order by O_S_PROPERTY_ENTRY.id desc";
    }

    private void mockDeleteEntry(long id, int type, int rows) {
        queryDslAccessor.setUpdateResults("delete from propertyentry\n" +
                        "where propertyentry.id = " + id +
                        " and propertyentry.propertytype = " + type,
                rows);
    }

    private void mockDeleteValue(String table, long id, int rows) {
        queryDslAccessor.setUpdateResults("delete from " + table + "\n" +
                        "where " + table + ".id = " + id,
                rows);

    }


    /**
     * Assert that all expected DB queries have run and clear the state so that any further queries will fail.
     */
    private void verifyDb() {
        queryDslAccessor.assertAllExpectedStatementsWereRun();
        queryDslAccessor.reset();
    }

    private int loadCount(String entityName, long entityId) {
        final AtomicInteger count = loadCount.get(new CacheKey(entityName, entityId));
        return (count != null) ? count.get() : 0;
    }

    private static String in(long first, long last) {
        final StringBuilder sb = new StringBuilder(1024).append("in (");
        for (long id = first; id <= last; ++id) {
            sb.append(id).append(", ");
        }
        return sb.replace(sb.length() - 2, sb.length(), ")").toString();
    }

    private static Matcher<PropertyEntry> isEntry(int type, Object value) {
        return new PropertyEntryMatcher(type, value);
    }

    static class PropertyEntryMatcher extends TypeSafeMatcher<PropertyEntry> {
        private final int expectedType;
        private final Object expectedValue;

        PropertyEntryMatcher(int expectedType, Object expectedValue) {
            this.expectedType = expectedType;
            this.expectedValue = expectedValue;
        }

        @Override
        protected boolean matchesSafely(PropertyEntry propertyEntry) {
            return propertyEntry.getType() == expectedType &&
                    Objects.equals(propertyEntry.getValue(), expectedValue);
        }

        @Override
        public void describeTo(Description description) {
            description.appendText("PropertyEntry[type=" + expectedType + ",value=" + expectedValue + ']');
        }
    }
}

