package com.atlassian.jira.issue.search.searchers.renderer;

import org.junit.Test;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;

public class TestOptionWithValidity {

    @Test
    public void shouldGetNoCssClassIfValid() {
        assertThat(new OptionWithValidity("id", "name", true).getCssClass(), nullValue());
    }

    @Test
    public void shouldGetInvalidSelCssClassIfNotValid() {
        assertThat(new OptionWithValidity("id", "name", false).getCssClass(), is("invalid_sel"));
    }

}