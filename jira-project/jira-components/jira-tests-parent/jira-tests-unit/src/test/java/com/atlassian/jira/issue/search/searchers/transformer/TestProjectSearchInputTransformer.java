package com.atlassian.jira.issue.search.searchers.transformer;

import com.atlassian.core.util.collection.EasyList;
import com.atlassian.core.util.map.EasyMap;
import com.atlassian.jira.issue.IssueFieldConstants;
import com.atlassian.jira.issue.search.ClauseNames;
import com.atlassian.jira.issue.search.SearchContext;
import com.atlassian.jira.issue.search.constants.SystemSearchConstants;
import com.atlassian.jira.issue.search.searchers.util.IndexedInputHelper;
import com.atlassian.jira.issue.transport.impl.ActionParamsImpl;
import com.atlassian.jira.issue.transport.impl.FieldValuesHolderImpl;
import com.atlassian.jira.jql.operand.JqlOperandResolver;
import com.atlassian.jira.jql.resolver.ProjectIndexInfoResolver;
import com.atlassian.jira.jql.validator.MockJqlOperandResolver;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.UserProjectHistoryManager;
import com.atlassian.jira.util.collect.CollectionBuilder;
import com.atlassian.query.Query;
import com.atlassian.query.QueryImpl;
import com.atlassian.query.clause.AndClause;
import com.atlassian.query.clause.Clause;
import com.atlassian.query.clause.TerminalClauseImpl;
import com.atlassian.query.operand.MultiValueOperand;
import com.atlassian.query.operand.SingleValueOperand;
import com.atlassian.query.operator.Operator;
import com.google.common.collect.ImmutableSet;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestProjectSearchInputTransformer {
    @Rule
    public MockitoRule mockito = MockitoJUnit.rule();

    private static final String PROJECT_KEY = SystemSearchConstants.forProject().getUrlParameter();

    @Mock
    private JiraAuthenticationContext authenticationContext;
    @Mock
    private UserProjectHistoryManager projectHistoryManager;
    @Mock
    private ProjectManager projectManager;
    @Mock
    private SearchContext searchContext;
    private ApplicationUser theUser;

    @Before
    public void setUp() throws Exception {
        theUser = new MockApplicationUser("admin");
    }

    @Test
    public void testPopulateFromParams() throws Exception {
        final FieldFlagOperandRegistry mockFieldFlagOperandRegistry = mock(FieldFlagOperandRegistry.class);

        SearchInputTransformer transformer = new ProjectSearchInputTransformer(null, null, mockFieldFlagOperandRegistry, projectManager, projectHistoryManager, authenticationContext);
        final FieldValuesHolderImpl valuesHolder = new FieldValuesHolderImpl();
        final String[] values = {"val1", "val2"};
        final ActionParamsImpl actionParams = new ActionParamsImpl(EasyMap.build(PROJECT_KEY, values));
        transformer.populateFromParams(null, valuesHolder, actionParams);
        assertEquals(Arrays.asList(values), valuesHolder.get(PROJECT_KEY));
    }

    @Test
    public void testPopulateFromParamsDuplicatesValues() throws Exception {
        final FieldFlagOperandRegistry mockFieldFlagOperandRegistry = mock(FieldFlagOperandRegistry.class);

        SearchInputTransformer transformer = new ProjectSearchInputTransformer(null, null, mockFieldFlagOperandRegistry, projectManager, projectHistoryManager, authenticationContext);
        final FieldValuesHolderImpl valuesHolder = new FieldValuesHolderImpl();
        final String[] values = {"val1", "val1"};
        final ActionParamsImpl actionParams = new ActionParamsImpl(EasyMap.build(PROJECT_KEY, values));
        transformer.populateFromParams(null, valuesHolder, actionParams);
        assertEquals(Arrays.asList(new String[]{"val1",}), valuesHolder.get(PROJECT_KEY));
    }

    @Test
    public void testPopulateFromSearchRequestEmptyMatchingValues() throws Exception {
        final ProjectIndexInfoResolver mockProjectIndexInfoResolver = mock(ProjectIndexInfoResolver.class);

        final FieldFlagOperandRegistry mockFieldFlagOperandRegistry = mock(FieldFlagOperandRegistry.class);

        final IndexedInputHelper indexedInputHelper = mock(IndexedInputHelper.class);
        when(indexedInputHelper.getAllNavigatorValuesForMatchingClauses(theUser, SystemSearchConstants.forProject().getJqlClauseNames(), new QueryImpl())).thenReturn(ImmutableSet.of());

        final SearchInputTransformer transformer = new ProjectSearchInputTransformer(mockProjectIndexInfoResolver, MockJqlOperandResolver.createSimpleSupport(), mockFieldFlagOperandRegistry, projectManager, projectHistoryManager, authenticationContext) {

            @Override
            IndexedInputHelper createIndexedInputHelper() {
                return indexedInputHelper;
            }
        };
        final FieldValuesHolderImpl valuesHolder = new FieldValuesHolderImpl();
        transformer.populateFromQuery(theUser, valuesHolder, new QueryImpl(), searchContext);
        assertNull(valuesHolder.get(PROJECT_KEY));
    }

    @Test
    public void testPopulateFromSearchRequestHappyPath() throws Exception {
        final ProjectIndexInfoResolver mockProjectIndexInfoResolver = mock(ProjectIndexInfoResolver.class);
        final FieldFlagOperandRegistry mockFieldFlagOperandRegistry = mock(FieldFlagOperandRegistry.class);
        final IndexedInputHelper indexedInputHelper = mock(IndexedInputHelper.class);
        when(indexedInputHelper.getAllNavigatorValuesForMatchingClauses(theUser, new ClauseNames(IssueFieldConstants.PROJECT), new QueryImpl())).thenReturn(CollectionBuilder.newBuilder("1", "2").asSortedSet());

        final SearchInputTransformer transformer = new ProjectSearchInputTransformer(mockProjectIndexInfoResolver, MockJqlOperandResolver.createSimpleSupport(), mockFieldFlagOperandRegistry, projectManager, projectHistoryManager, authenticationContext) {

            @Override
            IndexedInputHelper createIndexedInputHelper() {
                return indexedInputHelper;
            }
        };
        final FieldValuesHolderImpl valuesHolder = new FieldValuesHolderImpl();
        transformer.populateFromQuery(theUser, valuesHolder, new QueryImpl(), searchContext);
        assertEquals(EasyList.build("1", "2"), valuesHolder.get(PROJECT_KEY));
    }

    @Test
    public void testGetSearchClauseHappyPath() throws Exception {
        final FieldValuesHolderImpl valuesHolder = new FieldValuesHolderImpl();
        valuesHolder.put(PROJECT_KEY, EasyList.build("1"));

        when(projectManager.getProjectObj(1L)).thenReturn(new MockProject(1L, "MKY"));

        final FieldFlagOperandRegistry mockFieldFlagOperandRegistry = mock(FieldFlagOperandRegistry.class);

        final ProjectSearchInputTransformer transformer = new ProjectSearchInputTransformer(null, null, mockFieldFlagOperandRegistry, projectManager, projectHistoryManager, authenticationContext);
        Clause result = transformer.getSearchClause(null, valuesHolder);
        assertEquals(new TerminalClauseImpl(SystemSearchConstants.forProject().getJqlClauseNames().getPrimaryName(), Operator.EQUALS, "MKY"), result);
    }

    @Test
    public void testGetSearchClauseSomeNumbersSomeNot() throws Exception {
        final FieldValuesHolderImpl valuesHolder = new FieldValuesHolderImpl();
        valuesHolder.put(PROJECT_KEY, CollectionBuilder.newBuilder("1", "frank").asList());

        when(projectManager.getProjectObj(1L)).thenReturn(new MockProject(1L, "MKY"));

        final FieldFlagOperandRegistry mockFieldFlagOperandRegistry = mock(FieldFlagOperandRegistry.class);

        final ProjectSearchInputTransformer transformer = new ProjectSearchInputTransformer(null, null, mockFieldFlagOperandRegistry, projectManager, projectHistoryManager, authenticationContext);
        Clause result = transformer.getSearchClause(null, valuesHolder);
        final Clause expectedClause = new TerminalClauseImpl(SystemSearchConstants.forProject().getJqlClauseNames().getPrimaryName(), Operator.IN, new MultiValueOperand("MKY", "frank"));
        assertEquals(expectedClause, result);
    }

    @Test
    public void testGetSearchClauseSomeValidIdsSomeNot() throws Exception {
        final FieldValuesHolderImpl valuesHolder = new FieldValuesHolderImpl();
        valuesHolder.put(PROJECT_KEY, CollectionBuilder.newBuilder("1", "2").asList());

        when(projectManager.getProjectObj(1L)).thenReturn(new MockProject(1L, "MKY"));

        when(projectManager.getProjectObj(2L)).thenReturn(null);

        final FieldFlagOperandRegistry mockFieldFlagOperandRegistry = mock(FieldFlagOperandRegistry.class);

        final ProjectSearchInputTransformer transformer = new ProjectSearchInputTransformer(null, null, mockFieldFlagOperandRegistry, projectManager, projectHistoryManager, authenticationContext);
        Clause result = transformer.getSearchClause(null, valuesHolder);
        final Clause expectedClause = new TerminalClauseImpl(SystemSearchConstants.forProject().getJqlClauseNames().getPrimaryName(), Operator.IN, new MultiValueOperand(new SingleValueOperand("MKY"), new SingleValueOperand(2L)));
        assertEquals(expectedClause, result);
    }

    @Test
    public void testGetSearchClauseInOperator() throws Exception {
        final FieldValuesHolderImpl valuesHolder = new FieldValuesHolderImpl();
        valuesHolder.put(PROJECT_KEY, EasyList.build("1", "2"));

        when(projectManager.getProjectObj(1L)).thenReturn(new MockProject(1L, "MKY"));

        when(projectManager.getProjectObj(2L)).thenReturn(new MockProject(2L, "HSP"));

        final FieldFlagOperandRegistry mockFieldFlagOperandRegistry = mock(FieldFlagOperandRegistry.class);

        final ProjectSearchInputTransformer transformer = new ProjectSearchInputTransformer(null, null, mockFieldFlagOperandRegistry, projectManager, projectHistoryManager, authenticationContext);
        Clause result = transformer.getSearchClause(null, valuesHolder);

        MultiValueOperand multiValueOperand = new MultiValueOperand("MKY", "HSP");

        assertEquals(new TerminalClauseImpl(SystemSearchConstants.forProject().getJqlClauseNames().getPrimaryName(), Operator.IN, multiValueOperand), result);
    }

    @Test
    public void testGetSearchClauseNoProjects() throws Exception {
        final FieldValuesHolderImpl valuesHolder = new FieldValuesHolderImpl();
        valuesHolder.put(PROJECT_KEY, EasyList.build());

        final FieldFlagOperandRegistry mockFieldFlagOperandRegistry = mock(FieldFlagOperandRegistry.class);

        final ProjectSearchInputTransformer transformer = new ProjectSearchInputTransformer(null, null, mockFieldFlagOperandRegistry, projectManager, projectHistoryManager, authenticationContext);
        Clause result = transformer.getSearchClause(null, valuesHolder);
        assertNull(result);
    }

    @Test
    public void testsetProjectIdInSessionOnlyOne() throws Exception {

        final FieldFlagOperandRegistry mockFieldFlagOperandRegistry = mock(FieldFlagOperandRegistry.class);

        final Project project = new MockProject(1L);
        when(projectManager.getProjectObj(1L)).thenReturn(project);
        when(authenticationContext.getUser()).thenReturn(theUser);

        final ProjectSearchInputTransformer transformer = new ProjectSearchInputTransformer(null, null, mockFieldFlagOperandRegistry, projectManager, projectHistoryManager, authenticationContext);
        transformer.setProjectIdInSession(CollectionBuilder.newBuilder("1").asSet());

        verify(projectHistoryManager).addProjectToHistory(theUser, project);
    }

    @Test
    public void testsetProjectIdInSessionMoreThanOne() throws Exception {

        final FieldFlagOperandRegistry mockFieldFlagOperandRegistry = mock(FieldFlagOperandRegistry.class);

        final ProjectSearchInputTransformer transformer = new ProjectSearchInputTransformer(null, null, mockFieldFlagOperandRegistry, projectManager, projectHistoryManager, authenticationContext);
        transformer.setProjectIdInSession(CollectionBuilder.newBuilder("1", "2").asSet());
    }

    @Test
    public void testValidateForNavigatorHappyPath() throws Exception {
        AndClause andClause = new AndClause(new TerminalClauseImpl(SystemSearchConstants.forProject().getJqlClauseNames().getPrimaryName(), Operator.EQUALS, "value"),
                new TerminalClauseImpl("other", Operator.EQUALS, "valueother"));

        final FieldFlagOperandRegistry fieldFlagOperandRegistry = mock(FieldFlagOperandRegistry.class);
        final JqlOperandResolver mockJqlOperandResolver = mock(JqlOperandResolver.class);

        ProjectSearchInputTransformer transformer = new ProjectSearchInputTransformer(null, null, null, projectManager, projectHistoryManager, authenticationContext) {
            @Override
            NavigatorStructureChecker<Project> createNavigatorStructureChecker() {
                return new NavigatorStructureChecker<Project>(SystemSearchConstants.forProject().getJqlClauseNames(), false, fieldFlagOperandRegistry, mockJqlOperandResolver) {
                    @Override
                    public boolean checkSearchRequest(final Query query) {
                        return true;
                    }
                };
            }
        };

        assertValidate(andClause, transformer, true);
    }

    @Test
    public void testValidateForNavigatorSadPath() throws Exception {
        AndClause andClause = new AndClause(new TerminalClauseImpl(SystemSearchConstants.forProject().getJqlClauseNames().getPrimaryName(), Operator.EQUALS, "value"),
                new TerminalClauseImpl("other", Operator.EQUALS, "valueother"));

        final FieldFlagOperandRegistry fieldFlagOperandRegistry = mock(FieldFlagOperandRegistry.class);
        final JqlOperandResolver mockJqlOperandResolver = mock(JqlOperandResolver.class);

        ProjectSearchInputTransformer transformer = new ProjectSearchInputTransformer(null, null, null, projectManager, projectHistoryManager, authenticationContext) {
            @Override
            NavigatorStructureChecker<Project> createNavigatorStructureChecker() {
                return new NavigatorStructureChecker<Project>(SystemSearchConstants.forProject().getJqlClauseNames(), false, fieldFlagOperandRegistry, mockJqlOperandResolver) {
                    @Override
                    public boolean checkSearchRequest(final Query query) {
                        return false;
                    }
                };
            }
        };

        assertValidate(andClause, transformer, false);
    }

    private void assertValidate(final AndClause andClause, final ProjectSearchInputTransformer transformer, final boolean isClauseValid) {
        assertEquals(isClauseValid, transformer.doRelevantClausesFitFilterForm(null, new QueryImpl(andClause), searchContext));
    }
}
