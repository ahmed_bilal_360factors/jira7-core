package com.atlassian.jira.issue.statistics;

import com.atlassian.jira.action.issue.customfields.option.MockOption;
import com.atlassian.jira.issue.customfields.converters.SelectConverter;
import com.atlassian.jira.issue.customfields.converters.SelectConverterImpl;
import com.atlassian.jira.issue.customfields.option.Option;
import com.atlassian.jira.issue.customfields.searchers.transformer.CustomFieldInputHelper;
import com.atlassian.jira.issue.customfields.statistics.GroupPickerStatisticsMapper;
import com.atlassian.jira.issue.customfields.statistics.ProjectSelectStatisticsMapper;
import com.atlassian.jira.issue.customfields.statistics.SelectStatisticsMapper;
import com.atlassian.jira.issue.customfields.statistics.TextStatisticsMapper;
import com.atlassian.jira.issue.customfields.statistics.UserPickerStatisticsMapper;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.search.ClauseNames;
import com.atlassian.jira.issue.statistics.util.ComparatorSelector;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.google.common.collect.ImmutableList;
import org.junit.Test;

import java.util.Comparator;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestSelectStatisticsMapper {
    @Test
    public void testComparator() throws Exception {
        final SelectStatisticsMapper mapper = new SelectStatisticsMapper(mock(CustomField.class), mock(SelectConverter.class), mock(JiraAuthenticationContext.class), mock(CustomFieldInputHelper.class));
        final Comparator<Option> comparator = ComparatorSelector.getComparator(mapper);

        // Create two mocks. Their value is the same "Green" but other identifying marks are different.
        // We should "compareTo" based on value, not id or sequence or anything else.

        final MockOption o1 = new MockOption(mock(Option.class), ImmutableList.of(mock(Option.class)), 1L, "Green", null, 2L);
        final MockOption o2 = new MockOption(mock(Option.class), ImmutableList.of(mock(Option.class)), 3L, "Green", null, 4L);

        assertEquals(0, comparator.compare(o1, o2));
    }

    @Test
    public void testEquals() {
        final CustomField mockCustomField = mock(CustomField.class);
        final String cfId = "customfield_10001";
        when(mockCustomField.getId()).thenReturn(cfId);

        final ClauseNames clauseNames = new ClauseNames("clauseName");
        when(mockCustomField.getClauseNames()).thenReturn(clauseNames);

        final JiraAuthenticationContext authenticationContext = mock(JiraAuthenticationContext.class);
        final CustomFieldInputHelper customFieldInputHelper = mock(CustomFieldInputHelper.class);

        final SelectStatisticsMapper mapper = new SelectStatisticsMapper(mockCustomField, new SelectConverterImpl(null), authenticationContext, customFieldInputHelper);

        // identity test
        assertThat(mapper, equalTo(mapper));
        assertEquals(mapper.hashCode(), mapper.hashCode());

        final CustomField mockCustomField2 = mock(CustomField.class);
        when(mockCustomField2.getId()).thenReturn(cfId);
        when(mockCustomField2.getClauseNames()).thenReturn(clauseNames);

        final SelectStatisticsMapper mapper2 = new SelectStatisticsMapper(mockCustomField2, new SelectConverterImpl(null), authenticationContext, customFieldInputHelper);

        // As the mappers are using the same custom field they should be equal
        assertThat(mapper2, equalTo(mapper));
        assertEquals(mapper.hashCode(), mapper2.hashCode());

        final CustomField mockCustomField3 = mock(CustomField.class);

        when(mockCustomField3.getId()).thenReturn("customfield_10002");
        when(mockCustomField3.getClauseNames()).thenReturn(clauseNames);

        final SelectStatisticsMapper mapper3 = new SelectStatisticsMapper(mockCustomField3, new SelectConverterImpl(null), authenticationContext, customFieldInputHelper);

        // As the mappers are using different custom field they should *not* be equal
        assertThat(mapper3, not(equalTo(mapper)));
        assertThat(mapper3.hashCode(), not(equalTo(mapper.hashCode())));

        assertThat(mapper, not(equalTo(null)));
        assertThat(mapper, not(equalTo(new Object())));
        assertThat(mapper, not(equalTo(new IssueKeyStatisticsMapper())));
        assertThat(mapper, not(equalTo(new IssueTypeStatisticsMapper(null))));

        // ensure other implmentations of same base class are not equal even though they both extend
        // they inherit the equals and hashCode implementations
        assertThat(mapper, not(equalTo(new UserPickerStatisticsMapper(mockCustomField, null, null))));
        assertThat(mapper, not(equalTo(new TextStatisticsMapper(mockCustomField))));
        assertThat(mapper, not(equalTo(new ProjectSelectStatisticsMapper(mockCustomField, null))));
        assertThat(mapper, not(equalTo(new GroupPickerStatisticsMapper(mockCustomField, null, authenticationContext, customFieldInputHelper))));
    }
}
