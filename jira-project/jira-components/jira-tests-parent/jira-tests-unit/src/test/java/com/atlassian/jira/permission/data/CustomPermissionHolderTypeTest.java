package com.atlassian.jira.permission.data;

import com.atlassian.jira.permission.PermissionHolderType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static com.atlassian.jira.permission.JiraPermissionHolderType.ASSIGNEE;
import static com.atlassian.jira.permission.JiraPermissionHolderType.USER;
import static com.atlassian.jira.permission.data.CustomPermissionHolderType.permissionHolderType;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(MockitoJUnitRunner.class)
public class CustomPermissionHolderTypeTest {

    private static final String NEW_KEY = "key";
    private static final String PARAM_VALUE = "param";
    private static final String EMPTY_PARAM = null;

    @Test
    public void typeThatAlreadyExistsShouldReturnExistingType() {
        PermissionHolderType typeRequiringParam = USER;
        PermissionHolderType typeNotRequiringParam = ASSIGNEE;

        PermissionHolderType permHolderTypeRequringParameter = permissionHolderType(typeRequiringParam.getKey(), PARAM_VALUE);
        PermissionHolderType permHolderTypeNotRequringParameter = permissionHolderType(typeNotRequiringParam.getKey(), EMPTY_PARAM);

        assertThat("Created type should be the same as existing type (parameter required)",
                permHolderTypeRequringParameter, equalTo(typeRequiringParam));

        assertThat("Created type should be the same as existing type (parameter not required)",
                permHolderTypeNotRequringParameter, equalTo(typeNotRequiringParam));
    }

    @Test
    public void newTypeWithEmptyParameterShouldNotRequireParameter() {
        PermissionHolderType permHolderType = permissionHolderType(NEW_KEY, "");

        assertThat("Created type should have the same key", permHolderType.getKey(), equalTo(NEW_KEY));
        assertFalse("An empty string should mean a parameter is not required", permHolderType.requiresParameter());
    }

    @Test
    public void newTypeWithNullParameterShouldNotRequireParameter() {
        PermissionHolderType permHolderType = permissionHolderType(NEW_KEY, null);

        assertThat("Created type should have the same key", permHolderType.getKey(), equalTo(NEW_KEY));
        assertFalse("A null string should mean a parameter is not required", permHolderType.requiresParameter());
    }

    @Test
    public void newTypeWithNonNullNonEmptyParameterShouldRequireAParameter() {
        PermissionHolderType permHolderType = permissionHolderType(NEW_KEY, PARAM_VALUE);

        assertThat("Created type should have the same key", permHolderType.getKey(), equalTo(NEW_KEY));
        assertTrue("A non-null and non-empty string should mean a parameter is required", permHolderType.requiresParameter());
    }
}
