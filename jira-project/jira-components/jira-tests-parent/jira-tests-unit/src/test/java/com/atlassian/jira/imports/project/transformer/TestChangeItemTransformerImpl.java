package com.atlassian.jira.imports.project.transformer;

import com.atlassian.jira.action.issue.customfields.MockCustomFieldType;
import com.atlassian.jira.action.issue.customfields.MockProjectImportableCustomFieldType;
import com.atlassian.jira.external.beans.ExternalChangeItem;
import com.atlassian.jira.imports.project.customfield.ProjectCustomFieldImporter;
import com.atlassian.jira.imports.project.customfield.ProjectImportableCustomField;
import com.atlassian.jira.imports.project.mapper.ProjectImportMapper;
import com.atlassian.jira.imports.project.mapper.ProjectImportMapperImpl;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.fields.CustomField;
import org.hamcrest.core.Is;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Collections;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @since v3.13
 */
@RunWith(MockitoJUnitRunner.class)
public class TestChangeItemTransformerImpl {

    @Mock
    private CustomFieldManager customFieldManager;

    private ProjectImportMapper projectImportMapper;

    private ChangeItemTransformerImpl changeItemTransformer;

    @Before
    public void setup() {
        projectImportMapper = new ProjectImportMapperImpl(null, null);
        changeItemTransformer = new ChangeItemTransformerImpl("23", customFieldManager);
    }

    @Test
    public void testTransformNoChangeGroupMapping() {
        ExternalChangeItem externalChangeItem = new ExternalChangeItem("12", "13", "jira", "sickurity", "100", "red", "200", "blue");
        assertNull(changeItemTransformer.transform(projectImportMapper, externalChangeItem).getChangeGroupId());
    }

    @Test
    public void testTransform() {
        projectImportMapper.getChangeGroupMapper().mapValue("13", "113");
        ExternalChangeItem externalChangeItem = new ExternalChangeItem("12", "13", "jira", "sickurity", "100", "red", "200", "blue");
        ExternalChangeItem transformed = changeItemTransformer.transform(projectImportMapper, externalChangeItem);
        assertNull(transformed.getId());
        assertEquals("113", transformed.getChangeGroupId());
        assertEquals("jira", transformed.getFieldType());
        assertEquals("sickurity", transformed.getField());
        assertEquals("100", transformed.getOldValue());
        assertEquals("red", transformed.getOldString());
        assertEquals("200", transformed.getNewValue());
        assertEquals("blue", transformed.getNewString());
    }

    @Test
    public void testTransformJiraStatus() {
        projectImportMapper.getChangeGroupMapper().mapValue("13", "113");
        projectImportMapper.getStatusMapper().mapValue("10013", "10000");
        projectImportMapper.getStatusMapper().mapValue("13", "4");
        ExternalChangeItem externalChangeItem = new ExternalChangeItem("12", "13", "jira", "status", "10013", "todo", "13", "done");
        ExternalChangeItem transformed = changeItemTransformer.transform(projectImportMapper, externalChangeItem);
        assertThat(transformed, notNullValue());
        assertThat(transformed.getOldValue(), is("10000"));
        assertThat(transformed.getNewValue(), is("4"));
    }

    @Test
    public void testTransformCustomField() {
        // mock importable custom field
        CustomField customField1 = mock(CustomField.class);
        MockProjectImportableCustomFieldType customField1Type = mock(MockProjectImportableCustomFieldType.class);
        when(customField1.getCustomFieldType()).thenReturn(customField1Type);
        ProjectCustomFieldImporter customField1Importer = mock(ProjectCustomFieldImporter.class);
        when(customField1Type.getProjectImporter()).thenReturn(customField1Importer);
        when(customField1Importer.getMappedImportValue(anyObject(), anyObject(), anyObject())).thenReturn(new ProjectCustomFieldImporter.MappedCustomFieldValue("44"));
        projectImportMapper.getChangeGroupMapper().mapValue("13", "113");
        when(customFieldManager.getCustomFieldObjectsByName("Sprint")).thenReturn(Arrays.asList(customField1));
        // transform change item
        ExternalChangeItem externalChangeItem = new ExternalChangeItem("12", "13", "custom", "Sprint", "22", "Sprint 1", "23", "Sprint 2");
        ExternalChangeItem transformed = changeItemTransformer.transform(projectImportMapper, externalChangeItem);
        assertThat(transformed, notNullValue());
        assertThat(transformed.getOldValue(), is("44"));
        assertThat(transformed.getNewValue(), is("44"));
    }

    @Test
    public void testTransformChangeItemValue() {
        // mock 1st CF - importable
        CustomField customField1 = mock(CustomField.class);
        MockProjectImportableCustomFieldType customField1Type = mock(MockProjectImportableCustomFieldType.class);
        when(customField1.getCustomFieldType()).thenReturn(customField1Type);
        ProjectCustomFieldImporter customField1Importer = mock(ProjectCustomFieldImporter.class);
        when(customField1Type.getProjectImporter()).thenReturn(customField1Importer);

        // mock 2nd CF - importable
        CustomField customField2 = mock(CustomField.class);
        MockProjectImportableCustomFieldType customField2Type = mock(MockProjectImportableCustomFieldType.class);
        when(customField2.getCustomFieldType()).thenReturn(customField2Type);
        ProjectCustomFieldImporter customField2Importer = mock(ProjectCustomFieldImporter.class);
        when(customField1Type.getProjectImporter()).thenReturn(customField1Importer);

        // mock 3rd CF - non-importable
        CustomField customField3 = mock(CustomField.class);
        MockCustomFieldType customField3Type = mock(MockCustomFieldType.class);
        when(customField3.getCustomFieldType()).thenReturn(customField3Type);

        // test no matches
        when(customFieldManager.getCustomFieldObjectsByName("Sprint")).thenReturn(Collections.emptyList());
        CustomField importableCustomField1 = changeItemTransformer.getImportableCustomFieldByName("Sprint");
        assertThat(importableCustomField1, nullValue());

        // test one match
        when(customFieldManager.getCustomFieldObjectsByName("Sprint")).thenReturn(Arrays.asList(customField1, customField3));
        CustomField importableCustomField2 = changeItemTransformer.getImportableCustomFieldByName("Sprint");
        assertThat(importableCustomField2, Is.is(customField1));
        assertThat(((ProjectImportableCustomField) importableCustomField2.getCustomFieldType()).getProjectImporter(), Is.is(customField1Importer));

        // test two matches which returns a null
        when(customFieldManager.getCustomFieldObjectsByName("Sprint")).thenReturn(Arrays.asList(customField1, customField2, customField3));
        CustomField importableCustomField3 = changeItemTransformer.getImportableCustomFieldByName("Sprint");
        assertThat(importableCustomField3, nullValue());

    }
}
