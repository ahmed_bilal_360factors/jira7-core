package com.atlassian.jira.crowd.embedded.ofbiz;

import com.atlassian.crowd.embedded.api.SearchRestriction;
import com.atlassian.crowd.search.query.entity.restriction.BooleanRestriction;
import com.atlassian.crowd.search.query.entity.restriction.BooleanRestrictionImpl;
import com.atlassian.crowd.search.query.entity.restriction.MatchMode;
import com.atlassian.crowd.search.query.entity.restriction.NullRestrictionImpl;
import com.atlassian.crowd.search.query.entity.restriction.PropertyUtils;
import com.atlassian.crowd.search.query.entity.restriction.TermRestriction;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.ofbiz.DefaultOfBizDelegator;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.mockito.Mock;
import org.ofbiz.core.entity.EntityCondition;
import org.ofbiz.core.entity.EntityConditionParam;
import org.ofbiz.core.entity.GenericDelegator;
import org.ofbiz.core.entity.model.ModelEntity;
import org.ofbiz.core.entity.model.ModelField;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.hasToString;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.collection.IsEmptyCollection.empty;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class GroupEntityConditionFactoryTest {
    private EntityConditionFactory factory;

    private final String tableName = "someprefix.cwd_group_attributes";

    @Rule
    public RuleChain mockito = MockitoMocksInContainer.forTest(this);

    @Mock
    @AvailableInContainer
    private FeatureManager featureManager;

    @Before
    public void setUp() throws Exception {
        ModelEntity entity = mock(ModelEntity.class);
        when(entity.getTableName(GroupAttributeEntity.ENTITY)).thenReturn(tableName);

        GenericDelegator genericDelegator = mock(GenericDelegator.class);
        when(genericDelegator.getModelEntity(eq(GroupAttributeEntity.ENTITY))).thenReturn(entity);
        //NOt sure if the return here is the correct format, but it servers as a marker string for the above model entity get
        when(genericDelegator.getEntityHelperName(eq(GroupAttributeEntity.ENTITY))).thenReturn(GroupAttributeEntity.ENTITY);

        factory = new GroupEntityConditionFactory(new DefaultOfBizDelegator(genericDelegator));
    }

    @Test
    public void testNullRestriction() throws Exception {
        assertNull(factory.getEntityConditionFor(NullRestrictionImpl.INSTANCE));
    }

    @Test
    public void testEqualRestrictionNullString() throws Exception {
        final SearchRestriction searchRestriction = new TermRestriction<>(PropertyUtils.ofTypeString("groupName"), MatchMode.EXACTLY_MATCHES, null);

        final EntityCondition condition = factory.getEntityConditionFor(searchRestriction);
        final List<EntityConditionParam> parameterValues = new ArrayList<>();
        final String query = condition.makeWhereString(getModelEntity(), parameterValues);

        assertEquals("lowerGroupName IS NULL ", query);
        assertThat(parameterValues, is(empty()));
    }

    @Test
    public void testEqualRestrictionEmptyString() throws Exception {
        final SearchRestriction searchRestriction = new TermRestriction<>(PropertyUtils.ofTypeString("groupName"), MatchMode.EXACTLY_MATCHES, "");

        final EntityCondition condition = factory.getEntityConditionFor(searchRestriction);
        final List<EntityConditionParam> parameterValues = new ArrayList<>();
        final String query = condition.makeWhereString(getModelEntity(), parameterValues);

        assertEquals("lowerGroupName =  ? ", query);
        assertThat(parameterValues, Matchers.contains(hasToString("lowerGroupName=")));
    }

    @Test
    public void testEqualRestrictionString() throws Exception {
        final SearchRestriction searchRestriction = new TermRestriction<>(PropertyUtils.ofTypeString("groupName"), MatchMode.EXACTLY_MATCHES,
                "accounts");

        final EntityCondition condition = factory.getEntityConditionFor(searchRestriction);
        final List<EntityConditionParam> parameterValues = new ArrayList<>();
        final String query = condition.makeWhereString(getModelEntity(), parameterValues);

        assertEquals("lowerGroupName =  ? ", query);
        assertThat(parameterValues, Matchers.contains(hasToString("lowerGroupName=accounts")));
    }

    @Test
    public void testEqualRestrictionBoolean() throws Exception {
        final SearchRestriction searchRestriction = new TermRestriction<>(PropertyUtils.ofTypeBoolean("active"), MatchMode.EXACTLY_MATCHES,
                Boolean.TRUE);

        final EntityCondition condition = factory.getEntityConditionFor(searchRestriction);
        final List<EntityConditionParam> parameterValues = new ArrayList<>();
        final String query = condition.makeWhereString(getModelEntity(), parameterValues);

        assertEquals("active =  ? ", query);
        assertThat(parameterValues, Matchers.contains(hasToString("active=1")));
    }

    @Test
    public void testGTRestriction() throws Exception {
        final SearchRestriction searchRestriction = new TermRestriction<>(PropertyUtils.ofTypeString("groupName"), MatchMode.GREATER_THAN,
                "accounts");

        final EntityCondition condition = factory.getEntityConditionFor(searchRestriction);
        final List<EntityConditionParam> parameterValues = new ArrayList<>();
        final String query = condition.makeWhereString(getModelEntity(), parameterValues);

        assertEquals("lowerGroupName >  ? ", query);
        assertThat(parameterValues, Matchers.contains(hasToString("lowerGroupName=accounts")));
    }

    @Test
    public void testLTRestriction() throws Exception {
        final SearchRestriction searchRestriction = new TermRestriction<>(PropertyUtils.ofTypeString("groupName"), MatchMode.LESS_THAN, "accounts");

        final EntityCondition condition = factory.getEntityConditionFor(searchRestriction);
        final List<EntityConditionParam> parameterValues = new ArrayList<>();
        final String query = condition.makeWhereString(getModelEntity(), parameterValues);

        assertEquals("lowerGroupName <  ? ", query);
        assertThat(parameterValues, Matchers.contains(hasToString("lowerGroupName=accounts")));
    }

    @Test
    public void testContainsRestriction() throws Exception {
        final SearchRestriction searchRestriction = new TermRestriction<>(PropertyUtils.ofTypeString("groupName"), MatchMode.CONTAINS, "accounts");

        final EntityCondition condition = factory.getEntityConditionFor(searchRestriction);
        final List<EntityConditionParam> parameterValues = new ArrayList<>();
        final String query = condition.makeWhereString(getModelEntity(), parameterValues);

        assertEquals("lowerGroupName LIKE  ? ", query);
        assertThat(parameterValues, Matchers.contains(hasToString("lowerGroupName=%accounts%")));
    }

    @Test
    public void testStartsWithRestriction() throws Exception {
        final SearchRestriction searchRestriction = new TermRestriction<>(PropertyUtils.ofTypeString("groupName"), MatchMode.STARTS_WITH, "accounts");

        final EntityCondition condition = factory.getEntityConditionFor(searchRestriction);
        final List<EntityConditionParam> parameterValues = new ArrayList<>();
        final String query = condition.makeWhereString(getModelEntity(), parameterValues);

        assertEquals("lowerGroupName LIKE  ? ", query);
        assertThat(parameterValues, Matchers.contains(hasToString("lowerGroupName=accounts%")));
    }

    @Test
    public void testSimpleOr() throws Exception {
        final SearchRestriction searchRestriction1 = new TermRestriction<>(PropertyUtils.ofTypeString("groupName"), MatchMode.EXACTLY_MATCHES,
                "accounts");
        final SearchRestriction searchRestriction2 = new TermRestriction<>(PropertyUtils.ofTypeString("description"), MatchMode.EXACTLY_MATCHES,
                "Bean counters");
        final SearchRestriction restriction = new BooleanRestrictionImpl(BooleanRestriction.BooleanLogic.OR, searchRestriction1, searchRestriction2);

        final EntityCondition condition = factory.getEntityConditionFor(restriction);
        final List<EntityConditionParam> parameterValues = new ArrayList<>();
        final String query = condition.makeWhereString(getModelEntity(), parameterValues);

        assertEquals("(lowerGroupName =  ? ) OR (lowerDescription =  ? )", query);
        assertThat(parameterValues, Matchers.contains(
                hasToString("lowerGroupName=accounts"),
                hasToString("lowerDescription=bean counters")));
    }

    @Test
    public void testSimpleAnd() throws Exception {
        final SearchRestriction searchRestriction1 = new TermRestriction<>(PropertyUtils.ofTypeString("groupName"), MatchMode.EXACTLY_MATCHES,
                "accounts");
        final SearchRestriction searchRestriction2 = new TermRestriction<>(PropertyUtils.ofTypeString("description"), MatchMode.EXACTLY_MATCHES,
                "Bean counters");
        final SearchRestriction restriction = new BooleanRestrictionImpl(BooleanRestriction.BooleanLogic.AND, searchRestriction1, searchRestriction2);

        final EntityCondition condition = factory.getEntityConditionFor(restriction);
        final List<EntityConditionParam> parameterValues = new ArrayList<>();
        final String query = condition.makeWhereString(getModelEntity(), parameterValues);

        assertEquals("(lowerGroupName =  ? ) AND (lowerDescription =  ? )", query);
        assertThat(parameterValues, Matchers.contains(
                hasToString("lowerGroupName=accounts"),
                hasToString("lowerDescription=bean counters")));
    }

    @Test
    public void testSimpleAndWithNullRestriction() throws Exception {
        final SearchRestriction searchRestriction1 = new TermRestriction<>(PropertyUtils.ofTypeString("groupName"), MatchMode.EXACTLY_MATCHES,
                "accounts");
        final SearchRestriction searchRestriction2 = NullRestrictionImpl.INSTANCE;
        final SearchRestriction restriction = new BooleanRestrictionImpl(BooleanRestriction.BooleanLogic.AND, searchRestriction1, searchRestriction2);

        final EntityCondition condition = factory.getEntityConditionFor(restriction);
        final List<EntityConditionParam> parameterValues = new ArrayList<>();
        final String query = condition.makeWhereString(getModelEntity(), parameterValues);

        assertEquals("(lowerGroupName =  ? )", query);
        assertThat(parameterValues, Matchers.contains(hasToString("lowerGroupName=accounts")));
    }

    @Test
    public void testTrivialBooleanAndWithOnlyNullRestriction() throws Exception {
        final SearchRestriction searchRestriction1 = NullRestrictionImpl.INSTANCE;
        final SearchRestriction searchRestriction2 = NullRestrictionImpl.INSTANCE;
        final SearchRestriction restriction = new BooleanRestrictionImpl(BooleanRestriction.BooleanLogic.AND, searchRestriction1, searchRestriction2);

        final EntityCondition condition = factory.getEntityConditionFor(restriction);
        final List<EntityConditionParam> parameterValues = new ArrayList<>();
        final String query = condition.makeWhereString(getModelEntity(), parameterValues);

        assertEquals("", query);
        assertThat(parameterValues, is(empty()));
    }

    @Test
    public void testSimpleOrWithNullRestriction() throws Exception {
        final SearchRestriction searchRestriction1 = new TermRestriction<>(PropertyUtils.ofTypeString("groupName"), MatchMode.EXACTLY_MATCHES,
                "accounts");
        final SearchRestriction searchRestriction2 = NullRestrictionImpl.INSTANCE;
        final SearchRestriction restriction = new BooleanRestrictionImpl(BooleanRestriction.BooleanLogic.OR, searchRestriction1, searchRestriction2);

        final EntityCondition condition = factory.getEntityConditionFor(restriction);
        assertNull(condition);
    }

    @Test
    public void testNestedQuery() throws Exception {
        final SearchRestriction searchRestriction1 = new TermRestriction<>(PropertyUtils.ofTypeString("groupName"), MatchMode.EXACTLY_MATCHES,
                "accounts");
        final SearchRestriction searchRestriction2 = new TermRestriction<>(PropertyUtils.ofTypeString("description"), MatchMode.EXACTLY_MATCHES,
                "Bean counters");
        final SearchRestriction searchRestriction3 = new TermRestriction<>(PropertyUtils.ofTypeString("groupName"), MatchMode.EXACTLY_MATCHES,
                "legal");
        final SearchRestriction searchRestriction4 = new TermRestriction<>(PropertyUtils.ofTypeString("description"), MatchMode.EXACTLY_MATCHES,
                "Bill and Frank's Dodgy Firm");
        final SearchRestriction booleanRestriction1 = new BooleanRestrictionImpl(BooleanRestriction.BooleanLogic.AND, searchRestriction1,
                searchRestriction2);
        final SearchRestriction booleanRestriction2 = new BooleanRestrictionImpl(BooleanRestriction.BooleanLogic.AND, searchRestriction3,
                searchRestriction4);
        final SearchRestriction booleanRestriction3 = new BooleanRestrictionImpl(BooleanRestriction.BooleanLogic.OR, booleanRestriction1,
                booleanRestriction2);

        final EntityCondition condition = factory.getEntityConditionFor(booleanRestriction3);
        final List<EntityConditionParam> parameterValues = new ArrayList<>();
        final String query = condition.makeWhereString(getModelEntity(), parameterValues);

        assertEquals("((lowerGroupName =  ? ) AND (lowerDescription =  ? )) OR ((lowerGroupName =  ? ) AND (lowerDescription =  ? ))", query);
        assertThat(parameterValues, Matchers.contains(
                hasToString("lowerGroupName=accounts"),
                hasToString("lowerDescription=bean counters"),
                hasToString("lowerGroupName=legal"),
                hasToString("lowerDescription=bill and frank's dodgy firm")));
    }

    @Test
    public void testAttributeRestriction() throws Exception {
        final SearchRestriction restriction = new TermRestriction<>(PropertyUtils.ofTypeString("manager"), MatchMode.EXACTLY_MATCHES, "Paul");

        final EntityCondition condition = factory.getEntityConditionFor(restriction);
        final List<EntityConditionParam> parameterValues = new ArrayList<>();
        final String query = condition.makeWhereString(getModelEntity(), parameterValues);

        assertEquals("id IN (SELECT group_id FROM " + tableName + " WHERE attribute_name = ? AND lower_attribute_value =  ? )", query);
        assertThat(parameterValues, Matchers.contains(
                hasToString("name=manager"),
                hasToString("value=paul")));
    }

    @Test
    public void testNestedWithAttributesQuery() throws Exception {
        final SearchRestriction searchRestriction1 = new TermRestriction<>(PropertyUtils.ofTypeString("groupName"), MatchMode.EXACTLY_MATCHES,
                "accounts");
        final SearchRestriction searchRestriction2 = new TermRestriction<>(PropertyUtils.ofTypeString("manager"), MatchMode.EXACTLY_MATCHES, "Paul");
        final SearchRestriction searchRestriction3 = new TermRestriction<>(PropertyUtils.ofTypeString("groupName"), MatchMode.EXACTLY_MATCHES,
                "legal");
        final SearchRestriction searchRestriction4 = new TermRestriction<>(PropertyUtils.ofTypeString("manager"), MatchMode.EXACTLY_MATCHES, "Paul");
        final SearchRestriction booleanRestriction1 = new BooleanRestrictionImpl(BooleanRestriction.BooleanLogic.AND, searchRestriction1,
                searchRestriction2);
        final SearchRestriction booleanRestriction2 = new BooleanRestrictionImpl(BooleanRestriction.BooleanLogic.AND, searchRestriction3,
                searchRestriction4);
        final SearchRestriction booleanRestriction3 = new BooleanRestrictionImpl(BooleanRestriction.BooleanLogic.OR, booleanRestriction1,
                booleanRestriction2);

        final EntityCondition condition = factory.getEntityConditionFor(booleanRestriction3);
        final List<EntityConditionParam> parameterValues = new ArrayList<>();
        final String query = condition.makeWhereString(getModelEntity(), parameterValues);

        assertEquals(
                "((lowerGroupName =  ? ) AND (id IN (SELECT group_id FROM " + tableName + " WHERE attribute_name = ? "
                        + "AND lower_attribute_value =  ? ))) OR ((lowerGroupName =  ? ) AND "
                        + "(id IN (SELECT group_id FROM " + tableName + " WHERE attribute_name = ? AND lower_attribute_value =  ? )))",
                query);
        assertThat(parameterValues, Matchers.contains(
                hasToString("lowerGroupName=accounts"),
                hasToString("name=manager"),
                hasToString("value=paul"),
                hasToString("lowerGroupName=legal"),
                hasToString("name=manager"),
                hasToString("value=paul")));
    }

    private ModelEntity getModelEntity() {
        final ModelEntity modelEntity = new ModelEntity();
        modelEntity.setTableName("Group");
        modelEntity.addField(getModelField("groupName"));
        modelEntity.addField(getModelField("lowerGroupName"));
        modelEntity.addField(getModelField("description"));
        modelEntity.addField(getModelField("lowerDescription"));
        modelEntity.addField(getModelField("type"));
        modelEntity.addField(getModelField("active", "boolean"));

        return modelEntity;
    }

    private ModelField getModelField(final String name) {
        return getModelField(name, "blah");
    }

    private ModelField getModelField(final String name, final String type) {
        return new ModelField(name, type, name, false, null);
    }
}