package com.atlassian.jira.util.system.check;

import com.atlassian.jira.util.BuildUtilsInfo;
import com.atlassian.jira.util.johnson.JohnsonProvider;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.PluginInformation;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestPluginVersionCheck {
    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @Mock
    private BuildUtilsInfo buildUtilsInfo;
    @Mock
    private PluginAccessor pluginAccessor;
    @Mock
    private JohnsonProvider johnsonProvider;

    @Before
    public void setUp() throws Exception {
        when(buildUtilsInfo.getVersion()).thenReturn("3.3.1");
    }

    @Test
    public void testCheckWithNoPlugin() {
        final AtomicBoolean addErrorsCalled = new AtomicBoolean(false);
        final PluginVersionCheck pluginVersionCheck = new PluginVersionCheck(pluginAccessor, buildUtilsInfo, johnsonProvider) {
            @Override
            void addErrors(final Set<Plugin> outdatedPlugins) {
                addErrorsCalled.set(true);
            }
        };

        pluginVersionCheck.check(null);
        assertFalse(addErrorsCalled.get());
    }

    @Test
    public void testCheckWithMinVersionGood() {
        final AtomicBoolean addErrorsCalled = new AtomicBoolean(false);
        final PluginVersionCheck pluginVersionCheck = new PluginVersionCheck(pluginAccessor, buildUtilsInfo, johnsonProvider) {
            @Override
            void addErrors(final Set<Plugin> outdatedPlugins) {
                addErrorsCalled.set(true);
            }
        };

        final PluginInformation mockPluginInformation = mock(PluginInformation.class);
        when(mockPluginInformation.getMinVersion()).thenReturn(2.8f);

        final Plugin mockPlugin = mock(Plugin.class);
        when(mockPlugin.getPluginInformation()).thenReturn(mockPluginInformation);

        when(pluginAccessor.getPlugin("com.atlassian.jira.ext.charting")).thenReturn(mockPlugin);

        pluginVersionCheck.check(null);
        assertFalse(addErrorsCalled.get());

        verify(mockPluginInformation).getMinVersion();
    }

    @Test
    public void testCheckWithError() {
        final AtomicBoolean addErrorsCalled = new AtomicBoolean(false);

        final PluginVersionCheck pluginVersionCheck = new PluginVersionCheck(pluginAccessor, buildUtilsInfo, johnsonProvider) {
            @Override
            void addErrors(final Set<Plugin> outdatedPlugins) {
                addErrorsCalled.set(true);
            }
        };

        final PluginInformation mockPluginInformation = mock(PluginInformation.class);
        //minVersion is above the current app version
        when(mockPluginInformation.getMinVersion()).thenReturn(100.1f);

        final Plugin mockPlugin = mock(Plugin.class);
        when(mockPlugin.getPluginInformation()).thenReturn(mockPluginInformation);

        when(pluginAccessor.getPlugin("com.atlassian.jira.ext.charting")).thenReturn(mockPlugin);

        pluginVersionCheck.check(null);
        assertTrue(addErrorsCalled.get());

        verify(mockPluginInformation).getMinVersion();
    }

    @Test
    public void testBlackListedPlugin() {
        final AtomicBoolean addErrorsCalled = new AtomicBoolean(false);

        final PluginVersionCheck pluginVersionCheck = new PluginVersionCheck(pluginAccessor, buildUtilsInfo, johnsonProvider) {
            @Override
            void addErrors(final Set<Plugin> outdatedPlugins) {
                addErrorsCalled.set(true);
            }
        };

        final Plugin mockPlugin = mock(Plugin.class);

        when(pluginAccessor.getPlugin("com.atlassian.jira.plugin.labels")).thenReturn(mockPlugin);

        pluginVersionCheck.check(null);
        assertTrue(addErrorsCalled.get());
    }
}
