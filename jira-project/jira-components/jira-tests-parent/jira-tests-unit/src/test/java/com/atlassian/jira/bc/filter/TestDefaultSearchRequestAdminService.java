package com.atlassian.jira.bc.filter;

import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.jira.favourites.FavouritesManager;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.issue.search.SearchRequestAdminManager;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.MockGroup;
import com.atlassian.jira.user.util.MockUserManager;
import com.atlassian.jira.util.collect.MockCloseableIterable;
import com.atlassian.jira.web.action.util.SimpleSearchRequestDisplay;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.mockito.Mock;

import java.util.Collection;

import static com.atlassian.core.util.collection.EasyList.build;
import static com.atlassian.jira.issue.search.MockSearchRequest.get;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestDefaultSearchRequestAdminService {
    @Rule
    public TestRule container = MockitoMocksInContainer.forTest(this);

    @Mock
    private SearchRequestAdminManager searchRequestAdminManager;

    @AvailableInContainer
    private MockUserManager userManager = new MockUserManager();

    private static final Long ID_123 = 123L;

    @Test
    public void testGetFiltersSharedWithGroup() {
        userManager.addUser(new MockApplicationUser("userName"));
        final SearchRequest searchRequest = get("userName", ID_123);
        searchRequest.setName("sr1");
        searchRequest.setDescription("sr1Desc");
        final Group group = new MockGroup("admin");

        final MockCloseableIterable<SearchRequest> closeableIterable = new MockCloseableIterable<SearchRequest>(build(searchRequest));
        when(searchRequestAdminManager.getSearchRequests(group)).thenReturn(closeableIterable);

        DefaultSearchRequestAdminService searchRequestAdminService = new DefaultSearchRequestAdminService(searchRequestAdminManager, mock(FavouritesManager.class));

        final Collection<?> result = searchRequestAdminService.getFiltersSharedWithGroup(group);
        assertNotNull(result);
        assertEquals(1, result.size());
        final SimpleSearchRequestDisplay simpleSearchRequestDisplay = (SimpleSearchRequestDisplay) result.iterator().next();
        assertEquals("userName", simpleSearchRequestDisplay.getOwnerUserName());
        assertEquals("sr1", simpleSearchRequestDisplay.getName());
        assertEquals(ID_123, simpleSearchRequestDisplay.getId());
    }

}
