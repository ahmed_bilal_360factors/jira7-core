package com.atlassian.jira.jql.permission;

import com.atlassian.jira.jql.operand.QueryLiteral;
import com.atlassian.jira.jql.resolver.IndexInfoResolver;
import com.atlassian.jira.jql.resolver.NameResolver;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.util.collect.CollectionBuilder;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Collections;
import java.util.List;

import static com.atlassian.jira.jql.operand.SimpleLiteralFactory.createLiteral;
import static com.atlassian.jira.permission.ProjectPermissions.BROWSE_PROJECTS;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestProjectLiteralSanitiser {
    @Rule
    public MockitoRule mockito = MockitoJUnit.rule();

    @Mock
    private IndexInfoResolver<Project> projectIndexInfoResolver;
    @Mock
    private NameResolver<Project> projectResolver;
    @Mock
    private PermissionManager permissionManager;

    private ApplicationUser user;

    private ProjectLiteralSanitiser sanitiser;

    @Before
    public void setUp() throws Exception {
        user = new MockApplicationUser("fred");
        sanitiser = new ProjectLiteralSanitiser(projectResolver, permissionManager, projectIndexInfoResolver, user);
    }

    @Test
    public void testGetIndexValues() throws Exception {
        final List<String> expectedValues1 = Collections.singletonList("StringIndexValues");
        final List<String> expectedValues2 = Collections.singletonList("LongIndexValues");

        when(projectIndexInfoResolver.getIndexedValues("litString")).thenReturn(expectedValues1);
        when(projectIndexInfoResolver.getIndexedValues(1111L)).thenReturn(expectedValues2);

        assertEquals(expectedValues1, sanitiser.getIndexValues(createLiteral("litString")));
        assertEquals(expectedValues2, sanitiser.getIndexValues(createLiteral(1111L)));
        assertEquals(Collections.<String>emptyList(), sanitiser.getIndexValues(new QueryLiteral()));
    }

    @Test
    public void testTwoProjectsNoModification() throws Exception {
        final List<QueryLiteral> literals = Collections.singletonList(createLiteral("HSP"));
        final MockProject project1 = new MockProject(10000L);
        final MockProject project2 = new MockProject(20000L);

        when(projectIndexInfoResolver.getIndexedValues("HSP")).thenReturn(CollectionBuilder.newBuilder("10000", "20000").asList());

        when(projectResolver.get(10000L)).thenReturn(project1);
        when(projectResolver.get(20000L)).thenReturn(project2);

        when(permissionManager.hasPermission(BROWSE_PROJECTS, project1, user)).thenReturn(true);
        when(permissionManager.hasPermission(BROWSE_PROJECTS, project2, user)).thenReturn(true);

        final LiteralSanitiser.Result result = sanitiser.sanitiseLiterals(literals);
        assertFalse(result.isModified());
    }

    @Test
    public void testOneProjectModification() throws Exception {
        final List<QueryLiteral> literals = Collections.singletonList(createLiteral("HSP"));
        final QueryLiteral expectedLiteral = createLiteral(10000L);
        final MockProject project1 = new MockProject(10000L);

        when(projectIndexInfoResolver.getIndexedValues("HSP")).thenReturn(Collections.singletonList("10000"));

        when(projectResolver.get(10000L)).thenReturn(project1);

        when(permissionManager.hasPermission(BROWSE_PROJECTS, project1, user)).thenReturn(false);

        final LiteralSanitiser.Result result = sanitiser.sanitiseLiterals(literals);
        assertTrue(result.isModified());
        assertEquals(1, result.getLiterals().size());
        assertEquals(expectedLiteral, result.getLiterals().get(0));
    }

    @Test
    public void testTwoProjectsOneModification() throws Exception {
        final QueryLiteral expectedLiteral1 = createLiteral("HSP");
        final QueryLiteral expectedLiteral2 = createLiteral(20000L);
        final List<QueryLiteral> inputLiterals = Collections.singletonList(expectedLiteral1);
        final List<QueryLiteral> expectedLiterals = CollectionBuilder.newBuilder(expectedLiteral1, expectedLiteral2).asList();
        final MockProject project1 = new MockProject(10000L);
        final MockProject project2 = new MockProject(20000L);

        when(projectIndexInfoResolver.getIndexedValues("HSP")).thenReturn(CollectionBuilder.newBuilder("10000", "20000").asList());

        when(projectResolver.get(10000L)).thenReturn(project1);
        when(projectResolver.get(20000L)).thenReturn(project2);

        when(permissionManager.hasPermission(BROWSE_PROJECTS, project1, user)).thenReturn(true);
        when(permissionManager.hasPermission(BROWSE_PROJECTS, project2, user)).thenReturn(false);

        final LiteralSanitiser.Result result = sanitiser.sanitiseLiterals(inputLiterals);
        assertTrue(result.isModified());
        assertEquals(expectedLiterals, result.getLiterals());
    }

    @Test
    public void testTwoProjectsModification() throws Exception {
        final List<QueryLiteral> inputLiterals = Collections.singletonList(createLiteral("HSP"));
        final List<QueryLiteral> expectedLiterals = CollectionBuilder.newBuilder(createLiteral(10000L), createLiteral(20000L)).asList();
        final MockProject project1 = new MockProject(10000L);
        final MockProject project2 = new MockProject(20000L);

        when(projectIndexInfoResolver.getIndexedValues("HSP")).thenReturn(CollectionBuilder.newBuilder("10000", "20000").asList());

        when(projectResolver.get(10000L)).thenReturn(project1);
        when(projectResolver.get(20000L)).thenReturn(project2);

        when(permissionManager.hasPermission(BROWSE_PROJECTS, project1, user)).thenReturn(false);
        when(permissionManager.hasPermission(BROWSE_PROJECTS, project2, user)).thenReturn(false);

        final LiteralSanitiser.Result result = sanitiser.sanitiseLiterals(inputLiterals);
        assertTrue(result.isModified());
        assertEquals(expectedLiterals, result.getLiterals());
    }

    @Test
    public void testTwoProjectsOneDoesNotExist() throws Exception {
        final List<QueryLiteral> inputLiterals = Collections.singletonList(createLiteral("HSP"));
        final List<QueryLiteral> expectedLiterals = CollectionBuilder.newBuilder(createLiteral("HSP"), createLiteral(20000L)).asList();
        final MockProject project2 = new MockProject(20000L);

        when(projectIndexInfoResolver.getIndexedValues("HSP")).thenReturn(CollectionBuilder.newBuilder("10000", "20000").asList());

        when(projectResolver.get(10000L)).thenReturn(null);
        when(projectResolver.get(20000L)).thenReturn(project2);

        when(permissionManager.hasPermission(BROWSE_PROJECTS, project2, user)).thenReturn(false);

        final LiteralSanitiser.Result result = sanitiser.sanitiseLiterals(inputLiterals);
        assertTrue(result.isModified());
        assertEquals(expectedLiterals, result.getLiterals());
    }

    @Test
    public void testTwoProjectsTwoDoesNotExist() throws Exception {
        final List<QueryLiteral> inputLiterals = Collections.singletonList(createLiteral("HSP"));

        when(projectIndexInfoResolver.getIndexedValues("HSP")).thenReturn(CollectionBuilder.newBuilder("10000", "20000").asList());

        when(projectResolver.get(10000L)).thenReturn(null);
        when(projectResolver.get(20000L)).thenReturn(null);

        final LiteralSanitiser.Result result = sanitiser.sanitiseLiterals(inputLiterals);
        assertFalse(result.isModified());
    }
}
