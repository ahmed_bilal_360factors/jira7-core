package com.atlassian.jira.notification.type;

import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.mock.issue.MockIssue;
import com.atlassian.jira.notification.NotificationRecipient;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.preferences.ExtendedPreferences;
import com.atlassian.jira.user.preferences.PreferenceKeys;
import com.atlassian.jira.user.preferences.UserPreferencesManager;
import com.atlassian.jira.user.util.UserManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.mockito.Mock;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

/**
 * Basic setUp/tearDown common to most of the simple notification tests.
 *
 * @since v6.0
 */
public abstract class AbstractNotificationTestCase {
    @Rule
    public MockitoContainer mockitoContainer = MockitoMocksInContainer.rule(this);

    @AvailableInContainer
    @Mock
    protected GroupManager groupManager;

    @AvailableInContainer
    @Mock
    protected UserManager userManager;

    @AvailableInContainer
    @Mock
    protected UserPreferencesManager userPreferencesManager;

    @AvailableInContainer
    @Mock
    protected ExtendedPreferences preferences;

    protected MockIssue issue;
    protected ApplicationUser user;

    @Before
    public final void setUp() {
        user = initUser();
        issue = new MockIssue();
        setUpTest();
    }

    @After
    public final void tearDown() {
        tearDownTest();
        groupManager = null;
        userManager = null;
        userPreferencesManager = null;
        user = null;
        issue = null;
        preferences = null;
    }

    protected void setUpTest() {
    }

    protected void tearDownTest() {
    }


    protected ApplicationUser initUser() {
        final ApplicationUser dude = new MockApplicationUser("ID12345", "DudeUser", "Dude User", "dudeuser@example.com");
        final MockIssue issue = new MockIssue();
        issue.setReporterId(dude.getKey());

        when(userManager.getUserByKey(dude.getKey())).thenReturn(dude);
        when(userManager.getUserByName(dude.getUsername())).thenReturn(dude);
        when(userPreferencesManager.getExtendedPreferences(dude)).thenReturn(preferences);
        when(preferences.getString(PreferenceKeys.USER_NOTIFICATIONS_MIMETYPE)).thenReturn(NotificationRecipient.MIMETYPE_HTML);
        return dude;
    }

    protected Map<String, String> paramsWithLevel() {
        final Map<String, String> params = new HashMap<String, String>(4);
        params.put("level", "group1");
        return params;
    }

    protected void checkRecipients(List<NotificationRecipient> actualRecipients, ApplicationUser... expectedUsers) {
        final List<NotificationRecipient> expectedRecipients = new ArrayList<NotificationRecipient>(expectedUsers.length);
        for (ApplicationUser user : expectedUsers) {
            expectedRecipients.add(new NotificationRecipient(user));
        }
        assertEquals(expectedRecipients, actualRecipients);
    }
}
