package com.atlassian.jira.web.action.setup;

import com.atlassian.jira.bc.license.JiraLicenseService;
import com.atlassian.jira.config.SubTaskManager;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutManager;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.JiraWebActionSupportDependencies;
import com.atlassian.jira.junit.rules.MockitoContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.license.LicenseJohnsonEventRaiser;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.upgrade.SetupUpgradeService;
import com.atlassian.jira.upgrade.UpgradeResult;
import com.atlassian.jira.util.JiraProductInformation;
import com.atlassian.jira.util.johnson.JohnsonProvider;
import com.atlassian.johnson.config.JohnsonConfig;
import com.atlassian.plugin.event.PluginEventManager;
import com.google.common.collect.ImmutableList;
import com.mockobjects.servlet.MockServletContext;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.mock.web.MockHttpServletResponse;
import webwork.action.Action;
import webwork.action.ServletActionContext;

import javax.servlet.http.HttpSession;

import static com.atlassian.jira.config.properties.APKeys.JIRA_OPTION_ALLOWATTACHMENTS;
import static com.atlassian.jira.config.properties.APKeys.JIRA_OPTION_ALLOWUNASSIGNED;
import static com.atlassian.jira.config.properties.APKeys.JIRA_OPTION_USER_EXTERNALMGT;
import static com.atlassian.jira.config.properties.APKeys.JIRA_OPTION_VOTING;
import static com.atlassian.jira.config.properties.APKeys.JIRA_OPTION_WATCHING;
import static com.atlassian.jira.config.properties.APKeys.JIRA_SETUP;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

public class TestSetupComplete {
    public static final String DASHBOARD_URL = "Dashboard.jspa";

    @Rule
    public final MockitoContainer mockContainer = MockitoMocksInContainer.rule(this);
    @Rule
    public TestRule webActionSupport = JiraWebActionSupportDependencies.build(mockContainer);


    private SetupComplete setupCompleteAction;
    private MockHttpServletResponse mockHttpServletResponse;

    @Mock
    private FieldLayoutManager fieldLayoutManager;
    @Mock
    private JiraLicenseService jiraLicenseService;
    @Mock
    private LicenseJohnsonEventRaiser licenseJohnsonEventRaiser;
    @Mock
    private PluginEventManager pluginEventManager;
    @Mock
    private SubTaskManager subTaskManager;
    @Mock
    private SetupUpgradeService upgradeService;
    @Mock
    private SetupCompleteRedirectHelper setupCompleteRedirectHelper;
    @Mock
    private HttpSession httpSession;
    @Mock
    private JohnsonProvider johnsonProvider;
    @Mock
    private JohnsonConfig johnsonConfig;
    @Mock
    private JiraProductInformation jiraProductInformation;
    @Mock
    @AvailableInContainer
    private JiraAuthenticationContext jiraAuthenticationContext;

    @Before
    public void setUp() {
        final MockServletContext mockServletContext = new MockServletContext();
        ServletActionContext.setServletContext(mockServletContext);
//        Johnson.initialize("test-johnson-config.xml");

        mockHttpServletResponse = new MockHttpServletResponse();

        ServletActionContext.setResponse(mockHttpServletResponse);

        // Mock out the license test
        setupCompleteAction = new SetupComplete(upgradeService, licenseJohnsonEventRaiser, subTaskManager,
                fieldLayoutManager, null, pluginEventManager, null, setupCompleteRedirectHelper, johnsonProvider,
                jiraProductInformation) {
            @Override
            protected boolean licenseTooOld() {
                return false;
            }

            @Override
            void setWikiRendererOnAllRenderableFields() {
            }
        };

        when(setupCompleteRedirectHelper.getRedirectUrl(Mockito.any())).thenReturn(DASHBOARD_URL);
        when(johnsonProvider.getConfig()).thenReturn(johnsonConfig);
        when(johnsonConfig.getErrorPath()).thenReturn("/blah");
    }

    @After
    public void tearDown() {
        ServletActionContext.setResponse(null);
        ServletActionContext.setServletContext(null);
//        Johnson.terminate();
    }

    @Test
    public void testDoDefaultRedirectsToTheDashboardWhenTheUserCanNotBeLoggedInAutomatically() throws Exception {
        // Set up
        expectNoUpgradeTaskErrors();

        // Invoke
        setupCompleteAction.doDefault();

        // Check
        assertEquals(mockHttpServletResponse.getRedirectedUrl(), DASHBOARD_URL);
    }

    private void expectNoUpgradeTaskErrors() throws Exception {
        when(upgradeService.runUpgrades()).thenReturn(UpgradeResult.OK);
    }

    @Test
    public void testExecuteWhenAlreadySetup() throws Exception {
        setupCompleteAction.getApplicationProperties().setString(JIRA_SETUP, "true");
        assertEquals("setupalready", setupCompleteAction.execute());
    }

    @Test
    public void testExecuteSetsApplicationPropertiesToTheirDefaultValues() throws Exception {
        // Set up
        setupCompleteAction.getApplicationProperties().setString(JIRA_SETUP, null);
        expectNoUpgradeTaskErrors();

        // Invoke
        setupCompleteAction.execute();

        // Check
        // set the default values for jira application properties in newly setup instances.
        assertEquals("true", setupCompleteAction.getApplicationProperties().getString(JIRA_SETUP));
        assertTrue(!setupCompleteAction.getApplicationProperties().getOption(JIRA_OPTION_ALLOWUNASSIGNED));
        assertTrue(!setupCompleteAction.getApplicationProperties().getOption(JIRA_OPTION_ALLOWATTACHMENTS));
        assertTrue(!setupCompleteAction.getApplicationProperties().getOption(JIRA_OPTION_USER_EXTERNALMGT));
        assertTrue(setupCompleteAction.getApplicationProperties().getOption(JIRA_OPTION_VOTING));
        assertTrue(setupCompleteAction.getApplicationProperties().getOption(JIRA_OPTION_WATCHING));
    }

    @Test
    public void testExecuteRunsUpgradeTasksRequiredForANewSetup() throws Exception {
        // Set up
        expectNoUpgradeTaskErrors();

        // Invoke
        setupCompleteAction.execute();
    }

    @Test
    public void testExecuteUpgradeManagerErrorsAreAdded() throws Exception {
        when(upgradeService.runUpgrades()).thenReturn(new UpgradeResult(ImmutableList.of("Error1", "Error2")));

        // Invoke
        final String result = setupCompleteAction.execute();

        // Check
        assertEquals(Action.ERROR, result);
        assertEquals(2, setupCompleteAction.getErrorMessages().size());
        assertTrue(setupCompleteAction.getErrorMessages().contains("Error1"));
        assertTrue(setupCompleteAction.getErrorMessages().contains("Error2"));
    }

    @Test
    public void testAJohnsonEventIsNotRaisedIfTheLicenseIsNotTooOldGivenTheUserCanNotBeLoggedInAutomatically()
            throws Exception {
        // Set up
        expectNoUpgradeTaskErrors();
        final SetupComplete licenseValidSetupComplete = new SetupComplete(upgradeService, licenseJohnsonEventRaiser, subTaskManager,
                fieldLayoutManager, null, pluginEventManager, null, setupCompleteRedirectHelper, johnsonProvider, jiraProductInformation) {
            @Override
            protected boolean licenseTooOld() {
                //license is not too old for build
                return false;
            }

            @Override
            void setWikiRendererOnAllRenderableFields() {
            }
        };

        // Invoke
        licenseValidSetupComplete.execute();

        // Check
        assertEquals(DASHBOARD_URL, mockHttpServletResponse.getRedirectedUrl());
    }

    @Test
    public void testAJohnsonEventIsRaisedIfTheLicenseIsNotTooOld() throws Exception {
        // Set up
        final SetupComplete licenseValidSetupComplete = new SetupComplete(upgradeService, licenseJohnsonEventRaiser, subTaskManager,
                fieldLayoutManager, null, pluginEventManager, null, setupCompleteRedirectHelper, johnsonProvider, jiraProductInformation) {
            @Override
            protected boolean licenseTooOld() {
                //license is too old for build
                return true;
            }

            @Override
            void setWikiRendererOnAllRenderableFields() {
            }
        };

        // Invoke
        licenseValidSetupComplete.execute();

        // Check
        assertEquals(mockHttpServletResponse.getRedirectedUrl(), "/blah");
    }
}
