package com.atlassian.jira.web.filters.johnson;

import com.atlassian.jira.web.startup.StartupPageSupport;
import com.atlassian.johnson.Johnson;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.mock.web.MockFilterConfig;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import java.io.IOException;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

@RunWith(MockitoJUnitRunner.class)
public class TestJiraJohnsonFilter {

    private static boolean startupPageSupportLaunched;

    @Mock
    private FilterChain mockFilterChain;

    private JiraJohnsonFilter johnsonFilter;

    @BeforeClass
    public static void beforeSuite() {
        startupPageSupportLaunched = StartupPageSupport.isLaunched();
    }

    @Before
    public void setUp() throws Exception {

        Johnson.initialize("test-johnson-config.xml");
        johnsonFilter = new JiraJohnsonFilter();
        johnsonFilter.init(new MockFilterConfig());

        StartupPageSupport.setLaunched(false);
    }

    @AfterClass
    public static void afterSuite() throws Exception {
        // StartupPageSupport value may interfere other tests, restoring the original value
        StartupPageSupport.setLaunched(startupPageSupportLaunched);
    }

    @Test
    public void verifyNoInterceptForErrorPageWhichIsIgnoredInConfig() throws IOException, ServletException {
        // '/secure/errors.jsp' is ignored in the 'test-johnson-config.xml'
        final MockHttpServletRequest request = new MockHttpServletRequest("GET", "/secure/errors.jsp");
        final MockHttpServletResponse response = new MockHttpServletResponse();
        johnsonFilter.doFilter(request, response, mockFilterChain);

        verify(mockFilterChain).doFilter(eq(request), eq(response));
    }

    @Test
    public void verifyInterceptForTheDashboardPage() throws IOException, ServletException {
        final MockHttpServletRequest request = new MockHttpServletRequest("GET", "/secure/Dashboard.jspa");
        final MockHttpServletResponse response = new MockHttpServletResponse();
        johnsonFilter.doFilter(request, response, mockFilterChain);

        verifyNoMoreInteractions(mockFilterChain);
    }
}
