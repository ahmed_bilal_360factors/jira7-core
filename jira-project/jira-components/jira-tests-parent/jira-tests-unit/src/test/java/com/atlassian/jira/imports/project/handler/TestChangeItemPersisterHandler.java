package com.atlassian.jira.imports.project.handler;

import com.atlassian.jira.exception.ParseException;
import com.atlassian.jira.external.beans.ExternalChangeItem;
import com.atlassian.jira.imports.project.ProjectImportPersister;
import com.atlassian.jira.imports.project.core.BackupProject;
import com.atlassian.jira.imports.project.core.EntityRepresentation;
import com.atlassian.jira.imports.project.core.ProjectImportResultsImpl;
import com.atlassian.jira.imports.project.mapper.ProjectImportMapper;
import com.atlassian.jira.imports.project.mapper.ProjectImportMapperImpl;
import com.atlassian.jira.imports.project.parser.ChangeItemParser;
import com.atlassian.jira.imports.project.transformer.ChangeItemTransformer;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.web.bean.MockI18nBean;
import org.junit.Before;
import org.junit.Test;

import java.util.Map;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @since v3.13
 */
public class TestChangeItemPersisterHandler {
    private static final String ITEM_ID = "12";
    private static final String GROUP_CHANGE_ID = "15";
    private static final String FIELD_TYPE = "jira";
    private static final String FIELD = "security";
    private static final String OLD_VALUE = "10000";
    private static final String OLD_STRING = "level1";
    private static final String NEW_VALUE = "10001";
    private static final String NEW_STRING = "level2";

    private static final String NOT_CHANGE_ITEM_ENTITY = "NOTChangeItem";

    private final ProjectImportMapper projectImportMapper = new ProjectImportMapperImpl(null, null);
    private final ExternalChangeItem externalChangeItem = externalChangeItemWithChangeGroup(GROUP_CHANGE_ID);
    private final ProjectImportResultsImpl projectImportResults = new ProjectImportResultsImpl(0, 0, 0, 0, new MockI18nBean());
    private final ChangeItemParser changeItemParser = mock(ChangeItemParser.class);
    private final ChangeItemTransformer changeItemTransformer = mock(ChangeItemTransformer.class);
    private final ProjectImportPersister projectImportPersister = mock(ProjectImportPersister.class);
    private final BackupProject backupProject = mock(BackupProject.class);
    private final CustomFieldManager customFieldManager = mock(CustomFieldManager.class);
    private final ChangeItemPersisterHandler itemPersisterHandler = new ChangeItemPersisterHandler(projectImportPersister, backupProject, projectImportMapper, projectImportResults, customFieldManager, new ExecutorForTests()) {
        ChangeItemParser getChangeItemParser() {
            return changeItemParser;
        }

        ChangeItemTransformer getChangeItemTransformer() {
            return changeItemTransformer;
        }
    };

    @Before
    public void setUp() throws ParseException {
        when(changeItemParser.parse(isNull(Map.class))).thenReturn(externalChangeItem);
        when(changeItemParser.getEntityRepresentation(eq(externalChangeItem))).thenReturn(null);
    }

    private ExternalChangeItem externalChangeItemWithChangeGroup(final String changeGroup) {
        return new ExternalChangeItem(ITEM_ID, changeGroup, FIELD_TYPE, FIELD, OLD_VALUE, OLD_STRING, NEW_VALUE, NEW_STRING);
    }

    private ExternalChangeItem externalChangeItemWithNullChangeGroup() {
        return externalChangeItemWithChangeGroup(null);
    }

    private void handleTestEntities() throws ParseException, AbortImportException {
        itemPersisterHandler.handleEntity(ChangeItemParser.CHANGE_ITEM_ENTITY_NAME, null);
        itemPersisterHandler.handleEntity(NOT_CHANGE_ITEM_ENTITY, null);
    }

    @Test
    public void testHandle() throws ParseException, AbortImportException {
        // given
        when(changeItemTransformer.transform(projectImportMapper, externalChangeItem)).thenReturn(externalChangeItem);
        when(projectImportPersister.createEntity(isNull(EntityRepresentation.class))).thenReturn(123l);
        // when
        handleTestEntities();
        // then
        assertThat(projectImportResults.getErrors(), empty());
    }

    @Test
    public void testHandleNullTransformedChangeItem() throws ParseException, AbortImportException {
        // given
        when(changeItemTransformer.transform(projectImportMapper, externalChangeItem)).thenReturn(externalChangeItemWithNullChangeGroup());
        // when
        handleTestEntities();
        // then
        assertThat(projectImportResults.getErrors(), empty());
    }

    @Test
    public void testHandleErrorAddingChangeItem() throws ParseException, AbortImportException {
        // given
        when(changeItemTransformer.transform(projectImportMapper, externalChangeItem)).thenReturn(externalChangeItem);
        when(projectImportPersister.createEntity(isNull(EntityRepresentation.class))).thenReturn(null);
        // when
        handleTestEntities();
        // then
        assertThat(projectImportResults.getErrors(), hasSize(1));
        assertThat(projectImportResults.getErrors(), contains(String.format("There was a problem saving change item with id '%s' for change group with id '%s'.", ITEM_ID, GROUP_CHANGE_ID)));
    }
}
