package com.atlassian.jira.web.action.filter;

import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.jira.datetime.DateTimeFormatter;
import com.atlassian.jira.datetime.DateTimeFormatterFactory;
import com.atlassian.jira.datetime.DateTimeStyle;
import com.atlassian.jira.issue.subscription.FilterSubscription;
import com.atlassian.jira.issue.subscription.SubscriptionManager;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.JiraWebActionSupportDependencies;
import com.atlassian.jira.junit.rules.MockHttp;
import com.atlassian.jira.junit.rules.MockitoContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.mock.i18n.MockI18nHelper;
import com.atlassian.jira.mock.servlet.MockHttpServletResponse;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.MockGroup;
import com.atlassian.jira.user.util.UserUtil;
import com.atlassian.jira.util.UriValidator;
import com.google.common.collect.ImmutableSortedSet;
import com.google.common.collect.Lists;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.mockito.Mock;
import org.ofbiz.core.entity.GenericEntityException;
import webwork.action.ServletActionContext;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Locale;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestFilterSubscription {
    @Rule
    public final MockitoContainer mockContainer = MockitoMocksInContainer.rule(this);
    @Rule
    public TestRule webActionSupport = JiraWebActionSupportDependencies.build(mockContainer);

    @Rule
    public final MockHttp mockHttp = MockHttp.withMocks(new MockHttpServletRequest(), new MockHttpServletResponse());

    @Mock
    @AvailableInContainer
    private SubscriptionManager subscriptionManager;

    @Mock
    @AvailableInContainer
    private DateTimeFormatterFactory dateTimeFormatterFactory;

    @Mock
    @AvailableInContainer
    private JiraAuthenticationContext authenticationContext;

    @Mock
    @AvailableInContainer
    private PermissionManager permissionManager;

    @Mock
    @AvailableInContainer
    private GroupManager groupManager;

    @Mock
    @AvailableInContainer
    private UserUtil userUtil;

    @AvailableInContainer
    private UriValidator uriValidator = new UriValidator("UTF-8");

    @Mock
    private DateTimeFormatter dateTimeFormatter;

    @Mock
    private HttpServletResponse servletResponse;

    private ApplicationUser loggedInUser;
    private EditSubscription filterSubscription;

    @Before
    public void setUp() throws Exception {
        filterSubscription = new EditSubscription(null, null, null);
        ServletActionContext.setResponse(servletResponse);
        loggedInUser = new MockApplicationUser("owen");

        when(dateTimeFormatter.withLocale(any(Locale.class))).thenReturn(dateTimeFormatter);
        when(dateTimeFormatter.withStyle(any(DateTimeStyle.class))).thenReturn(dateTimeFormatter);
        when(dateTimeFormatter.format(any(Date.class))).thenReturn("abc");
        when(dateTimeFormatterFactory.formatter()).thenReturn(dateTimeFormatter);

        when(authenticationContext.getI18nHelper()).thenReturn(new MockI18nHelper(Locale.getDefault()));
        when(authenticationContext.getUser()).thenReturn(loggedInUser);
        when(authenticationContext.getUser()).thenReturn(loggedInUser);
    }

    private void setupWithSubscription(Long subscriptionId) throws Exception {
        setupWithSubscriptionAndFilter(subscriptionId, 0L);
    }

    private void setupWithSubscriptionAndFilter(Long subscriptionId, Long filterId) throws Exception {
        final FilterSubscription subscription = mock(FilterSubscription.class);
        when(subscriptionManager.getFilterSubscription(loggedInUser, subscriptionId)).thenReturn(subscription);

        filterSubscription.setSubId(subscriptionId.toString());
        filterSubscription.setFilterId(filterId);
    }

    @Test
    public void testLastTimeAcceptsTimestamps() throws Exception {
        filterSubscription.setLastRun("123456");
        filterSubscription.setLastRun("1378460070");

        Assert.assertEquals(filterSubscription.getLastRun(), "1378460070");
    }

    @Test
    public void testNextRunAcceptsTimestamps() throws Exception {
        filterSubscription.setNextRun("123456");
        filterSubscription.setNextRun("1378460070");

        Assert.assertEquals(filterSubscription.getNextRun(), "1378460070");
    }

    @Test
    public void testGetLastRunStrFormatsDateWithOutlookFormatter() throws Exception {
        filterSubscription.setLastRun("123456");
        filterSubscription.getLastRunStr();

        verify(dateTimeFormatter).withStyle(DateTimeStyle.COMPLETE);
        verify(dateTimeFormatter).format(new Date(123456));
    }

    @Test
    public void testGetNextRunStrFormatsDateWithOutlookFormatter() throws Exception {
        filterSubscription.setNextRun("998877");
        filterSubscription.getNextRunStr();

        verify(dateTimeFormatter).withStyle(DateTimeStyle.COMPLETE);
        verify(dateTimeFormatter).format(new Date(998877));
    }

    @Test
    public void testDoDeleteRedirectsToViewSubscriptions() throws Exception {
        setupWithSubscriptionAndFilter(12345L, 775544L);

        filterSubscription.doDelete();

        verify(servletResponse).sendRedirect("ViewSubscriptions.jspa?filterId=775544");
    }

    @Test
    public void testDoDeleteDeletesSubscription() throws Exception {
        final Long subscriptionId = 111444L;
        setupWithSubscription(subscriptionId);

        filterSubscription.doDelete();

        verify(subscriptionManager).deleteSubscription(subscriptionId);
    }

    @Test
    public void testDoRunNowRunsSubscription() throws Exception {
        final Long subscriptionId = 987L;
        setupWithSubscription(subscriptionId);

        filterSubscription.doRunNow();

        verify(subscriptionManager).runSubscription(loggedInUser, subscriptionId);
    }

    @Test
    public void testDoRunNowSendsRedirectToViewSubscriptions() throws Exception {
        final Long filterId = 2345L;
        setupWithSubscriptionAndFilter(1L, filterId);

        filterSubscription.doRunNow();

        verify(servletResponse).sendRedirect("ViewSubscriptions.jspa?filterId=" + filterId);
    }

    @Test
    public void testHasPermissionChecksRightPermissionForLoggedInUser() throws Exception {
        when(permissionManager.hasPermission(Permissions.MANAGE_GROUP_FILTER_SUBSCRIPTIONS, loggedInUser)).thenReturn(true);
        assertTrue("hasPermissions should return true for logged in user", filterSubscription.hasGroupPermission());
    }

    @Test
    public void testGetSubmitNameUsesI18n() throws GenericEntityException {
        assertEquals("filtersubscription.subscribe", filterSubscription.getSubmitName());
        filterSubscription.setSubId("1");
        assertEquals("common.forms.update", filterSubscription.getSubmitName());
    }

    @Test
    public void testCancelStrReturnsProperLink() throws Exception {
        assertEquals("ManageFilters.jspa", filterSubscription.getCancelStr());

        filterSubscription.setSubId("2");
        filterSubscription.setFilterId(1L);
        assertEquals("ViewSubscriptions.jspa?filterId=1", filterSubscription.getCancelStr());

        filterSubscription.setReturnUrl("abc");
        assertEquals("abc", filterSubscription.getCancelStr());
    }

    @Test
    public void testGetGroupsForAdministratorReturnsAllGroups() throws Exception {
        final ArrayList<Group> allGroups = Lists.<Group>newArrayList(new MockGroup("G1"), new MockGroup("G2"), new MockGroup("G3"));
        when(permissionManager.hasPermission(Permissions.ADMINISTER, loggedInUser)).thenReturn(true);
        when(groupManager.getAllGroups()).thenReturn(allGroups);

        final Collection<String> groups = filterSubscription.getGroups();

        assertThat(groups, Matchers.contains("G1", "G2", "G3"));
    }

    @Test
    public void testGetGroupsForRegularUserReturnsOnlyHisGroups() throws Exception {
        when(permissionManager.hasPermission(Permissions.ADMINISTER, loggedInUser)).thenReturn(false);
        when(userUtil.getGroupNamesForUser(loggedInUser.getName())).thenReturn(ImmutableSortedSet.of("G001", "G002"));

        final Collection<String> groups = filterSubscription.getGroups();

        assertThat(groups, Matchers.contains("G001", "G002"));
    }

    private static class MockHttpServletRequest extends com.atlassian.jira.mock.servlet.MockHttpServletRequest {
        @Override
        public String getScheme() {
            return "http";
        }
    }
}
