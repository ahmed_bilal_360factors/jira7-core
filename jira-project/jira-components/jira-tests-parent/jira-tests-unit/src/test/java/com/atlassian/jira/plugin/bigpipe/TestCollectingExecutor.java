package com.atlassian.jira.plugin.bigpipe;

import org.junit.Test;

import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;

public class TestCollectingExecutor {
    private final Runnable emptyRunnable = () -> {
    };

    @Test
    public void singleExecuteAndPop() {
        CollectingExecutor ce = new CollectingExecutor();
        PrioritizedRunnable r1 = new PrioritizedRunnable(100, emptyRunnable);
        ce.execute(r1);

        Optional<Runnable> p = ce.pop();
        assertThat(p.isPresent(), is(true));
        assertThat(p.get(), sameInstance(r1));

        Optional<Runnable> empty = ce.pop();
        assertThat(empty.isPresent(), is(false));
    }


    @Test
    public void prioritiesHaveEffect() {
        CollectingExecutor ce = new CollectingExecutor();
        PrioritizedRunnable r1 = new PrioritizedRunnable(100, emptyRunnable);
        PrioritizedRunnable r2 = new PrioritizedRunnable(200, emptyRunnable);
        PrioritizedRunnable r3 = new PrioritizedRunnable(150, emptyRunnable);
        ce.execute(r1);
        ce.execute(r2);
        ce.execute(r3);

        Optional<Runnable> p1 = ce.pop();
        assertThat(p1.isPresent(), is(true));
        assertThat(p1.get(), sameInstance(r2));
        Optional<Runnable> p2 = ce.pop();
        assertThat(p2.isPresent(), is(true));
        assertThat(p2.get(), sameInstance(r3));
        Optional<Runnable> p3 = ce.pop();
        assertThat(p3.isPresent(), is(true));
        assertThat(p3.get(), sameInstance(r1));
        Optional<Runnable> empty = ce.pop();
        assertThat(empty.isPresent(), is(false));
    }

    @Test
    public void worksWithOrdinaryRunnables() {
        //Counter that is incremented - an action we can track
        AtomicLong counter = new AtomicLong();

        CollectingExecutor ce = new CollectingExecutor();
        ce.execute(counter::incrementAndGet);

        //The returned runnable will be wrapped so we can't check for equality
        //but we can run it and verify the underlying runnable was executed
        Optional<Runnable> p = ce.pop();
        assertThat(p.isPresent(), is(true));
        p.get().run();

        assertThat(counter.get(), is(1L));
    }

    @Test(expected = IllegalStateException.class)
    public void closingExecutorPreventsFurtherExecution() {
        CollectingExecutor ce = new CollectingExecutor();
        ce.close();
        ce.execute(emptyRunnable);
    }
}
