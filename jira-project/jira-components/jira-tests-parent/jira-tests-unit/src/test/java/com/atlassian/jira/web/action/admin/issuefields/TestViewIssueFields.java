package com.atlassian.jira.web.action.admin.issuefields;

import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.I18nHelper;

public class TestViewIssueFields extends AbstractTestViewIssueFields {
    private ViewIssueFields viewIssueFields;

    public TestViewIssueFields() {
        super();
    }

    public void setNewVif() {
        viewIssueFields = new ViewIssueFields(null, null, reindexMessageManager, fieldManager, fieldLayoutManager, fieldLayoutSchemeHelper, null, null, managedConfigurationItemService) {
            @Override
            protected I18nHelper getI18nHelper() {
                return i18Helper;
            }

            @Override
            public ApplicationUser getLoggedInUser() {
                return mockUser;
            }
        };
    }

    public AbstractConfigureFieldLayout getVif() {
        if (viewIssueFields == null) {
            setNewVif();
        }
        return viewIssueFields;
    }

    public void refreshVif() {
        viewIssueFields = null;
    }
}
