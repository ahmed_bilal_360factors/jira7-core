package com.atlassian.jira.imports.project.validation;

import com.atlassian.jira.imports.project.core.BackupProject;
import com.atlassian.jira.issue.fields.TextFieldCharacterLengthValidator;
import com.atlassian.jira.util.I18nHelper;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static com.atlassian.jira.util.MessageSetAssert.assert1ErrorNoWarnings;
import static com.atlassian.jira.util.MessageSetAssert.assertNoMessages;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

public class TestSystemFieldsMaxTextLengthValidator {

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @Mock
    private TextFieldCharacterLengthValidator textFieldCharacterLengthValidator;

    @Mock
    private I18nHelper i18nHelper;

    @Mock
    private BackupProject backupProject;

    @InjectMocks
    private SystemFieldsMaxTextLengthValidator validator;

    @Test
    public void validateReturnsNullWhenMaxSystemFieldLengthDoesNotExceedLimit() throws Exception {
        assertNoMessages(validator.validate(backupProject, null));
    }

    @Test
    public void validateReturnsErrorWhenMaxSystemFieldLengthExceedTheLimit() throws Exception {
        when(backupProject.getSystemFieldMaxTextLength()).thenReturn(1000);
        when(textFieldCharacterLengthValidator.isTextTooLong(1000)).thenReturn(true);

        final String expectedErrorMessage = "This will be expected error message";
        when(i18nHelper.getText(eq("admin.errors.project.import.field.text.too.long"), anyString(), anyString())).thenReturn(expectedErrorMessage);

        assert1ErrorNoWarnings(validator.validate(backupProject, i18nHelper), expectedErrorMessage);
    }


}