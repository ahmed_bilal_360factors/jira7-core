package com.atlassian.jira.issue.fields.rest.json.serializer;

import com.atlassian.jira.bc.issue.comment.CommentService;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.comments.Comment;
import com.atlassian.jira.issue.comments.MockComment;
import com.atlassian.jira.issue.fields.rest.json.beans.CommentJsonBean;
import com.atlassian.jira.issue.fields.rest.json.beans.StreamingCommentsJsonBean;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.IOException;
import java.io.StringWriter;
import java.util.stream.Stream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestStreamingCommentsJsonBeanSerializer {
    @Rule
    public MockitoContainer container = MockitoMocksInContainer.rule(this);

    @AvailableInContainer
    @Mock
    private CommentService commentService;

    @AvailableInContainer
    @Mock
    private JiraAuthenticationContext authenticationContext;

    @Mock
    private Issue issue;

    private StreamingCommentsJsonBeanSerializer serializer;

    private StringWriter stringWriter;
    private JsonGenerator jsonGenerator;

    @Before
    public void setup() throws IOException {
        MockApplicationUser user = new MockApplicationUser("user");
        when(authenticationContext.getLoggedInUser()).thenReturn(user);
        when(commentService.streamComments(eq(user), eq(issue))).thenReturn(Stream.of(new MockComment("author1", "body1"), new MockComment("author2", "body2")));

        stringWriter = new StringWriter();
        jsonGenerator = new JsonFactory(new ObjectMapper()).createJsonGenerator(stringWriter);

        serializer = new TestableStreamingCommentsJsonBeanSerializer();
    }

    @Test
    public void serializesStreamingCommentsWithoutRendering() throws IOException {
        serializer.serialize(new StreamingCommentsJsonBean(issue, false, "rendererType"), jsonGenerator, null);

        jsonGenerator.close();
        assertThat(stringWriter.toString(), is("{\"comments\":[{\"self\":null,\"id\":\"1\",\"author\":null,\"body\":\"body1\",\"renderedBody\":null,\"updateAuthor\":null,\"created\":null,\"updated\":null,\"visibility\":null,\"properties\":null},{\"self\":null,\"id\":\"1\",\"author\":null,\"body\":\"body2\",\"renderedBody\":null,\"updateAuthor\":null,\"created\":null,\"updated\":null,\"visibility\":null,\"properties\":null}],\"maxResults\":2,\"total\":2,\"startAt\":0}"));
    }

    @Test
    public void serializesStreamingCommentsWithRendering() throws IOException {
        serializer.serialize(new StreamingCommentsJsonBean(issue, true, "rendererType"), jsonGenerator, null);

        jsonGenerator.close();
        assertThat(stringWriter.toString(), is("{\"comments\":[{\"self\":null,\"id\":null,\"author\":null,\"body\":null,\"renderedBody\":\"renderedbody1\",\"updateAuthor\":null,\"created\":null,\"updated\":null,\"visibility\":null,\"properties\":null},{\"self\":null,\"id\":null,\"author\":null,\"body\":null,\"renderedBody\":\"renderedbody2\",\"updateAuthor\":null,\"created\":null,\"updated\":null,\"visibility\":null,\"properties\":null}],\"maxResults\":2,\"total\":2,\"startAt\":0}"));
    }

    private class TestableStreamingCommentsJsonBeanSerializer extends StreamingCommentsJsonBeanSerializer {
        @Override
        protected CommentJsonBean getShortCommentJsonBean(Comment comment, ApplicationUser loggedInUser) {
            CommentJsonBean commentJsonBean = new CommentJsonBean();
            commentJsonBean.setBody(comment.getBody());
            commentJsonBean.setId("1");

            return commentJsonBean;
        }

        @Override
        protected CommentJsonBean getRenderedCommentJsonBean(StreamingCommentsJsonBean serializableCommentsBean, Comment comment, ApplicationUser loggedInUser) {
            CommentJsonBean commentJsonBean = new CommentJsonBean();
            commentJsonBean.setRenderedBody("rendered" + comment.getBody());

            return commentJsonBean;
        }
    }
}