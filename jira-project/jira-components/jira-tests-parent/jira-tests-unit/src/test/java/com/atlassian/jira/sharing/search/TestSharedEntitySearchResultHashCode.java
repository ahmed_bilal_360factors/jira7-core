package com.atlassian.jira.sharing.search;

import com.atlassian.jira.junit.rules.MockitoContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.sharing.MockSharedEntity;
import com.atlassian.jira.sharing.SharedEntity;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.collect.MockCloseableIterable;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.mockito.Mock;

import javax.annotation.Nonnull;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(Parameterized.class)
public class TestSharedEntitySearchResultHashCode {

    private static final long PAGE_SIZE = 10L;

    @Rule
    public MockitoContainer container = MockitoMocksInContainer.rule(this);
    @Mock
    private ApplicationUser owner;
    private SharedEntity.SharePermissions permissions = SharedEntity.SharePermissions.GLOBAL;

    private final List<Long> firstIds;
    private final List<Long> secondIds;
    private final boolean expectedHash;

    /**
     * Create parameterized tests from the data supplied in {@link #parameterData()}.
     *
     * @param testIdentifier the first array element. It's unused in the test itself but the Runner uses it to name the test case.
     * @param firstIds       the second array element. A list of IDs is used to represent the results that would be encapsulated in {@link SharedEntitySearchResult}.
     * @param secondIds      the third array element. A list of IDs is used to represent the results that would be encapsulated in {@link SharedEntitySearchResult}.
     * @param expectedHash   whether we expect the firstIds and secondIds to generate equal hashes.
     */
    public TestSharedEntitySearchResultHashCode(@SuppressWarnings("UnusedParameters") String testIdentifier,
                                                List<Long> firstIds, List<Long> secondIds, boolean expectedHash) {
        this.firstIds = firstIds;
        this.secondIds = secondIds;
        this.expectedHash = expectedHash;
    }

    @Parameterized.Parameters(name = "Scenario: {0}")
    public static Iterable<?> parameterData() {
        return Arrays.asList(new Object[][]{
                {"no results",
                        Lists.newArrayList(),
                        Lists.newArrayList(),
                        Boolean.TRUE},
                {"one mismatch result",
                        Lists.newArrayList(1L),
                        Lists.newArrayList(2L),
                        Boolean.FALSE},
                {"short results",
                        Lists.newArrayList(1L, 2L, 3L),
                        Lists.newArrayList(1L, 2L, 3L),
                        Boolean.TRUE},
                {"short results rearranged",
                        Lists.newArrayList(1L, 3L, 2L),
                        Lists.newArrayList(1L, 2L, 3L),
                        Boolean.FALSE},
                {"short results repeated",
                        Lists.newArrayList(1L, 1L, 1L),
                        Lists.newArrayList(1L, 1L, 1L),
                        Boolean.TRUE},
                {"medium result mismatch at end",
                        Lists.newArrayList(1L, 2L, 3L, 4L, 5L, 6L, 7L, 8L, 9L, 10L),
                        Lists.newArrayList(1L, 2L, 3L, 4L, 5L, 6L, 7L, 8L, 9L, 99L),
                        Boolean.FALSE},
                // after the 10th item, results will not be compared as they are outside the page size
                {"long result mismatch not in page",
                        Lists.newArrayList(1L, 2L, 3L, 4L, 5L, 6L, 7L, 8L, 9L, 10L, 11L),
                        Lists.newArrayList(1L, 2L, 3L, 4L, 5L, 6L, 7L, 8L, 9L, 10L, 99L),
                        Boolean.TRUE},
                {"long result different sizes",
                        Lists.newArrayList(1L, 2L, 3L, 4L, 5L, 6L, 7L, 8L, 9L, 10L, 11L, 12L),
                        Lists.newArrayList(1L, 2L, 3L, 4L, 5L, 6L, 7L, 8L, 9L, 10L, 11L),
                        Boolean.FALSE},
        });
    }

    @Before
    public void setUp() {
        when(owner.getKey()).thenReturn("admin");
    }

    @Test
    public void testHashCode() throws Exception {
        SharedEntitySearchResult<MockSharedEntity> firstResult = buildResults(this.firstIds);
        SharedEntitySearchResult<MockSharedEntity> secondResult = buildResults(this.secondIds);
        if (this.expectedHash) {
            assertThat("Expected hashCodes to be equal", secondResult.hashCode(), is(firstResult.hashCode()));
        } else {
            assertThat("Expected hashCodes to not be equal", secondResult.hashCode(), not(is(firstResult.hashCode())));
        }
    }

    private SharedEntitySearchResult<MockSharedEntity> buildResults(@Nonnull final List<Long> ids) {
        final List<MockSharedEntity> sharedEntities = ids.stream()
                .limit(PAGE_SIZE) // simulate paged results
                .map(aLong -> new MockSharedEntity(aLong, owner, permissions))
                .collect(Collectors.toList());
        final boolean hasNext = ids.size() > PAGE_SIZE;
        return new SharedEntitySearchResult<>(new MockCloseableIterable<>(sharedEntities), hasNext, ids.size());
    }
}
