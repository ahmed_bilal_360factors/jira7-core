package com.atlassian.jira.imports.project.populator;

import com.atlassian.jira.external.beans.ExternalComment;
import com.atlassian.jira.imports.project.core.BackupOverviewBuilder;
import com.atlassian.jira.imports.project.parser.CommentParser;
import org.hamcrest.Matchers;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Map;

import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

public class TestCommentPopulator {
    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @Mock
    private CommentParser mockCommentParser;

    @Mock
    private ExternalComment comment;

    @Mock
    private Map attributes;

    @Mock
    BackupOverviewBuilder backupOverviewBuilder;

    @Test
    public void getCommentParserReturnsCommentParser() {
        assertThat((new CommentPopulator()).getCommentParser(), Matchers.instanceOf(CommentParser.class));
    }

    @Test
    public void populateCreatesCommentInBackupOverviewBuilder() throws Exception {
        when(mockCommentParser.parse(attributes)).thenReturn(comment);
        getTestCommentPopulator().populate(backupOverviewBuilder, "Action", attributes);

        verify(backupOverviewBuilder).addComment(comment);
    }

    @Test
    public void ignoresUnrecognizedElementName() throws Exception {
        getTestCommentPopulator().populate(backupOverviewBuilder, "Issue", attributes);

        verifyNoMoreInteractions(backupOverviewBuilder);
    }

    public CommentPopulator getTestCommentPopulator() {
        return new CommentPopulator() {
            CommentParser getCommentParser() {
                return mockCommentParser;
            }
        };
    }

}