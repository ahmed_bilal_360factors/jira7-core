package com.atlassian.jira.application;

import com.atlassian.jira.mock.security.MockSimpleAuthenticationContext;
import com.atlassian.jira.user.MockApplicationUser;
import org.junit.Test;

import static com.atlassian.jira.util.NoopI18nHelper.makeTranslation;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

public class JiraI18nResolverTest {
    private MockSimpleAuthenticationContext ctx
            = MockSimpleAuthenticationContext.createNoopContext(new MockApplicationUser("brenden"));

    private JiraI18nResolver i18n = new JiraI18nResolver(ctx);

    @Test
    public void getTextDelegates() {
        assertThat(i18n.getText("translate"), equalTo(makeTranslation("translate")));
    }

    @Test
    public void getTextDelegatesArguments() {
        assertThat(i18n.getText("translate", 1, 2, true), equalTo(makeTranslation("translate", 1, 2, true)));
    }
}