package com.atlassian.jira.license;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.extras.api.LicenseException;
import com.atlassian.extras.decoder.api.LicenseDecoder;
import com.atlassian.extras.decoder.v2.Version2LicenseDecoder;
import com.atlassian.jira.application.ApplicationRoleDefinitions;
import com.atlassian.jira.application.MockApplicationRoleDefinition;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.test.util.lic.License;
import com.atlassian.jira.test.util.lic.cloud.CloudLicenses;
import com.google.common.collect.ImmutableSet;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.mockito.Mock;

import java.util.Set;

import static com.atlassian.extras.common.LicensePropertiesConstants.UNLIMITED_USERS;
import static com.atlassian.fugue.Option.option;
import static com.atlassian.jira.license.Licenses.LICENSE_JIRA_HARDCORE;
import static com.atlassian.jira.license.Licenses.LICENSE_THREE_ROLES;
import static com.atlassian.jira.license.Licenses.LICENSE_WITH_0_APPS;
import static com.atlassian.jira.license.Licenses.LICENSE_WITH_1_APP;
import static com.atlassian.jira.license.Licenses.LICENSE_WITH_3_APPS;
import static com.atlassian.jira.license.Licenses.LICENSE_WITH_SOFTWARE_APP;
import static com.atlassian.jira.license.Licenses.LICENSE_WITH_SOFTWARE_UNLIMITED_USERS;
import static com.atlassian.jira.test.util.lic.ApplicationLicenseConstants.CORE_KEY;
import static com.atlassian.jira.test.util.lic.ApplicationLicenseConstants.SERVICE_DESK_KEY;
import static com.atlassian.jira.test.util.lic.ApplicationLicenseConstants.SOFTWARE_KEY;
import static com.atlassian.jira.test.util.lic.core.CoreLicenses.LICENSE_CORE;
import static com.atlassian.jira.test.util.lic.ela.ElaLicenses.LICENSE_ELA_CORE_SW_SD_TEST_REF_OTHER;
import static com.atlassian.jira.test.util.lic.other.OtherLicenses.LICENSE_OTHER;
import static com.atlassian.jira.test.util.lic.other.OtherLicenses.LICENSE_PRE_JIRA_APP_V2_COMMERCIAL;
import static com.atlassian.jira.test.util.lic.sd.ServiceDeskLicenses.LICENSE_SERVICE_DESK_ABP;
import static com.atlassian.jira.test.util.lic.sd.ServiceDeskLicenses.LICENSE_SERVICE_DESK_ABP_EVAL;
import static com.atlassian.jira.test.util.lic.sd.ServiceDeskLicenses.LICENSE_SERVICE_DESK_BAD_LEGACY;
import static com.atlassian.jira.test.util.lic.sd.ServiceDeskLicenses.LICENSE_SERVICE_DESK_RBP;
import static com.atlassian.jira.test.util.lic.sd.ServiceDeskLicenses.LICENSE_SERVICE_DESK_RBP_EVAL;
import static com.atlassian.jira.test.util.lic.sd.ServiceDeskLicenses.LICENSE_SERVICE_DESK_TBP;
import static com.atlassian.jira.test.util.lic.sd.ServiceDeskLicenses.LICENSE_SERVICE_DESK_TBP_EVAL;
import static com.atlassian.jira.test.util.lic.sd.ServiceDeskLicenses.LICENSE_SERVICE_DESK_TBP_UNLIMITED;
import static com.atlassian.jira.test.util.lic.software.SoftwareLicenses.LICENSE_SOFTWARE;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;


/**
 * Tests {@link DefaultLicensedApplications}.
 *
 * @since v6.3
 */
public class TestDefaultLicensedApplications {
    // required for DefaultLicensedApplications#getDescription
    @Mock
    @AvailableInContainer
    ApplicationRoleDefinitions roleDefinitions;
    @Rule
    public RuleChain initMyMockage = MockitoMocksInContainer.forTest(this);

    private static final LicenseDecoder LICENSE_DECODER = new Version2LicenseDecoder();

    private static final ApplicationKey SOFTWARE = ApplicationKey.valueOf(SOFTWARE_KEY);
    private static final ApplicationKey SERVICE_DESK = ApplicationKey.valueOf(SERVICE_DESK_KEY);
    private static final ApplicationKey CORE = ApplicationKey.valueOf(CORE_KEY);

    @Test
    public void testSingleRoleIsDetected() {
        LicensedApplications lic = newFromLicense(LICENSE_WITH_1_APP);
        Set<ApplicationKey> app = lic.getKeys();

        assertEquals("1 application role is detected", 1, app.size());

        assertEquals(
                "Role '" + SOFTWARE.value() + "' provides 69 seats",
                69, lic.getUserLimit(SOFTWARE));

        assertEquals(
                "Role '" + SERVICE_DESK.value() + "' not in license",
                0, lic.getUserLimit(SERVICE_DESK));

        assertEquals(
                "Role '" + CORE.value() + "' not in license",
                0, lic.getUserLimit(CORE));
    }

    @Test
    public void testMultipleRolesAreDetected() {
        LicensedApplications lic = newFromLicense(LICENSE_WITH_3_APPS);
        Set<ApplicationKey> app = lic.getKeys();

        assertEquals("3 application roles are detected", 3, app.size());

        assertEquals(
                "Role '" + SOFTWARE.value() + "' provides 33 seats",
                33, lic.getUserLimit(SOFTWARE));

        assertEquals(
                "Role '" + SERVICE_DESK.value() + "' provides 22 seats",
                22, lic.getUserLimit(SERVICE_DESK));

        assertEquals(
                "Role '" + CORE.value() + "' provides 11 seats",
                11, lic.getUserLimit(CORE));

        String desc = lic.getDescription();
        assertThat(desc, containsString("JIRA Core"));
        assertThat(desc, containsString("JIRA Software"));
        assertThat(desc, containsString("JIRA Service Desk"));
    }

    @Test
    public void testUnlimitedRoleLimits() {
        LicensedApplications lic = newFromLicense(LICENSE_WITH_SOFTWARE_UNLIMITED_USERS);

        Set<ApplicationKey> app = lic.getKeys();

        assertEquals("3 application roles are detected", 3, app.size());

        assertEquals(
                "Role '" + SOFTWARE.value() + "' provides unlimited seats",
                UNLIMITED_USERS, lic.getUserLimit(SOFTWARE));

        assertEquals(
                "Role '" + SERVICE_DESK.value() + "' not in license",
                0, lic.getUserLimit(SERVICE_DESK));

        assertEquals(
                "Role '" + CORE.value() + "' provides 1 seat",
                1, lic.getUserLimit(CORE));
    }

    @Test
    public void shouldInterpretOldLicenseAsSoftwareRoleLicense() {
        LicensedApplications lic = newFromLicense(LICENSE_WITH_0_APPS);
        assertEquals(ImmutableSet.of(SOFTWARE), lic.getKeys());
        assertEquals(101, lic.getUserLimit(SOFTWARE));
        assertEquals("JIRA Software", lic.getDescription());
    }

    @Test
    public void shouldInterpretOldOnDemandWithSdLicenseAsSoftwareAndServiceDeskRoleLicense() {
        LicensedApplications lic = newFromLicense(CloudLicenses.LICENSE_ONDEMAND_6x_SERVICEDESK_TBP);

        assertThat(lic.getKeys(), containsInAnyOrder(SOFTWARE, SERVICE_DESK));
        assertEquals(-1, lic.getUserLimit(SOFTWARE));
        assertEquals(-1, lic.getUserLimit(SERVICE_DESK));

        String desc = lic.getDescription();
        assertThat(desc, containsString("JIRA Software"));
        assertThat(desc, containsString("JIRA Service Desk"));
    }

    @Test
    public void shouldReturnActualSoftwareRoleCompatibleRoleLicense() {
        LicensedApplications lic = newFromLicense(LICENSE_WITH_SOFTWARE_APP);
        assertEquals(ImmutableSet.of(SOFTWARE), lic.getKeys());
        assertEquals(22, lic.getUserLimit(SOFTWARE));
        assertEquals("JIRA Software", lic.getDescription());
    }

    // The following Service Desk (SD) tests uses the following acronyms:
    //   RBP: Role Based Pricing (i.e. JIRA SD 7.x licenses)
    //   ABP: Agent Based Pricing (i.e. SD 2.x licenses)
    //   TBP: Tier Base Pricing (i.e. SD 1.x licenses)
    // See ServiceDeskLicenses for more details of the different licenses.

    @Test
    public void shouldReturnServiceDeskRoleForServiceDeskTBPUnlimitedLicense() {
        LicensedApplications lic = newFromLicense(LICENSE_SERVICE_DESK_TBP_UNLIMITED);

        assertEquals(ImmutableSet.of(SERVICE_DESK), lic.getKeys());
        assertEquals(UNLIMITED_USERS, lic.getUserLimit(SERVICE_DESK));
        assertEquals("JIRA Service Desk", lic.getDescription());
    }

    @Test
    public void shouldReturnServiceDeskRoleForServiceDeskTBPLimitedLicense() {
        LicensedApplications lic = newFromLicense(LICENSE_SERVICE_DESK_TBP);

        assertEquals(ImmutableSet.of(SERVICE_DESK), lic.getKeys());
        assertEquals(13, lic.getUserLimit(SERVICE_DESK));
        assertEquals("JIRA Service Desk", lic.getDescription());
    }

    @Test
    public void shouldReturnServiceDeskRoleForServiceDeskABPLicense() {
        LicensedApplications lic = newFromLicense(LICENSE_SERVICE_DESK_ABP);

        assertEquals(ImmutableSet.of(SERVICE_DESK), lic.getKeys());
        assertEquals(100, lic.getUserLimit(SERVICE_DESK));
        assertEquals("JIRA Service Desk", lic.getDescription());
    }

    @Test
    public void shouldReturnServiceDeskRoleForServiceDeskRBPLicense() {
        LicensedApplications lic = newFromLicense(LICENSE_SERVICE_DESK_RBP);

        assertEquals(ImmutableSet.of(SERVICE_DESK), lic.getKeys());
        assertEquals(10, lic.getUserLimit(SERVICE_DESK));
        assertEquals("JIRA Service Desk", lic.getDescription());
    }

    @Test
    public void shouldReturnServiceDeskRoleForServiceDeskTBPEvalLicense() {
        LicensedApplications lic = newFromLicense(LICENSE_SERVICE_DESK_TBP_EVAL);

        assertEquals(ImmutableSet.of(SERVICE_DESK), lic.getKeys());
        assertEquals(UNLIMITED_USERS, lic.getUserLimit(SERVICE_DESK));
        assertEquals("JIRA Service Desk", lic.getDescription());
    }

    @Test
    public void shouldReturnServiceDeskRoleForServiceDeskABPEvalLicense() {
        LicensedApplications lic = newFromLicense(LICENSE_SERVICE_DESK_ABP_EVAL);

        assertEquals(ImmutableSet.of(SERVICE_DESK), lic.getKeys());
        assertEquals(UNLIMITED_USERS, lic.getUserLimit(SERVICE_DESK));
        assertEquals("JIRA Service Desk", lic.getDescription());
    }

    @Test
    public void shouldReturnServiceDeskRoleForServiceDeskRBPEvalLicense() {
        LicensedApplications lic = newFromLicense(LICENSE_SERVICE_DESK_RBP_EVAL);

        assertEquals(ImmutableSet.of(SERVICE_DESK), lic.getKeys());
        assertEquals(UNLIMITED_USERS, lic.getUserLimit(SERVICE_DESK));
    }

    @Test(expected = LicenseException.class)
    public void shouldErrorOutOnLegacyServiceDeskLicenseWithNoUserCount() {
        //The UPM actually parses this type of license an an unlimited agent license. I decided to throw an error which
        //seems safer.
        new DefaultLicensedApplications(LICENSE_SERVICE_DESK_BAD_LEGACY.getLicenseString(), LICENSE_DECODER);
    }

    @Test
    public void getDescriptionUsesLicenseDescriptionPropertyIfAvailable() {
        LicensedApplications lic = newFromLicense(LICENSE_JIRA_HARDCORE);
        assertThat(lic.getDescription(), equalTo("JIRA HARDCORE"));
    }

    @Test
    public void getDescriptionUsesInstalledApplicationNameIfAvailable() {
        LicensedApplications lic = newFromLicense(LICENSE_THREE_ROLES);

        assertThat(lic.getDescription(), containsString("JIRA Core"));

        // make Core plugin appear installed
        MockApplicationRoleDefinition coreDef = new MockApplicationRoleDefinition(CORE.value(), "JIRA Korr");
        when(roleDefinitions.getDefined(CORE)).thenReturn(option(coreDef));

        assertThat(lic.getDescription(), containsString("JIRA Korr"));
    }

    private static DefaultLicensedApplications newFromLicense(final License license) {
        return new DefaultLicensedApplications(license.getLicenseString(), LICENSE_DECODER);
    }

    @Test
    public void shouldDetermineThatLicensesHasBeenIssuedAsRoleBasedLicense() {
        assertTrue(newFromLicense(LICENSE_WITH_1_APP).hasNativeRole());
        assertTrue(newFromLicense(LICENSE_WITH_3_APPS).hasNativeRole());
        assertTrue(newFromLicense(LICENSE_WITH_SOFTWARE_APP).hasNativeRole());
        assertTrue(newFromLicense(LICENSE_SERVICE_DESK_RBP).hasNativeRole());
        assertTrue(newFromLicense(LICENSE_CORE).hasNativeRole());
        assertTrue(newFromLicense(LICENSE_ELA_CORE_SW_SD_TEST_REF_OTHER).hasNativeRole());
        assertTrue(newFromLicense(LICENSE_OTHER).hasNativeRole());
        assertTrue(newFromLicense(LICENSE_SOFTWARE).hasNativeRole());
    }

    @Test
    public void shouldDetermineThatLicensesHasNotBeenIssuedAsRoleBasedLicense() {
        assertFalse(newFromLicense(LICENSE_SERVICE_DESK_TBP).hasNativeRole());
        assertFalse(newFromLicense(LICENSE_SERVICE_DESK_ABP).hasNativeRole());
        assertFalse(newFromLicense(LICENSE_SERVICE_DESK_TBP).hasNativeRole());
        assertFalse(newFromLicense(LICENSE_PRE_JIRA_APP_V2_COMMERCIAL).hasNativeRole());
    }
}
