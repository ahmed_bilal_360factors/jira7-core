package com.atlassian.jira.web.action.admin.customfields;

import com.atlassian.jira.issue.context.JiraContextNode;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.config.FieldConfigScheme;
import com.atlassian.jira.issue.fields.config.manager.FieldConfigSchemeManager;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.issue.search.SearchProvider;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.mock.ofbiz.MockGenericValue;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.query.Query;
import com.atlassian.query.QueryImpl;
import com.atlassian.query.clause.AndClause;
import com.atlassian.query.clause.NotClause;
import com.atlassian.query.clause.TerminalClause;
import com.atlassian.query.clause.TerminalClauseImpl;
import com.atlassian.query.operand.MultiValueOperand;
import com.atlassian.query.operator.Operator;
import com.atlassian.query.order.OrderByImpl;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.mockito.Mock;
import org.ofbiz.core.entity.GenericValue;

import java.util.Collections;
import java.util.List;
import java.util.Set;

import static com.google.common.collect.ImmutableList.copyOf;
import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Sets.newHashSet;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestCustomFieldContextConfigHelperImpl {
    @Rule
    public final RuleChain mockitoMocks = MockitoMocksInContainer.forTest(this);

    @Mock
    private SearchProvider searchProvider;
    @Mock
    private FieldConfigSchemeManager fieldConfigSchemeManager;
    @Mock
    private CustomField customField;

    private final ApplicationUser user = null;

    @Test
    public void testDoesContextHaveIssuesWithContextObjects() throws Exception {
        final JiraContextNode context1 = mock(JiraContextNode.class);
        when(context1.getProjectId()).thenReturn(null);

        final JiraContextNode context2 = mock(JiraContextNode.class);
        when(context2.getProjectId()).thenReturn(555L);

        final IssueType gv1 = mockIssueType(666L);

        final boolean contextHasIssues = true;
        final CustomFieldContextConfigHelperImpl helper = new CustomFieldContextConfigHelperImpl(searchProvider, fieldConfigSchemeManager) {
            @Override
            boolean _doesContextHaveIssues(final ApplicationUser user, final List<Long> projectIds, final List<Long> issueTypeIds) {
                assertEquals(ImmutableList.of(555L), copyOf(projectIds));
                assertEquals(ImmutableList.of(666L), issueTypeIds);
                return contextHasIssues;
            }
        };

        final boolean result = helper.doesContextHaveIssues(user, newArrayList(context1, context2, null), newArrayList(gv1, null));
        assertEquals(contextHasIssues, result);

        verify(context1).getProjectId();
        verify(context2, times(2)).getProjectId();
    }

    @Test
    public void testDoesContextHaveIssuesWithContextObjectsNull() throws Exception {
        final JiraContextNode context1 = mock(JiraContextNode.class);
        when(context1.getProjectId()).thenReturn(null);

        final JiraContextNode context2 = mock(JiraContextNode.class);
        when(context2.getProjectId()).thenReturn(555L);

        final boolean contextHasIssues = true;
        final CustomFieldContextConfigHelperImpl helper = new CustomFieldContextConfigHelperImpl(searchProvider, fieldConfigSchemeManager) {
            @Override
            boolean _doesContextHaveIssues(final ApplicationUser user, final List<Long> projectIds, final List<Long> issueTypeIds) {
                assertEquals(ImmutableList.of(555L), copyOf(projectIds));
                assertEquals(Collections.<Long>emptyList(), issueTypeIds);
                return contextHasIssues;
            }
        };

        final boolean result = helper.doesContextHaveIssues(user, newArrayList(context1, context2, null), null);
        assertEquals(contextHasIssues, result);

        verify(context1).getProjectId();
        verify(context2, times(2)).getProjectId();
    }

    protected IssueType mockIssueType(final Long id) {
        final IssueType it = mock(IssueType.class);
        when(it.getId()).thenReturn(Long.toString(id));
        return it;
    }

    @Test
    public void testDoesContextHaveIssuesWithGenericValues() throws Exception {
        final GenericValue project1 = new MockGenericValue("project", ImmutableMap.of("id", 555L));
        final IssueType issueType1 = mockIssueType(666L);

        final boolean contextHasIssues = true;
        final CustomFieldContextConfigHelperImpl helper = new CustomFieldContextConfigHelperImpl(searchProvider, fieldConfigSchemeManager) {
            @Override
            boolean _doesContextHaveIssues(final ApplicationUser user, final List<Long> projectIds, final List<Long> issueTypeIds) {
                assertEquals(ImmutableList.of(555L), copyOf(projectIds));
                assertEquals(ImmutableList.of(666L), issueTypeIds);
                return contextHasIssues;
            }
        };

        final boolean result = helper.doesContextHaveIssues(user, newArrayList(new MockProject(project1)), newHashSet(issueType1, null));
        assertEquals(contextHasIssues, result);
    }

    @Test
    public void testDoesContextHaveIssuesWithGenericValuesNull() throws Exception {
        final boolean contextHasIssues = true;
        final CustomFieldContextConfigHelperImpl helper = new CustomFieldContextConfigHelperImpl(searchProvider, fieldConfigSchemeManager) {
            @Override
            boolean _doesContextHaveIssues(final ApplicationUser user, final List<Long> projectIds, final List<Long> issueTypeIds) {
                assertEquals(Collections.<Long>emptyList(), copyOf(projectIds));
                assertEquals(Collections.<Long>emptyList(), issueTypeIds);
                return contextHasIssues;
            }
        };

        final boolean result = helper.doesContextHaveIssues(user, (List<Project>) null, null);
        assertEquals(contextHasIssues, result);
    }

    @Test
    public void testDoesGlobalContextHaveIssues() throws Exception {
        final boolean contextHasIssues = true;
        final CustomFieldContextConfigHelperImpl helper = new CustomFieldContextConfigHelperImpl(searchProvider, fieldConfigSchemeManager) {
            @Override
            boolean _doesContextHaveIssues(final ApplicationUser user, final List<Long> projectIds, final List<Long> issueTypeIds) {
                assertEquals(Collections.<Long>emptyList(), copyOf(projectIds));
                assertEquals(Collections.<Long>emptyList(), issueTypeIds);
                return contextHasIssues;
            }
        };

        final boolean result = helper.doesGlobalContextHaveIssues(user);
        assertEquals(contextHasIssues, result);
    }

    @Test
    public void test_DoesContextHaveIssuesEmptyLists() throws Exception {
        final Query query = new QueryImpl(null, new OrderByImpl(), null);
        when(searchProvider.searchCountOverrideSecurity(query, user)).thenReturn(1L);

        final CustomFieldContextConfigHelperImpl helper = new CustomFieldContextConfigHelperImpl(searchProvider, fieldConfigSchemeManager);

        final boolean result = helper._doesContextHaveIssues(user, Collections.<Long>emptyList(), Collections.<Long>emptyList());
        assertTrue(result);
    }

    @Test
    public void test_DoesContextHaveIssuesEmptyIssueTypes() throws Exception {
        final TerminalClauseImpl whereClause = new TerminalClauseImpl("project", Operator.IN, new MultiValueOperand(11L, 22L));
        final Query query = new QueryImpl(whereClause, new OrderByImpl(), null);
        when(searchProvider.searchCountOverrideSecurity(query, user)).thenReturn(0L);

        final CustomFieldContextConfigHelperImpl helper = new CustomFieldContextConfigHelperImpl(searchProvider, fieldConfigSchemeManager);

        final boolean result = helper._doesContextHaveIssues(user, ImmutableList.of(11L, 22L), Collections.<Long>emptyList());
        assertFalse(result);
    }

    @Test
    public void test_DoesContextHaveIssuesEmptyProjects() throws Exception {
        final TerminalClauseImpl whereClause = new TerminalClauseImpl("issuetype", Operator.IN, new MultiValueOperand(11L, 22L));
        final Query query = new QueryImpl(whereClause, new OrderByImpl(), null);
        when(searchProvider.searchCountOverrideSecurity(query, user))
                .thenReturn(0L);

        final CustomFieldContextConfigHelperImpl helper = new CustomFieldContextConfigHelperImpl(searchProvider, fieldConfigSchemeManager);

        final boolean result = helper._doesContextHaveIssues(user, Collections.<Long>emptyList(), ImmutableList.of(11L, 22L));
        assertFalse(result);
    }

    @Test
    public void test_DoesContextHaveIssuesProjectsAndIssueTypes() throws Exception {
        final TerminalClauseImpl projects = new TerminalClauseImpl("project", Operator.IN, new MultiValueOperand(11L, 22L));
        final TerminalClauseImpl issueTypes = new TerminalClauseImpl("issuetype", Operator.IN, new MultiValueOperand(11L, 22L));
        final AndClause whereClause = new AndClause(projects, issueTypes);

        final Query query = new QueryImpl(whereClause, new OrderByImpl(), null);
        when(searchProvider.searchCountOverrideSecurity(query, user)).thenReturn(0L);

        final CustomFieldContextConfigHelperImpl helper = new CustomFieldContextConfigHelperImpl(searchProvider, fieldConfigSchemeManager);

        final boolean result = helper._doesContextHaveIssues(user, ImmutableList.of(11L, 22L), ImmutableList.of(11L, 22L));
        assertFalse(result);
    }

    @Test
    public void testAddNewCustomFieldContextHasIssues() throws Exception {
        final CustomFieldContextConfigHelperImpl helper = new CustomFieldContextConfigHelperImpl(searchProvider, fieldConfigSchemeManager) {
            @Override
            boolean doesContextHaveIssues(final ApplicationUser user, final List<JiraContextNode> projectContexts, final List<IssueType> issueTypes) {
                return true;
            }
        };

        final boolean result = helper.doesAddingContextToCustomFieldAffectIssues(user, customField, Collections.<JiraContextNode>emptyList(), Collections.<IssueType>emptyList(), true);
        assertTrue(result);
    }

    @Test
    public void testAddNewCustomFieldContextDoesntHaveIssues() throws Exception {
        final CustomFieldContextConfigHelperImpl helper = new CustomFieldContextConfigHelperImpl(searchProvider, fieldConfigSchemeManager) {
            @Override
            boolean doesContextHaveIssues(final ApplicationUser user, final List<JiraContextNode> projectContexts, final List<IssueType> issueTypes) {
                return false;
            }
        };

        final boolean result = helper.doesAddingContextToCustomFieldAffectIssues(user, customField, Collections.<JiraContextNode>emptyList(), Collections.<IssueType>emptyList(), true);
        assertFalse(result);
    }

    @Test
    public void testAddExistingFieldHasGlobalScheme() throws Exception {
        final FieldConfigScheme scheme = mock(FieldConfigScheme.class);
        when(scheme.isGlobal()).thenReturn(true);
        when(fieldConfigSchemeManager.getConfigSchemesForField(customField))
                .thenReturn(Collections.singletonList(scheme));

        final CustomFieldContextConfigHelperImpl helper = new CustomFieldContextConfigHelperImpl(searchProvider, fieldConfigSchemeManager);

        final boolean result = helper.doesAddingContextToCustomFieldAffectIssues(user, customField, Collections.<JiraContextNode>emptyList(), Collections.<IssueType>emptyList(), false);
        assertFalse(result);
    }

    @Test
    public void testAddExistingFieldDoesntHaveGlobalScheme() throws Exception {
        final FieldConfigScheme scheme = mock(FieldConfigScheme.class);
        when(scheme.isGlobal()).thenReturn(false);
        when(fieldConfigSchemeManager.getConfigSchemesForField(customField))
                .thenReturn(ImmutableList.of(scheme));

        final boolean contextHaveIssues = false;
        final CustomFieldContextConfigHelperImpl helper = new CustomFieldContextConfigHelperImpl(searchProvider, fieldConfigSchemeManager) {
            @Override
            boolean doesContextHaveIssues(final ApplicationUser user, final List<JiraContextNode> projectContexts, final List<IssueType> issueTypes) {
                return contextHaveIssues;
            }
        };

        final boolean result = helper.doesAddingContextToCustomFieldAffectIssues(user, customField, Collections.<JiraContextNode>emptyList(), Collections.<IssueType>emptyList(), false);
        assertEquals(contextHaveIssues, result);
    }

    @Test
    public void testChangingNewIsGlobal() throws Exception {
        final FieldConfigScheme scheme = mock(FieldConfigScheme.class);
        when(scheme.isGlobal()).thenReturn(false);

        final boolean contextHaveIssues = false;
        final CustomFieldContextConfigHelperImpl helper = new CustomFieldContextConfigHelperImpl(searchProvider, fieldConfigSchemeManager) {
            @Override
            boolean doesGlobalContextHaveIssues(final ApplicationUser user) {
                return contextHaveIssues;
            }
        };

        final boolean result = helper.doesChangingContextAffectIssues(user, customField, scheme, true, Collections.<JiraContextNode>emptyList(), Collections.<IssueType>emptyList());
        assertEquals(contextHaveIssues, result);
    }

    @Test
    public void testChangingOldIsGlobal() throws Exception {
        final FieldConfigScheme scheme = mock(FieldConfigScheme.class);
        when(scheme.isGlobal())
                .thenReturn(true);

        final boolean contextHaveIssues = false;
        final CustomFieldContextConfigHelperImpl helper = new CustomFieldContextConfigHelperImpl(searchProvider, fieldConfigSchemeManager) {
            @Override
            boolean doesGlobalContextHaveIssues(final ApplicationUser user) {
                return contextHaveIssues;
            }
        };

        final boolean result = helper.doesChangingContextAffectIssues(user, customField, scheme, false, Collections.<JiraContextNode>emptyList(), Collections.<IssueType>emptyList());
        assertEquals(contextHaveIssues, result);
    }

    @Test
    public void testChangingNeitherGlobalFieldHasGlobal() throws Exception {
        final FieldConfigScheme oldScheme = mock(FieldConfigScheme.class);
        when(oldScheme.isGlobal()).thenReturn(false);

        final FieldConfigScheme otherScheme = mock(FieldConfigScheme.class);
        when(otherScheme.isGlobal()).thenReturn(true);

        when(fieldConfigSchemeManager.getConfigSchemesForField(customField))
                .thenReturn(ImmutableList.of(otherScheme));

        final CustomFieldContextConfigHelperImpl helper = new CustomFieldContextConfigHelperImpl(searchProvider, fieldConfigSchemeManager);

        final boolean result = helper.doesChangingContextAffectIssues(user, customField, oldScheme, false, Collections.<JiraContextNode>emptyList(), Collections.<IssueType>emptyList());
        assertFalse(result);

        verify(oldScheme).isGlobal();
        verify(otherScheme).isGlobal();
    }

    @Test
    public void testChangingNeitherGlobalFieldDoesntHaveGlobalNewContextHasIssues() throws Exception {
        final FieldConfigScheme oldScheme = mock(FieldConfigScheme.class);
        when(oldScheme.isGlobal()).thenReturn(false);

        when(fieldConfigSchemeManager.getConfigSchemesForField(customField))
                .thenReturn(Collections.<FieldConfigScheme>emptyList());

        final List<JiraContextNode> inputProjectContexts = Collections.emptyList();
        final List<IssueType> inputIssueTypes = Collections.emptyList();

        final CustomFieldContextConfigHelperImpl helper = new CustomFieldContextConfigHelperImpl(searchProvider, fieldConfigSchemeManager) {
            @Override
            boolean doesContextHaveIssues(final ApplicationUser user, final List<JiraContextNode> projectContexts, final List<IssueType> issueTypes) {
                assertSame(inputProjectContexts, projectContexts);
                assertSame(inputIssueTypes, issueTypes);
                return true;
            }
        };

        final boolean result = helper.doesChangingContextAffectIssues(user, customField, oldScheme, false, inputProjectContexts, inputIssueTypes);
        assertTrue(result);

        verify(oldScheme).isGlobal();
    }

    @Test
    public void testChangingNeitherGlobalFieldDoesntHaveGlobalNewContextDoesntHaveIssues() throws Exception {
        final List<Project> associatedProjects = ImmutableList.<Project>of(new MockProject(1l, "project"));
        final Set<IssueType> associatedIssueTypes = ImmutableSet.of(mockIssueType(1L));

        final FieldConfigScheme oldScheme = mock(FieldConfigScheme.class);
        when(oldScheme.isGlobal()).thenReturn(false);
        when(oldScheme.getAssociatedProjectObjects()).thenReturn(associatedProjects);
        when(oldScheme.getAssociatedIssueTypes()).thenReturn(associatedIssueTypes);

        when(fieldConfigSchemeManager.getConfigSchemesForField(customField))
                .thenReturn(Collections.<FieldConfigScheme>emptyList());

        final List<JiraContextNode> inputProjectContexts = Collections.emptyList();
        final List<IssueType> inputIssueTypes = Collections.emptyList();

        final CustomFieldContextConfigHelperImpl helper = new CustomFieldContextConfigHelperImpl(searchProvider, fieldConfigSchemeManager) {
            @Override
            boolean doesContextHaveIssues(final ApplicationUser user, final List<JiraContextNode> projectContexts, final List<IssueType> issueTypes) {
                assertSame(inputProjectContexts, projectContexts);
                assertSame(inputIssueTypes, issueTypes);
                return false;
            }

            @Override
            boolean doesContextHaveIssues(final ApplicationUser user, final List<Project> projects, final Set<IssueType> issueTypes) {
                assertEquals(associatedProjects, projects);
                assertEquals(associatedIssueTypes, issueTypes);
                return true;
            }
        };

        final boolean result = helper.doesChangingContextAffectIssues(user, customField, oldScheme, false, inputProjectContexts, inputIssueTypes);
        assertTrue(result);

        verify(oldScheme).isGlobal();
    }

    @Test
    public void testRemoveNonGlobalSchemeFieldStillHasGlobal() throws Exception {
        final FieldConfigScheme scheme = mock(FieldConfigScheme.class);
        when(scheme.isGlobal()).thenReturn(false);
        final FieldConfigScheme otherScheme = mock(FieldConfigScheme.class);
        when(otherScheme.isGlobal()).thenReturn(true);
        when(fieldConfigSchemeManager.getConfigSchemesForField(customField))
                .thenReturn(ImmutableList.of(otherScheme));

        final CustomFieldContextConfigHelperImpl helper = new CustomFieldContextConfigHelperImpl(searchProvider, fieldConfigSchemeManager);

        final boolean result = helper.doesRemovingSchemeFromCustomFieldAffectIssues(user, customField, scheme);
        assertFalse(result);

        verify(scheme).isGlobal();
        verify(otherScheme).isGlobal();
    }

    @Test
    public void testRemoveNonGlobalSchemeFieldHasNoOtherGlobal() throws Exception {
        final FieldConfigScheme scheme = mock(FieldConfigScheme.class);
        when(scheme.isGlobal())
                .thenReturn(false);
        when(fieldConfigSchemeManager.getConfigSchemesForField(customField))
                .thenReturn(Collections.<FieldConfigScheme>emptyList());

        final boolean contextHasIssues = true;
        final CustomFieldContextConfigHelperImpl helper = new CustomFieldContextConfigHelperImpl(searchProvider, fieldConfigSchemeManager) {
            @Override
            boolean doesFieldConfigSchemeHaveIssues(final ApplicationUser user, final FieldConfigScheme fieldConfigScheme) {
                assertSame(scheme, fieldConfigScheme);
                return contextHasIssues;
            }
        };

        final boolean result = helper.doesRemovingSchemeFromCustomFieldAffectIssues(user, customField, scheme);
        assertEquals(contextHasIssues, result);

        verify(scheme).isGlobal();
    }

    @Test
    public void testRemoveGlobalSchemeFieldHasNoOtherNonGlobalSchemes() throws Exception {
        final FieldConfigScheme scheme = mock(FieldConfigScheme.class);
        when(scheme.isGlobal()).thenReturn(true);
        when(fieldConfigSchemeManager.getConfigSchemesForField(customField))
                .thenReturn(Collections.<FieldConfigScheme>emptyList());

        final boolean contextHasIssues = true;
        final CustomFieldContextConfigHelperImpl helper = new CustomFieldContextConfigHelperImpl(searchProvider, fieldConfigSchemeManager) {
            @Override
            boolean doesGlobalContextHaveIssues(final ApplicationUser user) {
                return contextHasIssues;
            }
        };

        final boolean result = helper.doesRemovingSchemeFromCustomFieldAffectIssues(user, customField, scheme);
        assertEquals(contextHasIssues, result);

        verify(scheme).isGlobal();
    }

    @Test
    public void testRemoveGlobalSchemeFieldHasOneOtherNonGlobalScheme() throws Exception {
        final FieldConfigScheme scheme = mock(FieldConfigScheme.class);
        when(scheme.isGlobal())
                .thenReturn(true);

        final FieldConfigScheme otherScheme = mock(FieldConfigScheme.class);
        when(fieldConfigSchemeManager.getConfigSchemesForField(customField))
                .thenReturn(ImmutableList.of(otherScheme));

        final List<Project> associatedProjects = ImmutableList.<Project>of(new MockProject(555L, "project"));
        final Set<IssueType> associatedIssueTypes = ImmutableSet.of(mockIssueType(666L));

        when(otherScheme.isGlobal()).thenReturn(false);
        when(otherScheme.getAssociatedProjectObjects()).thenReturn(associatedProjects);
        when(otherScheme.getAssociatedIssueTypes()).thenReturn(associatedIssueTypes);

        final TerminalClause projectClause = new TerminalClauseImpl("project", Operator.IN, new MultiValueOperand(555L));
        final TerminalClause issueTypeClause = new TerminalClauseImpl("issuetype", Operator.IN, new MultiValueOperand(666L));
        final AndClause andClause = new AndClause(projectClause, issueTypeClause);
        final NotClause whereClause = new NotClause(andClause);
        final Query expectedQuery = new QueryImpl(whereClause, new OrderByImpl(), null);

        when(searchProvider.searchCountOverrideSecurity(expectedQuery, user)).thenReturn(1L);

        final CustomFieldContextConfigHelperImpl helper = new CustomFieldContextConfigHelperImpl(searchProvider, fieldConfigSchemeManager);

        final boolean result = helper.doesRemovingSchemeFromCustomFieldAffectIssues(user, customField, scheme);
        assertTrue(result);

        verify(scheme).isGlobal();
        verify(otherScheme).isGlobal();
    }

    @Test
    public void testRemoveGlobalSchemeFieldHasTwoOtherNonGlobalSchemes() throws Exception {
        final FieldConfigScheme scheme = mock(FieldConfigScheme.class);
        when(scheme.isGlobal())
                .thenReturn(true);

        final FieldConfigScheme otherScheme1 = mock(FieldConfigScheme.class);
        final FieldConfigScheme otherScheme2 = mock(FieldConfigScheme.class);
        when(fieldConfigSchemeManager.getConfigSchemesForField(customField))
                .thenReturn(ImmutableList.of(otherScheme1, otherScheme2));

        final Set<IssueType> associatedIssueTypes1 = ImmutableSet.of(mockIssueType(666L));
        final List<Project> associatedProjects2 = ImmutableList.<Project>of(new MockProject(555L, "project"));

        when(otherScheme1.isGlobal()).thenReturn(false);
        when(otherScheme1.getAssociatedProjectObjects()).thenReturn(null);
        when(otherScheme1.getAssociatedIssueTypes()).thenReturn(associatedIssueTypes1);

        when(otherScheme2.isGlobal()).thenReturn(false);
        when(otherScheme2.getAssociatedProjectObjects()).thenReturn(associatedProjects2);
        when(otherScheme2.getAssociatedIssueTypes()).thenReturn(null);

        final TerminalClause issueTypeClause = new TerminalClauseImpl("issuetype", Operator.IN, new MultiValueOperand(666L));
        final NotClause notClause1 = new NotClause(issueTypeClause);

        final TerminalClause projectClause = new TerminalClauseImpl("project", Operator.IN, new MultiValueOperand(555L));
        final NotClause notClause2 = new NotClause(projectClause);

        final AndClause andClause = new AndClause(notClause1, notClause2);

        final Query expectedQuery = new QueryImpl(andClause, new OrderByImpl(), null);

        when(searchProvider.searchCountOverrideSecurity(expectedQuery, user))
                .thenReturn(1L);

        final CustomFieldContextConfigHelperImpl helper = new CustomFieldContextConfigHelperImpl(searchProvider, fieldConfigSchemeManager);

        final boolean result = helper.doesRemovingSchemeFromCustomFieldAffectIssues(user, customField, scheme);
        assertTrue(result);

        verify(otherScheme1).isGlobal();
        verify(otherScheme1).getAssociatedProjectObjects();
        verify(otherScheme1).getAssociatedIssueTypes();

        verify(otherScheme2).isGlobal();
        verify(otherScheme2).getAssociatedProjectObjects();
        verify(otherScheme2).getAssociatedIssueTypes();
    }
}
