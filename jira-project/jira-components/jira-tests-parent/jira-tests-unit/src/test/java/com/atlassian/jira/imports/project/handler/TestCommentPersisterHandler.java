package com.atlassian.jira.imports.project.handler;

import com.atlassian.jira.external.beans.ExternalComment;
import com.atlassian.jira.imports.project.ProjectImportPersister;
import com.atlassian.jira.imports.project.core.BackupSystemInformation;
import com.atlassian.jira.imports.project.core.ProjectImportResults;
import com.atlassian.jira.imports.project.core.ProjectImportResultsImpl;
import com.atlassian.jira.imports.project.mapper.ProjectImportMapper;
import com.atlassian.jira.imports.project.mapper.ProjectImportMapperImpl;
import com.atlassian.jira.imports.project.parser.CommentParser;
import com.atlassian.jira.imports.project.transformer.CommentTransformer;
import com.atlassian.jira.web.bean.MockI18nBean;
import org.junit.Test;

import static com.atlassian.jira.imports.project.parser.CommentParser.COMMENT_ENTITY_NAME;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

/**
 * @since v3.13
 */
public class TestCommentPersisterHandler {
    private ProjectImportMapper projectImportMapper = new ProjectImportMapperImpl(null, null);

    @Test
    public void testHandle() throws Exception {
        final ExternalComment externalComment = new ExternalComment();
        externalComment.setIssueId("34");

        final CommentParser mockCommentParser = mock(CommentParser.class);
        when(mockCommentParser.parse(null)).thenReturn(externalComment);
        when(mockCommentParser.getEntityRepresentation(externalComment)).thenReturn(null);

        final CommentTransformer mockCommentTransformer = mock(CommentTransformer.class);
        when(mockCommentTransformer.transform(projectImportMapper, externalComment)).thenReturn(externalComment);

        final ProjectImportPersister mockProjectImportPersister = mock(ProjectImportPersister.class);
        when(mockProjectImportPersister.createEntity(null)).thenReturn(12L);

        final ProjectImportResultsImpl projectImportResults = new ProjectImportResultsImpl(0, 0, 0, 0, null);
        CommentPersisterHandler commentPersisterHandler = new CommentPersisterHandler(mockProjectImportPersister, projectImportMapper, projectImportResults, null, new ExecutorForTests()) {
            CommentParser getCommentParser() {
                return mockCommentParser;
            }

            CommentTransformer getCommentTransformer() {
                return mockCommentTransformer;
            }
        };

        commentPersisterHandler.handleEntity(COMMENT_ENTITY_NAME, null);
        commentPersisterHandler.handleEntity("NOTComment", null);

        assertThat(projectImportResults.getErrors(), hasSize(0));
    }

    @Test
    public void testHandleErrorAddingComment() throws Exception {
        final ExternalComment externalComment = new ExternalComment();
        externalComment.setIssueId("34");
        externalComment.setId("12");

        final CommentParser mockCommentParser = mock(CommentParser.class);
        when(mockCommentParser.parse(null)).thenReturn(externalComment);
        when(mockCommentParser.getEntityRepresentation(externalComment)).thenReturn(null);

        final CommentTransformer mockCommentTransformer = mock(CommentTransformer.class);
        when(mockCommentTransformer.transform(projectImportMapper, externalComment)).thenReturn(externalComment);

        final ProjectImportPersister mockProjectImportPersister = mock(ProjectImportPersister.class);
        when(mockProjectImportPersister.createEntity(null)).thenReturn(null);

        final BackupSystemInformation mockBackupSystemInformation = mock(BackupSystemInformation.class);
        when(mockBackupSystemInformation.getIssueKeyForId("34")).thenReturn("TST-1");

        final ProjectImportResultsImpl projectImportResults = new ProjectImportResultsImpl(0, 0, 0, 0, new MockI18nBean());
        CommentPersisterHandler commentPersisterHandler = new CommentPersisterHandler(mockProjectImportPersister, projectImportMapper, projectImportResults, mockBackupSystemInformation, new ExecutorForTests()) {
            CommentParser getCommentParser() {
                return mockCommentParser;
            }

            CommentTransformer getCommentTransformer() {
                return mockCommentTransformer;
            }
        };

        commentPersisterHandler.handleEntity(COMMENT_ENTITY_NAME, null);
        commentPersisterHandler.handleEntity("NOTComment", null);

        assertThat(projectImportResults.getErrors(), hasSize(1));
        assertThat(projectImportResults.getErrors(), hasItem("There was a problem saving comment with id '12' for issue 'TST-1'."));
    }

    @Test
    public void testHandleErrorNullIssueId() throws Exception {
        final ExternalComment externalComment = new ExternalComment();
        externalComment.setId("12");
        externalComment.setIssueId("34");

        final ExternalComment transformedExternalComment = new ExternalComment();
        transformedExternalComment.setId("12");

        final CommentParser mockCommentParser = mock(CommentParser.class);
        when(mockCommentParser.parse(null)).thenReturn(externalComment);

        final CommentTransformer mockCommentTransformer = mock(CommentTransformer.class);
        when(mockCommentTransformer.transform(projectImportMapper, externalComment)).thenReturn(transformedExternalComment);

        final ProjectImportPersister mockProjectImportPersister = mock(ProjectImportPersister.class);

        final BackupSystemInformation mockBackupSystemInformation = mock(BackupSystemInformation.class);
        when(mockBackupSystemInformation.getIssueKeyForId("34")).thenReturn("TST-1");

        final ProjectImportResultsImpl projectImportResults = new ProjectImportResultsImpl(0, 0, 0, 0, new MockI18nBean());
        CommentPersisterHandler commentPersisterHandler = new CommentPersisterHandler(mockProjectImportPersister, projectImportMapper, projectImportResults, mockBackupSystemInformation, null) {
            CommentParser getCommentParser() {
                return mockCommentParser;
            }

            CommentTransformer getCommentTransformer() {
                return mockCommentTransformer;
            }
        };

        commentPersisterHandler.handleEntity(COMMENT_ENTITY_NAME, null);
        commentPersisterHandler.handleEntity("NOTComment", null);

        assertThat(projectImportResults.getErrors(), hasSize(0));
    }

    @Test
    public void testHandleNonComment() throws Exception {
        ProjectImportMapper projectImportMapper = mock(ProjectImportMapper.class);

        final CommentParser mockCommentParser = mock(CommentParser.class);
        when(mockCommentParser.parse(null)).thenReturn(null);

        final CommentTransformer mockCommentTransformer = mock(CommentTransformer.class);

        final ProjectImportPersister mockProjectImportPersister = mock(ProjectImportPersister.class);

        final ProjectImportResults projectImportResults = mock(ProjectImportResults.class);
        CommentPersisterHandler commentPersisterHandler = new CommentPersisterHandler(mockProjectImportPersister, projectImportMapper, projectImportResults, null, null) {
            CommentParser getCommentParser() {
                return mockCommentParser;
            }

            CommentTransformer getCommentTransformer() {
                return mockCommentTransformer;
            }
        };

        commentPersisterHandler.handleEntity(COMMENT_ENTITY_NAME, null);
        commentPersisterHandler.handleEntity("NOTComment", null);

        verifyZeroInteractions(projectImportMapper, mockProjectImportPersister, projectImportResults);
    }
}
