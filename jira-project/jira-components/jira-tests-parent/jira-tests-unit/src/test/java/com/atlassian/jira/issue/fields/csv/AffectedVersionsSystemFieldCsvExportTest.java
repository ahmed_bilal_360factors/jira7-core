package com.atlassian.jira.issue.fields.csv;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.export.FieldExportParts;
import com.atlassian.jira.issue.fields.AffectedVersionsSystemField;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.NoopI18nHelper;
import com.google.common.collect.ImmutableList;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.mockito.Mock;

import java.util.Locale;

import static java.util.stream.Collectors.toList;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

public class AffectedVersionsSystemFieldCsvExportTest {
    @Rule
    public final RuleChain mockito = MockitoMocksInContainer.forTest(this);

    private static final String VERSION1_NAME = "Version1 name";
    private static final String VERSION2_NAME = "Version2 name";

    @Mock
    private JiraAuthenticationContext authenticationContext;

    private I18nHelper i18nHelper = new NoopI18nHelper();

    @Mock
    private Version version1;

    @Mock
    private Version version2;

    @Mock
    private Issue issue;

    private AffectedVersionsSystemField field;

    @Before
    public void setUp() {
        field = new AffectedVersionsSystemField(null, null, null, null, authenticationContext, null, null, null, null, null, null);

        when(version1.getName()).thenReturn(VERSION1_NAME);
        when(version2.getName()).thenReturn(VERSION2_NAME);
        when(authenticationContext.getI18nHelper()).thenReturn(i18nHelper);
        when(authenticationContext.getLocale()).thenReturn(Locale.ENGLISH);
    }

    @Test
    public void testCsvRepresentationForExpectedTypeAndValues() {
        when(issue.getAffectedVersions()).thenReturn(ImmutableList.of(version1, version2));

        final FieldExportParts representation = field.getRepresentationFromIssue(issue);
        assertThat(representation.getParts(), hasSize(1));
        assertThat(representation.getParts().get(0).getValues()::iterator, contains(VERSION1_NAME, VERSION2_NAME));
    }

    @Test
    public void testCsvRepresentationWhenThereAreNoAffectedVersions() {
        when(issue.getAffectedVersions()).thenReturn(ImmutableList.of());

        final FieldExportParts representation = field.getRepresentationFromIssue(issue);
        assertThat(representation.getParts(), hasSize(1));
        assertThat(representation.getParts().get(0).getValues().collect(toList()), hasSize(0));
    }

    @Test
    public void testCsvRepresentationWhenAffectedVersionsAreNull() {
        when(issue.getAffectedVersions()).thenReturn(null);

        final FieldExportParts representation = field.getRepresentationFromIssue(issue);
        assertThat(representation.getParts(), hasSize(1));
        assertThat(representation.getParts().get(0).getValues().collect(toList()), hasSize(0));
    }
}
