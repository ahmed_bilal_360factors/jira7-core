package com.atlassian.jira.dashboard;

import com.atlassian.beehive.ClusterLock;
import com.atlassian.beehive.ClusterLockService;
import com.atlassian.fugue.Option;
import com.atlassian.gadgets.DashboardItemState;
import com.atlassian.gadgets.DashboardItemStateVisitor;
import com.atlassian.gadgets.GadgetId;
import com.atlassian.gadgets.GadgetState;
import com.atlassian.gadgets.LocalDashboardItemModuleId;
import com.atlassian.gadgets.LocalDashboardItemState;
import com.atlassian.gadgets.OpenSocialDashboardItemModuleId;
import com.atlassian.gadgets.dashboard.Color;
import com.atlassian.gadgets.dashboard.DashboardId;
import com.atlassian.gadgets.dashboard.DashboardNotFoundException;
import com.atlassian.gadgets.dashboard.DashboardState;
import com.atlassian.gadgets.dashboard.spi.DashboardStateStoreException;
import com.atlassian.gadgets.dashboard.spi.changes.DashboardChange;
import com.atlassian.jira.portal.PortalPage;
import com.atlassian.jira.portal.PortalPageManager;
import com.atlassian.jira.portal.PortalPageStore;
import com.atlassian.jira.portal.PortletConfiguration;
import com.atlassian.jira.portal.PortletConfigurationImpl;
import com.atlassian.jira.portal.PortletConfigurationStore;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.plugin.ModuleCompleteKey;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import org.apache.commons.lang3.ObjectUtils;
import org.hamcrest.Description;
import org.hamcrest.Matchers;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.net.URI;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static com.atlassian.gadgets.dashboard.Color.color1;
import static com.atlassian.gadgets.dashboard.Color.color2;
import static com.atlassian.gadgets.dashboard.Color.color5;
import static com.atlassian.gadgets.dashboard.Color.color6;
import static com.atlassian.gadgets.dashboard.Color.color8;
import static com.atlassian.gadgets.dashboard.DashboardState.ColumnIndex.ONE;
import static com.atlassian.gadgets.dashboard.DashboardState.ColumnIndex.ZERO;
import static com.atlassian.gadgets.dashboard.DashboardState.dashboard;
import static com.atlassian.gadgets.dashboard.Layout.AA;
import static com.atlassian.gadgets.dashboard.Layout.AAA;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestJiraDashboardStateStoreManager {
    private static final long PORTAL_PAGE_ID = 10020;
    private static final long PORTLET_ID_1 = 10011;
    private static final long PORTLET_ID_2 = 10012;
    private static final long PORTLET_ID_3 = 10231;
    private static final Option<ModuleCompleteKey> MODULE_KEY = Option.some(new ModuleCompleteKey("com.atlassian.gadgets.test:complete.key"));
    private static final DashboardId DASHBOARD_ID = DashboardId.valueOf(Long.toString(PORTAL_PAGE_ID));
    private static final Map<String, String> PREFS = ImmutableMap.of("pref1", "value1", "pref2", "value2");
    private static final Map<String, String> EMPTY_PREFS = ImmutableMap.of();

    private static final MockApplicationUser ADMIN = new MockApplicationUser("admin");

    private static final String DASHBOARD_TITLE = "My Dashboard";
    private static final String PORTAL_NAME = "Test Dashboard";
    private static final String WRITE_LOCK_NAME = JiraDashboardStateStoreManager.getWriteLockName(DASHBOARD_ID);

    private static final PortalPage PORTAL_PAGE = PortalPage.id(PORTAL_PAGE_ID).name(PORTAL_NAME).description("")
            .owner(ADMIN).favouriteCount(0L).layout(AA).version(0L).build();

    public static final Option<URI> GOOGLE_URI = Option.some(URI.create("http://www.google.com/"));
    public static final Option<URI> MSN_URI = Option.some(URI.create("http://www.msn.com/"));

    @InjectMocks
    private JiraDashboardStateStoreManager dashboardStateStoreManager;
    @Mock
    private ClusterLock mockClusterLock;

    @Mock
    private ClusterLockService mockClusterLockService;
    @Mock
    private PortalPageManager mockPortalPageManager;
    @Mock
    private PortalPageStore mockPortalPageStore;
    @Mock
    private PortletConfigurationStore mockPortletConfigurationStore;

    private static PortletConfiguration eqPortletConfiguration(final PortletConfiguration expected) {
        return argThat(new PortletConfigurationMatcher(expected));
    }

    @Before
    public void setUp() {
        when(mockClusterLockService.getLockForName(WRITE_LOCK_NAME)).thenReturn(mockClusterLock);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testRetrieveNullId() {
        dashboardStateStoreManager.retrieve(null);
    }

    @Test(expected = DashboardNotFoundException.class)
    public void testRetrieveDashboardNotFound() {
        when(mockPortalPageStore.getPortalPage(PORTAL_PAGE_ID)).thenReturn(null);

        dashboardStateStoreManager.retrieve(DASHBOARD_ID);
    }

    @Test
    public void testRetrieveSuccess() {
        when(mockPortalPageStore.getPortalPage(PORTAL_PAGE_ID)).thenReturn(PORTAL_PAGE);
        when(mockPortalPageManager.getPortletConfigurations(PORTAL_PAGE_ID)).thenReturn(ImmutableList.<List<PortletConfiguration>>of(
                ImmutableList.<PortletConfiguration>of(
                        new PortletConfigurationImpl(PORTLET_ID_1, PORTAL_PAGE_ID, 0, 0, GOOGLE_URI, color1, PREFS, Option.<ModuleCompleteKey>none()),
                        new PortletConfigurationImpl(PORTLET_ID_2, PORTAL_PAGE_ID, 0, 1, MSN_URI, color2, PREFS, MODULE_KEY)
                ),
                ImmutableList.<PortletConfiguration>of(
                        new PortletConfigurationImpl(PORTLET_ID_3, PORTAL_PAGE_ID, 1, 0, Option.<URI>none(), color5, PREFS, MODULE_KEY)
                )
        ));

        final DashboardState state = dashboardStateStoreManager.retrieve(DASHBOARD_ID);

        assertThat(state.getId(), equalTo(DASHBOARD_ID));
        assertThat(state.getTitle(), equalTo(PORTAL_NAME));
        assertThat(state.getLayout(), equalTo(AA));

        final Iterable<DashboardItemState> firstColumn = state.getDashboardColumns().getItemsInColumn(ZERO);
        assertThat(firstColumn, Matchers.<DashboardItemState>contains(
                dashboardItemStateMatcher(PORTLET_ID_1, color1, PREFS, GOOGLE_URI, Option.<ModuleCompleteKey>none()),
                dashboardItemStateMatcher(PORTLET_ID_2, color2, PREFS, MSN_URI, MODULE_KEY)
        ));

        final Iterable<DashboardItemState> secondColumn = state.getDashboardColumns().getItemsInColumn(ONE);
        assertThat(secondColumn, contains(
                dashboardItemStateMatcher(PORTLET_ID_3, color5, PREFS, Option.<URI>none(), MODULE_KEY)
        ));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testUpdateWithNullDashboardState() {
        dashboardStateStoreManager.update(null, Collections.<DashboardChange>emptyList());

        verifyNoMoreInteractions(mockClusterLock);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testUpdateWithNullChanges() {
        final DashboardState dashboardState = dashboard(DASHBOARD_ID).title(DASHBOARD_TITLE).build();

        dashboardStateStoreManager.update(dashboardState, null);

        verifyNoMoreInteractions(mockClusterLock);
    }

    @Test(expected = DashboardStateStoreException.class)
    public void testUpdateDashboardThatDoesNotExist() {
        final DashboardState dashboardState = dashboard(DASHBOARD_ID).title(DASHBOARD_TITLE).build();
        when(mockPortalPageStore.getPortalPage(PORTAL_PAGE_ID)).thenReturn(null);

        dashboardStateStoreManager.update(dashboardState, Collections.<DashboardChange>emptyList());

        verifyWriteLockedAndUnlocked();
    }

    private void verifyWriteLockedAndUnlocked() {
        verify(mockClusterLock).lock();
        verify(mockClusterLock).unlock();
    }

    @Test
    public void testStoreDashboardWithPortalPageUpdate() {
        final Long newPortletId = 10001L;
        final Long portletToUpdateId = 10002L;
        final Long noChangedPortletId = 10003L;
        final Long portletToRemoveId = 10004L;

        when(mockPortalPageStore.updatePortalPageOptimisticLock(PORTAL_PAGE_ID, 0L)).thenReturn(true);

        final PortalPage portalPage = PortalPage.id(PORTAL_PAGE_ID).name(PORTAL_NAME).description("")
                .owner(ADMIN).favouriteCount(0L).layout(AA).version(0L).build();
        when(mockPortalPageStore.getPortalPage(PORTAL_PAGE_ID)).thenReturn(portalPage);

        final PortalPage newPortalPage = PortalPage.id(PORTAL_PAGE_ID).name(DASHBOARD_TITLE).description("")
                .owner(ADMIN).favouriteCount(0L).layout(AAA).version(0L).build();

        when(mockPortletConfigurationStore.getByPortalPage(PORTAL_PAGE_ID)).thenReturn(ImmutableList.<PortletConfiguration>of(
                new PortletConfigurationImpl(portletToUpdateId, PORTAL_PAGE_ID, 0, 0, GOOGLE_URI, color1, EMPTY_PREFS, Option.<ModuleCompleteKey>none()),
                new PortletConfigurationImpl(portletToRemoveId, PORTAL_PAGE_ID, 0, 1, MSN_URI, color2, PREFS, MODULE_KEY),
                new PortletConfigurationImpl(noChangedPortletId, PORTAL_PAGE_ID, 1, 2, Option.<URI>none(), color2, EMPTY_PREFS, MODULE_KEY)
        ));

        final DashboardItemState item1 = buildGadget(portletToUpdateId, URI.create("http://www.google.com/"), color6);
        final DashboardItemState item2 = buildLocalItem(noChangedPortletId, color5, MODULE_KEY.get(), Option.some(URI.create("http://www.msn.com/")));
        final DashboardItemState item3 = buildLocalItem(newPortletId, color2, MODULE_KEY.get(), Option.<URI>none());

        final List<List<DashboardItemState>> columns = ImmutableList.of(
                Collections.<DashboardItemState>emptyList(),
                ImmutableList.of(item1, item3),
                ImmutableList.of(item2)
        );
        final DashboardState state = dashboard(DASHBOARD_ID).title(DASHBOARD_TITLE).layout(AAA).dashboardColumns(columns).build();

        final JiraDashboardStateStoreManager stateStore = new JiraDashboardStateStoreManager(
                mockPortalPageStore, mockPortletConfigurationStore, mockPortalPageManager, mockClusterLockService) {
            @Override
            public DashboardState retrieve(final DashboardId dashboardId)
                    throws DashboardNotFoundException, DashboardStateStoreException {
                return state;
            }
        };

        final DashboardState newState = stateStore.update(state, Collections.<DashboardChange>emptyList());

        assertEquals(state, newState);
        verify(mockPortalPageStore).update(newPortalPage);
        verify(mockPortletConfigurationStore).store(eqPortletConfiguration(
                new PortletConfigurationImpl(portletToUpdateId, PORTAL_PAGE_ID, 1, 0, GOOGLE_URI, color6, EMPTY_PREFS, Option.<ModuleCompleteKey>none())));
        verify(mockPortletConfigurationStore).delete(eqPortletConfiguration(
                new PortletConfigurationImpl(portletToRemoveId, PORTAL_PAGE_ID, 0, 1, MSN_URI, color2, PREFS, MODULE_KEY)));
        verify(mockPortletConfigurationStore).addDashboardItem(PORTAL_PAGE_ID, newPortletId, 1, 1, Option.<URI>none(), color2, EMPTY_PREFS, MODULE_KEY);
        verify(mockPortletConfigurationStore, never()).store(eqPortletConfiguration(
                new PortletConfigurationImpl(noChangedPortletId, PORTAL_PAGE_ID, 1, 2, Option.<URI>none(), color2, EMPTY_PREFS, MODULE_KEY)));
    }

    @Test(expected = DashboardStateStoreException.class)
    public void testFindGadgetByIdDoesntExist() {
        final long portletId = 1;
        when(mockPortletConfigurationStore.getByPortletId(portletId)).thenReturn(null);
        final GadgetId gadgetId = asGadgetId(portletId);

        dashboardStateStoreManager.findDashboardWithGadget(gadgetId);
    }

    @Test(expected = DashboardNotFoundException.class)
    public void testFindGadgetByIdDashboardDoesntExist() {
        final PortletConfiguration portletConfiguration = new PortletConfigurationImpl(
                10027L, PORTAL_PAGE_ID, 0, 0, Option.<URI>none(), color8, EMPTY_PREFS, Option.<ModuleCompleteKey>none());
        final long portletId = 1;
        final GadgetId gadgetId = asGadgetId(portletId);

        when(mockPortletConfigurationStore.getByPortletId(portletId)).thenReturn(portletConfiguration);
        when(mockPortalPageStore.getPortalPage(PORTAL_PAGE_ID)).thenReturn(null);

        dashboardStateStoreManager.findDashboardWithGadget(gadgetId);
    }

    @Test
    public void testFindGadgetByIdSuccess() {
        final PortletConfiguration portletConfiguration = new PortletConfigurationImpl(
                10027L, PORTAL_PAGE_ID, 0, 0, Option.<URI>none(), color8, EMPTY_PREFS, Option.<ModuleCompleteKey>none());
        final long portletId = 1;
        final DashboardState state = dashboard(DASHBOARD_ID).title(DASHBOARD_TITLE).layout(AAA).build();
        final GadgetId gadgetId = asGadgetId(portletId);

        when(mockPortletConfigurationStore.getByPortletId(portletId)).thenReturn(portletConfiguration);

        final JiraDashboardStateStoreManager stateStore = new JiraDashboardStateStoreManager(
                mockPortalPageStore, mockPortletConfigurationStore, mockPortalPageManager, mockClusterLockService) {
            @Override
            public DashboardState retrieve(final DashboardId dashboardId)
                    throws DashboardNotFoundException, DashboardStateStoreException {
                return state;
            }
        };

        final DashboardState retrievedState = stateStore.findDashboardWithGadget(gadgetId);

        assertEquals(state, retrievedState);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testRemoveNullDashboardId() {
        dashboardStateStoreManager.remove(null);
    }

    @Test
    public void testRemoveDashboard() {
        dashboardStateStoreManager.remove(DASHBOARD_ID);

        verify(mockPortalPageManager).delete(PORTAL_PAGE_ID);
        verifyWriteLockedAndUnlocked();
    }

    private GadgetId asGadgetId(final long portletId) {
        return GadgetId.valueOf(Long.toString(portletId)); // GadgetId is final => can't be mocked
    }

    private DashboardItemState buildGadget(final long portletId, final URI specUri, final Color color) {
        return GadgetState.gadget(asGadgetId(portletId)).specUri(specUri).color(color).build();
    }

    private DashboardItemState buildLocalItem(final long portletId, final Color color, final ModuleCompleteKey moduleKey, final Option<URI> specUri) {
        return LocalDashboardItemState.builder()
                .gadgetId(asGadgetId(portletId))
                .color(color)
                .dashboardItemModuleId(buildLocalDashboardItemModuleId(moduleKey, specUri)).build();
    }

    private LocalDashboardItemModuleId buildLocalDashboardItemModuleId(final ModuleCompleteKey moduleKey, final Option<URI> specUri) {
        final Option<OpenSocialDashboardItemModuleId> openSocialId = specUri.map(
                new com.google.common.base.Function<URI, OpenSocialDashboardItemModuleId>() {
                    @Override
                    public OpenSocialDashboardItemModuleId apply(final URI openSocialSpecUri) {
                        return new OpenSocialDashboardItemModuleId(openSocialSpecUri);
                    }
                });
        return new LocalDashboardItemModuleId(moduleKey, openSocialId);
    }

    private TypeSafeMatcher<DashboardItemState> dashboardItemStateMatcher(final long portletId, final Color color,
                                                                          final Map<String, String> prefs, final Option<URI> specUri, final Option<ModuleCompleteKey> moduleKey) {
        return new TypeSafeMatcher<DashboardItemState>() {
            @Override
            protected boolean matchesSafely(final DashboardItemState item) {
                return item.accept(new DashboardItemStateVisitor<Boolean>() {
                    @Override
                    public Boolean visit(final GadgetState state) {
                        return state.getId().equals(asGadgetId(portletId)) &&
                                state.getColor().equals(color) &&
                                state.getGadgetSpecUri().equals(specUri.get()) &&
                                state.getUserPrefs().equals(prefs);
                    }

                    @Override
                    public Boolean visit(final LocalDashboardItemState state) {
                        final LocalDashboardItemModuleId moduleId = buildLocalDashboardItemModuleId(moduleKey.get(), specUri);
                        return state.getId().equals(asGadgetId(portletId)) &&
                                state.getColor().equals(color) &&
                                state.getProperties().equals(prefs) &&
                                state.getDashboardItemModuleId().equals(moduleId);
                    }
                });
            }

            @Override
            public void describeTo(final Description description) {
                description.appendText("Dashboard items doesn't have required properties.");
            }
        };
    }

    private static class PortletConfigurationMatcher extends TypeSafeMatcher<PortletConfiguration> {

        private final PortletConfiguration expected;

        PortletConfigurationMatcher(final PortletConfiguration expected) {
            this.expected = expected;
        }

        @Override
        protected boolean matchesSafely(final PortletConfiguration other) {
            return other.getId().equals(expected.getId()) &&
                    other.getColor().equals(expected.getColor()) &&
                    other.getColumn().equals(expected.getColumn()) &&
                    other.getRow().equals(expected.getRow()) &&
                    other.getUserPrefs().equals(expected.getUserPrefs()) &&
                    other.getDashboardPageId().equals(expected.getDashboardPageId()) &&
                    ObjectUtils.equals(other.getOpenSocialSpecUri(), expected.getOpenSocialSpecUri()) &&
                    ObjectUtils.equals(other.getCompleteModuleKey(), expected.getCompleteModuleKey());
        }

        @Override
        public void describeTo(final Description description) {
            description.appendText("eqPortletConfiguration(").appendValue(expected);
        }
    }
}
