package com.atlassian.jira.crowd.embedded.ofbiz;

import com.atlassian.beehive.ClusterLockService;
import com.atlassian.beehive.simple.SimpleClusterLockService;
import com.atlassian.cache.CacheManager;
import com.atlassian.cache.memory.MemoryCacheManager;
import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.embedded.spi.DirectoryDao;
import com.atlassian.crowd.exception.UserNotFoundException;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.model.user.UserTemplate;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.config.database.DatabaseConfig;
import com.atlassian.jira.config.database.JndiDatasource;
import com.atlassian.jira.config.database.MockDatabaseConfigurationManager;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.crowd.embedded.ofbiz.db.DefaultOfBizTransactionManager;
import com.atlassian.jira.crowd.embedded.ofbiz.db.OfBizTransactionManager;
import com.atlassian.jira.database.MockQueryDslAccessor;
import com.atlassian.jira.database.QueryDslAccessor;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.mock.ofbiz.MockOfBizDelegator;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.user.util.MockUserKeyStore;
import com.atlassian.jira.user.util.UserManager;
import com.google.common.collect.ImmutableMap;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.ofbiz.core.entity.DelegatorInterface;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static com.google.common.collect.ImmutableList.of;
import static java.util.stream.Collectors.toList;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.when;

/**
 * Clean unit tests for OfBizUserDao.
 * <p>
 * In particular it tests the various rename user scenarios set out in https://extranet.atlassian.com/display/JIRADEV/Detect+rename+user+in+LDAP+Scenarios .
 */
@RunWith(MockitoJUnitRunner.class)
public class TestOfBizUserDao extends AbstractTransactionalOfBizTestCase {
    private CacheManager cacheManager;

    private ClusterLockService clusterLockService;
    private QueryDslAccessor queryDslAccessor;
    @Mock
    private ApplicationProperties applicationProperties;
    @Mock
    @AvailableInContainer
    private UserManager userManager;

    private OfBizTransactionManager transactionManager = new DefaultOfBizTransactionManager();
    private DatabaseConfig databaseConfig = new DatabaseConfig("postgres", "jira", new JndiDatasource("jira"));
    private final MockDatabaseConfigurationManager databaseConfigurationManager = new MockDatabaseConfigurationManager();

    @Before
    public void setup() {
        when(applicationProperties.getOption(APKeys.CACHE_ALL_USERS_AND_GROUPS)).thenReturn(Boolean.TRUE);
        cacheManager = new MemoryCacheManager();
        clusterLockService = new SimpleClusterLockService();
        when(userManager.getDirectory(anyLong())).thenReturn(directory);
        databaseConfigurationManager.setDatabaseConfiguration(databaseConfig);
        queryDslAccessor = new MockQueryDslAccessor();

    }

    @Test
    public void testRenameUserCaseOnly() throws Exception {
        final OfBizDelegator ofBizDelegator = new MockOfBizDelegator();
        final DirectoryDao directoryDao = new MockDirectoryDao(1L, 2L);
        final MockUserKeyStore userKeyStore = new MockUserKeyStore();
        userKeyStore.useDefaultMapping(false);
        final InternalMembershipDao internalMembershipDao = new OfBizInternalMembershipDao(ofBizDelegator, queryDslAccessor, cacheManager);
        OfBizUserDao ofBizUserDao = new OfBizUserDao(ofBizDelegator, directoryDao, internalMembershipDao, userKeyStore, null, cacheManager, clusterLockService, applicationProperties, transactionManager, databaseConfigurationManager);
        final User oldUser = createUser(1, "aSmith");
        ofBizUserDao.add(oldUser, PasswordCredential.NONE);

        assertEquals("asmith", userKeyStore.getUsernameForKey("asmith"));
        assertEquals("aSmith", ofBizUserDao.findByName(1L, "asmith").getName());

        ofBizUserDao.rename(oldUser, "ASMITH");
        assertEquals("asmith", userKeyStore.getUsernameForKey("asmith"));
        assertEquals("asmith", userKeyStore.getKeyForUsername("asmith"));
        assertEquals("ASMITH", ofBizUserDao.findByName(1L, "asmith").getName());
    }

    @Test
    public void testRenameUserCaseOnlyHandlesGroupMemberships() throws Exception {
        final OfBizDelegator ofBizDelegator = new MockOfBizDelegator();
        final DirectoryDao directoryDao = new MockDirectoryDao(1L, 2L);
        final MockUserKeyStore userKeyStore = new MockUserKeyStore();
        userKeyStore.useDefaultMapping(false);
        final InternalMembershipDao internalMembershipDao = new OfBizInternalMembershipDao(ofBizDelegator, queryDslAccessor, cacheManager);
        OfBizUserDao ofBizUserDao = new OfBizUserDao(ofBizDelegator, directoryDao, internalMembershipDao, userKeyStore, null, cacheManager, clusterLockService, applicationProperties, transactionManager, databaseConfigurationManager);
        final User oldUser = ofBizUserDao.add(createUser(1, "ASmith"), PasswordCredential.NONE);
        internalMembershipDao.addUserToGroup(1, (UserOrGroupStub) oldUser, new SimpleUserOrGroupStub(12, 1, "jira-devs"));

        // Preconditions
        assertEquals("asmith", userKeyStore.getUsernameForKey("asmith"));
        assertTrue(internalMembershipDao.isUserDirectMember(1, "asmith", "jira-devs"));

        ofBizUserDao.rename(oldUser, "ASMITH");
        assertEquals("asmith", userKeyStore.getUsernameForKey("asmith"));
        assertEquals("asmith", userKeyStore.getKeyForUsername("asmith"));
        assertNotNull(ofBizUserDao.findByName(1L, "ASMITH"));
        assertTrue(internalMembershipDao.isUserDirectMember(1, "asmith", "jira-devs"));
    }

    @Test
    public void testRenameUserSimple() throws Exception {
        final OfBizDelegator ofBizDelegator = new MockOfBizDelegator();
        final DirectoryDao directoryDao = new MockDirectoryDao(1L, 2L);
        final MockUserKeyStore userKeyStore = new MockUserKeyStore();
        userKeyStore.useDefaultMapping(false);
        final InternalMembershipDao internalMembershipDao = new OfBizInternalMembershipDao(ofBizDelegator, queryDslAccessor, cacheManager);
        OfBizUserDao ofBizUserDao = new OfBizUserDao(ofBizDelegator, directoryDao, internalMembershipDao, userKeyStore, null, cacheManager, clusterLockService, applicationProperties, transactionManager, databaseConfigurationManager);
        final User oldUser = createUser(1, "asmith");
        ofBizUserDao.add(oldUser, PasswordCredential.NONE);

        assertEquals("asmith", userKeyStore.getUsernameForKey("asmith"));

        ofBizUserDao.rename(oldUser, "abrown");
        assertEquals("abrown", userKeyStore.getUsernameForKey("asmith"));
        assertEquals(null, userKeyStore.getUsernameForKey("abrown"));
        assertEquals(null, userKeyStore.getKeyForUsername("asmith"));
        assertEquals("asmith", userKeyStore.getKeyForUsername("abrown"));
        assertNotNull(ofBizUserDao.findByName(1L, "abrown"));
        // Assert asmith is gone from cwd_user
        try {
            ofBizUserDao.findByName(1L, "asmith");
            fail();
        } catch (UserNotFoundException expected) {
        }
    }

    @Test
    public void testRenameUserScenario1() throws Exception {
        // User B is renamed to A; A previously existed but has been deleted.

        final OfBizDelegator ofBizDelegator = new MockOfBizDelegator();
        final DirectoryDao directoryDao = new MockDirectoryDao(1L, 2L);
        final MockUserKeyStore userKeyStore = new MockUserKeyStore();
        userKeyStore.useDefaultMapping(false);
        final InternalMembershipDao internalMembershipDao = new OfBizInternalMembershipDao(ofBizDelegator, queryDslAccessor, cacheManager);
        OfBizUserDao ofBizUserDao = new OfBizUserDao(ofBizDelegator, directoryDao, internalMembershipDao, userKeyStore, null, cacheManager, clusterLockService, applicationProperties, transactionManager, databaseConfigurationManager);
        final User oldUser = createUser(1, "asmith");
        ofBizUserDao.add(oldUser, PasswordCredential.NONE);
        userKeyStore.ensureUniqueKeyForNewUser("abrown");

        assertEquals("asmith", userKeyStore.getUsernameForKey("asmith"));
        assertEquals("abrown", userKeyStore.getUsernameForKey("abrown"));

        ofBizUserDao.rename(oldUser, "abrown");
        assertEquals("abrown", userKeyStore.getUsernameForKey("asmith"));
        assertEquals("abrown#1", userKeyStore.getUsernameForKey("abrown"));
        assertEquals(null, userKeyStore.getKeyForUsername("asmith"));
        assertEquals("asmith", userKeyStore.getKeyForUsername("abrown"));
        assertEquals("abrown", userKeyStore.getKeyForUsername("abrown#1"));
        assertNotNull(ofBizUserDao.findByName(1L, "abrown"));
        // Assert asmith is gone from cwd_user
        try {
            ofBizUserDao.findByName(1L, "asmith");
            fail();
        } catch (UserNotFoundException expected) {
        }
    }

    @Test
    public void testRenameUserScenario2() throws Exception {
        // User B is renamed to A, but A already exists in another directory, higher than this one.

        final OfBizDelegator ofBizDelegator = new MockOfBizDelegator();
        final DirectoryDao directoryDao = new MockDirectoryDao(1L, 2L);
        final MockUserKeyStore userKeyStore = new MockUserKeyStore();
        userKeyStore.useDefaultMapping(false);
        final InternalMembershipDao internalMembershipDao = new OfBizInternalMembershipDao(ofBizDelegator, queryDslAccessor, cacheManager);
        OfBizUserDao ofBizUserDao = new OfBizUserDao(ofBizDelegator, directoryDao, internalMembershipDao, userKeyStore, null, cacheManager, clusterLockService, applicationProperties, transactionManager, databaseConfigurationManager);
        final User oldUser = createUser(2, "asmith");
        ofBizUserDao.add(oldUser, PasswordCredential.NONE);
        ofBizUserDao.add(createUser(1, "abrown"), PasswordCredential.NONE);

        assertEquals("asmith", userKeyStore.getUsernameForKey("asmith"));
        assertEquals("abrown", userKeyStore.getUsernameForKey("abrown"));

        ofBizUserDao.rename(oldUser, "abrown");
        assertEquals("asmith", userKeyStore.getUsernameForKey("asmith"));
        assertEquals("abrown", userKeyStore.getUsernameForKey("abrown"));
        assertEquals("asmith", userKeyStore.getKeyForUsername("asmith"));
        assertEquals("abrown", userKeyStore.getKeyForUsername("abrown"));
        assertNotNull(ofBizUserDao.findByName(1L, "abrown"));
        assertNotNull(ofBizUserDao.findByName(2L, "abrown"));
        // Assert asmith is gone from cwd_user
        try {
            ofBizUserDao.findByName(2L, "asmith");
            fail();
        } catch (UserNotFoundException expected) {
        }
    }

    @Test
    public void testRenameUserScenario3() throws Exception {
        // https://extranet.atlassian.com/display/JIRADEV/Detect+rename+user+in+LDAP+Scenarios
        // User B is renamed to A, but A already exists in another directory, lower than this one.

        final OfBizDelegator ofBizDelegator = new MockOfBizDelegator();
        final DirectoryDao directoryDao = new MockDirectoryDao(2L, 1L);
        final MockUserKeyStore userKeyStore = new MockUserKeyStore();
        userKeyStore.useDefaultMapping(false);
        final InternalMembershipDao internalMembershipDao = new OfBizInternalMembershipDao(ofBizDelegator, queryDslAccessor, cacheManager);
        OfBizUserDao ofBizUserDao = new OfBizUserDao(ofBizDelegator, directoryDao, internalMembershipDao, userKeyStore, null, cacheManager, clusterLockService, applicationProperties, transactionManager, databaseConfigurationManager);
        final User oldUser = createUser(2, "asmith");
        ofBizUserDao.add(oldUser, PasswordCredential.NONE);
        ofBizUserDao.add(createUser(1, "abrown"), PasswordCredential.NONE);

        assertEquals("asmith", userKeyStore.getUsernameForKey("asmith"));
        assertEquals("abrown", userKeyStore.getUsernameForKey("abrown"));

        ofBizUserDao.rename(oldUser, "abrown");
        assertEquals("abrown", userKeyStore.getUsernameForKey("asmith"));
        assertEquals("abrown#1", userKeyStore.getUsernameForKey("abrown"));
        assertEquals(null, userKeyStore.getKeyForUsername("asmith"));
        assertEquals("asmith", userKeyStore.getKeyForUsername("abrown"));
        assertEquals("abrown", userKeyStore.getKeyForUsername("abrown#1"));
        assertNotNull(ofBizUserDao.findByName(1L, "abrown"));
        assertNotNull(ofBizUserDao.findByName(2L, "abrown"));
        // Assert asmith is gone from cwd_user
        try {
            ofBizUserDao.findByName(2L, "asmith");
            fail();
        } catch (UserNotFoundException expected) {
        }
        // Assert abrown#1 is not in cwd_user
        try {
            ofBizUserDao.findByName(2L, "abrown#1");
            fail();
        } catch (UserNotFoundException expected) {
        }
    }

    @Test
    public void testRenameUserScenario4() throws Exception {
        // https://extranet.atlassian.com/display/JIRADEV/Detect+rename+user+in+LDAP+Scenarios
        // User B is renamed to A, but A currently exists in this directory

        final OfBizDelegator ofBizDelegator = new MockOfBizDelegator();
        final DirectoryDao directoryDao = new MockDirectoryDao(1L);
        final MockUserKeyStore userKeyStore = new MockUserKeyStore();
        userKeyStore.useDefaultMapping(false);
        final InternalMembershipDao internalMembershipDao = new OfBizInternalMembershipDao(ofBizDelegator, queryDslAccessor, cacheManager);
        OfBizUserDao ofBizUserDao = new OfBizUserDao(ofBizDelegator, directoryDao, internalMembershipDao, userKeyStore, null, cacheManager, clusterLockService, applicationProperties, transactionManager, databaseConfigurationManager);
        final User oldUser = createUser(1, "asmith");
        ofBizUserDao.add(oldUser, PasswordCredential.NONE);
        ofBizUserDao.add(createUser(1, "abrown"), PasswordCredential.NONE);

        assertEquals("asmith", userKeyStore.getUsernameForKey("asmith"));
        assertEquals("abrown", userKeyStore.getUsernameForKey("abrown"));

        ofBizUserDao.rename(oldUser, "abrown");
        assertEquals("abrown", userKeyStore.getUsernameForKey("asmith"));
        assertEquals("abrown#1", userKeyStore.getUsernameForKey("abrown"));
        assertEquals(null, userKeyStore.getKeyForUsername("asmith"));
        assertEquals("asmith", userKeyStore.getKeyForUsername("abrown"));
        assertEquals("abrown", userKeyStore.getKeyForUsername("abrown#1"));
        assertNotNull(ofBizUserDao.findByName(1L, "abrown"));
        assertNotNull(ofBizUserDao.findByName(1L, "abrown#1"));
        // Assert asmith is gone from cwd_user
        try {
            ofBizUserDao.findByName(2L, "asmith");
            fail();
        } catch (UserNotFoundException expected) {
        }
    }

    @Test
    public void testRenameUserScenario5() throws Exception {
        // User B is renamed to A, A already exists in this directory, but is shadowed by A in a higher directory

        final OfBizDelegator ofBizDelegator = new MockOfBizDelegator();
        final DirectoryDao directoryDao = new MockDirectoryDao(1L, 2L);
        final MockUserKeyStore userKeyStore = new MockUserKeyStore();
        userKeyStore.useDefaultMapping(false);
        final InternalMembershipDao internalMembershipDao = new OfBizInternalMembershipDao(ofBizDelegator, queryDslAccessor, cacheManager);
        OfBizUserDao ofBizUserDao = new OfBizUserDao(ofBizDelegator, directoryDao, internalMembershipDao, userKeyStore, null, cacheManager, clusterLockService, applicationProperties, transactionManager, databaseConfigurationManager);
        final User oldUser = createUser(2, "asmith");
        ofBizUserDao.add(oldUser, PasswordCredential.NONE);
        ofBizUserDao.add(createUser(1, "abrown"), PasswordCredential.NONE);
        ofBizUserDao.add(createUser(2, "abrown"), PasswordCredential.NONE);

        assertEquals("asmith", userKeyStore.getUsernameForKey("asmith"));
        assertEquals("abrown", userKeyStore.getUsernameForKey("abrown"));

        ofBizUserDao.rename(oldUser, "abrown");
        assertEquals("asmith", userKeyStore.getUsernameForKey("asmith"));
        assertEquals("abrown", userKeyStore.getUsernameForKey("abrown"));
        assertEquals(null, userKeyStore.getUsernameForKey("asmith#1"));
        assertEquals(null, userKeyStore.getUsernameForKey("abrown#1"));
        assertEquals("asmith", userKeyStore.getKeyForUsername("asmith"));
        assertEquals("abrown", userKeyStore.getKeyForUsername("abrown"));
        assertNotNull(ofBizUserDao.findByName(1L, "abrown"));
        assertNotNull(ofBizUserDao.findByName(2L, "abrown"));
        // Assert asmith is gone from cwd_user
        try {
            ofBizUserDao.findByName(2L, "asmith");
            fail();
        } catch (UserNotFoundException expected) {
        }
    }

    @Test
    public void testRenameUserScenario6() throws Exception {
        // https://extranet.atlassian.com/display/JIRADEV/Detect+rename+user+in+LDAP+Scenarios
        // User B is renamed to A, A already exists in this directory, and is shadowing A in a lower directory

        final OfBizDelegator ofBizDelegator = new MockOfBizDelegator();
        final DirectoryDao directoryDao = new MockDirectoryDao(2L, 1L);
        final MockUserKeyStore userKeyStore = new MockUserKeyStore();
        userKeyStore.useDefaultMapping(false);
        final InternalMembershipDao internalMembershipDao = new OfBizInternalMembershipDao(ofBizDelegator, queryDslAccessor, cacheManager);
        OfBizUserDao ofBizUserDao = new OfBizUserDao(ofBizDelegator, directoryDao, internalMembershipDao, userKeyStore, null, cacheManager, clusterLockService, applicationProperties, transactionManager, databaseConfigurationManager);
        final User oldUser = createUser(2, "asmith");
        ofBizUserDao.add(oldUser, PasswordCredential.NONE);
        ofBizUserDao.add(createUser(1, "abrown"), PasswordCredential.NONE);
        ofBizUserDao.add(createUser(2, "abrown"), PasswordCredential.NONE);

        assertEquals("asmith", userKeyStore.getUsernameForKey("asmith"));
        assertEquals("abrown", userKeyStore.getUsernameForKey("abrown"));

        ofBizUserDao.rename(oldUser, "abrown");
        assertEquals("abrown", userKeyStore.getUsernameForKey("asmith"));
        assertEquals("abrown#1", userKeyStore.getUsernameForKey("abrown"));
        assertEquals(null, userKeyStore.getKeyForUsername("asmith"));
        assertEquals("asmith", userKeyStore.getKeyForUsername("abrown"));
        assertEquals("abrown", userKeyStore.getKeyForUsername("abrown#1"));
        assertNotNull(ofBizUserDao.findByName(1L, "abrown"));
        assertNotNull(ofBizUserDao.findByName(2L, "abrown"));
        assertNotNull(ofBizUserDao.findByName(2L, "abrown#1"));
        // Assert asmith is gone from cwd_user
        try {
            ofBizUserDao.findByName(2L, "asmith");
            fail();
        } catch (UserNotFoundException expected) {
        }
    }

    @Test
    public void testRenameUserScenario7() throws Exception {
        // https://extranet.atlassian.com/display/JIRADEV/Detect+rename+user+in+LDAP+Scenarios
        // User B is renamed to A, B was shadowing another user
        final OfBizDelegator ofBizDelegator = new MockOfBizDelegator();
        final DirectoryDao directoryDao = new MockDirectoryDao(2L, 1L);
        final MockUserKeyStore userKeyStore = new MockUserKeyStore();
        userKeyStore.useDefaultMapping(false);
        final InternalMembershipDao internalMembershipDao = new OfBizInternalMembershipDao(ofBizDelegator, queryDslAccessor, cacheManager);
        OfBizUserDao ofBizUserDao = new OfBizUserDao(ofBizDelegator, directoryDao, internalMembershipDao, userKeyStore, null, cacheManager, clusterLockService, applicationProperties, transactionManager, databaseConfigurationManager);
        final User oldUser = createUser(2, "asmith");
        ofBizUserDao.add(oldUser, PasswordCredential.NONE);
        ofBizUserDao.add(createUser(1, "asmith"), PasswordCredential.NONE);

        assertEquals("asmith", userKeyStore.getUsernameForKey("asmith"));
        assertEquals(null, userKeyStore.getUsernameForKey("abrown"));

        ofBizUserDao.rename(oldUser, "abrown");
        assertEquals("asmith", userKeyStore.getKeyForUsername("abrown"));
        assertEquals("XYZ10001", userKeyStore.getKeyForUsername("asmith"));
        assertEquals("abrown", userKeyStore.getUsernameForKey("asmith"));
        assertEquals("asmith", userKeyStore.getUsernameForKey("XYZ10001"));
        assertNotNull(ofBizUserDao.findByName(2L, "abrown"));
        assertNotNull(ofBizUserDao.findByName(1L, "asmith"));
        // Assert asmith is gone from cwd_user
        try {
            ofBizUserDao.findByName(2L, "asmith");
            fail();
        } catch (UserNotFoundException expected) {
        }
    }

    @Test
    public void testRenameUserScenario8() throws Exception {
        // https://extranet.atlassian.com/display/JIRADEV/Detect+rename+user+in+LDAP+Scenarios
        // User B is renamed to A, B is currently shadowed

        final OfBizDelegator ofBizDelegator = new MockOfBizDelegator();
        final DirectoryDao directoryDao = new MockDirectoryDao(1L, 2L);
        final MockUserKeyStore userKeyStore = new MockUserKeyStore();
        userKeyStore.useDefaultMapping(false);
        final InternalMembershipDao internalMembershipDao = new OfBizInternalMembershipDao(ofBizDelegator, queryDslAccessor, cacheManager);
        OfBizUserDao ofBizUserDao = new OfBizUserDao(ofBizDelegator, directoryDao, internalMembershipDao, userKeyStore, null, cacheManager, clusterLockService, applicationProperties, transactionManager, databaseConfigurationManager);
        final User oldUser = createUser(2, "asmith");
        ofBizUserDao.add(oldUser, PasswordCredential.NONE);
        ofBizUserDao.add(createUser(1, "asmith"), PasswordCredential.NONE);

        assertEquals("asmith", userKeyStore.getUsernameForKey("asmith"));
        assertEquals(null, userKeyStore.getUsernameForKey("abrown"));

        ofBizUserDao.rename(oldUser, "abrown");
        assertEquals("abrown", userKeyStore.getKeyForUsername("abrown"));
        assertEquals("asmith", userKeyStore.getKeyForUsername("asmith"));
        assertEquals("asmith", userKeyStore.getUsernameForKey("asmith"));
        assertEquals("abrown", userKeyStore.getUsernameForKey("abrown"));
        assertNotNull(ofBizUserDao.findByName(2L, "abrown"));
        assertNotNull(ofBizUserDao.findByName(1L, "asmith"));
        // Assert asmith is gone from cwd_user
        try {
            ofBizUserDao.findByName(2L, "asmith");
            fail();
        } catch (UserNotFoundException expected) {
        }
    }

    @Test
    public void testFindNamesOfUsersInGroups()
            throws Exception {
        final OfBizDelegator ofBizDelegator = new MockOfBizDelegator();
        final DirectoryDao directoryDao = new MockDirectoryDao(1L, 2L);
        final MockUserKeyStore userKeyStore = new MockUserKeyStore();
        userKeyStore.useDefaultMapping(false);
        final InternalMembershipDao internalMembershipDao = new OfBizInternalMembershipDao(ofBizDelegator, queryDslAccessor, cacheManager);
        OfBizUserDao ofBizUserDao = new OfBizUserDao(ofBizDelegator, directoryDao, internalMembershipDao, userKeyStore, null, cacheManager, clusterLockService, applicationProperties, transactionManager, databaseConfigurationManager);
        final User user = ofBizUserDao.add(createUser(1, "ASmith"), PasswordCredential.NONE);
        internalMembershipDao.addUserToGroup(1L, (UserOrGroupStub) user, new SimpleUserOrGroupStub(12L, 1L, "jira-devs"));

        Collection<String> results = ofBizUserDao.findNamesOfUsersInGroups(of("jira-devs"));

        assertThat(results, containsInAnyOrder("asmith"));
    }

    @Test
    public void testFindNamesOfUsersInGroupsGroupNotFound()
            throws Exception {
        final OfBizDelegator ofBizDelegator = new MockOfBizDelegator();
        final DirectoryDao directoryDao = new MockDirectoryDao(1L, 2L);
        final MockUserKeyStore userKeyStore = new MockUserKeyStore();
        userKeyStore.useDefaultMapping(false);
        final InternalMembershipDao internalMembershipDao = new OfBizInternalMembershipDao(ofBizDelegator, queryDslAccessor, cacheManager);
        OfBizUserDao ofBizUserDao = new OfBizUserDao(ofBizDelegator, directoryDao, internalMembershipDao, userKeyStore, null, cacheManager, clusterLockService, applicationProperties, transactionManager, databaseConfigurationManager);
        final User user = ofBizUserDao.add(createUser(1, "ASmith"), PasswordCredential.NONE);
        internalMembershipDao.addUserToGroup(1L, (UserOrGroupStub) user, new SimpleUserOrGroupStub(12L, 1L, "jira-devs"));

        Collection<String> results = ofBizUserDao.findNamesOfUsersInGroups(of("galah-group"));

        assertThat(results, empty());
    }

    @Test
    public void testGetAllAttributeKeys()
            throws Exception {
        final MockOfBizDelegator ofBizDelegator = new MockOfBizDelegator();
        final DirectoryDao directoryDao = new MockDirectoryDao(1L, 2L);
        final MockUserKeyStore userKeyStore = new MockUserKeyStore();
        userKeyStore.useDefaultMapping(false);
        final InternalMembershipDao internalMembershipDao = new OfBizInternalMembershipDao(ofBizDelegator, queryDslAccessor, cacheManager);
        OfBizUserDao ofBizUserDao = new OfBizUserDao(ofBizDelegator, directoryDao, internalMembershipDao, userKeyStore, null, cacheManager, clusterLockService, applicationProperties, transactionManager, databaseConfigurationManager);

        ofBizDelegator.createValue(UserAttributeEntity.ENTITY, ImmutableMap.of(UserAttributeEntity.NAME, "one"));
        ofBizDelegator.createValue(UserAttributeEntity.ENTITY, ImmutableMap.of(UserAttributeEntity.NAME, "two"));

        Collection<String> results = ofBizUserDao.getAllAttributeKeys();

        assertThat(results, containsInAnyOrder("one", "two"));
    }

    @Test
    public void testProcessUsersShouldIterateOverEveryUser()
            throws Exception {
        final MockOfBizDelegator ofBizDelegator = new MockOfBizDelegator();
        final DirectoryDao directoryDao = new MockDirectoryDao(1L, 2L);
        final MockUserKeyStore userKeyStore = new MockUserKeyStore();
        userKeyStore.useDefaultMapping(false);
        final InternalMembershipDao internalMembershipDao = new OfBizInternalMembershipDao(ofBizDelegator, queryDslAccessor, cacheManager);
        OfBizUserDao ofBizUserDao = new OfBizUserDao(ofBizDelegator, directoryDao, internalMembershipDao, userKeyStore, null, cacheManager, clusterLockService, applicationProperties, transactionManager, databaseConfigurationManager);

        ofBizDelegator.createValue(UserWithDirectoryEntity.ENTITY, ImmutableMap.of(
                UserWithDirectoryEntity.ACTIVE, 1,
                UserEntity.USER_NAME, "asmith",
                UserEntity.LOWER_USER_NAME, "asmith",
                UserEntity.DIRECTORY_ID, 1L,
                UserEntity.ACTIVE, 1
        ));
        ofBizDelegator.createValue(UserWithDirectoryEntity.ENTITY, ImmutableMap.of(
                UserWithDirectoryEntity.ACTIVE, 1,
                UserEntity.USER_NAME, "abrown",
                UserEntity.LOWER_USER_NAME, "abrown",
                UserEntity.DIRECTORY_ID, 1L,
                UserEntity.ACTIVE, 1
        ));

        List<User> userList = new ArrayList<>();
        ofBizUserDao.processUsers(userList::add);

        assertThat(userList, hasSize(2));
        assertThat(userList.stream().map(User::getName).collect(toList()), containsInAnyOrder("asmith", "abrown"));
    }

    @Test
    public void testProcessUsersShouldIterateOverEveryUserInMultipleDirectories()
            throws Exception {
        final MockOfBizDelegator ofBizDelegator = new MockOfBizDelegator();
        final DirectoryDao directoryDao = new MockDirectoryDao(1L, 2L);
        final MockUserKeyStore userKeyStore = new MockUserKeyStore();
        userKeyStore.useDefaultMapping(false);
        final InternalMembershipDao internalMembershipDao = new OfBizInternalMembershipDao(ofBizDelegator, queryDslAccessor, cacheManager);
        OfBizUserDao ofBizUserDao = new OfBizUserDao(ofBizDelegator, directoryDao, internalMembershipDao, userKeyStore, null, cacheManager, clusterLockService, applicationProperties, transactionManager, databaseConfigurationManager);

        ofBizDelegator.createValue(UserWithDirectoryEntity.ENTITY, ImmutableMap.of(
                UserWithDirectoryEntity.ACTIVE, 1,
                UserEntity.USER_NAME, "asmith",
                UserEntity.LOWER_USER_NAME, "asmith",
                UserEntity.DIRECTORY_ID, 1L,
                UserEntity.ACTIVE, 1
        ));
        ofBizDelegator.createValue(UserWithDirectoryEntity.ENTITY, ImmutableMap.of(
                UserWithDirectoryEntity.ACTIVE, 1,
                UserEntity.USER_NAME, "abrown",
                UserEntity.LOWER_USER_NAME, "abrown",
                UserEntity.DIRECTORY_ID, 2L,
                UserEntity.ACTIVE, 1
        ));

        List<User> userList = new ArrayList<>();
        ofBizUserDao.processUsers(userList::add);

        assertThat(userList, hasSize(2));
        assertThat(userList.stream().map(User::getName).collect(toList()), containsInAnyOrder("asmith", "abrown"));
    }

    @Test
    public void testProcessUsersShouldIterateOverEveryUserIgnoresInactiveDirectories()
            throws Exception {
        final MockOfBizDelegator ofBizDelegator = new MockOfBizDelegator();
        final DirectoryDao directoryDao = new MockDirectoryDao(1L, 2L);
        final MockUserKeyStore userKeyStore = new MockUserKeyStore();
        userKeyStore.useDefaultMapping(false);
        final InternalMembershipDao internalMembershipDao = new OfBizInternalMembershipDao(ofBizDelegator, queryDslAccessor, cacheManager);
        OfBizUserDao ofBizUserDao = new OfBizUserDao(ofBizDelegator, directoryDao, internalMembershipDao, userKeyStore, null, cacheManager, clusterLockService, applicationProperties, transactionManager, databaseConfigurationManager);

        ofBizDelegator.createValue(UserWithDirectoryEntity.ENTITY, ImmutableMap.of(
                UserWithDirectoryEntity.ACTIVE, 1,
                UserEntity.USER_NAME, "asmith",
                UserEntity.LOWER_USER_NAME, "asmith",
                UserEntity.DIRECTORY_ID, 1L,
                UserEntity.ACTIVE, 1
        ));
        ofBizDelegator.createValue(UserWithDirectoryEntity.ENTITY, ImmutableMap.of(
                UserWithDirectoryEntity.ACTIVE, 0,
                UserEntity.USER_NAME, "abrown",
                UserEntity.LOWER_USER_NAME, "abrown",
                UserEntity.DIRECTORY_ID, 2L,
                UserEntity.ACTIVE, 1
        ));

        List<User> userList = new ArrayList<>();
        ofBizUserDao.processUsers(userList::add);

        assertThat(userList, hasSize(1));
        assertThat(userList.stream().map(User::getName).collect(toList()), containsInAnyOrder("asmith"));
    }

    @Test
    public void testProcessUsersShouldIterateOverEveryUserHandlesShadowedUsers()
            throws Exception {
        final MockOfBizDelegator ofBizDelegator = new MockOfBizDelegator();
        final DirectoryDao directoryDao = new MockDirectoryDao(1L, 2L);
        final MockUserKeyStore userKeyStore = new MockUserKeyStore();
        userKeyStore.useDefaultMapping(false);
        final InternalMembershipDao internalMembershipDao = new OfBizInternalMembershipDao(ofBizDelegator, queryDslAccessor, cacheManager);
        OfBizUserDao ofBizUserDao = new OfBizUserDao(ofBizDelegator, directoryDao, internalMembershipDao, userKeyStore, null, cacheManager, clusterLockService, applicationProperties, transactionManager, databaseConfigurationManager);

        ofBizDelegator.createValue(UserWithDirectoryEntity.ENTITY, ImmutableMap.<String, Object>builder()
                .put(UserWithDirectoryEntity.ACTIVE, 1)
                .put(UserEntity.USER_NAME, "asmith")
                .put(UserEntity.LOWER_USER_NAME, "asmith")
                .put(UserEntity.DIRECTORY_ID, 1L)
                .put(UserEntity.ACTIVE, 1)
                .put(UserWithDirectoryEntity.POSITION, 1)
                .build()
        );
        ofBizDelegator.createValue(UserWithDirectoryEntity.ENTITY, ImmutableMap.<String, Object>builder()
                .put(UserWithDirectoryEntity.ACTIVE, 1)
                .put(UserEntity.USER_NAME, "asmith")
                .put(UserEntity.LOWER_USER_NAME, "asmith")
                .put(UserEntity.DIRECTORY_ID, 2L)
                .put(UserEntity.ACTIVE, 1)
                .put(UserWithDirectoryEntity.POSITION, 2)
                .build()
        );

        List<User> userList = new ArrayList<>();
        ofBizUserDao.processUsers(userList::add);

        assertThat(userList, hasSize(1));
        User resultUser = userList.get(0);
        assertThat(resultUser.getName(), is("asmith"));
        assertThat(resultUser.getDirectoryId(), is(1L));
    }

    @Test
    public void testProcessUsersShouldIterateOverEveryUserHandlesShadowedUsersReversedPositions()
            throws Exception {
        final MockOfBizDelegator ofBizDelegator = new MockOfBizDelegator();
        final DirectoryDao directoryDao = new MockDirectoryDao(1L, 2L);
        final MockUserKeyStore userKeyStore = new MockUserKeyStore();
        userKeyStore.useDefaultMapping(false);
        final InternalMembershipDao internalMembershipDao = new OfBizInternalMembershipDao(ofBizDelegator, queryDslAccessor, cacheManager);
        OfBizUserDao ofBizUserDao = new OfBizUserDao(ofBizDelegator, directoryDao, internalMembershipDao, userKeyStore, null, cacheManager, clusterLockService, applicationProperties, transactionManager, databaseConfigurationManager);

        ofBizDelegator.createValue(UserWithDirectoryEntity.ENTITY, ImmutableMap.<String, Object>builder()
                .put(UserWithDirectoryEntity.ACTIVE, 1)
                .put(UserEntity.USER_NAME, "asmith")
                .put(UserEntity.LOWER_USER_NAME, "asmith")
                .put(UserEntity.DIRECTORY_ID, 1L)
                .put(UserEntity.ACTIVE, 1)
                .put(UserWithDirectoryEntity.POSITION, 2)
                .build()
        );
        ofBizDelegator.createValue(UserWithDirectoryEntity.ENTITY, ImmutableMap.<String, Object>builder()
                .put(UserWithDirectoryEntity.ACTIVE, 1)
                .put(UserEntity.USER_NAME, "asmith")
                .put(UserEntity.LOWER_USER_NAME, "asmith")
                .put(UserEntity.DIRECTORY_ID, 2L)
                .put(UserEntity.ACTIVE, 1)
                .put(UserWithDirectoryEntity.POSITION, 1)
                .build()
        );

        List<User> userList = new ArrayList<>();
        ofBizUserDao.processUsers(userList::add);

        assertThat(userList, hasSize(1));
        User resultUser = userList.get(0);
        assertThat(resultUser.getName(), is("asmith"));
        assertThat(resultUser.getDirectoryId(), is(2L));
    }

    @Test
    public void testProcessUsersShouldIterateOverEveryUserIgnoreDisabledDirectories()
            throws Exception {
        final MockOfBizDelegator ofBizDelegator = new MockOfBizDelegator();
        final DirectoryDao directoryDao = new MockDirectoryDao(1L, 2L);
        final MockUserKeyStore userKeyStore = new MockUserKeyStore();
        userKeyStore.useDefaultMapping(false);
        final InternalMembershipDao internalMembershipDao = new OfBizInternalMembershipDao(ofBizDelegator, queryDslAccessor, cacheManager);
        OfBizUserDao ofBizUserDao = new OfBizUserDao(ofBizDelegator, directoryDao, internalMembershipDao, userKeyStore, null, cacheManager, clusterLockService, applicationProperties, transactionManager, databaseConfigurationManager);

        //This one is disabled, so the second user should be used instead
        ofBizDelegator.createValue(UserWithDirectoryEntity.ENTITY, ImmutableMap.<String, Object>builder()
                .put(UserWithDirectoryEntity.ACTIVE, 0)
                .put(UserEntity.USER_NAME, "asmith")
                .put(UserEntity.LOWER_USER_NAME, "asmith")
                .put(UserEntity.DIRECTORY_ID, 1L)
                .put(UserEntity.ACTIVE, 1)
                .put(UserWithDirectoryEntity.POSITION, 1)
                .build()
        );
        ofBizDelegator.createValue(UserWithDirectoryEntity.ENTITY, ImmutableMap.<String, Object>builder()
                .put(UserWithDirectoryEntity.ACTIVE, 1)
                .put(UserEntity.USER_NAME, "asmith")
                .put(UserEntity.LOWER_USER_NAME, "asmith")
                .put(UserEntity.DIRECTORY_ID, 2L)
                .put(UserEntity.ACTIVE, 1)
                .put(UserWithDirectoryEntity.POSITION, 2)
                .build()
        );

        List<User> userList = new ArrayList<>();
        ofBizUserDao.processUsers(userList::add);

        assertThat(userList, hasSize(1));
        User resultUser = userList.get(0);
        assertThat(resultUser.getName(), is("asmith"));
        assertThat(resultUser.getDirectoryId(), is(2L));
    }

    private User createUser(final int directoryId, final String username) {
        return new UserTemplate(username, directoryId);
    }
}
