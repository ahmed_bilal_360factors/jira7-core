package com.atlassian.query.clause;

import com.atlassian.query.operand.SingleValueOperand;
import com.atlassian.query.operator.Operator;
import com.google.common.collect.ImmutableList;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * @since v4.0
 */
public class TestAndClause {
    @Rule
    public final ExpectedException expectedException = ExpectedException.none();

    @Rule
    public final MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private Clause mockClause;

    @Test
    public void testNullConstructorArgument() {
        expectedException.expect(IllegalArgumentException.class);
        new AndClause((Clause) null);
    }

    @Test
    public void testNullConstructorFirstArgument() {
        expectedException.expect(IllegalArgumentException.class);
        new AndClause(null, mockClause);
    }

    @Test
    public void testNullConstructorSecondArgument() {
        expectedException.expect(IllegalArgumentException.class);
        new AndClause(mockClause, null);
    }

    @Test
    public void testNullConstructorBothArguments() {
        expectedException.expect(IllegalArgumentException.class);
        new AndClause(null, null);
    }

    @Test
    public void testNullListConstructorArguments() {
        expectedException.expect(IllegalArgumentException.class);
        new AndClause((List<Clause>) null);
    }

    @Test
    public void testEmptyListConstructorArguments() {
        expectedException.expect(IllegalArgumentException.class);
        new AndClause(Collections.<Clause>emptyList());
    }

    @Test
    public void testName() throws Exception {
        assertEquals("AND", new AndClause(mockClause).getName());
    }

    @Test
    public void testToString() throws Exception {
        TerminalClause terminalClause1 = new TerminalClauseImpl("testField", Operator.EQUALS, new SingleValueOperand("test"));
        TerminalClause terminalClause2 = new TerminalClauseImpl("anotherField", Operator.GREATER_THAN, new SingleValueOperand("other"));
        AndClause andClause = new AndClause(terminalClause1, terminalClause2);
        assertEquals("{testField = \"test\"} AND {anotherField > \"other\"}", andClause.toString());
    }

    @Test
    public void testToStringWithPrecedence() throws Exception {
        TerminalClause terminalClause1 = new TerminalClauseImpl("testField", Operator.EQUALS, new SingleValueOperand("test"));
        TerminalClause terminalClause2 = new TerminalClauseImpl("anotherField", Operator.GREATER_THAN, new SingleValueOperand("other"));
        NotClause notClause = new NotClause(new TerminalClauseImpl("thirdField", Operator.GREATER_THAN, new SingleValueOperand("other")));
        OrClause orClause = new OrClause(new TerminalClauseImpl("fourthField", Operator.GREATER_THAN, new SingleValueOperand("other")),
                new TerminalClauseImpl("fifthField", Operator.GREATER_THAN, new SingleValueOperand("other")));
        AndClause andClause = new AndClause(terminalClause1, terminalClause2, notClause, orClause);
        assertEquals("{testField = \"test\"} AND {anotherField > \"other\"} AND NOT {thirdField > \"other\"} AND ( {fourthField > \"other\"} OR {fifthField > \"other\"} )", andClause.toString());
    }

    @Test
    public void testToStringWithPrecedenceNestedAnd() throws Exception {
        TerminalClause terminalClause1 = new TerminalClauseImpl("testField", Operator.EQUALS, new SingleValueOperand("test"));
        AndClause subAndClause = new AndClause(new TerminalClauseImpl("fourthField", Operator.GREATER_THAN, new SingleValueOperand("other")),
                new TerminalClauseImpl("fifthField", Operator.GREATER_THAN, new SingleValueOperand("other")));
        AndClause andClause = new AndClause(terminalClause1, subAndClause);
        assertEquals("{testField = \"test\"} AND {fourthField > \"other\"} AND {fifthField > \"other\"}", andClause.toString());
    }

    @Test
    public void testVisit() throws Exception {
        final AtomicBoolean visitCalled = new AtomicBoolean(false);
        ClauseVisitor visitor = new ClauseVisitor() {
            public Object visit(final AndClause andClause) {
                visitCalled.set(true);
                return null;
            }

            public Object visit(final NotClause notClause) {
                return failVisitor();
            }

            public Object visit(final OrClause orClause) {
                return failVisitor();
            }

            public Object visit(final TerminalClause clause) {
                return failVisitor();
            }

            @Override
            public Object visit(WasClause clause) {
                return failVisitor();
            }

            @Override
            public Object visit(ChangedClause clause) {
                return failVisitor();
            }
        };
        new AndClause(mockClause).accept(visitor);
        assertTrue(visitCalled.get());
    }

    private Object failVisitor() {
        fail("Should not be called");
        return null;
    }

    @Test
    public void testHappyPath() throws Exception {
        TerminalClause terminalClause1 = new TerminalClauseImpl("testField", Operator.EQUALS, new SingleValueOperand("test"));
        TerminalClause terminalClause2 = new TerminalClauseImpl("anotherField", Operator.GREATER_THAN, new SingleValueOperand("other"));
        AndClause andClause = new AndClause(ImmutableList.of(terminalClause1, terminalClause2));
        assertEquals(2, andClause.getClauses().size());
    }
}
