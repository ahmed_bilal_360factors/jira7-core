package com.atlassian.jira.web.component.subtask;

import com.atlassian.cache.CacheManager;
import com.atlassian.cache.memory.MemoryCacheManager;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.bean.SubTaskBean;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.mock.MockApplicationProperties;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.template.velocity.DefaultVelocityEngineFactory;
import com.atlassian.jira.template.velocity.DefaultVelocityTemplatingEngine;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.velocity.JiraVelocityManager;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;

import static org.apache.commons.lang3.StringUtils.isNotBlank;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestSubTaskReorderColumnLayoutItem {
    /*************************************************************************************************************/
    /**
     * Have no idea what it does, but it required For DefaultVelocityTemplatingEngine to work.
     * Copy pasted from {@link com.atlassian.jira.template.velocity.TestDefaultVelocityTemplatingEngine}
     */
    @Rule
    public TestRule initMockitoMock = MockitoMocksInContainer.forTest(this);

    @AvailableInContainer
    @Mock
    private EventPublisher eventPublisher;

    @SuppressWarnings("UnusedDeclaration")
    @AvailableInContainer(instantiateMe = true, interfaceClass = CacheManager.class)
    private MemoryCacheManager cacheManager;
    /*************************************************************************************************************/

    private DefaultVelocityTemplatingEngine templatingEngine = new DefaultVelocityTemplatingEngine(
            new JiraVelocityManager(null, new DefaultVelocityEngineFactory()),
            new MockApplicationProperties() {
                @Override
                public String getEncoding() {
                    return "UTF-8";
                }
            });

    @Mock
    Issue issue;
    @Mock
    Issue parent;
    @Mock
    PermissionManager permissionManager;
    @Mock
    SubTaskBean subTaskBean;
    @Mock
    ApplicationUser user;
    @Mock
    I18nHelper i18n;

    @Before
    public void setUp() throws Exception {
        when(i18n.getText(anyString())).thenReturn("text");
    }

    @Test
    public void shouldProduceHtmlWithEnabledReorderControls() throws Exception {
        String etalonHtml = "<!-- usage marker --><div class=\"subtask-reorder\">                        <img src=\"/images/border/spacer.gif\" class=\"sortArrow\" alt=\"\" />                            <a href=\"/secure/MoveIssueLink.jspa?id=0&currentSubTaskSequence=-1&subTaskSequence=0\"               title=\"text\"><span                    class=\"icon-default aui-icon aui-icon-small aui-iconfont-down\">text</span></a>            </div>";

        when(permissionManager.hasPermission(Permissions.EDIT_ISSUE, parent, user))
                .thenReturn(true);

        SubTaskReorderColumnLayoutItem subject = new SubTaskReorderColumnLayoutItem(permissionManager, user, templatingEngine,
                subTaskBean, "all", "",
                parent, i18n);

        String actualHtml = subject.getHtml(Collections.emptyMap(), issue).replace("\n", "").replace("\t", "");

        assertTrue(isNotBlank(actualHtml));
        assertEquals(etalonHtml, actualHtml);
    }

    @Test
    public void shoudProduceHtmlWithDisabledReorderControls() throws Exception {
        String etalonHtml = "<!-- usage marker --><div class=\"subtask-reorder\">            &nbsp;    </div>";

        when(permissionManager.hasPermission(Permissions.EDIT_ISSUE, parent, user))
                .thenReturn(false);

        SubTaskReorderColumnLayoutItem subject = new SubTaskReorderColumnLayoutItem(permissionManager, user, templatingEngine,
                subTaskBean, "all", "",
                parent, i18n);

        String actualHtml = subject.getHtml(Collections.emptyMap(), issue).replace("\n", "").replace("\t", "");

        assertTrue(isNotBlank(actualHtml));
        assertEquals(etalonHtml, actualHtml);
    }
}
