package com.atlassian.jira.issue.security;

import com.atlassian.cache.CacheManager;
import com.atlassian.cache.memory.MemoryCacheManager;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.association.NodeAssociationStore;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.database.QueryDslAccessor;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.index.IssueIndexingService;
import com.atlassian.jira.junit.rules.MockitoContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.mock.component.MockComponentWorker;
import com.atlassian.jira.mock.i18n.MockI18nHelper;
import com.atlassian.jira.mock.ofbiz.MockOfBizDelegator;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.permission.PermissionContextFactory;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.scheme.SchemeEntity;
import com.atlassian.jira.scheme.SchemeFactory;
import com.atlassian.jira.security.SecurityTypeManager;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.task.TaskDescriptor;
import com.atlassian.jira.task.TaskManager;
import com.atlassian.jira.util.I18nHelper;
import com.google.common.collect.ImmutableMap;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.ofbiz.core.entity.GenericEntityException;

import java.util.concurrent.Callable;

import static org.junit.Assert.assertFalse;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.util.AssertionErrors.assertTrue;

@RunWith(MockitoJUnitRunner.class)
public class TestIssueSecuritySchemeManagerImpl {
    @Rule
    public MockitoContainer container = MockitoMocksInContainer.rule(this);

    @Mock
    private ProjectManager projectManager;
    @Mock
    private SecurityTypeManager securityTypeManager;
    @Mock
    private PermissionContextFactory permissionContextFactory;
    @Mock
    private SchemeFactory schemeFactory;
    @Mock
    private EventPublisher eventPublisher;
    @Mock
    private GroupManager groupManager;
    @Mock
    private NodeAssociationStore nodeAssociationStore;

    private CacheManager cacheManager;

    @Mock
    private ApplicationProperties applicationProperties;
    @Mock
    private IssueSecuritySchemeManagerImpl issueSecuritySchemeMngr;
    @Mock
    private IssueIndexingService issueIndexingService;
    @Mock
    private IssueManager issueManager;
    @Mock
    private TaskManager taskManager;
    @Mock
    private QueryDslAccessor queryDslAccessor;
    private I18nHelper i18nHelper = new MockI18nHelper();

    private MockOfBizDelegator ofBizDelegator;

    @Before
    public void setUp() {
        ofBizDelegator = new MockOfBizDelegator();
        new MockComponentWorker().init().addMock(OfBizDelegator.class, ofBizDelegator);

        cacheManager = new MemoryCacheManager();

        issueSecuritySchemeMngr = new IssueSecuritySchemeManagerImpl(projectManager, securityTypeManager,
                permissionContextFactory, schemeFactory, eventPublisher, ofBizDelegator, groupManager,
                nodeAssociationStore, cacheManager, issueIndexingService, issueManager, taskManager,
                queryDslAccessor, i18nHelper);
    }

    @Test
    public void testGroupUsed() throws GenericEntityException {
        assertFalse("Group should not be used", issueSecuritySchemeMngr.isGroupUsed("myGroup"));

        issueSecuritySchemeMngr.createSchemeEntityNoEvent(null, new SchemeEntity("group", "myGroup", 7L));

        assertTrue("Group should not be used", issueSecuritySchemeMngr.isGroupUsed("myGroup"));
    }

    @Test
    public void testAssignSchemeToProject() {
        Project project = new MockProject(1, "HSP");
        TaskDescriptor mockTask = mock(TaskDescriptor.class);

        when(taskManager.submitTask(any(Callable.class), eq("admin.iss.associate.security.scheme [HSP]"), eq(new AssignIssueSecuritySchemeTaskContext(project)), eq(true)))
                .thenReturn(mockTask);

        IssueSecuritySchemeManagerImpl issueSecuritySchemeManagerImpl = new IssueSecuritySchemeManagerImpl(projectManager, securityTypeManager,
                permissionContextFactory, schemeFactory, eventPublisher, ofBizDelegator, groupManager,
                nodeAssociationStore, cacheManager, issueIndexingService, issueManager, taskManager,
                queryDslAccessor, i18nHelper);

        issueSecuritySchemeManagerImpl.assignSchemeToProject(project, 10L, ImmutableMap.of(1L, 11L, 2L, 12L));

        verify(taskManager, times(1)).submitTask(any(Callable.class), eq("admin.iss.associate.security.scheme [HSP]"), eq(new AssignIssueSecuritySchemeTaskContext(project)), eq(true));
        verify(mockTask, times(1)).getProgressURL();
    }


}
