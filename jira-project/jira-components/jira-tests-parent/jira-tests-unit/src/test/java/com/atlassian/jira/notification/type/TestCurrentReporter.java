package com.atlassian.jira.notification.type;

import com.atlassian.jira.event.issue.IssueEvent;
import com.atlassian.jira.mock.security.MockSimpleAuthenticationContext;
import org.junit.Test;

import java.util.Locale;

import static com.google.common.collect.Maps.newHashMap;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doReturn;

public class TestCurrentReporter extends AbstractNotificationTestCase {
    @Test
    public void testGetDisplayName() {
        CurrentReporter cr = new CurrentReporter(new MockSimpleAuthenticationContext(null, Locale.ENGLISH));
        assertEquals("Reporter", cr.getDisplayName());
    }

    @Test
    public void testGetRecipientsWithNoLevelSet() throws Exception {
        issue.setReporterId(user.getKey());

        final IssueEvent event = new IssueEvent(issue, newHashMap(), null, null);
        final CurrentReporter cr = new CurrentReporter(new MockSimpleAuthenticationContext(null, Locale.ENGLISH));
        checkRecipients(cr.getRecipients(event, null), user);
    }

    @Test
    public void testGetRecipientsWithLevelSatisfied() throws Exception {
        issue.setReporterId(user.getKey());
        doReturn(true).when(groupManager).isUserInGroup(user, "group1");

        final IssueEvent event = new IssueEvent(issue, paramsWithLevel(), null, null);
        final CurrentReporter cr = new CurrentReporter(new MockSimpleAuthenticationContext(null, Locale.ENGLISH));
        checkRecipients(cr.getRecipients(event, null), user);
    }

    @Test
    public void testGetRecipientsWithLevelNotSatisfied() throws Exception {
        issue.setReporterId(user.getKey());

        final IssueEvent event = new IssueEvent(issue, paramsWithLevel(), null, null);
        final CurrentReporter cr = new CurrentReporter(new MockSimpleAuthenticationContext(null, Locale.ENGLISH));
        checkRecipients(cr.getRecipients(event, null));
    }

}
