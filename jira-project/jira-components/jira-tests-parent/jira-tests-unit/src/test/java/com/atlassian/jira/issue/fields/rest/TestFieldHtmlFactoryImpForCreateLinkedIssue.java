package com.atlassian.jira.issue.fields.rest;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueFieldConstants;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.customfields.OperationContext;
import com.atlassian.jira.issue.fields.DescriptionSystemField;
import com.atlassian.jira.issue.fields.Field;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.fields.IssueLinksSystemField;
import com.atlassian.jira.issue.fields.IssueTypeSystemField;
import com.atlassian.jira.issue.fields.OrderableField;
import com.atlassian.jira.issue.fields.ProjectSystemField;
import com.atlassian.jira.issue.fields.SummarySystemField;
import com.atlassian.jira.issue.fields.rest.json.beans.FieldHtmlBean;
import com.atlassian.jira.issue.fields.screen.FieldScreenRendererFactory;
import com.atlassian.jira.issue.operation.ScreenableIssueOperation;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.I18nHelper;

import com.google.common.collect.Lists;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import webwork.action.Action;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.core.Is.is;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith (MockitoJUnitRunner.class)
public class TestFieldHtmlFactoryImpForCreateLinkedIssue {
    @Mock
    private I18nHelper.BeanFactory beanFactory;

    @Mock
    private I18nHelper i18nHelper;

    @Mock
    private FieldManager fieldManager;

    @Mock
    private FieldScreenRendererFactory fieldScreenRendererFactory;

    @Mock
    private PermissionManager permissionManager;

    @Mock
    private OperationContext operationContext;

    @Mock
    private Map<String, Object> fieldValuesHolder;

    @Before
    public void setUp() {
        when(beanFactory.getInstance(any(ApplicationUser.class))).thenReturn(i18nHelper);
        when(i18nHelper.getText(any(String.class))).thenAnswer(invocation -> invocation.getArguments()[0].toString());
        when(operationContext.getFieldValuesHolder()).thenReturn(fieldValuesHolder);

        setUpFields();
    }

    @Test
    public void testGetLinkedIssueCreateFieldsShouldReturnOnlyOneIssueLinksField() {
        testGetLinkedIssueCreateFieldsShouldReturnOnlyOneField(IssueFieldConstants.ISSUE_LINKS);
    }

    @Test
    public void testGetLinkedIssueCreateFieldsShouldReturnOnlyOneSummaryField() {
        testGetLinkedIssueCreateFieldsShouldReturnOnlyOneField(IssueFieldConstants.SUMMARY);
    }

    @Test
    public void testGetLinkedIssueCreateFieldsShouldReturnOnlyOneDescriptionField() {
        testGetLinkedIssueCreateFieldsShouldReturnOnlyOneField(IssueFieldConstants.DESCRIPTION);
    }

    @Test
    public void testGetLinkedIssueCreateFieldsShouldReturnIssueLinksField() {
        testGetLinkedIssueCreateFieldShouldReturnField(IssueFieldConstants.ISSUE_LINKS);
    }

    @Test
    public void testGetLinkedIssueCreateFieldsShouldReturnSummaryField() {
        testGetLinkedIssueCreateFieldShouldReturnField(IssueFieldConstants.SUMMARY);
    }

    @Test
    public void testGetLinkedIssueCreateFieldsShouldReturnDescriptionField() {
        testGetLinkedIssueCreateFieldShouldReturnField(IssueFieldConstants.DESCRIPTION);
    }

    @Test
    public void testRequiredFieldWithDefaultValueIsNotMarkedAsOptional() {
        final String requiredFieldId = "requiredfieldid";
        final FieldHtmlFactoryImpl.FieldRenderItemWithTab fieldRenderWithTab = createFieldRenderItemWithTab(requiredFieldId, true, "");
        final FieldHtmlFactory fieldHtmlFactory = createFieldHtmlFactoryImpl(Lists.newArrayList(fieldRenderWithTab));

        final List<FieldHtmlBean> createFields = fieldHtmlFactory.getLinkedIssueCreateFields(null, operationContext, null, mock(MutableIssue.class), mock(Issue.class), false, null);

        final List<FieldHtmlBean> requiredField = createFields.stream()
                .filter(field -> field.getId().equals(requiredFieldId))
                .collect(Collectors.toList());

        assertThat(requiredField, hasSize(1));
        assertThat(requiredField.get(0).isRequired(), is(true));
    }

    private void testGetLinkedIssueCreateFieldsShouldReturnOnlyOneField(final String fieldId) {
        final FieldHtmlFactoryImpl.FieldRenderItemWithTab fieldRenderWithTab = createFieldRenderItemWithTab(fieldId);
        final FieldHtmlFactory fieldHtmlFactory = createFieldHtmlFactoryImpl(Lists.newArrayList(fieldRenderWithTab));

        final List<FieldHtmlBean> createFields = fieldHtmlFactory.getLinkedIssueCreateFields(null, operationContext, null, mock(MutableIssue.class), mock(Issue.class), false, null);

        final int fieldCount = createFields.stream()
                .filter(field -> field.getId().equals(fieldId))
                .collect(Collectors.toList())
                .size();

        final int expectedFieldCount = 1;
        assertThat(fieldCount, equalTo(expectedFieldCount));
    }

    private void testGetLinkedIssueCreateFieldShouldReturnField(String fieldId) {
        final FieldHtmlFactory fieldHtmlFactory = createFieldHtmlFactoryImpl(Lists.newArrayList());
        final List<FieldHtmlBean> createFields = fieldHtmlFactory.getLinkedIssueCreateFields(null, operationContext, null, mock(MutableIssue.class), mock(Issue.class), false, null);

        final boolean isFieldIncluded = createFields.stream()
                .anyMatch(field -> field.getId().equals(fieldId));

        assertThat(isFieldIncluded, equalTo(true));
    }

    private FieldHtmlFactoryImpl createFieldHtmlFactoryImpl(final List<FieldHtmlFactoryImpl.FieldRenderItemWithTab> fieldRendersWithTabs) {
        return new FieldHtmlFactoryImpl(beanFactory, fieldManager, fieldScreenRendererFactory, permissionManager) {
            @Override
            List<FieldRenderItemWithTab> getRenderableItems(final Issue issue, final ScreenableIssueOperation operation) {
                return fieldRendersWithTabs;
            }
        };
    }

    private FieldHtmlFactoryImpl.FieldRenderItemWithTab createFieldRenderItemWithTab(final String fieldId) {
        return createFieldRenderItemWithTab("someHtml", fieldId, "someNameKey", false, null);
    }

    private FieldHtmlFactoryImpl.FieldRenderItemWithTab createFieldRenderItemWithTab(final String fieldId,
            final boolean isRequired, final Object defaultValue) {
        return createFieldRenderItemWithTab("someHtml", fieldId, "someNameKey", isRequired, defaultValue);
    }

    private FieldHtmlFactoryImpl.FieldRenderItemWithTab createFieldRenderItemWithTab(final String createHtml,
            final String fieldId, final String fieldNameKey, final boolean isRequired, final Object defaultValue) {
        final FieldHtmlFactoryImpl.FieldRenderItemWithTab fieldRenderWithTab = mock(FieldHtmlFactoryImpl.FieldRenderItemWithTab.class);
        final OrderableField orderableField = mock(OrderableField.class);

        when(fieldRenderWithTab.getCreateHtml(any(Action.class), any(OperationContext.class), any(Issue.class), any(Map.class))).thenReturn(createHtml);
        when(fieldRenderWithTab.getId()).thenReturn(fieldId);
        when(fieldRenderWithTab.getNameKey()).thenReturn(fieldNameKey);
        when(fieldRenderWithTab.isRequired()).thenReturn(isRequired);
        when(fieldRenderWithTab.getDefaultValue(any(Issue.class))).thenReturn(defaultValue);
        when(fieldRenderWithTab.isIssueLinkField()).thenCallRealMethod();
        when(fieldRenderWithTab.getOrderableField()).thenReturn(orderableField);

        return fieldRenderWithTab;
    }

    private void setUpFields() {
        setUpField(ProjectSystemField.class, IssueFieldConstants.PROJECT);
        setUpField(IssueTypeSystemField.class, IssueFieldConstants.ISSUE_TYPE);
        setUpField(IssueLinksSystemField.class, IssueFieldConstants.ISSUE_LINKS);
        setUpField(SummarySystemField.class, IssueFieldConstants.SUMMARY);
        setUpField(DescriptionSystemField.class, IssueFieldConstants.DESCRIPTION);
    }

    private <T extends Field> void setUpField(Class<T> fieldClass, String fieldId) {
        final Field field = mock(fieldClass);
        when(field.getId()).thenReturn(fieldId);

        when(fieldManager.getField(fieldId)).thenReturn(field);
    }
}
