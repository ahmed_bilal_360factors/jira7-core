package com.atlassian.jira.board.store;

import com.atlassian.jira.board.Board;
import com.atlassian.jira.board.BoardCreationData;
import com.atlassian.jira.board.BoardId;
import com.atlassian.jira.database.MockDbConnectionManager;
import com.atlassian.jira.database.MockQueryDslAccessor;
import com.atlassian.jira.database.ResultRow;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoContainer;
import com.atlassian.jira.mock.ofbiz.MockOfBizDelegator;
import com.atlassian.jira.model.querydsl.ProjectChangedTimeDTO;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.project.ProjectChangedTime;
import com.atlassian.jira.project.ProjectChangedTimeImpl;
import com.google.common.collect.ImmutableList;
import org.hamcrest.MatcherAssert;
import org.hamcrest.core.Is;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.sql.Timestamp;
import java.util.Date;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestQueryDslBoardStore {
    private static final Long PROJECT_ID = 1l;
    private static final Timestamp ISSUE_CHANGED_TIME = new Timestamp(new Date().getTime());
    private static final String GET_BOARD_LAST_UPDATED_TIME = "select PROJECT_CHANGED_TIME.project_id, PROJECT_CHANGED_TIME.issue_changed_time\n" +
            "from projectchangedtime PROJECT_CHANGED_TIME\n" +
            "inner join boardproject BOARD_PROJECT\n" +
            "on PROJECT_CHANGED_TIME.project_id = BOARD_PROJECT.project_id\n" +
            "where BOARD_PROJECT.board_id = 1\n" +
            "limit 1";
    private static final String JQL = "test_jql";
    private static final long ID = 1l;
    private static final String HAS_BOARD_QUERY = "select count(*)\n" +
            "from boardproject BOARD_PROJECT\n" +
            "where BOARD_PROJECT.project_id = 1";
    private static final String GET_BOARD_BY_ID_QUERY = "select Board.id, Board.jql\n"
            + "from board Board\n"
            + "where Board.id = 1\n"
            + "limit 1";

    @Rule
    public MockitoContainer mockitoContainer = new MockitoContainer(this);
    @AvailableInContainer
    private final OfBizDelegator ofBizDelegator = new MockOfBizDelegator();
    private MockQueryDslAccessor queryDslAccessor = new MockQueryDslAccessor();

    private BoardStore boardStore;

    @Before
    public void setUp() throws Exception {
        boardStore = new QueryDslBoardStore(ofBizDelegator, queryDslAccessor);
    }

    @Test
    public void testCreateBoard() throws Exception {
        Long nextSeqId = 10000l;
        String insertBoard = "insert into board (id, jql)\n"
                + "values (" + nextSeqId + ", 'test_jql')";

        String insertProject = "insert into boardproject (board_id, project_id)\n"
                + "values (" + nextSeqId + ", 1)";

        String sql = "select Board.id, Board.jql\n"
                + "from board Board\n"
                + "where Board.id = " + nextSeqId + "\n"
                + "limit 1";

        queryDslAccessor.setUpdateResults(insertBoard, 1);
        queryDslAccessor.setUpdateResults(insertProject, 1);
        queryDslAccessor.setQueryResults(sql, ImmutableList.of(new ResultRow(nextSeqId, JQL)));

        BoardCreationData boardCreationData = new BoardCreationData.Builder().jql(JQL).projectId(ID).build();

        final Board expectedBoard = new Board(new BoardId(nextSeqId), JQL);
        assertEquals(expectedBoard, boardStore.createBoard(boardCreationData));
        queryDslAccessor.assertAllExpectedStatementsWereRun();
    }

    @Test
    public void testDeleteBoard() throws Exception {
        Long boardId = 10000l;
        String deleteBoard = "delete from board\n"
                + "where board.id = " + boardId;
        String deleteProject = "delete from boardproject\n"
                + "where boardproject.board_id = " + boardId;
        queryDslAccessor.setUpdateResults(deleteBoard, 1);
        queryDslAccessor.setUpdateResults(deleteProject, 1);

        assertTrue(boardStore.deleteBoard(new BoardId(boardId)));
        queryDslAccessor.assertAllExpectedStatementsWereRun();
    }

    @Test
    public void testGetBoardById() throws Exception {
        queryDslAccessor.setQueryResults(GET_BOARD_BY_ID_QUERY, ImmutableList.of(new ResultRow(ID, JQL)));

        final Board expectedBoard = new Board(new BoardId(ID), JQL);

        assertEquals(expectedBoard, boardStore.getBoard(new BoardId(ID)).get());
        queryDslAccessor.assertAllExpectedStatementsWereRun();
    }

    @Test
    public void testGetBoardByIdReturnsEmptyOptionalWhenBoardDoesNotExist() {
        queryDslAccessor.setQueryResults(GET_BOARD_BY_ID_QUERY, ImmutableList.of());

        final Optional<Board> board = boardStore.getBoard(new BoardId(ID));

        assertFalse(board.isPresent());
        queryDslAccessor.assertAllExpectedStatementsWereRun();
    }

    @Test
    public void testGetBoardByProjectId() throws Exception {
        String sql = "select Board.id, Board.jql\n"
                + "from board Board\n"
                + "inner join boardproject BOARD_PROJECT\n"
                + "on Board.id = BOARD_PROJECT.board_id\n"
                + "where BOARD_PROJECT.project_id = 1";
        queryDslAccessor.setQueryResults(sql, ImmutableList.of(new ResultRow(ID, JQL)));

        final Board expectedBoard = new Board(new BoardId(ID), JQL);

        assertEquals(expectedBoard, boardStore.getBoardsForProject(ID).get(0));
        queryDslAccessor.assertAllExpectedStatementsWereRun();
    }

    @Test
    public void testHasBoardForProjectReturnsFalseWhenThereAreNoBoards() throws Exception {
        queryDslAccessor.setQueryResults(HAS_BOARD_QUERY, ImmutableList.of(new ResultRow(0l)));

        assertThat(boardStore.hasBoardForProject(1l), is(false));
        queryDslAccessor.assertAllExpectedStatementsWereRun();
    }

    @Test
    public void testHasBoardForProjectReturnsTrueWhenThereIsABoard() throws Exception {
        queryDslAccessor.setQueryResults(HAS_BOARD_QUERY, ImmutableList.of(new ResultRow(1l)));

        assertThat(boardStore.hasBoardForProject(1l), is(true));
        queryDslAccessor.assertAllExpectedStatementsWereRun();
    }

    @Test
    public void shouldReturnProjectUpdatedIfRecordExistsForAGivenProject() {
        queryDslAccessor.setQueryResults(GET_BOARD_LAST_UPDATED_TIME, ImmutableList.of(new ResultRow(PROJECT_ID, ISSUE_CHANGED_TIME)));
        ProjectChangedTime expectedProjectChangedTime = mock(ProjectChangedTime.class);
        when(expectedProjectChangedTime.getIssueChangedTime()).thenReturn(ISSUE_CHANGED_TIME);

        Optional<Date> actualProjectUpdated = boardStore.getBoardDataChangedTime(new BoardId(1l));

        assertThat(actualProjectUpdated.get(), is(expectedProjectChangedTime.getIssueChangedTime()));
        queryDslAccessor.assertAllExpectedStatementsWereRun();
    }

    @Test
    public void shouldReturnEmptyOptionalIfRecordDoesNotExist() {
        queryDslAccessor.setQueryResults(GET_BOARD_LAST_UPDATED_TIME, ImmutableList.of());

        Optional<Date> actualProjectUpdated = boardStore.getBoardDataChangedTime(new BoardId(1l));

        assertThat(actualProjectUpdated.isPresent(), is(false));
        queryDslAccessor.assertAllExpectedStatementsWereRun();
    }

    @Test
    public void shouldReturnEmptyOptionalIfIssueChangedTimeIsNull() {
        queryDslAccessor.setQueryResults(GET_BOARD_LAST_UPDATED_TIME, ImmutableList.of(new ResultRow(PROJECT_ID, null)));

        Optional<Date> actualProjectUpdated = boardStore.getBoardDataChangedTime(new BoardId(1l));

        assertThat(actualProjectUpdated.isPresent(), is(false));
        queryDslAccessor.assertAllExpectedStatementsWereRun();
    }
}