package com.atlassian.jira.ofbiz;

import com.atlassian.jira.mock.ofbiz.MockGenericValue;
import com.atlassian.jira.util.Consumer;
import com.atlassian.jira.util.Resolver;
import com.atlassian.jira.util.collect.EnclosedIterable;
import org.mockito.Mockito;
import org.mockito.stubbing.OngoingStubbing;
import org.ofbiz.core.entity.GenericValue;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.when;

/**
 * A base class with some common methods
 *
 * @since v3.13
 */
public abstract class AbstractDatabaseIterableTestCase {
    protected List<Long> getIdsBetweenXandYExclusive(final int start, final int endExclusive) {
        final List<Long> list = new ArrayList<Long>();
        for (int j = start; j < endExclusive; j++) {
            list.add(new Long(j));
        }
        return list;
    }

    protected <T> void assertIterableIsInThisOrder(final EnclosedIterable<T> closeableIterable, final List<T> expectedList) {
        assertEquals(expectedList.size(), closeableIterable.size());
        if (expectedList.size() == 0) {
            assertFalse(closeableIterable.isEmpty());
        }
        final Iterator<T> expected = expectedList.iterator();
        closeableIterable.foreach(new Consumer<T>() {
            public void consume(final T actual) {
                assertEquals(expected.next(), actual);
            }
        });
    }

    /**
     * This creates a OfBizListIterator that has GenericValues with and id of the expectedIdList and also this will
     * expect a close at the end of the call.
     *
     * @param expectedIdList a List of Long objects
     * @return an OfBizListIterator that iterators this list
     */
    protected OfBizListIterator getOfBizIteratorThatOrdersLikeThis(final List<Long> expectedIdList) {
        final OfBizListIterator ofBizListIterator = Mockito.mock(OfBizListIterator.class);
        OngoingStubbing<GenericValue> listOngoingStubbing = when(ofBizListIterator.next());
        for (final Long expectedId : expectedIdList) {
            final MockGenericValue gv = new MockGenericValue("MockGV", FieldMap.build("id", expectedId));

            listOngoingStubbing = listOngoingStubbing.thenReturn(gv);
        }
        // there will be one extra call to next() that is to return null
        listOngoingStubbing.thenReturn(null);

        ofBizListIterator.close();

        return ofBizListIterator;
    }

    class IdentityResolver<I> implements Resolver<I, I> {
        public I get(final I input) {
            return input;
        }
    }

    class IdGVResolver implements Resolver<GenericValue, Long> {
        public Long get(final GenericValue input) {
            return input.getLong("id");
        }
    }
}
