package com.atlassian.jira.project.util;

import com.atlassian.jira.junit.rules.MockitoContainer;
import com.atlassian.jira.mock.ofbiz.MockGenericValue;
import com.atlassian.jira.mock.ofbiz.MockOfBizDelegator;
import com.atlassian.jira.util.collect.MapBuilder;
import com.google.common.collect.Sets;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.Spy;

import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.anyMap;
import static org.mockito.Matchers.anyString;

public class TestProjectKeyStoreImpl {

    @Rule
    public MockitoContainer container = new MockitoContainer(this);

    @Spy
    protected final MockOfBizDelegator mockDelegator = new MockOfBizDelegator();
    protected ProjectKeyStore projectKeyStore;

    private static final Long TEST_PROJECT_ID = 100001L;
    private static final Long TEST_OTHER_PROJECT_ID = 100002L;
    private static final String TEST_PROJECT_KEY = "TEST";
    private static final String TEST_OTHER_PROJECT_KEY = "OTHER";

    @Before
    public void setUp() {
        projectKeyStore = new ProjectKeyStoreImpl(mockDelegator);
    }

    @Test
    public void getIdNoValue() {
        assertThat(projectKeyStore.getProjectId(TEST_PROJECT_KEY), nullValue());
    }

    @Test
    public void getIdSingleValueShouldReturnCorrectValue() {
        mockDelegator.createValue(ProjectKeyStoreImpl.ENTITY_NAME, MapBuilder.build(
                ProjectKeyStoreImpl.PROJECT_KEY, TEST_PROJECT_KEY,
                ProjectKeyStoreImpl.PROJECT_ID, TEST_PROJECT_ID));
        mockDelegator.createValue(ProjectKeyStoreImpl.ENTITY_NAME, MapBuilder.build(
                ProjectKeyStoreImpl.PROJECT_KEY, TEST_OTHER_PROJECT_KEY,
                ProjectKeyStoreImpl.PROJECT_ID, TEST_OTHER_PROJECT_ID));
        assertEquals(TEST_PROJECT_ID, projectKeyStore.getProjectId(TEST_PROJECT_KEY));
    }

    @Test(expected = Exception.class)
    public void getIdMultipleValueShouldThrowException() {
        mockDelegator.createValue(ProjectKeyStoreImpl.ENTITY_NAME, MapBuilder.build(
                ProjectKeyStoreImpl.PROJECT_KEY, TEST_PROJECT_KEY,
                ProjectKeyStoreImpl.PROJECT_ID, TEST_PROJECT_ID));
        mockDelegator.createValue(ProjectKeyStoreImpl.ENTITY_NAME, MapBuilder.build(
                ProjectKeyStoreImpl.PROJECT_KEY, TEST_PROJECT_KEY,
                ProjectKeyStoreImpl.PROJECT_ID, TEST_OTHER_PROJECT_ID));

        projectKeyStore.getProjectId(TEST_PROJECT_KEY);
    }

    @Test
    public void addProjectKey() {
        projectKeyStore.addProjectKey(TEST_PROJECT_ID, TEST_PROJECT_KEY);
        mockDelegator.verify(new MockGenericValue(ProjectKeyStoreImpl.ENTITY_NAME, MapBuilder.build(
                ProjectKeyStoreImpl.PROJECT_KEY, TEST_PROJECT_KEY,
                ProjectKeyStoreImpl.PROJECT_ID, TEST_PROJECT_ID)));
        assertEquals(1, mockDelegator.getCount(ProjectKeyStoreImpl.ENTITY_NAME));
    }

    @Test
    public void deleteProjectKeysRemoveOneValue() {
        mockDelegator.createValue(ProjectKeyStoreImpl.ENTITY_NAME, MapBuilder.build(
                ProjectKeyStoreImpl.PROJECT_KEY, TEST_PROJECT_KEY,
                ProjectKeyStoreImpl.PROJECT_ID, TEST_PROJECT_ID));
        mockDelegator.createValue(ProjectKeyStoreImpl.ENTITY_NAME, MapBuilder.build(
                ProjectKeyStoreImpl.PROJECT_KEY, TEST_OTHER_PROJECT_KEY,
                ProjectKeyStoreImpl.PROJECT_ID, TEST_OTHER_PROJECT_ID));

        projectKeyStore.deleteProjectKeys(TEST_PROJECT_ID);
        mockDelegator.verify(new MockGenericValue(ProjectKeyStoreImpl.ENTITY_NAME, MapBuilder.build(
                ProjectKeyStoreImpl.PROJECT_KEY, TEST_OTHER_PROJECT_KEY,
                ProjectKeyStoreImpl.PROJECT_ID, TEST_OTHER_PROJECT_ID)));
        assertEquals(1, mockDelegator.getCount(ProjectKeyStoreImpl.ENTITY_NAME));
    }

    @Test
    public void deleteProjectKeysRemoveMultipleValues() {
        mockDelegator.createValue(ProjectKeyStoreImpl.ENTITY_NAME, MapBuilder.build(
                ProjectKeyStoreImpl.PROJECT_KEY, TEST_PROJECT_KEY,
                ProjectKeyStoreImpl.PROJECT_ID, TEST_PROJECT_ID));
        mockDelegator.createValue(ProjectKeyStoreImpl.ENTITY_NAME, MapBuilder.build(
                ProjectKeyStoreImpl.PROJECT_KEY, TEST_OTHER_PROJECT_KEY,
                ProjectKeyStoreImpl.PROJECT_ID, TEST_PROJECT_ID));
        mockDelegator.createValue(ProjectKeyStoreImpl.ENTITY_NAME, MapBuilder.build(
                ProjectKeyStoreImpl.PROJECT_KEY, TEST_OTHER_PROJECT_KEY,
                ProjectKeyStoreImpl.PROJECT_ID, TEST_OTHER_PROJECT_ID));

        projectKeyStore.deleteProjectKeys(TEST_PROJECT_ID);
        mockDelegator.verify(new MockGenericValue(ProjectKeyStoreImpl.ENTITY_NAME, MapBuilder.build(
                ProjectKeyStoreImpl.PROJECT_KEY, TEST_OTHER_PROJECT_KEY,
                ProjectKeyStoreImpl.PROJECT_ID, TEST_OTHER_PROJECT_ID)));
        assertEquals(1, mockDelegator.getCount(ProjectKeyStoreImpl.ENTITY_NAME));

        // Make sure this is done in only 1 db call.
        Mockito.verify(mockDelegator).removeByAnd(anyString(), anyMap());
    }

    @Test
    public void getAllProjectKeys() {
        mockDelegator.createValue(ProjectKeyStoreImpl.ENTITY_NAME, MapBuilder.build(
                ProjectKeyStoreImpl.PROJECT_KEY, TEST_PROJECT_KEY,
                ProjectKeyStoreImpl.PROJECT_ID, TEST_PROJECT_ID));
        mockDelegator.createValue(ProjectKeyStoreImpl.ENTITY_NAME, MapBuilder.build(
                ProjectKeyStoreImpl.PROJECT_KEY, TEST_OTHER_PROJECT_KEY,
                ProjectKeyStoreImpl.PROJECT_ID, TEST_OTHER_PROJECT_ID));

        assertEquals(MapBuilder.build(TEST_PROJECT_KEY, TEST_PROJECT_ID, TEST_OTHER_PROJECT_KEY, TEST_OTHER_PROJECT_ID),
                projectKeyStore.getAllProjectKeys());
    }

    @Test
    public void getProjectIdByKeyIgnoreCase() {
        mockDelegator.createValue(ProjectKeyStoreImpl.ENTITY_NAME, MapBuilder.build(
                ProjectKeyStoreImpl.PROJECT_KEY, TEST_PROJECT_KEY,
                ProjectKeyStoreImpl.PROJECT_ID, TEST_PROJECT_ID));
        mockDelegator.createValue(ProjectKeyStoreImpl.ENTITY_NAME, MapBuilder.build(
                ProjectKeyStoreImpl.PROJECT_KEY, TEST_OTHER_PROJECT_KEY,
                ProjectKeyStoreImpl.PROJECT_ID, TEST_OTHER_PROJECT_ID));
        assertEquals(TEST_PROJECT_ID, projectKeyStore.getProjectIdByKeyIgnoreCase(TEST_PROJECT_KEY.toLowerCase()));
    }

    @Test
    public void getProjectKeys() {
        mockDelegator.createValue(ProjectKeyStoreImpl.ENTITY_NAME, MapBuilder.build(
                ProjectKeyStoreImpl.PROJECT_KEY, TEST_PROJECT_KEY,
                ProjectKeyStoreImpl.PROJECT_ID, TEST_PROJECT_ID));
        mockDelegator.createValue(ProjectKeyStoreImpl.ENTITY_NAME, MapBuilder.build(
                ProjectKeyStoreImpl.PROJECT_KEY, TEST_OTHER_PROJECT_KEY,
                ProjectKeyStoreImpl.PROJECT_ID, TEST_PROJECT_ID));
        mockDelegator.createValue(ProjectKeyStoreImpl.ENTITY_NAME, MapBuilder.build(
                ProjectKeyStoreImpl.PROJECT_KEY, TEST_OTHER_PROJECT_KEY,
                ProjectKeyStoreImpl.PROJECT_ID, TEST_OTHER_PROJECT_ID));

        assertEquals(Sets.newHashSet(TEST_PROJECT_KEY, TEST_OTHER_PROJECT_KEY),projectKeyStore.getProjectKeys(TEST_PROJECT_ID));
    }
}
