package com.atlassian.jira.bc.project.version;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.mock.MockPermissionManager;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.security.plugin.ProjectPermissionKey;
import com.atlassian.jira.user.ApplicationUser;

import javax.annotation.Nonnull;

/**
 * A MockPermissionManager where all the hasPermission methods have been overriden to return the default permission
 * setting.
 * <p>
 * TODO - merge with MockPermissionManager?
 *
 * @since v3.13
 */
final class MyPermissionManager extends MockPermissionManager {
    static MockPermissionManager createPermissionManager(final boolean defaultPermission) {
        MockPermissionManager permissionManager = new MyPermissionManager();
        permissionManager.setDefaultPermission(defaultPermission);
        return permissionManager;
    }

    public boolean hasPermission(final int permissionsId, final ApplicationUser user) {
        return isDefaultPermission();
    }

    public boolean hasPermission(final int permissionsId, final Issue issue, final ApplicationUser u) {
        return isDefaultPermission();
    }

    public boolean hasPermission(final int permissionsId, final Project project, final ApplicationUser user) {
        return isDefaultPermission();
    }

    public boolean hasPermission(final int permissionsId, final Project project, final ApplicationUser user, final boolean issueCreation) {
        return isDefaultPermission();
    }

    public boolean hasPermission(@Nonnull final ProjectPermissionKey permissionKey, @Nonnull final Project project, final ApplicationUser user) {
        return isDefaultPermission();
    }
}
