package com.atlassian.jira.jql.operand.registry;

import com.atlassian.jira.jql.operand.FunctionOperandHandler;
import com.atlassian.jira.mock.plugin.MockPlugin;
import com.atlassian.jira.plugin.jql.function.JqlFunction;
import com.atlassian.jira.plugin.jql.function.JqlFunctionModuleDescriptor;
import com.atlassian.jira.plugin.util.ModuleDescriptors;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.web.bean.MockI18nBean;
import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.google.common.collect.Ordering;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Map;

import static com.atlassian.jira.mock.plugin.jql.operand.MockJqlFunctionModuleDescriptor.create;
import static com.google.common.collect.ImmutableList.of;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestLazyResettableJqlFunctionHandlerRegistry {
    @Rule
    public MockitoRule mocks = MockitoJUnit.rule();

    @Mock
    private PluginAccessor mockPluginAccessor;
    @Mock
    private ModuleDescriptors.Orderings mockModuleDescriptorOrderings;

    private LazyResettableJqlFunctionHandlerRegistry lazyResettableOperandRegistry;

    @Mock
    I18nHelper i18nHelper;


    /**
     * Tests that a plugin provided function will <em>not</em> override a system provided function if they have the same
     * name. We only load the system function.
     * <p>
     * The origin of the function is determined by the implementation of {@link com.atlassian.jira.plugin.util.ModuleDescriptors.Orderings#byOrigin()}
     */
    @Test
    public void pluginJqlFunctionsDoNotOverrideASystemFunctionWithTheSameName() {
        final String duplicateFunctionName = "echo";
        final JqlFunction systemJqlFunction = mock(JqlFunction.class);
        when(systemJqlFunction.getFunctionName()).thenReturn(duplicateFunctionName);
        final JqlFunctionModuleDescriptor systemJqlFunctionDescriptor =
                create(duplicateFunctionName, false, new MockI18nBean(), systemJqlFunction,
                        buildMockSystemPlugin());

        final JqlFunction userInstalledJqlFunction = mock(JqlFunction.class);
        when(userInstalledJqlFunction.getFunctionName()).thenReturn(duplicateFunctionName);
        final JqlFunctionModuleDescriptor userInstalledJqlFunctionDescriptor =
                create(duplicateFunctionName, false, new MockI18nBean(), userInstalledJqlFunction,
                        buildMockUserPlugin());

        when(mockPluginAccessor.getEnabledModuleDescriptorsByClass(JqlFunctionModuleDescriptor.class)).thenReturn(of(systemJqlFunctionDescriptor, userInstalledJqlFunctionDescriptor));

        when(mockModuleDescriptorOrderings.byOrigin()).thenReturn(buildStubLastParameterGreaterThanSecondOrdering(systemJqlFunctionDescriptor, userInstalledJqlFunctionDescriptor));

        when(mockModuleDescriptorOrderings.natural()).thenReturn(buildStubAlwaysEqualOrdering());

        lazyResettableOperandRegistry =
                new LazyResettableJqlFunctionHandlerRegistry(mockPluginAccessor, mockModuleDescriptorOrderings, i18nHelper);

        final Map<String, FunctionOperandHandler> functionOperandHandlerMap =
                lazyResettableOperandRegistry.loadFromJqlFunctionModuleDescriptors();

        assertThat(functionOperandHandlerMap.get(duplicateFunctionName).getJqlFunction(), equalTo(systemJqlFunction));
        assertThat(functionOperandHandlerMap.get(duplicateFunctionName).getJqlFunction(), not(equalTo(userInstalledJqlFunction)));
    }

    private MockPlugin buildMockSystemPlugin() {
        return new MockPlugin("System Plugin", "system-plugin", null);
    }

    private MockPlugin buildMockUserPlugin() {
        return new MockPlugin("User Plugin", "user-plugin", null);
    }

    /**
     * A dummy ordering that considers all objects to be equal.
     *
     * @return Always returns zero.
     */
    private Ordering<ModuleDescriptor> buildStubAlwaysEqualOrdering() {
        return new Ordering<ModuleDescriptor>() {
            @Override
            public int compare(final ModuleDescriptor o1, final ModuleDescriptor o2) {
                return 0;
            }
        };
    }

    /**
     * Tests that functions of the same &quot;origin&quot; (plugin provided or system provided) do not override each
     * other.
     * <p>
     * We only load one of them, according to &quot;natural ordering&quot; natural ordering is provided by {@link
     * com.atlassian.jira.plugin.util.ModuleDescriptors.Orderings#natural()}
     */
    @Test
    public void jqlFunctionsWithTheSameNameAndTheSameOriginShouldNotOverrideEachOtherAccordingToNaturalOrdering() {
        final String duplicateFunctionName = "echo";
        final JqlFunction pluginAbcJqlFunction = mock(JqlFunction.class);
        when(pluginAbcJqlFunction.getFunctionName()).thenReturn(duplicateFunctionName);
        final JqlFunctionModuleDescriptor pluginAbcJqlFunctionModuleDescriptor =
                create(duplicateFunctionName, false, new MockI18nBean(), pluginAbcJqlFunction,
                        buildAbcMockPlugin());

        final JqlFunction pluginXyzJqlFunction = mock(JqlFunction.class);
        when(pluginXyzJqlFunction.getFunctionName()).thenReturn(duplicateFunctionName);
        final JqlFunctionModuleDescriptor pluginXyzJqlFunctionModuleDescriptor =
                create(duplicateFunctionName, false, new MockI18nBean(), pluginXyzJqlFunction,
                        buildXyzMockPlugin());

        when(mockPluginAccessor.getEnabledModuleDescriptorsByClass(JqlFunctionModuleDescriptor.class)).thenReturn(of(pluginAbcJqlFunctionModuleDescriptor, pluginXyzJqlFunctionModuleDescriptor));

        when(mockModuleDescriptorOrderings.byOrigin()).thenReturn(buildStubAlwaysEqualOrdering());

        when(mockModuleDescriptorOrderings.natural()).thenReturn(buildStubLastParameterGreaterThanSecondOrdering
                (
                        pluginAbcJqlFunctionModuleDescriptor, pluginXyzJqlFunctionModuleDescriptor
                ));

        lazyResettableOperandRegistry =
                new LazyResettableJqlFunctionHandlerRegistry(mockPluginAccessor, mockModuleDescriptorOrderings, i18nHelper);

        final Map<String, FunctionOperandHandler> functionOperandHandlerMap =
                lazyResettableOperandRegistry.loadFromJqlFunctionModuleDescriptors();

        assertThat(functionOperandHandlerMap.get(duplicateFunctionName).getJqlFunction(), equalTo(pluginAbcJqlFunction));
        assertThat(functionOperandHandlerMap.get(duplicateFunctionName).getJqlFunction(), not(equalTo(pluginXyzJqlFunction)));
    }

    private Ordering<ModuleDescriptor> buildStubLastParameterGreaterThanSecondOrdering
            (final JqlFunctionModuleDescriptor firstFunctionDescriptor, final JqlFunctionModuleDescriptor secondFunctionDescriptor) {
        return new Ordering<ModuleDescriptor>() {
            @Override
            public int compare(final ModuleDescriptor o1, final ModuleDescriptor o2) {
                if (o1 == firstFunctionDescriptor || o2 == secondFunctionDescriptor) {
                    return -1;
                } else if (o1 == secondFunctionDescriptor || o2 == firstFunctionDescriptor) {
                    return 1;
                } else {
                    throw new RuntimeException("Unexpected descriptor.");
                }
            }
        };
    }

    private Plugin buildXyzMockPlugin() {
        return new MockPlugin("The Xyz User Plugin", "user-xyz-plugin", null);
    }

    private Plugin buildAbcMockPlugin() {
        return new MockPlugin("The Abc User Plugin", "user-abc-plugin", null);
    }
}
