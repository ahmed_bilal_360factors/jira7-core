package com.atlassian.jira.upgrade;

import com.atlassian.jira.util.BuildUtilsInfo;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.mockito.Mockito.when;

/**
 * @since v4.1
 */
public class TestDefaultBuildVersionRegistry {
    @Rule
    public final MockitoRule initMockito = MockitoJUnit.rule();
    @Mock
    private BuildUtilsInfo buildUtilsInfo;
    private static final int BUILD_NUMBER = 88888888;
    private static final String LATEST_VERSION = "LATEST VERSION";

    @Test
    public void testLookups() throws Exception {
        when(buildUtilsInfo.getApplicationBuildNumber()).thenReturn(BUILD_NUMBER);
        when(buildUtilsInfo.getVersion()).thenReturn(LATEST_VERSION);

        final DefaultBuildVersionRegistry registry = new DefaultBuildVersionRegistry(buildUtilsInfo);

        assertEquals(new BuildVersionImpl(BUILD_NUMBER, LATEST_VERSION), registry.getVersionForBuildNumber(99999999));
        assertEquals(new BuildVersionImpl(466, "4.0"), registry.getVersionForBuildNumber(466));
        assertEquals(new BuildVersionImpl(466, "4.0"), registry.getVersionForBuildNumber(465));
        assertEquals(new BuildVersionImpl(22, "1.4.3"), registry.getVersionForBuildNumber(1));

        assertEquals(new BuildVersionImpl(466, "4.0"), registry.getVersionForBuildNumber(466));
        assertEquals(new BuildVersionImpl(466, "4.0"), registry.getVersionForBuildNumber(465));
        assertEquals(new BuildVersionImpl(22, "1.4.3"), registry.getVersionForBuildNumber(1));
    }

    @Test
    public void testBuildVersionEquality() {
        assertEquals(new BuildVersionImpl(466, "4.0"), new BuildVersionImpl(466, "4.0"));
        assertNotEquals(new BuildVersionImpl(466, "4.0"), new BuildVersionImpl(467, "4.0"));
        assertNotEquals(new BuildVersionImpl(466, "4.0"), new BuildVersionImpl(466, "4.1"));
    }
}
