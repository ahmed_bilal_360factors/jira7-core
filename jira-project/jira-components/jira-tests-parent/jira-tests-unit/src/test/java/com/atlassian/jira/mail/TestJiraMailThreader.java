package com.atlassian.jira.mail;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.mock.component.MockComponentWorker;
import com.atlassian.jira.mock.issue.MockIssue;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.startsWith;
import static org.junit.Assert.assertThat;

public class TestJiraMailThreader {
    private JiraMailThreader jiraMailThreader;

    @Before
    public void setUp() {
        new MockComponentWorker().init();
        Issue issue = new MockIssue(10012L, 1347264545909L);
        jiraMailThreader = new JiraMailThreader(issue);
    }

    @Test
    public void issueIdTimestampAndSequenceIdAreCorrectInMessageId() throws Exception {
        String messageId = jiraMailThreader.getCustomMessageId(null);
        assertThat(messageId, startsWith("JIRA.10012.1347264545000.1."));

        String nextMessageId = jiraMailThreader.getCustomMessageId(null);
        assertThat("sequence id should be incremented", nextMessageId, startsWith("JIRA.10012.1347264545000.2"));
    }
}
