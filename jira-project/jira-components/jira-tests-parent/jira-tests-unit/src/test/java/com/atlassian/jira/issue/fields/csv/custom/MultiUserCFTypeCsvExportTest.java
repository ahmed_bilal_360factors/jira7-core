package com.atlassian.jira.issue.fields.csv.custom;

import com.atlassian.jira.issue.customfields.CustomFieldType;
import com.atlassian.jira.issue.customfields.impl.MultiUserCFType;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.google.common.collect.ImmutableList;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collection;
import java.util.Locale;

import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class MultiUserCFTypeCsvExportTest extends CustomFieldMultiValueCsvExporterTest<Collection<ApplicationUser>> {
    @Mock
    private JiraAuthenticationContext jiraAuthenticationContext;

    @Override
    protected CustomFieldType<Collection<ApplicationUser>, ?> createField() {
        when(jiraAuthenticationContext.getLocale()).thenReturn(Locale.ENGLISH);
        return new MultiUserCFType(null, null, null, null, jiraAuthenticationContext, null, null, null, null);
    }

    @Test
    public void allUsersAreExportedAsSeparateValues() {
        whenFieldValueIs(ImmutableList.<ApplicationUser>of(new MockApplicationUser("geralt"), new MockApplicationUser("p?otka")));
        assertExportedValue("geralt", "p?otka");
    }


    @Test
    public void allUsersAreExportedAsSortedValues() {
        whenFieldValueIs(ImmutableList.<ApplicationUser>of(new MockApplicationUser("b"), new MockApplicationUser("a")));
        assertExportedValue("a", "b");
    }
}
