package com.atlassian.jira.bc.group;

import com.atlassian.crowd.manager.directory.DirectoryManager;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.stubbing.Answer;

import java.util.Optional;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

public class GroupRelationshipCheckerTest {
    @Rule
    public final MethodRule initMockito = MockitoJUnit.rule();

    @Mock
    private DirectoryManager directoryManager;

    @InjectMocks
    private GroupRelationshipChecker checker;

    @Test
    public void shouldCheckStrictEqualityDisregardingDirectory() {
        assertTrue(checker.isGroupEqualOrNested(Optional.<Long>empty(), "tigers", "tigers"));
        assertTrue(checker.isGroupEqualOrNested(Optional.of(123L), "tigers", "tigers"));
    }

    @Test
    public void shouldCheckRelationshipWithCaseInsensitiveManner() {
        assertTrue(checker.isGroupEqualOrNested(Optional.empty(), "tigers", "TIGERS"));
        assertTrue(checker.isGroupEqualOrNested(Optional.of(123L), "tigers", "TIGERS"));
    }

    @Test
    public void shouldCheckPotentialNestingWhenDirectoryIdGiven() throws Exception {
        when(directoryManager.isGroupNestedGroupMember(123L, "tigers", "animals")).thenReturn(true);

        assertFalse(checker.isGroupEqualOrNested(Optional.<Long>empty(), "tigers", "animals"));
        assertTrue(checker.isGroupEqualOrNested(Optional.of(123L), "tigers", "animals"));
        assertFalse(
                "relationship is not equivalence relation",
                checker.isGroupEqualOrNested(Optional.of(123L), "animals", "tigers")
        );
    }

    public static Answer<Boolean> eqalityMockitoAnswer() {
        return invocation -> invocation.getArguments()[1].equals(invocation.getArguments()[2]);
    }

}