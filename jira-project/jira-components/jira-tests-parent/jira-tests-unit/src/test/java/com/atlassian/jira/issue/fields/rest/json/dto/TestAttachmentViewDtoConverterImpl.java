package com.atlassian.jira.issue.fields.rest.json.dto;

import com.atlassian.core.util.FileSize;
import com.atlassian.core.util.thumbnail.Thumbnail;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.datetime.DateTimeFormatter;
import com.atlassian.jira.datetime.DateTimeStyle;
import com.atlassian.jira.issue.AttachmentIndexManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.attachment.Attachment;
import com.atlassian.jira.issue.thumbnail.ThumbnailManager;
import com.atlassian.jira.issue.thumbnail.ThumbnailedImage;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.timezone.TimeZoneManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.google.common.collect.ImmutableList;
import org.joda.time.DateTime;
import org.joda.time.ReadableInstant;
import org.joda.time.format.DateTimeFormat;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.mockito.Answers;
import org.mockito.Mock;

import java.net.URI;
import java.sql.Timestamp;
import java.util.List;
import java.util.TimeZone;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.collection.IsIterableContainingInOrder.contains;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestAttachmentViewDtoConverterImpl {
    private static final TimeZone TIME_ZONE = TimeZone.getDefault();
    private static final Timestamp CREATION = new Timestamp(4523612L);
    private static final long FILE_SIZE = 1024l;
    private static final String AUTHOR_KEY = "authorkey";
    private static final long ISSUE_ID = 1l;
    private static final URI THUMBNAILURL = URI.create("dummy/thumbnail/image.png");
    private static final int THUMBNAIL_WIDTH = 128;
    private static final int THUMBNAIL_HEIGHT = 64;
    public static final String DISPLAY_NAME = "authordisplayname";

    @Rule
    public final RuleChain chain = MockitoMocksInContainer.forTest(this);

    @Mock
    private ThumbnailManager thumbnailManager;
    @Mock
    private JiraAuthenticationContext authenticationContext;
    @Mock
    private AttachmentIndexManager attachmentIndexManager;
    @Mock
    private IssueManager issueManager;
    @Mock
    private PermissionManager permissionManager;
    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private DateTimeFormatter dateTimeFormatter;
    @Mock
    private UserManager userManager;


    @Mock
    @AvailableInContainer
    private ApplicationProperties applicationProperties;
    @Mock
    @AvailableInContainer
    private TimeZoneManager timeZoneManager;

    private AttachmentViewDtoConverterImpl attachmentViewDtoConverter;

    @Mock
    private ApplicationUser author;
    @Mock
    private Issue issue;
    @Mock
    private ApplicationUser loggedInUser;
    @Mock
    private DateTimeFormatter userDateTimeFormatter;

    @Before
    public void setUp() throws Exception {
        when(applicationProperties.getEncoding()).thenReturn("UTF-8");

        attachmentViewDtoConverter = new AttachmentViewDtoConverterImpl(
                thumbnailManager,
                authenticationContext,
                applicationProperties,
                attachmentIndexManager,
                issueManager,
                permissionManager,
                dateTimeFormatter,
                userManager);

        when(author.getDisplayName()).thenReturn(DISPLAY_NAME);
        when(author.getKey()).thenReturn(AUTHOR_KEY);


        when(issue.getId()).thenReturn(ISSUE_ID);

        stubUser(null);
    }

    @Test
    public void shouldConvertNoAttachments() {
        final List<Attachment> attachments = ImmutableList.<Attachment>of();

        final List<AttachmentViewJsonDto> convertedList = attachmentViewDtoConverter.convert(attachments);

        assertThat("Empty attachment list convertedList to empty collection", convertedList, hasSize(0));
    }

    @Test
    public void shouldConvertFile() {
        final Attachment attachment = mockAttachment(1l, "file.txt", CREATION);

        final ImmutableList<Attachment> attachments = ImmutableList.<Attachment>of(attachment);
        final List<AttachmentViewJsonDto> convertedList = attachmentViewDtoConverter.convert(attachments);

        final AttachmentViewJsonDto expected = newExpectedDto(attachment, false, true, false, false, CREATION);
        assertThat(convertedList, contains(expected));
    }

    @Test
    public void shouldConvertFileAndImage() {
        final Attachment fileAttachment = mockAttachment(1l, "file.txt", CREATION);
        final Attachment imageAttachment = mockAttachment(2l, "file.png", CREATION);
        stubThumbnail(imageAttachment);
        final ImmutableList<Attachment> attachments = ImmutableList.<Attachment>of(fileAttachment, imageAttachment);

        final List<AttachmentViewJsonDto> convertedList = attachmentViewDtoConverter.convert(attachments);

        final AttachmentViewJsonDto expectedFileDto = newExpectedDto(fileAttachment, false, true, false, false, CREATION);
        final AttachmentViewJsonDto expectedImageDto = newExpectedDto(imageAttachment, false, true, false, true, CREATION);
        assertThat(convertedList, contains(expectedFileDto, expectedImageDto));
    }

    @Test
    public void shouldRespectAdminPermissions() {
        stubUser(loggedInUser);
        when(issueManager.isEditable(issue)).thenReturn(true);
        when(permissionManager.hasPermission(Permissions.ATTACHMENT_DELETE_ALL, issue, loggedInUser)).thenReturn(true);
        final Attachment attachment = mockAttachment(1l, "file.txt", CREATION);
        final ImmutableList<Attachment> attachments = ImmutableList.<Attachment>of(attachment);

        final List<AttachmentViewJsonDto> convertedList = attachmentViewDtoConverter.convert(attachments);

        final AttachmentViewJsonDto expected = newExpectedDto(attachment, true, true, false, false, CREATION);
        assertThat(convertedList, contains(expected));
    }

    @Test
    public void shouldRespectUserPermissions() {
        stubUser(loggedInUser);
        when(issueManager.isEditable(issue)).thenReturn(true);
        when(permissionManager.hasPermission(Permissions.ATTACHMENT_DELETE_OWN, issue, loggedInUser)).thenReturn(true);
        final Attachment attachment = mockAttachment(1l, "file.txt", CREATION);
        final ImmutableList<Attachment> attachments = ImmutableList.<Attachment>of(attachment);

        final List<AttachmentViewJsonDto> convertedList = attachmentViewDtoConverter.convert(attachments);

        final AttachmentViewJsonDto expected = newExpectedDto(attachment, false, true, false, false, CREATION);
        assertThat(convertedList, contains(expected));
    }

    @Test
    public void shouldDetectTheLatestVersion() {
        final Attachment olderAttachment = mockAttachment(1l, "file.txt", CREATION);
        final Timestamp laterCreation = new Timestamp(CREATION.getTime() + 23523);
        final Attachment newerAttachment = mockAttachment(2l, "file.txt", laterCreation);
        final ImmutableList<Attachment> attachments = ImmutableList.<Attachment>of(olderAttachment, newerAttachment);

        final List<AttachmentViewJsonDto> convertedList = attachmentViewDtoConverter.convert(attachments);

        final AttachmentViewJsonDto expectedOlderDto = newExpectedDto(olderAttachment, false, false, false, false, CREATION);
        final AttachmentViewJsonDto expectedNewerDto = newExpectedDto(newerAttachment, false, true, false, false, laterCreation);
        assertThat(convertedList, contains(expectedOlderDto, expectedNewerDto));
    }

    @Test
    public void shouldConvertExpandableFile() {
        final Attachment attachment = mockAttachment(1l, "file.txt", CREATION);
        when(applicationProperties.getOption(APKeys.JIRA_OPTION_ALLOW_ZIP_SUPPORT)).thenReturn(true);
        when(attachmentIndexManager.isExpandable(attachment)).thenReturn(true);

        final ImmutableList<Attachment> attachments = ImmutableList.<Attachment>of(attachment);
        final List<AttachmentViewJsonDto> convertedList = attachmentViewDtoConverter.convert(attachments);

        final AttachmentViewJsonDto expected = newExpectedDto(attachment, false, true, true, false, CREATION);
        assertThat(convertedList, contains(expected));
    }

    @Test
    public void shouldRespectZipSupport() {
        final Attachment attachment = mockAttachment(1l, "file.txt", CREATION);
        when(applicationProperties.getOption(APKeys.JIRA_OPTION_ALLOW_ZIP_SUPPORT)).thenReturn(false);
        when(attachmentIndexManager.isExpandable(attachment)).thenReturn(true);

        final ImmutableList<Attachment> attachments = ImmutableList.<Attachment>of(attachment);
        final List<AttachmentViewJsonDto> convertedList = attachmentViewDtoConverter.convert(attachments);

        final AttachmentViewJsonDto expected = newExpectedDto(attachment, false, true, false, false, CREATION);
        assertThat(convertedList, contains(expected));
    }

    @Test
    public void shouldHandleEmptyAuthor() {
        final Attachment attachment = mockAttachment(1l, "file.txt", CREATION);
        when(attachment.getAuthorObject()).thenReturn(null);
        when(userManager.getUserByKeyEvenWhenUnknown(attachment.getAuthorKey())).thenReturn(null);

        final ImmutableList<Attachment> attachments = ImmutableList.<Attachment>of(attachment);
        final List<AttachmentViewJsonDto> convertedList = attachmentViewDtoConverter.convert(attachments);

        final AttachmentViewJsonDto expected = newExpectedDto(attachment, false, true, false, false, CREATION);
        assertThat(convertedList, contains(expected));
    }

    private void stubThumbnail(final Attachment attachment) {
        final Thumbnail thumbnail = mock(Thumbnail.class);
        when(thumbnailManager.getThumbnail(issue, attachment)).thenReturn(thumbnail);

        final ThumbnailedImage thumbnailedImage = mock(ThumbnailedImage.class);
        when(thumbnailedImage.getImageURL()).thenReturn(THUMBNAILURL.toString());
        when(thumbnailedImage.getWidth()).thenReturn(THUMBNAIL_WIDTH);
        when(thumbnailedImage.getHeight()).thenReturn(THUMBNAIL_HEIGHT);
        when(thumbnailManager.toThumbnailedImage(thumbnail)).thenReturn(thumbnailedImage);
    }

    private void stubUser(final ApplicationUser user) {
        final ApplicationUser directoryUser = user == null ? null : mock(ApplicationUser.class);

        when(authenticationContext.getUser()).thenReturn(user);
        when(authenticationContext.getLoggedInUser()).thenReturn(directoryUser);

        when(dateTimeFormatter
                .forUser(directoryUser)
                .withStyle(DateTimeStyle.COMPLETE))
                .thenReturn(userDateTimeFormatter);

        when(userDateTimeFormatter.getZone()).thenReturn(TIME_ZONE);
    }

    private AttachmentViewJsonDto newExpectedDto(
            final Attachment attachment,
            final boolean deletable,
            final boolean latest,
            final boolean expandable,
            final boolean thumbnailed,
            final Timestamp creation) {
        final ReadableInstant creationInstant = toInstant(creation);
        return new AttachmentViewJsonDto(
                attachment.getId(),
                latest,
                deletable,
                expandable,
                thumbnailed ? THUMBNAILURL : null,
                thumbnailed ? THUMBNAIL_WIDTH : 0,
                thumbnailed ? THUMBNAIL_HEIGHT : 0,
                URI.create(String.format("secure/attachment/%d/%s", attachment.getId(), attachment.getFilename())),
                attachment.getAuthorObject() == null ? null : DISPLAY_NAME,
                AUTHOR_KEY,
                FileSize.format(FILE_SIZE),
                attachment.getFilename(),
                attachment.getMimetype(),
                creationInstant,
                customFormat(creationInstant)
        );
    }

    private DateTime toInstant(final Timestamp creation) {
        return new DateTime(creation.getTime());
    }

    private Attachment mockAttachment(
            final Long attachmentId,
            final String filename,
            final Timestamp creation) {
        final Long issueId = issue.getId();
        final String authorKey = author.getKey();

        final Attachment attachment = mock(Attachment.class);

        when(attachment.getId()).thenReturn(attachmentId);
        when(attachment.getFilename()).thenReturn(filename);
        when(attachment.getIssueId()).thenReturn(issueId);
        when(attachment.getFilesize()).thenReturn(FILE_SIZE);
        when(attachment.getIssueObject()).thenReturn(issue);
        when(attachment.getIssue()).thenReturn(issue);
        when(attachment.getAuthorObject()).thenReturn(author);
        when(attachment.getAuthorKey()).thenReturn(authorKey);
        when(attachment.getAuthor()).thenReturn(authorKey);
        when(attachment.getCreated()).thenReturn(creation);

        when(thumbnailManager.getThumbnail(issue, attachment)).thenReturn(null);
        when(userDateTimeFormatter.format(attachment.getCreated())).thenReturn(customFormat(toInstant(creation)));
        when(userManager.getUserByKeyEvenWhenUnknown(authorKey)).thenReturn(author);

        return attachment;
    }

    private String customFormat(final ReadableInstant instant) {
        return DateTimeFormat.mediumDateTime().print(instant);
    }
}
