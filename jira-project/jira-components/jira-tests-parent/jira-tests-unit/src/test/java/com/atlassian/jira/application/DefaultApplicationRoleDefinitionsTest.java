package com.atlassian.jira.application;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.application.host.plugin.MockPluginApplicationMetaDataManager;
import com.atlassian.application.host.plugin.PluginApplicationMetaData;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.junit.rules.InitMockitoMocks;
import com.atlassian.jira.license.JiraLicenseManager;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.hamcrest.TypeSafeDiagnosingMatcher;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.mockito.Mock;

import java.util.List;

import static com.atlassian.jira.application.ApplicationRoleDefinitions.ApplicationRoleDefinition;
import static com.atlassian.jira.matchers.OptionMatchers.none;
import static com.atlassian.jira.matchers.OptionMatchers.some;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

public class DefaultApplicationRoleDefinitionsTest {
    public static final ApplicationKey UNDEFINED_KEY = ApplicationKey.valueOf("random");

    @Rule
    public TestRule rule = new InitMockitoMocks(this);
    @Mock
    private JiraLicenseManager jiraLicenseManager;
    @Mock
    private EventPublisher publisher;

    private DefaultApplicationRoleDefinitions manager;
    private MockPluginApplicationMetaData app1;
    private MockPluginApplicationMetaData app2;
    private MockPluginApplicationMetaData app3;
    private ApplicationKey applicationKey1 = ApplicationKey.valueOf("app-one");
    private ApplicationKey applicationKey2 = ApplicationKey.valueOf("app-two");
    private ApplicationKey applicationKey3 = ApplicationKey.valueOf("app-three");

    @Before
    public void before() {
        final MockPluginApplicationMetaDataManager metaDataManager = new MockPluginApplicationMetaDataManager();

        app1 = MockPluginApplicationMetaData.valueOf(applicationKey1).name("one");
        app2 = MockPluginApplicationMetaData.valueOf(applicationKey2).name("two");
        app3 = MockPluginApplicationMetaData.valueOf(applicationKey3).name("three");

        metaDataManager.addMetaData(app1);
        metaDataManager.addMetaData(app2);
        metaDataManager.addMetaData(app3);

        manager = new DefaultApplicationRoleDefinitions(jiraLicenseManager, metaDataManager);

        when(jiraLicenseManager.getAllLicensedApplicationKeys()).thenReturn(Sets.newHashSet(applicationKey1, applicationKey2));
    }

    @Test
    public void getDefinedReturnsDefinedApplications() {
        assertThat(manager.getDefined(), contains(true, app1, app2, app3));
    }

    @Test
    public void isDefinedReturnsCorrectly() {
        assertThat(manager.isDefined(UNDEFINED_KEY), equalTo(false));
        assertThat(manager.isDefined(app1.getKey()), equalTo(true));
        assertThat(manager.isDefined(app2.getKey()), equalTo(true));
        assertThat(manager.isDefined(app3.getKey()), equalTo(true));
        assertThat(manager.isDefined(ApplicationKeys.CORE), equalTo(true));
    }

    @Test
    public void getDefinedReturnsCorrectly() {
        assertThat(manager.getDefined(UNDEFINED_KEY), none());
        assertThat(manager.getDefined(app1.getKey()), some(metaMatcher(app1)));
        assertThat(manager.getDefined(app2.getKey()), some(metaMatcher(app2)));
        assertThat(manager.getDefined(app3.getKey()), some(metaMatcher(app3)));
        assertThat(manager.getDefined(ApplicationKeys.CORE), some(platformMatcher()));
    }

    @Test
    public void getLicensedReturnsLicensedRoles() {
        assertThat(manager.getLicensed(), contains(false, app1, app2));
    }

    @Test
    public void isLicensedReturnsCorrectly() {
        assertThat(manager.isLicensed(UNDEFINED_KEY), equalTo(false));
        assertThat(manager.isLicensed(app1.getKey()), equalTo(true));
        assertThat(manager.isLicensed(app2.getKey()), equalTo(true));
        assertThat(manager.isLicensed(app3.getKey()), equalTo(false));
        assertThat(manager.isLicensed(ApplicationKeys.CORE), equalTo(false));
    }

    @Test
    public void getIfLicensedReturnsCorrectly() {
        assertThat(manager.getLicensed(UNDEFINED_KEY), none());
        assertThat(manager.getLicensed(app1.getKey()), some(metaMatcher(app1)));
        assertThat(manager.getLicensed(app2.getKey()), some(metaMatcher(app2)));
        assertThat(manager.getLicensed(app3.getKey()), none());
        assertThat(manager.getLicensed(ApplicationKeys.CORE), none());
    }

    private static Matcher<Iterable<? extends ApplicationRoleDefinition>> contains(boolean platform, PluginApplicationMetaData... data) {
        List<Matcher<? super ApplicationRoleDefinition>> matchers = Lists.newArrayList();
        if (platform) {
            matchers.add(platformMatcher());
        }

        for (PluginApplicationMetaData metaData : data) {
            matchers.add(metaMatcher(metaData));
        }

        return Matchers.containsInAnyOrder(matchers);
    }

    private static Matcher<ApplicationRoleDefinition> metaMatcher(PluginApplicationMetaData data) {
        return new ApplicationDefinitionMatcher(data.getKey(), data.getName());
    }

    private static Matcher<ApplicationRoleDefinition> platformMatcher() {
        return new ApplicationDefinitionMatcher(ApplicationKeys.CORE, "JIRA Core");
    }

    private static class ApplicationDefinitionMatcher extends TypeSafeDiagnosingMatcher<ApplicationRoleDefinition> {
        private ApplicationKey key;
        private String name;

        private ApplicationDefinitionMatcher(ApplicationKey key, String name) {
            this.key = key;
            this.name = name;
        }

        @Override
        protected boolean matchesSafely(final ApplicationRoleDefinition item, final Description mismatchDescription) {
            if (!key.equals(item.key()) || !name.equals(item.name())) {
                mismatchDescription.appendText(String.format("[%s(%s)]", item.name(), item.key()));
                return false;
            } else {
                return true;
            }
        }

        @Override
        public void describeTo(final Description description) {
            description.appendText(String.format("[%s(%s)]", name, key));
        }
    }

    @Test
    public void shouldReturnExpectedDisplayNameForUninstalledApplications() {
        assertExpectedDisplayNameForUninstalledApplication("jira-other", "JIRA Other");
        assertExpectedDisplayNameForUninstalledApplication("jira-other.one.-is", "JIRA Other One Is");
    }

    public void assertExpectedDisplayNameForUninstalledApplication(final String applicationKey, final String expectedDisplayName) {
        ApplicationKey appKey = ApplicationKey.valueOf(applicationKey);
        when(jiraLicenseManager.getAllLicensedApplicationKeys()).thenReturn(Sets.newHashSet(appKey));
        final String displayName = manager.getLicensed(appKey).get().name();
        assertEquals(expectedDisplayName, displayName);
    }
}
