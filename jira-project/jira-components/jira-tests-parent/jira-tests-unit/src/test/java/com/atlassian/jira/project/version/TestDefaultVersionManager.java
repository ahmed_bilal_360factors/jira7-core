package com.atlassian.jira.project.version;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.fugue.Option;
import com.atlassian.jira.association.NodeAssocationType;
import com.atlassian.jira.association.NodeAssociationStore;
import com.atlassian.jira.association.NodeAssociationStoreImpl;
import com.atlassian.jira.bc.project.version.DeleteVersionWithReplacementsParametrsBuilderImpl;
import com.atlassian.jira.bc.project.version.VersionBuilderImpl;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.database.DbConnectionManager;
import com.atlassian.jira.database.MockQueryDslAccessor;
import com.atlassian.jira.database.ResultRow;
import com.atlassian.jira.database.ResultRowBuilder;
import com.atlassian.jira.entity.Entity;
import com.atlassian.jira.event.project.VersionDeleteEvent;
import com.atlassian.jira.event.project.VersionMergeEvent;
import com.atlassian.jira.event.type.EventDispatchOption;
import com.atlassian.jira.exception.CreateException;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.UpdateIssueRequest;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.index.IndexException;
import com.atlassian.jira.issue.index.IssueIndexingService;
import com.atlassian.jira.issue.managers.MockCustomFieldManager;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.matchers.OptionalMatchers;
import com.atlassian.jira.mock.component.MockComponentWorker;
import com.atlassian.jira.mock.issue.MockIssue;
import com.atlassian.jira.mock.ofbiz.MockGenericValue;
import com.atlassian.jira.mock.ofbiz.MockOfBizDelegator;
import com.atlassian.jira.mock.project.MockVersion;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.project.version.DeleteVersionWithCustomFieldParameters.CustomFieldReplacement;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.google.common.collect.ImmutableList;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.RuleChain;
import org.mockito.Answers;
import org.mockito.ArgumentMatcher;
import org.mockito.Mock;
import org.mockito.Mockito;

import javax.annotation.Nonnull;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import static com.atlassian.fugue.Option.option;
import static com.atlassian.jira.matchers.FeatureMatchers.hasFeature;
import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Sets.newHashSet;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.emptyIterable;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.AdditionalAnswers.returnsSecondArg;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.argThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.same;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

@SuppressWarnings("deprecation")
public class TestDefaultVersionManager {
    private static final AtomicLong SEQUENCE = new AtomicLong(10000L);

    @Rule
    public RuleChain mockitoMocksInContainer = MockitoMocksInContainer.forTest(this);
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @AvailableInContainer
    @Mock
    private ProjectManager projectManager;
    @Mock
    private NodeAssociationStore nodeAssociationStore;
    @Mock
    private IssueIndexingService issueIndexService;
    @Mock
    private IssueManager issueManager;
    @Mock
    private EventPublisher eventPublisher;
    @Mock(answer = Answers.RETURNS_MOCKS)
    @AvailableInContainer
    private OfBizDelegator ofBizDelegator;
    @Mock(answer = Answers.RETURNS_MOCKS)
    private VersionStore versionStore;
    @Mock
    private DbConnectionManager dbConnectionManager;
    @Mock
    private Version mockedVersion;
    @Mock
    private CustomField customField;
    @Mock
    final private MockCustomFieldManager mockCustomFieldManager = new MockCustomFieldManager();

    private MockQueryDslAccessor mockDbConnectionManager;

    private DefaultVersionManager versionManager;

    private MockProject project;

    private Version version1;
    private Version version2;
    private Version version3;
    private Version version4;
    private Version version5;
    private Version version6;
    private Version version7;
    private Version version8;

    private Version versionOne;

    final private ApplicationUser mockUser = new MockApplicationUser("admin");
    private UpdateIssueRequest updateIssueRequest;

    @Before
    public void setUp() throws Exception {
        mockDbConnectionManager = new MockQueryDslAccessor();
        VersionCustomFieldStore versionCustomFieldStore = new VersionCustomFieldStore(mockDbConnectionManager);
        versionManager = new DefaultVersionManager(
                issueManager,
                nodeAssociationStore,
                issueIndexService,
                versionStore,
                eventPublisher,
                dbConnectionManager,
                versionCustomFieldStore);

        updateIssueRequest = UpdateIssueRequest.builder().
                eventDispatchOption(EventDispatchOption.ISSUE_UPDATED).
                sendMail(false).build();

        project = new MockProject(1L, "ABC", "Project 1");

        when(projectManager.getProjectObj(project.getId())).thenReturn(project);

        version1 = new VersionImpl(project.getId(), 1001L, "Version 1", null, 1L, false, false, null, null);

        version2 = new VersionImpl(project.getId(), 1002L, "Version 2", "The description", 2L, false, false, new Timestamp(1), null);

        version3 = new VersionImpl(project.getId(), 1003L, "Version 3", null, 3L, false, false, null, null);

        version4 = new VersionImpl(project.getId(), 1004L, "Version 4", null, 4L, true, true, null, null);

        version5 = new VersionImpl(project.getId(), 1005L, "Version 5", null, 5L, true, true, null, null);

        version6 = new VersionImpl(project.getId(), 1006L, "Version 6", null, 6L, true, true, null, null);

        version7 = new VersionImpl(project.getId(), 1007L, "Version 7", null, 7L, false, true, null, null);

        version8 = new VersionImpl(project.getId(), 1008L, "Version 8", null, 8L, true, false, null, null);

        versionOne = new VersionImpl(project.getId(), 1005L, "Version 1", null, 1L, false, false, null, null);

        resetAndInitVersionStore();
    }

    // ---- Create Version Tests ----
    @Test
    public void shouldThrowExceptionWhenInvalidParamsArePassed() throws Exception {
        expectedException.expect(CreateException.class);
        expectedException.expectMessage("You cannot create a version without a name.");
        versionManager.createVersion(null, null, null, (Long) null, null);
    }

    @Test
    public void shouldCreateFirstVersion() throws Exception {
        final String expectedVersionName = "Version Name";
        final Version version = versionManager.createVersion(expectedVersionName, null, null, project.getId(), null);
        assertVersion(expectedVersionName, null, null, project, 1L, version);
    }

    @Test
    public void shouldCreateSecondVersionWithCorrectSequenceId() throws Exception {
        final String expectedVersionName = "Version Name";
        when(versionStore.getVersionsByProject(project.getId())).thenReturn(Collections.singletonList(version1));
        final Version version = versionManager.createVersion(expectedVersionName, null, null, project.getId(), null);
        assertVersion(expectedVersionName, null, null, project, 2L, version);
    }

    @Test
    public void shouldCreateVersionInFirstSequencePositionWhenProperArgumentIsPassed() throws Exception {
        when(versionStore.getVersionsByProject(project.getId())).thenReturn(Collections.singletonList(version1));

        final String expectedVersionName = "Version Name";
        final Version version = versionManager.createVersion(expectedVersionName, null, null, project.getId(), new Long(-1));

        assertVersion(expectedVersionName, null, null, project, 1L, version);

        // make sure version1 was moved
        verify(versionStore).storeVersions(Collections.singletonList(sequence(version1, 2L)));
    }

    private void assertVersion(final String expectedVersionName, final Date expectedReleaseDate, final String expectedDescription,
                               final Project expectedProject, final Long expectedSequence, final Version actual) {
        assertEquals(expectedVersionName, actual.getName());
        assertEquals(expectedReleaseDate, actual.getReleaseDate());
        assertEquals(expectedDescription, actual.getDescription());
        assertEquals(expectedProject, actual.getProjectObject());
        assertEquals(expectedSequence, actual.getSequence());
    }

    @Test
    public void shouldCreateVersionWithReleaseDateAndDescription() throws Exception {
        final String expectedVersionName = "Version Name";
        final String expectedDescription = "Version description";
        final Date expectedReleaseDate = new Date();

        when(versionStore.getVersionsByProject(project.getId())).thenReturn(Collections.singletonList(version1));
        final Version version = versionManager.createVersion(expectedVersionName, expectedReleaseDate, expectedDescription, project.getId(),
                null);
        assertVersion(expectedVersionName, expectedReleaseDate, expectedDescription, project, 2L, version);
    }

    @Test
    public void shouldBeAbleToCreateAReleaseVersionWithJustNameAndDescription() throws CreateException {
        final String expectedVersionName = "Version Name";
        final String expectedDescription = "Version description";
        final boolean isRelease = true;

        final Version version = versionManager.createVersion(expectedVersionName, null, null, expectedDescription, project.getId(), null, isRelease);
        assertVersion(expectedVersionName, null, expectedDescription, project, 1L, version);
        assertEquals(version.isReleased(), true);
    }

    @Test
    public void shouldBeAbleToCreateAReleaseVersionWithReleaseDateAndANameAndADescription() throws CreateException {
        final String expectedVersionName = "Version Name";
        final String expectedDescription = "Version description";
        final Date expectedReleaseDate = new Date();
        final boolean isRelease = true;

        final Version version = versionManager.createVersion(expectedVersionName, null, expectedReleaseDate, expectedDescription, project.getId(), null, isRelease);
        assertVersion(expectedVersionName, expectedReleaseDate, expectedDescription, project, 1L, version);
        assertEquals(version.isReleased(), true);
    }

    @Test
    public void shouldBeAbleToCreateAReleaseVersionWithBothStartAndReleaseDateIncludingNameAndDescription() throws CreateException {
        final String expectedVersionName = "Version Name";
        final String expectedDescription = "Version description";
        final Date expectedStartDate = Date.from(LocalDate.of(2015, 10, 1).atStartOfDay(ZoneId.systemDefault()).toInstant());
        final Date expectedReleaseDate = Date.from(LocalDate.of(2015, 10, 21).atStartOfDay(ZoneId.systemDefault()).toInstant());
        final boolean isRelease = true;

        final Version version = versionManager.createVersion(expectedVersionName, expectedStartDate, expectedReleaseDate, expectedDescription, project.getId(), null, isRelease);
        assertVersion(expectedVersionName, expectedReleaseDate, expectedDescription, project, 1L, version);
        assertEquals(expectedStartDate, version.getStartDate());
        assertEquals(version.isReleased(), true);
    }

    @Test
    public void shouldNotBeAbleToCreateAVersionWithANullName() throws Exception {
        expectedException.expect(CreateException.class);
        expectedException.expectMessage("You cannot create a version without a name.");
        versionManager.createVersion(null, null, null, (Long) null, null);
    }

    @Test
    public void shouldNotBeAbleToCreateAVersionWithANullProjectId() throws Exception {
        expectedException.expect(CreateException.class);
        expectedException.expectMessage("You cannot create a version without a project.");
        versionManager.createVersion("Test Name", null, null, (Long) null, null);
    }

    // ---- Version Scheduling Tests ----

    @Test
    public void shouldReorderVersionList() throws Exception {
        // versions 2+3 are swapped, so we expect that the issues for these versions are flushed
        versionManager.storeReorderedVersionList(Arrays.asList(version1, version3, version2, version4));

        verify(versionStore).storeVersions(
                ImmutableList.of(
                        sequence(version3, 2L),
                        sequence(version2, 3L)
                ));
    }

    @Test
    public void shouldDeleteVersion() throws Exception {
        // it is called after version was deleted - because of this one it does not contains version 2
        when(versionStore.getVersionsByProject(project.getId())).thenReturn(
                Arrays.asList(version1, version3, version4));
        versionManager.deleteVersion(version2);

        // checks that request version was deleted over store
        verify(versionStore).deleteVersion(version2);

        // order was corrupted
        // checks that new sequence is OK
        // and that it was stored
        verify(versionStore).storeVersions(Arrays.asList(
                sequence(version3, 2L),
                sequence(version4, 3L)
        ));
    }

    // ---- Version Edit Tests ----
    @Test
    public void shouldThrowExceptionWhenGivenNameIsInvalid() throws Exception {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("You must specify a valid version name.");
        versionManager.editVersionDetails(null, null, null);
    }

    @Test
    public void changeVersionNameShouldFailWhenVersionWithNewNameAlreadyExists() throws Exception {
        project.setVersions(Collections.singletonList(version1));
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("A version with this name already exists in this project.");
        versionManager.editVersionDetails(versionOne, "Version 1", null);
    }

    @Test
    public void shouldChangeVersionName() throws Exception {
        final String newVersionName = "Version 2";
        versionManager.editVersionDetails(version1, newVersionName, null);

        verify(versionStore).storeVersion(new VersionBuilderImpl(version1).name(newVersionName).build());
    }

    protected Version sequence(final @Nonnull Version version, final @Nonnull Long sequence) {
        return new VersionBuilderImpl(version).sequence(sequence).build();
    }

    @Test
    public void shouldMoveVersionToLastOrFirstPositionDepenedOnGivenParameter() throws Exception {
        // assert the intial sequence order (1, 2, 3 and 4)
        assertEquals(new Long(1), version1.getSequence());
        assertEquals(new Long(2), version2.getSequence());
        assertEquals(new Long(3), version3.getSequence());
        assertEquals(new Long(4), version4.getSequence());

        List<Version> currentVersions, expectedVersions, alteredVersions;

        currentVersions = Arrays.asList(version1, version2, version3, version4);

        // move pos 1 to last using null (modifies all versions as it needs to shift everything forward by 1)
        // v1 ---+   v2
        // v2    | = v3
        // v3    |   v4
        // v4    |
        //    <--+   v1
        expectedVersions = Arrays.asList(version2, version3, version4, version1);
        alteredVersions = Arrays.asList(version2, version3, version4, version1);
        currentVersions = assertMoveVersionAfter(alteredVersions, currentVersions, version1, null, expectedVersions);

        // move pos 2 to last using null (modifies 3, 4 and 1)
        // v2        v2
        // v3 ---+ = v4
        // v4    |   v1
        // v1    |
        //    <--+   v3
        expectedVersions = Arrays.asList(version2, version4, version1, version3);
        alteredVersions = Arrays.asList(version4, version1, version3);
        resetAndInitVersionStore();
        currentVersions = assertMoveVersionAfter(alteredVersions, currentVersions,
                sequence(version3, 2L), null, expectedVersions);

        // move pos 3 to last using null (modifies 1 and 3)
        // v2        v2
        // v4      = v4
        // v1 ---+   v3
        // v3    |
        //    <--+   v1
        expectedVersions = Arrays.asList(version2, version4, version3, version1);
        alteredVersions = Arrays.asList(version3, version1);
        resetAndInitVersionStore();
        currentVersions = assertMoveVersionAfter(alteredVersions, currentVersions,
                sequence(version1, 3L), null, expectedVersions);

        // move pos 4 to last using null (should do nothing)
        // v2        v2
        // v4      = v4
        // v3        v3
        // v1 ---+   v1
        //    <--+
        expectedVersions = Arrays.asList(version2, version4, version3, version1);
        resetAndInitVersionStore();
        currentVersions = assertMoveVersionAfter(null, currentVersions, sequence(version1, 4L), null, expectedVersions);

        // move pos 4 to first using -1 (modifies all versions as it needs to shift everything back by 1)
        //    <--+   v1
        // v2    |
        // v4    | = v2
        // v3    |   v4
        // v1 ---+   v3
        expectedVersions = Arrays.asList(version1, version2, version4, version3);
        alteredVersions = Arrays.asList(version1, version2, version4, version3);
        resetAndInitVersionStore();
        currentVersions = assertMoveVersionAfter(alteredVersions, currentVersions, sequence(version1, 4L), (long) -1, expectedVersions);

        // move pos 3 to first using -1 (modifies 1, 2 and 4)
        //    <--+   v4
        // v1    |
        // v2    | = v1
        // v4 ---+   v2
        // v3        v3
        expectedVersions = Arrays.asList(version4, version1, version2, version3);
        alteredVersions = Arrays.asList(version4, version1, version2);
        resetAndInitVersionStore();
        currentVersions = assertMoveVersionAfter(alteredVersions, currentVersions, sequence(version4, 3L), (long) -1, expectedVersions);

        // move pos 2 to first using -1 (modifies 1 and 4)
        //    <--+   v1
        // v4    |
        // v1 ---+   v4
        // v2      = v2
        // v3        v3
        expectedVersions = Arrays.asList(version1, version4, version2, version3);
        alteredVersions = Arrays.asList(version1, version4);
        resetAndInitVersionStore();
        currentVersions = assertMoveVersionAfter(alteredVersions, currentVersions, sequence(version1, 2L), (long) -1, expectedVersions);

        // move pos 1 to first using -1 (should do nothing)
        //    <--+   v1
        // v1 ---+
        // v4        v4
        // v2      = v2
        // v3        v3
        expectedVersions = Arrays.asList(version1, version4, version2, version3);
        resetAndInitVersionStore();
        assertMoveVersionAfter(null, currentVersions, version1, (long) -1, expectedVersions);
    }

    @Test
    public void shouldMoveVersionToNextPositionInVersionsList() throws Exception {
        // assert the intial sequence order (1, 2, 3 and 4)
        assertEquals(new Long(1), version1.getSequence());
        assertEquals(new Long(2), version2.getSequence());
        assertEquals(new Long(3), version3.getSequence());
        assertEquals(new Long(4), version4.getSequence());

        List<Version> currentVersions, expectedVersions, alteredVersions;

        currentVersions = Arrays.asList(version1, version2, version3, version4);

        // move v1 to second
        // v1 ---+   v2
        // v2 <--+ = v1
        // v3        v3
        // v4        v4
        expectedVersions = Arrays.asList(version2, version1, version3, version4);
        alteredVersions = Arrays.asList(version2, version1);
        currentVersions = assertMoveVersionAfter(alteredVersions, currentVersions, version1, version2.getId(), expectedVersions);

        // move v1 to 3rd position
        // v2        v2
        // v1 ---+ = v3
        // v3 <--+   v1
        // v4        v4
        resetAndInitVersionStore();
        expectedVersions = Arrays.asList(version2, version3, version1, version4);
        alteredVersions = Arrays.asList(version3, version1);
        currentVersions = assertMoveVersionAfter(alteredVersions, currentVersions,
                sequence(version1, 2L), version3.getId(), expectedVersions);

        // move v1 to 4th position
        // v2        v2
        // v3        v3
        // v1 <--+ = v4
        // v4 ---+   v1
        resetAndInitVersionStore();
        expectedVersions = Arrays.asList(version2, version3, version4, version1);
        alteredVersions = Arrays.asList(version4, version1);
        currentVersions = assertMoveVersionAfter(alteredVersions, currentVersions, sequence(version1, 3L), version4.getId(), expectedVersions);

        // move v1 to 2nd position
        // v2        v2
        // v3 <--+ = v3
        // v4 ---+   v4
        // v1        v1
        reset(versionStore);
        when(versionStore.getVersion(version3.getId())).thenReturn(sequence(version3, 2L));
        expectedVersions = Arrays.asList(version2, version3, version4, version1);
        currentVersions = assertMoveVersionAfter(null, currentVersions, sequence(version4, 3L), version3.getId(), expectedVersions);

        // move v1 to 2nd position
        // v2 <--+ = v2
        // v3 ---+   v3
        // v4        v4
        // v1        v1
        reset(versionStore);
        when(versionStore.getVersion(version2.getId())).thenReturn(sequence(version2, 1L));
        expectedVersions = Arrays.asList(version2, version3, version4, version1);
        currentVersions = assertMoveVersionAfter(null, currentVersions, sequence(version3, 2L), version2.getId(), expectedVersions);

        // v2 ---+   v3
        // v3    | = v4
        // v4    |   v1
        // v1 <--+   v2
        reset(versionStore);
        when(versionStore.getVersion(version1.getId())).thenReturn(sequence(version1, 4L));
        expectedVersions = Arrays.asList(version3, version4, version1, version2);
        alteredVersions = Arrays.asList(version3, version4, version1, version2);
        currentVersions = assertMoveVersionAfter(alteredVersions, currentVersions, sequence(version2, 1L), version1.getId(), expectedVersions);

        // v3        v3
        // v4 ---+ = v1
        // v1    |   v2
        // v2 <--+   v4
        reset(versionStore);
        when(versionStore.getVersion(version2.getId())).thenReturn(sequence(version2, 4L));
        expectedVersions = Arrays.asList(version3, version1, version2, version4);
        alteredVersions = Arrays.asList(version1, version2, version4);
        currentVersions = assertMoveVersionAfter(alteredVersions, currentVersions, sequence(version4, 2L), version2.getId(), expectedVersions);

        // v3        v3
        // v1 <--+ = v1
        // v2    |   v4
        // v4 ---+   v2
        reset(versionStore);
        when(versionStore.getVersion(version1.getId())).thenReturn(sequence(version1, 2L));
        expectedVersions = Arrays.asList(version3, version1, version4, version2);
        alteredVersions = Arrays.asList(version4, version2);
        currentVersions = assertMoveVersionAfter(alteredVersions, currentVersions, version4, version1.getId(), expectedVersions);

        // v3 ---+ = v1
        // v1    |   v4
        // v4 <--+   v3
        // v2        v2
        reset(versionStore);
        when(versionStore.getVersion(version4.getId())).thenReturn(sequence(version4, 3L));
        expectedVersions = Arrays.asList(version1, version4, version3, version2);
        alteredVersions = Arrays.asList(version1, version4, version3);
        currentVersions = assertMoveVersionAfter(alteredVersions, currentVersions, sequence(version3, 1L), version4.getId(), expectedVersions);

        // v1 <--+ = v1
        // v4    |   v3
        // v3 ---+   v4
        // v2        v2
        resetAndInitVersionStore();
        expectedVersions = Arrays.asList(version1, version3, version4, version2);
        alteredVersions = Arrays.asList(version3, version4);
        currentVersions = assertMoveVersionAfter(alteredVersions, currentVersions, version3, version1.getId(), expectedVersions);

        // v1 <--+   v1
        // v3    | = v2
        // v4    |   v3
        // v2 ---+   v4
        resetAndInitVersionStore();
        expectedVersions = Arrays.asList(version1, version2, version3, version4);
        alteredVersions = Arrays.asList(version2, version3, version4);
        assertMoveVersionAfter(alteredVersions, currentVersions, sequence(version2, 4L), version1.getId(), expectedVersions);
    }

    // ---- Release Version Tests ----
    @Test
    public void shouldReleaseVersionWhenPassedParameterIsTrue() throws Exception {
        versionManager.releaseVersions(Arrays.asList(version1), true);

        verify(versionStore).storeVersions(Collections.singletonList(new VersionBuilderImpl(version1).released(true).build()));
    }

    // // ---- Version Archive Tests ----
    @Test
    public void shouldArchiveVersionsWithPassedIds() throws Exception {
        when(versionStore.getVersion(version1.getId())).thenReturn(version1);

        @SuppressWarnings("ConstantConditions")
        final String[] idsToArchive = {version1.getId().toString()};
        final String[] idsToUnArchive = {};

        versionManager.archiveVersions(idsToArchive, idsToUnArchive);

        verify(versionStore).storeVersions(Collections.singletonList(new VersionBuilderImpl(version1).archived(true).build()));
    }

    @Test
    public void shouldReturnUnarchivedVersionsForPassedProjectId() throws Exception {
        final List<Version> unarchivedVersions = Arrays.asList(version1, version2,
                version3);
        when(versionStore.getVersionsByProject(project.getId())).thenReturn(unarchivedVersions);

        final Collection<Version> unarchived = versionManager.getVersionsUnarchived(project.getId());
        assertEquals(Arrays.asList(version1, version2, version3), unarchived);
    }

    @Test
    public void shouldReturnArchivedVersionsForProjectWithGivenId() throws Exception {
        final List<Version> unarchivedVersions = Arrays.asList(version4, version5,
                version6);
        when(versionStore.getVersionsByProject(project.getId())).thenReturn(unarchivedVersions);

        final Collection<Version> archived = versionManager.getVersionsArchived(project);

        assertEquals(Arrays.asList(version4, version5, version6), archived);
    }

    @Test
    public void shouldReturnAllExistingVersions() throws Exception {
        final List<Version> allVersionsIn = Arrays.asList(versionOne, version1,
                version2, version3, version4, version5,
                version6);
        when(versionStore.getAllVersions()).thenReturn(allVersionsIn);

        final Collection<Version> allVersions = versionManager.getAllVersions();
        assertEquals(Arrays.asList(versionOne, version1, version2, version3, version4, version5, version6), allVersions);
    }

    @Test
    public void shouldReturnAllExistingReleasedVersions() throws Exception {
        final List<Version> allVersionsIn = Arrays.asList(versionOne, version1,
                version2, version3, version4, version5,
                version6, version7);
        when(versionStore.getAllVersions()).thenReturn(allVersionsIn);

        final Collection<Version> releasedVersions = versionManager.getAllVersionsReleased(true);
        assertEquals(Arrays.asList(version4, version5, version6, version7), releasedVersions);

        final Collection<Version> releasedVersionsNotArchived = versionManager.getAllVersionsReleased(false);
        assertEquals(Arrays.asList(version7), releasedVersionsNotArchived);
    }

    @Test
    public void shouldReturnAllUnreleasedVersions() throws Exception {
        final List<Version> allVersionsIn = Arrays.asList(version1, version2,
                version3, version4, version5, version6,
                version8);
        when(versionStore.getAllVersions()).thenReturn(allVersionsIn);

        final Collection<Version> unreleasedVersions = versionManager.getAllVersionsUnreleased(true);
        assertEquals(Arrays.asList(version1, version2, version3, version8), unreleasedVersions);

        final Collection<Version> unreleasedVersionsNotArchived = versionManager.getAllVersionsUnreleased(false);
        assertEquals(Arrays.asList(version1, version2, version3), unreleasedVersionsNotArchived);
    }

    @Test
    public void shouldReturnFalseWhenVersionIsOverDue() throws Exception {
        final Calendar tomorrow = Calendar.getInstance();
        tomorrow.setTimeInMillis(System.currentTimeMillis());
        tomorrow.add(Calendar.DATE, 1);

        final Version vTomorrow = new VersionImpl(project.getId(), 22222L, "Version Tomorrow", "The description", 2L,
                false, false, new Timestamp(tomorrow.getTimeInMillis()), null);

        assertFalse("Future day was marked incorrectly overdue", versionManager.isVersionOverDue(vTomorrow));

        final Calendar yesterday = Calendar.getInstance();
        yesterday.setTimeInMillis(System.currentTimeMillis());
        yesterday.add(Calendar.DATE, -1);

        final Version vOverdue = new VersionImpl(project.getId(), 33333L, "Version Yesterday", "The description", 2L,
                false, false, new Timestamp(yesterday.getTimeInMillis()), null);

        assertTrue("Past date was marked as not overdue", versionManager.isVersionOverDue(vOverdue));

        final Version vToday = new VersionImpl(project.getId(), 11111L, "Version Today", "The description", 2L,
                false, false, new Timestamp(System.currentTimeMillis()), null);

        assertFalse("Today was marked incorrectly overdue", versionManager.isVersionOverDue(vToday));
    }

    @Test
    public void shouldUpdateIssueAffectsVersions() throws Exception {
        final Issue issue = new MockIssue(12L);
        final MockOfBizDelegator mockOfBizDelegator = new MockOfBizDelegator();
        // add the version GVs
        mockOfBizDelegator.createValue(new MockGenericValue(Entity.Name.VERSION, 1L));
        mockOfBizDelegator.createValue(new MockGenericValue(Entity.Name.VERSION, 2L));
        mockOfBizDelegator.createValue(new MockGenericValue(Entity.Name.VERSION, 3L));

        nodeAssociationStore = new NodeAssociationStoreImpl(mockOfBizDelegator);
        DefaultVersionManager defaultVersionManager = new DefaultVersionManager(null, nodeAssociationStore, null, null, null, dbConnectionManager, null);
        List<Long> sinkIds;
        // Assert Preconditions
        sinkIds = nodeAssociationStore.getSinkIdsFromSource(NodeAssocationType.ISSUE_TO_AFFECTS_VERISON, 12L);
        assertEquals(0, sinkIds.size());

        // Test 1: Add an affects version
        defaultVersionManager.updateIssueAffectsVersions(issue, Arrays.<Version>asList(new MockVersion(2L, "v2")));
        // assert outcome
        sinkIds = nodeAssociationStore.getSinkIdsFromSource(NodeAssocationType.ISSUE_TO_AFFECTS_VERISON, 12L);
        assertEquals(1, sinkIds.size());
        assertTrue(sinkIds.contains(2L));

        // Test 2: Add another affects version
        defaultVersionManager.updateIssueAffectsVersions(issue, Arrays.<Version>asList(new MockVersion(1L, "v1"), new MockVersion(2L, "v2")));
        // assert outcome
        sinkIds = nodeAssociationStore.getSinkIdsFromSource(NodeAssocationType.ISSUE_TO_AFFECTS_VERISON, 12L);
        assertEquals(2, sinkIds.size());
        assertTrue(sinkIds.contains(1L));
        assertTrue(sinkIds.contains(2L));

        // Test 3: Change version
        defaultVersionManager.updateIssueAffectsVersions(issue, Arrays.<Version>asList(new MockVersion(3L, "v3"), new MockVersion(1L, "v1")));
        // assert outcome
        sinkIds = nodeAssociationStore.getSinkIdsFromSource(NodeAssocationType.ISSUE_TO_AFFECTS_VERISON, 12L);
        assertEquals(2, sinkIds.size());
        assertTrue(sinkIds.contains(3L));
        assertTrue(sinkIds.contains(1L));

        // Test 4: Delete all
        defaultVersionManager.updateIssueAffectsVersions(issue, Arrays.<Version>asList());
        // assert outcome
        sinkIds = nodeAssociationStore.getSinkIdsFromSource(NodeAssocationType.ISSUE_TO_AFFECTS_VERISON, 12L);
        assertEquals(0, sinkIds.size());
    }

    @Test
    public void shouldUpdateIssueFixVersion() throws Exception {
        final Issue issue = new MockIssue(12L);
        final MockOfBizDelegator mockOfBizDelegator = new MockOfBizDelegator();
        // add the version GVs
        mockOfBizDelegator.createValue(new MockGenericValue(Entity.Name.VERSION, 1L));
        mockOfBizDelegator.createValue(new MockGenericValue(Entity.Name.VERSION, 2L));
        mockOfBizDelegator.createValue(new MockGenericValue(Entity.Name.VERSION, 3L));

        nodeAssociationStore = new NodeAssociationStoreImpl(mockOfBizDelegator);
        DefaultVersionManager defaultVersionManager = new DefaultVersionManager(null, nodeAssociationStore, null, null, null, dbConnectionManager, null);
        List<Long> sinkIds;
        // Assert Preconditions
        sinkIds = nodeAssociationStore.getSinkIdsFromSource(NodeAssocationType.ISSUE_TO_FIX_VERISON, 12L);
        assertEquals(0, sinkIds.size());

        // Test 1: Add a fix version
        defaultVersionManager.updateIssueFixVersions(issue, Arrays.<Version>asList(new MockVersion(2L, "v2")));
        // assert outcome
        sinkIds = nodeAssociationStore.getSinkIdsFromSource(NodeAssocationType.ISSUE_TO_FIX_VERISON, 12L);
        assertEquals(1, sinkIds.size());
        assertTrue(sinkIds.contains(2L));

        // Test 2: Add another fix version
        defaultVersionManager.updateIssueFixVersions(issue, Arrays.<Version>asList(new MockVersion(1L, "v1"), new MockVersion(2L, "v2")));
        // assert outcome
        sinkIds = nodeAssociationStore.getSinkIdsFromSource(NodeAssocationType.ISSUE_TO_FIX_VERISON, 12L);
        assertEquals(2, sinkIds.size());
        assertTrue(sinkIds.contains(1L));
        assertTrue(sinkIds.contains(2L));

        // Test 3: Change version
        defaultVersionManager.updateIssueFixVersions(issue, Arrays.<Version>asList(new MockVersion(3L, "v3"), new MockVersion(1L, "v1")));
        // assert outcome
        sinkIds = nodeAssociationStore.getSinkIdsFromSource(NodeAssocationType.ISSUE_TO_FIX_VERISON, 12L);
        assertEquals(2, sinkIds.size());
        assertTrue(sinkIds.contains(3L));
        assertTrue(sinkIds.contains(1L));

        // Test 4: Delete all
        defaultVersionManager.updateIssueFixVersions(issue, Arrays.<Version>asList());
        // assert outcome
        sinkIds = nodeAssociationStore.getSinkIdsFromSource(NodeAssocationType.ISSUE_TO_FIX_VERISON, 12L);
        assertEquals(0, sinkIds.size());
    }

    @Test
    public void shouldDoNothingWhenNoIssueIsUsingAnyVersion() {
        ComponentAccessor.initialiseWorker(new MockComponentWorker().addMock(CustomFieldManager.class, mockCustomFieldManager));
        when(nodeAssociationStore.getSourceIdsFromSink(NodeAssocationType.ISSUE_TO_AFFECTS_VERISON, version1.getId())).thenReturn(Collections.<Long>emptyList());
        when(nodeAssociationStore.getSourceIdsFromSink(NodeAssocationType.ISSUE_TO_FIX_VERISON, version1.getId())).thenReturn(Collections.<Long>emptyList());
        mockDbConnectionManager.setDefaultQueryResult(Collections.emptyList());

        versionManager.swapVersionForRelatedIssues(mockUser, version1, option(version2), Option.<Version>none());

        verifyZeroInteractions(issueManager, issueIndexService, eventPublisher);
    }

    @Test
    public void shouldRemoveFixVersionFromIssueWhenOptionObjectIsSetToNone() throws IndexException {
        ComponentAccessor.initialiseWorker(new MockComponentWorker().addMock(CustomFieldManager.class, mockCustomFieldManager));
        final MockIssue mockIssue = new MockIssue(10034L);
        mockIssue.setAffectedVersions(newHashSet());
        mockIssue.setFixVersions(newHashSet(version1, version2));

        mockDbConnectionManager.setDefaultQueryResult(newArrayList());

        when(nodeAssociationStore.getSourceIdsFromSink(NodeAssocationType.ISSUE_TO_AFFECTS_VERISON, version1.getId())).thenReturn(Collections.<Long>emptyList());
        when(nodeAssociationStore.getSourceIdsFromSink(NodeAssocationType.ISSUE_TO_FIX_VERISON, version1.getId())).thenReturn(newArrayList(10034L));

        when(issueManager.getIssueObject(10034L)).thenReturn(mockIssue);
        when(issueManager.updateIssue(eq(mockUser), eq(mockIssue), eq(updateIssueRequest))).thenReturn(mockIssue);

        //lets simulate swapping fix version with nothing (i.e. simply remove the fix version that matches).
        versionManager.swapVersionForRelatedIssues(mockUser, version1, option(version2), Option.<Version>none());

        assertThat((Collection<Version>)mockIssue.getAffectedVersions(), empty());
        assertThat((Collection<Version>)mockIssue.getFixVersions(), contains(version2));

        verify(issueManager).updateIssue(eq(mockUser), eq(mockIssue), eq(updateIssueRequest));
        verify(issueIndexService).reIndex(mockIssue);
    }

    @Test
    public void shouldDoNothingWhenDeletedVersionIsNotUsedByIssue() throws Exception {
        ComponentAccessor.initialiseWorker(new MockComponentWorker().addMock(CustomFieldManager.class, mockCustomFieldManager));
        final MockIssue mockIssue = new MockIssue(10034L);
        mockIssue.setAffectedVersions(newHashSet());
        mockIssue.setFixVersions(newHashSet());

        mockDbConnectionManager.setQueryResults(createQueryForGettingIssuesIdsWithCustomFieldShowing(version1), newArrayList());
        mockDbConnectionManager.setQueryResults(createQueryForGettingCustomFieldId(version1), newArrayList());

        when(nodeAssociationStore.getSourceIdsFromSink(NodeAssocationType.ISSUE_TO_AFFECTS_VERISON, version1.getId())).thenReturn(Collections.<Long>emptyList());
        when(nodeAssociationStore.getSourceIdsFromSink(NodeAssocationType.ISSUE_TO_FIX_VERISON, version1.getId())).thenReturn(newArrayList(10034L));

        when(issueManager.getIssueObject(10034L)).thenReturn(mockIssue);
        when(issueManager.updateIssue(eq(mockUser), eq(mockIssue), eq(updateIssueRequest))).thenReturn(mockIssue);

        //lets simulate swapping fix version.  Nothing should happen since the issue currently has no fix versions set.
        versionManager.swapVersionForRelatedIssues(mockUser, version1, Option.<Version>none(), option(version3));

        assertTrue(mockIssue.getAffectedVersions().isEmpty());
        assertTrue(mockIssue.getFixVersions().isEmpty());

        verify(issueManager).updateIssue(eq(mockUser), eq(mockIssue), eq(updateIssueRequest));
        verify(issueIndexService).reIndex(mockIssue);
        verify(eventPublisher).publish(Mockito.any(VersionMergeEvent.class));
    }

    @Test
    public void shouldReturnEmptyIterableWhenThereAreNoCustomFieldUsages() throws Exception {
        final ResultRowBuilder resultRowBuilder = new ResultRowBuilder();

        mockDbConnectionManager.setQueryResults(createQueryForGettingCustomFieldUsage(version1), resultRowBuilder.toList());

        final Iterable<CustomFieldWithVersionUsage> fieldUsages = versionManager.getCustomFieldsUsing(version1);
        assertThat(fieldUsages, is(emptyIterable()));
    }


    @Test
    public void shouldReturnCustomFieldUsages() throws Exception {
        final ResultRowBuilder resultRowBuilder = new ResultRowBuilder();
        resultRowBuilder.addRow(10000L, "Field1", 1L);
        resultRowBuilder.addRow(10001L, "Field2", 2L);
        resultRowBuilder.addRow(10002L, "Field3", 3L);

        mockDbConnectionManager.setQueryResults(createQueryForGettingCustomFieldUsage(version1), resultRowBuilder.toList());

        final Iterable<CustomFieldWithVersionUsage> fieldUsages = versionManager.getCustomFieldsUsing(version1);
        assertThat(fieldUsages, contains(
                fieldCountMatches(10000L, "Field1", 1L),
                fieldCountMatches(10001L, "Field2", 2L),
                fieldCountMatches(10002L, "Field3", 3L)
        ));
    }

    private Matcher<CustomFieldWithVersionUsage> fieldCountMatches(long cfId, String fieldName, long usageCount) {
        return allOf(
                hasFeature(CustomFieldWithVersionUsage::getCustomFieldId, equalTo(cfId), "Field id", "Field id"),
                hasFeature(CustomFieldWithVersionUsage::getFieldName, equalTo(fieldName), "Field name", "Field name"),
                hasFeature(CustomFieldWithVersionUsage::getIssueCountWithVersionInCustomField, equalTo(usageCount), "Usage count", "Usage count")
        );
    }

    @Test
    public void shouldSwapVersionsInIssues() throws IndexException {
        ComponentAccessor.initialiseWorker(new MockComponentWorker().addMock(CustomFieldManager.class, mockCustomFieldManager));
        final MockIssue mockIssue = new MockIssue(10034L);
        mockIssue.setAffectedVersions(newArrayList(version4, version1));
        mockIssue.setFixVersions(newArrayList(version1, version5, version4));

        mockDbConnectionManager.setDefaultQueryResult(newArrayList());

        when(nodeAssociationStore.getSourceIdsFromSink(NodeAssocationType.ISSUE_TO_AFFECTS_VERISON, version1.getId())).thenReturn(newArrayList(10034L));
        when(nodeAssociationStore.getSourceIdsFromSink(NodeAssocationType.ISSUE_TO_FIX_VERISON, version1.getId())).thenReturn(newArrayList(10034L));

        when(issueManager.getIssueObject(10034L)).thenReturn(mockIssue);
        when(issueManager.updateIssue(eq(mockUser), eq(mockIssue), eq(updateIssueRequest))).thenReturn(mockIssue);

        //lets simulate swapping fix version.  Nothing should happen since the issue currently has no fix versions set.
        versionManager.swapVersionForRelatedIssues(mockUser, version1, option(version7), option(version3));

        assertEquals(newArrayList(version4, version7), mockIssue.getAffectedVersions());
        assertEquals(newArrayList(version3, version5, version4), mockIssue.getFixVersions());

        verify(issueManager).updateIssue(eq(mockUser), eq(mockIssue), eq(updateIssueRequest));
        verify(issueIndexService).reIndex(mockIssue);
        verify(eventPublisher).publish(Mockito.any(VersionMergeEvent.class));
    }

    @Test
    public void shouldSwapVersionOnMultipleIssues() throws Exception {
        ComponentAccessor.initialiseWorker(new MockComponentWorker().addMock(CustomFieldManager.class, mockCustomFieldManager));
        final MockIssue mockIssue = new MockIssue(10034L);
        mockIssue.setAffectedVersions(newArrayList(version4, version1));
        mockIssue.setFixVersions(newArrayList(version1, version5, version4));

        final MockIssue mockIssue2 = new MockIssue(2000L);
        mockIssue2.setAffectedVersions(newArrayList(version7, version1, version3));
        mockIssue2.setFixVersions(newArrayList());

        when(nodeAssociationStore.getSourceIdsFromSink(NodeAssocationType.ISSUE_TO_AFFECTS_VERISON, version1.getId())).thenReturn(newArrayList(2000L));
        when(nodeAssociationStore.getSourceIdsFromSink(NodeAssocationType.ISSUE_TO_FIX_VERISON, version1.getId())).thenReturn(newArrayList(10034L));

        when(issueManager.getIssueObject(10034L)).thenReturn(mockIssue);
        when(issueManager.updateIssue(eq(mockUser), eq(mockIssue), eq(updateIssueRequest))).thenReturn(mockIssue);
        when(issueManager.getIssueObject(2000L)).thenReturn(mockIssue2);
        when(issueManager.updateIssue(eq(mockUser), eq(mockIssue2), eq(updateIssueRequest))).thenReturn(mockIssue2);
        mockDbConnectionManager.setQueryResults(createQueryForGettingIssuesIdsWithCustomFieldShowing(version1), newArrayList());
        mockDbConnectionManager.setQueryResults(createQueryForGettingCustomFieldId(version1), newArrayList());

        //lets simulate swapping fix version.  Nothing should happen since the issue currently has no fix versions set.
        versionManager.swapVersionForRelatedIssues(mockUser, version1, option(version2), option(version2));

        assertThat((Collection<Version>) mockIssue.getAffectedVersions(), contains(version4, version2));
        assertThat((Collection<Version>) mockIssue.getFixVersions(), contains(version2, version5, version4));

        assertThat((Collection<Version>) mockIssue2.getAffectedVersions(), contains(version7, version2, version3));
        assertThat((Collection<Version>) mockIssue2.getFixVersions(), Matchers.empty());

        verify(issueManager).updateIssue(eq(mockUser), eq(mockIssue), eq(updateIssueRequest));
        verify(issueManager).updateIssue(eq(mockUser), eq(mockIssue2), eq(updateIssueRequest));
        verify(issueIndexService).reIndex(mockIssue);
        verify(issueIndexService).reIndex(mockIssue2);

        //event should only be published once, since only one issue had fix versions replaced.
        verify(eventPublisher).publish(Mockito.any(VersionMergeEvent.class));
    }

    @Test
    public void shouldPublishEventThatHasInformationAboutTheMerge() {
        ComponentAccessor.initialiseWorker(new MockComponentWorker().addMock(CustomFieldManager.class, mockCustomFieldManager));
        createIssueWithAffectsVersion(version1);

        mockDbConnectionManager.setQueryResults(createQueryForGettingIssuesIdsWithCustomFieldShowing(version1), newArrayList());
        mockDbConnectionManager.setDefaultQueryResult(newArrayList());

        versionManager.merge(mockUser, version1, version2);

        verify(eventPublisher, times(1)).publish(Mockito.eq(VersionDeleteEvent.deletedAndMerged(version1, version2)));
        verify(eventPublisher, times(1)).publish(Mockito.eq(new VersionMergeEvent(version2, version1)));
    }

    @Test
    public void versionShouldBeSwappedWhenNewVersionIsPassedInOptionObject() throws Exception {
        ComponentAccessor.initialiseWorker(new MockComponentWorker().addMock(CustomFieldManager.class, mockCustomFieldManager));
        final Issue issueWithAffectsVersion = createIssueWithAffectsVersion(version1);
        final Issue issueWithFixVersion = createIssueWithFixVersion(version1);

        mockDbConnectionManager.setDefaultQueryResult(newArrayList());

        versionManager.deleteVersion(mockUser, version1, option(version2), option(version3));

        assertThat(issueWithAffectsVersion.getAffectedVersions(), contains(version2));
        assertThat(issueWithAffectsVersion.getAffectedVersions(), hasSize(1));

        assertThat(issueWithFixVersion.getFixVersions(), contains(version3));
        assertThat(issueWithFixVersion.getFixVersions(), hasSize(1));

        verify(eventPublisher).publish(Mockito.eq(new VersionDeleteEvent.VersionDeleteEventBuilder(version1)
                .affectsVersionSwappedTo(version2)
                .fixVersionSwappedTo(version3)
                .createEvent()));
    }

    @Test
    public void shouldMergeVersionAndReplaceTheOldOneWithNewInIssues() throws Exception {
        ComponentAccessor.initialiseWorker(new MockComponentWorker().addMock(CustomFieldManager.class, mockCustomFieldManager));
        final Issue issue = createIssueWithAffectsVersion(version1);

        mockDbConnectionManager.setDefaultQueryResult(newArrayList());

        versionManager.merge(mockUser, version1, version2);

        assertThat(issue.getAffectedVersions(), hasSize(1));
        assertThat(issue.getAffectedVersions(), contains(version2));

        verify(versionStore).deleteVersion(version1);
    }

    @Test
    public void shouldRemoveVersionFromIssues() throws Exception {
        ComponentAccessor.initialiseWorker(new MockComponentWorker().addMock(CustomFieldManager.class, mockCustomFieldManager));
        final Issue issue = createIssueWithAffectsVersion(version1);

        mockDbConnectionManager.setDefaultQueryResult(newArrayList());
        mockDbConnectionManager.setDefaultUpdateResult(0);

        versionManager.deleteAndRemoveFromIssues(mockUser, version1);

        assertThat(issue.getAffectedVersions(), hasSize(0));

        verify(versionStore).deleteVersion(version1);
    }

    @Test
    public void shouldRemoveVersionFromCustomField() throws Exception {
        ComponentAccessor.initialiseWorker(new MockComponentWorker().addMock(CustomFieldManager.class, mockCustomFieldManager));
        final MutableIssue issue = mock(MutableIssue.class);
        final List<Version> customFieldValuesList = newArrayList(version1);
        final List<String> customFieldNamesList = newArrayList("Custom1");
        final ResultRowBuilder customFieldNamesResultBuilder = new ResultRowBuilder();
        final ResultRowBuilder issueIdsResultBuilder = new ResultRowBuilder();
        when(issue.getAffectedVersions()).thenReturn(newArrayList());
        when(issue.getFixVersions()).thenReturn(newArrayList());
        customFieldNamesList.stream().forEach(customFieldNamesResultBuilder::addRow);
        issueIdsResultBuilder.addRow(issue.getId());
        final long customFieldId = 34589L;
        when(customField.getIdAsLong()).thenReturn(customFieldId);

        mockDbConnectionManager.setQueryResults(createQueryForGettingIssuesIdsWithCustomFieldShowing(version1), issueIdsResultBuilder.toList());
        mockDbConnectionManager.setQueryResults(createQueryForGettingCustomFieldId(version1), ImmutableList.of(new ResultRow(customFieldId)));

        when(issue.getCustomFieldValue(customField)).thenReturn(customFieldValuesList);
        when(issueManager.getIssueObject(issue.getId())).thenReturn(issue);
        when(mockCustomFieldManager.getCustomFieldObject(customFieldId)).thenReturn(customField);

        versionManager.deleteVersion(mockUser, version1, Option.none(), Option.none());

        verify(issue).setCustomFieldValue(same(customField), argThat(empty()));
        mockDbConnectionManager.assertAllExpectedStatementsWereRun();
    }

    @Test
    public void shouldPublishEventWithCustomFieldReplacements() throws Exception {
        ComponentAccessor.initialiseWorker(new MockComponentWorker().addMock(CustomFieldManager.class, mockCustomFieldManager));
        final long customFieldId = 1000L;

        mockDbConnectionManager.setDefaultQueryResult(Collections.emptyList());

        versionManager.deleteVersionAndSwap(mockUser,
                new DeleteVersionWithReplacementsParametrsBuilderImpl(version1)
                        .moveCustomFieldTo(customFieldId, version2)
                        .build()
        );

        verify(eventPublisher).publish(argThat(
                hasFeature(
                        VersionDeleteEvent::getCustomFieldReplacements,
                        contains(allOf(
                                hasFeature(
                                        CustomFieldReplacement::getCustomFieldId,
                                        equalTo(customFieldId),
                                        "customFieldId", "customFieldId"),
                                hasFeature(
                                        CustomFieldReplacement::getMoveTo,
                                        OptionalMatchers.some(
                                                hasFeature(
                                                        Version::getId,
                                                        equalTo(version2.getId()),
                                                        "version id", "version id")),
                                        "moveTo version", "moveTo version")
                        )),
                        "customFieldReplacements", "customFieldReplacements"))
        );
        Mockito.verifyNoMoreInteractions(eventPublisher);
    }

    @Test
    public void shouldReindexIssuesFromStandardAndCustomFields() throws Exception {
        ComponentAccessor.initialiseWorker(new MockComponentWorker().addMock(CustomFieldManager.class, mockCustomFieldManager));
        final long customFieldId = 1000L;

        mockDbConnectionManager.setQueryResults(createQueryForGettingIssuesIdsWithCustomFieldShowing(version1), ImmutableList.of(
                new ResultRow(2L)));
        mockDbConnectionManager.setQueryResults(createQueryForGettingCustomFieldId(version1), ImmutableList.of(
                new ResultRow(customFieldId)));
        when(nodeAssociationStore.getSourceIdsFromSink(NodeAssocationType.ISSUE_TO_AFFECTS_VERISON, version1.getId())).thenReturn(
                ImmutableList.of(12L));
        when(nodeAssociationStore.getSourceIdsFromSink(NodeAssocationType.ISSUE_TO_FIX_VERISON, version1.getId())).thenReturn(
                ImmutableList.of(22L));

        MutableIssue customFieldIssue = mock(MutableIssue.class);
        MutableIssue fixVersionIssue = mock(MutableIssue.class);
        MutableIssue affectsVersionIssue = mock(MutableIssue.class);
        when(issueManager.getIssueObject(2L)).thenReturn(customFieldIssue);
        when(issueManager.getIssueObject(12L)).thenReturn(affectsVersionIssue);
        when(issueManager.getIssueObject(22L)).thenReturn(fixVersionIssue);
        when(issueManager.updateIssue(any(), any(), any())).then(returnsSecondArg());
        when(mockCustomFieldManager.getCustomFieldObject(customFieldId)).thenReturn(mock(CustomField.class));

        versionManager.deleteVersionAndSwap(mockUser,
                new DeleteVersionWithReplacementsParametrsBuilderImpl(version1)
                        .moveCustomFieldTo(1000, version2)
                        .build()
        );

        verify(issueManager).updateIssue(any(), eq(customFieldIssue), any());
        verify(issueManager).updateIssue(any(), eq(fixVersionIssue), any());
        verify(issueManager).updateIssue(any(), eq(affectsVersionIssue), any());
        verify(issueIndexService).reIndex(customFieldIssue);
        verify(issueIndexService).reIndex(fixVersionIssue);
        verify(issueIndexService).reIndex(affectsVersionIssue);
        mockDbConnectionManager.assertAllExpectedStatementsWereRun();
    }

    @Test
    public void testDeleteAllVersions() {
        versionManager.deleteAllVersions(project.getId());

        verify(versionStore).deleteAllVersions(project.getId());
        verifyZeroInteractions(eventPublisher);
    }

    private Issue createIssueWithAffectsVersion(final Version version) {
        final MockIssue issue = new MockIssue(SEQUENCE.incrementAndGet());
        issue.setAffectedVersions(Collections.singleton(version));
        issue.setFixVersions(Collections.singleton(version));
        when(nodeAssociationStore.getSourceIdsFromSink(NodeAssocationType.ISSUE_TO_AFFECTS_VERISON, version.getId())).thenReturn(newArrayList(issue.getId()));
        when(issueManager.getIssueObject(issue.getId())).thenReturn(issue);
        return issue;
    }

    private Issue createIssueWithFixVersion(final Version version) {
        final MockIssue issue = new MockIssue(SEQUENCE.incrementAndGet());
        issue.setAffectedVersions(Collections.singleton(version));
        issue.setFixVersions(Collections.singleton(version));
        when(nodeAssociationStore.getSourceIdsFromSink(NodeAssocationType.ISSUE_TO_FIX_VERISON, version.getId())).thenReturn(newArrayList(issue.getId()));
        when(issueManager.getIssueObject(issue.getId())).thenReturn(issue);
        return issue;
    }

    private List<Version> assertMoveVersionAfter(final List<Version> expectedStoredVersions, final List<Version> currentVersions,
                                                 final Version toMove, final Long scheduleAfterVersionId, final List<Version> expectedVersions) {
        when(versionStore.getVersionsByProject(project.getId())).thenReturn(currentVersions);

        versionManager.moveVersionAfter(toMove, scheduleAfterVersionId);

        assertStoredVersions(expectedStoredVersions);

        // rewrite result so sequence number are correct
        final List<Version> result = newArrayList();
        for (int i = 0; i < expectedVersions.size(); ++i) {
            result.add(sequence(expectedVersions.get(i), (long) i + 1));
        }
        return result;
    }

    private void assertStoredVersions(final List<Version> versions) {
        verify(versionStore, Mockito.never()).storeVersion(Mockito.<Version>any());
        if (versions == null) {
            verify(versionStore, Mockito.never()).storeVersions(argThat(new ArgumentMatcher<Collection<Version>>() {

                @Override
                public boolean matches(final Object argument) {
                    @SuppressWarnings("unchecked")
                    final Collection<Version> versions = (Collection<Version>) argument;
                    return !versions.isEmpty();
                }

            }));
        } else {
            verify(versionStore, Mockito.atLeastOnce()).storeVersions(argThat(new ArgumentMatcher<List<Version>>() {

                @SuppressWarnings("unchecked")
                @Override
                public boolean matches(final Object argument) {
                    final List<Version> versionsArgument = (List<Version>) argument;
                    if (versions.size() != versionsArgument.size()) {
                        return false;
                    }
                    for (int i = 0; i < versions.size(); ++i) {
                        //noinspection ConstantConditions
                        if (!versions.get(i).getId().equals(versionsArgument.get(i).getId())) {
                            return false;
                        }
                    }
                    return true;
                }

                @Override
                public void describeTo(final Description description) {
                    description.appendValue(versions).appendText(" in any order");
                }

            }));
        }
    }

    private void resetAndInitVersionStore() {
        reset(versionStore);
        when(versionStore.createVersion(Mockito.<Version>any())).thenAnswer(invocation -> new VersionBuilderImpl((Version) invocation.getArguments()[0]).build());
        whenGetVersionByVersionId(versionOne, version1, version2, version3, version4, version5, version6, version7, version8);
    }

    private void whenGetVersionByVersionId(final Version... versions) {
        for (final Version version : versions) {
            when(versionStore.getVersion(version.getId())).thenReturn(version);
        }
    }

    private String createQueryForGettingCustomFieldUsage(final Version version) {
        return "select CUSTOM_FIELD_VALUE.customfield, CUSTOM_FIELD.cfname, count(distinct CUSTOM_FIELD_VALUE.issue)\n" +
                "from customfieldvalue CUSTOM_FIELD_VALUE\n" +
                "left join customfield CUSTOM_FIELD\n" +
                "on CUSTOM_FIELD_VALUE.customfield = CUSTOM_FIELD.id\n" +
                "where CUSTOM_FIELD_VALUE.numbervalue = " + version.getId() + ".0 and " +
                "(CUSTOM_FIELD.customfieldtypekey = 'com.atlassian.jira.plugin.system.customfieldtypes:version' or " +
                "CUSTOM_FIELD.customfieldtypekey = 'com.atlassian.jira.plugin.system.customfieldtypes:multiversion')\n" +
                "group by CUSTOM_FIELD.cfname, CUSTOM_FIELD_VALUE.customfield";
    }

    private String createQueryForGettingIssuesIdsWithCustomFieldShowing(final Version version) {
        return "select distinct CUSTOM_FIELD_VALUE.issue\n" +
                "from customfieldvalue CUSTOM_FIELD_VALUE\n" +
                "left join customfield CUSTOM_FIELD\n" +
                "on CUSTOM_FIELD_VALUE.customfield = CUSTOM_FIELD.id\n" +
                "where CUSTOM_FIELD_VALUE.numbervalue = " + version.getId() + ".0 and (CUSTOM_FIELD.customfieldtypekey = 'com.atlassian.jira.plugin.system.customfieldtypes:version' or CUSTOM_FIELD.customfieldtypekey = 'com.atlassian.jira.plugin.system.customfieldtypes:multiversion')";
    }

    private String createQueryForGettingCustomFieldId(final Version version) {
        return "select distinct CUSTOM_FIELD_VALUE.customfield\n" +
                "from customfieldvalue CUSTOM_FIELD_VALUE\n" +
                "left join customfield CUSTOM_FIELD\n" +
                "on CUSTOM_FIELD_VALUE.customfield = CUSTOM_FIELD.id\n" +
                "where CUSTOM_FIELD_VALUE.numbervalue = " + version.getId() + ".0 and (CUSTOM_FIELD.customfieldtypekey = 'com.atlassian.jira.plugin.system.customfieldtypes:version' or CUSTOM_FIELD.customfieldtypekey = 'com.atlassian.jira.plugin.system.customfieldtypes:multiversion')";
    }

}
