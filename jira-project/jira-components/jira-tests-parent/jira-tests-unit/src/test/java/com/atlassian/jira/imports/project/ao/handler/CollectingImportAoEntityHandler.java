package com.atlassian.jira.imports.project.ao.handler;

import com.atlassian.jira.exception.ParseException;
import com.atlassian.jira.imports.project.core.BackupProject;
import com.atlassian.jira.imports.project.core.BackupSystemInformation;
import com.atlassian.jira.imports.project.core.ProjectImportResults;
import com.atlassian.jira.imports.project.handler.AbortImportException;
import com.atlassian.jira.imports.project.mapper.ProjectImportMapper;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A helper for examining how AO Import parsing is going on in Unit Tests.
 *
 * @since v6.5
 */
class CollectingImportAoEntityHandler implements PluggableImportAoEntityHandler {
    public Map<String, List<Map<String, Object>>> tables = new HashMap<String, List<Map<String, Object>>>();
    public long totalTables = 0;
    public long totalRows = 0;

    @Override
    public boolean handlesEntity(final String entityName) {
        return true;
    }

    @Override
    public Long getEntityWeight(final String entityName) {
        return 1L;
    }

    @Override
    public void handleEntity(final String entityName, final Map<String, Object> attributes)
            throws ParseException, AbortImportException {
        List<Map<String, Object>> table = tables.get(entityName);
        if (table == null) {
            table = new ArrayList<Map<String, Object>>();
            tables.put(entityName, table);
        }
        table.add(attributes);
        totalRows++;
    }

    @Override
    public void startDocument() {
    }

    @Override
    public void endDocument() {
    }

    @Override
    public void setBackupProject(final BackupProject backupProject) {
    }

    @Override
    public void setBackupSystemInformation(final BackupSystemInformation backupSystemInformation) {
    }

    @Override
    public void setProjectImportMapper(final ProjectImportMapper projectImportMapper) {
    }

    @Override
    public void setProjectImportResults(@Nullable final ProjectImportResults projectImportResults) {
    }

    @Override
    public void endTable(final String tableName) {
        totalTables++;
    }
}
