package com.atlassian.jira;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class JiraUnitTestProperties {
    public static final Path UNIT_TESTS_PATH = Paths.get("jira-components/jira-tests-parent/jira-tests-unit");

    /**
     * Tries to find out where is project root. The cwd is different for maven and for IDE.
     *
     * @return the project root path
     */
    public static Path getProjectRootPath() {
        final Path rootPath;
        final Path cwd = Paths.get(System.getProperty("user.dir"));
        final File distributionPomFile = cwd.resolve("jira-distribution/pom.xml").toFile();

        if (distributionPomFile.exists()) {
            rootPath = cwd;
        } else {
            // Where I am? Maybe in unit-tests?
            if (cwd.endsWith(UNIT_TESTS_PATH)) {
                final Path goUpToRoot = Stream
                        .generate(() -> Paths.get(".."))
                        .limit(UNIT_TESTS_PATH.getNameCount())
                        .reduce(Paths.get(""), Path::resolve);

                rootPath = cwd.resolve(goUpToRoot).normalize();
            } else {
                // Not in unit tests - no idea where - project structure has changed?
                throw new AssertionError("I'm lost, I don't know where I am! Please fix me. "
                        + "The current working directory is: " + cwd);
            }
        }

        return rootPath;
    }
}
