package com.atlassian.jira.bc.project;

import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.bc.JiraServiceContextImpl;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.config.properties.JiraProperties;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.ClearStatics;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.mock.MockApplicationProperties;
import com.atlassian.jira.project.AssigneeTypes;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.project.template.ProjectTemplateKey;
import com.atlassian.jira.project.template.ProjectTemplateManager;
import com.atlassian.jira.project.type.ProjectTypeKey;
import com.atlassian.jira.project.util.ProjectKeyStore;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.util.MockUserManager;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.NoopI18nFactory;
import com.atlassian.jira.util.SimpleErrorCollection;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.junit.rules.TestRule;
import org.mockito.Mock;

import java.util.Optional;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestDefaultProjectServiceValidation {
    @Rule
    public TestRule testRUle = RuleChain.emptyRuleChain()
            .around(new ClearStatics())
            .around(MockitoMocksInContainer.forTest(this));
    @Mock
    private ProjectManager projectManager;
    @Mock
    private ProjectTypeValidator projectTypeValidator;
    @Mock
    private ProjectTemplateManager projectTemplateManager;
    @Mock
    @AvailableInContainer
    private JiraProperties jiraProperties;
    @AvailableInContainer
    private final I18nHelper.BeanFactory i18nFactory = new NoopI18nFactory();
    @AvailableInContainer
    private final ApplicationProperties applicationProperties = new MockApplicationProperties();

    private ProjectService projectService;
    private ErrorCollection errorCollection;
    private static final ApplicationUser TEST_USER = new MockApplicationUser("test user");
    private static final Long ASSIGNEETYPE_PROJECTLEAD = AssigneeTypes.PROJECT_LEAD;
    private JiraServiceContext serviceContext;

    @Before
    public void setUp() throws Exception {
        when(jiraProperties.getProperty("os.name")).thenReturn("other");
        when(projectTypeValidator.isValid(any(ApplicationUser.class), any(ProjectTypeKey.class))).thenReturn(true);

        errorCollection = new SimpleErrorCollection();
        serviceContext = constructServiceContext(errorCollection);

        projectService = createProjectService(projectManager, applicationProperties, i18nFactory, projectTypeValidator, projectTemplateManager);
    }

    @Test
    public void testIsValidRequiredProjectDataHappyPath() {
        boolean isProjectDataValid = projectService.isValidRequiredProjectData(
                serviceContext,
                new ProjectCreationData.Builder().withName("project").withKey("PRJ").withLead(TEST_USER).build()
        );

        assertTrue("Expected validation to pass", isProjectDataValid);
        assertFalse("Expected not errors", errorCollection.hasAnyErrors());
    }

    @Test
    public void testIsValidRequiredProjectDataNameEmpty() {
        boolean isProjectDataValid = projectService.isValidRequiredProjectData(
                serviceContext,
                new ProjectCreationData.Builder().withName("").withKey("PRJ").withLead(TEST_USER).build()
        );

        assertFalse("Expected validation to fail", isProjectDataValid);
        assertErrorPresent("projectName", "admin.errors.must.specify.a.valid.project.name{[]}");
    }

    @Test
    public void testIsValidRequiredProjectDataNullName() {
        boolean isProjectDataValid = projectService.isValidRequiredProjectData(
                serviceContext,
                new ProjectCreationData.Builder().withKey("PRJ").withLead(TEST_USER).build()
        );

        assertFalse("Expected validation to fail", isProjectDataValid);
        assertErrorPresent("projectName", "admin.errors.must.specify.a.valid.project.name{[]}");
    }

    @Test
    public void testIsValidRequiredProjectDataDuplicateName() {
        when(projectManager.getProjectObjByName("project")).thenReturn(new MockProject());

        boolean isProjectDataValid = projectService.isValidRequiredProjectData(
                serviceContext,
                new ProjectCreationData.Builder().withName("project").withKey("PRJ").withLead(TEST_USER).build()
        );

        assertFalse("Expected validation to fail", isProjectDataValid);
        assertErrorPresent("projectName", "admin.errors.project.with.that.name.already.exists{[]}");
    }

    @Test
    public void testIsValidRequiredProjectDataKeyIsEmptyString() {
        boolean isProjectDataValid = projectService.isValidRequiredProjectData(
                serviceContext,
                new ProjectCreationData.Builder().withName("project").withKey("").withLead(TEST_USER).build()
        );

        assertFalse("Expected validation to fail", isProjectDataValid);
        assertErrorPresent("projectKey", "admin.errors.must.specify.unique.project.key{[]}");
    }

    @Test
    public void testIsValidRequiredProjectDataKeyIsNull() {
        boolean isProjectDataValid = projectService.isValidRequiredProjectData(
                serviceContext,
                new ProjectCreationData.Builder().withName("project").withLead(TEST_USER).build()
        );

        assertFalse("Expected validation to fail", isProjectDataValid);
        assertErrorPresent("projectKey", "admin.errors.must.specify.unique.project.key{[]}");
    }

    @Test
    public void testIsValidRequiredProjectDataInvalidKeyDefaultPattern() {
        boolean isProjectDataValid = projectService.isValidRequiredProjectData(
                serviceContext,
                new ProjectCreationData.Builder().withName("project").withKey("!NV4L!D K3Y").withLead(TEST_USER).build()
        );

        assertFalse("Expected validation to fail", isProjectDataValid);
        assertErrorPresent("projectKey", "admin.errors.must.specify.unique.project.key{[]}");
    }

    @Test
    public void testIsValidRequiredProjectDataInvalidKeyCustomPattern() {
        applicationProperties.setText(APKeys.JIRA_PROJECTKEY_WARNING, "Test Project Key Regex Warning Message");
        applicationProperties.setText(APKeys.JIRA_PROJECTKEY_PATTERN, "wrong");

        boolean isProjectDataValid = projectService.isValidRequiredProjectData(
                serviceContext,
                new ProjectCreationData.Builder().withName("project").withKey("AA").withLead(TEST_USER).build()
        );

        assertFalse("Expected validation to fail", isProjectDataValid);
        assertErrorPresent("projectKey", "Test Project Key Regex Warning Message{[]}");
    }

    @Test
    public void testIsValidRequiredProjectDataInvalidKeyReservedWord() {
        when(jiraProperties.getProperty("os.name")).thenReturn("windows");
        applicationProperties.setString(APKeys.JIRA_PROJECTKEY_RESERVEDWORDS_LIST, "CON CORD");

        boolean isProjectDataValid = projectService.isValidRequiredProjectData(
                serviceContext,
                new ProjectCreationData.Builder().withName("project").withKey("CON").withLead(TEST_USER).build()
        );

        assertFalse("Expected validation to fail", isProjectDataValid);
        assertErrorPresent("projectKey", "admin.errors.project.keyword.invalid{[]}");
    }

    @Test
    public void testIsValidRequiredProjectDataDuplicateKey() {
        when(projectManager.getProjectObjByKey("PRJ")).thenReturn(new MockProject());

        boolean isProjectDataValid = projectService.isValidRequiredProjectData(
                serviceContext,
                new ProjectCreationData.Builder().withName("project").withKey("PRJ").withLead(TEST_USER).build()
        );

        assertFalse("Expected validation to fail", isProjectDataValid);
        assertErrorPresent("projectKey", "admin.errors.project.with.that.key.already.exists{[null]}");
    }

    @Test
    public void testIsValidRequiredProjectDataNoLeadEmptyNull() {
        boolean isProjectDataValid = projectService.isValidRequiredProjectData(
                serviceContext,
                new ProjectCreationData.Builder().withName("project").withKey("PRJ").withLead(null).build()
        );

        assertFalse("Expected validation to fail", isProjectDataValid);
        assertErrorPresent("projectLead", "admin.errors.must.specify.project.lead{[]}");
    }

    @Test
    public void testIsValidAllProjectDataHappyPath() {
        boolean isProjectDataValid = projectService.isValidAllProjectData(
                serviceContext,
                validProjectCreationData()
        );

        assertTrue("Expected validation to pass", isProjectDataValid);
        assertFalse("Unexpected errors", errorCollection.hasAnyErrors());
    }

    @Test
    public void testIsValidAllProjectDataHappyPathNonRequiredFieldsLeftBlank() {
        boolean isProjectDataValid = projectService.isValidAllProjectData(
                serviceContext,
                new ProjectCreationData.Builder().withName("project").withKey("PRJ").withLead(TEST_USER).build()
        );

        assertTrue("Expected validation to pass", isProjectDataValid);
        assertFalse("Unexpected errors", errorCollection.hasAnyErrors());
    }

    @Test
    public void testIsValidAllProjectDataInvalidURL() {
        boolean isProjectDataValid = projectService.isValidAllProjectData(
                serviceContext,
                new ProjectCreationData.Builder().withName("project").withKey("PRJ").withLead(TEST_USER).withUrl("not - a - valid - url").build()
        );

        assertFalse("Expected validation to fail", isProjectDataValid);
        assertErrorPresent("projectUrl", "admin.errors.url.specified.is.not.valid{[]}");
    }

    @Test
    public void testIsValidAllProjectDataInvalidAssigneeType() {
        boolean isProjectDataValid = projectService.isValidAllProjectData(
                serviceContext,
                new ProjectCreationData.Builder().withName("project").withKey("PRJ").withLead(TEST_USER).withAssigneeType(500L).build()
        );

        assertFalse("Expected validation to fail", isProjectDataValid);
        assertErrorMessageWasAdded("admin.errors.invalid.default.assignee{[]}");
    }

    @Test
    public void isValidRequiredProjectDataReturnsErrorForTheProjectTypeIfProjectTypeValidatorReturnsFalse() {
        String projectType = "type";
        when(projectTypeValidator.isValid(serviceContext.getLoggedInApplicationUser(), new ProjectTypeKey(projectType))).thenReturn(false);

        boolean isProjectDataValid = projectService.isValidRequiredProjectData(
                serviceContext,
                new ProjectCreationData.Builder().withName("project").withKey("PRJ").withType(projectType).withLead(TEST_USER).build()
        );

        assertFalse("Expected validation to fail", isProjectDataValid);
        assertErrorPresent(ProjectService.PROJECT_TYPE, "admin.errors.invalid.project.type{[]}");
        assertReasonWasAdded(ErrorCollection.Reason.VALIDATION_FAILED);
    }

    @Test
    public void isValidAllProjectDataDoesNotFailIfTheProjectTemplateKeyContainsANull() {
        ProjectTemplateKey emptyKey = new ProjectTemplateKey(null);
        when(projectTemplateManager.getProjectTemplate(emptyKey)).thenReturn(Optional.empty());

        boolean isProjectDataValid = projectService.isValidAllProjectData(
                serviceContext,
                new ProjectCreationData.Builder().from(validProjectCreationData()).withProjectTemplateKey(emptyKey.getKey()).build()
        );

        assertTrue("Expected validation to pass", isProjectDataValid);
        assertFalse("Unexpected errors", errorCollection.hasAnyErrors());
    }

    @Test
    public void isValidAllProjectDataReturnsErrorIfATemplateThatDoesNotExistIsSpecified() {
        ProjectTemplateKey nonExistentKey = new ProjectTemplateKey("non-existent-template");
        when(projectTemplateManager.getProjectTemplate(nonExistentKey)).thenReturn(Optional.empty());

        boolean isProjectDataValid = projectService.isValidAllProjectData(
                serviceContext,
                new ProjectCreationData.Builder().from(validProjectCreationData()).withProjectTemplateKey(nonExistentKey.getKey()).build()
        );

        assertFalse("Expected validation to fail", isProjectDataValid);
        assertErrorMessageWasAdded("admin.errors.invalid.project.template{[]}");
    }

    private ProjectCreationData validProjectCreationData() {
        return new ProjectCreationData.Builder().withName("project").withKey("PRJ").withLead(TEST_USER).withUrl("http://www.example.com").withAssigneeType(ASSIGNEETYPE_PROJECTLEAD).build();
    }

    private JiraServiceContext constructServiceContext(final ErrorCollection errorCollection) {
        return new JiraServiceContextImpl((ApplicationUser) null, errorCollection);
    }

    private void assertErrorPresent(final String errorKey, final String value) {
        assertTrue("Expected some errors", errorCollection.hasAnyErrors());
        assertThat(errorCollection.getErrors().keySet(), Matchers.<String>hasSize(1));
        assertThat(errorCollection.getErrors(), Matchers.hasEntry(errorKey, value));
    }

    private void assertErrorMessageWasAdded(final String expectedError) {
        assertTrue("Expected some errors", errorCollection.hasAnyErrors());
        assertThat(errorCollection.getErrorMessages(), Matchers.containsInAnyOrder(expectedError));
    }

    private void assertReasonWasAdded(final ErrorCollection.Reason reason) {
        assertThat(errorCollection.getReasons().contains(reason), is(true));
    }

    private UserManager userManager() {
        final MockUserManager userManager = new MockUserManager();
        userManager.addUser(TEST_USER);
        return userManager;
    }

    private ProjectKeyStore projectKeyStoreReturningNull() {
        ProjectKeyStore projectKeyStore = mock(ProjectKeyStore.class);
        when(projectKeyStore.getProjectId(anyString())).thenReturn(null);
        return projectKeyStore;
    }

    private DefaultProjectService createProjectService(
            ProjectManager projectManager,
            ApplicationProperties applicationProperties,
            I18nHelper.BeanFactory i18nFactory,
            ProjectTypeValidator projectTypeValidator,
            ProjectTemplateManager projectTemplateManager) {
        return new DefaultProjectService(
                mock(JiraAuthenticationContext.class),
                projectManager,
                applicationProperties,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                i18nFactory,
                null,
                userManager(),
                null,
                projectKeyStoreReturningNull(),
                projectTypeValidator,
                null,
                null,
                null,
                projectTemplateManager,
                null,
                null,
                null);
    }
}
