package com.atlassian.jira.upgrade;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

/**
 * A mock upgrade task for testing purposes.
 * <p>
 * To use this Class to mock upgrade tasks, you should set a range of build numbers for the tasks to reply before
 * invoking the class to be tested, using {@link MockUpgradeTask#setBuildNumbers(Integer...)}
 * Because each task is instantiated dynamically by JiraUtils.loadComponent the same class would normally
 * always return the same build number so this bit of hackery allows us to reuse the class to return multiples.
 *
 * @since v6.4
 */
public class MockUpgradeTask extends AbstractUpgradeTask {
    private static Queue<Integer> buildNumbers = new LinkedList<Integer>();

    private final int buildNumber;

    public static void setBuildNumbers(Integer... bNs) {
        buildNumbers.clear();
        buildNumbers.addAll(Arrays.asList(bNs));
    }

    public MockUpgradeTask() {
        super();
        buildNumber = assignBuildNumber();
    }

    @Override
    public int getBuildNumber() {
        return buildNumber;
    }

    public int assignBuildNumber() {
        if (buildNumbers.isEmpty()) {
            return 7;
        }
        return buildNumbers.poll();
    }

    @Override
    public String getShortDescription() {
        return "test";
    }

    @Override
    public void doUpgrade(final boolean setupMode) throws Exception {

    }

    @Override
    public ScheduleOption getScheduleOption() {
        return ScheduleOption.AFTER_JIRA_STARTED;
    }

    @Override
    public boolean isDowngradeTaskRequired() {
        return true;
    }
}
