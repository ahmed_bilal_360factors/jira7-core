package com.atlassian.jira.upgrade.tasks;

import com.atlassian.jira.avatar.Avatar;
import com.atlassian.jira.avatar.AvatarImpl;
import com.atlassian.jira.avatar.AvatarManager;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.plugin.icon.DefaultSystemIconImageProvider;
import com.atlassian.jira.icon.IconType;
import com.atlassian.jira.plugin.icon.UserIconTypePolicy;
import com.google.common.collect.ImmutableList;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

public class TestUpgradeTask_Build554 {
    @Test
    public void testDoUpgrade() throws Exception {
        final List<String> avatars = ImmutableList.of("angel.png", "businessman.png", "businessman2.png", "devil.png", "doctor.png",
                "dude1.png", "dude2.png", "dude3.png", "dude4.png", "dude5.png",
                "ghost.png", "security_agent.png",
                "user1.png", "user2.png", "user3.png", "user_headphones.png", "userprofile_silhouette.png");

        final ApplicationProperties mockApplicationProperties = mock(ApplicationProperties.class);
        final AvatarManager mockAvatarManager = mock(AvatarManager.class);
        final Avatar mockAvatar = mock(Avatar.class);
        final Avatar mockCreatedAvatar = mock(Avatar.class);

        when(mockCreatedAvatar.getId()).thenReturn(9999L);
        when(mockAvatar.getId()).thenReturn(12323L);

        List<Avatar> systemAvatars = new ArrayList<Avatar>();
        systemAvatars.add(mockAvatar);
        when(mockAvatarManager.getAllSystemAvatars(IconType.USER_ICON_TYPE)).thenReturn(systemAvatars);

        UserIconTypePolicy userIconTypePolicy = mock(UserIconTypePolicy.class);
        DefaultSystemIconImageProvider imageProvider = mock(DefaultSystemIconImageProvider.class);

        avatars.stream().forEach(avatar -> when(mockAvatarManager.create(AvatarImpl.createSystemAvatar(avatar, "image/png", IconType.USER_ICON_TYPE))).thenReturn(mockCreatedAvatar));

        UpgradeTask_Build554 upgradeTask = new UpgradeTask_Build554(mockAvatarManager, mockApplicationProperties);
        upgradeTask.doUpgrade(false);

        verify(mockApplicationProperties).setString("jira.avatar.user.default.id", 9999L + "");
        avatars.stream().forEach(avatar -> verify(mockAvatarManager).create(AvatarImpl.createSystemAvatar(avatar, "image/png", IconType.USER_ICON_TYPE)));
        verify(mockAvatarManager).delete(12323L);
        verify(mockAvatarManager).getAllSystemAvatars(IconType.USER_ICON_TYPE);
        verifyNoMoreInteractions(mockAvatarManager);
    }

    @Test
    public void testMetaData() {
        UpgradeTask_Build554 upgradeTask = new UpgradeTask_Build554(null, null);
        assertEquals(554, upgradeTask.getBuildNumber());
        assertEquals("Creates system user avatars.", upgradeTask.getShortDescription());
    }
}
