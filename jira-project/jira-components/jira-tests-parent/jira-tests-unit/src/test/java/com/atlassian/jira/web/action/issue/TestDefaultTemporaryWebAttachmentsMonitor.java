package com.atlassian.jira.web.action.issue;

import com.atlassian.fugue.Option;
import com.atlassian.fugue.Unit;
import com.atlassian.jira.issue.attachment.StreamAttachmentStore;
import com.atlassian.jira.issue.attachment.TemporaryAttachmentId;
import com.atlassian.jira.issue.attachment.TemporaryAttachmentMonitorStore;
import com.atlassian.jira.issue.attachment.TemporaryWebAttachment;
import com.atlassian.jira.junit.rules.InitMockitoMocks;
import com.atlassian.jira.junit.rules.Log4jLogger;
import com.atlassian.jira.matchers.OptionMatchers;
import com.atlassian.scheduler.SchedulerService;
import com.atlassian.util.concurrent.Promises;
import org.hamcrest.Matchers;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.TestRule;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import static com.atlassian.jira.matchers.OptionMatchers.none;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestDefaultTemporaryWebAttachmentsMonitor {
    public static final String FORM_TOKEN = "formToken";
    public static final String ATTACHMENT_ID = "greatId";
    @Rule
    public TestRule initMocks = new InitMockitoMocks(this);
    @Rule
    public Log4jLogger logger = new Log4jLogger();
    @Rule
    public ExpectedException expectedException = ExpectedException.none();
    @Mock
    private StreamAttachmentStore attachmentStore;
    @Mock
    private TemporaryWebAttachment temporaryWebAttachment;
    @Mock
    private SchedulerService schedulerService;

    private TemporaryAttachmentMonitorStore temporaryAttachmentMonitorStore;

    private DefaultTemporaryWebAttachmentsMonitor webAttachmentsMonitor;

    @Before
    public void setUp() throws Exception {

        // create a fake implementation since its so simple
        temporaryAttachmentMonitorStore = new TemporaryAttachmentMonitorStore() {
            Map<String, TemporaryWebAttachment> mapOfIdToAttachment = new HashMap<>();

            @Override
            public Option<TemporaryWebAttachment> getById(TemporaryAttachmentId temporaryAttachmentId) {
                return Option.option(mapOfIdToAttachment.get(temporaryAttachmentId.toStringId()));
            }

            @Override
            public Collection<TemporaryWebAttachment> getByFormToken(String formToken) {
                return mapOfIdToAttachment.values().stream().filter(twa -> twa.getFormToken().equals(formToken)).collect(Collectors.toList());
            }

            @Override
            public Option<TemporaryWebAttachment> removeById(TemporaryAttachmentId temporaryAttachmentId) {
                Option<TemporaryWebAttachment> tempAttachment = getById(temporaryAttachmentId);
                this.mapOfIdToAttachment.remove(temporaryAttachmentId.toStringId());
                return tempAttachment;
            }

            @Override
            public boolean putIfAbsent(TemporaryWebAttachment temporaryWebAttachment) {
                return mapOfIdToAttachment.putIfAbsent(temporaryWebAttachment.getStringId(), temporaryWebAttachment) != null;
            }

            @Override
            public long removeOlderThan(DateTime dateTime) {
                throw new UnsupportedOperationException("Not implemented");
            }
        };
        webAttachmentsMonitor = new DefaultTemporaryWebAttachmentsMonitor(attachmentStore, temporaryAttachmentMonitorStore, schedulerService);
    }

    @Test
    public void shouldNotReturnAttachmentWhenNotDefinedById() throws Exception {
        //having
        //when
        final Option<TemporaryWebAttachment> tempWebAttachment = webAttachmentsMonitor.getById("some.id");
        //then
        assertThat("Temporary attachment should not be returned", tempWebAttachment, none());
    }

    @Test
    public void shouldThrowNPEWhenAttachmentIsNullInAdd() {
        //having
        //then
        //noinspection unchecked
        expectedException.expect(NullPointerException.class);
        expectedException.expectMessage("temporaryAttachment");
        expectedException.expectCause(nullValue(Throwable.class));

        //when
        //noinspection ConstantConditions
        webAttachmentsMonitor.add(null);

    }

    @Test
    public void shouldThrowNPEWhenAttachmentIdIsNullInAdd() {
        //having
        //then
        //noinspection unchecked
        expectedException.expect(NullPointerException.class);
        expectedException.expectMessage("temporaryAttachment");

        //when
        webAttachmentsMonitor.add(null);

    }

    @Test
    public void shouldThrowIllegalArgExWhenAddingDuplicatedAttachmentId() {
        //having
        addAttachmentWithIdAndToken(ATTACHMENT_ID, FORM_TOKEN);
        when(temporaryWebAttachment.getStringId()).thenReturn(ATTACHMENT_ID);

        //then
        //noinspection unchecked
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Temporary attachment with id='" + ATTACHMENT_ID + "' already in monitor");
        expectedException.expectCause(nullValue(Throwable.class));

        //when
        webAttachmentsMonitor.add(temporaryWebAttachment);

    }

    @Test
    public void shouldReturnAttachmentWhenPreviouslyInsertedById() throws Exception {
        //having
        final String id = "greatId";
        when(temporaryWebAttachment.getStringId()).thenReturn(id);
        webAttachmentsMonitor.add(temporaryWebAttachment);

        //when
        final Option<TemporaryWebAttachment> tempAttachmentById = webAttachmentsMonitor.getById(id);

        //then
        //noinspection RedundantTypeArguments
        assertThat("should return the same object that was inserted", tempAttachmentById,
                OptionMatchers.<TemporaryWebAttachment>some(sameInstance(temporaryWebAttachment)));
    }

    @Test
    public void shouldNotReturnAttachmentIfPreviouslyRemovedById() throws Exception {
        //having
        when(temporaryWebAttachment.getStringId()).thenReturn(ATTACHMENT_ID);
        webAttachmentsMonitor.add(temporaryWebAttachment);
        webAttachmentsMonitor.removeById(ATTACHMENT_ID);

        //when
        final Option<TemporaryWebAttachment> tempAttachmentById = webAttachmentsMonitor.getById(ATTACHMENT_ID);

        //then
        assertThat("temporary attachment should not be returned", tempAttachmentById, none());
    }

    @Test
    public void shouldNotAttemptToRemoveAttachmentFromStoreWhenRemovedById() throws Exception {
        //having
        when(temporaryWebAttachment.getStringId()).thenReturn(ATTACHMENT_ID);
        webAttachmentsMonitor.add(temporaryWebAttachment);

        //when
        webAttachmentsMonitor.removeById(ATTACHMENT_ID);

        //then
        verify(attachmentStore, never()).deleteTemporaryAttachment(Mockito.<TemporaryAttachmentId>any());
    }

    @Test
    public void shouldLogExceptionWhenRemovingAttachmentFromStoreFailed() throws Exception {
        //having
        final TemporaryAttachmentId temporaryAttachmentId = addAttachmentWithIdAndToken(ATTACHMENT_ID, FORM_TOKEN).getTemporaryAttachmentId();
        when(attachmentStore.deleteTemporaryAttachment(temporaryAttachmentId)).thenReturn(Promises.<Unit>rejected(new Exception("serious bug")));

        //when
        webAttachmentsMonitor.cleanByFormToken(FORM_TOKEN);

        //then
        assertThat(webAttachmentsMonitor.getByFormToken(FORM_TOKEN), Matchers.<TemporaryWebAttachment>empty());
    }

    @Test
    public void shouldRemoveAllAttachmentsOnCleanByFormToken() {
        //having
        final TemporaryAttachmentId temporaryAttachmentId1 = addAttachmentWithIdAndToken("id1", FORM_TOKEN).getTemporaryAttachmentId();
        final TemporaryAttachmentId temporaryAttachmentId2 = addAttachmentWithIdAndToken("id2", FORM_TOKEN).getTemporaryAttachmentId();
        final TemporaryAttachmentId temporaryAttachmentId3 = addAttachmentWithIdAndToken("id3", "some other token").getTemporaryAttachmentId();
        when(attachmentStore.deleteTemporaryAttachment(any(TemporaryAttachmentId.class)))
                .thenReturn(Promises.promise(Unit.VALUE));
        //when
        webAttachmentsMonitor.cleanByFormToken(FORM_TOKEN);

        //then
        verify(attachmentStore).deleteTemporaryAttachment(temporaryAttachmentId1);
        verify(attachmentStore).deleteTemporaryAttachment(temporaryAttachmentId2);
        verify(attachmentStore, never()).deleteTemporaryAttachment(temporaryAttachmentId3);
    }

    @Test
    public void shouldReturnAttachmentsFilteredByFormToken() {
        //having
        final TemporaryWebAttachment temporaryWebAttachment1 = addAttachmentWithIdAndToken("id1", FORM_TOKEN);
        final TemporaryWebAttachment temporaryWebAttachment2 = addAttachmentWithIdAndToken("id2", FORM_TOKEN);
        addAttachmentWithIdAndToken("id3", "some other token");

        //when
        final Iterable<TemporaryWebAttachment> temporaryWebAttachments = webAttachmentsMonitor.getByFormToken(FORM_TOKEN);

        //then
        assertThat(temporaryWebAttachments, containsInAnyOrder(temporaryWebAttachment1, temporaryWebAttachment2));
    }

    @Test
    public void shouldReturnEmptyCollectionWhenNoAttachmentsMatchedByFormToken() {
        //having
        addAttachmentWithIdAndToken("id1", FORM_TOKEN);
        addAttachmentWithIdAndToken("id2", FORM_TOKEN);

        //when
        final Collection<TemporaryWebAttachment> temporaryWebAttachments = webAttachmentsMonitor.getByFormToken(FORM_TOKEN + "decoy");

        //then
        assertThat(temporaryWebAttachments, Matchers.<TemporaryWebAttachment>empty());
    }

    private TemporaryWebAttachment addAttachmentWithIdAndToken(final String attachmentId, final String formToken) {
        final TemporaryAttachmentId temporaryAttachmentId = TemporaryAttachmentId.fromString(attachmentId);
        final TemporaryWebAttachment temporaryWebAttachment = mock(TemporaryWebAttachment.class);
        when(temporaryWebAttachment.getStringId()).thenReturn(attachmentId);
        when(temporaryWebAttachment.getFormToken()).thenReturn(formToken);
        when(temporaryWebAttachment.getTemporaryAttachmentId()).thenReturn(temporaryAttachmentId);

        webAttachmentsMonitor.add(temporaryWebAttachment);
        return temporaryWebAttachment;
    }
}