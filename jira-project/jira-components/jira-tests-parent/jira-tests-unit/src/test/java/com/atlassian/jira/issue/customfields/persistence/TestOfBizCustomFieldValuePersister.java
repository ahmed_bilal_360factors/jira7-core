package com.atlassian.jira.issue.customfields.persistence;

import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.mock.ofbiz.MockGenericValue;
import com.atlassian.jira.mock.ofbiz.MockOfBizDelegator;
import com.atlassian.jira.ofbiz.FieldMap;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.ofbiz.core.entity.GenericValue;

import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyMap;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 */
public class TestOfBizCustomFieldValuePersister {
    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Rule
    public MockitoRule mockitoInitializerRule = MockitoJUnit.rule();

    @Mock
    private OfBizDelegator delegator;

    @InjectMocks
    private OfBizCustomFieldValuePersister persister;

    @Test
    public void testRemoveAllValuesNullId() throws Exception {
        OfBizCustomFieldValuePersister persister = new OfBizCustomFieldValuePersister(new MockOfBizDelegator());

        exception.expect(IllegalArgumentException.class);
        persister.removeAllValues((String) null);
    }

    @Test
    public void testRemoveAllValues() throws Exception {
        final GenericValue mockGV1 = new MockGenericValue("CustomFieldValue", FieldMap.build("issue", -1L));
        final GenericValue mockGV2 = new MockGenericValue("CustomFieldValue", FieldMap.build("issue", 10200L));
        final GenericValue mockGV3 = new MockGenericValue("CustomFieldValue", FieldMap.build("issue", 10300L));

        when(delegator.findByAnd("CustomFieldValue", FieldMap.build("customfield", 10000L))).thenReturn(
                Lists.newArrayList(mockGV1, mockGV2, mockGV3));

        final Set<Long> issueIdsRemoved = persister.removeAllValues("customfield_10000");
        assertTrue(issueIdsRemoved.contains(10200L));
        assertTrue(issueIdsRemoved.contains(10300L));
        assertFalse(issueIdsRemoved.contains(-1L));
        assertEquals(2, issueIdsRemoved.size());
    }

    @Test
    public void testRemoveAllValuesDeprecated() {
        final MockGenericValue genericValue = new MockGenericValue(OfBizCustomFieldValuePersister.TABLE_CUSTOMFIELD_VALUE, ImmutableMap.of(OfBizCustomFieldValuePersister.ENTITY_ISSUE_ID, 1l));

        when(delegator.findByAnd(Mockito.eq(OfBizCustomFieldValuePersister.TABLE_CUSTOMFIELD_VALUE),
                Mockito.anyMap())).thenReturn(Lists.newArrayList(genericValue));

        final String valuesToDelete = "1000L";
        persister.removeAllValues(valuesToDelete);
        verify(delegator).removeAll(Mockito.<List<GenericValue>>argThat((Matcher) Matchers.hasItems(genericValue)));
    }

    @Test
    public void oldValuesAreNotRemovedIfUpdateInTheDatabaseThrowsRuntimeException() {
        // first make sure deleting old values is done with the `removeAll` method
        persister.updateValues(mock(CustomField.class), 1L, PersistenceFieldType.TYPE_LIMITED_TEXT, ImmutableList.of("a"));
        verify(delegator, times(1)).removeAll(any());
        reset(delegator);

        // assume DB throws an exception when trying to insert new value
        when(delegator.createValue(anyString(), anyMap())).thenThrow(DbException.class);

        try {
            persister.updateValues(mock(CustomField.class), 1L, PersistenceFieldType.TYPE_LIMITED_TEXT, ImmutableList.of("a"));
        } catch (DbException ex) {
            // `exception.expect(DbException.class)` prevents verify to return correct result, so let's do this the ugly way
        }

        try {
            persister.updateValues(mock(CustomField.class), 1L, PersistenceFieldType.TYPE_LIMITED_TEXT, ImmutableList.of("a"), "parent");
        } catch (DbException ex) {
        }

        // verify that old values were not removed
        verify(delegator, times(0)).removeAll(any());
    }

    private static class DbException extends RuntimeException {
    }
}
