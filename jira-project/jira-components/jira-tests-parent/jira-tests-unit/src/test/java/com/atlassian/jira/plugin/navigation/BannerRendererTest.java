package com.atlassian.jira.plugin.navigation;

import com.atlassian.jira.mock.plugin.MockModuleDescriptor;
import com.atlassian.jira.mock.plugin.MockPlugin;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.web.Condition;
import com.atlassian.plugin.web.ContextProvider;
import com.atlassian.plugin.web.WebFragmentHelper;
import com.atlassian.plugin.web.WebInterfaceManager;
import com.atlassian.plugin.web.descriptors.WebItemModuleDescriptor;
import com.atlassian.plugin.web.descriptors.WebPanelModuleDescriptor;
import com.atlassian.plugin.web.descriptors.WebSectionModuleDescriptor;
import com.atlassian.plugin.web.model.WebLabel;
import com.atlassian.plugin.web.model.WebPanel;
import com.atlassian.plugin.web.model.WebParam;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;
import org.apache.commons.lang3.NotImplementedException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class BannerRendererTest {
    private static final String BANNER_LOCATION = "jira-banner";

    private MockWebInterfaceManager mockWebInterfaceManager = new MockWebInterfaceManager();
    private BannerRenderer bannerRenderer;

    @Before
    public void start() {
        bannerRenderer = new BannerRenderer(mockWebInterfaceManager, HashMap::new);
    }

    @Test
    public void rendersFirstBanner() {
        mockWebInterfaceManager
                .addWebPanel(BANNER_LOCATION, "one")
                .addWebPanel(BANNER_LOCATION, "two")
                .addWebPanel("randomLocation", "three");

        assertThat(render(), equalTo("one"));
    }

    @Test
    public void ignoresExceptions() {
        mockWebInterfaceManager
                .addWebPanel(BANNER_LOCATION, new NullPointerException());

        assertThat(render(), equalTo(""));
    }

    private String render() {
        final StringWriter writer = new StringWriter();
        bannerRenderer.writeBanners(writer);
        return writer.toString();
    }

    private static class MockWebInterfaceManager implements WebInterfaceManager {
        private final MockPlugin plugin = new MockPlugin("key");
        private ListMultimap<String, WebPanelModuleDescriptor> panelDescriptors = ArrayListMultimap.create();

        @Override
        public boolean hasSectionsForLocation(final String s) {
            throw new NotImplementedException("Not yet implemented in this mock");
        }

        @Override
        public List<WebSectionModuleDescriptor> getSections(final String s) {
            throw new NotImplementedException("Not yet implemented in this mock");
        }

        @Override
        public List<WebSectionModuleDescriptor> getDisplayableSections(final String s, final Map<String, Object> map) {
            throw new NotImplementedException("Not yet implemented in this mock");
        }

        @Override
        public List<WebItemModuleDescriptor> getItems(final String s) {
            throw new NotImplementedException("Not yet implemented in this mock");
        }

        @Override
        public List<WebItemModuleDescriptor> getDisplayableItems(final String s, final Map<String, Object> map) {
            throw new NotImplementedException("Not yet implemented in this mock");
        }

        @Override
        public List<WebPanel> getWebPanels(final String s) {
            throw new NotImplementedException("Not yet implemented in this mock");
        }

        @Override
        public List<WebPanel> getDisplayableWebPanels(final String s, final Map<String, Object> map) {
            throw new NotImplementedException("Not yet implemented in this mock");
        }

        @Override
        public List<WebPanelModuleDescriptor> getWebPanelDescriptors(final String s) {
            throw new NotImplementedException("Not yet implemented in this mock");
        }

        @Override
        public List<WebPanelModuleDescriptor> getDisplayableWebPanelDescriptors(final String s, final Map<String, Object> map) {
            return panelDescriptors.get(s);
        }

        @Override
        public void refresh() {

        }

        @Override
        public WebFragmentHelper getWebFragmentHelper() {
            throw new NotImplementedException("Not yet implemented in this mock");
        }

        private MockWebInterfaceManager addWebPanel(String location, String data) {
            return addWebPanel(location, new MockWebPanel(data));

        }

        private MockWebInterfaceManager addWebPanel(String location, Exception e) {
            final WebPanel panel = Mockito.mock(WebPanel.class);
            try {
                Mockito.doThrow(e).when(panel).writeHtml(Mockito.any(), Mockito.any());
            } catch (IOException wontHappen) {
                throw new RuntimeException(wontHappen);
            }

            return addWebPanel(location, panel);
        }

        private MockWebInterfaceManager addWebPanel(String location, WebPanel webPanel) {
            panelDescriptors.put(location, new MockWebPanelModuleDescriptor(location, webPanel, plugin));
            return this;
        }
    }

    private static class MockWebPanelModuleDescriptor extends MockModuleDescriptor<WebPanel> implements WebPanelModuleDescriptor {
        private final WebPanel panel;
        private final String location;

        public MockWebPanelModuleDescriptor(final String location, final WebPanel panel, final Plugin plugin) {
            super(WebPanel.class, plugin, location);
            this.panel = panel;
            this.location = location;
        }

        @Override
        public WebPanel getModule() {
            return panel;
        }

        @Override
        public boolean isEnabled() {
            return true;
        }

        @Override
        public String getLocation() {
            return location;
        }

        @Override
        public int getWeight() {
            return 0;
        }

        @Override
        public WebLabel getWebLabel() {
            throw new NotImplementedException("Not yet implemented in this mock");
        }

        @Override
        public WebLabel getTooltip() {
            throw new NotImplementedException("Not yet implemented in this mock");
        }

        @Override
        public Condition getCondition() {
            throw new NotImplementedException("Not yet implemented in this mock");
        }

        @Override
        public WebParam getWebParams() {
            throw new NotImplementedException("Not yet implemented in this mock");
        }

        @Override
        public ContextProvider getContextProvider() {
            throw new NotImplementedException("Not yet implemented in this mock");
        }

        @Override
        public void enabled() {
            throw new NotImplementedException("Not yet implemented in this mock");
        }

        @Override
        public void disabled() {
            throw new NotImplementedException("Not yet implemented in this mock");
        }
    }

    private static class MockWebPanel implements WebPanel {
        private final String msg;

        private MockWebPanel(final String msg) {
            this.msg = msg;
        }

        @Override
        public String getHtml(final Map<String, Object> context) {
            throw new NotImplementedException("Not yet implemented in this mock");
        }

        @Override
        public void writeHtml(final Writer writer, final Map<String, Object> context) throws IOException {
            writer.append(msg);
        }
    }
}