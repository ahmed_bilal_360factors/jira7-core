package com.atlassian.jira.issue.customfields.impl;

import com.atlassian.jira.datetime.DateTimeFormatterFactory;
import com.atlassian.jira.imports.project.customfield.NoTransformationCustomFieldImporter;
import com.atlassian.jira.issue.customfields.converters.DatePickerConverter;
import com.atlassian.jira.issue.customfields.manager.GenericConfigManager;
import com.atlassian.jira.issue.customfields.persistence.CustomFieldValuePersister;
import com.atlassian.jira.issue.history.DateTimeFieldChangeLogHelper;
import com.atlassian.jira.issue.views.util.csv.CsvDateFormatter;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.util.DateFieldFormat;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;

/**
 * @since v3.13
 */
public class TestDateCFType {
    @Rule
    public MockitoContainer container = MockitoMocksInContainer.rule(this);
    
    @Mock
    @AvailableInContainer
    private CsvDateFormatter csvDateFormatter;

    @Test
    public void testGetProjectImporter() throws Exception {
        DateCFType dateCFType = new DateCFType(mock(CustomFieldValuePersister.class),
                mock(DatePickerConverter.class),
                mock(GenericConfigManager.class),
                mock(DateTimeFieldChangeLogHelper.class),
                mock(DateFieldFormat.class),
                mock(DateTimeFormatterFactory.class));
        assertTrue(dateCFType.getProjectImporter() instanceof NoTransformationCustomFieldImporter);
    }
}
