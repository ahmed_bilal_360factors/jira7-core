package com.atlassian.jira.config.component;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.picocontainer.PicoContainer;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

public class TestAbstractSwitchingInvocationAdaptor {
    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    PicoContainer container;

    boolean enabled = true;

    @Before
    public void setUp() {
        when(container.getComponent(EnabledFactory.class)).thenReturn(new EnabledFactory());
        when(container.getComponent(DisabledFactory.class)).thenReturn(new DisabledFactory());
        when(container.getComponent(ErrorFactory.class)).thenReturn(new ErrorFactory());
    }

    @Test
    public void callsTheRightImplementations() {
        final IntFactory component = new TestSwitcher().getComponentInstance(container);

        enabled = true;
        assertThat(component.getInt(), is(1));
        assertThat(component.getInt(), is(1));

        enabled = false;
        assertThat(component.getInt(), is(0));
        assertThat(component.getInt(), is(0));

        enabled = true;
        assertThat(component.getInt(), is(1));
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void propagatesExceptionsCorrectly() {
        new TestSwitcher(ErrorFactory.class).getComponentInstance(container).getInt();
    }


    interface IntFactory {
        int getInt();
    }

    static class EnabledFactory implements IntFactory {
        @Override
        public int getInt() {
            return 1;
        }
    }

    static class DisabledFactory implements IntFactory {
        @Override
        public int getInt() {
            return 0;
        }
    }

    static class ErrorFactory implements IntFactory {
        @Override
        public int getInt() {
            throw new ArrayIndexOutOfBoundsException(42);
        }
    }

    class TestSwitcher extends AbstractSwitchingInvocationAdaptor<IntFactory> {
        public TestSwitcher() {
            this(EnabledFactory.class);
        }

        public TestSwitcher(Class<? extends IntFactory> enabledClass) {
            super(IntFactory.class, enabledClass, DisabledFactory.class);
        }

        @Override
        protected InvocationSwitcher getInvocationSwitcher() {
            return () -> enabled;
        }
    }
}