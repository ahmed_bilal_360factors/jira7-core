package com.atlassian.jira.bc.issue;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.concurrent.BarrierFactory;
import com.atlassian.jira.config.SubTaskManager;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.event.type.EventDispatchOption;
import com.atlassian.jira.exception.CreateException;
import com.atlassian.jira.exception.UpdateException;
import com.atlassian.jira.issue.AttachmentManager;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.IssueInputParameters;
import com.atlassian.jira.issue.IssueInputParametersImpl;
import com.atlassian.jira.issue.changehistory.metadata.HistoryMetadata;
import com.atlassian.jira.issue.link.IssueLinkManager;
import com.atlassian.jira.issue.link.IssueLinkTypeManager;
import com.atlassian.jira.issue.link.RemoteIssueLinkManager;
import com.atlassian.jira.junit.rules.MockitoContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.mock.issue.MockIssue;
import com.atlassian.jira.task.TaskManager;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.jira.workflow.TransitionOptions;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;
import java.util.HashMap;

import static com.atlassian.jira.mock.Strict.strict;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

/**
 * Tests that deprecated issue services are just wrappers for replacing methods
 *
 * @since v6.4
 */
@RunWith(MockitoJUnitRunner.class)
public class TestDeprecatedDefaultIssueService {
    private static final long PARENT_ID = 111L;
    private static final long ISSUE_ID = 123L;

    private EventPublisher eventPublisher = mock(EventPublisher.class, strict());
    @Mock
    private ApplicationProperties applicationProperties;
    @Mock
    private IssueLinkTypeManager issueLinkTypeManager;
    @Mock
    private IssueLinkManager issueLinkManager;
    @Mock
    private RemoteIssueLinkManager remoteIssueLinkManager;
    @Mock
    private AttachmentManager attachmentManager;
    @Mock
    private SubTaskManager subTaskManager;
    @Mock
    private TaskManager taskManager;
    @Mock
    private BarrierFactory barrierFactory;
    @Mock
    private CustomFieldManager customFieldManager;
    @Spy
    private DefaultIssueService defaultIssueService = new DefaultIssueService(null, null, null, null, null, null, null, null, null, null, eventPublisher, null, applicationProperties, issueLinkTypeManager, issueLinkManager, remoteIssueLinkManager, attachmentManager, subTaskManager, customFieldManager, taskManager, barrierFactory);
    @Mock
    private IssueService.IssueResult issueResult;
    @Rule
    public MockitoContainer initMockitoMocks = MockitoMocksInContainer.rule(this);
    private final MockIssue mockIssue = new MockIssue(ISSUE_ID);


    @Test
    public void testGetIssueById() throws Exception {
        Long l = 12L;
        doReturn(issueResult).when(defaultIssueService).getIssue(null, l);
        assertEquals(issueResult, defaultIssueService.getIssue(null, l));
    }

    @Test
    public void testGetIssueByKey() throws Exception {
        doReturn(issueResult).when(defaultIssueService).getIssue(null, "TST-1");
        assertEquals(issueResult, defaultIssueService.getIssue(null, "TST-1"));
    }

    @Test
    public void testValidateCreate() {
        IssueInputParameters issueInputParameters = new IssueInputParametersImpl();
        final IssueService.CreateValidationResult validationResult = new IssueService.CreateValidationResult(mockIssue, new SimpleErrorCollection(), new HashMap<>(), Collections.emptyMap());

        doReturn(validationResult).when(defaultIssueService).validateCreate(null, issueInputParameters);
        assertEquals(validationResult, defaultIssueService.validateCreate(null, issueInputParameters));
    }

    @Test
    public void testValidateSubTaskCreate() {
        IssueInputParameters issueInputParameters = new IssueInputParametersImpl();
        final IssueService.CreateValidationResult validationResult = new IssueService.CreateValidationResult(mockIssue, new SimpleErrorCollection(), new HashMap<>(), Collections.emptyMap());

        doReturn(validationResult).when(defaultIssueService).validateSubTaskCreate(null, PARENT_ID, issueInputParameters);
        assertEquals(validationResult, defaultIssueService.validateSubTaskCreate(null, PARENT_ID, issueInputParameters));
    }

    @Test
    public void testCreate() throws CreateException {
        final IssueService.CreateValidationResult validationResult = new IssueService.CreateValidationResult(mockIssue, new SimpleErrorCollection(), new HashMap<>(), Collections.emptyMap());

        doReturn(issueResult).when(defaultIssueService).create(null, validationResult);
        assertEquals(issueResult, defaultIssueService.create(null, validationResult));
    }

    @Test
    public void testValidateUpdate() {
        IssueInputParameters issueInputParameters = new IssueInputParametersImpl();
        final IssueService.UpdateValidationResult validationResult = new IssueService.UpdateValidationResult(mockIssue, new SimpleErrorCollection(), Collections.emptyMap());

        doReturn(validationResult).when(defaultIssueService).validateUpdate(null, ISSUE_ID, issueInputParameters);
        assertEquals(validationResult, defaultIssueService.validateUpdate(null, ISSUE_ID, issueInputParameters));
    }

    @Test
    public void testUpdate() throws UpdateException {
        EventDispatchOption eventDispatchOption = EventDispatchOption.DO_NOT_DISPATCH;
        final HistoryMetadata metadata = HistoryMetadata.builder("test").build();
        final IssueService.UpdateValidationResult issueValidationResult = new IssueService.UpdateValidationResult(mockIssue, new SimpleErrorCollection(), new HashMap<>(), metadata, Collections.emptyMap());

        doReturn(issueResult).when(defaultIssueService).update(null, issueValidationResult, eventDispatchOption, true);
        assertEquals(issueResult, defaultIssueService.update(null, issueValidationResult, eventDispatchOption, true));
    }

    @Test
    public void testValidateDelete() {
        final IssueService.DeleteValidationResult deleteResult = new IssueService.DeleteValidationResult(mockIssue, new SimpleErrorCollection());

        doReturn(deleteResult).when(defaultIssueService).validateDelete(null, ISSUE_ID);
        assertEquals(deleteResult, defaultIssueService.validateDelete(null, ISSUE_ID));
    }

    @Test
    public void testDeleteIssue() {
        EventDispatchOption eventDispatchOption = EventDispatchOption.DO_NOT_DISPATCH;
        final SimpleErrorCollection errorCollection = new SimpleErrorCollection();
        final IssueService.DeleteValidationResult deleteResult = new IssueService.DeleteValidationResult(mockIssue, errorCollection);

        doReturn(errorCollection).when(defaultIssueService).delete(null, deleteResult, eventDispatchOption, true);
        assertEquals(errorCollection, defaultIssueService.delete(null, deleteResult, eventDispatchOption, true));
    }

    @Test
    public void testValidateTransition() throws Exception {
        final int actionId = 2;
        final TransitionOptions transitionOptions = TransitionOptions.defaults();
        IssueInputParameters issueInputParameters = new IssueInputParametersImpl();
        IssueService.TransitionValidationResult validationResult = new IssueService.TransitionValidationResult(mockIssue, new SimpleErrorCollection(), new HashMap<>(), new HashMap(), 7);

        doReturn(validationResult).when(defaultIssueService).validateTransition(null, ISSUE_ID, actionId, issueInputParameters, transitionOptions);
        assertEquals(validationResult, defaultIssueService.validateTransition(null, ISSUE_ID, actionId, issueInputParameters, transitionOptions));
    }
}
