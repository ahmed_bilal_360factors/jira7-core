package com.atlassian.jira.jql.permission;

import com.atlassian.jira.exception.DataAccessException;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.fields.SearchableField;
import com.atlassian.jira.user.ApplicationUser;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mockito;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

/**
 * test for {@link FieldClausePermissionChecker}.
 *
 * @since v4.0
 */
public class TestFieldClausePermissionChecker {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void testHasPermissionToUseClause() throws Exception {
        final SearchableField searcherField = Mockito.mock(SearchableField.class);
        final FieldManager fieldManager = Mockito.mock(FieldManager.class);

        when(fieldManager.isFieldHidden((ApplicationUser) null, searcherField)).thenReturn(false, true);

        final FieldClausePermissionChecker checker = new FieldClausePermissionChecker(searcherField, fieldManager);


        assertTrue(checker.hasPermissionToUseClause(null));
        assertFalse(checker.hasPermissionToUseClause(null));
    }

    @Test
    public void testHasPermissionToUseClauseDataAccessException() throws Exception {
        final SearchableField searcherField = Mockito.mock(SearchableField.class);
        final FieldManager fieldManager = Mockito.mock(FieldManager.class);

        when(fieldManager.isFieldHidden((ApplicationUser) null, searcherField)).thenThrow(new DataAccessException("duh!"));

        final FieldClausePermissionChecker checker = new FieldClausePermissionChecker(searcherField, fieldManager);


        expectedException.expect(DataAccessException.class);
        checker.hasPermissionToUseClause(null);
    }
}
