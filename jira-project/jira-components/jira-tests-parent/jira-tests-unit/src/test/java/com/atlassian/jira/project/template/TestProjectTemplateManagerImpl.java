package com.atlassian.jira.project.template;

import com.atlassian.jira.application.ApplicationAuthorizationService;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.mock.component.MockComponentWorker;
import com.atlassian.jira.project.template.descriptor.ProjectTemplateModuleDescriptor;
import com.atlassian.jira.project.template.hook.AddProjectModule;
import com.atlassian.jira.project.template.module.ProjectTemplateModule;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.util.NoopI18nHelper;
import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.web.Condition;
import com.atlassian.plugin.webresource.UrlMode;
import com.atlassian.plugin.webresource.WebResourceUrlProvider;
import org.hamcrest.Description;
import org.hamcrest.TypeSafeDiagnosingMatcher;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import java.util.List;
import java.util.Optional;

import static com.atlassian.jira.project.template.ProjectTemplateManagerImpl.JIRA_DEFAULT_TEMPLATE_KEY_POST_7_0;
import static com.google.common.collect.Lists.newArrayList;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.mockito.Matchers.anyMap;
import static org.mockito.Mockito.RETURNS_DEEP_STUBS;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestProjectTemplateManagerImpl {
    private static final ProjectTemplateKey KEY = new ProjectTemplateKey("key");

    @Rule
    public MethodRule initMockito = MockitoJUnit.rule();

    @Mock
    private PluginAccessor pluginAccessor;
    @Mock
    private JiraAuthenticationContext authenticationContext;
    @Mock
    private ProjectTemplateBuilderFactory templateBuilderFactory;
    @Mock
    private ApplicationProperties applicationProperties;
    @Mock
    private ApplicationAuthorizationService applicationAuthorizationService;

    private ProjectTemplateManagerImpl manager;

    @Before
    public void setUp() {
        mockLoggedInUser();
        mockProjectTemplateBuilderFactory();
        mockNoOpI18nHelper();
        mockEncoding();
        manager = new ProjectTemplateManagerImpl(pluginAccessor, authenticationContext, templateBuilderFactory);
    }

    @Test
    public void getAllTemplatesFiltersOutNotDisplayableTemplates() {
        ProjectTemplateModuleDescriptor moduleDescriptor1 = notDisplayableModuleDescriptorWith("key-1");
        ProjectTemplateModuleDescriptor moduleDescriptor2 = notDisplayableModuleDescriptorWith("key-2");
        ProjectTemplateModuleDescriptor moduleDescriptor3 = displayableModuleDescriptorWith("key-3");
        ProjectTemplateModuleDescriptor moduleDescriptor4 = displayableModuleDescriptorWith("key-4");
        when(pluginAccessor.getEnabledModuleDescriptorsByClass(ProjectTemplateModuleDescriptor.class)).thenReturn(newArrayList(
            moduleDescriptor1, moduleDescriptor2, moduleDescriptor3, moduleDescriptor4
        ));

        List<ProjectTemplate> projectTemplates = manager.getProjectTemplates();

        assertThat(projectTemplates, contains(templateWithKey("key-3"), templateWithKey("key-4")));
    }

    @Test
    public void getAllTemplatesSortsThemBeforeReturningThem() {
        ProjectTemplateModuleDescriptor moduleDescriptor1 = displayableModuleDescriptorWithWeight("key-1", 400);
        ProjectTemplateModuleDescriptor moduleDescriptor2 = displayableModuleDescriptorWithWeight("key-2", 300);
        ProjectTemplateModuleDescriptor moduleDescriptor3 = displayableModuleDescriptorWithWeight("key-3", 200);
        ProjectTemplateModuleDescriptor moduleDescriptor4 = displayableModuleDescriptorWithWeight("key-4", 100);
        when(pluginAccessor.getEnabledModuleDescriptorsByClass(ProjectTemplateModuleDescriptor.class)).thenReturn(newArrayList(
            moduleDescriptor1, moduleDescriptor2, moduleDescriptor3, moduleDescriptor4
        ));

        List<ProjectTemplate> projectTemplates = manager.getProjectTemplates();

        assertThat(projectTemplates, contains(templateWithKey("key-4"), templateWithKey("key-3"), templateWithKey("key-2"), templateWithKey("key-1")));
    }

    @Test
    public void getByKeyReturnsNothingIfThereIsNoPluginModuleWithTheGivenKey() {
        mockModuleDescriptor(KEY, null);

        Optional<ProjectTemplate> projectTemplate = manager.getProjectTemplate(KEY);

        assertThat(projectTemplate.isPresent(), is(false));
    }

    @Test
    public void getByKeyReturnsNothingIfThereIsAPluginModuleWithTheGivenKeyButItIsNotAProjectTemplateModuleDescriptor() {
        mockModuleDescriptor(KEY, mock(ModuleDescriptor.class));

        Optional<ProjectTemplate> projectTemplate = manager.getProjectTemplate(KEY);

        assertThat(projectTemplate.isPresent(), is(false));
    }

    @Test
    public void getByKeyReturnsNoneIfTemplateIsNotDisplayable() {
        ProjectTemplateModuleDescriptor moduleDescriptor = notDisplayableModuleDescriptor();
        mockModuleDescriptor(KEY, moduleDescriptor);

        Optional<ProjectTemplate> projectTemplate = manager.getProjectTemplate(KEY);

        assertThat(projectTemplate.isPresent(), is(false));
    }

    @Test
    public void getByKeyReturnsNoneIfTheTemplateKeyIsNull() {
        Optional<ProjectTemplate> projectTemplate = manager.getProjectTemplate(null);

        assertThat(projectTemplate.isPresent(), is(false));
    }

    @Test
    public void getByKeyReturnsNoneIfTheTemplateKeyIsNotNullButTheStringInsideOfItIs() {
        Optional<ProjectTemplate> projectTemplate = manager.getProjectTemplate(new ProjectTemplateKey(null));

        assertThat(projectTemplate.isPresent(), is(false));
    }

    @Test
    public void getByKeyReturnsTheProjectTemplateIfItIsDefinedAndIsDisplayable() {
        ProjectTemplateModuleDescriptor moduleDescriptor = displayableModuleDescriptorWith("key");
        mockModuleDescriptor(KEY, moduleDescriptor);

        Optional<ProjectTemplate> projectTemplate = manager.getProjectTemplate(KEY);

        assertThat(projectTemplate.isPresent(), is(true));
    }

    @Test
    public void projectTemplateIsCorrectlyBuiltFromTheModuleDescriptor() {
        Integer weight = 100;
        ProjectTemplateModuleDescriptor moduleDescriptor = moduleDescriptorWith(
                weight,
                "key",
                "label",
                "description",
                Optional.of("long-description"),
                "icon-url",
                "background-url",
                "soy-path",
                "project-type",
                mock(AddProjectModule.class)
        );
        mockModuleDescriptor(KEY, moduleDescriptor);

        ProjectTemplate projectTemplate = manager.getProjectTemplate(KEY).get();

        assertThat(projectTemplate.getKey().getKey(), is("key"));
        assertThat(projectTemplate.getWeight(), is(weight));
        assertThat(projectTemplate.getName(), is("label{[]}"));
        assertThat(projectTemplate.getDescription(), is("description{[]}"));
        assertThat(projectTemplate.getLongDescriptionContent(), is("long-description{[]}"));
        assertThat(projectTemplate.getIconUrl(), is("icon-url"));
        assertThat(projectTemplate.getBackgroundIconUrl(), is("background-url"));
        assertThat(projectTemplate.getInfoSoyPath(), is("soy-path"));
        assertThat(projectTemplate.hasAddProjectModule(), is(true));
        assertThat(projectTemplate.getProjectTypeKey().getKey(), is("project-type"));
    }

    @Test
    public void defaultTemplateIsJiraDefaultSchemes() {
        Integer weight = 100;
        ProjectTemplateModuleDescriptor moduleDescriptor = moduleDescriptorWith(
                weight,
                JIRA_DEFAULT_TEMPLATE_KEY_POST_7_0.getKey(),
                "label",
                "description",
                Optional.of("long-description"),
                "icon-url",
                "background-url",
                "soy-path",
                "project-type",
                mock(AddProjectModule.class)
        );
        mockModuleDescriptor(JIRA_DEFAULT_TEMPLATE_KEY_POST_7_0, moduleDescriptor);

        ProjectTemplate defaultTemplate = manager.getDefaultTemplate();

        assertThat(defaultTemplate.getKey(), is(JIRA_DEFAULT_TEMPLATE_KEY_POST_7_0));
    }

    @Test
    public void defaultTemplateIsTaskManagementWhenRenaissanceIsTurnedOn() {
        Integer weight = 100;
        ProjectTemplateModuleDescriptor moduleDescriptor = moduleDescriptorWith(
                weight,
                JIRA_DEFAULT_TEMPLATE_KEY_POST_7_0.getKey(),
                "label",
                "description",
                Optional.of("long-description"),
                "icon-url",
                "background-url",
                "soy-path",
                "project-type",
                mock(AddProjectModule.class)
        );
        mockModuleDescriptor(JIRA_DEFAULT_TEMPLATE_KEY_POST_7_0, moduleDescriptor);

        ProjectTemplate defaultTemplate = manager.getDefaultTemplate();

        assertThat(defaultTemplate.getKey(), is(JIRA_DEFAULT_TEMPLATE_KEY_POST_7_0));
    }

    @Test(expected = IllegalStateException.class)
    public void throwsAnExceptionIfTheJiraDefaultSchemesTemplateIsNotPresent() {
        mockModuleDescriptor(JIRA_DEFAULT_TEMPLATE_KEY_POST_7_0, null);

        manager.getDefaultTemplate();
    }

    private void mockLoggedInUser() {
        when(authenticationContext.getLoggedInUser()).thenReturn(new MockApplicationUser("user"));
    }

    @SuppressWarnings("unchecked")
    private void mockModuleDescriptor(ProjectTemplateKey key, ModuleDescriptor descriptor) {
        when(pluginAccessor.getEnabledPluginModule(key.getKey())).thenReturn(descriptor);
    }

    private ProjectTemplateModuleDescriptor notDisplayableModuleDescriptor() {
        return mockProjectTemplateModuleDescriptor(false, Optional.empty());
    }

    private ProjectTemplateModule anyProjectTemplateModule() {
        return mock(ProjectTemplateModule.class, RETURNS_DEEP_STUBS);
    }

    private ProjectTemplateModuleDescriptor displayableModuleDescriptorWith(String key) {
        return moduleDescriptorWith(true, 0, key, "", "", Optional.of(""), "", "", "", "", mock(AddProjectModule.class));
    }

    private ProjectTemplateModuleDescriptor notDisplayableModuleDescriptorWith(String key) {
        return moduleDescriptorWith(false, 0, key, "", "", Optional.of(""), "", "", "", "", mock(AddProjectModule.class));
    }

    private ProjectTemplateModuleDescriptor displayableModuleDescriptorWithWeight(String key, int weight) {
        return moduleDescriptorWith(true, weight, key, "", "", Optional.of(""), "", "", "", "", mock(AddProjectModule.class));
    }

    private ProjectTemplateModuleDescriptor moduleDescriptorWith(
            int weight,
            String key,
            String label,
            String description,
            Optional<String> longDescription,
            String iconUrl,
            String backgroundUrl,
            String soyPath,
            String projectType,
            AddProjectModule addProjectModule) {
        return moduleDescriptorWith(true, weight, key, label, description, longDescription, iconUrl, backgroundUrl, soyPath, projectType, addProjectModule);
    }

    private ProjectTemplateModuleDescriptor moduleDescriptorWith(
            boolean shouldBeDisplayed,
            Integer weight,
            String key,
            String label,
            String description,
            Optional<String> longDescription,
            String iconUrl,
            String backgroundUrl,
            String soyPath,
            String projectType,
            AddProjectModule addProjectModule) {
        ProjectTemplateModule projectTemplateModule = anyProjectTemplateModule();

        when(projectTemplateModule.weight()).thenReturn(weight);
        when(projectTemplateModule.key()).thenReturn(key);
        when(projectTemplateModule.labelKey()).thenReturn(label);
        when(projectTemplateModule.descriptionKey()).thenReturn(description);
        when(projectTemplateModule.longDescriptionKey()).thenReturn(longDescription);
        when(projectTemplateModule.icon().url()).thenReturn(iconUrl);
        when(projectTemplateModule.backgroundIcon().url()).thenReturn(backgroundUrl);
        when(projectTemplateModule.getInfoSoyPath()).thenReturn(soyPath);
        when(projectTemplateModule.hasAddProjectModule()).thenReturn(true);
        when(projectTemplateModule.addProjectModule()).thenReturn(addProjectModule);
        when(projectTemplateModule.projectTypeKey()).thenReturn(projectType);

        return mockProjectTemplateModuleDescriptor(shouldBeDisplayed, Optional.ofNullable(projectTemplateModule));
    }

    private ProjectTemplateModuleDescriptor mockProjectTemplateModuleDescriptor(boolean shouldDisplay, Optional<ProjectTemplateModule> module) {
        Condition condition = mock(Condition.class);
        when(condition.shouldDisplay(anyMap())).thenReturn(shouldDisplay);

        ProjectTemplateModuleDescriptor moduleDescriptor = mock(ProjectTemplateModuleDescriptor.class);
        when(moduleDescriptor.getCondition()).thenReturn(condition);
        when(moduleDescriptor.getModule()).thenReturn(module.orElse(anyProjectTemplateModule()));

        return moduleDescriptor;
    }

    private void mockProjectTemplateBuilderFactory() {
        WebResourceUrlProvider webResourceUrlProvider = mock(WebResourceUrlProvider.class);
        when(webResourceUrlProvider.getBaseUrl(UrlMode.ABSOLUTE)).thenReturn("http://localhost");
        when(templateBuilderFactory.newBuilder()).thenReturn(new ProjectTemplateBuilder(webResourceUrlProvider));
    }

    private void mockNoOpI18nHelper() {
        when(authenticationContext.getI18nHelper()).thenReturn(new NoopI18nHelper());
    }

    private ProjectTemplateMatcher templateWithKey(String expectedKey) {
        return new ProjectTemplateMatcher(expectedKey);
    }

    private void mockEncoding() {
        when(applicationProperties.getEncoding()).thenReturn("UTF-8");
        new MockComponentWorker().addMock(ApplicationProperties.class, applicationProperties).init();
    }

    private static class ProjectTemplateMatcher extends TypeSafeDiagnosingMatcher<ProjectTemplate> {
        private final ProjectTemplateKey expectedKey;

        private ProjectTemplateMatcher(String expectedKey) {
            this.expectedKey = new ProjectTemplateKey(expectedKey);
        }

        @Override
        protected boolean matchesSafely(ProjectTemplate template, Description mismatchDescription) {
            return template.getKey().equals(expectedKey);
        }

        @Override
        public void describeTo(Description description) {
            description.appendText("project template with key: " + expectedKey);
        }
    }
}
