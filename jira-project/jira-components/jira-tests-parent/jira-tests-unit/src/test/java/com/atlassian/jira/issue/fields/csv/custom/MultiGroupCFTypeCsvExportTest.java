package com.atlassian.jira.issue.fields.csv.custom;

import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.jira.issue.customfields.CustomFieldType;
import com.atlassian.jira.issue.customfields.impl.MultiGroupCFType;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.MockGroup;
import com.google.common.collect.ImmutableList;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collection;
import java.util.Locale;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class MultiGroupCFTypeCsvExportTest extends CustomFieldMultiValueCsvExporterTest<Collection<Group>> {
    @Mock
    private JiraAuthenticationContext jiraAuthenticationContext;

    @Override
    protected CustomFieldType<Collection<Group>, ?> createField() {
        when(jiraAuthenticationContext.getLocale()).thenReturn(Locale.ENGLISH);
        return new MultiGroupCFType(null, null, null, null, jiraAuthenticationContext, null, null, null);
    }

    @Test
    public void allGroupNamesAreExportedAsSeparateValues() {
        whenFieldValueIs(ImmutableList.<Group>of(
                new MockGroup("witchers"), new MockGroup("witches")
        ));
        assertExportedValue("witchers", "witches");
    }

    @Test
    public void groupNamesAreSorted() {
        whenFieldValueIs(ImmutableList.<Group>of(
                new MockGroup("b"), new MockGroup("a")
        ));
        assertExportedValue("a", "b");
    }
}
