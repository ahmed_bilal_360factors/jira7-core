package com.atlassian.jira.upgrade.tasks.role;

import com.atlassian.collectors.CollectorsUtil;
import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.crowd.embedded.impl.ImmutableUser;
import com.atlassian.fugue.Option;
import com.atlassian.jira.application.ApplicationKeys;
import com.atlassian.jira.test.util.lic.core.CoreLicenses;
import com.atlassian.jira.test.util.lic.sd.ServiceDeskLicenses;
import com.atlassian.jira.test.util.lic.software.SoftwareLicenses;
import com.atlassian.jira.upgrade.tasks.role.MigrationValidatorImpl.MigrationValidationFailedException;
import com.atlassian.jira.user.MockGroup;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import org.hamcrest.Matcher;
import org.hamcrest.core.StringContains;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Collection;
import java.util.Set;

import static com.atlassian.jira.upgrade.tasks.role.TestUtils.toLicense;
import static com.google.common.collect.ImmutableSet.of;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.core.StringContains.containsString;
import static org.mockito.Mockito.when;


public class MigrationValidatorImplAgentTest {
    private static final MockGroup ADMIN_ONE = new MockGroup("AdminOne");
    private static final MockGroup ADMIN_TWO = new MockGroup("AdminTwo");
    private static final MockGroup ADMIN_THREE = new MockGroup("AdminThree");
    private static final MockGroup SOFTWARE_ONE = new MockGroup("SoftwareOne");
    private static final MockGroup SOFTWARE_TWO = new MockGroup("SoftwareTwo");
    private static final MockGroup SOFTWARE_THREE = new MockGroup("SoftwareThree");
    private static final MockGroup OTHER_GROUP = new MockGroup("Other");
    private static final MockGroup AGENT_ONE = new MockGroup("AgentOne");
    private static final MockGroup AGENT_TWO = new MockGroup("AgentTwo");
    private static final MockGroup AGENT_THREE = new MockGroup("AgentThree");
    private static final UserWithPermissions realAgent = newUserWithPermissions("realAgent", true, false);
    private static final UserWithPermissions badAgent = newUserWithPermissions("badAgent", false, false);
    private static final UserWithPermissions adminAgent = newUserWithPermissions("adminAgent", false, true);
    private static final UserWithPermissions adminUserAgent = newUserWithPermissions("adminUserAgent", true, true);

    final License swLicense = toLicense(SoftwareLicenses.LICENSE_SOFTWARE);
    final License coreLicense = toLicense(CoreLicenses.LICENSE_CORE);
    final License sdLicense = toLicense(ServiceDeskLicenses.LICENSE_SERVICE_DESK_TBP);
    final License sdAbpLicense = toLicense(ServiceDeskLicenses.LICENSE_SERVICE_DESK_ABP);
    final Licenses twoLicenses = new Licenses(ImmutableSet.of(swLicense, coreLicense));
    final Licenses threeLicenses = new Licenses(ImmutableSet.of(swLicense, coreLicense, sdLicense));

    @Mock
    private GlobalPermissionDao globalPermissionDao;
    @Mock
    Jira6xServiceDeskLicenseProvider licenseSupplier;
    @Mock
    MigrationGroupService migrationGroupService;

    private final Set<Group> adminGroups = ImmutableSet.of(ADMIN_ONE, ADMIN_TWO, ADMIN_THREE);
    private final Set<Group> useGroups = ImmutableSet.of(SOFTWARE_ONE, SOFTWARE_TWO);
    private final Set<Group> useAndAdminGroups = ImmutableSet.of(SOFTWARE_ONE, SOFTWARE_TWO, ADMIN_ONE, ADMIN_TWO, ADMIN_THREE);
    private final Set<Group> serviceDeskGroups = ImmutableSet.of(AGENT_ONE, AGENT_TWO, AGENT_THREE);
    private final Set<Group> serviceDeskWithAdminGroups = ImmutableSet.of(AGENT_ONE, AGENT_TWO, AGENT_THREE, ADMIN_THREE);
    private final ApplicationRole softwareRole = ApplicationRole.forKey(ApplicationKeys.SOFTWARE)
            .withGroups(useAndAdminGroups, useGroups);
    private final ApplicationRole coreRole = ApplicationRole.forKey(ApplicationKeys.CORE)
            .withGroups(useAndAdminGroups, useGroups);
    private final ApplicationRole sdRole = ApplicationRole.forKey(ApplicationKeys.SERVICE_DESK)
            .withGroups(serviceDeskWithAdminGroups, serviceDeskGroups);
    private MigrationValidator validator;

    @Rule
    public ExpectedException thrown = ExpectedException.none();
    @Rule
    public MockitoRule initMocks = MockitoJUnit.rule();

    @Before
    public void setUp() throws Exception {
        validator = new MigrationValidatorImpl(this.globalPermissionDao, licenseSupplier, migrationGroupService);
        when(globalPermissionDao.groupsWithAdminPermission()).thenReturn(adminGroups);
        when(globalPermissionDao.groupsWithUsePermission()).thenReturn(useAndAdminGroups);
        when(globalPermissionDao.groupsWithSdAgentPermission()).thenReturn(serviceDeskWithAdminGroups);
        when(licenseSupplier.serviceDeskLicense()).thenReturn(Option.some(sdAbpLicense));
    }

    @Test
    public void validationShouldPassWhenAgentsAreAllValid() {
        // given
        when(migrationGroupService.getUsersInGroup(AGENT_ONE))
                .thenReturn(of(adminUserAgent, adminAgent, realAgent));
        when(migrationGroupService.getUsersInGroup(ADMIN_THREE))
                .thenReturn(of(adminUserAgent));

        MigrationState state = migrationStateOf(threeLicenses,
                new ApplicationRoles(ImmutableList.of(softwareRole, coreRole, sdRole)));

        // when
        validator.validate(TestUtils.emptyState(), state);
        // then
        // there is no exception thrown
    }

    @Test
    public void validationShouldFailWhenAgentsDoNotHaveUseOrAdminAccess() {
        thrown.expect(MigrationValidationFailedException.class);
        thrown.expectMessage(containsString(AGENT_TWO.getName()));

        // given
        when(migrationGroupService.getUsersInGroup(AGENT_ONE)).thenReturn(of(adminUserAgent, adminAgent));
        when(migrationGroupService.getUsersInGroup(AGENT_TWO)).thenReturn(of(badAgent));
        when(migrationGroupService.getUsersInGroup(ADMIN_THREE)).thenReturn(of(adminUserAgent));

        MigrationState state = migrationStateOf(threeLicenses,
                new ApplicationRoles(ImmutableList.of(softwareRole, coreRole, sdRole)));

        // when
        validator.validate(TestUtils.emptyState(), state);
        // then
        // there is no exception thrown
    }

    @Test
    public void validationShouldFailWhenExtraGroupsInSdApp() {
        // expect
        thrown.expect(MigrationValidationFailedException.class);
        thrown.expectMessage(containsString(SOFTWARE_THREE.getName()));

        // given
        final ApplicationRole serviceDeskRole = ApplicationRole.forKey(ApplicationKeys.SERVICE_DESK)
                .withGroups(ImmutableSet.of(AGENT_ONE, AGENT_TWO, AGENT_THREE, ADMIN_THREE, SOFTWARE_THREE),
                        serviceDeskGroups);

        MigrationState state = migrationStateOf(threeLicenses,
                new ApplicationRoles(ImmutableList.of(softwareRole, coreRole, serviceDeskRole)));

        // when
        validator.validate(TestUtils.emptyState(), state);
    }

    @Test
    public void validationShouldPassWhenExtraGroupIsInPreviousState() {
        // given
        Set<Group> extraGroups = ImmutableSet.of(SOFTWARE_ONE, SOFTWARE_TWO, OTHER_GROUP);
        final ApplicationRole initialServiceDeskRole = ApplicationRole.forKey(ApplicationKeys.SERVICE_DESK)
                .withGroups(extraGroups, ImmutableSet.of());
        final ApplicationRole serviceDeskRole = ApplicationRole.forKey(ApplicationKeys.SERVICE_DESK)
                .withGroups(Sets.union(serviceDeskWithAdminGroups, extraGroups), serviceDeskGroups);

        MigrationState originalState = migrationStateOf(threeLicenses, new ApplicationRoles(ImmutableList.of(initialServiceDeskRole)));
        MigrationState state = migrationStateOf(threeLicenses,
                new ApplicationRoles(ImmutableList.of(softwareRole, coreRole, serviceDeskRole)));

        // when
        validator.validate(originalState, state);
        // then
        // there is no exception thrown
    }

    @Test
    public void validationShouldPassNoAppsWhenNoUseAndAdminGroupsFound() {
        // given

        when(licenseSupplier.serviceDeskLicense()).thenReturn(Option.some(sdAbpLicense));
        when(globalPermissionDao.groupsWithSdAgentPermission()).thenReturn(ImmutableSet.of());

        MigrationState state = migrationStateOf(threeLicenses,
                new ApplicationRoles(ImmutableList.of()));

        // when
        validator.validate(TestUtils.emptyState(), state);
        // then
        // there is no exception thrown
    }

    @Test
    public void validationShouldFailWhenServiceDeskIsUnlicensed() {
        // given
        thrown.expect(MigrationValidationFailedException.class);
        thrown.expectMessage(stringContainsInAnyOrder(
                ImmutableList.of("Service Desk appears to have groups when it is not license",
                        AGENT_ONE.getName(), AGENT_TWO.getName())));
        when(licenseSupplier.serviceDeskLicense()).thenReturn(Option.none());

        MigrationState state = migrationStateOf(twoLicenses,
                new ApplicationRoles(ImmutableList.of(softwareRole, coreRole, sdRole)));

        // when
        validator.validate(TestUtils.emptyState(), state);
        // then
        // there is no exception thrown
    }

    @Test
    public void validationShouldPassWhenPreExistingSdUsersHaveNoUsePermission() {
        // given
        Set<Group> extraGroups = ImmutableSet.of(SOFTWARE_ONE, SOFTWARE_TWO, OTHER_GROUP);
        final ApplicationRole initialServiceDeskRole = ApplicationRole.forKey(ApplicationKeys.SERVICE_DESK)
                .withGroups(extraGroups, ImmutableSet.of());
        final ApplicationRole serviceDeskRole = ApplicationRole.forKey(ApplicationKeys.SERVICE_DESK)
                .withGroups(Sets.union(serviceDeskWithAdminGroups, extraGroups), serviceDeskGroups);
        when(migrationGroupService.getUsersInGroup(OTHER_GROUP)).thenReturn(of(badAgent));

        MigrationState originalState = migrationStateOf(threeLicenses, new ApplicationRoles(ImmutableList.of(initialServiceDeskRole)));
        MigrationState state = migrationStateOf(threeLicenses,
                new ApplicationRoles(ImmutableList.of(softwareRole, coreRole, serviceDeskRole)));

        // when
        validator.validate(originalState, state);
        // then
        // there is no exception thrown

    }

    @Test
    public void validationShouldPassWhen6xAgentGroupsAreNotMigratedToSdBecauseAgentsDidntHaveUsePermission() {
        // given
        final ApplicationRole serviceDeskRole = ApplicationRole.forKey(ApplicationKeys.SERVICE_DESK)
                .withGroups(serviceDeskWithAdminGroups, serviceDeskGroups);
        when(globalPermissionDao.groupsWithSdAgentPermission()).thenReturn(
                Sets.union(serviceDeskWithAdminGroups, ImmutableSet.of(OTHER_GROUP)));

        when(migrationGroupService.getUsersInGroup(OTHER_GROUP)).thenReturn(of(badAgent));

        MigrationState state = migrationStateOf(threeLicenses,
                new ApplicationRoles(ImmutableList.of(softwareRole, coreRole, serviceDeskRole)));

        // when
        validator.validate(TestUtils.emptyState(), state);
        // then
        // there is no exception thrown

    }

    private static MigrationState migrationStateOf(Licenses licenses, ApplicationRoles roles) {
        return new MigrationState(licenses, roles, ImmutableList.<Runnable>of(), new MigrationLogImpl());
    }

    private static Matcher<String> stringContainsInAnyOrder(Collection<String> s) {

        return allOf(s.stream().map(StringContains::containsString).collect(CollectorsUtil.toImmutableList()));
    }

    private static UserWithPermissions newUserWithPermissions(String name, boolean hasUsePermissions,
                                                              boolean hasAdminPermissions) {
        return new UserWithPermissions(ImmutableUser.newUser().name(name).toUser(), hasUsePermissions, hasAdminPermissions);
    }
}