package com.atlassian.jira.issue.fields.config.manager;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.event.MockEventPublisher;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.fields.config.FieldConfigScheme;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.mockito.Mock;

import java.util.List;

import static com.google.common.collect.Lists.newArrayList;
import static org.hamcrest.Matchers.arrayContaining;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

/**
 * Unit tests for TestIssueTypeSchemeManagerImpl
 *
 * @since v3.13.2
 */
public class TestIssueTypeSchemeManagerImpl {
    @Rule
    public TestRule mocksInContainer = MockitoMocksInContainer.rule(this);

    @Mock
    @AvailableInContainer
    ApplicationProperties applicationProperties;

    @Mock
    @AvailableInContainer
    FieldManager mockFieldManager;

    @Mock
    @AvailableInContainer
    FieldConfigSchemeManager mockFieldConfigSchemeManager;

    @Test
    public void testSchemeOrder() {
        final FieldConfigScheme scheme1 = createScheme("aardvark", 1L);
        final FieldConfigScheme scheme2 = createScheme("blarney", 2L);
        final FieldConfigScheme scheme3 = createScheme("default", 3L);
        final FieldConfigScheme scheme4 = createScheme("zedbra", 4L);
        final FieldConfigScheme scheme5 = createScheme("@#$@", 5L);
        final FieldConfigScheme scheme6 = createScheme(null, 6L);
        final FieldConfigScheme scheme7 = createScheme("nullid", null);
        final FieldConfigScheme scheme8 = createScheme(null, null);

        final Long DEFAUL_ISSUE_TYPE_SCHEME = 3l;

        when(mockFieldManager.getIssueTypeField()).thenReturn(null);
        when(applicationProperties.getString(APKeys.DEFAULT_ISSUE_TYPE_SCHEME)).thenReturn(DEFAUL_ISSUE_TYPE_SCHEME.toString());

        when(mockFieldConfigSchemeManager.getFieldConfigScheme(DEFAUL_ISSUE_TYPE_SCHEME)).thenReturn(scheme3);
        when(mockFieldConfigSchemeManager.getConfigSchemesForField(null)).thenReturn(newArrayList(scheme1, scheme2, scheme3, scheme4, scheme5, scheme6, scheme7, scheme8));

        final EventPublisher eventPublisher = new MockEventPublisher();

        final IssueTypeSchemeManager manager = new IssueTypeSchemeManagerImpl(mockFieldConfigSchemeManager, null, null, null, eventPublisher);

        final List<FieldConfigScheme> schemes = manager.getAllSchemes();


        assertThat(
                schemes.stream().map(FieldConfigScheme::getName).toArray(String[]::new),
                is(arrayContaining(
                        "default", null, null, "@#$@", "aardvark", "blarney", "nullid", "zedbra"
                )));
    }

    FieldConfigScheme createScheme(final String name, final Long id) {
        return new FieldConfigScheme.Builder().setId(id).setName(name).toFieldConfigScheme();
    }
}
