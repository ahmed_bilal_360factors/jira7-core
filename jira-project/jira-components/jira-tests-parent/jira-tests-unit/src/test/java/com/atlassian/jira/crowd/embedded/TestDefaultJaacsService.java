package com.atlassian.jira.crowd.embedded;

import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.exception.ApplicationNotFoundException;
import com.atlassian.crowd.manager.application.ApplicationManager;
import com.atlassian.crowd.model.application.Application;
import com.atlassian.crowd.model.application.RemoteAddress;
import com.atlassian.jira.bc.MockJiraServiceContext;
import com.atlassian.jira.junit.rules.InitMockitoMocks;
import com.atlassian.jira.mock.i18n.MockI18nHelper;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.ValidationFailureException;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import org.hamcrest.FeatureMatcher;
import org.hamcrest.Matcher;
import org.hamcrest.MatcherAssert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Matchers;
import org.mockito.Mock;

import java.util.Set;

import static java.util.Collections.singleton;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestDefaultJaacsService {
    @Rule
    public ExpectedException exception = ExpectedException.none();
    @Rule
    public InitMockitoMocks mocks = new InitMockitoMocks(this);

    @Mock
    Application app;
    @Mock
    ApplicationManager applicationManager;
    @Mock
    PermissionManager permissionManager;
    @Mock
    I18nHelper.BeanFactory i18nFactory;

    private Set<RemoteAddress> addressList1;
    private RemoteAddress address1;
    private RemoteAddress address2;
    private RemoteAddress address3;

    private MockJiraServiceContext mockServiceContext;
    private ApplicationUser user = new MockApplicationUser("user");
    private DefaultJaacsService defaultJaacsService;

    @Before
    public void setup() {
        mockServiceContext = new MockJiraServiceContext(user);

        when(i18nFactory.getInstance(user)).thenReturn(new MockI18nHelper());

        address1 = new RemoteAddress("10.10.8.22");
        address2 = new RemoteAddress("10.10.10.11");
        address3 = new RemoteAddress("117.21.21.145");

        addressList1 = ImmutableSet.of(address1, address2, address3);

        defaultJaacsService = new DefaultJaacsService(applicationManager, permissionManager, i18nFactory);
    }

    @Test
    public void testGetRemoteAddresses() throws Exception {
        when(app.getRemoteAddresses()).thenReturn(addressList1);
        when(applicationManager.findById(1)).thenReturn(app);

        assertEquals(Sets.newHashSet(address1, address2, address3),
                defaultJaacsService.getRemoteAddresses(null, 1));
    }

    @Test
    public void testValidateAddRemoteAddress_ip4() throws Exception {
        when(app.getRemoteAddresses()).thenReturn(addressList1);
        when(applicationManager.findById(1)).thenReturn(app);
        when(permissionManager.hasPermission(Permissions.ADMINISTER, user)).thenReturn(true);

        assertTrue(defaultJaacsService.validateAddRemoteAddress(mockServiceContext, "10.10.10.10", 1));
    }

    @Test
    public void testValidateAddRemoteAddress_ip6() throws Exception {
        when(app.getRemoteAddresses()).thenReturn(addressList1);
        when(applicationManager.findById(1)).thenReturn(app);
        when(permissionManager.hasPermission(Permissions.ADMINISTER, user)).thenReturn(true);

        assertTrue(defaultJaacsService.validateAddRemoteAddress(mockServiceContext, "[fe80::213:2ff:fe57:43fd]", 1));
    }

    @Test
    public void testValidateAddRemoteAddress_nullIP() throws Exception {
        when(permissionManager.hasPermission(Permissions.ADMINISTER, user)).thenReturn(true);

        assertFalse(defaultJaacsService.validateAddRemoteAddress(mockServiceContext, null, 1));
        // Check the error
        final ErrorCollection errorCollection = mockServiceContext.getErrorCollection();
        assertEquals("An address is required.", errorCollection.getErrors().get("remoteAddresses"));
    }

    @Test
    public void testValidateAddRemoteAddress_emptyIP() throws Exception {
        when(permissionManager.hasPermission(Permissions.ADMINISTER, user)).thenReturn(true);

        assertFalse(defaultJaacsService.validateAddRemoteAddress(mockServiceContext, "", 1));
        // Check the error
        final ErrorCollection errorCollection = mockServiceContext.getErrorCollection();
        assertEquals("An address is required.", errorCollection.getErrors().get("remoteAddresses"));
    }

    @Test
    public void testValidateAddRemoteAddress_invalidIP() throws Exception {
        when(permissionManager.hasPermission(Permissions.ADMINISTER, user)).thenReturn(true);

        assertFalse(defaultJaacsService.validateAddRemoteAddress(mockServiceContext, "265.245.234.1111", 1));
        // Check the error
        final ErrorCollection errorCollection = mockServiceContext.getErrorCollection();
        assertEquals("'265.245.234.1111' is not a valid IP address.", errorCollection.getErrors().get("remoteAddresses"));
    }

    @Test
    public void testValidateAddRemoteAddress_notPermitted() throws Exception {
        when(permissionManager.hasPermission(Permissions.ADMINISTER, user)).thenReturn(false);

        assertFalse(defaultJaacsService.validateAddRemoteAddress(mockServiceContext, null, 1));
        // Check the error
        final ErrorCollection errorCollection = mockServiceContext.getErrorCollection();
        MatcherAssert.assertThat(errorCollection.getErrorMessages(), containsInAnyOrder("You must be a JIRA Administrator to configure JIRA as a Crowd Server."));
    }

    @Test
    public void testAddRemoteAddress() throws Exception {
        when(app.getRemoteAddresses()).thenReturn(addressList1);
        when(applicationManager.findById(1)).thenReturn(app);
        when(permissionManager.hasPermission(Permissions.ADMINISTER, user)).thenReturn(true);

        assertTrue(defaultJaacsService.addRemoteAddress(mockServiceContext, "10.10.10.10", 1));
        verify(applicationManager).addRemoteAddress(app, new RemoteAddress("10.10.10.10"));
    }

    @Test
    public void testAddRemoteAddress_validationFail() throws Exception {
        when(permissionManager.hasPermission(Permissions.ADMINISTER, user)).thenReturn(false);

        assertFalse(defaultJaacsService.addRemoteAddress(mockServiceContext, "10.10.10.10", 1));
        // Check the error
        final ErrorCollection errorCollection = mockServiceContext.getErrorCollection();
        MatcherAssert.assertThat(errorCollection.getErrorMessages(), containsInAnyOrder("You must be a JIRA Administrator to configure JIRA as a Crowd Server."));
    }

    @Test
    public void testValidateDeleteApplication() throws Exception {
        when(applicationManager.findById(1)).thenReturn(app);
        when(permissionManager.hasPermission(Permissions.ADMINISTER, user)).thenReturn(true);

        assertTrue(defaultJaacsService.validateDeleteApplication(mockServiceContext, 1));
    }

    @Test
    public void testValidateDeleteApplication_invalidApp() throws Exception {
        // We can delete invalid addresses.
        when(applicationManager.findById(1)).thenThrow(new ApplicationNotFoundException(1L));
        when(permissionManager.hasPermission(Permissions.ADMINISTER, user)).thenReturn(true);

        assertFalse(defaultJaacsService.validateDeleteApplication(mockServiceContext, 1));
    }

    @Test
    public void testValidateDeleteRemoteAddress_notPermitted() throws Exception {
        when(permissionManager.hasPermission(Permissions.ADMINISTER, user)).thenReturn(false);

        assertFalse(defaultJaacsService.validateDeleteApplication(mockServiceContext, 1));
        // Check the error
        final ErrorCollection errorCollection = mockServiceContext.getErrorCollection();
        MatcherAssert.assertThat(errorCollection.getErrorMessages(), containsInAnyOrder("You must be a JIRA Administrator to configure JIRA as a Crowd Server."));
    }

    @Test
    public void testDeleteApplication() throws Exception {
        when(applicationManager.findById(1)).thenReturn(app);
        when(permissionManager.hasPermission(Permissions.ADMINISTER, user)).thenReturn(true);

        assertTrue(defaultJaacsService.deleteApplication(mockServiceContext, 1));
        verify(applicationManager).remove(app);
    }

    @Test
    public void testDeleteRemoteAddress_notPermitted() throws Exception {
        when(permissionManager.hasPermission(Permissions.ADMINISTER, user)).thenReturn(false);

        assertFalse(defaultJaacsService.deleteApplication(mockServiceContext, 1));
        // Check the error
        final ErrorCollection errorCollection = mockServiceContext.getErrorCollection();
        MatcherAssert.assertThat(errorCollection.getErrorMessages(), containsInAnyOrder("You must be a JIRA Administrator to configure JIRA as a Crowd Server."));
    }

    @Test
    public void testValidateResetPassword() throws Exception {
        when(permissionManager.hasPermission(Permissions.ADMINISTER, user)).thenReturn(true);

        assertTrue(defaultJaacsService.validateResetPassword(mockServiceContext, "secret", 1));
    }

    @Test
    public void testValidateResetPassword_nullPass() throws Exception {
        when(permissionManager.hasPermission(Permissions.ADMINISTER, user)).thenReturn(true);

        assertFalse(defaultJaacsService.validateResetPassword(mockServiceContext, null, 1));
        // Check the error
        final ErrorCollection errorCollection = mockServiceContext.getErrorCollection();
        assertEquals("A password is required.", errorCollection.getErrors().get("credential"));
    }

    @Test
    public void testValidateResetPassword_EmptyPass() throws Exception {
        when(permissionManager.hasPermission(Permissions.ADMINISTER, user)).thenReturn(true);

        assertFalse(defaultJaacsService.validateResetPassword(mockServiceContext, "", 1));
        // Check the error
        final ErrorCollection errorCollection = mockServiceContext.getErrorCollection();
        assertEquals("A password is required.", errorCollection.getErrors().get("credential"));
    }


    @Test
    public void testValidateResetPassword_notPermitted() throws Exception {
        when(permissionManager.hasPermission(Permissions.ADMINISTER, user)).thenReturn(false);

        assertFalse(defaultJaacsService.validateResetPassword(mockServiceContext, null, 1));
        // Check the error
        final ErrorCollection errorCollection = mockServiceContext.getErrorCollection();
        MatcherAssert.assertThat(errorCollection.getErrorMessages(), containsInAnyOrder("You must be a JIRA Administrator to configure JIRA as a Crowd Server."));
    }

    @Test
    public void testResetPassword() throws Exception {
        when(applicationManager.findById(1)).thenReturn(app);
        when(permissionManager.hasPermission(Permissions.ADMINISTER, user)).thenReturn(true);

        assertTrue(defaultJaacsService.resetPassword(mockServiceContext, "secret", 1));
        verify(applicationManager).updateCredential(app, new PasswordCredential("secret"));
    }

    @Test
    public void testResetPassword_notPermitted() throws Exception {
        when(permissionManager.hasPermission(Permissions.ADMINISTER, user)).thenReturn(false);

        assertFalse(defaultJaacsService.resetPassword(mockServiceContext, "secret", 1));
    }

    @Test
    public void findAllShouldThrowAValidationExceptionIfUserHasNoAdminAccess() throws Exception {
        // given
        when(permissionManager.hasPermission(Matchers.anyInt(), eq(user))).thenReturn(false);

        // expect
        exception.expect(ValidationFailureException.class);
        exception.expect(hasErrorMessagesThat(hasItem("admin.jaacs.application.admin.required")));

        // once
        defaultJaacsService.findAll(user);
    }

    @Test
    public void findByIdShouldThrowAValidationExceptionIfUserHasNoAdminAccess() throws Exception {
        // given
        when(permissionManager.hasPermission(Matchers.anyInt(), eq(user))).thenReturn(false);

        // expect
        exception.expect(ValidationFailureException.class);
        exception.expect(hasErrorMessagesThat(hasItem("admin.jaacs.application.admin.required")));

        // once
        defaultJaacsService.findById(user, 1L);
    }

    @Test
    public void createShouldThrowAValidationExceptionIfUserHasNoAdminAccess() throws Exception {
        // given
        when(permissionManager.hasPermission(Matchers.anyInt(), eq(user))).thenReturn(false);

        // expect
        exception.expect(ValidationFailureException.class);
        exception.expect(hasErrorMessagesThat(hasItem("admin.jaacs.application.admin.required")));

        // once
        defaultJaacsService.create(user, null);
    }

    @Test
    public void updateShouldThrowAValidationExceptionIfUserHasNoAdminAccess() throws Exception {
        // given
        when(permissionManager.hasPermission(Matchers.anyInt(), eq(user))).thenReturn(false);

        // expect
        exception.expect(ValidationFailureException.class);
        exception.expect(hasErrorMessagesThat(hasItem("admin.jaacs.application.admin.required")));

        // once
        defaultJaacsService.update(user, null);
    }

    @Test
    public void validateApplicationShouldCheckNameAndCredentialAndRemoteAddresses() throws Exception {
        // given
        when(app.getName()).thenReturn("");
        when(app.getCredential()).thenReturn(new PasswordCredential(""));
        when(app.getRemoteAddresses()).thenReturn(singleton(new RemoteAddress("256.256.256.256")));

        //expect
        exception.expect(ValidationFailureException.class);
        exception.expect(allOf(
                containsNamedErrorThat("name", equalTo("admin.jaacs.application.remote.application.name.required")),
                containsNamedErrorThat("credential", equalTo("admin.jaacs.application.password.empty")),
                containsNamedErrorThat("remoteAddresses", equalTo("admin.jaacs.application.remote.address.invalid.ip [256.256.256.256]"))

        ));

        //once
        defaultJaacsService.validateApplication(user, app);
    }

    private Matcher<ValidationFailureException> hasErrorMessagesThat(final Matcher<Iterable<? super String>> messageMatcher) {
        return new FeatureMatcher<ValidationFailureException, Iterable<String>>(messageMatcher, "contains messages that", "messages") {
            @Override
            protected Iterable<String> featureValueOf(final ValidationFailureException actual) {
                return actual.errors().getErrorMessages();
            }
        };
    }

    private Matcher<ValidationFailureException> containsNamedErrorThat(final String error, final Matcher<String> errorTextMatcher) {
        return new FeatureMatcher<ValidationFailureException, String>(errorTextMatcher, "contains messages that", "messages") {
            @Override
            protected String featureValueOf(final ValidationFailureException actual) {
                return actual.errors().getErrors().get(error);
            }
        };
    }
}
