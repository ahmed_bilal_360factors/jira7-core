package com.atlassian.jira.upgrade.tasks.role.scenarios;

import cucumber.deps.com.thoughtworks.xstream.annotations.XStreamConverter;
import cucumber.runtime.CommaSeparatedStringToListConverter;

import java.util.List;

/**
 * Represents table of user with groups. Example table:
 * <pre>
 * | name       | permissions                   |
 * | users      | USE                           |
 * | admins     | ADMINISTER                    |
 * | useAdmins  | USE, SYSTEM_ADMIN             |
 * | sysAdmins  | SYSTEM_ADMIN                  |
 * | everything | SYSTEM_ADMIN, ADMINISTER, USE |
 * </pre>
 */
public class UserWithGroups {
    private final String user;

    @XStreamConverter(CommaSeparatedStringToListConverter.class)
    private final List<String> groups;

    private final boolean disabled;

    public UserWithGroups(final String user, final List<String> groups) {
        this(user, groups, false);
    }

    public UserWithGroups(final String user, final List<String> groups, final boolean disabled) {
        this.user = user;
        this.groups = groups;
        this.disabled = disabled;
    }


    public String getUser() {
        return user;
    }

    public List<String> getGroups() {
        return groups;
    }

    public boolean isDisabled() {
        return disabled;
    }
}
