package com.atlassian.jira.event.issue;

import com.atlassian.jira.issue.IssueFieldConstants;
import com.atlassian.jira.issue.history.ChangeItemBean;
import com.google.common.collect.ImmutableList;
import org.junit.Test;

import java.util.Optional;

import static com.atlassian.jira.matchers.OptionalMatchers.none;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class ChangeItemBeanKitTest {

    @Test
    public void test_when_change_has_not_really_happened() throws Exception {

        ImmutableList<ChangeItemBean> changes = ImmutableList.of(
                createChangeItemBean(IssueFieldConstants.STATUS, null, null),
                createChangeItemBean(IssueFieldConstants.STATUS, "same", "same")
        );

        Optional<ChangeItemBean> actual = ChangeItemBeanKit.getFieldChange(IssueFieldConstants.STATUS, changes);
        assertThat(actual, is(none()));
    }

    @Test
    public void test_when_change_has_really_happened() throws Exception {
        ImmutableList<ChangeItemBean> changes = ImmutableList.of(
                createChangeItemBean(IssueFieldConstants.STATUS, "same", "different"),
                createChangeItemBean(IssueFieldConstants.DESCRIPTION, "was", "is now")
        );

        Optional<ChangeItemBean> actual = ChangeItemBeanKit.getFieldChange(IssueFieldConstants.STATUS, changes);
        assertThat(actual.isPresent(), equalTo(true));
        assertThat(actual.get().getField(), equalTo(IssueFieldConstants.STATUS));
        assertThat(actual.get().getFrom(), equalTo("same"));
        assertThat(actual.get().getTo(), equalTo("different"));

    }

    @Test
    public void test_when_change_has_really_happened_from_null() throws Exception {
        ImmutableList<ChangeItemBean> changes = ImmutableList.of(
                createChangeItemBean(IssueFieldConstants.STATUS, null, "new"),
                createChangeItemBean(IssueFieldConstants.DESCRIPTION, "was", "is now")
        );

        Optional<ChangeItemBean> actual = ChangeItemBeanKit.getFieldChange(IssueFieldConstants.STATUS, changes);
        assertThat(actual.isPresent(), equalTo(true));
        assertThat(actual.get().getField(), equalTo(IssueFieldConstants.STATUS));
    }

    @Test
    public void test_when_change_has_really_happened_to_null() throws Exception {
        ImmutableList<ChangeItemBean> changes = ImmutableList.of(
                createChangeItemBean(IssueFieldConstants.STATUS, "old", null),
                createChangeItemBean(IssueFieldConstants.DESCRIPTION, "was", "is now")
        );

        Optional<ChangeItemBean> actual = ChangeItemBeanKit.getFieldChange(IssueFieldConstants.STATUS, changes);
        assertThat(actual.isPresent(), equalTo(true));
        assertThat(actual.get().getField(), equalTo(IssueFieldConstants.STATUS));
    }

    private ChangeItemBean createChangeItemBean(String fieldName, String from, String to) {
        return new ChangeItemBean(ChangeItemBean.STATIC_FIELD, fieldName, from, from, to, to);
    }
}