package com.atlassian.jira.upgrade.tasks.role.scenarios;

import cucumber.deps.com.thoughtworks.xstream.converters.SingleValueConverter;

/**
 * @since v7.0
 */
public class GroupNameConverter implements SingleValueConverter {
    @Override
    public String toString(final Object obj) {
        return obj.toString();
    }

    @Override
    public Object fromString(final String str) {
        return GroupName.valueOf(str);
    }

    @Override
    public boolean canConvert(final Class type) {
        return GroupName.class.equals(type);
    }
}
