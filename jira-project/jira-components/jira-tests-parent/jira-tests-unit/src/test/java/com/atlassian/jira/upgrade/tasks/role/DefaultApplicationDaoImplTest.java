package com.atlassian.jira.upgrade.tasks.role;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.jira.mock.ofbiz.MockGenericValue;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import java.util.Arrays;
import java.util.Collections;
import java.util.Set;
import java.util.stream.Collectors;

import static com.google.common.collect.ImmutableSet.of;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class DefaultApplicationDaoImplTest {
    private static final ApplicationKey JIRA_CORE = ApplicationKey.valueOf("jira-core");
    private static final ApplicationKey JIRA_SOFTWARE = ApplicationKey.valueOf("jira-software");
    private static final org.ofbiz.core.entity.GenericValue JIRA_CORE_GV = toGV(JIRA_CORE.toString());
    private static final org.ofbiz.core.entity.GenericValue JIRA_SOFTWARE_GV = toGV(JIRA_SOFTWARE.toString());
    private static final org.ofbiz.core.entity.GenericValue JIRA_CORRUPTED_KEY_GV = toGV("j!r@-corrupted");

    @Rule
    public final MethodRule initMockito = MockitoJUnit.rule();

    private DefaultApplicationDaoImpl applicationDao;

    @Mock
    private OfBizDelegator delegator;

    @Before
    public void setUp() throws Exception {
        applicationDao = new DefaultApplicationDaoImpl(delegator);
    }

    @Test
    public void verifyGetApplicationsWhenApplicationsAreAvailable() {
        when(delegator.findAll(eq("LicenseRoleDefault"))).thenReturn(ImmutableList.of(JIRA_CORE_GV, JIRA_SOFTWARE_GV));
        assertThat(applicationDao.get(), Matchers.containsInAnyOrder(getExpectedApplications("jira-core", "jira-software")));
    }

    @Test
    public void verifyGetApplicationsWhenNoApplications() {
        when(delegator.findAll(eq("LicenseRoleDefault"))).thenReturn(Collections.emptyList());
        assertThat(applicationDao.get(), Matchers.emptyCollectionOf(ApplicationKey.class));
    }

    @Test
    public void verifyUpdateApplicationsWhenNoApplications() {
        when(delegator.findAll(eq("LicenseRoleDefault"))).thenReturn(Collections.emptyList());
        applicationDao.setApplicationsAsDefault(of(JIRA_CORE));

        verify(delegator).createValue(eq("LicenseRoleDefault"), eq(ImmutableMap.of("licenseRoleName", "jira-core")));
    }

    @Test
    public void verifyUpdateApplicationsWhenUpdatingSoftwareAndCoreWhenSoftwareExists() {
        when(delegator.findAll(eq("LicenseRoleDefault"))).thenReturn(ImmutableList.of(JIRA_SOFTWARE_GV));
        applicationDao.setApplicationsAsDefault(of(JIRA_CORE, JIRA_SOFTWARE));

        verify(delegator).createValue(eq("LicenseRoleDefault"), eq(ImmutableMap.of("licenseRoleName", "jira-core")));
    }

    @Test
    public void verifyUpdateApplicationsWhenUpdatingSoftwareWhenCoreExists() {
        when(delegator.findAll(eq("LicenseRoleDefault"))).thenReturn(ImmutableList.of(JIRA_CORE_GV));
        applicationDao.setApplicationsAsDefault(of(JIRA_SOFTWARE));

        verify(delegator).createValue(eq("LicenseRoleDefault"), eq(ImmutableMap.of("licenseRoleName", "jira-software")));
    }

    @Test
    public void verifyUpdateApplicationsWhenUpdatingSoftwareAndCoreWhenNoExists() {
        when(delegator.findAll(eq("LicenseRoleDefault"))).thenReturn(Collections.emptyList());
        applicationDao.setApplicationsAsDefault(of(JIRA_CORE, JIRA_SOFTWARE));

        verify(delegator).createValue(eq("LicenseRoleDefault"), eq(ImmutableMap.of("licenseRoleName", "jira-core")));
        verify(delegator).createValue(eq("LicenseRoleDefault"), eq(ImmutableMap.of("licenseRoleName", "jira-software")));
    }

    @Test
    public void verifyGetApplicationWithCorruptedKey() {
        when(delegator.findAll(eq("LicenseRoleDefault"))).thenReturn(ImmutableList.of(JIRA_CORE_GV, JIRA_CORRUPTED_KEY_GV));
        final Set<ApplicationKey> applicationKeys = applicationDao.get();

        assertThat("Only valid application keys should be returned by .get()", ImmutableList.of(JIRA_CORE), Matchers.containsInAnyOrder(applicationKeys.toArray()));
    }

    private Object[] getExpectedApplications(String... applications) {
        return Arrays.stream(applications).map(ApplicationKey::valueOf).collect(Collectors.toList()).toArray();
    }

    private static MockGenericValue toGV(final String theApplicaiton) {
        return new MockGenericValue("LicenseRoleDefault", ImmutableMap.of("licenseRoleName", theApplicaiton));
    }
}
