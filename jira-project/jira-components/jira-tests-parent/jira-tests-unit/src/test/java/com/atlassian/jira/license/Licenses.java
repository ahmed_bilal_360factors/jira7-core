package com.atlassian.jira.license;

import com.atlassian.jira.test.util.lic.License;
import com.atlassian.jira.test.util.lic.LicenseReader;

final class Licenses {
    /**
     * Sample (version 2) license with 3 embedded roles.
     */
    static final License LICENSE_WITH_3_APPS = getLicense("three-app-roles.lic");

    /**
     * Sample (version 2) license with 1 embedded role.
     */
    static final License LICENSE_WITH_1_APP = getLicense("one-app-role.lic");

    /**
     * Sample (version 2) license with no embedded roles.
     */
    static final License LICENSE_WITH_0_APPS = getLicense("no-app-roles.lic");

    /**
     * Sample (version 2) license with 3 embedded roles, 1 unlimited, 1 with 0 seats, 1 with 1 seat.
     */
    static final License
            LICENSE_WITH_SOFTWARE_UNLIMITED_USERS = getLicense("unlimited-app-role.lic");

    /**
     * Software license role
     */
    static final License LICENSE_WITH_SOFTWARE_APP = getLicense("software-app.lic");

    /**
     * ELA license with jira-core, jira-reference, jira-software, jira-func-test, jira-servicedesk
     */
    static final License LICENSE_ELA_4_ROLES = getLicense("ela-core-sw-servicedesk-test-reference-roles.lic");

    /**
     * License with no-roles.
     */
    static final License LICENSE_NO_ROLES = getLicense("no-app-roles.lic");

    /**
     * License with jira-software
     */
    static final License LICENSE_ONE_ROLE = getLicense("one-app-role.lic");

    /**
     * License with jira-core, jira-software, jira-servicedesk
     */
    static final License LICENSE_THREE_ROLES = getLicense("three-app-roles.lic");

    /**
     * License with jira-software
     */
    static final License LICENSE_ONE_APP_ROLE_NO_JIRA_ACTIVE = getLicense("single-app-role-without-jira-active-flag.lic");

    /**
     * A license with jira-core but with an explicit application name set in the Description.
     *
     * @see com.atlassian.extras.common.LicensePropertiesConstants#DESCRIPTION
     */
    static final License LICENSE_JIRA_HARDCORE = getLicense("jira-core-with-explicit-description.lic");

    /**
     * A license with no active products. It can't be parsed.
     */
    static final String LICENSE_NO_APP_ROLES_INVALID = getString("no-app-roles-invalid.lic");

    private Licenses() {
    }

    private static License getLicense(final String license) {
        return LicenseReader.readLicense(license, Licenses.class);
    }

    private static String getString(final String license) {
        return LicenseReader.readLicenseEncodedString(license, Licenses.class);
    }
}
