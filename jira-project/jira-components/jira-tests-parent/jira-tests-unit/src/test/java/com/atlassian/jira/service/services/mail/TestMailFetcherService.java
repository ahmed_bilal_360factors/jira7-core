package com.atlassian.jira.service.services.mail;

import com.atlassian.jira.JiraApplicationContext;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.mail.MailLoggingManager;
import com.atlassian.jira.mail.MailThreadManager;
import com.atlassian.jira.mail.settings.MailSettings;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.service.util.handler.MessageHandler;
import com.atlassian.jira.service.util.handler.MessageHandlerContext;
import com.atlassian.jira.service.util.handler.MessageHandlerExecutionMonitor;
import com.atlassian.jira.service.util.handler.MessageHandlerFactory;
import com.atlassian.jira.template.VelocityTemplatingEngine;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.NoopI18nFactory;
import com.atlassian.mail.Email;
import com.atlassian.mail.MailException;
import com.atlassian.mail.MailProtocol;
import com.atlassian.mail.server.MailServer;
import com.atlassian.mail.server.MailServerManager;
import com.atlassian.mail.server.SMTPMailServer;
import com.google.common.base.Optional;
import com.opensymphony.module.propertyset.PropertySet;
import com.sun.mail.imap.IMAPMessage;
import com.sun.mail.pop3.POP3Message;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.mockito.Answers;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.FolderClosedException;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Store;
import javax.mail.search.FlagTerm;

import static org.hamcrest.Matchers.equalToIgnoringCase;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.argThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestMailFetcherService {
    private static final String MESSAGE_ID = "contentId-18877260-9924014896@localhost";
    private static final String HOSTNAME = "hostname";
    private static final String USERNAME = "username";
    private static final String PASSWORD = "password";
    private static final int PORT = 143;
    private static final Long MAIL_SERVER_ID = 1L;
    private static final Long MAIL_SERVER2_ID = 2L;
    private static final String FOLDER_NAME = "INBOX";
    private static final String FOLDER2_NAME = "FOLDER";
    private static final String MESSAGE_ID_HEADER = "Message-id";
    private static final String FIRST_POP3_MESSAGE_ID = "firstPOP3MessageId";
    private static final String SECOND_POP3_MESSAGE_ID = "secondPOP3MessageId";
    private static final String FIRST_IMAP_MESSAGE_ID = "firstIMAPMessageId";
    private static final String SECOND_IMAP_MESSAGE_ID = "secondIMAPMessageId";
    private static final String POPSERVER = "popserver";

    @Mock
    PropertySet props, props2;

    @Mock
    MailServerManager mailServerManager;

    @Mock
    MessageHandlerContext context;

    @Mock
    MessageHandler handler;

    @Mock
    Message problematicMessage;

    @Mock
    Message newMessage;

    @Mock
    Folder folder, folder2;

    @Mock
    Store store;

    @Mock
    DeadLetterStore deadLetterStore;

    @AvailableInContainer
    @Mock
    MailThreadManager mailThreadManager;

    @Mock
    MessageHandlerExecutionMonitor monitor;

    @AvailableInContainer
    @Mock
    MailSettings mailSettings;

    @Mock
    MailSettings.Fetch fetch;

    @AvailableInContainer
    @Mock
    MailLoggingManager loggingManager;

    @AvailableInContainer
    @Mock
    MessageHandlerFactory messageHandlerFactory;

    @Mock
    Logger logger;

    @Mock
    MailServer mailServer;

    @Mock
    SMTPMailServer smtpMailServer;

    @AvailableInContainer
    private I18nHelper.BeanFactory beanFactory = new NoopI18nFactory();

    @Mock
    @AvailableInContainer
    JiraApplicationContext jiraApplicationContext;

    @Mock(answer = Answers.RETURNS_MOCKS)
    @AvailableInContainer
    VelocityTemplatingEngine velocityTemplatingEngine;

    @Mock(answer = Answers.RETURNS_MOCKS)
    @AvailableInContainer
    OfBizDelegator ofBizDelegator;

    @Rule
    public RuleChain mockItAll = MockitoMocksInContainer.forTest(this);

    private MailFetcherService service;
    private POP3Message firstPOP3Message;
    private POP3Message secondPOP3Message;
    private IMAPMessage firstImapMessage;
    private IMAPMessage secondIMapMessage;

    @Before
    public void setUp() throws Exception {
        when(logger.isDebugEnabled()).thenReturn(true);

        when(context.getMonitor()).thenReturn(monitor);
        when(mailSettings.fetch()).thenReturn(fetch);

        when(loggingManager.getIncomingMailChildLogger(anyString())).thenReturn(logger);

        when(fetch.isDisabled()).thenReturn(false);

        when(props.getString("handler")).thenReturn("handler");
        when(props2.getString("handler")).thenReturn("handler");
        when(messageHandlerFactory.getHandler("handler")).thenReturn(handler);

        when(props.exists(POPSERVER)).thenReturn(true);
        when(props.getString(POPSERVER)).thenReturn(MAIL_SERVER_ID.toString());

        when(props2.exists(POPSERVER)).thenReturn(true);
        when(props2.getString(POPSERVER)).thenReturn(MAIL_SERVER2_ID.toString());

        when(mailServerManager.getMailServer(MAIL_SERVER_ID)).thenReturn(mailServer);
        when(mailServerManager.getMailServer(MAIL_SERVER2_ID)).thenReturn(mailServer);
        when(mailServerManager.getDefaultSMTPMailServer()).thenReturn(smtpMailServer);

        when(mailServer.getHostname()).thenReturn(HOSTNAME);
        when(mailServer.getUsername()).thenReturn(USERNAME);
        when(mailServer.getPassword()).thenReturn(PASSWORD);

        when(mailServer.getMailProtocol()).thenReturn(MailProtocol.IMAP);
        when(mailServer.getPort()).thenReturn(Integer.toString(PORT));

        when(context.isRealRun()).thenReturn(true);

        when(store.getFolder(FOLDER_NAME)).thenReturn(folder);
        when(store.getFolder(FOLDER2_NAME)).thenReturn(folder2);
        when(folder.search(any(FlagTerm.class))).thenReturn(new Message[]{problematicMessage});
        when(problematicMessage.getHeader(argThat(equalToIgnoringCase(MESSAGE_ID_HEADER)))).thenReturn(new String[]{MESSAGE_ID});
        when(newMessage.getHeader(argThat(equalToIgnoringCase(MESSAGE_ID_HEADER)))).thenReturn(new String[]{"abc"});

        service = createMailFetcherService();

        firstPOP3Message = mockMessage(POP3Message.class, FIRST_POP3_MESSAGE_ID, true);
        firstImapMessage = mockMessage(IMAPMessage.class, FIRST_IMAP_MESSAGE_ID, true);
        secondPOP3Message = mockMessage(POP3Message.class, SECOND_POP3_MESSAGE_ID, true);
        secondIMapMessage = mockMessage(IMAPMessage.class, SECOND_IMAP_MESSAGE_ID, true);
    }

    /**
     * This cover scenario when a mail with a large attachment is processed. CreateIssueHandler will create an issue
     * first and then it will proceed with downloading attachments during which it will fail with a
     * FolderClosedIOException. At the end the handler returns true indicating that the problematicMessage has been
     * processed and can be deleted but when we attempt to do it a FolderClosedException is thrown (due to previous
     * error in attachment processing). This problematic email will be left in the mailbox and the next time we process
     * email a duplicate issue will be created and we don't want that.
     */
    @Test
    public void problematicMailSkippedInSubsequentRuns() throws Exception {
        when(handler.handleMessage(any(Message.class), any(MessageHandlerContext.class))).thenReturn(true);
        doThrow(new FolderClosedException(folder)).doNothing().when(problematicMessage).setFlag(Flags.Flag.DELETED, true);
        when(deadLetterStore.exists(MESSAGE_ID, MAIL_SERVER_ID, FOLDER_NAME)).thenReturn(false).thenReturn(true).thenReturn(false);

        service.runImpl(context);

        // new message has arrived after run 1 has completed
        when(folder.search(any(FlagTerm.class))).thenReturn(new Message[]{problematicMessage, newMessage});

        service.runImpl(context);

        InOrder inOrder = inOrder(store, handler, problematicMessage, newMessage);
        // 1st run - problematicMessage is processed and we attempt to delete it but this fails with a FolderClosedException
        inOrder.verify(store).connect(HOSTNAME, PORT, USERNAME, PASSWORD);
        inOrder.verify(handler).handleMessage(eq(problematicMessage), any(MessageHandlerContext.class));
        inOrder.verify(problematicMessage).setFlag(eq(Flags.Flag.DELETED), eq(true));
        // 2nd run - problematicMessage is skipped and message is deleted
        inOrder.verify(store).connect(HOSTNAME, PORT, USERNAME, PASSWORD);
        inOrder.verify(handler, never()).handleMessage(eq(problematicMessage), any(MessageHandlerContext.class));
        inOrder.verify(problematicMessage).setFlag(eq(Flags.Flag.DELETED), eq(true));
        // 2nd run - but the new message is processed as usual
        inOrder.verify(handler).handleMessage(eq(newMessage), any(MessageHandlerContext.class));
        inOrder.verify(newMessage).setFlag(eq(Flags.Flag.DELETED), eq(true));
    }

    /**
     * When mail handler encounters a failure while processing attachments (typically a FolderClosedIOException) the whole communication
     * to mail server is lost. An attempt to forward this email (if configured) will also fail but this should not prevent
     * the message from being deleted if the handler has used {@link MessageHandlerExecutionMonitor#markMessageForDeletion(String)}
     */
    @Test
    public void markMessageForDeletionRespectedOnForwardEmailFailure() throws Exception {
        doThrow(new MailException()).when(smtpMailServer).send(any(Email.class));
        when(handler.handleMessage(eq(problematicMessage), any(MessageHandlerContext.class))).thenAnswer(new Answer<Boolean>() {
            @Override
            public Boolean answer(final InvocationOnMock invocation) throws Throwable {
                Object[] args = invocation.getArguments();
                MessageHandlerContext context = (MessageHandlerContext) args[1];
                // return false but mark the message for deletion
                context.getMonitor().markMessageForDeletion("Some reason");
                return false;
            }
        });
        when(props.getString("forwardEmail")).thenReturn("dev@null");

        service.runImpl(context);

        InOrder inOrder = inOrder(store, handler, problematicMessage, monitor);
        inOrder.verify(store).connect(HOSTNAME, PORT, USERNAME, PASSWORD);
        inOrder.verify(handler).handleMessage(eq(problematicMessage), any(MessageHandlerContext.class));
        inOrder.verify(monitor).markMessageForDeletion(anyString());
        inOrder.verify(problematicMessage).setFlag(eq(Flags.Flag.DELETED), eq(true));
    }

    @Test
    public void deletesExpiredDeadLetterEntriesEachRun() {
        service.runImpl(context);
        verify(deadLetterStore).deleteOldDeadLetters();
    }

    @Test
    public void invalidatesEachPOP3MessageAfterProcessing() throws Exception {
        when(folder.search(any(FlagTerm.class))).thenReturn(new Message[]{firstPOP3Message, secondPOP3Message});

        service.runImpl(context);

        InOrder inOrder = inOrder(handler, firstPOP3Message, secondPOP3Message);
        inOrder.verify(handler).handleMessage(eq(firstPOP3Message), any(MessageHandlerContext.class));
        inOrder.verify(firstPOP3Message).invalidate(true);
        inOrder.verify(handler).handleMessage(eq(secondPOP3Message), any(MessageHandlerContext.class));
        inOrder.verify(secondPOP3Message).invalidate(true);
    }

    @Test
    public void dryRunDoesNotDeleteMessages() throws Exception {
        when(context.isRealRun()).thenReturn(false);
        when(folder.search(any(FlagTerm.class))).thenReturn(new Message[]{firstPOP3Message, secondPOP3Message});

        service.runImpl(context);

        InOrder inOrder = inOrder(handler, firstPOP3Message, secondPOP3Message);
        inOrder.verify(handler).handleMessage(eq(firstPOP3Message), any(MessageHandlerContext.class));
        inOrder.verify(firstPOP3Message, never()).setFlag(eq(Flags.Flag.DELETED), eq(true));
        inOrder.verify(handler).handleMessage(eq(secondPOP3Message), any(MessageHandlerContext.class));
        inOrder.verify(secondPOP3Message, never()).setFlag(eq(Flags.Flag.DELETED), eq(true));
    }

    @Test
    public void createsDeadLetterEntryForPOP3MessageWhenDeleting() throws Exception {
        when(folder.search(any(FlagTerm.class))).thenReturn(new Message[]{firstPOP3Message, secondPOP3Message});

        service.runImpl(context);

        InOrder inOrder = inOrder(handler, firstPOP3Message, secondPOP3Message, deadLetterStore);
        inOrder.verify(handler).handleMessage(eq(firstPOP3Message), any(MessageHandlerContext.class));
        inOrder.verify(firstPOP3Message).setFlag(eq(Flags.Flag.DELETED), eq(true));
        inOrder.verify(deadLetterStore).createOrUpdate(FIRST_POP3_MESSAGE_ID, MAIL_SERVER_ID, FOLDER_NAME);
        inOrder.verify(handler).handleMessage(eq(secondPOP3Message), any(MessageHandlerContext.class));
        inOrder.verify(secondPOP3Message).setFlag(eq(Flags.Flag.DELETED), eq(true));
        inOrder.verify(deadLetterStore).createOrUpdate(SECOND_POP3_MESSAGE_ID, MAIL_SERVER_ID, FOLDER_NAME);
    }

    @Test
    public void clearsDeadLettersForPOP3MessagesAfterClosingFolder() throws Exception {
        when(folder.search(any(FlagTerm.class))).thenReturn(new Message[]{firstPOP3Message, secondPOP3Message});

        service.runImpl(context);

        InOrder inOrder = inOrder(handler, folder, store, deadLetterStore);
        inOrder.verify(handler).handleMessage(eq(firstPOP3Message), any(MessageHandlerContext.class));
        inOrder.verify(handler).handleMessage(eq(secondPOP3Message), any(MessageHandlerContext.class));
        inOrder.verify(folder).close(true);
        inOrder.verify(store).close();
        inOrder.verify(deadLetterStore).delete(FIRST_POP3_MESSAGE_ID, MAIL_SERVER_ID, FOLDER_NAME);
        inOrder.verify(deadLetterStore).delete(SECOND_POP3_MESSAGE_ID, MAIL_SERVER_ID, FOLDER_NAME);
    }

    @Test
    public void createsDeadLetterEntryForIMAPMessageWhenDeleteFails() throws Exception {
        when(folder.search(any(FlagTerm.class))).thenReturn(new Message[]{firstImapMessage, secondIMapMessage});
        doThrow(new FolderClosedException(folder)).when(secondIMapMessage).setFlag(Flags.Flag.DELETED, true);

        service.runImpl(context);

        InOrder inOrder = inOrder(handler, secondIMapMessage, deadLetterStore);
        inOrder.verify(handler).handleMessage(eq(secondIMapMessage), any(MessageHandlerContext.class));
        inOrder.verify(secondIMapMessage).setFlag(eq(Flags.Flag.DELETED), eq(true));
        inOrder.verify(deadLetterStore).createOrUpdate(SECOND_IMAP_MESSAGE_ID, MAIL_SERVER_ID, FOLDER_NAME);
    }

    @Test
    public void clearsDeadLettersForIMAPMessagesAfterDelete() throws Exception {
        when(folder.search(any(FlagTerm.class))).thenReturn(new Message[]{firstImapMessage, secondIMapMessage});

        service.runImpl(context);

        InOrder inOrder = inOrder(handler, firstImapMessage, secondIMapMessage, deadLetterStore);
        inOrder.verify(handler).handleMessage(eq(firstImapMessage), any(MessageHandlerContext.class));
        inOrder.verify(firstImapMessage).setFlag(eq(Flags.Flag.DELETED), eq(true));
        inOrder.verify(deadLetterStore).delete(FIRST_IMAP_MESSAGE_ID, MAIL_SERVER_ID, FOLDER_NAME);

        inOrder.verify(handler).handleMessage(eq(secondIMapMessage), any(MessageHandlerContext.class));
        inOrder.verify(secondIMapMessage).setFlag(eq(Flags.Flag.DELETED), eq(true));
        inOrder.verify(deadLetterStore).delete(SECOND_IMAP_MESSAGE_ID, MAIL_SERVER_ID, FOLDER_NAME);
    }

    @Test
    public void stopsProcessingMessagesOnFolderClosedExceptionInMailHandler() throws Exception {
        when(folder.search(any(FlagTerm.class))).thenReturn(new Message[]{firstImapMessage, secondIMapMessage});
        doThrow(new FolderClosedException(folder)).when(handler).handleMessage(eq(firstImapMessage), any(MessageHandlerContext.class));

        service.runImpl(context);

        InOrder inOrder = inOrder(handler, firstImapMessage, secondIMapMessage, folder, store);
        inOrder.verify(handler).handleMessage(eq(firstImapMessage), any(MessageHandlerContext.class));
        inOrder.verify(firstImapMessage, never()).setFlag(eq(Flags.Flag.DELETED), eq(true));
        inOrder.verify(handler, never()).handleMessage(eq(secondIMapMessage), any(MessageHandlerContext.class));
        inOrder.verify(folder).close(true);
        inOrder.verify(store).close();
    }

    /**
     * This is the case when one message ends up in two folders on the same server and each folder is
     * processed by a separate handler.
     * When first handler runs into problems while processing the message the other handler should still process
     * message in its folder even though they have the sam Message-id.
     */
    @Test
    public void twoHandlersOnSeparateFoldersHandleTheSameMessageFirstOneFails() throws Exception {
        MailFetcherService service2 = createMailFetcherService(FOLDER2_NAME, props);
        IMAPMessage firstImapMessageInFolder2 = mockMessage(IMAPMessage.class, FIRST_IMAP_MESSAGE_ID, true);

        when(folder.search(any(FlagTerm.class))).thenReturn(new Message[]{firstImapMessage});
        when(handler.handleMessage(eq(firstImapMessage), any(MessageHandlerContext.class))).thenReturn(true);
        doThrow(new FolderClosedException(folder)).when(firstImapMessage).setFlag(Flags.Flag.DELETED, true);

        service.runImpl(context);

        when(folder2.search(any(FlagTerm.class))).thenReturn(new Message[]{firstImapMessageInFolder2});
        when(handler.handleMessage(eq(firstImapMessageInFolder2), any(MessageHandlerContext.class))).thenReturn(true);

        service2.runImpl(context);

        InOrder inOrder = inOrder(handler, firstImapMessage, deadLetterStore, firstImapMessageInFolder2);
        inOrder.verify(deadLetterStore).exists(FIRST_IMAP_MESSAGE_ID, MAIL_SERVER_ID, FOLDER_NAME);
        inOrder.verify(handler).handleMessage(eq(firstImapMessage), any(MessageHandlerContext.class));
        inOrder.verify(firstImapMessage).setFlag(eq(Flags.Flag.DELETED), eq(true));

        inOrder.verify(deadLetterStore).exists(FIRST_IMAP_MESSAGE_ID, MAIL_SERVER_ID, FOLDER2_NAME);
        inOrder.verify(handler).handleMessage(eq(firstImapMessageInFolder2), any(MessageHandlerContext.class));
        inOrder.verify(firstImapMessageInFolder2).setFlag(eq(Flags.Flag.DELETED), eq(true));
    }

    /**
     * This is the case when one message ends up in two mailservers and is processed by two separate handlers.
     * When first handler runs into problems while processing the message the other handler should still process
     * message in its folder even though they have the sam Message-id.
     */
    @Test
    public void twoHandlersOnSeparateServersHandleTheSameMessageFirstOneFails() throws Exception {
        MailFetcherService service2 = createMailFetcherService(FOLDER_NAME, props2);
        IMAPMessage firstImapMessageOnDifferentServer = mockMessage(IMAPMessage.class, FIRST_IMAP_MESSAGE_ID, true);

        when(folder.search(any(FlagTerm.class))).thenReturn(new Message[]{firstImapMessage});
        when(handler.handleMessage(eq(firstImapMessage), any(MessageHandlerContext.class))).thenReturn(true);
        doThrow(new FolderClosedException(folder)).when(firstImapMessage).setFlag(Flags.Flag.DELETED, true);

        service.runImpl(context);

        when(folder.search(any(FlagTerm.class))).thenReturn(new Message[]{firstImapMessageOnDifferentServer});
        when(handler.handleMessage(eq(firstImapMessageOnDifferentServer), any(MessageHandlerContext.class))).thenReturn(true);

        service2.runImpl(context);

        InOrder inOrder = inOrder(handler, firstImapMessage, deadLetterStore, firstImapMessageOnDifferentServer);
        inOrder.verify(deadLetterStore).exists(FIRST_IMAP_MESSAGE_ID, MAIL_SERVER_ID, FOLDER_NAME);
        inOrder.verify(handler).handleMessage(eq(firstImapMessage), any(MessageHandlerContext.class));
        inOrder.verify(firstImapMessage).setFlag(eq(Flags.Flag.DELETED), eq(true));

        inOrder.verify(deadLetterStore).exists(FIRST_IMAP_MESSAGE_ID, MAIL_SERVER2_ID, FOLDER_NAME);
        inOrder.verify(handler).handleMessage(eq(firstImapMessageOnDifferentServer), any(MessageHandlerContext.class));
        inOrder.verify(firstImapMessageOnDifferentServer).setFlag(eq(Flags.Flag.DELETED), eq(true));
    }

    private <T extends Message> T mockMessage(Class<T> clazz, final String messageId, final boolean handlerReturnValue) throws MessagingException {
        T message = mock(clazz);
        when(message.getHeader(argThat(equalToIgnoringCase(MESSAGE_ID_HEADER)))).thenReturn(new String[]{messageId});
        when(handler.handleMessage(eq(message), any(MessageHandlerContext.class))).thenReturn(handlerReturnValue);
        return message;
    }

    private MailFetcherService createMailFetcherService() throws Exception {
        return createMailFetcherService(FOLDER_NAME, props);
    }

    private MailFetcherService createMailFetcherService(final String folderName, final PropertySet props) throws Exception {
        MailFetcherService service = new MailFetcherService() {
            @Override
            MailServerManager getMailServerManager() {
                return mailServerManager;
            }

            @Override
            protected String getFolderName(final MailServer server) {
                return folderName;
            }

            @Override
            Optional<Store> getStore(final MailServer mailServer, final String protocol, final MessageHandlerExecutionMonitor monitor) {
                return Optional.of(store);
            }

            @Override
            DeadLetterStore getDeadLetterStore() {
                return deadLetterStore;
            }
        };
        service.init(props);
        return service;
    }

}
