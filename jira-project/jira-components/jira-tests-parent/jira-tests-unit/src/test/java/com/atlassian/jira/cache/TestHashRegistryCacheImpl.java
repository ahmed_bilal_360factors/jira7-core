package com.atlassian.jira.cache;

import com.atlassian.modzdetector.Modifications;
import com.atlassian.modzdetector.ModzDetector;
import com.atlassian.modzdetector.ModzRegistryException;
import org.junit.Test;

import java.lang.ref.SoftReference;

import static org.junit.Assert.assertSame;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @since v3.13
 */
public class TestHashRegistryCacheImpl {
    @Test
    public void testGetModifications() throws ModzRegistryException {
        final Modifications mods = new Modifications();

        final ModzDetector mockModzDetector = mock(ModzDetector.class);
        when(mockModzDetector.getModifiedFiles()).thenReturn(mods);

        final HashRegistryCacheImpl registry = new HashRegistryCacheImpl(mockModzDetector, new SoftReference<Modifications>(null));

        assertSame(mods, registry.getModifications());
    }
}
