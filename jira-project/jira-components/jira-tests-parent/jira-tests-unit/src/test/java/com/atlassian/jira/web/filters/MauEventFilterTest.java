package com.atlassian.jira.web.filters;

import com.atlassian.jira.config.InstanceFeatureManager;
import com.atlassian.jira.event.mau.MauEventService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Optional;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class MauEventFilterTest {

    @Mock
    private MauEventService mauEventService;
    @Mock
    private InstanceFeatureManager featureManager;
    @Mock
    private HttpServletRequest request;
    @Mock
    private FilterChain filterChain;

    @Test
    public void filterShouldNotTriggerEventIfHeaderProvided() throws IOException, ServletException {
        final MauEventFilter filter = createFilterWithService(mauEventService, featureManager);
        when(request.getHeader("x-atlassian-mau-ignore")).thenReturn("true");

        filter.doFilter(request, null, filterChain);

        verify(mauEventService, never()).triggerEvent();
    }

    @Test
    public void filterTriggersMauEvent() throws IOException, ServletException {
        final MauEventFilter filter = createFilterWithService(mauEventService, featureManager);

        filter.doFilter(request, null, filterChain);

        verify(mauEventService, times(1)).triggerEvent();
    }

    @Test
    public void filterCanHandleMauServiceNotBeingAvailable() throws IOException, ServletException {
        final MauEventFilter filter = createFilterWithService(null, featureManager);

        filter.doFilter(request, null, filterChain);  //this would throw an exception if its not null safe.
    }

    @Test
    public void filterCanHandleFeatureManagerNotBeingAvailable() throws IOException, ServletException {
        final MauEventFilter filter = createFilterWithService(mauEventService, null);

        filter.doFilter(request, null, filterChain);  //this would throw an exception if its not null safe.
    }

    private MauEventFilter createFilterWithService(final MauEventService mauEventService, final InstanceFeatureManager featureManager) {
        return new MauEventFilter() {

            @Override
            Optional<MauEventService> getMauEventService() {
                return Optional.ofNullable(mauEventService);
            }

            @Override
            Optional<InstanceFeatureManager> getFeatureManager() {
                return Optional.ofNullable(featureManager);
            }
        };


    }

}