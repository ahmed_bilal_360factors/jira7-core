package com.atlassian.jira.imports.project.ao.handler;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.internal.matchers.Null;
import org.mockito.runners.MockitoJUnitRunner;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.util.List;
import java.util.Map;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * @since v6.5
 */
@RunWith(MockitoJUnitRunner.class)
public class TestChainedAoSaxHandler {
    @Test
    public void testHandlerWithRealXml() throws ParserConfigurationException, SAXException, IOException {
        final String xml = "<?xml version='1.0' encoding='UTF-8'?>\n"
                + "<backup xmlns=\"http://www.atlassian.com/ao\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\n"
                + "  <database>\n"
                + "    <meta key=\"database.name\" value=\"PostgreSQL\"/>\n"
                + "    <meta key=\"database.version\" value=\"9.3.5\"/>\n"
                + "    <meta key=\"database.minorVersion\" value=\"3\"/>\n"
                + "    <meta key=\"database.majorVersion\" value=\"9\"/>\n"
                + "    <meta key=\"driver.name\" value=\"PostgreSQL Native Driver\"/>\n"
                + "    <meta key=\"driver.version\" value=\"PostgreSQL 9.0 JDBC4 (build 801)\"/>\n"
                + "  </database>\n"
                + "  <table name=\"AO_21D670_WHITELIST_RULES\">\n"
                + "    <column name=\"ALLOWINBOUND\" primaryKey=\"false\" autoIncrement=\"false\" sqlType=\"-7\" precision=\"1\"/>\n"
                + "    <column name=\"EXPRESSION\" primaryKey=\"false\" autoIncrement=\"false\" sqlType=\"12\" precision=\"1073741824\"/>\n"
                + "    <column name=\"ID\" primaryKey=\"true\" autoIncrement=\"true\" sqlType=\"4\" precision=\"10\"/>\n"
                + "    <column name=\"TYPE\" primaryKey=\"false\" autoIncrement=\"false\" sqlType=\"12\" precision=\"255\"/>\n"
                + "  </table>\n"
                + "  <table name=\"AO_38321B_CUSTOM_CONTENT_LINK\">\n"
                + "    <column name=\"CONTENT_KEY\" primaryKey=\"false\" autoIncrement=\"false\" sqlType=\"12\" precision=\"255\"/>\n"
                + "    <column name=\"ID\" primaryKey=\"true\" autoIncrement=\"true\" sqlType=\"4\" precision=\"10\"/>\n"
                + "    <column name=\"LINK_LABEL\" primaryKey=\"false\" autoIncrement=\"false\" sqlType=\"12\" precision=\"255\"/>\n"
                + "    <column name=\"LINK_URL\" primaryKey=\"false\" autoIncrement=\"false\" sqlType=\"12\" precision=\"255\"/>\n"
                + "    <column name=\"SEQUENCE\" primaryKey=\"false\" autoIncrement=\"false\" sqlType=\"4\" precision=\"10\"/>\n"
                + "  </table>\n"
                + "  <data tableName=\"AO_21D670_WHITELIST_RULES\">\n"
                + "    <column name=\"ALLOWINBOUND\"/>\n"
                + "    <column name=\"EXPRESSION\"/>\n"
                + "    <column name=\"ID\"/>\n"
                + "    <column name=\"TYPE\"/>\n"
                + "    <row>\n"
                + "      <boolean>false</boolean>\n"
                + "      <string>00bc4990-0e6d-35da-b719-c1e2d199099e</string>\n"
                + "      <integer>1</integer>\n"
                + "      <string>APPLICATION_LINK</string>\n"
                + "    </row>\n"
                + "    <row>\n"
                + "      <boolean>true</boolean>\n"
                + "      <string>bf9e23a3-cc04-36ee-b79b-9cedfd31c093</string>\n"
                + "      <integer>2</integer>\n"
                + "      <string>APPLICATION_LINK</string>\n"
                + "    </row>\n"
                + "    <row>\n"
                + "      <boolean>false</boolean>\n"
                + "      <string></string>\n"
                + "      <integer>3</integer>\n"
                + "      <string></string>\n"
                + "    </row>\n"
                + "  </data>\n"
                + "  <data tableName=\"AO_38321B_CUSTOM_CONTENT_LINK\">\n"
                + "    <column name=\"CONTENT_KEY\"/>\n"
                + "    <column name=\"ID\"/>\n"
                + "    <column name=\"LINK_LABEL\"/>\n"
                + "    <column name=\"LINK_URL\"/>\n"
                + "    <column name=\"SEQUENCE\"/>\n"
                + "  </data>\n"
                + "\n"
                + "</backup>";

        final ChainedAoSaxHandler handler = new ChainedAoSaxHandler();
        CollectingImportAoEntityHandler collectingImportAoEntityHandler = new CollectingImportAoEntityHandler();

        handler.registerHandler(collectingImportAoEntityHandler);

        final SAXParserFactory factory = SAXParserFactory.newInstance();
        final SAXParser saxParser = factory.newSAXParser();
        final InputSource inputSource = new InputSource(new StringReader(xml));
        // Scan the XML
        saxParser.parse(inputSource, handler);

        // Validate we got what we were after
        assertThat(handler.getEntityCount(), is(3L));
        assertThat(collectingImportAoEntityHandler.totalTables, is(2L));
        assertThat(collectingImportAoEntityHandler.totalRows, is(3L));
        assertThat(collectingImportAoEntityHandler.tables.size(), is(1));
        Map<String, List<Map<String, Object>>> tables = collectingImportAoEntityHandler.tables;
        assertThat(tables.containsKey("AO_21D670_WHITELIST_RULES"), is(Boolean.TRUE));

        final List<Map<String, Object>> table1 = tables.get("AO_21D670_WHITELIST_RULES");
        Map<String, Object> row = table1.get(0);
        assertThat((Boolean) row.get("ALLOWINBOUND"), is(Boolean.FALSE));
        assertThat((String) row.get("EXPRESSION"), is("00bc4990-0e6d-35da-b719-c1e2d199099e"));
        assertThat((Long) row.get("ID"), is(1L));
        assertThat((String) row.get("TYPE"), is("APPLICATION_LINK"));

        row = table1.get(1);
        assertThat((Boolean) row.get("ALLOWINBOUND"), is(Boolean.TRUE));
        assertThat((String) row.get("EXPRESSION"), is("bf9e23a3-cc04-36ee-b79b-9cedfd31c093"));
        assertThat((Long) row.get("ID"), is(2L));
        assertThat((String) row.get("TYPE"), is("APPLICATION_LINK"));

        row = table1.get(2);
        assertThat((Boolean) row.get("ALLOWINBOUND"), is(Boolean.FALSE));
        assertThat(row.get("EXPRESSION"), Null.NULL);
        assertThat((Long) row.get("ID"), is(3L));
        assertThat(row.get("TYPE"), Null.NULL);

    }

    @Test
    public void testHandlerWithBigXml() throws ParserConfigurationException, SAXException, IOException {
        final ChainedAoSaxHandler handler = new ChainedAoSaxHandler();
        CollectingImportAoEntityHandler collectingImportAoEntityHandler = new CollectingImportAoEntityHandler();

        handler.registerHandler(collectingImportAoEntityHandler);

        final SAXParserFactory factory = SAXParserFactory.newInstance();
        final SAXParser saxParser = factory.newSAXParser();
        InputStream inputStream = TestChainedAoSaxHandler.class.getResourceAsStream("/com/atlassian/jira/imports/xml/testActiveObjectsParser.xml");
        final InputSource inputSource = new InputSource(new InputStreamReader(inputStream));
        // Scan the XML
        saxParser.parse(inputSource, handler);

        // Validate we got what we were after
        assertThat(handler.getEntityCount(), is(350637L));
        assertThat(collectingImportAoEntityHandler.totalTables, is(68L));
        assertThat(collectingImportAoEntityHandler.totalRows, is(350637L));
        // We only collected those that had data
        assertThat(collectingImportAoEntityHandler.tables.size(), is(17));
        Map<String, List<Map<String, Object>>> tables = collectingImportAoEntityHandler.tables;
        assertThat(tables.containsKey("AO_21D670_WHITELIST_RULES"), is(Boolean.TRUE));

        final List<Map<String, Object>> table1 = tables.get("AO_21D670_WHITELIST_RULES");
        Map<String, Object> row = table1.get(0);
        assertThat((Boolean) row.get("ALLOWINBOUND"), is(Boolean.FALSE));
        assertThat((String) row.get("EXPRESSION"), is("00bc4990-0e6d-35da-b719-c1e2d199099e"));
        assertThat((Long) row.get("ID"), is(1L));
        assertThat((String) row.get("TYPE"), is("APPLICATION_LINK"));

    }

    @Test
    public void testOnlyCallsHandlerIfWanted() throws ParserConfigurationException, SAXException, IOException {
        final ChainedAoSaxHandler handler = new ChainedAoSaxHandler();
        CollectingImportAoEntityHandler whitelistHandler = new SingleAoEntityHandler("AO_21D670_WHITELIST_RULES");
        handler.registerHandler(whitelistHandler);
        CollectingImportAoEntityHandler customContentHandler = new SingleAoEntityHandler("AO_38321B_CUSTOM_CONTENT_LINK");
        handler.registerHandler(customContentHandler);

        final SAXParserFactory factory = SAXParserFactory.newInstance();
        final SAXParser saxParser = factory.newSAXParser();
        InputStream inputStream = TestChainedAoSaxHandler.class.getResourceAsStream("/com/atlassian/jira/imports/xml/testActiveObjectsParser.xml");
        final InputSource inputSource = new InputSource(new InputStreamReader(inputStream));
        // Scan the XML
        saxParser.parse(inputSource, handler);

        // Validate we got what we were after
        assertThat(handler.getEntityCount(), is(350637L));
        // Whitelist
        assertThat(whitelistHandler.totalTables, is(1L));
        assertThat(whitelistHandler.totalRows, is(10L));
        // We only collected those that had data
        assertThat(whitelistHandler.tables.size(), is(1));
        Map<String, List<Map<String, Object>>> tables = whitelistHandler.tables;
        assertThat(tables.containsKey("AO_21D670_WHITELIST_RULES"), is(Boolean.TRUE));

        final List<Map<String, Object>> table1 = tables.get("AO_21D670_WHITELIST_RULES");
        Map<String, Object> row = table1.get(0);
        assertThat((Boolean) row.get("ALLOWINBOUND"), is(Boolean.FALSE));
        assertThat((String) row.get("EXPRESSION"), is("00bc4990-0e6d-35da-b719-c1e2d199099e"));
        assertThat((Long) row.get("ID"), is(1L));
        assertThat((String) row.get("TYPE"), is("APPLICATION_LINK"));

        //Custom Content
        assertThat(customContentHandler.totalTables, is(1L));
        assertThat(customContentHandler.totalRows, is(0L));
        // We only collected those that had data
        assertThat(customContentHandler.tables.size(), is(0));
    }

    private static class SingleAoEntityHandler extends CollectingImportAoEntityHandler {
        private final String entity;

        private SingleAoEntityHandler(final String entity) {
            this.entity = entity;
        }

        @Override
        public boolean handlesEntity(final String entityName) {
            return entityName.equals(entity);
        }
    }
}
