package com.atlassian.jira.transaction;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;

import static com.google.common.collect.ImmutableList.of;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class RunnablesQueueImplTest {

    @Mock
    Runnable runnable;

    private RunnablesQueueImpl classUnderTest;

    @Before
    public void setUp() throws Exception {
        doNothing().when(runnable).run();

        classUnderTest = new RunnablesQueueImpl();
    }

    @Test
    public void testOffer() throws Exception {
        List<Runnable> expectedEvents = of(runnable, runnable);
        expectedEvents.forEach(e -> classUnderTest.offer(e));
        classUnderTest.runAndClear();
        verify(runnable, times(2)).run();
    }

    @Test
    public void testRunClearsQueue() throws Exception {
        List<Runnable> expectedEvents = of(runnable, runnable);
        expectedEvents.forEach(classUnderTest::offer);

        classUnderTest.runAndClear();
        assertThat(classUnderTest.queue().size(), equalTo(0));
    }

    @Test
    public void testClear() throws Exception {
        List<Runnable> expectedEvents = of(runnable, runnable);
        expectedEvents.forEach(classUnderTest::offer);

        classUnderTest.clear();
        assertThat(classUnderTest.queue().size(), equalTo(0));

        classUnderTest.runAndClear();

        verify(runnable, times(0)).run();
    }

    @Test
    public void testTestRuntimeExceptionSafety() throws Exception {
        Runnable exceptionThrower = () -> {
            throw new UnsupportedOperationException("Not implemented");
        };

        classUnderTest.offer(exceptionThrower);

        // no runtime exceptions get out
        classUnderTest.runAndClear();
    }

}