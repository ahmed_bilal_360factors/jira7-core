package com.atlassian.jira.jql.values;

import com.atlassian.jira.issue.security.IssueSecurityLevelManager;
import com.atlassian.jira.mock.ofbiz.MockGenericValue;
import com.atlassian.jira.ofbiz.FieldMap;
import com.atlassian.jira.user.ApplicationUser;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.ofbiz.core.entity.GenericEntityException;
import org.ofbiz.core.entity.GenericValue;

import java.util.Collection;
import java.util.List;
import java.util.Locale;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.empty;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestSecurityLevelClauseValuesGenerator {
    @Rule
    public MockitoRule mocks = MockitoJUnit.rule();

    @Mock
    private IssueSecurityLevelManager issueSecurityLevelManager;
    private SecurityLevelClauseValuesGenerator valuesGenerator;

    @Before
    public void setUp() throws Exception {
        valuesGenerator = new SecurityLevelClauseValuesGenerator(issueSecurityLevelManager) {
            @Override
            Locale getLocale(final ApplicationUser searcher) {
                return Locale.ENGLISH;
            }
        };
    }

    @Test
    public void testGetRelevantSecurityLevelsHappyPath() throws Exception {
        final MockGenericValue level1 = new MockGenericValue("SecLevel", FieldMap.build("name", "Aa lev"));
        final MockGenericValue level2 = new MockGenericValue("SecLevel", FieldMap.build("name", "A lev"));
        final MockGenericValue level3 = new MockGenericValue("SecLevel", FieldMap.build("name", "B lev"));
        final MockGenericValue level4 = new MockGenericValue("SecLevel", FieldMap.build("name", "C lev"));

        final Collection<GenericValue> levels = ImmutableList.of(level4, level3, level2, level1);
        when(issueSecurityLevelManager.getAllUsersSecurityLevels(null)).thenReturn(levels);

        final List<GenericValue> result = valuesGenerator.getRelevantSecurityLevels(null);

        assertEquals(result, levels);
    }

    @Test
    public void testGetRelevantSecurityLevelsNullLevels() throws Exception {
        when(issueSecurityLevelManager.getAllUsersSecurityLevels(null)).thenReturn(null);

        final List<GenericValue> result = valuesGenerator.getRelevantSecurityLevels(null);

        assertThat(result, empty());
    }

    @Test
    public void testGetRelevantSecurityLevelsException() throws Exception {
        when(issueSecurityLevelManager.getAllUsersSecurityLevels(null)).thenThrow(GenericEntityException.class);

        final List<GenericValue> result = valuesGenerator.getRelevantSecurityLevels(null);

        assertThat(result, empty());
    }

    @Test
    public void testGetPossibleValuesHappyPath() throws Exception {
        final MockGenericValue level1 = new MockGenericValue("SecLevel", FieldMap.build("name", "Aa lev"));
        final MockGenericValue level2 = new MockGenericValue("SecLevel", FieldMap.build("name", "A lev"));
        final MockGenericValue level3 = new MockGenericValue("SecLevel", FieldMap.build("name", "B lev"));
        final MockGenericValue level4 = new MockGenericValue("SecLevel", FieldMap.build("name", "C lev"));
        final List<GenericValue> levels = Lists.newArrayList(level4, level3, level2, level1);

        valuesGenerator = new SecurityLevelClauseValuesGenerator(issueSecurityLevelManager) {
            @Override
            List<GenericValue> getRelevantSecurityLevels(final ApplicationUser searcher) {
                return levels;
            }

            @Override
            Locale getLocale(final ApplicationUser searcher) {
                return Locale.ENGLISH;
            }
        };

        final ClauseValuesGenerator.Results result = valuesGenerator.getPossibleValues(null, "level", "", 10);

        assertThat(result.getResults(), contains(
                new ClauseValuesGenerator.Result(level1.getString("name")),
                new ClauseValuesGenerator.Result(level2.getString("name")),
                new ClauseValuesGenerator.Result(level3.getString("name")),
                new ClauseValuesGenerator.Result(level4.getString("name"))
        ));
    }

    @Test
    public void testGetPossibleValuesMatchFullValue() throws Exception {
        final MockGenericValue level1 = new MockGenericValue("SecLevel", FieldMap.build("name", "Aa lev"));
        final MockGenericValue level2 = new MockGenericValue("SecLevel", FieldMap.build("name", "A lev"));
        final MockGenericValue level3 = new MockGenericValue("SecLevel", FieldMap.build("name", "B lev"));
        final MockGenericValue level4 = new MockGenericValue("SecLevel", FieldMap.build("name", "C lev"));
        final List<GenericValue> levels = Lists.newArrayList(level4, level3, level2, level1);

        valuesGenerator = new SecurityLevelClauseValuesGenerator(issueSecurityLevelManager) {
            @Override
            List<GenericValue> getRelevantSecurityLevels(final ApplicationUser searcher) {
                return levels;
            }

            @Override
            Locale getLocale(final ApplicationUser searcher) {
                return Locale.ENGLISH;
            }
        };

        final ClauseValuesGenerator.Results result = valuesGenerator.getPossibleValues(null, "level", "Aa lev", 10);

        assertThat(result.getResults(), contains(
                new ClauseValuesGenerator.Result(level1.getString("name"))));
    }

    @Test
    public void testGetPossibleValuesExactMatchWithOthers() throws Exception {
        final MockGenericValue level1 = new MockGenericValue("SecLevel", FieldMap.build("name", "Aa lev"));
        final MockGenericValue level2 = new MockGenericValue("SecLevel", FieldMap.build("name", "Aa lev blah"));
        final MockGenericValue level3 = new MockGenericValue("SecLevel", FieldMap.build("name", "B lev"));
        final MockGenericValue level4 = new MockGenericValue("SecLevel", FieldMap.build("name", "C lev"));
        final List<GenericValue> levels = Lists.newArrayList(level4, level3, level2, level1);

        valuesGenerator = new SecurityLevelClauseValuesGenerator(issueSecurityLevelManager) {
            @Override
            List<GenericValue> getRelevantSecurityLevels(final ApplicationUser searcher) {
                return levels;
            }

            @Override
            Locale getLocale(final ApplicationUser searcher) {
                return Locale.ENGLISH;
            }
        };

        final ClauseValuesGenerator.Results result = valuesGenerator.getPossibleValues(null, "level", "Aa lev", 10);

        assertThat(result.getResults(), contains(
                new ClauseValuesGenerator.Result(level1.getString("name")),
                new ClauseValuesGenerator.Result(level2.getString("name"))));
    }

    @Test
    public void testGetPossibleValuesMatchNone() throws Exception {
        final MockGenericValue level1 = new MockGenericValue("SecLevel", FieldMap.build("name", "Aa lev"));
        final MockGenericValue level2 = new MockGenericValue("SecLevel", FieldMap.build("name", "A lev"));
        final MockGenericValue level3 = new MockGenericValue("SecLevel", FieldMap.build("name", "B lev"));
        final MockGenericValue level4 = new MockGenericValue("SecLevel", FieldMap.build("name", "C lev"));
        final List<GenericValue> levels = Lists.newArrayList(level4, level3, level2, level1);

        valuesGenerator = new SecurityLevelClauseValuesGenerator(issueSecurityLevelManager) {
            @Override
            List<GenericValue> getRelevantSecurityLevels(final ApplicationUser searcher) {
                return levels;
            }

            @Override
            Locale getLocale(final ApplicationUser searcher) {
                return Locale.ENGLISH;
            }
        };

        final ClauseValuesGenerator.Results result = valuesGenerator.getPossibleValues(null, "level", "Z", 10);

        assertThat(result.getResults(), empty());
    }

    @Test
    public void testGetPossibleValuesMatchSome() throws Exception {
        final MockGenericValue level1 = new MockGenericValue("SecLevel", FieldMap.build("name", "Aa lev"));
        final MockGenericValue level2 = new MockGenericValue("SecLevel", FieldMap.build("name", "A lev"));
        final MockGenericValue level3 = new MockGenericValue("SecLevel", FieldMap.build("name", "B lev"));
        final MockGenericValue level4 = new MockGenericValue("SecLevel", FieldMap.build("name", "C lev"));
        final List<GenericValue> levels = Lists.newArrayList(level4, level3, level2, level1);

        valuesGenerator = new SecurityLevelClauseValuesGenerator(issueSecurityLevelManager) {
            @Override
            List<GenericValue> getRelevantSecurityLevels(final ApplicationUser searcher) {
                return levels;
            }

            @Override
            Locale getLocale(final ApplicationUser searcher) {
                return Locale.ENGLISH;
            }
        };

        final ClauseValuesGenerator.Results result = valuesGenerator.getPossibleValues(null, "level", "a", 10);

        assertThat(result.getResults(), contains(
                new ClauseValuesGenerator.Result(level1.getString("name")),
                new ClauseValuesGenerator.Result(level2.getString("name"))));
    }

    @Test
    public void testGetPossibleValuesMatchToLimit() throws Exception {
        final MockGenericValue level1 = new MockGenericValue("SecLevel", FieldMap.build("name", "Aa lev"));
        final MockGenericValue level2 = new MockGenericValue("SecLevel", FieldMap.build("name", "A lev"));
        final MockGenericValue level3 = new MockGenericValue("SecLevel", FieldMap.build("name", "B lev"));
        final MockGenericValue level4 = new MockGenericValue("SecLevel", FieldMap.build("name", "C lev"));
        final List<GenericValue> levels = Lists.newArrayList(level4, level3, level2, level1);

        valuesGenerator = new SecurityLevelClauseValuesGenerator(issueSecurityLevelManager) {
            @Override
            List<GenericValue> getRelevantSecurityLevels(final ApplicationUser searcher) {
                return levels;
            }

            @Override
            Locale getLocale(final ApplicationUser searcher) {
                return Locale.ENGLISH;
            }
        };

        final ClauseValuesGenerator.Results result = valuesGenerator.getPossibleValues(null, "level", "", 3);


        assertThat(result.getResults(), contains(
                new ClauseValuesGenerator.Result(level1.getString("name")),
                new ClauseValuesGenerator.Result(level2.getString("name")),
                new ClauseValuesGenerator.Result(level3.getString("name"))
        ));
    }
}
