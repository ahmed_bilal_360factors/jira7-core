package com.atlassian.jira.issue.fields.csv;

import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.bc.issue.worklog.WorklogService;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.export.FieldExportParts;
import com.atlassian.jira.issue.fields.WorklogSystemField;
import com.atlassian.jira.issue.fields.csv.util.StubCsvDateFormatter;
import com.atlassian.jira.issue.views.util.csv.CsvDateFormatter;
import com.atlassian.jira.issue.worklog.Worklog;
import com.atlassian.jira.junit.rules.InitMockitoMocks;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.MockComponentLocator;
import com.google.common.collect.ImmutableList;
import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;

import java.util.Date;
import java.util.stream.Stream;

import static java.util.Optional.of;
import static java.util.Optional.ofNullable;
import static java.util.stream.Collectors.joining;
import static org.hamcrest.Matchers.contains;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class WorklogSystemFieldCsvExportTest {
    @Rule
    public TestRule initMockito = new InitMockitoMocks(this);

    @Mock
    private WorklogService worklogService;

    @Spy
    private CsvDateFormatter csvDateFormatter = new StubCsvDateFormatter();

    @Mock
    private Issue issue;

    @Spy
    private MockComponentLocator locator = new MockComponentLocator();

    @InjectMocks
    private WorklogSystemField field;

    @Mock
    private JiraAuthenticationContext authenticationContext;

    @Mock
    private I18nHelper i18nHelper;

    @Mock
    private ApplicationUser applicationUser;

    @Before
    public void setUp() {
        locator.register(WorklogService.class, worklogService);
        when(authenticationContext.getUser()).thenReturn(applicationUser);
        when(authenticationContext.getI18nHelper()).thenReturn(i18nHelper);
    }

    @Test
    public void allWorklogsAreExportedAsSeparateValues() {
        final Worklog c1 = worklog(null, 5, null, null);
        final Worklog c2 = worklog("b", 3, null, null);

        when(worklogService.getByIssueVisibleToUser(any(JiraServiceContext.class), any(Issue.class))).thenReturn(ImmutableList.of(
                c1, c2
        ));

        assertExported(exported(c1), exported(c2));
    }

    @Test
    public void dateAndAuthorAndCommentAreExportedCorrectly() {
        final Worklog c1 = worklog("a", 6, new Date(), "admin");
        final Worklog c2 = worklog(null, 10, new Date(), "admin");
        final Worklog c3 = worklog("c", 14, null, "admin");
        final Worklog c4 = worklog("d", 18, new Date(), null);

        when(worklogService.getByIssueVisibleToUser(any(JiraServiceContext.class), any(Issue.class))).thenReturn(ImmutableList.of(
                c1, c2, c3, c4
        ));

        assertExported(exported(c1), exported(c2), exported(c3), exported(c4));
    }

    private void assertExported(String... values) {
        final FieldExportParts representation = field.getRepresentationFromIssue(issue);

        assertThat(representation.getParts().get(0).getValues()::iterator, contains(values));
    }

    private Worklog worklog(final String comment, final long timeSpent, final Date created, final String author) {
        final Worklog worklog = mock(Worklog.class);

        when(worklog.getComment()).thenReturn(comment);
        when(worklog.getCreated()).thenReturn(created);
        when(worklog.getTimeSpent()).thenReturn(timeSpent);

        if (author != null) {
            when(worklog.getAuthorObject()).thenReturn(new MockApplicationUser(author));
        }

        return worklog;
    }

    private String exported(final Worklog worklog) {
        return Stream.of(
                ofNullable(worklog.getComment()).filter(StringUtils::isNotBlank),
                ofNullable(worklog.getCreated()).map(csvDateFormatter::formatDateTime),
                ofNullable(worklog.getAuthorObject()).map(ApplicationUser::getUsername),
                of(worklog.getTimeSpent()).map(String::valueOf)
        )
                .map(opt -> opt.orElse(""))
                .collect(joining(";"));
    }
}
