package com.atlassian.jira.jql.query;

import com.atlassian.jira.jql.validator.MockJqlOperandResolver;

/**
 * @since v4.0
 */
public class TestAssigneeClauseQueryFactory extends TestUserClauseQueryFactory {

    public TestAssigneeClauseQueryFactory() {
        this.fieldNameUnderTest = "assignee";
        this.clauseQueryFactory = new AssigneeClauseQueryFactory(userResolver, MockJqlOperandResolver.createSimpleSupport());
    }
}
