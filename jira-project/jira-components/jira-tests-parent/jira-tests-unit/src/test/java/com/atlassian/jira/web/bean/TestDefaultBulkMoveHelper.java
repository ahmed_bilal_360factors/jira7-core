package com.atlassian.jira.web.bean;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.fields.OrderableField;
import com.atlassian.jira.issue.fields.layout.field.FieldLayout;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem;
import com.atlassian.jira.junit.rules.InitMockitoMocks;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.util.Function;
import com.google.common.collect.Lists;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.mockito.Mock;

import java.util.ArrayList;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @since v4.1
 */
public class TestDefaultBulkMoveHelper {
    @Rule
    public TestRule rule = new InitMockitoMocks(this);
    BulkMoveHelper helper = new DefaultBulkMoveHelper() {
        protected BulkEditBean getBulkEditBeanFromSession() {
            return new BulkEditBeanImpl(null);
        }
    };
    @Mock
    private BulkEditBean bulkEditBean;
    @Mock
    private OrderableField orderableField;
    @Mock
    private Function issueValueResolution;
    @Mock
    private Function nameResolution;
    @Mock
    private FieldLayout fieldLayout;
    @Mock
    private FieldLayoutItem targetFieldLayoutItem;
    @Mock
    private Map targetIssueObjects;
    @Mock
    private Project project;

    private void expectSelectedIssues(Issue... selectedIssues) {
        when(orderableField.getId()).thenReturn("1");
        when(bulkEditBean.getTargetFieldLayout()).thenReturn(fieldLayout);
        when(fieldLayout.getFieldLayoutItem(orderableField)).thenReturn(targetFieldLayoutItem);
        when(bulkEditBean.getSelectedIssues()).thenReturn(Lists.newArrayList(selectedIssues));
    }

    @Test
    public void testGetDistinctValuesForMoveEmptyList() {
        expectSelectedIssues();

        Map<Long, BulkMoveHelper.DistinctValueResult> result = helper.getDistinctValuesForMove(bulkEditBean, orderableField, issueValueResolution, nameResolution);
        assertEquals(0, result.size());
    }

    private void expectProject(Issue issue, Long id) {
        when(issue.getProjectId()).thenReturn(id);
    }

    private void expectTargetIssue(Issue selectedIssue, Issue targetIssue, Long selectedIssueId, Long targetIssueId) {
        when(bulkEditBean.getTargetIssueObjects()).thenReturn(targetIssueObjects);
        when(targetIssueObjects.get(selectedIssue)).thenReturn(targetIssue);
        expectProject(targetIssue, targetIssueId);
        expectProject(selectedIssue, selectedIssueId);
    }


    @Test
    public void testGetDistinctValuesForMoveSameProject() {
        Issue selectedIssue = mock(Issue.class);
        Issue targetIssue = mock(Issue.class);
        expectSelectedIssues(selectedIssue);
        expectTargetIssue(selectedIssue, targetIssue, 1L, 1L);

        Map<Long, BulkMoveHelper.DistinctValueResult> result = helper.getDistinctValuesForMove(bulkEditBean, orderableField, issueValueResolution, nameResolution);
        assertEquals(0, result.size());
    }

    @Test
    public void testGetDistinctValuesEmptyValuesForIssueNotRequired() {
        Issue selectedIssue = mock(Issue.class);
        Issue targetIssue = mock(Issue.class);
        expectSelectedIssues(selectedIssue);
        expectTargetIssue(selectedIssue, targetIssue, 1L, 2L);

        when(issueValueResolution.get(selectedIssue)).thenReturn(new ArrayList());
        when(targetFieldLayoutItem.isRequired()).thenReturn(false);

        Map<Long, BulkMoveHelper.DistinctValueResult> result = helper.getDistinctValuesForMove(bulkEditBean, orderableField, issueValueResolution, nameResolution);
        assertEquals(0, result.size());
    }

    @Test
    public void testGetDistinctValuesEmptyValuesForIssueRequired() {
        Issue selectedIssue = mock(Issue.class);
        Issue targetIssue = mock(Issue.class);
        expectSelectedIssues(selectedIssue);
        expectTargetIssue(selectedIssue, targetIssue, 1L, 2L);

        when(issueValueResolution.get(selectedIssue)).thenReturn(new ArrayList());
        when(targetFieldLayoutItem.isRequired()).thenReturn(true);

        Map<Long, BulkMoveHelper.DistinctValueResult> result = helper.getDistinctValuesForMove(bulkEditBean, orderableField, issueValueResolution, nameResolution);
        assertEquals(1, result.size());
        assertEquals(result.get(-1L), new BulkMoveHelper.DistinctValueResult());
    }

    @Test
    public void testGetDistinctValuesWithValueForIssueResolvedToNull() {
        Issue selectedIssue = mock(Issue.class);
        Issue targetIssue = mock(Issue.class);
        expectSelectedIssues(selectedIssue);
        expectTargetIssue(selectedIssue, targetIssue, 1L, 2L);

        when(issueValueResolution.get(selectedIssue)).thenReturn(Lists.newArrayList("55"));
        when(nameResolution.get("55")).thenReturn(null);

        Map<Long, BulkMoveHelper.DistinctValueResult> result = helper.getDistinctValuesForMove(bulkEditBean, orderableField, issueValueResolution, nameResolution);
        assertEquals(0, result.size());
    }

    private void expectProjectName(Issue issue, String projectName) {
        when(issue.getProjectObject()).thenReturn(project);
        when(project.getName()).thenReturn(projectName);
    }

    @Test
    public void testGetDistinctValuesWithValueForIssueResolved() {
        Issue selectedIssue = mock(Issue.class);
        Issue targetIssue = mock(Issue.class);
        expectSelectedIssues(selectedIssue);
        expectTargetIssue(selectedIssue, targetIssue, 1L, 2L);

        when(issueValueResolution.get(selectedIssue)).thenReturn(Lists.newArrayList("-1"));
        when(nameResolution.get("-1")).thenReturn(" resolvedName ");
        expectProjectName(selectedIssue, " myProjectName ");

        Map<Long, BulkMoveHelper.DistinctValueResult> result = helper.getDistinctValuesForMove(bulkEditBean, orderableField, issueValueResolution, nameResolution);
        assertEquals(1, result.size());
        assertEquals(result.get(-1L), new BulkMoveHelper.DistinctValueResult("resolvedName", " myProjectName ", null, false));
    }

    @Test
    public void testGetDistinctValuesWithValueForIssueResolvedWithDuplicates() {
        Issue selectedIssue = mock(Issue.class);
        Issue targetIssue = mock(Issue.class);
        expectSelectedIssues(selectedIssue);
        expectTargetIssue(selectedIssue, targetIssue, 1L, 2L);

        when(issueValueResolution.get(selectedIssue)).thenReturn(Lists.newArrayList("10", "10"));
        when(nameResolution.get("10")).thenReturn(" resolvedName ");
        expectProjectName(selectedIssue, " myProjectName ");

        Map<Long, BulkMoveHelper.DistinctValueResult> result = helper.getDistinctValuesForMove(bulkEditBean, orderableField, issueValueResolution, nameResolution);
        assertEquals(1, result.size());
        assertEquals(result.get(10L), new BulkMoveHelper.DistinctValueResult("resolvedName", " myProjectName ", null, false));
    }

    @Test
    public void testNeedsSelection() {
        assertFalse(helper.needsSelection(new BulkMoveHelper.DistinctValueResult(), 1L, "value"));
        assertTrue(helper.needsSelection(new BulkMoveHelper.DistinctValueResult("valueName", "projectName", 1L, true), 1L, ""));
        assertFalse(helper.needsSelection(new BulkMoveHelper.DistinctValueResult("valueName", "projectName", 1L, true), 2L, ""));
        assertTrue(helper.needsSelection(new BulkMoveHelper.DistinctValueResult("valueName", "projectName", null, false), 2L, "valueName"));
        assertFalse(helper.needsSelection(new BulkMoveHelper.DistinctValueResult("valueName", "projectName", null, true), 2L, "valueName"));
        assertFalse(helper.needsSelection(new BulkMoveHelper.DistinctValueResult("valueName", "projectName", null, false), 2L, "VALUENAME"));
        assertFalse(helper.needsSelection(new BulkMoveHelper.DistinctValueResult("valueName", "projectName", null, false), 2L, "ANOTHER NAME"));
    }
}
