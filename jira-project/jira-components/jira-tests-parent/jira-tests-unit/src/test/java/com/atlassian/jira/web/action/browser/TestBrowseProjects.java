package com.atlassian.jira.web.action.browser;

import com.atlassian.fugue.Option;
import com.atlassian.jira.avatar.Avatar;
import com.atlassian.jira.avatar.AvatarManager;
import com.atlassian.jira.mock.component.MockComponentWorker;
import com.atlassian.jira.plugin.userformat.UserFormats;
import com.atlassian.jira.plugin.webfragment.SimpleLinkManager;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.project.type.BrowseProjectTypeManager;
import com.atlassian.jira.project.type.ProjectType;
import com.atlassian.jira.project.type.ProjectTypeKey;
import com.atlassian.jira.project.type.ProjectTypeManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.plugin.ProjectPermissionKey;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.UserProjectHistoryManager;
import com.atlassian.plugin.web.WebInterfaceManager;
import com.atlassian.webresource.api.assembler.PageBuilderService;
import com.atlassian.webresource.api.assembler.RequiredData;
import com.atlassian.webresource.api.assembler.RequiredResources;
import com.atlassian.webresource.api.assembler.WebResourceAssembler;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class TestBrowseProjects {
    private BrowseProjects browseProjects;
    @Mock
    private UserProjectHistoryManager projectHistoryManager;
    @Mock
    private ProjectManager projectManager;
    @Mock
    private PermissionManager permissionManager;
    @Mock
    private SimpleLinkManager simpleLinkManager;
    @Mock
    private WebInterfaceManager webInterfaceManager;
    @Mock
    private AvatarManager avatarManager;
    @Mock
    private PageBuilderService pageBuilderService;
    @Mock
    private UserFormats userFOrmats;
    @Mock
    private ProjectTypeManager projectTypeManager;
    @Mock
    private JiraAuthenticationContext jiraAuthenticationContextMock;
    @Mock
    private com.atlassian.jira.util.I18nHelper i18nHelper;
    @Mock
    private WebResourceAssembler webResourceAssembler;
    @Mock
    private RequiredResources requiredResources;
    @Mock
    private RequiredData requiredData;

    @Mock
    private BrowseProjectTypeManager browseProjectTypeManager;
    private Collection<Project> projectList = projectList();
    public static final String BUSINESS = "business";
    private static final ProjectTypeKey BUSINESS_KEY = new ProjectTypeKey(BUSINESS);

    public static final String SOFTWARE = "software";
    private static final ProjectTypeKey SOFTWARE_KEY = new ProjectTypeKey(SOFTWARE);

    public static final String SERVICE_DESK = "servicedesk";
    private static final ProjectTypeKey SERVICE_DESK_KEY = new ProjectTypeKey(SERVICE_DESK);

    private static final ProjectType BUSINESS_TYPE = new ProjectType(BUSINESS_KEY, "business_key", "business_icon", "blue", 10);
    private static final ProjectType SOFTWARE_TYPE = new ProjectType(SOFTWARE_KEY, "software_key", "software_icon", "orange", 20);
    private static final ProjectType SERVICE_DESK_TYPE = new ProjectType(SERVICE_DESK_KEY, "service_desk_key", "service_desk_icon", "red", 30);

    public static final MockApplicationUser ADMIN = new MockApplicationUser("admin");
    private static final String INACCESSIBLE_ICON = "inaccessible_icon";
    public static final ProjectType INACCESSIBLE_PROJECT_TYPE = new ProjectType(new ProjectTypeKey("inaccessible"), "d", INACCESSIBLE_ICON, "fd", 0);
    private static final String ALL = "all";

    @Before
    public void setUp() throws Exception {
        setUpMockEnvironment();
        setUpAllProjectTypesAvailable();
        setUpPageBuilderService();
        browseProjects = new BrowseProjects(
                projectHistoryManager,
                projectManager,
                permissionManager,
                simpleLinkManager,
                webInterfaceManager,
                avatarManager,
                pageBuilderService,
                userFOrmats,
                projectTypeManager,
                browseProjectTypeManager
        );
        when(permissionManager.getProjects(any(ProjectPermissionKey.class), any(ApplicationUser.class))).thenReturn(projectList);
    }

    @Test
    public void shouldAddAllProjectTypeToTheStartOfList() throws Exception {
        when(browseProjectTypeManager.getAllProjectTypes(ADMIN)).thenReturn(Arrays.asList(BUSINESS_TYPE, SOFTWARE_TYPE, SERVICE_DESK_TYPE));
        final List<ProjectTypeBean> projectTypes = browseProjects.getAvailableProjectTypes().get();
        assertProjectTypes(projectTypes, ALL, BUSINESS, SOFTWARE, SERVICE_DESK);
    }

    @Test
    public void getSelectedProjectTypeShouldReturnAsItIsIfItIsNotNull() throws Exception {
        browseProjects.setSelectedProjectType(BUSINESS);
        assertThat(browseProjects.getSelectedProjectType(), is(BUSINESS));
    }

    @Test
    public void getSelectedCategoryShouldReturnAsItIsIfItIsNotNull() throws Exception {
        browseProjects.setSelectedCategory(SERVICE_DESK);
        assertThat(browseProjects.getSelectedCategory(), is(SERVICE_DESK));
    }

    @Test
    public void getSelectedProjectTypeShouldBeDefaultToAllIfNotSet() throws Exception {
        browseProjects.setSelectedProjectType(null);
        assertThat(browseProjects.getSelectedProjectType(), is(ALL));
    }

    @Test
    public void getSelectedCategoryShouldBeDefaultToAllIfNotSet() throws Exception {
        browseProjects.setSelectedCategory(null);
        assertThat(browseProjects.getSelectedCategory(), is(ALL));
    }

    private void assertProjectTypes(List<ProjectTypeBean> projectTypes, String... projectTypeKeys) {
        for (int i = 0; i < projectTypeKeys.length; i++) {
            assertThat(projectTypes.get(i).getKey(), is(projectTypeKeys[i]));
        }
    }

    private void setUpPageBuilderService() {
        when(requiredResources.requireWebResource(anyString())).thenReturn(requiredResources);
        when(webResourceAssembler.resources()).thenReturn(requiredResources);
        when(webResourceAssembler.data()).thenReturn(requiredData);
        when(pageBuilderService.assembler()).thenReturn(webResourceAssembler);
    }

    private void setUpAllProjectTypesAvailable() {
        when(projectTypeManager.getAccessibleProjectType(eq(ADMIN), eq(BUSINESS_KEY))).thenReturn(Option.some(BUSINESS_TYPE));
        when(projectTypeManager.getAccessibleProjectType(eq(ADMIN), eq(SOFTWARE_KEY))).thenReturn(Option.some(SOFTWARE_TYPE));
        when(projectTypeManager.getAccessibleProjectType(eq(ADMIN), eq(SERVICE_DESK_KEY))).thenReturn(Option.some(SERVICE_DESK_TYPE));
        when(projectTypeManager.getInaccessibleProjectType()).thenReturn(INACCESSIBLE_PROJECT_TYPE);
    }

    private Collection<Project> projectList() {
        List<Project> projectList = new ArrayList<>();
        projectList.add(createProject(1, "software"));
        projectList.add(createProject(2, "servicedesk"));
        projectList.add(createProject(3, "business"));
        projectList.add(createProject(4, "software"));
        projectList.add(createProject(5, "business"));

        return projectList;
    }

    private Project createProject(int key, String projectTypeKey) {
        MockProject project = new MockProject(key, "DRAG", new ProjectTypeKey(projectTypeKey));
        project.setAvatar(mock(Avatar.class));
        return project;
    }

    private void setUpMockEnvironment() {
        when(i18nHelper.getText(anyString())).thenReturn("text");
        when(jiraAuthenticationContextMock.getUser()).thenReturn(ADMIN);
        when(jiraAuthenticationContextMock.getI18nHelper()).thenReturn(i18nHelper);
        MockComponentWorker componentWorker = new MockComponentWorker();
        componentWorker.registerMock(JiraAuthenticationContext.class, jiraAuthenticationContextMock);
        componentWorker.init();
    }
}