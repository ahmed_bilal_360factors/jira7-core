package com.atlassian.jira.web.landingpage;

import com.atlassian.jira.user.ApplicationUser;

import java.util.Optional;
import java.util.function.Predicate;

/**
 * Implementation of {@link PageRedirect} that always redirect to same URL;
 *
 * @since 7.0
 */
public final class StaticUrlRedirect implements PageRedirect {
    private final String url;
    private final Predicate<ApplicationUser> activePredicate;

    public StaticUrlRedirect(final String url, final Predicate<ApplicationUser> activePredicate) {
        this.url = url;
        this.activePredicate = activePredicate;
    }

    @Override
    public Optional<String> url(ApplicationUser user) {
        return activePredicate.test(user) ? Optional.of(url) : Optional.empty();
    }
}
