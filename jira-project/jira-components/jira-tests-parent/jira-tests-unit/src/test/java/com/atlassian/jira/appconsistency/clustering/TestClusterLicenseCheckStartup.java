package com.atlassian.jira.appconsistency.clustering;

import com.atlassian.jira.application.ApplicationKeys;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.license.Jira6xServiceDeskPluginEncodedLicenseSupplier;
import com.atlassian.jira.license.JiraLicenseManager;
import com.atlassian.jira.license.LicenseDetails;
import com.atlassian.jira.util.I18nHelper;
import com.google.common.collect.ImmutableList;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Answers;
import org.mockito.Mock;

import static com.atlassian.fugue.Option.option;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Matchers.anyCollectionOf;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Tests {@link ClusterLicenseStartupCheck}.
 *
 * @see com.atlassian.jira.license.TestClusterLicenseCheckImpl
 * @see com.atlassian.jira.license.ClusterLicenseCheck
 * @since 7.0
 */
public class TestClusterLicenseCheckStartup {
    @Mock
    @AvailableInContainer
    JiraLicenseManager licenseManager;
    @Mock
    com.atlassian.jira.license.ClusterLicenseCheck licenseCheck;
    @Mock
    com.atlassian.jira.license.LicenseCheck.Result licenseCheckResult;
    @Mock
    Jira6xServiceDeskPluginEncodedLicenseSupplier serviceDeskLicenseSupplier;
    @Mock
    I18nHelper i18n;
    @Mock(answer = Answers.RETURNS_MOCKS)
    LicenseDetails lic1, lic2;
    @Rule
    public final MockitoContainer initMocks = MockitoMocksInContainer.rule(this);
    ClusterLicenseStartupCheck clusterLicenseStartupCheck;

    @Before
    public void setup() {
        clusterLicenseStartupCheck = new ClusterLicenseStartupCheck(licenseCheck, i18n, licenseManager, serviceDeskLicenseSupplier);
        when(licenseCheck.evaluate()).thenReturn(licenseCheckResult);
    }

    @Test
    public void checkPassesWhenAllLicensesAreDataCenter() {
        when(licenseCheckResult.isPass()).thenReturn(true);
        assertThat(clusterLicenseStartupCheck.isOk(), equalTo(true));
    }

    @Test
    public void checkRemovesNonDataCenterLicensesOnFailure() {
        when(licenseCheckResult.isPass()).thenReturn(false);
        when(licenseCheckResult.getFailedLicenses()).thenReturn(ImmutableList.of(lic2));

        assertThat(clusterLicenseStartupCheck.isOk(), equalTo(true));
        verify(licenseManager).removeLicenses(ImmutableList.of(lic2));
    }

    @Test
    public void checkAlsoSpeciallyRemovesServiceDeskPluginLicenseIfPresentOnFailure() {
        when(licenseCheckResult.isPass()).thenReturn(false);
        when(licenseCheckResult.getFailedLicenses()).thenReturn(ImmutableList.of(lic1, lic2));

        // make lic2 a ServiceDesk license
        when(lic2.hasApplication(ApplicationKeys.SERVICE_DESK)).thenReturn(true);
        when(serviceDeskLicenseSupplier.get()).thenReturn(option("service desk plugin license text"));

        assertThat(clusterLicenseStartupCheck.isOk(), equalTo(true));
        verify(licenseManager).removeLicenses(ImmutableList.of(lic1, lic2));
        verify(serviceDeskLicenseSupplier).moveToUpgradeStore();
    }

    @Test
    public void checkLicenseRemovalToleratesExceptions() {
        when(licenseCheckResult.isPass()).thenReturn(false);
        when(licenseCheckResult.getFailedLicenses()).thenReturn(ImmutableList.of(lic2));
        doThrow(IllegalStateException.class).when(licenseManager).removeLicenses(anyCollectionOf(LicenseDetails.class));

        assertThat(clusterLicenseStartupCheck.isOk(), equalTo(false));
        verify(licenseManager).removeLicenses(ImmutableList.of(lic2));
    }
}
