package com.atlassian.jira.mock.jql.validator;

import com.atlassian.jira.issue.fields.layout.field.FieldLayout;
import com.atlassian.jira.jql.permission.ClausePermissionHandler;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.query.clause.Clause;
import com.atlassian.query.clause.TerminalClause;

import java.util.Set;

/**
 * @since v4.0
 */
public class MockClausePermissionHandler implements ClausePermissionHandler {
    private final boolean hasPerm;

    public MockClausePermissionHandler(final boolean hasPerm) {
        this.hasPerm = hasPerm;
    }

    public MockClausePermissionHandler() {
        this(true);
    }

    public Clause sanitise(final ApplicationUser user, final TerminalClause clause) {
        return clause;
    }

    public boolean hasPermissionToUseClause(final ApplicationUser user) {
        return hasPerm;
    }

    @Override
    public boolean hasPermissionToUseClause(ApplicationUser searcher, Set<FieldLayout> fieldLayouts) {
        return hasPerm;
    }
}
