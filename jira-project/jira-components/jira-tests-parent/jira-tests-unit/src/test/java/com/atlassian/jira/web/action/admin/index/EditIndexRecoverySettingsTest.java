package com.atlassian.jira.web.action.admin.index;

import com.atlassian.core.ofbiz.test.UtilsForTests;
import com.atlassian.jira.index.ha.IndexRecoveryService;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.JiraWebActionSupportDependencies;
import com.atlassian.jira.junit.rules.MockitoContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.util.collect.MapBuilder;
import com.atlassian.jira.web.component.cron.CronEditorBean;
import com.atlassian.scheduler.SchedulerService;
import com.atlassian.scheduler.config.Schedule;
import com.atlassian.scheduler.cron.CronSyntaxException;
import com.atlassian.scheduler.cron.ErrorCode;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.mockito.Mock;
import webwork.action.ActionContext;

import java.util.Date;

import static com.atlassian.jira.mock.security.MockSimpleAuthenticationContext.createNoopContext;
import static com.atlassian.jira.util.ErrorCollectionAssert.assert1FieldError;
import static com.atlassian.jira.util.ErrorCollectionAssert.assertNoErrors;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class EditIndexRecoverySettingsTest {
    private static final String GOOD_CRON_EXPRESSION = "0 0 0 3,15,22 * ?";
    private static final String MALFORMED_CRON_EXPRESSION = "???? MALFORMED";
    private static final String PARAM_CRON_MODE = "service.schedule.dailyWeeklyMonthly";
    private static final String PARAM_CRON_STRING = "service.schedule.cronString";
    private static final String PARAM_CRON_INTERVAL = "service.schedule.interval";
    private static final String PARAM_CRON_RUN_ONCE_MERIDIAN = "service.schedule.runOnceMeridian";
    private static final String PARAM_CRON_RUN_ONCE_HOURS = "service.schedule.runOnceHours";
    private static final String PARAM_CRON_RUN_ONCE_MINS = "service.schedule.runOnceMins";
    private static final String CRON_RUN_ONCE_DAILY_EXPRESSION = "0 0 12 ? * *";
    private static final java.util.Map<String, String[]> CRON_RUN_ONCE_DAILY_PARAMS = MapBuilder.<String, String[]>newBuilder()
            .add(PARAM_CRON_MODE, new String[]{CronEditorBean.DAILY_SPEC_MODE})
            .add(PARAM_CRON_RUN_ONCE_MERIDIAN, new String[]{"PM"})
            .add(PARAM_CRON_RUN_ONCE_HOURS, new String[]{"12"})
            .add(PARAM_CRON_RUN_ONCE_MINS, new String[]{"00"})
            .add(PARAM_CRON_INTERVAL, new String[]{"0"})
            .toMap();

    @Rule
    public final MockitoContainer mockContainer = MockitoMocksInContainer.rule(this);
    @Rule
    public TestRule webActionSupport = JiraWebActionSupportDependencies.build(mockContainer);

    private MockApplicationUser user = new MockApplicationUser("User");

    @AvailableInContainer
    private JiraAuthenticationContext authenticationContext = createNoopContext(user);

    @Mock
    private ProjectManager projectManager;
    @Mock
    private PermissionManager permissionManager;
    @Mock
    private IndexRecoveryService recoveryService;
    @Mock
    private SchedulerService schedulerService;

    private EditIndexRecoverySettings indexRecoverySettings;


    @Before
    public void setup() {
        UtilsForTests.cleanWebWork();
        indexRecoverySettings = new EditIndexRecoverySettings(projectManager, permissionManager, recoveryService, schedulerService);
    }

    @AfterClass
    public static void tearDown() {
        UtilsForTests.cleanWebWork();
    }

    @Test
    public void doDefaultRetrievesIndexBackupSettingsFromRecoveryServiceIfItIsEnabled() throws Exception {
        when(recoveryService.isRecoveryEnabled(any(ApplicationUser.class))).thenReturn(true);
        when(recoveryService.getSnapshotCronExpression(user)).thenReturn(GOOD_CRON_EXPRESSION);
        when(schedulerService.calculateNextRunTime(any(Schedule.class))).thenReturn(new Date());

        indexRecoverySettings.doDefault();
        assertThat("isRecoveryEnabled", indexRecoverySettings.isRecoveryEnabled(), is(true));
        assertThat("isAdvancedMode", indexRecoverySettings.getCronEditorBean().isAdvancedMode(), is(true));
        assertThat("cronExpression", indexRecoverySettings.getCronEditorBean().getCronString(), is(GOOD_CRON_EXPRESSION));
    }

    @Test
    public void doDefaultSetsRecoveryFlagToDisabledIfIndexRecoveryIsDisabled() throws Exception {
        when(recoveryService.isRecoveryEnabled(any(ApplicationUser.class))).thenReturn(false);

        indexRecoverySettings.doDefault();
        assertThat("isRecoveryEnabled", indexRecoverySettings.isRecoveryEnabled(), is(false));
    }

    @Test
    public void doDefaultSetsRecoveryFlagToDisabledIfCronExpressionInRecoveryServiceIsInvalid() throws Exception {
        when(recoveryService.isRecoveryEnabled(any(ApplicationUser.class))).thenReturn(true);
        when(recoveryService.getSnapshotCronExpression(user)).thenReturn(MALFORMED_CRON_EXPRESSION);

        indexRecoverySettings.doDefault();

        assertThat("isRecoveryEnabled", indexRecoverySettings.isRecoveryEnabled(), is(false));
    }

    @Test
    public void validatesIfCronExpressionIsValid() throws Exception {
        ActionContext.setParameters(MapBuilder.build(
                PARAM_CRON_MODE, new String[]{CronEditorBean.ADVANCED_MODE},
                PARAM_CRON_STRING, new String[]{MALFORMED_CRON_EXPRESSION},
                PARAM_CRON_INTERVAL, new String[]{"0"}));

        final CronSyntaxException expected = CronSyntaxException.builder()
                .errorCode(ErrorCode.ILLEGAL_CHARACTER_AFTER_QM)
                .cronExpression(MALFORMED_CRON_EXPRESSION)
                .value('?')
                .build();

        when(schedulerService.calculateNextRunTime(any(Schedule.class))).thenThrow(expected);

        indexRecoverySettings.doValidation();

        assert1FieldError(indexRecoverySettings, "cron.editor.name", "cron.expression.invalid.ILLEGAL_CHARACTER_AFTER_QM{[?]}");
    }

    @Test
    public void validateReturnsNoErrorForCorrectCronSettings() throws Exception {
        ActionContext.setParameters(CRON_RUN_ONCE_DAILY_PARAMS);
        when(schedulerService.calculateNextRunTime(any(Schedule.class))).thenReturn(new Date());

        indexRecoverySettings.doValidation();

        assertNoErrors(indexRecoverySettings);
    }

    @Test
    public void doExecuteUpdatesRecoverySettings() throws Exception {
        ActionContext.setParameters(CRON_RUN_ONCE_DAILY_PARAMS);

        indexRecoverySettings.setRecoveryEnabled(true);
        indexRecoverySettings.doValidation();
        indexRecoverySettings.doExecute();

        verify(recoveryService).updateRecoverySettings(user, true, CRON_RUN_ONCE_DAILY_EXPRESSION);
    }

}