package com.atlassian.jira.web.servlet;

import com.atlassian.fugue.Unit;
import com.atlassian.jira.exception.DataAccessException;
import com.atlassian.jira.exception.PermissionException;
import com.atlassian.jira.issue.attachment.Attachment;
import com.atlassian.jira.issue.attachment.AttachmentConstants;
import com.atlassian.jira.mock.component.MockComponentWorker;
import com.atlassian.jira.mock.ofbiz.MockGenericValue;
import com.atlassian.jira.mock.ofbiz.MockOfBizDelegator;
import com.atlassian.jira.ofbiz.FieldMap;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.io.InputStreamConsumer;
import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @since v7.2
 */
public class TestAbstractViewFileServlet {
    @Mock
    private HttpServletRequest mockRequest;
    @Mock
    private HttpServletResponse mockResponse;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testDoGet_RangeSupported() throws Exception {
        // Set up
        final AbstractViewFileServlet viewFileServlet = makeAbstractViewFileServlet(true);

        when(mockRequest.getPathInfo()).thenReturn("/10100/alphabet.txt");
        when(mockRequest.getHeader("Range")).thenReturn("bytes=2-5");
        final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        when(mockResponse.getOutputStream()).thenReturn(newServletOutputStream(outputStream));

        // Invoke
        viewFileServlet.doGet(mockRequest, mockResponse);

        // Check
        verify(mockResponse).setStatus(HttpServletResponse.SC_PARTIAL_CONTENT);
        verify(mockResponse).setContentLength(4);
        verify(mockResponse).setHeader("Accept-Ranges", "bytes");
        verify(mockResponse).setHeader("Content-Range", "bytes 2-5/10");
        assertEquals("cdef", outputStream.toString());

        verify(mockResponse).getOutputStream();
        Mockito.verifyNoMoreInteractions(mockResponse);
    }

    @Test
    public void testDoGet_RangeNotSupported() throws Exception {
        // Set up
        final AbstractViewFileServlet viewFileServlet = makeAbstractViewFileServlet(false);

        when(mockRequest.getPathInfo()).thenReturn("/10100/alphabet.txt");
        when(mockRequest.getHeader("Range")).thenReturn("bytes=2-5");
        final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        when(mockResponse.getOutputStream()).thenReturn(newServletOutputStream(outputStream));

        // Invoke
        viewFileServlet.doGet(mockRequest, mockResponse);

        // Check
        verify(mockResponse).setContentLength(10);
        assertEquals("abcdefghij", outputStream.toString());

        verify(mockResponse).getOutputStream();
        Mockito.verifyNoMoreInteractions(mockResponse);
    }

    @Test
    public void testDoGet_RangeNotSatisfiable() throws Exception {
        // Set up
        final AbstractViewFileServlet viewFileServlet = makeAbstractViewFileServlet(true);

        when(mockRequest.getPathInfo()).thenReturn("/10100/alphabet.txt");
        when(mockRequest.getHeader("Range")).thenReturn("bytes=1024-");

        // Invoke
        viewFileServlet.doGet(mockRequest, mockResponse);

        // Check
        verify(mockResponse).sendError(416, "Cannot return request range 'bytes=1024-'. Attachment has only 10 bytes");
        verify(mockResponse).setHeader("Accept-Ranges", "bytes");
        verify(mockResponse).setHeader("Content-Range", "bytes */10");
    }

    private AbstractViewFileServlet makeAbstractViewFileServlet(final boolean supportsRangeRequests) {
        return new AbstractViewFileServlet() {
            final String content = "abcdefghij";

            @Override
            protected int getContentLength(String attachmentPath) {
                return content.length();
            }

            @Override
            protected boolean supportsRangeRequests() {
                return supportsRangeRequests;
            }

            @Override
            protected void getInputStream(String attachmentPath, InputStreamConsumer<Unit> consumer) throws IOException, PermissionException {
                consumer.withInputStream(new ByteArrayInputStream(content.getBytes()));
            }

            @Override
            protected void setResponseHeaders(HttpServletRequest request, Optional<RangeResponse> rangeResponse, HttpServletResponse response) throws InvalidAttachmentPathException, DataAccessException, IOException {
                if (rangeResponse.isPresent()) {
                    if (!supportsRangeRequests) {
                        Assert.fail("I don't support Range Requests :P");
                    }
                    // Status Code: 206 Partial Content
                    response.setStatus(HttpServletResponse.SC_PARTIAL_CONTENT);
                    // the Content Length for the partial response
                    response.setContentLength(rangeResponse.get().calculateContentLength());
                    // The range we are returning eg "Content-Range: bytes 21010-47021/47022"
                    response.setHeader("Content-Range", rangeResponse.get().calculateContentRange());
                } else {
                    // We will return the whole file
                    response.setContentLength(content.length());
                }
                if (supportsRangeRequests) {
                    response.setHeader("Accept-Ranges", "bytes");
                }
            }
        };
    }

    private ServletOutputStream newServletOutputStream(ByteArrayOutputStream outputStream) {
        return new ServletOutputStream() {
            @Override
            public void write(int b) throws IOException {
                outputStream.write(b);
            }
        };
    }
}
