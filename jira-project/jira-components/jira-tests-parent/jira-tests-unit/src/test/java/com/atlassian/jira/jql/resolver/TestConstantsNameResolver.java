package com.atlassian.jira.jql.resolver;

import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.issue.IssueConstant;
import com.atlassian.jira.issue.status.StatusImpl;
import com.atlassian.jira.mock.ofbiz.MockGenericValue;
import com.atlassian.jira.ofbiz.FieldMap;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import java.util.Collection;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestConstantsNameResolver {
    @Rule
    public final MethodRule initMockito = MockitoJUnit.rule();

    @Mock
    private ConstantsManager mockConstantsManager;
    @Mock
    private IssueConstant mockIssueConstant;

    @Test
    public void testGetIdsFromNameNoConstant() throws Exception {
        when(mockConstantsManager.getConstantByNameIgnoreCase("TestConstant", "blah")).thenReturn(null);

        ConstantsNameResolver<Object> constantsNameResolver = new ConstantsNameResolver<Object>(mockConstantsManager, "TestConstant") {
            public Collection<Object> getAll() {
                return null;
            }
        };

        assertTrue(constantsNameResolver.getIdsFromName("blah").isEmpty());
    }

    @Test
    public void testGetIdsFromNameHappyPath() throws Exception {
        final MockGenericValue genericValue = new MockGenericValue("TestConst", FieldMap.build("id", "10000"));

        when(mockConstantsManager.getConstantByNameIgnoreCase("TestConstant", "blah"))
                .thenReturn(new StatusImpl(genericValue, null, null, null, null));

        ConstantsNameResolver<Object> constantsNameResolver = new ConstantsNameResolver<Object>(mockConstantsManager, "TestConstant") {
            public Collection<Object> getAll() {
                return null;
            }
        };

        final List<String> ids = constantsNameResolver.getIdsFromName("blah");
        assertEquals(1, ids.size());
        assertEquals("10000", ids.get(0));
    }

    @Test
    public void testNameExists() throws Exception {
        final MockGenericValue genericValue = new MockGenericValue("TestConst", FieldMap.build("id", "10000"));

        when(mockConstantsManager.getConstantByNameIgnoreCase("TestConstant", "blah"))
                .thenReturn(new StatusImpl(genericValue, null, null, null, null));

        ConstantsNameResolver<Object> constantsNameResolver = new ConstantsNameResolver<Object>(mockConstantsManager, "TestConstant") {
            public Collection<Object> getAll() {
                return null;
            }
        };

        assertTrue(constantsNameResolver.nameExists("blah"));
    }

    @Test
    public void testNameDoesNotExist() throws Exception {
        when(mockConstantsManager.getConstantByNameIgnoreCase("TestConstant", "blah"))
                .thenReturn(null);

        ConstantsNameResolver<Object> constantsNameResolver = new ConstantsNameResolver<Object>(mockConstantsManager, "TestConstant") {
            public Collection<Object> getAll() {
                return null;
            }
        };

        assertFalse(constantsNameResolver.nameExists("blah"));
    }

    @Test
    public void testGet() throws Exception {
        when(mockConstantsManager.getConstantObject("TestConstant", "123"))
                .thenReturn(mockIssueConstant);

        ConstantsNameResolver<Object> constantsNameResolver = new ConstantsNameResolver<Object>(mockConstantsManager, "TestConstant") {
            public Collection<Object> getAll() {
                return null;
            }
        };

        final Object o = constantsNameResolver.get(123L);
        assertEquals(mockIssueConstant, o);
    }

    @Test
    public void testIdExists() throws Exception {
        ConstantsNameResolver<Object> constantsNameResolver = new ConstantsNameResolver<Object>(null, "TestConstant") {
            public Collection<Object> getAll() {
                return null;
            }

            public Object get(final Long id) {
                return new Object();
            }
        };

        assertTrue(constantsNameResolver.idExists(12L));
    }

}
