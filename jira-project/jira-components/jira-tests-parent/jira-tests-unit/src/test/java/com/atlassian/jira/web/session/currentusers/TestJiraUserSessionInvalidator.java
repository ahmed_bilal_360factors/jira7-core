package com.atlassian.jira.web.session.currentusers;

import com.atlassian.crowd.event.user.UserCredentialUpdatedEvent;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.mock.component.MockComponentWorker;
import com.atlassian.jira.mock.servlet.MockHttpServletRequest;
import com.google.common.collect.ImmutableList;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class TestJiraUserSessionInvalidator {

    @Mock
    private EventPublisher eventPublisher;

    @Mock
    private HttpSession httpSession;
    private final String SESSION_ID = "ID";

    @Mock
    private JiraUserSessionTracker jiraUserSessionTracker;

    private JiraUserSessionInvalidator jiraUserSessionInvalidator;

    @Before
    public void setup() {
        ComponentAccessor.initialiseWorker(new MockComponentWorker()
            .addMock(JiraUserSessionTracker.class, jiraUserSessionTracker)
        );

        when(httpSession.getId()).thenReturn(SESSION_ID);

        jiraUserSessionInvalidator = new JiraUserSessionInvalidator();
    }

    @Test
    public void sessionShouldBeInvalidatedIfIsMarked() {
        HttpServletRequest httpServletRequest = new MockHttpServletRequest(httpSession);
        when(jiraUserSessionTracker.isSessionMarkedForInvalidation(SESSION_ID)).thenReturn(true);

        jiraUserSessionInvalidator.handleSessionInvalidation(httpServletRequest);

        verify(httpSession).invalidate();
        verify(jiraUserSessionTracker).removeInvalidateFlagFromSession(SESSION_ID);
    }

    @Test
    public void sessionShouldNotBeInvalidatedIfSessionDoesNotExist() {
        HttpServletRequest httpServletRequest = new MockHttpServletRequest();

        jiraUserSessionInvalidator.handleSessionInvalidation(httpServletRequest);

        verify(httpSession, never()).invalidate();
        verify(jiraUserSessionTracker, never()).removeInvalidateFlagFromSession(any());
    }

    @Test
    public void sessionShouldNotBeInvalidatedIfIsNotMarkedAsInvalidate() {
        HttpServletRequest httpServletRequest = new MockHttpServletRequest(httpSession);

        jiraUserSessionInvalidator.handleSessionInvalidation(httpServletRequest);

        verify(httpSession, never()).invalidate();
    }

    @Test
    public void invalidationFlagFromSessionShouldBeRemoved() {
        HttpServletRequest httpServletRequest = new MockHttpServletRequest(httpSession);

        jiraUserSessionInvalidator.removeInvalidationFlagFromSession(httpServletRequest);

        verify(jiraUserSessionTracker).removeInvalidateFlagFromSession(SESSION_ID);
    }

    @Test
    public void invalidateAllSessionsShouldInvalidateOnlyUserSessions() {
        final List<JiraUserSession> mockedSessionList = ImmutableList.of(
                mockSession("1", null), mockSession("2", "other"), mockSession("3", "fred"));
        when(jiraUserSessionTracker.getSnapshot()).thenReturn(mockedSessionList);

        jiraUserSessionInvalidator.onUserCredentialUpdatedEvent(userCredentialUpdatedEvent("fred"));

        verify(jiraUserSessionTracker).markSessionAsInvalid("3");
        verify(jiraUserSessionTracker, never()).markSessionAsInvalid("1");
        verify(jiraUserSessionTracker, never()).markSessionAsInvalid("2");
    }


    private JiraUserSession mockSession(String id, String username) {
        final JiraUserSession jiraUserSession = mock(JiraUserSession.class);

        when(jiraUserSession.getId()).thenReturn(id);
        when(jiraUserSession.getUserName()).thenReturn(username);

        return jiraUserSession;
    }

    private UserCredentialUpdatedEvent userCredentialUpdatedEvent(String username) {
        final UserCredentialUpdatedEvent userCredentialUpdatedEvent = mock(UserCredentialUpdatedEvent.class);

        when(userCredentialUpdatedEvent.getUsername()).thenReturn(username);

        return userCredentialUpdatedEvent;
    }
}
