package com.atlassian.jira.issue.fields.csv;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.export.FieldExportParts;
import com.atlassian.jira.issue.fields.TimeSpentSystemField;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;

import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.I18nHelper;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

public class TimeSpentSystemFieldCsvExportTest {
    @Rule
    public final RuleChain mockito = MockitoMocksInContainer.forTest(this);

    private static final Long TIME_SPENT = 500L;

    @Mock
    private Issue issue;

    @Mock
    private JiraAuthenticationContext authenticationContext;

    @Mock
    private I18nHelper i18nHelper;

    @InjectMocks
    private TimeSpentSystemField field;

    @Before
    public void setUp() {
        when(authenticationContext.getI18nHelper()).thenReturn(i18nHelper);
    }

    @Test
    public void testCsvRepresentationForExpectedTypeAndValues() {
        when(issue.getTimeSpent()).thenReturn(TIME_SPENT);

        final FieldExportParts representation = field.getRepresentationFromIssue(issue);
        assertThat(representation.getParts(), hasSize(1));
        assertThat(representation.getParts().get(0).getValues()::iterator, contains(String.valueOf(TIME_SPENT)));
    }

    @Test
    public void testCsvRepresentationWhenTimeSpentIsNull() {
        when(issue.getTimeSpent()).thenReturn(null);

        final FieldExportParts representation = field.getRepresentationFromIssue(issue);
        assertThat(representation.getParts(), hasSize(1));
        assertThat(representation.getParts().get(0).getValues()::iterator, contains(String.valueOf("")));
    }
}
