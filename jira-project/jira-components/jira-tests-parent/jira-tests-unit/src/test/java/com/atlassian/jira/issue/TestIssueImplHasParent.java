package com.atlassian.jira.issue;

import com.atlassian.jira.bc.project.component.ProjectComponentManager;
import com.atlassian.jira.config.SubTaskManager;
import com.atlassian.jira.issue.label.LabelManager;
import com.atlassian.jira.mock.issue.MockIssue;
import com.atlassian.jira.mock.ofbiz.MockGenericValue;
import com.atlassian.jira.project.version.VersionManager;
import org.junit.Test;
import org.mockito.Mockito;
import org.ofbiz.core.entity.GenericEntityException;
import org.ofbiz.core.entity.GenericValue;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.when;

public class TestIssueImplHasParent {

    @Test
    public void testIssueImplHasNoParent() {
        MockGenericValue gv = new MockGenericValue("Issue");
        SubTaskManager subTaskManager = Mockito.mock(SubTaskManager.class);
        IssueImpl issue = new IssueImpl(gv, null, null, null, null, null, subTaskManager, null, null, null, null, null);

        when(subTaskManager.getParentIssueId(issue)).thenReturn(null);

        assertNull(issue.getParentId());
    }

    @Test
    public void testIssueImplHasParent() {
        MockGenericValue gv = new MockGenericValue("Issue");

        Long parentId = new Long(999);
        SubTaskManager subTaskManager = Mockito.mock(SubTaskManager.class);
        IssueImpl issue = new IssueImpl(gv, null, null, null, null, null, subTaskManager, null, null, null, null, null);
        when(subTaskManager.getParentIssueId(issue)).thenReturn(parentId);

        assertEquals(parentId, issue.getParentId());
    }

    /**
     * Test to see if you can override the has parent call.
     * issue.hasNoParentId can be set to ensure in memory issues don't go to the database to get parent issue id
     * Set to true to override.
     */
    @Test
    public void testIssueImplHasParentButOverriden() {
        MockGenericValue gv = new MockGenericValue("Issue");

        Long parentId = new Long(999);
        SubTaskManager subTaskManager = Mockito.mock(SubTaskManager.class);
        IssueImpl issue = new IssueImpl(gv, null, null, null, null, null, subTaskManager, null, null, null, null, null);
        when(subTaskManager.getParentIssueId(issue)).thenReturn(parentId);
        issue.hasNoParentId = true;

        assertNull(issue.getParentId());
    }

    @Test
    public void testCopyIssueHasSameParent() throws Exception {
        MockIssue mock = new MockIssue(new Long(123)) {
            protected GenericValue getHackedGVThatReturnsId() {
                return null;
            }
        };

        final ProjectComponentManager projectComponentManager = createProjectComponentManager();
        // check with no parent id
        IssueImpl copy = new IssueImpl(mock, null, null, null, null, null, null, null, null, projectComponentManager, null, null);
        assertEquals(mock.getParentId(), copy.getParentId());

        // check with parent id
        mock.setParentId(new Long(987));
        copy = new IssueImpl(mock, null, null, null, null, null, null, null, null, projectComponentManager, null, null);
        assertEquals(mock.getParentId(), copy.getParentId());
    }

    @Test
    public void testCopyIssueHasSameParentGVNoParent() throws Exception {
        MockGenericValue gv = new MockGenericValue("Issue");

        SubTaskManager subTaskManager = createSubTaskManager(gv, null);
        VersionManager versionManager = createVersionManager();
        IssueManager issueManager = createIssueManager();
        LabelManager labelManager = createLabelManager();
        final ProjectComponentManager projectComponentManager = createProjectComponentManager();

        IssueImpl issue = new IssueImpl(gv, issueManager, null, versionManager, null, null, subTaskManager, null, labelManager, projectComponentManager, null, null);
        assertNull(issue.getParentId());

        IssueImpl copy = new IssueImpl(issue, null, null, null, null, null, null, null, null, projectComponentManager, null, null);
        assertEquals(issue.getParentId(), copy.getParentId());
    }

    @Test
    public void testCopyIssueHasSameParentGVWithParent() throws Exception {
        MockGenericValue gv = new MockGenericValue("Issue");

        SubTaskManager subTaskManager = createSubTaskManager(gv, new Long(987));
        VersionManager versionManager = createVersionManager();
        IssueManager issueManager = createIssueManager();
        LabelManager labelManager = createLabelManager();
        final ProjectComponentManager projectComponentManager = createProjectComponentManager();

        IssueImpl issue = new IssueImpl(gv, issueManager, null, versionManager, null, null, subTaskManager, null, labelManager, projectComponentManager, null, null);
        assertEquals(new Long(987), issue.getParentId());

        IssueImpl copy = new IssueImpl(issue, null, null, null, null, null, null, null, null, projectComponentManager, null, null);
        assertEquals(issue.getParentId(), copy.getParentId());
    }

    private SubTaskManager createSubTaskManager(MockGenericValue gv, Long parentIssueId) {
        SubTaskManager subTaskManager = Mockito.mock(SubTaskManager.class);
        IssueImpl issue = new IssueImpl(gv, null, null, null, null, null, subTaskManager, null, null, null, null, null);
        when(subTaskManager.getParentIssueId(issue)).thenReturn(parentIssueId);
        return subTaskManager;
    }

    private IssueManager createIssueManager() throws GenericEntityException {
        IssueManager issueManager = Mockito.mock(IssueManager.class);
        return issueManager;
    }

    private ProjectComponentManager createProjectComponentManager() {
        ProjectComponentManager projectComponentManager = Mockito.mock(ProjectComponentManager.class);
        return projectComponentManager;
    }

    private LabelManager createLabelManager() {
        LabelManager labelManager = Mockito.mock(LabelManager.class);
        return labelManager;
    }

    private VersionManager createVersionManager() {
        VersionManager versionManager = Mockito.mock(VersionManager.class);
        return versionManager;
    }
}
