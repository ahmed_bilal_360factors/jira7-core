package com.atlassian.jira.servermetrics;

import com.atlassian.jira.application.install.ThrowableFunctions;
import com.atlassian.jira.matchers.OptionalMatchers;
import com.google.common.collect.Maps;
import org.junit.Test;

import javax.servlet.http.HttpServletRequest;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.toList;
import static org.hamcrest.Matchers.contains;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestMultiThreadedRequestKeyResolver {

    public static final String INNER_REQUEST_ID = "inner";
    public static final String OUTER_REQUEST_ID = "outer";
    public static final String FIRST_REQUEST_ID = "first";
    public static final String SECOND_REQUEST_ID = "second";

    @Test
    public void shouldGiveEmptyIdWhenResolverDoesntDefineOne() {
        MultiThreadedRequestKeyResolver testObj = new MultiThreadedRequestKeyResolver(new MockRequestKeyResolver(ignore -> Optional.empty()));

        final HttpServletRequest outerRequest = mock(HttpServletRequest.class);
        final HttpServletRequest innerRequest = mock(HttpServletRequest.class);
        testObj.requestStarted(outerRequest);
        testObj.requestStarted(innerRequest);
        testObj.requestFinished(innerRequest);

        // when
        final Optional<String> requestId = testObj.requestFinished(outerRequest);

        // then
        assertThat(requestId, OptionalMatchers.none());
    }

    @Test
    public void shouldReturnOuterRequestIfInnerDoesntRedefinIt() {
        final HttpServletRequest outerRequest = mock(HttpServletRequest.class);
        final HttpServletRequest innerRequest = mock(HttpServletRequest.class);

        MultiThreadedRequestKeyResolver testObj =
                new MultiThreadedRequestKeyResolver(
                        MockRequestKeyResolver.build()
                                .add(outerRequest, Optional.of(OUTER_REQUEST_ID))
                                .add(innerRequest, Optional.empty())
                                .build());

        // when
        testObj.requestStarted(outerRequest);
        testObj.requestStarted(innerRequest);
        final Optional<String> innerRequestId = testObj.requestFinished(innerRequest);
        final Optional<String> outerRequestId = testObj.requestFinished(outerRequest);

        // then
        assertThat(innerRequestId, OptionalMatchers.none());
        assertThat(outerRequestId, OptionalMatchers.some(OUTER_REQUEST_ID));
    }

    @Test
    public void innerRequestIdShouldOverwriteOuter() {
        final HttpServletRequest outerRequest = mock(HttpServletRequest.class);
        final HttpServletRequest innerRequest = mock(HttpServletRequest.class);

        MultiThreadedRequestKeyResolver testObj =
                new MultiThreadedRequestKeyResolver(
                        MockRequestKeyResolver.build()
                                .add(outerRequest, Optional.of(OUTER_REQUEST_ID))
                                .add(innerRequest, Optional.of(INNER_REQUEST_ID))
                                .build());

        // when
        testObj.requestStarted(outerRequest);
        testObj.requestStarted(innerRequest);
        final Optional<String> innerRequestId = testObj.requestFinished(innerRequest);
        final Optional<String> outerRequestId = testObj.requestFinished(outerRequest);

        // then
        assertThat(innerRequestId, OptionalMatchers.some(INNER_REQUEST_ID));
        assertThat(outerRequestId, OptionalMatchers.some(INNER_REQUEST_ID));
    }

    @Test
    public void shouldHandleSubsequentRequest() {
        final HttpServletRequest firstRequest = mock(HttpServletRequest.class);
        final HttpServletRequest secondRequest = mock(HttpServletRequest.class);

        MultiThreadedRequestKeyResolver testObj =
                new MultiThreadedRequestKeyResolver(
                        MockRequestKeyResolver.build()
                                .add(firstRequest, Optional.of(FIRST_REQUEST_ID))
                                .add(secondRequest, Optional.of(SECOND_REQUEST_ID))
                                .build());

        // when
        testObj.requestStarted(firstRequest);
        final Optional<String> firstRequestId = testObj.requestFinished(firstRequest);
        testObj.requestStarted(secondRequest);
        final Optional<String> secondRequestId = testObj.requestFinished(secondRequest);

        // then
        assertThat(firstRequestId, OptionalMatchers.some(FIRST_REQUEST_ID));
        assertThat(secondRequestId, OptionalMatchers.some(SECOND_REQUEST_ID));
    }

    @Test
    public void shouldNotMixCheckpointInEachThtread() throws InterruptedException {
        MultiThreadedRequestKeyResolver testObj =
                new MultiThreadedRequestKeyResolver(
                        new MockRequestKeyResolver(httpServletRequest -> Optional.of(httpServletRequest.getContextPath())));

        int numThreads = 16;
        final List<String> perThreadRequestIds = IntStream.range(0, numThreads)
                .mapToObj(num -> "request-id-" + num)
                .collect(toList());

        final List<Callable<Optional<String>>> executors = perThreadRequestIds.stream()
                .map(checkpointName -> (Callable<Optional<String>>) () -> {
                            final HttpServletRequest request = mock(HttpServletRequest.class);
                            when(request.getContextPath()).thenReturn(checkpointName);

                            testObj.requestStarted(request);
                            return testObj.requestFinished(request);
                        }
                )
                .collect(toList());

        // when
        final ExecutorService fixedThreadPool = Executors.newFixedThreadPool(numThreads);
        try {
            final List<Optional<String>> threadRequestIds = fixedThreadPool
                    .invokeAll(executors)
                    .stream()
                    .map(ThrowableFunctions.uncheckFunction(future -> future.get(500, TimeUnit.MILLISECONDS)))
                    .collect(toList());

            // then
            assertThat(
                    threadRequestIds,
                    contains(perThreadRequestIds.stream().map(OptionalMatchers::some).collect(toList())));
        } finally {
            fixedThreadPool.shutdown();
        }
    }


    private static class MockRequestKeyResolver extends RequestKeyResolver {
        private final Function<HttpServletRequest, Optional<String>> resolvingFunction;

        private MockRequestKeyResolver(Function<HttpServletRequest, Optional<String>> resolvingFunction) {
            this.resolvingFunction = resolvingFunction;
        }

        @Override
        public Optional<String> getRequestId(HttpServletRequest httpServletRequest) {
            return resolvingFunction.apply(httpServletRequest);
        }

        public static Builder build() {
            return new Builder();
        }

        public static class Builder {
            final IdentityHashMap<HttpServletRequest, Optional<String>> requestToId = Maps.newIdentityHashMap();

            public Builder add(HttpServletRequest rq, Optional<String> res) {
                requestToId.put(rq, res);
                return this;
            }

            public MockRequestKeyResolver build() {
                return new MockRequestKeyResolver(requestToId::get);
            }
        }
    }
}