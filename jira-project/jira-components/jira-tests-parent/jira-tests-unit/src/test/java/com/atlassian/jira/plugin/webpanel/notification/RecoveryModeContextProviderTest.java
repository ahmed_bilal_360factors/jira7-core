package com.atlassian.jira.plugin.webpanel.notification;

import com.atlassian.jira.help.MockHelpUrl;
import com.atlassian.jira.help.MockHelpUrls;
import com.atlassian.jira.mock.security.MockSimpleAuthenticationContext;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.util.StaticRecoveryMode;
import com.google.common.collect.ImmutableMap;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.Map;

import static com.atlassian.jira.mock.security.MockSimpleAuthenticationContext.createNoopContext;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

public class RecoveryModeContextProviderTest {
    private static final String RECOVERY_USER = "recovery_admin";

    private final MockHelpUrl helpUrl = new MockHelpUrl().setKey("recovery-mode").setUrl("http://example.com/");
    private final MockHelpUrls helpUrls = new MockHelpUrls().addUrl(helpUrl);
    private final StaticRecoveryMode recoveryMode = StaticRecoveryMode.enabled(RECOVERY_USER);
    private final GlobalPermissionManager permissionManager = Mockito.mock(GlobalPermissionManager.class);

    @Test
    public void checkContextAsRecoveryUser() {
        //given
        final MockApplicationUser currentUser = new MockApplicationUser(RECOVERY_USER);
        when(permissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, currentUser)).thenReturn(true);
        final MockSimpleAuthenticationContext authCtx = createNoopContext(currentUser);
        final RecoveryModeContextProvider provider = new RecoveryModeContextProvider(recoveryMode, helpUrls, authCtx, permissionManager);

        //when
        final Map<String, Object> map = provider.getContextMap(ImmutableMap.of());

        //then
        assertThat(map, Matchers.hasEntry("helpUrl", helpUrl.getUrl()));
        assertThat(map, Matchers.hasEntry("isAdmin", true));
        assertThat(map, Matchers.hasEntry("isRecoveryAdmin", true));
    }

    @Test
    public void checkContextAsAdminUser() {
        //given
        final MockApplicationUser currentUser = new MockApplicationUser("not_" + RECOVERY_USER);
        when(permissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, currentUser)).thenReturn(true);
        final MockSimpleAuthenticationContext authCtx = createNoopContext(currentUser);
        final RecoveryModeContextProvider provider = new RecoveryModeContextProvider(recoveryMode, helpUrls, authCtx, permissionManager);

        //when
        final Map<String, Object> map = provider.getContextMap(ImmutableMap.of());

        //then
        assertThat(map, Matchers.hasEntry("helpUrl", helpUrl.getUrl()));
        assertThat(map, Matchers.hasEntry("isAdmin", true));
        assertThat(map, Matchers.hasEntry("isRecoveryAdmin", false));
    }

    @Test
    public void checkContextAsNonAdminUser() {
        //given
        final MockApplicationUser currentUser = new MockApplicationUser("not_" + RECOVERY_USER);
        final MockSimpleAuthenticationContext authCtx = createNoopContext(currentUser);
        final RecoveryModeContextProvider provider = new RecoveryModeContextProvider(recoveryMode, helpUrls, authCtx, permissionManager);

        //when
        final Map<String, Object> map = provider.getContextMap(ImmutableMap.of());

        //then
        assertThat(map, Matchers.hasEntry("helpUrl", helpUrl.getUrl()));
        assertThat(map, Matchers.hasEntry("isAdmin", false));
        assertThat(map, Matchers.hasEntry("isRecoveryAdmin", false));
    }
}