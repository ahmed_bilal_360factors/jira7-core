package com.atlassian.jira.sharing.type;

import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.sharing.SharePermission;
import com.atlassian.jira.sharing.SharePermissionImpl;
import com.atlassian.jira.sharing.SharedEntity;
import com.atlassian.jira.sharing.SharedEntity.TypeDescriptor;
import com.atlassian.jira.sharing.search.ShareTypeSearchParameter;
import com.atlassian.jira.sharing.type.ShareType.Name;
import com.atlassian.jira.user.ApplicationUser;
import com.google.common.collect.ImmutableSet;
import org.apache.lucene.document.Field;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.Query;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import java.util.Comparator;
import java.util.Map;

import static org.hamcrest.Matchers.contains;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.same;
import static org.mockito.Mockito.when;

/**
 * Test for the {@link DefaultShareTypeFactory}.
 *
 * @since v3.13
 */

public class TestDefaultShareTypeFactory {
    private static final Name BADTYPE = new Name("badtype");
    private static final SharePermission BAD_PERMISSION = new SharePermissionImpl(TestDefaultShareTypeFactory.BADTYPE, null, null);

    @Rule
    public MethodRule initMockito = MockitoJUnit.rule();
    @Mock
    private Comparator<SharePermission> type1Comparator;
    @Mock
    private Comparator<SharePermission> type2Comparator;
    @Mock
    private Comparator<SharePermission> type3Comparator;

    private ShareType type1;
    private ShareType type2;
    private ShareType type3;

    private DefaultShareTypeFactory factory;

    private SharePermission permission1;
    private SharePermission permission2;
    private SharePermission permission3;

    @Before
    public void setUp() throws Exception {
        type1 = new AbstractShareType(new Name("type1"), false, 3, createNopShareTypeRenderer(), createNopValidator(), createNopPermissionChecker(),
                createNopShareTypeQueryBuilder(), type1Comparator);

        type2 = new AbstractShareType(new Name("type2"), false, 1, createNopShareTypeRenderer(), createNopValidator(), createNopPermissionChecker(),
                createNopShareTypeQueryBuilder(), type2Comparator);

        type3 = new AbstractShareType(new Name("type3"), false, 7, createNopShareTypeRenderer(), createNopValidator(), createNopPermissionChecker(),
                createNopShareTypeQueryBuilder(), type3Comparator);
        factory = new DefaultShareTypeFactory(ImmutableSet.of(type1, type2, type3));

        permission1 = new SharePermissionImpl(type1.getType(), "ahh", null);
        permission2 = new SharePermissionImpl(type1.getType(), "ahh", null);
        permission3 = new SharePermissionImpl(type2.getType(), "ahh", null);
    }

    private ShareQueryFactory createNopShareTypeQueryBuilder() {
        return new ShareQueryFactory() {
            @Override
            public Query getQuery(ShareTypeSearchParameter parameter, ApplicationUser user) {
                throw new UnsupportedOperationException("Not implemented");
            }

            public Query getQuery(final ShareTypeSearchParameter searchParameter) {
                return null;
            }

            @Override
            public Term[] getTerms(ApplicationUser user) {
                throw new UnsupportedOperationException("Not implemented");
            }

            public Field getField(final SharedEntity entity, final SharePermission permission) {
                throw new UnsupportedOperationException();
            }
        };
    }

    @Test
    public void testGetShareType() {
        assertSame(type1, factory.getShareType(type1.getType()));
        assertSame(type2, factory.getShareType(type2.getType()));
        assertSame(type3, factory.getShareType(type3.getType()));

        assertNull(factory.getShareType(TestDefaultShareTypeFactory.BADTYPE));
    }

    @Test
    public void testGetAllShareTypes() {
        assertThat(factory.getAllShareTypes(), contains(type2, type1, type3));
    }

    @Test
    public void testComparatorSamePermission() {
        final Comparator comparator = factory.getPermissionComparator();
        assertEquals(0, comparator.compare(permission1, permission1));
    }

    @Test
    public void testComparatorEqualsPermission() {
        when(type1Comparator.compare(same(permission1), same(permission2))).thenReturn(0);

        final Comparator comparator = factory.getPermissionComparator();
        assertEquals(0, comparator.compare(permission1, permission2));
    }

    @Test
    public void testComparatorNotEqualsPermission() {
        when(type1Comparator.compare(same(permission1), same(permission2))).thenReturn(Integer.MAX_VALUE);

        final Comparator comparator = factory.getPermissionComparator();
        assertEquals(Integer.MAX_VALUE, comparator.compare(permission1, permission2));
    }

    @Test
    public void testComparatorLeftTypeMorePermission() {
        final Comparator comparator = factory.getPermissionComparator();
        assertTrue(comparator.compare(permission1, permission3) > 0);
    }

    @Test
    public void testComparatorLeftTypeLessPermission() {
        final Comparator comparator = factory.getPermissionComparator();
        assertTrue(comparator.compare(permission3, permission1) < 0);
    }

    @Test
    public void testComparatorLeftNull() {
        final Comparator comparator = factory.getPermissionComparator();
        assertTrue(comparator.compare(null, permission2) < 0);
    }

    @Test
    public void testComparatorRightNull() {
        final Comparator comparator = factory.getPermissionComparator();
        assertTrue(comparator.compare(permission1, null) > 0);
    }

    @Test
    public void testComparatorBothNull() {
        final Comparator comparator = factory.getPermissionComparator();
        assertTrue(comparator.compare(null, null) == 0);
    }

    @Test
    public void testComparatorBadLeft() {
        final Comparator comparator = factory.getPermissionComparator();
        assertTrue(comparator.compare(TestDefaultShareTypeFactory.BAD_PERMISSION, permission1) < 0);
    }

    @Test
    public void testComparatorBadRight() {
        final Comparator comparator = factory.getPermissionComparator();
        assertTrue(comparator.compare(permission1, TestDefaultShareTypeFactory.BAD_PERMISSION) > 0);
    }

    @Test
    public void testComparatorBoth() {
        final Comparator comparator = factory.getPermissionComparator();
        assertTrue(comparator.compare(TestDefaultShareTypeFactory.BAD_PERMISSION, TestDefaultShareTypeFactory.BAD_PERMISSION) == 0);
    }

    @Test(expected = ClassCastException.class)
    public void testComparatorBadType() {
        final Comparator comparator = factory.getPermissionComparator();
        comparator.compare("aa", "bb");
        fail("Should not accept invalid types.");
    }

    private static ShareTypePermissionChecker createNopPermissionChecker() {
        return new ShareTypePermissionChecker() {
            public boolean hasPermission(final ApplicationUser user, final SharePermission permission) {
                return false;
            }
        };
    }

    private static ShareTypeValidator createNopValidator() {
        return new ShareTypeValidator() {
            public boolean checkSharePermission(final JiraServiceContext ctx, final SharePermission permission) {
                return false;
            }

            public boolean checkSearchParameter(final JiraServiceContext ctx, final ShareTypeSearchParameter searchParameter) {
                return false;
            }
        };
    }

    private static ShareTypeRenderer createNopShareTypeRenderer() {
        return new ShareTypeRenderer() {
            public String renderPermission(final SharePermission permission, final JiraAuthenticationContext userCtx) {
                return null;
            }

            public String getSimpleDescription(final SharePermission permission, final JiraAuthenticationContext userCtx) {
                return null;
            }

            public String getShareTypeEditor(final JiraAuthenticationContext userCtx) {
                return null;
            }

            public boolean isAddButtonNeeded(final JiraAuthenticationContext userCtx) {
                return false;
            }

            public String getShareTypeLabel(final JiraAuthenticationContext userCtx) {
                return null;
            }

            public Map /*<String, String>*/getTranslatedTemplates(final JiraAuthenticationContext userCtx, final TypeDescriptor type, final RenderMode mode) {
                return null;
            }
        };
    }
}
