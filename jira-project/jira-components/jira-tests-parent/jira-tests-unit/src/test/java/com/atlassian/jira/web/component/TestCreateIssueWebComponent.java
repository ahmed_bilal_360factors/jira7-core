package com.atlassian.jira.web.component;

import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.security.PermissionManager;
import org.junit.Test;

import static com.atlassian.jira.permission.ProjectPermissions.CREATE_ISSUES;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestCreateIssueWebComponent {

    @Test
    public void showChecksPermissionForCreateIssuesInProject() throws Exception {
        MockProject project = new MockProject(123);

        final PermissionManager mockPermissionManager = mock(PermissionManager.class);
        when(mockPermissionManager.hasPermission(CREATE_ISSUES, project, null)).thenReturn(false).thenReturn(true);

        final CreateIssueWebComponent component = new CreateIssueWebComponent(null, null, mockPermissionManager, null);

        assertFalse(component.show(null, null));
        assertFalse(component.show(project, null));
        assertTrue(component.show(project, null));
    }

}
