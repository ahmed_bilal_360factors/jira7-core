package com.atlassian.jira.auditing;


import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.transaction.Transaction;
import com.atlassian.jira.transaction.TransactionSupport;
import org.hamcrest.Matcher;
import org.hamcrest.collection.IsMapContaining;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Map;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AuditingStoreImplTest {
    @Mock
    private OfBizDelegator ofBizDelegator;
    @Mock
    private TransactionSupport transactionSupport;
    @Mock
    private Transaction transaction;

    AuditingCategory category = AuditingCategory.MIGRATION;

    @Before
    public void beforeMethod() {
        when(transactionSupport.begin()).thenReturn(transaction);
    }

    @Test
    public void descriptionShouldBeStoredIfPresent() {
        AuditingStoreImpl auditingStore = new AuditingStoreImpl(ofBizDelegator, transactionSupport);

        AuditingEntry entry = AuditingEntry.builder(category, "some summary", "Some source")
                .categoryName(null)
                .author(null)
                .remoteAddress(null)
                .objectItem(null)
                .changedValues(null)
                .associatedItems(null)
                .isAuthorSysAdmin(false)
                .description("some description").build();
        auditingStore.storeRecord(entry);
        Matcher description = IsMapContaining.hasEntry("longDescription", "some description");

        verify(ofBizDelegator).createValue(anyString(), Matchers.<Map<String, Object>>argThat(description));
    }

    @Test
    public void absentDescriptionShouldNotCauseAnException() {
        AuditingStoreImpl auditingStore = new AuditingStoreImpl(ofBizDelegator, transactionSupport);

        auditingStore.storeRecord(category, null, "some summary", "Some source", null, null, null, null, null, false);
        Matcher description = IsMapContaining.hasEntry("description", "some description");

        verify(ofBizDelegator, times(0)).createValue(anyString(), Matchers.<Map<String, Object>>argThat(description));
    }
}
