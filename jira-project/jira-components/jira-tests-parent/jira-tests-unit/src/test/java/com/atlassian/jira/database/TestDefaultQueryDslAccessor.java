package com.atlassian.jira.database;

import java.sql.Connection;
import java.util.function.Function;

import com.atlassian.jira.config.database.DatabaseConfig;
import com.atlassian.jira.config.database.DatabaseConfigurationManager;
import com.atlassian.jira.config.database.Datasource;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.ofbiz.core.entity.DelegatorInterface;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestDefaultQueryDslAccessor {
    @Rule
    public final MethodRule initMockito = MockitoJUnit.rule();

    @Mock
    private DatabaseAccessor databaseAccessor;
    @Mock
    private DelegatorInterface delegatorInterface;
    @Mock
    private DatabaseConfigurationManager databaseConfigurationManager;

    @Mock
    private Datasource datasource;
    private DatabaseConfig databaseConfig;
    @Mock
    private Connection connection;
    @Mock
    private DatabaseConnection databaseConnection;

    private DefaultQueryDslAccessor accessor;

    @Before
    public void setUp() throws Exception {
        databaseConfig = new DatabaseConfig("h2", "public", datasource);
        when(databaseConfigurationManager.getDatabaseConfiguration()).thenReturn(databaseConfig);

        when(databaseAccessor.runInTransaction(any(Function.class))).thenAnswer(args -> ((Function) args.getArguments()[0]).apply(connection));
        when(databaseAccessor.executeQuery(any(ConnectionFunction.class))).thenAnswer(args -> ((ConnectionFunction) args.getArguments()[0]).run(databaseConnection));

        accessor = new DefaultQueryDslAccessor(databaseAccessor, delegatorInterface, databaseConfigurationManager);
    }

    @Test
    public void shouldExecuteRunInTransactionByDefault() {
        accessor.execute(dbConnection -> { });

        verify(databaseAccessor).runInTransaction(any(Function.class));
    }

    @Test
    public void shouldExecuteQueryRunInTransactionByDefault() {
        assertEquals("result", accessor.executeQuery(dbConnection -> "result"));

        verify(databaseAccessor).runInTransaction(any(Function.class));
    }

    @Test
    public void shouldExecuteRunBorrowNewConnection() {
        accessor.withNewConnection().execute(dbConnection -> { });

        verify(databaseAccessor).executeQuery(any(ConnectionFunction.class));
    }

    @Test
    public void shouldExecuteQueryBorrowNewConnection() {
        final String result = accessor.withNewConnection().executeQuery(dbConnection -> "result");
        assertEquals("result", result);

        verify(databaseAccessor).executeQuery(any(ConnectionFunction.class));
    }
}
