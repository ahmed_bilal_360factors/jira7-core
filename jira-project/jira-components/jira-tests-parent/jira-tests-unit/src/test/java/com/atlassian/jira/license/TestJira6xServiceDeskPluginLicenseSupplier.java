package com.atlassian.jira.license;

import com.atlassian.extras.api.LicenseException;
import com.atlassian.fugue.Option;
import com.atlassian.jira.matchers.OptionMatchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static com.atlassian.jira.application.ApplicationKeys.SERVICE_DESK;
import static com.atlassian.jira.test.util.lic.sd.ServiceDeskLicenses.LICENSE_SERVICE_DESK_ABP;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Tests {@link Jira6xServiceDeskPluginLicenseSupplier}.
 *
 * @since 7.0
 */
@RunWith(MockitoJUnitRunner.class)
public class TestJira6xServiceDeskPluginLicenseSupplier {
    private static final String SERVICE_DESK_LICENSE = LICENSE_SERVICE_DESK_ABP.getLicenseString();

    @Mock
    private Jira6xServiceDeskPluginEncodedLicenseSupplier encodedLicenseSupplier;
    @Mock
    private LicenseDetailsFactory licenseDetailsFactory;
    @Mock
    private LicenseDetails sdLicense, otherLicense;

    private Jira6xServiceDeskPluginLicenseSupplier supplier;

    @Before
    public void setup() {
        supplier = new Jira6xServiceDeskPluginLicenseSupplier(encodedLicenseSupplier, licenseDetailsFactory);

        when(licenseDetailsFactory.getLicense(SERVICE_DESK_LICENSE)).thenReturn(sdLicense);
        when(sdLicense.hasApplication(SERVICE_DESK)).thenReturn(true);
    }

    @Test
    public void sdLicenseReturnedWhenSdLicensePresent() {
        when(encodedLicenseSupplier.get()).thenReturn(Option.some(SERVICE_DESK_LICENSE));
        assertThat(supplier.get(), OptionMatchers.some(sdLicense));
    }

    @Test
    public void sdLicenseNotReturnedWhenSdLicenseNotPresent() {
        when(encodedLicenseSupplier.get()).thenReturn(Option.none());
        assertThat(supplier.get(), OptionMatchers.none());
    }

    @Test
    public void sdLicenseNotReturnedWhenLicenseInvalid() {
        when(encodedLicenseSupplier.get()).thenReturn(Option.some(SERVICE_DESK_LICENSE));
        when(licenseDetailsFactory.getLicense(anyString())).thenThrow(new LicenseException());
        assertThat(supplier.get(), OptionMatchers.none());
    }

    @Test
    public void moveToUpgradeStoreDelegates() {
        //when
        supplier.moveToUpgradeStore();

        //then
        verify(encodedLicenseSupplier).moveToUpgradeStore();
    }
}
