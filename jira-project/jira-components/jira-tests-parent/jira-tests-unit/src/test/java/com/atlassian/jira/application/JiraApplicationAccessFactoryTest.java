package com.atlassian.jira.application;

import com.atlassian.application.api.ApplicationAccess;
import com.atlassian.application.api.ApplicationKey;
import com.atlassian.application.host.NotLicensedAccess;
import com.atlassian.fugue.Option;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.license.JiraLicenseManager;
import com.atlassian.jira.license.LicenseDetails;
import com.atlassian.jira.license.MockLicenseDetails;
import com.atlassian.jira.user.util.UserManager;
import org.hamcrest.Matchers;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.mockito.Mock;
import org.mockito.Mockito;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.when;

public class JiraApplicationAccessFactoryTest {
    private static final ApplicationKey EXISTS_KEY = ApplicationKey.valueOf("existsApplicationKey");
    private static final ApplicationKey DONT_EXIST_KEY = ApplicationKey.valueOf("dontExistApplicationKey");
    private static final DateTime BUILD_DATE = DateTime.now();

    @Rule
    public RuleChain init = MockitoMocksInContainer.forTest(this);

    private MockApplicationRole existsApplication = new MockApplicationRole().key(EXISTS_KEY);
    @Mock
    private ApplicationRoleManager applicationRoleManager;
    @Mock
    private UserManager userManager;
    @Mock
    private JiraLicenseManager licenseManager;
    @Mock
    private FeatureManager featureManager;

    @Mock
    @AvailableInContainer
    private ApplicationAuthorizationService service;

    private JiraApplicationAccessFactory factory;

    @Before
    public void setup() {
        when(applicationRoleManager.getRole(EXISTS_KEY))
                .thenReturn(Option.<ApplicationRole>some(existsApplication));
        when(applicationRoleManager.getRole(DONT_EXIST_KEY))
                .thenReturn(Option.none(ApplicationRole.class));

        when(licenseManager.getLicense(Mockito.any(ApplicationKey.class)))
                .thenReturn(Option.none(LicenseDetails.class));

        factory = new JiraApplicationAccessFactory(applicationRoleManager, userManager, licenseManager);
    }

    @Test
    public void notLicensedReturnedWhenRoleDoesNotExist() {
        final ApplicationAccess access = factory.access(DONT_EXIST_KEY, BUILD_DATE);

        assertThat(access, Matchers.instanceOf(NotLicensedAccess.class));
        assertThat(access.getApplicationKey(), Matchers.equalTo(DONT_EXIST_KEY));
        assertThat(access.getManagementPage(), Matchers.equalTo(JiraApplicationAccess.getManagementPageForRole(DONT_EXIST_KEY)));
    }

    @Test
    public void notLicensedReturnedWhenRoleExistsButNotLicensed() {
        final ApplicationAccess access = factory.access(EXISTS_KEY, BUILD_DATE);

        assertThat(access, Matchers.instanceOf(NotLicensedAccess.class));
        assertThat(access.getApplicationKey(), Matchers.equalTo(EXISTS_KEY));
        assertThat(access.getManagementPage(), Matchers.equalTo(JiraApplicationAccess.getManagementPageForRole(EXISTS_KEY)));
    }

    @Test
    public void jiraAccessReturnedWhenRoleExists() {
        final MockApplicationRole mockApplicationRole = new MockApplicationRole().key(EXISTS_KEY);
        final MockLicenseDetails mockLicenseDetails = new MockLicenseDetails();
        when(licenseManager.getLicense(EXISTS_KEY))
                .thenReturn(Option.<LicenseDetails>some(mockLicenseDetails));
        when(applicationRoleManager.getRole(EXISTS_KEY)).thenReturn(Option.<ApplicationRole>some(mockApplicationRole));

        final ApplicationAccess access = factory.access(EXISTS_KEY, BUILD_DATE);
        assertThat(access, Matchers.instanceOf(JiraApplicationAccess.class));
        assertThat(access.getApplicationKey(), Matchers.equalTo(EXISTS_KEY));
        assertThat(access.getManagementPage().toString(), Matchers.equalTo(String.format(JiraApplicationAccess.URL_USER_BROWSER_BTF, EXISTS_KEY.value())));
    }
}