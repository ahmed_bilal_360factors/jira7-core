package com.atlassian.jira.util;

import org.hamcrest.Matcher;
import org.hamcrest.Matchers;

/**
 * Matcher for {@link org.springframework.data.domain.Page}.
 *
 * @since v6.5
 */
public class PageMatcher {
    private PageMatcher() {
    }

    public static <T> Matcher<Page<? extends T>> matcher(final Matcher<Long> startMatcher,
                                                         final Matcher<Long> totalMatcher,
                                                         final Matcher<Integer> sizeMatcher,
                                                         final Matcher<? extends Iterable<? extends T>> valuesMatcher) {
        return Matchers.allOf(Matchers.hasProperty("start", startMatcher),
                Matchers.hasProperty("total", totalMatcher),
                Matchers.hasProperty("size", sizeMatcher),
                Matchers.hasProperty("values", valuesMatcher));
    }

}
