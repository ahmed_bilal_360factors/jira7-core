package com.atlassian.jira.project.template.module;

import com.atlassian.plugin.webresource.UrlMode;
import com.atlassian.plugin.webresource.WebResourceUrlProvider;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestIcon {
    @Test
    public void returnedLocationIsTheEmptyStringIfThereIsNone() {
        assertThat(new Icon().location(), is(""));
    }

    @Test
    public void returnedUrlIsTheEmptyStringIfThereIsNoLocation() {
        assertThat(new Icon().url(), is(""));
    }

    @Test
    public void returnedUrlIsTheStaticOneFromThePluginWhenThereIsALocation() {
        WebResourceUrlProvider webResourceUrlProvider = mock(WebResourceUrlProvider.class);
        when(webResourceUrlProvider.getStaticPluginResourceUrl("moduleKey", "resourceName", UrlMode.AUTO)).thenReturn("expected-url");

        Icon icon = new Icon(webResourceUrlProvider, "location", "moduleKey", "resourceName");

        assertThat(icon.url(), is("expected-url"));
    }
}
