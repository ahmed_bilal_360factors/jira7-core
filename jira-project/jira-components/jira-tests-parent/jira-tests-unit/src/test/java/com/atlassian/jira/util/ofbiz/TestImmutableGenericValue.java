package com.atlassian.jira.util.ofbiz;

import com.atlassian.jira.ofbiz.FieldMap;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.ofbiz.core.entity.GenericDelegator;
import org.ofbiz.core.entity.GenericEntityException;
import org.ofbiz.core.entity.GenericValue;
import org.ofbiz.core.entity.model.ModelEntity;
import org.ofbiz.core.entity.model.ModelField;

import java.util.Collections;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

public class TestImmutableGenericValue {
    @Rule
    public ExpectedException expect = ExpectedException.none();

    private final ImmutableGenericValue gv = new ImmutableGenericValue(new GenericDelegator() {
        public ModelEntity getModelEntity(String entityName) {
            return new ModelEntity();
        }
    }, "name", Collections.emptyMap());

    @Test
    public final void testSetStringObjectBoolean() {
        expect.expect(UnsupportedOperationException.class);
        gv.set("name", new Object(), true);
    }

    @Test
    public final void testSetPKFieldsMapBoolean() {
        expect.expect(UnsupportedOperationException.class);
        gv.setPKFields(Collections.emptyMap(), true);
    }

    @Test
    public final void testSetFields() {
        expect.expect(UnsupportedOperationException.class);
        gv.setFields(Collections.emptyMap());
    }

    @Test
    public final void testRemoveObject() {
        expect.expect(UnsupportedOperationException.class);
        gv.remove("fred");
    }

    @Test
    public final void testPut() {
        expect.expect(UnsupportedOperationException.class);
        gv.put("name", new Object());
    }

    @Test
    public final void testStore() throws GenericEntityException {
        expect.expect(UnsupportedOperationException.class);
        gv.store();
    }

    @Test
    public final void testRemove() throws GenericEntityException {
        expect.expect(UnsupportedOperationException.class);
        gv.remove();
    }

    @Test
    public final void testRefresh() throws GenericEntityException {
        expect.expect(UnsupportedOperationException.class);
        gv.refresh();
    }

    @Test
    public final void testRemoveRelated() throws GenericEntityException {
        expect.expect(UnsupportedOperationException.class);
        gv.removeRelated("stuff");
    }

    @Test
    public final void testPutAllMap() {
        expect.expect(UnsupportedOperationException.class);
        gv.putAll(Collections.emptyMap());
    }

    @Test
    public final void testSetDelegator() {
        expect.expect(UnsupportedOperationException.class);
        gv.setDelegator(new GenericDelegator() {
        });
    }

    @Test
    public final void testSetString() {
        expect.expect(UnsupportedOperationException.class);
        gv.setString("name", "value");
    }

    @Test
    public final void testSetBytes() {
        expect.expect(UnsupportedOperationException.class);
        gv.setBytes("name", new byte[16]);
    }

    @Test
    public final void testClear() {
        expect.expect(UnsupportedOperationException.class);
        gv.clear();
    }

    @Test
    public void testConstructorWithMapValues() throws Exception {
        final ModelEntity modelEntity = new ModelEntity();
        modelEntity.addField(getModelField("key"));
        modelEntity.addField(getModelField("name"));
        modelEntity.addField(getModelField("id"));
        assertNotNull(modelEntity.getField("key"));
        assertNotNull(modelEntity.getField("name"));
        assertNotNull(modelEntity.getField("id"));
        assertNull(modelEntity.getField("description"));

        Map values = FieldMap.build("name", "the.name", "key", "the.key", "id", "the.id");
        GenericValue value = new ImmutableGenericValue(new GenericDelegator() {
            public ModelEntity getModelEntity(String entityName) {
                return modelEntity;
            }
        }, "name", values);

        assertEquals("the.id", value.get("id"));
        assertEquals("the.name", value.get("name"));
        assertEquals("the.key", value.get("key"));

        expect.expect(IllegalArgumentException.class);
        value.get("description");
    }

    private ModelField getModelField(String name) {
        return new ModelField(name, null, name, false, null);
    }
}
