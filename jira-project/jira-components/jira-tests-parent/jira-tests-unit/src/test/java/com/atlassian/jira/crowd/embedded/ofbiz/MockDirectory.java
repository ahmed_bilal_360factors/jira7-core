package com.atlassian.jira.crowd.embedded.ofbiz;

import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.DirectoryType;
import com.atlassian.crowd.embedded.api.OperationType;
import com.atlassian.crowd.embedded.impl.AbstractDelegatingEntityWithAttributes;

import java.util.Date;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class MockDirectory extends AbstractDelegatingEntityWithAttributes implements Directory {
    private final long id;
    private final String name;
    private final HashSet<OperationType> operationsAllowed;

    public MockDirectory(final long directoryId) {
        this(directoryId, null);
    }

    public MockDirectory(final long directoryId, final String name) {
        super(null);
        this.id = directoryId;
        this.name = name;
        this.operationsAllowed = new HashSet<>();
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public boolean isActive() {
        return true;
    }

    @Override
    public String getEncryptionType() {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public Map<String, String> getAttributes() {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public Set<OperationType> getAllowedOperations() {
        return operationsAllowed;
    }

    public void addAllowedOperation(OperationType operationTypeAllowed) {
        operationsAllowed.add(operationTypeAllowed);
    }

    @Override
    public String getDescription() {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public DirectoryType getType() {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public String getImplementationClass() {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public Date getCreatedDate() {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public Date getUpdatedDate() {
        throw new UnsupportedOperationException("Not implemented");
    }
}
