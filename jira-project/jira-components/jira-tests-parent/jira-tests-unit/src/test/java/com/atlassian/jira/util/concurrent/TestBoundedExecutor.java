package com.atlassian.jira.util.concurrent;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertArrayEquals;

public class TestBoundedExecutor {
    private ExecutorService executorService;
    private final CountDownLatch latch = new CountDownLatch(1);

    @Before
    public void setUp() {
        executorService = new MyThreadPoolExecutor();
    }

    @After
    public void tearDown() {
        if (executorService != null) {
            executorService.shutdownNow();
        }
    }

    @Test
    public void testShutdownAndWaitDoesNotInterruptRunnable() {
        final Boolean[] done = {false};

        BoundedExecutor exe = new BoundedExecutor(executorService, 1);
        exe.execute(() -> {
            try {
                //If shutdownAndWait() interrupts then this will throw InterruptedException and done will never be set
                latch.await();
                done[0] = true;
            } catch (InterruptedException e) {
                //Should not happen, bounded executor should not be interruptible
            }
        });

        assertArrayEquals(new Boolean[]{false}, done);
        exe.shutdownAndWait();
        assertArrayEquals(new Boolean[]{true}, done);
    }

    @Test
    public void testInterruptedThreadDoesNotInterruptShutdownAndWait() {
        final Boolean[] done = {false};

        BoundedExecutor exe = new BoundedExecutor(executorService, 1);
        exe.execute(() -> {
            try {
                //If shutdownAndWait() interrupts then this will throw InterruptedException and done will never be set
                latch.await();
                done[0] = true;
            } catch (InterruptedException e) {
                //Should not happen, bounded executor should not be interruptible
            }
        });

        assertArrayEquals(new Boolean[]{false}, done);

        try {
            Thread.currentThread().interrupt();
            exe.shutdownAndWait();

            assertArrayEquals(new Boolean[]{true}, done);
        } finally {
            Thread.interrupted();
        }
    }

    /**
     * This ThreadPoolExecutor hardly waits at all and releases our latch once it has allowing the test to
     * continue at an appropriate time for the test.
     */
    private class MyThreadPoolExecutor extends ThreadPoolExecutor {
        public MyThreadPoolExecutor() {
            super(1, 1, 0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<>());
        }

        @Override
        public boolean awaitTermination(final long timeout, final TimeUnit unit) throws InterruptedException {
            final boolean result = super.awaitTermination(1, TimeUnit.MILLISECONDS);
            latch.countDown();
            return result;
        }
    }
}
