package com.atlassian.jira.security;

import com.atlassian.cache.CacheManager;
import com.atlassian.jira.application.ApplicationRoleManager;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.security.IssueSecuritySchemeManager;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.mock.issue.MockIssue;
import com.atlassian.jira.mock.ofbiz.MockGenericValue;
import com.atlassian.jira.ofbiz.FieldMap;
import com.atlassian.jira.permission.GlobalPermissionType;
import com.atlassian.jira.permission.PermissionSchemeManager;
import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.security.plugin.GlobalPermissionTypesManager;
import com.atlassian.jira.security.plugin.ProjectPermissionKey;
import com.atlassian.jira.security.plugin.ProjectPermissionOverride;
import com.atlassian.jira.security.plugin.ProjectPermissionOverrideModuleDescriptor;
import com.atlassian.jira.security.plugin.ProjectPermissionTypesManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.util.RecoveryMode;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.user.util.UserUtil;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.event.PluginEventManager;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.ArgumentMatcher;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.ofbiz.core.entity.GenericValue;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.atlassian.jira.permission.ProjectPermissions.ASSIGN_ISSUES;
import static com.atlassian.jira.permission.ProjectPermissions.BROWSE_PROJECTS;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.argThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestDefaultPermissionManager {
    protected MockApplicationUser bob;
    protected Project project;
    protected Project project2;
    protected GenericValue scheme;
    protected MockIssue issue;

    @Mock
    @AvailableInContainer
    private PermissionSchemeManager permissionSchemeManager;
    @Mock
    @AvailableInContainer
    private ProjectManager projectManager;
    @Mock
    @AvailableInContainer
    private IssueSecuritySchemeManager issueSecuritySchemeManager;
    @Mock
    @AvailableInContainer
    private PluginAccessor pluginAccessor;
    @Mock
    @AvailableInContainer
    private PluginEventManager pluginEventManager;
    @Mock
    private GroupManager groupManager;
    @Mock
    @AvailableInContainer
    private UserManager userManager;

    @AvailableInContainer
    private GlobalPermissionManager globalPermissionManager = MockGlobalPermissionManager.withSystemGlobalPermissions();
    @Mock
    private GlobalPermissionTypesManager globalPermissionTypesManager;
    @Mock
    private UserUtil userUtil;
    @Mock
    private CacheManager cacheManager;
    @Mock
    private ProjectPermissionTypesManager projectPermissionTypesManager;
    @Mock
    private ProjectPermissionOverrideDescriptorCache projectPermissionOverrideDescriptorCache;
    @Mock
    private FeatureManager featureManager;

    @Rule
    public MockitoContainer initMockitoMocks = MockitoMocksInContainer.rule(this);
    @Rule
    public ExpectedException exception = ExpectedException.none();

    private DefaultPermissionManager permissionManager;

    @Mock
    private ApplicationRoleManager applicationRoleManager;

    @Mock
    private RecoveryMode recoveryMode;

    @Before
    public void setUp() throws Exception {
        when(globalPermissionTypesManager.getAll()).thenReturn(Lists.<GlobalPermissionType>newArrayList());
        permissionManager = new DefaultPermissionManager(projectPermissionTypesManager, projectPermissionOverrideDescriptorCache);

        bob = new MockApplicationUser("bob");

        scheme = new MockGenericValue("PermissionScheme", ImmutableMap.of("id", 10L, "name", "Test Scheme", "description", "test"));

        project = new MockProject(new MockGenericValue("Project", ImmutableMap.of("id", 2L, "lead", "paul")));
        project2 = new MockProject(new MockGenericValue("Project", ImmutableMap.of("id", 3L)));

        ApplicationUser bobApplicationUser = mock(ApplicationUser.class);
        when(userManager.getUserByKey("bob")).thenReturn(bobApplicationUser);
//        when(bobApplicationUser.getDirectoryUser()).thenReturn(bob);

        issue = new MockIssue(new MockGenericValue("Issue", ImmutableMap.of("key", "ABC-1", "project", 2L, "reporter", "bob", "assignee", "bob", "security", 15L))) {
            @Override
            public Project getProjectObject() {
                return project;
            }

            @Override
            public Long getId() {
                return 1l;
            }
        };
    }

    /**
     * Cannot call hasPermission (project) for a global permission
     */
    @Test
    public void testHasPermissionForProjectFailGlobalPermission() {
        DefaultPermissionManager permissionManager = new DefaultPermissionManager(projectPermissionTypesManager, projectPermissionOverrideDescriptorCache) {
            protected boolean isGlobalPermission(int permissionId) {
                return true;
            }
        };

        try {
            permissionManager.hasPermission(0, (Project) null, null, false);
            fail("Should throw IllegalArgument exception due to global permission id");
        } catch (IllegalArgumentException e) {
            //expected
            assertEquals(e.getMessage(), "PermissionType passed to this function must NOT be a global permission, 0 is global");
        }
    }

    /**
     * Cannot check permissions for null project
     */
    @Test
    public void testHasPermissionForProjectFailNullProject() {
        DefaultPermissionManager permissionManager = new DefaultPermissionManager(projectPermissionTypesManager, projectPermissionOverrideDescriptorCache) {
            protected boolean isGlobalPermission(int permissionId) {
                return false;
            }
        };

        try {
            permissionManager.hasPermission(0, (Project) null, (ApplicationUser) null, false);
            fail("Should throw IllegalArgument exception due to null Project");
        } catch (IllegalArgumentException e) {
            //expected
            assertEquals(e.getMessage(), "The Project argument and its backing generic value must not be null");
        }
    }

    /**
     * A project with a null backing GV is a serious state problem for the domain object - also it's required by
     * underlying methods
     */
    @Test
    public void testHasPermissionForProjectFailNullProjectBackingGV() {
        DefaultPermissionManager permissionManager = new DefaultPermissionManager(projectPermissionTypesManager, projectPermissionOverrideDescriptorCache) {
            protected boolean isGlobalPermission(int permissionId) {
                return false;
            }
        };

        Project project = new MockProject() {
            public GenericValue getGenericValue() {
                return null;
            }
        };

        try {
            permissionManager.hasPermission(0, project, null, false);
            fail("Should throw IllegalArgument exception due to null backing GV for Project");
        } catch (IllegalArgumentException e) {
            //expected
            assertEquals(e.getMessage(), "The Project argument and its backing generic value must not be null");
        }
    }

    /**
     * If issue object's GV is null, need to defer to the project object for permission check.
     */
    @Test
    public void testHasPermissionForIssueCreation() {
        final int testPermission = Permissions.CLOSE_ISSUE;

        DefaultPermissionManager permissionManager = new DefaultPermissionManager(projectPermissionTypesManager, projectPermissionOverrideDescriptorCache) {
            public boolean hasPermission(final int permissionsId, final Issue entity, final ApplicationUser user) {
                // verify arguments are called through correctly
                assertEquals(testPermission, permissionsId);
                assertEquals(issue, entity);
                assertNull(user);
                return true;
            }

            public boolean hasPermission(int permissionsId, Project project, ApplicationUser user, boolean issueCreation) {
                // verify arguments are called through correctly
                assertEquals(testPermission, permissionsId);
                assertEquals(project, project);
                assertNull(user);
                assertEquals(true, issueCreation);
                return true;
            }
        };

        assertTrue(permissionManager.hasPermission(testPermission, issue, (ApplicationUser) null));
        assertTrue(permissionManager.hasPermission(testPermission, issue, (ApplicationUser) null));
    }

    @Test
    public void testHasProjectWidePermission_NO_ISSUES() throws Exception {
        ProjectPermissionTypesManager projectPermissionTypesManager = new MockProjectPermissionTypesManager();
        DefaultPermissionManager permissionManager = new DefaultPermissionManager(projectPermissionTypesManager, null);
        final ApplicationUser user = new MockApplicationUser("johnno");

        when(permissionSchemeManager.hasProjectWidePermission(ProjectPermissions.TRANSITION_ISSUES, project, user, false))
                .thenReturn(ProjectWidePermission.NO_ISSUES);
        // If the core permission check denies, then no override needed
        final ProjectWidePermission projectWidePermission = permissionManager.hasProjectWidePermission(ProjectPermissions.TRANSITION_ISSUES, project, user);
        assertEquals(ProjectWidePermission.NO_ISSUES, projectWidePermission);
    }

    @Test
    public void testHasProjectWidePermission_ALL_ISSUES_NoOverride() throws Exception {
        ProjectPermissionTypesManager projectPermissionTypesManager = new MockProjectPermissionTypesManager();
        DefaultPermissionManager permissionManager = new DefaultPermissionManager(projectPermissionTypesManager, projectPermissionOverrideDescriptorCache);
        final ApplicationUser user = new MockApplicationUser("johnno");

        when(permissionSchemeManager.hasProjectWidePermission(ProjectPermissions.TRANSITION_ISSUES, project, user, false))
                .thenReturn(ProjectWidePermission.ALL_ISSUES);
        // Core permission = ALL_ISSUES and no override
        final ProjectWidePermission projectWidePermission = permissionManager.hasProjectWidePermission(ProjectPermissions.TRANSITION_ISSUES, project, user);
        assertEquals(ProjectWidePermission.ALL_ISSUES, projectWidePermission);
    }

    @Test
    public void testHasProjectWidePermission_ALL_ISSUES_OverrideDeny() throws Exception {
        ProjectPermissionTypesManager projectPermissionTypesManager = new MockProjectPermissionTypesManager();
        DefaultPermissionManager permissionManager = new DefaultPermissionManager(projectPermissionTypesManager, projectPermissionOverrideDescriptorCache);
        final ApplicationUser user = new MockApplicationUser("johnno");

        when(permissionSchemeManager.hasProjectWidePermission(ProjectPermissions.TRANSITION_ISSUES, project, user, false))
                .thenReturn(ProjectWidePermission.ALL_ISSUES);
        ArrayList<ProjectPermissionOverrideModuleDescriptor> moduleDescriptors = Lists.newArrayList(permissionOverrideModuleDescriptor(projectPermissionOverride(ProjectPermissionOverride.Decision.DENY)));
        when(projectPermissionOverrideDescriptorCache.getProjectPermissionOverrideDescriptors()).thenReturn(moduleDescriptors);
        // Core permission = ALL_ISSUES but override = DENY
        final ProjectWidePermission projectWidePermission = permissionManager.hasProjectWidePermission(ProjectPermissions.TRANSITION_ISSUES, project, user);
        assertEquals(ProjectWidePermission.NO_ISSUES, projectWidePermission);
    }

    @Test
    public void testHasProjectWidePermission_ISSUE_SPECIFIC_NoOverride() throws Exception {
        ProjectPermissionTypesManager projectPermissionTypesManager = new MockProjectPermissionTypesManager();
        DefaultPermissionManager permissionManager = new DefaultPermissionManager(projectPermissionTypesManager, projectPermissionOverrideDescriptorCache);
        final ApplicationUser user = new MockApplicationUser("johnno");

        when(permissionSchemeManager.hasProjectWidePermission(ProjectPermissions.TRANSITION_ISSUES, project, user, false))
                .thenReturn(ProjectWidePermission.ISSUE_SPECIFIC);
        // Core permission = ISSUE_SPECIFIC and no override
        final ProjectWidePermission projectWidePermission = permissionManager.hasProjectWidePermission(ProjectPermissions.TRANSITION_ISSUES, project, user);
        assertEquals(ProjectWidePermission.ISSUE_SPECIFIC, projectWidePermission);
    }

    @Test
    public void testHasProjectWidePermission_ISSUE_SPECIFIC_OverrideDeny() throws Exception {
        ProjectPermissionTypesManager projectPermissionTypesManager = new MockProjectPermissionTypesManager();
        DefaultPermissionManager permissionManager = new DefaultPermissionManager(projectPermissionTypesManager, projectPermissionOverrideDescriptorCache);
        final ApplicationUser user = new MockApplicationUser("johnno");

        when(permissionSchemeManager.hasProjectWidePermission(ProjectPermissions.TRANSITION_ISSUES, project, user, false))
                .thenReturn(ProjectWidePermission.ISSUE_SPECIFIC);
        ArrayList<ProjectPermissionOverrideModuleDescriptor> moduleDescriptors = Lists.newArrayList(permissionOverrideModuleDescriptor(projectPermissionOverride(ProjectPermissionOverride.Decision.DENY)));
        when(projectPermissionOverrideDescriptorCache.getProjectPermissionOverrideDescriptors()).thenReturn(moduleDescriptors);
        // Core permission = ISSUE_SPECIFIC but override = DENY
        final ProjectWidePermission projectWidePermission = permissionManager.hasProjectWidePermission(ProjectPermissions.TRANSITION_ISSUES, project, user);
        assertEquals(ProjectWidePermission.NO_ISSUES, projectWidePermission);
    }

    @Test
    public void testHasProjectWidePermission_Anonymous_NoPermission() throws Exception {
        ProjectPermissionTypesManager projectPermissionTypesManager = new MockProjectPermissionTypesManager();
        DefaultPermissionManager permissionManager = new DefaultPermissionManager(projectPermissionTypesManager, projectPermissionOverrideDescriptorCache);

        when(permissionSchemeManager.hasSchemePermission(ProjectPermissions.TRANSITION_ISSUES, project))
                .thenReturn(false);
        // Core permission = false
        final ProjectWidePermission projectWidePermission = permissionManager.hasProjectWidePermission(ProjectPermissions.TRANSITION_ISSUES, project, null);
        assertEquals(ProjectWidePermission.NO_ISSUES, projectWidePermission);
    }

    @Test
    public void testHasProjectWidePermission_Anonymous_HasPermission() throws Exception {
        ProjectPermissionTypesManager projectPermissionTypesManager = new MockProjectPermissionTypesManager();
        DefaultPermissionManager permissionManager = new DefaultPermissionManager(projectPermissionTypesManager, projectPermissionOverrideDescriptorCache);

        when(permissionSchemeManager.hasSchemePermission(ProjectPermissions.TRANSITION_ISSUES, project))
                .thenReturn(true);
        // Core permission = true and no override
        final ProjectWidePermission projectWidePermission = permissionManager.hasProjectWidePermission(ProjectPermissions.TRANSITION_ISSUES, project, null);
        assertEquals(ProjectWidePermission.ALL_ISSUES, projectWidePermission);
    }

    @Test
    public void testHasProjectWidePermission_Anonymous_HasPermission_OverrideDeny() throws Exception {
        ProjectPermissionTypesManager projectPermissionTypesManager = new MockProjectPermissionTypesManager();
        DefaultPermissionManager permissionManager = new DefaultPermissionManager(projectPermissionTypesManager, projectPermissionOverrideDescriptorCache);

        when(permissionSchemeManager.hasSchemePermission(ProjectPermissions.TRANSITION_ISSUES, project))
                .thenReturn(true);
        ArrayList<ProjectPermissionOverrideModuleDescriptor> moduleDescriptors = Lists.newArrayList(permissionOverrideModuleDescriptor(projectPermissionOverride(ProjectPermissionOverride.Decision.DENY)));
        when(projectPermissionOverrideDescriptorCache.getProjectPermissionOverrideDescriptors()).thenReturn(moduleDescriptors);
        // Core permission = true but override = deny
        final ProjectWidePermission projectWidePermission = permissionManager.hasProjectWidePermission(ProjectPermissions.TRANSITION_ISSUES, project, null);
        assertEquals(ProjectWidePermission.NO_ISSUES, projectWidePermission);
    }

    private ProjectPermissionOverride projectPermissionOverride(final ProjectPermissionOverride.Decision decision) {
        return new ProjectPermissionOverride() {
            @Override
            public Decision hasPermission(final ProjectPermissionKey projectPermissionKey, final Project project, @Nullable final ApplicationUser applicationUser) {
                return decision;
            }

            @Override
            public Reason getReason(final ProjectPermissionKey projectPermissionKey, final Project project, @Nullable final ApplicationUser applicationUser) {
                throw new UnsupportedOperationException("Not implemented");
            }
        };
    }

    @Test
    public void testProtectedMethodHasProjectPermissionAnonymous() {
        MockGenericValue projectGv = new MockGenericValue("Project", 1l);
        MockProject project = new MockProject(projectGv);

        when(permissionSchemeManager.hasSchemePermission(ASSIGN_ISSUES, project)).thenReturn(true);
        when(projectPermissionTypesManager.exists(ASSIGN_ISSUES)).thenReturn(true);

        assertTrue(permissionManager.hasPermission(Permissions.ASSIGN_ISSUE, project, (ApplicationUser) null));
        assertTrue(permissionManager.hasPermission(ASSIGN_ISSUES, project, (ApplicationUser) null));
    }

    @Test
    public void testHasPermission_ProjectUser_True() {
        MockProjectPermissionTypesManager mockProjectPermissionTypesManager = new MockProjectPermissionTypesManager();
        permissionManager = new DefaultPermissionManager(mockProjectPermissionTypesManager, null);
        when(permissionSchemeManager.hasSchemePermission(BROWSE_PROJECTS, project, bob, false)).thenReturn(true);

        // Test with key
        assertTrue(permissionManager.hasPermission(BROWSE_PROJECTS, project, bob, false));
        // Test with int (legacy)
        assertTrue(permissionManager.hasPermission(Permissions.BROWSE, project, bob, false));
    }

    @Test
    public void testHasPermission_ProjectUser_False() {
        MockProjectPermissionTypesManager mockProjectPermissionTypesManager = new MockProjectPermissionTypesManager();
        permissionManager = new DefaultPermissionManager(mockProjectPermissionTypesManager, null);
        when(permissionSchemeManager.hasSchemePermission(BROWSE_PROJECTS, project, bob, false)).thenReturn(false);

        // Test with key
        assertFalse(permissionManager.hasPermission(BROWSE_PROJECTS, project, bob, false));
        // Test with int (legacy)
        assertFalse(permissionManager.hasPermission(Permissions.BROWSE, project, bob, false));
    }

    @Test
    public void testHasProjectsNoProjectsExist() throws Exception {
        DefaultPermissionManager permissionManager = new DefaultPermissionManager(projectPermissionTypesManager, projectPermissionOverrideDescriptorCache) {
            protected boolean isGlobalPermission(int permissionId) {
                return false;
            }
        };

        assertFalse(permissionManager.hasProjects(0, bob));
    }

    @Test
    public void testGetProjectsUserHasNoVisibleProjects() throws Exception {
        DefaultPermissionManager permissionManager = new DefaultPermissionManager(projectPermissionTypesManager, projectPermissionOverrideDescriptorCache) {
            protected boolean isGlobalPermission(int permissionId) {
                return false;
            }

        };
        assertFalse(permissionManager.hasProjects(0, bob));
    }

    @Test
    public void testGetProjectsNoProjectsExist() throws Exception {
        DefaultPermissionManager permissionManager = new DefaultPermissionManager(projectPermissionTypesManager, projectPermissionOverrideDescriptorCache) {
            @Override
            public boolean hasPermission(@Nonnull final ProjectPermissionKey permissionsKey, @Nonnull final Project project, final ApplicationUser user) {
                return true;
            }
        };

        assertTrue(permissionManager.getProjects(Permissions.BROWSE, bob).isEmpty());
        assertTrue(permissionManager.getProjects(BROWSE_PROJECTS, bob).isEmpty());
    }

    @Test
    public void testGetProjectObjectsProjectsExist() throws Exception {
        MockGenericValue mockProjectGV = new MockGenericValue("Project", FieldMap.build("name", "proj", "key", "key", "id", 1000L));
        final Project project = new MockProject(mockProjectGV);
        final List<Project> projects = Collections.singletonList(project);

        when(projectManager.getProjectObjects()).thenReturn(projects);
        when(projectPermissionTypesManager.exists(BROWSE_PROJECTS)).thenReturn(true);

        DefaultPermissionManager permissionManager = new DefaultPermissionManager(projectPermissionTypesManager, projectPermissionOverrideDescriptorCache) {
            @Override
            public boolean hasPermission(@Nonnull final ProjectPermissionKey permissionsKey, @Nonnull final Project project, final ApplicationUser user) {
                return true;
            }
        };

        assertEquals(projects, permissionManager.getProjects(Permissions.BROWSE, bob));
        assertEquals(projects, permissionManager.getProjects(BROWSE_PROJECTS, bob));
    }

    @Test
    public void shouldNotAllowToCheckPermissionAgainstEntityWhenItsGlobal() {
        exception.expect(IllegalArgumentException.class);
        permissionManager.hasPermission(Permissions.ADMINISTER, (Project) null, bob, false);
    }

    @Test
    public void shouldCheckPermissionsAgainstProjectWhenProjectGivenLegacy() {
        when(projectPermissionTypesManager.exists(BROWSE_PROJECTS)).thenReturn(true);

        permissionManager.hasPermission(Permissions.BROWSE, project, bob, true);

        verify(permissionSchemeManager).hasSchemePermission(BROWSE_PROJECTS, project, bob, true);
    }

    @Test
    public void shouldCheckPermissionsAgainstProjectWhenProjectGiven() {
        when(projectPermissionTypesManager.exists(BROWSE_PROJECTS)).thenReturn(true);

        permissionManager.hasPermission(BROWSE_PROJECTS, project, bob, true);

        verify(permissionSchemeManager).hasSchemePermission(BROWSE_PROJECTS, project, bob, true);
    }

    @Test
    public void shouldCheckPermissionAgainstProjectsIssueWhenIssueGivenLegacy() {
        when(projectPermissionTypesManager.exists(BROWSE_PROJECTS)).thenReturn(true);

        permissionManager.hasPermission(Permissions.BROWSE, issue, bob);

        verify(permissionSchemeManager).hasSchemePermission(BROWSE_PROJECTS, project, bob, false);
        verify(permissionSchemeManager, never()).hasSchemePermission(BROWSE_PROJECTS, issue, bob, false);
    }

    @Test
    public void shouldCheckPermissionAgainstProjectsIssueWhenIssueGiven() {
        when(projectPermissionTypesManager.exists(BROWSE_PROJECTS)).thenReturn(true);

        permissionManager.hasPermission(BROWSE_PROJECTS, issue, bob);

        verify(permissionSchemeManager).hasSchemePermission(BROWSE_PROJECTS, project, bob, false);
        verify(permissionSchemeManager, never()).hasSchemePermission(BROWSE_PROJECTS, issue, bob, false);
    }

    @Test
    public void shouldCheckPermissionAgainstIssueWhenIssueGivenAndProjectCheckWasSuccessfulLegacy() {
        when(projectPermissionTypesManager.exists(BROWSE_PROJECTS)).thenReturn(true);

        issue.setSecurityLevelId((long) Permissions.BROWSE);
        when(permissionSchemeManager.hasSchemePermission(BROWSE_PROJECTS, project, bob, false)).thenReturn(true);

        permissionManager.hasPermission(Permissions.BROWSE, issue, bob);

        verify(permissionSchemeManager).hasSchemePermission(BROWSE_PROJECTS, issue, bob, false);
    }

    @Test
    public void shouldCheckPermissionAgainstIssueWhenIssueGivenAndProjectCheckWasSuccessful() {
        when(projectPermissionTypesManager.exists(BROWSE_PROJECTS)).thenReturn(true);

        issue.setSecurityLevelId((long) Permissions.BROWSE);
        when(permissionSchemeManager.hasSchemePermission(BROWSE_PROJECTS, project, bob, false)).thenReturn(true);

        permissionManager.hasPermission(BROWSE_PROJECTS, issue, bob);

        verify(permissionSchemeManager).hasSchemePermission(BROWSE_PROJECTS, issue, bob, false);
    }

    @Test
    public void shouldCheckAgainstIssueSecurityWhenUserHasGeneralAccess() {
        issue.setSecurityLevelId(1L);
        when(permissionSchemeManager.hasSchemePermission(ProjectPermissions.ADD_COMMENTS, project, bob, false)).thenReturn(true);
        when(permissionSchemeManager.hasSchemePermission(ProjectPermissions.ADD_COMMENTS, issue, bob, false)).thenReturn(true);
        when(issueSecuritySchemeManager.hasSecurityLevelAccess(issue, bob)).thenReturn(true);
        when(projectPermissionTypesManager.exists(ProjectPermissions.ADD_COMMENTS)).thenReturn(true);

        assertTrue(permissionManager.hasPermission(Permissions.COMMENT_ISSUE, issue, bob));
        assertTrue(permissionManager.hasPermission(ProjectPermissions.ADD_COMMENTS, issue, bob));
    }

    @Test
    public void shouldNotAllowAccessWhenPermsAreGoodButUserIsNotActive() {
        bob.setActive(false);

        assertFalse(permissionManager.hasPermission(Permissions.BROWSE, issue, bob));
        assertFalse(permissionManager.hasPermission(BROWSE_PROJECTS, issue, bob));
    }

    @Test
    public void permissionOverriddenLegacy() {
        ApplicationUser user = new MockApplicationUser("user");
        Project project = new MockProject(1l);
        Issue issue = mock(Issue.class);
        when(issue.getProjectObject()).thenReturn(project);
        when(issue.getSecurityLevelId()).thenReturn(1L);
        when(permissionSchemeManager.hasSchemePermission(eq(ASSIGN_ISSUES), any(Issue.class), Mockito.any(ApplicationUser.class), anyBoolean())).thenReturn(true);
        when(permissionSchemeManager.hasSchemePermission(eq(ASSIGN_ISSUES), any(Project.class), Mockito.any(ApplicationUser.class), anyBoolean())).thenReturn(true);
        when(issueSecuritySchemeManager.hasSecurityLevelAccess(any(Issue.class), Mockito.any(ApplicationUser.class))).thenReturn(true);
        ProjectPermissionOverride projectPermissionOverride = mock(ProjectPermissionOverride.class);
        when(projectPermissionOverride.hasPermission(argThat(permissionKey(ASSIGN_ISSUES)), eq(project), eq(user))).thenReturn(ProjectPermissionOverride.Decision.DENY);
        ArrayList<ProjectPermissionOverrideModuleDescriptor> moduleDescriptors = Lists.newArrayList(permissionOverrideModuleDescriptor(projectPermissionOverride));
        when(projectPermissionOverrideDescriptorCache.getProjectPermissionOverrideDescriptors()).thenReturn(moduleDescriptors);
        when(projectPermissionTypesManager.exists(ASSIGN_ISSUES)).thenReturn(true);

        assertThat(permissionManager.hasPermission(Permissions.ASSIGN_ISSUE, issue, user), Matchers.is(false));
        verify(projectPermissionOverride).hasPermission(argThat(permissionKey(ASSIGN_ISSUES)), eq(project), eq(user));
    }

    @Test
    public void permissionOverridden() {
        ApplicationUser user = new MockApplicationUser("user");
        Project project = new MockProject(1l);
        Issue issue = mock(Issue.class);
        when(issue.getProjectObject()).thenReturn(project);
        when(issue.getSecurityLevelId()).thenReturn(1L);
        when(permissionSchemeManager.hasSchemePermission(eq(ASSIGN_ISSUES), any(Issue.class), Mockito.any(ApplicationUser.class), anyBoolean())).thenReturn(true);
        when(permissionSchemeManager.hasSchemePermission(eq(ASSIGN_ISSUES), any(Project.class), Mockito.any(ApplicationUser.class), anyBoolean())).thenReturn(true);
        when(issueSecuritySchemeManager.hasSecurityLevelAccess(any(Issue.class), Mockito.any(ApplicationUser.class))).thenReturn(true);
        ProjectPermissionOverride projectPermissionOverride = mock(ProjectPermissionOverride.class);
        when(projectPermissionOverride.hasPermission(argThat(permissionKey(ASSIGN_ISSUES)), eq(project), eq(user))).thenReturn(ProjectPermissionOverride.Decision.DENY);
        ArrayList<ProjectPermissionOverrideModuleDescriptor> moduleDescriptors = Lists.newArrayList(permissionOverrideModuleDescriptor(projectPermissionOverride));
        when(projectPermissionOverrideDescriptorCache.getProjectPermissionOverrideDescriptors()).thenReturn(moduleDescriptors);
        when(projectPermissionTypesManager.exists(ASSIGN_ISSUES)).thenReturn(true);

        assertThat(permissionManager.hasPermission(ASSIGN_ISSUES, issue, user), Matchers.is(false));
        verify(projectPermissionOverride).hasPermission(argThat(permissionKey(ASSIGN_ISSUES)), eq(project), eq(user));
    }

    @Test
    public void permissionOverrideExceptionHandledAndCorePermissionCheckReturned() {
        issue.setSecurityLevelId(1L);
        ApplicationUser user = new MockApplicationUser("user");
        ProjectPermissionOverride projectPermissionOverride = mock(ProjectPermissionOverride.class);
        doAnswer(new Answer() {
            @Override
            public Object answer(final InvocationOnMock invocationOnMock) throws Throwable {
                throw new RuntimeException("sorry mate");
            }
        }).when(projectPermissionOverride).hasPermission(argThat(permissionKey(Permissions.ASSIGN_ISSUE)), eq(project), eq(user));
        ArrayList<ProjectPermissionOverrideModuleDescriptor> moduleDescriptors = Lists.newArrayList(permissionOverrideModuleDescriptor(projectPermissionOverride));
        when(pluginAccessor.getEnabledModuleDescriptorsByClass(eq(ProjectPermissionOverrideModuleDescriptor.class))).thenReturn(moduleDescriptors);
        when(issueSecuritySchemeManager.hasSecurityLevelAccess(any(Issue.class), Mockito.any(ApplicationUser.class))).thenReturn(true);
        when(projectPermissionOverrideDescriptorCache.getProjectPermissionOverrideDescriptors()).thenReturn(moduleDescriptors);
        when(permissionSchemeManager.hasSchemePermission(eq(ASSIGN_ISSUES), any(Issue.class), Mockito.any(ApplicationUser.class), anyBoolean())).thenReturn(true);
        when(permissionSchemeManager.hasSchemePermission(eq(ASSIGN_ISSUES), any(Project.class), Mockito.any(ApplicationUser.class), anyBoolean())).thenReturn(true);
        when(projectPermissionTypesManager.exists(ASSIGN_ISSUES)).thenReturn(true);

        assertThat(permissionManager.hasPermission(Permissions.ASSIGN_ISSUE, issue, user), Matchers.is(true));
        verify(projectPermissionOverride).hasPermission(argThat(permissionKey(ASSIGN_ISSUES)), eq(project), eq(user));
    }

    @Test
    public void permissionOverrideNotExecutedForBrowsePermissionLegacy() {
        ApplicationUser user = new MockApplicationUser("user");
        issue.setSecurityLevelId(1L);
        when(issueSecuritySchemeManager.hasSecurityLevelAccess(any(Issue.class), Mockito.any(ApplicationUser.class))).thenReturn(true);
        when(permissionSchemeManager.hasSchemePermission(eq(BROWSE_PROJECTS), any(Issue.class), Mockito.any(ApplicationUser.class), anyBoolean())).thenReturn(true);
        when(permissionSchemeManager.hasSchemePermission(eq(BROWSE_PROJECTS), any(Project.class), Mockito.any(ApplicationUser.class), anyBoolean())).thenReturn(true);
        when(projectPermissionTypesManager.exists(BROWSE_PROJECTS)).thenReturn(true);

        ProjectPermissionOverride projectPermissionOverride = mock(ProjectPermissionOverride.class);
        final ArrayList<ProjectPermissionOverrideModuleDescriptor> moduleDescriptors = Lists.newArrayList(permissionOverrideModuleDescriptor(projectPermissionOverride));
        when(pluginAccessor.getEnabledModuleDescriptorsByClass(eq(ProjectPermissionOverrideModuleDescriptor.class))).thenReturn(moduleDescriptors);

        assertThat(permissionManager.hasPermission(Permissions.BROWSE, issue, user), Matchers.is(true));
        verify(projectPermissionOverride, never()).hasPermission(argThat(permissionKey(BROWSE_PROJECTS)), eq(project), eq(user));
    }

    @Test
    public void permissionOverrideNotExecutedForBrowsePermission() {
        ApplicationUser user = new MockApplicationUser("user");
        issue.setSecurityLevelId(1L);
        when(issueSecuritySchemeManager.hasSecurityLevelAccess(any(Issue.class), Mockito.any(ApplicationUser.class))).thenReturn(true);
        when(permissionSchemeManager.hasSchemePermission(eq(BROWSE_PROJECTS), any(Issue.class), Mockito.any(ApplicationUser.class), anyBoolean())).thenReturn(true);
        when(permissionSchemeManager.hasSchemePermission(eq(BROWSE_PROJECTS), any(Project.class), Mockito.any(ApplicationUser.class), anyBoolean())).thenReturn(true);
        when(projectPermissionTypesManager.exists(BROWSE_PROJECTS)).thenReturn(true);

        ProjectPermissionOverride projectPermissionOverride = mock(ProjectPermissionOverride.class);
        final ArrayList<ProjectPermissionOverrideModuleDescriptor> moduleDescriptors = Lists.newArrayList(permissionOverrideModuleDescriptor(projectPermissionOverride));
        when(pluginAccessor.getEnabledModuleDescriptorsByClass(eq(ProjectPermissionOverrideModuleDescriptor.class))).thenReturn(moduleDescriptors);

        assertThat(permissionManager.hasPermission(BROWSE_PROJECTS, issue, user), Matchers.is(true));
        verify(projectPermissionOverride, never()).hasPermission(argThat(permissionKey(BROWSE_PROJECTS)), eq(project), eq(user));
    }

    @Test
    public void permissionNotOverriddenAllAbstainedLegacy() {
        issue.setSecurityLevelId(1L);
        ApplicationUser user = new MockApplicationUser("username");
        ProjectPermissionOverride projectPermissionOverride1 = mock(ProjectPermissionOverride.class);
        when(projectPermissionOverride1.hasPermission(any(ProjectPermissionKey.class), Mockito.any(Project.class), Mockito.any(ApplicationUser.class))).thenReturn(ProjectPermissionOverride.Decision.ABSTAIN);
        ProjectPermissionOverride projectPermissionOverride2 = mock(ProjectPermissionOverride.class);
        when(projectPermissionOverride2.hasPermission(any(ProjectPermissionKey.class), Mockito.any(Project.class), Mockito.any(ApplicationUser.class))).thenReturn(ProjectPermissionOverride.Decision.ABSTAIN);
        when(permissionSchemeManager.hasSchemePermission(eq(ASSIGN_ISSUES), any(Issue.class), Mockito.any(ApplicationUser.class), anyBoolean())).thenReturn(true);
        when(permissionSchemeManager.hasSchemePermission(eq(ASSIGN_ISSUES), any(Project.class), Mockito.any(ApplicationUser.class), anyBoolean())).thenReturn(true);
        when(issueSecuritySchemeManager.hasSecurityLevelAccess(any(Issue.class), Mockito.any(ApplicationUser.class))).thenReturn(true);
        when(projectPermissionTypesManager.exists(ASSIGN_ISSUES)).thenReturn(true);

        ArrayList<ProjectPermissionOverrideModuleDescriptor> moduleDescriptors = Lists.newArrayList(permissionOverrideModuleDescriptor(projectPermissionOverride1), permissionOverrideModuleDescriptor(projectPermissionOverride2));
        when(projectPermissionOverrideDescriptorCache.getProjectPermissionOverrideDescriptors()).thenReturn(moduleDescriptors);

        assertThat(permissionManager.hasPermission(Permissions.ASSIGN_ISSUE, issue, user), Matchers.is(true));
        verify(projectPermissionOverride1).hasPermission(any(ProjectPermissionKey.class), any(Project.class), any(ApplicationUser.class));
        verify(projectPermissionOverride2).hasPermission(any(ProjectPermissionKey.class), any(Project.class), any(ApplicationUser.class));
    }

    @Test
    public void permissionNotOverriddenAllAbstained() {
        issue.setSecurityLevelId(1L);
        ApplicationUser user = new MockApplicationUser("username");
        ProjectPermissionOverride projectPermissionOverride1 = mock(ProjectPermissionOverride.class);
        when(projectPermissionOverride1.hasPermission(any(ProjectPermissionKey.class), Mockito.any(Project.class), Mockito.any(ApplicationUser.class))).thenReturn(ProjectPermissionOverride.Decision.ABSTAIN);
        ProjectPermissionOverride projectPermissionOverride2 = mock(ProjectPermissionOverride.class);
        when(projectPermissionOverride2.hasPermission(any(ProjectPermissionKey.class), Mockito.any(Project.class), Mockito.any(ApplicationUser.class))).thenReturn(ProjectPermissionOverride.Decision.ABSTAIN);
        when(permissionSchemeManager.hasSchemePermission(eq(ASSIGN_ISSUES), any(Issue.class), Mockito.any(ApplicationUser.class), anyBoolean())).thenReturn(true);
        when(permissionSchemeManager.hasSchemePermission(eq(ASSIGN_ISSUES), any(Project.class), Mockito.any(ApplicationUser.class), anyBoolean())).thenReturn(true);
        when(issueSecuritySchemeManager.hasSecurityLevelAccess(any(Issue.class), Mockito.any(ApplicationUser.class))).thenReturn(true);
        when(projectPermissionTypesManager.exists(ASSIGN_ISSUES)).thenReturn(true);

        ArrayList<ProjectPermissionOverrideModuleDescriptor> moduleDescriptors = Lists.newArrayList(permissionOverrideModuleDescriptor(projectPermissionOverride1), permissionOverrideModuleDescriptor(projectPermissionOverride2));
        when(projectPermissionOverrideDescriptorCache.getProjectPermissionOverrideDescriptors()).thenReturn(moduleDescriptors);

        assertThat(permissionManager.hasPermission(ASSIGN_ISSUES, issue, user), Matchers.is(true));
        verify(projectPermissionOverride1).hasPermission(any(ProjectPermissionKey.class), any(Project.class), any(ApplicationUser.class));
        verify(projectPermissionOverride2).hasPermission(any(ProjectPermissionKey.class), any(Project.class), any(ApplicationUser.class));
    }

    @Test
    public void permissionOverridingNotExecutedForPermissionDeniedLegacy() {
        ApplicationUser user = new MockApplicationUser("username");
        Project project = new MockProject(1l);
        Issue issue = mock(Issue.class);
        when(issue.getProjectObject()).thenReturn(project);
        ProjectPermissionOverride projectPermissionOverride = mock(ProjectPermissionOverride.class);
        when(projectPermissionOverride.hasPermission(any(ProjectPermissionKey.class), Mockito.any(Project.class), Mockito.any(ApplicationUser.class))).thenReturn(ProjectPermissionOverride.Decision.ABSTAIN);
        ArrayList<ProjectPermissionOverrideModuleDescriptor> moduleDescriptors = Lists.newArrayList(permissionOverrideModuleDescriptor(projectPermissionOverride));
        when(pluginAccessor.getEnabledModuleDescriptorsByClass(eq(ProjectPermissionOverrideModuleDescriptor.class))).thenReturn(moduleDescriptors);
        when(permissionSchemeManager.hasSchemePermission(eq(ASSIGN_ISSUES), any(Issue.class), Mockito.any(ApplicationUser.class), anyBoolean())).thenReturn(false);

        assertThat(permissionManager.hasPermission(Permissions.ASSIGN_ISSUE, issue, user), Matchers.is(false));
        verify(projectPermissionOverride, never()).hasPermission(any(ProjectPermissionKey.class), eq(project), eq(user));
    }

    @Test
    public void permissionOverridingNotExecutedForPermissionDenied() {
        ApplicationUser user = new MockApplicationUser("username");
        Project project = new MockProject(1l);
        Issue issue = mock(Issue.class);
        when(issue.getProjectObject()).thenReturn(project);
        ProjectPermissionOverride projectPermissionOverride = mock(ProjectPermissionOverride.class);
        when(projectPermissionOverride.hasPermission(any(ProjectPermissionKey.class), Mockito.any(Project.class), Mockito.any(ApplicationUser.class))).thenReturn(ProjectPermissionOverride.Decision.ABSTAIN);
        ArrayList<ProjectPermissionOverrideModuleDescriptor> moduleDescriptors = Lists.newArrayList(permissionOverrideModuleDescriptor(projectPermissionOverride));
        when(pluginAccessor.getEnabledModuleDescriptorsByClass(eq(ProjectPermissionOverrideModuleDescriptor.class))).thenReturn(moduleDescriptors);

        assertThat(permissionManager.hasPermission(ASSIGN_ISSUES, issue, user), Matchers.is(false));
        verify(projectPermissionOverride, never()).hasPermission(any(ProjectPermissionKey.class), eq(project), eq(user));
    }

    @Test
    public void permissionManagerForNullUserShouldDelegateToGlobal() throws Exception {
        final DefaultPermissionManager pm = new DefaultPermissionManager(projectPermissionTypesManager, projectPermissionOverrideDescriptorCache);

        // test admin permission with no scheme
        pm.hasPermission(Permissions.ADMINISTER, (ApplicationUser) null);
        Mockito.verify(globalPermissionManager).hasPermission(Permissions.ADMINISTER);

        // test group filter permission with no scheme
        pm.hasPermission(Permissions.MANAGE_GROUP_FILTER_SUBSCRIPTIONS, (ApplicationUser) null);
        Mockito.verify(globalPermissionManager).hasPermission(Permissions.MANAGE_GROUP_FILTER_SUBSCRIPTIONS);

        // test group filter permission with no scheme
        pm.hasPermission(Permissions.CREATE_SHARED_OBJECTS, (ApplicationUser) null);
        Mockito.verify(globalPermissionManager).hasPermission(Permissions.CREATE_SHARED_OBJECTS);

    }

    private static ProjectPermissionOverrideModuleDescriptor permissionOverrideModuleDescriptor(final ProjectPermissionOverride projectPermissionOverride) {
        ProjectPermissionOverrideModuleDescriptor projectPermissionOverrideModuleDescriptor = mock(ProjectPermissionOverrideModuleDescriptor.class);
        when(projectPermissionOverrideModuleDescriptor.getModule()).thenReturn(projectPermissionOverride);
        return projectPermissionOverrideModuleDescriptor;
    }

    private Matcher<ProjectPermissionKey> permissionKey(final int permissionid) {
        return new ArgumentMatcher<ProjectPermissionKey>() {

            @Override
            public boolean matches(final Object argument) {
                return new ProjectPermissionKey(permissionid).equals(argument);
            }
        };
    }

    private Matcher<ProjectPermissionKey> permissionKey(final ProjectPermissionKey permissionKey) {
        return new ArgumentMatcher<ProjectPermissionKey>() {

            @Override
            public boolean matches(final Object argument) {
                return permissionKey.equals(argument);
            }
        };
    }
}
