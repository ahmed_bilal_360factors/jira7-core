package com.atlassian.jira.web.bean;

import com.atlassian.jira.bulkedit.operation.BulkMigrateOperation;
import com.atlassian.jira.bulkedit.operation.BulkMoveOperation;
import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.issue.issuetype.MockIssueType;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.mock.issue.MockIssue;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.google.common.collect.ImmutableList;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.mockito.Mock;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestMultiBulkMoveBeanImpl {

    private MultiBulkMoveBean multiBulkMoveBean;

    @Mock
    @AvailableInContainer
    private IssueManager issueManager;

    @Mock
    @AvailableInContainer
    private ProjectManager projectManager;

    @Mock
    @AvailableInContainer
    private ConstantsManager constantsManager;

    @Rule
    public final RuleChain mockitoMocks = MockitoMocksInContainer.forTest(this);

    @Before
    public void dataSetUp() throws Exception {
        this.multiBulkMoveBean = new MultiBulkMoveBeanImpl(BulkMoveOperation.NAME, issueManager);
    }

    /**
     * when issues are converted from issue to subtask then parent needs to be selected. when two or more issues have
     * the same destination project and issueType then they are merged to one destination. the problem here is that their
     * parent may be different and they should be grouped also by parent (if present)
     */
    @Test
    public void issueToSubissueWithSameProjectAndIssueTypeAndDifferentParentShouldProduceDifferentTargetBulkEditBeans() {
        this.multiBulkMoveBean = new MultiBulkMoveBeanImpl(BulkMigrateOperation.OPERATION_NAME, issueManager);

        List<MutableIssue> issues = new ArrayList<>();

        Long projectId = 1l;
        int id = 0;
        MutableIssue parent1 = new MockIssue(id, "parent-" + (id++));
        MutableIssue parent2 = new MockIssue(id, "parent-" + (id++));
        MutableIssue issue1 = new MockIssue(id, "key-" + (id++));
        MutableIssue issue2 = new MockIssue(id, "key-" + (id++));
        parent1.setProjectId(projectId);
        parent2.setProjectId(projectId);
        issue1.setProjectId(projectId);
        issue2.setProjectId(projectId);
        long issueTypeId = 1l;
        parent1.setIssueType(new MockIssueType(issueTypeId, "issueType-" + (issueTypeId++)));
        parent2.setIssueType(new MockIssueType(issueTypeId, "issueType-" + (issueTypeId++)));
        issue1.setIssueType(new MockIssueType(issueTypeId, "issueType-" + (issueTypeId++)));
        issue2.setIssueType(new MockIssueType(issueTypeId, "issueType-" + (issueTypeId++)));

        Long targetProjectId = 123l;
        Project targetProject = new MockProject(targetProjectId);
        IssueType targetIssueType = new MockIssueType(issueTypeId, "targetIssueType-" + (issueTypeId++));

        issues.add(issue1);
        issues.add(issue2);
        this.multiBulkMoveBean.initFromIssues(issues, null);

        int i = 0;
        for (Object value : multiBulkMoveBean.getBulkEditBeans().values()) {
            BulkEditBean bulkEditBean = (BulkEditBean) value;
            bulkEditBean.setTargetIssueTypeId(targetIssueType.getId());
            //distribute between 2 parents
            if (++i % 2 == 1) {
                bulkEditBean.setParentIssueKey(parent1.getKey());
            } else {
                bulkEditBean.setParentIssueKey(parent2.getKey());
            }
        }

        //ensure that there are 2 bulk edit beans constructed
        assertThat("There should be 2 bulk edit beans because there are 2 different (project, issueType) records", this.multiBulkMoveBean.getBulkEditBeans().size(), Matchers.is(2));

        when(projectManager.getProjectObj(targetProject.getId())).thenReturn(targetProject);
        when(constantsManager.getIssueTypeObject(targetIssueType.getId())).thenReturn(targetIssueType);

        when(issueManager.getIssueObject(parent1.getKey())).thenReturn(parent1);
        when(issueManager.getIssueObject(parent2.getKey())).thenReturn(parent2);
        this.multiBulkMoveBean.remapBulkEditBeansByTargetContext();

        assertThat("There should be 2 bulk edit beans because there are 2 different (project, issueType, project) records", this.multiBulkMoveBean.getBulkEditBeans().size(), Matchers.is(2));
    }

    @Test
    public void bulkMigrateSubtasksWithTheSameProjectAndIssueTypeButDifferentParentsShouldProduceDifferentBulkEditBeansInEntryStepAndSameBulkEditBeansInSubtaskStep() {
        this.multiBulkMoveBean = new MultiBulkMoveBeanImpl(BulkMigrateOperation.OPERATION_NAME, issueManager);
        Long projectId = 1l;
        int id = 0;
        MutableIssue parent1 = new MockIssue(id, "parent-" + (id++), false);
        MutableIssue parent2 = new MockIssue(id, "parent-" + (id++), false);

        MutableIssue subtask1 = new MockIssue(id, "key-" + (id++), true);
        subtask1.setProjectId(projectId);
        subtask1.setParentObject(parent1);
        MutableIssue subtask2 = new MockIssue(id, "key-" + (id++), true);
        subtask2.setProjectId(projectId);
        subtask2.setParentObject(parent1);
        MutableIssue subtask3 = new MockIssue(id, "key-" + (id++), true);
        subtask3.setProjectId(projectId);
        subtask3.setParentObject(parent2);
        MutableIssue subtask4 = new MockIssue(id, "key-" + (id++), true);
        subtask4.setProjectId(projectId);
        subtask4.setParentObject(parent2);
        IssueType subtaskIssueType = new MockIssueType(1l, "subtask");
        subtask1.setIssueType(subtaskIssueType);
        subtask2.setIssueType(subtaskIssueType);
        subtask3.setIssueType(subtaskIssueType);
        subtask4.setIssueType(subtaskIssueType);

        List<Issue> issues = ImmutableList.of(subtask1, subtask2, subtask3, subtask4);
        this.multiBulkMoveBean.initFromIssues(issues, null);
        assertThat("There should be 2 bulkEditBeans, one for each combination of project, issue type, parent issue", this.multiBulkMoveBean.getBulkEditBeans().size(), Matchers.is(2));

        this.multiBulkMoveBean.initFromIssues(issues, mock(BulkEditBean.class));
        assertThat("There should be 1 bulkEditBeans, one for each combination of project, issue type", this.multiBulkMoveBean.getBulkEditBeans().size(), Matchers.is(1));
    }

    @Test
    public void issueToSubissueWithSameProjectAndIssueTypeAndSameParentShouldProduceOneTargetBulkEditBeans() {
        List<MutableIssue> issues = new ArrayList<>();


        Long projectId = 1l;
        int id = 0;
        MutableIssue parent1 = new MockIssue(id, "parent-" + (id++));
        MutableIssue issue1 = new MockIssue(id, "key-" + (id++));
        MutableIssue issue2 = new MockIssue(id, "key-" + (id++));
        parent1.setProjectId(projectId);
        issue1.setProjectId(projectId);
        issue2.setProjectId(projectId);
        long issueTypeId = 1l;
        parent1.setIssueType(new MockIssueType(issueTypeId, "issueType-" + (issueTypeId++)));
        issue1.setIssueType(new MockIssueType(issueTypeId, "issueType-" + (issueTypeId++)));
        issue2.setIssueType(new MockIssueType(issueTypeId, "issueType-" + (issueTypeId++)));

        Long targetProjectId = 123l;
        Project targetProject = new MockProject(targetProjectId);
        IssueType targetIssueType = new MockIssueType(issueTypeId, "targetIssueType-" + (issueTypeId++));

        issues.add(issue1);
        issues.add(issue2);
        this.multiBulkMoveBean.initFromIssues(issues, null);

        for (Object value : multiBulkMoveBean.getBulkEditBeans().values()) {
            BulkEditBean bulkEditBean = (BulkEditBean) value;
            bulkEditBean.setTargetIssueTypeId(targetIssueType.getId());
            //distribute between 2 parents
            bulkEditBean.setParentIssueKey(parent1.getKey());
        }

        //ensure that there are 2 bulk edit beans constructed
        assertThat("There should be 2 bulk edit beans because there are 2 different (project, issueType) records", this.multiBulkMoveBean.getBulkEditBeans().size(), Matchers.is(2));

        when(projectManager.getProjectObj(targetProject.getId())).thenReturn(targetProject);
        when(constantsManager.getIssueTypeObject(targetIssueType.getId())).thenReturn(targetIssueType);
        when(issueManager.getIssueObject(parent1.getKey())).thenReturn(parent1);
        this.multiBulkMoveBean.remapBulkEditBeansByTargetContext();

        assertThat("There should be 1 bulk edit bean because there is 1 (project, issueType, project) records", this.multiBulkMoveBean.getBulkEditBeans().size(), Matchers.is(1));
    }

    @Test
    public void issueToSubissueWithSameProjectAndIssueTypeAndNoParentShouldProduceOneTargetBulkEditBeans() {
        List<MutableIssue> issues = new ArrayList<>();

        Long projectId = 1l;
        int id = 0;
        MutableIssue issue1 = new MockIssue(id, "key-" + (id++));
        MutableIssue issue2 = new MockIssue(id, "key-" + (id++));
        issue1.setProjectId(projectId);
        issue2.setProjectId(projectId);
        long issueTypeId = 1l;
        issue1.setIssueType(new MockIssueType(issueTypeId, "issueType-" + (issueTypeId++)));
        issue2.setIssueType(new MockIssueType(issueTypeId, "issueType-" + (issueTypeId++)));

        Long targetProjectId = 123l;
        Project targetProject = new MockProject(targetProjectId);
        IssueType targetIssueType = new MockIssueType(issueTypeId, "targetIssueType-" + (issueTypeId++));

        issues.add(issue1);
        issues.add(issue2);
        this.multiBulkMoveBean.initFromIssues(issues, null);

        for (Object value : multiBulkMoveBean.getBulkEditBeans().values()) {
            BulkEditBean bulkEditBean = (BulkEditBean) value;
            bulkEditBean.setTargetIssueTypeId(targetIssueType.getId());
        }

        //ensure that there are 2 bulk edit beans constructed
        assertThat("There should be 2 bulk edit beans because there are 2 different (project, issueType) records", this.multiBulkMoveBean.getBulkEditBeans().size(), Matchers.is(2));

        when(projectManager.getProjectObj(targetProject.getId())).thenReturn(targetProject);
        when(constantsManager.getIssueTypeObject(targetIssueType.getId())).thenReturn(targetIssueType);
        this.multiBulkMoveBean.remapBulkEditBeansByTargetContext();

        assertThat("There should be 1 bulk edit bean because there is 1 (project, issueType, project) records", this.multiBulkMoveBean.getBulkEditBeans().size(), Matchers.is(1));
    }

}
