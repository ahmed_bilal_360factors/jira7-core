package com.atlassian.jira.plugin.webfragment.conditions;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

public class TestIsSubTaskCondition {
    @Rule
    public MockitoRule mockito = MockitoJUnit.rule();
    @Mock
    private Issue issue;
    private IsSubTaskCondition condition;

    @Before
    public void setUp() throws Exception {
        condition = new IsSubTaskCondition();
    }

    @Test
    public void testFalse() {
        final ApplicationUser fred = new MockApplicationUser("fred");

        when(issue.isSubTask()).thenReturn(false);

        assertFalse(condition.shouldDisplay(fred, issue, null));
    }

    @Test
    public void testTrue() {
        when(issue.isSubTask()).thenReturn(true);

        assertTrue(condition.shouldDisplay(null, issue, null));
    }


}
