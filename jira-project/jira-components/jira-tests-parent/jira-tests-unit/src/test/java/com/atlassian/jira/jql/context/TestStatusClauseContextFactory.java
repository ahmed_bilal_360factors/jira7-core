package com.atlassian.jira.jql.context;

import com.atlassian.jira.issue.fields.config.manager.IssueTypeSchemeManager;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.issue.issuetype.MockIssueType;
import com.atlassian.jira.issue.status.MockStatus;
import com.atlassian.jira.issue.status.Status;
import com.atlassian.jira.jql.operand.JqlOperandResolver;
import com.atlassian.jira.jql.resolver.StatusResolver;
import com.atlassian.jira.jql.validator.MockJqlOperandResolver;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.collect.CollectionBuilder;
import com.atlassian.jira.util.collect.MapBuilder;
import com.atlassian.jira.workflow.JiraWorkflow;
import com.atlassian.jira.workflow.WorkflowManager;
import com.atlassian.jira.workflow.WorkflowSchemeManager;
import com.atlassian.query.clause.TerminalClause;
import com.atlassian.query.clause.TerminalClauseImpl;
import com.atlassian.query.operand.EmptyOperand;
import com.atlassian.query.operand.MultiValueOperand;
import com.atlassian.query.operand.Operand;
import com.atlassian.query.operand.SingleValueOperand;
import com.atlassian.query.operator.Operator;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import java.util.Collections;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

import static com.atlassian.jira.jql.operand.SimpleLiteralFactory.createLiteral;
import static com.atlassian.jira.permission.ProjectPermissions.BROWSE_PROJECTS;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestStatusClauseContextFactory {
    private static final String field = "status";

    @Rule
    public final MethodRule initMockito = MockitoJUnit.rule();

    @Mock
    private IssueTypeSchemeManager issueTypeSchemeManager;
    @Mock
    private WorkflowManager workflowManager;
    @Mock
    private PermissionManager permissionManager;
    @Mock
    private JqlOperandResolver jqlOperandResolver;
    @Mock
    private StatusResolver statusResolver;
    @Mock
    private WorkflowSchemeManager workflowSchemeManager;
    @Mock
    private JiraWorkflow workflow;
    @Mock
    private JiraWorkflow workflow1;
    @Mock
    private JiraWorkflow workflow2;
    @Mock
    private JiraWorkflow workflow3;
    @Mock
    private JiraWorkflow workflow4;
    private Project project1;
    private Project project2;
    private IssueType issueType1;
    private IssueType issueType2;

    private Status status1;
    private Status status2;

    @Before
    public void setUp() throws Exception {
        project1 = new MockProject(1);
        project2 = new MockProject(2);

        issueType1 = new MockIssueType("10", "name");
        issueType2 = new MockIssueType("20", "name");

        status1 = new MockStatus("72828", "Status1");
        status2 = new MockStatus("45454", "Status2");
    }

    @Test
    public void getClauseContextIsEmpty() throws Exception {
        final EmptyOperand operand = EmptyOperand.EMPTY;
        final TerminalClauseImpl clause = new TerminalClauseImpl("blah", Operator.IS, operand);

        final StatusClauseContextFactory factory = new StatusClauseContextFactory(MockJqlOperandResolver.createSimpleSupport(), statusResolver, workflowManager, permissionManager, issueTypeSchemeManager, workflowSchemeManager);
        final ClauseContext result = factory.getClauseContext(null, clause);
        final ClauseContext expectedResult = ClauseContextImpl.createGlobalClauseContext();

        assertEquals(expectedResult, result);
    }

    @Test
    public void getClauseContextIsNotEmpty() throws Exception {
        final EmptyOperand operand = EmptyOperand.EMPTY;
        final TerminalClauseImpl clause = new TerminalClauseImpl("blah", Operator.IS_NOT, operand);

        final StatusClauseContextFactory factory = new StatusClauseContextFactory(MockJqlOperandResolver.createSimpleSupport(), statusResolver, workflowManager, permissionManager, issueTypeSchemeManager, workflowSchemeManager);
        final ClauseContext result = factory.getClauseContext(null, clause);
        final ClauseContext expectedResult = ClauseContextImpl.createGlobalClauseContext();

        assertEquals(expectedResult, result);
    }

    @Test
    public void getClauseContextBadOperator() throws Exception {
        final SingleValueOperand operand = new SingleValueOperand("blah");
        final TerminalClauseImpl clause = new TerminalClauseImpl("blah", Operator.GREATER_THAN, operand);

        final StatusClauseContextFactory factory = new StatusClauseContextFactory(jqlOperandResolver, statusResolver, workflowManager, permissionManager, issueTypeSchemeManager, workflowSchemeManager);
        final ClauseContext result = factory.getClauseContext(null, clause);
        final ClauseContext expectedResult = ClauseContextImpl.createGlobalClauseContext();

        assertEquals(expectedResult, result);
    }

    //
    //Test when status is matched by no workflow.
    //
    @Test
    public void getClauseContextStatusNoneFound() throws Exception {
        // project1 -> {(null, workflow1[11])}

        when(permissionManager.getProjects(BROWSE_PROJECTS, null)).thenReturn(CollectionBuilder.newBuilder(project1).asList());
        when(workflowSchemeManager.getWorkflowMap(project1)).thenReturn(MapBuilder.<String, String>build(null, "workflow1"));
        when(workflowManager.getWorkflow("workflow1")).thenReturn(workflow);
        when(workflow.getLinkedStatusObjects()).thenReturn(Collections.<Status>singletonList(new MockStatus("11", "dontMatchMe")));

        final Set<String> statusIds = CollectionBuilder.newBuilder(status1.getId(), status1.getId()).asListOrderedSet();
        assertEquals(Collections.<ProjectIssueTypeContext>emptySet(), createForIds(statusIds).getContextFromStatusValues(null, new TerminalClauseImpl("blarg", Operator.EQUALS, "one"), true));
    }

    //
    // Test what happens when status matches are for all project, issue type combinations. It should return global
    //
    @Test
    public void getClauseContextStatusFoundGlobal() throws Exception {
        //project1 -> {(null, workflow1[10]), (34, workflow2[20])}
        //project1 -> {(null, workflow3[20])}

        when(permissionManager.getProjects(BROWSE_PROJECTS, null)).thenReturn(CollectionBuilder.newBuilder(project1, project2).asList());

        when(workflowSchemeManager.getWorkflowMap(project1))
                .thenReturn(MapBuilder.<String, String>newBuilder(null, "workflow1").add("34", "workflow2").toMutableMap());
        when(workflowManager.getWorkflow("workflow1")).thenReturn(workflow1);
        when(workflow1.getLinkedStatusObjects()).thenReturn(Collections.singletonList(status1));
        when(issueTypeSchemeManager.getIssueTypesForProject(project1)).thenReturn(CollectionBuilder.list(issueType1, issueType2));
        when(workflowManager.getWorkflow("workflow2")).thenReturn(workflow2);
        when(workflow2.getLinkedStatusObjects()).thenReturn(Collections.singletonList(status2));


        when(workflowSchemeManager.getWorkflowMap(project2))
                .thenReturn(MapBuilder.<String, String>newBuilder(null, "workflow3").toMutableMap());
        when(workflowManager.getWorkflow("workflow3")).thenReturn(workflow3);
        when(issueTypeSchemeManager.getIssueTypesForProject(project2)).thenReturn(CollectionBuilder.list(issueType2));
        when(workflow3.getLinkedStatusObjects()).thenReturn(Collections.singletonList(status2));

        final CollectionBuilder<ProjectIssueTypeContext> result = CollectionBuilder.<ProjectIssueTypeContext>newBuilder()
                .add(ProjectIssueTypeContextImpl.createGlobalContext());

        final Set<String> statusIds = CollectionBuilder.newBuilder(status1.getId(), status2.getId()).asListOrderedSet();
        assertEquals(result.asSet(), createForIds(statusIds).getContextFromStatusValues(null, new TerminalClauseImpl("blarg", Operator.EQUALS, "one"), true));
    }

    //
    // Test what happens when status matches are for all the issue types in one project.
    //
    @Test
    public void getClauseContextStatusFoundProject() throws Exception {
        //project1 -> {(null, workflow1[10]), (34, workflow2[20])}
        //project2 -> {(null, workflow3[484949])}

        when(permissionManager.getProjects(BROWSE_PROJECTS, null)).thenReturn(CollectionBuilder.newBuilder(project1, project2).asList());

        when(workflowSchemeManager.getWorkflowMap(project1))
                .thenReturn(MapBuilder.<String, String>newBuilder(null, "workflow1").add("34", "workflow2").toMutableMap());
        when(workflowManager.getWorkflow("workflow1")).thenReturn(workflow1);
        when(workflow1.getLinkedStatusObjects()).thenReturn(Collections.singletonList(status1));
        when(issueTypeSchemeManager.getIssueTypesForProject(project1)).thenReturn(CollectionBuilder.list(issueType1, issueType2));
        when(workflowManager.getWorkflow("workflow2")).thenReturn(workflow2);
        when(workflow2.getLinkedStatusObjects()).thenReturn(Collections.singletonList(status2));


        when(workflowSchemeManager.getWorkflowMap(project2))
                .thenReturn(MapBuilder.<String, String>newBuilder(null, "workflow3").toMutableMap());
        when(workflowManager.getWorkflow("workflow3")).thenReturn(workflow3);
        when(workflow3.getLinkedStatusObjects()).thenReturn(Collections.<Status>singletonList(new MockStatus("484949", "dontMatch")));

        final CollectionBuilder<ProjectIssueTypeContext> result = CollectionBuilder.<ProjectIssueTypeContext>newBuilder()
                .add(new ProjectIssueTypeContextImpl(new ProjectContextImpl(project1.getId()), AllIssueTypesContext.getInstance()));

        final Set<String> statusIds = CollectionBuilder.newBuilder(status1.getId(), status2.getId()).asListOrderedSet();
        assertEquals(result.asSet(), createForIds(statusIds).getContextFromStatusValues(null, new TerminalClauseImpl("blarg", Operator.EQUALS, "one"), true));
    }

    //
    // Test what happens when status matches are for project & issue type combinations.
    //
    @Test
    public void getClauseContextStatusFoundProjectIssueType() throws Exception {
        //project1 -> {(null, workflow1[10]), (34, workflow2[484949])}
        //project2 -> {(null, workflow3[484949])}

        when(permissionManager.getProjects(BROWSE_PROJECTS, null)).thenReturn(CollectionBuilder.newBuilder(project1, project2).asList());

        when(workflowSchemeManager.getWorkflowMap(project1))
                .thenReturn(MapBuilder.<String, String>newBuilder(null, "workflow1").add("34", "workflow2").toMutableMap());
        when(workflowManager.getWorkflow("workflow1")).thenReturn(workflow1);
        when(workflow1.getLinkedStatusObjects()).thenReturn(Collections.singletonList(status1));
        when(issueTypeSchemeManager.getIssueTypesForProject(project1)).thenReturn(CollectionBuilder.list(issueType1, issueType2));
        when(workflowManager.getWorkflow("workflow2")).thenReturn(workflow2);
        when(workflow2.getLinkedStatusObjects()).thenReturn(Collections.singletonList(status2));


        when(workflowSchemeManager.getWorkflowMap(project2))
                .thenReturn(MapBuilder.<String, String>newBuilder(null, "workflow3").toMutableMap());
        when(workflowManager.getWorkflow("workflow3")).thenReturn(workflow3);
        when(workflow3.getLinkedStatusObjects()).thenReturn(Collections.singletonList(status2));

        final CollectionBuilder<ProjectIssueTypeContext> result = CollectionBuilder.<ProjectIssueTypeContext>newBuilder()
                .add(new ProjectIssueTypeContextImpl(new ProjectContextImpl(project1.getId()), new IssueTypeContextImpl(issueType1.getId())))
                .add(new ProjectIssueTypeContextImpl(new ProjectContextImpl(project1.getId()), new IssueTypeContextImpl(issueType2.getId())));

        final Set<String> statusIds = CollectionBuilder.newBuilder(status1.getId()).asListOrderedSet();
        assertEquals(result.asSet(), createForIds(statusIds).getContextFromStatusValues(null, new TerminalClauseImpl("blarg", Operator.EQUALS, "one"), true));
    }

    //
    // Test a simple combination of issue contexts.
    //
    @Test
    public void getClauseContextStatusFoundCombination() throws Exception {
        final String issueType3Id = "45";

        //project1 -> {(null, workflow1[10]), (34, workflow2[10])}
        //project2 -> {(null, workflow3[89]), (45, workflow4[10])}

        when(permissionManager.getProjects(BROWSE_PROJECTS, null)).thenReturn(CollectionBuilder.newBuilder(project1, project2).asList());

        when(workflowSchemeManager.getWorkflowMap(project1))
                .thenReturn(MapBuilder.<String, String>newBuilder(null, "workflow1").add("34", "workflow2").toMutableMap());
        when(workflowManager.getWorkflow("workflow1")).thenReturn(workflow1);
        when(workflow1.getLinkedStatusObjects()).thenReturn(Collections.singletonList(status1));
        when(issueTypeSchemeManager.getIssueTypesForProject(project1)).thenReturn(CollectionBuilder.list(issueType1, issueType2));
        when(workflowManager.getWorkflow("workflow2")).thenReturn(workflow2);
        when(workflow2.getLinkedStatusObjects()).thenReturn(Collections.singletonList(status1));

        when(workflowSchemeManager.getWorkflowMap(project2))
                .thenReturn(MapBuilder.<String, String>newBuilder(null, "workflow3").add(issueType3Id, "workflow4").toMutableMap());
        when(workflowManager.getWorkflow("workflow3")).thenReturn(workflow3);
        when(workflow3.getLinkedStatusObjects()).thenReturn(Collections.<Status>singletonList(new MockStatus("89", "dontMatch")));
        when(workflowManager.getWorkflow("workflow4")).thenReturn(workflow4);
        when(workflow4.getLinkedStatusObjects()).thenReturn(Collections.singletonList(status1));

        final CollectionBuilder<ProjectIssueTypeContext> result = CollectionBuilder.<ProjectIssueTypeContext>newBuilder()
                .add(new ProjectIssueTypeContextImpl(new ProjectContextImpl(project1.getId()), AllIssueTypesContext.INSTANCE))
                .add(new ProjectIssueTypeContextImpl(new ProjectContextImpl(project2.getId()), new IssueTypeContextImpl(issueType3Id)));

        final Set<String> statusIds = CollectionBuilder.newBuilder(status1.getId(), status2.getId()).asListOrderedSet();
        assertEquals(result.asSet(), createForIds(statusIds).getContextFromStatusValues(null, new TerminalClauseImpl("blarg", Operator.EQUALS, "one"), true));
    }

    //
    // Test what happens when every workflow matches during negation.
    //
    @Test
    public void getClauseContextStatusNegationNoneFound() throws Exception {
        //project1 -> {(null, workflow1[10])}

        when(permissionManager.getProjects(BROWSE_PROJECTS, null)).thenReturn(CollectionBuilder.newBuilder(project1).asList());
        when(workflowSchemeManager.getWorkflowMap(project1)).thenReturn(MapBuilder.<String, String>build(null, "workflow1"));
        when(workflowManager.getWorkflow("workflow1")).thenReturn(workflow);
        when(workflow.getLinkedStatusObjects()).thenReturn(Collections.singletonList(status1));

        final Set<String> statusIds = CollectionBuilder.newBuilder(status1.getId(), status2.getId()).asListOrderedSet();
        assertEquals(Collections.<ProjectIssueTypeContext>emptySet(), createForIds(statusIds).getContextFromStatusValues(null, new TerminalClauseImpl("blarg", Operator.EQUALS, "one"), false));
    }

    //
    // Test what happens when one workflow matches during negation.
    //
    @Test
    public void getClauseContextStatusNegationOneFound() throws Exception {
        //project1 -> {(null, workflow1[10]), (434343, workflow2[11])}

        final String issueType3Id = "434343";

        when(permissionManager.getProjects(BROWSE_PROJECTS, null)).thenReturn(CollectionBuilder.newBuilder(project1).asList());
        when(workflowSchemeManager.getWorkflowMap(project1)).thenReturn(MapBuilder.newBuilder(issueType3Id, "workflow1").add(null, "workflow2").toMutableMap());
        when(workflowManager.getWorkflow("workflow1")).thenReturn(workflow);
        when(workflow.getLinkedStatusObjects()).thenReturn(Collections.<Status>singletonList(new MockStatus("11", "dontMatchMe")));
        when(workflowManager.getWorkflow("workflow2")).thenReturn(workflow2);
        when(workflow2.getLinkedStatusObjects()).thenReturn(Collections.singletonList(status2));

        final CollectionBuilder<ProjectIssueTypeContext> result = CollectionBuilder.<ProjectIssueTypeContext>newBuilder()
                .add(new ProjectIssueTypeContextImpl(new ProjectContextImpl(project1.getId()), new IssueTypeContextImpl(issueType3Id)));


        final Set<String> statusIds = CollectionBuilder.newBuilder(status1.getId(), status2.getId()).asListOrderedSet();
        assertEquals(result.asSet(), createForIds(statusIds).getContextFromStatusValues(null, new TerminalClauseImpl("blarg", Operator.EQUALS, "one"), false));
    }

    //
    // Test that caching works.
    //
    @Test
    public void getClauseContextStatusCaching() throws Exception {
        //project1 -> {(null, workflow1[10]), (1, workflow2[11])}
        //project2 -> {(null, workflow2[11]), (1, workflow1[10])}

        final MockStatus dontMatchStatus = new MockStatus("11", "dontMatchMe");

        when(permissionManager.getProjects(BROWSE_PROJECTS, null)).thenReturn(CollectionBuilder.newBuilder(project1, project2).asList());
        when(workflowSchemeManager.getWorkflowMap(project1)).thenReturn(MapBuilder.<String, String>newBuilder(null, "workflow1").add("1", "workflow2").toMutableMap());
        when(workflowManager.getWorkflow("workflow1")).thenReturn(workflow1);
        when(workflow1.getLinkedStatusObjects()).thenReturn(Collections.singletonList(status1));
        when(issueTypeSchemeManager.getIssueTypesForProject(project1)).thenReturn(Collections.singletonList(issueType1));
        when(workflowManager.getWorkflow("workflow2")).thenReturn(workflow2);
        when(workflow2.getLinkedStatusObjects()).thenReturn(Collections.<Status>singletonList(dontMatchStatus));

        //For the second project we don't expect any calls to managers because the result of the previous calculations should have been cached.
        when(workflowSchemeManager.getWorkflowMap(project2)).thenReturn(MapBuilder.<String, String>newBuilder(null, "workflow2").add("1", "workflow1").toMutableMap());

        final CollectionBuilder<ProjectIssueTypeContext> result = CollectionBuilder.<ProjectIssueTypeContext>newBuilder()
                .add(new ProjectIssueTypeContextImpl(new ProjectContextImpl(project1.getId()), new IssueTypeContextImpl(issueType1.getId())))
                .add(new ProjectIssueTypeContextImpl(new ProjectContextImpl(project2.getId()), new IssueTypeContextImpl("1")));

        final Set<String> statusIds = CollectionBuilder.newBuilder(status1.getId(), status2.getId()).asListOrderedSet();
        assertEquals(result.asSet(), createForIds(statusIds).getContextFromStatusValues(null, new TerminalClauseImpl("blarg", Operator.EQUALS, "one"), true));
    }

    //
    // Test matching "default workflow" returns the correct issue types.
    //
    @Test
    public void getClauseContext() throws Exception {
        //project1 -> {(null, workflow1[10]), (10, workflow2[11])}

        final MockStatus dontMatchStatus = new MockStatus("11", "dontMatchMe");

        when(permissionManager.getProjects(BROWSE_PROJECTS, null)).thenReturn(CollectionBuilder.newBuilder(project1).asList());
        when(workflowSchemeManager.getWorkflowMap(project1)).thenReturn(MapBuilder.<String, String>newBuilder(null, "workflow1").add("10", "workflow2").toMutableMap());
        when(workflowManager.getWorkflow("workflow1")).thenReturn(workflow1);
        when(workflow1.getLinkedStatusObjects()).thenReturn(Collections.singletonList(status1));
        when(issueTypeSchemeManager.getIssueTypesForProject(project1)).thenReturn(CollectionBuilder.list(issueType1, issueType2));
        when(workflowManager.getWorkflow("workflow2")).thenReturn(workflow2);
        when(workflow2.getLinkedStatusObjects()).thenReturn(Collections.<Status>singletonList(dontMatchStatus));

        final CollectionBuilder<ProjectIssueTypeContext> result = CollectionBuilder.<ProjectIssueTypeContext>newBuilder()
                .add(new ProjectIssueTypeContextImpl(new ProjectContextImpl(project1.getId()), new IssueTypeContextImpl(issueType2.getId())));

        final Set<String> statusIds = CollectionBuilder.newBuilder(status1.getId()).asListOrderedSet();
        assertEquals(result.asSet(), createForIds(statusIds).getContextFromStatusValues(null, new TerminalClauseImpl("blarg", Operator.EQUALS, "one"), true));
    }

    // Asserts fix for JRA-19026
    @Test
    public void getClauseContextStatusContainsNull() throws Exception {
        final Set<String> statusIds = CollectionBuilder.newBuilder("15").asListOrderedSet();
        final ClauseContext expectedResult = ClauseContextImpl.createGlobalClauseContext();
        _testGetClauseContextWithNullStatus(statusIds, Operator.EQUALS, expectedResult);
    }

    @Test
    public void getIdsNullLiterals() throws Exception {
        final Operand operand = new SingleValueOperand(10L);
        final TerminalClauseImpl clause = new TerminalClauseImpl(field, Operator.EQUALS, operand);

        when(jqlOperandResolver.getValues((ApplicationUser) null, operand, clause)).thenReturn(null);

        final StatusClauseContextFactory factory = new StatusClauseContextFactory(jqlOperandResolver, statusResolver, workflowManager, permissionManager, issueTypeSchemeManager, workflowSchemeManager);

        final Set<String> result = factory.getIds(null, clause);
        final Set<String> expectedResult = CollectionBuilder.<String>newBuilder().asListOrderedSet();

        assertEquals(expectedResult, result);
    }

    @Test
    public void getIdsLong() throws Exception {
        final Operand operand = new SingleValueOperand(10L);
        final TerminalClauseImpl clause = new TerminalClauseImpl(field, Operator.EQUALS, operand);

        when(jqlOperandResolver.getValues((ApplicationUser) null, operand, clause)).thenReturn(CollectionBuilder.newBuilder(createLiteral(10L)).asList());
        when(statusResolver.idExists(10L)).thenReturn(true);

        final StatusClauseContextFactory factory = new StatusClauseContextFactory(jqlOperandResolver, statusResolver, workflowManager, permissionManager, issueTypeSchemeManager, workflowSchemeManager);

        final Set<String> result = factory.getIds(null, clause);
        final Set<String> expectedResult = CollectionBuilder.newBuilder("10").asListOrderedSet();

        assertEquals(expectedResult, result);
    }

    @Test
    public void getIdsString() throws Exception {
        Operand operand = new SingleValueOperand("name");
        final TerminalClauseImpl clause = new TerminalClauseImpl(field, Operator.EQUALS, operand);

        when(jqlOperandResolver.getValues((ApplicationUser) null, operand, clause)).thenReturn(CollectionBuilder.newBuilder(createLiteral("name")).asList());
        when(statusResolver.getIdsFromName("name")).thenReturn(CollectionBuilder.newBuilder("10", "20").asList());

        final StatusClauseContextFactory factory = new StatusClauseContextFactory(jqlOperandResolver, statusResolver, workflowManager, permissionManager, issueTypeSchemeManager, workflowSchemeManager);

        final Set<String> result = factory.getIds(null, clause);
        final Set<String> expectedResult = CollectionBuilder.newBuilder("10", "20").asListOrderedSet();

        assertEquals(expectedResult, result);
    }

    @Test
    public void getIdsMixed() throws Exception {
        Operand operand = new MultiValueOperand(CollectionBuilder.<Operand>newBuilder(new SingleValueOperand("name"), new SingleValueOperand(30L), new SingleValueOperand(10L)).asList());
        final TerminalClauseImpl clause = new TerminalClauseImpl(field, Operator.EQUALS, operand);

        when(jqlOperandResolver.getValues((ApplicationUser) null, operand, clause))
                .thenReturn(CollectionBuilder.newBuilder(createLiteral("name"), createLiteral(10L), createLiteral(30L)).asList());

        when(statusResolver.getIdsFromName("name")).thenReturn(CollectionBuilder.newBuilder("10", "20").asList());
        when(statusResolver.idExists(10L)).thenReturn(true);
        when(statusResolver.idExists(30L)).thenReturn(true);

        final StatusClauseContextFactory factory = new StatusClauseContextFactory(jqlOperandResolver, statusResolver, workflowManager, permissionManager, issueTypeSchemeManager, workflowSchemeManager);

        final Set<String> result = factory.getIds(null, clause);
        final Set<String> expectedResult = CollectionBuilder.newBuilder("10", "20", "30").asListOrderedSet();

        assertEquals(expectedResult, result);
    }

    private void _testGetClauseContextWithNullStatus(final Set<String> statusIds, final Operator operator, final ClauseContext expectedResult) {
        final SingleValueOperand operand = new SingleValueOperand("blah");

        when(permissionManager.getProjects(BROWSE_PROJECTS, null))
                .thenReturn(CollectionBuilder.newBuilder(project1).asList());

        when(workflowSchemeManager.getWorkflowMap(project1))
                .thenReturn(MapBuilder.newBuilder(issueType1.getId(), "workflow1").toMutableMap());

        when(workflowManager.getWorkflow("workflow1"))
                .thenReturn(workflow1);

        when(workflow1.getLinkedStatusObjects())
                .thenReturn(Collections.<Status>singletonList(null));

        when(workflow1.getName()).thenReturn("Test Workflow");

        final AtomicBoolean called = new AtomicBoolean(false);
        final StatusClauseContextFactory factory = new StatusClauseContextFactory(jqlOperandResolver, statusResolver, workflowManager, permissionManager, issueTypeSchemeManager, workflowSchemeManager) {
            @Override
            Set<String> getIds(final ApplicationUser searcher, final TerminalClause clause) {
                called.set(true);
                return statusIds;
            }
        };

        final ClauseContext result = factory.getClauseContext(null, new TerminalClauseImpl("blah", operator, operand));

        assertTrue(called.get());
        assertEquals(expectedResult, result);
    }

    private StatusClauseContextFactory createForIds(final Set<String> ids) {
        return new StatusClauseContextFactory(jqlOperandResolver, statusResolver, workflowManager, permissionManager, issueTypeSchemeManager, workflowSchemeManager) {
            @Override
            Set<String> getIds(final ApplicationUser searcher, final TerminalClause clause) {
                return ids;
            }
        };
    }
}
