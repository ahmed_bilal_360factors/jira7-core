package com.atlassian.jira.application;

import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.jira.user.MockGroup;
import com.google.common.collect.ImmutableSet;
import org.junit.Test;

import javax.annotation.Nullable;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static com.atlassian.application.api.ApplicationKey.valueOf;
import static com.atlassian.extras.common.LicensePropertiesConstants.UNLIMITED_USERS;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertTrue;

public class IdApplicationRoleTest {
    @Test(expected = IllegalArgumentException.class)
    public void constructorFailsWithNullId() {
        new IdApplicationRole(null, groups(), groups(), 0, false);
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructorFailsWithNullGroups() {
        new IdApplicationRole(valueOf("jack"), null, groups(), 0, false);
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructorFailsWithNullInGroups() {
        new IdApplicationRole(valueOf("jack"), groups("abc", null), groups(), 0, false);
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructorFailsWithNullDefaultGroups() {
        new IdApplicationRole(valueOf("jack"), groups(), null, 0, false);
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructorFailsWithNullInDefaultGroups() {
        new IdApplicationRole(valueOf("jack"), groups("abc"), groups("abc", null), 0, false);
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructorFailsWithDefaultGroupNotInRole() {
        new IdApplicationRole(valueOf("jack"), groups("one", "two"), groups("three"), 0, false);
    }

    @Test
    public void equalsOnlyListensToId() {
        List<ApplicationRole> roles = Arrays.<ApplicationRole>asList(
                newRole("id", groups("one"), groups(), false),
                newRole("id", groups("one"), groups(), true),
                newRole("id", groups("two", "thee"), groups(), false),
                newRole("id", groups("two", "thee"), groups(), true),
                newRole("id", groups("two", "three"), groups("three"), false),
                newRole("id", groups("two", "three"), groups("three"), true),
                newRole("id", groups("two", "three"), groups("three", "two"), false),
                newRole("id", groups("two", "three"), groups("three", "two"), true));

        for (ApplicationRole left : roles) {
            for (ApplicationRole right : roles) {
                assertThat(left.equals(right), equalTo(true));
            }
        }

        List<ApplicationRole> otherRoles = Arrays.asList(
                newRole("idTwo", groups("one"), groups(), false),
                newRole("idTwo", groups("one"), groups(), true),
                newRole("idTwo", groups("two", "thee"), groups(), false),
                newRole("idTwo", groups("two", "thee"), groups(), true),
                newRole("idTwo", groups("two", "three"), groups("three"), false),
                newRole("idTwo", groups("two", "three"), groups("three"), true),
                newRole("idTwo", groups("two", "three"), groups("three", "two"), false),
                newRole("idTwo", groups("two", "three"), groups("three", "two"), true));

        for (ApplicationRole left : roles) {
            for (ApplicationRole right : otherRoles) {
                assertThat(left.equals(right), equalTo(false));
            }
        }
    }

    @Test
    public void hashListensToId() {
        List<ApplicationRole> roles = Arrays.asList(
                newRole("id", groups("one"), groups(), false),
                newRole("id", groups("one"), groups(), true),
                newRole("id", groups("two", "thee"), groups(), false),
                newRole("id", groups("two", "thee"), groups(), true),
                newRole("id", groups("two", "three"), groups("three"), false),
                newRole("id", groups("two", "three"), groups("three"), true),
                newRole("id", groups("two", "three"), groups("three", "two"), false),
                newRole("id", groups("two", "three"), groups("three", "two"), true));

        for (ApplicationRole left : roles) {
            for (ApplicationRole right : roles) {
                assertThat(left.hashCode() == right.hashCode(), equalTo(true));
            }
        }
    }

    @Test
    public void getGroupsReturnsGroups() {
        assertThat(newRole("id", groups("one"), groups(), false).getGroups(), contains(group("one")));
        assertThat(newRole("id", groups("one", "two"), groups(), false).getGroups(),
                containsInAnyOrder(group("one"), group("two")));
    }

    @Test
    public void getDefaultGroupsReturnsDefaultGroups() {
        assertTrue(newRole("id", groups("one"), groups(), false).getDefaultGroups().isEmpty());
        assertThat(newRole("id", groups("one", "two"), groups("one", "two"), false).getDefaultGroups(),
                containsInAnyOrder(group("two"), group("one")));
    }

    @Test
    public void getIdReturnsId() {
        assertThat(newRole("id", groups("one"), groups(), false).getKey(), equalTo(valueOf("id")));
    }

    @Test
    public void getNameReturnsId() {
        assertThat(newRole("id", groups("one"), groups(), false).getName(), equalTo("id"));
    }

    @Test
    public void getSelectedByDefaultReturnsSelectedByDefault() {
        assertThat(newRole("id", groups(), groups(), true).isSelectedByDefault(), equalTo(true));
        assertThat(newRole("id", groups(), groups(), false).isSelectedByDefault(), equalTo(false));
    }

    @Test
    public void withGroupsUpdatesGroupInformation() {
        final ApplicationRole orig = newRole("id", groups("one"), groups(), false);
        final ApplicationRole newRole = orig.withGroups(groups("one", "two"), groups());

        assertThat(orig, new ApplicationRoleMatcher().key("id").name("id").groupNames("one").seats(UNLIMITED_USERS));
        assertThat(newRole, new ApplicationRoleMatcher().key("id").name("id").groupNames("one", "two").seats(UNLIMITED_USERS));
    }

    @Test
    public void withGroupsUpdatesDefaultGroupInformation() {
        final ApplicationRole orig = newRole("id", groups("one"), groups(), false);
        final ApplicationRole newRole = orig.withGroups(groups("one", "two"), groups("two"));

        assertThat(orig, new ApplicationRoleMatcher().key("id").name("id").groupNames("one").seats(UNLIMITED_USERS));
        assertThat(newRole, new ApplicationRoleMatcher().key("id").name("id")
                .groupNames("one", "two").defaultGroupNames("two").seats(UNLIMITED_USERS));

        final ApplicationRole noneRole = newRole.withGroups(groups("one", "two"), groups());

        assertThat(newRole, new ApplicationRoleMatcher().key("id").name("id")
                .groupNames("one", "two").defaultGroupNames("two").seats(UNLIMITED_USERS));

        assertThat(noneRole, new ApplicationRoleMatcher().key("id").name("id")
                .groupNames("one", "two").seats(UNLIMITED_USERS));
    }

    @Test
    public void withSelectedByDefaultUpdateSelectedByDefaultInformation() {
        final ApplicationRole orig = newRole("id", groups(), groups(), false);
        final ApplicationRole newRole = orig.withSelectedByDefault(true);

        assertThat(orig, new ApplicationRoleMatcher().key("id").name("id").seats(UNLIMITED_USERS));
        assertThat(newRole, new ApplicationRoleMatcher().key("id").name("id").selectedByDefault(true).seats(UNLIMITED_USERS));
    }

    @Test(expected = IllegalArgumentException.class)
    public void withGroupsFailsWithNullGroups() {
        final ApplicationRole idApplicationRole = newRole("id", groups("one"), groups(), false);
        idApplicationRole.withGroups(null, groups());
    }

    @Test(expected = IllegalArgumentException.class)
    public void withGroupsFailsWithNullInGroups() {
        final ApplicationRole idApplicationRole = newRole("id", groups("one"), groups(), false);
        idApplicationRole.withGroups(groups("abc", null), groups());
    }

    @Test(expected = IllegalArgumentException.class)
    public void withGroupsFailsWithNullDefaultGroups() {
        final ApplicationRole idApplicationRole = newRole("id", groups("one"), groups(), false);
        idApplicationRole.withGroups(groups("abc"), null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void withGroupsFailsWithNullDefaultInGroups() {
        final ApplicationRole idApplicationRole = newRole("id", groups("one"), groups(), false);
        idApplicationRole.withGroups(groups("abc"), groups("abc", null));
    }

    @Test(expected = IllegalArgumentException.class)
    public void withGroupsFailsWithDefaultGroupNotContained() {
        final ApplicationRole idApplicationRole = newRole("id", groups("one"), groups("one", "two"), false);
        idApplicationRole.withGroups(groups("abc"), groups("def"));
    }

    private static ApplicationRole newRole(String id, Iterable<Group> groups, Iterable<Group> defaultGroups, boolean selectedByDefault) {
        return new IdApplicationRole(valueOf(id), groups, defaultGroups, UNLIMITED_USERS, selectedByDefault);
    }

    private static Set<Group> groups(@Nullable String... groups) {
        if (groups == null) {
            return ImmutableSet.of();
        } else {
            return Arrays.stream(groups)
                    .map(IdApplicationRoleTest::group)
                    .collect(Collectors.toSet());
        }
    }

    private static Group group(@Nullable final String name) {
        return name == null ? null : new MockGroup(name);
    }

    private static Set<Group> groups() {
        return ImmutableSet.of();
    }
}