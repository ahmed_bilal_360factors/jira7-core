package com.atlassian.jira.issue.search.searchers.renderer;

import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.search.SearchContext;
import com.atlassian.jira.issue.search.constants.SystemSearchConstants;
import com.atlassian.jira.issue.search.searchers.util.DateSearcherConfig;
import com.atlassian.jira.mock.MockApplicationProperties;
import com.atlassian.jira.template.VelocityTemplatingEngine;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.velocity.DefaultVelocityRequestContextFactory;
import com.atlassian.jira.web.FieldVisibilityManager;
import com.atlassian.jira.web.action.util.CalendarLanguageUtil;
import com.atlassian.query.Query;
import com.atlassian.query.QueryImpl;
import com.atlassian.query.clause.AndClause;
import com.atlassian.query.clause.TerminalClauseImpl;
import com.atlassian.query.operator.Operator;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Test for {@link TestSystemDateSearchRenderer}.
 *
 * @since v4.0
 */
public class TestSystemDateSearchRenderer {
    private static final String FIELD_NAME = "duedate";

    @Test
    public void testIsShownTrueWhenFieldCares() {
        final SearchContext context = mock(SearchContext.class);

        final FieldVisibilityManager mockFieldVisibilityManager = mock(FieldVisibilityManager.class);

        when(mockFieldVisibilityManager.isFieldHiddenInAllSchemes(FIELD_NAME, context, null)).thenReturn(false);

        final DateSearchRenderer dateSearchRenderer = createRenderer(FIELD_NAME, mockFieldVisibilityManager);
        assertTrue(dateSearchRenderer.isShown(null, context));
    }

    @Test
    public void testIsShownFalseWhenFieldCares() {
        final SearchContext context = mock(SearchContext.class);

        final FieldVisibilityManager mockFieldVisibilityManager = mock(FieldVisibilityManager.class);
        when(mockFieldVisibilityManager.isFieldHiddenInAllSchemes(FIELD_NAME, context, null)).thenReturn(true);

        final DateSearchRenderer dateSearchRenderer = createRenderer(FIELD_NAME, mockFieldVisibilityManager);
        assertFalse(dateSearchRenderer.isShown(null, context));

    }

    @Test
    public void testIsRelevantForSearchRequest() {
        final FieldVisibilityManager mockFieldVisibilityManager = mock(FieldVisibilityManager.class);
        final DateSearchRenderer dateSearchRenderer = createRenderer(FIELD_NAME, mockFieldVisibilityManager);

        Query query = new QueryImpl(new TerminalClauseImpl(FIELD_NAME + "SoNot", Operator.LESS_THAN_EQUALS, "test"));

        assertFalse(dateSearchRenderer.isRelevantForQuery((ApplicationUser) null, query));

        query = new QueryImpl(new AndClause(
                new TerminalClauseImpl(FIELD_NAME + "SoNot", Operator.LESS_THAN_EQUALS, "test"),
                new TerminalClauseImpl(FIELD_NAME, Operator.LESS_THAN_EQUALS, "test")));

        assertTrue(dateSearchRenderer.isRelevantForQuery((ApplicationUser) null, query));
    }

    private DateSearchRenderer createRenderer(final String id, final FieldVisibilityManager fieldVisibilityManager) {
        final ApplicationProperties renderProperties = new MockApplicationProperties();

        final VelocityTemplatingEngine mockTemplatingEngine = mock(VelocityTemplatingEngine.class);
        final CalendarLanguageUtil calendarUtils = mock(CalendarLanguageUtil.class);

        return new DateSearchRenderer(SystemSearchConstants.forDueDate(), new DateSearcherConfig(id, SystemSearchConstants.forDueDate().getJqlClauseNames(),
                SystemSearchConstants.forDueDate().getJqlClauseNames().getPrimaryName()), "mykey",
                new DefaultVelocityRequestContextFactory(renderProperties), renderProperties, mockTemplatingEngine,
                calendarUtils, fieldVisibilityManager);
    }
}
