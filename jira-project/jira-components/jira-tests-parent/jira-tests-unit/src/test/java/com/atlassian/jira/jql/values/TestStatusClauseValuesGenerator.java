package com.atlassian.jira.jql.values;

import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.issue.status.MockStatus;
import com.atlassian.jira.issue.status.Status;
import com.google.common.collect.ImmutableList;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestStatusClauseValuesGenerator {
    @Rule
    public MockitoRule mockito = MockitoJUnit.rule();
    @Mock
    private ConstantsManager constantsManager;
    @InjectMocks
    private StatusClauseValuesGenerator valuesGenerator;

    @Test
    public void testGetAllConstants() throws Exception {
        final MockStatus type1 = new MockStatus("1", "Aa it");
        final MockStatus type2 = new MockStatus("2", "A it");
        final MockStatus type3 = new MockStatus("3", "B it");
        final MockStatus type4 = new MockStatus("4", "C it");
        final ImmutableList<Status> statusObjects = ImmutableList.of(type4, type3, type2, type1);

        when(constantsManager.getStatusObjects()).thenReturn(statusObjects);
        assertEquals(statusObjects, valuesGenerator.getAllConstants());
    }

}
