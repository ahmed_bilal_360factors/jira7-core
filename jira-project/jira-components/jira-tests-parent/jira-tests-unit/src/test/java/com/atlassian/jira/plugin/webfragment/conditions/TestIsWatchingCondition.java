package com.atlassian.jira.plugin.webfragment.conditions;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.watchers.WatcherManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

public class TestIsWatchingCondition {
    @Rule
    public MockitoRule mockito = MockitoJUnit.rule();

    @Mock
    private WatcherManager watcherManager;
    @Mock
    private Issue issue;

    final ApplicationUser fred = new MockApplicationUser("fred");
    private IsWatchingIssueCondition condition;

    @Before
    public void setUp() throws Exception {
        condition = new IsWatchingIssueCondition(watcherManager);
    }

    @Test
    public void testNullUser() {
        assertFalse(condition.shouldDisplay(null, issue, null));
    }

    @Test
    public void testFalse() {
        when(watcherManager.isWatching(fred, issue)).thenReturn(false);

        assertFalse(condition.shouldDisplay(fred, issue, null));
    }

    @Test
    public void testTrue() {
        when(watcherManager.isWatching(fred, issue)).thenReturn(true);

        assertTrue(condition.shouldDisplay(fred, issue, null));
    }


}
