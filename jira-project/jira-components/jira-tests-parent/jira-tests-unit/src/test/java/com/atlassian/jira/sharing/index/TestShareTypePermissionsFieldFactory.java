package com.atlassian.jira.sharing.index;

import com.atlassian.gadgets.dashboard.Layout;
import com.atlassian.jira.portal.PortalPage;
import com.atlassian.jira.sharing.SharePermissionImpl;
import com.atlassian.jira.sharing.SharedEntity;
import com.atlassian.jira.sharing.search.ShareTypeSearchParameter;
import com.atlassian.jira.sharing.type.GlobalShareType;
import com.atlassian.jira.sharing.type.ShareQueryFactory;
import com.atlassian.jira.sharing.type.ShareType;
import com.atlassian.jira.sharing.type.ShareTypeFactory;
import com.atlassian.jira.user.MockApplicationUser;
import org.apache.lucene.document.Field;
import org.junit.Test;

import java.io.StringReader;
import java.util.Collection;

import static com.atlassian.jira.matchers.FeatureMatchers.hasFeature;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * A test case for ShareTypePermissionsFieldFactory
 *
 * @since v3.13
 */
public class TestShareTypePermissionsFieldFactory {
    private static final String THIS_IS_THE_RESULT = "ThisIsTheResult";

    /**
     * This always returns null.  Its only do as part of the interface implementation
     */
    @Test
    public void test_getFieldName() {
        final ShareTypePermissionsFieldFactory fieldFactory = new ShareTypePermissionsFieldFactory(null);

        assertNull(fieldFactory.getFieldName());
    }

    @Test
    public void test_getField_PrivatePermission() {
        final PortalPage portalPage = PortalPage.id(123L).name("name").description("desc").owner(new MockApplicationUser("ownerName")).
                favouriteCount(5L).layout(Layout.AA).version(0L).permissions(SharedEntity.SharePermissions.PRIVATE).build();

        final ShareTypePermissionsFieldFactory fieldFactory = new ShareTypePermissionsFieldFactory(null);
        final Collection<Field> fields = fieldFactory.getField(portalPage);

        assertThat(fields, contains(
                hasFeature(Field::name, equalTo("owner"), "Owner name", "Owner name")
        ));
    }

    @Test
    public void test_getField_GlobalPermission() {
        final PortalPage portalPage = PortalPage.id(123L).name("name").description("desc").owner(new MockApplicationUser("ownerName")).
                favouriteCount(5L).layout(Layout.AA).version(0L).permissions(SharedEntity.SharePermissions.GLOBAL).build();

        final ShareType.Name name = GlobalShareType.TYPE;
        final Field expectedField = new Field(THIS_IS_THE_RESULT, new StringReader(""));

        @SuppressWarnings("unchecked")
        final ShareQueryFactory<ShareTypeSearchParameter> shareQueryFactory = mock(ShareQueryFactory.class);
        when(shareQueryFactory.getField(portalPage, new SharePermissionImpl(name, null, null))).thenReturn(expectedField);

        final ShareType shareType = mock(ShareType.class);
        //noinspection unchecked
        when(shareType.getQueryFactory()).thenReturn((ShareQueryFactory) shareQueryFactory);

        final ShareTypeFactory shareTypeFactory = mock(ShareTypeFactory.class);
        when(shareTypeFactory.getShareType(name)).thenReturn(shareType);

        final ShareTypePermissionsFieldFactory fieldFactory = new ShareTypePermissionsFieldFactory(shareTypeFactory);
        final Collection<Field> fields = fieldFactory.getField(portalPage);

        assertThat(fields, contains(
                hasFeature(Field::name, equalTo(THIS_IS_THE_RESULT), "Owner name", "Owner name")
        ));
    }

}
