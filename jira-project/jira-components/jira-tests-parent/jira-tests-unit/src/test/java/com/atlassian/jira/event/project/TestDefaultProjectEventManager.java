package com.atlassian.jira.event.project;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.event.ProjectUpdatedDetailedChangesEvent;
import com.atlassian.jira.event.ProjectUpdatedEvent;
import com.atlassian.jira.event.ProjectUpdatedKeyChangedEvent;
import com.atlassian.jira.event.ProjectUpdatedTypeChangedEvent;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.type.ProjectTypeKey;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.web.action.RequestSourceType;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestDefaultProjectEventManager {
    @Mock
    private EventPublisher eventPublisher;
    @Mock
    private ApplicationUser user;
    @Mock
    private Project oldProject;
    @Mock
    private Project newProject;
    @Mock
    private ProjectTypeKey businessProject;
    @Mock
    private ProjectTypeKey softwareProject;
    private RequestSourceType requestSourceType = RequestSourceType.PAGE;

    private ProjectEventManager defaultProjectEventManager;

    @Before
    public void setup() {
        when(businessProject.getKey()).thenReturn("business");
        when(softwareProject.getKey()).thenReturn("software");
        when(oldProject.getProjectTypeKey()).thenReturn(businessProject);
        when(newProject.getProjectTypeKey()).thenReturn(businessProject);

        this.defaultProjectEventManager = new DefaultProjectEventManager(eventPublisher);
    }

    @Test
    public void projectUpdateWithIdenticalKeysAndTypesDoNotFireRespectiveEvents() {
        defaultProjectEventManager.dispatchProjectUpdated(user, newProject, oldProject, requestSourceType);

        verify(eventPublisher).publish(isA(ProjectUpdatedEvent.class));
        verify(eventPublisher).publish(isA(ProjectUpdatedDetailedChangesEvent.class));
        verify(eventPublisher, never()).publish(isA(ProjectUpdatedKeyChangedEvent.class));
        verify(eventPublisher, never()).publish(isA(ProjectUpdatedTypeChangedEvent.class));
    }

    @Test
    public void projectUpdateThatAlsoChangesProjectKeyShouldFireRespectiveEvent() {
        when(oldProject.getKey()).thenReturn("old");
        when(newProject.getKey()).thenReturn("new");

        defaultProjectEventManager.dispatchProjectUpdated(user, newProject, oldProject, requestSourceType);

        verify(eventPublisher).publish(isA(ProjectUpdatedEvent.class));
        verify(eventPublisher).publish(isA(ProjectUpdatedDetailedChangesEvent.class));
        verify(eventPublisher).publish(isA(ProjectUpdatedKeyChangedEvent.class));
        verify(eventPublisher, never()).publish(isA(ProjectUpdatedTypeChangedEvent.class));
    }

    @Test
    public void projectUpdateThatAlsoChangesProjectTypeShouldFireRespectiveEvent() {
        when(newProject.getProjectTypeKey()).thenReturn(softwareProject);

        defaultProjectEventManager.dispatchProjectUpdated(user, newProject, oldProject, requestSourceType);

        verify(eventPublisher).publish(isA(ProjectUpdatedEvent.class));
        verify(eventPublisher).publish(isA(ProjectUpdatedDetailedChangesEvent.class));
        verify(eventPublisher, never()).publish(isA(ProjectUpdatedKeyChangedEvent.class));
        verify(eventPublisher).publish(isA(ProjectUpdatedTypeChangedEvent.class));
    }
}
