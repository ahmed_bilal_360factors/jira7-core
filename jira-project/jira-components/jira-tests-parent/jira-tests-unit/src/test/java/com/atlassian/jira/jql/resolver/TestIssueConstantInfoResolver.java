package com.atlassian.jira.jql.resolver;

import com.atlassian.jira.issue.priority.Priority;
import com.atlassian.jira.issue.status.Status;
import com.google.common.collect.Lists;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Unit test for {@link IssueConstantInfoResolver}.
 *
 * @since v4.0
 */
public class TestIssueConstantInfoResolver {
    @Rule
    public MethodRule initMockito = MockitoJUnit.rule();
    @Mock
    private Priority mockPriority;
    @Mock
    private NameResolver<Priority> mockNameResolver;

    @Test(expected = RuntimeException.class)
    public void testConstructor() {
        new IssueConstantInfoResolver<Status>(null);
        fail("epxected problemo constructing from nulls"); // throws AssertionError, not RuntimeException
    }

    @Test
    public void testGetIndexedValuesString() {
        final List<String> priorityIds = Lists.newArrayList("123", "91919");
        when(mockNameResolver.getIdsFromName("somePriorityName")).thenReturn(priorityIds);

        IssueConstantInfoResolver<Priority> resolver = new IssueConstantInfoResolver<Priority>(mockNameResolver);
        assertEquals(priorityIds, resolver.getIndexedValues("somePriorityName"));
    }

    @Test
    public void testGetIndexedValuesStringFallBackToLong() {
        when(mockNameResolver.getIdsFromName("999")).thenReturn(new ArrayList<String>());
        when(mockNameResolver.idExists(new Long(999))).thenReturn(true);

        IssueConstantInfoResolver<Priority> resolver = new IssueConstantInfoResolver<Priority>(mockNameResolver);
        assertEquals(Lists.newArrayList("999"), resolver.getIndexedValues("999"));
    }

    @Test
    public void testGetIndexedValuesBothNameAndIdExist() {
        when(mockNameResolver.getIdsFromName("1")).thenReturn(Lists.newArrayList("2"));
        when(mockNameResolver.idExists(1L)).thenReturn(true);

        IssueConstantInfoResolver<Priority> resolver = new IssueConstantInfoResolver<Priority>(mockNameResolver);
        assertEquals(Lists.newArrayList("2", "1"), resolver.getIndexedValues("1"));
    }

    @Test
    public void testGetIndexedValuesLongExists() {
        when(mockNameResolver.idExists(4321L)).thenReturn(true);

        IssueConstantInfoResolver<Priority> resolver = new IssueConstantInfoResolver<Priority>(mockNameResolver);
        assertEquals(Lists.newArrayList("4321"), resolver.getIndexedValues(4321L));
    }

    @Test
    public void testGetIndexedValuesLongDoesNotExist() {
        when(mockNameResolver.idExists(4321L)).thenReturn(false);
        final List<String> priorityIds = Lists.newArrayList("86", "99");
        when(mockNameResolver.getIdsFromName("4321")).thenReturn(priorityIds);

        IssueConstantInfoResolver<Priority> resolver = new IssueConstantInfoResolver<Priority>(mockNameResolver);
        assertEquals(priorityIds, resolver.getIndexedValues(4321L));
    }

    @Test(expected = RuntimeException.class)
    public void testGetIndexedValue() {
        final Priority mockPriority = mock(Priority.class);
        when(mockPriority.getId()).thenReturn("666666");

        @SuppressWarnings("unchecked")
        final NameResolver<Priority> nameResolver = mock(NameResolver.class);
        IssueConstantInfoResolver<Priority> resolver = new IssueConstantInfoResolver<Priority>(nameResolver);
        assertEquals("666666", resolver.getIndexedValue(mockPriority));

        resolver.getIndexedValue(null);
        fail("expected RTE"); // throws AssertionError, not RuntimeException
    }

}
