package com.atlassian.jira.cache.monitor;

import com.atlassian.cache.CacheManager;
import com.atlassian.cache.CacheStatisticsKey;
import com.atlassian.cache.ManagedCache;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.cache.JiraVCacheRequestContextSupplier;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.config.properties.JiraSystemProperties;
import com.atlassian.jira.instrumentation.DefaultInstrumentationListenerManager;
import com.atlassian.jira.instrumentation.DefaultInstrumentationLogger;
import com.atlassian.scheduler.JobRunnerRequest;
import com.atlassian.util.concurrent.Supplier;
import com.atlassian.vcache.internal.VCacheLifecycleManager;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSortedMap;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.ArgumentMatcher;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import java.time.Duration;
import java.util.Map;
import java.util.SortedMap;

import static com.atlassian.cache.CacheStatisticsKey.EVICTION_COUNT;
import static com.atlassian.cache.CacheStatisticsKey.HEAP_SIZE;
import static com.atlassian.cache.CacheStatisticsKey.HIT_COUNT;
import static com.atlassian.cache.CacheStatisticsKey.MISS_COUNT;
import static com.atlassian.cache.CacheStatisticsKey.PUT_COUNT;
import static com.atlassian.cache.CacheStatisticsKey.REMOVE_COUNT;
import static com.atlassian.cache.CacheStatisticsKey.SIZE;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class CacheStatisticsMonitorTest {
    @Rule
    public final MethodRule initMockito = MockitoJUnit.rule();
    @Mock
    public EventPublisher eventPublisher;
    @Mock
    public CacheManager cacheManager;
    @Mock
    public JobRunnerRequest jobRunner;
    @Mock
    public ManagedCache managedCache;
    @Mock
    public VCacheLifecycleManager vCacheLifecycleManager;
    @Mock
    public JiraVCacheRequestContextSupplier contextSupplier;
    @Mock
    private FeatureManager featureManager;

    CacheStatisticsMonitor cacheStatisticsMonitor;

    @Before
    public void setUp() throws Exception {
        cacheStatisticsMonitor = new CacheStatisticsMonitor(cacheManager, eventPublisher,
                new DefaultInstrumentationListenerManager(new DefaultInstrumentationLogger(JiraSystemProperties.getInstance(), featureManager, eventPublisher), vCacheLifecycleManager,
                        contextSupplier, cacheManager), JiraSystemProperties.getInstance());
    }

    @Test
    public void collectingCacheStatsShouldNotCollectHeapSize() throws Exception {
        when(managedCache.isStatisticsEnabled()).thenReturn(true);
        when(managedCache.getStatistics()).thenReturn(getInputStatistics());
        when(managedCache.getName()).thenReturn("Test cache");
        when(cacheManager.getManagedCaches()).thenReturn(ImmutableList.of(managedCache));

        cacheStatisticsMonitor.runJob(jobRunner);
        verify(eventPublisher).publish(argThat(new CacheEventMatcher(managedCache.getName(), getOutputStatistics())));
    }

    @Test
    public void cacheStatisticJobShouldRunDaily() {
        assertEquals(Duration.ofDays(1L).toMillis(), CacheStatisticsMonitor.RUN_INTERVAL);
    }

    private SortedMap<CacheStatisticsKey, Supplier<Long>> getInputStatistics() {
        return ImmutableSortedMap.<CacheStatisticsKey, Supplier<Long>>orderedBy(CacheStatisticsKey.SORT_BY_LABEL)
                .put(SIZE, () -> 1L)
                .put(HEAP_SIZE, new Supplier<Long>() {
                    @Override
                    public Long get() {
                        throw new IllegalAccessError("HEAP_SIZE should not be collected, it may cause a performance problems. Please refer to CacheStatisticsKey.HEAP_SIZE documentation");
                    }
                })
                .put(HIT_COUNT, () -> 3L)
                .put(PUT_COUNT, () -> 4L)
                .put(REMOVE_COUNT, () -> 5L)
                .put(MISS_COUNT, () -> 6L)
                .put(EVICTION_COUNT, () -> 7L)
                .build();
    }

    private Map<CacheStatisticsKey, Long> getOutputStatistics() {
        return ImmutableMap.<CacheStatisticsKey, Long>builder()
                .put(SIZE, 1L)
                .put(HIT_COUNT, 3L)
                .put(PUT_COUNT, 4L)
                .put(REMOVE_COUNT, 5L)
                .put(MISS_COUNT, 6L)
                .put(EVICTION_COUNT, 7L)
                .build();
    }

    private class CacheEventMatcher extends ArgumentMatcher<CacheStatisticsAnalyticEvent> {
        private final String name;
        private final Map<CacheStatisticsKey, Long> outputStatistics;

        public CacheEventMatcher(final String name, final Map<CacheStatisticsKey, Long> outputStatistics) {
            this.name = name;
            this.outputStatistics = outputStatistics;
        }

        @Override
        public boolean matches(final Object argument) {
            final CacheStatisticsAnalyticEvent event = (CacheStatisticsAnalyticEvent) argument;
            return event.getName().equals("Test cache") &&
                    event.getCacheSize().equals(outputStatistics.get(SIZE)) &&
                    event.getHitCount().equals(outputStatistics.get(HIT_COUNT)) &&
                    event.getPutCount().equals(outputStatistics.get(PUT_COUNT)) &&
                    event.getRemoveCount().equals(outputStatistics.get(REMOVE_COUNT)) &&
                    event.getMissCount().equals(outputStatistics.get(MISS_COUNT)) &&
                    event.getEvictionCount().equals(outputStatistics.get(EVICTION_COUNT));
        }
    }
}