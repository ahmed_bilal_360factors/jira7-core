package com.atlassian.jira.issue.fields.screen;

import com.atlassian.jira.action.issue.customfields.MockCustomFieldType;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.fields.MockCustomField;
import com.atlassian.jira.issue.fields.MockFieldManager;
import com.atlassian.jira.issue.fields.OrderableField;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutManager;
import com.atlassian.jira.issue.fields.layout.field.MockFieldLayout;
import com.atlassian.jira.issue.fields.layout.field.MockFieldLayoutManager;
import com.atlassian.jira.issue.fields.renderer.HackyFieldRendererRegistry;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.issue.issuetype.MockIssueType;
import com.atlassian.jira.mock.issue.MockIssue;
import com.atlassian.jira.mock.security.MockSimpleAuthenticationContext;
import com.atlassian.jira.plugin.customfield.CustomFieldTypeModuleDescriptor;
import com.atlassian.jira.plugin.customfield.CustomFieldTypeModuleDescriptorImpl;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.plugin.module.ModuleFactory;
import com.google.common.collect.ImmutableList;
import com.opensymphony.workflow.loader.ActionDescriptor;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static java.util.stream.Collectors.toList;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;

/**
 * Test for {@link com.atlassian.jira.issue.fields.screen.BulkFieldScreenRendererFactory}
 *
 * @since v4.1
 */
public class TestBulkFieldScreenRendererFactory {
    @Test
    public void testCreateRendererNoTabs() {
        final FieldLayoutManager layoutManager = mock(FieldLayoutManager.class);
        final FieldManager fieldManager = mock(FieldManager.class);
        final HackyFieldRendererRegistry hackyFieldRendererRegistry = mock(HackyFieldRendererRegistry.class);

        final BulkFieldScreenRendererFactory factory = new BulkFieldScreenRendererFactory(fieldManager, layoutManager, hackyFieldRendererRegistry) {
            @Override
            FieldScreen getScreen(final ActionDescriptor actionDescriptor) {
                return null;
            }
        };
        final BulkFieldScreenRendererImpl screenRenderer = factory.createRenderer(Collections.<Issue>emptyList(), null);

        assertThat(screenRenderer.getFieldScreenRenderTabs(), empty());
    }

    @Test
    public void testCreateRendererNoUnavailable() {
        final Project mockProject = new MockProject(5);
        final IssueType mockIssueType1 = new MockIssueType("6", "Six");
        final IssueType mockIssueType2 = new MockIssueType("7", "Seven");

        //A couple of different issues.
        final MockIssue issue1 = new MockIssue(3);
        issue1.setIssueTypeObject(mockIssueType1);
        issue1.setProjectObject(mockProject);

        final MockIssue issue2 = new MockIssue(4);
        issue2.setIssueTypeObject(mockIssueType2);
        issue2.setProjectObject(mockProject);

        //Create some orderable fields fields.
        final MockFieldManager mfm = new MockFieldManager();
        final OrderableField of1 = mfm.addMockOrderableField(1);
        final OrderableField of2 = mfm.addMockOrderableField(2);
        final OrderableField of3 = mfm.addMockOrderableField(3);

        //Create the screens.
        final MockFieldScreen fieldScreen = new MockFieldScreen();
        fieldScreen.addMockTab().setName("EmptyScreen");

        final MockFieldScreenTab tab1 = fieldScreen.addMockTab();
        tab1.setName("One");
        tab1.addFieldScreenLayoutItem().setOrderableField(of1);
        tab1.addFieldScreenLayoutItem().setOrderableField(of2);

        final MockFieldScreenTab tab2 = fieldScreen.addMockTab();
        tab2.setName("Two");
        tab2.addFieldScreenLayoutItem().setOrderableField(of3);

        //Create some field layouts
        final MockFieldLayoutManager mflm = new MockFieldLayoutManager();
        final MockFieldLayout fieldLayout = mflm.addLayoutItem(issue1);
        fieldLayout.addFieldLayoutItem(of1);
        fieldLayout.addFieldLayoutItem(of2);
        fieldLayout.addFieldLayoutItem(of3);

        final MockFieldLayout fieldLayout2 = mflm.addLayoutItem(issue2);
        fieldLayout2.addFieldLayoutItem(of1);
        fieldLayout2.addFieldLayoutItem(of2);
        fieldLayout2.addFieldLayoutItem(of3);

        final BulkFieldScreenRendererFactory factory = new BulkFieldScreenRendererFactory(mfm, mflm, mock(HackyFieldRendererRegistry.class)) {
            @Override
            FieldScreen getScreen(final ActionDescriptor actionDescriptor) {
                return fieldScreen;
            }
        };

        final List<Issue> issues = ImmutableList.of(issue1, issue2);
        final BulkFieldScreenRendererImpl screenRenderer = factory.createRenderer(issues, null);
        final List<FieldScreenRenderTab> tabList = screenRenderer.getFieldScreenRenderTabs();
        assertThat(tabList, hasSize(2));

        //Check the first tab. Its items should be in the same order as the screen its generated from.
        FieldScreenRenderTab actualTab = tabList.get(0);
        assertThat(actualTab.getName(), equalTo("One"));
        List<FieldScreenRenderLayoutItem> actualRenderLayoutItems = actualTab.getFieldScreenRenderLayoutItems();
        assertThat(actualRenderLayoutItems, hasSize(2));

        List<FieldScreenLayoutItem> actualLayoutItems = actualRenderLayoutItems.stream().map(FieldScreenRenderLayoutItem::getFieldScreenLayoutItem).collect(toList());
        List<FieldScreenLayoutItem> expectedLayoutItems = tab1.getFieldScreenLayoutItems();
        assertThat(actualLayoutItems, containsInAnyOrder(expectedLayoutItems.toArray()));

        for (int i = 0; i < actualRenderLayoutItems.size(); i++) {
            final BulkFieldScreenRenderLayoutItemImpl actualBulkRenderItem = castToBulkItem(actualRenderLayoutItems.get(i));
            final Collection<FieldLayoutItem> expectedFieldItems = createFieldLayoutItem(mflm, issues, expectedLayoutItems.get(i).getOrderableField());
            final Collection<FieldLayoutItem> actualFieldLayoutItems = actualBulkRenderItem.getFieldLayoutItems();

            assertThat("In layout item: " + i, actualFieldLayoutItems, containsInAnyOrder(expectedFieldItems.toArray()));
        }

        //Check the second tab. Its items should be in the same order as the screen its generated from.
        actualTab = tabList.get(1);
        assertThat(actualTab.getName(), equalTo("Two"));

        actualRenderLayoutItems = actualTab.getFieldScreenRenderLayoutItems();
        assertThat(actualRenderLayoutItems, hasSize(1));

        actualLayoutItems = actualRenderLayoutItems.stream().map(FieldScreenRenderLayoutItem::getFieldScreenLayoutItem).collect(toList());
        expectedLayoutItems = tab2.getFieldScreenLayoutItems();
        assertThat(actualLayoutItems, containsInAnyOrder(expectedLayoutItems.toArray()));

        for (int i = 0; i < actualRenderLayoutItems.size(); i++) {
            final BulkFieldScreenRenderLayoutItemImpl actualBulkRenderItem = castToBulkItem(actualRenderLayoutItems.get(i));
            final Collection<FieldLayoutItem> expectedFieldItems = createFieldLayoutItem(mflm, issues, expectedLayoutItems.get(i).getOrderableField());
            final Collection<FieldLayoutItem> actualFieldLayoutItems = actualBulkRenderItem.getFieldLayoutItems();

            assertThat("In layout item: " + i, actualFieldLayoutItems, containsInAnyOrder(expectedFieldItems.toArray()));
        }
    }

    @Test
    public void testCreateRendererUnavailableFields() {
        final Project mockProject = new MockProject(5);
        final IssueType mockIssueType1 = new MockIssueType("6", "Six");
        final IssueType mockIssueType2 = new MockIssueType("7", "Seven");

        //A couple of different issues.
        final MockIssue issue1 = new MockIssue(3);
        issue1.setIssueTypeObject(mockIssueType1);
        issue1.setProjectObject(mockProject);

        final MockIssue issue2 = new MockIssue(4);
        issue2.setIssueTypeObject(mockIssueType2);
        issue2.setProjectObject(mockProject);

        //Create some orderable fields fields.
        final MockFieldManager mfm = new MockFieldManager();
        final OrderableField of1 = mfm.addMockOrderableField(1);
        final OrderableField of2 = mfm.addMockOrderableField(2);
        final OrderableField of3 = mfm.addMockOrderableField(3);
        mfm.addUnavilableField(of2);

        //Create the screens.
        final MockFieldScreen fieldScreen = new MockFieldScreen();
        fieldScreen.addMockTab().setName("EmptyScreen");

        final MockFieldScreenTab tab1 = fieldScreen.addMockTab();
        tab1.setName("One");
        tab1.addFieldScreenLayoutItem().setOrderableField(of1);
        tab1.addFieldScreenLayoutItem().setOrderableField(of2);

        final MockFieldScreenTab tab2 = fieldScreen.addMockTab();
        tab2.setName("Two");
        tab2.addFieldScreenLayoutItem().setOrderableField(of3);

        //Create some field layouts
        final MockFieldLayoutManager mflm = new MockFieldLayoutManager();
        MockFieldLayout fieldLayout = mflm.addLayoutItem(issue1);
        fieldLayout.addFieldLayoutItem(of1);
        fieldLayout.addFieldLayoutItem(of2);
        fieldLayout.addFieldLayoutItem(of3);

        fieldLayout = mflm.addLayoutItem(issue2);
        fieldLayout.addFieldLayoutItem(of1);
        fieldLayout.addFieldLayoutItem(of2);
        fieldLayout.addFieldLayoutItem(of3);

        final BulkFieldScreenRendererFactory factory = new BulkFieldScreenRendererFactory(mfm, mflm, mock(HackyFieldRendererRegistry.class)) {
            @Override
            FieldScreen getScreen(final ActionDescriptor actionDescriptor) {
                return fieldScreen;
            }
        };

        final List<Issue> issues = Arrays.<Issue>asList(issue1, issue2);
        final BulkFieldScreenRendererImpl screenRenderer = factory.createRenderer(issues, null);
        final List<FieldScreenRenderTab> tabList = screenRenderer.getFieldScreenRenderTabs();
        assertThat(tabList, hasSize(2));

        //Check the first tab.
        FieldScreenRenderTab actualTab = tabList.get(0);
        assertThat(actualTab.getName(), equalTo("One"));
        List<FieldScreenRenderLayoutItem> actualLayoutItems = actualTab.getFieldScreenRenderLayoutItems();
        assertThat(actualLayoutItems, hasSize(1));

        FieldScreenRenderLayoutItem item = actualLayoutItems.get(0);
        FieldScreenLayoutItem screenLayoutItem = tab1.getFieldScreenLayoutItem(0);
        assertThat(item.getFieldScreenLayoutItem(), equalTo(screenLayoutItem));

        BulkFieldScreenRenderLayoutItemImpl bulkItem = castToBulkItem(item);
        assertThat(bulkItem.getFieldLayoutItems(), containsInAnyOrder(createFieldLayoutItem(mflm, issues, screenLayoutItem.getOrderableField()).toArray()));

        //Check the second tab
        actualTab = tabList.get(1);
        assertThat(actualTab.getName(), equalTo("Two"));
        actualLayoutItems = actualTab.getFieldScreenRenderLayoutItems();
        assertThat(actualLayoutItems, hasSize(1));

        item = actualLayoutItems.get(0);
        screenLayoutItem = tab2.getFieldScreenLayoutItem(0);
        assertThat(item.getFieldScreenLayoutItem(), equalTo(screenLayoutItem));

        bulkItem = castToBulkItem(item);
        assertThat(bulkItem.getFieldLayoutItems(), containsInAnyOrder(createFieldLayoutItem(mflm, issues, screenLayoutItem.getOrderableField()).toArray()));
    }

    @Test
    public void testCreateRendererCustomFieldsFields() {
        final Project mockProject = new MockProject(5);
        final IssueType mockIssueType1 = new MockIssueType("6", "Six");
        final IssueType mockIssueType2 = new MockIssueType("7", "Seven");

        //A couple of different issues.
        final MockIssue issue1 = new MockIssue(3);
        issue1.setIssueTypeObject(mockIssueType1);
        issue1.setProjectObject(mockProject);

        final MockIssue issue2 = new MockIssue(4);
        issue2.setIssueTypeObject(mockIssueType2);
        issue2.setProjectObject(mockProject);

        //Create some orderable fields fields.
        final MockFieldManager mfm = new MockFieldManager();
        final OrderableField of1 = mfm.addMockOrderableField(1);
        final MockCustomField of2 = mfm.addMockCustomField(2);
        makeViewExists(of2, false);
        final MockCustomField of3 = mfm.addMockCustomField(3);
        makeViewExists(of3, true);

        //Create the screens.
        final MockFieldScreen fieldScreen = new MockFieldScreen();
        fieldScreen.addMockTab().setName("EmptyScreen");

        final MockFieldScreenTab tab1 = fieldScreen.addMockTab();
        tab1.setName("One");
        tab1.addFieldScreenLayoutItem().setOrderableField(of1);
        tab1.addFieldScreenLayoutItem().setOrderableField(of2);

        final MockFieldScreenTab tab2 = fieldScreen.addMockTab();
        tab2.setName("Two");
        tab2.addFieldScreenLayoutItem().setOrderableField(of3);

        //Create some field layouts
        final MockFieldLayoutManager mflm = new MockFieldLayoutManager();
        MockFieldLayout fieldLayout = mflm.addLayoutItem(issue1);
        fieldLayout.addFieldLayoutItem(of1);
        fieldLayout.addFieldLayoutItem(of2);
        fieldLayout.addFieldLayoutItem(of3);

        fieldLayout = mflm.addLayoutItem(issue2);
        fieldLayout.addFieldLayoutItem(of1);
        fieldLayout.addFieldLayoutItem(of2);
        fieldLayout.addFieldLayoutItem(of3);

        final BulkFieldScreenRendererFactory factory = new BulkFieldScreenRendererFactory(mfm, mflm, mock(HackyFieldRendererRegistry.class)) {
            @Override
            FieldScreen getScreen(final ActionDescriptor actionDescriptor) {
                return fieldScreen;
            }
        };

        final List<Issue> issues = ImmutableList.of(issue1, issue2);
        final BulkFieldScreenRendererImpl screenRenderer = factory.createRenderer(issues, null);
        final List<FieldScreenRenderTab> tabList = screenRenderer.getFieldScreenRenderTabs();
        assertThat(tabList, hasSize(2));

        //Check the first tab.
        FieldScreenRenderTab actualTab = tabList.get(0);
        assertThat(actualTab.getName(), equalTo("One"));
        List<FieldScreenRenderLayoutItem> actualLayoutItems = actualTab.getFieldScreenRenderLayoutItems();
        assertThat(actualLayoutItems, hasSize(1));

        FieldScreenRenderLayoutItem item = actualLayoutItems.get(0);
        FieldScreenLayoutItem screenLayoutItem = tab1.getFieldScreenLayoutItem(0);
        assertThat(item.getFieldScreenLayoutItem(), equalTo(screenLayoutItem));

        BulkFieldScreenRenderLayoutItemImpl bulkItem = castToBulkItem(item);
        assertThat(bulkItem.getFieldLayoutItems(), containsInAnyOrder(createFieldLayoutItem(mflm, issues, screenLayoutItem.getOrderableField()).toArray()));

        //Check the second tab
        actualTab = tabList.get(1);
        assertThat(actualTab.getName(), equalTo("Two"));
        actualLayoutItems = actualTab.getFieldScreenRenderLayoutItems();
        assertThat(actualLayoutItems, hasSize(1));

        item = actualLayoutItems.get(0);
        screenLayoutItem = tab2.getFieldScreenLayoutItem(0);
        assertThat(item.getFieldScreenLayoutItem(), equalTo(screenLayoutItem));

        bulkItem = castToBulkItem(item);
        assertThat(bulkItem.getFieldLayoutItems(), containsInAnyOrder(createFieldLayoutItem(mflm, issues, screenLayoutItem.getOrderableField()).toArray()));
    }


    private Collection<FieldLayoutItem> createFieldLayoutItem(final FieldLayoutManager flm, final Collection<Issue> issues, final OrderableField field) {
        return issues.stream().map(issue -> flm.getFieldLayout(issue).getFieldLayoutItem(field)).collect(toList());
    }

    private static CustomField makeViewExists(final MockCustomField field, final boolean exists) {
        final MockCustomFieldType customFieldType = field.createCustomFieldType();
        customFieldType.init(createViewExistsCFType(exists));
        field.setCustomFieldType(customFieldType);

        return field;
    }

    private static CustomFieldTypeModuleDescriptor createViewExistsCFType(final boolean exists) {
        return new CustomFieldTypeModuleDescriptorImpl(new MockSimpleAuthenticationContext(null), null, ModuleFactory.LEGACY_MODULE_FACTORY, null) {
            @Override
            public boolean isViewTemplateExists() {
                return exists;
            }
        };
    }

    private BulkFieldScreenRenderLayoutItemImpl castToBulkItem(final FieldScreenRenderLayoutItem item) {
        return (BulkFieldScreenRenderLayoutItemImpl) item;
    }
}
