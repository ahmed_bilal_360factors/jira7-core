package com.atlassian.jira.bc.filter;

import com.atlassian.jira.bc.JiraServiceContextImpl;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.ErrorCollectionAssert;
import com.atlassian.jira.util.NoopI18nHelper;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.jira.web.component.cron.parser.CronExpressionParser;
import com.atlassian.scheduler.SchedulerService;
import com.atlassian.scheduler.SchedulerServiceException;
import com.atlassian.scheduler.caesium.cron.CaesiumCronExpressionValidator;
import com.atlassian.scheduler.config.Schedule;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import java.util.Date;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestFilterSubscriptionCronValidation {
    private static final Date DATE = new Date();

    private FilterSubscriptionService service;

    @Mock
    private SchedulerService schedulerService;

    @Before
    public void setUp() throws Exception {
        service = createService();

        final ArgumentCaptor<Schedule> scheduleCaptor = ArgumentCaptor.forClass(Schedule.class);
        when(schedulerService.calculateNextRunTime(scheduleCaptor.capture())).thenAnswer(new Answer<Date>() {
            @Override
            public Date answer(final InvocationOnMock invocation) throws Throwable {
                new CaesiumCronExpressionValidator()
                        .validate(scheduleCaptor.getValue()
                                .getCronScheduleInfo()
                                .getCronExpression());
                return DATE;
            }
        });
    }


    @After
    public void tearDown() throws Exception {
        service = null;
    }

    @Test
    public void testFilterCronNullExpr() {
        assert1Error(null, "cron.expression.invalid.INTERNAL_PARSER_FAILURE{[null]}");
    }

    @Test
    public void testFilterCronEmptyStringExpr() {
        assert1Error("", "cron.expression.invalid.UNEXPECTED_END_OF_EXPRESSION{[]}");
    }

    @Test
    public void testFilterCronUnexpectedEnd() {
        assert1Error("0 0 0 ? 1", "cron.expression.invalid.UNEXPECTED_END_OF_EXPRESSION{[]}");
    }

    @Test
    public void testFilterCronIllegalFormat() {
        assert1Error("0 0 0 ? 1 NO", "cron.expression.invalid.INVALID_NAME_DAY_OF_WEEK{[NO]}");
    }

    @Test
    public void testFilterCronInvalidMonthName() {
        assertNoErrors("0 0 0 ? FEB MON");
        assertNoErrors("0 0 0 ? FEB-NOV MON");

        assert1Error("0 0 0 ? NOT MON", "cron.expression.invalid.INVALID_NAME_MONTH{[NOT]}");
        assert1Error("0 0 0 ? FEB-NOT MON", "cron.expression.invalid.INVALID_NAME_MONTH{[NOT]}");
    }

    @Test
    public void testFilterCronInvalidDayName() {
        assertNoErrors("0 0 0 ? 1 MON");
        assertNoErrors("0 0 0 ? 1 MON-TUE");

        assert1Error("0 0 0 ? 1 NOT", "cron.expression.invalid.INVALID_NAME_DAY_OF_WEEK{[NOT]}");
        assert1Error("0 0 0 ? 1 MON-NOT", "cron.expression.invalid.INVALID_NAME_DAY_OF_WEEK{[NOT]}");
    }

    @Test
    public void testFilterCronNumericAfterHash() {
        assertNoErrors("0 0 0 ? 1 MON#3");
        assertNoErrors("0 0 0 ? 1 1#3");

        assert1Error("0 0 0 ? 1 MON#8", "cron.expression.invalid.ILLEGAL_CHARACTER_AFTER_HASH{[]}");
        assert1Error("0 0 0 ? 1 MON#three", "cron.expression.invalid.ILLEGAL_CHARACTER_AFTER_HASH{[]}");
        assert1Error("0 0 0 ? 1 1#8", "cron.expression.invalid.ILLEGAL_CHARACTER_AFTER_HASH{[]}");
        assert1Error("0 0 0 ? 1 1#three", "cron.expression.invalid.ILLEGAL_CHARACTER_AFTER_HASH{[]}");
    }

    @Test
    public void testFilterCronInvalidChars() {
        assert1Error("0 XXX 0 ? 1 MON-TUE", "cron.expression.invalid.INVALID_NAME_FIELD{[XXX]}");
    }

    @Test
    public void testFilterCronIllegalCharAfterQuestionMark() {
        assertNoErrors("0 0 0 ? 1 MON-TUE");
        assertNoErrors("0 0 0 ?\t1 MON-TUE");

        assert1Error("0 0 0 ?X 1 MON-TUE", "cron.expression.invalid.ILLEGAL_CHARACTER_AFTER_QM{[X]}");
        assert1Error("0 0 0 ?XX 1 MON-TUE", "cron.expression.invalid.ILLEGAL_CHARACTER_AFTER_QM{[X]}");
    }

    @Test
    public void testFilterCronInvalidQuestionMark() {
        assert1Error("0 ? 0 ? 1 1", "cron.expression.invalid.QM_CANNOT_USE_HERE{[]}");
    }

    @Test
    public void testFilterCronInvalidQuestionMarkForBoth() {
        assert1Error("0 0 0 ? 1 ?", "cron.expression.invalid.QM_CANNOT_USE_FOR_BOTH_DAYS{[]}");
    }

    @Test
    public void testFilterCronInvalidIncrements() {
        assertNoErrors("0 /5 0 ? 1 MON");
        assertNoErrors("0 5/5 0 ? 1 MON");
        assertNoErrors("0 */5 0 ? 1 MON");

        assert1Error("0 / 0 ? 1 MON", "cron.expression.invalid.INVALID_STEP{[]}");
        assert1Error("/65 0 0 ? 1 MON", "cron.expression.invalid.INVALID_STEP_SECOND_OR_MINUTE{[65]}");
        assert1Error("0 /65 0 ? 1 MON", "cron.expression.invalid.INVALID_STEP_SECOND_OR_MINUTE{[65]}");
        assert1Error("0 0 /26 ? 1 MON", "cron.expression.invalid.INVALID_STEP_HOUR{[26]}");
        assert1Error("0 0 0 ? /15 MON", "cron.expression.invalid.INVALID_STEP_MONTH{[15]}");
        assert1Error("0 0 0 ? 1 /9", "cron.expression.invalid.INVALID_STEP_DAY_OF_WEEK{[9]}");
        assert1Error("0 0 0 /36 1 ?", "cron.expression.invalid.INVALID_STEP_DAY_OF_MONTH{[36]}");
        assert1Error("0 0 0 1/6a 1 ?", "cron.expression.invalid.ILLEGAL_CHARACTER_AFTER_INTERVAL{[A]}");
    }

    @Test
    public void testFilterCronInvalidCharacter() {
        assert1Error("0 @ 0 1 1 ?", "cron.expression.invalid.ILLEGAL_CHARACTER{[@]}");
    }

    @Test
    public void testFilterCronInvalidLOption() {
        assertNoErrors("0 0 0 ? 1 L");
        assertNoErrors("0 0 0 L 1 ?");

        assert1Error("L 0 0 1 1 ?", "cron.expression.invalid.UNEXPECTED_TOKEN_FLAG_L{[]}");
        assert1Error("6L 0 0 1 1 ?", "cron.expression.invalid.UNEXPECTED_TOKEN_FLAG_L{[]}");
    }

    @Test
    public void testFilterCronInvalidWOption() {
        assertNoErrors("0 0 0 LW 1 ?");
        assertNoErrors("0 0 0 15W 1 ?");

        assert1Error("6W 0 0 1 1 ?", "cron.expression.invalid.UNEXPECTED_TOKEN_FLAG_W{[]}");
    }

    @Test
    public void testFilterCronInvalidHashOption() {
        assertNoErrors("0 0 0 ? 1 1#3");
        assertNoErrors("0 0 0 ? 1 MON#3");

        assert1Error("6#4 0 0 1 1 ?", "cron.expression.invalid.UNEXPECTED_TOKEN_HASH{[]}");
    }


    @Test
    public void testFilterCronInvalidCOption() {
        assert1Error("0 0 0 ? 1 5C", "cron.expression.invalid.ILLEGAL_CHARACTER{[C]}");
        assert1Error("0 0 0 5C 1 ?", "cron.expression.invalid.ILLEGAL_CHARACTER{[C]}");
        assert1Error("6C 0 0 1 1 ?", "cron.expression.invalid.ILLEGAL_CHARACTER{[C]}");
    }

    @Test
    public void testFilterCronInvalidValues() {
        assert1Error("65 0 0 ? 1 MON", "cron.expression.invalid.INVALID_NUMBER_SEC_OR_MIN{[]}");
        assert1Error("0 65 0 ? 1 MON", "cron.expression.invalid.INVALID_NUMBER_SEC_OR_MIN{[]}");
        assert1Error("0 0 26 ? 1 MON", "cron.expression.invalid.INVALID_NUMBER_HOUR{[]}");
        assert1Error("0 0 0 33 1 ?", "cron.expression.invalid.INVALID_NUMBER_DAY_OF_MONTH{[]}");
        assert1Error("0 0 0 ? 15 MON", "cron.expression.invalid.INVALID_NUMBER_MONTH{[]}");
        assert1Error("0 0 0 ? 1 9", "cron.expression.invalid.INVALID_NUMBER_DAY_OF_WEEK{[]}");
    }

    /**
     * Check validity of default string used to seed the cron editor.
     */
    @Test
    public void testFilterWithDefaultValue() {
        assertNoErrors(CronExpressionParser.DEFAULT_CRONSTRING);
    }

    @Test
    public void testFilterWillNeverRun() throws SchedulerServiceException {
        // Clear out the fake answer we use for all the other tests...
        doReturn(null).when(schedulerService).calculateNextRunTime(any(Schedule.class));

        assert1Error("0 0 1 31 2 ?", "cron.expression.invalid.will.never.run{[0 0 1 31 2 ?]}");
    }

    private DefaultFilterSubscriptionService createService() {
        JiraAuthenticationContext authenticationContext = mock(JiraAuthenticationContext.class);
        when(authenticationContext.getI18nHelper()).thenReturn(new NoopI18nHelper());

        return new DefaultFilterSubscriptionService(authenticationContext, null, schedulerService);
    }

    private ErrorCollection validate(String cronExpression) {
        final JiraServiceContextImpl context = new JiraServiceContextImpl((ApplicationUser) null, new SimpleErrorCollection());
        service.validateCronExpression(context, cronExpression);
        return context.getErrorCollection();
    }

    private void assertNoErrors(String cronExpression) {
        ErrorCollectionAssert.assertNoErrors(validate(cronExpression));
    }

    private void assert1Error(String cronExpression, String expectedError) {
        ErrorCollectionAssert.assert1ErrorMessage(validate(cronExpression), expectedError);
    }
}
