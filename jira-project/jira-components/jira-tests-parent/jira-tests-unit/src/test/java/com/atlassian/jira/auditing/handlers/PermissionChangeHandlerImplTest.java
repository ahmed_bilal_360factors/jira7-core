package com.atlassian.jira.auditing.handlers;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.event.permission.PermissionAddedEvent;
import com.atlassian.jira.event.permission.PermissionDeletedEvent;
import com.atlassian.jira.local.runner.ListeningMockitoRunner;
import com.atlassian.jira.mock.component.MockComponentWorker;
import com.atlassian.jira.permission.PermissionSchemeManager;
import com.atlassian.jira.permission.PermissionTypeManager;
import com.atlassian.jira.scheme.SchemeEntity;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.plugin.ProjectPermissionKey;
import com.atlassian.jira.util.I18nHelper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;

import java.util.Locale;

import static com.atlassian.jira.permission.ProjectPermissions.ADMINISTER_PROJECTS;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

/*
 * @since v6.3
 */
@RunWith(ListeningMockitoRunner.class)
public class PermissionChangeHandlerImplTest {
    @Mock
    private PermissionSchemeManager permissionSchemeManager;

    @Mock
    private PermissionManager permissionManager;

    @Mock
    private PermissionTypeManager permissionTypeManager;

    @Mock
    private I18nHelper.BeanFactory beanFactory;

    @Mock
    private SchemeEntity schemeEntity;

    @Mock
    private com.atlassian.jira.util.I18nHelper i18Helper;

    @Mock
    private com.atlassian.jira.security.type.SecurityType securityType;

    private PermissionChangeHandlerImpl permissionChangeHandler;

    @Before
    public void setUp() throws Exception {
        ComponentAccessor.initialiseWorker(new MockComponentWorker()
                        .addMock(I18nHelper.BeanFactory.class, beanFactory)
        );

        when(beanFactory.getInstance(Locale.ENGLISH)).thenReturn(i18Helper);
        when(i18Helper.getText(anyString())).thenReturn("some text value");
        when(permissionTypeManager.getSchemeType("scheme type")).thenReturn(securityType);
        when(securityType.getDisplayName()).thenReturn("some name");

        permissionChangeHandler =
                new PermissionChangeHandlerImpl(permissionSchemeManager, permissionManager, permissionTypeManager, beanFactory) {
                    protected String getPermissionName(ProjectPermissionKey permissionKey) {
                        return "some permission key";
                    }
                };
    }

    @Test
    public void schemeEntityTypeIdMustBeProjectPermissionKey() {
        permissionChangeHandler.computeChangedValues(new PermissionAddedEvent(1L, new SchemeEntity("scheme type", ADMINISTER_PROJECTS)));
        permissionChangeHandler.computeChangedValues(new PermissionDeletedEvent(1L, new SchemeEntity("scheme type", ADMINISTER_PROJECTS)));

        try {
            permissionChangeHandler.computeChangedValues(new PermissionAddedEvent(1L, new SchemeEntity("scheme type", "WTF")));
            fail();
        } catch (IllegalArgumentException e) {
            // Expected
        }

        try {
            permissionChangeHandler.computeChangedValues(new PermissionDeletedEvent(1L, new SchemeEntity("scheme type", "WTF")));
            fail();
        } catch (IllegalArgumentException e) {
            // Expected
        }
    }
}
