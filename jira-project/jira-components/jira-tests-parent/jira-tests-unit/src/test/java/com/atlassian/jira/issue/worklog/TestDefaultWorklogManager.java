package com.atlassian.jira.issue.worklog;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.event.worklog.WorklogDeletedEvent;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.mock.issue.MockIssue;
import com.atlassian.jira.security.roles.ProjectRoleManager;
import com.atlassian.jira.transaction.MockTransactionSupport;
import com.atlassian.jira.transaction.TransactionSupport;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.RuleChain;
import org.mockito.Mock;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestDefaultWorklogManager {

    @Rule
    public final RuleChain mocksInContainer = MockitoMocksInContainer.forTest(this);
    @Rule
    public final ExpectedException expectedException = ExpectedException.none();

    @Mock
    private EventPublisher eventPublisher;
    @Mock
    private TimeTrackingIssueUpdater timeTrackingIssueUpdater;
    @Mock
    private WorklogStore worklogStore;
    @Mock
    private ProjectRoleManager projectRoleManager;

    @AvailableInContainer
    private TransactionSupport transactionSupport = new MockTransactionSupport();

    private DefaultWorklogManager worklogManager;

    @Before
    public void setUp() throws Exception {
        worklogManager = new DefaultWorklogManager(projectRoleManager, worklogStore, timeTrackingIssueUpdater, eventPublisher);
    }

    @Test
    public void testCreateWorklogNullWorklog() {
        try {
            worklogManager.create(null, null, null, true);
            fail();
        } catch (final IllegalArgumentException e) {
            assertEquals("Worklog must not be null.", e.getMessage());
        }
    }

    @Test
    public void testCreateWorklogNullIssueInWorklog() {
        Worklog worklog = mock(Worklog.class);
        when(worklog.getIssue()).thenReturn(null);
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("The worklogs issue must not be null.");
        worklogManager.create(null, worklog, null, true);
    }

    @Test
    public void testCreateWorklogHappyPath() {
        Worklog worklog = mock(Worklog.class);
        when(worklog.getIssue()).thenReturn(new MockIssue());
        when(worklogStore.create(worklog)).thenReturn(worklog);

        worklogManager.create(null, worklog, null, true);

        verify(timeTrackingIssueUpdater).updateIssueOnWorklogCreate(null, worklog, null, true);
    }

    @Test
    public void testUpdateWorklogNullWorklog() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Worklog must not be null.");
        worklogManager.update(null, null, null, true);
    }

    @Test
    public void testUpdateWorklogNullIssueInWorklog() {
        Worklog worklog = mock(Worklog.class);
        when(worklog.getIssue()).thenReturn(null);

        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("The worklogs issue must not be null.");

        worklogManager.update(null, worklog, null, true);
    }

    @Test
    public void testUpdateWorklogNullOriginalWorklog() {
        when(worklogStore.getById(10l)).thenReturn(null);

        Worklog worklog = mock(Worklog.class);
        when(worklog.getIssue()).thenReturn(new MockIssue());
        when(worklog.getId()).thenReturn(10l);

        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Unable to find a worklog in the datastore for the provided id: '10'");
        worklogManager.update(null, worklog, null, true);
    }

    @Test
    public void testUpdateWorklogHappyPath() {
        final long originalTimeSpent = 10l;
        final long worklogId = 1l;

        Worklog originalWorklog = mock(Worklog.class);
        when(originalWorklog.getTimeSpent()).thenReturn(originalTimeSpent);
        when(worklogStore.getById(worklogId)).thenReturn(originalWorklog);

        Worklog newWorklog = mock(Worklog.class);
        when(newWorklog.getIssue()).thenReturn(new MockIssue());
        when(newWorklog.getId()).thenReturn(worklogId);
        when(worklogStore.update(newWorklog)).thenReturn(newWorklog);
        long newEstimate = 15l;

        worklogManager.update(null, newWorklog, newEstimate, true);

        verify(timeTrackingIssueUpdater).updateIssueOnWorklogUpdate(any(),
                eq(originalWorklog), eq(newWorklog), eq(10l), eq(newEstimate), eq(true));
        verify(worklogStore).update(newWorklog);
    }

    @Test
    public void testDeleteWorklogHappyPath() {
        Long worklogId = 1l;

        Worklog worklog = mock(Worklog.class);
        when(worklog.getIssue()).thenReturn(mock(Issue.class));
        when(worklog.getId()).thenReturn(worklogId);
        Long estimate = 10l;

        when(worklogStore.delete(worklogId)).thenReturn(true);
        worklogManager.delete(null, worklog, estimate, true);

        verify(timeTrackingIssueUpdater).updateIssueOnWorklogDelete(null, worklog, estimate, true);
    }

    @Test
    public void testValidateWorklogWithNullWorklogThrowsIllegalArgumentException() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Worklog must not be null.");
        worklogManager.validateWorklog(null, true);
    }

    @Test
    public void testValidateWorklogNullIssueOnWorklog() {
        Worklog worklog = mock(Worklog.class);
        when(worklog.getIssue()).thenReturn(null);

        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("The worklogs issue must not be null.");
        worklogManager.validateWorklog(worklog, false);
    }

    @Test
    public void testValidateWorklogNullWorklogIdNotCreate() {
        Worklog worklog = mock(Worklog.class);
        when(worklog.getIssue()).thenReturn(new MockIssue());
        when(worklog.getId()).thenReturn(null);

        worklogManager.validateWorklog(worklog, true);
    }

    @Test
    public void testValidateWorklogNullWorklogId() {
        Worklog worklog = mock(Worklog.class);
        when(worklog.getIssue()).thenReturn(new MockIssue());
        when(worklog.getId()).thenReturn(null);

        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Can not modify a worklog with a null id.");
        worklogManager.validateWorklog(worklog, false);
    }

    @Test
    public void testEventsPublishedWhenDeletingWorklogsForIssue() throws Exception {
        MockIssue issue = new MockIssue();
        Worklog worklog = mock(Worklog.class);
        List<Worklog> worklogsDeleted = Lists.newArrayList(worklog);
        when(worklogStore.getByIssue(issue)).thenReturn(worklogsDeleted);
        when(worklogStore.deleteWorklogsForIssue(issue)).thenReturn(1l);

        worklogManager.deleteWorklogsForIssue(issue);

        verify(eventPublisher).publish(new WorklogDeletedEvent(worklog));
    }
}
