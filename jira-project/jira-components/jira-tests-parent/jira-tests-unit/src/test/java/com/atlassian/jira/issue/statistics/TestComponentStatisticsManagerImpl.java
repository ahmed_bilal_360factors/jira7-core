package com.atlassian.jira.issue.statistics;

import com.atlassian.jira.bc.project.component.ProjectComponent;
import com.atlassian.jira.bc.project.component.ProjectComponentManager;
import com.atlassian.jira.project.Project;
import com.atlassian.query.Query;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;

import com.google.common.collect.Maps;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.collection.IsMapContaining.hasEntry;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestComponentStatisticsManagerImpl {

    @Mock
    StatisticsManagerImpl statisticsUtil;

    @Mock
    ProjectComponentManager projectComponentManager;

    @InjectMocks
    ComponentStatisticsManagerImpl componentStatisticsManager;

    Map<Project, Map<ProjectComponent, Integer>> testData;
    Project proj1;
    Project proj2;
    ProjectComponent emptyComp1;
    ProjectComponent emptyComp2;
    ProjectComponent comp1;
    ProjectComponent comp2;
    ProjectComponent comp4;
    ProjectComponent comp5;

    @Before
    public void setUp() throws Exception {
        testData = Maps.newHashMap();

        proj1 = mock(Project.class);
        when(proj1.getId()).thenReturn(1L);
        comp1 = mock(ProjectComponent.class);
        comp2 = mock(ProjectComponent.class);
        emptyComp1 = mock(ProjectComponent.class);

        proj2 = mock(Project.class);
        when(proj2.getId()).thenReturn(2L);
        comp4 = mock(ProjectComponent.class);
        comp5 = mock(ProjectComponent.class);
        emptyComp2 = mock(ProjectComponent.class);

        testData.put(proj1, Maps.newHashMap());
        testData.put(proj2, Maps.newHashMap());
        
        testData.get(proj1).put(comp1, 5);
        testData.get(proj1).put(comp2, 3);
        testData.get(proj1).put(null, 2);

        testData.get(proj2).put(comp4, 73);
        testData.get(proj2).put(comp5, 1000);
        
        when(projectComponentManager.findAllForProject(1L)).thenReturn(Arrays.asList(comp1, comp2, emptyComp1));
        when(projectComponentManager.findAllForProject(2L)).thenReturn(Arrays.asList(comp4, comp5, emptyComp2));
    }

    @Test
    public void testGetProjectsWithComponentsWithIssueCount() throws Exception {
        Optional<Query> q = Optional.of(mock(Query.class));
        when(statisticsUtil.<ProjectComponent>getProjectsWithItemsWithIssueCount(
                eq(q), any(Function.class), eq("components"))).thenReturn(testData);

        Map<Project, Map<ProjectComponent, Integer>> result = componentStatisticsManager.getProjectsWithComponentsWithIssueCount(q);

        assertThat("Returned data is the same object", result, is(testData));
        assertThat("Project 1 has expected number of items", testData.get(proj1).size(), is(4));
        assertThat("Non-empty components are still there", testData.get(proj1), hasEntry(comp1, 5));
        assertThat("Non-empty components are still there", testData.get(proj1), hasEntry(comp2, 3));
        assertThat("Non-empty components are still there", testData.get(proj1), hasEntry(null, 2));
        assertThat("Empty components have been added", testData.get(proj1), hasEntry(emptyComp1, 0));
        
        assertThat("Project 2 has expected number of items", testData.get(proj2).size(), is(3));
        assertThat("Non-empty components are still there", testData.get(proj2), hasEntry(comp4, 73));
        assertThat("Non-empty components are still there", testData.get(proj2), hasEntry(comp5, 1000));
        assertThat("Empty components have been added", testData.get(proj2), hasEntry(emptyComp2, 0));
    }
}