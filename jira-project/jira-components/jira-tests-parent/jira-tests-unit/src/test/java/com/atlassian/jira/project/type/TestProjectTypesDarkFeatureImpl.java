package com.atlassian.jira.project.type;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertTrue;

@RunWith(MockitoJUnitRunner.class)
public class TestProjectTypesDarkFeatureImpl {
    private ProjectTypesDarkFeatureImpl projectTypesDarkFeature;

    @Before
    public void setUp() {
        projectTypesDarkFeature = new ProjectTypesDarkFeatureImpl();
    }

    @Test
    public void isAlwaysEnabled() {
        assertTrue(projectTypesDarkFeature.isEnabled());
    }

}
