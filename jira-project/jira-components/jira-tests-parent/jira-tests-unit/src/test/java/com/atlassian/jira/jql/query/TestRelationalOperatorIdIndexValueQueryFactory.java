package com.atlassian.jira.jql.query;

import com.atlassian.core.util.collection.EasyList;
import com.atlassian.jira.issue.comparator.PriorityObjectComparator;
import com.atlassian.jira.issue.priority.MockPriority;
import com.atlassian.jira.issue.priority.Priority;
import com.atlassian.jira.jql.operand.QueryLiteral;
import com.atlassian.jira.jql.resolver.IndexInfoResolver;
import com.atlassian.jira.jql.resolver.PriorityResolver;
import com.atlassian.query.operator.Operator;
import com.google.common.collect.ImmutableList;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TermQuery;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import static com.atlassian.jira.jql.operand.SimpleLiteralFactory.createLiteral;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestRelationalOperatorIdIndexValueQueryFactory {
    @Rule
    public MockitoRule mockito = MockitoJUnit.rule();

    @Mock
    private Comparator<Priority> mockPriorityComparator;
    @Mock
    private PriorityResolver mockPriorityResolver;
    @Mock
    private IndexInfoResolver<Priority> mockIssueConstantInfoResolver;


    RelationalOperatorIdIndexValueQueryFactory relationOperatorQueryFactory;

    @Before
    public void setUp() throws Exception {
        relationOperatorQueryFactory = new RelationalOperatorIdIndexValueQueryFactory<Priority>(mockPriorityComparator, mockPriorityResolver, mockIssueConstantInfoResolver);
    }

    @Test
    public void testCreateQueryForSingleValueNullAndEmptyValuesList() throws Exception {
        QueryFactoryResult query = relationOperatorQueryFactory.createQueryForSingleValue("blah", Operator.LESS_THAN, null);
        assertFalse(query.mustNotOccur());
        assertTrue(query.getLuceneQuery() instanceof BooleanQuery);
        assertEquals("", query.getLuceneQuery().toString());

        query = relationOperatorQueryFactory.createQueryForSingleValue("blah", Operator.LESS_THAN, Collections.<QueryLiteral>emptyList());
        assertFalse(query.mustNotOccur());
        assertTrue(query.getLuceneQuery() instanceof BooleanQuery);
        assertEquals("", query.getLuceneQuery().toString());
    }

    @Test
    public void testCreateQueryForSingleValueOperatorSingleInstance() throws Exception {
        when(mockIssueConstantInfoResolver.getIndexedValues("123")).thenReturn(ImmutableList.of("123"));

        final AtomicBoolean generateCalled = new AtomicBoolean(false);
        final BooleanQuery query = new BooleanQuery(true);
        relationOperatorQueryFactory = new RelationalOperatorIdIndexValueQueryFactory<Priority>(mockPriorityComparator, mockPriorityResolver, mockIssueConstantInfoResolver) {
            @Override
            Query generateQueryForValue(final String fieldName, final Operator operator, final String id) {
                generateCalled.set(true);
                return query;
            }
        };

        final QueryFactoryResult generatedQuery = relationOperatorQueryFactory.createQueryForSingleValue("blah", Operator.LESS_THAN, ImmutableList.of(createLiteral("123")));
        assertFalse(generatedQuery.mustNotOccur());
        assertTrue(generateCalled.get());
        assertEquals(query, generatedQuery.getLuceneQuery());
    }

    @Test
    public void testCreateQueryForSingleValueOperatorSingleInstanceNullQuery() throws Exception {
        when(mockIssueConstantInfoResolver.getIndexedValues("123")).thenReturn(ImmutableList.of("123"));

        final AtomicBoolean generateCalled = new AtomicBoolean(false);
        relationOperatorQueryFactory = new RelationalOperatorIdIndexValueQueryFactory<Priority>(mockPriorityComparator, mockPriorityResolver, mockIssueConstantInfoResolver) {
            @Override
            Query generateQueryForValue(final String fieldName, final Operator operator, final String id) {
                generateCalled.set(true);
                return null;
            }
        };

        final QueryFactoryResult generatedQuery = relationOperatorQueryFactory.createQueryForSingleValue("blah", Operator.LESS_THAN, ImmutableList.of(createLiteral("123")));
        assertTrue(generateCalled.get());
        assertFalse(generatedQuery.mustNotOccur());
        assertEquals(new BooleanQuery(), generatedQuery.getLuceneQuery());
    }

    @Test
    public void testCreateQueryForSingleValueOperatorMultipleValues() throws Exception {
        when(mockIssueConstantInfoResolver.getIndexedValues("123")).thenReturn(ImmutableList.of("123"));
        when(mockIssueConstantInfoResolver.getIndexedValues("234")).thenReturn(ImmutableList.of("234"));
        when(mockIssueConstantInfoResolver.getIndexedValues("345")).thenReturn(ImmutableList.of("345"));

        relationOperatorQueryFactory = new RelationalOperatorIdIndexValueQueryFactory<Priority>(mockPriorityComparator, mockPriorityResolver, mockIssueConstantInfoResolver) {
            @Override
            Query generateQueryForValue(final String fieldName, final Operator operator, final String id) {
                return new TermQuery(new Term(fieldName, id));
            }
        };

        final QueryFactoryResult generatedQuery = relationOperatorQueryFactory.createQueryForSingleValue("blah", Operator.LESS_THAN, ImmutableList.of(createLiteral("123"), createLiteral("234"), createLiteral("345")));
        assertFalse(generatedQuery.mustNotOccur());
        BooleanQuery expectedQuery = new BooleanQuery();
        expectedQuery.add(new TermQuery(new Term("blah", "123")), BooleanClause.Occur.SHOULD);
        expectedQuery.add(new TermQuery(new Term("blah", "234")), BooleanClause.Occur.SHOULD);
        expectedQuery.add(new TermQuery(new Term("blah", "345")), BooleanClause.Occur.SHOULD);
        assertEquals(expectedQuery, generatedQuery.getLuceneQuery());
    }

    @Test
    public void testCreateQueryForSingleValueOperatorMultipleValuesNullQueries() throws Exception {
        when(mockIssueConstantInfoResolver.getIndexedValues("123")).thenReturn(ImmutableList.of("123"));
        when(mockIssueConstantInfoResolver.getIndexedValues("345")).thenReturn(ImmutableList.of("345"));

        relationOperatorQueryFactory = new RelationalOperatorIdIndexValueQueryFactory<Priority>(mockPriorityComparator, mockPriorityResolver, mockIssueConstantInfoResolver) {
            @Override
            Query generateQueryForValue(final String fieldName, final Operator operator, final String id) {
                return null;
            }
        };

        final QueryFactoryResult generatedQuery = relationOperatorQueryFactory.createQueryForSingleValue("blah", Operator.LESS_THAN, EasyList.build(createLiteral("123"), null, createLiteral("345")));
        assertFalse(generatedQuery.mustNotOccur());
        assertEquals(new BooleanQuery(), generatedQuery.getLuceneQuery());
    }

    @Test
    public void testCheckQueryForEmpty() throws Exception {
        BooleanQuery query = new BooleanQuery();
        final QueryFactoryResult newQuery = relationOperatorQueryFactory.checkQueryForEmpty(query);
        assertFalse(newQuery.mustNotOccur());
        assertEquals(new BooleanQuery(), newQuery.getLuceneQuery());
    }

    @Test
    public void testCreateQueryForSingleValueUnsupportedOperator() throws Exception {
        final QueryFactoryResult result = relationOperatorQueryFactory.createQueryForSingleValue("blah", Operator.EQUALS, Collections.<QueryLiteral>emptyList());
        assertEquals(QueryFactoryResult.createFalseResult(), result);
    }

    @Test
    public void testHandlersOperator() throws Exception {
        assertTrue(relationOperatorQueryFactory.handlesOperator(Operator.GREATER_THAN));
        assertTrue(relationOperatorQueryFactory.handlesOperator(Operator.GREATER_THAN_EQUALS));
        assertTrue(relationOperatorQueryFactory.handlesOperator(Operator.LESS_THAN));
        assertTrue(relationOperatorQueryFactory.handlesOperator(Operator.LESS_THAN_EQUALS));
        assertFalse(relationOperatorQueryFactory.handlesOperator(Operator.EQUALS));
        assertFalse(relationOperatorQueryFactory.handlesOperator(Operator.NOT_EQUALS));
        assertFalse(relationOperatorQueryFactory.handlesOperator(Operator.IN));
    }

    @Test
    public void testMultipleValues() throws Exception {
        final QueryFactoryResult result = relationOperatorQueryFactory.createQueryForMultipleValues("blah", Operator.IN, Collections.<QueryLiteral>emptyList());
        assertEquals(QueryFactoryResult.createFalseResult(), result);
    }

    @Test
    public void testEmptyOperand() throws Exception {
        final QueryFactoryResult result = relationOperatorQueryFactory.createQueryForEmptyOperand("blah", Operator.EQUALS);
        assertEquals(QueryFactoryResult.createFalseResult(), result);
    }

    @Test
    public void testGenerateQueryForValueNullId() throws Exception {
        assertNull(relationOperatorQueryFactory.generateQueryForValue("blah", Operator.LESS_THAN, null));
    }

    @Test
    public void testGenerateQueryForValueHappyPath() throws Exception {
        final MockPriority mockPriority = new MockPriority("123", "testPri");
        mockPriority.setSequence(1L);
        final MockPriority mockPriority2 = new MockPriority("666", "evilPri");
        mockPriority2.setSequence(2L);
        final MockPriority mockPriority3 = new MockPriority("999", "otherPri");
        mockPriority3.setSequence(3L);

        when(mockPriorityResolver.get(123L)).thenReturn(mockPriority);
        when(mockPriorityResolver.getAll()).thenReturn(ImmutableList.of(mockPriority, mockPriority2, mockPriority3));


        when(mockIssueConstantInfoResolver.getIndexedValue(mockPriority)).thenReturn("123");
        when(mockIssueConstantInfoResolver.getIndexedValue(mockPriority2)).thenReturn("666");
        when(mockIssueConstantInfoResolver.getIndexedValue(mockPriority3)).thenReturn("999");

        relationOperatorQueryFactory = new RelationalOperatorIdIndexValueQueryFactory<Priority>(PriorityObjectComparator.PRIORITY_OBJECT_COMPARATOR, mockPriorityResolver, mockIssueConstantInfoResolver);

        final Query query = relationOperatorQueryFactory.generateQueryForValue("blah", Operator.LESS_THAN_EQUALS, "123");
        assertTrue(query instanceof BooleanQuery);
        BooleanQuery booleanQuery = (BooleanQuery) query;
        assertEquals(3, booleanQuery.clauses().size());
        final BooleanClause[] booleanClauses = booleanQuery.getClauses();

        final List expectedTerms = ImmutableList.of(new TermQuery(new Term("blah", "123")), new TermQuery(new Term("blah", "666")), new TermQuery(new Term("blah", "999")));
        for (BooleanClause booleanClause : booleanClauses) {
            assertTrue(BooleanClause.Occur.SHOULD.equals(booleanClause.getOccur()));
            assertTrue(expectedTerms.contains(booleanClause.getQuery()));
        }
    }

    @Test
    public void testGetIndexValuesNullDoesNotGetAddedForUnresolved() throws Exception {
        when(mockIssueConstantInfoResolver.getIndexedValues("testVal")).thenReturn(null);

        final List<String> indexVals = relationOperatorQueryFactory.getIndexValues(ImmutableList.of(createLiteral("testVal")));

        assertEquals(0, indexVals.size());
    }

    @Test
    public void testGetIndexValuesNullDoNotGetAddedForUnresolved() throws Exception {
        when(mockIssueConstantInfoResolver.getIndexedValues("testVal")).thenReturn(ImmutableList.of());


        final List<String> indexVals = relationOperatorQueryFactory.getIndexValues(ImmutableList.of(createLiteral("testVal")));

        assertEquals(0, indexVals.size());
    }

    @Test
    public void testGetIndexValuesHappyPathStringValue() throws Exception {
        when(mockIssueConstantInfoResolver.getIndexedValues("testVal")).thenReturn(ImmutableList.of("123", "666", "999"));

        final List<String> indexVals = relationOperatorQueryFactory.getIndexValues(ImmutableList.of(createLiteral("testVal")));
        assertEquals(3, indexVals.size());
        assertEquals(ImmutableList.of("123", "666", "999"), indexVals);
    }

    @Test
    public void testGetIndexValuesHappyPathLongValue() throws Exception {
        when(mockIssueConstantInfoResolver.getIndexedValues(12345L)).thenReturn(ImmutableList.of("123", "666"));
        when(mockIssueConstantInfoResolver.getIndexedValues(56789L)).thenReturn(ImmutableList.of("999"));

        final List<String> indexVals = relationOperatorQueryFactory.getIndexValues(ImmutableList.of(createLiteral(12345l), createLiteral(56789L)));
        assertEquals(3, indexVals.size());
        assertEquals(ImmutableList.of("123", "666", "999"), indexVals);
    }

}
