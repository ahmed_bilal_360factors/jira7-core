package com.atlassian.jira.jql.query;

import com.atlassian.jira.jql.validator.MockJqlOperandResolver;

/**
 * @since v4.0
 */
public class TestReporterClauseQueryFactory extends TestUserClauseQueryFactory {
    public TestReporterClauseQueryFactory() {
        this.fieldNameUnderTest = "reporter";
        this.clauseQueryFactory = new ReporterClauseQueryFactory(userResolver, MockJqlOperandResolver.createSimpleSupport());
    }
}
