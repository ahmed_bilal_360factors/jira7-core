package com.atlassian.jira.imports.project.core;

import com.atlassian.jira.external.beans.ExternalCustomField;
import com.atlassian.jira.external.beans.ExternalProject;
import com.atlassian.jira.imports.project.customfield.ExternalCustomFieldConfiguration;
import com.google.common.collect.ImmutableList;
import org.junit.Test;

import java.util.Collection;

import static java.util.Collections.emptyList;
import static java.util.Collections.emptyMap;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;

public class TestBackupProjectImpl {

    @Test
    public void getCustomFieldConfigurationEmpty() throws Exception {
        final BackupProject backupProject = new BackupProjectImpl(new ExternalProject(), emptyList(), emptyList(),
                emptyList(), emptyList(), 100, emptyMap());

        final ExternalCustomFieldConfiguration customFieldConfiguration = backupProject.getCustomFieldConfiguration("someId");

        assertNull(customFieldConfiguration);
    }

    @Test
    public void getCustomFieldConfigurationSingleDefaultOption() throws Exception {
        final ExternalCustomFieldConfiguration customFieldConfiguration = new ExternalCustomFieldConfiguration(
                emptyList(), null, new ExternalCustomField("100", "f", "s"), "12");
        final BackupProject backupProject = new BackupProjectImpl(new ExternalProject(), emptyList(), emptyList(),
                ImmutableList.of(customFieldConfiguration), emptyList(), 100, emptyMap());

        final ExternalCustomFieldConfiguration result = backupProject.getCustomFieldConfiguration("100");

        assertThat(result, is(customFieldConfiguration));

    }

    @Test
    public void getCustomFieldConfigurationSingleProjectOption() throws Exception {
        final ExternalCustomFieldConfiguration customFieldConfiguration = new ExternalCustomFieldConfiguration(
                emptyList(), "11", new ExternalCustomField("100", "f", "s"), "12");
        final BackupProject backupProject = new BackupProjectImpl(new ExternalProject(), emptyList(), emptyList(), ImmutableList.of(customFieldConfiguration), emptyList(), 100, emptyMap());

        final ExternalCustomFieldConfiguration result = backupProject.getCustomFieldConfiguration("100");

        assertThat(result, is(customFieldConfiguration));

    }

    @Test
    public void getCustomFieldConfigurationTwoOptions() throws Exception {
        final ExternalCustomFieldConfiguration customFieldConfigurationDefault = new ExternalCustomFieldConfiguration(
                emptyList(), null, new ExternalCustomField("100", "f", "s"), "12");
        final ExternalCustomFieldConfiguration customFieldConfigurationProject = new ExternalCustomFieldConfiguration(
                emptyList(), "11", new ExternalCustomField("100", "f", "s"), "12");
        final BackupProject backupProject = new BackupProjectImpl(new ExternalProject(), emptyList(), emptyList(),
                ImmutableList.of(customFieldConfigurationDefault, customFieldConfigurationProject), emptyList(), 100, emptyMap());

        final ExternalCustomFieldConfiguration result = backupProject.getCustomFieldConfiguration("100");

        assertThat(result, is(customFieldConfigurationProject));
    }


    @Test
    public void getCustomFieldConfigurationsEmpty() throws Exception {
        final BackupProject backupProject = new BackupProjectImpl(new ExternalProject(), emptyList(), emptyList(), emptyList(), emptyList(), 100, emptyMap());

        final Collection<ExternalCustomFieldConfiguration> customFieldConfiguration = backupProject.getCustomFieldConfigurations("someId");

        assertNotNull(customFieldConfiguration);
        assertThat(customFieldConfiguration, hasSize(0));
    }

    @Test
    public void getCustomFieldConfigurationsSingleDefaultOption() throws Exception {
        final ExternalCustomFieldConfiguration customFieldConfiguration = new ExternalCustomFieldConfiguration(
                emptyList(), null, new ExternalCustomField("100", "f", "s"), "12");
        final BackupProject backupProject = new BackupProjectImpl(new ExternalProject(), emptyList(), emptyList(), ImmutableList.of(customFieldConfiguration), emptyList(), 100, emptyMap());

        final Collection<ExternalCustomFieldConfiguration> result = backupProject.getCustomFieldConfigurations("100");

        assertNotNull(result);
        assertThat(result, hasSize(1));
        assertThat(result.iterator().next(), is(customFieldConfiguration));

    }

    @Test
    public void getCustomFieldConfigurationsSingleProjectOption() throws Exception {
        final ExternalCustomFieldConfiguration customFieldConfiguration = new ExternalCustomFieldConfiguration(
                emptyList(), "11", new ExternalCustomField("100", "f", "s"), "12");
        final BackupProject backupProject = new BackupProjectImpl(new ExternalProject(), emptyList(), emptyList(), ImmutableList.of(customFieldConfiguration), emptyList(), 100, emptyMap());

        final Collection<ExternalCustomFieldConfiguration> result = backupProject.getCustomFieldConfigurations("100");

        assertNotNull(result);
        assertThat(result, hasSize(1));
        assertThat(result.iterator().next(), is(customFieldConfiguration));

    }

    @Test
    public void getCustomFieldConfigurationsTwoOptions() throws Exception {
        final ExternalCustomFieldConfiguration customFieldConfigurationDefault = new ExternalCustomFieldConfiguration(
                emptyList(), null, new ExternalCustomField("100", "f", "s"), "12");
        final ExternalCustomFieldConfiguration customFieldConfigurationProject = new ExternalCustomFieldConfiguration(
                emptyList(), "11", new ExternalCustomField("100", "f", "s"), "12");
        final BackupProject backupProject = new BackupProjectImpl(new ExternalProject(), emptyList(), emptyList(),
                ImmutableList.of(customFieldConfigurationDefault, customFieldConfigurationProject), emptyList(), 100, emptyMap());

        final Collection<ExternalCustomFieldConfiguration> result = backupProject.getCustomFieldConfigurations("100");

        assertNotNull(result);
        assertThat(result, hasSize(2));
        assertThat(result, hasItem(customFieldConfigurationProject));
        assertThat(result, hasItem(customFieldConfigurationDefault));
    }

}