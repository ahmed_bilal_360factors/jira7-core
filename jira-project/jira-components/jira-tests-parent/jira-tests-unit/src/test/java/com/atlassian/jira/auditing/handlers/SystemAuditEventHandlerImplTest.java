package com.atlassian.jira.auditing.handlers;

import com.atlassian.extras.api.jira.JiraLicense;
import com.atlassian.fugue.Option;
import com.atlassian.jira.auditing.AffectedLicense;
import com.atlassian.jira.auditing.AssociatedItem;
import com.atlassian.jira.auditing.AuditingCategory;
import com.atlassian.jira.auditing.ChangedValue;
import com.atlassian.jira.auditing.ChangedValueImpl;
import com.atlassian.jira.auditing.RecordRequest;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.datetime.DateTimeFormatter;
import com.atlassian.jira.datetime.DateTimeFormatterFactory;
import com.atlassian.jira.datetime.DateTimeStyle;
import com.atlassian.jira.license.ConfirmEvaluationLicenseEvent;
import com.atlassian.jira.license.LicenseChangedEvent;
import com.atlassian.jira.license.LicenseDetails;
import com.atlassian.jira.license.LicensedApplications;
import com.atlassian.jira.local.runner.ListeningMockitoRunner;
import com.atlassian.jira.mock.component.MockComponentWorker;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.web.util.OutlookDateManager;
import com.google.common.collect.ImmutableList;
import org.hamcrest.BaseMatcher;
import org.hamcrest.CoreMatchers;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import javax.annotation.Nonnull;
import java.util.Date;
import java.util.Locale;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * All unit test for the SystemAuditEventHandler. since 6.4
 */
@RunWith(ListeningMockitoRunner.class)
public class SystemAuditEventHandlerImplTest {
    private static final String ORGANIZATION = "Atlassian";
    private static final String DESCRIPTION = "Atlassian description";
    private static final String PURCHASE_DATE = "14/12/2010";
    private static final String SEN = "1234";
    private static final java.lang.Integer LICENSE_VERSION = 1553;
    private static final Date MAINTENANCE_EXPIRE_DATE = new Date(10000);
    private static final String MAINTENANCE_EXPIRE_DATE_STRING = "Correct Date";
    private static final String USER_NAME = "USERINSERTHERE";
    private static final String SEN_TWO = "5678";
    private static final String I18N_ADMIN_LICENSE_ORGANISATION = "i18n.admin.license.organisation";
    private static final String I18N_ADMIN_LICENSE_DATE_PURCHASED = "i18n.admin.license.date.purchased";
    private static final String I18N_ADMIN_LICENSE_TYPE = "i18n.admin.license.type";
    private static final String I18N_ADMIN_SERVER_ID = "i18n.admin.server.id";
    private static final String I18N_ADMIN_LICENSE_SEN = "i18n.admin.license.sen";
    private static final String I18N_ADMIN_LICENSE_DATE_EXPIRED = "i18n.admin.license.date.expired";
    private static final String I18N_ADMIN_LICENSE_USER_LIMIT = "i18n.admin.license.user.limit";
    private static final String I18N_COMMON_WORDS_UNLIMITED = "i18n.common.words.unlimited";
    private static final String CHANGED_VALUES_RECORDED_AS_EXPECTED = "Changed values recorded as expected";

    @Mock
    private I18nHelper.BeanFactory beanFactory;

    @Mock
    private I18nHelper i18Helper;

    @Mock
    private OutlookDateManager outlookDateManager;

    @Mock
    private DateTimeFormatterFactory dateTimeFormatterFactory;

    @Mock
    private DateTimeFormatter dateTimeFormatter;

    @Mock
    private LicenseDetails licenseDetails;

    @Mock
    private JiraLicense license;

    @Mock
    private LicensedApplications licensedApplications;

    private SystemAuditEventHandler systemAuditEventHandler;

    @Before
    public void setUp() throws Exception {
        ComponentAccessor.initialiseWorker(new MockComponentWorker()
                        .addMock(I18nHelper.BeanFactory.class, beanFactory)
        );

        when(beanFactory.getInstance(Locale.ENGLISH)).thenReturn(i18Helper);
        when(i18Helper.getText(anyString())).thenAnswer(new Answer<Object>() {
            @Override
            public Object answer(final InvocationOnMock invocation) throws Throwable {
                return "i18n." + invocation.getArguments()[0];
            }
        });

        // Basic data to put in the license licenseDetails.
        when(licenseDetails.getOrganisation()).thenReturn(ORGANIZATION);
        when(licenseDetails.getDescription()).thenReturn(DESCRIPTION);
        when(licenseDetails.getLicenseVersion()).thenReturn(LICENSE_VERSION);
        when(licenseDetails.getPurchaseDate((DateTimeFormatter) null)).thenReturn(PURCHASE_DATE);
        when(licenseDetails.getSupportEntitlementNumber()).thenReturn(SEN);
        when(licenseDetails.isUnlimitedNumberOfUsers()).thenReturn(true);
        when(licenseDetails.getJiraLicense()).thenReturn(license);
        when(license.getServerId()).thenReturn("0.0.0.0");
        when(license.getMaintenanceExpiryDate()).thenReturn(MAINTENANCE_EXPIRE_DATE);
        when(dateTimeFormatterFactory.formatter()).thenReturn(dateTimeFormatter);
        when(dateTimeFormatter.format(MAINTENANCE_EXPIRE_DATE)).thenReturn(MAINTENANCE_EXPIRE_DATE_STRING);
        when(dateTimeFormatter.withStyle(any(DateTimeStyle.class))).thenReturn(dateTimeFormatter);

        systemAuditEventHandler = new SystemAuditEventHandlerImpl(i18Helper, outlookDateManager, dateTimeFormatterFactory);
    }

    @Test
    public void newLicenseEventIsAuditedWhenProperLicenseUsed() throws Exception {
        when(licenseDetails.getLicensedApplications()).thenReturn(licensedApplications);
        final LicenseChangedEvent event = new LicenseChangedEvent(Option.<LicenseDetails>none(), Option.option(licenseDetails));
        final RecordRequest request = systemAuditEventHandler.onLicenseChangedEvent(event);

        assertThat("Category type is SYSTEM enum", request.getCategory(), equalTo(AuditingCategory.SYSTEM));
        assertThat("Summary is internationalised str", request.getSummary(), equalTo("i18n.jira.auditing.system.license.added"));
        assertNotNull(request.getObjectItem());
        assertThat("Associated Object type enum to be License", request.getObjectItem().getObjectType(), equalTo(AssociatedItem.Type.LICENSE));
        assertThat("Type of license is not evaluation", request.getObjectItem().getParentName(), equalTo(AffectedLicense.TYPE_LICENSE));
        assertThat("Object name is the SEN number", request.getObjectItem().getObjectName(), equalTo(SEN));

        Matcher[] changedValues =
                {
                        changedValue(I18N_ADMIN_LICENSE_ORGANISATION, ORGANIZATION),
                        changedValue(I18N_ADMIN_LICENSE_DATE_PURCHASED, PURCHASE_DATE),
                        changedValue(I18N_ADMIN_LICENSE_TYPE, DESCRIPTION),
                        changedValue(I18N_ADMIN_SERVER_ID, "0.0.0.0"),
                        changedValue(I18N_ADMIN_LICENSE_SEN, SEN),
                        changedValue(I18N_ADMIN_LICENSE_USER_LIMIT, I18N_COMMON_WORDS_UNLIMITED)
                };

        Matcher<Iterable<? super ChangedValue>> hasChangedValues = CoreMatchers.hasItems(changedValues);
        assertThat(CHANGED_VALUES_RECORDED_AS_EXPECTED, request.getChangedValues(), hasChangedValues);
    }

    @Test
    public void onExtendTrialLicenseWhenExpiredSupportUsedCheckRecordsTwoLicensesFromEvent() throws Exception {
        final LicenseDetails secondLicenseDetails = mock(LicenseDetails.class);
        when(secondLicenseDetails.getOrganisation()).thenReturn(ORGANIZATION);
        when(secondLicenseDetails.getDescription()).thenReturn(DESCRIPTION);
        when(secondLicenseDetails.getLicenseVersion()).thenReturn(LICENSE_VERSION);
        when(secondLicenseDetails.getPurchaseDate((DateTimeFormatter) null)).thenReturn(PURCHASE_DATE);
        when(secondLicenseDetails.getSupportEntitlementNumber()).thenReturn(SEN_TWO);
        when(secondLicenseDetails.isEvaluation()).thenReturn(true);
        when(secondLicenseDetails.isUnlimitedNumberOfUsers()).thenReturn(true);
        when(secondLicenseDetails.getJiraLicense()).thenReturn(license);

        final ImmutableList<LicenseDetails> detailsList = ImmutableList.of(licenseDetails, secondLicenseDetails);
        final ConfirmEvaluationLicenseEvent event = new ConfirmEvaluationLicenseEvent(USER_NAME, detailsList);
        final RecordRequest request = systemAuditEventHandler.onExtendTrialLicense(event);

        assertThat("Category type is SYSTEM enum", request.getCategory(), equalTo(AuditingCategory.SYSTEM));
        assertThat("Summary is internationalised str", request.getSummary(), equalTo("i18n.jira.auditing.system.license.extend.evaluation"));
        assertNotNull(request.getObjectItem());
        assertThat("Associated Object type enum to be License", request.getObjectItem().getObjectType(), equalTo(AssociatedItem.Type.LICENSE));
        assertThat("Object name is User's name", request.getObjectItem().getObjectName(), equalTo(USER_NAME));
        assertThat("Type of license is Evaluation", request.getObjectItem().getParentName(), equalTo(AffectedLicense.TYPE_EVALUATION_LICENSE));

        Matcher[] changedValues =
                {
                        changedValue(I18N_ADMIN_LICENSE_ORGANISATION, ORGANIZATION),
                        changedValue(I18N_ADMIN_LICENSE_DATE_PURCHASED, PURCHASE_DATE),
                        // check that both licenses are listed
                        changedValue(I18N_ADMIN_LICENSE_SEN, SEN),
                        // in the changed values
                        changedValue(I18N_ADMIN_LICENSE_SEN, SEN_TWO),
                        changedValue(I18N_ADMIN_LICENSE_TYPE, DESCRIPTION),
                        changedValue(I18N_ADMIN_LICENSE_DATE_EXPIRED, MAINTENANCE_EXPIRE_DATE_STRING),
                        changedValue(I18N_ADMIN_SERVER_ID, "0.0.0.0"),
                        changedValue(I18N_ADMIN_LICENSE_USER_LIMIT, I18N_COMMON_WORDS_UNLIMITED)
                };

        Matcher<Iterable<? super ChangedValue>> hasChangedValues = CoreMatchers.hasItems(changedValues);
        assertThat(CHANGED_VALUES_RECORDED_AS_EXPECTED, request.getChangedValues(), hasChangedValues);
        assertThat("ChangeValue collection has 14 items (2xlicenses)", request.getChangedValues().size(), equalTo(14));
    }

    @Test
    public void testLimitNumberOfUsersInLicenseForRecordRequest() throws Exception {
        when(licenseDetails.isUnlimitedNumberOfUsers()).thenReturn(false);
        when(licenseDetails.getJiraLicense().getMaximumNumberOfUsers()).thenReturn(100);
        when(licenseDetails.getLicensedApplications()).thenReturn(licensedApplications);

        final LicenseChangedEvent event = new LicenseChangedEvent(Option.<LicenseDetails>none(), Option.option(licenseDetails));
        final RecordRequest request = systemAuditEventHandler.onLicenseChangedEvent(event);

        Matcher<Iterable<? super ChangedValue>> hasChangedValues = hasItem(changedValue(I18N_ADMIN_LICENSE_USER_LIMIT, "100"));
        assertThat(CHANGED_VALUES_RECORDED_AS_EXPECTED, request.getChangedValues(), hasChangedValues);
    }

    @Test
    public void testLicenseAddedEvent() throws Exception {
        when(licenseDetails.getLicensedApplications()).thenReturn(licensedApplications);

        final LicenseChangedEvent event = new LicenseChangedEvent(Option.<LicenseDetails>none(), Option.option(licenseDetails));
        final RecordRequest request = systemAuditEventHandler.onLicenseChangedEvent(event);

        assertEquals("i18n.jira.auditing.system.license.added", request.getSummary());
    }

    @Test
    public void testLicenseUpdatedEvent() throws Exception {
        when(licenseDetails.getLicensedApplications()).thenReturn(licensedApplications);

        final LicenseChangedEvent event = new LicenseChangedEvent(Option.<LicenseDetails>option(licenseDetails), Option.option(licenseDetails));
        final RecordRequest request = systemAuditEventHandler.onLicenseChangedEvent(event);

        assertEquals("i18n.jira.auditing.system.license.updated", request.getSummary());
    }

    @Test
    public void testLicenseRemovedEvent() throws Exception {
        when(licenseDetails.getLicensedApplications()).thenReturn(licensedApplications);

        final LicenseChangedEvent event = new LicenseChangedEvent(Option.option(licenseDetails), Option.<LicenseDetails>none());
        final RecordRequest request = systemAuditEventHandler.onLicenseChangedEvent(event);

        assertEquals("i18n.jira.auditing.system.license.removed", request.getSummary());
    }

    private BaseMatcher<ChangedValue> changedValue(
            @Nonnull final String name, @Nonnull final String to) {
        return new BaseMatcher<ChangedValue>() {
            @Override
            public boolean matches(final Object o) {
                if (!(o instanceof ChangedValue)) {
                    return false;
                }
                final ChangedValue changedValue = (ChangedValue) o;
                return name.equals(changedValue.getName())
                        && changedValue.getFrom() == null
                        && to.equals(changedValue.getTo());
            }

            @Override
            public void describeTo(final Description description) {
                description.appendText(new ChangedValueImpl(name, null, to).toString());
            }
        };
    }
}
