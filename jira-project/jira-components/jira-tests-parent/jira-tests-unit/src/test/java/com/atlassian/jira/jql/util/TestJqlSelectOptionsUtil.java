package com.atlassian.jira.jql.util;

import com.atlassian.jira.action.issue.customfields.option.MockOption;
import com.atlassian.jira.exception.DataAccessException;
import com.atlassian.jira.issue.customfields.manager.OptionsManager;
import com.atlassian.jira.issue.customfields.option.Option;
import com.atlassian.jira.issue.customfields.option.Options;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.issue.fields.config.FieldConfigScheme;
import com.atlassian.jira.issue.fields.config.manager.FieldConfigSchemeManager;
import com.atlassian.jira.jql.context.ClauseContext;
import com.atlassian.jira.jql.context.ClauseContextImpl;
import com.atlassian.jira.jql.context.FieldConfigSchemeClauseContextUtil;
import com.atlassian.jira.jql.context.QueryContext;
import com.atlassian.jira.jql.context.QueryContextImpl;
import com.atlassian.jira.jql.operand.QueryLiteral;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.collect.CollectionBuilder;
import org.apache.commons.collections.MultiHashMap;
import org.apache.commons.collections.MultiMap;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import static com.atlassian.jira.jql.operand.SimpleLiteralFactory.createLiteral;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestJqlSelectOptionsUtil {
    @Rule
    public final MethodRule initMockito = MockitoJUnit.rule();

    @Mock
    private OptionsManager optionsManager;
    @Mock
    private FieldConfigSchemeManager fieldConfigSchemeManager;
    @Mock
    private FieldConfigSchemeClauseContextUtil fieldConfigSchemeClauseContextUtil;
    @Mock
    private CustomField customField;
    @Mock
    private CustomField customField2;
    @Mock
    private FieldConfigScheme configScheme;
    @Mock
    private Option option;
    @Mock
    private FieldConfig fieldConfig;
    @Mock
    private FieldConfig fieldConfig1;
    @Mock
    private FieldConfig fieldConfig2;

    @Test
    public void testGetOptionsNoVisibleFieldConfigScheme() throws Exception {
        QueryContext queryContext = new QueryContextImpl(new ClauseContextImpl());
        when(fieldConfigSchemeClauseContextUtil.getFieldConfigSchemeFromContext(queryContext, customField)).thenReturn(null);

        final JqlSelectOptionsUtil util = new JqlSelectOptionsUtil(optionsManager, fieldConfigSchemeManager, fieldConfigSchemeClauseContextUtil);
        final List<Option> result = util.getOptions(customField, queryContext, createLiteral("10"), false);
        final List<Option> expectedResult = CollectionBuilder.<Option>newBuilder().asList();

        assertEquals(expectedResult, result);
    }

    @Test
    public void testGetOptionsHappyPath() throws Exception {
        final Option option1 = new MockOption(null, null, null, "1", null, 1L);
        final Option option2 = new MockOption(null, null, null, "1", null, 1L);
        final Option option3 = new MockOption(null, null, null, "1", null, 1L);

        QueryContext queryContext = new QueryContextImpl(new ClauseContextImpl());

        when(fieldConfigSchemeClauseContextUtil.getFieldConfigSchemeFromContext(queryContext, customField)).thenReturn(configScheme);

        final AtomicBoolean customFieldOptionsCalled = new AtomicBoolean(false);
        final AtomicBoolean schemeOptionsCalled = new AtomicBoolean(false);
        final JqlSelectOptionsUtil util = new JqlSelectOptionsUtil(optionsManager, fieldConfigSchemeManager, fieldConfigSchemeClauseContextUtil) {
            @Override
            public List<Option> getOptions(final CustomField customField, final QueryLiteral literal, final boolean checkOptionIds) {
                customFieldOptionsCalled.set(true);
                return CollectionBuilder.newBuilder(option2, option3).asList();
            }

            @Override
            public List<Option> getOptionsForScheme(final FieldConfigScheme fieldConfigScheme) {
                schemeOptionsCalled.set(true);
                return CollectionBuilder.newBuilder(option1, option2).asList();
            }
        };

        final List<Option> result = util.getOptions(customField, queryContext, createLiteral("10"), false);
        final List<Option> expectedResult = CollectionBuilder.<Option>newBuilder(option2).asList();
        assertEquals(expectedResult, result);
        assertTrue(customFieldOptionsCalled.get());
        assertTrue(schemeOptionsCalled.get());
    }

    @Test
    public void testGetOptionsVisibleToUser() throws Exception {
        QueryLiteral literal = createLiteral(10L);

        final MockOption option1 = new MockOption(null, null, null, null, null, 10L);
        final MockOption option2 = new MockOption(null, null, null, null, null, 20L);

        final AtomicBoolean getCalled = new AtomicBoolean(false);
        final AtomicInteger visibleCalled = new AtomicInteger(0);
        final JqlSelectOptionsUtil util = new JqlSelectOptionsUtil(optionsManager, fieldConfigSchemeManager, fieldConfigSchemeClauseContextUtil) {
            @Override
            public List<Option> getOptions(final CustomField customField, final QueryLiteral literal, final boolean checkOptionIds) {
                getCalled.set(true);
                return CollectionBuilder.<Option>newBuilder(option1, option2).asList();
            }

            @Override
            boolean optionIsVisible(final Option option, final ApplicationUser user) {
                visibleCalled.incrementAndGet();
                return option.getOptionId().equals(10L);
            }
        };

        final List<Option> result = util.getOptions(customField, (ApplicationUser) null, literal, false);
        final List<Option> expectedResult = CollectionBuilder.<Option>newBuilder(option1).asList();

        assertEquals(expectedResult, result);
        assertEquals(2, visibleCalled.get());
        assertTrue(getCalled.get());
    }

    @Test
    public void testGetOptionsEmptyLiteral() throws Exception {
        QueryContext queryContext = new QueryContextImpl(new ClauseContextImpl());
        QueryLiteral literal = new QueryLiteral();

        final JqlSelectOptionsUtil util = new JqlSelectOptionsUtil(optionsManager, fieldConfigSchemeManager, fieldConfigSchemeClauseContextUtil);
        final List<Option> result1 = util.getOptions(customField, queryContext, literal, false);
        final List<Option> result2 = util.getOptions(customField, literal, false);

        assertTrue(result1.isEmpty());
        assertEquals(1, result2.size());
        assertTrue(result2.contains(null));
    }

    @Test
    public void testOptionIsVisibleToUserTrue() throws Exception {
        when(option.getRelatedCustomField()).thenReturn(fieldConfig);
        when(fieldConfigSchemeManager.getConfigSchemeForFieldConfig(fieldConfig)).thenReturn(configScheme);
        final ClauseContext context = ClauseContextImpl.createGlobalClauseContext();
        when(fieldConfigSchemeClauseContextUtil.getContextForConfigScheme(null, configScheme)).thenReturn(context);

        final JqlSelectOptionsUtil util = new JqlSelectOptionsUtil(optionsManager, fieldConfigSchemeManager, fieldConfigSchemeClauseContextUtil);
        assertTrue(util.optionIsVisible(option, null));
        assertTrue(util.optionIsVisible(option, null));
    }

    @Test
    public void testOptionIsVisibleNullScheme() throws Exception {
        when(option.getRelatedCustomField()).thenReturn(fieldConfig);
        when(fieldConfigSchemeManager.getConfigSchemeForFieldConfig(fieldConfig)).thenReturn(null);

        final JqlSelectOptionsUtil util = new JqlSelectOptionsUtil(optionsManager, fieldConfigSchemeManager, fieldConfigSchemeClauseContextUtil);

        assertFalse(util.optionIsVisible(option, null));
    }

    @Test
    public void testOptionIsVisibleToUserFalse() throws Exception {
        when(option.getRelatedCustomField()).thenReturn(fieldConfig);
        when(fieldConfigSchemeManager.getConfigSchemeForFieldConfig(fieldConfig)).thenReturn(configScheme);
        final ClauseContextImpl context = new ClauseContextImpl();
        when(fieldConfigSchemeClauseContextUtil.getContextForConfigScheme(null, configScheme)).thenReturn(context);

        final JqlSelectOptionsUtil util = new JqlSelectOptionsUtil(optionsManager, fieldConfigSchemeManager, fieldConfigSchemeClauseContextUtil);

        assertFalse(util.optionIsVisible(option, null));
    }

    @Test
    public void testGetOptionsLongIdsEnabledIdFound() throws Exception {
        when(fieldConfig1.getCustomField()).thenReturn(customField);
        final MockOption option1 = new MockOption(null, null, null, null, fieldConfig1, 10L);
        QueryLiteral literal = createLiteral(10L);
        when(optionsManager.findByOptionId(10L)).thenReturn(option1);

        final JqlSelectOptionsUtil jqlSelectOptionsUtil = new JqlSelectOptionsUtil(optionsManager, fieldConfigSchemeManager, fieldConfigSchemeClauseContextUtil);
        final List<Option> result = jqlSelectOptionsUtil.getOptions(customField, literal, true);
        final List<Option> expectedResult = CollectionBuilder.<Option>newBuilder(option1).asList();

        assertEquals(expectedResult, result);
    }

    @Test
    public void testGetOptionsLongIdsEnabledIdFoundWrongCustomField() throws Exception {
        when(fieldConfig1.getCustomField()).thenReturn(customField);
        when(fieldConfig2.getCustomField()).thenReturn(customField);

        final MockOption option1 = new MockOption(null, null, null, null, fieldConfig1, 10L);
        QueryLiteral literal = createLiteral(10L);

        final MockOption option2 = new MockOption(null, null, null, null, fieldConfig2, 10L);

        when(optionsManager.findByOptionId(10L)).thenReturn(option1);
        when(optionsManager.findByOptionValue("10")).thenReturn(CollectionBuilder.<Option>newBuilder(option1).asList());

        final JqlSelectOptionsUtil jqlSelectOptionsUtil = new JqlSelectOptionsUtil(optionsManager, fieldConfigSchemeManager, fieldConfigSchemeClauseContextUtil);
        final List<Option> result = jqlSelectOptionsUtil.getOptions(customField, literal, true);
        final List<Option> expectedResult = CollectionBuilder.<Option>newBuilder(option2).asList();

        assertEquals(expectedResult, result);
    }

    @Test
    public void testGetOptionsLongIdsEnabledIdFoundNoCustomField() throws Exception {
        when(fieldConfig1.getCustomField()).thenReturn(customField);
        when(fieldConfig2.getCustomField()).thenReturn(customField);

        final MockOption option1 = new MockOption(null, null, null, null, fieldConfig1, 10L);
        QueryLiteral literal = createLiteral(10L);

        final MockOption option2 = new MockOption(null, null, null, null, fieldConfig2, 10L);

        when(optionsManager.findByOptionId(10L)).thenReturn(option1);
        when(optionsManager.findByOptionValue("10")).thenReturn(CollectionBuilder.<Option>newBuilder(option1).asList());

        final JqlSelectOptionsUtil jqlSelectOptionsUtil = new JqlSelectOptionsUtil(optionsManager, fieldConfigSchemeManager, fieldConfigSchemeClauseContextUtil);
        final List<Option> result = jqlSelectOptionsUtil.getOptions(customField, literal, true);
        final List<Option> expectedResult = CollectionBuilder.<Option>newBuilder(option2).asList();
        assertEquals(expectedResult, result);
    }

    @Test
    public void testGetOptionsLongIdsEnabledIdNotFoundValueOption() throws Exception {
        when(fieldConfig1.getCustomField()).thenReturn(customField);

        final MockOption option = new MockOption(null, null, null, null, fieldConfig1, 10L);
        QueryLiteral literal = createLiteral(10L);

        when(optionsManager.findByOptionId(10L)).thenReturn(null);
        when(optionsManager.findByOptionValue("10")).thenReturn((List) CollectionBuilder.newBuilder(option).asList());

        final JqlSelectOptionsUtil jqlSelectOptionsUtil = new JqlSelectOptionsUtil(optionsManager, fieldConfigSchemeManager, fieldConfigSchemeClauseContextUtil);
        final List<Option> result = jqlSelectOptionsUtil.getOptions(customField, literal, true);
        final List<Option> expectedResult = CollectionBuilder.<Option>newBuilder(option).asList();

        assertEquals(expectedResult, result);
    }

    @Test
    public void testGetOptionsLongIdsEnabledIdNotFoundValueOptionWrongField() throws Exception {
        when(fieldConfig1.getCustomField()).thenReturn(customField2);

        final MockOption option = new MockOption(null, null, null, null, fieldConfig1, 10L);
        QueryLiteral literal = createLiteral(10L);

        when(optionsManager.findByOptionId(10L)).thenReturn(null);
        when(optionsManager.findByOptionValue("10")).thenReturn((List) CollectionBuilder.newBuilder(option).asList());

        final JqlSelectOptionsUtil jqlSelectOptionsUtil = new JqlSelectOptionsUtil(optionsManager, fieldConfigSchemeManager, fieldConfigSchemeClauseContextUtil);
        final List<Option> result = jqlSelectOptionsUtil.getOptions(customField, literal, true);
        final List<Option> expectedResult = CollectionBuilder.<Option>newBuilder().asList();

        assertEquals(expectedResult, result);
    }

    @Test
    public void testGetOptionsLongIdsNotEnabled() throws Exception {
        when(fieldConfig1.getCustomField()).thenReturn(customField);

        final MockOption option = new MockOption(null, null, null, null, fieldConfig1, 10L);
        QueryLiteral literal = createLiteral(10L);

        when(optionsManager.findByOptionValue("10")).thenReturn((List) CollectionBuilder.newBuilder(option).asList());

        final JqlSelectOptionsUtil jqlSelectOptionsUtil = new JqlSelectOptionsUtil(optionsManager, fieldConfigSchemeManager, fieldConfigSchemeClauseContextUtil);
        final List<Option> result = jqlSelectOptionsUtil.getOptions(customField, literal, false);
        final List<Option> expectedResult = CollectionBuilder.<Option>newBuilder(option).asList();

        assertEquals(expectedResult, result);
    }

    @Test
    public void testGetOptionsLongIdsNotEnabledNonFound() throws Exception {
        QueryLiteral literal = createLiteral(10L);

        when(optionsManager.findByOptionValue("10")).thenReturn((List) CollectionBuilder.newBuilder().asList());

        final JqlSelectOptionsUtil jqlSelectOptionsUtil = new JqlSelectOptionsUtil(optionsManager, fieldConfigSchemeManager, fieldConfigSchemeClauseContextUtil);
        final List<Option> result = jqlSelectOptionsUtil.getOptions(customField, literal, false);
        final List<Option> expectedResult = CollectionBuilder.<Option>newBuilder().asList();

        assertEquals(expectedResult, result);
    }

    @Test
    public void testGetOptionsStringIdsNotEnabled() throws Exception {
        when(fieldConfig1.getCustomField()).thenReturn(customField);

        final MockOption option = new MockOption(null, null, null, null, fieldConfig1, 10L);
        QueryLiteral literal = createLiteral("value");

        when(optionsManager.findByOptionValue("value")).thenReturn((List) CollectionBuilder.newBuilder(option).asList());

        final JqlSelectOptionsUtil jqlSelectOptionsUtil = new JqlSelectOptionsUtil(optionsManager, fieldConfigSchemeManager, fieldConfigSchemeClauseContextUtil);
        final List<Option> result = jqlSelectOptionsUtil.getOptions(customField, literal, false);
        final List<Option> expectedResult = CollectionBuilder.<Option>newBuilder(option).asList();

        assertEquals(expectedResult, result);
    }

    @Test
    public void testGetOptionsStringIdsNotEnabledNotFound() throws Exception {
        final MockOption option = new MockOption(null, null, null, null, null, 10L);
        QueryLiteral literal = createLiteral("value");

        when(optionsManager.findByOptionValue("value")).thenReturn((List) CollectionBuilder.newBuilder().asList());

        final JqlSelectOptionsUtil jqlSelectOptionsUtil = new JqlSelectOptionsUtil(optionsManager, fieldConfigSchemeManager, fieldConfigSchemeClauseContextUtil);
        final List<Option> result = jqlSelectOptionsUtil.getOptions(customField, literal, false);
        final List<Option> expectedResult = CollectionBuilder.<Option>newBuilder().asList();

        assertEquals(expectedResult, result);
    }

    @Test
    public void testGetOptionsStringIdsEnabledInvalidId() throws Exception {
        QueryLiteral literal = createLiteral("value");

        when(optionsManager.findByOptionValue("value")).thenReturn((List) CollectionBuilder.newBuilder().asList());

        final JqlSelectOptionsUtil jqlSelectOptionsUtil = new JqlSelectOptionsUtil(optionsManager, fieldConfigSchemeManager, fieldConfigSchemeClauseContextUtil);
        final List<Option> result = jqlSelectOptionsUtil.getOptions(customField, literal, true);
        final List<Option> expectedResult = CollectionBuilder.<Option>newBuilder().asList();

        assertEquals(expectedResult, result);
    }

    @Test
    public void testGetOptionsStringIdsEnabledFoundById() throws Exception {
        when(fieldConfig1.getCustomField()).thenReturn(customField);

        final MockOption option = new MockOption(null, null, null, null, fieldConfig1, 10L);
        QueryLiteral literal = createLiteral("10");

        when(optionsManager.findByOptionValue("10")).thenReturn((List) CollectionBuilder.newBuilder().asList());
        when(optionsManager.findByOptionId(10L)).thenReturn(option);

        final JqlSelectOptionsUtil jqlSelectOptionsUtil = new JqlSelectOptionsUtil(optionsManager, fieldConfigSchemeManager, fieldConfigSchemeClauseContextUtil);
        final List<Option> result = jqlSelectOptionsUtil.getOptions(customField, literal, true);
        final List<Option> expectedResult = CollectionBuilder.<Option>newBuilder(option).asList();

        assertEquals(expectedResult, result);
    }

    @Test
    public void testGetOptionsStringIdsEnabledFoundByIdButNoCustomField() throws Exception {
        when(fieldConfig1.getCustomField()).thenThrow(new DataAccessException("blarg"));

        final MockOption option = new MockOption(null, null, null, null, fieldConfig1, 10L);
        QueryLiteral literal = createLiteral("10");

        when(optionsManager.findByOptionValue("10")).thenReturn((List) CollectionBuilder.newBuilder().asList());
        when(optionsManager.findByOptionId(10L)).thenReturn(option);

        final JqlSelectOptionsUtil jqlSelectOptionsUtil = new JqlSelectOptionsUtil(optionsManager, fieldConfigSchemeManager, fieldConfigSchemeClauseContextUtil);
        final List<Option> result = jqlSelectOptionsUtil.getOptions(customField, literal, true);
        final List<Option> expectedResult = CollectionBuilder.<Option>newBuilder().asList();

        assertEquals(expectedResult, result);
    }

    @Test
    public void testGetOptionsForScheme() throws Exception {
        final FieldConfig config1 = mock(FieldConfig.class);
        final FieldConfig config2 = mock(FieldConfig.class);
        final FieldConfigScheme fieldConfigScheme = mock(FieldConfigScheme.class);

        final Option option1 = mock(Option.class);
        final Option option2 = mock(Option.class);
        final Option option3 = mock(Option.class);

        when(option1.getChildOptions()).thenReturn(CollectionBuilder.newBuilder(option3).asList());
        when(option2.getChildOptions()).thenReturn((List) CollectionBuilder.newBuilder().asList());

        Options options1 = new MyOptions(CollectionBuilder.newBuilder(option1).asList());
        Options options2 = new MyOptions(CollectionBuilder.newBuilder(option2).asList());

        MultiMap map = new MultiHashMap();
        map.put(config1, null);
        map.put(config2, null);

        when(fieldConfigScheme.getConfigsByConfig()).thenReturn(map);
        when(optionsManager.getOptions(config1)).thenReturn(options1);
        when(optionsManager.getOptions(config2)).thenReturn(options2);

        final JqlSelectOptionsUtil util = new JqlSelectOptionsUtil(optionsManager, fieldConfigSchemeManager, fieldConfigSchemeClauseContextUtil);
        final Set<Option> result = new HashSet(util.getOptionsForScheme(fieldConfigScheme));
        final Set<Option> expectedResult = CollectionBuilder.newBuilder(option1, option3, option2).asHashSet();

        assertEquals(result, expectedResult);
    }

    @Test
    public void testGetOptionByIdFound() throws Exception {
        when(optionsManager.findByOptionId(10L)).thenReturn(option);

        final JqlSelectOptionsUtil jqlSelectOptionsUtil = new JqlSelectOptionsUtil(optionsManager, fieldConfigSchemeManager, fieldConfigSchemeClauseContextUtil);
        final Option result = jqlSelectOptionsUtil.getOptionById(10L);

        assertEquals(option, result);
    }

    @Test
    public void testGetOptionByIdNotFound() throws Exception {
        when(optionsManager.findByOptionId(10L)).thenReturn(null);

        final JqlSelectOptionsUtil jqlSelectOptionsUtil = new JqlSelectOptionsUtil(optionsManager, fieldConfigSchemeManager, fieldConfigSchemeClauseContextUtil);
        final Option result = jqlSelectOptionsUtil.getOptionById(10L);

        assertNull(result);
    }

    static class MyOptions extends ArrayList<Option> implements Options {
        public MyOptions(List<Option> options) {
            this.addAll(options);
        }

        public List<Option> getRootOptions() {
            return null;
        }

        public Option getOptionById(final Long optionId) {
            return null;
        }

        public Option getOptionForValue(final String value, final Long parentOptionId) {
            return null;
        }

        public Option addOption(final Option parent, final String value) {
            return null;
        }

        public void removeOption(final Option option) {
        }

        public void moveToStartSequence(final Option option) {
        }

        public void incrementSequence(final Option option) {
        }

        public void decrementSequence(final Option option) {
        }

        public void moveToLastSequence(final Option option) {
        }

        public FieldConfig getRelatedFieldConfig() {
            return null;
        }

        public void sortOptionsByValue(final Option parentOption) {
        }

        public void moveOptionToPosition(final Map<Integer, Option> positionsToOptions) {
        }

        public void setValue(final Option option, final String value) {
        }

        public void enableOption(final Option option) {
        }

        public void disableOption(final Option option) {
        }
    }
}
