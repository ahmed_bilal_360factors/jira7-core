package com.atlassian.jira.imports.project.taskprogress;

import org.junit.Test;

import java.time.Clock;
import java.time.Instant;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestThrottlingTaskProgressProcessor {
    @Test
    public void shouldHave2SecondsThrottling() throws Exception {
        //given
        final TaskProgressProcessor taskProgressProcessorMock = mock(TaskProgressProcessor.class);
        final Instant startingMoment = Instant.now();
        final Clock clockMock = mock(Clock.class);
        when(clockMock.instant()).thenReturn(startingMoment);
        final ThrottlingTaskProgressProcessor progressWriter = new ThrottlingTaskProgressProcessor(taskProgressProcessorMock, clockMock);

        //when
        when(clockMock.instant()).thenReturn(startingMoment.plusMillis(1));
        progressWriter.processTaskProgress("name", 100, 100, 1);        //right after the creation. Must be processed
        verify(taskProgressProcessorMock, times(1)).processTaskProgress(any(), anyInt(), anyLong(), anyLong());

        when(clockMock.instant()).thenReturn(startingMoment.plusSeconds(1));
        progressWriter.processTaskProgress("name", 100, 100, 2);        //inside 2 sec period. Must be skipped
        verify(taskProgressProcessorMock, times(1)).processTaskProgress(any(), anyInt(), anyLong(), anyLong());

        when(clockMock.instant()).thenReturn(startingMoment.plusSeconds(3));
        progressWriter.processTaskProgress("name", 100, 100, 3);        //after 2 sec period. Must be processed
        verify(taskProgressProcessorMock, times(2)).processTaskProgress(any(), anyInt(), anyLong(), anyLong());
    }

}