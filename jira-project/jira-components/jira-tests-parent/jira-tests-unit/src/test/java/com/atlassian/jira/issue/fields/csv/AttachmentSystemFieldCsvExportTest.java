package com.atlassian.jira.issue.fields.csv;

import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.AttachmentManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.attachment.Attachment;
import com.atlassian.jira.issue.export.FieldExportParts;
import com.atlassian.jira.issue.fields.AttachmentSystemField;
import com.atlassian.jira.issue.fields.csv.util.StubCsvDateFormatter;
import com.atlassian.jira.issue.fields.rest.json.beans.JiraBaseUrls;
import com.atlassian.jira.issue.views.util.csv.CsvDateFormatter;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.InitMockitoMocks;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.NoopI18nHelper;
import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import edu.umd.cs.findbugs.annotations.Nullable;
import org.hamcrest.Matchers;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.mockito.Mock;
import org.mockito.Spy;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class AttachmentSystemFieldCsvExportTest {
    private static final String BASE_URL = "http://jira-base-url/";
    private static final Date NOW = DateTime.now().toDate();

    @Rule
    public TestRule initMockito = new InitMockitoMocks(this);

    @Mock
    private AttachmentManager attachmentManager;
    @Spy
    private final CsvDateFormatter dateFormatter = new StubCsvDateFormatter();
    @Mock
    private JiraBaseUrls jiraBaseUrls;
    @Mock
    private Issue issue;
    @Mock
    @AvailableInContainer
    private ApplicationProperties applicationProperties;

    @Mock
    private JiraAuthenticationContext authenticationContext;

    private I18nHelper i18nHelper = new NoopI18nHelper();

    private AttachmentSystemField field;

    @Before
    public void setUp() {
        field = new AttachmentSystemField(null, applicationProperties, attachmentManager, authenticationContext, null, jiraBaseUrls, null,
                null, null, null, null, dateFormatter);
        when(jiraBaseUrls.baseUrl()).thenReturn(BASE_URL);
        when(applicationProperties.getEncoding()).thenReturn("UTF-8");
        when(authenticationContext.getI18nHelper()).thenReturn(i18nHelper);
    }

    @Test
    public void allAttachmentsAreReturnedAsSeparateValues() throws UnsupportedEncodingException {
        Attachment at1 = attachment(1, "at1", null, null);
        Attachment at2 = attachment(2, "at2", null, null);
        when(attachmentManager.getAttachments(any(Issue.class))).thenReturn(ImmutableList.of(at1, at2));

        assertExported(
                exported(1, "at1", null, null),
                exported(2, "at2", null, null)
        );
    }

    @Test
    public void attachmentFileNameIsEncoded() throws UnsupportedEncodingException {
        Attachment att = attachment(1, "Knausgård", null, null);
        when(attachmentManager.getAttachments(issue)).thenReturn(ImmutableList.of(att));
        assertExported(exported(1, "Knausgård", null, null));
    }

    @Test
    public void dateIsFormattedWithCsvFormatter() throws UnsupportedEncodingException {
        Attachment att = attachment(1, "a", new Timestamp(NOW.getTime()), null);
        when(attachmentManager.getAttachments(issue)).thenReturn(ImmutableList.of(att));
        assertExported(exported(1, "a", new Timestamp(NOW.getTime()), null));

    }

    @Test
    public void attachmentOwnerIsExported() throws UnsupportedEncodingException {
        Attachment att = attachment(1, "a", new Timestamp(NOW.getTime()), "joey");
        when(attachmentManager.getAttachments(issue)).thenReturn(ImmutableList.of(att));
        assertExported(exported(1, "a", new Timestamp(NOW.getTime()), "joey"));

    }

    private String exported(final int id, final String name, final Date created, final String username) throws UnsupportedEncodingException {
        List<String> parts = Lists.newArrayList();
        if (created != null) {
            parts.add(dateFormatter.formatDateTime(created));
        }
        if (username != null) {
            parts.add(username);
        }

        parts.add(name);
        parts.add(String.format("%s/secure/attachment/%s/%s", BASE_URL, id, URLEncoder.encode(name, "UTF-8")));
        return Joiner.on(";").join(parts);
    }

    private void assertExported(String... values) {
        FieldExportParts representation = field.getRepresentationFromIssue(issue);
        assertThat(representation.getParts(), hasSize(1));
        assertThat(representation.getParts().get(0).getValues()::iterator, Matchers.contains(values));
    }

    private Attachment attachment(long id, final String attachmentName, @Nullable Timestamp createdDate, @Nullable String authorUsername) {
        Attachment attachment = mock(Attachment.class);
        when(attachment.getFilename()).thenReturn(attachmentName);
        when(attachment.getId()).thenReturn(id);
        if (authorUsername != null) {
            when(attachment.getAuthorObject()).thenReturn(new MockApplicationUser(authorUsername));
        }
        when(attachment.getCreated()).thenReturn(createdDate);
        return attachment;
    }
}
