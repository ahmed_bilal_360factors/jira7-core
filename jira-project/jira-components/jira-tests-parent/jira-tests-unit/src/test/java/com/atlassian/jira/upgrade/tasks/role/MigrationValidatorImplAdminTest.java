package com.atlassian.jira.upgrade.tasks.role;

import com.atlassian.collectors.CollectorsUtil;
import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.jira.application.ApplicationKeys;
import com.atlassian.jira.test.util.lic.core.CoreLicenses;
import com.atlassian.jira.test.util.lic.software.SoftwareLicenses;
import com.atlassian.jira.upgrade.tasks.role.MigrationValidatorImpl.MigrationValidationFailedException;
import com.atlassian.jira.user.MockGroup;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import org.hamcrest.Matcher;
import org.hamcrest.core.StringContains;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Collection;
import java.util.Set;

import static com.atlassian.jira.upgrade.tasks.role.TestUtils.toLicense;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.core.StringContains.containsString;
import static org.mockito.Mockito.when;

public class MigrationValidatorImplAdminTest {
    @Rule
    public MockitoRule initMocks = MockitoJUnit.rule();

    private static final MockGroup ADMIN_ONE = new MockGroup("AdminOne");
    private static final MockGroup ADMIN_TWO = new MockGroup("AdminTwo");
    private static final MockGroup ADMIN_THREE = new MockGroup("AdminThree");
    private static final MockGroup SOFTWARE_ONE = new MockGroup("SoftwareOne");
    private static final MockGroup SOFTWARE_TWO = new MockGroup("SoftwareTwo");
    private final License swLicense = toLicense(SoftwareLicenses.LICENSE_SOFTWARE);
    private final License coreLicense = toLicense(CoreLicenses.LICENSE_CORE);
    private final Licenses licenses = new Licenses(ImmutableSet.of(swLicense, coreLicense));

    @Mock
    private GlobalPermissionDao globalPermissionDao;
    @Mock
    private Jira6xServiceDeskLicenseProvider licenseSupplier;
    @Mock
    private MigrationGroupService migrationGroupService;

    private final Set<Group> adminGroups = ImmutableSet.of(ADMIN_ONE, ADMIN_TWO, ADMIN_THREE);
    private final Set<Group> useGroups = ImmutableSet.of(SOFTWARE_ONE, SOFTWARE_TWO);
    private final Set<Group> useAndAdminGroups = ImmutableSet.of(SOFTWARE_ONE, SOFTWARE_TWO, ADMIN_ONE, ADMIN_TWO, ADMIN_THREE);
    private final ImmutableSet<String> adminGroupNames = adminGroups.stream()
            .map(Group::getName)
            .collect(CollectorsUtil.toImmutableSet());

    private MigrationValidator validator;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void setUp() throws Exception {
        validator = new MigrationValidatorImpl(this.globalPermissionDao, licenseSupplier, this.migrationGroupService);
    }

    @Test
    public void validationShouldPassIfNoAdministratorGroupsAreDefaultGroups() {
        // given
        setupGlobalPermissionGroups();
        final ApplicationRole SoftwareRole = ApplicationRole.forKey(ApplicationKeys.SOFTWARE)
                .withGroups(useAndAdminGroups, useGroups);
        final ApplicationRole coreRole = ApplicationRole.forKey(ApplicationKeys.CORE)
                .withGroups(useAndAdminGroups, useGroups);

        MigrationState state = migrationStateOf(licenses, new ApplicationRoles(ImmutableList.of(SoftwareRole, coreRole)));
        // when
        validator.validate(TestUtils.emptyState(), state);
        // then
        // there is no exception thrown
    }

    @Test
    public void administratorGroupAsDefaultFailsValidation() {
        thrown.expect(MigrationValidationFailedException.class);
        thrown.expectMessage(stringContainsInAnyOrder(adminGroupNames));
        // given

        setupGlobalPermissionGroups();
        final ApplicationRole SoftwareRole = ApplicationRole.forKey(ApplicationKeys.SOFTWARE)
                .withGroups(useAndAdminGroups, useAndAdminGroups);
        final ApplicationRole coreRole = ApplicationRole.forKey(ApplicationKeys.CORE)
                .withGroups(useAndAdminGroups, useGroups);

        MigrationState state = migrationStateOf(licenses, new ApplicationRoles(ImmutableList.of(SoftwareRole, coreRole)));
        // when
        validator.validate(TestUtils.emptyState(), state);
        // then
        // exception is thrown
    }

    @Test
    public void noAdministratorGroupIsFine() {
        // given
        when(globalPermissionDao.groupsWithAdminPermission()).thenReturn(ImmutableSet.of());
        when(globalPermissionDao.groupsWithUsePermission()).thenReturn(useGroups);
        final ApplicationRole SoftwareRole = ApplicationRole.forKey(ApplicationKeys.SOFTWARE)
                .withGroups(useGroups, useGroups);
        final ApplicationRole coreRole = ApplicationRole.forKey(ApplicationKeys.CORE)
                .withGroups(useGroups, useGroups);
        MigrationState state = migrationStateOf(licenses, new ApplicationRoles(ImmutableList.of(SoftwareRole, coreRole)));

        // when
        validator.validate(TestUtils.emptyState(), state);

        // then
        // there is no exception thrown
    }

    @Test
    public void singleAdministratorGroupInDefaultShallFailVaildation() {

        // given
        when(globalPermissionDao.groupsWithAdminPermission()).thenReturn(ImmutableSet.of(ADMIN_ONE));
        when(globalPermissionDao.groupsWithUsePermission()).thenReturn(useGroups);
        final ApplicationRole softwareRole = ApplicationRole.forKey(ApplicationKeys.SOFTWARE)
                .withGroups(adminGroups, adminGroups);
        final ApplicationRole coreRole = ApplicationRole.forKey(ApplicationKeys.CORE)
                .withGroups(useAndAdminGroups, useAndAdminGroups);

        MigrationState state = migrationStateOf(licenses, new ApplicationRoles(ImmutableList.of(softwareRole, coreRole)));

        // expect
        thrown.expect(MigrationValidationFailedException.class);
        thrown.expectMessage(containsString(ADMIN_ONE.getName()));

        // when
        validator.validate(TestUtils.emptyState(), state);
    }

    @Test
    public void administratorGroupInDefaultShallFailValidationWhenTwoRolesHaveSameGroups() {
        // given
        thrown.expect(MigrationValidationFailedException.class);
        thrown.expectMessage(stringContainsInAnyOrder(adminGroupNames));

        setupGlobalPermissionGroups();
        final ApplicationRole softwareRole = ApplicationRole.forKey(ApplicationKeys.SOFTWARE)
                .withGroups(useAndAdminGroups, adminGroups);
        final ApplicationRole coreRole = ApplicationRole.forKey(ApplicationKeys.CORE)
                .withGroups(useAndAdminGroups, adminGroups);

        MigrationState state = migrationStateOf(licenses, new ApplicationRoles(ImmutableList.of(softwareRole, coreRole)));

        // when
        validator.validate(TestUtils.emptyState(), state);

        // then
        // exception is thrown
    }

    @Test
    public void administratorGroupInDefaultShallFailValidationWhenTwoRolesHaveDifferentGroups() {
        // given
        thrown.expect(MigrationValidationFailedException.class);
        thrown.expectMessage(stringContainsInAnyOrder(ImmutableList.of(ADMIN_THREE.getName(), ADMIN_TWO.getName())));

        setupGlobalPermissionGroups();
        final ApplicationRole softwareRole = ApplicationRole.forKey(ApplicationKeys.SOFTWARE)
                .withGroups(useAndAdminGroups, ImmutableSet.of(SOFTWARE_ONE, ADMIN_THREE));
        final ApplicationRole coreRole = ApplicationRole.forKey(ApplicationKeys.CORE)
                .withGroups(useAndAdminGroups, ImmutableSet.of(SOFTWARE_TWO, ADMIN_TWO));

        MigrationState state = migrationStateOf(licenses, new ApplicationRoles(ImmutableList.of(softwareRole, coreRole)));

        // when
        validator.validate(TestUtils.emptyState(), state);

        // then
        // exception is thrown
    }

    @Test
    public void preExistingDefaultAdminGroupsAreValid() {
        // given
        setupGlobalPermissionGroups();
        final ApplicationRole initialSoftwareRole = ApplicationRole.forKey(ApplicationKeys.SOFTWARE)
                .withGroups(ImmutableSet.of(SOFTWARE_ONE, SOFTWARE_TWO, ADMIN_THREE),
                        ImmutableSet.of(ADMIN_THREE));
        final ApplicationRole softwareRole = ApplicationRole.forKey(ApplicationKeys.SOFTWARE)
                .withGroups(ImmutableSet.of(SOFTWARE_ONE, SOFTWARE_TWO, ADMIN_THREE),
                        ImmutableSet.of(SOFTWARE_ONE, ADMIN_THREE));
        final ApplicationRole coreRole = ApplicationRole.forKey(ApplicationKeys.CORE)
                .withGroups(useAndAdminGroups, ImmutableSet.of(SOFTWARE_TWO));

        MigrationState originalState = migrationStateOf(licenses, new ApplicationRoles(ImmutableList.of(initialSoftwareRole)));
        MigrationState state = migrationStateOf(licenses, new ApplicationRoles(ImmutableList.of(softwareRole, coreRole)));

        // when
        validator.validate(originalState, state);

        // then
        // there is no exceptionn
    }

    private void setupGlobalPermissionGroups() {
        when(globalPermissionDao.groupsWithAdminPermission()).thenReturn(adminGroups);
        when(globalPermissionDao.groupsWithUsePermission()).thenReturn(useAndAdminGroups);
    }

    private static MigrationState migrationStateOf(Licenses licenses, ApplicationRoles roles) {
        return new MigrationState(licenses, roles, ImmutableList.<Runnable>of(), new MigrationLogImpl());
    }

    private Matcher<String> stringContainsInAnyOrder(Collection<String> s) {

        return allOf(s.stream().map(StringContains::containsString).collect(CollectorsUtil.toImmutableList()));
    }
}
