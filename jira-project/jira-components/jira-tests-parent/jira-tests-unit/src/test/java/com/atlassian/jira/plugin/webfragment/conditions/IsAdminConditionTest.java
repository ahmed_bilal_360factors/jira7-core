package com.atlassian.jira.plugin.webfragment.conditions;

import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.MockApplicationUser;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static java.util.Collections.emptyMap;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class IsAdminConditionTest {
    @Mock
    private JiraAuthenticationContext authenticationContext;

    @Mock
    private GlobalPermissionManager globalPermissionManager;

    private IsAdminCondition condition;

    @Before
    public void setup() {
        condition = new IsAdminCondition(authenticationContext, globalPermissionManager);
    }

    @Test
    public void shouldNotDisplayIfUserIsNotLoggedIn() {
        when(authenticationContext.getLoggedInUser()).thenReturn(null);

        final boolean result = condition.shouldDisplay(emptyMap());
        assertFalse(result);
    }

    @Test
    public void shouldNotDisplayIfUserDoesNotHaveAdministerPermission() {
        MockApplicationUser appUser = new MockApplicationUser("user");
        when(authenticationContext.getLoggedInUser()).thenReturn(appUser);
        when(globalPermissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, appUser)).thenReturn(false);

        final boolean result = condition.shouldDisplay(emptyMap());
        assertFalse(result);
    }

    @Test
    public void shouldDisplayIfUserHasAdministerPermission() {
        MockApplicationUser appUser = new MockApplicationUser("user");
        when(authenticationContext.getLoggedInUser()).thenReturn(appUser);
        when(globalPermissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, appUser)).thenReturn(true);

        final boolean result = condition.shouldDisplay(emptyMap());
        assertTrue(result);
    }
}