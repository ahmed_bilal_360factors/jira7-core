package com.atlassian.jira.issue.views.conditions;

import com.atlassian.jira.config.FeatureManager;
import com.google.common.collect.ImmutableMap;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Map;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

public class TestIsCsvExportEnabledCondition {

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private FeatureManager featureManager;

    private IsCsvExportEnabledCondition condition;
    private Map<String, Object> context = ImmutableMap.of();

    @Before
    public void setUp() {
        condition = new IsCsvExportEnabledCondition(featureManager);
    }

    @Test
    public void shouldNotDisplayWhenNoDarkFeaturesAreEnabled() {
        when(featureManager.isEnabled(IsCsvExportEnabledCondition.DARK_FEATURE_CSV_EXPORT_ENABLED)).thenReturn(false);

        assertThat(condition.shouldDisplay(context), equalTo(false));
    }

    @Test
    public void shouldDisplayWhenCsvExportIsEnabled() {
        when(featureManager.isEnabled(IsCsvExportEnabledCondition.DARK_FEATURE_CSV_EXPORT_ENABLED)).thenReturn(true);

        assertThat(condition.shouldDisplay(context), equalTo(true));

        when(featureManager.isEnabled(IsCsvExportEnabledCondition.DARK_FEATURE_CSV_EXPORT_DISABLED)).thenReturn(true);

        assertThat(condition.shouldDisplay(context), equalTo(false));
    }
}
