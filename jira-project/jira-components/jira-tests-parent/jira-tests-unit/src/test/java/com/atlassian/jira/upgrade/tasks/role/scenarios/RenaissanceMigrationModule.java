package com.atlassian.jira.upgrade.tasks.role.scenarios;


import com.atlassian.jira.CachingComponent;
import com.atlassian.jira.application.ApplicationRoleManager;
import com.atlassian.jira.license.JiraLicenseManager;
import com.atlassian.jira.license.RenaissanceMigrationStatusImpl;
import com.atlassian.jira.mock.MockApplicationProperties;
import com.atlassian.jira.mock.ofbiz.MockOfBizDelegator;
import com.atlassian.jira.upgrade.tasks.role.ApplicationRolesDaoImpl;
import com.atlassian.jira.upgrade.tasks.role.LicenseDaoImpl;
import com.atlassian.jira.upgrade.tasks.role.RenaissanceMigrationImpl;
import com.atlassian.jira.user.MockCrowdService;
import com.atlassian.jira.util.RealClock;
import com.atlassian.jira.web.util.ExternalLinkUtil;
import cucumber.runtime.Module;
import org.picocontainer.MutablePicoContainer;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.withSettings;

/**
 * Provides dependencies needed to to test renaissance migration.
 */
public class RenaissanceMigrationModule implements Module {
    @Override
    public void wire(final MutablePicoContainer container) {
        container.addComponent(RenaissanceMigrationImpl.class);
        container.addComponent(new MockCrowdService());
        container.addComponent(RealClock.getInstance());
        container.addComponent(ApplicationRolesDaoImpl.class);
        container.addComponent(LicenseDaoImpl.class);
        container.addComponent(MigrationTestHelper.class);
        container.addComponent(RenaissanceMigrationStatusImpl.class);

        container.addComponent(new MockApplicationProperties());
        container.addComponent(new MockOfBizDelegator());

        container.addComponent(mock(ExternalLinkUtil.class));

        //JiraLicenseManager and ApplicationRoleManager are only needed to call refresh on them
        container.addComponent(mock(JiraLicenseManager.class,
                withSettings().extraInterfaces(CachingComponent.class)));
        container.addComponent(mock(ApplicationRoleManager.class,
                withSettings().extraInterfaces(CachingComponent.class)));
    }
}
