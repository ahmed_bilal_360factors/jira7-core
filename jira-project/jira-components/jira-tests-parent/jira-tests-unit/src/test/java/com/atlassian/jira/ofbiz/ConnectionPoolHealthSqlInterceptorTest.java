package com.atlassian.jira.ofbiz;

import com.atlassian.jira.ofbiz.ConnectionPoolHealthSqlInterceptor.CountHolder;
import org.junit.Test;
import org.ofbiz.core.entity.jdbc.interceptors.connection.ConnectionPoolState;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.RETURNS_DEEP_STUBS;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class ConnectionPoolHealthSqlInterceptorTest {

    @Test
    public void countHolderShouldNotGoBelowZero() {
        final CountHolder countHolder = spy(CountHolder.class);
        doNothing().when(countHolder).logWarn(any(), any());

        final ConnectionPoolState state = mock(ConnectionPoolState.class, RETURNS_DEEP_STUBS);
        when(state.getConnectionPoolInfo().getMaxSize()).thenReturn(20);

        countHolder.replaced(state);

        //Unfortunately we don't have access to countHolder internals. So we check it indirectly.
        verify(countHolder, never()).logWarn(any(), any());
    }

}