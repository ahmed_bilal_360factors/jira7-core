package com.atlassian.jira.project.template.descriptor;

import com.atlassian.jira.project.template.hook.AddProjectHook;
import com.atlassian.jira.project.template.hook.AddProjectModule;
import com.atlassian.jira.project.template.hook.ConfigureData;
import com.atlassian.jira.project.template.hook.ConfigureResponse;
import com.atlassian.jira.project.template.hook.ValidateData;
import com.atlassian.jira.project.template.hook.ValidateResponse;
import com.atlassian.jira.project.template.module.AddProjectModuleBuilder;
import com.atlassian.jira.project.template.module.ProjectTemplateModule;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.elements.ResourceDescriptor;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugin.web.Condition;
import com.atlassian.plugin.web.WebInterfaceManager;
import com.atlassian.plugin.web.descriptors.ConditionElementParser;
import com.atlassian.plugin.webresource.WebResourceUrlProvider;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import java.net.URL;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.core.IsInstanceOf.instanceOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestProjectTemplateModuleDescriptor {
    @Rule
    public MethodRule initMockito = MockitoJUnit.rule();

    private static final String PLUGIN_KEY = "test-plugin-key";

    @Mock
    private ModuleFactory moduleFactory;

    @Mock
    private WebResourceUrlProvider urlProvider;

    @Mock
    private Plugin plugin;

    @Mock
    private ResourceDescriptorFactory resourceDescriptorFactory;

    @Mock
    private AddProjectModuleBuilder addProjectModuleBuilder;

    @Mock
    private AddProjectModule addProjectModule;

    @Mock
    private ConfigTemplateParser configTemplateParser;

    @Mock
    private WebInterfaceManager webInterfaceManager;

    private ConditionElementParser.ConditionFactory conditionFactory = (className, plugin1) -> {
        try {
            return (Condition) mock(Class.forName(className));
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    };

    private Element xml;
    private ProjectTemplateModuleDescriptor descriptor;

    @Before
    public void setUp() throws Exception {
        when(plugin.getKey()).thenReturn(PLUGIN_KEY);
        when(plugin.getClassLoader()).thenReturn(getClass().getClassLoader());
        when(addProjectModuleBuilder.addProjectHookClassName(any(String.class))).thenReturn(addProjectModuleBuilder);
        when(addProjectModuleBuilder.templateConfigurationFile(any(String.class))).thenReturn(addProjectModuleBuilder);
        when(addProjectModuleBuilder.build()).thenReturn(addProjectModule);
        when(plugin.getResource("/config/simple-issue-tracking.json")).thenReturn(new URL("http://localhost"));

        when(resourceDescriptorFactory.createResource(eq(plugin), anyString(), anyString(), any())).thenReturn(mock(ResourceDescriptor.class));

        xml = getProjectTemplateElement();
        descriptor = new ProjectTemplateModuleDescriptor(moduleFactory, urlProvider, resourceDescriptorFactory, webInterfaceManager, conditionFactory, configTemplateParser) {
            @Override
            protected AddProjectModuleBuilder getProjectTemplateModuleBuilder() {
                return addProjectModuleBuilder;
            }
        };
    }

    @Test
    public void templateDescriptorParsesXmlCorrectly() throws Exception {
        initDescriptor();

        ProjectTemplateModule module = descriptor.getModule();
        assertEquals(PLUGIN_KEY + ":issuetracking-blueprint", module.key());
        assertEquals("labelKey", "jira.issuetracking.project.item", module.labelKey());
        assertEquals("descriptionKey", "jira.issuetracking.project.item.description", module.descriptionKey());
        assertEquals("longDescriptionKey", Optional.of("jira.issuetracking.project.item.description.long"), module.longDescriptionKey());
        assertEquals("icon.location", "images/project-template-bug.png", module.icon().location());
        assertEquals("backgroundIcon.location", "images/background-image-issue-tracking.png", module.backgroundIcon().location());
        assertEquals("software", module.projectTypeKey());
        verify(addProjectModuleBuilder).addProjectHookClassName("com.atlassian.jira.project.template.descriptor.TestProjectTemplateModuleDescriptor$TestHook");
        verify(addProjectModuleBuilder).templateConfigurationFile(any(String.class));
    }

    @Test
    public void templateDescriptorIsConditional() throws Exception {
        initDescriptor();

        assertThat(descriptor.getCondition(), instanceOf(MockCondition.class));
    }

    @Test
    public void addProjectModuleIsOptional() {
        getNode("add-project").detach();
        initDescriptor();

        assertFalse(descriptor.getModule().hasAddProjectModule());
        assertThat(descriptor.getModule().addProjectModule(), equalTo(null));
    }

    @Test(expected = Exception.class)
    public void projectTypeIsMandatory() {
        getNode("projectTypeKey").detach();
        initDescriptor();

        assertThat(descriptor.getModule().projectTypeKey(), equalTo(null));
    }

    @Test
    public void projectCreateHookIsOptional() throws Exception {
        getNode("add-project/hook").detach();
        initDescriptor();

        assertTrue(descriptor.getModule().hasAddProjectModule());
        verify(addProjectModuleBuilder).addProjectHookClassName(null);
    }

    @Test
    public void configFileIsOptional() throws Exception {
        getNode("add-project/descriptor").detach();
        initDescriptor();

        verify(addProjectModuleBuilder).templateConfigurationFile(null);
    }

    @Test
    public void namesOfIconResourcesHaveTheRightExtension() throws Exception {
        initDescriptor();
        verify(resourceDescriptorFactory).createResource(eq(plugin), eq("icon.png"), anyString(), eq(Optional.empty()));
        verify(resourceDescriptorFactory).createResource(eq(plugin), eq("backgroundIcon.png"), anyString(), eq(Optional.of("a/customtype; charset=UTF-8")));
    }

    @Test
    public void templateWeightIsOptional() throws Exception {
        xml.remove(xml.attribute("weight"));
        initDescriptor();

        assertThat(descriptor.getModule().weight(), equalTo(100));
    }

    @Test
    public void iconAndBackgroundIconAreOptional() throws Exception {
        getNode("icon").detach();
        getNode("backgroundIcon").detach();
        descriptor.init(plugin, xml);

        ProjectTemplateModule module = descriptor.getModule();
        assertThat(module.icon().location(), equalTo(""));
        assertThat(module.backgroundIcon().location(), equalTo(""));
    }

    private void initDescriptor() {
        descriptor.init(plugin, xml);
        descriptor.enabled();
        descriptor.getModule();
    }

    private Element getProjectTemplateElement() throws DocumentException {
        SAXReader reader = new SAXReader();
        Document doc = reader.read(getClass().getResource("/project-templates/ProjectTemplateModuleDescriptorTest.xml"));
        return doc.getRootElement();
    }

    private Element getNode(String name) {
        return (Element) xml.selectSingleNode("//project-blueprint/" + name);
    }

    public static class TestHook implements AddProjectHook {
        @Override
        public ValidateResponse validate(ValidateData validateData) {
            throw new UnsupportedOperationException();
        }

        @Override
        public ConfigureResponse configure(ConfigureData configureData) {
            throw new UnsupportedOperationException();
        }
    }
}
