package com.atlassian.jira.dashboard.permission;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.fugue.Option;
import com.atlassian.gadgets.DashboardItemState;
import com.atlassian.gadgets.GadgetId;
import com.atlassian.gadgets.GadgetState;
import com.atlassian.gadgets.LocalDashboardItemModuleId;
import com.atlassian.gadgets.LocalDashboardItemState;
import com.atlassian.gadgets.OpenSocialDashboardItemModuleId;
import com.atlassian.gadgets.Vote;
import com.atlassian.gadgets.dashboard.Color;
import com.atlassian.gadgets.dashboard.DashboardId;
import com.atlassian.gadgets.dashboard.DashboardState;
import com.atlassian.gadgets.dashboard.spi.DashboardPermissionService;
import com.atlassian.gadgets.plugins.GadgetLocationTranslator;
import com.atlassian.gadgets.plugins.PluginGadgetSpec;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.MockGlobalPermissionManager;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.util.collect.MapBuilder;
import com.atlassian.plugin.ModuleCompleteKey;
import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.web.Condition;
import com.google.common.collect.ImmutableList;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.RuleChain;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static com.atlassian.gadgets.Vote.ALLOW;
import static com.atlassian.gadgets.Vote.DENY;
import static com.atlassian.jira.permission.ProjectPermissions.BROWSE_PROJECTS;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestJiraGadgetPermissionManager {
    @Rule
    public final ExpectedException exception = ExpectedException.none();
    @Rule
    public final RuleChain mockitoMocks = MockitoMocksInContainer.forTest(this);

    private final ApplicationUser nullUser = (ApplicationUser) null;
    private final Map<String, String> emptyParams = Collections.<String, String>emptyMap();
    private final Map<String, Object> emptyContext = Collections.<String, Object>emptyMap();
    private final ApplicationUser admin = new MockApplicationUser("admin");
    private final ApplicationUser user = new MockApplicationUser("user");

    @AvailableInContainer
    public JiraAuthenticationContext jiraAuthenticationContext = Mockito.mock(JiraAuthenticationContext.class);
    @AvailableInContainer
    public GlobalPermissionManager globalPermissionManager = MockGlobalPermissionManager.withSystemGlobalPermissions();
    @AvailableInContainer
    public GadgetLocationTranslator gadgetLocationTranslator = Mockito.mock(GadgetLocationTranslator.class);
    @Mock
    private DashboardPermissionService dashboardPermissionService;
    @Mock
    private Condition enabledCondition;
    @Mock
    private Condition localCondition;
    @Mock
    private ModuleDescriptor moduleDescriptor;
    @Mock
    private PermissionManager permissionManager;
    @Mock
    private EventPublisher eventPublisher;
    @Mock
    private Plugin plugin;
    @Mock
    private PluginAccessor pluginAccessor;
    private JiraGadgetPermissionManager jiraGadgetPermissionManager;

    @Before
    public void setUp() {
        when(plugin.getKey()).thenReturn("com.atlassian.jira.gadgets");
        jiraGadgetPermissionManager = new JiraGadgetPermissionManager(permissionManager, pluginAccessor, dashboardPermissionService, eventPublisher);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testVoteOnNull() {
        jiraGadgetPermissionManager.voteOn(null, null);
    }

    @Test
    public void testGetNonPluginGadgetWithNonConformingURL() throws URISyntaxException {
        URI uri = new URI("somerandomthing:stuff");
        createGadgetStateAndAddToTranslationService("1", uri.toASCIIString());
        Option<PluginGadgetSpec> gadgetSpec = jiraGadgetPermissionManager.getPluginGadgetSpec(uri);
        assertThat(gadgetSpec.isEmpty(), is(true));
        verify(pluginAccessor, never()).getEnabledPluginModule(anyString());
    }

    @Test
    public void testGetNonPluginGadgetWithConformingURL() throws URISyntaxException {
        URI uri = new URI("rest/gadgets/1.0/g/com.atlassian.jira.gadgets:admin-gadget/gadgets/admin-gadget.xml");
        createGadgetStateAndAddToTranslationService("1", uri.toASCIIString());
        Option<PluginGadgetSpec> gadgetSpec = jiraGadgetPermissionManager.getPluginGadgetSpec(uri);
        assertThat(gadgetSpec.isEmpty(), is(true));
        verify(pluginAccessor).getEnabledPluginModule("com.atlassian.jira.gadgets:admin-gadget");
    }

    @Test
    public void testVoteOnGadgetNoRolesSpecified() {
        ModuleDescriptor mockModuleDescriptor2 = mock(ModuleDescriptor.class);
        PluginGadgetSpec mockSpec = createPluginGadgetSpec("intro-gadget", "rest/intro.xml");
        PluginGadgetSpec mockSpec2 = createPluginGadgetSpec("intro-gadget2", "rest/intro.xml", MapBuilder.<String, String>newBuilder().add("roles-required", "").toMap());

        when(moduleDescriptor.getParams()).thenReturn(Collections.<String, String>emptyMap());
        when(mockModuleDescriptor2.getParams()).thenReturn(MapBuilder.<String, String>newBuilder().add("roles-required", "").toMap());
        when(pluginAccessor.getEnabledPluginModule("com.atlassian.jira.gadgets:intro-gadget")).thenReturn(moduleDescriptor);
        when(pluginAccessor.getEnabledPluginModule("com.atlassian.jira.gadgets:intro-gadget2")).thenReturn(mockModuleDescriptor2);

        Vote vote = jiraGadgetPermissionManager.voteOn(mockSpec, user);
        assertEquals(ALLOW, vote);

        vote = jiraGadgetPermissionManager.voteOn(mockSpec2, nullUser);
        assertEquals(ALLOW, vote);
    }

    @Test
    public void testVoteOnGadgetInvalidRolesSpecified() {
        PluginGadgetSpec mockSpec = createPluginGadgetSpec("intro-gadget", "rest/intro.xml",
                MapBuilder.<String, String>newBuilder().add("roles-required", "someinvalidmumbojumbo").toMap());
        when(permissionManager.hasPermission(Permissions.ADMINISTER, user)).thenReturn(false);

        Vote vote = jiraGadgetPermissionManager.voteOn(mockSpec, user);
        assertEquals(Vote.PASS, vote);
    }

    @Test
    public void testVoteOnGadgetGlobalRolesSpecified() {
        PluginGadgetSpec mockSpec = createPluginGadgetSpec("intro-gadget", "rest/intro.xml",
                MapBuilder.<String, String>newBuilder().add("roles-required", "use").toMap());

        when(permissionManager.hasPermission(Permissions.ADMINISTER, admin)).thenReturn(false);
        when(permissionManager.hasPermission(Permissions.USE, admin)).thenReturn(true);
        when(permissionManager.hasPermission(Permissions.ADMINISTER, user)).thenReturn(false);
        when(permissionManager.hasPermission(Permissions.USE, user)).thenReturn(false);

        Vote vote = jiraGadgetPermissionManager.voteOn(mockSpec, admin);
        assertEquals(ALLOW, vote);

        vote = jiraGadgetPermissionManager.voteOn(mockSpec, user);
        assertEquals(DENY, vote);
    }

    @Test
    public void testVoteOnGadgetProjectRolesSpecified() throws Exception {
        PluginGadgetSpec mockSpec = createPluginGadgetSpec("intro-gadget", "rest/intro.xml",
                MapBuilder.<String, String>newBuilder().add("roles-required", "browse").toMap());

        when(permissionManager.hasPermission(Permissions.ADMINISTER, admin)).thenReturn(false);
        when(permissionManager.hasProjects(BROWSE_PROJECTS, admin)).thenReturn(true);
        when(permissionManager.hasPermission(Permissions.ADMINISTER, user)).thenReturn(false);
        when(permissionManager.hasProjects(BROWSE_PROJECTS, user)).thenReturn(false);

        Vote vote = jiraGadgetPermissionManager.voteOn(mockSpec, admin);
        assertEquals(ALLOW, vote);

        vote = jiraGadgetPermissionManager.voteOn(mockSpec, user);
        assertEquals(DENY, vote);
    }

    @Test
    public void testVoteOnGadgetGlobalAdmin() throws Exception {
        PluginGadgetSpec mockSpec = createPluginGadgetSpec("intro-gadget", "rest/intro.xml",
                MapBuilder.<String, String>newBuilder().add("roles-required", "browse").toMap());

        when(permissionManager.hasPermission(Permissions.ADMINISTER, admin)).thenReturn(true);

        Vote vote = jiraGadgetPermissionManager.voteOn(mockSpec, admin);
        assertEquals(ALLOW, vote);
    }

    @Test(expected = Exception.class)
    public void testFilterNullDashboardState() {
        jiraGadgetPermissionManager.filterGadgets(null, nullUser);
    }

    @Test
    public void testDoesNotFilterOutGadgetsWithUndefinedSpec() {
        DashboardState state = mockDashboardStateWithGadgets();

        when(pluginAccessor.getEnabledPluginModule("com.atlassian.jira.gadgets:introduction-gadget")).thenReturn(null);

        DashboardState filteredState = jiraGadgetPermissionManager.filterGadgets(state, user);

        assertDashboardStatesAreEquivalentButNotSameObject(state, filteredState);
    }

    @Test
    public void testFilterReadOnlyDashboardState() {
        DashboardState state = mockDashboardStateWithGadgets();

        when(dashboardPermissionService.isWritableBy(DashboardId.valueOf("1"), user.getName())).thenReturn(false);

        DashboardState filteredState = jiraGadgetPermissionManager.filterGadgets(state, user);

        assertDashboardStatesAreEquivalentButNotSameObject(state, filteredState);
    }

    @Test
    public void testDoesNotFilterWriteableDashboardState() {
        DashboardState state = mockDashboardStateWithGadgets();

        when(dashboardPermissionService.isWritableBy(DashboardId.valueOf("1"), user.getName())).thenReturn(true);

        DashboardState filteredState = jiraGadgetPermissionManager.filterGadgets(state, user);

        assertEquals(state, filteredState);
    }

    private DashboardState mockDashboardStateWithGadgets() {
        PluginGadgetSpec introductionGadget = createPluginGadgetSpec("introduction-gadget", "introduction-gadget.xml");

        List<List<DashboardItemState>> columns = new ArrayList<List<DashboardItemState>>();
        List<DashboardItemState> columnOne = new ArrayList<DashboardItemState>();
        columnOne.add(createGadgetStateAndAddToTranslationService("100", "rest/gadgets/1.0/g/someothergadget.xml"));
        columnOne.add(createGadgetStateAndAddToTranslationService("100", "rest/gadgets/1.0/g/someothergadget2.xml"));
        columnOne.add(createGadgetStateAndAddToTranslationService("100", "http://www.atlassian.com/stream.xml"));
        List<DashboardItemState> columnTwo = new ArrayList<DashboardItemState>();
        columnTwo.add(createGadgetStateAndAddToTranslationService("100", "rest/gadgets/1.0/g/com.atlassian.jira.gadgets:introduction-gadget/gadgets/introduction-gadget.xml"));
        columnTwo.add(createGadgetStateAndAddToTranslationService("100", "http://www.atlassian.com/stream3.xml"));
        columns.add(columnOne);
        columns.add(columnTwo);

        when(moduleDescriptor.getModule()).thenReturn(introductionGadget);

        return DashboardState.dashboard(DashboardId.valueOf("1")).title("System Dashboard").dashboardColumns(columns).build();
    }

    private void assertDashboardStatesAreEquivalentButNotSameObject(DashboardState expected, DashboardState actual) {
        assertNotSame(expected, actual);
        assertEquals(expected.getLayout(), actual.getLayout());

        List<List<DashboardItemState>> expectedColumns = expected.getDashboardColumns().getColumns();
        List<List<DashboardItemState>> actualColumns = actual.getDashboardColumns().getColumns();

        assertEquals(expectedColumns, actualColumns);
    }

    @Test
    public void testNotFilterLocalDashboardItemState() {
        PluginGadgetSpec loginGadget = createPluginGadgetSpec("login-gadget", "login.xml");

        List<List<DashboardItemState>> columns = ImmutableList.<List<DashboardItemState>>of(
                ImmutableList.<DashboardItemState>of(
                        createLocalDasboardItemState("90"), createLocalDasboardItemState("100")
                ),
                ImmutableList.of(
                        createGadgetStateAndAddToTranslationService("110", "rest/gadgets/1.0/g/com.atlassian.jira.gadgets:login-gadget/login.xml"),
                        createLocalDasboardItemState("120")
                )
        );

        DashboardState state = DashboardState.dashboard(DashboardId.valueOf("1")).title("System Dashboard").dashboardColumns(columns).build();

        when(dashboardPermissionService.isWritableBy(DashboardId.valueOf("1"), null)).thenReturn(false);
        when(moduleDescriptor.getModule()).thenReturn(loginGadget);
        when(pluginAccessor.getEnabledPluginModule("com.atlassian.jira.gadgets:login-gadget")).thenReturn(moduleDescriptor);

        DashboardState filteredState = jiraGadgetPermissionManager.filterGadgets(state, null);

        assertThat(filteredState, is(state));
    }

    @Test
    public void testExtractModuleKey() {
        String key = jiraGadgetPermissionManager.extractModuleKey("http://localhost:8090/jira/rest/gadgets/1.0/g/com.atlassian.jira.gadgets:admin-gadget/gadgets/admin-gadget.xml");
        assertEquals("com.atlassian.jira.gadgets:admin-gadget", key);
        key = jiraGadgetPermissionManager.extractModuleKey("rest/gadgets/1.0/g/com.atlassian.jira.gadgets:admin-gadget/gadgets/admin-gadget.xml");
        assertEquals("com.atlassian.jira.gadgets:admin-gadget", key);
        key = jiraGadgetPermissionManager.extractModuleKey("/rest/gadgets/1.0/g/com.atlassian.jira.gadgets:admin-gadget/gadgets/admin-gadget.xml");
        assertEquals("com.atlassian.jira.gadgets:admin-gadget", key);
        key = jiraGadgetPermissionManager.extractModuleKey("rest/gadgets/1.0/g/some/gadgets/admin-gadget.xml");
        assertNull(key);
        key = jiraGadgetPermissionManager.extractModuleKey("rest/gadgets/1.0/g/admin-gadget.xml");
        assertNull(key);
    }

    @Test
    public void voteOnAcceptsGadgetWhenAllConditionsPass() {
        PluginGadgetSpec spec = createPluginGadgetSpec("awesome-gadget", "rest/awesome.xml",
                emptyParams, enabledCondition, localCondition);

        when(enabledCondition.shouldDisplay(emptyContext)).thenReturn(true);
        when(localCondition.shouldDisplay(emptyContext)).thenReturn(true);
        when(moduleDescriptor.getModule()).thenReturn(spec);
        when(pluginAccessor.getEnabledPluginModule("com.atlassian.jira.gadgets:awesome-gadget")).thenReturn(moduleDescriptor);

        Vote vote = jiraGadgetPermissionManager.voteOn(spec, user);

        assertThat(vote, equalTo(ALLOW));
        verify(enabledCondition).shouldDisplay(emptyContext);
        verify(localCondition).shouldDisplay(emptyContext);
    }

    @Test
    public void voteOnDeniesGadgetWhenEnabledConditionFails() {
        PluginGadgetSpec spec = createPluginGadgetSpec("awesome-gadget", "rest/awesome.xml",
                MapBuilder.<String, String>emptyMap(), enabledCondition, localCondition);

        when(enabledCondition.shouldDisplay(emptyContext)).thenReturn(false);
        when(localCondition.shouldDisplay(emptyContext)).thenReturn(true);
        when(moduleDescriptor.getModule()).thenReturn(spec);
        when(pluginAccessor.getEnabledPluginModule("com.atlassian.jira.gadgets:awesome-gadget")).thenReturn(moduleDescriptor);

        Vote vote = jiraGadgetPermissionManager.voteOn(spec, user);

        assertThat(vote, equalTo(DENY));
        verify(enabledCondition).shouldDisplay(emptyContext);
        verify(localCondition, never()).shouldDisplay(emptyContext);
    }

    @Test
    public void voteOnDeniesGadgetWhenLocalConditionFails() {
        PluginGadgetSpec spec = createPluginGadgetSpec("awesome-gadget", "rest/awesome.xml",
                MapBuilder.<String, String>emptyMap(), enabledCondition, localCondition);

        when(enabledCondition.shouldDisplay(emptyContext)).thenReturn(true);
        when(localCondition.shouldDisplay(emptyContext)).thenReturn(false);
        when(moduleDescriptor.getModule()).thenReturn(spec);
        when(pluginAccessor.getEnabledPluginModule("com.atlassian.jira.gadgets:awesome-gadget")).thenReturn(moduleDescriptor);

        Vote vote = jiraGadgetPermissionManager.voteOn(spec, user);

        assertThat(vote, equalTo(DENY));

        verify(enabledCondition).shouldDisplay(emptyContext);
        verify(localCondition).shouldDisplay(emptyContext);
    }

    private PluginGadgetSpec createPluginGadgetSpec(String moduleKey, String location) {
        return createPluginGadgetSpec(moduleKey, location, MapBuilder.<String, String>emptyMap());
    }

    private PluginGadgetSpec createPluginGadgetSpec(String moduleKey, String location, Map<String, String> params) {
        return createPluginGadgetSpec(moduleKey, location, params, null);
    }

    private PluginGadgetSpec createPluginGadgetSpec(String moduleKey, String location, Map<String, String> params, Condition condition) {
        return createPluginGadgetSpec(moduleKey, location, params, condition, condition);
    }

    private PluginGadgetSpec createPluginGadgetSpec(String moduleKey, String location, Map<String, String> params, Condition enabled, Condition local) {
        return PluginGadgetSpec.builder()
                .plugin(plugin)
                .moduleKey(moduleKey)
                .location(location)
                .params(params)
                .enabledCondition(enabled)
                .localCondition(local)
                .build();
    }

    private GadgetState createGadgetStateAndAddToTranslationService(String id, String url) {
        GadgetState gadgetState = GadgetState.gadget(GadgetId.valueOf(id)).specUri(URI.create(url)).build();
        when(gadgetLocationTranslator.translate(URI.create(url))).thenReturn(URI.create(url));
        return gadgetState;
    }

    private LocalDashboardItemState createLocalDasboardItemState(String id) {
        final ModuleCompleteKey completeKey = new ModuleCompleteKey("plugin", "module");
        final LocalDashboardItemModuleId dashboardItemModuleId =
                new LocalDashboardItemModuleId(completeKey, Option.<OpenSocialDashboardItemModuleId>none());
        return LocalDashboardItemState.builder()
                .gadgetId(GadgetId.valueOf(id))
                .color(Color.color2)
                .dashboardItemModuleId(dashboardItemModuleId).build();
    }
}
