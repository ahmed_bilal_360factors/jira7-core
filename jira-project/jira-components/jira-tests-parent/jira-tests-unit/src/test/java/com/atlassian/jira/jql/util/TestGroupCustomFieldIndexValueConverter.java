package com.atlassian.jira.jql.util;

import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.jira.issue.customfields.converters.GroupConverter;
import com.atlassian.jira.issue.customfields.impl.FieldValidationException;
import com.atlassian.jira.jql.operand.QueryLiteral;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static com.atlassian.jira.jql.operand.SimpleLiteralFactory.createLiteral;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestGroupCustomFieldIndexValueConverter {
    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @Mock
    private GroupConverter groupConverter;
    @Mock
    private Group group;

    @InjectMocks
    private GroupCustomFieldIndexValueConverter converter;

    @Test
    public void testConvertToIndexValueExceptionThrown() throws Exception {
        when(groupConverter.getGroup("blah")).thenThrow(new FieldValidationException("nas"));

        assertNull(converter.convertToIndexValue(createLiteral("blah")));
    }

    @Test
    public void testConvertToIndexValueNull() throws Exception {
        when(groupConverter.getGroup("blah")).thenReturn(null);

        assertNull(converter.convertToIndexValue(createLiteral("blah")));
    }

    @Test
    public void testConvertToIndexValueEmptyLiteral() throws Exception {
        assertNull(converter.convertToIndexValue(new QueryLiteral()));
    }

    @Test
    public void testConvertToIndexValueReturnsGroup() throws Exception {
        when(groupConverter.getGroup("blah")).thenReturn(group);
        when(groupConverter.getString(group)).thenReturn("result");

        assertEquals("result", converter.convertToIndexValue(createLiteral("blah")));
    }
}
