package com.atlassian.jira.web.filters;

import com.atlassian.jira.bc.security.login.DeniedReason;
import com.atlassian.jira.bc.security.login.LoginResult;
import com.atlassian.jira.bc.security.login.LoginService;
import com.atlassian.jira.junit.rules.MockitoContainer;
import com.atlassian.jira.mock.servlet.MockHttpServletRequest;
import com.atlassian.jira.mock.servlet.MockHttpServletResponse;
import com.atlassian.jira.web.startup.StartupPageSupport;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.StringWriter;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

public class DisplayErrorServletTest {
    private static final String CONTEXT_PATH = "/jira";

    @Rule
    public MockitoContainer mockitoContainer = new MockitoContainer(this);
    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Mock
    HttpSession session;
    @Mock
    RequestDispatcher dispatcher;

    MockHttpServletRequest request;
    MockHttpServletResponse response;
    StringWriter out;
    DisplayErrorServlet fixture;

    @Before
    public void setUp() {
        out = new StringWriter();
        request = new MockHttpServletRequest(session) {
            @Override
            public RequestDispatcher getRequestDispatcher(String name) {
                assertThat(name, is("/displayError.jsp"));
                return dispatcher;
            }
        };
        response = new MockHttpServletResponse(out);

        request.setContextPath(CONTEXT_PATH);
        request.setRequestURL(CONTEXT_PATH + "/display-error");

        fixture = new DisplayErrorServlet();
        StartupPageSupport.setLaunched(true);
    }

    @After
    public void tearDown() {
        StartupPageSupport.setLaunched(true);
    }

    private void service() throws ServletException, IOException {
        // Note: explicit cast avoids matching the (HttpServletRequest, HttpServletResponse) signature
        new DisplayErrorServlet().service((ServletRequest) request, response);
    }

    @Test
    public void redirectsToStartupPageWhenNotStarted() throws Exception {
        StartupPageSupport.setLaunched(false);

        service();

        assertThat(response.getStatus(), is(HttpServletResponse.SC_MOVED_TEMPORARILY));
        assertThat(response.getRedirect(), is("/jira/startup.jsp?returnTo=%2Fdisplay-error"));
        verifyZeroInteractions(dispatcher);
    }

    @Test
    public void forwardsAs404IfErrorCodeWasUnset() throws Exception {
        service();

        assertThat(response.getStatus(), is(HttpServletResponse.SC_NOT_FOUND));
        assertForwarded();
    }

    @Test
    public void forwardsAsGivenStatusCodeUnderNormalConditions() throws Exception {
        request.setAttribute(DisplayErrorServlet.STATUS_CODE, HttpServletResponse.SC_BAD_REQUEST);

        service();

        assertThat(response.getStatus(), is(HttpServletResponse.SC_BAD_REQUEST));
        assertForwarded();
    }

    @Test
    public void forwards401As403IfDeniedReasonsArePresent() throws Exception {
        final LoginResult loginResult = mock(LoginResult.class);
        when(loginResult.getDeniedReasons()).thenReturn(ImmutableSet.of(
                new DeniedReason("Nope!"),
                new DeniedReason("Bye, Felicia!")));
        request.setAttribute(DisplayErrorServlet.STATUS_CODE, HttpServletResponse.SC_UNAUTHORIZED);
        request.setAttribute(LoginService.LOGIN_RESULT, loginResult);

        service();

        assertThat(response.getStatus(), is(HttpServletResponse.SC_FORBIDDEN));
        assertThat(response.getHeaders(DeniedReason.X_DENIED_HEADER), containsInAnyOrder("Nope!", "Bye, Felicia!"));
        assertForwarded();
    }

    @Test
    public void forwards401As401IfThereAreNoDeniedReasonsPresent() throws Exception {
        final LoginResult loginResult = mock(LoginResult.class);
        when(loginResult.getDeniedReasons()).thenReturn(ImmutableSet.of());
        request.setAttribute(DisplayErrorServlet.STATUS_CODE, HttpServletResponse.SC_UNAUTHORIZED);
        request.setAttribute(LoginService.LOGIN_RESULT, loginResult);

        service();

        assertThat(response.getStatus(), is(HttpServletResponse.SC_UNAUTHORIZED));
        assertThat(response.getHeaders(DeniedReason.X_DENIED_HEADER), is(ImmutableList.of()));
        assertForwarded();
    }

    private void assertForwarded() throws ServletException, IOException {
        verify(dispatcher).forward(request, response);
    }
}
