package com.atlassian.jira.plugin.webfragment.conditions;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import com.atlassian.fugue.Option;
import com.atlassian.jira.entity.property.EntityProperty;
import com.atlassian.jira.entity.property.EntityPropertyConditionHelper;
import com.atlassian.jira.entity.property.EntityPropertyImpl;
import com.atlassian.jira.entity.property.EntityPropertyService;
import com.atlassian.jira.plugin.entity.EntityPropertyConditionHelperModuleDescriptor;
import com.atlassian.jira.plugin.webfragment.JiraWebInterfaceManager;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.ErrorCollections;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.PluginParseException;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import org.apache.commons.lang3.StringUtils;
import org.codehaus.jackson.node.BooleanNode;
import org.codehaus.jackson.node.JsonNodeFactory;
import org.codehaus.jackson.node.ObjectNode;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import static com.atlassian.jira.plugin.webfragment.conditions.EntityPropertyEqualToCondition.getValueForPath;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestEntityPropertyEqualToCondition {
    private static final String PROPERTY_KEY_PARAM = "propertyKey";
    public static final String EXPECTED_VALUE = "value";
    public static final String OBJECT_NAME = "objectName";
    private static final String VALUE_PARAM = EXPECTED_VALUE;
    private static final String ENTITY_PARAM = "entity";
    public static final String ENTITY_NAME = "existing_entity";
    public static final String PROPERTY_KEY = "property";
    public static final long ENTITY_ID = 1l;

    public static final String COMPLICATED_EXAMPLE;

    static {
        final ObjectNode root = JsonNodeFactory.instance.objectNode();
        root.put("random", "example");

        final ObjectNode one = root.putObject("one");
        one.put("ignore", "this");

        final ObjectNode two = one.putObject("two");
        two.put("three", true);
        two.put("ignore", "this");

        COMPLICATED_EXAMPLE = root.toString();
    }

    @Rule
    public final MethodRule initMockito = MockitoJUnit.rule();

    @Mock
    private PluginAccessor pluginAccessor;
    @Mock
    private EntityPropertyConditionHelper entityPropertyConditionHelper;
    @Mock
    private JiraHelper jiraHelper;
    @Mock
    private ApplicationUser user;

    private EntityPropertyEqualToCondition condition;

    @Before
    public void setUp() throws Exception {
        EntityPropertyConditionHelperModuleDescriptor entityPropertyConditionHelperModuleDescriptor =
                mock(EntityPropertyConditionHelperModuleDescriptor.class);

        when(entityPropertyConditionHelperModuleDescriptor.getModule()).thenReturn(entityPropertyConditionHelper);
        when(pluginAccessor.getEnabledModulesByClass(EntityPropertyConditionHelper.class))
                .thenReturn(Lists.newArrayList(entityPropertyConditionHelper));

        condition = new EntityPropertyEqualToCondition(pluginAccessor);
    }

    @Test(expected = PluginParseException.class)
    public void initFailsWithoutGivenEntity() {
        final Map<String, String> initParams = new HashMap<>();
        initParams.put(PROPERTY_KEY_PARAM, "a");
        initParams.put(VALUE_PARAM, "a");
        condition.init(initParams);
    }

    @Test(expected = PluginParseException.class)
    public void initFailsWithoutGivenPropertyKey() {
        final Map<String, String> initParams = new HashMap<>();
        initParams.put(ENTITY_PARAM, "issue");
        initParams.put(VALUE_PARAM, "a");
        condition.init(initParams);
    }

    @Test(expected = PluginParseException.class)
    public void initFailsWithoutGivenValue() {
        final Map<String, String> initParams = new HashMap<>();
        initParams.put(ENTITY_PARAM, "issue");
        initParams.put(PROPERTY_KEY_PARAM, "a");
        condition.init(initParams);
    }

    private Map<String, Object> makeContext(Map<String, Object> extraContext) {
        final ImmutableMap.Builder<String, Object> context = ImmutableMap.builder();

        // Standard context
        context.put(JiraWebInterfaceManager.CONTEXT_KEY_USER, user);
        context.put(JiraWebInterfaceManager.CONTEXT_KEY_HELPER, jiraHelper);

        // Text extra context
        context.putAll(extraContext);

        return context.build();
    }

    @Test
    public void shouldDisplayReturnsFalseWhenConditionEvaluatedForUnknownEntity() throws Exception {
        initCondition("not_existing_entity", PROPERTY_KEY, EXPECTED_VALUE, Optional.<String>empty());

        when(entityPropertyConditionHelper.getEntityName()).thenReturn(ENTITY_NAME);

        assertFalse(condition.shouldDisplay(makeContext(ImmutableMap.of())));
    }

    @Test
    public void shouldDisplayReturnsFalseWhenEntityIdNotFoundInContext() {
        initCondition(ENTITY_NAME, PROPERTY_KEY, EXPECTED_VALUE, Optional.<String>empty());
        when(entityPropertyConditionHelper.getEntityName()).thenReturn(ENTITY_NAME);
        when(entityPropertyConditionHelper.getEntityId(any())).thenReturn(Optional.empty());

        assertFalse(condition.shouldDisplay(makeContext(ImmutableMap.of())));
    }

    @Test
    public void shouldDisplayReturnsFalseWhenPropertyNotFound() {
        initCondition(ENTITY_NAME, PROPERTY_KEY, EXPECTED_VALUE, Optional.<String>empty());
        when(entityPropertyConditionHelper.getEntityName()).thenReturn(ENTITY_NAME);
        when(entityPropertyConditionHelper.getEntityId(any())).thenReturn(Optional.of(ENTITY_ID));
        when(entityPropertyConditionHelper.getProperty(user, ENTITY_ID, PROPERTY_KEY))
                .thenReturn(new EntityPropertyService.PropertyResult(ErrorCollections.create("error", ErrorCollection.Reason.NOT_FOUND), Option.none()));

        assertFalse(condition.shouldDisplay(makeContext(ImmutableMap.of())));
    }

    @Test
    public void shouldDisplayReturnsFalseWhenPropertyFoundAndNotMatched() {
        initCondition(ENTITY_NAME, PROPERTY_KEY, EXPECTED_VALUE, Optional.<String>empty());
        EntityPropertyImpl.existing(10l, ENTITY_NAME, ENTITY_ID, PROPERTY_KEY, "value2", new Timestamp(0l), new Timestamp(0l));

        when(entityPropertyConditionHelper.getEntityName()).thenReturn(ENTITY_NAME);
        when(entityPropertyConditionHelper.getEntityId(any())).thenReturn(Optional.of(ENTITY_ID));
        when(entityPropertyConditionHelper.getProperty(user, ENTITY_ID, PROPERTY_KEY))
                .thenReturn(new EntityPropertyService.PropertyResult(ErrorCollections.empty(),
                        Option.none()));

        assertFalse(condition.shouldDisplay(makeContext(ImmutableMap.of())));
    }

    @Test
    public void shouldDisplayReturnsTrueWhenPropertyFoundAndValuesMatched() {
        initCondition(ENTITY_NAME, PROPERTY_KEY, EXPECTED_VALUE, Optional.<String>empty());
        EntityProperty entityProperty = EntityPropertyImpl.existing(10l, ENTITY_NAME, ENTITY_ID, PROPERTY_KEY, EXPECTED_VALUE, new Timestamp(0l), new Timestamp(0l));

        when(entityPropertyConditionHelper.getEntityName()).thenReturn(ENTITY_NAME);
        when(entityPropertyConditionHelper.getEntityId(any())).thenReturn(Optional.of(ENTITY_ID));
        when(entityPropertyConditionHelper.getProperty(user, ENTITY_ID, PROPERTY_KEY))
                .thenReturn(new EntityPropertyService.PropertyResult(ErrorCollections.empty(),
                        Option.some(entityProperty)));

        assertTrue(condition.shouldDisplay(makeContext(ImmutableMap.of())));
    }

    @Test
    public void shouldDisplayReturnsTrueWhenPropertyFoundAndNestedValueMatches() {
        final JsonNodeFactory jnf = JsonNodeFactory.instance;

        // Create the entity property
        final ObjectNode actualValue = jnf.objectNode();
        final ObjectNode nestedObject = actualValue.putObject("nested");
        nestedObject.put("node", true);
        nestedObject.put("nodeDistraction", false);

        // Create the value to compare to
        final BooleanNode expectedValue = jnf.booleanNode(true);

        initCondition(ENTITY_NAME, PROPERTY_KEY, expectedValue.toString(), Optional.of("nested.node"));

        EntityProperty entityProperty = EntityPropertyImpl.existing(10l, ENTITY_NAME, ENTITY_ID, PROPERTY_KEY, actualValue.toString(), new Timestamp(0l), new Timestamp(0l));

        when(entityPropertyConditionHelper.getEntityName()).thenReturn(ENTITY_NAME);
        when(entityPropertyConditionHelper.getEntityId(any())).thenReturn(Optional.of(ENTITY_ID));
        when(entityPropertyConditionHelper.getProperty(user, ENTITY_ID, PROPERTY_KEY))
                .thenReturn(new EntityPropertyService.PropertyResult(ErrorCollections.empty(),
                        Option.some(entityProperty)));

        assertTrue(condition.shouldDisplay(makeContext(ImmutableMap.of())));
    }

    @Test
    public void shouldDisplayReturnsFalseWhenPropertyFoundAndNestedValueDoesNotMatch() {
        final JsonNodeFactory jnf = JsonNodeFactory.instance;

        // Create the entity property
        final ObjectNode actualValue = jnf.objectNode();
        final ObjectNode nestedObject = actualValue.putObject("nested");
        nestedObject.put("node", true);
        nestedObject.put("nodeDistraction", false);

        // Create the value to compare to
        final BooleanNode expectedValue = jnf.booleanNode(false);

        initCondition(ENTITY_NAME, PROPERTY_KEY, expectedValue.toString(), Optional.of("nested.node"));

        EntityProperty entityProperty = EntityPropertyImpl.existing(10l, ENTITY_NAME, ENTITY_ID, PROPERTY_KEY, actualValue.toString(), new Timestamp(0l), new Timestamp(0l));

        when(entityPropertyConditionHelper.getEntityName()).thenReturn(ENTITY_NAME);
        when(entityPropertyConditionHelper.getEntityId(any())).thenReturn(Optional.of(ENTITY_ID));
        when(entityPropertyConditionHelper.getProperty(user, ENTITY_ID, PROPERTY_KEY))
                .thenReturn(new EntityPropertyService.PropertyResult(ErrorCollections.empty(),
                        Option.some(entityProperty)));

        assertFalse(condition.shouldDisplay(makeContext(ImmutableMap.of())));
    }

    @Test
    public void shouldDisplayReturnsTrueWhenPropertyFoundAndNestedNonPrimitiveValueDoesMatch() {
        final JsonNodeFactory jnf = JsonNodeFactory.instance;

        // Create the entity property
        final ObjectNode actualValue = jnf.objectNode();
        actualValue.putObject("nested").putObject("node").put("value", true);

        // Create the value to compare to
        final ObjectNode expectedValue = jnf.objectNode();
        expectedValue.put("value", true);

        initCondition(ENTITY_NAME, PROPERTY_KEY, expectedValue.toString(), Optional.of("nested.node"));

        EntityProperty entityProperty = EntityPropertyImpl.existing(10l, ENTITY_NAME, ENTITY_ID, PROPERTY_KEY, actualValue.toString(), new Timestamp(0l), new Timestamp(0l));

        when(entityPropertyConditionHelper.getEntityName()).thenReturn(ENTITY_NAME);
        when(entityPropertyConditionHelper.getEntityId(any())).thenReturn(Optional.of(ENTITY_ID));
        when(entityPropertyConditionHelper.getProperty(user, ENTITY_ID, PROPERTY_KEY))
            .thenReturn(new EntityPropertyService.PropertyResult(ErrorCollections.empty(), Option.some(entityProperty)));

        assertTrue(condition.shouldDisplay(makeContext(ImmutableMap.of())));
    }

    @Test
    public void shouldDisplayReturnsFalseWhenPropertyFoundAndNestedNonPrimitiveValueDoesNotMatch() {
        final JsonNodeFactory jnf = JsonNodeFactory.instance;

        // Create the entity property
        final ObjectNode actualValue = jnf.objectNode();
        actualValue.putObject("nested").putObject("node").put("value", true);

        // Create the value to compare to
        final ObjectNode expectedValue = jnf.objectNode();
        expectedValue.put("value", "hello, is it me you're looking for");

        initCondition(ENTITY_NAME, PROPERTY_KEY, expectedValue.toString(), Optional.of("nested.node"));

        EntityProperty entityProperty = EntityPropertyImpl.existing(10l, ENTITY_NAME, ENTITY_ID, PROPERTY_KEY, actualValue.toString(), new Timestamp(0l), new Timestamp(0l));

        when(entityPropertyConditionHelper.getEntityName()).thenReturn(ENTITY_NAME);
        when(entityPropertyConditionHelper.getEntityId(any())).thenReturn(Optional.of(ENTITY_ID));
        when(entityPropertyConditionHelper.getProperty(user, ENTITY_ID, PROPERTY_KEY))
                .thenReturn(new EntityPropertyService.PropertyResult(ErrorCollections.empty(), Option.some(entityProperty)));

        assertFalse(condition.shouldDisplay(makeContext(ImmutableMap.of())));
    }

    @Test
    public void shouldDisplayReturnsTrueWhenPropertyFoundAndValuesMatchedViaTypeCoercionFromBooleanToString() {
        initCondition(ENTITY_NAME, PROPERTY_KEY, "true", Optional.<String>empty());
        EntityProperty entityProperty = EntityPropertyImpl.existing(10l, ENTITY_NAME, ENTITY_ID, PROPERTY_KEY, JsonNodeFactory.instance.textNode("true").toString(), new Timestamp(0l), new Timestamp(0l));

        when(entityPropertyConditionHelper.getEntityName()).thenReturn(ENTITY_NAME);
        when(entityPropertyConditionHelper.getEntityId(any())).thenReturn(Optional.of(ENTITY_ID));
        when(entityPropertyConditionHelper.getProperty(user, ENTITY_ID, PROPERTY_KEY))
                .thenReturn(new EntityPropertyService.PropertyResult(ErrorCollections.empty(),
                        Option.some(entityProperty)));

        assertTrue(condition.shouldDisplay(makeContext(ImmutableMap.of())));
    }

    @Test
    public void shouldDisplayReturnsTrueWhenPropertyFoundAndValuesMatchedViaTypeCoercionFromNumberToString() {
        initCondition(ENTITY_NAME, PROPERTY_KEY, "10", Optional.<String>empty());
        EntityProperty entityProperty = EntityPropertyImpl.existing(10l, ENTITY_NAME, ENTITY_ID, PROPERTY_KEY, JsonNodeFactory.instance.textNode("10").toString(), new Timestamp(0l), new Timestamp(0l));

        when(entityPropertyConditionHelper.getEntityName()).thenReturn(ENTITY_NAME);
        when(entityPropertyConditionHelper.getEntityId(any())).thenReturn(Optional.of(ENTITY_ID));
        when(entityPropertyConditionHelper.getProperty(user, ENTITY_ID, PROPERTY_KEY))
                .thenReturn(new EntityPropertyService.PropertyResult(ErrorCollections.empty(),
                        Option.some(entityProperty)));

        assertTrue(condition.shouldDisplay(makeContext(ImmutableMap.of())));
    }

    private void initCondition(String entityName, String propertyKey, String expectedValue, Optional<String> objectName) {
        final Map<String, String> initParams = new HashMap<>();
        initParams.put(ENTITY_PARAM, entityName);
        initParams.put(PROPERTY_KEY_PARAM, propertyKey);
        initParams.put(VALUE_PARAM, expectedValue);
        if(objectName.isPresent()) {
            initParams.put(OBJECT_NAME, objectName.get());
        }
        condition.init(initParams);
    }

    @Test
    public void testGetValueForPath__null_json_entity_returns_nothing() {
        assertTrue(getValueForPath(null, "key").isEmpty());
    }

    @Test(expected = NullPointerException.class)
    public void testGetValueForPath__null_object_name_throws_exception() {
        final JsonParser jsonParser = new JsonParser();
        final JsonElement emptyObject = jsonParser.parse("{}");
        getValueForPath(emptyObject, null);
    }

    @Test
    public void testGetValueForPath__no_object_name_returns_equivalent_object() {
        final JsonElement inputJson = new JsonParser().parse(COMPLICATED_EXAMPLE);
        final Option<JsonElement> result = getValueForPath(inputJson, StringUtils.EMPTY);
        final JsonElement expectedResult = new JsonParser().parse(COMPLICATED_EXAMPLE);
        assertEquals(Option.some(expectedResult), result);
    }

    @Test
    public void testGetValueForPath__one_level_deep() {
        final JsonElement inputJson = new JsonParser().parse(COMPLICATED_EXAMPLE);
        final Option<JsonElement> result = getValueForPath(inputJson, "one");
        final JsonElement expectedResult = new JsonParser().parse("{\"two\": {\"three\": true, \"ignore\": \"this\"}, \"ignore\": \"this\" }");
        assertEquals(Option.some(expectedResult), result);
    }

    @Test
    public void testGetValueForPath__two_levels_deep() {
        final JsonElement inputJson = new JsonParser().parse(COMPLICATED_EXAMPLE);
        final Option<JsonElement> result = getValueForPath(inputJson, "one.two");
        final JsonElement expectedResult = new JsonParser().parse("{\"three\": true, \"ignore\": \"this\"}");
        assertEquals(Option.some(expectedResult), result);
    }

    @Test
    public void testGetValueForPath__three_levels_deep() {
        final JsonElement inputJson = new JsonParser().parse(COMPLICATED_EXAMPLE);
        final Option<JsonElement> result = getValueForPath(inputJson, "one.two.three");
        final JsonElement expectedResult = new JsonParser().parse("true");
        assertEquals(Option.some(expectedResult), result);
    }

    @Test
    public void testGetValueForPath__missing_object_in_path() {
        final JsonElement inputJson = new JsonParser().parse(COMPLICATED_EXAMPLE);
        final Option<JsonElement> result = getValueForPath(inputJson, "one.none");
        assertEquals(Option.none(), result);
    }

    @Test
    public void testGetValueForPath__missing_object_in_complex_path() {
        final JsonElement inputJson = new JsonParser().parse(COMPLICATED_EXAMPLE);
        final Option<JsonElement> result = getValueForPath(inputJson, "one.none.three");
        assertEquals(Option.none(), result);
    }

    @Test
    public void testGetValueForPath__primitive_in_path() {
        final JsonElement inputJson = new JsonParser().parse("{\"one\": {\"two\": true, \"ignore\": \"this\" }, \"random\": \"example\"}");
        final Option<JsonElement> result = getValueForPath(inputJson, "one.two.three");
        assertEquals(Option.none(), result);
    }

    @Test
    public void testGetValueForPath__array_in_path() {
        // one.two would return an array with three in it. Thus there is no path to three
        final JsonElement inputJson = new JsonParser().parse("{\"one\": {\"two\": [{\"three\": true, \"ignore\": \"this\"}], \"ignore\": \"this\" }, \"random\": \"example\"}");
        final Option<JsonElement> result = getValueForPath(inputJson, "one.two.three");
        assertEquals(Option.none(), result);
    }
}
