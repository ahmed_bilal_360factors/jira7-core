package com.atlassian.jira.jql.context;

import com.atlassian.jira.config.SubTaskManager;
import com.atlassian.jira.issue.issuetype.MockIssueType;
import com.atlassian.jira.jql.operand.JqlOperandResolver;
import com.atlassian.jira.jql.operand.QueryLiteral;
import com.atlassian.jira.jql.util.JqlIssueSupport;
import com.atlassian.jira.mock.issue.MockIssue;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.query.clause.TerminalClauseImpl;
import com.atlassian.query.operand.EmptyOperand;
import com.atlassian.query.operand.Operand;
import com.atlassian.query.operand.SingleValueOperand;
import com.atlassian.query.operator.Operator;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Collections;

import static com.atlassian.jira.jql.operand.SimpleLiteralFactory.createLiteral;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestIssueParentClauseContextFactory {
    @Rule
    public MockitoRule mockito = MockitoJUnit.rule();

    @Mock
    private SubTaskManager subTaskManager;
    @Mock
    private JqlOperandResolver jqlOperandResolver;
    @Mock
    private JqlIssueSupport jqlIssueSupport;
    private ApplicationUser theUser = null;

    @Before
    public void setUp() throws Exception {
        when(subTaskManager.isSubTasksEnabled()).thenReturn(true);
    }

    @Test
    public void testGetClauseContext() throws Exception {
        final MockProject mockProject1 = new MockProject(10L, "test");
        final MockProject mockProject2 = new MockProject(11L, "test");
        final MockIssueType mockIssueType1 = new MockIssueType("10", "test");
        final MockIssueType mockIssueType2 = new MockIssueType("11", "test");
        final SingleValueOperand operand = new SingleValueOperand("blah");
        final TerminalClauseImpl clause = new TerminalClauseImpl("blah", Operator.EQUALS, operand);

        when(jqlOperandResolver.getValues(theUser, operand, clause)).thenReturn(ImmutableList.of(createLiteral("key"), createLiteral(10L)));

        MockIssue subIssue1 = new MockIssue();
        subIssue1.setProjectObject(mockProject1);
        subIssue1.setIssueTypeId(mockIssueType1.getId());

        MockIssue parentIssue1 = new MockIssue();
        parentIssue1.setSubTaskObjects(Collections.singletonList(subIssue1));

        MockIssue subIssue2 = new MockIssue();
        subIssue2.setProjectObject(mockProject2);
        subIssue2.setIssueTypeId(mockIssueType2.getId());

        MockIssue parentIssue2 = new MockIssue();
        parentIssue2.setSubTaskObjects(Collections.singletonList(subIssue2));

        when(jqlIssueSupport.getIssue("key", null)).thenReturn(parentIssue1);
        when(jqlIssueSupport.getIssue(10L, (ApplicationUser) null)).thenReturn(parentIssue2);

        IssueParentClauseContextFactory factory = new IssueParentClauseContextFactory(jqlIssueSupport, jqlOperandResolver, subTaskManager);
        final ClauseContext result = factory.getClauseContext(theUser, clause);
        ClauseContext expectedResult = new ClauseContextImpl(ImmutableSet.of(
                new ProjectIssueTypeContextImpl(new ProjectContextImpl(10L), new IssueTypeContextImpl("10")),
                new ProjectIssueTypeContextImpl(new ProjectContextImpl(11L), new IssueTypeContextImpl("11"))
        ));

        assertEquals(expectedResult, result);
    }

    @Test
    public void testGetClauseContextWhenCalculationWasEmpty() throws Exception {
        final SingleValueOperand operand = new SingleValueOperand("blah");
        final TerminalClauseImpl clause = new TerminalClauseImpl("blah", Operator.EQUALS, operand);

        when(jqlOperandResolver.getValues(theUser, operand, clause)).thenReturn(ImmutableList.of());

        IssueParentClauseContextFactory factory = new IssueParentClauseContextFactory(jqlIssueSupport, jqlOperandResolver, subTaskManager);
        final ClauseContext result = factory.getClauseContext(theUser, clause);
        ClauseContext expectedResult = ClauseContextImpl.createGlobalClauseContext();

        assertEquals(expectedResult, result);

    }

    @Test
    public void testGetClauseContextWhenCalculationWasNull() throws Exception {
        final SingleValueOperand operand = new SingleValueOperand("blah");
        final TerminalClauseImpl clause = new TerminalClauseImpl("blah", Operator.EQUALS, operand);

        when(jqlOperandResolver.getValues(theUser, operand, clause)).thenReturn(null);

        IssueParentClauseContextFactory factory = new IssueParentClauseContextFactory(jqlIssueSupport, jqlOperandResolver, subTaskManager);
        final ClauseContext result = factory.getClauseContext(theUser, clause);
        ClauseContext expectedResult = ClauseContextImpl.createGlobalClauseContext();

        assertEquals(expectedResult, result);
    }

    @Test
    public void testGetClauseContextSingleEmptyLiteral() throws Exception {
        final Operand operand = EmptyOperand.EMPTY;
        final TerminalClauseImpl clause = new TerminalClauseImpl("blah", Operator.IS, operand);

        when(jqlOperandResolver.getValues(theUser, operand, clause)).thenReturn(ImmutableList.of(new QueryLiteral()));

        IssueParentClauseContextFactory factory = new IssueParentClauseContextFactory(jqlIssueSupport, jqlOperandResolver, subTaskManager);
        final ClauseContext result = factory.getClauseContext(theUser, clause);
        ClauseContext expectedResult = ClauseContextImpl.createGlobalClauseContext();

        assertEquals(expectedResult, result);
    }

    @Test
    public void testGetClauseContextInequality() throws Exception {
        ClauseContext expectedResult = ClauseContextImpl.createGlobalClauseContext();

        IssueParentClauseContextFactory factory = new IssueParentClauseContextFactory(jqlIssueSupport, jqlOperandResolver, subTaskManager);
        final ClauseContext result = factory.getClauseContext(theUser, new TerminalClauseImpl("blah", Operator.NOT_EQUALS, "blah"));

        assertEquals(expectedResult, result);
    }

    @Test
    public void testGetClauseContextBadOperator() throws Exception {
        ClauseContext expectedResult = ClauseContextImpl.createGlobalClauseContext();

        IssueParentClauseContextFactory factory = new IssueParentClauseContextFactory(jqlIssueSupport, jqlOperandResolver, subTaskManager);
        final ClauseContext result = factory.getClauseContext(theUser, new TerminalClauseImpl("blah", Operator.LESS_THAN, "blah"));

        assertEquals(expectedResult, result);
    }

    @Test
    public void testGetClauseContextSubTasksDisabled() throws Exception {
        when(subTaskManager.isSubTasksEnabled()).thenReturn(false);

        ClauseContext expectedResult = ClauseContextImpl.createGlobalClauseContext();

        IssueParentClauseContextFactory factory = new IssueParentClauseContextFactory(jqlIssueSupport, jqlOperandResolver, subTaskManager);
        final ClauseContext result = factory.getClauseContext(theUser, new TerminalClauseImpl("blah", Operator.LESS_THAN, "blah"));

        assertEquals(expectedResult, result);
    }

    @Test
    public void testIsNegationOperator() throws Exception {
        IssueParentClauseContextFactory factory = new IssueParentClauseContextFactory(jqlIssueSupport, jqlOperandResolver, subTaskManager);

        assertTrue(factory.isNegationOperator(Operator.NOT_EQUALS));
        assertTrue(factory.isNegationOperator(Operator.NOT_IN));

        assertFalse(factory.isNegationOperator(Operator.IS_NOT));
        assertFalse(factory.isNegationOperator(Operator.IS));
        assertFalse(factory.isNegationOperator(Operator.EQUALS));
        assertFalse(factory.isNegationOperator(Operator.IN));
    }
}
