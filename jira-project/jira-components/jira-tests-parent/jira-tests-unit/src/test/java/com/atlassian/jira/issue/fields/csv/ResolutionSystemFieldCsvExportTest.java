package com.atlassian.jira.issue.fields.csv;

import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.export.FieldExportParts;
import com.atlassian.jira.issue.fields.ResolutionSystemField;
import com.atlassian.jira.issue.resolution.Resolution;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.I18nHelper;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.util.Collection;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @since v6.5
 */
public class ResolutionSystemFieldCsvExportTest {
    private static Resolution mockResolution1 = getResolutionMock("1");
    private static Resolution mockResolution2 = getResolutionMock("2");
    private static Resolution mockResolution3 = getResolutionMock("3");
    private static Resolution mockResolution4 = getResolutionMock("4");
    private static Resolution mockResolution5 = getResolutionMock("5");
    private static Collection<Resolution> resolutions = Lists.newArrayList(mockResolution1, mockResolution2, mockResolution3, mockResolution4, mockResolution5);

    private static final String RESOLUTION_NAME = "Resolution name";

    @Rule
    public final RuleChain mockito = MockitoMocksInContainer.forTest(this);

    @Mock
    private JiraAuthenticationContext authenticationContext;

    @Mock
    private I18nHelper i18nHelper;
    @InjectMocks
    private ResolutionSystemField field;

    @Mock
    private ConstantsManager mockConstantsManager;
    @Mock
    private Issue issue;
    @Mock
    private Resolution resolution;

    @Before
    public void setUp() throws Exception {
        when(mockConstantsManager.getResolutions()).thenReturn(resolutions);
        when(resolution.getNameTranslation()).thenReturn(RESOLUTION_NAME);
        when(authenticationContext.getI18nHelper()).thenReturn(i18nHelper);
    }

    @Test
    public void testCsvRepresentationForExpectedTypeAndValues() {
        when(issue.getResolution()).thenReturn(resolution);

        final FieldExportParts representation = field.getRepresentationFromIssue(issue);
        assertThat(representation.getParts(), hasSize(1));
        assertThat(representation.getParts().get(0).getValues()::iterator, contains(RESOLUTION_NAME));
    }

    @Test
    public void testCsvRepresentationWhenThereIsNoPriority() {
        when(issue.getResolution()).thenReturn(null);

        final FieldExportParts representation = field.getRepresentationFromIssue(issue);
        assertThat(representation.getParts(), hasSize(1));
        assertThat(representation.getParts().get(0).getValues()::iterator, contains(""));
    }

    private static Resolution getResolutionMock(String resolutionId) {
        Resolution resolution = mock(Resolution.class);
        when(resolution.getId()).thenReturn(resolutionId);
        when(resolution.getName()).thenReturn(resolutionId);
        return resolution;
    }
}
