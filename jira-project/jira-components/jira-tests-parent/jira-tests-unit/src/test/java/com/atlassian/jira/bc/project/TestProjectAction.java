package com.atlassian.jira.bc.project;

import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.security.plugin.ProjectPermissionKey;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.mockito.Mock;

import static com.atlassian.jira.permission.GlobalPermissionKey.GLOBAL_PERMISSION_ID_TRANSLATION;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @since v4.4
 */
public class TestProjectAction {
    @Rule
    public final RuleChain mocksInContainer = MockitoMocksInContainer.forTest(this);

    @Mock
    @AvailableInContainer
    private GlobalPermissionManager globalPermissionManager;

    @Test
    public void testCorrectPermissions() throws Exception {
        assertThat(ProjectAction.EDIT_PROJECT_CONFIG.getPermissions(),
                is(new int[]{Permissions.ADMINISTER, Permissions.PROJECT_ADMIN}));

        assertThat(ProjectAction.VIEW_ISSUES.getPermissions(),
                is(new int[]{Permissions.BROWSE}));

        assertThat(ProjectAction.VIEW_PROJECT.getPermissions(),
                is(new int[]{Permissions.ADMINISTER, Permissions.BROWSE, Permissions.PROJECT_ADMIN}));
    }

    @Test
    public void testCheckActionPermission() throws Exception {
        final Project mockProject1 = new MockProject(11781L, "ABC");
        final ApplicationUser mockUser = new MockApplicationUser("admin");

        final int[] perms = ProjectAction.VIEW_PROJECT.getPermissions();
        for (int i = -1; i < perms.length; i++) {
            final PermissionManager permissionManager = mock(PermissionManager.class);

            boolean success = false;
            for (int j = 0; !success && j < perms.length; j++) {
                final int perm = perms[j];
                success = i == j;
                if (GLOBAL_PERMISSION_ID_TRANSLATION.containsKey(perm)) {
                    when(globalPermissionManager.isGlobalPermission(perm)).thenReturn(true);
                    when(permissionManager.hasPermission(perm, mockUser)).thenReturn(success);
                } else {
                    when(permissionManager.hasPermission(new ProjectPermissionKey(perm), mockProject1, mockUser)).thenReturn(success);
                }

            }
            assertEquals(success, ProjectAction.VIEW_PROJECT.hasPermission(permissionManager, mockUser, mockProject1));
        }
    }
}
