package com.atlassian.jira.plugin.navigation;

import com.atlassian.extras.api.LicenseType;
import com.atlassian.jira.application.ApplicationRoleManager;
import com.atlassian.jira.bc.license.JiraLicenseService;
import com.atlassian.jira.cluster.ClusterManager;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.license.JiraLicenseManager;
import com.atlassian.jira.license.LicenseDetails;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.BuildUtilsInfo;
import com.atlassian.jira.web.util.ExternalLinkUtilImpl;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.util.profiling.UtilTimerStack;
import com.google.common.collect.ImmutableList;
import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.atlassian.jira.config.properties.APKeys.JIRA_SHOW_CONTACT_ADMINISTRATORS_FORM;
import static com.atlassian.jira.permission.GlobalPermissionKey.SYSTEM_ADMIN;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

/**
 * Unit tests for com.atlassian.jira.plugin.navigation.FooterModuleDescriptorImpl
 *
 * @since 7.0
 */
@RunWith(MockitoJUnitRunner.class)
public class FooterModuleDescriptorImplTest {
    public static final String ERROR_CLASS = "licensemessagered";
    @Mock
    JiraAuthenticationContext authenticationContext;
    @Mock
    JiraLicenseService jiraLicenseService;
    @Mock
    BuildUtilsInfo buildUtilsInfo;
    @Mock
    ModuleFactory moduleFactory;
    @Mock
    GlobalPermissionManager permissionManager;
    @Mock
    ApplicationProperties applicationProperties;
    @Mock
    ClusterManager clusterManager;
    @Mock
    JiraLicenseManager licenseManager;
    @Mock
    HttpServletRequest httpServletRequest;
    @Mock
    ApplicationRoleManager applicationRoleManager;

    @Mock
    LicenseDetails evaluationLicense;
    @Mock
    LicenseDetails communityLicense;
    @Mock
    LicenseDetails starterLicense;

    private FooterModuleDescriptorImpl footerModuleDescriptor;

    @Before
    public void setup() {
        when(buildUtilsInfo.getVersion()).thenReturn("hi mum!");
        when(buildUtilsInfo.getCurrentBuildNumber()).thenReturn("some build number");
        when(evaluationLicense.isEvaluation()).thenReturn(true);
        when(evaluationLicense.getLicenseType()).thenReturn(LicenseType.COMMERCIAL);
        when(evaluationLicense.isCommercial()).thenReturn(true);

        when(communityLicense.getLicenseType()).thenReturn(LicenseType.COMMUNITY);

        when(starterLicense.getLicenseType()).thenReturn(LicenseType.STARTER);
        when(starterLicense.isStarter()).thenReturn(true);

        footerModuleDescriptor = new FooterModuleDescriptorImpl(authenticationContext,
                jiraLicenseService, buildUtilsInfo,
                moduleFactory, permissionManager,
                applicationProperties, clusterManager,
                licenseManager, applicationRoleManager);
    }

    @Test
    public void defaultParametersShouldBePresent() {
        when(jiraLicenseService.getServerId()).thenReturn("test-server-id");

        Map<String, ?> result = createVelocityParams();
        assertEquals(result.get("serverid"), ("test-server-id"));
        assertEquals(result.get("version"), (buildUtilsInfo.getVersion()));
        assertEquals(result.get("buildNumber"), (buildUtilsInfo.getCurrentBuildNumber()));
        assertEquals(result.get("build"), (buildUtilsInfo));
        assertEquals(result.get("req"), (httpServletRequest));

        assertNotNull(result.get("string"));
        assertTrue(result.get("string") instanceof StringUtils);

        assertNotNull(result.get("externalLinkUtil"));
        assertTrue(result.get("externalLinkUtil") instanceof ExternalLinkUtilImpl);

        assertNotNull(result.get("utilTimerStack"));
        assertTrue(result.get("utilTimerStack") instanceof UtilTimerStack);
    }

    @Test
    public void sysadminShouldBeTrueForSysadmin() {
        when(permissionManager.hasPermission(eq(SYSTEM_ADMIN), Matchers.<ApplicationUser>anyObject())).thenReturn(true);
        Map<String, ?> result = createVelocityParams();

        assertTrue(result.get("isSysAdmin").equals(true));
    }

    @Test
    public void sysadminShouldBeFalseForNonSysadmin() {

        when(permissionManager.hasPermission(eq(SYSTEM_ADMIN), Matchers.<ApplicationUser>anyObject())).thenReturn(false);
        Map<String, ?> result = createVelocityParams();

        assertTrue(result.get("isSysAdmin").equals(false));
    }

    @Test
    public void showContactAdministratorsFormShouldBeRespectedIfOn() {
        when(applicationProperties.getOption(JIRA_SHOW_CONTACT_ADMINISTRATORS_FORM)).thenReturn(true);

        Map<String, ?> result = createVelocityParams();

        assertTrue(result.get("showContactAdminForm").equals(true));
    }

    @Test
    public void showContactAdministratorsFormShouldBeRespectedIfOff() {
        when(applicationProperties.getOption(JIRA_SHOW_CONTACT_ADMINISTRATORS_FORM)).thenReturn(false);

        Map<String, ?> result = createVelocityParams();

        assertTrue(result.get("showContactAdminForm").equals(false));
    }

    @Test
    public void clusteringShouldBeEnabledWhenEnabled() {
        when(clusterManager.isClustered()).thenReturn(true);
        when(clusterManager.getNodeId()).thenReturn("elephant");

        Map<String, ?> result = createVelocityParams();

        assertEquals(("elephant"), result.get("nodeId"));
    }

    @Test
    public void clusteringShouldBeDisabledWhenDisabled() {
        when(clusterManager.isClustered()).thenReturn(false);

        Map<String, ?> result = createVelocityParams();

        verify(clusterManager).isClustered();
        verifyNoMoreInteractions(clusterManager);

        assertNull(result.get("nodeId"));
    }

    @Test
    public void noLicenseShouldIndicateNoLicense() {
        Map<String, ?> result = createVelocityParams();

        assertEquals(true, result.get("notfull"));
        assertEquals(true, result.get("unlicensed"));
        assertEquals("<Unknown>", result.get("organisation"));
        assertLicenseErrorClass(result);
    }

    @Test
    public void gracePeriodEnabledShouldShowInParams() {
        when(jiraLicenseService.isLicenseSet()).thenReturn(true);
        when(licenseManager.hasLicenseTooOldForBuildConfirmationBeenDone()).thenReturn(true);

        Map<String, ?> result = createVelocityParams();
        assertEquals(true, result.get("confirmedWithOldLicense"));
        assertLicenseErrorClass(result);
    }

    @Test
    public void evaluationLicenseShouldTakePrecedence() {
        when(jiraLicenseService.isLicenseSet()).thenReturn(true);
        List<LicenseDetails> licenses = ImmutableList.of(communityLicense, evaluationLicense);
        when(jiraLicenseService.getLicenses()).thenReturn(licenses);

        Map<String, ?> result = createVelocityParams();
        assertEquals(true, result.get("evaluation"));
        assertEquals(true, result.get("notfull"));
        assertLicenseErrorClass(result);
    }

    @Test
    public void communityLicenseShouldHaveInfoClass() {
        when(jiraLicenseService.isLicenseSet()).thenReturn(true);
        List<LicenseDetails> licenses = ImmutableList.of(communityLicense, starterLicense);
        when(jiraLicenseService.getLicenses()).thenReturn(licenses);

        Map<String, ?> result = createVelocityParams();
        assertEquals(true, result.get("community"));
        assertEquals(true, result.get("notfull"));
        assertEquals("licensemessage", result.get("licenseMessageClass"));
    }

    @Test
    public void starterLicenseShouldNotSetMessageClass() {
        when(jiraLicenseService.isLicenseSet()).thenReturn(true);
        List<LicenseDetails> licenses = ImmutableList.of(starterLicense);
        when(jiraLicenseService.getLicenses()).thenReturn(licenses);

        Map<String, ?> result = createVelocityParams();
        assertNull(result.get("notfull"));
        assertNull(result.get("licenseMessageClass"));
    }

    private Map<String, ?> createVelocityParams() {
        HashMap<String, Object> params = new HashMap<>();
        return footerModuleDescriptor.createVelocityParams(httpServletRequest, params);
    }

    private void assertLicenseErrorClass(final Map<String, ?> result) {
        assertEquals(ERROR_CLASS, result.get("licenseMessageClass"));
    }


}
