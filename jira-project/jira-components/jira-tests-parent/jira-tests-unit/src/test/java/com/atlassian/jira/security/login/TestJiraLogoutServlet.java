package com.atlassian.jira.security.login;

import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.xsrf.XsrfCheckResult;
import com.atlassian.jira.security.xsrf.XsrfInvocationChecker;
import com.atlassian.jira.user.MockApplicationUser;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Responsible for testing {@link com.atlassian.jira.security.login.JiraLogoutServlet}
 *
 * @since v4.1.1
 */
public class TestJiraLogoutServlet {
    @Rule
    public MockitoRule mockito = MockitoJUnit.rule();
    @Mock
    private HttpServletRequest mockHttpServletRequest;
    @Mock
    private HttpServletResponse mockHttpServletResponse;
    @Mock
    private XsrfInvocationChecker mockXsrfInvocationChecker;
    @Mock
    private HttpServlet mockSeraphLogoutServlet;
    @Mock
    private XsrfTokenAppendingResponse mockXsrfTokenAppendingResponse;
    @Mock
    private JiraAuthenticationContext mockAuthenticationContext;

    private JiraLogoutServlet jiraLogoutServlet;

    @Before
    public void setUp() throws Exception {

        jiraLogoutServlet = new JiraLogoutServlet() {
            @Override
            HttpServlet getSeraphLogoutServlet() {
                return mockSeraphLogoutServlet;
            }

            @Override
            XsrfInvocationChecker getXsrfInvocationChecker() {
                return mockXsrfInvocationChecker;
            }

            @Override
            XsrfTokenAppendingResponse createXsrfTokenAppendingResponse(final HttpServletRequest request,
                                                                        final HttpServletResponse response) {
                return mockXsrfTokenAppendingResponse;
            }

            @Override
            JiraAuthenticationContext getAuthenticationContext() {
                return mockAuthenticationContext;
            }
        };

    }

    @Test
    public void testDelegatesToSeraphServletWhenTokenIsValid() throws Exception {
        when(mockXsrfInvocationChecker.checkWebRequestInvocation(mockHttpServletRequest)).thenReturn(result(true, true, true));

        jiraLogoutServlet.service(mockHttpServletRequest, mockHttpServletResponse);

        verify(mockSeraphLogoutServlet).service(mockHttpServletRequest, mockXsrfTokenAppendingResponse);
    }

    private XsrfCheckResult result(final boolean required, final boolean valid, final boolean authed) {
        return new XsrfCheckResult() {
            @Override
            public boolean isRequired() {
                return required;
            }

            @Override
            public boolean isValid() {
                return valid;
            }

            @Override
            public boolean isGeneratedForAuthenticatedUser() {
                return authed;
            }
        };
    }

    @Test
    public void testRedirectsToAlreadyLoggedOutPageWhenTheXsrfTokenIsNotValidAndThereIsNoRememberMeCookie()
            throws Exception {
        String contextPath = "context";

        when(mockXsrfInvocationChecker.checkWebRequestInvocation(mockHttpServletRequest)).thenReturn(result(true, false, true));
        when(mockHttpServletRequest.getContextPath()).thenReturn(contextPath);

        when(mockAuthenticationContext.getLoggedInUser()).thenReturn(null);

        jiraLogoutServlet.service(mockHttpServletRequest, mockHttpServletResponse);

        verify(mockHttpServletResponse).sendRedirect(contextPath + JiraLogoutServlet.ALREADY_LOGGED_OUT_PAGE);
    }

    @Test
    public void testRedirectsToConfirmLogOutPageWhenTheXsrfTokenIsNotValidAndThereIsARememberMeCookie()
            throws Exception {
        String contextPath = "context";

        when(mockXsrfInvocationChecker.checkWebRequestInvocation(mockHttpServletRequest)).thenReturn(result(true, false, true));
        when(mockHttpServletRequest.getContextPath()).thenReturn(contextPath);

        MockApplicationUser mockUser = new MockApplicationUser("fred");
        when(mockAuthenticationContext.getUser()).thenReturn(mockUser);

        jiraLogoutServlet.service(mockHttpServletRequest, mockHttpServletResponse);

        verify(mockHttpServletResponse).sendRedirect(contextPath + JiraLogoutServlet.LOG_OUT_CONFIRM_PAGE);
    }
}
