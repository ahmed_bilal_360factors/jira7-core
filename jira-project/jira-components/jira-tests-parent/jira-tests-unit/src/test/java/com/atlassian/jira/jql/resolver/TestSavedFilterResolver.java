package com.atlassian.jira.jql.resolver;

import com.atlassian.jira.issue.search.MockSearchRequest;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.issue.search.SearchRequestManager;
import com.atlassian.jira.jql.operand.QueryLiteral;
import com.atlassian.jira.sharing.search.SharedEntitySearchParameters;
import com.atlassian.jira.sharing.search.SharedEntitySearchParametersBuilder;
import com.atlassian.jira.sharing.search.SharedEntitySearchResult;
import com.atlassian.jira.user.ApplicationUser;
import org.hamcrest.Matchers;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Collections;
import java.util.List;

import static com.atlassian.jira.jql.operand.SimpleLiteralFactory.createLiteral;
import static com.atlassian.jira.sharing.search.SharedEntitySearchParameters.TextSearchMode.EXACT;
import static com.atlassian.jira.util.collect.CollectionBuilder.newBuilder;
import static com.atlassian.jira.util.collect.CollectionEnclosedIterable.from;
import static java.lang.Integer.MAX_VALUE;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.hasItem;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestSavedFilterResolver {
    @Rule
    public MockitoRule mocks = MockitoJUnit.rule();

    @Mock
    private SearchRequestManager searchRequestManager;
    private ApplicationUser theUser = null;

    @Test
    public void testGetSearchRequestEmptyLiteral() throws Exception {
        final SavedFilterResolver filterResolver = new SavedFilterResolver(searchRequestManager);
        final List<SearchRequest> result = filterResolver.getSearchRequest(theUser, Collections.singletonList(new QueryLiteral()));

        assertThat(result, empty());
    }

    @Test
    public void testGetSearchRequestEmptyString() throws Exception {
        final SavedFilterResolver filterResolver = new SavedFilterResolver(searchRequestManager);

        List<SearchRequest> result = filterResolver.getSearchRequest(theUser, Collections.singletonList(createLiteral("")));
        assertThat(result, empty());

        result = filterResolver.getSearchRequest(theUser, Collections.singletonList(createLiteral("    ")));
        assertThat(result, empty());
    }

    @Test
    public void testGetSearchRequestNoSearchRequestById() throws Exception {
        final long searchRequestId = 123L;
        when(searchRequestManager.getSearchRequestById(theUser, searchRequestId)).thenReturn(null);

        final SharedEntitySearchParametersBuilder builder = new SharedEntitySearchParametersBuilder().setName(String.valueOf(searchRequestId)).setTextSearchMode(SharedEntitySearchParameters.TextSearchMode.EXACT);
        when(searchRequestManager.search(builder.toSearchParameters(), theUser, 0, Integer.MAX_VALUE)).thenReturn(new SharedEntitySearchResult<>(from(Collections.<SearchRequest>emptyList()), false, 0));

        final SavedFilterResolver filterResolver = new SavedFilterResolver(searchRequestManager);
        final List<SearchRequest> searchRequest = filterResolver.getSearchRequest(theUser, Collections.singletonList(createLiteral(searchRequestId)));

        assertThat(searchRequest, empty());
    }

    @Test
    public void testGetSearchRequestFoundSearchRequestByNameForId() throws Exception {
        final long searchRequestId = 123L;
        final MockSearchRequest searchRequest = new MockSearchRequest("dude");
        final SharedEntitySearchParametersBuilder builder = new SharedEntitySearchParametersBuilder().setName(String.valueOf(searchRequestId)).setTextSearchMode(SharedEntitySearchParameters.TextSearchMode.EXACT);

        when(searchRequestManager.search(builder.toSearchParameters(), theUser, 0, Integer.MAX_VALUE)).thenReturn(new SharedEntitySearchResult<>(from(newBuilder(searchRequest).asList()), false, 0));
        when(searchRequestManager.getSearchRequestById(theUser, searchRequestId)).thenReturn(null);

        final SavedFilterResolver filterResolver = new SavedFilterResolver(searchRequestManager);
        final List<SearchRequest> list = filterResolver.getSearchRequest(theUser, Collections.singletonList(createLiteral(searchRequestId)));

        assertThat(list, hasItem(searchRequest));
    }

    @Test
    public void testGetSearchRequestFoundSearchRequestByIdForName() throws Exception {
        final long searchRequestId = 123L;
        final MockSearchRequest searchRequest = new MockSearchRequest("dude");
        final SharedEntitySearchParametersBuilder builder = new SharedEntitySearchParametersBuilder().setName(String.valueOf(searchRequestId)).setTextSearchMode(EXACT);

        when(searchRequestManager.search(builder.toSearchParameters(), theUser, 0, Integer.MAX_VALUE)).thenReturn(new SharedEntitySearchResult<>(from(emptyList()), false, 0));
        when(searchRequestManager.getSearchRequestById(theUser, searchRequestId)).thenReturn(searchRequest);

        final SavedFilterResolver filterResolver = new SavedFilterResolver(searchRequestManager);
        final List<SearchRequest> list = filterResolver.getSearchRequest(theUser, singletonList(createLiteral(String.valueOf(searchRequestId))));

        assertThat(list, hasItem(searchRequest));
    }

    @Test
    public void testGetSearchRequestFoundSearchRequestById() throws Exception {
        final long searchRequestId = 123L;
        final MockSearchRequest searchRequest = new MockSearchRequest("dude");

        when(searchRequestManager.getSearchRequestById(theUser, searchRequestId)).thenReturn(searchRequest);

        final SavedFilterResolver filterResolver = new SavedFilterResolver(searchRequestManager);
        final List<SearchRequest> list = filterResolver.getSearchRequest(theUser, singletonList(createLiteral(searchRequestId)));

        assertThat(list, hasItem(searchRequest));
    }

    @Test
    public void testGetSearchRequestFoundSearchRequestByIdOverrideSecurity() throws Exception {
        final long searchRequestId = 1L;
        final MockSearchRequest searchRequest1 = new MockSearchRequest("dude", searchRequestId);

        when(searchRequestManager.getSearchRequestById(searchRequestId)).thenReturn(searchRequest1);

        final SavedFilterResolver filterResolver = new SavedFilterResolver(searchRequestManager);
        final List<SearchRequest> list = filterResolver.getSearchRequestOverrideSecurity(Collections.singletonList(createLiteral(searchRequestId)));

        assertThat(list, Matchers.contains(searchRequest1));
    }

    @Test
    public void testGetSearchRequestNoneFoundByIdOverrideSecurity() throws Exception {
        final long searchRequestId = 3L;
        when(searchRequestManager.getSearchRequestById(searchRequestId)).thenReturn(null);
        when(searchRequestManager.findByNameIgnoreCase(String.valueOf(searchRequestId))).thenReturn(emptyList());

        final SavedFilterResolver filterResolver = new SavedFilterResolver(searchRequestManager);
        final List<SearchRequest> list = filterResolver.getSearchRequestOverrideSecurity(Collections.singletonList(createLiteral(searchRequestId)));

        assertThat(list, empty());
    }

    @Test
    public void testGetSearchRequestFoundMultipleSearchRequestByName() throws Exception {
        final String name = "123";
        final MockSearchRequest searchRequest1 = new MockSearchRequest("dude");
        final MockSearchRequest searchRequest2 = new MockSearchRequest("sweet");

        final SharedEntitySearchParametersBuilder builder = new SharedEntitySearchParametersBuilder().setName(name).setTextSearchMode(EXACT);
        when(searchRequestManager.search(builder.toSearchParameters(), theUser, 0, MAX_VALUE)).thenReturn(new SharedEntitySearchResult<>(from(newBuilder(searchRequest1, searchRequest2).asList()), false, 0));

        final SavedFilterResolver filterResolver = new SavedFilterResolver(searchRequestManager);
        final List<SearchRequest> list = filterResolver.getSearchRequest(theUser, singletonList(createLiteral(name)));

        assertThat(list, Matchers.contains(searchRequest1, searchRequest2));
    }

    @Test
    public void testGetSearchRequestFoundByNameOverrideSecurity() throws Exception {
        final MockSearchRequest searchRequest1 = new MockSearchRequest("dude", 1L, "filter1");

        when(searchRequestManager.findByNameIgnoreCase("FILTER1")).thenReturn(Collections.singletonList(searchRequest1));

        final SavedFilterResolver filterResolver = new SavedFilterResolver(searchRequestManager);
        final List<SearchRequest> list = filterResolver.getSearchRequestOverrideSecurity(Collections.singletonList(createLiteral("FILTER1")));

        assertThat(list, Matchers.contains(searchRequest1));
    }
}
