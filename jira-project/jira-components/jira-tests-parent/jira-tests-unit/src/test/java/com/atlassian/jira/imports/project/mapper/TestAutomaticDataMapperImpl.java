package com.atlassian.jira.imports.project.mapper;

import com.atlassian.jira.action.issue.customfields.option.MockOption;
import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.config.SubTaskManager;
import com.atlassian.jira.external.beans.ExternalCustomField;
import com.atlassian.jira.external.beans.ExternalProject;
import com.atlassian.jira.imports.project.core.BackupProject;
import com.atlassian.jira.imports.project.core.BackupProjectImpl;
import com.atlassian.jira.imports.project.customfield.ExternalCustomFieldConfiguration;
import com.atlassian.jira.imports.project.customfield.ExternalCustomFieldOption;
import com.atlassian.jira.imports.project.util.IssueTypeImportHelper;
import com.atlassian.jira.imports.project.validation.CustomFieldMapperValidator;
import com.atlassian.jira.imports.project.validation.StatusMapperValidator;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.context.JiraContextNode;
import com.atlassian.jira.issue.context.ProjectContext;
import com.atlassian.jira.issue.customfields.CustomFieldType;
import com.atlassian.jira.issue.customfields.option.Option;
import com.atlassian.jira.issue.customfields.option.Options;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.issue.fields.config.FieldConfigScheme;
import com.atlassian.jira.issue.fields.config.manager.FieldConfigSchemeManager;
import com.atlassian.jira.issue.issuetype.MockIssueType;
import com.atlassian.jira.issue.link.IssueLinkTypeManager;
import com.atlassian.jira.issue.link.MockIssueLinkType;
import com.atlassian.jira.issue.priority.MockPriority;
import com.atlassian.jira.issue.resolution.MockResolution;
import com.atlassian.jira.issue.security.IssueSecurityLevelManager;
import com.atlassian.jira.issue.security.IssueSecuritySchemeManager;
import com.atlassian.jira.issue.status.MockStatus;
import com.atlassian.jira.mock.MockProjectManager;
import com.atlassian.jira.mock.ofbiz.MockGenericValue;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.scheme.Scheme;
import com.atlassian.jira.security.roles.ProjectRoleManager;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collection;
import java.util.List;

import static com.atlassian.jira.mock.MockProjectRoleManager.MockProjectRole;
import static com.google.common.collect.ImmutableList.of;
import static java.util.Collections.emptyList;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.doCallRealMethod;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @since v3.13
 */
@RunWith(MockitoJUnitRunner.class)
public class TestAutomaticDataMapperImpl {
    private CustomFieldMapper customFieldMapper;

    @Before
    public void setUp() {
        customFieldMapper = new CustomFieldMapper();
    }

    @Test
    public void testGetIssueSecuritySchemeIdHappyPath() throws Exception {
        final MockProject projectGV = new MockProject();

        final ProjectManager mockProjectManager = mock(ProjectManager.class);
        when(mockProjectManager.getProjectObjByKey("TST")).thenReturn(projectGV);

        final Scheme mockScheme = mock(Scheme.class);
        when(mockScheme.getId()).thenReturn(5L);
        when(mockScheme.getName()).thenReturn("my scheme");
        final IssueSecuritySchemeManager mockIssueSecuritySchemeManager = mock(IssueSecuritySchemeManager.class);
        when(mockIssueSecuritySchemeManager.getSchemeFor(projectGV)).thenReturn(mockScheme);

        final AutomaticDataMapperImpl automaticDataMapper = new AutomaticDataMapperImpl(null, null, null, mockProjectManager, null, null, null, null,
                null, null, mockIssueSecuritySchemeManager, null);

        assertThat(automaticDataMapper.getIssueSecuritySchemeId("TST"), equalTo(5L));
    }

    @Test
    public void testGetIssueSecuritySchemeIdNoProject() throws Exception {
        final ProjectManager mockProjectManager = mock(ProjectManager.class);
        when(mockProjectManager.getProjectObjByKey("TST")).thenReturn(null);

        final AutomaticDataMapperImpl automaticDataMapper = new AutomaticDataMapperImpl(null, null, null, mockProjectManager, null, null, null, null,
                null, null, null, null);

        assertNull(automaticDataMapper.getIssueSecuritySchemeId("TST"));
    }

    @Test
    public void testGetIssueSecuritySchemeIdProjectExistsWithEmptyScheme() throws Exception {
        final MockProject projectGV = new MockProject();

        final ProjectManager mockProjectManager = mock(ProjectManager.class);
        when(mockProjectManager.getProjectObjByKey("TST")).thenReturn(projectGV);

        final IssueSecuritySchemeManager mockIssueSecuritySchemeManager = mock(IssueSecuritySchemeManager.class);
        when(mockIssueSecuritySchemeManager.getSchemeFor(projectGV)).thenReturn(null);

        final AutomaticDataMapperImpl automaticDataMapper = new AutomaticDataMapperImpl(null, null, null, mockProjectManager, null, null, null, null,
                null, null, mockIssueSecuritySchemeManager, null);

        assertNull(automaticDataMapper.getIssueSecuritySchemeId("TST"));
    }

    @Test
    public void testMapIssueSecurityLevelsHappyPath() throws Exception {
        final IssueSecurityLevelManager mockIssueSecurityLevelManager = mock(IssueSecurityLevelManager.class);
        when(mockIssueSecurityLevelManager.getSchemeIssueSecurityLevels(5L)).thenReturn(of(new MockGenericValue("BSThing", ImmutableMap.of("name", "level1", "id",
                4L)), new MockGenericValue("BSThing", ImmutableMap.of("name", "level2", "id", 3L))));

        final SimpleProjectImportIdMapperImpl issueSecurityLevelMapper = new SimpleProjectImportIdMapperImpl();

        issueSecurityLevelMapper.registerOldValue("1", "level1");
        issueSecurityLevelMapper.registerOldValue("2", "level2");
        issueSecurityLevelMapper.registerOldValue("3", "level3");
        final AutomaticDataMapperImpl automaticDataMapper = new AutomaticDataMapperImpl(null, null, null, null, null, null, null, null, null,
                mockIssueSecurityLevelManager, null, null) {
            @Override
            Long getIssueSecuritySchemeId(final String projectKey) {
                return 5L;
            }
        };

        automaticDataMapper.mapIssueSecurityLevels("TST", issueSecurityLevelMapper);
        assertEquals("4", issueSecurityLevelMapper.getMappedId("1"));
        assertEquals("3", issueSecurityLevelMapper.getMappedId("2"));
        assertNull(issueSecurityLevelMapper.getMappedId("3"));
    }

    @Test
    public void testMapIssueSecurityLevelsNoScheme() throws Exception {
        final IssueSecurityLevelManager mockIssueSecurityLevelManager = mock(IssueSecurityLevelManager.class);

        final SimpleProjectImportIdMapperImpl issueSecurityLevelMapper = new SimpleProjectImportIdMapperImpl();

        issueSecurityLevelMapper.registerOldValue("1", "level1");
        issueSecurityLevelMapper.registerOldValue("2", "level2");
        issueSecurityLevelMapper.registerOldValue("3", "level3");
        final AutomaticDataMapperImpl automaticDataMapper = new AutomaticDataMapperImpl(null, null, null, null, null, null, null, null, null,
                mockIssueSecurityLevelManager, null, null) {
            @Override
            Long getIssueSecuritySchemeId(final String projectKey) {
                return null;
            }
        };

        automaticDataMapper.mapIssueSecurityLevels("TST", issueSecurityLevelMapper);
        assertNull(issueSecurityLevelMapper.getMappedId("1"));
        assertNull(issueSecurityLevelMapper.getMappedId("2"));
        assertNull(issueSecurityLevelMapper.getMappedId("3"));
    }

    @Test
    public void testMapIssueTypes() {
        // Create our issueTypeMapper and add the Issue Types found in the import file.
        final IssueTypeMapper issueTypeMapper = new IssueTypeMapper();
        issueTypeMapper.registerOldValue("600", "Task", true);
        issueTypeMapper.registerOldValue("601", "WasteTime", true);
        issueTypeMapper.registerOldValue("602", "Bug", false);
        issueTypeMapper.registerOldValue("603", "New Feature", true);

        final IssueTypeImportHelper mockIssueTypeImportHelper = mock(IssueTypeImportHelper.class);

        final MockIssueType task = new MockIssueType("12", "Task");
        when(mockIssueTypeImportHelper.getIssueTypeForName("Task")).thenReturn(task);

        when(mockIssueTypeImportHelper.getIssueTypeForName("WasteTime")).thenReturn(null);

        final MockIssueType bug = new MockIssueType("10", "Bug");
        when(mockIssueTypeImportHelper.getIssueTypeForName("Bug")).thenReturn(bug);

        final MockIssueType newFeature = new MockIssueType("11", "New Feature");
        when(mockIssueTypeImportHelper.getIssueTypeForName("New Feature")).thenReturn(newFeature);

        when(mockIssueTypeImportHelper.isMappingValid(bug, "TST", false)).thenReturn(true);

        when(mockIssueTypeImportHelper.isMappingValid(task, "TST", true)).thenReturn(true);

        when(mockIssueTypeImportHelper.isMappingValid(newFeature, "TST", true)).thenReturn(false);

        final ExternalProject project = new ExternalProject();
        project.setKey("TST");
        final BackupProject backupProject = new BackupProjectImpl(project, emptyList(), emptyList(), emptyList(),
                of(12L, 14L), 0, ImmutableMap.of());

        final AutomaticDataMapper automaticDataMapper = new AutomaticDataMapperImpl(null, null, null, null, mockIssueTypeImportHelper, null, null,
                null, null, null, null, null);
        automaticDataMapper.mapIssueTypes(backupProject, issueTypeMapper);

        assertEquals("12", issueTypeMapper.getMappedId("600"));
        assertEquals("10", issueTypeMapper.getMappedId("602"));
        // We expect the others to not map
        assertEquals(null, issueTypeMapper.getMappedId("601"));
        assertEquals(null, issueTypeMapper.getMappedId("603"));
    }

    @Test
    public void testMapIssueLinkTypesSubtasksNotEnabled() {
        final IssueLinkTypeMapper issueLinkTypeMapper = new IssueLinkTypeMapper();
        issueLinkTypeMapper.registerOldValue("2", "xxx", "jira_subtask");

        final IssueLinkTypeManager mockIssueLinkTypeManager = mock(IssueLinkTypeManager.class);
        when(mockIssueLinkTypeManager.getIssueLinkTypesByName("xxx")).thenReturn(of(new MockIssueLinkType(102, "xxx", "A", "B", "jira_subtask")));

        final SubTaskManager mockSubTaskManager = mock(SubTaskManager.class);
        when(mockSubTaskManager.isSubTasksEnabled()).thenReturn(false);

        final AutomaticDataMapper automaticDataMapper = new AutomaticDataMapperImpl(null, null, null, null, null, null, null,
                mockIssueLinkTypeManager, mockSubTaskManager, null, null, null);
        automaticDataMapper.mapIssueLinkTypes(issueLinkTypeMapper);

        assertEquals(null, issueLinkTypeMapper.getMappedId("2"));
    }

    @Test
    public void testMapIssueLinkTypes() {
        // Create and populate our IssueLinkTypeMapper
        final IssueLinkTypeMapper issueLinkTypeMapper = new IssueLinkTypeMapper();
        issueLinkTypeMapper.registerOldValue("1", "Duplicate", null);
        issueLinkTypeMapper.registerOldValue("2", "xxx", "jira_subtask");
        issueLinkTypeMapper.registerOldValue("3", "yyy", "green");
        issueLinkTypeMapper.registerOldValue("4", "zzz", null);
        issueLinkTypeMapper.registerOldValue("5", "ImportOnly", null);

        final IssueLinkTypeManager mockIssueLinkTypeManager = mock(IssueLinkTypeManager.class);
        when(mockIssueLinkTypeManager.getIssueLinkTypesByName("Duplicate")).thenReturn(of(new MockIssueLinkType(101, "Duplicate", "duplicates", "is duplicated by", null)));
        when(mockIssueLinkTypeManager.getIssueLinkTypesByName("xxx")).thenReturn(of(new MockIssueLinkType(102, "xxx", "A", "B", "jira_subtask")));
        when(mockIssueLinkTypeManager.getIssueLinkTypesByName("yyy")).thenReturn(of(new MockIssueLinkType(103, "yyy", "A", "B", "jira_subtask")));
        when(mockIssueLinkTypeManager.getIssueLinkTypesByName("zzz")).thenReturn(of(new MockIssueLinkType(104, "zzz", "A", "B", "jira_subtask")));
        when(mockIssueLinkTypeManager.getIssueLinkTypesByName("ImportOnly")).thenReturn(emptyList());

        final SubTaskManager mockSubTaskManager = mock(SubTaskManager.class);
        when(mockSubTaskManager.isSubTasksEnabled()).thenReturn(true);

        final AutomaticDataMapper automaticDataMapper = new AutomaticDataMapperImpl(null, null, null, null, null, null, null,
                mockIssueLinkTypeManager, mockSubTaskManager, null, null, null);
        automaticDataMapper.mapIssueLinkTypes(issueLinkTypeMapper);
        //
        assertEquals("101", issueLinkTypeMapper.getMappedId("1"));
        assertEquals("102", issueLinkTypeMapper.getMappedId("2"));
        // Not Mapped - wrong style
        assertEquals(null, issueLinkTypeMapper.getMappedId("3"));
        // Not Mapped - wrong style
        assertEquals(null, issueLinkTypeMapper.getMappedId("4"));
        // Not Mapped - missing in current system
        assertEquals(null, issueLinkTypeMapper.getMappedId("5"));
    }

    @Test
    public void testMapPriorities() {
        // Set up a mock ConstantsManager
        final ConstantsManager constantsManager = mock(ConstantsManager.class);
        when(constantsManager.getPriorityObjects()).thenReturn(ImmutableList.of(new MockPriority("10", "P1"), new MockPriority("11", "P2"), new MockPriority("12", "P3")));

        // Create our mapper and add the stuff found in the import file.
        final SimpleProjectImportIdMapper mapper = new SimpleProjectImportIdMapperImpl();
        mapper.registerOldValue("41", "P3");
        mapper.registerOldValue("42", "P4");
        mapper.registerOldValue("43", "P1");
        mapper.registerOldValue("44", "P2");

        final AutomaticDataMapper automaticDataMapper = new AutomaticDataMapperImpl(constantsManager, null, null, null, null, null, null, null, null,
                null, null, null);
        automaticDataMapper.mapPriorities(mapper);

        // Check the Old ID to new ID mapping
        assertEquals("12", mapper.getMappedId("41"));
        assertEquals("10", mapper.getMappedId("43"));
        assertEquals("11", mapper.getMappedId("44"));
        // P4 could not be mapped.
        assertEquals(null, mapper.getMappedId("42"));
    }

    @Test
    public void testMapResolutions() {
        // Set up a mock ConstantsManager
        final ConstantsManager constantsManager = mock(ConstantsManager.class);
        when(constantsManager.getResolutionObjects()).thenReturn(ImmutableList.of(new MockResolution("27", "Fixed"),
                new MockResolution("28", "Duplicate"), new MockResolution("29", "Ignored")));

        // Create our mapper and add the stuff found in the import file.
        final SimpleProjectImportIdMapper mapper = new SimpleProjectImportIdMapperImpl();
        mapper.registerOldValue("41", "Duplicate");
        mapper.registerOldValue("42", "Fixed");
        mapper.registerOldValue("43", "Whatever");

        final AutomaticDataMapper automaticDataMapper = new AutomaticDataMapperImpl(constantsManager, null, null, null, null, null, null, null, null,
                null, null, null);
        automaticDataMapper.mapResolutions(mapper);

        // Check the Old ID to new ID mapping
        assertEquals("28", mapper.getMappedId("41"));
        assertEquals("27", mapper.getMappedId("42"));
        // "Whatever" could not be mapped.
        assertEquals(null, mapper.getMappedId("43"));
    }

    @Test
    public void testMapStatuses() {
        // Set up a mock ConstantsManager

        final ConstantsManager mockConstantsManager = mock(ConstantsManager.class);
        final MockStatus closedStatus = new MockStatus("30", "Closed");
        when(mockConstantsManager.getStatusByName("Closed")).thenReturn(closedStatus);
        when(mockConstantsManager.getStatusByName("In Progress")).thenReturn(null);
        final MockStatus openStatus = new MockStatus("27", "Open");
        when(mockConstantsManager.getStatusByName("Open")).thenReturn(openStatus);

        // Create our mapper and add the stuff found in the import file.
        final StatusMapper mapper = new StatusMapper();
        mapper.registerOldValue("41", "Closed");
        mapper.registerOldValue("42", "Open");
        mapper.registerOldValue("43", "In Progress");
        mapper.flagValueAsRequired("41", "111");
        mapper.flagValueAsRequired("42", "222");

        final IssueTypeMapper issueTypeMapper = new IssueTypeMapper();
        issueTypeMapper.mapValue("111", "333");
        issueTypeMapper.mapValue("222", "444");

        final ExternalProject project = new ExternalProject();
        project.setKey("TST");
        final BackupProject backupProject = new BackupProjectImpl(project, emptyList(), emptyList(), emptyList(),
                of(12L, 14L), 0, ImmutableMap.of());

        final StatusMapperValidator mockStatusMapperValidator = mock(StatusMapperValidator.class);
        when(mockStatusMapperValidator.isStatusValid("41", closedStatus, mapper, issueTypeMapper, "TST")).thenReturn(true);
        when(mockStatusMapperValidator.isStatusValid("42", openStatus, mapper, issueTypeMapper, "TST")).thenReturn(false);

        final AutomaticDataMapper automaticDataMapper = new AutomaticDataMapperImpl(mockConstantsManager, null, null, null, null,
                mockStatusMapperValidator, null, null, null, null, null, null);
        automaticDataMapper.mapStatuses(backupProject, mapper, issueTypeMapper);

        // Mapped status
        assertEquals("30", mapper.getMappedId("41"));
        // "Whatever" could not be mapped.
        assertNull(mapper.getMappedId("43"));
        assertNull(mapper.getMappedId("42"));
    }

    @Test
    public void testMapCustomFieldsFieldTypeNotImportable() {
        // Create a mock CustomFieldMapperValidator
        final CustomFieldMapperValidator customFieldMapperValidator = mock(CustomFieldMapperValidator.class);
        when(customFieldMapperValidator.customFieldTypeIsImportable("a.b.c:Colour")).thenReturn(Boolean.FALSE);

        final ExternalProject project = new ExternalProject();
        project.setId("2");
        project.setKey("MKY");
        final List customFieldConfigurations = ImmutableList.of(new ExternalCustomFieldConfiguration(null, null, new ExternalCustomField("13", "John",
                "a.b.c:Colour"), "12"));
        final BackupProject backupProject = new BackupProjectImpl(project, emptyList(), emptyList(), customFieldConfigurations,
                emptyList(), 0, ImmutableMap.of());

        final AutomaticDataMapperImpl automaticDataMapper = new AutomaticDataMapperImpl(null, null, customFieldMapperValidator, null, null, null,
                null, null, null, null, null, null);
        automaticDataMapper.mapCustomFields(backupProject, customFieldMapper, null);
        // This should have not mapped any fields
        assertNull(customFieldMapper.getMappedId("13"));
    }

    @Test
    public void testMapCustomFieldsFieldMissing() {
        // Create a mock CustomFieldMapperValidator
        final CustomFieldMapperValidator customFieldMapperValidator = mock(CustomFieldMapperValidator.class);
        when(customFieldMapperValidator.customFieldTypeIsImportable("a.b.c:Colour")).thenReturn(Boolean.TRUE);

        final ExternalProject project = new ExternalProject();
        project.setId("2");
        project.setKey("MKY");
        final List customFieldConfigurations = ImmutableList.of(new ExternalCustomFieldConfiguration(null, null, new ExternalCustomField("13", "John",
                "a.b.c:Colour"), "12"));
        final BackupProject backupProject = new BackupProjectImpl(project, emptyList(), emptyList(), customFieldConfigurations,
                emptyList(), 0, ImmutableMap.of());

        // Make a mock custom field manager
        final CustomFieldManager customFieldManager = mock(CustomFieldManager.class);
        // We want to test when no custom fields with the given name exist
        when(customFieldManager.getCustomFieldObjectsByName(anyString())).thenReturn(emptyList());

        final AutomaticDataMapperImpl automaticDataMapper = new AutomaticDataMapperImpl(null, customFieldManager,
                customFieldMapperValidator, null, null, null, null, null, null, null, null, null);
        automaticDataMapper.mapCustomFields(backupProject, customFieldMapper, null);
        // This should have not mapped any fields
        assertNull(customFieldMapper.getMappedId("13"));
    }

    @Test
    public void testMapCustomFieldsOrphanRequiredValueGetsIgnored() {
        // Create a mock CustomFieldMapperValidator
        final CustomFieldMapperValidator customFieldMapperValidator = mock(CustomFieldMapperValidator.class);
        when(customFieldMapperValidator.customFieldTypeIsImportable("a.b.c:Colour")).thenReturn(Boolean.TRUE);

        final ExternalProject project = new ExternalProject();
        project.setId("2");
        project.setKey("MKY");
        final List customFieldConfigurations = ImmutableList.of(new ExternalCustomFieldConfiguration(null, null, new ExternalCustomField("13", "John",
                "a.b.c:Colour"), "12"));
        final BackupProject backupProject = new BackupProjectImpl(project, emptyList(), emptyList(), customFieldConfigurations,
                emptyList(), 0, ImmutableMap.of());

        // Make a mock custom field manager
        final CustomFieldManager customFieldManager = mock(CustomFieldManager.class);
        // We want to test when no custom fields with the given name exist
        when(customFieldManager.getCustomFieldObjectsByName(anyString())).thenReturn(emptyList());

        final AutomaticDataMapperImpl automaticDataMapper = new AutomaticDataMapperImpl(null, customFieldManager,
                customFieldMapperValidator, null, null, null, null, null, null, null, null, null);
        // Flag custom field 12 as required but don't register it
        customFieldMapper.flagValueAsRequired("12", "12");

        automaticDataMapper.mapCustomFields(backupProject, customFieldMapper, null);
        // This should have not mapped any fields
        assertNull(customFieldMapper.getMappedId("13"));
        // This should also have flagged the field as ignored
        assertTrue(customFieldMapper.isIgnoredCustomField("12"));
    }

    @Test
    public void testMapCustomFieldsFieldWrongType() {
        // Create a mock CustomFieldMapperValidator
        final CustomFieldMapperValidator customFieldMapperValidator = mock(CustomFieldMapperValidator.class);
        when(customFieldMapperValidator.customFieldTypeIsImportable("a.b.c:Colour")).thenReturn(Boolean.TRUE);

        final MockProjectManager mockProjectManager = new MockProjectManager();
        mockProjectManager.addProject(new MockGenericValue("Project", ImmutableMap.of("id", 2L, "key", "MKY")));

        final ExternalProject project = new ExternalProject();
        project.setId("2");
        project.setKey("MKY");
        final List customFieldConfigurations = ImmutableList.of(new ExternalCustomFieldConfiguration(null, null, new ExternalCustomField("13", "John",
                "a.b.c:Colour"), "12"));
        final BackupProject backupProject = new BackupProjectImpl(project, emptyList(), emptyList(), customFieldConfigurations,
                emptyList(), 0, ImmutableMap.of());

        // Make a mock custom field manager
        final CustomFieldManager customFieldManager = mock(CustomFieldManager.class);

        // Make a mock Custom Field Type
        final CustomFieldType customFieldType = mock(CustomFieldType.class);
        when(customFieldType.getKey()).thenReturn("a.b.c:WRONG");

        // Make a mock Custom Field
        final CustomField customField = mock(CustomField.class);
        when(customField.getCustomFieldType()).thenReturn(customFieldType);

        when(customFieldManager.getCustomFieldObjectsByName("John")).thenReturn(ImmutableList.of(customField));

        final AutomaticDataMapperImpl automaticDataMapper = new AutomaticDataMapperImpl(null, customFieldManager,
                customFieldMapperValidator, null, null, null, null, null, null, null, null, null);
        automaticDataMapper.mapCustomFields(backupProject, customFieldMapper, null);
        // This should have not mapped any fields
        assertNull(customFieldMapper.getMappedId("13"));
    }

    @Test
    public void testMapCustomFieldsFieldInvalidForIssueType() {
        final CustomFieldMapperValidator customFieldMapperValidator = mock(CustomFieldMapperValidator.class);
        when(customFieldMapperValidator.customFieldIsValidForRequiredContexts(
                any(ExternalCustomFieldConfiguration.class), any(CustomField.class), anyString(), any(CustomFieldMapper.class), any(IssueTypeMapper.class), anyString()
        )).thenReturn(Boolean.FALSE);
        when(customFieldMapperValidator.customFieldTypeIsImportable("a.b.c:Colour")).thenReturn(Boolean.TRUE);

        final ExternalProject project = new ExternalProject();
        project.setId("2");
        project.setKey("MKY");
        final List customFieldConfigurations = ImmutableList.of(new ExternalCustomFieldConfiguration(null, null, new ExternalCustomField("13", "John",
                "a.b.c:Colour"), "12"));
        final BackupProject backupProject = new BackupProjectImpl(project, emptyList(), emptyList(), customFieldConfigurations,
                emptyList(), 0, ImmutableMap.of());

        // Make a mock custom field manager
        final CustomFieldManager customFieldManager = mock(CustomFieldManager.class);

        // Make a mock Custom Field Type
        final CustomFieldType customFieldType = mock(CustomFieldType.class);
        when(customFieldType.getKey()).thenReturn("a.b.c:Colour");

        // Make a mock Custom Field
        final CustomField customField = mock(CustomField.class);
        when(customField.getCustomFieldType()).thenReturn(customFieldType);

        when(customFieldManager.getCustomFieldObjectsByName("John")).thenReturn(ImmutableList.of(customField));

        // Set up the Mapper with some values.
        customFieldMapper.flagValueAsRequired("13", "2001");
        customFieldMapper.flagIssueTypeInUse("2001", "1");

        final AutomaticDataMapperImpl automaticDataMapper = new AutomaticDataMapperImpl(null, customFieldManager,
                customFieldMapperValidator, null, null, null, null, null, null, null, null, null);
        automaticDataMapper.mapCustomFields(backupProject, customFieldMapper, null);
        // This should have not mapped any fields
        assertNull(customFieldMapper.getMappedId("13"));
    }

    @Test
    public void testMapCustomFieldsFieldSimpleHappyPath() {
        final CustomFieldMapperValidator customFieldMapperValidator = mock(CustomFieldMapperValidator.class);
        when(customFieldMapperValidator.customFieldIsValidForRequiredContexts(
                any(ExternalCustomFieldConfiguration.class), any(CustomField.class), anyString(), any(CustomFieldMapper.class), any(IssueTypeMapper.class), anyString()
        )).thenReturn(Boolean.TRUE);
        when(customFieldMapperValidator.customFieldTypeIsImportable("a.b.c:Colour")).thenReturn(Boolean.TRUE);

        final ExternalProject project = new ExternalProject();
        project.setId("2");
        project.setKey("MKY");
        final List customFieldConfigurations = ImmutableList.of(new ExternalCustomFieldConfiguration(null, null, new ExternalCustomField("13", "John",
                "a.b.c:Colour"), "12"));
        final BackupProject backupProject = new BackupProjectImpl(project, emptyList(), emptyList(), customFieldConfigurations,
                emptyList(), 0, ImmutableMap.of());

        // Make a mock custom field manager
        final CustomFieldManager customFieldManager = mock(CustomFieldManager.class);

        // Make a mock Custom Field Type
        final CustomFieldType customFieldType = mock(CustomFieldType.class);
        when(customFieldType.getKey()).thenReturn("a.b.c:Colour");

        // Make a mock Custom Field
        final CustomField customField = mock(CustomField.class);
        when(customField.getIdAsLong()).thenReturn(1000000L);
        when(customField.getCustomFieldType()).thenReturn(customFieldType);
        // !!! We want to return NOT null because we want the Custom Field to no be valid for the reuired Issue Type.

        when(customFieldManager.getCustomFieldObjectsByName("John")).thenReturn(ImmutableList.of(customField));

        // Set up the Mapper with some values.
        customFieldMapper.registerOldValue("13", "Test Custom Field");
        customFieldMapper.flagValueAsRequired("13", "2001");
        customFieldMapper.flagIssueTypeInUse("2001", "1");

        final AutomaticDataMapperImpl automaticDataMapper = new AutomaticDataMapperImpl(null, customFieldManager,
                customFieldMapperValidator, null, null, null, null, null, null, null, null, null);
        automaticDataMapper.mapCustomFields(backupProject, customFieldMapper, null);
        // This should have just one mapped field
        assertEquals("1000000", customFieldMapper.getMappedId("13"));
        assertNull(customFieldMapper.getMappedId("14"));
    }

    @Test
    public void testMapProjectSimpleHappyPath() {
        final ProjectManager projectManager = mock(ProjectManager.class);

        when(projectManager.getProjectObjects()).thenReturn(ImmutableList.of(new MockProject(1L, "TST"), new MockProject(2L, "QQQ")));

        final AutomaticDataMapperImpl automaticDataMapper = new AutomaticDataMapperImpl(null, null, null,
                projectManager, null, null, null, null, null, null, null, null);
        final SimpleProjectImportIdMapper projectMapper = new SimpleProjectImportIdMapperImpl();
        projectMapper.registerOldValue("432", "QQQ");
        projectMapper.registerOldValue("433", "UWM");
        automaticDataMapper.mapProjects(projectMapper);
        assertEquals("2", projectMapper.getMappedId("432"));
        assertNull(projectMapper.getMappedId("433"));
        assertNull(projectMapper.getMappedId("1"));
        assertNull(projectMapper.getMappedId("2"));
    }

    @Test
    public void testMapOptionsForParent() throws Exception {
        final ExternalCustomFieldOption oldParentOption = new ExternalCustomFieldOption("111", "222", "333", null, "Choc");
        final ExternalCustomFieldOption oldParentOption2 = new ExternalCustomFieldOption("1", "2", "3", null, "lkjasdf");

        final Options mockOptions = mock(Options.class);

        when(mockOptions.getOptionForValue("Choc", null)).thenReturn(new MockOption(null, null, null, "Choc", null, 555L));

        when(mockOptions.getOptionForValue("lkjasdf", null)).thenReturn(null);

        final CustomFieldOptionMapper customFieldOptionsMapper = new CustomFieldOptionMapper();
        customFieldOptionsMapper.registerOldValue(oldParentOption);
        customFieldOptionsMapper.registerOldValue(oldParentOption2);

        final AutomaticDataMapperImpl automaticDataMapper = new AutomaticDataMapperImpl(null, null, null, null, null, null, null, null, null, null,
                null, null);

        automaticDataMapper.mapOptions(ImmutableList.of(mockOptions), customFieldOptionsMapper, of(oldParentOption, oldParentOption2));

        assertEquals("555", customFieldOptionsMapper.getMappedId("111"));
        assertEquals(null, customFieldOptionsMapper.getMappedId("1"));
    }

    @Test
    public void testMapOptionsForParentAndChildren() throws Exception {
        final ExternalCustomFieldOption oldParentOption = new ExternalCustomFieldOption("111", "222", "333", null, "Choc");
        final ExternalCustomFieldOption oldChildOption = new ExternalCustomFieldOption("666", "222", "333", "111", "Straw");
        final ExternalCustomFieldOption oldChildOption2 = new ExternalCustomFieldOption("6", "2", "3", "111", "Van");

        final Options mockOptions = mock(Options.class);

        when(mockOptions.getOptionForValue("Choc", null)).thenReturn(new MockOption(null, null, null, "Choc", null, 555L));

        when(mockOptions.getOptionForValue("Straw", 555L)).thenReturn(new MockOption(null, null, null, "Straw", null, 777L));

        when(mockOptions.getOptionForValue("Van", 555L)).thenReturn(null);

        final CustomFieldOptionMapper customFieldOptionsMapper = new CustomFieldOptionMapper();
        customFieldOptionsMapper.registerOldValue(oldParentOption);
        customFieldOptionsMapper.registerOldValue(oldChildOption);
        customFieldOptionsMapper.registerOldValue(oldChildOption2);

        final AutomaticDataMapperImpl automaticDataMapper = new AutomaticDataMapperImpl(null, null, null, null, null, null, null, null, null, null,
                null, null);

        automaticDataMapper.mapOptions(ImmutableList.of(mockOptions), customFieldOptionsMapper, of(oldParentOption));

        assertEquals("555", customFieldOptionsMapper.getMappedId("111"));
        assertEquals("777", customFieldOptionsMapper.getMappedId("666"));
        assertEquals(null, customFieldOptionsMapper.getMappedId("6"));
    }

    @Test
    public void testGetNewOptionsNoMapping() throws Exception {
        final AutomaticDataMapperImpl automaticDataMapper = new AutomaticDataMapperImpl(null, null, null, null, null, null, null, null, null, null,
                null, null);
        final ExternalCustomFieldConfiguration externalCustomFieldConfiguration = new ExternalCustomFieldConfiguration(null, null,
                new ExternalCustomField("12", "", ""), "432");

        final List<Options> optionsList = automaticDataMapper.getNewOptions(null, customFieldMapper, null, externalCustomFieldConfiguration);

        assertNotNull(optionsList);
        assertThat(optionsList, hasSize(0));
    }

    @Test
    public void testGetNewOptions() throws Exception {
        final ExternalCustomFieldConfiguration externalCustomFieldConfiguration = new ExternalCustomFieldConfiguration(null, null,
                new ExternalCustomField("1", "CustoField", "some.key"), "2");

        final ProjectContext mockProjectContext = mock(ProjectContext.class);

        final CustomField mockCustomField = mock(CustomField.class);
        when(mockCustomField.getOptions(null, null, mockProjectContext)).thenReturn(null);

        final CustomFieldManager mockCustomFieldManager = mock(CustomFieldManager.class);
        when(mockCustomFieldManager.getCustomFieldObject(2L)).thenReturn(mockCustomField);

        final ProjectManager mockProjectManager = mock(ProjectManager.class);
        when(mockProjectManager.getProjectObjByKey("TST")).thenReturn(null);

        final FieldConfigSchemeManager mockFieldConfigSchemeManager = mock(FieldConfigSchemeManager.class);
        when(mockFieldConfigSchemeManager.getRelevantConfigScheme(mockProjectContext, mockCustomField)).thenReturn(null);

        final AutomaticDataMapperImpl automaticDataMapper = new AutomaticDataMapperImpl(null, mockCustomFieldManager, null, mockProjectManager, null,
                null, null, null, null, null, null, mockFieldConfigSchemeManager) {
            @Override
            JiraContextNode getProjectContext(final Long newProjectId) {
                return mockProjectContext;
            }
        };

        final ExternalProject project = new ExternalProject();
        project.setKey("TST");
        final BackupProject backupProject = new BackupProjectImpl(project, emptyList(), emptyList(), emptyList(),
                emptyList(), 0, ImmutableMap.of());

        final IssueTypeMapper issueTypeMapper = new IssueTypeMapper();
        issueTypeMapper.mapValue("3", "4");

        customFieldMapper.mapValue("1", "2");

        final List<Options> options = automaticDataMapper.getNewOptions(backupProject, customFieldMapper, issueTypeMapper, externalCustomFieldConfiguration);
        assertThat(options, hasSize(0));
    }

    @Test
    public void testGetNewOptionsRelevantConfigFound() throws Exception {
        final ExternalCustomFieldConfiguration externalCustomFieldConfiguration = new ExternalCustomFieldConfiguration(null, null,
                new ExternalCustomField("1", "CustoField", "some.key"), "2");

        final ProjectContext mockProjectContext = mock(ProjectContext.class);
        final Project mockProject = mock(Project.class);

        final CustomField mockCustomField = mock(CustomField.class);
        when(mockCustomField.getOptions(null, null, null)).thenReturn(null);

        final CustomFieldManager mockCustomFieldManager = Mockito.mock(CustomFieldManager.class);
        when(mockCustomFieldManager.getCustomFieldObject(2L)).thenReturn(mockCustomField);

        final ProjectManager mockProjectManager = mock(ProjectManager.class);
        when(mockProjectManager.getProjectObjByKey("TST")).thenReturn(mockProject);

        final FieldConfigSchemeManager mockFieldConfigSchemeManager = mock(FieldConfigSchemeManager.class);
        final FieldConfigScheme mockFieldConfigSheme = mock(FieldConfigScheme.class);
        final FieldConfig mockFieldConfig = mock(FieldConfig.class);
        when(mockFieldConfigSchemeManager.getRelevantConfigScheme(mockProject, mockCustomField)).thenReturn(mockFieldConfigSheme);
        when(mockFieldConfigSheme.getOneAndOnlyConfig()).thenReturn(mockFieldConfig);

        final AutomaticDataMapperImpl automaticDataMapper = new AutomaticDataMapperImpl(null, mockCustomFieldManager, null, mockProjectManager, null,
                null, null, null, null, null, null, mockFieldConfigSchemeManager) {
            @Override
            JiraContextNode getProjectContext(final Long newProjectId) {
                return mockProjectContext;
            }
        };

        final ExternalProject project = new ExternalProject();
        project.setKey("TST");
        final BackupProject backupProject = new BackupProjectImpl(project, emptyList(), emptyList(), emptyList(),
                emptyList(), 0, ImmutableMap.of());

        final IssueTypeMapper issueTypeMapper = new IssueTypeMapper();
        issueTypeMapper.mapValue("3", "4");

        customFieldMapper.mapValue("1", "2");

        automaticDataMapper.getNewOptions(backupProject, customFieldMapper, issueTypeMapper, externalCustomFieldConfiguration);

        verify(mockFieldConfigSchemeManager, times(2)).getRelevantConfigScheme(Mockito.<Project>any(), any());

        final ArgumentCaptor<FieldConfig> fieldConfigCaptor = ArgumentCaptor.forClass(FieldConfig.class);
        verify(mockCustomField, times(1)).getOptions(Mockito.anyString(), fieldConfigCaptor.capture(), Mockito.any());
        assertThat(fieldConfigCaptor.getValue(), sameInstance(mockFieldConfig));
    }


    /**
     * This test is big and everything is repeated twice here.
     * It checks mapping of custom field {@link Option}s in case if there are 2 configurations in source and target JIRA instances.
     */
    @Test
    public void testMapCustomFieldOptionsTwoExternalConfigs() {
        //given
        final ExternalCustomField externalCustomField = new ExternalCustomField("1", "CustoField", "some.key");
        final ExternalCustomFieldConfiguration externalCustomFieldConfigurationGlobal = new ExternalCustomFieldConfiguration(null, null,
                externalCustomField, "1");
        final ExternalCustomFieldConfiguration externalCustomFieldConfigurationProject = new ExternalCustomFieldConfiguration(null, "23",
                externalCustomField, "2");

        final ProjectContext mockProjectContext = mock(ProjectContext.class);
        final Project mockProject = mock(Project.class);

        final CustomField mockCustomField = mock(CustomField.class);
        when(mockCustomField.getOptions(null, null, null)).thenReturn(null);

        final CustomFieldManager mockCustomFieldManager = Mockito.mock(CustomFieldManager.class);
        when(mockCustomFieldManager.getCustomFieldObject(2L)).thenReturn(mockCustomField);

        final ProjectManager mockProjectManager = mock(ProjectManager.class);
        when(mockProjectManager.getProjectObjByKey("TST")).thenReturn(mockProject);

        final FieldConfigSchemeManager mockFieldConfigSchemeManager = mock(FieldConfigSchemeManager.class);
        final FieldConfigScheme mockFieldConfigShemeGlobal = mock(FieldConfigScheme.class);
        final FieldConfigScheme mockFieldConfigShemeProject = mock(FieldConfigScheme.class);
        final FieldConfig mockFieldConfigGlobal = mock(FieldConfig.class);
        final FieldConfig mockFieldConfigProject = mock(FieldConfig.class);
        when(mockFieldConfigSchemeManager.getRelevantConfigScheme(mockProject, mockCustomField)).thenReturn(mockFieldConfigShemeProject);
        when(mockFieldConfigSchemeManager.getRelevantConfigScheme((Project) null, mockCustomField)).thenReturn(mockFieldConfigShemeGlobal);
        when(mockFieldConfigShemeGlobal.getOneAndOnlyConfig()).thenReturn(mockFieldConfigGlobal);
        when(mockFieldConfigShemeProject.getOneAndOnlyConfig()).thenReturn(mockFieldConfigProject);

        final Options mockOptionsGlobal = mock(Options.class);
        final Options mockOptionsProject = mock(Options.class);
        when(mockOptionsGlobal.getOptionForValue("a", null)).thenReturn(new MockOption(null, null, 1L, "a", mockFieldConfigGlobal, 401L));
        when(mockOptionsProject.getOptionForValue("b", null)).thenReturn(new MockOption(null, null, 1L, "b", mockFieldConfigProject, 501L));
        when(mockCustomField.getOptions(null, mockFieldConfigGlobal, null)).thenReturn(mockOptionsGlobal);
        when(mockCustomField.getOptions(null, mockFieldConfigProject, null)).thenReturn(mockOptionsProject);

        final AutomaticDataMapperImpl automaticDataMapper = new AutomaticDataMapperImpl(null, mockCustomFieldManager, null, mockProjectManager, null,
                null, null, null, null, null, null, mockFieldConfigSchemeManager) {
            @Override
            JiraContextNode getProjectContext(final Long newProjectId) {
                return mockProjectContext;
            }
        };

        final ExternalProject project = new ExternalProject();
        project.setKey("TST");
        final BackupProject backupProject = new BackupProjectImpl(project, emptyList(), emptyList(),
                ImmutableList.of(externalCustomFieldConfigurationGlobal, externalCustomFieldConfigurationProject),
                emptyList(), 0, ImmutableMap.of());

        final IssueTypeMapper issueTypeMapper = new IssueTypeMapper();
        issueTypeMapper.mapValue("3", "4");

        customFieldMapper.mapValue("1", "2");

        final CustomFieldOptionMapper customFieldOptionMapper = spy(new CustomFieldOptionMapper());
        when(customFieldOptionMapper.getParentOptions("1")).thenReturn(ImmutableList.of(
                new ExternalCustomFieldOption("200", "1", "20", null, "a"),
                new ExternalCustomFieldOption("201", "1", "20", null, "b")
        ));
        when(customFieldOptionMapper.getParentOptions("2")).thenReturn(ImmutableList.of(
                new ExternalCustomFieldOption("300", "1", "20", null, "a"),
                new ExternalCustomFieldOption("301", "1", "20", null, "b")
        ));
        doCallRealMethod().when(customFieldOptionMapper).mapValue(anyString(), anyString());

        //when
        automaticDataMapper.mapCustomFieldOptions(backupProject, customFieldOptionMapper, customFieldMapper, issueTypeMapper);

        //then
        verify(mockFieldConfigSchemeManager, times(4)).getRelevantConfigScheme(Mockito.<Project>any(), any());
        verify(customFieldOptionMapper, times(4)).mapValue(anyString(), anyString());
        assertThat(customFieldOptionMapper.getMappedId("200"), is("401"));
        assertThat(customFieldOptionMapper.getMappedId("201"), is("501"));
        assertThat(customFieldOptionMapper.getMappedId("300"), is("401"));
        assertThat(customFieldOptionMapper.getMappedId("301"), is("501"));

        final ArgumentCaptor<FieldConfig> fieldConfigCaptor = ArgumentCaptor.forClass(FieldConfig.class);
        verify(mockCustomField, times(4)).getOptions(Mockito.anyString(), fieldConfigCaptor.capture(), Mockito.any());
        assertThat(fieldConfigCaptor.getAllValues().get(0), sameInstance(mockFieldConfigProject));
        assertThat(fieldConfigCaptor.getAllValues().get(1), sameInstance(mockFieldConfigGlobal));
        assertThat(fieldConfigCaptor.getAllValues().get(2), sameInstance(mockFieldConfigProject));
        assertThat(fieldConfigCaptor.getAllValues().get(3), sameInstance(mockFieldConfigGlobal));
    }

    @Test
    public void testMapCustomFieldOptions() throws Exception {
        //given
        final ExternalProject project = new ExternalProject();
        project.setKey("TST");
        final ExternalCustomFieldConfiguration externalCustomFieldConfiguration1 = new ExternalCustomFieldConfiguration(null, null,
                new ExternalCustomField("1", "Friend", "123"), "12");
        final ExternalCustomFieldConfiguration externalCustomFieldConfiguration2 = new ExternalCustomFieldConfiguration(null, null,
                new ExternalCustomField("2", "Schmoo", "4312"), "24");
        final List customFieldConfigurations = ImmutableList.of(externalCustomFieldConfiguration1, externalCustomFieldConfiguration2);
        final BackupProject backupProject = new BackupProjectImpl(project, emptyList(), emptyList(), customFieldConfigurations,
                emptyList(), 0, ImmutableMap.of());

        final CustomFieldOptionMapper customFieldOptionMapper = new CustomFieldOptionMapper();
        final ExternalCustomFieldOption customFieldOption = new ExternalCustomFieldOption("1", "40", "12", null, "Rum n Raisin");
        customFieldOptionMapper.registerOldValue(customFieldOption);

        final Options mockOptions = mock(Options.class);
        final ArgumentCaptor<List<Options>> optionsCaptor = new ArgumentCaptor<>();
        final ArgumentCaptor<Collection<ExternalCustomFieldOption>> externalOptionsCaptor = new ArgumentCaptor<>();

        final AutomaticDataMapperImpl automaticDataMapper = spy(new AutomaticDataMapperImpl(null, null, null, null, null, null, null, null, null, null, null, null));
        doReturn(ImmutableList.of(mockOptions)).when(automaticDataMapper).getNewOptions(any(), any(), any(), any());
        doCallRealMethod().when(automaticDataMapper).mapOptions(optionsCaptor.capture(), any(), externalOptionsCaptor.capture());

        //when
        automaticDataMapper.mapCustomFieldOptions(backupProject, customFieldOptionMapper, null, null);

        //then
        verify(automaticDataMapper, times(1)).getNewOptions(any(), any(), any(), any());
        verify(automaticDataMapper, times(1)).mapOptions(any(), any(), any());

        assertThat(optionsCaptor.getValue(), hasSize(1));
        assertThat(optionsCaptor.getValue().get(0), is(mockOptions));

        assertThat(externalOptionsCaptor.getValue(), hasSize(1));
        assertThat(externalOptionsCaptor.getValue().iterator().next(), is(customFieldOption));

    }

    @Test
    public void testMapCustomFieldOptionsCustomFieldNotMapped() throws Exception {
        //given
        final ExternalProject project = new ExternalProject();
        project.setKey("TST");
        final ExternalCustomFieldConfiguration externalCustomFieldConfiguration1 = new ExternalCustomFieldConfiguration(null, null,
                new ExternalCustomField("1", "Friend", "123"), "12");
        final List customFieldConfigurations = ImmutableList.of(externalCustomFieldConfiguration1);
        final BackupProject backupProject = new BackupProjectImpl(project, emptyList(), emptyList(), customFieldConfigurations,
                emptyList(), 0, ImmutableMap.of());

        final CustomFieldOptionMapper customFieldOptionMapper = new CustomFieldOptionMapper();
        final ExternalCustomFieldOption customFieldOption = new ExternalCustomFieldOption("1", "40", "12", null, "Rum n Raisin");
        customFieldOptionMapper.registerOldValue(customFieldOption);

        final ArgumentCaptor<List<Options>> optionsCaptor = new ArgumentCaptor<>();
        final ArgumentCaptor<Collection<ExternalCustomFieldOption>> externalOptionsCaptor = new ArgumentCaptor<>();

        final AutomaticDataMapperImpl automaticDataMapper = spy(new AutomaticDataMapperImpl(null, null, null, null, null, null, null, null, null, null, null, null));
        doReturn(emptyList()).when(automaticDataMapper).getNewOptions(any(), any(), any(), any());
        doCallRealMethod().when(automaticDataMapper).mapOptions(optionsCaptor.capture(), any(), externalOptionsCaptor.capture());

        //when
        automaticDataMapper.mapCustomFieldOptions(backupProject, customFieldOptionMapper, null, null);

        //then
        verify(automaticDataMapper, times(1)).getNewOptions(any(), any(), any(), any());
        verify(automaticDataMapper, times(1)).mapOptions(any(), any(), any());

        assertThat(optionsCaptor.getValue(), hasSize(0));

        assertThat(externalOptionsCaptor.getValue(), hasSize(1));
        assertThat(externalOptionsCaptor.getValue().iterator().next(), is(customFieldOption));
    }

    @Test
    public void testMapProjectRoles() {
        // Set up a mock ProjectRoleManager

        final ProjectRoleManager mockProjectRoleManager = mock(ProjectRoleManager.class);
        when(mockProjectRoleManager.getProjectRole("Administrators")).thenReturn(new MockProjectRole(12, "Administrators", ""));
        when(mockProjectRoleManager.getProjectRole("Developers")).thenReturn(new MockProjectRole(13, "Developers", ""));
        when(mockProjectRoleManager.getProjectRole("Users")).thenReturn(null);

        // Create our mapper and add the stuff found in the import file.
        final SimpleProjectImportIdMapper mapper = new SimpleProjectImportIdMapperImpl();
        mapper.registerOldValue("41", "Administrators");
        mapper.registerOldValue("42", "Developers");
        mapper.registerOldValue("43", "Users");

        final AutomaticDataMapper automaticDataMapper = new AutomaticDataMapperImpl(null, null, null, null, null, null, mockProjectRoleManager, null,
                null, null, null, null);
        automaticDataMapper.mapProjectRoles(mapper);

        // Check the Old ID to new ID mapping
        assertEquals("12", mapper.getMappedId("41"));
        assertEquals("13", mapper.getMappedId("42"));
        // "Users" could not be mapped.
        assertEquals(null, mapper.getMappedId("43"));
    }
}
