package com.atlassian.jira.project.type;

import com.atlassian.fugue.Option;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.security.PermissionManager;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.atlassian.jira.permission.ProjectPermissions.BROWSE_PROJECTS;
import static com.atlassian.jira.project.type.ProjectTypeManagerImpl.INACCESSIBLE_WEIGHT;
import static org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder;
import static org.hamcrest.collection.IsIterableContainingInOrder.contains;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestBrowseProjectTypeManagerImpl {
    private static final String BUSINESS = "business";
    private static final String SOFTWARE = "software";
    private static final String SERVICE_DESK = "service_desk";
    private static final ProjectType BUSINESS_TYPE = new ProjectType(new ProjectTypeKey(BUSINESS), null, null, null, 10);
    private static final ProjectType SOFTWARE_TYPE = new ProjectType(new ProjectTypeKey(SOFTWARE), null, null, null, 20);
    private static final ProjectType SOFTWARE_TYPE_INACCESSIBLE = new ProjectType(new ProjectTypeKey(SOFTWARE), null, null, null, INACCESSIBLE_WEIGHT);
    private static final ProjectType SERVICE_DESK_TYPE = new ProjectType(new ProjectTypeKey(SERVICE_DESK), null, null, null, 30);
    private static final Map<String, ProjectType> projectTypeMap = new HashMap<>();
    private static final ProjectType INACCESSIBLE_PROJECT_TYPE = new ProjectType(new ProjectTypeKey("inaccessible"), null, null, null, INACCESSIBLE_WEIGHT);

    {
        projectTypeMap.put(BUSINESS, BUSINESS_TYPE);
        projectTypeMap.put(SOFTWARE, SOFTWARE_TYPE);
        projectTypeMap.put(SERVICE_DESK, SERVICE_DESK_TYPE);
    }

    private BrowseProjectTypeManager browseProjectType;
    @Mock
    private ProjectTypeManager projectTypeManager;
    @Mock
    private PermissionManager permissionManager;
    @Mock
    private com.atlassian.jira.user.ApplicationUser user;

    @Before
    public void setUp() throws Exception {
        setUpAccessibleProjectTypes(BUSINESS, SOFTWARE, SERVICE_DESK);
        when(projectTypeManager.getInaccessibleProjectType()).thenReturn(INACCESSIBLE_PROJECT_TYPE);
        browseProjectType = new BrowseProjectTypeManagerImpl(projectTypeManager, permissionManager);

    }

    @Test
    public void shouldReturnAllInstalledAndLicensedProjectTypesWhenNoExistingProjects() throws Exception {
        setUpInStalledAndLicensedProjectTypes(BUSINESS_TYPE, SOFTWARE_TYPE);
        final List<ProjectType> projectTypes = browseProjectType.getAllProjectTypes(user);
        assertThat(projectTypes, containsInAnyOrder(BUSINESS_TYPE, SOFTWARE_TYPE));
    }

    @Test
    public void shouldReturnAllInstalledAndLicensedProjectTypesPlusTypesOfExistingProjects() throws Exception {
        setUpInStalledAndLicensedProjectTypes(BUSINESS_TYPE);
        setUpExistingProjects(project(SOFTWARE), project(SERVICE_DESK));
        final List<ProjectType> projectTypes = browseProjectType.getAllProjectTypes(user);
        assertThat(projectTypes, containsInAnyOrder(BUSINESS_TYPE, SOFTWARE_TYPE, SERVICE_DESK_TYPE));
    }

    @Test
    public void shouldAlwaysShowBusinessProjectType() throws Exception {
        setUpInStalledAndLicensedProjectTypes(SOFTWARE_TYPE);
        final List<ProjectType> projectTypes = browseProjectType.getAllProjectTypes(user);
        assertThat(projectTypes, containsInAnyOrder(BUSINESS_TYPE, SOFTWARE_TYPE));
    }

    @Test
    public void shouldRemoveDuplicateProjectTypeIfExists() throws Exception {
        setUpInStalledAndLicensedProjectTypes(BUSINESS_TYPE, SOFTWARE_TYPE);
        setUpExistingProjects(project(SOFTWARE), project(SERVICE_DESK));
        final List<ProjectType> projectTypes = browseProjectType.getAllProjectTypes(user);
        assertThat(projectTypes, containsInAnyOrder(BUSINESS_TYPE, SOFTWARE_TYPE, SERVICE_DESK_TYPE));
    }

    @Test
    public void shouldSortTheListByProjectTypeWeight() throws Exception {
        setUpInStalledAndLicensedProjectTypes(SERVICE_DESK_TYPE, BUSINESS_TYPE);
        setUpExistingProjects(project(SOFTWARE));
        final List<ProjectType> list = browseProjectType.getAllProjectTypes(user);
        assertThat(list, contains(BUSINESS_TYPE, SOFTWARE_TYPE, SERVICE_DESK_TYPE));
    }

    @Test
    public void shouldPutInAccessibleProjectTypeForAllInstalledProjectTypesToTheLast() throws Exception {
        setUpInStalledAndLicensedProjectTypes(SERVICE_DESK_TYPE, BUSINESS_TYPE, SOFTWARE_TYPE);
        setUpExistingProjects(project(BUSINESS));
        setUpInAccessibleProjectTypes(SOFTWARE);

        final List<ProjectType> list = browseProjectType.getAllProjectTypes(user);

        assertThat(list, contains(BUSINESS_TYPE, SERVICE_DESK_TYPE, SOFTWARE_TYPE_INACCESSIBLE));
    }

    @Test
    public void shouldPutInAccessibleProjectTypeForExistingProjectsToTheLast() throws Exception {
        setUpInStalledAndLicensedProjectTypes(SERVICE_DESK_TYPE, BUSINESS_TYPE);
        setUpExistingProjects(project(BUSINESS), project(SOFTWARE));
        setUpInAccessibleProjectTypes(SOFTWARE);

        final List<ProjectType> list = browseProjectType.getAllProjectTypes(user);

        assertThat(list, contains(BUSINESS_TYPE, SERVICE_DESK_TYPE, SOFTWARE_TYPE_INACCESSIBLE));
    }

    @Test
    public void shouldReturnTrueIfThereIsMoreThanOneAccessibleProjectType() {
        setUpInStalledAndLicensedProjectTypes(SERVICE_DESK_TYPE, BUSINESS_TYPE);
        Project project = project(BUSINESS);
        setUpExistingProjects(project, project(SOFTWARE));

        final boolean projectTypeChangeAllowed = browseProjectType.isProjectTypeChangeAllowed(project);

        assertThat(projectTypeChangeAllowed, is(true));
    }

    @Test
    public void shouldReturnTrueIfCurrentProjectTypeIsNotBusinessProjectType() {
        Project project = project(SOFTWARE);
        setUpExistingProjects(project);

        final boolean projectTypeChangeAllowed = browseProjectType.isProjectTypeChangeAllowed(project);

        assertThat(projectTypeChangeAllowed, is(true));
    }

    @Test
    public void shouldReturnFalseIfThereAreNoOtherAccessibleProjectTypes() {
        Project project = project(BUSINESS);
        setUpExistingProjects(project);

        final boolean projectTypeChangeAllowed = browseProjectType.isProjectTypeChangeAllowed(project);

        assertThat(projectTypeChangeAllowed, is(false));
    }

    private void setUpAccessibleProjectTypes(String... projectTypes) {
        for (String projectType : projectTypes) {
            when(projectTypeManager.getByKey(new ProjectTypeKey(projectType))).thenReturn(Option.some(projectTypeMap.get(projectType)));
        }
    }

    private void setUpInAccessibleProjectTypes(String... projectTypes) {
        for (String projectType : projectTypes) {
            when(projectTypeManager.getByKey(new ProjectTypeKey(projectType))).thenReturn(Option.none());
        }
    }

    private Project project(String projectType) {
        return new MockProject(1l, "key", new ProjectTypeKey(projectType));
    }

    private void setUpExistingProjects(Project... projects) {
        when(permissionManager.getProjects(BROWSE_PROJECTS, user)).thenReturn(Arrays.asList(projects));
    }

    private void setUpInStalledAndLicensedProjectTypes(ProjectType... projectTypes) {
        when(projectTypeManager.getAllAccessibleProjectTypes()).thenReturn(Arrays.asList(projectTypes));
    }
}