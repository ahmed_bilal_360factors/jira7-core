package com.atlassian.jira.upgrade.tasks.role;

import com.atlassian.jira.mock.ofbiz.MockGenericValue;
import com.atlassian.jira.mock.ofbiz.MockOfBizDelegator;
import com.google.common.collect.ImmutableMap;
import org.junit.Before;
import org.junit.Test;
import org.ofbiz.core.entity.GenericValue;

public class ServiceDeskPropertySetDaoTest {
    private MockOfBizDelegator ofBizDelegator;

    private ServiceDeskPropertySetDao propertySetDao;

    @Before
    public void setup() {
        ofBizDelegator = new MockOfBizDelegator();
        propertySetDao = new ServiceDeskPropertySetDaoImpl(ofBizDelegator);
    }

    @Test
    public void createsNewStringPropertyIfOldDoesNotExist() {
        propertySetDao.writeStringProperty("someProperty", "textValue");

        final GenericValue osPropertyEntry = new MockGenericValue("OSPropertyEntry",
                ImmutableMap.<String, Object>builder()
                        .put("entityName", "vp.properties")
                        .put("entityId", 1)
                        .put("propertyKey", "someProperty")
                        .put("type", 5)
                        .put("id", (long) MockOfBizDelegator.STARTING_ID)
                        .build());


        ofBizDelegator.verify(osPropertyEntry,
                createOsPropertyString((long) MockOfBizDelegator.STARTING_ID, "textValue"));
    }

    @Test
    public void updatesStringPropertyIfThereIsOldValue() {
        ofBizDelegator.createValue(createOsPropertyEntry(5L, "vp.properties", 1, "someProperty", 5));
        ofBizDelegator.createValue(createOsPropertyString(5L, "toBeReplaced"));

        propertySetDao.writeStringProperty("someProperty", "newValue");

        ofBizDelegator.verify(createOsPropertyEntry(5L, "vp.properties", 1, "someProperty", 5),
                createOsPropertyString(5L, "newValue"));

    }

    @Test
    public void updatesLastStringPropertyIfThereAreMultipleSameKeys() {
        ofBizDelegator.createValue(createOsPropertyEntry(5L, "vp.properties", 1, "someProperty", 5));
        ofBizDelegator.createValue(createOsPropertyEntry(6L, "vp.properties", 1, "someProperty", 5));
        ofBizDelegator.createValue(createOsPropertyString(5L, "toBeReplaced1"));
        ofBizDelegator.createValue(createOsPropertyString(6L, "toBeReplaced2"));


        propertySetDao.writeStringProperty("someProperty", "newValue");


        ofBizDelegator.verify(createOsPropertyEntry(5L, "vp.properties", 1, "someProperty", 5),
                createOsPropertyString(5L, "toBeReplaced1"),
                createOsPropertyEntry(6L, "vp.properties", 1, "someProperty", 5),
                createOsPropertyString(6L, "newValue")
        );
    }

    private GenericValue createOsPropertyString(long id, String value) {
        return new MockGenericValue("OSPropertyString",
                ImmutableMap.<String, Object>builder()
                        .put("value", value)
                        .put("id", id)
                        .build());
    }

    private GenericValue createOsPropertyEntry(long id, String entityName, int entityId, String propertyKey,
                                               int type) {
        return new MockGenericValue("OSPropertyEntry",
                ImmutableMap.<String, Object>builder()
                        .put("entityName", entityName)
                        .put("entityId", entityId)
                        .put("propertyKey", propertyKey)
                        .put("type", type)
                        .put("id", id)
                        .build());
    }

}