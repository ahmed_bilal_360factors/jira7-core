package com.atlassian.jira.issue.customfields.searchers.transformer;

import com.atlassian.jira.action.issue.customfields.option.MockOption;
import com.atlassian.jira.bc.issue.search.QueryContextConverter;
import com.atlassian.jira.issue.customfields.converters.SelectConverter;
import com.atlassian.jira.issue.customfields.option.Option;
import com.atlassian.jira.issue.customfields.view.CustomFieldParams;
import com.atlassian.jira.issue.customfields.view.CustomFieldParamsImpl;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.search.ClauseNames;
import com.atlassian.jira.issue.search.SearchContext;
import com.atlassian.jira.jql.context.QueryContext;
import com.atlassian.jira.jql.operand.JqlOperandResolver;
import com.atlassian.jira.jql.operand.QueryLiteral;
import com.atlassian.jira.jql.util.JqlSelectOptionsUtil;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.query.Query;
import com.atlassian.query.QueryImpl;
import com.atlassian.query.clause.Clause;
import com.atlassian.query.clause.TerminalClause;
import com.atlassian.query.clause.TerminalClauseImpl;
import com.atlassian.query.operand.SingleValueOperand;
import com.atlassian.query.operator.Operator;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Collections;

import static com.atlassian.jira.jql.operand.SimpleLiteralFactory.createLiteral;
import static com.google.common.collect.ImmutableList.of;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestSelectCustomFieldSearchInputTransformer {
    @Rule
    public MockitoRule mockito = MockitoJUnit.rule();

    @Mock
    private CustomField customField;
    @Mock
    private JqlSelectOptionsUtil jqlSelectOptionsUtil;
    @Mock
    private SearchContext searchContext;
    @Mock
    private QueryContextConverter queryContextConverter;
    @Mock
    private CustomFieldInputHelper customFieldInputHelper;
    @Mock
    private JqlOperandResolver jqlOperandResolver;

    private ClauseNames clauseNames = new ClauseNames("cf[100]");
    private final ApplicationUser searcher = null;

    @Test
    public void testCreateClauseIsAll() throws Exception {
        final SelectCustomFieldSearchInputTransformer transformer = new SelectCustomFieldSearchInputTransformer(customField, clauseNames, "blah", jqlSelectOptionsUtil, queryContextConverter, jqlOperandResolver, customFieldInputHelper);
        assertNull(transformer.createSearchClause(searcher, SelectConverter.ALL_STRING));
    }

    @Test
    public void testCreateClause() throws Exception {
        final Option option1 = new MockOption(null, null, null, "option", null, 12L);

        clauseNames = new ClauseNames("blah");
        when(customField.getUntranslatedName()).thenReturn("ABC");
        when(jqlSelectOptionsUtil.getOptionById(12L)).thenReturn(option1);
        when(customFieldInputHelper.getUniqueClauseName(searcher, clauseNames.getPrimaryName(), "ABC")).thenReturn("ABC");

        final SelectCustomFieldSearchInputTransformer transformer = new SelectCustomFieldSearchInputTransformer(customField, clauseNames, "blah", jqlSelectOptionsUtil, queryContextConverter, jqlOperandResolver, customFieldInputHelper);
        final Clause result = transformer.createSearchClause(searcher, "12");
        TerminalClause expectedResult = new TerminalClauseImpl("ABC", Operator.EQUALS, "option");
        assertEquals(expectedResult, result);
    }

    @Test
    public void testDoRelevantClauseFitNavigatorMoreThanOneOption() throws Exception {
        Query query = new QueryImpl();
        final Option option1 = new MockOption(null, null, null, null, null, 10L);
        final Option option2 = new MockOption(null, null, null, null, null, 20L);

        final QueryContext context = mock(QueryContext.class);


        when(jqlSelectOptionsUtil.getOptions(customField, context, createLiteral("value"), true)).thenReturn(of(option1, option2));
        when(queryContextConverter.getQueryContext(searchContext)).thenReturn(context);

        final SelectCustomFieldSearchInputTransformer transformer = new SelectCustomFieldSearchInputTransformer(customField, clauseNames, "blah", jqlSelectOptionsUtil, queryContextConverter, jqlOperandResolver, customFieldInputHelper) {
            @Override
            NavigatorConversionResult convertForNavigator(final Query query) {
                return new NavigatorConversionResult(true, new SingleValueOperand("value"));
            }

        };

        assertFalse(transformer.doRelevantClausesFitFilterForm(null, query, searchContext));
    }

    @Test
    public void testDoRelevantClauseFitNavigatorHappyPath() throws Exception {
        Query query = new QueryImpl();
        final Option option1 = new MockOption(null, null, null, null, null, 10L);

        final QueryContext context = mock(QueryContext.class);


        when(queryContextConverter.getQueryContext(searchContext)).thenReturn(context);
        when(jqlSelectOptionsUtil.getOptions(customField, context, createLiteral("value"), true)).thenReturn(of(option1));

        final SelectCustomFieldSearchInputTransformer transformer = new SelectCustomFieldSearchInputTransformer(customField, clauseNames, "blah", jqlSelectOptionsUtil, queryContextConverter, jqlOperandResolver, customFieldInputHelper) {
            @Override
            NavigatorConversionResult convertForNavigator(final Query query) {
                return new NavigatorConversionResult(true, new SingleValueOperand("value"));
            }

        };

        assertTrue(transformer.doRelevantClausesFitFilterForm(null, query, searchContext));
    }

    @Test
    public void testDoRelevantClauseFitNavigatorDoesntFit() throws Exception {
        final SelectCustomFieldSearchInputTransformer transformer = new SelectCustomFieldSearchInputTransformer(customField, clauseNames, "blah", jqlSelectOptionsUtil, queryContextConverter, jqlOperandResolver, customFieldInputHelper) {
            @Override
            NavigatorConversionResult convertForNavigator(final Query query) {
                return new NavigatorConversionResult(false, new SingleValueOperand("value"));
            }

        };

        assertFalse(transformer.doRelevantClausesFitFilterForm(null, new QueryImpl(), searchContext));
    }

    @Test
    public void testDoRelevantClauseFitNavigatorDoesntFitNoValue() throws Exception {
        final SelectCustomFieldSearchInputTransformer transformer = new SelectCustomFieldSearchInputTransformer(customField, clauseNames, "blah", jqlSelectOptionsUtil, queryContextConverter, jqlOperandResolver, customFieldInputHelper) {
            @Override
            NavigatorConversionResult convertForNavigator(final Query query) {
                return new NavigatorConversionResult(false, null);
            }

        };

        assertFalse(transformer.doRelevantClausesFitFilterForm(null, new QueryImpl(), searchContext));
    }

    @Test
    public void testGetParamsHappyPath() throws Exception {
        TerminalClauseImpl whereClause = new TerminalClauseImpl("cf[100]", Operator.EQUALS, "12");
        Query query = new QueryImpl(whereClause);

        when(jqlOperandResolver.getValues(searcher, whereClause.getOperand(), whereClause)).thenReturn(of(new QueryLiteral(new SingleValueOperand("value"), 12L)));


        final QueryContext context = mock(QueryContext.class);
        when(queryContextConverter.getQueryContext(searchContext)).thenReturn(context);

        final Option option1 = new MockOption(null, null, null, "option1", null, 12L);
        when(jqlSelectOptionsUtil.getOptions(customField, context, createLiteral(12L), true)).thenReturn(of(option1));

        final SelectCustomFieldSearchInputTransformer transformer = new SelectCustomFieldSearchInputTransformer(customField, clauseNames, "blah", jqlSelectOptionsUtil, queryContextConverter, jqlOperandResolver, customFieldInputHelper);

        final CustomFieldParams result = transformer.getParamsFromSearchRequest(null, query, searchContext);
        final CustomFieldParamsImpl expectedResult = new CustomFieldParamsImpl(customField, Collections.singleton("12"));

        assertEquals(expectedResult, result);
    }

    @Test
    public void testGetParamsDoesntFit() throws Exception {
        final SelectCustomFieldSearchInputTransformer transformer = new SelectCustomFieldSearchInputTransformer(customField, clauseNames, "blah", jqlSelectOptionsUtil, queryContextConverter, jqlOperandResolver, customFieldInputHelper) {
            @Override
            NavigatorConversionResult convertForNavigator(final Query query) {
                return new NavigatorConversionResult(false, new SingleValueOperand("value"));
            }

        };

        assertNull(transformer.getParamsFromSearchRequest(null, new QueryImpl(), searchContext));
    }

    @Test
    public void testGetParamsDoesntFitNoValue() throws Exception {
        final SelectCustomFieldSearchInputTransformer transformer = new SelectCustomFieldSearchInputTransformer(customField, clauseNames, "blah", jqlSelectOptionsUtil, queryContextConverter, jqlOperandResolver, customFieldInputHelper) {
            @Override
            NavigatorConversionResult convertForNavigator(final Query query) {
                return new NavigatorConversionResult(false, null);
            }

        };

        assertNull(transformer.getParamsFromSearchRequest(null, new QueryImpl(), searchContext));
    }
}
