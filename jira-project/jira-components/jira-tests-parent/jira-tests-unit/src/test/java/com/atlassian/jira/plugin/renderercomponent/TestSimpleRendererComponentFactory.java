package com.atlassian.jira.plugin.renderercomponent;

import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.mock.plugin.MockPluginAccessor;
import com.atlassian.jira.plugin.renderer.RendererComponentDecoratorFactory;
import com.atlassian.jira.plugin.renderer.RendererComponentDecoratorFactoryDescriptor;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.renderer.v2.components.RendererComponent;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableList;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.RuleChain;
import org.mockito.Mock;

import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @since v3.12
 */
public class TestSimpleRendererComponentFactory {
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Rule
    public RuleChain ruleChain = MockitoMocksInContainer.forTest(this);

    @AvailableInContainer
    @Mock
    private PluginAccessor pluginAccessor;

    @Test
    public void testInitMissingParam() {
        expectedException.expect(PluginParseException.class);
        expectedException.expectMessage(containsString("is missing the required"));
        expectedException.expectMessage(containsString("pluginkey"));

        final RendererComponentFactoryDescriptor mockRendererComponentFactoryDescriptor = mock(RendererComponentFactoryDescriptor.class);
        when(mockRendererComponentFactoryDescriptor.getCompleteKey()).thenReturn("pluginkey");

        final SimpleRendererComponentFactory simpleRendererComponentFactory = new SimpleRendererComponentFactory();
        simpleRendererComponentFactory.init(mockRendererComponentFactoryDescriptor);
    }

    @Test
    public void testInitInvalidClass() throws ClassNotFoundException {
        expectedException.expect(PluginParseException.class);
        expectedException.expectMessage(containsString("Could not load renderer"));
        expectedException.expectMessage(containsString("pluginkey"));
        expectedException.expectMessage(containsString("com.test.invalid.class"));

        final RendererComponentFactoryDescriptor mockRendererComponentFactoryDescriptor = mock(RendererComponentFactoryDescriptor.class);
        final Plugin plugin = mock(Plugin.class);

        when(mockRendererComponentFactoryDescriptor.getParams()).thenReturn(ImmutableMap.of("rendererComponentClass", "com.test.invalid.class"));
        when(mockRendererComponentFactoryDescriptor.getCompleteKey()).thenReturn("pluginkey");
        when(mockRendererComponentFactoryDescriptor.getPlugin()).thenReturn(plugin);
        when(plugin.loadClass(eq("com.test.invalid.class"), any())).thenThrow(new ClassNotFoundException());

        final SimpleRendererComponentFactory simpleRendererComponentFactory = new SimpleRendererComponentFactory() {
            @Override
            RendererComponent loadRendererComponent(final Class rendererComponentClass) throws ClassNotFoundException {
                throw new ClassNotFoundException();
            }
        };

        simpleRendererComponentFactory.init(mockRendererComponentFactoryDescriptor);
    }

    @Test
    public void testInitSuccessful() throws PluginParseException {
        final RendererComponentFactoryDescriptor mockRendererComponentFactoryDescriptor = mock(RendererComponentFactoryDescriptor.class);
        final Plugin mockPlugin = mock(Plugin.class);
        final RendererComponent mockRendererComponent = mock(RendererComponent.class);
        when(mockRendererComponentFactoryDescriptor.getParams()).thenReturn(ImmutableMap.of("rendererComponentClass", "java.lang.String"));
        when(mockRendererComponentFactoryDescriptor.getCompleteKey()).thenReturn("pluginkey");
        when(mockRendererComponentFactoryDescriptor.getPlugin()).thenReturn(mockPlugin);

        final SimpleRendererComponentFactory simpleRendererComponentFactory = new SimpleRendererComponentFactory() {
            RendererComponent loadRendererComponent(final Class rendererComponentClass) throws ClassNotFoundException {
                return mockRendererComponent;
            }
        };

        simpleRendererComponentFactory.init(mockRendererComponentFactoryDescriptor);
        final RendererComponent rendererComponent = simpleRendererComponentFactory.getRendererComponent();
        assertNotNull(rendererComponent);
        assertEquals(mockRendererComponent, rendererComponent);
    }

    @Test
    public void shouldReturnDecoratedComponent() throws PluginParseException {
        final RendererComponentFactoryDescriptor mockRendererComponentFactoryDescriptor = mock(RendererComponentFactoryDescriptor.class);
        final Plugin mockPlugin = mock(Plugin.class);
        final RendererComponent mockRendererComponent = mock(RendererComponent.class);
        when(mockRendererComponentFactoryDescriptor.getParams()).thenReturn(ImmutableMap.of("rendererComponentClass", "java.lang.String"));
        when(mockRendererComponentFactoryDescriptor.getCompleteKey()).thenReturn("pluginkey");
        when(mockRendererComponentFactoryDescriptor.getPlugin()).thenReturn(mockPlugin);

        final RendererComponent decoratedComponent = mock(RendererComponent.class);
        RendererComponentDecoratorFactoryDescriptor mockFactory = mock(RendererComponentDecoratorFactoryDescriptor.class);
        RendererComponentDecoratorFactory factory = new RendererComponentDecoratorFactory() {
            @Override
            public RendererComponent decorate(final Class<RendererComponent> type, final RendererComponent rendererComponent)
            {
                return decoratedComponent;
            }
        };
        when(mockFactory.getModule()).thenReturn(factory);
        when(pluginAccessor.getEnabledModuleDescriptorsByClass(RendererComponentDecoratorFactoryDescriptor.class)).thenReturn(ImmutableList.of(mockFactory));

        final SimpleRendererComponentFactory simpleRendererComponentFactory = new SimpleRendererComponentFactory() {
            RendererComponent loadRendererComponent(final Class rendererComponentClass) throws ClassNotFoundException {
                return mockRendererComponent;
            }
        };

        simpleRendererComponentFactory.init(mockRendererComponentFactoryDescriptor);
        final RendererComponent rendererComponent = simpleRendererComponentFactory.getRendererComponent();
        assertNotNull(rendererComponent);
        assertEquals(decoratedComponent, rendererComponent);
    }
}
