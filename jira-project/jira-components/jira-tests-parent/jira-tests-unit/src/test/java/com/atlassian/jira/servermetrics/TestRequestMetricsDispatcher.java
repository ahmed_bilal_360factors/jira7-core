package com.atlassian.jira.servermetrics;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.instrumentation.operations.OpSnapshot;
import com.atlassian.instrumentation.operations.ThreadOpTimerFactory;
import com.atlassian.instrumentation.operations.registry.OpRegistry;
import com.atlassian.jira.instrumentation.InstrumentationName;
import com.google.common.collect.ImmutableList;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import javax.servlet.http.HttpServletRequest;
import java.time.Duration;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static java.time.Duration.ofMillis;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.same;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestRequestMetricsDispatcher {
    private static final String REQ_KEY = "req-key";
    @Rule
    public MockitoRule mocks = MockitoJUnit.rule();

    @Mock
    MultiThreadedRequestMetricsCollector collector;
    @Mock
    EventPublisher eventPublisher;
    @Mock
    MultiThreadedRequestKeyResolver requestKeyResolver;
    @Mock
    HttpServletRequest request;
    @Mock
    TimingInformationToEvent timingInformationToEvent;
    @Mock
    ThreadOpTimerFactory instrumentRegistry;
    @Mock
    OpRegistry registry;


    RequestMetricsDispatcher testObj;


    @Before
    public void setUp() throws Exception {
        when(instrumentRegistry.getOpRegistry()).thenReturn(registry);
        when(registry.snapshot()).thenReturn(Collections.emptyList());

        testObj = new RequestMetricsDispatcher(collector, eventPublisher, timingInformationToEvent, requestKeyResolver, instrumentRegistry);
    }

    @Test
    public void shouldStartAndFinishCollectionInNotBlacklistedRequest() {
        when(request.getServletPath()).thenReturn("/servlet");
        when(request.getPathInfo()).thenReturn("/pathInfo");
        when(collector.finishCollectionInCurrentThread()).thenReturn(Optional.empty());

        final RequestMetricsDispatcher.CollectorHandle collectorHandle = testObj.startCollection(request);
        collectorHandle.close();

        verify(collector, times(1)).startCollectionInCurrentThread();
        verify(collector, times(1)).finishCollectionInCurrentThread();
    }

    @Test
    public void shouldNotStartCollectionOnBlacklistedRequest() {
        final List<String> blacklistedServlets = ImmutableList.of("/s", "/rest", "/images", "/download");
        when(request.getPathInfo()).thenReturn("pathInfo");
        when(collector.finishCollectionInCurrentThread()).thenReturn(Optional.empty());

        blacklistedServlets.forEach((blacklistedServlet) -> {
            when(request.getServletPath()).thenReturn(blacklistedServlet);

            final RequestMetricsDispatcher.CollectorHandle collectorHandle = testObj.startCollection(request);
            collectorHandle.close();
        });

        verify(collector, never()).startCollectionInCurrentThread();
        verify(collector, never()).finishCollectionInCurrentThread();
    }

    @Test
    public void whenCollectorReturnEmptyTimingNoEventShouldBeRaised() {
        final RequestMetricsDispatcher.CollectorHandle collectorHandle = testObj.startCollection(request);
        when(request.getServletPath()).thenReturn("/servlet");
        when(request.getPathInfo()).thenReturn("/pathInfo");
        when(collector.finishCollectionInCurrentThread()).thenReturn(Optional.empty());

        collectorHandle.close();

        verify(timingInformationToEvent, never()).createStatEvent(any(), any(), any());
        verify(eventPublisher, never()).publish(any());
    }

    @Test
    public void shouldTransformTimingToEvent() {
        when(request.getServletPath()).thenReturn("/servlet");
        when(request.getPathInfo()).thenReturn("/pathInfo");
        when(requestKeyResolver.requestFinished(any())).thenReturn(Optional.of(REQ_KEY));
        final TimingInformation sampleTimingInformation = mock(TimingInformation.class);
        when(collector.finishCollectionInCurrentThread()).thenReturn(Optional.of(sampleTimingInformation));
        RequestMetricsEvent mockEvent = mock(RequestMetricsEvent.class);
        when(timingInformationToEvent.createStatEvent(any(), any(), any())).thenReturn(mockEvent);

        final RequestMetricsDispatcher.CollectorHandle collectorHandle = testObj.startCollection(request);
        collectorHandle.close();

        verify(timingInformationToEvent)
                .createStatEvent(same(sampleTimingInformation), same(request), eq(Optional.of(REQ_KEY)));
        verify(eventPublisher, times(1)).publish(mockEvent);
    }

    @Test
    public void shouldRecordDbMetrics() {
        when(request.getServletPath()).thenReturn("/servlet");
        when(request.getPathInfo()).thenReturn("/pathInfo");
        when(requestKeyResolver.requestFinished(any())).thenReturn(Optional.of(REQ_KEY));
        final TimingInformation sampleTimingInformation = mock(TimingInformation.class);
        when(collector.finishCollectionInCurrentThread()).thenReturn(Optional.of(sampleTimingInformation));
        RequestMetricsEvent mockEvent = mock(RequestMetricsEvent.class);
        when(timingInformationToEvent.createStatEvent(any(), any(), any())).thenReturn(mockEvent);
        Mockito.reset(registry);
        final Duration dbReadDuration = ofMillis(2);
        OpSnapshot ops = new OpSnapshot(InstrumentationName.DB_READS.getInstrumentName(), 1, dbReadDuration.getNano(), 1, 1, 1, 1, 1, 1);
        when(registry.snapshot()).thenReturn(ImmutableList.of(ops));


        final RequestMetricsDispatcher.CollectorHandle collectorHandle = testObj.startCollection(request);
        collectorHandle.close();

        verify(collector)
                .setTimeSpentInActivity("dbRead", dbReadDuration);
    }
}