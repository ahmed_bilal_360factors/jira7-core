package com.atlassian.jira.dashboard.permission;

import com.atlassian.gadgets.dashboard.DashboardId;
import com.atlassian.jira.bc.JiraServiceContextImpl;
import com.atlassian.jira.bc.portal.PortalPageService;
import com.atlassian.jira.portal.PortalPage;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.util.UserUtil;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestJiraPermissionService {
    @Rule
    public final MethodRule initMockito = MockitoJUnit.rule();

    @Mock
    private UserUtil mockUserUtil;
    @Mock
    private PortalPageService mockPortalPageService;
    @Mock
    private PermissionManager permissionManager;

    @InjectMocks
    private JiraPermissionService jiraPermissionService;

    private ApplicationUser fred = new MockApplicationUser("fred");

    @Before
    public void setUp() throws Exception {
        when(mockUserUtil.getUserByName("fred")).thenReturn(fred);
        JiraPermissionService.setAllowEditingOfDefaultDashboard(JiraPermissionService.INITIAL_ALLOW_EDITING_OF_DEFAULT_DASHBOARD);
    }

    @After
    public void tearDown() throws Exception {
        JiraPermissionService.setAllowEditingOfDefaultDashboard(JiraPermissionService.INITIAL_ALLOW_EDITING_OF_DEFAULT_DASHBOARD);
    }

    @Test
    public void testIsReadableBy() {
        when(mockPortalPageService.validateForGetPortalPage(new JiraServiceContextImpl(fred), 10011L)).thenReturn(true);

        final boolean readableBy = jiraPermissionService.isReadableBy(DashboardId.valueOf(Long.toString(10011L)), "fred");
        assertTrue(readableBy);
    }

    @Test
    public void testIsNotReadableBy() {
        final boolean readableBy = jiraPermissionService.isReadableBy(DashboardId.valueOf(Long.toString(10011)), "fred");

        assertFalse(readableBy);
        verify(mockPortalPageService).validateForGetPortalPage(new JiraServiceContextImpl(fred), 10011L);
    }

    @Test
    public void testIsWritableBy() {
        final PortalPage portalPage = PortalPage.name("Non system page").build();
        final JiraServiceContextImpl serviceContext = new JiraServiceContextImpl(fred);

        when(mockPortalPageService.getPortalPage(serviceContext, 10011L)).thenReturn(portalPage);
        when(mockPortalPageService.validateForUpdate(serviceContext, portalPage)).thenReturn(true);

        final boolean writableBy = jiraPermissionService.isWritableBy(DashboardId.valueOf(Long.toString(10011)), "fred");
        assertTrue(writableBy);
    }

    @Test
    public void testSystemDefaultDashboardNotWritable() {
        final PortalPage portalPage = PortalPage.name("System Default").systemDashboard().build();
        final JiraServiceContextImpl serviceContext = new JiraServiceContextImpl(fred);

        when(mockPortalPageService.getPortalPage(serviceContext, 10011L)).thenReturn(portalPage);

        JiraPermissionService.setAllowEditingOfDefaultDashboard(false);
        final boolean writableBy = jiraPermissionService.isWritableBy(DashboardId.valueOf(Long.toString(10011)), "fred");
        assertFalse(writableBy);
    }

    @Test
    public void testSystemDefaultDashboardIsWritable() {
        final PortalPage portalPage = PortalPage.name("System Default").systemDashboard().build();
        final JiraServiceContextImpl serviceContext = new JiraServiceContextImpl(fred);

        when(mockPortalPageService.getPortalPage(serviceContext, 10011L)).thenReturn(portalPage);
        when(mockPortalPageService.validateForUpdate(serviceContext, portalPage)).thenReturn(true);

        JiraPermissionService.setAllowEditingOfDefaultDashboard(true);
        final boolean writableBy = jiraPermissionService.isWritableBy(DashboardId.valueOf(Long.toString(10011)), "fred");
        assertTrue(writableBy);
    }

    @Test
    public void testIsNotWritableBy() {
        final PortalPage portalPage = PortalPage.name("System Default").build();
        final JiraServiceContextImpl serviceContext = new JiraServiceContextImpl(fred);

        when(mockPortalPageService.getPortalPage(serviceContext, 10011L)).thenReturn(portalPage);

        final boolean writableBy = jiraPermissionService.isWritableBy(DashboardId.valueOf(Long.toString(10011)), "fred");
        assertFalse(writableBy);
        verify(mockPortalPageService).validateForUpdate(serviceContext, portalPage);
    }
}
