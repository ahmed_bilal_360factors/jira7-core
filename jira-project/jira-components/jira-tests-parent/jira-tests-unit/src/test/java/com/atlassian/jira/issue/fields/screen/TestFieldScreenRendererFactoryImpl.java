package com.atlassian.jira.issue.fields.screen;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.fields.Field;
import com.atlassian.jira.mock.issue.MockIssue;
import com.atlassian.jira.util.Predicate;
import com.google.common.collect.ImmutableList;
import com.opensymphony.workflow.loader.ActionDescriptor;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.List;

import static com.atlassian.jira.issue.fields.util.FieldPredicates.isCustomField;
import static com.atlassian.jira.issue.operation.IssueOperations.EDIT_ISSUE_OPERATION;
import static com.atlassian.jira.issue.operation.IssueOperations.VIEW_ISSUE_OPERATION;
import static com.atlassian.jira.util.Predicates.truePredicate;
import static java.util.Arrays.asList;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Test for {@link com.atlassian.jira.issue.fields.screen.FieldScreenRendererFactoryImpl}.
 *
 * @since v4.1
 */
public class TestFieldScreenRendererFactoryImpl {
    @Rule
    public final MockitoRule rule = MockitoJUnit.rule();

    @Mock
    private Issue mockIssue;

    @Mock
    private FieldScreenRenderer screenRenderer;

    @Mock
    private StandardFieldScreenRendererFactory stdFactory;

    private FieldScreenRendererFactoryImpl factory;

    @Before
    public void setUp() {
        factory = new FieldScreenRendererFactoryImpl(null, stdFactory);
    }

    @Test
    public void testGetFieldScreenRendererWithAll() {
        when(stdFactory.createFieldScreenRenderer(mockIssue, VIEW_ISSUE_OPERATION, truePredicate())).thenReturn(screenRenderer);

        final FieldScreenRenderer actualRenderer = factory.getFieldScreenRenderer(mockIssue, VIEW_ISSUE_OPERATION);

        assertThat(actualRenderer, equalTo(screenRenderer));
    }

    @Test
    public void testGetFieldScreenRendererWithAllLegacy() {
        when(stdFactory.createFieldScreenRenderer(mockIssue, VIEW_ISSUE_OPERATION, truePredicate())).thenReturn(screenRenderer);

        final FieldScreenRenderer actualRenderer = factory.getFieldScreenRenderer(null, mockIssue, VIEW_ISSUE_OPERATION, false);

        assertThat(actualRenderer, equalTo(screenRenderer));
    }

    @Test
    public void testGetFieldScreenRendererWithCustomOnly() {
        when(stdFactory.createFieldScreenRenderer(mockIssue, VIEW_ISSUE_OPERATION, isCustomField())).thenReturn(screenRenderer);

        final FieldScreenRenderer actualRenderer = factory.getFieldScreenRenderer(mockIssue, VIEW_ISSUE_OPERATION, isCustomField());

        assertThat(actualRenderer, equalTo(screenRenderer));
    }

    @Test
    public void testGetFieldScreenRendererWithCustomOnlyLegacy() {
        when(stdFactory.createFieldScreenRenderer(mockIssue, VIEW_ISSUE_OPERATION, isCustomField())).thenReturn(screenRenderer);

        final FieldScreenRenderer actualRenderer = factory.getFieldScreenRenderer(null, mockIssue, VIEW_ISSUE_OPERATION, true);

        assertThat(actualRenderer, equalTo(screenRenderer));
    }

    @Test
    public void testGetFieldScreenRendererOperation() {
        final Predicate<Field> falsePredicate = input -> false;
        when(stdFactory.createFieldScreenRenderer(mockIssue, VIEW_ISSUE_OPERATION, falsePredicate)).thenReturn(screenRenderer);

        final FieldScreenRenderer actualRenderer = factory.getFieldScreenRenderer(mockIssue, VIEW_ISSUE_OPERATION, falsePredicate);

        assertThat(actualRenderer, equalTo(screenRenderer));
    }

    @Test
    public void testGetFieldScreenRendererActionDescriptor() {
        final ActionDescriptor desc = mock(ActionDescriptor.class);
        when(stdFactory.createFieldScreenRenderer(mockIssue, desc)).thenReturn(screenRenderer);

        final FieldScreenRenderer actualRenderer = factory.getFieldScreenRenderer(mockIssue, desc);

        assertThat(actualRenderer, equalTo(screenRenderer));
    }

    @Test
    public void testGetFieldScreenRendererForIssue() {
        when(stdFactory.createFieldScreenRenderer(mockIssue)).thenReturn(screenRenderer);

        final FieldScreenRenderer actualRenderer = factory.getFieldScreenRenderer(mockIssue);

        assertThat(actualRenderer, equalTo(screenRenderer));
    }

    @Test
    public void testGetFieldScreenRendererForFields() {
        final List<String> fieldIds = asList("one", "two", "three");
        when(stdFactory.createFieldScreenRenderer(fieldIds, mockIssue, EDIT_ISSUE_OPERATION)).thenReturn(screenRenderer);

        final FieldScreenRenderer actualRenderer = factory.getFieldScreenRenderer(fieldIds, null, mockIssue, EDIT_ISSUE_OPERATION);

        assertThat(actualRenderer, equalTo(screenRenderer));
    }

    @Test
    public void testGetFieldScreenRendererForBulk() {
        final List<Issue> issues = ImmutableList.of(new MockIssue(1), new MockIssue(2));

        final ActionDescriptor action = mock(ActionDescriptor.class);
        final BulkFieldScreenRendererImpl renderer = new BulkFieldScreenRendererImpl(ImmutableList.of());
        final BulkFieldScreenRendererFactory bulkFieldScreenRendererFactory = mock(BulkFieldScreenRendererFactory.class);
        when(bulkFieldScreenRendererFactory.createRenderer(issues, action)).thenReturn(renderer);

        final FieldScreenRendererFactoryImpl factory = new FieldScreenRendererFactoryImpl(bulkFieldScreenRendererFactory, null);
        final FieldScreenRenderer actualRenderer = factory.getFieldScreenRenderer(issues, action);

        assertThat(actualRenderer, equalTo(renderer));
    }
}
