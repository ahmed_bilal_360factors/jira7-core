package com.atlassian.jira.issue.util;

import com.google.common.collect.Lists;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeDiagnosingMatcher;

import java.util.ArrayList;

/**
 * Matcher that takes issue ids from given IssuesIterable and matches them with given matcher.
 *
 * @since v6.5
 */
public class IssueIterableIssueIdMatcher extends TypeSafeDiagnosingMatcher<IssuesIterable> {
    public static Matcher<IssuesIterable> issuesIdsThat(final Matcher<Iterable<? extends Long>> issueIdsMatcher) {
        return new IssueIterableIssueIdMatcher(issueIdsMatcher);
    }

    final private Matcher<Iterable<? extends Long>> issueIdsMatcher;

    public IssueIterableIssueIdMatcher(final Matcher<Iterable<? extends Long>> issueIdsMatcher) {
        this.issueIdsMatcher = issueIdsMatcher;
    }

    @Override
    protected boolean matchesSafely(final IssuesIterable item, final Description mismatchDescription) {
        ArrayList<Long> items = Lists.newArrayList();
        item.foreach(element -> items.add(element.getId()));

        return issueIdsMatcher.matches(items);
    }

    @Override
    public void describeTo(final Description description) {
        description.appendText("issues ids ");
        issueIdsMatcher.describeTo(description);
    }
}
