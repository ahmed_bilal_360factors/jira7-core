package com.atlassian.jira.jql.builder;

import com.atlassian.jira.jql.util.JqlDateSupport;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.plugin.jql.function.AllStandardIssueTypesFunction;
import com.atlassian.jira.plugin.jql.function.AllSubIssueTypesFunction;
import com.atlassian.jira.timezone.TimeZoneManager;
import com.atlassian.jira.util.collect.CollectionBuilder;
import com.atlassian.query.Query;
import com.atlassian.query.QueryImpl;
import com.atlassian.query.clause.AndClause;
import com.atlassian.query.clause.Clause;
import com.atlassian.query.clause.TerminalClause;
import com.atlassian.query.clause.TerminalClauseImpl;
import com.atlassian.query.operand.EmptyOperand;
import com.atlassian.query.operand.FunctionOperand;
import com.atlassian.query.operand.MultiValueOperand;
import com.atlassian.query.operand.Operand;
import com.atlassian.query.operand.Operands;
import com.atlassian.query.operand.SingleValueOperand;
import com.atlassian.query.operator.Operator;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.RuleChain;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.TimeZone;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Test for {@link DefaultJqlClauseBuilder}.
 *
 * @since v4.0
 */
public class TestDefaultJqlClauseBuilder {
    @Rule
    public final RuleChain mockitoMocks = MockitoMocksInContainer.forTest(this);
    @Rule
    public final ExpectedException exception = ExpectedException.none();


    @Mock
    @AvailableInContainer
    private TimeZoneManager timeZoneManager;
    @Mock
    private SimpleClauseBuilder clauseBuilder;
    @Mock
    private JqlDateSupport dateSupport;

    @Before
    public void setUp() throws Exception {
        when(timeZoneManager.getLoggedInUserTimeZone()).thenReturn(TimeZone.getDefault());
    }

    @Test
    public void testClear() throws Exception {
        when(clauseBuilder.clear()).thenReturn(clauseBuilder);

        final JqlClauseBuilder builder = createBuilder(clauseBuilder);
        assertSame(builder, builder.clear());
        resetInnerBuilderAndCallClearToVerifyBuilderStructure(builder);
    }

    @Test
    public void testEmptyConditionWithNullParam() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addEmptyCondition(null);
    }

    @Test
    public void testEmptyCondition() throws Exception {
        final String clauseName = "name";
        when(clauseBuilder.clause(new TerminalClauseImpl(clauseName, Operator.IS, EmptyOperand.EMPTY))).thenReturn(clauseBuilder);

        DefaultJqlClauseBuilder builder = createBuilder(clauseBuilder);
        assertSame(builder, builder.addEmptyCondition(clauseName));
    }

    @Test
    public void testEndWhere() throws Exception {
        final JqlQueryBuilder queryBuilder = JqlQueryBuilder.newBuilder();
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(null, timeZoneManager);
        assertNull(builder.endWhere());

        builder = new DefaultJqlClauseBuilder(queryBuilder, timeZoneManager);
        assertSame(queryBuilder, builder.endWhere());
    }

    @Test
    public void testBuildQuery() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(null, timeZoneManager);
        builder.unresolved();
        final Clause expectedClause = builder.buildClause();

        assertEquals(new QueryImpl(expectedClause), builder.buildQuery());

        final JqlQueryBuilder parent = JqlQueryBuilder.newBuilder();
        parent.orderBy().assignee(null).endOrderBy().where().workRatio().gt().number(5L);

        builder = new DefaultJqlClauseBuilder(parent, timeZoneManager);
        builder.unresolved();

        final Query expectedQuery = parent.buildQuery();

        //This ensures that we call build on the "parent" because we look for "workRatio > 5" rather
        //than "resolution is EMPTY".
        assertEquals(expectedQuery, builder.buildQuery());
    }

    @Test
    public void testAddDateConditionNullQuery() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addDateCondition(null, Operator.LESS_THAN, new Date());
    }

    @Test
    public void testAddDateConditionNullOperator() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addDateCondition("qwerty", null, new Date());
    }

    @Test
    public void testAddDateConditionNullDate() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addDateCondition("shasg", Operator.IN, (Date) null);
    }

    @Test
    public void testAddDateCondition() throws Exception {
        final Date date = new Date();
        final String dateString = "%&*##*$&$";
        final String clauseName = "name";

        when(dateSupport.getDateString(date)).thenReturn(dateString);
        when(clauseBuilder.clause(new TerminalClauseImpl(clauseName, Operator.LIKE, dateString))).thenReturn(clauseBuilder);
        DefaultJqlClauseBuilder builder = createBuilder(clauseBuilder, dateSupport);

        assertSame(builder, builder.addDateCondition(clauseName, Operator.LIKE, date));
        resetInnerBuilderAndCallClearToVerifyBuilderStructure(builder);
    }

    @Test
    public void testAddDateRangeConditionDateNullQuery() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addDateRangeCondition(null, new Date(), new Date());
    }

    @Test
    public void testAddDateRangeConditionDateNullRange() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addDateRangeCondition("qwerty", null, null);
    }

    @Test
    public void testAddDateRangeConditionDate() throws Exception {
        final Date dateStart = new Date(34573284534L);
        final Date dateEnd = new Date(3452785834573895748L);
        final String dateStartString = "%&*##*$&$";
        final String dateEndString = "dhakjdhakjhsdsa";

        final String clauseName = "CreateDate";

        when(dateSupport.getDateString(dateStart)).thenReturn(dateStartString);
        when(dateSupport.getDateString(dateEnd)).thenReturn(dateEndString);

        when(clauseBuilder.clause(new TerminalClauseImpl(clauseName, Operator.GREATER_THAN_EQUALS, dateStartString))).thenReturn(clauseBuilder);
        when(clauseBuilder.clause(new TerminalClauseImpl(clauseName, Operator.LESS_THAN_EQUALS, dateEndString))).thenReturn(clauseBuilder);
        when(clauseBuilder.clause(new AndClause(new TerminalClauseImpl(clauseName, Operator.GREATER_THAN_EQUALS, dateStartString),
                new TerminalClauseImpl(clauseName, Operator.LESS_THAN_EQUALS, dateEndString)))).thenReturn(clauseBuilder);

        DefaultJqlClauseBuilder builder = createBuilder(clauseBuilder, dateSupport);
        assertSame(builder, builder.addDateRangeCondition(clauseName, dateStart, null));
        assertSame(builder, builder.addDateRangeCondition(clauseName, null, dateEnd));
        assertSame(builder, builder.addDateRangeCondition(clauseName, dateStart, dateEnd));
        resetInnerBuilderAndCallClearToVerifyBuilderStructure(builder);
    }

    @Test
    public void testAddStringRangeConditionStringNullQuery() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addStringRangeCondition(null, "", "");
    }

    @Test
    public void testAddStringRangeConditionStringNullRange() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addStringRangeCondition("qwerty", null, null);
    }

    @Test
    public void testAddStringRangeConditionString() throws Exception {
        final String start = "%&*##*$&$";
        final String end = "dhakjdhakjhsdsa";
        final String clauseName = "weqekwq";

        when(clauseBuilder.clause(new TerminalClauseImpl(clauseName, Operator.GREATER_THAN_EQUALS, start))).thenReturn(clauseBuilder);
        when(clauseBuilder.clause(new TerminalClauseImpl(clauseName, Operator.LESS_THAN_EQUALS, end))).thenReturn(clauseBuilder);
        when(clauseBuilder.clause(new AndClause(new TerminalClauseImpl(clauseName, Operator.GREATER_THAN_EQUALS, start), new TerminalClauseImpl(
                clauseName, Operator.LESS_THAN_EQUALS, end)))).thenReturn(clauseBuilder);

        DefaultJqlClauseBuilder builder = createBuilder(clauseBuilder);
        assertSame(builder, builder.addStringRangeCondition(clauseName, start, null));
        assertSame(builder, builder.addStringRangeCondition(clauseName, null, end));
        assertSame(builder, builder.addStringRangeCondition(clauseName, start, end));
        resetInnerBuilderAndCallClearToVerifyBuilderStructure(builder);
    }

    @Test
    public void testAddNumberRangeConditionStringNullQuery() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addNumberRangeCondition(null, 8L, 23L);
    }

    @Test
    public void testAddNumberRangeConditionStringNullRange() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addNumberRangeCondition("qwerty", null, null);
    }

    @Test
    public void testAddNumberRangeConditionString() throws Exception {
        final Long start = 538748L;
        final Long end = 567L;
        final String clauseName = "blash";

        when(clauseBuilder.clause(new TerminalClauseImpl(clauseName, Operator.GREATER_THAN_EQUALS, start))).thenReturn(clauseBuilder);
        when(clauseBuilder.clause(new TerminalClauseImpl(clauseName, Operator.LESS_THAN_EQUALS, end))).thenReturn(clauseBuilder);
        when(clauseBuilder.clause(new AndClause(new TerminalClauseImpl(clauseName, Operator.GREATER_THAN_EQUALS, start), new TerminalClauseImpl(
                clauseName, Operator.LESS_THAN_EQUALS, end)))).thenReturn(clauseBuilder);

        DefaultJqlClauseBuilder builder = createBuilder(clauseBuilder);
        assertSame(builder, builder.addNumberRangeCondition(clauseName, start, null));
        assertSame(builder, builder.addNumberRangeCondition(clauseName, null, end));
        assertSame(builder, builder.addNumberRangeCondition(clauseName, start, end));
        resetInnerBuilderAndCallClearToVerifyBuilderStructure(builder);
    }

    @Test
    public void testAddRangeConditionStringNullQuery() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addRangeCondition(null, Operands.valueOf(10L), Operands.valueOf(57584L));
    }

    @Test
    public void testAddRangeConditionStringNullRange() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addRangeCondition("qwerty", null, null);
    }

    @Test
    public void testAddRangeConditionString() throws Exception {
        final Operand start = Operands.valueOf("%&*##*$&$");
        final Operand end = Operands.valueOf("dhakjdhakjhsdsa");
        final String clauseName = "qwerty";

        when(clauseBuilder.clause(new TerminalClauseImpl(clauseName, Operator.GREATER_THAN_EQUALS, start))).thenReturn(clauseBuilder);
        when(clauseBuilder.clause(new TerminalClauseImpl(clauseName, Operator.LESS_THAN_EQUALS, end))).thenReturn(clauseBuilder);
        when(clauseBuilder.clause(new AndClause(new TerminalClauseImpl(clauseName, Operator.GREATER_THAN_EQUALS, start), new TerminalClauseImpl(
                clauseName, Operator.LESS_THAN_EQUALS, end)))).thenReturn(clauseBuilder);

        DefaultJqlClauseBuilder builder = createBuilder(clauseBuilder);
        assertSame(builder, builder.addRangeCondition(clauseName, start, null));
        assertSame(builder, builder.addRangeCondition(clauseName, null, end));
        assertSame(builder, builder.addRangeCondition(clauseName, start, end));
        resetInnerBuilderAndCallClearToVerifyBuilderStructure(builder);
    }

    @Test
    public void testAddDateConditionVarArgsNullQuery() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addDateCondition(null, new Date(), new Date());
    }

    @Test
    public void testAddDateConditionVarArgsNullVarargs() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addDateCondition("cool", (Date[]) null);
    }

    @Test
    public void testAddDateConditionVarArgsNullSecondVarargParam() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addDateCondition("cool", new Date(), null);
    }

    @Test
    public void testAddDateConditionVarArgsNoVarargParams() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addDateCondition("cool");
    }

    @Test
    public void testAddDateConditionVarArgs() throws Exception {
        final Date date1 = new Date(1L);
        final Date date2 = new Date(2L);
        final String date1String = "IamDate1String";
        final String date2String = "Date2StringIam";
        final String clauseName = "clauseName";

        when(dateSupport.getDateString(date1)).thenReturn(date1String);
        when(dateSupport.getDateString(date2)).thenReturn(date2String);

        when(clauseBuilder.clause(new TerminalClauseImpl(clauseName, Operator.EQUALS, new SingleValueOperand(date1String)))).thenReturn(
                clauseBuilder);
        when(clauseBuilder.clause(new TerminalClauseImpl(clauseName, Operator.IN, new MultiValueOperand(date1String, date2String)))).thenReturn(
                clauseBuilder);

        DefaultJqlClauseBuilder builder = createBuilder(clauseBuilder, dateSupport);
        assertSame(builder, builder.addDateCondition(clauseName, date1));
        assertSame(builder, builder.addDateCondition(clauseName, date1, date2));
        resetInnerBuilderAndCallClearToVerifyBuilderStructure(builder);
    }

    @Test
    public void testAddDateConditionVarArgsOperatorNullQuery() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addDateCondition(null, Operator.IN, new Date(), new Date());
    }

    @Test
    public void testAddDateConditionVarArgsOperatorNullOperator() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addDateCondition("brenden", (Operator) null, new Date(), new Date());
    }

    @Test
    public void testAddDateConditionVarArgsOperatorNullVarargs() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addDateCondition("cool", Operator.IN, (Date[]) null);
    }

    @Test
    public void testAddDateConditionVarArgsOperatorNullSecondVarargParam() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addDateCondition("cool", Operator.EQUALS, new Date(), null);
    }

    @Test
    public void testAddDateConditionVarArgsOperatorNoVarargParams() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addDateCondition("cool", Operator.NOT_IN);
    }

    @Test
    public void testAddDateConditionVarArgsOperator() throws Exception {
        final Date date1 = new Date(1L);
        final Date date2 = new Date(2L);
        final String date1String = "IamDate1String";
        final String date2String = "Date2StringIam";
        final String clauseName = "clauseName";

        when(dateSupport.getDateString(date1)).thenReturn(date1String);
        when(dateSupport.getDateString(date2)).thenReturn(date2String);

        when(clauseBuilder.clause(new TerminalClauseImpl(clauseName, Operator.LIKE, new MultiValueOperand(date1String)))).thenReturn(clauseBuilder);
        when(clauseBuilder.clause(new TerminalClauseImpl(clauseName, Operator.LIKE, new MultiValueOperand(date1String, date2String)))).thenReturn(
                clauseBuilder);

        DefaultJqlClauseBuilder builder = createBuilder(clauseBuilder, dateSupport);
        assertSame(builder, builder.addDateCondition(clauseName, Operator.LIKE, new Date[]{date1}));
        assertSame(builder, builder.addDateCondition(clauseName, Operator.LIKE, date1, date2));
        resetInnerBuilderAndCallClearToVerifyBuilderStructure(builder);
    }

    @Test
    public void testAddDateConditionCollectionNullQuery() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addDateCondition(null, Collections.singletonList(new Date()));
    }

    @Test
    public void testAddDateConditionCollectionNullCollection() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addDateCondition("cool", (Collection<Date>) null);
    }

    @Test
    public void testAddDateConditionCollectionCollectionContainingNull() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addDateCondition("cool", Collections.<Date>singletonList(null));
    }

    @Test
    public void testAddDateConditionCollectionEmptyCollection() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addDateCondition("cool", Collections.<Date>emptyList());
    }

    @Test
    public void testAddDateConditionCollection() throws Exception {
        final Date date1 = new Date(1L);
        final Date date2 = new Date(2L);
        final String date1String = "IamDate1String";
        final String date2String = "Date2StringIam";
        final String clauseName = "clauseName";

        when(dateSupport.getDateString(date1)).thenReturn(date1String);
        when(dateSupport.getDateString(date2)).thenReturn(date2String);

        when(clauseBuilder.clause(new TerminalClauseImpl(clauseName, Operator.EQUALS, new SingleValueOperand(date1String)))).thenReturn(
                clauseBuilder);
        when(clauseBuilder.clause(new TerminalClauseImpl(clauseName, Operator.IN, new MultiValueOperand(date1String, date2String)))).thenReturn(
                clauseBuilder);

        DefaultJqlClauseBuilder builder = createBuilder(clauseBuilder, dateSupport);
        assertSame(builder, builder.addDateCondition(clauseName, Collections.singletonList(date1)));
        assertSame(builder, builder.addDateCondition(clauseName, CollectionBuilder.newBuilder(date1, date2).asCollection()));
        resetInnerBuilderAndCallClearToVerifyBuilderStructure(builder);
    }

    @Test
    public void testAddDateConditionCollectionOperatorNullQuery() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addDateCondition(null, Operator.LIKE, Collections.singletonList(new Date()));
    }

    @Test
    public void testAddDateConditionCollectionOperatorNullOperator() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addDateCondition("crap", null, Collections.singletonList(new Date()));
    }

    @Test
    public void testAddDateConditionCollectionOperatorNullCollection() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addDateCondition("cool", Operator.GREATER_THAN_EQUALS, (Collection<Date>) null);
    }

    @Test
    public void testAddDateConditionCollectionOperatorCollectionContainingNull() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addDateCondition("cool", Operator.IS, Collections.<Date>singletonList(null));
    }

    @Test
    public void testAddDateConditionCollectionOperatorEmptyCollection() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addDateCondition("cool", Operator.IS_NOT, Collections.<Date>emptyList());
    }

    @Test
    public void testAddDateConditionCollectionOperator() throws Exception {
        final Date date1 = new Date(1L);
        final Date date2 = new Date(2L);
        final String date1String = "IamDate1String";
        final String date2String = "Date2StringIam";
        final String clauseName = "clauseName";

        when(dateSupport.getDateString(date1)).thenReturn(date1String);
        when(dateSupport.getDateString(date2)).thenReturn(date2String);

        when(clauseBuilder.clause(new TerminalClauseImpl(clauseName, Operator.GREATER_THAN, new MultiValueOperand(date1String)))).thenReturn(clauseBuilder);
        when(clauseBuilder.clause(new TerminalClauseImpl(clauseName, Operator.IS, new MultiValueOperand(date1String, date2String)))).thenReturn(clauseBuilder);

        DefaultJqlClauseBuilder builder = createBuilder(clauseBuilder, dateSupport);
        assertSame(builder, builder.addDateCondition(clauseName, Operator.GREATER_THAN, Collections.singletonList(date1)));
        assertSame(builder, builder.addDateCondition(clauseName, Operator.IS, CollectionBuilder.newBuilder(date1, date2).asCollection()));
        resetInnerBuilderAndCallClearToVerifyBuilderStructure(builder);
    }


    @Test
    public void testEmpty() throws Exception {
        final DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        assertNull(builder.buildClause());
    }

    @Test
    public void testAddFunctionConditionNullQuery() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addFunctionCondition(null, "blah");
    }

    @Test
    public void testAddFunctionConditionNullFunction() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addFunctionCondition("cool", null);
    }

    @Test
    public void testAddFunctionConditionNoArgs() throws Exception {
        when(clauseBuilder.clause(new TerminalClauseImpl("name", Operator.EQUALS, new FunctionOperand("func")))).thenReturn(clauseBuilder);

        DefaultJqlClauseBuilder builder = createBuilder(clauseBuilder);
        assertSame(builder, builder.addFunctionCondition("name", "func"));
        resetInnerBuilderAndCallClearToVerifyBuilderStructure(builder);
    }

    @Test
    public void testAddFunctionConditionVarArgsNullQuery() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addFunctionCondition(null, "blah", "what");
    }

    @Test
    public void testAddFunctionConditionVarArgsNullFunction() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addFunctionCondition("cool", null, "what");
    }

    @Test
    public void testAddFunctionConditionVarArgsNullVarargs() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addFunctionCondition("cool", "blah", (String[]) null);
    }

    @Test
    public void testAddFunctionConditionVarArgsWithNullAsArg() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addFunctionCondition("cool", "blah", null, "contains null");
    }

    @Test
    public void testAddFunctionConditionVarArgs() throws Exception {
        when(clauseBuilder.clause(new TerminalClauseImpl("name", Operator.EQUALS, new FunctionOperand("func", "arg1", "arg2")))).thenReturn(
                clauseBuilder);

        DefaultJqlClauseBuilder builder = createBuilder(clauseBuilder);
        assertSame(builder, builder.addFunctionCondition("name", "func", "arg1", "arg2"));
        resetInnerBuilderAndCallClearToVerifyBuilderStructure(builder);
    }

    @Test
    public void testAddFunctionConditionCollectionNullQuery() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addFunctionCondition(null, "blah", "what");
    }

    @Test
    public void testAddFunctionConditionCollectionNullFunction() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addFunctionCondition("cool", null, "what");
    }

    @Test
    public void testAddFunctionConditionCollectionNullCollection() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addFunctionCondition("cool", "blah", (Collection<String>) null);
    }

    @Test
    public void testAddFunctionConditionCollectionCollectionContainingNull() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addFunctionCondition("cool", "blah", Collections.<String>singletonList(null));
    }

    @Test
    public void testAddFunctionConditionCollection() throws Exception {
        final Collection<String> args = CollectionBuilder.newBuilder("arg1", "arg2", "arg3").asCollection();
        when(clauseBuilder.clause(new TerminalClauseImpl("name", Operator.EQUALS, new FunctionOperand("func", args)))).thenReturn(clauseBuilder);

        DefaultJqlClauseBuilder builder = createBuilder(clauseBuilder);
        assertSame(builder, builder.addFunctionCondition("name", "func", args));
        resetInnerBuilderAndCallClearToVerifyBuilderStructure(builder);
    }

    @Test
    public void testAddFunctionConditionWithOperatorNoArgsNullQuery() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addFunctionCondition(null, Operator.EQUALS, "blah");
    }

    @Test
    public void testAddFunctionConditionWithOperatorNoArgsNullCondition() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addFunctionCondition("cool", Operator.EQUALS, null);
    }

    @Test
    public void testAddFunctionConditionWithOperatorNoArgsNullOperator() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addFunctionCondition("cool", null, "func");
    }

    @Test
    public void testAddFunctionConditionWithOperatorNoArgs() throws Exception {
        when(clauseBuilder.clause(new TerminalClauseImpl("name", Operator.LESS_THAN, new FunctionOperand("func")))).thenReturn(clauseBuilder);

        DefaultJqlClauseBuilder builder = createBuilder(clauseBuilder);
        assertSame(builder, builder.addFunctionCondition("name", Operator.LESS_THAN, "func"));
        resetInnerBuilderAndCallClearToVerifyBuilderStructure(builder);
    }

    @Test
    public void testAddFunctionConditionWithOperatorVarArgsNullQuery() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addFunctionCondition(null, Operator.EQUALS, "blah", "what");
    }

    @Test
    public void testAddFunctionConditionWithOperatorVarArgsNullFunction() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addFunctionCondition("cool", Operator.EQUALS, null, "what");
    }

    @Test
    public void testAddFunctionConditionWithOperatorVarArgsNullVarargs() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addFunctionCondition("cool", Operator.EQUALS, "blah", (String[]) null);
    }

    @Test
    public void testAddFunctionConditionWithOperatorVarArgsNullInVarargs() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addFunctionCondition("cool", Operator.EQUALS, "blah", null, "contains null");
    }

    @Test
    public void testAddFunctionConditionWithOperatorVarArgsNullOperator() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addFunctionCondition("cool", (Operator) null, "blah", "aaa");
    }

    @Test
    public void testAddFunctionConditionWithOperatorVarArgs() throws Exception {
        when(clauseBuilder.clause(new TerminalClauseImpl("name", Operator.GREATER_THAN, new FunctionOperand("func", "arg1", "arg2")))).thenReturn(
                clauseBuilder);

        DefaultJqlClauseBuilder builder = createBuilder(clauseBuilder);
        assertSame(builder, builder.addFunctionCondition("name", Operator.GREATER_THAN, "func", "arg1", "arg2"));
        resetInnerBuilderAndCallClearToVerifyBuilderStructure(builder);
    }

    @Test
    public void testAddFunctionConditionWithOperatorCollectionNullQuery() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addFunctionCondition(null, Operator.NOT_EQUALS, "blah", "what");
    }

    @Test
    public void testAddFunctionConditionWithOperatorCollectionNullFunction() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addFunctionCondition("cool", Operator.NOT_EQUALS, null, "what");
    }

    @Test
    public void testAddFunctionConditionWithOperatorCollectionNullCollection() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addFunctionCondition("cool", Operator.NOT_EQUALS, "blah", (Collection<String>) null);
    }

    @Test
    public void testAddFunctionConditionWithOperatorCollectionCollectionContainingNull() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addFunctionCondition("cool", Operator.NOT_EQUALS, "blah", Collections.<String>singletonList(null));
    }

    @Test
    public void testAddFunctionConditionWithOperatorCollectionNullOperator() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addFunctionCondition("cool", null, "blah", Collections.singletonList("me"));
    }

    @Test
    public void testAddFunctionConditionWithOperatorCollection() throws Exception {
        final Collection<String> args = CollectionBuilder.newBuilder("arg1", "arg2", "arg3").asCollection();
        when(clauseBuilder.clause(new TerminalClauseImpl("name", Operator.NOT_EQUALS, new FunctionOperand("func", args)))).thenReturn(clauseBuilder);

        DefaultJqlClauseBuilder builder = createBuilder(clauseBuilder);
        assertSame(builder, builder.addFunctionCondition("name", Operator.NOT_EQUALS, "func", args));
        resetInnerBuilderAndCallClearToVerifyBuilderStructure(builder);
    }

    @Test
    public void testAddStringConditionSingleNullQuery() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addStringCondition(null, "blah");
    }

    @Test
    public void testAddStringConditionSingleNullCondition() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addStringCondition("cool", (String) null);
    }

    @Test
    public void testAddStringConditionSingle() throws Exception {
        when(clauseBuilder.clause(new TerminalClauseImpl("name", Operator.EQUALS, "value"))).thenReturn(clauseBuilder);

        DefaultJqlClauseBuilder builder = createBuilder(clauseBuilder);
        assertSame(builder, builder.addStringCondition("name", "value"));
        resetInnerBuilderAndCallClearToVerifyBuilderStructure(builder);
    }

    @Test
    public void testAddStringConditionVarArgsNullQuery() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addStringCondition(null, "blah", "blah2");
    }

    @Test
    public void testAddStringConditionVarArgsNullVarargs() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addStringCondition("cool", (String[]) null);
    }

    @Test
    public void testAddStringConditionVarArgsNullInVarargs() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addStringCondition("cool", "me", null);
    }

    @Test
    public void testAddStringConditionVarArgs() throws Exception {
        when(clauseBuilder.clause(new TerminalClauseImpl("name", Operator.EQUALS, new SingleValueOperand("value")))).thenReturn(clauseBuilder);
        when(clauseBuilder.clause(new TerminalClauseImpl("name", Operator.IN, new MultiValueOperand("value", "value2")))).thenReturn(clauseBuilder);

        DefaultJqlClauseBuilder builder = createBuilder(clauseBuilder);
        assertSame(builder, builder.addStringCondition("name", new String[]{"value"}));
        assertSame(builder, builder.addStringCondition("name", "value", "value2"));
        resetInnerBuilderAndCallClearToVerifyBuilderStructure(builder);
    }

    @Test
    public void testAddStringConditionCollectionNullQuery() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addStringCondition(null, CollectionBuilder.newBuilder("blah", "blah2").asList());
    }

    @Test
    public void testAddStringConditionCollectionNullCollection() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addStringCondition("cool", (Collection<String>) null);
    }

    @Test
    public void testAddStringConditionCollectionCollectionContainingNull() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addStringCondition("cool", CollectionBuilder.newBuilder("blah", null).asList());
    }

    @Test
    public void testAddStringConditionCollection() throws Exception {
        when(clauseBuilder.clause(new TerminalClauseImpl("name", Operator.EQUALS, new SingleValueOperand("value")))).thenReturn(clauseBuilder);
        when(clauseBuilder.clause(new TerminalClauseImpl("name", Operator.IN, new MultiValueOperand("value", "value2")))).thenReturn(clauseBuilder);

        DefaultJqlClauseBuilder builder = createBuilder(clauseBuilder);
        assertSame(builder, builder.addStringCondition("name", Collections.singletonList("value")));
        assertSame(builder, builder.addStringCondition("name", CollectionBuilder.newBuilder("value", "value2").asList()));
        resetInnerBuilderAndCallClearToVerifyBuilderStructure(builder);
    }

    @Test
    public void testAddStringConditionSingleOperatorNullQuery() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addStringCondition(null, Operator.IN, "blah");
    }

    @Test
    public void testAddStringConditionSingleOperatorNullCondition() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addStringCondition("cool", Operator.IS_NOT, (String) null);
    }

    @Test
    public void testAddStringConditionSingleOperatorNullOperator() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addStringCondition("cool", null, "ejklwr");
    }

    @Test
    public void testAddStringConditionSingleOperator() throws Exception {
        when(clauseBuilder.clause(new TerminalClauseImpl("name", Operator.LIKE, "value"))).thenReturn(clauseBuilder);

        DefaultJqlClauseBuilder builder = createBuilder(clauseBuilder);
        assertSame(builder, builder.addStringCondition("name", Operator.LIKE, "value"));
        resetInnerBuilderAndCallClearToVerifyBuilderStructure(builder);
    }

    @Test
    public void testAddStringConditionVarArgsOperatorNullQuery() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addStringCondition(null, Operator.LIKE, "blah", "blah2");
    }

    @Test
    public void testAddStringConditionVarArgsOperatorNullVarargs() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addStringCondition("cool", Operator.IS_NOT, (String[]) null);
    }

    @Test
    public void testAddStringConditionVarArgsOperatorNullInVarargs() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addStringCondition("cool", Operator.LIKE, "me", null);
    }

    @Test
    public void testAddStringConditionVarArgsOperatorNullOperator() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addStringCondition("cool", (Operator) null, "me", "two");
    }

    @Test
    public void testAddStringConditionVarArgsOperator() throws Exception {
        when(clauseBuilder.clause(new TerminalClauseImpl("name", Operator.LIKE, new MultiValueOperand("value")))).thenReturn(clauseBuilder);
        when(clauseBuilder.clause(new TerminalClauseImpl("name", Operator.NOT_IN, new MultiValueOperand("value", "value2")))).thenReturn(
                clauseBuilder);

        DefaultJqlClauseBuilder builder = createBuilder(clauseBuilder);
        assertSame(builder, builder.addStringCondition("name", Operator.LIKE, new String[]{"value"}));
        assertSame(builder, builder.addStringCondition("name", Operator.NOT_IN, "value", "value2"));
        resetInnerBuilderAndCallClearToVerifyBuilderStructure(builder);
    }

    @Test
    public void testAddStringConditionCollectionOperatorNullQuery() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addStringCondition(null, Operator.GREATER_THAN, CollectionBuilder.newBuilder("blah", "blah2").asList());
    }

    @Test
    public void testAddStringConditionCollectionOperatorNullCollection() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addStringCondition("cool", Operator.LESS_THAN, (Collection<String>) null);
    }

    @Test
    public void testAddStringConditionCollectionOperatorCollectionContainingNull() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addStringCondition("cool", Operator.LESS_THAN, CollectionBuilder.newBuilder("blah", null).asList());
    }

    @Test
    public void testAddStringConditionCollectionOperatorNullOperator() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addStringCondition("cool", null, CollectionBuilder.newBuilder("blah", "qwerty").asList());
    }

    @Test
    public void testAddStringConditionCollectionOperator() throws Exception {
        when(clauseBuilder.clause(new TerminalClauseImpl("name", Operator.GREATER_THAN_EQUALS, new MultiValueOperand("value")))).thenReturn(
                clauseBuilder);
        when(clauseBuilder.clause(new TerminalClauseImpl("name", Operator.LESS_THAN_EQUALS, new MultiValueOperand("value", "value2")))).thenReturn(
                clauseBuilder);

        DefaultJqlClauseBuilder builder = createBuilder(clauseBuilder);
        assertSame(builder, builder.addStringCondition("name", Operator.GREATER_THAN_EQUALS, Collections.singletonList("value")));
        assertSame(builder, builder.addStringCondition("name", Operator.LESS_THAN_EQUALS, CollectionBuilder.newBuilder("value", "value2").asList()));
        resetInnerBuilderAndCallClearToVerifyBuilderStructure(builder);
    }

    @Test
    public void testAddNumberConditionSingleNullQuery() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addNumberCondition(null, 5L);
    }

    @Test
    public void testAddNumberConditionSingleNullCondition() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addNumberCondition("DJHSKJD", (Long) null);
    }

    @Test
    public void testAddNumberConditionSingle() throws Exception {
        when(clauseBuilder.clause(new TerminalClauseImpl("name", Operator.EQUALS, 5))).thenReturn(clauseBuilder);

        DefaultJqlClauseBuilder builder = createBuilder(clauseBuilder);
        assertSame(builder, builder.addNumberCondition("name", 5L));
        resetInnerBuilderAndCallClearToVerifyBuilderStructure(builder);
    }

    @Test
    public void testAddNumberConditionVarArgsNullQuery() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addNumberCondition(null, 5L, 6L);
    }

    @Test
    public void testAddNumberConditionVarArgsNullVarargs() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addNumberCondition("cool", (Long[]) null);
    }

    @Test
    public void testAddNumberConditionVarArgsNullInVarargs() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addNumberCondition("cool", 6L, null, 56383L);
    }

    @Test
    public void testAddNumberConditionVarArgs() throws Exception {
        when(clauseBuilder.clause(new TerminalClauseImpl("name", Operator.EQUALS, new SingleValueOperand(6L)))).thenReturn(clauseBuilder);
        when(clauseBuilder.clause(new TerminalClauseImpl("name", Operator.IN, new MultiValueOperand(6L, 8L)))).thenReturn(clauseBuilder);

        DefaultJqlClauseBuilder builder = createBuilder(clauseBuilder);
        assertSame(builder, builder.addNumberCondition("name", new Long[]{6L}));
        assertSame(builder, builder.addNumberCondition("name", 6L, 8L));
        resetInnerBuilderAndCallClearToVerifyBuilderStructure(builder);
    }

    @Test
    public void testAddNumberConditionCollectionNullQuery() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addNumberCondition(null, CollectionBuilder.newBuilder(5l, 6l).asList());
    }

    @Test
    public void testAddNumberConditionCollectionNullCollection() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addNumberCondition("cool", (Collection<Long>) null);
    }

    @Test
    public void testAddNumberConditionCollectionNullInCollection() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addNumberCondition("cool", CollectionBuilder.newBuilder(5L, null).asList());
    }

    @Test
    public void testAddNumberConditionCollection() throws Exception {
        when(clauseBuilder.clause(new TerminalClauseImpl("name", Operator.EQUALS, new SingleValueOperand(5L)))).thenReturn(clauseBuilder);
        when(clauseBuilder.clause(new TerminalClauseImpl("name", Operator.IN, new MultiValueOperand(6L, 5747L)))).thenReturn(clauseBuilder);

        DefaultJqlClauseBuilder builder = createBuilder(clauseBuilder);
        assertSame(builder, builder.addNumberCondition("name", Collections.singletonList(5L)));
        assertSame(builder, builder.addNumberCondition("name", CollectionBuilder.newBuilder(6L, 5747L).asList()));
        resetInnerBuilderAndCallClearToVerifyBuilderStructure(builder);
    }

    @Test
    public void testAddNumberConditionSingleOperatorNullQuery() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addNumberCondition(null, Operator.IN, 6L);
    }

    @Test
    public void testAddNumberConditionSingleOperatorNullOperator() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addNumberCondition("cool", null, 5L);
    }

    @Test
    public void testAddNumberConditionSingleOperatorNullCondition() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addNumberCondition("cool", Operator.LESS_THAN, (Long) null);
    }

    @Test
    public void testAddNumberConditionSingleOperator() throws Exception {
        when(clauseBuilder.clause(new TerminalClauseImpl("name", Operator.LIKE, 6L))).thenReturn(clauseBuilder);

        DefaultJqlClauseBuilder builder = createBuilder(clauseBuilder);
        assertSame(builder, builder.addNumberCondition("name", Operator.LIKE, 6L));
        resetInnerBuilderAndCallClearToVerifyBuilderStructure(builder);
    }

    @Test
    public void testAddNumberConditionMultipleOperatorNullQuery() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addNumberCondition(null, Operator.LIKE, 5L, 8L);
    }

    @Test
    public void testAddNumberConditionMultipleOperatorNullVarargs() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addNumberCondition("cool", Operator.IS_NOT, (Long[]) null);
    }

    @Test
    public void testAddNumberConditionMultipleOperatorNullInVarargs() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addNumberCondition("cool", Operator.IS_NOT, 5L, null, 47373L);
    }

    @Test
    public void testAddNumberConditionMultipleOperatorNullOperator() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addNumberCondition("cool", (Operator) null, 3L, 95L);
    }

    @Test
    public void testAddNumberConditionMultipleOperator() throws Exception {
        when(clauseBuilder.clause(new TerminalClauseImpl("name", Operator.LIKE, new MultiValueOperand(7L)))).thenReturn(clauseBuilder);
        when(clauseBuilder.clause(new TerminalClauseImpl("name", Operator.NOT_IN, new MultiValueOperand(6L, 12L)))).thenReturn(clauseBuilder);

        DefaultJqlClauseBuilder builder = createBuilder(clauseBuilder);
        assertSame(builder, builder.addNumberCondition("name", Operator.LIKE, new Long[]{7L}));
        assertSame(builder, builder.addNumberCondition("name", Operator.NOT_IN, 6L, 12L));
        resetInnerBuilderAndCallClearToVerifyBuilderStructure(builder);
    }

    @Test
    public void testAddNumberConditionCollectionOperatorNullQuery() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addNumberCondition(null, Operator.GREATER_THAN, CollectionBuilder.newBuilder(65l, 3423l).asList());
    }

    @Test
    public void testAddNumberConditionCollectionOperatorNullCollection() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addNumberCondition("cool", Operator.LESS_THAN, (Collection<Long>) null);
    }

    @Test
    public void testAddNumberConditionCollectionOperatorCollectionContainingNull() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addNumberCondition("cool", Operator.LESS_THAN, CollectionBuilder.newBuilder(6l, null).asList());
    }

    @Test
    public void testAddNumberConditionCollectionOperatorNullOperator() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addNumberCondition("cool", null, CollectionBuilder.newBuilder(6l, 7657l).asList());
    }

    @Test
    public void testAddNumberConditionCollectionOperator() throws Exception {
        when(clauseBuilder.clause(new TerminalClauseImpl("name", Operator.GREATER_THAN_EQUALS, new MultiValueOperand(5L)))).thenReturn(clauseBuilder);
        when(clauseBuilder.clause(new TerminalClauseImpl("name", Operator.LESS_THAN_EQUALS, new MultiValueOperand(67L, 654L)))).thenReturn(
                clauseBuilder);

        DefaultJqlClauseBuilder builder = createBuilder(clauseBuilder);
        assertSame(builder, builder.addNumberCondition("name", Operator.GREATER_THAN_EQUALS, Collections.singletonList(5L)));
        assertSame(builder, builder.addNumberCondition("name", Operator.LESS_THAN_EQUALS, CollectionBuilder.newBuilder(67L, 654L).asList()));
        resetInnerBuilderAndCallClearToVerifyBuilderStructure(builder);
    }

    @Test
    public void testConditionBuilder() throws Exception {
        when(clauseBuilder.clause(new TerminalClauseImpl("meh", Operator.EQUALS, 16L))).thenReturn(clauseBuilder);

        final JqlClauseBuilder builder = createBuilder(clauseBuilder);
        assertSame(builder, builder.addCondition("meh").eq().number(16L));
        resetInnerBuilderAndCallClearToVerifyBuilderStructure(builder);
    }

    @Test
    public void testConditionSingleNullQuery() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addCondition(null, new SingleValueOperand("5"));
    }

    @Test
    public void testConditionSingleNullOperand() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addCondition("DJHSKJD", (Operand) null);
    }

    @Test
    public void testConditionSingle() throws Exception {
        when(clauseBuilder.clause(new TerminalClauseImpl("name", Operator.EQUALS, new SingleValueOperand(5L)))).thenReturn(clauseBuilder);

        DefaultJqlClauseBuilder builder = createBuilder(clauseBuilder);
        assertSame(builder, builder.addCondition("name", new SingleValueOperand(5L)));
        resetInnerBuilderAndCallClearToVerifyBuilderStructure(builder);
    }

    @Test
    public void testAddConditionVarArgsNullQuery() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addCondition(null, new SingleValueOperand(5L), new SingleValueOperand(6L));
    }

    @Test
    public void testAddConditionVarArgsNullVarargs() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addCondition("cool", (Operand[]) null);
    }

    @Test
    public void testAddConditionVarArgsNullInVarargs() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addCondition("cool", new SingleValueOperand(6L), null, new SingleValueOperand(56383L));
    }

    @Test
    public void testAddConditionVarArgs() throws Exception {
        when(clauseBuilder.clause(new TerminalClauseImpl("name", Operator.IN, new MultiValueOperand(6L)))).thenReturn(clauseBuilder);
        when(clauseBuilder.clause(new TerminalClauseImpl("name", Operator.IN, new MultiValueOperand(6L, 8L)))).thenReturn(clauseBuilder);

        DefaultJqlClauseBuilder builder = createBuilder(clauseBuilder);
        assertSame(builder, builder.addCondition("name", new Operand[]{new SingleValueOperand(6L)}));
        assertSame(builder, builder.addCondition("name", new SingleValueOperand(6L), new SingleValueOperand(8L)));
        resetInnerBuilderAndCallClearToVerifyBuilderStructure(builder);
    }

    @Test
    public void testAddConditionCollectionNullQuery() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addCondition(null, CollectionBuilder.newBuilder(new SingleValueOperand(5l), new SingleValueOperand(6l)).asList());
    }

    @Test
    public void testAddConditionCollectionNullCollection() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addCondition("cool", (Collection<Operand>) null);
    }

    @Test
    public void testAddConditionCollectionCollectionContainingNull() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addCondition("cool", CollectionBuilder.newBuilder(new SingleValueOperand(5L), null).asList());
    }

    @Test
    public void testAddConditionCollection() throws Exception {
        when(clauseBuilder.clause(new TerminalClauseImpl("name", Operator.IN, new MultiValueOperand(5L)))).thenReturn(clauseBuilder);
        when(clauseBuilder.clause(new TerminalClauseImpl("name", Operator.IN, new MultiValueOperand(6L, 5747L)))).thenReturn(clauseBuilder);

        DefaultJqlClauseBuilder builder = createBuilder(clauseBuilder);
        assertSame(builder, builder.addCondition("name", Collections.singletonList(new SingleValueOperand(5L))));
        assertSame(builder, builder.addCondition("name",
                CollectionBuilder.newBuilder(new SingleValueOperand(6L), new SingleValueOperand(5747L)).asList()));
        resetInnerBuilderAndCallClearToVerifyBuilderStructure(builder);
    }

    @Test
    public void testAddConditionSingleOperatorNullQuery() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addCondition(null, Operator.IN, new SingleValueOperand(6L));
    }

    @Test
    public void testAddConditionSingleOperatorNullOperator() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addCondition("cool", null, new SingleValueOperand(5L));
    }

    @Test
    public void testAddConditionSingleOperatorNullCondition() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addCondition("cool", Operator.LESS_THAN, (Operand) null);
    }

    @Test
    public void testAddConditionSingleOperator() throws Exception {
        when(clauseBuilder.clause(new TerminalClauseImpl("name", Operator.LIKE, 6L))).thenReturn(clauseBuilder);

        DefaultJqlClauseBuilder builder = createBuilder(clauseBuilder);
        assertSame(builder, builder.addCondition("name", Operator.LIKE, new SingleValueOperand(6L)));
        resetInnerBuilderAndCallClearToVerifyBuilderStructure(builder);
    }

    @Test
    public void testAddConditionMultipleOperatorNullQuery() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addCondition(null, Operator.LIKE, new SingleValueOperand(5L), new SingleValueOperand(8L));
    }

    @Test
    public void testAddConditionMultipleOperatorNullVarargs() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addCondition("cool", Operator.IS_NOT, (SingleValueOperand[]) null);
    }

    @Test
    public void testAddConditionMultipleOperatorNullInVarargs() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addCondition("cool", Operator.IS_NOT, new SingleValueOperand(5L), null, new SingleValueOperand(5349239852L));
    }

    @Test
    public void testAddConditionMultipleOperatorNullOperator() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addCondition("cool", (Operator) null, new SingleValueOperand(5L), new SingleValueOperand(95L));
    }

    @Test
    public void testAddConditionMultipleOperator() throws Exception {
        when(clauseBuilder.clause(new TerminalClauseImpl("name", Operator.LIKE, new MultiValueOperand(7L)))).thenReturn(clauseBuilder);
        when(clauseBuilder.clause(new TerminalClauseImpl("name", Operator.NOT_IN, new MultiValueOperand(6L, 12L)))).thenReturn(clauseBuilder);

        DefaultJqlClauseBuilder builder = createBuilder(clauseBuilder);
        assertSame(builder, builder.addCondition("name", Operator.LIKE, new Operand[]{new SingleValueOperand(7L)}));
        assertSame(builder, builder.addCondition("name", Operator.NOT_IN, new SingleValueOperand(6L), new SingleValueOperand(12L)));
        resetInnerBuilderAndCallClearToVerifyBuilderStructure(builder);
    }

    @Test
    public void testAddConditionCollectionOperatorNullQuery() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addCondition(null, Operator.GREATER_THAN,
                CollectionBuilder.newBuilder(new SingleValueOperand(65l), new SingleValueOperand(3423l)).asList());
    }

    @Test
    public void testAddConditionCollectionOperatorNullCollection() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addCondition("cool", Operator.LESS_THAN, (Collection<Operand>) null);
    }

    @Test
    public void testAddConditionCollectionOperatorCollectionContainingNull() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addCondition("cool", Operator.LESS_THAN, CollectionBuilder.newBuilder(new SingleValueOperand(6l), null).asList());
    }

    @Test
    public void testAddConditionCollectionOperatorNullOperator() throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        exception.expect(IllegalArgumentException.class);
        builder.addCondition("cool", null, CollectionBuilder.newBuilder(new SingleValueOperand(6L), new SingleValueOperand(7657l)).asList());
    }

    @Test
    public void testAddConditionCollectionOperator() throws Exception {
        when(clauseBuilder.clause(new TerminalClauseImpl("name", Operator.GREATER_THAN_EQUALS, new MultiValueOperand(5L)))).thenReturn(clauseBuilder);
        when(clauseBuilder.clause(new TerminalClauseImpl("name", Operator.LESS_THAN_EQUALS, new MultiValueOperand(67L, 654L)))).thenReturn(
                clauseBuilder);

        DefaultJqlClauseBuilder builder = createBuilder(clauseBuilder);
        assertSame(builder, builder.addCondition("name", Operator.GREATER_THAN_EQUALS, Collections.singletonList(new SingleValueOperand(5L))));
        assertSame(builder, builder.addCondition("name", Operator.LESS_THAN_EQUALS, CollectionBuilder.newBuilder(new SingleValueOperand(67L),
                new SingleValueOperand(654L)).asList()));
        resetInnerBuilderAndCallClearToVerifyBuilderStructure(builder);
    }

    @Test
    public void testBuildClause() throws Exception {
        final Clause expectedReturn = new TerminalClauseImpl("check", Operator.GREATER_THAN_EQUALS, 5L);

        when(clauseBuilder.build()).thenReturn(expectedReturn);

        final JqlClauseBuilder builder = createBuilder(clauseBuilder);
        assertEquals(expectedReturn, builder.buildClause());
    }

    @Test
    public void testDefaultAnd() throws Exception {
        when(clauseBuilder.defaultAnd()).thenReturn(clauseBuilder);

        final JqlClauseBuilder builder = createBuilder(clauseBuilder);
        assertSame(builder, builder.defaultAnd());
        resetInnerBuilderAndCallClearToVerifyBuilderStructure(builder);
    }

    @Test
    public void testAnd() throws Exception {
        when(clauseBuilder.and()).thenReturn(clauseBuilder);

        final JqlClauseBuilder builder = createBuilder(clauseBuilder);
        assertSame(builder, builder.and());
        resetInnerBuilderAndCallClearToVerifyBuilderStructure(builder);
    }

    @Test
    public void testDefaultOr() throws Exception {
        when(clauseBuilder.defaultOr()).thenReturn(clauseBuilder);

        final JqlClauseBuilder builder = createBuilder(clauseBuilder);
        assertSame(builder, builder.defaultOr());
        resetInnerBuilderAndCallClearToVerifyBuilderStructure(builder);
    }

    @Test
    public void testOr() throws Exception {
        when(clauseBuilder.or()).thenReturn(clauseBuilder);

        final JqlClauseBuilder builder = createBuilder(clauseBuilder);
        assertSame(builder, builder.or());
        resetInnerBuilderAndCallClearToVerifyBuilderStructure(builder);
    }

    @Test
    public void testDefaultNone() throws Exception {
        when(clauseBuilder.defaultNone()).thenReturn(clauseBuilder);

        final JqlClauseBuilder builder = createBuilder(clauseBuilder);
        assertSame(builder, builder.defaultNone());
        resetInnerBuilderAndCallClearToVerifyBuilderStructure(builder);
    }

    @Test
    public void testNot() throws Exception {
        when(clauseBuilder.not()).thenReturn(clauseBuilder);

        final JqlClauseBuilder builder = createBuilder(clauseBuilder);
        assertSame(builder, builder.not());
        resetInnerBuilderAndCallClearToVerifyBuilderStructure(builder);
    }

    @Test
    public void testSub() throws Exception {
        when(clauseBuilder.sub()).thenReturn(clauseBuilder);

        final JqlClauseBuilder builder = createBuilder(clauseBuilder);
        assertSame(builder, builder.sub());
        resetInnerBuilderAndCallClearToVerifyBuilderStructure(builder);
    }

    @Test
    public void testEndSub() throws Exception {
        when(clauseBuilder.endsub()).thenReturn(clauseBuilder);

        final JqlClauseBuilder builder = createBuilder(clauseBuilder);
        assertSame(builder, builder.endsub());
        resetInnerBuilderAndCallClearToVerifyBuilderStructure(builder);
    }

    @Test
    public void testAddClause() throws Exception {
        final Clause clause1 = new TerminalClauseImpl("name", Operator.EQUALS, "value");
        final Clause clause2 = new TerminalClauseImpl("bad", Operator.NOT_EQUALS, "egg");

        when(clauseBuilder.clause(clause1)).thenReturn(clauseBuilder);
        when(clauseBuilder.clause(clause2)).thenReturn(clauseBuilder);

        final JqlClauseBuilder builder = createBuilder(clauseBuilder);
        assertSame(builder, builder.addClause(clause1));
        assertSame(builder, builder.addClause(clause2));
        resetInnerBuilderAndCallClearToVerifyBuilderStructure(builder);
    }

    @Test
    public void testAffectedVersionSingle() throws Exception {
        assertStringSingle("affectedVersion", JqlClauseBuilder::affectedVersion);
    }

    @Test
    public void testAffectedVersionMultiple() throws Exception {
        assertStringVarArgs("affectedVersion", JqlClauseBuilder::affectedVersion);
    }

    @Test
    public void testAffectedVersionIsEmpty() throws Exception {
        assertEmpty("affectedVersion", (builder, argument) -> builder.affectedVersionIsEmpty());
    }

    @Test
    public void testAffectedVersion() throws Exception {
        assertBuilder("affectedVersion", (builder, argument) -> builder.affectedVersion());
    }

    @Test
    public void testFixVersionSingle() throws Exception {
        assertStringSingle("fixVersion", JqlClauseBuilder::fixVersion);
    }

    @Test
    public void testFixVersionMultiple() throws Exception {
        assertStringVarArgs("fixVersion", JqlClauseBuilder::fixVersion);
    }

    @Test
    public void testFixVersionIdSingle() throws Exception {
        assertLongSingle("fixVersion", JqlClauseBuilder::fixVersion);
    }

    @Test
    public void testFixVersionIdMultiple() throws Exception {
        assertLongVarArgs("fixVersion", JqlClauseBuilder::fixVersion);
    }

    @Test
    public void testFixVersionIsEmpty() throws Exception {
        assertEmpty("fixVersion", (builder, argument) -> builder.fixVersionIsEmpty());
    }

    @Test
    public void testFixVersion() throws Exception {
        assertBuilder("fixVersion", (builder, argument) -> builder.fixVersion());
    }

    @Test
    public void testPriorityMultiple() throws Exception {
        assertStringVarArgs("priority", JqlClauseBuilder::priority);
    }

    @Test
    public void testPriority() throws Exception {
        assertBuilder("priority", (builder, argument) -> builder.priority());
    }

    @Test
    public void testResolutionMultiple() throws Exception {
        assertStringVarArgs("resolution", JqlClauseBuilder::resolution);
    }

    @Test
    public void testResolution() throws Exception {
        assertBuilder("resolution", (builder, argument) -> builder.resolution());
    }

    @Test
    public void testUnresolved() throws Exception {
        final SimpleClauseBuilder builder = mock(SimpleClauseBuilder.class);
        final TerminalClause clause = new TerminalClauseImpl("resolution", Operator.EQUALS, "Unresolved");

        when(builder.clause(clause)).thenReturn(builder);

        final JqlClauseBuilder clauseBuilder = createBuilder(builder);
        assertSame(clauseBuilder, clauseBuilder.unresolved());
    }

    @Test
    public void testStatusMultiple() throws Exception {
        assertStringVarArgs("status", JqlClauseBuilder::status);
    }

    @Test
    public void testStatusBuilder() throws Exception {
        assertBuilder("status", (builder, argument) -> builder.status());
    }

    @Test
    public void testIssueTypeMultiple() throws Exception {
        assertStringVarArgs("issuetype", JqlClauseBuilder::issueType);
    }

    @Test
    public void testIssueTypeBuilder() throws Exception {
        assertBuilder("issuetype", (builder, argument) -> builder.issueType());
    }

    @Test
    public void testIssueTypeIsStandard() throws Exception {
        final JqlClauseBuilder builder = createBuilder(clauseBuilder);
        when(clauseBuilder.clause(new TerminalClauseImpl("issuetype", Operator.IN, new FunctionOperand(
                AllStandardIssueTypesFunction.FUNCTION_STANDARD_ISSUE_TYPES)))).thenReturn(clauseBuilder);
        assertSame(builder, builder.issueTypeIsStandard());
        resetInnerBuilderAndCallClearToVerifyBuilderStructure(builder);
    }

    @Test
    public void testIssueTypeIsSubtask() throws Exception {
        final JqlClauseBuilder builder = createBuilder(clauseBuilder);
        when(clauseBuilder.clause(new TerminalClauseImpl("issuetype", Operator.IN, new FunctionOperand(
                AllSubIssueTypesFunction.FUNCTION_SUB_ISSUE_TYPES)))).thenReturn(clauseBuilder);
        assertSame(builder, builder.issueTypeIsSubtask());
        resetInnerBuilderAndCallClearToVerifyBuilderStructure(builder);
    }

    @Test
    public void testDescriptionSingleValue() throws Exception {
        assertStringSingle("description", JqlClauseBuilder::description, Operator.LIKE);
    }

    @Test
    public void testDescriptionIsEmpty() throws Exception {
        assertEmpty("description", (builder, argument) -> builder.descriptionIsEmpty());
    }

    @Test
    public void testDescriptionBuilder() throws Exception {
        assertBuilder("description", (builder, argument) -> builder.description());
    }

    @Test
    public void testSummarySingleValue() throws Exception {
        assertStringSingle("summary", JqlClauseBuilder::summary, Operator.LIKE);
    }

    @Test
    public void testSummaryBuilder() throws Exception {
        assertBuilder("summary", (builder, argument) -> builder.summary());
    }

    @Test
    public void testEnvironmentSingleValue() throws Exception {
        assertStringSingle("environment", JqlClauseBuilder::environment, Operator.LIKE);
    }

    @Test
    public void testEnvironmentIsEmpty() throws Exception {
        assertEmpty("environment", (builder, argument) -> builder.environmentIsEmpty());
    }

    @Test
    public void testEnvironmentBuilder() throws Exception {
        assertBuilder("environment", (builder, argument) -> builder.environment());
    }

    @Test
    public void testCommentSingleValue() throws Exception {
        assertStringSingle("comment", JqlClauseBuilder::comment, Operator.LIKE);
    }

    @Test
    public void testCommentBuilder() throws Exception {
        assertBuilder("comment", (builder, argument) -> builder.comment());
    }

    @Test
    public void testProjectStrings() throws Exception {
        assertStringVarArgs("project", JqlClauseBuilder::project);
    }

    @Test
    public void testProjectLongs() throws Exception {
        assertLongVarArgs("project", JqlClauseBuilder::project);
    }

    @Test
    public void testProjectBuilder() throws Exception {
        assertBuilder("project", (builder, argument) -> builder.project());
    }

    @Test
    public void testCategoryStrings() throws Exception {
        assertStringVarArgs("category", JqlClauseBuilder::category);
    }

    @Test
    public void testCategoryBuilder() throws Exception {
        assertBuilder("category", (builder, argument) -> builder.category());
    }

    @Test
    public void testCreatedAfterDate() throws Exception {
        assertDateAfterDate("created", JqlClauseBuilder::createdAfter);
    }

    @Test
    public void testCreatedAfterString() throws Exception {
        assertDateAfterString("created", JqlClauseBuilder::createdAfter);
    }

    @Test
    public void testCreatedBetweenDate() throws Exception {
        assertDateRangeDate("created", (builder, argument) -> builder.createdBetween(argument[0], argument[1]));
    }

    @Test
    public void testCreatedBetweenString() throws Exception {
        assertDateRangeString("created", (builder, argument) -> builder.createdBetween(argument[0], argument[1]));
    }

    @Test
    public void testCreatedBuilder() throws Exception {
        assertBuilder("created", (builder, argument) -> builder.created());
    }

    @Test
    public void testUpdatedAfterDate() throws Exception {
        assertDateAfterDate("updated", JqlClauseBuilder::updatedAfter);
    }

    @Test
    public void testUpdatedAfterString() throws Exception {
        assertDateAfterString("updated", JqlClauseBuilder::updatedAfter);
    }

    @Test
    public void testUpdatedBetweenDate() throws Exception {
        assertDateRangeDate("updated", (builder, argument) -> builder.updatedBetween(argument[0], argument[1]));
    }

    @Test
    public void testUpdatedBetweenString() throws Exception {
        assertDateRangeString("updated", (builder, argument) -> builder.updatedBetween(argument[0], argument[1]));
    }

    @Test
    public void testUpdatedBuilder() throws Exception {
        assertBuilder("updated", (builder, argument) -> builder.updated());
    }

    @Test
    public void testDueAfterDate() throws Exception {
        assertDateAfterDate("due", JqlClauseBuilder::dueAfter);
    }

    @Test
    public void testDueAfterString() throws Exception {
        assertDateAfterString("due", JqlClauseBuilder::dueAfter);
    }

    @Test
    public void testDueBetweenDate() throws Exception {
        assertDateRangeDate("due", (builder, argument) -> builder.dueBetween(argument[0], argument[1]));
    }

    @Test
    public void testDueBetweenString() throws Exception {
        assertDateRangeString("due", (builder, argument) -> builder.dueBetween(argument[0], argument[1]));
    }

    @Test
    public void testDueBuilder() throws Exception {
        assertBuilder("due", (builder, argument) -> builder.due());
    }


    @Test
    public void testLastViewedAfterDate() throws Exception {
        assertDateAfterDate("lastViewed", JqlClauseBuilder::lastViewedAfter);
    }

    @Test
    public void testLastViewedAfterString() throws Exception {
        assertDateAfterString("lastViewed", JqlClauseBuilder::lastViewedAfter);
    }

    @Test
    public void testLastViewedBetweenDate() throws Exception {
        assertDateRangeDate("lastViewed", (builder, argument) -> builder.lastViewedBetween(argument[0], argument[1]));
    }

    @Test
    public void testLastViewedBetweenString() throws Exception {
        assertDateRangeString("lastViewed", (builder, argument) -> builder.lastViewedBetween(argument[0], argument[1]));
    }

    @Test
    public void testLastViewedBuilder() throws Exception {
        assertBuilder("lastViewed", (builder, argument) -> builder.lastViewed());
    }

    @Test
    public void testResolutionDateBetweenDate() throws Exception {
        assertDateRangeDate("resolved", (builder, argument) -> builder.resolutionDateBetween(argument[0], argument[1]));
    }

    // JRA-21590
    @Test
    public void testResolutionDateBetweenString() throws Exception {
        assertDateRangeString("resolved", (builder, argument) -> builder.resolutionDateBetween(argument[0], argument[1]));
    }

    @Test
    public void testCreateBetween() {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        builder.createdBetween("-1h", null);
        assertEquals("{created >= \"-1h\"}", builder.buildClause().toString());

        builder = new DefaultJqlClauseBuilder(timeZoneManager);
        builder.createdBetween(null, "-1h");
        assertEquals("{created <= \"-1h\"}", builder.buildClause().toString());
    }

    @Test
    public void testResolutionDateBuilder() throws Exception {
        assertBuilder("resolved", (builder, argument) -> builder.resolutionDate());
    }

    @Test
    public void testReporterUser() throws Exception {
        assertStringSingle("reporter", JqlClauseBuilder::reporterUser);
    }

    @Test
    public void testReporterInGroupNullGroup() throws Exception {
        final JqlClauseBuilder builder = createBuilder(clauseBuilder);
        exception.expect(IllegalArgumentException.class);
        builder.reporterInGroup(null);
    }

    @Test
    public void testReporterInGroup() throws Exception {
        final String jqlName = "reporter";
        final String group = "group1";

        when(clauseBuilder.clause(new TerminalClauseImpl(jqlName, Operator.IN, new FunctionOperand("membersOf", group)))).thenReturn(clauseBuilder);

        final JqlClauseBuilder builder = createBuilder(clauseBuilder);
        assertSame(builder, builder.reporterInGroup(group));
        resetInnerBuilderAndCallClearToVerifyBuilderStructure(builder);
    }

    @Test
    public void testReporterIsCurrent() throws Exception {
        final String jqlName = "reporter";
        when(clauseBuilder.clause(new TerminalClauseImpl(jqlName, Operator.EQUALS, new FunctionOperand("currentUser")))).thenReturn(clauseBuilder);

        final JqlClauseBuilder builder = createBuilder(clauseBuilder);
        assertSame(builder, builder.reporterIsCurrentUser());
        resetInnerBuilderAndCallClearToVerifyBuilderStructure(builder);
    }

    @Test
    public void testReporterIsEmpty() throws Exception {
        assertEmpty("reporter", (builder, argument) -> builder.reporterIsEmpty());
    }

    @Test
    public void testReporterBuilder() throws Exception {
        assertBuilder("reporter", (builder, argument) -> builder.reporter());
    }

    @Test
    public void testAssigneeUser() throws Exception {
        assertStringSingle("assignee", JqlClauseBuilder::assigneeUser);
    }

    @Test
    public void testAssigneeInGroupNullGroup() throws Exception {
        final JqlClauseBuilder builder = createBuilder(clauseBuilder);
        exception.expect(IllegalArgumentException.class);
        builder.assigneeInGroup(null);
    }

    @Test
    public void testAssigneeInGroup() throws Exception {
        final String jqlName = "assignee";
        final String group = "group1";

        when(clauseBuilder.clause(new TerminalClauseImpl(jqlName, Operator.IN, new FunctionOperand("membersOf", group)))).thenReturn(clauseBuilder);
        final JqlClauseBuilder builder = createBuilder(clauseBuilder);
        assertSame(builder, builder.assigneeInGroup(group));
        resetInnerBuilderAndCallClearToVerifyBuilderStructure(builder);
    }

    @Test
    public void testAssigneeIsCurrent() throws Exception {
        final String jqlName = "assignee";
        when(clauseBuilder.clause(new TerminalClauseImpl(jqlName, Operator.EQUALS, new FunctionOperand("currentUser")))).thenReturn(clauseBuilder);
        final JqlClauseBuilder builder = createBuilder(clauseBuilder);
        assertSame(builder, builder.assigneeIsCurrentUser());
        resetInnerBuilderAndCallClearToVerifyBuilderStructure(builder);
    }

    @Test
    public void testAssigneeIsEmpty() throws Exception {
        assertEmpty("assignee", (builder, argument) -> builder.assigneeIsEmpty());
    }

    @Test
    public void testAssigneeBuilder() throws Exception {
        assertBuilder("assignee", (builder, argument) -> builder.assignee());
    }

    @Test
    public void testComponentStrings() throws Exception {
        assertStringVarArgs("component", JqlClauseBuilder::component);
    }

    @Test
    public void testComponentLongs() throws Exception {
        assertLongVarArgs("component", JqlClauseBuilder::component);
    }

    @Test
    public void testComponentEmpty() throws Exception {
        assertEmpty("component", (builder, argument) -> builder.componentIsEmpty());
    }

    @Test
    public void testComponentBuilder() throws Exception {
        assertBuilder("component", (builder, argument) -> builder.component());
    }

    @Test
    public void testIssueKeys() throws Exception {
        assertStringVarArgs("key", JqlClauseBuilder::issue);
    }

    @Test
    public void testIssueKeyInHistory() throws Exception {
        final String jqlName = "key";
        when(clauseBuilder.clause(new TerminalClauseImpl(jqlName, Operator.IN, new FunctionOperand("issueHistory")))).thenReturn(clauseBuilder);

        final JqlClauseBuilder builder = createBuilder(clauseBuilder);
        assertSame(builder, builder.issueInHistory());
        resetInnerBuilderAndCallClearToVerifyBuilderStructure(builder);
    }

    @Test
    public void testIssueKeyInWatchedIssues() throws Exception {
        final String jqlName = "key";
        when(clauseBuilder.clause(new TerminalClauseImpl(jqlName, Operator.IN, new FunctionOperand("watchedIssues")))).thenReturn(clauseBuilder);

        final JqlClauseBuilder builder = createBuilder(clauseBuilder);
        assertSame(builder, builder.issueInWatchedIssues());
        resetInnerBuilderAndCallClearToVerifyBuilderStructure(builder);
    }

    @Test
    public void testIssueKeyInVotedIssues() throws Exception {
        final String jqlName = "key";
        when(clauseBuilder.clause(new TerminalClauseImpl(jqlName, Operator.IN, new FunctionOperand("votedIssues")))).thenReturn(clauseBuilder);

        final JqlClauseBuilder builder = createBuilder(clauseBuilder);
        assertSame(builder, builder.issueInVotedIssues());
        resetInnerBuilderAndCallClearToVerifyBuilderStructure(builder);
    }

    @Test
    public void testIssueKeyBuilder() throws Exception {
        assertBuilder("key", (builder, argument) -> builder.issue());
    }

    @Test
    public void testIssueParent() throws Exception {
        assertStringVarArgs("parent", JqlClauseBuilder::issueParent);
    }

    @Test
    public void testIssueParentBuilder() throws Exception {
        assertBuilder("parent", (builder, argument) -> builder.issueParent());
    }

    @Test
    public void testOriginalEstimateBuilder() throws Exception {
        assertBuilder("originalEstimate", (builder, argument) -> builder.originalEstimate());
    }

    @Test
    public void testCurrentEstimateBuilder() throws Exception {
        assertBuilder("remainingEstimate", (builder, argument) -> builder.currentEstimate());
    }

    @Test
    public void testTimespent() throws Exception {
        assertBuilder("timespent", (builder, argument) -> builder.timeSpent());
    }

    @Test
    public void testWorkRatio() throws Exception {
        assertBuilder("workratio", (builder, argument) -> builder.workRatio());
    }

    @Test
    public void testIssueLevel() throws Exception {
        assertStringVarArgs("level", JqlClauseBuilder::level);
    }

    @Test
    public void testIssueLevelBuilder() throws Exception {
        assertBuilder("level", (builder, argument) -> builder.level());
    }

    @Test
    public void testSavedFilter() throws Exception {
        assertStringVarArgs("filter", JqlClauseBuilder::savedFilter);
    }

    @Test
    public void testSavedFilterBuilder() throws Exception {
        assertBuilder("filter", (builder, argument) -> builder.savedFilter());
    }

    @Test
    public void testVotesBuilder() throws Exception {
        assertBuilder("votes", (builder, argument) -> builder.votes());
    }

    @Test
    public void testVoterUser() throws Exception {
        assertStringSingle("voter", JqlClauseBuilder::voterUser);
    }

    @Test
    public void testVoterInGroupNullGroup() throws Exception {
        final JqlClauseBuilder builder = createBuilder(clauseBuilder);
        exception.expect(IllegalArgumentException.class);
        builder.voterInGroup(null);
    }

    @Test
    public void testVoterInGroup() throws Exception {
        final String jqlName = "voter";
        final String group = "group1";
        when(clauseBuilder.clause(new TerminalClauseImpl(jqlName, Operator.IN, new FunctionOperand("membersOf", group)))).thenReturn(clauseBuilder);

        final JqlClauseBuilder builder = createBuilder(clauseBuilder);
        assertSame(builder, builder.voterInGroup(group));
        resetInnerBuilderAndCallClearToVerifyBuilderStructure(builder);
    }

    @Test
    public void testVoterIsCurrent() throws Exception {
        final String jqlName = "voter";
        when(clauseBuilder.clause(new TerminalClauseImpl(jqlName, Operator.EQUALS, new FunctionOperand("currentUser")))).thenReturn(clauseBuilder);

        final JqlClauseBuilder builder = createBuilder(clauseBuilder);
        assertSame(builder, builder.voterIsCurrentUser());
        resetInnerBuilderAndCallClearToVerifyBuilderStructure(builder);
    }

    @Test
    public void testVoterIsEmpty() throws Exception {
        assertEmpty("voter", (builder, argument) -> builder.voterIsEmpty());
    }

    @Test
    public void testVoterBuilder() throws Exception {
        assertBuilder("voter", (builder, argument) -> builder.voter());
    }

    @Test
    public void testWatcherUser() throws Exception {
        assertStringSingle("watcher", JqlClauseBuilder::watcherUser);
    }

    @Test
    public void testWatcherInGroupNullGroup() throws Exception {
        final JqlClauseBuilder builder = createBuilder(clauseBuilder);
        exception.expect(IllegalArgumentException.class);
        builder.watcherInGroup(null);
    }

    @Test
    public void testWatcherInGroup() throws Exception {
        final String jqlName = "watcher";
        final String group = "group1";
        when(clauseBuilder.clause(new TerminalClauseImpl(jqlName, Operator.IN, new FunctionOperand("membersOf", group)))).thenReturn(clauseBuilder);

        final JqlClauseBuilder builder = createBuilder(clauseBuilder);
        assertSame(builder, builder.watcherInGroup(group));
        resetInnerBuilderAndCallClearToVerifyBuilderStructure(builder);
    }

    @Test
    public void testWatcherIsCurrent() throws Exception {
        final String jqlName = "watcher";
        when(clauseBuilder.clause(new TerminalClauseImpl(jqlName, Operator.EQUALS, new FunctionOperand("currentUser")))).thenReturn(clauseBuilder);

        final JqlClauseBuilder builder = createBuilder(clauseBuilder);
        assertSame(builder, builder.watcherIsCurrentUser());
        resetInnerBuilderAndCallClearToVerifyBuilderStructure(builder);
    }

    @Test
    public void testWatcherIsEmpty() throws Exception {
        assertEmpty("watcher", (builder, argument) -> builder.watcherIsEmpty());
    }

    @Test
    public void testWatcherBuilder() throws Exception {
        assertBuilder("watcher", (builder, argument) -> builder.watcher());
    }

    @Test
    public void testFieldBuilder() throws Exception {
        final String fieldName = "my field";
        assertBuilder(fieldName, (builder, argument) -> builder.field(fieldName));
    }

    @Test
    public void testCustomField() throws Exception {
        final Long id = 50023428L;
        assertBuilder("cf[" + id + "]", (builder, argument) -> builder.customField(id));
    }

    private interface TestCallable<V, A> {
        V call(JqlClauseBuilder builder, A argument);
    }

    private void assertBuilder(final String jqlName, final TestCallable<ConditionBuilder, Void> callable)
            throws Exception {
        when(clauseBuilder.clause(new TerminalClauseImpl(jqlName, Operator.EQUALS, 16L))).thenReturn(clauseBuilder);

        final JqlClauseBuilder builder = createBuilder(clauseBuilder);
        assertSame(builder, callable.call(builder, null).eq().number(16L));
        resetInnerBuilderAndCallClearToVerifyBuilderStructure(builder);
    }

    private void assertEmpty(final String jqlName, final TestCallable<JqlClauseBuilder, Void> callable) throws Exception {
        when(clauseBuilder.clause(new TerminalClauseImpl(jqlName, Operator.IS, EmptyOperand.EMPTY))).thenReturn(clauseBuilder);

        final JqlClauseBuilder builder = createBuilder(clauseBuilder);
        assertSame(builder, callable.call(builder, null));
        resetInnerBuilderAndCallClearToVerifyBuilderStructure(builder);
    }

    private void assertStringSingle(final String jqlName, final TestCallable<JqlClauseBuilder, String> callable)
            throws Exception {
        assertStringSingle(jqlName, callable, Operator.EQUALS);
    }

    private void assertStringSingle(final String jqlName, final TestCallable<JqlClauseBuilder, String> callable, final Operator operator)
            throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);

        try {
            callable.call(builder, null);
            fail("Expecting an error to be thrown.");
        } catch (final IllegalArgumentException e) {
            //expected.
        }

        final String value = "10";
        when(clauseBuilder.clause(new TerminalClauseImpl(jqlName, operator, value))).thenReturn(clauseBuilder);

        builder = createBuilder(clauseBuilder);
        assertSame(builder, callable.call(builder, value));
        resetInnerBuilderAndCallClearToVerifyBuilderStructure(builder);

    }

    private void assertStringVarArgs(final String jqlName, final TestCallable<JqlClauseBuilder, String[]> callable)
            throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        try {
            callable.call(builder, new String[]{});
            fail("Expecting an error to be thrown.");
        } catch (final IllegalArgumentException e) {
            //expected.
        }

        try {
            callable.call(builder, new String[]{"value", null});
            fail("Expecting an error to be thrown.");
        } catch (final IllegalArgumentException e) {
            //expected.
        }

        final String[] values = new String[]{"10", "", "3939"};
        when(clauseBuilder.clause(new TerminalClauseImpl(jqlName, Operator.IN, new MultiValueOperand(values)))).thenReturn(clauseBuilder);

        builder = createBuilder(clauseBuilder);
        assertSame(builder, callable.call(builder, values));
        resetInnerBuilderAndCallClearToVerifyBuilderStructure(builder);
    }

    private void assertLongSingle(final String jqlName, final TestCallable<JqlClauseBuilder, Long> callable)
            throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);

        try {
            callable.call(builder, null);
            fail("Expecting an error to be thrown.");
        } catch (final IllegalArgumentException e) {
            //expected.
        }

        final Long value = 10L;
        when(clauseBuilder.clause(new TerminalClauseImpl(jqlName, Operator.EQUALS, value))).thenReturn(clauseBuilder);

        builder = createBuilder(clauseBuilder);
        assertSame(builder, callable.call(builder, value));
        resetInnerBuilderAndCallClearToVerifyBuilderStructure(builder);
    }

    private void assertLongVarArgs(final String jqlName, final TestCallable<JqlClauseBuilder, Long[]> callable)
            throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        try {
            callable.call(builder, new Long[]{});
            fail("Expecting an error to be thrown.");
        } catch (final IllegalArgumentException e) {
            //expected.
        }

        try {
            callable.call(builder, new Long[]{6L, null});
            fail("Expecting an error to be thrown.");
        } catch (final IllegalArgumentException e) {
            //expected.
        }

        final Long[] values = new Long[]{1L, 50L, 60L};
        when(clauseBuilder.clause(new TerminalClauseImpl(jqlName, Operator.IN, new MultiValueOperand(values)))).thenReturn(clauseBuilder);

        builder = createBuilder(clauseBuilder);
        assertSame(builder, callable.call(builder, values));
        resetInnerBuilderAndCallClearToVerifyBuilderStructure(builder);
    }

    private void assertDateAfterDate(final String jqlName, final TestCallable<JqlClauseBuilder, Date> callable)
            throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        try {
            callable.call(builder, null);
            fail("Expecting an error to be thrown.");
        } catch (final IllegalArgumentException e) {
            //expected.
        }

        final Date dateStart = new Date(34573284534L);
        final String dateStartString = "%&*##*$&$";

        when(dateSupport.getDateString(dateStart)).thenReturn(dateStartString);
        when(clauseBuilder.clause(new TerminalClauseImpl(jqlName, Operator.GREATER_THAN_EQUALS, dateStartString))).thenReturn(clauseBuilder);

        builder = createBuilder(clauseBuilder, dateSupport);
        assertSame(builder, callable.call(builder, dateStart));
        resetInnerBuilderAndCallClearToVerifyBuilderStructure(builder);
    }

    private void assertDateAfterString(final String jqlName, final TestCallable<JqlClauseBuilder, String> callable)
            throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        try {
            callable.call(builder, null);
            fail("Expecting an error to be thrown.");
        } catch (final IllegalArgumentException e) {
            //expected.
        }

        final String dateStartString = "%&*##*$&$";
        when(clauseBuilder.clause(new TerminalClauseImpl(jqlName, Operator.GREATER_THAN_EQUALS, dateStartString))).thenReturn(clauseBuilder);

        builder = createBuilder(clauseBuilder);
        assertSame(builder, callable.call(builder, dateStartString));
        resetInnerBuilderAndCallClearToVerifyBuilderStructure(builder);
    }

    private void assertDateRangeDate(final String jqlName, final TestCallable<JqlClauseBuilder, Date[]> callable)
            throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        try {
            callable.call(builder, new Date[]{null, null});
            fail("Expecting an error to be thrown.");
        } catch (final IllegalArgumentException e) {
            //expected.
        }

        final Date dateStart = new Date(34573284534L);
        final Date dateEnd = new Date(3452785834573895748L);
        final String dateStartString = "%&*##*$&$";
        final String dateEndString = "dhakjdhakjhsdsa";

        when(dateSupport.getDateString(dateStart)).thenReturn(dateStartString);
        when(dateSupport.getDateString(dateEnd)).thenReturn(dateEndString);

        when(clauseBuilder.clause(new TerminalClauseImpl(jqlName, Operator.GREATER_THAN_EQUALS, dateStartString))).thenReturn(clauseBuilder);
        when(clauseBuilder.clause(new TerminalClauseImpl(jqlName, Operator.LESS_THAN_EQUALS, dateEndString))).thenReturn(clauseBuilder);
        when(clauseBuilder.clause(new AndClause(new TerminalClauseImpl(jqlName, Operator.GREATER_THAN_EQUALS, dateStartString),
                new TerminalClauseImpl(jqlName, Operator.LESS_THAN_EQUALS, dateEndString)))).thenReturn(clauseBuilder);

        builder = createBuilder(clauseBuilder, dateSupport);
        assertSame(builder, callable.call(builder, new Date[]{dateStart, null}));
        assertSame(builder, callable.call(builder, new Date[]{null, dateEnd}));
        assertSame(builder, callable.call(builder, new Date[]{dateStart, dateEnd}));
        resetInnerBuilderAndCallClearToVerifyBuilderStructure(builder);
    }

    private void assertDateRangeString(final String jqlName, final TestCallable<JqlClauseBuilder, String[]> callable)
            throws Exception {
        DefaultJqlClauseBuilder builder = new DefaultJqlClauseBuilder(timeZoneManager);
        try {
            callable.call(builder, new String[]{null, null});
            fail("Expecting an error to be thrown.");
        } catch (final IllegalArgumentException e) {
            //expected.
        }

        final String dateStartString = "%&*##*$&$";
        final String dateEndString = "dhakjdhakjhsdsa";

        when(clauseBuilder.clause(new TerminalClauseImpl(jqlName, Operator.GREATER_THAN_EQUALS, dateStartString))).thenReturn(clauseBuilder);
        when(clauseBuilder.clause(new TerminalClauseImpl(jqlName, Operator.LESS_THAN_EQUALS, dateEndString))).thenReturn(clauseBuilder);
        when(clauseBuilder.clause(new AndClause(new TerminalClauseImpl(jqlName, Operator.GREATER_THAN_EQUALS, dateStartString),
                new TerminalClauseImpl(jqlName, Operator.LESS_THAN_EQUALS, dateEndString)))).thenReturn(clauseBuilder);

        builder = createBuilder(clauseBuilder);
        assertSame(builder, callable.call(builder, new String[]{dateStartString, null}));
        assertSame(builder, callable.call(builder, new String[]{null, dateEndString}));
        assertSame(builder, callable.call(builder, new String[]{dateStartString, dateEndString}));
        resetInnerBuilderAndCallClearToVerifyBuilderStructure(builder);
    }

    @Test
    public void testAttachmentsExists() throws Exception {
        final String jqlName = "attachments";
        when(clauseBuilder.clause(Mockito.eq(new TerminalClauseImpl(jqlName, Operator.IS_NOT, EmptyOperand.OPERAND_NAME)))).thenReturn(clauseBuilder);

        final JqlClauseBuilder builder = createBuilder(clauseBuilder);
        assertSame(builder, builder.attachmentsExists(true));
        resetInnerBuilderAndCallClearToVerifyBuilderStructure(builder);
    }

    DefaultJqlClauseBuilder createBuilder(final SimpleClauseBuilder clauseBuilder) {
        return createBuilder(clauseBuilder, dateSupport);
    }

    DefaultJqlClauseBuilder createBuilder(final SimpleClauseBuilder clauseBuilder, final JqlDateSupport dateSupport) {
        return new DefaultJqlClauseBuilder(null, clauseBuilder, dateSupport);
    }

    /*
     We need to check, if the inner clause builder is still what we expect. Calling clear here is the
     lightest I could find to assert on the implementation.
     If this succeeds this means all the other mocking on clause builder was actually called, if the test is
     structured to mock inner SimpleClauseBuilder to return self on specific calls.
     */
    private void resetInnerBuilderAndCallClearToVerifyBuilderStructure(final JqlClauseBuilder builder) {
        reset(clauseBuilder);
        builder.clear();
        verify(clauseBuilder).clear();
    }

}
