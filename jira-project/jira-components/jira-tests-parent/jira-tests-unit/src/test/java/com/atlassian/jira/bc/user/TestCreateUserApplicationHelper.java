package com.atlassian.jira.bc.user;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.application.api.ApplicationManager;
import com.atlassian.collectors.CollectorsUtil;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.crowd.embedded.api.OperationType;
import com.atlassian.extras.api.jira.JiraLicense;
import com.atlassian.jira.application.ApplicationKeys;
import com.atlassian.jira.application.ApplicationRole;
import com.atlassian.jira.application.ApplicationRoleManager;
import com.atlassian.jira.bc.group.GroupsToApplicationsSeatingHelper;
import com.atlassian.jira.bc.license.JiraLicenseService;
import com.atlassian.jira.bc.license.MockApplicationRoleTestHelper;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.exception.CreateException;
import com.atlassian.jira.license.JiraLicenseManager;
import com.atlassian.jira.license.LicenseCountService;
import com.atlassian.jira.license.LicenseDetails;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.auth.AuthorisationManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.MockCrowdService;
import com.atlassian.jira.user.MockGroup;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.BaseUrl;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.MockBaseUrl;
import com.atlassian.jira.util.NoopI18nHelper;
import com.atlassian.jira.web.component.admin.group.GroupLabelsService;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeDiagnosingMatcher;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

import static com.atlassian.jira.bc.user.CreateUserApplicationHelper.LINK_END;
import static com.atlassian.jira.bc.user.CreateUserApplicationHelper.LINK_START;
import static com.atlassian.jira.bc.user.UserApplicationHelper.ApplicationSelection;
import static com.atlassian.jira.util.NoopI18nHelper.makeTranslation;
import static com.google.common.collect.ImmutableSet.of;
import static java.lang.String.format;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.emptyIterable;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestCreateUserApplicationHelper {
    private static final Optional<Long> DIRECTORY_ID = Optional.of(1L);

    private final MockApplicationUser mockedUser = new MockApplicationUser("fred");

    @Mock
    private ApplicationRoleManager applicationRoleManager;
    @Mock
    private GroupsToApplicationsSeatingHelper groupsToApplicationsSeatingHelper;
    @Mock
    private UserManager userManager;

    private final I18nHelper i18nHelper = new NoopI18nHelper();
    private final BaseUrl baseUrl = new MockBaseUrl("", "");
    private CreateUserApplicationHelper helper;
    private MockApplicationRoleTestHelper mockLicHelper;
    private MockCrowdService crowdService;
    @Mock
    private GlobalPermissionManager globalPermissionManager;
    @Mock
    private ApplicationManager applicationManager;
    @Mock
    private LicenseCountService licenseCountService;
    @Mock
    private JiraLicenseService jiraLicenseService;
    @Mock
    private FeatureManager featureManager;
    @Mock
    private JiraLicenseManager jiraLicenseManager;
    @Mock
    private GroupLabelsService groupLabels;
    @Mock
    private Directory directory;
    @Mock
    private AuthorisationManager authorisationManager;

    @Before
    public void setupMock() {
        mockLicHelper = new MockApplicationRoleTestHelper();
        crowdService = new MockCrowdService();

        when(userManager.getDirectory(DIRECTORY_ID.get())).thenReturn(directory);
        when(directory.getAllowedOperations()).thenReturn(ImmutableSet.of(OperationType.CREATE_GROUP));
        when(directory.getName()).thenReturn("My awesome directory");


        helper = new CreateUserApplicationHelper(applicationRoleManager, i18nHelper, baseUrl, globalPermissionManager,
                licenseCountService, featureManager, jiraLicenseManager, groupsToApplicationsSeatingHelper, userManager, crowdService, groupLabels, authorisationManager);
    }

    @Test
    public void shouldReturnNoErrorsWhenNoApplicationKeysSelectedByUser() {
        //Given
        //When
        final Collection<String> errorCollection = helper.validateApplicationKeys(DIRECTORY_ID, Sets.<ApplicationKey>newHashSet());

        //Then
        assertNotNull(errorCollection);
        assertTrue(errorCollection.isEmpty());
    }

    @Test
    public void shouldReturnErrorWhenSelectedApplicationIsUnlicensed() {
        //When
        final Collection<String> errorCollection = helper.validateApplicationKeys(DIRECTORY_ID, Sets.newHashSet(mockLicHelper.UNLICENSED_APP_KEY));

        //Then
        assertNotNull(errorCollection);
        assertFalse(errorCollection.isEmpty());
        assertEquals(1, errorCollection.size());
        assertEquals("admin.errors.user.add.user.application.not.licensed{[" +
                mockLicHelper.UNLICENSED_APP_KEY.value() + "]}", errorCollection.iterator().next());
    }

    @Test
    public void shouldReturnErrorWhenUnableToAddUserToOneOfApplications() {
        //Given
        final Set<ApplicationRole> applicationRoles = of(mockLicHelper.ZERO_USER_LIMIT_APP_ROLE, mockLicHelper.NO_DEFAULT_GROUP_APP_ROLE);
        final Set<ApplicationKey> applicationKeys = applicationRoles.stream().map(ApplicationRole::getKey).collect(CollectorsUtil.toImmutableSet());
        mockLicHelper.setup(applicationManager, ImmutableList.of(), applicationRoleManager, applicationRoles);

        when(groupsToApplicationsSeatingHelper.findEffectiveApplications(DIRECTORY_ID, applicationRoles)).thenReturn(applicationRoles);

        //When
        final Collection<String> errorCollection = helper.validateApplicationKeys(DIRECTORY_ID, applicationKeys);

        //Then
        assertThat(errorCollection, hasItems(
                format("admin.errors.user.add.user.application.no.default.group{[%s, , ]}",
                        mockLicHelper.NO_DEFAULT_GROUP_APP_ROLE.getName()),
                format("admin.errors.user.add.user.application.license.limit.reached{[%s, , , , ]}",
                        mockLicHelper.ZERO_USER_LIMIT_APP_ROLE.getName())
        ));
    }

    @Test
    public void shouldReturnErrorWhenApplicationAccessNotEffectivelyGranted() {
        //Given
        final Set<ApplicationRole> applicationRoles = of(mockLicHelper.ZERO_USER_LIMIT_APP_ROLE, mockLicHelper.NO_DEFAULT_GROUP_APP_ROLE);
        final Set<ApplicationKey> applicationKeys = applicationRoles.stream().map(ApplicationRole::getKey).collect(CollectorsUtil.toImmutableSet());
        mockLicHelper.setup(applicationManager, ImmutableList.of(), applicationRoleManager, applicationRoles);

        when(groupsToApplicationsSeatingHelper.findEffectiveApplications(DIRECTORY_ID, applicationRoles)).thenReturn(of());

        //When
        final Collection<String> errorCollection = helper.validateApplicationKeys(DIRECTORY_ID, applicationKeys);

        //Then
        assertThat(errorCollection, contains("admin.errors.user.add.user.application.access.effectively.not.granted{[]}"));
    }

    @Test
    public void shouldCheckAgainstDefaultDirectoryWhenEmptyGiven() {
        //Given
        final Directory directory = mock(Directory.class);
        when(directory.getId()).thenReturn(42L);
        when(userManager.getDefaultCreateDirectory()).thenReturn(Optional.of(directory));

        final Set<ApplicationRole> applicationRoles = of(mockLicHelper.ZERO_USER_LIMIT_APP_ROLE, mockLicHelper.NO_DEFAULT_GROUP_APP_ROLE);
        mockLicHelper.setup(applicationManager, ImmutableList.of(), applicationRoleManager, applicationRoles);
        when(groupsToApplicationsSeatingHelper.findEffectiveApplications(Optional.of(42L), of())).thenReturn(applicationRoles);

        //When
        final Collection<String> errorCollection = helper.validateApplicationKeys(Optional.empty(), of());

        //Then
        assertThat(errorCollection, hasItems(
                format("admin.errors.user.add.user.application.license.limit.reached{[%s, , , , ]}",
                        mockLicHelper.ZERO_USER_LIMIT_APP_ROLE.getName()),
                format("admin.errors.user.add.user.application.no.default.group{[%s, , ]}",
                        mockLicHelper.NO_DEFAULT_GROUP_APP_ROLE.getName())
        ));
    }

    @Test
    public void shouldReturnErrorWhenThereIsNoWritableDirectory() {
        //Given
        when(userManager.getDefaultCreateDirectory()).thenReturn(Optional.empty());

        //When
        final Collection<String> errorCollection = helper.validateApplicationKeys(Optional.empty(), of());

        //Then
        assertThat(errorCollection, hasItem("admin.errors.no.writable.directory{[]}"));
    }

    @Test
    public void shouldNotCheckRoleFreeSeatsWhenUserIsAlreadyInGivenApp() {
        final MockApplicationUser user = new MockApplicationUser("fred");

        when(applicationRoleManager.getRolesForUser(user)).thenReturn(of(mockLicHelper.CORE_APP_ROLE));

        final CreateUserApplicationHelper.ValidationParams validationParams = CreateUserApplicationHelper.ValidationParams.withUser(user);
        assertFalse(helper.validateRoleSeats(mockLicHelper.CORE_APP_ROLE, validationParams).isPresent());

        verify(applicationRoleManager, never()).hasSeatsAvailable(mockLicHelper.CORE_APP_KEY, 1);
    }

    @Test
    public void shouldReturnErrorWhenThereIsNoMoreFreeSeats() {
        final MockApplicationUser user = new MockApplicationUser("fred");

        when(applicationRoleManager.hasSeatsAvailable(mockLicHelper.CORE_APP_KEY, 1)).thenReturn(false);

        final CreateUserApplicationHelper.ValidationParams validationParams = CreateUserApplicationHelper.ValidationParams.withUser(user);
        assertThat(helper.validateRoleSeats(mockLicHelper.CORE_APP_ROLE, validationParams).get().getMessage(),
                is("admin.errors.user.add.user.application.license.limit.reached{[JIRA Core, , , , ]}")
        );
    }

    @Test
    public void shouldGetLicencedApplicationsForSelection() {
        //Given
        when(applicationRoleManager.getRoles()).thenReturn(mockLicHelper.SOFTWARE_PORTFOLIO_OTHER_APP_ROLES);

        //When
        final List<ApplicationSelection> applicationsForSelection = helper.getApplicationsForSelection(ImmutableSet.of(), DIRECTORY_ID);

        //Then
        assertNotNull(applicationsForSelection);
        assertEquals(mockLicHelper.SOFTWARE_PORTFOLIO_OTHER_APP_ROLES.size(), applicationsForSelection.size());
    }

    @Test
    public void shouldGetEmptyApplicationsForSelectionWhenNotLicensed() {
        //When
        final List<ApplicationSelection> applicationsForSelection = helper.getApplicationsForSelection(ImmutableSet.of(), DIRECTORY_ID);

        //Then
        assertNotNull(applicationsForSelection);

        final String errorMessageIfNotEmpty = format("ApplicationForSelection not empty found [%d] items : %s",
                applicationsForSelection.size(), Objects.toString(applicationsForSelection));
        assertTrue(errorMessageIfNotEmpty, applicationsForSelection.isEmpty());
    }

    @Test
    public void shouldNotBeAbleToSelectApplicationWithError() {
        //Given
        mockLicHelper.setup(
                applicationManager, Sets.newHashSet(mockLicHelper.NO_DEFAULT_GROUP_APPLICATION,
                        mockLicHelper.ZERO_USER_LIMIT_APPLICATION,
                        mockLicHelper.SOFTWARE_APPLICATION),
                applicationRoleManager, Sets.newHashSet(mockLicHelper.NO_DEFAULT_GROUP_APP_ROLE,
                        mockLicHelper.ZERO_USER_LIMIT_APP_ROLE,
                        mockLicHelper.SOFTWARE_APP_ROLE));
        //When
        final List<ApplicationSelection> applicationsForSelection = helper.getApplicationsForSelection(ImmutableSet.of(), DIRECTORY_ID);

        //Then
        List<Matcher<? super ApplicationSelection>> expected = Lists.newArrayList();

        expected.add(matcherForKey(mockLicHelper.NO_DEFAULT_GROUP_APP_KEY)
                .withDisplayNameNoCore(mockLicHelper.NO_DEFAULT_GROUP_TRADEMARK)
                .withNoGroupError()
                .notSelectable()
                .notDefined());

        expected.add(matcherForKey(mockLicHelper.SOFTWARE_APP_KEY)
                .withDisplayNameNoCore(mockLicHelper.SOFTWARE_TRADEMARK)
                .selectable()
                .notDefined());

        expected.add(matcherForKey(mockLicHelper.ZERO_USER_LIMIT_APP_KEY)
                .withDisplayNameNoCore(mockLicHelper.ZERO_USER_LIMIT_TRADEMARK)
                .withNoSeatsError()
                .notSelectable()
                .notDefined());

        assertThat(applicationsForSelection, contains(expected));
    }

    @Test
    public void shouldNotBeAbleToSelectApplicationWhenDefaultDirectoryIsNotWritable() {
        when(directory.getAllowedOperations()).thenReturn(ImmutableSet.of());

        //Given
        mockLicHelper.setup(
                applicationManager, Sets.newHashSet(mockLicHelper.SOFTWARE_APPLICATION),
                applicationRoleManager, Sets.newHashSet(mockLicHelper.SOFTWARE_APP_ROLE));

        //When
        final List<ApplicationSelection> applicationsForSelection = helper.getApplicationsForSelection(ImmutableSet.of(), DIRECTORY_ID);
        List<Matcher<? super ApplicationSelection>> expected = ImmutableList.of(
                matcherForKey(mockLicHelper.SOFTWARE_APP_KEY)
                        .withDisplayNameNoCore(mockLicHelper.SOFTWARE_TRADEMARK)
                        .noWritableDirectory()
        );

        //Then
        assertThat(applicationsForSelection, contains(expected));
    }

    @Test
    public void shallListCoreLicenseLast() {
        //Given
        when(applicationRoleManager.hasSeatsAvailable(any(ApplicationKey.class), eq(1))).thenReturn(true);
        when(applicationRoleManager.getRoles()).thenReturn(Sets.newHashSet(mockLicHelper.SOFTWARE_APP_ROLE,
                mockLicHelper.CORE_APP_ROLE, mockLicHelper.ZERO_USER_LIMIT_APP_ROLE));

        //when
        final List<ApplicationSelection> applicationsForSelection = helper.getApplicationsForSelection(ImmutableSet.of(), DIRECTORY_ID);

        //Then
        assertNotNull(applicationsForSelection);
        assertTrue(applicationsForSelection.size() == 3);
        assertThat("CORE is last", applicationsForSelection.get(2).getKey(), equalTo(ApplicationKeys.CORE.value()));
    }

    @Test
    public void shallMarkSelectedApps() {
        //Given
        mockLicHelper.setup(
                applicationManager, Sets.newHashSet(mockLicHelper.CORE_APPLICATION,
                        mockLicHelper.PORTFOLIO_APPLICATION,
                        mockLicHelper.SOFTWARE_APPLICATION),
                applicationRoleManager, Sets.newHashSet(mockLicHelper.CORE_APP_ROLE,
                        mockLicHelper.PORTFOLIO_APP_ROLE,
                        mockLicHelper.SOFTWARE_APP_ROLE));

        //when
        final List<ApplicationSelection>
                applicationsForSelection = helper.getApplicationsForSelection(
                of(mockLicHelper.CORE_APP_KEY, mockLicHelper.SOFTWARE_APP_KEY), DIRECTORY_ID);

        //Then
        List<Matcher<? super ApplicationSelection>> expected = Lists.newArrayList();

        expected.add(matcherForKey(mockLicHelper.PORTFOLIO_APP_KEY)
                .withDisplayNameAndCore(mockLicHelper.PORTFOLIO_TRADEMARK)
                .selectable()
                .notSelected());

        expected.add(matcherForKey(mockLicHelper.SOFTWARE_APP_KEY)
                .withDisplayNameAndCore(mockLicHelper.SOFTWARE_TRADEMARK)
                .selectable()
                .selected());

        expected.add(matcherForKey(mockLicHelper.CORE_APP_KEY)
                .withDisplayNameNoCore(mockLicHelper.CORE_TRADEMARK)
                .selectable()
                .selected());

        assertThat(applicationsForSelection, contains(expected));
    }

    @Test
    public void shallMarkIndeterminateApps() {
        //Given
        mockLicHelper.setup(
                applicationManager, Sets.newHashSet(mockLicHelper.CORE_APPLICATION,
                        mockLicHelper.OTHER_APPLICATION,
                        mockLicHelper.PORTFOLIO_APPLICATION,
                        mockLicHelper.SOFTWARE_APPLICATION),
                applicationRoleManager, Sets.newHashSet(mockLicHelper.CORE_APP_ROLE,
                        mockLicHelper.OTHER_APP_ROLE,
                        mockLicHelper.PORTFOLIO_APP_ROLE,
                        mockLicHelper.SOFTWARE_APP_ROLE));
        when(mockLicHelper.PORTFOLIO_APP_ROLE.getGroups()).thenReturn(ImmutableSet.of(mockLicHelper.CORE_DEFAULT_GROUP,
                mockLicHelper.PORTFOLIO_DEFAULT_GROUP, mockLicHelper.PORTFOLIO_NOT_DEFAULT_GROUP));

        //when
        final List<ApplicationSelection>
                applicationsForSelection = helper.getApplicationsForSelection(
                of(mockLicHelper.CORE_APP_KEY, mockLicHelper.SOFTWARE_APP_KEY), DIRECTORY_ID);

        //Then
        List<Matcher<? super ApplicationSelection>> expected = Lists.newArrayList();

        expected.add(matcherForKey(mockLicHelper.OTHER_APP_KEY)
                .withDisplayNameAndCore(mockLicHelper.OTHER_TRADEMARK)
                .selectable()
                .notSelected()
                .notIndeterminate());

        expected.add(matcherForKey(mockLicHelper.PORTFOLIO_APP_KEY)
                .withDisplayNameAndCore(mockLicHelper.PORTFOLIO_TRADEMARK)
                .selectable()
                .notSelected()
                .indeterminate());

        expected.add(matcherForKey(mockLicHelper.SOFTWARE_APP_KEY)
                .withDisplayNameAndCore(mockLicHelper.SOFTWARE_TRADEMARK)
                .selectable()
                .selected());

        expected.add(matcherForKey(mockLicHelper.CORE_APP_KEY)
                .withDisplayNameNoCore(mockLicHelper.CORE_TRADEMARK)
                .selectable()
                .selected());

        assertThat(applicationsForSelection, contains(expected));
    }

    @Test
    public void shallNotMarkIndeterminateAppsWhenGroupsDontIntersect() {
        //Given
        mockLicHelper.setup(
                applicationManager, Sets.newHashSet(mockLicHelper.CORE_APPLICATION,
                        mockLicHelper.PORTFOLIO_APPLICATION,
                        mockLicHelper.SOFTWARE_APPLICATION),
                applicationRoleManager, Sets.newHashSet(mockLicHelper.CORE_APP_ROLE,
                        mockLicHelper.PORTFOLIO_APP_ROLE,
                        mockLicHelper.SOFTWARE_APP_ROLE));
        when(mockLicHelper.PORTFOLIO_APP_ROLE.getGroups()).thenReturn(mockLicHelper.OTHER_GROUPS);
        when(mockLicHelper.PORTFOLIO_APP_ROLE.getDefaultGroups()).thenReturn(ImmutableSet.of(mockLicHelper.OTHER_DEFAULT_GROUP));

        //when
        final List<ApplicationSelection>
                applicationsForSelection = helper.getApplicationsForSelection(
                of(mockLicHelper.CORE_APP_KEY, mockLicHelper.SOFTWARE_APP_KEY), DIRECTORY_ID);

        //Then
        List<Matcher<? super ApplicationSelection>> expected = Lists.newArrayList();

        expected.add(matcherForKey(mockLicHelper.PORTFOLIO_APP_KEY)
                .withDisplayNameAndCore(mockLicHelper.PORTFOLIO_TRADEMARK)
                .selectable()
                .notSelected()
                .notIndeterminate());

        expected.add(matcherForKey(mockLicHelper.SOFTWARE_APP_KEY)
                .withDisplayNameAndCore(mockLicHelper.SOFTWARE_TRADEMARK)
                .selectable()
                .selected());

        expected.add(matcherForKey(mockLicHelper.CORE_APP_KEY)
                .withDisplayNameNoCore(mockLicHelper.CORE_TRADEMARK)
                .selectable()
                .selected());

        assertThat(applicationsForSelection, contains(expected));
    }

    @Test
    public void shallMarkIndeterminateAppsWhenUserEffectiveAccess() {
        //given
        mockLicHelper.setup(
                applicationManager, Sets.newHashSet(mockLicHelper.CORE_APPLICATION,
                        mockLicHelper.PORTFOLIO_APPLICATION,
                        mockLicHelper.SOFTWARE_APPLICATION),
                applicationRoleManager, Sets.newHashSet(mockLicHelper.CORE_APP_ROLE,
                        mockLicHelper.PORTFOLIO_APP_ROLE,
                        mockLicHelper.SOFTWARE_APP_ROLE));
        ApplicationUser user = new MockApplicationUser("freddie", DIRECTORY_ID.get());
        crowdService.addUserToGroup(user, mockLicHelper.SOFTWARE_DEFAULT_GROUP);
        crowdService.addUserToGroup(user, mockLicHelper.CORE_DEFAULT_GROUP);
        crowdService.addUserToGroup(user, mockLicHelper.PORTFOLIO_NOT_DEFAULT_GROUP);

        //when
        final List<ApplicationSelection>
                applicationsForSelection = helper.getApplicationsForUser(user);

        //Then
        List<Matcher<? super ApplicationSelection>> expected = Lists.newArrayList();

        expected.add(matcherForKey(mockLicHelper.PORTFOLIO_APP_KEY)
                .withDisplayNameAndCore(mockLicHelper.PORTFOLIO_TRADEMARK)
                .selectable()
                .notSelected()
                .indeterminate());

        expected.add(matcherForKey(mockLicHelper.SOFTWARE_APP_KEY)
                .withDisplayNameAndCore(mockLicHelper.SOFTWARE_TRADEMARK)
                .selectable()
                .selected());

        expected.add(matcherForKey(mockLicHelper.CORE_APP_KEY)
                .withDisplayNameNoCore(mockLicHelper.CORE_TRADEMARK)
                .selectable()
                .selected());

        assertThat(applicationsForSelection, contains(expected));
    }

    @Test
    public void shallNotMarkIndeterminateAppsWhenUserHasNoEffectiveAccess() {
        //given
        mockLicHelper.setup(
                applicationManager, Sets.newHashSet(mockLicHelper.CORE_APPLICATION,
                        mockLicHelper.PORTFOLIO_APPLICATION,
                        mockLicHelper.SOFTWARE_APPLICATION),
                applicationRoleManager, Sets.newHashSet(mockLicHelper.CORE_APP_ROLE,
                        mockLicHelper.PORTFOLIO_APP_ROLE,
                        mockLicHelper.SOFTWARE_APP_ROLE));
        ApplicationUser user = new MockApplicationUser("freddie", DIRECTORY_ID.get());
        crowdService.addUserToGroup(user, mockLicHelper.SOFTWARE_DEFAULT_GROUP);
        crowdService.addUserToGroup(user, mockLicHelper.CORE_DEFAULT_GROUP);

        //when
        final List<ApplicationSelection>
                applicationsForSelection = helper.getApplicationsForUser(user);

        //Then
        List<Matcher<? super ApplicationSelection>> expected = Lists.newArrayList();

        expected.add(matcherForKey(mockLicHelper.PORTFOLIO_APP_KEY)
                .withDisplayNameAndCore(mockLicHelper.PORTFOLIO_TRADEMARK)
                .selectable()
                .notSelected()
                .notIndeterminate());

        expected.add(matcherForKey(mockLicHelper.SOFTWARE_APP_KEY)
                .withDisplayNameAndCore(mockLicHelper.SOFTWARE_TRADEMARK)
                .selectable()
                .selected());

        expected.add(matcherForKey(mockLicHelper.CORE_APP_KEY)
                .withDisplayNameNoCore(mockLicHelper.CORE_TRADEMARK)
                .selectable()
                .selected());

        assertThat(applicationsForSelection, contains(expected));
    }

    @Test
    public void shallReturnErrorForApplicationRoleWithoutSeats() {
        //Given
        mockLicHelper.setup(
                applicationManager, Sets.newHashSet(mockLicHelper.CORE_APPLICATION,
                        mockLicHelper.ZERO_USER_LIMIT_APPLICATION,
                        mockLicHelper.SOFTWARE_APPLICATION),
                applicationRoleManager, Sets.newHashSet(mockLicHelper.CORE_APP_ROLE,
                        mockLicHelper.ZERO_USER_LIMIT_APP_ROLE,
                        mockLicHelper.SOFTWARE_APP_ROLE));

        //when
        final List<ApplicationSelection>
                applicationsForSelection = helper.getApplicationsForSelection(
                of(mockLicHelper.CORE_APP_KEY, mockLicHelper.SOFTWARE_APP_KEY), DIRECTORY_ID);

        //Then
        List<Matcher<? super ApplicationSelection>> expected = Lists.newArrayList();

        expected.add(matcherForKey(mockLicHelper.SOFTWARE_APP_KEY)
                .withDisplayNameAndCore(mockLicHelper.SOFTWARE_TRADEMARK)
                .selectable()
                .selected()
                .notDefined());

        expected.add(matcherForKey(mockLicHelper.ZERO_USER_LIMIT_APP_KEY)
                .withDisplayNameAndCore(mockLicHelper.ZERO_USER_LIMIT_TRADEMARK)
                .withNoSeatsError()
                .notSelectable()
                .notSelected()
                .notDefined());

        expected.add(matcherForKey(mockLicHelper.CORE_APP_KEY)
                .withDisplayNameNoCore(mockLicHelper.CORE_TRADEMARK)
                .selectable()
                .selected()
                .notDefined());

        assertThat(applicationsForSelection, contains(expected));
    }

    @Test
    public void shallReturnErrorForApplicationRoleWithoutDefaultGroup() {
        //Given
        mockLicHelper.setup(
                applicationManager, Sets.newHashSet(mockLicHelper.CORE_APPLICATION,
                        mockLicHelper.NO_DEFAULT_GROUP_APPLICATION,
                        mockLicHelper.SOFTWARE_APPLICATION),
                applicationRoleManager, Sets.newHashSet(mockLicHelper.CORE_APP_ROLE,
                        mockLicHelper.NO_DEFAULT_GROUP_APP_ROLE,
                        mockLicHelper.SOFTWARE_APP_ROLE));

        //when
        final List<ApplicationSelection>
                applicationsForSelection = helper.getApplicationsForSelection(
                of(mockLicHelper.NO_DEFAULT_GROUP_APP_KEY), DIRECTORY_ID);

        //Then
        List<Matcher<? super ApplicationSelection>> expected = Lists.newArrayList();

        expected.add(matcherForKey(mockLicHelper.NO_DEFAULT_GROUP_APP_KEY)
                .withDisplayNameAndCore(mockLicHelper.NO_DEFAULT_GROUP_TRADEMARK)
                .withNoGroupError()
                .notSelectable()
                .selected()
                .notDefined());

        expected.add(matcherForKey(mockLicHelper.SOFTWARE_APP_KEY)
                .withDisplayNameAndCore(mockLicHelper.SOFTWARE_TRADEMARK)
                .selectable()
                .notSelected()
                .notDefined());

        expected.add(matcherForKey(mockLicHelper.CORE_APP_KEY)
                .withDisplayNameNoCore(mockLicHelper.CORE_TRADEMARK)
                .selectable()
                .notSelected()
                .notDefined());

        assertThat(applicationsForSelection, contains(expected));
    }

    @Test
    public void shallReturnSelectedEvenWhenApplicationInError() {
        //Given
        mockLicHelper.setup(
                applicationManager, Sets.newHashSet(mockLicHelper.NO_DEFAULT_GROUP_APPLICATION),
                applicationRoleManager, Sets.newHashSet(mockLicHelper.NO_DEFAULT_GROUP_APP_ROLE));

        //when
        final List<ApplicationSelection>
                applicationsForSelection = helper.getApplicationsForSelection(
                of(mockLicHelper.NO_DEFAULT_GROUP_APP_KEY), DIRECTORY_ID);

        //Then
        List<Matcher<? super ApplicationSelection>> expected = Lists.newArrayList();

        expected.add(matcherForKey(mockLicHelper.NO_DEFAULT_GROUP_APP_KEY)
                .withDisplayNameNoCore(mockLicHelper.NO_DEFAULT_GROUP_TRADEMARK)
                .withNoGroupError()
                .notSelectable()
                .selected()
                .notDefined());

        assertThat(applicationsForSelection, contains(expected));
    }

    @Test
    public void coreInDescriptionWhenCoreLicensed() {
        //Given
        mockLicHelper.setup(
                applicationManager, Sets.newHashSet(mockLicHelper.CORE_APPLICATION, mockLicHelper.SOFTWARE_APPLICATION),
                applicationRoleManager, Sets.newHashSet(mockLicHelper.CORE_APP_ROLE, mockLicHelper.SOFTWARE_APP_ROLE));

        //when
        final List<ApplicationSelection>
                applicationsForSelection = helper.getApplicationsForSelection(
                ImmutableSet.<ApplicationKey>of(), DIRECTORY_ID);

        //Then
        List<Matcher<? super ApplicationSelection>> expected = Lists.newArrayList();

        expected.add(matcherForKey(mockLicHelper.SOFTWARE_APP_KEY)
                .withDisplayNameAndCore(mockLicHelper.SOFTWARE_TRADEMARK)
                .selectable()
                .notSelected());

        expected.add(matcherForKey(mockLicHelper.CORE_APP_KEY)
                .withDisplayNameNoCore(mockLicHelper.CORE_TRADEMARK)
                .selectable()
                .notSelected());

        assertThat(applicationsForSelection, contains(expected));
    }

    @Test
    public void coreNotInDescriptionWhenCoreNotLicensed() {
        //Given
        mockLicHelper.setup(
                applicationManager, Sets.newHashSet(mockLicHelper.SOFTWARE_APPLICATION),
                applicationRoleManager, Sets.newHashSet(mockLicHelper.SOFTWARE_APP_ROLE));

        //when
        final List<ApplicationSelection>
                applicationsForSelection = helper.getApplicationsForSelection(
                ImmutableSet.<ApplicationKey>of(), DIRECTORY_ID);

        //Then
        List<Matcher<? super ApplicationSelection>> expected = Lists.newArrayList();

        expected.add(matcherForKey(mockLicHelper.SOFTWARE_APP_KEY)
                .withDisplayNameNoCore(mockLicHelper.SOFTWARE_TRADEMARK)
                .selectable()
                .notSelected());

        assertThat(applicationsForSelection, contains(expected));
    }

    @Test
    public void shouldGetDefaultGroupsWhenApplicationsAreLicensed() throws CreateException {
        //Given
        final ApplicationKey applicationKey = ApplicationKey.valueOf("com.app.one");
        final ApplicationKey applicationKey2 = ApplicationKey.valueOf("com.app.two");
        final ApplicationKey applicationKey3 = ApplicationKey.valueOf("com.app.three");
        final Group groupOne = groupWithName("groupOne");
        setupDefaultGroupsForApplication(applicationKey, groupOne);
        final Group groupTwo = groupWithName("groupTwo");
        final Group groupThree = groupWithName("groupThree");
        setupDefaultGroupsForApplication(applicationKey2, groupTwo, groupThree);
        final Group notLicensed = groupWithName("notLicensed");
        setupDefaultGroupsForApplication(applicationKey3, notLicensed);
        setupLicenseLimitForApplication(applicationKey, 1);
        setupLicenseLimitForApplication(applicationKey2, 1);

        //When
        final Collection<Group> groupsForCreate = helper.getDefaultGroupsForNewUser(of(applicationKey, applicationKey2));

        //Then
        assertThat(groupsForCreate, containsInAnyOrder(groupWithName("groupOne"), groupWithName("groupTwo"), groupWithName("groupThree")));
    }

    @Test
    public void shouldReturnNoGroupsWhenDefaultGroupsNotSet() throws CreateException {
        //Given
        final ApplicationKey applicationKey = ApplicationKey.valueOf("com.app.one");
        setupLicenseLimitForApplication(applicationKey, 1);
        when(applicationRoleManager.getDefaultGroups(applicationKey)).thenReturn(Collections.<Group>emptySet());

        //When
        final Set<Group> groupsForNewUser = helper.getDefaultGroupsForNewUser(of(applicationKey));
        assertThat(groupsForNewUser, emptyIterable());
    }

    @Test
    public void shouldNotReturnGroupsWhenNoSeatsAvailableInAppLicense() throws CreateException {
        //Given
        final ApplicationKey applicationKey = ApplicationKey.valueOf("com.app.one");
        final ApplicationKey applicationKey2 = ApplicationKey.valueOf("com.app.two");

        setupDefaultGroupsForApplication(applicationKey, groupWithName("groupOne"));
        setupDefaultGroupsForApplication(applicationKey2, groupWithName("groupTwo"));
        setupLicenseLimitForApplication(applicationKey, 1);

        //When
        final Set<Group> groupsForNewUser = helper.getDefaultGroupsForNewUser(of(applicationKey, applicationKey2));
        assertThat(groupsForNewUser, contains(groupWithName("groupOne")));
    }

    @Test
    public void shouldReturnEmptyGroupIfEmptyApplicationKeyProvided() throws CreateException {
        //When
        final Collection<Group> groupsForCreate = helper.getDefaultGroupsForNewUser(ImmutableSet.<ApplicationKey>of());
        assertNotNull(groupsForCreate);
        assertThat(groupsForCreate, emptyIterable());
    }

    @Test
    public void canUserLoginShouldReturnFalseIfUserIsNull() {
        final boolean result = helper.canUserLogin(null);

        assertFalse("User is null so can't login", result);
    }

    @Test
    public void canUserLoginShouldReturnFalseWhenAuthorisationFail() {
        when(authorisationManager.hasUserAccessToJIRA(mockedUser)).thenReturn(false);

        final boolean result = helper.canUserLogin(mockedUser);

        assertFalse("User fail validation so can't login", result);
    }

    @Test
    public void canUserLoginShouldReturnTrueWhenAuthorisationPass() {
        when(authorisationManager.hasUserAccessToJIRA(mockedUser)).thenReturn(true);

        final boolean result = helper.canUserLogin(mockedUser);

        assertTrue("User fail validation pass so can login", result);
    }

    private Group groupWithName(final String groupName) {
        return new MockGroup(groupName);
    }


    private Group crowdGroupWithName(final String groupName) {
        final Group group = groupWithName(groupName);
        crowdService.addGroup(group);
        return group;
    }

    private Set<Group> setupDefaultGroupsForApplication(final ApplicationKey applicationKey, final Group... groups) {
        final Set<Group> groupObjects = Sets.newHashSet();
        for (final Group group : groups) {
            crowdService.addGroup(group);
            groupObjects.add(group);
        }
        when(applicationRoleManager.getDefaultGroups(applicationKey)).thenReturn(groupObjects);
        return groupObjects;
    }

    private void setupLicenseLimitForApplication(ApplicationKey applicationKey, int limitCount) {
        when(applicationRoleManager.getRemainingSeats(applicationKey)).thenReturn(limitCount);
        when(applicationRoleManager.hasSeatsAvailable(applicationKey, 1)).thenReturn(true);
    }

    private ApplicationSelectionMatcher matcherForKey(ApplicationKey key) {
        return new ApplicationSelectionMatcher(key);
    }

    private class ApplicationSelectionMatcher extends TypeSafeDiagnosingMatcher<ApplicationSelection> {
        private static final String STRING_REPRESENTATION = "ApplicationSelection{key='%s', name='%s', displayName='%s', message='%s', messageMarkup='%s', selectable=%s, selected=%s, defined=%s, indeterminate=%s}";
        private final ApplicationKey key;
        private String name;
        private String displayName;
        private String message;
        private String messageMarkup;
        private boolean defined;
        private boolean selectable;
        private boolean selected;
        private boolean indeterminate;

        private ApplicationSelectionMatcher(final ApplicationKey key) {
            this.key = key;
        }

        private ApplicationSelectionMatcher withDisplayNameAndCore(String name) {
            this.name = name;
            this.displayName = makeTranslation("admin.adduser.application.selection.name.includes.core", name,
                    mockLicHelper.CORE_TRADEMARK);

            return this;
        }

        private ApplicationSelectionMatcher withDisplayNameNoCore(String name) {
            this.name = name;
            this.displayName = makeTranslation("admin.adduser.application.selection.name.without.core", name);
            return this;
        }

        private ApplicationSelectionMatcher withNoGroupError() {
            final String key = "admin.errors.user.add.user.application.no.default.group";
            final String start = String.format(LINK_START, editRoleLink());
            message = makeTranslation(key, this.name, "", "");
            messageMarkup = makeTranslation(key, this.name, start, LINK_END);
            return this;
        }

        private ApplicationSelectionMatcher noWritableDirectory() {
            final String i18ntext = makeTranslation("admin.errors.directory.fully.read.only", directory.getName());
            message = i18ntext;
            messageMarkup = i18ntext;
            return this;
        }

        private ApplicationSelectionMatcher withNoSeatsError() {
            final String key = "admin.errors.user.add.user.application.license.limit.reached";
            final String startUser = String.format(LINK_START, userBrowserLink());
            final String startVLlink = String.format(LINK_START, versionsAndLicensesLink());
            message = makeTranslation(key, this.name, "", "", "", "");
            messageMarkup = makeTranslation(key, this.name, startUser, LINK_END, startVLlink, LINK_END);
            return this;
        }

        private ApplicationSelectionMatcher getApplicationAccessLink(String key) {
            final String start = String.format("<a href=\"%s\">", editRoleLink());
            message = makeTranslation(key, this.name, "", "");
            messageMarkup = makeTranslation(key, this.name, start, "</a>");
            return this;
        }

        private ApplicationSelectionMatcher selectable() {
            this.selectable = true;
            return this;
        }

        private ApplicationSelectionMatcher notSelectable() {
            this.selectable = false;
            return this;
        }

        private ApplicationSelectionMatcher selected() {
            this.selected = true;
            return this;
        }

        private ApplicationSelectionMatcher notSelected() {
            this.selected = false;
            return this;
        }

        private ApplicationSelectionMatcher defined() {
            this.defined = true;
            return this;
        }

        private ApplicationSelectionMatcher notDefined() {
            this.defined = false;
            return this;
        }

        private ApplicationSelectionMatcher indeterminate() {
            this.indeterminate = true;
            return this;
        }

        private ApplicationSelectionMatcher notIndeterminate() {
            this.indeterminate = false;
            return this;
        }

        private String editRoleLink() {
            return baseUrl.getBaseUrl() + "/secure/admin/ApplicationAccess.jspa";
        }

        private String userBrowserLink() {
            return baseUrl.getBaseUrl() + "/secure/admin/user/UserBrowser.jspa";
        }

        private String versionsAndLicensesLink() {
            return baseUrl.getBaseUrl() + "/plugins/servlet/applications/versions-licenses";
        }

        @Override
        protected boolean matchesSafely(final ApplicationSelection item, final Description mismatchDescription) {
            boolean keysEqual = Objects.equals(this.key.value(), item.getKey());
            boolean nameEqual = Objects.equals(this.name, item.getName());
            boolean displayEqual = Objects.equals(this.displayName, item.getDisplayName());
            boolean messageEqual = Objects.equals(this.message, item.getMessage());
            boolean messageMarkupEqual = Objects.equals(this.messageMarkup, item.getMessageMarkup());
            boolean selectableEqual = this.selectable == item.isSelectable();
            boolean selectedEqual = this.selected == item.isSelected();
            boolean definedEqual = this.defined == item.isDefined();
            boolean indeterminateEqual = this.indeterminate == item.isIndeterminate();

            if (keysEqual && nameEqual && displayEqual && messageEqual && messageMarkupEqual && selectableEqual && selectedEqual && definedEqual && indeterminateEqual) {
                return true;
            } else {
                mismatchDescription.appendText(String.format(STRING_REPRESENTATION, item.getKey(), item.getName(),
                        item.getDisplayName(), item.getMessage(), item.getMessageMarkup(),
                        item.isSelectable(), item.isSelected(), item.isDefined(), item.isIndeterminate()));
                return false;
            }
        }

        @Override
        public void describeTo(final Description description) {
            description.appendText(
                    String.format(STRING_REPRESENTATION, key, name, displayName, message, messageMarkup, selectable, selected, defined, indeterminate));
        }
    }
}
