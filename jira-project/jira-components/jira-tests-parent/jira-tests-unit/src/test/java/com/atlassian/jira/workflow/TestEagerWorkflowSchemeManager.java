package com.atlassian.jira.workflow;

import com.atlassian.beehive.ClusterLockService;
import com.atlassian.beehive.simple.SimpleClusterLockService;
import com.atlassian.cache.Cache;
import com.atlassian.cache.CacheManager;
import com.atlassian.cache.memory.MemoryCacheManager;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.association.NodeAssociationStore;
import com.atlassian.jira.database.MockQueryDslAccessor;
import com.atlassian.jira.database.ResultRow;
import com.atlassian.jira.event.workflow.WorkflowSchemeCreatedEvent;
import com.atlassian.jira.event.workflow.WorkflowSchemeDeletedEvent;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoContainer;
import com.atlassian.jira.mock.i18n.MockI18nHelper;
import com.atlassian.jira.mock.ofbiz.MockGenericValue;
import com.atlassian.jira.mock.ofbiz.MockOfBizDelegator;
import com.atlassian.jira.mock.security.MockSimpleAuthenticationContext;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.scheme.DefaultSchemeFactory;
import com.atlassian.jira.scheme.SchemeFactory;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.task.MockTaskDescriptor;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.NoopI18nFactory;
import com.atlassian.jira.util.collect.MapBuilder;
import com.atlassian.jira.web.action.admin.workflow.WorkflowMigrationResult;
import com.atlassian.jira.workflow.migration.WorkflowSchemeMigrationTaskAccessor;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.ofbiz.core.entity.GenericEntityException;
import org.ofbiz.core.entity.GenericValue;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import static com.google.common.collect.Iterables.find;
import static com.google.common.collect.Iterables.size;
import static com.google.common.collect.Iterables.transform;
import static java.lang.String.format;
import static java.util.Arrays.asList;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.RETURNS_MOCKS;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.only;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

/**
 * Test the default workflow scheme manager
 */
@SuppressWarnings({"deprecation", "ConstantConditions"})
public class TestEagerWorkflowSchemeManager {
    private MockSimpleAuthenticationContext context;
    private AssignableWorkflowScheme defaultScheme;

    @Mock
    private UserManager userManager;

    @Mock
    protected EventPublisher mockEventPublisher;

    @Mock
    private Cache<Long, Map<String, GenericValue>> workflowSchemeEntityCache;

    @Mock
    @AvailableInContainer
    protected OfBizDelegator ofBizDelegator;

    private ApplicationUser user;

    private I18nHelper.BeanFactory i18nFactory;
    private ClusterLockService clusterLockService;
    private MockQueryDslAccessor mockQueryDslAccessor;
    private CacheManager cacheManager;
    @Mock
    private NodeAssociationStore mockNodeAssociationStore;
    @Mock
    private ProjectManager mockProjectManager;

    @Rule
    public MockitoContainer mockitoContainer = new MockitoContainer(this);

    @Before
    public void setup() {
        clusterLockService = new SimpleClusterLockService();
        context = new MockSimpleAuthenticationContext(new MockApplicationUser("bbain"), Locale.ENGLISH, new MockI18nHelper());
        defaultScheme = new DefaultWorkflowScheme(context);
        when(userManager.getUserByKey(Mockito.notNull(String.class))).thenAnswer(invocationOnMock ->
                new MockApplicationUser((String) invocationOnMock.getArguments()[0]));
        mockQueryDslAccessor = new MockQueryDslAccessor();
        cacheManager = new MemoryCacheManager();

        user = new MockApplicationUser("admin");

        i18nFactory = new NoopI18nFactory();
    }

    @Test
    public void getAllAssignable() {
        MockAssignableWorkflowSchemeStore schemeStore = new MockAssignableWorkflowSchemeStore();

        MockAssignableWorkflowScheme scheme1 = new MockAssignableWorkflowScheme(10191L, "ZZZ");
        MockAssignableWorkflowScheme scheme3 = new MockAssignableWorkflowScheme(10192L, "aaa");
        MockAssignableWorkflowScheme scheme4 = new MockAssignableWorkflowScheme(10193L, "BBB");

        schemeStore.addStateForScheme(scheme1);
        schemeStore.addStateForScheme(scheme3);
        schemeStore.addStateForScheme(scheme4);

        final WorkflowSchemeManager defaultWorkflowSchemeManager = new EagerWorkflowSchemeManager(
                null, null, null, null, null, null, null, mockEventPublisher, null, null, null, context, userManager,
                i18nFactory, schemeStore, cacheManager, clusterLockService, mockQueryDslAccessor);

        assertThat(transform(defaultWorkflowSchemeManager.getAssignableSchemes(), MockAssignableWorkflowScheme.toMock()),
                containsInAnyOrder(scheme1, scheme3, scheme4));
    }

    @Test
    public void testHasDraft() {
        final MockDraftWorkflowSchemeStore mockDraftWorkflowSchemeStore = new MockDraftWorkflowSchemeStore();

        final AssignableWorkflowScheme parentScheme = new MockAssignableWorkflowScheme(10001L, "name", "description");
        final DraftWorkflowScheme draftWorkflowScheme = new MockDraftWorkflowScheme(18291L, parentScheme);

        mockDraftWorkflowSchemeStore.addStateForScheme(draftWorkflowScheme);

        final AssignableWorkflowScheme noDraft = new MockAssignableWorkflowScheme(10002L, "name", "description");

        final AssignableWorkflowScheme noId = new MockAssignableWorkflowScheme(null, "name", "description");

        final WorkflowSchemeManager defaultWorkflowSchemeManager = new EagerWorkflowSchemeManager(
                null, null, null, null, null, null, null, null, null, null, mockDraftWorkflowSchemeStore, context,
                userManager, i18nFactory, null, cacheManager, clusterLockService, mockQueryDslAccessor);
        assertThat(defaultWorkflowSchemeManager.hasDraft(parentScheme), is(true));
        assertThat(defaultWorkflowSchemeManager.hasDraft(noDraft), is(false));
        assertThat(defaultWorkflowSchemeManager.hasDraft(defaultScheme), is(false));
        assertThat(defaultWorkflowSchemeManager.hasDraft(noId), is(false));
    }

    @Test
    public void testCreateDraftOfBad() {
        final MockDraftWorkflowSchemeStore mockDraftWorkflowSchemeStore = new MockDraftWorkflowSchemeStore();

        final AssignableWorkflowScheme parentScheme = new MockAssignableWorkflowScheme(10001L, "name", "description");
        final DraftWorkflowScheme draftWorkflowScheme = new MockDraftWorkflowScheme(18291L, parentScheme);

        mockDraftWorkflowSchemeStore.addStateForScheme(draftWorkflowScheme);

        final AssignableWorkflowScheme noId = new MockAssignableWorkflowScheme(null, "name", "description");

        final WorkflowSchemeManager defaultWorkflowSchemeManager = new EagerWorkflowSchemeManager(
                null, null, null, null, null, null, null, mockEventPublisher, null, null, mockDraftWorkflowSchemeStore,
                context, userManager, i18nFactory, null, cacheManager, clusterLockService, mockQueryDslAccessor);

        //No scheme.
        try {
            defaultWorkflowSchemeManager.createDraftOf(null, null);
            fail("Expected Exception");
        } catch (IllegalArgumentException e) {
            //expected.
        }

        //No scheme
        try {
            defaultWorkflowSchemeManager.createDraftOf(null, noId);
            fail("Expected an error.");
        } catch (IllegalArgumentException e) {
            //expected.
        }

        //Already has draft
        try {
            defaultWorkflowSchemeManager.createDraftOf(null, parentScheme);
            fail("Expected an error.");
        } catch (IllegalArgumentException e) {
            //expected.
        }

        //default scheme
        try {
            defaultWorkflowSchemeManager.createDraftOf(null, defaultScheme);
            fail("Expected an error.");
        } catch (IllegalArgumentException e) {
            //expected.
        }
    }

    @Test
    public void testCreateDraftOf() {
        checkCreateDraftOfForUser(user);
        checkCreateDraftOfForUser(null);
    }

    private void checkCreateDraftOfForUser(ApplicationUser creator) {
        final MockDraftWorkflowSchemeStore store = new MockDraftWorkflowSchemeStore();

        final MockAssignableWorkflowScheme newScheme = new MockAssignableWorkflowScheme(10001L, "name", "description");
        newScheme.setMapping("one", "two");

        final DSMForTest defaultWorkflowSchemeManager = new DSMForTest(store, context, userManager, i18nFactory, null);
        defaultWorkflowSchemeManager.addScheme(newScheme);

        final DraftWorkflowScheme draft = defaultWorkflowSchemeManager.createDraftOf(creator, newScheme);

        checkWorkflowScheme(newScheme, creator, store.getLastDate(), store.getLastId(), draft);
        checkStoreContainsScheme(store, draft, creator);
    }

    @Test
    public void testCreateDraftBad() {
        final MockDraftWorkflowSchemeStore mockDraftWorkflowSchemeStore = new MockDraftWorkflowSchemeStore();

        final AssignableWorkflowScheme parentScheme = new MockAssignableWorkflowScheme(10001L, "name", "description");
        final DraftWorkflowScheme draftWorkflowScheme = new MockDraftWorkflowScheme(18291L, parentScheme);

        mockDraftWorkflowSchemeStore.addStateForScheme(draftWorkflowScheme);

        final AssignableWorkflowScheme noId = new MockAssignableWorkflowScheme(null, "name", "description");
        final DraftWorkflowScheme draftNoId = new MockDraftWorkflowScheme(null, noId);

        final DraftWorkflowScheme noParent = new MockDraftWorkflowScheme(null, null);

        final WorkflowSchemeManager defaultWorkflowSchemeManager = new EagerWorkflowSchemeManager(
                null, null, null, null, null, null, null, mockEventPublisher, null, null, mockDraftWorkflowSchemeStore,
                context, userManager, i18nFactory, null, cacheManager, clusterLockService, mockQueryDslAccessor);

        //No scheme.
        try {
            defaultWorkflowSchemeManager.createDraft(null, null);
            fail("Expected Exception");
        } catch (IllegalArgumentException e) {
            //expected.
        }

        //No scheme
        try {
            defaultWorkflowSchemeManager.createDraft(null, draftNoId);
            fail("Expected an error.");
        } catch (IllegalArgumentException e) {
            //expected.
        }

        //Already has draft
        try {
            defaultWorkflowSchemeManager.createDraft(null, draftWorkflowScheme);
            fail("Expected an error.");
        } catch (IllegalArgumentException e) {
            //expected.
        }

        //Draft does not have a parent.
        try {
            defaultWorkflowSchemeManager.createDraft(null, noParent);
            fail("Expected an error.");
        } catch (IllegalArgumentException e) {
            //expected.
        }
    }

    @Test
    public void testCreateDraft() {
        checkCreateDraft(user);
        checkCreateDraft(null);
    }

    private void checkCreateDraft(ApplicationUser user) {
        final MockDraftWorkflowSchemeStore store = new MockDraftWorkflowSchemeStore();

        final MockAssignableWorkflowScheme newScheme = new MockAssignableWorkflowScheme(10001L, "name", "description");
        newScheme.setMapping("one", "two");

        final MockDraftWorkflowScheme newDraftScheme = new MockDraftWorkflowScheme(null, newScheme);
        newDraftScheme.setMapping("three", "four");

        final DSMForTest defaultWorkflowSchemeManager = new DSMForTest(store, context, userManager, i18nFactory, null);
        defaultWorkflowSchemeManager.addScheme(newScheme);

        final DraftWorkflowScheme draft = defaultWorkflowSchemeManager.createDraft(user, newDraftScheme);

        checkWorkflowScheme(newDraftScheme, user, store.getLastDate(), store.getLastId(), draft);
        checkStoreContainsScheme(store, draft, user);
    }

    private void checkWorkflowScheme(WorkflowScheme expectedScheme, ApplicationUser expectedCreator, Date expectedDate,
                                     long expectedId, DraftWorkflowScheme actualDraft) {
        assertEquals((Long) expectedId, actualDraft.getId());
        assertEquals(expectedScheme.getName(), actualDraft.getName());
        assertEquals(expectedScheme.getDescription(), actualDraft.getDescription());
        assertEquals(expectedScheme.getMappings(), actualDraft.getMappings());
        assertEquals(expectedCreator, actualDraft.getLastModifiedUser());
        assertEquals(expectedDate, actualDraft.getLastModifiedDate());
    }

    private void checkStoreContainsScheme(MockDraftWorkflowSchemeStore store, DraftWorkflowScheme draft, ApplicationUser creator) {
        final DraftWorkflowSchemeStore.DraftState state = store.get(draft.getId());
        assertEquals(draft.getId(), state.getId());
        assertEquals(draft.getMappings(), state.getMappings());
        assertEquals(creator == null ? null : creator.getKey(), state.getLastModifiedUser());
        assertEquals(state.getLastModifiedDate(), store.getLastDate());
    }

    @Test
    public void testDraftWorkflowSchemeBuilder() {
        final AssignableWorkflowScheme parentScheme = new MockAssignableWorkflowScheme(10001L, "name", "description");

        final WorkflowSchemeManager defaultWorkflowSchemeManager = new EagerWorkflowSchemeManager(
                null, null, null, null, null, null, null, mockEventPublisher, null, null, null, context, userManager,
                i18nFactory, null, cacheManager, clusterLockService, mockQueryDslAccessor);
        try {
            defaultWorkflowSchemeManager.draftBuilder(null);
            fail("Should not accept null parent.");
        } catch (IllegalArgumentException e) {
            //expected
        }
        try {
            defaultWorkflowSchemeManager.draftBuilder(new MockAssignableWorkflowScheme(null, "Something"));
            fail("Should not accept workflow scheme with null id.");
        } catch (IllegalArgumentException e) {
            //expected.
        }

        DraftWorkflowScheme.Builder builder = defaultWorkflowSchemeManager.draftBuilder(parentScheme);
        assertThat(builder, notNullValue());

        DraftWorkflowScheme childScheme = builder.build();
        assertSame(parentScheme, childScheme.getParentScheme());
        assertThat(childScheme.getLastModifiedDate(), notNullValue());
    }

    @Test
    public void testGetWorkflowScheme() {
        final WorkflowSchemeManager defaultWorkflowSchemeManager = new EagerWorkflowSchemeManager(
                null, null, null, null, null, null, null, mockEventPublisher, null, null, null, context, userManager,
                i18nFactory, null, cacheManager, clusterLockService, mockQueryDslAccessor) {
            @Override
            Map<String, String> getWorkflowMap(Long schemeId) {
                return ImmutableMap.of("bud", "workflows " + schemeId);
            }

            @Override
            public GenericValue getScheme(Long id) {
                return new MockGenericValue("something",
                        ImmutableMap.of("name", "name of " + id,
                                "description", "description of " + id,
                                "id", id));
            }
        };


        final WorkflowScheme workflowScheme = defaultWorkflowSchemeManager.getWorkflowSchemeObj(1277L);
        assertEquals(Long.valueOf(1277), workflowScheme.getId());
        assertEquals("name of 1277", workflowScheme.getName());
        assertEquals("description of 1277", workflowScheme.getDescription());
        assertEquals(ImmutableMap.of("bud", "workflows 1277"), workflowScheme.getMappings());
    }

    @Test
    public void testGetWorkflowSchemeObj() {
        final WorkflowSchemeManager defaultWorkflowSchemeManager = new EagerWorkflowSchemeManager(
                null, null, null, null, null, null, null, mockEventPublisher, null, null, null, context, userManager,
                i18nFactory, null, cacheManager, clusterLockService, mockQueryDslAccessor) {
            @Override
            Map<String, String> getWorkflowMap(Long schemeId) {
                return ImmutableMap.of("bud", "workflows " + schemeId);
            }

            @Override
            GenericValue getSchemeForProject(Project project) {
                return new MockGenericValue("something",
                        ImmutableMap.of("name", "name of " + project.getId(),
                                "description", "description of " + project.getId(),
                                "id", project.getId()));
            }
        };

        final MockProject mockProject = new MockProject(1277L);
        final WorkflowScheme workflowScheme = defaultWorkflowSchemeManager.getWorkflowSchemeObj(mockProject);

        assertEquals(Long.valueOf(1277), workflowScheme.getId());
        assertEquals("name of 1277", workflowScheme.getName());
        assertEquals("description of 1277", workflowScheme.getDescription());
        assertEquals(ImmutableMap.of("bud", "workflows 1277"), workflowScheme.getMappings());
    }

    @Test
    public void testGetWorkflowSchemeObjDefaultScheme() {
        final WorkflowSchemeManager defaultWorkflowSchemeManager = new EagerWorkflowSchemeManager(
                null, null, null, null, null, null, null, mockEventPublisher, null, null, null, context, userManager,
                i18nFactory, null, cacheManager, clusterLockService, mockQueryDslAccessor) {
            @Override
            Map<String, String> getWorkflowMap(Long schemeId) {
                return ImmutableMap.of("bud", "workflows " + schemeId);
            }

            @Override
            GenericValue getSchemeForProject(Project project) {
                return null;
            }
        };

        final MockProject mockProject = new MockProject(1277L);
        final WorkflowScheme workflowScheme = defaultWorkflowSchemeManager.getWorkflowSchemeObj(mockProject);

        assertThat(workflowScheme.getId(), nullValue());
        assertEquals("admin.schemes.workflows.default", workflowScheme.getName());
        assertEquals("admin.schemes.workflows.default.desc", workflowScheme.getDescription());
        assertEquals(MapBuilder.build((String) null, JiraWorkflow.DEFAULT_WORKFLOW_NAME), workflowScheme.getMappings());
    }

    @Test(expected = IllegalArgumentException.class)
    public void getDraftForParentBad() {
        final WorkflowSchemeManager defaultWorkflowSchemeManager = new EagerWorkflowSchemeManager(
                null, null, null, null, null, null, null, mockEventPublisher, null, null, null, context, userManager,
                i18nFactory, null, cacheManager, clusterLockService, mockQueryDslAccessor);

        //Null scheme
        defaultWorkflowSchemeManager.getDraftForParent(null);
    }

    @Test
    public void getDraftForParent() {
        final AssignableWorkflowScheme parent = new MockAssignableWorkflowScheme(1028291L, "name", "description");
        final AssignableWorkflowScheme noDraft = new MockAssignableWorkflowScheme(10002L, "name", "description");
        final AssignableWorkflowScheme noId = new MockAssignableWorkflowScheme(null, "name", "description");
        final DraftWorkflowScheme draftWorkflowScheme = new MockDraftWorkflowScheme(18291L, parent);

        final MockDraftWorkflowSchemeStore store = new MockDraftWorkflowSchemeStore();
        store.addStateForScheme(draftWorkflowScheme);

        final DSMForTest schemeManager = new DSMForTest(store, context, userManager, i18nFactory, null);
        schemeManager.addScheme(parent);

        final WorkflowScheme actualDraft = schemeManager.getDraftForParent(parent);
        assertEquals(draftWorkflowScheme.getName(), actualDraft.getName());
        assertEquals(draftWorkflowScheme.getDescription(), actualDraft.getDescription());
        assertEquals(draftWorkflowScheme.getId(), actualDraft.getId());
        assertEquals(draftWorkflowScheme.getMappings(), actualDraft.getMappings());

        assertThat(schemeManager.getDraftForParent(noDraft), nullValue());
        assertThat(schemeManager.getDraftForParent(noId), nullValue());
        assertThat(schemeManager.getDraftForParent(defaultScheme), nullValue());
    }

    @Test
    public void isActive() {
        final WorkflowScheme active = new MockAssignableWorkflowScheme(1028291L, "active", "description");
        final WorkflowScheme noId = new MockAssignableWorkflowScheme(null, "noid", "description");
        final WorkflowScheme inactive = new MockAssignableWorkflowScheme(10002L, "inactive", "description");

        final AssignableWorkflowScheme parentScheme = new MockAssignableWorkflowScheme(10001L, "parent", "description");
        final DraftWorkflowScheme draftWorkflowScheme = new MockDraftWorkflowScheme(18291L, parentScheme);

        final Project project = createProject(100002L, "BJB");
        setBulkGetQueryResult(ImmutableList.of(new ResultRow(100002L, 1028291L, 1000L, 1028291L, "one","0"),
                new ResultRow(null, 10002L, 1001L, 10002L, "two","two"),
                new ResultRow(null, 10001L, 10001L, 9000L, "three","three")));

        when(mockProjectManager.getProjects()).thenReturn(ImmutableList.of(project));

        final WorkflowSchemeManager schemeManager = new EagerWorkflowSchemeManager(mockProjectManager, null, null,
                new DefaultSchemeFactory(), null, null, new MockOfBizDelegator(), mockEventPublisher, null, null, null,
                context, userManager, i18nFactory, null, cacheManager, clusterLockService, mockQueryDslAccessor);

        assertThat(schemeManager.isActive(noId), is(false));
        assertThat(schemeManager.isActive(draftWorkflowScheme), is(false));
        assertThat(schemeManager.isActive(active), is(true));
        assertThat(schemeManager.isActive(inactive), is(false));
    }

    @Test
    public void isActiveDefaultScheme() {
        final Project project1 = new MockProject(1L);
        final Project project2 = new MockProject(2L);

        when(mockProjectManager.getProjects()).thenReturn(ImmutableList.of(project1));
        setBulkGetQueryResult(ImmutableList.of(new ResultRow(1L, 9000L, 1000L, 9000L, "test", "0")));

        final WorkflowSchemeManager schemeManager = new EagerWorkflowSchemeManager(
                mockProjectManager, null, null, null, null, null, null, mockEventPublisher, null, null, null, context,
                userManager, i18nFactory, null, cacheManager, clusterLockService, mockQueryDslAccessor);

        assertThat(schemeManager.isActive(defaultScheme), is(false));
        when(mockProjectManager.getProjects()).thenReturn(Lists.newArrayList(project1, project2));
        schemeManager.clearWorkflowCache();
        assertThat(schemeManager.isActive(defaultScheme), is(true));
    }

    @Test
    public void deleteScheme() {
        // Set up
        final NodeAssociationStore nodeAssociationStore = Mockito.mock(NodeAssociationStore.class);
        final DraftWorkflowSchemeStore draftStore = Mockito.mock(DraftWorkflowSchemeStore.class);
        final OfBizDelegator delegator = Mockito.mock(OfBizDelegator.class);
        final WorkflowSchemeMigrationTaskAccessor taskAccessor = mock(WorkflowSchemeMigrationTaskAccessor.class);

        setBulkGetQueryResult(Collections.emptyList());
        when(mockNodeAssociationStore.getSinksFromSource(anyString(), anyLong(), anyString(), anyString()))
                .thenReturn(Collections.emptyList());
        when(mockProjectManager.getProjects()).thenReturn(Collections.emptyList());

        final EagerWorkflowSchemeManager schemeManager = new EagerWorkflowSchemeManager(mockProjectManager, null, null, null,
                null, null, delegator, mockEventPublisher, nodeAssociationStore, null, draftStore, context, userManager,
                i18nFactory, null, cacheManager, clusterLockService, mockQueryDslAccessor) {
            @Override
            public GenericValue getScheme(Long id) {
                return new MockGenericValue("Scheme", ImmutableMap.of("id", id));
            }

            @Override
            WorkflowSchemeMigrationTaskAccessor getTaskAccessor() {
                return taskAccessor;
            }
        };
        schemeManager.start();

        // Invoke
        schemeManager.deleteScheme(10L);
        schemeManager.deleteScheme(null);

        // Check
        verify(draftStore, only()).deleteByParentId(10);

        //Did we fire the correct event?
        verify(mockEventPublisher).register(schemeManager);
        final ArgumentCaptor<WorkflowSchemeDeletedEvent> eventCaptor =
                ArgumentCaptor.forClass(WorkflowSchemeDeletedEvent.class);
        verify(mockEventPublisher).publish(eventCaptor.capture());
        verifyNoMoreInteractions(mockEventPublisher);
        assertEquals(Long.valueOf(10), eventCaptor.getValue().getId());
        mockQueryDslAccessor.assertAllExpectedStatementsWereRun();
    }

    @Test
    public void getDeleteWorkflowSchemeBad() {
        final WorkflowScheme noId = new MockAssignableWorkflowScheme(null, "name", "description");

        final WorkflowScheme activeScheme = new MockAssignableWorkflowScheme(10101L, "name", "description");

        WorkflowSchemeManager defaultWorkflowSchemeManager = new EagerWorkflowSchemeManager(
                null, null, null, null, null, null, null, mockEventPublisher, null, null, null, context, userManager,
                i18nFactory, null, cacheManager, clusterLockService, mockQueryDslAccessor) {
            @Override
            public boolean isActive(@Nonnull WorkflowScheme scheme) {
                assertSame(scheme, activeScheme);
                return true;
            }
        };

        defaultWorkflowSchemeManager = Mockito.spy(defaultWorkflowSchemeManager);

        //Null scheme
        try {
            defaultWorkflowSchemeManager.deleteWorkflowScheme(null);
            fail("Expected an error.");
        } catch (IllegalArgumentException e) {
            //expected.
        }

        //No scheme
        try {
            defaultWorkflowSchemeManager.deleteWorkflowScheme(noId);
            fail("Expected an error.");
        } catch (IllegalArgumentException e) {
            //expected.
        }

        //No scheme
        try {
            defaultWorkflowSchemeManager.deleteWorkflowScheme(defaultScheme);
            fail("Expected an error.");
        } catch (IllegalArgumentException e) {
            //expected.
        }

        //Active scheme
        try {
            defaultWorkflowSchemeManager.deleteWorkflowScheme(activeScheme);
            fail("Expected an error.");
        } catch (IllegalArgumentException e) {
            //expected.
        }
    }

    @Test
    public void deleteWorkflowScheme() throws GenericEntityException {
        final AssignableWorkflowScheme parent = new MockAssignableWorkflowScheme(1028291L, "name", "description");
        final AssignableWorkflowScheme noDraft = new MockAssignableWorkflowScheme(10002L, "name", "description");

        final AssignableWorkflowScheme parentScheme = new MockAssignableWorkflowScheme(10001L, "name", "description");
        final DraftWorkflowScheme draftWorkflowScheme = new MockDraftWorkflowScheme(18291L, parentScheme);

        final DraftWorkflowSchemeStore store = Mockito.mock(DraftWorkflowSchemeStore.class);
        final WorkflowSchemeMigrationTaskAccessor taskAccessor = mock(WorkflowSchemeMigrationTaskAccessor.class);

        EagerWorkflowSchemeManager schemeManager = new EagerWorkflowSchemeManager(
                null, null, null, null, null, null, null, mockEventPublisher, null, null, store, context, userManager,
                i18nFactory, null, cacheManager, clusterLockService, mockQueryDslAccessor) {
            @Override
            WorkflowSchemeMigrationTaskAccessor getTaskAccessor() {
                return taskAccessor;
            }
        };
        schemeManager = Mockito.spy(schemeManager);
        doNothing().when(schemeManager).doDeleteScheme(Mockito.<AssignableWorkflowScheme>any());
        doReturn(false).when(schemeManager).isActive(Mockito.<WorkflowScheme>any());

        assertThat(schemeManager.deleteWorkflowScheme(parent), is(true));
        verify(schemeManager).doDeleteScheme(parent);

        assertThat(schemeManager.deleteWorkflowScheme(noDraft), is(true));
        verify(schemeManager).doDeleteScheme(noDraft);

        assertThat(schemeManager.deleteWorkflowScheme(draftWorkflowScheme), is(false));
        verify(store).delete(draftWorkflowScheme.getId());
    }

    @Test
    public void updateDraftWorkflowScheme() throws GenericEntityException {
        final AssignableWorkflowScheme parent = new MockAssignableWorkflowScheme(1028291L, "name", "description");
        final DraftWorkflowScheme draft = new MockDraftWorkflowScheme(10002L, parent).setLastModifiedDate(new Date());

        MockDraftWorkflowSchemeStore store = new MockDraftWorkflowSchemeStore();
        store.addStateForScheme(draft);

        final WorkflowSchemeMigrationTaskAccessor taskAccessor = mock(WorkflowSchemeMigrationTaskAccessor.class);
        DSMForTest schemeManager = new DSMForTest(store, context, userManager, i18nFactory, null) {
            @Override
            WorkflowSchemeMigrationTaskAccessor getTaskAccessor() {
                return taskAccessor;
            }
        };
        schemeManager.addScheme(parent);

        DraftWorkflowScheme.Builder builder = draft.builder()
                .setMappings(Collections.singletonMap("one", "two"));

        DraftWorkflowScheme expectedScheme = builder.build();
        DraftWorkflowScheme newDraft = schemeManager.updateDraftWorkflowScheme(null, expectedScheme);
        checkStoreContainsScheme(store, expectedScheme, null);
        checkWorkflowScheme(expectedScheme, null, store.getLastDate(), draft.getId(), newDraft);
    }

    @Test
    public void updateDraftWorkflowSchemeBad() {
        final DraftWorkflowScheme noId = new MockDraftWorkflowScheme();
        MockAssignableWorkflowScheme parent = new MockAssignableWorkflowScheme(1028291L, "name", "description");
        final DraftWorkflowScheme doesntExist = new MockDraftWorkflowScheme(7383L, parent);
        final DraftWorkflowScheme migrating = new MockDraftWorkflowScheme(10002L, parent);
        final DraftWorkflowSchemeStore store = Mockito.mock(DraftWorkflowSchemeStore.class);

        final WorkflowSchemeMigrationTaskAccessor taskAccessor = mock(WorkflowSchemeMigrationTaskAccessor.class);

        WorkflowSchemeManager schemeManager = new EagerWorkflowSchemeManager(
                null, null, null, null, null, null, null, mockEventPublisher, null, null, store, context, userManager,
                i18nFactory, null, cacheManager, clusterLockService, mockQueryDslAccessor) {
            @Override
            WorkflowSchemeMigrationTaskAccessor getTaskAccessor() {
                return taskAccessor;
            }
        };

        try {
            schemeManager.updateDraftWorkflowScheme(null, null);
            fail("Expected an exception.");
        } catch (IllegalArgumentException expected) {
            // Success
        }

        try {
            schemeManager.updateDraftWorkflowScheme(null, noId);
            fail("Expected an exception.");
        } catch (IllegalArgumentException expected) {
            // Success
        }

        try {
            schemeManager.updateDraftWorkflowScheme(null, doesntExist);
            fail("Expected an exception.");
        } catch (IllegalArgumentException expected) {
            // Success
        }

        when(taskAccessor.getActiveByProjects(migrating, true)).thenReturn(new MockTaskDescriptor<WorkflowMigrationResult>());
        try {
            schemeManager.updateDraftWorkflowScheme(null, migrating);
            fail("Expected an exception.");
        } catch (SchemeIsBeingMigratedException expected) {
            // Success
        }
    }

    @Test
    public void updateWorkflowSchemeBad() {
        final AssignableWorkflowScheme noId = new MockAssignableWorkflowScheme();
        final AssignableWorkflowScheme migrating = new MockAssignableWorkflowScheme(10002L, "name");

        final WorkflowSchemeMigrationTaskAccessor taskAccessor = mock(WorkflowSchemeMigrationTaskAccessor.class);

        WorkflowSchemeManager schemeManager = new EagerWorkflowSchemeManager(
                null, null, null, null, null, null, null, mockEventPublisher, null, null, null, context, userManager,
                i18nFactory, null, cacheManager, clusterLockService, mockQueryDslAccessor) {
            @Override
            WorkflowSchemeMigrationTaskAccessor getTaskAccessor() {
                return taskAccessor;
            }
        };

        try {
            schemeManager.updateWorkflowScheme(null);
            fail("Expected an exception.");
        } catch (IllegalArgumentException expected) {
            // Success
        }

        try {
            schemeManager.updateWorkflowScheme(noId);
            fail("Expected an exception.");
        } catch (IllegalArgumentException expected) {
            // Success
        }

        when(taskAccessor.getActive(migrating)).thenReturn(new MockTaskDescriptor<WorkflowMigrationResult>());
        try {
            schemeManager.updateWorkflowScheme(migrating);
            fail("Expected an exception.");
        } catch (SchemeIsBeingMigratedException expected) {
            // Success
        }
    }

    @Test
    public void getProjectsUsing() {
        final AssignableWorkflowScheme parent = new MockAssignableWorkflowScheme(11L, "name", "description");

        final MockProject project1 = new MockProject(10, "ABC");
        final MockProject project2 = new MockProject(parent.getId(), "DEF");
        final MockProject project3 = new MockProject(12, "JKL");

        final DraftWorkflowSchemeStore store = Mockito.mock(DraftWorkflowSchemeStore.class);

        when(mockProjectManager.getProjectObjects()).thenReturn(ImmutableList.<Project>of(project1, project2, project3));

        final WorkflowSchemeManager schemeManager = new EagerWorkflowSchemeManager(
                mockProjectManager, null, null, null, null, null, null, mockEventPublisher, null, null, store, context,
                userManager, i18nFactory, null, cacheManager, clusterLockService, mockQueryDslAccessor) {
            @Override
            GenericValue getSchemeForProject(Project project) {
                if (project.equals(project1)) {
                    return null;
                } else {
                    return new MockGenericValue("WorkflowScheme", project.getId());
                }
            }
        };

        assertEquals(Collections.<Project>singletonList(project1), schemeManager.getProjectsUsing(defaultScheme));
        assertEquals(Collections.<Project>singletonList(project2), schemeManager.getProjectsUsing(parent));
    }

    @Test
    public void testUpdateSchemesForRenamedWorkflow() {
        // Cache clear will happen on every call of start, so there will be at least 1 cache clear.
        assertUpdateSchemesForRenamedWorkflow(null, "new", 1, "should not be null");
        assertUpdateSchemesForRenamedWorkflow("", "new", 1, "should not be empty");
        assertUpdateSchemesForRenamedWorkflow(" ", "new", 1, "should not be empty");
        assertUpdateSchemesForRenamedWorkflow("old", null, 1, "should not be null");
        assertUpdateSchemesForRenamedWorkflow("old", "", 1, "should not be empty");
        assertUpdateSchemesForRenamedWorkflow("old", " ", 1, "should not be empty");
        assertUpdateSchemesForRenamedWorkflow("old", "new", 2, "");
    }

    private void assertUpdateSchemesForRenamedWorkflow( final String oldWorkflowName, final String newWorkflowName,
                                                        final int expectedClearCacheCount, String expectedMessageContains) {
        final AtomicInteger cacheClearedCount = new AtomicInteger(0);

        final OfBizDelegator mockOfBizDelegator = Mockito.mock(OfBizDelegator.class);
        final DraftWorkflowSchemeStore store = Mockito.mock(DraftWorkflowSchemeStore.class);
        final EagerWorkflowSchemeManager workflowSchemeManager = new EagerWorkflowSchemeManager(
                null, null, null, null, null, null, mockOfBizDelegator, mockEventPublisher, null, null, store, context,
                userManager, i18nFactory, null, cacheManager, clusterLockService, mockQueryDslAccessor) {
            @Override
            public void clearWorkflowCache() {
                cacheClearedCount.incrementAndGet();
                super.clearWorkflowCache();
            }
        };
        workflowSchemeManager.start();

        if (expectedClearCacheCount > 1) {
            workflowSchemeManager.updateSchemesForRenamedWorkflow(oldWorkflowName, newWorkflowName);
            verify(mockOfBizDelegator).bulkUpdateByAnd("WorkflowSchemeEntity",
                    ImmutableMap.of("workflow", newWorkflowName), ImmutableMap.of("workflow", oldWorkflowName));
            verify(store).renameWorkflow(oldWorkflowName, newWorkflowName);
        } else {
            try {
                workflowSchemeManager.updateSchemesForRenamedWorkflow(oldWorkflowName, newWorkflowName);
                fail("Expected an exception to be thrown");
            } catch (final IllegalArgumentException e) {
                assertThat(e.getMessage(), containsString(expectedMessageContains));
            }
        }

        assertThat(cacheClearedCount.get(), is(equalTo(expectedClearCacheCount)));
    }

    @Test
    public void testGetWorkflowMap() throws Exception {
        // Set up
        final Project project = createProject(100002L, "BJB");
        final MockGenericValue schemeGV = new MockGenericValue("NodeAssociation");
        schemeGV.set("id", 9000L);
        schemeGV.set("name", "test_scheme");
        schemeGV.set("description", "test");

        setBulkGetQueryResult(ImmutableList.of(new ResultRow(100002L, 9000L, 1000L, 9000L, "one","0"),
                new ResultRow(100002L, 9000L, 1001L, 9000L, "two","two"),
                new ResultRow(100002L, 9000L, 1002L, 9000L, "three","three"),
                new ResultRow(100002L, 9000L, 1003L, 9000L, "four","four")));

        when(mockNodeAssociationStore.getSinksFromSource(eq("Project"), eq(100002L), eq("WorkflowScheme"), eq("ProjectScheme")))
                .thenReturn(ImmutableList.of(schemeGV));
        when(mockProjectManager.getProjects()).thenReturn(ImmutableList.of(project));

        final EagerWorkflowSchemeManager testingClass = new EagerWorkflowSchemeManager(mockProjectManager, null,
                null, null, null, null, new MockOfBizDelegator(), mockEventPublisher, mockNodeAssociationStore, null, null, context,
                userManager, i18nFactory, null, cacheManager, clusterLockService, mockQueryDslAccessor);
        testingClass.start();

        // Invoke and check
        final Map<String, String> expectedMap = MapBuilder.<String, String>newBuilder().add(null, "one")
                .add("two", "two").add("three", "three").add("four", "four").toMutableMap();
        assertEquals(expectedMap, testingClass.getWorkflowMap(project));
        assertEquals(expectedMap, testingClass.getWorkflowMap(project));
        mockQueryDslAccessor.assertAllExpectedStatementsWereRun();
    }

    @Test
    public void testGetWorkflowNameFromScheme() throws Exception {
        // Set up
        final GenericValue schemeGv = createSchemeGV(9000L, "one", "two", "three", "four");

        final MockGenericValue schemeGVResult = new MockGenericValue("NodeAssociation", MapBuilder.build("id", 9000L,
                "name", "test_scheme", "description", "test"));

        setBulkGetQueryResult(ImmutableList.of(new ResultRow(100002L, 9000L, 1000L, 9000L, "one","0"),
                new ResultRow(100002L, 9000L, 1001L, 9000L, "two","two"),
                new ResultRow(100002L, 9000L, 1002L, 9000L, "three","three"),
                new ResultRow(100002L, 9000L, 1003L, 9000L, "four","four")));
        when(mockNodeAssociationStore.getSinksFromSource(eq("Project"), eq(100002L), eq("WorkflowScheme"), eq("ProjectScheme")))
                .thenReturn(ImmutableList.of(schemeGVResult));
        when(mockProjectManager.getProjects()).thenReturn(Collections.emptyList());

        final EagerWorkflowSchemeManager testingClass = new EagerWorkflowSchemeManager(mockProjectManager, null,
                null, null, null, null, new MockOfBizDelegator(), mockEventPublisher, null, null, null, context,
                userManager, i18nFactory, null, cacheManager, clusterLockService, mockQueryDslAccessor);
        testingClass.start();

        // Invoke and check
        assertEquals("one", testingClass.getWorkflowName(schemeGv, "0"));
        assertEquals("two", testingClass.getWorkflowName(schemeGv, "two"));
        assertEquals("three", testingClass.getWorkflowName(schemeGv, "three"));
        assertEquals("four", testingClass.getWorkflowName(schemeGv, "four"));
        assertEquals("four", testingClass.getWorkflowName(schemeGv, "four"));
    }

    @Test
    public void testGetWorkflowNameFromProject() throws Exception {
        final Project project = createProject(100002L, "BJB");
        final GenericValue schemeGv = createSchemeGV(9000L);

        setBulkGetQueryResult(ImmutableList.of(new ResultRow(100002L, 9000L, 1000L, 9000L, "0","0"),
                new ResultRow(100002L, 9000L, 1001L, 9000L, "two","two"),
                new ResultRow(100002L, 9000L, 1002L, 9000L, "three","three"),
                new ResultRow(100002L, 9000L, 1003L, 9000L, "four","four")));
        when(mockNodeAssociationStore.getSinksFromSource(eq("Project"), eq(100002L), eq("WorkflowScheme"), eq("ProjectScheme")))
                .thenReturn(ImmutableList.of(schemeGv));
        when(mockProjectManager.getProjects()).thenReturn(Collections.emptyList());

        final EagerWorkflowSchemeManager testingClass = new EagerWorkflowSchemeManager(mockProjectManager, null,
                null, null, null, null, new MockOfBizDelegator(), mockEventPublisher, null, null, null, context, userManager,
                i18nFactory, null, cacheManager, clusterLockService, mockQueryDslAccessor) {
            @Override
            public String getWorkflowName(final GenericValue scheme, final String issueType) {
                assertSame(schemeGv, scheme);
                return issueType;
            }

            @Override
            public List<GenericValue> getSchemes(final GenericValue projectGV) throws GenericEntityException {
                assertSame(project.getGenericValue(), projectGV);

                return Collections.singletonList(schemeGv);
            }
        };

        assertEquals("0", testingClass.getWorkflowName(project, "0"));
        assertEquals("two", testingClass.getWorkflowName(schemeGv, "two"));
        assertEquals("three", testingClass.getWorkflowName(schemeGv, "three"));
        assertEquals("four", testingClass.getWorkflowName(schemeGv, "four"));
        assertEquals("four", testingClass.getWorkflowName(schemeGv, "four"));
    }

    @Test
    public void testGetSchemesForWorkflowIncludingDrafts() {
        final MockDraftWorkflowSchemeStore store = new MockDraftWorkflowSchemeStore();

        setBulkGetQueryResult(Collections.emptyList());
        when(mockNodeAssociationStore.getSinksFromSource(anyString(), anyLong(), anyString(), anyString()))
                .thenReturn(Collections.emptyList());
        when(mockProjectManager.getProjects()).thenReturn(Collections.emptyList());

        DSMForTest defaultWorkflowSchemeManager = new DSMForTest(store, context, userManager, i18nFactory, null) {
            @Override
            public Collection<GenericValue> getSchemesForWorkflow(JiraWorkflow workflow) {
                return Arrays.<GenericValue>asList(createSchemeGV(10001L));
            }
        };
        defaultWorkflowSchemeManager.start();

        MockJiraWorkflow jiraWorkflow = new MockJiraWorkflow();
        jiraWorkflow.setName("Test workflow");

        final AssignableWorkflowScheme assignableScheme = new MockAssignableWorkflowScheme(10001L, "name");
        defaultWorkflowSchemeManager.addScheme(assignableScheme);

        final MockDraftWorkflowScheme draftWorkflowScheme = new MockDraftWorkflowScheme(247837L, assignableScheme);
        draftWorkflowScheme.setMapping("1", jiraWorkflow.getName());

        store.addStateForScheme(draftWorkflowScheme);

        final Iterable<WorkflowScheme> schemes =
                defaultWorkflowSchemeManager.getSchemesForWorkflowIncludingDrafts(jiraWorkflow);

        assertEquals(2, size(schemes));
        assertThat(find(schemes, input -> input.isDraft() && input.getId().equals(draftWorkflowScheme.getId())),
                notNullValue());

        assertThat(find(schemes, input -> !input.isDraft() && input.getId().equals(assignableScheme.getId())),
                notNullValue());
    }

    @Test
    public void copyAndDeleteDraftOfReturnsNullIfDefaultScheme() {
        MockDraftWorkflowSchemeStore store = new MockDraftWorkflowSchemeStore();

        final EagerWorkflowSchemeManager manager = new EagerWorkflowSchemeManager(
                null, null, null, null, null, null, null, mockEventPublisher, null, null, store, context, userManager,
                i18nFactory, null, cacheManager, clusterLockService, mockQueryDslAccessor);
        EagerWorkflowSchemeManager spy = Mockito.spy(manager);

        Project project = new MockProject();

        doReturn(defaultScheme).when(spy).getWorkflowSchemeObj(project);
        assertThat(spy.cleanUpSchemeDraft(project, user), nullValue());

        verify(spy, never()).deleteWorkflowScheme(any(WorkflowScheme.class));
        verify(spy, never()).createSchemeObject(any(String.class), any(String.class));
    }

    @Test
    public void copyAndDeleteDraftOfReturnsNullIfUsedByMultipleProjects() {
        MockDraftWorkflowSchemeStore store = new MockDraftWorkflowSchemeStore();

        final EagerWorkflowSchemeManager manager = new EagerWorkflowSchemeManager(
                null, null, null, null, null, null, null, mockEventPublisher, null, null, store, context, userManager,
                i18nFactory, null, cacheManager, clusterLockService, mockQueryDslAccessor);
        EagerWorkflowSchemeManager spy = Mockito.spy(manager);

        Project project = new MockProject();
        AssignableWorkflowScheme scheme = new MockAssignableWorkflowScheme(10002L, "name", "description");

        doReturn(scheme).when(spy).getWorkflowSchemeObj(project);
        doReturn(asList(project, new MockProject())).when(spy).getProjectsUsing(scheme);

        assertThat(spy.cleanUpSchemeDraft(project, user), nullValue());

        verify(spy, never()).deleteWorkflowScheme(any(WorkflowScheme.class));
        verify(spy, never()).createSchemeObject(any(String.class), any(String.class));
    }

    @Test
    public void copyAndDeleteDraftOfReturnsNullIfNoDraft() {
        MockDraftWorkflowSchemeStore store = new MockDraftWorkflowSchemeStore();

        final EagerWorkflowSchemeManager manager = new EagerWorkflowSchemeManager(
                null, null, null, null, null, null, null, mockEventPublisher, null, null, store, context, userManager,
                i18nFactory, null, cacheManager, clusterLockService, mockQueryDslAccessor);
        EagerWorkflowSchemeManager spy = Mockito.spy(manager);

        Project project = new MockProject();
        AssignableWorkflowScheme scheme = new MockAssignableWorkflowScheme(10002L, "name", "description");

        doReturn(scheme).when(spy).getWorkflowSchemeObj(project);
        doReturn(asList(project)).when(spy).getProjectsUsing(scheme);
        doReturn(null).when(spy).getDraftForParent(scheme);

        assertThat(spy.cleanUpSchemeDraft(project, user), nullValue());

        verify(spy, never()).deleteWorkflowScheme(any(WorkflowScheme.class));
        verify(spy, never()).createSchemeObject(any(String.class), any(String.class));
    }

    @Test
    public void copyAndDeleteDraftOfReturnsCopyOfDraftIfDraftExists() throws GenericEntityException {
        MockDraftWorkflowSchemeStore store = new MockDraftWorkflowSchemeStore();
        final WorkflowSchemeMigrationTaskAccessor taskAccessor = mock(WorkflowSchemeMigrationTaskAccessor.class);

        DSMForTest manager = new DSMForTest(store, context, userManager, i18nFactory, null) {
            @Override
            WorkflowSchemeMigrationTaskAccessor getTaskAccessor() {
                return taskAccessor;
            }
        };
        EagerWorkflowSchemeManager spy = Mockito.spy(manager);

        Project project = new MockProject();
        AssignableWorkflowScheme originalScheme = new MockAssignableWorkflowScheme(10002L, "name", "description");
        MockDraftWorkflowScheme draftWorkflowScheme = new MockDraftWorkflowScheme(1000L, originalScheme);
        draftWorkflowScheme.setMapping("one", "two");

        doReturn(originalScheme).when(spy).getWorkflowSchemeObj(project);
        doReturn(asList(project)).when(spy).getProjectsUsing(originalScheme);
        doReturn(draftWorkflowScheme).when(spy).getDraftForParent(originalScheme);

        String copyName = "Copy of " + draftWorkflowScheme.getName();
        String copyDescription = draftWorkflowScheme.getDescription() + " copied";

        doReturn(copyName).when(spy).getNameForCopy(draftWorkflowScheme.getName(), 255);
        doReturn(copyDescription).when(spy).getDescriptionForCopy(user, originalScheme);

        AssignableWorkflowScheme copyScheme = new MockAssignableWorkflowScheme(null, copyName, copyDescription);

        doReturn(copyScheme).when(spy).createScheme(Matchers.<AssignableWorkflowScheme>any());

        AssignableWorkflowScheme copy = spy.cleanUpSchemeDraft(project, user);

        assertEquals(copyName, copy.getName());
        assertEquals(copyDescription, copy.getDescription());

        verify(spy).deleteWorkflowScheme(draftWorkflowScheme);

        ArgumentCaptor<AssignableWorkflowScheme> captor = ArgumentCaptor.forClass(AssignableWorkflowScheme.class);
        verify(spy).createScheme(captor.capture());
        assertEquals(copyName, captor.getValue().getName());
        assertEquals(copyDescription, captor.getValue().getDescription());
    }

    @Test
    public void createWorkflowScheme() {
        MockAssignableWorkflowSchemeStore store = new MockAssignableWorkflowSchemeStore();

        MockAssignableWorkflowScheme expectedScheme = new MockAssignableWorkflowScheme(null, "Test");
        expectedScheme.setMapping("one", "two");

        final EagerWorkflowSchemeManager manager = new EagerWorkflowSchemeManager(
                null, null, null, null, null, null, ofBizDelegator, mockEventPublisher, null, null, null, context,
                userManager, i18nFactory, store, cacheManager, clusterLockService, mockQueryDslAccessor);
        AssignableWorkflowScheme actualScheme = manager.createScheme(expectedScheme);

        expectedScheme.setId(store.getLastId());

        assertEquals(expectedScheme, new MockAssignableWorkflowScheme(actualScheme));
        assertEquals(new MockAssignableWorkflowSchemeState(expectedScheme), store.get(actualScheme.getId()));

        verify(mockEventPublisher).publish(any(WorkflowSchemeCreatedEvent.class));
    }

    @Test
    public void createWorkflowSchemeUsingOldApiTriggersEvent() throws GenericEntityException {
        // Set up
        final SchemeFactory schemeFactory = mock(SchemeFactory.class, RETURNS_MOCKS);
        final EagerWorkflowSchemeManager manager = new EagerWorkflowSchemeManager(
                null, null, null, schemeFactory, null, null, ofBizDelegator, mockEventPublisher, null, null, null,
                context, userManager, i18nFactory, null, cacheManager, clusterLockService, mockQueryDslAccessor);
        manager.start();

        // Invoke
        manager.createScheme("New Scheme", "New Description");

        // Check
        verify(mockEventPublisher).publish(any(WorkflowSchemeCreatedEvent.class));
    }

    @Test
    public void assignableBuilder() {
        DSMForTest manager = new DSMForTest(null, context, userManager, i18nFactory, null);
        AssignableWorkflowScheme.Builder builder = manager.assignableBuilder();

        assertThat(builder, notNullValue());
    }

    @Test
    public void testIsUsingDefaultWorkflowUsingDefault() {
        final Project project = createProject(100002L, "BJB");

        setBulkGetQueryResult(ImmutableList.of(new ResultRow(100002L, 9000L, 1000L, 9000L, "test", "0")));
        when(mockProjectManager.getProjectObjects()).thenReturn(Lists.newArrayList(project));

        final EagerWorkflowSchemeManager testingClass = new EagerWorkflowSchemeManager(
                mockProjectManager, null, null, null, null, null, null, mockEventPublisher, null, null, null, context, userManager,
                i18nFactory, null, cacheManager, clusterLockService, mockQueryDslAccessor);

        assertThat(testingClass.isUsingDefaultScheme(project), is(false));
    }

    @Test
    public void testIsUsingDefaultWorkflowNotUsingDefault() {
        final Project project = createProject(100002L, "BJB");

        setBulkGetQueryResult(Collections.emptyList());
        when(mockProjectManager.getProjectObjects()).thenReturn(Lists.newArrayList(project));

        final EagerWorkflowSchemeManager testingClass = new EagerWorkflowSchemeManager(mockProjectManager, null, null,
                null, null, null, null, mockEventPublisher, null, null, null, context, userManager,
                i18nFactory, null, cacheManager, clusterLockService, mockQueryDslAccessor);

        assertThat(testingClass.isUsingDefaultScheme(project), is(true));
    }

    private MockGenericValue createSchemeGV(final long id) {
        return createSchemeGV(id, null);
    }

    private MockGenericValue createSchemeGV(final long id, String defaultWf, String... others) {
        final MockGenericValue schemeGv = new MockGenericValue("WorkflowScheme");
        schemeGv.set("id", id);
        schemeGv.setRelated("ChildWorkflowSchemeEntity", createSchemeEntries(defaultWf, others));
        when(ofBizDelegator.findById("WorkflowScheme", id)).thenReturn(schemeGv);
        return schemeGv;
    }

    private void setBulkGetQueryResult(Collection<ResultRow> results) {
        mockQueryDslAccessor.setQueryResults("select NodeAssociationFiltered.source_node_id, WORKFLOW_SCHEME.id, WORKFLOW_SCHEME_ENTITY.id, WORKFLOW_SCHEME_ENTITY.scheme, WORKFLOW_SCHEME_ENTITY.workflow, WORKFLOW_SCHEME_ENTITY.issuetype\n" +
                        "from workflowscheme WORKFLOW_SCHEME\n" +
                        "left join workflowschemeentity WORKFLOW_SCHEME_ENTITY\n" +
                        "on WORKFLOW_SCHEME.id = WORKFLOW_SCHEME_ENTITY.scheme\n" +
                        "left join (select NODE_ASSOCIATION.source_node_id, NODE_ASSOCIATION.source_node_entity, NODE_ASSOCIATION.sink_node_id, NODE_ASSOCIATION.sink_node_entity, NODE_ASSOCIATION.association_type, NODE_ASSOCIATION.sequence\n" +
                        "from nodeassociation NODE_ASSOCIATION\n" +
                        "where NODE_ASSOCIATION.source_node_entity = 'Project' and NODE_ASSOCIATION.sink_node_entity = 'WorkflowScheme') as NodeAssociationFiltered\n" +
                        "on WORKFLOW_SCHEME.id = NodeAssociationFiltered.sink_node_id",
                results);
    }

    private static List<GenericValue> createSchemeEntries(final String defaultWorkflow, String... args) {
        List<GenericValue> entries = new ArrayList<>(args.length / 2 + 1);
        if (defaultWorkflow != null) {
            MockGenericValue defaultValue = new MockGenericValue("sjsjs");
            defaultValue.set("workflow", defaultWorkflow);
            defaultValue.set("issuetype", "0");
            entries.add(defaultValue);
        }

        for (String arg : args) {
            MockGenericValue value = new MockGenericValue("sjsjs");
            value.set("workflow", arg);
            value.set("issuetype", arg);

            entries.add(value);
        }
        return entries;
    }

    private static Project createProject(long id, String name) {
        final GenericValue projectGv = new MockGenericValue("Project");
        projectGv.set("id", id);
        return new MockProject(id, name, name, projectGv);
    }

    private class DSMForTest extends EagerWorkflowSchemeManager {
        private Map<Long, AssignableWorkflowScheme> workflowScheme = Maps.newHashMap();

        public DSMForTest(DraftWorkflowSchemeStore draftWorkflowSchemeStore, JiraAuthenticationContext context,
                          UserManager userManager, I18nHelper.BeanFactory i18nFactory,
                          AssignableWorkflowSchemeStore assignableWorkflowSchemeStore) {
            super(mockProjectManager, null, null, null, null, null, ofBizDelegator, mockEventPublisher, null, null,
                    draftWorkflowSchemeStore, context, userManager, i18nFactory, assignableWorkflowSchemeStore,
                    cacheManager, clusterLockService, mockQueryDslAccessor);
        }

        private DSMForTest addScheme(AssignableWorkflowScheme assignableWorkflowScheme) {
            workflowScheme.put(assignableWorkflowScheme.getId(), assignableWorkflowScheme);
            return this;
        }

        @Override
        public AssignableWorkflowScheme getWorkflowSchemeObj(long id) {
            if (workflowScheme.containsKey(id)) {
                return workflowScheme.get(id);
            }
            throw new IllegalArgumentException(format("Was not expecting a query for a workflow scheme with id '%d'.", id));
        }
    }
}
