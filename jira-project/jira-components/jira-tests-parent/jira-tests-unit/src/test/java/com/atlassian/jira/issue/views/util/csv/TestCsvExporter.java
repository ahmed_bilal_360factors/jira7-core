package com.atlassian.jira.issue.views.util.csv;

import com.atlassian.jira.issue.IssueFactory;
import com.atlassian.jira.issue.fields.Field;
import com.atlassian.jira.issue.link.IssueLinkType;
import com.atlassian.jira.issue.search.SearchProviderFactory;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.issue.views.util.SearchRequestViewBodyWriterUtil;
import com.atlassian.jira.junit.rules.InitMockitoMocks;
import com.atlassian.jira.plugin.searchrequestview.SearchRequestParams;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.web.component.TableLayoutFactory;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.mockito.Mock;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

public class TestCsvExporter {
//    @Rule
//    public TestRule initMockito = new InitMockitoMocks(this);
//
//    @Mock
//    private JiraAuthenticationContext authenticationContext;
//
//    @Mock
//    private TableLayoutFactory tableLayoutFactory;
//
//    @Mock
//    private SearchRequest searchRequest;
//
//    @Mock
//    private SearchRequestParams searchRequestParams;
//
//    @Mock
//    private I18nHelper i18nHelper;
//
//    @Mock
//    private CsvExportableItem exportableItem;
//
//    @Mock
//    private IssueLinkType issueLinkType;
//
//    @Mock
//    private IssueFactory issueFactory;
//
//    @Mock
//    private SearchProviderFactory searchProviderFactory;
//
//    @Mock
//    private SearchRequestViewBodyWriterUtil searchRequestViewBodyWriterUtil;
//
//    @Mock
//    private Field field;
//
//    private static final String I18N_COLUMN_PREFIX = "admin.issue.views.searchrequest.plugin.csv.field.name.";
//
//    private CsvExporter csvExporter;
//
//    @Before
//    public void setUp() {
//        csvExporter = new CsvExporter(authenticationContext, tableLayoutFactory, issueFactory, searchProviderFactory, searchRequestViewBodyWriterUtil);
//
//        when(authenticationContext.getI18nHelper()).thenReturn(i18nHelper);
//        when(exportableItem.getField()).thenReturn(field);
//    }
//
//    @Test
//    public void testColumnHeaderForCustomField() {
//        when(exportableItem.getType()).thenReturn(CsvExportableItemType.CUSTOM_FIELD);
//        when(field.getName()).thenReturn("a name");
//        when(i18nHelper.getText(I18N_COLUMN_PREFIX + "custom.field", "a name")).thenReturn("custom field column");
//
//        assertThat(csvExporter.getColumnHeader(exportableItem), equalTo("custom field column"));
//    }
//
//    @Test
//    public void testColumnHeaderForIssueLinkOutward() {
//        when(exportableItem.getType()).thenReturn(CsvExportableItemType.ISSUE_LINK_OUTWARD);
//        when(exportableItem.getIssueLinkType()).thenReturn(issueLinkType);
//        when(issueLinkType.getName()).thenReturn("a name");
//        when(i18nHelper.getText(I18N_COLUMN_PREFIX + "issue.link.outward", "a name")).thenReturn("issue link column");
//
//        assertThat(csvExporter.getColumnHeader(exportableItem), equalTo("issue link column"));
//    }
//
//    @Test
//    public void testColumnHeaderForGenericField() {
//        when(exportableItem.getType()).thenReturn(CsvExportableItemType.SUMMARY);
//        when(i18nHelper.getText(I18N_COLUMN_PREFIX + "summary")).thenReturn("summary column");
//
//        assertThat(csvExporter.getColumnHeader(exportableItem), equalTo("summary column"));
//    }
}
