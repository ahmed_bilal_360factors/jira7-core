package com.atlassian.jira.jql.util;

import com.atlassian.jira.action.issue.customfields.option.MockOption;
import com.atlassian.jira.issue.customfields.option.Option;
import com.atlassian.jira.jql.operand.QueryLiteral;
import com.atlassian.jira.util.collect.CollectionBuilder;
import com.atlassian.query.operand.EmptyOperand;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.atlassian.jira.jql.operand.SimpleLiteralFactory.createLiteral;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestJqlCascadingSelectLiteralUtil {
    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @Mock
    private JqlSelectOptionsUtil jqlSelectOptionsUtil;

    @InjectMocks
    private JqlCascadingSelectLiteralUtil util;


    @Test
    public void testCreateQueryLiteralsFromOptions() throws Exception {
        final Option posOption = new MockOption(null, null, null, null, null, 10L);
        final Option negOption = new MockOption(null, null, null, null, null, 20L);

        final List<QueryLiteral> result = util.createQueryLiteralsFromOptions(EmptyOperand.EMPTY, Collections.singleton(posOption), Collections.singleton(negOption));

        assertEquals(2, result.size());
        assertTrue(result.contains(createLiteral(10L)));
        assertTrue(result.contains(createLiteral(-20L)));
    }

    @Test
    public void testProcessPositiveNegativeOptionLiteralsNoLiterals() throws Exception {
        final List<QueryLiteral> literals = CollectionBuilder.<QueryLiteral>newBuilder().asList();
        final List<QueryLiteral> positiveLiterals = new ArrayList<>();
        final List<QueryLiteral> negativeLiterals = new ArrayList<>();

        util.processPositiveNegativeOptionLiterals(literals, positiveLiterals, negativeLiterals);

        assertTrue(positiveLiterals.isEmpty());
        assertTrue(negativeLiterals.isEmpty());
    }

    @Test
    public void testProcessPositiveNegativeOptionLiterals() throws Exception {
        final List<QueryLiteral> literals = CollectionBuilder.<QueryLiteral>newBuilder(new QueryLiteral(), createLiteral("500"), createLiteral(500L), createLiteral(-500L), createLiteral(-600L)).asList();
        final List<QueryLiteral> positiveLiterals = new ArrayList<>();
        final List<QueryLiteral> negativeLiterals = new ArrayList<>();
        when(jqlSelectOptionsUtil.getOptionById(500L)).thenReturn(new MockOption(null, null, null, null, null, 500L));
        when(jqlSelectOptionsUtil.getOptionById(600L)).thenReturn(null);

        util.processPositiveNegativeOptionLiterals(literals, positiveLiterals, negativeLiterals);

        final List<QueryLiteral> expectedPositiveLiterals = CollectionBuilder.<QueryLiteral>newBuilder(new QueryLiteral(), createLiteral("500"), createLiteral(500L), createLiteral(-600L)).asList();
        final List<QueryLiteral> expectedNegativeLiterals = CollectionBuilder.<QueryLiteral>newBuilder(createLiteral(500L)).asList();
        assertEquals(expectedPositiveLiterals, positiveLiterals);
        assertEquals(expectedNegativeLiterals, negativeLiterals);
    }

    @Test
    public void testIsNegativeLiteral() throws Exception {
        final QueryLiteral positiveLiteral1 = createLiteral(500L);
        final QueryLiteral positiveLiteral2 = createLiteral("abc");
        final QueryLiteral negativeLiteral = createLiteral(-500L);
        final QueryLiteral emptyLiteral = new QueryLiteral();
        when(jqlSelectOptionsUtil.getOptionById(500L)).thenReturn(new MockOption(null, null, null, null, null, 500L));

        assertFalse(util.isNegativeLiteral(positiveLiteral1));
        assertFalse(util.isNegativeLiteral(positiveLiteral2));
        assertTrue(util.isNegativeLiteral(negativeLiteral));
        assertFalse(util.isNegativeLiteral(emptyLiteral));
    }
}
