package com.atlassian.jira.issue.fields.csv.util;

import java.util.Date;

import javax.annotation.Nullable;

import com.atlassian.jira.issue.views.util.csv.CsvDateFormatterImpl;

public class StubCsvDateFormatter extends CsvDateFormatterImpl {
    public StubCsvDateFormatter() {
        super(null);
    }

    @Nullable
    @Override
    public String formatDateTime(@Nullable Date date) {
        return date != null ? date + " datetime" : null;
    }

    @Nullable
    @Override
    public String formatDate(@Nullable final Date date) {
        return date != null ? date + " date" : null;
    }
}

