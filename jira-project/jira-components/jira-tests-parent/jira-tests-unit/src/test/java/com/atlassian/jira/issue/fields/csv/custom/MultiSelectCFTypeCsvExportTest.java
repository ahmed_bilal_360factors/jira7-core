package com.atlassian.jira.issue.fields.csv.custom;

import com.atlassian.jira.issue.customfields.CustomFieldType;
import com.atlassian.jira.issue.customfields.impl.MultiSelectCFType;
import com.atlassian.jira.issue.customfields.option.Option;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.google.common.collect.ImmutableList;
import org.junit.Test;

import java.util.Collection;
import java.util.Locale;

import static com.atlassian.jira.action.issue.customfields.option.MockOption.value;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class MultiSelectCFTypeCsvExportTest extends CustomFieldMultiValueCsvExporterTest<Collection<Option>> {
    @Override
    protected CustomFieldType<Collection<Option>, ?> createField() {
        JiraAuthenticationContext jac = mock(JiraAuthenticationContext.class);
        when(jac.getLocale()).thenReturn(Locale.ENGLISH);
        return new MultiSelectCFType(null, null, null, null, null, null, jac);
    }

    @Test
    public void allSelectedItemsAreExportedAsSeparateValues() {
        whenFieldValueIs(ImmutableList.of(value("a"), value("b")));
        assertExportedValue("a", "b");
    }

    @Test
    public void allSelectedItemsAreExportedAsSepareteValues() {
        whenFieldValueIs(ImmutableList.of(value("a"), value("b")));
        assertExportedValue("a", "b");
    }
}
