package com.atlassian.jira.jql.validator;

import com.atlassian.jira.jql.operand.JqlOperandResolver;
import com.atlassian.jira.jql.resolver.StatusResolver;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.MessageSet;
import com.atlassian.jira.util.MessageSetImpl;
import com.atlassian.query.clause.TerminalClause;
import com.atlassian.query.clause.TerminalClauseImpl;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static com.atlassian.query.operator.Operator.GREATER_THAN;
import static org.hamcrest.Matchers.contains;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestStatusValidator {
    @Rule
    public MockitoRule mocks = MockitoJUnit.rule();

    @Mock
    private SupportedOperatorsValidator mockSupportedOperatorsValidator;
    @Mock
    private RawValuesExistValidator mockRawValuesExistValidator;

    @Test
    public void testRawValuesDelegateNotCalledWithOperatorProblem() throws Exception {
        final TerminalClauseImpl clause = new TerminalClauseImpl("test", GREATER_THAN, 12L);

        final MessageSetImpl supportedOperatorsMessageSet = new MessageSetImpl();
        final String message = "blah blah";
        supportedOperatorsMessageSet.addErrorMessage(message);
        final MessageSetImpl valExistValidatorMessageSet = new MessageSetImpl();
        valExistValidatorMessageSet.addErrorMessage("no ne");

        when(mockSupportedOperatorsValidator.validate(null, clause)).thenReturn(supportedOperatorsMessageSet);
        when(mockRawValuesExistValidator.validate(any(ApplicationUser.class), any(TerminalClause.class))).thenReturn(valExistValidatorMessageSet);


        final StatusValidator statusValidator = new StatusValidator(null, null, null) {
            @Override
            SupportedOperatorsValidator getSupportedOperatorsValidator() {
                return mockSupportedOperatorsValidator;
            }

            @Override
            RawValuesExistValidator getRawValuesValidator(final StatusResolver statusResolver, final JqlOperandResolver operandSupport) {
                return mockRawValuesExistValidator;
            }
        };

        final MessageSet validationResult = statusValidator.validate(null, clause);
        assertThat(validationResult.getErrorMessages(), contains(message));
    }

    @Test
    public void testRawValuesDelegateCalledWithNoOperatorProblem() throws Exception {
        final TerminalClauseImpl clause = new TerminalClauseImpl("test", GREATER_THAN, 12L);

        final MessageSetImpl supportedOperatorsMessageSet = new MessageSetImpl();
        final String message = "no ne";
        final MessageSetImpl valExistValidatorMessageSet = new MessageSetImpl();
        valExistValidatorMessageSet.addErrorMessage(message);

        when(mockSupportedOperatorsValidator.validate(null, clause)).thenReturn(supportedOperatorsMessageSet);
        when(mockRawValuesExistValidator.validate(any(ApplicationUser.class), any(TerminalClause.class))).thenReturn(valExistValidatorMessageSet);

        final StatusValidator statusValidator = new StatusValidator(null, null, null) {
            @Override
            SupportedOperatorsValidator getSupportedOperatorsValidator() {
                return mockSupportedOperatorsValidator;
            }

            @Override
            RawValuesExistValidator getRawValuesValidator(final StatusResolver statusResolver, final JqlOperandResolver operandSupport) {
                return mockRawValuesExistValidator;
            }
        };

        final MessageSet validationResult = statusValidator.validate(null, clause);
        assertThat(validationResult.getErrorMessages(), contains(message));
    }

}
