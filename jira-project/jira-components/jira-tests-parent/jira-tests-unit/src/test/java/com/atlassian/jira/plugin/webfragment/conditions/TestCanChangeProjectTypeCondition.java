package com.atlassian.jira.plugin.webfragment.conditions;

import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.type.BrowseProjectTypeManager;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestCanChangeProjectTypeCondition {
    @Mock
    private BrowseProjectTypeManager browseProjectTypeManager;

    @Mock
    private JiraHelper jiraHelper;

    private CanChangeProjectTypeCondition canChangeProjectTypeCondition;

    @Before
    public void setUp() {
        when(jiraHelper.getProject()).thenReturn(mock(Project.class));
        canChangeProjectTypeCondition = new CanChangeProjectTypeCondition(browseProjectTypeManager);
    }

    @Test
    public void testFalse() {
        when(browseProjectTypeManager.isProjectTypeChangeAllowed(isA(Project.class))).thenReturn(false);

        assertThat(canChangeProjectTypeCondition.shouldDisplay(null, jiraHelper), is(false));
    }

    @Test
    public void testTrue() {
        when(browseProjectTypeManager.isProjectTypeChangeAllowed(isA(Project.class))).thenReturn(true);

        assertThat(canChangeProjectTypeCondition.shouldDisplay(null, jiraHelper), is(true));
    }
}
