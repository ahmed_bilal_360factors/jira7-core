package com.atlassian.jira.imports.project;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.spi.LoggingEvent;
import org.apache.log4j.spi.ThrowableInformation;
import org.apache.log4j.varia.NullAppender;
import org.hamcrest.FeatureMatcher;
import org.hamcrest.Matcher;
import org.mockito.Mockito;

import java.util.Optional;

import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.verify;

/**
 * @since v6.5
 */
public class LogVerifier implements AutoCloseable {

    private final Logger logger;
    private final NullAppender appenderSpy;

    public static LogVerifier catchLogMessages(Logger logger) {
        return new LogVerifier(logger);
    }

    public LogVerifier(final Logger logger) {
        this.logger = logger;
        appenderSpy = Mockito.spy(NullAppender.getNullAppender());
        logger.addAppender(appenderSpy);
    }

    public void verifyEventLogged(Matcher<LoggingEvent> loggingEventMatcher) {
        verify(appenderSpy).doAppend(
                argThat(loggingEventMatcher));
    }

    @Override
    public void close() throws Exception {
        logger.removeAppender(appenderSpy);
    }

    public static Matcher<LoggingEvent> level(final Matcher<? super Level> levelMatcher) {
        return new LoggingEventTypeMatcher(levelMatcher);
    }

    public static Matcher<LoggingEvent> exception(final Matcher<? super Throwable> subMatcher) {
        return new LoggingEventExceptionMatcher(subMatcher);
    }

    public static Matcher<LoggingEvent> renderedMessage(final Matcher<? super String> subMatcher) {
        return new LoggingEventRenderedMessageMatcher(subMatcher);
    }

    static class LoggingEventTypeMatcher extends FeatureMatcher<LoggingEvent, Level> {
        public LoggingEventTypeMatcher(final Matcher<? super Level> subMatcher) {
            super(subMatcher, "LoggingEvent level", "logging level");
        }

        @Override
        protected Level featureValueOf(final LoggingEvent actual) {
            return actual.getLevel();
        }
    }

    static class LoggingEventExceptionMatcher extends FeatureMatcher<LoggingEvent, Throwable> {
        public LoggingEventExceptionMatcher(final Matcher<? super Throwable> subMatcher) {
            super(subMatcher, "LoggingEvent level", "logging level");
        }

        @Override
        protected Throwable featureValueOf(final LoggingEvent actual) {
            return Optional.ofNullable(actual.getThrowableInformation())
                    .map(ThrowableInformation::getThrowable)
                    .orElse(null);
        }
    }

    static class LoggingEventRenderedMessageMatcher extends FeatureMatcher<LoggingEvent, String> {
        public LoggingEventRenderedMessageMatcher(final Matcher<? super String> subMatcher) {
            super(subMatcher, "LoggingEvent level", "logging level");
        }

        @Override
        protected String featureValueOf(final LoggingEvent actual) {
            return actual.getRenderedMessage();
        }
    }
}
