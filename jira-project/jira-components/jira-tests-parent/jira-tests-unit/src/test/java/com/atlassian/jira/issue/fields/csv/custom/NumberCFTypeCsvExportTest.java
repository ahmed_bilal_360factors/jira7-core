package com.atlassian.jira.issue.fields.csv.custom;

import com.atlassian.jira.issue.customfields.CustomFieldType;
import com.atlassian.jira.issue.customfields.impl.NumberCFType;
import org.junit.Test;

public class NumberCFTypeCsvExportTest extends CustomFieldSingleValueCsvExporterTest<Double> {
    @Override
    protected CustomFieldType<Double, ?> createField() {
        return new NumberCFType(null, null, null);
    }

    @Test
    public void numbersAreExportedProperly() {
        whenFieldValueIs(0.2d);
        assertExportedValue("0.2");
    }
}
