package com.atlassian.jira.sharing;

import com.atlassian.jira.index.Index;
import com.atlassian.jira.index.MockResult;
import com.atlassian.jira.issue.search.MockSearchRequest;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.portal.PortalPage;
import com.atlassian.jira.sharing.SharedEntity.SharePermissions;
import com.atlassian.jira.sharing.index.SharedEntityIndexer;
import com.atlassian.jira.sharing.search.SharedEntitySearchResult;
import com.atlassian.jira.sharing.search.SharedEntitySearcher;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.util.Consumer;
import com.atlassian.jira.util.collect.MockCloseableIterable;
import com.google.common.collect.Lists;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestDefaultSharePermissionReindexer {
    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Test
    public void testConstructorThrowsWithNullArg() throws Exception {
        exception.expect(IllegalArgumentException.class);
        new DefaultSharePermissionReindexer(null);
    }

    @Test
    public void testReindexThrowsWithUnknown() throws Exception {
        @SuppressWarnings("unchecked")
        final AtomicInteger closed = new AtomicInteger();

        final ApplicationUser user = new MockApplicationUser("test");

        final SearchRequest searchRequestEntity = new MockSearchRequest(user.getName(), 1L);
        final PortalPage portalPageEntity = new PortalPage.Builder().id(123L).build();

        final SharedEntityIndexer indexer = mock(SharedEntityIndexer.class);

        // indexer is called once for each entity type
        final Index.Result mockResult = new MockResult();
        when(indexer.index(searchRequestEntity)).thenReturn(mockResult);
        when(indexer.index(portalPageEntity)).thenReturn(mockResult);

        final SharedEntitySearcher<SearchRequest> searchRequestSearcher = prepareSearchResult(closed, searchRequestEntity);
        when(indexer.getSearcher(SearchRequest.ENTITY_TYPE)).thenReturn(searchRequestSearcher);

        final SharedEntitySearcher<PortalPage> portalPageSharedEntitySearcher = prepareSearchResult(closed, portalPageEntity);
        when(indexer.getSearcher(PortalPage.ENTITY_TYPE)).thenReturn(portalPageSharedEntitySearcher);

        final DefaultSharePermissionReindexer reindexer = new DefaultSharePermissionReindexer(indexer);

        // now the test
        reindexer.reindex(SharePermissions.GLOBAL.getPermissionSet().iterator().next());
        assertEquals(2, closed.get());
    }

    private <T extends SharedEntity> SharedEntitySearcher<T> prepareSearchResult(final AtomicInteger closed, final T entity) {
        final MockCloseableIterable<T> results = new MockCloseableIterable<T>(Lists.<T>newArrayList(entity)) {
            @Override
            public void foreach(final Consumer<T> sink) {
                closed.incrementAndGet();
                super.foreach(sink);
            }
        };
        final SharedEntitySearcher<T> searcher = mock(SharedEntitySearcher.class);
        when(searcher.search(any())).thenReturn(new SharedEntitySearchResult<>(results, false, 0));
        return searcher;
    }

    @Test
    public void testReindexThrowsIllegalStateIfIndexerDoesntSupportKnownReindexableTypes() throws Exception {
        final SharedEntityIndexer indexer = mock(SharedEntityIndexer.class);
        when(indexer.getSearcher(any())).thenReturn(null);
        final SharePermissionReindexer reindexer = new DefaultSharePermissionReindexer(indexer);
        exception.expect(IllegalStateException.class);
        reindexer.reindex(SharePermissions.GLOBAL.getPermissionSet().iterator().next());
    }
}
