package com.atlassian.jira.util;

import org.junit.Test;

import java.util.Optional;
import java.util.stream.Stream;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class TestJiraCollectionUtils {
    @Test
    public void streamFromEmptyOptional() {
        final Stream s = JiraCollectionUtils.streamFromOptional(Optional.empty());

        assertThat(s.count(), is(0L));
    }

    @Test
    public void streamFromPresentOptional() {
        final Stream s = JiraCollectionUtils.streamFromOptional(Optional.of("string"));

        assertThat(s.count(), is(1L));
    }
}