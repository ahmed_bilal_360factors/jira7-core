package com.atlassian.jira.issue.comments;

import com.atlassian.jira.config.database.DatabaseConfig;
import com.atlassian.jira.config.database.DatabaseConfigurationManager;
import com.atlassian.jira.config.database.JndiDatasource;
import com.atlassian.jira.database.MockQueryDslAccessor;
import com.atlassian.jira.database.ResultRow;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.junit.rules.MockitoContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.mock.issue.MockIssue;
import com.atlassian.jira.security.roles.ProjectRoleManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.JiraDateUtils;
import com.google.common.collect.ImmutableSet;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.mockito.Mock;

import java.sql.Timestamp;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.function.LongPredicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.LongStream;
import java.util.stream.Stream;

import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Sets.cartesianProduct;
import static java.util.stream.Collectors.toList;
import static java.util.stream.IntStream.rangeClosed;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsIterableContainingInOrder.contains;
import static org.hamcrest.core.Is.is;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.when;

@RunWith(Parameterized.class)
public class TestStreamingCommentsRetriever {
    /** this should be kept in sync with {@link StreamingCommentsRetriever#COMMENTS_FETCH_SIZE} */
    private static final int BATCH_SIZE = 10000;
    private static final String QUERY = "select ACTION.id, ACTION.issueid, ACTION.author, ACTION.actiontype, ACTION.actionlevel, ACTION.rolelevel, ACTION.actionbody, ACTION.created, ACTION.updateauthor, ACTION.updated, ACTION.actionnum\n" +
            "from jiraaction ACTION\n" +
            "where ACTION.issueid = 1 and ACTION.actiontype = 'comment'\n" +
            "order by ACTION.created asc, ACTION.id asc\n" +
            "limit " + BATCH_SIZE;
    private static final String BATCH_QUERY = "select ACTION.id, ACTION.issueid, ACTION.author, ACTION.actiontype, ACTION.actionlevel, ACTION.rolelevel, ACTION.actionbody, ACTION.created, ACTION.updateauthor, ACTION.updated, ACTION.actionnum\n" +
            "from jiraaction ACTION\n" +
            "where ACTION.issueid = 1 and ACTION.actiontype = 'comment' and (ACTION.created > %2$s or ACTION.created = %2$s and ACTION.id > %1$d)\n" +
            "order by ACTION.created asc, ACTION.id asc\n" +
            "limit " + BATCH_SIZE;
    private static final long ISSUE_ID = 1L;
    private static final Issue SOME_ISSUE = new MockIssue(ISSUE_ID);
    private static final String COMMENT_AUTHOR = "stubauthor";
    private static final String COMMENT_UPDATE_AUTHOR = "stubupdateauthor";
    private static final String COMMENT_BODY = "stubactionbody";
    private static final Timestamp COMMENT_CREATED = new Timestamp(10000L);
    private static final Timestamp COMMENT_UPDATED = new Timestamp(20000L);
    private static final long COMMENT_ROLE_LEVEL = 1L;
    private static final String COMMENT_GROUP_LEVEL = null;
    private static final ApplicationUser LOGGED_IN_USER = new MockApplicationUser("loggedInUser");
    private final int commentCount;
    private final LongPredicate permissionCheck;

    @Rule
    public MockitoContainer container = MockitoMocksInContainer.rule(this);

    private MockQueryDslAccessor queryDslAccessor = new MockQueryDslAccessor();
    @Mock
    private ProjectRoleManager projectRoleManager;
    @Mock
    private UserManager userManager;
    @Mock
    private CommentPermissionManager commentPermissionManager;
    @Mock
    private DatabaseConfigurationManager databaseConfigurationManager;

    private StreamingCommentsRetriever streamingCommentsRetriever;

    @Parameterized.Parameters(name = "{0} comments, {1} allowed")
    public static Collection<Object[]> parameters() {
        @SuppressWarnings("unchecked")
        Set<List<Object[]>> permutations = cartesianProduct(
                IntStream.of(
                        3,
                        BATCH_SIZE - 1,
                        BATCH_SIZE,
                        BATCH_SIZE + 1,
                        2 * BATCH_SIZE - 1,
                        2 * BATCH_SIZE,
                        2 * BATCH_SIZE + 1
                ).mapToObj(i -> new Object[] { i }).collect(Collectors.toSet()),
                ImmutableSet.of(
                        new Object[] {
                                "all comments",
                                (LongPredicate) id -> true
                        },
                        new Object[] {
                                "every other comment",
                                (LongPredicate) id -> id % 2 == 0
                        }
                )
        );
        return permutations.stream().map(permutation -> permutation.stream().flatMap(Stream::of).toArray()).collect(toList());
    }

    public TestStreamingCommentsRetriever(int commentCount,
                                          @SuppressWarnings("unused") String predicateType,
                                          LongPredicate permissionCheck) {
        this.commentCount = commentCount;
        this.permissionCheck = permissionCheck;
    }

    @Before
    public void setup() {
        mockBatchQueries();

        when(commentPermissionManager.hasBrowsePermission(eq(LOGGED_IN_USER), isA(Comment.class))).thenAnswer(invocation -> {
            Comment comment = (Comment) invocation.getArguments()[1];
            return permissionCheck.test(comment.getId());
        });

        when(databaseConfigurationManager.getDatabaseConfiguration()).thenReturn(new DatabaseConfig("fooDb", "jira", new JndiDatasource("jndi;yo")));

        streamingCommentsRetriever = new StreamingCommentsRetriever(
                queryDslAccessor,
                projectRoleManager,
                userManager,
                commentPermissionManager,
                databaseConfigurationManager
        );
    }

    @Test
    public void shouldReturnCorrectCommentCount() {
        long count = streamingCommentsRetriever.stream(LOGGED_IN_USER, SOME_ISSUE).count();

        assertThat(count, is((long) getExpectedIds().size()));
    }

    @Test
    public void shouldCallTheProvidedCallbackForEachComment() {
        List<Long> expectedIds = getExpectedIds();

        Iterable<Long> actualIds = streamingCommentsRetriever.stream(LOGGED_IN_USER, SOME_ISSUE).map(Comment::getId)::iterator;

        assertThat(actualIds, contains(expectedIds.toArray()));
    }

    @Test
    public void shouldConvertActionDTOToComment() {
        queryDslAccessor.setQueryResults(QUERY, newArrayList(
                getActionDTOResultRow(1L)
        ));
        ApplicationUser user1 = new MockApplicationUser("user1");
        when(userManager.getUserByKeyEvenWhenUnknown(COMMENT_AUTHOR)).thenReturn(user1);
        ApplicationUser user2 = new MockApplicationUser("user2");
        when(userManager.getUserByKeyEvenWhenUnknown(COMMENT_UPDATE_AUTHOR)).thenReturn(user2);

        streamingCommentsRetriever.stream(LOGGED_IN_USER, SOME_ISSUE).forEach(comment -> verifyComment(comment, user1, user2));
    }

    private void verifyComment(Comment comment, ApplicationUser user1, ApplicationUser user2) {
        assertThat(comment.getId(), is(1L));
        assertThat(comment.getBody(), is(COMMENT_BODY));
        assertThat(comment.getGroupLevel(), is(COMMENT_GROUP_LEVEL));
        assertThat(comment.getRoleLevelId(), is(COMMENT_ROLE_LEVEL));
        assertThat(comment.getCreated(), is(JiraDateUtils.copyDateNullsafe(COMMENT_CREATED)));
        assertThat(comment.getUpdated(), is(JiraDateUtils.copyDateNullsafe(COMMENT_UPDATED)));
        assertThat(comment.getAuthorApplicationUser(), is(user1));
        assertThat(comment.getUpdateAuthorApplicationUser(), is(user2));
        assertThat(comment.getIssue(), is(SOME_ISSUE));
    }

    private ResultRow getActionDTOResultRow(long id) {
        return new ResultRow(id, ISSUE_ID, COMMENT_AUTHOR, "stubactiontype", COMMENT_GROUP_LEVEL, COMMENT_ROLE_LEVEL, COMMENT_BODY, COMMENT_CREATED, COMMENT_UPDATE_AUTHOR, COMMENT_UPDATED, 1L);
    }

    private List<Long> getExpectedIds() {
        return LongStream.rangeClosed(1, commentCount).filter(permissionCheck).boxed().collect(Collectors.toList());
    }

    private void mockBatchQueries() {
        ResultRow lastComment = null;
        for (int i = 0, to = commentCount / BATCH_SIZE; i <= to; i++) {
            int start = i * BATCH_SIZE;
            List<ResultRow> list = rangeClosed(start + 1, Math.min(start + BATCH_SIZE, commentCount)).mapToObj(this::getActionDTOResultRow).collect(toList());
            if (lastComment == null) {
                // first query
                queryDslAccessor.setQueryResults(QUERY, list);
            } else {
                // following queries
                queryDslAccessor.setQueryResults(String.format(BATCH_QUERY, lastComment.getObject(1), lastComment.getObject(8)), list);
            }
            if (!list.isEmpty()) {
                lastComment = list.get(list.size() - 1);
            }
        }
    }
}
