package com.atlassian.jira.scheme;

/*
 * Copyright (c) 2002-2004
 * All rights reserved.
 */

import com.atlassian.jira.issue.security.IssueSecurityLevel;
import com.atlassian.jira.issue.security.IssueSecurityLevelImpl;
import com.atlassian.jira.issue.security.IssueSecurityLevelManager;
import com.atlassian.jira.issue.security.IssueSecurityLevelService;
import com.atlassian.jira.issue.security.IssueSecurityLevelServiceImpl;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.SimpleErrorCollection;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static org.apache.commons.lang3.StringUtils.repeat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestIssueSecurityLevelEdit {

    @Rule
    public MockitoRule mockitoMocksInContainer = MockitoJUnit.rule();

    @Mock
    private IssueSecurityLevelManager issueSecurityLevelManager;

    @Mock
    private GlobalPermissionManager globalPermissionManager;

    @Mock
    private ApplicationUser applicationUser;

    @Mock
    private I18nHelper i18nHelper;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    private IssueSecurityLevelService underTest;
    private IssueSecurityLevel securityLevelA;
    private IssueSecurityLevel securityLevelB;


    @Before
    public void setUp() {
        securityLevelA = new IssueSecurityLevelImpl(1l, "Level A", "Description A", 1l);
        securityLevelB = new IssueSecurityLevelImpl(2l, "Level B", "Description B", 1l);

        when(issueSecurityLevelManager.getSecurityLevel(anyLong())).thenReturn(securityLevelA);
        underTest = Mockito.spy(new IssueSecurityLevelServiceImpl(issueSecurityLevelManager, globalPermissionManager, i18nHelper));
        when(globalPermissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, applicationUser)).thenReturn(true);
        when(issueSecurityLevelManager.getSecurityLevelByNameAndSchema(anyString(), anyLong())).thenReturn(null);

    }

    @Test
    public void levelIsValidIfNameAndDescriptionAreProvided() throws Exception {
        String newName = "Level C";
        String newDescription = "Description of Level C";
        IssueSecurityLevelService.LevelValidationResult vr = underTest.validateUpdate(applicationUser, securityLevelA, newName, newDescription);
        assertTrue(vr.isValid());
    }

    @Test
    public void levelIsNotValidWhenNameIsNull() throws Exception {
        checkValidityForName(null, false);
    }

    @Test
    public void levelIsNotValidIfNameIsEmpty() throws Exception {
        checkValidityForName("", false);
    }

    @Test
    public void levelIsNotValidIfNameLongerThan255Chars() throws Exception {
        String newName = repeat('a', 256);
        checkValidityForName(newName, false);
    }

    @Test
    public void levelIsValidIfNameIs255CharLong() throws Exception {
        String newName = repeat('a', 255);
        checkValidityForName(newName, true);
    }

    @Test
    public void levelIsNotValidIfLevelWithTheSameNameAlreadyExists() throws Exception {
        IssueSecurityLevel fakeDuplicate = Mockito.mock(IssueSecurityLevel.class);
        when(issueSecurityLevelManager.getSecurityLevelByNameAndSchema(securityLevelB.getName(), securityLevelA.getSchemeId())).thenReturn(fakeDuplicate);
        checkValidityForName(securityLevelB.getName(), false);
    }

    @Test
    public void levelIsValidIfNameDidntChange() throws Exception {
        checkValidityForName(securityLevelA.getName(), true);
    }

    @Test
    public void levelIsValidIfDescriptionHasChanged() throws Exception {
        String newDescription = securityLevelA.getDescription() + "something";
        IssueSecurityLevelService.LevelValidationResult vr = underTest.validateUpdate(applicationUser, securityLevelA, securityLevelA.getName(), newDescription);
        assertTrue(vr.isValid());
    }

    @Test
    public void levelIsValidWithAnEmptyDescription() throws Exception {
        IssueSecurityLevelService.LevelValidationResult vr = underTest.validateUpdate(applicationUser, securityLevelA, securityLevelA.getName(), "");
        assertTrue(vr.isValid());
    }

    private void checkValidityForName(String name, Boolean shouldBeValid) throws Exception {
        IssueSecurityLevelService.LevelValidationResult vr = underTest.validateUpdate(applicationUser, securityLevelA, name, securityLevelA.getDescription());
        assertEquals(vr.isValid(), shouldBeValid);
    }

    @Test
    public void shouldNotSaveLevelIfValidationResultIsInvalid() {
        thrown.expect(IllegalStateException.class);
        underTest.update(applicationUser, getUpdateValidationMock(false, null));
    }

    @Test
    public void shouldNotCreateLevelIfValidationResultIsInvalid() {
        thrown.expect(IllegalStateException.class);
        underTest.create(applicationUser, getCreateValidationMock(false, null));
    }

    @Test
    public void shouldNotUpdateLevelIfUserIsNotAdmin() {
        when(globalPermissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, applicationUser)).thenReturn(false);
        underTest.update(applicationUser, getUpdateValidationMock(true, securityLevelA));
        verify(issueSecurityLevelManager, never()).updateIssueSecurityLevel(any(IssueSecurityLevel.class));
    }

    @Test
    public void shouldNotCreateLevelIfUserIsNotAdmin() {
        when(globalPermissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, applicationUser)).thenReturn(false);
        underTest.create(applicationUser, getCreateValidationMock(true, securityLevelA));
        verify(issueSecurityLevelManager, never()).createIssueSecurityLevel(any(IssueSecurityLevel.class));
    }

    @Test
    public void shouldCreateLevelIfUserIsAdminAndLevelIsValid() {
        underTest.create(applicationUser, getCreateValidationMock(true, securityLevelA));
        verify(issueSecurityLevelManager, times(1)).createIssueSecurityLevel(any(IssueSecurityLevel.class));
    }

    @Test
    public void shouldUpdateLevelIfUserIsAdminAndLevelIsValid() {
        underTest.update(applicationUser, getUpdateValidationMock(true, securityLevelA));
        verify(issueSecurityLevelManager, times(1)).updateIssueSecurityLevel(any(IssueSecurityLevel.class));
    }

    private IssueSecurityLevelService.CreateValidationResult getCreateValidationMock(Boolean isValid, IssueSecurityLevel level) {
        IssueSecurityLevelService.CreateValidationResult vr = mock(IssueSecurityLevelService.CreateValidationResult.class);
        when(vr.isValid()).thenReturn(isValid);
        when(vr.getLevel()).thenReturn(level);
        if (isValid) {
            when(vr.getErrors()).thenReturn(new SimpleErrorCollection());
        }
        return vr;
    }

    private IssueSecurityLevelService.UpdateValidationResult getUpdateValidationMock(Boolean isValid, IssueSecurityLevel level) {
        IssueSecurityLevelService.UpdateValidationResult vr = mock(IssueSecurityLevelService.UpdateValidationResult.class);
        when(vr.isValid()).thenReturn(isValid);
        when(vr.getLevel()).thenReturn(level);
        if (isValid) {
            when(vr.getErrors()).thenReturn(new SimpleErrorCollection());
        }
        return vr;
    }
}