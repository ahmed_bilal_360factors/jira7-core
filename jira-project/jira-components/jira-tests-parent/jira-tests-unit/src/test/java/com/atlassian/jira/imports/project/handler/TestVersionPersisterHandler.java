package com.atlassian.jira.imports.project.handler;

import com.atlassian.jira.external.beans.ExternalNodeAssociation;
import com.atlassian.jira.imports.project.ProjectImportPersister;
import com.atlassian.jira.imports.project.core.BackupSystemInformation;
import com.atlassian.jira.imports.project.core.ProjectImportResultsImpl;
import com.atlassian.jira.imports.project.mapper.ProjectImportMapper;
import com.atlassian.jira.imports.project.mapper.ProjectImportMapperImpl;
import com.atlassian.jira.imports.project.parser.NodeAssociationParser;
import com.atlassian.jira.imports.project.transformer.VersionTransformer;
import com.atlassian.jira.junit.rules.InitMockitoMocks;
import com.atlassian.jira.web.bean.MockI18nBean;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static com.atlassian.jira.imports.project.parser.NodeAssociationParser.AFFECTS_VERSION_TYPE;
import static com.atlassian.jira.imports.project.parser.NodeAssociationParser.FIX_VERSION_TYPE;
import static com.atlassian.jira.imports.project.parser.NodeAssociationParser.NODE_ASSOCIATION_ENTITY_NAME;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.emptyIterable;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

/**
 * @since v3.13
 */
@RunWith(MockitoJUnitRunner.class)
public class TestVersionPersisterHandler {
    @Rule
    public TestRule initMockitoMocks = new InitMockitoMocks(this);

    @Mock
    private VersionTransformer mockVersionTransformer;
    @Mock
    private NodeAssociationParser mockNodeAssociationParser;
    @Mock
    private ProjectImportPersister mockProjectImportPersister;
    @Mock
    private BackupSystemInformation mockBackupSystemInformation;

    ProjectImportMapper projectImportMapper;

    @Before
    public void setUp() {
        projectImportMapper = new ProjectImportMapperImpl(null, null);
    }

    @Test
    public void testHandle() throws Exception {

        final ExternalNodeAssociation externalNodeAssociation = new ExternalNodeAssociation("", "", "", "", AFFECTS_VERSION_TYPE);

        when(mockNodeAssociationParser.parse(null)).thenReturn(externalNodeAssociation);
        when(mockVersionTransformer.transform(projectImportMapper, externalNodeAssociation)).thenReturn(externalNodeAssociation);
        when(mockProjectImportPersister.createAssociation(externalNodeAssociation)).thenReturn(true);

        final ProjectImportResultsImpl projectImportResults = new ProjectImportResultsImpl(0, 0, 0, 0, null);
        VersionPersisterHandler versionPersisterHandler = new VersionPersisterHandler(mockProjectImportPersister, projectImportMapper, projectImportResults, null, new ExecutorForTests()) {
            NodeAssociationParser getNodeAssociationParser() {
                return mockNodeAssociationParser;
            }

            VersionTransformer getVersionTransformer() {
                return mockVersionTransformer;
            }
        };

        versionPersisterHandler.handleEntity(NODE_ASSOCIATION_ENTITY_NAME, null);
        versionPersisterHandler.handleEntity("NOTVersion", null);

        assertThat(projectImportResults.getErrors(), emptyIterable());
    }

    @Test
    public void testHandleErrorAddingAffectsVersion() throws Exception {
        projectImportMapper.getVersionMapper().registerOldValue("12", "Version 1");
        final ExternalNodeAssociation externalNodeAssociation = new ExternalNodeAssociation("34", "", "12", "", AFFECTS_VERSION_TYPE);

        final ExternalNodeAssociation transformedExternalNodeAssociation = new ExternalNodeAssociation("34", "", "12", "", AFFECTS_VERSION_TYPE);

        when(mockNodeAssociationParser.parse(null)).thenReturn(externalNodeAssociation);
        when(mockVersionTransformer.transform(projectImportMapper, externalNodeAssociation)).thenReturn(transformedExternalNodeAssociation);
        when(mockProjectImportPersister.createAssociation(transformedExternalNodeAssociation)).thenReturn(false);
        when(mockBackupSystemInformation.getIssueKeyForId("34")).thenReturn("TST-1");

        final ProjectImportResultsImpl projectImportResults = new ProjectImportResultsImpl(0, 0, 0, 0, new MockI18nBean());
        VersionPersisterHandler versionPersisterHandler = new VersionPersisterHandler(mockProjectImportPersister, projectImportMapper, projectImportResults, mockBackupSystemInformation, new ExecutorForTests()) {
            NodeAssociationParser getNodeAssociationParser() {
                return mockNodeAssociationParser;
            }

            VersionTransformer getVersionTransformer() {
                return mockVersionTransformer;
            }
        };

        versionPersisterHandler.handleEntity(NODE_ASSOCIATION_ENTITY_NAME, null);
        versionPersisterHandler.handleEntity("NOTVersion", null);

        assertThat(projectImportResults.getErrors(), contains("There was a problem saving affects version 'Version 1' for issue 'TST-1'."));
    }

    @Test
    public void testHandleErrorAddingFixVersion() throws Exception {
        projectImportMapper.getVersionMapper().registerOldValue("12", "Version 1");
        final ExternalNodeAssociation externalNodeAssociation = new ExternalNodeAssociation("34", "", "12", "", FIX_VERSION_TYPE);

        final ExternalNodeAssociation transformedExternalNodeAssociation = new ExternalNodeAssociation("34", "", "12", "", FIX_VERSION_TYPE);

        when(mockNodeAssociationParser.parse(null)).thenReturn(externalNodeAssociation);
        when(mockVersionTransformer.transform(projectImportMapper, externalNodeAssociation)).thenReturn(transformedExternalNodeAssociation);
        when(mockProjectImportPersister.createAssociation(transformedExternalNodeAssociation)).thenReturn(false);
        when(mockBackupSystemInformation.getIssueKeyForId("34")).thenReturn("TST-1");


        final ProjectImportResultsImpl projectImportResults = new ProjectImportResultsImpl(0, 0, 0, 0, new MockI18nBean());
        VersionPersisterHandler versionPersisterHandler = new VersionPersisterHandler(mockProjectImportPersister, projectImportMapper, projectImportResults, mockBackupSystemInformation, new ExecutorForTests()) {
            NodeAssociationParser getNodeAssociationParser() {
                return mockNodeAssociationParser;
            }

            VersionTransformer getVersionTransformer() {
                return mockVersionTransformer;
            }
        };

        versionPersisterHandler.handleEntity(NODE_ASSOCIATION_ENTITY_NAME, null);
        versionPersisterHandler.handleEntity("NOTVersion", null);

        assertThat(projectImportResults.getErrors(), contains("There was a problem saving fix version 'Version 1' for issue 'TST-1'."));
    }


    @Test
    public void testHandleErrorNullIssueId() throws Exception {
        projectImportMapper.getVersionMapper().registerOldValue("12", "Version 1");
        final ExternalNodeAssociation externalNodeAssociation = new ExternalNodeAssociation("34", "", "12", "", FIX_VERSION_TYPE);

        final ExternalNodeAssociation transformedExternalNodeAssociation = new ExternalNodeAssociation(null, "", "12", "", FIX_VERSION_TYPE);

        when(mockNodeAssociationParser.parse(null)).thenReturn(externalNodeAssociation);
        when(mockVersionTransformer.transform(projectImportMapper, externalNodeAssociation)).thenReturn(transformedExternalNodeAssociation);


        final BackupSystemInformation mockBackupSystemInformation = mock(BackupSystemInformation.class);
        when(mockBackupSystemInformation.getIssueKeyForId("34")).thenReturn("TST-1");


        final ProjectImportResultsImpl projectImportResults = new ProjectImportResultsImpl(0, 0, 0, 0, new MockI18nBean());
        VersionPersisterHandler versionPersisterHandler = new VersionPersisterHandler(mockProjectImportPersister, projectImportMapper, projectImportResults, mockBackupSystemInformation, null) {
            NodeAssociationParser getNodeAssociationParser() {
                return mockNodeAssociationParser;
            }

            VersionTransformer getVersionTransformer() {
                return mockVersionTransformer;
            }
        };

        versionPersisterHandler.handleEntity(NODE_ASSOCIATION_ENTITY_NAME, null);
        versionPersisterHandler.handleEntity("NOTVersion", null);

        assertThat(projectImportResults.getErrors(), emptyIterable());
    }

    @Test
    public void testHandleNonVersion() throws Exception {
        when(mockNodeAssociationParser.parse(null)).thenReturn(null);


        final ProjectImportResultsImpl projectImportResults = new ProjectImportResultsImpl(0, 0, 0, 0, null);
        VersionPersisterHandler versionPersisterHandler = new VersionPersisterHandler(mockProjectImportPersister, projectImportMapper, projectImportResults, null, null) {
            NodeAssociationParser getNodeAssociationParser() {
                return mockNodeAssociationParser;
            }

            VersionTransformer getVersionTransformer() {
                return mockVersionTransformer;
            }
        };

        versionPersisterHandler.handleEntity(NODE_ASSOCIATION_ENTITY_NAME, null);
        versionPersisterHandler.handleEntity("NOTVersion", null);

        verifyZeroInteractions(mockBackupSystemInformation, mockProjectImportPersister, mockVersionTransformer);
    }
}
