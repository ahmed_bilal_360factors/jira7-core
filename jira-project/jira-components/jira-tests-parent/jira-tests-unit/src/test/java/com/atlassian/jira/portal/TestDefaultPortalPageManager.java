package com.atlassian.jira.portal;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.fugue.Option;
import com.atlassian.gadgets.dashboard.Color;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.instrumentation.InstrumentationListenerManager;
import com.atlassian.jira.instrumentation.InstrumentationLogger;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.mock.component.MockComponentWorker;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.sharing.ShareManager;
import com.atlassian.jira.sharing.SharePermission;
import com.atlassian.jira.sharing.SharePermissionImpl;
import com.atlassian.jira.sharing.SharedEntity;
import com.atlassian.jira.sharing.SharedEntity.Identifier;
import com.atlassian.jira.sharing.SharedEntity.SharePermissions;
import com.atlassian.jira.sharing.index.MockSharedEntityIndexer;
import com.atlassian.jira.sharing.type.GlobalShareType;
import com.atlassian.jira.sharing.type.GroupShareType;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.plugin.ModuleCompleteKey;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.net.URI;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import static com.atlassian.gadgets.dashboard.Color.defaultColor;
import static com.atlassian.gadgets.dashboard.Layout.AAA;
import static com.atlassian.jira.portal.PortalPage.ENTITY_TYPE;
import static com.atlassian.jira.portal.PortalPage.id;
import static com.atlassian.jira.portal.PortalPage.name;
import static com.atlassian.jira.portal.PortalPage.portalPage;
import static com.atlassian.jira.sharing.SharedEntity.SharePermissions.GLOBAL;
import static com.atlassian.jira.sharing.SharedEntity.SharePermissions.PRIVATE;
import static java.util.Collections.singleton;
import static java.util.Collections.singletonList;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Unit tests for {@link com.atlassian.jira.issue.search.DefaultSearchRequestManager}
 *
 * @since v3.13
 */
@RunWith(MockitoJUnitRunner.class)
public class TestDefaultPortalPageManager {
    public static final long PAGE_ID = 1000L;

    private static final String PAGE_NAME = "pageName1";
    private static final Long PORTLET_ID = 664L;
    private static final Map<String, String> EMPTY_PREFS = ImmutableMap.of();
    private static final Option<URI> XML_URI = Option.some(URI.create("http://www.google.com"));
    private static final Option<ModuleCompleteKey> MODULE_KEY = Option.some(new ModuleCompleteKey("com.atlassian.gadgets.test:complete.key"));

    private final ApplicationUser user = new MockApplicationUser("admin");
    private final SharePermission perm2 = new SharePermissionImpl(GlobalShareType.TYPE, null, null);
    private final SharePermission perm1 = new SharePermissionImpl(GroupShareType.TYPE, "jira-user", null);
    private final PortalPage portalPage1 = PortalPage.id(1L).name("one").description("one description").owner(user).build();
    private final PortalPage portalPage2 = PortalPage.id(2L).name("two").description("two description").owner(user).build();
    private final PortalPage portalPage3 = PortalPage.id(3L).name("three").description("three description").owner(user).build();
    private final PortalPage defaultPage = PortalPage.id(23L).name("default").description("desc").systemDashboard().build();

    @Mock
    private PortalPageStore portalPageStore;
    @Mock
    private EventPublisher eventPublisher;
    @Mock
    private JiraAuthenticationContext authenticationContext;
    @Mock
    private PortletConfigurationManager portletConfigurationManager;
    @Mock
    private ShareManager shareManager;
    @Mock
    private PortalPageManager portalPageManager;

    @Before
    public void setUp() throws Exception {
        portalPageManager = new DefaultPortalPageManager(shareManager, portalPageStore, portletConfigurationManager,
                new MockSharedEntityIndexer(), eventPublisher, authenticationContext);
        ComponentAccessor.initialiseWorker(new MockComponentWorker());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetAllOwnedPortalPagesNullUser() {
        portalPageManager.getAllOwnedPortalPages((ApplicationUser) null);
    }

    @Test
    public void testGetAllOwnedPortalPagesNullReturned() {
        when(portalPageStore.getAllOwnedPortalPages(user)).thenReturn(null);

        final Collection results = portalPageManager.getAllOwnedPortalPages(user);
        assertNotNull(results);
        assertTrue(results.isEmpty());
    }

    @Test
    public void testGetAllOwnedPortalPagesEmptyList() {
        when(portalPageStore.getAllOwnedPortalPages(user)).thenReturn(Collections.<PortalPage>emptyList());

        final Collection results = portalPageManager.getAllOwnedPortalPages(user);
        assertNotNull(results);
        assertTrue(results.isEmpty());
    }

    @Test
    public void testGetAllOwnedPortalPagesNonEmptyList() {
        final ImmutableList<PortalPage> returnedList = ImmutableList.of(portalPage1, portalPage2, portalPage3);
        when(portalPageStore.getAllOwnedPortalPages(user)).thenReturn(returnedList);

        final SharePermissions portalPage2Perm = new SharePermissions(singleton(perm2));
        final SharePermissions portalPage1Perm = new SharePermissions(singleton(perm1));
        when(shareManager.getSharePermissions(portalPage1)).thenReturn(portalPage1Perm);
        when(shareManager.getSharePermissions(portalPage2)).thenReturn(portalPage2Perm);
        when(shareManager.getSharePermissions(portalPage3)).thenReturn(PRIVATE);

        final Collection results = portalPageManager.getAllOwnedPortalPages(user);
        assertNotNull(results);
        assertEquals(3, results.size());

        final Iterator iterator = results.iterator();
        PortalPage result = (PortalPage) iterator.next();
        assertEquals(portalPage1.getId(), result.getId());
        assertEquals(portalPage1Perm, result.getPermissions());

        result = (PortalPage) iterator.next();
        assertEquals(portalPage2.getId(), result.getId());
        assertEquals(portalPage2Perm, result.getPermissions());

        result = (PortalPage) iterator.next();
        assertEquals(portalPage3.getId(), result.getId());
        assertEquals(PRIVATE, result.getPermissions());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetPortalPageByNameNullUser() {
        portalPageManager.getPortalPageByName((ApplicationUser) null, PAGE_NAME);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetPortalPageByNameNullName() {
        portalPageManager.getPortalPageByName(user, null);
    }

    @Test
    public void test_getPortalPageByNameNullPortalPageReturnedByStore() {
        when(portalPageStore.getPortalPageByOwnerAndName(user, PAGE_NAME)).thenReturn(null);

        final PortalPage portalPage = portalPageManager.getPortalPageByName(user, PAGE_NAME);
        assertNull(portalPage);
    }

    @Test
    public void test_getPortalPageByNameWithPortalPageNoShares() {
        when(portalPageStore.getPortalPageByOwnerAndName(user, PAGE_NAME)).thenReturn(portalPage1);
        when(shareManager.getSharePermissions(portalPage1)).thenReturn(PRIVATE);

        final PortalPage portalPage = portalPageManager.getPortalPageByName(user, PAGE_NAME);
        assertNotNull(portalPage);
        assertEquals(portalPage1, portalPage);
        assertEquals(PRIVATE, portalPage.getPermissions());
    }

    @Test
    public void test_getPortalPageByNameWithPortalPageWithShares() {
        final SharePermissions permSet2 = new SharePermissions(singleton(perm2));
        when(portalPageStore.getPortalPageByOwnerAndName(user, PAGE_NAME)).thenReturn(portalPage1);
        when(shareManager.getSharePermissions(portalPage1)).thenReturn(permSet2);

        final PortalPage portalPage = portalPageManager.getPortalPageByName(user, PAGE_NAME);
        assertNotNull(portalPage);
        assertEquals(portalPage1.getId(), portalPage.getId());
        assertEquals(permSet2, portalPage.getPermissions());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetPortalPageNullId() {
        portalPageManager.getPortalPage(user, null);
    }

    @Test
    public void testGetPortalPageNullUserNoPerm() {
        when(portalPageStore.getPortalPage(portalPage1.getId())).thenReturn(portalPage1);
        when(shareManager.isSharedWith((ApplicationUser) null, portalPage1)).thenReturn(false);

        final PortalPage portalPage = portalPageManager.getPortalPage((ApplicationUser) null, portalPage1.getId());
        assertNull(portalPage);
    }

    @Test
    public void testGetPortalPageNullUserHasPerm() {
        when(portalPageStore.getPortalPage(portalPage2.getId())).thenReturn(portalPage2);
        when(shareManager.isSharedWith((ApplicationUser) null, portalPage2)).thenReturn(true);
        final SharePermissions permSet2 = new SharePermissions(singleton(perm2));
        when(shareManager.getSharePermissions(portalPage2)).thenReturn(permSet2);

        final PortalPage portalPage = portalPageManager.getPortalPage((ApplicationUser) null, portalPage2.getId());

        assertNotNull(portalPage);
        assertEquals(portalPage2.getId(), portalPage.getId());
        assertEquals(permSet2, portalPage.getPermissions());
    }

    @Test
    public void testGetPortalPageUserNoPerm() {
        when(portalPageStore.getPortalPage(portalPage1.getId())).thenReturn(portalPage1);
        when(shareManager.isSharedWith(user, portalPage1)).thenReturn(false);

        final PortalPage portalPage = portalPageManager.getPortalPage(user, portalPage1.getId());

        assertNull(portalPage);
    }

    @Test
    public void testGetPortalPageUserHasPerm() {
        when(portalPageStore.getPortalPage(portalPage1.getId())).thenReturn(portalPage1);
        when(shareManager.isSharedWith(user, portalPage1)).thenReturn(true);
        final SharePermissions permSet1 = new SharePermissions(singleton(perm1));
        when(shareManager.getSharePermissions(portalPage1)).thenReturn(permSet1);

        final PortalPage portalPage = portalPageManager.getPortalPage(user, portalPage1.getId());

        assertNotNull(portalPage);
        assertEquals(portalPage1.getId(), portalPage.getId());
        assertEquals(permSet1, portalPage.getPermissions());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateWithNullRequest() {

        portalPageManager.create(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateWithNoOwner() {

        final PortalPage portalPage = name("name").description("desc").owner((ApplicationUser) null).build();
        portalPageManager.create(portalPage);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateWithNoName() {

        final PortalPage portalPage = name(null).description("desc").owner(user).build();
        portalPageManager.create(portalPage);
    }

    @Test
    public void testCreateNoPerms() {
        when(portalPageStore.create(portalPage1)).thenReturn(portalPage1);
        when(shareManager.updateSharePermissions(portalPage1)).thenReturn(PRIVATE);

        final PortalPage portalPage = portalPageManager.create(portalPage1);
        assertNotNull(portalPage);
        assertEquals(portalPage1, portalPage);
        assertTrue(portalPage.getPermissions().isEmpty());
    }

    @Test
    public void testCreateHasPerms() {
        final SharePermissions sharePermissions = new SharePermissions(ImmutableSet.of(perm1, perm2));
        final PortalPage portalPage = PortalPage.portalPage(portalPage1).permissions(sharePermissions).build();
        when(portalPageStore.create(portalPage)).thenReturn(portalPage);
        when(shareManager.updateSharePermissions(portalPage)).thenReturn(sharePermissions);

        final PortalPage updatedPage = portalPageManager.create(portalPage);
        assertNotNull(updatedPage);
        assertEquals(portalPage, updatedPage);
        assertEquals(sharePermissions, updatedPage.getPermissions());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testUpdateWithNullRequest() {

        portalPageManager.update(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testUpdateWithNoOwner() {

        final PortalPage portalPage = name("name").description("desc").owner((ApplicationUser) null).build();
        portalPageManager.update(portalPage);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testUpdateWithNoName() {

        final PortalPage portalPage = name(null).description("desc").owner(user).build();
        portalPageManager.update(portalPage);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testUpdateWithNoId() {

        final PortalPage notIdRequest = name("user").description("desc").owner(user).build();
        portalPageManager.update(notIdRequest);
    }

    @Test
    public void testUpdateNoPerms() {

        final PortalPage portalPage = portalPage(portalPage3).permissions(PRIVATE).build();
        when(portalPageStore.update(portalPage)).thenReturn(portalPage);
        when(shareManager.updateSharePermissions(portalPage)).thenReturn(PRIVATE);

        final PortalPage updatedPage = portalPageManager.update(portalPage);
        assertNotNull(updatedPage);
        assertEquals(portalPage, updatedPage);
        assertTrue(updatedPage.getPermissions().isEmpty());
    }

    @Test
    public void testUpdateHasPerms() {
        final SharePermissions permSet1 = new SharePermissions(singleton(perm2));
        final PortalPage portalPage = portalPage(portalPage3).permissions(permSet1).build();

        when(portalPageStore.update(portalPage)).thenReturn(portalPage);

        when(shareManager.updateSharePermissions(portalPage)).thenReturn(permSet1);

        final PortalPage updatedPage = portalPageManager.update(portalPage);
        assertNotNull(updatedPage);
        assertEquals(portalPage, updatedPage);
        assertEquals(permSet1, updatedPage.getPermissions());
    }

    @Test
    public void testUserDefaultDashboardCanBeSaved() throws Exception {
        when(portalPageStore.update(defaultPage)).thenReturn(defaultPage);
        when(shareManager.updateSharePermissions(defaultPage)).thenReturn(PRIVATE);

        final PortalPage portalPage = portalPageManager.update(defaultPage);
        assertNotNull(portalPage);
        assertEquals(defaultPage, portalPage);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDeleteNull() {

        portalPageManager.delete(null);
    }

    @Test
    public void testDeleteValid() {
        final PortletConfiguration deletedPC = new PortletConfigurationImpl((long) 1, (long) 2, 2, 3, XML_URI, defaultColor(), EMPTY_PREFS, MODULE_KEY);
        when(portletConfigurationManager.getByPortalPage(portalPage1.getId())).thenReturn(singletonList(deletedPC));

        portalPageManager.delete(portalPage1.getId());

        verify(portletConfigurationManager).delete(deletedPC);
        verify(shareManager).deletePermissions(new Identifier(portalPage1.getId(), ENTITY_TYPE, (ApplicationUser) null));
        verify(portalPageStore).delete(portalPage1.getId());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testAdjustFavouriteCountNullEntity() {
        portalPageManager.adjustFavouriteCount(null, 1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testAdjustFavouriteCountInvalidEntity() {
        final SharedEntity entity = new Identifier(1L, SearchRequest.ENTITY_TYPE, user);
        portalPageManager.adjustFavouriteCount(entity, 1);
    }

    @Test
    public void testAdjustFavouriteCount() {
        when(portalPageStore.adjustFavouriteCount(portalPage1, 1)).thenReturn(portalPage1);
        when(shareManager.getSharePermissions(portalPage1)).thenReturn(PRIVATE);
        when(portalPageStore.getPortalPage(1L)).thenReturn(portalPage1);
        when(shareManager.getSharePermissions(portalPage1)).thenReturn(PRIVATE);
        portalPageManager.adjustFavouriteCount(portalPage1, 1);

        final PortalPage returnedPage = portalPageManager.getPortalPageById(portalPage1.getId());
        assertThat(returnedPage, is(portalPage1));
    }

    @Test
    public void testAdjustFavouriteCountNegative() {
        when(portalPageStore.adjustFavouriteCount(portalPage1, -3)).thenReturn(portalPage1);
        when(shareManager.getSharePermissions(portalPage1)).thenReturn(PRIVATE);
        when(portalPageStore.getPortalPage(portalPage1.getId())).thenReturn(portalPage1);
        when(shareManager.getSharePermissions(portalPage1)).thenReturn(PRIVATE);
        portalPageManager.adjustFavouriteCount(portalPage1, -3);

        final PortalPage returnedPage = portalPageManager.getPortalPageById(portalPage1.getId());
        assertThat(returnedPage, is(portalPage1));
    }

    @Test(expected = IllegalArgumentException.class)
    public void test_getPortalPageById_NullId() throws Exception {
        portalPageManager.getPortalPageById(null);
    }

    @Test
    public void test_getPortalPageById() throws Exception {
        final SharePermissions sharePermissions = new SharePermissions(singleton(perm1));
        when(portalPageStore.getPortalPage(portalPage1.getId())).thenReturn(portalPage1);
        when(shareManager.getSharePermissions(portalPage1)).thenReturn(sharePermissions);
        final PortalPage portalPage = portalPageManager.getPortalPageById(portalPage1.getId());
        assertEquals(portalPage1.getId(), portalPage.getId());
        assertEquals(sharePermissions, portalPage.getPermissions());
    }

    @Test
    public void test_getPortalPageById_NullReturned() throws Exception {
        when(portalPageStore.getPortalPage(portalPage1.getId())).thenReturn(null);
        final PortalPage portalPage = portalPageManager.getPortalPageById(portalPage1.getId());
        assertNull(portalPage);
    }

    @Test
    public void test_getSystemDefaultPortalPage() throws Exception {
        final SharePermissions sharePermissions = PRIVATE;

        when(portalPageStore.getSystemDefaultPortalPage()).thenReturn(portalPage1);
        when(shareManager.getSharePermissions(portalPage1)).thenReturn(sharePermissions);
        final PortalPage portalPage = portalPageManager.getSystemDefaultPortalPage();
        assertEquals(portalPage1.getId(), portalPage.getId());
        assertEquals(sharePermissions, portalPage.getPermissions());
    }

    /**
     * Make sure the configuration is not saved when it does not exist on the page.
     */
    @Test(expected = IllegalStateException.class)
    public void testSavePortalPagePortletConfigurationNullConfiguration() {
        new PortletConfigurationImpl(PORTLET_ID, PAGE_ID, 1, 3, Option.<URI>none(), defaultColor(), EMPTY_PREFS, Option.<ModuleCompleteKey>none());
        final Long dashboardPageId = 666L;
        final PortletConfiguration expectedPC = new PortletConfigurationImpl(PORTLET_ID, dashboardPageId, 1, 3, XML_URI, defaultColor(), EMPTY_PREFS, MODULE_KEY);

        when(portletConfigurationManager.getByPortletId(PORTLET_ID)).thenReturn(null);
        portalPageManager.saveLegacyPortletConfiguration(expectedPC);
    }

    /**
     * Make sure that PortalPageManager.getSharedEntity calls through to correct method.
     */
    @Test
    public void testGetSharedEntityId() {
        final AtomicInteger getPortalPageByIdCount = new AtomicInteger(0);
        final PortalPageManager portalPageManager = new DefaultPortalPageManager(shareManager, portalPageStore,
                portletConfigurationManager, new MockSharedEntityIndexer(), eventPublisher, authenticationContext) {
            @Override
            public PortalPage getPortalPageById(final Long portalPageId) {
                assertEquals(portalPage1.getId(), portalPageId);

                getPortalPageByIdCount.incrementAndGet();

                return portalPage2;
            }
        };

        final SharedEntity actualPortalPage = portalPageManager.getSharedEntity(portalPage1.getId());

        assertEquals(1, getPortalPageByIdCount.get());
        assertEquals(portalPage2, actualPortalPage);
    }

    /**
     * Make sure the method works as expected when the id is null.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testGetSharedEntityIdNull() {

        portalPageManager.getSharedEntity(null);
    }

    /**
     * Make sure that PortalPageManager.getSharedEntity calls the correct method.
     */
    @Test
    public void testGetSharedEntityUser() {
        final AtomicInteger getPortalPageCount = new AtomicInteger(0);
        final PortalPageManager portalPageManager = new DefaultPortalPageManager(shareManager, portalPageStore,
                portletConfigurationManager, new MockSharedEntityIndexer(), eventPublisher, authenticationContext) {
            @Override
            public PortalPage getPortalPage(final ApplicationUser actualUser, final Long id) {
                assertEquals(portalPage1.getId(), id);
                assertEquals(actualUser, user);
                getPortalPageCount.incrementAndGet();

                return portalPage3;
            }
        };

        final SharedEntity actualPortalPage = portalPageManager.getSharedEntity(user, portalPage1.getId());

        assertEquals(1, getPortalPageCount.get());
        assertSame(portalPage3, actualPortalPage);
    }

    /**
     * Make sure the method works as expected when the id is null.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testGetSharedEntityUserNullId() {

        portalPageManager.getSharedEntity(null, null);
    }

    /**
     * Make sure that PortalPageManager.getSharedEntity calls the correct method.
     */
    @Test
    public void testGetSharedEntityUserAnonymous() {
        final AtomicInteger getPortalPageCount = new AtomicInteger(0);
        final PortalPageManager portalPageManager = new DefaultPortalPageManager(shareManager, portalPageStore,
                portletConfigurationManager, new MockSharedEntityIndexer(), eventPublisher, authenticationContext) {
            @Override
            public PortalPage getPortalPage(final ApplicationUser inputUser, final Long id) {
                assertEquals(portalPage2.getId(), id);
                assertNull(inputUser);
                getPortalPageCount.incrementAndGet();

                return portalPage1;
            }
        };

        final SharedEntity actualPortalPage = portalPageManager.getSharedEntity(null, portalPage2.getId());

        assertEquals(1, getPortalPageCount.get());
        assertSame(portalPage1, actualPortalPage);
    }

    /**
     * Make sure that method throws an error with a null SharedEntity.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testHasPermissionToUserNullSharedEntity() {

        portalPageManager.hasPermissionToUse(null, null);
    }

    /**
     * Make sure the user has permission to use the system default.
     */
    @Test
    public void testHasPermissionsToUseSystemDefault() {

        final boolean actualResult = portalPageManager.hasPermissionToUse(user, defaultPage);

        assertTrue(actualResult);
    }

    /**
     * Make sure method returns true when the user is able to see the shared entity.
     */
    @Test
    public void testHasPermissionsToUseGood() {
        when(shareManager.isSharedWith((ApplicationUser) null, portalPage1)).thenReturn(true);

        final boolean actualResult = portalPageManager.hasPermissionToUse(null, portalPage1);

        assertTrue(actualResult);
    }

    /**
     * Make sure method returns false when the user is not able to see the shared entity.
     */
    @Test
    public void testHasPermissionsToUseBad() {
        when(shareManager.isSharedWith(user, portalPage2)).thenReturn(false);
        final boolean actualResult = portalPageManager.hasPermissionToUse(user, portalPage2);

        assertFalse(actualResult);
    }

    @Test
    public void testGetPortletConfigurationsNone() {
        when(shareManager.getSharePermissions(portalPage1)).thenReturn(GLOBAL);
        when(portalPageStore.getPortalPage(portalPage1.getId())).thenReturn(portalPage1);
        when(portletConfigurationManager.getByPortalPage(portalPage1.getId())).thenReturn(Collections.<PortletConfiguration>emptyList());

        final List<List<PortletConfiguration>> configs = portalPageManager.getPortletConfigurations(portalPage1.getId());
        assertTrue(configs.isEmpty());
    }

    @Test
    public void testGetPortletConfigurations() {
        final PortalPage portalPage = id(PAGE_ID).name("Mine").layout(AAA).build();
        final PortletConfiguration pc1 = new PortletConfigurationImpl(10000L, PAGE_ID, 0, 0, XML_URI, Color.defaultColor(), EMPTY_PREFS, MODULE_KEY);
        final PortletConfiguration pc2 = new PortletConfigurationImpl(10010L, PAGE_ID, 0, 1, XML_URI, Color.defaultColor(), EMPTY_PREFS, MODULE_KEY);
        final PortletConfiguration pc3 = new PortletConfigurationImpl(10030L, PAGE_ID, 2, 0, XML_URI, Color.defaultColor(), EMPTY_PREFS, MODULE_KEY);
        final PortletConfiguration pc4 = new PortletConfigurationImpl(10040L, PAGE_ID, 2, 1, XML_URI, Color.defaultColor(), EMPTY_PREFS, MODULE_KEY);
        final PortletConfiguration pc5 = new PortletConfigurationImpl(10050L, PAGE_ID, 2, 5, XML_URI, Color.defaultColor(), EMPTY_PREFS, MODULE_KEY);

        when(shareManager.getSharePermissions(portalPage)).thenReturn(GLOBAL);
        when(portalPageStore.getPortalPage(PAGE_ID)).thenReturn(portalPage);
        when(portletConfigurationManager.getByPortalPage(portalPage.getId())).thenReturn(ImmutableList.of(pc4, pc1, pc2, pc5, pc3));

        final List<List<PortletConfiguration>> configs = portalPageManager.getPortletConfigurations(PAGE_ID);
        List<? extends List<PortletConfiguration>> expected = ImmutableList.of(
                ImmutableList.of(pc1, pc2),
                ImmutableList.<PortletConfiguration>of(),
                ImmutableList.of(pc3, pc4, pc5)
        );

        assertEquals(expected, configs);
    }
}
