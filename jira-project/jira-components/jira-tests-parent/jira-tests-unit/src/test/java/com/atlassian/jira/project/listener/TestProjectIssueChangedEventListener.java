package com.atlassian.jira.project.listener;

import com.atlassian.jira.event.issue.IssueEvent;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.changehistory.ChangeHistoryManager;
import com.atlassian.jira.issue.history.ChangeItemBean;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectChangedTimeStore;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.sql.Timestamp;

import static com.atlassian.jira.event.type.EventType.ISSUE_COMMENTED_ID;
import static com.atlassian.jira.event.type.EventType.ISSUE_CREATED_ID;
import static com.atlassian.jira.event.type.EventType.ISSUE_MOVED_ID;
import static com.google.common.collect.Lists.newArrayList;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestProjectIssueChangedEventListener {
    private final static long SOURCE_PROJECT_ID = 100L;
    private final static long DESTINATION_PROJECT_ID = 101L;
    private final static long PROJECT_ID = 102L;

    @Mock
    private ProjectChangedTimeStore projectChangedTimeStore;

    @Mock
    private ChangeHistoryManager changeHistoryManager;

    private ProjectIssueChangedEventListener projectIssueChangedEventListener;

    @Before
    public void setup() {
        projectIssueChangedEventListener = new ProjectIssueChangedEventListenerImpl(
                projectChangedTimeStore,
                changeHistoryManager
        );
    }

    @Test
    public void shouldSetProjectChangedTimeForAllowedEventType() {
        projectIssueChangedEventListener.listenForIssueChangedEvent(issueEventOfTypeForProject(ISSUE_CREATED_ID, PROJECT_ID));

        verify(projectChangedTimeStore).updateOrAddIssueChangedTime(eq(PROJECT_ID), isA(Timestamp.class));
    }

    @Test
    public void shouldNotSetProjectChangedTimeForNotAllowedEventType() {
        projectIssueChangedEventListener.listenForIssueChangedEvent(issueEventOfTypeForProject(ISSUE_COMMENTED_ID, PROJECT_ID));

        verify(projectChangedTimeStore, never()).updateOrAddIssueChangedTime(eq(PROJECT_ID), isA(Timestamp.class));
    }

    @Test
    public void shouldSetProjectChangedTimeOfSourceProjectAndDestinationProjectWhenAnIssueHasBeenMoved() {
        IssueEvent moveIssueEvent = issueEventOfTypeForProject(ISSUE_MOVED_ID, DESTINATION_PROJECT_ID);
        ChangeItemBean projectFieldChange = projectFieldChangedFrom(SOURCE_PROJECT_ID);
        changesToProjectFieldOfIssueAre(moveIssueEvent.getIssue(), projectFieldChange);

        projectIssueChangedEventListener.listenForIssueChangedEvent(moveIssueEvent);

        verify(projectChangedTimeStore).updateOrAddIssueChangedTime(eq(DESTINATION_PROJECT_ID), isA(Timestamp.class));
        verify(projectChangedTimeStore).updateOrAddIssueChangedTime(eq(SOURCE_PROJECT_ID), isA(Timestamp.class));
    }

    private IssueEvent issueEventOfTypeForProject(long eventTypeId, long projectId) {
        return new IssueEvent(anIssueWithProjectId(projectId), null, null, eventTypeId);
    }

    private Issue anIssueWithProjectId(long projectId) {
        Issue issue = mock(Issue.class);
        Project project = new MockProject(projectId);
        when(issue.getProjectObject()).thenReturn(project);

        return issue;
    }

    private ChangeItemBean projectFieldChangedFrom(long previousProjectId) {
        ChangeItemBean changeItem = mock(ChangeItemBean.class);
        when(changeItem.getFrom()).thenReturn(String.valueOf(previousProjectId));

        return changeItem;
    }

    private void changesToProjectFieldOfIssueAre(Issue issue, ChangeItemBean... changes) {
        when(changeHistoryManager.getChangeItemsForField(issue, "project")).thenReturn(newArrayList(changes));
    }
}
