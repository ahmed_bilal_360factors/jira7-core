package com.atlassian.jira.web.action.admin;

import com.atlassian.jira.junit.rules.JiraWebActionSupportDependencies;
import com.atlassian.jira.junit.rules.MockitoContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.notification.NotificationSchemeManager;
import com.atlassian.mail.queue.MailQueue;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import webwork.action.ServletActionContext;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class TestMailQueueAdmin {
    @Rule
    public final MockitoContainer mockContainer = MockitoMocksInContainer.rule(this);
    @Rule
    public TestRule webActionSupport = JiraWebActionSupportDependencies.build(mockContainer);

    @Before
    public void setUp() throws Exception {
        ServletActionContext.setRequest(mock(HttpServletRequest.class));
        ServletActionContext.setResponse(mock(HttpServletResponse.class));
    }

    @Test
    public void shouldNotFlushTheQueueIfTheFlushParameterHasNotBeenSet() throws Exception {
        final MailQueue mockMailQueue = mock(MailQueue.class);
        final MailQueueAdmin mailQueueAdminAction =
                new MailQueueAdmin(mockMailQueue, mock(NotificationSchemeManager.class));

        mailQueueAdminAction.execute();
        verify(mockMailQueue, never()).sendBuffer();
    }

    @Test
    public void shouldFlushTheQueueIfTheFlushParameterHasBeenSet() throws Exception {
        final MailQueue mockMailQueue = mock(MailQueue.class);

        final MailQueueAdmin mailQueueAdminAction =
                new MailQueueAdmin(mockMailQueue, mock(NotificationSchemeManager.class));
        mailQueueAdminAction.setFlush(true);

        mailQueueAdminAction.execute();
        verify(mockMailQueue, times(1)).sendBuffer();
    }
}
