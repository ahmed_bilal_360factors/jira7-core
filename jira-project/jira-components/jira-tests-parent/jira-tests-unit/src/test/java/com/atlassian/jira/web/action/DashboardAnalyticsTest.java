package com.atlassian.jira.web.action;

import com.atlassian.gadgets.DashboardItemState;
import com.atlassian.gadgets.GadgetId;
import com.atlassian.gadgets.GadgetState;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.dashboard.analytics.DashboardViewAnalyticsEvent;
import com.atlassian.jira.mock.component.MockComponentWorker;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class DashboardAnalyticsTest {
    private List<List<DashboardItemState>> columns;
    private List<List<DashboardItemState>> filteredColumns;
    private Long dashboardId = 1L;

    @InjectMocks
    private Dashboard dashboard;

    @BeforeClass
    public static void beforeClass() {
        final MockComponentWorker componentAccessorWorker = new MockComponentWorker();
        ComponentAccessor.initialiseWorker(componentAccessorWorker);
    }

    private List<List<DashboardItemState>> createExampleDashboardColumns() {
        List<List<DashboardItemState>> columns = new ArrayList<List<DashboardItemState>>();
        List<DashboardItemState> columnOne = new ArrayList<DashboardItemState>();
        columnOne.add(GadgetState.gadget(GadgetId.valueOf("100")).specUri(URI.create("rest/gadgets/1.0/g/someothergadget.xml")).build());
        columnOne.add(GadgetState.gadget(GadgetId.valueOf("100")).specUri(URI.create("rest/gadgets/1.0/g/someothergadget2.xml")).build());
        columnOne.add(GadgetState.gadget(GadgetId.valueOf("100")).specUri(URI.create("http://www.atlassian.com/stream.xml")).build());
        List<DashboardItemState> columnTwo = new ArrayList<DashboardItemState>();
        columnTwo.add(GadgetState.gadget(GadgetId.valueOf("100")).specUri(URI.create("rest/gadgets/1.0/g/com.atlassian.jira.gadgets:login-gadget/login.xml")).build());
        columnTwo.add(GadgetState.gadget(GadgetId.valueOf("100")).specUri(URI.create("http://www.atlassian.com/stream3.xml")).build());
        columns.add(columnOne);
        columns.add(columnTwo);

        return columns;
    }

    @Before
    public void setUp() {
        columns = createExampleDashboardColumns();

        filteredColumns = createExampleDashboardColumns();
        filteredColumns.get(0).remove(1);
        filteredColumns.get(1).remove(0);
    }

    @Test
    public void testEventHasCorrectNumberOfFilteredItems() throws Exception {
        DashboardViewAnalyticsEvent event = dashboard.generateDashboardAnalyticEvent(dashboardId, columns, filteredColumns);

        assertEquals(1L, (long) event.getDashboardId());
        assertEquals(2L, (long) event.getHiddenItemsCount());
        assertEquals(3L, (long) event.getShownItemsCount());
    }

    @Test
    public void testEventWhenNoItemsFiltered() throws Exception {
        filteredColumns = columns;

        DashboardViewAnalyticsEvent event = dashboard.generateDashboardAnalyticEvent(dashboardId, columns, filteredColumns);

        assertEquals(1L, (long) event.getDashboardId());
        assertEquals(0L, (long) event.getHiddenItemsCount());
        assertEquals(5L, (long) event.getShownItemsCount());
    }
}