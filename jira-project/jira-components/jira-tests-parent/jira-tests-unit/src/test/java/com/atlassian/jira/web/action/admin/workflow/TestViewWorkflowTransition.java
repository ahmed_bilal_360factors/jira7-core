package com.atlassian.jira.web.action.admin.workflow;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.web.action.admin.workflow.analytics.WorkflowTransitionTabEvent;
import com.atlassian.jira.workflow.JiraWorkflow;
import com.atlassian.jira.workflow.tabs.WorkflowTransitionTabProvider;
import com.atlassian.plugin.web.descriptors.WebPanelModuleDescriptor;
import com.google.common.collect.Lists;
import com.opensymphony.workflow.loader.ActionDescriptor;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class TestViewWorkflowTransition {
    @Mock
    private EventPublisher eventPublisher;

    @Mock
    private WorkflowTransitionTabProvider workflowTransitionTabProvider;

    @InjectMocks
    private ViewWorkflowTransition viewWorkflowTransition;

    @Mock
    private WebPanelModuleDescriptor validatorTabModule;
    private WorkflowTransitionTabProvider.WorkflowTransitionTab validatorTab;
    private static String validatorKey = "validator";

    @Mock
    private WebPanelModuleDescriptor postFunctionTabModule;
    private WorkflowTransitionTabProvider.WorkflowTransitionTab postFunctionTab;
    private static String postFunctionKey = "post";

    @Before
    public void setUp() {
        mockTransitionTabs();
    }

    @Test
    public void shouldRaiseAnalyticsWhenTabIsLoadedInSetDescriptorTab() {
        viewWorkflowTransition.setDescriptorTab("triggers");
        verify(eventPublisher).publish(any(WorkflowTransitionTabEvent.class));
    }

    @Test
    public void defaultsToFirstTabIfNoDescriptorTab() {
        mockGetTabs(validatorTab, postFunctionTab);
        String descriptorTab = viewWorkflowTransition.getDescriptorTab();
        assertEquals("Should have defaulted to first tab if no descriptor set", validatorKey, descriptorTab);
        verify(eventPublisher, never()).publish(any());
    }

    @Test
    public void noTabsReturnsEmptyString() {
        mockGetTabs();

        String descriptorTab = viewWorkflowTransition.getDescriptorTab();
        assertEquals("Should have defaulted to first tab", "", descriptorTab);
        verify(eventPublisher, never()).publish(any());
    }

    @Test
    public void setTabFindsTab() {
        mockGetTabs(validatorTab, postFunctionTab);
        viewWorkflowTransition.setDescriptorTab(postFunctionKey);
        String descriptorTab = viewWorkflowTransition.getDescriptorTab();

        assertEquals("Should have found the descriptor key for existing tab", postFunctionKey, descriptorTab);
        verify(eventPublisher, times(1)).publish(any(WorkflowTransitionTabEvent.class));
    }

    @Test
    public void setTabCannotFindTabReturnsFirst() {
        mockGetTabs(validatorTab, postFunctionTab);
        viewWorkflowTransition.setDescriptorTab("InvalidTab");
        String descriptorTab = viewWorkflowTransition.getDescriptorTab();

        assertEquals("Should have returned first key for non-existing tab", validatorKey, descriptorTab);
        verify(eventPublisher, times(1)).publish(any(WorkflowTransitionTabEvent.class));
    }

    private void mockTransitionTabs() {
        validatorTab = new WorkflowTransitionTabProvider.WorkflowTransitionTab("label", "2", validatorTabModule);
        when(validatorTabModule.getKey()).thenReturn(validatorKey);
        postFunctionTab = new WorkflowTransitionTabProvider.WorkflowTransitionTab("label", "3", postFunctionTabModule);
        when(postFunctionTabModule.getKey()).thenReturn(postFunctionKey);
    }

    private void mockGetTabs(WorkflowTransitionTabProvider.WorkflowTransitionTab... tabs) {
        when(workflowTransitionTabProvider.getTabs(any(ActionDescriptor.class), any(JiraWorkflow.class))).thenReturn(Lists.newArrayList(tabs));
    }
}
