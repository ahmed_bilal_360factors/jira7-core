package com.atlassian.jira.issue.context.persistence;

import com.atlassian.cache.memory.MemoryCacheManager;
import com.atlassian.jira.issue.context.JiraContextNode;
import com.atlassian.jira.issue.context.ProjectContext;
import com.atlassian.jira.issue.fields.config.FieldConfigScheme;
import com.atlassian.jira.junit.rules.MockitoContainer;
import com.atlassian.jira.mock.ofbiz.MockGenericValue;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.util.collect.MapBuilder;
import com.google.common.collect.Maps;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;

import java.util.Arrays;
import java.util.Collections;
import java.util.Map;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyMap;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

public class TestFieldConfigContextPersisterWorker {
    @Rule
    public MockitoContainer mockitoContainer = new MockitoContainer(this);

    @Mock
    protected OfBizDelegator ofBizDelegator;
    @Mock
    protected ProjectManager projectManager;

    protected FieldConfigContextPersisterWorker worker;

    private final static Long TEST_ID = 1L;

    @Before
    public void setUp() throws Exception {
        worker = new FieldConfigContextPersisterWorker(ofBizDelegator, projectManager, new MemoryCacheManager());
    }

    @Test
    public void storeNewValue() {
        FieldConfigScheme mockScheme = mock(FieldConfigScheme.class);
        when(mockScheme.getId()).thenReturn(TEST_ID);
        JiraContextNode mockContextNode = mock(JiraContextNode.class);
        when(mockContextNode.appendToParamsMap(eq(Collections.emptyMap()))).thenReturn(getDefaultMap());

        worker.store("test_field_id", mockContextNode, mockScheme);

        verify(ofBizDelegator).findByAnd(eq(FieldConfigContextPersisterWorker.ENTITY_TABLE_NAME), anyMap());
        verify(ofBizDelegator).createValue(eq(FieldConfigContextPersisterWorker.ENTITY_TABLE_NAME), eq(MapBuilder
                .newBuilder(FieldConfigContextPersisterWorker.ENTITY_KEY, (Object)"test_field_id")
                .add("test", "value")
                .add(FieldConfigContextPersisterWorker.ENTITY_SCHEME_ID, TEST_ID)
                .toMap()));
        verifyNoMoreInteractions(ofBizDelegator);
    }

    @Test
    public void replaceOldValue() {
        FieldConfigScheme mockScheme = mock(FieldConfigScheme.class);
        when(mockScheme.getId()).thenReturn(TEST_ID);
        JiraContextNode mockContextNode = mock(JiraContextNode.class);
        when(mockContextNode.appendToParamsMap(eq(Collections.emptyMap()))).thenReturn(getDefaultMap());

        when(ofBizDelegator.findByAnd(eq(FieldConfigContextPersisterWorker.ENTITY_TABLE_NAME), anyMap())).thenReturn(
                Arrays.asList(new MockGenericValue(FieldConfigContextPersisterWorker.ENTITY_TABLE_NAME,
                        MapBuilder.build(FieldConfigContextPersisterWorker.ENTITY_SCHEME_ID, TEST_ID)))
        );

        worker.store("test_field_id", mockContextNode, mockScheme);

        verify(ofBizDelegator).findByAnd(eq(FieldConfigContextPersisterWorker.ENTITY_TABLE_NAME), anyMap());
        verify(ofBizDelegator).removeByAnd(eq(FieldConfigContextPersisterWorker.ENTITY_TABLE_NAME), anyMap());
        verify(ofBizDelegator).createValue(eq(FieldConfigContextPersisterWorker.ENTITY_TABLE_NAME), eq(MapBuilder
                .newBuilder(FieldConfigContextPersisterWorker.ENTITY_KEY, (Object)"test_field_id")
                .add("test", "value")
                .add(FieldConfigContextPersisterWorker.ENTITY_SCHEME_ID, TEST_ID)
                .toMap()));
        verifyNoMoreInteractions(ofBizDelegator);
    }

    @Test
    public void getAllContextsForConfigSchemeWhereItDoesNotExist() {
        FieldConfigScheme mockScheme = mock(FieldConfigScheme.class);
        when(mockScheme.getId()).thenReturn(TEST_ID);

        when(ofBizDelegator.findAll(eq(FieldConfigContextPersisterWorker.ENTITY_TABLE_NAME))).thenReturn(Collections.emptyList());

        assertThat(worker.getAllContextsForConfigScheme(mockScheme), empty());
        verify(ofBizDelegator).findAll(eq(FieldConfigContextPersisterWorker.ENTITY_TABLE_NAME));
        verifyNoMoreInteractions(ofBizDelegator);
    }

    @Test
    public void getAllContextsForConfigSchemeWhereNothingPassesFilter() {
        FieldConfigScheme mockScheme = mock(FieldConfigScheme.class);
        when(mockScheme.getId()).thenReturn(TEST_ID);

        when(ofBizDelegator.findAll(eq(FieldConfigContextPersisterWorker.ENTITY_TABLE_NAME))).thenReturn(Arrays.asList(
                new MockGenericValue("test")
        ));

        assertThat(worker.getAllContextsForConfigScheme(mockScheme), empty());
        verify(ofBizDelegator).findAll(eq(FieldConfigContextPersisterWorker.ENTITY_TABLE_NAME));
        verifyNoMoreInteractions(ofBizDelegator);
    }

    @Test
    public void getAllContextsForConfigSchemeOneValueReturned() {
        FieldConfigScheme mockScheme = mock(FieldConfigScheme.class);
        when(mockScheme.getId()).thenReturn(TEST_ID);

        when(projectManager.getProjectObj(any())).thenReturn(new MockProject());
        when(ofBizDelegator.findAll(eq(FieldConfigContextPersisterWorker.ENTITY_TABLE_NAME))).thenReturn(Arrays.asList(
                new MockGenericValue("test", MapBuilder.build(FieldConfigContextPersisterWorker.ENTITY_PROJECT, 2L,
                        FieldConfigContextPersisterWorker.ENTITY_SCHEME_ID, TEST_ID)),
                new MockGenericValue("test", MapBuilder.build(FieldConfigContextPersisterWorker.ENTITY_PROJECT, 2L,
                        FieldConfigContextPersisterWorker.ENTITY_SCHEME_ID, 2L))
        ));

        assertThat(worker.getAllContextsForConfigScheme(mockScheme), containsInAnyOrder(new ProjectContext(2L, projectManager)));
        verify(ofBizDelegator).findAll(eq(FieldConfigContextPersisterWorker.ENTITY_TABLE_NAME));
        verifyNoMoreInteractions(ofBizDelegator);
    }

    @Test
    public void getAllContextsForConfigSchemeMultipleValuesReturned() {
        FieldConfigScheme mockScheme = mock(FieldConfigScheme.class);
        when(mockScheme.getId()).thenReturn(TEST_ID);

        when(projectManager.getProjectObj(any())).thenReturn(new MockProject());
        when(ofBizDelegator.findAll(eq(FieldConfigContextPersisterWorker.ENTITY_TABLE_NAME))).thenReturn(Arrays.asList(
                new MockGenericValue("test", MapBuilder.build(FieldConfigContextPersisterWorker.ENTITY_PROJECT, 2L,
                        FieldConfigContextPersisterWorker.ENTITY_SCHEME_ID, TEST_ID)),
                new MockGenericValue("test", MapBuilder.build(FieldConfigContextPersisterWorker.ENTITY_PROJECT, 3L,
                        FieldConfigContextPersisterWorker.ENTITY_SCHEME_ID, TEST_ID)),
                new MockGenericValue("test", MapBuilder.build(FieldConfigContextPersisterWorker.ENTITY_PROJECT, 4L,
                        FieldConfigContextPersisterWorker.ENTITY_SCHEME_ID, TEST_ID)),
                new MockGenericValue("test", MapBuilder.build(FieldConfigContextPersisterWorker.ENTITY_PROJECT, 2L,
                        FieldConfigContextPersisterWorker.ENTITY_SCHEME_ID, 2L))
        ));

        assertThat(worker.getAllContextsForConfigScheme(mockScheme), containsInAnyOrder(new ProjectContext(2L, projectManager),
                new ProjectContext(3L, projectManager), new ProjectContext(4L, projectManager)));
        verify(ofBizDelegator).findAll(eq(FieldConfigContextPersisterWorker.ENTITY_TABLE_NAME));
        verifyNoMoreInteractions(ofBizDelegator);
    }

    @Test
    public void noValueInDatabaseReturnsNull() {
        JiraContextNode mockContextNode = mock(JiraContextNode.class);
        when(mockContextNode.appendToParamsMap(anyMap())).thenReturn(Maps.newHashMap());
        assertThat(worker.retrieve(mockContextNode, "key"), nullValue());
    }

    @Test
    public void returnsCorrectSingleValue() {
        JiraContextNode mockContextNode = mock(JiraContextNode.class);
        when(mockContextNode.appendToParamsMap(anyMap())).thenReturn(Maps.newHashMap());
        when(ofBizDelegator.findByAnd(eq(FieldConfigContextPersisterWorker.ENTITY_TABLE_NAME), anyMap())).thenReturn(
                Arrays.asList(new MockGenericValue(FieldConfigContextPersisterWorker.ENTITY_TABLE_NAME,
                        MapBuilder.build(FieldConfigContextPersisterWorker.ENTITY_SCHEME_ID, TEST_ID)))
        );
        assertEquals(TEST_ID, worker.retrieve(mockContextNode, "key"));
    }

    @Test
    public void removeHitsDelegator() {
        JiraContextNode mockContextNode = mock(JiraContextNode.class);
        when(mockContextNode.appendToParamsMap(anyMap())).thenReturn(Maps.newHashMap());
        worker.remove(mockContextNode, "key");
        verify(ofBizDelegator).removeByAnd(eq(FieldConfigContextPersisterWorker.ENTITY_TABLE_NAME), anyMap());
        verifyNoMoreInteractions(ofBizDelegator);
    }

    @Test
    public void removeContextsForProject() {
        Project mockProject = mock(Project.class);
        when(mockProject.getId()).thenReturn(TEST_ID);
        worker.removeContextsForProject(mockProject);
        verify(ofBizDelegator).removeByAnd(eq(FieldConfigContextPersisterWorker.ENTITY_TABLE_NAME),
                eq(Collections.singletonMap(FieldConfigContextPersisterWorker.ENTITY_PROJECT, TEST_ID)));
        verifyNoMoreInteractions(ofBizDelegator);
    }

    @Test
    public void removeContextsForFieldConfigScheme() {
        FieldConfigScheme mockScheme = mock(FieldConfigScheme.class);
        when(mockScheme.getId()).thenReturn(TEST_ID);
        worker.removeContextsForConfigScheme(mockScheme);
        verify(ofBizDelegator).removeByAnd(eq(FieldConfigContextPersisterWorker.ENTITY_TABLE_NAME),
                eq(Collections.singletonMap(FieldConfigContextPersisterWorker.ENTITY_SCHEME_ID, TEST_ID)));
        verifyNoMoreInteractions(ofBizDelegator);
    }

    private static Map<String, Object> getDefaultMap() {
        return MapBuilder.build("test", "value");
    }
}
