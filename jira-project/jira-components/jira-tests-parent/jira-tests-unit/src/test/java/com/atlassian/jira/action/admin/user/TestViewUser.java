package com.atlassian.jira.action.admin.user;

import com.atlassian.crowd.embedded.api.CrowdDirectoryService;
import com.atlassian.crowd.embedded.api.CrowdService;
import com.atlassian.jira.bc.user.UserApplicationHelper;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.UserPropertyManager;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.web.action.admin.user.ViewUser;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.mockito.Mock;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

/**
 * @since 7.1
 */
public class TestViewUser {

    @Rule
    public RuleChain mockitoMocksInContainer = MockitoMocksInContainer.forTest(this);

    private static String USER_NAME = "freddie";

    private ViewUser viewUser;

    @Mock
    private CrowdService crowdService;

    @Mock
    private CrowdDirectoryService crowdDirectoryService;

    @Mock
    private UserPropertyManager userPropertyManager;

    @Mock
    private UserManager userManager;

    @Mock
    private FeatureManager featureManager;

    @Mock
    private UserApplicationHelper applicationHelper;

    @Mock
    @AvailableInContainer
    private GlobalPermissionManager globalPermissionManager;

    @Mock
    @AvailableInContainer
    private JiraAuthenticationContext jiraAuthenticationContext;

    @Mock
    private ApplicationUser user;

    @Mock
    private ApplicationUser currentUser;


    @Before
    public void setUp() throws Exception {
        viewUser = new ViewUser(crowdService, crowdDirectoryService, userPropertyManager, userManager, featureManager, applicationHelper);
        viewUser.setName(USER_NAME);

        when(userManager.getUserByName(USER_NAME)).thenReturn(user);
        when(jiraAuthenticationContext.getLoggedInUser()).thenReturn(currentUser);
    }

    @Test
    public void shouldCheckGroupMembershipUpdatePermission() {
        when(userManager.canUpdateUser(user)).thenReturn(true);
        when(globalPermissionManager.hasPermission(Permissions.SYSTEM_ADMIN, user)).thenReturn(false);
        when(userManager.canUpdateGroupMembershipForUser(user)).thenReturn(false);

        assertTrue("User is editable", viewUser.isSelectedUserEditable());
        assertFalse("Application access is not editable", viewUser.isSelectedUserApplicationAccessEditable());
    }

    @Test
    public void shouldCheckGroupMembershipUpdatePermissionWhenUserIsNotEditable() {
        when(userManager.canUpdateUser(user)).thenReturn(false);
        when(globalPermissionManager.hasPermission(Permissions.SYSTEM_ADMIN, user)).thenReturn(false);
        when(userManager.canUpdateGroupMembershipForUser(user)).thenReturn(true);

        assertFalse("User is editable", viewUser.isSelectedUserEditable());
        assertTrue("Application access is not editable", viewUser.isSelectedUserApplicationAccessEditable());
    }

}
