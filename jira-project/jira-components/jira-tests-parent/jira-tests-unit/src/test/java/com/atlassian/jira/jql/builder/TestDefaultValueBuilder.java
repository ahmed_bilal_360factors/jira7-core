package com.atlassian.jira.jql.builder;

import com.atlassian.jira.util.collect.CollectionBuilder;
import com.atlassian.query.operand.EmptyOperand;
import com.atlassian.query.operand.Operand;
import com.atlassian.query.operand.Operands;
import com.atlassian.query.operator.Operator;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertSame;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

/**
 * Test for {@link com.atlassian.jira.jql.builder.ValueBuilder}.
 *
 * @since v4.0
 */
public class TestDefaultValueBuilder {
    @Rule
    public MockitoRule mockito = MockitoJUnit.rule();

    @Mock
    private JqlClauseBuilder builder;

    @Test
    public void testConstructorBad() throws Exception {
        try {
            new DefaultValueBuilder(null, "name", Operator.EQUALS);
            fail("Exception expected.");
        } catch (IllegalArgumentException e) {
        }

        try {
            new DefaultValueBuilder(builder, null, Operator.EQUALS);
            fail("Exception expected.");
        } catch (IllegalArgumentException e) {
        }

        try {
            new DefaultValueBuilder(builder, "name", null);
            fail("Exception expected.");
        } catch (IllegalArgumentException e) {
        }

        verifyNoMoreInteractions(builder);
    }

    @Test
    public void testStringSingle() throws Exception {
        final String value = "value";
        final String name = "name";

        when(builder.addStringCondition(name, Operator.EQUALS, value)).thenReturn(builder);

        final DefaultValueBuilder defaultValueBuilder = new DefaultValueBuilder(builder, name, Operator.EQUALS);
        assertSame(builder, defaultValueBuilder.string(value));
    }

    @Test
    public void testStringVarArgs() throws Exception {
        final String value = "value";
        final String value2 = "value2";
        final String name = "name";

        when(builder.addStringCondition(name, Operator.EQUALS, value, value2)).thenReturn(builder);

        final DefaultValueBuilder defaultValueBuilder = new DefaultValueBuilder(builder, name, Operator.EQUALS);
        assertSame(builder, defaultValueBuilder.strings(value, value2));

    }

    @Test
    public void testStringCollection() throws Exception {
        final String value = "value";
        final String value2 = "value2";
        final String name = "name";
        final Collection<String> values = CollectionBuilder.newBuilder(value, value2).asLinkedList();


        when(builder.addStringCondition(name, Operator.EQUALS, values)).thenReturn(builder);

        final DefaultValueBuilder defaultValueBuilder = new DefaultValueBuilder(builder, name, Operator.EQUALS);
        assertSame(builder, defaultValueBuilder.strings(values));

    }

    @Test
    public void testNumberSingle() throws Exception {
        final long value = -7;
        final String name = "name";

        when(builder.addNumberCondition(name, Operator.EQUALS, value)).thenReturn(builder);

        final DefaultValueBuilder defaultValueBuilder = new DefaultValueBuilder(builder, name, Operator.EQUALS);
        assertSame(builder, defaultValueBuilder.number(value));

    }

    @Test
    public void testNumberVarArgs() throws Exception {
        final long value = 5;
        final long value2 = 6;
        final String name = "name";

        when(builder.addNumberCondition(name, Operator.EQUALS, value, value2)).thenReturn(builder);

        final DefaultValueBuilder defaultValueBuilder = new DefaultValueBuilder(builder, name, Operator.EQUALS);
        assertSame(builder, defaultValueBuilder.numbers(value, value2));

    }

    @Test
    public void testNumberCollection() throws Exception {
        final String name = "name";
        final Collection<Long> values = CollectionBuilder.newBuilder(6L, 7L).asLinkedList();


        when(builder.addNumberCondition(name, Operator.EQUALS, values)).thenReturn(builder);

        final DefaultValueBuilder defaultValueBuilder = new DefaultValueBuilder(builder, name, Operator.EQUALS);
        assertSame(builder, defaultValueBuilder.numbers(values));

    }

    @Test
    public void testOperandSingle() throws Exception {
        final Operand value = Operands.valueOf("3");
        final String name = "name";

        when(builder.addCondition(name, Operator.EQUALS, value)).thenReturn(builder);

        final DefaultValueBuilder defaultValueBuilder = new DefaultValueBuilder(builder, name, Operator.EQUALS);
        assertSame(builder, defaultValueBuilder.operand(value));

    }

    @Test
    public void testOperandVarArgs() throws Exception {
        final Operand value = Operands.valueOf("5");
        final Operand value2 = Operands.valueOf(6L);
        final String name = "name";

        when(builder.addCondition(name, Operator.EQUALS, value, value2)).thenReturn(builder);

        final DefaultValueBuilder defaultValueBuilder = new DefaultValueBuilder(builder, name, Operator.EQUALS);
        assertSame(builder, defaultValueBuilder.operands(value, value2));

    }

    @Test
    public void testOperandCollection() throws Exception {
        final String name = "name";
        final List<Operand> values = CollectionBuilder.newBuilder(Operands.valueOf(6L), Operands.valueOf("7L")).asLinkedList();

        when(builder.addCondition(name, Operator.EQUALS, values)).thenReturn(builder);

        final DefaultValueBuilder defaultValueBuilder = new DefaultValueBuilder(builder, name, Operator.EQUALS);
        assertSame(builder, defaultValueBuilder.operands(values));

    }

    @Test
    public void testDateSingle() throws Exception {
        final Date date = new Date(56L);
        final String name = "name";

        when(builder.addDateCondition(name, Operator.LESS_THAN, date)).thenReturn(builder);

        final DefaultValueBuilder defaultValueBuilder = new DefaultValueBuilder(builder, name, Operator.LESS_THAN);
        assertSame(builder, defaultValueBuilder.date(date));

    }

    @Test
    public void testDateVarArgs() throws Exception {
        final Date value = new Date(347289347234L);
        final Date value2 = new Date(5878784545L);
        final String name = "name";

        when(builder.addDateCondition(name, Operator.NOT_IN, value, value2)).thenReturn(builder);

        final DefaultValueBuilder defaultValueBuilder = new DefaultValueBuilder(builder, name, Operator.NOT_IN);
        assertSame(builder, defaultValueBuilder.dates(value, value2));

    }

    @Test
    public void testDateCollection() throws Exception {
        final String name = "name";
        final List<Date> values = CollectionBuilder.newBuilder(new Date(7438243), new Date(2318283)).asLinkedList();

        when(builder.addDateCondition(name, Operator.LESS_THAN, values)).thenReturn(builder);

        final DefaultValueBuilder defaultValueBuilder = new DefaultValueBuilder(builder, name, Operator.LESS_THAN);
        assertSame(builder, defaultValueBuilder.dates(values));

    }

    @Test
    public void testEmpty() throws Exception {
        final String name = "name";

        when(builder.addCondition(name, Operator.EQUALS, EmptyOperand.EMPTY)).thenReturn(builder);

        final DefaultValueBuilder defaultValueBuilder = new DefaultValueBuilder(builder, name, Operator.EQUALS);
        assertSame(builder, defaultValueBuilder.empty());

    }

    @Test
    public void testFunction() throws Exception {
        final String name = "name";
        final String funcName = "funcName";

        when(builder.addFunctionCondition(name, Operator.EQUALS, funcName)).thenReturn(builder);

        final DefaultValueBuilder defaultValueBuilder = new DefaultValueBuilder(builder, name, Operator.EQUALS);
        assertSame(builder, defaultValueBuilder.function(funcName));

    }

    @Test
    public void testFunctionVarArgs() throws Exception {
        final String name = "name";
        final String funcName = "funcName";

        String[] args = new String[]{"7", "8", "9"};

        when(builder.addFunctionCondition(name, Operator.EQUALS, funcName, args)).thenReturn(builder);

        final DefaultValueBuilder defaultValueBuilder = new DefaultValueBuilder(builder, name, Operator.EQUALS);
        assertSame(builder, defaultValueBuilder.function(funcName, args));

    }

    @Test
    public void testFunctionCollection() throws Exception {
        final String name = "nam3";
        final String funcName = "asdhjajhds";

        Collection<String> args = CollectionBuilder.newBuilder("what", "is", "a", "builder").asCollection();

        when(builder.addFunctionCondition(name, Operator.GREATER_THAN, funcName, args)).thenReturn(builder);

        final DefaultValueBuilder defaultValueBuilder = new DefaultValueBuilder(builder, name, Operator.GREATER_THAN);
        assertSame(builder, defaultValueBuilder.function(funcName, args));

    }

    @Test
    public void testFunctionStandardIssueTypes() {
        final String name = "fjewlkjrwlkrjelwkjre";

        when(builder.addFunctionCondition(name, Operator.GREATER_THAN, "standardIssueTypes")).thenReturn(builder);

        final DefaultValueBuilder defaultValueBuilder = new DefaultValueBuilder(builder, name, Operator.GREATER_THAN);
        assertSame(builder, defaultValueBuilder.functionStandardIssueTypes());

    }

    @Test
    public void testFunctionSubTaskIssueTypes() {
        final String name = "adhdfhfhfdhjsdjhkfdkhjskjhdfjhskvbxvbksh475653874637";

        when(builder.addFunctionCondition(name, Operator.LESS_THAN, "subTaskIssueTypes")).thenReturn(builder);

        final DefaultValueBuilder defaultValueBuilder = new DefaultValueBuilder(builder, name, Operator.LESS_THAN);
        assertSame(builder, defaultValueBuilder.functionSubTaskIssueTypes());

    }

    @Test
    public void testFunctionMembersOf() {
        final String name = "asdfgh";

        final String groupName = "group";
        when(builder.addFunctionCondition(name, Operator.LESS_THAN, "membersOf", groupName)).thenReturn(builder);
        final ValueBuilder defaultValueBuilder = new DefaultValueBuilder(builder, name, Operator.LESS_THAN);

        try {
            defaultValueBuilder.functionMembersOf(null);
            fail("Expected illegal argument exception.");
        } catch (IllegalArgumentException expected) {
        }

        assertSame(builder, defaultValueBuilder.functionMembersOf(groupName));

    }

    @Test
    public void testFunctionCurrentUser() {
        final String name = "currentUser";

        when(builder.addFunctionCondition(name, Operator.GREATER_THAN_EQUALS, "currentUser")).thenReturn(builder);

        final DefaultValueBuilder defaultValueBuilder = new DefaultValueBuilder(builder, name, Operator.GREATER_THAN_EQUALS);
        assertSame(builder, defaultValueBuilder.functionCurrentUser());

    }

    @Test
    public void testFunctionIssueHistory() {
        final String name = "issueHistory";

        when(builder.addFunctionCondition(name, Operator.GREATER_THAN_EQUALS, "issueHistory")).thenReturn(builder);

        final DefaultValueBuilder defaultValueBuilder = new DefaultValueBuilder(builder, name, Operator.GREATER_THAN_EQUALS);
        assertSame(builder, defaultValueBuilder.functionIssueHistory());

    }

    @Test
    public void testFunctionUnreleasedVersions() {
        final String name = "meh";

        when(builder.addFunctionCondition(name, Operator.IN, "unreleasedVersions", new String[]{})).thenReturn(builder);
        when(builder.addFunctionCondition(name, Operator.IN, "unreleasedVersions", "JRA", "CONF")).thenReturn(builder);

        final DefaultValueBuilder defaultValueBuilder = new DefaultValueBuilder(builder, name, Operator.IN);
        assertSame(builder, defaultValueBuilder.functionUnreleasedVersions());
        assertSame(builder, defaultValueBuilder.functionUnreleasedVersions("JRA", "CONF"));

    }

    @Test
    public void testFunctionReleasedVersions() {
        final String name = "pft";

        when(builder.addFunctionCondition(name, Operator.EQUALS, "releasedVersions", new String[]{})).thenReturn(builder);
        when(builder.addFunctionCondition(name, Operator.EQUALS, "releasedVersions", "JRA", "CONF")).thenReturn(builder);

        final DefaultValueBuilder defaultValueBuilder = new DefaultValueBuilder(builder, name, Operator.EQUALS);
        assertSame(builder, defaultValueBuilder.functionReleasedVersions());
        assertSame(builder, defaultValueBuilder.functionReleasedVersions("JRA", "CONF"));

    }

    @Test
    public void testFunctionNow() {
        final String name = "now";

        when(builder.addFunctionCondition(name, Operator.EQUALS, "now")).thenReturn(builder);

        final DefaultValueBuilder defaultValueBuilder = new DefaultValueBuilder(builder, name, Operator.EQUALS);
        assertSame(builder, defaultValueBuilder.functionNow());

    }

    @Test
    public void testFunctionWatachedIssues() {
        final String name = "watchedIssues";

        when(builder.addFunctionCondition(name, Operator.EQUALS, "watchedIssues")).thenReturn(builder);

        final DefaultValueBuilder defaultValueBuilder = new DefaultValueBuilder(builder, name, Operator.EQUALS);
        assertSame(builder, defaultValueBuilder.functionWatchedIssues());

    }

    @Test
    public void testFunctionVotedIssues() {
        final String name = "votedIssues";

        when(builder.addFunctionCondition(name, Operator.EQUALS, "votedIssues")).thenReturn(builder);

        final DefaultValueBuilder defaultValueBuilder = new DefaultValueBuilder(builder, name, Operator.EQUALS);
        assertSame(builder, defaultValueBuilder.functionVotedIssues());

    }

    @Test
    public void testFunctionLinkedIssues() {
        final String name = "linkedIssues";
        final String issueKey = "KEY1";
        final String linkType1 = "caused by";
        final String linkType2 = "related";

        when(builder.addFunctionCondition(name, Operator.EQUALS, "linkedIssues", Collections.singletonList(issueKey))).thenReturn(builder);
        when(builder.addFunctionCondition(name, Operator.EQUALS, "linkedIssues", CollectionBuilder.newBuilder(issueKey, linkType1, linkType2).asList())).thenReturn(builder);

        final DefaultValueBuilder defaultValueBuilder = new DefaultValueBuilder(builder, name, Operator.EQUALS);
        assertSame(builder, defaultValueBuilder.functionLinkedIssues(issueKey));
        assertSame(builder, defaultValueBuilder.functionLinkedIssues(issueKey, linkType1, linkType2));

        try {
            defaultValueBuilder.functionLinkedIssues(null);
            fail("Expected an exception");
        } catch (IllegalArgumentException expected) {
            //expected.
        }

        try {
            defaultValueBuilder.functionLinkedIssues("blah", (String[]) null);
            fail("Expected an exception");
        } catch (IllegalArgumentException expected) {
            //expected.
        }
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFunctionRemoteLinkByGlobalId() {
        final String clause = "issue";
        final String name = "issuesWithRemoteLinksByGlobalId";
        final String globalId1 = "a-global-id";
        final String globalId2 = "another-global-id";

        when(builder.addFunctionCondition(clause, Operator.EQUALS, name, globalId1, globalId2)).thenReturn(builder);

        final DefaultValueBuilder defaultValueBuilder = new DefaultValueBuilder(builder, clause, Operator.EQUALS);
        assertSame(builder, defaultValueBuilder.functionRemoteLinksByGlobalId(globalId1, globalId2));

        defaultValueBuilder.functionRemoteLinksByGlobalId(null);
        defaultValueBuilder.functionRemoteLinksByGlobalId();
    }

    @Test
    public void testFunctionCascaingOptionParent() {
        final String name = "cascadeOption";
        final String parent = "parent";

        when(builder.addFunctionCondition(name, Operator.IN, "cascadeOption", parent)).thenReturn(builder);

        final DefaultValueBuilder defaultValueBuilder = new DefaultValueBuilder(builder, name, Operator.IN);
        assertSame(builder, defaultValueBuilder.functionCascaingOption(parent));
    }

    @Test
    public void testFunctionCascaingOptionParentAndChild() {
        final String name = "cascadeOption";
        final String child = "child";
        final String parent = "parent";

        when(builder.addFunctionCondition(name, Operator.IN, "cascadeOption", parent, child)).thenReturn(builder);

        final DefaultValueBuilder defaultValueBuilder = new DefaultValueBuilder(builder, name, Operator.IN);
        assertSame(builder, defaultValueBuilder.functionCascaingOption(parent, child));

    }

    @Test
    public void testFunctionCascaingOptionParentOnly() {
        final String name = "cascadeOption";
        final String parent = "parent";
        final String child = "\"none\"";

        when(builder.addFunctionCondition(name, Operator.IN, "cascadeOption", parent, child)).thenReturn(builder);

        final DefaultValueBuilder defaultValueBuilder = new DefaultValueBuilder(builder, name, Operator.IN);
        assertSame(builder, defaultValueBuilder.functionCascaingOption(parent, child));
    }
}
