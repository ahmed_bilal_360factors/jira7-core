package com.atlassian.jira.project.type;

import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import org.junit.Before;
import org.junit.Test;

import java.util.Collection;

import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.toList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestProjectTypeUpdatedRegistrarImpl {
    private static final ApplicationUser USER = anyUser();
    private static final Project PROJECT = anyProject();
    public static final ProjectTypeKey OLD_PROJECT_TYPE = projectTypeKey("old");
    public static final ProjectTypeKey NEW_PROJECT_TYPE = projectTypeKey("new");

    private ProjectTypeUpdatedRegistrarImpl registrar;

    @Before
    public void setUp() {
        registrar = new ProjectTypeUpdatedRegistrarReturningHandlersInOrder();
    }

    @Test
    public void aRegisteredHandlerReceivesANotificationWhenTheProjectTypeIsUpdated() throws Exception {
        ProjectTypeUpdatedHandler handler = handlerWithId("id");

        register(handler);
        notifyAllHandlers();

        assertHandlerWasNotifiedOnce(handler);
    }

    @Test
    public void severalHandlersCanBeRegistered() throws Exception {
        ProjectTypeUpdatedHandler firstHandler = handlerWithId("first");
        ProjectTypeUpdatedHandler secondHandler = handlerWithId("second");

        register(firstHandler);
        register(secondHandler);
        notifyAllHandlers();

        assertHandlerWasNotifiedOnce(firstHandler);
        assertHandlerWasNotifiedOnce(secondHandler);
    }

    @Test
    public void aRegisteredHandlerCanNotBeRegisteredAgain() throws Exception {
        ProjectTypeUpdatedHandler handler = handlerWithId("id");

        register(handler);
        register(handler);
        notifyAllHandlers();

        assertHandlerWasNotifiedOnce(handler);
    }

    @Test
    public void unregisteringAHandlerThatHasNotBeenRegisteredIsANoOp() throws Exception {
        ProjectTypeUpdatedHandler firstHandler = handlerWithId("first");
        ProjectTypeUpdatedHandler secondHandler = handlerWithId("second");

        register(firstHandler);
        unregister(secondHandler);
        notifyAllHandlers();

        assertHandlerWasNotifiedOnce(firstHandler);
    }

    @Test
    public void aHandlerThatHasBeenUnregisteredDoesNotReceiveMoreNotificationsWhenAProjectTypeHasBeenUpdated() throws Exception {
        ProjectTypeUpdatedHandler handler = handlerWithId("id");

        register(handler);
        unregister(handler);
        notifyAllHandlers();

        assertHandlerWasNotNotified(handler);
    }

    @Test
    public void notifyWithNoHandlersIsANoOp() throws Exception {
        boolean result = notifyAllHandlers();

        assertThat(result, is(true));
    }

    @Test
    public void whenAHandlerReturnsErrorsAllHandlersThatWereExecutedAreRolledBack() throws Exception {
        ProjectTypeUpdatedHandler firstHandler = handlerWithId("first");
        ProjectTypeUpdatedHandler secondHandler = handlerWithId("second");
        ProjectTypeUpdatedHandler thirdHandler = handlerThrowingException("third");

        register(firstHandler);
        register(secondHandler);
        register(thirdHandler);
        notifyAllHandlers();

        assertHandlerWasNotifiedOnce(firstHandler);
        assertHandlerWasRolledBack(firstHandler);

        assertHandlerWasNotifiedOnce(secondHandler);
        assertHandlerWasRolledBack(secondHandler);

        assertHandlerWasNotifiedOnce(thirdHandler);
        assertHandlerWasRolledBack(thirdHandler);
    }

    @Test
    public void whenAHandlerReturnsErrorsHandlersThatHaveNotBeenNotifiedYetAreNotRolledBack() throws Exception {
        ProjectTypeUpdatedHandler firstHandler = handlerWithId("first");
        ProjectTypeUpdatedHandler secondHandler = handlerThrowingException("second");
        ProjectTypeUpdatedHandler thirdHandler = handlerWithId("third");

        register(firstHandler);
        register(secondHandler);
        register(thirdHandler);
        notifyAllHandlers();

        assertHandlerWasNotifiedOnce(firstHandler);
        assertHandlerWasRolledBack(firstHandler);

        assertHandlerWasNotifiedOnce(secondHandler);
        assertHandlerWasRolledBack(secondHandler);

        assertHandlerWasNotNotified(thirdHandler);
        assertHandlerWasNotRolledBack(thirdHandler);
    }

    @Test
    public void whenAHandlerThrowsAnExceptionFalseIsReturned() throws Exception {
        ProjectTypeUpdatedHandler handler = handlerThrowingException("first");

        register(handler);
        boolean result = notifyAllHandlers();

        assertThat(result, is(false));
    }

    private void assertHandlerWasNotifiedOnce(ProjectTypeUpdatedHandler handler) throws Exception {
        verify(handler, times(1)).onProjectTypeUpdated(USER, PROJECT, OLD_PROJECT_TYPE, NEW_PROJECT_TYPE);
    }

    private void assertHandlerWasNotNotified(ProjectTypeUpdatedHandler handler) throws Exception {
        verify(handler, never()).onProjectTypeUpdated(USER, PROJECT, OLD_PROJECT_TYPE, NEW_PROJECT_TYPE);
    }

    private void assertHandlerWasRolledBack(ProjectTypeUpdatedHandler handler) {
        ProjectTypeUpdatedOutcome expectedOutcome = handler.onProjectTypeUpdated(USER, PROJECT, OLD_PROJECT_TYPE, NEW_PROJECT_TYPE);

        verify(handler, times(1)).onProjectTypeUpdateRolledBack(USER, PROJECT, OLD_PROJECT_TYPE, NEW_PROJECT_TYPE, expectedOutcome);
    }

    private void assertHandlerWasNotRolledBack(ProjectTypeUpdatedHandler handler) {
        ProjectTypeUpdatedOutcome expectedOutcome = handler.onProjectTypeUpdated(USER, PROJECT, OLD_PROJECT_TYPE, NEW_PROJECT_TYPE);

        verify(handler, never()).onProjectTypeUpdateRolledBack(USER, PROJECT, OLD_PROJECT_TYPE, NEW_PROJECT_TYPE, expectedOutcome);
    }

    private static ApplicationUser anyUser() {
        return new MockApplicationUser("username");
    }

    private static Project anyProject() {
        return new MockProject();
    }

    private static ProjectTypeKey projectTypeKey(String key) {
        return new ProjectTypeKey(key);
    }

    private boolean notifyAllHandlers() {
        return registrar.notifyAllHandlers(USER, PROJECT, OLD_PROJECT_TYPE, NEW_PROJECT_TYPE);
    }

    private void register(ProjectTypeUpdatedHandler handler) {
        registrar.register(handler);
    }

    private void unregister(ProjectTypeUpdatedHandler handler) {
        registrar.unregister(handler);
    }

    private ProjectTypeUpdatedHandler handlerWithId(String id) throws Exception {
        ProjectTypeUpdatedHandler handler = mock(ProjectTypeUpdatedHandler.class);
        when(handler.getHandlerId()).thenReturn(id);

        ProjectTypeUpdatedOutcome outcome = projectTypeUpdatedOutcome(true);
        when(handler.onProjectTypeUpdated(any(ApplicationUser.class), any(Project.class), any(ProjectTypeKey.class), any(ProjectTypeKey.class)))
                .thenReturn(outcome);
        return handler;
    }

    private ProjectTypeUpdatedOutcome projectTypeUpdatedOutcome(boolean successful) {
        ProjectTypeUpdatedOutcome outcome = mock(ProjectTypeUpdatedOutcome.class);
        when(outcome.isSuccessful()).thenReturn(successful);
        return outcome;
    }

    private ProjectTypeUpdatedHandler handlerThrowingException(String id) throws Exception {
        ProjectTypeUpdatedHandler handler = mock(ProjectTypeUpdatedHandler.class);
        when(handler.getHandlerId()).thenReturn(id);

        ProjectTypeUpdatedOutcome outcome = projectTypeUpdatedOutcome(false);
        when(handler.onProjectTypeUpdated(any(ApplicationUser.class), any(Project.class), any(ProjectTypeKey.class), any(ProjectTypeKey.class)))
                .thenReturn(outcome);

        return handler;
    }

    private static class ProjectTypeUpdatedRegistrarReturningHandlersInOrder extends ProjectTypeUpdatedRegistrarImpl {
        @Override
        protected Collection<ProjectTypeUpdatedHandler> getHandlers() {
            return handlers.values().stream().sorted(comparing(ProjectTypeUpdatedHandler::getHandlerId)).collect(toList());
        }
    }
}
