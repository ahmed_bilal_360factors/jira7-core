package com.atlassian.jira.transaction;

import javax.annotation.Nonnull;

public class MockTransactionSupport implements TransactionSupport {
    @Override
    public Transaction begin() throws TransactionRuntimeException {
        return new MockTransaction();
    }

    private class MockTransaction implements Transaction {
        @Override
        public void commit() throws TransactionRuntimeException {
        }

        @Override
        public void rollback() throws TransactionRuntimeException {
        }

        @Override
        public void finallyRollbackIfNotCommitted() {
        }

        @Override
        public boolean isNewTransaction() {
            return false;
        }

        @Override
        public void runAfterSuccessfulCommit(@Nonnull Runnable runnable) {
        }
    }
}
