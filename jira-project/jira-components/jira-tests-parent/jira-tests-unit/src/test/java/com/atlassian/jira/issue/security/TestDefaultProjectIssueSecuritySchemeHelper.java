package com.atlassian.jira.issue.security;

import com.atlassian.jira.mock.security.MockSimpleAuthenticationContext;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.scheme.Scheme;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Sets;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import java.util.Collection;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

/**
 * @since v4.4
 */
public class TestDefaultProjectIssueSecuritySchemeHelper {
    @Rule
    public final MethodRule initMockito = MockitoJUnit.rule();

    @Mock
    private PermissionManager permissionManager;
    @Mock
    private IssueSecuritySchemeManager issueSecuritySchemeManager;
    private MockApplicationUser user = new MockApplicationUser("mtan");
    private MockSimpleAuthenticationContext authContext = new MockSimpleAuthenticationContext(user);

    @Test
    public void testGetProjectsNonDefault() throws Exception {
        final MockProject project1 = new MockProject(101928282L, "ONE");
        final MockProject project2 = new MockProject(35438590L, "TWO");

        final Scheme issueSecurityScheme = new Scheme("101010L", "lalala");

        when(issueSecuritySchemeManager.getProjects(issueSecurityScheme))
                .thenReturn(ImmutableList.of(project1, project2));

        ProjectIssueSecuritySchemeHelper helper = createHelper(Sets.newHashSet(project1));

        List<Project> projects = helper.getSharedProjects(issueSecurityScheme);
        assertEquals(ImmutableList.<Project>of(project1), projects);
    }

    private ProjectIssueSecuritySchemeHelper createHelper(final Collection<? extends Project> allowedProjects) {
        return new DefaultProjectIssueSecuritySchemeHelper(issueSecuritySchemeManager, authContext, permissionManager) {
            boolean hasEditPermission(final ApplicationUser user, final Project project) {
                return allowedProjects.contains(project);
            }
        };
    }

}
