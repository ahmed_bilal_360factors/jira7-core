package com.atlassian.jira.jql.context;

import com.atlassian.jira.jql.operand.JqlOperandResolver;
import com.atlassian.jira.jql.operator.OperatorClasses;
import com.atlassian.jira.jql.util.JqlIssueSupport;
import com.atlassian.jira.mock.issue.MockIssue;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.util.collect.CollectionBuilder;
import com.atlassian.jira.util.lang.Pair;
import com.atlassian.query.clause.TerminalClauseImpl;
import com.atlassian.query.operand.EmptyOperand;
import com.atlassian.query.operand.Operand;
import com.atlassian.query.operand.SingleValueOperand;
import com.atlassian.query.operator.Operator;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import java.util.Arrays;
import java.util.HashSet;

import static com.atlassian.jira.jql.operand.SimpleLiteralFactory.createLiteral;
import static com.google.common.collect.Sets.newHashSet;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestIssueIdClauseContextFactory {
    private ApplicationUser theUser = new MockApplicationUser("lwlodarczyk");

    @Rule
    public final MethodRule initMockito = MockitoJUnit.rule();

    @Mock
    private JqlOperandResolver jqlOperandResolver;
    @Mock
    private JqlIssueSupport jqlIssueSupport;

    @Test
    public void testGetClauseContextRelational() throws Exception {
        final SingleValueOperand operand = new SingleValueOperand("blah");
        final TerminalClauseImpl clause = new TerminalClauseImpl("blah", Operator.GREATER_THAN, operand);

        final MockIssue issue1 = new MockIssue(1L);
        issue1.setProjectObject(new MockProject(222L));
        final MockIssue issue2 = new MockIssue(12345L);
        issue2.setProjectObject(new MockProject(888L));

        when(jqlOperandResolver.getValues(theUser, operand, clause))
                .thenReturn(Arrays.asList(createLiteral("key-10"), createLiteral(12345L)));

        when(jqlIssueSupport.getProjectIssueTypePairsByIds(newHashSet(12345L))).thenReturn(newHashSet(Pair.of(888L, "")));
        when(jqlIssueSupport.getProjectIssueTypePairsByKeys(newHashSet("key-10"))).thenReturn(newHashSet(Pair.of(222L, "")));

        IssueIdClauseContextFactory factory = new IssueIdClauseContextFactory(jqlIssueSupport, jqlOperandResolver,
                OperatorClasses.EQUALITY_AND_RELATIONAL);
        final ClauseContext result = factory.getClauseContext(theUser, clause);
        ClauseContext expectedResult = new ClauseContextImpl(CollectionBuilder.<ProjectIssueTypeContext>newBuilder(
                new ProjectIssueTypeContextImpl(new ProjectContextImpl(222L), AllIssueTypesContext.INSTANCE),
                new ProjectIssueTypeContextImpl(new ProjectContextImpl(888L), AllIssueTypesContext.INSTANCE)
        ).asSet());

        assertEquals(expectedResult, result);
    }

    @Test
    public void testGetClauseContextRelationalNotAllowed() throws Exception {
        ClauseContext expectedResult = ClauseContextImpl.createGlobalClauseContext();

        for (Operator operator : OperatorClasses.RELATIONAL_ONLY_OPERATORS) {
            IssueIdClauseContextFactory factory = new IssueIdClauseContextFactory(jqlIssueSupport, jqlOperandResolver,
                    OperatorClasses.EQUALITY_OPERATORS);
            final ClauseContext result =
                    factory.getClauseContext(theUser, new TerminalClauseImpl("blah", operator, "blah"));

            assertEquals(expectedResult, result);
        }
    }

    @Test
    public void testGetClauseContext() throws Exception {
        final SingleValueOperand operand = new SingleValueOperand("blah");
        final TerminalClauseImpl clause = new TerminalClauseImpl("blah", Operator.EQUALS, operand);

        when(jqlOperandResolver.getValues(theUser, operand, clause))
                .thenReturn(Arrays.asList(createLiteral("key"), createLiteral(10L)));

        when(jqlIssueSupport.getProjectIssueTypePairsByIds(newHashSet(10L))).thenReturn(newHashSet(Pair.of(11L, "11")));
        when(jqlIssueSupport.getProjectIssueTypePairsByKeys(newHashSet("key"))).thenReturn(newHashSet(Pair.of(10L, "10")));

        IssueIdClauseContextFactory factory = new IssueIdClauseContextFactory(jqlIssueSupport, jqlOperandResolver,
                OperatorClasses.EQUALITY_AND_RELATIONAL);
        final ClauseContext result = factory.getClauseContext(theUser, clause);
        ClauseContext expectedResult = new ClauseContextImpl(CollectionBuilder.<ProjectIssueTypeContext>newBuilder(
                new ProjectIssueTypeContextImpl(new ProjectContextImpl(10L), new IssueTypeContextImpl("10")),
                new ProjectIssueTypeContextImpl(new ProjectContextImpl(11L), new IssueTypeContextImpl("11"))
        ).asSet());

        assertEquals(expectedResult, result);
    }

    @Test
    public void testGetClauseContextIneqaulity() throws Exception {
        ClauseContext expectedResult = ClauseContextImpl.createGlobalClauseContext();

        IssueIdClauseContextFactory factory = new IssueIdClauseContextFactory(jqlIssueSupport, jqlOperandResolver,
                OperatorClasses.EQUALITY_AND_RELATIONAL);
        final ClauseContext result =
                factory.getClauseContext(theUser, new TerminalClauseImpl("blah", Operator.NOT_EQUALS, "blah"));

        assertEquals(expectedResult, result);
    }

    @Test
    public void testGetClauseContextRelationalBadId() throws Exception {
        final SingleValueOperand singleValueOperand = new SingleValueOperand(10L);
        final TerminalClauseImpl clause = new TerminalClauseImpl("blah", Operator.GREATER_THAN, singleValueOperand);

        when(jqlOperandResolver.getValues(theUser, singleValueOperand, clause)).thenReturn(Arrays.asList(createLiteral(10L)));
        when(jqlIssueSupport.getProjectIssueTypePairsByIds(newHashSet(10L))).thenReturn(new HashSet<>());

        IssueIdClauseContextFactory factory = new IssueIdClauseContextFactory(jqlIssueSupport, jqlOperandResolver,
                OperatorClasses.EQUALITY_AND_RELATIONAL);
        final ClauseContext result = factory.getClauseContext(theUser, clause);
        ClauseContext expectedResult = ClauseContextImpl.createGlobalClauseContext();

        assertEquals(expectedResult, result);
    }

    @Test
    public void testGetClauseContextRelationalBadKey() throws Exception {
        final SingleValueOperand singleValueOperand = new SingleValueOperand(10L);
        final TerminalClauseImpl clause = new TerminalClauseImpl("blah", Operator.GREATER_THAN, singleValueOperand);

        when(jqlOperandResolver.getValues(theUser, singleValueOperand, clause)).thenReturn(Arrays.asList(createLiteral("badkey")));
        when(jqlIssueSupport.getProjectIssueTypePairsByKeys(newHashSet("badkey"))).thenReturn(new HashSet<>());

        IssueIdClauseContextFactory factory = new IssueIdClauseContextFactory(jqlIssueSupport, jqlOperandResolver,
                OperatorClasses.EQUALITY_AND_RELATIONAL);
        final ClauseContext result = factory.getClauseContext(theUser, clause);
        ClauseContext expectedResult = ClauseContextImpl.createGlobalClauseContext();

        assertEquals(expectedResult, result);
    }

    @Test
    public void testGetClauseContextSingleEmptyLiteral() throws Exception {
        final Operand operand = EmptyOperand.EMPTY;
        final TerminalClauseImpl clause = new TerminalClauseImpl("blah", Operator.IS, operand);
        final IssueIdClauseContextFactory factory =
                new IssueIdClauseContextFactory(jqlIssueSupport, jqlOperandResolver,
                        OperatorClasses.EQUALITY_OPERATORS);

        final ClauseContext result = factory.getClauseContext(theUser, clause);
        final ClauseContext expectedResult = ClauseContextImpl.createGlobalClauseContext();

        assertEquals(expectedResult, result);
    }

    @Test
    public void testGetClauseContextSingleInvalidEmptyLiteral() throws Exception {
        final TerminalClauseImpl clause =
                new TerminalClauseImpl("blah", Operator.IS, new SingleValueOperand("illegal"));
        final IssueIdClauseContextFactory factory =
                new IssueIdClauseContextFactory(jqlIssueSupport, jqlOperandResolver,
                        OperatorClasses.EQUALITY_OPERATORS);

        final ClauseContext result = factory.getClauseContext(theUser, clause);
        final ClauseContext expectedResult = ClauseContextImpl.createGlobalClauseContext();

        assertEquals(expectedResult, result);
    }
}
