package com.atlassian.jira.issue.fields;

import com.atlassian.jira.issue.IssueFieldConstants;
import com.atlassian.jira.issue.ModifiedValue;
import com.atlassian.jira.issue.customfields.OperationContext;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem;
import com.atlassian.jira.issue.fields.rest.json.beans.JiraBaseUrls;
import com.atlassian.jira.issue.fields.screen.FieldScreenRenderLayoutItem;
import com.atlassian.jira.issue.history.ChangeItemBean;
import com.atlassian.jira.issue.label.Label;
import com.atlassian.jira.issue.label.LabelComparator;
import com.atlassian.jira.issue.label.LabelManager;
import com.atlassian.jira.issue.util.IssueChangeHolder;
import com.atlassian.jira.junit.rules.InitMockitoMocks;
import com.atlassian.jira.mock.i18n.MockI18nHelper;
import com.atlassian.jira.mock.issue.MockIssue;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.jira.util.collect.CollectionBuilder;
import com.atlassian.jira.util.collect.MapBuilder;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.mockito.Mock;

import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;

import static com.atlassian.jira.issue.IssueFieldConstants.LABELS;
import static com.atlassian.jira.issue.label.LabelParser.MAX_LABEL_LENGTH;
import static com.atlassian.jira.util.collect.CollectionBuilder.list;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.argThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isA;
import static org.mockito.Matchers.isNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

/**
 * @since v4.2
 */
public class TestLabelsSystemField {
    private static final String TOO_LONG_ERROR = "<too-long>";
    private static final String INVALID_CHAR_ERROR = "<invalid-char>";

    public static final AtomicLong IDS = new AtomicLong();

    @Rule
    public TestRule initMockito = new InitMockitoMocks(this);

    private LabelsSystemField labelsSystemField;
    @Mock
    private OperationContext mockContext;
    @Mock
    private ErrorCollection mockErrors;
    @Mock
    private I18nHelper mockI18n;
    @Mock
    private LabelManager mockLabelManager;
    @Mock
    private JiraAuthenticationContext mockAuthenticationContext;
    @Mock
    private JiraBaseUrls jiraBaseUrls;
    @Mock
    private FieldLayoutItem mockLayoutItem;

    private static final long ISSUE_ID = 1;

    @Before
    public void setUp() throws Exception {
        labelsSystemField = new LabelsSystemField(null, null, mockAuthenticationContext, null, null, mockLabelManager, null, jiraBaseUrls);
    }

    @Test
    public void testGoodLabels() {
        when(mockContext.getFieldValuesHolder()).thenReturn(MapBuilder.<String, Object>newBuilder().add(LABELS,
                CollectionBuilder.newBuilder(new Label(null, null, "foo"), new Label(null, null, "bar")).asListOrderedSet()).toMap());

        labelsSystemField.validateParams(mockContext, mockErrors, null, null, null);
    }

    @Test
    public void testEmptyLabels() {
        final FieldScreenRenderLayoutItem mockRenderLayoutItem = mock(FieldScreenRenderLayoutItem.class);
        labelsSystemField.validateParams(mockContext, mockErrors, null, null, mockRenderLayoutItem);
    }

    @Test
    public void testEmptyLabelsIsRequired() {
        final FieldScreenRenderLayoutItem mockRenderLayoutItem = mock(FieldScreenRenderLayoutItem.class);
        when(mockRenderLayoutItem.isRequired()).thenReturn(true);

        final SimpleErrorCollection errors = new SimpleErrorCollection();
        labelsSystemField.validateParams(mockContext, errors, new MockI18nHelper(), null, mockRenderLayoutItem);
        assertEquals("issue.field.required [issue.field.labels]", errors.getErrors().get("labels"));
    }

    @Test
    public void testLabelTooLong() {
        final StringBuilder sb = new StringBuilder();
        for (int i = 0; i <= MAX_LABEL_LENGTH; i++) {
            sb.append("x");
        }
        final String tooLongLabel = sb.toString();
        assertTrue(tooLongLabel.length() > MAX_LABEL_LENGTH);

        when(mockContext.getFieldValuesHolder()).thenReturn(MapBuilder.<String, Object>newBuilder().add(LABELS,
                CollectionBuilder.newBuilder(new Label(null, null, tooLongLabel)).asListOrderedSet()).toMap());
        when(mockI18n.getText("label.service.error.label.toolong", tooLongLabel)).thenReturn(TOO_LONG_ERROR);

        labelsSystemField.validateParams(mockContext, mockErrors, mockI18n, null, null);

        verify(mockErrors).addError("labels", TOO_LONG_ERROR);
    }

    @Test
    public void testInvalidCharacters() {
        final String invalidLabel = "bad label";
        when(mockContext.getFieldValuesHolder()).thenReturn(MapBuilder.<String, Object>newBuilder().add(LABELS,
                CollectionBuilder.newBuilder(new Label(null, null, invalidLabel)).asListOrderedSet()).toMap());
        when(mockI18n.getText("label.service.error.label.invalid", invalidLabel)).thenReturn(INVALID_CHAR_ERROR);

        labelsSystemField.validateParams(mockContext, mockErrors, mockI18n, null, null);

        verify(mockErrors).addError("labels", INVALID_CHAR_ERROR);
    }

    @Test
    public void testUpdateNonChangedLabels() {
        final ModifiedValue mockModifiedValue = newMockModifiedValue("one three two", "one two three");
        final IssueChangeHolder mockChangeHolder = mock(IssueChangeHolder.class);

        labelsSystemField.updateValue(mockLayoutItem, newMockIssue(ISSUE_ID), mockModifiedValue, mockChangeHolder);

        verifyZeroInteractions(mockLabelManager);
    }

    @Test
    public void testUpdateChangedLabels() {
        final ModifiedValue mockModifiedValue = newMockModifiedValue("one three two", "one six five");
        final IssueChangeHolder mockChangeHolder = mock(IssueChangeHolder.class);
        //noinspection unchecked
        when(mockLabelManager.setLabels(isNull(ApplicationUser.class), eq(1L), isA(Set.class), eq(false), eq(false)))
                .thenReturn(CollectionBuilder.newBuilder(label("one"), label("five"), label("six"))
                        .asSortedSet(LabelComparator.INSTANCE));
        labelsSystemField.updateValue(mockLayoutItem, newMockIssue(ISSUE_ID), mockModifiedValue, mockChangeHolder);

        verify(mockChangeHolder).addChangeItem(eqBean("one three two", "five one six",
                IssueFieldConstants.LABELS, ChangeItemBean.STATIC_FIELD));
    }

    @Test
    public void testUpdateEmptyLabels() {
        final ModifiedValue mockModifiedValue = newMockModifiedValue("  ", "   ");
        final IssueChangeHolder mockChangeHolder = mock(IssueChangeHolder.class);

        labelsSystemField.updateValue(mockLayoutItem, newMockIssue(ISSUE_ID), mockModifiedValue, mockChangeHolder);

        verifyZeroInteractions(mockLabelManager);
    }

    @Test
    public void testNeedsMoveEmptyLabels() {
        final MockIssue mockIssue = new MockIssue(1000L);
        final MockIssue mockTargetIssue = new MockIssue(1010L);

        when(mockLayoutItem.isRequired()).thenReturn(true);

        assertTrue(labelsSystemField.needsMove(list(mockIssue), mockTargetIssue, mockLayoutItem).getResult());
    }

    @Test
    public void testNeedsMoveWithLabels() {
        final MockIssue mockIssue = new MockIssue(1000L);
        mockIssue.setLabels(CollectionBuilder.newBuilder(new Label(null, 1000L, "somelabel")).asSet());

        final MockIssue mockTargetIssue = new MockIssue(1010L);

        assertFalse(labelsSystemField.needsMove(CollectionBuilder.list(mockIssue), mockTargetIssue, mockLayoutItem).getResult());
    }

    private Label label(final String label) {
        return new Label(IDS.incrementAndGet(), ISSUE_ID, label);
    }

    private MockIssue newMockIssue(final long id) {
        return new MockIssue(id);
    }

    private ModifiedValue newMockModifiedValue(final String old, final String newVal) {
        return new ModifiedValue(old, newVal);
    }

    private ChangeItemBean eqBean(final String oldLabelsList, final String newLabelsList, final String fieldName,
                                  final String fieldType) {
        return argThat(newChangeItemBeanMatcher(oldLabelsList, newLabelsList, fieldName, fieldType));
    }

    private Matcher<ChangeItemBean> newChangeItemBeanMatcher(final String oldLabelsList, final String newLabelsList,
                                                             final String fieldName, final String fieldType) {
        return new TypeSafeMatcher<ChangeItemBean>() {
            @Override
            protected boolean matchesSafely(final ChangeItemBean item) {
                return fieldType.equals(item.getFieldType())
                        && fieldName.equals(item.getField())
                        && oldLabelsList.equals(item.getFromString())
                        && newLabelsList.equals(item.getToString());
            }

            @Override
            public void describeTo(final Description description) {
                description.appendValue(new ChangeItemBean(fieldType, fieldName, oldLabelsList, newLabelsList));
            }
        };
    }
}