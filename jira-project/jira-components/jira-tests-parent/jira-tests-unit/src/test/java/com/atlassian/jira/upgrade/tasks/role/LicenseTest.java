package com.atlassian.jira.upgrade.tasks.role;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.jira.test.util.lic.sd.ServiceDeskLicenses;
import com.atlassian.jira.test.util.lic.software.SoftwareLicenses;
import org.junit.Test;

import java.util.Set;
import java.util.stream.Collectors;

import static com.atlassian.jira.upgrade.tasks.role.TestUtils.toLicense;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;

public class LicenseTest {
    @Test
    public void licenseIsParsedCorrectly() {
        //given
        final String licenseString = ServiceDeskLicenses.LICENSE_SERVICE_DESK_ABP.getLicenseString();
        final Set<ApplicationKey> keys = ServiceDeskLicenses.LICENSE_SERVICE_DESK_ABP
                .getRoles().keySet().stream()
                .map(ApplicationKey::valueOf)
                .collect(Collectors.toSet());

        //when
        final License license = new License(licenseString);

        //then
        assertThat(license.licenseString(), equalTo(licenseString));
        assertThat(license.applicationKeys(), equalTo(keys));
    }

    @Test
    public void equalToMatchesOnTheLicense() {
        //given
        final License licenseSd1 = toLicense(ServiceDeskLicenses.LICENSE_SERVICE_DESK_ABP);
        final License licenseSd2 = toLicense(ServiceDeskLicenses.LICENSE_SERVICE_DESK_ABP);
        final License licenseSw = toLicense(SoftwareLicenses.LICENSE_SOFTWARE);

        //then
        assertThat(licenseSd1, equalTo(licenseSd2));
        assertThat(licenseSd2, equalTo(licenseSd1));
        assertThat(licenseSd1.hashCode(), equalTo(licenseSd2.hashCode()));

        assertThat(licenseSw, not(equalTo(licenseSd1)));
        assertThat(licenseSw, not(equalTo(licenseSd2)));
        assertThat(licenseSd1, not(equalTo(licenseSw)));
        assertThat(licenseSd2, not(equalTo(licenseSw)));
    }

    @Test(expected = MigrationFailedException.class)
    public void throwsLicenseExceptionOnBadLicense() {
        new License("abc");
    }
}