package com.atlassian.jira.bc.dataimport;

import com.google.common.collect.ImmutableMap;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;
import java.util.Optional;

import static com.google.common.collect.ImmutableMap.of;
import static java.util.Collections.emptyMap;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestDataImportPropertiesAdapter {
    private static final String PROPERTY_KEY = "property.key";
    private static final String PROPERTY_ID = "1";
    private static final String STRING_VALUE = "value";
    private static final String NUMERIC_VALUE = "10000";

    @Mock
    private OfbizImportHandler ofbizImportHandler;

    @InjectMocks
    private DataImportPropertiesAdapter dataImportPropertiesAdapter;

    @Before
    public void prepare() {
        when(ofbizImportHandler.getPropertyKeyToId()).thenReturn(emptyMap());
        when(ofbizImportHandler.getOsPropertyStringMap()).thenReturn(emptyMap());
        when(ofbizImportHandler.getOsPropertyTextMap()).thenReturn(emptyMap());
        when(ofbizImportHandler.getOsPropertyNumberMap()).thenReturn(emptyMap());
    }

    @Test
    public void shouldReturnAnEmptyOptionalWhenGettingAStringPropertyAndPropertyKeyDoesNotExist() {
        Optional<String> result = dataImportPropertiesAdapter.getStringProperty(PROPERTY_KEY);

        assertThat(result.isPresent(), is(false));
    }

    @Test
    public void shouldReturnAnEmptyOptionalWhenGettingAStringPropertyAndPropertyValueDoesNotExist() {
        when(ofbizImportHandler.getPropertyKeyToId()).thenReturn(of(PROPERTY_KEY, PROPERTY_ID));

        Optional<String> result = dataImportPropertiesAdapter.getStringProperty(PROPERTY_KEY);

        assertThat(result.isPresent(), is(false));
    }

    @Test
    public void shouldReturnANonEmptyOptionalWhenGettingAStringPropertyAndPropertyKeyAndValueExist() {
        when(ofbizImportHandler.getPropertyKeyToId()).thenReturn(of(PROPERTY_KEY, PROPERTY_ID));
        when(ofbizImportHandler.getOsPropertyStringMap()).thenReturn(of(PROPERTY_ID, STRING_VALUE));

        Optional<String> result = dataImportPropertiesAdapter.getStringProperty(PROPERTY_KEY);

        assertThat(result.get(), equalTo(STRING_VALUE));
    }

    @Test
    public void shouldReturnAnEmptyOptionalWhenGettingATextPropertyAndPropertyKeyDoesNotExist() {
        Optional<String> result = dataImportPropertiesAdapter.getTextProperty(PROPERTY_KEY);

        assertThat(result.isPresent(), is(false));
    }

    @Test
    public void shouldReturnAnEmptyOptionalWhenGettingATextPropertyAndPropertyValueDoesNotExist() {
        when(ofbizImportHandler.getPropertyKeyToId()).thenReturn(of(PROPERTY_KEY, PROPERTY_ID));

        Optional<String> result = dataImportPropertiesAdapter.getTextProperty(PROPERTY_KEY);

        assertThat(result.isPresent(), is(false));
    }

    @Test
    public void shouldReturnANonEmptyOptionalWhenGettingATextPropertyAndPropertyKeyAndValueExist() {
        when(ofbizImportHandler.getPropertyKeyToId()).thenReturn(of(PROPERTY_KEY, PROPERTY_ID));
        when(ofbizImportHandler.getOsPropertyTextMap()).thenReturn(of(PROPERTY_ID, STRING_VALUE));

        Optional<String> result = dataImportPropertiesAdapter.getTextProperty(PROPERTY_KEY);

        assertThat(result.get(), equalTo(STRING_VALUE));
    }

    @Test
    public void shouldReturnAnEmptyOptionalWhenGettingANumberPropertyAndPropertyKeyDoesNotExist() {
        Optional<Long> result = dataImportPropertiesAdapter.getNumberProperty(PROPERTY_KEY);

        assertThat(result.isPresent(), is(false));
    }

    @Test
    public void shouldReturnAnEmptyOptionalWhenGettingANumberPropertyAndPropertyValueDoesNotExist() {
        when(ofbizImportHandler.getPropertyKeyToId()).thenReturn(of(PROPERTY_KEY, PROPERTY_ID));

        Optional<Long> result = dataImportPropertiesAdapter.getNumberProperty(PROPERTY_KEY);

        assertThat(result.isPresent(), is(false));
    }

    @Test
    public void shouldReturnANonEmptyOptionalWhenGettingANumberPropertyAndPropertyKeyAndValueExist() {
        when(ofbizImportHandler.getPropertyKeyToId()).thenReturn(of(PROPERTY_KEY, PROPERTY_ID));
        when(ofbizImportHandler.getOsPropertyNumberMap()).thenReturn(of(PROPERTY_ID, NUMERIC_VALUE));

        Optional<Long> result = dataImportPropertiesAdapter.getNumberProperty(PROPERTY_KEY);

        assertThat(result.get(), equalTo(10000L));
    }
}
