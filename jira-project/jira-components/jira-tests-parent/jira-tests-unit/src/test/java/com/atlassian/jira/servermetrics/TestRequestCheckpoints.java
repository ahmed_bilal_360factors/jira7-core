package com.atlassian.jira.servermetrics;

import com.atlassian.jira.JiraUnitTestProperties;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.hamcrest.Matchers.isIn;
import static org.junit.Assert.assertThat;

@RunWith(Parameterized.class)
public class TestRequestCheckpoints {

    private static Set<String> whitelistedProperties;
    private static final Path ANALYTICS_WHITELIST_FILE =
            Paths.get("jira-components/jira-plugins/jira-analytics-whitelist-plugin/" +
                    "src/main/resources/jira-analytics-whitelist-plugin-whitelist.json");


    @BeforeClass
    public static void loadWhitelist() throws IOException {
        whitelistedProperties = loadWhitelistPropertiesForEvent("jira.http.request.stats");
    }

    @Parameterized.Parameters(name = "timingEvents.{0}")
    public static Collection<Object[]> data() {
        return Arrays.stream(RequestCheckpoints.values()).map(checkpoint -> new Object[]{checkpoint}).collect(Collectors.toList());
    }

    private final RequestCheckpoints testObj;

    public TestRequestCheckpoints(RequestCheckpoints checkpoint) {
        this.testObj = checkpoint;
    }

    public static Set<String> loadWhitelistPropertiesForEvent(String eventName) throws IOException {
        final File analyticsWhitelistFile = JiraUnitTestProperties.getProjectRootPath().resolve(ANALYTICS_WHITELIST_FILE).toFile();

        final JsonElement whitelist;
        try (final FileReader analyticsWhitelistReader = new FileReader(analyticsWhitelistFile)) {
            whitelist = new JsonParser().parse(analyticsWhitelistReader);
        }

        final JsonArray whitelistedProperties = whitelist.getAsJsonObject().getAsJsonArray(eventName);
        return StreamSupport.stream(whitelistedProperties.spliterator(), false)
                .map(JsonElement::getAsString)
                .collect(Collectors.toSet());
    }

    @Test
    public void shouldPropertyBeIncludedInWhitelist() {
        assertThat("timingEvents." + testObj.name(), isIn(whitelistedProperties));
    }
}