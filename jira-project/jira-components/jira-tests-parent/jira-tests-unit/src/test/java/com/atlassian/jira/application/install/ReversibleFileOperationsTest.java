package com.atlassian.jira.application.install;

import org.apache.commons.io.FileUtils;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.IOException;

import static com.atlassian.jira.matchers.FileMatchers.exists;
import static org.hamcrest.Matchers.arrayWithSize;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;

public class ReversibleFileOperationsTest {
    private static final String ENCODING = "utf-8";
    @Rule
    public TemporaryFolder temporaryFolder = new TemporaryFolder();

    @Test
    public void shouldKeepCreatedFileAfterCommit() throws IOException {
        final File file;
        // when
        try (ReversibleFileOperations testObj = new ReversibleFileOperations()) {
            file = temporaryFolder.newFile();
            testObj.removeOnRollback(file);

            testObj.commit();
        }

        // then
        assertThat(file, exists());
    }

    @Test
    public void shouldDeleteFiles() throws IOException {
        // given
        final File file = temporaryFolder.newFile();

        try (ReversibleFileOperations testObj = new ReversibleFileOperations()) {
            // when
            testObj.fileDelete(file);

            // then
            assertThat(file, not(exists()));
        }
    }

    @Test
    public void shouldCleanDeletedFilesAfterCommit() throws IOException {
        // given
        final File file = temporaryFolder.newFile();

        // when
        try (ReversibleFileOperations testObj = new ReversibleFileOperations()) {
            testObj.fileDelete(file);

            testObj.commit();
        }

        // then
        assertThat(temporaryFolder.getRoot().list(), arrayWithSize(0));
    }

    @Test
    public void shouldReverFileCreationIfNotCommited() throws IOException {
        final File file;
        // when
        try (ReversibleFileOperations testObj = new ReversibleFileOperations()) {
            file = temporaryFolder.newFile();
            testObj.removeOnRollback(file);
        }

        // then
        assertThat(temporaryFolder.getRoot().list(), arrayWithSize(0));
    }

    @Test
    public void shouldRevertFileDeletionIfNotCommited() throws IOException {
        // given
        final File file = temporaryFolder.newFile();

        // when
        try (ReversibleFileOperations testObj = new ReversibleFileOperations()) {
            testObj.fileDelete(file);
        }

        // then
        assertThat(file, exists());
    }

    @Test
    public void shouldRevertToOriginalContentWhenFileWasDeletedAndCreated() throws IOException {
        final String originalContent = "original-content";
        final String newContent = "new-content";

        // given
        final File file = temporaryFolder.newFile();
        FileUtils.writeStringToFile(file, originalContent, ENCODING);

        // when
        try (ReversibleFileOperations testObj = new ReversibleFileOperations()) {
            testObj.fileDelete(file);
            testObj.removeOnRollback(file);
            FileUtils.writeStringToFile(file, newContent, ENCODING);
            testObj.fileDelete(file);
        }

        // then
        assertThat(file, exists());
        assertThat(FileUtils.readFileToString(file, ENCODING), equalTo(originalContent));
    }

    @Test
    public void operationCanBePartiallyCommited() throws IOException {
        // given
        final File commitedFile = temporaryFolder.newFile();
        final File notCommitedFile = temporaryFolder.newFile();

        // when
        try (ReversibleFileOperations testObj = new ReversibleFileOperations()) {
            testObj.fileDelete(commitedFile);
            testObj.commit();
            testObj.fileDelete(notCommitedFile);
        }

        // then
        assertThat(commitedFile, not(exists()));
        assertThat(notCommitedFile, exists());
    }

    @Test
    public void shouldIgnoreDeleteOfNonExistingFiles() throws IOException {
        // given
        final File commitedFile = new File(temporaryFolder.getRoot(), "im-not-there");

        // when
        try (ReversibleFileOperations testObj = new ReversibleFileOperations()) {
            testObj.fileDelete(commitedFile);
        }

        // then
        assertThat(temporaryFolder.getRoot().list(), arrayWithSize(0));
    }
}