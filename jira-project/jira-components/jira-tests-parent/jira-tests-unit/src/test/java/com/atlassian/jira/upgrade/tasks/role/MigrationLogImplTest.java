package com.atlassian.jira.upgrade.tasks.role;

import com.google.common.collect.ImmutableList;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Iterator;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

@RunWith(MockitoJUnitRunner.class)
public class MigrationLogImplTest {
    @Mock
    private AuditEntry entry;
    @Mock
    private AuditEntry entry1;

    @Test
    public void addingEventsShouldAccumulateToANewInstance() {
        MigrationLogImpl log = new MigrationLogImpl();
        MigrationLog log1 = log.log(entry);

        assertFalse(log.events().iterator().hasNext());
        assertEquals(entry, log1.events().iterator().next());
    }

    @Test
    public void multipleEventsShouldAccumulateInConstructor() {
        List<AuditEntry> events = ImmutableList.of(entry);
        MigrationLogImpl log = new MigrationLogImpl(events, entry1);
        Iterable<AuditEntry> result = log.events();

        Iterator<AuditEntry> iterator = result.iterator();
        assertEquals(entry, iterator.next());
        assertEquals(entry1, iterator.next());
        assertFalse(iterator.hasNext());
    }
}
