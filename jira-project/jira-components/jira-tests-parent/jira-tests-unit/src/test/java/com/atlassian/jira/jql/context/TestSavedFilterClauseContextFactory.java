package com.atlassian.jira.jql.context;

import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.issue.search.constants.SystemSearchConstants;
import com.atlassian.jira.issue.search.managers.SearchHandlerManager;
import com.atlassian.jira.jql.operand.JqlOperandResolver;
import com.atlassian.jira.jql.operand.QueryLiteral;
import com.atlassian.jira.jql.resolver.SavedFilterResolver;
import com.atlassian.jira.jql.validator.SavedFilterCycleDetector;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.collect.CollectionBuilder;
import com.atlassian.query.Query;
import com.atlassian.query.QueryImpl;
import com.atlassian.query.clause.Clause;
import com.atlassian.query.clause.NotClause;
import com.atlassian.query.clause.TerminalClause;
import com.atlassian.query.clause.TerminalClauseImpl;
import com.atlassian.query.operand.EmptyOperand;
import com.atlassian.query.operand.MultiValueOperand;
import com.atlassian.query.operator.Operator;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.atlassian.jira.jql.operand.SimpleLiteralFactory.createLiteral;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestSavedFilterClauseContextFactory {
    @Rule
    public MockitoRule mockito = MockitoJUnit.rule();

    @Mock
    private JqlOperandResolver jqlOperandResolver;
    @Mock
    private SavedFilterResolver savedFilterResolver;
    @Mock
    private ContextSetUtil contextSetUtil;
    @Mock
    private SavedFilterCycleDetector savedFilterCycleDetector;
    @Mock
    private QueryContextVisitor.QueryContextVisitorFactory queryContextVisitorFactory;
    private ApplicationUser theUser = null;
    private boolean overrideSecurity = false;
    private DumbVisitor dumbVisitor;

    @Before
    public void setUp() throws Exception {
        dumbVisitor = new DumbVisitor();
    }

    @Test
    public void testGetClauseContext() throws Exception {
        final SearchRequest searchRequest1 = mock(SearchRequest.class);

        final TerminalClauseImpl anyClause = new TerminalClauseImpl("blah", Operator.EQUALS, "blah");
        Query query1 = new QueryImpl(anyClause);
        Query query2 = new QueryImpl(anyClause);
        Query query3 = new QueryImpl();

        // these values are meaningless as long as they are different
        final ClauseContext clauseContext1 = new ClauseContextImpl(CollectionBuilder.<ProjectIssueTypeContext>newBuilder(new ProjectIssueTypeContextImpl(new ProjectContextImpl(10L), new IssueTypeContextImpl("10"))).asSet());
        final ClauseContext clauseContext2 = new ClauseContextImpl(CollectionBuilder.<ProjectIssueTypeContext>newBuilder(new ProjectIssueTypeContextImpl(new ProjectContextImpl(20L), new IssueTypeContextImpl("20"))).asSet());
        final ClauseContext clauseContext3 = new ClauseContextImpl(CollectionBuilder.<ProjectIssueTypeContext>newBuilder(new ProjectIssueTypeContextImpl(AllProjectsContext.INSTANCE, AllIssueTypesContext.INSTANCE)).asSet());

        final ClauseContext union = new ClauseContextImpl(CollectionBuilder.<ProjectIssueTypeContext>newBuilder(new ProjectIssueTypeContextImpl(new ProjectContextImpl(40L), new IssueTypeContextImpl("40"))).asSet());

        TerminalClause clause = new TerminalClauseImpl(SystemSearchConstants.forSavedFilter().getJqlClauseNames().getPrimaryName(), Operator.EQUALS, new MultiValueOperand("blarg", "orgle"));

        final List<QueryLiteral> literals = CollectionBuilder.newBuilder(createLiteral("blarg"), createLiteral("orgle")).asList();
        when(jqlOperandResolver.getValues(theUser, clause.getOperand(), clause)).thenReturn(literals);
        when(savedFilterResolver.getSearchRequest(theUser, literals)).thenReturn(CollectionBuilder.newBuilder(searchRequest1, searchRequest1, searchRequest1).asList());
        when(searchRequest1.getQuery()).thenReturn(query1, query2, query3);

        when(savedFilterCycleDetector.containsSavedFilterReference(theUser, overrideSecurity, searchRequest1, null)).thenReturn(false);

        when(contextSetUtil.union(CollectionBuilder.newBuilder(clauseContext1, clauseContext2).asSet())).thenReturn(union);
        when(contextSetUtil.union(CollectionBuilder.newBuilder(union, clauseContext3).asSet())).thenReturn(union);

        final SavedFilterClauseContextFactory factory = new SavedFilterClauseContextFactory(savedFilterResolver, jqlOperandResolver, queryContextVisitorFactory, contextSetUtil, savedFilterCycleDetector) {
            int called = 0;

            private ClauseContext[] contexts = new ClauseContext[]{clauseContext1, clauseContext2, clauseContext3};
            private Clause[] expected = new Clause[]{anyClause, anyClause, null};

            @Override
            ClauseContext getSavedFilterContext(final ApplicationUser searcher, final Clause clause) {
                assertEquals(expected[called], clause);
                return contexts[called++];
            }
        };

        assertEquals(union, factory.getClauseContext(theUser, clause));
    }

    @Test
    public void testGetClauseContextClauseContainsCycle() throws Exception {
        final SearchRequest searchRequest1 = mock(SearchRequest.class);
        final SearchRequest searchRequest2 = mock(SearchRequest.class);

        final TerminalClauseImpl anyClause = new TerminalClauseImpl("blah", Operator.EQUALS, "blah");
        Query query1 = new QueryImpl(anyClause);

        // these values are meaningless as long as they are different
        final ClauseContext clauseContext1 = new ClauseContextImpl(CollectionBuilder.<ProjectIssueTypeContext>newBuilder(new ProjectIssueTypeContextImpl(new ProjectContextImpl(10L), new IssueTypeContextImpl("10"))).asSet());
        final ClauseContext clauseContext2 = new ClauseContextImpl(CollectionBuilder.<ProjectIssueTypeContext>newBuilder(new ProjectIssueTypeContextImpl(new ProjectContextImpl(20L), new IssueTypeContextImpl("20"))).asSet());

        TerminalClause clause = new TerminalClauseImpl(SystemSearchConstants.forSavedFilter().getJqlClauseNames().getPrimaryName(), Operator.EQUALS, new MultiValueOperand("blarg", "orgle"));

        final List<QueryLiteral> literals = CollectionBuilder.newBuilder(createLiteral("blarg"), createLiteral("orgle")).asList();
        when(jqlOperandResolver.getValues(theUser, clause.getOperand(), clause)).thenReturn(literals);

        when(savedFilterResolver.getSearchRequest(theUser, literals)).thenReturn(CollectionBuilder.newBuilder(searchRequest1, searchRequest2).asList());

        when(searchRequest1.getQuery()).thenReturn(query1);

        when(savedFilterCycleDetector.containsSavedFilterReference(theUser, overrideSecurity, searchRequest1, null)).thenReturn(false);
        when(savedFilterCycleDetector.containsSavedFilterReference(theUser, overrideSecurity, searchRequest2, null)).thenReturn(true);

        SavedFilterClauseContextFactory factory = new SavedFilterClauseContextFactory(savedFilterResolver, jqlOperandResolver, queryContextVisitorFactory, contextSetUtil, savedFilterCycleDetector) {
            int called = 0;

            @Override
            ClauseContext getSavedFilterContext(final ApplicationUser searcher, final Clause clause) {
                called++;
                if (called == 1) {
                    return clauseContext1;
                } else {
                    return clauseContext2;
                }

            }
        };

        final ClauseContext expectedResult = ClauseContextImpl.createGlobalClauseContext();
        assertEquals(expectedResult, factory.getClauseContext(theUser, clause));
    }

    @Test
    public void testGetClauseContextInvalidClause() throws Exception {
        final TerminalClauseImpl clause = new TerminalClauseImpl("blah", Operator.IS, EmptyOperand.EMPTY);
        SavedFilterClauseContextFactory factory = new SavedFilterClauseContextFactory(savedFilterResolver, jqlOperandResolver, queryContextVisitorFactory, contextSetUtil, savedFilterCycleDetector);

        final ClauseContext result = factory.getClauseContext(theUser, clause);
        final ClauseContext expectedResult = ClauseContextImpl.createGlobalClauseContext();
        assertEquals(expectedResult, result);

    }

    @Test
    public void testGetClauseContextIneqality() throws Exception {
        final SearchRequest searchRequest1 = mock(SearchRequest.class);

        final TerminalClauseImpl anyClause = new TerminalClauseImpl("blah", Operator.NOT_EQUALS, "blah");
        final Query query1 = new QueryImpl(anyClause);
        final Query query2 = new QueryImpl(anyClause);
        final Query query3 = new QueryImpl();

        // these values are meaningless as long as they are different
        final ClauseContext clauseContext1 = new ClauseContextImpl(CollectionBuilder.<ProjectIssueTypeContext>newBuilder(new ProjectIssueTypeContextImpl(new ProjectContextImpl(10L), new IssueTypeContextImpl("10"))).asSet());
        final ClauseContext clauseContext2 = new ClauseContextImpl(CollectionBuilder.<ProjectIssueTypeContext>newBuilder(new ProjectIssueTypeContextImpl(new ProjectContextImpl(20L), new IssueTypeContextImpl("20"))).asSet());
        final ClauseContext clauseContext3 = new ClauseContextImpl(CollectionBuilder.<ProjectIssueTypeContext>newBuilder(new ProjectIssueTypeContextImpl(AllProjectsContext.INSTANCE, AllIssueTypesContext.INSTANCE)).asSet());

        final ClauseContext intersection = new ClauseContextImpl(CollectionBuilder.<ProjectIssueTypeContext>newBuilder(new ProjectIssueTypeContextImpl(new ProjectContextImpl(30L), new IssueTypeContextImpl("30"))).asSet());

        TerminalClause clause = new TerminalClauseImpl(SystemSearchConstants.forSavedFilter().getJqlClauseNames().getPrimaryName(), Operator.NOT_EQUALS, new MultiValueOperand("blarg", "orgle"));

        final List<QueryLiteral> literals = CollectionBuilder.newBuilder(createLiteral("blarg"), createLiteral("orgle")).asList();
        when(jqlOperandResolver.getValues(theUser, clause.getOperand(), clause)).thenReturn(literals);

        when(savedFilterResolver.getSearchRequest(theUser, literals)).thenReturn(CollectionBuilder.newBuilder(searchRequest1, searchRequest1, searchRequest1).asList());

        when(searchRequest1.getQuery()).thenReturn(query1, query2, query3);

        when(savedFilterCycleDetector.containsSavedFilterReference(theUser, overrideSecurity, searchRequest1, null)).thenReturn(false);
        when(contextSetUtil.intersect(CollectionBuilder.newBuilder(clauseContext1, clauseContext2).asSet())).thenReturn(intersection);
        when(contextSetUtil.intersect(CollectionBuilder.newBuilder(intersection, clauseContext3).asSet())).thenReturn(intersection);

        SavedFilterClauseContextFactory factory = new SavedFilterClauseContextFactory(savedFilterResolver, jqlOperandResolver, queryContextVisitorFactory, contextSetUtil, savedFilterCycleDetector) {
            int called = 0;

            private ClauseContext[] contexts = new ClauseContext[]{clauseContext1, clauseContext2, clauseContext3};
            private Clause[] expected = new Clause[]{new NotClause(anyClause), new NotClause(anyClause), null};

            @Override
            ClauseContext getSavedFilterContext(final ApplicationUser searcher, final Clause clause) {
                assertEquals(expected[called], clause);
                return contexts[called++];
            }
        };

        assertEquals(intersection, factory.getClauseContext(theUser, clause));
    }

    @Test
    public void testGetSavedFilterContextNullClause() throws Exception {
        final SavedFilterClauseContextFactory factory = new SavedFilterClauseContextFactory(savedFilterResolver, jqlOperandResolver, queryContextVisitorFactory, contextSetUtil, savedFilterCycleDetector);
        final ClauseContext filterContext = factory.getSavedFilterContext(null, null);
        assertEquals(ClauseContextImpl.createGlobalClauseContext(), filterContext);
    }

    @Test
    public void testGetSavedFilterContextNullCalculatedContextContext() throws Exception {
        final Clause clause = mock(Clause.class);
        when(clause.accept(dumbVisitor)).thenReturn(new QueryContextVisitor.ContextResult(null, null));

        final SavedFilterClauseContextFactory factory = new SavedFilterClauseContextFactory(savedFilterResolver, jqlOperandResolver, createFactory(), contextSetUtil, savedFilterCycleDetector);
        final ClauseContext filterContext = factory.getSavedFilterContext(null, clause);
        assertEquals(ClauseContextImpl.createGlobalClauseContext(), filterContext);
    }

    @Test
    public void testGetSavedFilterContextImplicit() throws Exception {
        Set<ProjectIssueTypeContext> ctxs = new HashSet<ProjectIssueTypeContext>();
        ctxs.add(new ProjectIssueTypeContextImpl(new ProjectContextImpl(10L), new IssueTypeContextImpl("18")));
        ctxs.add(new ProjectIssueTypeContextImpl(new ProjectContextImpl(17L), AllIssueTypesContext.getInstance()));
        ctxs.add(new ProjectIssueTypeContextImpl(AllProjectsContext.INSTANCE, new IssueTypeContextImpl("3475832")));
        ctxs.add(ProjectIssueTypeContextImpl.createGlobalContext());

        final ClauseContext expectedContext = new ClauseContextImpl(ctxs);

        final Clause clause = mock(Clause.class);
        when(clause.accept(dumbVisitor)).thenReturn(new QueryContextVisitor.ContextResult(expectedContext, expectedContext));

        final SavedFilterClauseContextFactory factory = new SavedFilterClauseContextFactory(savedFilterResolver, jqlOperandResolver, createFactory(), contextSetUtil, savedFilterCycleDetector);
        final ClauseContext filterContext = factory.getSavedFilterContext(null, clause);
        assertEquals(expectedContext, filterContext);
    }

    @Test
    public void testGetSavedFilterContextExplicit() throws Exception {
        Set<ProjectIssueTypeContext> ctxs = new HashSet<ProjectIssueTypeContext>();
        ctxs.add(new ProjectIssueTypeContextImpl(new ProjectContextImpl(10L), new IssueTypeContextImpl("18")));
        ctxs.add(new ProjectIssueTypeContextImpl(new ProjectContextImpl(17L), AllIssueTypesContext.getInstance()));
        ctxs.add(new ProjectIssueTypeContextImpl(AllProjectsContext.INSTANCE, new IssueTypeContextImpl("3475832")));
        ctxs.add(ProjectIssueTypeContextImpl.createGlobalContext());

        final ClauseContext inputContext = new ClauseContextImpl(ctxs);

        ctxs = new HashSet<ProjectIssueTypeContext>();
        ctxs.add(new ProjectIssueTypeContextImpl(new ProjectContextImpl(10L), new IssueTypeContextImpl("18")));
        ctxs.add(new ProjectIssueTypeContextImpl(new ProjectContextImpl(17L), AllIssueTypesContext.getInstance()));
        ctxs.add(new ProjectIssueTypeContextImpl(AllProjectsContext.INSTANCE, new IssueTypeContextImpl("3475832")));
        ctxs.add(ProjectIssueTypeContextImpl.createGlobalContext());

        final ClauseContext expectedContext = new ClauseContextImpl(ctxs);

        final Clause clause = mock(Clause.class);
        when(clause.accept(dumbVisitor)).thenReturn(new QueryContextVisitor.ContextResult(inputContext, inputContext));

        final SavedFilterClauseContextFactory factory = new SavedFilterClauseContextFactory(savedFilterResolver, jqlOperandResolver, createFactory(), contextSetUtil, savedFilterCycleDetector);
        final ClauseContext filterContext = factory.getSavedFilterContext(null, clause);
        assertEquals(expectedContext, filterContext);
    }

    private QueryContextVisitor.QueryContextVisitorFactory createFactory() {

        return new QueryContextVisitor.QueryContextVisitorFactory(mock(ContextSetUtil.class), mock(SearchHandlerManager.class)) {
            @Override
            public QueryContextVisitor createVisitor(final ApplicationUser searcher) {
                return dumbVisitor;
            }
        };
    }

    class DumbVisitor extends QueryContextVisitor {
        public DumbVisitor() {
            super(null, null, null);
        }

        @Override
        public ContextResult createContext(final Clause clause) {
            return new ContextResult(ClauseContextImpl.createGlobalClauseContext(), ClauseContextImpl.createGlobalClauseContext());
        }
    }
}
