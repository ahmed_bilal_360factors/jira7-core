package com.atlassian.jira.jql.operand;

import com.atlassian.jira.jql.query.QueryCreationContext;
import com.atlassian.jira.jql.query.QueryCreationContextImpl;
import com.atlassian.jira.plugin.jql.function.JqlFunction;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.MessageSetImpl;
import com.atlassian.query.clause.TerminalClause;
import com.atlassian.query.clause.TerminalClauseImpl;
import com.atlassian.query.operand.FunctionOperand;
import com.atlassian.query.operator.Operator;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestFunctionOperandHandler {
    @Rule
    public MockitoRule mocks = MockitoJUnit.rule();

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    private final String fieldName = "field";
    private final ApplicationUser theUser = null;
    private final QueryCreationContext queryCreationContext = new QueryCreationContextImpl(theUser);
    @Mock
    private I18nHelper i18nHelper;

    @Test
    public void testConstructor() throws Exception {
        expectedException.expect(IllegalArgumentException.class);
        new FunctionOperandHandler(null, i18nHelper);
    }

    @Test
    public void testIsFunction() throws Exception {
        final JqlFunction jqlFunction = mock(JqlFunction.class);

        final FunctionOperandHandler operandHandler = new FunctionOperandHandler(jqlFunction, i18nHelper);

        assertTrue(operandHandler.isFunction());
    }

    @Test
    public void testIsEmpty() throws Exception {
        final JqlFunction jqlFunction = mock(JqlFunction.class);

        final FunctionOperandHandler operandHandler = new FunctionOperandHandler(jqlFunction, i18nHelper);

        assertFalse(operandHandler.isEmpty());
    }

    @Test
    public void testGetValues() throws Exception {
        final JqlFunction jqlFunction = mock(JqlFunction.class);
        final FunctionOperand operand = new FunctionOperand("blah");
        final TerminalClause terminalClause = new TerminalClauseImpl(fieldName, Operator.EQUALS, operand);
        final List<QueryLiteral> values = Collections.emptyList();
        when(jqlFunction.getValues(queryCreationContext, operand, terminalClause)).thenReturn(values);
        final FunctionOperandHandler operandHandler = new FunctionOperandHandler(jqlFunction, i18nHelper);

        assertEquals(values, operandHandler.getValues(queryCreationContext, operand, terminalClause));
    }

    @Test
    public void testValidate() throws Exception {
        final JqlFunction jqlFunction = mock(JqlFunction.class);
        final FunctionOperand operand = new FunctionOperand("blah");
        final TerminalClause terminalClause = new TerminalClauseImpl(fieldName, Operator.EQUALS, operand);
        final MessageSetImpl expectedMessages = new MessageSetImpl();

        when(jqlFunction.validate(null, operand, terminalClause)).thenReturn(expectedMessages);

        final FunctionOperandHandler operandHandler = new FunctionOperandHandler(jqlFunction, i18nHelper);
        assertEquals(expectedMessages, operandHandler.validate(null, operand, terminalClause));
    }

    @Test
    public void testIsList() throws Exception {
        final JqlFunction jqlFunction = mock(JqlFunction.class);
        when(jqlFunction.isList()).thenReturn(false);

        final FunctionOperandHandler operandHandler = new FunctionOperandHandler(jqlFunction, i18nHelper);

        assertFalse(operandHandler.isList());
    }

    @Test
    public void testGetJqlFunction() throws Exception {
        final JqlFunction jqlFunction = mock(JqlFunction.class);

        final FunctionOperandHandler operandHandler = new FunctionOperandHandler(jqlFunction, i18nHelper);

        assertSame(jqlFunction, operandHandler.getJqlFunction());
    }
}
