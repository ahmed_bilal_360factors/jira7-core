package com.atlassian.jira.plugin.webfragment.conditions;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.when;

public class TestIsIssueEditableCondition {
    @Rule
    public MockitoRule mockito = MockitoJUnit.rule();
    @Mock
    private Issue issue;
    @Mock
    private IssueManager issueManager;
    private IsIssueEditableCondition condition;

    @Before
    public void setUp() throws Exception {
        condition = new IsIssueEditableCondition(issueManager);
    }

    @Test
    public void testFalse() {
        final ApplicationUser fred = new MockApplicationUser("fred");
        when(issueManager.isEditable(issue)).thenReturn(false);

        assertFalse(condition.shouldDisplay(fred, issue, null));
    }

    @Test
    public void testTrue() {
        when(issueManager.isEditable(issue)).thenReturn(false);

        assertFalse(condition.shouldDisplay(null, issue, null));
    }


}
