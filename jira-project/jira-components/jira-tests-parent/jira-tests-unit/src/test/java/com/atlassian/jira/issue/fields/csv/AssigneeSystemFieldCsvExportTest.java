package com.atlassian.jira.issue.fields.csv;

import com.atlassian.jira.issue.export.FieldExportParts;
import com.atlassian.jira.issue.fields.AssigneeSystemField;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.mock.issue.MockIssue;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.NoopI18nHelper;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

public class AssigneeSystemFieldCsvExportTest {
    private final static String ASSIGNEE_USERNAME = "Assignee username";

    @Rule
    public final RuleChain mockito = MockitoMocksInContainer.forTest(this);


    @Mock
    private MockIssue issue;

    @Mock
    @AvailableInContainer
    private JiraAuthenticationContext authenticationContext;

    private I18nHelper i18nHelper = new NoopI18nHelper();

    @Mock
    private ApplicationUser assignee;

    @InjectMocks
    private AssigneeSystemField systemUnderTest;

    @Before
    public void setUp() {
        when(assignee.getName()).thenReturn(ASSIGNEE_USERNAME);
        when(authenticationContext.getI18nHelper()).thenReturn(i18nHelper);
    }

    @Test
    public void testCsvRepresentationForExpectedTypeAndValues() {
        when(issue.getAssignee()).thenReturn(assignee);

        final FieldExportParts representation = systemUnderTest.getRepresentationFromIssue(issue);

        assertThat(representation.getParts(), hasSize(1));
        assertThat(representation.getParts().get(0).getItemLabel(), equalTo(systemUnderTest.getName()));
        assertThat(representation.getParts().get(0).getValues()::iterator, contains(ASSIGNEE_USERNAME));
    }

    @Test
    public void testCsvRepresentationWhenThereIsNoAssignee() {
        when(issue.getAssignee()).thenReturn(null);

        final FieldExportParts representation = systemUnderTest.getRepresentationFromIssue(issue);

        assertThat(representation.getParts(), hasSize(1));
        assertThat(representation.getParts().get(0).getItemLabel(), equalTo(systemUnderTest.getName()));
        assertThat(representation.getParts().get(0).getValues()::iterator, contains(""));
    }
}
