package com.atlassian.jira.upgrade.tasks.role;

import com.atlassian.fugue.Option;
import com.atlassian.jira.CachingComponent;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.license.JiraLicenseManager;
import com.atlassian.jira.matchers.OptionMatchers;
import com.atlassian.jira.mock.MockApplicationProperties;
import com.atlassian.jira.mock.ofbiz.MockOfBizDelegator;
import com.atlassian.jira.test.util.lic.core.CoreLicenses;
import com.google.common.collect.ImmutableMap;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.List;
import java.util.stream.Collectors;

import static com.atlassian.jira.matchers.OptionMatchers.some;
import static com.atlassian.jira.test.util.lic.core.CoreLicenses.LICENSE_CORE;
import static com.atlassian.jira.test.util.lic.sd.ServiceDeskLicenses.LICENSE_SERVICE_DESK_ABP;
import static com.atlassian.jira.test.util.lic.sd.ServiceDeskLicenses.LICENSE_SERVICE_DESK_RBP;
import static com.atlassian.jira.test.util.lic.software.SoftwareLicenses.LICENSE_SOFTWARE;
import static com.atlassian.jira.upgrade.tasks.role.TestUtils.toLicenses;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.nullValue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class LicenseDaoImplTest {
    private static final String ENTITY = "ProductLicense";
    private static final String COLUMN_LICENSE = "license";

    @Rule
    public MockitoRule mockitoMocks = MockitoJUnit.rule();

    private MockApplicationProperties properties = new MockApplicationProperties();
    private MockOfBizDelegator delegator = new MockOfBizDelegator();
    @Mock(extraInterfaces = JiraLicenseManager.class)
    private CachingComponent licenseCache;
    private LicenseDaoImpl dao;

    @Before
    public void setUp() throws Exception {
        dao = new LicenseDaoImpl(properties, delegator, (JiraLicenseManager) licenseCache);
    }

    @Test
    public void get6xLicenseReturnsSomeForGoodLicense() {
        //given
        properties.setString(APKeys.JIRA_LICENSE, LICENSE_CORE.getLicenseString());

        //when
        final Option<License> license = dao.get6xLicense();

        //then
        assertThat(license, some(new License(LICENSE_CORE.getLicenseString())));
    }

    @Test
    public void get6xLicenseReturnsNoneWhenNoLicense() {
        //when
        final Option<License> license = dao.get6xLicense();

        //then
        assertThat(license, OptionMatchers.none());
    }

    @Test(expected = MigrationFailedException.class)
    public void get6xLicenseThrowsExceptionWhenInvalidLicensePresent() {
        //given
        properties.setString(APKeys.JIRA_LICENSE, "bad license string");

        //when
        dao.get6xLicense();
    }

    @Test
    public void getLicensesReturnsAllLicensesInStore() {
        //given
        setJiraLicenses(LICENSE_CORE, LICENSE_SOFTWARE);

        //when
        final Licenses licenses = dao.getLicenses();

        //then
        assertThat(licenses, equalTo(toLicenses(LICENSE_CORE, LICENSE_SOFTWARE)));
    }

    @Test(expected = MigrationFailedException.class)
    public void getLicensesThrowsMigrationFailedWithLicensesWithDuplicateKeys() {
        //given
        setJiraLicenses(LICENSE_SERVICE_DESK_ABP, LICENSE_SERVICE_DESK_RBP);

        //then
        dao.getLicenses();
    }

    @Test(expected = MigrationFailedException.class)
    public void getLicensesThrowsMigrationFailedExceptionWithInvalidLicenseInStore() {
        //given
        setJiraLicenses(LICENSE_SERVICE_DESK_ABP.getLicenseString(), "abc");

        //then
        dao.getLicenses();
    }

    @Test(expected = MigrationFailedException.class)
    public void setLicensesThrowsMigrationFailedWhenTryingToRemoveLicense() {
        //given
        setJiraLicenses(LICENSE_SERVICE_DESK_ABP, LICENSE_SOFTWARE);
        Licenses licenses = toLicenses(LICENSE_SOFTWARE);

        //then
        dao.setLicenses(licenses);
    }

    @Test
    public void setLicensesDoesNothingOnNoChange() {
        //given
        setJiraLicenses(LICENSE_SERVICE_DESK_ABP, LICENSE_SOFTWARE);
        Licenses licenses = toLicenses(LICENSE_SOFTWARE, LICENSE_SERVICE_DESK_ABP);

        //when
        dao.setLicenses(licenses);

        //then
        assertThat(readLicenses(), equalTo(licenses));
    }

    @Test
    public void setLicensesAddsNewLicense() {
        //given
        setJiraLicenses(LICENSE_SERVICE_DESK_ABP);
        Licenses expectedLicenses = toLicenses(LICENSE_SOFTWARE, LICENSE_SERVICE_DESK_ABP);

        //when
        dao.setLicenses(expectedLicenses);

        //then
        assertThat(readLicenses(), equalTo(expectedLicenses));
        verify(this.licenseCache).clearCache();
    }

    @Test
    public void removeJira6xLicenseRemovesLicense() {
        //given
        properties.setString(APKeys.JIRA_LICENSE, CoreLicenses.LICENSE_CORE.getLicenseString());

        //when
        dao.remove6xLicense();

        //then
        assertThat(properties.getString(APKeys.JIRA_LICENSE), nullValue());
    }

    @Test(expected = IllegalArgumentException.class)
    public void cotrFailsIfCacheNotPassed() {
        new LicenseDaoImpl(properties, delegator, mock(JiraLicenseManager.class));
    }

    private Licenses readLicenses() {
        List<License> licenses = delegator.findAll(ENTITY)
                .stream()
                .map(gv -> gv.getString(COLUMN_LICENSE))
                .map(License::new)
                .collect(Collectors.toList());

        return new Licenses(licenses);
    }

    private void setJiraLicenses(com.atlassian.jira.test.util.lic.License... licenses) {
        for (com.atlassian.jira.test.util.lic.License license : licenses) {
            delegator.createValue(ENTITY, ImmutableMap.of(COLUMN_LICENSE, license.getLicenseString()));
        }
    }

    private void setJiraLicenses(String... licenses) {
        for (String license : licenses) {
            delegator.createValue(ENTITY, ImmutableMap.of(COLUMN_LICENSE, license));
        }
    }
}