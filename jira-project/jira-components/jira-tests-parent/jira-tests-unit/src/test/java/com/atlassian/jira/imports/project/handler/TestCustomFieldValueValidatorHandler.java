package com.atlassian.jira.imports.project.handler;

import com.atlassian.core.util.map.EasyMap;
import com.atlassian.jira.action.issue.customfields.MockProjectImportableCustomFieldType;
import com.atlassian.jira.exception.ParseException;
import com.atlassian.jira.external.beans.ExternalProject;
import com.atlassian.jira.imports.project.core.BackupProject;
import com.atlassian.jira.imports.project.core.BackupProjectImpl;
import com.atlassian.jira.imports.project.customfield.ExternalCustomFieldValue;
import com.atlassian.jira.imports.project.customfield.ExternalCustomFieldValueImpl;
import com.atlassian.jira.imports.project.customfield.ProjectCustomFieldImporter;
import com.atlassian.jira.imports.project.mapper.CustomFieldMapper;
import com.atlassian.jira.imports.project.mapper.IssueTypeMapper;
import com.atlassian.jira.imports.project.mapper.ProjectImportMapper;
import com.atlassian.jira.imports.project.mapper.ProjectImportMapperImpl;
import com.atlassian.jira.imports.project.mapper.SimpleProjectImportIdMapperImpl;
import com.atlassian.jira.imports.project.parser.CustomFieldValueParser;
import com.atlassian.jira.imports.project.parser.CustomFieldValueParserImpl;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.context.IssueContextImpl;
import com.atlassian.jira.issue.customfields.CustomFieldType;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.junit.rules.InitMockitoMocks;
import com.atlassian.jira.util.MessageSet;
import com.atlassian.jira.util.MessageSetImpl;
import com.atlassian.jira.web.bean.I18nBean;
import com.atlassian.jira.web.bean.MockI18nBean;
import com.google.common.collect.ImmutableMap;
import org.hamcrest.Matcher;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.TestRule;
import org.mockito.Mock;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

import static com.atlassian.jira.imports.project.handler.CustomFieldValueValidatorHandler.CustomFieldConfigMapKey;
import static org.hamcrest.Matchers.emptyIterable;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @since v3.13
 */
public class TestCustomFieldValueValidatorHandler {
    @Rule
    public TestRule initMockitoMocks = new InitMockitoMocks(this);

    @Rule
    public ExpectedException exception = ExpectedException.none();

    private final Map<String, CustomFieldValueParser> parsers = new HashMap<>();

    private final MockI18nBean i18nBean = new MockI18nBean();

    @Mock
    private ProjectImportMapper mockProjectImportMapper;

    @Mock
    private CustomFieldMapper mockCustomFieldMapper;

    @Mock
    private IssueTypeMapper mockIssueTypeMapper;

    @Mock
    private ExternalCustomFieldValueImpl mockExternalCustomFieldValue;

    @Mock
    private ProjectCustomFieldImporter projectCustomFieldImporter;

    @Mock
    private CustomFieldManager customFieldManager;

    private ProjectImportMapper projectImportMapper;

    private BackupProject backupProject;

    @Before
    public void setUp() throws Exception {
        projectImportMapper = new ProjectImportMapperImpl(null, null);
        parsers.put(CustomFieldValueParser.CUSTOM_FIELD_VALUE_ENTITY_NAME, new CustomFieldValueParserImpl());
        final ExternalProject externalProject = new ExternalProject();
        externalProject.setId("10");
        backupProject = new BackupProjectImpl(externalProject, Collections.emptyList(), Collections.emptyList(),
                Collections.emptyList(), Collections.emptyList(), 0, ImmutableMap.of());

        when(mockProjectImportMapper.getProjectMapper()).thenReturn(new SimpleProjectImportIdMapperImpl());
        when(mockProjectImportMapper.getCustomFieldMapper()).thenReturn(mockCustomFieldMapper);
        when(mockProjectImportMapper.getIssueTypeMapper()).thenReturn(mockIssueTypeMapper);
    }

    @Test
    public void testHandleEntityRubbishEntityName() throws Exception {
        projectImportMapper.getProjectMapper().mapValue("10", "20");
        final CustomFieldValueValidatorHandler customFieldValueValidatorHandler = new CustomFieldValueValidatorHandler(backupProject,
                projectImportMapper, null, parsers);
        customFieldValueValidatorHandler.handleEntity("Basuro", null);
        assertThat(customFieldValueValidatorHandler.getValidationResults().keySet(), (Matcher) emptyIterable());
    }

    @Test
    public void testHandleEntityCustomFieldIsIgnored() throws Exception {
        projectImportMapper.getProjectMapper().mapValue("10", "20");
        projectImportMapper.getCustomFieldMapper().ignoreCustomField("12");

        final CustomFieldValueValidatorHandler customFieldValueValidatorHandler = new CustomFieldValueValidatorHandler(backupProject,
                projectImportMapper, null, parsers);
        customFieldValueValidatorHandler.handleEntity("CustomFieldValue", EasyMap.build("id", "123", "issue", "63", "customfield", "12"));
        assertThat(customFieldValueValidatorHandler.getValidationResults().keySet(), (Matcher) emptyIterable());
    }

    @Test
    public void testHandleEntityCustomFieldNullCustomFieldFound() throws Exception {
        projectImportMapper.getProjectMapper().mapValue("10", "20");
        projectImportMapper.getCustomFieldMapper().mapValue("12", "14");

        final CustomFieldValueValidatorHandler customFieldValueValidatorHandler = new CustomFieldValueValidatorHandler(backupProject,
                projectImportMapper, null, parsers) {
            @Override
            CustomField getCustomField(final ExternalCustomFieldValue externalCustomFieldValue) throws ParseException {
                return null;
            }
        };
        customFieldValueValidatorHandler.handleEntity("CustomFieldValue", EasyMap.build("id", "123", "issue", "63", "customfield", "12"));
        assertThat(customFieldValueValidatorHandler.getValidationResults().keySet(), (Matcher) emptyIterable());
    }

    @Test
    public void testHandleEntityCustomFieldNotImportableCustomFieldFound() throws Exception {
        projectImportMapper.getProjectMapper().mapValue("10", "20");
        projectImportMapper.getCustomFieldMapper().mapValue("12", "14");

        final CustomFieldValueValidatorHandler customFieldValueValidatorHandler = new CustomFieldValueValidatorHandler(backupProject,
                projectImportMapper, null, parsers) {
            @Override
            CustomField getCustomField(final ExternalCustomFieldValue externalCustomFieldValue) throws ParseException {
                final CustomField customField = mock(CustomField.class);
                final CustomFieldType customFieldType = mock(CustomFieldType.class);
                when(customField.getCustomFieldType()).thenReturn(customFieldType);
                return customField;
            }
        };
        customFieldValueValidatorHandler.handleEntity("CustomFieldValue", EasyMap.build("id", "123", "issue", "63", "customfield", "12"));
        assertThat(customFieldValueValidatorHandler.getValidationResults().keySet(), (Matcher) emptyIterable());
    }

    @Test
    public void testHandleEntityCustomFieldHappyPath() throws Exception {
        projectImportMapper.getProjectMapper().mapValue("10", "20");
        projectImportMapper.getCustomFieldMapper().mapValue("12", "14");

        final AtomicBoolean validateCalled = new AtomicBoolean(false);
        final CustomFieldValueValidatorHandler customFieldValueValidatorHandler = new CustomFieldValueValidatorHandler(backupProject, projectImportMapper, null, parsers) {
            @Override
            CustomField getCustomField(final ExternalCustomFieldValue externalCustomFieldValue) throws ParseException {
                final CustomField customField = mock(CustomField.class);
                when(customField.getCustomFieldType()).thenReturn(new MockProjectImportableCustomFieldType(null));
                return customField;
            }

            @Override
            void validateCustomFieldValueWithField(final CustomField customField, final ProjectCustomFieldImporter projectCustomFieldImporter, final ExternalCustomFieldValue customFieldValue) {
                validateCalled.set(true);
            }
        };
        customFieldValueValidatorHandler.handleEntity("CustomFieldValue", EasyMap.build("id", "123", "issue", "63", "customfield", "12"));
        assertThat(customFieldValueValidatorHandler.getValidationResults().keySet(), (Matcher) emptyIterable());
        assertTrue(validateCalled.get());
    }

    @Test
    public void testValidateCustomFieldValueWithFieldNoMappedIssueType() {
        when(mockCustomFieldMapper.getIssueTypeForIssue("1111")).thenReturn("2222");
        when(mockIssueTypeMapper.getMappedId("2222")).thenReturn(null);

        final CustomFieldValueValidatorHandler customFieldValueValidatorHandler = new CustomFieldValueValidatorHandler(backupProject, mockProjectImportMapper, null, parsers);

        final ExternalCustomFieldValueImpl mockExternalCustomFieldValue = mock(ExternalCustomFieldValueImpl.class);
        when(mockExternalCustomFieldValue.getIssueId()).thenReturn("1111");

        customFieldValueValidatorHandler.validateCustomFieldValueWithField(null, null, mockExternalCustomFieldValue);

        assertThat(customFieldValueValidatorHandler.getValidationResults().keySet(), (Matcher) emptyIterable());
    }

    @Test
    public void testValidateCustomFieldValueWithField() {
        when(mockCustomFieldMapper.getIssueTypeForIssue("1111")).thenReturn("2222");
        when(mockIssueTypeMapper.getMappedId("2222")).thenReturn("3333");

        final FieldConfig fieldConfig = mock(FieldConfig.class);
        final CustomFieldValueValidatorHandler customFieldValueValidatorHandler = new CustomFieldValueValidatorHandler(backupProject,
                mockProjectImportMapper, null, parsers) {
            @Override
            FieldConfig getCustomFieldConfig(final CustomField customField, final String newIssueTypeId) {
                return fieldConfig;
            }

            @Override
            I18nBean getI18nFromCustomField(final CustomField customField) {
                return i18nBean;
            }
        };

        when(mockExternalCustomFieldValue.getIssueId()).thenReturn("1111");
        when(mockExternalCustomFieldValue.getCustomFieldId()).thenReturn("5555");

        final MessageSet messageSet = new MessageSetImpl();
        messageSet.addErrorMessage("I am an error");
        when(projectCustomFieldImporter.canMapImportValue(eq(mockProjectImportMapper), eq(mockExternalCustomFieldValue), eq(fieldConfig), eq(i18nBean))).thenReturn(messageSet);

        customFieldValueValidatorHandler.validateCustomFieldValueWithField(null, projectCustomFieldImporter,
                mockExternalCustomFieldValue);

        assertThat(customFieldValueValidatorHandler.getValidationResults().keySet(), (Matcher) hasSize(1));
        assertTrue(((MessageSet) customFieldValueValidatorHandler.getValidationResults().get("5555")).hasAnyErrors());
        assertEquals("I am an error",
                ((MessageSet) customFieldValueValidatorHandler.getValidationResults().get("5555")).getErrorMessages().iterator().next());
    }

    @Test
    public void testValidateCustomFieldValueWithFieldExitingMessageSet() {
        when(mockCustomFieldMapper.getIssueTypeForIssue("1111")).thenReturn("2222");
        when(mockIssueTypeMapper.getMappedId("2222")).thenReturn("3333");

        final FieldConfig fieldConfig = mock(FieldConfig.class);
        final CustomFieldValueValidatorHandler customFieldValueValidatorHandler = new CustomFieldValueValidatorHandler(backupProject, mockProjectImportMapper, null, parsers) {
            @Override
            FieldConfig getCustomFieldConfig(final CustomField customField, final String newIssueTypeId) {
                return fieldConfig;
            }

            @Override
            I18nBean getI18nFromCustomField(final CustomField customField) {
                return i18nBean;
            }
        };
        customFieldValueValidatorHandler.fieldMessages.put("5555", new MessageSetImpl());

        when(mockExternalCustomFieldValue.getIssueId()).thenReturn("1111");
        when(mockExternalCustomFieldValue.getCustomFieldId()).thenReturn("5555");

        final MessageSet messageSet = new MessageSetImpl();
        messageSet.addErrorMessage("I am an error");
        when(projectCustomFieldImporter.canMapImportValue(eq(mockProjectImportMapper), eq(mockExternalCustomFieldValue), eq(fieldConfig), eq(i18nBean))).thenReturn(messageSet);

        customFieldValueValidatorHandler.validateCustomFieldValueWithField(null, projectCustomFieldImporter,
                mockExternalCustomFieldValue);

        assertThat(customFieldValueValidatorHandler.getValidationResults().keySet(), (Matcher) hasSize(1));
        assertTrue(((MessageSet) customFieldValueValidatorHandler.getValidationResults().get("5555")).hasAnyErrors());
        assertEquals("I am an error",
                ((MessageSet) customFieldValueValidatorHandler.getValidationResults().get("5555")).getErrorMessages().iterator().next());
    }

    @Test
    public void testGetCustomFieldNoMappedId() throws ParseException {
        when(mockCustomFieldMapper.getMappedId("5555")).thenReturn(null);
        when(mockProjectImportMapper.getCustomFieldMapper()).thenReturn(mockCustomFieldMapper);
        when(mockExternalCustomFieldValue.getCustomFieldId()).thenReturn("5555");

        final CustomFieldValueValidatorHandler customFieldValueValidatorHandler = new CustomFieldValueValidatorHandler(backupProject, mockProjectImportMapper, null, parsers);

        exception.expect(ParseException.class);
        exception.expectMessage("During custom field value validation we have encountered a custom field with id '5555' which the mapper does not know about.");

        customFieldValueValidatorHandler.getCustomField(mockExternalCustomFieldValue);
    }

    @Test
    public void testGetCustomFieldBadMappedId() throws ParseException {
        when(mockCustomFieldMapper.getMappedId("5555")).thenReturn("badnumber");
        when(mockExternalCustomFieldValue.getCustomFieldId()).thenReturn("5555");

        final CustomFieldValueValidatorHandler customFieldValueValidatorHandler = new CustomFieldValueValidatorHandler(backupProject, mockProjectImportMapper, null, parsers);

        exception.expect(ParseException.class);
        exception.expectMessage("Encountered a custom field value with a custom field id 'badnumber' which is not a valid number.");

        customFieldValueValidatorHandler.getCustomField(mockExternalCustomFieldValue);
    }

    @Test
    public void testGetCustomField() throws ParseException {
        when(mockCustomFieldMapper.getMappedId("5555")).thenReturn("1111");
        CustomField expectedCustomField = mock(CustomField.class);
        when(customFieldManager.getCustomFieldObject(1111L)).thenReturn(expectedCustomField);
        when(mockExternalCustomFieldValue.getCustomFieldId()).thenReturn("5555");

        final CustomFieldValueValidatorHandler customFieldValueValidatorHandler = new CustomFieldValueValidatorHandler(backupProject, mockProjectImportMapper, customFieldManager, parsers);
        CustomField returnedCustomField = customFieldValueValidatorHandler.getCustomField(mockExternalCustomFieldValue);
        assertThat(returnedCustomField, equalTo(expectedCustomField));
    }

    @Test
    public void testGetCustomFieldConfig() {
        final ProjectImportMapper mockProjectImportMapper = mock(ProjectImportMapper.class);
        when(mockProjectImportMapper.getProjectMapper()).thenReturn(new SimpleProjectImportIdMapperImpl());

        final CustomFieldValueValidatorHandler customFieldValueValidatorHandler = new CustomFieldValueValidatorHandler(backupProject, mockProjectImportMapper, null, parsers);

        final FieldConfig fieldConfig = mock(FieldConfig.class);

        final CustomField customField = mock(CustomField.class);
        when(customField.getId()).thenReturn("5555");
        when(customField.getRelevantConfig(eq(new IssueContextImpl(null, "2")))).thenReturn(fieldConfig);
        assertEquals(fieldConfig, customFieldValueValidatorHandler.getCustomFieldConfig(customField, "2"));
    }

    @Test
    public void testGetCustomFieldConfigUsesCachedValue() {
        final CustomFieldValueValidatorHandler customFieldValueValidatorHandler = new CustomFieldValueValidatorHandler(backupProject, mockProjectImportMapper, null, parsers);

        final FieldConfig fieldConfig = mock(FieldConfig.class);
        customFieldValueValidatorHandler.customFieldConfigMap.put(new CustomFieldConfigMapKey("5555", "2"), fieldConfig);

        final CustomField customField = mock(CustomField.class);
        when(customField.getId()).thenReturn("5555");

        assertEquals(fieldConfig, customFieldValueValidatorHandler.getCustomFieldConfig(customField, "2"));
    }
}
