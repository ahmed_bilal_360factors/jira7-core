package com.atlassian.jira.application;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.crowd.exception.CrowdException;
import com.atlassian.crowd.exception.OperationNotPermittedException;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.fugue.Option;
import com.atlassian.jira.config.group.GroupConfigurationIdentifier;
import com.atlassian.jira.junit.rules.Log4jLogger;
import com.atlassian.jira.license.JiraLicenseManager;
import com.atlassian.jira.license.LicenseDetails;
import com.atlassian.jira.license.MockLicenseDetails;
import com.atlassian.jira.license.MockLicensedApplications;
import com.atlassian.jira.matchers.OptionMatchers;
import com.atlassian.jira.message.MockMessageUtilFactory;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.permission.GlobalPermissionType;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.security.groups.MockGroupManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.MockGroup;
import com.atlassian.jira.user.util.MockUserManager;
import com.atlassian.jira.util.johnson.JohnsonProvider;
import com.atlassian.johnson.JohnsonEventContainer;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collection;
import java.util.Collections;

import static com.atlassian.jira.application.ApplicationKeys.CORE;
import static com.atlassian.jira.application.ApplicationKeys.SERVICE_DESK;
import static java.util.Collections.emptyList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

/**
 * @since 7.0
 */
@RunWith(MockitoJUnitRunner.class)
public class ApplicationConfigurationHelperForSetupTest {
    @Rule
    public ExpectedException exception = ExpectedException.none();
    @Rule
    public final Log4jLogger log4jLogger = new Log4jLogger();

    private MockApplicationManager applicationManager;
    private MockApplicationRoleManager applicationRoleManager;
    private MockApplicationRoleStore applicationRoleStore;
    private MockGroupManager groupManager;
    private ApplicationConfigurationHelper appConfigHelper;
    @Mock
    private GlobalPermissionManager globalPermissionManager;
    @Mock
    private EventPublisher eventPublisher;
    @Mock
    private JiraAuthenticationContext authenticationContext;
    @Mock
    private JiraLicenseManager licenseManager;
    private Collection<Group> ADMIN_GROUPS = Lists.newArrayList(new MockGroup("admin-group"), new MockGroup("another-admin-group"));
    @Mock
    private JohnsonProvider johnsonProvider;
    @Mock
    private JohnsonEventContainer johnsonEventContainer;

    private static ApplicationRoleMatcher createMatcher(final ApplicationKey serviceDesk) {
        return new ApplicationRoleMatcher().key(serviceDesk).name(serviceDesk.value());
    }

    @Before
    public void setUp() throws Exception {
        groupManager = new MockGroupManager();
        when(authenticationContext.getLoggedInUser()).thenReturn(null);
        when(licenseManager.getLicenses()).thenReturn(Sets.newHashSet());
        appConfigHelper = newAppConfigHelper(groupManager);
        when(globalPermissionManager.getGroupsWithPermission(GlobalPermissionKey.ADMINISTER)).thenReturn(ADMIN_GROUPS);
        setupDefaultGlobalPermissions();
        when(johnsonEventContainer.getEvents()).thenReturn(ImmutableList.of());
        when(johnsonProvider.getContainer()).thenReturn(johnsonEventContainer);
    }

    private void setupDefaultGlobalPermissions() {
        for (GlobalPermissionKey gpKey : GlobalPermissionKey.DEFAULT_APP_GLOBAL_PERMISSIONS) {
            when(globalPermissionManager.getGlobalPermission(gpKey))
                    .thenReturn(Option.some(new GlobalPermissionType(gpKey.getKey(), null, null, false)));
        }
    }

    private ApplicationConfigurationHelper newAppConfigHelper(GroupManager groupManager) {
        applicationRoleManager = new MockApplicationRoleManager();
        return newAppConfigHelper(groupManager, applicationRoleManager);
    }

    private ApplicationConfigurationHelper newAppConfigHelper(GroupManager groupManager,
                                                              ApplicationRoleManager applicationRoleManager) {
        applicationManager = new MockApplicationManager();
        applicationRoleStore = new MockApplicationRoleStore();
        return new ApplicationConfigurationHelper(applicationManager, groupManager,
                applicationRoleManager, globalPermissionManager, new MockUserManager(),
                applicationRoleStore, new MockMessageUtilFactory(), mock(GroupConfigurationIdentifier.class),
                eventPublisher, authenticationContext, licenseManager, johnsonProvider);
    }

    @Test
    public void initialiseApplicationIsNoOpWhenRenaissanceNotEnabled() {
        GroupManager groupManager = mock(GroupManager.class);
        ApplicationRoleManager roleManager = mock(ApplicationRoleManager.class);
        appConfigHelper = newAppConfigHelper(groupManager, roleManager);

        //When
        appConfigHelper.configureApplicationsForSetup(emptyList(), true);

        //then
        verifyNoMoreInteractions(groupManager);
        verify(roleManager, never()).setRole(any());
    }

    @Test
    public void initialiseApplicationsCreatesAndAddsDefaultGroupsToAllLicensedAndInstalledApplications() {
        //Given
        final MockApplication sdApplication = applicationManager.addApplication(SERVICE_DESK);
        final MockApplication coreApplication = applicationManager.addApplication(CORE);

        Group group2 = groupManager.createGroup("group2");
        Group coreDefault = groupManager.createGroup(coreApplication.getDefaultGroup());

        //Empty Application role.
        applicationRoleManager.createRole(SERVICE_DESK);

        //Has the default groups + another group.
        final MockApplicationRole role = applicationRoleManager.createRole(CORE).groups(group2,
                coreDefault).defaultGroups(coreDefault);
        applicationRoleStore.save(role);

        //When
        when(licenseManager.getLicenses()).thenReturn(Sets.newHashSet(mockLicenseDetails(SERVICE_DESK, CORE)));
        appConfigHelper.configureApplicationsForSetup(Collections.emptyList(), true);

        //Then
        final Group serviceDeskGroup = groupManager.getGroup(sdApplication.getDefaultGroup());
        assertThat("SD Default created", serviceDeskGroup, notNullValue());

        assertThat("SD Default added to role.", applicationRoleManager.getRole(SERVICE_DESK),
                OptionMatchers.some(createMatcher(SERVICE_DESK)
                        .groups(serviceDeskGroup).defaultGroups(serviceDeskGroup).selectedByDefault(true)));

        assertThat("Core role updated.", applicationRoleManager.getRole(CORE),
                OptionMatchers.some(createMatcher(CORE)
                        .groups(group2, coreDefault).defaultGroups(coreDefault).selectedByDefault(true)));
        assertDefaultGroupsAddedToDefaultGlobalPermissions(serviceDeskGroup.getName());
        assertAllAdminGroupsAddedToDefaultGlobalPermissions();
        assertConfigurationEventForApplications(SERVICE_DESK, CORE);
    }

    @Test
    public void initialiseApplicationsAddsPassedGroupsToAllLicensedRoles() {
        //Given
        final MockApplication sdApplication = applicationManager.addApplication(SERVICE_DESK);

        Group group3 = groupManager.createGroup("group3");
        Group group4 = groupManager.createGroup("group4");

        //Application role that has group3 already configured.
        applicationRoleManager.createRole(SERVICE_DESK).groups(group3);

        //Empty core role.
        applicationRoleManager.createRole(CORE);

        //When
        when(licenseManager.getLicenses()).thenReturn(Sets.newHashSet(mockLicenseDetails(SERVICE_DESK, CORE)));
        appConfigHelper.configureApplicationsForSetup(ImmutableList.of(group3, group4), true);

        //Then
        final String expectedCoreName = "jira-core-users";
        final Group serviceDeskGroup = groupManager.getGroup(sdApplication.getDefaultGroup());
        final Group coreDefaultGroup = groupManager.getGroup(expectedCoreName);
        assertThat("SD Default created", serviceDeskGroup, notNullValue());
        assertThat("Core Default created", coreDefaultGroup, notNullValue());

        assertThat("SD Default added to role.", applicationRoleManager.getRole(SERVICE_DESK),
                OptionMatchers.some(createMatcher(SERVICE_DESK)
                        .groups(serviceDeskGroup, group3, group4)
                        .defaultGroups(serviceDeskGroup)
                        .selectedByDefault(true)));

        assertThat("Core role updated.", applicationRoleManager.getRole(CORE),
                OptionMatchers.some(createMatcher(CORE)
                        .groups(group3, group4, coreDefaultGroup)
                        .defaultGroups(coreDefaultGroup)
                        .selectedByDefault(true)));
        assertDefaultGroupsAddedToDefaultGlobalPermissions(serviceDeskGroup.getName());
        assertDefaultGroupsAddedToDefaultGlobalPermissions(coreDefaultGroup.getName());
        assertAllAdminGroupsAddedToDefaultGlobalPermissions();
        assertConfigurationEventForApplications(SERVICE_DESK, CORE);
    }

    @Test
    public void initialiseApplicationsAndDoesNotPublishEventIfFlagIsFalse() {
        Group group3 = groupManager.createGroup("group3");

        applicationRoleManager.createRole(CORE);

        //When
        when(licenseManager.getLicenses()).thenReturn(Sets.newHashSet(mockLicenseDetails(CORE)));
        appConfigHelper.configureApplicationsForSetup(ImmutableList.of(group3), false);

        //Then
        final String expectedCoreName = "jira-core-users";
        final Group coreDefaultGroup = groupManager.getGroup(expectedCoreName);
        assertThat("Core Default created", coreDefaultGroup, notNullValue());


        assertThat("Core role updated.", applicationRoleManager.getRole(CORE),
                OptionMatchers.some(createMatcher(CORE)
                        .groups(group3, coreDefaultGroup)
                        .defaultGroups(coreDefaultGroup)
                        .selectedByDefault(true)));
        assertDefaultGroupsAddedToDefaultGlobalPermissions(coreDefaultGroup.getName());
        assertAllAdminGroupsAddedToDefaultGlobalPermissions();
        verify(eventPublisher, never()).publish(any());
    }

    @Test
    public void initialiseApplicationsCreatesAndAddsDefaultGroupsToAllLicensedApplications() {
        //Given
        final MockApplication sdApplication = applicationManager.addApplication(SERVICE_DESK);

        //Empty Application role.
        applicationRoleManager.createRole(SERVICE_DESK);
        //This application role is licensed but not installed.
        applicationRoleManager.createRole(CORE);

        //When
        when(licenseManager.getLicenses()).thenReturn(Sets.newHashSet(mockLicenseDetails(SERVICE_DESK, CORE)));
        appConfigHelper.configureApplicationsForSetup(emptyList(), true);

        //Then
        final String expectedCoreName = "jira-core-users";
        final Group coreDefault = groupManager.getGroup(expectedCoreName);
        Group sdDefault = groupManager.createGroup(sdApplication.getDefaultGroup());
        assertThat("Core Default created", coreDefault, notNullValue());

        assertThat(applicationRoleManager.getRole(SERVICE_DESK),
                OptionMatchers.some(createMatcher(SERVICE_DESK)
                        .groups(sdDefault).defaultGroups(sdDefault).selectedByDefault(true)));

        assertThat(applicationRoleManager.getRole(CORE),
                OptionMatchers.some(createMatcher(CORE)
                        .groups(coreDefault).defaultGroups(coreDefault).selectedByDefault(true)));
        assertDefaultGroupsAddedToDefaultGlobalPermissions(coreDefault.getName());
        assertDefaultGroupsAddedToDefaultGlobalPermissions(sdDefault.getName());
        assertAllAdminGroupsAddedToDefaultGlobalPermissions();
        assertConfigurationEventForApplications(SERVICE_DESK, CORE);
    }

    @Test
    public void initialiseApplicationsThrowsErrorWhenPassedNull() {
        //given
        exception.expect(IllegalArgumentException.class);

        //when
        appConfigHelper.configureApplicationsForSetup(null, true);

        //then an exception is thrown.
        assertNoConfigurationEvent();
    }

    @Test
    public void setupAdminNoOpWhenRenaissanceDisabled() {
        //given
        ApplicationUser user = new MockApplicationUser("admin");
        GroupManager groupManager = mock(GroupManager.class);
        ApplicationRoleManager roleManager = mock(ApplicationRoleManager.class);
        appConfigHelper = newAppConfigHelper(groupManager, roleManager);

        //When
        appConfigHelper.setupAdminForDefaultApplications(user);

        //then
        verifyNoMoreInteractions(groupManager);
        verify(roleManager, never()).setRole(any());
    }

    @Test
    public void setupAdminForDefaultApplicationsAddsAdminsToAllDefaultGroups() {
        //given
        final String groupOne = "one";
        final String groupTwo = "two";
        applicationRoleManager.createRole(CORE)
                .groupNames(groupOne, groupTwo)
                .defaultGroupNames(groupOne);

        applicationRoleManager.createRole(ApplicationKeys.SOFTWARE)
                .groupNames(groupOne, groupTwo)
                .defaultGroupNames();

        final ApplicationUser admin = new MockApplicationUser("admin");

        //when
        appConfigHelper.setupAdminForDefaultApplications(admin);

        //then
        assertThat(groupManager.isUserInGroup(admin, groupOne), is(true));
        assertThat(groupManager.isUserInGroup(admin, groupTwo), is(false));
    }

    @Test
    public void setupAdminForDefaultApplicationsAddsAdminsToDefaultGroupsGroupsSomeAlreadyAssigned() {
        //given
        final Group groupOne = groupManager.addGroup("one");
        final Group groupTwo = groupManager.addGroup("two");
        applicationRoleManager.createRole(CORE)
                .groups(groupOne, groupTwo)
                .groups(groupOne);

        applicationRoleManager.createRole(ApplicationKeys.SOFTWARE)
                .groups(groupOne, groupTwo)
                .groups(groupOne);

        final ApplicationUser admin = new MockApplicationUser("admin");
        groupManager.addUserToGroup(admin, groupOne);

        //when
        appConfigHelper.setupAdminForDefaultApplications(admin);

        //then
        assertThat(groupManager.isUserInGroup(admin, groupOne), is(true));
        assertThat(groupManager.isUserInGroup(admin, groupTwo), is(false));
    }

    @Test
    public void testThatWhenWeAreUnableToAddUserToGroupWarningIsLogged() throws CrowdException {
        //given
        final GroupManager groupManager = Mockito.mock(GroupManager.class);
        final OperationNotPermittedException cause = new OperationNotPermittedException();
        doThrow(cause).when(groupManager).addUserToGroup(any(), any());

        appConfigHelper = newAppConfigHelper(groupManager);
        final String groupOne = "one";
        applicationRoleManager.createRole(SERVICE_DESK).groupNames(groupOne)
                .defaultGroupNames(groupOne);

        //when
        appConfigHelper.setupAdminForDefaultApplications(new MockApplicationUser("admin"));

        assertThat(log4jLogger.getMessage(),
                containsString("Unable to add user to group during application configuration."));
    }

    @Test
    public void testThatWhenWeAreUnableToCreateGroupWarningIsLogged() throws CrowdException {
        //given
        final GroupManager groupManager = Mockito.mock(GroupManager.class);
        final OperationNotPermittedException cause = new OperationNotPermittedException();
        doThrow(cause).when(groupManager).createGroup(any());

        appConfigHelper = newAppConfigHelper(groupManager);
        applicationRoleManager.createRole(SERVICE_DESK).groupNames("one");

        //when
        appConfigHelper.configureApplicationsForImport(mockLicenseDetails(SERVICE_DESK));

        assertThat(log4jLogger.getMessage(),
                containsString("Unable to create group during application configuration."));
    }

    @Test
    public void setupAdminForDefaultGivesErrorWhenUserIsNull() {
        //given
        exception.expect(IllegalArgumentException.class);

        //when
        appConfigHelper.setupAdminForDefaultApplications(null);

        //then an exception is thrown.

        assertNoConfigurationEvent();
    }

    private void assertNoConfigurationEvent() {
        verify(eventPublisher, never()).publish(any(ApplicationConfigurationEvent.class));
    }

    private void assertConfigurationEventForApplications(final ApplicationKey... keys) {
        ArgumentCaptor<ApplicationConfigurationEvent> eventCaptor =
                ArgumentCaptor.forClass(ApplicationConfigurationEvent.class);
        verify(eventPublisher, times(1)).publish(eventCaptor.capture());
        eventCaptor.getValue().getApplicationsConfigured().containsAll(Sets.newHashSet(keys));
    }

    private void assertDefaultGroupsAddedToDefaultGlobalPermissions(final String defaultGroupName) {
        for (GlobalPermissionKey gpKey : GlobalPermissionKey.DEFAULT_APP_GLOBAL_PERMISSIONS) {
            final GlobalPermissionType globalPermissionType = globalPermissionManager.getGlobalPermission(gpKey).get();
            assertThat(gpKey.getKey(), is(globalPermissionType.getKey()));
            verify(globalPermissionManager).addPermission(globalPermissionType, defaultGroupName);
        }
    }

    private void assertAllAdminGroupsAddedToDefaultGlobalPermissions() {
        ADMIN_GROUPS.forEach(adminGroup -> assertDefaultGroupsAddedToDefaultGlobalPermissions(adminGroup.getName()));
    }

    private LicenseDetails mockLicenseDetails(final ApplicationKey... keys) {
        final MockLicenseDetails licenseDetails = new MockLicenseDetails();
        final MockLicensedApplications licensedApplications = new MockLicensedApplications(keys);
        licenseDetails.setLicensedApplications(licensedApplications);
        return licenseDetails;
    }
}