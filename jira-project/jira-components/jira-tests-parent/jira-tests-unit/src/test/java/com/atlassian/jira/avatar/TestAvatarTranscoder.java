package com.atlassian.jira.avatar;

import org.apache.batik.transcoder.TranscoderInput;
import org.apache.batik.transcoder.TranscoderOutput;
import org.apache.batik.transcoder.image.PNGTranscoder;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.TemporaryFolder;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import javax.annotation.Nonnull;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertFalse;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

public class TestAvatarTranscoder {
    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Rule
    public TemporaryFolder temporaryFolder = new TemporaryFolder();

    @Mock
    private AvatarImageDataStorage avatarImageDataStorage;

    @Mock
    private AvatarTagger avatarTagger;

    @Mock
    private PNGTranscoder transcoder;

    @Mock
    private File rasterizedFile;

    @Mock
    private Avatar avatar;

    private final Avatar.Size size = Avatar.Size.NORMAL;

    private AvatarTranscoder avatarTranscoder;

    @Before
    public void setup() {
        avatarTranscoder = new AvatarTranscoderImpl(avatarTagger, avatarImageDataStorage) {
            @Override
            PNGTranscoder createPngTranscoder() {
                return transcoder;
            }

            @Override
            File getTranscodedFile(@Nonnull final Avatar avatar, @Nonnull final Avatar.Size size) {
                assertThat(avatar, equalTo(TestAvatarTranscoder.this.avatar));
                assertThat(size, equalTo(TestAvatarTranscoder.this.size));
                return rasterizedFile;
            }
        };

        when(avatar.isSystemAvatar()).thenReturn(true);
    }

    @Test
    public void testGetOrCreateRasterizedExistingFile() throws IOException {
        when(rasterizedFile.exists()).thenReturn(true);

        InputStream inputStream = mock(InputStream.class);
        final File actual = avatarTranscoder.getOrCreateRasterizedAvatarFile(avatar, size, inputStream);

        assertThat(actual, equalTo(rasterizedFile));
        verifyZeroInteractions(transcoder);
        verifyZeroInteractions(avatarTagger);
    }

    @Test
    public void testGetOrCreateRasterizedFileShouldThrowExceptionForNonSystemAvatar() throws IOException {
        expectedException.expect(IllegalArgumentException.class);

        when(avatar.isSystemAvatar()).thenReturn(false);

        InputStream inputStream = mock(InputStream.class);
        avatarTranscoder.getOrCreateRasterizedAvatarFile(avatar, size, inputStream);
    }

    @Test
    public void testGetOrCreateRasterizedFileShouldNotCreateNewFileWhenExceptionOccurs() throws Exception {
        final String fileName = "bug.svg";
        doThrow(new RuntimeException()).when(transcoder).transcode(any(TranscoderInput.class), any(TranscoderOutput.class));
        when(avatar.getFileName()).thenReturn(fileName);
        final File transcodedFile = new File(temporaryFolder.getRoot(), fileName);

        avatarTranscoder = new AvatarTranscoderImpl(avatarTagger, avatarImageDataStorage) {
            @Override
            PNGTranscoder createPngTranscoder() {
                return transcoder;
            }

            @Override
            File getTranscodedFile(@Nonnull final Avatar avatar, @Nonnull final Avatar.Size size) {
                assertThat(avatar, equalTo(TestAvatarTranscoder.this.avatar));
                assertThat(size, equalTo(TestAvatarTranscoder.this.size));

                return transcodedFile;
            }
        };

        try {
            InputStream inputStream = mock(InputStream.class);
            avatarTranscoder.getOrCreateRasterizedAvatarFile(avatar, size, inputStream);
        } catch (final Exception ignore) {
        }

        assertFalse(transcodedFile.exists());
    }
}