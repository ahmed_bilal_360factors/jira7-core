package com.atlassian.jira.issue.index.indexers.impl;

import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;

import static com.atlassian.jira.issue.index.DocumentConstants.ISSUE_PROPERTY_VALUE_PREFIX;

public class ContainsIssuePropertyMatcher extends BaseMatcher<String> {
    private final String propertyKey;
    private final String path;

    public ContainsIssuePropertyMatcher(String propertyKey, String path) {
        this.propertyKey = propertyKey;
        this.path = path;
    }

    @Override
    public boolean matches(Object o) {
        return getPropertyFieldName().equals(o);
    }

    private String getPropertyFieldName() {
        return ISSUE_PROPERTY_VALUE_PREFIX + propertyKey + "$" + path;
    }

    @Override
    public void describeTo(Description description) {
        description.appendText("expected value but not found: ").appendValue(getPropertyFieldName());
    }
}
