package com.atlassian.jira.upgrade.mock;

import com.atlassian.application.api.Application;
import com.atlassian.application.api.ApplicationKey;
import com.atlassian.jira.entity.EntityEngine;
import com.atlassian.jira.license.ProductLicense;
import com.atlassian.jira.mock.ofbiz.MockGenericValue;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.security.GlobalPermissionEntry;
import com.atlassian.jira.test.util.lic.core.CoreLicenses;
import com.atlassian.jira.test.util.lic.ela.ElaLicenses;
import com.atlassian.jira.test.util.lic.other.OtherLicenses;
import com.atlassian.jira.test.util.lic.sd.ServiceDeskLicenses;
import com.atlassian.jira.test.util.lic.software.SoftwareLicenses;
import com.atlassian.jira.util.collect.MapBuilder;
import com.atlassian.jira.util.lang.Pair;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import org.ofbiz.core.entity.GenericValue;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.atlassian.jira.entity.Entity.GLOBAL_PERMISSION_ENTRY;
import static com.atlassian.jira.entity.Entity.PRODUCT_LICENSE;
import static com.google.common.collect.Maps.newHashMapWithExpectedSize;
import static com.google.common.collect.Sets.newHashSet;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Mock helper class used to mock licenses and global permissions used during the JIRA renaissance migration upgrade
 * tasks.
 *
 * @since v7.0
 */
public final class MockRenaissanceUpgradeInstance {

    private static final String globalPermissionUsers = "USE";
    private static final String globalPermissionAdmin = "ADMINISTER";
    private static final String globalPermissionSysAdmin = "SYSTEM_ADMIN";

    private static final String ENTITY_NAME = "LicenseRoleGroup";
    private static final String COLUMN_APP_KEY = "licenseRoleName";
    private static final String COLUMN_GROUP_ID = "groupId";
    private static final String COLUMN_PRIMARY_INDICATOR = "primaryGroup";

    private final EntityEngine entityEngine;
    private final OfBizDelegator ofBizDelegator;

    private EntityEngine.SelectFromContext<ProductLicense> licenseSelectFromContext;
    private EntityEngine.WhereContext<ProductLicense> licenseWhereContext;
    private EntityEngine.SelectFromContext<GlobalPermissionEntry> gpFromContext;
    private EntityEngine.SelectFromContext<GlobalPermissionEntry> gpSelectFromContext;
    private EntityEngine.WhereEqualContext<GlobalPermissionEntry> gpUseWhereContext;
    private EntityEngine.WhereEqualContext<GlobalPermissionEntry> gpAdminWhereContext;
    private EntityEngine.WhereEqualContext<GlobalPermissionEntry> gpSysAdminWhereContext;

    private final JiraInstanceMigratingFrom from;

    private final Set<Licenses> licenses = new HashSet<>();

    private final Set<String> groupsForJiraUsers = new HashSet<>();
    private final Set<String> groupsForJiraSysadmin = new HashSet<>();
    private final Set<String> groupsForJiraAdmin = new HashSet<>();
    private final List<GenericValue> existingAppRolesWithGroups = new ArrayList<>();

    public MockRenaissanceUpgradeInstance(EntityEngine entityEngine, JiraInstanceMigratingFrom from,
                                          OfBizDelegator ofBizDelegator) {
        this.entityEngine = entityEngine;
        this.from = from;
        this.ofBizDelegator = ofBizDelegator;

        this.licenseSelectFromContext = mock(EntityEngine.SelectFromContext.class);
        this.licenseWhereContext = mock(EntityEngine.WhereContext.class);

        this.gpFromContext = mock(EntityEngine.SelectFromContext.class);
        this.gpSelectFromContext = mock(EntityEngine.SelectFromContext.class);
        this.gpUseWhereContext = mock(EntityEngine.WhereEqualContext.class);
        this.gpAdminWhereContext = mock(EntityEngine.WhereEqualContext.class);
        this.gpSysAdminWhereContext = mock(EntityEngine.WhereEqualContext.class);
    }

    public MockRenaissanceUpgradeInstance withLicenses(final Licenses... licenses) {
        this.licenses.addAll(newHashSet(licenses));
        return this;
    }

    public MockRenaissanceUpgradeInstance groupsForJiraUsers(final String... groups) {
        this.groupsForJiraUsers.addAll(newHashSet(groups));
        return this;
    }

    public MockRenaissanceUpgradeInstance groupsForJiraAdmin(final String... groups) {
        this.groupsForJiraAdmin.addAll(newHashSet(groups));
        return this;
    }

    public MockRenaissanceUpgradeInstance groupsForJiraSysAdmin(final String... groups) {
        this.groupsForJiraSysadmin.addAll(newHashSet(groups));
        return this;
    }

    public MockRenaissanceUpgradeInstance hasAppRolesWithGroups(Licenses licenses, Pair<String, Boolean>... groups) {
        hasAppRolesWithGroups(licenses, Lists.newArrayList(groups));
        return this;
    }

    public MockRenaissanceUpgradeInstance hasAppRolesWithGroups(Licenses licenses, List<Pair<String, Boolean>> groups) {
        final String appKeyVal = licenses.getAppKey();

        for (Pair<String, Boolean> groupEntry : groups) {
            final String groupName = groupEntry.first();

            final Map<String, String> condition1 = MapBuilder.build(COLUMN_APP_KEY, appKeyVal, COLUMN_GROUP_ID, groupName);
            final Map<String, String> condition2 = MapBuilder.build(COLUMN_APP_KEY, appKeyVal);

            final List<GenericValue> values = new ArrayList<>();
            for (Pair<String, Boolean> entry : groups) {
                if (entry.first().endsWith(groupName)) {
                    Map<String, Object> mockMap = newHashMapWithExpectedSize(3);
                    mockMap.put(COLUMN_APP_KEY, appKeyVal);
                    mockMap.put(COLUMN_GROUP_ID, groupName);

                    if (entry.second() != null) {
                        mockMap.put(COLUMN_PRIMARY_INDICATOR, entry.second());
                    }

                    final MockGenericValue mockGenericValue = new MockGenericValue(ENTITY_NAME,
                            ImmutableMap.copyOf(mockMap));
                    existingAppRolesWithGroups.add(mockGenericValue);
                    values.add(mockGenericValue);
                }
            }
            when(ofBizDelegator.findByAnd(ENTITY_NAME, condition1)).thenReturn(values);
            when(ofBizDelegator.findByAnd(ENTITY_NAME, condition2)).thenReturn(values);
        }
        return this;
    }

    public MockRenaissanceUpgradeInstance hasAppRolesWithGroups(Map<Licenses, List<Pair<String, Boolean>>> appWithGroups) {
        for (Map.Entry<Licenses, List<Pair<String, Boolean>>> appGroupsEntry : appWithGroups.entrySet()) {
            hasAppRolesWithGroups(appGroupsEntry.getKey(), appGroupsEntry.getValue());
        }
        when(ofBizDelegator.findAll(ENTITY_NAME)).thenReturn(existingAppRolesWithGroups);
        return this;
    }

    public MockRenaissanceUpgradeInstance to(final JiraApplication to) {
        Application app = mock(Application.class);
        when(app.getKey()).thenReturn(to.getKey());
        setupLicenses();
        setupGlobalPermissionsUse();
        return this;
    }

    private void setupLicenses() {
        final List<ProductLicense> productLicenses = Lists.newArrayList();
        for (Licenses lic : licenses) {
            productLicenses.add(new ProductLicense(lic.getLicenseString()));
        }
        when(entityEngine.selectFrom(PRODUCT_LICENSE)).thenReturn(licenseSelectFromContext);
        when(licenseSelectFromContext.findAll()).thenReturn(licenseWhereContext);
        when(licenseWhereContext.list()).thenReturn(productLicenses);
    }

    private void setupGlobalPermissionsUse() {
        final List<GlobalPermissionEntry> gps = Lists.newArrayList();
        for (String group : groupsForJiraUsers) {
            gps.add(new GlobalPermissionEntry(globalPermissionUsers, group));
        }
        when(gpSelectFromContext.whereEqual("permission", globalPermissionUsers)).thenReturn(gpUseWhereContext);
        when(gpUseWhereContext.list()).thenReturn(gps);
        when(gpFromContext.whereEqual("permission", globalPermissionUsers)).thenReturn(gpUseWhereContext);

        final List<GlobalPermissionEntry> gpsAdmin = Lists.newArrayList();
        for (String group : groupsForJiraAdmin) {
            gpsAdmin.add(new GlobalPermissionEntry(globalPermissionAdmin, group));
        }
        when(gpSelectFromContext.whereEqual("permission", globalPermissionAdmin)).thenReturn(gpAdminWhereContext);
        when(gpAdminWhereContext.list()).thenReturn(gpsAdmin);
        when(gpFromContext.whereEqual("permission", globalPermissionAdmin)).thenReturn(gpAdminWhereContext);

        final List<GlobalPermissionEntry> gpsSysAdmin = Lists.newArrayList();
        for (String group : groupsForJiraSysadmin) {
            gpsSysAdmin.add(new GlobalPermissionEntry(globalPermissionSysAdmin, group));
        }
        when(gpSelectFromContext.whereEqual("permission", globalPermissionSysAdmin)).thenReturn(gpSysAdminWhereContext);
        when(gpSysAdminWhereContext.list()).thenReturn(gpsSysAdmin);
        when(gpFromContext.whereEqual("permission", globalPermissionSysAdmin)).thenReturn(gpSysAdminWhereContext);

        when(entityEngine.selectFrom(GLOBAL_PERMISSION_ENTRY)).thenReturn(gpFromContext);
    }

    public enum JiraInstanceMigratingFrom {
        PRE_RENAISSANCE("jira-old"),
        SERVICE_DESK_PLUGIN("jira-servicedesk-plugin"),
        JIRA_AGILE_PLUGIN("jira-agile");

        private final ApplicationKey applicationKey;

        JiraInstanceMigratingFrom(final String applicationKey) {
            this.applicationKey = ApplicationKey.valueOf(applicationKey);
        }

        public ApplicationKey getKey() {
            return applicationKey;
        }
    }

    public enum JiraApplication {
        SERVICE_DESK("jira-servicedesk"),
        JIRA_SOFTWARE("jira-software"),
        CORE("jira-core"),
        REFERENCE("jira-reference"),
        OTHER("jira-other");

        private final ApplicationKey applicationKey;

        JiraApplication(final String applicationKey) {
            this.applicationKey = ApplicationKey.valueOf(applicationKey);
        }

        public ApplicationKey getKey() {
            return applicationKey;
        }
    }

    public enum Licenses {
        PRE_RENAISSANCE("jira-software", OtherLicenses.LICENSE_PRE_JIRA_APP_V2_COMMERCIAL.getLicenseString()),
        REFERENCE("jira-reference", OtherLicenses.LICENSE_REFERENCE.getLicenseString()),
        SERVICE_DESK("jira-servicedesk", ServiceDeskLicenses.LICENSE_SERVICE_DESK_ABP.getLicenseString()),
        SOFTWARE("jira-software", SoftwareLicenses.LICENSE_SOFTWARE.getLicenseString()),
        CORE("jira-core", CoreLicenses.LICENSE_CORE.getLicenseString()),
        ELA("jira-ela", ElaLicenses.LICENSE_ELA_CORE_SW_SD_TEST_REF_OTHER.getLicenseString()),
        OTHER("jira-other", OtherLicenses.LICENSE_OTHER.getLicenseString()),
        INVALID("jira-invalid", "@#$%^&*");

        private final ApplicationKey applicationKey;
        private final String licenseString;

        Licenses(final String applicationKey, final String licenseString) {
            this.applicationKey = ApplicationKey.valueOf(applicationKey);
            this.licenseString = licenseString;
        }

        public String getAppKey() {
            return applicationKey.value();
        }

        public ApplicationKey getKey() {
            return applicationKey;
        }

        public String getLicenseString() {
            return licenseString;
        }
    }
}
