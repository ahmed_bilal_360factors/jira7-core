package com.atlassian.jira.upgrade.tasks;

import com.atlassian.jira.entity.EntityEngine;
import com.atlassian.jira.entity.EntityEngineImpl;
import com.atlassian.jira.mock.ofbiz.MockGenericValue;
import com.atlassian.jira.mock.ofbiz.MockOfBizDelegator;
import com.atlassian.jira.ofbiz.FieldMap;
import com.atlassian.jira.scheduler.JiraParameterMapSerializer;
import org.junit.After;
import org.junit.Test;
import org.mockito.Mock;

import static com.atlassian.jira.mock.Strict.reject;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

/**
 * Unit test to verify only that the upgrade task is skipped when upgrade task 6307 or 6317 has run.
 * <p>
 * There is a functional test to take care of the actual upgrade logic in upgradeFilterSubscriptionSchedules,
 * so it is short-circuited, here.
 * </p>
 *
 * @since v6.3 (as TestUpgradeTask_Build6317)
 */
public class TestUpgradeTask_Build70011 {
    private MockOfBizDelegator delegator = new MockOfBizDelegator();
    private EntityEngine entityEngine = new EntityEngineImpl(delegator);

    @Mock
    private JiraParameterMapSerializer jiraParameterMapSerializer;

    @After
    public void tearDown() {
        delegator = null;
    }


    @Test
    public void testSkipsIf6307IsPresent() throws Exception {
        delegator.createValue(new MockGenericValue("UpgradeHistory", FieldMap.build(
                "upgradeclass", "com.atlassian.jira.upgrade.tasks.UpgradeTask_Build6307"
        )));

        final UpgradeTask_Build70011 fixture = fixture();
        reject(fixture).upgradeFilterSubscriptionSchedules();

        fixture.doUpgrade(false);
    }

    @Test
    public void testSkipsIf6317IsPresent() throws Exception {
        delegator.createValue(new MockGenericValue("UpgradeHistory", FieldMap.build(
                "upgradeclass", "com.atlassian.jira.upgrade.tasks.UpgradeTask_Build6317"
        )));

        final UpgradeTask_Build70011 fixture = fixture();
        reject(fixture).upgradeFilterSubscriptionSchedules();

        fixture.doUpgrade(false);
    }

    @Test
    public void testRunsIf6307IsMissing() throws Exception {
        delegator.createValue(new MockGenericValue("UpgradeHistory", FieldMap.build(
                "upgradeclass", "com.atlassian.jira.upgrade.tasks.UpgradeTask_Build6303"
        )));

        final UpgradeTask_Build70011 fixture = fixture();
        doNothing().when(fixture).upgradeFilterSubscriptionSchedules();

        fixture.doUpgrade(false);
        verify(fixture).upgradeFilterSubscriptionSchedules();
    }

    UpgradeTask_Build70011 fixture() {
        return spy(new UpgradeTask_Build70011(entityEngine, null, jiraParameterMapSerializer));
    }
}
