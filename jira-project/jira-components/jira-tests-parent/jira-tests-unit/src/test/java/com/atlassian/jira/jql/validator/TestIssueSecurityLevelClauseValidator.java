package com.atlassian.jira.jql.validator;

import com.atlassian.jira.issue.security.IssueSecurityLevel;
import com.atlassian.jira.issue.security.IssueSecurityLevelImpl;
import com.atlassian.jira.jql.operand.JqlOperandResolver;
import com.atlassian.jira.jql.operand.QueryLiteral;
import com.atlassian.jira.jql.resolver.IssueSecurityLevelResolver;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.MessageSet;
import com.atlassian.jira.util.MessageSetImpl;
import com.atlassian.jira.util.collect.CollectionBuilder;
import com.atlassian.jira.web.bean.MockI18nBean;
import com.atlassian.query.clause.TerminalClause;
import com.atlassian.query.clause.TerminalClauseImpl;
import com.atlassian.query.operand.EmptyOperand;
import com.atlassian.query.operand.MultiValueOperand;
import com.atlassian.query.operand.Operand;
import com.atlassian.query.operand.SingleValueOperand;
import com.atlassian.query.operator.Operator;
import com.google.common.collect.ImmutableList;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Collections;

import static com.atlassian.jira.jql.operand.SimpleLiteralFactory.createLiteral;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestIssueSecurityLevelClauseValidator {
    @Rule
    public MockitoRule mockito = MockitoJUnit.rule();
    @Mock
    private JqlOperandResolver jqlOperandResolver;
    @Mock
    private IssueSecurityLevelResolver levelResolver;

    private IssueSecurityLevelClauseValidator levelClauseValidator;

    private ApplicationUser theUser;

    @Before
    public void setUp() throws Exception {
        theUser = new MockApplicationUser("fred");

        levelClauseValidator = new IssueSecurityLevelClauseValidator(levelResolver, jqlOperandResolver) {
            @Override
            I18nHelper getI18n(final ApplicationUser user) {
                return new MockI18nBean();
            }
        };
    }

    @Test
    public void testValidateHappyPathEmptyLiteral() throws Exception {
        final Operand operand = EmptyOperand.EMPTY;

        final QueryLiteral emptyLiteral = new QueryLiteral();

        final TerminalClause clause = new TerminalClauseImpl("test", Operator.IS, operand);

        when(jqlOperandResolver.getValues(theUser, operand, clause)).thenReturn(ImmutableList.of(emptyLiteral));

        final MessageSet messageSet = levelClauseValidator.validate(theUser, clause);
        assertFalse(messageSet.hasAnyMessages());
    }

    @Test
    public void testValidateHappyPathMultipleLevels() throws Exception {
        final SingleValueOperand level1Operand = new SingleValueOperand("level1");
        final SingleValueOperand level2Operand = new SingleValueOperand("level2");
        final SingleValueOperand level3Operand = new SingleValueOperand(123L);

        final QueryLiteral queryLiteral1 = createLiteral("level1");
        final QueryLiteral queryLiteral2 = createLiteral("level2");
        final QueryLiteral queryLiteral3 = createLiteral(123L);

        final MultiValueOperand operand = new MultiValueOperand(CollectionBuilder.newBuilder(level1Operand, level2Operand, level3Operand).asList());
        final TerminalClause clause = new TerminalClauseImpl("test", Operator.IN, operand);

        when(jqlOperandResolver.getValues(theUser, operand, clause)).thenReturn(ImmutableList.of(queryLiteral1, queryLiteral2, queryLiteral3));

        when(levelResolver.getIssueSecurityLevels(theUser, queryLiteral1)).thenReturn(Collections.singletonList(createMockSecurityLevel(1L, "level1")));
        when(levelResolver.getIssueSecurityLevels(theUser, queryLiteral2)).thenReturn(Collections.singletonList(createMockSecurityLevel(2L, "level2")));
        when(levelResolver.getIssueSecurityLevels(theUser, queryLiteral3)).thenReturn(Collections.singletonList(createMockSecurityLevel(3L, "level3")));


        final MessageSet messageSet = levelClauseValidator.validate(theUser, clause);
        assertFalse(messageSet.hasAnyMessages());
    }

    @Test
    public void testErrorFindingLevelByName() throws Exception {
        final SingleValueOperand level1Operand = new SingleValueOperand("level1");
        final SingleValueOperand level2Operand = new SingleValueOperand("level2");
        final SingleValueOperand level3Operand = new SingleValueOperand(123L);

        final QueryLiteral queryLiteral1 = new QueryLiteral(level1Operand, "level1");
        final QueryLiteral queryLiteral2 = new QueryLiteral(level2Operand, "level2");
        final QueryLiteral queryLiteral3 = new QueryLiteral(level3Operand, 123L);

        final MultiValueOperand operand = new MultiValueOperand(CollectionBuilder.newBuilder(level1Operand, level2Operand, level3Operand).asList());
        final TerminalClause clause = new TerminalClauseImpl("test", Operator.IN, operand);

        when(jqlOperandResolver.getValues(theUser, operand, clause)).thenReturn(CollectionBuilder.newBuilder(queryLiteral1, queryLiteral2, queryLiteral3).asList());
        when(jqlOperandResolver.isFunctionOperand(level2Operand)).thenReturn(false);
        when(levelResolver.getIssueSecurityLevels(theUser, queryLiteral1)).thenReturn(Collections.singletonList(createMockSecurityLevel(1L, "level1")));
        when(levelResolver.getIssueSecurityLevels(theUser, queryLiteral2)).thenReturn(Collections.emptyList());
        when(levelResolver.getIssueSecurityLevels(theUser, queryLiteral3)).thenReturn(Collections.singletonList(createMockSecurityLevel(3L, "level3")));

        final MessageSet messageSet = levelClauseValidator.validate(theUser, clause);
        assertTrue(messageSet.hasAnyMessages());
        assertEquals("The value 'level2' does not exist for the field 'test'.", messageSet.getErrorMessages().iterator().next());
    }

    @Test
    public void testErrorFindingLevelByNameFromFunction() throws Exception {
        final SingleValueOperand level1Operand = new SingleValueOperand("level1");
        final SingleValueOperand level2Operand = new SingleValueOperand("level2");
        final SingleValueOperand level3Operand = new SingleValueOperand(123L);

        final QueryLiteral queryLiteral1 = new QueryLiteral(level1Operand, "level1");
        final QueryLiteral queryLiteral2 = new QueryLiteral(level2Operand, "level2");
        final QueryLiteral queryLiteral3 = new QueryLiteral(level3Operand, 123L);

        final MultiValueOperand operand = new MultiValueOperand(CollectionBuilder.newBuilder(level1Operand, level2Operand, level3Operand).asList());
        final TerminalClause clause = new TerminalClauseImpl("test", Operator.IN, operand);

        when(jqlOperandResolver.getValues(theUser, operand, clause)).thenReturn(CollectionBuilder.newBuilder(queryLiteral1, queryLiteral2, queryLiteral3).asList());
        when(jqlOperandResolver.isFunctionOperand(level2Operand)).thenReturn(true);
        when(levelResolver.getIssueSecurityLevels(theUser, queryLiteral1)).thenReturn(Collections.singletonList(createMockSecurityLevel(1L, "level1")));
        when(levelResolver.getIssueSecurityLevels(theUser, queryLiteral2)).thenReturn(Collections.emptyList());
        when(levelResolver.getIssueSecurityLevels(theUser, queryLiteral3)).thenReturn(Collections.singletonList(createMockSecurityLevel(3L, "level3")));

        final MessageSet messageSet = levelClauseValidator.validate(theUser, clause);
        assertTrue(messageSet.hasAnyMessages());
        assertEquals("A value provided by the function 'SingleValueOperand' is invalid for the field 'test'.", messageSet.getErrorMessages().iterator().next());
    }

    @Test
    public void testErrorFindingLevelById() throws Exception {
        final SingleValueOperand level1Operand = new SingleValueOperand("level1");
        final SingleValueOperand level2Operand = new SingleValueOperand("level2");
        final SingleValueOperand level3Operand = new SingleValueOperand(123L);

        final QueryLiteral queryLiteral1 = new QueryLiteral(level1Operand, "level1");
        final QueryLiteral queryLiteral2 = new QueryLiteral(level2Operand, "level2");
        final QueryLiteral queryLiteral3 = new QueryLiteral(level3Operand, 123L);

        final MultiValueOperand operand = new MultiValueOperand(CollectionBuilder.newBuilder(level1Operand, level2Operand, level3Operand).asList());
        final TerminalClause clause = new TerminalClauseImpl("test", Operator.IN, operand);

        when(jqlOperandResolver.getValues(theUser, operand, clause)).thenReturn(CollectionBuilder.newBuilder(queryLiteral1, queryLiteral2, queryLiteral3).asList());
        when(jqlOperandResolver.isFunctionOperand(level3Operand)).thenReturn(false);

        when(levelResolver.getIssueSecurityLevels(theUser, queryLiteral1)).thenReturn(Collections.singletonList(createMockSecurityLevel(1L, "level1")));
        when(levelResolver.getIssueSecurityLevels(theUser, queryLiteral2)).thenReturn(Collections.singletonList(createMockSecurityLevel(2L, "level2")));
        when(levelResolver.getIssueSecurityLevels(theUser, queryLiteral3)).thenReturn(Collections.emptyList());

        final MessageSet messageSet = levelClauseValidator.validate(theUser, clause);
        assertTrue(messageSet.hasAnyMessages());
        assertEquals("A value with ID '123' does not exist for the field 'test'.", messageSet.getErrorMessages().iterator().next());

    }

    @Test
    public void testErrorFindingLevelByIdFromFunction() throws Exception {
        final SingleValueOperand level1Operand = new SingleValueOperand("level1");
        final SingleValueOperand level2Operand = new SingleValueOperand("level2");
        final SingleValueOperand level3Operand = new SingleValueOperand(123L);

        final QueryLiteral queryLiteral1 = new QueryLiteral(level1Operand, "level1");
        final QueryLiteral queryLiteral2 = new QueryLiteral(level2Operand, "level2");
        final QueryLiteral queryLiteral3 = new QueryLiteral(level3Operand, 123L);

        final MultiValueOperand operand = new MultiValueOperand(CollectionBuilder.newBuilder(level1Operand, level2Operand, level3Operand).asList());
        final TerminalClause clause = new TerminalClauseImpl("test", Operator.IN, operand);

        when(jqlOperandResolver.getValues(theUser, operand, clause)).thenReturn(CollectionBuilder.newBuilder(queryLiteral1, queryLiteral2, queryLiteral3).asList());
        when(jqlOperandResolver.isFunctionOperand(level3Operand)).thenReturn(true);

        when(levelResolver.getIssueSecurityLevels(theUser, queryLiteral1)).thenReturn(Collections.singletonList(createMockSecurityLevel(1L, "level1")));
        when(levelResolver.getIssueSecurityLevels(theUser, queryLiteral2)).thenReturn(Collections.singletonList(createMockSecurityLevel(2L, "level2")));
        when(levelResolver.getIssueSecurityLevels(theUser, queryLiteral3)).thenReturn(Collections.emptyList());

        final MessageSet messageSet = levelClauseValidator.validate(theUser, clause);
        assertTrue(messageSet.hasAnyMessages());
        assertEquals("A value provided by the function 'SingleValueOperand' is invalid for the field 'test'.", messageSet.getErrorMessages().iterator().next());
    }

    @Test
    public void testInvalidOperator() throws Exception {
        final MessageSetImpl messageSet = new MessageSetImpl();
        messageSet.addErrorMessage("dude");

        final TerminalClause clause = new TerminalClauseImpl("test", Operator.EQUALS, "a");
        final SupportedOperatorsValidator operatorsValidator = mock(SupportedOperatorsValidator.class);
        when(operatorsValidator.validate(theUser, clause)).thenReturn(messageSet);

        levelClauseValidator = new IssueSecurityLevelClauseValidator(levelResolver, jqlOperandResolver) {
            @Override
            SupportedOperatorsValidator getSupportedOperatorsValidator() {
                return operatorsValidator;
            }
        };

        final MessageSet foundSet = levelClauseValidator.validate(theUser, clause);
        assertTrue(foundSet.hasAnyMessages());
    }

    private IssueSecurityLevel createMockSecurityLevel(final Long id, final String name) {
        return new IssueSecurityLevelImpl(id, name, name, 0L);
    }
}
