package com.atlassian.jira.issue.customfields;

import com.atlassian.jira.action.issue.customfields.impl.MockOptions;
import com.atlassian.jira.action.issue.customfields.option.MockOption;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.customfields.option.Option;
import com.atlassian.jira.issue.customfields.option.Options;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.Field;
import com.atlassian.jira.mock.component.MockComponentWorker;
import com.atlassian.jira.render.Encoder;
import com.atlassian.jira.render.SwitchingEncoder;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.web.FieldVisibilityManager;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Collections;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestCustomFieldUtils {

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private ApplicationProperties applicationProperties;

    @Before
    public void setUp() throws Exception {
        new MockComponentWorker()
                .addMock(Encoder.class, new SwitchingEncoder(applicationProperties))
                .init();
    }

    @Test
    public void testParseCustomFieldId() {
        assertThat(CustomFieldUtils.getCustomFieldId("customfield_123"), is(123L));
        assertThat(CustomFieldUtils.getCustomFieldId("customfield_123:after"), is(123L));
        assertThat(CustomFieldUtils.getCustomFieldId("customfield_123:before"), is(123L));
    }

    @Test
    public void testIsShownAndVisibleOld() {
        final CustomField customFieldMock = mock(CustomField.class);
        final FieldVisibilityManager fieldVisibilityManager = mock(FieldVisibilityManager.class);
        when(fieldVisibilityManager.isFieldHiddenInAllSchemes(Matchers.eq(customFieldMock), anyObject(), anyObject())).thenAnswer(
                invocation -> Boolean.valueOf(((Field) invocation.getArguments()[0]).getId())
        );

        //false false
        when(customFieldMock.isInScope(anyObject())).thenReturn(false);
        when(customFieldMock.getId()).thenReturn("true");
        assertFalse(CustomFieldUtils.isShownAndVisible(customFieldMock, null, null, fieldVisibilityManager));

        //false, true
        when(customFieldMock.isInScope(anyObject())).thenReturn(false);
        when(customFieldMock.getId()).thenReturn("false");
        assertFalse(CustomFieldUtils.isShownAndVisible(customFieldMock, null, null, fieldVisibilityManager));

        //true false
        when(customFieldMock.isInScope(anyObject())).thenReturn(true);
        when(customFieldMock.getId()).thenReturn("true");
        assertFalse(CustomFieldUtils.isShownAndVisible(customFieldMock, null, null, fieldVisibilityManager));

        //true true
        when(customFieldMock.isInScope(anyObject())).thenReturn(true);
        when(customFieldMock.getId()).thenReturn("false");
        assertTrue(CustomFieldUtils.isShownAndVisible(customFieldMock, null, null, fieldVisibilityManager));
    }

    @Test
    public void testIsShownAndVisibleNew() {
        final CustomField customFieldMock = mock(CustomField.class);
        final FieldVisibilityManager fieldVisibilityManager = mock(FieldVisibilityManager.class);
        when(fieldVisibilityManager.isFieldHiddenInAllSchemes(Matchers.eq(customFieldMock), anyObject(), anyObject())).thenAnswer(
                invocation -> Boolean.valueOf(((Field) invocation.getArguments()[0]).getId())
        );
        final ApplicationUser user = new MockApplicationUser("dave");

        //false false
        when(customFieldMock.isInScope(anyObject())).thenReturn(false);
        when(customFieldMock.getId()).thenReturn("true");
        assertFalse(CustomFieldUtils.isShownAndVisible(customFieldMock, user, null, fieldVisibilityManager));

        //false, true
        when(customFieldMock.isInScope(anyObject())).thenReturn(false);
        when(customFieldMock.getId()).thenReturn("false");
        assertFalse(CustomFieldUtils.isShownAndVisible(customFieldMock, user, null, fieldVisibilityManager));

        //true false
        when(customFieldMock.isInScope(anyObject())).thenReturn(true);
        when(customFieldMock.getId()).thenReturn("true");
        assertFalse(CustomFieldUtils.isShownAndVisible(customFieldMock, user, null, fieldVisibilityManager));

        //true true
        when(customFieldMock.isInScope(anyObject())).thenReturn(true);
        when(customFieldMock.getId()).thenReturn("false");
        assertTrue(CustomFieldUtils.isShownAndVisible(customFieldMock, user, null, fieldVisibilityManager));
    }

    @Test
    public void shouldNotEscapeHtmlInOptionsWhenSwitchIsOn() throws Exception {
        final Option option = new MockOption(null, null, 1L, "<div>option</div>", null, 1L);
        final Options options = new MockOptions(null, Collections.singletonMap(option, null));
        enableXss(true);

        final String html = CustomFieldUtils.prettyPrintOptions(options);

        assertThat(html, is("<ul class=\"optionslist\"><li><div>option</div></li></ul>"));
    }

    @Test
    public void shouldEscapeHtmlInOptionsWhenSwitchIsOff() throws Exception {
        final Option option = new MockOption(null, null, 1L, "<div>option</div>", null, 1L);
        final Options options = new MockOptions(null, Collections.singletonMap(option, null));
        enableXss(false);

        final String html = CustomFieldUtils.prettyPrintOptions(options);

        assertThat(html, is("<ul class=\"optionslist\"><li>&lt;div&gt;option&lt;/div&gt;</li></ul>"));
    }

    private void enableXss(final boolean enableXss) {
        when(applicationProperties.getOption(APKeys.JIRA_OPTION_HTML_IN_CUSTOM_FIELDS_ENABLED)).thenReturn(enableXss);
    }
}
