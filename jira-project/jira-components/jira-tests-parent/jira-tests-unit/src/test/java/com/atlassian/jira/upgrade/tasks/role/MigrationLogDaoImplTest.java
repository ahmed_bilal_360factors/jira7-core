package com.atlassian.jira.upgrade.tasks.role;

import com.atlassian.jira.auditing.AssociatedItem;
import com.atlassian.jira.auditing.AuditRecordImpl;
import com.atlassian.jira.auditing.ChangedValueImpl;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.google.common.collect.ImmutableList;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.ofbiz.core.entity.GenericValue;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class MigrationLogDaoImplTest {
    @Mock
    private OfBizDelegator ofBizDelegator;
    @Mock
    private GenericValue recordValue;
    @Mock
    private MigrationLog migrationLog;
    @Mock
    private AuditEntry auditEntry;

    @Test
    public void writeShouldTolerateNullValues() {
        List<AuditEntry> auditEntries = new ArrayList<>();
        auditEntries.add(null);
        when(migrationLog.events()).thenReturn(auditEntries);

        MigrationLogDaoImpl migrationLogDao = new MigrationLogDaoImpl(ofBizDelegator);

        migrationLogDao.write(migrationLog);
        // we're just expecting to not get an exception here.
    }

    @Test
    public void writeShouldTolerateErrorsInAuditMessages() {
        List<AuditEntry> auditEntries = new ArrayList<>();
        auditEntries.add(auditEntry);

        when(auditEntry.getSummary()).thenThrow(new RuntimeException());
        when(migrationLog.events()).thenReturn(auditEntries);

        MigrationLogDaoImpl migrationLogDao = new MigrationLogDaoImpl(ofBizDelegator);

        migrationLogDao.write(migrationLog);
        // we're just expecting to not get an exception here.
    }

    @Test
    public void writeShouldSaveAllTheValues() {
        //given
        ChangedValueImpl changeValue = new ChangedValueImpl("some name", "some fromval", "some toval");
        when(auditEntry.getChangedValues()).thenReturn(ImmutableList.of(changeValue));

        List<AuditEntry> auditEntries = new ArrayList<>();
        AuditEntry auditEntry = new AuditEntry(MigrationLogDaoImplTest.class,
                "a summary of license moving",
                "description example",
                AssociatedItem.Type.GROUP,
                "example changed object",
                false,
                changeValue);

        auditEntries.add(auditEntry);
        when(migrationLog.events()).thenReturn(auditEntries);

        when(ofBizDelegator.createValue(eq("AuditLog"), any())).thenReturn(recordValue);
        when(recordValue.getLong("id")).thenReturn(183L);

        MigrationLogDaoImpl migrationLogDao = new MigrationLogDaoImpl(ofBizDelegator);

        //when
        migrationLogDao.write(migrationLog);

        //then
        ArgumentCaptor<Map> valuesCaptor = ArgumentCaptor.forClass(Map.class);
        verify(ofBizDelegator).createValue(eq("AuditLog"), valuesCaptor.capture());
        Map<String, Object> actualValues = valuesCaptor.getValue();
        assertNotNull(null, actualValues.get(AuditRecordImpl.CREATED));
        assertEquals(null, actualValues.get(AuditRecordImpl.AUTHOR_KEY));
        assertEquals("a summary of license moving", actualValues.get(AuditRecordImpl.SUMMARY));
        assertEquals("migration", actualValues.get(AuditRecordImpl.CATEGORY));
        assertEquals(1L, actualValues.get(AuditRecordImpl.AUTHOR_TYPE));
        assertEquals(MigrationLogDaoImplTest.class.getSimpleName(), actualValues.get(AuditRecordImpl.EVENT_SOURCE));
        assertEquals("description example", actualValues.get(AuditRecordImpl.LONG_DESCRIPTION));

        assertEquals("0", actualValues.get(AuditRecordImpl.OBJECT_ID));
        assertEquals("example changed object", actualValues.get(AuditRecordImpl.OBJECT_NAME));
        assertEquals("0", actualValues.get(AuditRecordImpl.OBJECT_PARENT_ID));
        assertEquals("", actualValues.get(AuditRecordImpl.OBJECT_PARENT_NAME));
        assertEquals("GROUP", actualValues.get(AuditRecordImpl.OBJECT_TYPE));

        assertEquals("a summary of license moving jira.auditing.category.migration migrationlogdaoimpltest some name fromval toval",
                actualValues.get(AuditRecordImpl.SEARCH_FIELD));

        ArgumentCaptor<Map> changedValuesCaptor = ArgumentCaptor.forClass(Map.class);
        verify(ofBizDelegator).createValue(eq("AuditChangedValue"), changedValuesCaptor.capture());
        Map<String, Object> actualChangedValues = changedValuesCaptor.getValue();
        assertEquals(183L, actualChangedValues.get("logId"));
        assertEquals("some name", actualChangedValues.get("name"));
        assertEquals("some fromval", actualChangedValues.get("deltaFrom"));
        assertEquals("some toval", actualChangedValues.get("deltaTo"));
    }

    @Test
    public void nonBTFOnlyIsVisibleForAdmins() {
        //given
        ChangedValueImpl changeValue = new ChangedValueImpl("some name", "some fromval", "some toval");
        when(auditEntry.getChangedValues()).thenReturn(ImmutableList.of(changeValue));

        List<AuditEntry> auditEntries = new ArrayList<>();
        AuditEntry auditEntry = new AuditEntry(MigrationLogDaoImplTest.class,
                "a summary of",
                "description example",
                AssociatedItem.Type.GROUP,
                "example changed object",
                true,
                changeValue);

        auditEntries.add(auditEntry);
        when(migrationLog.events()).thenReturn(auditEntries);

        when(ofBizDelegator.createValue(eq("AuditLog"), any())).thenReturn(recordValue);
        when(recordValue.getLong("id")).thenReturn(183L);

        MigrationLogDaoImpl migrationLogDao = new MigrationLogDaoImpl(ofBizDelegator);

        //when
        migrationLogDao.write(migrationLog);

        //then
        ArgumentCaptor<Map> valuesCaptor = ArgumentCaptor.forClass(Map.class);
        verify(ofBizDelegator).createValue(eq("AuditLog"), valuesCaptor.capture());
        Map<String, Object> actualValues = valuesCaptor.getValue();
        assertNotNull(null, actualValues.get(AuditRecordImpl.CREATED));
        assertEquals(0L, actualValues.get(AuditRecordImpl.AUTHOR_TYPE));
    }
}
