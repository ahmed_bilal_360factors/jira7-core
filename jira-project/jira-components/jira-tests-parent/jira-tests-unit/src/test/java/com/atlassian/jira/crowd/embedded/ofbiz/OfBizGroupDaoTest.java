package com.atlassian.jira.crowd.embedded.ofbiz;

import com.atlassian.beehive.ClusterLockService;
import com.atlassian.beehive.simple.SimpleClusterLockService;
import com.atlassian.cache.CacheManager;
import com.atlassian.cache.memory.MemoryCacheManager;
import com.atlassian.crowd.embedded.spi.DirectoryDao;
import com.atlassian.crowd.exception.GroupNotFoundException;
import com.atlassian.crowd.model.group.Group;
import com.atlassian.crowd.model.group.GroupType;
import com.atlassian.crowd.model.group.GroupWithAttributes;
import com.atlassian.crowd.search.query.entity.GroupQuery;
import com.atlassian.crowd.search.query.entity.restriction.MatchMode;
import com.atlassian.crowd.search.query.entity.restriction.Property;
import com.atlassian.crowd.search.query.entity.restriction.PropertyRestriction;
import com.atlassian.crowd.util.BatchResult;
import com.atlassian.gzipfilter.org.apache.commons.lang.StringUtils;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.crowd.embedded.TestData;
import com.atlassian.jira.database.MockQueryDslAccessor;
import com.google.common.collect.ImmutableSet;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.ofbiz.core.entity.GenericValue;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.atlassian.jira.crowd.embedded.TestData.DIRECTORY_ID;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.collection.IsIterableContainingInOrder.contains;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class OfBizGroupDaoTest extends AbstractTransactionalOfBizTestCase {
    private OfBizGroupDao groupDao;

    @Mock
    private ApplicationProperties applicationProperties;

    private static final String LONG_DESCRIPTION = StringUtils.leftPad("description", GroupEntity.MAX_DESCRIPTION_LENGTH + 1, "\u00cc");

    @Before
    public void setUp() throws Exception {
        when(applicationProperties.getOption(APKeys.CACHE_ALL_USERS_AND_GROUPS)).thenReturn(Boolean.TRUE);

        final CacheManager cacheManager = new MemoryCacheManager();
        final DirectoryDao directoryDao = new OfBizDirectoryDao(getOfBizDelegator(), cacheManager);
        final OfBizInternalMembershipDao internalmembershipDao = new OfBizInternalMembershipDao(getOfBizDelegator(),new MockQueryDslAccessor(), cacheManager);
        ClusterLockService clusterLockService = new SimpleClusterLockService();
        groupDao = new OfBizGroupDao(getOfBizDelegator(), directoryDao, internalmembershipDao, cacheManager, clusterLockService, applicationProperties);
    }

    @After
    public void tearDown() throws Exception {
        groupDao = null;
    }

    @Test
    public void addAndFindGroupByName() throws Exception {
        final Group createdGroup = groupDao.add(TestData.Group.getTestData());

        TestData.Group.assertEqualsTestGroup(createdGroup);

        final Group retrievedGroup = groupDao.findByName(TestData.DIRECTORY_ID, TestData.Group.NAME);

        TestData.Group.assertEqualsTestGroup(retrievedGroup);
    }

    @Test
    public void addAndStoreAttributesAndFindGroupWithAttributesByName() throws Exception {
        final Group createdGroup = groupDao.add(TestData.Group.getTestData());
        TestData.Group.assertEqualsTestGroup(createdGroup);

        groupDao.storeAttributes(createdGroup, TestData.Attributes.getTestData());

        final GroupWithAttributes retrievedGroup = groupDao.findByNameWithAttributes(TestData.DIRECTORY_ID, TestData.Group.NAME);
        TestData.Group.assertEqualsTestGroup(retrievedGroup);
        TestData.Attributes.assertEqualsTestData(retrievedGroup);
    }

    @Test
    public void updateGroup() throws Exception {
        final Group createdGroup = groupDao.add(TestData.Group.getTestData());
        TestData.Group.assertEqualsTestGroup(createdGroup);

        final boolean updatedIsActive = false;
        final String updatedDescription = "updated Description";

        groupDao.update(TestData.Group.getGroup(createdGroup.getName(), createdGroup.getDirectoryId(), updatedIsActive, updatedDescription, createdGroup.getType()));

        final Group updatedGroup = groupDao.findByName(TestData.DIRECTORY_ID, TestData.Group.NAME);

        assertEquals(TestData.Group.NAME, updatedGroup.getName());
        assertEquals(TestData.DIRECTORY_ID, updatedGroup.getDirectoryId());
        assertEquals(TestData.Group.TYPE, updatedGroup.getType());
        assertEquals(updatedIsActive, updatedGroup.isActive());
        assertEquals(updatedDescription, updatedGroup.getDescription());
    }

    @Test
    public void removeGroup() throws Exception {
        groupDao.add(TestData.Group.getTestData());
        assertNotNull(groupDao.findByName(TestData.DIRECTORY_ID, TestData.Group.NAME));

        groupDao.remove(TestData.Group.getTestData());
        try {
            groupDao.findByName(TestData.DIRECTORY_ID, TestData.Group.NAME);
            fail("Should have thrown a user not found exception");
        } catch (GroupNotFoundException e) {
            assertEquals(TestData.Group.NAME, e.getGroupName());
        }
    }

    @Test
    public void removeAttribute() throws Exception {
        final Group createdGroup = groupDao.add(TestData.Group.getTestData());
        groupDao.storeAttributes(createdGroup, TestData.Attributes.getTestData());

        TestData.Attributes.assertEqualsTestData(groupDao.findByNameWithAttributes(TestData.DIRECTORY_ID, TestData.Group.NAME));

        groupDao.removeAttribute(createdGroup, TestData.Attributes.ATTRIBUTE1);
        final GroupWithAttributes groupWithLessAttributes = groupDao.findByNameWithAttributes(TestData.DIRECTORY_ID, TestData.Group.NAME);

        assertNull(groupWithLessAttributes.getValue(TestData.Attributes.ATTRIBUTE1));
    }

    @Test
    public void searchAllGroupNames() {
        final String groupName2 = "group2";
        groupDao.add(TestData.Group.getTestData());
        groupDao.add(TestData.Group.getGroup(groupName2, DIRECTORY_ID, true, "d", GroupType.GROUP));

        @SuppressWarnings("unchecked")
        final GroupQuery<String> query = mock(GroupQuery.class);
        when(query.getReturnType()).thenReturn(String.class);

        final List<String> groupNames = groupDao.search(DIRECTORY_ID, query);
        assertThat(groupNames, containsInAnyOrder(TestData.Group.NAME, groupName2));
    }

    @Test
    public void searchGroupNamesWithRestriction() {
        final String groupName2 = "group2";
        groupDao.add(TestData.Group.getTestData());
        groupDao.add(TestData.Group.getGroup(groupName2, DIRECTORY_ID, true, "d", GroupType.GROUP));

        @SuppressWarnings("unchecked")
        final GroupQuery<String> query = mock(GroupQuery.class);
        when(query.getReturnType()).thenReturn(String.class);
        mockSearchRestriction(query);

        final List<String> groupNames = groupDao.search(DIRECTORY_ID, query);
        assertThat(groupNames, contains(TestData.Group.NAME));
    }

    @Test
    public void searchAllGroups() {
        final String groupName2 = "group2";
        final Group group1 = TestData.Group.getTestData();
        final Group group2 = TestData.Group.getGroup(groupName2, DIRECTORY_ID, true, "d", GroupType.GROUP);
        groupDao.add(group1);
        groupDao.add(group2);

        @SuppressWarnings("unchecked")
        final GroupQuery<Group> query = mock(GroupQuery.class);
        when(query.getReturnType()).thenReturn(Group.class);

        final List<Group> groups = groupDao.search(DIRECTORY_ID, query);
        assertThat(groups, containsInAnyOrder(group1, group2));
    }

    @Test
    public void searchGroupsWithLimitAndOffset() {
        final String groupName1 = "group1";
        final String groupName2 = "group2";
        final String groupName3 = "group3";
        final String groupName4 = "group4";
        final Group group1 = TestData.Group.getGroup(groupName1, DIRECTORY_ID, true, "d", GroupType.GROUP);
        final Group group2 = TestData.Group.getGroup(groupName2, DIRECTORY_ID, true, "d", GroupType.GROUP);
        final Group group3 = TestData.Group.getGroup(groupName3, DIRECTORY_ID, true, "d", GroupType.GROUP);
        final Group group4 = TestData.Group.getGroup(groupName4, DIRECTORY_ID, true, "d", GroupType.GROUP);
        groupDao.add(group1);
        groupDao.add(group2);
        groupDao.add(group3);
        groupDao.add(group4);

        @SuppressWarnings("unchecked")
        final GroupQuery<Group> query = mock(GroupQuery.class);
        when(query.getReturnType()).thenReturn(Group.class);
        when(query.getStartIndex()).thenReturn(1);
        when(query.getMaxResults()).thenReturn(2);

        final List<Group> groups = groupDao.search(DIRECTORY_ID, query);
        assertThat(groups, contains(group2, group3));
    }

    @Test
    public void searchGroupsWithRestriction() {
        final String groupName2 = "group2";
        final Group group1 = TestData.Group.getTestData();
        final Group group2 = TestData.Group.getGroup(groupName2, DIRECTORY_ID, true, "d", GroupType.GROUP);
        groupDao.add(group1);
        groupDao.add(group2);

        @SuppressWarnings("unchecked")
        final GroupQuery<Group> query = mock(GroupQuery.class);
        when(query.getReturnType()).thenReturn(Group.class);
        mockSearchRestriction(query);

        final List<Group> groups = groupDao.search(DIRECTORY_ID, query);
        assertThat(groups, contains(group1));
    }

    @Test
    public void searchAllGroupsWithAttributes() throws GroupNotFoundException {
        final Group group1 = TestData.Group.getTestData();
        final Map<String, Set<String>> group1attrs = TestData.Attributes.getTestData();

        final String groupName2 = "group2";
        final Group group2 = TestData.Group.getGroup(groupName2, DIRECTORY_ID, true, "d", GroupType.GROUP);

        groupDao.add(group1);
        groupDao.add(group2);
        groupDao.storeAttributes(group1, group1attrs);

        @SuppressWarnings("unchecked")
        final GroupQuery<GroupWithAttributes> query = mock(GroupQuery.class);
        when(query.getReturnType()).thenReturn(GroupWithAttributes.class);

        List<GroupWithAttributes> groups = groupDao.search(DIRECTORY_ID, query);

        // copy and reverse sort by name to guarantee a stable order
        groups = new ArrayList<>(groups);
        Collections.sort(groups, (first, second) -> second.getName().compareTo(first.getName()));

        assertThat(groups, hasSize(2));
        final GroupWithAttributes foundGroup1 = groups.get(0);
        TestData.Group.assertEqualsTestGroup(foundGroup1);
        TestData.Attributes.assertEqualsTestData(foundGroup1);
        final GroupWithAttributes foundGroup2 = groups.get(1);
        assertThat(foundGroup2.getName(), equalTo(groupName2));
        assertThat("Expecting no attributes on group2", foundGroup2.getKeys(), hasSize(0));
    }

    @Test
    public void searchGroupsWithAttributesWithRestriction() throws GroupNotFoundException {
        final Group group1 = TestData.Group.getTestData();
        final Map<String, Set<String>> group1attrs = TestData.Attributes.getTestData();

        final String groupName2 = "group2";
        final Group group2 = TestData.Group.getGroup(groupName2, DIRECTORY_ID, true, "d", GroupType.GROUP);

        groupDao.add(group1);
        groupDao.add(group2);
        groupDao.storeAttributes(group1, group1attrs);

        @SuppressWarnings("unchecked")
        final GroupQuery<GroupWithAttributes> query = mock(GroupQuery.class);
        when(query.getReturnType()).thenReturn(GroupWithAttributes.class);
        mockSearchRestriction(query);

        List<GroupWithAttributes> groups = groupDao.search(DIRECTORY_ID, query);

        assertThat(groups, hasSize(1));
        final GroupWithAttributes foundGroup1 = groups.get(0);
        TestData.Group.assertEqualsTestGroup(foundGroup1);
        TestData.Attributes.assertEqualsTestData(foundGroup1);
    }

    @Test
    public void removeAll() {
        final Group createdGroup = groupDao.add(TestData.Group.getTestData());

        BatchResult<String> batchResult = groupDao.removeAllGroups(DIRECTORY_ID,
                ImmutableSet.of(createdGroup.getName(), "non-existent-group"));

        assertThat(batchResult.getSuccessfulEntities(), contains(createdGroup.getName()));
        assertThat(batchResult.getFailedEntities(), contains("non-existent-group"));
    }

    // Note: Search restrictions prevent us from using the in-memory cache to implement a search.
    // We should test both code paths where possible.
    @SuppressWarnings("unchecked")
    private static void mockSearchRestriction(GroupQuery<?> query) {
        final Property<String> name = mock(Property.class);
        when(name.getPropertyName()).thenReturn(GroupEntity.NAME);
        when(name.getPropertyType()).thenReturn(String.class);

        final PropertyRestriction<String> restriction = mock(PropertyRestriction.class);
        when(restriction.getProperty()).thenReturn(name);
        when(restriction.getMatchMode()).thenReturn(MatchMode.STARTS_WITH);
        when(restriction.getValue()).thenReturn("T");

        when(query.getSearchRestriction()).thenReturn(restriction);
    }

    @Test
    public void addWithDescriptionTruncation() throws Exception {
        final Group group2 = TestData.Group.getGroup(TestData.Group.NAME, DIRECTORY_ID, true, LONG_DESCRIPTION, GroupType.GROUP);
        groupDao.add(group2);

        final GenericValue retrievedGroup = groupDao.findGroupGenericValue(TestData.DIRECTORY_ID, TestData.Group.NAME);

        assertThat(retrievedGroup.getString(GroupEntity.DESCRIPTION).length(), equalTo(GroupEntity.MAX_DESCRIPTION_LENGTH));
        assertThat(retrievedGroup.getString(GroupEntity.LOWER_DESCRIPTION).length(), equalTo(GroupEntity.MAX_DESCRIPTION_LENGTH));
    }

    @Test
    public void addWithDescriptionTruncationWithLocale() throws Exception {
        String tempLanguage = System.getProperty("crowd.identifier.language");
        System.setProperty("crowd.identifier.language", "lt");

        try {
            final Group group2 = TestData.Group.getGroup(TestData.Group.NAME, DIRECTORY_ID, true, LONG_DESCRIPTION, GroupType.GROUP);
            groupDao.add(group2);

            final GenericValue retrievedGroup = groupDao.findGroupGenericValue(TestData.DIRECTORY_ID, TestData.Group.NAME);

            assertThat((retrievedGroup.getString(GroupEntity.DESCRIPTION)).length(), equalTo(GroupEntity.MAX_DESCRIPTION_LENGTH));
            assertThat((retrievedGroup.getString(GroupEntity.LOWER_DESCRIPTION)).length(), equalTo(GroupEntity.MAX_DESCRIPTION_LENGTH));
        } finally {
            // Clean up our locale setting, just in case...
            if (tempLanguage != null) {
                System.setProperty("crowd.identifier.language", tempLanguage);
            } else {
                System.clearProperty("crowd.identifier.language");
            }
        }
    }


    @Test
    public void updateWithDescriptionTruncation() throws Exception {
        final Group createdGroup = groupDao.add(TestData.Group.getTestData());
        TestData.Group.assertEqualsTestGroup(createdGroup);

        final boolean updatedIsActive = false;
        final String updatedDescription = LONG_DESCRIPTION;


        groupDao.update(TestData.Group.getGroup(createdGroup.getName(), createdGroup.getDirectoryId(), updatedIsActive,
                updatedDescription, createdGroup.getType()));

        GenericValue val = groupDao.findGroupGenericValue(createdGroup.getDirectoryId(), createdGroup.getName());

        assertThat(val.getString(GroupEntity.DESCRIPTION).length(), equalTo(GroupEntity.MAX_DESCRIPTION_LENGTH));
        assertThat(val.getString(GroupEntity.LOWER_DESCRIPTION).length(), equalTo(GroupEntity.MAX_DESCRIPTION_LENGTH));
    }

    @Test
    public void updateWithDescriptionTruncationWithLocale() throws Exception {
        String tempLanguage = System.getProperty("crowd.identifier.language");
        System.setProperty("crowd.identifier.language", "lt");

        try {
            final Group createdGroup = groupDao.add(TestData.Group.getTestData());
            TestData.Group.assertEqualsTestGroup(createdGroup);

            final boolean updatedIsActive = false;
            final String updatedDescription = LONG_DESCRIPTION;
            groupDao.update(TestData.Group.getGroup(createdGroup.getName(), createdGroup.getDirectoryId(), updatedIsActive,
                    updatedDescription, createdGroup.getType()));

            GenericValue val = groupDao.findGroupGenericValue(createdGroup.getDirectoryId(), createdGroup.getName());

            assertThat(val.getString(GroupEntity.DESCRIPTION).length(), equalTo(GroupEntity.MAX_DESCRIPTION_LENGTH));
            assertThat(val.getString(GroupEntity.LOWER_DESCRIPTION).length(), equalTo(GroupEntity.MAX_DESCRIPTION_LENGTH));
            // Clean up our locale setting, just in case...
        } finally {
            if (tempLanguage != null) {
                System.setProperty("crowd.identifier.language", tempLanguage);
            } else {
                System.clearProperty("crowd.identifier.language");
            }
        }
    }
}

