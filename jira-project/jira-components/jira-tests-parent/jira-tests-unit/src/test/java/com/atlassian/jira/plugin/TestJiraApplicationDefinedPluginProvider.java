package com.atlassian.jira.plugin;

import com.atlassian.application.api.ApplicationPlugin;
import com.atlassian.jira.application.JiraApplicationMetaDataModuleDescriptor;
import com.atlassian.jira.application.JiraPluginApplicationMetaData;
import com.atlassian.plugin.ModuleDescriptor;
import org.junit.Test;

import java.util.List;
import java.util.Set;

import static java.util.Arrays.asList;
import static java.util.Arrays.stream;
import static java.util.stream.Collectors.toList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasItems;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @since 7.3
 */
@SuppressWarnings("unchecked")
public class TestJiraApplicationDefinedPluginProvider {

    @Test
    public void applicationRelatedPluginProviderShouldReturnTheListOfRelatedPluginKeys() throws Exception {
        final Set<String> pluginNames = new JiraApplicationDefinedPluginProvider().getPluginKeys(asList(
                jiraModuleDesc("plugin1", "plugin2"),
                moduleDesc(), moduleDesc()
        ));
        assertThat(pluginNames, hasItems("plugin1", "plugin2"));
    }

    private ModuleDescriptor moduleDesc() {
        return mock(JiraResourcedModuleDescriptor.class);
    }

    private ModuleDescriptor jiraModuleDesc(final String... pluginKeys) {
        final JiraApplicationMetaDataModuleDescriptor desc = mock(JiraApplicationMetaDataModuleDescriptor.class);
        final JiraPluginApplicationMetaData module = mock(JiraPluginApplicationMetaData.class);
        when(desc.getModule()).thenReturn(module);
        final List<ApplicationPlugin> plugins = stream(pluginKeys).map(key -> {
            final ApplicationPlugin plugin = mock(ApplicationPlugin.class);
            when(plugin.getPluginKey()).thenReturn(key);
            return plugin;
        }).collect(toList());
        when(module.getPlugins()).thenReturn(plugins);
        return desc;
    }
}
