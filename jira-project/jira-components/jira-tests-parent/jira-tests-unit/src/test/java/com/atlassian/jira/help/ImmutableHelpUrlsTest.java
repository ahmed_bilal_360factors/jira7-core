package com.atlassian.jira.help;

import com.google.common.collect.Maps;
import org.junit.Test;

import java.util.Arrays;
import java.util.Map;
import java.util.Set;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.fail;

/**
 * @since v6.2.4
 */
public class ImmutableHelpUrlsTest {
    @Test
    public void getUrlReturnsMappedUrls() {
        HelpUrl defaultUrl = MockHelpUrl.simpleUrl("default");
        HelpUrl one = MockHelpUrl.simpleUrl("one");
        HelpUrl two = MockHelpUrl.simpleUrl("two");

        HelpUrls urls = urls(defaultUrl, one, two);
        assertThat(urls.getUrl(defaultUrl.getKey()), equalTo(defaultUrl));
        assertThat(urls.getUrl(one.getKey()), equalTo(one));
        assertThat(urls.getUrl(two.getKey()), equalTo(two));
    }

    @Test
    public void getUrlReturnsDefaultUrlForUnmapped() {
        HelpUrl defaultUrl = MockHelpUrl.simpleUrl("default");
        HelpUrl one = MockHelpUrl.simpleUrl("one");

        HelpUrls urls = urls(defaultUrl, one);
        assertThat(urls.getUrl(defaultUrl.getKey()), equalTo(defaultUrl));
        assertThat(urls.getUrl(one.getKey()), equalTo(one));
        assertThat(urls.getUrl("two"), equalTo(defaultUrl));
    }

    @Test
    public void getDefaultReturnsDefault() {
        HelpUrl defaultUrl = MockHelpUrl.simpleUrl("default");
        HelpUrl one = MockHelpUrl.simpleUrl("one");

        HelpUrls urls = urls(defaultUrl, one);
        assertThat(urls.getDefaultUrl(), equalTo(defaultUrl));
    }

    @Test
    public void getKeysReturnKeys() {
        HelpUrl defaultUrl = MockHelpUrl.simpleUrl("default");
        HelpUrl one = MockHelpUrl.simpleUrl("one");

        HelpUrls urls = urls(defaultUrl, one);
        assertThat(urls.getUrlKeys(), containsInAnyOrder(one.getKey(), defaultUrl.getKey()));
    }

    @Test
    public void getKeysIsImmutable() {
        HelpUrl defaultUrl = MockHelpUrl.simpleUrl("default");
        HelpUrl one = MockHelpUrl.simpleUrl("one");

        HelpUrls urls = urls(defaultUrl, one);
        Set<String> urlKeys = urls.getUrlKeys();
        try {
            urlKeys.remove(one.getKey());
            fail("Should not be able to remove key.");
        } catch (RuntimeException expected) {
            //good
        }
        assertThat(urlKeys, containsInAnyOrder(one.getKey(), defaultUrl.getKey()));
    }

    @Test
    public void putsDefaultOverwritesEntryWithSameKey() {
        HelpUrl defaultUrl = MockHelpUrl.simpleUrl("default");
        HelpUrl one = MockHelpUrl.simpleUrl("one");
        HelpUrl two = MockHelpUrl.simpleUrl("default").setTitle("NonDefault");

        HelpUrls urls = urls(defaultUrl, one, two);
        assertThat(urls.getDefaultUrl(), equalTo(defaultUrl));
        assertThat(urls.getUrl(defaultUrl.getKey()), equalTo(defaultUrl));
        assertThat(urls.getUrl(one.getKey()), equalTo(one));
    }

    @Test
    public void getUrlsReturnsAllUrls() {
        HelpUrl defaultUrl = MockHelpUrl.simpleUrl("default");
        HelpUrl one = MockHelpUrl.simpleUrl("one");
        HelpUrl two = MockHelpUrl.simpleUrl("two");

        HelpUrls urls = urls(defaultUrl, one, two);
        assertThat(urls, containsInAnyOrder(defaultUrl, one, two));
    }

    @Test
    public void mergePreservesDefaultUrlFromFirst() {
        HelpUrl defaultUrl = MockHelpUrl.simpleUrl("default");
        ImmutableHelpUrls first = urls(defaultUrl);

        HelpUrl defaultUrl2 = MockHelpUrl.simpleUrl("default").setUrl("default2");
        ImmutableHelpUrls second = urls(defaultUrl2);

        assertThat(ImmutableHelpUrls.merge(first, second).getDefaultUrl(), is(defaultUrl));
        assertThat(ImmutableHelpUrls.merge(second, first).getDefaultUrl(), is(defaultUrl2));
    }

    @Test
    public void mergeFlattensUrlsInOrder() {
        HelpUrl defaultUrl = MockHelpUrl.simpleUrl("default");
        HelpUrl one = MockHelpUrl.simpleUrl("one");
        HelpUrl two = MockHelpUrl.simpleUrl("two");
        HelpUrl three = MockHelpUrl.simpleUrl("three");

        ImmutableHelpUrls first = urls(defaultUrl, one, two, three);

        HelpUrl defaultUrl2 = MockHelpUrl.simpleUrl("default").setUrl("default2");
        HelpUrl one2 = MockHelpUrl.simpleUrl("one").setUrl("one2");
        HelpUrl two2 = MockHelpUrl.simpleUrl("two").setUrl("two2");

        ImmutableHelpUrls second = urls(defaultUrl2, one2, two2);

        assertThat(ImmutableHelpUrls.merge(first, second), containsInAnyOrder(defaultUrl, one2, two2, three));
    }

    @Test
    public void getUrlsReturnsAllUrlsWithDefaultTakingPrecedence() {
        HelpUrl defaultUrl = MockHelpUrl.simpleUrl("default");
        HelpUrl one = MockHelpUrl.simpleUrl("one");
        HelpUrl two = MockHelpUrl.simpleUrl("two");
        MockHelpUrl fakeDefault = MockHelpUrl.simpleUrl("default").setTitle("Some");

        HelpUrls urls = urls(defaultUrl, one, two, fakeDefault);
        assertThat(urls, containsInAnyOrder(defaultUrl, one, two));
    }

    private ImmutableHelpUrls urls(HelpUrl defaultUrl, HelpUrl... urls) {
        return new ImmutableHelpUrls(defaultUrl, Arrays.asList(urls));
    }

    private Map<String, HelpUrl> asMap(HelpUrl defaultUrl, HelpUrl... urls) {
        Map<String, HelpUrl> result = Maps.newHashMap();
        for (HelpUrl url : urls) {
            result.put(url.getKey(), url);
        }
        result.put(defaultUrl.getKey(), defaultUrl);
        return result;
    }

}
