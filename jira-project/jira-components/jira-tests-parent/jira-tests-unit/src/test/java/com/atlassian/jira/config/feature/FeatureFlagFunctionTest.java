package com.atlassian.jira.config.feature;

import com.atlassian.jira.config.FeatureManager;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

public class FeatureFlagFunctionTest {
    @Mock
    FeatureManager jiraFeatureManager;

    FeatureFlagFunction function;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        when(jiraFeatureManager.isEnabled(anyString())).thenReturn(false);
        function = new FeatureFlagFunction(jiraFeatureManager);
    }

    @Test
    public void testDefaultOnFeatureFlag() {
        String featureKey = "test.on.feature.flag";

        when(jiraFeatureManager.isEnabled(any(String.class))).thenReturn(true);

        assertTrue("Test feature flag should be enabled", function.apply(featureKey));
    }

    @Test
    public void testDefaultOffFeatureFlag() {
        String featureKey = "test.off.feature.flag";

        when(jiraFeatureManager.isEnabled(any(String.class))).thenReturn(false);

        assertFalse("Test feature flag should be disabled", function.apply(featureKey));
    }


    @Test(expected = IllegalArgumentException.class)
    public void testWrongNumberOfArguments() {
        function.apply("test.some.feature.flag", "blah!");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testWrongArgumentType() {
        function.apply(new Object());
    }
}
