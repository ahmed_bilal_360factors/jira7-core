package com.atlassian.jira.user;

import com.atlassian.jira.util.collect.CollectionBuilder;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.List;

import static org.hamcrest.Matchers.empty;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Unit tests for {@link com.atlassian.jira.user.DefaultUserAdminHistoryManager}
 *
 * @since v4.1
 */
public class TestDefaultUserAdminHistoryManager {
    @Rule
    public MockitoRule initMocks = MockitoJUnit.rule();
    @Mock
    private UserHistoryManager historyManager;
    @InjectMocks
    private DefaultUserAdminHistoryManager adminHistoryManager;
    private final ApplicationUser user = new MockApplicationUser("admin");


    @Test(expected = IllegalArgumentException.class)
    public void testAddAdminPageNull() {
        adminHistoryManager.addAdminPageToHistory(user, null, null);

    }

    @Test
    public void testAddAdminPageNullUser() {
        final String key = "123";

        adminHistoryManager.addAdminPageToHistory(null, key, null);
        verify(historyManager).addItemToHistory(UserHistoryItem.ADMIN_PAGE, null, "123", null);
    }

    @Test
    public void testAddProject() {
        final String key = "123";

        adminHistoryManager.addAdminPageToHistory(user, key, null);

        verify(historyManager).addItemToHistory(UserHistoryItem.ADMIN_PAGE, user, "123", null);
    }


    @Test
    public void testGetAdminPageHistoryWithOutChecks() {
        final UserHistoryItem item1 = new UserHistoryItem(UserHistoryItem.ADMIN_PAGE, "123");
        final UserHistoryItem item2 = new UserHistoryItem(UserHistoryItem.ADMIN_PAGE, "1234");
        final UserHistoryItem item3 = new UserHistoryItem(UserHistoryItem.ADMIN_PAGE, "1235");
        final UserHistoryItem item4 = new UserHistoryItem(UserHistoryItem.ADMIN_PAGE, "1236");
        final List<UserHistoryItem> list = CollectionBuilder.newBuilder(item1, item2, item3, item4).asList();
        when(historyManager.getHistory(UserHistoryItem.ADMIN_PAGE, user)).thenReturn(list);

        final List<UserHistoryItem> returnedList = adminHistoryManager.getAdminPageHistoryWithoutPermissionChecks(user);

        assertEquals(list, returnedList);
    }

    @Test
    public void testGetAdminPageHistoryWithOutChecksNullUser() {
        final UserHistoryItem item1 = new UserHistoryItem(UserHistoryItem.ADMIN_PAGE, "123");
        final UserHistoryItem item2 = new UserHistoryItem(UserHistoryItem.ADMIN_PAGE, "1234");
        final UserHistoryItem item3 = new UserHistoryItem(UserHistoryItem.ADMIN_PAGE, "1235");
        final UserHistoryItem item4 = new UserHistoryItem(UserHistoryItem.ADMIN_PAGE, "1236");
        final List<UserHistoryItem> list = CollectionBuilder.newBuilder(item1, item2, item3, item4).asList();
        when(historyManager.getHistory(UserHistoryItem.ADMIN_PAGE, null)).thenReturn(list);

        final List<UserHistoryItem> returnedList = adminHistoryManager.getAdminPageHistoryWithoutPermissionChecks(null);

        assertEquals(list, returnedList);
    }

    @Test
    public void testGetAdminPageHistoryWithOutChecksNullHistory() {
        when(historyManager.getHistory(UserHistoryItem.ADMIN_PAGE, user)).thenReturn(null);

        final List<UserHistoryItem> returnedList = adminHistoryManager.getAdminPageHistoryWithoutPermissionChecks(user);
        assertThat(returnedList, empty());

    }


}
