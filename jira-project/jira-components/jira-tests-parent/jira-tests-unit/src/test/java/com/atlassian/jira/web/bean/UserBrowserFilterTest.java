package com.atlassian.jira.web.bean;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.collectors.CollectorsUtil;
import com.atlassian.crowd.embedded.api.CrowdService;
import com.atlassian.crowd.embedded.api.Query;
import com.atlassian.jira.application.ApplicationRole;
import com.atlassian.jira.application.ApplicationRoleManager;
import com.atlassian.jira.application.MockApplicationRole;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.util.MockUserManager;
import com.google.common.collect.ImmutableList;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;

import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.atlassian.labs.mockito.MockitoBooster.whenOption;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.isEmptyOrNullString;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


/**
 * @since v7.0
 */
public class UserBrowserFilterTest {
    @Rule
    public MockitoContainer container = MockitoMocksInContainer.rule(this);
    @Rule
    public ExpectedException exception = ExpectedException.none();

    private final Locale locale = Locale.getDefault();
    @Mock
    private ApplicationRoleManager roleManager;
    @AvailableInContainer
    @Mock
    private CrowdService crowdService;
    @AvailableInContainer(instantiateMe = true)
    private MockUserManager userManager;

    private UserBrowserFilter userBrowserFilter;

    private ApplicationUser fred;
    private ApplicationUser bob;
    private ApplicationUser george;

    @Before
    public void setUp() {
        fred = userManager.addUser(new MockApplicationUser("fred"));
        bob = userManager.addUser(new MockApplicationUser("bob"));
        george = userManager.addUser(new MockApplicationUser("george"));
        final List<ApplicationUser> allUsers = ImmutableList.of(fred, bob, george);
        final Map<String, ApplicationUser> userMap = allUsers.stream().collect(Collectors.toMap(ApplicationUser::getName, Function.identity()));

        when(crowdService.search(anyObject())).then(invocation ->
        {
            Query query = (Query) invocation.getArguments()[0];
            if (query.getReturnType() == String.class) {
                return allUsers.stream().map(ApplicationUser::getName).collect(CollectorsUtil.toImmutableList());
            } else {
                return allUsers.stream().map(ApplicationUser::getDirectoryUser).collect(CollectorsUtil.toImmutableList());
            }
        });
        when(crowdService.getUser(anyString())).then(invocation -> userMap.get(invocation.getArgumentAt(0, String.class)).getDirectoryUser());

        userBrowserFilter = new UserBrowserFilter(locale, roleManager);
    }

    @Test
    public void anyAccessSpecialValueIsNotValidApplicationKey() {
        exception.expect(IllegalArgumentException.class);
        exception.expectMessage("'key' must match the regular expression");
        ApplicationKey.valueOf(UserBrowserFilter.ANY_APPLICATION_ACCESS_FILTER);
    }

    @Test
    public void noAccessSpecialValueIsNotValidApplicationKey() {
        exception.expect(IllegalArgumentException.class);
        exception.expectMessage("'key' must match the regular expression");
        ApplicationKey.valueOf(UserBrowserFilter.NO_APPLICATION_ACCESS_FILTER);
    }

    @Test
    public void shouldNotFilterByApplicationWhenApplicationSetIsEmpty() throws Exception {
        userBrowserFilter.setApplicationFilter("");

        assertThat(userBrowserFilter.getFilteredUsers(), containsInAnyOrder(bob, fred, george));
    }

    @Test
    public void shouldReturnUsersWithAnyAccessWhenAnyFilterIsSetUp() throws Exception {
        userBrowserFilter.setApplicationFilter(UserBrowserFilter.ANY_APPLICATION_ACCESS_FILTER);
        when(roleManager.hasAnyRole(fred)).thenReturn(true);
        when(roleManager.hasAnyRole(bob)).thenReturn(true);

        assertThat(userBrowserFilter.getFilteredUsers(), containsInAnyOrder(fred, bob));
    }

    @Test
    public void shouldReturnUsersWithoutAnyAccessWhenNoAccessFilterIsSetUp() throws Exception {
        userBrowserFilter.setApplicationFilter(UserBrowserFilter.NO_APPLICATION_ACCESS_FILTER);
        when(roleManager.hasAnyRole(bob)).thenReturn(true);

        assertThat(userBrowserFilter.getFilteredUsers(), containsInAnyOrder(fred, george));
    }

    @Test
    public void shouldReturnFilteredByStrictApplicationAccessWhenIsSet() throws Exception {
        ApplicationKey myKey = ApplicationKey.valueOf("my-application-key");
        ApplicationRole myRole = new MockApplicationRole(myKey);
        whenOption(roleManager.getRole(myKey)).thenSome(myRole);

        when(roleManager.userOccupiesRole(bob, myKey)).thenReturn(true);
        when(roleManager.userOccupiesRole(george, myKey)).thenReturn(true);

        userBrowserFilter.setApplicationFilter("my-application-key");
        assertThat(userBrowserFilter.getFilteredUsers(), containsInAnyOrder(bob, george));
    }

    @Test
    public void shouldIgnoreGivenKeyWhenIsNotExisting() throws Exception {
        ApplicationKey myKey = ApplicationKey.valueOf("non-installed-key");
        whenOption(roleManager.getRole(myKey)).thenNone();

        assertThat(userBrowserFilter.getFilteredUsers(), containsInAnyOrder(bob, fred, george));

        verify(roleManager, never()).userHasRole(any(ApplicationUser.class), any(ApplicationKey.class));
    }

    @Test
    public void applicationGetterAndSetterShouldWorkAsRegularGetterForSpecials() throws Exception {
        checkSetGet(UserBrowserFilter.ANY_APPLICATION_ACCESS_FILTER);
        checkSetGet(UserBrowserFilter.NO_APPLICATION_ACCESS_FILTER);
    }

    @Test
    public void applicationGetterAndSetterShouldWorkAsRegularGetterExistingApps() throws Exception {
        final ApplicationKey key = ApplicationKey.valueOf("my-valid-key");
        whenOption(roleManager.getRole(key)).thenSome(new MockApplicationRole(key));
        checkSetGet("my-valid-key");
    }

    @Test
    public void applicationGetterShouldReturnNullWhenApplicationUnknown() throws Exception {
        userBrowserFilter.setApplicationFilter("completely invalid key!");
        assertThat(userBrowserFilter.getApplicationFilter(), isEmptyOrNullString());

        whenOption(roleManager.getRole(ApplicationKey.valueOf("my-valid-key"))).thenNone();
        userBrowserFilter.setApplicationFilter("my-valid-key");
        assertThat(userBrowserFilter.getApplicationFilter(), isEmptyOrNullString());
    }

    @Test
    public void applicationGetterShouldReturnNullWhenApplicationInvalid() throws Exception {
        whenOption(roleManager.getRole(ApplicationKey.valueOf("my-valid-key"))).thenNone();
        userBrowserFilter.setApplicationFilter("my-valid-key");
        assertThat(userBrowserFilter.getApplicationFilter(), isEmptyOrNullString());
    }

    private void checkSetGet(final String filterValue) {
        userBrowserFilter.setApplicationFilter(filterValue);
        assertThat(userBrowserFilter.getApplicationFilter(), equalTo(filterValue));
    }
}
