package com.atlassian.jira.issue.fields.csv.custom;

import com.atlassian.jira.issue.customfields.impl.AbstractSingleFieldType;
import com.atlassian.jira.issue.customfields.impl.RenderableTextCFType;

public class RenderableTextCFTypeCsvExportTest extends TextCustomFieldCsvExportTest {
    @Override
    protected AbstractSingleFieldType<String> createField() {
        return new RenderableTextCFType(null, null, null, null);
    }
}
