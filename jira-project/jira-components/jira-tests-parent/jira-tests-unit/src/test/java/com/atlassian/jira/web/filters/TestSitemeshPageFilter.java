package com.atlassian.jira.web.filters;

import com.atlassian.jira.util.collect.IteratorEnumeration;
import com.google.common.collect.ImmutableList;
import com.opensymphony.module.sitemesh.Factory;
import com.opensymphony.module.sitemesh.RequestConstants;
import com.opensymphony.sitemesh.webapp.SiteMeshFilter;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.springframework.util.ReflectionUtils;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Collections;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Test case for the SitemeshPageFilter,
 *
 * @since v4.2
 */
public class TestSitemeshPageFilter {
    /**
     * The request attribute that needs to get set (got using reflection).
     */
    private static final String ALREADY_APPLIED_KEY;

    static {
        final Field f = ReflectionUtils.findField(SiteMeshFilter.class, "ALREADY_APPLIED_KEY");

        ReflectionUtils.makeAccessible(f);
        ALREADY_APPLIED_KEY = (String) ReflectionUtils.getField(f, null);
    }

    @Rule
    public MethodRule initMockito = MockitoJUnit.rule();
    @Mock
    private HttpServletRequest httpRequest;
    @Mock
    private FilterConfig filterConfig;
    @Mock
    private ServletContext servletContext;
    @Mock
    private Factory factory;
    @Mock
    private FilterChain filterChain;
    @Mock
    private HttpServletResponse httpServletResponse;

    private SitemeshPageFilter tested;

    @Test
    public void differentPathShouldNotBeExcluded() throws Exception {
        setExcludedPaths("/one,/two,/four");

        runTestNotExcluded("/three");
    }

    @Test
    public void subpathShouldNotBeExcludedByExactPattern() throws Exception {
        setExcludedPaths("/one");

        runTestNotExcluded("/one/three");
    }

    @Test
    public void exactMatchShouldBeExcluded() throws Exception {
        setExcludedPaths("/one");

        runTestExcluded("/one");
    }

    @Test
    public void subpathShouldBeExcludedByWildcardPattern() throws Exception {
        setExcludedPaths("/one,/two/*");

        runTestExcluded("/two/three");
    }

    @Test
    public void wildcardAfterSlashShouldNotExcludePath() throws Exception {
        setExcludedPaths("/one/two/*,/one");

        runTestNotExcluded("/one/two");
    }

    @Test
    public void wildcardWithNoSlashShouldExcludeFullPath() throws Exception {
        setExcludedPaths("/one/two*");

        runTestExcluded("/one/two");
    }

    @Test
    public void pathsInInitParamShouldBeTrimmed() throws Exception {
        setExcludedPaths("  /one,\n/one/two  ,/three");

        runTestExcluded("/one/two");
    }

    @Test(expected = IllegalStateException.class)
    public void excludePathInitParamIsRequired() throws Exception {
        setExcludedPaths(null);
        runTestNotExcluded("/abc");
    }

    @Test
    public void excludeHeaderIsWorking() throws Exception {
        setExcludedPaths("/ignored");
        setExcludedHeaders("X-IGNORE, X-SHEEIT");
        when(httpRequest.getHeaders("X-IGNORE")).thenReturn(IteratorEnumeration.fromIterable(ImmutableList.of("true")));
        when(httpRequest.getHeaderNames()).thenReturn(IteratorEnumeration.fromIterable(ImmutableList.of("X-IGNORE")));

        tested.init(filterConfig);
        tested.doFilter(httpRequest, httpServletResponse, filterChain);

        verifyExcluded();
    }

    private void setExcludedHeaders(final String excludedHeaders) {
        when(filterConfig.getInitParameter(SitemeshPageFilter.InitParams.EXCLUDED_HEADERS.key())).thenReturn(excludedHeaders);
    }

    @Before
    public void setUpMocks() throws Exception {
        tested = new SitemeshPageFilter();

        setUpRequest();
        setUpFilterConfig();
    }

    private void setUpFilterConfig() {
        when(filterConfig.getFilterName()).thenReturn("PathExclusion");
        when(filterConfig.getServletContext()).thenReturn(servletContext);
        when(servletContext.getAttribute("sitemesh.factory")).thenReturn(factory);
    }

    private void setUpRequest() {
        when(httpRequest.getAttribute(ALREADY_APPLIED_KEY)).thenReturn(null);

        // stuff that the sitemesh filter sets
        when(httpRequest.getPathInfo()).thenReturn(null);
        when(httpRequest.getQueryString()).thenReturn(null);

        // may or may not get called
        httpRequest.setAttribute("com.opensymphony.sitemesh.USINGSTREAM", false);
        httpRequest.setAttribute(RequestConstants.SECONDARY_STORAGE_LIMIT, -1L);
    }

    private void runTestNotExcluded(final String servletPath) throws IOException, ServletException {
        runTest(servletPath);
        verifyNotExcluded();
    }

    private void verifyNotExcluded() {
        //this bases on fact that site mesh leaves side effect on httpRequest with this flag being set to true
        verify(httpRequest).setAttribute(ALREADY_APPLIED_KEY, true);
    }

    private void runTestExcluded(final String servletPath) throws IOException, ServletException {
        runTest(servletPath);
        verifyExcluded();
    }

    private void verifyExcluded() {
        //this bases on fact that site mesh leaves side effect on httpRequest so if it was not executed there is no side effect
        verify(httpRequest, never()).setAttribute(ALREADY_APPLIED_KEY, true);
    }

    private void runTest(final String servletPath) throws IOException, ServletException {
        // set up request path and headers
        when(httpRequest.getServletPath()).thenReturn(servletPath);
        when(httpRequest.getHeaderNames()).thenReturn(IteratorEnumeration.fromIterable(Collections.<String>emptyList()));

        tested.init(filterConfig);
        tested.doFilter(httpRequest, httpServletResponse, filterChain);
        tested.destroy();

    }

    private void setExcludedPaths(final String excludedPaths) {
        when(filterConfig.getInitParameter(SitemeshPageFilter.InitParams.EXCLUDED_PATHS.key())).thenReturn(excludedPaths);
    }
}
