package com.atlassian.jira.plugin.bigpipe;

import com.atlassian.plugin.elements.ResourceDescriptor;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Map;

public class ContextPriorityConfigurerForTesting implements BigPipePriorityConfigurer {
    static final String PRIORITY_KEY = "my.priority";

    @Nullable
    @Override
    public Integer calculatePriority(@Nonnull ResourceDescriptor resource, @Nonnull Map<String, ?> context) {
        return (Integer) context.get(PRIORITY_KEY);
    }
}
