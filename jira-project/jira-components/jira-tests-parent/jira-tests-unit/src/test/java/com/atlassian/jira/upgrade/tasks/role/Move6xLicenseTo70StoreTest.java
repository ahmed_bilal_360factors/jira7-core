package com.atlassian.jira.upgrade.tasks.role;

import com.atlassian.jira.auditing.AssociatedItem;
import com.atlassian.jira.test.util.lic.License;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static com.atlassian.fugue.Option.some;
import static com.atlassian.jira.test.util.lic.other.OtherLicenses.LICENSE_PRE_JIRA_APP_V2_COMMERCIAL;
import static com.atlassian.jira.test.util.lic.sd.ServiceDeskLicenses.LICENSE_SERVICE_DESK_ABP;
import static com.atlassian.jira.test.util.lic.software.SoftwareLicenses.LICENSE_SOFTWARE;
import static com.atlassian.jira.upgrade.tasks.role.TestUtils.emptyState;
import static com.atlassian.jira.upgrade.tasks.role.TestUtils.licensesState;
import static com.atlassian.jira.upgrade.tasks.role.TestUtils.toLicense;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class Move6xLicenseTo70StoreTest {
    @Rule
    public MockitoRule mockitoMocks = MockitoJUnit.rule();

    @Mock
    private LicenseDao dao;

    @Test
    public void migrationDoesNothingWhenNo6xLicense() {
        //given
        MigrationState state = emptyState().changeLicenses(l -> l.addLicense(toLicense(LICENSE_SERVICE_DESK_ABP)));

        //when
        final MigrationState finalState = new Move6xLicenseTo70Store(dao).migrate(state, false);

        //then
        assertThat(finalState, new MigrationStateMatcher()
                .licenses(LICENSE_SERVICE_DESK_ABP)
                .log(cantFindExistingLicenseAudit()));
    }

    @Test
    public void migrationDoesNotAccess6xWithUserSuppliedLicense() {
        //given
        when(dao.get6xLicense()).thenThrow(new MigrationFailedException("Broken"));
        MigrationState state = emptyState().changeLicenses(l -> l.addLicense(toLicense(LICENSE_SERVICE_DESK_ABP)));

        //when
        final MigrationState finalState = new Move6xLicenseTo70Store(dao).migrate(state, true);

        //then
        assertThat(finalState, new MigrationStateMatcher()
                .licenses(LICENSE_SERVICE_DESK_ABP)
                .log(userProvidedLicenseAudit()));
    }

    @Test
    public void migrationMoves6xLicenseIntoStore() {
        //given
        when(dao.get6xLicense()).thenReturn(some(toLicense(LICENSE_SOFTWARE)));
        MigrationState initialState = emptyState();

        //when
        final MigrationState finalState = new Move6xLicenseTo70Store(dao).migrate(initialState, false);

        //then
        assertThat(finalState, new MigrationStateMatcher()
                .licenses(LICENSE_SOFTWARE)
                .log(movedLicenseAudit(LICENSE_SOFTWARE)));
    }

    @Test
    public void migrationDoesNotMove6xLicenseIfAlreadyInStore() {
        //given
        when(dao.get6xLicense()).thenReturn(some(toLicense(LICENSE_SOFTWARE)));
        MigrationState initialState = licensesState(LICENSE_PRE_JIRA_APP_V2_COMMERCIAL);

        //when
        final MigrationState finalState = new Move6xLicenseTo70Store(dao).migrate(initialState, false);

        //then
        assertThat(finalState, new MigrationStateMatcher()
                .licenses(LICENSE_PRE_JIRA_APP_V2_COMMERCIAL)
                .log(licenseAlreadyExistsAudit()));
    }

    @Test
    public void migrationMoves6xLicenseIfUnrelatedLicenseInTheStore() {
        //given
        when(dao.get6xLicense()).thenReturn(some(toLicense(LICENSE_SOFTWARE)));
        MigrationState initialState = TestUtils.licensesState(LICENSE_SERVICE_DESK_ABP);

        //when
        final MigrationState finalState = new Move6xLicenseTo70Store(dao).migrate(initialState, false);

        //then
        assertThat(finalState, new MigrationStateMatcher()
                .licenses(LICENSE_SOFTWARE, LICENSE_SERVICE_DESK_ABP)
                .log(movedLicenseAudit(LICENSE_SOFTWARE)));
    }

    @Test
    public void afterSaveTaskRemovesOldJira6xLicense() {
        final MigrationState state = new Move6xLicenseTo70Store(dao).migrate(emptyState(), false);

        state.afterSaveTasks().forEach(Runnable::run);

        verify(dao).remove6xLicense();
    }

    private static AuditEntry movedLicenseAudit(final License license) {
        return new AuditEntry(Move6xLicenseTo70Store.class,
                "Moved license location",
                "Moved license from old location in application properties to new location in license table",
                AssociatedItem.Type.LICENSE,
                license.getSen(), false);
    }

    private static AuditEntry licenseAlreadyExistsAudit() {
        return new AuditEntry(Move6xLicenseTo70Store.class,
                "Not moving license location",
                "Incompatible license already exists in license table.",
                AssociatedItem.Type.LICENSE, null, false);
    }

    private static AuditEntry cantFindExistingLicenseAudit() {
        return new AuditEntry(Move6xLicenseTo70Store.class,
                "Not moving license location",
                "No valid existing license present in data.",
                AssociatedItem.Type.LICENSE, null, false);
    }

    private static AuditEntry userProvidedLicenseAudit() {
        return new AuditEntry(Move6xLicenseTo70Store.class,
                "Not moving license location",
                "License entered during import supersedes all licenses in data.",
                AssociatedItem.Type.LICENSE, null, false);
    }
}