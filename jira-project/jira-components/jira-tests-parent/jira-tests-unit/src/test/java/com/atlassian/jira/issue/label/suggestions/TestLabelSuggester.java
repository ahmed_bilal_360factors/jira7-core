package com.atlassian.jira.issue.label.suggestions;

import com.atlassian.jira.issue.customfields.CustomFieldUtils;
import com.atlassian.jira.issue.index.DocumentConstants;
import com.atlassian.jira.issue.index.indexers.impl.CustomFieldLabelsIndexer;
import com.atlassian.jira.issue.label.Label;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.google.common.collect.ImmutableSet;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Collections;
import java.util.Set;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

public class TestLabelSuggester {
    private static final String EMPTY_TOKEN = "";
    private static final long ISSUE_ID = 1000L;
    private static final Set<String> POPULAR_LABELS = ImmutableSet.of("most", "popular", "labels");
    private static final Set<String> PREFIXED_LABELS = ImmutableSet.of("prefix-a", "prefix-b", "prefix-c");
    private static final Set<Label> ISSUE_LABELS = ImmutableSet.of(new Label(1L, ISSUE_ID, "most"), new Label(2L, ISSUE_ID, "popular"));
    private static final Set<Label> ISSUE_WITH_NO_LABELS = Collections.emptySet();
    private static final String PREFIX = "prefix";
    private static final String SEARCH_FIELD = DocumentConstants.ISSUE_LABELS_FOLDED;
    private static final String DISPLAY_FIELD = DocumentConstants.ISSUE_LABELS;
    private static final ApplicationUser user = new MockApplicationUser("admin");
    private static final long CUSTOM_FIELD_ID = 1234L;
    private static final String CUSTOM_FIELD = CustomFieldUtils.CUSTOM_FIELD_PREFIX + CUSTOM_FIELD_ID;

    @Rule
    public MockitoRule initializeMocksRule = MockitoJUnit.rule();

    @Mock
    PopularLabelsProvider popularLabelsProvider;

    @Mock
    PrefixSearchLabelsProvider prefixSearchLabelsProvider;

    @Test
    public void testPopularLabelsSuggestions() {
        // Given
        when(popularLabelsProvider.findMostPopular(DISPLAY_FIELD, user)).thenReturn(POPULAR_LABELS);

        final LabelSuggester labelSuggester = new LabelSuggester(popularLabelsProvider, prefixSearchLabelsProvider);

        // When
        final Set<String> suggestedLabels = labelSuggester.getSuggestedLabels(EMPTY_TOKEN, ISSUE_ID, ISSUE_WITH_NO_LABELS, user);

        // Then
        assertEquals(POPULAR_LABELS, suggestedLabels);
    }

    @Test
    public void testNullToken() {
        // Given
        when(popularLabelsProvider.findMostPopular(DISPLAY_FIELD, user)).thenReturn(POPULAR_LABELS);

        final LabelSuggester labelSuggester = new LabelSuggester(popularLabelsProvider, prefixSearchLabelsProvider);

        // When
        final Set<String> suggestedLabels = labelSuggester.getSuggestedLabels(null, ISSUE_ID, ISSUE_WITH_NO_LABELS, user);

        // Then
        assertEquals(POPULAR_LABELS, suggestedLabels);
    }

    @Test
    public void testPrefixSearchLabelSuggestions() {
        // Given
        when(prefixSearchLabelsProvider.findByPrefixToken(PREFIX, SEARCH_FIELD, DISPLAY_FIELD, user))
                .thenReturn(PREFIXED_LABELS);

        final LabelSuggester labelSuggester = new LabelSuggester(popularLabelsProvider, prefixSearchLabelsProvider);

        // When
        final Set<String> suggestedLabels = labelSuggester.getSuggestedLabels(PREFIX, ISSUE_ID, ISSUE_WITH_NO_LABELS, user);

        // Then
        assertEquals(PREFIXED_LABELS, suggestedLabels);
    }

    @Test
    public void testRemovingLabelsFoundInGivenIssue() {
        // Given
        when(popularLabelsProvider.findMostPopular(DISPLAY_FIELD, user)).thenReturn(POPULAR_LABELS);

        final LabelSuggester labelSuggester = new LabelSuggester(popularLabelsProvider, prefixSearchLabelsProvider);

        // When
        final Set<String> suggestedLabels = labelSuggester.getSuggestedLabels(EMPTY_TOKEN, ISSUE_ID, ISSUE_LABELS, user);

        // Then
        assertEquals(ImmutableSet.of("labels"), suggestedLabels);
    }

    @Test
    public void testSortingSuggestions() {
        // Given
        when(prefixSearchLabelsProvider.findByPrefixToken("aNdro", SEARCH_FIELD, DISPLAY_FIELD, user))
                .thenReturn(ImmutableSet.of("Android", "aNdroid", "Androids"));

        final LabelSuggester labelSuggester = new LabelSuggester(popularLabelsProvider, prefixSearchLabelsProvider);

        final String[] sortedLabels = {"aNdroid", "Android", "Androids"};

        // When
        final Set<String> suggestedLabels = labelSuggester.getSuggestedLabels("aNdro", ISSUE_ID, ISSUE_WITH_NO_LABELS, user);

        // Then
        assertArrayEquals(sortedLabels, suggestedLabels.toArray());
    }

    @Test
    public void testCustomFieldSuggestionsWithPopularLabels() {
        // Given
        when(popularLabelsProvider.findMostPopular(CUSTOM_FIELD, user)).thenReturn(POPULAR_LABELS);

        final LabelSuggester labelSuggester = new LabelSuggester(popularLabelsProvider, prefixSearchLabelsProvider);

        // When
        final Set<String> suggestedLabels = labelSuggester.getSuggestedLabels(EMPTY_TOKEN, ISSUE_ID, CUSTOM_FIELD_ID, ISSUE_WITH_NO_LABELS, user);

        // Then
        assertEquals(POPULAR_LABELS, suggestedLabels);
    }

    @Test
    public void testCustomFieldSuggestionsWithPrefixSearch() {
        // Given
        when(prefixSearchLabelsProvider.findByPrefixToken("Andro", CUSTOM_FIELD + CustomFieldLabelsIndexer.FOLDED_EXT, CUSTOM_FIELD, user))
                .thenReturn(ImmutableSet.of("Android", "aNdroid", "Androids"));

        final LabelSuggester labelSuggester = new LabelSuggester(popularLabelsProvider, prefixSearchLabelsProvider);

        final String[] sortedLabels = {"Android", "Androids", "aNdroid"};

        // When
        final Set<String> suggestedLabels = labelSuggester.getSuggestedLabels("Andro", ISSUE_ID, CUSTOM_FIELD_ID, ISSUE_WITH_NO_LABELS, user);

        // Then
        assertArrayEquals(sortedLabels, suggestedLabels.toArray());
    }
}