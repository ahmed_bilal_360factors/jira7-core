package com.atlassian.jira.plugin.jql.function;

import com.atlassian.jira.JiraDataTypes;
import com.atlassian.jira.jql.operand.QueryLiteral;
import com.atlassian.jira.jql.query.QueryCreationContext;
import com.atlassian.jira.jql.query.QueryCreationContextImpl;
import com.atlassian.jira.junit.rules.InitMockitoMocks;
import com.atlassian.jira.mock.plugin.jql.operand.MockJqlFunctionModuleDescriptor;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.util.UserUtil;
import com.atlassian.jira.util.MessageSet;
import com.atlassian.query.clause.TerminalClause;
import com.atlassian.query.operand.FunctionOperand;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.mockito.Mock;

import java.util.ArrayList;
import java.util.List;

import static com.atlassian.jira.mock.plugin.jql.operand.MockJqlFunctionModuleDescriptor.create;
import static com.atlassian.jira.permission.ProjectPermissions.BROWSE_PROJECTS;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestProjectsLeadByUserFunction {
    private ApplicationUser theUser;
    private QueryCreationContext queryCreationContext;
    private final TerminalClause terminalClause = null;
    private final List<Project> projectsList1 = new ArrayList<>();
    private final List<Project> projectsList2 = new ArrayList<>();

    @Rule
    public TestRule initMockito = new InitMockitoMocks(this);

    @Mock
    UserUtil userUtil;
    @Mock
    ProjectManager projectManager;
    @Mock
    PermissionManager permissionManager;

    @Before
    public void setUp() throws Exception {
        theUser = new MockApplicationUser("fred");
        queryCreationContext = new QueryCreationContextImpl(theUser);

        final Project project1 = new MockProject(21l, "c1");
        final Project project2 = new MockProject(22l, "c2");
        final Project project3 = new MockProject(23l, "c3");
        final Project project4 = new MockProject(24l, "c4");

        projectsList1.add(project1);
        projectsList1.add(project2);
        projectsList1.add(project3);
        projectsList1.add(project4);

        projectsList2.add(project1);
        projectsList2.add(project2);
    }

    @Test
    public void testDataType() throws Exception {
        final ProjectsLeadByUserFunction projectsLeadByUserFunction = new ProjectsLeadByUserFunction(permissionManager, projectManager, userUtil);

        assertEquals(JiraDataTypes.PROJECT, projectsLeadByUserFunction.getDataType());
    }

    @Test
    public void testValidateWrongArgs() throws Exception {
        final ProjectsLeadByUserFunction projectsLeadByUserFunction = new ProjectsLeadByUserFunction(permissionManager, projectManager, userUtil);
        projectsLeadByUserFunction.init(MockJqlFunctionModuleDescriptor.create("projectsLeadByUser", true));

        MessageSet messageSet = projectsLeadByUserFunction.validate(new MockApplicationUser("bob"), new FunctionOperand("projectsLeadByUser", "badArg1", "badArg2"), terminalClause);
        assertTrue(messageSet.hasAnyErrors());
        assertEquals(1, messageSet.getErrorMessages().size());
        assertEquals("Function 'projectsLeadByUser' expected between '0' and '1' arguments but received '2'.", messageSet.getErrorMessages().iterator().next());

        messageSet = projectsLeadByUserFunction.validate(new MockApplicationUser("bob"), new FunctionOperand("projectsLeadByUser", "badUser"), terminalClause);
        assertEquals(1, messageSet.getErrorMessages().size());
        assertEquals("Function 'projectsLeadByUser' can not generate a list of projects for user 'badUser'; the user does not exist.", messageSet.getErrorMessages().iterator().next());

    }

    @Test
    public void testValidateHappyPath() throws Exception {
        final ProjectsLeadByUserFunction projectsLeadByUserFunction = new ProjectsLeadByUserFunction(permissionManager, projectManager, userUtil);
        projectsLeadByUserFunction.init(create("projectsLeadByUser", true));

        // No user name supplied
        MessageSet messageSet = projectsLeadByUserFunction.validate(new MockApplicationUser("bob"), new FunctionOperand("projectsLeadByUser"), terminalClause);
        assertFalse(messageSet.hasAnyErrors());

        // One valid user name supplied
        when(userUtil.getUserByName("fred")).thenReturn(theUser);
        messageSet = projectsLeadByUserFunction.validate(new MockApplicationUser("bob"), new FunctionOperand("projectsLeadByUser", "fred"), terminalClause);
        assertFalse(messageSet.hasAnyErrors());
    }

    @Test
    public void testValidateAnonymous() {
        final ProjectsLeadByUserFunction projectsLeadByUserFunction = new ProjectsLeadByUserFunction(permissionManager, projectManager, userUtil);
        projectsLeadByUserFunction.init(MockJqlFunctionModuleDescriptor.create("projectsLeadByUser", true));

        final MessageSet messageSet = projectsLeadByUserFunction.validate(null, new FunctionOperand("projectsLeadByUser"), terminalClause);
        assertTrue(messageSet.hasAnyErrors());
        assertEquals(1, messageSet.getErrorMessages().size());
        assertEquals("Function 'projectsLeadByUser' cannot be called as anonymous user.", messageSet.getErrorMessages().iterator().next());
    }

    @Test
    public void testGetValuesHappyPath() throws Exception {
        final ProjectsLeadByUserFunction projectsLeadByUserFunction = new ProjectsLeadByUserFunction(permissionManager, projectManager, userUtil);
        projectsLeadByUserFunction.init(create("projectsLeadByUser", true));

        when(userUtil.getUserByName("fred")).thenReturn(theUser);
        when(projectManager.getProjectsLeadBy(theUser)).thenReturn(projectsList1);
        for (final Project project : projectsList1) {
            when(permissionManager.hasPermission(BROWSE_PROJECTS, project, theUser)).thenReturn(true);
        }
        final ApplicationUser bill = new MockApplicationUser("bill");
        when(userUtil.getUserByName("bill")).thenReturn(bill);
        when(projectManager.getProjectsLeadBy(bill)).thenReturn(projectsList2);
        for (final Project project : projectsList2) {
            when(permissionManager.hasPermission(BROWSE_PROJECTS, project, theUser)).thenReturn(true);
        }

        List<QueryLiteral> list = projectsLeadByUserFunction.getValues(queryCreationContext, new FunctionOperand("projectsLeadByUser"), terminalClause);
        assertEquals(4, list.size());
        assertEquals(new Long(21), list.get(0).getLongValue());
        assertEquals(new Long(22), list.get(1).getLongValue());
        assertEquals(new Long(23), list.get(2).getLongValue());
        assertEquals(new Long(24), list.get(3).getLongValue());

        list = projectsLeadByUserFunction.getValues(queryCreationContext, new FunctionOperand("projectsLeadByUser", "bill"), terminalClause);
        assertEquals(2, list.size());
        assertEquals(new Long(21), list.get(0).getLongValue());
        assertEquals(new Long(22), list.get(1).getLongValue());
    }

    @Test
    public void testGetValuesNoPermissions() throws Exception {
        final ProjectsLeadByUserFunction projectsLeadByUserFunction = new ProjectsLeadByUserFunction(permissionManager, projectManager, userUtil);
        projectsLeadByUserFunction.init(create("projectsLeadByUser", true));

        final ApplicationUser bill = new MockApplicationUser("bill");

        // No permissions on projects 22 & 23
        when(userUtil.getUserByName("fred")).thenReturn(theUser);
        when(projectManager.getProjectsLeadBy(theUser)).thenReturn(projectsList1);
        when(permissionManager.hasPermission(BROWSE_PROJECTS, projectsList1.get(0), theUser)).thenReturn(true);
        when(permissionManager.hasPermission(BROWSE_PROJECTS, projectsList1.get(1), theUser)).thenReturn(false);
        when(permissionManager.hasPermission(BROWSE_PROJECTS, projectsList1.get(2), theUser)).thenReturn(false);
        when(permissionManager.hasPermission(BROWSE_PROJECTS, projectsList1.get(3), theUser)).thenReturn(true);

        when(userUtil.getUserByName("bill")).thenReturn(bill);
        when(projectManager.getProjectsLeadBy(bill)).thenReturn(projectsList2);
        when(permissionManager.hasPermission(BROWSE_PROJECTS, projectsList2.get(0), theUser)).thenReturn(true);
        when(permissionManager.hasPermission(BROWSE_PROJECTS, projectsList2.get(1), theUser)).thenReturn(false);

        List<QueryLiteral> list = projectsLeadByUserFunction.getValues(queryCreationContext, new FunctionOperand("projectsLeadByUser"), terminalClause);
        assertEquals(2, list.size());
        assertEquals(new Long(21), list.get(0).getLongValue());
        assertEquals(new Long(24), list.get(1).getLongValue());

        list = projectsLeadByUserFunction.getValues(queryCreationContext, new FunctionOperand("projectsLeadByUser", "bill"), terminalClause);
        assertEquals(1, list.size());
        assertEquals(new Long(21), list.get(0).getLongValue());
    }

    @Test
    public void testGetValuesAnonymous() {
        final ProjectsLeadByUserFunction projectsLeadByUserFunction = new ProjectsLeadByUserFunction(permissionManager, projectManager, userUtil);
        projectsLeadByUserFunction.init(create("projectsLeadByUser", true));

        final ApplicationUser bill = new MockApplicationUser("bill");
        when(userUtil.getUserByName("bill")).thenReturn(bill);
        when(projectManager.getProjectsLeadBy(bill)).thenReturn(projectsList2);
        for (final Project project : projectsList2) {
            when(permissionManager.hasPermission(BROWSE_PROJECTS, project, null)).thenReturn(true);
        }

        List<QueryLiteral> list = projectsLeadByUserFunction.getValues(new QueryCreationContextImpl((ApplicationUser) null), new FunctionOperand("projectsLeadByUser"), terminalClause);
        assertTrue(list.isEmpty());

        list = projectsLeadByUserFunction.getValues(new QueryCreationContextImpl((ApplicationUser) null), new FunctionOperand("projectsLeadByUser", "bill"), terminalClause);
        assertEquals(2, list.size());
        assertEquals(new Long(21), list.get(0).getLongValue());
        assertEquals(new Long(22), list.get(1).getLongValue());
    }

    @Test
    public void testGetValuesAnonymousNoPermissions() {
        final ProjectsLeadByUserFunction projectsLeadByUserFunction = new ProjectsLeadByUserFunction(permissionManager, projectManager, userUtil);
        projectsLeadByUserFunction.init(create("projectsLeadByUser", true));

        final ApplicationUser bill = new MockApplicationUser("bill");
        when(userUtil.getUserByName("bill")).thenReturn(bill);
        when(projectManager.getProjectsLeadBy(bill)).thenReturn(projectsList2);
        for (final Project project : projectsList2) {
            when(permissionManager.hasPermission(BROWSE_PROJECTS, project, null)).thenReturn(false);
        }

        List<QueryLiteral> list = projectsLeadByUserFunction.getValues(new QueryCreationContextImpl((ApplicationUser) null), new FunctionOperand("projectsLeadByUser"), terminalClause);
        assertTrue(list.isEmpty());

        list = projectsLeadByUserFunction.getValues(new QueryCreationContextImpl((ApplicationUser) null), new FunctionOperand("projectsLeadByUser", "bill"), terminalClause);
        assertTrue(list.isEmpty());
    }

    @Test
    public void testGetMinimumNumberOfExpectedArguments() throws Exception {
        final ProjectsLeadByUserFunction projectsLeadByUserFunction = new ProjectsLeadByUserFunction(permissionManager, projectManager, userUtil);
        projectsLeadByUserFunction.init(MockJqlFunctionModuleDescriptor.create("projectsLeadByUser", true));

        assertEquals(0, projectsLeadByUserFunction.getMinimumNumberOfExpectedArguments());
    }
}
