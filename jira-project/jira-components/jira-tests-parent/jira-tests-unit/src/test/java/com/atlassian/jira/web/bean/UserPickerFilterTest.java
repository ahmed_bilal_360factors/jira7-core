package com.atlassian.jira.web.bean;

import com.atlassian.jira.issue.comparator.UserNameComparator;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.google.common.collect.ImmutableList;
import org.junit.Test;

import java.util.List;
import java.util.Locale;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;

/**
 * @since v6.2
 */
public class UserPickerFilterTest {
    @SuppressWarnings("unchecked")
    @Test
    public void testIntersectLists() throws Exception {
        final List<TestCase<List<ApplicationUser>[], List<ApplicationUser>>> testCases = ImmutableList.<TestCase<List<ApplicationUser>[], List<ApplicationUser>>>builder()
                .add(new TestCase<List<ApplicationUser>[], List<ApplicationUser>>(
                        "null lists",
                        new List[]{null, null},
                        ImmutableList.<ApplicationUser>of()
                ))
                .add(new TestCase<List<ApplicationUser>[], List<ApplicationUser>>(
                        "null list1",
                        new List[]{null, ImmutableList.of(new MockApplicationUser("user1"))},
                        ImmutableList.<ApplicationUser>of()
                ))
                .add(new TestCase<List<ApplicationUser>[], List<ApplicationUser>>(
                        "null list2",
                        new List[]{ImmutableList.of(new MockApplicationUser("user1")), null},
                        ImmutableList.<ApplicationUser>of()
                ))
                .add(new TestCase<List<ApplicationUser>[], List<ApplicationUser>>(
                        "empty list1",
                        new List[]{ImmutableList.of(), ImmutableList.of(new MockApplicationUser("user1"))},
                        ImmutableList.<ApplicationUser>of()
                ))
                .add(new TestCase<List<ApplicationUser>[], List<ApplicationUser>>(
                        "empty list2",
                        new List[]{ImmutableList.of(new MockApplicationUser("user1")), ImmutableList.of()},
                        ImmutableList.<ApplicationUser>of()
                ))
                .add(new TestCase<List<ApplicationUser>[], List<ApplicationUser>>(
                        "empty list2",
                        new List[]{ImmutableList.of(new MockApplicationUser("user1")), ImmutableList.of()},
                        ImmutableList.<ApplicationUser>of()
                ))
                .add(new TestCase<List<ApplicationUser>[], List<ApplicationUser>>(
                        "no intersection",
                        new List[]{ImmutableList.of(new MockApplicationUser("user1")), ImmutableList.of(new MockApplicationUser("user2"))},
                        ImmutableList.<ApplicationUser>of()
                ))
                .add(new TestCase<List<ApplicationUser>[], List<ApplicationUser>>(
                        "same list one item",
                        new List[]{ImmutableList.of(new MockApplicationUser("user1")), ImmutableList.of(new MockApplicationUser("user1"))},
                        ImmutableList.<ApplicationUser>of(new MockApplicationUser("user1"))
                ))
                .add(new TestCase<List<ApplicationUser>[], List<ApplicationUser>>(
                        "same list two items",
                        new List[]{ImmutableList.of(
                                new MockApplicationUser("user1"),
                                new MockApplicationUser("user2")
                        ), ImmutableList.of(
                                new MockApplicationUser("user1"),
                                new MockApplicationUser("user2")
                        )},
                        ImmutableList.<ApplicationUser>of(new MockApplicationUser("user1"), new MockApplicationUser("user2"))
                ))
                .add(new TestCase<List<ApplicationUser>[], List<ApplicationUser>>(
                        "diff list one item diff beginning",
                        new List[]{ImmutableList.of(
                                new MockApplicationUser("user1"),
                                new MockApplicationUser("user2")
                        ), ImmutableList.of(
                                new MockApplicationUser("user2")
                        )},
                        ImmutableList.<ApplicationUser>of(new MockApplicationUser("user2"))
                ))
                .add(new TestCase<List<ApplicationUser>[], List<ApplicationUser>>(
                        "diff list one item diff end",
                        new List[]{ImmutableList.of(
                                new MockApplicationUser("user2")
                        ), ImmutableList.of(
                                new MockApplicationUser("user2"),
                                new MockApplicationUser("user3")
                        )},
                        ImmutableList.<ApplicationUser>of(new MockApplicationUser("user2"))
                ))
                .add(new TestCase<List<ApplicationUser>[], List<ApplicationUser>>(
                        "diff list one item diff beginning and end",
                        new List[]{ImmutableList.of(
                                new MockApplicationUser("user1"),
                                new MockApplicationUser("user2")
                        ), ImmutableList.of(
                                new MockApplicationUser("user2"),
                                new MockApplicationUser("user3")
                        )},
                        ImmutableList.<ApplicationUser>of(new MockApplicationUser("user2"))
                ))
                .add(new TestCase<List<ApplicationUser>[], List<ApplicationUser>>(
                        "diff list two items diff middle",
                        new List[]{ImmutableList.of(
                                new MockApplicationUser("user1"),
                                new MockApplicationUser("user2"),
                                new MockApplicationUser("user3")
                        ), ImmutableList.of(
                                new MockApplicationUser("user1"),
                                new MockApplicationUser("user3"),
                                new MockApplicationUser("user4")
                        )},
                        ImmutableList.<ApplicationUser>of(new MockApplicationUser("user1"), new MockApplicationUser("user3"))
                ))
                .add(new TestCase<List<ApplicationUser>[], List<ApplicationUser>>(
                        "diff list two items continuous same",
                        new List[]{ImmutableList.of(
                                new MockApplicationUser("user1"),
                                new MockApplicationUser("user2"),
                                new MockApplicationUser("user3"),
                                new MockApplicationUser("user4")
                        ), ImmutableList.of(
                                new MockApplicationUser("user1"),
                                new MockApplicationUser("user3"),
                                new MockApplicationUser("user4")
                        )},
                        ImmutableList.<ApplicationUser>of(new MockApplicationUser("user1"), new MockApplicationUser("user3"), new MockApplicationUser("user4"))
                ))
                .build();
        final UserNameComparator userNameComparator = new UserNameComparator(Locale.ENGLISH);
        UserPickerFilter filter = new UserPickerFilter(null, null, null, null, null);
        for (TestCase<List<ApplicationUser>[], List<ApplicationUser>> testCase : testCases) {
            final List<ApplicationUser> result = filter.intersectLists(userNameComparator, testCase.input[0], testCase.input[1]);
            assertThat(testCase.name + " size", result, hasSize(testCase.expected.size()));
            for (int i = 0; i < result.size(); i++) {
                assertThat(testCase.name + " item " + i, result.get(i), equalTo(testCase.expected.get(i)));
            }
        }
    }

    private static class TestCase<Input, Output> {
        String name;
        Input input;
        Output expected;

        TestCase(final String name, final Input input, final Output expected) {
            this.name = name;
            this.input = input;
            this.expected = expected;
        }

        @Override
        public String toString() {
            return "TestCase{name='" + name + "'}";
        }
    }
}
