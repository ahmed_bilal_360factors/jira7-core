package com.atlassian.jira.imports.project.handler;

import com.atlassian.jira.exception.ParseException;
import com.atlassian.jira.external.beans.ExternalAttachment;
import com.atlassian.jira.external.beans.ExternalChangeGroup;
import com.atlassian.jira.external.beans.ExternalComment;
import com.atlassian.jira.external.beans.ExternalProject;
import com.atlassian.jira.external.beans.ExternalProjectRoleActor;
import com.atlassian.jira.external.beans.ExternalVoter;
import com.atlassian.jira.external.beans.ExternalWatcher;
import com.atlassian.jira.external.beans.ExternalWorklog;
import com.atlassian.jira.imports.project.core.BackupProject;
import com.atlassian.jira.imports.project.core.BackupProjectImpl;
import com.atlassian.jira.imports.project.core.ProjectImportOptionsImpl;
import com.atlassian.jira.imports.project.mapper.UserMapper;
import com.atlassian.jira.imports.project.parser.AttachmentParser;
import com.atlassian.jira.imports.project.parser.ChangeGroupParser;
import com.atlassian.jira.imports.project.parser.CommentParser;
import com.atlassian.jira.imports.project.parser.ProjectRoleActorParser;
import com.atlassian.jira.imports.project.parser.UserAssociationParser;
import com.atlassian.jira.imports.project.parser.WorklogParser;
import com.atlassian.jira.issue.attachment.AttachmentStore;
import com.atlassian.jira.security.roles.ProjectRoleActor;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import org.hamcrest.collection.IsCollectionWithSize;
import org.hamcrest.collection.IsIterableContainingInOrder;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import java.util.Collection;
import java.util.Date;

import static com.atlassian.jira.imports.project.parser.AttachmentParser.ATTACHMENT_ENTITY_NAME;
import static com.atlassian.jira.imports.project.parser.ChangeGroupParser.CHANGE_GROUP_ENTITY_NAME;
import static com.atlassian.jira.imports.project.parser.CommentParser.COMMENT_ENTITY_NAME;
import static com.atlassian.jira.imports.project.parser.ProjectRoleActorParser.PROJECT_ROLE_ACTOR_ENTITY_NAME;
import static com.atlassian.jira.imports.project.parser.UserAssociationParser.USER_ASSOCIATION_ENTITY_NAME;
import static com.atlassian.jira.issue.worklog.DatabaseWorklogStore.WORKLOG_ENTITY;
import static com.google.common.collect.ImmutableList.of;
import static java.util.Collections.emptyList;
import static java.util.Collections.emptyMap;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyMap;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

/**
 * @since v3.13
 */
public class TestUserMapperHandler {
    @Mock
    AttachmentStore attachmentStore;

    ProjectImportOptionsImpl projectImportOptions;

    UserMapperHandler mapperHandler;

    UserMapper mapper;

    BackupProject backupProject;

    ExternalProject project;

    @Before
    public void setUp() {
        projectImportOptions = new ProjectImportOptionsImpl("/some/path", "/Some/path");

        mapper = new UserMapper(null);


        project = new ExternalProject();
        project.setId("1234");
        project.setLead("dude");

        backupProject = new BackupProjectImpl(project, emptyList(), emptyList(),
                emptyList(), ImmutableList.of(12345L), 0, ImmutableMap.of());

        mapperHandler = spy(new UserMapperHandler(projectImportOptions, backupProject, mapper, attachmentStore));
    }

    @Test
    public void testProjectLeadSetInEndDocument() throws Exception {
        projectImportOptions.setOverwriteProjectDetails(true);

        mapperHandler.endDocument();

        assertEquals(1, mapper.getRequiredOldIds().size());
        assertEquals("dude", mapper.getRequiredOldIds().iterator().next());
    }

    @Test
    public void testProjectLeadNotSetInEndDocument() throws Exception {
        projectImportOptions.setOverwriteProjectDetails(false);

        mapperHandler.endDocument();

        assertEquals(0, mapper.getRequiredOldIds().size());
    }

    @Test
    public void testMapperFlaggedByAttacher() throws ParseException {
        ExternalAttachment externalAttachment = new ExternalAttachment("1", "12345", "test.txt", new Date(), "dude");

        final AttachmentParser mockAttachmentParser = mock(AttachmentParser.class);
        when(mockAttachmentParser.parse(emptyMap())).thenReturn(externalAttachment);

        doReturn(mockAttachmentParser).when(mapperHandler).getAttachmentParser();

        mapperHandler.handleEntity(ATTACHMENT_ENTITY_NAME, emptyMap());

        assertEquals(1, mapper.getOptionalOldIds().size());
        assertEquals("dude", mapper.getOptionalOldIds().iterator().next());
    }

    @Test
    public void testMapperNotFlaggedByUnhandledAttacher() throws ParseException {
        ExternalAttachment externalAttachment = new ExternalAttachment("1", "2", "test.txt", new Date(), "dude");

        final AttachmentParser mockAttachmentParser = mock(AttachmentParser.class);
        when(mockAttachmentParser.parse(emptyMap())).thenReturn(externalAttachment);

        doReturn(mockAttachmentParser).when(mapperHandler).getAttachmentParser();

        mapperHandler.handleEntity(ATTACHMENT_ENTITY_NAME, emptyMap());

        assertEquals(0, mapper.getRequiredOldIds().size());
    }

    @Test
    public void testMapperFlaggedByVoter() throws ParseException {
        ExternalVoter externalVoter = new ExternalVoter();
        externalVoter.setIssueId("12345");
        externalVoter.setVoter("dude");

        UserAssociationParser mockUserAssociationParser = mock(UserAssociationParser.class);
        when(mockUserAssociationParser.parseVoter(anyMap())).thenReturn(externalVoter);
        when(mockUserAssociationParser.parseWatcher(anyMap())).thenReturn(null);

        final UserMapper mapper = new UserMapper(null);

        mapperHandler = spy(new UserMapperHandler(projectImportOptions, backupProject, mapper, attachmentStore));
        when(mapperHandler.getUserAssociationParser()).thenReturn(mockUserAssociationParser);

        mapperHandler.handleEntity(USER_ASSOCIATION_ENTITY_NAME, emptyMap());

        assertThat(mapper.getOptionalOldIds(), IsCollectionWithSize.hasSize(1));
        assertThat(mapper.getOptionalOldIds(), IsIterableContainingInOrder.contains("dude"));
    }

    // The voter is not relevant because the issue is not handled by the project
    @Test
    public void testMapperFlaggedNotFlaggedByUnhandledVoter() throws ParseException {
        mapper = mock(UserMapper.class);
        ExternalVoter externalVoter = new ExternalVoter();
        externalVoter.setIssueId("66666");
        externalVoter.setVoter("dude");

        UserAssociationParser mockUserAssociationParser = mock(UserAssociationParser.class);
        when(mockUserAssociationParser.parseVoter(anyMap())).thenReturn(externalVoter);
        when(mockUserAssociationParser.parseWatcher(anyMap())).thenReturn(null);
        when(mapperHandler.getUserAssociationParser()).thenReturn(mockUserAssociationParser);

        mapperHandler.handleEntity(USER_ASSOCIATION_ENTITY_NAME, emptyMap());
        verifyZeroInteractions(mapper);
    }

    @Test
    public void testMapperFlaggedByProjectRole() throws ParseException {
        ExternalProjectRoleActor externalProjectRoleActor = new ExternalProjectRoleActor("12", "1234", "4321", ProjectRoleActor.USER_ROLE_ACTOR_TYPE, "dude");

        final ProjectRoleActorParser mockProjectRoleActorParser = mock(ProjectRoleActorParser.class);
        when(mockProjectRoleActorParser.parse(null)).thenReturn(externalProjectRoleActor);
        when(mapperHandler.getProjectRoleActorParser()).thenReturn(mockProjectRoleActorParser);

        mapperHandler.handleEntity(PROJECT_ROLE_ACTOR_ENTITY_NAME, null);

        assertEquals(1, mapper.getOptionalOldIds().size());
        assertEquals("dude", mapper.getOptionalOldIds().iterator().next());
    }

    @Test
    public void testMapperNotFlaggedByUnhandledProjectRoleNotRightProject() throws ParseException {
        ExternalProjectRoleActor externalProjectRoleActor = new ExternalProjectRoleActor("12", null, "4321", ProjectRoleActor.USER_ROLE_ACTOR_TYPE, "dude");

        final ProjectRoleActorParser mockProjectRoleActorParser = mock(ProjectRoleActorParser.class);
        when(mockProjectRoleActorParser.parse(null)).thenReturn(externalProjectRoleActor);
        when(mapperHandler.getProjectRoleActorParser()).thenReturn(mockProjectRoleActorParser);

        mapperHandler.handleEntity(PROJECT_ROLE_ACTOR_ENTITY_NAME, null);

        assertEquals(0, mapper.getOptionalOldIds().size());
    }

    @Test
    public void testMapperNotFlaggedByUnhandledProjectRoleNotRightRoleType() throws ParseException {
        ExternalProjectRoleActor externalProjectRoleActor = new ExternalProjectRoleActor("12", "5555", "4321", ProjectRoleActor.GROUP_ROLE_ACTOR_TYPE, "dude");

        final ProjectRoleActorParser mockProjectRoleActorParser = mock(ProjectRoleActorParser.class);
        when(mockProjectRoleActorParser.parse(null)).thenReturn(externalProjectRoleActor);

        doReturn(mockProjectRoleActorParser).when(mapperHandler).getProjectRoleActorParser();

        mapperHandler.handleEntity(PROJECT_ROLE_ACTOR_ENTITY_NAME, null);

        assertEquals(0, mapper.getOptionalOldIds().size());
    }

    @Test
    public void testMapperFlaggedByWatcher() throws ParseException {
        ExternalWatcher externalWatcher = new ExternalWatcher();
        externalWatcher.setIssueId("12345");
        externalWatcher.setWatcher("dude");

        final UserAssociationParser mockUserAssociationParser = mock(UserAssociationParser.class);
        when(mockUserAssociationParser.parseVoter(anyMap())).thenReturn(null);
        when(mockUserAssociationParser.parseWatcher(anyMap())).thenReturn(externalWatcher);

        when(mapperHandler.getUserAssociationParser()).thenReturn(mockUserAssociationParser);

        mapperHandler.handleEntity(USER_ASSOCIATION_ENTITY_NAME, emptyMap());

        assertEquals(1, mapper.getOptionalOldIds().size());
        assertEquals("dude", mapper.getOptionalOldIds().iterator().next());
    }

    // The watcher is not relevant because the issue is not handled by the project
    @Test
    public void testMapperFlaggedNotFlaggedByUnhandledWatcher() throws ParseException {
        mapper = mock(UserMapper.class);

        ExternalWatcher externalWatcher = new ExternalWatcher();
        externalWatcher.setIssueId("66666");
        externalWatcher.setWatcher("dude");

        final UserAssociationParser mockUserAssociationParser = mock(UserAssociationParser.class);
        when(mockUserAssociationParser.parseVoter(anyMap())).thenReturn(null);
        when(mockUserAssociationParser.parseWatcher(anyMap())).thenReturn(externalWatcher);

        when(mapperHandler.getUserAssociationParser()).thenReturn(mockUserAssociationParser);

        mapperHandler.handleEntity(USER_ASSOCIATION_ENTITY_NAME, emptyMap());

        verifyZeroInteractions(mapper);
    }


    @Test
    public void testMapperFlaggedByComment() throws ParseException {
        ExternalComment externalComment = new ExternalComment();
        externalComment.setIssueId("12345");
        externalComment.setUpdateAuthor("dude");
        externalComment.setUsername("someauthor");

        final CommentParser mockCommentParser = mock(CommentParser.class);
        when(mockCommentParser.parse(anyMap())).thenReturn(externalComment);

        when(mapperHandler.getCommentParser()).thenReturn(mockCommentParser);

        mapperHandler.handleEntity(COMMENT_ENTITY_NAME, emptyMap());

        assertEquals(2, mapper.getOptionalOldIds().size());
        Collection expected = of("dude", "someauthor");
        assertTrue(mapper.getOptionalOldIds().containsAll(expected));
    }

    // The comment is not relevant because the issue is not handled by the project
    @Test
    public void testMapperFlaggedNotFlaggedByUnhandledComment() throws ParseException {
        mapper = mock(UserMapper.class);

        ExternalComment externalComment = new ExternalComment();
        externalComment.setIssueId("66666");
        externalComment.setUpdateAuthor("dude");
        externalComment.setUsername("someauthor");

        final CommentParser mockCommentParser = mock(CommentParser.class);
        when(mockCommentParser.parse(anyMap())).thenReturn(externalComment);

        when(mapperHandler.getCommentParser()).thenReturn(mockCommentParser);

        mapperHandler.handleEntity(COMMENT_ENTITY_NAME, emptyMap());
        verifyZeroInteractions(mapper);
    }

    @Test
    public void testMapperFlaggedByWorklog() throws ParseException {
        ExternalWorklog externalWorklog = new ExternalWorklog();
        externalWorklog.setIssueId("12345");
        externalWorklog.setUpdateAuthor("dude");
        externalWorklog.setAuthor("someauthor");

        final WorklogParser mockWorklogParser = mock(WorklogParser.class);
        when(mockWorklogParser.parse(anyMap())).thenReturn(externalWorklog);
        when(mapperHandler.getWorklogParser()).thenReturn(mockWorklogParser);

        mapperHandler.handleEntity(WORKLOG_ENTITY, emptyMap());

        assertEquals(2, mapper.getOptionalOldIds().size());
        assertTrue(mapper.getOptionalOldIds().containsAll(of("dude", "someauthor")));
    }

    // The worklog is not relevant because the issue is not handled by the project
    @Test
    public void testMapperFlaggedNotFlaggedByUnhandledWorklog() throws ParseException {
        mapper = mock(UserMapper.class);

        ExternalWorklog externalWorklog = new ExternalWorklog();
        externalWorklog.setIssueId("66666");
        externalWorklog.setUpdateAuthor("dude");
        externalWorklog.setAuthor("someauthor");

        final WorklogParser mockWorklogParser = mock(WorklogParser.class);
        when(mockWorklogParser.parse(anyMap())).thenReturn(externalWorklog);
        when(mapperHandler.getWorklogParser()).thenReturn(mockWorklogParser);

        mapperHandler.handleEntity(WORKLOG_ENTITY, emptyMap());
        verifyZeroInteractions(mapper);
    }

    @Test
    public void testMapperFlaggedByChangeGroup() throws ParseException {
        ExternalChangeGroup externalChangeGroup = new ExternalChangeGroup();
        externalChangeGroup.setIssueId("12345");
        externalChangeGroup.setAuthor("dude");

        final ChangeGroupParser mockChangeGroupParser = mock(ChangeGroupParser.class);
        when(mockChangeGroupParser.parse(anyMap())).thenReturn(externalChangeGroup);
        when(mapperHandler.getChangeGroupParser()).thenReturn(mockChangeGroupParser);

        mapperHandler.handleEntity(CHANGE_GROUP_ENTITY_NAME, emptyMap());

        assertEquals(1, mapper.getOptionalOldIds().size());
        assertEquals("dude", mapper.getOptionalOldIds().iterator().next());

    }

    // The changeGroup is not relevant because the issue is not handled by the project
    @Test
    public void testMapperFlaggedNotFlaggedByUnhandledChangeGroup() throws ParseException {
        mapper = mock(UserMapper.class);

        ExternalChangeGroup externalChangegroup = new ExternalChangeGroup();
        externalChangegroup.setIssueId("66666");
        externalChangegroup.setAuthor("dude");

        final ChangeGroupParser mockChangeGroupParser = mock(ChangeGroupParser.class);


        when(mockChangeGroupParser.parse(anyMap())).thenReturn(externalChangegroup);
        when(mapperHandler.getChangeGroupParser()).thenReturn(mockChangeGroupParser);

        mapperHandler.handleEntity(CHANGE_GROUP_ENTITY_NAME, emptyMap());
        verifyZeroInteractions(mapper);
    }

    @Test
    public void testMapperFlaggedNotFlaggedByUnhandledEntity() throws ParseException {
        mapper = mock(UserMapper.class);

        mapperHandler.handleEntity("SomeEntity", emptyMap());

        verifyZeroInteractions(mapper);
    }

}
