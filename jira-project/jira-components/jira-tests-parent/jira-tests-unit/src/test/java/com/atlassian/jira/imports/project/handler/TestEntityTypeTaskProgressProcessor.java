package com.atlassian.jira.imports.project.handler;

import com.atlassian.jira.imports.project.taskprogress.EntityTypeTaskProgressProcessor;
import com.atlassian.jira.junit.rules.InitMockitoMocks;
import com.atlassian.jira.mock.i18n.MockI18nHelper;
import com.atlassian.jira.task.TaskProgressSink;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;

import static org.mockito.Mockito.verify;

/**
 * @since v3.13
 */
public class TestEntityTypeTaskProgressProcessor {
    private final MockI18nHelper i18nHelper = new MockI18nHelper();

    @Rule
    public InitMockitoMocks initMocks = new InitMockitoMocks(this);

    @Mock
    public TaskProgressSink taskProgressSink;

    @Test
    public void testNullTaskProgressSink() {
        EntityTypeTaskProgressProcessor entityTypeTaskProgressProcessor = new EntityTypeTaskProgressProcessor(20, null, i18nHelper);
        entityTypeTaskProgressProcessor.processTaskProgress("IssueTypes", 5, 40, 8);
        // That's all we do - just want to prove it doesn't throw NPE
    }

    @Test
    public void testProcessTaskProgress0PercentDone() {
        EntityTypeTaskProgressProcessor entityTypeTaskProgressProcessor = new EntityTypeTaskProgressProcessor(20, taskProgressSink, i18nHelper);
        entityTypeTaskProgressProcessor.processTaskProgress("IssueType", 1, 8, 8);

        // We are up to entity Type 1, which means we have completed 0/20 = 0%
        verify(taskProgressSink).makeProgress(0, "admin.message.task.progress.processing [IssueType]", "8");
    }

    @Test
    public void testProcessTaskProgress20PercentDone() {
        EntityTypeTaskProgressProcessor entityTypeTaskProgressProcessor = new EntityTypeTaskProgressProcessor(20, taskProgressSink, i18nHelper);
        entityTypeTaskProgressProcessor.processTaskProgress("IssueType", 5, 40, 8);

        // We are up to entity Type 5, which means we have completed 4/20 = 20%
        verify(taskProgressSink).makeProgress(20, "admin.message.task.progress.processing [IssueType]", "8");
    }

    @Test
    public void testProcessTaskProgress95PercentDone() {
        EntityTypeTaskProgressProcessor entityTypeTaskProgressProcessor = new EntityTypeTaskProgressProcessor(20, taskProgressSink, i18nHelper);
        entityTypeTaskProgressProcessor.processTaskProgress("IssueType", 20, 40, 8);

        // We are up to entity Type 20, which means we have completed 19/20 = 95%
        verify(taskProgressSink).makeProgress(95, "admin.message.task.progress.processing [IssueType]", "8");
    }
}
