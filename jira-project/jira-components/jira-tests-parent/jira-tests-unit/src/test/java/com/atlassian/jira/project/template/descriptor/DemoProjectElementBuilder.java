package com.atlassian.jira.project.template.descriptor;

import org.apache.commons.io.IOUtils;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

public class DemoProjectElementBuilder {
    private Element xmlElement;

    private DemoProjectElementBuilder(Element xmlElement) {
        this.xmlElement = xmlElement;
    }

    public static DemoProjectElementBuilder fromReference(String reference) {
        final SAXReader saxReader = new SAXReader();
        try {
            final Document document = saxReader.read(IOUtils.toInputStream(reference));
            return new DemoProjectElementBuilder(document.getRootElement());

        } catch (DocumentException e) {
            throw new RuntimeException(e);
        }
    }

    public DemoProjectElementBuilder weight(String weight){
        xmlElement.addAttribute("weight", weight);
        return this;
    }

    public DemoProjectElementBuilder longDescriptionKey(String key){
        return updateOrRemoveWhenNull("longDescription", "key", key);
    }

    public DemoProjectElementBuilder projectTemplateKey(String key){
        return updateOrRemoveWhenNull("projectTemplate", "key", key);
    }

    public DemoProjectElementBuilder projectTypeKey(String key){
        return updateOrRemoveWhenNull("projectType", "key", key);
    }

    public DemoProjectElementBuilder iconLocation(String location){
        return updateOrRemoveWhenNull("icon", "location", location);
    }

    public DemoProjectElementBuilder backgroundIconLocation(String location){
        return updateOrRemoveWhenNull("backgroundIcon", "location", location);
    }

    public DemoProjectElementBuilder importFileLocation(String location){
        return updateOrRemoveWhenNull("importFile", "location", location);
    }

    private DemoProjectElementBuilder updateOrRemoveWhenNull(String elementName, String attributeName, String value){
        final Element element = xmlElement.element(elementName);
        if(value == null){
            xmlElement.remove(element);
        } else {
            element.addAttribute(attributeName, value);
        }
        return this;
    }

    public Element getXmlElement() {
        return xmlElement.createCopy();
    }

}
