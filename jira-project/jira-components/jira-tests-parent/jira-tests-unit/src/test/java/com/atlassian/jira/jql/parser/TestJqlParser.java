package com.atlassian.jira.jql.parser;

import com.atlassian.fugue.Option;
import com.atlassian.jira.jql.util.JqlStringSupportImpl;
import com.atlassian.jira.util.collect.CollectionBuilder;
import com.atlassian.query.Query;
import com.atlassian.query.clause.AndClause;
import com.atlassian.query.clause.Clause;
import com.atlassian.query.clause.NotClause;
import com.atlassian.query.clause.OrClause;
import com.atlassian.query.clause.Property;
import com.atlassian.query.clause.TerminalClauseImpl;
import com.atlassian.query.clause.WasClauseImpl;
import com.atlassian.query.operand.EmptyOperand;
import com.atlassian.query.operand.FunctionOperand;
import com.atlassian.query.operand.MultiValueOperand;
import com.atlassian.query.operand.Operand;
import com.atlassian.query.operand.SingleValueOperand;
import com.atlassian.query.operator.Operator;
import com.atlassian.query.order.OrderBy;
import com.atlassian.query.order.OrderByImpl;
import com.atlassian.query.order.SearchSort;
import com.atlassian.query.order.SortOrder;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static com.atlassian.jira.jql.parser.JqlQueryParserConstants.ILLEGAL_CHARS_STRING;
import static com.atlassian.query.operator.Operator.EQUALS;
import static com.atlassian.query.operator.Operator.GREATER_THAN;
import static com.atlassian.query.operator.Operator.GREATER_THAN_EQUALS;
import static com.atlassian.query.operator.Operator.IN;
import static com.atlassian.query.operator.Operator.IS;
import static com.atlassian.query.operator.Operator.IS_NOT;
import static com.atlassian.query.operator.Operator.LESS_THAN;
import static com.atlassian.query.operator.Operator.LESS_THAN_EQUALS;
import static com.atlassian.query.operator.Operator.LIKE;
import static com.atlassian.query.operator.Operator.NOT_EQUALS;
import static com.atlassian.query.operator.Operator.NOT_IN;
import static com.atlassian.query.operator.Operator.NOT_LIKE;
import static org.junit.Assert.assertEquals;

/**
 * @since v4.0
 */
@RunWith(Parameterized.class)
public class TestJqlParser {
    @Parameters(name = "{0}")
    public static Collection<Object[]> data() {
        final DataBuilder data = new DataBuilder();

        final BigInteger tooBigNumber = BigInteger.valueOf(Long.MAX_VALUE).add(BigInteger.TEN);

        //Some tests to make sure that the white space is ignored during parsing.
        {
            final TerminalClauseImpl expectedClause = new TerminalClauseImpl("priority", EQUALS, "qwerty");
            data.add("priority = \"qwerty\"", expectedClause);
            data.add("priority=\"qwerty\"", expectedClause);
            data.add("priority=qwerty", expectedClause);
            data.add("  priority=qwerty  ", expectedClause);
            data.add("priority=     qwerty order      by priority, other", expectedClause, new OrderByImpl(new SearchSort("priority"), new SearchSort("other")));
        }

        //Checking minus after removing it from the reserved characters list.
        data.add("key = one-1", new TerminalClauseImpl("key", EQUALS, "one-1"));
        data.add("key in (one-1, -1)", new TerminalClauseImpl("key", IN, new MultiValueOperand(new SingleValueOperand("one-1"), new SingleValueOperand(-1L))));
        data.add("key in (one-1, 1-1)", new TerminalClauseImpl("key", IN, new MultiValueOperand(new SingleValueOperand("one-1"), new SingleValueOperand("1-1"))));
        data.add("-78a = a", new TerminalClauseImpl("-78a", EQUALS, new SingleValueOperand("a")));
        data.add("numberfield >= -29202", new TerminalClauseImpl("numberfield", GREATER_THAN_EQUALS, -29202));
        data.add("numberfield >= -29202-", new TerminalClauseImpl("numberfield", GREATER_THAN_EQUALS, "-29202-"));
        data.add("numberfield >= w-88 ", new TerminalClauseImpl("numberfield", GREATER_THAN_EQUALS, "w-88"));

        //Test to ensure that newline is accepted.
        data.add("newline = \"hello\nworld\"", new TerminalClauseImpl("newline", EQUALS, "hello\nworld"));
        data.add("newline = \"hello\\nworld\"", new TerminalClauseImpl("newline", EQUALS, "hello\nworld"));
        data.add("newline = 'hello\r'", new TerminalClauseImpl("newline", EQUALS, "hello\r"));
        data.add("newline = 'hello\\r'", new TerminalClauseImpl("newline", EQUALS, "hello\r"));
        data.add("newline = '\r'", new TerminalClauseImpl("newline", EQUALS, "\r"));
        data.add("newline = '\\r'", new TerminalClauseImpl("newline", EQUALS, "\r"));
        data.add("'new\nline' = 'b'", new TerminalClauseImpl("new\nline", EQUALS, "b"));
        data.add("'new\\nline' = 'b'", new TerminalClauseImpl("new\nline", EQUALS, "b"));
        data.add("'newline' = 'fun\rc'()", new TerminalClauseImpl("newline", EQUALS, new FunctionOperand("fun\rc")));
        data.add("'newline' = 'fun\\rc'()", new TerminalClauseImpl("newline", EQUALS, new FunctionOperand("fun\rc")));

        //Some tests for the other operators.
        data.add("coolness >= awesome", new TerminalClauseImpl("coolness", GREATER_THAN_EQUALS, "awesome"));
        data.add("coolness > awesome", new TerminalClauseImpl("coolness", GREATER_THAN, "awesome"));
        data.add("coolness < awesome", new TerminalClauseImpl("coolness", LESS_THAN, "awesome"));
        data.add("coolness <= awesome", new TerminalClauseImpl("coolness", LESS_THAN_EQUALS, "awesome"));
        data.add("coolness        !=       awesome order     by     coolness desc", new TerminalClauseImpl("coolness", NOT_EQUALS, "awesome"), new OrderByImpl(new SearchSort("coolness", SortOrder.DESC)));

        //Some tests for the in operator.
        data.add("language in (java, c, \"python2\")", new TerminalClauseImpl("language", IN, new MultiValueOperand("java", "c", "python2")));
        data.add("languagein   IN    (   java, c     , \"python2\")", new TerminalClauseImpl("languagein", IN, new MultiValueOperand("java", "c", "python2")));
        data.add("inlanguage in (java, c, \"python2\")", new TerminalClauseImpl("inlanguage", IN, new MultiValueOperand("java", "c", "python2")));
        data.add("pri in (java,c,\"python2\")", new TerminalClauseImpl("pri", IN, new MultiValueOperand("java", "c", "python2")));
        data.add("pri in(java)", new TerminalClauseImpl("pri", IN, new MultiValueOperand("java")));
        data.add("pri In(java)", new TerminalClauseImpl("pri", IN, new MultiValueOperand("java")));
        data.add("pri iN(java)", new TerminalClauseImpl("pri", IN, new MultiValueOperand("java")));

        //Some tests for the NOT in operator.
        data.add("language not in (java, c, \"python2\")", new TerminalClauseImpl("language", NOT_IN, new MultiValueOperand("java", "c", "python2")));
        data.add("languagein  NOT   IN    (   java, c     , \"python2\")", new TerminalClauseImpl("languagein", NOT_IN, new MultiValueOperand("java", "c", "python2")));
        data.add("inlanguage not in (java, c, \"python2\")", new TerminalClauseImpl("inlanguage", NOT_IN, new MultiValueOperand("java", "c", "python2")));
        data.add("pri NOT in (java,c,\"python2\")", new TerminalClauseImpl("pri", NOT_IN, new MultiValueOperand("java", "c", "python2")));
        data.add("pri not in(java)", new TerminalClauseImpl("pri", NOT_IN, new MultiValueOperand("java")));
        data.add("pri NoT In(java)", new TerminalClauseImpl("pri", NOT_IN, new MultiValueOperand("java")));
        data.add("pri nOT iN(java)", new TerminalClauseImpl("pri", NOT_IN, new MultiValueOperand("java")));

        // Some tests for the LIKE operator.
        data.add("pri ~ stuff", new TerminalClauseImpl("pri", LIKE, new SingleValueOperand("stuff")));
        data.add("pri~stuff", new TerminalClauseImpl("pri", LIKE, new SingleValueOperand("stuff")));
        data.add("pri ~ 12", new TerminalClauseImpl("pri", LIKE, new SingleValueOperand(12L)));
        data.add("pri~12", new TerminalClauseImpl("pri", LIKE, new SingleValueOperand(12L)));
        data.add("pri ~ (\"stuff\", 12)", new TerminalClauseImpl("pri", LIKE, new MultiValueOperand(CollectionBuilder.newBuilder(new SingleValueOperand("stuff"), new SingleValueOperand(12L)).asList())));

        // Some tests for the NOT_LIKE operator.
        data.add("pri !~ stuff", new TerminalClauseImpl("pri", NOT_LIKE, new SingleValueOperand("stuff")));
        data.add("pri!~stuff", new TerminalClauseImpl("pri", NOT_LIKE, new SingleValueOperand("stuff")));
        data.add("pri !~ 12", new TerminalClauseImpl("pri", NOT_LIKE, new SingleValueOperand(12L)));
        data.add("pri!~12", new TerminalClauseImpl("pri", NOT_LIKE, new SingleValueOperand(12L)));
        data.add("pri !~ (\"stuff\", 12)", new TerminalClauseImpl("pri", NOT_LIKE, new MultiValueOperand(CollectionBuilder.newBuilder(new SingleValueOperand("stuff"), new SingleValueOperand(12L)).asList())));

        // Some tests for the IS operator
        data.add("pri IS stuff", new TerminalClauseImpl("pri", IS, new SingleValueOperand("stuff")));
        data.add("pri is stuff", new TerminalClauseImpl("pri", IS, new SingleValueOperand("stuff")));
        data.add("pri IS EMPTY", new TerminalClauseImpl("pri", IS, new EmptyOperand()));

        // Some tests for the IS_NOT operator
        data.add("pri IS NOT stuff", new TerminalClauseImpl("pri", IS_NOT, new SingleValueOperand("stuff")));
        data.add("pri IS not stuff", new TerminalClauseImpl("pri", IS_NOT, new SingleValueOperand("stuff")));
        data.add("pri is Not stuff", new TerminalClauseImpl("pri", IS_NOT, new SingleValueOperand("stuff")));
        data.add("pri is not stuff", new TerminalClauseImpl("pri", IS_NOT, new SingleValueOperand("stuff")));


        //Test for the nested behaviour of in clause.
        {
            final List<Operand> nested = Arrays.asList(new MultiValueOperand("java"), new SingleValueOperand("duke"));
            data.add("pri iN((java), duke)", new TerminalClauseImpl("pri", IN, new MultiValueOperand(nested)));
        }

        //Test to make sure that numbers are returned correctly.
        data.add("priority = 12345", new TerminalClauseImpl("priority", EQUALS, 12345L));
        data.add("priority = -12345", new TerminalClauseImpl("priority", EQUALS, -12345L));
        data.add("priority = \"12a345\"", new TerminalClauseImpl("priority", EQUALS, "12a345"));
        data.add("priority = 12345a", new TerminalClauseImpl("priority", EQUALS, "12345a"));

        //Test custom field labels
        data.add("cf[12345] = 12345a", new TerminalClauseImpl("cf[12345]", EQUALS, "12345a"));
        data.add("Cf  [ 0005 ] = x", new TerminalClauseImpl("cf[5]", EQUALS, "x"));

        //Test properties
        data.add("issue.property[x] = x", new TerminalClauseImpl("issue.property", EQUALS, new SingleValueOperand("x"), property("x", null)));
        data.add("issue.property[issue.status] = resolved", new TerminalClauseImpl("issue.property", EQUALS, new SingleValueOperand("resolved"), property("issue.status", null)));
        data.add("ISSUE.property[\"issue.status\"] = resolved", new TerminalClauseImpl("ISSUE.property", EQUALS, new SingleValueOperand("resolved"), property("issue.status", null)));
        data.add("issue.property[\'issue.status\'] = resolved", new TerminalClauseImpl("issue.property", EQUALS, new SingleValueOperand("resolved"), property("issue.status", null)));
        data.add("issue.property     [\'issue.status\'] = resolved", new TerminalClauseImpl("issue.property", EQUALS, new SingleValueOperand("resolved"), property("issue.status", null)));
        data.add("issue.property[\'1@4s\'] = resolved", new TerminalClauseImpl("issue.property", EQUALS, new SingleValueOperand("resolved"), property("1@4s", null)));
        data.add("issue.property[1234] = resolved", new TerminalClauseImpl("issue.property", EQUALS, new SingleValueOperand("resolved"), property("1234", null)));
        data.add("issue.property[-1234] = resolved", new TerminalClauseImpl("issue.property", EQUALS, new SingleValueOperand("resolved"), property("-1234", null)));
        data.add("issue.property.x = y", new TerminalClauseImpl("issue.property.x", EQUALS, new SingleValueOperand("y")));
        // reserved characters inside
        data.add("issue.ProPeRty[\'-@.,@\'] = resolved", new TerminalClauseImpl("issue.ProPeRty", EQUALS, new SingleValueOperand("resolved"), property("-@.,@", null)));
        data.add("comment.prop[author] = filip", new TerminalClauseImpl("comment.prop", EQUALS, new SingleValueOperand("filip"), property("author", null)));
        data.add("comment.prop[author].author.name = filip", new TerminalClauseImpl("comment.prop", EQUALS, new SingleValueOperand("filip"), property("author", "author.name")));
        data.add("comment.prop[author].Author < filip", new TerminalClauseImpl("comment.prop", LESS_THAN, new SingleValueOperand("filip"), property("author", "Author")));

        //Make sure that a quoted number is actually returned as a string.
        data.add("priority = \"12345\"", new TerminalClauseImpl("priority", EQUALS, "12345"));

        //An invalid number should be returned as a string.
        data.add("priority=\"12a345\"", new TerminalClauseImpl("priority", EQUALS, "12a345"));
        //Should accept dot separated values as rhv
        data.add("version= 1.2.3", new TerminalClauseImpl("version", EQUALS, "1.2.3"));

        //Some tests to check the empty operand
        data.add("testfield = EMPTY", new TerminalClauseImpl("testfield", EQUALS, new EmptyOperand()));
        data.add("testfield = empty", new TerminalClauseImpl("testfield", EQUALS, new EmptyOperand()));
        data.add("testfield = NULL", new TerminalClauseImpl("testfield", EQUALS, new EmptyOperand()));
        data.add("testfield = null", new TerminalClauseImpl("testfield", EQUALS, new EmptyOperand()));
        data.add("testfield = \"null\"", new TerminalClauseImpl("testfield", EQUALS, "null"));
        data.add("testfield = \"NULL\"", new TerminalClauseImpl("testfield", EQUALS, "NULL"));
        data.add("testfield = \"EMPTY\"", new TerminalClauseImpl("testfield", EQUALS, "EMPTY"));
        data.add("testfield = \"empty\"", new TerminalClauseImpl("testfield", EQUALS, "empty"));

        // tests for quoted strings with characters that must be quoted
        data.add("priority = \"a big string ~ != foo and priority = haha \"", new TerminalClauseImpl("priority", EQUALS, "a big string ~ != foo and priority = haha "));
        data.add("priority = \"\"", new TerminalClauseImpl("priority", EQUALS, ""));

        //test for strange field names.
        data.add("prior\\'ty = testvalue", new TerminalClauseImpl("prior'ty", EQUALS, "testvalue"));
        data.add("priority\\ ty=testvalue", new TerminalClauseImpl("priority ty", EQUALS, "testvalue"));
        data.add("priority\u2ee5 > 6", new TerminalClauseImpl("priority\u2ee5", GREATER_THAN, 6));
        data.add("priori\\nty\\u2ee5 > 6", new TerminalClauseImpl("priori\nty\u2ee5", GREATER_THAN, 6));
        data.add("\"this is a strange field \" = google", new TerminalClauseImpl("this is a strange field ", EQUALS, "google"));
        data.add("\"don't\" = 'true'", new TerminalClauseImpl("don't", EQUALS, "true"));
        data.add("\"don\\\"t\" = 'false'", new TerminalClauseImpl("don\"t", EQUALS, "false"));
        data.add("\"don't\" = 'false'", new TerminalClauseImpl("don't", EQUALS, "false"));
        data.add("'don\\'t' = 'false' order by 'don\\'t' DEsc", new TerminalClauseImpl("don't", EQUALS, "false"), new OrderByImpl(new SearchSort("don't", SortOrder.DESC)));
        data.add("'don\"t' = 'false'", new TerminalClauseImpl("don\"t", EQUALS, "false"));
        data.add("'cf[1220]' = abc", new TerminalClauseImpl("cf[1220]", EQUALS, "abc"));
        data.add("'cf' = abc", new TerminalClauseImpl("cf", EQUALS, "abc"));
        data.add("10245948 = abc      order          by 10245948", new TerminalClauseImpl("10245948", EQUALS, "abc"), new OrderByImpl(new SearchSort("10245948")));
        data.add("-10245948 = abc", new TerminalClauseImpl("-10245948", EQUALS, "abc"));
        data.add("new\\nline = abc", new TerminalClauseImpl("new\nline", EQUALS, "abc"));
        data.add("some\\u0082control = abc  order by some\\u0082control", new TerminalClauseImpl("some\u0082control", EQUALS, "abc"), new OrderByImpl(new SearchSort("some\u0082control")));

        //test for strange field values.
        data.add("b = ''", new TerminalClauseImpl("b", EQUALS, ""));
        data.add("b = \\ ", new TerminalClauseImpl("b", EQUALS, " "));
        data.add("b = don\\'t\\ stop\\ me\\ now", new TerminalClauseImpl("b", EQUALS, "don't stop me now"));
        data.add("b = \u2ee5", new TerminalClauseImpl("b", EQUALS, "\u2ee5"));
        data.add("b = \\u2EE5jkdfskjfd", new TerminalClauseImpl("b", EQUALS, "\u2ee5jkdfskjfd"));
        data.add("b not in 'jack says, \"Hello World!\"'", new TerminalClauseImpl("b", NOT_IN, "jack says, \"Hello World!\""));
        data.add("b not in 'jack says, \\'Hello World!\\''", new TerminalClauseImpl("b", NOT_IN, "jack says, 'Hello World!'"));
        data.add("b not in \"jack says, 'Hello World!'\"", new TerminalClauseImpl("b", NOT_IN, "jack says, 'Hello World!'"));
        data.add("b not in \"jack says, \\\"Hello World!'\\\"\"", new TerminalClauseImpl("b", NOT_IN, "jack says, \"Hello World!'\""));
        data.add("b not in \"jack says, \\tnothing\"", new TerminalClauseImpl("b", NOT_IN, "jack says, \tnothing"));
        data.add("bad ~ wt\\u007f", new TerminalClauseImpl("bad", LIKE, "wt\u007f"));

        //tests for escaping.
        data.add("priority = \"a \\n new \\r line\"", new TerminalClauseImpl("priority", EQUALS, "a \n new \r line"));
        data.add("priority = \"Tab:\\t NewLine:\\n Carrage Return:\\r\"", new TerminalClauseImpl("priority", EQUALS, "Tab:\t NewLine:\n Carrage Return:\r"));
        data.add("priority = \"Quote:\\\" Single:\\' Back Slash:\\\\ Space:\\ \"", new TerminalClauseImpl("priority", EQUALS, "Quote:\" Single:' Back Slash:\\ Space: "));
        data.add("priority = \"Unicode: \\ufeeF1 Unicode2: \\u6EEF\"", new TerminalClauseImpl("priority", EQUALS, "Unicode: \ufeef1 Unicode2: \u6eef"));
        data.add("priority = 'Escape\" don\\'t'", new TerminalClauseImpl("priority", EQUALS, "Escape\" don't"));
        data.add("priority = \"Escape' don\\\"t\"", new TerminalClauseImpl("priority", EQUALS, "Escape' don\"t"));

        //Some tests for function calls.
        data.add("priority = higherThan(Major)", new TerminalClauseImpl("priority", EQUALS, new FunctionOperand("higherThan", Collections.singletonList("Major"))));
        data.add("priority In     randomName(Major, Minor,      \"cool\", -65784)", new TerminalClauseImpl("priority", IN, new FunctionOperand("randomName", Arrays.asList("Major", "Minor", "cool", "-65784"))));
        data.add("priority    >=    randomName()", new TerminalClauseImpl("priority", GREATER_THAN_EQUALS, new FunctionOperand("randomName", Collections.<String>emptyList())));
        data.add(String.format("pri not in func(%d)", tooBigNumber), new TerminalClauseImpl("pri", NOT_IN, new FunctionOperand("func", Collections.singletonList(tooBigNumber.toString()))));
        data.add("pri not in fun(name\\u0082e)", new TerminalClauseImpl("pri", NOT_IN, new FunctionOperand("fun", "name\u0082e")));

        //test for strange function names.
        data.add("a = func\\'  ()", new TerminalClauseImpl("a", EQUALS, new FunctionOperand("func'")));
        data.add("a = fu\\\"nc\\'()", new TerminalClauseImpl("a", EQUALS, new FunctionOperand("fu\"nc'")));
        data.add("a=function\\ name(  )", new TerminalClauseImpl("a", EQUALS, new FunctionOperand("function name")));
        data.add("a = \u2ee5()", new TerminalClauseImpl("a", EQUALS, new FunctionOperand("\u2ee5")));
        data.add("a = somereallystrangestring\\u2ee5()", new TerminalClauseImpl("a", EQUALS, new FunctionOperand("somereallystrangestring\u2ee5")));
        data.add("version <= \"affected\\ versions\"(   )", new TerminalClauseImpl("version", LESS_THAN_EQUALS, new FunctionOperand("affected versions")));
        data.add("version <= \"affected\\ versio'ns\"(   )", new TerminalClauseImpl("version", LESS_THAN_EQUALS, new FunctionOperand("affected versio'ns")));
        data.add("version <= \"affected versio\\\"ns\"(   )", new TerminalClauseImpl("version", LESS_THAN_EQUALS, new FunctionOperand("affected versio\"ns")));
        data.add("version <= 'my messed up versio\\'ns'     (   )", new TerminalClauseImpl("version", LESS_THAN_EQUALS, new FunctionOperand("my messed up versio'ns")));
        data.add("version <= 'my m\\nessed up\\ versio\"ns'     (   )", new TerminalClauseImpl("version", LESS_THAN_EQUALS, new FunctionOperand("my m\nessed up versio\"ns")));
        data.add("version <= 4759879855`(   )", new TerminalClauseImpl("version", LESS_THAN_EQUALS, new FunctionOperand("4759879855`")));
        data.add("version <= 4759879(   )", new TerminalClauseImpl("version", LESS_THAN_EQUALS, new FunctionOperand("4759879")));
        data.add("version = badname\\u0091", new TerminalClauseImpl("version", EQUALS, "badname\u0091"));


        //test some of the string breaks
        data.add("a=b&c=d", new AndClause(new TerminalClauseImpl("a", EQUALS, "b"), new TerminalClauseImpl("c", EQUALS, "d")));
        data.add("a=b&&c=d", new AndClause(new TerminalClauseImpl("a", EQUALS, "b"), new TerminalClauseImpl("c", EQUALS, "d")));
        data.add("a=b|c=d", new OrClause(new TerminalClauseImpl("a", EQUALS, "b"), new TerminalClauseImpl("c", EQUALS, "d")));
        data.add("a=b||c=d", new OrClause(new TerminalClauseImpl("a", EQUALS, "b"), new TerminalClauseImpl("c", EQUALS, "d")));
        data.add("a<b", new TerminalClauseImpl("a", LESS_THAN, "b"));
        data.add("a>b", new TerminalClauseImpl("a", GREATER_THAN, "b"));
        data.add("a~b", new TerminalClauseImpl("a", LIKE, "b"));

        {
            //Check the and operator.
            final Clause priorityEqualsMajor = new TerminalClauseImpl("priority", EQUALS, "major");
            final Clause fooGtBarFunc = new TerminalClauseImpl("foo", GREATER_THAN, new FunctionOperand("bar"));
            data.add("priority = major and foo > bar()", new AndClause(priorityEqualsMajor, fooGtBarFunc));
            data.add("priority = majorand and foo>bar()", new AndClause(new TerminalClauseImpl("priority", EQUALS, "majorand"), fooGtBarFunc));
            data.add("priority = major and foo > bar()", new AndClause(priorityEqualsMajor, fooGtBarFunc));
            data.add("priority != major    and      foo >      bar()", new AndClause(new TerminalClauseImpl("priority", NOT_EQUALS, "major"), fooGtBarFunc));
            data.add("priority != major    &&      foo >      bar()", new AndClause(new TerminalClauseImpl("priority", NOT_EQUALS, "major"), fooGtBarFunc));
            data.add("priority != andmajor    &      foo >      bar()", new AndClause(new TerminalClauseImpl("priority", NOT_EQUALS, "andmajor"), fooGtBarFunc));
            data.add("priority != andmajor    and      foo >      bar() order by priority     DESC,      foo",
                    new AndClause(new TerminalClauseImpl("priority", NOT_EQUALS, "andmajor"), fooGtBarFunc),
                    new OrderByImpl(new SearchSort("priority", SortOrder.DESC), new SearchSort("foo")));

            //Check the or operator.
            final Clause priorityEqualsMinor = new TerminalClauseImpl("priority", EQUALS, "minor");
            data.add("priority = major or foo > bar()", new OrClause(priorityEqualsMajor, fooGtBarFunc));
            data.add("priority = major or foo > bar()", new OrClause(priorityEqualsMajor, fooGtBarFunc));
            data.add("priority = major or foo > bar() or priority = minor", new OrClause(priorityEqualsMajor, fooGtBarFunc, priorityEqualsMinor));
            data.add("priority = major || foo > bar() | priority = minor", new OrClause(priorityEqualsMajor, fooGtBarFunc, priorityEqualsMinor));
            data.add("priority = major or foo > bar() || priority = minor", new OrClause(priorityEqualsMajor, fooGtBarFunc, priorityEqualsMinor));

            //Checks for operator precedence for and and or.
            final Clause fooGtBarFunc123 = new TerminalClauseImpl("foo", GREATER_THAN, new FunctionOperand("bar", Arrays.asList("1", "2", "3")));
            final Clause priorityFooAnd = new AndClause(priorityEqualsMajor, fooGtBarFunc123);
            final Clause bazNotEqual1234 = new TerminalClauseImpl("baz", NOT_EQUALS, 1234L);
            final Clause priorityBazAnd = new AndClause(priorityEqualsMinor, bazNotEqual1234);
            final Clause outerOr = new OrClause(priorityFooAnd, priorityBazAnd);
            data.add("priority = major and foo > bar(1,2,3) | priority = minor and baz != 1234", outerOr);
            data.add("priority =     major AND foo > bar(1,2,3) oR priority = minor and baz != 1234", outerOr);
            data.add("priority=major and foo>bar(1,2,3)|| priority=minor  and  baz!=1234", outerOr);
            data.add("priority = major AND foo > bar(1,2,3) Or priority = minor AND baz != 1234", outerOr);

            //Another test for the and operator.
            data.add("priority = major && foo > bar(1,2,3) & priority = minor and baz != 1234", new AndClause(priorityEqualsMajor, fooGtBarFunc123, priorityEqualsMinor, bazNotEqual1234));

            // use parentheses to overthrow precedence
            data.add("priority = major and (foo > bar(1,2,3) | priority = minor) and baz != 1234", new AndClause(priorityEqualsMajor, new OrClause(fooGtBarFunc123, priorityEqualsMinor), bazNotEqual1234));

            //make sure the precedence still works with the brackets.
            data.add("priority = major or (foo > bar(1,2,3) or priority = minor) && baz != 1234", new OrClause(priorityEqualsMajor, new AndClause(new OrClause(fooGtBarFunc123, priorityEqualsMinor), bazNotEqual1234)));

            //test for the not operator.
            data.add("not priority = major or foo > bar() or priority = minor", new OrClause(new NotClause(priorityEqualsMajor), fooGtBarFunc, priorityEqualsMinor));
            data.add("not priority = major or foo > bar() AnD priority=\"minor\"", new OrClause(new NotClause(priorityEqualsMajor), new AndClause(fooGtBarFunc, priorityEqualsMinor)));
            data.add("not priority = major or not foo > bar() AnD priority=\"minor\"", new OrClause(new NotClause(priorityEqualsMajor), new AndClause(new NotClause(fooGtBarFunc), priorityEqualsMinor)));
            data.add("not (priority = major or not foo > bar()) AnD priority=\"minor\"", new AndClause(new NotClause(new OrClause(priorityEqualsMajor, new NotClause(fooGtBarFunc))), priorityEqualsMinor));

            //check the '!' operator.
            data.add("! (priority = major or ! foo > bar()) AnD priority=\"minor\"", new AndClause(new NotClause(new OrClause(priorityEqualsMajor, new NotClause(fooGtBarFunc))), priorityEqualsMinor));

            //test that a double not also works.
            data.add("not ! (! priority = major or     foo >bar()) &         priority=\"minor\"", new AndClause(new NotClause(new NotClause(new OrClause(new NotClause(priorityEqualsMajor), fooGtBarFunc))), priorityEqualsMinor));
        }

        //Tests to make sure illegal characters can be escaped.
        for (int i = 0; i < ILLEGAL_CHARS_STRING.length(); i++) {
            final char currentChar = ILLEGAL_CHARS_STRING.charAt(i);
            final String fieldName = String.format("test%cdfjd", currentChar);
            data.add(String.format("'%s' = 'good'", fieldName), new TerminalClauseImpl(fieldName, EQUALS, "good"));
        }

        //Make sure the reserved words can be escaped.
        for (final String reservedWord : JqlStringSupportImpl.RESERVED_WORDS) {
            data.add(String.format("priority NOT IN ('%s')", reservedWord), new TerminalClauseImpl("priority", NOT_IN, new MultiValueOperand(reservedWord)));
        }

        //We want to be able to add sort when there is no where clause.
        data.add("order by crap", null, new OrderByImpl(new SearchSort("crap")));
        data.add("order by crap  DESC", null, new OrderByImpl(new SearchSort("crap", SortOrder.DESC)));
        data.add("order by crap  ASC", null, new OrderByImpl(new SearchSort("crap", SortOrder.ASC)));
        data.add("order by cf[12345]  ASC", null, new OrderByImpl(new SearchSort("cf[12345]", SortOrder.ASC)));

        data.add("", null, null);
        data.add("       ", null, null);

        // was clause change history queries
        data.add("status was open", new WasClauseImpl("status", Operator.WAS, new SingleValueOperand("open"), null));

        return data.data();
    }

    private final String inputJql;
    private final Clause expectedClause;
    private final OrderBy orderBy;

    public TestJqlParser(final String inputJql, final Clause expectedClause, final OrderBy orderBy) {
        this.expectedClause = expectedClause;
        this.inputJql = inputJql;
        this.orderBy = orderBy;
    }

    @Test
    public void testParser() throws Throwable {
        final DefaultJqlQueryParser parser = new DefaultJqlQueryParser();
        final Query query = parser.parseQuery(inputJql);
        final Clause whereClause = query.getWhereClause();
        assertEquals(inputJql, query.getQueryString());
        assertEquals(expectedClause, whereClause);
        assertEquals(orderBy, query.getOrderByClause());
    }

    private static Option<Property> property(final String property, final String objectReferences) {
        final Splitter splitter = Splitter.on('.').omitEmptyStrings();
        final ArrayList<String> objReferences = StringUtils.isEmpty(objectReferences) ? Lists.<String>newArrayList() : Lists.newArrayList(splitter.split(objectReferences));
        return Option.some(new Property(Lists.newArrayList(splitter.split(property)), objReferences));
    }

    private static class DataBuilder {
        final List<Object[]> data = new ArrayList<>();


        private void add(final String jql, final Clause expectedClause) {
            add(jql, expectedClause, new OrderByImpl());
        }

        private void add(final String jql, final Clause expectedClause, final OrderBy orderBy) {
            data.add(new Object[]{jql, expectedClause, (orderBy == null) ? new OrderByImpl() : orderBy});
        }

        public List<Object[]> data() {
            return data;
        }
    }
}
