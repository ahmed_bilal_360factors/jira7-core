package com.atlassian.jira.user.util;

import com.atlassian.core.user.preferences.Preferences;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.sharing.SharedEntity.SharePermissions;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.preferences.PreferenceKeys;
import com.atlassian.jira.user.preferences.UserPreferencesManager;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

/**
 * Test for {@link UserSharingPreferencesUtilImpl}.
 *
 * @since v3.13
 */
public class TestUserSharingPreferencesUtilImpl {
    @Rule
    public final MockitoRule initMockito = MockitoJUnit.rule();

    private ApplicationUser user = new MockApplicationUser("testuser");
    @Mock
    private PermissionManager permissionManager;
    @Mock
    private Preferences preferences;
    @Mock
    private UserPreferencesManager userPreferencesManager;

    private UserSharingPreferencesUtil sharingPreferences;

    @Before
    public void setUp() {
        sharingPreferences = new UserSharingPreferencesUtilImpl(permissionManager, userPreferencesManager);
    }

    /**
     * Ensure that passing the anonymous user works.
     */
    @Test
    public void testGetDefaultSharePermissionsNullUser() {
        assertEquals(SharePermissions.PRIVATE, sharingPreferences.getDefaultSharePermissions(null));
    }

    /**
     * Ensure that a user without sharing permission is forced to save as private.
     */
    @Test
    public void testGetDefaultSharePermissionsNoSharingAllowed() {
        when(permissionManager.hasPermission(Permissions.CREATE_SHARED_OBJECTS, user)).thenReturn(false);

        final UserSharingPreferencesUtil sharingPreferences = new UserSharingPreferencesUtilImpl(permissionManager, userPreferencesManager);
        assertEquals(SharePermissions.PRIVATE, sharingPreferences.getDefaultSharePermissions(user));
    }

    /**
     * Ensure that a user with private setting returns private shares.
     */
    @Test
    public void testGetDefaultSharePermissionsDefaultPrivate() {
        when(permissionManager.hasPermission(Permissions.CREATE_SHARED_OBJECTS, user)).thenReturn(true);
        when(preferences.getBoolean(PreferenceKeys.USER_DEFAULT_SHARE_PRIVATE)).thenReturn(true);
        when(userPreferencesManager.getPreferences(user)).thenReturn(preferences);

        final UserSharingPreferencesUtil sharingPreferences = new UserSharingPreferencesUtilImpl(permissionManager, userPreferencesManager);
        assertEquals(SharePermissions.PRIVATE, sharingPreferences.getDefaultSharePermissions(user));
    }

    /**
     * Ensure that a user without private settings returns authenticated shares.
     */
    @Test
    public void testGetDefaultSharePermissionsDefaultPublic() {
        when(permissionManager.hasPermission(Permissions.CREATE_SHARED_OBJECTS, user)).thenReturn(true);
        when(preferences.getBoolean(PreferenceKeys.USER_DEFAULT_SHARE_PRIVATE)).thenReturn(false);
        when(userPreferencesManager.getPreferences(user)).thenReturn(preferences);

        final UserSharingPreferencesUtil sharingPreferences = new UserSharingPreferencesUtilImpl(permissionManager, userPreferencesManager);
        assertEquals(SharePermissions.AUTHENTICATED, sharingPreferences.getDefaultSharePermissions(user));
    }

}
