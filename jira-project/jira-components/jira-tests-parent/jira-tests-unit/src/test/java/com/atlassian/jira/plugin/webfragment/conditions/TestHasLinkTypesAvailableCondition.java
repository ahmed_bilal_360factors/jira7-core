package com.atlassian.jira.plugin.webfragment.conditions;

import com.atlassian.jira.issue.link.IssueLinkTypeManager;
import com.atlassian.jira.issue.link.MockIssueLinkType;
import com.google.common.collect.ImmutableList;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

public class TestHasLinkTypesAvailableCondition {
    @Rule
    public MockitoRule mockito = MockitoJUnit.rule();

    @Mock
    private IssueLinkTypeManager issueLinkTypeManager;

    @Test
    public void testTrue() {
        when(issueLinkTypeManager.getIssueLinkTypes()).thenReturn(ImmutableList.of(new MockIssueLinkType()));

        final HasLinkTypesAvailableCondition condition = new HasLinkTypesAvailableCondition(issueLinkTypeManager);

        assertTrue(condition.shouldDisplay(null, null));
    }

    @Test
    public void testFalseEmpty() {
        when(issueLinkTypeManager.getIssueLinkTypes()).thenReturn(ImmutableList.of());

        final HasLinkTypesAvailableCondition condition = new HasLinkTypesAvailableCondition(issueLinkTypeManager);

        assertFalse(condition.shouldDisplay(null, null));
    }

    @Test
    public void testFalseNull() {
        when(issueLinkTypeManager.getIssueLinkTypes()).thenReturn(null);

        final HasLinkTypesAvailableCondition condition = new HasLinkTypesAvailableCondition(issueLinkTypeManager);

        assertFalse(condition.shouldDisplay(null, null));
    }
}
