package com.atlassian.jira.help;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.fugue.Option;
import com.atlassian.jira.mock.security.MockSimpleAuthenticationContext;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.NoopI18nFactory;
import com.atlassian.jira.util.NoopI18nHelper;
import com.atlassian.jira.util.resourcebundle.HelpResourceBundleLoader;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.Locale;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @since v6.2.4
 */
public class DefaultHelpUrlsLoaderTest {
    private static final String DEFAULT_KEY = "default";
    private static final String DEFAULT_URL = "https://confluence.atlassian.com/display/JIRA/";
    private static final ApplicationKey APP_KEY = ApplicationKey.valueOf("jira-core");

    private MockSimpleAuthenticationContext ctx =
            new MockSimpleAuthenticationContext(new MockApplicationUser("bbain"), Locale.ENGLISH, new NoopI18nHelper(Locale.ENGLISH));
    private MockLocalHelpUrls internalHelpUrlLoader = new MockLocalHelpUrls();
    private I18nHelper.BeanFactory factory = new NoopI18nFactory();
    private MockHelpUrlsParser urlParser = new MockHelpUrlsParser();
    private HelpUrlsApplicationKeyProvider helpUrlsApplicationKeyProvider = mock(HelpUrlsApplicationKeyProvider.class);
    private HelpResourceBundleLoader helpResourceBundleLoader = mock(HelpResourceBundleLoader.class);
    private DefaultHelpUrlsLoader defaultHelpUrlLoader;
    private ApplicationHelpSpaceProvider applicationHelpSpaceProvider = mock(ApplicationHelpSpaceProvider.class);
    private MockHelpUrlsParserBuilder mockHelpUrlsParserBuilder = new MockHelpUrlsParserBuilder(urlParser);

    private HelpUrlsParserBuilderFactory helpUrlsParserBuilderFactory = mock(HelpUrlsParserBuilderFactory.class);

    @Before
    public void setUp() {
        when(helpUrlsParserBuilderFactory.newBuilder()).thenReturn(mockHelpUrlsParserBuilder);

        defaultHelpUrlLoader = new DefaultHelpUrlsLoader(helpResourceBundleLoader, ctx,
                internalHelpUrlLoader, factory, helpUrlsParserBuilderFactory, helpUrlsApplicationKeyProvider, applicationHelpSpaceProvider);
    }

    @Test
    public void getCurrentUserKeyReturnsCorrectState() {
        when(helpUrlsApplicationKeyProvider.getApplicationKeyForUser()).thenReturn(APP_KEY);
        assertKeyForCurrentUser(Locale.ENGLISH);
        assertKeyForCurrentUser(Locale.GERMAN);
    }

    @Test
    public void loadNoDefaultsAndNoPluginsReturnsABasicHelpPath() {
        HelpUrls load = defaultHelpUrlLoader.apply(new DefaultHelpUrlsLoader.LoaderKey(Locale.ENGLISH, APP_KEY));

        assertThat(load.getUrlKeys(), containsInAnyOrder(DEFAULT_KEY));
        HelpUrlMatcher defaultMatcher = defaultDefaultMatcher();
        assertThat(load.getUrl(DEFAULT_KEY), defaultMatcher);
        assertThat(load.getDefaultUrl(), defaultMatcher);
        assertThat(load.getUrl("other"), defaultMatcher);
    }

    @Test
    public void loadReadsInternalUrls() {
        MockHelpUrl one = internalHelpUrlLoader.add("one");
        MockHelpUrl two = internalHelpUrlLoader.add("two");

        HelpUrls load = defaultHelpUrlLoader.apply(new DefaultHelpUrlsLoader.LoaderKey(Locale.ENGLISH, APP_KEY));
        HelpUrlMatcher defaultMatcher = defaultDefaultMatcher();

        assertThat(load.getUrlKeys(), containsInAnyOrder(DEFAULT_KEY, one.getKey(), two.getKey()));
        assertThat(load.getUrl(DEFAULT_KEY), defaultMatcher);
        assertThat(load.getUrl("three"), defaultMatcher);
        assertThat(load.getUrl(one.getKey()), new HelpUrlMatcher(one));
        assertThat(load.getUrl(two.getKey()), new HelpUrlMatcher(two));
    }

    @Test
    public void loadReadExternalUrls() {
        urlParser.defaultUrl("defaultUrl", "defaultTitle");
        urlParser.createUrlOd("onDemand", "OD+Demand");
        MockHelpUrl btf = urlParser.createUrl("btf", "BTF");
        MockHelpUrl admin = urlParser.createUrl("admin", "Admin");

        when(helpResourceBundleLoader.load(HelpResourceBundleLoader.Type.USER_HELP, Locale.ENGLISH)).thenReturn(ImmutableMap.of(btf.getKey(), "btfAlt"));
        when(helpResourceBundleLoader.load(HelpResourceBundleLoader.Type.ADMIN_HELP, Locale.ENGLISH)).thenReturn(ImmutableMap.of(admin.getKey(), "AdminAlt"));

        HelpUrls load = defaultHelpUrlLoader.apply(new DefaultHelpUrlsLoader.LoaderKey(Locale.ENGLISH, APP_KEY));

        assertThat(load.getUrlKeys(), containsInAnyOrder(DEFAULT_KEY, btf.getKey(), admin.getKey()));
        HelpUrlMatcher defaultMatcher = new HelpUrlMatcher(urlParser.getGeneratedDefault(null));
        assertThat(load.getUrl(DEFAULT_KEY), defaultMatcher);
        assertThat(load.getUrl("three"), defaultMatcher);
        assertThat(load.getUrl(btf.getKey()), new HelpUrlMatcher(urlParser.getGeneratedUrl(btf, "btfAlt")));
        assertThat(load.getUrl(admin.getKey()), new HelpUrlMatcher(urlParser.getGeneratedUrl(admin, "AdminAlt")));
    }

    @Test
    public void loadReadExternalUrlsOd() {
        urlParser.onDemand(true);
        urlParser.defaultUrl("defaultUrl", "defaultTitle");
        MockHelpUrl onDemand = urlParser.createUrlOd("onDemand", "OD+Demand");
        MockHelpUrl btf = urlParser.createUrl("btf", "BTF");

        when(helpResourceBundleLoader.load(HelpResourceBundleLoader.Type.USER_HELP, Locale.ENGLISH)).
                thenReturn(ImmutableMap.of(btf.getKey(), "btfAlt", onDemand.getKey(), "odAlt", "default", "defaultAlt"));

        HelpUrls load = defaultHelpUrlLoader.apply(new DefaultHelpUrlsLoader.LoaderKey(Locale.ENGLISH, APP_KEY));

        assertThat(load.getUrlKeys(), containsInAnyOrder(DEFAULT_KEY, btf.getKey(), onDemand.getKey()));
        HelpUrlMatcher defaultMatcher = new HelpUrlMatcher(urlParser.getGeneratedDefault("defaultAlt"));
        assertThat(load.getUrl(DEFAULT_KEY), defaultMatcher);
        assertThat(load.getUrl("three"), defaultMatcher);
        assertThat(load.getUrl(btf.getKey()), new HelpUrlMatcher(urlParser.getGeneratedUrl(btf, "btfAlt")));
        assertThat(load.getUrl(onDemand.getKey()), new HelpUrlMatcher(urlParser.getGeneratedUrl(onDemand, "odAlt")));
    }

    @Test
    public void configuresParserToUseApplicationHelpSpaceIfDefined() {
        final Option<String> appHelpSpace = Option.some("someapp-space-010");

        when(helpResourceBundleLoader.load(HelpResourceBundleLoader.Type.USER_HELP, Locale.ENGLISH)).thenReturn(ImmutableMap.of());
        when(applicationHelpSpaceProvider.getHelpSpace(APP_KEY)).thenReturn(appHelpSpace);

        defaultHelpUrlLoader.apply(new DefaultHelpUrlsLoader.LoaderKey(Locale.ENGLISH, APP_KEY));

        assertThat(urlParser.getApplicationHelpSpace(), is(appHelpSpace));
    }

    @Test
    public void configuresParserToUseJiraCoreHelpSpaceIfApplicationHelpSpaceNotDefined() {
        final Option<String> fallbackHelpSpace = Option.some("core-space-080");

        when(helpResourceBundleLoader.load(HelpResourceBundleLoader.Type.USER_HELP, Locale.ENGLISH)).thenReturn(ImmutableMap.of());
        when(applicationHelpSpaceProvider.getHelpSpace(APP_KEY)).thenReturn(Option.none());
        when(applicationHelpSpaceProvider.getHelpSpace(ApplicationKey.valueOf("jira-core"))).thenReturn(fallbackHelpSpace);

        defaultHelpUrlLoader.apply(new DefaultHelpUrlsLoader.LoaderKey(Locale.ENGLISH, APP_KEY));

        assertThat(urlParser.getApplicationHelpSpace(), is(fallbackHelpSpace));
    }

    @Test
    public void urlLoadingOrder() {
        List<HelpUrl> urls = Lists.newArrayList();

        //This HelpUrl is overwritten by a plugin.
        MockHelpUrl internalOne = new MockHelpUrl().setKey("one").setUrl("internal");

        //This HelpUrl is not overwritten by a plugin.
        MockHelpUrl two = new MockHelpUrl().setKey("two").setUrl("external");
        MockHelpUrl one = new MockHelpUrl().setKey("one").setUrl("external");

        urls.add(two);
        urls.add(one);

        internalHelpUrlLoader.add(internalOne).add(two);
        urlParser.defaultUrl("defaultUrl", "defaultTitle");
        urlParser.register(one);
        when(helpResourceBundleLoader.load(HelpResourceBundleLoader.Type.USER_HELP, Locale.ENGLISH)).thenReturn(ImmutableMap.of("one", "one"));

        HelpUrls load = defaultHelpUrlLoader.apply(new DefaultHelpUrlsLoader.LoaderKey(Locale.ENGLISH, APP_KEY));

        assertThat(load.getUrlKeys(), containsInAnyOrder(DEFAULT_KEY, one.getKey(), two.getKey()));
        HelpUrlMatcher defaultMatcher = new HelpUrlMatcher(urlParser.getGeneratedDefault(null));
        assertThat(load.getUrl(DEFAULT_KEY), defaultMatcher);
        assertThat(load.getUrl("three"), defaultMatcher);
        assertThat(load.getUrl(one.getKey()), new HelpUrlMatcher(urlParser.getGeneratedUrl(one, "one")));
        assertThat(load.getUrl(two.getKey()), new HelpUrlMatcher(two));
    }

    private HelpUrlMatcher defaultDefaultMatcher() {
        return new HelpUrlMatcher()
                .key(DEFAULT_KEY)
                .url(DEFAULT_URL)
                .title(NoopI18nHelper.makeTranslation("jira.help.paths.help.title"));
    }

    private void assertKeyForCurrentUser(Locale locale) {
        ctx.setLocale(locale);

        HelpUrlsLoader.HelpUrlsLoaderKey key = defaultHelpUrlLoader.keyForCurrentUser();

        assertThat(key, Matchers.instanceOf(DefaultHelpUrlsLoader.LoaderKey.class));
        assertThat(key, Matchers.<HelpUrlsLoader.HelpUrlsLoaderKey>equalTo(new DefaultHelpUrlsLoader.LoaderKey(locale, APP_KEY)));
    }
}
