package com.atlassian.jira.upgrade.tasks.role;

import com.atlassian.fugue.Option;
import com.google.common.collect.ImmutableList;
import org.hamcrest.Description;
import org.hamcrest.TypeSafeDiagnosingMatcher;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.atlassian.jira.upgrade.tasks.role.TestUtils.toLicenses;
import static com.google.common.collect.Iterables.isEmpty;
import static java.util.Arrays.asList;

/**
 * @since v7.0
 */
public class MigrationStateMatcher extends TypeSafeDiagnosingMatcher<MigrationState> {
    private Licenses licenses = new Licenses(ImmutableList.of());
    private ApplicationRoles roles = new ApplicationRoles(ImmutableList.of());
    private Option<List<AuditEntry>> logs = Option.some(new ArrayList<>());

    @Override
    protected boolean matchesSafely(final MigrationState item, final Description mismatchDescription) {
        if (item.licenses().equals(licenses)
                && item.applicationRoles().equals(roles)
                && (!logs.isDefined() || logs.get().equals(item.log().events()))) {
            return true;
        } else {
            mismatchDescription
                    .appendText("[licenses: ").appendValue(item.licenses())
                    .appendText(", roles: ").appendValue(item.applicationRoles())
                    .appendText(", logs: ").appendValue(isEmpty(item.log().events()) ? "any logs" : item.log().events())
                    .appendText("]");
            return false;
        }
    }

    @Override
    public void describeTo(final Description description) {
        description
                .appendText("[licenses: ").appendValue(licenses)
                .appendText(", roles: ").appendValue(roles)
                .appendText(", logs: ").appendValue(logs.isEmpty() ? "any logs" : logs.get())
                .appendText("]");
    }

    MigrationStateMatcher licenses(com.atlassian.jira.test.util.lic.License... license) {
        this.licenses = toLicenses(license);
        return this;
    }

    MigrationStateMatcher licenses(License... license) {
        this.licenses = toLicenses(license);
        return this;
    }

    MigrationStateMatcher licenses(String... encodedLicenses) {
        this.licenses = toLicenses(asList(encodedLicenses).stream().map(License::new).toArray(License[]::new));
        return this;
    }

    MigrationStateMatcher licenses(Licenses licenses) {
        this.licenses = licenses;
        return this;
    }

    MigrationStateMatcher roles(ApplicationRoles roles) {
        this.roles = roles;
        return this;
    }

    MigrationStateMatcher roles(ApplicationRole... roles) {
        this.roles = new ApplicationRoles(Arrays.asList(roles));
        return this;
    }

    MigrationStateMatcher merge(MigrationState state) {
        licenses(state.licenses());
        roles(state.applicationRoles());
        return this;
    }

    public MigrationStateMatcher logs(AuditEntry... logs) {
        this.logs = Option.some(ImmutableList.copyOf(logs));
        return this;
    }

    public MigrationStateMatcher log(AuditEntry log) {
        this.logs = Option.some(ImmutableList.of(log));
        return this;
    }

    public MigrationStateMatcher anyLog() {
        this.logs = Option.none();
        return this;
    }
}
