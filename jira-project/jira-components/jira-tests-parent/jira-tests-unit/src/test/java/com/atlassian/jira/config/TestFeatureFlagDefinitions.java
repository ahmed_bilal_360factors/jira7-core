package com.atlassian.jira.config;


import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class TestFeatureFlagDefinitions {
    private FeatureFlagDefinitions FLAGS;

    @Before
    public void setup() {
        FLAGS = new FeatureFlagDefinitions();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testRegisterDuplicatedFeatureKey() {
        String feature1 = "feature1";
        String duplicatedFeature1 = "feature1";
        FLAGS.featureFlag(feature1).define();
        FLAGS.featureFlag(duplicatedFeature1).define();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testRegisterNullFeatureKey() {
        FLAGS.featureFlag(null).define();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testRegisterFeatureKeyEndWithEnabledPrefix() {
        String featureKey = "feature1" + FeatureFlag.POSTFIX_ENABLED;
        FLAGS.featureFlag(featureKey).define();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testRegisterFeatureKeyEndWithDisabledPrefix() {
        String featureKey = "feature1" + FeatureFlag.POSTFIX_DISABLED;
        FLAGS.featureFlag(featureKey).define();
    }

    @Test
    public void testRegisterFeatureFlags() {
        String feature1 = "feature1";
        String feature2 = "feature2";
        String feature3 = "feature3";

        FeatureFlag flag1 = FLAGS.featureFlag(feature1).define();
        assertThat(flag1.featureKey(), equalTo(feature1));
        assertThat(flag1.isOnByDefault(), equalTo(false));

        FeatureFlag flag2 = FLAGS.featureFlag(feature2).define();
        assertThat(flag2.featureKey(), equalTo(feature2));
        assertThat(flag2.isOnByDefault(), equalTo(false));

        FeatureFlag flag3 = FLAGS.featureFlag(feature3).onByDefault().define();
        assertThat(flag3.featureKey(), equalTo(feature3));
        assertThat(flag3.isOnByDefault(), equalTo(true));

        assertTrue(FLAGS.getFeatureFlags().size() == 3);
    }
}

