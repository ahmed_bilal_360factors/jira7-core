package com.atlassian.jira.issue.fields.csv;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.export.FieldExportParts;
import com.atlassian.jira.issue.fields.LabelsSystemField;
import com.atlassian.jira.issue.label.Label;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.I18nHelper;
import com.google.common.collect.ImmutableSet;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.util.Locale;
import java.util.concurrent.atomic.AtomicLong;

import static java.util.stream.Collectors.toList;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

public class LabelsSystemFieldCsvExportTest {
    private static final AtomicLong IDS = new AtomicLong();

    @Rule
    public final RuleChain mockito = MockitoMocksInContainer.forTest(this);

    @Mock
    private JiraAuthenticationContext authenticationContext;

    @Mock
    private I18nHelper i18nHelper;

    @Before
    public void setUpAuthenticationContext() throws Exception {
        when(authenticationContext.getI18nHelper()).thenReturn(i18nHelper);
        when(authenticationContext.getLocale()).thenReturn(Locale.ENGLISH);
    }

    @InjectMocks
    private LabelsSystemField labelsSystemField;

    @Mock
    private Issue issue;

    @Test
    public void testCsvRepresentationForExpectedTypeAndValues() {
        final Label label1 = label("label1");
        final Label label2 = label("label2");

        when(issue.getLabels()).thenReturn(ImmutableSet.of(label1, label2));

        final FieldExportParts representation = labelsSystemField.getRepresentationFromIssue(issue);
        assertThat(representation.getParts(), hasSize(1));
        assertThat(representation.getParts().get(0).getValues()::iterator, contains("label1", "label2"));
    }

    @Test
    public void testCsvRepresentationValuesAreSorted() {
        final Label label1 = label("b");
        final Label label2 = label("a");

        when(issue.getLabels()).thenReturn(ImmutableSet.of(label1, label2));

        final FieldExportParts representation = labelsSystemField.getRepresentationFromIssue(issue);
        assertThat(representation.getParts(), hasSize(1));
        assertThat(representation.getParts().get(0).getValues()::iterator, contains("a", "b"));
    }

    @Test
    public void testCsvRepresentationWhenThereAreNoLabels() {
        when(issue.getLabels()).thenReturn(ImmutableSet.<Label>of());

        final FieldExportParts representation = labelsSystemField.getRepresentationFromIssue(issue);
        assertThat(representation.getParts(), hasSize(1));
        assertThat(representation.getParts().get(0).getValues().collect(toList()), hasSize(0));
    }

    @Test
    public void testCsvRepresentationWhenLabelsAreNull() {
        when(issue.getLabels()).thenReturn(null);

        final FieldExportParts representation = labelsSystemField.getRepresentationFromIssue(issue);
        assertThat(representation.getParts(), hasSize(1));
        assertThat(representation.getParts().get(0).getValues().collect(toList()), hasSize(0));
    }

    private Label label(final String label) {
        return new Label(IDS.incrementAndGet(), 1L, label);
    }
}
