package com.atlassian.jira.imports.project.ao.handler;

import com.atlassian.core.util.FileUtils;
import com.atlassian.jira.imports.project.util.AoImportTemporaryFiles;
import com.atlassian.jira.imports.project.util.AoImportTemporaryFilesImpl;
import com.atlassian.jira.imports.project.util.XMLEscapeUtil;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.annotation.Nonnull;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.util.List;
import java.util.Map;

import static org.hamcrest.collection.IsArrayWithSize.arrayWithSize;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * @since v6.5
 */
@RunWith(MockitoJUnitRunner.class)
public class TestAoPartitionHandler {

    private File dir;

    @Before
    public void setUp() throws Exception {
        dir = createTempDirectory();
    }

    @After
    public void tearDown() throws Exception {
        FileUtils.deleteDir(dir);
    }

    @Test
    public void testHandlerWithRealXml() throws ParserConfigurationException, SAXException, IOException {
        final String xml = "<?xml version='1.0' encoding='UTF-8'?>\n"
                + "<backup xmlns=\"http://www.atlassian.com/ao\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\n"
                + "  <database>\n"
                + "    <meta key=\"database.name\" value=\"PostgreSQL\"/>\n"
                + "    <meta key=\"database.version\" value=\"9.3.5\"/>\n"
                + "    <meta key=\"database.minorVersion\" value=\"3\"/>\n"
                + "    <meta key=\"database.majorVersion\" value=\"9\"/>\n"
                + "    <meta key=\"driver.name\" value=\"PostgreSQL Native Driver\"/>\n"
                + "    <meta key=\"driver.version\" value=\"PostgreSQL 9.0 JDBC4 (build 801)\"/>\n"
                + "  </database>\n"
                + "  <table name=\"AO_21D670_WHITELIST_RULES\">\n"
                + "    <column name=\"ALLOWINBOUND\" primaryKey=\"false\" autoIncrement=\"false\" sqlType=\"-7\" precision=\"1\"/>\n"
                + "    <column name=\"EXPRESSION\" primaryKey=\"false\" autoIncrement=\"false\" sqlType=\"12\" precision=\"1073741824\"/>\n"
                + "    <column name=\"ID\" primaryKey=\"true\" autoIncrement=\"true\" sqlType=\"4\" precision=\"10\"/>\n"
                + "    <column name=\"TYPE\" primaryKey=\"false\" autoIncrement=\"false\" sqlType=\"12\" precision=\"255\"/>\n"
                + "  </table>\n"
                + "  <table name=\"AO_38321B_CUSTOM_CONTENT_LINK\">\n"
                + "    <column name=\"CONTENT_KEY\" primaryKey=\"false\" autoIncrement=\"false\" sqlType=\"12\" precision=\"255\"/>\n"
                + "    <column name=\"ID\" primaryKey=\"true\" autoIncrement=\"true\" sqlType=\"4\" precision=\"10\"/>\n"
                + "    <column name=\"LINK_LABEL\" primaryKey=\"false\" autoIncrement=\"false\" sqlType=\"12\" precision=\"255\"/>\n"
                + "    <column name=\"LINK_URL\" primaryKey=\"false\" autoIncrement=\"false\" sqlType=\"12\" precision=\"255\"/>\n"
                + "    <column name=\"SEQUENCE\" primaryKey=\"false\" autoIncrement=\"false\" sqlType=\"4\" precision=\"10\"/>\n"
                + "  </table>\n"
                + "  <table name=\"AO_60DB71_BOARDADMINS\">\n"
                + "     <column name=\"ID\" primaryKey=\"true\" autoIncrement=\"true\" sqlType=\"-5\" precision=\"19\"/>\n"
                + "     <column name=\"KEY\" primaryKey=\"false\" autoIncrement=\"false\" sqlType=\"12\" precision=\"255\"/>\n"
                + "     <column name=\"RAPID_VIEW_ID\" primaryKey=\"false\" autoIncrement=\"false\" sqlType=\"-5\" precision=\"19\"/>\n"
                + "     <column name=\"TYPE\" primaryKey=\"false\" autoIncrement=\"false\" sqlType=\"12\" precision=\"255\"/>\n"
                + "     <foreignKey fromTable=\"AO_60DB71_BOARDADMINS\" fromColumn=\"RAPID_VIEW_ID\" toTable=\"AO_60DB71_RAPIDVIEW\" toColumn=\"ID\"/>\n"
                + "  </table>\n"
                + "  <data tableName=\"AO_21D670_WHITELIST_RULES\">\n"
                + "    <column name=\"ALLOWINBOUND\"/>\n"
                + "    <column name=\"EXPRESSION\"/>\n"
                + "    <column name=\"ID\"/>\n"
                + "    <column name=\"TYPE\"/>\n"
                + "    <row>\n"
                + "      <boolean>false</boolean>\n"
                + "      <string>00bc4990-0e6d-35da-b719-c1e2d199099e</string>\n"
                + "      <integer>1</integer>\n"
                + "      <string>APPLICATION_LINK</string>\n"
                + "    </row>\n"
                + "    <row>\n"
                + "      <boolean>true</boolean>\n"
                + "      <string>bf9e23a3-cc04-36ee-b79b-9cedfd31c093</string>\n"
                + "      <integer>2</integer>\n"
                + "      <string>APPLICATION_LINK</string>\n"
                + "    </row>\n"
                + "    <row>\n"
                + "      <boolean>false</boolean>\n"
                + "      <string></string>\n"
                + "      <integer>3</integer>\n"
                + "      <string></string>\n"
                + "    </row>\n"
                + "  </data>\n"
                + "  <data tableName=\"AO_38321B_CUSTOM_CONTENT_LINK\">\n"
                + "    <column name=\"CONTENT_KEY\"/>\n"
                + "    <column name=\"ID\"/>\n"
                + "    <column name=\"LINK_LABEL\"/>\n"
                + "    <column name=\"LINK_URL\"/>\n"
                + "    <column name=\"SEQUENCE\"/>\n"
                + "  </data>\n"
                + "  <data tableName=\"AO_60DB71_BOARDADMINS\">\n"
                + "     <column name=\"ID\"/>\n"
                + "     <column name=\"KEY\"/>\n"
                + "     <column name=\"RAPID_VIEW_ID\"/>\n"
                + "     <column name=\"TYPE\"/>\n"
                + "     <row>\n"
                + "         <integer>1</integer>\n"
                + "         <string>&lt;script>alert(!)&lt;/script></string>\n"
                + "         <integer>1</integer>\n"
                + "         <string>" + XMLEscapeUtil.ESCAPING_CHAR + "</string>\n"
                + "     </row>\n"
                + "  </data>\n"
                + "\n"
                + "</backup>";

        final ChainedAoSaxHandler handler = new ChainedAoSaxHandler();

        AoImportTemporaryFiles aoImportTemporaryFiles = new AoImportTemporaryFilesImpl(dir);

        AoEntityHandler aoPartitionHandler = new AoPartitionHandler(aoImportTemporaryFiles, "UTF-8");

        handler.registerHandler(aoPartitionHandler);

        final SAXParserFactory factory = SAXParserFactory.newInstance();
        final SAXParser saxParser = factory.newSAXParser();
        final InputSource inputSource = new InputSource(new StringReader(xml));
        // Scan the XML
        saxParser.parse(inputSource, handler);

        // Validate we got what we were after
        assertThat(handler.getEntityCount(), is(4L));

        assertThat(dir.listFiles(), arrayWithSize(2));

        // now parse the generated entity files
        for (String entity : aoImportTemporaryFiles.getAOXmlFiles().keySet()) {
            final File entityFile = aoImportTemporaryFiles.getFileForEntity(entity);
            final ChainedAoSaxHandler entityHandler = new ChainedAoSaxHandler();
            saxParser.parse(entityFile, entityHandler);
            // check each entity file has the expected number of entities
            if (entity.equals("AO_21D670_WHITELIST_RULES")) {
                assertThat(entityHandler.getEntityCount(), is(3L));
            } else if (entity.equals("AO_60DB71_BOARDADMINS")) {
                assertThat(entityHandler.getEntityCount(), is(1L));
            }
        }
    }

    @Test
    public void testHandlerWithBigXml() throws ParserConfigurationException, SAXException, IOException {
        final ChainedAoSaxHandler handler = new ChainedAoSaxHandler();
        File dir = createTempDirectory();
        AoImportTemporaryFiles aoImportTemporaryFiles = new AoImportTemporaryFilesImpl(dir);

        AoEntityHandler aoPartitionHandler = new AoPartitionHandler(aoImportTemporaryFiles, "UTF-8");

        handler.registerHandler(aoPartitionHandler);

        final SAXParserFactory factory = SAXParserFactory.newInstance();
        final SAXParser saxParser = factory.newSAXParser();
        InputStream inputStream = TestAoPartitionHandler.class.getResourceAsStream("/com/atlassian/jira/imports/xml/testActiveObjectsParser.xml");
        final InputSource inputSource = new InputSource(new InputStreamReader(inputStream));
        // Scan the XML
        saxParser.parse(inputSource, handler);

        // Validate we got what we were after
        assertThat(handler.getEntityCount(), is(350637L));

        assertThat(dir.listFiles().length, is(17));

        checkWhitelistFile(aoImportTemporaryFiles.createFileForEntity("AO_21D670_WHITELIST_RULES"));

    }

    private void checkWhitelistFile(final File file) throws IOException, SAXException, ParserConfigurationException {
        final ChainedAoSaxHandler handler = new ChainedAoSaxHandler();
        CollectingImportAoEntityHandler collectingImportAoEntityHandler = new CollectingImportAoEntityHandler();

        handler.registerHandler(collectingImportAoEntityHandler);

        final SAXParserFactory factory = SAXParserFactory.newInstance();
        final SAXParser saxParser = factory.newSAXParser();
        final InputSource inputSource = new InputSource(new FileReader(file));
        // Scan the XML
        saxParser.parse(inputSource, handler);

        // Validate we got what we were after
        assertThat(handler.getEntityCount(), is(10L));
        assertThat(collectingImportAoEntityHandler.totalTables, is(1L));
        assertThat(collectingImportAoEntityHandler.totalRows, is(10L));
        // We only collected those that had data
        assertThat(collectingImportAoEntityHandler.tables.size(), is(1));
        Map<String, List<Map<String, Object>>> tables = collectingImportAoEntityHandler.tables;
        assertThat(tables.containsKey("AO_21D670_WHITELIST_RULES"), is(Boolean.TRUE));

        final List<Map<String, Object>> table1 = tables.get("AO_21D670_WHITELIST_RULES");
        Map<String, Object> row = table1.get(0);
        assertThat((Boolean) row.get("ALLOWINBOUND"), is(Boolean.FALSE));
        assertThat((String) row.get("EXPRESSION"), is("00bc4990-0e6d-35da-b719-c1e2d199099e"));
        assertThat((Long) row.get("ID"), is(1L));
        assertThat((String) row.get("TYPE"), is("APPLICATION_LINK"));
    }

    @Nonnull
    private File createTempDirectory() throws IOException {
        File baseDir = new File(System.getProperty("java.io.tmpdir"));
        String name = "Test" + System.currentTimeMillis();
        File dir = new File(baseDir, name);
        dir.mkdir();
        return dir;
    }

}
