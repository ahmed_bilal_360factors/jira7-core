package com.atlassian.jira.application;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.application.host.ApplicationConfigurationManager;
import com.atlassian.application.host.events.ApplicationDefinedEvent;
import com.atlassian.application.host.events.ApplicationUndefinedEvent;
import com.atlassian.cache.memory.MemoryCacheManager;
import com.atlassian.collectors.CollectorsUtil;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.crowd.embedded.spi.DirectoryDao;
import com.atlassian.crowd.event.application.ApplicationDirectoryOrderUpdatedEvent;
import com.atlassian.crowd.event.directory.DirectoryUpdatedEvent;
import com.atlassian.crowd.event.group.GroupCreatedEvent;
import com.atlassian.crowd.event.group.GroupDeletedEvent;
import com.atlassian.crowd.event.group.GroupUpdatedEvent;
import com.atlassian.crowd.manager.directory.SynchronisationStatusManager;
import com.atlassian.crowd.model.application.Application;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.fugue.Option;
import com.atlassian.jira.auditing.AuditingService;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.config.database.DatabaseConfig;
import com.atlassian.jira.config.database.JndiDatasource;
import com.atlassian.jira.config.database.MockDatabaseConfigurationManager;
import com.atlassian.jira.crowd.embedded.ofbiz.InternalMembershipDao;
import com.atlassian.jira.crowd.embedded.ofbiz.MockDirectory;
import com.atlassian.jira.crowd.embedded.ofbiz.OfBizUserDao;
import com.atlassian.jira.crowd.embedded.ofbiz.db.DefaultOfBizTransactionManager;
import com.atlassian.jira.event.ClearCacheEvent;
import com.atlassian.jira.imports.project.handler.ExecutorForTests;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.license.JiraLicenseManager;
import com.atlassian.jira.license.LicenseChangedEvent;
import com.atlassian.jira.license.LicenseCountService;
import com.atlassian.jira.license.LicenseDetails;
import com.atlassian.jira.license.MockLicensedApplications;
import com.atlassian.jira.matchers.OptionMatchers;
import com.atlassian.jira.security.groups.MockGroupManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.MockCrowdService;
import com.atlassian.jira.user.MockGroup;
import com.atlassian.jira.user.MockUser;
import com.atlassian.jira.user.util.DisabledRecoveryMode;
import com.atlassian.jira.user.util.MockUserManager;
import com.atlassian.jira.user.util.StaticRecoveryMode;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.hamcrest.CoreMatchers;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.mockito.Answers;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import static com.atlassian.jira.application.ApplicationRoleDefinitions.ApplicationRoleDefinition;
import static com.google.common.collect.ImmutableMap.of;
import static java.lang.String.format;
import static java.util.Arrays.stream;
import static java.util.stream.Collectors.toSet;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsEmptyCollection.empty;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.anyCollectionOf;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

/**
 * General tests for {@link com.atlassian.jira.application.DefaultApplicationRoleManager}. See also the more specific
 * tests below.
 *
 * @see com.atlassian.jira.application.DefaultApplicationRoleManagerGroupTest
 * @see com.atlassian.jira.application.DefaultApplicationRoleManagerUserCountTest
 * @see com.atlassian.jira.application.DefaultApplicationRoleManagerUserLimitTest
 * @see com.atlassian.jira.application.DefaultApplicationRoleManagerLicenseCountServiceTest
 */
@SuppressWarnings({"ConstantConditions", "deprecation"})
public class DefaultApplicationRoleManagerTest {
    private static final String APPLICATION_ROLE_KEY_ONE = "role.one";
    private static final String APPLICATION_ROLE_KEY_TWO = "role.two";
    private static final String APPLICATION_ROLE_KEY_THREE = "role.three";
    private static final String APPLICATION_ROLE_KEY_FOUR = "role.four";

    private static final Group GROUP1 = group("group.one");
    private static final Group GROUP2 = group("group.two");
    private static final Group GROUP3 = group("group.three");
    private static final Group GROUP4 = group("group.four");
    private static final Group GROUP5 = group("group.five");
    private static final Group GROUP_CORE = group("group.core");
    private static final Group BAD_GROUP = group("bad.group");

    private static final int SEATS_ROLE1 = 10;
    private static final int SEATS_ROLE2 = 20;
    private static final int SEATS_ROLE3 = 30;
    private static final int SEATS_ROLE4 = 0;
    private static final int SEATS_ROLE_CORE = 10;

    private static final ApplicationUser RECOVERY_USER = new MockApplicationUser("recovery_user");

    private MockGroupManager groupManager = new MockGroupManager();
    private MockCrowdService crowdService;
    private MockUserManager userManager;
    @Mock
    private JiraLicenseManager jiraLicenseManager;
    @Mock
    private Directory directory;
    @Mock
    FeatureManager featureManager;
    @Mock
    EventPublisher eventPublisher;
    @Mock
    InternalMembershipDao internalMembershipDao;
    @Mock
    DirectoryDao directoryDao;
    @Mock
    private com.atlassian.crowd.model.group.Group group;
    @Mock
    private Application application;
    private OfBizUserDao ofBizUserDao;
    @Mock
    @AvailableInContainer
    private AuditingService auditingService; // used in setRole
    @Mock
    @AvailableInContainer
    private LicenseCountService licenseCountService; // used in legacyIsAnyRoleLimitExceeded
    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private SynchronisationStatusManager crowdSyncStatusManager;

    public final
    @Rule
    RuleChain mockitoMocks = MockitoMocksInContainer.forTest(this);

    @Captor
    private ArgumentCaptor<ApplicationConfigurationEvent> captureAppConfigEvent;

    private final MemoryCacheManager cacheManager = new MemoryCacheManager();

    private final MockApplicationRoleStore store = new MockApplicationRoleStore();
    private final MockApplicationRoleDefinitions definitions = new MockApplicationRoleDefinitions();
    private final MockApplicationRoleDefinition def1 = new MockApplicationRoleDefinition(APPLICATION_ROLE_KEY_ONE);
    private final MockApplicationRoleDefinition def2 = new MockApplicationRoleDefinition(APPLICATION_ROLE_KEY_TWO);
    private final MockApplicationRoleDefinition def3 = new MockApplicationRoleDefinition(APPLICATION_ROLE_KEY_THREE);
    private final MockApplicationRoleDefinition def4 = new MockApplicationRoleDefinition(APPLICATION_ROLE_KEY_FOUR);
    private final MockApplicationRoleDefinition defCore = new MockApplicationRoleDefinition(ApplicationKeys.CORE.value());

    private DefaultApplicationRoleManager manager;

    private MockApplicationRole role1 = new MockApplicationRole()
            .key(def1.key())
            .groups(GROUP1)
            .name(APPLICATION_ROLE_KEY_ONE)
            .numberOfSeats(SEATS_ROLE1);

    private MockApplicationRole role2 = new MockApplicationRole()
            .key(def2.key())
            .groups(GROUP2)
            .name(APPLICATION_ROLE_KEY_TWO)
            .numberOfSeats(SEATS_ROLE2);

    private MockApplicationRole role3 = new MockApplicationRole()
            .key(def3.key())
            .groups(GROUP3, GROUP4)
            .name(APPLICATION_ROLE_KEY_THREE)
            .numberOfSeats(SEATS_ROLE3)
            .selectedByDefault(true);

    private MockApplicationRole role4 = new MockApplicationRole()
            .key(def4.key())
            .groups()
            .name(APPLICATION_ROLE_KEY_FOUR)
            .numberOfSeats(SEATS_ROLE4)
            .selectedByDefault(true);

    private MockApplicationRole roleCore = new MockApplicationRole()
            .key(ApplicationKeys.CORE)
            .groups(GROUP_CORE)
            .name(ApplicationKeys.CORE.value())
            .numberOfSeats(SEATS_ROLE_CORE)
            .selectedByDefault(false);

    @Before
    public void before() {
        final MockDatabaseConfigurationManager databaseConfigurationManager = new MockDatabaseConfigurationManager();
        databaseConfigurationManager.setDatabaseConfiguration(new DatabaseConfig("postgres", "jira", new JndiDatasource("jira")));
        ofBizUserDao = new OfBizUserDao(null, directoryDao, null, null, null, cacheManager, null, null, new DefaultOfBizTransactionManager(), databaseConfigurationManager) {
            @Override
            public boolean useFullCache() {
                return false;
            }

            @Override
            public void processUsers(Consumer userProcessor) {
                userManager.getAllUsers().forEach(appUser -> userProcessor.accept(appUser.getDirectoryUser()));
            }
        };

        crowdService = new MockCrowdService();
        userManager = new MockUserManager(crowdService);
        userManager.useCrowdServiceToGetUsers();

        manager = new DefaultApplicationRoleManager(cacheManager, store, definitions, groupManager,
                jiraLicenseManager, new StaticRecoveryMode(RECOVERY_USER.getKey()), crowdService,
                eventPublisher, internalMembershipDao, directoryDao, ofBizUserDao, new DefaultOfBizTransactionManager(),
                crowdSyncStatusManager, featureManager);

        defineAndLicense(def1);
        defineAndLicense(def2);
        defineAndLicense(def3);
        defineOnly(def4);
        defineAndLicense(defCore);

        final List<LicenseDetails> detailsList = Lists.newArrayList();
        detailsList.add(createDetails(of(role1.getKey(), role1.getNumberOfSeats())));
        detailsList.add(createDetails(of(role2.getKey(), SEATS_ROLE2, role3.getKey(), SEATS_ROLE3)));
        detailsList.add(createDetails(of(roleCore.getKey(), roleCore.getNumberOfSeats())));

        when(jiraLicenseManager.getLicenses()).thenReturn(detailsList);
        when(jiraLicenseManager.getAllLicensedApplicationKeys()).thenReturn(
                Sets.newHashSet(role1.getKey(), role2.getKey(), role3.getKey(), roleCore.getKey()));

        when(jiraLicenseManager.isLicensed(def1.key())).thenReturn(true);
        when(jiraLicenseManager.isLicensed(def2.key())).thenReturn(true);
        when(jiraLicenseManager.isLicensed(def3.key())).thenReturn(true);
        when(jiraLicenseManager.isLicensed(defCore.key())).thenReturn(true);

        ImmutableList.of(GROUP1, GROUP2, GROUP3, GROUP4, GROUP5, GROUP_CORE).forEach(groupManager::addGroup);

        when(jiraLicenseManager.isLicenseSet()).thenReturn(true);

        when(directoryDao.findAll()).thenReturn(ImmutableList.of(new MockDirectory(MockUser.MOCK_DIRECTORY_ID)));
        when(internalMembershipDao.findGroupChildrenOfGroups(anyLong(), anyCollectionOf(String.class))).thenReturn(Collections.emptyList());
        when(internalMembershipDao.findUserChildrenOfGroups(anyLong(), anyCollectionOf(String.class))).thenAnswer(invocation ->
        {
            @SuppressWarnings("unchecked") Collection<String> groupNames = invocation.getArgumentAt(1, Collection.class);
            List<Group> groups = groupNames.stream().map(MockGroup::new).collect(Collectors.toList());
            return groupManager.getUserNamesInGroups(groups);
        });

    }

    @Test
    public void undefinedApplicationRoleIsUndefined() {
        // Setup
        final ApplicationKey key = def1.key();
        final ApplicationRoleDefinitions roleDefinitions = mock(ApplicationRoleDefinitions.class);
        when(roleDefinitions.getLicensed(key)).thenReturn(Option.option(def1));
        when(roleDefinitions.isDefined(key)).thenReturn(false);

        ApplicationRoleManager applicationRoleManager = new DefaultApplicationRoleManager(cacheManager, store,
                roleDefinitions, groupManager, jiraLicenseManager, new DisabledRecoveryMode(),
                crowdService, eventPublisher, internalMembershipDao, directoryDao, ofBizUserDao, new DefaultOfBizTransactionManager(),
                crowdSyncStatusManager, featureManager);

        manager.onClearCache(ClearCacheEvent.INSTANCE);

        // Test
        assertFalse("An undefined application should return false for isDefined()",
                applicationRoleManager.getRole(key).get().isDefined());
    }

    @Test
    public void definedApplicationRoleIsDefined() {
        // Setup
        final ApplicationKey key = def1.key();
        final ApplicationRoleDefinitions roleDefinitions = mock(ApplicationRoleDefinitions.class);
        when(roleDefinitions.getLicensed(key)).thenReturn(Option.option(def1));
        when(roleDefinitions.isDefined(key)).thenReturn(true);

        ApplicationRoleManager applicationRoleManager = new DefaultApplicationRoleManager(cacheManager, store,
                roleDefinitions, groupManager, jiraLicenseManager, new DisabledRecoveryMode(),
                crowdService, eventPublisher, internalMembershipDao, directoryDao, ofBizUserDao, new DefaultOfBizTransactionManager(),
                crowdSyncStatusManager, featureManager);

        manager.onClearCache(ClearCacheEvent.INSTANCE);

        // Test
        assertTrue("A defined application should return true for isDefined()",
                applicationRoleManager.getRole(key).get().isDefined());
    }

    @Test
    public void seatCountIsCorrect() {
        store.save(role1);
        store.save(role2);
        store.save(role3);
        store.save(role4);

        assertThat(manager.getRole(role1.getKey()), OptionMatchers.some(new ApplicationRoleMatcher().merge(role1)));
        assertThat(manager.getRole(role2.getKey()), OptionMatchers.some(new ApplicationRoleMatcher().merge(role2)));
        assertThat(manager.getRole(role3.getKey()), OptionMatchers.some(new ApplicationRoleMatcher().merge(role3)));

        //This role has no associated license. It should be reported as none (not licensed).
        assertThat(manager.getRole(role4.getKey()), OptionMatchers.none());
    }

    @Test
    public void getApplicationRoleIsCached() {
        store.save(createData(def1, toNames(GROUP1), groups()));
        removeDefinitionAndLicense(def2);
        removeLicense(def2.key());

        assertThat("Ensure role1 is cached by making initial request", manager.getRole(role1.getKey()),
                OptionMatchers.some(new ApplicationRoleMatcher().merge(role1)));

        assertThat("Ensure role1 is cached by making initial request", manager.getRole(role2.getKey()),
                OptionMatchers.none());

        // Initial cache load finished with role1 having group 1 and role2 not existing.
        // Now we change the state of the store and ensure that the cache has not changed.
        store.save(new ApplicationRoleStore.ApplicationRoleData(def2.key(), toNames(GROUP2), Option.none(), false));
        defineAndLicense(def2);

        Option<ApplicationRole> roleOption = manager.getRole(role2.getKey());
        assertThat("Ensure cache still retains original role2", roleOption, OptionMatchers.none());

        store.save(new ApplicationRoleStore.ApplicationRoleData(def1.key(), toNames(GROUP2), toNames(GROUP2), false));
        assertThat("Verify that role1 is cached from initial request", manager.getRole(role1.getKey()),
                OptionMatchers.some(new ApplicationRoleMatcher().merge(role1)));
    }

    @Test
    public void getRolesReturnsAllLicensedRoles() {
        store.save(createData(def1, toNames(GROUP1), groups()));
        store.save(createData(def2, toNames(GROUP2), groups()));
        store.save(createData(def3, toNames(GROUP3, GROUP4), groups(), true));
        store.save(createData(defCore, toNames(GROUP_CORE), groups()));

        //Not licensed
        store.save(createData(def4, toNames(GROUP4), groups()));

        assertThat("Verify that only defined roles 1 still licensed",
                manager.getRoles(), Matchers.containsInAnyOrder(
                        new ApplicationRoleMatcher().merge(role1),
                        new ApplicationRoleMatcher().merge(role2),
                        new ApplicationRoleMatcher().merge(role3),
                        new ApplicationRoleMatcher().merge(roleCore)
                ));
    }

    @Test
    public void getRolesReturnsOnlyLicensedRolesWhenLicenseRemoved() {
        store.save(createData(def1, toNames(GROUP1), groups()));
        store.save(createData(def2, toNames(GROUP2), groups()));
        store.save(createData(def3, toNames(GROUP3), groups()));

        //When: license key containing role2 role3 and coreRole removed
        removeDefinitionAndLicense(def3);
        removeDefinitionAndLicense(def4);
        removeDefinitionAndLicense(defCore);
        removeLicense(def3.key());
        removeLicense(def4.key());
        removeLicense(defCore.key());

        assertThat("Verify that only defined roles 1 still licensed",
                manager.getRoles(), Matchers.contains(new ApplicationRoleMatcher().merge(role1)));
    }

    @Test
    public void getGroupsForInstalledRolesOnlyReturnsExistingGroups() {
        store.save(createData(def1, toNames(GROUP1), groups()));
        store.save(createData(def2, toNames(GROUP2), groups()));
        store.save(createData(def3, toNames(GROUP3, GROUP4, group("notARealGroup")), groups()));

        //Uninstall Role 1
        removeDefinitionAndLicense(def1);
        removeLicense(def1.key());

        assertThat("Verify group2 (part of undefined role2) is not part of groups for installed licenses",
                manager.getGroupsForLicensedRoles(), Matchers.containsInAnyOrder(GROUP2, GROUP3, GROUP4));
    }

    /**
     * @see com.atlassian.jira.application.DefaultApplicationRoleManagerGroupTest
     */
    @Test
    public void testGetRolesForGroup() {
        store.save(createData(def1, toNames(GROUP1), groups()));
        store.save(createData(def2, toNames(GROUP2, GROUP3), groups()));
        store.save(createData(def3, toNames(GROUP3, GROUP4), groups()));
        store.save(createData(def4, groups(), groups()));

        assertEquals(Sets.newHashSet(role1), manager.getRolesForGroup(GROUP1));
        assertEquals(Sets.newHashSet(role2), manager.getRolesForGroup(GROUP2));
        assertEquals(Sets.newHashSet(role2, role3), manager.getRolesForGroup(GROUP3));
        assertEquals(Sets.newHashSet(role3), manager.getRolesForGroup(GROUP4));
        assertEquals(Collections.emptySet(), manager.getRolesForGroup(GROUP5));
    }

    @Test
    public void shouldGetDefaultRolesReturnEmptySetWhenNoRoleMarkedAsDefault() {
        store.save(createData(def1, toNames(GROUP1), groups()));
        store.save(createData(def2, toNames(GROUP2), groups()));

        final Set<ApplicationRole> defaultRoles = manager.getDefaultRoles();

        assertThat(defaultRoles, is(empty()));
    }

    @Test
    public void shouldGetDefaultRolesReturnOneRoleSetWhenOneRoleMarkedAsDefault() {
        store.save(createData(def1, toNames(GROUP1), groups()));
        store.save(createData(def2, toNames(GROUP2), groups(), true));

        final Set<ApplicationRole> defaultRoles = manager.getDefaultRoles();

        assertThat(defaultRoles, Matchers.contains(new ApplicationRoleMatcher().merge(role2).selectedByDefault(true)));
    }

    @Test
    public void isInstalled() {
        //Role2 is no longer installed.
        removeDefinitionAndLicense(def2);

        assertThat("Verify role1 is installed",
                manager.isRoleInstalledAndLicensed(def1.key()), Matchers.equalTo(true));
        assertThat("Verify role2 is  not installed",
                manager.isRoleInstalledAndLicensed(def2.key()), Matchers.equalTo(false));
        assertThat("Verify role1 is installed",
                manager.isRoleInstalledAndLicensed(def3.key()), Matchers.equalTo(true));
    }

    @Test
    public void clearCacheClearsTheRoleCache() {
        assertRoleCacheCleared(() -> manager.onClearCache(ClearCacheEvent.INSTANCE));
    }

    @Test
    public void applicationDefinedClearsTheRoleCache() {
        assertRoleCacheCleared(() -> manager.onApplicationDefined(new ApplicationDefinedEvent(ApplicationKey.valueOf("jira-software"))));
    }

    @Test
    public void applicationUndefinedClearsTheRoleCache() {
        assertRoleCacheCleared(() -> manager.onApplicationUndefined(new ApplicationUndefinedEvent(ApplicationKey.valueOf("jira-software"))));
    }

    @Test
    public void newLicenseClearsTheCache() {
        assertRoleCacheCleared(() -> manager.onLicenseChanged(new LicenseChangedEvent(Option.none(), Option.none())));
    }

    @Test
    public void groupDeletedClearsCache() {
        assertRoleCacheCleared(() -> manager.onGroupDeleted(new GroupDeletedEvent(null, directory, null)));
    }

    @Test
    public void groupCreatedClearsCache() {
        assertRoleCacheCleared(() -> manager.onGroupCreated(new GroupCreatedEvent(null, directory, group)));
    }

    @Test
    public void groupUpdatedClearsCache() {
        assertRoleCacheCleared(() -> manager.onGroupUpdated(new GroupUpdatedEvent(null, directory, group)));
    }

    @Test
    public void directoryReorderedClearsCache() {
        assertRoleCacheCleared(() -> manager.onDirectoryReorder(new ApplicationDirectoryOrderUpdatedEvent(application, directory)));
    }

    @Test
    public void directoryUpdatedClearsCache() {
        assertRoleCacheCleared(() -> manager.onDirectoryUpdated(new DirectoryUpdatedEvent(null, directory)));
    }

    @Test(expected = IllegalArgumentException.class)
    public void setRoleFailsForInvalidGroup() {
        manager.setRole(role3.withGroups(groups(BAD_GROUP), groups()));
    }

    @Test
    public void setRoleDelegatesToStoreAndClearsCacheAndLogsChanges() {
        final ApplicationKey key = def1.key();
        final ApplicationRole input = new MockApplicationRole()
                .name(key.value())
                .key(key)
                .groups(GROUP1, GROUP2, GROUP3)
                .defaultGroups(GROUP2)
                .numberOfSeats(SEATS_ROLE1);

        //start with default license for role1 and role2
        store.save(createData(def1, toNames(GROUP1), groups()));
        store.save(createData(def2, toNames(GROUP2), groups()));

        //ensure licenses are different before starting test
        assertThat("input != role1 ", role1, Matchers.not(new ApplicationRoleMatcher().merge(input)));
        // populate cache with role1.
        assertThat("Ensure that role1 is cached", manager.getRole(role1.getKey()),
                OptionMatchers.some(new ApplicationRoleMatcher().merge(role1)));
        assertThat("Ensure that role2 is cached", manager.getRole(role2.getKey()),
                OptionMatchers.some(new ApplicationRoleMatcher().merge(role2)));

        store.save(createData(def2, toNames(GROUP1, GROUP2), groups()));

        // Set the role, check it has been stored, and the role has been changed in the cache.
        final ApplicationRole output = manager.setRole(input);
        assertThat("Verify that role1 is the one that was set", manager.getRole(role1.getKey()),
                OptionMatchers.some(new ApplicationRoleMatcher().merge(input)));
        assertThat("Verify role 2 in not modified while storing role1", manager.getRole(role2.getKey()),
                OptionMatchers.some(new ApplicationRoleMatcher().merge(role2)));

        assertThat("Verify setRole returned the input", output, new ApplicationRoleMatcher().merge(input));
        assertThat("Verify the store has the new role1 version",
                store.get(key), new ApplicationRoleDataMatcher().merge(input));

        verify(auditingService)
                .storeRecord(eq("applications"), anyString(), anyObject(), anyObject(), anyObject(), anyString());
    }

    @Test
    public void setRoleReturnsEmptyRoleIfCacheCantFindIt() {
        final ApplicationKey key = def1.key();
        final ApplicationRole input = new MockApplicationRole()
                .name("some.role")
                .key(key)
                .groups(GROUP1, GROUP2, GROUP3)
                .defaultGroups(GROUP2);

        final ApplicationRoleDefinitions roleDefinitions = mock(ApplicationRoleDefinitions.class);
        when(roleDefinitions.isDefined(key)).thenReturn(true);
        when(roleDefinitions.isLicensed(key)).thenReturn(true);
        when(roleDefinitions.getLicensed(key)).thenReturn(Option.none(ApplicationRoleDefinition.class));
        when(jiraLicenseManager.getAllLicensedApplicationKeys()).thenReturn(Sets.newHashSet(key));

        ApplicationRoleManager applicationRoleManager = new DefaultApplicationRoleManager(cacheManager, store,
                roleDefinitions, groupManager, jiraLicenseManager, new DisabledRecoveryMode(),
                crowdService, eventPublisher, internalMembershipDao, directoryDao, ofBizUserDao, new DefaultOfBizTransactionManager(),
                crowdSyncStatusManager, featureManager);

        final ApplicationRole output = applicationRoleManager.setRole(input);
        assertThat(output, new ApplicationRoleMatcher().key(key).name(def1.name()));
    }

    @Test
    public void roleLoaderReturnsNoneRoleNotLicensed() {
        final ApplicationKey applicationKey = def1.key();

        store.save(applicationKey, GROUP1.getName());
        removeDefinitionAndLicense(def1);
        removeLicense(def1.key());

        final Option<ApplicationRole> load = manager.getRole(applicationKey);
        assertThat("Verify role1 is not loaded in the cache when it is not defined", load, OptionMatchers.none());
    }

    @Test
    public void applicationRoleLoaderReturnsOnlyValidGroups() {
        final ApplicationKey applicationKey = def1.key();

        store.save(applicationKey, GROUP1.getName(), BAD_GROUP.getName());

        final Option<ApplicationRole> load = manager.getRole(applicationKey);
        assertThat("Verify that when Group2 is non-existent is removed from the application role's groups",
                load, OptionMatchers.some(new ApplicationRoleMatcher().merge(def1).groups(GROUP1).seats(SEATS_ROLE1)));
    }

    @Test
    public void applicationRoleLoaderReturnsRemovesInvalidDefaultGroups() {
        final ApplicationKey applicationKey = def1.key();

        // check when the group is not in the license groups
        store.save(new ApplicationRoleStore.ApplicationRoleData(applicationKey, toNames(GROUP1, GROUP2), toNames(GROUP3), false));
        Option<ApplicationRole> load = manager.getRole(applicationKey);
        assertThat("Verify that GROUP3 is removed from defaults",
                load, OptionMatchers.some(new ApplicationRoleMatcher().merge(def1).groups(GROUP1, GROUP2).seats(SEATS_ROLE1)));

        // Check when the group manager indicates that group DOES NOT exist
        store.save(new ApplicationRoleStore.ApplicationRoleData(applicationKey, toNames(GROUP1, GROUP2, GROUP3), toNames(GROUP3), false));
        when(groupManager.getGroup(GROUP3.getName())).thenReturn(null);
        load = manager.getRole(applicationKey);
        assertThat("Verify that GROUP3 being non-existent will be removed from default groups",
                load, OptionMatchers.some(new ApplicationRoleMatcher().merge(def1).groups(GROUP1, GROUP2).seats(SEATS_ROLE1)));
    }

    @Test
    public void applicationRoleLoaderReturnsValidDefaultGroups() {
        final ApplicationKey applicationKey = def1.key();
        store.save(new ApplicationRoleStore.ApplicationRoleData(applicationKey, toNames(GROUP1, GROUP2), toNames(GROUP2, GROUP1), false));

        final Option<ApplicationRole> load = manager.getRole(applicationKey);
        assertThat("Default group is returned by the cache",
                load, OptionMatchers.some(new ApplicationRoleMatcher()
                        .merge(def1)
                        .groups(GROUP1, GROUP2)
                        .defaultGroups(GROUP2, GROUP1).seats(SEATS_ROLE1)));
    }

    @Test
    public void removeGroupFromRolesRemovesGroupAndClearsCacheAndLogsGroups() {
        store.save(createData(def1, toNames(GROUP1, GROUP2), toNames(GROUP2)));
        store.save(createData(def2, toNames(GROUP1), toNames(GROUP1)));

        assertThat("Pull role1 in to cache", manager.getRole(def1.key()),
                OptionMatchers.some(new ApplicationRoleMatcher().merge(def1)
                        .groups(GROUP1, GROUP2).defaultGroups(GROUP2).seats(SEATS_ROLE1)));

        assertThat("Pull role2 in to cache", manager.getRole(def2.key()),
                OptionMatchers.some(new ApplicationRoleMatcher()
                        .merge(def2).groups(GROUP1).defaultGroups(GROUP1).seats(SEATS_ROLE2)));

        //Change the store but the role2 in the cache will not change.
        store.save(createData(def2, toNames(GROUP1, GROUP3), toNames(GROUP3)));

        manager.removeGroupFromRoles(GROUP2);

        assertThat("Verify Group2 has been removed from role1", manager.getRole(def1.key()),
                OptionMatchers.some(new ApplicationRoleMatcher()
                        .merge(def1).groups(GROUP1).defaultGroups(Option.none()).seats(SEATS_ROLE1)));

        assertThat("Verify that whole cache was cleared, hence new role2 has been updated",
                manager.getRole(def2.key()), OptionMatchers.some(
                        new ApplicationRoleMatcher()
                                .merge(def2).groups(GROUP1, GROUP3).defaultGroups(GROUP3).seats(SEATS_ROLE2)));

        verify(auditingService).storeRecord(eq("applications"), eq("Group was deleted"), anyObject(), anyObject(), eq(null), (String) eq(null));
    }

    /**
     * Tests {@link DefaultApplicationRoleManager#hasAnyRole(com.atlassian.jira.user.ApplicationUser)}.
     *
     * @see com.atlassian.jira.application.DefaultApplicationRoleManagerGroupTest
     */
    @Test
    public void hasAnyRole() {
        //Make sure role2 not licensed
        final List<LicenseDetails> detailsList = Lists.newArrayList();
        detailsList.add(createDetails(of(role1.getKey(), role1.getNumberOfSeats())));
        detailsList.add(createDetails(of(role3.getKey(), SEATS_ROLE3)));
        when(jiraLicenseManager.getAllLicensedApplicationKeys()).thenReturn(
                Sets.newHashSet(role1.getKey(), role3.getKey()));

        MockApplicationUser fred = new MockApplicationUser("fred");

        store.save(createData(def1, toNames(GROUP1), groups()));
        store.save(createData(def2, toNames(GROUP2, group("notAppIsNotLicensed")), groups()));
        store.save(createData(def3, toNames(GROUP3, GROUP4, group("notARealGroup")), groups()));
        store.save(createData(def4, toNames(group("notAppIsNotLicensed")), groups()));

        assertThat("setup is correct", manager.getGroupsForLicensedRoles(),
                CoreMatchers.equalTo(ImmutableSet.of(GROUP1, GROUP3, GROUP4)));

        groupManager.setUserMembership(fred, GROUP2);
        assertThat("fred doesn't have a role", manager.hasAnyRole(fred), equalTo(false));

        groupManager.setUserMembership(fred, GROUP1);
        assertThat("fred has a role", manager.hasAnyRole(fred), equalTo(true));

        groupManager.setUserMembership(fred, GROUP1, GROUP2);
        assertThat("fred has a role", manager.hasAnyRole(fred), equalTo(true));
    }

    @Test
    public void hasAnyRoleRecoveryUser() {
        assertThat(manager.hasAnyRole(RECOVERY_USER), equalTo(true));
        assertThat(manager.hasAnyRole(new MockApplicationUser("fred")), equalTo(false));
    }

    @Test
    public void userHasRoleReturnsFalseWithAnonymousUser() {
        ApplicationUser anonUser = null;
        store.save(createData(def1, toNames(GROUP1), groups()));

        // role1 is installed and licensed with default groups.
        assertThat("Anonymous users have no roles", manager.userHasRole(anonUser, role1.getKey()), equalTo(false));
    }

    @Test
    public void userHasRoleReturnsFalseWithNoApplicationRole() {
        ApplicationKey keyWithNoRole = ApplicationKey.valueOf("no-role");
        ApplicationUser user = new MockApplicationUser("Bob");

        assertThat("Users should not have role when role does not exist",
                manager.userHasRole(user, keyWithNoRole), equalTo(false));
    }

    @Test
    public void userHasRoleReturnsTrueWhenUserHasRole() {
        ApplicationUser user = new MockApplicationUser("Bob");
        store.save(createData(def1, toNames(GROUP1), groups()));
        groupManager.addUserToGroup(user, GROUP1);

        assertThat("Users added to a role's group should have that role",
                manager.userHasRole(user, role1.getKey()), equalTo(true));
    }

    @Test
    public void userHasRoleReturnsFalseWhenUserDoesNotHaveRole() {
        ApplicationUser user = new MockApplicationUser("Bob");
        store.save(createData(def1, toNames(GROUP1), groups()));

        assertThat("Users not added to a role's group have no role",
                manager.userHasRole(user, role1.getKey()), equalTo(false));
    }

    @Test
    public void getOccupiedLicenseRolesForUserShouldNotReturnImplicitlyDefinedCore() {
        ApplicationUser user = new MockApplicationUser("Tom");
        store.save(createData(def1, toNames(GROUP1), groups()));
        store.save(createData(defCore, toNames(GROUP2), groups()));
        groupManager.addUserToGroup(user, GROUP1);

        assertThat("User should not have Core application returned when Core is implicitly defined", manager.getOccupiedLicenseRolesForUser(user), Matchers.contains(role1));
    }

    @Test
    public void getOccupiedLicenseRolesForUserShouldReturnExplicitlyDefinedCore() {
        ApplicationUser user = new MockApplicationUser("Tom");
        store.save(createData(defCore, toNames(GROUP2), groups()));
        groupManager.addUserToGroup(user, GROUP2);

        assertThat("User should have Core application returned when Core is explicitly defined", manager.getOccupiedLicenseRolesForUser(user), Matchers.contains(roleCore));
    }

    @Test
    public void getOccupiedLicenseRolesForUserShouldNotReturnImplicitlyDefinedCoreWithExplicitlyDefinedCore() {
        ApplicationUser user = new MockApplicationUser("Tom");
        store.save(createData(def1, toNames(GROUP1), groups()));
        store.save(createData(defCore, toNames(GROUP2), groups()));
        groupManager.addUserToGroup(user, GROUP1);
        groupManager.addUserToGroup(user, GROUP2);

        assertThat("User should not have Core application returned when Core is implicitly and explicitly defined", manager.getOccupiedLicenseRolesForUser(user), Matchers.contains(role1));
    }

    @Test
    public void getOccupiedLicenseRolesForUserShouldReturnExplicitlyDefinedCoreWithExceededExplicitlyDefinedOtherApplication()
            throws Exception {
        userManager.doNotUseCrowdServiceToGetUsers();

        ApplicationUser user = new MockApplicationUser("Tom");
        store.save(createData(def1, toNames(GROUP1), groups()));
        store.save(createData(defCore, toNames(GROUP2), groups()));
        groupManager.addUserToGroup(user, GROUP1);
        groupManager.addUserToGroup(user, GROUP2);
        crowdService.addUser(user);
        userManager.addUser(user);

        //exceed seats in role1
        for (int i = 0; i < SEATS_ROLE1; i++) {
            MockApplicationUser curUser = new MockApplicationUser("Tom" + i);
            groupManager.addUserToGroup(curUser, GROUP1);
            crowdService.addUser(curUser);
            userManager.addUser(curUser);
        }

        assertThat("User should have Core application returned when Core is explicitly defined and user has all other application roles exceeded",
                manager.getOccupiedLicenseRolesForUser(user), Matchers.containsInAnyOrder(role1, roleCore));
    }

    @Test
    public void clearConfigurationShouldRemoveAppRole() {
        store.save(createData(def1, toNames(GROUP1, GROUP2), toNames(GROUP1), true));

        store.removeByKey(def1.key());

        assertThat("Default configuration not cleared",
                manager.getRole(def1.key()), OptionMatchers.some(
                        new ApplicationRoleMatcher()
                                .merge(def1).seats(10)));
    }

    @Test
    public void emptyLogRecordsShouldNotBeSavedOnApplicationRoleUpdate() {
        store.save(role1);
        manager.clearCache();
        ApplicationRole updated = role1.copy();
        manager.setRole(updated);
        verifyZeroInteractions(auditingService);

        store.save(role2);
        manager.clearCache();
        updated = role2.copy();
        manager.setRole(updated);
        verifyZeroInteractions(auditingService);

        store.save(role3);
        manager.clearCache();
        updated = role3.copy();
        manager.setRole(updated);
        verifyZeroInteractions(auditingService);
    }

    @Test
    public void nonEmptyLogRecordsShouldBeSavedOnApplicationRoleUpdate() {
        store.save(role1);
        ApplicationRole updated = role1.copy().groups();//remove group
        manager.setRole(updated);
        verify(auditingService, times(1)).storeRecord(eq("applications"), anyString(), anyObject(), anyObject(), eq(null), (String) eq(null));

        store.save(role2);
        updated = role2.copy().groups(GROUP1, GROUP2);
        manager.setRole(updated);
        verify(auditingService, times(2)).storeRecord(eq("applications"), anyString(), anyObject(), anyObject(), eq(null), (String) eq(null));

        store.save(role3);
        updated = role3.copy().selectedByDefault(false);
        manager.setRole(updated);
        verify(auditingService, times(3)).storeRecord(eq("applications"), anyString(), anyObject(), anyObject(), eq(null), (String) eq(null));
    }

    @Test
    public void clearConfigurationForDefinedApplicationShouldProduceAuditLogAndFireConfigEvent() {
        ApplicationKey key = def1.key();
        definitions.removeLicensed(key);
        definitions.addDefined(def1);

        manager.clearConfiguration(key);

        verify(auditingService).storeRecord(eq("applications"), eq("Application configuration cleared."), anyObject(), anyObject(), eq(null), (String) eq(null));
        verify(eventPublisher).publish(captureAppConfigEvent.capture());
        assertThat(captureAppConfigEvent.getValue().getApplicationsConfigured(), is(ImmutableSet.of(key)));
    }

    private void assertRoleCacheCleared(Runnable clearCache) {
        store.save(role1);

        assertThat("Verify role1 is cached making initial request", manager.getRole(role1.getKey()),
                OptionMatchers.some(new ApplicationRoleMatcher().merge(role1)));

        store.save(createData(def1, toNames(GROUP2), groups()));
        assertThat("Ensure role1 is still cached from initial request", manager.getRole(role1.getKey()),
                OptionMatchers.some(new ApplicationRoleMatcher().merge(role1)));

        clearCache.run();

        assertThat("Verify role1 is reloaded in the cache", manager.getRole(role1.getKey()),
                OptionMatchers.some(new ApplicationRoleMatcher().merge(role1).groups(GROUP2)));
    }

    private static ApplicationRoleStore.ApplicationRoleData createData(ApplicationRoleDefinition def,
                                                                       Iterable<String> groups, Iterable<String> defaults) {
        return createData(def, groups, defaults, false);
    }

    private static ApplicationRoleStore.ApplicationRoleData createData(ApplicationRoleDefinition def,
                                                                       Iterable<String> groups, Iterable<String> defaults, boolean selectedByDefault) {
        return new ApplicationRoleStore.ApplicationRoleData(def.key(), ImmutableSet.copyOf(groups),
                ImmutableSet.copyOf(defaults), selectedByDefault);
    }

    private static LicenseDetails createDetails(Map<ApplicationKey, Integer> count) {
        final LicenseDetails details = mock(LicenseDetails.class);
        when(details.getLicensedApplications()).thenReturn(new MockLicensedApplications(count));

        return details;
    }

    private void removeDefinitionAndLicense(ApplicationRoleDefinition definition) {
        definitions.removeDefinition(definition);
        definitions.removeLicensed(definition);
    }

    private void defineAndLicense(ApplicationRoleDefinition definition) {
        definitions.addDefined(definition);
        definitions.addLicensed(definition);
    }

    private void defineOnly(ApplicationRoleDefinition definition) {
        definitions.addDefined(definition);
    }

    private void removeLicense(ApplicationKey applicationKey) {
        final List<LicenseDetails> detailsList = Lists.newArrayList();
        final Set<ApplicationKey> licensedAppKeys = Sets.newHashSet();
        for (LicenseDetails licenseDetails : jiraLicenseManager.getLicenses()) {
            final Set<ApplicationKey> licenseAppKeys = licenseDetails.getLicensedApplications().getKeys();
            if (!licenseAppKeys.contains(applicationKey)) {
                detailsList.add(licenseDetails);
                licensedAppKeys.addAll(licenseAppKeys);
            }
        }
        when(jiraLicenseManager.getLicenses()).thenReturn(detailsList);
        when(jiraLicenseManager.getAllLicensedApplicationKeys()).thenReturn(licensedAppKeys);
    }

    private static Set<String> toNames(Group... groups) {
        return stream(groups).map(Group::getName).collect(CollectorsUtil.toImmutableSet());
    }

    private static <T> Set<T> groups() {
        return ImmutableSet.of();
    }

    private static Set<Group> groups(Group... groups) {
        return ImmutableSet.copyOf(groups);
    }

    private static Group group(final String name) {
        return new MockGroup(name);
    }

    @SuppressWarnings({"ConstantConditions", "EmptyCatchBlock"})
    final class ApplicationRoleManagerOperationTester implements ApplicationRoleManager, ApplicationConfigurationManager {
        Set<String> callableMethods = stream(ApplicationRoleManager.class.getDeclaredMethods())
                .map(Method::getName)
                .collect(toSet());

        @Nonnull
        @Override
        public Option<ApplicationRole> getRole(@Nonnull final ApplicationKey role) {
            assertThat(callableMethods.remove("getRole"), is(true));
            return null;
        }

        @Nonnull
        @Override
        public Set<ApplicationRole> getRoles() {
            assertThat(callableMethods.remove("getRoles"), is(true));
            return null;
        }

        @Nonnull
        @Override
        public Set<Group> getDefaultGroups(@Nonnull final ApplicationKey key) {
            try {
                manager.getDefaultGroups(key);
                failNotCallable();
            } catch (UnsupportedOperationException expected) {
            }
            assertThat(callableMethods.remove("getDefaultGroups"), is(true));
            return null;
        }

        @Nonnull
        @Override
        public Set<ApplicationRole> getDefaultRoles() {
            assertThat(callableMethods.remove("getDefaultRoles"), is(true));
            return null;
        }

        @Nonnull
        @Override
        public Set<ApplicationKey> getDefaultApplicationKeys() {
            assertThat(callableMethods.remove("getDefaultApplicationKeys"), is(true));
            return null;
        }

        @Override
        public boolean hasAnyRole(@Nullable final ApplicationUser user) {
            try {
                manager.hasAnyRole(user);
                failNotCallable();
            } catch (UnsupportedOperationException expected) {
            }
            assertThat(callableMethods.remove("hasAnyRole"), is(true));
            return false;
        }

        @Override
        public boolean userHasRole(@Nullable final ApplicationUser user, final ApplicationKey key) {
            try {
                manager.userHasRole(user, key);
                failNotCallable();
            } catch (UnsupportedOperationException expected) {
            }
            assertThat(callableMethods.remove("userHasRole"), is(true));
            return false;
        }

        @Override
        public boolean userOccupiesRole(@Nullable final ApplicationUser user, final ApplicationKey key) {
            try {
                manager.userOccupiesRole(user, key);
                failNotCallable();
            } catch (UnsupportedOperationException expected) {
            }
            assertThat(callableMethods.remove("userOccupiesRole"), is(true));
            return false;
        }

        @Override
        public boolean hasExceededAllRoles(@Nonnull ApplicationUser user) {
            assertThat(callableMethods.remove("hasExceededAllRoles"), is(true));
            return false;
        }

        @Override
        public boolean isAnyRoleLimitExceeded() {
            assertThat(callableMethods.remove("isAnyRoleLimitExceeded"), is(true));
            return false;
        }

        @Override
        public boolean isRoleLimitExceeded(@Nonnull ApplicationKey role) {
            try {
                manager.isRoleLimitExceeded(role);
                failNotCallable();
            } catch (UnsupportedOperationException expected) {
            }
            assertThat(callableMethods.remove("isRoleLimitExceeded"), is(true));
            return false;
        }

        @Override
        public Set<ApplicationRole> getRolesForUser(@Nonnull ApplicationUser user) {
            try {
                manager.getRolesForUser(user);
                failNotCallable();
            } catch (UnsupportedOperationException expected) {
            }
            assertThat(callableMethods.remove("getRolesForUser"), is(true));
            return null;
        }

        @Override
        public Set<ApplicationRole> getOccupiedLicenseRolesForUser(@Nonnull ApplicationUser user) {
            try {
                manager.getOccupiedLicenseRolesForUser(user);
                failNotCallable();
            } catch (UnsupportedOperationException expected) {
            }
            assertThat(callableMethods.remove("getOccupiedLicenseRolesForUser"), is(true));
            return null;
        }

        @Override
        public Set<ApplicationRole> getRolesForGroup(@Nonnull Group group) {
            try {
                manager.getRolesForGroup(group);
                failNotCallable();
            } catch (UnsupportedOperationException expected) {
            }
            assertThat(callableMethods.remove("getRolesForGroup"), is(true));
            return null;
        }

        @Nonnull
        @Override
        public Set<Group> getGroupsForLicensedRoles() {
            assertThat(callableMethods.remove("getGroupsForLicensedRoles"), is(true));
            return null;
        }

        @Override
        public void removeGroupFromRoles(@Nonnull final Group group) {
            assertThat(callableMethods.remove("removeGroupFromRoles"), is(true));
        }

        @Override
        public boolean isRoleInstalledAndLicensed(@Nonnull ApplicationKey key) {
            assertThat(callableMethods.remove("isRoleInstalledAndLicensed"), is(true));
            return false;
        }

        @Nonnull
        @Override
        public ApplicationRole setRole(@Nonnull final ApplicationRole role) {
            try {
                manager.setRole(role);
                failNotCallable();
            } catch (UnsupportedOperationException expected) {
            }
            assertThat(callableMethods.remove("setRole"), is(true));
            return null;
        }

        @Override
        public void clearConfiguration(final ApplicationKey key) {
            try {
                manager.clearConfiguration(key);
                failNotCallable();
            } catch (UnsupportedOperationException expected) {
            }
            assertThat(callableMethods.remove("clearRoleConfiguration"), is(true));
        }

        @Override
        public int getUserCount(@Nonnull final ApplicationKey key) {
            try {
                manager.getUserCount(key);
                failNotCallable();
            } catch (UnsupportedOperationException expected) {
            }
            assertThat(callableMethods.remove("getUserCount"), is(true));
            return 0;
        }

        @Override
        public int getRemainingSeats(@Nonnull final ApplicationKey key) {
            try {
                manager.getRemainingSeats(key);
                failNotCallable();
            } catch (UnsupportedOperationException expected) {
            }
            assertThat(callableMethods.remove("getRemainingSeats"), is(true));
            return 0;
        }

        @Override
        public boolean hasSeatsAvailable(@Nonnull final ApplicationKey key, final int seatCount) {
            try {
                manager.hasSeatsAvailable(key, seatCount);
                failNotCallable();
            } catch (UnsupportedOperationException expected) {
            }
            assertThat(callableMethods.remove("hasSeatsAvailable"), is(true));
            return false;
        }

        private void failNotCallable() {
            String methodName = new Exception().getStackTrace()[1].getMethodName();
            fail(format("Method '%s' is not in JIRA 7 and later.", methodName));
        }

        private Method getMethod(@Nonnull String methodName, Class<?>... paramTypes) {
            try {
                return manager.getClass().getMethod(methodName, paramTypes);
            } catch (NoSuchMethodException e) {
                throw new RuntimeException(e);
            }
        }

        public void thatAllMethodsOnTheInterfaceWereChecked() {
            callableMethods.remove("rolesEnabled");
            if (!callableMethods.isEmpty()) {
                throw new AssertionError("The following methods on ApplicationRole were not checked: "
                        + callableMethods
                        + ", please add them to the test");
            }
        }
    }
}