package com.atlassian.jira.plugin.webfragment.conditions;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.fields.IssueTypeSystemField;
import com.atlassian.jira.issue.fields.option.Option;
import com.atlassian.jira.util.collect.CollectionBuilder;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Collection;
import java.util.Collections;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestHasSubTasksAvailableCondition {
    @Rule
    public MockitoRule mockito = MockitoJUnit.rule();
    @Mock
    private Issue issue;
    @Mock
    private FieldManager fieldManager;
    private HasSubTasksAvailableCondition condition;

    @Before
    public void setUp() throws Exception {
        condition = new HasSubTasksAvailableCondition(fieldManager);
    }

    @Test
    public void testTrue() {
        final Option option = mock(Option.class);

        IssueTypeSystemField field = new IssueTypeSystemField(null, null, null, null, null, null, null, null, null, null, null, null, null) {
            @Override
            public Collection getOptionsForIssue(Issue issue, boolean isSubTask) {
                return CollectionBuilder.list(option);
            }
        };

        when(fieldManager.getIssueTypeField()).thenReturn(field);

        assertTrue(condition.shouldDisplay(null, issue, null));
    }

    @Test
    public void testFalseEmpty() {
        IssueTypeSystemField field = new IssueTypeSystemField(null, null, null, null, null, null, null, null, null, null, null, null, null) {
            @Override
            public Collection getOptionsForIssue(Issue issue, boolean isSubTask) {
                return Collections.emptyList();
            }
        };

        when(fieldManager.getIssueTypeField()).thenReturn(field);

        assertFalse(condition.shouldDisplay(null, issue, null));
    }

    @Test
    public void testFalseNull() {
        IssueTypeSystemField field = new IssueTypeSystemField(null, null, null, null, null, null, null, null, null, null, null, null, null) {
            @Override
            public Collection getOptionsForIssue(Issue issue, boolean isSubTask) {
                return null;
            }
        };

        when(fieldManager.getIssueTypeField()).thenReturn(field);

        assertFalse(condition.shouldDisplay(null, issue, null));
    }
}
