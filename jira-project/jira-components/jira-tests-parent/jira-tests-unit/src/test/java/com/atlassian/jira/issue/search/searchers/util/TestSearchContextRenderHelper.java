package com.atlassian.jira.issue.search.searchers.util;

import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.issue.issuetype.MockIssueType;
import com.atlassian.jira.issue.search.SearchContext;
import com.atlassian.jira.ofbiz.FieldMap;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.google.common.collect.ImmutableList;
import org.junit.Test;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestSearchContextRenderHelper {
    @Test
    public void testSearchContextRendererHelper() {
        final Project project1 = new MockProject(1L, "PROJ1", "Project 1");
        final Project projectXss = new MockProject(42L, "PWNED", "'Project' <script>alert();</script>");
        final IssueType issueTypeBug = new MockIssueType("bug", "Bug");
        final IssueType issueTypeXss = new MockIssueType("pwned", "'Pwned' <script>alert();</script>");

        final SearchContext searchContext = mock(SearchContext.class);
        when(searchContext.getProjects()).thenReturn(ImmutableList.of(project1, projectXss));
        when(searchContext.getIssueTypes()).thenReturn(ImmutableList.of(issueTypeBug, issueTypeXss));

        final FieldMap velocityParameters = new FieldMap();
        SearchContextRenderHelper.addSearchContextParams(searchContext, velocityParameters);
        assertThat(velocityParameters.get("contextProjectNames"),
                is("Project 1, &#39;Project&#39; &lt;script&gt;alert();&lt;/script&gt;"));
        assertThat(velocityParameters.get("contextIssueTypeNames"),
                is("Bug, &#39;Pwned&#39; &lt;script&gt;alert();&lt;/script&gt;"));
    }

}