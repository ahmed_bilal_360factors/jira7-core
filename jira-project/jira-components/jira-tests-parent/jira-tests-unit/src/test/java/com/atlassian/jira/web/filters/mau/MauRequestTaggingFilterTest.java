package com.atlassian.jira.web.filters.mau;

import javax.servlet.FilterConfig;
import javax.servlet.http.HttpServletRequest;

import com.atlassian.jira.application.ApplicationKeys;
import com.atlassian.jira.event.mau.MauApplicationKey;
import com.atlassian.jira.event.mau.MauEventService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.internal.verification.VerificationModeFactory.times;

@RunWith(MockitoJUnitRunner.class)
public class MauRequestTaggingFilterTest {
    private static final MauApplicationKey MAU_SERVICE_DESK = MauApplicationKey.forApplication(ApplicationKeys.SERVICE_DESK);
    private static final MauApplicationKey MAU_SOFTWARE = MauApplicationKey.forApplication(ApplicationKeys.SOFTWARE);
    private static final MauApplicationKey MAU_FAMILY = MauApplicationKey.family();

    @Mock
    private MauRequestTaggingFilter mauRequestTaggingFilter;

    @Mock
    private MauEventService mauEventService;

    @Mock
    private HttpServletRequest request;

    @Mock
    private FilterConfig filterConfig;

    @Before
    public void setUp() throws Exception {
        mauRequestTaggingFilter = new MauRequestTaggingFilter();
        mauRequestTaggingFilter.init(filterConfig);

        when(filterConfig.getInitParameter(eq("mauApplicationKey"))).thenReturn("family");
    }

    @Test
    public void testTagsRequestWithFamilyParam() throws Exception {
        mauRequestTaggingFilter.tagRequest(mauEventService, request);

        verify(mauEventService, times(1)).setApplicationForThread(eq(MAU_FAMILY));
    }

    @Test
    public void testTagsRequestWithSoftwareParam() throws Exception {
        String softwareKey = ApplicationKeys.SOFTWARE.value();
        when(filterConfig.getInitParameter(eq("mauApplicationKey"))).thenReturn(softwareKey);

        mauRequestTaggingFilter.tagRequest(mauEventService, request);

        verify(mauEventService, times(1)).setApplicationForThread(eq(MAU_SOFTWARE));
    }

    @Test
    public void testTagsRequestWithServiceDeskParam() throws Exception {
        String softwareKey = ApplicationKeys.SERVICE_DESK.value();
        when(filterConfig.getInitParameter(eq("mauApplicationKey"))).thenReturn(softwareKey);

        mauRequestTaggingFilter.tagRequest(mauEventService, request);

        verify(mauEventService, times(1)).setApplicationForThread(eq(MAU_SERVICE_DESK));
    }

    @Test
    public void testDoesNotTagRequestWithUnknownParam() throws Exception {
        when(filterConfig.getInitParameter(eq("mauApplicationKey"))).thenReturn("unknown");

        mauRequestTaggingFilter.tagRequest(mauEventService, request);

        verify(mauEventService, never()).setApplicationForThread(any(MauApplicationKey.class));
    }

    @Test
    public void testDoesNotTagRequestWithRubbishParam() throws Exception {
        when(filterConfig.getInitParameter(eq("mauApplicationKey"))).thenReturn("garbage");

        mauRequestTaggingFilter.tagRequest(mauEventService, request);

        verify(mauEventService, never()).setApplicationForThread(any(MauApplicationKey.class));
    }

    @Test
    public void testDoesNotTagRequestWithEmptyParam() throws Exception {
        when(filterConfig.getInitParameter(eq("mauApplicationKey"))).thenReturn(null);

        mauRequestTaggingFilter.tagRequest(mauEventService, request);

        verify(mauEventService, never()).setApplicationForThread(any(MauApplicationKey.class));
    }
}