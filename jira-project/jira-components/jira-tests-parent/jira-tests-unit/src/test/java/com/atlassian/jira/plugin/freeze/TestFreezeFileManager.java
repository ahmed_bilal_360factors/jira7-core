package com.atlassian.jira.plugin.freeze;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.cluster.disasterrecovery.JiraHomeChangeEvent;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.atlassian.jira.cluster.disasterrecovery.JiraHomeChangeEvent.Action.FILE_ADD;
import static com.atlassian.jira.cluster.disasterrecovery.JiraHomeChangeEvent.Action.FILE_DELETED;
import static com.atlassian.jira.cluster.disasterrecovery.JiraHomeChangeEvent.FileType.PLUGIN;
import static java.nio.file.Files.lines;
import static java.util.Arrays.asList;
import static java.util.stream.Collectors.toList;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.arrayContaining;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.isA;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class TestFreezeFileManager {
    @Rule
    public TemporaryFolder temporaryFolder = new TemporaryFolder();

    @Mock
    private EventPublisher eventPublisher;

    public File folder;
    public File installedPluginsFolder;
    private File freezeFile;

    private FreezeFileManager freezer;
    private File plugin1;
    private File plugin2;
    private File plugin3;
    private List<File> installedPlugins;

    public TestFreezeFileManager() throws IOException {
    }

    @Before
    public void setUp() throws IOException {
        folder = temporaryFolder.newFolder();
        installedPluginsFolder = new File(folder, "installed-plugins");
        installedPluginsFolder.mkdir();
        freezeFile = new File(folder, "installed-plugins-freeze.list");
        plugin1 = new File(installedPluginsFolder, "a");
        plugin2 = new File(installedPluginsFolder, "b");
        plugin3 = new File(installedPluginsFolder, "c");
        installedPlugins = new ArrayList<>(asList(plugin1, plugin2, plugin3));
        freezer = new FreezeFileManager(eventPublisher, freezeFile, installedPluginsFolder);
    }

    @Test
    public void freezeFileCreation() throws IOException {
        mockInstalledPlugins();
        freezer.freeze();

        List<String> entries = lines(freezeFile.toPath()).collect(toList());
        assertThat(entries, hasItems("installed-plugins/a", "installed-plugins/b", "installed-plugins/c"));
        verify(eventPublisher, times(1)).publish(argThat(allOf(
                isA(JiraHomeChangeEvent.class),
                hasProperty("action", is(FILE_ADD)),
                hasProperty("files", arrayContaining(freezeFile)),
                hasProperty("fileType", is(PLUGIN))
        )));
    }

    @Test
    public void unfreezeDeletesFreezeFile() throws IOException {
        mockInstalledPlugins();
        freezer.freeze();

        freezer.unfreeze();

        assertThat(freezeFile.exists(), is(false));

        verify(eventPublisher, times(1)).publish(argThat(allOf(
                isA(JiraHomeChangeEvent.class),
                hasProperty("action", is(FILE_DELETED)),
                hasProperty("files", arrayContaining(freezeFile)),
                hasProperty("fileType", is(PLUGIN))
        )));
    }

    @Test
    public void removeUnfrozenPluginFiles() throws IOException {
        mockInstalledPlugins();
        freezer.freeze();
        File newPlugin = new File(installedPluginsFolder, "e");
        newPlugin.createNewFile();

        freezer.removeUnfrozenPlugins();

        assertThat(newPlugin.exists(), is(false));
        assertThat(plugin1.exists(), is(true));
        assertThat(plugin2.exists(), is(true));
        assertThat(plugin3.exists(), is(true));

        verify(eventPublisher, times(1)).publish(argThat(allOf(
                isA(JiraHomeChangeEvent.class),
                hasProperty("action", is(FILE_DELETED)),
                hasProperty("files", arrayContaining(newPlugin)),
                hasProperty("fileType", is(PLUGIN))
        )));
    }

    private void mockInstalledPlugins() throws IOException {
        for (File file: installedPlugins) {
            file.createNewFile();
        }
    }
}
