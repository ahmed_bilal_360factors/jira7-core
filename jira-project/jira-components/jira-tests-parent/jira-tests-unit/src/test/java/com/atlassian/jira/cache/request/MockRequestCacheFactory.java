package com.atlassian.jira.cache.request;

import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

/**
 * Use {@code @Rule public MockRequestCacheFactory requestCacheFactory = MockRequestCacheFactory.rule();} if you need to
 * test a component that uses request caches.  It produces request caches that are fully functional, starts a
 * context for them, and destroys the context afterwards.
 *
 * @since v6.4.8
 */
public class MockRequestCacheFactory extends RequestCacheFactoryImpl implements TestRule {
    private static final MockRequestCacheFactory RULE = new MockRequestCacheFactory();

    private MockRequestCacheFactory() {
    }

    public static MockRequestCacheFactory rule() {
        return RULE;
    }

    @Override
    public Statement apply(final Statement base, Description description) {
        return new Statement() {
            @Override
            public void evaluate() throws Throwable {
                RequestCacheController.startContext();
                try {
                    base.evaluate();
                } finally {
                    while (RequestCacheController.isInContext()) {
                        RequestCacheController.closeContext();
                    }
                }
            }
        };
    }
}
