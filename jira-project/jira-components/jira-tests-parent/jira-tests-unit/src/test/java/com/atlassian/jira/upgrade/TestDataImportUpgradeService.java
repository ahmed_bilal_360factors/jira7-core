package com.atlassian.jira.upgrade;

import com.atlassian.jira.index.request.ReindexRequestType;
import com.atlassian.jira.index.request.ReindexRequestTypes;
import com.atlassian.upgrade.api.UpgradeContext;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import java.util.Set;

import static com.atlassian.upgrade.api.UpgradeContext.UpgradeTrigger.IMPORT;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anySetOf;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestDataImportUpgradeService {

    @Rule
    public MethodRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private LoggingUpgradeService loggingUpgradeService;

    @Captor
    private ArgumentCaptor<Set<ReindexRequestType>> reindexTypes;

    @Captor
    private ArgumentCaptor<UpgradeContext> upgradeContext;

    private DataImportUpgradeService dataImportUpgradeService;

    @Before
    public void setUp() {
        dataImportUpgradeService = new DataImportUpgradeService(loggingUpgradeService);

        when(loggingUpgradeService.runUpgradesWithLogging(anySetOf(ReindexRequestType.class), any(UpgradeContext.class), anyString()))
                .thenReturn(UpgradeResult.OK);
    }

    @Test
    public void shouldDelegateRunUpgrades() {

        dataImportUpgradeService.runUpgrades();

        verify(loggingUpgradeService).runUpgradesWithLogging(reindexTypes.capture(), upgradeContext.capture(), anyString());
        assertThat(reindexTypes.getValue(), equalTo(ReindexRequestTypes.noneAllowed()));
        assertThat(upgradeContext.getValue().getTrigger(), equalTo((IMPORT)));
    }
}