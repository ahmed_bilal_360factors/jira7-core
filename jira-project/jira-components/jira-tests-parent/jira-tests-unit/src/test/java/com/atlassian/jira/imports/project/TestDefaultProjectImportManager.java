package com.atlassian.jira.imports.project;

import com.atlassian.core.util.FileUtils;
import com.atlassian.jira.bc.project.component.ProjectComponent;
import com.atlassian.jira.exception.DataAccessException;
import com.atlassian.jira.external.ExternalException;
import com.atlassian.jira.external.beans.ExternalComponent;
import com.atlassian.jira.external.beans.ExternalCustomField;
import com.atlassian.jira.external.beans.ExternalProject;
import com.atlassian.jira.external.beans.ExternalProjectRoleActor;
import com.atlassian.jira.external.beans.ExternalUser;
import com.atlassian.jira.external.beans.ExternalVersion;
import com.atlassian.jira.imports.project.ao.handler.AoImportHandlerModuleDescriptor;
import com.atlassian.jira.imports.project.core.BackupOverview;
import com.atlassian.jira.imports.project.core.BackupProject;
import com.atlassian.jira.imports.project.core.BackupProjectImpl;
import com.atlassian.jira.imports.project.core.BackupSystemInformation;
import com.atlassian.jira.imports.project.core.BackupSystemInformationImpl;
import com.atlassian.jira.imports.project.core.EntityRepresentation;
import com.atlassian.jira.imports.project.core.EntityRepresentationImpl;
import com.atlassian.jira.imports.project.core.MappingResult;
import com.atlassian.jira.imports.project.core.ProjectImportDataImpl;
import com.atlassian.jira.imports.project.core.ProjectImportOptions;
import com.atlassian.jira.imports.project.core.ProjectImportOptionsImpl;
import com.atlassian.jira.imports.project.core.ProjectImportResults;
import com.atlassian.jira.imports.project.core.ProjectImportResultsImpl;
import com.atlassian.jira.imports.project.customfield.ExternalCustomFieldConfiguration;
import com.atlassian.jira.imports.project.handler.AbortImportException;
import com.atlassian.jira.imports.project.handler.AttachmentFileValidatorHandler;
import com.atlassian.jira.imports.project.handler.ChainedOfBizSaxHandler;
import com.atlassian.jira.imports.project.handler.CustomFieldMapperHandler;
import com.atlassian.jira.imports.project.handler.CustomFieldValueValidatorHandler;
import com.atlassian.jira.imports.project.handler.IssueComponentMapperHandler;
import com.atlassian.jira.imports.project.handler.IssueMapperHandler;
import com.atlassian.jira.imports.project.handler.IssuePartitionHandler;
import com.atlassian.jira.imports.project.handler.IssueRelatedEntitiesPartitionHandler;
import com.atlassian.jira.imports.project.handler.IssueTypeMapperHandler;
import com.atlassian.jira.imports.project.handler.IssueVersionMapperHandler;
import com.atlassian.jira.imports.project.handler.ProjectIssueSecurityLevelMapperHandler;
import com.atlassian.jira.imports.project.handler.ProjectMapperHandler;
import com.atlassian.jira.imports.project.handler.RegisterUserMapperHandler;
import com.atlassian.jira.imports.project.handler.RequiredProjectRolesMapperHandler;
import com.atlassian.jira.imports.project.handler.SimpleEntityMapperHandler;
import com.atlassian.jira.imports.project.handler.UserMapperHandler;
import com.atlassian.jira.imports.project.mapper.AutomaticDataMapper;
import com.atlassian.jira.imports.project.mapper.CustomFieldMapper;
import com.atlassian.jira.imports.project.mapper.ProjectImportMapper;
import com.atlassian.jira.imports.project.mapper.ProjectImportMapperImpl;
import com.atlassian.jira.imports.project.mapper.SimpleProjectImportIdMapperImpl;
import com.atlassian.jira.imports.project.mapper.UserMapper;
import com.atlassian.jira.imports.project.parser.AttachmentParser;
import com.atlassian.jira.imports.project.parser.ChangeGroupParser;
import com.atlassian.jira.imports.project.parser.ChangeItemParser;
import com.atlassian.jira.imports.project.parser.CommentParser;
import com.atlassian.jira.imports.project.parser.CustomFieldValueParser;
import com.atlassian.jira.imports.project.parser.CustomFieldValueParserImpl;
import com.atlassian.jira.imports.project.parser.EntityPropertyParser;
import com.atlassian.jira.imports.project.parser.IssueLinkParser;
import com.atlassian.jira.imports.project.parser.IssueParser;
import com.atlassian.jira.imports.project.parser.NodeAssociationParser;
import com.atlassian.jira.imports.project.parser.ProjectRoleActorParser;
import com.atlassian.jira.imports.project.parser.UserAssociationParser;
import com.atlassian.jira.imports.project.taskprogress.TaskProgressInterval;
import com.atlassian.jira.imports.project.taskprogress.TaskProgressProcessor;
import com.atlassian.jira.imports.project.util.AoImportTemporaryFiles;
import com.atlassian.jira.imports.project.util.MockProjectImportTemporaryFiles;
import com.atlassian.jira.imports.project.util.ProjectImportTemporaryFiles;
import com.atlassian.jira.imports.project.validation.CustomFieldMapperValidator;
import com.atlassian.jira.imports.project.validation.CustomFieldOptionMapperValidator;
import com.atlassian.jira.imports.project.validation.GroupMapperValidator;
import com.atlassian.jira.imports.project.validation.IssueLinkTypeMapperValidator;
import com.atlassian.jira.imports.project.validation.IssueSecurityLevelValidator;
import com.atlassian.jira.imports.project.validation.IssueTypeMapperValidator;
import com.atlassian.jira.imports.project.validation.PriorityMapperValidator;
import com.atlassian.jira.imports.project.validation.ProjectImportValidators;
import com.atlassian.jira.imports.project.validation.ProjectRoleActorMapperValidator;
import com.atlassian.jira.imports.project.validation.ProjectRoleMapperValidator;
import com.atlassian.jira.imports.project.validation.ResolutionMapperValidator;
import com.atlassian.jira.imports.project.validation.StatusMapperValidator;
import com.atlassian.jira.imports.project.validation.SystemFieldsMaxTextLengthValidator;
import com.atlassian.jira.imports.project.validation.UserMapperValidator;
import com.atlassian.jira.imports.xml.BackupXmlParser;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.attachment.AttachmentStore;
import com.atlassian.jira.issue.customfields.CustomFieldType;
import com.atlassian.jira.issue.index.IndexException;
import com.atlassian.jira.issue.label.OfBizLabelStore;
import com.atlassian.jira.issue.link.IssueLinkManager;
import com.atlassian.jira.issue.worklog.DatabaseWorklogStore;
import com.atlassian.jira.junit.rules.InitMockitoMocks;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.security.roles.ProjectRoleManager;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.util.UserUtil;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.MessageSetImpl;
import com.atlassian.jira.util.collect.MapBuilder;
import com.atlassian.jira.util.concurrent.BoundedExecutor;
import com.atlassian.jira.web.bean.MockI18nBean;
import com.atlassian.plugin.ModuleDescriptorFactory;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.predicate.ModuleDescriptorOfTypePredicate;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableMultiset;
import com.google.common.collect.Sets;
import org.hamcrest.Matcher;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.TestRule;
import org.mockito.Answers;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.stubbing.Answer;
import org.ofbiz.core.entity.GenericDelegator;
import org.ofbiz.core.entity.model.MockModelEntity;
import org.ofbiz.core.entity.model.ModelEntity;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.annotation.Nonnull;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.time.Clock;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import static com.atlassian.core.util.map.EasyMap.build;
import static com.atlassian.jira.security.roles.ProjectRoleActor.GROUP_ROLE_ACTOR_TYPE;
import static com.atlassian.jira.security.roles.ProjectRoleActor.USER_ROLE_ACTOR_TYPE;
import static java.lang.System.currentTimeMillis;
import static java.util.Collections.emptyMap;
import static java.util.Collections.emptyList;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @since v3.13
 */
public class TestDefaultProjectImportManager {
    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Rule
    public TestRule initMockitoMocks = new InitMockitoMocks(this);

    private MapBuilder<String, CustomFieldValueParser> entities = MapBuilder.newBuilder();

    private Answer<ModelEntity> modelEntityAnswer = invocation -> new MockModelEntity((String) invocation.getArguments()[0]);

    private final I18nHelper i18n = new MockI18nBean();

    @Mock(answer = Answers.RETURNS_MOCKS)
    private GenericDelegator genericDelegator;
    @Mock
    private BackupXmlParser mockBackupXmlParser;
    @Mock
    private AttachmentStore attachmentStore;
    @Mock
    private ModuleDescriptorFactory mockModuleDescriptorFactory;
    @Mock
    private PluginAccessor mockPluginAccessor;
    @Mock
    private AoImportTemporaryFiles aoImportTemporaryFiles;
    @Mock
    private ProjectImportTemporaryFiles temporaryFiles;
    @Mock
    private ChainedOfBizSaxHandler mockChainedOfBizSaxHandler;
    @Mock
    private IssueTypeMapperValidator mockIssueTypeMapperValidator;
    @Mock
    private ProjectImportValidators mockProjectImportValidators;
    @Mock
    private AutomaticDataMapper mockAutomaticDataMapper;
    @Mock
    private CustomFieldMapperValidator mockCustomFieldMapperValidator;
    @Mock
    private UserUtil mockUserUtil;
    @Mock
    private ProjectImportPersister mockProjectImportPersister;
    @Mock
    private BoundedExecutor mockBoundedExecutor;
    @Mock
    private ProjectImportOptions mockProjectImportOptions;
    @Mock
    private ProjectManager mockProjectManager;
    @Mock
    private ProjectImportPersister mockProjectImportPersister1;
    @Mock
    private CustomFieldManager mockCustomFieldManager;
    @Mock
    private IssueLinkManager mockIssueLinkManager;
    @Mock
    private GroupManager mockGroupManager;
    @Mock
    private ProjectRoleManager mockProjectRoleManager;
    @Mock
    private ProjectRoleActorParser mockProjectRoleActorParser;
    @Mock
    private CustomFieldOptionMapperValidator mockCustomFieldOptionMapperValidator;

    private ProjectImportMapper projectImportMapper;
    private DefaultProjectImportManager defaultProjectImportManager;
    private File tempDirectory;

    @Before
    public void setUp() throws Exception {
        when(genericDelegator.getDelegatorName()).thenReturn("default");
        entities.add(CustomFieldValueParser.CUSTOM_FIELD_VALUE_ENTITY_NAME, new CustomFieldValueParserImpl());
        defaultProjectImportManager = spy(new DefaultProjectImportManager(mockBackupXmlParser, genericDelegator, null, null,
                null, null, null, null, null, null, null, null, null, null, null, mockPluginAccessor, Clock.systemUTC()));

        tempDirectory = createTempDirectory();

        when(mockModuleDescriptorFactory.getModuleDescriptorClass(anyString())).thenAnswer(invocationOnMock -> AoImportHandlerModuleDescriptor.class);
        when(mockPluginAccessor.getModuleDescriptors(any(ModuleDescriptorOfTypePredicate.class))).thenReturn(ImmutableList.of());
        when(temporaryFiles.getWriter(anyString())).thenReturn(new PrintWriter(new StringWriter()));
        projectImportMapper = new ProjectImportMapperImpl(null, null);
    }

    @After
    public void tearDown() throws Exception {
        FileUtils.deleteDir(tempDirectory);
    }

    @Test
    public void testGetBackupOverviewIllegalArgument() throws IOException, SAXException {
        exception.expect(IllegalArgumentException.class);
        defaultProjectImportManager.getBackupOverview(null, null, new MockI18nBean());
    }

    @Test
    public void testGetCustomFieldValuesHandler() throws IOException {
        final AtomicInteger callCount = new AtomicInteger(0);
        when(mockCustomFieldManager.getCustomFieldTypes()).thenReturn(Collections.<CustomFieldType<?, ?>>emptyList());

        final DefaultProjectImportManager manager = new DefaultProjectImportManager(null, genericDelegator, null, null,
                null, mockCustomFieldManager, null, null, null, null, null, null, null, null, null, null, null) {
            @Override
            ModelEntity getModelEntity(final String entityName) {
                callCount.incrementAndGet();
                assertEquals(CustomFieldValueParser.CUSTOM_FIELD_VALUE_ENTITY_NAME, entityName);
                return new MockModelEntity(CustomFieldValueParser.CUSTOM_FIELD_VALUE_ENTITY_NAME);
            }
        };

        final IssueRelatedEntitiesPartitionHandler handler = new IssueRelatedEntitiesPartitionHandler(
                null, temporaryFiles, manager.getModelEntities(manager.getCustomFieldEntityNames()), genericDelegator);
        assertNotNull(handler);
        assertThat(handler.getRegisteredHandlers().keySet(), hasSize(1));
        assertNotNull(handler.getRegisteredHandlers().get(CustomFieldValueParser.CUSTOM_FIELD_VALUE_ENTITY_NAME));
        assertEquals(1, callCount.get());
    }

    @Test
    public void testGetIssueRelatedEntitesHandler() throws IOException {
        doAnswer(modelEntityAnswer).when(defaultProjectImportManager).getModelEntity(anyString());

        final IssueRelatedEntitiesPartitionHandler handler = new IssueRelatedEntitiesPartitionHandler(
                null, temporaryFiles, defaultProjectImportManager.getModelEntities(defaultProjectImportManager.getIssueRelatedEntityNamesForPartitioning()), genericDelegator);
        assertNotNull(handler);
        final Map registeredHandlers = handler.getRegisteredHandlers();
        assertThat(registeredHandlers.keySet(), (Matcher) hasSize(9));
        assertNotNull(handler.getRegisteredHandlers().get(NodeAssociationParser.NODE_ASSOCIATION_ENTITY_NAME));
        assertNotNull(handler.getRegisteredHandlers().get(IssueLinkParser.ISSUE_LINK_ENTITY_NAME));
        assertNotNull(handler.getRegisteredHandlers().get(CommentParser.COMMENT_ENTITY_NAME));
        assertNotNull(handler.getRegisteredHandlers().get(ChangeGroupParser.CHANGE_GROUP_ENTITY_NAME));
        assertNotNull(handler.getRegisteredHandlers().get(ChangeItemParser.CHANGE_ITEM_ENTITY_NAME));
        assertNotNull(handler.getRegisteredHandlers().get(DatabaseWorklogStore.WORKLOG_ENTITY));
        assertNotNull(handler.getRegisteredHandlers().get(UserAssociationParser.USER_ASSOCIATION_ENTITY_NAME));
        assertNotNull(handler.getRegisteredHandlers().get(OfBizLabelStore.TABLE));
        assertNotNull(handler.getRegisteredHandlers().get(EntityPropertyParser.ENTITY_PROPERTY_ENTITY_NAME));
    }

    @Test
    public void testGetFileAttachmentEntitesHandler() throws IOException {
        doAnswer(modelEntityAnswer).when(defaultProjectImportManager).getModelEntity(anyString());

        final IssueRelatedEntitiesPartitionHandler handler = new IssueRelatedEntitiesPartitionHandler(
                null, temporaryFiles, defaultProjectImportManager.getModelEntities(
                defaultProjectImportManager.getFileAttachmentEntityNames()), genericDelegator);
        assertNotNull(handler);
        final Map registeredHandlers = handler.getRegisteredHandlers();
        assertThat(registeredHandlers.keySet(), (Matcher) hasSize(1));
        assertNotNull(handler.getRegisteredHandlers().get(AttachmentParser.ATTACHMENT_ENTITY_NAME));
    }

    @Test
    public void testGetIssuePartitioner() throws IOException {
        final AtomicInteger callCount = new AtomicInteger(0);
        doAnswer(invocation -> {
            callCount.incrementAndGet();
            assertEquals(IssueParser.ISSUE_ENTITY_NAME, invocation.getArguments()[0]);
            return new MockModelEntity(IssueParser.ISSUE_ENTITY_NAME);
        }).when(defaultProjectImportManager).getModelEntity(anyString());

        final IssuePartitionHandler handler = defaultProjectImportManager.getIssuePartitioner(temporaryFiles, null);
        assertEquals(1, callCount.get());
        assertNotNull(handler);
    }

    @Test
    public void testGetStatusMapperHandler() {
        final SimpleEntityMapperHandler handler = new SimpleEntityMapperHandler(SimpleEntityMapperHandler.STATUS_ENTITY_NAME,
                projectImportMapper.getStatusMapper());
        final SimpleEntityMapperHandler simpleEntityMapperHandler = defaultProjectImportManager.getStatusMapperHandler(projectImportMapper);
        assertEquals(handler, simpleEntityMapperHandler);
    }

    @Test
    public void testGetRegisterUserMapperHandler() {
        final RegisterUserMapperHandler handler = new RegisterUserMapperHandler(projectImportMapper.getUserMapper());
        final RegisterUserMapperHandler registerUserMapperHandler = defaultProjectImportManager.getRegisterUserMapperHandler(projectImportMapper);
        assertEquals(handler, registerUserMapperHandler);
    }

    @Test
    public void testGetResolutionMapperHandler() {
        final SimpleEntityMapperHandler handler = new SimpleEntityMapperHandler(SimpleEntityMapperHandler.RESOLUTION_ENTITY_NAME,
                projectImportMapper.getResolutionMapper());
        final SimpleEntityMapperHandler simpleEntityMapperHandler = defaultProjectImportManager.getResolutionMapperHandler(projectImportMapper);
        assertEquals(handler, simpleEntityMapperHandler);
    }

    @Test
    public void testGetIssueVersionMapperHandler() {
        final IssueVersionMapperHandler handler = new IssueVersionMapperHandler(null, projectImportMapper.getVersionMapper());
        final IssueVersionMapperHandler versionMapperHandler = defaultProjectImportManager.getIssueVersionMapperHandler(null, projectImportMapper);
        assertEquals(handler, versionMapperHandler);
    }

    @Test
    public void testGetIssueComponentMapperHandler() {
        final IssueComponentMapperHandler handler = new IssueComponentMapperHandler(null, projectImportMapper.getComponentMapper());
        final IssueComponentMapperHandler componentMapperHandler = defaultProjectImportManager.getIssueComponentMapperHandler(null, projectImportMapper);
        assertEquals(handler, componentMapperHandler);
    }

    @Test
    public void testGetProjectRoleRegistrationHandler() {
        final SimpleEntityMapperHandler handler = new SimpleEntityMapperHandler(SimpleEntityMapperHandler.PROJECT_ROLE_ENTITY_NAME,
                projectImportMapper.getProjectRoleMapper());
        final SimpleEntityMapperHandler simpleEntityMapperHandler = defaultProjectImportManager.getProjectRoleRegistrationHandler(projectImportMapper);
        assertEquals(handler, simpleEntityMapperHandler);
    }

    @Test
    public void testGetRequiredProjectRolesMapperHandler() {
        final RequiredProjectRolesMapperHandler expectedMapperHandler = new RequiredProjectRolesMapperHandler(null,
                projectImportMapper.getProjectRoleMapper());
        final RequiredProjectRolesMapperHandler mapperHandler = defaultProjectImportManager.getRequiredProjectRolesMapperHandler(null, projectImportMapper);
        assertEquals(expectedMapperHandler, mapperHandler);
    }

    @Test
    public void testGetPriorityMapperHandler() {
        final SimpleEntityMapperHandler handler = new SimpleEntityMapperHandler(SimpleEntityMapperHandler.PRIORITY_ENTITY_NAME,
                projectImportMapper.getPriorityMapper());
        final SimpleEntityMapperHandler simpleEntityMapperHandler = defaultProjectImportManager.getPriorityMapperHandler(projectImportMapper);
        assertEquals(handler, simpleEntityMapperHandler);
    }

    @Test
    public void testGetIssueTypeMapperHandler() {
        final IssueTypeMapperHandler handler = new IssueTypeMapperHandler(projectImportMapper.getIssueTypeMapper());
        final IssueTypeMapperHandler issueTypeMapperHandler = defaultProjectImportManager.getIssueTypeMapperHandler(projectImportMapper);
        assertEquals(handler, issueTypeMapperHandler);
    }

    @Test
    public void testGetProjectIssueSecurityLevelMapperHandler() {
        final ProjectIssueSecurityLevelMapperHandler expectedMapperHandler = new ProjectIssueSecurityLevelMapperHandler(null,
                projectImportMapper.getIssueSecurityLevelMapper());
        final ProjectIssueSecurityLevelMapperHandler mapperHandler = defaultProjectImportManager.getProjectIssueSecurityLevelMapperHandler(null,
                projectImportMapper);
        assertEquals(expectedMapperHandler, mapperHandler);
    }

    @Test
    public void testGetIssueMapperHandler() {
        final IssueMapperHandler expectedMapperHandler = new IssueMapperHandler(null, projectImportMapper);
        final IssueMapperHandler mapperHandler = defaultProjectImportManager.getIssueMapperHandler(null, projectImportMapper);
        assertEquals(expectedMapperHandler, mapperHandler);
    }

    @Test
    public void testGetUserMapperHandler() {
        final UserMapperHandler expectedMapperHandler = new UserMapperHandler(null, null, projectImportMapper.getUserMapper(), attachmentStore);
        final UserMapperHandler mapperHandler = defaultProjectImportManager.getUserMapperHandler(null, null, projectImportMapper);
        assertEquals(expectedMapperHandler, mapperHandler);
    }

    @Test
    public void testGetCustomFieldMapperHandler() {
        final CustomFieldMapperHandler expectedMapperHandler = new CustomFieldMapperHandler(null, projectImportMapper.getCustomFieldMapper(), entities.toMap());

        when(mockCustomFieldManager.getCustomFieldTypes()).thenReturn(Collections.<CustomFieldType<?, ?>>emptyList());

        final DefaultProjectImportManager projectImportManager = new DefaultProjectImportManager(null, null, null, null, null, mockCustomFieldManager, null, null,
                null, null, null, null, null, null, null, null, null);
        final CustomFieldMapperHandler mapperHandler = projectImportManager.getCustomFieldMapperHandler(null, projectImportMapper);
        assertEquals(expectedMapperHandler, mapperHandler);
    }

    @Test
    public void testGetProjectMapperHandler() {
        final ProjectMapperHandler expectedMapperHandler = new ProjectMapperHandler(projectImportMapper.getProjectMapper());
        final ProjectMapperHandler mapperHandler = defaultProjectImportManager.getProjectMapperHandler(projectImportMapper);
        assertEquals(expectedMapperHandler, mapperHandler);
    }

    @Test
    public void testGetCustomFieldValueValidatorHandler() {
        // Set up the BackupProject
        final ExternalProject externalProject = new ExternalProject();
        externalProject.setId("10");
        final BackupProject backupProject = new BackupProjectImpl(externalProject, Collections.emptyList(), Collections.emptyList(),
                Collections.emptyList(), Collections.emptyList(), 0, ImmutableMap.of());

        when(mockCustomFieldManager.getCustomFieldTypes()).thenReturn(Collections.<CustomFieldType<?, ?>>emptyList());

        final CustomFieldValueValidatorHandler expectedMapperHandler = new CustomFieldValueValidatorHandler(backupProject, projectImportMapper, mockCustomFieldManager, null);

        final DefaultProjectImportManager projectImportManager = new DefaultProjectImportManager(null, null, null, null, null, mockCustomFieldManager, null, null,
                null, null, null, null, null, null, null, null, null);
        final CustomFieldValueValidatorHandler mapperHandler = projectImportManager.getCustomFieldValueValidatorHandler(backupProject,
                projectImportMapper);
        assertEquals(expectedMapperHandler, mapperHandler);
    }

    @Test
    public void testPopulateCustomFieldMapperOldValues() {
        final ExternalCustomFieldConfiguration configuration1 = new ExternalCustomFieldConfiguration(null, null, new ExternalCustomField("11111",
                "CustomField1", "df.df.df:eee"), "321");
        final ExternalCustomFieldConfiguration configuration2 = new ExternalCustomFieldConfiguration(null, null, new ExternalCustomField("22222",
                "CustomField2", "df.df.df:fff"), "321");
        final List customFIeldConfigs = ImmutableList.of(configuration1, configuration2);
        final BackupProject backupProject = new BackupProjectImpl(new ExternalProject(), Collections.emptyList(), Collections.emptyList(),
                customFIeldConfigs, Collections.emptyList(), 0, ImmutableMap.of());
        final CustomFieldMapper customFieldMapper = new CustomFieldMapper();

        defaultProjectImportManager.populateCustomFieldMapperOldValues(backupProject, customFieldMapper);

        assertThat(customFieldMapper.getRegisteredOldIds(), containsInAnyOrder("11111", "22222"));
        assertEquals("CustomField1", customFieldMapper.getKey("11111"));
        assertEquals("CustomField2", customFieldMapper.getKey("22222"));
    }

    @Test
    public void testPopulateVersionMapperOldValues() {
        final ExternalVersion version1 = new ExternalVersion("Version1");
        version1.setId("11111");
        final ExternalVersion version2 = new ExternalVersion("Version2");
        version2.setId("22222");
        final List versions = ImmutableList.of(version1, version2);
        final BackupProject backupProject = new BackupProjectImpl(new ExternalProject(), versions, Collections.emptyList(), Collections.emptyList(),
                Collections.emptyList(), 0, ImmutableMap.of());
        final SimpleProjectImportIdMapperImpl versionMapper = new SimpleProjectImportIdMapperImpl();

        defaultProjectImportManager.populateVersionMapperOldValues(backupProject, versionMapper);

        assertThat(versionMapper.getRegisteredOldIds(), containsInAnyOrder("11111", "22222"));
        assertEquals("Version1", versionMapper.getKey("11111"));
        assertEquals("Version2", versionMapper.getKey("22222"));
    }

    @Test
    public void testPopulateVersionMapper() throws Exception {
        final SimpleProjectImportIdMapperImpl versionMapper = new SimpleProjectImportIdMapperImpl();

        final Version mockVersion = mock(Version.class);
        when(mockVersion.getId()).thenReturn(1L);

        final Version mockVersion2 = mock(Version.class);
        when(mockVersion2.getId()).thenReturn(2L);

        defaultProjectImportManager.populateVersionMapper(versionMapper, build("12", mockVersion, "14", mockVersion2));

        assertEquals("1", versionMapper.getMappedId("12"));
        assertEquals("2", versionMapper.getMappedId("14"));
    }

    @Test
    public void testPopulateComponentMapperOldValues() {
        final ExternalComponent component1 = new ExternalComponent("Component1");
        component1.setId("11111");
        final ExternalComponent component2 = new ExternalComponent("Component2");
        component2.setId("22222");
        final List components = ImmutableList.of(component1, component2);
        final BackupProject backupProject = new BackupProjectImpl(new ExternalProject(), Collections.emptyList(), components, Collections.emptyList(),
                Collections.emptyList(), 0, ImmutableMap.of());
        final SimpleProjectImportIdMapperImpl componentMapper = new SimpleProjectImportIdMapperImpl();

        defaultProjectImportManager.populateComponentMapperOldValues(backupProject, componentMapper);

        assertThat(componentMapper.getRegisteredOldIds(), containsInAnyOrder("11111", "22222"));
        assertEquals("Component1", componentMapper.getKey("11111"));
        assertEquals("Component2", componentMapper.getKey("22222"));
    }

    @Test
    public void testPopulateComponentMapper() throws Exception {
        final SimpleProjectImportIdMapperImpl componentMapper = new SimpleProjectImportIdMapperImpl();

        final ProjectComponent mockProjectComponent = mock(ProjectComponent.class);
        when(mockProjectComponent.getId()).thenReturn(1L);

        final ProjectComponent mockProjectComponent2 = mock(ProjectComponent.class);
        when(mockProjectComponent2.getId()).thenReturn(2L);

        defaultProjectImportManager.populateComponentMapper(componentMapper, build("12", mockProjectComponent, "14", mockProjectComponent2));

        assertEquals("1", componentMapper.getMappedId("12"));
        assertEquals("2", componentMapper.getMappedId("14"));
    }

    @Test
    public void testValidateCustomFieldValuesNullBackupProject() throws IOException, SAXException {
        exception.expect(IllegalArgumentException.class);
        final ProjectImportDataImpl projectImportData = new ProjectImportDataImpl(null, null, aoImportTemporaryFiles, 0, 0, 0, 0, 0, 0);
        defaultProjectImportManager.validateCustomFieldValues(projectImportData, new MappingResult(), null, null, null);
    }

    @Test
    public void testValidateCustomFieldValuesNullMappingResult() throws IOException, SAXException {
        exception.expect(IllegalArgumentException.class);
        final ProjectImportDataImpl projectImportData = new ProjectImportDataImpl(null, null, aoImportTemporaryFiles, 0, 0, 0, 0, 0, 0);
        defaultProjectImportManager.validateCustomFieldValues(projectImportData, null, new BackupProjectImpl(new ExternalProject(),
                emptyList(), emptyList(), emptyList(), emptyList(), 0, ImmutableMap.of()), null, null);
    }

    @Test
    public void testValidateCustomFieldValuesNullProjectImporter() throws IOException, SAXException {
        exception.expect(IllegalArgumentException.class);
        final ProjectImportDataImpl projectImportData = new ProjectImportDataImpl(null, null, aoImportTemporaryFiles, 0, 0, 0, 0, 0, 0);
        defaultProjectImportManager.validateCustomFieldValues(projectImportData, new MappingResult(), new BackupProjectImpl(
                new ExternalProject(), emptyList(), emptyList(), emptyList(), emptyList(), 0, ImmutableMap.of()), null, null);
    }

    @Test
    public void testValidateCustomFieldValuesNullCustFieldXmlPath() throws IOException, SAXException {
        exception.expect(IllegalArgumentException.class);
        final ProjectImportDataImpl projectImportData = new ProjectImportDataImpl(null, null, aoImportTemporaryFiles, 0, 0, 0, 0, 0, 0);
        defaultProjectImportManager.validateCustomFieldValues(projectImportData, new MappingResult(), new BackupProjectImpl(
                new ExternalProject(), emptyList(), emptyList(), emptyList(), emptyList(), 0, ImmutableMap.of()), null, null);
    }

    @Test
    public void testValidateCustomFieldValuesHappyPath() throws IOException, SAXException {
        // Set up the BackupProject
        final ExternalProject externalProject = new ExternalProject();
        externalProject.setId("10");
        final BackupProject backupProject = new BackupProjectImpl(externalProject, emptyList(), emptyList(),
                emptyList(), emptyList(), 0, ImmutableMap.of());

        final MockI18nBean i18nBean = new MockI18nBean();

        when(mockCustomFieldManager.getCustomFieldTypes()).thenReturn(emptyList());

        final CustomFieldValueValidatorHandler valueValidatorHandler = new CustomFieldValueValidatorHandler(backupProject, projectImportMapper, mockCustomFieldManager, null);

        final ProjectImportTemporaryFiles temporaryFiles = new MockProjectImportTemporaryFiles("TST");

        when(mockProjectImportValidators.getCustomFieldOptionMapperValidator()).thenReturn(mockCustomFieldOptionMapperValidator);

        final DefaultProjectImportManager defaultProjectImportManager = new DefaultProjectImportManager(mockBackupXmlParser, null, null, null, null,
                mockCustomFieldManager, null, mockProjectImportValidators, null, null, null, null, null, null, null, null, null) {
            @Override
            ChainedOfBizSaxHandler getOfBizChainedHandler(final TaskProgressProcessor taskProgressProcessor) {
                return mockChainedOfBizSaxHandler;
            }
        };

        final ProjectImportDataImpl projectImportData = new ProjectImportDataImpl(projectImportMapper, temporaryFiles, aoImportTemporaryFiles, 0, 0, 0, 0, 0, 0);
        final MappingResult backupParsingResult = new MappingResult();

        defaultProjectImportManager.validateCustomFieldValues(projectImportData, backupParsingResult, backupProject, null, i18nBean);

        verify(mockChainedOfBizSaxHandler).registerHandler(valueValidatorHandler);
        verify(mockCustomFieldOptionMapperValidator).validateMappings(i18nBean, backupProject, projectImportMapper.getCustomFieldOptionMapper(),
                projectImportMapper.getCustomFieldMapper(), valueValidatorHandler.getValidationResults());
        verify(mockBackupXmlParser).parseOfBizBackupXml(temporaryFiles.getEntityXmlFile("CustomFieldValue").getAbsolutePath(), mockChainedOfBizSaxHandler);
    }

    private BackupSystemInformationImpl getBackupSystemInformationImpl() {
        return new BackupSystemInformationImpl("123", "Pro", Collections.emptyList(), true, Collections.emptyMap(), Sets.<String>newHashSet(), 0);
    }

    @Test
    public void testValidateFileAttachmentsNullBackupProject() throws IOException, SAXException {
        exception.expect(IllegalArgumentException.class);
        final ProjectImportDataImpl projectImportData = new ProjectImportDataImpl(null, null, aoImportTemporaryFiles, 0, 0, 0, 0, 0, 0);
        defaultProjectImportManager.validateFileAttachments(new ProjectImportOptionsImpl("", "/attach/path"), projectImportData,
                new MappingResult(), null, getBackupSystemInformationImpl(), null, null);
    }

    @Test
    public void testValidateFileAttachmentsNullMappingResult() throws IOException, SAXException {
        exception.expect(IllegalArgumentException.class);
        final ProjectImportDataImpl projectImportData = new ProjectImportDataImpl(null, null, aoImportTemporaryFiles, 0, 0, 0, 0, 0, 0);
        defaultProjectImportManager.validateFileAttachments(new ProjectImportOptionsImpl("", "/attach/path"), projectImportData, null,
                new BackupProjectImpl(new ExternalProject(), emptyList(), emptyList(), emptyList(),
                        emptyList(), 0, ImmutableMap.of()), getBackupSystemInformationImpl(), null, null);
    }

    @Test
    public void testValidateFileAttachmentsNullBackupSystemInfo() throws IOException, SAXException {
        exception.expect(IllegalArgumentException.class);
        final ProjectImportDataImpl projectImportData = new ProjectImportDataImpl(null, null, aoImportTemporaryFiles, 0, 0, 0, 0, 0, 0);
        defaultProjectImportManager.validateFileAttachments(new ProjectImportOptionsImpl("", "/attach/path"), projectImportData,
                new MappingResult(), new BackupProjectImpl(new ExternalProject(), emptyList(), emptyList(),
                        emptyList(), emptyList(), 0, ImmutableMap.of()), null, null, null);
    }

    @Test
    public void testValidateFileAttachmentsHappyPath() throws IOException, SAXException {
        // Set up the BackupProject
        final ExternalProject externalProject = new ExternalProject();
        externalProject.setId("10");
        final BackupProject backupProject = new BackupProjectImpl(externalProject, Collections.emptyList(), Collections.emptyList(),
                Collections.emptyList(), Collections.emptyList(), 0, ImmutableMap.of());

        final ProjectImportOptionsImpl projectImportOptions = new ProjectImportOptionsImpl("/", "/");
        final BackupSystemInformationImpl backupSysInfo = getBackupSystemInformationImpl();
        final AttachmentFileValidatorHandler valueValidatorHandler = new AttachmentFileValidatorHandler(backupProject, projectImportOptions,
                backupSysInfo, null, attachmentStore);

        final ProjectImportTemporaryFiles temporaryFiles = new MockProjectImportTemporaryFiles("TST");

        final DefaultProjectImportManager defaultProjectImportManager = new DefaultProjectImportManager(mockBackupXmlParser, null, null, null, null,
                null, null, null, null, null, null, null, null, null, null, null, null) {
            ChainedOfBizSaxHandler getOfBizChainedHandler(final TaskProgressProcessor taskProgressProcessor) {
                return mockChainedOfBizSaxHandler;
            }

            AttachmentFileValidatorHandler getAttachmentFileValidatorHandler(final BackupProject backupProject, final ProjectImportOptions projectImportOptions, final BackupSystemInformation backupSystemInformation, final I18nHelper i18nHelper) {
                return valueValidatorHandler;
            }
        };

        final MappingResult backupParsingResult = new MappingResult();

        final ProjectImportDataImpl projectImportData = new ProjectImportDataImpl(projectImportMapper, temporaryFiles, aoImportTemporaryFiles, 0, 0, 0, 0, 0, 0);
        defaultProjectImportManager.validateFileAttachments(projectImportOptions, projectImportData, backupParsingResult, backupProject,
                backupSysInfo, null, new MockI18nBean());

        verify(mockChainedOfBizSaxHandler).registerHandler(valueValidatorHandler);
        verify(mockBackupXmlParser).parseOfBizBackupXml(temporaryFiles.getEntityXmlFile("FileAttachment").getAbsolutePath(), mockChainedOfBizSaxHandler);
    }

    @Test
    public void testAutoMapAndValidateIssueTypes() throws IOException, SAXException {
        // Set up the BackupProject
        final BackupProject backupProject = new BackupProjectImpl(new ExternalProject(), emptyList(), emptyList(),
                emptyList(), emptyList(), 0, ImmutableMap.of());

        final MessageSetImpl messageSet = new MessageSetImpl();
        messageSet.addErrorMessage("I am an error message");
        when(mockIssueTypeMapperValidator.validateMappings(i18n, backupProject, projectImportMapper.getIssueTypeMapper())).thenReturn(messageSet);

        when(mockProjectImportValidators.getIssueTypeMapperValidator()).thenReturn(mockIssueTypeMapperValidator);

        final DefaultProjectImportManager defaultProjectImportManager = new DefaultProjectImportManager(null, null, null, null, null, null,
                mockAutomaticDataMapper, mockProjectImportValidators, null, null, null, null, null, null, null, null, null);

        final MappingResult mappingResult = new MappingResult();

        final ProjectImportTemporaryFiles temporaryFiles = new MockProjectImportTemporaryFiles("TST");
        final ProjectImportDataImpl projectImportData = new ProjectImportDataImpl(projectImportMapper, temporaryFiles, aoImportTemporaryFiles, 0, 0, 0, 0, 0, 0);

        defaultProjectImportManager.autoMapAndValidateIssueTypes(projectImportData, mappingResult, backupProject, i18n);

        assertTrue(mappingResult.getIssueTypeMessageSet().hasAnyErrors());
    }

    @Test
    public void testAutoMapAndValidateCustomFields() throws IOException, SAXException {
        // Set up the BackupProject
        final BackupProject backupProject = new BackupProjectImpl(new ExternalProject(), emptyList(), emptyList(),
                emptyList(), emptyList(), 0, ImmutableMap.of());

        final MessageSetImpl messageSet = new MessageSetImpl();
        messageSet.addErrorMessage("I am an error message");
        when(mockCustomFieldMapperValidator.validateMappings(i18n, backupProject, projectImportMapper.getIssueTypeMapper(), projectImportMapper.getCustomFieldMapper())).thenReturn(messageSet);

        when(mockProjectImportValidators.getCustomFieldMapperValidator()).thenReturn(mockCustomFieldMapperValidator);

        final DefaultProjectImportManager defaultProjectImportManager = new DefaultProjectImportManager(null, null, null, null, null, null,
                mockAutomaticDataMapper, mockProjectImportValidators, null, null, null, null, null, null, null, null, null);

        final MappingResult mappingResult = new MappingResult();

        final ProjectImportTemporaryFiles temporaryFiles = new MockProjectImportTemporaryFiles("TST");
        final ProjectImportDataImpl projectImportData = new ProjectImportDataImpl(projectImportMapper, temporaryFiles, aoImportTemporaryFiles, 0, 0, 0, 0, 0, 0);
        defaultProjectImportManager.autoMapAndValidateCustomFields(projectImportData, mappingResult, backupProject, i18n);

        assertTrue(mappingResult.getCustomFieldMessageSet().hasAnyErrors());
    }

    @Test
    public void testAutoMapSystemFields() throws IOException, SAXException {
        // Set up the BackupProject
        final BackupProject backupProject = new BackupProjectImpl(new ExternalProject(), Collections.emptyList(), Collections.emptyList(),
                Collections.emptyList(), Collections.emptyList(), 0, ImmutableMap.of());

        final DefaultProjectImportManager defaultProjectImportManager = new DefaultProjectImportManager(null, null, null, null, null, null,
                mockAutomaticDataMapper, null, null, null, null, null, null, null, null, null, null);

        final ProjectImportDataImpl projectImportData = new ProjectImportDataImpl(projectImportMapper, null, aoImportTemporaryFiles, 0, 0, 0, 0, 0, 0);
        defaultProjectImportManager.autoMapSystemFields(projectImportData, backupProject);

        verify(mockAutomaticDataMapper).mapPriorities(projectImportMapper.getPriorityMapper());
        verify(mockAutomaticDataMapper).mapProjects(projectImportMapper.getProjectMapper());
        verify(mockAutomaticDataMapper).mapResolutions(projectImportMapper.getResolutionMapper());
        verify(mockAutomaticDataMapper).mapStatuses(backupProject, projectImportMapper.getStatusMapper(), projectImportMapper.getIssueTypeMapper());
        verify(mockAutomaticDataMapper).mapIssueLinkTypes(projectImportMapper.getIssueLinkTypeMapper());
        verify(mockAutomaticDataMapper).mapIssueSecurityLevels(backupProject.getProject().getKey(), projectImportMapper.getIssueSecurityLevelMapper());
    }

    @Test
    public void testAutoMapCustomFieldOptions() throws IOException, SAXException {
        // Set up the BackupProject
        final BackupProject backupProject = new BackupProjectImpl(new ExternalProject(), Collections.emptyList(), Collections.emptyList(),
                Collections.emptyList(), Collections.emptyList(), 0, ImmutableMap.of());

        final DefaultProjectImportManager defaultProjectImportManager = new DefaultProjectImportManager(null, null, null, null, null, null,
                mockAutomaticDataMapper, null, null, null, null, null, null, null, null, null, null);

        final ProjectImportDataImpl projectImportData = new ProjectImportDataImpl(projectImportMapper, null, aoImportTemporaryFiles, 0, 0, 0, 0, 0, 0);

        defaultProjectImportManager.autoMapCustomFieldOptions(projectImportData, backupProject);

        verify(mockAutomaticDataMapper).mapCustomFieldOptions(backupProject, projectImportMapper.getCustomFieldOptionMapper(),
                projectImportMapper.getCustomFieldMapper(), projectImportMapper.getIssueTypeMapper());
    }

    @Test
    public void testAutoMapProjectRoles() throws IOException, SAXException {
        final DefaultProjectImportManager defaultProjectImportManager = new DefaultProjectImportManager(null, null, null, null, null, null,
                mockAutomaticDataMapper, null, null, null, null, null, null, null, null, null, null);

        final ProjectImportDataImpl projectImportData = new ProjectImportDataImpl(projectImportMapper, null, aoImportTemporaryFiles, 0, 0, 0, 0, 0, 0);

        defaultProjectImportManager.autoMapProjectRoles(projectImportData);

        verify(mockAutomaticDataMapper).mapProjectRoles(projectImportMapper.getProjectRoleMapper());
    }

    @Test
    public void testValidateSystemFields() throws IOException, SAXException {
        // Set up the BackupProject
        final BackupProject backupProject = new BackupProjectImpl(new ExternalProject(), emptyList(), emptyList(),
                emptyList(), emptyList(), 0, ImmutableMap.of());

        final PriorityMapperValidator mockPriorityMapperValidator = mock(PriorityMapperValidator.class);
        final MessageSetImpl priorityMessageSet = new MessageSetImpl();
        priorityMessageSet.addErrorMessage("I am Priority error message");
        when(mockPriorityMapperValidator.validateMappings(i18n, projectImportMapper.getPriorityMapper())).thenReturn(priorityMessageSet);

        final ResolutionMapperValidator mockResolutionMapperValidator = mock(ResolutionMapperValidator.class);
        final MessageSetImpl resolutionMessageSet = new MessageSetImpl();
        resolutionMessageSet.addErrorMessage("I am Resolution error message");
        when(mockResolutionMapperValidator.validateMappings(i18n, projectImportMapper.getResolutionMapper())).thenReturn(resolutionMessageSet);

        final StatusMapperValidator mockStatusMapperValidator = mock(StatusMapperValidator.class);
        final MessageSetImpl statusMessageSet = new MessageSetImpl();
        statusMessageSet.addErrorMessage("I am Status error message");
        when(mockStatusMapperValidator.validateMappings(i18n, backupProject, projectImportMapper.getIssueTypeMapper(), projectImportMapper.getStatusMapper())).thenReturn(statusMessageSet);

        final UserMapperValidator mockUserMapperValidator = mock(UserMapperValidator.class);
        final MessageSetImpl userMessageSet = new MessageSetImpl();
        userMessageSet.addErrorMessage("I am user error message");
        when(mockUserMapperValidator.validateMappings(i18n, projectImportMapper.getUserMapper())).thenReturn(userMessageSet);

        final ProjectRoleMapperValidator mockProjectRoleMapperValidator = mock(ProjectRoleMapperValidator.class);
        final MessageSetImpl projectRoleMessageSet = new MessageSetImpl();
        projectRoleMessageSet.addErrorMessage("Invalid project role mappings");
        when(mockProjectRoleMapperValidator.validateMappings(i18n, projectImportMapper.getProjectRoleMapper())).thenReturn(projectRoleMessageSet);

        final ProjectRoleActorMapperValidator mockProjectRoleActorMapperValidator = mock(ProjectRoleActorMapperValidator.class);
        final MessageSetImpl projectRoleActorMessageSet = new MessageSetImpl();
        projectRoleActorMessageSet.addErrorMessage("Invalid project role mappings");
        when(mockProjectRoleActorMapperValidator.validateProjectRoleActors(i18n, projectImportMapper, null)).thenReturn(projectRoleActorMessageSet);

        final GroupMapperValidator mockGroupMapperValidator = mock(GroupMapperValidator.class);
        final MessageSetImpl groupMessageSet = new MessageSetImpl();
        groupMessageSet.addErrorMessage("Invalid project role mappings");
        when(mockGroupMapperValidator.validateMappings(i18n, projectImportMapper.getGroupMapper())).thenReturn(groupMessageSet);

        final IssueLinkTypeMapperValidator mockIssueLinkTypeMapperValidator = mock(IssueLinkTypeMapperValidator.class);
        final MessageSetImpl issueLinkTypeMessageSet = new MessageSetImpl();
        issueLinkTypeMessageSet.addErrorMessage("Invalid issue link type mappings");
        when(mockIssueLinkTypeMapperValidator.validateMappings(i18n, backupProject, projectImportMapper.getIssueLinkTypeMapper())).thenReturn(issueLinkTypeMessageSet);

        final IssueSecurityLevelValidator mockIssueSecurityLevelValidator = mock(IssueSecurityLevelValidator.class);
        final MessageSetImpl issueSecurityLevelMessageSet = new MessageSetImpl();
        issueSecurityLevelMessageSet.addErrorMessage("Invalid issue security level mappings");
        when(mockIssueSecurityLevelValidator.validateMappings(projectImportMapper.getIssueSecurityLevelMapper(), backupProject, i18n)).thenReturn(issueSecurityLevelMessageSet);

        final SystemFieldsMaxTextLengthValidator mockSystemFieldsMaxTextLengthValidator = mock(SystemFieldsMaxTextLengthValidator.class);
        final MessageSetImpl systemFieldsMaxTextLengthValidatorMessageSet = new MessageSetImpl();
        systemFieldsMaxTextLengthValidatorMessageSet.addErrorMessage("Invalid max sytem text length");
        when(mockSystemFieldsMaxTextLengthValidator.validate(backupProject, i18n)).thenReturn(systemFieldsMaxTextLengthValidatorMessageSet);

        when(mockProjectImportValidators.getPriorityMapperValidator()).thenReturn(mockPriorityMapperValidator);
        when(mockProjectImportValidators.getResolutionMapperValidator()).thenReturn(mockResolutionMapperValidator);
        when(mockProjectImportValidators.getStatusMapperValidator()).thenReturn(mockStatusMapperValidator);
        when(mockProjectImportValidators.getProjectRoleMapperValidator()).thenReturn(mockProjectRoleMapperValidator);
        when(mockProjectImportValidators.getProjectRoleActorMapperValidator()).thenReturn(mockProjectRoleActorMapperValidator);
        when(mockProjectImportValidators.getUserMapperValidator()).thenReturn(mockUserMapperValidator);
        when(mockProjectImportValidators.getGroupMapperValidator()).thenReturn(mockGroupMapperValidator);
        when(mockProjectImportValidators.getIssueLinkTypeMapperValidator()).thenReturn(mockIssueLinkTypeMapperValidator);
        when(mockProjectImportValidators.getIssueSecurityLevelValidator()).thenReturn(mockIssueSecurityLevelValidator);
        when(mockProjectImportValidators.getSystemFieldsMaxTextLengthValidator()).thenReturn(mockSystemFieldsMaxTextLengthValidator);

        final DefaultProjectImportManager defaultProjectImportManager = new DefaultProjectImportManager(null, null, null, null, null, null, null,
                mockProjectImportValidators, null, null, null, null, null, null, null, null, null);

        final MappingResult mappingResult = new MappingResult();

        final ProjectImportDataImpl projectImportData = new ProjectImportDataImpl(projectImportMapper, null, aoImportTemporaryFiles, 0, 0, 0, 0, 0, 0);
        defaultProjectImportManager.validateSystemFields(projectImportData, mappingResult, null, backupProject, null, i18n);

        assertTrue(mappingResult.getPriorityMessageSet().hasAnyErrors());
        assertTrue(mappingResult.getStatusMessageSet().hasAnyErrors());
        assertTrue(mappingResult.getResolutionMessageSet().hasAnyErrors());
        assertTrue(mappingResult.getProjectRoleMessageSet().hasAnyErrors());
        assertTrue(mappingResult.getUserMessageSet().hasAnyErrors());
        assertTrue(mappingResult.getProjectRoleMessageSet().hasAnyErrors());
        assertTrue(mappingResult.getProjectRoleActorMessageSet().hasAnyErrors());
        assertTrue(mappingResult.getGroupMessageSet().hasAnyErrors());
        assertTrue(mappingResult.getIssueLinkTypeMessageSet().hasAnyErrors());
        assertTrue(mappingResult.getIssueSecurityLevelMessageSet().hasAnyErrors());
        assertTrue(mappingResult.getTextFieldLengthExceedingLimitMessageSet().hasAnyErrors());
    }

    @Test
    public void testCreateUsers() throws AbortImportException {
        final ProjectImportResultsImpl projectImportResults = new ProjectImportResultsImpl(0, 0, 0, 0, new MockI18nBean());
        final ExternalUser externalUser = new ExternalUser("fredKey", "fred", "Fred Flinstone", "fred@bedrock.com", "dead");

        when(mockUserUtil.userExists("fred")).thenReturn(false);
        when(mockUserUtil.userExists("barney")).thenReturn(false);

        final UserMapper userMapper = new UserMapper(mockUserUtil);
        userMapper.flagUserAsInUse("fredKey");
        userMapper.flagUserAsInUse("barney");
        userMapper.registerOldValue(externalUser);

        when(mockProjectImportPersister.createUser(userMapper, externalUser)).thenReturn(true);

        final DefaultProjectImportManager defaultProjectImportManager = new DefaultProjectImportManager(null, null, null, null, null, null, null,
                null, mockProjectImportPersister, null, null, null, null, null, null, null, null);

        defaultProjectImportManager.createMissingUsers(userMapper, projectImportResults, null);

        assertEquals(1, projectImportResults.getUsersCreatedCount());
    }

    @Test
    public void testCreateUsersErrorCreatingUser() throws AbortImportException {
        final ProjectImportResultsImpl projectImportResults = new ProjectImportResultsImpl(0, 0, 0, 0, new MockI18nBean());
        final ExternalUser externalUser = new ExternalUser("fredKey", "fred", "Fred Flinstone", "fred@bedrock.com", "dead");

        when(mockUserUtil.userExists("fred")).thenReturn(false);
        when(mockUserUtil.userExists("barney")).thenReturn(false);

        final UserMapper userMapper = new UserMapper(mockUserUtil);
        userMapper.flagUserAsInUse("fredKey");
        userMapper.flagUserAsInUse("barney");
        userMapper.registerOldValue(externalUser);

        when(mockProjectImportPersister.createUser(userMapper, externalUser)).thenReturn(false);

        final DefaultProjectImportManager defaultProjectImportManager = new DefaultProjectImportManager(null, null, null, null, null, null, null,
                null, mockProjectImportPersister, null, null, null, null, null, null, null, null);

        defaultProjectImportManager.createMissingUsers(userMapper, projectImportResults, null);

        assertEquals(0, projectImportResults.getUsersCreatedCount());
        assertThat(projectImportResults.getErrors(), hasSize(1));
        assertEquals("Could not create user 'fred'.", projectImportResults.getErrors().iterator().next());
    }

    @Test
    public void testCreateUsersErrorCreatingUserAndAbort() throws AbortImportException {
        final ProjectImportResultsImpl projectImportResults = new ProjectImportResultsImpl(0, 0, 0, 0, new MockI18nBean());
        // seed with a bunch of errors
        for (int i = 0; i < 10; i++) {
            projectImportResults.addError("error" + i);
        }

        final ExternalUser externalUser = new ExternalUser("fredKey", "fred", "Fred Flinstone", "fred@bedrock.com", "dead");

        final DefaultProjectImportManager defaultProjectImportManager = new DefaultProjectImportManager(null, null, null, null, null, null, null,
                null, mockProjectImportPersister, null, null, null, null, null, null, null, null) {
            BoundedExecutor createExecutor(final String threadName) {
                return mockBoundedExecutor;
            }
        };

        when(mockUserUtil.userExists("fred")).thenReturn(false);
        when(mockUserUtil.userExists("barney")).thenReturn(false);

        final UserMapper userMapper = new UserMapper(mockUserUtil);
        userMapper.flagUserAsInUse("fredKey");
        userMapper.flagUserAsInUse("barney");
        userMapper.registerOldValue(externalUser);

        exception.expect(AbortImportException.class);
        defaultProjectImportManager.createMissingUsers(userMapper, projectImportResults, null);

        assertEquals(0, projectImportResults.getUsersCreatedCount());
        verify(mockBoundedExecutor).shutdownAndIgnoreQueue();
    }

    @Test
    public void testImportProject_DontUpdateProject() throws ExternalException, AbortImportException {
        // user doesn't "update Proj details", so ProjectImportPersister.updateProjectDetails() should NOT be called.

        final ExternalProject oldProject = new ExternalProject();
        oldProject.setId("12");
        oldProject.setCounter("23");
        final BackupProject backupProject = new BackupProjectImpl(oldProject, emptyList(), emptyList(), emptyList(),
                emptyList(), 0, ImmutableMap.of());

        // Mock ProjectImportOptions

        when(mockProjectImportOptions.overwriteProjectDetails()).thenReturn(false);

        when(mockProjectManager.getProjectObjByKey(oldProject.getKey())).thenReturn(new MockProject());

        final ProjectImportMapper projectImportMapper = new ProjectImportMapperImpl(mockUserUtil, null);
        projectImportMapper.getProjectMapper().mapValue("12", "112");

        when(mockProjectImportPersister.createVersions(backupProject)).thenReturn(emptyMap());
        when(mockProjectImportPersister.createComponents(backupProject, projectImportMapper)).thenReturn(emptyMap());

        final DefaultProjectImportManager defaultProjectImportManager = new DefaultProjectImportManager(null, null, null, null, null, null, null,
                null, mockProjectImportPersister, null, null, null, mockProjectManager, null, null, null, null);

        defaultProjectImportManager.importProject(mockProjectImportOptions, projectImportMapper, backupProject, new ProjectImportResultsImpl(0, 0, 0,
                0, null), null);

        verify(mockProjectImportPersister).createVersions(backupProject);
        verify(mockProjectImportPersister).createComponents(backupProject, projectImportMapper);
        verify(mockProjectImportPersister).updateProjectIssueCounter(backupProject, 23);
    }

    @Test
    public void testImportProject_UpdateProject() throws ExternalException, AbortImportException {
        // user chooses "update Proj details", so ProjectImportPersister.updateProjectDetails() should be called.

        final ExternalProject oldProject = new ExternalProject();
        oldProject.setId("12");
        oldProject.setCounter("23");
        final BackupProject backupProject = new BackupProjectImpl(oldProject, emptyList(), emptyList(), emptyList(),
                emptyList(), 0, ImmutableMap.of());

        when(mockProjectImportOptions.overwriteProjectDetails()).thenReturn(true);

        // Make the Monkey Project already exist.
        projectImportMapper.getProjectMapper().mapValue("12", "112");

        when(mockProjectImportPersister.updateProjectDetails(oldProject)).thenReturn(new MockProject());
        when(mockProjectImportPersister.createVersions(backupProject)).thenReturn(emptyMap());
        when(mockProjectImportPersister.createComponents(backupProject, projectImportMapper)).thenReturn(emptyMap());

        final AtomicBoolean importRoleCalled = new AtomicBoolean(false);
        final DefaultProjectImportManager defaultProjectImportManager = new DefaultProjectImportManager(null, null, null, null, null, null, null,
                null, mockProjectImportPersister, null, null, null, null, null, null, null, null) {
            public void importProjectRoleMembers(final Project project, final ProjectImportMapper projectImportMapper, final ProjectImportResults projectImportResults, final TaskProgressInterval taskProgressInterval) {
                // do nothing
                importRoleCalled.set(true);
            }
        };

        defaultProjectImportManager.importProject(mockProjectImportOptions, projectImportMapper, backupProject, new ProjectImportResultsImpl(0, 0, 0,
                0, null), null);

        assertTrue(importRoleCalled.get());
        verify(mockProjectImportPersister).updateProjectDetails(oldProject);
        verify(mockProjectImportPersister).createVersions(backupProject);
        verify(mockProjectImportPersister).createComponents(backupProject, projectImportMapper);
        verify(mockProjectImportPersister).updateProjectIssueCounter(backupProject, 23);
    }

    @Test
    public void testImportProject_CreateProject() throws ExternalException, AbortImportException {
        final ExternalProject oldProject = new ExternalProject();
        oldProject.setId("4");
        oldProject.setCounter("23");
        final BackupProject backupProject = new BackupProjectImpl(oldProject, emptyList(), emptyList(), emptyList(),
                emptyList(), 0, ImmutableMap.of());

        when(mockProjectImportPersister.createProject(backupProject.getProject())).thenReturn(new MockProject(12));
        when(mockProjectImportPersister.createVersions(backupProject)).thenReturn(emptyMap());
        when(mockProjectImportPersister.createComponents(backupProject, projectImportMapper)).thenReturn(emptyMap());

        final DefaultProjectImportManager defaultProjectImportManager = new DefaultProjectImportManager(null, null, null, null, null, null, null,
                null, mockProjectImportPersister, null, null, null, null, null, null, null, null);

        defaultProjectImportManager.importProject(new ProjectImportOptionsImpl("", "", false), projectImportMapper, backupProject,
                new ProjectImportResultsImpl(0, 0, 0, 0, null), null);

        assertEquals("12", projectImportMapper.getProjectMapper().getMappedId("4"));
        verify(mockProjectImportPersister).createProject(backupProject.getProject());
        verify(mockProjectImportPersister).updateProjectIssueCounter(backupProject, 23);
        verify(mockProjectImportPersister).createVersions(backupProject);
        verify(mockProjectImportPersister).createComponents(backupProject, projectImportMapper);
    }

    @Test
    public void testImportProject_VersionsAbort() throws ExternalException, AbortImportException {
        // user chooses "update Proj details", so ProjectImportPersister.updateProjectDetails() should be called.

        final ExternalProject oldProject = new ExternalProject();
        oldProject.setId("12");
        oldProject.setCounter("23");
        final BackupProject backupProject = new BackupProjectImpl(oldProject, emptyList(), emptyList(), emptyList(),
                emptyList(), 0, ImmutableMap.of());

        when(mockProjectImportOptions.overwriteProjectDetails()).thenReturn(true);

        when(mockProjectImportPersister.updateProjectDetails(oldProject)).thenReturn(new MockProject());
        when(mockProjectImportPersister.createVersions(backupProject)).thenThrow(new DataAccessException("blah"));

        // Make the Monkey Project already exist.
        projectImportMapper.getProjectMapper().mapValue("12", "112");
        final DefaultProjectImportManager defaultProjectImportManager = new DefaultProjectImportManager(null, null, null, null, null, null, null,
                null, mockProjectImportPersister, null, null, null, null, null, null, null, null);

        exception.expect(AbortImportException.class);
        defaultProjectImportManager.importProject(mockProjectImportOptions, projectImportMapper, backupProject, new ProjectImportResultsImpl(0,
                0, 0, 0, null), null);

        verify(mockProjectImportPersister).updateProjectDetails(oldProject);
        verify(mockProjectImportPersister).updateProjectIssueCounter(backupProject, 23);
        verify(mockProjectImportPersister).createVersions(backupProject);
    }

    @Test
    public void testImportProject_ComponentsAbort() throws ExternalException, AbortImportException {
        // user chooses "update Proj details", so ProjectImportPersister.updateProjectDetails() should be called.

        final ExternalProject oldProject = new ExternalProject();
        oldProject.setId("12");
        oldProject.setCounter("23");
        final BackupProject backupProject = new BackupProjectImpl(oldProject, emptyList(), emptyList(), emptyList(),
                emptyList(), 0, ImmutableMap.of());

        when(mockProjectImportOptions.overwriteProjectDetails()).thenReturn(true);

        // Make the Monkey Project already exist.
        projectImportMapper.getProjectMapper().mapValue("12", "112");

        when(mockProjectImportPersister.updateProjectDetails(oldProject)).thenReturn(new MockProject());
        when(mockProjectImportPersister.createVersions(backupProject)).thenReturn(emptyMap());
        when(mockProjectImportPersister.createComponents(backupProject, projectImportMapper)).thenThrow(new DataAccessException("blah"));

        final DefaultProjectImportManager defaultProjectImportManager = new DefaultProjectImportManager(null, null, null, null, null, null, null,
                null, mockProjectImportPersister, null, null, null, null, null, null, null, null);

        exception.expect(AbortImportException.class);
        defaultProjectImportManager.importProject(mockProjectImportOptions, projectImportMapper, backupProject, new ProjectImportResultsImpl(0,
                0, 0, 0, null), null);

        verify(mockProjectImportPersister.updateProjectDetails(oldProject));
        verify(mockProjectImportPersister).updateProjectIssueCounter(backupProject, 23);
        verify(mockProjectImportPersister).createVersions(backupProject);
        verify(mockProjectImportPersister).createComponents(backupProject, projectImportMapper);
    }

    @Test
    public void testImportProject_CreateProjectAbortImportException() throws ExternalException, AbortImportException {
        final ExternalProject oldProject = new ExternalProject();
        oldProject.setId("4");
        oldProject.setCounter("23");
        final BackupProject backupProject = new BackupProjectImpl(oldProject, emptyList(), emptyList(), emptyList(),
                emptyList(), 0, ImmutableMap.of());

        when(mockProjectImportPersister.createProject(backupProject.getProject())).thenThrow(new ExternalException());

        final DefaultProjectImportManager defaultProjectImportManager = new DefaultProjectImportManager(null, null, null, null, null, null, null,
                null, mockProjectImportPersister, null, null, null, null, null, null, null, null);

        exception.expect(AbortImportException.class);
        defaultProjectImportManager.importProject(new ProjectImportOptionsImpl("", "", false), projectImportMapper, backupProject,
                new ProjectImportResultsImpl(0, 0, 0, 0, null), null);
    }

    @Test
    public void testDoImport() throws IndexException, IOException, SAXException {
        final MockI18nBean i18n = new MockI18nBean();
        final ProjectImportTemporaryFiles temporaryFiles = new MockProjectImportTemporaryFiles("TST");
        final ProjectImportOptions projectImportOptions = new ProjectImportOptionsImpl("/path", "/attach/path");

        final ExternalProject oldProject = new ExternalProject();
        oldProject.setId("4");
        final BackupProject backupProject = new BackupProjectImpl(oldProject, Collections.emptyList(), Collections.emptyList(), Collections.emptyList(),
                Collections.emptyList(), 0, ImmutableMap.of());

        final ChainedOfBizSaxHandler chainedOfBizSaxHandler = new ChainedOfBizSaxHandler();

        projectImportMapper.getProjectMapper().mapValue("4", "5");

        final ImmutableMultiset<String> entitySet = ImmutableMultiset.of(
                IssueParser.ISSUE_ENTITY_NAME, CustomFieldValueParser.CUSTOM_FIELD_VALUE_ENTITY_NAME,
                DatabaseWorklogStore.WORKLOG_ENTITY, OfBizLabelStore.TABLE, NodeAssociationParser.NODE_ASSOCIATION_ENTITY_NAME,
                IssueLinkParser.ISSUE_LINK_ENTITY_NAME, CommentParser.COMMENT_ENTITY_NAME, ChangeGroupParser.CHANGE_GROUP_ENTITY_NAME,
                ChangeItemParser.CHANGE_ITEM_ENTITY_NAME, UserAssociationParser.USER_ASSOCIATION_ENTITY_NAME,
                EntityPropertyParser.ENTITY_PROPERTY_ENTITY_NAME, AttachmentParser.ATTACHMENT_ENTITY_NAME,
                // TODO Second degree entities are parsed twice - investigate
                CommentParser.COMMENT_ENTITY_NAME, ChangeGroupParser.CHANGE_GROUP_ENTITY_NAME);

        when(mockCustomFieldManager.getCustomFieldTypes()).thenReturn(Collections.<CustomFieldType<?, ?>>emptyList());

        final DefaultProjectImportManager defaultProjectImportManager = new DefaultProjectImportManager(mockBackupXmlParser, null, null, null,
                mockIssueLinkManager, mockCustomFieldManager, null, null, mockProjectImportPersister1, null, null, null, null, null, null, mockPluginAccessor, Clock.systemUTC()) {
            int getTotalEntitiesCount() {
                return 1;
            }

            ChainedOfBizSaxHandler getOfBizChainedHandler(final TaskProgressProcessor taskProgressProcessor) {
                return chainedOfBizSaxHandler;
            }
        };

        final ProjectImportResults projectImportResults = new ProjectImportResultsImpl(System.currentTimeMillis(), 2, 1, 0, null);
        defaultProjectImportManager.doImport(projectImportOptions, new ProjectImportDataImpl(projectImportMapper, temporaryFiles, aoImportTemporaryFiles, 0, 0, 0, 0, 0, 0),
                backupProject, getBackupSystemInformationImpl(), projectImportResults, null, i18n, null);

        verify(mockIssueLinkManager).clearCache();
        verify(mockProjectImportPersister1).updateProjectIssueCounter(backupProject, 0);
        verify(mockProjectImportPersister1).reIndexProject(projectImportMapper, null, i18n);
        for (String entity : entitySet) {
            verify(mockBackupXmlParser, times(entitySet.count(entity))).parseOfBizBackupXml(temporaryFiles.getEntityXmlFile(entity).getAbsolutePath(), chainedOfBizSaxHandler);
        }
    }

    @Test
    public void testImportIssues_Error() throws IndexException, IOException, SAXException {
        final ProjectImportTemporaryFiles temporaryFiles = new MockProjectImportTemporaryFiles("TST");

        final ChainedOfBizSaxHandler chainedOfBizSaxHandler = new ChainedOfBizSaxHandler();

        projectImportMapper.getProjectMapper().mapValue("4", "5");

        doThrow(new AbortImportException()).when(mockBackupXmlParser).parseOfBizBackupXml(temporaryFiles.getEntityXmlFile("Issue").getAbsolutePath(), chainedOfBizSaxHandler);

        final DefaultProjectImportManager defaultProjectImportManager = new DefaultProjectImportManager(mockBackupXmlParser, null, null, null, null,
                null, null, null, null, null, null, null, null, null, null, mockPluginAccessor, null) {
            int getTotalEntitiesCount() {
                return 1;
            }

            ChainedOfBizSaxHandler getOfBizChainedHandler(final TaskProgressProcessor taskProgressProcessor) {
                return chainedOfBizSaxHandler;
            }

            BoundedExecutor createExecutor(final String threadName) {
                return mockBoundedExecutor;
            }
        };

        final ProjectImportResults projectImportResults = new ProjectImportResultsImpl(currentTimeMillis(), 2, 1, 0, null);
        // Make the results contain an error
        for (int i = 0; i < 10; i++) {
            projectImportResults.addError("error" + i);
        }

        exception.expect(AbortImportException.class);
        defaultProjectImportManager.importIssues(new ProjectImportDataImpl(projectImportMapper, temporaryFiles, aoImportTemporaryFiles, 0, 0, 0, 0, 0, 0),
                projectImportResults, i18n, null, projectImportMapper, null, null, null);

        verify(mockBoundedExecutor).shutdownAndIgnoreQueue();
    }

    @Test
    public void testImportIssueRelatedEntities_Error() throws IndexException, IOException, SAXException {
        final ProjectImportTemporaryFiles temporaryFiles = new MockProjectImportTemporaryFiles("TST");

        final ChainedOfBizSaxHandler chainedOfBizSaxHandler = new ChainedOfBizSaxHandler();

        projectImportMapper.getProjectMapper().mapValue("4", "5");

        doThrow(new AbortImportException()).when(mockBackupXmlParser).parseOfBizBackupXml(temporaryFiles.getEntityXmlFile("Worklog").getAbsolutePath(), chainedOfBizSaxHandler);

        final DefaultProjectImportManager defaultProjectImportManager = new DefaultProjectImportManager(mockBackupXmlParser, null, null, null, null,
                null, null, null, null, null, null, null, null, null, null, mockPluginAccessor, null) {
            int getTotalEntitiesCount() {
                return 1;
            }

            ChainedOfBizSaxHandler getOfBizChainedHandler(final TaskProgressProcessor taskProgressProcessor) {
                return chainedOfBizSaxHandler;
            }

            BoundedExecutor createExecutor(final String threadName) {
                return mockBoundedExecutor;
            }
        };

        final ProjectImportResults projectImportResults = new ProjectImportResultsImpl(currentTimeMillis(), 2, 1, 0, null);
        // Make the results contain an error
        for (int i = 0; i < 10; i++) {
            projectImportResults.addError("error" + i);
        }

        exception.expect(AbortImportException.class);
        defaultProjectImportManager.importIssueRelatedData(new ProjectImportDataImpl(projectImportMapper, temporaryFiles, aoImportTemporaryFiles, 0, 0, 0, 0, 0, 0), null,
                projectImportResults, i18n, null, projectImportMapper, null, null);

        verify(mockBoundedExecutor).shutdownAndIgnoreQueue();
    }

    @Test
    public void testImportChangeItemEntities_Error() throws IndexException, IOException, SAXException {
        final ProjectImportTemporaryFiles temporaryFiles = new MockProjectImportTemporaryFiles("TST");

        final ChainedOfBizSaxHandler chainedOfBizSaxHandler = new ChainedOfBizSaxHandler();

        projectImportMapper.getProjectMapper().mapValue("4", "5");

        doThrow(new AbortImportException()).when(mockBackupXmlParser).parseOfBizBackupXml(temporaryFiles.getEntityXmlFile("ChangeGroup").getAbsolutePath(), chainedOfBizSaxHandler);

        final DefaultProjectImportManager defaultProjectImportManager = new DefaultProjectImportManager(mockBackupXmlParser, null, null, null, null,
                null, null, null, null, null, null, null, null, null, null, mockPluginAccessor, null) {
            int getTotalEntitiesCount() {
                return 1;
            }

            ChainedOfBizSaxHandler getOfBizChainedHandler(final TaskProgressProcessor taskProgressProcessor) {
                return chainedOfBizSaxHandler;
            }

            BoundedExecutor createExecutor(final String threadName) {
                return mockBoundedExecutor;
            }
        };

        final ProjectImportResults projectImportResults = new ProjectImportResultsImpl(currentTimeMillis(), 2, 1, 0, null);
        // Make the results contain an error
        for (int i = 0; i < 10; i++) {
            projectImportResults.addError("error" + i);
        }

        exception.expect(AbortImportException.class);
        defaultProjectImportManager.importIssueSecondDegreeEntities(new ProjectImportDataImpl(projectImportMapper, temporaryFiles, aoImportTemporaryFiles, 0, 0, 0, 0, 0, 0),
                projectImportResults, i18n, projectImportMapper, null, null, null);

        verify(mockBackupXmlParser).parseOfBizBackupXml(temporaryFiles.getEntityXmlFile("Action").getAbsolutePath(), chainedOfBizSaxHandler);
        verify(mockBackupXmlParser).parseOfBizBackupXml(temporaryFiles.getEntityXmlFile("ChangeItem").getAbsolutePath(), chainedOfBizSaxHandler);
        verify(mockBoundedExecutor).shutdownAndIgnoreQueue();
    }

    @Test
    public void testImportCustomFieldValues_Error() throws IndexException, IOException, SAXException {
        final ProjectImportTemporaryFiles temporaryFiles = new MockProjectImportTemporaryFiles("TST");

        final ExternalProject oldProject = new ExternalProject();
        oldProject.setId("4");
        final BackupProject backupProject = new BackupProjectImpl(oldProject, emptyList(), emptyList(), emptyList(),
                emptyList(), 0, ImmutableMap.of());

        final ChainedOfBizSaxHandler chainedOfBizSaxHandler = new ChainedOfBizSaxHandler();

        projectImportMapper.getProjectMapper().mapValue("4", "5");

        doThrow(new AbortImportException()).when(mockBackupXmlParser).parseOfBizBackupXml(temporaryFiles.getEntityXmlFile("CustomFieldValue").getAbsolutePath(), chainedOfBizSaxHandler);

        when(mockCustomFieldManager.getCustomFieldTypes()).thenReturn(emptyList());

        final DefaultProjectImportManager defaultProjectImportManager = new DefaultProjectImportManager(mockBackupXmlParser, null, null, null, null,
                mockCustomFieldManager, null, null, null, null, null, null, null, null, null, mockPluginAccessor, null) {
            int getTotalEntitiesCount() {
                return 1;
            }

            ChainedOfBizSaxHandler getOfBizChainedHandler(final TaskProgressProcessor taskProgressProcessor) {
                return chainedOfBizSaxHandler;
            }

            BoundedExecutor createExecutor(final String threadName) {
                return mockBoundedExecutor;
            }
        };

        final ProjectImportResults projectImportResults = new ProjectImportResultsImpl(currentTimeMillis(), 2, 1, 0, null);
        // Make the results contain an error
        for (int i = 0; i < 10; i++) {
            projectImportResults.addError("error" + i);
        }

        exception.expect(AbortImportException.class);
        defaultProjectImportManager.importCustomFieldValues(new ProjectImportDataImpl(projectImportMapper, temporaryFiles, aoImportTemporaryFiles, 0, 0, 0, 0, 0, 0),
                backupProject, null, projectImportResults, i18n, projectImportMapper, null);

        verify(mockBoundedExecutor).shutdownAndIgnoreQueue();
    }

    @Test
    public void testImportAttachments_Error() throws IndexException, IOException, SAXException {
        final ProjectImportTemporaryFiles temporaryFiles = new MockProjectImportTemporaryFiles("TST");

        final ProjectImportOptions projectImportOptions = new ProjectImportOptionsImpl("/path", "/attach/path");

        final ExternalProject oldProject = new ExternalProject();
        oldProject.setId("4");
        final BackupProject backupProject = new BackupProjectImpl(oldProject, emptyList(), emptyList(), emptyList(),
                emptyList(), 0, ImmutableMap.of());

        final ChainedOfBizSaxHandler chainedOfBizSaxHandler = new ChainedOfBizSaxHandler();

        projectImportMapper.getProjectMapper().mapValue("4", "5");

        doThrow(new AbortImportException()).when(mockBackupXmlParser).parseOfBizBackupXml(temporaryFiles.getEntityXmlFile("FileAttachment").getAbsolutePath(), chainedOfBizSaxHandler);

        final DefaultProjectImportManager defaultProjectImportManager = new DefaultProjectImportManager(mockBackupXmlParser, null, null, null, null,
                null, null, null, null, null, null, null, null, null, null, mockPluginAccessor, null) {
            int getTotalEntitiesCount() {
                return 1;
            }

            ChainedOfBizSaxHandler getOfBizChainedHandler(final TaskProgressProcessor taskProgressProcessor) {
                return chainedOfBizSaxHandler;
            }

            BoundedExecutor createExecutor(final String threadName) {
                return mockBoundedExecutor;
            }
        };

        final ProjectImportResults projectImportResults = new ProjectImportResultsImpl(currentTimeMillis(), 2, 1, 0, null);
        // Make the results contain an error
        for (int i = 0; i < 10; i++) {
            projectImportResults.addError("error" + i);
        }

        exception.expect(AbortImportException.class);
        defaultProjectImportManager.importAttachments(projectImportOptions, new ProjectImportDataImpl(projectImportMapper, temporaryFiles, aoImportTemporaryFiles, 0, 0,
                0, 0, 0, 0), backupProject, null, projectImportResults, i18n, null, projectImportMapper);

        verify(mockBoundedExecutor).shutdownAndIgnoreQueue();
    }

    @Test
    public void testImportProjectRoleMembersGroups() throws Exception {
        when(mockGroupManager.groupExists("dudes")).thenReturn(true);
        when(mockGroupManager.groupExists("dudettes")).thenReturn(false);

        final EntityRepresentation dudeEntityRepresentation = new EntityRepresentationImpl("X", new HashMap());

        when(mockProjectImportPersister.createEntity(dudeEntityRepresentation)).thenReturn(34L);

        final ProjectImportMapper projectImportMapper = new ProjectImportMapperImpl(null, mockGroupManager);
        projectImportMapper.getProjectRoleActorMapper().flagValueActorAsInUse(
                new ExternalProjectRoleActor("1", "2", "3", GROUP_ROLE_ACTOR_TYPE, "dudes"));
        projectImportMapper.getProjectRoleActorMapper().flagValueActorAsInUse(
                new ExternalProjectRoleActor("1", "2", "3", GROUP_ROLE_ACTOR_TYPE, "dudettes"));
        // Set up mapped IDs
        projectImportMapper.getProjectRoleMapper().registerOldValue("3", "My Role");
        projectImportMapper.getProjectMapper().mapValue("2", "12");
        projectImportMapper.getProjectRoleMapper().mapValue("3", "13");

        when(mockProjectRoleActorParser.getEntityRepresentation(new ExternalProjectRoleActor(null, "12", "13", GROUP_ROLE_ACTOR_TYPE, "dudes"))).thenReturn(dudeEntityRepresentation);

        final DefaultProjectImportManager defaultProjectImportManager = new DefaultProjectImportManager(null, null, null, null, null, null, null,
                null, mockProjectImportPersister, null, mockGroupManager, mockProjectRoleManager, null, null, null, null, null) {
            ProjectRoleActorParser getProjectRoleActorParser() {
                return mockProjectRoleActorParser;
            }
        };
        final ProjectImportResultsImpl projectImportResults = new ProjectImportResultsImpl(0, 0, 0, 0, new MockI18nBean());
        defaultProjectImportManager.importProjectRoleMembers(null, projectImportMapper, projectImportResults, null);

        assertThat(projectImportResults.getRoles(), hasSize(1));
        assertThat(projectImportResults.getRoles(), hasItem("My Role"));
        assertEquals(1, projectImportResults.getGroupsCreatedCountForRole("My Role"));
    }

    @Test
    public void testImportProjectRoleMembersGroupsReportError() throws Exception {
        when(mockGroupManager.groupExists("dudes")).thenReturn(true);
        when(mockGroupManager.groupExists("dudettes")).thenReturn(false);

        final EntityRepresentation dudeEntityRepresentation = new EntityRepresentationImpl("X", new HashMap());

        when(mockProjectImportPersister.createEntity(dudeEntityRepresentation)).thenReturn(null);

        projectImportMapper.getProjectRoleActorMapper().flagValueActorAsInUse(
                new ExternalProjectRoleActor("1", "2", "3", GROUP_ROLE_ACTOR_TYPE, "dudes"));
        projectImportMapper.getProjectRoleActorMapper().flagValueActorAsInUse(
                new ExternalProjectRoleActor("1", "2", "3", GROUP_ROLE_ACTOR_TYPE, "dudettes"));
        // Set up mapped IDs
        projectImportMapper.getProjectRoleMapper().registerOldValue("3", "My Role");
        projectImportMapper.getProjectMapper().mapValue("2", "12");
        projectImportMapper.getProjectRoleMapper().mapValue("3", "13");

        when(mockProjectRoleActorParser.getEntityRepresentation(new ExternalProjectRoleActor(null, "12", "13", GROUP_ROLE_ACTOR_TYPE, "dudes"))).thenReturn(dudeEntityRepresentation);

        final DefaultProjectImportManager defaultProjectImportManager = new DefaultProjectImportManager(null, null, null, null, null, null, null,
                null, mockProjectImportPersister, null, mockGroupManager, mockProjectRoleManager, null, null, null, null, null) {
            ProjectRoleActorParser getProjectRoleActorParser() {
                return mockProjectRoleActorParser;
            }
        };
        final ProjectImportResultsImpl projectImportResults = new ProjectImportResultsImpl(0, 0, 0, 0, new MockI18nBean());
        defaultProjectImportManager.importProjectRoleMembers(null, projectImportMapper, projectImportResults, null);

        assertThat(projectImportResults.getRoles(), hasSize(0));
        assertFalse(projectImportResults.getRoles().contains("My Role"));
        assertEquals(0, projectImportResults.getGroupsCreatedCountForRole("My Role"));
        assertThat(projectImportResults.getErrors(), hasSize(1));
        assertEquals("There was an error adding group 'dudes' to the Project Role 'My Role'.", projectImportResults.getErrors().iterator().next());
    }

    @Test
    public void testImportProjectRoleMembersGroupsReportErrorAndAbort() throws Exception {
        final ProjectImportMapper projectImportMapper = new ProjectImportMapperImpl(null, mockGroupManager);
        projectImportMapper.getProjectRoleActorMapper().flagValueActorAsInUse(
                new ExternalProjectRoleActor("1", "2", "3", GROUP_ROLE_ACTOR_TYPE, "dudes"));
        projectImportMapper.getProjectRoleActorMapper().flagValueActorAsInUse(
                new ExternalProjectRoleActor("1", "2", "3", GROUP_ROLE_ACTOR_TYPE, "dudettes"));
        // Set up mapped IDs
        projectImportMapper.getProjectRoleMapper().registerOldValue("3", "My Role");
        projectImportMapper.getProjectMapper().mapValue("2", "12");
        projectImportMapper.getProjectRoleMapper().mapValue("3", "13");

        final DefaultProjectImportManager defaultProjectImportManager = new DefaultProjectImportManager(null, null, null, null, null, null, null,
                null, mockProjectImportPersister, null, mockGroupManager, mockProjectRoleManager, null, null, null, null, null) {
            ProjectRoleActorParser getProjectRoleActorParser() {
                return mockProjectRoleActorParser;
            }

            BoundedExecutor createExecutor(final String threadName) {
                return mockBoundedExecutor;
            }
        };
        final ProjectImportResultsImpl projectImportResults = new ProjectImportResultsImpl(0, 0, 0, 0, new MockI18nBean());
        // Add 9 errors ahead of time
        for (int i = 0; i < 10; i++) {
            projectImportResults.addError("error" + i);
        }

        exception.expect(AbortImportException.class);
        defaultProjectImportManager.importProjectRoleMembers(null, projectImportMapper, projectImportResults, null);

        assertThat(projectImportResults.getRoles(), hasSize(0));
        assertFalse(projectImportResults.getRoles().contains("My Role"));
        assertEquals(0, projectImportResults.getGroupsCreatedCountForRole("My Role"));

        verify(mockBoundedExecutor).shutdownAndWait();
    }

    @Test
    public void testImportProjectRoleMembersUsers() throws Exception {
        final ExternalUser peter = new ExternalUser("peterKey", "peter", "Peter", "", "");
        final ExternalUser paul = new ExternalUser("paulKey", "paul", "Paul", "", "");

        when(mockUserUtil.userExists("peter")).thenReturn(true);
        when(mockUserUtil.userExists("paul")).thenReturn(false);
        when(mockUserUtil.getUserByName("peter")).thenReturn(new MockApplicationUser("someKey", "peter"));

        final EntityRepresentation paulEntityRepresentation = new EntityRepresentationImpl("X", new HashMap());

        when(mockProjectImportPersister.createEntity(paulEntityRepresentation)).thenReturn(34L);

        final ProjectImportMapper projectImportMapper = new ProjectImportMapperImpl(mockUserUtil, null);
        projectImportMapper.getProjectRoleActorMapper().flagValueActorAsInUse(
                new ExternalProjectRoleActor("1", "2", "3", USER_ROLE_ACTOR_TYPE, "peterKey"));
        projectImportMapper.getProjectRoleActorMapper().flagValueActorAsInUse(
                new ExternalProjectRoleActor("1", "2", "3", USER_ROLE_ACTOR_TYPE, "paulKey"));
        // random one should be ignored with a log message.
        projectImportMapper.getProjectRoleActorMapper().flagValueActorAsInUse(new ExternalProjectRoleActor("1", "2", "3", "weird", "Joe"));
        // Set up mapped IDs
        projectImportMapper.getProjectRoleMapper().registerOldValue("3", "My Role");
        projectImportMapper.getProjectMapper().mapValue("2", "12");
        projectImportMapper.getProjectRoleMapper().mapValue("3", "13");
        projectImportMapper.getUserMapper().registerOldValue(peter);
        projectImportMapper.getUserMapper().registerOldValue(paul);

        when(mockProjectRoleActorParser.getEntityRepresentation(new ExternalProjectRoleActor(null, "12", "13", USER_ROLE_ACTOR_TYPE, "someKey"))).thenReturn(paulEntityRepresentation);

        final DefaultProjectImportManager defaultProjectImportManager = new DefaultProjectImportManager(null, null, null, null, null, null, null,
                null, mockProjectImportPersister, mockUserUtil, null, mockProjectRoleManager, null, null, null, null, null) {
            ProjectRoleActorParser getProjectRoleActorParser() {
                return mockProjectRoleActorParser;
            }
        };
        final ProjectImportResultsImpl projectImportResults = new ProjectImportResultsImpl(0, 0, 0, 0, new MockI18nBean());
        defaultProjectImportManager.importProjectRoleMembers(null, projectImportMapper, projectImportResults, null);

        assertThat(projectImportResults.getRoles(), hasSize(1));
        assertThat(projectImportResults.getRoles(), hasItem("My Role"));
        assertEquals(1, projectImportResults.getUsersCreatedCountForRole("My Role"));
    }

    @Test
    public void testImportProjectRoleMembersUsersReportError() throws Exception {
        final ExternalUser peter = new ExternalUser("peterKey", "peter", "Peter", "", "");
        final ExternalUser paul = new ExternalUser("paulKey", "paul", "Paul", "", "");

        when(mockUserUtil.userExists("peter")).thenReturn(true);
        when(mockUserUtil.userExists("paul")).thenReturn(false);
        when(mockUserUtil.getUserByName("peter")).thenReturn(new MockApplicationUser("someKey", "peter"));

        final EntityRepresentation paulEntityRepresentation = new EntityRepresentationImpl("X", new HashMap());

        when(mockProjectImportPersister.createEntity(paulEntityRepresentation)).thenReturn(null);

        final ProjectImportMapper projectImportMapper = new ProjectImportMapperImpl(mockUserUtil, null);
        projectImportMapper.getProjectRoleActorMapper().flagValueActorAsInUse(
                new ExternalProjectRoleActor("1", "2", "3", USER_ROLE_ACTOR_TYPE, "peterKey"));
        projectImportMapper.getProjectRoleActorMapper().flagValueActorAsInUse(
                new ExternalProjectRoleActor("1", "2", "3", USER_ROLE_ACTOR_TYPE, "paulKey"));
        // random one should be ignored with a log message.
        projectImportMapper.getProjectRoleActorMapper().flagValueActorAsInUse(new ExternalProjectRoleActor("1", "2", "3", "weird", "Joe"));
        // Set up mapped IDs
        projectImportMapper.getProjectRoleMapper().registerOldValue("3", "My Role");
        projectImportMapper.getProjectMapper().mapValue("2", "12");
        projectImportMapper.getProjectRoleMapper().mapValue("3", "13");
        projectImportMapper.getUserMapper().registerOldValue(peter);
        projectImportMapper.getUserMapper().registerOldValue(paul);

        when(mockProjectRoleActorParser.getEntityRepresentation(new ExternalProjectRoleActor(null, "12", "13", USER_ROLE_ACTOR_TYPE, "someKey"))).thenReturn(paulEntityRepresentation);

        final DefaultProjectImportManager defaultProjectImportManager = new DefaultProjectImportManager(null, null, null, null, null, null, null,
                null, mockProjectImportPersister, mockUserUtil, null, mockProjectRoleManager, null, null, null, null, null) {
            ProjectRoleActorParser getProjectRoleActorParser() {
                return mockProjectRoleActorParser;
            }
        };
        final ProjectImportResultsImpl projectImportResults = new ProjectImportResultsImpl(0, 0, 0, 0, new MockI18nBean());
        defaultProjectImportManager.importProjectRoleMembers(null, projectImportMapper, projectImportResults, null);

        assertThat(projectImportResults.getRoles(), hasSize(0));
        assertFalse(projectImportResults.getRoles().contains("My Role"));
        assertEquals(0, projectImportResults.getUsersCreatedCountForRole("My Role"));
        assertThat(projectImportResults.getErrors(), hasSize(1));
        assertEquals("There was an error adding user 'peter' to the Project Role 'My Role'.", projectImportResults.getErrors().iterator().next());
    }

    @Test
    public void testGetBackupOverview() throws IOException, SAXException {
        final String filename = "/somerubbish";

        doReturn(1).when(defaultProjectImportManager).getTotalEntitiesCount();

        final BackupOverview backupOverview = defaultProjectImportManager.getBackupOverview(filename, null, new MockI18nBean());

        assertNotNull(backupOverview);
        verify(mockBackupXmlParser).parseOfBizBackupXml(eq(filename), Matchers.<DefaultHandler>any());
    }

    @Nonnull
    private File createTempDirectory() throws IOException {
        File baseDir = new File(System.getProperty("java.io.tmpdir"));
        String name = "Test" + System.currentTimeMillis();
        File dir = new File(baseDir, name);
        dir.mkdir();
        return dir;
    }
}
