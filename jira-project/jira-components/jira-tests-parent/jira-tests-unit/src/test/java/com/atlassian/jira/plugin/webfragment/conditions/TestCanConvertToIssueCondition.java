package com.atlassian.jira.plugin.webfragment.conditions;

import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.bc.JiraServiceContextImpl;
import com.atlassian.jira.bc.subtask.conversion.SubTaskToIssueConversionService;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.SimpleErrorCollection;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestCanConvertToIssueCondition {
    @Test
    public void testTrue() {
        final Issue issue = mock(Issue.class);
        final SubTaskToIssueConversionService conversionService = mock(SubTaskToIssueConversionService.class);
        final JiraServiceContext context = new JiraServiceContextImpl((ApplicationUser) null, new SimpleErrorCollection());
        when(conversionService.canConvertIssue(context, issue)).thenReturn(true);

        final CanConvertToIssueCondition condition = new CanConvertToIssueCondition(conversionService);

        assertTrue(condition.shouldDisplay(null, issue, null));
    }

    @Test
    public void testFalse() {
        final Issue issue = mock(Issue.class);
        final SubTaskToIssueConversionService conversionService = mock(SubTaskToIssueConversionService.class);
        final JiraServiceContext context = new JiraServiceContextImpl((ApplicationUser) null, new SimpleErrorCollection());
        when(conversionService.canConvertIssue(context, issue)).thenReturn(false);

        final CanConvertToIssueCondition condition = new CanConvertToIssueCondition(conversionService);

        assertFalse(condition.shouldDisplay(null, issue, null));
    }
}
