package com.atlassian.jira.project.type;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;
import static org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class TestProjectTypeKeyFormatter {
    private String input;
    private String expected;

    public TestProjectTypeKeyFormatter(String input, String expected) {
        this.input = input;
        this.expected = expected;
    }

    @Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {null, "Unknown"},
                {"", "Unknown"},
                {"_", ""},
                {"business", "Business"},
                {"service_desk", "Service Desk"},
                {"one_two_three", "One Two Three"},
                {"service_", "Service"},
                {"_service", "Service"},
                {"_service_", "Service"},
        });
    }

    @Test
    public void formatsProjectTypeKeysAsExpected() {
        assertEquals(expected, ProjectTypeKeyFormatter.format(new ProjectTypeKey(input)));
    }
}
