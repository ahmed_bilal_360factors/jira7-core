package com.atlassian.jira.issue.fields.csv.custom;

import com.atlassian.jira.action.issue.customfields.option.MockOption;
import com.atlassian.jira.issue.customfields.impl.CascadingSelectCFType;
import com.atlassian.jira.issue.customfields.option.Option;
import com.google.common.collect.Maps;
import org.junit.Test;

import java.util.Map;

public class CascadingSelectCFTypeCsvExportTest extends CustomFieldSingleValueCsvExporterTest<Map<String, Option>> {
    @Override
    protected CascadingSelectCFType createField() {
        return new CascadingSelectCFType(null, null, null, null);
    }

    @Test
    public void correctHierarchyIsExportedForParentAndChild() {
        Map<String, Option> value = Maps.newHashMap();
        value.put(CascadingSelectCFType.PARENT_KEY, MockOption.value("a"));
        value.put(CascadingSelectCFType.CHILD_KEY, MockOption.value("b"));
        whenFieldValueIs(value);

        assertExportedValue("a -> b");
    }

    @Test
    public void correctHierarchyIsExportedForParentOnly() {
        Map<String, Option> value = Maps.newHashMap();
        value.put(CascadingSelectCFType.PARENT_KEY, MockOption.value("a"));
        whenFieldValueIs(value);

        assertExportedValue("a");
    }


}
