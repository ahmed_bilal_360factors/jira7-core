package com.atlassian.jira.web.action.admin.workflow.tabs;

import com.opensymphony.workflow.loader.ActionDescriptor;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Map;

import static com.atlassian.jira.web.action.admin.workflow.tabs.WorkflowTransitionContextUtils.TRANSITION_KEY;
import static java.util.Collections.singletonList;
import static java.util.Collections.singletonMap;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestWorkflowTransitionOtherCondition {

    @Mock private ActionDescriptor transition;

    private WorkflowTransitionOtherCondition condition;

    private Map<String, Object> context;

    @Before
    public void setUp() {
        condition = new WorkflowTransitionOtherCondition();
        context = singletonMap(TRANSITION_KEY, transition);
    }

    @Test
    public void shouldNotDisplayIfContextDoesNotContainTransition() {
        // Invoke
        final boolean shouldDisplay = condition.shouldDisplay(singletonMap(TRANSITION_KEY, "foo"));

        // Check
        assertThat(shouldDisplay, is(false));
    }

    @Test
    public void shouldDisplayIfTransitionHasConditionalResults() {
        // Set up
        final Object conditionalResult = mock(Object.class);
        when(transition.getConditionalResults()).thenReturn(singletonList(conditionalResult));

        // Invoke
        final boolean shouldDisplay = condition.shouldDisplay(context);

        // Check
        assertThat(shouldDisplay, is(true));
    }
}
