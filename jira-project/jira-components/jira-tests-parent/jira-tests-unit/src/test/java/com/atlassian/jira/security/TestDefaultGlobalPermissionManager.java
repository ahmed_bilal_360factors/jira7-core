package com.atlassian.jira.security;

import com.atlassian.cache.CacheManager;
import com.atlassian.cache.memory.MemoryCacheManager;
import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.crowd.exception.CrowdException;
import com.atlassian.crowd.exception.OperationNotPermittedException;
import com.atlassian.crowd.exception.embedded.InvalidGroupException;
import com.atlassian.fugue.Option;
import com.atlassian.jira.application.ApplicationRoleManager;
import com.atlassian.jira.config.CoreFeatures;
import com.atlassian.jira.event.MockEventPublisher;
import com.atlassian.jira.exception.CreateException;
import com.atlassian.jira.exception.RemoveException;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.license.LicenseCountService;
import com.atlassian.jira.mock.MockFeatureManager;
import com.atlassian.jira.mock.ofbiz.MockOfBizDelegator;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.permission.GlobalPermissionType;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.security.groups.MockGroupManager;
import com.atlassian.jira.security.plugin.GlobalPermissionTypesManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.MockCrowdService;
import com.atlassian.jira.user.MockGroup;
import com.atlassian.jira.user.util.StaticRecoveryMode;
import com.google.common.collect.Lists;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.RuleChain;
import org.mockito.Mock;
import org.mockito.Mockito;

import static com.atlassian.fugue.Option.some;
import static com.atlassian.jira.security.Permissions.ADMINISTER;
import static com.atlassian.jira.security.Permissions.ATTACHMENT_DELETE_ALL;
import static com.atlassian.jira.security.Permissions.ATTACHMENT_DELETE_OWN;
import static com.atlassian.jira.security.Permissions.BULK_CHANGE;
import static com.atlassian.jira.security.Permissions.COMMENT_DELETE_ALL;
import static com.atlassian.jira.security.Permissions.COMMENT_DELETE_OWN;
import static com.atlassian.jira.security.Permissions.COMMENT_EDIT_ALL;
import static com.atlassian.jira.security.Permissions.COMMENT_EDIT_OWN;
import static com.atlassian.jira.security.Permissions.CREATE_SHARED_OBJECTS;
import static com.atlassian.jira.security.Permissions.MANAGE_GROUP_FILTER_SUBSCRIPTIONS;
import static com.atlassian.jira.security.Permissions.SYSTEM_ADMIN;
import static com.atlassian.jira.security.Permissions.USE;
import static com.atlassian.jira.security.Permissions.USER_PICKER;
import static com.atlassian.jira.security.Permissions.WORKLOG_DELETE_ALL;
import static com.atlassian.jira.security.Permissions.WORKLOG_DELETE_OWN;
import static com.atlassian.jira.security.Permissions.WORKLOG_EDIT_ALL;
import static com.atlassian.jira.security.Permissions.WORKLOG_EDIT_OWN;
import static com.google.common.collect.ImmutableList.of;
import static com.google.common.collect.Sets.newHashSet;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestDefaultGlobalPermissionManager {
    private static final String RECOVERY_USER = "recovery_user";

    @Rule
    public final RuleChain mockitoMocks = MockitoMocksInContainer.forTest(this);

    @Rule
    public final ExpectedException exception = ExpectedException.none();
    protected MockCrowdService crowdService;
    protected MockEventPublisher eventPublisher;
    protected DefaultGlobalPermissionManager globalPermissionManager;
    @Mock
    @AvailableInContainer
    private LicenseCountService licenseCountService;
    @Mock
    private GlobalPermissionTypesManager globalPermissionTypesManager;
    @Mock
    private CacheManager cacheManager;
    private GroupManager groupManager;

    private MockFeatureManager featureManager = new MockFeatureManager();
    @Mock
    private ApplicationRoleManager applicationRoleManager;

    @Before
    public void setUp() {
        crowdService = new MockCrowdService();
        eventPublisher = new MockEventPublisher();
        groupManager = new MockGroupManager();
        when(globalPermissionTypesManager.getAll()).thenReturn(MockGlobalPermissionTypeManager.SYSTEM_PERMISSIONS);
        globalPermissionManager = new DefaultGlobalPermissionManager(crowdService, new MockOfBizDelegator(),
                eventPublisher, globalPermissionTypesManager, new MemoryCacheManager(), applicationRoleManager,
                groupManager, new StaticRecoveryMode(RECOVERY_USER), featureManager);

        when(globalPermissionTypesManager.getGlobalPermission(GlobalPermissionKey.USE)).thenReturn(Option.option(new GlobalPermissionType(GlobalPermissionKey.USE.getKey(), null, null, false)));
        when(globalPermissionTypesManager.getGlobalPermission(GlobalPermissionKey.ADMINISTER)).thenReturn(Option.option(new GlobalPermissionType(GlobalPermissionKey.ADMINISTER.getKey(), null, null, false)));
        when(globalPermissionTypesManager.getGlobalPermission(GlobalPermissionKey.SYSTEM_ADMIN)).thenReturn(Option.option(new GlobalPermissionType(GlobalPermissionKey.SYSTEM_ADMIN.getKey(), null, null, false)));
        when(globalPermissionTypesManager.getGlobalPermission(GlobalPermissionKey.BULK_CHANGE)).thenReturn(Option.option(new GlobalPermissionType(GlobalPermissionKey.BULK_CHANGE.getKey(), null, null, true)));
        when(globalPermissionTypesManager.getGlobalPermission(GlobalPermissionKey.MANAGE_GROUP_FILTER_SUBSCRIPTIONS)).thenReturn(Option.option(new GlobalPermissionType(GlobalPermissionKey.MANAGE_GROUP_FILTER_SUBSCRIPTIONS.getKey(), null, null, true)));
    }

    @Test
    public void canAddToNullGroup() throws Exception {
        globalPermissionManager.addPermission(new GlobalPermissionType("Foo", null, null, true), null);
    }

    @Test
    public void hasPermission_Legacy() throws Exception {
        // user1 belongs to group1
        final ApplicationUser user1 = new MockApplicationUser("anne");
        final Group group1 = new MockGroup("group1");
        groupManager.addUserToGroup(user1, group1);
        // user2 belongs to group2
        final ApplicationUser user2 = new MockApplicationUser("bob");
        final Group group2 = new MockGroup("group2");
        groupManager.addUserToGroup(user2, group2);

        assertFalse(globalPermissionManager.hasPermission(Permissions.ADMINISTER, user1));
        globalPermissionManager.addPermission(Permissions.ADMINISTER, "group1");
        assertTrue(globalPermissionManager.hasPermission(Permissions.ADMINISTER, user1));
        assertFalse(globalPermissionManager.hasPermission(Permissions.ADMINISTER, user2));

        assertFalse(globalPermissionManager.hasPermission(Permissions.MANAGE_GROUP_FILTER_SUBSCRIPTIONS, user2));
        globalPermissionManager.addPermission(Permissions.MANAGE_GROUP_FILTER_SUBSCRIPTIONS, "group2");
        assertTrue(globalPermissionManager.hasPermission(Permissions.MANAGE_GROUP_FILTER_SUBSCRIPTIONS, user2));
        assertFalse(globalPermissionManager.hasPermission(Permissions.MANAGE_GROUP_FILTER_SUBSCRIPTIONS, user1));
    }

    @Test
    public void userWithUsePermission() throws Exception {
        ApplicationUser anne = new MockApplicationUser("anne");

        globalPermissionManager.hasPermission(Permissions.USE, anne);
        verify(applicationRoleManager).hasAnyRole(anne);
    }


    @Test(expected = UnsupportedOperationException.class)
    public void addingUsePermissionShouldFail() throws CrowdException {
        globalPermissionManager.addPermission(globalPermissionManager.getGlobalPermission(GlobalPermissionKey.USE).get(), "somegroupname");
    }

    @Test
    public void userWithoutUsePermission() throws Exception {
        final ApplicationUser user2 = new MockApplicationUser("bob");
        final Group group2 = new MockGroup("group2");
        groupManager.addUserToGroup(user2, group2);
        final Group group1 = new MockGroup("group1");
        when(applicationRoleManager.getGroupsForLicensedRoles()).thenReturn(newHashSet(group1));
        assertFalse(globalPermissionManager.hasPermission(Permissions.USE, user2));
    }

    @Test
    public void hasPermission() throws Exception {
        // user1 belongs to group1
        final ApplicationUser user1 = new MockApplicationUser("anne");
        final Group group1 = new MockGroup("group1");
        groupManager.addUserToGroup(user1, group1);

        // user2 belongs to group2
        final ApplicationUser user2 = new MockApplicationUser("bob");
        final Group group2 = new MockGroup("group2");
        groupManager.addUserToGroup(user2, group2);

        assertFalse(globalPermissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, user1));
        globalPermissionManager.addPermission(globalPermissionManager.getGlobalPermission(GlobalPermissionKey.ADMINISTER).get(), "group1");
        assertTrue(globalPermissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, user1));
        assertFalse(globalPermissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, user2));

        assertFalse(globalPermissionManager.hasPermission(GlobalPermissionKey.MANAGE_GROUP_FILTER_SUBSCRIPTIONS, user2));
        globalPermissionManager.addPermission(globalPermissionManager.getGlobalPermission(GlobalPermissionKey.MANAGE_GROUP_FILTER_SUBSCRIPTIONS).get(), "group2");
        assertTrue(globalPermissionManager.hasPermission(GlobalPermissionKey.MANAGE_GROUP_FILTER_SUBSCRIPTIONS, user2));
        assertFalse(globalPermissionManager.hasPermission(GlobalPermissionKey.MANAGE_GROUP_FILTER_SUBSCRIPTIONS, user1));
    }

    @Test
    public void testSysAdminImpliesAdminHack() throws Exception {
        // user1 belongs to group1
        final ApplicationUser bob = new MockApplicationUser("bob");
        final Group group1 = new MockGroup("group1");
        groupManager.addUserToGroup(bob, group1);
        // user2 belongs to group2
        final ApplicationUser joe = new MockApplicationUser("joe");
        final Group group2 = new MockGroup("group2");
        groupManager.addUserToGroup(joe, group2);

        // Add bob's group to the SYSTEM_ADMIN global permission
        globalPermissionManager.addPermission(Permissions.SYSTEM_ADMIN, "group1");
        // Add joe's group to the ADMIN global permission
        globalPermissionManager.addPermission(Permissions.ADMINISTER, "group2");

        // Verify that asking for bob in the ADMIN global role returns true
        assertTrue(globalPermissionManager.hasPermission(Permissions.ADMINISTER, bob));
        // Make sure that the explict call for SYS_ADMIN is correct as well
        assertTrue(globalPermissionManager.hasPermission(Permissions.SYSTEM_ADMIN, bob));

        // Verify that joe is an Admin
        assertTrue(globalPermissionManager.hasPermission(Permissions.ADMINISTER, joe));
        // Verify that joe is not a Sys Admin
        assertFalse(globalPermissionManager.hasPermission(Permissions.SYSTEM_ADMIN, joe));
    }

    @Test
    public void testAddUsePermissionClearsUserCount() throws CreateException {
        globalPermissionManager.addPermission(Permissions.SYSTEM_ADMIN, "group1");
        verify(licenseCountService).flush();
    }

    @Test
    public void testAddNonUsePermissionDoesNotClearUserCount() throws CreateException {
        globalPermissionManager.addPermission(Permissions.BULK_CHANGE, "group1");
        verify(licenseCountService, never()).flush();
    }

    @Test
    public void testRemovePermissionWithUserLimit() throws RemoveException, CreateException {
        //let's create a permission first.
        globalPermissionManager.addPermission(Permissions.ADMINISTER, "group1");

        verify(licenseCountService, times(1)).flush();
        globalPermissionManager.removePermission(Permissions.ADMINISTER, "group1");
        verify(licenseCountService, times(2)).flush();
    }

    @Test
    public void testRemoveNonUsePermissionWithUserLimit() throws RemoveException, CreateException {
        //let's create a permission first.
        globalPermissionManager.addPermission(Permissions.BULK_CHANGE, "group1");

        globalPermissionManager.removePermission(Permissions.BULK_CHANGE, "group1");
        verify(licenseCountService, never()).flush();
    }

    @Test
    public void testRemovePermissionsWithLimitedLicense()
            throws CreateException, RemoveException, OperationNotPermittedException, InvalidGroupException {
        crowdService.addGroup(new MockGroup("group1"));
        //let's create a permission first.
        globalPermissionManager.addPermission(Permissions.ADMINISTER, "group1");
        globalPermissionManager.addPermission(Permissions.SYSTEM_ADMIN, "group1");
        globalPermissionManager.addPermission(Permissions.BULK_CHANGE, "group1");


        verify(licenseCountService, times(2)).flush();
        globalPermissionManager.removePermissions("group1");
        verify(licenseCountService, times(4)).flush();
    }

    @Test(expected = UnsupportedOperationException.class)
    public void removeUsePermissionAsIntShouldFail() {
        crowdService.addGroup(new MockGroup("abc"));
        globalPermissionManager.removePermission(Permissions.USE, "abc");
    }

    @Test(expected = UnsupportedOperationException.class)
    public void removeUsePermissionAsTypeShouldFail() {
        crowdService.addGroup(new MockGroup("abc"));

        GlobalPermissionType usePermission = globalPermissionManager.getGlobalPermission(GlobalPermissionKey.USE).get();
        globalPermissionManager.removePermission(usePermission, "abc");
    }

    @Test
    public void testIsGlobalPermission() {
        assertFalse(globalPermissionManager.isGlobalPermission(COMMENT_EDIT_ALL));
        assertFalse(globalPermissionManager.isGlobalPermission(COMMENT_EDIT_OWN));
        assertFalse(globalPermissionManager.isGlobalPermission(COMMENT_DELETE_ALL));
        assertFalse(globalPermissionManager.isGlobalPermission(COMMENT_DELETE_OWN));
        assertFalse(globalPermissionManager.isGlobalPermission(ATTACHMENT_DELETE_ALL));
        assertFalse(globalPermissionManager.isGlobalPermission(ATTACHMENT_DELETE_OWN));
        assertFalse(globalPermissionManager.isGlobalPermission(WORKLOG_DELETE_ALL));
        assertFalse(globalPermissionManager.isGlobalPermission(WORKLOG_DELETE_OWN));
        assertFalse(globalPermissionManager.isGlobalPermission(WORKLOG_EDIT_ALL));
        assertFalse(globalPermissionManager.isGlobalPermission(WORKLOG_EDIT_OWN));

        assertTrue(globalPermissionManager.isGlobalPermission(SYSTEM_ADMIN));
        assertTrue(globalPermissionManager.isGlobalPermission(ADMINISTER));
        assertTrue(globalPermissionManager.isGlobalPermission(USE));
        assertTrue(globalPermissionManager.isGlobalPermission(USER_PICKER));
        assertTrue(globalPermissionManager.isGlobalPermission(CREATE_SHARED_OBJECTS));
        assertTrue(globalPermissionManager.isGlobalPermission(MANAGE_GROUP_FILTER_SUBSCRIPTIONS));
        assertTrue(globalPermissionManager.isGlobalPermission(BULK_CHANGE));
    }

    @Test
    public void testGetPluginGlobalPermissions() {
        when(globalPermissionTypesManager.getAll()).thenReturn(Lists.newArrayList(
                new GlobalPermissionType("one", null, null, true),
                new GlobalPermissionType("two", null, null, true)));

        Iterable<GlobalPermissionType> globalPermissions = globalPermissionManager.getAllGlobalPermissions();

        assertThat(globalPermissions, Matchers.<GlobalPermissionType>hasItem(Matchers.hasProperty("key", Matchers.is("one"))));
        assertThat(globalPermissions, Matchers.<GlobalPermissionType>hasItem(Matchers.hasProperty("key", Matchers.is("two"))));
    }

    @Test
    public void testGetPluggablePermission() {
        when(globalPermissionTypesManager.getGlobalPermission(Mockito.eq("permissionKey"))).thenReturn(some(new GlobalPermissionType(null, null, null, true)));

        assertThat(globalPermissionManager.getGlobalPermission("permissionKey").isDefined(), Matchers.is(true));
    }

    @Test
    public void testRemovalNonExistingPermission() {
        exception.expect(IllegalArgumentException.class);
        globalPermissionManager.removePermission(100, null);
    }

    @Test
    public void alwaysAllowsAccessToRecoveryAdminForGlobalPermissionKey() {
        for (GlobalPermissionKey key : of(GlobalPermissionKey.ADMINISTER, GlobalPermissionKey.SYSTEM_ADMIN, GlobalPermissionKey.USE)) {
            assertFalse(globalPermissionManager.hasPermission(key, new MockApplicationUser("NOT_" + RECOVERY_USER)));
            assertTrue(globalPermissionManager.hasPermission(key, new MockApplicationUser(RECOVERY_USER)));
        }
    }

    @Test
    public void alwaysAllowsAccessToRecoveryAdminForGlobalPermissionType() {
        for (String key : of(GlobalPermissionType.ADMINISTER, GlobalPermissionType.SYSTEM_ADMIN, GlobalPermissionType.USE)) {
            final GlobalPermissionType type = new GlobalPermissionType(key, null, null, true);

            assertFalse(globalPermissionManager.hasPermission(type, new MockApplicationUser("NOT_" + RECOVERY_USER)));
            assertTrue(globalPermissionManager.hasPermission(type, new MockApplicationUser(RECOVERY_USER)));
        }
    }

    @Test
    public void alwaysAllowsAccessToRecoveryUsingIntPermissionAndApplicationUser() {
        for (int permission : of(Permissions.ADMINISTER, Permissions.SYSTEM_ADMIN, Permissions.USE)) {
            assertFalse(globalPermissionManager.hasPermission(permission, new MockApplicationUser("NOT_" + RECOVERY_USER)));
            assertTrue(globalPermissionManager.hasPermission(permission, new MockApplicationUser(RECOVERY_USER)));
        }
    }

    @Test
    public void alwaysAllowsAccessToRecoveryUsingIntPermissionAndCrowdUser() {
        for (int permission : of(Permissions.ADMINISTER, Permissions.SYSTEM_ADMIN, Permissions.USE)) {
            assertFalse(globalPermissionManager.hasPermission(permission, new MockApplicationUser("NOT_" + RECOVERY_USER)));
            assertTrue(globalPermissionManager.hasPermission(permission, new MockApplicationUser(RECOVERY_USER)));
        }
    }

    @Test
    public void ifManagedByUMFeatureIsEnabledSystemAdminAdministerAndUsePermissionsAreNotManagedByJira() {
        featureManager.enable(CoreFeatures.PERMISSIONS_MANAGED_BY_UM);

        assertFalse(globalPermissionManager.isPermissionManagedByJira(GlobalPermissionKey.ADMINISTER));
        assertFalse(globalPermissionManager.isPermissionManagedByJira(GlobalPermissionKey.USE));
        assertFalse(globalPermissionManager.isPermissionManagedByJira(GlobalPermissionKey.SYSTEM_ADMIN));
        assertTrue(globalPermissionManager.isPermissionManagedByJira(GlobalPermissionKey.BULK_CHANGE));
    }

    @Test
    public void ifManagedByUMFeatureIsDisabledAllPermissionsAreManagedByJira() {
        featureManager.disable(CoreFeatures.PERMISSIONS_MANAGED_BY_UM);

        for (GlobalPermissionKey permissionKey : GlobalPermissionKey.GLOBAL_PERMISSION_ID_TRANSLATION.values()) {
            assertTrue(globalPermissionManager.isPermissionManagedByJira(permissionKey));

        }
    }
}
