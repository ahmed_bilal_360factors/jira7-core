package com.atlassian.jira.web.action.setup;

import com.atlassian.application.api.Application;
import com.atlassian.application.api.ApplicationManager;
import com.atlassian.application.api.PlatformApplication;
import com.atlassian.fugue.Option;
import com.google.common.collect.Lists;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.net.URI;
import java.net.URISyntaxException;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PostSetupURIProviderTest {
    public static final String DEFAULT_POSTINSTALL_URI = "Dashboard.jspa";
    public static final String COMPLICATED_POSTINSTALL_URI = "Dashboard.jspa?option=6&value=3";
    public static final String APPLICATION_POSTINSTALL_URI = "/application";
    @Mock
    private ApplicationManager applicationManager;

    // testing Application post URI
    @Mock
    Application applicationWithPostUri;
    @Mock
    Application applicationWithoutPostUri;
    @Mock
    Application applicationWithComplicatedPostUri;
    @Mock
    PlatformApplication platformApplicationWithPostUri;
    @Mock
    PlatformApplication platformApplicationWithoutPostUri;

    @InjectMocks
    PostSetupURIProvider setupCompleteAction;

    @Before
    public void setUp() throws Exception {
        when(applicationWithPostUri.getPostInstallURI()).thenReturn(
                Option.some(new URI(APPLICATION_POSTINSTALL_URI))
        );
        when(platformApplicationWithPostUri.getPostInstallURI()).thenReturn(
                Option.some(new URI("/platform"))
        );
        when(applicationWithoutPostUri.getPostInstallURI()).thenReturn(
                Option.<URI>none()
        );
        when(platformApplicationWithoutPostUri.getPostInstallURI()).thenReturn(
                Option.<URI>none()
        );
        when(applicationWithComplicatedPostUri.getPostInstallURI()).thenReturn(
                Option.some(new URI(COMPLICATED_POSTINSTALL_URI))
        );
    }


    private void initApplications(PlatformApplication platformApplication, Iterable<Application> applications) {
        when(applicationManager.getApplications())
                .thenReturn(applications);
        when(applicationManager.getPlatform())
                .thenReturn(platformApplication);
    }

    @Test
    public void shouldReturnDefaultRedirectURIWhenThereAreNoApplicationsWithNull() throws URISyntaxException {
        // when
        initApplications(platformApplicationWithoutPostUri, null);

        // then
        final String postInstallRedirect = setupCompleteAction.getPostSetupRedirect(Option.<String>none());
        Assert.assertThat(
                postInstallRedirect,
                Matchers.equalTo(DEFAULT_POSTINSTALL_URI));
    }

    @Test
    public void shouldReturnDefaultRedirectURIWhenThereAreNoApplications() throws URISyntaxException {
        // when
        initApplications(platformApplicationWithoutPostUri, com.atlassian.fugue.Iterables.<Application>emptyIterable());

        // then
        final String postInstallRedirect = setupCompleteAction.getPostSetupRedirect(Option.<String>none());
        Assert.assertThat(
                postInstallRedirect,
                Matchers.equalTo(DEFAULT_POSTINSTALL_URI));
    }

    @Test
    public void shouldReturnDefaultRedirectWhenThereIsPlatformWithoutURI() throws URISyntaxException {
        // when
        initApplications(platformApplicationWithoutPostUri, Lists.<Application>newArrayList(platformApplicationWithoutPostUri));
        when(applicationManager.getPlatform())
                .thenReturn(platformApplicationWithPostUri);

        // then
        final String postInstallRedirect = setupCompleteAction.getPostSetupRedirect(Option.<String>none());
        Assert.assertThat(
                postInstallRedirect,
                Matchers.equalTo(DEFAULT_POSTINSTALL_URI));
    }

    @Test
    public void shouldIgnorePlatformURI() throws URISyntaxException {
        // when
        initApplications(platformApplicationWithPostUri, Lists.<Application>newArrayList(platformApplicationWithPostUri));

        // then
        final String postInstallRedirect = setupCompleteAction.getPostSetupRedirect(Option.<String>none());
        Assert.assertThat(
                postInstallRedirect,
                Matchers.equalTo(DEFAULT_POSTINSTALL_URI));
    }

    @Test
    public void shouldReturnDefaultURIWhenThereAreApplicationsWithoutPostUri() throws URISyntaxException {
        // when
        initApplications(platformApplicationWithoutPostUri, Lists.<Application>newArrayList(applicationWithoutPostUri));

        // then
        final String postInstallRedirect = setupCompleteAction.getPostSetupRedirect(Option.<String>none());
        Assert.assertThat(
                postInstallRedirect,
                Matchers.equalTo(DEFAULT_POSTINSTALL_URI));
    }

    @Test
    public void shouldReturnApplicationUri() throws URISyntaxException {
        // when
        initApplications(platformApplicationWithPostUri, Lists.<Application>newArrayList(platformApplicationWithPostUri, applicationWithPostUri));

        // then
        final String postInstallRedirect = setupCompleteAction.getPostSetupRedirect(Option.<String>none());
        Assert.assertThat(
                postInstallRedirect,
                Matchers.equalTo(APPLICATION_POSTINSTALL_URI));
    }

    @Test
    public void shouldAddOriginToDefaultPostInstallUri() throws URISyntaxException {
        // when
        initApplications(platformApplicationWithPostUri, Lists.<Application>newArrayList(platformApplicationWithPostUri));

        // then
        final String postInstallRedirect = setupCompleteAction.getPostSetupRedirect(Option.some("SetupMethod"));
        Assert.assertThat(
                postInstallRedirect,
                Matchers.equalTo(DEFAULT_POSTINSTALL_URI + "?src=SetupMethod"));
    }

    @Test
    public void shouldAddOriginToApplicationPostInstallUri() throws URISyntaxException {
        // when
        initApplications(platformApplicationWithPostUri, Lists.<Application>newArrayList(applicationWithPostUri));

        // then
        final String postInstallRedirect = setupCompleteAction.getPostSetupRedirect(Option.some("SetupMethod"));
        Assert.assertThat(
                postInstallRedirect,
                Matchers.equalTo(APPLICATION_POSTINSTALL_URI + "?src=SetupMethod"));
    }

    @Test
    public void shouldAddOriginToComplicatedApplicationPostInstallUri() throws URISyntaxException {
        // when
        initApplications(platformApplicationWithPostUri, Lists.<Application>newArrayList(applicationWithComplicatedPostUri));

        // then
        final String postInstallRedirect = setupCompleteAction.getPostSetupRedirect(Option.some("SetupMethod"));
        Assert.assertThat(
                postInstallRedirect,
                Matchers.equalTo(COMPLICATED_POSTINSTALL_URI + "&src=SetupMethod"));
    }

}