package com.atlassian.jira.issue.index.indexers.impl;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.mock.project.MockVersion;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.web.FieldVisibilityManager;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Collections;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestVersionCustomFieldIndexer {
    @Rule
    public MockitoRule mockito = MockitoJUnit.rule();

    @Mock
    private CustomField customField;
    @Mock
    private FieldVisibilityManager visibilityManager;

    @Test
    public void testAddIndexNullValue() throws Exception {
        final Issue theIssue = null;
        final Document doc = new Document();

        when(customField.getId()).thenReturn("blah");
        when(customField.isRelevantForIssueContext(theIssue)).thenReturn(true);
        when(customField.getValue(theIssue)).thenReturn(null);
        when(visibilityManager.isFieldVisible("blah", theIssue)).thenReturn(true);

        VersionCustomFieldIndexer indexer = new VersionCustomFieldIndexer(visibilityManager, customField);

        indexer.addIndex(doc, theIssue);

        assertTrue(doc.getFields().isEmpty());
    }

    @Test
    public void testAddIndexValueNotCollection() throws Exception {
        final Issue theIssue = null;
        final Document doc = new Document();

        when(customField.getId()).thenReturn("blah");
        when(customField.isRelevantForIssueContext(theIssue)).thenReturn(true);
        when(customField.getValue(theIssue)).thenReturn("NotACollection");
        when(visibilityManager.isFieldVisible("blah", theIssue)).thenReturn(true);

        VersionCustomFieldIndexer indexer = new VersionCustomFieldIndexer(visibilityManager, customField);

        indexer.addIndex(doc, theIssue);

        assertTrue(doc.getFields().isEmpty());
    }

    @Test
    public void testAddIndexHappyPath() throws Exception {
        final Issue theIssue = null;
        final Document doc = new Document();
        final String customFieldId = "customField";

        final Version version = new MockVersion(123L, "Test");

        when(customField.getId()).thenReturn(customFieldId);
        when(customField.isRelevantForIssueContext(theIssue)).thenReturn(true);
        when(customField.getValue(theIssue)).thenReturn(Collections.singleton(version));
        when(visibilityManager.isFieldVisible(customFieldId, theIssue)).thenReturn(true);

        VersionCustomFieldIndexer indexer = new VersionCustomFieldIndexer(visibilityManager, customField);

        indexer.addIndex(doc, theIssue);

        final Field field = doc.getField(customFieldId);
        assertEquals("123", field.stringValue());
    }
}
