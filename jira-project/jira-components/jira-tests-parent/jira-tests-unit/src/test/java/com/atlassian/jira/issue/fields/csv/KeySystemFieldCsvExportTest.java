package com.atlassian.jira.issue.fields.csv;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.export.FieldExportParts;
import com.atlassian.jira.issue.fields.KeySystemField;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;

import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.I18nHelper;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.mockito.Mock;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

public class KeySystemFieldCsvExportTest {
    @Rule
    public final RuleChain mockito = MockitoMocksInContainer.forTest(this);

    private static final Long ISSUE_ID = 1000L;
    private static final Long PARENT_ID = 1010L;
    private static final String ISSUE_KEY = "ISSUE-5";

    @Mock
    private Issue issue;

    @Mock
    private JiraAuthenticationContext authenticationContext;

    @Mock
    private I18nHelper i18nHelper;


    private KeySystemField field;

    @Before
    public void setUp() {
        field = new KeySystemField(null, null, authenticationContext);

        when(authenticationContext.getI18nHelper()).thenReturn(i18nHelper);

        when(issue.getId()).thenReturn(ISSUE_ID);
        when(issue.getKey()).thenReturn(ISSUE_KEY);
    }

    @Test
    public void testCsvRepresentationForExpectedTypeAndValues() {
        when(issue.getParentId()).thenReturn(PARENT_ID);

        final FieldExportParts representation = field.getRepresentationFromIssue(issue);
        assertThat(representation.getParts(), hasSize(3));

        assertThat(representation.getPartWithId("issueId").get().getValues()::iterator, contains(String.valueOf(ISSUE_ID)));
        assertThat(representation.getPartWithId("parentId").get().getValues()::iterator, contains(String.valueOf(PARENT_ID)));
        assertThat(representation.getPartWithId("key").get().getValues()::iterator, contains(ISSUE_KEY));
    }

    @Test
    public void testCsvRepresentationWhenThereIsNoParentIssue() {
        when(issue.getParentId()).thenReturn(null);

        final FieldExportParts representation = field.getRepresentationFromIssue(issue);
        assertThat(representation.getParts(), hasSize(2));

        assertThat(representation.getPartWithId("issueId").get().getValues()::iterator, contains(String.valueOf(ISSUE_ID)));
        assertThat(representation.getPartWithId("key").get().getValues()::iterator, contains(ISSUE_KEY));
    }
}
