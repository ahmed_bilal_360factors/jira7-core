package com.atlassian.jira.jql.validator;

import com.atlassian.jira.jql.operand.JqlOperandResolver;
import com.atlassian.jira.jql.operand.MockOperandHandler;
import com.atlassian.jira.jql.operand.QueryLiteral;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.MessageSet;
import com.atlassian.jira.web.bean.MockI18nBean;
import com.atlassian.query.clause.TerminalClause;
import com.atlassian.query.clause.TerminalClauseImpl;
import com.atlassian.query.operand.EmptyOperand;
import com.atlassian.query.operand.FunctionOperand;
import com.atlassian.query.operand.MultiValueOperand;
import com.atlassian.query.operator.Operator;
import org.apache.lucene.queryParser.QueryParser;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.empty;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestFreeTextFieldValidator {
    private static final String FIELD = "fieldz";
    @Rule
    public final MethodRule initMockito = MockitoJUnit.rule();
    @Mock
    private QueryParser queryParser;

    @Test
    public void validateEmptyReturnsEmptyMessagesSet() throws Exception {
        TerminalClause terminalClause = new TerminalClauseImpl(FIELD, Operator.IS, EmptyOperand.EMPTY);
        ClauseValidator validator = getTestFreeTextFieldValidator();

        MessageSet messageSet = validator.validate(null, terminalClause);

        assertThat(messageSet.getErrorMessages(), empty());
        assertThat(messageSet.getWarningMessages(), empty());
    }

    @Test
    public void validateHappyPath() throws Exception {
        when(queryParser.parse(Matchers.notNull(String.class))).thenReturn(null);

        _testValidateHappyPathForOperator(Operator.LIKE);
        _testValidateHappyPathForOperator(Operator.NOT_LIKE);
        _testValidateHappyPathForOperator(Operator.IS);
        _testValidateHappyPathForOperator(Operator.IS_NOT);
    }

    private void _testValidateHappyPathForOperator(final Operator operator) {
        TerminalClause terminalClause = new TerminalClauseImpl(FIELD, operator, "val");
        ClauseValidator validator = getTestFreeTextFieldValidator();

        MessageSet messageSet = validator.validate(null, terminalClause);

        assertThat(messageSet.getErrorMessages(), empty());
        assertThat(messageSet.getWarningMessages(), empty());
    }

    @Test
    public void invalidQueryDoesntParse() throws Exception {
        when(queryParser.parse("val")).thenThrow(new org.apache.lucene.queryParser.ParseException());
        TerminalClause terminalClause = new TerminalClauseImpl(FIELD, Operator.LIKE, "val");
        ClauseValidator validator = getTestFreeTextFieldValidator();

        MessageSet messageSet = validator.validate(null, terminalClause);

        assertThat(messageSet.getErrorMessages(), contains("Unable to parse the text 'val' for field '" + FIELD + "'."));
    }

    @Test
    public void invalidQueryDoesntParseForBadFuzzyQuery() throws Exception {
        // JRA-27018
        when(queryParser.parse("a~1")).thenThrow(new IllegalArgumentException());
        TerminalClause terminalClause = new TerminalClauseImpl(FIELD, Operator.LIKE, "a~1");
        ClauseValidator validator = getTestFreeTextFieldValidator();

        MessageSet messageSet = validator.validate(null, terminalClause);

        assertThat(messageSet.getErrorMessages(), contains("Unable to parse the text 'a~1' for field '" + FIELD + "'."));
    }

    @Test
    public void twoQueriesSecondDoesntParse() throws Exception {
        when(queryParser.parse("val1")).thenReturn(null);
        when(queryParser.parse("val2")).thenThrow(new org.apache.lucene.queryParser.ParseException());
        ClauseValidator validator = getTestFreeTextFieldValidator();
        TerminalClause terminalClause = new TerminalClauseImpl(FIELD, Operator.LIKE, new MultiValueOperand("val1", "val2"));

        MessageSet messageSet = validator.validate(null, terminalClause);

        assertThat(messageSet.getErrorMessages(), contains("Unable to parse the text 'val2' for field '" + FIELD + "'."));
    }

    @Test
    public void invalidQueryDoesntParseFunction() throws Exception {
        final FunctionOperand operand = new FunctionOperand("function");
        when(queryParser.parse("val")).thenThrow(new org.apache.lucene.queryParser.ParseException());

        final MockOperandHandler handler = new MockOperandHandler(true, false, true);
        handler.add(new QueryLiteral(operand, "val"));

        final MockJqlOperandResolver operandResolver = MockJqlOperandResolver.createSimpleSupport();
        operandResolver.addHandler("function", handler);

        ClauseValidator validator = getTestFreeTextFieldValidator(FIELD, operandResolver);
        TerminalClause terminalClause = new TerminalClauseImpl(FIELD, Operator.LIKE, operand);

        MessageSet messageSet = validator.validate(null, terminalClause);

        assertThat(messageSet.getErrorMessages(), contains("The field '" + FIELD + "' is unable to parse the text given to it by the function 'function'."));
    }

    @Test
    public void inputIsEmptyString() throws Exception {
        ClauseValidator validator = getTestFreeTextFieldValidator();
        TerminalClause terminalClause = new TerminalClauseImpl(FIELD, Operator.LIKE, "");

        MessageSet messageSet = validator.validate(null, terminalClause);

        assertThat(messageSet.getErrorMessages(), contains("The field '" + FIELD + "' does not support searching for an empty string."));
    }

    @Test
    public void inputIsBlankString() throws Exception {
        ClauseValidator validator = getTestFreeTextFieldValidator();
        TerminalClause terminalClause = new TerminalClauseImpl(FIELD, Operator.LIKE, "  ");

        MessageSet messageSet = validator.validate(null, terminalClause);

        assertThat(messageSet.getErrorMessages(), contains("The field '" + FIELD + "' does not support searching for an empty string."));
    }

    @Test
    public void testInvalidOperators() throws Exception {
        _testValidateInvalidOperator(Operator.EQUALS);
        _testValidateInvalidOperator(Operator.NOT_EQUALS);
        _testValidateInvalidOperator(Operator.IN);
        _testValidateInvalidOperator(Operator.NOT_IN);
        _testValidateInvalidOperator(Operator.GREATER_THAN);
        _testValidateInvalidOperator(Operator.GREATER_THAN_EQUALS);
        _testValidateInvalidOperator(Operator.LESS_THAN);
        _testValidateInvalidOperator(Operator.LESS_THAN_EQUALS);
    }

    private void _testValidateInvalidOperator(Operator operator) {
        TerminalClause terminalClause = new TerminalClauseImpl(FIELD, operator, "!val");
        ClauseValidator validator = getTestFreeTextFieldValidator();

        MessageSet messageSet = validator.validate(null, terminalClause);

        assertThat(messageSet.getErrorMessages(), contains("The operator '" + operator.getDisplayString() + "' is not supported by the '" + FIELD + "' field."));
    }

    private FreeTextFieldValidator getTestFreeTextFieldValidator() {
        return getTestFreeTextFieldValidator(FIELD, MockJqlOperandResolver.createSimpleSupport());
    }

    private FreeTextFieldValidator getTestFreeTextFieldValidator(final String indexField, JqlOperandResolver jqlOperandResolver) {
        return new FreeTextFieldValidator(indexField, jqlOperandResolver) {
            @Override
            protected I18nHelper getI18n(final ApplicationUser user) {
                return new MockI18nBean();
            }

            @Override
            QueryParser getQueryParser(final String fieldName) {
                return queryParser;
            }
        };
    }

}
