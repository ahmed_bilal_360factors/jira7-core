package com.atlassian.jira.upgrade;

import org.junit.Test;

import java.lang.reflect.Field;
import java.util.Map;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;

public class PluginSystemLauncherTest {
    static class ClassWithEntriesBase extends ClassLoader {
        private Map<String, String> resourceEntries;
    }

    static class ClassWithEntriesDerivative extends ClassWithEntriesBase {
    }

    static class ClassWithoutWithEntries extends ClassLoader {
    }


    @Test
    public void shouldFindPrivateResourceEntriesElementInDeclaringClass() throws Exception {
        final Field resourceEntriesField = PluginSystemLauncher.getResourceEntriesField(ClassWithEntriesBase.class);
        assertThat(resourceEntriesField, is(notNullValue()));
        assertThat(resourceEntriesField.getDeclaringClass(), is(sameInstance(ClassWithEntriesBase.class)));
    }

    @Test
    public void shouldFindPrivateResourceEntriesElementInDerivedClass() throws Exception {
        final Field resourceEntriesField = PluginSystemLauncher.getResourceEntriesField(ClassWithEntriesDerivative.class);
        assertThat(resourceEntriesField, is(notNullValue()));
        assertThat(resourceEntriesField.getDeclaringClass(), is(sameInstance(ClassWithEntriesBase.class)));
    }

    @Test(expected = NoSuchFieldException.class)
    public void shouldThrowWhenClassLoaderDoesntContainResourceEntries() throws Exception {
        PluginSystemLauncher.getResourceEntriesField(ClassWithoutWithEntries.class);
    }
}