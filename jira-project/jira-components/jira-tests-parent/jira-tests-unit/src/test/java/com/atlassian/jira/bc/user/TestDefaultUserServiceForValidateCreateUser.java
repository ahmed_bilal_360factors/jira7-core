package com.atlassian.jira.bc.user;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.application.ApplicationRoleManager;
import com.atlassian.jira.bc.user.UserService.CreateUserRequest;
import com.atlassian.jira.bc.user.UserService.CreateUserValidationResult;
import com.atlassian.jira.bc.user.UserValidationHelper.Validations;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.mock.security.MockSimpleAuthenticationContext;
import com.atlassian.jira.plugin.user.PreDeleteUserErrorsManager;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.MockUserDeleteVeto;
import com.atlassian.jira.user.UserDeleteVeto;
import com.atlassian.jira.user.util.MockUserManager;
import com.atlassian.jira.user.util.UserUtil;
import com.atlassian.jira.util.ErrorCollection.Reason;
import com.atlassian.jira.util.GlobalPermissionGroupAssociationUtil;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.jira.util.SimpleWarningCollection;
import com.atlassian.jira.util.WarningCollection;
import com.atlassian.jira.web.bean.MockI18nBean;
import com.google.common.collect.ImmutableSet;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.mockito.Mock;

import java.util.Collection;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static com.atlassian.jira.matchers.ErrorCollectionMatcher.hasNoErrors;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.when;

/**
 * Test for createUser in UserService
 */
public class TestDefaultUserServiceForValidateCreateUser {
    @Rule
    public final RuleChain mockContainer = MockitoMocksInContainer.forTest(this);

    @Mock
    private UserUtil userUtil;
    @Mock
    private PermissionManager permissionManager;
    @Mock
    private PreDeleteUserErrorsManager preDeleteUserErrorsManager;
    @Mock
    private CreateUserApplicationHelper applicationHelper;
    @Mock
    private ApplicationRoleManager applicationRoleManager;
    @Mock
    private UserValidationHelper validationHelper;
    @Mock
    private Validations validations;
    final SimpleErrorCollection validationErrors = new SimpleErrorCollection();

    @Mock
    @AvailableInContainer
    private EventPublisher eventPublisher;
    @AvailableInContainer
    private JiraAuthenticationContext jiraAuthenticationContext = new MockSimpleAuthenticationContext(null);

    private UserDeleteVeto userDeleteVeto = new MockUserDeleteVeto();
    private MockUserManager userManager = new MockUserManager();
    private I18nHelper.BeanFactory i18nFactory = new MockI18nBean.MockI18nBeanFactory();

    private UserService userService;

    final Set<ApplicationKey> DEFAULT_APP_KEYS = new HashSet<>();
    @Mock
    private GlobalPermissionGroupAssociationUtil groupAccessLabelsManager;
    @Mock
    private GlobalPermissionManager globalPermissionManager;
    @Mock
    private WarningCollection validationWarnings;

    @Before
    public void setUp() throws Exception {
        when(validations.getErrors()).thenReturn(validationErrors);
        when(validations.getWarnings()).thenReturn(validationWarnings);
        when(validationHelper.validations(anyObject())).thenReturn(validations);
        DEFAULT_APP_KEYS.add(ApplicationKey.valueOf("default"));
        when(applicationRoleManager.getDefaultApplicationKeys()).thenReturn(DEFAULT_APP_KEYS);
        userService = new DefaultUserService(userUtil, userDeleteVeto, permissionManager, userManager,
                i18nFactory, jiraAuthenticationContext, null, preDeleteUserErrorsManager,
                applicationHelper, applicationRoleManager, validationHelper,
                groupAccessLabelsManager, globalPermissionManager);
    }

    @Test
    public void testThatValidateSetsDefaultGroupsWhenUnset() {
        //Given
        final CreateUserRequest createUserRequest = CreateUserRequest.withUserDetails(null, null, null, null, null)
                .skipValidation();

        //When
        final CreateUserValidationResult validationResult = userService.validateCreateUser(createUserRequest);

        //Then
        assertThat(validationResult.getApplicationKeys(), is(DEFAULT_APP_KEYS));
        assertTrue(validationResult.isValid());
    }

    @Test
    public void testThatValidateDoesNotSetsDefaultGroupsWhenSet() {
        //Given
        final Set<ApplicationKey> setAppKeys = new HashSet<>();
        setAppKeys.add(ApplicationKey.valueOf("provided"));
        final CreateUserRequest createUserRequest = CreateUserRequest.withUserDetails(null, null, null, null, null)
                .withApplicationAccess(setAppKeys)
                .skipValidation();

        //When
        final CreateUserValidationResult validationResult = userService.validateCreateUser(createUserRequest);

        //Then
        assertThat(validationResult.getApplicationKeys(), contains(ApplicationKey.valueOf("provided")));
        assertTrue(validationResult.isValid());
    }

    @Test
    public void testValidationFailWhenLoggedInUserNotAdmin() {
        //Given
        //User does not have create access
        final MockApplicationUser loggedInUser = new MockApplicationUser("nonAdmin");
        when(validations.hasCreateAccess(loggedInUser)).thenReturn(false);
        final SimpleErrorCollection errors = new SimpleErrorCollection("Test", Reason.VALIDATION_FAILED);
        when(validations.getErrors()).thenReturn(errors);
        final CreateUserRequest createUserRequest = CreateUserRequest.withUserDetails(loggedInUser, null, null, null, null);

        //When
        final CreateUserValidationResult validationResult = userService.validateCreateUser(createUserRequest);

        //Then
        assertThat(validationResult.getErrorCollection(), is(errors));
        assertFalse(validationResult.isValid());
    }

    @Test
    public void testValidationFailsWhenNoWritableDefaultDirectory() {
        //Given
        when(validations.hasWritableDefaultCreateDirectory()).thenReturn(false);
        final SimpleErrorCollection errors = new SimpleErrorCollection("Test", Reason.VALIDATION_FAILED);
        when(validations.getErrors()).thenReturn(errors);
        final CreateUserRequest createUserRequest = CreateUserRequest.withUserDetails(null, null, null, null, null)
                .performPermissionCheck(false);

        //When
        final CreateUserValidationResult validationResult = userService.validateCreateUser(createUserRequest);

        //Then
        assertThat(validationResult.getErrorCollection(), is(errors));
        assertFalse(validationResult.isValid());
    }

    @Test
    public void testValidationFailsWhenSpecifiedDirectoryNotWritable() {
        //Given
        when(validations.writableDirectory(445L)).thenReturn(false);
        final SimpleErrorCollection errors = new SimpleErrorCollection("Test", Reason.VALIDATION_FAILED);
        when(validations.getErrors()).thenReturn(errors);
        final CreateUserRequest createUserRequest = CreateUserRequest.withUserDetails(null, null, null, null, null)
                .inDirectory(445L)
                .performPermissionCheck(false);

        //When
        final CreateUserValidationResult validationResult = userService.validateCreateUser(createUserRequest);

        //Then
        assertThat(validationResult.getErrorCollection(), is(errors));
        assertFalse(validationResult.isValid());
    }

    @Test
    public void testValidationFailsWhenRequiredPasswordNotProvided() {
        //Given
        final SimpleErrorCollection errors = new SimpleErrorCollection("Test", Reason.VALIDATION_FAILED);
        when(validations.getErrors()).thenReturn(errors);
        final CreateUserRequest createUserRequest = CreateUserRequest.withUserDetails(null, null, null, null, null)
                .performPermissionCheck(false)
                .passwordRequired();

        //When
        final CreateUserValidationResult validationResult = userService.validateCreateUser(createUserRequest);

        //Then
        assertThat(validationResult.getErrorCollection(), is(errors));
        assertFalse(validationResult.isValid());
    }

    @Test
    public void testPassAllValidations() {
        //Given
        when(validations.hasWritableDefaultCreateDirectory()).thenReturn(true);
        final CreateUserRequest createUserRequest = CreateUserRequest.withUserDetails(null, null, null, null, null)
                .performPermissionCheck(false);

        //When
        final CreateUserValidationResult validationResult = userService.validateCreateUser(createUserRequest);

        //Then
        assertThat(validationResult.getErrorCollection(), hasNoErrors());
        assertTrue(validationResult.isValid());
    }

    @Test
    public void validationShouldPassWithWarningsWhenApplicationsExceedSeatLimit() {
        //Given
        final WarningCollection warningCollection = new SimpleWarningCollection();
        final String errorMessage = "ApplicationKey validation error";
        when(validations.hasWritableDefaultCreateDirectory()).thenReturn(true);
        when(applicationHelper.validateApplicationKeys(org.mockito.Matchers.<Optional<Long>>any(),
                any())).thenReturn(ImmutableSet.of(errorMessage));
        when(validationHelper.validations(any()).getWarnings()).thenReturn(warningCollection);
        final CreateUserRequest createUserRequest = CreateUserRequest.withUserDetails(null, null, null, null, null)
                .performPermissionCheck(false);

        //When
        final CreateUserValidationResult validationResult = userService.validateCreateUser(createUserRequest);

        //Then
        final Collection<String> actualWarnings = validationResult.getWarningCollection().getWarnings();
        assertTrue(actualWarnings.contains(errorMessage));
        assertTrue(actualWarnings.size() == 1);
        assertTrue(validationResult.isValid());
    }

    @Test
    public void validationShouldSucceedWhenShouldValidateApplicationKeysWithoutErrors() {
        //Given
        when(validations.hasWritableDefaultCreateDirectory()).thenReturn(true);
        when(applicationHelper.validateApplicationKeys(org.mockito.Matchers.<Optional<Long>>any(),
                any())).thenReturn(ImmutableSet.of());
        final CreateUserRequest createUserRequest = CreateUserRequest.withUserDetails(null, null, null, null, null)
                .performPermissionCheck(false);

        //When
        final CreateUserValidationResult validationResult = userService.validateCreateUser(createUserRequest);

        //Then
        assertThat(validationResult.getErrorCollection(), hasNoErrors());
        assertTrue(validationResult.isValid());
    }
}