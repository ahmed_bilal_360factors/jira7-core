package com.atlassian.jira.project.type.warning;

import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.type.ProjectTypeKey;
import com.atlassian.jira.project.type.ProjectTypeManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.util.NoopI18nFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestInaccessibleProjectTypeDialogContentProvider {
    private static final ProjectTypeKey SOFTWARE_PROJECT_TYPE = new ProjectTypeKey("software");
    private static final ApplicationUser USER = new MockApplicationUser("user");

    @Mock
    private ProjectTypeManager projectTypeManager;

    private InaccessibleProjectTypeDialogContentProvider provider;

    @Before
    public void setUp() {
        provider = new InaccessibleProjectTypeDialogContentProvider(projectTypeManager, new NoopI18nFactory());
    }

    @Test
    public void buildsExpectedDialogContentWhenProjectTypeIsUninstalled() {
        Project project = projectWithType(SOFTWARE_PROJECT_TYPE);
        projectTypeIsUninstalled(SOFTWARE_PROJECT_TYPE);

        InaccessibleProjectTypeDialogContent content = provider.getContent(USER, project);

        assertDialogCommonContentIsCorrect(content);
        assertThat(content.getFirstParagraph(), is("project.type.warning.dialog.project.type.uninstalled{[Software]}"));
    }

    @Test
    public void buildsExpectedDialogContentWhenProjectTypeIsInaccessible() {
        Project project = projectWithType(SOFTWARE_PROJECT_TYPE);
        projectTypeIsInstalledButInaccessible(SOFTWARE_PROJECT_TYPE);

        InaccessibleProjectTypeDialogContent content = provider.getContent(USER, project);

        assertDialogCommonContentIsCorrect(content);
        assertThat(content.getFirstParagraph(), is("project.type.warning.dialog.project.type.unlicensed{[Software]}"));
    }

    private void assertDialogCommonContentIsCorrect(InaccessibleProjectTypeDialogContent content) {
        assertThat(content.getTitle(), is("project.type.warning.dialog.title{[Software]}"));
        assertThat(content.getSecondParagraph(), is("project.type.warning.dialog.project.still.accessible{[Software]}"));
        assertThat(content.getCallToActionText(), is("project.type.warning.dialog.change.project.type{[]}"));
    }

    private void projectTypeIsUninstalled(ProjectTypeKey projectTypeKey) {
        when(projectTypeManager.isProjectTypeUninstalled(projectTypeKey)).thenReturn(true);
    }

    private void projectTypeIsInstalledButInaccessible(ProjectTypeKey projectTypeKey) {
        when(projectTypeManager.isProjectTypeUninstalled(projectTypeKey)).thenReturn(false);
    }

    private Project projectWithType(ProjectTypeKey projectTypeKey) {
        Project project = mock(Project.class);
        when(project.getProjectTypeKey()).thenReturn(projectTypeKey);
        return project;
    }
}
