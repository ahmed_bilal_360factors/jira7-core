package com.atlassian.jira.permission.management;


import com.atlassian.jira.help.HelpUrl;
import com.atlassian.jira.help.HelpUrls;
import com.atlassian.jira.mock.i18n.MockI18nHelper;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.permission.PermissionTypeManager;
import com.atlassian.jira.permission.management.beans.SecurityTypeBean;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.type.SecurityType;
import com.atlassian.jira.security.type.SecurityTypeKeys;
import com.atlassian.jira.security.type.SingleUser;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.I18nHelper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class SecurityTypeValuesServiceImplTest {

    @Mock
    private GlobalPermissionManager globalPermissionManager;

    @Mock
    private PermissionTypeManager permissionTypeManager;

    private I18nHelper i18n = new MockI18nHelper();

    @Mock
    private HelpUrls helpUrls;

    @Mock
    private HelpUrl url;

    @Mock
    ApplicationUser user;

    @Mock
    SingleUser userSecurityType;

    private SecurityTypeValuesServiceImpl securityTypeValuesHelper;

    private static final String USER_DESC = SecurityTypeKeys.USER.getKey();

    private static final String HELP_URL = "SomeUrl";

    @Before
    public void setUp() {
        securityTypeValuesHelper = new SecurityTypeValuesServiceImpl(globalPermissionManager, permissionTypeManager, i18n, helpUrls);

        mockHelpUrls();
    }

    public void mockHelpUrls() {
        when(helpUrls.getUrl(anyString())).thenReturn(url);
        when(url.getUrl()).thenReturn(HELP_URL);
    }

    public void mockReturnUserSecurity() {
        Map<String, SecurityType> securityTypes = new HashMap<>();
        securityTypes.put(USER_DESC, userSecurityType);

        when(userSecurityType.getDisplayName()).thenReturn("Single user");
        when(userSecurityType.getType()).thenReturn(USER_DESC);

        when(permissionTypeManager.getSecurityTypes()).thenReturn(securityTypes);
        when(permissionTypeManager.getSchemeType(eq(USER_DESC))).thenReturn(userSecurityType);
    }

    public void mockUserBrowse(final Boolean permission) {
        when(globalPermissionManager.hasPermission(eq(GlobalPermissionKey.USER_PICKER), eq(user))).thenReturn(permission);

    }

    public void userHasBrowse() {
        mockUserBrowse(true);
    }

    public void userHasNoBrowse() {
        mockUserBrowse(false);
    }

    @Test
    public void noBrowseUserHasHelpInfoInUserSecurityType() {
        mockReturnUserSecurity();
        userHasNoBrowse();

        List<SecurityTypeBean> securityTypeBeans = securityTypeValuesHelper.buildSecondarySecurityTypes(user);
        assertEquals("Should return one security type as one returned by PermissionTypeManager", 1, securityTypeBeans.size());
        assertNotNull("Help should not be null if user has no browse permission", securityTypeBeans.get(0).getHelp());
    }

    @Test
    public void hasBrowseUserHasNoHelpInfoInUserSecurityType() {
        mockReturnUserSecurity();
        userHasBrowse();

        List<SecurityTypeBean> securityTypeBeans = securityTypeValuesHelper.buildSecondarySecurityTypes(user);
        assertEquals("Should return one security type as one returned by PermissionTypeManager", 1, securityTypeBeans.size());
        assertNull("Help should not be null if user has browse permission", securityTypeBeans.get(0).getHelp());
    }
}
