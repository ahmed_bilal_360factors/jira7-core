package com.atlassian.jira.imports.project.handler;

import com.atlassian.jira.exception.ParseException;
import com.atlassian.jira.external.beans.ExternalIssueImpl;
import com.atlassian.jira.imports.project.ProjectImportPersister;
import com.atlassian.jira.imports.project.core.ProjectImportResults;
import com.atlassian.jira.imports.project.core.ProjectImportResultsImpl;
import com.atlassian.jira.imports.project.mapper.ProjectImportMapper;
import com.atlassian.jira.imports.project.mapper.ProjectImportMapperImpl;
import com.atlassian.jira.imports.project.parser.IssueParser;
import com.atlassian.jira.imports.project.transformer.IssueTransformer;
import com.atlassian.jira.mock.issue.MockIssue;
import com.atlassian.jira.web.bean.MockI18nBean;
import com.google.common.collect.ImmutableMap;
import org.junit.Test;

import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @since v3.13
 */
public class TestIssuePersisterHandler {
    final ProjectImportMapper projectImportMapper = new ProjectImportMapperImpl(null, null);

    @Test
    public void testLargestIssueKeyNumber() throws ParseException, AbortImportException {
        final Date importDate = new Date();
        final ExternalIssueImpl extIssue1 = new ExternalIssueImpl(null);
        final ExternalIssueImpl extIssue2 = new ExternalIssueImpl(null);
        final ExternalIssueImpl extIssue3 = new ExternalIssueImpl(null);

        final MockIssue issue1 = new MockIssue();
        issue1.setId(12L);
        issue1.setKey("TST-10");
        final MockIssue issue2 = new MockIssue();
        issue2.setId(13L);
        issue2.setKey("TST-164");
        final MockIssue issue3 = new MockIssue();
        issue3.setId(14L);
        issue3.setKey("TST-18");
        final ProjectImportPersister mockProjectImportPersister = mock(ProjectImportPersister.class);
        when(mockProjectImportPersister.createIssue(extIssue1, importDate, null)).thenReturn(issue1);
        when(mockProjectImportPersister.createIssue(extIssue2, importDate, null)).thenReturn(issue2);
        when(mockProjectImportPersister.createIssue(extIssue3, importDate, null)).thenReturn(issue3);

        final IssueParser mockIssueParser = mock(IssueParser.class);
        when(mockIssueParser.parse(null)).thenReturn(extIssue1, extIssue2, extIssue3);

        final IssueTransformer mockIssueTransformer = mock(IssueTransformer.class);
        when(mockIssueTransformer.transform(projectImportMapper, extIssue1)).thenReturn(extIssue1);
        when(mockIssueTransformer.transform(projectImportMapper, extIssue2)).thenReturn(extIssue2);
        when(mockIssueTransformer.transform(projectImportMapper, extIssue3)).thenReturn(extIssue3);

        ProjectImportResults projectImportResults = new ProjectImportResultsImpl(0, 0, 0, 0, null);

        IssuePersisterHandler issuePersisterHandler = new IssuePersisterHandler(mockProjectImportPersister, projectImportMapper, null, projectImportResults, new ExecutorForTests()) {
            IssueParser getIssueParser() {
                return mockIssueParser;
            }

            IssueTransformer getIssueTransformer() {
                return mockIssueTransformer;
            }

            Date getImportDate() {
                return importDate;
            }
        };

        issuePersisterHandler.handleEntity("Issue", null);
        issuePersisterHandler.handleEntity("Issue", null);
        issuePersisterHandler.handleEntity("Issue", null);
        issuePersisterHandler.handleEntity("NotIssue", ImmutableMap.of("id", "16", "key", "HSP-13", "desc", "More stuff happened."));

        assertEquals(164, issuePersisterHandler.getLargestIssueKeyNumber());
        assertEquals(3, projectImportResults.getIssuesCreatedCount());
    }

    @Test
    public void testNullPersistedIssue() throws ParseException, AbortImportException {
        final ExternalIssueImpl extIssue1 = new ExternalIssueImpl(null);
        extIssue1.setKey("TST-10");
        final Date importDate = new Date();

        final ProjectImportPersister mockProjectImportPersister = mock(ProjectImportPersister.class);
        when(mockProjectImportPersister.createIssue(extIssue1, importDate, null)).thenReturn(null);

        final IssueParser mockIssueParser = mock(IssueParser.class);
        when(mockIssueParser.parse(null)).thenReturn(extIssue1);

        final IssueTransformer mockIssueTransformer = mock(IssueTransformer.class);
        when(mockIssueTransformer.transform(projectImportMapper, extIssue1)).thenReturn(extIssue1);

        ProjectImportResults projectImportResults = new ProjectImportResultsImpl(0, 0, 0, 0, new MockI18nBean());
        IssuePersisterHandler issuePersisterHandler = new IssuePersisterHandler(mockProjectImportPersister, projectImportMapper, null, projectImportResults, new ExecutorForTests()) {
            IssueParser getIssueParser() {
                return mockIssueParser;
            }

            IssueTransformer getIssueTransformer() {
                return mockIssueTransformer;
            }

            Date getImportDate() {
                return importDate;
            }
        };

        issuePersisterHandler.handleEntity("Issue", null);

        assertEquals(0, issuePersisterHandler.getLargestIssueKeyNumber());
        assertEquals(0, projectImportResults.getIssuesCreatedCount());
        assertEquals(1, projectImportResults.getErrors().size());
        assertEquals("Unable to create issue with key 'TST-10'.", projectImportResults.getErrors().iterator().next());
    }

}
