package com.atlassian.jira.upgrade.tasks.role;

import com.atlassian.jira.CachingComponent;
import com.atlassian.jira.application.ApplicationRoleManager;
import com.atlassian.jira.auditing.ChangedValue;
import com.atlassian.jira.test.util.lic.core.CoreLicenses;
import com.atlassian.jira.test.util.lic.sd.ServiceDeskLicenses;
import com.atlassian.jira.test.util.lic.software.SoftwareLicenses;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static com.atlassian.jira.upgrade.tasks.role.TestUtils.toLicense;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.withSettings;

public class DefaultApplicationServiceImplTest {
    @Rule
    public final MethodRule initMockito = MockitoJUnit.rule();

    @Mock
    private ApplicationRolesDao rolesDao;
    @Mock
    private LicenseDao licenseDao;
    @Mock
    private DefaultApplicationDao defaultApplicationDao;

    private ApplicationRoleManager applicationRoleManager;
    private MockMigrationLogDao mockAuditingService = new MockMigrationLogDao();

    final License swLicense = toLicense(SoftwareLicenses.LICENSE_SOFTWARE);
    final License coreLicense = toLicense(CoreLicenses.LICENSE_CORE);
    final License sdLicense = toLicense(ServiceDeskLicenses.LICENSE_SERVICE_DESK_TBP);

    private DefaultApplicationService defaultApplicationService;

    @Before
    public void setUp() throws Exception {
        applicationRoleManager = mock(ApplicationRoleManager.class, withSettings().extraInterfaces(CachingComponent.class));
        defaultApplicationService = new DefaultApplicationServiceImpl(mockAuditingService, licenseDao, defaultApplicationDao, applicationRoleManager);
    }

    @Test
    public void verifyMigrateClearsRoleCaches() {
        when(licenseDao.getLicenses()).thenReturn(licenses(coreLicense, swLicense, sdLicense));
        defaultApplicationService.setApplicationsAsDefaultDuring6xTo7xUpgrade();
        verify((CachingComponent) applicationRoleManager).clearCache();
    }

    @Test
    public void verifyAuditingLog() {
        when(licenseDao.getLicenses()).thenReturn(licenses(coreLicense, swLicense, sdLicense));
        defaultApplicationService.setApplicationsAsDefaultDuring6xTo7xUpgrade();
        final List<AuditEntry> events = mockAuditingService.getEvents();
        assertEquals("A single event is expected", 1, events.size());

        final AuditEntry auditEntry = events.get(0);
        assertEquals("Setting the application defaults", auditEntry.getSummary());
        assertEquals("Applications were marked as default,"
                        + " based on the product license. New users will be created with respect to the default applications selection.",
                auditEntry.getDescription());
        assertEquals(AuditEntrySeverity.INFO, auditEntry.getSeverity());
        assertEquals("jira-software", auditEntry.getChangedObject());
        assertTrue("Audit log should be visible for OD admins", auditEntry.eventShowsInCloudLog());

        final ChangedValue changedValue = Iterables.getFirst(auditEntry.getChangedValues(), null);
        assertNotNull("Changed value must exist", changedValue);
        assertEquals("jira-software", changedValue.getName());
        assertEquals("", changedValue.getFrom());
        assertEquals("jira-software (default)", changedValue.getTo());
    }

    private Licenses licenses(final License... licenses) {
        final Set<License> licensesSet = ImmutableSet.copyOf(licenses);
        return new Licenses(licensesSet);
    }


    private static class MockMigrationLogDao implements MigrationLogDao {
        private List<AuditEntry> events = new ArrayList<>();

        @Override
        public void write(final MigrationLog record) {
            throw new UnsupportedOperationException("Not implemented");
        }

        @Override
        public void store(final AuditEntry event) {
            events.add(event);
        }

        public List<AuditEntry> getEvents() {
            return events;
        }
    }
}
