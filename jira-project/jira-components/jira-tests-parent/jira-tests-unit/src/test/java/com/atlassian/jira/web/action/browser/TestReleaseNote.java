package com.atlassian.jira.web.action.browser;

import com.atlassian.jira.bc.project.ProjectAction;
import com.atlassian.jira.bc.project.ProjectService;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.mock.component.MockComponentWorker;
import com.atlassian.jira.mock.ofbiz.MockOfBizDelegator;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.project.VersionProxy;
import com.atlassian.jira.project.util.ReleaseNoteManager;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.project.version.VersionImpl;
import com.atlassian.jira.project.version.VersionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.I18nHelper;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static webwork.action.Action.INPUT;

/**
 * Unit test of {@link ReleaseNote}.
 *
 * @since 6.2
 */
public class TestReleaseNote {
    private static final String SUCCESS = "success";
    private static final String SECURITY_BREACH = "securitybreach";

    @Mock
    private I18nHelper mockI18nHelper;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        final JiraAuthenticationContext mockJiraAuthenticationContext = Mockito.mock(JiraAuthenticationContext.class);
        when(mockJiraAuthenticationContext.getI18nHelper()).thenReturn(mockI18nHelper);
        new MockComponentWorker().init()
                .addMock(OfBizDelegator.class, new MockOfBizDelegator())
                .addMock(JiraAuthenticationContext.class, mockJiraAuthenticationContext);
    }

    @After
    public void tearDownWorker() {
        ComponentAccessor.initialiseWorker(null);
    }

    @Test
    public void testGetVersionsCallsProjectManagerCorrectly() throws Exception {
        // Set up
        final VersionManager mockVersionManager = mock(VersionManager.class);

        final long projectId = 79;
        final Version unreleasedVersion = new VersionImpl(projectId, 1001L, "Version 1");

        final Collection<Version> unreleasedVersions = Arrays.asList(unreleasedVersion);
        final ProjectService mockProjectService = mockProjectServiceThatReturnsProject(new MockProject(projectId));
        when(mockVersionManager.getVersionsUnreleased(projectId, false)).thenReturn(unreleasedVersions);
        final Collection<Version> releasedVersions = Collections.emptyList();
        when(mockVersionManager.getVersionsReleased(projectId, false)).thenReturn(releasedVersions);
        final ReleaseNote releaseNote = new ReleaseNote(null, mockProjectService, null, null, mockVersionManager);
        releaseNote.setProjectId(projectId);
        releaseNote.doDefault();
        when(mockI18nHelper.getText("common.filters.unreleasedversions")).thenReturn("Unreleased Versions");

        // Invoke
        final Collection versions = releaseNote.getVersions();

        // Check
        final Collection<?> expectedVersions =
                Arrays.asList(new VersionProxy(-2, "Unreleased Versions"), new VersionProxy(unreleasedVersion));
        assertEquals(expectedVersions, versions);
    }

    @Test
    public void testGetStyleNames() {
        // Set up
        final ReleaseNoteManager mockReleaseNoteManager = mock(ReleaseNoteManager.class);
        when(mockReleaseNoteManager.getStyles()).thenReturn(ImmutableMap.of("text", "text-template", "html", "html-template"));
        final ReleaseNote releaseNote = new ReleaseNote(null, null, mockReleaseNoteManager, null, null);

        // Invoke
        final Collection<?> styleNames = releaseNote.getStyleNames();

        // Check
        assertEquals(2, styleNames.size());
        assertTrue(styleNames.contains("text"));
        assertTrue(styleNames.contains("html"));
    }

    @Test
    public void testInputResultForInvalidVersion() throws Exception {
        assertInvalidVersion(null, null);
        assertInvalidVersion("-1", "");
        assertInvalidVersion("-2", "");
        assertInvalidVersion("-3", "");
    }

    private void assertInvalidVersion(final String version, final String styleName) throws Exception {
        // Set up
        final String errorMessage = "anything";
        when(mockI18nHelper.getText("releasenotes.version.select")).thenReturn(errorMessage);

        final ReleaseNote releaseNote = new ReleaseNote(null, null, null, null, null);
        releaseNote.setVersion(version);
        releaseNote.setStyleName(styleName);

        // Invoke
        final String result = releaseNote.execute();

        // Check
        assertEquals(INPUT, result);
        assertEquals(errorMessage, releaseNote.getErrors().get("version"));
    }

    @Test
    public void testGetterSetters() {
        final long projectId = 1L;
        final String styleName = "test-style";
        final String version = "test-version";

        final ProjectManager mockProjectManager = mock(ProjectManager.class);
        final ReleaseNote releaseNote = new ReleaseNote(mockProjectManager, null, null, null, null);

        releaseNote.setProjectId(projectId);
        assertEquals(projectId, releaseNote.getProjectId());

        releaseNote.setStyleName(styleName);
        assertEquals(styleName, releaseNote.getStyleName());

        releaseNote.setVersion(version);
        assertEquals(version, releaseNote.getVersion());
    }

    @Test
    public void doExecuteIsSuccessfulIfUserHasBrowsePermissionForProject() throws Exception {
        final ReleaseNote releaseNote = new ReleaseNote(null, mockAllowingProjectService(), null, null, null);
        assertThat(releaseNote.doExecute(), is(SUCCESS));
    }

    @Test
    public void doExecuteResultsInSecurityBreachIfUserLacksBrowsePermissionForProject() throws Exception {
        final ReleaseNote releaseNote = new ReleaseNote(null, mockDenyingProjectService(), null, null, null);
        assertThat(releaseNote.doExecute(), is(SECURITY_BREACH));
    }

    @Test
    public void doConfigureIsSuccessfulIfUserHasBrowsePermissionForProject() throws Exception {
        final ReleaseNote releaseNote = new ReleaseNote(null,
                mockAllowingProjectService(), mockReleaseNoteManagerWithStyles(), null, mockVersionManagerWithVersions());
        assertThat(releaseNote.doConfigure(), is(SUCCESS));
    }

    @Test
    public void doConfigureResultsInSecurityBreachIfUserLacksBrowsePermissionForProject() throws Exception {
        final ReleaseNote releaseNote = new ReleaseNote(null,
                mockDenyingProjectService(), mockReleaseNoteManagerWithStyles(), null, mockVersionManagerWithVersions());
        assertThat(releaseNote.doConfigure(), is(SECURITY_BREACH));
    }

    private ReleaseNoteManager mockReleaseNoteManagerWithStyles() {
        final ReleaseNoteManager mockReleaseNoteManager = mock(ReleaseNoteManager.class);

        ImmutableMap<String, String> styles = ImmutableMap.of("key1", "value1", "key2", "value2");
        when(mockReleaseNoteManager.getStyles()).thenReturn(styles);

        return mockReleaseNoteManager;
    }

    private VersionManager mockVersionManagerWithVersions() {
        final VersionManager mockVersionManager = mock(VersionManager.class);

        when(mockVersionManager.getVersionsReleased(anyLong(), anyBoolean())).thenReturn(ImmutableList.of(mock(Version.class)));
        when(mockVersionManager.getVersionsUnreleased(anyLong(), anyBoolean())).thenReturn(ImmutableList.of(mock(Version.class)));

        return mockVersionManager;
    }

    private ProjectService mockAllowingProjectService() {
        return mockProjectService(mock(Project.class), true);
    }

    private ProjectService mockDenyingProjectService() {
        return mockProjectService(mock(Project.class), false);
    }

    private ProjectService mockProjectServiceThatReturnsProject(final Project project) {
        return mockProjectService(project, true);
    }

    private ProjectService mockProjectService(final Project project, final boolean isUserAllowedToBrowseProject) {
        final ProjectService.GetProjectResult result = mock(ProjectService.GetProjectResult.class);
        if (isUserAllowedToBrowseProject) {
            when(result.getProject()).thenReturn(project);
        }

        final ProjectService projectService = mock(ProjectService.class);
        when(projectService.getProjectByIdForAction(Mockito.<ApplicationUser>any(), anyLong(), eq(ProjectAction.VIEW_PROJECT))).thenReturn(result);

        return projectService;
    }
}
