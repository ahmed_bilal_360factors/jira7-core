package com.atlassian.jira.issue.fields.screen.issuetype;

import com.atlassian.jira.bc.ServiceOutcome;
import com.atlassian.jira.bc.project.ProjectAction;
import com.atlassian.jira.bc.project.ProjectService;
import com.atlassian.jira.issue.fields.screen.FieldScreenScheme;
import com.atlassian.jira.issue.fields.screen.MockFieldScreenScheme;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.mock.issue.fields.screen.issuetype.MockIssueTypeScreenScheme;
import com.atlassian.jira.mock.ofbiz.MockGenericValue;
import com.atlassian.jira.mock.security.MockSimpleAuthenticationContext;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectFactory;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.security.plugin.ProjectPermissionKey;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.util.NoopI18nHelper;
import com.atlassian.jira.util.collect.MapBuilder;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.event.PluginEventManager;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import com.google.common.collect.Sets;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.ofbiz.core.entity.GenericValue;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.empty;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @since v4.4
 */
public class TestDefaultProjectIssueTypeScreenSchemeHelper {
    @Rule
    public final RuleChain mockContainer = MockitoMocksInContainer.forTest(this);

    @Mock
    @AvailableInContainer
    private PluginAccessor pluginAccessor;
    @Mock
    @AvailableInContainer
    private PluginEventManager pluginEventManager;
    @Mock
    @AvailableInContainer
    private GlobalPermissionManager globalPermissionManager;

    private JiraAuthenticationContext authenticationContext;

    @Mock
    private IssueTypeScreenSchemeManager issueTypeScreenSchemeManager;
    private MockApplicationUser user;

    @Mock
    private ProjectService projectService;

    @Before
    public void setUp() throws Exception {
        user = new MockApplicationUser("mtan");
        authenticationContext = new MockSimpleAuthenticationContext(user, Locale.ENGLISH, new NoopI18nHelper());
    }


    @Test
    public void testGetActiveFieldScreenSchemesWithInvalidServiceResult() {
        @SuppressWarnings("unchecked") final ServiceOutcome<List<Project>> invalidServiceOutcome = mock(ServiceOutcome.class);
        when(invalidServiceOutcome.isValid()).thenReturn(false);
        when(projectService.getAllProjectsForAction(user, ProjectAction.EDIT_PROJECT_CONFIG)).thenReturn(invalidServiceOutcome);

        final FieldScreenScheme fieldScreenScheme = mock(FieldScreenScheme.class);


        final ProjectIssueTypeScreenSchemeHelper projectIssueTypeScreenSchemeHelper = new DefaultProjectIssueTypeScreenSchemeHelper(projectService,
                authenticationContext, issueTypeScreenSchemeManager, null, null);

        final Multimap<FieldScreenScheme, Project> activefieldScreenSchemes = projectIssueTypeScreenSchemeHelper
                .getProjectsForFieldScreenSchemes(Sets.<FieldScreenScheme>newHashSet(fieldScreenScheme));

        assertThat(activefieldScreenSchemes.entries(), empty());

        final List<Project> projectsForprojectsForFieldScreenScheme = projectIssueTypeScreenSchemeHelper.getProjectsForFieldScreenScheme(fieldScreenScheme);

        assertThat(projectsForprojectsForFieldScreenScheme, empty());
    }

    @Test
    public void testGetActiveFieldScreenSchemesWithValidServiceResult() {
        final MockGenericValue projectGV = new MockGenericValue("lala");
        final MockProject project = new MockProject(888L, "aaa", "aaa", projectGV)
                .setIssueTypes("Bug", "Task");

        final MockGenericValue unrelatedProjectGV = new MockGenericValue("lalala");
        final MockProject otherProject = new MockProject(777L, "bbb", "bbb", unrelatedProjectGV)
                .setIssueTypes("Bug", "Task");

        final FieldScreenScheme fieldScreenScheme = mock(FieldScreenScheme.class);

        final MockIssueTypeScreenScheme issueTypeScreenScheme = new MockIssueTypeScreenScheme(6868L, "lala", "lala");
        final IssueTypeScreenSchemeEntity mockIssueTypeScreenSchemeEntity = mock(IssueTypeScreenSchemeEntity.class);
        when(mockIssueTypeScreenSchemeEntity.getFieldScreenScheme()).thenReturn(fieldScreenScheme);
        issueTypeScreenScheme.setEntities(MapBuilder.<String, IssueTypeScreenSchemeEntity>newBuilder()
                .add(null, mockIssueTypeScreenSchemeEntity)
                .toMap());

        when(issueTypeScreenSchemeManager.getIssueTypeScreenScheme(project))
                .thenReturn(issueTypeScreenScheme);

        final MockIssueTypeScreenScheme unrelatedIssueTypeScreenScheme = new MockIssueTypeScreenScheme(9393L, "haha", "haha");
        final IssueTypeScreenSchemeEntity mockUnrelatedIssueTypeScreenSchemeEntity = mock(IssueTypeScreenSchemeEntity.class);
        final MockFieldScreenScheme otherFieldScreenScheme = new MockFieldScreenScheme();
        when(mockUnrelatedIssueTypeScreenSchemeEntity.getFieldScreenScheme()).thenReturn(otherFieldScreenScheme);
        unrelatedIssueTypeScreenScheme.setEntities(MapBuilder.<String, IssueTypeScreenSchemeEntity>newBuilder()
                .add(null, mockUnrelatedIssueTypeScreenSchemeEntity)
                .add("Bug", mockIssueTypeScreenSchemeEntity)
                .toMap());

        when(issueTypeScreenSchemeManager.getIssueTypeScreenScheme(otherProject))
                .thenReturn(unrelatedIssueTypeScreenScheme);

        @SuppressWarnings("unchecked") final ServiceOutcome<List<Project>> validServiceOutcome = mock(ServiceOutcome.class);
        when(validServiceOutcome.isValid()).thenReturn(true);
        when(validServiceOutcome.getReturnedValue()).thenReturn(
                Lists.<Project>newArrayList(
                        project, otherProject
                )
        );

        when(projectService.getAllProjectsForAction(user,
                ProjectAction.EDIT_PROJECT_CONFIG)).thenReturn(validServiceOutcome);

        final ProjectIssueTypeScreenSchemeHelper projectIssueTypeScreenSchemeHelper =
                new DefaultProjectIssueTypeScreenSchemeHelper(projectService, authenticationContext, issueTypeScreenSchemeManager, null, null);

        final Multimap<FieldScreenScheme, Project> activeFieldScreenSchemes = projectIssueTypeScreenSchemeHelper
                .getProjectsForFieldScreenSchemes(Sets.<FieldScreenScheme>newHashSet(fieldScreenScheme));

        assertThat(activeFieldScreenSchemes.keySet(), contains(fieldScreenScheme));
        assertThat(activeFieldScreenSchemes.get(fieldScreenScheme), contains(project, otherProject));

        List<Project> projectsForprojectsForFieldScreenScheme = projectIssueTypeScreenSchemeHelper.getProjectsForFieldScreenScheme(fieldScreenScheme);
        assertThat(projectsForprojectsForFieldScreenScheme, contains(project, otherProject));

        projectsForprojectsForFieldScreenScheme = projectIssueTypeScreenSchemeHelper.getProjectsForFieldScreenScheme(otherFieldScreenScheme);
        assertThat(projectsForprojectsForFieldScreenScheme, contains(otherProject));
    }

    @Test
    public void testGetActiveFieldScreenSchemesWithValidServiceResultButNoMatchingFieldScreenSchemes() {
        final MockGenericValue projectGV = new MockGenericValue("lala");
        final MockProject project = new MockProject(888L, "aaa", "aaa", projectGV)
                .setIssueTypes("Bug", "Task");

        final FieldScreenScheme fieldScreenScheme = mock(FieldScreenScheme.class);

        final MockIssueTypeScreenScheme issueTypeScreenScheme = new MockIssueTypeScreenScheme(6868L, "lala", "lala");
        final IssueTypeScreenSchemeEntity mockIssueTypeScreenSchemeEntity = mock(IssueTypeScreenSchemeEntity.class);
        when(mockIssueTypeScreenSchemeEntity.getFieldScreenScheme()).thenReturn(fieldScreenScheme);
        issueTypeScreenScheme.setEntities(MapBuilder.<String, IssueTypeScreenSchemeEntity>newBuilder()
                .add(null, mockIssueTypeScreenSchemeEntity)
                .toMap());

        when(issueTypeScreenSchemeManager.getIssueTypeScreenScheme(project))
                .thenReturn(issueTypeScreenScheme);

        final MockFieldScreenScheme otherFieldScreenScheme = new MockFieldScreenScheme();

        @SuppressWarnings("unchecked") final ServiceOutcome<List<Project>> validServiceOutcome = mock(ServiceOutcome.class);
        when(validServiceOutcome.isValid()).thenReturn(true);
        when(validServiceOutcome.getReturnedValue()).thenReturn(
                Lists.<Project>newArrayList(
                        project
                )
        );

        when(projectService.getAllProjectsForAction(user, ProjectAction.EDIT_PROJECT_CONFIG))
                .thenReturn(validServiceOutcome);

        final ProjectIssueTypeScreenSchemeHelper projectIssueTypeScreenSchemeHelper =
                new DefaultProjectIssueTypeScreenSchemeHelper(projectService, authenticationContext, issueTypeScreenSchemeManager, null, null);

        final Multimap<FieldScreenScheme, Project> activeFieldScreenSchemes = projectIssueTypeScreenSchemeHelper
                .getProjectsForFieldScreenSchemes(Sets.<FieldScreenScheme>newHashSet(fieldScreenScheme, otherFieldScreenScheme));

        assertThat(activeFieldScreenSchemes.keySet(), contains(fieldScreenScheme));

        // Didn't have any associated projects
        Collection<Project> multimapProjectsForFieldScreenScheme = activeFieldScreenSchemes.get(otherFieldScreenScheme);
        assertThat(multimapProjectsForFieldScreenScheme, empty());

        List<Project> projectsForprojectsForFieldScreenScheme = projectIssueTypeScreenSchemeHelper.getProjectsForFieldScreenScheme(otherFieldScreenScheme);
        assertThat(projectsForprojectsForFieldScreenScheme, empty());

        // Did have associated projects
        multimapProjectsForFieldScreenScheme = activeFieldScreenSchemes.get(fieldScreenScheme);
        assertThat(multimapProjectsForFieldScreenScheme, contains(project));

        projectsForprojectsForFieldScreenScheme = projectIssueTypeScreenSchemeHelper.getProjectsForFieldScreenScheme(fieldScreenScheme);
        assertThat(projectsForprojectsForFieldScreenScheme, contains(project));
    }

    @Test
    public void testGetActiveFieldScreenSchemesWithValidServiceResultAndNoProjects() {
        final FieldScreenScheme fieldScreenScheme = mock(FieldScreenScheme.class);

        final MockIssueTypeScreenScheme issueTypeScreenScheme = new MockIssueTypeScreenScheme(6868L, "lala", "lala");
        final IssueTypeScreenSchemeEntity mockIssueTypeScreenSchemeEntity = mock(IssueTypeScreenSchemeEntity.class);
        when(mockIssueTypeScreenSchemeEntity.getFieldScreenScheme()).thenReturn(fieldScreenScheme);
        issueTypeScreenScheme.setEntities(MapBuilder.<String, IssueTypeScreenSchemeEntity>newBuilder()
                .add(null, mockIssueTypeScreenSchemeEntity)
                .toMap());

        @SuppressWarnings("unchecked") final ServiceOutcome<List<Project>> validServiceOutcome = mock(ServiceOutcome.class);
        when(validServiceOutcome.isValid()).thenReturn(true);
        when(validServiceOutcome.getReturnedValue()).thenReturn(
                Collections.<Project>emptyList()
        );

        when(projectService.getAllProjectsForAction(user, ProjectAction.EDIT_PROJECT_CONFIG))
                .thenReturn(validServiceOutcome);

        final ProjectIssueTypeScreenSchemeHelper projectIssueTypeScreenSchemeHelper = new DefaultProjectIssueTypeScreenSchemeHelper(projectService,
                authenticationContext, issueTypeScreenSchemeManager, null, null);

        final Multimap<FieldScreenScheme, Project> activeFieldScreenSchemes = projectIssueTypeScreenSchemeHelper
                .getProjectsForFieldScreenSchemes(Sets.<FieldScreenScheme>newHashSet(fieldScreenScheme));

        assertThat(activeFieldScreenSchemes.keySet(), empty());

        final List<Project> projectsForFieldScreenScheme = projectIssueTypeScreenSchemeHelper.getProjectsForFieldScreenScheme(fieldScreenScheme);

        assertThat(projectsForFieldScreenScheme, empty());
    }

    @Test
    public void testGetProjectsForScheme() {
        final MockProject project1 = new MockProject(101928282L, "ONE");
        final MockProject project2 = new MockProject(35438590L, "TWO");

        final List<GenericValue> genericValues = Arrays.asList(project1.getGenericValue(), project2.getGenericValue());

        final MockIssueTypeScreenScheme issueTypeScreenScheme = new MockIssueTypeScreenScheme(6868L, "lala", "lala")
                .setProjects(genericValues);

        final List<Project> associatedProjects = Lists.<Project>newArrayList(project1, project2);

        final ProjectFactory projectFactory = mock(ProjectFactory.class);
        when(projectFactory.getProjects(genericValues)).thenReturn(associatedProjects);
        final PermissionManager permissionManager = mock(PermissionManager.class);
        when(globalPermissionManager.isGlobalPermission(Permissions.Permission.PROJECT_ADMIN.getId())).thenReturn(false);
        when(globalPermissionManager.isGlobalPermission(Permissions.Permission.ADMINISTER.getId())).thenReturn(true);
        when(
                permissionManager.hasPermission(
                        Mockito.any(ProjectPermissionKey.class),
                        Mockito.eq(project1),
                        Mockito.any(ApplicationUser.class)))
                .thenReturn(true);

        final ProjectIssueTypeScreenSchemeHelper projectIssueTypeScreenSchemeHelper = new DefaultProjectIssueTypeScreenSchemeHelper(projectService,
                authenticationContext, issueTypeScreenSchemeManager, permissionManager, projectFactory);

        final List<Project> projects = projectIssueTypeScreenSchemeHelper.getProjectsForScheme(issueTypeScreenScheme);
        assertThat(projects, contains(project1));
    }

}
