package com.atlassian.jira.security.auth.trustedapps;

import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.security.auth.trustedapps.CurrentApplication;
import com.atlassian.security.auth.trustedapps.TrustedApplication;
import com.atlassian.security.auth.trustedapps.TrustedApplicationsManager;
import org.junit.Before;
import org.junit.Test;

import java.security.PublicKey;
import java.util.concurrent.atomic.AtomicBoolean;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.withSettings;

public class TestSeraphTrustedApplicationsManager {
    private CurrentApplication mockCurrentApplication;

    private JiraAuthenticationContext jiraAuthContext;

    @Before
    @SuppressWarnings("deprecation")
    public void prepareJiraAuthContext() throws Exception {
        jiraAuthContext = mock(JiraAuthenticationContext.class);
        when(jiraAuthContext.getLoggedInUser()).thenReturn(new MockApplicationUser("admin", "admin", "admin@example.com"));
        mockCurrentApplication = mock(CurrentApplication.class, withSettings().extraInterfaces(TrustedApplication.class));
    }

    @Test
    public void testReturnsInfoIfFound() throws Exception {
        final TrustedApplicationInfo info = new MockTrustedApplicationInfo(1, "appId", "TheApp", 10000);
        final TrustedApplicationsManager manager = new SeraphTrustedApplicationsManager(new MockTrustedApplicationManager(info),
                new MockCurrentApplicationFactory(mockCurrentApplication), jiraAuthContext);

        final TrustedApplication trustedApplication = manager.getTrustedApplication("appId");
        assertNotNull(trustedApplication);
        assertTrue(((TrustedApplicationInfo) trustedApplication).isValidKey());
    }

    @Test
    public void testReturnsNullIfNotFound() throws Exception {
        final TrustedApplicationInfo info = new MockTrustedApplicationInfo(1, "appId", "TheApp", 10000);
        when(mockCurrentApplication.getID()).thenReturn("thecurrentapp");
        final TrustedApplicationsManager manager = new SeraphTrustedApplicationsManager(new MockTrustedApplicationManager(info),
                new MockCurrentApplicationFactory(mockCurrentApplication), jiraAuthContext);

        final TrustedApplication trustedApplication = manager.getTrustedApplication("nonExistantId");
        assertNull(trustedApplication);
    }

    @Test
    public void testReturnsCurrentApp() throws Exception {
        final TrustedApplicationInfo info = new MockTrustedApplicationInfo(1, "appId", "TheApp", 10000);
        when(mockCurrentApplication.getID()).thenReturn("thecurrentapp");
        final TrustedApplicationsManager manager = new SeraphTrustedApplicationsManager(new MockTrustedApplicationManager(info),
                new MockCurrentApplicationFactory(mockCurrentApplication), jiraAuthContext);

        final TrustedApplication trustedApplication = manager.getTrustedApplication("thecurrentapp");
        assertSame(mockCurrentApplication, trustedApplication);
    }

    // JRA-24743, configured trusted apps *must* override current app, for Studio
    @Test
    public void testConfiguredTrustedAppsOverrideCurrentApp() {
        final TrustedApplicationInfo info = new MockTrustedApplicationInfo(1, "appId", "TheApp", 10000);
        when(mockCurrentApplication.getID()).thenReturn("appId");
        final TrustedApplicationsManager manager = new SeraphTrustedApplicationsManager(new MockTrustedApplicationManager(info),
                new MockCurrentApplicationFactory(mockCurrentApplication), jiraAuthContext);

        final TrustedApplication trustedApplication = manager.getTrustedApplication("appId");
        assertTrue(((TrustedApplicationInfo) trustedApplication).isValidKey());
        assertNotSame(mockCurrentApplication, trustedApplication);
        assertEquals(10000, ((TrustedApplicationInfo) trustedApplication).getTimeout());
    }

    @Test
    public void testReturnsNullIfInvalidKey() throws Exception {
        when(mockCurrentApplication.getID()).thenReturn("thecurrentapp");
        final TrustedApplicationInfo info = new MockTrustedApplicationInfo(1, "appId", "TheApp", 10000) {
            @Override
            public boolean isValidKey() {
                return false;
            }

            @Override
            public PublicKey getPublicKey() {
                return new KeyFactory.InvalidPublicKey(new RuntimeException("bad key"));
            }
        };
        final TrustedApplicationsManager manager = new SeraphTrustedApplicationsManager(new MockTrustedApplicationManager(info),
                new MockCurrentApplicationFactory(mockCurrentApplication), jiraAuthContext);

        final TrustedApplication trustedApplication = manager.getTrustedApplication("appId");
        assertNull(trustedApplication);
    }

    @Test
    public void testUsesApplicationFactory() throws Exception {
        final AtomicBoolean called = new AtomicBoolean(false);
        final TrustedApplicationsManager manager = new SeraphTrustedApplicationsManager(new MockTrustedApplicationManager(),
                new CurrentApplicationFactory() {
                    public CurrentApplication getCurrentApplication() {
                        called.set(true);
                        return null;
                    }
                }, jiraAuthContext);
        assertNull(manager.getCurrentApplication());
        assertTrue(called.get());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testConstructionFailsIfManagerNull() {
        new SeraphTrustedApplicationsManager(null, mock(CurrentApplicationFactory.class), jiraAuthContext);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testConstructionFailsIfFactoryNull() {
        new SeraphTrustedApplicationsManager(mock(TrustedApplicationManager.class), null, jiraAuthContext);
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructorShouldThrowIllegalArgumentExceptionIfJiraAuthContextIsNull() {
        new SeraphTrustedApplicationsManager(mock(TrustedApplicationManager.class), mock(CurrentApplicationFactory.class), null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testConstructionFailsIfAllNull() {
        new SeraphTrustedApplicationsManager(null, null, null);
    }

    public static class MockCurrentApplicationFactory implements CurrentApplicationFactory {
        private CurrentApplication application;

        public MockCurrentApplicationFactory(CurrentApplication application) {
            this.application = application;
        }

        @Override
        public CurrentApplication getCurrentApplication() {
            return application;
        }
    }
}
