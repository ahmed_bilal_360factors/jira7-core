package com.atlassian.jira.bc.project;

import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.template.ProjectTemplateKey;
import com.atlassian.jira.project.type.ProjectTypeKey;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNot.not;
import static org.hamcrest.core.IsSame.sameInstance;

@RunWith(MockitoJUnitRunner.class)
public class TestProjectCreationDataBuilder {
    @Mock
    private ApplicationUser user;

    @Test
    public void buildsTheProjectCreationDataAsExpected() {
        ProjectCreationData data = buildProjectCreationData();

        assertProjectCreationDataIsCorrect(data);
    }

    @Test
    public void canCreateAProjectCreationDataUsingAnotherAsAStartingPoint() {
        ProjectCreationData template = buildProjectCreationData();

        ProjectCreationData data = new ProjectCreationData.Builder().from(template).build();

        assertThat(data, not(sameInstance(template)));
        assertProjectCreationDataIsCorrect(data);
    }

    @Test
    public void canCreateAProjectCreationDataWithExistingProject() {
        Project existingProject = createMockProject();
        ProjectCreationData originalProjectCreationData = buildProjectCreationData();

        ProjectCreationData data = new ProjectCreationData.Builder().fromExistingProject(existingProject, originalProjectCreationData).build();

        assertThat(data, not(sameInstance(originalProjectCreationData)));
        assertProjectCreationDataBasedOnProject(data);
        assertThat(data.getAssigneeType(), is(1L));
        assertThat(data.getLead(), is(new MockApplicationUser("lead")));
    }

    @Test
    public void existingProjectLeadAndAssigneeWillBeSetWhenInputDataDoesntContainThem() {
        Project existingProject = createMockProject();
        ProjectCreationData originalProjectCreationData = buildProjectCreationData();
        ProjectCreationData dataMissingLeadAndAssigneeType = new ProjectCreationData.Builder().from(originalProjectCreationData).withAssigneeType(null).withLead(null).build();

        ProjectCreationData data = new ProjectCreationData.Builder().fromExistingProject(existingProject, dataMissingLeadAndAssigneeType).build();

        assertThat(data, not(sameInstance(dataMissingLeadAndAssigneeType)));
        assertProjectCreationDataBasedOnProject(data);
        assertThat(data.getAssigneeType(), is(30L));
        assertThat(data.getLead(), is(new MockApplicationUser("dude")));
    }

    @Test
    public void nullProjectTemplateKeyDoesntGetSet() {
        ProjectCreationData data = new ProjectCreationData.Builder().withProjectTemplateKey(null).build();

        assertThat(data.getProjectTemplateKey(), nullValue());
    }

    private Project createMockProject() {
        MockProject existingProject = new MockProject(10020L);
        existingProject.setProjectTypeKey("software");
        existingProject.setAssigneeType(30L);
        existingProject.setLead(new MockApplicationUser("dude"));
        return existingProject;
    }

    private ProjectCreationData buildProjectCreationData() {
        return new ProjectCreationData.Builder()
                .withName("name")
                .withKey("key")
                .withDescription("desc")
                .withType("type")
                .withProjectTemplateKey("some-template")
                .withLead(new MockApplicationUser("lead"))
                .withUrl("url")
                .withAssigneeType(1L)
                .withAvatarId(2L)
                .build();
    }

    private void assertProjectCreationDataBasedOnProject(final ProjectCreationData data) {
        assertThat(data.getName(), is("name"));
        assertThat(data.getKey(), is("key"));
        assertThat(data.getDescription(), is("desc"));
        assertThat(data.getProjectTypeKey(), is(new ProjectTypeKey("software")));
        assertThat(data.getProjectTemplateKey(), nullValue());
        assertThat(data.getUrl(), is("url"));
        assertThat(data.getAvatarId(), is(2L));
    }

    private void assertProjectCreationDataIsCorrect(ProjectCreationData data) {
        assertThat(data.getName(), is("name"));
        assertThat(data.getKey(), is("key"));
        assertThat(data.getDescription(), is("desc"));
        assertThat(data.getProjectTypeKey(), is(new ProjectTypeKey("type")));
        assertThat(data.getProjectTemplateKey(), is(new ProjectTemplateKey("some-template")));
        assertThat(data.getLead(), is(new MockApplicationUser("lead")));
        assertThat(data.getUrl(), is("url"));
        assertThat(data.getAssigneeType(), is(1L));
        assertThat(data.getAvatarId(), is(2L));
    }
}
