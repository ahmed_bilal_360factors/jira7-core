package com.atlassian.jira.web.filters;

import com.atlassian.jira.junit.rules.InitMockitoMocks;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.mockito.Mock;

import javax.servlet.http.HttpServletRequest;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestJiraCachingStrategy {
    @Rule
    public TestRule injectMocks = new InitMockitoMocks(this);

    @Mock
    private HttpServletRequest servletRequest;
    private JiraCachingFilter.JiraCachingStrategy strategy = new JiraCachingFilter.JiraCachingStrategy();


    //JSPA should not be cached.
    @Test
    public void testMatchesJSPA() {
        when(servletRequest.getServletPath()).thenReturn("/secure/MyJiraHome.jspa");
        when(servletRequest.isSecure()).thenReturn(false);
        when(servletRequest.getRequestURI()).thenReturn("/secure/MyJiraHome.jspa");

        final boolean nonCachableUri = strategy.matches(servletRequest);
        assertTrue(nonCachableUri);
    }

    //JSP should also not be cached.
    @Test
    public void testMatchesJSP() {
        when(servletRequest.getServletPath()).thenReturn("/secure/viewissue.jsp");
        when(servletRequest.isSecure()).thenReturn(false);
        when(servletRequest.getRequestURI()).thenReturn("/secure/viewissue.jsp");

        final boolean nonCachableUriJsp = strategy.matches(servletRequest);
        assertTrue(nonCachableUriJsp);
    }

    //attachments over secure should not add no-cache headers
    @Test
    public void testMatchesAttachmentSecure() {
        when(servletRequest.getServletPath()).thenReturn("/attachment/");
        when(servletRequest.isSecure()).thenReturn(true);

        final boolean nonCachableUriAttachmentSecure = strategy.matches(servletRequest);
        assertFalse(nonCachableUriAttachmentSecure);
    }

    //attachments over non-secure should not add no cache headers
    @Test
    public void testMatchesAttachment() {
        when(servletRequest.getServletPath()).thenReturn("/attachment/");
        when(servletRequest.isSecure()).thenReturn(false);
        when(servletRequest.getRequestURI()).thenReturn("/attachment/");

        final boolean nonCachableUriAttachment = strategy.matches(servletRequest);
        assertFalse(nonCachableUriAttachment);
    }

    //Browse should add no-cache headers
    @Test
    public void testMatchesBrowse() {
        when(servletRequest.getServletPath()).thenReturn("/browse/JRA-123123");
        when(servletRequest.isSecure()).thenReturn(false);

        final boolean nonCachableUriBrowse = strategy.matches(servletRequest);
        assertTrue(nonCachableUriBrowse);
    }

    //Browse should add no-cache headers
    @Test
    public void testMatchesOther() {
        when(servletRequest.getServletPath()).thenReturn("/someotherurl");
        when(servletRequest.isSecure()).thenReturn(false);
        when(servletRequest.getRequestURI()).thenReturn("/someotherurl");

        final boolean nonCachableUriBrowse = strategy.matches(servletRequest);
        assertFalse(nonCachableUriBrowse);
    }
}
