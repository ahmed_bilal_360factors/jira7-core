package com.atlassian.jira.soy;

import com.atlassian.jira.help.MockHelpUrl;
import com.atlassian.jira.help.MockHelpUrls;
import com.atlassian.soy.renderer.JsExpression;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

public class HelpUrlFunctionTest {

    private MockHelpUrls helpUrls;
    private AbstractHelpFunction abstractHelpFunction;

    @Before
    public void setUp() {
        helpUrls = new MockHelpUrls();
        helpUrls.addUrl(new MockHelpUrl().setUrl("https://docs.atlassian.com/jira/jadm-docs-123/test").setKey("key1"));
        helpUrls.addUrl(new MockHelpUrl().setUrl("/jira/admin/help").setKey("key2"));
        abstractHelpFunction = new HelpUrlFunction(helpUrls);
    }

    @Test
    public void testGenerateUrl() {
        final String urlForKey1 = abstractHelpFunction.generate(new JsExpression("'key1'")).getText();
        assertThat(urlForKey1, equalTo("\"https:\\/\\/docs.atlassian.com\\/jira\\/jadm-docs-123\\/test\""));

        final String urlForKey2 = abstractHelpFunction.generate(new JsExpression("'key2'")).getText();
        assertThat(urlForKey2, equalTo("\"\\/jira\\/admin\\/help\""));
    }

    @Test
    public void testApplyUrl() {
        final String urlForKey1 = abstractHelpFunction.apply("key1");
        assertThat(urlForKey1, equalTo("https://docs.atlassian.com/jira/jadm-docs-123/test"));

        final String urlForKey2 = abstractHelpFunction.apply("key2");
        assertThat(urlForKey2, equalTo("/jira/admin/help"));
    }

}
