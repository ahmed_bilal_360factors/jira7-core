package com.atlassian.jira.web.action.util;

import com.atlassian.jira.config.IssueTypeManager;
import com.atlassian.jira.config.MockIssueConstantFactory;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.fields.config.manager.IssueTypeSchemeManager;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.issue.issuetype.MockIssueType;
import com.atlassian.jira.issue.search.ReaderCache;
import com.atlassian.jira.issue.search.SearchProvider;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.mock.ofbiz.MockOfBizDelegator;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.util.collect.CollectionBuilder;
import com.atlassian.jira.web.FieldVisibilityManager;
import org.hamcrest.Matchers;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.mockito.Mock;

import java.util.List;
import java.util.Set;

import static com.atlassian.core.util.collection.EasyList.build;
import static java.util.Collections.emptySet;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestPopularIssueTypesUtilImpl {
    @Rule
    public TestRule container = MockitoMocksInContainer.forTest(this);

    @Mock
    @AvailableInContainer
    private SearchProvider searchProvider;
    @Mock
    @AvailableInContainer
    private FieldVisibilityManager fieldVisibilityManager;
    @Mock
    @AvailableInContainer
    private ReaderCache readerCache;
    @Mock
    @AvailableInContainer
    private FieldManager fieldManager;
    @Mock
    @AvailableInContainer
    private IssueTypeManager issueTypeManager;
    @Mock
    @AvailableInContainer
    private ProjectManager projectManager;

    private static final MockIssueType ISSUE_TYPE_1 = new MockIssueType("1", "Not Popular 1", false);
    private static final MockIssueType ISSUE_TYPE_2 = new MockIssueType("2", "Not Popular 2", false);
    private static final MockIssueType ISSUE_TYPE_3 = new MockIssueType("3", "Not Popular 3", false);
    private static final MockIssueType ISSUE_TYPE_4 = new MockIssueType("4", "Not Popular 4", false);

    private static final MockIssueType SUB_ISSUE_TYPE_1 = new MockIssueType("11", "Sub Not Popular 1", true);
    private static final MockIssueType SUB_ISSUE_TYPE_2 = new MockIssueType("22", "Sub Not Popular 2", true);
    private OfBizDelegator ofBizDelegator = new MockOfBizDelegator();

    @Test
    public void testNoPopularTypesForUserButEnoughFromProject() throws Exception {
        MockProject project = new MockProject(1l);

        IssueTypeSchemeManager issueTypeSchemeManager = mock(IssueTypeSchemeManager.class);
        when(issueTypeSchemeManager.getNonSubTaskIssueTypesForProject(project)).thenReturn(build(ISSUE_TYPE_1, ISSUE_TYPE_2));

        PopularIssueTypesUtilImpl util = new PopularIssueTypesUtilImpl(issueTypeSchemeManager, new MockIssueConstantFactory(ofBizDelegator));

        final List<IssueType> list = util.getPopularIssueTypesForProject(project, new MockApplicationUser("admin"));
        assertEquals(2, list.size());
        assertEquals(ISSUE_TYPE_1, list.get(0));
        assertEquals(ISSUE_TYPE_2, list.get(1));
    }

    @Test
    public void testNoPopularTypesForUserButTooManyFromProject() throws Exception {
        MockProject project = new MockProject(1l);
        IssueTypeSchemeManager issueTypeSchemeManager = mock(IssueTypeSchemeManager.class);
        when(issueTypeSchemeManager.getNonSubTaskIssueTypesForProject(project)).thenReturn(build(ISSUE_TYPE_1, ISSUE_TYPE_2, ISSUE_TYPE_3, ISSUE_TYPE_4));

        PopularIssueTypesUtilImpl util = new PopularIssueTypesUtilImpl(issueTypeSchemeManager, new MockIssueConstantFactory(ofBizDelegator));
        final List<IssueType> list = util.getPopularIssueTypesForProject(project, new MockApplicationUser("admin"));

        assertThat(list, Matchers.contains(ISSUE_TYPE_1, ISSUE_TYPE_2));
    }

    @Test
    public void testOnlySubTaskPopularTypesForUserButEnoughFromProject() throws Exception {
        MockProject project = new MockProject(1l);
        IssueTypeSchemeManager issueTypeSchemeManager = mock(IssueTypeSchemeManager.class);
        when(issueTypeSchemeManager.getNonSubTaskIssueTypesForProject(project)).thenReturn(build(ISSUE_TYPE_1, ISSUE_TYPE_2, ISSUE_TYPE_3, ISSUE_TYPE_4));

        PopularIssueTypesUtilImpl util = new PopularIssueTypesUtilImpl(issueTypeSchemeManager, new MockIssueConstantFactory(ofBizDelegator)) {
            Set<IssueType> getPopularIssueTypesFromSearch(final Project project, final ApplicationUser user, final String period) {
                if (user != null) {
                    return CollectionBuilder.<IssueType>newBuilder(SUB_ISSUE_TYPE_1, SUB_ISSUE_TYPE_2).asListOrderedSet();
                } else {
                    return CollectionBuilder.<IssueType>newBuilder(ISSUE_TYPE_3, ISSUE_TYPE_4).asListOrderedSet();
                }
            }
        };

        final List<IssueType> list = util.getPopularIssueTypesForProject(project, new MockApplicationUser("admin"));

        assertThat(list, Matchers.contains(ISSUE_TYPE_3, ISSUE_TYPE_4));
    }

    @Test
    public void testNotEnoughPopularTypesForUser() throws Exception {
        final MockProject project = new MockProject(100L);
        IssueTypeSchemeManager issueTypeSchemeManager = mock(IssueTypeSchemeManager.class);
        when(issueTypeSchemeManager.getNonSubTaskIssueTypesForProject(project)).thenReturn(build(ISSUE_TYPE_4, ISSUE_TYPE_1, ISSUE_TYPE_2, ISSUE_TYPE_3));

        PopularIssueTypesUtilImpl util = new PopularIssueTypesUtilImpl(issueTypeSchemeManager, new MockIssueConstantFactory(ofBizDelegator));

        final List<IssueType> list = util.getPopularIssueTypesForProject(project, new MockApplicationUser("admin"));
        assertEquals(2, list.size());
        assertEquals(ISSUE_TYPE_4, list.get(0));
        assertEquals(ISSUE_TYPE_1, list.get(1));
    }

    @Test
    public void testNotEnoughPopularTypesForProjectOrUser() throws Exception {
        final MockProject project = new MockProject(100L);
        IssueTypeSchemeManager issueTypeSchemeManager = mock(IssueTypeSchemeManager.class);
        when(issueTypeSchemeManager.getNonSubTaskIssueTypesForProject(project)).thenReturn(build(ISSUE_TYPE_2, ISSUE_TYPE_3, ISSUE_TYPE_4, ISSUE_TYPE_1));

        PopularIssueTypesUtilImpl util = new PopularIssueTypesUtilImpl(issueTypeSchemeManager, new MockIssueConstantFactory(ofBizDelegator)) {
            Set<IssueType> getPopularIssueTypesFromSearch(final Project project, final ApplicationUser user, final String period) {
                return emptySet();
            }
        };

        final List<IssueType> list = util.getPopularIssueTypesForProject(project, new MockApplicationUser("admin"));
        assertEquals(2, list.size());
        assertEquals(ISSUE_TYPE_2, list.get(0));
        assertEquals(ISSUE_TYPE_3, list.get(1));
    }

    @Test
    public void testOtherIssueTypes() throws Exception {
        final MockProject project = new MockProject(100L);
        IssueTypeSchemeManager issueTypeSchemeManager = mock(IssueTypeSchemeManager.class);
        when(issueTypeSchemeManager.getNonSubTaskIssueTypesForProject(project)).thenReturn(build(ISSUE_TYPE_4, ISSUE_TYPE_1, ISSUE_TYPE_2, ISSUE_TYPE_3));

        PopularIssueTypesUtilImpl util = new PopularIssueTypesUtilImpl(issueTypeSchemeManager, new MockIssueConstantFactory(ofBizDelegator)) {
            Set<IssueType> getPopularIssueTypesFromSearch(final Project project, final ApplicationUser user, final String period) {
                return CollectionBuilder.<IssueType>newBuilder(ISSUE_TYPE_1, ISSUE_TYPE_2).asListOrderedSet();
            }
        };

        final List<IssueType> list = util.getOtherIssueTypesForProject(new MockProject(100L), new MockApplicationUser("admin"));

        assertThat(list, Matchers.contains(ISSUE_TYPE_4, ISSUE_TYPE_3));
    }

    @Test
    public void testConglomerateMethod() {
        final MockProject project = new MockProject(100L);
        IssueTypeSchemeManager issueTypeSchemeManager = mock(IssueTypeSchemeManager.class);
        when(issueTypeSchemeManager.getNonSubTaskIssueTypesForProject(project)).thenReturn(build(ISSUE_TYPE_4, ISSUE_TYPE_1, ISSUE_TYPE_2, ISSUE_TYPE_3));

        PopularIssueTypesUtilImpl util = new PopularIssueTypesUtilImpl(issueTypeSchemeManager, new MockIssueConstantFactory(ofBizDelegator)) {
            Set<IssueType> getPopularIssueTypesFromSearch(final Project project, final ApplicationUser user, final String period) {
                return CollectionBuilder.<IssueType>newBuilder(ISSUE_TYPE_1, ISSUE_TYPE_2).asListOrderedSet();
            }
        };

        final PopularIssueTypesUtil.PopularIssueTypesHolder lists = util.getPopularAndOtherIssueTypesForProject(new MockProject(100L), new MockApplicationUser("admin"));

        List<IssueType> list = lists.getOtherIssueTypes();
        assertThat(list, Matchers.contains(ISSUE_TYPE_4, ISSUE_TYPE_3));

        list = lists.getPopularIssueTypes();
        assertThat(list, Matchers.contains(ISSUE_TYPE_1, ISSUE_TYPE_2));
    }

    @Test
    public void testOverByOneCondition() {
        final MockProject project = new MockProject(100L);
        IssueTypeSchemeManager issueTypeSchemeManager = mock(IssueTypeSchemeManager.class);
        when(issueTypeSchemeManager.getNonSubTaskIssueTypesForProject(project)).thenReturn(build(ISSUE_TYPE_1, ISSUE_TYPE_2, ISSUE_TYPE_3));


        PopularIssueTypesUtilImpl util = new PopularIssueTypesUtilImpl(issueTypeSchemeManager, new MockIssueConstantFactory(ofBizDelegator));

        final PopularIssueTypesUtil.PopularIssueTypesHolder lists = util.getPopularAndOtherIssueTypesForProject(new MockProject(100L), new MockApplicationUser("admin"));
        List<IssueType> list = lists.getOtherIssueTypes();
        assertTrue(list.isEmpty());

        list = lists.getPopularIssueTypes();
        assertThat(list, Matchers.contains(ISSUE_TYPE_1, ISSUE_TYPE_2, ISSUE_TYPE_3));
    }

    @Test
    public void testOverByOneConditionNotEnough() {
        final MockProject project = new MockProject(100L);
        IssueTypeSchemeManager issueTypeSchemeManager = mock(IssueTypeSchemeManager.class);
        when(issueTypeSchemeManager.getNonSubTaskIssueTypesForProject(project)).thenReturn(build(ISSUE_TYPE_1, ISSUE_TYPE_2, ISSUE_TYPE_3));

        PopularIssueTypesUtilImpl util = new PopularIssueTypesUtilImpl(issueTypeSchemeManager, new MockIssueConstantFactory(ofBizDelegator)) {
            Set<IssueType> getPopularIssueTypesFromSearch(final Project project, final ApplicationUser user, final String period) {
                return CollectionBuilder.<IssueType>newBuilder(ISSUE_TYPE_2, ISSUE_TYPE_1).asListOrderedSet();
            }
        };

        final PopularIssueTypesUtil.PopularIssueTypesHolder lists = util.getPopularAndOtherIssueTypesForProject(new MockProject(100L), new MockApplicationUser("admin"));
        List<IssueType> list = lists.getOtherIssueTypes();
        assertTrue(list.isEmpty());

        list = lists.getPopularIssueTypes();
        assertThat(list, Matchers.contains(ISSUE_TYPE_2, ISSUE_TYPE_1, ISSUE_TYPE_3));
    }

}
