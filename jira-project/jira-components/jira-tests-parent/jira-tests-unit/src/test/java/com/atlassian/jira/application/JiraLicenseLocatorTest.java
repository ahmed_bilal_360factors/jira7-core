package com.atlassian.jira.application;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.jira.junit.rules.MockitoContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.matchers.OptionMatchers;
import com.atlassian.sal.api.license.LicenseHandler;
import com.atlassian.sal.api.license.SingleProductLicenseDetailsView;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mockito;

import static com.atlassian.jira.matchers.OptionMatchers.some;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.sameInstance;
import static org.mockito.Mockito.when;

public class JiraLicenseLocatorTest {
    @Rule
    public MockitoContainer mockitoContainer = MockitoMocksInContainer.rule(this);

    @Test
    public void noneReturnedWhenLicenseHandlerNotAvailable() {
        final JiraLicenseLocator locator = new JiraLicenseLocator();

        assertThat(locator.apply(ApplicationKey.valueOf("testKey")), OptionMatchers.none());
    }

    @Test
    public void noneReturnedWhenLicenseHandlerReturnsNull() {
        final LicenseHandler handler = Mockito.mock(LicenseHandler.class);
        mockitoContainer.getMockComponentContainer().addMock(LicenseHandler.class, handler);

        final JiraLicenseLocator locator = new JiraLicenseLocator();
        assertThat(locator.apply(ApplicationKey.valueOf("testKey")), OptionMatchers.none());
    }

    @Test
    public void noneReturnedWhenApplicationKeyIsNull() {
        final LicenseHandler handler = Mockito.mock(LicenseHandler.class);
        mockitoContainer.getMockComponentContainer().addMock(LicenseHandler.class, handler);

        final JiraLicenseLocator locator = new JiraLicenseLocator();
        assertThat(locator.apply(null), OptionMatchers.none());
    }

    @Test
    public void someReturnedWhenLicenseHandlerReturnsValue() {
        final ApplicationKey testKey = ApplicationKey.valueOf("testKey");
        final LicenseHandler handler = Mockito.mock(LicenseHandler.class);
        final SingleProductLicenseDetailsView view = Mockito.mock(SingleProductLicenseDetailsView.class);

        when(handler.getProductLicenseDetails(testKey.value())).thenReturn(view);

        mockitoContainer.getMockComponentContainer().addMock(LicenseHandler.class, handler);
        final JiraLicenseLocator locator = new JiraLicenseLocator();
        assertThat(locator.apply(testKey), some(sameInstance(view)));
    }
}