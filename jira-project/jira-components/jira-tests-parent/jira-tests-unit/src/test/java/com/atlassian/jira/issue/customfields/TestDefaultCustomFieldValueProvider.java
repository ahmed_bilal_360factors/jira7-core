package com.atlassian.jira.issue.customfields;

import com.atlassian.jira.issue.customfields.view.CustomFieldParams;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.transport.FieldValuesHolder;
import com.atlassian.jira.issue.transport.impl.FieldValuesHolderImpl;
import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.Mockito;

import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestDefaultCustomFieldValueProvider {
    @Test
    public void testGetStringValue() throws Exception {
        final FieldValuesHolder fieldValuesHolder = new FieldValuesHolderImpl();
        final CustomFieldParams customFieldParams = Mockito.mock(CustomFieldParams.class);
        final CustomField customField = Mockito.mock(CustomField.class);
        final CustomFieldType customFieldType = Mockito.mock(CustomFieldType.class);

        when(customField.getCustomFieldValues(fieldValuesHolder)).thenReturn(customFieldParams);
        when(customField.getCustomFieldType()).thenReturn(customFieldType);
        when(customFieldType.getStringValueFromCustomFieldParams(customFieldParams)).thenReturn(null);

        final DefaultCustomFieldValueProvider customFieldValueProvider = new DefaultCustomFieldValueProvider();
        customFieldValueProvider.getStringValue(customField, fieldValuesHolder);

        InOrder inOrder = inOrder(customField, customFieldType);

        inOrder.verify(customField).getCustomFieldType();
        inOrder.verify(customFieldType).getStringValueFromCustomFieldParams(customFieldParams);
    }

    @Test
    public void testGetValue() throws Exception {
        final FieldValuesHolder fieldValuesHolder = new FieldValuesHolderImpl();
        final CustomFieldParams customFieldParams = Mockito.mock(CustomFieldParams.class);
        final CustomField customField = Mockito.mock(CustomField.class);
        final CustomFieldType customFieldType = Mockito.mock(CustomFieldType.class);

        when(customField.getCustomFieldValues(fieldValuesHolder)).thenReturn(customFieldParams);
        when(customFieldType.getValueFromCustomFieldParams(customFieldParams)).thenReturn(null);
        when(customField.getCustomFieldType()).thenReturn(customFieldType);

        final DefaultCustomFieldValueProvider customFieldValueProvider = new DefaultCustomFieldValueProvider();
        customFieldValueProvider.getValue(customField, fieldValuesHolder);

        InOrder inOrder = inOrder(customField, customFieldType);

        inOrder.verify(customField).getCustomFieldType();
        inOrder.verify(customFieldType).getValueFromCustomFieldParams(customFieldParams);
    }
}
