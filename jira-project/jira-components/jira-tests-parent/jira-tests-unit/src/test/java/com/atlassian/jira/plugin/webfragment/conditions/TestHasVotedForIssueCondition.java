package com.atlassian.jira.plugin.webfragment.conditions;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.vote.VoteManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

public class TestHasVotedForIssueCondition {
    @Rule
    public MockitoRule mockito = MockitoJUnit.rule();
    @Mock
    private Issue issue;
    @Mock
    private VoteManager voteManager;
    private HasVotedForIssueCondition condition;

    @Before
    public void setUp() throws Exception {
        condition = new HasVotedForIssueCondition(voteManager);
    }

    @Test
    public void testTrue() {
        final ApplicationUser fred = new MockApplicationUser("fred");
        when(voteManager.hasVoted(fred, issue)).thenReturn(true);

        assertTrue(condition.shouldDisplay(fred, issue, null));
    }

    @Test
    public void testNullUser() {
        assertFalse(condition.shouldDisplay(null, issue, null));
    }

    @Test
    public void testFalseEmpty() {
        final ApplicationUser fred = new MockApplicationUser("fred");
        when(voteManager.hasVoted(fred, issue)).thenReturn(false);

        assertFalse(condition.shouldDisplay(fred, issue, null));
    }
}
