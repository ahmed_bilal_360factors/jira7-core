package com.atlassian.jira.jql.resolver;

import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.issue.resolution.ResolutionImpl;
import com.atlassian.jira.mock.ofbiz.MockGenericValue;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestStatusResolver {
    @Rule
    public final MethodRule initMockito = MockitoJUnit.rule();

    @Mock
    private ConstantsManager constantsManager;

    @InjectMocks
    private ResolutionResolver resolutionResolver;

    @Test
    public void testGetAll() throws Exception {
        // when:
        resolutionResolver.getAll();

        // then:
        verify(constantsManager).getResolutionObjects();
    }

    @Test
    public void testNameIsCorrect() throws Exception {
        // given:
        when(constantsManager.getConstantByNameIgnoreCase(ConstantsManager.RESOLUTION_CONSTANT_TYPE, "test"))
                .thenReturn(new ResolutionImpl(new MockGenericValue("blah"), null, null, null));

        // when:
        final boolean result = resolutionResolver.nameExists("test");

        // then:
        assertTrue("ConstantManager was not called with expected parameters!", result);
    }
}
