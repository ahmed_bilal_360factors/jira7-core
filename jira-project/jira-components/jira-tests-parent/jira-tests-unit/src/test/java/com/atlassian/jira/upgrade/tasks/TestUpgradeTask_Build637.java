package com.atlassian.jira.upgrade.tasks;

import com.atlassian.jira.bc.admin.ApplicationPropertiesService;
import com.atlassian.jira.bc.admin.ApplicationProperty;
import com.atlassian.jira.bc.admin.ApplicationPropertyMetadata;
import com.atlassian.jira.config.properties.ApplicationPropertiesStore;
import com.atlassian.jira.config.util.JiraHome;
import com.atlassian.validation.Success;
import com.atlassian.validation.Validated;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicBoolean;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Unit test for {@link UpgradeTask_Build637}.
 *
 * @since v4.4
 */
public class TestUpgradeTask_Build637 {

    @Test
    public void testUpgrade() throws Exception {
        final ApplicationProperty xsrf = createProperty("jira.table.cols.subtasks", "foo");
        final ApplicationProperty projPattern = createProperty("jira.projectkey.pattern", ".*");

        final ApplicationPropertiesService service = mock(ApplicationPropertiesService.class);
        when(service.getApplicationProperty("jira.table.cols.subtasks")).thenReturn(xsrf);
        when(service.getApplicationProperty("jira.projectkey.pattern")).thenReturn(projPattern);
        when(service.getApplicationProperty("somekey")).thenReturn(createProperty("somekey", "somevalue"));
        when(service.getApplicationProperty("extra.prop")).thenReturn(createProperty("extra.prop", "extra.value.default"));
        when(service.getApplicationProperty("extra.prop2")).thenReturn(createProperty("extra.prop2", "extra.value2.default"));
        when(service.getApplicationProperty("jira.home")).thenReturn(createProperty("jira.home", "/opt/wherever/"));
        when(service.setApplicationProperty("jira.table.cols.subtasks", "definitely")).thenReturn(createValidatedProperty("jira.table.cols.subtasks", "definitely"));
        when(service.setApplicationProperty("jira.projectkey.pattern", "[A-Z]{3,}")).thenReturn(createValidatedProperty("jira.projectkey.pattern", "[A-Z]{3,}"));

        final Properties p = new Properties();

        p.setProperty("jira.table.cols.subtasks", "definitely");
        p.setProperty("jira.projectkey.pattern", "[A-Z]{3,}");
        // these are destined for the overlay file
        p.setProperty("extra.prop", "extra.value.new");
        p.setProperty("extra.prop2", "extra.value2.new");
        // this is left as default
        p.setProperty("somekey", "somevalue");

        // this is not migratable and not for the overlay file
        p.setProperty("jira.home", "D:/some/crazy path");

        final ByteArrayInputStream propsIs = toInputStream(p);

        final JiraHome jiraHome = mock(JiraHome.class);
        when(jiraHome.getHomePath()).thenReturn("D:/some/crazy path");

        final AtomicBoolean refreshed = new AtomicBoolean(false);

        final ApplicationPropertiesStore store = mock(ApplicationPropertiesStore.class);


        final UpgradeTask_Build637 task = new UpgradeTask_Build637(service, store, jiraHome) {
            @Override
            InputStream getLegacyPropertiesStream() {
                return propsIs;
            }

            @Override
            void handleFailure(final String key, final String property, final Exception e) {
                super.handleFailure(key, property, e);
                throw new RuntimeException("exploding on failure in test", e);
            }

            @Override
            void writeOverlayFile(final Properties overlayProps) throws IOException {
                assertTrue(overlayProps.getProperty("extra.prop").equals("extra.value.new"));
                assertTrue(overlayProps.getProperty("extra.prop2").equals("extra.value2.new"));
                assertNull("jira home should not be put in the overlay file in the home directory, duh",
                        overlayProps.getProperty("jira.home"));
                assertEquals(2, overlayProps.size());
            }
        };

        task.doUpgrade(false);

        verify(store).refresh();
    }

    static ApplicationProperty createProperty(final String key, final String value) {
        return createValidatedProperty(key, value).getValue();
    }

    static Validated<ApplicationProperty> createValidatedProperty(final String key, final String value) {
        final ApplicationPropertyMetadata md = new ApplicationPropertyMetadata.Builder()
                .key(key)
                .type("string")
                .defaultValue(value)
                .validatorName(null)
                .sysAdminEditable(true)
                .requiresRestart(false)
                .name(key + " name")
                .desc(key + " desc")
                .build();
        final ApplicationProperty tApplicationProperty = new ApplicationProperty(md, value);
        return new Validated<>(new Success(value), tApplicationProperty);
    }

    private ByteArrayInputStream toInputStream(final Properties p) throws IOException {
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        p.store(byteArrayOutputStream, "yo mama");
        return new ByteArrayInputStream(byteArrayOutputStream.toByteArray());
    }

}
