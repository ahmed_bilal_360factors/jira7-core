package com.atlassian.jira.issue.fields.csv.custom;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.customfields.CustomFieldType;
import com.atlassian.jira.issue.export.FieldExportParts;
import com.atlassian.jira.issue.export.customfield.CustomFieldExportContext;
import com.atlassian.jira.issue.export.customfield.ExportableCustomFieldType;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.csv.util.StubCsvDateFormatter;
import com.atlassian.jira.issue.views.util.csv.CsvDateFormatter;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;

import static java.util.stream.Collectors.toList;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

public abstract class CustomFieldCsvExporterTest<T> {
    protected final CsvDateFormatter csvDateFormatter = new StubCsvDateFormatter();
    protected final DateTime now = DateTime.now();

    protected final CustomFieldExportContext context = mock(CustomFieldExportContext.class);

    private CustomFieldType<T, ?> field;

    @Before
    public void setUp() {
        field = spy(createField());

        when(context.getCustomField()).thenReturn(mock(CustomField.class));
        when(context.getDefaultColumnHeader()).thenReturn("Stub header");
    }

    abstract protected CustomFieldType<T, ?> createField();

    protected final void whenFieldValueIs(T fieldValue) {
        doReturn(fieldValue).when(field).getValueFromIssue(any(CustomField.class), any(Issue.class));
    }

    protected void assertExportedValue(final String... exportedValue) {
        FieldExportParts representation = ((ExportableCustomFieldType) field).getRepresentationFromIssue(null, context);

        if (exportedValue.length > 0) {
            assertThat(representation.getParts().get(0).getValues()::iterator, contains(exportedValue));
        } else {
            assertThat(representation.getParts(), hasSize(1));
            assertThat(representation.getParts().get(0).getValues().collect(toList()), hasSize(0));
        }
    }

    @Test
    abstract public void testExportWithNullValue();
}
