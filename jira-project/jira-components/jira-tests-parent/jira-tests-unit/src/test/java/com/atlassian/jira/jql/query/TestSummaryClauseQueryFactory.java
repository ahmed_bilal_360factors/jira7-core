package com.atlassian.jira.jql.query;

import com.atlassian.jira.jql.operand.JqlOperandResolver;
import org.apache.lucene.search.Query;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestSummaryClauseQueryFactory {
    @Rule
    public final MethodRule initMockito = MockitoJUnit.rule();

    @Mock
    private ClauseQueryFactory mockClauseQueryFactory;

    @Mock
    private Query mockQuery;

    @Test
    public void testGetQueryHappyPath() throws Exception {
        final QueryFactoryResult queryFactoryResult = new QueryFactoryResult(mockQuery);

        when(mockClauseQueryFactory.getQuery(null, null)).thenReturn(queryFactoryResult);

        SummaryClauseQueryFactory summaryClauseQueryFactory = new SummaryClauseQueryFactory(null) {
            @Override
            ClauseQueryFactory getDelegate(final JqlOperandResolver operandSupport) {
                return mockClauseQueryFactory;
            }
        };

        summaryClauseQueryFactory.getQuery(null, null);

        verify(mockQuery).setBoost(SummaryClauseQueryFactory.SUMMARY_BOOST_FACTOR);
    }

    @Test
    public void testGetQueryNullQuery() throws Exception {
        when(mockClauseQueryFactory.getQuery(null, null)).thenReturn(null);

        SummaryClauseQueryFactory summaryClauseQueryFactory = new SummaryClauseQueryFactory(null) {
            @Override
            ClauseQueryFactory getDelegate(final JqlOperandResolver operandSupport) {
                return mockClauseQueryFactory;
            }
        };

        assertNull(summaryClauseQueryFactory.getQuery(null, null));
    }

}
