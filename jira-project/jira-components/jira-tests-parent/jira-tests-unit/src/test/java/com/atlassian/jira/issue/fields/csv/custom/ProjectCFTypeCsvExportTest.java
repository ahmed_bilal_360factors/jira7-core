package com.atlassian.jira.issue.fields.csv.custom;

import com.atlassian.jira.issue.customfields.impl.AbstractSingleFieldType;
import com.atlassian.jira.issue.customfields.impl.ProjectCFType;
import com.atlassian.jira.junit.rules.InitMockitoMocks;
import com.atlassian.jira.project.Project;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.mockito.Mock;

import static org.mockito.Mockito.when;

public class ProjectCFTypeCsvExportTest extends CustomFieldSingleValueCsvExporterTest<Project> {
    @Rule
    public TestRule initMockito = new InitMockitoMocks(this);

    @Mock
    private Project project;

    @Override
    protected AbstractSingleFieldType<Project> createField() {
        return new ProjectCFType(null, null, null, null, null, null, null);
    }

    @Test
    public void projectNameIsExported() {
        when(project.getName()).thenReturn("Project name");
        whenFieldValueIs(project);

        assertExportedValue("Project name");
    }
}
