package com.atlassian.jira.imports.project.transformer;

import com.atlassian.jira.action.issue.customfields.MockProjectImportableCustomFieldType;
import com.atlassian.jira.imports.project.customfield.ExternalCustomFieldValue;
import com.atlassian.jira.imports.project.customfield.ExternalCustomFieldValueImpl;
import com.atlassian.jira.imports.project.customfield.ProjectCustomFieldImporter;
import com.atlassian.jira.imports.project.mapper.ProjectImportMapper;
import com.atlassian.jira.imports.project.mapper.ProjectImportMapperImpl;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.context.IssueContextImpl;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

/**
 * @since v3.13
 */
public class TestCustomFieldValueTransformerImpl {

    @Rule
    public final MethodRule initMockito = MockitoJUnit.rule();

    @Mock
    CustomField customField;
    @Mock
    CustomFieldManager customFieldManager;
    @Mock
    FieldConfig fieldConfig;
    @Mock
    ProjectCustomFieldImporter mockProjectCustomFieldImporter;

    private ProjectImportMapper projectImportMapper;

    @Before
    public void setUp() throws Exception {
        projectImportMapper = new ProjectImportMapperImpl(null, null);
    }

    @Test
    public void testTransformWithIgnoredCustomField() throws Exception {
        projectImportMapper.getCustomFieldMapper().ignoreCustomField("12");

        final ExternalCustomFieldValueImpl externalCustomFieldValue = new ExternalCustomFieldValueImpl("123", "12", "432");

        final CustomFieldValueTransformerImpl customFieldValueTransformer = new CustomFieldValueTransformerImpl(null);
        assertNull(customFieldValueTransformer.transform(projectImportMapper, externalCustomFieldValue, 1L));
    }

    @Test
    public void testTransformWithNotMappedIssue() throws Exception {
        projectImportMapper.getCustomFieldMapper().mapValue("12", "14");
        when(customFieldManager.getCustomFieldObject(14L)).thenReturn(customField);

        final ExternalCustomFieldValueImpl externalCustomFieldValue = new ExternalCustomFieldValueImpl("123", "12", "432");

        final CustomFieldValueTransformerImpl customFieldValueTransformer = new CustomFieldValueTransformerImpl(customFieldManager);
        assertNull(customFieldValueTransformer.transform(projectImportMapper, externalCustomFieldValue, 1L));
        verifyZeroInteractions(customField);
    }

    @Test
    public void testTransformWithNullTransformedValue() throws Exception {
        projectImportMapper.getCustomFieldMapper().mapValue("12", "14");
        projectImportMapper.getCustomFieldMapper().flagValueAsRequired("12", "432");
        projectImportMapper.getCustomFieldMapper().flagIssueTypeInUse("432", "1");
        projectImportMapper.getCustomFieldMapper().registerIssueTypesInUse();
        projectImportMapper.getIssueTypeMapper().mapValue("1", "2");
        projectImportMapper.getIssueMapper().mapValue("432", "765");

        final ExternalCustomFieldValueImpl externalCustomFieldValue = new ExternalCustomFieldValueImpl("123", "12", "432");

        when(mockProjectCustomFieldImporter.getMappedImportValue(projectImportMapper, externalCustomFieldValue, fieldConfig))
                .thenReturn(new ProjectCustomFieldImporter.MappedCustomFieldValue(null));

        final MockProjectImportableCustomFieldType mockProjectImportableCustomFieldType = new MockProjectImportableCustomFieldType(mockProjectCustomFieldImporter);

        when(customField.getRelevantConfig(new IssueContextImpl(1L, "2"))).thenReturn(fieldConfig);
        when(customField.getCustomFieldType()).thenReturn(mockProjectImportableCustomFieldType);
        when(customFieldManager.getCustomFieldObject(14L)).thenReturn(customField);

        final CustomFieldValueTransformerImpl customFieldValueTransformer = new CustomFieldValueTransformerImpl(customFieldManager);
        assertNull(customFieldValueTransformer.transform(projectImportMapper, externalCustomFieldValue, 1L));
        verifyZeroInteractions(fieldConfig);
    }

    @Test
    public void testTransformHappyPathStringValue() throws Exception {
        projectImportMapper.getCustomFieldMapper().mapValue("12", "14");
        projectImportMapper.getCustomFieldMapper().flagValueAsRequired("12", "432");
        projectImportMapper.getCustomFieldMapper().flagIssueTypeInUse("432", "1");
        projectImportMapper.getCustomFieldMapper().registerIssueTypesInUse();
        projectImportMapper.getIssueTypeMapper().mapValue("1", "2");
        projectImportMapper.getIssueMapper().mapValue("432", "765");

        final ExternalCustomFieldValueImpl externalCustomFieldValue = new ExternalCustomFieldValueImpl("123", "12", "432");
        externalCustomFieldValue.setStringValue("hello world");

        when(mockProjectCustomFieldImporter.getMappedImportValue(projectImportMapper, externalCustomFieldValue, fieldConfig))
                .thenReturn(new ProjectCustomFieldImporter.MappedCustomFieldValue("goodbye world"));

        final MockProjectImportableCustomFieldType mockProjectImportableCustomFieldType = new MockProjectImportableCustomFieldType(mockProjectCustomFieldImporter);

        when(customField.getRelevantConfig(new IssueContextImpl(1L, "2"))).thenReturn(fieldConfig);
        when(customField.getCustomFieldType()).thenReturn(mockProjectImportableCustomFieldType);

        when(customFieldManager.getCustomFieldObject(14L)).thenReturn(customField);

        final CustomFieldValueTransformerImpl customFieldValueTransformer = new CustomFieldValueTransformerImpl(customFieldManager);
        final ExternalCustomFieldValue customFieldValue = customFieldValueTransformer.transform(projectImportMapper, externalCustomFieldValue, 1L);

        assertEquals("goodbye world", customFieldValue.getStringValue());
        verifyZeroInteractions(fieldConfig);
    }

    @Test
    public void testTransformHappyPathTextValue() throws Exception {
        projectImportMapper.getCustomFieldMapper().mapValue("12", "14");
        projectImportMapper.getCustomFieldMapper().flagValueAsRequired("12", "432");
        projectImportMapper.getCustomFieldMapper().flagIssueTypeInUse("432", "1");
        projectImportMapper.getCustomFieldMapper().registerIssueTypesInUse();
        projectImportMapper.getIssueTypeMapper().mapValue("1", "2");
        projectImportMapper.getIssueMapper().mapValue("432", "765");

        final ExternalCustomFieldValueImpl externalCustomFieldValue = new ExternalCustomFieldValueImpl("123", "12", "432");
        externalCustomFieldValue.setTextValue("hello world");

        when(mockProjectCustomFieldImporter.getMappedImportValue(projectImportMapper, externalCustomFieldValue, fieldConfig))
                .thenReturn(new ProjectCustomFieldImporter.MappedCustomFieldValue("goodbye world"));

        final MockProjectImportableCustomFieldType mockProjectImportableCustomFieldType = new MockProjectImportableCustomFieldType(mockProjectCustomFieldImporter);

        when(customField.getRelevantConfig(new IssueContextImpl(1L, "2"))).thenReturn(fieldConfig);
        when(customField.getCustomFieldType()).thenReturn(mockProjectImportableCustomFieldType);

        when(customFieldManager.getCustomFieldObject(14L)).thenReturn(customField);

        final CustomFieldValueTransformerImpl customFieldValueTransformer = new CustomFieldValueTransformerImpl(customFieldManager);
        final ExternalCustomFieldValue customFieldValue = customFieldValueTransformer.transform(projectImportMapper, externalCustomFieldValue, 1L);

        assertEquals("goodbye world", customFieldValue.getTextValue());
        verifyZeroInteractions(fieldConfig);
    }

    @Test
    public void testTransformHappyPathDateValue() throws Exception {
        projectImportMapper.getCustomFieldMapper().mapValue("12", "14");
        projectImportMapper.getCustomFieldMapper().flagValueAsRequired("12", "432");
        projectImportMapper.getCustomFieldMapper().flagIssueTypeInUse("432", "1");
        projectImportMapper.getCustomFieldMapper().registerIssueTypesInUse();
        projectImportMapper.getIssueTypeMapper().mapValue("1", "2");
        projectImportMapper.getIssueMapper().mapValue("432", "765");

        final ExternalCustomFieldValueImpl externalCustomFieldValue = new ExternalCustomFieldValueImpl("123", "12", "432");
        externalCustomFieldValue.setDateValue("hello world");

        when(mockProjectCustomFieldImporter.getMappedImportValue(projectImportMapper, externalCustomFieldValue, fieldConfig))
                .thenReturn(new ProjectCustomFieldImporter.MappedCustomFieldValue("goodbye world"));

        final MockProjectImportableCustomFieldType mockProjectImportableCustomFieldType = new MockProjectImportableCustomFieldType(mockProjectCustomFieldImporter);

        when(customField.getRelevantConfig(new IssueContextImpl(1L, "2"))).thenReturn(fieldConfig);
        when(customField.getCustomFieldType()).thenReturn(mockProjectImportableCustomFieldType);

        when(customFieldManager.getCustomFieldObject(14L)).thenReturn(customField);

        final CustomFieldValueTransformerImpl customFieldValueTransformer = new CustomFieldValueTransformerImpl(customFieldManager);
        final ExternalCustomFieldValue customFieldValue = customFieldValueTransformer.transform(projectImportMapper, externalCustomFieldValue, 1L);

        assertEquals("goodbye world", customFieldValue.getDateValue());
        verifyZeroInteractions(fieldConfig);
    }

    @Test
    public void testTransformHappyPathNumberValue() throws Exception {
        projectImportMapper.getCustomFieldMapper().mapValue("12", "14");
        projectImportMapper.getCustomFieldMapper().flagValueAsRequired("12", "432");
        projectImportMapper.getCustomFieldMapper().flagIssueTypeInUse("432", "1");
        projectImportMapper.getCustomFieldMapper().registerIssueTypesInUse();
        projectImportMapper.getIssueTypeMapper().mapValue("1", "2");
        projectImportMapper.getIssueMapper().mapValue("432", "765");

        final ExternalCustomFieldValueImpl externalCustomFieldValue = new ExternalCustomFieldValueImpl("123", "12", "432");
        externalCustomFieldValue.setNumberValue("hello world");
        externalCustomFieldValue.setParentKey("parent old value");

        when(mockProjectCustomFieldImporter.getMappedImportValue(projectImportMapper, externalCustomFieldValue, fieldConfig))
                .thenReturn(new ProjectCustomFieldImporter.MappedCustomFieldValue("goodbye world", "parent new value"));

        final MockProjectImportableCustomFieldType mockProjectImportableCustomFieldType = new MockProjectImportableCustomFieldType(mockProjectCustomFieldImporter);

        when(customField.getRelevantConfig(new IssueContextImpl(1L, "2"))).thenReturn(fieldConfig);
        when(customField.getCustomFieldType()).thenReturn(mockProjectImportableCustomFieldType);

        when(customFieldManager.getCustomFieldObject(14L)).thenReturn(customField);

        final CustomFieldValueTransformerImpl customFieldValueTransformer = new CustomFieldValueTransformerImpl(customFieldManager);
        final ExternalCustomFieldValue customFieldValue = customFieldValueTransformer.transform(projectImportMapper, externalCustomFieldValue, 1L);

        assertEquals("goodbye world", customFieldValue.getNumberValue());
        assertEquals("parent new value", customFieldValue.getParentKey());
        verifyZeroInteractions(fieldConfig);
    }
}
