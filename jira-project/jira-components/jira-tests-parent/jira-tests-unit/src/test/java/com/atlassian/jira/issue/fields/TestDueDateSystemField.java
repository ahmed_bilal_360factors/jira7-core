package com.atlassian.jira.issue.fields;

import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.datetime.DateTimeFormatterFactoryStub;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.customfields.OperationContext;
import com.atlassian.jira.issue.fields.screen.FieldScreenRenderLayoutItem;
import com.atlassian.jira.issue.history.DateTimeFieldChangeLogHelper;
import com.atlassian.jira.issue.search.handlers.DueDateSearchHandlerFactory;
import com.atlassian.jira.issue.views.util.csv.CsvDateFormatter;
import com.atlassian.jira.jql.permission.FieldClausePermissionChecker;
import com.atlassian.jira.jql.query.DueDateClauseQueryFactory;
import com.atlassian.jira.jql.validator.DueDateValidator;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.template.VelocityTemplatingEngine;
import com.atlassian.jira.util.ComponentFactory;
import com.atlassian.jira.util.DateFieldFormatImpl;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.google.common.collect.ImmutableMap;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestDueDateSystemField {

    private DueDateSearchHandlerFactory dueDateSearchHandlerFactory;

    private DueDateSystemField dueDateSystemField;

    @Mock
    private VelocityTemplatingEngine templatingEngine;

    @Mock
    private JiraAuthenticationContext jiraAuthenticationContext;

    private ErrorCollection errorCollection;

    @Before
    public void setUp() {
        dueDateSearchHandlerFactory = new DueDateSearchHandlerFactory(
            mock(ComponentFactory.class),
            mock(DueDateClauseQueryFactory.class),
            mock(DueDateValidator.class),
            mock(FieldClausePermissionChecker.Factory.class)
        );
        dueDateSystemField = new DueDateSystemField(
            templatingEngine,
            mock(ApplicationProperties.class),
            mock(PermissionManager.class),
            jiraAuthenticationContext,
            dueDateSearchHandlerFactory,
            new DateFieldFormatImpl(new DateTimeFormatterFactoryStub()),
            mock(DateTimeFieldChangeLogHelper.class),
            mock(CsvDateFormatter.class)
        );
        errorCollection = new SimpleErrorCollection();
    }

    @Test
    public void validateParamsAcceptsAValidDateStringWithATwoDigitYear() {
        dueDateSystemField.validateParams(
                getOperationContext("1/May/15"),
                errorCollection,
                mock(I18nHelper.class),
                mock(Issue.class),
                mock(FieldScreenRenderLayoutItem.class)
        );
        assertThat(errorCollection.hasAnyErrors(), is(false));
    }

    @Test
    public void validateParamsRejectsANonDateStringWhichDoesNotMatchAnAcceptedDateFormat() {
        dueDateSystemField.validateParams(
                getOperationContext("afsdfdsfd"),
                errorCollection,
                mock(I18nHelper.class),
                mock(Issue.class),
                mock(FieldScreenRenderLayoutItem.class)
        );
        assertThat(errorCollection.hasAnyErrors(), is(true));
    }

    /**
     * JRA-45776 - validator was allowing dates > 9999 that caused exceptions in LuceneUtils.localDateToString
     */
    @Test
    public void validateParamsRejectsADateStringWithAFiveDigitYear() {
        dueDateSystemField.validateParams(
                getOperationContext("1/May/20015"),
                errorCollection,
                mock(I18nHelper.class),
                mock(Issue.class),
                mock(FieldScreenRenderLayoutItem.class)
        );
        assertThat(errorCollection.hasAnyErrors(), is(true));
    }

    private OperationContext getOperationContext(String dueDateStr) {
        OperationContext operationContext = mock(OperationContext.class);
        Map<String, Object> fieldValuesHolder = ImmutableMap.of("duedate", dueDateStr);
        when(operationContext.getFieldValuesHolder()).thenReturn(fieldValuesHolder);
        return operationContext;
    }
}
