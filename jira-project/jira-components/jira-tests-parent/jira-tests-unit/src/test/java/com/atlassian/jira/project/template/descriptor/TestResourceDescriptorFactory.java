package com.atlassian.jira.project.template.descriptor;

import com.atlassian.plugin.Plugin;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.stubbing.Answer;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

public class TestResourceDescriptorFactory {
    @Rule
    public MethodRule initMockito = MockitoJUnit.rule();

    @Mock
    private Plugin plugin;

    private ResourceDescriptorFactory factory;

    @Before
    public void setUp() throws Exception {
        factory = new ResourceDescriptorFactory();
    }

    @Test(expected = FileNotFoundException.class)
    public void createResourceDescriptorThrowsFileNotFoundException() throws Exception {
        when(plugin.getResourceAsStream(anyString())).thenReturn(null);

        factory.createResource(plugin, "name", "/path/to/missing/file", Optional.empty());
    }

    @Test
    public void resourceHasContentTypeParam() throws Exception {
        final String testFile = "/project-templates/pluginLogo.png";
        when(plugin.getResourceAsStream(testFile)).thenAnswer(new NewInputStreamFor(testFile));

        assertThat(factory.createResource(plugin, "name", testFile, Optional.empty()).getParameter("content-type"), equalTo("image/png; charset=binary"));
    }

    @Test
    public void resourceParamCanOverrideContentTypeParam() throws Exception {
        final String testFile = "/project-templates/pluginLogo.png";
        when(plugin.getResourceAsStream(testFile)).thenAnswer(new NewInputStreamFor(testFile));

        assertThat(factory.createResource(plugin, "name", testFile, Optional.of("xml+svg")).getParameter("content-type"), equalTo("xml+svg"));
    }

    private class NewInputStreamFor implements Answer<InputStream> {
        private final String testFile;

        public NewInputStreamFor(String testFile) {
            this.testFile = testFile;
        }

        @Override
        public InputStream answer(InvocationOnMock invocation) throws Throwable {
            return getClass().getResourceAsStream(testFile);
        }
    }
}
