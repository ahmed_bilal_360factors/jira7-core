package com.atlassian.jira.web.action.admin;

import com.atlassian.jira.config.ReindexMessageManager;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.JiraWebActionSupportDependencies;
import com.atlassian.jira.junit.rules.MockitoContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.mock.MockApplicationProperties;
import com.atlassian.jira.mock.security.MockAuthenticationContext;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.JiraDurationUtils;
import com.atlassian.plugin.PluginAccessor;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.mockito.Mock;
import webwork.action.Action;
import webwork.action.ServletActionContext;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class TestTimeTrackingAdmin {
    @Rule
    public final MockitoContainer mockContainer = MockitoMocksInContainer.rule(this);
    @Rule
    public TestRule webActionSupport = JiraWebActionSupportDependencies.build(mockContainer);

    private MockApplicationProperties applicationProperties = new MockApplicationProperties();
    private JiraDurationUtils mockJiraDurationUtils = mock(JiraDurationUtils.class);
    @AvailableInContainer
    private JiraAuthenticationContext authenticationContext = new MockAuthenticationContext(null);

    @Mock
    private FieldManager fieldManager;
    @Mock
    private ReindexMessageManager reindexMessageManager;

    private TimeTrackingAdmin timeTrackingAdmin;

    @Before
    public void setUp() throws Exception {
        applicationProperties.setText(APKeys.JIRA_TIMETRACKING_HOURS_PER_DAY, "24");
        applicationProperties.setText(APKeys.JIRA_TIMETRACKING_DAYS_PER_WEEK, "7");

        timeTrackingAdmin = new TimeTrackingAdmin(
                applicationProperties,
                fieldManager,
                mockJiraDurationUtils,
                authenticationContext,
                mock(PluginAccessor.class),
                reindexMessageManager
        );
    }

    @After
    public void tearDown() throws Exception {
        ServletActionContext.setResponse(null);
    }

    @Test
    public void testGetsSets() {
        timeTrackingAdmin.setHoursPerDay("5");
        assertEquals("5", timeTrackingAdmin.getHoursPerDay());
        timeTrackingAdmin.setHoursPerDay("5.5");
        assertEquals("5.5", timeTrackingAdmin.getHoursPerDay());
        timeTrackingAdmin.setDaysPerWeek("6");
        assertEquals("6", timeTrackingAdmin.getDaysPerWeek());
        timeTrackingAdmin.setDaysPerWeek("6.5");
        assertEquals("6.5", timeTrackingAdmin.getDaysPerWeek());
        timeTrackingAdmin.setDefaultUnit("HOUR");
        assertEquals("HOUR", timeTrackingAdmin.getDefaultUnit());

        timeTrackingAdmin.setDefaultUnit("UNKNOWN");
        assertEquals("MINUTE", timeTrackingAdmin.getDefaultUnit());
    }

    @Test
    public void testDoDefault2() throws Exception {
        applicationProperties.setString(APKeys.JIRA_TIMETRACKING_HOURS_PER_DAY, "10");
        applicationProperties.setString(APKeys.JIRA_TIMETRACKING_DAYS_PER_WEEK, "4");

        timeTrackingAdmin = new TimeTrackingAdmin(
                applicationProperties,
                fieldManager,
                mockJiraDurationUtils,
                null,
                null,
                reindexMessageManager
        );

        assertEquals(Action.INPUT, timeTrackingAdmin.doDefault());
        assertEquals("10", timeTrackingAdmin.getHoursPerDay());
        assertEquals("4", timeTrackingAdmin.getDaysPerWeek());
    }

    @Test
    public void testIsTimeTracking() {
        assertFalse(timeTrackingAdmin.isTimeTracking());

        applicationProperties.setOption(APKeys.JIRA_OPTION_TIMETRACKING, true);
        assertTrue(timeTrackingAdmin.isTimeTracking());
    }

    @Test
    public void testDeactivateCausesFieldsRefresh() throws Exception {
        applicationProperties.setOption(APKeys.JIRA_OPTION_TIMETRACKING, true);

        assertTrue(timeTrackingAdmin.isTimeTracking());

        timeTrackingAdmin.doDeactivate();
        verify(fieldManager).refresh();

        assertFalse(timeTrackingAdmin.isTimeTracking());
    }

    @Test
    public void testActivateCausesFieldRefresh() throws Exception {
        timeTrackingAdmin.doActivate();

        verify(fieldManager).refresh();
        verify(reindexMessageManager).pushMessage(null, "admin.notifications.task.timetracking");
        verify(mockJiraDurationUtils).updateFormatters(applicationProperties, authenticationContext);

        assertTrue(timeTrackingAdmin.isTimeTracking());
    }

    @Test
    public void testActivateWithValues() throws Exception {
        timeTrackingAdmin.setHoursPerDay("7");
        timeTrackingAdmin.setDaysPerWeek("5");

        timeTrackingAdmin.doActivate();

        verify(reindexMessageManager).pushMessage(null, "admin.notifications.task.timetracking");
        verify(fieldManager).refresh();

        assertTrue(timeTrackingAdmin.isTimeTracking());
        assertEquals("7", applicationProperties.getString(APKeys.JIRA_TIMETRACKING_HOURS_PER_DAY));
        assertEquals("5", applicationProperties.getString(APKeys.JIRA_TIMETRACKING_DAYS_PER_WEEK));
    }

    @Test
    public void testInvalidFractionalHoursPerDay() throws Exception {
        timeTrackingAdmin.setHoursPerDay("6.72");
        timeTrackingAdmin.setDaysPerWeek("5");
        assertEquals("error", timeTrackingAdmin.doActivate());
    }

    @Test
    public void testValidFractionalHoursPerDay() throws Exception {
        timeTrackingAdmin.setHoursPerDay("8.5");
        timeTrackingAdmin.setDaysPerWeek("5");
        timeTrackingAdmin.doActivate();

        verify(reindexMessageManager).pushMessage(null, "admin.notifications.task.timetracking");
        verify(fieldManager).refresh();

        assertEquals("8.5", applicationProperties.getString(APKeys.JIRA_TIMETRACKING_HOURS_PER_DAY));
        assertEquals("5", applicationProperties.getString(APKeys.JIRA_TIMETRACKING_DAYS_PER_WEEK));
    }
}
