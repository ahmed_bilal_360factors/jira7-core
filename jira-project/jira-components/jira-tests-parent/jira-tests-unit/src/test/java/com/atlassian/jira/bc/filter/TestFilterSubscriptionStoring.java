package com.atlassian.jira.bc.filter;

import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.bc.JiraServiceContextImpl;
import com.atlassian.jira.exception.DataAccessException;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.issue.subscription.FilterSubscription;
import com.atlassian.jira.issue.subscription.SubscriptionManager;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.util.MockUserManager;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.NoopI18nHelper;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.query.QueryImpl;
import com.atlassian.scheduler.SchedulerService;
import com.atlassian.scheduler.SchedulerServiceException;
import com.atlassian.scheduler.config.Schedule;
import com.atlassian.scheduler.cron.CronSyntaxException;
import com.atlassian.scheduler.cron.ErrorCode;
import com.google.common.collect.ImmutableList;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.ofbiz.core.entity.GenericEntityException;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import static com.atlassian.jira.util.ErrorCollectionAssert.assert1ErrorMessage;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

public class TestFilterSubscriptionStoring {
    @Rule
    public MockitoContainer mockitoContainer = MockitoMocksInContainer.rule(this);

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    private FilterSubscriptionService service;
    private JiraServiceContext context;
    private ApplicationUser user;

    @AvailableInContainer(interfaceClass = UserManager.class)
    private MockUserManager userManager = new MockUserManager();

    @Mock
    private SubscriptionManager subscriptionManager;
    @Mock
    private SchedulerService schedulerService;

    @Before
    public void setUp() throws Exception {
        context = createContext();
        user = new MockApplicationUser("admin");
        userManager.addUser(user);
    }

    @After
    public void tearDown() throws Exception {
        service = null;
        context = null;
        user = null;
    }


    @Test
    public void testSubscriptionStoring() throws SchedulerServiceException {
        final Long filterId = 9999L;
        final String cronExpression = "0 0 0 ? 1 MON";
        when(schedulerService.calculateNextRunTime(any(Schedule.class))).thenReturn(new Date());

        service = createService(subscriptionManager);
        service.storeSubscription(context, filterId, "Group", cronExpression, true);

        verify(subscriptionManager).createSubscription(null, filterId, "Group", cronExpression, true);
    }

    @Test
    public void testSubscriptionStoringInvalidString() throws SchedulerServiceException {
        final Long filterId = 9999L;
        final String cronExpression = "0 0 0 ? 1 NOT";
        final CronSyntaxException expected = CronSyntaxException.builder()
                .cronExpression(cronExpression)
                .errorCode(ErrorCode.INVALID_NAME_DAY_OF_WEEK)
                .value("NOT")
                .build();
        when(schedulerService.calculateNextRunTime(any(Schedule.class))).thenThrow(expected);

        service = createService(subscriptionManager);
        service.storeSubscription(context, filterId, "Group", cronExpression, true);

        assert1ErrorMessage(context.getErrorCollection(), "cron.expression.invalid.INVALID_NAME_DAY_OF_WEEK{[NOT]}");
        verifyZeroInteractions(subscriptionManager);
    }

    @Test
    public void testGetVisibleSubscriptionsForAuthor() {
        final Long filterId = 9999L;
        final SearchRequest filter = new SearchRequest(new QueryImpl(), user, "filter", "filterDesc", filterId, 0L);
        final List<FilterSubscription> expected = ImmutableList.of(Mockito.mock(FilterSubscription.class));

        final FilterSubscription subscription = mock(FilterSubscription.class);

        when(subscriptionManager.getAllFilterSubscriptions(filterId)).thenReturn(ImmutableList.of(subscription));

        service = createService(subscriptionManager);
        assertThat(service.getVisibleFilterSubscriptions(user, filter), contains(subscription));
    }

    @Test
    public void testGetVisibleSubscriptionsForNonAuthor() throws GenericEntityException {
        final Long filterId = (long) 999;
        final ApplicationUser other = new MockApplicationUser("other");
        final SearchRequest filter = new SearchRequest(new QueryImpl(), other, "filter", "filterDesc", filterId, 0L);
        final List<FilterSubscription> expected = ImmutableList.of(Mockito.mock(FilterSubscription.class));

        when(subscriptionManager.getFilterSubscriptions(user, filterId)).thenReturn(expected);

        service = createService(subscriptionManager);

        Collection<FilterSubscription> result = service.getVisibleFilterSubscriptions(user, filter);
        assertEquals(expected, result);

    }

    @Test
    public void testGetVisibleSubscriptionsForError() throws GenericEntityException {
        final Long filterId = 999L;
        final ApplicationUser other = new MockApplicationUser("other");
        final SearchRequest filter = new SearchRequest(new QueryImpl(), other, "filter", "filterDesc", filterId, 0L);

        when(subscriptionManager.getFilterSubscriptions(user, filterId)).thenThrow(new GenericEntityException("ouch!"));

        service = createService(subscriptionManager);

        expectedException.expect(DataAccessException.class);
        expectedException.expectMessage("GenericEntityException: ouch!");
        service.getVisibleFilterSubscriptions(user, filter);
    }

    @Test
    public void testGetVisibleSubscriptionsForNullFilterAndUser() {
        final Long filterId = 999L;
        final SearchRequest filter = new SearchRequest(new QueryImpl(), user, "filter", "filterDesc", filterId, 0L);

        service = createService(subscriptionManager);

        Collection<FilterSubscription> result = service.getVisibleFilterSubscriptions(null, filter);
        assertThat(result, hasSize(0));
        verifyZeroInteractions(subscriptionManager);

        result = service.getVisibleFilterSubscriptions(user, null);
        assertThat(result, hasSize(0));
        verifyZeroInteractions(subscriptionManager);
    }

    private DefaultFilterSubscriptionService createService(SubscriptionManager subscriptionManager) {
        JiraAuthenticationContext authenticationContext = mock(JiraAuthenticationContext.class);
        when(authenticationContext.getI18nHelper()).thenReturn(new NoopI18nHelper());

        return new DefaultFilterSubscriptionService(authenticationContext, subscriptionManager, schedulerService);
    }

    private JiraServiceContextImpl createContext() {
        return new JiraServiceContextImpl((ApplicationUser) null, new SimpleErrorCollection());
    }
}
