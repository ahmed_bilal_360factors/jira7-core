package com.atlassian.jira;

import com.atlassian.fugue.Option;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.entity.Entity;
import com.atlassian.jira.permission.LegacyProjectPermissionKeyMapping;
import com.atlassian.jira.security.plugin.ProjectPermissionKey;
import com.atlassian.jira.upgrade.UpgradesXmlParser;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Maps;
import com.google.common.collect.Ordering;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.hamcrest.Matchers;
import org.junit.Test;

import javax.xml.stream.Location;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.function.Function;

import static com.atlassian.jira.matchers.SetMatchers.subsetOf;
import static com.atlassian.jira.upgrade.tasks.UpgradeTask_Build70100.RENAISSANCE_BUILD_NUMBER;
import static java.lang.String.format;
import static java.nio.charset.StandardCharsets.UTF_8;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * This is an test which runs through startup database looking for certain errors.
 *
 * @since v6.1
 */
public class TestStartupDatabaseXml {
    /**
     * Class path pattern to find translation files for checking
     */
    private static final String STARTUP_DATABASE_XML = "/startupdatabase.xml";

    private final static Set<String> ALLOWED_ENTITIES = ImmutableSet.of("Application", "Avatar",
            "ConfigurationContext", "Directory", "DirectoryAttribute", "DirectoryOperation", "EventType",
            "FieldConfigScheme", "FieldConfigSchemeIssueType", "FieldConfiguration", "FieldLayout",
            "FieldLayoutItem", "FieldScreen", "FieldScreenLayoutItem", "FieldScreenScheme", "FieldScreenSchemeItem",
            "FieldScreenTab", "GadgetUserPreference", "GenericConfiguration", "GlobalPermissionEntry", "Group",
            "IssueLinkType", "IssueType", "IssueTypeScreenScheme", "IssueTypeScreenSchemeEntity", "JQRTZLocks",
            "ListenerConfig", "Notification", "NotificationScheme", "OSPropertyEntry", "OSPropertyNumber",
            "OSPropertyString", "OSPropertyText", "OptionConfiguration", "PermissionScheme", "PortalPage",
            "PortletConfiguration", "Priority", "ProjectRole", "ProjectRoleActor", "Resolution",
            "SchemePermissions", "SequenceValueItem", "ServiceConfig", "SharePermissions", "Status",
            "Workflow", "WorkflowScheme", "WorkflowSchemeEntity", "Feature", "UpgradeHistory");

    @Test
    public void testStartupXmlValid() {
        final StartupXml parse = StartupXml.parse(STARTUP_DATABASE_XML);
        verifyParsedXml(parse);
        //We need to downgrade to JIRA 7.0.0 BTF - include UpgradeTask 70102 and higher
        verifyCanDowngrade(parse, 70102);
    }

    private void verifyParsedXml(final StartupXml parse) {
        assertThat("JIRA 6.x JIRA License not included.", parse.hasAppProperty(APKeys.JIRA_LICENSE), is(false));
        assertThat("Only contains allowed rows.", parse.seenEntities(), subsetOf(ALLOWED_ENTITIES));
        final String patchedVersion = parse.appPropertyOrThrow(APKeys.JIRA_PATCHED_VERSION);
        assertThat("Startup must have DB build number.", patchedVersion, notNullValue());
        int patchedVersionNumber = Integer.parseInt(patchedVersion);
        assertThat(String.format("Startup must have upgrade version less than %d.", RENAISSANCE_BUILD_NUMBER),
                patchedVersionNumber, Matchers.lessThanOrEqualTo(RENAISSANCE_BUILD_NUMBER));

        //Current Maximum Ids from the rows in the data.
        final Map<String, Long> currentMax = parse.currentMax();

        //Current sequence items in the data (i.e. what will be used for new keys).
        final Map<String, Long> currentSequence = parse.currentSequence();

        //Check to make sure that the sequence item is not smaller than the actual IDs in the backup.
        currentMax.forEach((type, max) ->
        {
            //If there is no sequence in the database then it is assumed to be 10000L by OfBiz. We do this here too.
            final Long maxSequence = currentSequence.getOrDefault(type, 10000L);
            if (maxSequence.compareTo(max) <= 0) {
                fail(format("Row '%s' has current max of '%d' but the sequence item reports max of '%d'.",
                        type, max, maxSequence));
            }
        });
    }

    /**
     * This part of the verification ensures that it is possible to move from JIRA Cloud -> JIRA Server.
     * <p>
     * https://jdog.jira-dev.com/browse/HIROL-1386
     * We need to include implicit Upgrade History in our startupdatabase.xml so that BTF knows how to downgrade safely.
     *
     * @param parsedStartupXml
     * @param minDowngradeBuildNumber
     */
    private void verifyCanDowngrade(final StartupXml parsedStartupXml, int minDowngradeBuildNumber) {
        final String patchedVersion = parsedStartupXml.appPropertyOrThrow(APKeys.JIRA_PATCHED_VERSION);
        final int dbBuildNumber = Integer.parseInt(patchedVersion);

        final SortedSet<Integer> upgradeTasks = UpgradesXmlParser.getUpgradeTaskNumbers();
        boolean writeUpgradeHistoryForMinDowngradeBuildNumber = false;

        for (Integer upgradeTask : upgradeTasks) {
            if (upgradeTask > minDowngradeBuildNumber && upgradeTask <= dbBuildNumber) {
                // In order to downgrade, we need startupdatabase.xml to contain UpgradeHistory rows for all upgrade tasks
                // that have "run implicitly" (we will need to read downgradetaskrequired = true or false for each).
                assertTrue("StartupXml is missing UpgradeHistory for " + upgradeTask, parsedStartupXml.containsUpgradeHistory(upgradeTask));
            }
            if (upgradeTask == minDowngradeBuildNumber) {
                // We need this upgrade task to appear in the exported XML UpgradeHistory so old JIRA is happy that
                // it can see enough UpgradeHistory.
                if (dbBuildNumber < minDowngradeBuildNumber) {
                    // then we will actually run this upgrade task after importing StartupXml
                    writeUpgradeHistoryForMinDowngradeBuildNumber = true;
                } else {
                    // we will require this UpgradeHistory to appear in the StartupXml
                    assertTrue("StartupXml is missing UpgradeHistory for " + upgradeTask, parsedStartupXml.containsUpgradeHistory(upgradeTask));
                    writeUpgradeHistoryForMinDowngradeBuildNumber = true;
                }
            }
        }
        assertTrue("You will be unable to downgrade data to build number " + minDowngradeBuildNumber +
                " because the export will not have sufficient upgrade task history.", writeUpgradeHistoryForMinDowngradeBuildNumber);
    }

    private static class StartupXml {
        private static final String ROW_SEQ = "SequenceValueItem";
        private static final String ATTR_SEQ_NAME = "seqName";
        private static final String ATTR_SEQ_MAX = "seqId";

        private static final String ROW_APP_ENTRY = "OSPropertyEntry";
        private static final String ROW_APP_TEXT = "OSPropertyText";
        private static final String ROW_APP_STRING = "OSPropertyString";
        private static final String ROW_APP_NUMBER = "OSPropertyNumber";

        private static final String ATTR_APP_REL_NAME = "entityName";
        private static final String ATTR_APP_REL_ID = "entityId";
        private static final String ATTR_APP_TYPE = "type";
        private static final String ATTR_APP_KEY = "propertyKey";
        private static final String ATTR_APP_VALUE = "value";

        private static final String VALUE_APP_REL_NAME = "jira.properties";
        private static final long VALUE_APP_REL_ID = 1;

        private static final int VALUE_APP_TYPE_NUMBER = 1;
        private static final int VALUE_APP_TYPE_STRING = 5;
        private static final int VALUE_APP_TYPE_TEXT = 6;

        private final Map<String, Long> currentSequence;
        private final Map<String, Long> currentMax;
        private final Map<String, Object> applicationProperties;
        private final Set<Integer> upgradeHistory = new HashSet<>();

        private static StartupXml parse(final String databaseXmlFile) {
            try (InputStream is = TestStartupDatabaseXml.class.getResourceAsStream(databaseXmlFile)) {
                try (StartupXmlRowParser parser = new StartupXmlRowParser(is)) {
                    return new StartupXml(parser);
                }
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }

        private StartupXml(Iterator<Row> rows) {
            final Map<String, Long> currentSequence = Maps.newLinkedHashMap();
            final Map<String, Long> currentMax = Maps.newLinkedHashMap();
            final Map<Long, Row> propertyString = Maps.newLinkedHashMap();
            final Map<Long, Row> propertyText = Maps.newLinkedHashMap();
            final Map<Long, Row> propertyNumber = Maps.newLinkedHashMap();
            final Map<Long, Row> property = Maps.newLinkedHashMap();
            final Map<String, Map<Long, Row>> rowNameToMap = ImmutableMap.of(ROW_APP_ENTRY, property,
                    ROW_APP_TEXT, propertyText,
                    ROW_APP_STRING, propertyString,
                    ROW_APP_NUMBER, propertyNumber);

            final Ordering<Long> compareLongs = Ordering.natural().nullsFirst();
            rows.forEachRemaining(row ->
            {
                final String name = row.name();
                final Option<Long> id = row.maybeId();
                if (id.isDefined()) {
                    currentMax.compute(name, (key, value) -> compareLongs.max(value, id.get()));
                }

                if (ROW_SEQ.equals(name)) {
                    final String key = row.getOrThrow(ATTR_SEQ_NAME);
                    if (currentSequence.put(key, row.getLongOrThrow(ATTR_SEQ_MAX)) != null) {
                        throw new RuntimeException(format("Two sequence items for %s.", key));
                    }
                } else if (name.equals(Entity.Name.UPGRADE_HISTORY)) {
                    upgradeHistory.add(row.getIntOrThrow("targetbuild"));
                } else if (name.equals(Entity.Name.SCHEME_PERMISSIONS)) {
                    // check that declared permission ID & key match
                    String permissionKey = row.getMaybe("permissionKey").getOrNull();
                    int permissionId = row.getIntOrThrow("permission");
                    if (permissionKey != null) {
                        ProjectPermissionKey expected = LegacyProjectPermissionKeyMapping.getKey(permissionId);
                        if (!permissionKey.equals(expected.permissionKey())) {
                            throw new RuntimeException(format(
                                    "Permission key mismatch for permission ID '%d'. Expected '%s', got '%s':\n%s",
                                    permissionId, expected.permissionKey(), permissionKey, row.toString()));
                        }
                    }
                } else {
                    final Map<Long, Row> rowMap = rowNameToMap.get(name);
                    if (rowMap != null) {
                        final long key = row.id();
                        final Row old = rowMap.put(key, row);
                        if (old != null) {
                            throw new RuntimeException(format("Two rows of %s share the same ID %d.", name, key));
                        }
                    }
                }
            });

            Map<String, Object> applicationProperties = Maps.newLinkedHashMap();
            property.forEach((key, entryRow) ->
            {
                final String relatedName = entryRow.getOrThrow(ATTR_APP_REL_NAME);
                final long relatedId = entryRow.getLongOrThrow(ATTR_APP_REL_ID);

                if (relatedId == VALUE_APP_REL_ID && VALUE_APP_REL_NAME.equals(relatedName)) {
                    final int type = entryRow.getIntOrThrow(ATTR_APP_TYPE);
                    if (type == VALUE_APP_TYPE_NUMBER) {
                        final Row valueRow = propertyNumber.get(key);
                        if (valueRow == null) {
                            throw new RuntimeException(format("Row %s[%d] does not have matching entry in %s.",
                                    ROW_APP_ENTRY, key, ROW_APP_NUMBER));
                        }
                        applicationProperties.put(entryRow.getOrThrow(ATTR_APP_KEY), valueRow.getLongOrThrow(ATTR_APP_VALUE));
                    } else if (type == VALUE_APP_TYPE_STRING) {
                        final Row valueRow = propertyString.get(key);
                        if (valueRow == null) {
                            throw new RuntimeException(format("Row %s[%d] does not have matching entry in %s.",
                                    ROW_APP_ENTRY, key, ROW_APP_STRING));
                        }
                        applicationProperties.put(entryRow.getOrThrow(ATTR_APP_KEY), valueRow.getOrThrow(ATTR_APP_VALUE));
                    } else if (type == VALUE_APP_TYPE_TEXT) {
                        final Row valueRow = propertyText.get(key);
                        if (valueRow == null) {
                            throw new RuntimeException(format("Row %s[%d] does not have matching entry in %s.",
                                    ROW_APP_ENTRY, key, ROW_APP_TEXT));
                        }
                        applicationProperties.put(entryRow.getOrThrow(ATTR_APP_KEY), valueRow.getOrThrow(ATTR_APP_VALUE));
                    } else {
                        throw new RuntimeException(format("Type %s is not valid in row %s[%d].",
                                type, ROW_APP_ENTRY, key));
                    }
                }
            });

            this.currentSequence = ImmutableMap.copyOf(currentSequence);
            this.currentMax = ImmutableMap.copyOf(currentMax);
            this.applicationProperties = ImmutableMap.copyOf(applicationProperties);
        }

        private Set<String> seenEntities() {
            return currentMax.keySet();
        }

        private Map<String, Long> currentSequence() {
            return currentSequence;
        }

        private Map<String, Long> currentMax() {
            return currentMax;
        }

        private boolean hasAppProperty(String key) {
            return applicationProperties.containsKey(key);
        }

        private String appPropertyOrThrow(String key) {
            return appPropertyOrThrow(key, String.class);
        }

        private long appPropertyLongOrThrow(String key) {
            return appPropertyOrThrow(key, Long.class);
        }

        private <T> T appPropertyOrThrow(final String key, final Class<T> type) {
            final Object o = applicationProperties.get(key);
            if (o == null) {
                throw new RuntimeException(format("Unable find application property for '%s'.", key));
            } else {
                if (type.isInstance(o)) {
                    return type.cast(o);
                } else {
                    throw new RuntimeException(format("Application property for '%s' is not a %s.",
                            key, type.getSimpleName()));
                }
            }
        }

        public boolean containsUpgradeHistory(final int upgradeTask) {
            return upgradeHistory.contains(upgradeTask);
        }
    }

    private static class StartupXmlRowParser implements Closeable, Iterator<Row> {
        private static final String ROOT_ELEMENT = "entity-engine-xml";
        private final XMLEventReader reader;
        private Row next;
        private boolean seenRoot = false;

        private StartupXmlRowParser(InputStream inputStream) {
            final XMLInputFactory xmlInputFactory = XMLInputFactory.newFactory();
            try {
                this.reader = xmlInputFactory.createXMLEventReader(new InputStreamReader(inputStream, UTF_8));
            } catch (XMLStreamException e) {
                throw new RuntimeException(e);
            }
        }

        private Row consumeRow(StartElement element) {
            final Map<String, Option<String>> attributes = getAttributeMap(element);
            StartElement childStart = null;
            StringBuilder buffer = new StringBuilder();
            while (hasNextEvent()) {
                final XMLEvent next = nextEvent();
                switch (next.getEventType()) {
                    case XMLEvent.START_ELEMENT: {
                        if (childStart == null) {
                            childStart = next.asStartElement();
                        } else {
                            throw new RuntimeException(format("%s - Unexpected sub element in data.",
                                    location(next)));
                        }
                        break;
                    }
                    case XMLEvent.END_ELEMENT: {
                        if (childStart == null) {
                            return new Row(element.getName().getLocalPart(), attributes);
                        } else {
                            String name = childStart.getName().getLocalPart();
                            String value = StringUtils.trimToNull(buffer.toString());
                            if (name == null) {
                                throw new RuntimeException(format("%s - Name of attribute is null.", location(next)));
                            }
                            attributes.put(name, Option.option(value));
                            buffer = new StringBuilder();
                            childStart = null;
                        }
                        break;
                    }
                    case XMLEvent.CHARACTERS:
                    case XMLEvent.CDATA: {
                        final Characters characters = (Characters) next;
                        if (childStart == null && !characters.isWhiteSpace()) {
                            throw new RuntimeException(format("%s - Illegal characters '%s'.", characters.getData(),
                                    location(next)));
                        } else {
                            buffer.append(characters.getData());
                        }
                        break;
                    }
                    case XMLEvent.SPACE:
                        break;
                    default:
                        throw new RuntimeException(format("%s - Unexpected XML content.", location(next)));
                }
            }
            throw new RuntimeException("Bad XML document - row not closed.");
        }

        private void consumeRootElement() {
            while (!seenRoot && hasNextEvent()) {
                final XMLEvent next = nextEvent();
                switch (next.getEventType()) {
                    case XMLEvent.START_ELEMENT: {
                        StartElement startElement = (StartElement) next;
                        if (!ROOT_ELEMENT.equals(startElement.getName().getLocalPart())) {
                            throw new RuntimeException(format("%s - Root element not '%s'.",
                                    location(startElement), ROOT_ELEMENT));
                        }
                        seenRoot = true;
                        break;
                    }
                    case XMLEvent.CHARACTERS: {
                        final Characters characters = next.asCharacters();
                        if (!characters.isWhiteSpace()) {
                            throw new RuntimeException(format("%s - Illegal characters '%s'.",
                                    location(characters), characters.getData()));
                        }
                        //fall through;
                    }
                    case XMLEvent.START_DOCUMENT:
                    case XMLEvent.PROCESSING_INSTRUCTION:
                    case XMLEvent.COMMENT:
                    case XMLStreamConstants.SPACE:
                        break;
                    default:
                        throw new RuntimeException(format("%s - Unexpected XML content before root node: %s",
                                location(next), next));
                }
            }
            if (!seenRoot) {
                throw new RuntimeException(format("Unable to find root element '%s'.", ROOT_ELEMENT));
            }
        }

        private Row nextRow() {
            if (!seenRoot) {
                consumeRootElement();
            }

            while (hasNextEvent()) {
                final XMLEvent next = nextEvent();
                switch (next.getEventType()) {
                    case XMLEvent.START_ELEMENT: {
                        return consumeRow(next.asStartElement());
                    }
                    case XMLEvent.CHARACTERS:
                    case XMLEvent.CDATA: {
                        final Characters characters = next.asCharacters();
                        if (!characters.isWhiteSpace()) {
                            throw new RuntimeException(format("%s - Illegal characters '%s'.",
                                    location(characters), characters.getData()));
                        }
                        //fall through
                    }
                    case XMLEvent.END_DOCUMENT:
                    case XMLStreamConstants.END_ELEMENT:
                    case XMLStreamConstants.COMMENT:
                    case XMLStreamConstants.SPACE:
                        break;
                    default:
                        throw new RuntimeException(format("%s - Unexpected XML content before root node: %s",
                                location(next), next));
                }
            }

            return null;
        }

        private static String location(XMLEvent event) {
            final Location location = event.getLocation();
            return format("[%s:%s]", location.getLineNumber(), location.getColumnNumber());
        }

        private static Map<String, Option<String>> getAttributeMap(final StartElement next) {
            final Map<String, Option<String>> attributes = Maps.newHashMap();
            final Iterator iterator = next.getAttributes();
            while (iterator.hasNext()) {
                Attribute attribute = (Attribute) iterator.next();
                final String name = attribute.getName().getLocalPart();
                final String value = StringUtils.stripToNull(attribute.getValue());
                attributes.put(name, Option.option(value));
            }
            return attributes;
        }

        private boolean hasNextEvent() {
            return reader.hasNext();
        }

        private XMLEvent nextEvent() {
            try {
                return reader.nextEvent();
            } catch (XMLStreamException e) {
                throw new RuntimeException(e);
            }
        }

        @Override
        public void close() throws IOException {
            try {
                reader.close();
            } catch (XMLStreamException e) {
                throw new IOException(e);
            }
        }

        @Override
        public boolean hasNext() {
            if (next == null) {
                next = nextRow();
            }
            return next != null;
        }

        @Override
        public Row next() {
            Row result = next;
            if (result == null) {
                result = nextRow();
                if (result == null) {
                    throw new IllegalStateException("Iterator is empty.");
                } else {
                    return result;
                }
            } else {
                next = null;
                return result;
            }
        }
    }

    private static class Row {
        private static final String ID_KEY = "id";

        private final String name;
        private final Map<String, Option<String>> data;

        private Row(String name, Map<String, Option<String>> data) {
            this.name = name;
            this.data = ImmutableMap.copyOf(data);
        }

        private Option<Long> maybeId() {
            return getMaybe(ID_KEY).map(value ->
            {
                try {
                    return Long.parseLong(value);
                } catch (NumberFormatException e) {
                    throw new RuntimeException(format("Property '%s' on '%s' is not a number (%s).", ID_KEY, name(), value));
                }

            });
        }

        private long id() {
            return getLongOrThrow(ID_KEY);
        }

        private String name() {
            return name;
        }

        private long getLongOrThrow(String key) {
            return getMapOrThrow(key, value ->
            {
                try {
                    return Long.parseLong(value);
                } catch (NumberFormatException e) {
                    throw new RuntimeException(format("Property '%s' on '%s' is not a number (%s).", key, name(), value));
                }
            });
        }

        private int getIntOrThrow(String key) {
            return getMapOrThrow(key, value ->
            {
                try {
                    return Integer.parseInt(value);
                } catch (NumberFormatException e) {
                    throw new RuntimeException(format("Property '%s' on '%s' is not a number (%s).", key, name(), value));
                }
            });
        }

        private String getOrThrow(String key) {
            return getMapOrThrow(key, Function.identity());
        }

        private <T> T getMapOrThrow(String key, Function<String, ? extends T> map) {
            final Option<String> option = data.get(key);
            if (option == null) {
                throw new IllegalArgumentException(format("Property %s does not exist on '%s'.", key, name()));
            } else {
                return map.apply(option.get());
            }
        }

        private Option<String> getMaybe(String key) {
            return data.getOrDefault(key, Option.none());
        }

        @Override
        public String toString() {
            return new ToStringBuilder(this)
                    .append("name", name)
                    .append("data", data)
                    .toString();
        }
    }
}
