package com.atlassian.jira.timezone;

import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.bc.JiraServiceContextImpl;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.preferences.UserPreferencesManager;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.SimpleErrorCollection;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import static com.atlassian.jira.config.properties.APKeys.JIRA_DEFAULT_TIMEZONE;
import static com.atlassian.jira.security.Permissions.ADMINISTER;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @since v4.4
 */
public class TestTimeZoneManager {
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    private TimeZoneService timeZoneManager;

    @Mock
    private ApplicationProperties applicationProperties;

    @Mock
    private PermissionManager permissionManager;

    @Mock
    private UserPreferencesManager userPreferenceManager;

    @Mock
    private I18nHelper i18nHelper;

    @Before
    public void setUp() throws Exception {
        timeZoneManager = new TimeZoneServiceImpl(applicationProperties, permissionManager, userPreferenceManager) {
            @Override
            protected TimeZone getJVMTimeZone() {
                return TimeZone.getTimeZone("Europe/Berlin");
            }
        };
    }

    @Test
    public void testGetJVMTimeZoneInfo() throws Exception {
        when(i18nHelper.getLocale()).thenReturn(new Locale("de", "DE"));
        when(i18nHelper.getText("timezone.zone.europe.berlin")).thenReturn("Berlin, Berlin!");
        final MockApplicationUser user = new MockApplicationUser("bob");
        final SimpleErrorCollection errorCollection = new SimpleErrorCollection();

        final JiraServiceContext jiraServiceContext = new JiraServiceContextImpl(user, errorCollection, i18nHelper);
        final TimeZoneInfo jvmTimeZoneInfo = timeZoneManager.getJVMTimeZoneInfo(jiraServiceContext);

        assertEquals("Europe", jvmTimeZoneInfo.getRegionKey());
        assertEquals("Berlin, Berlin!", jvmTimeZoneInfo.getCity());
        assertEquals("(GMT+01:00)", jvmTimeZoneInfo.getGMTOffset());
    }

    @Test
    public void testGetDefaultTimeZoneInfo() throws Exception {
        when(i18nHelper.getLocale()).thenReturn(new Locale("en", "US"));
        when(i18nHelper.getText("timezone.zone.europe.amsterdam")).thenReturn("Off to Amsterdam!");
        when(applicationProperties.getString(JIRA_DEFAULT_TIMEZONE)).thenReturn("Europe/Amsterdam");
        final MockApplicationUser user = new MockApplicationUser("bob");
        final SimpleErrorCollection errorCollection = new SimpleErrorCollection();

        final JiraServiceContext jiraServiceContext = new JiraServiceContextImpl(user, errorCollection, i18nHelper);
        final TimeZoneInfo timeZoneInfo = timeZoneManager.getDefaultTimeZoneInfo(jiraServiceContext);

        assertEquals("Europe", timeZoneInfo.getRegionKey());
        assertEquals("Off to Amsterdam!", timeZoneInfo.getCity());
        assertEquals("(GMT+01:00)", timeZoneInfo.getGMTOffset());
    }

    @Test
    public void testGetTimeZoneRegions() throws Exception {
        final String[] regions = {"Etc", "Pacific", "America", "Antarctica", "Atlantic", "Africa", "Europe", "Asia", "Indian", "Australia"};
        for (String region : regions) {
            when(i18nHelper.getText("timezone.region." + region.toLowerCase())).thenReturn("GMT");
        }

        final MockApplicationUser user = new MockApplicationUser("bob");
        final SimpleErrorCollection errorCollection = new SimpleErrorCollection();

        final JiraServiceContext jiraServiceContext = new JiraServiceContextImpl(user, errorCollection, i18nHelper);
        final List<RegionInfo> timeZoneRegions = timeZoneManager.getTimeZoneRegions(jiraServiceContext);

        assertEquals(10, timeZoneRegions.size());
    }

    @Test
    public void testUsesSystemTimeZone() throws Exception {
        when(applicationProperties.getString(JIRA_DEFAULT_TIMEZONE)).thenReturn(null);
        assertEquals(true, timeZoneManager.useSystemTimeZone());
    }

    @Test
    public void testUsesNotSystemTimeZone() throws Exception {
        when(applicationProperties.getString(JIRA_DEFAULT_TIMEZONE)).thenReturn("Europe/Berlin");
        assertEquals(false, timeZoneManager.useSystemTimeZone());
    }

    @Test
    public void testSetDefaultTimeZone() throws Exception {
        final MockApplicationUser user = new MockApplicationUser("bob");
        final SimpleErrorCollection errorCollection = new SimpleErrorCollection();
        final JiraServiceContext jiraServiceContext = new JiraServiceContextImpl(user, errorCollection, i18nHelper);
        when(permissionManager.hasPermission(ADMINISTER, user)).thenReturn(true);

        timeZoneManager.setDefaultTimeZone("Europe/Berlin", jiraServiceContext);

        verify(applicationProperties).setString(JIRA_DEFAULT_TIMEZONE, "Europe/Berlin");
    }

    @Test
    public void testSetDefaultTimeZoneNoPermission() throws Exception {
        expectedException.expect(RuntimeException.class);
        expectedException.expectMessage(equalTo("This user does not have the JIRA Administrator permission. This permission is required to change the default timezone."));

        final MockApplicationUser user = new MockApplicationUser("bob");
        final SimpleErrorCollection errorCollection = new SimpleErrorCollection();
        final JiraServiceContext jiraServiceContext = new JiraServiceContextImpl(user, errorCollection, i18nHelper);
        when(permissionManager.hasPermission(ADMINISTER, user)).thenReturn(false);

        timeZoneManager.setDefaultTimeZone("Europe/Berlin", jiraServiceContext);
    }

    @Test
    public void testClearDefaultTimeZone() throws Exception {
        final MockApplicationUser user = new MockApplicationUser("bob");
        final SimpleErrorCollection errorCollection = new SimpleErrorCollection();
        final JiraServiceContext jiraServiceContext = new JiraServiceContextImpl(user, errorCollection, i18nHelper);
        when(permissionManager.hasPermission(ADMINISTER, user)).thenReturn(true);

        timeZoneManager.clearDefaultTimeZone(jiraServiceContext);

        verify(applicationProperties).setString(JIRA_DEFAULT_TIMEZONE, null);
    }

    @Test
    public void testClearDefaultTimeZoneNoAdminPermission() throws Exception {
        expectedException.expect(RuntimeException.class);
        expectedException.expectMessage(equalTo("This user does not have the JIRA Administrator permission. This permission is required to change the default timezone."));

        final MockApplicationUser user = new MockApplicationUser("bob");
        final SimpleErrorCollection errorCollection = new SimpleErrorCollection();
        final JiraServiceContext jiraServiceContext = new JiraServiceContextImpl(user, errorCollection, i18nHelper);
        when(permissionManager.hasPermission(ADMINISTER, user)).thenReturn(false);

        timeZoneManager.clearDefaultTimeZone(jiraServiceContext);
    }
}
