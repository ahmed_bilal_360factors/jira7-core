package com.atlassian.jira.plugin.jql.function;

import com.atlassian.jira.JiraDataTypes;
import com.atlassian.jira.bc.security.login.LoginInfo;
import com.atlassian.jira.bc.security.login.LoginService;
import com.atlassian.jira.jql.operand.QueryLiteral;
import com.atlassian.jira.jql.query.QueryCreationContext;
import com.atlassian.jira.jql.query.QueryCreationContextImpl;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.MessageSet;
import com.atlassian.jira.web.bean.MockI18nBean;
import com.atlassian.query.clause.TerminalClause;
import com.atlassian.query.operand.FunctionOperand;
import org.junit.Test;

import java.util.List;

import static com.atlassian.jira.plugin.jql.function.CurrentLoginFunction.FUNCTION_CURRENT_LOGIN;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @since v4.1
 */
public class TestCurrentLoginFunction {
    private final TerminalClause terminalClause = null;

    @Test
    public void testValidateTooManyArguments() throws Exception {
        CurrentLoginFunction loginFunction = new CurrentLoginFunction(null) {
            @Override
            protected I18nHelper getI18n() {
                return new MockI18nBean();
            }
        };
        FunctionOperand function = new FunctionOperand(FUNCTION_CURRENT_LOGIN, asList("should", "not", "be", "here"));
        final MessageSet messageSet = loginFunction.validate(null, function, terminalClause);
        assertTrue(messageSet.hasAnyMessages());
        assertTrue(messageSet.hasAnyErrors());
        assertFalse(messageSet.hasAnyWarnings());
        assertThat(messageSet.getErrorMessages(), hasSize(1));
        assertEquals("Function 'currentLogin' expected '0' arguments but received '4'.", messageSet.getErrorMessages().iterator().next());
    }

    @Test
    public void testDataType() throws Exception {
        CurrentLoginFunction handler = new CurrentLoginFunction(null);
        assertEquals(JiraDataTypes.DATE, handler.getDataType());
    }

    @Test
    public void testGetValuesNullContext() throws Exception {
        CurrentLoginFunction loginFunction = new CurrentLoginFunction(null);
        FunctionOperand function = new FunctionOperand(FUNCTION_CURRENT_LOGIN, emptyList());
        final List<QueryLiteral> value = loginFunction.getValues(null, function, terminalClause);
        assertNotNull(value);
        assertThat(value, hasSize(0));
    }

    @Test
    public void testGetValuesNullUser() throws Exception {
        CurrentLoginFunction loginFunction = new CurrentLoginFunction(null);
        FunctionOperand function = new FunctionOperand(FUNCTION_CURRENT_LOGIN, emptyList());
        final QueryCreationContext context = new QueryCreationContextImpl((ApplicationUser) null);
        final List<QueryLiteral> value = loginFunction.getValues(context, function, terminalClause);
        assertNotNull(value);
        assertThat(value, hasSize(0));
    }

    @Test
    public void testGetValues() throws Exception {
        final long lastLoginTime = 2345678900000L;
        final ApplicationUser user = new MockApplicationUser("bob");
        final LoginService loginService = mock(LoginService.class);
        final LoginInfo loginInfo = mock(LoginInfo.class);
        when(loginInfo.getLastLoginTime()).thenReturn(lastLoginTime);
        when(loginService.getLoginInfo(user.getName())).thenReturn(loginInfo);

        final CurrentLoginFunction loginFunction = new CurrentLoginFunction(loginService);
        final FunctionOperand function = new FunctionOperand(FUNCTION_CURRENT_LOGIN, emptyList());
        final QueryCreationContext context = new QueryCreationContextImpl(user);
        final List<QueryLiteral> value = loginFunction.getValues(context, function, terminalClause);

        assertThat(value, notNullValue());
        assertThat(value, hasSize(1));
        assertEquals(lastLoginTime, value.get(0).getLongValue().longValue());
    }

    @Test
    public void testGetMinimumNumberOfExpectedArguments() throws Exception {
        CurrentLoginFunction loginFunction = new CurrentLoginFunction(null);
        assertEquals(0, loginFunction.getMinimumNumberOfExpectedArguments());
    }
}
