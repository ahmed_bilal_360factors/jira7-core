package com.atlassian.jira.issue.subscription;

import com.atlassian.core.ofbiz.test.UtilsForTests;
import com.atlassian.crowd.embedded.api.CrowdService;
import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.crowd.exception.InvalidCredentialException;
import com.atlassian.crowd.exception.InvalidUserException;
import com.atlassian.crowd.exception.OperationNotPermittedException;
import com.atlassian.crowd.exception.embedded.InvalidGroupException;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.datetime.DateTimeFormatterFactory;
import com.atlassian.jira.entity.Entity;
import com.atlassian.jira.entity.EntityEngineImpl;
import com.atlassian.jira.entity.Select;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoContainer;
import com.atlassian.jira.mail.SubscriptionMailQueueItem;
import com.atlassian.jira.mail.SubscriptionMailQueueItemFactory;
import com.atlassian.jira.mock.ofbiz.MockOfBizDelegator;
import com.atlassian.jira.ofbiz.FieldMap;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.template.TemplateManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.MockGroup;
import com.atlassian.jira.user.util.MockUserManager;
import com.atlassian.mail.queue.MailQueue;
import com.atlassian.scheduler.SchedulerService;
import com.atlassian.scheduler.config.JobConfig;
import com.atlassian.scheduler.config.JobId;
import com.atlassian.scheduler.status.JobDetails;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.ofbiz.core.entity.GenericEntityException;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static com.atlassian.jira.component.ComponentAccessor.getComponent;
import static com.atlassian.jira.component.ComponentAccessor.getMailQueue;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.isIn;
import static org.hamcrest.Matchers.lessThanOrEqualTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestDefaultSubscriptionManager {
    @Rule
    public MockitoContainer container = new MockitoContainer(this);

    private final long runAt = new Date().getTime();

    private FilterSubscription subscrip;
    private MockApplicationUser u;
    private final Long filterId = 1000L;

    @Mock
    @AvailableInContainer
    private GroupManager groupManager;

    @Mock
    @AvailableInContainer
    private SchedulerService schedulerService;

    @Mock
    @AvailableInContainer
    private MailQueue mailQueue;

    @Mock
    @AvailableInContainer
    private SubscriptionMailQueueItemFactory factory;

    @AvailableInContainer
    private final MockUserManager userManager = new MockUserManager().alwaysReturnUsers();

    @AvailableInContainer
    private final OfBizDelegator ofbiz = new MockOfBizDelegator();

    @Mock
    @AvailableInContainer
    private DateTimeFormatterFactory formatterFactory;

    private EntityEngineImpl entityEngine;


    public TestDefaultSubscriptionManager() {
    }

    private MockApplicationUser createMockUser(final String userName, final String name, final String email) throws OperationNotPermittedException,
            InvalidUserException, InvalidCredentialException {
        final MockApplicationUser user = new MockApplicationUser(userName, name, email);
        return user;
    }

    private ApplicationUser createMockUser(final String userName) throws Exception {
        return createMockUser(userName, userName, "");
    }

    @Before
    public void setUp() throws Exception {
        entityEngine = new EntityEngineImpl(ofbiz);

        u = createMockUser("owen", "owen", "owen@atlassian.com");

        final FieldMap columns = FieldMap.build("filterID", filterId, "username", "owen", "group", "group", "lastRun",
                new Timestamp(runAt));
        columns.put("emailOnEmpty", Boolean.TRUE.toString());
        UtilsForTests.getTestEntity("FilterSubscription", columns);

        subscrip = new DefaultFilterSubscription(1000L, filterId, "owen", "group", new Timestamp(runAt), true);

        userManager.addUser(createMockUser("owen", "owen", "owen@atlassian.com"));
    }

    @Test
    public void testHasSubscription() throws GenericEntityException {
        when(groupManager.getGroupNamesForUser(u)).thenReturn(Collections.<String>emptyList());

        final SubscriptionManager sm = getSubscriptionManager(null, null, null);
        assertThat(sm.hasSubscription(u, filterId), is(true));
    }

    @Test
    public void testGetSubscription() throws GenericEntityException {
        final SubscriptionManager sm = getSubscriptionManager(null, null, null);
        final FilterSubscription subscription = sm.getFilterSubscription(u, filterId);
        assertNotNull(subscription);
        assertThat(subscrip, is(subscription));
    }

    @Test
    public void testGetSubscriptions() throws Exception {
        final List<String> groupNames = new ArrayList<String>();

        when(groupManager.getGroupNamesForUser(u)).thenReturn(groupNames);

        // Test retrieving group subscriptions for a filter given a user
        final SubscriptionManager sm = getSubscriptionManager(null, null, null);

        // Test if the subscription owned by the user is returned
        List<FilterSubscription> subscriptions = sm.getFilterSubscriptions(u, filterId);
        assertThat(subscriptions, contains(subscrip));

        // Create a subscription that is shared with a group that user does not
        // belong to
        final ApplicationUser testUser = createMockUser("test");
        final Group anotherGroup = createMockGroup("anothergroup");

        FilterSubscription anotherSubscription = new DefaultFilterSubscription(null, filterId, testUser.getName(), anotherGroup.getName(), new Timestamp(runAt), true);
        anotherSubscription = entityEngine.createValue(Entity.FILTER_SUBSCRIPTION, anotherSubscription);

        // Ensure that only the old subscription is returned
        subscriptions = sm.getFilterSubscriptions(u, filterId);
        assertThat(subscriptions, contains(subscrip));

        // Put the user into the group
        groupNames.add("anothergroup");
        when(groupManager.getGroupNamesForUser(u)).thenReturn(groupNames);

        // Now test that both subscriptions come back
        subscriptions = sm.getFilterSubscriptions(u, filterId);
        assertThat(subscriptions, containsInAnyOrder(subscrip, anotherSubscription));

        FilterSubscription ownedSubscription = new DefaultFilterSubscription(null, filterId, u.getName(), null, new Timestamp(runAt), false);
        ownedSubscription = entityEngine.createValue(Entity.FILTER_SUBSCRIPTION, ownedSubscription);

        // Ensure it is returned as the user owns it
        subscriptions = sm.getFilterSubscriptions(u, filterId);
        assertThat(subscriptions, containsInAnyOrder(subscrip, anotherSubscription, ownedSubscription));

        // Create another group
        when(groupManager.getGroupNamesForUser(u)).thenReturn(groupNames);

        FilterSubscription yetAnotherSubscription = new DefaultFilterSubscription(null, filterId, testUser.getName(), "testGroup", new Timestamp(runAt), false);
        yetAnotherSubscription = entityEngine.createValue(Entity.FILTER_SUBSCRIPTION, yetAnotherSubscription);

        // Ensure that the subscription is not returned
        subscriptions = sm.getFilterSubscriptions(u, filterId);
        assertThat(subscriptions, containsInAnyOrder(subscrip, anotherSubscription, ownedSubscription));

        // Make the user member fo the group
        groupNames.add("testGroup");
        when(groupManager.getGroupNamesForUser(u)).thenReturn(groupNames);

        // Tets that the subscription is returned
        subscriptions = sm.getFilterSubscriptions(u, filterId);
        assertThat(subscriptions,
                containsInAnyOrder(subscrip, anotherSubscription, ownedSubscription, yetAnotherSubscription));

        // Now remove the user from the groups
        groupNames.clear();
        when(groupManager.getGroupNamesForUser(u)).thenReturn(groupNames);

        // Test that only owned filters are returned
        subscriptions = sm.getFilterSubscriptions(u, filterId);
        assertThat(subscriptions, containsInAnyOrder(subscrip, ownedSubscription));
    }

    protected Group createMockGroup(final String groupName) throws OperationNotPermittedException,
            InvalidGroupException {
        return new MockGroup(groupName);
    }

    @Test
    public void testCreateSubscriptions() throws Exception {
        final SubscriptionManager sm = getSubscriptionManager(null, null, null);
        final FilterSubscription subscription = sm.createSubscription(u, filterId, null, "* * 1", false);
        final List<FilterSubscription> subs = Select.from(Entity.FILTER_SUBSCRIPTION).runWith(ofbiz).asList();

        assertThat(subscription, isIn(subs));

        verify(schedulerService).scheduleJob(any(JobId.class), any(JobConfig.class));
    }

    @Test
    public void testUpdateSubscription() throws Exception {
        final JobId jobId = JobId.of(DefaultSubscriptionManager.SUBSCRIPTION_PREFIX + ':' + subscrip.getId());

        final SubscriptionManager sm = getSubscriptionManager(null, null, null);

        sm.updateSubscription(u, subscrip.getId(), "newgroup", "", true);

        final List subs = ComponentAccessor.getOfBizDelegator().findAll("FilterSubscription");
        assertEquals(1, subs.size());

        verify(schedulerService).unscheduleJob(eq(jobId));
        verify(schedulerService).scheduleJob(any(JobId.class), any(JobConfig.class));
    }

    @Test
    public void testDeleteSubscription() throws Exception {
        final JobId jobId = JobId.of(DefaultSubscriptionManager.SUBSCRIPTION_PREFIX + ':' + subscrip.getId());

        when(schedulerService.getJobDetails(eq(jobId))).thenReturn(Mockito.mock(JobDetails.class));

        final SubscriptionManager sm = getSubscriptionManager(null, null, null);

        sm.deleteSubscription(subscrip.getId());

        final List<FilterSubscription> subs = Select.from(Entity.FILTER_SUBSCRIPTION).runWith(entityEngine).asList();
        assertThat(subs, Matchers.empty());

        verify(schedulerService).unscheduleJob(eq(jobId));
    }

    @Test
    public void testRunSubscription() throws Exception {
        final Timestamp ts = new Timestamp(new Date().getTime());
        final SubscriptionManager sm = getSubscriptionManager(mailQueue, null, null);
        FilterSubscription sub = sm.getFilterSubscription(u, subscrip.getId());
        assertThat(sub.getLastRunTime().getTime(), lessThanOrEqualTo(ts.getTime()));

        sm.runSubscription(subscrip.getId());
        sub = sm.getFilterSubscription(u, subscrip.getId());
        assertThat(sub.getLastRunTime().getTime(), greaterThanOrEqualTo(ts.getTime()));

        verify(mailQueue).addItem(any(SubscriptionMailQueueItem.class));
    }

    @Test
    public void testOmitSubscriptionOfDisabledUser() throws Exception {
        u.setActive(false);
        userManager.addUser(u);
        SubscriptionManager subscriptionManager = getSubscriptionManager(mailQueue, null, null);
        subscriptionManager.getFilterSubscription(u, subscrip.getId());

        subscriptionManager.runSubscription(subscrip.getId());

        verify(mailQueue, Mockito.never()).addItem(any());
    }

    private DefaultSubscriptionManager getSubscriptionManager(MailQueue mailQueue, TemplateManager templateManager,
                                                              SubscriptionMailQueueItemFactory subscriptionMailQueueItemFactory) {
        if (mailQueue == null) {
            mailQueue = getMailQueue();
        }
        if (templateManager == null) {
            templateManager = getComponent(TemplateManager.class);
        }
        if (subscriptionMailQueueItemFactory == null) {
            subscriptionMailQueueItemFactory = getComponent(SubscriptionMailQueueItemFactory.class);
        }

        return new DefaultSubscriptionManager(mailQueue, templateManager, subscriptionMailQueueItemFactory,
                null, groupManager, schedulerService, entityEngine, formatterFactory, userManager);
    }

    @Test
    public void testIsGroupUsed() throws Exception {
        final DefaultSubscriptionManager sm = getSubscriptionManager(null, null, null);
        assertFalse("Group should not be configured", sm.isGroupUsed("myGroup"));

        sm.createSubscription(u, filterId, "myGroup", "* * 1", false);
        assertTrue("Group should be configured", sm.isGroupUsed("myGroup"));
    }
}