package com.atlassian.jira.jql.validator;

import com.atlassian.jira.bc.project.component.ProjectComponentManager;
import com.atlassian.jira.jql.operand.JqlOperandResolver;
import com.atlassian.jira.jql.resolver.ComponentResolver;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.util.MessageSetImpl;
import com.atlassian.query.clause.TerminalClauseImpl;
import com.atlassian.query.operator.Operator;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.empty;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestComponentValidator {
    @Rule
    public final MethodRule initMockito = MockitoJUnit.rule();

    @Mock
    private SupportedOperatorsValidator mockSupportedOperatorsValidator;
    @Mock
    private JqlOperandResolver jqlOperandResolver;
    @Mock
    private RawValuesExistValidator mockRawValuesExistValidator;

    @Test
    public void testRawValuesDelegateNotCalledWithOperatorProblem() throws Exception {
        final TerminalClauseImpl clause = new TerminalClauseImpl("test", Operator.GREATER_THAN, 12L);
        final MessageSetImpl messageSet = new MessageSetImpl();
        messageSet.addErrorMessage("blah blah");
        when(mockSupportedOperatorsValidator.validate(null, clause)).thenReturn(messageSet);

        final ComponentValidator componentValidator = getComponentValidator();

        assertEquals(messageSet, componentValidator.validate(null, clause));
    }

    @Test
    public void testRawValuesDelegateCalledWithValueProblem() throws Exception {
        final TerminalClauseImpl clause = new TerminalClauseImpl("test", Operator.GREATER_THAN, 12L);
        final MessageSetImpl messageSet = new MessageSetImpl();
        messageSet.addErrorMessage("blah blah");

        when(mockSupportedOperatorsValidator.validate(null, clause)).thenReturn(new MessageSetImpl());
        when(mockRawValuesExistValidator.validate(null, clause)).thenReturn(messageSet);

        final ComponentValidator componentValidator = getComponentValidator();

        assertEquals(messageSet, componentValidator.validate(null, clause));
    }

    @Test
    public void testRawValuesDelegateCalledWithNoOperatorProblem() throws Exception {
        final TerminalClauseImpl clause = new TerminalClauseImpl("test", Operator.GREATER_THAN, 12L);

        when(mockSupportedOperatorsValidator.validate(null, clause)).thenReturn(new MessageSetImpl());
        when(mockRawValuesExistValidator.validate(null, clause)).thenReturn(new MessageSetImpl());

        final ComponentValidator componentValidator = getComponentValidator();

        assertThat(componentValidator.validate(null, clause).getErrorMessages(), empty());
    }

    private ComponentValidator getComponentValidator() {
        return new ComponentValidator(null, jqlOperandResolver, null, null, null, null) {
            @Override
            SupportedOperatorsValidator getSupportedOperatorsValidator() {
                return mockSupportedOperatorsValidator;
            }

            @Override
            ValuesExistValidator getValuesValidator(final ComponentResolver componentResolver, final JqlOperandResolver operandSupport, final PermissionManager permissionManager, final ProjectComponentManager projectComponentManager, final ProjectManager projectManager) {
                return mockRawValuesExistValidator;
            }
        };
    }
}
