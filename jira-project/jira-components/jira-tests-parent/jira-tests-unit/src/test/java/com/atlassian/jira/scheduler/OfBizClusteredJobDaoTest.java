package com.atlassian.jira.scheduler;

import com.atlassian.jira.entity.EntityEngine;
import com.atlassian.jira.entity.EntityEngineImpl;
import com.atlassian.jira.exception.DataAccessException;
import com.atlassian.jira.mock.ofbiz.MockOfBizDelegator;
import com.atlassian.scheduler.caesium.impl.ImmutableClusteredJob;
import com.atlassian.scheduler.caesium.spi.ClusteredJob;
import com.atlassian.scheduler.config.JobId;
import com.atlassian.scheduler.config.JobRunnerKey;
import com.atlassian.scheduler.config.Schedule;
import com.google.common.base.Objects;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import static com.atlassian.jira.entity.Entity.CLUSTERED_JOB;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.spy;

/**
 * @since v7.0
 */
public class OfBizClusteredJobDaoTest {
    // Mon 27 Apr 2015 06:10:00 Australia/Sydney
    private static final long EPOCH = 1430079000000L;

    private static final long ID_1 = 1L;
    private static final long ID_2 = 2L;
    private static final long ID_3 = 3L;
    private static final long ID_4 = 4L;
    private static final JobId JOB_ID_1 = JobId.of("job.id.1");
    private static final JobId JOB_ID_2 = JobId.of("job.id.2");
    private static final JobId JOB_ID_3 = JobId.of("job.id.3");
    private static final JobId JOB_ID_4 = JobId.of("job.id.4");
    private static final JobRunnerKey JOB_RUNNER_KEY_1 = JobRunnerKey.of("job.runner.1");
    private static final JobRunnerKey JOB_RUNNER_KEY_2 = JobRunnerKey.of("job.runner.2");
    private static final JobRunnerKey JOB_RUNNER_KEY_3 = JobRunnerKey.of("job.runner.3");
    private static final Date NEXT_RUN_1 = new Date(EPOCH + 30000L);
    private static final Date NEXT_RUN_2 = new Date(EPOCH + 5000L);
    private static final Date NEXT_RUN_3 = new Date(EPOCH);
    private static final Date NEXT_RUN_4 = null;
    private static final Schedule SCHED_1 = Schedule.forInterval(60000L, new Date(EPOCH - 30000L));
    private static final Schedule SCHED_2 = Schedule.runOnce(NEXT_RUN_2);
    private static final Schedule SCHED_3 = Schedule.forCronExpression("0 */5 * * * ?",
            TimeZone.getTimeZone("Australia/Sydney"));
    private static final Schedule SCHED_4 = Schedule.forCronExpression("0 0 2 30 2 ?",
            TimeZone.getTimeZone("Australia/Sydney"));  // Feb-30, so never matches
    private static final byte[] PARAMS_3 = {1, 2, -3, 4, 5};
    private static final OfBizClusteredJob JOB_1 = new OfBizClusteredJob(ID_1, ImmutableClusteredJob.builder()
            .jobId(JOB_ID_1)
            .jobRunnerKey(JOB_RUNNER_KEY_1)
            .schedule(SCHED_1)
            .nextRunTime(NEXT_RUN_1)
            .build());
    private static final OfBizClusteredJob JOB_2 = new OfBizClusteredJob(ID_2, ImmutableClusteredJob.builder()
            .jobId(JOB_ID_2)
            .jobRunnerKey(JOB_RUNNER_KEY_2)
            .schedule(SCHED_2)
            .nextRunTime(NEXT_RUN_2)
            .version(42L)
            .build());
    private static final OfBizClusteredJob JOB_3 = new OfBizClusteredJob(ID_3, ImmutableClusteredJob.builder()
            .jobId(JOB_ID_3)
            .jobRunnerKey(JOB_RUNNER_KEY_3)
            .schedule(SCHED_3)
            .nextRunTime(NEXT_RUN_3)
            .parameters(PARAMS_3)
            .build());
    private static final OfBizClusteredJob JOB_4 = new OfBizClusteredJob(ID_4, ImmutableClusteredJob.builder()
            .jobId(JOB_ID_4)
            .jobRunnerKey(JOB_RUNNER_KEY_2)
            .schedule(SCHED_4)
            .nextRunTime(NEXT_RUN_4)
            .version(3L)
            .build());
    private static final List<OfBizClusteredJob> JOBS = ImmutableList.of(JOB_1, JOB_2, JOB_3, JOB_4);

    private MockOfBizDelegator delegator;
    private EntityEngine entityEngine;
    private OfBizClusteredJobDao clusteredJobDao;

    @Before
    public void setUp() {
        delegator = new MockOfBizDelegator();
        entityEngine = new EntityEngineImpl(delegator);
        clusteredJobDao = new OfBizClusteredJobDao(entityEngine, null);
    }

    @After
    public void tearDown() {
        clusteredJobDao = null;
        entityEngine = null;
        delegator = null;
    }

    @Test
    public void testGetNextRunTime() throws Exception {
        initSimpleData();
        assertThat(clusteredJobDao.getNextRunTime(JOB_ID_1), is(NEXT_RUN_1));
        assertThat(clusteredJobDao.getNextRunTime(JOB_ID_2), is(NEXT_RUN_2));
        assertThat(clusteredJobDao.getNextRunTime(JOB_ID_3), is(NEXT_RUN_3));
        assertThat(clusteredJobDao.getNextRunTime(JOB_ID_4), is(NEXT_RUN_4));
        assertThat(clusteredJobDao.getNextRunTime(JobId.of("missing")), nullValue());
    }

    @Test
    public void testGetVersion() throws Exception {
        initSimpleData();
        assertThat(clusteredJobDao.getVersion(JOB_ID_1), is(1L));
        assertThat(clusteredJobDao.getVersion(JOB_ID_2), is(42L));
        assertThat(clusteredJobDao.getVersion(JOB_ID_3), is(1L));
        assertThat(clusteredJobDao.getVersion(JOB_ID_4), is(3L));
        assertThat(clusteredJobDao.getVersion(JobId.of("missing")), nullValue());
    }

    @Test
    public void testFind() throws Exception {
        initSimpleData();
        assertThat(clusteredJobDao.find(JOB_ID_1), isJob(JOB_1));
        assertThat(clusteredJobDao.find(JOB_ID_2), isJob(JOB_2));
        assertThat(clusteredJobDao.find(JOB_ID_3), isJob(JOB_3));
        assertThat(clusteredJobDao.find(JOB_ID_4), isJob(JOB_4));
        assertThat(clusteredJobDao.find(JobId.of("missing")), nullValue());
    }

    @Test
    public void testFindByJobRunnerKey() throws Exception {
        initSimpleData();
        assertThat(clusteredJobDao.findByJobRunnerKey(JOB_RUNNER_KEY_1), contains(isJob(JOB_1)));
        assertThat(clusteredJobDao.findByJobRunnerKey(JOB_RUNNER_KEY_2),
                containsInAnyOrder(isJob(JOB_2), isJob(JOB_4)));
        assertThat(clusteredJobDao.findByJobRunnerKey(JOB_RUNNER_KEY_3), contains(isJob(JOB_3)));
        assertThat(clusteredJobDao.findByJobRunnerKey(JobRunnerKey.of("missing")), hasSize(0));
    }

    @Test
    public void testRefresh() throws Exception {
        initSimpleData();
        final Map<JobId, Date> expected = ImmutableMap.of(
                JOB_ID_1, NEXT_RUN_1,
                JOB_ID_2, NEXT_RUN_2,
                JOB_ID_3, NEXT_RUN_3);
        // JOB_ID_4 has a NULL value for next run time, so it is excluded
        assertThat(clusteredJobDao.refresh(), is(expected));
    }

    @Test
    public void testFindAllJobRunnerKeys() throws Exception {
        initSimpleData();
        assertThat(clusteredJobDao.findAllJobRunnerKeys(),
                containsInAnyOrder(JOB_RUNNER_KEY_1, JOB_RUNNER_KEY_2, JOB_RUNNER_KEY_3));
    }

    @Test
    public void testCreate_success() throws Exception {
        for (ClusteredJob job : JOBS) {
            assertThat("create", clusteredJobDao.create(job), is(true));
        }
        for (ClusteredJob job : JOBS) {
            assertThat(clusteredJobDao.find(job.getJobId()), isJob(job));
        }
        assertThat(clusteredJobDao.find(JobId.of("missing")), nullValue());
    }

    @Test
    public void testCreate_failure() throws Exception {
        // MockOfBizDelegator doesn't do unique constraints, so we'll have to fake this one out.  The
        // implementation doesn't make any attempt to determine whether the failure was due to a unique
        // constraint or something else, anyway.
        initSpy();
        doThrow(DataAccessException.class).when(entityEngine).createValue(eq(CLUSTERED_JOB), any(OfBizClusteredJob.class));

        assertThat("create", clusteredJobDao.create(JOB_2), is(false));
        assertThat("no jobs created", clusteredJobDao.findAllJobRunnerKeys(), hasSize(0));
    }

    @Test
    public void testUpdateNextRunTime_success() throws Exception {
        initSimpleData();
        final Date updatedRunTime = new Date(EPOCH + 300000L);
        final ClusteredJob modifiedJob3 = ImmutableClusteredJob.builder(JOB_3)
                .nextRunTime(updatedRunTime)
                .version(2L)
                .build();

        assertThat("Should succeed", clusteredJobDao.updateNextRunTime(JOB_ID_3, updatedRunTime, 1L), is(true));
        assertThat("Job should be modified", clusteredJobDao.find(JOB_ID_3), isJob(modifiedJob3));
    }

    @Test
    public void testUpdateNextRunTime_wrongVersion() throws Exception {
        initSimpleData();
        assertThat("should fail to update with version mismatched",
                clusteredJobDao.updateNextRunTime(JOB_ID_3, NEXT_RUN_2, 2495L), is(false));
        assertThat("should not be modified", clusteredJobDao.find(JOB_ID_3), isJob(JOB_3));
    }

    @Test
    public void testUpdateNextRunTime_noSuchJob() throws Exception {
        initSimpleData();
        assertThat(clusteredJobDao.updateNextRunTime(JobId.of("missing"), NEXT_RUN_3, 43L), is(false));
    }

    @Test
    public void testDelete() throws Exception {
        initSimpleData();
        assertThat("JOB_ID_2 (first)", clusteredJobDao.delete(JOB_ID_2), is(true));
        assertThat("JOB_ID_2 (second)", clusteredJobDao.delete(JOB_ID_2), is(false));
        assertThat("Should still have all keys because of JOB_4", clusteredJobDao.findAllJobRunnerKeys(),
                containsInAnyOrder(JOB_RUNNER_KEY_1, JOB_RUNNER_KEY_2, JOB_RUNNER_KEY_3));
        assertThat("JOB_ID_4", clusteredJobDao.delete(JOB_ID_4), is(true));
        assertThat("Should no longer have JOB_RUNNER_KEY_2", clusteredJobDao.findAllJobRunnerKeys(),
                containsInAnyOrder(JOB_RUNNER_KEY_1, JOB_RUNNER_KEY_3));
        assertThat("missing", clusteredJobDao.delete(JobId.of("missing")), is(false));
    }


    void initSimpleData() {
        for (OfBizClusteredJob job : JOBS) {
            entityEngine.createValue(CLUSTERED_JOB, job);
        }
    }

    void initSpy() {
        entityEngine = spy(entityEngine);
        clusteredJobDao = new OfBizClusteredJobDao(entityEngine, null);
    }

    static Matcher<? super ClusteredJob> isJob(ClusteredJob expected) {
        return new JobMatcher(expected);
    }

    static class JobMatcher extends TypeSafeMatcher<ClusteredJob> {
        private final ClusteredJob expected;

        JobMatcher(final ClusteredJob expected) {
            this.expected = expected;
        }

        @Override
        protected boolean matchesSafely(final ClusteredJob actual) {
            return Objects.equal(expected.getJobId(), actual.getJobId())
                    && Objects.equal(expected.getJobRunnerKey(), actual.getJobRunnerKey())
                    && Objects.equal(expected.getNextRunTime(), actual.getNextRunTime())
                    && Objects.equal(expected.getSchedule(), actual.getSchedule())
                    && Objects.equal(expected.getVersion(), actual.getVersion())
                    && Arrays.equals(expected.getRawParameters(), actual.getRawParameters());
        }

        @Override
        public void describeTo(final Description description) {
            description.appendValue(expected);
        }
    }
}