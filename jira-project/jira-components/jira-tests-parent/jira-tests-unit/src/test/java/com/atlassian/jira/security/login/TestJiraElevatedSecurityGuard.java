package com.atlassian.jira.security.login;

import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.mockito.Mock;
import org.mockito.Mockito;

import javax.servlet.http.HttpServletRequest;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

/**
 */
public class TestJiraElevatedSecurityGuard {
    @Rule
    public final RuleChain mockitoMocks = MockitoMocksInContainer.forTest(this);

    @Mock
    @AvailableInContainer
    private LoginManager loginManager;
    @Mock
    private HttpServletRequest httpServletRequest;

    private JiraElevatedSecurityGuard securityGuard = new JiraElevatedSecurityGuard();

    @Test
    public void testPerformElevatedSecurityCheck() {
        when(loginManager.performElevatedSecurityCheck(httpServletRequest, "userName")).thenReturn(true);
        assertTrue(securityGuard.performElevatedSecurityCheck(httpServletRequest, "userName"));
    }

    @Test
    public void testOnFailedLoginAttempt() {
        securityGuard.onFailedLoginAttempt(httpServletRequest, "userName");
        Mockito.verify(loginManager).onLoginAttempt(httpServletRequest, "userName", false);
    }

    @Test
    public void testOnSuccessfulLoginAttempt() {
        securityGuard.onSuccessfulLoginAttempt(httpServletRequest, "userName");
        Mockito.verify(loginManager).onLoginAttempt(httpServletRequest, "userName", true);
    }
}
