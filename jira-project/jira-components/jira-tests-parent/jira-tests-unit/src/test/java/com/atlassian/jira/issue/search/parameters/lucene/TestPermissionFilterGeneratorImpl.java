package com.atlassian.jira.issue.search.parameters.lucene;

import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.security.plugin.ProjectPermissionKey;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.Query;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestPermissionFilterGeneratorImpl {


    @Rule
    public final MethodRule initMockito = MockitoJUnit.rule();
    private ApplicationUser theUser;

    @Mock
    private PermissionsFilterCache cache;

    @Before
    public void setUp() {
        theUser = new MockApplicationUser("fred");
    }

    @After
    public void tearDown() {
        theUser = null;
    }

    @Test
    public void testGetQueryNoCache() throws Exception {
        final BooleanQuery generatedQuery = new BooleanQuery();

        when(cache.getQuery(theUser)).thenReturn(null);

        cache.storeQuery(generatedQuery, theUser);

        final PermissionQueryFactory permissionQueryFactory = new PermissionQueryFactory() {
            @Override
            public Query getQuery(ApplicationUser searcher, ProjectPermissionKey permissionId) {
                assertEquals(theUser, searcher);
                assertEquals(ProjectPermissions.BROWSE_PROJECTS, permissionId);
                return generatedQuery;
            }
        };

        final PermissionsFilterGeneratorImpl generator = new PermissionsFilterGeneratorImpl(permissionQueryFactory) {
            @Override
            PermissionsFilterCache getCache() {
                return cache;
            }
        };

        final Query result = generator.getQuery(theUser);

        assertSame(generatedQuery, result);
        verify(cache, atLeastOnce()).storeQuery(generatedQuery, theUser);
    }

    @Test
    public void testGetQueryCached() throws Exception {
        final BooleanQuery cachedQuery = new BooleanQuery();

        when(cache.getQuery(theUser)).thenReturn(cachedQuery);

        final PermissionQueryFactory permissionQueryFactory = new PermissionQueryFactory() {
            @Override
            public Query getQuery(ApplicationUser searcher, ProjectPermissionKey permissionId) {
                fail("Should not be called as query was cached");
                return null;
            }
        };

        final PermissionsFilterGeneratorImpl generator = new PermissionsFilterGeneratorImpl(permissionQueryFactory) {
            @Override
            PermissionsFilterCache getCache() {
                return cache;
            }
        };

        final Query result = generator.getQuery(theUser);

        assertSame(cachedQuery, result);
    }
}
