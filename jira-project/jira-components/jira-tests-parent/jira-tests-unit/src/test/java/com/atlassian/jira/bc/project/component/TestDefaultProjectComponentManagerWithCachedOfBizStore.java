package com.atlassian.jira.bc.project.component;

import com.atlassian.beehive.ClusterLockService;
import com.atlassian.cache.memory.MemoryCacheManager;
import com.atlassian.jira.mock.ofbiz.MockOfBizDelegator;

public class TestDefaultProjectComponentManagerWithCachedOfBizStore
        extends TestDefaultProjectComponentManagerWithOfBizStore {

    protected ProjectComponentStore createStore(MockOfBizDelegator ofBizDelegator, final ClusterLockService clusterLockService) {
        return new CachingProjectComponentStore(new OfBizProjectComponentStore(ofBizDelegator, null), clusterLockService, new MemoryCacheManager());
    }
}
