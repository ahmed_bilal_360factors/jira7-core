package com.atlassian.jira.instrumentation.jdbc;

import com.google.common.collect.ImmutableList;
import org.junit.Test;

import java.util.List;

import static org.hamcrest.Matchers.contains;
import static org.junit.Assert.assertThat;

public class StackTraceHelperTest {
    @Test
    public void filtersOutIrrelevantElements() {
        List<StackTraceElement> stackTrace = ImmutableList.of(
                createElement("com.github.gquintana.metrics.sql.DefaultMetricNamingStrategy"),
                createElement("com.github.gquintana.metrics.sql.SomeOtherClass"),
                createElement("com.atlassian.jira.VeryRelevantClass"),
                createElement("com.github.gquintana.metrics.sql.Discard"),
                createElement("com.atlassian.jira.AnotherRelevantClass")
        );

        final List<StackTraceElement> stackTraceElements = StackTraceHelper.filterStackTrace(stackTrace);

        assertThat(stackTraceElements, contains(
                createElement("com.atlassian.jira.VeryRelevantClass"),
                createElement("com.atlassian.jira.AnotherRelevantClass")
        ));
    }

    private StackTraceElement createElement(String className) {
        return new StackTraceElement(className, "nvrmnd", null, 0);
    }
}