package com.atlassian.jira.plugin.jql.function;

import com.atlassian.jira.JiraDataTypes;
import com.atlassian.jira.issue.vote.VotedIssuesAccessor;
import com.atlassian.jira.jql.operand.QueryLiteral;
import com.atlassian.jira.jql.query.QueryCreationContext;
import com.atlassian.jira.jql.query.QueryCreationContextImpl;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.util.MessageSet;
import com.atlassian.query.clause.TerminalClause;
import com.atlassian.query.operand.FunctionOperand;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.mockito.Mock;

import java.util.List;

import static com.atlassian.jira.issue.vote.VotedIssuesAccessor.Security.OVERRIDE;
import static com.atlassian.jira.issue.vote.VotedIssuesAccessor.Security.RESPECT;
import static com.atlassian.jira.mock.plugin.jql.operand.MockJqlFunctionModuleDescriptor.create;
import static java.util.Collections.singletonList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestVotedIssuesFunction {
    @Rule
    public TestRule container = MockitoMocksInContainer.forTest(this);

    @Mock
    private VotedIssuesAccessor votedIssuesAccessor;

    private ApplicationUser theUser = new MockApplicationUser("fred");
    private QueryCreationContext queryCreationContext = new QueryCreationContextImpl(theUser);
    private VotedIssuesFunction votedIssuesFunction;
    private TerminalClause terminalClause = null;

    @Before
    public void setUp() throws Exception {
        this.votedIssuesFunction = new VotedIssuesFunction(votedIssuesAccessor);
    }

    @Test
    public void testDataType() throws Exception {

        assertEquals(JiraDataTypes.ISSUE, votedIssuesFunction.getDataType());
    }

    @Test
    public void testValidateVotingDisabled() throws Exception {
        when(votedIssuesAccessor.isVotingEnabled()).thenReturn(false);

        votedIssuesFunction.init(create("votedIssues", true));

        final MessageSet messageSet = votedIssuesFunction.validate(null, new FunctionOperand("votedIssues"), terminalClause);
        assertTrue(messageSet.hasAnyErrors());
        assertEquals(1, messageSet.getErrorMessages().size());
        assertEquals("Function 'votedIssues' cannot be called as voting on issues is currently disabled.", messageSet.getErrorMessages().iterator().next());
    }

    @Test
    public void testValidateWrongArgs() throws Exception {
        when(votedIssuesAccessor.isVotingEnabled()).thenReturn(true);

        votedIssuesFunction.init(create("votedIssues", true));

        final MessageSet messageSet = votedIssuesFunction.validate(new MockApplicationUser("bob"), new FunctionOperand("votedIssues", "badArg"), terminalClause);
        assertTrue(messageSet.hasAnyErrors());
        assertEquals(1, messageSet.getErrorMessages().size());
        assertEquals("Function 'votedIssues' expected '0' arguments but received '1'.", messageSet.getErrorMessages().iterator().next());
    }

    @Test
    public void testValidateHappyPath() throws Exception {
        when(votedIssuesAccessor.isVotingEnabled()).thenReturn(true);

        votedIssuesFunction.init(create("votedIssues", true));

        final MessageSet messageSet = votedIssuesFunction.validate(new MockApplicationUser("bob"), new FunctionOperand("votedIssues"), terminalClause);
        assertFalse(messageSet.hasAnyErrors());
    }

    @Test
    public void testValidateAnonymous() {
        when(votedIssuesAccessor.isVotingEnabled()).thenReturn(true);

        votedIssuesFunction.init(create("votedIssues", true));

        final MessageSet messageSet = votedIssuesFunction.validate(null, new FunctionOperand("votedIssues"), terminalClause);
        assertTrue(messageSet.hasAnyErrors());
        assertEquals(1, messageSet.getErrorMessages().size());
        assertEquals("Function 'votedIssues' cannot be called as anonymous user.", messageSet.getErrorMessages().iterator().next());
    }

    @Test
    public void testGetVotedIssuesDontOverrideSecurity() throws Exception {
        final Iterable<Long> voted = singletonList(55L);

        when(votedIssuesAccessor.getVotedIssueIds(theUser, theUser, RESPECT)).thenReturn(voted);

        final Iterable<Long> list = votedIssuesFunction.getVotedIssues(theUser, false);
        assertEquals(voted, list);
    }

    @Test
    public void testGetVotedIssuesOverrideSecurity() throws Exception {
        final Iterable<Long> voted = singletonList(55L);


        when(votedIssuesAccessor.getVotedIssueIds(theUser, theUser, OVERRIDE)).thenReturn(voted);

        final Iterable<Long> list = votedIssuesFunction.getVotedIssues(theUser, true);
        assertEquals(voted, list);
    }

    @Test
    public void testGetValuesHappyPath() throws Exception {
        final List<Long> voted = singletonList(55L);


        when(votedIssuesAccessor.getVotedIssueIds(theUser, theUser, RESPECT)).thenReturn(voted);

        final List<QueryLiteral> list = votedIssuesFunction.getValues(queryCreationContext, new FunctionOperand("votedIssues"), terminalClause);
        assertEquals(1, list.size());
        assertEquals(new Long(55), list.get(0).getLongValue());
    }

    @Test
    public void testGetValuesAnonymous() {
        final List<QueryLiteral> list = votedIssuesFunction.getValues(new QueryCreationContextImpl((ApplicationUser) null), new FunctionOperand("votedIssues"), terminalClause);
        assertTrue(list.isEmpty());
    }

    @Test
    public void testGetMinimumNumberOfExpectedArguments() throws Exception {
        assertEquals(0, votedIssuesFunction.getMinimumNumberOfExpectedArguments());
    }

}
