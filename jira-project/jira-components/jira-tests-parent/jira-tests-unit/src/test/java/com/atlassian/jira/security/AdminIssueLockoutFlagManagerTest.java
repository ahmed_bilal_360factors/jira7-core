package com.atlassian.jira.security;

import com.atlassian.jira.application.ApplicationRoleManager;
import com.atlassian.jira.event.user.LoginEvent;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.mock.security.MockSimpleAuthenticationContext;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.flag.FlagDismissalService;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.mockito.Mock;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

public class AdminIssueLockoutFlagManagerTest {
    @Rule
    public RuleChain obj = MockitoMocksInContainer.forTest(this);

    @Mock
    private GlobalPermissionManager globalPermissionManager;
    @Mock
    private FlagDismissalService flagDismissalService;
    @Mock
    private ApplicationRoleManager applicationRoleManager;
    private AdminIssueLockoutFlagManager flagManager;
    private ApplicationUser admin = new MockApplicationUser("admin");
    private ApplicationUser notAdmin = new MockApplicationUser("notAdmin");

    @AvailableInContainer
    private JiraAuthenticationContext context = MockSimpleAuthenticationContext.createNoopContext(admin);

    @Before
    public void setup() {
        flagManager = new AdminIssueLockoutFlagManager(globalPermissionManager, flagDismissalService, applicationRoleManager);

        when(globalPermissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, admin)).thenReturn(true);
        when(globalPermissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, notAdmin)).thenReturn(false);
        when(applicationRoleManager.hasAnyRole(admin)).thenReturn(true);
        when(applicationRoleManager.hasAnyRole(notAdmin)).thenReturn(true);
    }

    @Test
    public void resetIgnoredIfNotAdmin() {
        //when
        flagManager.removeDismissalOnLogin(new LoginEvent(notAdmin));

        //then
        verifyNoMoreInteractions(flagDismissalService);
    }

    @Test
    public void resetIgnoredForAnonymous() {
        //given
        when(globalPermissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, null)).thenReturn(true);

        //when
        flagManager.removeDismissalOnLogin(new LoginEvent(null));

        //then
        verifyNoMoreInteractions(flagDismissalService);
    }

    @Test
    public void resetIgnoredIfAdminDoesNotHaveApplication() {
        //given
        when(applicationRoleManager.hasAnyRole(admin)).thenReturn(false);

        //when
        flagManager.removeDismissalOnLogin(new LoginEvent(null));

        //then
        verifyNoMoreInteractions(flagDismissalService);

    }

    @Test
    public void resetPerformedIfAdminDoesHasApplication() {
        //when
        flagManager.removeDismissalOnLogin(new LoginEvent(admin));

        //then
        verify(flagDismissalService).removeDismissFlagForUser(AdminIssueLockoutFlagManager.FLAG, admin);
    }

    @Test
    public void isAdminWithoutPermissionIsFalseForAnonymousUser() {
        //given
        when(globalPermissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, null)).thenReturn(true);

        //when
        final boolean permission = flagManager.isAdminWithoutIssuePermission(null);

        //then
        assertThat(permission, is(false));

    }

    @Test
    public void isAdminWithoutPermissionIsFalseIfNotAdmin() {
        //when
        final boolean permission = flagManager.isAdminWithoutIssuePermission(notAdmin);

        //then
        assertThat(permission, is(false));

    }

    @Test
    public void isAdminWithoutPermissionIsFalseWhenAdminHasApplications() {
        //when
        final boolean permission = flagManager.isAdminWithoutIssuePermission(admin);

        //then
        assertThat(permission, is(false));

    }

    @Test
    public void isAdminWithoutPermissionIsTrueWhenAdminHasNoApplications() {
        //given
        when(applicationRoleManager.hasAnyRole(admin)).thenReturn(false);

        //when
        final boolean permission = flagManager.isAdminWithoutIssuePermission(admin);

        //then
        assertThat(permission, is(true));
    }
}