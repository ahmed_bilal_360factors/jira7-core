package com.atlassian.jira.issue.customfields.searchers.transformer;

import com.atlassian.jira.issue.customfields.converters.DoubleConverter;
import com.atlassian.jira.issue.customfields.impl.FieldValidationException;
import com.atlassian.jira.issue.customfields.searchers.NumberRangeSearcher;
import com.atlassian.jira.issue.customfields.view.CustomFieldParams;
import com.atlassian.jira.issue.customfields.view.CustomFieldParamsImpl;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.search.ClauseNames;
import com.atlassian.jira.issue.search.SearchContext;
import com.atlassian.jira.issue.transport.FieldValuesHolder;
import com.atlassian.jira.issue.transport.impl.FieldValuesHolderImpl;
import com.atlassian.jira.jql.operand.JqlOperandResolver;
import com.atlassian.jira.jql.operand.QueryLiteral;
import com.atlassian.jira.jql.validator.MockJqlOperandResolver;
import com.atlassian.jira.mock.i18n.MockI18nHelper;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.jira.util.collect.CollectionBuilder;
import com.atlassian.jira.util.collect.MapBuilder;
import com.atlassian.query.Query;
import com.atlassian.query.QueryImpl;
import com.atlassian.query.clause.AndClause;
import com.atlassian.query.clause.Clause;
import com.atlassian.query.clause.TerminalClause;
import com.atlassian.query.clause.TerminalClauseImpl;
import com.atlassian.query.operand.FunctionOperand;
import com.atlassian.query.operator.Operator;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import java.util.Collections;

import static com.atlassian.jira.jql.operand.SimpleLiteralFactory.createLiteral;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestNumberRangeCustomFieldSearchInputTransformer {
    @Rule
    public MethodRule initMockito = MockitoJUnit.rule();

    private final ClauseNames clauseNames = new ClauseNames("cf[100]");
    private String url = "cf_100";
    private String id = "cf_100";
    private MockI18nHelper i18nHelper;
    private JqlOperandResolver jqlOperandResolver;
    @Mock
    private CustomField customField;
    @Mock
    private DoubleConverter doubleConverter;
    @Mock
    private NumberRangeCustomFieldInputHelper inputHelper;
    @Mock
    private CustomFieldInputHelper customFieldInputHelper;
    @Mock
    private SearchContext searchContext;
    private ApplicationUser theUser = null;

    @Before
    public void setUp() throws Exception {
        i18nHelper = new MockI18nHelper();
        jqlOperandResolver = MockJqlOperandResolver.createSimpleSupport();

        when(customField.getId()).thenReturn(id);
    }

    @Test
    public void testValidateParamsNullParams() throws Exception {
        FieldValuesHolder holder = new FieldValuesHolderImpl();
        final SimpleErrorCollection errors = new SimpleErrorCollection();

        NumberRangeCustomFieldSearchInputTransformer transformer = new NumberRangeCustomFieldSearchInputTransformer(clauseNames, customField, url, doubleConverter, jqlOperandResolver, customFieldInputHelper);

        transformer.validateParams(null, null, holder, i18nHelper, errors);

        assertFalse(errors.hasAnyErrors());
    }

    @Test
    public void testValidateParamsNoParams() throws Exception {
        CustomFieldParams customFieldParams = new CustomFieldParamsImpl();
        FieldValuesHolder holder = new FieldValuesHolderImpl(MapBuilder.newBuilder(id, customFieldParams).toMap());
        final SimpleErrorCollection errors = new SimpleErrorCollection();

        NumberRangeCustomFieldSearchInputTransformer transformer = new NumberRangeCustomFieldSearchInputTransformer(clauseNames, customField, url, doubleConverter, jqlOperandResolver, customFieldInputHelper);

        transformer.validateParams(null, null, holder, i18nHelper, errors);

        assertFalse(errors.hasAnyErrors());
    }

    @Test
    public void testValidateParamsInvalidNumber() throws Exception {
        CustomFieldParams customFieldParams = new CustomFieldParamsImpl(customField, MapBuilder.newBuilder().add(NumberRangeSearcher.GREATER_THAN_PARAM, Collections.singleton("blah")).toMap());
        FieldValuesHolder holder = new FieldValuesHolderImpl(MapBuilder.newBuilder(id, customFieldParams).toMap());
        final SimpleErrorCollection errors = new SimpleErrorCollection();

        doThrow(new FieldValidationException("blah")).when(doubleConverter).getDouble("blah");

        NumberRangeCustomFieldSearchInputTransformer transformer = new NumberRangeCustomFieldSearchInputTransformer(clauseNames, customField, url, doubleConverter, jqlOperandResolver, customFieldInputHelper);

        transformer.validateParams(null, null, holder, i18nHelper, errors);

        assertTrue(errors.hasAnyErrors());
        assertEquals("blah", errors.getErrors().get(id));
    }

    @Test
    public void testValidateParamsValid() throws Exception {
        CustomFieldParams customFieldParams = new CustomFieldParamsImpl(customField, MapBuilder.newBuilder().add(NumberRangeSearcher.GREATER_THAN_PARAM, Collections.singleton("blah")).toMap());
        FieldValuesHolder holder = new FieldValuesHolderImpl(MapBuilder.newBuilder(id, customFieldParams).toMap());
        final SimpleErrorCollection errors = new SimpleErrorCollection();

        when(doubleConverter.getDouble("blah")).thenReturn(10D);

        NumberRangeCustomFieldSearchInputTransformer transformer = new NumberRangeCustomFieldSearchInputTransformer(clauseNames, customField, url, doubleConverter, jqlOperandResolver, customFieldInputHelper);

        transformer.validateParams(null, null, holder, i18nHelper, errors);

        assertFalse(errors.hasAnyErrors());
    }

    @Test
    public void testGetParamsFromSearchRequestNoWhereClause() throws Exception {
        NumberRangeCustomFieldSearchInputTransformer transformer = new NumberRangeCustomFieldSearchInputTransformer(clauseNames, customField, url, doubleConverter, jqlOperandResolver, customFieldInputHelper);

        assertNull(transformer.getParamsFromSearchRequest(null, new QueryImpl(), searchContext));
    }

    @Test
    public void testGetParamsFromSearchRequestHelperReturnsNull() throws Exception {
        final TerminalClauseImpl clause = new TerminalClauseImpl("blah", Operator.LESS_THAN_EQUALS, "blah");
        final Query query = new QueryImpl(clause);

        when(inputHelper.getValuesFromQuery(query)).thenReturn(null);

        NumberRangeCustomFieldSearchInputTransformer transformer = new NumberRangeCustomFieldSearchInputTransformer(clauseNames, customField, url, doubleConverter, jqlOperandResolver, customFieldInputHelper) {
            @Override
            NumberRangeCustomFieldInputHelper createInputHelper(final ClauseNames clauseNames, final JqlOperandResolver jqlOperandResolver) {
                return inputHelper;
            }
        };

        assertNull(transformer.getParamsFromSearchRequest(null, query, searchContext));
    }

    @Test
    public void testGetParamsFromSearchRequestHelperReturnsNoClauses() throws Exception {
        final TerminalClauseImpl clause = new TerminalClauseImpl("blah", Operator.LESS_THAN_EQUALS, "blah");
        final Query query = new QueryImpl(clause);

        when(inputHelper.getValuesFromQuery(query)).thenReturn(Collections.emptyList());

        NumberRangeCustomFieldSearchInputTransformer transformer = new NumberRangeCustomFieldSearchInputTransformer(clauseNames, customField, url, doubleConverter, jqlOperandResolver, customFieldInputHelper) {
            @Override
            NumberRangeCustomFieldInputHelper createInputHelper(final ClauseNames clauseNames, final JqlOperandResolver jqlOperandResolver) {
                return inputHelper;
            }
        };

        assertNull(transformer.getParamsFromSearchRequest(null, query, searchContext));
    }

    @Test
    public void testGetParamsFromSearchRequestMultipleLiteralsResolved() throws Exception {
        final Query query = new QueryImpl(new TerminalClauseImpl("blah", Operator.LESS_THAN_EQUALS, "blah"));

        final FunctionOperand operand = new FunctionOperand("blah");
        final TerminalClause clause1 = new TerminalClauseImpl(clauseNames.getPrimaryName(), Operator.LESS_THAN_EQUALS, operand);
        when(inputHelper.getValuesFromQuery(query)).thenReturn(CollectionBuilder.newBuilder(clause1).asList());

        jqlOperandResolver = mock(JqlOperandResolver.class);
        when(jqlOperandResolver.getValues(theUser, operand, clause1)).thenReturn(CollectionBuilder.newBuilder(createLiteral("1"), createLiteral("2")).asList());

        NumberRangeCustomFieldSearchInputTransformer transformer = new NumberRangeCustomFieldSearchInputTransformer(clauseNames, customField, url, doubleConverter, jqlOperandResolver, customFieldInputHelper) {
            @Override
            NumberRangeCustomFieldInputHelper createInputHelper(final ClauseNames clauseNames, final JqlOperandResolver jqlOperandResolver) {
                return inputHelper;
            }
        };

        final CustomFieldParams result = transformer.getParamsFromSearchRequest(theUser, query, searchContext);
        assertNull(result);
    }

    @Test
    public void testGetParamsFromSearchRequestEmptyLiteralResolved() throws Exception {
        final Query query = new QueryImpl(new TerminalClauseImpl("blah", Operator.LESS_THAN_EQUALS, "blah"));

        final FunctionOperand operand = new FunctionOperand("blah");
        final TerminalClause clause1 = new TerminalClauseImpl(clauseNames.getPrimaryName(), Operator.LESS_THAN_EQUALS, operand);
        when(inputHelper.getValuesFromQuery(query)).thenReturn(CollectionBuilder.newBuilder(clause1).asList());

        jqlOperandResolver = mock(JqlOperandResolver.class);
        when(jqlOperandResolver.getValues(theUser, operand, clause1)).thenReturn(CollectionBuilder.newBuilder(new QueryLiteral()).asList());

        NumberRangeCustomFieldSearchInputTransformer transformer = new NumberRangeCustomFieldSearchInputTransformer(clauseNames, customField, url, doubleConverter, jqlOperandResolver, customFieldInputHelper) {
            @Override
            NumberRangeCustomFieldInputHelper createInputHelper(final ClauseNames clauseNames, final JqlOperandResolver jqlOperandResolver) {
                return inputHelper;
            }
        };

        final CustomFieldParams result = transformer.getParamsFromSearchRequest(theUser, query, searchContext);
        assertNull(result);
    }

    @Test
    public void testGetParamsFromSearchRequestHelperClauses() throws Exception {
        final Query query = new QueryImpl(new TerminalClauseImpl("blah", Operator.LESS_THAN_EQUALS, "blah"));

        final TerminalClause clause1 = new TerminalClauseImpl(clauseNames.getPrimaryName(), Operator.LESS_THAN_EQUALS, "10");
        final TerminalClause clause2 = new TerminalClauseImpl(clauseNames.getPrimaryName(), Operator.GREATER_THAN_EQUALS, "15");
        when(inputHelper.getValuesFromQuery(query)).thenReturn(CollectionBuilder.newBuilder(clause1, clause2).asList());

        NumberRangeCustomFieldSearchInputTransformer transformer = new NumberRangeCustomFieldSearchInputTransformer(clauseNames, customField, url, doubleConverter, jqlOperandResolver, customFieldInputHelper) {
            @Override
            NumberRangeCustomFieldInputHelper createInputHelper(final ClauseNames clauseNames, final JqlOperandResolver jqlOperandResolver) {
                return inputHelper;
            }
        };

        final CustomFieldParams result = transformer.getParamsFromSearchRequest(null, query, searchContext);
        final CustomFieldParamsImpl expectedResult = new CustomFieldParamsImpl(customField, MapBuilder.newBuilder()
                .add(NumberRangeSearcher.GREATER_THAN_PARAM, Collections.singleton("15"))
                .add(NumberRangeSearcher.LESS_THAN_PARAM, Collections.singleton("10"))
                .toMap());

        assertEquals(result, expectedResult);
    }

    @Test
    public void testDoRelevantClausesFitFilterFormNoWhereClause() throws Exception {
        NumberRangeCustomFieldSearchInputTransformer transformer = new NumberRangeCustomFieldSearchInputTransformer(clauseNames, customField, url, doubleConverter, jqlOperandResolver, customFieldInputHelper) {
            @Override
            NumberRangeCustomFieldInputHelper createInputHelper(final ClauseNames clauseNames, final JqlOperandResolver jqlOperandResolver) {
                return inputHelper;
            }
        };

        assertTrue(transformer.doRelevantClausesFitFilterForm(null, new QueryImpl(), searchContext));
    }

    @Test
    public void testDoRelevantClausesFitFilterFormInputHelperReturnsNull() throws Exception {
        final Query query = new QueryImpl(new TerminalClauseImpl("blah", Operator.LESS_THAN_EQUALS, "blah"));

        when(inputHelper.getValuesFromQuery(query)).thenReturn(null);

        NumberRangeCustomFieldSearchInputTransformer transformer = new NumberRangeCustomFieldSearchInputTransformer(clauseNames, customField, url, doubleConverter, jqlOperandResolver, customFieldInputHelper) {
            @Override
            NumberRangeCustomFieldInputHelper createInputHelper(final ClauseNames clauseNames, final JqlOperandResolver jqlOperandResolver) {
                return inputHelper;
            }
        };

        assertFalse(transformer.doRelevantClausesFitFilterForm(null, query, searchContext));
    }

    @Test
    public void testDoRelevantClausesFitFilterFormInputHelperReturnsNotNull() throws Exception {
        final Query query = new QueryImpl(new TerminalClauseImpl("blah", Operator.LESS_THAN_EQUALS, "blah"));

        when(inputHelper.getValuesFromQuery(query)).thenReturn(Collections.emptyList());

        NumberRangeCustomFieldSearchInputTransformer transformer = new NumberRangeCustomFieldSearchInputTransformer(clauseNames, customField, url, doubleConverter, jqlOperandResolver, customFieldInputHelper) {
            @Override
            NumberRangeCustomFieldInputHelper createInputHelper(final ClauseNames clauseNames, final JqlOperandResolver jqlOperandResolver) {
                return inputHelper;
            }
        };

        assertTrue(transformer.doRelevantClausesFitFilterForm(null, query, searchContext));
    }

    @Test
    public void testGetSearchClauseNullParams() throws Exception {
        FieldValuesHolder holder = new FieldValuesHolderImpl();
        NumberRangeCustomFieldSearchInputTransformer transformer = new NumberRangeCustomFieldSearchInputTransformer(clauseNames, customField, url, doubleConverter, jqlOperandResolver, customFieldInputHelper);

        assertNull(transformer.getSearchClause(null, holder));
    }

    @Test
    public void testGetSearchClauseNoParams() throws Exception {
        CustomFieldParams customFieldParams = new CustomFieldParamsImpl();
        FieldValuesHolder holder = new FieldValuesHolderImpl(MapBuilder.newBuilder(id, customFieldParams).toMap());

        NumberRangeCustomFieldSearchInputTransformer transformer = new NumberRangeCustomFieldSearchInputTransformer(clauseNames, customField, url, doubleConverter, jqlOperandResolver, customFieldInputHelper);

        assertNull(transformer.getSearchClause(null, holder));
    }

    @Test
    public void testGetSearchClauseLessThanParam() throws Exception {
        when(customField.getUntranslatedName()).thenReturn("ABC");
        when(customFieldInputHelper.getUniqueClauseName(theUser, clauseNames.getPrimaryName(), "ABC")).thenReturn(clauseNames.getPrimaryName());

        CustomFieldParams customFieldParams = new CustomFieldParamsImpl(customField, MapBuilder.newBuilder()
                .add(NumberRangeSearcher.LESS_THAN_PARAM, Collections.singleton("10"))
                .toMap());
        FieldValuesHolder holder = new FieldValuesHolderImpl(MapBuilder.newBuilder(id, customFieldParams).toMap());

        NumberRangeCustomFieldSearchInputTransformer transformer = new NumberRangeCustomFieldSearchInputTransformer(clauseNames, customField, url, doubleConverter, jqlOperandResolver, customFieldInputHelper);

        final Clause result = transformer.getSearchClause(null, holder);
        final TerminalClauseImpl expectedResult = new TerminalClauseImpl(clauseNames.getPrimaryName(), Operator.LESS_THAN_EQUALS, "10");
        assertEquals(expectedResult, result);
    }

    @Test
    public void testGetSearchClauseGreaterThanParam() throws Exception {
        when(customField.getUntranslatedName()).thenReturn("ABC");
        when(customFieldInputHelper.getUniqueClauseName(theUser, clauseNames.getPrimaryName(), "ABC")).thenReturn(clauseNames.getPrimaryName());

        CustomFieldParams customFieldParams = new CustomFieldParamsImpl(customField, MapBuilder.newBuilder()
                .add(NumberRangeSearcher.GREATER_THAN_PARAM, Collections.singleton("10"))
                .toMap());
        FieldValuesHolder holder = new FieldValuesHolderImpl(MapBuilder.newBuilder(id, customFieldParams).toMap());

        NumberRangeCustomFieldSearchInputTransformer transformer = new NumberRangeCustomFieldSearchInputTransformer(clauseNames, customField, url, doubleConverter, jqlOperandResolver, customFieldInputHelper);

        final Clause result = transformer.getSearchClause(null, holder);
        final TerminalClauseImpl expectedResult = new TerminalClauseImpl(clauseNames.getPrimaryName(), Operator.GREATER_THAN_EQUALS, "10");
        assertEquals(expectedResult, result);
    }

    @Test
    public void testGetSearchClauseBothParams() throws Exception {
        when(customField.getUntranslatedName()).thenReturn("ABC");
        when(customFieldInputHelper.getUniqueClauseName(theUser, clauseNames.getPrimaryName(), "ABC")).thenReturn(clauseNames.getPrimaryName());

        CustomFieldParams customFieldParams = new CustomFieldParamsImpl(customField, MapBuilder.newBuilder()
                .add(NumberRangeSearcher.GREATER_THAN_PARAM, Collections.singleton("10"))
                .add(NumberRangeSearcher.LESS_THAN_PARAM, Collections.singleton("15"))
                .toMap());
        FieldValuesHolder holder = new FieldValuesHolderImpl(MapBuilder.newBuilder(id, customFieldParams).toMap());

        NumberRangeCustomFieldSearchInputTransformer transformer = new NumberRangeCustomFieldSearchInputTransformer(clauseNames, customField, url, doubleConverter, jqlOperandResolver, customFieldInputHelper);

        final Clause result = transformer.getSearchClause(null, holder);
        final Clause expectedResult = new AndClause(
                new TerminalClauseImpl(clauseNames.getPrimaryName(), Operator.LESS_THAN_EQUALS, "15"),
                new TerminalClauseImpl(clauseNames.getPrimaryName(), Operator.GREATER_THAN_EQUALS, "10"));
        assertEquals(expectedResult, result);
    }

    @Test
    public void testGetSearchClauseNoRelevantParams() throws Exception {
        CustomFieldParams customFieldParams = new CustomFieldParamsImpl(customField, MapBuilder.newBuilder().toMap());
        FieldValuesHolder holder = new FieldValuesHolderImpl(MapBuilder.newBuilder(id, customFieldParams).toMap());

        NumberRangeCustomFieldSearchInputTransformer transformer = new NumberRangeCustomFieldSearchInputTransformer(clauseNames, customField, url, doubleConverter, jqlOperandResolver, customFieldInputHelper);

        assertNull(transformer.getSearchClause(null, holder));
    }
}
