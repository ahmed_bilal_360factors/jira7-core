package com.atlassian.jira.user.util;

import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.manager.recovery.RecoveryModeService;
import com.atlassian.jira.junit.rules.InitMockitoMocks;
import org.hamcrest.Matchers;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class CachingRecoveryModeServiceTest {
    private static final String RECOVERY_USER = "user_name";

    @Rule
    public InitMockitoMocks mockitoMocks = new InitMockitoMocks(this);
    @Mock
    public Directory directory;
    @Mock
    public RecoveryModeService delegate;

    @Test
    public void getDirectoryDelegatesAndIsCachedWhenEnabled() {
        final RecoveryModeService service = createServiceRecoveryEnabled();

        assertThat(service.getRecoveryDirectory(), Matchers.sameInstance(directory));
        assertThat(service.getRecoveryDirectory(), Matchers.sameInstance(directory));

        verify(delegate, times(1)).getRecoveryDirectory();
    }

    @Test(expected = IllegalStateException.class)
    public void getDirectoryThrowsErrorWhenDisabled() {
        createServiceRecoveryDisabled().getRecoveryDirectory();
    }

    @Test
    public void isRecoveryModeOnDelegatesAndIsCachedWhenEnabled() {
        final RecoveryModeService service = createServiceRecoveryEnabled();

        assertThat(service.isRecoveryModeOn(), Matchers.equalTo(true));
        assertThat(service.isRecoveryModeOn(), Matchers.equalTo(true));

        verify(delegate, times(1)).isRecoveryModeOn();
    }

    @Test
    public void isRecoveryModeOnDelegatesAndIsCachedWhenDisabled() {
        final RecoveryModeService disabled = createServiceRecoveryDisabled();

        assertThat(disabled.isRecoveryModeOn(), Matchers.equalTo(false));
        assertThat(disabled.isRecoveryModeOn(), Matchers.equalTo(false));

        verify(delegate, times(1)).isRecoveryModeOn();
    }

    @Test
    public void getRecoveryUserDelegatesAndIsCachedWhenEnabled() {
        final RecoveryModeService service = createServiceRecoveryEnabled();

        assertThat(service.getRecoveryUsername(), Matchers.equalTo(RECOVERY_USER));
        assertThat(service.getRecoveryUsername(), Matchers.equalTo(RECOVERY_USER));

        verify(delegate, times(1)).getRecoveryUsername();
    }

    @Test(expected = IllegalStateException.class)
    public void getRecoveryUserThrowsErrorWhenDisabled() {
        createServiceRecoveryDisabled().getRecoveryUsername();
    }

    private RecoveryModeService createServiceRecoveryEnabled() {
        when(delegate.getRecoveryDirectory()).thenReturn(directory);
        when(delegate.getRecoveryUsername()).thenReturn(RECOVERY_USER);
        when(delegate.isRecoveryModeOn()).thenReturn(true);

        return new CachingRecoveryModeService(delegate);
    }

    private RecoveryModeService createServiceRecoveryDisabled() {
        when(delegate.getRecoveryDirectory()).thenThrow(IllegalStateException.class);
        when(delegate.getRecoveryUsername()).thenThrow(IllegalStateException.class);
        when(delegate.isRecoveryModeOn()).thenReturn(false);

        return new CachingRecoveryModeService(delegate);
    }
}