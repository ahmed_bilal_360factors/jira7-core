package com.atlassian.jira.project.template;

import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.mock.component.MockComponentWorker;
import com.atlassian.plugin.webresource.UrlMode;
import com.atlassian.plugin.webresource.WebResourceUrlProvider;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

public class TestProjectTemplateBuilder {
    @Rule
    public MethodRule initMockito = MockitoJUnit.rule();

    @Mock
    private WebResourceUrlProvider webResourceUrlProvider;
    @Mock
    private ApplicationProperties applicationProperties;

    @Before
    public void setUp() throws Exception {
        mockEncoding();
    }

    @Test
    public void builderAddsDefaultIcons() throws Exception {
        when(webResourceUrlProvider.getBaseUrl(UrlMode.ABSOLUTE)).thenReturn("http://jira.com");

        ProjectTemplate template = newBuilder()
                .weight(20)
                .iconUrl("")
                .backgroundIconUrl("")
                .build();

        assertThat(template.getIconUrl(), equalTo("http://jira.com/images/project-templates/" + ProjectTemplateBuilder.PT_DEFAULT_IMG_NAME));
        assertThat(template.getBackgroundIconUrl(), equalTo("http://jira.com/images/project-templates/" + ProjectTemplateBuilder.PT_DEFAULT_BG_IMG_NAME));
    }

    private void mockEncoding() {
        when(applicationProperties.getEncoding()).thenReturn("UTF-8");
        new MockComponentWorker().addMock(ApplicationProperties.class, applicationProperties).init();
    }

    private ProjectTemplateBuilder newBuilder() {
        return new ProjectTemplateBuilder(webResourceUrlProvider);
    }
}
