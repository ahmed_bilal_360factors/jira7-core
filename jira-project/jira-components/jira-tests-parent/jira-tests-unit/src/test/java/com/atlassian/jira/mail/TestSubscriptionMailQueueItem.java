package com.atlassian.jira.mail;

import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.jira.issue.fields.layout.column.ColumnLayoutItem;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.issue.subscription.FilterSubscription;
import com.atlassian.jira.security.groups.MockGroupManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.UserDetails;
import com.atlassian.jira.user.util.MockUserManager;
import com.atlassian.jira.web.component.ColumnLayoutItemProvider;
import com.atlassian.mail.queue.MailQueue;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.annotation.Nullable;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestSubscriptionMailQueueItem {
    public static final String TEST_GROUP = "Test Group";
    public static final String ACTIVEUSER = "activeuser";
    public static final String DISABLEDUSER = "disableduser";
    @Mock
    private ColumnLayoutItemProvider columnsProvider;

    private TestableSubscriptionMailQueueItem queueItem;

    @Before
    public void setUp() {
        queueItem = new TestableSubscriptionMailQueueItem(columnsProvider);
    }

    @Test
    public void getColumnsReturnsUserColumnsIfThereIsNoSearchRequest() throws Exception {
        final ApplicationUser user = mock(ApplicationUser.class);
        final SearchRequest searchRequest = null;

        final List<ColumnLayoutItem> expectedColumns = someColumns();
        when(columnsProvider.getUserColumns(user)).thenReturn(expectedColumns);

        queueItem.setSearchRequest(searchRequest);
        final List<ColumnLayoutItem> actualColumns = queueItem.getColumns(user);

        assertThat(actualColumns, is(expectedColumns));
    }

    @Test
    public void getColumnsTakesIntoAccountSearchResultWhenReturningTheColumnsIfThereIsASearchResult() throws Exception {
        final ApplicationUser user = mock(ApplicationUser.class);
        final SearchRequest searchRequest = mock(SearchRequest.class);

        final List<ColumnLayoutItem> expectedColumns = someColumns();
        when(columnsProvider.getColumns(user, searchRequest)).thenReturn(expectedColumns);

        queueItem.setSearchRequest(searchRequest);
        final List<ColumnLayoutItem> actualColumns = queueItem.getColumns(user);

        assertThat(actualColumns, is(expectedColumns));
    }

    @Test
    public void sendOnlyToActiveUsers() throws Exception {
        final FilterSubscription sub = mock(FilterSubscription.class);
        when(sub.getGroupName()).thenReturn(TEST_GROUP);

        final MockGroupManager groupManager = new MockGroupManager();
        final MockUserManager userManager = new MockUserManager() {
            @Override
            public Collection<Group> getGroups() {
                return groupManager.getAllGroups();
            }

            @Override
            public Group getGroup(@Nullable final String groupName) {
                return groupManager.getGroup(groupName);
            }
        };

        final Group group = groupManager.createGroup(TEST_GROUP);

        final ApplicationUser activeUser = userManager.createUser(new UserDetails(ACTIVEUSER, "Active User"));
        final ApplicationUser disabledUser = userManager.createUser(new UserDetails(DISABLEDUSER, "Disabled User"));

        ((MockApplicationUser) disabledUser).setActive(false);

        groupManager.addUserToGroup(activeUser, group);
        groupManager.addUserToGroup(disabledUser, group);

        final ArgumentCaptor<ApplicationUser> recipientCaptor = ArgumentCaptor.forClass(ApplicationUser.class);
        final DefaultSubscriptionMailQueueItemFactory mailQueueItemFactory = mock(DefaultSubscriptionMailQueueItemFactory.class);
        when(mailQueueItemFactory.createSelfEvaluatingEmailQueueItem(Matchers.any(), recipientCaptor.capture()))
                .thenReturn(mock(SubscriptionSingleRecepientMailQueueItem.class));

        // done mocking, do the test
        final SubscriptionMailQueueItem item = new SubscriptionMailQueueItem(sub, userManager, groupManager,
                mock(MailQueue.class), mailQueueItemFactory);

        item.send();

        assertThat("email was sent to one user", recipientCaptor.getAllValues().size(), is(1));
        assertThat("user is active", recipientCaptor.getValue().isActive(), is(true));

    }

    private List<ColumnLayoutItem> someColumns() {
        return Arrays.asList(mock(ColumnLayoutItem.class));
    }

    private class TestableSubscriptionMailQueueItem extends SubscriptionSingleRecepientMailQueueItem {
        private SearchRequest searchRequest;
        private ColumnLayoutItemProvider columnsProvider;

        TestableSubscriptionMailQueueItem(final ColumnLayoutItemProvider columnsProvider) {
            super(null, null, null, null, null, null, null);
            this.columnsProvider = columnsProvider;
        }

        private void setSearchRequest(final SearchRequest searchRequest) {
            this.searchRequest = searchRequest;
        }

        @Override
        SearchRequest getSearchRequest() {
            return this.searchRequest;
        }

        @Override
        ColumnLayoutItemProvider getColumnsProvider() {
            return this.columnsProvider;
        }
    }
}
