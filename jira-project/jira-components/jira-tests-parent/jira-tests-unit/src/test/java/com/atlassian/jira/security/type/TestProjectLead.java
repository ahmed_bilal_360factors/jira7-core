package com.atlassian.jira.security.type;

import com.atlassian.jira.issue.security.IssueSecurityLevel;
import com.atlassian.jira.issue.security.IssueSecurityLevelImpl;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.user.MockApplicationUser;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TermQuery;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;

public class TestProjectLead {
    @Rule
    public TestRule rule = MockitoMocksInContainer.forTest(this);

    private static final MockApplicationUser FRED = new MockApplicationUser("fred");
    private static final MockApplicationUser WILMA = new MockApplicationUser("wilma");

    private final MockProject project = new MockProject(12, "ABC");

    private final ProjectLead projectLead = new ProjectLead(null);

    @Test
    public void testGetQueryWithNullProject() throws Exception {
        Query query = projectLead.getQuery(FRED, null, "developers");
        assertNull(query);
    }

    @Test
    public void testGetQueryWithProjectOnly() throws Exception {
        TermQuery query = (TermQuery) projectLead.getQuery(FRED, project, "developers");

        assertThat(query.toString(), equalTo("projid:12"));
        assertThat(query.getTerm().field(), equalTo("projid"));
        assertThat(query.getTerm().text(), equalTo("12"));
    }

    @Test
    public void testGetQueryWithSecurityLevelSearcherNotProjectLead() throws Exception {
        project.setLead(WILMA);
        IssueSecurityLevel securityLevel = new IssueSecurityLevelImpl(10100L, "Blue", "", 20L);

        Query query = projectLead.getQuery(FRED, project, securityLevel, "developers");

        assertNull(query);
    }

    @Test
    public void testGetQueryWithSecurityLevelSearcherIsProjectLead() throws Exception {
        project.setLead(FRED);
        IssueSecurityLevel securityLevel = new IssueSecurityLevelImpl(10100L, "Blue", "", 20L);

        Query query = projectLead.getQuery(FRED, project, securityLevel, "developers");

        assertThat(query.toString(), equalTo("+projid:12 +issue_security_level:10100"));
    }
}
