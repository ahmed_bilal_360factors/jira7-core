package com.atlassian.jira.issue.index.indexers.impl;

import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.IssueFieldConstants;
import com.atlassian.jira.issue.index.DocumentConstants;
import com.atlassian.jira.mock.issue.MockIssue;
import com.atlassian.jira.web.FieldVisibilityManager;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

public class TestProgressIndexer {
    @Rule
    public MockitoRule mockito = MockitoJUnit.rule();

    @Mock
    private FieldVisibilityManager fieldVisibilityManager;
    @Mock
    private ApplicationProperties applicationProperties;

    @Test
    public void testAddIndex() {
        MockIssue issue = new MockIssue();
        issue.setEstimate(new Long(100));
        issue.setTimeSpent(new Long(100));

        when(fieldVisibilityManager.isFieldVisible(IssueFieldConstants.TIMETRACKING, issue)).thenReturn(true);
        when(applicationProperties.getOption(APKeys.JIRA_OPTION_TIMETRACKING)).thenReturn(true);

        ProgressIndexer indexer = new ProgressIndexer(fieldVisibilityManager, applicationProperties);
        Document doc = new Document();


        indexer.addIndex(doc, issue);
        final Field field = doc.getField(DocumentConstants.ISSUE_PROGRESS);
        assertNotNull("should have progress", field);
        assertEquals("50", field.stringValue());
        assertTrue(field.isIndexed());
    }

    @Test
    public void testAddIndexTimeTrackingNotVisible() {
        MockIssue issue = new MockIssue();
        issue.setEstimate(new Long(100));
        issue.setTimeSpent(new Long(100));

        when(fieldVisibilityManager.isFieldVisible(IssueFieldConstants.TIMETRACKING, issue)).thenReturn(false);
        when(applicationProperties.getOption(APKeys.JIRA_OPTION_TIMETRACKING)).thenReturn(true);

        ProgressIndexer indexer = new ProgressIndexer(fieldVisibilityManager, applicationProperties);
        Document doc = new Document();


        indexer.addIndex(doc, issue);
        final Field field = doc.getField(DocumentConstants.ISSUE_PROGRESS);
        assertNotNull("should have progress", field);
        assertEquals("50", field.stringValue());
        assertFalse(field.isIndexed());
    }

    @Test
    public void testAddIndexFailsIfNegativeEstimate() {
        ProgressIndexer indexer = new ProgressIndexer(fieldVisibilityManager, applicationProperties);
        Document doc = new Document();

        MockIssue issue = new MockIssue();
        issue.setEstimate(new Long(-100));
        issue.setTimeSpent(new Long(100));
        indexer.addIndex(doc, issue);
        final Field field = doc.getField(DocumentConstants.ISSUE_PROGRESS);
        assertNull("should not have progress indexed", field);
    }

    @Test
    public void testAddIndexFailsIfNegativeTimeSpent() {
        ProgressIndexer indexer = new ProgressIndexer(fieldVisibilityManager, applicationProperties);
        Document doc = new Document();

        MockIssue issue = new MockIssue();
        issue.setEstimate(new Long(100));
        issue.setTimeSpent(new Long(-100));
        indexer.addIndex(doc, issue);
        final Field field = doc.getField(DocumentConstants.ISSUE_PROGRESS);
        assertNull("should not have progress indexed", field);
    }
}
