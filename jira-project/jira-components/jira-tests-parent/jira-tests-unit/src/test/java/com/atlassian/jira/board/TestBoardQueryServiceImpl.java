package com.atlassian.jira.board;

import com.atlassian.jira.bc.ServiceOutcome;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.jql.builder.JqlQueryBuilder;
import com.atlassian.jira.junit.rules.MockitoContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.MessageSet;
import com.atlassian.jira.util.MessageSetImpl;
import com.atlassian.jira.web.bean.MockI18nBean;
import com.atlassian.query.Query;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static com.atlassian.jira.bc.issue.search.SearchService.ParseResult;
import static com.atlassian.jira.util.ErrorCollection.Reason.VALIDATION_FAILED;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestBoardQueryServiceImpl {
    private static final BoardId BOARD_ID = new BoardId(1L);
    private static final String BOARD_JQL = "some jql";
    private static final Board BOARD = new Board(BOARD_ID, BOARD_JQL);
    private static final ApplicationUser USER = new MockApplicationUser("user");
    private static final String ERROR_MESSAGE = "something screwed up";
    private static final String JQL_PARSE_ERROR_MESSAGE = "There was an error parsing the JQL for this board.";

    @Rule
    public MockitoContainer container = MockitoMocksInContainer.rule(this);

    private I18nHelper.BeanFactory beanFactory = new MockI18nBean.MockI18nBeanFactory();
    @Mock
    private SearchService searchService;

    private BoardQueryServiceImpl boardQueryService;

    @Before
    public void setup() {
        boardQueryService = new BoardQueryServiceImpl(beanFactory, searchService);
    }

    @Test
    public void expectedQueryIsReturnedIfThereIsNoErrorParsingTheJql() {
        boardJqlParsesWithoutErrors();

        ServiceOutcome<Query> queryResult = boardQueryService.getBaseQueryForBoard(USER, BOARD);

        assertThat(queryResult.isValid(), is(true));
        assertThat(queryResult.get().toString(), containsString("{issuetype in standardIssueTypes()}"));
    }

    @Test
    public void expectedErrorIsReturnedIfThereIsAnErrorParsingTheJql() {
        boardJqlParsesWithAnError();

        ServiceOutcome<Query> queryResult = boardQueryService.getBaseQueryForBoard(USER, BOARD);

        assertThat(queryResult.isValid(), is(false));
        assertThat(queryResult.getErrorCollection().getErrorMessages(), contains(JQL_PARSE_ERROR_MESSAGE));
        assertThat(queryResult.getErrorCollection().getReasons(), contains(VALIDATION_FAILED));
    }

    @Test
    public void augmentsAnEmptyQueryWithExpectedConstraints() {
        Query emptyQuery = JqlQueryBuilder.newBuilder().buildQuery();

        Query augmentedQuery = boardQueryService.getAugmentedQueryForDoneIssues(emptyQuery);

        String expectedJqlString = "{statusCategory != \"done\"} OR {statusCategory = \"done\"} AND {status changed during (\"-14d\", now())} order by key DESC";
        assertThat(augmentedQuery.toString(), is(expectedJqlString));
    }

    @Test
    public void augmentsSimpleQueryWithExpectedConstraints() {
        Query simpleQuery = JqlQueryBuilder.newBuilder()
                .where()
                .project()
                .eq("TEST-PROJECT")
                .endWhere()
                .buildQuery();

        Query augmentedQuery = boardQueryService.getAugmentedQueryForDoneIssues(simpleQuery);

        String expectedJqlString = "{project = \"TEST-PROJECT\"} AND ( {statusCategory != \"done\"} OR {statusCategory = \"done\"} AND {status changed during (\"-14d\", now())} ) order by key DESC";
        assertThat(augmentedQuery.toString(), is(expectedJqlString));
    }

    @Test
    public void augmentsGivenQueryCorrectlyEvenIfItHasFieldsInCommonWithTheAdditionalQuery() {
        Query todoQuery = JqlQueryBuilder.newBuilder()
                .where()
                .statusCategory()
                .eq("new")
                .endWhere()
                .buildQuery();

        Query augmentedQuery = boardQueryService.getAugmentedQueryForDoneIssues(todoQuery);

        String expectedJqlString = "{statusCategory = \"new\"} AND ( {statusCategory != \"done\"} OR {statusCategory = \"done\"} AND {status changed during (\"-14d\", now())} ) order by key DESC";
        assertThat(augmentedQuery.toString(), is(expectedJqlString));
    }

    private Query boardJqlParsesWithoutErrors() {
        Query baseBoardQuery = JqlQueryBuilder.newBuilder()
                .where()
                .project()
                .eq("some project")
                .endWhere()
                .buildQuery();
        parseResultIs(new ParseResult(baseBoardQuery, new MessageSetImpl()));

        return baseBoardQuery;
    }

    private void boardJqlParsesWithAnError() {
        MessageSet messageSet = new MessageSetImpl();
        messageSet.addErrorMessage(ERROR_MESSAGE);
        parseResultIs(new ParseResult(null, messageSet));
    }

    private void parseResultIs(ParseResult parseResult) {
        when(searchService.parseQuery(USER, BOARD_JQL)).thenReturn(parseResult);
    }
}
