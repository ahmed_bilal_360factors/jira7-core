package com.atlassian.jira.issue.link;

import com.atlassian.crowd.embedded.api.CrowdService;
import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.crowd.exception.InvalidCredentialException;
import com.atlassian.crowd.exception.InvalidUserException;
import com.atlassian.crowd.exception.OperationNotPermittedException;
import com.atlassian.crowd.exception.embedded.InvalidGroupException;
import com.atlassian.jira.cache.request.MockRequestCacheFactory;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.SubTaskManager;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.database.MockQueryDslAccessor;
import com.atlassian.jira.database.QueryDslAccessor;
import com.atlassian.jira.event.type.EventType;
import com.atlassian.jira.exception.CreateException;
import com.atlassian.jira.exception.RemoveException;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueImpl;
import com.atlassian.jira.issue.history.ChangeItemBean;
import com.atlassian.jira.issue.index.IssueIndexingService;
import com.atlassian.jira.issue.util.IssueUpdateBean;
import com.atlassian.jira.issue.util.IssueUpdater;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoContainer;
import com.atlassian.jira.mock.MockApplicationProperties;
import com.atlassian.jira.mock.MockIssueManager;
import com.atlassian.jira.mock.issue.MockIssue;
import com.atlassian.jira.mock.ofbiz.MockGenericValue;
import com.atlassian.jira.mock.ofbiz.MockOfBizDelegator;
import com.atlassian.jira.ofbiz.FieldMap;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.MockCrowdService;
import com.atlassian.jira.web.FieldVisibilityManager;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.hamcrest.collection.IsEmptyCollection;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.ofbiz.core.entity.GenericEntityException;
import org.ofbiz.core.entity.GenericValue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

import static com.atlassian.core.ofbiz.test.UtilsForTests.getTestEntity;
import static com.atlassian.jira.entity.IssueLinkFactory.DESTINATION;
import static com.atlassian.jira.entity.IssueLinkFactory.LINK_TYPE;
import static com.atlassian.jira.entity.IssueLinkFactory.SEQUENCE;
import static com.atlassian.jira.entity.IssueLinkFactory.SOURCE;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isA;
import static org.mockito.Matchers.isNull;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

public class TestDefaultIssueLinkManager {
    private static final Long SOURCE_ID = 1L;
    private static final Long DESTINATION_ID = 2L;

    @Rule
    public MockitoContainer mockitoContainer = new MockitoContainer(this);

    @Mock
    @AvailableInContainer
    private FieldVisibilityManager visibilityManager;

    @Mock
    @AvailableInContainer
    private IssueLinkTypeManager issueLinkTypeManager;

    @Mock
    @AvailableInContainer
    private PermissionManager permissionManager;

    @Mock
    private IssueIndexingService issueIndexingService;

    @AvailableInContainer
    private OfBizDelegator ofbiz = new MockOfBizDelegator();

    @AvailableInContainer
    private ApplicationProperties applicationProperties = new MockApplicationProperties();

    @AvailableInContainer
    private MockIssueManager issueManager = new MockIssueManager();

    @AvailableInContainer
    CrowdService crowdService = new MockCrowdService();

    private DefaultIssueLinkManager dilm;
    private GenericValue sourceIssue;
    private MockIssue sourceIssueObject;
    private GenericValue destinationIssue;

    @Rule
    public MockRequestCacheFactory requestCacheFactory = MockRequestCacheFactory.rule();

    @Before
    public void setUp() throws Exception {
        applicationProperties.setString(APKeys.JIRA_I18N_LANGUAGE_INPUT, APKeys.Languages.ENGLISH);
        when(visibilityManager.isFieldVisible(anyString(), anyObject())).thenReturn(true);

        sourceIssue = getTestEntity("Issue", FieldMap.build(
                "id", SOURCE_ID,
                "key", "TST-1",
                "summary", "test source summary"));
        sourceIssueObject = new MockIssue(sourceIssue);

        destinationIssue = getTestEntity("Issue", FieldMap.build(
                "id", 2L,
                "key", "TST-2",
                "summary", "test destination summary"));

        issueManager.addIssue(sourceIssue);
        issueManager.addIssue(destinationIssue);
    }

    @After
    public void tearDown() {
        visibilityManager = null;
        issueLinkTypeManager = null;
        permissionManager = null;
        issueIndexingService = null;
        ofbiz = null;
        applicationProperties = null;
        issueManager = null;
        crowdService = null;

        dilm = null;
        sourceIssue = null;
        sourceIssueObject = null;
        destinationIssue = null;
    }


    @Test
    public void testCreateIssueLinkSystemLinkTypeCreatesGenericValue() throws CreateException, GenericEntityException {
        final Long testSourceId = 0L;
        final Long testDestinationId = 1L;
        final Long testSequence = 0L;
        final Long testLinkType = 11L;

        final Map<String, Long> expectedFields = ImmutableMap.of(LINK_TYPE, testLinkType, SOURCE, testSourceId, DESTINATION, testDestinationId, SEQUENCE,
                testSequence);
        final GenericValue issueLinkGV = new MockGenericValue("IssueLink", new HashMap<>(expectedFields));
        final MockOfBizDelegator delegator = new MockOfBizDelegator(null, ImmutableList.of(issueLinkGV));

        final GenericValue issueLinkTypeGV = new MockGenericValue("IssueLinkType", ImmutableMap.of("linkname", "test name", "outward", "test outward",
                "inward", "test inward", "style", "jira_test style"));
        final MockIssueManager issueManager = new MockIssueManager();
        issueManager.addIssue(sourceIssue);
        issueManager.addIssue(destinationIssue);

        when(issueLinkTypeManager.getIssueLinkType(anyLong())).thenReturn(new IssueLinkTypeImpl(issueLinkTypeGV));
        when(issueLinkTypeManager.getIssueLinkType(anyLong(), anyBoolean())).thenReturn(
                new IssueLinkTypeImpl(issueLinkTypeGV));

        final IssueLinkCreator issueLinkCreator = issueLinkGV1 -> new IssueLinkImpl(issueLinkGV1, issueLinkTypeManager, issueManager);

        dilm = new DefaultIssueLinkManager(delegator, null, issueLinkCreator, issueLinkTypeManager, null,
                issueIndexingService,
                applicationProperties,
                requestCacheFactory);
        assertThat(dilm.getOutwardLinks(testSourceId), empty());
        assertThat(dilm.getInwardLinks(testDestinationId), empty());
        dilm.createIssueLink(testSourceId, testDestinationId, testLinkType, testSequence, null);
        delegator.verify();
        assertThat(dilm.getOutwardLinks(testSourceId), contains(issueLink(testSourceId, testDestinationId, testLinkType)));
        assertThat(dilm.getInwardLinks(testDestinationId), contains(issueLink(testSourceId, testDestinationId, testLinkType)));
    }

    @Test
    public void testCreateIssueLinkNonSystemLinkTypeCreateGVsAndChangeItems()
            throws CreateException, GenericEntityException, OperationNotPermittedException, InvalidUserException, InvalidCredentialException {
        final Long testSequence = 0L;
        final Long testLinkType = 11L;

        final GenericValue issueLinkTypeGV = new MockGenericValue("IssueLinkType", ImmutableMap.of("linkname", "test name", "outward", "test outward",
                "inward", "test inward", "style", ""));

        when(issueLinkTypeManager.getIssueLinkType(anyLong())).thenReturn(new IssueLinkTypeImpl(issueLinkTypeGV));
        when(issueLinkTypeManager.getIssueLinkType(anyLong(), anyBoolean())).thenReturn(
                new IssueLinkTypeImpl(issueLinkTypeGV));


        final GenericValue sourceIssue = spy(new MockGenericValue("Issue", ImmutableMap.of("summary", "test source summary", "number", 1L, "id",
                SOURCE_ID)));
        doReturn(Lists.newArrayList()).when(sourceIssue).getRelatedOrderBy(anyString(), anyObject());

        final GenericValue destinationIssue = spy(new MockGenericValue("Issue", ImmutableMap.of("summary", "test destination summary", "number", 2L,
                "id", DESTINATION_ID)));
        doReturn(Lists.newArrayList()).when(sourceIssue).getRelatedOrderBy(anyString(), anyObject());

        final MockIssueManager mockIssueManager = new MockIssueManager();
        mockIssueManager.addIssue(sourceIssue);
        mockIssueManager.addIssue(destinationIssue);

        final IssueLinkCreator issueLinkCreator = issueLinkGV -> new IssueLinkImpl(issueLinkGV, issueLinkTypeManager, mockIssueManager);

        final ApplicationUser testUser = createMockUser("test user");

        final IssueUpdateBean issueUpdateBean1 = new IssueUpdateBean(sourceIssue, sourceIssue, EventType.ISSUE_UPDATED_ID, testUser);
        issueUpdateBean1.setDispatchEvent(false);
        final ChangeItemBean expectedCib = new ChangeItemBean(ChangeItemBean.STATIC_FIELD, "Link", null, null, destinationIssue.getString("key"),
                "This issue " + "test outward" + ' ' + destinationIssue.getString("key"));
        issueUpdateBean1.setChangeItems(ImmutableList.of(expectedCib));

        final IssueUpdateBean issueUpdateBean2 = new IssueUpdateBean(destinationIssue, destinationIssue, EventType.ISSUE_UPDATED_ID, testUser);
        final ChangeItemBean expectedCib2 = new ChangeItemBean(ChangeItemBean.STATIC_FIELD, "Link", null, null, sourceIssue.getString("key"),
                "This issue " + "test inward" + ' ' + sourceIssue.getString("key"));
        issueUpdateBean2.setDispatchEvent(false);
        issueUpdateBean2.setChangeItems(ImmutableList.of(expectedCib2));

        final List<IssueUpdateBean> issueUpdateBeans = new ArrayList<>();
        final IssueUpdater updater = (issueUpdateBean, generateChangeItems) -> issueUpdateBeans.add(issueUpdateBean);

        final GenericValue issueLink = getTestEntity("IssueLink", FieldMap.build(
                "id", 1000L,
                SOURCE, SOURCE_ID,
                DESTINATION, DESTINATION_ID,
                SEQUENCE, 0L,
                LINK_TYPE, 11L));
        final MockOfBizDelegator delegator = new MockOfBizDelegator(null, ImmutableList.of(issueLink));

        dilm = new DefaultIssueLinkManager(delegator, null, issueLinkCreator, issueLinkTypeManager, updater,
                issueIndexingService, applicationProperties, requestCacheFactory);
        dilm.createIssueLink(SOURCE_ID, DESTINATION_ID, testLinkType, testSequence, testUser);

        delegator.verify();

        assertThat(issueUpdateBeans, containsInAnyOrder(issueUpdateBean1, issueUpdateBean2));
    }

    @Test
    public void testRemoveIssueLinkSystemLinkType() throws RemoveException, GenericEntityException
    {
        final GenericValue issueLinkTypeGV = getTestEntity("IssueLinkType",
                ImmutableMap.of("outward", "test out", "inward",
                        "test inward", "linkname", "test name", "style", "jira_some system style"));
        final IssueLinkType issueLinkType = new IssueLinkTypeImpl(issueLinkTypeGV);
        final Long linkTypeId = issueLinkType.getId();

        when(issueLinkTypeManager.getIssueLinkType(eq(linkTypeId))).thenReturn(issueLinkType);

        setupManager(issueLinkTypeManager, null);

        final GenericValue issueLinkGV = getTestEntity("IssueLink", ImmutableMap.of(SOURCE, 0L, DESTINATION, 1L,
                LINK_TYPE, linkTypeId));
        final IssueLink issueLink = new IssueLinkImpl(issueLinkGV, issueLinkTypeManager, null);
        dilm.removeIssueLink(issueLink, null);

        final List<GenericValue> issueLinkGVs = ofbiz.findAll("IssueLink");
        assertThat(issueLinkGVs, IsEmptyCollection.<GenericValue>empty());

    }

    @Test
    public void testRemoveIssueLinkNonSystemLinkType()
            throws RemoveException, GenericEntityException, OperationNotPermittedException, InvalidUserException, InvalidCredentialException {
        final ApplicationUser testUser = createMockUser("test user");
        final GenericValue issueLinkTypeGV = getTestEntity("IssueLinkType", FieldMap.build(
                "linkname", "test name",
                "outward", "test outward",
                "inward", "test inward"));
        final IssueLinkType issueLinkType = new IssueLinkTypeImpl(issueLinkTypeGV);

        when(issueLinkTypeManager.getIssueLinkType(eq(issueLinkType.getId()))).thenReturn(issueLinkType);

        final MyIssueUpdater issueUpdater = new MyIssueUpdater() {
            int called = 0;
            int expectedCalled = 2;

            @Override
            public void doUpdate(final IssueUpdateBean issueUpdateBean, final boolean generateChangeItems) {
                if (called == 0) {
                    assertFalse("issueUpdateBean#isDispatchEvent() should be false", issueUpdateBean.isDispatchEvent());
                    assertThat(sourceIssue, is(issueUpdateBean.getOriginalIssue()));
                    assertThat(sourceIssue, is(issueUpdateBean.getChangedIssue()));
                    assertThat(testUser, is(issueUpdateBean.getApplicationUser()));
                    assertThat(EventType.ISSUE_UPDATED_ID, is(issueUpdateBean.getEventTypeId()));
                    final ChangeItemBean expectedCib = new ChangeItemBean(ChangeItemBean.STATIC_FIELD, "Link", destinationIssue.getString("key"),
                            "This issue " + issueLinkType.getOutward() + ' ' + destinationIssue.getString("key"), null, null);
                    assertThat(ImmutableList.of(expectedCib), hasItems(issueUpdateBean.getChangeItems().toArray(new ChangeItemBean[]{})));
                    called++;
                } else if (called == 1) {
                    assertFalse("issueUpdateBean#isDispatchEvent() should be false", issueUpdateBean.isDispatchEvent());
                    assertThat(destinationIssue, is(issueUpdateBean.getOriginalIssue()));
                    assertThat(destinationIssue, is(issueUpdateBean.getChangedIssue()));
                    assertThat(testUser, is(issueUpdateBean.getApplicationUser()));
                    assertThat(EventType.ISSUE_UPDATED_ID, is(issueUpdateBean.getEventTypeId()));
                    final ChangeItemBean expectedCib = new ChangeItemBean(ChangeItemBean.STATIC_FIELD, "Link", sourceIssue.getString("key"),
                            "This issue " + issueLinkType.getInward() + ' ' + sourceIssue.getString("key"), null, null);
                    assertThat(ImmutableList.of(expectedCib), hasItems(issueUpdateBean.getChangeItems().toArray(new ChangeItemBean[]{})));
                    called++;
                } else {
                    fail("doUpdate called " + ++called + " times.");
                }
            }

            @Override
            public void verify() {
                if (called != expectedCalled) {
                    fail("doUpdate was called '" + called + " times instead of " + expectedCalled + '.');
                }
            }
        };

        final GenericValue issueLinkGV = getTestEntity("IssueLink", FieldMap.build(
                SOURCE, sourceIssue.getLong("id"),
                DESTINATION, destinationIssue.getLong("id"),
                LINK_TYPE, issueLinkType.getId()));
        final IssueLink issueLink = new IssueLinkImpl(issueLinkGV, issueLinkTypeManager, issueManager);

        setupManager(issueLinkTypeManager, issueUpdater);

        assertThat(dilm.getOutwardLinks(sourceIssue.getLong("id")), contains(issueLink));
        assertThat(dilm.getInwardLinks(destinationIssue.getLong("id")), contains(issueLink));

        dilm.removeIssueLink(issueLink, testUser);

        assertThat(dilm.getOutwardLinks(sourceIssue.getLong("id")), empty());
        assertThat(dilm.getInwardLinks(destinationIssue.getLong("id")), empty());

        issueUpdater.verify();
    }

    @Test
    public void testRemoveIssueLinks() throws RemoveException, GenericEntityException {
        // Setup system link type
        final GenericValue issueLinkTypeGV = getTestEntity("IssueLinkType", FieldMap.build(
                "outward", "test out",
                "inward", "test inward",
                "linkname", "test name",
                "style", "jira_some system style"));
        final IssueLinkType issueLinkType = new IssueLinkTypeImpl(issueLinkTypeGV);

        when(issueLinkTypeManager.getIssueLinkType(eq(issueLinkType.getId()))).thenReturn(issueLinkType);

        setupManager(issueLinkTypeManager, null);

        // Create issue links - one with the issue as source the other as destination
        getTestEntity("IssueLink", FieldMap.build(
                SOURCE, sourceIssue.getLong("id"),
                DESTINATION, 999L,
                LINK_TYPE, issueLinkType.getId()));
        getTestEntity("IssueLink", FieldMap.build(
                SOURCE, 7654L,
                DESTINATION, sourceIssue.getLong("id"),
                LINK_TYPE, issueLinkType.getId()));

        final Issue issue = new IssueImpl(sourceIssue, issueManager, null, null, null, null, null, null,
                null, null, null, null);
        dilm.removeIssueLinks(issue, null);

        final List<GenericValue> issueLinkGVs = ofbiz.findAll("IssueLink");
        assertThat(issueLinkGVs, IsEmptyCollection.<GenericValue>empty());

    }

    @Test
    public void testGetOutwardLinks() throws GenericEntityException {
        final List<IssueLinkType> issueLinkTypes = setupIssueLinkTypes(Optional.empty());

        final List<IssueLink> expectedLinks = setupLinks(issueLinkTypes, false, sourceIssue, destinationIssue);
        mockLinkTypes(issueLinkTypes);

        setupManager(null, null);
        final List<IssueLink> outwardLinks = dilm.getOutwardLinks(sourceIssue.getLong("id"));

        assertThat(outwardLinks, containsInAnyOrder(expectedLinks.get(0), expectedLinks.get(1)));
    }

    @Test
    public void testGetInwardLinks() throws GenericEntityException {
        final List<IssueLinkType> issueLinkTypes = setupIssueLinkTypes(Optional.empty());
        final List<IssueLink> expectedLinks = setupLinks(issueLinkTypes, false, sourceIssue, destinationIssue);
        mockLinkTypes(issueLinkTypes);

        setupManager(null, null);
        final List<IssueLink> inwardLinks = dilm.getInwardLinks(destinationIssue.getLong("id"));

        assertThat(inwardLinks, containsInAnyOrder(expectedLinks.get(0), expectedLinks.get(1)));
    }

    @Test
    public void testGetInwardLinksNullId() throws GenericEntityException {
        setupManager(null, null);
        final List<IssueLink> inwardLinks = dilm.getInwardLinks(null);
        assertThat(inwardLinks, hasSize(0));
    }

    @Test
    public void testGetOutwardLinksNullId() throws GenericEntityException {
        setupManager(null, null);
        final List<IssueLink> outwardLinks = dilm.getOutwardLinks(null);
        assertThat(outwardLinks, hasSize(0));
    }

    @Test
    public void testGetLinkCollection() throws GenericEntityException {
        when(permissionManager.hasPermission(eq(Permissions.BROWSE), isA(Issue.class), (ApplicationUser) isNull())).thenReturn(Boolean.TRUE);

        final List<IssueLinkType> expectedIssueLinkTypes = setupIssueLinkTypes(Optional.empty());
        setupLinks(expectedIssueLinkTypes, true, sourceIssue, destinationIssue);

        mockLinkTypes(expectedIssueLinkTypes);

        setupManager(issueLinkTypeManager, null);

        final LinkCollection linkCollection = dilm.getLinkCollection(sourceIssueObject, null, true);

        final Set<IssueLinkType> resultLinkTypes = linkCollection.getLinkTypes();
        assertThat(resultLinkTypes, hasSize(2));

        // Iterate over the expected link types
        for (final Iterator<IssueLinkType> iterator = expectedIssueLinkTypes.iterator(); iterator.hasNext(); ) {
            final IssueLinkType issueLinkType = iterator.next();

            // Test Outward issues
            final List<Issue> resultOutwardIssues = linkCollection.getOutwardIssues(issueLinkType.getName());
            assertThat(resultOutwardIssues, contains(issueId(DESTINATION_ID)));

            final List<Issue> resultInwardIssues = linkCollection.getInwardIssues(issueLinkType.getName());
            assertThat(resultInwardIssues, contains(issueId(DESTINATION_ID)));
        }

    }

    @Test
    public void testGetLinkCollectionLinksParentEagerlyForSubtasks() throws GenericEntityException {
        when(permissionManager.hasPermission(eq(Permissions.BROWSE), isA(Issue.class), (ApplicationUser) isNull())).thenReturn(Boolean.TRUE);

        final List<IssueLinkType> expectedIssueLinkTypes =
                setupIssueLinkTypes(Optional.of(SubTaskManager.SUB_TASK_ISSUE_TYPE_STYLE));
        setupLinks(expectedIssueLinkTypes, true, sourceIssue, destinationIssue);

        mockLinkTypes(expectedIssueLinkTypes);

        setupManager(issueLinkTypeManager, null);

        final LinkCollection linkCollection = dilm.getLinkCollection(sourceIssueObject, null, false);

        final Set<IssueLinkType> resultLinkTypes = linkCollection.getLinkTypes();
        assertThat(resultLinkTypes, hasSize(2));

        // Iterate over the expected link types
        for (final Iterator<IssueLinkType> iterator = expectedIssueLinkTypes.iterator(); iterator.hasNext(); ) {
            final IssueLinkType issueLinkType = iterator.next();

            // Test Outward issues
            final List<Issue> resultOutwardIssues = linkCollection.getOutwardIssues(issueLinkType.getName());
            assertThat(resultOutwardIssues, contains(issueId(DESTINATION_ID)));
            if (issueLinkType.isSubTaskLinkType()) {
                for (Issue destinationIssue: resultOutwardIssues) {
                    assertThat(destinationIssue.getParentObject(), is(sourceIssueObject));
                }
            }

            final List<Issue> resultInwardIssues = linkCollection.getInwardIssues(issueLinkType.getName());
            assertThat(resultInwardIssues, contains(issueId(DESTINATION_ID)));
        }

    }

    @Test
    public void testGetLinkCollectionIssueObject() throws GenericEntityException {
        when(permissionManager.hasPermission(eq(Permissions.BROWSE), isA(Issue.class), (ApplicationUser) isNull())).thenReturn(Boolean.TRUE);

        final List<IssueLinkType> expectedIssueLinkTypes = setupIssueLinkTypes(Optional.empty());
        setupLinks(expectedIssueLinkTypes, true, sourceIssue, destinationIssue);

        mockLinkTypes(expectedIssueLinkTypes);

        setupManager(issueLinkTypeManager, null);

        final LinkCollection linkCollection = dilm.getLinkCollection(sourceIssueObject, null);

        final Set<IssueLinkType> resultLinkTypes = linkCollection.getLinkTypes();
        assertThat(resultLinkTypes, hasSize(2));

        // Iterate over the expected link types
        for (final Iterator<IssueLinkType> iterator = expectedIssueLinkTypes.iterator(); iterator.hasNext(); ) {
            final IssueLinkType issueLinkType = iterator.next();
            assertThat(resultLinkTypes, hasItems(issueLinkType));

            // Test Outward issues
            final List<Issue> resultOutwardIssues = linkCollection.getOutwardIssues(issueLinkType.getName());
            assertThat(resultOutwardIssues, contains(issueId(DESTINATION_ID)));

            // Test Inward Links
            final List<Issue> resultInwardIssues = linkCollection.getInwardIssues(issueLinkType.getName());
            assertThat(resultInwardIssues, contains(issueId(DESTINATION_ID)));
        }
    }

    @Test
    public void testGetLinkCollectionIssueObjectOverrideSecurity() throws GenericEntityException {
        when(permissionManager.hasPermission(eq(Permissions.BROWSE), isA(Issue.class), (ApplicationUser) isNull())).thenReturn(Boolean.TRUE);

        final List<IssueLinkType> expectedIssueLinkTypes = setupIssueLinkTypes(Optional.empty());
        setupLinks(expectedIssueLinkTypes, true, sourceIssue, destinationIssue);

        mockLinkTypes(expectedIssueLinkTypes);

        setupManager(issueLinkTypeManager, null);

        final LinkCollection linkCollection = dilm.getLinkCollectionOverrideSecurity(sourceIssueObject);

        final Set<IssueLinkType> resultLinkTypes = linkCollection.getLinkTypes();
        assertThat(resultLinkTypes, hasSize(2));

        // Iterate over the expected link types
        for (final Iterator<IssueLinkType> iterator = expectedIssueLinkTypes.iterator(); iterator.hasNext(); ) {
            final IssueLinkType issueLinkType = iterator.next();
            assertThat(resultLinkTypes, hasItems(issueLinkType));

            // Test Outward issues
            final List<Issue> resultOutwardIssues = linkCollection.getOutwardIssues(issueLinkType.getName());
            assertThat(resultOutwardIssues, contains(issueId(DESTINATION_ID)));

            // Test Inward Links
            final List<Issue> resultInwardIssues = linkCollection.getInwardIssues(issueLinkType.getName());
            assertThat(resultInwardIssues, contains(issueId(DESTINATION_ID)));
        }
    }

    @Test
    public void testMoveIssueLink() throws GenericEntityException {
        final List<IssueLink> issueLinks = setupIssueLinkSequence();

        // Sanity check on the issue links before update
        assertThat(issueLinks.get(0).getId(), is(1001L));
        assertThat(issueLinks.get(0).getSequence(), is(0L));

        final MockQueryDslAccessor mockQueryDslAccessor = new MockQueryDslAccessor();
        mockQueryDslAccessor.setUpdateResults(
                "update issuelink\n"
                        + "set sequence = 0\n"
                        + "where issuelink.id = 1002", 1);
        mockQueryDslAccessor.setUpdateResults(
                "update issuelink\n"
                        + "set sequence = 1\n"
                        + "where issuelink.id = 1003", 1);
        mockQueryDslAccessor.setUpdateResults(
                "update issuelink\n"
                        + "set sequence = 2\n"
                        + "where issuelink.id = 1001", 1);

        setupManager(mockQueryDslAccessor);
        // this asks to move the issue link at seq 0 (ID 1001) to seq 2
        dilm.moveIssueLink(issueLinks, 0L, 2L);

        // assert that all expected update statements actually were run
        mockQueryDslAccessor.assertAllExpectedStatementsWereRun();
    }

    @Test
    public void testResetSequences() throws GenericEntityException {
        final List<IssueLink> issueLinks = setupIssueLinkSequence();

        // Sanity check on the issue links list
        assertThat(issueLinks.get(0).getId(), is(1001L));
        assertEquals(new Long(1002), issueLinks.get(1).getId());
        assertEquals(new Long(1003), issueLinks.get(2).getId());

        final MockQueryDslAccessor mockQueryDslAccessor = new MockQueryDslAccessor();
        mockQueryDslAccessor.setUpdateResults(
                "update issuelink\n"
                        + "set sequence = 0\n"
                        + "where issuelink.id = 1001", 1);
        mockQueryDslAccessor.setUpdateResults(
                "update issuelink\n"
                        + "set sequence = 1\n"
                        + "where issuelink.id = 1002", 1);
        mockQueryDslAccessor.setUpdateResults(
                "update issuelink\n"
                        + "set sequence = 2\n"
                        + "where issuelink.id = 1003", 1);

        setupManager(mockQueryDslAccessor);
        dilm.resetSequences(issueLinks);

        // assert all updates were run
        mockQueryDslAccessor.assertAllExpectedStatementsWereRun();
    }

    @Test
    public void testGetIssueLink() {
        final List<IssueLink> expectedIssueLinks = setupIssueLinkSequence();
        setupManager(null, null);

        final IssueLink expectedIssueLink = expectedIssueLinks.get(0);
        final IssueLink result = dilm.getIssueLink(expectedIssueLink.getSourceId(), expectedIssueLink.getDestinationId(),
                expectedIssueLink.getLinkTypeId());

        // As we do not care about which actual link we get as long as the source, destination and link type id are the same just test them
        assertThat(SOURCE, result.getSourceId(), is(expectedIssueLink.getSourceId()));
        assertThat(DESTINATION, result.getDestinationId(), is(expectedIssueLink.getDestinationId()));
        assertThat(LINK_TYPE, result.getLinkTypeId(), is(expectedIssueLink.getLinkTypeId()));
    }

    @Test
    public void testChangeIssueLinkTypeNonSystemLink()
            throws RemoveException, GenericEntityException, OperationNotPermittedException, InvalidUserException, InvalidCredentialException {
        final ApplicationUser testUser = createMockUser("test use");

        final List<IssueLinkType> issueLinkTypes = setupIssueLinkTypes(Optional.empty());
        final IssueLinkType issueLinkType = issueLinkTypes.get(0);

        when(issueLinkTypeManager.getIssueLinkType(eq(issueLinkType.getId()))).thenReturn(issueLinkType);

        final GenericValue issueLinkGV = getTestEntity("IssueLink",
                ImmutableMap.of(SOURCE, sourceIssue.getLong("id"), DESTINATION,
                        destinationIssue.getLong("id"), LINK_TYPE, issueLinkType.getId()));

        final IssueLink issueLink = new IssueLinkImpl(issueLinkGV, issueLinkTypeManager, issueManager);
        final IssueLinkType swapIssueLinkType = issueLinkTypes.get(1);

        final MyIssueUpdater issueUpdater = new MyIssueUpdater() {
            int called = 0;
            int expectedCalled = 4;

            @Override
            public void doUpdate(final IssueUpdateBean issueUpdateBean, final boolean generateChangeItems) {
                if (called == 0) {
                    assertFalse("issueUpdateBean#isDispatchEvent() should be false", issueUpdateBean.isDispatchEvent());
                    assertThat(sourceIssue, is(issueUpdateBean.getOriginalIssue()));
                    assertThat(sourceIssue, is(issueUpdateBean.getChangedIssue()));
                    assertThat(testUser, is(issueUpdateBean.getApplicationUser()));
                    assertThat(EventType.ISSUE_UPDATED_ID, is(issueUpdateBean.getEventTypeId()));
                    final ChangeItemBean expectedCib = new ChangeItemBean(ChangeItemBean.STATIC_FIELD, "Link", destinationIssue.getString("key"),
                            "This issue " + issueLinkType.getOutward() + ' ' + destinationIssue.getString("key"), null, null);
                    assertThat(ImmutableList.of(expectedCib), hasItems(issueUpdateBean.getChangeItems().toArray(new ChangeItemBean[]{})));
                    called++;
                } else if (called == 1) {
                    assertFalse("issueUpdateBean#isDispatchEvent() should be false", issueUpdateBean.isDispatchEvent());
                    assertThat(destinationIssue, is(issueUpdateBean.getOriginalIssue()));
                    assertThat(destinationIssue, is(issueUpdateBean.getChangedIssue()));
                    assertThat(testUser, is(issueUpdateBean.getApplicationUser()));
                    assertThat(EventType.ISSUE_UPDATED_ID, is(issueUpdateBean.getEventTypeId()));
                    final ChangeItemBean expectedCib = new ChangeItemBean(ChangeItemBean.STATIC_FIELD, "Link", sourceIssue.getString("key"),
                            "This issue " + issueLinkType.getInward() + ' ' + sourceIssue.getString("key"), null, null);
                    assertThat(ImmutableList.of(expectedCib), hasItems(issueUpdateBean.getChangeItems().toArray(new ChangeItemBean[]{})));
                    called++;
                } else if (called == 2) {
                    assertFalse("issueUpdateBean#isDispatchEvent() should be false", issueUpdateBean.isDispatchEvent());
                    assertThat(sourceIssue, is(issueUpdateBean.getOriginalIssue()));
                    assertThat(sourceIssue, is(issueUpdateBean.getChangedIssue()));
                    assertThat(testUser, is(issueUpdateBean.getApplicationUser()));
                    assertThat(EventType.ISSUE_UPDATED_ID, is(issueUpdateBean.getEventTypeId()));
                    final ChangeItemBean expectedCib = new ChangeItemBean(ChangeItemBean.STATIC_FIELD, "Link", null, null,
                            destinationIssue.getString("key"), "This issue " + swapIssueLinkType.getOutward() + ' ' + destinationIssue.getString("key"));
                    assertThat(ImmutableList.of(expectedCib), hasItems(issueUpdateBean.getChangeItems().toArray(new ChangeItemBean[]{})));
                    called++;
                } else if (called == 3) {
                    assertFalse("issueUpdateBean#isDispatchEvent() should be false", issueUpdateBean.isDispatchEvent());
                    assertThat(destinationIssue, is(issueUpdateBean.getOriginalIssue()));
                    assertThat(destinationIssue, is(issueUpdateBean.getChangedIssue()));
                    assertThat(testUser, is(issueUpdateBean.getApplicationUser()));
                    assertThat(EventType.ISSUE_UPDATED_ID, is(issueUpdateBean.getEventTypeId()));
                    final ChangeItemBean expectedCib = new ChangeItemBean(ChangeItemBean.STATIC_FIELD, "Link", null, null,
                            sourceIssue.getString("key"), "This issue " + swapIssueLinkType.getInward() + ' ' + sourceIssue.getString("key"));
                    assertThat(ImmutableList.of(expectedCib), hasItems(issueUpdateBean.getChangeItems().toArray(new ChangeItemBean[]{})));
                    called++;
                } else {
                    fail("doUpdate called " + ++called + " times.");
                }
            }

            @Override
            public void verify() {
                if (called != expectedCalled) {
                    fail("doUpdate was called '" + called + " times instead of " + expectedCalled + '.');
                }
            }
        };

        MockQueryDslAccessor queryDslAccessor = new MockQueryDslAccessor();
        queryDslAccessor.setUpdateResults(
                "update issuelink\n"
                        + "set linktype = 1001\n"
                        + "where issuelink.id = 1002",
                1
        );

        dilm = new DefaultIssueLinkManager(ofbiz, queryDslAccessor, null, null, issueUpdater, issueIndexingService,
                applicationProperties, requestCacheFactory);
        dilm.changeIssueLinkType(issueLink, swapIssueLinkType, testUser);

        // assert pre-conditions on old and new link type
        assertEquals(new Long(1000), issueLink.getLinkTypeId());
        assertThat(swapIssueLinkType.getId(), is(1001L));
        // assert that the UPDATE statement was actually run:
        queryDslAccessor.assertAllExpectedStatementsWereRun();

        issueUpdater.verify();
    }

    @Test
    public void testChangeIssueLinkTypeSystemLink() throws Exception {
        final List<IssueLinkType> issueLinkTypes = setupIssueLinkTypes(Optional.of("jira_some system style"));
        final IssueLinkType issueLinkType = issueLinkTypes.get(0);

        when(issueLinkTypeManager.getIssueLinkType(eq(issueLinkType.getId()))).thenReturn(issueLinkType);

        final GenericValue issueLinkGV = getTestEntity("IssueLink",
                ImmutableMap.of(SOURCE, sourceIssue.getLong("id"), DESTINATION,
                        destinationIssue.getLong("id"), LINK_TYPE, issueLinkType.getId()));
        final IssueLink issueLink = new IssueLinkImpl(issueLinkGV, issueLinkTypeManager, issueManager);
        final IssueLinkType swapIssueLinkType = issueLinkTypes.get(1);

        MockQueryDslAccessor queryDslAccessor = new MockQueryDslAccessor();
        queryDslAccessor.setUpdateResults(
                "update issuelink\n"
                        + "set linktype = 1001\n"
                        + "where issuelink.id = 1002",
                1
        );

        dilm = new DefaultIssueLinkManager(ofbiz, queryDslAccessor, null, null, null, issueIndexingService,
                applicationProperties, requestCacheFactory);
        dilm.changeIssueLinkType(issueLink, swapIssueLinkType, new MockApplicationUser("admin"));

        // assert pre-conditions on old and new link type
        assertEquals(new Long(1000), issueLink.getLinkTypeId());
        assertThat(swapIssueLinkType.getId(), is(1001L));
        // assert that the UPDATE statement was actually run:
        queryDslAccessor.assertAllExpectedStatementsWereRun();
    }

    private List<IssueLinkType> setupIssueLinkTypes(final Optional<String> style) throws GenericEntityException {
        // Setup system link type
        final GenericValue issueLinkTypeGV1 = getTestEntity("IssueLinkType",
                ImmutableMap.of("outward", "test outward", "inward",
                        "test inward", "linkname", "test name"));
        final GenericValue issueLinkTypeGV2 = getTestEntity("IssueLinkType",
                ImmutableMap.of("outward", "out test", "inward", "in test",
                        "linkname", "another test name"));

        if (style.isPresent()) {
            issueLinkTypeGV1.set("style", style.get());
            issueLinkTypeGV1.store();
            issueLinkTypeGV2.set("style", style.get());
            issueLinkTypeGV2.store();
        }

        final List<IssueLinkType> issueLinkTypes = new ArrayList<>();
        issueLinkTypes.add(new IssueLinkTypeImpl(issueLinkTypeGV1));
        issueLinkTypes.add(new IssueLinkTypeImpl(issueLinkTypeGV2));
        return issueLinkTypes;
    }

    private void mockLinkTypes(final List<IssueLinkType> linkTypes) {
        when(issueLinkTypeManager.getIssueLinkType(anyLong(), anyBoolean())).thenAnswer(invocation -> {
            final Long id = (Long)invocation.getArguments()[0];
            for (final Iterator<IssueLinkType> iterator = linkTypes.iterator(); iterator.hasNext(); ) {
                final IssueLinkType issueLinkType = iterator.next();
                if (issueLinkType.getId().equals(id)) {
                    return issueLinkType;
                }
            }
            fail("Invalid sourceIssue link type id '" + id + "'.");
            return null;
        });
    }

    private void setupManager(final IssueLinkTypeManager issueLinkTypeManager, final IssueUpdater issueUpdater) {
        dilm = new DefaultIssueLinkManager(ofbiz, null, new DefaultIssueLinkCreator(issueLinkTypeManager, issueManager), issueLinkTypeManager,
                issueUpdater, issueIndexingService, applicationProperties, requestCacheFactory) {
            @Override
            protected void reindexLinkedIssues(final IssueLink issueLink) {
            }
        };
    }

    private void setupManager(final QueryDslAccessor queryDslAccessor) {
        dilm = new DefaultIssueLinkManager(ofbiz, queryDslAccessor, new DefaultIssueLinkCreator(null, issueManager), null,
                null, issueIndexingService, applicationProperties, requestCacheFactory);
    }


    private List<IssueLink> setupIssueLinkSequence() {
        final GenericValue issueLinkTypeGV = getTestEntity("IssueLinkType",
                ImmutableMap.of("outward", "test outward", "inward",
                        "test inward", "linkname", "test name"));

        final IssueLinkType issueLinkType = new IssueLinkTypeImpl(issueLinkTypeGV);

        final GenericValue issueLinkGV1 = getTestEntity("IssueLink", ImmutableMap.of(
                SOURCE, SOURCE_ID,
                DESTINATION, DESTINATION_ID,
                LINK_TYPE, issueLinkType.getId(),
                SEQUENCE, 0L));
        final GenericValue issueLinkGV2 = getTestEntity("IssueLink", ImmutableMap.of(
                SOURCE, SOURCE_ID,
                DESTINATION, DESTINATION_ID,
                LINK_TYPE, issueLinkType.getId(),
                SEQUENCE, 1L));
        final GenericValue issueLinkGV3 = getTestEntity("IssueLink", ImmutableMap.of(
                SOURCE, SOURCE_ID,
                DESTINATION, DESTINATION_ID,
                LINK_TYPE, issueLinkType.getId(),
                SEQUENCE, 2L));

        final List<IssueLink> issueLinks = new ArrayList<>(3);
        issueLinks.add(new IssueLinkImpl(issueLinkGV1, null, null));
        issueLinks.add(new IssueLinkImpl(issueLinkGV2, null, null));
        issueLinks.add(new IssueLinkImpl(issueLinkGV3, null, null));
        return issueLinks;
    }

    private List<IssueLink> setupLinks(final List<IssueLinkType> linkTypes, final boolean addReciprocalLink,
            final GenericValue sourceIssue, final GenericValue destinationIssue) {
        // Create a link from sourceIssue to destinationIssue for every passed issue link type
        final List<IssueLink> issueLinks = new ArrayList<>(linkTypes.size());
        for (final Iterator<IssueLinkType> iterator = linkTypes.iterator(); iterator.hasNext();) {
            final IssueLinkType issueLinkType = iterator.next();
            final GenericValue issueLinkGV = getTestEntity("IssueLink", FieldMap.build(
                    SOURCE, sourceIssue.getLong("id"),
                    DESTINATION, destinationIssue.getLong("id"),
                    LINK_TYPE, issueLinkType.getId()));
            issueLinks.add(new IssueLinkImpl(issueLinkGV, issueLinkTypeManager, issueManager));

            if (addReciprocalLink) {
                final GenericValue issueLinkGVBack = getTestEntity("IssueLink", ImmutableMap.of(
                        SOURCE, destinationIssue.getLong("id"),
                        DESTINATION, sourceIssue.getLong("id"),
                        LINK_TYPE, issueLinkType.getId()));
                issueLinks.add(new IssueLinkImpl(issueLinkGVBack, issueLinkTypeManager, issueManager));
            }
        }

        return issueLinks;
    }

    protected ApplicationUser createMockUser(final String userName, final String name, final String email)
            throws OperationNotPermittedException, InvalidUserException, InvalidCredentialException {
        final ApplicationUser user = new MockApplicationUser(userName, name, email);
        final CrowdService crowdService = ComponentAccessor.getCrowdService();
        crowdService.addUser(user.getDirectoryUser(), "password");
        return user;
    }

    protected ApplicationUser createMockUser(final String userName)
            throws OperationNotPermittedException, InvalidUserException, InvalidCredentialException {
        return createMockUser(userName, userName, "");
    }

    protected void addUserToGroup(final ApplicationUser user, final Group group)
            throws OperationNotPermittedException, InvalidGroupException {
        final CrowdService crowdService = ComponentAccessor.getCrowdService();
        crowdService.addUserToGroup(user.getDirectoryUser(), group);
    }

    static Matcher<Issue> issueId(Long expectedId) {
        return new IssueWithId(expectedId);
    }

    private static class IssueWithId extends TypeSafeMatcher<Issue> {
        private final Long expectedId;

        public IssueWithId(Long expectedId) {
            this.expectedId = expectedId;
        }

        @Override
        protected boolean matchesSafely(Issue issue) {
            return Objects.equals(expectedId, issue.getId());
        }

        @Override
        public void describeTo(Description description) {
            description.appendText("issue[id=" + expectedId + ']');
        }
    }

    private static Matcher<IssueLink> issueLink(final Long sourceId, final Long destinationId, final Long linkType) {
        return new TypeSafeMatcher<IssueLink>() {
            @Override
            protected boolean matchesSafely(final IssueLink item) {
                return sourceId.equals(item.getSourceId())
                        && destinationId.equals(item.getDestinationId())
                        && linkType.equals(item.getLinkTypeId());
            }

            @Override
            public void describeTo(final Description description) {
                description.appendText("IssueLink with: ")
                        .appendText("sourceId: ").appendValue(sourceId)
                        .appendText(", destinationId: ").appendValue(destinationId)
                        .appendText(", linkType: ").appendValue(linkType);
            }
        };
    }

    static abstract class MyIssueUpdater implements IssueUpdater {
        abstract public void verify();
    }
}

