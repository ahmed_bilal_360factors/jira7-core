package com.atlassian.jira.issue.search.searchers.util;

import com.atlassian.jira.JiraDataTypes;
import com.atlassian.jira.issue.search.constants.SimpleFieldSearchConstants;
import com.atlassian.jira.jql.operand.JqlOperandResolver;
import com.atlassian.jira.jql.operand.QueryLiteral;
import com.atlassian.jira.jql.validator.MockJqlOperandResolver;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.query.clause.AndClause;
import com.atlassian.query.clause.Clause;
import com.atlassian.query.clause.TerminalClause;
import com.atlassian.query.clause.TerminalClauseImpl;
import com.atlassian.query.operand.EmptyOperand;
import com.atlassian.query.operand.MultiValueOperand;
import com.atlassian.query.operand.Operand;
import com.atlassian.query.operand.SingleValueOperand;
import com.atlassian.query.operator.Operator;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.atlassian.query.operator.Operator.LESS_THAN_EQUALS;
import static java.util.Collections.singletonList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestDefaultWorkRatioSearcherInputHelper {
    @Rule
    public TestRule container = MockitoMocksInContainer.forTest(this);

    private static final ApplicationUser USER = new MockApplicationUser("test");

    @Test
    public void testNullQuery() throws Exception {
        SimpleFieldSearchConstants constants = new SimpleFieldSearchConstants("test", Collections.<Operator>emptySet(), JiraDataTypes.NUMBER);
        JqlOperandResolver operandResolver = mock(JqlOperandResolver.class);
        final DefaultWorkRatioSearcherInputHelper helper = new DefaultWorkRatioSearcherInputHelper(constants, operandResolver);

        assertInvalidResult(helper, null);
    }

    @Test
    public void testStructureCheckFails() throws Exception {
        final Long fromInput = 30L;
        final String fieldId = "test";

        final TerminalClauseImpl clause = new TerminalClauseImpl(fieldId, Operator.GREATER_THAN_EQUALS, fromInput);

        SimpleFieldSearchConstants constants = new SimpleFieldSearchConstants(fieldId, Collections.<Operator>emptySet(), JiraDataTypes.NUMBER);
        JqlOperandResolver operandResolver = mock(JqlOperandResolver.class);
        final DefaultWorkRatioSearcherInputHelper helper = new DefaultWorkRatioSearcherInputHelper(constants, operandResolver) {
            @Override
            List<TerminalClause> validateClauseStructure(final Clause clause) {
                return null;
            }
        };

        assertInvalidResult(helper, clause);
    }

    @Test
    public void testLowerBounds() {
        final Long input = 30L;
        final String fieldId = "workratio";

        final TerminalClauseImpl clause = new TerminalClauseImpl(fieldId, Operator.GREATER_THAN_EQUALS, input);

        Map<String, String> expectedHolder = new HashMap<String, String>();
        expectedHolder.put("workratio:min", "30");

        DefaultWorkRatioSearcherInputHelper helper = createHelper(fieldId);
        assertValidResult(helper, clause, expectedHolder);
    }

    @Test
    public void testUpperBounds() {
        final Long input = 30L;
        final String fieldId = "workratio";

        final TerminalClauseImpl clause = new TerminalClauseImpl(fieldId, Operator.LESS_THAN_EQUALS, input);

        Map<String, String> expectedHolder = new HashMap<String, String>();
        expectedHolder.put("workratio:max", "30");

        DefaultWorkRatioSearcherInputHelper helper = createHelper(fieldId);
        assertValidResult(helper, clause, expectedHolder);
    }

    @Test
    public void testLowerAndUpperBoundsOfQuery() {
        final Long maxInput = 1000L;
        final String maxOutput = "1000";

        final String minInput = "500";
        final String minOutput = "500";

        final String fieldId = "workratio";
        final AndClause clause = new AndClause(
                new TerminalClauseImpl(fieldId, Operator.LESS_THAN_EQUALS, maxInput),
                new TerminalClauseImpl(fieldId, Operator.GREATER_THAN_EQUALS, minInput)
        );

        Map<String, String> expectedHolder = new HashMap<String, String>();
        expectedHolder.put("workratio:min", minOutput);
        expectedHolder.put("workratio:max", maxOutput);

        DefaultWorkRatioSearcherInputHelper helper = createHelper(fieldId);
        assertValidResult(helper, clause, expectedHolder);
    }

    @Test
    public void testTooManyMinimums() {
        final Long minInput1 = 10L;
        final Long minInput2 = 20L;

        final String fieldId = "workratio";
        final AndClause clause = new AndClause(
                new TerminalClauseImpl(fieldId, Operator.GREATER_THAN_EQUALS, minInput1),
                new TerminalClauseImpl(fieldId, Operator.GREATER_THAN_EQUALS, minInput2),
                new TerminalClauseImpl(fieldId, Operator.LESS_THAN_EQUALS, "44")
        );

        DefaultWorkRatioSearcherInputHelper helper = createHelper(fieldId);
        assertInvalidResult(helper, clause);
    }

    @Test
    public void testTooManyMaximums() {
        final Long maxInput1 = 10L;
        final Long maxInput2 = 20L;

        final String fieldId = "workratio";
        final AndClause clause = new AndClause(
                new TerminalClauseImpl(fieldId, Operator.LESS_THAN_EQUALS, maxInput1),
                new TerminalClauseImpl(fieldId, Operator.LESS_THAN_EQUALS, maxInput2),
                new TerminalClauseImpl(fieldId, Operator.GREATER_THAN_EQUALS, "44")
        );

        DefaultWorkRatioSearcherInputHelper helper = createHelper(fieldId);
        assertInvalidResult(helper, clause);
    }

    @Test
    public void testQueryWithABadOperandDoesNotCompute() {
        final Operand operand = new SingleValueOperand("xxx");
        final String fieldId = "field1";
        final TerminalClauseImpl clause = new TerminalClauseImpl(fieldId, Operator.LESS_THAN_EQUALS, operand);

        final JqlOperandResolver jqlOperandResolver = mock(JqlOperandResolver.class);
        when(jqlOperandResolver.isEmptyOperand(operand)).thenReturn(false);
        when(jqlOperandResolver.getValues(USER, operand, clause)).thenReturn(Collections.singletonList(new QueryLiteral(new SingleValueOperand("blarg"), (String) null)));

        DefaultWorkRatioSearcherInputHelper helper = createHelper(fieldId, jqlOperandResolver);
        assertInvalidResult(helper, clause);
    }

    @Test
    public void testQueryWithoutValue() {
        final Operand operand = new SingleValueOperand("xxx");
        final String fieldId = "field1";
        final TerminalClauseImpl clause = new TerminalClauseImpl(fieldId, LESS_THAN_EQUALS, operand);

        final JqlOperandResolver jqlOperandResolver = mock(JqlOperandResolver.class);
        when(jqlOperandResolver.isEmptyOperand(operand)).thenReturn(false);
        when(jqlOperandResolver.getValues(USER, operand, clause)).thenReturn(singletonList(new QueryLiteral()));

        DefaultWorkRatioSearcherInputHelper helper = createHelper(fieldId, jqlOperandResolver);
        assertInvalidResult(helper, clause);
    }

    @Test
    public void testQueryWithEmptyOperand() {
        final Operand operand = new EmptyOperand();
        final String fieldId = "field1";
        final TerminalClauseImpl clause = new TerminalClauseImpl(fieldId, LESS_THAN_EQUALS, operand);

        final JqlOperandResolver jqlOperandResolver = mock(JqlOperandResolver.class);
        when(jqlOperandResolver.isEmptyOperand(operand)).thenReturn(true);

        DefaultWorkRatioSearcherInputHelper helper = createHelper(fieldId);
        assertInvalidResult(helper, clause);
    }

    @Test
    public void testQueryWithInvalidOperand() {
        final String badRatio = "13/13/2008";

        final String fieldId = "field1";
        final TerminalClauseImpl clause = new TerminalClauseImpl(fieldId, Operator.LESS_THAN_EQUALS, badRatio);

        Map<String, String> expectedHolder = new HashMap<String, String>();
        expectedHolder.put("field1:max", badRatio);

        DefaultWorkRatioSearcherInputHelper helper = createHelper(fieldId);
        assertValidResult(helper, clause, expectedHolder);
    }

    @Test
    public void testNoWorkRatio() {
        final String fieldId = "notTestName";
        final TerminalClauseImpl clause = new TerminalClauseImpl(fieldId, Operator.EQUALS, "something");

        DefaultWorkRatioSearcherInputHelper helper = createHelper(fieldId + "NOT");
        assertInvalidResult(helper, clause);
    }

    /**
     * Test what happens when the operand returns multiple ratios. We don't support this.
     */
    @Test
    public void testMultiValueOperand() {
        final String fieldId = "created2";
        final TerminalClauseImpl clause = new TerminalClauseImpl(fieldId, Operator.LESS_THAN_EQUALS, new MultiValueOperand("something", "more"));

        DefaultWorkRatioSearcherInputHelper helper = createHelper(fieldId);
        assertInvalidResult(helper, clause);
    }

    private static DefaultWorkRatioSearcherInputHelper createHelper(final String fieldId) {
        return createHelper(fieldId, MockJqlOperandResolver.createSimpleSupport());
    }

    private static DefaultWorkRatioSearcherInputHelper createHelper(final String fieldId, final JqlOperandResolver simpleSupport) {
        return new DefaultWorkRatioSearcherInputHelper(new SimpleFieldSearchConstants(fieldId, Collections.<Operator>emptySet(), JiraDataTypes.NUMBER), simpleSupport);
    }

    private static void assertInvalidResult(DefaultWorkRatioSearcherInputHelper helper, final Clause clause) {
        assertNull(helper.convertClause(clause, USER));
    }

    private static void assertValidResult(DefaultWorkRatioSearcherInputHelper helper, final Clause clause, Map<String, String> expectedValues) {
        final Map<String, String> actualResult = helper.convertClause(clause, null);
        assertNotNull(actualResult);
        assertEquals(expectedValues, actualResult);
    }
}
