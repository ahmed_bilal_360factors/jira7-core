package com.atlassian.jira.issue.index;

import org.junit.Test;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class IssueIndexingParamsTest {

    @Test
    public void testIsSomethingToIndex() {
        assertThat(IssueIndexingParams.INDEX_NONE.isIndex(), equalTo(false));
        assertThat(IssueIndexingParams.INDEX_ISSUE_ONLY.isIndex(), equalTo(true));
        assertThat(IssueIndexingParams.INDEX_ALL.isIndex(), equalTo(true));
    }

    @Test
    public void testIsEverythingToIndex() {
        assertThat(IssueIndexingParams.INDEX_NONE.isIndexAll(), equalTo(false));
        assertThat(IssueIndexingParams.INDEX_ISSUE_ONLY.isIndexAll(), equalTo(false));
        assertThat(IssueIndexingParams.INDEX_ALL.isIndexAll(), equalTo(true));
    }

    @Test
    public void testBuilderAddIndexingParams() {
        final IssueIndexingParams.Builder builder = IssueIndexingParams.builder();
        assertThat(builder.build(), equalTo(IssueIndexingParams.INDEX_ISSUE_ONLY));

        builder.addIndexingParams(IssueIndexingParams.INDEX_NONE);
        assertThat(builder.build(), equalTo(IssueIndexingParams.INDEX_ISSUE_ONLY));

        builder.addIndexingParams(IssueIndexingParams.INDEX_ALL);
        assertThat(builder.build(), equalTo(IssueIndexingParams.INDEX_ALL));
    }

    @Test
    public void testBuilderDefaultParams() {
        final IssueIndexingParams params = IssueIndexingParams.builder(IssueIndexingParams.INDEX_ALL).build();

        assertTrue(params.isIndexComments());
        assertTrue(params.isIndexIssues());
        assertTrue(params.isIndexWorklogs());
        assertTrue(params.isIndexChangeHistory());
        assertTrue(params.isForceReloadFromDatabase());

        final IssueIndexingParams paramsDefault = IssueIndexingParams.builder().build();

        assertFalse(paramsDefault.isIndexComments());
        assertTrue(paramsDefault.isIndexIssues());
        assertFalse(paramsDefault.isIndexWorklogs());
        assertFalse(paramsDefault.isIndexChangeHistory());
        assertTrue(paramsDefault.isForceReloadFromDatabase());
    }

}