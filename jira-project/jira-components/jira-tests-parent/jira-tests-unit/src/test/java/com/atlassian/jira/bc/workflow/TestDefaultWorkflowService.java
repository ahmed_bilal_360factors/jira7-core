package com.atlassian.jira.bc.workflow;

import com.atlassian.beehive.ClusterLock;
import com.atlassian.beehive.ClusterLockService;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.bc.JiraServiceContextImpl;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.junit.rules.MockComponentContainer;
import com.atlassian.jira.local.runner.ListeningMockitoRunner;
import com.atlassian.jira.mock.i18n.MockI18nHelper;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.jira.web.bean.MockI18nBean;
import com.atlassian.jira.workflow.AssignableWorkflowScheme;
import com.atlassian.jira.workflow.JiraWorkflow;
import com.atlassian.jira.workflow.MockJiraWorkflow;
import com.atlassian.jira.workflow.WorkflowException;
import com.atlassian.jira.workflow.WorkflowManager;
import com.atlassian.jira.workflow.WorkflowSchemeManager;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.opensymphony.workflow.loader.DescriptorFactory;
import com.opensymphony.workflow.loader.StepDescriptor;
import com.opensymphony.workflow.loader.WorkflowDescriptor;
import org.hamcrest.core.IsCollectionContaining;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

import static com.atlassian.jira.bc.workflow.DefaultWorkflowService.OVERWRITE_WORKFLOW_LOCK_NAME;
import static com.atlassian.jira.permission.ProjectPermissions.ADMINISTER_PROJECTS;
import static com.google.common.collect.Iterables.getFirst;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

@RunWith(ListeningMockitoRunner.class)
public class TestDefaultWorkflowService {
    private I18nHelper mockI18nHelper;

    @Mock
    private WorkflowManager mockWorkflowManager;
    @Mock
    private PermissionManager mockPermissionManager;
    @Mock
    private GlobalPermissionManager globalPermissionManager;
    @Mock
    private ClusterLockService mockClusterLockService;
    @Mock
    private ClusterLock mockOverwriteWorkflowLock;
    @Mock
    private WorkflowSchemeManager workflowSchemeManager;
    @Mock
    private ProjectManager projectManager;
    @Mock
    private FeatureManager featureManager;
    @Mock
    private EventPublisher eventPublisher;

    @Mock
    private JiraWorkflow mockJiraWorkflow;
    @Mock
    private JiraWorkflow mockDraftWorkflow;
    @Mock
    private JiraWorkflow mockSharedJiraWorkflow;
    @Mock
    private JiraWorkflow mockNotSharedJiraWorkflow2;
    @Mock
    private JiraWorkflow mockSharedJiraWorkflow2;
    @Mock
    private JiraWorkflow mockNotSharedJiraWorkflow3;
    @Mock
    private JiraWorkflow mockNotUsedWorkflow;

    @Mock
    private JiraAuthenticationContext authenticationContext;
    @Rule
    public final MockComponentContainer mockContainer = new MockComponentContainer(this);

    private final static String WORKFLOW_NAME = "jiraworkflow";
    private final static String SHARED_WORKFLOW_NAME = "sharedjiraworkflow";

    private ApplicationUser testUser = new MockApplicationUser("testUser");

    @Before
    public void setUp() throws Exception {
        mockI18nHelper = new MockI18nHelper();
        when(authenticationContext.getI18nHelper()).thenReturn(mockI18nHelper);
        mockContainer.addMockComponent(JiraAuthenticationContext.class, authenticationContext);
        when(mockClusterLockService.getLockForName(OVERWRITE_WORKFLOW_LOCK_NAME)).thenReturn(mockOverwriteWorkflowLock);
        when(workflowSchemeManager.getSchemesForWorkflowIncludingDrafts(Mockito.any(JiraWorkflow.class))).thenReturn(ImmutableList.of());
        when(mockWorkflowManager.getWorkflow(WORKFLOW_NAME)).thenReturn(mockJiraWorkflow);
        when(mockWorkflowManager.getWorkflow(SHARED_WORKFLOW_NAME)).thenReturn(mockSharedJiraWorkflow);
    }

    private DefaultWorkflowService createWorkflowServiceOverridingPermissionCheck(final boolean hasAdminPermission) {
        final DefaultWorkflowService workflowService =
                new DefaultWorkflowService(mockWorkflowManager, authenticationContext, mockPermissionManager, workflowSchemeManager, mockClusterLockService, globalPermissionManager, projectManager, featureManager, eventPublisher) {
                    @Override
                    I18nHelper getI18nBean() {
                        return mockI18nHelper;
                    }

                    @Override
                    boolean hasAdminPermission(final JiraServiceContext jiraServiceContext) {
                        return hasAdminPermission;
                    }

                    @Override
                    boolean hasAdminPermission(final ApplicationUser user) {
                        return hasAdminPermission;
                    }
                };
        workflowService.start();
        return workflowService;
    }

    private DefaultWorkflowService createWorkflowServiceWithRealPermissionCheck() {
        final DefaultWorkflowService workflowService =
                new DefaultWorkflowService(mockWorkflowManager, null, mockPermissionManager, workflowSchemeManager, mockClusterLockService, globalPermissionManager, projectManager, featureManager, eventPublisher) {
                    @Override
                    I18nHelper getI18nBean() {
                        return mockI18nHelper;
                    }
                };
        workflowService.start();
        return workflowService;
    }

    @SuppressWarnings("deprecation")
    private void stubHasAllPermisions(final boolean hasPermissions) {
        when(mockPermissionManager.hasPermission(anyInt(), any(ApplicationUser.class))).thenReturn(hasPermissions);
        when(globalPermissionManager.hasPermission(any(GlobalPermissionKey.class), any(ApplicationUser.class))).thenReturn(hasPermissions);
    }

    private JiraServiceContextImpl createServiceContext() {
        return createServiceContext(new SimpleErrorCollection());
    }

    private JiraServiceContextImpl createServiceContextWithNoUser() {
        return new JiraServiceContextImpl((ApplicationUser) null, new SimpleErrorCollection());
    }

    private JiraServiceContextImpl createServiceContext(ErrorCollection errorCollection) {
        return new JiraServiceContextImpl(testUser, errorCollection);
    }

    private String getFirstMessage(JiraServiceContext jiraServiceContext) {
        return getFirst(jiraServiceContext.getErrorCollection().getErrorMessages(), null);
    }

    private void assertNoErrors(JiraServiceContext jiraServiceContext) {
        assertFalse("Expected no errors in " + jiraServiceContext, jiraServiceContext.getErrorCollection().hasAnyErrors());
    }

    /**
     * Asserts that the given service context has error collection with exactly one error message equal to
     * <tt>expectedMessage</tt>
     *
     * @param expectedMessage expected message
     * @param serviceContext  service context to check
     */
    private void assertErrorMessage(final String expectedMessage, final JiraServiceContext serviceContext) {
        assertErrorMessage(expectedMessage, serviceContext.getErrorCollection());
    }

    /**
     * Asserts that the given ErrorCollection contains only the given message.
     *
     * @param message         Expected message
     * @param errorCollection Actual ErrorCollection
     */
    private void assertErrorMessage(final String message, final ErrorCollection errorCollection) {
        assertEquals("Expected 1 error message", 1, errorCollection.getErrorMessages().size());
        // We only expect "error messages", not "errors"
        if (errorCollection.getErrors().isEmpty()) {
            assertEquals(message, errorCollection.getErrorMessages().iterator().next());
        } else {
            fail("ErrorCollection was only expected to contain an error of type 'ErrorMessage', but it contains type"
                    + " 'Error' as well.");
        }
    }

    private void resetErrorCollection(JiraServiceContext serviceContext) {
        serviceContext.getErrorCollection().setErrorMessages(Lists.<String>newArrayList());
        serviceContext.getErrorCollection().getErrors().clear();
    }

    @Test
    public void testGetDraftWorkflow() {
        when(mockWorkflowManager.getWorkflow("testworkflow")).thenReturn(mockJiraWorkflow);
        when(mockWorkflowManager.getDraftWorkflow("testworkflow")).thenReturn(mockDraftWorkflow);

        final ErrorCollection errorCollection = new SimpleErrorCollection();

        final JiraServiceContext jiraServiceContext = createServiceContext(errorCollection);
        final DefaultWorkflowService workflowService = createWorkflowServiceOverridingPermissionCheck(true);

        assertSame(mockDraftWorkflow, workflowService.getDraftWorkflow(jiraServiceContext, "testworkflow"));
        assertFalse(jiraServiceContext.getErrorCollection().hasAnyErrors());
    }

    @Test
    public void testGetDraftWorkflowWithNoAdminPermission() {
        final ErrorCollection errorCollection = new SimpleErrorCollection();

        final JiraServiceContext jiraServiceContext = createServiceContext(errorCollection);
        final DefaultWorkflowService workflowService = createWorkflowServiceOverridingPermissionCheck(false);

        assertNull(workflowService.getDraftWorkflow(jiraServiceContext, "testworkflow"));
        assertTrue(jiraServiceContext.getErrorCollection().hasAnyErrors());
        assertEquals("admin.workflows.service.error.no.admin.permission", getFirstMessage(jiraServiceContext));
    }

    @Test
    public void testGetDraftWorkflowWithNoParent() {
        when(mockWorkflowManager.getWorkflow(anyString())).thenReturn(null);

        final ErrorCollection errorCollection = new SimpleErrorCollection();
        final JiraServiceContext jiraServiceContext = createServiceContext(errorCollection);
        final DefaultWorkflowService workflowService = createWorkflowServiceOverridingPermissionCheck(true);

        assertNull(workflowService.getDraftWorkflow(jiraServiceContext, "testworkflow"));
        assertTrue(jiraServiceContext.getErrorCollection().hasAnyErrors());
        assertEquals("admin.workflows.service.error.retrieve.no.parent", jiraServiceContext.getErrorCollection().getErrorMessages().iterator().next());

        jiraServiceContext.getErrorCollection().setErrorMessages(Lists.<String>newArrayList());
        assertNull(workflowService.getDraftWorkflow(jiraServiceContext, null));
        assertEquals("admin.workflows.service.error.no.parent", getFirstMessage(jiraServiceContext));

        jiraServiceContext.getErrorCollection().setErrorMessages(Lists.<String>newArrayList());
        workflowService.getDraftWorkflow(jiraServiceContext, "");
        assertEquals("admin.workflows.service.error.no.parent", getFirstMessage(jiraServiceContext));
    }

    @Test
    public void testCreateDraftWorkflow() {
        when(mockWorkflowManager.getWorkflow("testworkflow")).thenReturn(mockJiraWorkflow);
        when(mockWorkflowManager.isActive(mockJiraWorkflow)).thenReturn(true);
        when(mockWorkflowManager.createDraftWorkflow(testUser, "testworkflow")).thenReturn(mockDraftWorkflow);


        final ErrorCollection errorCollection = new SimpleErrorCollection();
        final JiraServiceContext jiraServiceContext = createServiceContext(errorCollection);
        final DefaultWorkflowService workflowService = createWorkflowServiceOverridingPermissionCheck(true);

        assertSame(mockDraftWorkflow, workflowService.createDraftWorkflow(jiraServiceContext, "testworkflow"));
        assertFalse(jiraServiceContext.getErrorCollection().hasAnyErrors());
    }

    @Test
    public void testCreateDraftWorkflowNoAdminPermission() {
        final ErrorCollection errorCollection = new SimpleErrorCollection();
        final JiraServiceContext jiraServiceContext = createServiceContext(errorCollection);
        final DefaultWorkflowService workflowService = createWorkflowServiceOverridingPermissionCheck(false);

        assertNull(workflowService.createDraftWorkflow(jiraServiceContext, "testworkflow"));
        assertTrue(jiraServiceContext.getErrorCollection().hasAnyErrors());
        assertEquals("admin.workflows.service.error.no.admin.permission",
                jiraServiceContext.getErrorCollection().getErrorMessages().iterator().next());
    }

    @Test
    public void testCreateDraftWorkflowWithNullUser() {
        when(mockWorkflowManager.getWorkflow(anyString())).thenReturn(mockJiraWorkflow);
        when(mockWorkflowManager.isActive(mockJiraWorkflow)).thenReturn(true);
        when(mockWorkflowManager.createDraftWorkflow((ApplicationUser) null, "testworkflow")).thenReturn(mockDraftWorkflow);

        final JiraServiceContext jiraServiceContext = createServiceContextWithNoUser();
        final DefaultWorkflowService workflowService = createWorkflowServiceOverridingPermissionCheck(true);

        assertSame(mockDraftWorkflow, workflowService.createDraftWorkflow(jiraServiceContext, "testworkflow"));
        assertFalse(jiraServiceContext.getErrorCollection().hasAnyErrors());
    }

    @Test
    public void testCreateDraftWorkflowWithNoParentWorkflow() {
        when(mockWorkflowManager.getWorkflow(anyString())).thenReturn(null);

        final JiraServiceContext jiraServiceContext = createServiceContext();
        final DefaultWorkflowService workflowService = createWorkflowServiceOverridingPermissionCheck(true);

        assertNull(workflowService.createDraftWorkflow(jiraServiceContext, "testworkflow"));
        assertTrue(jiraServiceContext.getErrorCollection().hasAnyErrors());
        assertEquals("admin.workflows.service.error.no.parent", getFirstMessage(jiraServiceContext));

        jiraServiceContext.getErrorCollection().setErrorMessages(Lists.<String>newArrayList());
        workflowService.createDraftWorkflow(jiraServiceContext, null);
        assertEquals("admin.workflows.service.error.no.parent", getFirstMessage(jiraServiceContext));

        jiraServiceContext.getErrorCollection().setErrorMessages(Lists.<String>newArrayList());
        workflowService.createDraftWorkflow(jiraServiceContext, "");
        assertEquals("admin.workflows.service.error.no.parent", getFirstMessage(jiraServiceContext));

    }

    @Test
    public void testCreateDraftWorkflowWithInactiveParentWorkflow() {
        when(mockWorkflowManager.getWorkflow("testworkflow")).thenReturn(mockJiraWorkflow);
        when(mockWorkflowManager.isActive(mockJiraWorkflow)).thenReturn(false);

        final ErrorCollection errorCollection = new SimpleErrorCollection();
        final JiraServiceContext jiraServiceContext = createServiceContext(errorCollection);
        final DefaultWorkflowService workflowService = createWorkflowServiceOverridingPermissionCheck(true);

        assertNull(workflowService.createDraftWorkflow(jiraServiceContext, "testworkflow"));
        assertTrue(jiraServiceContext.getErrorCollection().hasAnyErrors());
        assertEquals("admin.workflows.service.error.parent.not.active", getFirstMessage(jiraServiceContext));
    }

    @Test
    public void testCreateDraftWorkflowIllegalState() {
        when(mockWorkflowManager.getWorkflow("testworkflow")).thenReturn(mockJiraWorkflow);
        when(mockWorkflowManager.isActive(mockJiraWorkflow)).thenReturn(true);
        //this may happen if two users try to create 2 drafts at the same time.
        when(mockWorkflowManager.createDraftWorkflow(testUser, "testworkflow")).thenThrow(new IllegalStateException("Draft already exists"));


        final ErrorCollection errorCollection = new SimpleErrorCollection();
        final JiraServiceContext jiraServiceContext = createServiceContext(errorCollection);
        final DefaultWorkflowService workflowService = createWorkflowServiceOverridingPermissionCheck(true);

        assertNull(workflowService.createDraftWorkflow(jiraServiceContext, "testworkflow"));
        assertErrorMessage("admin.workflows.service.error.draft.exists.or.workflow.not.active", jiraServiceContext);
    }

    @Test
    public void testDeleteDraftWorkflow() {
        when(mockWorkflowManager.deleteDraftWorkflow("testWorkflow")).thenReturn(true);
        final DefaultWorkflowService workflowService = createWorkflowServiceOverridingPermissionCheck(true);
        assertTrue(workflowService.deleteDraftWorkflow(null, "testWorkflow"));
        verify(mockWorkflowManager).getWorkflow("testWorkflow");
        verify(mockWorkflowManager).deleteDraftWorkflow("testWorkflow");
        verifyNoMoreInteractions(mockWorkflowManager);
    }

    @Test
    public void testDeleteDraftWorkflowWithNoAdminPermission() {
        final DefaultWorkflowService workflowService = createWorkflowServiceOverridingPermissionCheck(false);

        final ErrorCollection errorCollection = new SimpleErrorCollection();
        final JiraServiceContext jiraServiceContext = createServiceContext(errorCollection);
        assertFalse(workflowService.deleteDraftWorkflow(jiraServiceContext, "testWorkflow"));
        assertErrorMessage("admin.workflows.service.error.no.admin.permission", jiraServiceContext);
    }

    @Test
    public void testDeleteDraftWorkflowWithNullParent() {
        final DefaultWorkflowService workflowService = createWorkflowServiceOverridingPermissionCheck(true);
        final JiraServiceContext jiraServiceContext = createServiceContext();
        workflowService.deleteDraftWorkflow(jiraServiceContext, null);

        assertTrue(jiraServiceContext.getErrorCollection().hasAnyErrors());
        assertErrorMessage("admin.workflows.service.error.delete.no.parent", jiraServiceContext);
    }

    @Test
    public void testUpdateDraftWorkflow() {
        when(mockDraftWorkflow.getDescriptor()).thenReturn(new DescriptorFactory().createWorkflowDescriptor());
        when(mockDraftWorkflow.isEditable()).thenReturn(true);

        final DefaultWorkflowService workflowService = createWorkflowServiceOverridingPermissionCheck(true);
        final JiraServiceContext jiraServiceContext = createServiceContext();
        workflowService.updateWorkflow(jiraServiceContext, mockDraftWorkflow);

        assertFalse(jiraServiceContext.getErrorCollection().hasAnyErrors());
        verify(mockWorkflowManager).updateWorkflow(testUser, mockDraftWorkflow);
    }

    @Test
    public void testUpdateWorkflowNullWorkflow() {
        final DefaultWorkflowService workflowService = createWorkflowServiceOverridingPermissionCheck(true);

        final JiraServiceContext jiraServiceContext = createServiceContext();
        workflowService.updateWorkflow(jiraServiceContext, null);
        assertErrorMessage("admin.workflows.service.error.update.no.workflow", jiraServiceContext);
        assertReason(ErrorCollection.Reason.VALIDATION_FAILED, jiraServiceContext);
    }

    @Test
    public void testUpateWorkflowNullDescriptor() {
        when(mockDraftWorkflow.getDescriptor()).thenReturn(null);
        final DefaultWorkflowService workflowService = createWorkflowServiceOverridingPermissionCheck(true);

        final JiraServiceContext jiraServiceContext = createServiceContext();
        workflowService.updateWorkflow(jiraServiceContext, mockDraftWorkflow);

        assertErrorMessage("admin.workflows.service.error.update.no.workflow", jiraServiceContext);
        assertReason(ErrorCollection.Reason.VALIDATION_FAILED, jiraServiceContext);
    }

    @Test
    public void testUpdateWorkflowNoAdminPermission() {
        final DefaultWorkflowService workflowService = createWorkflowServiceOverridingPermissionCheck(false);

        final JiraServiceContext jiraServiceContext = createServiceContext();
        workflowService.updateWorkflow(jiraServiceContext, mockDraftWorkflow);

        assertErrorMessage("admin.workflows.service.error.no.admin.permission", jiraServiceContext);
        assertReason(ErrorCollection.Reason.FORBIDDEN, jiraServiceContext);
    }

    @Test
    public void testUpdateWorkflowNullUsername() {
        when(mockDraftWorkflow.getDescriptor()).thenReturn(new DescriptorFactory().createWorkflowDescriptor());
        when(mockDraftWorkflow.isEditable()).thenReturn(true);

        final DefaultWorkflowService workflowService = createWorkflowServiceOverridingPermissionCheck(true);
        final JiraServiceContext jiraServiceContext = createServiceContextWithNoUser();
        workflowService.updateWorkflow(jiraServiceContext, mockDraftWorkflow);
        assertNoErrors(jiraServiceContext);
        verify(mockWorkflowManager).updateWorkflow((ApplicationUser) null, mockDraftWorkflow);
    }

    @Test
    public void testUpdateWorkflowNotEditable() {
        when(mockDraftWorkflow.getDescriptor()).thenReturn(new DescriptorFactory().createWorkflowDescriptor());
        when(mockDraftWorkflow.isEditable()).thenReturn(false);
        final DefaultWorkflowService workflowService = createWorkflowServiceOverridingPermissionCheck(true);
        final JiraServiceContext jiraServiceContext = createServiceContext();
        workflowService.updateWorkflow(jiraServiceContext, mockDraftWorkflow);
        assertErrorMessage("admin.workflows.service.error.not.editable", jiraServiceContext);
        assertReason(ErrorCollection.Reason.VALIDATION_FAILED, jiraServiceContext);
    }

    @Test
    public void testOverwriteWorkflowNullName() {
        final DefaultWorkflowService defaultWorkflowService = createWorkflowServiceOverridingPermissionCheck(true);
        final JiraServiceContext jiraServiceContext = createServiceContext();

        defaultWorkflowService.overwriteActiveWorkflow(jiraServiceContext, null);
        assertErrorMessage("admin.workflows.service.error.overwrite.no.parent [null]", jiraServiceContext);
    }

    @Test
    public void testOverwriteWorkflowNoAdminPermission() {
        final DefaultWorkflowService defaultWorkflowService = createWorkflowServiceOverridingPermissionCheck(false);
        final JiraServiceContext jiraServiceContext = createServiceContext();

        defaultWorkflowService.overwriteActiveWorkflow(jiraServiceContext, "jiraworkflow");
        assertErrorMessage("admin.workflows.service.error.no.admin.permission", jiraServiceContext);
    }

    @Test
    public void testOverwriteWorkflow() {
        final DefaultWorkflowService defaultWorkflowService =
                new DefaultWorkflowService(mockWorkflowManager, null, null, null, mockClusterLockService, globalPermissionManager, projectManager, featureManager, eventPublisher) {
                    @Override
                    I18nHelper getI18nBean() {
                        return mockI18nHelper;
                    }

                    @Override
                    boolean hasAdminPermission(final JiraServiceContext jiraServiceContext) {
                        return true;
                    }

                    @Override
                    boolean hasAdminPermission(final ApplicationUser user) {
                        return true;
                    }

                    @Override
                    public void validateOverwriteWorkflow(final JiraServiceContext jiraServiceContext, final String workflowName) {
                        // Don't do any validation for this test.
                    }
                };
        defaultWorkflowService.start();
        final JiraServiceContext jiraServiceContext = createServiceContext();

        defaultWorkflowService.overwriteActiveWorkflow(jiraServiceContext, "jiraworkflow");
        assertNoErrors(jiraServiceContext);
        verify(mockWorkflowManager).overwriteActiveWorkflow(testUser, "jiraworkflow");
    }

    @Test
    public void testOverwriteWorkflowNullUser() {
        final DefaultWorkflowService defaultWorkflowService =
                new DefaultWorkflowService(mockWorkflowManager, null, null, null, mockClusterLockService, globalPermissionManager, projectManager, featureManager, eventPublisher) {
                    @Override
                    I18nHelper getI18nBean() {
                        return mockI18nHelper;
                    }

                    @Override
                    boolean hasAdminPermission(final JiraServiceContext jiraServiceContext) {
                        return true;
                    }

                    @Override
                    boolean hasAdminPermission(final ApplicationUser user) {
                        return true;
                    }

                    @Override
                    public void validateOverwriteWorkflow(final JiraServiceContext jiraServiceContext, final String workflowName) {
                        // Don't do any validation for this test.
                    }
                };
        defaultWorkflowService.start();
        final JiraServiceContext jiraServiceContext = createServiceContextWithNoUser();

        defaultWorkflowService.overwriteActiveWorkflow(jiraServiceContext, "jiraworkflow");

        assertNoErrors(jiraServiceContext);
        verify(mockWorkflowManager).overwriteActiveWorkflow((ApplicationUser) null, "jiraworkflow");
    }

    @Test
    public void testValidateOverwriteWorkflowNoPermission() {
        stubHasAllPermisions(false);
        final JiraServiceContext jiraServiceContext = createServiceContext();
        final DefaultWorkflowService defaultWorkflowService = createWorkflowServiceWithRealPermissionCheck();

        // Null workflow name is invalid
        defaultWorkflowService.validateOverwriteWorkflow(jiraServiceContext, null);
        assertErrorMessage("admin.workflows.service.error.no.admin.permission", jiraServiceContext.getErrorCollection());
    }

    @Test
    public void testValidateOverwriteWorkflowInvalidWorkflowName() {
        stubHasAllPermisions(true);
        when(mockWorkflowManager.getWorkflow("Some Rubbish")).thenReturn(null);

        final DefaultWorkflowService defaultWorkflowService = createWorkflowServiceWithRealPermissionCheck();
        final JiraServiceContext serviceContext = createServiceContext();

        // Null workflow name is invalid
        defaultWorkflowService.validateOverwriteWorkflow(serviceContext, null);
        assertErrorMessage("admin.workflows.service.error.overwrite.no.parent [null]", serviceContext.getErrorCollection());

        // Empty workflow name is invalid
        resetErrorCollection(serviceContext);
        defaultWorkflowService.validateOverwriteWorkflow(serviceContext, "");
        assertErrorMessage("admin.workflows.service.error.overwrite.no.parent []", serviceContext.getErrorCollection());

        // Workflow name is unknown
        resetErrorCollection(serviceContext);
        defaultWorkflowService.validateOverwriteWorkflow(serviceContext, "Some Rubbish");
        assertErrorMessage("admin.workflows.service.error.overwrite.no.parent [Some Rubbish]", serviceContext.getErrorCollection());
    }

    @Test
    public void testValidateOverwriteWorkflowNoDraftWorkflow() throws Exception {
        stubHasAllPermisions(true);
        when(mockWorkflowManager.getWorkflow("My Workflow")).thenReturn(mockJiraWorkflow);
        when(mockWorkflowManager.getDraftWorkflow("My Workflow")).thenReturn(null);
        when(mockJiraWorkflow.isActive()).thenReturn(true);
        final DefaultWorkflowService defaultWorkflowService = createWorkflowServiceWithRealPermissionCheck();
        final JiraServiceContext jiraServiceContext = createServiceContext();

        defaultWorkflowService.validateOverwriteWorkflow(jiraServiceContext, "My Workflow");
        assertErrorMessage("admin.workflows.service.error.overwrite.no.draft [My Workflow]", jiraServiceContext.getErrorCollection());
    }


    @Test
    public void testValidateOverwriteWorkflowInactiveParent() throws Exception {
        stubHasAllPermisions(true);
        when(mockWorkflowManager.getWorkflow("My Workflow")).thenReturn(mockJiraWorkflow);
        when(mockJiraWorkflow.isActive()).thenReturn(false);

        final DefaultWorkflowService defaultWorkflowService = createWorkflowServiceWithRealPermissionCheck();
        final JiraServiceContext jiraServiceContext = createServiceContext();

        defaultWorkflowService.validateOverwriteWorkflow(jiraServiceContext, "My Workflow");
        assertErrorMessage("admin.workflows.service.error.overwrite.inactive.parent [My Workflow]", jiraServiceContext);
    }

    @Test
    public void testValidateOverwriteWorkflow() throws Exception {
        stubHasAllPermisions(true);
        final MockJiraWorkflow oldJiraWorkflow = new MockJiraWorkflow();
        final MockJiraWorkflow newJiraWorkflow = new MockJiraWorkflow();
        when(mockWorkflowManager.getWorkflow("My Workflow")).thenReturn(oldJiraWorkflow);
        when(mockWorkflowManager.getDraftWorkflow("My Workflow")).thenReturn(newJiraWorkflow);
        final AtomicBoolean validateAddWorkflowTransitionCalled = new AtomicBoolean(false);

        final DefaultWorkflowService defaultWorkflowService =
                new DefaultWorkflowService(mockWorkflowManager, null, mockPermissionManager, null, mockClusterLockService, globalPermissionManager, projectManager, featureManager, eventPublisher) {
                    @Override
                    I18nHelper getI18nBean() {
                        // TODO bad, it actually loads translations. We don't want that in unit tests
                        return new MockI18nBean();
                    }

                    @Override
                    public void validateAddWorkflowTransitionToDraft(JiraServiceContext jiraServiceContext, JiraWorkflow workflow, int stepId) {
                        validateAddWorkflowTransitionCalled.set(true);
                        super.validateAddWorkflowTransitionToDraft(jiraServiceContext, workflow, stepId);
                    }
                };
        defaultWorkflowService.start();
        final JiraServiceContext serviceContext = createServiceContext();

        // Set up a minimal workflow
        oldJiraWorkflow.addStep(1, "Open");
        newJiraWorkflow.addStep(1, "Closed");
        defaultWorkflowService.validateOverwriteWorkflow(serviceContext, "My Workflow");
        assertErrorMessage("The draft workflow does not contain required status 'Open'.", serviceContext);

        // Now make the workflow have the required status but with wrong step ID
        resetErrorCollection(serviceContext);
        oldJiraWorkflow.clear();
        oldJiraWorkflow.addStep(1, "Open");
        newJiraWorkflow.clear();
        newJiraWorkflow.addStep(2, "Open");
        defaultWorkflowService.validateOverwriteWorkflow(serviceContext, "My Workflow");
        assertErrorMessage("You cannot change the association between step '1' and status 'Open'.", serviceContext);

        // OK - now finally make a trivial valid change
        resetErrorCollection(serviceContext);
        oldJiraWorkflow.clear();
        oldJiraWorkflow.addStep(1, "Open");
        newJiraWorkflow.clear();
        newJiraWorkflow.addStep(1, "Open");
        newJiraWorkflow.addStep(2, "Closed");
        defaultWorkflowService.validateOverwriteWorkflow(serviceContext, "My Workflow");
        assertNoErrors(serviceContext);

        // Bigger set of steps
        resetErrorCollection(serviceContext);
        oldJiraWorkflow.clear();
        oldJiraWorkflow.addStep(1, "Open");
        oldJiraWorkflow.addStep(2, "Assigned");
        oldJiraWorkflow.addStep(3, "Resolved");
        oldJiraWorkflow.addStep(4, "Closed");
        newJiraWorkflow.clear();
        newJiraWorkflow.addStep(1, "Open");
        newJiraWorkflow.addStep(4, "Closed");
        newJiraWorkflow.addStep(2, "Assigned");
        newJiraWorkflow.addStep(3, "Resolved");
        newJiraWorkflow.addStep(5, "Re-Opened");
        defaultWorkflowService.validateOverwriteWorkflow(serviceContext, "My Workflow");
        assertNoErrors(serviceContext);

        // OK - now finally make a trivial valid change
        resetErrorCollection(serviceContext);
        oldJiraWorkflow.clear();
        oldJiraWorkflow.addStep(1, "Open");
        oldJiraWorkflow.addStep(2, "Assigned");
        oldJiraWorkflow.addStep(3, "Resolved");
        oldJiraWorkflow.addStep(4, "Closed");
        newJiraWorkflow.clear();
        newJiraWorkflow.addStep(1, "Open");
        newJiraWorkflow.addStep(2, "Assigned");
        newJiraWorkflow.addStep(4, "Closed");
        defaultWorkflowService.validateOverwriteWorkflow(serviceContext, "My Workflow");
        assertErrorMessage("The draft workflow does not contain required status 'Resolved'.", serviceContext);

        // OK - now finally make a trivial valid change
        resetErrorCollection(serviceContext);
        oldJiraWorkflow.clear();
        oldJiraWorkflow.addStep(1, "Open");
        oldJiraWorkflow.addStep(2, "Assigned");
        oldJiraWorkflow.addStep(3, "Resolved");
        oldJiraWorkflow.addStep(4, "Closed");
        newJiraWorkflow.clear();
        newJiraWorkflow.addStep(1, "Open");
        newJiraWorkflow.addStep(2, "Assigned");
        newJiraWorkflow.addStep(3, "Resolved");
        newJiraWorkflow.addStep(4, "Rubbish");
        defaultWorkflowService.validateOverwriteWorkflow(serviceContext, "My Workflow");
        assertErrorMessage("The draft workflow does not contain required status 'Closed'.", serviceContext);

        // OK - now finally make a trivial valid change
        resetErrorCollection(serviceContext);
        oldJiraWorkflow.clear();
        oldJiraWorkflow.addStep(1, "Open");
        oldJiraWorkflow.addStep(2, "Assigned");
        oldJiraWorkflow.addStep(3, "Resolved");
        oldJiraWorkflow.addStep(4, "Closed");
        newJiraWorkflow.clear();
        newJiraWorkflow.addStep(1, "Open");
        newJiraWorkflow.addStep(2, "Assigned");
        newJiraWorkflow.addStep(3, "Resolved");
        newJiraWorkflow.addStep(6, "Closed");
        defaultWorkflowService.validateOverwriteWorkflow(serviceContext, "My Workflow");
        assertErrorMessage("You cannot change the association between step '4' and status 'Closed'.", serviceContext);
        assertTrue(validateAddWorkflowTransitionCalled.get());
    }

    @Test
    public void testGetWorkflowNullName() {
        final DefaultWorkflowService workflowService = createWorkflowServiceWithRealPermissionCheck();

        final JiraServiceContext serviceContext = createServiceContext();
        workflowService.getWorkflow(serviceContext, null);
        assertErrorMessage("admin.workflows.service.error.null.name", serviceContext);

        resetErrorCollection(serviceContext);
        workflowService.getWorkflow(serviceContext, "");
        assertErrorMessage("admin.workflows.service.error.null.name", serviceContext);
    }

    @Test
    public void testValidateCopyWorkflowNoAdminRights() {
        final DefaultWorkflowService workflowService = createWorkflowServiceOverridingPermissionCheck(false);
        final JiraServiceContext serviceContext = createServiceContext();
        workflowService.validateCopyWorkflow(serviceContext, null);
        assertErrorMessage("admin.workflows.service.error.no.admin.permission", serviceContext);
    }

    @Test
    public void testValidateCopyWorkflow() {
        when(mockWorkflowManager.workflowExists("Copy of Workflow")).thenReturn(true);
        final DefaultWorkflowService workflowService = createWorkflowServiceOverridingPermissionCheck(true);

        final JiraServiceContext jiraServiceContext = createServiceContext();
        workflowService.validateCopyWorkflow(jiraServiceContext, null);
        assertEquals("admin.errors.you.must.specify.a.workflow.name", jiraServiceContext.getErrorCollection().getErrors().get("newWorkflowName"));

        resetErrorCollection(jiraServiceContext);
        //non-ascii chars
        workflowService.validateCopyWorkflow(jiraServiceContext, "The\u0192\u00e7WORKFLOW");
        assertEquals("admin.errors.please.use.only.ascii.characters", jiraServiceContext.getErrorCollection().getErrors().get("newWorkflowName"));


        resetErrorCollection(jiraServiceContext);
        //already exists.
        workflowService.validateCopyWorkflow(jiraServiceContext, "Copy of Workflow");
        assertEquals("admin.errors.a.workflow.with.this.name.already.exists", jiraServiceContext.getErrorCollection().getErrors().get("newWorkflowName"));
    }

    @Test
    public void testValidateCopyWorkflowSuccess() {
        when(mockWorkflowManager.workflowExists("Copy of Workflow")).thenReturn(false);
        final DefaultWorkflowService workflowService = createWorkflowServiceOverridingPermissionCheck(true);
        final JiraServiceContext jiraServiceContext = createServiceContext();

        workflowService.validateCopyWorkflow(jiraServiceContext, "Copy of Workflow");
        assertNoErrors(jiraServiceContext);
        verify(mockWorkflowManager).workflowExists("Copy of Workflow");
    }

    @Test
    public void testCopyWorkflowSuccess() {
        final MockJiraWorkflow workflow = new MockJiraWorkflow();
        when(mockWorkflowManager.copyWorkflow(testUser, "Copy of Workflow", null, workflow)).thenReturn(workflow);
        final DefaultWorkflowService workflowService = createWorkflowServiceOverridingPermissionCheck(true);

        final JiraServiceContext jiraServiceContext = createServiceContext();

        assertSame(workflow, workflowService.copyWorkflow(jiraServiceContext, "Copy of Workflow", null, workflow));
        assertNoErrors(jiraServiceContext);
    }

    @Test
    public void testCopyWorkflowSuccessWithNullUser() {
        final MockJiraWorkflow workflow = new MockJiraWorkflow();
        when(mockWorkflowManager.copyWorkflow((ApplicationUser) null, "Copy of Workflow", null, workflow)).thenReturn(workflow);
        final DefaultWorkflowService workflowService = createWorkflowServiceOverridingPermissionCheck(true);
        final JiraServiceContext jiraServiceContext = createServiceContextWithNoUser();

        assertSame(workflow, workflowService.copyWorkflow(jiraServiceContext, "Copy of Workflow", null, workflow));
        assertNoErrors(jiraServiceContext);
    }

    @Test
    public void testCopyWorkflowNoAdminPermission() {
        final MockJiraWorkflow workflow = new MockJiraWorkflow();
        final DefaultWorkflowService workflowService = createWorkflowServiceOverridingPermissionCheck(false);

        final JiraServiceContext jiraServiceContext = createServiceContext();

        workflowService.copyWorkflow(jiraServiceContext, "Copy of Workflow", null, workflow);
        assertErrorMessage("admin.workflows.service.error.no.admin.permission", jiraServiceContext);
    }


    @Test
    public void testValidateUpdateWorkflowNoAdminPermission() {
        final DefaultWorkflowService workflowService = createWorkflowServiceOverridingPermissionCheck(false);
        final JiraServiceContext jiraServiceContext = createServiceContext();
        workflowService.validateUpdateWorkflowNameAndDescription(jiraServiceContext, null, null);
        assertErrorMessage("admin.workflows.service.error.no.admin.permission", jiraServiceContext);
    }

    @Test
    public void testValidateUpdateWorkflowThatIsNotModifiableIsNotAllowed() throws WorkflowException {
        final DefaultWorkflowService workflowService = createWorkflowServiceOverridingPermissionCheck(true);

        final JiraServiceContext jiraServiceContext = createServiceContext();

        when(mockJiraWorkflow.isEditable()).thenReturn(false);
        when(mockJiraWorkflow.getDescriptor()).thenReturn(new DescriptorFactory().createWorkflowDescriptor());
        workflowService.validateUpdateWorkflowNameAndDescription(jiraServiceContext, mockJiraWorkflow, null);
        assertErrorMessage("admin.errors.workflow.cannot.be.edited.as.it.is.not.editable", jiraServiceContext);
    }

    @Test
    public void testValidateUpdateWorkflowWithNullNewName() throws WorkflowException {

        final DefaultWorkflowService workflowService = createWorkflowServiceOverridingPermissionCheck(true);

        final JiraServiceContext serviceContext = createServiceContext();

        when(mockJiraWorkflow.isDraftWorkflow()).thenReturn(false);
        when(mockJiraWorkflow.isEditable()).thenReturn(true);
        when(mockJiraWorkflow.getDescriptor()).thenReturn(new DescriptorFactory().createWorkflowDescriptor());
        workflowService.validateUpdateWorkflowNameAndDescription(serviceContext, mockJiraWorkflow, null);
        assertTrue(serviceContext.getErrorCollection().getErrors().get("newWorkflowName").equals("admin.errors.you.must.specify.a.workflow.name"));
    }

    @Test
    public void testValidateUpdateWorkflowWithEmptyNewName() throws WorkflowException {
        final DefaultWorkflowService workflowService = createWorkflowServiceOverridingPermissionCheck(true);

        final JiraServiceContext jiraServiceContext = createServiceContext();
        when(mockJiraWorkflow.isDraftWorkflow()).thenReturn(false);
        when(mockJiraWorkflow.isEditable()).thenReturn(true);
        when(mockJiraWorkflow.getDescriptor()).thenReturn(new DescriptorFactory().createWorkflowDescriptor());
        workflowService.validateUpdateWorkflowNameAndDescription(jiraServiceContext, mockJiraWorkflow, "");
        assertTrue(jiraServiceContext.getErrorCollection().getErrors().get("newWorkflowName").equals("admin.errors.you.must.specify.a.workflow.name"));
    }

    @Test
    public void testValidateUpdateWorkflowWithInvalidNewName() throws WorkflowException {
        final DefaultWorkflowService workflowService = createWorkflowServiceOverridingPermissionCheck(true);

        final JiraServiceContext jiraServiceContext = createServiceContext();
        when(mockJiraWorkflow.isDraftWorkflow()).thenReturn(false);
        when(mockJiraWorkflow.isEditable()).thenReturn(true);
        when(mockJiraWorkflow.getDescriptor()).thenReturn(new DescriptorFactory().createWorkflowDescriptor());
        workflowService.validateUpdateWorkflowNameAndDescription(jiraServiceContext, mockJiraWorkflow, "InvalidNewName\u0192\u00e7");
        assertTrue(jiraServiceContext.getErrorCollection().getErrors().get("newWorkflowName").equals("admin.errors.please.use.only.ascii.characters"));
    }

    @Test
    public void testValidateUpdateWorkflowWithNewWorkflowExists() throws WorkflowException {
        when(mockWorkflowManager.workflowExists("newWorkflow")).thenReturn(true);

        final DefaultWorkflowService workflowService = createWorkflowServiceOverridingPermissionCheck(true);
        final JiraServiceContext jiraServiceContext = createServiceContext();

        when(mockJiraWorkflow.getName()).thenReturn("workflowName");
        when(mockJiraWorkflow.isDraftWorkflow()).thenReturn(false);
        when(mockJiraWorkflow.isEditable()).thenReturn(true);
        when(mockJiraWorkflow.getDescriptor()).thenReturn(new DescriptorFactory().createWorkflowDescriptor());
        workflowService.validateUpdateWorkflowNameAndDescription(jiraServiceContext, mockJiraWorkflow, "newWorkflow");
        assertTrue(jiraServiceContext.getErrorCollection().getErrors().get("newWorkflowName").equals(
                "admin.errors.a.workflow.with.this.name.already.exists"));
    }

    @Test
    public void testValidateUpdateWorkflow() throws WorkflowException {
        when(mockWorkflowManager.workflowExists("newWorkflow")).thenReturn(false);

        final DefaultWorkflowService workflowService = createWorkflowServiceOverridingPermissionCheck(true);

        final JiraServiceContext jiraServiceContext = createServiceContext();
        when(mockJiraWorkflow.getName()).thenReturn("workflowName");
        when(mockJiraWorkflow.isDraftWorkflow()).thenReturn(false);
        when(mockJiraWorkflow.isEditable()).thenReturn(true);
        when(mockJiraWorkflow.getDescriptor()).thenReturn(new DescriptorFactory().createWorkflowDescriptor());
        workflowService.validateUpdateWorkflowNameAndDescription(jiraServiceContext, mockJiraWorkflow, "newWorkflow");
        assertNoErrors(jiraServiceContext);
    }

    @Test
    public void testUpdateWorkflowNameNoAdminPermission() {
        final DefaultWorkflowService workflowService = createWorkflowServiceOverridingPermissionCheck(false);

        final JiraServiceContext jiraServiceContext = createServiceContext();
        workflowService.updateWorkflowNameAndDescription(jiraServiceContext, null, null, null);
        assertErrorMessage("admin.workflows.service.error.no.admin.permission", jiraServiceContext);
    }

    @Test
    public void testUpdateWorkflowName() {
        final DefaultWorkflowService workflowService = createWorkflowServiceOverridingPermissionCheck(true);

        final JiraServiceContext jiraServiceContext = createServiceContext();

        workflowService.updateWorkflowNameAndDescription(jiraServiceContext, mockJiraWorkflow, "newWorkflowName", "newDescription");
        assertNoErrors(jiraServiceContext);
        verify(mockWorkflowManager).updateWorkflowNameAndDescription(testUser, mockJiraWorkflow, "newWorkflowName", "newDescription");
    }

    @Test
    public void testValidateAddWorkflowTransitionToDraftNoAdminPermission() {
        final DefaultWorkflowService workflowService = createWorkflowServiceOverridingPermissionCheck(false);
        final JiraServiceContext jiraServiceContext = createServiceContext();
        workflowService.validateAddWorkflowTransitionToDraft(jiraServiceContext, null, 1);
        assertErrorMessage("admin.workflows.service.error.no.admin.permission", jiraServiceContext);
    }

    @Test
    public void testValidateAddWorkflowTransitionToDraftActiveWorkflow() {
        final DefaultWorkflowService workflowService = createWorkflowServiceOverridingPermissionCheck(true);

        when(mockJiraWorkflow.isDraftWorkflow()).thenReturn(false);

        final JiraServiceContext jiraServiceContext = createServiceContext();

        workflowService.validateAddWorkflowTransitionToDraft(jiraServiceContext, mockJiraWorkflow, 1);
        assertNoErrors(jiraServiceContext);
    }

    @Test
    public void testValidateAddWorkflowTransitionToDraftWorkflow() {
        final JiraServiceContext jiraServiceContext = createServiceContext();
        stubAndExecuteValidateAddWorkflowTransitionToDraftWorkflow(jiraServiceContext, Lists.newArrayList("Some dude", "blah"));
        assertNoErrors(jiraServiceContext);
    }

    @Test
    public void testValidateAddWorkflowTransitionToStatusWithNoOutgoingTransitions() {
        final ErrorCollection errorCollection = new SimpleErrorCollection();
        final JiraServiceContext jiraServiceContext = createServiceContext(errorCollection);
        stubAndExecuteValidateAddWorkflowTransitionToDraftWorkflow(jiraServiceContext, Collections.<String>emptyList());
        assertNoErrors(jiraServiceContext);
    }

    @Test
    public void testAdminHasEditWorkflowPermission() {
        final DefaultWorkflowService workflowService = createWorkflowServiceOverridingPermissionCheck(true);

        assertTrue("User with ADMINISTER permission should have access to edit workflow",
                workflowService.hasEditWorkflowPermission(testUser, mockJiraWorkflow));
    }

    @Test
    public void testProjectAdminHasEditWorkflowPermissionWhenHasEditWorkflowPermissionInAllProjects() {
        final DefaultWorkflowService workflowService = createWorkflowServiceOverridingPermissionCheck(false);

        setUpEditWorkflowPermissions(editWorkflowInProject(true), editWorkflowInProject(true));

        assertTrue("User with EDIT_WORKFLOW permission in all projects using permission should be able to edit it",
                workflowService.hasEditWorkflowPermission(testUser, mockJiraWorkflow));
    }

    @Test
    public void testProjectAdminHasNoEditWorkflowPermissionForSystemWorkflows() {
        final DefaultWorkflowService workflowService = createWorkflowServiceOverridingPermissionCheck(false);
        setUpEditWorkflowPermissions(editWorkflowInProject(true), editWorkflowInProject(true));
        when(mockJiraWorkflow.isSystemWorkflow()).thenReturn(true);

        assertFalse("User with EDIT_WORKFLOW permission should not be able to edit system workflows",
                workflowService.hasEditWorkflowPermission(testUser, mockJiraWorkflow));
    }

    @Test
    public void testProjectAdminNotHaveEditWorkflowPermissionWhenDoesntHaveEditWorkflowPermissionInAllProjects() {
        final DefaultWorkflowService workflowService = createWorkflowServiceOverridingPermissionCheck(false);

        setUpEditWorkflowPermissions(editWorkflowInProject(false), editWorkflowInProject(true));

        assertFalse("User without EDIT_WORKFLOW permission in all projects using permission should not be able to edit it",
                workflowService.hasEditWorkflowPermission(testUser, mockJiraWorkflow));
    }

    @Test
    public void testNullWorkflowShouldReturnEditPermissionForProjectLevelAdmin() {
        final DefaultWorkflowService workflowService = createWorkflowServiceWithRealPermissionCheck();

        assertFalse("No edit workflow permission is returned for null workflows and non global admin",
                workflowService.hasEditWorkflowPermission(testUser, null));
        verify(globalPermissionManager).hasPermission(GlobalPermissionKey.ADMINISTER, testUser);
        verifyZeroInteractions(workflowSchemeManager);
    }

    @Test
    public void testProjectAdminNotHaveEditWorkflowPermissionWhenWorkflowSchemeIsUnused() {
        final DefaultWorkflowService workflowService = createWorkflowServiceOverridingPermissionCheck(false);

        assertFalse("Workflow in schemes not used by any project should not be editable by non ADMINISTER user",
                workflowService.hasEditWorkflowPermission(testUser, mockJiraWorkflow));
    }

    @Test
    public void testProjectAdminNotHaveEditWorkflowPermissionWhenWorkflowIsUnused() {
        final DefaultWorkflowService workflowService = createWorkflowServiceOverridingPermissionCheck(false);

        setUpEditWorkflowPermissions(editWorkflowInProject(true), editWorkflowInProject(true));

        assertFalse("User should not have permission to edit unused workflow",
                workflowService.hasEditWorkflowPermission(testUser, mockNotUsedWorkflow));
        verify(projectManager).getProjects();
    }

    @Test
    public void testProjectAdminNotHaveEditWorkflowPermissionWhenWorkflowIsShared() {
        final DefaultWorkflowService workflowService = createWorkflowServiceOverridingPermissionCheck(false);

        setUpEditWorkflowPermissions(editWorkflowInProject(true), editWorkflowInProject(true));

        assertFalse("User should not have permission to edit shared workflow",
                workflowService.hasEditWorkflowPermission(testUser, mockSharedJiraWorkflow));
    }

    /**
     * Test for situation where:
     * Project1 (bug, task)
     * - bug -> workflow1
     * - task -> workflow1
     * - default -> workflow2
     * Project2 (bug)
     * - bug -> workflow2
     * - default -> workflow2
     *
     * In this situation effectively workflow2 is not used in Project1 (as there are no issue types without defined workflow) so workflow2 should not be treated as used in Project1 (shared).
     */
    @Test
    public void testWhenAllIssueTypesHaveAssignedWorkflowThenWorkflowForUnassignedIssueTypesShouldNotBeCountedAsShared() {
        final DefaultWorkflowService workflowService = createWorkflowServiceOverridingPermissionCheck(false);

        setUpEditWorkflowPermissions(editWorkflowInProject(true), editWorkflowInProject(true));

        assertTrue("User should have permission to edit shared workflow",
                workflowService.hasEditWorkflowPermission(testUser, mockNotSharedJiraWorkflow3));
    }

    @Test
    public void testWhenWorkflowIsBeingSharedByUsingDefaultWorkflowSetting() {
        final DefaultWorkflowService workflowService = createWorkflowServiceOverridingPermissionCheck(false);

        setUpEditWorkflowPermissions(editWorkflowInProject(true), editWorkflowInProject(true));

        assertFalse("User should not have permission to edit shared workflow",
                workflowService.hasEditWorkflowPermission(testUser, mockSharedJiraWorkflow2));
    }

    @Test
    public void testWhenDarkFeatureIsNotEnabledThenUserShouldHaveNoEditWorkflowPermission() {
        final DefaultWorkflowService workflowService = createWorkflowServiceOverridingPermissionCheck(false);

        when(featureManager.isEnabled(DefaultWorkflowService.PROJECT_LEVEL_ADMIN_EDIT_WORKFLOW_FEATURE)).thenReturn(false);

        assertFalse("User should not have permission to edit workflow if DF is off and user is not admin",
                workflowService.hasEditWorkflowPermission(testUser, null));
    }

    /**
     Sets up:
     Project1
     - bug -> workflowNotShared
     - task -> workflowShared
     - unassigned -> workflowShared

     Project2
     - bug -> workflowShared
     - task -> workflowShared
     - unassigned -> workflowShared

     Project3
     - bug -> workflowNotShared2
     - task -> unassigned
     - unassigned -> workflowShared2

     Project4:
     - bug -> workflowShared2
     - unassigned -> workflowNotShared3

     Project5:
     - bug -> unassigned
     - task -> unassigned
     - unassigned -> workflowNotShared3

     mockNotUsedWorkflow -> schemeNotUsed

     EDIT_WORKFLOW permission depends on passed parameters.
     @param project1EditWorkflowPermission
     @param project2EditWorkflowPermission
     */
    private void setUpEditWorkflowPermissions(boolean project1EditWorkflowPermission, boolean project2EditWorkflowPermission) {
        when(featureManager.isEnabled(DefaultWorkflowService.PROJECT_LEVEL_ADMIN_EDIT_WORKFLOW_FEATURE)).thenReturn(true);
        AssignableWorkflowScheme scheme1 = Mockito.mock(AssignableWorkflowScheme.class);
        AssignableWorkflowScheme scheme2 = Mockito.mock(AssignableWorkflowScheme.class);
        AssignableWorkflowScheme scheme3 = Mockito.mock(AssignableWorkflowScheme.class);
        AssignableWorkflowScheme scheme4 = Mockito.mock(AssignableWorkflowScheme.class);
        AssignableWorkflowScheme scheme5 = Mockito.mock(AssignableWorkflowScheme.class);
        AssignableWorkflowScheme schemeNotUsed = Mockito.mock(AssignableWorkflowScheme.class);
        Project project1 = Mockito.mock(Project.class);
        Project project2 = Mockito.mock(Project.class);
        Project project3 = Mockito.mock(Project.class);
        Project project4 = Mockito.mock(Project.class);
        Project project5 = Mockito.mock(Project.class);
        when(projectManager.getProjects()).thenReturn(ImmutableList.of(project1, project2, project3, project4, project5));
        when(mockJiraWorkflow.getName()).thenReturn("workflowNotShared");
        when(mockNotSharedJiraWorkflow2.getName()).thenReturn("workflowNotShared2");
        when(mockNotSharedJiraWorkflow3.getName()).thenReturn("workflowNotShared3");
        when(mockSharedJiraWorkflow.getName()).thenReturn("workflowShared");
        when(mockSharedJiraWorkflow2.getName()).thenReturn("workflowShared2");
        when(mockNotUsedWorkflow.getName()).thenReturn("workflowNotUsed");

        IssueType bug = issueType("Bug");
        IssueType task = issueType("Task");

        when(project1.getIssueTypes()).thenReturn(ImmutableList.of(bug, task));
        Map<String, String> workflowMap1 = buildMap("Bug", mockJiraWorkflow.getName(), "Task", mockSharedJiraWorkflow.getName(), null, mockSharedJiraWorkflow.getName());
        when(workflowSchemeManager.getWorkflowMap(project1)).thenReturn(workflowMap1);

        when(project2.getIssueTypes()).thenReturn(ImmutableList.of(bug, task));
        Map<String, String> workflowMap2 = buildMap("Bug", mockSharedJiraWorkflow.getName(), "Task", mockSharedJiraWorkflow.getName(), null, mockSharedJiraWorkflow.getName());
        when(workflowSchemeManager.getWorkflowMap(project2)).thenReturn(workflowMap2);

        when(project3.getIssueTypes()).thenReturn(ImmutableList.of(bug, task));
        Map<String, String> workflowMap3 = buildMap("Bug", mockNotSharedJiraWorkflow2.getName(), null, mockSharedJiraWorkflow2.getName());
        when(workflowSchemeManager.getWorkflowMap(project3)).thenReturn(workflowMap3);

        when(project4.getIssueTypes()).thenReturn(ImmutableList.of(bug));
        Map<String, String> workflowMap4 = buildMap("Bug", mockSharedJiraWorkflow2.getName(), null, mockNotSharedJiraWorkflow3.getName());
        when(workflowSchemeManager.getWorkflowMap(project4)).thenReturn(workflowMap4);

        when(project5.getIssueTypes()).thenReturn(ImmutableList.of(bug, task));
        Map<String, String> workflowMap5 = buildMap(null, mockNotSharedJiraWorkflow3.getName());
        when(workflowSchemeManager.getWorkflowMap(project5)).thenReturn(workflowMap5);

        when(project1.getId()).thenReturn(1l);
        when(project2.getId()).thenReturn(2l);
        when(project3.getId()).thenReturn(3l);
        when(project4.getId()).thenReturn(4l);
        when(project5.getId()).thenReturn(5l);

        when(workflowSchemeManager.getProjectsUsing(scheme1)).thenReturn(ImmutableList.of(project1));
        when(workflowSchemeManager.getProjectsUsing(scheme2)).thenReturn(ImmutableList.of(project2));
        when(workflowSchemeManager.getProjectsUsing(scheme3)).thenReturn(ImmutableList.of(project3));
        when(workflowSchemeManager.getProjectsUsing(scheme4)).thenReturn(ImmutableList.of(project4));
        when(workflowSchemeManager.getProjectsUsing(scheme5)).thenReturn(ImmutableList.of(project5));
        when(workflowSchemeManager.getProjectsUsing(schemeNotUsed)).thenReturn(ImmutableList.of());

        when(workflowSchemeManager.getSchemesForWorkflowIncludingDrafts(mockJiraWorkflow)).thenReturn(ImmutableList.of(scheme1));
        when(workflowSchemeManager.getSchemesForWorkflowIncludingDrafts(mockSharedJiraWorkflow)).thenReturn(ImmutableList.of(scheme1, scheme2));
        when(workflowSchemeManager.getSchemesForWorkflowIncludingDrafts(mockSharedJiraWorkflow2)).thenReturn(ImmutableList.of(scheme3, scheme4));
        when(workflowSchemeManager.getSchemesForWorkflowIncludingDrafts(mockNotSharedJiraWorkflow2)).thenReturn(ImmutableList.of(scheme3));
        when(workflowSchemeManager.getSchemesForWorkflowIncludingDrafts(mockNotSharedJiraWorkflow3)).thenReturn(ImmutableList.of(scheme4, scheme5));
        when(workflowSchemeManager.getSchemesForWorkflowIncludingDrafts(mockNotUsedWorkflow)).thenReturn(ImmutableList.of(schemeNotUsed));

        when(mockPermissionManager.hasPermission(ADMINISTER_PROJECTS, project1, testUser)).thenReturn(project1EditWorkflowPermission);
        when(mockPermissionManager.hasPermission(ADMINISTER_PROJECTS, project2, testUser)).thenReturn(project2EditWorkflowPermission);
        when(mockPermissionManager.hasPermission(ADMINISTER_PROJECTS, project3, testUser)).thenReturn(true);
        when(mockPermissionManager.hasPermission(ADMINISTER_PROJECTS, project4, testUser)).thenReturn(true);
        when(mockPermissionManager.hasPermission(ADMINISTER_PROJECTS, project5, testUser)).thenReturn(true);

    }

    private Map<String, String> buildMap(String... parameters) {
        final Map<String, String> map = new HashMap<>(parameters.length / 2);
        for (int i = 0; i < parameters.length; i += 2) {
            map.put(parameters[i], parameters[i + 1]);
        }
        return map;
    }

    private IssueType issueType(String id) {
        IssueType it = Mockito.mock(IssueType.class);
        when(it.getId()).thenReturn(id);
        return it;
    }

    private boolean admin(boolean isAdmin) {
        return isAdmin;
    }

    private boolean editWorkflowInProject(boolean editWorkflow) {
        return editWorkflow;
    }

    @SuppressWarnings("unchecked")
    private void stubAndExecuteValidateAddWorkflowTransitionToDraftWorkflow(final JiraServiceContext jiraServiceContext,
                                                                            final List<String> actions) {
        final DefaultWorkflowService workflowService = new DefaultWorkflowService(null, null, null, null, mockClusterLockService, globalPermissionManager, projectManager, featureManager, eventPublisher) {
            boolean hasAdminPermission(final JiraServiceContext jiraServiceContext) {
                return true;
            }

            boolean hasAdminPermission(final ApplicationUser user) {
                return true;
            }

            public JiraWorkflow getWorkflow(final JiraServiceContext jiraServiceContext, final String name) {
                assertEquals("Hamlet", name);
                final StepDescriptor mockStepDescriptor = new DescriptorFactory().createStepDescriptor();
                final WorkflowDescriptor mockWorkflowDescriptor = new DescriptorFactory().createWorkflowDescriptor();
                mockStepDescriptor.setId(120);
                mockStepDescriptor.getActions().addAll(actions);
                mockWorkflowDescriptor.addStep(mockStepDescriptor);
                when(mockJiraWorkflow.getDescriptor()).thenReturn(mockWorkflowDescriptor);
                return mockJiraWorkflow;
            }

            I18nHelper getI18nBean() {
                return mockI18nHelper;
            }
        };
        workflowService.start();
        when(mockDraftWorkflow.isDraftWorkflow()).thenReturn(true);
        when(mockDraftWorkflow.getName()).thenReturn("Hamlet");

        if (actions.isEmpty()) {
            final StepDescriptor mockNewStepDescriptor = new DescriptorFactory().createStepDescriptor();
            mockNewStepDescriptor.getActions().addAll(Arrays.asList("Some", "Actions"));
            mockNewStepDescriptor.setName("Gretel");
            mockNewStepDescriptor.setId(120);

            final WorkflowDescriptor mockNewWorkflowDescriptor = new DescriptorFactory().createWorkflowDescriptor();
            mockNewWorkflowDescriptor.addStep(mockNewStepDescriptor);
            when(mockDraftWorkflow.getDescriptor()).thenReturn(mockNewWorkflowDescriptor);
        }

        workflowService.validateAddWorkflowTransitionToDraft(jiraServiceContext, mockDraftWorkflow, 120);
    }

    private void assertReason(final ErrorCollection.Reason reason, final JiraServiceContext jiraServiceContext) {
        assertThat(jiraServiceContext.getErrorCollection().getReasons(),
                IsCollectionContaining.hasItem(reason));
    }
}
