package com.atlassian.jira.imports.project;

import com.atlassian.jira.exception.ParseException;
import com.atlassian.jira.external.beans.ExternalComponent;
import com.atlassian.jira.external.beans.ExternalProject;
import com.atlassian.jira.external.beans.ExternalVersion;
import com.atlassian.jira.imports.project.ao.handler.AoImportHandlerModuleDescriptor;
import com.atlassian.jira.imports.project.core.BackupProject;
import com.atlassian.jira.imports.project.core.BackupProjectImpl;
import com.atlassian.jira.imports.project.core.BackupSystemInformation;
import com.atlassian.jira.imports.project.core.BackupSystemInformationImpl;
import com.atlassian.jira.imports.project.core.ProjectImportData;
import com.atlassian.jira.imports.project.core.ProjectImportOptions;
import com.atlassian.jira.imports.project.core.ProjectImportOptionsImpl;
import com.atlassian.jira.imports.project.customfield.ExternalCustomFieldConfiguration;
import com.atlassian.jira.imports.project.handler.ChainedOfBizSaxHandler;
import com.atlassian.jira.imports.project.handler.CustomFieldMapperHandler;
import com.atlassian.jira.imports.project.handler.CustomFieldOptionsMapperHandler;
import com.atlassian.jira.imports.project.handler.GenericEntitiesPartitionHandler;
import com.atlassian.jira.imports.project.handler.GroupMapperHandler;
import com.atlassian.jira.imports.project.handler.ImportOfBizEntityHandler;
import com.atlassian.jira.imports.project.handler.IssueComponentMapperHandler;
import com.atlassian.jira.imports.project.handler.IssueLinkMapperHandler;
import com.atlassian.jira.imports.project.handler.IssueMapperHandler;
import com.atlassian.jira.imports.project.handler.IssuePartitionHandler;
import com.atlassian.jira.imports.project.handler.IssueRelatedEntitiesPartitionHandler;
import com.atlassian.jira.imports.project.handler.IssueTypeMapperHandler;
import com.atlassian.jira.imports.project.handler.IssueVersionMapperHandler;
import com.atlassian.jira.imports.project.handler.ProjectIssueSecurityLevelMapperHandler;
import com.atlassian.jira.imports.project.handler.ProjectMapperHandler;
import com.atlassian.jira.imports.project.handler.ProjectRoleActorMapperHandler;
import com.atlassian.jira.imports.project.handler.RegisterUserMapperHandler;
import com.atlassian.jira.imports.project.handler.RequiredProjectRolesMapperHandler;
import com.atlassian.jira.imports.project.handler.SimpleEntityMapperHandler;
import com.atlassian.jira.imports.project.handler.UserMapperHandler;
import com.atlassian.jira.imports.project.mapper.CustomFieldMapper;
import com.atlassian.jira.imports.project.mapper.ProjectImportMapper;
import com.atlassian.jira.imports.project.mapper.SimpleProjectImportIdMapper;
import com.atlassian.jira.imports.project.parser.AttachmentParser;
import com.atlassian.jira.imports.project.parser.ChangeGroupParser;
import com.atlassian.jira.imports.project.parser.ChangeItemParser;
import com.atlassian.jira.imports.project.parser.CommentParser;
import com.atlassian.jira.imports.project.parser.CustomFieldValueParser;
import com.atlassian.jira.imports.project.parser.CustomFieldValueParserImpl;
import com.atlassian.jira.imports.project.parser.EntityPropertyParser;
import com.atlassian.jira.imports.project.parser.IssueLinkParser;
import com.atlassian.jira.imports.project.parser.IssueParser;
import com.atlassian.jira.imports.project.parser.NodeAssociationParser;
import com.atlassian.jira.imports.project.parser.UserAssociationParser;
import com.atlassian.jira.imports.project.taskprogress.TaskProgressProcessor;
import com.atlassian.jira.imports.project.util.AoImportTemporaryFiles;
import com.atlassian.jira.imports.project.util.ProjectImportTemporaryFiles;
import com.atlassian.jira.imports.xml.BackupXmlParser;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.customfields.CustomFieldType;
import com.atlassian.jira.issue.label.OfBizLabelStore;
import com.atlassian.jira.issue.worklog.DatabaseWorklogStore;
import com.atlassian.jira.util.collect.MapBuilder;
import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.ModuleDescriptorFactory;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.predicate.ModuleDescriptorOfTypePredicate;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import org.apache.commons.io.FileUtils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.ofbiz.core.entity.DelegatorInterface;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.IOException;
import java.time.Clock;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static java.util.Collections.emptyList;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.when;

/**
 * @since v3.13
 */
public class TestDefaultProjectImportManager2 {
    private static final List<ExternalComponent> NO_COMPONENTS = emptyList();
    private static final List<ExternalCustomFieldConfiguration> NO_CUSTOM_FIELD_CONFIGURATIONS = emptyList();
    private static final List<ExternalVersion> NO_VERSIONS = emptyList();

    private final List<ImportOfBizEntityHandler> allImportOfBizHandlers = new ArrayList<>();

    @Mock
    private ModuleDescriptorFactory mockModuleDescriptorFactory;
    @Mock
    private PluginAccessor mockPluginAccessor;
    @Mock
    private AoImportTemporaryFiles aoImportTemporaryFiles;
    private MapBuilder<String, CustomFieldValueParser> entities = MapBuilder.newBuilder();
    @Mock
    private DelegatorInterface mockDelegator;
    @Mock
    private CustomFieldManager mockCustomFieldManager;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        when(mockDelegator.getDelegatorName()).thenReturn("default");
        entities.add(CustomFieldValueParser.CUSTOM_FIELD_VALUE_ENTITY_NAME, new CustomFieldValueParserImpl());

        when(mockModuleDescriptorFactory.getModuleDescriptorClass(anyString())).thenAnswer(new Answer<Class<? extends ModuleDescriptor>>() {
            @Override
            public Class<? extends ModuleDescriptor> answer(InvocationOnMock invocationOnMock) throws Throwable {
                return AoImportHandlerModuleDescriptor.class;
            }
        });
        when(mockPluginAccessor.getModuleDescriptors(any(ModuleDescriptorOfTypePredicate.class))).thenReturn(ImmutableList.of());
        when(mockCustomFieldManager.getCustomFieldTypes()).thenReturn(ImmutableList.<CustomFieldType<?, ?>>of());

    }

    @Test
    public void testXmlPartitioningHappyPath() throws IOException, SAXException, ParseException {
        // This test is to assure that all the handlers we expect to get correctly registered

        final BackupProject backupProject = new BackupProjectImpl(new ExternalProject(), NO_VERSIONS, NO_COMPONENTS,
                NO_CUSTOM_FIELD_CONFIGURATIONS, Collections.<Long>emptyList(), 0, ImmutableMap.of());
        final String fileName = "/test/path";
        final ProjectImportOptionsImpl projectImportOptions = new ProjectImportOptionsImpl(fileName, fileName);
        final Set<String> entitySet = ImmutableSet.of(
                IssueParser.ISSUE_ENTITY_NAME, CustomFieldValueParser.CUSTOM_FIELD_VALUE_ENTITY_NAME,
                DatabaseWorklogStore.WORKLOG_ENTITY, OfBizLabelStore.TABLE, NodeAssociationParser.NODE_ASSOCIATION_ENTITY_NAME,
                IssueLinkParser.ISSUE_LINK_ENTITY_NAME, CommentParser.COMMENT_ENTITY_NAME, ChangeGroupParser.CHANGE_GROUP_ENTITY_NAME,
                ChangeItemParser.CHANGE_ITEM_ENTITY_NAME, UserAssociationParser.USER_ASSOCIATION_ENTITY_NAME,
                EntityPropertyParser.ENTITY_PROPERTY_ENTITY_NAME, AttachmentParser.ATTACHMENT_ENTITY_NAME);
        final BackupSystemInformation backupSystemInformation = new BackupSystemInformationImpl("123", "Prof",
                Collections.emptyList(), true, Collections.emptyMap(), entitySet, 0);

        final IssuePartitionHandler issuePartitonHandler = makeImportOfbizHandler(IssuePartitionHandler.class);
        final IssueRelatedEntitiesPartitionHandler issueRelatedEntitiesPartionHandler = makeImportOfbizHandler(IssueRelatedEntitiesPartitionHandler.class);
        final IssueRelatedEntitiesPartitionHandler customFieldValuesPartitionHandler = makeImportOfbizHandler(IssueRelatedEntitiesPartitionHandler.class);
        final IssueRelatedEntitiesPartitionHandler fileAttachmentPartitionHandler = makeImportOfbizHandler(IssueRelatedEntitiesPartitionHandler.class);

        final ChainedOfBizSaxHandler mockChainedOfBizSaxHandler = Mockito.mock(ChainedOfBizSaxHandler.class);
        mockChainedOfBizSaxHandler.registerHandler(issuePartitonHandler);
        mockChainedOfBizSaxHandler.registerHandler(customFieldValuesPartitionHandler);
        mockChainedOfBizSaxHandler.registerHandler(issueRelatedEntitiesPartionHandler);
        mockChainedOfBizSaxHandler.registerHandler(fileAttachmentPartitionHandler);

        final GenericEntitiesPartitionHandler genericEntitiesPartitionHandler = makeImportOfbizHandler(GenericEntitiesPartitionHandler.class);
        final UserMapperHandler userMapperHandler = makeImportOfbizHandler(UserMapperHandler.class);
        final GroupMapperHandler groupMapperHandler = makeImportOfbizHandler(GroupMapperHandler.class);
        final IssueMapperHandler issueMapperHandler = makeImportOfbizHandler(IssueMapperHandler.class);
        final ProjectIssueSecurityLevelMapperHandler securityLevelMapperHandler = makeImportOfbizHandler(ProjectIssueSecurityLevelMapperHandler.class);
        final IssueTypeMapperHandler issueTypeHandler = makeImportOfbizHandler(IssueTypeMapperHandler.class);
        final SimpleEntityMapperHandler priorityHandler = makeImportOfbizHandler(SimpleEntityMapperHandler.class);
        final SimpleEntityMapperHandler resolutionHandler = makeImportOfbizHandler(SimpleEntityMapperHandler.class);
        final SimpleEntityMapperHandler statusHandler = makeImportOfbizHandler(SimpleEntityMapperHandler.class);
        final CustomFieldMapperHandler customFieldHandler = makeImportOfbizHandler(CustomFieldMapperHandler.class);
        final ProjectMapperHandler projectMapperHandler = makeImportOfbizHandler(ProjectMapperHandler.class);
        final CustomFieldOptionsMapperHandler customFieldOptionMapperHandler = makeImportOfbizHandler(CustomFieldOptionsMapperHandler.class);
        final SimpleEntityMapperHandler projectRoleRegistrationMapperHandler = makeImportOfbizHandler(SimpleEntityMapperHandler.class);
        final RequiredProjectRolesMapperHandler requiredProjectRolesMapperHandler = makeImportOfbizHandler(RequiredProjectRolesMapperHandler.class);
        final IssueVersionMapperHandler issueVersionMapperHandler = makeImportOfbizHandler(IssueVersionMapperHandler.class);
        final IssueComponentMapperHandler issueComponentMapperHandler = makeImportOfbizHandler(IssueComponentMapperHandler.class);
        final IssueLinkMapperHandler issueLinkMapperHandler = makeImportOfbizHandler(IssueLinkMapperHandler.class);
        final RegisterUserMapperHandler registerUserMapperHandler = makeImportOfbizHandler(RegisterUserMapperHandler.class);
        final ProjectRoleActorMapperHandler projectRoleActorMapperHandler = makeImportOfbizHandler(ProjectRoleActorMapperHandler.class);

        final BackupXmlParser mockBackupXmlParser = Mockito.mock(BackupXmlParser.class);

        final DefaultProjectImportManager defaultProjectImportManager = new DefaultProjectImportManager(
                mockBackupXmlParser, mockDelegator, null, null, null, mockCustomFieldManager, null, null, null, null, null, null, null, null, null, mockPluginAccessor, Clock.systemUTC()) {
            @Override
            String getApplicationEncoding() {
                return "UTF-8";
            }

            @Override
            IssuePartitionHandler getIssuePartitioner(final ProjectImportTemporaryFiles projectImportTemporaryFiles,
                                                      final BackupProject backupProject) {
                return issuePartitonHandler;
            }

            @Override
            IssueRelatedEntitiesPartitionHandler getIssueRelatedEntitesHandler(final ProjectImportTemporaryFiles projectImportTemporaryFiles,
                                                                               final BackupProject backupProject) {
                return issueRelatedEntitiesPartionHandler;
            }

            @Override
            IssueRelatedEntitiesPartitionHandler getCustomFieldValuesHandler(final ProjectImportTemporaryFiles projectImportTemporaryFiles,
                                                                             final BackupProject backupProject) {
                return customFieldValuesPartitionHandler;
            }

            @Override
            IssueRelatedEntitiesPartitionHandler getFileAttachmentHandler(final ProjectImportTemporaryFiles projectImportTemporaryFiles,
                                                                          final BackupProject backupProject) {
                return fileAttachmentPartitionHandler;
            }

            @Override
            ChainedOfBizSaxHandler getOfBizChainedHandler(final TaskProgressProcessor taskProgressProcessor) {
                return mockChainedOfBizSaxHandler;
            }

            @Override
            IssueMapperHandler getIssueMapperHandler(final BackupProject backupProject, final ProjectImportMapper projectImportMapper) {
                return issueMapperHandler;
            }

            @Override
            IssueTypeMapperHandler getIssueTypeMapperHandler(final ProjectImportMapper projectImportMapper) {
                return issueTypeHandler;
            }

            @Override
            SimpleEntityMapperHandler getPriorityMapperHandler(final ProjectImportMapper projectImportMapper) {
                return priorityHandler;
            }

            @Override
            ProjectIssueSecurityLevelMapperHandler getProjectIssueSecurityLevelMapperHandler(final BackupProject backupProject, final ProjectImportMapper projectImportMapper) {
                return securityLevelMapperHandler;
            }

            @Override
            SimpleEntityMapperHandler getResolutionMapperHandler(final ProjectImportMapper projectImportMapper) {
                return resolutionHandler;
            }

            @Override
            SimpleEntityMapperHandler getStatusMapperHandler(final ProjectImportMapper projectImportMapper) {
                return statusHandler;
            }

            @Override
            UserMapperHandler getUserMapperHandler(final ProjectImportOptions projectImportOptions, final BackupProject backupProject, final ProjectImportMapper projectImportMapper) {
                return userMapperHandler;
            }

            @Override
            GroupMapperHandler getGroupMapperHandler(final BackupProject backupProject, final ProjectImportMapper projectImportMapper) {
                return groupMapperHandler;
            }

            @Override
            CustomFieldMapperHandler getCustomFieldMapperHandler(final BackupProject backupProject, final ProjectImportMapper projectImportMapper) {
                return customFieldHandler;
            }

            @Override
            ProjectMapperHandler getProjectMapperHandler(final ProjectImportMapper projectImportMapper) {
                return projectMapperHandler;
            }

            @Override
            CustomFieldOptionsMapperHandler getCustomFieldOptionMapperHandler(final ProjectImportMapper projectImportMapper) {
                return customFieldOptionMapperHandler;
            }

            @Override
            SimpleEntityMapperHandler getProjectRoleRegistrationHandler(final ProjectImportMapper projectImportMapper) {
                return projectRoleRegistrationMapperHandler;
            }

            @Override
            RequiredProjectRolesMapperHandler getRequiredProjectRolesMapperHandler(final BackupProject backupProject, final ProjectImportMapper projectImportMapper) {
                return requiredProjectRolesMapperHandler;
            }

            @Override
            IssueVersionMapperHandler getIssueVersionMapperHandler(final BackupProject backupProject, final ProjectImportMapper projectImportMapper) {
                return issueVersionMapperHandler;
            }

            @Override
            IssueComponentMapperHandler getIssueComponentMapperHandler(final BackupProject backupProject, final ProjectImportMapper projectImportMapper) {
                return issueComponentMapperHandler;
            }

            @Override
            IssueLinkMapperHandler getIssueLinkMapperHandler(final BackupProject backupProject, final BackupSystemInformation backupSystemInformation, final ProjectImportMapper projectImportMapper) {
                return issueLinkMapperHandler;
            }

            @Override
            RegisterUserMapperHandler getRegisterUserMapperHandler(final ProjectImportMapper projectImportMapper) {
                return registerUserMapperHandler;
            }

            @Override
            void populateCustomFieldMapperOldValues(final BackupProject backupProject, final CustomFieldMapper customFieldMapper) {
                // do nothing
            }

            @Override
            void populateVersionMapper(final SimpleProjectImportIdMapper versionMapper, final Map newVersions) {
                // do nothing
            }

            @Override
            void populateComponentMapper(final SimpleProjectImportIdMapper componentMapper, final Map newComponents) {
                // do nothing
            }

            @Override
            ProjectRoleActorMapperHandler getProjectRoleActorMapperHandler(final BackupProject backupProject, final ProjectImportMapper projectImportMapper) {
                return projectRoleActorMapperHandler;
            }

            @Override
            GenericEntitiesPartitionHandler getGenericEntitesHandler(final ProjectImportTemporaryFiles projectImportTemporaryFiles, final BackupProject backupProject, final BackupSystemInformation backupSystemInformation)
                    throws IOException {
                return genericEntitiesPartitionHandler;
            }

        };
        ProjectImportData backupParsingResult = null;
        try {
            backupParsingResult = defaultProjectImportManager.getProjectImportData(projectImportOptions, backupProject,
                    backupSystemInformation, null);

            // Test that the files exist
            for (String entityName : entitySet) {
                assertTrue(new File(backupParsingResult.getPathToEntityXml(entityName)).exists());
            }

            for (ImportOfBizEntityHandler handler : allImportOfBizHandlers) {
                Mockito.verify(mockChainedOfBizSaxHandler, atLeastOnce()).registerHandler(handler);
            }
        } finally {
            if (backupParsingResult != null) {
                for (String entityName : entitySet) {
                    FileUtils.deleteQuietly(new File(backupParsingResult.getPathToEntityXml(entityName)));
                }
            }
        }
    }

    private <T extends ImportOfBizEntityHandler> T makeImportOfbizHandler(final Class<T> handlerClass) {
        T handler = Mockito.mock(handlerClass);
        allImportOfBizHandlers.add(handler);
        return handler;
    }
}
