package com.atlassian.jira.help;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.jira.util.JiraProductInformation;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

public class BootstrapHelpUrlsApplicationKeyProviderTest {
    @Rule
    public MockitoRule initMockito = MockitoJUnit.rule();

    @Mock
    private JiraProductInformation jiraProductInformation;

    @InjectMocks
    BootstrapHelpUrlsApplicationKeyProvider provider;

    @Test
    public void getApplicationKeyForUserReadsApplicationKeyFromJiraProductInformation() throws Exception {
        final ApplicationKey key = ApplicationKey.valueOf("some-app");

        when(jiraProductInformation.getSetupHelpApplicationKey()).thenReturn("some-app");

        assertThat(provider.getApplicationKeyForUser(), is(key));
    }

}