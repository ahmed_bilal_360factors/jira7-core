package com.atlassian.jira.plugin.webfragment.conditions;

import com.atlassian.jira.bc.issue.worklog.WorklogService;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestTimeTrackingEnabledCondition {
    @Test
    public void testTrueWhenTimeTrackingEnabled() {
        final WorklogService worklogService = mock(WorklogService.class);

        when(worklogService.isTimeTrackingEnabled()).thenReturn(true);

        final TimeTrackingEnabledCondition condition = new TimeTrackingEnabledCondition(worklogService);

        assertTrue(condition.shouldDisplay(null, null));
    }

    @Test
    public void testFalseWhenTimeTrackingDisabled() {
        final WorklogService worklogService = mock(WorklogService.class);

        when(worklogService.isTimeTrackingEnabled()).thenReturn(false);

        final TimeTrackingEnabledCondition condition = new TimeTrackingEnabledCondition(worklogService);

        assertFalse(condition.shouldDisplay(null, null));
    }

}
