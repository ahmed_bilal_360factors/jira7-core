package com.atlassian.jira.project;

import com.atlassian.jira.database.MockQueryDslAccessor;
import com.atlassian.jira.database.ResultRow;
import com.atlassian.jira.junit.rules.MockitoContainer;
import com.atlassian.jira.model.querydsl.ProjectChangedTimeDTO;
import com.google.common.collect.ImmutableList;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.sql.Timestamp;
import java.util.Optional;

import static java.util.Collections.emptyList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class TestProjectChangedTimeStoreImpl {

    private static final Timestamp ISSUE_CHANGED_TIME = new Timestamp(116, 1, 24, 16, 55, 0, 0);
    private static final long PROJECT_ID = 1L;
    private static final String GET_PROJECT_CHANGED_TIME_SQL = "select PROJECT_CHANGED_TIME.project_id, " +
            "PROJECT_CHANGED_TIME.issue_changed_time\n" +
            "from projectchangedtime PROJECT_CHANGED_TIME\n" +
            "where PROJECT_CHANGED_TIME.project_id = 1\n" +
            "limit 2";
    private static final String UPDATE_PROJECT_CHANGED_TIME_SQL = "update projectchangedtime\n" +
            "set issue_changed_time = 2016-02-24 16:55:00.0\n" +
            "where projectchangedtime.project_id = 1";
    private static final String INSERT_PROJECT_CHANGED_TIME_SQL = "insert into projectchangedtime (project_id, " +
            "issue_changed_time)\n" +
            "values (1, 2016-02-24 16:55:00.0)";

    @Rule
    public MockitoContainer mockitoContainer = new MockitoContainer(this);
    private MockQueryDslAccessor queryDslAccessor = new MockQueryDslAccessor();
    private ProjectChangedTimeStore projectChangedTimeStore;

    @Before
    public void setUp() {
        projectChangedTimeStore = new ProjectChangedTimeStoreImpl(queryDslAccessor);
    }

    @Test
    public void shouldReturnProjectUpdatedIfRecordExistsForAGivenProject() {
        queryDslAccessor.setQueryResults(GET_PROJECT_CHANGED_TIME_SQL, ImmutableList.of(new ResultRow(PROJECT_ID, ISSUE_CHANGED_TIME)));
        ProjectChangedTime expectedProjectChangedTime = new ProjectChangedTimeImpl(new ProjectChangedTimeDTO(PROJECT_ID, ISSUE_CHANGED_TIME));

        Optional<ProjectChangedTime> actualProjectUpdated = projectChangedTimeStore.getProjectChangedTime(PROJECT_ID);

        assertThat(actualProjectUpdated.get(), is(expectedProjectChangedTime));
        queryDslAccessor.assertAllExpectedStatementsWereRun();
    }

    @Test
    public void shouldReturnEmptyOptionalIfNoRecordExistsForTheProject() {
        queryDslAccessor.setQueryResults(GET_PROJECT_CHANGED_TIME_SQL, emptyList());

        Optional<ProjectChangedTime> actualProjectUpdated = projectChangedTimeStore.getProjectChangedTime(PROJECT_ID);

        assertThat(actualProjectUpdated.isPresent(), is(false));
        queryDslAccessor.assertAllExpectedStatementsWereRun();
    }

    @Test
    public void shouldUpdateProjectUpdatedIfRecordExistsForAGivenProject() {
        queryDslAccessor.setUpdateResults(UPDATE_PROJECT_CHANGED_TIME_SQL, 1);

        projectChangedTimeStore.updateOrAddIssueChangedTime(PROJECT_ID, ISSUE_CHANGED_TIME);

        queryDslAccessor.assertAllExpectedStatementsWereRun();
    }

    @Test
    public void shouldInsertARecordIfNoRecordExistsForAGivenProject() {
        queryDslAccessor.setUpdateResults(UPDATE_PROJECT_CHANGED_TIME_SQL, 0);
        queryDslAccessor.setUpdateResults(INSERT_PROJECT_CHANGED_TIME_SQL, 1);

        projectChangedTimeStore.updateOrAddIssueChangedTime(PROJECT_ID, ISSUE_CHANGED_TIME);

        queryDslAccessor.assertAllExpectedStatementsWereRun();
    }
}
