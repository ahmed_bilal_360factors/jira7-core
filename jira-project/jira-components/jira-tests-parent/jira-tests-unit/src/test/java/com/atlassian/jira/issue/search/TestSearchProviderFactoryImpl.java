package com.atlassian.jira.issue.search;

import com.atlassian.jira.index.LuceneVersion;
import com.atlassian.jira.issue.index.IssueIndexManager;

import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.store.RAMDirectory;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.io.IOException;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestSearchProviderFactoryImpl {
    @Rule
    public ExpectedException exception = ExpectedException.none();

    private SearchProviderFactoryImpl searchProviderFactory;
    private IssueIndexManager issueIndexManager;

    @Before
    public void setUp() throws Exception {
        issueIndexManager = mock(IssueIndexManager.class);
        searchProviderFactory = new SearchProviderFactoryImpl(issueIndexManager);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testGetNullSearcher() {
        searchProviderFactory.getSearcher(null);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testGetEmptySearcher() {
        searchProviderFactory.getSearcher("");
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testGetUnknownSearcher() {
        searchProviderFactory.getSearcher("hi there");
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testGetBlahSearcher() {
        searchProviderFactory.getSearcher("blah");
    }

    @Test
    public void testGetIssueSearcher() throws Exception {
        final IndexSearcher expectedSearcher = createIndexSearcher();
        when(issueIndexManager.getIssueSearcher()).thenReturn(expectedSearcher);

        final IndexSearcher searcher = searchProviderFactory.getSearcher(SearchProviderFactory.ISSUE_INDEX);
        assertThat(searcher, equalTo(expectedSearcher));

        expectedSearcher.close();
    }

    @Test
    public void testGetSearcherThrowsSameException() throws Exception {
        final IllegalMonitorStateException expectedException = new IllegalMonitorStateException("something went wrong");
        exception.expect(equalTo(expectedException));

        when(issueIndexManager.getIssueSearcher()).thenThrow(expectedException);
        searchProviderFactory.getSearcher(SearchProviderFactory.ISSUE_INDEX);
    }

    @Test
    public void testGetSearcherThrowsSomeRuntimeException() throws Exception {
        final NullPointerException expectedException = new NullPointerException("something went wrong");
        exception.expect(equalTo(expectedException));

        when(issueIndexManager.getIssueSearcher()).thenThrow(expectedException);
        searchProviderFactory.getSearcher(SearchProviderFactory.ISSUE_INDEX);
    }

    @Test
    public void testGetCommentSearcher() throws Exception {
        final IndexSearcher expectedSearcher = createIndexSearcher();
        when(issueIndexManager.getCommentSearcher()).thenReturn(expectedSearcher);

        final IndexSearcher searcher = searchProviderFactory.getSearcher(SearchProviderFactory.COMMENT_INDEX);

        assertThat(searcher, equalTo(expectedSearcher));
        expectedSearcher.close();
    }

    @Test
    public void testGetCommentSearcherThrowsSameException() throws Exception {
        final IllegalMonitorStateException expectedException = new IllegalMonitorStateException("something went wrong");
        exception.expect(equalTo(expectedException));

        when(issueIndexManager.getCommentSearcher()).thenThrow(expectedException);
        searchProviderFactory.getSearcher(SearchProviderFactory.COMMENT_INDEX);
    }

    @Test
    public void testGetCommentSearcherThrowsSomeRuntimeException() throws Exception {
        final NullPointerException expectedException = new NullPointerException("something went wrong");
        exception.expect(equalTo(expectedException));

        when(issueIndexManager.getCommentSearcher()).thenThrow(expectedException);
        searchProviderFactory.getSearcher(SearchProviderFactory.COMMENT_INDEX);
    }

    private IndexSearcher createIndexSearcher() throws IOException {
        final RAMDirectory directory = new RAMDirectory();
        IndexWriterConfig conf = new IndexWriterConfig(LuceneVersion.get(), null);
        conf.setOpenMode(IndexWriterConfig.OpenMode.CREATE);
        new IndexWriter(directory, conf).close();
        return new IndexSearcher(directory);
    }
}
