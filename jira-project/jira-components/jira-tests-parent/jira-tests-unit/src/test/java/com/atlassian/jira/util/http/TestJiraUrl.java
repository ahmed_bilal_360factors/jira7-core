package com.atlassian.jira.util.http;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import javax.servlet.http.HttpServletRequest;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

public class TestJiraUrl {
    @Rule
    public final MethodRule initMockito = MockitoJUnit.rule();

    @Mock
    private HttpServletRequest servletRequest;

    @Test
    public void testConstructBaseUrl() {
        when(servletRequest.getScheme()).thenReturn("http");
        when(servletRequest.getServerName()).thenReturn("jira.atlassian.com");
        when(servletRequest.getServerPort()).thenReturn(80);
        when(servletRequest.getContextPath()).thenReturn("/");

        final String baseUrl = JiraUrl.constructBaseUrl(servletRequest);
        assertEquals("http://jira.atlassian.com/", baseUrl);
    }

    @Test
    public void testConstructBaseUrlForHttps() {
        when(servletRequest.getScheme()).thenReturn("https");
        when(servletRequest.getServerName()).thenReturn("extranet.atlassian.com");
        when(servletRequest.getServerPort()).thenReturn(443);
        when(servletRequest.getContextPath()).thenReturn("/jira");

        final String baseUrl = JiraUrl.constructBaseUrl(servletRequest);
        assertEquals("https://extranet.atlassian.com/jira", baseUrl);
    }

    @Test
    public void testConstructBaseUrlForNonStandardPort() {
        when(servletRequest.getScheme()).thenReturn("http");
        when(servletRequest.getServerName()).thenReturn("localhost");
        when(servletRequest.getServerPort()).thenReturn(8080);
        when(servletRequest.getContextPath()).thenReturn("/atlassian-jira");

        final String baseUrl = JiraUrl.constructBaseUrl(servletRequest);
        assertEquals("http://localhost:8080/atlassian-jira", baseUrl);
    }

    @Test
    public void testConstructBaseUrlFor443onHttp() {
        when(servletRequest.getScheme()).thenReturn("http");
        when(servletRequest.getServerName()).thenReturn("stuffed.up.com");
        when(servletRequest.getServerPort()).thenReturn(443);
        when(servletRequest.getContextPath()).thenReturn("/atlassian-jira");

        final String baseUrl = JiraUrl.constructBaseUrl(servletRequest);
        assertEquals("http://stuffed.up.com:443/atlassian-jira", baseUrl);
    }

}
