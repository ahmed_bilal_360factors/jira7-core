package com.atlassian.jira.help;

import com.atlassian.fugue.Option;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

/**
 * @since v6.2.4
 */
public class HelpUrlBuilderFactoryTemplateTest {
    private static final String DOCS_VERSION = "071";
    private static final String APPLICATION_HELP_SPACE = "help-space-072";

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @Test
    public void getCreatesBuilderWithRightPrefixAndSuffix() {
        final FactoryForTest factory = getFactoryForTest();
        final MockHelpUrlBuilder builder = (MockHelpUrlBuilder) factory.get("prefix", ".suffix");

        assertThat(builder.prefix(), equalTo("prefix"));
        assertThat(builder.suffix(), equalTo(".suffix"));
    }

    @Test
    public void getSubstitutesDocVersionIntoPrefix() {
        testSubstitutesDocVersionIntoPrefix("http://localhost/${docs.version}", "http://localhost/" + DOCS_VERSION);
    }

    @Test
    public void getSubstitutesApplicationHelpSpaceVersionIntoPrefix() {
        testSubstitutesDocVersionIntoPrefix("http://localhost/${application.help.space}", "http://localhost/" + APPLICATION_HELP_SPACE);
    }

    @Test
    public void getSubstitutesDocVersionAndApplicationHelpSpaceVersionIntoPrefix() {
        testSubstitutesDocVersionIntoPrefix("http://localhost/${application.help.space}/${docs.version}",
                "http://localhost/" + APPLICATION_HELP_SPACE + "/" + DOCS_VERSION);
    }

    @Test
    public void getDoesNotSubstituteUnkonwnVariables() {
        testSubstitutesDocVersionIntoPrefix("http://localhost/${jcore.docs.version}",
                "http://localhost/${jcore.docs.version}");
    }

    public void testSubstitutesDocVersionIntoPrefix(final String prefix, final String expectedGetPrefix) {
        final FactoryForTest factory = getFactoryForTest();
        MockHelpUrlBuilder builder = (MockHelpUrlBuilder) factory.get(prefix, ".html");

        assertThat(builder.prefix(), equalTo(expectedGetPrefix));
        assertThat(builder.suffix(), equalTo(".html"));
    }

    private FactoryForTest getFactoryForTest() {
        return new FactoryForTest(DOCS_VERSION, APPLICATION_HELP_SPACE);
    }

    private static class FactoryForTest extends HelpUrlBuilderFactoryTemplate {
        FactoryForTest(final String docsVersion, final String applicationHelpSpace) {
            super(docsVersion, Option.option(applicationHelpSpace));
        }

        @Override
        HelpUrlBuilder newUrlBuilder(final String prefix, final String suffix) {
            return new MockHelpUrlBuilder(prefix, suffix);
        }
    }
}
