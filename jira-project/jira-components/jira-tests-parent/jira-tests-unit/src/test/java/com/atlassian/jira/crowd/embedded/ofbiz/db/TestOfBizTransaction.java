package com.atlassian.jira.crowd.embedded.ofbiz.db;

import com.atlassian.jira.crowd.embedded.ofbiz.AbstractTransactionalOfBizTestCase;
import org.junit.Test;
import org.ofbiz.core.entity.GenericTransactionException;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

import static org.hamcrest.Matchers.arrayWithSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

/**
 * No exceptions left behind!  Test various error situations, ensure the right exceptions are propagated to the
 * withTransaction() caller and that any subsequent exceptions are added as suppressed exceptions to the original
 * when appropriate.
 */
public class TestOfBizTransaction extends AbstractTransactionalOfBizTestCase {
    /**
     * A successful body code run should commit the transaction.
     */
    @Test
    public void testGoodRunWithoutErrors() {
        AtomicInteger counter = new AtomicInteger();
        DefaultOfBizTransaction transaction = DefaultOfBizTransaction.withTransaction(t ->
        {
            //Verify we have a real transaction and it is not yet processed
            assertThat(t.isProcessed(), is(false));

            counter.incrementAndGet();

            return t;
        });

        //We should get here with no errors, the body executed once
        assertThat(counter.get(), is(1));

        //Check the transaction
        assertThat(transaction.isProcessed(), is(true));
        assertThat(transaction.getStatus(), is(DefaultOfBizTransaction.Status.COMMITTED));
    }

    /**
     * Body code fails.  Should rollback transaction and throw original exception.
     */
    @Test
    public void testRolledBackWhenBodyFails() {
        RuntimeException err = new RuntimeException();

        AtomicInteger counter = new AtomicInteger();
        AtomicReference<DefaultOfBizTransaction> transactionHolder = new AtomicReference<>();
        try {
            DefaultOfBizTransaction.withTransaction(t ->
            {
                //Verify we have a real transaction and it is not yet processed
                assertThat(t.isProcessed(), is(false));

                counter.incrementAndGet();
                transactionHolder.set(t);

                //Hackery to choose the right overload of withTransaction() (otherwise we have ambiguous lambda)
                //Normally you would never write a lambda that always throws for a withTransaction() anyway
                if (true) {
                    throw err;
                }

                return t;
            });

            fail("Exception should have been propagated.");
        } catch (RuntimeException e) {
            //Expected, should be propogated
            assertThat(e, sameInstance(err));
        }

        DefaultOfBizTransaction transaction = transactionHolder.get();

        //We should get here with no errors, the body executed once
        assertThat(counter.get(), is(1));

        //Check the transaction
        assertThat(transaction.isProcessed(), is(true));
        assertThat(transaction.getStatus(), is(DefaultOfBizTransaction.Status.ROLLED_BACK));
    }

    /**
     * Body code fails, rollback also fails.  Original exception should be thrown with rollback's exception added
     * as a suppressed exception.
     */
    @Test
    public void testOriginalExceptionNotSwallowedWhenRollbackAlsoFails() {
        RuntimeException err = new RuntimeException();

        AtomicInteger counter = new AtomicInteger();
        AtomicReference<DefaultOfBizTransaction> transactionHolder = new AtomicReference<>();
        try {
            DefaultOfBizTransaction.withTransaction(new FailingRollbackTransactionUtil(), t ->
            {
                //Verify we have a real transaction and it is not yet processed
                assertThat(t.isProcessed(), is(false));

                counter.incrementAndGet();
                transactionHolder.set(t);

                throw err;
            });

            fail("Exception should have been propagated.");
        } catch (RuntimeException e) {
            //Expected, should be propagated
            assertThat(e, sameInstance(err));

            //Exception from failing rollback should not be lost
            assertThat(e.getSuppressed(), arrayWithSize(1));
        }

        DefaultOfBizTransaction transaction = transactionHolder.get();

        //We should get here with no errors, the body executed once
        assertThat(counter.get(), is(1));

        //Check the transaction - should not be processed since rollback failed
        assertThat(transaction.isProcessed(), is(false));
        assertThat(transaction.getStatus(), is(DefaultOfBizTransaction.Status.UNPROCESSED));
    }

    /**
     * Body code succeeds but commit fails, should throw exception from failed commit and rollback.
     */
    @Test
    public void testGoodRunButFailsCommit() {
        AtomicInteger counter = new AtomicInteger();
        AtomicReference<DefaultOfBizTransaction> transactionHolder = new AtomicReference<>();
        try {
            DefaultOfBizTransaction.withTransaction(new FailingCommitTransactionUtil(), t ->
            {
                //Verify we have a real transaction and it is not yet processed
                assertThat(t.isProcessed(), is(false));

                counter.incrementAndGet();
                transactionHolder.set(t);

                return null;
            });

            fail("Commit should have thrown exception.");
        } catch (OfBizTransaction.TransactionException e) {
            //This is expected
        }

        DefaultOfBizTransaction transaction = transactionHolder.get();

        //The body executed once
        assertThat(counter.get(), is(1));

        //Commit failed so it attempted to roll back afterwards
        assertThat(transaction.isProcessed(), is(true));
        assertThat(transaction.getStatus(), is(DefaultOfBizTransaction.Status.ROLLED_BACK));
    }

    /**
     * Body runs successfully, commit fails, and rollback also fails.  Should throw exception from commit with
     * suppressed exception from rollback failure.
     */
    @Test
    public void testGoodRunFailsCommitAndFailsRollback() {
        AtomicInteger counter = new AtomicInteger();
        AtomicReference<DefaultOfBizTransaction> transactionHolder = new AtomicReference<>();
        try {
            DefaultOfBizTransaction.withTransaction(new FailingCommitAndRollbackTransactionUtil(), t ->
            {
                //Verify we have a real transaction and it is not yet processed
                assertThat(t.isProcessed(), is(false));

                counter.incrementAndGet();
                transactionHolder.set(t);

                return null;
            });

            fail("Commit should have thrown exception.");
        } catch (OfBizTransaction.TransactionException e) {
            //This is expected
            //Verify rollback exception is in suppressed list
            assertThat(e.getSuppressed(), arrayWithSize(1));
        }

        DefaultOfBizTransaction transaction = transactionHolder.get();

        //The body executed once
        assertThat(counter.get(), is(1));

        //Commit failed so it attempted to roll back afterwards, but that failed too
        assertThat(transaction.isProcessed(), is(false));
        assertThat(transaction.getStatus(), is(DefaultOfBizTransaction.Status.UNPROCESSED));
    }

    private static class FailingRollbackTransactionUtil extends DefaultOfBizTransaction.OfBizTransactionUtil {
        @Override
        public void rollback(boolean began) throws GenericTransactionException {
            throw new GenericTransactionException("Rollback failed");
        }
    }

    private static class FailingCommitTransactionUtil extends DefaultOfBizTransaction.OfBizTransactionUtil {
        @Override
        public void commit(boolean began) throws GenericTransactionException {
            throw new GenericTransactionException("Commit failed");
        }
    }

    private static class FailingCommitAndRollbackTransactionUtil extends FailingCommitTransactionUtil {
        @Override
        public void rollback(boolean began) throws GenericTransactionException {
            throw new GenericTransactionException("Rollback failed");
        }
    }
}
