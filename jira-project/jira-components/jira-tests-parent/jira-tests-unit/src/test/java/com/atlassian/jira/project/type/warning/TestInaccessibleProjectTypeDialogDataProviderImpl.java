package com.atlassian.jira.project.type.warning;

import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.type.ProjectTypeKey;
import com.atlassian.jira.project.type.ProjectTypeManager;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.json.marshal.Jsonable;
import com.atlassian.webresource.api.assembler.WebResourceAssembler;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static com.atlassian.jira.project.type.warning.InaccessibleProjectTypeDialogDataProviderImpl.DIALOGS_KEY;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestInaccessibleProjectTypeDialogDataProviderImpl {
    private static final ApplicationUser USER = new MockApplicationUser("user");
    private static final ProjectTypeKey PROJECT_TYPE = new ProjectTypeKey("software");

    @Mock
    private GlobalPermissionManager globalPermissionManager;
    @Mock
    private PermissionManager permissionManager;
    @Mock
    private ProjectTypeManager projectTypeManager;
    @Mock
    private InaccessibleProjectTypeDialogContentProvider dialogDataProvider;
    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private WebResourceAssembler assembler;

    private ProviderExposingPageData provider;

    @Before
    public void setUp() {
        provider = new ProviderExposingPageData(
                globalPermissionManager,
                permissionManager,
                projectTypeManager,
                dialogDataProvider
        );
    }

    @Test
    public void itShouldNotDisplayWarningIfTheUserIsNotGlobalAdminNorProjectAdmin() {
        Project project = projectWithType(PROJECT_TYPE);

        userIsNotGlobalAdmin(USER);
        userIsNotProjectAdmin(USER, project);
        projectTypeIsUninstalled(PROJECT_TYPE);

        boolean shouldDisplayWarning = provider.shouldDisplayInaccessibleWarning(USER, project);

        assertThat(shouldDisplayWarning, is(false));
    }

    @Test
    public void itShouldDisplayWarningIfTheTheUserIsProjectAdminAndProjectTypeIsUninstalled() {
        Project project = projectWithType(PROJECT_TYPE);

        userIsNotGlobalAdmin(USER);
        userIsProjectAdmin(USER, project);
        projectTypeIsUninstalled(PROJECT_TYPE);

        boolean shouldDisplayWarning = provider.shouldDisplayInaccessibleWarning(USER, project);

        assertThat(shouldDisplayWarning, is(true));
    }

    @Test
    public void itShouldDisplayWarningIfTheTheUserIsGlobalAdminAndProjectTypeIsUninstalled() {
        Project project = projectWithType(PROJECT_TYPE);

        userIsGlobalAdmin(USER);
        userIsNotProjectAdmin(USER, project);
        projectTypeIsUninstalled(PROJECT_TYPE);

        boolean shouldDisplayWarning = provider.shouldDisplayInaccessibleWarning(USER, project);

        assertThat(shouldDisplayWarning, is(true));
    }

    @Test
    public void itShouldDisplayWarningIfTheTheUserIsProjectAdminAndProjectTypeIsInstalledButInaccessible() {
        Project project = projectWithType(PROJECT_TYPE);

        userIsNotGlobalAdmin(USER);
        userIsProjectAdmin(USER, project);
        projectTypeIsInstalledButInaccessible(PROJECT_TYPE);

        boolean shouldDisplayWarning = provider.shouldDisplayInaccessibleWarning(USER, project);

        assertThat(shouldDisplayWarning, is(true));
    }

    @Test
    public void itShouldDisplayWarningIfTheTheUserIsGlobalAdminAndProjectTypeIsInstalledButInaccessible() {
        Project project = projectWithType(PROJECT_TYPE);

        userIsGlobalAdmin(USER);
        userIsNotProjectAdmin(USER, project);
        projectTypeIsInstalledButInaccessible(PROJECT_TYPE);

        boolean shouldDisplayWarning = provider.shouldDisplayInaccessibleWarning(USER, project);

        assertThat(shouldDisplayWarning, is(true));
    }

    @Test
    public void shouldNotDisplayWarningIfTheUserIsProjectAdminAndTheProjectTypeIsInstalledAndLicensed() {
        Project project = projectWithType(PROJECT_TYPE);

        userIsNotGlobalAdmin(USER);
        userIsProjectAdmin(USER, project);
        projectTypeIsInstalledAndLicensed(PROJECT_TYPE);

        boolean shouldDisplayWarning = provider.shouldDisplayInaccessibleWarning(USER, project);

        assertThat(shouldDisplayWarning, is(false));
    }

    @Test
    public void shouldNotDisplayWarningIfTheUserIsGlobalAdminAndTheProjectTypeIsInstalledAndLicensed() {
        Project project = projectWithType(PROJECT_TYPE);

        userIsGlobalAdmin(USER);
        userIsNotProjectAdmin(USER, project);
        projectTypeIsInstalledAndLicensed(PROJECT_TYPE);

        boolean shouldDisplayWarning = provider.shouldDisplayInaccessibleWarning(USER, project);

        assertThat(shouldDisplayWarning, is(false));
    }

    @Test
    public void providesExpectedData() {
        Project project = projectWithType(PROJECT_TYPE);
        InaccessibleProjectTypeDialogContent dialogContent = mockDialogContent(project);

        provider.provideData(assembler, USER, project);

        assertThat(provider.pageData, is(dialogContent));

        assertAssemblerIncludedData();
    }

    private void userIsGlobalAdmin(ApplicationUser user) {
        when(globalPermissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, user)).thenReturn(true);
    }

    private void userIsNotGlobalAdmin(ApplicationUser user) {
        when(globalPermissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, user)).thenReturn(false);
    }

    private void userIsNotProjectAdmin(ApplicationUser user, Project project) {
        when(permissionManager.hasPermission(ProjectPermissions.ADMINISTER_PROJECTS, project, user)).thenReturn(false);
    }

    private void userIsProjectAdmin(ApplicationUser user, Project project) {
        when(permissionManager.hasPermission(ProjectPermissions.ADMINISTER_PROJECTS, project, user)).thenReturn(true);
    }

    private void projectTypeIsUninstalled(ProjectTypeKey projectTypeKey) {
        when(projectTypeManager.isProjectTypeUninstalled(projectTypeKey)).thenReturn(true);
    }

    private void projectTypeIsInstalled(ProjectTypeKey projectTypeKey) {
        when(projectTypeManager.isProjectTypeUninstalled(projectTypeKey)).thenReturn(false);
    }

    private void projectTypeIsInstalledButInaccessible(ProjectTypeKey projectTypeKey) {
        projectTypeIsInstalled(projectTypeKey);
        when(projectTypeManager.isProjectTypeInstalledButInaccessible(projectTypeKey)).thenReturn(true);
    }

    private void projectTypeIsInstalledAndLicensed(ProjectTypeKey projectTypeKey) {
        projectTypeIsInstalled(projectTypeKey);
        when(projectTypeManager.isProjectTypeInstalledButInaccessible(projectTypeKey)).thenReturn(false);
    }

    private InaccessibleProjectTypeDialogContent mockDialogContent(Project project) {
        InaccessibleProjectTypeDialogContent dialogContent = mock(InaccessibleProjectTypeDialogContent.class);
        when(dialogDataProvider.getContent(USER, project)).thenReturn(dialogContent);
        return dialogContent;
    }

    private void assertAssemblerIncludedData() {
        verify(assembler.data()).requireData(eq(DIALOGS_KEY), any(Jsonable.class));
    }

    private Project projectWithType(ProjectTypeKey projectTypeKey) {
        Project project = mock(Project.class);
        when(project.getProjectTypeKey()).thenReturn(projectTypeKey);
        return project;
    }

    private static class ProviderExposingPageData extends InaccessibleProjectTypeDialogDataProviderImpl {
        public InaccessibleProjectTypeDialogContent pageData;

        public ProviderExposingPageData(
                GlobalPermissionManager globalPermissionManager,
                PermissionManager permissionManager,
                ProjectTypeManager projectTypeManager,
                InaccessibleProjectTypeDialogContentProvider dialogDataProvider) {
            super(globalPermissionManager, permissionManager, projectTypeManager, dialogDataProvider);
        }

        @Override
        protected Jsonable getJsonable(final InaccessibleProjectTypeDialogContent pageData) {
            this.pageData = pageData;
            return super.getJsonable(pageData);
        }
    }
}
