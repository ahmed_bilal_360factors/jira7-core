package com.atlassian.jira.template.velocity;

import com.atlassian.event.api.EventPublisher;
import org.apache.velocity.app.VelocityEngine;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.time.Clock;
import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestInstrumentedVelocityEngineFactory {

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private VelocityEngineFactory velocityEngineFactory;

    @Mock
    private VelocityEngine velocityEngine;

    @Mock
    private EventPublisher eventPublisher;

    @Mock
    private Clock clock;

    private InstrumentedVelocityEngineFactory instrumentedVelocityEngineFactory;

    @Before
    public void setup() {
        instrumentedVelocityEngineFactory = new InstrumentedVelocityEngineFactory(velocityEngineFactory, eventPublisher, clock);
        when(velocityEngineFactory.getEngine()).thenReturn(velocityEngine);
    }

    @Test
    public void shouldMeasureInitializationTimeAndFireAnEvent() {

        final Instant now = Instant.now();
        final Instant later = now.plus(200, ChronoUnit.MILLIS);
        final Duration duration = Duration.between(now, later);
        when(clock.instant()).thenReturn(now).thenReturn(later);
        final ArgumentCaptor<VelocityEngineInitializedEvent> actualEvent = ArgumentCaptor.forClass(VelocityEngineInitializedEvent.class);

        final VelocityEngine actualVelocityEngine = instrumentedVelocityEngineFactory.getEngine();

        assertThat(actualVelocityEngine, equalTo(velocityEngine));
        verify(eventPublisher).publish(actualEvent.capture());
        assertThat(actualEvent.getValue().getInitializationTimeMillis(), equalTo(duration.toMillis()));
    }
}