package com.atlassian.jira.workflow;

import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoContainer;
import com.atlassian.jira.mock.security.MockSimpleAuthenticationContext;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.NoopI18nHelper;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.opensymphony.workflow.WorkflowContext;
import org.hamcrest.Matchers;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;

import java.util.Locale;
import java.util.Map;

import static com.atlassian.jira.util.NoopI18nHelper.makeTranslation;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestWorkflowUtil {
    private static final String FIELD_NAME = "fName";

    @Rule
    public final MockitoContainer mockContainer = new MockitoContainer(this);

    @SuppressWarnings("UnusedDeclaration")
    @AvailableInContainer
    private JiraAuthenticationContext authenticationContext
            = new MockSimpleAuthenticationContext(new MockApplicationUser("WAT"), Locale.ENGLISH, new NoopI18nHelper());

    @Mock
    private JiraWorkflow workflow;

    private ErrorCollection errorCollection = new SimpleErrorCollection();

    @Test
    public void shouldReturnFalseWhenNameIsNull() {
        final boolean check = WorkflowUtil.isAcceptableName(null, FIELD_NAME, errorCollection);

        assertEquals(makeTranslation("admin.errors.you.must.specify.a.workflow.name"),
                errorCollection.getErrors().get(FIELD_NAME));
        assertFalse(check);
    }

    @Test
    public void shouldReturnFalseWhenNameIsBlank() {
        final boolean check = WorkflowUtil.isAcceptableName("   ", FIELD_NAME, errorCollection);

        assertEquals(makeTranslation("admin.errors.you.must.specify.a.workflow.name"),
                errorCollection.getErrors().get(FIELD_NAME));
        assertFalse(check);
    }

    @Test
    public void shouldReturnFalseWhenNameContainsNonASCIILikeCharacters() {
        final boolean check = WorkflowUtil.isAcceptableName("workflow \u4321", FIELD_NAME, errorCollection);

        assertEquals(makeTranslation("admin.errors.please.use.only.ascii.characters"),
                errorCollection.getErrors().get(FIELD_NAME));
        assertFalse(check);
    }

    @Test
    public void shouldReturnFalseWhenNameContainsTrailingWhitespaces() {
        final boolean check = WorkflowUtil.isAcceptableName("workflowName ", FIELD_NAME, errorCollection);

        assertEquals(makeTranslation("admin.errors.workflow.name.cannot.contain.leading.or.trailing.whitespaces"),
                errorCollection.getErrors().get(FIELD_NAME));
        assertFalse(check);
    }

    @Test
    public void shouldReturnTrueAndAddNoErrorWheNameIsOK() {
        final boolean check = WorkflowUtil.isAcceptableName("workflowName", FIELD_NAME, errorCollection);

        assertNull(errorCollection.getErrors().get(FIELD_NAME));
        assertTrue(check);
    }

    @SuppressWarnings("ConstantConditions")
    @Test(expected = NullPointerException.class)
    public void shouldReturnThrowExceptionWhenErrorCollectionIsNull() {
        WorkflowUtil.isAcceptableName(null, FIELD_NAME, null);
    }

    @SuppressWarnings("ConstantConditions")
    @Test(expected = NullPointerException.class)
    public void shouldReturnThrowExceptionWhenFieldNameIsNull() {
        WorkflowUtil.isAcceptableName(null, null, errorCollection);
    }

    @Test
    public void testGetWorkflowDisplayNameWithNullWorkflow() {
        String displayName = WorkflowUtil.getWorkflowDisplayName(null);
        //noinspection ConstantConditions
        assertNull(displayName);
    }

    @Test
    public void testGetWorkflowDisplayNameNormalWorkflow() {
        when(workflow.isDraftWorkflow()).thenReturn(false);
        when(workflow.getName()).thenReturn("Gregory");

        String displayName = WorkflowUtil.getWorkflowDisplayName(workflow);
        assertEquals("Gregory", displayName);
    }

    @Test
    public void testGetWorkflowDisplayNameDraftWorkflow() {
        when(workflow.isDraftWorkflow()).thenReturn(true);
        when(workflow.getName()).thenReturn("Gregory");

        String displayName = WorkflowUtil.getWorkflowDisplayName(workflow);
        final String expectedName = String.format("Gregory (%s)", makeTranslation("common.words.draft"));
        assertEquals(expectedName, displayName);
    }


    @Test
    public void isReservedKey() {
        assertThat(WorkflowUtil.isReservedKey("jira.reserved"), Matchers.equalTo(true));
        assertThat(WorkflowUtil.isReservedKey(" jira.reserved "), Matchers.equalTo(true));

        for (String s : JiraWorkflow.JIRA_META_ATTRIBUTE_ALLOWED_LIST) {
            assertThat(WorkflowUtil.isReservedKey(s), Matchers.equalTo(false));
            assertThat(WorkflowUtil.isReservedKey(s + ".extra"), Matchers.equalTo(false));
        }

        assertThat(WorkflowUtil.isReservedKey("other"), Matchers.equalTo(false));
        assertThat(WorkflowUtil.isReservedKey("<>"), Matchers.equalTo(false));
        assertThat(WorkflowUtil.isReservedKey(null), Matchers.equalTo(false));
    }

    @Test
    public void getCallerKeyGetsCallerKeyFromWorkflowContext() {
        final String test_user_key = "test_user_key";
        final WorkflowContext workflowContext = mock(WorkflowContext.class);
        when(workflowContext.getCaller()).thenReturn(test_user_key);
        final ImmutableMap<String, WorkflowContext> transientVars = ImmutableMap.of("context", workflowContext);

        final String actualUserKey = WorkflowUtil.getCallerKey(transientVars);
        assertEquals(test_user_key, actualUserKey);
    }

    @Test
    public void getCallerKeyReturnsNullWhenTransientVarsAreNull() {
        assertNull(WorkflowUtil.getCallerKey(null));
    }

    @Test
    public void getCallerKeyReturnsNullWhenContextIsNull() {
        final Map<String, WorkflowContext> transientVars = Maps.newHashMap();
        transientVars.put("context", null);
        assertNull(WorkflowUtil.getCallerKey(transientVars));
    }
}
