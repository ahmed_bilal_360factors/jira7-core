package com.atlassian.jira.issue.watchers;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.association.UserAssociationStore;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.database.QueryDslAccessor;
import com.atlassian.jira.issue.IssueFactory;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.index.IssueIndexManager;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoContainer;
import com.atlassian.jira.mock.issue.MockIssue;
import com.atlassian.jira.security.JiraAuthenticationContextImpl;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.util.UserKeyStore;
import com.atlassian.jira.user.util.UserManager;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Answers;
import org.mockito.Mock;

import static org.junit.Assert.assertFalse;

/**
 * @since v6.0
 */

public class TestUnitDefaultWatcherManager {
    @Rule
    public MockitoContainer mockitoContainer = new MockitoContainer(this);

    @AvailableInContainer
    @Mock
    private UserKeyStore userKeyStore;

    @Mock
    private UserAssociationStore userAssociationStore;
    @Mock
    private ApplicationProperties applicationProperties;
    @Mock
    private IssueIndexManager indexManager;
    @Mock
    private UserManager userManager;
    @Mock(answer = Answers.RETURNS_MOCKS)
    private IssueFactory issueFactory;
    @Mock
    private EventPublisher eventPublisher;
    @Mock
    private DefaultWatcherManager watcherManager;
    @Mock
    private IssueManager issueManager;
    @Mock
    private QueryDslAccessor queryDslAccessor;

    @Before
    public void setUp() throws Exception {
        JiraAuthenticationContextImpl.clearRequestCache();

        watcherManager = new DefaultWatcherManager(
                userAssociationStore, applicationProperties, indexManager, issueFactory, eventPublisher, issueManager, userManager, queryDslAccessor);
    }

    @Test
    public void shouldNotThrowNPEWhenNoWatchersDefined() {
        MockIssue issue = new MockIssue(1, "IS-1");
        ApplicationUser user = new MockApplicationUser("test");

        final boolean watching = watcherManager.isWatching(user, issue);
        assertFalse("User should not be watching this issue", watching);
    }
}
