package com.atlassian.jira.bc.issue;


import com.atlassian.instrumentation.InstrumentRegistry;
import com.atlassian.jira.bc.issue.properties.IssuePropertyService;
import com.atlassian.jira.concurrent.BarrierFactory;
import com.atlassian.jira.config.SubTaskManager;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.AttachmentManager;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueFactory;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.customfields.CloneOptionConfiguration;
import com.atlassian.jira.issue.customfields.CustomFieldType;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.MockCustomField;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.issue.link.IssueLinkManager;
import com.atlassian.jira.issue.link.IssueLinkTypeManager;
import com.atlassian.jira.issue.link.RemoteIssueLinkManager;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.mock.i18n.MockI18nHelper;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.task.TaskManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.util.I18nHelper;
import com.google.common.collect.ImmutableMap;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.mockito.Mock;
import org.ofbiz.core.entity.GenericValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.Map;
import java.util.Optional;

import static java.util.Arrays.asList;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestCloneIssueCommand {
    @Rule
    public RuleChain ruleChain = MockitoMocksInContainer.forTest(this);

    @Mock
    IssueFactory mockIssueFactory;
    @Mock
    private ApplicationProperties applicationProperties;
    @Mock
    private IssueLinkTypeManager issueLinkTypeManager;
    @Mock
    private IssueLinkManager issueLinkManager;
    @Mock
    private RemoteIssueLinkManager remoteIssueLinkManager;
    @Mock
    private AttachmentManager attachmentManager;
    @Mock
    private SubTaskManager subTaskManager;
    @Mock
    private TaskManager taskManager;
    @Mock
    private InstrumentRegistry instrumentRegistry;
    @Mock
    private IssuePropertyService issuePropertyService;
    @Mock
    private CustomFieldManager customFieldManager;

    @Mock
    private IssueFactory issueFactory;
    @Mock
    private PermissionManager permissionManager;
    @Mock
    private IssueManager issueManager;
    private ApplicationUser user = new MockApplicationUser("bill");
    @Mock
    private BarrierFactory barrierFactory;
    private Logger log = LoggerFactory.getLogger(TestCloneIssueCommand.class);
    private I18nHelper i18n = new MockI18nHelper();

    @Test
    public void testShouldCloneCustomFieldValuesUsingCustomFieldType() {
        CustomField cf1 = givenACustomFieldWithId("10000");
        CustomField cf2 = givenACustomFieldWithId("10001");
        CustomField cf3 = givenACustomFieldWithId("10002");
        CustomField cf4 = givenACustomFieldWithId("10003");
        CustomField cf5 = givenACustomFieldWithId("10004");

        Map<CustomField, Optional<Boolean>> cloneOptionSelections = ImmutableMap.<CustomField, Optional<Boolean>>builder()
                .put(cf1, Optional.empty())
                .put(cf2, Optional.of(Boolean.TRUE))
                .put(cf3, Optional.of(Boolean.FALSE))
                .put(cf4, Optional.of(Boolean.TRUE))
                .put(cf5, Optional.of(Boolean.FALSE))
                .build();

        Issue originalIssue = originalIssueWithNeededInformationToBeCloned();

        CloneIssueCommand command = new CloneIssueCommand(user, originalIssue, "New Summary",
                false, false, false, cloneOptionSelections,
                issueManager, issueFactory, applicationProperties, issueLinkTypeManager, issueLinkManager, remoteIssueLinkManager,
                attachmentManager, subTaskManager, permissionManager, customFieldManager, log, i18n, barrierFactory);

        MutableIssue clonedIssue = mock(MutableIssue.class);

        when(customFieldManager.getCustomFieldObjects(originalIssue.getProjectId(), originalIssue.getIssueTypeId())).thenReturn(asList(cf1, cf2, cf3, cf4, cf5));
        when(cf1.getCustomFieldType().getCloneValue(cf1, originalIssue, Optional.empty())).thenReturn(null);
        when(cf2.getCustomFieldType().getCloneValue(cf2, originalIssue, Optional.of(true))).thenReturn("Value2");
        when(cf3.getCustomFieldType().getCloneValue(cf3, originalIssue, Optional.of(false))).thenReturn(null);
        when(cf4.getCustomFieldType().getCloneValue(cf4, originalIssue, Optional.of(true))).thenReturn("Value4");
        when(cf5.getCustomFieldType().getCloneValue(cf5, originalIssue, Optional.of(false))).thenReturn(null);

        command.cloneCustomFields(user, originalIssue, clonedIssue, cloneOptionSelections);

        verify(cf1.getCustomFieldType()).getCloneValue(cf1, originalIssue, Optional.empty());
        verify(cf2.getCustomFieldType()).getCloneValue(cf2, originalIssue, Optional.of(true));
        verify(cf3.getCustomFieldType()).getCloneValue(cf3, originalIssue, Optional.of(false));
        verify(cf4.getCustomFieldType()).getCloneValue(cf4, originalIssue, Optional.of(true));
        verify(cf5.getCustomFieldType()).getCloneValue(cf5, originalIssue, Optional.of(false));

        verify(clonedIssue, never()).setCustomFieldValue(eq(cf1), any());
        verify(clonedIssue, times(1)).setCustomFieldValue(cf2, "Value2");
        verify(clonedIssue, never()).setCustomFieldValue(eq(cf3), any());
        verify(clonedIssue, times(1)).setCustomFieldValue(cf4, "Value4");
        verify(clonedIssue, never()).setCustomFieldValue(eq(cf5), any());
    }

    @Test
    public void testShouldNotCloneNullValues() {

        CustomField cf1 = givenACustomFieldWithId("10000");
        CustomField cf2 = givenACustomFieldWithId("10001");
        CustomField cf3 = givenACustomFieldWithId("10002");
        CustomField cf4 = givenACustomFieldWithId("10003");
        CustomField cf5 = givenACustomFieldWithId("10004");

        Map<CustomField, Optional<Boolean>> cloneOptionSelections = ImmutableMap.<CustomField, Optional<Boolean>>builder()
                .put(cf1, Optional.empty())
                .put(cf2, Optional.of(Boolean.TRUE))
                .put(cf3, Optional.of(Boolean.FALSE))
                .put(cf4, Optional.of(Boolean.TRUE))
                .put(cf5, Optional.of(Boolean.FALSE))
                .build();

        Issue originalIssue = originalIssueWithNeededInformationToBeCloned();

        CloneIssueCommand command = new CloneIssueCommand(user, originalIssue, "New Summary",
                false, false, false, cloneOptionSelections,
                issueManager, issueFactory, applicationProperties, issueLinkTypeManager, issueLinkManager, remoteIssueLinkManager,
                attachmentManager, subTaskManager, permissionManager, customFieldManager, log, i18n, barrierFactory);

        MutableIssue clonedIssue = mock(MutableIssue.class);

        when(customFieldManager.getCustomFieldObjects(originalIssue.getProjectId(), originalIssue.getIssueTypeId())).thenReturn(asList(cf1, cf2, cf3, cf4, cf5));
        when(cf1.getCustomFieldType().getCloneValue(cf1, originalIssue, Optional.of(true))).thenReturn(null);
        when(cf2.getCustomFieldType().getCloneValue(cf2, originalIssue, Optional.of(true))).thenReturn("Value2");
        when(cf3.getCustomFieldType().getCloneValue(cf3, originalIssue, Optional.of(true))).thenReturn(null);
        when(cf4.getCustomFieldType().getCloneValue(cf4, originalIssue, Optional.of(true))).thenReturn("Value4");
        when(cf5.getCustomFieldType().getCloneValue(cf5, originalIssue, Optional.of(true))).thenReturn(null);

        command.cloneCustomFields(user, originalIssue, clonedIssue, cloneOptionSelections);

        verify(cf1.getCustomFieldType()).getCloneValue(cf1, originalIssue, Optional.empty());
        verify(cf2.getCustomFieldType()).getCloneValue(cf2, originalIssue, Optional.of(true));
        verify(cf3.getCustomFieldType()).getCloneValue(cf3, originalIssue, Optional.of(false));
        verify(cf4.getCustomFieldType()).getCloneValue(cf4, originalIssue, Optional.of(true));
        verify(cf5.getCustomFieldType()).getCloneValue(cf5, originalIssue, Optional.of(false));

        verify(clonedIssue, never()).setCustomFieldValue(eq(cf1), any());
        verify(clonedIssue, never()).setCustomFieldValue(eq(cf3), any());
        verify(clonedIssue, never()).setCustomFieldValue(eq(cf5), any());
    }

    private CustomField givenACustomFieldWithId(String customFieldId) {
        CustomFieldType cfType = mock(CustomFieldType.class);
        when(cfType.getCloneOptionConfiguration(any(CustomField.class), any(Issue.class))).thenReturn(CloneOptionConfiguration.DO_NOT_DISPLAY);

        return new MockCustomField(customFieldId, "Custom field " + customFieldId, cfType);
    }

    private Issue originalIssueWithNeededInformationToBeCloned() {
        Issue originalIssueToBeCloned = mock(MutableIssue.class);
        when(originalIssueToBeCloned.getOriginalEstimate()).thenReturn(1L);
        when(originalIssueToBeCloned.getFixVersions()).thenReturn(Collections.emptyList());
        when(originalIssueToBeCloned.getAffectedVersions()).thenReturn(Collections.emptyList());
        when(originalIssueToBeCloned.getId()).thenReturn(1L);

        Project originalProjectObject = mock(Project.class);
        when(originalProjectObject.getId()).thenReturn(1L);
        when(originalIssueToBeCloned.getProjectObject()).thenReturn(originalProjectObject);

        IssueType originalIssueType = mock(IssueType.class);
        when(originalIssueType.getId()).thenReturn("issue1");
        when(originalIssueToBeCloned.getIssueTypeObject()).thenReturn(originalIssueType);
        when(mockIssueFactory.cloneIssue(any(Issue.class))).thenReturn((MutableIssue) originalIssueToBeCloned);
        when(mockIssueFactory.getIssue(any(GenericValue.class))).thenReturn((MutableIssue) originalIssueToBeCloned);
        return originalIssueToBeCloned;
    }


}