package com.atlassian.jira.crowd.embedded.ofbiz;

import com.atlassian.crowd.model.group.Group;
import com.atlassian.crowd.model.group.GroupType;

public class MockGroup implements Group {

    private final String name;
    private final boolean active;
    private final String description;
    private final long directoryId;
    private final GroupType type;

    public MockGroup(String name, GroupType type, boolean active, String description, long directoryId) {
        this.name = name;
        this.type = type;
        this.active = active;
        this.description = description;
        this.directoryId = directoryId;
    }

    @Override
    public GroupType getType() {
        return type;
    }

    @Override
    public boolean isActive() {
        return active;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public int compareTo(Group o) {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public long getDirectoryId() {
        return directoryId;
    }

    @Override
    public String getName() {
        return name;
    }
}
