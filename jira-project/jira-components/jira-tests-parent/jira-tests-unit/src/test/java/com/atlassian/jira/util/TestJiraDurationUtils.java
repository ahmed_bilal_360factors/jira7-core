package com.atlassian.jira.util;

import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoContainer;
import com.atlassian.jira.web.bean.MockI18nBean;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;

import java.math.BigDecimal;
import java.util.Locale;
import java.util.ResourceBundle;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestJiraDurationUtils {
    @Rule
    public MockitoContainer mockitoContainer = new MockitoContainer(this);

    @Mock
    @AvailableInContainer
    private DurationFormatterProvider durationFormatterProvider;

    @Test
    public void testPrettyDurationFormatter() {
        JiraDurationUtils.PrettyDurationFormatter formatter =
                new JiraDurationUtils.PrettyDurationFormatter(24, 7, new MockI18nBean());

        String duration = formatter.format(0L);
        assertEquals("0 minutes", duration);

        // negatives are not handled
        duration = formatter.format(-630L);
        assertEquals("", duration);

        duration = formatter.format(630L); // 10m 30s
        assertEquals("10 minutes", duration);

        duration = formatter.format(3700L); // 1h 1m 40s
        assertEquals("1 hour, 1 minute", duration);

        duration = formatter.format(90100L); // 1d 1h 1m 40s
        assertEquals("1 day, 1 hour, 1 minute", duration);

        duration = formatter.format(694900L); // 1w 1d 1h 1m 40s
        assertEquals("1 week, 1 day, 1 hour, 1 minute", duration);
    }

    @Test
    public void testHoursDurationFormatter() {
        JiraDurationUtils.HoursDurationFormatter formatter = new JiraDurationUtils.HoursDurationFormatter(new MockI18nBean());

        String duration = formatter.format(0L);
        assertEquals("0h", duration);

        // negatives are not handled
        duration = formatter.format(-630L);
        assertEquals("", duration);

        duration = formatter.format(630L); // 10m 30s
        assertEquals("10.5m", duration);

        duration = formatter.format(3700L); // 1h 1m 40s
        assertEquals("1h 1m", duration);

        duration = formatter.format(90100L); // 1d 1h 1m 40s
        assertEquals("25h 1m", duration);

        duration = formatter.format(694900L); // 1w 1d 1h 1m 40s
        assertEquals("193h 1m", duration);

        assertEquals("5.5h", formatter.format(19800L));
        assertEquals("7.18h", formatter.format(25848L));
    }

    @Test
    public void testDaysDurationFormatter8() {
        JiraDurationUtils.DaysDurationFormatter formatter = new JiraDurationUtils.DaysDurationFormatter(BigDecimal.valueOf(8), new MockI18nBean());

        String duration = formatter.format(0L);
        assertEquals("0d", duration);

        // negatives are not handled
        duration = formatter.format(-630L);
        assertEquals("", duration);

        duration = formatter.format(630L); // 10m 30s
        assertEquals("10.5m", duration);

        duration = formatter.format(3700L); // 1h 1m 40s
        assertEquals("1h 1m", duration);

        duration = formatter.format(90100L); // 1d 1h 1m 40s
        assertEquals("3d 1h 1m", duration);

        duration = formatter.format(694900L); // 1w 1d 1h 1m 40s
        assertEquals("24d 1h 1m", duration);
    }

    @Test
    public void testDaysDurationFormatter24() {
        JiraDurationUtils.DaysDurationFormatter formatter = new JiraDurationUtils.DaysDurationFormatter(BigDecimal.valueOf(24), new MockI18nBean());
        String duration = formatter.format(0L);
        assertEquals("0d", duration);

        // negatives are not handled
        duration = formatter.format(-630L);
        assertEquals("", duration);

        duration = formatter.format(630L); // 10m 30s
        assertEquals("10.5m", duration);

        assertEquals("5.5d", formatter.format(475200L));
        assertEquals("2.25d", formatter.format(194400L));

        duration = formatter.format(3700L); // 1h 1m 40s
        assertEquals("1h 1m", duration);

        duration = formatter.format(90100L); // 1d 1h 1m 40s
        assertEquals("1d 1h 1m", duration);

        duration = formatter.format(694900L); // 1w 1d 1h 1m 40s
        assertEquals("8d 1h 1m", duration);
    }


    @Test
    public void formattingDurationByLocaleShouldUseEntireResourceBundleNotJustDefault() {
        // Set up
        final long duration = 1234;
        final int hoursPerDay = 20;
        final int daysPerWeek = 6;
        final I18nHelper mockI18nHelper = mock(I18nHelper.class);
        final ResourceBundle mockResourceBundle = mock(ResourceBundle.class);
        final String prettyDuration = "Ooh, nice!";
        final JiraDurationUtils.DurationFormatter durationFormatter = new JiraDurationUtils.PrettyDurationFormatter(hoursPerDay, daysPerWeek, mockI18nHelper) {
            @Override
            String getDurationPrettySeconds(final Long actualDuration, final ResourceBundle resourceBundle,
                                            final long secondsPerDay, final long secondsPerWeek) {
                assertSame(mockResourceBundle, resourceBundle);
                assertEquals(duration, actualDuration.longValue());
                assertEquals(hoursPerDay * 3600, secondsPerDay);
                assertEquals(daysPerWeek * secondsPerDay, secondsPerWeek);
                return prettyDuration;
            }
        };
        final Locale locale = Locale.getDefault();
        when(mockI18nHelper.getResourceBundle()).thenReturn(mockResourceBundle);


        // Invoke
        final String actualPrettyDuration = durationFormatter.format(duration, locale);

        // Check
        assertEquals(prettyDuration, actualPrettyDuration);
    }

    @Test
    public void formattingDurationWithoutLocaleShouldUseDefaultResourceBundle() {
        // Set up
        final long duration = 1234;
        final int hoursPerDay = 20;
        final int daysPerWeek = 6;
        final I18nHelper mockI18nHelper = mock(I18nHelper.class);
        final ResourceBundle mockResourceBundle = mock(ResourceBundle.class);
        final String prettyDuration = "Ooh, nice!";
        final JiraDurationUtils.DurationFormatter durationFormatter = new JiraDurationUtils.PrettyDurationFormatter(hoursPerDay, daysPerWeek, mockI18nHelper) {
            @Override
            String getDurationPrettySeconds(final Long actualDuration, final ResourceBundle resourceBundle,
                                            final long secondsPerDay, final long secondsPerWeek) {
                assertSame(mockResourceBundle, resourceBundle);
                assertEquals(duration, actualDuration.longValue());
                assertEquals(hoursPerDay * 3600, secondsPerDay);
                assertEquals(daysPerWeek * secondsPerDay, secondsPerWeek);
                return prettyDuration;
            }
        };
        when(mockI18nHelper.getDefaultResourceBundle()).thenReturn(mockResourceBundle);


        // Invoke
        final String actualPrettyDuration = durationFormatter.format(duration);

        // Check
        assertEquals(prettyDuration, actualPrettyDuration);
    }
}
