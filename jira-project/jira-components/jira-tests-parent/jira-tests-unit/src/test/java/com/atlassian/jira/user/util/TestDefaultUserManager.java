package com.atlassian.jira.user.util;

import com.atlassian.applinks.api.auth.oauth.ConsumerTokenService;
import com.atlassian.beehive.ClusterLockService;
import com.atlassian.beehive.simple.SimpleClusterLockService;
import com.atlassian.cache.CacheManager;
import com.atlassian.cache.memory.MemoryCacheManager;
import com.atlassian.crowd.embedded.api.CrowdDirectoryService;
import com.atlassian.crowd.embedded.api.CrowdService;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.DirectoryType;
import com.atlassian.crowd.embedded.api.OperationType;
import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.crowd.embedded.impl.ImmutableDirectory;
import com.atlassian.crowd.embedded.impl.ImmutableUser;
import com.atlassian.crowd.embedded.spi.DirectoryDao;
import com.atlassian.crowd.exception.DirectoryNotFoundException;
import com.atlassian.crowd.manager.application.ApplicationManager;
import com.atlassian.crowd.manager.directory.DirectoryManager;
import com.atlassian.crowd.model.application.Application;
import com.atlassian.crowd.model.user.UserTemplate;
import com.atlassian.crowd.search.query.entity.UserQuery;
import com.atlassian.jira.config.database.DatabaseConfig;
import com.atlassian.jira.config.database.JndiDatasource;
import com.atlassian.jira.config.database.MockDatabaseConfigurationManager;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.crowd.embedded.ofbiz.OfBizUserDao;
import com.atlassian.jira.crowd.embedded.ofbiz.db.DefaultOfBizTransactionManager;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.license.LicenseCountService;
import com.atlassian.jira.mock.MockApplicationProperties;
import com.atlassian.jira.mock.component.MockComponentWorker;
import com.atlassian.jira.mock.ofbiz.MockOfBizDelegator;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.MockCrowdDirectoryService;
import com.atlassian.jira.user.MockCrowdService;
import com.atlassian.jira.user.MockUser;
import com.atlassian.jira.user.UserDetails;
import com.atlassian.jira.user.UserKeyService;
import com.atlassian.jira.user.UserKeyServiceImpl;
import com.atlassian.jira.user.util.UserManager.UserState;
import com.google.common.collect.ImmutableList;
import org.hamcrest.Matchers;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.mockito.Mock;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static com.atlassian.jira.user.ApplicationUsers.toDirectoryUsers;
import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

/**
 * @since v4.4
 */
public class TestDefaultUserManager {
    @Rule
    public final RuleChain mockContainer = MockitoMocksInContainer.forTest(this);

    @AvailableInContainer
    @Mock
    private LicenseCountService licenseCountService;

    @Test
    public void testCanUpdateUser_readOnlyDirectory() throws Exception {
        final ApplicationUser mockUser = createMockUser();
        final Directory mockDirectory = createMockDirectory(mockUser.getDirectoryId(), null, false);

        final CrowdDirectoryService crowdDirectoryService = new MockCrowdDirectoryService();
        crowdDirectoryService.addDirectory(mockDirectory);

        final DefaultUserManager defaultUserManager = new DefaultUserManager(null, crowdDirectoryService, null, null, null, null, null);
        assertFalse("canUpdateUser", defaultUserManager.canUpdateUser(mockUser));
    }

    @Test
    public void testCanUpdateUser_writableDirectory() throws Exception {
        final ApplicationUser mockUser = createMockUser();
        final Directory mockDirectory = createMockDirectory(mockUser.getDirectoryId(), null, true);

        final CrowdDirectoryService crowdDirectoryService = new MockCrowdDirectoryService();
        crowdDirectoryService.addDirectory(mockDirectory);

        final DefaultUserManager defaultUserManager = new DefaultUserManager(null, crowdDirectoryService, null, null, null, null, null);
        assertTrue("canUpdateUser", defaultUserManager.canUpdateUser(mockUser));
    }

    @Test
    public void testCanUpdateUserPassword_readOnlyDirectory() throws Exception {
        final ApplicationUser mockUser = createMockUser();
        final Directory mockDirectory = createMockDirectory(mockUser.getDirectoryId(), null, false);

        final CrowdDirectoryService crowdDirectoryService = new MockCrowdDirectoryService();
        crowdDirectoryService.addDirectory(mockDirectory);

        final DefaultUserManager defaultUserManager = new DefaultUserManager(null, crowdDirectoryService, null, null, null, null, null);
        assertFalse("canUpdateUserPassword", defaultUserManager.canUpdateUserPassword(mockUser));
    }

    @Test
    public void testCanUpdateUserPassword_writableNonDelegatedLdapDirectory() throws Exception {
        final ApplicationUser mockUser = createMockUser();
        final Directory mockDirectory = createMockDirectory(mockUser.getDirectoryId(), DirectoryType.CONNECTOR, true);

        final CrowdDirectoryService crowdDirectoryService = new MockCrowdDirectoryService();
        crowdDirectoryService.addDirectory(mockDirectory);

        final DefaultUserManager defaultUserManager = new DefaultUserManager(null, crowdDirectoryService, null, null, null, null, null);
        assertTrue("canUpdateUserPassword", defaultUserManager.canUpdateUserPassword(mockUser));
    }

    @Test
    public void testCanUpdateUserPassword_writableDelegatedLdapDirectory() throws Exception {
        final ApplicationUser mockUser = createMockUser();
        final Directory mockDirectory = createMockDirectory(mockUser.getDirectoryId(), DirectoryType.DELEGATING, true);

        final CrowdDirectoryService crowdDirectoryService = new MockCrowdDirectoryService();
        crowdDirectoryService.addDirectory(mockDirectory);

        final DefaultUserManager defaultUserManager = new DefaultUserManager(null, crowdDirectoryService, null, null, null, null, null);
        assertFalse("canUpdateUserPassword", defaultUserManager.canUpdateUserPassword(mockUser));
    }

    @Test
    public void testCanRenameUser_invalidDirectory() throws Exception {
        final ApplicationUser mockUser = createMockApplicationUser();
        final CrowdDirectoryService crowdDirectoryService = new MockCrowdDirectoryService();

        final DefaultUserManager defaultUserManager = new DefaultUserManager(null, crowdDirectoryService, null, null, null, null, null);
        assertFalse("canRenameUser", defaultUserManager.canRenameUser(mockUser));
    }

    @Test
    public void testCanRenameUser_readOnlyDirectory() throws Exception {
        final ApplicationUser mockUser = createMockApplicationUser();
        final Directory mockDirectory = createMockDirectory(mockUser.getDirectoryId(), null, false);
        final CrowdDirectoryService crowdDirectoryService = new MockCrowdDirectoryService();
        crowdDirectoryService.addDirectory(mockDirectory);

        final DefaultUserManager defaultUserManager = new DefaultUserManager(null, crowdDirectoryService, null, null, null, null, null);
        assertFalse("canRenameUser", defaultUserManager.canRenameUser(mockUser));
    }

    @Test
    public void testCanRenameUser_badDirectoryType() throws Exception {
        final ApplicationUser mockUser = createMockApplicationUser();
        final Directory mockDirectory = createMockDirectory(mockUser.getDirectoryId(), DirectoryType.UNKNOWN, true);
        final CrowdDirectoryService crowdDirectoryService = new MockCrowdDirectoryService();
        crowdDirectoryService.addDirectory(mockDirectory);

        final DefaultUserManager defaultUserManager = new DefaultUserManager(null, crowdDirectoryService, null, null, null, null, null);
        assertFalse("canRenameUser", defaultUserManager.canRenameUser(mockUser));
    }

    @Test
    public void testCanRenameUser_actingAsCrowdServer() throws Exception {
        final ApplicationUser mockUser = createMockApplicationUser();
        final Directory mockDirectory = createMockDirectory(mockUser.getDirectoryId(), DirectoryType.INTERNAL, true);
        final CrowdDirectoryService crowdDirectoryService = new MockCrowdDirectoryService();
        final ApplicationManager applicationManager = mock(ApplicationManager.class);
        final Application embeddedCrowd = mock(Application.class);
        final Application externalApplication = mock(Application.class);
        final ApplicationProperties applicationProperties = new MockApplicationProperties();

        crowdDirectoryService.addDirectory(mockDirectory);
        when(embeddedCrowd.isPermanent()).thenReturn(true);
        when(applicationManager.findAll()).thenReturn(asList(embeddedCrowd, externalApplication));

        final DefaultUserManager defaultUserManager = new DefaultUserManager(null, crowdDirectoryService, null, null,
                applicationManager, applicationProperties, null);
        assertFalse("canRenameUser", defaultUserManager.canRenameUser(mockUser));
    }

    @Test
    public void testCanRenameUser_applicationPropertyOverridesCrowdServerCheck() throws Exception {
        final ApplicationUser mockUser = createMockApplicationUser();
        final Directory mockDirectory = createMockDirectory(mockUser.getDirectoryId(), DirectoryType.INTERNAL, true);
        final CrowdDirectoryService crowdDirectoryService = new MockCrowdDirectoryService();
        final ApplicationManager applicationManager = mock(ApplicationManager.class);
        final Application embeddedCrowd = mock(Application.class);
        final Application externalApplication = mock(Application.class);
        final ApplicationProperties applicationProperties = new MockApplicationProperties();

        crowdDirectoryService.addDirectory(mockDirectory);
        when(embeddedCrowd.isPermanent()).thenReturn(true);
        when(applicationManager.findAll()).thenReturn(asList(embeddedCrowd, externalApplication));
        applicationProperties.setOption(APKeys.JIRA_OPTION_USER_CROWD_ALLOW_RENAME, true);

        final DefaultUserManager defaultUserManager = new DefaultUserManager(null, crowdDirectoryService, null, null,
                applicationManager, applicationProperties, null);
        assertTrue("canRenameUser", defaultUserManager.canRenameUser(mockUser));
    }

    @Test
    public void testCanRenameUser_internalDirectoryAndNotActingAsCrowdServer() throws Exception {
        final ApplicationUser mockUser = createMockApplicationUser();
        final Directory mockDirectory = createMockDirectory(mockUser.getDirectoryId(), DirectoryType.INTERNAL, true);
        final CrowdDirectoryService crowdDirectoryService = new MockCrowdDirectoryService();
        final ApplicationManager applicationManager = mock(ApplicationManager.class);
        final Application embeddedCrowd = mock(Application.class);
        final ApplicationProperties applicationProperties = new MockApplicationProperties();

        crowdDirectoryService.addDirectory(mockDirectory);
        when(embeddedCrowd.isPermanent()).thenReturn(true);
        when(applicationManager.findAll()).thenReturn(asList(embeddedCrowd));

        final DefaultUserManager defaultUserManager = new DefaultUserManager(null, crowdDirectoryService, null, null,
                applicationManager, applicationProperties, null);
        assertTrue("canRenameUser", defaultUserManager.canRenameUser(mockUser));
    }

    @Test
    public void testHasPasswordWritableDirectory_none() throws Exception {
        final Directory readOnlyDirectory = createMockDirectory(1, null, false);
        final Directory writableDelegatedDirectory = createMockDirectory(2, DirectoryType.DELEGATING, true);

        final CrowdDirectoryService crowdDirectoryService = new MockCrowdDirectoryService();
        crowdDirectoryService.addDirectory(readOnlyDirectory);
        crowdDirectoryService.addDirectory(writableDelegatedDirectory);

        final DefaultUserManager defaultUserManager = new DefaultUserManager(null, crowdDirectoryService, null, null, null, null, null);
        assertFalse("hasPasswordWritableDirectory", defaultUserManager.hasPasswordWritableDirectory());
    }

    @Test
    public void testHasPasswordWritableDirectory_some() throws Exception {
        final Directory readOnlyDirectory = createMockDirectory(1, null, false);
        final Directory writableDelegatedDirectory = createMockDirectory(2, DirectoryType.DELEGATING, true);
        final Directory writableDirectory = createMockDirectory(3, DirectoryType.CONNECTOR, true);

        final CrowdDirectoryService crowdDirectoryService = new MockCrowdDirectoryService();
        crowdDirectoryService.addDirectory(readOnlyDirectory);
        crowdDirectoryService.addDirectory(writableDelegatedDirectory);
        crowdDirectoryService.addDirectory(writableDirectory);

        final DefaultUserManager defaultUserManager = new DefaultUserManager(null, crowdDirectoryService, null, null, null, null, null);
        assertTrue("hasPasswordWritableDirectory", defaultUserManager.hasPasswordWritableDirectory());
    }

    @Test
    public void testUpdateUserEmail() throws Exception {
        final String mail = "mail@example.com";
        final String mailWithWhiteCharacters = " " + mail + "\t";
        final ApplicationUser johnny = new MockApplicationUser("johnny", "John Doe", mailWithWhiteCharacters);

        final UserKeyStore mockKeyStore = new MockUserKeyStore();
        final MockCrowdService mockCrowdService = new MockCrowdService();
        final DefaultUserManager defaultUserManager = new DefaultUserManager(mockCrowdService, null, null, mockKeyStore, null, null, null);
        defaultUserManager.updateUser(johnny);

        final ApplicationUser updatedJohnny = defaultUserManager.getUser("johnny");
        verify(licenseCountService).flush();
        assertEquals(mail, updatedJohnny.getEmailAddress());
    }

    @Test
    public void testRenameUser_simple() throws Exception {
        final DirectoryManager directoryManager = mock(DirectoryManager.class);
        final UserKeyStore userKeyStore = mock(UserKeyStore.class);
        final CrowdService crowdService = mock(CrowdService.class);
        final ConsumerTokenService consumerTokenService = mock(ConsumerTokenService.class);

        new MockComponentWorker()
                .addMock(LicenseCountService.class, licenseCountService)
                .addMock(ConsumerTokenService.class, consumerTokenService)
                .init();

        final ApplicationUser newFred = new MockApplicationUser("FredKey", "NewFred", "Fred Flintstone", "fred@example.com");
        when(userKeyStore.getUsernameForKey("FredKey")).thenReturn("oldfred");

        final DefaultUserManager userManager = new DefaultUserManager(crowdService, null, directoryManager, userKeyStore, null, null, null);
        userManager.updateUser(newFred);

        verify(directoryManager).renameUser(1L, "oldfred", "NewFred");
        verify(userKeyStore, never()).ensureUniqueKeyForNewUser("oldfred");
        verify(crowdService).updateUser(newFred.getDirectoryUser());
        verify(licenseCountService).flush();
    }

    @Test
    public void testRenameUser_unshadow() throws Exception {
        final DirectoryManager directoryManager = mock(DirectoryManager.class);
        final UserKeyStore userKeyStore = mock(UserKeyStore.class);
        final CrowdService crowdService = mock(CrowdService.class);
        final ConsumerTokenService consumerTokenService = mock(ConsumerTokenService.class);
        final ApplicationUser newFred = new MockApplicationUser("FredKey", "NewFred", "Fred Flintstone", "fred@example.com");
        final ImmutableUser unshadowed = new ImmutableUser(2, "oldfred", "Old Fred", "oldfred@example.com", true);

        new MockComponentWorker()
                .addMock(LicenseCountService.class, licenseCountService)
                .addMock(ConsumerTokenService.class, consumerTokenService)
                .init();

        when(userKeyStore.getUsernameForKey("FredKey")).thenReturn("oldfred");
        when(crowdService.getUser("oldfred")).thenReturn(unshadowed);
        when(userKeyStore.ensureUniqueKeyForNewUser("oldfred")).thenReturn("NewKey");

        final DefaultUserManager userManager = new DefaultUserManager(crowdService, null, directoryManager, userKeyStore, null, null, null);
        userManager.updateUser(newFred);

        verify(directoryManager).renameUser(1L, "oldfred", "NewFred");
        verify(userKeyStore).ensureUniqueKeyForNewUser("oldfred");
        verify(crowdService).updateUser(newFred.getDirectoryUser());
        verify(licenseCountService).flush();
    }

    @Test
    public void testRenameUser_exists() throws Exception {
        final DirectoryManager directoryManager = mock(DirectoryManager.class);
        final UserKeyStore userKeyStore = mock(UserKeyStore.class);
        final CrowdService crowdService = mock(CrowdService.class);
        final UserUtil userUtil = mock(UserUtil.class);
        new MockComponentWorker().addMock(UserUtil.class, userUtil).init();

        when(userKeyStore.getUsernameForKey("FredKey")).thenReturn("oldfred");
        when(crowdService.getUser("newfred")).thenReturn(new MockUser("NewFred", "Some Other Fred", "someone@example.com"));

        final ApplicationUser newFred = new MockApplicationUser("FredKey", "NewFred", "Fred Flintstone", "fred@example.com");
        final DefaultUserManager userManager = new DefaultUserManager(crowdService, null, directoryManager, userKeyStore, null, null, null);
        try {
            userManager.updateUser(newFred);
            fail("Should have thrown IllegalArgumentException");
        } catch (IllegalArgumentException iae) {
            // expected
        }

        verify(crowdService).getUser("newfred");
        verify(userKeyStore).getUsernameForKey("FredKey");
        verifyNoMoreInteractions(crowdService, userKeyStore, directoryManager, userUtil);
    }

    @Test
    public void testRenameUser_evictDeleted() throws Exception {
        final DirectoryManager directoryManager = mock(DirectoryManager.class);
        final UserKeyStore userKeyStore = mock(UserKeyStore.class);
        final CrowdService crowdService = mock(CrowdService.class);
        final MockComponentWorker mockComponentWorker = new MockComponentWorker();
        final ConsumerTokenService consumerTokenService = mock(ConsumerTokenService.class);
        mockComponentWorker
                .addMock(LicenseCountService.class, licenseCountService)
                .addMock(ConsumerTokenService.class, consumerTokenService)
                .init();

        final ApplicationUser newFred = new MockApplicationUser("FredKey", "NewFred", "Fred Flintstone", "fred@example.com");
        when(userKeyStore.getUsernameForKey("FredKey")).thenReturn("oldfred");
        when(userKeyStore.getKeyForUsername("newfred")).thenReturn("Deleted user (otherwise crowdService.getUser would have returned it)");
        when(userKeyStore.getKeyForUsername("newfred#1")).thenReturn("Doesn't matter if these really still exist or not");
        when(userKeyStore.getKeyForUsername("newfred#2")).thenReturn("Same here");

        final DefaultUserManager userManager = new DefaultUserManager(crowdService, null, directoryManager, userKeyStore, null, null, null);
        userManager.updateUser(newFred);

        verify(crowdService).getUser("newfred");
        verify(crowdService).getUser("oldfred");
        verify(userKeyStore).getUsernameForKey("FredKey");
        verify(userKeyStore).getKeyForUsername("newfred");
        verify(userKeyStore).getKeyForUsername("newfred#1");
        verify(userKeyStore).getKeyForUsername("newfred#2");
        verify(userKeyStore).getKeyForUsername("newfred#3");
        verify(userKeyStore).renameUser("newfred", "newfred#3");
        // verify(userKeyStore).renameUser("oldfred", "newfred");  // would be invoked indirectly by...
        verify(directoryManager).renameUser(1L, "oldfred", "NewFred");
        verify(crowdService).updateUser(newFred.getDirectoryUser());
        verify(licenseCountService).flush();

        verifyNoMoreInteractions(userKeyStore, directoryManager, crowdService, licenseCountService);
    }

    @Test
    public void testGetUsers() throws Exception {
        final CacheManager cacheManager = new MemoryCacheManager();
        final ClusterLockService clusterLockService = new SimpleClusterLockService();
        final DirectoryDao directoryDao = mock(DirectoryDao.class);
        final OfBizDelegator ofBizDelegator = new MockOfBizDelegator();
        final ApplicationProperties applicationProperties = mock(ApplicationProperties.class);
        final MockDatabaseConfigurationManager databaseConfigurationManager = new MockDatabaseConfigurationManager();
        databaseConfigurationManager.setDatabaseConfiguration(new DatabaseConfig("postgres", "jira", new JndiDatasource("jira")));
        final OfBizUserDao userDao = new OfBizUserDao(ofBizDelegator, directoryDao, null, null, null, cacheManager, clusterLockService, applicationProperties, new DefaultOfBizTransactionManager(), databaseConfigurationManager) {
            @Override
            public long getUniqueUserCount(Set<Long> directoryIds) throws DirectoryNotFoundException {
                return (5L);
            }
        };

        final CrowdDirectoryService crowdDirectoryService = mock(CrowdDirectoryService.class);
        final Directory dir1 = directory(1L, true);
        final Directory dir2 = directory(2L, false);
        final Directory dir3 = directory(3L, true);
        final Directory dir4 = directory(4L, true);
        when(crowdDirectoryService.findAllDirectories()).thenReturn(asList(dir1, dir2, dir3, dir4));

        final User fred1 = user(1L, "fred");
        final User fred2 = user(2L, "fred");
        final User ginny2 = user(2L, "ginny");
        final User george2 = user(2L, "george");
        final User george3 = user(3L, "george");
        final User harry3 = user(3L, "harry");
        final User harry4 = user(4L, "harry");
        final User ron3 = user(3L, "ron");
        final User hermione4 = user(4L, "hermione");
        final User[] expectedUsers = new User[]{fred1, george3, harry3, ron3, hermione4};

        final DirectoryManager directoryManager = mock(DirectoryManager.class);
        mockUsersInDirectory(directoryManager, 1L, fred1);
        mockUsersInDirectory(directoryManager, 2L, fred2, ginny2, george2);
        mockUsersInDirectory(directoryManager, 3L, george3, harry3, ron3);
        mockUsersInDirectory(directoryManager, 4L, harry4, hermione4);

        final MockUserKeyStore mockKeyStore = new MockUserKeyStore();

        final DefaultUserManager userManager = new DefaultUserManager(null, crowdDirectoryService, directoryManager, mockKeyStore, null, null, userDao);
        assertThat(toDirectoryUsers(userManager.getUsers()), Matchers.containsInAnyOrder(expectedUsers));
        assertThat(toDirectoryUsers(userManager.getAllUsers()), Matchers.containsInAnyOrder(expectedUsers));
        assertEquals(expectedUsers.length, userManager.getTotalUserCount());

        //noinspection unchecked
        verify(directoryManager, never()).searchUsers(eq(2L), any(UserQuery.class));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetUserState_NullUsername() throws Exception {
        final DefaultUserManager userManager = new DefaultUserManager(null, null, null, null, null, null, null);
        //noinspection ConstantConditions
        assertEquals(UserState.INVALID_USER, userManager.getUserState(null, 1L));
    }

    @Test
    public void testGetUserState_InvalidUserFromDirectoryIdMinus1() throws Exception {
        final DefaultUserManager userManager = new DefaultUserManager(null, null, null, null, null, null, null);
        assertEquals(UserState.INVALID_USER, userManager.getUserState("fred", -1L));
    }

    @Test
    public void testGetUserState_InvalidUserFromExhaustiveSearch() throws Exception {
        final CrowdDirectoryService crowdDirectoryService = mock(CrowdDirectoryService.class);
        final Directory dir1 = directory(1L, true);
        final Directory dir2 = directory(2L, false);
        final Directory dir3 = directory(3L, true);
        when(crowdDirectoryService.findAllDirectories()).thenReturn(asList(dir1, dir2, dir3));

        final DirectoryManager directoryManager = mock(DirectoryManager.class);
        final UserUtil userUtil = mock(UserUtil.class);
        final MockComponentWorker mockComponentWorker = new MockComponentWorker();
        final ConsumerTokenService consumerTokenService = mock(ConsumerTokenService.class);
        mockComponentWorker
                .addMock(UserUtil.class, userUtil)
                .addMock(ConsumerTokenService.class, consumerTokenService)
                .init();

        final MockUserKeyStore mockKeyStore = new MockUserKeyStore();

        final DefaultUserManager userManager = new DefaultUserManager(null, crowdDirectoryService, directoryManager, mockKeyStore, null, null, null);
        assertEquals(UserState.INVALID_USER, userManager.getUserState("fred", 4L));

        verify(directoryManager).findUserByName(1L, "fred");
        verify(directoryManager, never()).findUserByName(2L, "fred");
        verify(directoryManager).findUserByName(3L, "fred");
    }

    @Test
    public void testGetUserState_InvalidUserFromDisabledDirectoryEvenWhenFoundInOtherFirst() throws Exception {
        final CrowdDirectoryService crowdDirectoryService = mock(CrowdDirectoryService.class);
        final Directory dir1 = directory(1L, true);
        final Directory dir2 = directory(2L, false);
        final Directory dir3 = directory(3L, true);
        when(crowdDirectoryService.findAllDirectories()).thenReturn(asList(dir1, dir2, dir3));

        final DirectoryManager directoryManager = mock(DirectoryManager.class);
        mockUserInDirectory(directoryManager, 1L, "fred");

        final MockUserKeyStore mockKeyStore = new MockUserKeyStore();

        final DefaultUserManager userManager = new DefaultUserManager(null, crowdDirectoryService, directoryManager, mockKeyStore, null, null, null);
        assertEquals(UserState.INVALID_USER, userManager.getUserState("fred", 2L));

        verify(directoryManager).findUserByName(1L, "fred");
        verify(directoryManager, never()).findUserByName(2L, "fred");
        verify(directoryManager, never()).findUserByName(3L, "fred");
    }

    @Test
    public void testGetUserState_IsShadowed() throws Exception {
        final CrowdDirectoryService crowdDirectoryService = mock(CrowdDirectoryService.class);
        final Directory dir1 = directory(1L, true);
        final Directory dir2 = directory(2L, false);
        final Directory dir3 = directory(3L, true);
        final Directory dir4 = directory(4L, true);
        final Directory dir5 = directory(5L, true);
        when(crowdDirectoryService.findAllDirectories()).thenReturn(asList(dir1, dir2, dir3, dir4, dir5));

        final DirectoryManager directoryManager = mock(DirectoryManager.class);
        mockUserInDirectory(directoryManager, 1L, "fred");
        mockUserInDirectory(directoryManager, 4L, "fred");

        final MockUserKeyStore mockKeyStore = new MockUserKeyStore();

        final DefaultUserManager userManager = new DefaultUserManager(null, crowdDirectoryService, directoryManager, mockKeyStore, null, null, null);
        assertEquals(UserState.SHADOW_USER, userManager.getUserState("fred", 4L));

        verify(directoryManager).findUserByName(1L, "fred");
        verify(directoryManager, never()).findUserByName(2L, "fred");
        verify(directoryManager, never()).findUserByName(3L, "fred");
        verify(directoryManager).findUserByName(4L, "fred");
        verify(directoryManager, never()).findUserByName(5L, "fred");
    }

    @Test
    public void testGetUserState_HasShadow() throws Exception {
        final CrowdDirectoryService crowdDirectoryService = mock(CrowdDirectoryService.class);
        final Directory dir1 = directory(1L, true);
        final Directory dir2 = directory(2L, false);
        final Directory dir3 = directory(3L, true);
        final Directory dir4 = directory(4L, true);
        final Directory dir5 = directory(5L, true);
        when(crowdDirectoryService.findAllDirectories()).thenReturn(asList(dir1, dir2, dir3, dir4, dir5));

        final DirectoryManager directoryManager = mock(DirectoryManager.class);
        mockUserInDirectory(directoryManager, 1L, "fred");
        mockUserInDirectory(directoryManager, 4L, "fred");

        final MockUserKeyStore mockKeyStore = new MockUserKeyStore();

        final DefaultUserManager userManager = new DefaultUserManager(null, crowdDirectoryService, directoryManager, mockKeyStore, null, null, null);
        assertEquals(UserState.NORMAL_USER_WITH_SHADOW, userManager.getUserState("fred", 1L));

        verify(directoryManager).findUserByName(1L, "fred");
        verify(directoryManager, never()).findUserByName(2L, "fred");
        verify(directoryManager).findUserByName(3L, "fred");
        verify(directoryManager).findUserByName(4L, "fred");
        verify(directoryManager, never()).findUserByName(5L, "fred");
    }

    private ApplicationUser createMockUser() {
        return new MockApplicationUser("user1");
    }

    private ApplicationUser createMockApplicationUser() {
        return new MockApplicationUser("User1");
    }

    private Directory createMockDirectory(final long id, final DirectoryType type, final boolean writable) {
        final ImmutableDirectory.Builder builder = ImmutableDirectory.newBuilder();
        builder.setId(id);

        if (writable) {
            final Set<OperationType> allowedOperations = new HashSet<OperationType>();
            allowedOperations.add(OperationType.CREATE_USER);
            allowedOperations.add(OperationType.UPDATE_USER);
            builder.setAllowedOperations(allowedOperations);
        }

        builder.setType(type);

        return builder.toDirectory();
    }

    private User user(final long directoryId, final String name) {
        return new ImmutableUser(directoryId, name, name, name + "@example.com", true) {
            @Override
            public String toString() {
                return name + ':' + directoryId;
            }
        };
    }

    private void mockUserInDirectory(final DirectoryManager directoryManager, long directoryId, String userName) throws Exception {
        final com.atlassian.crowd.model.user.User user = mock(com.atlassian.crowd.model.user.User.class);
        when(user.getDirectoryId()).thenReturn(directoryId);
        when(user.getName()).thenReturn(userName);

        // when(...).thenReturn(...) form doesn't work here due to the mismatched User types
        doReturn(user).when(directoryManager).findUserByName(directoryId, userName);
    }

    private void mockUsersInDirectory(final DirectoryManager directoryManager, long directoryId, User... users) throws Exception {
        //noinspection unchecked
        when(directoryManager.searchUsers(eq(directoryId), any(UserQuery.class))).thenReturn(asList(users));

        for (User user : users) {
            mockUserInDirectory(directoryManager, directoryId, user.getName());
        }
    }

    private Directory directory(long id, boolean active) {
        final Directory dir = mock(Directory.class);
        when(dir.getId()).thenReturn(id);
        when(dir.isActive()).thenReturn(active);
        return dir;
    }

    @Test
    public void shouldGeneratePasswordIfNull() {
        //Given
        final DefaultUserManager defaultUserManager = new DefaultUserManager(null, null, null, null, null, null, null);

        //When
        final String generatedPassword = defaultUserManager.generatePasswordIfEmpty(null);

        //Then
        assertNotNull(generatedPassword);
        assertTrue(generatedPassword.length() > 26);
    }

    @Test
    public void shouldGeneratePasswordIfBlank() {
        //Given
        final DefaultUserManager defaultUserManager = new DefaultUserManager(null, null, null, null, null, null, null);

        //When
        final String generatedPassword = defaultUserManager.generatePasswordIfEmpty("");

        //Then
        assertNotNull(generatedPassword);
        assertTrue(generatedPassword.length() > 26);
    }

    @Test
    public void shouldReturnSamePasswordIfNotEmpty() {
        //Given
        final DefaultUserManager defaultUserManager = new DefaultUserManager(null, null, null, null, null, null, null);
        final String expectedPassword = "this is the expected password";

        //When
        final String generatedPassword = defaultUserManager.generatePasswordIfEmpty(expectedPassword);

        //Then
        assertSame(expectedPassword, generatedPassword);
    }

    @Test
    public void shouldTransformApplicationUserRequestToUserTemplate() {
        //Given
        final DefaultUserManager defaultUserManager = new DefaultUserManager(null, null, null, null, null, null, null);
        final UserDetails userData = new UserDetails("username", "displayName")
                .withDirectory(5L)
                .withEmail("  emailAddress   ")
                .withPassword("password");
        //When
        UserTemplate userTemplate = defaultUserManager.from(userData);

        //Then
        assertEquals(userData.getUsername(), userTemplate.getName());
        assertEquals(userData.getDisplayName(), userTemplate.getDisplayName());
        //Email address gets trimmed
        assertEquals("emailAddress", userTemplate.getEmailAddress());
        //Should not set directory id
        assertFalse(userTemplate.getDirectoryId() == 5L);
    }

    @Test
    public void shouldCreateUserInDefaultDirectory() throws Exception {
        //Given
        final CrowdService crowdService = mock(CrowdService.class);
        final User mockUser = mock(User.class);
        when(mockUser.getName()).thenReturn("mockUser");

        final MockUserKeyStore mockKeyStore = new MockUserKeyStore();

        final DefaultUserManager defaultUserManager = new DefaultUserManager(crowdService, null, null, mockKeyStore, null, null, null);

        final MockComponentWorker mockComponentWorker = new MockComponentWorker();
        mockComponentWorker
                .addMock(UserKeyStore.class, mockKeyStore)
                .addMock(UserKeyService.class, new UserKeyServiceImpl(mockKeyStore))
                .addMock(UserManager.class, defaultUserManager)
                .init();

        final UserDetails userData = new UserDetails("username", "displayName")
                .withEmail("  emailAddress   ")
                .withPassword("password");

        when(crowdService.addUser(org.mockito.Matchers.<User>anyObject(), anyString())).thenReturn(mockUser);

        //When
        final ApplicationUser user = defaultUserManager.createUser(userData);

        //Then
        assertEquals(user.getDirectoryUser(), mockUser);
    }

    @Test
    public void shouldCreateUserInSpecifiedDirectory() throws Exception {
        //Given
        final DirectoryManager directoryManager = mock(DirectoryManager.class);
        final com.atlassian.crowd.model.user.User mockUser = mock(com.atlassian.crowd.model.user.User.class);
        when(mockUser.getName()).thenReturn("mockUser");

        final MockUserKeyStore mockKeyStore = new MockUserKeyStore();

        final DefaultUserManager defaultUserManager = new DefaultUserManager(null, null, directoryManager, mockKeyStore, null, null, null);

        final MockComponentWorker mockComponentWorker = new MockComponentWorker();
        mockComponentWorker
                .addMock(UserKeyStore.class, mockKeyStore)
                .addMock(UserKeyService.class, new UserKeyServiceImpl(mockKeyStore))
                .addMock(UserManager.class, defaultUserManager)
                .init();

        final UserDetails userRequest = new UserDetails("username", "displayName")
                .withDirectory(6L).withPassword("password").withEmail("emailAddress   ");
        when(directoryManager.addUser(anyLong(), org.mockito.Matchers.<UserTemplate>any(), org.mockito.Matchers.<PasswordCredential>any())).
                thenReturn(mockUser);

        //When
        final ApplicationUser user = defaultUserManager.createUser(userRequest);

        //Then
        assertEquals(user.getDirectoryUser(), mockUser);
    }

    @Test
    public void shouldReturnFirstWritableDirectoryAsDefault() {
        //Given
        Directory firstWritableDirectory = createMockDirectory(2L, DirectoryType.CUSTOM, true);

        CrowdDirectoryService crowdDirectoryService = mock(CrowdDirectoryService.class);
        when(crowdDirectoryService.findAllDirectories()).thenReturn(ImmutableList.of(
                        createMockDirectory(1L, DirectoryType.CUSTOM, false),
                        firstWritableDirectory,
                        createMockDirectory(3L, DirectoryType.CUSTOM, true))
        );

        final DefaultUserManager defaultUserManager = new DefaultUserManager(null, crowdDirectoryService, null, null, null, null, null);

        //When
        final Optional<Directory> defaultDirectory = defaultUserManager.getDefaultCreateDirectory();

        //Then
        assertSame(firstWritableDirectory, defaultDirectory.get());
    }

    @Test
    public void shouldReturnEmptyWhenAllDirectoriesAreReadonly() {
        //Given
        CrowdDirectoryService crowdDirectoryService = mock(CrowdDirectoryService.class);
        when(crowdDirectoryService.findAllDirectories()).thenReturn(ImmutableList.of(
                        createMockDirectory(1L, DirectoryType.CUSTOM, false),
                        createMockDirectory(2L, DirectoryType.CUSTOM, false),
                        createMockDirectory(3L, DirectoryType.CUSTOM, false))
        );

        final DefaultUserManager defaultUserManager = new DefaultUserManager(null, crowdDirectoryService, null, null, null, null, null);

        //When
        final Optional<Directory> defaultDirectory = defaultUserManager.getDefaultCreateDirectory();

        //Then
        assertSame(Optional.<Directory>empty(), defaultDirectory);
    }

}
