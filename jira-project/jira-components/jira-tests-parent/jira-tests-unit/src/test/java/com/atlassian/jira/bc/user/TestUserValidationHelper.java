package com.atlassian.jira.bc.user;


import com.atlassian.crowd.embedded.api.OperationType;
import com.atlassian.jira.bc.user.UserService.FieldName;
import com.atlassian.jira.bc.user.UserValidationHelper.Validations;
import com.atlassian.jira.crowd.embedded.ofbiz.MockDirectory;
import com.atlassian.jira.plugin.user.PasswordPolicyManager;
import com.atlassian.jira.plugin.user.WebErrorMessage;
import com.atlassian.jira.plugin.user.WebErrorMessageImpl;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.NoopI18nFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.Optional;

import static com.atlassian.jira.matchers.ErrorCollectionMatcher.hasError;
import static com.atlassian.jira.matchers.ErrorCollectionMatcher.hasErrorMessage;
import static com.atlassian.jira.matchers.ErrorCollectionMatcher.hasNoErrors;
import static com.atlassian.jira.matchers.ErrorCollectionMatcher.hasOnlyError;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Test user validations.
 */
@RunWith(MockitoJUnitRunner.class)
public class TestUserValidationHelper {

    private I18nHelper.BeanFactory i18nFactory = new NoopI18nFactory();
    @Mock
    private JiraAuthenticationContext jiraAuthenticationContext;
    @Mock
    private PermissionManager permissionManager;
    @Mock
    private UserManager userManager;
    @Mock
    private PasswordPolicyManager passwordPolicyManager;

    private Validations validations;

    UserValidationHelper userValidationHelper;

    @Before
    public void setUp() throws Exception {
        userValidationHelper = new UserValidationHelper(i18nFactory, jiraAuthenticationContext,
                permissionManager, userManager, passwordPolicyManager);
        validations = userValidationHelper.validations(new MockApplicationUser("mockUser"));
    }

    @Test
    public void testWhenNoUserProvidedDefaultI18nHelperProvided() {
        final I18nHelper mockI18nHelper = mock(I18nHelper.class);
        when(jiraAuthenticationContext.getI18nHelper()).thenReturn(mockI18nHelper);
        //No writable directories
        when(userManager.hasWritableDirectory()).thenReturn(false);

        //When
        validations = userValidationHelper.validations(null);
        when(mockI18nHelper.getText("admin.errors.cannot.add.user.all.directories.read.only")).thenReturn("myValue");
        validations.hasWritableDirectory();

        //Then
        assertThat(validations.getErrors(), hasErrorMessage("myValue"));
    }

    @Test
    public void testContainsMultipleErrors() {
        //No writable directories
        when(userManager.hasWritableDirectory()).thenReturn(false);

        //Then
        assertFalse(validations.hasWritableDirectory());
        assertThat(validations.getErrors(), hasErrorMessage("admin.errors.cannot.add.user.all.directories.read.only{[]}"));
        assertFalse(validations.validateConfirmPassword(null, "aaaa"));
        assertThat(validations.getErrors(), hasError(FieldName.CONFIRM_PASSWORD, "signup.error.password.mustmatch{[]}"));
        assertFalse(validations.validateEmailAddress(new String(new char[256])));
        assertThat(validations.getErrors(), hasError(FieldName.EMAIL, "signup.error.email.greater.than.max.chars{[]}"));
    }

    @Test
    public void testHasWritableDirectory() throws Exception {
        //Given
        //Has a writable directory
        when(userManager.hasWritableDirectory()).thenReturn(true);

        //When
        final boolean passedValidation = validations.hasWritableDirectory();

        //Then
        assertValidationPassed("has writable directory", validations, passedValidation);
        assertThat(validations.getErrors(), hasNoErrors());
    }

    @Test
    public void testDoesNotHaveWritableDirectory() throws Exception {
        //Given
        //No writable directories
        when(userManager.hasWritableDirectory()).thenReturn(false);

        //When
        final boolean passedValidation = validations.hasWritableDirectory();

        //Then
        assertValidationFailed("does not have writable directory", validations, passedValidation);
        assertThat(validations.getErrors(), hasErrorMessage("admin.errors.cannot.add.user.all.directories.read.only{[]}"));
    }

    @Test
    public void testWritableDirectory() throws Exception {
        //Given
        //Has a writable directory
        final MockDirectory mockDirectory = new MockDirectory(1L);
        mockDirectory.addAllowedOperation(OperationType.CREATE_USER);
        when(userManager.getDirectory(1L)).thenReturn(mockDirectory);

        //When
        final boolean passedValidation = validations.writableDirectory(1L);

        //Then
        assertValidationPassed("creation allowed for directory with if 1L", validations, passedValidation);
        assertThat(validations.getErrors(), hasNoErrors());
    }

    @Test
    public void testWritableDirectoryDoesNotExist() throws Exception {
        //When
        final boolean passedValidation = validations.writableDirectory(757L);

        //Then
        assertValidationFailed("random directory id that does not exist", validations, passedValidation);
        assertThat(validations.getErrors(), hasErrorMessage("admin.errors.cannot.add.user.no.such.directory{[757]}"));
    }

    @Test
    public void testWritableDirectoryDoesNotAllowCreate() throws Exception {
        //When
        final MockDirectory mockDirectory = new MockDirectory(1L, "TestDir");
        when(userManager.getDirectory(1L)).thenReturn(mockDirectory);
        final boolean passedValidation = validations.writableDirectory(1L);

        //Then
        assertValidationFailed("directory exist but does not allow create", validations, passedValidation);
        assertThat(validations.getErrors(), hasErrorMessage("admin.errors.cannot.add.user.read.only.directory{[TestDir]}"));
    }

    @Test
    public void testHasCreateAccess() throws Exception {
        //When
        final MockApplicationUser mockUser = new MockApplicationUser("mockUser");
        when(permissionManager.hasPermission(Permissions.ADMINISTER, mockUser)).thenReturn(true);
        final boolean passedValidation = validations.hasCreateAccess(mockUser);

        //Then
        assertValidationPassed("user has admin permission", validations, passedValidation);
        assertThat(validations.getErrors(), hasNoErrors());
    }

    @Test
    public void testUserDoesNotHaveCreateAccess() throws Exception {
        //When
        final boolean passedValidation = validations.hasCreateAccess(new MockApplicationUser("mockUser"));

        //Then
        assertValidationFailed("user does not have admin permission", validations, passedValidation);
        assertThat(validations.getErrors(), hasErrorMessage("admin.errors.user.no.permission.to.create{[]}"));
    }

    @Test
    public void testAnonymousUserDoesNotHaveCreateAccess() throws Exception {
        //When
        when(permissionManager.hasPermission(Permissions.ADMINISTER, null)).thenReturn(false);
        final boolean passedValidation = validations.hasCreateAccess(null);

        //Then
        assertValidationFailed("anonymous users are not admins", validations, passedValidation);
        assertThat(validations.getErrors(), hasErrorMessage("admin.errors.user.no.permission.to.create{[]}"));
    }

    @Test
    public void testHasPasswordRequired() throws Exception {
        //When
        final boolean passedValidation = validations.passwordRequired("password", true);

        //Then
        assertValidationPassed("required password provided", validations, passedValidation);
        assertThat(validations.getErrors(), hasNoErrors());
    }

    @Test
    public void testHasPasswordRequiredWithoutConfirmation() throws Exception {
        //When
        final boolean passedValidation = validations.passwordRequired("password", false);

        //Then
        assertValidationPassed("required password provided", validations, passedValidation);
        assertThat(validations.getErrors(), hasNoErrors());
    }

    @Test
    public void testDoesNotHaveRequiredPassword() throws Exception {
        //When
        final boolean passedValidation = validations.passwordRequired(null, true);

        //Then
        assertValidationFailed("required password not provided", validations, passedValidation);
        assertThat(validations.getErrors(), hasOnlyError(FieldName.PASSWORD, "signup.error.password.required{[]}"));
    }

    @Test
    public void testDoesNotHaveRequiredPasswordWithoutConfirmation() throws Exception {
        //When
        final boolean passedValidation = validations.passwordRequired(null, false);

        //Then
        assertValidationFailed("required password not provided", validations, passedValidation);
        assertThat(validations.getErrors(), hasOnlyError(FieldName.PASSWORD, "signup.error.password.required.without.confirmation{[]}"));
    }

    @Test
    public void testDoesNotHaveRequiredPasswordEmpty() throws Exception {
        //When
        final boolean passedValidation = validations.passwordRequired("", true);

        //Then
        assertValidationFailed("required password empty", validations, passedValidation);
        assertThat(validations.getErrors(), hasOnlyError(FieldName.PASSWORD, "signup.error.password.required{[]}"));
    }

    @Test
    public void testDoesNotHaveRequiredPasswordEmptyWithoutConfirmation() throws Exception {
        //When
        final boolean passedValidation = validations.passwordRequired("", false);

        //Then
        assertValidationFailed("required password empty", validations, passedValidation);
        assertThat(validations.getErrors(), hasOnlyError(FieldName.PASSWORD, "signup.error.password.required.without.confirmation{[]}"));
    }

    @Test
    public void testValidateConfirmPasswordWithNoPasswordProvided() throws Exception {
        //When
        final boolean passedValidation = validations.validateConfirmPassword(null, "aaaa");

        //Then
        assertValidationFailed("confirm password provided but not a password", validations, passedValidation);
        assertThat(validations.getErrors(), hasOnlyError(FieldName.CONFIRM_PASSWORD, "signup.error.password.mustmatch{[]}"));
    }

    @Test
    public void testValidateConfirmPasswordWithNoConfirmPasswordProvided() throws Exception {
        //When
        final boolean passedValidation = validations.validateConfirmPassword("aaaa", null);

        //Then
        assertValidationFailed("no confirm password provided", validations, passedValidation);
        assertThat(validations.getErrors(), hasOnlyError(FieldName.CONFIRM_PASSWORD, "signup.error.password.mustmatch{[]}"));
    }

    @Test
    public void testValidateConfirmPasswordNoneProvided() throws Exception {
        //When
        final boolean passedValidation = validations.validateConfirmPassword(null, null);

        //Then
        assertValidationPassed("no password and no confirm password", validations, passedValidation);
        assertThat(validations.getErrors(), hasNoErrors());
    }

    @Test
    public void testValidateConfirmPasswordDoesNotMatch() throws Exception {
        //When
        final boolean passedValidation = validations.validateConfirmPassword("aa", "bb");

        //Then
        assertValidationFailed("password does not match confirm password", validations, passedValidation);
        assertThat(validations.getErrors(), hasOnlyError(FieldName.CONFIRM_PASSWORD, "signup.error.password.mustmatch{[]}"));
    }

    @Test
    public void testValidateConfirmPasswordMatch() throws Exception {
        //When
        final boolean passedValidation = validations.validateConfirmPassword("aa", "aa");

        //Then
        assertValidationPassed("password and confirm password match", validations, passedValidation);
        assertThat(validations.getErrors(), hasNoErrors());
    }

    @Test
    public void testValidateEmailAddress() throws Exception {
        //When
        final boolean passedValidation = validations.validateEmailAddress("jo@jo");

        //Then
        assertValidationPassed("valid email provided", validations, passedValidation);
        assertThat(validations.getErrors(), hasNoErrors());
    }

    @Test
    public void testValidateEmailAddressNoEmailProvided() throws Exception {
        //When
        final boolean passedValidation = validations.validateEmailAddress(null);

        //Then
        assertValidationFailed("no email provided", validations, passedValidation);
        assertThat(validations.getErrors(), hasOnlyError(FieldName.EMAIL, "signup.error.email.required{[]}"));
    }

    @Test
    public void testValidateEmailAddressEmailAddressToLong() throws Exception {
        //When
        final boolean passedValidation = validations.validateEmailAddress(new String(new char[256]));

        //Then
        assertValidationFailed("provided email exceeds acceptable lenght", validations, passedValidation);
        assertThat(validations.getErrors(), hasOnlyError(FieldName.EMAIL, "signup.error.email.greater.than.max.chars{[]}"));
    }

    @Test
    public void testValidateEmailAddressInvalidEmailAddress() throws Exception {
        //When
        final boolean passedValidation = validations.validateEmailAddress("jo");

        //Then
        assertValidationFailed("invalid email provided", validations, passedValidation);
        assertThat(validations.getErrors(), hasOnlyError(FieldName.EMAIL, "signup.error.email.valid{[]}"));
    }

    @Test
    public void testValidateDisplayNameRequired() throws Exception {
        //When
        final boolean passedValidation = validations.validateDisplayName(null);

        //Then
        assertValidationFailed("no display name provided", validations, passedValidation);
        assertThat(validations.getErrors(), hasOnlyError(FieldName.FULLNAME, "signup.error.fullname.required{[]}"));
    }

    @Test
    public void testValidateDisplayNameFailWhenExceedChars() throws Exception {
        //When
        final boolean passedValidation = validations.validateDisplayName(new String(new char[256]));

        //Then
        assertValidationFailed("display name exceeds acceptable lenght", validations, passedValidation);
        assertThat(validations.getErrors(), hasOnlyError(FieldName.FULLNAME, "signup.error.full.name.greater.than.max.chars{[]}"));
    }

    @Test
    public void testValidateDisplayName() throws Exception {
        //When
        final boolean passedValidation = validations.validateDisplayName("Sammi");

        //Then
        assertValidationPassed("Acceptable display name provided", validations, passedValidation);
        assertThat(validations.getErrors(), hasNoErrors());
    }

    @Test
    public void testDoesNotHaveRequiredUsername() throws Exception {
        //When
        final boolean passedValidation = validations.hasRequiredUsername(null);

        //Then
        assertValidationFailed("required username not provided", validations, passedValidation);
        assertThat(validations.getErrors(), hasOnlyError(FieldName.NAME, "signup.error.username.required{[]}"));
    }

    @Test
    public void testHasRequiredUsername() throws Exception {
        //When
        final boolean passedValidation = validations.hasRequiredUsername("Jo");

        //Then
        assertValidationPassed("has required username", validations, passedValidation);
        assertThat(validations.getErrors(), hasNoErrors());
    }

    @Test
    public void testValidateUsernamePolicy() throws Exception {
        //When
        final boolean passedValidation = validations.validateUsernamePolicy("jo");

        //Then
        assertValidationPassed("username meets username policy", validations, passedValidation);
        assertThat(validations.getErrors(), hasNoErrors());
    }

    @Test
    public void testValidateUsernamePolicyUserNameToLong() throws Exception {
        //When
        final boolean passedValidation = validations.validateUsernamePolicy(new String(new char[256]));

        //Then
        assertValidationFailed("username to long exceeds expected length", validations, passedValidation);
        assertThat(validations.getErrors(), hasOnlyError(FieldName.NAME, "signup.error.username.greater.than.max.chars{[]}"));
    }

    @Test
    public void testValidateUsernamePolicyInvalidChars() throws Exception {
        //When
        final boolean passedValidation = validations.validateUsernamePolicy("{}{P}<>M><");

        //Then
        assertValidationFailed("invalid character in username", validations, passedValidation);
        assertThat(validations.getErrors(), hasOnlyError(FieldName.NAME, "signup.error.username.invalid.chars{[]}"));
    }

    @Test
    public void testUsernameDoesNotExistInDefaultDirectory() throws Exception {
        //When
        final boolean passedValidation = validations.usernameDoesNotExist(null, "jo");

        //Then
        assertValidationPassed("username does not exist and should be free", validations, passedValidation);
        assertThat(validations.getErrors(), hasNoErrors());
    }

    @Test
    public void testUsernameDoesNotExist() throws Exception {
        //When
        final boolean passedValidation = validations.usernameDoesNotExist(1L, "jo");

        //Then
        assertValidationPassed("username does not exist in directory and should be free", validations, passedValidation);
        assertThat(validations.getErrors(), hasNoErrors());
    }

    @Test
    public void testUsernameDoesExistInSpecificDirectory() throws Exception {
        //When
        when(userManager.findUserInDirectory("jo", 1L)).thenReturn(new MockApplicationUser("jo"));
        final boolean passedValidation = validations.usernameDoesNotExist(1L, "jo");

        //Then
        assertValidationFailed("user exist in directroy and should not be used", validations, passedValidation);
        assertThat(validations.getErrors(), hasOnlyError(FieldName.NAME, "signup.error.username.exists{[]}"));
    }

    @Test
    public void testUsernameDoesExistInDefaultDirectory() throws Exception {
        //When
        when(userManager.getUserByName("jo")).thenReturn(new MockApplicationUser("jo"));
        final boolean passedValidation = validations.usernameDoesNotExist(null, "jo");

        //Then
        assertValidationFailed("username exist in default directory and should not be used", validations, passedValidation);
        assertThat(validations.getErrors(), hasOnlyError(FieldName.NAME, "signup.error.username.exists{[]}"));
    }

    @Test
    public void testHasValidUsername() throws Exception {
        //When
        final boolean passedValidation = validations.hasValidUsername("jo", 1L);

        //Then
        assertValidationPassed("username meets all requirements", validations, passedValidation);
        assertThat(validations.getErrors(), hasNoErrors());
    }

    @Test
    public void testHasValidUsernameFailsWhenRequiredUsernameNotProvided() throws Exception {
        //When
        final boolean passedValidation = validations.hasValidUsername(null, null);

        //Then
        assertValidationFailed("no username provided", validations, passedValidation);
        assertThat(validations.getErrors(), hasOnlyError(FieldName.NAME, "signup.error.username.required{[]}"));
    }

    @Test
    public void testHasWritableDefaultCreateDirectory() throws Exception {
        //When
        when(userManager.getDefaultCreateDirectory()).thenReturn(Optional.of(new MockDirectory(1L)));
        final boolean passedValidation = validations.hasWritableDefaultCreateDirectory();

        //Then
        assertValidationPassed("directory should be writable", validations, passedValidation);
        assertThat(validations.getErrors(), hasNoErrors());
    }

    @Test
    public void testHasNoWritableDefaultCreateDirectory() throws Exception {
        //When
        when(userManager.getDefaultCreateDirectory()).thenReturn(Optional.empty());
        final boolean passedValidation = validations.hasWritableDefaultCreateDirectory();

        //Then
        assertValidationFailed("no default create directory", validations, passedValidation);
        assertThat(validations.getErrors(), hasErrorMessage("admin.errors.cannot.add.user.all.directories.read.only{[]}"));
    }

    @Test
    public void testPasswordConformsToPasswordPolicy() {
        //When
        List<WebErrorMessage> webErrorMessagesFromPassPolicyMan = new ArrayList<>();
        when(passwordPolicyManager.checkPolicy("", "", "", "")).thenReturn(webErrorMessagesFromPassPolicyMan);
        final List<WebErrorMessage> webErrors = validations.validatePasswordPolicy("", "", "", "");

        //Then
        assertThat(webErrorMessagesFromPassPolicyMan, is(webErrors));
        assertThat(validations.getErrors(), hasNoErrors());
    }

    @Test
    public void testPasswordDoesNotConformToPasswordPolicy() {
        //When
        List<WebErrorMessage> webErrorMessagesFromPassPolicyMan = new ArrayList<>();
        webErrorMessagesFromPassPolicyMan.add(new WebErrorMessageImpl("", "", null));
        when(passwordPolicyManager.checkPolicy("", "", "", "")).thenReturn(webErrorMessagesFromPassPolicyMan);
        final List<WebErrorMessage> webErrors = validations.validatePasswordPolicy("", "", "", "");

        //Then
        assertThat(webErrorMessagesFromPassPolicyMan, is(webErrors));
        assertThat(validations.getErrors(), hasOnlyError(FieldName.PASSWORD, "signup.error.password.rejected{[]}"));
    }

    private void assertValidationPassed(final String reason, final Validations validations, final boolean passedValidation) {
        assertTrue("Validation expected to pass, " + reason + "." + toStringError(validations), passedValidation);
    }

    private void assertValidationFailed(final String reason, final Validations validations, final boolean passedValidation) {
        assertFalse("Validation expected to fail, " + reason + "." + toStringError(validations), passedValidation);
    }

    private String toStringError(final Validations validations) {
        final ErrorCollection errors = validations.getErrors();
        final StringBuffer buffer = new StringBuffer();
        if (errors == null || !errors.hasAnyErrors()) {
            buffer.append(" No errors found in validation result.");
        } else {
            buffer.append("\n");
            buffer.append("Errors : ");
            for (String errorMessages : errors.getErrorMessages()) {
                buffer.append(errorMessages);
                buffer.append("\n");
            }
            for (ErrorCollection.Reason reason : errors.getReasons()) {
                buffer.append(reason.toString());
                buffer.append("\n");
            }
            for (Entry<String, String> entry : errors.getErrors().entrySet()) {
                buffer.append(entry.getKey());
                buffer.append(":");
                buffer.append(entry.getValue());
                buffer.append("\n");
            }
        }
        return buffer.toString();
    }
}