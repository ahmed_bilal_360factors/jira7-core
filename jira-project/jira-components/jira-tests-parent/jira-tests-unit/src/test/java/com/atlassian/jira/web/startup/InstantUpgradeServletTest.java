package com.atlassian.jira.web.startup;

import com.atlassian.jira.bc.ServiceOutcomeImpl;
import com.atlassian.jira.junit.rules.MockComponentContainer;
import com.atlassian.jira.mock.servlet.MockHttpServletResponse;
import com.atlassian.jira.startup.InstantUpgradeManager;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.StringWriter;

import static javax.servlet.http.HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
import static javax.servlet.http.HttpServletResponse.SC_OK;
import static javax.servlet.http.HttpServletResponse.SC_SERVICE_UNAVAILABLE;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class InstantUpgradeServletTest {

    @Rule
    public MockComponentContainer container = new MockComponentContainer(this);
    
    @Mock
    private InstantUpgradeManager instantUpgradeManager;

    @Mock
    HttpServletRequest request;

    private MockHttpServletResponse response;
    private StringWriter responseOutput;
    private InstantUpgradeServlet instantUpgradeServlet;

    @Before
    public void setUp() throws Exception {
        instantUpgradeServlet = new InstantUpgradeServlet();
        responseOutput = new StringWriter();
        response = new MockHttpServletResponse(responseOutput);
    }
    
    @Test
    public void testNotAvailable() throws ServletException, IOException {
        instantUpgradeServlet.doGet(request, response);
        assertEquals(SC_SERVICE_UNAVAILABLE, response.getStatus());
        assertThat(responseOutput.toString(), is("{\"message\":\"instant upgrade manager is not available\"}"));
    }

    @Test
    public void testGetStatusStarting() throws Exception {
        container.addMock(InstantUpgradeManager.class, instantUpgradeManager);
                
        when(instantUpgradeManager.getState()).thenReturn(InstantUpgradeManager.State.EARLY_STARTUP);
        instantUpgradeServlet.doGet(request, response);
        assertEquals(SC_OK, response.getStatus());
        assertThat(responseOutput.toString(), is("{\"state\":\"EARLY_STARTUP\"}"));
    }

    @Test
    public void testGetStatusWaiting() throws Exception {
        container.addMock(InstantUpgradeManager.class, instantUpgradeManager);

        when(instantUpgradeManager.getState()).thenReturn(InstantUpgradeManager.State.WAITING_FOR_ACTIVATION);
        instantUpgradeServlet.doGet(request, response);
        assertEquals(SC_OK, response.getStatus());
        assertThat(responseOutput.toString(), is("{\"state\":\"WAITING_FOR_ACTIVATION\"}"));
    }

    @Test
    public void testGetStatusActivated() throws Exception {
        container.addMock(InstantUpgradeManager.class, instantUpgradeManager);
        
        when(instantUpgradeManager.getState()).thenReturn(InstantUpgradeManager.State.LATE_STARTUP);
        instantUpgradeServlet.doGet(request, response);
        assertEquals(SC_OK, response.getStatus());
        assertThat(responseOutput.toString(), is("{\"state\":\"LATE_STARTUP\"}"));
    }

    @Test
    public void testActivateInstanceError() throws Exception {
        container.addMock(InstantUpgradeManager.class, instantUpgradeManager);

        when(instantUpgradeManager.activateInstance()).thenReturn(ServiceOutcomeImpl.error("Some error."));
        when(instantUpgradeManager.getState()).thenReturn(InstantUpgradeManager.State.WAITING_FOR_ACTIVATION);
        
        instantUpgradeServlet.doPut(request, response);
        assertEquals(SC_INTERNAL_SERVER_ERROR, response.getStatus());
        assertThat(responseOutput.toString(), is("{\"message\":[\"Some error.\"]}"));
    }

    @Test
    public void testActivateInstanceOk() throws Exception {
        container.addMock(InstantUpgradeManager.class, instantUpgradeManager);

        when(instantUpgradeManager.activateInstance()).thenReturn(ServiceOutcomeImpl.ok(null));
        when(instantUpgradeManager.getState()).thenReturn(InstantUpgradeManager.State.WAITING_FOR_ACTIVATION);
        instantUpgradeServlet.doPut(request, response);
        assertEquals(SC_OK, response.getStatus());
        assertThat(responseOutput.toString(), is(""));
    }
}
