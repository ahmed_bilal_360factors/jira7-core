package com.atlassian.jira.issue.statistics.util;

import com.atlassian.jira.bc.project.component.MockProjectComponent;
import com.atlassian.jira.bc.project.component.ProjectComponent;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.IssueConstant;
import com.atlassian.jira.issue.statistics.FilterStatisticsValuesGenerator;
import com.atlassian.jira.mock.i18n.MockI18nHelper;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.util.I18nHelper;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.ofbiz.core.entity.GenericValue;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

/**
 * Unit test for {@link com.atlassian.jira.issue.statistics.util.DefaultFieldValueToDisplayTransformer}
 *
 * @since v4.1
 */
public class TestDefaultFieldValueToDisplayTransformer {
    @Rule
    public MethodRule initMockito = MockitoJUnit.rule();
    @Mock
    private CustomFieldManager customFieldManager;
    @Mock
    private GenericValue mockGenericValue;
    @Mock
    private Version version;
    @Mock
    private IssueConstant mockIssueConstant;

    final I18nHelper i18nBean = new MockI18nHelper();
    final FieldValueToDisplayTransformer<String> transformer = new DefaultFieldValueToDisplayTransformer(i18nBean,
            customFieldManager);
    final String url = "http://www.lolcats.com";
    final String expectedDisplayValue = "LOLCATS";

    @Test
    public void testTransformVersionAndAllVersion() throws Exception {
        when(version.getName()).thenReturn(expectedDisplayValue);
        final String versionResult = ObjectToFieldValueMapper.transform(FilterStatisticsValuesGenerator.VERSION, version, url, transformer);
        assertEquals(versionResult, expectedDisplayValue);

        final String allVersionResult = ObjectToFieldValueMapper.transform(FilterStatisticsValuesGenerator.ALLVERSION, version, url, transformer);
        assertEquals(allVersionResult, expectedDisplayValue);

        final String noVersionResult = ObjectToFieldValueMapper.transform(FilterStatisticsValuesGenerator.VERSION, null, url, transformer);
        assertEquals(noVersionResult, i18nBean.getText("gadget.filterstats.raisedin.unscheduled"));

        final String noAllVersionResult = ObjectToFieldValueMapper.transform(FilterStatisticsValuesGenerator.ALLVERSION, null, url, transformer);
        assertEquals(noAllVersionResult, i18nBean.getText("gadget.filterstats.raisedin.unscheduled"));

    }

    @Test
    public void testTransformFixForAndAllFixFor() throws Exception {
        when(version.getName()).thenReturn(expectedDisplayValue);
        final String fixForResult = ObjectToFieldValueMapper.transform(FilterStatisticsValuesGenerator.FIXFOR, version, url, transformer);
        assertEquals(fixForResult, expectedDisplayValue);

        when(version.getName()).thenReturn(expectedDisplayValue);
        final String allFixForResult = ObjectToFieldValueMapper.transform(FilterStatisticsValuesGenerator.ALLFIXFOR, version, url, transformer);
        assertEquals(allFixForResult, expectedDisplayValue);

        final String noFixForResult = ObjectToFieldValueMapper.transform(FilterStatisticsValuesGenerator.FIXFOR, null, url, transformer);
        assertEquals(noFixForResult, i18nBean.getText("gadget.filterstats.fixfor.unscheduled"));

        final String noAllFixForResult = ObjectToFieldValueMapper.transform(FilterStatisticsValuesGenerator.ALLFIXFOR, null, url, transformer);
        assertEquals(noAllFixForResult, i18nBean.getText("gadget.filterstats.fixfor.unscheduled"));
    }

    @Test
    public void testIrrelevant() throws Exception {
        final String result = ObjectToFieldValueMapper.transform(null, FilterStatisticsValuesGenerator.IRRELEVANT, null, transformer);
        assertEquals(result, i18nBean.getText("common.concepts.irrelevant"));
    }

    @Test
    public void testProject() throws Exception {
        final Project project = new MockProject(10000L, "blah", expectedDisplayValue);
        final String result = ObjectToFieldValueMapper.transform(FilterStatisticsValuesGenerator.PROJECT, project, null, transformer);
        assertEquals(result, expectedDisplayValue);
    }

    @Test
    public void testComponents() throws Exception {
        final ProjectComponent component = new MockProjectComponent(1l, "LOLCATS");

        final String result = ObjectToFieldValueMapper.transform(FilterStatisticsValuesGenerator.COMPONENTS, component, null, transformer);
        assertEquals(result, expectedDisplayValue);

        final String noComponentResult = ObjectToFieldValueMapper.transform(FilterStatisticsValuesGenerator.COMPONENTS, null, null, transformer);
        assertEquals(noComponentResult, i18nBean.getText("gadget.filterstats.component.nocomponent"));
    }

    @Test
    public void testAssigneesAndReporter() throws Exception {
        final ApplicationUser mockUser = new MockApplicationUser("test", expectedDisplayValue, "someguy@hotmail.com");
        final String result = ObjectToFieldValueMapper.transform(FilterStatisticsValuesGenerator.ASSIGNEES, mockUser, null, transformer);
        assertEquals(result, expectedDisplayValue);

        final String noAssigneeResult = ObjectToFieldValueMapper.transform(FilterStatisticsValuesGenerator.ASSIGNEES, null, null, transformer);
        assertEquals(noAssigneeResult, i18nBean.getText("gadget.filterstats.assignee.unassigned"));

        final String reporterResult = ObjectToFieldValueMapper.transform(FilterStatisticsValuesGenerator.REPORTER, mockUser, null, transformer);
        assertEquals(reporterResult, expectedDisplayValue);

        final String noReporterResult = ObjectToFieldValueMapper.transform(FilterStatisticsValuesGenerator.REPORTER, null, null, transformer);
        assertEquals(noReporterResult, i18nBean.getText("gadget.filterstats.reporter.unknown"));

    }

    @Test
    public void testCreator() throws Exception {
        final ApplicationUser mockUser = new MockApplicationUser("test", expectedDisplayValue, "someguy@hotmail.com");
        final String result = ObjectToFieldValueMapper.transform(FilterStatisticsValuesGenerator.CREATOR, mockUser, null, transformer);
        assertEquals(result, expectedDisplayValue);

        final String noCreatorResult = ObjectToFieldValueMapper.transform(FilterStatisticsValuesGenerator.CREATOR, null, null, transformer);
        assertEquals(noCreatorResult, i18nBean.getText("gadget.filterstats.creator.unknown"));
    }

    @Test
    public void testCustomField() throws Exception {

        final String noCustomResult = ObjectToFieldValueMapper.transform("Some custom field", null, null, transformer);
        assertEquals(noCustomResult, i18nBean.getText("common.words.none"));

    }

    @Test
    public void testPriorities() throws Exception {

        when(mockIssueConstant.getNameTranslation()).thenReturn(expectedDisplayValue);

        final String prioritiesResult = ObjectToFieldValueMapper.transform(FilterStatisticsValuesGenerator.PRIORITIES, mockIssueConstant, null, transformer);
        assertEquals(prioritiesResult, expectedDisplayValue);

        final String noPrioritiesResult = ObjectToFieldValueMapper.transform(FilterStatisticsValuesGenerator.PRIORITIES, null, null, transformer);
        assertEquals(noPrioritiesResult, i18nBean.getText("gadget.filterstats.priority.nopriority"));

    }

    @Test
    public void testIssueTypes() throws Exception {

        when(mockIssueConstant.getNameTranslation()).thenReturn(expectedDisplayValue);

        final String issueTypesResult = ObjectToFieldValueMapper.transform(FilterStatisticsValuesGenerator.ISSUETYPE, mockIssueConstant, null, transformer);
        assertEquals(issueTypesResult, expectedDisplayValue);

    }

    @Test
    public void testResolution() throws Exception {

        when(mockIssueConstant.getNameTranslation()).thenReturn(expectedDisplayValue);

        final String resolutionResult = ObjectToFieldValueMapper.transform(FilterStatisticsValuesGenerator.RESOLUTION, mockIssueConstant, null, transformer);
        assertEquals(resolutionResult, expectedDisplayValue);

        final String noResolutionResult = ObjectToFieldValueMapper.transform(FilterStatisticsValuesGenerator.RESOLUTION, null, null, transformer);
        assertEquals(noResolutionResult, i18nBean.getText("common.resolution.unresolved"));
    }


}
