package com.atlassian.jira.issue.index;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.config.ReindexMessageManager;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.config.util.IndexingConfiguration;
import com.atlassian.jira.config.util.MockIndexPathManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.util.IssuesIterable;
import com.atlassian.jira.mock.issue.MockIssue;
import com.atlassian.jira.mock.ofbiz.MockGenericValue;
import com.atlassian.jira.util.Consumer;
import com.atlassian.johnson.event.Event;
import com.atlassian.johnson.event.EventType;
import com.google.common.collect.Lists;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.ofbiz.core.entity.GenericValue;

import java.util.Collection;
import java.util.Collections;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verifyNoMoreInteractions;

public class TestBulkOnlyIndexManager {
    @Rule
    public MockitoRule mockito = MockitoJUnit.rule();

    @Mock
    private ApplicationProperties mockApplicationProperties;
    @Mock
    private IssueIndexer issueIndexer;
    @Mock
    private ReindexMessageManager reindexMessageManager;
    @Mock
    private EventPublisher eventPublisher;
    private BulkOnlyIndexManager indexManager;
    private final Event event = new Event(new EventType("social", "butterfly"), "party");
    private final IssuesIterable issuesIterable = new IssuesIterable() {
        public int size() {
            throw new UnsupportedOperationException();
        }

        public boolean isEmpty() {
            throw new UnsupportedOperationException();
        }

        public void foreach(final Consumer<Issue> sink) {
            throw new UnsupportedOperationException();
        }
    };

    @Before
    public void setUp() throws Exception {
        indexManager = new BulkOnlyIndexManager(new IndexingConfiguration.PropertiesAdapter(mockApplicationProperties),
                issueIndexer, new MockIndexPathManager(), reindexMessageManager, eventPublisher, null, null, null, null, null, null, null, null);
    }

    @After
    public void verifyMocks() {
        // these guys should NEVER get called
        verifyNoMoreInteractions(mockApplicationProperties);
        verifyNoMoreInteractions(issueIndexer);
    }

    /**
     * Test that all the methods we want overridden are declared by BulkOnlyIndexManager
     *
     * @throws NoSuchMethodException if any of the methods are not declared by BulkOnlyIndexManager
     */
    @Test
    public void testMethodsDeclared() throws NoSuchMethodException {
        final Class<BulkOnlyIndexManager> theClass = BulkOnlyIndexManager.class;
        theClass.getDeclaredMethod("deIndex", new Class[]{GenericValue.class});
        theClass.getDeclaredMethod("reIndex", new Class[]{GenericValue.class});
        theClass.getDeclaredMethod("reIndex", new Class[]{Issue.class});
        theClass.getDeclaredMethod("reIndexAll", new Class[0]);
        theClass.getDeclaredMethod("reIndexAll", new Class[]{Event.class});
        theClass.getDeclaredMethod("reIndexIssueObjects", new Class[]{Collection.class});
        theClass.getDeclaredMethod("reIndexIssues", new Class[]{IssuesIterable.class, Event.class});
        theClass.getDeclaredMethod("reIndexIssues", new Class[]{Collection.class});
        theClass.getDeclaredMethod("optimize", new Class[0]);
    }

    @Test
    public void testReIndexAll() throws IndexException {
        assertEquals(-1, indexManager.reIndexAll());
    }

    @Test
    public void testReIndexAllWithEvent() throws IndexException {
        assertEquals(-1, indexManager.reIndexAll(event));
    }

    @Test
    public void testReIndexIssueObjects() throws IndexException {
        assertEquals(-1, indexManager.reIndexIssueObjects(Collections.<Issue>emptyList()));
        final Collection<MockIssue> issueObjects = Lists.newArrayList(new MockIssue(), new MockIssue());
        assertEquals(-1, indexManager.reIndexIssueObjects(issueObjects));
    }

    @Test
    public void testReIndexIssues() throws IndexException {
        assertEquals(-1, indexManager.reIndexIssues(issuesIterable, event));
    }

    @Test
    public void testReIndexIssuesWithIssues() throws IndexException {
        assertEquals(-1, indexManager.reIndexIssues(Collections.<GenericValue>emptyList()));
        final Collection<GenericValue> issueObjects = Lists.<GenericValue>newArrayList(new MockGenericValue("Issue"), new MockGenericValue("Issue"));
        assertEquals(-1, indexManager.reIndexIssues(issueObjects));
    }

    @Test
    public void testOptimize() throws IndexException {
        assertEquals(-1, indexManager.optimize());
    }
}
