package com.atlassian.jira.bc.security.login;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class LoginInfoImplTest {
    @Test
    public void builderSetsCorrectFields() {
        final LoginInfo build = LoginInfoImpl.builder()
                .setCurrentFailedLoginCount(1L)
                .setElevatedSecurityCheckRequired(true)
                .setLastFailedLoginTime(2L)
                .setLastLoginTime(3L)
                .setLoginCount(4l)
                .setMaxAuthenticationAttemptsAllowed(5L)
                .setPreviousLoginTime(6L)
                .setTotalFailedLoginCount(7L)
                .build();

        assertEquals(1L, (long) build.getCurrentFailedLoginCount());
        assertTrue(build.isElevatedSecurityCheckRequired());
        assertEquals(2L, (long) build.getLastFailedLoginTime());
        assertEquals(3L, (long) build.getLastLoginTime());
        assertEquals(4L, (long) build.getLoginCount());
        assertEquals(5L, (long) build.getMaxAuthenticationAttemptsAllowed());
        assertEquals(6L, (long) build.getPreviousLoginTime());
        assertEquals(7L, (long) build.getTotalFailedLoginCount());
    }

    @Test
    public void builderCopiesCorrectFields() {
        final LoginInfo source = LoginInfoImpl.builder()
                .setCurrentFailedLoginCount(1L)
                .setElevatedSecurityCheckRequired(true)
                .setLastFailedLoginTime(2L)
                .setLastLoginTime(3L)
                .setLoginCount(4l)
                .setMaxAuthenticationAttemptsAllowed(5L)
                .setPreviousLoginTime(6L)
                .setTotalFailedLoginCount(7L)
                .build();

        final LoginInfo copy = LoginInfoImpl.builder(source).build();

        assertEquals(1L, (long) copy.getCurrentFailedLoginCount());
        assertTrue(copy.isElevatedSecurityCheckRequired());
        assertEquals(2L, (long) copy.getLastFailedLoginTime());
        assertEquals(3L, (long) copy.getLastLoginTime());
        assertEquals(4L, (long) copy.getLoginCount());
        assertEquals(5L, (long) copy.getMaxAuthenticationAttemptsAllowed());
        assertEquals(6L, (long) copy.getPreviousLoginTime());
        assertEquals(7L, (long) copy.getTotalFailedLoginCount());
    }

    @Test
    public void successAtSetsCorrectFields() {
        final LoginInfo build = LoginInfoImpl.builder()
                .setCurrentFailedLoginCount(1L)
                .setElevatedSecurityCheckRequired(true)
                .setLastFailedLoginTime(2L)
                .setLastLoginTime(3L)
                .setLoginCount(4l)
                .setMaxAuthenticationAttemptsAllowed(5L)
                .setPreviousLoginTime(6L)
                .setTotalFailedLoginCount(7L)
                .succeededAt(8L).build();

        assertEquals(0L, (long) build.getCurrentFailedLoginCount());
        assertTrue(build.isElevatedSecurityCheckRequired());
        assertEquals(0L, (long) build.getLastFailedLoginTime());
        assertEquals(8L, (long) build.getLastLoginTime());
        assertEquals(5L, (long) build.getLoginCount());
        assertEquals(5L, (long) build.getMaxAuthenticationAttemptsAllowed());
        assertEquals(3L, (long) build.getPreviousLoginTime());
        assertEquals(7L, (long) build.getTotalFailedLoginCount());
    }

    @Test
    public void successAtSetsCorrectFieldsForEmptyLoginInfo() {
        final LoginInfo build = LoginInfoImpl.builder().succeededAt(8L).build();

        assertEquals(0L, (long) build.getCurrentFailedLoginCount());
        assertFalse(build.isElevatedSecurityCheckRequired());
        assertEquals(0L, (long) build.getLastFailedLoginTime());
        assertEquals(8L, (long) build.getLastLoginTime());
        assertEquals(1L, (long) build.getLoginCount());
        assertNull(build.getMaxAuthenticationAttemptsAllowed());
        assertNull(build.getPreviousLoginTime());
        assertNull(build.getTotalFailedLoginCount());
    }

    @Test
    public void failureAtSetsCorrectFields() {
        final LoginInfo build = LoginInfoImpl.builder()
                .setCurrentFailedLoginCount(1L)
                .setElevatedSecurityCheckRequired(true)
                .setLastFailedLoginTime(2L)
                .setLastLoginTime(3L)
                .setLoginCount(4l)
                .setMaxAuthenticationAttemptsAllowed(5L)
                .setPreviousLoginTime(6L)
                .setTotalFailedLoginCount(7L)
                .failedAt(8L).build();

        assertEquals(2L, (long) build.getCurrentFailedLoginCount());
        assertTrue(build.isElevatedSecurityCheckRequired());
        assertEquals(8L, (long) build.getLastFailedLoginTime());
        assertEquals(3L, (long) build.getLastLoginTime());
        assertEquals(4L, (long) build.getLoginCount());
        assertEquals(5L, (long) build.getMaxAuthenticationAttemptsAllowed());
        assertEquals(6L, (long) build.getPreviousLoginTime());
        assertEquals(8L, (long) build.getTotalFailedLoginCount());
    }

    @Test
    public void failedAtSetsCorrectFieldsForEmptyLoginInfo() {
        final LoginInfo build = LoginInfoImpl.builder().failedAt(8L).build();

        assertEquals(1L, (long) build.getCurrentFailedLoginCount());
        assertFalse(build.isElevatedSecurityCheckRequired());
        assertEquals(8L, (long) build.getLastFailedLoginTime());
        assertNull(build.getLastLoginTime());
        assertNull(build.getLoginCount());
        assertNull(build.getMaxAuthenticationAttemptsAllowed());
        assertNull(build.getPreviousLoginTime());
        assertEquals(1L, (long) build.getTotalFailedLoginCount());
    }
}