package com.atlassian.jira.upgrade.tasks;

import com.atlassian.jira.upgrade.tasks.role.RenaissanceMigration;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static java.lang.Integer.parseInt;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.only;
import static org.mockito.Mockito.verify;

/**
 * See {@link UpgradeTask_Build70100} for migration details.
 */
public class TestUpgradeTask_Build70100 {
    @Rule
    public final MockitoRule initMocks = MockitoJUnit.rule();

    @Mock
    private RenaissanceMigration migration;
    private UpgradeTask_Build70100 upgrade;

    @Before
    public void setUp() throws Exception {
        upgrade = new UpgradeTask_Build70100(() -> migration);
    }

    @Test
    public void mirgateShouldDeletegateInSetupMode() {
        //when
        upgrade.doUpgrade(true);

        //then
        verify(migration, only()).migrate();
    }

    @Test
    public void mirgateShouldDeletegateOtherMode() {
        //when
        upgrade.doUpgrade(false);

        //then
        verify(migration, only()).migrate();
    }

    @Test
    public void buildNumberCorrect() {
        //then
        assertThat(upgrade.getBuildNumber(), Matchers.equalTo(70100));
    }

    @Test
    public void downgradeTaskRequired() {
        //when
        final boolean downgradeRequired = upgrade.isDowngradeTaskRequired();

        //then
        assertThat(downgradeRequired, Matchers.equalTo(downgradeRequired));
    }
}