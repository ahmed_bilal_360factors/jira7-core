package com.atlassian.jira.issue.fields.csv;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.export.ExportableSystemField;
import com.atlassian.jira.issue.fields.CreatedSystemField;
import com.atlassian.jira.issue.fields.DueDateSystemField;
import com.atlassian.jira.issue.fields.ResolutionDateSystemField;
import com.atlassian.jira.issue.fields.UpdatedSystemField;
import com.atlassian.jira.issue.fields.csv.util.StubCsvDateFormatter;
import com.atlassian.jira.issue.search.handlers.UpdatedDateSearchHandlerFactory;
import com.atlassian.jira.issue.views.util.csv.CsvDateFormatter;
import com.atlassian.jira.jql.operand.JqlOperandResolver;
import com.atlassian.jira.jql.permission.FieldClausePermissionChecker;
import com.atlassian.jira.jql.query.UpdatedDateClauseQueryFactory;
import com.atlassian.jira.jql.util.JqlDateSupport;
import com.atlassian.jira.jql.validator.UpdatedDateValidator;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.timezone.TimeZoneManager;
import com.atlassian.jira.util.ComponentFactory;
import com.atlassian.jira.util.NoopI18nHelper;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.mockito.Mock;

import java.sql.Timestamp;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@Ignore("JSEV-460")
public abstract class DateSystemFieldsCsvExportTest<T extends ExportableSystemField> {
    enum Type {
        DATE, DATETIME
    }

    protected DateSystemFieldsCsvExportTest() {
        this(Type.DATETIME);
    }

    protected DateSystemFieldsCsvExportTest(final Type type) {
        this.type = type;
    }

    @Rule
    public final RuleChain mockito = MockitoMocksInContainer.forTest(this);

    @Mock
    static private JiraAuthenticationContext authenticationContext;

    private static final DateTime NOW = DateTime.now();

    @Mock
    private Issue issue;

    abstract protected T createField(CsvDateFormatter csvDateFormatter);

    abstract protected void setUpIssueWithDate(Issue issueMock, Timestamp date);

    private final Type type;


    @Before
    public void setUp() throws Exception {
        when(authenticationContext.getI18nHelper()).thenReturn(new NoopI18nHelper());
    }

    @Test
    public void fieldFormatsDateWithCsvDateFormatter() {
        CsvDateFormatter csvDateFormatter = new StubCsvDateFormatter();
        Timestamp now = new Timestamp(NOW.toDate().getTime());

        ExportableSystemField field = createField(csvDateFormatter);
        setUpIssueWithDate(issue, now);

        assertThat(field.getRepresentationFromIssue(issue).getParts(), hasSize(1));
        String expected = type == Type.DATETIME ? csvDateFormatter.formatDateTime(now) : csvDateFormatter.formatDate(now);
        assertThat(field.getRepresentationFromIssue(issue).getParts().get(0).getValues()::iterator, contains(expected));
    }

    public static class CreatedSystemFieldCsvExportTest extends DateSystemFieldsCsvExportTest<CreatedSystemField> {
        @Override
        protected CreatedSystemField createField(CsvDateFormatter dateFormatter) {
            return new CreatedSystemField(null, null, authenticationContext, null, null, null, dateFormatter);
        }

        @Override
        protected void setUpIssueWithDate(final Issue issueMock, final Timestamp date) {
            when(issueMock.getCreated()).thenReturn(date);
        }
    }

    public static class DueDateSystemFieldCsvExportTest extends DateSystemFieldsCsvExportTest<DueDateSystemField> {

        public DueDateSystemFieldCsvExportTest() {
            super(Type.DATE);
        }

        @Override
        protected DueDateSystemField createField(final CsvDateFormatter csvDateFormatter) {
            return new DueDateSystemField(null, null, null, authenticationContext, null, null, null, csvDateFormatter);
        }

        @Override
        protected void setUpIssueWithDate(final Issue issueMock, final Timestamp date) {
            when(issueMock.getDueDate()).thenReturn(date);
        }
    }

    public static class ResolutionDateSystemFieldCsvExportTest extends DateSystemFieldsCsvExportTest<ResolutionDateSystemField> {

        @Override
        protected ResolutionDateSystemField createField(final CsvDateFormatter csvDateFormatter) {
            return new ResolutionDateSystemField(null, null, authenticationContext, null, null, null, csvDateFormatter);
        }

        @Override
        protected void setUpIssueWithDate(final Issue issueMock, final Timestamp date) {
            when(issueMock.getResolutionDate()).thenReturn(date);
        }
    }

    public static class UpdatedSystemFieldCsvExportTest extends DateSystemFieldsCsvExportTest<UpdatedSystemField> {
        @Override
        protected UpdatedSystemField createField(final CsvDateFormatter csvDateFormatter) {
            return new UpdatedSystemField(null, null, authenticationContext, new UpdatedDateSearchHandlerFactory(mock(ComponentFactory.class), new UpdatedDateClauseQueryFactory(mock(JqlDateSupport.class), mock(JqlOperandResolver.class)), new UpdatedDateValidator(mock(JqlOperandResolver.class), mock(TimeZoneManager.class)), mock(FieldClausePermissionChecker.Factory.class)), null, null, csvDateFormatter);
        }

        @Override
        protected void setUpIssueWithDate(final Issue issueMock, final Timestamp date) {
            when(issueMock.getUpdated()).thenReturn(date);
        }
    }
}
