package com.atlassian.jira.jql.context;

import com.atlassian.jira.issue.search.ClauseNames;
import com.atlassian.jira.issue.search.constants.SystemSearchConstants;
import com.atlassian.jira.issue.search.managers.SearchHandlerManager;
import com.atlassian.jira.jql.ClauseHandler;
import com.atlassian.jira.jql.MockClauseHandler;
import com.atlassian.jira.mock.jql.MockClauseInformation;
import com.atlassian.jira.util.collect.CollectionBuilder;
import com.atlassian.query.clause.AndClause;
import com.atlassian.query.clause.Clause;
import com.atlassian.query.clause.NotClause;
import com.atlassian.query.clause.OrClause;
import com.atlassian.query.clause.TerminalClause;
import com.atlassian.query.clause.TerminalClauseImpl;
import com.atlassian.query.operator.Operator;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestQueryContextVisitor {
    @Test
    public void testRootClause() throws Exception {
        AndClause andClause = new AndClause(new TerminalClauseImpl("blah", Operator.GREATER_THAN, "blah"));
        NotClause notClause = new NotClause(new TerminalClauseImpl("blah", Operator.GREATER_THAN, "blah"));
        OrClause orClause = new OrClause(new TerminalClauseImpl("blah", Operator.GREATER_THAN, "blah"));
        TerminalClause termClause = new TerminalClauseImpl("blah", Operator.GREATER_THAN, "blah");

        assertRootCalled(andClause, createProject(1));
        assertRootCalled(orClause, createProject(34));
        assertRootCalled(notClause, createProject(333));
        assertRootCalled(termClause, createProject(12));
    }

    private void assertRootCalled(Clause clause, ClauseContext expectedCtx) {
        class RootContextVisitor extends QueryContextVisitor {
            private Clause calledWith;
            private final ClauseContext result;

            public RootContextVisitor(ClauseContext result) {
                super(null, null, null);
                this.result = result;
            }

            @Override
            public ContextResult createContext(final Clause clause) {
                calledWith = clause;
                return new ContextResult(result, result);
            }

            public Clause getCalledClause() {
                return calledWith;
            }
        }

        final RootContextVisitor rootVisitor = new RootContextVisitor(expectedCtx);

        final QueryContextVisitor.ContextResult contextResult = clause.accept(rootVisitor);
        assertEquals(clause, rootVisitor.getCalledClause());
        assertEquals(expectedCtx, contextResult.getFullContext());
        assertEquals(expectedCtx, contextResult.getSimpleContext());
    }

    @Test
    public void testOr() throws Exception {
        TerminalClause termClause1 = new TerminalClauseImpl("blah1", Operator.GREATER_THAN, "val1");
        TerminalClause termClause2 = new TerminalClauseImpl("blah2", Operator.GREATER_THAN, "val2");
        Clause andClause = new OrClause(termClause1, termClause2);

        final ClauseContext clauseContext1 = createProject(78);

        final ClauseContextFactory clauseContextFactory = mock(ClauseContextFactory.class);
        when(clauseContextFactory.getClauseContext(null, termClause1)).thenReturn(clauseContext1);
        when(clauseContextFactory.getClauseContext(null, termClause2)).thenReturn(clauseContext1);

        final ClauseHandler clauseHandler = new MockClauseHandler().setContextFactory(clauseContextFactory).setInformation(new MockClauseInformation(new ClauseNames("blah1")));
        final ClauseHandler clauseHandler2 = new MockClauseHandler().setContextFactory(clauseContextFactory).setInformation(new MockClauseInformation(new ClauseNames("blah2")));

        final SearchHandlerManager searchHandlerManager = mock(SearchHandlerManager.class);
        when(searchHandlerManager.getClauseHandler(null, "blah1")).thenReturn(Collections.singleton(clauseHandler));
        when(searchHandlerManager.getClauseHandler(null, "blah2")).thenReturn(Collections.singleton(clauseHandler2));

        final ContextSetUtil contextSetUtil = mock(ContextSetUtil.class);

        QueryContextVisitor visitor = new QueryContextVisitor(null, contextSetUtil, searchHandlerManager);
        final QueryContextVisitor.ContextResult result = andClause.accept(visitor);

        QueryContextVisitor.ContextResult expectedResult = new QueryContextVisitor.ContextResult(clauseContext1, ClauseContextImpl.createGlobalClauseContext());

        assertEquals(expectedResult, result);
    }

    @Test
    public void testOrUnion() throws Exception {
        TerminalClause termClause1 = new TerminalClauseImpl("blah1", Operator.GREATER_THAN, "val1");
        TerminalClause termClause2 = new TerminalClauseImpl("blah2", Operator.GREATER_THAN, "val2");
        TerminalClause termClause2negated = new TerminalClauseImpl("blah2", Operator.LESS_THAN_EQUALS, "val2");
        OrClause orClause = new OrClause(termClause1, new NotClause(termClause2));

        final ClauseContext clauseContext1 = createProject(18202);
        final ProjectIssueTypeContext projectIssueTypeContext = new ProjectIssueTypeContextImpl(new ProjectContextImpl(10L), new IssueTypeContextImpl("10"));
        final ClauseContext clauseContext2 = new ClauseContextImpl(Collections.singleton(projectIssueTypeContext));

        final ClauseContextFactory clauseContextFactory = mock(ClauseContextFactory.class);
        when(clauseContextFactory.getClauseContext(null, termClause1)).thenReturn(clauseContext1);
        when(clauseContextFactory.getClauseContext(null, termClause2negated)).thenReturn(clauseContext2);

        final ClauseHandler clauseHandler = new MockClauseHandler().setContextFactory(clauseContextFactory).setInformation(new MockClauseInformation(new ClauseNames("blah1")));
        final ClauseHandler clauseHandler2 = new MockClauseHandler().setContextFactory(clauseContextFactory).setInformation(new MockClauseInformation(new ClauseNames("blah2")));

        final SearchHandlerManager searchHandlerManager = mock(SearchHandlerManager.class);
        when(searchHandlerManager.getClauseHandler(null, "blah1")).thenReturn(Collections.singleton(clauseHandler));
        when(searchHandlerManager.getClauseHandler(null, "blah2")).thenReturn(Collections.singleton(clauseHandler2));

        final ContextSetUtil contextSetUtil = mock(ContextSetUtil.class);

        final ClauseContext clauseContext3 = createProject(15, 6, 0x3993);
        when(contextSetUtil.union(CollectionBuilder.newBuilder(clauseContext1, clauseContext2).asSet())).thenReturn(clauseContext3);

        QueryContextVisitor visitor = new QueryContextVisitor(null, contextSetUtil, searchHandlerManager);
        final QueryContextVisitor.ContextResult result = orClause.accept(visitor);
        QueryContextVisitor.ContextResult expectedResult = new QueryContextVisitor.ContextResult(clauseContext3, ClauseContextImpl.createGlobalClauseContext());

        assertEquals(expectedResult, result);
    }

    @Test
    public void testOrUnionSimpleAndComplexDifferent() throws Exception {
        TerminalClause termClause1 = new TerminalClauseImpl("blah1", Operator.GREATER_THAN, "val1");
        TerminalClause termClause2 = new TerminalClauseImpl("project", Operator.GREATER_THAN, "val2");
        TerminalClause termClause3 = new TerminalClauseImpl("type", Operator.GREATER_THAN, "val2");
        Clause andClause = new OrClause(termClause1, termClause2, termClause3);

        final ClauseContext clauseContext1 = createProject(78);
        final ProjectIssueTypeContext projectIssueTypeContext = new ProjectIssueTypeContextImpl(new ProjectContextImpl(10L), new IssueTypeContextImpl("10"));
        final ClauseContext clauseContext2 = new ClauseContextImpl(Collections.singleton(projectIssueTypeContext));
        final ClauseContext clauseContext3 = createProject(678, 292);

        final ClauseContextFactory clauseContextFactory = mock(ClauseContextFactory.class);
        when(clauseContextFactory.getClauseContext(null, termClause1)).thenReturn(clauseContext1);
        when(clauseContextFactory.getClauseContext(null, termClause2)).thenReturn(clauseContext2);
        when(clauseContextFactory.getClauseContext(null, termClause3)).thenReturn(clauseContext3);

        final ClauseHandler clauseHandler = new MockClauseHandler().setContextFactory(clauseContextFactory).setInformation(new MockClauseInformation(new ClauseNames("blah1")));
        final ClauseHandler clauseHandler2 = new MockClauseHandler().setContextFactory(clauseContextFactory).setInformation(new MockClauseInformation(SystemSearchConstants.forProject().getJqlClauseNames()));
        final ClauseHandler clauseHandler3 = new MockClauseHandler().setContextFactory(clauseContextFactory).setInformation(new MockClauseInformation(SystemSearchConstants.forIssueType().getJqlClauseNames()));

        final SearchHandlerManager searchHandlerManager = mock(SearchHandlerManager.class);
        when(searchHandlerManager.getClauseHandler(null, "blah1")).thenReturn(Collections.singleton(clauseHandler));
        when(searchHandlerManager.getClauseHandler(null, "project")).thenReturn(Collections.singleton(clauseHandler2));
        when(searchHandlerManager.getClauseHandler(null, "type")).thenReturn(Collections.singleton(clauseHandler3));

        final ContextSetUtil contextSetUtil = mock(ContextSetUtil.class);

        // the end result of visitation is at a minimum the Global context; never an empty context.
        final ClauseContext clauseContextComplex = createProject(2, 4, 6);
        final ClauseContext clauseContextSimple = createProject(2, 4, 6);
        when(contextSetUtil.union(CollectionBuilder.newBuilder(clauseContext1, clauseContext2, clauseContext3).asSet())).thenReturn(clauseContextComplex);
        when(contextSetUtil.union(CollectionBuilder.newBuilder(ClauseContextImpl.createGlobalClauseContext(), clauseContext2, clauseContext3).asSet())).thenReturn(clauseContextSimple);

        QueryContextVisitor visitor = new QueryContextVisitor(null, contextSetUtil, searchHandlerManager);
        final QueryContextVisitor.ContextResult result = andClause.accept(visitor);

        QueryContextVisitor.ContextResult expectedResult = new QueryContextVisitor.ContextResult(clauseContextComplex, clauseContextSimple);

        assertEquals(expectedResult, result);
    }

    @Test
    public void testOrUnionSimpleAndComplexSame() throws Exception {
        TerminalClause termClause2 = new TerminalClauseImpl("project", Operator.GREATER_THAN, "val2");
        TerminalClause termClause3 = new TerminalClauseImpl("type", Operator.GREATER_THAN, "val2");
        Clause andClause = new OrClause(termClause2, termClause3);

        final ProjectIssueTypeContext projectIssueTypeContext = new ProjectIssueTypeContextImpl(new ProjectContextImpl(10L), new IssueTypeContextImpl("10"));
        final ClauseContext clauseContext2 = new ClauseContextImpl(Collections.singleton(projectIssueTypeContext));
        final ClauseContext clauseContext3 = createProject(678, 292);

        final ClauseContextFactory clauseContextFactory = mock(ClauseContextFactory.class);
        when(clauseContextFactory.getClauseContext(null, termClause2)).thenReturn(clauseContext2);
        when(clauseContextFactory.getClauseContext(null, termClause3)).thenReturn(clauseContext3);

        final ClauseHandler clauseHandler2 = new MockClauseHandler().setContextFactory(clauseContextFactory).setInformation(new MockClauseInformation(SystemSearchConstants.forProject().getJqlClauseNames()));
        final ClauseHandler clauseHandler3 = new MockClauseHandler().setContextFactory(clauseContextFactory).setInformation(new MockClauseInformation(SystemSearchConstants.forIssueType().getJqlClauseNames()));

        final SearchHandlerManager searchHandlerManager = mock(SearchHandlerManager.class);
        when(searchHandlerManager.getClauseHandler(null, "project")).thenReturn(Collections.singleton(clauseHandler2));
        when(searchHandlerManager.getClauseHandler(null, "type")).thenReturn(Collections.singleton(clauseHandler3));

        final ContextSetUtil contextSetUtil = mock(ContextSetUtil.class);

        // the end result of visitation is at a minimum the Global context; never an empty context.
        final ClauseContext clauseContextComplex = createProject(2, 4, 6);
        when(contextSetUtil.union(CollectionBuilder.newBuilder(clauseContext2, clauseContext3).asSet())).thenReturn(clauseContextComplex);

        QueryContextVisitor visitor = new QueryContextVisitor(null, contextSetUtil, searchHandlerManager);
        final QueryContextVisitor.ContextResult result = andClause.accept(visitor);

        QueryContextVisitor.ContextResult expectedResult = new QueryContextVisitor.ContextResult(clauseContextComplex, clauseContextComplex);

        assertEquals(expectedResult, result);
    }


    @Test
    public void testAnd() throws Exception {
        TerminalClause termClause1 = new TerminalClauseImpl("blah1", Operator.GREATER_THAN, "val1");
        TerminalClause termClause2 = new TerminalClauseImpl("blah2", Operator.GREATER_THAN, "val2");
        Clause andClause = new AndClause(termClause1, termClause2);

        final ClauseContext clauseContext1 = createProject(78);

        final ClauseContextFactory clauseContextFactory = mock(ClauseContextFactory.class);
        when(clauseContextFactory.getClauseContext(null, termClause1)).thenReturn(clauseContext1);
        when(clauseContextFactory.getClauseContext(null, termClause2)).thenReturn(clauseContext1);

        final ClauseHandler clauseHandler = new MockClauseHandler().setContextFactory(clauseContextFactory).setInformation(new MockClauseInformation(new ClauseNames("blah1")));
        final ClauseHandler clauseHandler2 = new MockClauseHandler().setContextFactory(clauseContextFactory).setInformation(new MockClauseInformation(new ClauseNames("blah2")));

        final SearchHandlerManager searchHandlerManager = mock(SearchHandlerManager.class);
        when(searchHandlerManager.getClauseHandler(null, "blah1")).thenReturn(Collections.singleton(clauseHandler));
        when(searchHandlerManager.getClauseHandler(null, "blah2")).thenReturn(Collections.singleton(clauseHandler2));

        final ContextSetUtil contextSetUtil = mock(ContextSetUtil.class);

        QueryContextVisitor visitor = new QueryContextVisitor(null, contextSetUtil, searchHandlerManager);
        final QueryContextVisitor.ContextResult result = andClause.accept(visitor);

        QueryContextVisitor.ContextResult expectedResult = new QueryContextVisitor.ContextResult(clauseContext1, ClauseContextImpl.createGlobalClauseContext());

        assertEquals(expectedResult, result);
    }

    @Test
    public void testAndIntersect() throws Exception {
        //We are checking the negation here.
        TerminalClause termClause1 = new TerminalClauseImpl("blah1", Operator.GREATER_THAN, "val1");
        TerminalClause termClause1negated = new TerminalClauseImpl("blah1", Operator.LESS_THAN_EQUALS, "val1");
        TerminalClause termClause2 = new TerminalClauseImpl("blah2", Operator.GREATER_THAN, "val2");
        TerminalClause termClause2negated = new TerminalClauseImpl("blah2", Operator.LESS_THAN_EQUALS, "val2");
        Clause andClause = new NotClause(new OrClause(termClause1, termClause2));

        final ClauseContext clauseContext1 = createProject(78);
        final ProjectIssueTypeContext projectIssueTypeContext = new ProjectIssueTypeContextImpl(new ProjectContextImpl(10L), new IssueTypeContextImpl("10"));
        final ClauseContext clauseContext2 = new ClauseContextImpl(Collections.singleton(projectIssueTypeContext));

        final ClauseContextFactory clauseContextFactory = mock(ClauseContextFactory.class);
        when(clauseContextFactory.getClauseContext(null, termClause1negated)).thenReturn(clauseContext1);
        when(clauseContextFactory.getClauseContext(null, termClause2negated)).thenReturn(clauseContext2);

        final ClauseHandler clauseHandler = new MockClauseHandler().setContextFactory(clauseContextFactory).setInformation(new MockClauseInformation(new ClauseNames("blah1")));
        final ClauseHandler clauseHandler2 = new MockClauseHandler().setContextFactory(clauseContextFactory).setInformation(new MockClauseInformation(new ClauseNames("blah2")));

        final SearchHandlerManager searchHandlerManager = mock(SearchHandlerManager.class);
        when(searchHandlerManager.getClauseHandler(null, "blah1")).thenReturn(Collections.singleton(clauseHandler));
        when(searchHandlerManager.getClauseHandler(null, "blah2")).thenReturn(Collections.singleton(clauseHandler2));

        final ContextSetUtil contextSetUtil = mock(ContextSetUtil.class);

        // the end result of visitation is at a minimum the Global context; never an empty context.
        final ClauseContext clauseContextComplex = createProject(2, 4, 6);
        when(contextSetUtil.intersect(CollectionBuilder.newBuilder(clauseContext1, clauseContext2).asSet())).thenReturn(clauseContextComplex);

        QueryContextVisitor visitor = new QueryContextVisitor(null, contextSetUtil, searchHandlerManager);
        final QueryContextVisitor.ContextResult result = andClause.accept(visitor);

        QueryContextVisitor.ContextResult expectedResult = new QueryContextVisitor.ContextResult(clauseContextComplex, ClauseContextImpl.createGlobalClauseContext());

        assertEquals(expectedResult, result);
    }

    @Test
    public void testAndIntersectSimpleAndComplexDifferent() throws Exception {
        TerminalClause termClause1 = new TerminalClauseImpl("blah1", Operator.GREATER_THAN, "val1");
        TerminalClause termClause2 = new TerminalClauseImpl("project", Operator.GREATER_THAN, "val2");
        TerminalClause termClause3 = new TerminalClauseImpl("type", Operator.GREATER_THAN, "val2");
        Clause andClause = new AndClause(termClause1, termClause2, termClause3);

        final ClauseContext clauseContext1 = createProject(78);
        final ProjectIssueTypeContext projectIssueTypeContext = new ProjectIssueTypeContextImpl(new ProjectContextImpl(10L), new IssueTypeContextImpl("10"));
        final ClauseContext clauseContext2 = new ClauseContextImpl(Collections.singleton(projectIssueTypeContext));
        final ClauseContext clauseContext3 = createProject(678, 292);

        final ClauseContextFactory clauseContextFactory = mock(ClauseContextFactory.class);
        when(clauseContextFactory.getClauseContext(null, termClause1)).thenReturn(clauseContext1);
        when(clauseContextFactory.getClauseContext(null, termClause2)).thenReturn(clauseContext2);
        when(clauseContextFactory.getClauseContext(null, termClause3)).thenReturn(clauseContext3);

        final ClauseHandler clauseHandler = new MockClauseHandler().setContextFactory(clauseContextFactory).setInformation(new MockClauseInformation(new ClauseNames("blah1")));
        final ClauseHandler clauseHandler2 = new MockClauseHandler().setContextFactory(clauseContextFactory).setInformation(new MockClauseInformation(SystemSearchConstants.forProject().getJqlClauseNames()));
        final ClauseHandler clauseHandler3 = new MockClauseHandler().setContextFactory(clauseContextFactory).setInformation(new MockClauseInformation(SystemSearchConstants.forIssueType().getJqlClauseNames()));

        final SearchHandlerManager searchHandlerManager = mock(SearchHandlerManager.class);
        when(searchHandlerManager.getClauseHandler(null, "blah1")).thenReturn(Collections.singleton(clauseHandler));
        when(searchHandlerManager.getClauseHandler(null, "project")).thenReturn(Collections.singleton(clauseHandler2));
        when(searchHandlerManager.getClauseHandler(null, "type")).thenReturn(Collections.singleton(clauseHandler3));

        final ContextSetUtil contextSetUtil = mock(ContextSetUtil.class);

        // the end result of visitation is at a minimum the Global context; never an empty context.
        final ClauseContext clauseContextComplex = createProject(2, 4, 6);
        final ClauseContext clauseContextSimple = createProject(2, 4, 6);
        when(contextSetUtil.intersect(CollectionBuilder.newBuilder(clauseContext1, clauseContext2, clauseContext3).asSet())).thenReturn(clauseContextComplex);
        when(contextSetUtil.intersect(CollectionBuilder.newBuilder(ClauseContextImpl.createGlobalClauseContext(), clauseContext2, clauseContext3).asSet())).thenReturn(clauseContextSimple);

        QueryContextVisitor visitor = new QueryContextVisitor(null, contextSetUtil, searchHandlerManager);
        final QueryContextVisitor.ContextResult result = andClause.accept(visitor);

        QueryContextVisitor.ContextResult expectedResult = new QueryContextVisitor.ContextResult(clauseContextComplex, clauseContextSimple);

        assertEquals(expectedResult, result);
    }

    @Test
    public void testAndIntersectSimpleAndComplexSame() throws Exception {
        TerminalClause termClause1 = new TerminalClauseImpl("blah1", Operator.GREATER_THAN, "val1");
        TerminalClause termClause2 = new TerminalClauseImpl("project", Operator.GREATER_THAN, "val2");
        TerminalClause termClause3 = new TerminalClauseImpl("type", Operator.GREATER_THAN, "val2");
        Clause andClause = new AndClause(termClause1, termClause2, termClause3);

        final ClauseContext clauseContext1 = ClauseContextImpl.createGlobalClauseContext();
        final ProjectIssueTypeContext projectIssueTypeContext = new ProjectIssueTypeContextImpl(new ProjectContextImpl(10L), new IssueTypeContextImpl("10"));
        final ClauseContext clauseContext2 = new ClauseContextImpl(Collections.singleton(projectIssueTypeContext));
        final ClauseContext clauseContext3 = createProject(678, 292);

        final ClauseContextFactory clauseContextFactory = mock(ClauseContextFactory.class);
        when(clauseContextFactory.getClauseContext(null, termClause1)).thenReturn(clauseContext1);
        when(clauseContextFactory.getClauseContext(null, termClause2)).thenReturn(clauseContext2);
        when(clauseContextFactory.getClauseContext(null, termClause3)).thenReturn(clauseContext3);

        final ClauseHandler clauseHandler = new MockClauseHandler().setContextFactory(clauseContextFactory).setInformation(new MockClauseInformation(new ClauseNames("blah1")));
        final ClauseHandler clauseHandler2 = new MockClauseHandler().setContextFactory(clauseContextFactory).setInformation(new MockClauseInformation(SystemSearchConstants.forProject().getJqlClauseNames()));
        final ClauseHandler clauseHandler3 = new MockClauseHandler().setContextFactory(clauseContextFactory).setInformation(new MockClauseInformation(SystemSearchConstants.forIssueType().getJqlClauseNames()));

        final SearchHandlerManager searchHandlerManager = mock(SearchHandlerManager.class);
        when(searchHandlerManager.getClauseHandler(null, "blah1")).thenReturn(Collections.singleton(clauseHandler));
        when(searchHandlerManager.getClauseHandler(null, "project")).thenReturn(Collections.singleton(clauseHandler2));
        when(searchHandlerManager.getClauseHandler(null, "type")).thenReturn(Collections.singleton(clauseHandler3));

        final ContextSetUtil contextSetUtil = mock(ContextSetUtil.class);

        // the end result of visitation is at a minimum the Global context; never an empty context.
        final ClauseContext clauseContextComplex = createProject(2, 4, 6);
        when(contextSetUtil.intersect(CollectionBuilder.newBuilder(clauseContext1, clauseContext2, clauseContext3).asSet())).thenReturn(clauseContextComplex);

        QueryContextVisitor visitor = new QueryContextVisitor(null, contextSetUtil, searchHandlerManager);
        final QueryContextVisitor.ContextResult result = andClause.accept(visitor);

        QueryContextVisitor.ContextResult expectedResult = new QueryContextVisitor.ContextResult(clauseContextComplex, clauseContextComplex);

        assertEquals(expectedResult, result);
    }

    @Test
    public void testTerminalClause() throws Exception {
        TerminalClause termClause1 = new TerminalClauseImpl("blah1", Operator.GREATER_THAN, "val1");

        final ClauseContext clauseContext1 = createProject(394748494);

        final ClauseContextFactory clauseContextFactory = mock(ClauseContextFactory.class);
        when(clauseContextFactory.getClauseContext(null, termClause1)).thenReturn(clauseContext1);

        final ClauseHandler clauseHandler = new MockClauseHandler().setContextFactory(clauseContextFactory).setInformation(new MockClauseInformation(new ClauseNames("blah1")));

        final SearchHandlerManager searchHandlerManager = mock(SearchHandlerManager.class);
        when(searchHandlerManager.getClauseHandler(null, "blah1")).thenReturn(Collections.singleton(clauseHandler));

        final ContextSetUtil contextSetUtil = mock(ContextSetUtil.class);

        final QueryContextVisitor.QueryContextVisitorFactory visitorFactory = new QueryContextVisitor.QueryContextVisitorFactory(contextSetUtil, searchHandlerManager);
        QueryContextVisitor visitor = visitorFactory.createVisitor(null);
        final QueryContextVisitor.ContextResult result = termClause1.accept(visitor);

        final QueryContextVisitor.ContextResult expectedResult = new QueryContextVisitor.ContextResult(clauseContext1, ClauseContextImpl.createGlobalClauseContext());
        assertEquals(expectedResult, result);
    }

    @Test
    public void testTerminalClauseNoHandlers() throws Exception {
        TerminalClause termClause1 = new TerminalClauseImpl("blah1", Operator.GREATER_THAN, "val1");

        final SearchHandlerManager searchHandlerManager = mock(SearchHandlerManager.class);
        when(searchHandlerManager.getClauseHandler(null, "blah1")).thenReturn(Collections.<ClauseHandler>emptyList());

        final ContextSetUtil contextSetUtil = mock(ContextSetUtil.class);

        final QueryContextVisitor.QueryContextVisitorFactory visitorFactory = new QueryContextVisitor.QueryContextVisitorFactory(contextSetUtil, searchHandlerManager);
        QueryContextVisitor visitor = visitorFactory.createVisitor(null);
        final QueryContextVisitor.ContextResult result = termClause1.accept(visitor);

        final QueryContextVisitor.ContextResult expectedResult = new QueryContextVisitor.ContextResult(ClauseContextImpl.createGlobalClauseContext(), ClauseContextImpl.createGlobalClauseContext());
        assertEquals(expectedResult, result);
    }

    @Test
    public void testTerminalClauseMultipleHandlers() throws Exception {
        final TerminalClause termClause1 = new TerminalClauseImpl("gin", Operator.EQUALS, "1");

        final ClauseContext clauseContext1 = createProject(1);
        final ClauseContext clauseContext2 = createProject(2);
        final ClauseContext clauseContext3 = createProject(3);

        final ClauseContextFactory clauseContextFactory = mock(ClauseContextFactory.class);
        when(clauseContextFactory.getClauseContext(null, termClause1)).thenReturn(clauseContext1);

        final ClauseContextFactory clauseContextFactory2 = mock(ClauseContextFactory.class);
        when(clauseContextFactory2.getClauseContext(null, termClause1)).thenReturn(clauseContext2);

        final ClauseHandler handler1 = new MockClauseHandler().setContextFactory(clauseContextFactory).setInformation(new MockClauseInformation(new ClauseNames("gin")));
        final ClauseHandler handler2 = new MockClauseHandler().setContextFactory(clauseContextFactory2).setInformation(new MockClauseInformation(new ClauseNames("gin")));

        final SearchHandlerManager searchHandlerManager = mock(SearchHandlerManager.class);
        when(searchHandlerManager.getClauseHandler(null, "gin")).thenReturn(Arrays.asList(handler1, handler2));

        final ContextSetUtil contextSetUtil = mock(ContextSetUtil.class);
        when(contextSetUtil.union(CollectionBuilder.newBuilder(clauseContext1, clauseContext2).asSet())).thenReturn(clauseContext3);

        final QueryContextVisitor.QueryContextVisitorFactory visitorFactory = new QueryContextVisitor.QueryContextVisitorFactory(contextSetUtil, searchHandlerManager);
        QueryContextVisitor visitor = visitorFactory.createVisitor(null);
        final QueryContextVisitor.ContextResult result = termClause1.accept(visitor);

        final QueryContextVisitor.ContextResult expectedResult = new QueryContextVisitor.ContextResult(clauseContext3, ClauseContextImpl.createGlobalClauseContext());
        assertEquals(expectedResult, result);
    }

    @Test
    public void testTerminalClauseExplicit() throws Exception {
        final TerminalClause termClause1 = new TerminalClauseImpl("project", Operator.EQUALS, "1");

        final ClauseContext clauseContext1 = createProject(1);

        final ClauseContextFactory clauseContextFactory = mock(ClauseContextFactory.class);
        when(clauseContextFactory.getClauseContext(null, termClause1)).thenReturn(clauseContext1);

        final ClauseHandler handler1 = new MockClauseHandler().setContextFactory(clauseContextFactory).setInformation(new MockClauseInformation(SystemSearchConstants.forProject().getJqlClauseNames()));

        final SearchHandlerManager searchHandlerManager = mock(SearchHandlerManager.class);
        when(searchHandlerManager.getClauseHandler(null, "project")).thenReturn(Arrays.asList(handler1));

        final ContextSetUtil contextSetUtil = mock(ContextSetUtil.class);

        final QueryContextVisitor.QueryContextVisitorFactory visitorFactory = new QueryContextVisitor.QueryContextVisitorFactory(contextSetUtil, searchHandlerManager);
        QueryContextVisitor visitor = visitorFactory.createVisitor(null);
        final QueryContextVisitor.ContextResult result = termClause1.accept(visitor);

        final QueryContextVisitor.ContextResult expectedResult = new QueryContextVisitor.ContextResult(clauseContext1, clauseContext1);
        assertEquals(expectedResult, result);
    }

    @Test
    public void testTerminalClauseEmptyContext() throws Exception {
        final TerminalClause termClause1 = new TerminalClauseImpl("project", Operator.EQUALS, "1");

        final ClauseContextFactory clauseContextFactory = mock(ClauseContextFactory.class);
        when(clauseContextFactory.getClauseContext(null, termClause1)).thenReturn(new ClauseContextImpl());

        final ClauseHandler handler1 = new MockClauseHandler().setContextFactory(clauseContextFactory).setInformation(new MockClauseInformation(SystemSearchConstants.forProject().getJqlClauseNames()));

        final SearchHandlerManager searchHandlerManager = mock(SearchHandlerManager.class);
        when(searchHandlerManager.getClauseHandler(null, "project")).thenReturn(Arrays.asList(handler1));

        final ContextSetUtil contextSetUtil = mock(ContextSetUtil.class);

        final QueryContextVisitor.QueryContextVisitorFactory visitorFactory = new QueryContextVisitor.QueryContextVisitorFactory(contextSetUtil, searchHandlerManager);
        QueryContextVisitor visitor = visitorFactory.createVisitor(null);
        final QueryContextVisitor.ContextResult result = termClause1.accept(visitor);

        final QueryContextVisitor.ContextResult expectedResult = new QueryContextVisitor.ContextResult(ClauseContextImpl.createGlobalClauseContext(), ClauseContextImpl.createGlobalClauseContext());
        assertEquals(expectedResult, result);
    }

    private static ClauseContext createProject(int... ids) {
        Set<ProjectIssueTypeContext> ctxs = new HashSet<ProjectIssueTypeContext>();
        for (int id : ids) {
            ctxs.add(new ProjectIssueTypeContextImpl(new ProjectContextImpl((long) id), AllIssueTypesContext.getInstance()));
        }
        return new ClauseContextImpl(ctxs);
    }
}
