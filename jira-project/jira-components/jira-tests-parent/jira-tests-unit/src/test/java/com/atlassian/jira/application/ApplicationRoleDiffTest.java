package com.atlassian.jira.application;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.fugue.Option;
import com.atlassian.jira.auditing.ChangedValue;
import com.atlassian.jira.auditing.ChangedValueImpl;
import org.h2.util.StringUtils;
import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;

import static com.atlassian.application.api.ApplicationKey.valueOf;
import static com.atlassian.jira.application.ApplicationRoleDiff.compare;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.empty;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

/**
 * @since 7.0
 */
@RunWith(MockitoJUnitRunner.class)
public class ApplicationRoleDiffTest {
    @Test
    public void diffNoneToSomeShowsSensibleGroups() {
        Option<ApplicationRole> old = Option.none();
        ApplicationRole newRole = new MockApplicationRole(valueOf("moa")).groupNames("tigers", "leopards").defaultGroupNames("leopards");

        final ApplicationRoleDiff diff = ApplicationRoleDiff.diff(old, newRole);

        assertEquals("Changed access configuration of moa", diff.summary());
        final List<ChangedValue> messages = diff.messages();

        assertEquals(2, messages.size());

        assertThat(messages, containsInAnyOrder(
                new ChangedValueMatcher("leopards", "Not associated", "Associated (default)"),
                new ChangedValueMatcher("tigers", "Not associated", "Associated")));
    }

    @Test
    public void diffNoneToSomeShowsSelectedByDefaultChangeWhenDefault() {
        Option<ApplicationRole> old = Option.none();
        ApplicationRole newRole = new MockApplicationRole(valueOf("moa")).selectedByDefault(true);

        final ApplicationRoleDiff diff = ApplicationRoleDiff.diff(old, newRole);

        assertEquals("Changed access configuration of moa", diff.summary());
        final List<ChangedValue> messages = diff.messages();

        assertEquals(1, messages.size());

        assertThat(messages, contains(
                new ChangedValueMatcher("Default application for new users", "false", "true")));
    }

    @Test
    public void diffNoneToSomeDoesNotShowSelectedByDefaultChangeWhenNotDefault() {
        Option<ApplicationRole> old = Option.none();
        ApplicationRole newRole = new MockApplicationRole(valueOf("moa")).selectedByDefault(false);

        final ApplicationRoleDiff diff = ApplicationRoleDiff.diff(old, newRole);

        assertEquals("Changed access configuration of moa", diff.summary());
        final List<ChangedValue> messages = diff.messages();

        assertThat(messages, empty());
    }

    @Test(expected = IllegalArgumentException.class)
    public void applicationKeysMustMatchInDiff() {
        Option<ApplicationRole> old = Option.some(new MockApplicationRole(valueOf("nyala")));
        ApplicationRole newRole = new MockApplicationRole(valueOf("ostrich")).groupNames("tigers", "leopards").defaultGroupNames("leopards");

        ApplicationRoleDiff.diff(old, newRole);
    }

    @Test
    public void diffToAllNewShouldShowRegularAndDefaults() {
        final ApplicationKey applicationKey = valueOf("ostrich");
        Option<ApplicationRole> old = Option.some(new MockApplicationRole(applicationKey));
        ApplicationRole newRole = new MockApplicationRole(applicationKey).groupNames("tigers", "leopards").defaultGroupNames("leopards");

        final ApplicationRoleDiff diff = ApplicationRoleDiff.diff(old, newRole);

        assertEquals("Changed access configuration of ostrich", diff.summary());
        final List<ChangedValue> messages = diff.messages();

        assertEquals(2, messages.size());

        assertThat(messages, containsInAnyOrder(
                new ChangedValueMatcher("leopards", "Not associated", "Associated (default)"),
                new ChangedValueMatcher("tigers", "Not associated", "Associated")));
    }

    @Test
    public void diffAllToNoneShouldShowRemoval() {
        final ApplicationKey applicationKey = valueOf("emu");
        Option<ApplicationRole> old = Option.some(new MockApplicationRole(applicationKey).groupNames("tigers", "leopards").defaultGroupNames("leopards"));
        ApplicationRole newRole = new MockApplicationRole(applicationKey);

        final ApplicationRoleDiff diff = ApplicationRoleDiff.diff(old, newRole);

        assertEquals("Changed access configuration of emu", diff.summary());
        final List<ChangedValue> messages = diff.messages();

        assertEquals(2, messages.size());

        assertThat(messages, containsInAnyOrder(
                new ChangedValueMatcher("leopards", "Associated (default)", "Not associated"),
                new ChangedValueMatcher("tigers", "Associated", "Not associated")));
    }

    @Test
    public void diffAllToNoneShouldShowRemovalForSelectedDefaultWhenSelected() {
        final ApplicationKey applicationKey = valueOf("emu");
        Option<ApplicationRole> old = Option.some(new MockApplicationRole(applicationKey).selectedByDefault(true));
        ApplicationRole newRole = new MockApplicationRole(applicationKey);

        final ApplicationRoleDiff diff = ApplicationRoleDiff.diff(old, newRole);

        assertEquals("Changed access configuration of emu", diff.summary());
        final List<ChangedValue> messages = diff.messages();

        assertEquals(1, messages.size());

        assertThat(messages, contains(
                new ChangedValueMatcher("Default application for new users", "true", "false")));
    }

    @Test
    public void diffAllToNoneShouldNotShowRemovalForSelectedDefaultWhenNotSelected() {
        final ApplicationKey applicationKey = valueOf("emu");
        Option<ApplicationRole> old = Option.some(new MockApplicationRole(applicationKey).selectedByDefault(false));
        ApplicationRole newRole = new MockApplicationRole(applicationKey);

        final ApplicationRoleDiff diff = ApplicationRoleDiff.diff(old, newRole);

        assertEquals("Changed access configuration of emu", diff.summary());
        final List<ChangedValue> messages = diff.messages();

        assertThat(messages, empty());
    }

    @Test
    public void diffSomeToDefaultShouldReturnOneMessageForDefaultChange() {
        final ApplicationKey applicationKey = valueOf("emu");
        Option<ApplicationRole> old = Option.some(new MockApplicationRole(applicationKey).groupNames("leopards"));
        ApplicationRole newRole = new MockApplicationRole(applicationKey).groupNames("leopards").defaultGroupNames("leopards");

        final ApplicationRoleDiff diff = ApplicationRoleDiff.diff(old, newRole);

        assertEquals("Changed access configuration of emu", diff.summary());
        final List<ChangedValue> messages = diff.messages();

        assertEquals(1, messages.size());

        assertThat(messages, contains(new ChangedValueMatcher("leopards", "Associated", "Associated (default)")));
    }

    @Test
    public void diffDefaultUnselectShouldReturnOneMessageForDefaultChange() {
        final ApplicationKey applicationKey = valueOf("emu");
        Option<ApplicationRole> old = Option.some(new MockApplicationRole(applicationKey).groupNames("leopards").defaultGroupNames("leopards"));
        ApplicationRole newRole = new MockApplicationRole(applicationKey).groupNames("leopards");

        final ApplicationRoleDiff diff = ApplicationRoleDiff.diff(old, newRole);

        assertEquals("Changed access configuration of emu", diff.summary());
        final List<ChangedValue> messages = diff.messages();

        assertEquals(1, messages.size());

        assertThat(messages, contains(new ChangedValueMatcher("leopards", "Associated (default)", "Associated")));
    }

    @Test
    public void diffRemoveShouldIncludeDefaultAndRegularGroups() {
        final ApplicationKey applicationKey = valueOf("cassowary");
        ApplicationRole newRole = new MockApplicationRole(applicationKey).groupNames("leopards", "tigers").defaultGroupNames("leopards");
        final ApplicationRoleDiff applicationRoleDiff = ApplicationRoleDiff.diffRemoved(newRole);


        assertEquals("Changed access configuration of cassowary", applicationRoleDiff.summary());
        final List<ChangedValue> messages = applicationRoleDiff.messages();

        assertEquals(2, messages.size());

        assertThat(messages, containsInAnyOrder(
                new ChangedValueMatcher("leopards", "Associated (default)", "Not associated"),
                new ChangedValueMatcher("tigers", "Associated", "Not associated")));
    }

    @Test
    public void diffRemoveShouldPrintRemovingSelectedByDefaultIfItWasSetToTrue() {
        final ApplicationKey applicationKey = valueOf("cassowary");
        ApplicationRole newRole = new MockApplicationRole(applicationKey).selectedByDefault(true);
        final ApplicationRoleDiff applicationRoleDiff = ApplicationRoleDiff.diffRemoved(newRole);

        assertEquals("Changed access configuration of cassowary", applicationRoleDiff.summary());
        final List<ChangedValue> messages = applicationRoleDiff.messages();

        assertEquals(1, messages.size());
        assertThat(messages, contains(
                new ChangedValueMatcher("Default application for new users", "true", "false")
        ));
    }

    @Test
    public void compareShouldTreatNullsAsEqual() {
        final int compare = compare(null, null);
        assertEquals(0, compare);
    }

    @Test
    public void leftNullShouldBeLessThanRightValue() {
        final int compare = compare(null, "");
        assertEquals(-1, compare);
    }

    @Test
    public void rightNullShouldBeLessThanLeftValue() {
        final int compare = compare("", null);
        assertEquals(1, compare);
    }

    @Test
    public void equalStringsShouldBeEqual() {
        @SuppressWarnings("StringBufferReplaceableByString")
        final StringBuilder gi = new StringBuilder("gi"); // don't inline both strings to same constant
        final int compare = compare("giant", gi.append("ant").toString());
        assertEquals(0, compare);
    }

    private static final class ChangedValueMatcher extends BaseMatcher<ChangedValue> {
        private final ChangedValue value;

        public ChangedValueMatcher(String name, String from, String to) {
            this.value = new ChangedValueImpl(name, from, to);
        }

        @Override
        public boolean matches(final Object item) {
            if ((item instanceof ChangedValue)) {
                ChangedValue matchedItem = (ChangedValue) item;

                return StringUtils.equals(matchedItem.getName(), value.getName())
                        && StringUtils.equals(matchedItem.getFrom(), value.getFrom())
                        && StringUtils.equals(matchedItem.getTo(), value.getTo());
            } else {
                return false;
            }
        }

        @Override
        public void describeTo(final Description description) {
            description.appendText(String.format(" ChangedValue: {%s, %s, %s,} ", value.getName(), value.getFrom(), value.getTo()));
        }
    }
}
