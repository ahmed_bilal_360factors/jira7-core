package com.atlassian.jira.upgrade.tasks;

import com.atlassian.jira.mock.ofbiz.MockGenericValue;
import com.atlassian.jira.mock.ofbiz.MockOfBizDelegator;
import com.atlassian.jira.portal.CachingPortletConfigurationStore;
import com.atlassian.jira.util.collect.MapBuilder;
import org.junit.Before;
import org.junit.Test;
import org.ofbiz.core.entity.GenericValue;

import static com.google.common.collect.Lists.newArrayList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class TestUpgradeTask_Build70013 {
    private MockOfBizDelegator ofBizDelegator;
    private CachingPortletConfigurationStore cachingPortletConfigurationStore;
    private GenericValue gadget1, gadget2, adminDashboardItem, adminGadget;

    private UpgradeTask_Build70013 upgradeTask;

    @Before
    public void setUp() throws Exception {
        cachingPortletConfigurationStore = mock(CachingPortletConfigurationStore.class);

        gadget1 = new MockGenericValue("PortletConfiguration", MapBuilder.<String, Object>newBuilder()
                .add("id", 10000L)
                .add("portalpage", 10010L)
                .add("portletId", null)
                .add("columnNumber", 0)
                .add("position", 0)
                .add("gadgetXml", "local/filter-results.xml")
                .add("dashboardModuleCompleteKey", "")
                .add("color", "color0")
                .toMap());

        gadget2 = new MockGenericValue("PortletConfiguration", MapBuilder.<String, Object>newBuilder()
                .add("id", 10011L)
                .add("portalpage", 10010L)
                .add("portletId", null)
                .add("columnNumber", 0)
                .add("position", 2)
                .add("gadgetXml", "local/piechart.xml")
                .add("dashboardModuleCompleteKey", "")
                .add("color", "color0")
                .toMap());

        adminDashboardItem = new MockGenericValue("PortletConfiguration", MapBuilder.<String, Object>newBuilder()
                .add("id", 10001L)
                .add("portalpage", 10000L)
                .add("portletId", null)
                .add("columnNumber", 0)
                .add("position", 1)
                .add("dashboardModuleCompleteKey", "com.atlassian.jira.gadgets:admin-dashboard-item")
                .add("color", "color0")
                .toMap());

        adminGadget = new MockGenericValue("PortletConfiguration", MapBuilder.<String, Object>newBuilder()
                .add("id", 10002L)
                .add("portalpage", 10000L)
                .add("portletId", null)
                .add("columnNumber", 0)
                .add("position", 1)
                .add("gadgetXml", "rest/gadgets/1.0/g/com.atlassian.jira.gadgets:admin-gadget/gadgets/admin-gadget.xml")
                .add("color", "color0")
                .toMap());

        ofBizDelegator = new MockOfBizDelegator();

        upgradeTask = new UpgradeTask_Build70013(ofBizDelegator, cachingPortletConfigurationStore);
    }

    @Test
    public void deletesAdminDashboardItem() throws Exception {
        ofBizDelegator.createValue(gadget1);
        ofBizDelegator.createValue(gadget2);
        ofBizDelegator.createValue(adminDashboardItem);

        upgradeTask.doUpgrade(false);

        ofBizDelegator.verify(newArrayList(gadget1, gadget2));
        verify(cachingPortletConfigurationStore).flush();
    }

    @Test
    public void deletesAdminGadget() throws Exception {
        ofBizDelegator.createValue(gadget1);
        ofBizDelegator.createValue(gadget2);
        ofBizDelegator.createValue(adminGadget);

        upgradeTask.doUpgrade(false);

        ofBizDelegator.verify(newArrayList(gadget1, gadget2));
        verify(cachingPortletConfigurationStore).flush();
    }

    @Test
    public void deletesAdminGadgetAndDashboardItemInSetupMode() throws Exception {
        ofBizDelegator.createValue(gadget1);
        ofBizDelegator.createValue(gadget2);
        ofBizDelegator.createValue(adminGadget);
        ofBizDelegator.createValue(adminDashboardItem);

        upgradeTask.doUpgrade(true);

        ofBizDelegator.verify(newArrayList(gadget1, gadget2));
        verify(cachingPortletConfigurationStore).flush();
    }

    @Test
    public void downgradeTaskIsNotRequired() throws Exception {
        assertThat(upgradeTask.isDowngradeTaskRequired(), is(false));
    }
}