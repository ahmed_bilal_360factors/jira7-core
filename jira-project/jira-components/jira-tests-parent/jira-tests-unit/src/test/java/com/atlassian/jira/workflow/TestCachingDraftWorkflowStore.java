package com.atlassian.jira.workflow;

import com.atlassian.cache.CacheManager;
import com.atlassian.cache.memory.MemoryCacheManager;
import com.atlassian.jira.exception.DataAccessException;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.google.common.collect.ImmutableList;
import com.opensymphony.workflow.FactoryException;
import com.opensymphony.workflow.loader.DescriptorFactory;
import com.opensymphony.workflow.loader.WorkflowDescriptor;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

/**
 *
 */
public class TestCachingDraftWorkflowStore {
    private static final String WORKFLOW_XML = "<workflow>testXML</workflow>";

    @Rule
    public final MethodRule initMockito = MockitoJUnit.rule();
    @Rule
    public final ExpectedException exception = ExpectedException.none();

    private ApplicationUser testUser;
    private CacheManager cacheManager;

    @Mock
    private JiraWorkflow mockJiraWorkflow;
    @Mock
    private DraftWorkflowStore draftWorkflowStore;

    @Before
    public void init() {
        testUser = new MockApplicationUser("username");
        cacheManager = new MemoryCacheManager();
    }

    @Test
    public void testGetDraftWorkflow() {
        final WorkflowDescriptor descriptor = new DescriptorFactory().createWorkflowDescriptor();
        when(mockJiraWorkflow.getDescriptor()).thenReturn(descriptor);
        when(draftWorkflowStore.getDraftWorkflow("parentWorkflow")).thenReturn(mockJiraWorkflow);

        final CachingDraftWorkflowStore cachingDraftWorkflowStore = new CachingDraftWorkflowStore(draftWorkflowStore, cacheManager) {

            @Override
            String convertDescriptorToXML(final WorkflowDescriptor descriptor) {
                return WORKFLOW_XML;
            }

            @Override
            WorkflowDescriptor convertXMLtoWorkflowDescriptor(final String parentWorkflowXML) throws FactoryException {
                if (!parentWorkflowXML.equals(WORKFLOW_XML)) {
                    throw new RuntimeException("XML not equal to original XML: '" + parentWorkflowXML + "' vs '" + WORKFLOW_XML + "'");
                }
                return descriptor;
            }

            @Override
            WorkflowManager getWorkflowManager() {
                return null;
            }
        };
        final JiraWorkflow jiraWorkflow = cachingDraftWorkflowStore.getDraftWorkflow("parentWorkflow");
        assertNotNull(jiraWorkflow);
        assertEquals(descriptor, jiraWorkflow.getDescriptor());
    }

    @Test
    public void testGetDraftWorkflowTwice() {
        final WorkflowDescriptor descriptor = new DescriptorFactory().createWorkflowDescriptor();
        when(mockJiraWorkflow.getDescriptor()).thenReturn(descriptor);
        when(draftWorkflowStore.getDraftWorkflow("parentWorkflow")).thenReturn(mockJiraWorkflow);

        final CachingDraftWorkflowStore cachingDraftWorkflowStore = new CachingDraftWorkflowStore(draftWorkflowStore, cacheManager) {

            @Override
            String convertDescriptorToXML(final WorkflowDescriptor descriptor) {
                return WORKFLOW_XML;
            }

            @Override
            WorkflowDescriptor convertXMLtoWorkflowDescriptor(final String parentWorkflowXML) throws FactoryException {
                if (!parentWorkflowXML.equals(WORKFLOW_XML)) {
                    throw new RuntimeException("XML not equal to original XML: '" + parentWorkflowXML + "' vs '" + WORKFLOW_XML + "'");
                }
                return descriptor;
            }

            @Override
            WorkflowManager getWorkflowManager() {
                return null;
            }
        };
        JiraWorkflow jiraWorkflow = cachingDraftWorkflowStore.getDraftWorkflow("parentWorkflow");
        assertNotNull(jiraWorkflow);
        assertEquals(descriptor, jiraWorkflow.getDescriptor());
        verify(draftWorkflowStore).getDraftWorkflow("parentWorkflow");

        jiraWorkflow = cachingDraftWorkflowStore.getDraftWorkflow("parentWorkflow");
        assertNotNull(jiraWorkflow);
        assertEquals(descriptor, jiraWorkflow.getDescriptor());
        verifyNoMoreInteractions(draftWorkflowStore);
    }

    @Test
    public void testGetDraftWorkflowNullParentWorkflowName() {
        final CachingDraftWorkflowStore cachingDraftWorkflowStore = new CachingDraftWorkflowStore(null, cacheManager);
        exception.expect(IllegalArgumentException.class);
        cachingDraftWorkflowStore.getDraftWorkflow(null);
    }

    @Test
    public void testGetDraftWorkflowWithNoDraftWorkflowAvailable() {
        final CachingDraftWorkflowStore cachingDraftWorkflowStore = new CachingDraftWorkflowStore(draftWorkflowStore, cacheManager);

        final JiraWorkflow jiraWorkflow = cachingDraftWorkflowStore.getDraftWorkflow("parentWorkflow");
        assertNull(jiraWorkflow);
        verify(draftWorkflowStore).getDraftWorkflow("parentWorkflow");
    }

    @Test
    public void testCreateDraftWorkflow() {
        when(mockJiraWorkflow.getName()).thenReturn("parentWorkflow");
        when(draftWorkflowStore.createDraftWorkflow(testUser, mockJiraWorkflow)).thenReturn(mockJiraWorkflow);

        final CachingDraftWorkflowStore cachingDraftWorkflowStore = new CachingDraftWorkflowStore(draftWorkflowStore, cacheManager) {
            @Override
            String convertDescriptorToXML(final WorkflowDescriptor descriptor) {
                return WORKFLOW_XML;
            }
        };

        final JiraWorkflow jiraWorkflow = cachingDraftWorkflowStore.createDraftWorkflow(testUser, mockJiraWorkflow);
        assertNotNull(jiraWorkflow);
        assertEquals(jiraWorkflow, mockJiraWorkflow);
    }

    //test ensures that the get will retrieve the workflow from the cache rather than from the delegate after a create.
    @Test
    public void testCreateAndGetDraftWorkflow() {
        final WorkflowDescriptor descriptor = new DescriptorFactory().createWorkflowDescriptor();
        when(mockJiraWorkflow.getDescriptor()).thenReturn(descriptor);
        when(mockJiraWorkflow.getName()).thenReturn("parentWorkflow");

        when(draftWorkflowStore.createDraftWorkflow(testUser, mockJiraWorkflow)).thenReturn(mockJiraWorkflow);
        when(draftWorkflowStore.getDraftWorkflow("parentWorkflow")).thenReturn(mockJiraWorkflow);

        final CachingDraftWorkflowStore cachingDraftWorkflowStore = new CachingDraftWorkflowStore(draftWorkflowStore, cacheManager) {
            @Override
            String convertDescriptorToXML(final WorkflowDescriptor descriptor) {
                return WORKFLOW_XML;
            }

            @Override
            WorkflowDescriptor convertXMLtoWorkflowDescriptor(final String parentWorkflowXML) throws FactoryException {
                if (!WORKFLOW_XML.equals(parentWorkflowXML)) {
                    throw new RuntimeException("Cached workflow XML does not equal original XML!");
                }
                return descriptor;
            }

            @Override
            WorkflowManager getWorkflowManager() {
                return null;
            }
        };

        cachingDraftWorkflowStore.createDraftWorkflow(testUser, mockJiraWorkflow);
        cachingDraftWorkflowStore.getDraftWorkflow("parentWorkflow");
    }

    @Test
    public void testDeleteDraftWorkflow() {
        when(draftWorkflowStore.deleteDraftWorkflow("parentWorkflow")).thenReturn(true);

        final CachingDraftWorkflowStore cachingDraftWorkflowStore = new CachingDraftWorkflowStore(draftWorkflowStore, cacheManager);

        final boolean deleted = cachingDraftWorkflowStore.deleteDraftWorkflow("parentWorkflow");
        assertTrue(deleted);
    }

    @Test
    public void testCreateAndDeleteDraftWorkflow() {
        final WorkflowDescriptor descriptor = new DescriptorFactory().createWorkflowDescriptor();
        when(mockJiraWorkflow.getDescriptor()).thenReturn(descriptor);
        when(mockJiraWorkflow.getName()).thenReturn("parentWorkflow");

        when(draftWorkflowStore.createDraftWorkflow(testUser, mockJiraWorkflow)).thenReturn(mockJiraWorkflow);
        when(draftWorkflowStore.getDraftWorkflow("parentWorkflow")).thenReturn(mockJiraWorkflow);
        when(draftWorkflowStore.deleteDraftWorkflow("parentWorkflow")).thenAnswer(invocation -> {
            when(draftWorkflowStore.getDraftWorkflow("parentWorkflow")).thenReturn(null);
            return true;
        });

        final CachingDraftWorkflowStore cachingDraftWorkflowStore = new CachingDraftWorkflowStore(draftWorkflowStore, cacheManager) {
            @Override
            String convertDescriptorToXML(final WorkflowDescriptor descriptor) {
                return WORKFLOW_XML;
            }

            @Override
            WorkflowDescriptor convertXMLtoWorkflowDescriptor(final String parentWorkflowXML) throws FactoryException {
                if (!WORKFLOW_XML.equals(parentWorkflowXML)) {
                    throw new RuntimeException("Error, cached value '" + parentWorkflowXML + "' does not equal original '" + WORKFLOW_XML + "'.");
                }
                return descriptor;
            }

            @Override
            WorkflowManager getWorkflowManager() {
                return null;
            }
        };

        cachingDraftWorkflowStore.createDraftWorkflow(testUser, mockJiraWorkflow);

        //check that something is in the cache
        JiraWorkflow jiraWorkflow = cachingDraftWorkflowStore.getDraftWorkflow("parentWorkflow");
        assertNotNull(jiraWorkflow);
        verify(draftWorkflowStore).getDraftWorkflow("parentWorkflow");

        //now delete the draft workflow. This should also delete the cache.
        final boolean deleted = cachingDraftWorkflowStore.deleteDraftWorkflow("parentWorkflow");
        assertTrue(deleted);
        verify(draftWorkflowStore).deleteDraftWorkflow("parentWorkflow");

        //this should call through to the delegate and eventually return null.
        jiraWorkflow = cachingDraftWorkflowStore.getDraftWorkflow("parentWorkflow");
        assertNull(jiraWorkflow);
        verify(draftWorkflowStore, times(2)).getDraftWorkflow("parentWorkflow");
    }

    @Test
    public void testDeleteWorkflowIsAtomic() throws ExecutionException, InterruptedException {
        final WorkflowDescriptor descriptor = new DescriptorFactory().createWorkflowDescriptor();
        when(mockJiraWorkflow.getName()).thenReturn("parentWorkflow");

        final MockDraftWorkflowStore mockDraftWorkflowStore = new MockDraftWorkflowStore();
        mockDraftWorkflowStore.createdWorkflow = mockJiraWorkflow;

        final CountDownLatch deleteLatch = new CountDownLatch(1);
        mockDraftWorkflowStore.deleteLatch = deleteLatch;

        final CachingDraftWorkflowStore cachingDraftWorkflowStore = new CachingDraftWorkflowStore(mockDraftWorkflowStore, cacheManager) {
            @Override
            String convertDescriptorToXML(final WorkflowDescriptor descriptor) {
                return WORKFLOW_XML;
            }

            @Override
            WorkflowDescriptor convertXMLtoWorkflowDescriptor(final String parentWorkflowXML) throws FactoryException {
                if (!WORKFLOW_XML.equals(parentWorkflowXML)) {
                    throw new RuntimeException("Error, cached value '" + parentWorkflowXML + "' does not equal original '" + WORKFLOW_XML + "'.");
                }
                return descriptor;
            }

            @Override
            WorkflowManager getWorkflowManager() {
                return null;
            }
        };

        cachingDraftWorkflowStore.createDraftWorkflow(testUser, mockJiraWorkflow);

        final List<Callable<Object>> tasks = ImmutableList.of(
                () -> {
                    cachingDraftWorkflowStore.deleteDraftWorkflow("parentWorkflow");
                    return null;
                },
                () -> {
                    deleteLatch.await();
                    final JiraWorkflow deletedWorkflow = cachingDraftWorkflowStore.getDraftWorkflow("parentWorkflow");
                    //ensure the get can never see the worfklow that's just been deleted
                    assertNull(deletedWorkflow);
                    return null;
                }
        );
        runMultiThreadedTest(tasks, 2);
    }

    @Test
    public void testUpdateWorkflow() {
        final JiraDraftWorkflow mockJiraDraftWorkflow = mock(JiraDraftWorkflow.class);
        when(draftWorkflowStore.updateDraftWorkflow(testUser, "parentWorkflow", mockJiraDraftWorkflow)).thenReturn(mockJiraDraftWorkflow);

        final CachingDraftWorkflowStore cachingDraftWorkflowStore = new CachingDraftWorkflowStore(draftWorkflowStore, cacheManager) {

            @Override
            String convertDescriptorToXML(final WorkflowDescriptor descriptor) {
                return WORKFLOW_XML;
            }
        };

        final JiraWorkflow updatedWorkflow = cachingDraftWorkflowStore.updateDraftWorkflow(testUser, "parentWorkflow", mockJiraDraftWorkflow);
        assertNotNull(updatedWorkflow);
        assertEquals(mockJiraDraftWorkflow, updatedWorkflow);
    }

    private void runMultiThreadedTest(final List<Callable<Object>> tasks, final int threads) throws InterruptedException, ExecutionException {
        final ExecutorService pool = Executors.newFixedThreadPool(threads);

        List<Future<Object>> /*<Future>*/futures;
        try {
            futures = pool.invokeAll(tasks);
        } catch (final InterruptedException e) {
            throw new RuntimeException(e);
        }

        //wait until all tasks have finished executing.
        for (final Future<Object> future : futures) {
            future.get();
        }
    }

    private class MockDraftWorkflowStore implements DraftWorkflowStore {
        public JiraWorkflow createdWorkflow;
        public CountDownLatch deleteLatch;

        public JiraWorkflow getDraftWorkflow(final String parentWorkflowName) throws DataAccessException {
            return createdWorkflow;
        }

        public JiraWorkflow createDraftWorkflow(final ApplicationUser author, final JiraWorkflow parentWorkflow) throws DataAccessException, IllegalStateException, IllegalArgumentException {
            return createdWorkflow;
        }

        public boolean deleteDraftWorkflow(final String parentWorkflowName) throws DataAccessException, IllegalArgumentException {
            try {
                createdWorkflow = null;
                deleteLatch.countDown();
                Thread.sleep(50);
                return true;
            } catch (final InterruptedException e) {
                throw new RuntimeException(e);
            }
        }

        public JiraWorkflow updateDraftWorkflow(final ApplicationUser user, final String parentWorkflowName, final JiraWorkflow workflow) throws DataAccessException {
            return null;
        }

        public JiraWorkflow updateDraftWorkflowWithoutAudit(final String parentWorkflowName, final JiraWorkflow workflow)
                throws DataAccessException {
            return null;
        }
    }

}
