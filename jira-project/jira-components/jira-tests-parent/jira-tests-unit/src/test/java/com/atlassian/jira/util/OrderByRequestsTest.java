package com.atlassian.jira.util;

import com.google.common.base.Objects;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static com.atlassian.jira.util.Entity.entity;
import static com.atlassian.jira.util.OrderByRequest.asc;
import static com.atlassian.jira.util.OrderByRequest.desc;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

/**
 * @since v6.5
 */
public class OrderByRequestsTest {
    @Test
    public void sortMethodSortsAccordingToNaturalOrdering() {
        assertThat(sort(entities(1, 2, 3), asc(Fields.id)), equalTo(entities(1, 2, 3)));
        assertThat(sort(entities(1, 2, 3), desc(Fields.id)), equalTo(entities(3, 2, 1)));
        assertThat(sort(entities(4, 5, 2, 3, 1), asc(Fields.id)), equalTo(entities(1, 2, 3, 4, 5)));
    }

    @Test
    public void sortMethodPlacesNullValuesLast() {
        assertThat(sort(entities(3, 2, 1, null, null), asc(Fields.id)), equalTo(entities(1, 2, 3, null, null)));
    }

    @Test
    public void sortMethodIsStable() {
        List<Entity> entities = Lists.newArrayList(
                entity(5L, "end"),
                entity(1L, "1"),
                entity(1L, "2"),
                entity(1L, "3"),
                entity(1L, "4"),
                entity(1L, "5"));

        List<Entity> sorted = sort(entities, asc(Fields.id));

        assertThat(sorted.subList(0, sorted.size() - 1), equalTo(entities.subList(1, sorted.size())));
    }

    private <T, F extends OrderByRequests.ExtractableField<T>> List<T> sort(final List<T> entities, final OrderByRequest<F> orderByRequest) {
        return OrderByRequests.toOrdering(orderByRequest).sortedCopy(entities);
    }

    private List<Entity> entities(Integer... ids) {
        return ImmutableList.copyOf(Iterables.transform(Arrays.asList(ids), new com.google.common.base.Function<Integer, Entity>() {
            @Override
            public Entity apply(final Integer input) {
                return Entity.entity(input != null ? input.longValue() : null);
            }
        }));
    }
}

final class Entity {
    public final Long id;
    public final String name;
    public final String description;

    private Entity(Long id, String name, String description) {
        this.id = id;
        this.name = name;
        this.description = description;
    }

    public static Entity entity(Long id) {
        return new Entity(id, "name of " + id, "description of " + id);
    }

    public static Entity entity(Long id, String name) {
        return new Entity(id, name, "description of " + id.toString());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Entity that = (Entity) o;

        return Objects.equal(this.id, that.id) &&
                Objects.equal(this.name, that.name) &&
                Objects.equal(this.description, that.description);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id, name, description);
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("id", id)
                .add("name", name)
                .add("description", description)
                .toString();
    }
}

enum Fields implements OrderByRequests.ExtractableField<Entity> {
    id {
        @Override
        public Comparable getValue(final Entity entity) {
            return entity.id;
        }
    },
    name {
        @Override
        public Comparable getValue(final Entity entity) {
            return entity.name;
        }
    },
    description {
        @Override
        public Comparable getValue(final Entity entity) {
            return entity.description;
        }
    }
}
