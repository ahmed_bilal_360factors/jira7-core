package com.atlassian.jira.permission.management;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.fugue.Either;
import com.atlassian.fugue.Option;
import com.atlassian.jira.bc.ServiceOutcome;
import com.atlassian.jira.mock.i18n.MockI18nHelper;
import com.atlassian.jira.permission.JiraPermissionHolderType;
import com.atlassian.jira.permission.MockProjectPermission;
import com.atlassian.jira.permission.PermissionGrant;
import com.atlassian.jira.permission.PermissionHolder;
import com.atlassian.jira.permission.PermissionScheme;
import com.atlassian.jira.permission.PermissionSchemeManager;
import com.atlassian.jira.permission.PermissionSchemeService;
import com.atlassian.jira.permission.PermissionTypeManager;
import com.atlassian.jira.permission.ProjectPermission;
import com.atlassian.jira.permission.data.PermissionGrantImpl;
import com.atlassian.jira.permission.management.beans.GrantToPermissionInputBean;
import com.atlassian.jira.permission.management.beans.PermissionsInputBean;
import com.atlassian.jira.permission.management.beans.ProjectPermissionAddBean;
import com.atlassian.jira.permission.management.beans.ProjectPermissionOperationResultBean;
import com.atlassian.jira.permission.management.beans.ProjectPermissionSchemeBean;
import com.atlassian.jira.permission.management.beans.SecurityTypeBean;
import com.atlassian.jira.permission.management.beans.SecurityTypeValueBean;
import com.atlassian.jira.permission.management.events.PermissionGrantedToSecurityTypeEvent;
import com.atlassian.jira.permission.management.events.PermissionRevokedFromSecurityTypeEvent;
import com.atlassian.jira.permission.management.events.SuccessfulPermissionSchemeGrantEvent;
import com.atlassian.jira.permission.management.events.SuccessfulPermissionSchemeRevokeEvent;
import com.atlassian.jira.scheme.Scheme;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.plugin.ProjectPermissionKey;
import com.atlassian.jira.security.type.SecurityType;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.ofbiz.core.entity.GenericValue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import static com.atlassian.jira.matchers.ErrorCollectionMatcher.hasErrorMessage;
import static com.atlassian.jira.matchers.ErrorCollectionMatcher.hasOnlyErrorMessage;
import static com.atlassian.jira.permission.ProjectPermissionCategory.PROJECTS;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyList;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public final class ManagedPermissionSchemeHelperImplTest {

    @Mock
    private PermissionManager permissionManager;

    @Mock
    private UserManager userManager;

    @Mock
    private PermissionSchemeManager permissionSchemeManager;

    @Mock
    private PermissionSchemeService permissionSchemeService;

    @Mock
    private PermissionTypeManager permissionTypeManager;

    @Mock
    private ApplicationUser user;

    @Mock
    private ServiceOutcome<PermissionScheme> serviceOutcome;

    @Mock
    private Scheme scheme;

    @Mock
    private ProjectPermission projectPermission;

    @Mock
    private SecurityTypeValuesService securityTypeValuesService;

    @Mock
    private ManagedPermissionSchemeEditingService managedPermissionSchemeEditingService;

    @Mock
    private EventPublisher eventPublisher;

    @Mock
    private SecurityType securityType;

    @Mock
    private ErrorCollection errorCollection;

    @Mock
    private PermissionsInputBean permissionsInputBean;

    @Mock
    private PermissionScheme permissionScheme;

    @Mock
    private GrantToPermissionInputBean grantToPermissionInputBean;

    @Mock
    private GenericValue securityTypeValueEntity;

    @Mock
    private SecurityType mockSecurityType;

    private Long SCHEME_ID = 0L;

    private String PERMISSION_KEY = "TEST";

    private ProjectPermissionKey projectPermissionKey = new ProjectPermissionKey(PERMISSION_KEY);

    private I18nHelper i18n = new MockI18nHelper();

    ManagedPermissionSchemeHelper managedPermissionSchemeHelper;

    @Before
    public void setUp() {
        when(securityTypeValueEntity.get(eq("permissionKey"))).thenReturn("permission-key-value");
        when(securityTypeValueEntity.get(eq("type"))).thenReturn("type-value");

        managedPermissionSchemeHelper = new ManagedPermissionSchemeHelperImpl(
                permissionSchemeManager,
                permissionSchemeService,
                permissionManager,
                permissionTypeManager,
                i18n,
                securityTypeValuesService,
                managedPermissionSchemeEditingService,
                eventPublisher,
                userManager);
    }

    private void returnServiceOutcome(Boolean isValid) {
        when(permissionSchemeService.getPermissionScheme(eq(user), eq(SCHEME_ID))).thenReturn(serviceOutcome);
        when(serviceOutcome.isValid()).thenReturn(isValid);
        when(serviceOutcome.get()).thenReturn(permissionScheme);
    }

    private void ableToFindPermissionScheme() {
        returnServiceOutcome(true);
        when(permissionSchemeManager.getSchemeObject(eq(SCHEME_ID))).thenReturn(scheme);
    }

    private void unableToFindPermissionScheme() {
        returnServiceOutcome(false);
        when(serviceOutcome.getErrorCollection()).thenReturn(errorCollection);
    }

    private void ableToFindProjectPermission() {
        Option<ProjectPermission> option = Option.some(projectPermission);
        when(permissionManager.getProjectPermission(any())).thenReturn(option);

        when(projectPermission.getProjectPermissionKey()).thenReturn(projectPermissionKey);
        when(projectPermission.getDescriptionI18nKey()).thenReturn("KEY");
        when(projectPermission.getNameI18nKey()).thenReturn("NAME");
    }

    private void unableToFindProjectPermission() {
        Option<ProjectPermission> option = Option.none();
        when(permissionManager.getProjectPermission(any())).thenReturn(option);
    }

    private List<SecurityTypeBean> setUpSecurityTypeBeanList() {
        SecurityTypeBean securityTypeBean = SecurityTypeBean.builder()
                .setSecurityType("security-type")
                .build();
        List<SecurityTypeBean> securityTypeBeanList = ImmutableList.of(securityTypeBean);
        when(securityTypeValuesService.buildPrimarySecurityTypes(any())).thenReturn(securityTypeBeanList);
        when(securityTypeValuesService.buildSecondarySecurityTypes(any())).thenReturn(securityTypeBeanList);
        return securityTypeBeanList;
    }

    @Test
    public void getAddViewNoPermissionSchemeReturnsError() {
        unableToFindPermissionScheme();
        Either<ErrorCollection, ProjectPermissionAddBean> returnValue = managedPermissionSchemeHelper.getManagedPermissionSchemeAddView(user, SCHEME_ID, PERMISSION_KEY);

        assertTrue(returnValue.isLeft());

        assertEquals("Error collection should be from the service outcome", returnValue.left().get(), errorCollection);
    }

    @Test
    public void getAddViewNoProjectPermissionReturnsError() {
        ableToFindPermissionScheme();
        unableToFindProjectPermission();

        Either<ErrorCollection, ProjectPermissionAddBean> returnValue = managedPermissionSchemeHelper.getManagedPermissionSchemeAddView(user, SCHEME_ID, PERMISSION_KEY);

        assertThat(returnValue.left().get(), hasOnlyErrorMessage("admin.permissions.errors.mustselectscheme", ErrorCollection.Reason.NOT_FOUND));
    }

    @Test
    public void getAddViewSecurityTypesReturnsSecurityTypesWithoutKey() {
        ableToFindPermissionScheme();
        ableToFindProjectPermission();
        List<SecurityTypeBean> securityTypeBeanList = setUpSecurityTypeBeanList();

        Either<ErrorCollection, ProjectPermissionAddBean> returnValue = managedPermissionSchemeHelper.getManagedPermissionSchemeAddViewSecurityTypes(user);

        assertTrue("Should be a successful result", returnValue.isRight());
        assertThat("Result should not contain a permission key", returnValue.right().get().getPermission(), is(nullValue()));
        assertThat("Result should contain primary security types", returnValue.right().get().getPrimarySecurityType(), equalTo(securityTypeBeanList));
        assertThat("Result should contain secondary security types", returnValue.right().get().getSecondarySecurityType(), equalTo(securityTypeBeanList));
    }

    @Test
    public void getAddViewShouldReturnSecurityTypesWithKey() {
        ableToFindPermissionScheme();
        ableToFindProjectPermission();
        List<SecurityTypeBean> securityTypeBeanList = setUpSecurityTypeBeanList();

        Either<ErrorCollection, ProjectPermissionAddBean> returnValue = managedPermissionSchemeHelper.getManagedPermissionSchemeAddView(user, SCHEME_ID, PERMISSION_KEY);

        assertTrue("Should be a successful result", returnValue.isRight());
        assertThat("Result should contain a permission key", returnValue.right().get().getPermission().getPermissionKey(), equalTo(PERMISSION_KEY));
        assertThat("Result should contain primary security types", returnValue.right().get().getPrimarySecurityType(), equalTo(securityTypeBeanList));
        assertThat("Result should contain secondary security types", returnValue.right().get().getSecondarySecurityType(), equalTo(securityTypeBeanList));
    }

    @Test
    public void grantAttemptThatDoesNotValidateShouldReturnErrorCollection() {
        when(permissionSchemeManager.getSchemeObject(SCHEME_ID)).thenReturn(null);

        final Either<ErrorCollection, ProjectPermissionSchemeBean> result = managedPermissionSchemeHelper.addManagedPermissionSchemeGrants(user, SCHEME_ID, permissionsInputBean);

        assertThat("should be an error result set", result.isLeft(), is(true));
        final ErrorCollection errorCollection = result.left().get();
        assertThat(errorCollection, hasErrorMessage("admin.permissions.errors.mustselectscheme"));
    }

    @Test
    public void schemeIdThatHasNoSchemeAssociatedShouldReturnErrorMessagesAndNoOperationResult() {
        final String SOME_ERROR_MESSAGE = "something.about.invalid.combination";

        when(permissionSchemeManager.getSchemeObject(SCHEME_ID)).thenReturn(scheme);
        SimpleErrorCollection simpleErrorCollection = new SimpleErrorCollection();
        simpleErrorCollection.addErrorMessage(SOME_ERROR_MESSAGE);
        when(managedPermissionSchemeEditingService.validateAddPermissions(user, permissionsInputBean)).thenReturn(simpleErrorCollection);

        final Either<ErrorCollection, ProjectPermissionSchemeBean> result = managedPermissionSchemeHelper.addManagedPermissionSchemeGrants(user, SCHEME_ID, permissionsInputBean);

        assertThat("should be an error result set", result.isLeft(), is(true));
        final ErrorCollection errorCollection = result.left().get();
        assertThat(errorCollection, hasErrorMessage(SOME_ERROR_MESSAGE));

        // no analytics should have been fired
        verify(eventPublisher, never()).publish(any(SuccessfulPermissionSchemeRevokeEvent.class));
    }

    @Test
    public void successfulAddGrantShouldHaveOperationResultSet() {
        ableToFindPermissionScheme();
        ableToFindProjectPermission();
        when(permissionSchemeManager.getSchemeObject(SCHEME_ID)).thenReturn(scheme);
        when(managedPermissionSchemeEditingService.validateAddPermissions(user, permissionsInputBean)).thenReturn(errorCollection);
        when(permissionsInputBean.getPermissionKeys()).thenReturn(Arrays.asList("single-permission"));

        Either<ErrorCollection, ProjectPermissionSchemeBean> result = managedPermissionSchemeHelper.addManagedPermissionSchemeGrants(user, SCHEME_ID, permissionsInputBean);

        assertThat("should be a valid result set", result.isRight(), is(true));
        ProjectPermissionOperationResultBean operationResult = result.right().get().getOperationResult();
        assertThat("operation result type should be success", operationResult.getType(), is(ProjectPermissionOperationResultBean.SUCCESS_TYPE));
        assertThat("operation result should have exactly one success message", operationResult.getMessages().size(), is(1));
        assertThat(operationResult.getMessages(), contains("admin.permissions.feedback.successfulgrant.single"));

        // now with multiple items to be granted
        when(permissionsInputBean.getPermissionKeys()).thenReturn(Arrays.asList("multiple", "items", "here"));

        result = managedPermissionSchemeHelper.addManagedPermissionSchemeGrants(user, SCHEME_ID, permissionsInputBean);

        assertThat("should be a valid result set", result.isRight(), is(true));
        operationResult = result.right().get().getOperationResult();
        assertThat("operation result type should be success", operationResult.getType(), is(ProjectPermissionOperationResultBean.SUCCESS_TYPE));
        assertThat("operation result should have exactly one success message", operationResult.getMessages().size(), is(1));
        assertThat(operationResult.getMessages(), contains("admin.permissions.feedback.successfulgrant.multiple"));
    }

    @Test
    public void successfulAddGrantShouldTriggerAnalyticsEvent() {
        ableToFindPermissionScheme();
        ableToFindProjectPermission();
        when(permissionSchemeManager.getSchemeObject(SCHEME_ID)).thenReturn(scheme);
        when(managedPermissionSchemeEditingService.validateAddPermissions(user, permissionsInputBean)).thenReturn(errorCollection);
        when(permissionsInputBean.getPermissionKeys()).thenReturn(Arrays.asList("single-permission"));
        when(grantToPermissionInputBean.getSecurityType()).thenReturn("security-type-key");
        when(permissionsInputBean.getGrants()).thenReturn(Arrays.asList(grantToPermissionInputBean));

        managedPermissionSchemeHelper.addManagedPermissionSchemeGrants(user, SCHEME_ID, permissionsInputBean);

        verify(eventPublisher, times(1)).publish(eq(new SuccessfulPermissionSchemeGrantEvent(SCHEME_ID, "security-type-key", 1)));
        verify(eventPublisher, times(1)).publish(eq(new PermissionGrantedToSecurityTypeEvent(SCHEME_ID, "single-permission", "security-type-key")));
    }

    @Test
    public void removeRequiresGrants() {
        Either<ErrorCollection, ProjectPermissionSchemeBean> result;
        when(permissionSchemeManager.getSchemeObject(SCHEME_ID)).thenReturn(scheme);

        result = managedPermissionSchemeHelper.removeManagedPermissionSchemeGrants(user, SCHEME_ID, null);
        assertThat(result.left().get(), hasErrorMessage("admin.errors.permissions.specify.permission.to.delete"));

        result = managedPermissionSchemeHelper.removeManagedPermissionSchemeGrants(user, SCHEME_ID, new ArrayList<>());
        assertThat(result.left().get(), hasErrorMessage("admin.errors.permissions.specify.permission.to.delete"));

        // no analytics should have been fired
        verify(eventPublisher, never()).publish(any(SuccessfulPermissionSchemeRevokeEvent.class));
    }

    @Test
    public void successfulRemoveShouldHaveOperationResultSet() {
        ableToFindPermissionScheme();
        ableToFindProjectPermission();
        when(permissionSchemeManager.getSchemeObject(SCHEME_ID)).thenReturn(scheme);

        Either<ErrorCollection, ProjectPermissionSchemeBean> result = managedPermissionSchemeHelper.removeManagedPermissionSchemeGrants(user, SCHEME_ID, Arrays.asList(12345L));

        assertThat("should be a valid result set", result.isRight(), is(true));
        ProjectPermissionOperationResultBean operationResult = result.right().get().getOperationResult();
        assertThat("operation result type should be success", operationResult.getType(), is(ProjectPermissionOperationResultBean.SUCCESS_TYPE));
        assertThat("operation result should have exactly one success message", operationResult.getMessages().size(), is(1));
        assertThat(operationResult.getMessages(), contains("admin.permissions.feedback.successfuldelete.single"));

        // now with multiple items to be deleted
        result = managedPermissionSchemeHelper.removeManagedPermissionSchemeGrants(user, SCHEME_ID, Arrays.asList(12345L, 67890L));

        assertThat("should be a valid result set", result.isRight(), is(true));
        operationResult = result.right().get().getOperationResult();
        assertThat("operation result type should be success", operationResult.getType(), is(ProjectPermissionOperationResultBean.SUCCESS_TYPE));
        assertThat("operation result should have exactly one success message", operationResult.getMessages().size(), is(1));
        assertThat(operationResult.getMessages(), contains("admin.permissions.feedback.successfuldelete.multiple"));
    }

    @Test
    public void successfulRemoveShouldTriggerAnalyticsEvent() {
        ableToFindPermissionScheme();
        ableToFindProjectPermission();
        when(permissionSchemeManager.getSchemeObject(SCHEME_ID)).thenReturn(scheme);
        when(permissionSchemeManager.getEntitiesByIds(anyList())).thenReturn(Arrays.asList(securityTypeValueEntity));

        managedPermissionSchemeHelper.removeManagedPermissionSchemeGrants(user, SCHEME_ID, Arrays.asList(12345L));

        verify(eventPublisher, times(1)).publish(eq(new SuccessfulPermissionSchemeRevokeEvent(SCHEME_ID, "permission-key-value", 1)));
        verify(eventPublisher, times(1)).publish(eq(new PermissionRevokedFromSecurityTypeEvent(SCHEME_ID, "permission-key-value", "type-value")));
    }

    @Test
    public void validPropertyIsNotSetForNonSingleUserPermission() {
        ableToFindPermissionScheme();
        String parameter = "groupname";
        setupPermissionsWithGrant(JiraPermissionHolderType.GROUP, parameter);

        Either<ErrorCollection, ProjectPermissionSchemeBean> returnValue = managedPermissionSchemeHelper.getManagedPermissionScheme(user, SCHEME_ID);

        assertTrue("The bean should have been generated", returnValue.isRight());
        SecurityTypeValueBean securityTypeValueBean = returnValue.right().get().getPermissions().get(0).getGrants().get(0).getValues().get(0);
        assertThat("Should correspond to the grant created", securityTypeValueBean.getValue(), equalTo(parameter));
        assertThat("The valid property is null for a non-single user permission", securityTypeValueBean.getValid(), equalTo(null));
    }

    @Test
    public void usersThatExistSetValidPropertyToTrueForSingleUserPermission() {
        ableToFindPermissionScheme();
        String parameter = "username";
        setupPermissionsWithGrant(JiraPermissionHolderType.USER, parameter);
        setupUser(parameter, true);

        Either<ErrorCollection, ProjectPermissionSchemeBean> returnValue = managedPermissionSchemeHelper.getManagedPermissionScheme(user, SCHEME_ID);

        assertTrue("The bean should have been generated", returnValue.isRight());
        SecurityTypeValueBean securityTypeValueBean = returnValue.right().get().getPermissions().get(0).getGrants().get(0).getValues().get(0);
        assertThat("Should correspond to the grant created", securityTypeValueBean.getValue(), equalTo(parameter));
        assertThat("The valid property is true for a single user permission with a user that exists",
                securityTypeValueBean.getValid(), equalTo(true));
    }

    @Test
    public void deletedUsersSetValidPropertyToFalseForSingleUserPermission() {
        ableToFindPermissionScheme();
        String parameter = "username";
        setupPermissionsWithGrant(JiraPermissionHolderType.USER, parameter);
        setupUser(parameter, false);

        Either<ErrorCollection, ProjectPermissionSchemeBean> returnValue = managedPermissionSchemeHelper.getManagedPermissionScheme(user, SCHEME_ID);

        assertTrue("The bean should have been generated", returnValue.isRight());
        SecurityTypeValueBean securityTypeValueBean = returnValue.right().get().getPermissions().get(0).getGrants().get(0).getValues().get(0);
        assertThat("Should correspond to the grant created", securityTypeValueBean.getValue(), equalTo(parameter));
        assertThat("The valid property is false for a single user permission with a user that does not exists",
                securityTypeValueBean.getValid(), equalTo(false));
    }

    private void setupPermissionsWithGrant(JiraPermissionHolderType type, String parameter) {
        ProjectPermission projectPermission = new MockProjectPermission("projectPermissionKey", "projectNameKey", "descriptionKey", PROJECTS);
        when(permissionManager.getAllProjectPermissions()).thenReturn(ImmutableList.of(projectPermission));

        PermissionHolder permHolder = PermissionHolder.holder(type, parameter);
        PermissionGrant permissionGrant = new PermissionGrantImpl(0L, permHolder, projectPermission.getProjectPermissionKey());

        Collection<PermissionGrant> permissionGrants = ImmutableList.of(permissionGrant);
        when(permissionScheme.getPermissions()).thenReturn(permissionGrants);

        Map<String, SecurityType> types = ImmutableMap.of(permHolder.getType().getKey(), mockSecurityType);
        when(permissionTypeManager.getTypes()).thenReturn(types);
    }

    private void setupUser(String username, boolean exists) {
        ApplicationUser mockUser = new MockApplicationUser(username);
        when(userManager.getUserByNameEvenWhenUnknown(username)).thenReturn(mockUser);
        when(userManager.isUserExisting(mockUser)).thenReturn(exists);
    }
}