package com.atlassian.jira.bulkedit.operation;

import com.atlassian.jira.bc.subtask.conversion.IssueToSubTaskConversionService;
import com.atlassian.jira.config.SubTaskManager;
import com.atlassian.jira.config.util.AttachmentPathManager;
import com.atlassian.jira.event.issue.IssueEventBundle;
import com.atlassian.jira.event.issue.IssueEventBundleFactory;
import com.atlassian.jira.event.issue.IssueEventManager;
import com.atlassian.jira.event.issue.txnaware.TxnAwareEventFactory;
import com.atlassian.jira.event.type.EventType;
import com.atlassian.jira.exception.CreateException;
import com.atlassian.jira.issue.AttachmentManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueFactory;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.history.ChangeItemBean;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.issue.issuetype.MockIssueType;
import com.atlassian.jira.issue.link.IssueLink;
import com.atlassian.jira.issue.link.IssueLinkManager;
import com.atlassian.jira.issue.link.IssueLinkType;
import com.atlassian.jira.issue.link.MockIssueLinkType;
import com.atlassian.jira.issue.util.IssueChangeHolder;
import com.atlassian.jira.issue.util.IssueUpdateBean;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.mock.issue.MockIssue;
import com.atlassian.jira.mock.ofbiz.MockGenericValue;
import com.atlassian.jira.mock.ofbiz.MockOfBizDelegator;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.task.context.Context;
import com.atlassian.jira.transaction.Transaction;
import com.atlassian.jira.transaction.TransactionSupport;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.web.bean.BulkEditBean;
import com.atlassian.jira.workflow.JiraWorkflow;
import com.atlassian.jira.workflow.WorkflowManager;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.mockito.Mock;
import org.ofbiz.core.entity.GenericValue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


public class TestBulkMoveOperation {
    @Rule
    public RuleChain mockitoMocksInContainer = MockitoMocksInContainer.forTest(this);

    // construct operation object:
    @Mock
    WorkflowManager workflowManager;
    @Mock
    ProjectManager projectManager;
    @Mock
    FieldManager fieldManager;
    @Mock
    IssueFactory issueFactory;
    @Mock
    IssueManager issueManager;
    @Mock
    AttachmentPathManager attachmentPathManager;
    @Mock
    IssueEventManager issueEventManager;
    @Mock
    AttachmentManager attachmentManager;
    @Mock
    IssueEventBundleFactory issueEventBundleFactory;
    @Mock
    IssueLinkManager issueLinkManager;
    @Mock
    SubTaskManager subTaskManager;

    @Mock
    BulkEditBean bulkEditBean;
    @Mock
    ApplicationUser applicationUser;
    @Mock
    TxnAwareEventFactory txnAwareEventFactory;

    @Mock
    @AvailableInContainer
    PermissionManager permissionManager;

    @Mock
    @AvailableInContainer
    TransactionSupport transactionSupport;

    @AvailableInContainer
    OfBizDelegator delegator = new MockOfBizDelegator();

    @Mock
    Context context;

    @Mock
    IssueToSubTaskConversionService issueToSubTaskConversionService;

    BulkMoveOperationImpl bulkMoveOperation = null;
    BulkMigrateOperation bulkMigrateOperation = null;

    Project project;
    Project projectNew;
    MutableIssue issueSingle;
    MutableIssue issueParent;
    MutableIssue issueParentNew;
    MutableIssue issueParentNewMoved;
    MutableIssue issueParentNewChangedIssueType;
    MutableIssue issueParentNewChangedWorkflow;
    MutableIssue issueParentNewChangedSecurityLevel;
    MutableIssue subtask;
    MutableIssue subtaskOther;
    MutableIssue subtaskWithoutParent;

    IssueType issueTypeIssue;
    IssueType issueTypeSubtask;
    IssueType issueTypeSubtaskOther;
    IssueType issueTypeNew;

    @Mock
    IssueLink parentToSubtaskLink;

    @Mock
    JiraWorkflow workflow;
    @Mock
    Context.Task task;
    @Mock
    Transaction transaction;

    @Before
    public void setUp() {
        bulkMoveOperation = new BulkMoveOperationImpl(workflowManager, projectManager, fieldManager, issueFactory, issueManager, issueEventManager, null, attachmentManager, issueEventBundleFactory, issueLinkManager, subTaskManager, issueToSubTaskConversionService, txnAwareEventFactory);
        bulkMigrateOperation = new BulkMigrateOperation(bulkMoveOperation, subTaskManager, issueManager);

        project = new MockProject(1l, "PRO", "Project");
        projectNew = new MockProject(2l, "PRO2", "Project2");

        issueTypeIssue = new MockIssueType("1", "Issue", false);
        issueTypeSubtask = new MockIssueType("2", "Sub-task", true);
        issueTypeSubtaskOther = new MockIssueType("3", "Sub-task-other", true);
        issueTypeNew = new MockIssueType("3", "New", false);

        issueParent = new MockIssue(2, "parent-1");
        issueParent.setIssueType(issueTypeIssue);
        issueParent.setIssueTypeId(issueTypeIssue.getId());
        issueParent.setProjectObject(project);
        issueParent.setProjectId(project.getId());
        issueParent.setWorkflowId(1l);
        issueParent.setSecurityLevelId(1l);
        issueParent.setParentObject(null);

        issueParentNew = new MockIssue(102, "parentNew-102");
        issueParentNew.setIssueType(issueTypeIssue);
        issueParentNew.setIssueTypeId(issueTypeIssue.getId());
        issueParentNew.setProjectObject(project);
        issueParentNew.setProjectId(project.getId());
        issueParentNew.setWorkflowId(1l);
        issueParentNew.setSecurityLevelId(1l);
        issueParentNew.setParentObject(null);

        issueParentNewMoved = new MockIssue(issueParentNew.getId().intValue(), issueParentNew.getKey());
        issueParentNewMoved.setIssueType(issueParentNew.getIssueType());
        issueParentNewMoved.setIssueTypeId(issueParentNew.getIssueTypeId());
        issueParentNewMoved.setProjectObject(projectNew);
        issueParentNewMoved.setProjectId(projectNew.getId());
        issueParentNewMoved.setWorkflowId(issueParentNew.getWorkflowId());
        issueParentNewMoved.setSecurityLevelId(issueParentNew.getSecurityLevelId());
        issueParentNewMoved.setParentObject(issueParentNew.getParentObject());

        issueParentNewChangedIssueType = new MockIssue(issueParentNew.getId().intValue(), issueParentNew.getKey());
        issueParentNewChangedIssueType.setIssueType(issueTypeNew);
        issueParentNewChangedIssueType.setIssueTypeId(issueTypeNew.getId());
        issueParentNewChangedIssueType.setProjectObject(issueParentNew.getProjectObject());
        issueParentNewChangedIssueType.setProjectId(issueParentNew.getId());
        issueParentNewChangedIssueType.setWorkflowId(issueParentNew.getWorkflowId());
        issueParentNewChangedIssueType.setSecurityLevelId(issueParentNew.getSecurityLevelId());
        issueParentNewChangedIssueType.setParentObject(issueParentNew.getParentObject());

        issueParentNewChangedWorkflow = new MockIssue(issueParentNew.getId().intValue(), issueParentNew.getKey());
        issueParentNewChangedWorkflow.setIssueType(issueParentNew.getIssueType());
        issueParentNewChangedWorkflow.setIssueTypeId(issueParentNew.getIssueTypeId());
        issueParentNewChangedWorkflow.setProjectObject(issueParentNew.getProjectObject());
        issueParentNewChangedWorkflow.setProjectId(issueParentNew.getId());
        issueParentNewChangedWorkflow.setWorkflowId(issueParentNew.getWorkflowId() + 1);
        issueParentNewChangedWorkflow.setSecurityLevelId(issueParentNew.getSecurityLevelId());
        issueParentNewChangedWorkflow.setParentObject(issueParentNew.getParentObject());

        issueParentNewChangedSecurityLevel = new MockIssue(issueParentNew.getId().intValue(), issueParentNew.getKey());
        issueParentNewChangedSecurityLevel.setIssueType(issueParentNew.getIssueType());
        issueParentNewChangedSecurityLevel.setIssueTypeId(issueParentNew.getIssueTypeId());
        issueParentNewChangedSecurityLevel.setProjectObject(issueParentNew.getProjectObject());
        issueParentNewChangedSecurityLevel.setProjectId(issueParentNew.getId());
        issueParentNewChangedSecurityLevel.setWorkflowId(issueParentNew.getWorkflowId());
        issueParentNewChangedSecurityLevel.setSecurityLevelId(issueParentNew.getSecurityLevelId() + 1);
        issueParentNewChangedSecurityLevel.setParentObject(issueParentNew.getParentObject());

        issueSingle = new MockIssue(1, "issue-1");
        issueSingle.setIssueType(issueTypeIssue);
        issueSingle.setIssueTypeId(issueTypeIssue.getId());
        issueSingle.setProjectObject(project);
        issueSingle.setProjectId(project.getId());
        issueSingle.setWorkflowId(1l);
        issueSingle.setSecurityLevelId(1l);
        issueSingle.setParentObject(null);

        subtask = new MockIssue(1, "subtask-1");
        subtask.setIssueType(issueTypeSubtask);
        subtask.setIssueTypeId(issueTypeSubtask.getId());
        subtask.setParentId(issueParent.getId());
        subtask.setParentObject(issueParent);
        subtask.setProjectObject(project);
        subtask.setProjectId(project.getId());
        subtask.setWorkflowId(1l);
        subtask.setSecurityLevelId(1l);

        subtaskOther = new MockIssue(1, "subtask-1");
        subtaskOther.setIssueType(issueTypeSubtaskOther);
        subtaskOther.setIssueTypeId(issueTypeSubtask.getId());
        subtaskOther.setParentId(issueParent.getId());
        subtaskOther.setParentObject(issueParent);
        subtaskOther.setProjectObject(project);
        subtaskOther.setProjectId(project.getId());
        subtaskOther.setWorkflowId(1l);
        subtaskOther.setSecurityLevelId(1l);

        subtaskWithoutParent = new MockIssue(1, "subtask-4");
        subtaskWithoutParent.setIssueType(issueTypeSubtask);
        subtaskWithoutParent.setIssueTypeId(issueTypeSubtask.getId());
        subtaskWithoutParent.setProjectObject(project);
        subtaskWithoutParent.setProjectId(project.getId());
        subtaskWithoutParent.setWorkflowId(1l);
        subtaskWithoutParent.setSecurityLevelId(1l);
        subtaskWithoutParent.setParentObject(null);
        for (MutableIssue issue : new MutableIssue[]{issueSingle, subtask, subtaskWithoutParent, issueParent, issueParentNew}) {
            when(workflowManager.getWorkflow(issue)).thenReturn(workflow);
            when(context.start(issue)).thenReturn(task);
        }
        for (MutableIssue issue : new MutableIssue[]{issueParent, issueParentNew}) {
            when(issueManager.getIssueObject(issue.getId())).thenReturn(issue);
        }

        when(bulkEditBean.getTargetWorkflow()).thenReturn(workflow);

        IssueLinkType subtaskIssueLinkType = new MockIssueLinkType(1, "Subtask", "subtask", "subtasked by", "jira_subtask");
        when(parentToSubtaskLink.getIssueLinkType()).thenReturn(subtaskIssueLinkType);
        when(parentToSubtaskLink.getSourceId()).thenReturn(issueParent.getId());
        when(parentToSubtaskLink.getDestinationId()).thenReturn(subtask.getId());
        when(issueLinkManager.getOutwardLinks(issueParent.getId())).thenReturn(ImmutableList.of(parentToSubtaskLink));

        when(transactionSupport.begin()).thenReturn(transaction);

        when(bulkEditBean.getTargetPid()).thenReturn(project.getId());
        when(bulkEditBean.getTargetProject()).thenReturn(project);
    }

    @Test
    public void moveCanBePerformedWhenPermitted() {
        // given:
        final List<Issue> issues = ImmutableList.of(
                new MockIssueBuilder().permission(true).build(),
                new MockIssueBuilder().permission(true).build(),
                new MockIssueBuilder().permission(true)
                        .subTask(new MockIssueBuilder().permission(true).build())
                        .subTask(new MockIssueBuilder().permission(true).build())
                        .build());
        when(bulkEditBean.getSelectedIssues()).thenReturn(issues);

        // then:
        assertThat("All issues are movable.", bulkMoveOperation.canPerform(bulkEditBean, applicationUser), Matchers.is(true));
    }

    @Test
    public void testMoveCannotBePerformedWhenNotPermitted() {
        // given:
        final List<Issue> issues = ImmutableList.of(
                new MockIssueBuilder().permission(true).build(),
                new MockIssueBuilder().permission(false).build());
        when(bulkEditBean.getSelectedIssues()).thenReturn(issues);

        // then:
        assertThat("No permission for one of the issues should reject move.",
                bulkMoveOperation.canPerform(bulkEditBean, applicationUser), Matchers.is(false));
    }

    @Test
    public void testMoveCannotBePerformedWhenNotPermittedForSubTasks() {
        // given:
        final Issue issue = new MockIssueBuilder().permission(true)
                .subTask(new MockIssueBuilder().permission(true).build())
                .subTask(new MockIssueBuilder().permission(false).build())
                .build();
        when(bulkEditBean.getSelectedIssues()).thenReturn(Collections.singletonList(issue));

        // then:
        assertThat("No permission for sub task should reject move.",
                bulkMoveOperation.canPerform(bulkEditBean, applicationUser), Matchers.is(false));
    }

    @Test
    public void noEventsAreDispatchedIfUpdateLogIsNull() {
        Issue issue = mock(Issue.class);
        GenericValue updateLog = null;
        boolean subtasksUpdated = true;
        ApplicationUser user = mock(ApplicationUser.class);
        boolean sendEmail = true;
        boolean issueWasMoved = true;

        bulkMoveOperation.dispatchEvents(
                issue,
                updateLog,
                issueChangeHolderWith(Arrays.asList(mock(ChangeItemBean.class)), subtasksUpdated),
                bulkEditBeanWith(sendEmail),
                user,
                issueWasMoved
        );

        verify(issueEventManager, never()).dispatchRedundantEvent(anyLong(), any(Issue.class), any(ApplicationUser.class), any(GenericValue.class), anyBoolean(), anyBoolean());
        verify(issueEventManager, never()).dispatchEvent(any(IssueEventBundle.class));
    }

    @Test
    public void noEventsAreDispatchedIfTheListOfChangedFieldsIsEmpty() {
        Issue issue = mock(Issue.class);
        GenericValue updateLog = new MockGenericValue("ChangeGroup");
        boolean subtasksUpdated = true;
        ApplicationUser user = mock(ApplicationUser.class);
        boolean sendEmail = true;
        boolean issueWasMoved = true;

        bulkMoveOperation.dispatchEvents(
                issue,
                updateLog,
                issueChangeHolderWith(Collections.<ChangeItemBean>emptyList(), subtasksUpdated),
                bulkEditBeanWith(sendEmail),
                user,
                issueWasMoved
        );

        verify(issueEventManager, never()).dispatchRedundantEvent(anyLong(), any(Issue.class), any(ApplicationUser.class), any(GenericValue.class), anyBoolean(), anyBoolean());
        verify(issueEventManager, never()).dispatchEvent(any(IssueEventBundle.class));
    }

    @Test
    public void issueMovedEventIsDispatchedIfTheIssueHasBeenMoved() {
        Issue issue = mock(Issue.class);
        GenericValue updateLog = new MockGenericValue("ChangeGroup");
        boolean subtasksUpdated = true;
        ApplicationUser user = mock(ApplicationUser.class);
        boolean sendEmail = true;
        boolean issueWasMoved = true;

        bulkMoveOperation.dispatchEvents(
                issue,
                updateLog,
                issueChangeHolderWith(Arrays.asList(mock(ChangeItemBean.class)), subtasksUpdated),
                bulkEditBeanWith(sendEmail),
                user,
                issueWasMoved
        );

        verify(issueEventManager).dispatchRedundantEvent(EventType.ISSUE_MOVED_ID, issue, user, updateLog, sendEmail, subtasksUpdated);
    }

    @Test
    public void issueUpdatedEventIsDispatchedIfTheIssueHasNotBeenMoved() {
        Issue issue = mock(Issue.class);
        GenericValue updateLog = new MockGenericValue("ChangeGroup");
        boolean subtasksUpdated = true;
        ApplicationUser user = mock(ApplicationUser.class);
        boolean sendEmail = true;
        boolean issueWasMoved = false;

        bulkMoveOperation.dispatchEvents(
                issue,
                updateLog,
                issueChangeHolderWith(Arrays.asList(mock(ChangeItemBean.class)), subtasksUpdated),
                bulkEditBeanWith(sendEmail),
                user,
                issueWasMoved
        );

        verify(issueEventManager).dispatchRedundantEvent(EventType.ISSUE_UPDATED_ID, issue, user, updateLog, sendEmail, subtasksUpdated);
    }

    @Test
    public void issueEventBundledIsDispatched() {
        Issue issue = mock(Issue.class);
        GenericValue updateLog = new MockGenericValue("ChangeGroup");
        boolean subtasksUpdated = true;
        ApplicationUser user = mock(ApplicationUser.class);
        boolean sendEmail = true;
        boolean issueWasMoved = false;

        IssueEventBundle issueEventBundle = mock(IssueEventBundle.class);
        when(issueEventBundleFactory.createIssueUpdateEventBundle(any(Issue.class), any(GenericValue.class), any(IssueUpdateBean.class), any(ApplicationUser.class))).thenReturn(issueEventBundle);

        bulkMoveOperation.dispatchEvents(
                issue,
                updateLog,
                issueChangeHolderWith(Arrays.asList(mock(ChangeItemBean.class)), subtasksUpdated),
                bulkEditBeanWith(sendEmail),
                user,
                issueWasMoved
        );

        verify(issueEventManager).dispatchEvent(issueEventBundle);
    }

    @Test
    public void parentLinkIsRemovedWhenDoingSubtaskToIssueConversion() {
        //additional setup
        when(issueManager.getIssueObject(subtask.getId())).thenReturn(subtask);
        when(bulkEditBean.getSelectedIssues()).thenReturn(ImmutableList.of(subtask));
        when(bulkEditBean.getTargetIssueObjects()).thenReturn(ImmutableMap.of(subtask, issueSingle));

        //object under tests
        bulkMoveOperation.moveIssuesAndIndex(bulkEditBean, applicationUser, context);

        //call to the issue link manages should be made
        verify(issueLinkManager, times(1)).getOutwardLinks(issueParent.getId());
        verify(issueLinkManager, times(1)).removeIssueLink(eq(parentToSubtaskLink), anyObject());
    }

    @Test
    public void onlyParentLinkIsRemovedWhenDoingSubtaskToIssueConversion() {
        //additional setup
        when(issueManager.getIssueObject(subtask.getId())).thenReturn(subtask);
        when(bulkEditBean.getSelectedIssues()).thenReturn(ImmutableList.of(subtask));
        when(bulkEditBean.getTargetIssueObjects()).thenReturn(ImmutableMap.of(subtask, issueSingle));

        //setup links
        List<IssueLink> outwardLinks = new ArrayList<>();
        outwardLinks.add(parentToSubtaskLink);
        // add some links from parent to child
        IssueLink someLink = createIssueLink(issueParent.getId(), subtask.getId(), true);
        outwardLinks.add(someLink);
        someLink = createIssueLink(issueParent.getId(), subtask.getId(), false);
        outwardLinks.add(someLink);

        when(issueLinkManager.getOutwardLinks(issueParent.getId())).thenReturn(outwardLinks);

        //object under tests
        bulkMoveOperation.moveIssuesAndIndex(bulkEditBean, applicationUser, context);

        //call to the issue link manages should be made
        verify(issueLinkManager, times(1)).getOutwardLinks(issueParent.getId());
        verify(issueLinkManager, times(1)).removeIssueLink(eq(parentToSubtaskLink), anyObject());
    }

    @Test
    public void parentLinkIsNotRemovedWhenDoingSubtaskToSubtaskConversion() {
        //additional setup
        when(issueManager.getIssueObject(subtask.getId())).thenReturn(subtask);
        when(bulkEditBean.getSelectedIssues()).thenReturn(ImmutableList.of(subtask));
        when(bulkEditBean.getTargetIssueObjects()).thenReturn(ImmutableMap.of(subtask, subtaskOther));

        //setup links
        List<IssueLink> outwardLinks = new ArrayList<>();
        outwardLinks.add(parentToSubtaskLink);
        // add some links from parent to child
        IssueLink someLink = createIssueLink(issueParent.getId(), subtask.getId(), true);
        outwardLinks.add(someLink);
        someLink = createIssueLink(issueParent.getId(), subtask.getId(), false);
        outwardLinks.add(someLink);

        when(issueLinkManager.getOutwardLinks(issueParent.getId())).thenReturn(outwardLinks);

        //object under tests
        bulkMoveOperation.moveIssuesAndIndex(bulkEditBean, applicationUser, context);

        //no calls to issueLinkManager should be made
        verify(issueLinkManager, times(0)).getOutwardLinks(any());
        verify(issueLinkManager, times(0)).removeIssueLink(anyObject(), anyObject());
    }

    private IssueLink createIssueLink(final Long sourceId, final Long destinationId, final boolean isSubtask) {
        IssueLink someLink = mock(IssueLink.class);
        IssueLinkType someLinkType = mock(IssueLinkType.class);
        when(someLink.getIssueLinkType()).thenReturn(someLinkType);
        when(someLinkType.isSubTaskLinkType()).thenReturn(isSubtask);
        when(someLink.getSourceId()).thenReturn(sourceId);
        when(someLink.getDestinationId()).thenReturn(destinationId);
        return someLink;
    }

    @Test
    public void parentLinkIsAddedFromNewParentWhenDoingSubtaskSubtaskWithNewParentConversion() throws CreateException {
        //additional setup
        when(issueManager.getIssueObject(subtask.getId())).thenReturn(subtask);
        when(bulkEditBean.getSelectedIssues()).thenReturn(ImmutableList.of(subtask));
        subtaskOther.setParentId(issueParentNew.getId());
        subtaskOther.setParentObject(issueParentNew);
        when(bulkEditBean.getTargetIssueObjects()).thenReturn(ImmutableMap.of(subtask, subtaskOther));

        //object under tests
        bulkMoveOperation.moveIssuesAndIndex(bulkEditBean, applicationUser, context);

        verify(issueLinkManager, times(1)).getOutwardLinks(anyObject());
        verify(issueLinkManager, times(1)).removeIssueLink(eq(parentToSubtaskLink), anyObject());
        //new link should be added between new parent and child
        verify(subTaskManager, times(1)).createSubTaskIssueLink(eq(issueParentNew), eq(subtaskOther), anyObject());
    }

    @Test
    public void parentLinkIsRemovedFromCurrentParentWhenDoingSubtaskSubtaskWithNewParentConversion() {
        //additional setup
        when(issueManager.getIssueObject(subtask.getId())).thenReturn(subtask);
        when(bulkEditBean.getSelectedIssues()).thenReturn(ImmutableList.of(subtask));
        subtaskOther.setParentId(issueParentNew.getId());
        subtaskOther.setParentObject(issueParentNew);
        when(bulkEditBean.getTargetIssueObjects()).thenReturn(ImmutableMap.of(subtask, subtaskOther));

        //object under tests
        bulkMoveOperation.moveIssuesAndIndex(bulkEditBean, applicationUser, context);

        //call to the issue link manages should be made
        verify(issueLinkManager, times(1)).getOutwardLinks(issueParent.getId());
        verify(issueLinkManager, times(1)).removeIssueLink(eq(parentToSubtaskLink), anyObject());
    }

    @Test
    public void noLinksAreTouchedWhenDoingSubtaskSubtaskWithoutNewParentConversion() throws CreateException {
        //additional setup
        when(issueManager.getIssueObject(subtask.getId())).thenReturn(subtask);
        when(bulkEditBean.getSelectedIssues()).thenReturn(ImmutableList.of(subtask));
        when(bulkEditBean.getTargetIssueObjects()).thenReturn(ImmutableMap.of(subtask, subtaskOther));

        //object under tests
        bulkMoveOperation.moveIssuesAndIndex(bulkEditBean, applicationUser, context);

        verify(issueLinkManager, times(0)).getOutwardLinks(anyObject());
        verify(issueLinkManager, times(0)).removeIssueLink(eq(parentToSubtaskLink), anyObject());
        verify(subTaskManager, times(0)).createSubTaskIssueLink(any(Issue.class), any(Issue.class), anyObject());
    }

    @Test
    public void validationFailsWhenParentIssueIsRemovedFromDbBetweenFormProcessingAndBulkMoveExecution()
            throws CreateException {
        //additional setup
        when(bulkEditBean.getSelectedIssues()).thenReturn(ImmutableList.of(subtask));
        subtaskOther.setParentId(issueParentNew.getId());
        subtaskOther.setParentObject(issueParentNew);
        when(bulkEditBean.getTargetIssueObjects()).thenReturn(ImmutableMap.of(subtask, subtaskOther));

        //fake removal of new parent issue from db
        when(issueManager.getIssueObject(issueParentNew.getId())).thenReturn(null);

        //object under tests
        Optional<Issue> concurrentUpdate = bulkMigrateOperation.isConcurrentIssueUpdateSingleBulkEditBean(bulkEditBean);

        assertTrue(concurrentUpdate.isPresent());
        assertThat(concurrentUpdate.get().getKey(), equalTo(subtask.getKey()));
    }


    @Test
    public void validationFailsThrownWhenParentIssueIsMovedBetweenFormProcessingAndBulkMoveExecution()
            throws CreateException {
        //additional setup
        when(bulkEditBean.getSelectedIssues()).thenReturn(ImmutableList.of(subtask));
        subtaskOther.setParentId(issueParentNew.getId());
        subtaskOther.setParentObject(issueParentNew);
        when(bulkEditBean.getTargetIssueObjects()).thenReturn(ImmutableMap.of(subtask, subtaskOther));

        //fake move of new parent issue
        MutableIssue parentIssueNewMoved = new MockIssueBuilder().id(subtaskOther.getParentId()).projectId(issueParentNew.getProjectId() + 1).projectObject(project).issueTypeId("1").build();
        when(issueManager.getIssueObject(subtaskOther.getParentId())).thenReturn(parentIssueNewMoved);

        //object under tests
        Optional<Issue> concurrentUpdate = bulkMigrateOperation.isConcurrentIssueUpdateSingleBulkEditBean(bulkEditBean);

        assertTrue(concurrentUpdate.isPresent());
        assertThat(concurrentUpdate.get().getKey(), equalTo(subtask.getKey()));
    }

    @Test
    public void validationFailsThrownWhenParentIssueHasChangedIssueTypeBetweenFormProcessingAndBulkMoveExecution()
            throws CreateException {
        //additional setup
        when(bulkEditBean.getSelectedIssues()).thenReturn(ImmutableList.of(subtask));
        subtaskOther.setParentId(issueParentNew.getId());
        subtaskOther.setParentObject(issueParentNew);
        when(bulkEditBean.getTargetIssueObjects()).thenReturn(ImmutableMap.of(subtask, subtaskOther));

        //fake parent issue change
        when(issueManager.getIssueObject(issueParentNew.getId())).thenReturn(issueParentNewChangedIssueType);

        //object under tests
        Optional<Issue> concurrentUpdate = bulkMigrateOperation.isConcurrentIssueUpdateSingleBulkEditBean(bulkEditBean);

        assertTrue(concurrentUpdate.isPresent());
        assertThat(concurrentUpdate.get().getKey(), equalTo(subtask.getKey()));
    }

    @Test
    public void validationFailsThrownWhenParentIssueHasChangedSecurityLevelBetweenFormProcessingAndBulkMoveExecution()
            throws CreateException {
        //additional setup
        when(bulkEditBean.getSelectedIssues()).thenReturn(ImmutableList.of(subtask));
        subtaskOther.setParentId(issueParentNew.getId());
        subtaskOther.setParentObject(issueParentNew);
        when(bulkEditBean.getTargetIssueObjects()).thenReturn(ImmutableMap.of(subtask, subtaskOther));

        //fake parent issue change
        when(issueManager.getIssueObject(issueParentNew.getId())).thenReturn(issueParentNewChangedSecurityLevel);

        //object under tests
        Optional<Issue> concurrentUpdate = bulkMigrateOperation.isConcurrentIssueUpdateSingleBulkEditBean(bulkEditBean);

        assertTrue(concurrentUpdate.isPresent());
        assertThat(concurrentUpdate.get().getKey(), equalTo(subtask.getKey()));
    }

    @Test
    public void validationFailsThrownWhenParentIssueHasChangedWorkflowBetweenFormProcessingAndBulkMoveExecution()
            throws CreateException {
        //additional setup
        when(bulkEditBean.getSelectedIssues()).thenReturn(ImmutableList.of(subtask));
        subtaskOther.setParentId(issueParentNew.getId());
        subtaskOther.setParentObject(issueParentNew);
        when(bulkEditBean.getTargetIssueObjects()).thenReturn(ImmutableMap.of(subtask, subtaskOther));

        //fake parent issue change
        when(issueManager.getIssueObject(issueParentNew.getId())).thenReturn(issueParentNewChangedWorkflow);

        //object under tests
        Optional<Issue> concurrentUpdate = bulkMigrateOperation.isConcurrentIssueUpdateSingleBulkEditBean(bulkEditBean);

        assertTrue(concurrentUpdate.isPresent());
        assertThat(concurrentUpdate.get().getKey(), equalTo(subtask.getKey()));
    }

    @Test
    public void noLinksAreTouchedWhenDoingSubtaskWithoutParentToSubtaskConversion() throws CreateException {
        //additional setup
        when(issueManager.getIssueObject(subtaskWithoutParent.getId())).thenReturn(subtaskWithoutParent);
        when(bulkEditBean.getSelectedIssues()).thenReturn(ImmutableList.of(subtaskWithoutParent));
        when(bulkEditBean.getTargetIssueObjects()).thenReturn(ImmutableMap.of(subtaskWithoutParent, subtaskWithoutParent));

        //object under tests
        bulkMoveOperation.moveIssuesAndIndex(bulkEditBean, applicationUser, context);

        verify(issueLinkManager, times(0)).getOutwardLinks(anyObject());
        verify(issueLinkManager, times(0)).removeIssueLink(anyObject(), anyObject());
        verify(subTaskManager, times(0)).createSubTaskIssueLink(any(Issue.class), any(Issue.class), anyObject());
    }

    @Test
    public void noLinksAreTouchedWhenDoingSubtaskWithoutParentToIssueConversion() throws CreateException {
        //additional setup
        when(issueManager.getIssueObject(subtaskWithoutParent.getId())).thenReturn(subtaskWithoutParent);
        when(bulkEditBean.getSelectedIssues()).thenReturn(ImmutableList.of(subtaskWithoutParent));
        when(bulkEditBean.getTargetIssueObjects()).thenReturn(ImmutableMap.of(subtaskWithoutParent, issueSingle));

        //object under tests
        bulkMoveOperation.moveIssuesAndIndex(bulkEditBean, applicationUser, context);

        verify(issueLinkManager, times(0)).getOutwardLinks(anyObject());
        verify(issueLinkManager, times(0)).removeIssueLink(anyObject(), anyObject());
        verify(subTaskManager, times(0)).createSubTaskIssueLink(any(Issue.class), any(Issue.class), anyObject());
    }

    @Test
    public void validationFailsThrownWhenIssueHasChangedParentBetweenFormProcessingAndBulkMoveExecution()
            throws CreateException {
        //additional setup
        when(bulkEditBean.getSelectedIssues()).thenReturn(ImmutableList.of(subtask));
        when(bulkEditBean.getTargetIssueObjects()).thenReturn(ImmutableMap.of(subtask, subtaskOther));

        //fake parent change
        final MutableIssue issueModified = new MockIssue(subtask.getGenericValue());
        issueModified.setParentId(subtask.getParentId() + 1);
        when(issueManager.getIssueObject(subtask.getId())).thenReturn(issueModified);

        //object under tests
        Optional<Issue> concurrentUpdate = bulkMigrateOperation.isConcurrentIssueUpdateSingleBulkEditBean(bulkEditBean);

        assertTrue(concurrentUpdate.isPresent());
        assertThat(concurrentUpdate.get().getKey(), equalTo(subtask.getKey()));
    }

    @Test
    public void validationFailsThrownWhenIssueHasChangedSecurityLevelBetweenFormProcessingAndBulkMoveExecution()
            throws CreateException {
        //additional setup
        when(bulkEditBean.getSelectedIssues()).thenReturn(ImmutableList.of(subtask));
        when(bulkEditBean.getTargetIssueObjects()).thenReturn(ImmutableMap.of(subtask, subtaskOther));

        //fake security level change
        final MutableIssue issueModified = new MockIssue(subtask.getGenericValue());
        issueModified.setSecurityLevelId(123l);
        when(issueManager.getIssueObject(subtask.getId())).thenReturn(issueModified);

        //object under tests
        Optional<Issue> concurrentUpdate = bulkMigrateOperation.isConcurrentIssueUpdateSingleBulkEditBean(bulkEditBean);

        assertTrue(concurrentUpdate.isPresent());
        assertThat(concurrentUpdate.get().getKey(), equalTo(subtask.getKey()));
    }

    @Test
    public void validationFailsThrownWhenIssueHasChangedProjectBetweenFormProcessingAndBulkMoveExecution()
            throws CreateException {
        //additional setup
        when(bulkEditBean.getSelectedIssues()).thenReturn(ImmutableList.of(subtask));
        when(bulkEditBean.getTargetIssueObjects()).thenReturn(ImmutableMap.of(subtask, subtaskOther));

        //fake project change
        final MutableIssue issueModified = new MockIssue(subtask.getGenericValue());
        issueModified.setProjectId(subtask.getProjectId() + 1);
        when(issueManager.getIssueObject(subtask.getId())).thenReturn(issueModified);

        //object under tests
        Optional<Issue> concurrentUpdate = bulkMigrateOperation.isConcurrentIssueUpdateSingleBulkEditBean(bulkEditBean);

        assertTrue(concurrentUpdate.isPresent());
        assertThat(concurrentUpdate.get().getKey(), equalTo(subtask.getKey()));
    }

    private IssueChangeHolder issueChangeHolderWith(List<ChangeItemBean> changes, boolean subtasksUpdated) {
        IssueChangeHolder issueChangeHolder = mock(IssueChangeHolder.class);
        when(issueChangeHolder.getChangeItems()).thenReturn(changes);
        when(issueChangeHolder.isSubtasksUpdated()).thenReturn(subtasksUpdated);
        return issueChangeHolder;
    }

    private BulkEditBean bulkEditBeanWith(boolean sendEmail) {
        BulkEditBean bulkEditBean = mock(BulkEditBean.class);
        when(bulkEditBean.isSendBulkNotification()).thenReturn(sendEmail);
        return bulkEditBean;
    }

    class MockIssueBuilder {
        private final MutableIssue issue = mock(MutableIssue.class);
        private final Collection<Issue> subTasks = Lists.newArrayList();

        MutableIssue build() {
            when(issue.getSubTaskObjects()).thenReturn(subTasks);
            return issue;
        }

        MockIssueBuilder permission(final boolean hasPermission) {
            when(permissionManager.hasPermission(Permissions.MOVE_ISSUE, issue, applicationUser)).thenReturn(hasPermission);
            return this;
        }

        MockIssueBuilder subTask(final Issue subIssue) {
            subTasks.add(subIssue);
            return this;
        }

        MockIssueBuilder id(final Long id) {
            when(issue.getId()).thenReturn(id);
            return this;
        }

        MockIssueBuilder projectId(final Long id) {
            when(issue.getProjectId()).thenReturn(id);
            return this;
        }

        MockIssueBuilder parentId(final Long id) {
            when(issue.getParentId()).thenReturn(id);
            return this;
        }

        MockIssueBuilder projectObject(final Project project) {
            when(issue.getProjectObject()).thenReturn(project);
            return this;
        }

        MockIssueBuilder issueTypeId(final String issueTypeId) {
            when(issue.getIssueTypeId()).thenReturn(issueTypeId);
            return this;
        }

        MockIssueBuilder workflowId(final Long workflowId) {
            when(issue.getWorkflowId()).thenReturn(workflowId);
            return this;
        }

        MockIssueBuilder securityLevelId(final Long securityLevelId) {
            when(issue.getSecurityLevelId()).thenReturn(securityLevelId);
            return this;
        }
    }
}
