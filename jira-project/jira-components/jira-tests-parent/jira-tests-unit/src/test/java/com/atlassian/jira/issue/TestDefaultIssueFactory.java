package com.atlassian.jira.issue;

import com.atlassian.jira.bc.project.component.ProjectComponentManager;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.label.Label;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.mock.issue.MockIssue;
import com.google.common.collect.ImmutableList;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.mockito.Mock;
import org.ofbiz.core.entity.GenericValue;

import java.util.List;

import static org.hamcrest.Matchers.anyOf;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.collection.IsMapContaining.hasKey;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


public class TestDefaultIssueFactory {
    @Rule
    public final RuleChain mockitoMocks = MockitoMocksInContainer.forTest(this);

    @AvailableInContainer
    @Mock
    private CustomFieldManager customFieldManager;
    @Mock
    private CustomField cf1;
    @Mock
    private CustomField cf2;
    @Mock
    private CustomField cf3;

    private final String cf1Id = "cf1Id";
    private final String cf2Id = "cf2Id";
    private final String cf3Id = "cf3Id";
    private final String cf1Value = "cf1Value";
    private final Integer cf2Value = Integer.valueOf(321);
    private final List<Label> cf3Value = ImmutableList.of(new Label(1L, 10900L, "label1"));

    @Before
    public void setUp() {
        when(cf1.getId()).thenReturn(cf1Id);
        when(cf2.getId()).thenReturn(cf2Id);
        when(cf3.getId()).thenReturn(cf3Id);

        final List<CustomField> customFields = ImmutableList.of(cf1, cf2, cf3);

        when(customFieldManager.getCustomFieldObjects(any(Issue.class))).thenReturn(customFields);

    }

    @Test
    public void copyHasSameParentAsOriginal() throws Exception {
        //noinspection deprecation
        DefaultIssueFactory factory = new DefaultIssueFactory(null, null, null, null, null, null, null, null, null, null, mock(ProjectComponentManager.class), null, null, null);
        MockIssue mock = new MockIssue(123L);

        Issue copy = factory.cloneIssue(mock);
        assertEquals(mock.getParentId(), copy.getParentId());

        mock.setParentId(987L);
        copy = factory.cloneIssue(mock);
        assertEquals(mock.getParentId(), copy.getParentId());
    }

    @Test
    public void clonesCustomFieldsUsingAllFieldsSetting() {
        DefaultIssueFactory factory = new DefaultIssueFactory(null, null, null, null, null, null, null, null, null, null, mock(ProjectComponentManager.class), null, null, null);

        MutableIssue original = createOriginalIssue();
        MutableIssue clone = factory.cloneIssueWithAllFields(original);

        assertThat(clone.getCustomFieldValue(cf1), is(original.getCustomFieldValue(cf1)));
        assertThat(clone.getCustomFieldValue(cf2), is(original.getCustomFieldValue(cf2)));
        assertThat(clone.getCustomFieldValue(cf3), is(original.getCustomFieldValue(cf3)));

        assertThat(clone.getModifiedFields().get(cf1Id).getNewValue(), is(original.getModifiedFields().get(cf1Id).getNewValue()));
        assertThat(clone.getModifiedFields().get(cf2Id).getNewValue(), is(original.getModifiedFields().get(cf2Id).getNewValue()));
        assertThat(clone.getModifiedFields().get(cf3Id).getNewValue(), is(original.getModifiedFields().get(cf3Id).getNewValue()));
    }

    @Test
    public void notClonesCustomFieldsUsingWithoutCustomFieldsSetting() {
        DefaultIssueFactory factory = new DefaultIssueFactory(null, null, null, null, null, null, null, null, null, null, mock(ProjectComponentManager.class), null, null, null);
        MutableIssue original = createOriginalIssue();

        MutableIssue clone = factory.cloneIssue(original);

        assertThat(clone.getModifiedFields(), not(anyOf(hasKey(cf1Id), hasKey(cf2Id), hasKey(cf3Id))));

        // when customFieldValues has no mapping for given key it's populated with customField.getDefaultValue(this) which will be null
        assertThat(clone.getCustomFieldValue(cf1), is(nullValue()));
        assertThat(clone.getCustomFieldValue(cf2), is(nullValue()));
        assertThat(clone.getCustomFieldValue(cf3), is(nullValue()));
    }

    MutableIssue createOriginalIssue() {
        MutableIssue original = new IssueImpl((GenericValue) null, null, null, null, null, null, null, null, null, mock(ProjectComponentManager.class), null, null);

        original.setCustomFieldValue(cf1, cf1Value);
        original.setCustomFieldValue(cf2, cf2Value);
        original.setCustomFieldValue(cf3, cf3Value);
        return original;
    }
}
