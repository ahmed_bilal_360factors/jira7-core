package com.atlassian.jira.util;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import org.hamcrest.Matchers;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.List;
import java.util.function.IntFunction;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class PagesTest {

    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Test
    public void testCreatingPageFromIterable() {
        List<Testee> values = listWithOrderedItems(100, Testee::new);

        Page<Result> page = Pages.toPage(values, PageRequests.request(10L, 10), t -> true, t -> new Result(t.getIndex()));

        Result[] results = Iterables.toArray(listWithOrderedItems(IntStream.range(10, 20), Result::new), Result.class);
        assertThat(page, PageMatcher.matcher(is(10l), is(100l), is(10), contains(results)));
    }

    @Test
    public void testGettingLessResultsIfCollectionSmallerThenExpected() {
        List<Testee> values = listWithOrderedItems(100, Testee::new);
        Page<Result> page = Pages.toPage(values, PageRequests.request(95L, 10), t -> true, t -> new Result(t.getIndex()));

        assertThat(page, PageMatcher.matcher(is(95l), is(100l), is(5), Matchers.equalTo(listWithOrderedItems(IntStream.range(95, 100), Result::new))));
    }

    @Test
    public void testRequestedStartGreaterThenCollectionSize() {
        List<Testee> values = listWithOrderedItems(100, Testee::new);

        Page<Result> page = Pages.toPage(values, PageRequests.request(100L, 10), t -> true, t -> new Result(t.getIndex()));

        assertThat(page, PageMatcher.matcher(is(100l), is(100l), is(0), Matchers.<Result>hasSize(0)));
    }

    @Test
    public void testGettingOnlyTheseElementsWhichMatchFilter() throws Exception {
        List<Testee> values = listWithOrderedItems(100, Testee::new);
        Page<Result> page = Pages.toPage(values, PageRequests.request(0L, 10), t -> t.getIndex() == 5, t -> new Result(t.getIndex()));

        assertThat(page, PageMatcher.matcher(is(0l), is(1l), is(1), Matchers.<Result>contains(new Result(5))));
    }

    @Test
    public void testRequestedMoreElementsThenCollectionSize() {
        List<Testee> values = listWithOrderedItems(100, Testee::new);
        Page<Result> page = Pages.toPage(values, PageRequests.request(0L, 120), t -> t.getIndex() < 50, t -> new Result(t.getIndex()));

        Result[] results = Iterables.toArray(listWithOrderedItems(IntStream.range(0, 50), Result::new), Result.class);
        assertThat(page, PageMatcher.matcher(is(0l), is(50l), is(50), contains(results)));
    }

    @Test
    public void testGettingFirstElement() {
        List<Testee> values = listWithOrderedItems(100, Testee::new);
        Page<Result> page = Pages.toPage(values, PageRequests.request(0l, 1), t -> true, t -> new Result(t.getIndex()));

        assertThat(page, PageMatcher.matcher(is(0l), is(100l), is(1), Matchers.<Result>contains(new Result(0))));
    }

    @Test
    public void testGettingLastElement() {
        List<Testee> values = listWithOrderedItems(100, Testee::new);
        Page<Result> page = Pages.toPage(values, PageRequests.request(99l, 1), t -> true, t -> new Result(t.getIndex()));

        assertThat(page, PageMatcher.matcher(is(99l), is(100l), is(1), Matchers.<Result>contains(new Result(99))));
    }

    @Test
    public void testPageSizeCantBeGreaterThenGlobalPageSizeLimit() {
        int tooBigPageSize = PageRequest.MAX_PAGE_LIMIT + 1;
        List<Testee> values = listWithOrderedItems(tooBigPageSize, Testee::new);
        Page<Result> page = Pages.toPage(values, PageRequests.request(0l, tooBigPageSize), t -> true, t -> new Result(t.getIndex()));

        Result[] results = Iterables.toArray(listWithOrderedItems(IntStream.range(0, PageRequest.MAX_PAGE_LIMIT), Result::new), Result.class);
        assertThat(page, PageMatcher.matcher(is(0l), is((long) tooBigPageSize), is(PageRequest.MAX_PAGE_LIMIT), contains(results)));
    }

    @Test
    public void testPageWithTotalCountFactoryMethodHasCorrectIsLast() {
        assertTrue(Pages.page(ImmutableList.of(1, 2), 2, PageRequests.request(0L, 2)).isLast());
        assertTrue(Pages.page(ImmutableList.of(1, 2), 10, PageRequests.request(8L, 2)).isLast());
        assertTrue(Pages.page(ImmutableList.of(1, 2), 10, PageRequests.request(8L, 4)).isLast());
        assertFalse(Pages.page(ImmutableList.of(1, 2), 10, PageRequests.request(0L, 2)).isLast());
        assertFalse(Pages.page(ImmutableList.of(1, 2), 3, PageRequests.request(0L, 2)).isLast());
    }

    @Test
    public void testPageWithTotalCountFactoryMethodThrowsIllegalArgumentExceptionIfNotLastPageAndNumberOfValuesIsDifferentThanThePageLimit() {
        exception.expectMessage("Inconsistent arguments: this is not the last page and yet the number of values is different than the page limit");
        Pages.page(ImmutableList.of(1, 2), 3, PageRequests.request(0L, 3));
        exception.expectMessage("Inconsistent arguments: this is not the last page and yet the number of values is different than the page limit");
        Pages.page(ImmutableList.of(1, 2, 3), 2, PageRequests.request(0L, 5));
    }

    @Test
    public void testPageWithTotalCountFactoryMethodThrowsIllegalArgumentExceptionIfValuesCountGreaterThanTotalCount() {
        exception.expectMessage("Inconsistent arguments: size of the page is greater than totalCount");
        Pages.page(ImmutableList.of(1, 2, 3), 2, PageRequests.request(0L, 3));
    }

    private <E> List<E> listWithOrderedItems(final int end, IntFunction<E> newElement) {
        return listWithOrderedItems(IntStream.range(0, end), newElement);
    }

    private <E> List<E> listWithOrderedItems(IntStream range, IntFunction<E> newElement) {
        return range
                .mapToObj(newElement)
                .collect(Collectors.toList());
    }

    private static abstract class Indexed {
        private final int index;

        public Indexed(final int index) {
            this.index = index;
        }

        public int getIndex() {
            return index;
        }

        @Override
        public boolean equals(final Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }

            final Indexed indexed = (Indexed) o;

            if (index != indexed.index) {
                return false;
            }

            return true;
        }

        @Override
        public int hashCode() {
            return index;
        }
    }

    private static final class Result extends Indexed {
        public Result(final int index) {
            super(index);
        }
    }

    private static final class Testee extends Indexed {
        public Testee(final int index) {
            super(index);
        }
    }

}