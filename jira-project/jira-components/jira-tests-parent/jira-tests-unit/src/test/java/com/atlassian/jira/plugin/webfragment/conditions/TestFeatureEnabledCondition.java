package com.atlassian.jira.plugin.webfragment.conditions;


import com.atlassian.jira.config.FeatureManager;
import com.atlassian.plugin.PluginParseException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestFeatureEnabledCondition {
    @Mock
    private FeatureManager featureManager;
    private FeatureEnabledCondition featureEnabledCondition;
    private static final String PARAMETER_NAME = "feature-key";
    private static final String FEATURE_KEY = "feature.enabled";

    @Before
    public void setup() {
        featureManager = mock(FeatureManager.class);
        featureEnabledCondition = new FeatureEnabledCondition(featureManager);
    }

    @Test
    public void test_configuredCorrectly() {
        when(featureManager.isEnabled(FEATURE_KEY)).thenReturn(true);

        Map<String, String> params = new HashMap();
        params.put(PARAMETER_NAME, FEATURE_KEY);
        featureEnabledCondition.init(params);

        boolean actual = featureEnabledCondition.shouldDisplay(new HashMap<String, Object>());

        assertEquals(true, actual);
    }

    @Test(expected = PluginParseException.class)
    public void test_misconfigured() {
        Map<String, String> params = new HashMap();
        featureEnabledCondition.init(params);
    }
}
