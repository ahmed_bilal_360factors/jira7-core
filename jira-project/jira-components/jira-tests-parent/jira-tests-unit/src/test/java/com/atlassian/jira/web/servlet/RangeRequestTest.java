package com.atlassian.jira.web.servlet;

import junit.framework.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.Date;
import java.util.TimeZone;

import static junit.framework.Assert.assertEquals;

public class RangeRequestTest {
    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Test
    public void testParseSimple() throws BadRequestException {
        assertEquals(new RangeRequest(0, 1), RangeRequest.parse("bytes=0-1"));
        assertEquals(new RangeRequest(null, 12), RangeRequest.parse("bytes=-12"));
        assertEquals(new RangeRequest(16, null), RangeRequest.parse("bytes=16-"));
    }

    @Test
    public void testParseMultipleRanges() throws BadRequestException {
        assertEquals(new RangeRequest(0, 1), RangeRequest.parse("bytes=0-1,512-612,123383-"));
        assertEquals(new RangeRequest(null, 12), RangeRequest.parse("bytes=-12,512-612,123383-"));
        assertEquals(new RangeRequest(1024, null), RangeRequest.parse("bytes=1024-,1-3,500-559"));
    }

    @Test
    public void testParseInvalid() {
        assertBadRequestException("bytes=");
        assertBadRequestException("bytes=-");
        assertBadRequestException("bytes=,0-1");
        assertBadRequestException("bytes=4-foo");
        assertBadRequestException("bytes=4x5-3");
    }

    private void assertBadRequestException(String rangeValue) {
        try {
            RangeRequest.parse(rangeValue);
            Assert.fail();
        } catch (BadRequestException ex) {
            assertEquals("Malformed Range header", ex.getMessage());
        }
    }

    @Test
    public void testToString() {
        assertEquals("bytes=0-1", new RangeRequest(0, 1).toString());
        assertEquals("bytes=601-", new RangeRequest(601, null).toString());
        assertEquals("bytes=-1024", new RangeRequest(null, 1024).toString());
    }
}
