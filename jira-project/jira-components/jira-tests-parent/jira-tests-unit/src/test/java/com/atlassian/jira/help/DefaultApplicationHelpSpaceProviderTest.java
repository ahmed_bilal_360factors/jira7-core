package com.atlassian.jira.help;

import com.atlassian.application.api.Application;
import com.atlassian.application.api.ApplicationKey;
import com.atlassian.application.api.ApplicationManager;
import com.atlassian.fugue.Option;
import com.atlassian.jira.config.FeatureManager;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.net.URI;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;

public class DefaultApplicationHelpSpaceProviderTest {
    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @Mock
    ApplicationManager applicationManager;

    @Mock
    Application application;

    @InjectMocks
    private DefaultApplicationHelpSpaceProvider docsVersion;

    private static final ApplicationKey KEY = ApplicationKey.valueOf("some-key");

    @Before
    public void setUp() {
        when(applicationManager.getApplication(KEY)).thenReturn(Option.some(application));
    }

    @Test
    public void returnsNoneForNonExistingApp() {
        when(applicationManager.getApplication(KEY)).thenReturn(Option.none());

        assertThat(docsVersion.getHelpSpace(KEY), is(Option.none()));
    }

    @Test
    public void returnsApplicationHelpSpaceInServer() {
        URI uri = URI.create("some-space-080");
        when(application.getProductHelpServerSpaceURI()).thenReturn(Option.some(uri));

        assertThat(docsVersion.getHelpSpace(KEY), is(Option.some("some-space-080")));
    }

}