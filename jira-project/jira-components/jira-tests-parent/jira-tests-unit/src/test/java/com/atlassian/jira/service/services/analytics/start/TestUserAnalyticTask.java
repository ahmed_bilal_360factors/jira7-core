package com.atlassian.jira.service.services.analytics.start;

import com.atlassian.jira.license.LicenseCountService;
import com.atlassian.jira.user.util.UserManager;
import org.junit.Test;

import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestUserAnalyticTask {

    @Test
    public void testAnalytic() {
        LicenseCountService licenseCountService = mock(LicenseCountService.class);
        UserManager userManager = mock(UserManager.class);
        UserAnalyticTask task = new UserAnalyticTask(licenseCountService, userManager);

        when(userManager.getTotalUserCount()).thenReturn(1000);
        when(licenseCountService.totalBillableUsers()).thenReturn(500);

        Map<String, Object> analytics = task.getAnalytics();

        assertEquals(1000, analytics.get("users.total"));
        assertEquals(500, analytics.get("users.active"));
    }
}