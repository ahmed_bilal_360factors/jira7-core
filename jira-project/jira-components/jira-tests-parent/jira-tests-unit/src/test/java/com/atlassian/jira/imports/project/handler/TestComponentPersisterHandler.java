package com.atlassian.jira.imports.project.handler;

import com.atlassian.jira.external.beans.ExternalNodeAssociation;
import com.atlassian.jira.imports.project.ProjectImportPersister;
import com.atlassian.jira.imports.project.core.BackupSystemInformation;
import com.atlassian.jira.imports.project.core.ProjectImportResultsImpl;
import com.atlassian.jira.imports.project.mapper.ProjectImportMapper;
import com.atlassian.jira.imports.project.mapper.ProjectImportMapperImpl;
import com.atlassian.jira.imports.project.parser.NodeAssociationParser;
import com.atlassian.jira.imports.project.transformer.ComponentTransformer;
import com.atlassian.jira.web.bean.MockI18nBean;
import org.junit.Test;

import static com.atlassian.jira.imports.project.parser.NodeAssociationParser.COMPONENT_TYPE;
import static com.atlassian.jira.imports.project.parser.NodeAssociationParser.NODE_ASSOCIATION_ENTITY_NAME;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

/**
 * @since v3.13
 */
public class TestComponentPersisterHandler {
    private ProjectImportMapper projectImportMapper = new ProjectImportMapperImpl(null, null);

    @Test
    public void testHandle() throws Exception {
        final ExternalNodeAssociation externalNodeAssociation = new ExternalNodeAssociation("", "", "", "", COMPONENT_TYPE);

        final NodeAssociationParser mockNodeAssociationParser = mock(NodeAssociationParser.class);
        when(mockNodeAssociationParser.parse(null)).thenReturn(externalNodeAssociation);

        final ComponentTransformer mockComponentTransformer = mock(ComponentTransformer.class);
        when(mockComponentTransformer.transform(projectImportMapper, externalNodeAssociation)).thenReturn(externalNodeAssociation);

        final ProjectImportPersister mockProjectImportPersister = mock(ProjectImportPersister.class);
        when(mockProjectImportPersister.createAssociation(externalNodeAssociation)).thenReturn(true);

        final ProjectImportResultsImpl projectImportResults = new ProjectImportResultsImpl(0, 0, 0, 0, new MockI18nBean());
        ComponentPersisterHandler componentPersisterHandler = new ComponentPersisterHandler(mockProjectImportPersister, projectImportMapper, projectImportResults, null, new ExecutorForTests()) {
            NodeAssociationParser getNodeAssociationParser() {
                return mockNodeAssociationParser;
            }

            ComponentTransformer getComponentTransformer() {
                return mockComponentTransformer;
            }
        };

        componentPersisterHandler.handleEntity(NODE_ASSOCIATION_ENTITY_NAME, null);
        componentPersisterHandler.handleEntity("NOTComponent", null);

        assertThat(projectImportResults.getErrors(), hasSize(0));
    }

    @Test
    public void testHandleNonComponent() throws Exception {
        ProjectImportMapper projectImportMapper = mock(ProjectImportMapper.class);

        final NodeAssociationParser mockNodeAssociationParser = mock(NodeAssociationParser.class);
        when(mockNodeAssociationParser.parse(null)).thenReturn(null);

        final ComponentTransformer mockComponentTransformer = mock(ComponentTransformer.class);

        final ProjectImportPersister mockProjectImportPersister = mock(ProjectImportPersister.class);

        final ProjectImportResultsImpl projectImportResults = new ProjectImportResultsImpl(0, 0, 0, 0, new MockI18nBean());
        ComponentPersisterHandler componentPersisterHandler = new ComponentPersisterHandler(mockProjectImportPersister, projectImportMapper, projectImportResults, null, null) {
            NodeAssociationParser getNodeAssociationParser() {
                return mockNodeAssociationParser;
            }

            ComponentTransformer getComponentTransformer() {
                return mockComponentTransformer;
            }
        };

        componentPersisterHandler.handleEntity(NODE_ASSOCIATION_ENTITY_NAME, null);
        componentPersisterHandler.handleEntity("NOTComponent", null);

        verifyZeroInteractions(mockComponentTransformer, mockProjectImportPersister, projectImportMapper);
    }

    @Test
    public void testHandleErrorAddingFixComponent() throws Exception {
        projectImportMapper.getComponentMapper().registerOldValue("12", "Component 1");
        final ExternalNodeAssociation externalNodeAssociation = new ExternalNodeAssociation("34", "", "12", "", COMPONENT_TYPE);

        final ExternalNodeAssociation transformedExternalNodeAssociation = new ExternalNodeAssociation("34", "", "12", "", COMPONENT_TYPE);

        final NodeAssociationParser mockNodeAssociationParser = mock(NodeAssociationParser.class);
        when(mockNodeAssociationParser.parse(null)).thenReturn(externalNodeAssociation);

        final ComponentTransformer mockComponentTransformer = mock(ComponentTransformer.class);
        when(mockComponentTransformer.transform(projectImportMapper, externalNodeAssociation)).thenReturn(transformedExternalNodeAssociation);

        final ProjectImportPersister mockProjectImportPersister = mock(ProjectImportPersister.class);
        when(mockProjectImportPersister.createAssociation(transformedExternalNodeAssociation)).thenReturn(false);

        final BackupSystemInformation mockBackupSystemInformation = mock(BackupSystemInformation.class);
        when(mockBackupSystemInformation.getIssueKeyForId("34")).thenReturn("TST-1");

        final ProjectImportResultsImpl projectImportResults = new ProjectImportResultsImpl(0, 0, 0, 0, new MockI18nBean());
        ComponentPersisterHandler componentPersisterHandler = new ComponentPersisterHandler(mockProjectImportPersister, projectImportMapper, projectImportResults, mockBackupSystemInformation, new ExecutorForTests()) {
            NodeAssociationParser getNodeAssociationParser() {
                return mockNodeAssociationParser;
            }

            ComponentTransformer getComponentTransformer() {
                return mockComponentTransformer;
            }
        };

        componentPersisterHandler.handleEntity(NODE_ASSOCIATION_ENTITY_NAME, null);
        componentPersisterHandler.handleEntity("NOTComponent", null);

        assertThat(projectImportResults.getErrors(), hasSize(1));
        assertTrue(projectImportResults.getErrors().contains("There was a problem saving component 'Component 1' for issue 'TST-1'."));
    }

    @Test
    public void testHandleErrorNullIssueId() throws Exception {
        projectImportMapper.getComponentMapper().registerOldValue("12", "Component 1");
        final ExternalNodeAssociation externalNodeAssociation = new ExternalNodeAssociation("34", "", "12", "", COMPONENT_TYPE);

        final ExternalNodeAssociation transformedExternalNodeAssociation = new ExternalNodeAssociation(null, "", "12", "", COMPONENT_TYPE);

        final NodeAssociationParser mockNodeAssociationParser = mock(NodeAssociationParser.class);
        when(mockNodeAssociationParser.parse(null)).thenReturn(externalNodeAssociation);

        final ComponentTransformer mockComponentTransformer = mock(ComponentTransformer.class);
        when(mockComponentTransformer.transform(projectImportMapper, externalNodeAssociation)).thenReturn(transformedExternalNodeAssociation);

        final ProjectImportPersister mockProjectImportPersister = mock(ProjectImportPersister.class);

        final BackupSystemInformation mockBackupSystemInformation = mock(BackupSystemInformation.class);
        when(mockBackupSystemInformation.getIssueKeyForId("34")).thenReturn("TST-1");

        final ProjectImportResultsImpl projectImportResults = new ProjectImportResultsImpl(0, 0, 0, 0, new MockI18nBean());
        ComponentPersisterHandler componentPersisterHandler = new ComponentPersisterHandler(mockProjectImportPersister, projectImportMapper, projectImportResults, mockBackupSystemInformation, null) {
            NodeAssociationParser getNodeAssociationParser() {
                return mockNodeAssociationParser;
            }

            ComponentTransformer getComponentTransformer() {
                return mockComponentTransformer;
            }
        };

        componentPersisterHandler.handleEntity(NODE_ASSOCIATION_ENTITY_NAME, null);
        componentPersisterHandler.handleEntity("NOTComponent", null);

        assertThat(projectImportResults.getErrors(), hasSize(0));
    }
}
