package com.atlassian.jira.web.action.admin.user;

import com.atlassian.crowd.exception.InvalidCredentialException;
import com.atlassian.crowd.exception.OperationNotPermittedException;
import com.atlassian.crowd.exception.UserNotFoundException;
import com.atlassian.jira.exception.PermissionException;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.util.UserUtil;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.SimpleErrorCollection;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Answers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import javax.servlet.http.HttpServletRequest;

import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

public class TestPasswordChangeService {
    private ApplicationUser expectedUser = new MockApplicationUser("userName");
    private static final String EXPECTED_NEW_PASSWORD = "expectedNewPassword";
    @Rule
    public MockitoRule mocks = MockitoJUnit.rule();

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Mock
    private UserUtil userUtil;

    @Mock
    private HttpServletRequest httpServletRequest;

    @Mock
    private I18nHelper i18nHelper;

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private JiraAuthenticationContext jiraAuthenticationContext;

    private ErrorCollection errorCollection;

    private PasswordChangeService passwordChangeService;

    @Before
    public void setUp() throws Exception {
        errorCollection = new SimpleErrorCollection();
        when(jiraAuthenticationContext.getI18nHelper()).thenReturn(i18nHelper);

        passwordChangeService = new PasswordChangeService(userUtil, jiraAuthenticationContext);
    }

    @Test
    public void testNullUserInput() {
        expectedException.expect(IllegalArgumentException.class);

        passwordChangeService.setPassword(errorCollection, null, "newpassword");
    }

    @Test
    public void testSetPasswordOK()
            throws PermissionException, OperationNotPermittedException, UserNotFoundException, InvalidCredentialException {
        passwordChangeService.setPassword(errorCollection, expectedUser, EXPECTED_NEW_PASSWORD);

        assertThat(errorCollection.getErrorMessages(), Matchers.emptyIterable());
        Mockito.verify(userUtil).changePassword(expectedUser, EXPECTED_NEW_PASSWORD);
    }

    @Test
    public void testSetPasswordNotOK()
            throws PermissionException, OperationNotPermittedException, UserNotFoundException, InvalidCredentialException {
        final String EXCEPTION_MSG = "Shite has occurred!";
        Mockito.doThrow(new RuntimeException(EXCEPTION_MSG)).when(userUtil).changePassword(expectedUser, EXPECTED_NEW_PASSWORD);
        when(i18nHelper.getText("admin.setpassword.osuser.immutable.exception", EXCEPTION_MSG)).thenReturn(EXCEPTION_MSG);

        passwordChangeService.setPassword(errorCollection, expectedUser, EXPECTED_NEW_PASSWORD);

        assertThat(errorCollection.getErrorMessages(), Matchers.contains(
                Matchers.containsString(EXCEPTION_MSG)
        ));
        Mockito.verify(userUtil).changePassword(expectedUser, EXPECTED_NEW_PASSWORD);
    }
}
