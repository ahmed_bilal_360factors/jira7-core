package com.atlassian.jira.event.mau;

import com.atlassian.analytics.api.events.MauEvent;
import com.atlassian.application.api.ApplicationKey;
import com.atlassian.cache.memory.MemoryCacheManager;
import com.atlassian.core.util.Clock;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.fugue.Option;
import com.atlassian.jira.application.MockApplication;
import com.atlassian.jira.config.properties.JiraProperties;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.type.ProjectTypeKey;
import com.atlassian.jira.project.type.ProjectTypeManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.util.concurrent.LazyReference;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class MauEventServiceImplTest {

    private MauEventServiceImpl mauEventService;

    @Mock
    private ProjectTypeManager projectTypeManager;
    @Mock
    private JiraAuthenticationContext jiraAuthenticationContext;
    @Mock
    private EventPublisher eventPublisher;
    @Mock
    private Clock clock;
    @Mock
    private JiraProperties jiraProperties;
    @Mock
    private PluginAccessor pluginAccessor;

    private final Project mockProject = new MockProject(10000L, "HSP", new ProjectTypeKey("software"));

    @Before
    public void setUp() throws Exception {
        mauEventService = new MauEventServiceImpl(projectTypeManager, jiraAuthenticationContext,
                new MemoryCacheManager(), eventPublisher, clock, jiraProperties, pluginAccessor);
    }

    @Test
    public void anonymousUsersShouldNotTriggerMauEvents() {
        mockAnonymous();
        mockFuncTestPlugin(true);
        mauEventService.triggerEvent();

        verify(eventPublisher, never()).publish(any());
    }

    @Test
    public void rateLimitsEventsToFiveMinutesInProduction() {
        mockLoggedInUser();
        mockFuncTestPlugin(true);

        mockCurrentTime(TimeUnit.MINUTES.toMillis(10));  //current time is 10 minutes from 0
        mauEventService.triggerEvent();

        mockCurrentTime(TimeUnit.MINUTES.toMillis(11));  //current time is 11 minutes from 0
        mauEventService.triggerEvent(); //this will not trigger an event. It's too soon to the previous call.

        mockCurrentTime(TimeUnit.MINUTES.toMillis(16));
        mauEventService.triggerEvent();  //this will trigger an event again.

        verify(eventPublisher, times(2)).publish(any());
    }

    @Test
    public void settingApplicationForCurrentThreadAddsItToEvent() {
        mockLoggedInUser();
        mockFuncTestPlugin(true);
        mockCurrentTime(TimeUnit.MINUTES.toMillis(10));

        mauEventService.setApplicationForThread(MauApplicationKey.family());
        mauEventService.triggerEvent();

        verify(eventPublisher, times(1)).publish(eq(new MauEvent.Builder().application("family").build("fred@example.com")));
    }

    @Test
    public void settingApplicationForCurrentThreadViaProjectAddsItToEvent() {
        mockLoggedInUser();
        mockFuncTestPlugin(true);
        mockCurrentTime(TimeUnit.MINUTES.toMillis(10));
        mockProjectTypeBelongsToSoftwareApplication();

        mauEventService.setApplicationForThreadBasedOnProject(mockProject);
        mauEventService.triggerEvent();

        verify(eventPublisher, times(1)).publish(eq(new MauEvent.Builder().application("jira-software").build("fred@example.com")));
    }

    @Test
    public void settingApplicationForCurrentThreadViaProjectIsNullSafe() {
        mockLoggedInUser();
        mockFuncTestPlugin(true);
        mockCurrentTime(TimeUnit.MINUTES.toMillis(10));

        mauEventService.setApplicationForThreadBasedOnProject(null);
        mauEventService.triggerEvent();

        verify(eventPublisher, times(1)).publish(eq(new MauEvent.Builder().application("unknown").build("fred@example.com")));
    }

    private void mockProjectTypeBelongsToSoftwareApplication() {
        when(projectTypeManager.getApplicationWithType(new ProjectTypeKey("software")))
                .thenReturn(Option.option(new MockApplication(ApplicationKey.valueOf("jira-software"))));
    }

    private void mockLoggedInUser() {
        when(jiraAuthenticationContext.getLoggedInUser()).thenReturn(new MockApplicationUser("fred", "Freddy", "fred@example.com"));
    }

    private void mockDevMode(boolean isEnabled) {
        when(jiraProperties.isDevMode()).thenReturn(isEnabled);
    }

    private void mockFuncTestPlugin(boolean isEnabled) {
        when(pluginAccessor.isPluginEnabled("com.atlassian.jira.dev.func-test-plugin")).thenReturn(isEnabled);
    }

    private void mockAnonymous() {
        when(jiraAuthenticationContext.getLoggedInUser()).thenReturn(null);
    }

    private void mockCurrentTime(final long timestamp) {
        when(clock.getCurrentDate()).thenReturn(new Date(timestamp));
    }
}