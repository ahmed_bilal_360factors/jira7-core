package com.atlassian.jira.issue.fields.csv;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.MockIssueFactory;
import com.atlassian.jira.issue.export.FieldExportParts;
import com.atlassian.jira.issue.fields.SecurityLevelSystemField;
import com.atlassian.jira.issue.security.IssueSecurityLevel;
import com.atlassian.jira.issue.security.IssueSecurityLevelManager;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.mock.MockProjectManager;
import com.atlassian.jira.mock.component.MockComponentWorker;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.I18nHelper;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.when;

/**
 * @since v6.5
 */
public class SecurityLevelSystemFieldCsvExportTest {
    @Rule
    public final RuleChain mockito = MockitoMocksInContainer.forTest(this);

    private MockProjectManager mockProjectManager = new MockProjectManager();

    @Mock
    private Issue issue;
    @Mock
    private IssueSecurityLevel issueSecurityLevel;
    @Mock
    private IssueSecurityLevelManager issueSecurityLevelManager;
    @Mock
    private JiraAuthenticationContext authenticationContext;
    @Mock
    private I18nHelper i18nHelper;

    @InjectMocks
    private SecurityLevelSystemField field;

    @Before
    public void setUp() {
        ComponentAccessor.initialiseWorker(new MockComponentWorker());

        MockIssueFactory.setProjectManager(mockProjectManager);
        mockProjectManager.addProject(new MockProject(1, "COW"));
        mockProjectManager.addProject(new MockProject(2, "DOG"));

        when(authenticationContext.getI18nHelper()).thenReturn(i18nHelper);
    }

    @Test
    public void testCsvRepresentationForExpectedTypeAndValues() {
        final String SECURITY_LEVEL = "security level";

        when(issue.getSecurityLevelId()).thenReturn(1L);
        when(issueSecurityLevelManager.getSecurityLevel(anyLong())).thenReturn(issueSecurityLevel);
        when(issueSecurityLevel.getName()).thenReturn(SECURITY_LEVEL);

        final FieldExportParts representation = field.getRepresentationFromIssue(issue);
        assertThat(representation.getParts(), hasSize(1));
        assertThat(representation.getParts().get(0).getValues()::iterator, contains(SECURITY_LEVEL));
    }

    @Test
    public void testCsvRepresentationWhenThereIsNoSecurityLevelSet() {
        when(issue.getSecurityLevelId()).thenReturn(null);
        final FieldExportParts representation = field.getRepresentationFromIssue(issue);
        assertThat(representation.getParts(), hasSize(1));
        assertThat(representation.getParts().get(0).getValues()::iterator, contains(""));
    }
}
