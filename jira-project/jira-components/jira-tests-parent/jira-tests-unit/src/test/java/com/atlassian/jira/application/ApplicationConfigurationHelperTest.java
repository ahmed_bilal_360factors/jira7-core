package com.atlassian.jira.application;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.crowd.embedded.impl.ImmutableGroup;
import com.atlassian.crowd.exception.OperationNotPermittedException;
import com.atlassian.crowd.exception.embedded.InvalidGroupException;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.fugue.Option;
import com.atlassian.jira.config.group.GroupConfigurationIdentifier;
import com.atlassian.jira.junit.rules.Log4jLogger;
import com.atlassian.jira.license.JiraLicenseManager;
import com.atlassian.jira.license.LicenseChangedEvent;
import com.atlassian.jira.license.LicenseDetails;
import com.atlassian.jira.license.MockLicenseDetails;
import com.atlassian.jira.license.MockLicensedApplications;
import com.atlassian.jira.message.MockMessageUtilFactory;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.permission.GlobalPermissionType;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.groups.MockGroupManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.MockGroup;
import com.atlassian.jira.user.util.MockUserManager;
import com.atlassian.jira.util.johnson.JohnsonProvider;
import com.atlassian.johnson.JohnsonEventContainer;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Sets;
import org.hamcrest.MatcherAssert;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.annotation.Nullable;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static com.atlassian.jira.permission.GlobalPermissionKey.ADMINISTER;
import static java.lang.String.format;
import static org.hamcrest.Matchers.any;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @since 7.0
 */
@RunWith(MockitoJUnitRunner.class)
public class ApplicationConfigurationHelperTest {
    private final ApplicationKey KEY_REF_APP = ApplicationKey.valueOf("jira-ref");
    private final ApplicationUser ADMIN = new MockApplicationUser("admin");
    private final ApplicationUser NON_ADMIN = new MockApplicationUser("non-admin");
    private MockApplicationRoleManager applicationRoleManager;
    private MockUserManager userManager;
    private MockApplicationRoleStore applicationRoleStore;
    private MockApplicationManager applicationManager;
    private MockGroupManager groupManager;

    @Mock
    private GlobalPermissionManager globalPermissionManager;
    @Mock
    private GroupConfigurationIdentifier latentGroupConfIdentifier;
    @Mock
    private EventPublisher eventPublisher;


    @Mock
    private JiraAuthenticationContext authenticationContext;
    @Mock
    private JiraLicenseManager licenseManager;
    @Mock
    private JohnsonProvider johnsonProvider;
    @Mock
    private JohnsonEventContainer johnsonEventContainer;
    @Rule
    public ExpectedException exception = ExpectedException.none();
    @Rule
    public final Log4jLogger log4jLogger = new Log4jLogger();

    private ApplicationConfigurationHelper appConfigHelper;

    @Before
    public void setUp() throws Exception {
        applicationManager = new MockApplicationManager();
        applicationRoleManager = new MockApplicationRoleManager();
        applicationRoleStore = new MockApplicationRoleStore();
        userManager = new MockUserManager();
        groupManager = new MockGroupManager();
        when(licenseManager.getLicenses()).thenReturn(Sets.newHashSet());
        when(authenticationContext.getLoggedInUser()).thenReturn(null);
        appConfigHelper = new ApplicationConfigurationHelper(applicationManager, groupManager,
                applicationRoleManager, globalPermissionManager, userManager,
                applicationRoleStore, new MockMessageUtilFactory(), latentGroupConfIdentifier,
                eventPublisher, authenticationContext, licenseManager, johnsonProvider);
        setupMockData();
    }

    private void setupMockData() {
        //Install application
        applicationManager.addApplication(KEY_REF_APP);
        setupDefaultGlobalPermissions();
        //Set Admin as default logged-in user
        when(authenticationContext.getLoggedInUser()).thenReturn(ADMIN);
        //Give Admin admin permission
        when(globalPermissionManager.hasPermission(ADMINISTER, ADMIN)).thenReturn(true);
        when(globalPermissionManager.hasPermission(ADMINISTER, NON_ADMIN)).thenReturn(false);
        when(johnsonEventContainer.getEvents()).thenReturn(ImmutableList.of());
        when(johnsonProvider.getContainer()).thenReturn(johnsonEventContainer);
    }

    private void setupDefaultGlobalPermissions() {
        for (GlobalPermissionKey gpKey : GlobalPermissionKey.DEFAULT_APP_GLOBAL_PERMISSIONS) {
            when(globalPermissionManager.getGlobalPermission(gpKey))
                    .thenReturn(Option.some(new GlobalPermissionType(gpKey.getKey(), null, null, false)));
        }
    }

    /**
     * This is a fresh installation. This represents the happy path. Admin installs an application for the first time
     * and have a license. The application default group will be created and the admin would get added to that default
     * group. In effect the current user (admin) has access to the newly installed application.
     */
    @Test
    public void testFreshInstallation() {
        assertNasNoValidationErrors(KEY_REF_APP, ADMIN);

        installLicense(KEY_REF_APP);
        appConfigHelper.configureLicense(newLicenseChangedEvent(KEY_REF_APP));

        assertConfiguredDefaultGroupWithName(KEY_REF_APP);
        assertUserAddedToDefaultGroup(ADMIN, KEY_REF_APP);
        assertConfigurationEventForApplications(KEY_REF_APP);
    }

    /**
     * The admin installed an application (ref app). Changed the default group from jira-ref-group to jj-group and then
     * uninstalled ref app, choosing to keep the config. When admin installs the ref app again and add the license, the
     * config should not be changed and old config would be restored.
     */
    @Test
    public void testApplicationReinstalledWithExistingConfig() {
        updateRoleWithOnlyDefaultGroup(KEY_REF_APP, "jj-group");

        appConfigHelper.configureLicense(newLicenseChangedEvent(KEY_REF_APP));

        assertDefaultGroupNotConfigured(KEY_REF_APP);
        assertConfiguredDefaultGroupWithName(KEY_REF_APP, "jj-group");
    }

    /**
     * Admin has configured external user directory and it is read only.
     */
    @Test
    public void testNoWritableDirectoryToCreateDefaultGroup() {
        userManager.setGroupWritableDirectory(false);

        assertHasValidationError(KEY_REF_APP, "application.installation.configuration.use.to.app"
                + "{[url.default, jira-ref-name]}", ADMIN);
    }

    /**
     * An application default group already exist, users could possibly already be added to that group. It could be used
     * as nested group. Group could be used in permission schemes etc. Admin would have to manually configure the
     * default group of tha application.
     */
    @Test
    public void testThatIfDefaultGroupExistWarnAdmin() throws OperationNotPermittedException, InvalidGroupException {
        groupManager.createGroup(defaultGroupName(KEY_REF_APP));
        useAllTheNextAvailableGroupNames();
        assertHasValidationError(KEY_REF_APP, "application.installation.configuration.group.exist"
                + "{[jira-ref-group, jira-ref-name, <a href=\"baseurl.default\">title.default{[]}</a>]}", ADMIN);
    }

    /**
     * When valid default group configuration exist for license, do not perform configuration. Existing configuration
     * will be restored when license gets added.
     */
    @Test
    public void testConfigurationDoesNotGetPerformedWhenConfigurationExists()
            throws OperationNotPermittedException, InvalidGroupException {
        final Group MY_GROUP = groupManager.createGroup("blah");
        ApplicationRole role = new MockApplicationRole(KEY_REF_APP);
        role = role.withGroups(Sets.newHashSet(MY_GROUP), Sets.newHashSet(MY_GROUP));
        applicationRoleStore.save(role);

        assertNasNoValidationErrors(KEY_REF_APP, ADMIN);
        appConfigHelper.configureLicense(newLicenseChangedEvent(KEY_REF_APP));

        assertThat(groupManager.getGroup(defaultGroupName(KEY_REF_APP)), nullValue());
        verify(eventPublisher, never()).publish(any(ApplicationConfigurationEvent.class));
    }

    /**
     * When invalid configuration exist for license, perform configuration. Existing configuration will not be returned
     * as group is no longer active.
     */
    @Test
    public void testConfigurationGetsPerformedWhenInvalidConfigurationExists()
            throws OperationNotPermittedException, InvalidGroupException {
        final Group OLD_GROUP = new ImmutableGroup("blah");
        //Add old config
        applicationRoleStore.save(KEY_REF_APP, OLD_GROUP);
        applicationRoleManager.setRole(new MockApplicationRole(KEY_REF_APP).groups(OLD_GROUP));

        assertNasNoValidationErrors(KEY_REF_APP, ADMIN);
        appConfigHelper.configureLicense(newLicenseChangedEvent(KEY_REF_APP));

        final Group REF_APP_DEFAULT_GROUP = new ImmutableGroup(defaultGroupName(KEY_REF_APP));
        assertThat(groupManager.getGroup(defaultGroupName(KEY_REF_APP)), is(REF_APP_DEFAULT_GROUP));
        assertThat(applicationRoleManager.getRole(KEY_REF_APP).get().getGroups(),
                containsInAnyOrder(OLD_GROUP, REF_APP_DEFAULT_GROUP));
        assertThat(groupManager.isUserInGroup(ADMIN, REF_APP_DEFAULT_GROUP), is(true));
        assertThat(groupManager.isUserInGroup(ADMIN, OLD_GROUP), is(false));
        assertConfigurationEventForApplications(KEY_REF_APP);
    }

    /**
     * Test that when an ELA license gets inserted that all the applications in that single license key gets
     * configured.
     */
    @Test
    public void testThatOtherAppInElaLicenseGetsConfigured() {
        final ApplicationKey OTHER_APP_KEY = ApplicationKey.valueOf("jira-other");
        final ApplicationKey HR_APP_KEY = ApplicationKey.valueOf("jira-tt");
        final String OTHER_APP_GROUP = OTHER_APP_KEY + "-users";
        final String HR_APP_GROUP = HR_APP_KEY + "-users";

        installLicense(KEY_REF_APP);
        installLicense(OTHER_APP_KEY);
        installLicense(HR_APP_KEY);

        mockLicenseDetails(KEY_REF_APP, OTHER_APP_KEY);
        mockLicenseDetails(HR_APP_KEY);

        appConfigHelper.configureLicense(newLicenseChangedEvent(KEY_REF_APP, OTHER_APP_KEY));

        assertConfiguredDefaultGroupWithName(KEY_REF_APP);
        assertConfiguredDefaultGroupWithName(OTHER_APP_KEY, OTHER_APP_GROUP);
        assertUserAddedToDefaultGroup(ADMIN, KEY_REF_APP);
        assertTrue(groupManager.isUserInGroup(ADMIN, OTHER_APP_GROUP));
        assertFalse(groupManager.isUserInGroup(ADMIN, HR_APP_GROUP));
        assertDefaultGroupNotConfigured(HR_APP_KEY, HR_APP_GROUP);
        assertConfigurationEventForApplications(KEY_REF_APP, OTHER_APP_KEY);
    }

    @Test
    public void testThatAdminDoesNotGetConfiguredDuringImport() {
        installLicense(KEY_REF_APP);
        when(authenticationContext.getLoggedInUser()).thenReturn(ADMIN);

        appConfigHelper.configureApplicationsForImport(mockLicenseDetails(KEY_REF_APP));
        assertUserNotAddedToDefaultGroup(ADMIN, KEY_REF_APP);
    }

    @Test
    public void testAppConfEventFiredWhenLicenseRemoved() {
        installLicense(KEY_REF_APP);
        appConfigHelper.configureLicense(new LicenseChangedEvent(Option.option(mockLicenseDetails(KEY_REF_APP)),
                Option.none()));
        assertConfigurationEventForApplications(KEY_REF_APP);
    }

    @Test
    public void testAppConfEventFiredWhenConfigReactivated() {
        final Group group = groupManager.createGroup("jira-tt");
        ApplicationRole role = new MockApplicationRole(KEY_REF_APP);
        role = role.withGroups(Sets.newHashSet(group), Sets.newHashSet(group));
        applicationRoleStore.save(role);
        installLicense(KEY_REF_APP);
        appConfigHelper.configureLicense(newLicenseChangedEvent(KEY_REF_APP));
        assertConfigurationEventForApplications(KEY_REF_APP);
        assertFalse(groupManager.groupExists(defaultGroupName(KEY_REF_APP)));
    }

    @Test
    public void testAppConfEventFiredWhenNewConfigAdded() {
        installLicense(KEY_REF_APP);
        appConfigHelper.configureLicense(newLicenseChangedEvent(KEY_REF_APP));
        assertConfigurationEventForApplications(KEY_REF_APP);
    }

    @Test
    public void testThatConfigurationDoesNotGetPerformedWhenNonWritableDirectory() {
        installLicense(KEY_REF_APP);
        userManager.setGroupWritableDirectory(false);

        assertHasValidationError(KEY_REF_APP, "application.installation.configuration.use.to.app"
                + "{[url.default, jira-ref-name]}", ADMIN);

        appConfigHelper.configureLicense(newLicenseChangedEvent(KEY_REF_APP));
        assertNoDefaultGroupConfigured(KEY_REF_APP);
    }

    @Test
    public void testThatWhenDefaultGroupIsAlreadyUsedNextAvailableGroupGetsConfigured() {
        installLicense(KEY_REF_APP);
        groupManager.createGroup(defaultGroupName(KEY_REF_APP));

        appConfigHelper.configureLicense(newLicenseChangedEvent(KEY_REF_APP));

        assertConfiguredDefaultGroupWithName(KEY_REF_APP, defaultGroupName(KEY_REF_APP) + "-1");
    }

    @Test
    public void testThatWhenDefaultGroupIsAlreadyUsedNextAvailableGroupGetsConfigured_with_date() {
        installLicense(KEY_REF_APP);
        final String defaultGroupName = defaultGroupName(KEY_REF_APP);
        groupManager.createGroup(defaultGroupName);
        for (int i = 1; i <= 5; i++) {
            groupManager.createGroup(defaultGroupName + "-" + i);
        }

        appConfigHelper.configureLicense(newLicenseChangedEvent(KEY_REF_APP));
        final String dateString = new DateTime().toString(DateTimeFormat.forPattern("yyyyMMdd"));
        assertConfiguredDefaultGroupWithName(KEY_REF_APP, defaultGroupName + "-auto-" + dateString + "-1");
    }

    @Test
    public void testThatWhenAllGroupNamesAreUsedNoConfigurationGetsPerformed() {
        installLicense(KEY_REF_APP);
        useAllTheNextAvailableGroupNames();

        appConfigHelper.configureLicense(newLicenseChangedEvent(KEY_REF_APP));
        assertNoDefaultGroupConfigured(KEY_REF_APP);

        assertThat(log4jLogger.getMessage(),
                containsString(
                        format("The default group name [%s] for [%2$s] already exist. "
                                        + "Unable to provide an unused group name for [%2$s]",
                                defaultGroupName(KEY_REF_APP)
                                , "jira-ref-name")));
    }

    @Test
    public void testThatWarningIsLoggedWhenUnableToCreateGroup() {
        installLicense(KEY_REF_APP);
        userManager.setGroupWritableDirectory(false);

        appConfigHelper.configureLicense(newLicenseChangedEvent(KEY_REF_APP));

        assertThat(log4jLogger.getMessage(),
                containsString("There are no writable directories to create the default group for the new license."));
    }

    @Test
    public void testNextGroupGetsUsedIfDanglingGroupExists() {
        installLicense(KEY_REF_APP);
        final String defaultGroupName = defaultGroupName(KEY_REF_APP);
        groupManager.createGroup(defaultGroupName);
        for (int i = 1; i <= 5; i++) {
            groupManager.createGroup(defaultGroupName + "-" + i);
        }

        appConfigHelper.configureLicense(newLicenseChangedEvent(KEY_REF_APP));
        final String dateString = new DateTime().toString(DateTimeFormat.forPattern("yyyyMMdd"));
        assertConfiguredDefaultGroupWithName(KEY_REF_APP, defaultGroupName + "-auto-" + dateString + "-1");
    }

    @Test
    public void testThatWeDoNotReconfigureExistingGroupDuringSynch() {
        final String defaultGroupName = defaultGroupName(KEY_REF_APP);
        updateRoleWithOnlyDefaultGroupName(KEY_REF_APP, defaultGroupName);

        assertFalse(groupManager.groupExists(defaultGroupName));

        final InvalidGroupException invalidGroupException = new InvalidGroupException(new ImmutableGroup(defaultGroupName), "Group already exists");
        groupManager.throwExceptionOnCreateGroup(defaultGroupName, invalidGroupException);

        installLicense(KEY_REF_APP);
        appConfigHelper.configureLicense(newLicenseChangedEvent(KEY_REF_APP));

        assertThat(log4jLogger.getMessage(),
                containsString(
                        format("Application already %s already configured with group %s", KEY_REF_APP, defaultGroupName)
                ));
    }

    private void useAllTheNextAvailableGroupNames() {
        final String defaultGroupName = defaultGroupName(KEY_REF_APP);
        groupManager.createGroup(defaultGroupName);
        for (int i = 1; i <= 5; i++) {
            groupManager.createGroup(defaultGroupName + "-" + i);
        }
        final String dateString = new DateTime().toString(DateTimeFormat.forPattern("yyyyMMdd"));
        final String nextGroupName = defaultGroupName + "-auto-" + dateString;
        for (int i = 1; i <= 999; i++) {
            groupManager.createGroup(nextGroupName + "-" + i);
        }
    }

    /**
     * Create a new {@link LicenseChangedEvent} for the specified application licenses.
     *
     * @param keys that identifies the application license.
     * @return an license change event for the license with the specified keys.
     */
    private LicenseChangedEvent newLicenseChangedEvent(final ApplicationKey... keys) {
        return new LicenseChangedEvent(Option.none(), Option.option(mockLicenseDetails(keys)));
    }

    /**
     * Assert that the user has been added to the default group for the specified application.
     *
     * @param appUser current user.
     * @param key     identifying the Application key.
     */
    private void assertUserAddedToDefaultGroup(final ApplicationUser appUser, final ApplicationKey key) {
        assertTrue(groupManager.isUserInGroup(appUser, defaultGroupName(key)));
    }

    /**
     * Assert that the user has not been added to the default group for the specified application.
     *
     * @param appUser current user.
     * @param key     identifying the Application key.
     */
    private void assertUserNotAddedToDefaultGroup(final ApplicationUser appUser, final ApplicationKey key) {
        assertFalse(groupManager.isUserInGroup(appUser, defaultGroupName(key)));
    }

    /**
     * Assert that there is no default group configured for the specified application.
     *
     * @param key identifying the Application key.
     */
    private void assertNoDefaultGroupConfigured(final ApplicationKey key) {
        assertTrue("Expected no default groups.", applicationRoleStore.get(key).getDefaultGroups().isEmpty() &&
                applicationRoleManager.getRole(key).get().getDefaultGroups().isEmpty());
    }

    /**
     * Assert that the default group configured for the specified application.
     *
     * @param key identifying the Application key.
     */
    private void assertConfiguredDefaultGroupWithName(final ApplicationKey key) {
        assertConfiguredDefaultGroupWithName(key, defaultGroupName(key));
        assertDefaultGroupsAddedToDefaultGlobalPermissions(defaultGroupName(key));
    }

    /**
     * Assert that default group has been configured for the default global permissions.
     *
     * @param defaultGroupName the default group that should be configured with the default global permissions.
     */
    private void assertDefaultGroupsAddedToDefaultGlobalPermissions(final String defaultGroupName) {
        for (GlobalPermissionKey gpKey : GlobalPermissionKey.DEFAULT_APP_GLOBAL_PERMISSIONS) {
            final GlobalPermissionType globalPermissionType = globalPermissionManager.getGlobalPermission(gpKey).get();
            MatcherAssert.assertThat(gpKey.getKey(), is(globalPermissionType.getKey()));
            verify(globalPermissionManager).addPermission(globalPermissionType, defaultGroupName);
        }
    }

    /**
     * Assert that an {@link ApplicationConfigurationEvent} has been fired for the specified applications.
     */
    private void assertConfigurationEventForApplications(final ApplicationKey... keys) {
        ArgumentCaptor<ApplicationConfigurationEvent> eventCaptor =
                ArgumentCaptor.forClass(ApplicationConfigurationEvent.class);
        verify(eventPublisher).publish(eventCaptor.capture());
        eventCaptor.getValue().getApplicationsConfigured().containsAll(Sets.newHashSet(keys));
    }

    /**
     * Assert that the specified group has been configured as the default group for the specified application.
     *
     * @param key       identifying the Application key.
     * @param groupName the expected default group name.
     */
    private void assertConfiguredDefaultGroupWithName(final ApplicationKey key, final String groupName) {
        final Set<Group> defaultGroups = applicationRoleManager.getDefaultGroups(key);
        final boolean hasDefaultGroup = defaultGroups.stream()
                .anyMatch(grp -> grp.getName().equals(groupName));
        final StringBuilder stringBuilder = new StringBuilder();
        defaultGroups.forEach(group -> stringBuilder.append("[").append(group.getName()).append("] "));
        assertTrue("Expected default group [" + groupName + "] has [" + stringBuilder.toString() + "]", hasDefaultGroup);
    }

    /**
     * Assert that the default group has not been configured for the specified application.
     *
     * @param key identifying the Application key.
     */
    private void assertDefaultGroupNotConfigured(final ApplicationKey key) {
        final String defaultGroupName = defaultGroupName(key);
        assertDefaultGroupNotConfigured(key, defaultGroupName);
    }

    /**
     * Assert that the default group has not been configured for the specified application.
     *
     * @param key identifying the Application key.
     */
    private void assertDefaultGroupNotConfigured(final ApplicationKey key, final String defaultGroupName) {
        final boolean hasDefaultGroup = applicationRoleManager.getDefaultGroups(key).stream()
                .anyMatch(grp -> grp.getName().equals(defaultGroupName));
        assertFalse("Default group not expected" + defaultGroupName, hasDefaultGroup);
    }

    /**
     * Assert that the application can be configured. Should have no validation errors.
     *
     * @param key identifying the Application key.
     */
    private void assertNasNoValidationErrors(final ApplicationKey key, @Nullable final ApplicationUser user) {
        final Optional<String> validationResult = appConfigHelper.validateApplicationForConfiguration(key, user);
        final String validationMessage = validationResult.isPresent() ? "Expected not error found: " +
                validationResult.get() : "None";
        assertFalse(validationMessage, validationResult.isPresent());
    }

    /**
     * Assert that the application can be configured. Should have no validation errors.
     *
     * @param key identifying the Application key.
     */
    private void assertHasValidationError(final ApplicationKey key, final String error, final ApplicationUser user) {
        final Optional<String> validationResult = appConfigHelper.validateApplicationForConfiguration(key, user);
        assertEquals(error, validationResult.get());
    }

    /**
     * Update the role with only the specified default group name. This removes all previous config.
     *
     * @param key              identifier user to identify the application role.
     * @param defaultGroupName the name of the default group to be configured for the application role.
     */
    private void updateRoleWithOnlyDefaultGroup(final ApplicationKey key, final String defaultGroupName) {
        groupManager.createGroup(defaultGroupName);
        updateRoleWithOnlyDefaultGroupName(key, defaultGroupName);
    }

    private void updateRoleWithOnlyDefaultGroupName(final ApplicationKey key, final String defaultGroupName) {
        ApplicationRole role = new MockApplicationRole(key);
        final HashSet<Group> mockGroups = Sets.newHashSet(new MockGroup(defaultGroupName));
        role = role.withGroups(mockGroups, mockGroups);
        applicationRoleManager.setRole(role);
        applicationRoleStore.save(role);
    }

    /**
     * Install the license key for the specified application.
     *
     * @param key identifying the Application key.
     */
    private void installLicense(final ApplicationKey key) {
        MockApplicationRole role = new MockApplicationRole(key);
        applicationRoleManager.setRole(role);
        mockLicenseDetails(key);
    }

    /**
     * Create a mock {@link LicenseDetails} for the specified applications.
     */
    private LicenseDetails mockLicenseDetails(final ApplicationKey... keys) {
        final MockLicenseDetails licenseDetails = new MockLicenseDetails();
        final MockLicensedApplications licensedApplications = new MockLicensedApplications(keys);
        licenseDetails.setLicensedApplications(licensedApplications);
        return licenseDetails;
    }

    /**
     * Generate the default group name for the specified application.
     */
    private String defaultGroupName(final ApplicationKey key) {
        return key + "-group";
    }
}