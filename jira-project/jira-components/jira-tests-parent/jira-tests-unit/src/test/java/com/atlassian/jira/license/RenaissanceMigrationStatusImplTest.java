package com.atlassian.jira.license;

import com.atlassian.jira.mock.MockApplicationProperties;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * @since 7.0
 */
public class RenaissanceMigrationStatusImplTest {
    private static final String KEY = "renaissanceMigrationDone";

    private final MockApplicationProperties properties = new MockApplicationProperties();
    private final RenaissanceMigrationStatusImpl predicate = new RenaissanceMigrationStatusImpl(properties);

    @Test
    public void getDelegates() {
        //given
        properties.setOption(KEY, true);

        //when
        final boolean actual = predicate.hasMigrationRun();

        //then
        assertThat(actual, is(true));
    }

    @Test
    public void markDelegatesEvenWhenTrue() {
        //given
        properties.setOption(KEY, true);

        //when
        final boolean actual = predicate.markDone();

        //then
        assertThat(actual, is(true));
        assertThat(properties.getOption(KEY), is(true));
    }

    @Test
    public void markDelegates() {
        //given
        properties.setOption(KEY, false);

        //when
        final boolean actual = predicate.markDone();

        //then
        assertThat(actual, is(false));
        assertThat(properties.getOption(KEY), is(true));
    }
}