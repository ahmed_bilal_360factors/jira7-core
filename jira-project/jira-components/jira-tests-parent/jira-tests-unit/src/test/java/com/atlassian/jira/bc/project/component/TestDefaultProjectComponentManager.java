package com.atlassian.jira.bc.project.component;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.database.DbConnectionManager;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.mockito.Mock;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

public class TestDefaultProjectComponentManager {
    @Rule
    public RuleChain mockitoMocksInContainer = MockitoMocksInContainer.forTest(this);

    @Mock
    private DbConnectionManager dbConnectionManager;

    @Test
    public void testDeleteAllComponents() {
        final ProjectComponentStore projectComponentStore = mock(ProjectComponentStore.class);
        final EventPublisher eventPublisher = mock(EventPublisher.class);

        final DefaultProjectComponentManager defaultProjectComponentManager = new DefaultProjectComponentManager(projectComponentStore, null, eventPublisher, null, null, dbConnectionManager);

        defaultProjectComponentManager.deleteAllComponents(1L);

        verify(projectComponentStore).deleteAllComponents(1L);
        verifyNoMoreInteractions(eventPublisher);
    }
}
