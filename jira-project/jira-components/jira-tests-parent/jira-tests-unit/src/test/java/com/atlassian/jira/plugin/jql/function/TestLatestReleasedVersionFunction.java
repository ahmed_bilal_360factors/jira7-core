package com.atlassian.jira.plugin.jql.function;

import com.atlassian.jira.JiraDataTypes;
import com.atlassian.jira.jql.operand.QueryLiteral;
import com.atlassian.jira.jql.query.QueryCreationContext;
import com.atlassian.jira.jql.query.QueryCreationContextImpl;
import com.atlassian.jira.jql.resolver.ProjectResolver;
import com.atlassian.jira.mock.plugin.jql.operand.MockJqlFunctionModuleDescriptor;
import com.atlassian.jira.mock.project.MockVersion;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.project.version.VersionManager;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.MessageSet;
import com.atlassian.jira.web.bean.MockI18nBean;
import com.atlassian.query.clause.TerminalClause;
import com.atlassian.query.operand.FunctionOperand;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static com.atlassian.jira.permission.ProjectPermissions.BROWSE_PROJECTS;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @since v4.3
 */
public class TestLatestReleasedVersionFunction {
    @Rule
    public MockitoRule mockito = MockitoJUnit.rule();

    @Mock
    private PermissionManager permissionmanager;
    @Mock
    private ProjectResolver projectResolver;
    @Mock
    private VersionManager versionManager;

    private ApplicationUser theUser;
    private QueryCreationContext queryCreationContext;
    private TerminalClause terminalClause = null;

    private List<Version> releasedVersionList = new ArrayList<Version>();
    private Project project1;

    @Before
    public void setUp() throws Exception {
        theUser = new MockApplicationUser("fred");
        queryCreationContext = new QueryCreationContextImpl(theUser);

        project1 = new MockProject(21l, "c1");

        releasedVersionList.add(new MockVersion(1, "V2", project1, 2l));
        releasedVersionList.add(new MockVersion(2, "V1", project1, 1l));
        releasedVersionList.add(new MockVersion(3, "V5", project1, 3l));
    }

    @Test
    public void testValidateProjectArgument() throws Exception {


        LatestReleasedVersionFunction latestreleasedversionFunction = new LatestReleasedVersionFunction(versionManager, projectResolver, permissionmanager) {
            @Override
            protected I18nHelper getI18n() {
                return new MockI18nBean();
            }
        };
        latestreleasedversionFunction.init(MockJqlFunctionModuleDescriptor.create("latestReleasedVersion", true));
        FunctionOperand function = new FunctionOperand(LatestReleasedVersionFunction.FUNCTION_LATEST_RELEASED_VERSION, Arrays.asList("c1"));
        when(projectResolver.getIdsFromName("c1")).thenReturn(Arrays.asList("21"));
        when(projectResolver.get(21l)).thenReturn(project1);
        when(permissionmanager.hasPermission(BROWSE_PROJECTS, project1, null)).thenReturn(true);

        final MessageSet messageSet = latestreleasedversionFunction.validate(null, function, terminalClause);
        assertTrue(!messageSet.hasAnyMessages());
    }

    @Test
    public void testValidateBadProjectArgument() throws Exception {
        LatestReleasedVersionFunction latestreleasedversionFunction = new LatestReleasedVersionFunction(versionManager, projectResolver, permissionmanager) {
            @Override
            protected I18nHelper getI18n() {
                return new MockI18nBean();
            }
        };
        latestreleasedversionFunction.init(MockJqlFunctionModuleDescriptor.create("latestReleasedVersion", true));
        FunctionOperand function = new FunctionOperand(LatestReleasedVersionFunction.FUNCTION_LATEST_RELEASED_VERSION, Arrays.asList("badproject"));
        when(projectResolver.getIdsFromName("badproject")).thenReturn(Collections.<String>emptyList());

        final MessageSet messageSet = latestreleasedversionFunction.validate(null, function, terminalClause);
        assertTrue(messageSet.hasAnyMessages());
        assertTrue(messageSet.hasAnyErrors());
        assertFalse(messageSet.hasAnyWarnings());
        assertEquals(1, messageSet.getErrorMessages().size());
        assertEquals("Could not resolve the project 'badproject' provided to function 'latestReleasedVersion'.", messageSet.getErrorMessages().iterator().next());
    }

    @Test
    public void testDataType() throws Exception {
        LatestReleasedVersionFunction latestreleasedversionFunction = new LatestReleasedVersionFunction(versionManager, projectResolver, permissionmanager);
        assertEquals(JiraDataTypes.VERSION, latestreleasedversionFunction.getDataType());
    }

    @Test
    public void testGetValues() throws Exception {
        LatestReleasedVersionFunction latestreleasedversionFunction = new LatestReleasedVersionFunction(versionManager, projectResolver, permissionmanager);
        FunctionOperand function = new FunctionOperand(LatestReleasedVersionFunction.FUNCTION_LATEST_RELEASED_VERSION, Arrays.asList("c1"));

        when(projectResolver.getIdsFromName("c1")).thenReturn(Arrays.asList("21"));
        when(projectResolver.get(21l)).thenReturn(project1);
        when(permissionmanager.hasPermission(BROWSE_PROJECTS, project1, theUser)).thenReturn(true);
        when(versionManager.getVersionsReleased(21L, false)).thenReturn(releasedVersionList);

        final List<QueryLiteral> value = latestreleasedversionFunction.getValues(queryCreationContext, function, terminalClause);
        verify(versionManager).getVersionsReleased(21L, false);
        assertNotNull(value);
        assertEquals(1, value.size());
        assertEquals(3, value.get(0).getLongValue().longValue());
    }

    @Test
    public void testGetValuesNoVersions() throws Exception {
        LatestReleasedVersionFunction latestreleasedversionFunction = new LatestReleasedVersionFunction(versionManager, projectResolver, permissionmanager);
        FunctionOperand function = new FunctionOperand(LatestReleasedVersionFunction.FUNCTION_LATEST_RELEASED_VERSION, Arrays.asList("c1"));

        when(projectResolver.getIdsFromName("c1")).thenReturn(Arrays.asList("21"));
        when(projectResolver.get(21l)).thenReturn(project1);
        when(permissionmanager.hasPermission(BROWSE_PROJECTS, project1, theUser)).thenReturn(true);
        when(versionManager.getVersionsReleased(21L, false)).thenReturn(Collections.<Version>emptyList());

        final List<QueryLiteral> value = latestreleasedversionFunction.getValues(queryCreationContext, function, terminalClause);
        verify(versionManager).getVersionsReleased(21L, false);
        assertNotNull(value);
        assertEquals(0, value.size());
    }

}
