package com.atlassian.jira.application;

import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.SettableFuture;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

public class DefaultApplicationsRoleManagerSettableFutureTest {

    private final AtomicBoolean exception = new AtomicBoolean(false);
    private final AtomicBoolean getterFinished = new AtomicBoolean(false);
    private final AtomicBoolean unexpectedExecution = new AtomicBoolean(false);
    private DefaultApplicationRoleManager.DelegatingSettableFutureTask sut;

    @Test(timeout = 5000L)
    public void getIsBlockingAndReleasesOnceImmediateFutureIsSet() throws Exception {
        sut = new DefaultApplicationRoleManager.DelegatingSettableFutureTask(() -> unexpectedExecution.set(true));
        final Thread getterThread = new Thread(this::getterThread);
        getterThread.start();
        Thread.sleep(100L);

        Assert.assertFalse("Getter did not block on get", getterFinished.get());
        sut.setDelegate(Futures.immediateFuture(null));
        getterThread.join();
    }

    @Test(timeout = 5000L)
    public void getBlocksUntilRunnableFinishes() throws Exception {
        final CountDownLatch start = new CountDownLatch(1);
        final CountDownLatch stop = new CountDownLatch(1);

        sut = new DefaultApplicationRoleManager.DelegatingSettableFutureTask(() -> {
            start.countDown();
            awaitLatch(stop);
        });
        final Thread executorThread = new Thread(sut);
        executorThread.start();
        awaitLatch(start);


        final Thread getterThread = new Thread(this::getterThread);
        getterThread.start();
        Thread.sleep(100L);

        Assert.assertFalse("Getter did not block on get", getterFinished.get());
        stop.countDown();

        getterThread.join();
        executorThread.join();
    }

    @Test(timeout = 5000L)
    public void getBlocksUntilOtherFutureIsComplete() throws Exception {
        sut = new DefaultApplicationRoleManager.DelegatingSettableFutureTask(() -> unexpectedExecution.set(true));

        final SettableFuture<Void> otherFuture = SettableFuture.create();

        final Thread getterThread = new Thread(this::getterThread);
        getterThread.start();
        Thread.sleep(100L);
        sut.setDelegate(otherFuture);
        Thread.sleep(100L);

        Assert.assertFalse("Getter did not block on get", getterFinished.get());
        otherFuture.set(null);

        getterThread.join();
    }

    private void getterThread() {
        try {
            sut.get(5000, TimeUnit.MILLISECONDS);
            getterFinished.set(true);
        } catch (final Throwable e) {
            exception.set(true);
        }
    }

    private void awaitLatch(final CountDownLatch latch) {
        try {
            latch.await();
        } catch (final InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    @After
    public void checkGenericAssertions() {
        Assert.assertFalse("There was execution of unexpected part of code", unexpectedExecution.get());
        Assert.assertFalse("There was an unexpected exception in code", exception.get());
    }
}
