package com.atlassian.jira.upgrade.tasks;

import com.atlassian.jira.cluster.ClusterManager;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockComponentContainer;
import com.atlassian.jira.mock.ofbiz.MockOfBizDelegator;
import com.atlassian.jira.ofbiz.FieldMap;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.service.ServiceManager;
import com.atlassian.jira.upgrade.UpgradeTask;
import com.google.common.collect.ImmutableList;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.ofbiz.core.entity.GenericValue;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.atlassian.jira.upgrade.tasks.UpgradeTask_Build6302.LOCK_NAMES;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.verify;
import static org.ofbiz.core.entity.EntityUtil.getOnly;


/**
 * Test the upgrade of the Quartz tables.
 *
 * @since v6.2
 */
@RunWith(MockitoJUnitRunner.class)
public class TestUpgradeTask_Build6302 {
    private static final String QUARTZ1_JOB_CLASS = "com.atlassian.scheduler.quartz1.Quartz1Job";
    private static final List<GenericValue> NO_GVS = ImmutableList.of();
    private static final String STATE_NORMAL = "NORMAL";

    @Rule
    public MockComponentContainer mockComponentContainer = new MockComponentContainer(this);

    @Mock
    @AvailableInContainer
    private ApplicationProperties mockApplicationProperties;

    @Mock
    @AvailableInContainer
    private ClusterManager clusterManager;

    @Mock
    private ServiceManager serviceManager;

    @AvailableInContainer
    private OfBizDelegator ofBizDelegator = new MockOfBizDelegator();

    @Before
    public void setupMocks() throws ParseException {
        buildOldTables();
    }

    @Test
    public void testUpgradedSchedule() throws Exception {
        UpgradeTask task = new UpgradeTask_Build6302(ofBizDelegator, serviceManager);
        task.doUpgrade(false);

        assertThat(ofBizDelegator.findByField("JQRTZTriggers", "triggerName", "SEND_SUBCRIPTION"), is(NO_GVS));
        assertThat(ofBizDelegator.findByField("JQRTZTriggers", "groupName", "SEND_SUBCRIPTION"), is(NO_GVS));

        checkJobDetail("ServicesJob", "Default", QUARTZ1_JOB_CLASS, true);
        checkJobDetail("RefreshActiveUserCount", "Default", QUARTZ1_JOB_CLASS, true);

        final DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        checkSimpleTriggerDetails("ServicesTrigger", "ServicesJob", "ServicesJob", "Default",
                df.parse("2093-09-20 06:00:00"), STATE_NORMAL, df.parse("2092-03-07 09:29:19"), null, "", 0, 1,
                1, 10);
        checkCronTriggerDetails("RefreshActiveUserCountTrigger", "RefreshActiveUserCount", "RefreshActiveUserCount",
                "Default", df.parse("2093-09-20 06:00:00"), STATE_NORMAL,
                df.parse("2092-03-07 09:29:19"), null, "", 0, "0 0 0/2 * * ?");

        GenericValue trigger = getTrigger("SUBSCRIPTION_10100", "SEND_SUBSCRIPTION");
        assertThat("Subscription trigger should get discarded", trigger, nullValue());
        trigger = getTrigger("SUBSCRIPTION_10200", "SEND_SUBSCRIPTION");
        assertThat("Subscription trigger should get discarded", trigger, nullValue());
        verify(serviceManager).refreshAll();
    }

    private void checkJobDetail(final String jobName, final String jobGroup,
                                final String jobClass, final boolean durable) {
        final GenericValue jobDetail = getJobDetail(jobName, jobGroup);
        assertThat(jobDetail, not(nullValue()));
        assertThat(jobDetail.getString("jobName"), is(jobName));
        assertThat(jobDetail.getString("jobGroup"), is(jobGroup));
        assertThat(jobDetail.getString("description"), nullValue());
        assertThat(jobDetail.get("jobData"), nullValue());
        assertThat(jobDetail.getBoolean("isDurable"), is(durable));
//      Statefulness depends upon the job class and we never are and this test doesn't test the database content
//      so commented out but left visible here so its absence is not read as an oversight.
//        assertThat(jobDetail.getBoolean("isStateful"), is(stateful));
        assertThat(jobDetail.getString("className"), is(jobClass));
        checkLockEntitiesExist();
    }

    private GenericValue getJobDetail(final String jobName, final String jobGroup) {
        return getOnly(ofBizDelegator.findByAnd("JQRTZJobDetails", new FieldMap()
                .add("jobName", jobName)
                .add("jobGroup", jobGroup)));
    }

    private GenericValue getTrigger(final String triggerName, final String triggerGroup) {
        return getOnly(ofBizDelegator.findByAnd("JQRTZTriggers", new FieldMap()
                .add("triggerName", triggerName)
                .add("triggerGroup", triggerGroup)));
    }

    private GenericValue getSimpleTrigger(final String triggerName, final String triggerGroup) {
        return getOnly(ofBizDelegator.findByAnd("JQRTZSimpleTriggers", new FieldMap()
                .add("triggerName", triggerName)
                .add("triggerGroup", triggerGroup)));
    }

    private GenericValue getCronTrigger(final String triggerName, final String triggerGroup) {
        return getOnly(ofBizDelegator.findByAnd("JQRTZCronTriggers", new FieldMap()
                .add("triggerName", triggerName)
                .add("triggerGroup", triggerGroup)));
    }

    private void checkSimpleTriggerDetails(final String triggerName, final String triggerGroup, final String jobName,
                                           final String jobGroup, final Date nextFireTime, final String state, final Date startTime, final Date endTime,
                                           final String calendar, final int misfire, final long repeatCount, final long repeatInterval,
                                           final long timesTriggered) {
        final GenericValue trigger = getTrigger(triggerName, triggerGroup);
        assertThat(trigger, not(nullValue()));
        checkCommonTriggerDetails(trigger, triggerName, triggerGroup, jobName, jobGroup, nextFireTime, state, startTime,
                endTime, calendar, misfire);
        assertThat(trigger.getString("triggerType"), is("SIMPLE"));

        final GenericValue simpleTrigger = getSimpleTrigger(triggerName, triggerGroup);
        assertThat(simpleTrigger.getLong("repeatCount"), is(repeatCount));
        assertThat(simpleTrigger.getLong("repeatInterval"), is(repeatInterval));
        assertThat(simpleTrigger.getLong("timesTriggered"), is(timesTriggered));
    }

    private void checkCronTriggerDetails(final String triggerName, final String triggerGroup, final String jobName,
                                         final String jobGroup, final Date nextFireTime, final String state, final Date startTime, final Date endTime,
                                         final String calendar, final int misfire, final String cron) {
        final GenericValue trigger = getTrigger(triggerName, triggerGroup);
        assertThat(trigger, not(nullValue()));
        checkCommonTriggerDetails(trigger, triggerName, triggerGroup, jobName, jobGroup, nextFireTime, state, startTime,
                endTime, calendar, misfire);
        assertThat(trigger.getString("triggerType"), is("CRON"));

        final GenericValue cronTrigger = getCronTrigger(triggerName, triggerGroup);
        assertThat(cronTrigger.getString("cronExpression"), is(cron));
    }

    private void checkCommonTriggerDetails(final GenericValue trigger, final String triggerName, final String triggerGroup,
                                           final String jobName, final String jobGroup, final Date nextFireTime, final String state, final Date startTime,
                                           final Date endTime, final String calendar, final int misfire) {
        assertThat(trigger.getString("triggerName"), is(triggerName));
        assertThat(trigger.getString("triggerGroup"), is(triggerGroup));
        assertThat(trigger.getString("jobName"), is(jobName));
        assertThat(trigger.getString("jobGroup"), is(jobGroup));
        assertThat(trigger.getString("triggerState"), is(state));
        assertThat(toDate(trigger.getLong("nextFireTime")), is(nextFireTime));
        assertThat(toDate(trigger.getLong("startTime")), is(startTime));
        assertThat(toDate(trigger.getLong("endTime")), is(endTime));
        assertThat(trigger.getString("calendarName"), is(calendar));
        assertThat(trigger.getInteger("misfireInstr"), is(misfire));
    }

    private void checkLockEntitiesExist() {
        final String[] expectedLocks = LOCK_NAMES.toArray(new String[LOCK_NAMES.size()]);
        final List<String> actualLocks = new ArrayList<>(expectedLocks.length);
        for (GenericValue lock : ofBizDelegator.findAll("JQRTZLocks")) {
            actualLocks.add(lock.getString("lockName"));
        }
        assertThat(actualLocks, containsInAnyOrder(expectedLocks));
    }

    private void buildOldTables() throws ParseException {
        // Typically these contain the couple of standard jobs for running services and the subscriptions
        addJobDetail(10000, "SEND_SUBSCRIPTION", "SEND_SUBSCRIPTION", "com.atlassian.scheduler.quartz1.Quartz1Job",
                "true", "false", "false", "X");
        addJobDetail(10021, "ServicesJob", "Default", "com.atlassian.scheduler.quartz1.Quartz1Job",
                "true", "true", "false", "");
        addJobDetail(10022, "RefreshActiveUserCount", "Default", "com.atlassian.scheduler.quartz1.Quartz1Job",
                "true", "false", "false", "X");

        final DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        addSimpleTrigger(10000, "ServicesTrigger", "ServicesJob", 10021, df.parse("2093-09-20 06:00:00"),
                "WAITING", "SIMPLE", df.parse("2092-03-07 09:29:19"), null, "", 0, 1, 1, 10);
        addCronTrigger(10001, "RefreshActiveUserCountTrigger", "RefreshActiveUserCount", 10022, df.parse("2093-09-20 06:00:00"),
                "WAITING", "CRON", df.parse("2092-03-07 09:29:19"), null, "", 0, "0 0 0/2 * * ?");
        addCronTrigger(10100, "SUBSCRIPTION_10100", "SEND_SUBSCRIPTION", 10000, df.parse("2093-09-20 06:00:00"),
                "WAITING", "CRON", df.parse("2092-03-07 09:29:19"), null, "", 0, "0 0 6 ? * *");
        addCronTrigger(10101, "SUBSCRIPTION_10200", "SEND_SUBSCRIPTION", 10000, df.parse("2093-09-20 06:00:00"),
                "WAITING", "CRON", df.parse("2092-03-07 09:29:19"), null, "", 0, "0 0 7-15/3 ? * *");
    }

    private void addJobDetail(final long id, final String jobName, final String groupname, final String clazz,
                              final String durable, final String stateful, final String reqRecovery, final String jobdata) {
        final FieldMap values = new FieldMap();
        values.put("id", id);
        values.put("jobName", jobName);
        values.put("jobGroup", groupname);
        values.put("className", clazz);
        values.put("isDurable", durable);
        values.put("isStateful", stateful);
        values.put("requestsRecovery", reqRecovery);
        values.put("jobData", jobdata);

        ofBizDelegator.createValue("QRTZJobDetails", values);
    }

    private void addSimpleTrigger(final long id, final String triggerName, final String triggerGroup, final int jobId,
                                  final Date nextFire, final String state, final String type, final Date startTime, final Date endTime,
                                  final String calendar, final int misfire, final long repeatCount, final long repeatInterval,
                                  final long timesTriggered) {
        addTrigger(id, triggerName, triggerGroup, jobId, nextFire, state, type, startTime, endTime, calendar, misfire);
        final FieldMap values = new FieldMap();
        values.put("id", id);
        values.put("trigger", id);
        values.put("repeatCount", repeatCount);
        values.put("repeatInterval", repeatInterval);
        values.put("timesTriggered", timesTriggered);

        ofBizDelegator.createValue("QRTZSimpleTriggers", values);

    }

    private void addCronTrigger(final long id, final String triggerName, final String triggerGroup, final long jobId,
                                final Date nextFire, final String state, final String type, final Date startTime, final Date endTime,
                                final String calendar, final int misfire, final String cron) {
        addTrigger(id, triggerName, triggerGroup, jobId, nextFire, state, type, startTime, endTime, calendar, misfire);
        final FieldMap values = new FieldMap();
        values.put("id", id);
        values.put("trigger", id);
        values.put("cronExpression", cron);

        ofBizDelegator.createValue("QRTZCronTriggers", values);
    }

    private void addTrigger(final long id, final String triggerName, final String triggerGroup, final long jobId,
                            final Date nextFire, final String state, final String type, final Date startTime, final Date endTime,
                            final String calendar, final int misfire) {
        final FieldMap values = new FieldMap();
        values.put("id", id);
        values.put("triggerName", triggerName);
        values.put("triggerGroup", triggerGroup);
        values.put("job", jobId);
        values.put("nextFire", nextFire == null ? null : new Timestamp(nextFire.getTime()));
        values.put("triggerState", state);
        values.put("triggerType", type);
        values.put("startTime", startTime == null ? null : new Timestamp(startTime.getTime()));
        values.put("endTime", endTime == null ? null : new Timestamp(endTime.getTime()));
        values.put("calendarName", calendar);
        values.put("misfireInstr", misfire);

        ofBizDelegator.createValue("QRTZTriggers", values);
    }

    private static Date toDate(Long millis) {
        return (millis != null) ? new Date(millis) : null;
    }
}
