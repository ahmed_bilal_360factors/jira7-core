package com.atlassian.jira.application;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.cache.memory.MemoryCacheManager;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.crowd.embedded.spi.DirectoryDao;
import com.atlassian.crowd.manager.directory.SynchronisationStatusManager;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.cluster.ClusterMessagingService;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.config.database.DatabaseConfig;
import com.atlassian.jira.config.database.JndiDatasource;
import com.atlassian.jira.config.database.MockDatabaseConfigurationManager;
import com.atlassian.jira.crowd.embedded.ofbiz.InternalMembershipDao;
import com.atlassian.jira.crowd.embedded.ofbiz.MockDirectory;
import com.atlassian.jira.crowd.embedded.ofbiz.OfBizUserDao;
import com.atlassian.jira.crowd.embedded.ofbiz.db.DefaultOfBizTransactionManager;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.license.JiraLicenseManager;
import com.atlassian.jira.license.LicenseDetails;
import com.atlassian.jira.license.MockLicenseDetails;
import com.atlassian.jira.license.MockLicensedApplications;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.groups.MockGroupManager;
import com.atlassian.jira.user.ApplicationUsers;
import com.atlassian.jira.user.MockCrowdService;
import com.atlassian.jira.user.MockGroup;
import com.atlassian.jira.user.MockUser;
import com.atlassian.jira.user.util.DisabledRecoveryMode;
import com.atlassian.jira.user.util.MockUserManager;
import com.atlassian.jira.user.util.UserManager;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import static com.atlassian.jira.permission.GlobalPermissionKey.SYSTEM_ADMIN;
import static com.atlassian.jira.permission.GlobalPermissionKey.USE;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyCollectionOf;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

/**
 * Tests {@link DefaultApplicationRoleManager#totalBillableUsers()} and {@link DefaultApplicationRoleManager#flush()}.
 *
 * @since v7.0
 */
public class DefaultApplicationRoleManagerLicenseCountServiceTest {
    @Rule
    public MockitoContainer container = MockitoMocksInContainer.rule(this);

    private final MockGroupManager groupManager = new MockGroupManager();
    private final MemoryCacheManager cacheManager = new MemoryCacheManager();
    private final MockCrowdService crowdService = new MockCrowdService();
    private final MockApplicationRoleStore store = new MockApplicationRoleStore();
    private final MockApplicationRoleDefinitions definitions = new MockApplicationRoleDefinitions();
    private MockUserManager userManager;
    private DefaultApplicationRoleManager manager;

    @Mock
    private Directory directory;
    @Mock
    private JiraLicenseManager licenseManager;
    @Mock
    private FeatureManager featureManager;
    @Mock
    private EventPublisher eventPublisher;
    @Mock
    private InternalMembershipDao internalMembershipDao;
    @Mock
    private DirectoryDao directoryDao;
    @Mock
    private SynchronisationStatusManager crowdSyncStatusManager;

    @Mock
    @AvailableInContainer
    private GlobalPermissionManager globalPermissionManager;

    private static final ApplicationKey testAppKey = ApplicationKey.valueOf("com.test.application.one");
    private Set<LicenseDetails> licenses = new HashSet<>();
    private OfBizUserDao ofBizUserDao;

    @Before
    public void setup() {
        final MockDatabaseConfigurationManager databaseConfigurationManager = new MockDatabaseConfigurationManager();
        databaseConfigurationManager.setDatabaseConfiguration(new DatabaseConfig("postgres", "jira", new JndiDatasource("jira")));
        ofBizUserDao = new OfBizUserDao(null, directoryDao, null, null, null, cacheManager, null, null, new DefaultOfBizTransactionManager(), databaseConfigurationManager) {
            @Override
            public boolean useFullCache() {
                return false;
            }

            @Override
            public void processUsers(Consumer userProcessor) {
                userManager.getAllUsers().forEach(appUser -> userProcessor.accept(appUser.getDirectoryUser()));
            }
        };

        when(licenseManager.getLicenses()).thenReturn(licenses);
        userManager = new MockUserManager(crowdService);
        userManager.alwaysReturnUsers();
        userManager.useCrowdServiceToGetUsers();
        container.getMockWorker().addMock(UserManager.class, userManager);

        manager = new DefaultApplicationRoleManager(cacheManager, store, definitions, groupManager,
                licenseManager, new DisabledRecoveryMode(), crowdService, eventPublisher,
                internalMembershipDao, directoryDao, ofBizUserDao, new DefaultOfBizTransactionManager(),
                crowdSyncStatusManager, featureManager);

        when(directoryDao.findAll()).thenReturn(ImmutableList.of(new MockDirectory(MockUser.MOCK_DIRECTORY_ID)));
        when(internalMembershipDao.findGroupChildrenOfGroups(anyLong(), anyCollectionOf(String.class))).thenReturn(Collections.emptyList());
        when(internalMembershipDao.findUserChildrenOfGroups(anyLong(), anyCollectionOf(String.class))).thenAnswer(invocation ->
        {
            @SuppressWarnings("unchecked") Collection<String> groupNames = invocation.getArgumentAt(1, Collection.class);
            List<Group> groups = groupNames.stream().map(MockGroup::new).collect(Collectors.toList());
            return groupManager.getUserNamesInGroups(groups);

        });
    }

    @Test
    public void billableUsersShouldCountRegularUsers() {
        final Group group1 = setupGroup("group1");
        final Group group2 = setupGroup("group2");
        final Group notCountedGroup = setupGroup("irrelevant group");
        User user1 = setupMockUser("Bob", group1, notCountedGroup);
        User user2 = setupMockUser("Alice", group2, notCountedGroup);

        // Should not be used because roles enabled
        User notCountedUser = setupMockUser("irrelevant user", notCountedGroup);
        when(globalPermissionManager.getGroupsWithPermission(USE)).thenReturn(ImmutableList.of(notCountedGroup));

        setupApplication(testAppKey, 5, ImmutableSet.of(group1, group2));
        when(licenseManager.getAllLicensedApplicationKeys()).thenReturn(ImmutableSet.of(testAppKey));

        assertEquals("Should count regular user", 2, manager.totalBillableUsers());
    }

    @Test
    public void billableUsersShouldNotCountNonActiveUsers() {
        final Group group1 = setupGroup("group1");
        final Group group2 = setupGroup("group2");
        User user1 = setupMockUser("Alice", group2);
        ApplicationRole role = setupApplication(testAppKey, 5, ImmutableSet.of(group1, group2));

        MockUser user2 = new MockUser("Bob");
        user2.setActive(false);
        crowdService.addUser(user2, null);
        crowdService.addUserToGroup(user2, group1);
        groupManager.addUserToGroup(ApplicationUsers.from(user2), group1);

        when(licenseManager.getAllLicensedApplicationKeys()).thenReturn(ImmutableSet.of(testAppKey));

        assertEquals("Should count regular user", 1, manager.totalBillableUsers());
    }

    @Test
    public void totalBillableUsersShouldCountUserWhenStartsWith_addon_StringAndNotInAddOnsGroup() {
        final Group group1 = setupGroup("group1");
        MockUser connectUser = setupMockUser("addon_bob", group1);
        ApplicationRole role = setupApplication(testAppKey, 5, ImmutableSet.of(group1));
        when(licenseManager.getAllLicensedApplicationKeys()).thenReturn(ImmutableSet.of(testAppKey));

        assertThat("Should not count Connect User", manager.totalBillableUsers(), equalTo(1));
    }

    @Test
    public void totalBillableUsersShouldCountNotConnectUser_NonNullUserAttributesAndStartsWith_addon_String() {
        final Group nonConnectGroup = setupGroup("nonConnectGroup");
        final Group group2 = setupGroup("group2");
        MockUser connectUser = setupMockUser("addon_bob", nonConnectGroup);
        crowdService.setUserAttribute(connectUser, "nonConnectKey", "true");
        ApplicationRole role = setupApplication(testAppKey, 5, ImmutableSet.of(nonConnectGroup));
        when(licenseManager.getAllLicensedApplicationKeys()).thenReturn(ImmutableSet.of(testAppKey));

        assertThat("Should not count Connect User", manager.totalBillableUsers(), equalTo(1));
    }

    @Test
    public void totalBillableUsersShouldCountSysAdminInServer() {
        final Group group1 = setupGroup("group1");
        final Group group2 = setupGroup("group2");
        MockUser user1 = setupMockUser("Bob", group1);
        MockUser user2 = setupMockUser("Alice", group2);
        ApplicationRole role = setupApplication(testAppKey, 5, ImmutableSet.of(group1, group2));
        when(licenseManager.getAllLicensedApplicationKeys()).thenReturn(ImmutableSet.of(testAppKey));
        when(globalPermissionManager.hasPermission(SYSTEM_ADMIN, ApplicationUsers.from(user1))).thenReturn(true);

        assertEquals("Should count regular user", 2, manager.totalBillableUsers());
    }

    @Test
    public void billableUsersCountShouldBeCaseInsensitive() {
        final Group group1 = setupGroup("group1");
        final Group group2 = setupGroup("group2");
        MockUser user1 = setupMockUser("amy", group1);
        MockUser user2 = setupMockUser("AmY", group2);
        ApplicationRole role = setupApplication(testAppKey, 5, ImmutableSet.of(group1, group2));
        when(licenseManager.getAllLicensedApplicationKeys()).thenReturn(ImmutableSet.of(testAppKey));

        assertEquals("Should count regular user", 1, manager.totalBillableUsers());
    }

    @Test
    public void flushShouldResetAllMethods() throws Exception {
        final Group group1 = setupGroup("group1");
        final Group notCountedGroup = setupGroup("notCountedGroup");
        MockUser user1 = setupMockUser("Bob", group1, notCountedGroup);

        // Should not be used because roles enabled
        User notCountedUser = setupMockUser("irrelevant user", notCountedGroup);
        when(globalPermissionManager.getGroupsWithPermission(USE)).thenReturn(ImmutableList.of(notCountedGroup));

        setupApplication(testAppKey, 5, ImmutableSet.of(group1));
        when(licenseManager.getAllLicensedApplicationKeys()).thenReturn(ImmutableSet.of(testAppKey));

        assertEquals(1, manager.totalBillableUsers());

        ApplicationKey testAppKey2 = ApplicationKey.valueOf("com.test.application.two");
        setupApplication(testAppKey2, 5, ImmutableSet.of());

        // Make sure it's not changed until it's flushed. This isn't strictly necessary but it helps to ensure flush
        // actually does something. Think of it as internal verification that the test works.
        when(licenseManager.getAllLicensedApplicationKeys()).thenReturn(ImmutableSet.of(testAppKey2));
        assertEquals(1, manager.totalBillableUsers());

        manager.flush();
        assertEquals(0, manager.totalBillableUsers());
    }

    @Test
    public void totalBillableUserCountSameUserInMultipleGroups() throws Exception {
        final Group group1 = setupGroup("group1");
        final Group group2 = setupGroup("group2");
        final Group notCountedGroup = setupGroup("irrelevant group");
        MockUser user1 = setupMockUser("Bob", group1, group2, notCountedGroup);
        MockUser user2 = setupMockUser("Alice", group2);

        // Should not be used because roles enabled
        User notCountedUser = setupMockUser("irrelevant user", notCountedGroup);
        when(globalPermissionManager.getGroupsWithPermission(USE)).thenReturn(ImmutableList.of(notCountedGroup));

        setupApplication(testAppKey, 5, ImmutableSet.of(group1));
        when(licenseManager.getAllLicensedApplicationKeys()).thenReturn(ImmutableSet.of(testAppKey));

        assertEquals(1, manager.totalBillableUsers());

        ApplicationKey testAppKey2 = ApplicationKey.valueOf("com.test.application.two");
        setupApplication(testAppKey2, 5, ImmutableSet.of());

        // Make sure it's not changed until it's flushed. This isn't strictly necessary but it helps to ensure flush
        // actually does something. Think of it as internal verification that the test works.
        when(licenseManager.getAllLicensedApplicationKeys()).thenReturn(ImmutableSet.of(testAppKey2));
        assertEquals(1, manager.totalBillableUsers());

        manager.flush();
        assertEquals(0, manager.totalBillableUsers());
    }

    /**
     * Make sure we are reading from multiple directories correctly when there are no shadowed users.
     */
    @Test
    public void multipleDirectoryNonShadowedUsersCountTest() throws Exception {
        long secondDirectoryId = 2L;
        when(directoryDao.findAll()).thenReturn(ImmutableList.of(new MockDirectory(MockUser.MOCK_DIRECTORY_ID), new MockDirectory(secondDirectoryId)));
        when(internalMembershipDao.findGroupChildrenOfGroups(anyLong(), anyCollectionOf(String.class))).thenReturn(Collections.emptyList());
        when(internalMembershipDao.findUserChildrenOfGroups(eq(MockUser.MOCK_DIRECTORY_ID), anyCollectionOf(String.class))).thenAnswer(invocation ->
        {
            @SuppressWarnings("unchecked") Collection<String> groupNames = invocation.getArgumentAt(1, Collection.class);
            List<Group> groups = groupNames.stream().map(MockGroup::new).collect(Collectors.toList());
            return groupManager.getUserNamesInGroups(groups);

        });

        //Special logic for the second directory - Bob is in group2 in directory2
        when(internalMembershipDao.findUserChildrenOfGroups(eq(secondDirectoryId), anyCollectionOf(String.class))).thenAnswer(invocation ->
        {
            @SuppressWarnings("unchecked") Collection<String> groupNames = invocation.getArgumentAt(1, Collection.class);
            if (groupNames.contains("group2")) {
                return ImmutableList.of("Bob");
            } else {
                return ImmutableList.of();
            }
        });

        //Bob exists in both directories, but we use his groups from directory 1 only
        final Group group1 = setupGroup("group1");
        final Group group2 = setupGroup("group2");
        final Group notCountedGroup = setupGroup("irrelevant group");
        //Bob is in directory 2 only
        User user2 = setupMockUser("Alice", group2, notCountedGroup);

        // Should not be used because roles enabled
        User notCountedUser = setupMockUser("irrelevant user", notCountedGroup);
        when(globalPermissionManager.getGroupsWithPermission(USE)).thenReturn(ImmutableList.of(notCountedGroup));

        ApplicationRole role = setupApplication(testAppKey, 5, ImmutableSet.of(group1, group2));
        when(licenseManager.getAllLicensedApplicationKeys()).thenReturn(ImmutableSet.of(testAppKey));

        assertEquals("Should count regular user", 1, manager.totalBillableUsers());
    }

    /**
     * Make sure directory shadowing rules for users takes effect when there are multiple directories.
     */
    @Test
    public void multipleDirectoryShadowedUsersCountTest() throws Exception {
        long secondDirectoryId = 2L;
        when(directoryDao.findAll()).thenReturn(ImmutableList.of(new MockDirectory(MockUser.MOCK_DIRECTORY_ID), new MockDirectory(secondDirectoryId)));
        when(internalMembershipDao.findGroupChildrenOfGroups(anyLong(), anyCollectionOf(String.class))).thenReturn(Collections.emptyList());
        when(internalMembershipDao.findUserChildrenOfGroups(eq(MockUser.MOCK_DIRECTORY_ID), anyCollectionOf(String.class))).thenAnswer(invocation ->
        {
            @SuppressWarnings("unchecked") Collection<String> groupNames = invocation.getArgumentAt(1, Collection.class);
            List<Group> groups = groupNames.stream().map(MockGroup::new).collect(Collectors.toList());
            return groupManager.getUserNamesInGroups(groups);

        });

        //Special logic for the second directory - Bob is in group2 in directory2
        when(internalMembershipDao.findUserChildrenOfGroups(eq(secondDirectoryId), anyCollectionOf(String.class))).thenAnswer(invocation ->
        {
            @SuppressWarnings("unchecked") Collection<String> groupNames = invocation.getArgumentAt(1, Collection.class);
            if (groupNames.contains("group2")) {
                return ImmutableList.of("Bob");
            } else {
                return ImmutableList.of();
            }
        });

        //Bob exists in both directories, but we use his groups from directory 1 only
        final Group group1 = setupGroup("group1");
        final Group group2 = setupGroup("group2");
        final Group notCountedGroup = setupGroup("irrelevant group");
        User user1 = setupMockUser("Bob", notCountedGroup); //but in directory 2 he is in group2
        User user2 = setupMockUser("Alice", group2, notCountedGroup);

        // Should not be used because roles enabled
        User notCountedUser = setupMockUser("irrelevant user", notCountedGroup);
        when(globalPermissionManager.getGroupsWithPermission(USE)).thenReturn(ImmutableList.of(notCountedGroup));

        ApplicationRole role = setupApplication(testAppKey, 5, ImmutableSet.of(group1, group2));
        when(licenseManager.getAllLicensedApplicationKeys()).thenReturn(ImmutableSet.of(testAppKey));

        assertEquals("Should count regular user", 1, manager.totalBillableUsers());
    }

    private ApplicationRole setupApplication(ApplicationKey applicationId, int numSeats, Set<Group> groups) {
        //Make sure the application is licensed.
        definitions.addLicensed(new MockApplicationRoleDefinition(applicationId.value()));
        final MockLicenseDetails details = new MockLicenseDetails();
        details.setLicensedApplications(new MockLicensedApplications(ImmutableMap.of(applicationId, numSeats)));
        boolean newLicenseWasAdded = licenses.add(details);
        assert newLicenseWasAdded;

        // create the role & store
        ApplicationRole role = new MockApplicationRole(applicationId)
                .defaultGroups(groups)
                .groups(groups);

        store.save(role);

        return role;
    }

    // Creates a group and registers it with crowd and GroupManager
    private Group setupGroup(String name) {
        final Group group = new MockGroup(name);
        crowdService.addGroup(group);
        groupManager.addGroup(name);
        return group;
    }

    // Creates a user and adds it to the associated groups through CrowdService and GroupManager.
    private MockUser setupMockUser(String name, Group... groups) {
        MockUser user = new MockUser(name);
        crowdService.addUser(user, null);

        for (Group group : groups) {
            crowdService.addUserToGroup(user, group);
            groupManager.addUserToGroup(ApplicationUsers.from(user), group);
        }

        userManager.addUser(ApplicationUsers.from(user));

        return user;
    }
}
