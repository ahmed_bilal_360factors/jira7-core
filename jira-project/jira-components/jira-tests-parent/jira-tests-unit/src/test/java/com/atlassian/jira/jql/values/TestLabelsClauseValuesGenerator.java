package com.atlassian.jira.jql.values;

import com.atlassian.jira.issue.label.LabelManager;
import com.atlassian.jira.issue.search.managers.SearchHandlerManager;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSortedSet;
import com.google.common.collect.Sets;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.mockito.Mock;

import java.util.Collections;

import static com.atlassian.jira.jql.values.ClauseValuesGenerator.Result;
import static com.atlassian.jira.jql.values.ClauseValuesGenerator.Results;
import static com.atlassian.jira.util.collect.CollectionBuilder.list;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestLabelsClauseValuesGenerator {
    private ApplicationUser user = new MockApplicationUser("admin");

    @Rule
    public final TestRule mockInContainer = MockitoMocksInContainer.rule(this);

    @Mock
    @AvailableInContainer
    private SearchHandlerManager mockSearchHandlerManager;

    @Test
    public void testGetPossibleValues() {
        final LabelManager mockLabelManager = mock(LabelManager.class);
        final SearchHandlerManager mockSearchHandlerManager = mock(SearchHandlerManager.class);
        when(mockSearchHandlerManager.getFieldIds(user, "labels")).thenReturn(Collections.singletonList("labels"));
        when(mockLabelManager.getSuggestedLabels(user, null, "to")).thenReturn(ImmutableSortedSet.of("token", "toxic"));

        final LabelsClauseValuesGenerator generator = new LabelsClauseValuesGenerator(mockLabelManager) {
            @Override
            SearchHandlerManager getSearchHandlerManager() {
                return mockSearchHandlerManager;
            }
        };
        final Results results = generator.getPossibleValues(user, "labels", "to", 10);
        assertThat(results.getResults(), contains(new Result("token"), new Result("toxic")));
    }

    @Test
    public void testGetPossibleValuesSorting() {
        final LabelManager mockLabelManager = mock(LabelManager.class);
        final SearchHandlerManager mockSearchHandlerManager = mock(SearchHandlerManager.class);
        when(mockSearchHandlerManager.getFieldIds(user, "labels")).thenReturn(Collections.singletonList("labels"));
        when(mockLabelManager.getSuggestedLabels(user, null, "an")).thenReturn(Sets.newLinkedHashSet(ImmutableList.of("android", "Android", "anzelmo", "analogy")));

        final LabelsClauseValuesGenerator generator = new LabelsClauseValuesGenerator(mockLabelManager) {
            @Override
            SearchHandlerManager getSearchHandlerManager() {
                return mockSearchHandlerManager;
            }
        };
        final Results results = generator.getPossibleValues(user, "labels", "an", 10);

        assertThat(results.getResults(), contains(new Result("android"),
                new Result("Android"),
                new Result("anzelmo"),
                new Result("analogy")));
    }

    @Test
    public void testGetPossibleValuesMultipleFields() {
        final LabelManager mockLabelManager = mock(LabelManager.class);
        final SearchHandlerManager mockSearchHandlerManager = mock(SearchHandlerManager.class);
        when(mockSearchHandlerManager.getFieldIds(user, "epic")).thenReturn(list("customfield_10000", "customfield_10001"));
        when(mockLabelManager.getSuggestedLabels(user, null, 10000L, "to")).thenReturn(ImmutableSortedSet.of("token", "toxic"));
        when(mockLabelManager.getSuggestedLabels(user, null, 10001L, "to")).thenReturn(ImmutableSortedSet.of("tobar", "tozzle"));

        final LabelsClauseValuesGenerator generator = new LabelsClauseValuesGenerator(mockLabelManager) {
            @Override
            SearchHandlerManager getSearchHandlerManager() {
                return mockSearchHandlerManager;
            }
        };
        final Results results = generator.getPossibleValues(user, "epic", "to", 10);
        assertThat(results.getResults(), containsInAnyOrder(new Result("tobar"), new Result("token"),
                new Result("toxic"), new Result("tozzle")));
    }

    @Test
    public void testGetPossibleValuesMaxResults() {
        final LabelManager mockLabelManager = mock(LabelManager.class);
        when(mockSearchHandlerManager.getFieldIds(user, "epic")).thenReturn(list("customfield_10000", "customfield_10001"));
        when(mockLabelManager.getSuggestedLabels(user, null, 10000L, "to")).thenReturn(ImmutableSortedSet.of("token", "toxic"));
        when(mockLabelManager.getSuggestedLabels(user, null, 10001L, "to")).thenReturn(ImmutableSortedSet.of("tobar", "tozzle"));

        final LabelsClauseValuesGenerator generator = new LabelsClauseValuesGenerator(mockLabelManager);
        final Results results = generator.getPossibleValues(user, "epic", "to", 2);

        assertThat(results.getResults(), containsInAnyOrder(new Result("toxic"),
                new Result("token")));
    }
}
