package com.atlassian.jira.issue.search.searchers.transformer;

import com.atlassian.jira.issue.search.ClauseNames;
import com.atlassian.jira.jql.operand.JqlOperandResolver;
import com.atlassian.jira.jql.validator.MockJqlOperandResolver;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.util.collect.CollectionBuilder;
import com.atlassian.query.QueryImpl;
import com.atlassian.query.clause.AndClause;
import com.atlassian.query.clause.TerminalClauseImpl;
import com.atlassian.query.operand.EmptyOperand;
import com.atlassian.query.operand.FunctionOperand;
import com.atlassian.query.operand.MultiValueOperand;
import com.atlassian.query.operand.Operand;
import com.atlassian.query.operand.SingleValueOperand;
import com.atlassian.query.operator.Operator;
import com.google.common.collect.ImmutableSet;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestNavigatorStructureChecker {
    @Rule
    public MockitoRule mockito = MockitoJUnit.rule();

    private static final String FIELD_NAME = "fieldName";
    @Mock
    private JqlOperandResolver jqlOperandResolver;
    @Mock
    private FieldFlagOperandRegistry fieldFlagOperandRegistry;

    @Test
    public void testValidForNavigatorNullQuery() throws Exception {
        NavigatorStructureChecker transformer = createNavigatorStructureChecker(true);
        assertTrue(transformer.checkSearchRequest(null));
    }

    @Test
    public void testValidForNavigatorNullWhereClause() throws Exception {
        NavigatorStructureChecker transformer = createNavigatorStructureChecker(true);
        assertTrue(transformer.checkSearchRequest(new QueryImpl()));
    }

    @Test
    public void testCheckOperandSupportedFunction() throws Exception {
        FunctionOperand functionOperand = new FunctionOperand("function");

        when(fieldFlagOperandRegistry.getFlagForOperand(FIELD_NAME, functionOperand)).thenReturn(ImmutableSet.of());

        NavigatorStructureChecker checker = createNavigatorStructureChecker();
        assertTrue(checker.checkOperand(functionOperand, true));

        verify(fieldFlagOperandRegistry).getFlagForOperand(FIELD_NAME, functionOperand);
    }

    @Test
    public void testCheckOperandFunctionsNotAccepted() throws Exception {
        FunctionOperand functionOperand = new FunctionOperand("function");

        NavigatorStructureChecker checker = createNavigatorStructureChecker();
        assertFalse(checker.checkOperand(functionOperand, false));
    }

    @Test
    public void testCheckOperandUnsupportedFunction() throws Exception {
        FunctionOperand functionOperand = new FunctionOperand("function");

        when(fieldFlagOperandRegistry.getFlagForOperand(FIELD_NAME, functionOperand)).thenReturn(null);

        NavigatorStructureChecker checker = createNavigatorStructureChecker();
        assertFalse(checker.checkOperand(functionOperand, true));

        verify(fieldFlagOperandRegistry).getFlagForOperand(FIELD_NAME, functionOperand);
    }

    @Test
    public void testCheckOperandEmptyHasFlagForThisField() throws Exception {
        Operand operand = EmptyOperand.EMPTY;

        when(fieldFlagOperandRegistry.getFlagForOperand(FIELD_NAME, operand)).thenReturn(ImmutableSet.of("-1"));

        NavigatorStructureChecker checker = createNavigatorStructureChecker();
        assertTrue(checker.checkOperand(operand, false));
    }

    @Test
    public void testCheckOperandEmptyDoesntHaveFlagForThisField() throws Exception {
        Operand operand = EmptyOperand.EMPTY;

        when(fieldFlagOperandRegistry.getFlagForOperand(FIELD_NAME, operand)).thenReturn(null);

        NavigatorStructureChecker checker = createNavigatorStructureChecker();
        assertFalse(checker.checkOperand(operand, false));
    }

    @Test
    public void testCheckOperandSingleValueInFieldFlagOperandRegistry() throws Exception {
        SingleValueOperand operand = new SingleValueOperand(5L);

        when(fieldFlagOperandRegistry.getFlagForOperand(FIELD_NAME, operand)).thenReturn(ImmutableSet.of("X"));

        NavigatorStructureChecker checker = new NavigatorStructureChecker<Version>(new ClauseNames(FIELD_NAME), true, fieldFlagOperandRegistry, MockJqlOperandResolver.createSimpleSupport());
        assertTrue(checker.checkOperand(operand, false));
    }

    @Test
    public void testCheckOperandMultiValueWithNoFunctionValuesOkay() throws Exception {
        final SingleValueOperand o1 = new SingleValueOperand(5L);
        final SingleValueOperand o2 = new SingleValueOperand("test");
        MultiValueOperand multiValueOperand = new MultiValueOperand(CollectionBuilder.newBuilder(o1, o2).asList());

        when(fieldFlagOperandRegistry.getFlagForOperand(FIELD_NAME, o1)).thenReturn(null);
        when(fieldFlagOperandRegistry.getFlagForOperand(FIELD_NAME, o2)).thenReturn(null);

        NavigatorStructureChecker checker = new NavigatorStructureChecker<Version>(new ClauseNames(FIELD_NAME), true, fieldFlagOperandRegistry, MockJqlOperandResolver.createSimpleSupport());
        assertTrue(checker.checkOperand(multiValueOperand, true));
    }

    @Test
    public void testCheckOperandMultiValueWithNoFunctionValuesInRegistry() throws Exception {
        final SingleValueOperand o1 = new SingleValueOperand(5L);
        final SingleValueOperand o2 = new SingleValueOperand("test");
        MultiValueOperand multiValueOperand = new MultiValueOperand(CollectionBuilder.newBuilder(o1, o2).asList());

        when(fieldFlagOperandRegistry.getFlagForOperand(FIELD_NAME, o1)).thenReturn(ImmutableSet.of("5"));
        when(fieldFlagOperandRegistry.getFlagForOperand(FIELD_NAME, o2)).thenReturn(ImmutableSet.of("test"));

        NavigatorStructureChecker checker = new NavigatorStructureChecker<Version>(new ClauseNames(FIELD_NAME), true, fieldFlagOperandRegistry, MockJqlOperandResolver.createSimpleSupport());
        assertTrue(checker.checkOperand(multiValueOperand, true));
    }

    @Test
    public void testCheckOperandMultiValueWithFunctionNotAccepted() throws Exception {
        final SingleValueOperand o1 = new SingleValueOperand(5L);
        final FunctionOperand o2 = new FunctionOperand("test");
        MultiValueOperand multiValueOperand = new MultiValueOperand(CollectionBuilder.newBuilder(o1, o2).asList());

        when(fieldFlagOperandRegistry.getFlagForOperand(FIELD_NAME, o1)).thenReturn(null);

        NavigatorStructureChecker checker = new NavigatorStructureChecker<Version>(new ClauseNames(FIELD_NAME), false, fieldFlagOperandRegistry, MockJqlOperandResolver.createSimpleSupport());
        assertFalse(checker.checkOperand(multiValueOperand, true));
    }

    @Test
    public void testCheckOperandMultiValueWithFunctionIsAccepted() throws Exception {
        final SingleValueOperand o1 = new SingleValueOperand(5L);
        final FunctionOperand functionOperand = new FunctionOperand("test");
        MultiValueOperand multiValueOperand = new MultiValueOperand(CollectionBuilder.newBuilder(o1, functionOperand).asList());

        when(fieldFlagOperandRegistry.getFlagForOperand(FIELD_NAME, o1)).thenReturn(ImmutableSet.of("5"));
        when(fieldFlagOperandRegistry.getFlagForOperand(FIELD_NAME, functionOperand)).thenReturn(ImmutableSet.of("2"));

        NavigatorStructureChecker checker = new NavigatorStructureChecker<Version>(new ClauseNames(FIELD_NAME), true, fieldFlagOperandRegistry, MockJqlOperandResolver.createSimpleSupport());
        assertTrue(checker.checkOperand(multiValueOperand, true));
    }

    @Test
    public void testCheckOperandMultiValueWithEmptyHasFlag() throws Exception {
        final SingleValueOperand o1 = new SingleValueOperand(5L);
        final EmptyOperand o2 = EmptyOperand.EMPTY;
        MultiValueOperand multiValueOperand = new MultiValueOperand(CollectionBuilder.newBuilder(o1, o2).asList());

        when(fieldFlagOperandRegistry.getFlagForOperand(FIELD_NAME, o1)).thenReturn(ImmutableSet.of("5"));
        when(fieldFlagOperandRegistry.getFlagForOperand(FIELD_NAME, o2)).thenReturn(ImmutableSet.of("-1"));

        NavigatorStructureChecker checker = new NavigatorStructureChecker<Version>(new ClauseNames(FIELD_NAME), true, fieldFlagOperandRegistry, MockJqlOperandResolver.createSimpleSupport());
        ;
        assertTrue(checker.checkOperand(multiValueOperand, false));
    }

    @Test
    public void testCheckOperandMultiValueWithEmptyDoesntHaveFlag() throws Exception {
        final SingleValueOperand o1 = new SingleValueOperand(5L);
        final EmptyOperand o2 = EmptyOperand.EMPTY;
        MultiValueOperand multiValueOperand = new MultiValueOperand(CollectionBuilder.newBuilder(o1, o2).asList());

        when(fieldFlagOperandRegistry.getFlagForOperand(FIELD_NAME, o1)).thenReturn(ImmutableSet.of("HA"));
        when(fieldFlagOperandRegistry.getFlagForOperand(FIELD_NAME, o2)).thenReturn(null);

        NavigatorStructureChecker checker = new NavigatorStructureChecker<Version>(new ClauseNames(FIELD_NAME), true, fieldFlagOperandRegistry, MockJqlOperandResolver.createSimpleSupport());
        assertFalse(checker.checkOperand(multiValueOperand, false));
    }

    @Test
    public void testCheckOperator() throws Exception {
        NavigatorStructureChecker checker = createNavigatorStructureChecker();
        List<Operator> supportedOperators = CollectionBuilder.newBuilder(Operator.EQUALS, Operator.IN, Operator.IS).asList();

        for (Operator operator : Operator.values()) {
            if (!supportedOperators.contains(operator)) {
                assertFalse(checker.checkOperator(operator));
            } else {
                assertTrue(checker.checkOperator(operator));
            }
        }
    }

    @Test
    public void testCheckSearchRequestHappyPath() throws Exception {
        AndClause andClause = new AndClause(new TerminalClauseImpl(FIELD_NAME, Operator.EQUALS, "value"),
                new TerminalClauseImpl("other", Operator.EQUALS, "valueother"));

        NavigatorStructureChecker checker = new NavigatorStructureChecker<Version>(new ClauseNames(FIELD_NAME), true, fieldFlagOperandRegistry, MockJqlOperandResolver.createSimpleSupport()) {
            @Override
            boolean checkOperator(final Operator operator) {
                return true;
            }

            @Override
            boolean checkOperand(final Operand operand, final boolean acceptFunctions) {
                return true;
            }
        };

        assertValidate(andClause, checker, true);
    }

    @Test
    public void testCheckSearchRequestClausesNotValid() throws Exception {
        AndClause andClause = new AndClause(new TerminalClauseImpl(FIELD_NAME, Operator.EQUALS, "value"),
                new TerminalClauseImpl("other", Operator.EQUALS, "valueother"));

        final SimpleNavigatorCollectorVisitor mockVisitor = new SimpleNavigatorCollectorVisitor(FIELD_NAME) {
            @Override
            public boolean isValid() {
                return false;
            }

            @Override
            public Void visit(final AndClause andClause) {
                // NOOP
                return null;
            }
        };

        NavigatorStructureChecker checker = new NavigatorStructureChecker<Version>(new ClauseNames(FIELD_NAME), true, fieldFlagOperandRegistry, MockJqlOperandResolver.createSimpleSupport()) {
            @Override
            boolean checkOperator(final Operator operator) {
                throw new UnsupportedOperationException();
            }

            @Override
            boolean checkOperand(final Operand operand, final boolean acceptFunctions) {
                throw new UnsupportedOperationException();
            }

            @Override
            SimpleNavigatorCollectorVisitor createSimpleNavigatorCollectorVisitor() {
                return mockVisitor;
            }
        };

        assertValidate(andClause, checker, false);
    }

    @Test
    public void testCheckSearchRequestTooManyClauses() throws Exception {
        AndClause andClause = new AndClause(new TerminalClauseImpl(FIELD_NAME, Operator.EQUALS, "value"),
                new TerminalClauseImpl(FIELD_NAME, Operator.EQUALS, "valueother"));

        NavigatorStructureChecker checker = new NavigatorStructureChecker<Version>(new ClauseNames(FIELD_NAME), true, fieldFlagOperandRegistry, MockJqlOperandResolver.createSimpleSupport()) {
            @Override
            boolean checkOperator(final Operator operator) {
                throw new UnsupportedOperationException();
            }

            @Override
            boolean checkOperand(final Operand operand, final boolean acceptFunctions) {
                throw new UnsupportedOperationException();
            }

        };

        assertValidate(andClause, checker, false);
    }

    @Test
    public void testCheckSearchRequestOperatorNotValid() throws Exception {
        AndClause andClause = new AndClause(new TerminalClauseImpl(FIELD_NAME, Operator.EQUALS, "value"),
                new TerminalClauseImpl("other", Operator.EQUALS, "valueother"));

        NavigatorStructureChecker checker = new NavigatorStructureChecker<Version>(new ClauseNames(FIELD_NAME), true, fieldFlagOperandRegistry, MockJqlOperandResolver.createSimpleSupport()) {
            @Override
            boolean checkOperator(final Operator operator) {
                return false;
            }

            @Override
            boolean checkOperand(final Operand operand, final boolean acceptFunctions) {
                throw new UnsupportedOperationException();
            }

        };

        assertValidate(andClause, checker, false);
    }

    @Test
    public void testCheckSearchRequestOperandNotValid() throws Exception {
        AndClause andClause = new AndClause(new TerminalClauseImpl(FIELD_NAME, Operator.EQUALS, "value"),
                new TerminalClauseImpl("other", Operator.EQUALS, "valueother"));

        NavigatorStructureChecker checker = new NavigatorStructureChecker<Version>(new ClauseNames(FIELD_NAME), true, fieldFlagOperandRegistry, MockJqlOperandResolver.createSimpleSupport()) {
            @Override
            boolean checkOperator(final Operator operator) {
                return true;
            }

            @Override
            boolean checkOperand(final Operand operand, final boolean acceptFunctions) {
                return false;
            }

        };

        assertValidate(andClause, checker, false);
    }

    private NavigatorStructureChecker<Version> createNavigatorStructureChecker() {
        return new NavigatorStructureChecker<Version>(new ClauseNames(FIELD_NAME), true, fieldFlagOperandRegistry, MockJqlOperandResolver.createSimpleSupport());
    }

    private NavigatorStructureChecker<Version> createNavigatorStructureChecker(final boolean supportMultiLevelFunctions) {
        return new NavigatorStructureChecker<Version>(new ClauseNames(FIELD_NAME), supportMultiLevelFunctions, fieldFlagOperandRegistry, jqlOperandResolver);
    }

    private void assertValidate(final AndClause andClause, final NavigatorStructureChecker checker, final boolean isClauseValid) {
        assertEquals(isClauseValid, checker.checkSearchRequest(new QueryImpl(andClause)));
    }
}
