package com.atlassian.jira.issue.fields.csv;

import com.atlassian.jira.bc.project.component.ProjectComponent;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.export.FieldExportParts;
import com.atlassian.jira.issue.fields.ComponentsSystemField;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.NoopI18nHelper;
import com.google.common.collect.ImmutableList;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static java.util.stream.Collectors.toList;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

/**
 * @since v6.5
 */
public class ComponentsSystemFieldCsvExportTest {
    private static final String COMPONENT1_NAME = "Component1 name";
    private static final String COMPONENT2_NAME = "Component2 name";

    @Rule
    public final RuleChain mockito = MockitoMocksInContainer.forTest(this);

    @Mock
    private Issue issue;
    @Mock
    private ProjectComponent component1;
    @Mock
    private ProjectComponent component2;

    @InjectMocks
    private ComponentsSystemField componentsSystemField;

    @Mock
    JiraAuthenticationContext authenticationContext;

    @Before
    public void setUp() throws Exception {
        when(component1.getName()).thenReturn(COMPONENT1_NAME);
        when(component2.getName()).thenReturn(COMPONENT2_NAME);
        when(authenticationContext.getI18nHelper()).thenReturn(new NoopI18nHelper());
    }

    @Test
    public void testCsvRepresentationForExpectedTypeAndValues() {
        when(issue.getComponents()).thenReturn(ImmutableList.of(component1, component2));

        final FieldExportParts representation = componentsSystemField.getRepresentationFromIssue(issue);
        assertThat(representation.getParts(), hasSize(1));
        assertThat(representation.getParts().get(0).getValues()::iterator, contains(COMPONENT1_NAME, COMPONENT2_NAME));
    }

    @Test
    public void testCsvRepresentationWhenThereAreNoComponents() {
        when(issue.getComponents()).thenReturn(ImmutableList.<ProjectComponent>of());

        final FieldExportParts representation = componentsSystemField.getRepresentationFromIssue(issue);
        assertThat(representation.getParts(), hasSize(1));
        assertThat(representation.getParts().get(0).getValues().collect(toList()), hasSize(0));
    }

    @Test
    public void testCsvRepresentationWhenComponentsAreNull() {
        when(issue.getComponents()).thenReturn(null);

        final FieldExportParts representation = componentsSystemField.getRepresentationFromIssue(issue);
        assertThat(representation.getParts(), hasSize(1));
        assertThat(representation.getParts().get(0).getValues().collect(toList()), hasSize(0));
    }
}
