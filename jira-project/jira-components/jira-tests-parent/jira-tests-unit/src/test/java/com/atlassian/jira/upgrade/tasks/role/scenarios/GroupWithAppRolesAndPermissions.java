package com.atlassian.jira.upgrade.tasks.role.scenarios;

import com.google.common.collect.ImmutableList;
import cucumber.deps.com.thoughtworks.xstream.annotations.XStreamConverter;
import cucumber.runtime.CommaSeparatedStringToListConverter;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;

/**
 * Represents table of groups with multiple application roles, default application roles and permissions. Example table:
 * <pre>
 *      | name      | permissions    | applications     | default applications |
 * #      | users     | USE            | jira-servicedesk | jira-servicedesk     |
 * #      | admins    | ADMINISTER,USE | jira-servicedesk |                      |
 * #      | useAdmins | ADMINISTER     | jira-servicedesk |                      |
 * </pre>
 */
public class GroupWithAppRolesAndPermissions {
    @XStreamConverter(GroupNameConverter.class)
    private final GroupName name;
    @XStreamConverter(CommaSeparatedStringToListConverter.class)
    private final ImmutableList<String> permissions;
    @XStreamConverter(CommaSeparatedStringToListConverter.class)
    private final ImmutableList<String> applications;
    @XStreamConverter(CommaSeparatedStringToListConverter.class)
    private final ImmutableList<String> defaultApplications;

    public GroupWithAppRolesAndPermissions(GroupName name,
                                           final ImmutableList<String> permissions,
                                           final ImmutableList<String> applications,
                                           final ImmutableList<String> defaultApplications) {
        this.name = name;
        this.permissions = permissions;
        this.applications = applications;
        this.defaultApplications = defaultApplications;
    }

    public GroupName getName() {
        return name;
    }

    public List<String> getPermissions() {
        return permissions;
    }

    public List<String> getApplications() {
        return applications;
    }

    public List<String> getDefaultApplications() {
        return defaultApplications;
    }

    public GroupWithAppRolesAndPermissions withPermission(String permission) {
        return new GroupWithAppRolesAndPermissions(this.name, MigrationTestHelper.appendElement(permissions, permission),
                applications, defaultApplications);
    }

    public GroupWithAppRolesAndPermissions withApplication(String application) {
        return new GroupWithAppRolesAndPermissions(this.name, permissions,
                MigrationTestHelper.appendElement(applications, application), defaultApplications);
    }

    public GroupWithAppRolesAndPermissions withDefaultApplication(String defaultApplication) {
        return new GroupWithAppRolesAndPermissions(this.name, permissions,
                applications, MigrationTestHelper.appendElement(defaultApplications, defaultApplication));
    }

    public static GroupWithAppRolesAndPermissions empty(GroupName name) {
        return new GroupWithAppRolesAndPermissions(name, ImmutableList.of(), ImmutableList.of(), ImmutableList.of());
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("name", name)
                .append("permissions", permissions)
                .append("applications", applications)
                .append("defaultApplications", defaultApplications)
                .toString();
    }
}
