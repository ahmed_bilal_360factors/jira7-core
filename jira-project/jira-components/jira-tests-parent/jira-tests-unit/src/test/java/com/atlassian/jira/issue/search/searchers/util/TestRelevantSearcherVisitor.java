package com.atlassian.jira.issue.search.searchers.util;

import com.atlassian.jira.issue.search.managers.SearchHandlerManager;
import com.atlassian.jira.issue.search.searchers.IssueSearcher;
import com.atlassian.jira.issue.search.searchers.MockCustomFieldSearcher;
import com.atlassian.jira.issue.search.searchers.MockIssueSearcher;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.collect.CollectionBuilder;
import com.atlassian.query.clause.AndClause;
import com.atlassian.query.clause.NotClause;
import com.atlassian.query.clause.OrClause;
import com.atlassian.query.clause.TerminalClause;
import com.atlassian.query.clause.TerminalClauseImpl;
import com.atlassian.query.operator.Operator;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestRelevantSearcherVisitor {
    private static final ApplicationUser ANY_USER = null;
    @Rule
    public final MethodRule initMockito = MockitoJUnit.rule();
    @Mock
    private SearchHandlerManager searchHandlerManager;
    private RelevantSearcherVisitor relevantSearcherVisitor;

    @Before
    public void setUp() {
        relevantSearcherVisitor = new RelevantSearcherVisitor(searchHandlerManager, null);
    }

    @Test
    public void singleTerminalWithRelevantSearcher() throws Exception {
        final TerminalClause terminalClause = new TerminalClauseImpl("field", Operator.EQUALS, "value");
        final IssueSearcher issueSearcher = new MockCustomFieldSearcher("1");
        when(searchHandlerManager.getSearchersByClauseName(ANY_USER, "field")).thenReturn(createResult(issueSearcher));

        assertThat(terminalClause.accept(relevantSearcherVisitor), is(true));
        assertThat(relevantSearcherVisitor.getRelevantSearchers(), containsInAnyOrder(issueSearcher));
    }

    @Test
    public void singleTerminalWithNoRelevantSearcher() throws Exception {
        final TerminalClause terminalClause = new TerminalClauseImpl("field", Operator.EQUALS, "value");
        when(searchHandlerManager.getSearchersByClauseName(ANY_USER, "field")).thenReturn(createResult(null));

        assertThat(terminalClause.accept(relevantSearcherVisitor), is(false));
        assertThat(relevantSearcherVisitor.getRelevantSearchers(), empty());
    }

    @Test
    public void singleTerminalWithNoRelevantSearcherBecauseThereAreTwo() throws Exception {
        final TerminalClause terminalClause = new TerminalClauseImpl("field", Operator.EQUALS, "value");
        final List<IssueSearcher<?>> results = CollectionBuilder.<IssueSearcher<?>>newBuilder(new MockIssueSearcher("sweet"), new MockIssueSearcher("dude")).asList();
        when(searchHandlerManager.getSearchersByClauseName(ANY_USER, "field")).thenReturn(results);

        assertThat(terminalClause.accept(relevantSearcherVisitor), is(false));
        assertThat(relevantSearcherVisitor.getRelevantSearchers(), empty());
    }

    @Test
    public void oneLevelAndWithTwoRelevantSearchers() throws Exception {
        final TerminalClauseImpl terminal1 = new TerminalClauseImpl("field1", Operator.EQUALS, "value");
        final TerminalClauseImpl terminal2 = new TerminalClauseImpl("field2", Operator.EQUALS, "value");
        final AndClause andClause = new AndClause(terminal1, terminal2);

        final IssueSearcher issueSearcher1 = new MockCustomFieldSearcher("1");
        final IssueSearcher issueSearcher2 = new MockCustomFieldSearcher("2");

        when(searchHandlerManager.getSearchersByClauseName(ANY_USER, "field1")).thenReturn(createResult(issueSearcher1));
        when(searchHandlerManager.getSearchersByClauseName(ANY_USER, "field2")).thenReturn(createResult(issueSearcher2));

        assertThat(andClause.accept(relevantSearcherVisitor), is(true));
        assertThat(relevantSearcherVisitor.getRelevantSearchers(), containsInAnyOrder(issueSearcher1, issueSearcher2));
    }

    @Test
    public void oneLevelAndWithOneRelevantSearcherForBoth() throws Exception {
        final TerminalClauseImpl terminal1 = new TerminalClauseImpl("field1", Operator.EQUALS, "value");
        final TerminalClauseImpl terminal2 = new TerminalClauseImpl("field2", Operator.EQUALS, "value");
        final AndClause andClause = new AndClause(terminal1, terminal2);

        final IssueSearcher issueSearcher1 = new MockCustomFieldSearcher("1");

        when(searchHandlerManager.getSearchersByClauseName(ANY_USER, "field1")).thenReturn(createResult(issueSearcher1));
        when(searchHandlerManager.getSearchersByClauseName(ANY_USER, "field2")).thenReturn(createResult(issueSearcher1));

        assertThat(andClause.accept(relevantSearcherVisitor), is(true));
        assertThat(relevantSearcherVisitor.getRelevantSearchers(), containsInAnyOrder(issueSearcher1));
    }

    @Test
    public void oneLevelAndWithOneRelevantSearcherAndOneNull() throws Exception {
        final TerminalClauseImpl terminal1 = new TerminalClauseImpl("field1", Operator.EQUALS, "value");
        final TerminalClauseImpl terminal2 = new TerminalClauseImpl("field2", Operator.EQUALS, "value");
        final AndClause andClause = new AndClause(terminal1, terminal2);

        final IssueSearcher issueSearcher1 = new MockCustomFieldSearcher("1");

        when(searchHandlerManager.getSearchersByClauseName(ANY_USER, "field1")).thenReturn(createResult(issueSearcher1));
        when(searchHandlerManager.getSearchersByClauseName(ANY_USER, "field2")).thenReturn(createResult(null));

        assertThat(andClause.accept(relevantSearcherVisitor), is(false));
        assertThat(relevantSearcherVisitor.getRelevantSearchers(), containsInAnyOrder(issueSearcher1));
    }

    @Test
    public void oneLevelOrWithSameFieldTwice() throws Exception {
        final TerminalClauseImpl terminal1 = new TerminalClauseImpl("field", Operator.EQUALS, "value1");
        final TerminalClauseImpl subClause = new TerminalClauseImpl("field", Operator.EQUALS, "value2");
        final NotClause terminal2 = new NotClause(subClause);
        final OrClause orClause = new OrClause(terminal1, terminal2);

        final IssueSearcher issueSearcher1 = new MockCustomFieldSearcher("1");

        when(searchHandlerManager.getSearchersByClauseName(ANY_USER, "field")).thenReturn(createResult(issueSearcher1));

        assertThat(orClause.accept(relevantSearcherVisitor), is(true));
        assertThat(relevantSearcherVisitor.getRelevantSearchers(), containsInAnyOrder(issueSearcher1));
    }

    private Collection<IssueSearcher<?>> createResult(IssueSearcher searcher) {
        if (searcher == null) {
            return Collections.emptyList();
        } else {
            return Collections.<IssueSearcher<?>>singleton(searcher);
        }
    }
}
