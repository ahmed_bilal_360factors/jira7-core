package com.atlassian.jira.user.util;

import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.manager.recovery.RecoveryModeService;
import com.atlassian.jira.matchers.OptionMatchers;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import org.apache.commons.lang3.NotImplementedException;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class DefaultRecoveryModeTest {
    private BeanRecoveryModeService modeService;
    private DefaultRecoveryMode defaultRecoveryMode;

    @Before
    public void setUp() throws Exception {
        modeService = new BeanRecoveryModeService().disable();
        defaultRecoveryMode = new DefaultRecoveryMode(modeService);
    }

    @Test
    public void isRecoveryModeDelegatesToService() {
        assertThat(defaultRecoveryMode.isRecoveryModeOn(), equalTo(false));

        modeService.enable();
        assertThat(defaultRecoveryMode.isRecoveryModeOn(), equalTo(true));
    }

    @Test
    public void isRecoveryUserReturnsFalseAlwaysWhenRecoveryDisabledForApplicationUser() {
        assertThat(defaultRecoveryMode.isRecoveryUser(null), equalTo(false));
        assertThat(defaultRecoveryMode.isRecoveryUser(appUser(modeService.getRecoveryUsername())),
                equalTo(false));
    }

    @Test
    public void isRecoveryUserReturnsFalseAlwaysWhenRecoveryDisabledForUsername() {
        assertThat(defaultRecoveryMode.isRecoveryUsername(null), equalTo(false));
        assertThat(defaultRecoveryMode.isRecoveryUsername(modeService.getRecoveryUsername()),
                equalTo(false));
    }

    @Test
    public void isRecoveryUserReturnsTrueForRecoveryApplicationUser() {
        modeService.enable();

        assertThat(defaultRecoveryMode.isRecoveryUser(null), equalTo(false));
        assertThat(defaultRecoveryMode.isRecoveryUser(appUser(modeService.getRecoveryUsername())),
                equalTo(true));
        assertThat(defaultRecoveryMode.isRecoveryUser(appUser(modeService.getRecoveryUsername() + "1")),
                equalTo(false));
    }

    @Test
    public void getRecoveryUsernameReturnsNoneWhenRecoveryDisabled() {
        assertThat(defaultRecoveryMode.getRecoveryUsername(), OptionMatchers.none());
    }

    @Test
    public void getRecoveryUsernameReturnsSomeWhenRecoveryEnabled() {
        modeService.enable();
        assertThat(defaultRecoveryMode.getRecoveryUsername(), OptionMatchers.some(modeService.getRecoveryUsername()));
    }

    private static ApplicationUser appUser(final String username) {
        return new MockApplicationUser(username);
    }

    private static class BeanRecoveryModeService implements RecoveryModeService {
        private boolean enabled;
        private String recoveryUser = "recovery_user";

        @Override
        public boolean isRecoveryModeOn() {
            return enabled;
        }

        @Override
        public Directory getRecoveryDirectory() {
            throw new NotImplementedException("getRecoveryDirectory not implemented.");
        }

        @Override
        public String getRecoveryUsername() {
            return recoveryUser;
        }

        private BeanRecoveryModeService enable() {
            this.enabled = true;
            return this;
        }

        private BeanRecoveryModeService disable() {
            this.enabled = false;
            return this;
        }

        private BeanRecoveryModeService recoveryUser(String name) {
            this.recoveryUser = name;
            return this;
        }
    }
}