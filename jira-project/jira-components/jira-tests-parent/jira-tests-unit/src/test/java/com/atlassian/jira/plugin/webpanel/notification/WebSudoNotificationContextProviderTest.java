package com.atlassian.jira.plugin.webpanel.notification;

import com.atlassian.jira.help.MockHelpUrl;
import com.atlassian.jira.help.MockHelpUrls;
import com.atlassian.jira.junit.rules.InitMockitoMocks;
import com.atlassian.jira.security.websudo.InternalWebSudoManager;
import com.atlassian.jira.web.HttpServletVariables;
import com.atlassian.jira.web.MockHttpServletVariables;
import com.google.common.collect.ImmutableMap;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;

import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.when;

public class WebSudoNotificationContextProviderTest {
    @Rule
    public InitMockitoMocks mockitoMocks = new InitMockitoMocks(this);
    @Mock
    private InternalWebSudoManager internalWebSudoManager;
    private HttpServletVariables variables = new MockHttpServletVariables();
    private MockHelpUrl webSudoUrl = new MockHelpUrl().setKey("websudo").setUrl("http://example.com/websudo");
    private MockHelpUrls urls = new MockHelpUrls().addUrl(webSudoUrl);
    private WebSudoNotificationContextProvider provider;

    @Before
    public void before() {
        provider = new WebSudoNotificationContextProvider(internalWebSudoManager, urls, variables);
    }

    @Test
    public void checkContextWhenOnAdminPage() {
        //given
        when(internalWebSudoManager.isWebSudoRequest(variables.getHttpRequest())).thenReturn(true);

        //when
        final Map<String, Object> contextMap = provider.getContextMap(ImmutableMap.of());

        //then
        assertThat(contextMap, Matchers.hasEntry("helpUrl", webSudoUrl.getUrl()));
        assertThat(contextMap, Matchers.hasEntry("websudoRequest", true));
    }

    @Test
    public void validContextWhenRegularPage() {
        //given
        when(internalWebSudoManager.isWebSudoRequest(variables.getHttpRequest())).thenReturn(false);

        //when
        final Map<String, Object> contextMap = provider.getContextMap(ImmutableMap.of());

        //then
        assertThat(contextMap, Matchers.hasEntry("helpUrl", webSudoUrl.getUrl()));
        assertThat(contextMap, Matchers.hasEntry("websudoRequest", false));
    }
}