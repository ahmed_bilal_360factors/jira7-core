package com.atlassian.jira.database;

import com.atlassian.jira.config.database.DatabaseConfigurationManager;
import com.querydsl.sql.H2Templates;
import com.querydsl.sql.SQLTemplates;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.ofbiz.core.entity.DelegatorInterface;

import javax.annotation.Nonnull;
import java.util.function.Consumer;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.same;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestBootstrapQueryDslAccessor {
    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private DatabaseConfigurationManager databaseConfigurationManager;

    @Mock
    private DelegatorInterface delegatorInterface;

    @Mock
    private QueryDslAccessor delegate;

    @Test
    public void throwsExceptionWhenDbIsNotConfigured() {
        noDb(db -> db.execute(conn -> fail("Should not get this far")));
        noDb(db -> db.executeQuery(conn -> false));
        noDb(QueryDslAccessor::withNewConnection);
    }

    private void noDb(Consumer<QueryDslAccessor> tester) {
        try {
            tester.accept(fixture());
            fail("Expected an IllegalStateException because the database is not initialized");
        } catch (IllegalStateException ise) {
            assertThat(ise.getMessage(), containsString("database has not been configured"));
        }
    }

    @Test
    public void testExecute() {
        setUpDb();
        final SqlCallback callback = db -> {
        };

        fixture().execute(callback);

        verify(delegate).execute(same(callback));
    }

    @Test
    public void testExecuteQuery() {
        setUpDb();
        final QueryCallback<Boolean> query = db -> true;
        when(delegate.executeQuery(same(query))).thenReturn(true);

        assertThat(fixture().executeQuery(query), is(true));

        verify(delegate).executeQuery(same(query));
    }

    @Nonnull
    private SQLTemplates setUpDb() {
        final SQLTemplates templates = new H2Templates();
        when(databaseConfigurationManager.isDatabaseSetup()).thenReturn(true);
        return templates;
    }

    private QueryDslAccessor fixture() {
        return new BootstrapQueryDslAccessor(databaseConfigurationManager) {
            @Override
            QueryDslAccessor createDelegate() {
                return delegate;
            }
        };
    }
}