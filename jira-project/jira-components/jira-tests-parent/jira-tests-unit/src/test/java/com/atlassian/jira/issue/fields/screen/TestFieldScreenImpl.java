package com.atlassian.jira.issue.fields.screen;

import com.atlassian.jira.mock.ofbiz.MockGenericValue;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.same;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

/**
 * Test for {@link com.atlassian.jira.issue.fields.screen.FieldScreenImpl}.
 *
 * @since v4.1
 */
public class TestFieldScreenImpl {
    private static final String FIELD_ID = "id";
    private static final String DEFAULT_DESCRIPTION = "description";
    private static final String FIELD_DESCRIPTION = DEFAULT_DESCRIPTION;
    private static final String DEFAULT_NAME = "name";
    private static final String FIELD_NAME = DEFAULT_NAME;
    private static final int DEFAULT_ID = 10;

    @Rule
    public final MethodRule initMockito = MockitoJUnit.rule();
    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Mock
    private FieldScreenManager mgr;

    @Test
    public void testCotrNullGenericValue() throws Exception {
        FieldScreenImpl fieldScreen = new FieldScreenImpl(null);
        assertNull(fieldScreen.getDescription());
        assertNull(fieldScreen.getId());
        assertNull(fieldScreen.getName());

        fieldScreen = new FieldScreenImpl(null, null);
        assertNull(fieldScreen.getDescription());
        assertNull(fieldScreen.getId());
        assertNull(fieldScreen.getName());
    }

    @Test
    public void testCotrGenericValue() throws Exception {
        final long id = 100L;
        final String name = DEFAULT_NAME;
        final String description = "mine";

        MockGenericValue mgv = createGv(id, name, description);

        FieldScreenImpl fieldScreen = new FieldScreenImpl(null, mgv);
        assertEquals(Long.valueOf(id), fieldScreen.getId());
        assertEquals(name, fieldScreen.getName());
        assertEquals(description, fieldScreen.getDescription());
        assertFalse(fieldScreen.isModified());
    }

    @Test
    public void testSetIdNoGV() throws Exception {
        FieldScreenImpl fieldScreen = new FieldScreenImpl(null);
        assertNull(fieldScreen.getId());
        fieldScreen.setId(100L);
        assertEquals(100L, (long) fieldScreen.getId());
        fieldScreen.setId(101L);
        assertEquals(101L, (long) fieldScreen.getId());
    }

    @Test
    public void testSetIdWithGV() throws Exception {
        FieldScreenImpl fieldScreen = new FieldScreenImpl(null, new MockGenericValue("testSetIdWithGV"));
        assertNull(fieldScreen.getId());

        exception.expect(IllegalStateException.class);
        exception.reportMissingExceptionWithMessage("Should not be able to set id when GV is present.");
        fieldScreen.setId(100L);
    }

    @Test
    public void testSetNameNoGv() throws Exception {
        FieldScreenImpl fieldScreen = new FieldScreenImpl(null);
        assertNull(fieldScreen.getName());
        assertFalse(fieldScreen.isModified());
        fieldScreen.setName(DEFAULT_NAME);
        assertEquals(DEFAULT_NAME, fieldScreen.getName());
        assertTrue(fieldScreen.isModified());
    }

    @Test
    public void testSetNameGv() throws Exception {
        FieldScreenImpl fieldScreen = new FieldScreenImpl(null, createGv());
        assertEquals(DEFAULT_NAME, fieldScreen.getName());
        assertFalse(fieldScreen.isModified());
        fieldScreen.setName(DEFAULT_NAME);
        assertEquals(DEFAULT_NAME, fieldScreen.getName());
        assertFalse(fieldScreen.isModified());
        assertEquals(DEFAULT_NAME, fieldScreen.getGenericValue().get(FIELD_NAME));

        final String newName = "newName";
        fieldScreen.setName(newName);
        assertEquals(newName, fieldScreen.getName());
        assertTrue(fieldScreen.isModified());
        assertEquals(newName, fieldScreen.getGenericValue().get(FIELD_NAME));

        fieldScreen.setName(null);
        assertNull(fieldScreen.getName());
        assertTrue(fieldScreen.isModified());
        assertNull(fieldScreen.getGenericValue().get(FIELD_NAME));
    }

    @Test
    public void testSetDescriptionNoGv() throws Exception {
        final String desc = "desc";

        FieldScreenImpl fieldScreen = new FieldScreenImpl(null);
        assertNull(fieldScreen.getDescription());
        assertFalse(fieldScreen.isModified());
        fieldScreen.setDescription(desc);

        assertEquals(desc, fieldScreen.getDescription());
        assertTrue(fieldScreen.isModified());
    }

    @Test
    public void testSetDescriptopnGv() throws Exception {

        FieldScreenImpl fieldScreen = new FieldScreenImpl(null, createGv());
        assertEquals(DEFAULT_DESCRIPTION, fieldScreen.getDescription());
        assertFalse(fieldScreen.isModified());
        fieldScreen.setDescription(DEFAULT_DESCRIPTION);
        assertEquals(DEFAULT_DESCRIPTION, fieldScreen.getDescription());
        assertEquals(DEFAULT_DESCRIPTION, fieldScreen.getGenericValue().get(FIELD_DESCRIPTION));
        assertFalse(fieldScreen.isModified());

        final String newdesc = "newdescription";
        fieldScreen.setDescription(newdesc);
        assertEquals(newdesc, fieldScreen.getDescription());
        assertEquals(newdesc, fieldScreen.getGenericValue().get(FIELD_DESCRIPTION));
        assertTrue(fieldScreen.isModified());

        fieldScreen.setDescription(null);
        assertNull(fieldScreen.getDescription());
        assertNull(fieldScreen.getGenericValue().get(FIELD_DESCRIPTION));
        assertTrue(fieldScreen.isModified());
    }

    @Test
    public void testGetTabs() throws Exception {
        List<FieldScreenTab> tabs = Arrays.<FieldScreenTab>asList(new MockFieldScreenTab(), new MockFieldScreenTab());
        final FieldScreenImpl fieldScreen = new FieldScreenImpl(mgr, createGv());
        when(mgr.getFieldScreenTabs(same(fieldScreen))).thenReturn(tabs);
        assertEquals(tabs, fieldScreen.getTabs());
    }

    @Test
    public void testGetTabsPosition() throws Exception {
        // given:
        List<FieldScreenTab> tabs = ImmutableList.of(new MockFieldScreenTab(), new MockFieldScreenTab());
        final FieldScreenImpl fieldScreen = new FieldScreenImpl(mgr, createGv());
        when(mgr.getFieldScreenTabs(same(fieldScreen))).thenReturn(tabs);

        int count = 0;
        for (FieldScreenTab tab : tabs) {
            // when:
            final FieldScreenTab result = fieldScreen.getTab(count++);
            // then:
            assertSame(tab, result);
        }

        // and then expect:
        exception.expect(IndexOutOfBoundsException.class);
        exception.reportMissingExceptionWithMessage("Should not be able to get tabs past the end.");
        // once:
        fieldScreen.getTab(count);
    }

    @Test
    public void testAddTab() throws Exception {
        List<FieldScreenTab> tabs = Lists.newArrayList(new MockFieldScreenTab(), new MockFieldScreenTab());

        final AtomicBoolean storeCalled = new AtomicBoolean(false);
        final AtomicBoolean resequenceCalled = new AtomicBoolean(false);
        final FieldScreenImpl fieldScreen = new FieldScreenImpl(mgr, createGv()) {
            @Override
            public void store() {
                storeCalled.set(true);
            }

            @Override
            public void resequence() {
                resequenceCalled.set(true);
            }
        };
        when(mgr.getFieldScreenTabs(same(fieldScreen))).thenReturn(tabs);

        final FieldScreenTab tab = fieldScreen.addTab("testTab");
        assertEquals("testTab", tab.getName());
        assertEquals(2, tab.getPosition());
        assertSame(fieldScreen, tab.getFieldScreen());
        assertTrue(storeCalled.get());
        assertTrue(resequenceCalled.get());
    }

    @Test
    public void testRemoveTab() throws Exception {
        final StorableTab remainingTab = new StorableTab();
        final StorableTab removedTab = new StorableTab();
        List<FieldScreenTab> tabs = Lists.newArrayList(removedTab, remainingTab);

        final AtomicBoolean storeCalled = new AtomicBoolean(false);
        final AtomicBoolean resequenceCalled = new AtomicBoolean(false);


        final FieldScreenImpl fieldScreen = new FieldScreenImpl(mgr, createGv()) {
            @Override
            public void store() {
                storeCalled.set(true);
            }

            @Override
            public void resequence() {
                resequenceCalled.set(true);
            }
        };
        when(mgr.getFieldScreenTabs(same(fieldScreen))).thenReturn(tabs);

        fieldScreen.removeTab(0);
        assertTrue(storeCalled.get());
        assertTrue(resequenceCalled.get());

        assertEquals(1, removedTab.removeCount);
        assertEquals(ImmutableList.<FieldScreenTab>of(remainingTab), fieldScreen.getTabs());

        exception.expect(IndexOutOfBoundsException.class);
        exception.reportMissingExceptionWithMessage("Should not be able to remove tabs past the end.");
        fieldScreen.removeTab(1);
    }

    @Test
    public void testMoveFieldScreenTabLeft() {
        final StorableTab firstTab = new StorableTab();
        final StorableTab secondTab = new StorableTab();
        List<FieldScreenTab> tabs = new ArrayList<FieldScreenTab>(Arrays.asList(firstTab, secondTab));

        final AtomicBoolean resequenceCalled = new AtomicBoolean(false);

        final FieldScreenImpl fieldScreen = new FieldScreenImpl(mgr, createGv()) {
            @Override
            public void resequence() {
                resequenceCalled.set(true);
            }
        };
        when(mgr.getFieldScreenTabs(same(fieldScreen))).thenReturn(tabs);

        fieldScreen.moveFieldScreenTabLeft(1);
        assertTrue(resequenceCalled.get());

        assertEquals(ImmutableList.<FieldScreenTab>of(secondTab, firstTab), fieldScreen.getTabs());

        exception.expect(IndexOutOfBoundsException.class);
        exception.reportMissingExceptionWithMessage("Should not be able to move tabs past the end.");
        fieldScreen.moveFieldScreenTabLeft(2);
    }

    @Test
    public void testMoveFieldScreenTabRight() {
        final StorableTab firstTab = new StorableTab();
        final StorableTab secondTab = new StorableTab();
        List<FieldScreenTab> tabs = new ArrayList<FieldScreenTab>(Arrays.asList(firstTab, secondTab));

        final AtomicBoolean resequenceCalled = new AtomicBoolean(false);

        final FieldScreenImpl fieldScreen = new FieldScreenImpl(mgr, createGv()) {
            @Override
            public void resequence() {
                resequenceCalled.set(true);
            }
        };
        when(mgr.getFieldScreenTabs(same(fieldScreen))).thenReturn(tabs);

        fieldScreen.moveFieldScreenTabRight(0);
        assertTrue(resequenceCalled.get());

        assertEquals(ImmutableList.<FieldScreenTab>of(secondTab, firstTab), fieldScreen.getTabs());

        exception.reportMissingExceptionWithMessage("Should not be able to move tabs past the end.");
        exception.expect(IndexOutOfBoundsException.class);
        fieldScreen.moveFieldScreenTabRight(2);
    }

    @Test
    public void testResequence() {
        final MockFieldScreenTab firstTab = new MockFieldScreenTab();
        final MockFieldScreenTab secondTab = new MockFieldScreenTab();
        firstTab.setPosition(102020);
        secondTab.setPosition(3985903);

        List<FieldScreenTab> tabs = ImmutableList.of(firstTab, secondTab);
        final FieldScreenImpl fieldScreen = new FieldScreenImpl(mgr, createGv());
        when(mgr.getFieldScreenTabs(same(fieldScreen))).thenReturn(tabs);

        fieldScreen.resequence();

        int count = 0;
        for (FieldScreenTab tab : tabs) {
            assertEquals(count++, tab.getPosition());
        }
    }

    @Test
    public void testStoreNotModified() throws Exception {
        final FieldScreenImpl fieldScreen = new FieldScreenImpl(mgr, createGv());

        fieldScreen.store();
        verifyZeroInteractions(mgr);
    }

    @Test
    public void testStoreNewWithOutTabs() throws Exception {
        final FieldScreenImpl fieldScreen = new FieldScreenImpl(mgr);

        fieldScreen.setModified(true);
        fieldScreen.store();
        verify(mgr).createFieldScreen(same(fieldScreen));
    }

    @Test
    public void testStoreUpdateWithOutTabs() throws Exception {
        final FieldScreenImpl fieldScreen = new FieldScreenImpl(mgr, createGv());

        fieldScreen.setModified(true);
        fieldScreen.store();

        verify(mgr).updateFieldScreen(same(fieldScreen));

    }

    @Test
    public void testStoreWithTabs() throws Exception {
        List<FieldScreenTab> tabs = ImmutableList.of(new StorableTab(), new StorableTab());

        final FieldScreenImpl fieldScreen = new FieldScreenImpl(mgr, createGv());
        when(mgr.getFieldScreenTabs(same(fieldScreen))).thenReturn(tabs);

        fieldScreen.getTabs();
        fieldScreen.store();

        for (FieldScreenTab tab : tabs) {
            final StorableTab storableTab = (StorableTab) tab;
            assertEquals(1, storableTab.storeCount);
            assertEquals(0, storableTab.removeCount);
        }
    }

    @Test
    public void testRemoveTabs() throws Exception {
        List<FieldScreenTab> tabs = ImmutableList.of(new StorableTab(), new StorableTab());

        final FieldScreenImpl fieldScreen = new FieldScreenImpl(mgr, createGv());
        when(mgr.getFieldScreenTabs(same(fieldScreen))).thenReturn(tabs);

        fieldScreen.remove();

        for (FieldScreenTab tab : tabs) {
            verify(mgr).removeFieldScreenLayoutItems(same(tab));
        }
    }

    @Test
    public void testContainsField() throws Exception {
        MockFieldScreenTab tab = new MockFieldScreenTab();
        tab.addFieldScreenLayoutItem("one");
        tab.addFieldScreenLayoutItem("two");

        List<FieldScreenTab> tabs = Collections.singletonList(tab);

        final FieldScreenImpl fieldScreen = new FieldScreenImpl(mgr, createGv());
        when(mgr.getFieldScreenTabs(same(fieldScreen))).thenReturn(tabs);

        assertFalse(fieldScreen.containsField("rjeke"));
        assertTrue(fieldScreen.containsField("one"));
    }

    @Test
    public void testRemoveScreenLayoutItem() throws Exception {
        MockFieldScreenTab tab = new MockFieldScreenTab();
        tab.addFieldScreenLayoutItem("one");
        tab.addFieldScreenLayoutItem("two");

        List<FieldScreenTab> tabs = Collections.singletonList(tab);

        final FieldScreenImpl fieldScreen = new FieldScreenImpl(mgr, createGv());
        when(mgr.getFieldScreenTabs(same(fieldScreen))).thenReturn(tabs);

        fieldScreen.removeFieldScreenLayoutItem("two");
        assertNotNull(tab.getFieldScreenLayoutItem("one"));
        assertNull(tab.getFieldScreenLayoutItem("two"));

        exception.expect(IllegalArgumentException.class);
        fieldScreen.removeFieldScreenLayoutItem("sdkdlsa");
    }

    private MockGenericValue createGv(final long id, final String name, final String description) {
        MockGenericValue mgv = new MockGenericValue("dontCare");
        mgv.set(FIELD_ID, id);
        mgv.set(FIELD_NAME, name);
        mgv.set(FIELD_DESCRIPTION, description);
        return mgv;
    }

    private MockGenericValue createGv() {
        return createGv(DEFAULT_ID, DEFAULT_NAME, DEFAULT_DESCRIPTION);
    }

    private static class StorableTab extends MockFieldScreenTab {
        private int storeCount = 0;
        private int removeCount = 0;

        @Override
        public void store() {
            storeCount++;
        }

        @Override
        public void remove() {
            removeCount++;
        }
    }
}
