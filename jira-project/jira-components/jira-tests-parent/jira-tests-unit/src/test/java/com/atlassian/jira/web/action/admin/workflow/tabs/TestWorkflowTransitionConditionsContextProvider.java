package com.atlassian.jira.web.action.admin.workflow.tabs;

import com.google.common.collect.ImmutableList;
import com.opensymphony.workflow.loader.ActionDescriptor;
import com.opensymphony.workflow.loader.ConditionDescriptor;
import com.opensymphony.workflow.loader.ConditionsDescriptor;
import com.opensymphony.workflow.loader.RestrictionDescriptor;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;
import java.util.Map;

import static com.atlassian.jira.web.action.admin.workflow.tabs.WorkflowTransitionContextUtils.TRANSITION_KEY;
import static java.util.Collections.emptyList;
import static java.util.Collections.emptyMap;
import static java.util.Collections.singletonMap;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestWorkflowTransitionConditionsContextProvider {

    @Mock private ActionDescriptor transition;
    @Mock private ConditionsDescriptor conditionsDescriptor;
    @Mock private RestrictionDescriptor restriction;

    private WorkflowTransitionConditionsContextProvider contextProvider;

    private Map<String, Object> context;

    @Before
    public void setUp() {
        context = singletonMap(TRANSITION_KEY, transition);
        contextProvider = new WorkflowTransitionConditionsContextProvider();
    }

    @Test
    public void shouldGiveZeroCountForEmptyContext() {
        // Invoke
        final int count = contextProvider.getCount(emptyMap());

        // Check
        assertThat(count, is(0));
    }

    @Test
    public void shouldGiveZeroCountWhenRestrictionIsNull() {
        // Set up
        when(transition.getRestriction()).thenReturn(null);

        // Invoke
        final int count = contextProvider.getCount(context);

        // Check
        assertThat(count, is(0));
    }

    @Test
    public void shouldGiveZeroCountWhenRestrictionHasNoConditionsDescriptor() {
        // Set up
        when(transition.getRestriction()).thenReturn(restriction);
        when(restriction.getConditionsDescriptor()).thenReturn(null);

        // Invoke
        final int count = contextProvider.getCount(context);

        // Check
        assertThat(count, is(0));
    }

    @Test
    public void shouldGiveZeroCountWhenRestrictionHasNullConditionsList() {
        // Set up
        when(transition.getRestriction()).thenReturn(restriction);
        when(restriction.getConditionsDescriptor()).thenReturn(conditionsDescriptor);
        when(conditionsDescriptor.getConditions()).thenReturn(null);

        // Invoke
        final int count = contextProvider.getCount(context);

        // Check
        assertThat(count, is(0));
    }

    @Test
    public void shouldGiveZeroCountWhenRestrictionHasEmptyConditionsList() {
        // Set up
        when(transition.getRestriction()).thenReturn(restriction);
        when(restriction.getConditionsDescriptor()).thenReturn(conditionsDescriptor);
        when(conditionsDescriptor.getConditions()).thenReturn(emptyList());

        // Invoke
        final int count = contextProvider.getCount(context);

        // Check
        assertThat(count, is(0));
    }

    @Test
    public void shouldGiveCorrectCountWhenRestrictionHasConditions() {
        // Set up
        when(transition.getRestriction()).thenReturn(restriction);
        when(restriction.getConditionsDescriptor()).thenReturn(conditionsDescriptor);
        final ConditionDescriptor conditionDescriptor1 = mock(ConditionDescriptor.class);
        final ConditionDescriptor conditionDescriptor2 = mock(ConditionDescriptor.class);
        final List<ConditionDescriptor> conditionDescriptors = ImmutableList.of(conditionDescriptor1, conditionDescriptor2);
        when(conditionsDescriptor.getConditions()).thenReturn(conditionDescriptors);

        // Invoke
        final int count = contextProvider.getCount(context);

        // Check
        assertThat(count, is(conditionDescriptors.size()));
    }
}
