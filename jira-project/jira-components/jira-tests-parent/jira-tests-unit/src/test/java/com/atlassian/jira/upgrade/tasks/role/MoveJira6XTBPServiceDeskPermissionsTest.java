package com.atlassian.jira.upgrade.tasks.role;

import com.atlassian.jira.application.ApplicationKeys;
import com.google.common.collect.ImmutableSet;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static com.atlassian.jira.upgrade.tasks.role.TestUtils.emptyState;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

public class MoveJira6XTBPServiceDeskPermissionsTest {
    @Mock
    private UseBasedMigration useMigration;

    @Rule
    public MockitoRule initMocks = MockitoJUnit.rule();

    @Before
    public void setup() {
        initMocks(this);
    }

    @Test
    public void allUseGroupsAreMigratedToAgentRole() {
        //given
        final MigrationState state = emptyState();
        final MoveJira6xTBPServiceDeskPermissions task = new MoveJira6xTBPServiceDeskPermissions(useMigration);

        //when
        task.migrate(state, false);

        //then
        verify(useMigration).addUsePermissionToRoles(state, ImmutableSet.of(ApplicationKeys.SERVICE_DESK));
    }
}