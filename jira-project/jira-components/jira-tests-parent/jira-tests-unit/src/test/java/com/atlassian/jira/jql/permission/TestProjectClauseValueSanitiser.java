package com.atlassian.jira.jql.permission;

import com.atlassian.jira.jql.operand.JqlOperandResolver;
import com.atlassian.jira.jql.resolver.NameResolver;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.query.clause.Clause;
import com.atlassian.query.clause.TerminalClause;
import com.atlassian.query.clause.TerminalClauseImpl;
import com.atlassian.query.operand.MultiValueOperand;
import com.atlassian.query.operand.Operand;
import com.atlassian.query.operand.SingleValueOperand;
import com.atlassian.query.operator.Operator;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertSame;

/**
 * @since v4.0
 */
public class TestProjectClauseValueSanitiser {
    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    private final ApplicationUser theUser = new MockApplicationUser("fred");
    @Mock
    private PermissionManager permissionManager;
    @Mock
    private JqlOperandResolver jqlOperandResolver;
    @Mock
    private NameResolver<Project> projectResolver;

    @Test
    public void testSanitiseOperandDoesNotChange() throws Exception {
        final SingleValueOperand inputOperand = new SingleValueOperand("HSP");
        final TerminalClause clause = new TerminalClauseImpl("project", Operator.EQUALS, inputOperand);

        final ProjectClauseValueSanitiser.ProjectOperandSanitisingVisitor visitor = new ProjectClauseValueSanitiser.ProjectOperandSanitisingVisitor(jqlOperandResolver, projectResolver, permissionManager, theUser, clause) {
            @Override
            public Operand visit(final SingleValueOperand singleValueOperand) {
                assertEquals(inputOperand, singleValueOperand);
                return singleValueOperand;
            }
        };

        final ProjectClauseValueSanitiser sanitiser = new ProjectClauseValueSanitiser(permissionManager, jqlOperandResolver, projectResolver) {
            @Override
            ProjectOperandSanitisingVisitor createOperandVisitor(final ApplicationUser user, final TerminalClause terminalClause) {
                return visitor;
            }
        };

        final Clause result = sanitiser.sanitise(theUser, clause);
        assertSame(result, clause);
    }

    @Test
    public void testSanitiseOperandChangesToSingle() throws Exception {
        final SingleValueOperand inputOperand = new SingleValueOperand("HSP");
        final SingleValueOperand outputOperand = new SingleValueOperand(10000L);

        final TerminalClause clause = new TerminalClauseImpl("project", Operator.EQUALS, inputOperand);
        final TerminalClause expectedClause = new TerminalClauseImpl("project", Operator.EQUALS, outputOperand);

        final ProjectClauseValueSanitiser.ProjectOperandSanitisingVisitor visitor = new ProjectClauseValueSanitiser.ProjectOperandSanitisingVisitor(jqlOperandResolver, projectResolver, permissionManager, theUser, clause) {
            @Override
            public Operand visit(final SingleValueOperand singleValueOperand) {
                assertEquals(inputOperand, singleValueOperand);
                return outputOperand;
            }
        };

        final ProjectClauseValueSanitiser sanitiser = new ProjectClauseValueSanitiser(permissionManager, jqlOperandResolver, projectResolver) {
            @Override
            ProjectOperandSanitisingVisitor createOperandVisitor(final ApplicationUser user, final TerminalClause terminalClause) {
                return visitor;
            }
        };

        final Clause result = sanitiser.sanitise(theUser, clause);
        assertNotSame(result, clause);
        assertEquals(result, expectedClause);
    }

    @Test
    public void testSanitiseOperandChangesToMulti() throws Exception {
        final SingleValueOperand inputOperand = new SingleValueOperand("HSP");
        final SingleValueOperand outputOperand = new SingleValueOperand(10000L);

        final TerminalClause clause = new TerminalClauseImpl("project", Operator.IN, new MultiValueOperand(inputOperand));
        final TerminalClause expectedClause = new TerminalClauseImpl("project", Operator.IN, new MultiValueOperand(outputOperand));

        final ProjectClauseValueSanitiser.ProjectOperandSanitisingVisitor visitor = new ProjectClauseValueSanitiser.ProjectOperandSanitisingVisitor(jqlOperandResolver, projectResolver, permissionManager, theUser, clause) {
            @Override
            public Operand visit(final SingleValueOperand singleValueOperand) {
                assertEquals(inputOperand, singleValueOperand);
                return outputOperand;
            }
        };

        final ProjectClauseValueSanitiser sanitiser = new ProjectClauseValueSanitiser(permissionManager, jqlOperandResolver, projectResolver) {
            @Override
            ProjectOperandSanitisingVisitor createOperandVisitor(final ApplicationUser user, final TerminalClause terminalClause) {
                return visitor;
            }
        };

        final Clause result = sanitiser.sanitise(theUser, clause);
        assertNotSame(result, clause);
        assertEquals(result, expectedClause);
    }
}
