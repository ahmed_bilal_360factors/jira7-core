package com.atlassian.jira.util;

import com.atlassian.cache.memory.MemoryCacheManager;
import com.atlassian.jira.bc.issue.worklog.TimeTrackingConfiguration;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.junit.rules.MockitoContainer;
import com.atlassian.jira.mock.MockApplicationProperties;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.JiraDurationUtils.DaysDurationFormatter;
import com.atlassian.jira.util.JiraDurationUtils.DurationFormatter;
import com.atlassian.jira.util.JiraDurationUtils.HoursDurationFormatter;
import com.atlassian.jira.util.JiraDurationUtils.PrettyDurationFormatter;
import com.atlassian.jira.web.bean.MockI18nBean;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;

import java.math.BigDecimal;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.core.IsInstanceOf.instanceOf;
import static org.junit.Assert.assertThat;

public class TestDefaultDurationFormatterFactory {
    @Rule
    public MockitoContainer mockitoContainer = new MockitoContainer(this);

    @Mock
    private JiraAuthenticationContext authenticationContext;

    private ApplicationProperties applicationProperties;

    private DefaultDurationFormatterProvider durationFormatterFactory;

    @Before
    public void setUp() throws Exception {
        applicationProperties = new MockApplicationProperties();
        final TimeTrackingConfiguration.PropertiesAdaptor timeTrackingConfiguration = new TimeTrackingConfiguration.PropertiesAdaptor(applicationProperties);
        MockI18nBean.MockI18nBeanFactory i18nBeanFactory = new MockI18nBean.MockI18nBeanFactory();
        durationFormatterFactory = new DefaultDurationFormatterProvider(new MemoryCacheManager(), applicationProperties, timeTrackingConfiguration, i18nBeanFactory, authenticationContext);
    }


    @Test
    public void testJiraDurationUtilsPretty() {
        applicationProperties.setString(APKeys.JIRA_TIMETRACKING_HOURS_PER_DAY, "24");
        applicationProperties.setString(APKeys.JIRA_TIMETRACKING_DAYS_PER_WEEK, "7");
        applicationProperties.setString(APKeys.JIRA_TIMETRACKING_FORMAT, JiraDurationUtils.FORMAT_PRETTY);


        DurationFormatter formatter = durationFormatterFactory.getFormatter();
        assertThat(formatter, instanceOf(PrettyDurationFormatter.class));

        PrettyDurationFormatter durationFormatter = (PrettyDurationFormatter) formatter;
        assertThat(durationFormatter.getHoursPerDay(), is(BigDecimal.valueOf(24)));
        assertThat(durationFormatter.getDaysPerWeek(), is(BigDecimal.valueOf(7)));
    }

    @Test
    public void testJiraDurationUtilsDays() {
        applicationProperties.setString(APKeys.JIRA_TIMETRACKING_HOURS_PER_DAY, "24");
        applicationProperties.setString(APKeys.JIRA_TIMETRACKING_DAYS_PER_WEEK, "7");
        applicationProperties.setString(APKeys.JIRA_TIMETRACKING_FORMAT, JiraDurationUtils.FORMAT_DAYS);

        DurationFormatter formatter = durationFormatterFactory.getFormatter();
        assertThat(formatter, instanceOf(DaysDurationFormatter.class));

        DaysDurationFormatter daysDurationFormatterLong = (DaysDurationFormatter) formatter;
        assertThat(daysDurationFormatterLong.getHoursPerDay(), is(BigDecimal.valueOf(24)));
    }

    @Test
    public void testJiraDurationUtilsHours() {
        applicationProperties.setString(APKeys.JIRA_TIMETRACKING_HOURS_PER_DAY, "24");
        applicationProperties.setString(APKeys.JIRA_TIMETRACKING_DAYS_PER_WEEK, "7");
        applicationProperties.setString(APKeys.JIRA_TIMETRACKING_FORMAT, JiraDurationUtils.FORMAT_HOURS);

        assertThat(durationFormatterFactory.getFormatter(), instanceOf(HoursDurationFormatter.class));
    }
}
