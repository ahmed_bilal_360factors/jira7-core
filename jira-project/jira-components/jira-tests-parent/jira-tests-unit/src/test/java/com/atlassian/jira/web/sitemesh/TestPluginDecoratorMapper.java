package com.atlassian.jira.web.sitemesh;

import com.atlassian.core.util.collection.EasyList;
import com.atlassian.jira.plugin.decorator.DecoratorMapperModuleDescriptor;
import com.atlassian.jira.plugin.decorator.DecoratorModuleDescriptor;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.module.ModuleFactory;
import com.mockobjects.servlet.MockHttpServletRequest;
import com.opensymphony.module.sitemesh.Config;
import com.opensymphony.module.sitemesh.Decorator;
import com.opensymphony.module.sitemesh.DecoratorMapper;
import com.opensymphony.module.sitemesh.Page;
import com.opensymphony.module.sitemesh.mapper.DefaultDecorator;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import javax.servlet.http.HttpServletRequest;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestPluginDecoratorMapper {
    @Rule
    public final MethodRule initMockito = MockitoJUnit.rule();

    @Mock
    private PluginAccessor mockPluginAccessor;
    @Mock
    private DecoratorMapper mockParentMapper;
    @Mock
    private DecoratorMapper mockPluginMapper;

    private DecoratorMapperModuleDescriptor mockMapperDescriptor;
    private PluginDecoratorMapper mapper;

    @Before
    public void setUp() throws Exception {
        mapper = new PluginDecoratorMapper() {
            PluginAccessor getPluginAccessor() {
                return mockPluginAccessor;
            }
        };
        mapper.init(null, null, mockParentMapper);

        mockMapperDescriptor = new DecoratorMapperModuleDescriptor(ModuleFactory.LEGACY_MODULE_FACTORY) {
            public DecoratorMapper getDecoratorMapper(Config config, DecoratorMapper parent) {
                return mockPluginMapper;
            }

            public String getPluginKey() {
                return "plugin.key";
            }
        };

    }

    @Test
    public void testNoPlugins() {
        setupPluginAccessorExpectation(DecoratorMapperModuleDescriptor.class, Collections.emptyList());
        setupPluginAccessorExpectation(DecoratorModuleDescriptor.class, Collections.emptyList());
        Decorator decorator = new DefaultDecorator("decorator", "decorator", null);
        setupParentMapperInvocation(decorator);

        assertEquals(decorator, invokeGetDecorator());
    }

    @Test
    public void testMapperPluginMatches() {
        setupPluginAccessorExpectation(DecoratorMapperModuleDescriptor.class, EasyList.build(mockMapperDescriptor));
        Decorator decorator = new DefaultDecorator("decorator", "decorator", null);
        setupPluginMapperInvocation(decorator);

        assertEquals(decorator, invokeGetDecorator());
    }

    @Test
    public void testMapperPluginDoesntMatch() {
        setupPluginAccessorExpectation(DecoratorMapperModuleDescriptor.class, EasyList.build(mockMapperDescriptor));
        setupPluginAccessorExpectation(DecoratorModuleDescriptor.class, Collections.emptyList());
        setupPluginMapperInvocation(null);

        Decorator decorator = new DefaultDecorator("decorator", "decorator", null);
        setupParentMapperInvocation(decorator);

        assertEquals(decorator, invokeGetDecorator());
    }

    @Test
    public void testMapperPluginNull() {
        setupPluginAccessorExpectation(DecoratorMapperModuleDescriptor.class, EasyList.build(new DecoratorMapperModuleDescriptor(ModuleFactory.LEGACY_MODULE_FACTORY) {
            public DecoratorMapper getDecoratorMapper(Config config, DecoratorMapper parent) {
                return null;
            }

            public String getPluginKey() {
                return "plugin.key";
            }
        }));
        setupPluginAccessorExpectation(DecoratorModuleDescriptor.class, Collections.emptyList());
        Decorator decorator = new DefaultDecorator("decorator", "decorator", null);
        setupParentMapperInvocation(decorator);

        assertEquals(decorator, invokeGetDecorator());
    }

    @Test
    public void testDecoratorPluginMatches() {
        setupPluginAccessorExpectation(DecoratorMapperModuleDescriptor.class, Collections.emptyList());
        setupPluginAccessorExpectation(DecoratorModuleDescriptor.class, EasyList.build(createModuleDescriptor("page",
                "p.th")));
        assertEquals("page", invokeGetDecorator().getPage());
    }

    @Test
    public void testDecoratorPluginDoesntMatch() {
        setupPluginAccessorExpectation(DecoratorMapperModuleDescriptor.class, Collections.emptyList());
        setupPluginAccessorExpectation(DecoratorModuleDescriptor.class, EasyList.build(createModuleDescriptor("page",
                "psth")));
        Decorator decorator = new DefaultDecorator("decorator", "decorator", null);
        setupParentMapperInvocation(decorator);

        assertEquals(decorator, invokeGetDecorator());
    }

    @Test
    public void testNamedNoPlugins() {
        setupPluginAccessorExpectation(DecoratorMapperModuleDescriptor.class, Collections.emptyList());
        setupPluginAccessorExpectation(DecoratorModuleDescriptor.class, Collections.emptyList());
        Decorator decorator = new DefaultDecorator("decorator", "decorator", null);
        setupNamedParentMapperInvocation(decorator);

        assertEquals(decorator, invokeGetNamedDecorator());
    }

    @Test
    public void testNamedMapperPluginMatches() {
        setupPluginAccessorExpectation(DecoratorMapperModuleDescriptor.class, EasyList.build(mockMapperDescriptor));
        Decorator decorator = new DefaultDecorator("decorator", "decorator", null);
        setupNamedPluginMapperInvocation(decorator);

        assertEquals(decorator, invokeGetNamedDecorator());
    }

    @Test
    public void testNamedMapperPluginDoesntMatch() {
        setupPluginAccessorExpectation(DecoratorMapperModuleDescriptor.class, EasyList.build(mockMapperDescriptor));
        setupPluginAccessorExpectation(DecoratorModuleDescriptor.class, Collections.emptyList());
        setupNamedPluginMapperInvocation(null);

        Decorator decorator = new DefaultDecorator("decorator", "decorator", null);
        setupNamedParentMapperInvocation(decorator);

        assertEquals(decorator, invokeGetNamedDecorator());
    }

    @Test
    public void testNamedMapperPluginNull() {
        setupPluginAccessorExpectation(DecoratorMapperModuleDescriptor.class, EasyList.build(new DecoratorMapperModuleDescriptor(ModuleFactory.LEGACY_MODULE_FACTORY) {
            public DecoratorMapper getDecoratorMapper(Config config, DecoratorMapper parent) {
                return null;
            }

            public String getPluginKey() {
                return "plugin.key";
            }
        }));
        setupPluginAccessorExpectation(DecoratorModuleDescriptor.class, Collections.emptyList());
        Decorator decorator = new DefaultDecorator("decorator", "decorator", null);
        setupNamedParentMapperInvocation(decorator);

        assertEquals(decorator, invokeGetNamedDecorator());
    }

    @Test
    public void testNamedDecoratorPluginMatches() {
        setupPluginAccessorExpectation(DecoratorMapperModuleDescriptor.class, Collections.emptyList());
        setupPluginAccessorExpectation(DecoratorModuleDescriptor.class, EasyList.build(createModuleDescriptor("page",
                "name")));
        assertEquals("page", invokeGetNamedDecorator().getPage());
    }

    @Test
    public void testNamedDecoratorPluginDoesntMatch() {
        setupPluginAccessorExpectation(DecoratorMapperModuleDescriptor.class, Collections.emptyList());
        setupPluginAccessorExpectation(DecoratorModuleDescriptor.class, EasyList.build(createModuleDescriptor("page",
                "blah")));
        Decorator decorator = new DefaultDecorator("decorator", "decorator", null);
        setupNamedParentMapperInvocation(decorator);

        assertEquals(decorator, invokeGetNamedDecorator());
    }


    private void setupPluginAccessorExpectation(Class clazz, List result) {
        when(mockPluginAccessor.getEnabledModuleDescriptorsByClass(clazz)).thenReturn(result);
    }

    private void setupParentMapperInvocation(Decorator decorator) {
        when(mockParentMapper.getDecorator(any(), any())).thenReturn(decorator);
    }

    private void setupPluginMapperInvocation(Decorator decorator) {
        when(mockPluginMapper.getDecorator(any(), any())).thenReturn(decorator);
    }

    private void setupNamedParentMapperInvocation(Decorator decorator) {
        when(mockParentMapper.getNamedDecorator(any(), any())).thenReturn(decorator);
    }

    private void setupNamedPluginMapperInvocation(Decorator decorator) {
        when(mockPluginMapper.getNamedDecorator(any(), any())).thenReturn(decorator);
    }

    private Decorator invokeGetDecorator() {
        HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getServletPath()).thenReturn("path");
        when(request.getRequestURI()).thenReturn("path");
        when(request.getContextPath()).thenReturn("");
        // This runs the actual test
        return mapper.getDecorator(request, mock(Page.class));
    }

    private Decorator invokeGetNamedDecorator() {
        MockHttpServletRequest request = new MockHttpServletRequest();
        // This runs the actual test
        return mapper.getNamedDecorator(request, "name");
    }

    private DecoratorModuleDescriptor createModuleDescriptor(final String page, final String pattern) {
        return new DecoratorModuleDescriptor(ModuleFactory.LEGACY_MODULE_FACTORY) {
            public Pattern getPattern() {
                return Pattern.compile(pattern);
            }

            public String getPage() {
                return page;
            }

            public String getPluginKey() {
                return "plugin.key";
            }

            public String getName() {
                return pattern;
            }
        };
    }

}
