package com.atlassian.jira.jql.values;

import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import com.google.common.collect.ImmutableList;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Locale;

import static com.atlassian.jira.permission.ProjectPermissions.BROWSE_PROJECTS;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestProjectClauseValuesGenerator {
    @Rule
    public MockitoRule mockito = MockitoJUnit.rule();
    @Mock
    private PermissionManager permissionManager;
    private ProjectClauseValuesGenerator valuesGenerator;

    @Before
    public void setUp() throws Exception {
        valuesGenerator = new ProjectClauseValuesGenerator(permissionManager) {
            @Override
            Locale getLocale(final ApplicationUser searcher) {
                return Locale.ENGLISH;
            }
        };
    }

    @Test
    public void testGetPossibleValuesHappyPath() throws Exception {
        final MockProject type1 = new MockProject(1L, "TST", "Aa it");
        final MockProject type2 = new MockProject(2L, "TST", "A it");
        final MockProject type3 = new MockProject(3L, "TST", "B it");
        final MockProject type4 = new MockProject(4L, "ANA", "C it");

        when(permissionManager.getProjects(BROWSE_PROJECTS, null)).thenReturn(ImmutableList.of(type4, type3, type2, type1));

        final ClauseValuesGenerator.Results possibleValues = valuesGenerator.getPossibleValues(null, "type", "", 5);

        assertEquals(4, possibleValues.getResults().size());
        assertEquals(possibleValues.getResults().get(0), new ClauseValuesGenerator.Result(type1.getName(), new String[]{type1.getName(), " (" + type1.getKey() + ")"}));
        assertEquals(possibleValues.getResults().get(1), new ClauseValuesGenerator.Result(type2.getName(), new String[]{type2.getName(), " (" + type2.getKey() + ")"}));
        assertEquals(possibleValues.getResults().get(2), new ClauseValuesGenerator.Result(type3.getName(), new String[]{type3.getName(), " (" + type3.getKey() + ")"}));
        assertEquals(possibleValues.getResults().get(3), new ClauseValuesGenerator.Result(type4.getName(), new String[]{type4.getName(), " (" + type4.getKey() + ")"}));
    }

    @Test
    public void testGetPossibleValuesMatchFullValue() throws Exception {
        final MockProject type1 = new MockProject(1L, "TST", "Aa it");
        final MockProject type2 = new MockProject(2L, "TST", "A it");
        final MockProject type3 = new MockProject(3L, "TST", "B it");
        final MockProject type4 = new MockProject(4L, "ANA", "C it");

        when(permissionManager.getProjects(BROWSE_PROJECTS, null)).thenReturn(ImmutableList.of(type4, type3, type2, type1));

        final ClauseValuesGenerator.Results possibleValues = valuesGenerator.getPossibleValues(null, "type", "Aa it", 5);

        assertEquals(1, possibleValues.getResults().size());
        assertEquals(possibleValues.getResults().get(0), new ClauseValuesGenerator.Result(type1.getName(), new String[]{type1.getName(), " (" + type1.getKey() + ")"}));
    }

    @Test
    public void testGetPossibleValuesExactMatchWithOthers() throws Exception {
        final MockProject type1 = new MockProject(1L, "TST", "Aa it");
        final MockProject type2 = new MockProject(2L, "TST", "Aa it blah");
        final MockProject type3 = new MockProject(3L, "TST", "B it");
        final MockProject type4 = new MockProject(4L, "ANA", "C it");

        when(permissionManager.getProjects(BROWSE_PROJECTS, null)).thenReturn(ImmutableList.of(type4, type3, type2, type1));

        final ClauseValuesGenerator.Results possibleValues = valuesGenerator.getPossibleValues(null, "type", "Aa it", 5);

        assertEquals(2, possibleValues.getResults().size());
        assertEquals(possibleValues.getResults().get(0), new ClauseValuesGenerator.Result(type1.getName(), new String[]{type1.getName(), " (" + type1.getKey() + ")"}));
        assertEquals(possibleValues.getResults().get(1), new ClauseValuesGenerator.Result(type2.getName(), new String[]{type2.getName(), " (" + type2.getKey() + ")"}));
    }

    @Test
    public void testGetPossibleValuesMatchNone() throws Exception {
        final MockProject type1 = new MockProject(1L, "TST", "Aa it");
        final MockProject type2 = new MockProject(2L, "TST", "A it");
        final MockProject type3 = new MockProject(3L, "TST", "B it");
        final MockProject type4 = new MockProject(4L, "ANA", "C it");

        when(permissionManager.getProjects(BROWSE_PROJECTS, null)).thenReturn(ImmutableList.of(type4, type3, type2, type1));

        final ClauseValuesGenerator.Results possibleValues = valuesGenerator.getPossibleValues(null, "type", "Z", 5);

        assertEquals(0, possibleValues.getResults().size());
    }

    @Test
    public void testGetPossibleValuesMatchSome() throws Exception {
        final MockProject type1 = new MockProject(1L, "TST", "Aa it");
        final MockProject type2 = new MockProject(2L, "TST", "A it");
        final MockProject type3 = new MockProject(3L, "TST", "B it");
        final MockProject type4 = new MockProject(4L, "ZNZ", "C it");

        when(permissionManager.getProjects(BROWSE_PROJECTS, null)).thenReturn(ImmutableList.of(type4, type3, type2, type1));

        final ClauseValuesGenerator.Results possibleValues = valuesGenerator.getPossibleValues(null, "type", "a", 5);

        assertEquals(2, possibleValues.getResults().size());
        assertEquals(possibleValues.getResults().get(0), new ClauseValuesGenerator.Result(type1.getName(), new String[]{type1.getName(), " (" + type1.getKey() + ")"}));
        assertEquals(possibleValues.getResults().get(1), new ClauseValuesGenerator.Result(type2.getName(), new String[]{type2.getName(), " (" + type2.getKey() + ")"}));
    }

    @Test
    public void testGetPossibleValuesMatchSomeAndFromKey() throws Exception {
        final MockProject type1 = new MockProject(1L, "TST", "Aa it");
        final MockProject type2 = new MockProject(2L, "TST", "A it");
        final MockProject type3 = new MockProject(3L, "TST", "B it");
        final MockProject type4 = new MockProject(4L, "ANA", "C it");

        when(permissionManager.getProjects(BROWSE_PROJECTS, null)).thenReturn(ImmutableList.of(type4, type3, type2, type1));

        final ClauseValuesGenerator.Results possibleValues = valuesGenerator.getPossibleValues(null, "type", "a", 5);

        assertEquals(3, possibleValues.getResults().size());
        assertEquals(possibleValues.getResults().get(0), new ClauseValuesGenerator.Result(type1.getName(), new String[]{type1.getName(), " (" + type1.getKey() + ")"}));
        assertEquals(possibleValues.getResults().get(1), new ClauseValuesGenerator.Result(type2.getName(), new String[]{type2.getName(), " (" + type2.getKey() + ")"}));
        assertEquals(possibleValues.getResults().get(2), new ClauseValuesGenerator.Result(type4.getName(), new String[]{type4.getName(), " (" + type4.getKey() + ")"}));
    }

    @Test
    public void testGetPossibleValuesMatchToLimit() throws Exception {
        final MockProject type1 = new MockProject(1L, "TST", "Aa it");
        final MockProject type2 = new MockProject(2L, "TST", "A it");
        final MockProject type3 = new MockProject(3L, "TST", "B it");
        final MockProject type4 = new MockProject(4L, "ANA", "C it");

        when(permissionManager.getProjects(BROWSE_PROJECTS, null)).thenReturn(ImmutableList.of(type4, type3, type2, type1));

        final ClauseValuesGenerator.Results possibleValues = valuesGenerator.getPossibleValues(null, "type", "", 3);

        assertEquals(3, possibleValues.getResults().size());
        assertEquals(possibleValues.getResults().get(0), new ClauseValuesGenerator.Result(type1.getName(), new String[]{type1.getName(), " (" + type1.getKey() + ")"}));
        assertEquals(possibleValues.getResults().get(1), new ClauseValuesGenerator.Result(type2.getName(), new String[]{type2.getName(), " (" + type2.getKey() + ")"}));
        assertEquals(possibleValues.getResults().get(2), new ClauseValuesGenerator.Result(type3.getName(), new String[]{type3.getName(), " (" + type3.getKey() + ")"}));
    }

}
