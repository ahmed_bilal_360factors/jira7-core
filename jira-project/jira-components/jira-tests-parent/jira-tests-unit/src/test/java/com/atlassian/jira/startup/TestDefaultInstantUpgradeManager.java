package com.atlassian.jira.startup;

import com.atlassian.jira.bc.ServiceResult;
import com.atlassian.jira.config.properties.JiraProperties;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockComponentContainer;
import com.atlassian.jira.util.johnson.DefaultJohnsonProvider;
import com.atlassian.jira.util.johnson.JohnsonProvider;
import com.atlassian.johnson.Johnson;
import com.atlassian.johnson.event.Event;
import com.atlassian.johnson.event.EventLevel;
import com.atlassian.johnson.event.EventType;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @since v7.1
 */
@RunWith(MockitoJUnitRunner.class)
public class TestDefaultInstantUpgradeManager {
    @Rule
    public MockComponentContainer container = new MockComponentContainer(this);

    @Mock
    JiraProperties jiraProperties;

    @AvailableInContainer
    JohnsonProvider johnsonProvider = new DefaultJohnsonProvider();

    private InstantUpgradeManager instantUpgradeManager;

    @Before
    public void setUp() {
        Johnson.initialize("test-johnson-config.xml");
        instantUpgradeManager = new DefaultInstantUpgradeManager(jiraProperties);
        when(jiraProperties.getBoolean(InstantUpgradeManager.INSTANT_UPGRADE)).thenReturn(true);
    }

    @After
    public void tearDown() {
        Johnson.terminate();
    }

    @Test
    public void taskIsRunImmediatelyIfInstantUpgradeDisabled() {
        when(jiraProperties.getBoolean(InstantUpgradeManager.INSTANT_UPGRADE)).thenReturn(false);
        final Runnable task = mock(Runnable.class);
        instantUpgradeManager.doNowOrWhenInstanceBecomesActive(task, "description");
        verify(task).run();
    }

    @Test
    public void taskQueuedAndRunWhenInstanceActivated() {
        final Runnable task = mock(Runnable.class);
        instantUpgradeManager.doNowOrWhenInstanceBecomesActive(task, "description");
        verify(task, never()).run();
        ServiceResult result = instantUpgradeManager.activateInstance();
        assertThat(result.isValid(), is(true));
        verify(task).run();
        assertEquals(InstantUpgradeManager.State.FULLY_STARTED, instantUpgradeManager.getState());
    }

    @Test
    public void testGetStateDisabled() {
        when(jiraProperties.getBoolean(InstantUpgradeManager.INSTANT_UPGRADE)).thenReturn(false);
        assertThat(instantUpgradeManager.getState(), is(InstantUpgradeManager.State.DISABLED));
    }

    @Test
    public void testGetStateError() {
        johnsonProvider.getContainer().addEvent(new Event(EventType.get("startup"), "Startup Fatal",
                EventLevel.get(EventLevel.FATAL)));
        assertThat(instantUpgradeManager.getState(), is(InstantUpgradeManager.State.ERROR));
    }

    @Test
    public void testGetStateIsNotErrorIfAllJohnsonEventsAreWarnings() {
        johnsonProvider.getContainer().addEvent(new Event(EventType.get("upgrade"), "JIRA is currently being upgraded",
                EventLevel.get(EventLevel.WARNING)));
        assertThat(instantUpgradeManager.getState(), is(InstantUpgradeManager.State.EARLY_STARTUP));
    }

    @Test
    public void testGetStateStarting() {
        assertThat(instantUpgradeManager.getState(), is(InstantUpgradeManager.State.EARLY_STARTUP));
    }

    @Test
    public void testGetStateWaiting() {
        final Runnable task = mock(Runnable.class);
        instantUpgradeManager.doNowOrWhenInstanceBecomesActive(task, "description");
        assertThat(instantUpgradeManager.getState(), is(InstantUpgradeManager.State.WAITING_FOR_ACTIVATION));
    }
    
    @Test
    public void testGetStateLateStartup() throws InterruptedException {
        CountDownLatch jiraShouldContinueStarting = new CountDownLatch(1);
        CountDownLatch jiraActivated = new CountDownLatch(1);

        final Runnable lateStartupTask = () -> {
            // This tasks just waits for jiraStarted to be notified
            try {
                jiraActivated.countDown();
                jiraShouldContinueStarting.await();
            } catch (InterruptedException e) {
                fail(e.toString());
            }
        };
        instantUpgradeManager.doNowOrWhenInstanceBecomesActive(lateStartupTask, "late startup");
        assertEquals(InstantUpgradeManager.State.WAITING_FOR_ACTIVATION, instantUpgradeManager.getState());

        // Activate the instance in a different thread
        Thread activationThread = new Thread() {
            public void run() {
                instantUpgradeManager.activateInstance();
            }
        };
        activationThread.start();
        
        jiraActivated.await();
        assertEquals(InstantUpgradeManager.State.LATE_STARTUP, instantUpgradeManager.getState());

        // Notify lateStartupTask to finish
        jiraShouldContinueStarting.countDown();
        activationThread.join();
        
        assertEquals(InstantUpgradeManager.State.FULLY_STARTED, instantUpgradeManager.getState());
    }

    @Test
    public void testGetStateActivated() {
        final Runnable task = mock(Runnable.class);
        instantUpgradeManager.doNowOrWhenInstanceBecomesActive(task, "description");
        instantUpgradeManager.activateInstance();
        assertThat(instantUpgradeManager.getState(), is(InstantUpgradeManager.State.FULLY_STARTED));
    }

    @Test
    public void instanceActivationBeforeInstanceUpgradeTaskIsSetReturnsError() {
        ServiceResult result = instantUpgradeManager.activateInstance();
        assertThat(result.isValid(), is(false));
        assertThat(result.getErrorCollection().getErrorMessages().iterator().next(),
                is("Instance upgrade task was not set, ignoring instance activation request."));
    }

    @Test
    public void activatingInstanceWhenUpgradeDisabledReturnsError() {
        when(jiraProperties.getBoolean(InstantUpgradeManager.INSTANT_UPGRADE)).thenReturn(false);
        ServiceResult result = instantUpgradeManager.activateInstance();
        assertThat(result.isValid(), is(false));
        assertThat(result.getErrorCollection().getErrorMessages().iterator().next(),
                is("Instance upgrade is NOT enabled, ignoring instance activation request."));
    }

    @Test
    public void activatingInstanceMultipleTimesReturnsError() {
        final Runnable task = mock(Runnable.class);
        instantUpgradeManager.doNowOrWhenInstanceBecomesActive(task, "description");
        ServiceResult result = instantUpgradeManager.activateInstance();
        assertThat(result.isValid(), is(true));
        result = instantUpgradeManager.activateInstance();
        assertThat(result.isValid(), is(false));
        assertThat(result.getErrorCollection().getErrorMessages().iterator().next(),
                is("Instance is already activated, ignoring instance activation request."));
    }

    @Test(expected = IllegalStateException.class)
    public void onlyAllowedToRegisterOneInstantUpgradeTask() {
        final Runnable task1 = mock(Runnable.class);
        instantUpgradeManager.doNowOrWhenInstanceBecomesActive(task1, "description");
        final Runnable task2 = mock(Runnable.class);
        instantUpgradeManager.doNowOrWhenInstanceBecomesActive(task2, "description");
    }

    @Test
    public void taskExceptionWillTriggerJohnsonEvent() {
        final Runnable task = () -> {
            throw new RuntimeException("mock exception");
        };
        instantUpgradeManager.doNowOrWhenInstanceBecomesActive(task, "description");
        ServiceResult result = instantUpgradeManager.activateInstance();
        assertThat(result.isValid(), is(false));
        assertThat(result.getErrorCollection().getErrorMessages().iterator().next(),
                is("Unexpected exception during JIRA startup. This JIRA instance will not be able to recover. Please check the logs for details."));
    }

    @Test
    public void testWaitTillFullyStarted() throws InterruptedException {
        final Runnable task = mock(Runnable.class);
        instantUpgradeManager.doNowOrWhenInstanceBecomesActive(task, "description");
        boolean fullyStarted;
        fullyStarted = instantUpgradeManager.waitTillFullyStarted(100L, TimeUnit.MILLISECONDS);
        assertThat(fullyStarted, is(false));
        ServiceResult result = instantUpgradeManager.activateInstance();
        assertThat(result.isValid(), is(true));
        fullyStarted = instantUpgradeManager.waitTillFullyStarted(100L, TimeUnit.MILLISECONDS);
        assertThat(fullyStarted, is(true));
    }
}
