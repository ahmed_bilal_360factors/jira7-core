package com.atlassian.jira.jql.query;

import com.atlassian.jira.jql.validator.MockJqlOperandResolver;

/**
 * @since v6.2
 */
public class TestCreatorClauseQueryFactory extends TestUserClauseQueryFactory {

    public TestCreatorClauseQueryFactory() {
        this.fieldNameUnderTest = "creator";
        this.clauseQueryFactory = new CreatorClauseQueryFactory(userResolver, MockJqlOperandResolver.createSimpleSupport());
    }

}
