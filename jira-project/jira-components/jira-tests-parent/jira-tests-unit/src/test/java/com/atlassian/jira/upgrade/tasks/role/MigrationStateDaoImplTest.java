package com.atlassian.jira.upgrade.tasks.role;

import com.atlassian.jira.junit.rules.InitMockitoMocks;
import com.atlassian.jira.test.util.lic.core.CoreLicenses;
import com.atlassian.jira.test.util.lic.sd.ServiceDeskLicenses;
import com.atlassian.jira.test.util.lic.software.SoftwareLicenses;
import com.google.common.collect.ImmutableList;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;

import static com.atlassian.jira.application.ApplicationKeys.CORE;
import static com.atlassian.jira.upgrade.tasks.role.TestUtils.newGroup;
import static com.atlassian.jira.upgrade.tasks.role.TestUtils.toLicense;
import static com.atlassian.jira.upgrade.tasks.role.TestUtils.toLicenses;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class MigrationStateDaoImplTest {
    @Rule
    public InitMockitoMocks mockitoMocks = new InitMockitoMocks(this);

    @Mock
    private LicenseDao licenseDao;

    @Mock
    private ApplicationRolesDao applicationRolesDao;

    @Mock
    private MigrationLogDao migrationLogDao;

    @Mock
    private MigrationLogImpl log;

    private MigrationStateDaoImpl stateDao;

    @Before
    public void setUp() throws Exception {
        stateDao = new MigrationStateDaoImpl(licenseDao, applicationRolesDao, migrationLogDao);
    }

    @Test
    public void getParsesJiraCorrectly() {
        //given
        final License coreLicense = toLicense(CoreLicenses.LICENSE_CORE);
        final License sdLicense = toLicense(ServiceDeskLicenses.LICENSE_SERVICE_DESK_RBP);
        when(licenseDao.getLicenses()).thenReturn(toLicenses(sdLicense, coreLicense));

        final ApplicationRole coreRole = ApplicationRole.forKey(CORE).addGroupAsDefault(newGroup("core"));
        final ApplicationRoles roles = new ApplicationRoles(ImmutableList.of(coreRole));
        when(applicationRolesDao.get()).thenReturn(roles);

        //when
        final MigrationState state = stateDao.get();

        //then
        assertThat(state, new MigrationStateMatcher()
                .licenses(coreLicense, sdLicense)
                .roles(roles));
    }

    @Test
    public void putSavesCorrectly() {
        //given
        final Licenses licenses = toLicenses(SoftwareLicenses.LICENSE_SOFTWARE, CoreLicenses.LICENSE_CORE);
        final ApplicationRole coreRole = ApplicationRole.forKey(CORE).addGroupAsDefault(newGroup("core"));
        final ApplicationRoles roles = new ApplicationRoles(ImmutableList.of(coreRole));
        final MigrationState migrationState = new MigrationState(licenses, roles, ImmutableList.<Runnable>of(), log);

        //when
        stateDao.put(migrationState);

        //then
        verify(licenseDao).setLicenses(licenses);
        verify(applicationRolesDao).put(roles);
        verify(migrationLogDao).write(log);
    }
}