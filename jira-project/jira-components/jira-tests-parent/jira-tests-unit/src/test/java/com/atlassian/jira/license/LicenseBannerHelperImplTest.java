package com.atlassian.jira.license;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.jira.application.ApplicationKeys;
import com.atlassian.jira.mock.MockFeatureManager;
import com.atlassian.jira.mock.security.MockSimpleAuthenticationContext;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.template.soy.SoyTemplateRendererProvider;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.MockUserPropertyManager;
import com.atlassian.jira.util.BaseUrl;
import com.atlassian.soy.renderer.SoyException;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.google.common.collect.ImmutableList;
import com.opensymphony.module.propertyset.PropertySet;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import static com.atlassian.jira.license.LicenseBannerHelperImpl.EXPIRY_KEY;
import static com.atlassian.jira.license.LicenseBannerHelperImpl.MAINTENANCE_KEY;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.collection.IsMapContaining.hasValue;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.argThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Tests {@link com.atlassian.jira.license.LicenseBannerHelperImpl}.
 */
@RunWith(MockitoJUnitRunner.class)
public class LicenseBannerHelperImplTest {
    private static final String TEMPLATES_MODULE = "jira.webresources:soy-templates";
    private static final String SUBSCRIPTION_BANNER = "JIRA.Templates.LicenseBanner.expiryBanner";
    private static final String SUBSCRIPTION_BANNER_MULTIPLE = "JIRA.Templates.LicenseBanner.expiryBannerMultiple";
    private static final String MAINTENANCE_FLAG = "JIRA.Templates.LicenseBanner.maintenanceFlag";
    private static final String EXPIRY_FLAG = "JIRA.Templates.LicenseBanner.expiryFlag";
    private static final String MAINTENANCE_FLAG_MULTIPLE = "JIRA.Templates.LicenseBanner.maintenanceFlagMultiple";
    private static final String EXPIRY_FLAG_MULTIPLE = "JIRA.Templates.LicenseBanner.expiryFlagMultiple";
    private static final String LICENSED_APP_NAME = "JIRA For Mockito";
    private static final String BASE_URL = "base/url";

    private ApplicationUser user = new MockApplicationUser("admin");

    private MockSimpleAuthenticationContext context = MockSimpleAuthenticationContext.createNoopContext(user);
    @Mock
    private GlobalPermissionManager globalPermissionManager;
    private MockUserPropertyManager userPropertyManager = new MockUserPropertyManager();
    @Mock
    private JiraLicenseManager licenseManager;
    @Mock
    private SoyTemplateRendererProvider provider;
    @Mock
    private SoyTemplateRenderer renderer;
    private MockLicenseDetails license = new MockLicenseDetails();
    private LicenseBannerHelperImpl helper;
    private MockFeatureManager featureManager = new MockFeatureManager();
    private MockLicensedApplications singleApplicationAlpha = new MockLicensedApplications(
            ImmutableList.of(ApplicationKey.valueOf("APP.A")));
    private MockLicensedApplications singleApplicationBeta = new MockLicensedApplications(
            ImmutableList.of(ApplicationKey.valueOf("APP.B")));
    private MockLicensedApplications singleApplicationDelta = new MockLicensedApplications(
            ImmutableList.of(ApplicationKey.valueOf("APP.D")));
    private MockLicensedApplications multipleApplications = new MockLicensedApplications(
            ImmutableList.of(ApplicationKey.valueOf("APP.B"), ApplicationKey.valueOf("APP.C")));
    @Mock
    private BaseUrl baseUrl;

    @Before
    public void setup() {
        when(globalPermissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, user)).thenReturn(true);
        when(licenseManager.getLicenses()).thenReturn(ImmutableList.of(license));
        when(licenseManager.isLicenseSet()).thenReturn(true);
        when(provider.getRenderer()).thenReturn(renderer);
        when(baseUrl.getBaseUrl()).thenReturn(BASE_URL);


        license.setBriefDescription(LICENSED_APP_NAME);
        license.setSupportEntitlementNumber("SEN1234");
        license.setLicensedApplications(singleApplicationAlpha);

        userPropertyManager.createOrGetForUser(user);

        helper = new LicenseBannerHelperImpl(context, globalPermissionManager,
                userPropertyManager, licenseManager, provider, baseUrl, featureManager);
    }

    @Test
    public void ensureDatabasePropertyKeysArentChangedLightly() {
        assertThat(EXPIRY_KEY, equalTo("license.expiry.remindme"));
        assertThat(MAINTENANCE_KEY, equalTo("license.maintenance.remindme"));
    }

    @Test
    public void noNotificationsForAnonymous() {
        context.setLoggedInUser(null);
        license.setDaysToLicenseExpiry(1);
        assertNoNotifications();
    }

    @Test
    public void noNotificationsForNonAdmin() {
        when(globalPermissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, user))
                .thenReturn(false);
        license.setDaysToLicenseExpiry(1);
        assertNoNotifications();
    }

    @Test
    public void noNotificationsWhenLicenseNotSet() {
        when(licenseManager.isLicenseSet()).thenReturn(false);
        assertNoNotifications();
    }

    @Test
    public void developerLicenseShowsNotifications() {
        license.setDaysToLicenseExpiry(10).setDeveloper(true);
        assertExpiryFlagPresent(10);
    }

    @Test
    public void noNotificationsForLicenseWith91DaysToExpiry() {
        setExpiryRemindMeForUser(1000);
        license.setDaysToLicenseExpiry(91);
        assertNoNotifications();
        assertThat(getExpiryRemindMeForUser(), Matchers.nullValue());
    }

    @Test
    public void noNotificationsForLicenseWith91DaysToMaintenance() {
        setMaintenanceRemindMeForUser(1000);
        license.setDaysToMaintenanceExpiry(91);

        assertNoNotifications();
        assertThat(getMaintenanceRemindMeForUser(), Matchers.nullValue());
    }

    /**
     * Tests that multiple licenses with > 46 days to maintenance expiry produces no flag.
     */
    @Test
    public void noNotificationsForLicensesWithMoreThan2DaysMaintenance() {
        setupABCLicensesMaintenanceExpiry(91, 92, 93);

        assertNoNotifications();
        assertThat(getMaintenanceRemindMeForUser(), Matchers.nullValue());
    }

    @Test
    public void singleSupportBannerShownWhenOnlyOneLicenseWithLessThan2DaysMaintenance() {
        setupABCLicensesMaintenanceExpiry(99, 99, 1);

        helper.getLicenseFlag();
        assertRenderer(1, MAINTENANCE_FLAG); //second license has multiple apps
        reset(renderer);
        helper.getExpiryBanner();
        Mockito.verifyZeroInteractions(renderer);
    }

    @Test
    public void multiFlagShownWhenOnlyOneLicenseHaving2AppsWithLessThan2DaysMaintenance() {
        LicenseDetails license = new MockLicenseDetails().setDaysToMaintenanceExpiry(1)
                .setBriefDescription("App B").setLicensedApplications(multipleApplications);
        when(licenseManager.getLicenses()).thenReturn(ImmutableList.of(license));

        helper.getLicenseFlag();
        assertRenderer(1, MAINTENANCE_FLAG_MULTIPLE); //second license has multiple apps
        reset(renderer);
        helper.getExpiryBanner();
        Mockito.verifyZeroInteractions(renderer);
    }

    @Test
    public void doubleFlagShownWhenTwoOfThreeLicensesWithLessThan2DaysMaintenance() {
        setupABCLicensesMaintenanceExpiry(1, 99, 1);

        helper.getExpiryBanner();
        Mockito.verifyZeroInteractions(renderer);
        helper.getLicenseFlag();
        assertRenderer(1, MAINTENANCE_FLAG_MULTIPLE);
    }

    @Test
    public void multiFlagShownWhenThreeOfThreeLicensesWithLessThan1DaysMaintenance() {
        setupABCLicensesMaintenanceExpiry(1, 1, 1);

        helper.getExpiryBanner();
        Mockito.verifyZeroInteractions(renderer);
        helper.getLicenseFlag();
        assertRenderer(1, MAINTENANCE_FLAG_MULTIPLE);
    }

    @Test
    public void singleExpiryBannerShownWhenOnlyOneLicenseWithLessThan2DaysUntilExpiry() {
        setupABCLicensesSubscriptionExpiry(1, 2, 99);

        helper.getExpiryBanner();
        assertRenderer(1, SUBSCRIPTION_BANNER);
        reset(renderer);
        helper.getLicenseFlag();
        Mockito.verifyZeroInteractions(renderer);
    }

    @Test
    public void multiExpiryBannerShownWhenOnlyOneLicenseHaving2AppsWithLessThan2DaysUntilExpiry() {
        LicenseDetails license1 = new MockLicenseDetails().setDaysToLicenseExpiry(1)
                .setBriefDescription("App A").setLicensedApplications(multipleApplications);
        when(licenseManager.getLicenses()).thenReturn(ImmutableList.of(license1));

        helper.getExpiryBanner();
        assertRenderer(1, SUBSCRIPTION_BANNER_MULTIPLE);
        reset(renderer);
        helper.getLicenseFlag();
        Mockito.verifyZeroInteractions(renderer);
    }

    @Test
    public void doubleExpiryBannerShownWhenTwoOfThreeLicensesWithLessThan2DaysUntilExpiry() {
        setupABCLicensesSubscriptionExpiry(1, 1, 1);

        helper.getExpiryBanner();
        assertRenderer(1, SUBSCRIPTION_BANNER_MULTIPLE);
        reset(renderer);
        helper.getLicenseFlag();
        Mockito.verifyZeroInteractions(renderer);
    }

    @Test
    public void multiExpiryBannerShownWhenThreeOfThreeLicensesWithLessThan2DaysUntilExpiry() {
        setupABCLicensesSubscriptionExpiry(1, 1, 1);

        helper.getExpiryBanner();
        assertRenderer(1, SUBSCRIPTION_BANNER_MULTIPLE);
        reset(renderer);
        helper.getLicenseFlag();
        Mockito.verifyZeroInteractions(renderer);
    }

    @Test
    public void flagAndBannerCanDisplayAtTheSameTime() {
        LicenseDetails licenseWithMaintenanceExpiry = new MockLicenseDetails().setDaysToLicenseExpiry(99).setDaysToMaintenanceExpiry(5)
                .setBriefDescription("App A").setLicensedApplications(singleApplicationAlpha);
        LicenseDetails licenseWithSubscriptionExpiry = new MockLicenseDetails().setDaysToLicenseExpiry(1)
                .setBriefDescription("App B").setLicensedApplications(singleApplicationBeta);
        LicenseDetails licenseOK = new MockLicenseDetails().setDaysToLicenseExpiry(99)
                .setBriefDescription("App C").setLicensedApplications(singleApplicationDelta);
        when(licenseManager.getLicenses()).thenReturn(ImmutableList.of(licenseWithMaintenanceExpiry,
                licenseWithSubscriptionExpiry, licenseOK));

        helper.getExpiryBanner();
        assertRenderer(1, SUBSCRIPTION_BANNER);
        reset(renderer);
        helper.getLicenseFlag();
        assertRenderer(5, MAINTENANCE_FLAG);
    }

    @Test
    public void flagAndBannerDontDisplayAtTheSameTimeForTheSameLicense() {
        LicenseDetails license = new MockLicenseDetails().setDaysToLicenseExpiry(1).setDaysToMaintenanceExpiry(5)
                .setBriefDescription("App A").setLicensedApplications(singleApplicationAlpha);
        when(licenseManager.getLicenses()).thenReturn(ImmutableList.of(license));

        helper.getExpiryBanner();
        assertRenderer(1, SUBSCRIPTION_BANNER);
        reset(renderer);
        helper.getLicenseFlag();
        Mockito.verifyZeroInteractions(renderer);
    }

    @Test
    public void developerLicensesDisplayFlags() {
        MockLicenseDetails developerLicense = new MockLicenseDetails().setDaysToLicenseExpiry(20).setDeveloper(true);
        developerLicense.setLicensedApplications(new MockLicensedApplications(ApplicationKeys.SOFTWARE));
        when(licenseManager.getLicenses()).thenReturn(ImmutableList.of(developerLicense));

        helper.getLicenseFlag();
        assertRenderer(20, EXPIRY_FLAG);
    }

    @Test
    public void evaluationDoesNotDisplayUntilDay15() {
        MockLicenseDetails evaluationLicense = new MockLicenseDetails().setDaysToLicenseExpiry(20).setEvaluation(true);
        evaluationLicense.setLicensedApplications(new MockLicensedApplications(ApplicationKeys.SOFTWARE));
        when(licenseManager.getLicenses()).thenReturn(ImmutableList.of(evaluationLicense));
        assertNoNotifications();

        evaluationLicense.setDaysToLicenseExpiry(15);
        helper.getLicenseFlag();
        assertRenderer(15, true, EXPIRY_FLAG);
    }

    @Test
    public void evaluationBannerDisplaysOnLastDay() {
        MockLicenseDetails evaluationLicense = new MockLicenseDetails().setDaysToLicenseExpiry(1).setEvaluation(true);
        evaluationLicense.setLicensedApplications(new MockLicensedApplications(ApplicationKeys.SOFTWARE));
        when(licenseManager.getLicenses()).thenReturn(ImmutableList.of(evaluationLicense));

        evaluationLicense.setDaysToLicenseExpiry(1);
        helper.getExpiryBanner();
        assertRenderer(1, true, SUBSCRIPTION_BANNER);
    }

    @Test
    public void remindMeLaterFollowsSecondLicenseScheduleWhenEarlierLicenseIsRenewedForSubscription() {
        setupABCLicensesSubscriptionExpiry(17, 60, 25);
        assertMultipleExpiryFlagPresent(17);

        setExpiryRemindMeForUser(15); // now all flags are suppressed
        assertNoNotifications();

        // set first license to be renewed
        setupABCLicensesSubscriptionExpiry(90, 60, 25);
        assertNoNotifications();

        // now forward to the future and make the second license to match the schedule
        setupABCLicensesSubscriptionExpiry(90, 60, 13);
        assertExpiryFlagPresent(13);
    }

    @Test
    public void remindMeLaterIsResetAfterTwoLicensesAreRenewedWhenTheyExpireOnSameDateForSubscription() {
        setupABCLicensesSubscriptionExpiry(17, 60, 17);
        assertMultipleExpiryFlagPresent(17);

        setExpiryRemindMeForUser(15); // now all banners are suppressed
        assertNoNotifications();

        // set first license to be renewed
        setupABCLicensesSubscriptionExpiry(90, 60, 90);
        assertNoNotifications();

        //now check that the remind me flag was reset to the 1 days
        setupABCLicensesSubscriptionExpiry(55, 2, 55);
        assertExpiryFlagPresent(2);

        //now check that flag becomes banner last day
        setupABCLicensesSubscriptionExpiry(54, 1, 54);
        assertBannerPresent(1);
    }

    @Test
    public void remindMeLaterFollowsSecondLicenseScheduleWhenEarlierLicenseIsRenewedForMaintenance() {
        setupABCLicensesMaintenanceExpiry(17, 60, 25);
        assertMultipleMaintenanceFlagPresent(17);

        setMaintenanceRemindMeForUser(15); // now all banners are suppressed
        assertNoNotifications();

        // set first license to be renewed
        setupABCLicensesMaintenanceExpiry(90, 60, 25);
        assertNoNotifications();

        // now forward to the future and make the second license to match the schedule
        setupABCLicensesMaintenanceExpiry(90, 60, 13);
        assertMaintenanceFlagPresent(13);
    }

    @Test
    public void remindMeLaterIsResetAfterTwoLicensesAreRenewedWhenTheyExpireOnSameDateForMaintenance() {
        setupABCLicensesMaintenanceExpiry(17, 60, 17);
        assertMultipleMaintenanceFlagPresent(17);

        setMaintenanceRemindMeForUser(15); // now all banners are suppressed
        assertNoNotifications();

        // set first license to be renewed
        setupABCLicensesMaintenanceExpiry(90, 60, 90);
        assertNoNotifications();
        // now verify that the remind me flag was reset back to 1 days
        setupABCLicensesMaintenanceExpiry(90, 1, 90);
        assertMaintenanceFlagPresent(1);
    }

    @Test
    public void remindLaterForSubscriptionUntilBannerAppears() {
        setupABCLicensesSubscriptionExpiry(120, 100, 16);
        assertExpiryFlagPresent(16);
        helper.remindMeLater();
        assertNoNotifications();

        setupABCLicensesSubscriptionExpiry(120, 100, 15);
        assertExpiryFlagPresent(15);

        setupABCLicensesSubscriptionExpiry(120, 100, 9);
        assertExpiryFlagPresent(9);
        helper.remindMeLater();
        assertNoNotifications();

        setupABCLicensesSubscriptionExpiry(120, 100, 7);
        assertExpiryFlagPresent(7);
        helper.remindMeLater();
        assertNoNotifications();

        setupABCLicensesSubscriptionExpiry(120, 100, 5);
        assertNoNotifications();

        setupABCLicensesSubscriptionExpiry(120, 100, 1);
        assertBannerPresent(1);


        setupABCLicensesSubscriptionExpiry(120, 100, 0);
        assertBannerPresent(0);

        setupABCLicensesSubscriptionExpiry(120, 100, -7);
        assertBannerPresent(-7);
    }

    @Test
    public void lastReminderForMaintenanceAt7DaysAndThenStopRemindingUntilLicenseRenewal() {
        setupABCLicensesMaintenanceExpiry(120, 100, 9);
        assertMaintenanceFlagPresent(9);
        helper.remindMeLater();
        assertNoNotifications();

        setupABCLicensesMaintenanceExpiry(120, 100, 7);
        assertMaintenanceFlagPresent(7);
        helper.remindMeLater();
        assertNoNotifications();

        setupABCLicensesMaintenanceExpiry(120, 100, 5);
        assertNoNotifications();

        setupABCLicensesMaintenanceExpiry(120, 100, 1);
        assertMaintenanceFlagPresent(1);
        helper.remindMeLater();
        assertNoNotifications();

        setupABCLicensesMaintenanceExpiry(120, 100, 0);
        assertMaintenanceFlagPresent(0);
        helper.remindMeLater();
        assertNoNotifications();

        setupABCLicensesMaintenanceExpiry(120, 100, -7);
        assertMaintenanceFlagPresent(-7);
        helper.remindMeLater();
        assertNoNotifications();

        setupABCLicensesMaintenanceExpiry(120, 100, -14);
        assertNoNotifications();

        //Update licenses
        setupABCLicensesMaintenanceExpiry(120, 100, 110);
        assertNoNotifications();
        //expiry shall work properly
        setupABCLicensesMaintenanceExpiry(85, 100, 110);
        assertMaintenanceFlagPresent(85);
    }

    @Test
    public void bannerDisplayedForLicenseWith1DaysToExpiry() {
        license.setDaysToLicenseExpiry(1);
        assertBannerPresent(1);
    }

    @Test
    public void flagDisplayedForLicenseWith1DaysToMaintenance() {
        license.setDaysToMaintenanceExpiry(1);
        assertMaintenanceFlagPresent(1);
    }

    @Test
    public void flagHiddenForLicenseWith45DaysToExpiryOnRemindMe() {
        assertSubscriptionRemindLater(45);
    }

    @Test
    public void bannerHiddenForLicenseWith45DaysToMaintenanceOnRemindMe() {
        assertMaintenanceRemindLater(45);
    }

    @Test
    public void flagDisplayedForLicenseWith30DaysToExpiry() {
        license.setDaysToLicenseExpiry(30);
        assertExpiryFlagPresent(30);
    }

    @Test
    public void flagDisplayedForLicenseWith30DaysToMaintenance() {
        license.setDaysToMaintenanceExpiry(30);
        assertMaintenanceFlagPresent(30);
    }

    @Test
    public void bannerHiddenForLicenseWith30DaysToExpiryOnRemindMe() {
        assertSubscriptionRemindLater(30);
    }

    @Test
    public void bannerHiddenForLicenseWith30DaysToMaintenanceOnRemindMe() {
        assertMaintenanceRemindLater(30);
    }

    @Test
    public void maintenanceFlagDoesNotDisplayOnRemindMeLater() {
        license.setDaysToLicenseExpiry(15).setDaysToMaintenanceExpiry(15);
        helper.remindMeLater();
        assertNoNotifications();
    }

    @Test
    public void expiryFlagDoesDisplayOnRemindMeLater() {
        license.setDaysToLicenseExpiry(91).setDaysToMaintenanceExpiry(90);
        assertMaintenanceFlagPresent(90);
        helper.remindMeLater();
        license.setDaysToLicenseExpiry(90).setDaysToMaintenanceExpiry(89);
        assertExpiryFlagPresent(90);
    }

    @Test
    public void flagDisplayedForLicenseWith15DaysToExpiry() {
        license.setDaysToLicenseExpiry(15);
        assertExpiryFlagPresent(15);
    }

    @Test
    public void flagDisplayedForLicenseWith15DaysToMaintenance() {
        license.setDaysToMaintenanceExpiry(15);
        assertMaintenanceFlagPresent(15);
    }

    @Test
    public void bannerHiddenForLicenseWith15DaysToMaintenanceOnRemindMe() {
        assertMaintenanceRemindLater(15);
    }

    @Test
    public void bannerHiddenForLicenseWith15DaysToExpiryOnRemindMe() {
        assertSubscriptionRemindLater(15);
    }

    @Test
    public void flagDisplayedForLicenseWith7DaysToExpiry() {
        license.setDaysToLicenseExpiry(7);
        assertExpiryFlagPresent(7);
    }

    @Test
    public void flagDisplayedForLicenseWith7DaysToMaintenance() {
        license.setDaysToMaintenanceExpiry(7);
        assertMaintenanceFlagPresent(7);
    }

    @Test
    public void flagHiddenForLicenseWith7DaysToMaintenanceOnRemindMe() {
        assertMaintenanceRemindLater(7);
    }

    @Test
    public void bannerDisplayedForLicenseWith0DaysToExpiry() {
        license.setDaysToLicenseExpiry(0);
        assertBannerPresent(0);
    }

    @Test
    public void flagDisplayedForLicenseWith0DaysToMaintenance() {
        license.setDaysToMaintenanceExpiry(0);
        assertMaintenanceFlagPresent(0);
    }

    @Test
    public void bannerHiddenForLicenseWith0DaysToMaintenanceOnRemindMe() {
        assertMaintenanceRemindLater(0);
    }

    @Test
    public void bannerDisplayedForLicenseAfterExpiry() {
        license.setDaysToLicenseExpiry(-1);
        assertBannerPresent(-1);
    }

    @Test
    public void flagDisplayedForLicenseAfterMaintenanceExpired() {
        license.setDaysToMaintenanceExpiry(-1);
        assertMaintenanceFlagPresent(-1);
    }

    @Test
    public void bannerNotDisplayedForLicenseAfterMaintenanceOnRemindMe() {
        assertMaintenanceRemindLater(-1);
    }

    @Test
    public void bannerNotDisplayedOnRuntimeException() {
        when(provider.getRenderer()).thenThrow(new RuntimeException("What are you doing."));
        helper.getExpiryBanner();
        Mockito.verifyZeroInteractions(renderer);
        helper.getLicenseFlag();
        Mockito.verifyZeroInteractions(renderer);
    }

    @Test
    public void remindMeLaterIgnoredForAnonymous() {
        context.setLoggedInUser(null);
        //Just make sure no exception occurs.
        helper.remindMeLater();
    }

    @Test
    public void remindMeLaterRemovedForUnsetLicense() {
        setExpiryRemindMeForUser(0);
        setMaintenanceRemindMeForUser(1);

        when(licenseManager.isLicenseSet()).thenReturn(false);
        license.setDaysToLicenseExpiry(30);
        helper.remindMeLater();

        assertThat(getExpiryRemindMeForUser(), Matchers.nullValue());
        assertThat(getMaintenanceRemindMeForUser(), Matchers.nullValue());
    }

    @Test
    public void remindMeLaterExpiryRemovedAt1Day() {
        setExpiryRemindMeForUser(1);
        setMaintenanceRemindMeForUser(0);

        license.setDaysToLicenseExpiry(1);
        helper.remindMeLater();
        assertThat(getExpiryRemindMeForUser(), Matchers.nullValue());
    }

    @Test
    public void remindMeLaterMaintenanceRemovedAt91Days() {
        setExpiryRemindMeForUser(90);
        setMaintenanceRemindMeForUser(90);

        license.setDaysToLicenseExpiry(91);
        helper.remindMeLater();
        assertThat(getExpiryRemindMeForUser(), Matchers.nullValue());
        assertThat(getMaintenanceRemindMeForUser(), Matchers.nullValue());
    }

    @Test
    public void remindMeLaterExpirySetTo45At90Days() {
        assertRemindMeExpiry(90, 45);
    }

    @Test
    public void remindMeLaterExpirySetTo45At85Days() {
        assertRemindMeExpiry(85, 45);
    }

    @Test
    public void remindMeLaterMaintenanceSetTo45At90Days() {
        assertRemindMeMaintenance(90, 45);
    }

    @Test
    public void remindMeLaterMaintenanceSetTo45At46Days() {
        assertRemindMeMaintenance(46, 45);
    }

    @Test
    public void remindMeLaterExpirySetTo30At45Days() {
        assertRemindMeExpiry(45, 30);
    }

    @Test
    public void remindMeLaterExpirySetTo30At40Days() {
        assertRemindMeExpiry(40, 30);
    }

    @Test
    public void remindMeLaterMaintenanceSetTo30At45Days() {
        assertRemindMeMaintenance(45, 30);
    }

    @Test
    public void remindMeLaterMaintenanceSetTo30At31Days() {
        assertRemindMeMaintenance(31, 30);
    }

    @Test
    public void remindMeLaterExpirySetTo15At30Days() {
        assertRemindMeExpiry(30, 15);
    }

    @Test
    public void remindMeLaterExpirySetTo15At25Days() {
        assertRemindMeExpiry(25, 15);
    }

    @Test
    public void remindMeLaterMaintenanceSetTo15At30Days() {
        assertRemindMeMaintenance(30, 15);
    }

    @Test
    public void remindMeLaterMaintenanceSetTo15At25Days() {
        assertRemindMeMaintenance(25, 15);
    }

    @Test
    public void remindMeLaterExpirySetTo7At15Days() {
        assertRemindMeExpiry(15, 7);
    }

    @Test
    public void remindMeLaterExpirySetTo7At10Days() {
        assertRemindMeExpiry(10, 7);
    }

    @Test
    public void remindMeLaterMaintenanceSetTo7At15Days() {
        assertRemindMeMaintenance(15, 7);
    }

    @Test
    public void remindMeLaterMaintenanceSetTo7At13Days() {
        assertRemindMeMaintenance(13, 7);
    }

    @Test
    public void remindMeLaterMaintenanceSetTo0At7Days() {
        assertRemindMeMaintenance(7, 1);
    }

    @Test
    public void remindMeLaterMaintenanceSetTo0At6Days() {
        assertRemindMeMaintenance(6, 1);
    }

    @Test
    public void remindMeLaterMaintenanceSetToMinus7At0Days() {
        assertRemindMeMaintenance(0, -7);
    }

    @Test
    public void remindMeLaterMaintenanceSetToMinus8AtMinus1Days() {
        assertRemindMeMaintenance(-1, -8);
    }

    @Test
    public void remindMeLaterExpiryRemovedAt0Days() {
        license.setDaysToLicenseExpiry(0);
        helper.remindMeLater();
        assertThat(getExpiryRemindMeForUser(), Matchers.nullValue());
    }

    @Test
    public void remindMeLaterExpiryRemovedWhenExpired() {
        license.setDaysToLicenseExpiry(-1);
        helper.remindMeLater();
        assertThat(getExpiryRemindMeForUser(), Matchers.nullValue());
    }

    @Test
    public void clearRemindMeIgnoredForAnonymousUser() {
        context.setLoggedInUser(null);
        //No exceptions occur.
        helper.clearRemindMe();
    }

    @Test
    public void clearRemindMe() {
        setExpiryRemindMeForUser(1);
        setMaintenanceRemindMeForUser(30);

        helper.clearRemindMe();
        assertThat(getExpiryRemindMeForUser(), Matchers.nullValue());
        assertThat(getMaintenanceRemindMeForUser(), Matchers.nullValue());
    }

    private void assertRenderer(int daysToExpire, boolean evaluation, String banner) {
        try {
            verify(renderer, times(1)).render(eq(TEMPLATES_MODULE),
                    eq(banner),
                    argThat(allOf(hasValue((Object) daysToExpire),
                            hasEntry("isEvaluation", (Object) evaluation))));
        } catch (SoyException e) {
            fail("This will never happen as it is a mock function");
        }
    }

    private void assertRenderer(int daysToExpire, String banner) {
        assertRenderer(daysToExpire, false, banner);
    }

    private void setupABCLicensesSubscriptionExpiry(int expiryLicenseA, int expiryLicenseBc, int expiryLicenseD) {
        LicenseDetails license1 = new MockLicenseDetails().setDaysToLicenseExpiry(expiryLicenseA)
                .setBriefDescription("App A").setLicensedApplications(singleApplicationAlpha);
        LicenseDetails license2 = new MockLicenseDetails().setDaysToLicenseExpiry(expiryLicenseBc)
                .setBriefDescription("App B").setLicensedApplications(singleApplicationBeta);
        LicenseDetails license3 = new MockLicenseDetails().setDaysToLicenseExpiry(expiryLicenseD)
                .setBriefDescription("App C").setLicensedApplications(singleApplicationDelta);
        when(licenseManager.getLicenses()).thenReturn(ImmutableList.of(license1, license2, license3));
    }

    private void setupABCLicensesMaintenanceExpiry(int expiryLicenseA, int expiryLicenseBc, int expiryLicenseD) {
        LicenseDetails license1 = new MockLicenseDetails().setDaysToMaintenanceExpiry(expiryLicenseA)
                .setBriefDescription("App A").setLicensedApplications(singleApplicationAlpha);
        LicenseDetails license2 = new MockLicenseDetails().setDaysToMaintenanceExpiry(expiryLicenseBc)
                .setBriefDescription("App B").setLicensedApplications(singleApplicationBeta);
        LicenseDetails license3 = new MockLicenseDetails().setDaysToMaintenanceExpiry(expiryLicenseD)
                .setBriefDescription("App C").setLicensedApplications(singleApplicationDelta);
        when(licenseManager.getLicenses()).thenReturn(ImmutableList.of(license1, license2, license3));
    }

    private void setExpiryRemindMeForUser(int days) {
        userPropertyManager.getPropertySet(user).setInt(EXPIRY_KEY, days);
    }

    private void setMaintenanceRemindMeForUser(int days) {
        userPropertyManager.getPropertySet(user).setInt(MAINTENANCE_KEY, days);
    }

    private Integer getExpiryRemindMeForUser() {
        return getRemindMeForUser(EXPIRY_KEY);
    }

    private Integer getMaintenanceRemindMeForUser() {
        return getRemindMeForUser(MAINTENANCE_KEY);
    }

    private Integer getRemindMeForUser(final String key) {
        PropertySet propertySet = userPropertyManager.getPropertySet(user);
        if (propertySet.exists(key)) {
            return propertySet.getInt(key);
        } else {
            return null;
        }
    }

    private void assertBannerPresent(final int days) {
        helper.getExpiryBanner();
        assertRenderer(days, SUBSCRIPTION_BANNER);
        reset(renderer);
        helper.getLicenseFlag(); // check the flag does not kick in
        Mockito.verifyZeroInteractions(renderer);
    }

    private void assertMaintenanceFlagPresent(final int days) {
        helper.getLicenseFlag();
        assertRenderer(days, MAINTENANCE_FLAG);
        reset(renderer);
        assertNoBanner();
    }

    private void assertExpiryFlagPresent(final int days) {
        reset(renderer);
        helper.getLicenseFlag();
        assertRenderer(days, EXPIRY_FLAG);
        assertNoBanner();
    }

    private void assertMultipleMaintenanceFlagPresent(int days) {
        assertNoBanner();
        helper.getLicenseFlag();
        assertRenderer(days, MAINTENANCE_FLAG_MULTIPLE);
    }

    private void assertMultipleExpiryFlagPresent(int days) {
        assertNoBanner();
        helper.getLicenseFlag();
        assertRenderer(days, EXPIRY_FLAG_MULTIPLE);
    }

    private void assertNoBanner() {
        helper.getExpiryBanner();
        Mockito.verifyZeroInteractions(renderer);
    }

    private void assertSubscriptionRemindLater(final int days) {
        setExpiryRemindMeForUser(days - 1);
        setMaintenanceRemindMeForUser(100); // check also for cross-talk between the two remind me flags
        license.setDaysToLicenseExpiry(days);

        assertNoNotifications();
        assertThat(getExpiryRemindMeForUser(), equalTo(days - 1));
    }

    private void assertMaintenanceRemindLater(final int days) {
        setMaintenanceRemindMeForUser(days - 1);
        setExpiryRemindMeForUser(100); // check also for cross-talk between the two remind me flags
        license.setDaysToMaintenanceExpiry(days);

        assertNoNotifications();
        assertThat(getExpiryRemindMeForUser(), Matchers.nullValue());
        assertThat(getMaintenanceRemindMeForUser(), equalTo(days - 1));
    }

    private void assertRemindMeExpiry(int days, int expectedRemindMe) {
        license.setDaysToLicenseExpiry(days).setDaysToMaintenanceExpiry(0);
        setMaintenanceRemindMeForUser(0);

        helper.remindMeLater();
        assertThat(getExpiryRemindMeForUser(), equalTo(expectedRemindMe));
        assertThat(getMaintenanceRemindMeForUser(), equalTo(expectedRemindMe));
    }

    private void assertRemindMeMaintenance(final int days, final int nextRemind) {
        license.setDaysToMaintenanceExpiry(days);
        setExpiryRemindMeForUser(0);

        helper.remindMeLater();
        assertThat(getExpiryRemindMeForUser(), Matchers.nullValue());
        assertThat(getMaintenanceRemindMeForUser(), equalTo(nextRemind));
    }

    private void assertNoNotifications() {
        reset(renderer);
        helper.getExpiryBanner();
        Mockito.verifyZeroInteractions(renderer);
        helper.getLicenseFlag();
        Mockito.verifyZeroInteractions(renderer);
    }
}