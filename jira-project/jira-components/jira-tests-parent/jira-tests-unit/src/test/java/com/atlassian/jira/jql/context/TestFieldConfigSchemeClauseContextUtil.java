package com.atlassian.jira.jql.context;

import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.config.FieldConfigScheme;
import com.atlassian.jira.issue.fields.config.manager.IssueTypeSchemeManager;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.issue.issuetype.MockIssueType;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectFactory;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.collect.CollectionBuilder;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

import static com.atlassian.jira.permission.ProjectPermissions.BROWSE_PROJECTS;
import static com.google.common.collect.ImmutableList.of;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestFieldConfigSchemeClauseContextUtil {
    @Rule
    public MockitoRule mockito = MockitoJUnit.rule();


    @Mock
    private IssueTypeSchemeManager issueTypeSchemeManager;
    @Mock
    private ConstantsManager constantsManager;
    @Mock
    private PermissionManager permissionManager;
    @Mock
    private ProjectFactory projectFactory;
    private ApplicationUser searcher;
    final Project mockProject1 = new MockProject(10L, "test project1");
    final Project mockProject2 = new MockProject(20L, "test project2");
    private List<Project> projects = ImmutableList.of(mockProject1, mockProject2);

    @Before
    public void setUp() throws Exception {
        searcher = null;
    }

    @Test
    public void testGetFieldConfigSchemeFromContextOneGlobalOneNonGlobalVisible() throws Exception {
        final FieldConfigScheme global = mock(FieldConfigScheme.class);
        final FieldConfigScheme issueType = mock(FieldConfigScheme.class);
        final FieldConfigScheme project = mock(FieldConfigScheme.class);
        final FieldConfigScheme both = mock(FieldConfigScheme.class);

        setExpectations(global, issueType, project, both);
        _testGetMostSpeficScheme(CollectionBuilder.newBuilder(global, issueType).asList(), issueType);

        setExpectations(global, issueType, project, both);
        _testGetMostSpeficScheme(CollectionBuilder.newBuilder(global, project).asList(), project);

        setExpectations(global, issueType, project, both);
        _testGetMostSpeficScheme(CollectionBuilder.newBuilder(project, global).asList(), project);

        setExpectations(global, issueType, project, both);
        _testGetMostSpeficScheme(CollectionBuilder.newBuilder(issueType, global).asList(), issueType);

        setExpectations(global, issueType, project, both);
        _testGetMostSpeficScheme(CollectionBuilder.newBuilder(global, issueType, both, project).asList(), both);

        setExpectations(global, issueType, project, both);
        _testGetMostSpeficScheme(CollectionBuilder.newBuilder(project, global, both).asList(), both);

        setExpectations(global, issueType, project, both);
        _testGetMostSpeficScheme(CollectionBuilder.newBuilder(both, issueType, global).asList(), both);
    }

    private void setExpectations(final FieldConfigScheme global, final FieldConfigScheme issueType, final FieldConfigScheme project, final FieldConfigScheme both) {
        Mockito.reset(global, issueType, project, both);
        when(global.isGlobal()).thenReturn(true);
        when(global.isAllIssueTypes()).thenReturn(true);
        when(global.isAllProjects()).thenReturn(true);

        when(issueType.isGlobal()).thenReturn(false);
        when(issueType.isAllIssueTypes()).thenReturn(false);
        when(issueType.isAllProjects()).thenReturn(true);

        when(project.isGlobal()).thenReturn(false);
        when(project.isAllIssueTypes()).thenReturn(true);
        when(project.isAllProjects()).thenReturn(false);

        when(both.isGlobal()).thenReturn(false);
        when(both.isAllIssueTypes()).thenReturn(false);
        when(both.isAllProjects()).thenReturn(false);
    }

    private void _testGetMostSpeficScheme(List<FieldConfigScheme> schemes, FieldConfigScheme mostSpecific) {
        final CustomField customField = mock(CustomField.class);
        when(customField.getConfigurationSchemes()).thenReturn(schemes);

        QueryContext queryContext = new QueryContextImpl(new ClauseContextImpl());

        final FieldConfigSchemeClauseContextUtil util = new FieldConfigSchemeClauseContextUtil(issueTypeSchemeManager, constantsManager, permissionManager, projectFactory) {
            @Override
            public boolean isConfigSchemeVisibleUnderContext(final QueryContext queryContext, final FieldConfigScheme fieldConfigScheme) {
                return true;
            }
        };

        final FieldConfigScheme result = util.getFieldConfigSchemeFromContext(queryContext, customField);
        assertEquals(mostSpecific, result);
    }

    @Test
    public void testAddProjectIssueTypeContextsForProjectsAllIssueTypes() throws Exception {
        final FieldConfigScheme fieldConfigScheme = mock(FieldConfigScheme.class);
        when(fieldConfigScheme.isAllIssueTypes()).thenReturn(true);

        final FieldConfigSchemeClauseContextUtil util = new FieldConfigSchemeClauseContextUtil(issueTypeSchemeManager, constantsManager, permissionManager, projectFactory);

        final Set<ProjectIssueTypeContext> projectIssueTypeContextSet = util.addProjectIssueTypeContextsForProjects(fieldConfigScheme, projects);

        final Set<ProjectIssueTypeContext> expectedContextSet = CollectionBuilder.<ProjectIssueTypeContext>newBuilder(new ProjectIssueTypeContextImpl(new ProjectContextImpl(mockProject1.getId()), AllIssueTypesContext.INSTANCE),
                new ProjectIssueTypeContextImpl(new ProjectContextImpl(mockProject2.getId()), AllIssueTypesContext.INSTANCE)).asSet();

        assertEquals(expectedContextSet, projectIssueTypeContextSet);
    }

    @Test
    public void testAddProjectIssueTypeContextsForProjectsNullIssueTypes() throws Exception {
        final FieldConfigScheme fieldConfigScheme = mock(FieldConfigScheme.class);
        when(fieldConfigScheme.isAllIssueTypes()).thenReturn(false);
        when(fieldConfigScheme.getAssociatedIssueTypes()).thenReturn(null);

        final FieldConfigSchemeClauseContextUtil util = new FieldConfigSchemeClauseContextUtil(issueTypeSchemeManager, constantsManager, permissionManager, projectFactory);

        final Set<ProjectIssueTypeContext> projectIssueTypeContextSet = util.addProjectIssueTypeContextsForProjects(fieldConfigScheme, projects);

        final Set<ProjectIssueTypeContext> expectedContextSet = CollectionBuilder.<ProjectIssueTypeContext>newBuilder().asSet();

        assertEquals(expectedContextSet, projectIssueTypeContextSet);
    }

    @Test
    public void testAddProjectIssueTypeContextsForProjectsOneProjectAllIssueTypesOtherConstrained() throws Exception {
        final IssueType issueType1 = new MockIssueType("it1", "test type1");
        final IssueType issueType2 = new MockIssueType("it2", "test type2");

        final FieldConfigScheme fieldConfigScheme = mock(FieldConfigScheme.class);
        when(fieldConfigScheme.isAllIssueTypes()).thenReturn(true, false);
        when(fieldConfigScheme.getAssociatedIssueTypes()).thenReturn(of(issueType1));

        when(issueTypeSchemeManager.getIssueTypesForProject(mockProject2)).thenReturn(CollectionBuilder.newBuilder(issueType1, issueType2).asCollection());

        final FieldConfigSchemeClauseContextUtil util = new FieldConfigSchemeClauseContextUtil(issueTypeSchemeManager, constantsManager, permissionManager, projectFactory);

        final Set<ProjectIssueTypeContext> projectIssueTypeContextSet = util.addProjectIssueTypeContextsForProjects(fieldConfigScheme, projects);

        final Set<ProjectIssueTypeContext> expectedContextSet = CollectionBuilder.<ProjectIssueTypeContext>newBuilder(
                new ProjectIssueTypeContextImpl(new ProjectContextImpl(mockProject1.getId()), AllIssueTypesContext.INSTANCE),
                new ProjectIssueTypeContextImpl(new ProjectContextImpl(mockProject2.getId()), new IssueTypeContextImpl("it1"))).asSet();

        assertEquals(expectedContextSet, projectIssueTypeContextSet);
    }

    @Test
    public void testGetClauseContextForFieldContextGlobalProjectIssueTypeConfiguration() throws Exception {
        final FieldConfigScheme fieldConfigScheme = mock(FieldConfigScheme.class);
        when(fieldConfigScheme.isAllProjects()).thenReturn(true);
        when(fieldConfigScheme.isAllIssueTypes()).thenReturn(true);

        final FieldConfigSchemeClauseContextUtil util = new FieldConfigSchemeClauseContextUtil(issueTypeSchemeManager, constantsManager, permissionManager, projectFactory);

        final ClauseContext clauseContext = util.getContextForConfigScheme(null, fieldConfigScheme);

        final ClauseContext expectedContext = ClauseContextImpl.createGlobalClauseContext();

        assertEquals(expectedContext, clauseContext);
    }

    @Test
    public void testGetClauseContextForFieldContextGlobalProjectSpecifiedIssueTypeConfiguration() throws Exception {
        final FieldConfigScheme fieldConfigScheme = mock(FieldConfigScheme.class);
        when(fieldConfigScheme.isAllProjects()).thenReturn(true);
        when(fieldConfigScheme.isAllIssueTypes()).thenReturn(false);

        final IssueType it1 = mock(IssueType.class);
        final IssueType it2 = mock(IssueType.class);

        when(it1.getId()).thenReturn("it1");
        when(it2.getId()).thenReturn("it2");

        when(fieldConfigScheme.getAssociatedIssueTypes()).thenReturn(of(it1, it2));

        final Set<ProjectIssueTypeContext> expectedContextSet = CollectionBuilder.<ProjectIssueTypeContext>newBuilder(
                new ProjectIssueTypeContextImpl(AllProjectsContext.INSTANCE, new IssueTypeContextImpl("it1")),
                new ProjectIssueTypeContextImpl(AllProjectsContext.INSTANCE, new IssueTypeContextImpl("it2"))
        ).asSet();

        final FieldConfigSchemeClauseContextUtil util = new FieldConfigSchemeClauseContextUtil(issueTypeSchemeManager, constantsManager, permissionManager, projectFactory) {
            @Override
            Set<ProjectIssueTypeContext> addProjectIssueTypeContextsForProjects(final FieldConfigScheme fieldConfigScheme, final Collection<Project> associatedProjects) {
                return expectedContextSet;
            }
        };

        final ClauseContext clauseContext = util.getContextForConfigScheme(null, fieldConfigScheme);

        final ClauseContext expectedContext = new ClauseContextImpl(expectedContextSet);

        assertEquals(expectedContext, clauseContext);
    }

    @Test
    public void testGetClauseContextForFieldContextSpecifiedProjectSpecifiedIssueTypeConfiguration() throws Exception {
        final FieldConfigScheme fieldConfigScheme = mock(FieldConfigScheme.class);

        when(fieldConfigScheme.isAllProjects()).thenReturn(false);
        when(permissionManager.getProjects(BROWSE_PROJECTS, searcher)).thenReturn(ImmutableList.of(mockProject1, mockProject2));

        when(permissionManager.getProjects(BROWSE_PROJECTS, searcher)).thenReturn(CollectionBuilder.<Project>newBuilder(mockProject1, mockProject2).asList());

        // dont bother mocking the associated projects return value properly as its passed straight into getprojects
        when(fieldConfigScheme.getAssociatedProjectObjects()).thenReturn(ImmutableList.of(mockProject1));

        // dont bother mocking the associated projects return value properly as its passed straight into getprojects
        when(fieldConfigScheme.getAssociatedProjectObjects()).thenReturn(ImmutableList.of(mockProject1));

        final ProjectIssueTypeContextImpl projectIssueTypeContext = new ProjectIssueTypeContextImpl(new ProjectContextImpl(mockProject1.getId()), new IssueTypeContextImpl("it1"));
        final Set<ProjectIssueTypeContext> expectedContextSet = CollectionBuilder.<ProjectIssueTypeContext>newBuilder(projectIssueTypeContext).asSet();

        final FieldConfigSchemeClauseContextUtil util = new FieldConfigSchemeClauseContextUtil(issueTypeSchemeManager, constantsManager, permissionManager, projectFactory) {
            @Override
            Set<ProjectIssueTypeContext> addProjectIssueTypeContextsForProjects(final FieldConfigScheme fieldConfigScheme, final Collection<Project> associatedProjects) {
                return expectedContextSet;
            }
        };

        final ClauseContext clauseContext = util.getContextForConfigScheme(null, fieldConfigScheme);

        final ClauseContext expectedContext = new ClauseContextImpl(expectedContextSet);

        assertEquals(expectedContext, clauseContext);
    }

    @Test
    public void testGetIssueTypeIdsForScheme() throws Exception {
        final FieldConfigScheme configScheme = mock(FieldConfigScheme.class);
        final IssueType it1 = mock(IssueType.class);
        final IssueType it2 = mock(IssueType.class);

        when(it1.getId()).thenReturn("1");
        when(it2.getId()).thenReturn("2");

        when(configScheme.getAssociatedIssueTypes()).thenReturn(of(it1, it2));

        final FieldConfigSchemeClauseContextUtil schemeClauseContextUtil = new FieldConfigSchemeClauseContextUtil(issueTypeSchemeManager, constantsManager, permissionManager, projectFactory);
        final Set<String> result = schemeClauseContextUtil.getIssueTypeIdsForScheme(configScheme);

        Set<String> expectedResult = ImmutableSet.of("1", "2");
        assertEquals(expectedResult, result);
    }

    @Test
    public void testGetIssueTypeIdsForSchemeNullAssociatedIds() throws Exception {
        final FieldConfigScheme configScheme = mock(FieldConfigScheme.class);
        when(configScheme.getAssociatedIssueTypes()).thenReturn(null);
        final FieldConfigSchemeClauseContextUtil schemeClauseContextUtil = new FieldConfigSchemeClauseContextUtil(issueTypeSchemeManager, constantsManager, permissionManager, projectFactory);
        final Set<String> result = schemeClauseContextUtil.getIssueTypeIdsForScheme(configScheme);

        Set<String> expectedResult = CollectionBuilder.<String>newBuilder().asSet();
        assertEquals(expectedResult, result);
    }

    @Test
    public void testGetProjectIdsForScheme() throws Exception {
        final FieldConfigScheme configScheme = mock(FieldConfigScheme.class);
        final List<Project> projects = Arrays.asList(new MockProject(1), new MockProject(2));

        when(configScheme.getAssociatedProjectObjects()).thenReturn(projects);

        final FieldConfigSchemeClauseContextUtil schemeClauseContextUtil = new FieldConfigSchemeClauseContextUtil(issueTypeSchemeManager, constantsManager, permissionManager, projectFactory);
        final Set<Long> result = schemeClauseContextUtil.getProjectIdsForScheme(configScheme);

        Set<Long> expectedResult = CollectionBuilder.newBuilder(1L, 2L).asSet();
        assertEquals(expectedResult, result);
    }

    @Test
    public void testGetProjectIdsForSchemeNullProjects() throws Exception {
        final FieldConfigScheme configScheme = mock(FieldConfigScheme.class);

        when(configScheme.getAssociatedProjectObjects()).thenReturn(null);

        final FieldConfigSchemeClauseContextUtil schemeClauseContextUtil = new FieldConfigSchemeClauseContextUtil(issueTypeSchemeManager, constantsManager, permissionManager, projectFactory);
        final Set<Long> result = schemeClauseContextUtil.getProjectIdsForScheme(configScheme);

        assertTrue(result.isEmpty());
    }

    @Test
    public void testFieldConfigSchemeContainsContextProjectsUsingAllProjectContext() throws Exception {
        final QueryContextImpl queryContext = new QueryContextImpl(ClauseContextImpl.createGlobalClauseContext());

        final FieldConfigScheme configScheme = mock(FieldConfigScheme.class);

        final AtomicBoolean called = new AtomicBoolean(false);

        final FieldConfigSchemeClauseContextUtil util = new FieldConfigSchemeClauseContextUtil(issueTypeSchemeManager, constantsManager, permissionManager, projectFactory) {
            @Override
            Set<Long> getProjectIdsForScheme(final FieldConfigScheme fieldConfigScheme) {
                called.set(true);
                return Collections.singleton(100L);
            }
        };

        assertFalse(util.fieldConfigSchemeContainsContextProjects(queryContext, configScheme));
        assertTrue(called.get());
    }

    @Test
    public void testFieldConfigSchemeContainsContextProjectsUsingSpecificProjectContextDoesntMatch() throws Exception {
        final ProjectIssueTypeContext projectIssueTypeContext1 = new ProjectIssueTypeContextImpl(new ProjectContextImpl(100L), AllIssueTypesContext.INSTANCE);
        final ProjectIssueTypeContext projectIssueTypeContext2 = new ProjectIssueTypeContextImpl(new ProjectContextImpl(200L), AllIssueTypesContext.INSTANCE);
        final QueryContextImpl queryContext = new QueryContextImpl(new ClauseContextImpl(
                CollectionBuilder.newBuilder(projectIssueTypeContext1, projectIssueTypeContext2).asSet()));

        final FieldConfigScheme configScheme = mock(FieldConfigScheme.class);

        final AtomicBoolean called = new AtomicBoolean(false);

        final FieldConfigSchemeClauseContextUtil util = new FieldConfigSchemeClauseContextUtil(issueTypeSchemeManager, constantsManager, permissionManager, projectFactory) {
            @Override
            Set<Long> getProjectIdsForScheme(final FieldConfigScheme fieldConfigScheme) {
                called.set(true);
                return CollectionBuilder.newBuilder(300L, 400L).asSet();
            }
        };

        assertFalse(util.fieldConfigSchemeContainsContextProjects(queryContext, configScheme));
        assertTrue(called.get());
    }

    @Test
    public void testFieldConfigSchemeContainsContextProjectsUsingSpecificProjectContextMatches() throws Exception {
        final ProjectIssueTypeContext projectIssueTypeContext1 = new ProjectIssueTypeContextImpl(new ProjectContextImpl(100L), AllIssueTypesContext.INSTANCE);
        final ProjectIssueTypeContext projectIssueTypeContext2 = new ProjectIssueTypeContextImpl(new ProjectContextImpl(200L), AllIssueTypesContext.INSTANCE);
        final QueryContextImpl queryContext = new QueryContextImpl(new ClauseContextImpl(
                CollectionBuilder.newBuilder(projectIssueTypeContext1, projectIssueTypeContext2).asSet()));

        final FieldConfigScheme configScheme = mock(FieldConfigScheme.class);

        final AtomicBoolean called = new AtomicBoolean(false);

        final FieldConfigSchemeClauseContextUtil util = new FieldConfigSchemeClauseContextUtil(issueTypeSchemeManager, constantsManager, permissionManager, projectFactory) {
            @Override
            Set<Long> getProjectIdsForScheme(final FieldConfigScheme fieldConfigScheme) {
                called.set(true);
                return CollectionBuilder.newBuilder(200L, 400L).asSet();
            }
        };

        assertTrue(util.fieldConfigSchemeContainsContextProjects(queryContext, configScheme));
        assertTrue(called.get());
    }


    @Test
    public void testFieldConfigSchemeContainsContextIssueTypesUsingAllIssueTypesContext() throws Exception {
        final QueryContextImpl queryContext = new QueryContextImpl(ClauseContextImpl.createGlobalClauseContext());

        final FieldConfigScheme configScheme = mock(FieldConfigScheme.class);

        final AtomicBoolean called = new AtomicBoolean(false);

        final FieldConfigSchemeClauseContextUtil util = new FieldConfigSchemeClauseContextUtil(issueTypeSchemeManager, constantsManager, permissionManager, projectFactory) {
            @Override
            Set<String> getIssueTypeIdsForScheme(final FieldConfigScheme fieldConfigScheme) {
                called.set(true);
                return Collections.singleton("100");
            }
        };

        assertFalse(util.fieldConfigSchemeContainsContextIssueTypes(queryContext, configScheme));
        assertTrue(called.get());

    }

    @Test
    public void testFieldConfigSchemeContainsContextIssueTypesUsingSpecificIssueTypesContextDoesntMatch()
            throws Exception {
        final ProjectIssueTypeContext projectIssueTypeContext1 = new ProjectIssueTypeContextImpl(AllProjectsContext.INSTANCE, new IssueTypeContextImpl("100"));
        final ProjectIssueTypeContext projectIssueTypeContext2 = new ProjectIssueTypeContextImpl(AllProjectsContext.INSTANCE, new IssueTypeContextImpl("200"));
        final QueryContextImpl queryContext = new QueryContextImpl(new ClauseContextImpl(
                CollectionBuilder.newBuilder(projectIssueTypeContext1, projectIssueTypeContext2).asSet()));

        final FieldConfigScheme configScheme = mock(FieldConfigScheme.class);

        final AtomicBoolean called = new AtomicBoolean(false);

        final FieldConfigSchemeClauseContextUtil util = new FieldConfigSchemeClauseContextUtil(issueTypeSchemeManager, constantsManager, permissionManager, projectFactory) {
            @Override
            Set<String> getIssueTypeIdsForScheme(final FieldConfigScheme fieldConfigScheme) {
                called.set(true);
                return CollectionBuilder.newBuilder("300", "400").asSet();
            }
        };

        assertFalse(util.fieldConfigSchemeContainsContextIssueTypes(queryContext, configScheme));
        assertTrue(called.get());

    }

    @Test
    public void testFieldConfigSchemeContainsContextIssueTypesUsingSpecificIssueTypesContextMatches() throws Exception {
        final ProjectIssueTypeContext projectIssueTypeContext1 = new ProjectIssueTypeContextImpl(AllProjectsContext.INSTANCE, new IssueTypeContextImpl("100"));
        final ProjectIssueTypeContext projectIssueTypeContext2 = new ProjectIssueTypeContextImpl(AllProjectsContext.INSTANCE, new IssueTypeContextImpl("200"));
        final QueryContextImpl queryContext = new QueryContextImpl(new ClauseContextImpl(
                CollectionBuilder.newBuilder(projectIssueTypeContext1, projectIssueTypeContext2).asSet()));

        final FieldConfigScheme configScheme = mock(FieldConfigScheme.class);

        final AtomicBoolean called = new AtomicBoolean(false);

        final FieldConfigSchemeClauseContextUtil util = new FieldConfigSchemeClauseContextUtil(issueTypeSchemeManager, constantsManager, permissionManager, projectFactory) {
            @Override
            Set<String> getIssueTypeIdsForScheme(final FieldConfigScheme fieldConfigScheme) {
                called.set(true);
                return CollectionBuilder.newBuilder("200", "400").asSet();
            }
        };

        assertTrue(util.fieldConfigSchemeContainsContextIssueTypes(queryContext, configScheme));
        assertTrue(called.get());

    }

    @Test
    public void testFieldConfigSchemeContainsContextMappingProjectMatchButNoIssueTypeMatch() throws Exception {
        final ProjectIssueTypeContext projectIssueTypeContext1 = new ProjectIssueTypeContextImpl(new ProjectContextImpl(10L), new IssueTypeContextImpl("100"));
        final ProjectIssueTypeContext projectIssueTypeContext2 = new ProjectIssueTypeContextImpl(new ProjectContextImpl(10L), new IssueTypeContextImpl("200"));
        final ProjectIssueTypeContext projectIssueTypeContext3 = new ProjectIssueTypeContextImpl(new ProjectContextImpl(20L), new IssueTypeContextImpl("100"));
        final QueryContextImpl queryContext = new QueryContextImpl(new ClauseContextImpl(
                CollectionBuilder.newBuilder(projectIssueTypeContext1, projectIssueTypeContext2, projectIssueTypeContext3).asSet()));

        final FieldConfigScheme configScheme = mock(FieldConfigScheme.class);


        final AtomicBoolean getProjsCalled = new AtomicBoolean(false);
        final AtomicBoolean getTypesCalled = new AtomicBoolean(false);
        final FieldConfigSchemeClauseContextUtil util = new FieldConfigSchemeClauseContextUtil(issueTypeSchemeManager, constantsManager, permissionManager, projectFactory) {
            @Override
            Set<Long> getProjectIdsForScheme(final FieldConfigScheme fieldConfigScheme) {
                getProjsCalled.set(true);
                return CollectionBuilder.newBuilder(10L, 20L).asSet();
            }

            @Override
            Set<String> getIssueTypeIdsForScheme(final FieldConfigScheme fieldConfigScheme) {
                getTypesCalled.set(true);
                return CollectionBuilder.newBuilder("300", "400").asSet();
            }
        };

        assertFalse(util.fieldConfigSchemeContainsContextMapping(queryContext, configScheme));
        assertTrue(getProjsCalled.get());
        assertTrue(getTypesCalled.get());
    }

    @Test
    public void testFieldConfigSchemeContainsContextMappingMatch() throws Exception {
        final ProjectIssueTypeContext projectIssueTypeContext1 = new ProjectIssueTypeContextImpl(new ProjectContextImpl(10L), new IssueTypeContextImpl("100"));
        final ProjectIssueTypeContext projectIssueTypeContext2 = new ProjectIssueTypeContextImpl(new ProjectContextImpl(10L), new IssueTypeContextImpl("200"));
        final ProjectIssueTypeContext projectIssueTypeContext3 = new ProjectIssueTypeContextImpl(new ProjectContextImpl(20L), new IssueTypeContextImpl("100"));
        final QueryContextImpl queryContext = new QueryContextImpl(new ClauseContextImpl(
                CollectionBuilder.newBuilder(projectIssueTypeContext1, projectIssueTypeContext2, projectIssueTypeContext3).asSet()));

        final FieldConfigScheme configScheme = mock(FieldConfigScheme.class);


        final AtomicBoolean getProjsCalled = new AtomicBoolean(false);
        final AtomicBoolean getTypesCalled = new AtomicBoolean(false);
        final FieldConfigSchemeClauseContextUtil util = new FieldConfigSchemeClauseContextUtil(issueTypeSchemeManager, constantsManager, permissionManager, projectFactory) {
            @Override
            Set<Long> getProjectIdsForScheme(final FieldConfigScheme fieldConfigScheme) {
                getProjsCalled.set(true);
                return CollectionBuilder.newBuilder(10L, 30L).asSet();
            }

            @Override
            Set<String> getIssueTypeIdsForScheme(final FieldConfigScheme fieldConfigScheme) {
                getTypesCalled.set(true);
                return CollectionBuilder.newBuilder("200", "400").asSet();
            }
        };

        assertTrue(util.fieldConfigSchemeContainsContextMapping(queryContext, configScheme));
        assertTrue(getProjsCalled.get());
        assertTrue(getTypesCalled.get());
    }

    @Test
    public void testFieldConfigSchemeContainsContextMappingNoProjectMatch() throws Exception {
        final ProjectIssueTypeContext projectIssueTypeContext1 = new ProjectIssueTypeContextImpl(new ProjectContextImpl(10L), new IssueTypeContextImpl("100"));
        final ProjectIssueTypeContext projectIssueTypeContext2 = new ProjectIssueTypeContextImpl(new ProjectContextImpl(10L), new IssueTypeContextImpl("200"));
        final ProjectIssueTypeContext projectIssueTypeContext3 = new ProjectIssueTypeContextImpl(new ProjectContextImpl(20L), new IssueTypeContextImpl("100"));
        final QueryContextImpl queryContext = new QueryContextImpl(new ClauseContextImpl(
                CollectionBuilder.newBuilder(projectIssueTypeContext1, projectIssueTypeContext2, projectIssueTypeContext3).asSet()));

        final FieldConfigScheme configScheme = mock(FieldConfigScheme.class);


        final AtomicBoolean getProjsCalled = new AtomicBoolean(false);
        final AtomicBoolean getTypesCalled = new AtomicBoolean(false);
        final FieldConfigSchemeClauseContextUtil util = new FieldConfigSchemeClauseContextUtil(issueTypeSchemeManager, constantsManager, permissionManager, projectFactory) {
            @Override
            Set<Long> getProjectIdsForScheme(final FieldConfigScheme fieldConfigScheme) {
                getProjsCalled.set(true);
                return CollectionBuilder.newBuilder(30L, 40L).asSet();
            }

            @Override
            Set<String> getIssueTypeIdsForScheme(final FieldConfigScheme fieldConfigScheme) {
                getTypesCalled.set(true);
                return CollectionBuilder.newBuilder("200", "400").asSet();
            }
        };

        assertFalse(util.fieldConfigSchemeContainsContextMapping(queryContext, configScheme));
        assertTrue(getProjsCalled.get());
        assertTrue(getTypesCalled.get());
    }

    @Test
    public void testIsConfigSchemeVisibleUnderContextGlobal() throws Exception {
        final FieldConfigScheme configScheme = mock(FieldConfigScheme.class);
        when(configScheme.isGlobal()).thenReturn(true);

        final QueryContextImpl context = new QueryContextImpl(new ClauseContextImpl());
        final FieldConfigSchemeClauseContextUtil util = new FieldConfigSchemeClauseContextUtil(issueTypeSchemeManager, constantsManager, permissionManager, projectFactory);
        assertTrue(util.isConfigSchemeVisibleUnderContext(context, configScheme));
    }

    @Test
    public void testIsConfigSchemeVisibleUnderContextAllIssueTypes() throws Exception {
        final FieldConfigScheme configScheme = mock(FieldConfigScheme.class);

        when(configScheme.isGlobal()).thenReturn(false);
        when(configScheme.isAllIssueTypes()).thenReturn(true);

        final QueryContextImpl context = new QueryContextImpl(new ClauseContextImpl());

        final AtomicBoolean projsCalled = new AtomicBoolean(false);
        final FieldConfigSchemeClauseContextUtil util = new FieldConfigSchemeClauseContextUtil(issueTypeSchemeManager, constantsManager, permissionManager, projectFactory) {
            @Override
            boolean fieldConfigSchemeContainsContextProjects(final QueryContext queryContext, final FieldConfigScheme fieldConfigScheme) {
                projsCalled.set(true);
                return true;
            }
        };

        assertTrue(util.isConfigSchemeVisibleUnderContext(context, configScheme));
        assertTrue(projsCalled.get());
    }

    @Test
    public void testIsConfigSchemeVisibleUnderContextAllProjects() throws Exception {
        final FieldConfigScheme configScheme = mock(FieldConfigScheme.class);

        when(configScheme.isGlobal()).thenReturn(false);
        when(configScheme.isAllIssueTypes()).thenReturn(false);
        when(configScheme.isAllProjects()).thenReturn(true);

        final QueryContextImpl context = new QueryContextImpl(new ClauseContextImpl());

        final AtomicBoolean projsCalled = new AtomicBoolean(false);
        final AtomicBoolean typesCalled = new AtomicBoolean(false);
        final FieldConfigSchemeClauseContextUtil util = new FieldConfigSchemeClauseContextUtil(issueTypeSchemeManager, constantsManager, permissionManager, projectFactory) {
            @Override
            boolean fieldConfigSchemeContainsContextProjects(final QueryContext queryContext, final FieldConfigScheme fieldConfigScheme) {
                projsCalled.set(true);
                return true;
            }

            @Override
            boolean fieldConfigSchemeContainsContextIssueTypes(final QueryContext queryContext, final FieldConfigScheme fieldConfigScheme) {
                typesCalled.set(true);
                return true;
            }
        };

        assertTrue(util.isConfigSchemeVisibleUnderContext(context, configScheme));
        assertFalse(projsCalled.get());
        assertTrue(typesCalled.get());
    }

    @Test
    public void testIsConfigSchemeVisibleUnderContextNotAll() throws Exception {
        final FieldConfigScheme configScheme = mock(FieldConfigScheme.class);

        when(configScheme.isGlobal()).thenReturn(false);
        when(configScheme.isAllIssueTypes()).thenReturn(false);
        when(configScheme.isAllProjects()).thenReturn(false);

        final QueryContextImpl context = new QueryContextImpl(new ClauseContextImpl());

        final AtomicBoolean projsCalled = new AtomicBoolean(false);
        final AtomicBoolean typesCalled = new AtomicBoolean(false);
        final AtomicBoolean mappingCalled = new AtomicBoolean(false);
        final FieldConfigSchemeClauseContextUtil util = new FieldConfigSchemeClauseContextUtil(issueTypeSchemeManager, constantsManager, permissionManager, projectFactory) {
            @Override
            boolean fieldConfigSchemeContainsContextProjects(final QueryContext queryContext, final FieldConfigScheme fieldConfigScheme) {
                projsCalled.set(true);
                return true;
            }

            @Override
            boolean fieldConfigSchemeContainsContextIssueTypes(final QueryContext queryContext, final FieldConfigScheme fieldConfigScheme) {
                typesCalled.set(true);
                return true;
            }

            @Override
            boolean fieldConfigSchemeContainsContextMapping(final QueryContext queryContext, final FieldConfigScheme fieldConfigScheme) {
                mappingCalled.set(true);
                return true;
            }
        };

        assertTrue(util.isConfigSchemeVisibleUnderContext(context, configScheme));
        assertFalse(projsCalled.get());
        assertFalse(typesCalled.get());
        assertTrue(mappingCalled.get());
    }

    @Test
    public void testAddProjectIssueTypeContextsForIssueTypesOnlyNullIssueTypes() throws Exception {
        final FieldConfigScheme fieldConfigScheme = mock(FieldConfigScheme.class);
        when(fieldConfigScheme.getAssociatedIssueTypes()).thenReturn(null);

        final FieldConfigSchemeClauseContextUtil util = new FieldConfigSchemeClauseContextUtil(issueTypeSchemeManager, constantsManager, permissionManager, projectFactory);
        final Set<ProjectIssueTypeContext> contextSet = util.addProjectIssueTypeContextsForIssueTypesOnly(fieldConfigScheme);
        assertTrue(contextSet.isEmpty());
    }
}
