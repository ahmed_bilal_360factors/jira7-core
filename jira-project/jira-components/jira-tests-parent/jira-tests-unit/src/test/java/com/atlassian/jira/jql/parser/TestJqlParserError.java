package com.atlassian.jira.jql.parser;

import com.atlassian.jira.jql.parser.antlr.JqlLexer;
import com.atlassian.jira.jql.util.JqlStringSupportImpl;
import com.atlassian.jira.util.collect.CollectionBuilder;
import com.atlassian.query.Query;
import org.antlr.runtime.CommonToken;
import org.antlr.runtime.Token;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.atlassian.jira.jql.parser.JqlQueryParserConstants.ILLEGAL_CHARS_STRING;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * @since v4.0
 */
@RunWith(Parameterized.class)
public class TestJqlParserError {
    //Set of reserved words that the grammar currently parses.
    private static final Set<String> currentWords = CollectionBuilder.newBuilder("empty", "null", "and", "or", "not", "in", "is", "cf", "issue.property", "order", "by", "desc", "asc", "on", "before", "to", "after", "from", "was", "changed").asSet();

    @Parameterized.Parameters(name = "{0}")
    public static List<Object[]> data() {
        final DataBuilder data = new DataBuilder();

        final BigInteger tooBigNumber = BigInteger.valueOf(Long.MAX_VALUE).add(BigInteger.TEN);

        //Some tests for invalid parsing input.
        {
            data.addException("Tabs and Spaces String", "  \t  \n\r\f   ", JqlParseErrorMessages.illegalCharacter('\f', 2, 1));
            data.addException("Bare field", "foo", JqlParseErrorMessages.badOperator(createEOFToken(1, 3)));
            data.addException("Missing operand", "foo=", JqlParseErrorMessages.badOperand(createEOFToken(1, 4)));
            data.addException("Just an operator", "=", JqlParseErrorMessages.badFieldName(createToken("=", 1, 0)));
            data.addException("Just a two character operator", "!=", JqlParseErrorMessages.badFieldName(createToken("!=", 1, 0)));
            data.addException("Missing operator in first subclause of otherwise OK composite logical clause", "foo bar and 78foo = bar", JqlParseErrorMessages.badOperator(createToken("bar", 1, 4)));
            data.addException("Two logical operators only", "and and", JqlParseErrorMessages.badFieldName(createToken("and", 1, 0)));
            data.addException("missing logical operator", "a=b a=b", JqlParseErrorMessages.needLogicalOperator(createToken("a", 1, 4)));
            data.addException("Legal subclause with missing second subclause", "foo=bar and", JqlParseErrorMessages.badFieldName(createEOFToken(1, 11)));
            data.addException("Legal subclause with malformed second subclause", "foo=bar and and", JqlParseErrorMessages.badFieldName(createToken("and", 1, 12)));
            data.addException("Incomplete second subclause", "foo=bar and 78foo", JqlParseErrorMessages.badOperator(createEOFToken(1, 17)));
            data.addException("Incomplete second subclause2", "foo=bar and 78foo=", JqlParseErrorMessages.badOperand(createEOFToken(1, 18)));
            data.addException("Incomplete second subclause with newline", "foo=bar and \n78foo", JqlParseErrorMessages.badOperator(createEOFToken(2, 5)));
            data.addException("Middle subclause invalid", "foo=bar and 78foo brenden and a=b", JqlParseErrorMessages.badOperator(createToken("brenden", 1, 18)));
            data.addException("Middle subclause invalid with newline", "foo=bar and \n78foo brenden and a=b", JqlParseErrorMessages.badOperator(createToken("brenden", 2, 6)));
            data.addException("Second subclause missing with not", "foo=bar and not", JqlParseErrorMessages.badFieldName(createEOFToken(1, 15)));
            data.addException("Second subclause incomplete with not", "foo=bar and not foo =", JqlParseErrorMessages.badOperand(createEOFToken(1, 21)));
            data.addException("Just not", "not", JqlParseErrorMessages.badFieldName(createEOFToken(1, 3)));
            data.addException("two nots", "not not", JqlParseErrorMessages.badFieldName(createEOFToken(1, 7)));
            data.addException("not instead of field", "a=b and not not=b", JqlParseErrorMessages.badFieldName(createToken("=", 1, 15)));
            data.addException("missing logical operator (not instead)", "a=b not a=b", JqlParseErrorMessages.needLogicalOperator(createToken("not", 1, 4)));
            data.addException("just one paren", "(", JqlParseErrorMessages.badFieldName(createEOFToken(1, 1)));
            data.addException("empty list", "abc = ()", JqlParseErrorMessages.badOperand(createToken(")", 1, 7)));
            data.addException("empty list with like", "abc ~ ()", JqlParseErrorMessages.badOperand(createToken(")", 1, 7)));

            data.addException("complex nested parens don't match", "abc in ((fee, fie, foe, fum), 787, (34, (45))", JqlParseErrorMessages.expectedText(createEOFToken(1, 45), ")"));
            data.addException("Random crap on the end", "priority = 12345=== not p jfkff fjfjfj", JqlParseErrorMessages.needLogicalOperator(createToken("=", 1, 16)));
            data.addException("Random crap on the end2", "priority = 12345 \njfkff fjfjfj", JqlParseErrorMessages.needLogicalOperator(createToken("jfkff", 2, 0)));
            data.addException("Random crap on the end3", "priority=a jfkff=fjfjfj", JqlParseErrorMessages.needLogicalOperator(createToken("jfkff", 1, 11)));
            data.addException("Random crap on the end4", "priority=12345 jfkff=fjfjfj", JqlParseErrorMessages.needLogicalOperator(createToken("jfkff", 1, 15)));
            data.addException("Random crap on the end5", "a=b ,b", JqlParseErrorMessages.needLogicalOperator(createToken(",", 1, 4)));
            data.addException("Random crap on the end6", "a=b,b", JqlParseErrorMessages.needLogicalOperator(createToken(",", 1, 3)));

            //Come parens tests.
            data.addException("backwards parens", "abc = )foo(", JqlParseErrorMessages.badOperand(createToken(")", 1, 6)));
            data.addException("unmatched parens (open only)", "(a in a", JqlParseErrorMessages.expectedText(createEOFToken(1, 7), ")"));
            data.addException("unmatched parens (open only)", "abc in (foo", JqlParseErrorMessages.expectedText(createEOFToken(1, 11), ")"));
            data.addException("unmatched parens (open open)", "abc in (foo(", JqlParseErrorMessages.expectedText(createEOFToken(1, 12), ")"));
            data.addException("unmatched parens (double open)", "abc IN ((foo) abc = a", JqlParseErrorMessages.expectedText(createToken("abc", 1, 14), ")"));
            data.addException("unmatched parens (open close close)", "abc in (foo))", JqlParseErrorMessages.needLogicalOperator(createToken(")", 1, 12)));
            data.addException("unmatched parens (open open)", "abc in ((foo), a", JqlParseErrorMessages.expectedText(createEOFToken(1, 16), ")"));
            data.addException("unmatched parens (open open close)", "(abc  =        b or not (j=k and p=l)", JqlParseErrorMessages.expectedText(createEOFToken(1, 37), ")"));

            //Some CF tests.
            data.addException("Unclosed CF label", "cf[1234 = x", JqlParseErrorMessages.expectedText(createToken("=", 1, 8), "]"));
            data.addException("Unopened CF label", "cf1234] = x", JqlParseErrorMessages.badOperator(createToken("]", 1, 6)));
            data.addException("Non-numeric id in CF label", "cf[z123] = x", JqlParseErrorMessages.badCustomFieldId(createToken("z123", 1, 3)));
            data.addException("Negative numeric id in CF label", "cf[-123] = x", JqlParseErrorMessages.badCustomFieldId(createToken("-123", 1, 3)));
            data.addException("Too big numeric id in CF label", String.format("cf[%d] = x", tooBigNumber), JqlParseErrorMessages.illegalNumber(tooBigNumber.toString(), 1, 3));
            data.addException("Empty id in CF label", "cf[] = x", JqlParseErrorMessages.badCustomFieldId(createToken("]", 1, 3)));
            data.addException("Missing CF in CF label", "[54] = x", JqlParseErrorMessages.badFieldName(createToken("[", 1, 0, JqlLexer.LBRACKET)));
            data.addException("Custom Field (cf) without brackets", "cf = brenden", JqlParseErrorMessages.expectedText(createToken("=", 1, 3), "["));

            //Some property tests.
            data.addException("Unclosed property label", "issue.property[x = x", JqlParseErrorMessages.expectedText(createToken("=", 1, 17), "]"));
            data.addException("Unopened property label", "issue.property1234] = x", JqlParseErrorMessages.badOperator(createToken("]", 1, 18)));
            data.addException("Property without operand", "issue.property[x.1] = ", JqlParseErrorMessages.badOperand(createEOFToken(1, 22)));
            data.addException("Property without operator", "issue.property[x.1] unresolved", JqlParseErrorMessages.badOperator(createToken("unresolved", 1, 20)));
            data.addException("Missing property in property label", "[issue.status] = x", JqlParseErrorMessages.badFieldName(createToken("[", 1, 0, JqlLexer.LBRACKET)));
            data.addException("Empty property in property label", "issue.property[] = x", JqlParseErrorMessages.badPropertyArgument(createToken("]", 1, 15)));
            data.addException("Two dots in label", "comment.prop[author]..Author = Test", JqlParseErrorMessages.badPropertyArgument(createToken("..Author", 1, 20)));

            //Lets test some bad function arguments.
            data.addException("Bad function argument", "q = func( [", JqlParseErrorMessages.badFunctionArgument(createToken("[", 1, 10)));
            data.addException("Bad function argument2", "q = func(a, [)", JqlParseErrorMessages.badFunctionArgument(createToken("[", 1, 12)));
            data.addException("Bad function argument3", "q = func(a, [", JqlParseErrorMessages.badFunctionArgument(createToken("[", 1, 12)));
            data.addException("Bad function argument4", "q = func(a, )", JqlParseErrorMessages.emptyFunctionArgument(createToken(")", 1, 12)));
            data.addException("Bad function argument5", "q = func(a, ", JqlParseErrorMessages.badFunctionArgument(createEOFToken(1, 12)));
            data.addException("Bad function argument6", "q = func(a", JqlParseErrorMessages.expectedText(createEOFToken(1, 10), ")"));
            data.addException("Bad function argument7", "q = func(", JqlParseErrorMessages.expectedText(createEOFToken(1, 9), ")"));
            data.addException("Bad function argument8", "q = func(good, jack!, \"really good\")", JqlParseErrorMessages.expectedText(createToken("!", 1, 19), ")", ","));

            data.addException("No such operator", "pri notin (x)", JqlParseErrorMessages.reservedWord("notin", 1, 4));
            data.addException("No such operator2", "pri isnot empty", JqlParseErrorMessages.badOperator(createToken("isnot", 1, 4)));
            data.addException("No such operator3", "pri ^ empty", JqlParseErrorMessages.reservedCharacter('^', 1, 4));
            data.addException("No such operator4", "pri is", JqlParseErrorMessages.badOperand(createEOFToken(1, 6)));
            data.addException("No such operator5", "pri not is empty", JqlParseErrorMessages.expectedText(createToken("is", 1, 8), "IN"));
            data.addException("Bang does not work for 'not in'", "pri ! in (test)", JqlParseErrorMessages.badOperator(createToken("!", 1, 4)));
            data.addException("Bang does not work for 'not empty'", "pri is ! empty", JqlParseErrorMessages.expectedText(createToken("!", 1, 7), "NOT"));

            data.addException("Unmatched string quote", "priority = \"\"\"", JqlParseErrorMessages.unfinishedString(null, 1, 13));
            data.addException("Unmatched string quote2", "priority = \"", JqlParseErrorMessages.unfinishedString(null, 1, 11));
            data.addException("Unmatched string quote3", "priority = '''", JqlParseErrorMessages.unfinishedString(null, 1, 13));
            data.addException("Unmatched string quote4", "priority = '", JqlParseErrorMessages.unfinishedString(null, 1, 11));

            data.addException("Unescaped single quote", "what = hrejw'ewjrhejkw", JqlParseErrorMessages.unfinishedString("ewjrhejkw", 1, 12));
            data.addException("Unescaped quote", "wh\"at = hrejwewjrhejkw", JqlParseErrorMessages.unfinishedString("at = hrejwewjrhejkw", 1, 2));

            data.addException("Empty Field Name", "'' = bad", JqlParseErrorMessages.emptyFieldName(1, 0));
            data.addException("Empty Field Name2", "\"\" = bad", JqlParseErrorMessages.emptyFieldName(1, 0));
            data.addException("Empty Escaped Field Name", "\\     < 38", JqlParseErrorMessages.emptyFieldName(1, 0));
            data.addException("Empty Function Name", "a \n= ''()", JqlParseErrorMessages.emptyFunctionName(2, 2));
            data.addException("Empty Function Name2", "b = \"\"()", JqlParseErrorMessages.emptyFunctionName(1, 4));

            data.addException("Unterminated escape", "test = case\\", JqlParseErrorMessages.illegalEsacpe(null, 1, 11));
            data.addException("Illegal escape", "test = case\\k", JqlParseErrorMessages.illegalEsacpe("\\k", 1, 11));
            data.addException("Unterminated illegal unicode", "test = case\\u", JqlParseErrorMessages.illegalEsacpe("\\u", 1, 11));
            data.addException("Unterminated illegal unicode2", "test = case\\u278q", JqlParseErrorMessages.illegalEsacpe("\\u278q", 1, 11));
            data.addException("Unterminated illegal unicode3", "test = case\\u27", JqlParseErrorMessages.illegalEsacpe("\\u27", 1, 11));
            data.addException("Unterminated illegal unicode4", "test = case\\u-998", JqlParseErrorMessages.illegalEsacpe("\\u-", 1, 11));
            data.addException("Unterminated illegal unicode5", "test = case order by \\u-998", JqlParseErrorMessages.illegalEsacpe("\\u-", 1, 21));
            data.addException("Unterminated illegal unicode6", "test = case\\uzzzz", JqlParseErrorMessages.illegalEsacpe("\\uz", 1, 11));
            data.addException("Unterminated illegal unicode7", "test = case\\u278qzzzz", JqlParseErrorMessages.illegalEsacpe("\\u278q", 1, 11));
            data.addException("Unterminated illegal unicode8", "test = case\\u27zzzzz", JqlParseErrorMessages.illegalEsacpe("\\u27z", 1, 11));
            data.addException("Unterminated illegal unicode9", "tecase\\u-998zzzzz", JqlParseErrorMessages.illegalEsacpe("\\u-", 1, 6));
            data.addException("Unterminated illegal unicod10", "test = case order by \\u-998zzzzz", JqlParseErrorMessages.illegalEsacpe("\\u-", 1, 21));

            data.addException("Number too big", String.format("priority = %d", tooBigNumber), JqlParseErrorMessages.illegalNumber(tooBigNumber.toString(), 1, 11));
            data.addException("Number too big 2", String.format("priority in  (%d, 2)", tooBigNumber), JqlParseErrorMessages.illegalNumber(tooBigNumber.toString(), 1, 14));

            data.addException("Control Character String", "c = q\uffff", JqlParseErrorMessages.illegalCharacter('\uffff', 1, 5));
            data.addException("Control Character String 2", "c = c\ufdd0", JqlParseErrorMessages.illegalCharacter('\ufdd0', 1, 5));
            data.addException("Control Character String 3", "c = aa\ufdd0kkk", JqlParseErrorMessages.illegalCharacter('\ufdd0', 1, 6));
            data.addException("Control Character String 4", "\u0000iuyiuyiu", JqlParseErrorMessages.illegalCharacter('\u0000', 1, 0));
            data.addException("Control Character String 5", "aa = bb order by \u0001", JqlParseErrorMessages.illegalCharacter('\u0001', 1, 17));

            data.addException("Control Character Quoted", "control = \"char\ufffe\"", JqlParseErrorMessages.illegalCharacter('\ufffe', 1, 15));
            data.addException("Control Character Quoted 2", "control = \"char\ufdef\"", JqlParseErrorMessages.illegalCharacter('\ufdef', 1, 15));
            data.addException("Control Character Quoted 3", "\"\u001f\"", JqlParseErrorMessages.illegalCharacter('\u001f', 1, 1));
            data.addException("Control Character Quoted 4", "control = \"char\b\"", JqlParseErrorMessages.illegalCharacter('\b', 1, 15));
            data.addException("Control Character Quoted 5", "control = a order by \"char\b\"", JqlParseErrorMessages.illegalCharacter('\b', 1, 26));

            data.addException("Control Character Single Quoted", "control\n = 'char\u0002'", JqlParseErrorMessages.illegalCharacter('\u0002', 2, 8));
            data.addException("Control Character Single Quoted 2", "control = '\ufdd5'", JqlParseErrorMessages.illegalCharacter('\ufdd5', 1, 11));
            data.addException("Control Character Single Quoted 3", "control = '\ufdef'", JqlParseErrorMessages.illegalCharacter('\ufdef', 1, 11));
            data.addException("Control Character Single Quoted 4", "control = 'char\t'", JqlParseErrorMessages.illegalCharacter('\t', 1, 15));
            data.addException("Control Character Single Quoted 5", "control = a order by 'char\t'", JqlParseErrorMessages.illegalCharacter('\t', 1, 26));

            data.addException("Control Character Field Name", "cont\nrol = ''", JqlParseErrorMessages.badOperator(createToken("rol", 2, 0)));
            data.addException("Control Character Function Name", "control = f\run()", JqlParseErrorMessages.needLogicalOperator(createToken("un", 1, 12)));
            data.addException("Control Character Function Argument", "control = fun('\uffff')", JqlParseErrorMessages.illegalCharacter('\uffff', 1, 15));

            data.addException("Empty Order By", "a = b order by", JqlParseErrorMessages.badFieldName(createEOFToken(1, 14)));
            data.addException("Empty Order By2", "a = b order", JqlParseErrorMessages.expectedText(createEOFToken(1, 11), "by"));
            data.addException("Empty Order By3", "a = b order bad", JqlParseErrorMessages.expectedText(createToken("bad", 1, 12), "by"));
            data.addException("Unfinished Order By", "a = b order BY abc desc, ", JqlParseErrorMessages.badFieldName(createEOFToken(1, 25)));
            data.addException("Unfinished Order By 2", "a = b order BY abc desc, ]", JqlParseErrorMessages.badFieldName(createToken("]", 1, 25)));
            data.addException("Bad Order", "order BY abc s", JqlParseErrorMessages.badSortOrder(createToken("s", 1, 13)));
            data.addException("Bad Name Order", "order BY desc", JqlParseErrorMessages.badFieldName(createToken("desc", 1, 9)));
            data.addException("Order by Extra", "order BY era desc extra", JqlParseErrorMessages.expectedText(createToken("extra", 1, 18), ","));

            data.addException("Test newline in value", "query = hello\nworld", JqlParseErrorMessages.needLogicalOperator(createToken("world", 2, 0)));

            //Tests to make sure illegal characters cause an error.
            for (int i = 0; i < ILLEGAL_CHARS_STRING.length(); i++) {
                final char currentChar = ILLEGAL_CHARS_STRING.charAt(i);
                data.addException(String.format("Error with unescaped '%c'.", currentChar), String.format("test%cdfjd = 'bad'", currentChar), JqlParseErrorMessages.reservedCharacter(currentChar, 1, 4));
            }

            //Make sure that unquoted reserved words cause an error.
            final Set<String> words = new HashSet<>(JqlStringSupportImpl.RESERVED_WORDS);
            words.removeAll(currentWords);
            for (final String reservedWord : words) {
                data.addException(String.format("Unquoted reserved word '%s'.", reservedWord), String.format("priority NOT IN %s('arg', 10)", reservedWord), JqlParseErrorMessages.reservedWord(reservedWord, 1, 16));
            }
        }

        //Some lexical errors
        {
            //This test may no longer be valid when we expand the queries.
            data.addException("Lexical errors at end", "f\n = \n \n abc *", JqlParseErrorMessages.reservedCharacter('*', 4, 5));
            data.addException("Lexical errors in middle", "f *= abc", JqlParseErrorMessages.reservedCharacter('*', 1, 2));
        }

        //Tests for reserved characters.
        for (int i = 0; i < ILLEGAL_CHARS_STRING.length(); i++) {
            final char currentChar = ILLEGAL_CHARS_STRING.charAt(i);
            data.addException("Reserved character " + currentChar, "bbain = " + currentChar, JqlParseErrorMessages.reservedCharacter(currentChar, 1, 8));
        }

        // was clause change history queries
        data.addException("Incomplete was clause", "status was", JqlParseErrorMessages.badOperand(createEOFToken(1, 10)));

        return data.data();
    }

    private static Token createEOFToken(final int line, final int position) {
        return createToken(null, line, position, Token.EOF);
    }

    private static Token createToken(final String text, final int line, final int position) {
        return createToken(text, line, position, 10);
    }

    private static Token createToken(final String text, final int line, final int position, final int type) {
        final CommonToken token = new CommonToken(type);
        token.setLine(line);
        token.setCharPositionInLine(position);
        token.setText(text);
        return token;
    }

    private final String desc;
    private final String inputJql;
    private final JqlParseErrorMessage parseError;

    public TestJqlParserError(final String desc, final String inputJql, final JqlParseErrorMessage parseError) {
        this.desc = desc;
        this.inputJql = inputJql;
        this.parseError = parseError;
    }

    @Test
    public void testParseException() {
        final DefaultJqlQueryParser parser = new DefaultJqlQueryParser();
        try {
            final Query query = parser.parseQuery(inputJql);
            fail(desc + ": Expected exception on input '" + inputJql + "' but got query '" + query + "'");
        } catch (final JqlParseException e) {
            final JqlParseErrorMessage actual = e.getParseErrorMessage();
            assertEquals(parseError, actual);
        }
    }

    private static class DataBuilder {
        final List<Object[]> data = new ArrayList<>();

        private void addException(final String description, final String jql, final JqlParseErrorMessage errorMessage) {
            data.add(new Object[]{description, jql, errorMessage});
        }

        public List<Object[]> data() {
            return data;
        }
    }
}
