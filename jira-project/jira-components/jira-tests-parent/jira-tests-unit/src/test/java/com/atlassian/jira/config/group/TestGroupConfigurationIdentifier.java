package com.atlassian.jira.config.group;

import com.atlassian.crowd.embedded.impl.ImmutableGroup;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.picocontainer.PicoContainer;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @since 7.0
 */
@RunWith(MockitoJUnitRunner.class)
public class TestGroupConfigurationIdentifier {
    @Mock
    private PicoContainer picoContainer;

    private GroupConfigurationIdentifier groupConfigurationIdentifier;

    @Before
    public void setUp() {
        groupConfigurationIdentifier = new GroupConfigurationIdentifier(picoContainer);
    }

    private GroupConfigurable mockConfigurable() {
        final GroupConfigurable groupConfigurable = mock(GroupConfigurable.class);
        when(picoContainer.getComponents(GroupConfigurable.class)).thenReturn(Lists.newArrayList(groupConfigurable));
        return groupConfigurable;
    }

    @Test
    public void testHasNoGroupConfigured() {
        when(mockConfigurable().isGroupUsed(new ImmutableGroup("Three"))).thenReturn(false);
        assertFalse(groupConfigurationIdentifier.groupHasExistingConfiguration("Three"));
    }

    @Test
    public void testHasGroupConfigured() {
        when(mockConfigurable().isGroupUsed(new ImmutableGroup("three"))).thenReturn(true);
        assertTrue(groupConfigurationIdentifier.groupHasExistingConfiguration("Three"));
    }

    @Test
    public void testHasNoGroupConfiguredWhenNoProviders() {
        mockConfigurable();
        assertFalse(groupConfigurationIdentifier.groupHasExistingConfiguration("Three"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNullGroupNotSupported() {
        groupConfigurationIdentifier.groupHasExistingConfiguration(null);
    }

}