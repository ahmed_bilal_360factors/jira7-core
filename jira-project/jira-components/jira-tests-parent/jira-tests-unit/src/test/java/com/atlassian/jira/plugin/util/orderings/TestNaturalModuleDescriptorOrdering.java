package com.atlassian.jira.plugin.util.orderings;

import com.atlassian.plugin.ModuleDescriptor;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

/**
 * Responsible for holding tests of {@link NaturalModuleDescriptorOrdering}
 *
 * @since v4.4
 */
public class TestNaturalModuleDescriptorOrdering {
    @Rule
    public MockitoRule mockito = MockitoJUnit.rule();

    @Mock
    private ModuleDescriptor mockDescriptor2;
    @Mock
    private ModuleDescriptor mockDescriptor1;
    private NaturalModuleDescriptorOrdering naturalModuleDescriptorOrdering;

    @Before
    public void setUp() throws Exception {
        naturalModuleDescriptorOrdering = new NaturalModuleDescriptorOrdering();
    }

    @Test
    public void testCompareReturnsZeroForModulesWithTheSameCompleteKey() throws Exception {
        when(mockDescriptor1.getCompleteKey()).thenReturn("mock-plugin:descriptor");
        when(mockDescriptor2.getCompleteKey()).thenReturn("mock-plugin:descriptor");

        assertTrue(naturalModuleDescriptorOrdering.compare(mockDescriptor1, mockDescriptor2) == 0);
        assertTrue(naturalModuleDescriptorOrdering.compare(mockDescriptor2, mockDescriptor1) == 0);
    }

    @Test
    public void testCompareFollowsAlphabeticalOrderWhenComparingModulesWithTheSamePluginKey() throws Exception {
        when(mockDescriptor1.getCompleteKey()).thenReturn("mock-plugin:a-lower-descriptor");
        when(mockDescriptor2.getCompleteKey()).thenReturn("mock-plugin:b-upper-cdescriptor");

        assertTrue(naturalModuleDescriptorOrdering.compare(mockDescriptor1, mockDescriptor2) <= -1);
        assertTrue(naturalModuleDescriptorOrdering.compare(mockDescriptor2, mockDescriptor1) >= 1);
    }

    @Test
    public void testCompareFollowsAlphabeticalOrderWhenComparingModulesWithDifferentPluginKeys() throws Exception {
        when(mockDescriptor1.getCompleteKey()).thenReturn("d-mock-plugin:descriptor");
        when(mockDescriptor2.getCompleteKey()).thenReturn("f-mock-plugin:descriptor");

        assertTrue(naturalModuleDescriptorOrdering.compare(mockDescriptor1, mockDescriptor2) <= -1);
        assertTrue(naturalModuleDescriptorOrdering.compare(mockDescriptor2, mockDescriptor1) >= 1);
    }
}
