package com.atlassian.jira.upgrade.tasks;

import com.atlassian.jira.database.MockDbConnectionManager;
import com.atlassian.jira.index.request.AffectedIndex;
import com.atlassian.jira.index.request.ReindexRequestService;
import com.atlassian.jira.index.request.ReindexRequestType;
import com.atlassian.jira.index.request.SharedEntityType;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;

import java.util.EnumSet;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;

public class TestUpgradeTask_Build71001 {
    @Rule
    public MockitoContainer container = MockitoMocksInContainer.rule(this);

    @AvailableInContainer
    @Mock
    private ReindexRequestService reindexRequestService;

    private MockDbConnectionManager dbConnectionManager;
    private UpgradeTask_Build71001 upgradeTask;

    private final String NODE_DELETION_QUERY = "delete from nodeassociation\n" +
            "where nodeassociation.source_node_entity = 'Issue' and nodeassociation.sink_node_entity = 'Version' and nodeassociation.sink_node_id not in (select v.id\n" +
            "from projectversion v)";

    @Before
    public void setUp() throws Exception {
        dbConnectionManager = new MockDbConnectionManager();
        upgradeTask = new UpgradeTask_Build71001(dbConnectionManager);
    }

    @Test
    public void shouldPerformTheCorrectDeleteUpdateAndRequestReindexWhenAnyAssociationsAreAffected() throws Exception {
        dbConnectionManager.setUpdateResults(NODE_DELETION_QUERY, 1);

        upgradeTask.doUpgrade(false);

        dbConnectionManager.assertAllExpectedStatementsWereRun();
        verify(reindexRequestService).requestReindex(ReindexRequestType.DELAYED,
                EnumSet.of(AffectedIndex.ISSUE),
                EnumSet.noneOf(SharedEntityType.class));
    }

    @Test
    public void shouldPerformTheCorrectDeleteUpdateAndNotRequestReindexWhenZeroAssociationsAreAffected() throws Exception {
        dbConnectionManager.setUpdateResults(NODE_DELETION_QUERY, 0);

        upgradeTask.doUpgrade(false);

        dbConnectionManager.assertAllExpectedStatementsWereRun();
        verifyZeroInteractions(reindexRequestService);
    }
}
