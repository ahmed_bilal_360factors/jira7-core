package com.atlassian.jira.web.action.admin;

import com.atlassian.core.ofbiz.test.UtilsForTests;
import com.atlassian.fugue.Option;
import com.atlassian.jira.application.ApplicationRoleManager;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockComponentContainer;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.permission.GlobalPermissionType;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.util.GlobalPermissionGroupAssociationUtil;
import com.atlassian.jira.web.util.ExternalLinkUtil;
import com.google.common.collect.ImmutableList;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static com.atlassian.jira.mock.security.MockSimpleAuthenticationContext.createNoopContext;
import static com.atlassian.jira.permission.GlobalPermissionKey.ADMINISTER;
import static com.atlassian.jira.permission.GlobalPermissionKey.BULK_CHANGE;
import static com.atlassian.jira.permission.GlobalPermissionKey.CREATE_SHARED_OBJECTS;
import static com.atlassian.jira.permission.GlobalPermissionKey.SYSTEM_ADMIN;
import static com.atlassian.jira.permission.GlobalPermissionKey.USE;
import static org.hamcrest.Matchers.hasItem;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class GlobalPermissionsTest {
    private static final ImmutableList<GlobalPermissionType> GLOBAL_PERMISSION_TYPES = ImmutableList.of(createGlobalPermission(ADMINISTER),
            createGlobalPermission(USE), createGlobalPermission(CREATE_SHARED_OBJECTS), createGlobalPermission(BULK_CHANGE));

    @Rule
    public MockComponentContainer container = new MockComponentContainer(this);

    @SuppressWarnings("UnusedDeclaration")
    @AvailableInContainer
    private JiraAuthenticationContext authenticationContext = createNoopContext(new MockApplicationUser("User"));

    private GlobalPermissions action;

    @Mock
    private GlobalPermissionManager globalPermissionManager;

    @Mock
    private GlobalPermissionGroupAssociationUtil globalPermissionGroupAssociationUtil;

    @Mock
    private GroupManager groupManager;

    @Mock
    private ExternalLinkUtil externalLinkUtil;

    @Mock
    private ApplicationRoleManager applicationRoleManager;

    @Before
    public void setup() {
        UtilsForTests.cleanWebWork();

        when(globalPermissionManager.getAllGlobalPermissions()).thenReturn(GLOBAL_PERMISSION_TYPES);

        when(globalPermissionManager.isPermissionManagedByJira(any(GlobalPermissionKey.class))).thenReturn(true);
        when(globalPermissionManager.isPermissionManagedByJira(ADMINISTER)).thenReturn(false);
        when(globalPermissionManager.isPermissionManagedByJira(USE)).thenReturn(false);
        when(globalPermissionManager.isPermissionManagedByJira(SYSTEM_ADMIN)).thenReturn(false);

        action = new GlobalPermissions(globalPermissionManager, globalPermissionGroupAssociationUtil, groupManager,
                applicationRoleManager, externalLinkUtil);

    }

    @Test
    public void usePermissionShouldNotBeAvailable() {
        assertFalse(action.getGlobalPermTypes().containsKey(GlobalPermissionKey.USE.getKey()));
    }

    @Test
    public void validationFailsIfUserIsTryingToAddPermissionNotManagedByJira() {
        action.setAction("ADD");
        setAdminPermission();
        action.setGroupName("someGroup");
        action.doValidation();

        assertThat(action.getErrorMessages(), hasItem("admin.errors.permissions.permission.not.managed.by.jira{[]}"));
    }

    @Test
    public void validationSucceedsIfUserIsTryingToAddPermissionManagedBJira() {
        action.setAction("ADD");
        action.setGlobalPermType(GlobalPermissionKey.BULK_CHANGE.getKey());
        action.setGroupName("someGroup");
        action.doValidation();

        assertTrue(action.getErrorMessages().isEmpty());
    }

    @Test
    public void validationFailsIfUserIsTryingToDeletePermissionNotManagedBJira() {
        action.setAction("DELETE");
        setAdminPermission();
        action.setGroupName("someGroup");
        action.doValidation();

        assertThat(action.getErrorMessages(), hasItem("admin.errors.permissions.permission.not.managed.by.jira{[]}"));
    }

    @Test
    public void validationSucceedsIfUserIsTryingToDeletePermissionManagedBJira() {
        action.setAction("DELETE");
        action.setGlobalPermType(GlobalPermissionKey.BULK_CHANGE.getKey());
        action.setGroupName("someGroup");
        action.doValidation();

        assertTrue(action.getErrorMessages().isEmpty());
    }

    private void setAdminPermission() {
        action.setGlobalPermType(GlobalPermissionKey.ADMINISTER.getKey());
        when(globalPermissionManager.getGlobalPermission(GlobalPermissionKey.ADMINISTER.getKey()))
                .thenReturn(Option.some(new GlobalPermissionType(Permissions.Permission.ADMINISTER, false)));
    }

    private static GlobalPermissionType createGlobalPermission(GlobalPermissionKey key) {
        return new GlobalPermissionType(key.getKey(),
                key.getKey(),
                key.getKey(),
                false
        );
    }
}