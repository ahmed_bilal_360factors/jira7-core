package com.atlassian.jira.application.install;

import com.google.common.base.Throwables;

import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.IntFunction;

/**
 * Class that eases usage of stream with throwing functions.
 * Wraps checked exceptions into RuntimeExeceptions using Throwables.propagate
 * instead of:
 * <p>
 * {@code
 * .forEach(sourceFile -> {
 * try {
 * updatePlugin(sourceFile, installedVersionsSupplier, revertableFileOperations))
 * } catch( .... ) {
 * Throwables.propagate()...
 * }
 * }
 * );
 * }
 * use simplier:
 * {@code
 * .forEach(uncheckConsumer(sourceFile ->
 * updatePlugin(sourceFile, installedVersionsSupplier, revertableFileOperations))
 * );
 * }
 *
 * @since v6.5
 */
public class ThrowableFunctions {
    @FunctionalInterface
    public interface ThrowingConsumer<T, E extends Throwable> {
        void accept(T t) throws E;
    }

    @FunctionalInterface
    public interface ThrowingIntFunction<R, E extends Throwable> {
        R apply(int value) throws E;
    }

    @FunctionalInterface
    public interface ThrowingFunction<T, R, E extends Throwable> {
        R apply(T value) throws E;
    }

    public static <T, E extends Exception> Consumer<T> uncheckConsumer(ThrowingConsumer<T, E> action) {
        return val -> {
            try {
                action.accept(val);
            } catch (RuntimeException x) {
                throw x;
            } catch (Exception x) {
                throw Throwables.propagate(x);
            }
        };
    }

    public static <T, E extends Exception> IntFunction<T> uncheckIntFunction(ThrowingIntFunction<T, E> action) {
        return val -> {
            try {
                return action.apply(val);
            } catch (RuntimeException x) {
                throw x;
            } catch (Exception x) {
                throw Throwables.propagate(x);
            }
        };
    }

    public static <T, R, E extends Exception> Function<T, R> uncheckFunction(ThrowingFunction<T, R, E> action) {
        return val -> {
            try {
                return action.apply(val);
            } catch (RuntimeException x) {
                throw x;
            } catch (Exception x) {
                throw Throwables.propagate(x);
            }
        };
    }
}
