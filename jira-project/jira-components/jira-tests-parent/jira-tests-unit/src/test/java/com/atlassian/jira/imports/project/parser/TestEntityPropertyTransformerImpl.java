package com.atlassian.jira.imports.project.parser;

import com.atlassian.jira.entity.property.EntityProperty;
import com.atlassian.jira.external.beans.ExternalEntityProperty;
import com.atlassian.jira.imports.project.core.EntityRepresentation;
import com.atlassian.jira.imports.project.transformer.EntityPropertyTransformerImpl;
import org.junit.Test;

import java.sql.Timestamp;
import java.util.Map;

import static com.atlassian.jira.entity.property.EntityProperty.ENTITY_ID;
import static com.atlassian.jira.imports.project.parser.EntityPropertyParser.ENTITY_PROPERTY_ENTITY_NAME;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

public class TestEntityPropertyTransformerImpl {
    @Test
    public void getEntityRepresentation() {
        final ExternalEntityProperty property = new ExternalEntityProperty(1L, "entity", 2L, "key", "value",
                new Timestamp(System.currentTimeMillis()), new Timestamp(System.currentTimeMillis()));
        final EntityRepresentation representation = new EntityPropertyTransformerImpl().getEntityRepresentation(property, 3L);

        assertThat(representation.getEntityName(), is(ENTITY_PROPERTY_ENTITY_NAME));
        final Map<String, String> values = representation.getEntityValues();
        assertNotNull(values);
        assertEquals(6, values.size());
        assertFalse(values.containsKey(EntityProperty.ID));
        assertThat(values.get(ENTITY_ID), is("3"));
    }

}