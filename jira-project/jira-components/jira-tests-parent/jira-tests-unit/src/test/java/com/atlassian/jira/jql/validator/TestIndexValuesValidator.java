package com.atlassian.jira.jql.validator;

import com.atlassian.jira.jql.operand.JqlOperandResolver;
import com.atlassian.jira.jql.operand.QueryLiteral;
import com.atlassian.jira.jql.util.IndexValueConverter;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.MessageSet;
import com.atlassian.jira.util.collect.CollectionBuilder;
import com.atlassian.query.clause.TerminalClause;
import com.atlassian.query.clause.TerminalClauseImpl;
import com.atlassian.query.operand.EmptyOperand;
import com.atlassian.query.operand.FunctionOperand;
import com.atlassian.query.operand.Operand;
import com.atlassian.query.operand.SingleValueOperand;
import com.atlassian.query.operator.Operator;
import com.google.common.collect.ImmutableList;
import org.hamcrest.Matchers;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.concurrent.atomic.AtomicInteger;

import static com.atlassian.jira.jql.operand.SimpleLiteralFactory.createLiteral;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestIndexValuesValidator {
    @Rule
    public MockitoRule mockito = MockitoJUnit.rule();
    @Mock
    private JqlOperandResolver jqlOperandResolver;
    @Mock
    private IndexValueConverter indexValueConverter;
    private ApplicationUser theUser = null;

    @Test
    public void testValidateNullValid() throws Exception {
        final Operand operand = new FunctionOperand("DoesntExist");
        final TerminalClause clause = new TerminalClauseImpl("blah", Operator.EQUALS, operand);

        jqlOperandResolver = MockJqlOperandResolver.createSimpleSupport();

        final AtomicInteger called = new AtomicInteger(0);

        IndexValuesValidator validator = new IndexValuesValidator(jqlOperandResolver, indexValueConverter) {
            @Override
            void addError(final MessageSet messageSet, final ApplicationUser searcher, final TerminalClause terminalClause, final QueryLiteral literal) {
                called.getAndIncrement();
            }
        };

        validator.validate(theUser, clause);

        assertEquals(0, called.get());
    }

    @Test
    public void testValidateEmptyValid() throws Exception {
        final Operand operand = EmptyOperand.EMPTY;
        final TerminalClause clause = new TerminalClauseImpl("blah", Operator.IS, operand);

        jqlOperandResolver = MockJqlOperandResolver.createSimpleSupport();

        final AtomicInteger called = new AtomicInteger(0);

        IndexValuesValidator validator = new IndexValuesValidator(jqlOperandResolver, indexValueConverter) {
            @Override
            void addError(final MessageSet messageSet, final ApplicationUser searcher, final TerminalClause terminalClause, final QueryLiteral literal) {
                called.getAndIncrement();
            }
        };

        validator.validate(theUser, clause);

        assertEquals(0, called.get());
    }

    @Test
    public void testValidateEmptyInvalid() throws Exception {
        final Operand operand = EmptyOperand.EMPTY;
        final TerminalClause clause = new TerminalClauseImpl("blah", Operator.IS, operand);

        jqlOperandResolver = MockJqlOperandResolver.createSimpleSupport();

        final AtomicInteger called = new AtomicInteger(0);

        IndexValuesValidator validator = new IndexValuesValidator(jqlOperandResolver, indexValueConverter, false) {
            @Override
            void addError(final MessageSet messageSet, final ApplicationUser searcher, final TerminalClause terminalClause, final QueryLiteral literal) {
                called.getAndIncrement();
            }
        };

        validator.validate(theUser, clause);

        assertEquals(1, called.get());
    }

    @Test
    public void testValidateSingleValid() throws Exception {
        final Operand operand = new SingleValueOperand("blah");
        final TerminalClause clause = new TerminalClauseImpl("blah", Operator.EQUALS, operand);
        final QueryLiteral literal = createLiteral("blah");

        when(jqlOperandResolver.getValues(theUser, operand, clause)).thenReturn(ImmutableList.of(literal));
        when(indexValueConverter.convertToIndexValue(literal)).thenReturn("a string");

        final AtomicInteger called = new AtomicInteger(0);

        IndexValuesValidator validator = new IndexValuesValidator(jqlOperandResolver, indexValueConverter) {
            @Override
            void addError(final MessageSet messageSet, final ApplicationUser searcher, final TerminalClause terminalClause, final QueryLiteral literal) {
                called.getAndIncrement();
            }
        };


        validator.validate(theUser, clause);

        assertEquals(0, called.get());
    }

    @Test
    public void testValidateSingleInvalid() throws Exception {
        final Operand operand = new SingleValueOperand("blah");
        final TerminalClause clause = new TerminalClauseImpl("blah", Operator.EQUALS, operand);
        final QueryLiteral literal = createLiteral("blah");

        when(jqlOperandResolver.getValues(theUser, operand, clause)).thenReturn(ImmutableList.of(literal));
        when(indexValueConverter.convertToIndexValue(literal)).thenReturn(null);

        final AtomicInteger called = new AtomicInteger(0);

        IndexValuesValidator validator = new IndexValuesValidator(jqlOperandResolver, indexValueConverter) {
            @Override
            void addError(final MessageSet messageSet, final ApplicationUser searcher, final TerminalClause terminalClause, final QueryLiteral literal) {
                assertThat(terminalClause, sameInstance(clause));
                assertThat(literal, sameInstance(literal));
                called.getAndIncrement();
            }
        };


        validator.validate(theUser, clause);

        assertEquals(1, called.get());
    }

    @Test
    public void testValidateSingleMultiple() throws Exception {
        final Operand operand = new SingleValueOperand("blah");
        final TerminalClause clause = new TerminalClauseImpl("blah", Operator.EQUALS, operand);
        final QueryLiteral literalInvalid1 = createLiteral("blah1");
        final QueryLiteral literalInvalid2 = createLiteral("blah2");
        final QueryLiteral literalValid1 = createLiteral("blah3");
        final QueryLiteral literalValid2 = createLiteral("blah4");

        when(jqlOperandResolver.getValues(theUser, operand, clause)).thenReturn(CollectionBuilder.newBuilder(literalInvalid1, literalValid1, literalInvalid2, literalValid2).asList());
        when(indexValueConverter.convertToIndexValue(literalInvalid1)).thenReturn(null);
        when(indexValueConverter.convertToIndexValue(literalValid1)).thenReturn("a string");
        when(indexValueConverter.convertToIndexValue(literalInvalid2)).thenReturn(null);
        when(indexValueConverter.convertToIndexValue(literalValid2)).thenReturn("a string2");

        final AtomicInteger called = new AtomicInteger(0);

        IndexValuesValidator validator = new IndexValuesValidator(jqlOperandResolver, indexValueConverter) {
            @Override
            void addError(final MessageSet messageSet, final ApplicationUser searcher, final TerminalClause terminalClause, final QueryLiteral literal) {
                assertThat(terminalClause, sameInstance(clause));
                assertThat(literal, Matchers.isOneOf(literalInvalid1, literalInvalid2));
                called.getAndIncrement();
            }
        };

        validator.validate(theUser, clause);

        assertEquals(2, called.get());
    }


}
