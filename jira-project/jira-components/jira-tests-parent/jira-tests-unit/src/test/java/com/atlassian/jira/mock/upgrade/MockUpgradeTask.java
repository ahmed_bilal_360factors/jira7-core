package com.atlassian.jira.mock.upgrade;

import com.atlassian.jira.upgrade.AbstractUpgradeTask;
import com.atlassian.jira.upgrade.UpgradeTask;

import javax.annotation.Nullable;
import java.util.Collection;
import java.util.Collections;

public class MockUpgradeTask extends AbstractUpgradeTask {
    private final Integer version;
    private final String shortDescription;
    private final ScheduleOption scheduleOption;
    private boolean doUpgradeCalled;

    public MockUpgradeTask(final Integer version, final String shortDescription) {
        this.version = version;
        this.shortDescription = shortDescription;
        scheduleOption = ScheduleOption.BEFORE_JIRA_STARTED;
    }

    public MockUpgradeTask(final Integer version, final String shortDescription, final ScheduleOption scheduleOption) {
        this.version = version;
        this.shortDescription = shortDescription;
        this.scheduleOption = scheduleOption;
    }

    public int getBuildNumber() {
        return version;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void doUpgrade(boolean setupMode) {
        doUpgradeCalled = true;
    }

    public Collection<String> getErrors() {
        return Collections.emptyList();
    }

    @Override
    public boolean isDowngradeTaskRequired() {
        return true;
    }

    @Override
    public String toString() {
        return "version: " + version + ", shortDesctription: " + shortDescription;
    }

    public String getClassName() {
        return "MockUpgradeTask" + version;
    }

    @Nullable
    @Override
    public Integer dependsUpon() {
        return null;
    }

    @Override
    public ScheduleOption getScheduleOption() {
        return scheduleOption;
    }

    public boolean isDoUpgradeCalled() {
        return doUpgradeCalled;
    }
}
