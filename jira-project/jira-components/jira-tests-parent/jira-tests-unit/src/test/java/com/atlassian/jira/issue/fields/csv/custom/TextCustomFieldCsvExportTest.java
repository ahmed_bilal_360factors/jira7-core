package com.atlassian.jira.issue.fields.csv.custom;

import org.junit.Test;

public abstract class TextCustomFieldCsvExportTest extends CustomFieldSingleValueCsvExporterTest<String> {
    @Test
    public void textIsExportedAsIs() {
        String text = "some text\nwith new lines\n";
        whenFieldValueIs(text);
        assertExportedValue(text);
    }
}
