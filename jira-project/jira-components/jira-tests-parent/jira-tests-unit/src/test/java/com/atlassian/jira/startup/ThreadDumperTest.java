package com.atlassian.jira.startup;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.slf4j.Logger;

import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

/**
 * @since 6.4
 */
@RunWith(MockitoJUnitRunner.class)
public class ThreadDumperTest {
    public static final int LONG_WAIT = 200;
    public static final int SHORT_WAIT = 1;
    public static final int MEDIUM_WAIT = 20;

    private
    @Mock
    Logger log;

    @Test
    public void loggingShouldBeEnabledByDefault() {
        ThreadDumper threadDumper = new ThreadDumper(SHORT_WAIT);

        assertTrue(threadDumper.isLogEnabled());
    }

    @Test
    public void cancelDumpShouldClearLogFlag() {
        ThreadDumper threadDumper = new ThreadDumper(SHORT_WAIT);
        threadDumper.cancelDump();

        assertFalse(threadDumper.isLogEnabled());
    }

    @Test
    public void shouldLogDisabledShouldExitEarly() {
        ThreadDumper threadDumper = new ThreadDumper(SHORT_WAIT, log);
        threadDumper.cancelDump();

        threadDumper.run();
        verify(log).debug(anyString());
        verifyNoMoreInteractions(log);
    }

    @Test
    public void logsShouldOccurWhenInterrupted() throws Exception {
        ThreadDumper threadDumper = new ThreadDumper(LONG_WAIT, log);

        Thread thread = new Thread(threadDumper, "Thread-dumping thread from + " + this.getClass().getSimpleName());
        thread.setDaemon(true);
        thread.interrupt();

        thread.start();

        thread.join();

        verify(log).error(anyString());
        verifyNoMoreInteractions(log);
    }

    @Test
    public void logsShouldOccurWhenInterruptExceptioned() throws Exception {
        ThreadDumper threadDumper = new ThreadDumper(LONG_WAIT, log);

        Thread thread = new Thread(threadDumper, "Thread-dumping thread from + " + this.getClass().getSimpleName());
        thread.setDaemon(true);
        thread.start();

        Thread.sleep(MEDIUM_WAIT); // let the other thread get started.

        thread.interrupt();

        thread.join();

        verify(log).error(eq("Thread logging thread interrupted. Will log threads immediately."), any(InterruptedException.class));
        verify(log).error(anyString()); // the actual thread dump.
        verifyNoMoreInteractions(log);
    }

    @Test
    public void timeoutsLessThanOneShouldBecomeOne() {
        assertEquals(SHORT_WAIT, new ThreadDumper(0).getTimeout());
        assertEquals(SHORT_WAIT, new ThreadDumper(-SHORT_WAIT).getTimeout());
    }

    @Test
    public void nothingShouldHappenWhenThreadCancelledAfterLogging() throws Exception {
        ThreadDumper threadDumper = new ThreadDumper(SHORT_WAIT, log);

        Thread thread = new Thread(threadDumper, "Thread-dumping thread from + " + this.getClass().getSimpleName());
        thread.setDaemon(true);
        thread.start();
        thread.join();

        threadDumper.cancelDump();

        verify(log).error(anyString()); // the actual thread dump.
        verifyNoMoreInteractions(log);
    }

    @Test(timeout = 60000)
    public void threadDumperShouldntHangStressTest() throws Exception {
        for (int i = 0; i < 100; ++i) {
            // We don't want to accumulate 100 trials worth of invocation records
            Mockito.reset(log);

            ThreadDumper threadDumper = new ThreadDumper(SHORT_WAIT, log);

            Thread thread = new Thread(threadDumper, "Thread-dumping thread from + " + this.getClass().getSimpleName());
            thread.setDaemon(true);
            thread.start();
            thread.join();
        }
    }

    @Test
    public void spuriousWakeUpDoesNotAbortWait() throws Exception {
        // This has to be long enough that the thread overhead (startup and join) doesn't cover the effect we're
        // testing for.
        final long delay = 500;
        ThreadDumper threadDumper = new ThreadDumper(delay, log);
        Thread thread = new Thread(threadDumper, "Thread-dumping thread from + " + this.getClass().getSimpleName());
        thread.setDaemon(true);
        final long before = System.currentTimeMillis();
        thread.start();
        Thread.sleep(delay / 2);
        // Even though threadDumper is local, it's passed to another thread, so this synchronization does matter
        //noinspection SynchronizationOnLocalVariableOrMethodParameter
        synchronized (threadDumper) {
            // Unfortunately this is quite whitebox, but i don't know a better way to simulate the effect that requires
            // a while in the implementation. If it changes, this will likely still pass, but not be an effective test.
            // The only alternative i can see is to interrupt the thread but try to clear the interrupt flag before the
            // thread can read it and exit legimiately, which is likely even more fragile.
            threadDumper.notify();
        }
        thread.join();
        final long after = System.currentTimeMillis();
        assertThat(after - before, greaterThanOrEqualTo(delay));
    }

    @Test
    public void cancellingBeforeLoggingShouldNotLog() throws Exception {
        ThreadDumper threadDumper = new ThreadDumper(LONG_WAIT, log);

        Thread thread = new Thread(threadDumper, "Thread-dumping thread from + " + this.getClass().getSimpleName());
        thread.setDaemon(true);
        thread.start();

        Thread.sleep(MEDIUM_WAIT);

        threadDumper.cancelDump();
        thread.join();

        verify(log).info(eq("Thread logging requested and JIRA shutdown completed within specified timeout " + LONG_WAIT + "ms. Hooray!"));
        verifyNoMoreInteractions(log);
    }
}
