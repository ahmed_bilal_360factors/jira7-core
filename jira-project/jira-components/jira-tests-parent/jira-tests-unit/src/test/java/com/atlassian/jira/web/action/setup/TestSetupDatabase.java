package com.atlassian.jira.web.action.setup;

import com.atlassian.jira.config.database.DatabaseType;
import com.google.common.collect.ImmutableSet;
import org.junit.Test;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;

public class TestSetupDatabase {

    @Test
    public void testListOfDatabasesIncludesAllKnownDatabases() throws Exception {
        final Map<String, DatabaseType> sut = SetupDatabase.databaseTypeMap;

        final List<DatabaseType> knownTypesExceptEmbedded = DatabaseType.knownTypes().stream()
                .filter(input -> input != DatabaseType.HSQL && input != DatabaseType.H2)
                .collect(Collectors.toList());

        assertEquals(ImmutableSet.copyOf(knownTypesExceptEmbedded.stream()
                .map(DatabaseType::getTypeName)
                .collect(Collectors.toList())), sut.keySet());

        for (DatabaseType databaseType : knownTypesExceptEmbedded) {
            assertEquals(databaseType, sut.get(databaseType.getTypeName()));
        }
    }
}