package com.atlassian.jira.upgrade.tasks.role;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.jira.application.ApplicationRoleManager;
import com.atlassian.jira.test.util.lic.core.CoreLicenses;
import com.atlassian.jira.test.util.lic.sd.ServiceDeskLicenses;
import com.atlassian.jira.test.util.lic.software.SoftwareLicenses;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import java.util.Collection;
import java.util.Set;

import static com.atlassian.jira.upgrade.tasks.role.TestUtils.toLicense;
import static com.google.common.collect.ImmutableSet.of;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@RunWith(Parameterized.class)
public class DefaultApplicationServiceImplTestAllLicenseCases {
    private static final ApplicationKey JIRA_CORE = ApplicationKey.valueOf("jira-core");
    private static final ApplicationKey JIRA_SOFTWARE = ApplicationKey.valueOf("jira-software");

    @Rule
    public final MethodRule initMockito = MockitoJUnit.rule();

    @Mock
    private ApplicationRolesDao rolesDao;
    @Mock
    private LicenseDao licenseDao;
    @Mock
    private DefaultApplicationDao defaultApplicationDao;
    @Mock
    private MigrationLogDao migrationLogDao;
    @Mock
    private ApplicationRoleManager applicationRoleManager;

    static final License swLicense = toLicense(SoftwareLicenses.LICENSE_SOFTWARE);
    static final License coreLicense = toLicense(CoreLicenses.LICENSE_CORE);
    static final License sdLicense = toLicense(ServiceDeskLicenses.LICENSE_SERVICE_DESK_TBP);

    private DefaultApplicationService defaultApplicationService;

    private Licenses licenses;
    private Set<ApplicationKey> expectedApplicationKeys;

    @Parameterized.Parameters(name = "{index}: verifyMigrate({0}.) = {1}")
    public static Collection<Object[]> data() {
        return ImmutableList.copyOf(new Object[][]{

                {licenses(), of()},
                {licenses(coreLicense), of(JIRA_CORE)},
                {licenses(swLicense), of(JIRA_SOFTWARE)},
                {licenses(coreLicense, swLicense), of(JIRA_SOFTWARE)},
                {licenses(sdLicense), of()},
                {licenses(coreLicense, sdLicense), of(JIRA_CORE)},
                {licenses(swLicense, sdLicense), of(JIRA_SOFTWARE)},
                {licenses(coreLicense, swLicense, sdLicense), of(JIRA_SOFTWARE)},
        });
    }


    public DefaultApplicationServiceImplTestAllLicenseCases(final Licenses licenses, final Set<ApplicationKey> expectedApplicationKeys) {
        this.licenses = licenses;
        this.expectedApplicationKeys = expectedApplicationKeys;
    }

    @Before
    public void setUp() throws Exception {
        defaultApplicationService = new DefaultApplicationServiceImpl(migrationLogDao, licenseDao, defaultApplicationDao, applicationRoleManager);
    }

    @Test
    public void test() {
        when(licenseDao.getLicenses()).thenReturn(licenses);
        defaultApplicationService.setApplicationsAsDefaultDuring6xTo7xUpgrade();
        if (expectedApplicationKeys.isEmpty()) {
            verifyNoMoreInteractions(defaultApplicationDao);
        } else {
            verify(defaultApplicationDao).setApplicationsAsDefault(eq(expectedApplicationKeys));
        }
    }

    private static Licenses licenses(final License... licenses) {
        final Set<License> licensesSet = ImmutableSet.copyOf(licenses);
        return new Licenses(licensesSet);
    }

}