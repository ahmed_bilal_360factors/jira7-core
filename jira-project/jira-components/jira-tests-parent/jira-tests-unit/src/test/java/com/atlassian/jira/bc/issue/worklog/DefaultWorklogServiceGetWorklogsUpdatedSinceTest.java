package com.atlassian.jira.bc.issue.worklog;

import com.atlassian.jira.bc.issue.util.VisibilityValidator;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.worklog.Worklog;
import com.atlassian.jira.issue.worklog.WorklogImpl;
import com.atlassian.jira.issue.worklog.WorklogManager;
import com.atlassian.jira.mock.issue.MockIssue;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.security.roles.ProjectRoleManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.util.JiraDurationUtils;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.LongStream;

import static com.atlassian.jira.issue.worklog.WorklogManager.WORKLOG_UPDATE_DATA_PAGE_SIZE;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class DefaultWorklogServiceGetWorklogsUpdatedSinceTest {
    @Rule
    public final MethodRule initMockito = MockitoJUnit.rule();

    private final int pageLimit = WORKLOG_UPDATE_DATA_PAGE_SIZE;
    @Mock
    private GroupManager groupManager;
    @Mock
    private WorklogManager worklogManager;

    private final ApplicationUser user = new MockApplicationUser("user");
    private final Issue issue = new MockIssue(1L);

    private WorklogService worklogService;

    @Before
    public void setUp() throws Exception {
        worklogService = new DefaultWorklogService(worklogManager,
                mock(PermissionManager.class),
                mock(VisibilityValidator.class),
                mock(ProjectRoleManager.class), mock(IssueManager.class),
                mock(TimeTrackingConfiguration.class),
                mock(JiraDurationUtils.class),
                groupManager);
    }

    @Test
    public void whenWorklogUpdatedDateIsEqualToSinceWorklogIsReturned() {
        long since = 1000L;
        assumeWorklogsExists(ImmutableList.of(createWorklog(1, since)));

        WorklogChangedSincePage<Worklog> worklogsUpdatedSince = worklogService.getWorklogsUpdatedSince(user, since);
        assertThat(worklogsUpdatedSince.getChangedSince(), hasSize(1));
    }

    @Test
    public void untilIsEqualToLatestChangeDate() {
        long since = 1000L;
        long until = 2000L;
        assumeWorklogsExists(ImmutableList.of(createWorklog(1, since), createWorklog(2, until)));

        WorklogChangedSincePage<Worklog> worklogsUpdatedSince = worklogService.getWorklogsUpdatedSince(user, since);
        assertThat(worklogsUpdatedSince.getUntil(), is(until));
    }

    @Test
    public void whenThereIsNoWorklogsEmptyPageIsReturnedWhichIsLast() {
        int numberOfWorklogs = 0;
        assumeWorklogsExists(numberOfWorklogs);

        WorklogChangedSincePage<Worklog> worklogsUpdatedSince = worklogService.getWorklogsUpdatedSince(user, 0L);
        assertThat(worklogsUpdatedSince.isLastPage(), is(true));
        assertThat(worklogsUpdatedSince.getChangedSince(), hasSize(numberOfWorklogs));
    }

    @Test
    public void whenNumberOfWorklogsIsAlmostEqualToThePageSizeSinglePageIsReturnedAndItIsLast() {
        assumeWorklogsExists(pageLimit - 1);

        WorklogChangedSincePage<Worklog> worklogsUpdatedSince = worklogService.getWorklogsUpdatedSince(user, 0L);
        assertThat(worklogsUpdatedSince.isLastPage(), is(true));
        assertThat(worklogsUpdatedSince.getChangedSince(), hasSize(pageLimit - 1));
    }

    @Test
    public void whenNumberOfWorklogsIsAlmostEqualToThePageSizeAndWorklogsComesFromTwoPagesSinglePageIsReturnedAndItNotLast() {
        // the page isn't be the last one because we only know that the page is last if the manger returned less worklogs than page limit is.
        assumeWorklogsExists(pageLimit);

        WorklogChangedSincePage<Worklog> worklogsUpdatedSince = worklogService.getWorklogsUpdatedSince(user, 0L);
        assertThat(worklogsUpdatedSince.isLastPage(), is(false));
        assertThat(worklogsUpdatedSince.getChangedSince(), hasSize(pageLimit));
    }

    @Test
    public void whenNumberOfWorklogsIsEqualToThePageSizeAndWorklogsComesFromTwoPagesSinglePageIsReturnedAndItIsLast() {
        // in that case we know that this page is last because second page from manger wasn't full.
        ImmutableList<WorklogImpl> worklogs = ImmutableList.copyOf(Iterables.concat(
                createWorklogs(0, pageLimit - 10),
                ImmutableList.of(createPrivateWorklog(pageLimit - 10, pageLimit)),
                createWorklogs(pageLimit, 10)
        ));
        assumeWorklogsExists(worklogs);

        WorklogChangedSincePage<Worklog> worklogsUpdatedSince = worklogService.getWorklogsUpdatedSince(user, 0L);
        assertThat(worklogsUpdatedSince.getChangedSince(), hasSize(pageLimit));
        assertThat(worklogsUpdatedSince.isLastPage(), is(true));
    }

    @Test
    public void whenThereIsMoreWorklogsThanBathSizeReturnedPageIsNotLast() {
        int numberOfWorklogs = pageLimit + 1;
        assumeWorklogsExists(numberOfWorklogs);

        WorklogChangedSincePage<Worklog> worklogsUpdatedSince = worklogService.getWorklogsUpdatedSince(user, 0L);
        assertThat(worklogsUpdatedSince.getChangedSince(), hasSize(WORKLOG_UPDATE_DATA_PAGE_SIZE));
        assertThat(worklogsUpdatedSince.isLastPage(), is(false));
    }

    @Test
    public void whenThereIsMoreWorklogsThanPageSizeReturnedPageIsNotLastEventIfSecondPageFromMangerWasLastOne() {
        ImmutableList<WorklogImpl> worklogs = ImmutableList.copyOf(Iterables.concat(
                createWorklogs(0, pageLimit - 10),
                ImmutableList.of(createPrivateWorklog(pageLimit - 10, pageLimit)),
                createWorklogs(pageLimit, 100)
        ));
        assumeWorklogsExists(worklogs);

        WorklogChangedSincePage<Worklog> worklogsUpdatedSince = worklogService.getWorklogsUpdatedSince(user, 0L);
        assertThat(worklogsUpdatedSince.isLastPage(), is(false));
    }

    @Test
    public void whenThereIsMoreWorklogsThanPageSizeWorklogsFromLastMillisecondOfFirstPageShouldBeReturnedAtBeginningOfSecond() {
        int numberOfWorklogsInSecondPage = pageLimit / 2;
        int numberOfWorklogs = pageLimit + numberOfWorklogsInSecondPage;
        assumeWorklogsExists(numberOfWorklogs);

        WorklogChangedSincePage<Worklog> batchOfWorklogs1 = worklogService.getWorklogsUpdatedSince(user, 0L);
        WorklogChangedSincePage<Worklog> batchOfWorklogs2 = worklogService.getWorklogsUpdatedSince(user, batchOfWorklogs1.getUntil());
        assertThat(Iterables.getLast(batchOfWorklogs1.getChangedSince()), is(batchOfWorklogs2.getChangedSince().get(0)));
        assertThat(batchOfWorklogs2.getChangedSince(), hasSize(numberOfWorklogsInSecondPage + 1));
        assertThat(batchOfWorklogs2.isLastPage(), is(true));
    }

    @Test
    public void noWorklogsShouldBeReturnedIfUserDoesNotHavePermissionToAnyone() {
        assumeWorklogsExists(createPrivateWorklogs(0, pageLimit));

        WorklogChangedSincePage<Worklog> pageOfWorklogs = worklogService.getWorklogsUpdatedSince(user, 0L);
        assertThat(pageOfWorklogs.getChangedSince(), hasSize(0));
        assertThat(pageOfWorklogs.isLastPage(), is(true));
    }

    @Test
    public void numberOfWorklogsPerPageShouldApplyToWorklogsThatUserHasPermissionTo() {
        int numberOfWorklogs = 20;
        ImmutableList<WorklogImpl> worklogs = ImmutableList.copyOf(Iterables.concat(
                createWorklogs(0, 10),
                createPrivateWorklogs(10, pageLimit),
                createWorklogs(10 + pageLimit, 10)
        ));
        assumeWorklogsExists(worklogs);

        WorklogChangedSincePage<Worklog> pageOfWorklogs = worklogService.getWorklogsUpdatedSince(user, 0L);
        assertThat(pageOfWorklogs.getChangedSince(), hasSize(numberOfWorklogs));
        assertThat(pageOfWorklogs.isLastPage(), is(true));
    }

    @Test
    public void worklogsWithTheSameChangeTimeReturnedAtEdgesOfPagesReturnedByManagerShouldNotBeDuplicated() {
        // Worklogs which had changTime equal to until of the first page will have changeTime equal to since of the next page,
        // so they appear on the both page returned from database, but service shouldn't return them duplicated.

        int halfPageSize = pageLimit / 2;
        int worklogsOnSecondDataBasePage = 10;
        ImmutableList<WorklogImpl> worklogs = ImmutableList.copyOf(Iterables.concat(
                createWorklogs(0, halfPageSize),
                createPrivateWorklogs(halfPageSize, halfPageSize - 2),
                ImmutableList.of(   // the next two worklogs will be returned in two pages returned from database
                        createWorklog(10000L, pageLimit),
                        createWorklog(10001L, pageLimit)
                ),
                createWorklogs(pageLimit + 1, worklogsOnSecondDataBasePage)
        ));
        assumeWorklogsExists(worklogs);

        WorklogChangedSincePage<Worklog> pageOfWorklogs = worklogService.getWorklogsUpdatedSince(user, 0L);
        assertThat(pageOfWorklogs.getChangedSince(), hasSize(halfPageSize + worklogsOnSecondDataBasePage + 2));
    }

    @Test
    public void noMoreWorklogsIsReturnedThanPageSizeLimitIs() {
        ImmutableList<WorklogImpl> worklogs = ImmutableList.copyOf(Iterables.concat(
                createWorklogs(0, pageLimit - 1),
                ImmutableList.of(createPrivateWorklog(pageLimit - 1, pageLimit - 1)),
                createWorklogs(pageLimit, 100)
        ));
        assumeWorklogsExists(worklogs);

        WorklogChangedSincePage<Worklog> pageOfWorklogs = worklogService.getWorklogsUpdatedSince(user, 0L);
        assertThat(pageOfWorklogs.getChangedSince(), hasSize(pageLimit));
        assertThat(pageOfWorklogs.isLastPage(), is(false));
    }

    private void assumeWorklogsExists(int numberOfWorklogs) {
        List<WorklogImpl> worklogs = createWorklogs(0, numberOfWorklogs);
        assumeWorklogsExists(worklogs);
    }

    private List<WorklogImpl> createWorklogs(int startFrom, int numberOfWorklogs) {
        Comparator<WorklogImpl> compareWorklogsByUpdateDate = (w1, w2) -> w1.getUpdated().compareTo(w2.getUpdated());
        return LongStream.range(startFrom, startFrom + numberOfWorklogs)
                .mapToObj((id) -> createWorklog(id, id))
                .sorted(compareWorklogsByUpdateDate)
                .collect(Collectors.<WorklogImpl>toList());
    }

    private List<WorklogImpl> createPrivateWorklogs(int startFrom, int numberOfWorklogs) {
        Comparator<WorklogImpl> compareWorklogsByUpdateDate = (w1, w2) -> w1.getUpdated().compareTo(w2.getUpdated());
        return LongStream.range(startFrom, startFrom + numberOfWorklogs)
                .mapToObj((id) -> createPrivateWorklog(id, id))
                .sorted(compareWorklogsByUpdateDate)
                .collect(Collectors.<WorklogImpl>toList());
    }

    private void assumeWorklogsExists(final List<WorklogImpl> worklogs) {
        when(worklogManager.getWorklogsUpdatedSince(anyLong())).then(invocationOnMock -> {
            long since = (long) invocationOnMock.getArguments()[0];
            return worklogs.stream()
                    .filter((worklog) -> worklog.getUpdated().getTime() >= since)
                    .limit(pageLimit)
                    .collect(Collectors.toList());
        });
    }

    private WorklogImpl createWorklog(long id, long updated) {
        return new WorklogImpl(worklogManager, issue, id, "", "", null, null, null, 0L, "", null, new Date(updated));
    }

    private WorklogImpl createPrivateWorklog(long id, long updated) {
        final String groupname = "super admins";
        when(groupManager.isUserInGroup(any(ApplicationUser.class), eq(groupname))).thenReturn(false);
        return new WorklogImpl(worklogManager, issue, id, "", "", null, groupname, null, 0L, "", null, new Date(updated));
    }
}