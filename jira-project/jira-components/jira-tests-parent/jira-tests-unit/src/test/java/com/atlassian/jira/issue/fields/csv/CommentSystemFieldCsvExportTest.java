package com.atlassian.jira.issue.fields.csv;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.comments.Comment;
import com.atlassian.jira.issue.comments.CommentManager;
import com.atlassian.jira.issue.export.FieldExportParts;
import com.atlassian.jira.issue.fields.CommentSystemField;
import com.atlassian.jira.issue.fields.csv.util.StubCsvDateFormatter;
import com.atlassian.jira.issue.views.util.csv.CsvDateFormatter;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.util.NoopI18nHelper;
import com.google.common.base.Joiner;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Stream;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class CommentSystemFieldCsvExportTest {

    @Rule
    public final RuleChain mockito = MockitoMocksInContainer.forTest(this);

    @Mock
    private CommentManager commentManager;

    @Spy
    private CsvDateFormatter csvDateFormatter = new StubCsvDateFormatter();

    @Mock
    private Issue issue;

    @Mock
    private JiraAuthenticationContext authenticationContext;

    @InjectMocks
    private CommentSystemField field;

    @Mock
    private ApplicationUser applicationUser;

    @Before
    public void setUp() {
        when(authenticationContext.getLoggedInUser()).thenReturn(applicationUser);
        when(authenticationContext.getI18nHelper()).thenReturn(new NoopI18nHelper());
    }

    @Test
    public void allCommentsAreExportedAsSeparateValues() {
        final Comment c1 = comment("a", null, null);
        final Comment c2 = comment("b", null, null);

        when(commentManager.streamComments(eq(applicationUser), eq(issue))).thenReturn(Stream.of(c1, c2));

        assertExported("a", "b");
    }

    @Test
    public void dateAndAuthorAreExportedCorrectly() {
        final Comment c1 = comment("a", new Date(), "admin");
        final Comment c2 = comment("b", new Date(), null);
        final Comment c3 = comment("c", null, "admin");

        when(commentManager.streamComments(eq(applicationUser), eq(issue))).thenReturn(Stream.of(c1, c2, c3));

        assertExported(exported(c1), exported(c2), exported(c3));
    }

    private void assertExported(String... values) {
        final FieldExportParts representation = field.getRepresentationFromIssue(issue);

        assertThat(representation.getParts(), hasSize(1));
        assertThat(representation.getParts().get(0).getValues()::iterator, contains(values));
    }

    private Comment comment(final String body, final Date created, final String author) {
        final Comment comment = mock(Comment.class);

        when(comment.getBody()).thenReturn(body);
        when(comment.getCreated()).thenReturn(created);

        if (author != null) {
            when(comment.getAuthorApplicationUser()).thenReturn(new MockApplicationUser(author));
        }

        return comment;
    }

    private String exported(final Comment comment) {
        final List<String> parts = new ArrayList<>();
        final ApplicationUser author = comment.getAuthorApplicationUser();

        if (author != null && comment.getCreated() != null) {
            parts.add(csvDateFormatter.formatDateTime(comment.getCreated()));
            parts.add(author.getUsername());
        }

        parts.add(comment.getBody());

        return Joiner.on(";").join(parts);
    }
}
