package com.atlassian.jira.dashboard;

import com.atlassian.gadgets.GadgetId;
import com.atlassian.gadgets.dashboard.DashboardId;
import com.atlassian.jira.config.properties.ApplicationProperties;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.when;

public class TestDashboardUtil {
    @Rule
    public final MethodRule initMockito = MockitoJUnit.rule();
    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Mock
    private ApplicationProperties applicationProperties;


    @Test
    public void testToLong() {
        assertNull(DashboardUtil.toLong((DashboardId) null));
        assertNull(DashboardUtil.toLong((GadgetId) null));

        assertEquals(Long.valueOf(100), DashboardUtil.toLong(DashboardId.valueOf("100")));
        assertEquals(Long.valueOf(10220), DashboardUtil.toLong(GadgetId.valueOf("10220")));
    }

    @Test
    public void testTextToDashboardId() {
        exception.expect(Exception.class);
        DashboardUtil.toLong(DashboardId.valueOf("abc"));
    }

    @Test
    public void testTextToGadgetId() {
        exception.expect(Exception.class);
        DashboardUtil.toLong(GadgetId.valueOf("abc"));
    }

    @Test
    public void testGetMaxGadgets() {
        when(applicationProperties.getDefaultBackedString("jira.dashboard.max.gadgets")).thenReturn("25");

        final int max = DashboardUtil.getMaxGadgets(applicationProperties);
        assertEquals(25, max);
    }

    @Test
    public void testGetMaxGadgetsInvalid() {
        when(applicationProperties.getDefaultBackedString("jira.dashboard.max.gadgets")).thenReturn("asdfasdf");

        final int max = DashboardUtil.getMaxGadgets(applicationProperties);
        assertEquals(20, max);
    }

    @Test
    public void testGetMaxGadgetsNotSet() {
        when(applicationProperties.getDefaultBackedString("jira.dashboard.max.gadgets")).thenReturn(null);

        final int max = DashboardUtil.getMaxGadgets(applicationProperties);
        assertEquals(20, max);
    }
}
