package com.atlassian.jira.issue.fields.rest;

import com.atlassian.jira.issue.fields.rest.json.SuggestionBeanFactory;
import com.atlassian.jira.issue.fields.rest.json.beans.SuggestionBean;
import com.atlassian.jira.issue.fields.rest.json.beans.SuggestionGroupBean;
import com.atlassian.jira.mock.i18n.MockI18nHelper;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.UserHistoryItem;
import com.atlassian.jira.user.UserProjectHistoryManager;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import java.util.List;
import java.util.Optional;

import static com.atlassian.jira.permission.ProjectPermissions.CREATE_ISSUES;
import static com.google.common.collect.Lists.newArrayList;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestProjectSuggestionProviderImpl {
    @Rule
    public final MethodRule initMockito = MockitoJUnit.rule();

    @Mock
    private SuggestionBeanFactory suggestionBeanFactory;
    @Mock
    private JiraAuthenticationContext authenticationContext;
    @Mock
    private UserProjectHistoryManager projectHistoryManager;
    @Mock
    private PermissionManager permissionManager;
    @Mock
    private ProjectManager projectManager;

    private ProjectSuggestionProvider provider;

    private static final ApplicationUser FRED = new MockApplicationUser("fred");
    private static final Long SELECTED_PID = 10002L;
    private static final Project PROJECT_A = new MockProject(10000, "A");
    private static final Project PROJECT_B = new MockProject(10001, "B");
    private static final Project PROJECT_C = new MockProject(SELECTED_PID, "C");
    private static final List<UserHistoryItem> RECENT_ITEMS = newArrayList(
            new UserHistoryItem(UserHistoryItem.PROJECT, "10000"),
            new UserHistoryItem(UserHistoryItem.PROJECT, "10002")
    );
    private static final List<Project> RECENT = newArrayList(PROJECT_A, PROJECT_C);
    private static final List<Project> ALL = newArrayList(PROJECT_A, PROJECT_B, PROJECT_C);

    @Before
    public void setUp() throws Exception {
        when(authenticationContext.getLoggedInUser()).thenReturn(FRED);
        when(authenticationContext.getI18nHelper()).thenReturn(new MockI18nHelper());

        provider = new ProjectSuggestionProviderImpl(suggestionBeanFactory, authenticationContext, projectHistoryManager, permissionManager, projectManager);
    }

    @Test
    public void suggestionsShouldIncludeAllProjectsWithoutPermissionChecks() {
        setupMocksForRetrievingProjectsWithNoPermissionChecks();

        List<SuggestionBean> expectedSuggestionsRecent = newArrayList(toBean(PROJECT_A, false), toBean(PROJECT_C, true));
        List<SuggestionBean> expectedSuggestionsAll = newArrayList(toBean(PROJECT_A, false), toBean(PROJECT_B, false), toBean(PROJECT_C, true));
        when(suggestionBeanFactory.projectSuggestions(eq(RECENT), eq(Optional.empty()))).thenReturn(expectedSuggestionsRecent);
        when(suggestionBeanFactory.projectSuggestions(ALL, Optional.empty())).thenReturn(expectedSuggestionsAll);

        List<SuggestionGroupBean> suggestions = provider.getProjectPickerSuggestions(Optional.empty(), true);

        assertRecentAndAllSuggestionsReturned(suggestions, expectedSuggestionsRecent, expectedSuggestionsAll);
        verify(projectHistoryManager, never()).getProjectHistoryWithPermissionChecks(any(), any());
        verify(permissionManager, never()).getProjects(any(), any());
    }

    @Test
    public void suggestionsDontIncludeRecentProjectsWhenSpecified() {
        setupAllProjects();

        List<SuggestionBean> expectedSuggestions = newArrayList(toBean(PROJECT_A, false), toBean(PROJECT_B, false), toBean(PROJECT_C, false));
        when(suggestionBeanFactory.projectSuggestions(ALL, Optional.empty())).thenReturn(expectedSuggestions);

        List<SuggestionGroupBean> suggestions = provider.getProjectPickerSuggestions(CREATE_ISSUES, Optional.empty(), false);

        assertThat(suggestions.size(), is(1));
        assertThat(suggestions.get(0).getLabel(), is("menu.project.all"));
        assertThat(suggestions.get(0).getItems(), is(expectedSuggestions));

        verify(projectHistoryManager, never()).getProjectHistoryWithPermissionChecks(any(), any());
    }

    @Test
    public void getAllSuggestionsIncludesRecentAndAllProjects() {
        setupRecentAndAllProjects();

        List<SuggestionBean> expectedSuggestionsRecent = newArrayList(toBean(PROJECT_A, false), toBean(PROJECT_C, false));
        List<SuggestionBean> expectedSuggestionsAll = newArrayList(toBean(PROJECT_A, false), toBean(PROJECT_B, false), toBean(PROJECT_C, false));
        when(suggestionBeanFactory.projectSuggestions(RECENT, Optional.empty())).thenReturn(expectedSuggestionsRecent);
        when(suggestionBeanFactory.projectSuggestions(ALL, Optional.empty())).thenReturn(expectedSuggestionsAll);

        List<SuggestionGroupBean> suggestions = provider.getProjectPickerSuggestions(CREATE_ISSUES, Optional.empty(), true);

        assertRecentAndAllSuggestionsReturned(suggestions, expectedSuggestionsRecent, expectedSuggestionsAll);
    }

    @Test
    public void selectedProjectIsSelectedInResult() {
        setupRecentAndAllProjects();

        List<SuggestionBean> expectedSuggestionsRecent = newArrayList(toBean(PROJECT_A, false), toBean(PROJECT_C, true));
        List<SuggestionBean> expectedSuggestionsAll = newArrayList(toBean(PROJECT_A, false), toBean(PROJECT_B, false), toBean(PROJECT_C, true));
        when(suggestionBeanFactory.projectSuggestions(RECENT, Optional.of(SELECTED_PID))).thenReturn(expectedSuggestionsRecent);
        when(suggestionBeanFactory.projectSuggestions(ALL, Optional.of(SELECTED_PID))).thenReturn(expectedSuggestionsAll);

        List<SuggestionGroupBean> suggestions = provider.getProjectPickerSuggestions(CREATE_ISSUES, Optional.of(SELECTED_PID), true);

        assertRecentAndAllSuggestionsReturned(suggestions, expectedSuggestionsRecent, expectedSuggestionsAll);
    }

    private void setupMocksForRetrievingProjectsWithNoPermissionChecks() {
        when(projectManager.getProjectObjects()).thenReturn(ALL);
        when(projectManager.getProjectObj(10000L)).thenReturn(PROJECT_A);
        when(projectManager.getProjectObj(10002L)).thenReturn(PROJECT_C);
        when(projectHistoryManager.getProjectHistoryWithoutPermissionChecks(FRED)).thenReturn(RECENT_ITEMS);
    }

    private void assertRecentAndAllSuggestionsReturned(final List<SuggestionGroupBean> suggestions, final List<SuggestionBean> expectedSuggestionsRecent, final List<SuggestionBean> expectedSuggestionsAll) {
        assertThat(suggestions.size(), is(2));
        assertThat(suggestions.get(0).getLabel(), is("menu.project.recent"));
        assertThat(suggestions.get(0).getItems(), is(expectedSuggestionsRecent));
        assertThat(suggestions.get(1).getLabel(), is("menu.project.all"));
        assertThat(suggestions.get(1).getItems(), is(expectedSuggestionsAll));
    }

    private void setupRecentAndAllProjects() {
        when(projectHistoryManager.getProjectHistoryWithPermissionChecks(Permissions.CREATE_ISSUE, FRED)).thenReturn(RECENT);
        setupAllProjects();
    }

    private void setupAllProjects() {
        when(permissionManager.getProjects(CREATE_ISSUES, FRED)).thenReturn(ALL);
    }

    private SuggestionBean toBean(final Project project, boolean selected) {
        return new SuggestionBean(project.getName(), Long.toString(project.getId()), "icon/" + project.getKey(), selected);
    }

}