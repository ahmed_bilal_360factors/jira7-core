package com.atlassian.jira.upgrade.tasks.role.scenarios;

import com.atlassian.jira.test.util.lic.cloud.CloudLicenses;
import com.atlassian.jira.test.util.lic.core.CoreLicenses;
import com.atlassian.jira.test.util.lic.ela.ElaLicenses;
import com.atlassian.jira.test.util.lic.other.OtherLicenses;
import com.atlassian.jira.test.util.lic.sd.ServiceDeskLicenses;
import com.atlassian.jira.test.util.lic.software.SoftwareLicenses;

public enum MigrationLicenses {
    NONE(null),
    MALFORMED_LICENSE("SomeMalformedLicense"),
    SERVICE_DESK_TBP_UNLIMITED(ServiceDeskLicenses.LICENSE_SERVICE_DESK_TBP_UNLIMITED.getLicenseString()),
    SERVICE_DESK_ABP_UNLIMITED(ServiceDeskLicenses.LICENSE_SERVICE_DESK_ABP.getLicenseString()),
    JIRA7_SERVICE_DESK_RBP_NON_BACKWARD_COMPATIBLE(ServiceDeskLicenses.LICENSE_SERVICE_DESK_JIRA7_NON_BACKWARD_COMPATIBLE.getLicenseString()),
    JIRA_6X_COMMERCIAL(OtherLicenses.LICENSE_PRE_JIRA_APP_V2_COMMERCIAL.getLicenseString()),
    SERVICE_DESK_RENAISSANCE_RBP(ServiceDeskLicenses.LICENSE_SERVICE_DESK_RBP.getLicenseString()),
    CORE_RENAISSANCE(CoreLicenses.LICENSE_CORE.getLicenseString()),
    SOFTWARE_RENAISSANCE(SoftwareLicenses.LICENSE_SOFTWARE.getLicenseString()),
    ONDEMAND_6X_SERVICEDESK_TBP(CloudLicenses.LICENSE_ONDEMAND_6x_SERVICEDESK_TBP.getLicenseString()),
    JIRA_6X_ELA_SERVICEDESK_ABP(ElaLicenses.LICENSE_ELA_6x_SERVICEDESK_ABP.getLicenseString()),
    JIRA_6X_ELA_SERVICEDESK_TBP(ElaLicenses.LICENSE_ELA_6x_SERVICEDESK_TBP.getLicenseString());

    final String encodedLicense;

    MigrationLicenses(final String encodedLicense) {
        this.encodedLicense = encodedLicense;
    }

    public String getEncodedLicense() {
        return encodedLicense;
    }
}