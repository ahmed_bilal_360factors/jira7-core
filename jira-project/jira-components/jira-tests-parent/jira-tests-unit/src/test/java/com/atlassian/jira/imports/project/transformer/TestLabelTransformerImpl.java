package com.atlassian.jira.imports.project.transformer;

import com.atlassian.jira.external.beans.ExternalLabel;
import com.atlassian.jira.imports.project.mapper.CustomFieldMapper;
import com.atlassian.jira.imports.project.mapper.ProjectImportMapper;
import com.atlassian.jira.imports.project.mapper.SimpleProjectImportIdMapper;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestLabelTransformerImpl {
    @Test
    public void testTransform() {
        LabelTransformerImpl transformer = new LabelTransformerImpl();

        ExternalLabel oldLabel = new ExternalLabel();
        oldLabel.setId("10000");
        oldLabel.setIssueId("10001");
        oldLabel.setCustomFieldId("12000");
        oldLabel.setLabel("alabel");

        final SimpleProjectImportIdMapper mockIdMapper = mock(SimpleProjectImportIdMapper.class);
        when(mockIdMapper.getMappedId("10001")).thenReturn("20001");
        final CustomFieldMapper mockCustomFieldMapper = mock(CustomFieldMapper.class);
        when(mockCustomFieldMapper.getMappedId("12000")).thenReturn("14000");
        final ProjectImportMapper mockProjectImportMapper = mock(ProjectImportMapper.class);
        when(mockProjectImportMapper.getIssueMapper()).thenReturn(mockIdMapper);
        when(mockProjectImportMapper.getCustomFieldMapper()).thenReturn(mockCustomFieldMapper);

        final ExternalLabel externalLabel = transformer.transform(mockProjectImportMapper, oldLabel);
        ExternalLabel expected = new ExternalLabel();
        expected.setIssueId("20001");
        expected.setCustomFieldId("14000");
        expected.setLabel("alabel");
        assertEquals(expected, externalLabel);
    }

}
