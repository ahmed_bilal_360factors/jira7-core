package com.atlassian.jira.jql.resolver;

import com.atlassian.jira.bc.project.component.ProjectComponent;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.collect.CollectionBuilder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.ofbiz.core.entity.GenericValue;

import java.util.Collections;
import java.util.List;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.empty;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestComponentIndexInfoResolver {
    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Mock
    private NameResolver<ProjectComponent> nameResolver;

    @InjectMocks
    ComponentIndexInfoResolver resolver;

    @Test
    public void testGetIndexedValuesStringHappyPath() throws Exception {
        final MockComponent mockComponent1 = new MockComponent(1L, "component1");
        final MockComponent mockComponent2 = new MockComponent(2L, "component1");
        when(nameResolver.getIdsFromName("component1")).thenReturn(CollectionBuilder.newBuilder("1", "2").asList());

        final List<String> result = resolver.getIndexedValues("component1");

        assertThat(result, containsInAnyOrder(mockComponent1.getId().toString(), mockComponent2.getId().toString()));
    }

    @Test
    public void testGetIndexedValuesStringIsId() throws Exception {
        when(nameResolver.getIdsFromName("2")).thenReturn(Collections.emptyList());
        when(nameResolver.idExists(2L)).thenReturn(true);

        final List<String> result = resolver.getIndexedValues("2");

        assertThat(result, contains("2"));
    }

    @Test
    public void testGetIndexedValuesStringIsIdDoesntExist() throws Exception {
        when(nameResolver.getIdsFromName("2")).thenReturn(Collections.emptyList());
        when(nameResolver.idExists(2L)).thenReturn(false);

        final List<String> result = resolver.getIndexedValues("2");

        assertEquals(0, result.size());
    }

    @Test
    public void testGetIndexedValuesStringIsNotNameOrId() throws Exception {
        when(nameResolver.getIdsFromName("abc")).thenReturn(Collections.emptyList());

        final List<String> result = resolver.getIndexedValues("abc");

        assertEquals(0, result.size());
    }

    @Test
    public void testGetIndexedValuesLongExists() throws Exception {
        when(nameResolver.idExists(2L)).thenReturn(true);

        final List<String> result = resolver.getIndexedValues(2L);

        assertEquals(1, result.size());
        assertTrue(result.contains("2"));
        assertThat(result, contains("2"));
    }

    @Test
    public void testGetIndexedValuesLongIdIsName() throws Exception {
        final MockComponent mockComponent1 = new MockComponent(2L, "100");
        final MockComponent mockComponent2 = new MockComponent(1L, "100");
        when(nameResolver.idExists(100L)).thenReturn(false);
        when(nameResolver.getIdsFromName("100")).thenReturn(CollectionBuilder.newBuilder("2", "1").asList());

        final List<String> result = resolver.getIndexedValues(100L);

        assertThat(result, containsInAnyOrder(mockComponent1.getId().toString(), mockComponent2.getId().toString()));
    }

    @Test
    public void testGetIndexedValuesLongIdIsNameDoesntExist() throws Exception {
        when(nameResolver.idExists(100L)).thenReturn(false);
        nameResolver.getIdsFromName("100");
        when(nameResolver.getIdsFromName("100")).thenReturn(Collections.emptyList());

        final List<String> result = resolver.getIndexedValues(100L);

        assertThat(result, empty());
    }

    @Test
    public void testGetIndexedValue() throws Exception {
        final MockComponent mockComponent1 = new MockComponent(1L, "Component 1");

        final String indexedValue = resolver.getIndexedValue(mockComponent1);
        assertEquals("1", indexedValue);
        exception.expect(IllegalArgumentException.class);
        resolver.getIndexedValue(null);
    }

    static class MockComponent implements ProjectComponent {
        private final Long id;
        private final String name;

        MockComponent(Long id, String name) {
            this.id = id;
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public Long getId() {
            return id;
        }

        public String getDescription() {
            return null;
        }

        public String getLead() {
            return null;
        }

        @Override
        public ApplicationUser getComponentLead() {
            throw new UnsupportedOperationException("Not implemented");
        }

        public Long getProjectId() {
            return null;
        }

        public long getAssigneeType() {
            return 0;
        }

        public GenericValue getGenericValue() {
            return null;
        }
    }
}
