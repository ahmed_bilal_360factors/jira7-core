package com.atlassian.jira.scheme;

import com.atlassian.jira.mock.ofbiz.MockGenericValue;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.ofbiz.core.entity.GenericValue;

import java.util.Arrays;
import java.util.Collection;

import static com.atlassian.jira.permission.ProjectPermissions.ADMINISTER_PROJECTS;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * @since v6.3
 */
@RunWith(MockitoJUnitRunner.class)
public class TestDefaultSchemeFactory {
    private DefaultSchemeFactory defaultSchemeFactory = new DefaultSchemeFactory();

    @Test
    public void permissionSchemeEntityWithSystemPermissionKeyGetsSystemPermissionId() {
        MockGenericValue entityGv = new MockGenericValue("SchemePermissions");
        entityGv.set("id", 1L);
        entityGv.set("permissionKey", ADMINISTER_PROJECTS.permissionKey());
        entityGv.set("type", "group");
        entityGv.set("parameter", "test");

        Collection<SchemeEntity> entities = defaultSchemeFactory.convertToSchemeEntities(Arrays.<GenericValue>asList(entityGv), 100L, true);
        assertThat(entities.size(), equalTo(1));

        SchemeEntity entity = entities.iterator().next();
        assertThat(entity.getId(), equalTo(1L));
        assertThat(entity.getType(), equalTo("group"));
        assertThat(entity.getParameter(), equalTo("test"));
        assertThat(entity.getEntityTypeId(), equalTo((Object) ADMINISTER_PROJECTS));
    }

    @Test
    public void permissionSchemeEntityWithMissingPermissionKeyGetsIgnored() {
        MockGenericValue entityGv = new MockGenericValue("SchemePermissions");
        entityGv.set("id", 1L);
        entityGv.set("type", "group");
        entityGv.set("parameter", "test");

        Collection<SchemeEntity> entities = defaultSchemeFactory.convertToSchemeEntities(Arrays.<GenericValue>asList(entityGv), 100L, true);
        assertThat(entities.isEmpty(), is(true));
    }
}
