package com.atlassian.jira.project.template.descriptor;

import com.atlassian.jira.plugin.webfragment.conditions.AbstractWebCondition;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.user.ApplicationUser;

public class MockCondition extends AbstractWebCondition {
    public static boolean SHOULD_DISPLAY = true;

    @Override
    public boolean shouldDisplay(ApplicationUser applicationUser, JiraHelper jiraHelper) {
        return SHOULD_DISPLAY;
    }
}
