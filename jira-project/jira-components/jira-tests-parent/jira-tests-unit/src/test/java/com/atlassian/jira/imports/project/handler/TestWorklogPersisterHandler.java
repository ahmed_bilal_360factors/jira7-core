package com.atlassian.jira.imports.project.handler;

import com.atlassian.jira.external.beans.ExternalWorklog;
import com.atlassian.jira.imports.project.ProjectImportPersister;
import com.atlassian.jira.imports.project.core.BackupSystemInformation;
import com.atlassian.jira.imports.project.core.ProjectImportResultsImpl;
import com.atlassian.jira.imports.project.mapper.ProjectImportMapper;
import com.atlassian.jira.imports.project.mapper.ProjectImportMapperImpl;
import com.atlassian.jira.imports.project.parser.WorklogParser;
import com.atlassian.jira.imports.project.transformer.WorklogTransformer;
import com.atlassian.jira.junit.rules.InitMockitoMocks;
import com.atlassian.jira.web.bean.MockI18nBean;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.mockito.Mock;

import static com.atlassian.jira.imports.project.parser.WorklogParser.WORKLOG_ENTITY_NAME;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.emptyIterable;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

/**
 * @since v3.13
 */
public class TestWorklogPersisterHandler {
    @Rule
    public TestRule initMockitoMocks = new InitMockitoMocks(this);

    private final ProjectImportMapper projectImportMapper = new ProjectImportMapperImpl(null, null);

    private final ExternalWorklog externalWorklog = new ExternalWorklog();

    {
        externalWorklog.setIssueId("34");
        externalWorklog.setId("12");
    }

    @Mock
    private WorklogParser mockWorklogParser;
    @Mock
    private WorklogTransformer mockWorklogTransformer;
    @Mock
    private ProjectImportPersister mockProjectImportPersister;
    @Mock
    private BackupSystemInformation mockBackupSystemInformation;


    @Test
    public void testHandle() throws Exception {
        when(mockWorklogParser.parse(null)).thenReturn(externalWorklog);
        when(mockWorklogParser.getEntityRepresentation(externalWorklog)).thenReturn(null);
        when(mockWorklogTransformer.transform(projectImportMapper, externalWorklog)).thenReturn(externalWorklog);
        when(mockProjectImportPersister.createEntity(null)).thenReturn(12L);


        final ProjectImportResultsImpl projectImportResults = new ProjectImportResultsImpl(0, 0, 0, 0, null);
        WorklogPersisterHandler worklogPersisterHandler = new WorklogPersisterHandler(mockProjectImportPersister, projectImportMapper, projectImportResults, null, new ExecutorForTests()) {
            WorklogParser getWorklogParser() {
                return mockWorklogParser;
            }

            WorklogTransformer getWorklogTransformer() {
                return mockWorklogTransformer;
            }
        };

        worklogPersisterHandler.handleEntity(WORKLOG_ENTITY_NAME, null);
        worklogPersisterHandler.handleEntity("NOTWorklog", null);

        assertThat(projectImportResults.getErrors(), emptyIterable());
    }

    @Test
    public void testHandleErrorAddingWorklog() throws Exception {
        when(mockWorklogParser.parse(null)).thenReturn(externalWorklog);
        when(mockWorklogParser.getEntityRepresentation(externalWorklog)).thenReturn(null);
        when(mockWorklogTransformer.transform(projectImportMapper, externalWorklog)).thenReturn(externalWorklog);
        when(mockProjectImportPersister.createEntity(null)).thenReturn(null);
        when(mockBackupSystemInformation.getIssueKeyForId("34")).thenReturn("TST-1");

        final ProjectImportResultsImpl projectImportResults = new ProjectImportResultsImpl(0, 0, 0, 0, new MockI18nBean());
        WorklogPersisterHandler worklogPersisterHandler = new WorklogPersisterHandler(mockProjectImportPersister, projectImportMapper, projectImportResults, mockBackupSystemInformation, new ExecutorForTests()) {
            WorklogParser getWorklogParser() {
                return mockWorklogParser;
            }

            WorklogTransformer getWorklogTransformer() {
                return mockWorklogTransformer;
            }
        };

        worklogPersisterHandler.handleEntity(WORKLOG_ENTITY_NAME, null);
        worklogPersisterHandler.handleEntity("NOTWorklog", null);

        assertThat(projectImportResults.getErrors(), contains("There was a problem saving worklog with id '12' for issue 'TST-1'."));
    }

    @Test
    public void testHandleErrorNullIssueId() throws Exception {
        final ExternalWorklog transformedExternalWorklog = new ExternalWorklog();
        transformedExternalWorklog.setId("12");

        when(mockWorklogParser.parse(null)).thenReturn(externalWorklog);
        when(mockWorklogTransformer.transform(projectImportMapper, externalWorklog)).thenReturn(transformedExternalWorklog);
        when(mockBackupSystemInformation.getIssueKeyForId("34")).thenReturn("TST-1");

        final ProjectImportResultsImpl projectImportResults = new ProjectImportResultsImpl(0, 0, 0, 0, new MockI18nBean());
        WorklogPersisterHandler worklogPersisterHandler = new WorklogPersisterHandler(mockProjectImportPersister, projectImportMapper, projectImportResults, mockBackupSystemInformation, null) {
            WorklogParser getWorklogParser() {
                return mockWorklogParser;
            }

            WorklogTransformer getWorklogTransformer() {
                return mockWorklogTransformer;
            }
        };

        worklogPersisterHandler.handleEntity(WORKLOG_ENTITY_NAME, null);
        worklogPersisterHandler.handleEntity("NOTWorklog", null);

        assertThat(projectImportResults.getErrors(), emptyIterable());
    }
}
