package com.atlassian.jira.upgrade.tasks;


import com.atlassian.jira.upgrade.tasks.util.Sequences;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;


/**
 * Responsible for verifying that the upgrade task corrects the value of the &quot;Membership&quot; database sequence.
 * See JRA-24466.
 *
 * @since v4.4
 */
public class TestUpgradeTask_Build640 {
    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @Mock
    private Sequences mockSequences;

    @Test
    public void membershipSequenceShouldBeUpdatedOnUpgrade() throws Exception {
        final UpgradeTask_Build640 upgradeTask_build640 = new UpgradeTask_Build640(mockSequences);

        upgradeTask_build640.doUpgrade(false);

        verify(mockSequences).update(any(), eq("Membership"), eq("cwd_membership"));
    }
}
