package com.atlassian.jira.application;

import com.atlassian.application.api.ApplicationAccess.AccessError;
import com.atlassian.application.api.ApplicationKey;
import com.atlassian.fugue.Option;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.junit.rules.InitMockitoMocks;
import com.atlassian.jira.license.LicenseDetails;
import com.atlassian.jira.matchers.OptionMatchers;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.sal.api.user.UserKey;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.mockito.Mock;

import java.util.Date;
import java.util.EnumSet;
import java.util.Set;

import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

public class JiraApplicationAccessTest {
    private final static ApplicationKey KEY = ApplicationKey.valueOf("key");
    private final static Date BUILD_DATE = new Date();
    private final static ApplicationUser TEST_USER = new MockApplicationUser("admin");

    @Rule
    public TestRule rules = new InitMockitoMocks(this);

    private MockApplicationRole role = new MockApplicationRole().key(KEY);
    @Mock
    private ApplicationAuthorizationService authorizationService;
    @Mock
    private UserManager userManager;
    @Mock
    private LicenseDetails details;
    @Mock
    private FeatureManager featureManager;

    private JiraApplicationAccess access;

    @Before
    public void before() {
        when(userManager.getUserByKey(TEST_USER.getKey())).thenReturn(TEST_USER);
        when(details.isMaintenanceValidForBuildDate(BUILD_DATE)).thenReturn(true);

        access = new JiraApplicationAccess(role, authorizationService, userManager);
    }

    @Test
    public void getKeyReturnsKeyFromRole() {
        final JiraApplicationAccess access = new JiraApplicationAccess(role, authorizationService, userManager);
        assertThat(access.getApplicationKey(), Matchers.equalTo(KEY));
    }

    @Test
    public void getMaximumNumberOfSeatsReturnsNoneWhenUnlimited() {
        role.unlimitedSeats();
        assertThat(access.getMaximumUserCount(), OptionMatchers.none());
    }

    @Test
    public void getMaximumNumberOfSeatsReturnsCountWhenFromLicense() {
        role.numberOfSeats(10);
        assertThat(access.getMaximumUserCount(), OptionMatchers.some(10));
    }

    @Test
    public void canUserAccessApplicationShouldUseAuthorizationService() {
        when(authorizationService.canUseApplication(TEST_USER, role.getKey())).thenReturn(true);
        assertTrue("Should be able to access application when authorizationService says we can", access.canUserAccessApplication(toKey(TEST_USER)));
    }

    @Test
    public void getAccessErrorShouldUseAuthorizationService() {
        Set<AccessError> expectedResult = EnumSet.of(AccessError.UNLICENSED);
        when(authorizationService.getAccessErrors(TEST_USER, role.getKey())).thenReturn(expectedResult);
        assertThat("Should get error from authorizationService", access.getAccessError(toKey(TEST_USER)), Matchers.equalTo(Option.some(AccessError.UNLICENSED)));
    }

    @Test
    public void getManagementPageReturnsCorrectUrl() {
        assertThat(access.getManagementPage().toString(), Matchers.equalTo(String.format(JiraApplicationAccess.URL_USER_BROWSER_BTF, KEY.value())));
    }

    private static UserKey toKey(final ApplicationUser user) {
        return new UserKey(user.getKey());
    }
}