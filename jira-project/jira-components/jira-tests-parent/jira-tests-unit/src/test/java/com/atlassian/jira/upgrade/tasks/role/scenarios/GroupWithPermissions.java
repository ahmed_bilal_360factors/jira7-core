package com.atlassian.jira.upgrade.tasks.role.scenarios;

import cucumber.deps.com.thoughtworks.xstream.annotations.XStreamConverter;
import cucumber.runtime.CommaSeparatedStringToListConverter;

import javax.annotation.Nullable;
import java.util.List;

/**
 * Represents table of groups with permissions. Example table:
 * <pre>
 * | name      | permissions   |
 * | users     | USE           |
 * | admins    | ADMINISTER    |
 * | useAdmins | USE, SYSADMIN |
 * </pre>
 */
public class GroupWithPermissions {
    @XStreamConverter(GroupNameConverter.class)
    private final GroupName name;

    @XStreamConverter(CommaSeparatedStringToListConverter.class)
    private final List<String> permissions;

    public GroupWithPermissions(final GroupName name, final List<String> permissions) {
        this.name = name;
        this.permissions = permissions;
    }

    @Nullable
    public GroupName getName() {
        return name;
    }

    public List<String> getPermissions() {
        return permissions;
    }
}
