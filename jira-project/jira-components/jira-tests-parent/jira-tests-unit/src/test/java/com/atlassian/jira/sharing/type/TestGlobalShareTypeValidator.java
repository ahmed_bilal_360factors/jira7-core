package com.atlassian.jira.sharing.type;

import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.bc.JiraServiceContextImpl;
import com.atlassian.jira.bc.MockJiraServiceContext;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.sharing.SharePermission;
import com.atlassian.jira.sharing.SharePermissionImpl;
import com.atlassian.jira.sharing.search.GlobalShareTypeSearchParameter;
import com.atlassian.jira.sharing.search.PrivateShareTypeSearchParameter;
import com.atlassian.jira.sharing.type.ShareType.Name;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.web.bean.MockI18nBean;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.when;

/**
 * Unit tests for {@link GlobalShareTypeValidator}
 *
 * @since v3.13
 */

public class TestGlobalShareTypeValidator {
    @Rule
    public MockitoRule mockito = MockitoJUnit.rule();

    protected ApplicationUser user;
    @Mock
    protected PermissionManager permissionManager;
    @Mock
    private ApplicationProperties applicationProperties;
    private static final SharePermission GLOBAL_PERM = new SharePermissionImpl(GlobalShareType.TYPE, null, null);
    private static final SharePermission INVALID_PERM = new SharePermissionImpl(new Name("group"), "developers", null);
    protected JiraServiceContext ctx;

    @Before
    public void setUp() throws Exception {
        user = new MockApplicationUser("admin");
        ctx = new MockJiraServiceContext(user);
        when(applicationProperties.getOption(APKeys.JIRA_OPTION_GLOBAL_SHARING)).thenReturn(true);
    }

    @After
    public void tearDown() throws Exception {
        user = null;
        ctx = null;
    }

    @Test
    public void testCheckSharePermissionWithValidSharePermissionAndPermission() {
        when(permissionManager.hasPermission(Permissions.CREATE_SHARED_OBJECTS, user)).thenReturn(true);
        final ShareTypeValidator validator = new GlobalShareTypeValidator(permissionManager, applicationProperties);

        assertTrue(validator.checkSharePermission(ctx, TestGlobalShareTypeValidator.GLOBAL_PERM));
        assertFalse(ctx.getErrorCollection().hasAnyErrors());
    }

    @Test
    public void testCheckSharePermissionWithValidSharePermissionAndNoPermission() {
        when(permissionManager.hasPermission(Permissions.CREATE_SHARED_OBJECTS, user)).thenReturn(false);
        final ShareTypeValidator validator = new GlobalShareTypeValidator(permissionManager, applicationProperties);

        assertFalse(validator.checkSharePermission(ctx, TestGlobalShareTypeValidator.GLOBAL_PERM));
        assertTrue(ctx.getErrorCollection().hasAnyErrors());
    }

    @Test
    public void testCheckSharePermissionWithNoValidSharePermission() {
        final ShareTypeValidator validator = new GlobalShareTypeValidator(permissionManager, applicationProperties);

        try {
            validator.checkSharePermission(ctx, TestGlobalShareTypeValidator.INVALID_PERM);
            fail("checkSharePermission should have thrown IllegalArgumentException illegal permission");
        } catch (final IllegalArgumentException e) {
            // expected
        }

        assertFalse(ctx.getErrorCollection().hasAnyErrors());
    }

    @Test
    public void testCheckSharePermissionNullPermission() {
        final ShareTypeValidator validator = new GlobalShareTypeValidator(null, applicationProperties);

        try {
            validator.checkSharePermission(ctx, null);
            fail("checkSharePermission should not accept null permission");
        } catch (final IllegalArgumentException e) {
            // expected
        }
        assertFalse(ctx.getErrorCollection().hasAnyErrors());
    }

    @Test
    public void testCheckSharePermissionNullContext() {
        final ShareTypeValidator validator = new GlobalShareTypeValidator(null, applicationProperties);

        try {
            validator.checkSharePermission(null, TestGlobalShareTypeValidator.GLOBAL_PERM);
            fail("checkSharePermission should not accept null ctx");
        } catch (final IllegalArgumentException e) {
            // expected
        }
        assertFalse(ctx.getErrorCollection().hasAnyErrors());
    }

    @Test
    public void testCheckSharePermissionNullUserInContext() {
        when(permissionManager.hasPermission(Permissions.CREATE_SHARED_OBJECTS, null)).thenReturn(false);
        final ShareTypeValidator validator = new GlobalShareTypeValidator(permissionManager, applicationProperties);

        final JiraServiceContext nullUserCtx = new JiraServiceContextImpl((ApplicationUser) null) {
            @Override
            public I18nHelper getI18nBean() {
                return new MockI18nBean();
            }
        };

        assertFalse(validator.checkSharePermission(nullUserCtx, TestGlobalShareTypeValidator.GLOBAL_PERM));
        assertFalse(ctx.getErrorCollection().hasAnyErrors());

        when(permissionManager.hasPermission(Permissions.CREATE_SHARED_OBJECTS, null)).thenReturn(true);
        assertTrue(validator.checkSharePermission(nullUserCtx, TestGlobalShareTypeValidator.GLOBAL_PERM));
        assertFalse(ctx.getErrorCollection().hasAnyErrors());
    }

    @Test
    public void testCheckGoodUser() {
        final ShareTypeValidator validator = new GlobalShareTypeValidator(null, applicationProperties);

        final boolean result = validator.checkSearchParameter(ctx, GlobalShareTypeSearchParameter.GLOBAL_PARAMETER);
        assertTrue(result);
    }

    @Test
    public void testCheckAnonymous() {
        final ShareTypeValidator validator = new GlobalShareTypeValidator(null, applicationProperties);
        final JiraServiceContext nullUserCtx = new JiraServiceContextImpl(null);

        final boolean result = validator.checkSearchParameter(nullUserCtx, GlobalShareTypeSearchParameter.GLOBAL_PARAMETER);
        assertTrue(result);

    }

    @Test
    public void testInvalidArgument() {
        final ShareTypeValidator validator = new GlobalShareTypeValidator(null, applicationProperties);

        try {
            validator.checkSearchParameter(ctx, PrivateShareTypeSearchParameter.PRIVATE_PARAMETER);
            fail("Should not accept invalid search parameter.");
        } catch (final IllegalArgumentException e) {
            // expected.
        }
    }
}
