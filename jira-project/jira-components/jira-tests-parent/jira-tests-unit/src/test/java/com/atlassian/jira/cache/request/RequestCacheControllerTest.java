package com.atlassian.jira.cache.request;

import com.atlassian.cache.CacheLoader;
import com.atlassian.cache.Supplier;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import javax.annotation.Nonnull;

import static com.atlassian.jira.cache.request.RequestCacheController.closeContext;
import static com.atlassian.jira.cache.request.RequestCacheController.isInContext;
import static com.atlassian.jira.cache.request.RequestCacheController.startContext;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

/**
 * @since v6.4.8
 */
public class RequestCacheControllerTest {
    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void testSingleThread() throws Exception {
        final MockCacheLoader cacheLoader = new MockCacheLoader();
        final RequestCache<Integer, String> requestCache = RequestCacheController.createRequestCache("Shirley", cacheLoader);

        // no context
        // without a context, we keep going back to the CacheLoader
        requestCache.get(256);
        assertEquals(1, cacheLoader.accessCount);
        requestCache.get(256);
        assertEquals(2, cacheLoader.accessCount);

        // start a context
        startContext();
        requestCache.get(256);
        assertEquals(3, cacheLoader.accessCount);

        // with a context, we can now get the value out of the cache :)
        requestCache.get(256);
        assertEquals(3, cacheLoader.accessCount);

        // after a remove, we should use the loader again.  Calling it twice does not matter.
        requestCache.remove(256);
        requestCache.remove(256);
        requestCache.get(256);
        assertEquals(4, cacheLoader.accessCount);

        // close the context
        // without a context, we keep going back to the CacheLoader
        closeContext();
        requestCache.get(256);
        assertEquals(5, cacheLoader.accessCount);

        // remove has no effect, but should not raise an exception
        requestCache.remove(256);
        requestCache.get(256);
        assertEquals(6, cacheLoader.accessCount);
    }

    @Test
    public void testMultiThread() throws Exception {
        final MockCacheLoader cacheLoader = new MockCacheLoader();
        final RequestCache<Integer, String> requestCache = RequestCacheController.createRequestCache("Shirley", cacheLoader);

        // start a context in this thread
        startContext();
        assertEquals("<9> 1", requestCache.get(9));
        // with a context, we can now get the value out of the cache :)
        assertEquals("<9> 1", requestCache.get(9));

        // start a new Thread
        final Thread th = new Thread(new Runnable() {
            @Override
            public void run() {
                // assert no cache is in play on this thread
                assertEquals("<9> 2", requestCache.get(9));
                assertEquals("<9> 3", requestCache.get(9));

                startContext();
                // load
                assertEquals("<9> 4", requestCache.get(9));
                // cached
                assertEquals("<9> 4", requestCache.get(9));
            }
        });

        th.start();
        th.join();

        // this thread still has old cache
        assertEquals("<9> 1", requestCache.get(9));

        // so as not to pollute other tests
        closeContext();
    }

    @Test
    public void testReentrancy() throws Exception {
        final MockCacheLoader cacheLoader = new MockCacheLoader();
        final RequestCache<Integer, String> requestCache = RequestCacheController.createRequestCache("Shirley", cacheLoader);
        assertThat(isInContext(), is(false));

        startContext();
        assertThat(isInContext(), is(true));
        requestCache.get(256);
        assertEquals(1, cacheLoader.accessCount);

        startContext();
        assertThat(isInContext(), is(true));
        requestCache.get(256);
        assertEquals(1, cacheLoader.accessCount);

        closeContext();
        assertThat(isInContext(), is(true));
        requestCache.get(256);
        assertEquals(1, cacheLoader.accessCount);

        closeContext();
        assertThat(isInContext(), is(false));
        requestCache.get(256);
        assertEquals(2, cacheLoader.accessCount);

        try {
            closeContext();
            fail();
        } catch (IllegalStateException ex) {
            // :)
        }

        assertThat(isInContext(), is(false));
        requestCache.get(256);
        assertEquals(3, cacheLoader.accessCount);
    }

    @SuppressWarnings("ConstantConditions")
    @Test
    public void testNullKey() {
        final RequestCache<String, String> requestCache = RequestCacheController.createRequestCache("x", new IdentityCacheLoader());

        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("key should not");
        requestCache.get(null);
    }

    @Test
    public void testNullValueLoaded() {
        final RequestCache<String, String> requestCache = RequestCacheController.createRequestCache("x", new NullCacheLoader());

        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("cacheLoader returned null");
        requestCache.get("fred");
    }

    @Test
    public void testNullValueSupplied() {
        final RequestCache<String, String> requestCache = RequestCacheController.createRequestCache("x", new IdentityCacheLoader());

        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("supplier returned null");
        requestCache.get("fred", new Supplier<String>() {
            @Override
            @SuppressWarnings("ReturnOfNull")
            public String get() {
                return null;
            }
        });
    }

    static class MockCacheLoader implements CacheLoader<Integer, String> {
        private int accessCount = 0;

        @Nonnull
        @Override
        public String load(@Nonnull final Integer key) {
            accessCount++;
            return "<" + key + "> " + accessCount;
        }
    }

    static class IdentityCacheLoader implements CacheLoader<String, String> {
        @Nonnull
        @Override
        public String load(@Nonnull final String s) {
            return s;
        }
    }

    @SuppressWarnings({"ConstantConditions", "ReturnOfNull"})  // Intentional lies for testing
    static class NullCacheLoader implements CacheLoader<String, String> {
        @Nonnull
        @Override
        public String load(@Nonnull final String s) {
            return null;
        }
    }
}
