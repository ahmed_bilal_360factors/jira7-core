package com.atlassian.jira.imports.project.handler;

import com.atlassian.jira.exception.ParseException;
import com.atlassian.jira.external.beans.ExternalProject;
import com.atlassian.jira.imports.project.core.BackupProject;
import com.atlassian.jira.imports.project.core.BackupProjectImpl;
import com.atlassian.jira.imports.project.core.BackupSystemInformation;
import com.atlassian.jira.imports.project.mapper.IssueLinkTypeMapper;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.mock.issue.MockIssue;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import org.junit.Test;

import java.util.List;

import static java.lang.String.valueOf;
import static java.util.Collections.emptyList;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @since v3.13
 */
public class TestIssueLinkMapperHandler {

    private static final String JIRA_SUBTASK_LINK = "jira_subtask_link";
    private static final String JIRA_SUBTASK = "jira_subtask";
    private static final String ISSUE_LINK = "IssueLink";
    private static final String KEY_LINKTYPE = "linktype";
    private static final String KEY_SOURCE = "source";
    private static final String KEY_DESTINATION = "destination";
    private static final String KEY_ID = "id";
    private static final String KEY_LINKNAME = "linkname";
    private static final String KEY_STYLE = "style";
    private static final String ISSUE_LINK_TYPE = "IssueLinkType";

    @Test
    public void testHandleIssueLinkType() throws ParseException {
        final String linkNameDuplicate = "Duplicate";
        final String issueLinkId1 = "12";
        final String issueLinkId2 = "10001";

        final IssueLinkTypeMapper issueLinkTypeMapper = new IssueLinkTypeMapper();
        IssueLinkMapperHandler issueLinkMapperHandler = new IssueLinkMapperHandler(null, null, null, issueLinkTypeMapper);
        issueLinkMapperHandler.startDocument();
        // <IssueLinkType id="10001" linkname="jira_subtask_link" inward="jira_subtask_inward" outward="jira_subtask_outward" style="jira_subtask"/>
        issueLinkMapperHandler.handleEntity(ISSUE_LINK_TYPE, ImmutableMap.of(KEY_ID, issueLinkId1, KEY_LINKNAME, linkNameDuplicate));
        issueLinkMapperHandler.handleEntity(ISSUE_LINK_TYPE, ImmutableMap.of(KEY_ID, issueLinkId2, KEY_LINKNAME, JIRA_SUBTASK_LINK, KEY_STYLE, JIRA_SUBTASK));
        // Include another entity
        issueLinkMapperHandler.handleEntity("NOT_IssueLinkType", ImmutableMap.of(KEY_ID, issueLinkId2, KEY_LINKNAME, JIRA_SUBTASK_LINK, KEY_STYLE, JIRA_SUBTASK));
        issueLinkMapperHandler.endDocument();

        // Check in the Mapper that we have mapped the expected values.
        assertThat(issueLinkTypeMapper.getRegisteredOldIds(), containsInAnyOrder(issueLinkId1, issueLinkId2));
        assertEquals(linkNameDuplicate, issueLinkTypeMapper.getKey(issueLinkId1));
        assertEquals(JIRA_SUBTASK_LINK, issueLinkTypeMapper.getKey(issueLinkId2));
        assertEquals(null, issueLinkTypeMapper.getStyle(issueLinkId1));
        assertEquals(JIRA_SUBTASK, issueLinkTypeMapper.getStyle(issueLinkId2));
    }

    @Test
    public void testHandleIssueLink() throws ParseException {
        final String nonExistingIssueKey = "DOG-70";
        final String existingIssueKey = "DOG-69";
        final String nonExistingIssueId = "400";
        final String existingIssueId = "300";
        // Mock out BackupSystemInformation
        final BackupSystemInformation mockBackupSystemInformation = mock(BackupSystemInformation.class);
        when(mockBackupSystemInformation.getIssueKeyForId(nonExistingIssueId)).thenReturn(nonExistingIssueKey);
        when(mockBackupSystemInformation.getIssueKeyForId(existingIssueId)).thenReturn(existingIssueKey);


        final IssueManager mockIssueManager = mock(IssueManager.class);
        when(mockIssueManager.getIssueObject(nonExistingIssueKey)).thenReturn(null);
        when(mockIssueManager.getIssueObject(existingIssueKey)).thenReturn(new MockIssue(Long.valueOf(existingIssueId)));

        final IssueLinkTypeMapper issueLinkTypeMapper = new IssueLinkTypeMapper();
        final Long backupIssueId1 = 100L;
        final Long backupIssueId2 = 120L;
        final List<Long> issueIds = ImmutableList.of(backupIssueId1, backupIssueId2);
        final BackupProject backupProject = new BackupProjectImpl(new ExternalProject(), emptyList(), emptyList(), emptyList(), issueIds, 0, ImmutableMap.of());
        IssueLinkMapperHandler issueLinkMapperHandler = new IssueLinkMapperHandler(backupProject, mockBackupSystemInformation, mockIssueManager, issueLinkTypeMapper);
        issueLinkMapperHandler.startDocument();
        //     <IssueLink id="10044" linktype="10000" source="10184" destination="10030"/>
        // Source is in our project, Destination doesn't exist in current system
        issueLinkMapperHandler.handleEntity(ISSUE_LINK, ImmutableMap.of(KEY_ID, "12", KEY_LINKTYPE, "3", KEY_SOURCE, valueOf(backupIssueId1), KEY_DESTINATION, nonExistingIssueId));
        // Destination is in our project, Source doesn't exist in current system
        issueLinkMapperHandler.handleEntity(ISSUE_LINK, ImmutableMap.of(KEY_ID, "13", KEY_LINKTYPE, "4", KEY_SOURCE, nonExistingIssueId, KEY_DESTINATION, valueOf(backupIssueId2)));
        // Source is in our project, Destination exists in current system
        issueLinkMapperHandler.handleEntity(ISSUE_LINK, ImmutableMap.of(KEY_ID, "14", KEY_LINKTYPE, "5", KEY_SOURCE, valueOf(backupIssueId1), KEY_DESTINATION, existingIssueId));
        // Destination is in our project, Source exists in current system
        issueLinkMapperHandler.handleEntity(ISSUE_LINK, ImmutableMap.of(KEY_ID, "15", KEY_LINKTYPE, "6", KEY_SOURCE, existingIssueId, KEY_DESTINATION, valueOf(backupIssueId2)));
        // Source and Destination are both in this project
        issueLinkMapperHandler.handleEntity(ISSUE_LINK, ImmutableMap.of(KEY_ID, "16", KEY_LINKTYPE, "7", KEY_SOURCE, valueOf(backupIssueId1), KEY_DESTINATION, valueOf(backupIssueId2)));
        // Link is nothing to do with us.
        issueLinkMapperHandler.handleEntity(ISSUE_LINK, ImmutableMap.of(KEY_ID, "17", KEY_LINKTYPE, "8", KEY_SOURCE, existingIssueId, KEY_DESTINATION, nonExistingIssueId));
        issueLinkMapperHandler.endDocument();

        // Check in the Mapper that we have set the expected required linktypes
        assertThat(issueLinkTypeMapper.getRequiredOldIds(), containsInAnyOrder("5", "6", "7"));
    }
}
