package com.atlassian.jira.bc.issue.search;

import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.jql.builder.JqlQueryBuilder;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.mock.security.MockSimpleAuthenticationContext;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.util.ListOrderedMessageSetImpl;
import com.atlassian.jira.util.MessageSet;
import com.atlassian.query.Query;
import com.atlassian.query.QueryImpl;
import com.atlassian.query.order.SortOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Test for {@link com.atlassian.jira.bc.issue.search.LuceneCurrentSearchIssuePickerSearchProvider}.
 *
 * @since v4.2
 */
public class TestLuceneCurrentSearchIssuePickerSearchProvider {
    @Rule
    public final RuleChain mockContainer = MockitoMocksInContainer.forTest(this);

    private final LuceneCurrentSearchIssuePickerSearchProvider searchProvider = new LuceneCurrentSearchIssuePickerSearchProvider(null, null, null, null, null);

    @Test
    public void testGetId() {
        assertThat(searchProvider.getId(), equalTo("cs"));
    }

    @Test
    public void testGetKey() {
        assertThat(searchProvider.getLabelKey(), equalTo("jira.ajax.autocomplete.current.search"));
    }

    @Test
    public void testGetRequestNullJql() {
        assertNull(searchProvider.getRequest(createParameters()));
    }

    /*
     * Test when the JQL parses correctly.
     */
    @Test
    public void testGetRequestNoErrors() {
        final String jql = "this should pass through";
        final Query fullQuery = JqlQueryBuilder.newBuilder().where().assignee().eq("jack").endWhere().orderBy().assignee(SortOrder.DESC).buildQuery();
        final Query expectedQuery = new QueryImpl(fullQuery.getWhereClause(), null, null);

        final ApplicationUser user = new MockApplicationUser("test");
        final JiraAuthenticationContext ctx = new MockSimpleAuthenticationContext(user);

        final SearchService service = mock(SearchService.class);
        when(service.parseQuery(user, jql)).thenReturn(new SearchService.ParseResult(fullQuery, new ListOrderedMessageSetImpl()));

        final LuceneCurrentSearchIssuePickerSearchProvider searchProvider = new LuceneCurrentSearchIssuePickerSearchProvider(ctx, null, null, service, null);
        final SearchRequest request = searchProvider.getRequest(createParameters(null, jql));

        assertNotNull(request);
        assertThat(request.getQuery(), equalTo(expectedQuery));
    }

    /*
     * Test when the JQL does not parse correctly.
     */
    @Test
    public void testGetRequestWithErrors() {
        final String jql = "this should pass through";
        final Query expectedQuery = JqlQueryBuilder.newBuilder().where().assignee().eq("jack").endWhere().orderBy().assignee(SortOrder.DESC).buildQuery();
        final ApplicationUser user = new MockApplicationUser("test");
        final JiraAuthenticationContext ctx = new MockSimpleAuthenticationContext(user);
        final MessageSet errors = new ListOrderedMessageSetImpl();
        errors.addErrorMessage("I have an error");

        final SearchService service = mock(SearchService.class);
        when(service.parseQuery(user, jql)).thenReturn(new SearchService.ParseResult(expectedQuery, errors));

        final LuceneCurrentSearchIssuePickerSearchProvider searchProvider = new LuceneCurrentSearchIssuePickerSearchProvider(ctx, null, null, service, null);
        final SearchRequest request = searchProvider.getRequest(createParameters(null, jql));

        assertNull(request);
    }

    @Test
    public void testHandlesParameters() throws Exception {
        assertFalse(searchProvider.handlesParameters(null, createParameters("", "something = bad")));
        assertFalse(searchProvider.handlesParameters(null, createParameters(null, "something = bad")));
        assertFalse(searchProvider.handlesParameters(null, createParameters("H", null)));
        assertTrue(searchProvider.handlesParameters(null, createParameters("H", "")));
        assertTrue(searchProvider.handlesParameters(null, createParameters("H", "sosmesms")));
    }

    private static IssuePickerSearchService.IssuePickerParameters createParameters(String query, String jql) {
        return new IssuePickerSearchService.IssuePickerParameters(query, jql, null, null, true, true, 10);
    }

    private static IssuePickerSearchService.IssuePickerParameters createParameters() {
        return createParameters(null, null);
    }
}
