package com.atlassian.jira.util;

import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.when;

/**
 * @since v3.13
 */
public class TestEmailFormatterImpl {
    private static final String USER_EXAMPLE_ORG = "user@example.org";
    private static final String USER_EXAMPLE_ORG_MASKED = "user at example dot org";

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @Mock
    private ApplicationProperties mockApplicationProperties;

    @Test
    public void testFormatEmailAsLinkEncoding() {
        when(mockApplicationProperties.getString(APKeys.JIRA_OPTION_EMAIL_VISIBLE)).thenReturn("show");

        final EmailFormatterImpl formatter = new EmailFormatterImpl(mockApplicationProperties);

        String email = formatter.formatEmailAsLink("test@example.com", null);
        assertEquals("<a href=\"mailto:test@example.com\">test@example.com</a>", email);

        email = formatter.formatEmailAsLink("\"<script>alert('owned')</script>\"@localhost", null);
        assertEquals("<a href=\"mailto:&quot;&lt;script&gt;alert(&#39;owned&#39;)&lt;/script&gt;&quot;@localhost\">&quot;&lt;script&gt;alert(&#39;owned&#39;)&lt;/script&gt;&quot;@localhost</a>", email);

    }

    @Test
    public void testFormatEmailHidden() throws Exception {
        when(mockApplicationProperties.getString(APKeys.JIRA_OPTION_EMAIL_VISIBLE)).thenReturn("hidden");

        final EmailFormatterImpl formatter = new EmailFormatterImpl(mockApplicationProperties);

        assertNull(formatter.formatEmail(USER_EXAMPLE_ORG, true));
        assertNull(formatter.formatEmail(USER_EXAMPLE_ORG, false));
        assertEquals("", formatter.formatEmailAsLink(USER_EXAMPLE_ORG, null));
    }

    @Test
    public void testFormatEmailPublic() throws Exception {
        when(mockApplicationProperties.getString(APKeys.JIRA_OPTION_EMAIL_VISIBLE)).thenReturn("show");

        final EmailFormatterImpl formatter = new EmailFormatterImpl(mockApplicationProperties);

        assertEquals(USER_EXAMPLE_ORG, formatter.formatEmail(USER_EXAMPLE_ORG, true));
        assertEquals(USER_EXAMPLE_ORG, formatter.formatEmail(USER_EXAMPLE_ORG, false));
    }

    @Test
    public void testFormatEmailMasked() throws Exception {
        when(mockApplicationProperties.getString(APKeys.JIRA_OPTION_EMAIL_VISIBLE)).thenReturn("mask");

        final EmailFormatterImpl formatter = new EmailFormatterImpl(mockApplicationProperties);

        assertEquals(USER_EXAMPLE_ORG_MASKED, formatter.formatEmail(USER_EXAMPLE_ORG, true));
        assertEquals(USER_EXAMPLE_ORG_MASKED, formatter.formatEmail(USER_EXAMPLE_ORG, false));
        assertEquals(USER_EXAMPLE_ORG_MASKED, formatter.formatEmailAsLink(USER_EXAMPLE_ORG, null));
        assertEquals("&lt;script&gt; at &lt;script&gt; dot com", formatter.formatEmailAsLink("<script>@<script>.com", null));
    }

    @Test
    public void maskedEmailFormattingShouldReturnAnEmptyStringGivenTheProvidedEmailIsEmpty() throws Exception {
        when(mockApplicationProperties.getString(APKeys.JIRA_OPTION_EMAIL_VISIBLE)).thenReturn("mask");

        final EmailFormatterImpl formatter = new EmailFormatterImpl(mockApplicationProperties);

        assertEquals
                (
                        "If an user's email address is empty its masked value should be empty as well.",
                        "", formatter.formatEmail("", true)
                );

        assertEquals
                (
                        "If an user's email address is empty its masked value should be empty as well.",
                        "", formatter.formatEmail("", false)
                );
    }

    @Test
    public void testFormatEmailLoggedInOnly() throws Exception {
        when(mockApplicationProperties.getString(APKeys.JIRA_OPTION_EMAIL_VISIBLE)).thenReturn("user");

        final EmailFormatterImpl formatter = new EmailFormatterImpl(mockApplicationProperties);

        assertEquals(USER_EXAMPLE_ORG, formatter.formatEmail(USER_EXAMPLE_ORG, true));
        assertNull(formatter.formatEmail(USER_EXAMPLE_ORG, false));
    }
}
