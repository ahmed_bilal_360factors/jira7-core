package com.atlassian.jira.issue.fields.csv;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.export.FieldExportParts;
import com.atlassian.jira.issue.fields.ProjectSystemField;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.type.ProjectTypeKey;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.I18nHelper;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.mockito.Mock;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

public class ProjectSystemFieldCsvExportTest {
    @Rule
    public final RuleChain mockito = MockitoMocksInContainer.forTest(this);

    @Mock
    private JiraAuthenticationContext authenticationContext;

    @Mock
    private I18nHelper i18nHelper;

    private static final String LEAD_USERNAME = "lead username";
    private static final String PROJECT_DESCRIPTION = "Project description";
    private static final String PROJECT_KEY = "Project key";
    private static final String PROJECT_NAME = "Project name";
    private static final String PROJECT_TYPE = "Project type";
    private static final String PROJECT_URL = "Project url";

    @Mock
    private Project project;

    @Mock
    private ApplicationUser projectLead;

    @Mock
    private Issue issue;

    @Mock
    private ProjectTypeKey projectTypeKey;

    private ProjectSystemField field;

    @Before
    public void setUp() {
        field = new ProjectSystemField(null, null, authenticationContext, null, null, null, null, null, null);

        when(issue.getProjectObject()).thenReturn(project);
        when(projectLead.getUsername()).thenReturn(LEAD_USERNAME);
        when(projectTypeKey.getKey()).thenReturn(PROJECT_TYPE);

        when(project.getKey()).thenReturn(PROJECT_KEY);
        when(project.getName()).thenReturn(PROJECT_NAME);
        when(project.getProjectTypeKey()).thenReturn(projectTypeKey);
        when(authenticationContext.getI18nHelper()).thenReturn(i18nHelper);
    }

    @Test
    public void testCsvRepresentationForExpectedTypeAndValues() {
        when(project.getDescription()).thenReturn(PROJECT_DESCRIPTION);
        when(project.getUrl()).thenReturn(PROJECT_URL);
        when(project.getProjectLead()).thenReturn(projectLead);

        final FieldExportParts representation = field.getRepresentationFromIssue(issue);
        assertThat(representation.getParts(), hasSize(6));

        assertThat(representation.getPartWithId(ProjectSystemField.EXPORT_KEY_ID).get().getValues()::iterator, contains(PROJECT_KEY));
        assertThat(representation.getPartWithId(ProjectSystemField.EXPORT_PROJECT_NAME_ID).get().getValues()::iterator, contains(PROJECT_NAME));
        assertThat(representation.getPartWithId(ProjectSystemField.EXPORT_PROJECT_TYPE_ID).get().getValues()::iterator, contains(PROJECT_TYPE));
        assertThat(representation.getPartWithId(ProjectSystemField.EXPORT_PROJECT_LEAD_ID).get().getValues()::iterator, contains(LEAD_USERNAME));
        assertThat(representation.getPartWithId(ProjectSystemField.EXPORT_PROJECT_DESCRIPTION_ID).get().getValues()::iterator, contains(PROJECT_DESCRIPTION));
        assertThat(representation.getPartWithId(ProjectSystemField.EXPORT_PROJECT_URL_ID).get().getValues()::iterator, contains(PROJECT_URL));
    }

    @Test
    public void testCsvRepresentationWhenThereAreNoOptionalValues() {
        final FieldExportParts representation = field.getRepresentationFromIssue(issue);
        assertThat(representation.getParts(), hasSize(4));

        assertThat(representation.getPartWithId(ProjectSystemField.EXPORT_KEY_ID).get().getValues()::iterator, contains(PROJECT_KEY));
        assertThat(representation.getPartWithId(ProjectSystemField.EXPORT_PROJECT_NAME_ID).get().getValues()::iterator, contains(PROJECT_NAME));
        assertThat(representation.getPartWithId(ProjectSystemField.EXPORT_PROJECT_TYPE_ID).get().getValues()::iterator, contains(PROJECT_TYPE));
        assertThat(representation.getPartWithId(ProjectSystemField.EXPORT_PROJECT_URL_ID).get().getValues()::iterator, contains(""));
    }
}
