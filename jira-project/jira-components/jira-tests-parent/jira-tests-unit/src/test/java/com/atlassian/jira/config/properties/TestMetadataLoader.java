package com.atlassian.jira.config.properties;

import com.atlassian.jira.bc.admin.ApplicationPropertyMetadata;
import com.atlassian.validation.EnumValidator;
import org.junit.Before;
import org.junit.Test;

import java.io.InputStream;
import java.util.LinkedHashMap;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

/**
 * Unit test for {@link MetadataLoader}.
 *
 * @since v4.4
 */
public class TestMetadataLoader {
    private LinkedHashMap<String, ApplicationPropertyMetadata> map;

    @Before
    public void setUp() throws Exception {
        MetadataLoader ml = new MetadataLoader();
        InputStream xmlStream = getClass().getResourceAsStream("testmetadata.xml");
        map = ml.loadMetadata(xmlStream, "testmetadata.xml");
    }

    @Test
    public void testLoadingBooleanMetadata() {
        ApplicationPropertyMetadata metadata = getAndCheck("simple.key", "boolean", "true");
        assertThat(metadata.getName(), equalTo("Simple Name"));
        assertThat(metadata.getDescription(), equalTo("Yo, Simple Description, Dog"));
        assertThat("sysdamin is not editable", metadata.isSysadminEditable(), equalTo(false));
        assertThat("restart is required", metadata.isRequiresRestart(), equalTo(true));
    }

    @Test
    public void testLoadingEnumMetadata() {
        ApplicationPropertyMetadata metadata = getAndCheck("enum.key", "enum", "ni");
        assertThat(metadata.getName(), equalTo("Enumeration Test"));
        assertThat(metadata.getDescription(), equalTo("This tests multiple options in an enumeration"));
        assertThat("sysadmin is editable", metadata.isSysadminEditable(), equalTo(true));
        assertThat("restart is not required", metadata.isRequiresRestart(), equalTo(false));
        assertThat("Validator is instance of EnumValidator", metadata.getValidator() instanceof EnumValidator, equalTo(true));
        assertThat("'ichi' is valid", metadata.getValidator().validate("ichi").isValid(), equalTo(true));
        assertThat("'ni' is valid", metadata.getValidator().validate("ni").isValid(), equalTo(true));
        assertThat("'san' is valid", metadata.getValidator().validate("san").isValid(), equalTo(true));
        assertThat("'foo' is not valid", metadata.getValidator().validate("foo").isValid(), equalTo(false));
    }

    @Test
    public void testLoadingUnsignedIntMetadata() {
        ApplicationPropertyMetadata metadata = getAndCheck("jira.special.number", "uint", "5000");
        assertThat(metadata.isSysadminEditable(), equalTo(true));
    }

    @Test
    public void testLoadingListMetadata() {
        ApplicationPropertyMetadata metadata = getAndCheck("jira.scrabble.words", "list<string>", "qat,qis,qua,suq,qi");
        assertThat("'mock' is valid", metadata.getValidator().validate("mock").isValid(), equalTo(true));
        assertThat("'wok' is not valid", metadata.getValidator().validate("wok").isValid(), equalTo(false));
        assertThat("sysadmin is editable'", metadata.isSysadminEditable(), equalTo(true));
    }

    @Test
    public void testLoadingStrangeType() {
        ApplicationPropertyMetadata metadata = getAndCheck("jira.minimal.option", "music genre", "bubblegum-dubstep");
        assertThat("sysadmin is editable", metadata.isSysadminEditable(), equalTo(true));
        assertThat("restart is required", metadata.isRequiresRestart(), equalTo(true));
    }

    private ApplicationPropertyMetadata getAndCheck(String key, String type, String defaultValue) {
        ApplicationPropertyMetadata metadata = map.get(key);
        assertThat(metadata.getKey(), equalTo(key));
        assertThat(metadata.getType(), equalTo(type));
        assertThat(metadata.getDefaultValue(), equalTo(defaultValue));
        return metadata;
    }
}
