package com.atlassian.jira.application;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.jira.application.ApplicationRoleStore.ApplicationRoleData;
import com.atlassian.jira.mock.ofbiz.MockOfBizDelegator;
import com.atlassian.jira.util.CaseFolding;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Ordering;
import com.google.common.collect.Sets;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.junit.Test;
import org.ofbiz.core.entity.GenericValue;

import javax.annotation.Nullable;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import static com.atlassian.fugue.Option.none;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

public class OfBizApplicationRoleStoreTest {
    private static final String ROLE_ENTITY = "LicenseRoleGroup";
    private static final String ROLE_COL_NAME = "licenseRoleName";
    private static final String ROLE_COL_GROUP = "groupId";
    private static final String ROLE_COL_DEFAULT = "primaryGroup";

    private static final String DEFAULT_ENTITY = "LicenseRoleDefault";
    private static final String DEFAULT_COL_NAME = "licenseRoleName";

    private MockOfBizDelegator delegator = new MockOfBizDelegator();
    private OfBizApplicationRoleStore roleStore = new OfBizApplicationRoleStore(delegator);

    @Test
    public void getRoleNoRoleStored() {
        //given
        final ApplicationKey key = ApplicationKey.valueOf("iAmAKeyWithMultiCase");

        //when
        final ApplicationRoleData data = roleStore.get(key);

        //then
        assertThat(data, new ApplicationRoleDataMatcher().key(key));
    }

    @Test
    public void getRoleStoredWithoutDefault() {
        //given
        final ApplicationKey key = ApplicationKey.valueOf("iAmAKeyWithMultiCase");
        setDbRoles(dbRole(key).groups("ONE", "two", "THRee").defaultGroups());

        //when
        final ApplicationRoleData data = roleStore.get(key);

        //then
        assertThat(data, new ApplicationRoleDataMatcher().key(key).groups("one", "two", "three"));
    }

    @Test
    public void getRoleStoredWithAllNullDefault() {
        //given
        final ApplicationKey key = ApplicationKey.valueOf("iAmAKeyWithMultiCase");
        final String keyLowerCase = CaseFolding.foldString(key.value());
        makeNullDefaultEntries(keyLowerCase, groups("one", "TWO", "three"));

        //when
        final ApplicationRoleData data = roleStore.get(key);

        //then
        assertThat(data, new ApplicationRoleDataMatcher().key(key).groups("one", "two", "three"));
    }

    @Test
    public void getRoleStoredWithDefault() {
        //given
        final ApplicationKey key = ApplicationKey.valueOf("iAmAKeyWithMultiCase");
        setDbRoles(dbRole(key).groups("ONE", "two", "THRee").defaultGroups("ONE"));

        //when
        final ApplicationRoleData data = roleStore.get(key);

        //then
        assertThat(data, new ApplicationRoleDataMatcher().key(key).groups("one", "two", "three").defaultGroups("one"));
    }

    @Test
    public void getRoleStoredWithMultipleDefaults() {
        //given
        final ApplicationKey key = ApplicationKey.valueOf("iAmAKeyWithMultiCase");
        setDbRoles(dbRole(key).groups("one", "TWO", "abc").defaultGroups("one", "TWO", "abc"));

        //when
        final ApplicationRoleData data = roleStore.get(key);

        //then
        assertThat(data, new ApplicationRoleDataMatcher()
                .key(key)
                .groups("one", "abc", "two")
                .defaultGroups("one", "abc", "two"));
    }

    @Test
    public void setRoleDeleteAll() {
        final ApplicationKey key = ApplicationKey.valueOf("iAmAKeyWithMultiCase");
        final DbRole initial = dbRole(key).groups("one", "TWO", "three").defaultGroups("one");
        final ApplicationRoleData write = new ApplicationRoleData(key, none(), none(), false);

        assertSave(initial, write);
    }

    @Test
    public void setRoleRemoveGroup() {
        final ApplicationKey key = ApplicationKey.valueOf("iAmAKeyWithMultiCase");
        final DbRole initialState = dbRole(key).groups("one", "TWO", "three").defaultGroups();
        final ApplicationRoleData write = new ApplicationRoleData(key, groups("OnE", "two"), groups(), false);

        assertSave(initialState, write);
    }

    @Test
    public void setRoleAddGroupGroup() {
        final ApplicationKey key = ApplicationKey.valueOf("iAmAKeyWithMultiCase");
        final DbRole initial = dbRole(key).groups("one", "TWO").defaultGroups();
        final ApplicationRoleData write = new ApplicationRoleData(key, groups("OnE", "tWo", "THree"), groups(), false);

        assertSave(initial, write);
    }

    @Test
    public void setRoleAddGroupGroupAndSetSingleDefault() {
        final ApplicationKey key = ApplicationKey.valueOf("iAmAKeyWithMultiCase");
        final DbRole initial = dbRole(key).groups("one", "TWO").defaultGroups("TWO");
        final ApplicationRoleData write
                = new ApplicationRoleData(key, groups("ONE", "two", "THree"), groups("thrEE"), false);

        assertSave(initial, write);
    }

    @Test
    public void setRoleAddGroupGroupAndSetMultipleDefault() {
        //given
        final ApplicationKey key = ApplicationKey.valueOf("iAmAKeyWithMultiCase");
        final DbRole initial = dbRole(key).groups("one", "TWO").defaultGroups("one");
        final ApplicationRoleData write
                = new ApplicationRoleData(key, groups("one", "TWo", "THREE"), groups("two", "three"), false);

        assertSave(initial, write);
    }

    @Test
    public void setRoleAddGroupGroupAndUnSetDefault() {
        final ApplicationKey key = ApplicationKey.valueOf("iAmAKeyWithMultiCase");
        final DbRole initial = dbRole(key).groups("one", "TWO").defaultGroups(groups("one"));
        final ApplicationRoleData write = new ApplicationRoleData(key, groups("ONE", "tWo", "THRee"), groups(), false);

        assertSave(initial, write);
    }

    @Test
    public void setRoleReSetDefault() {
        final ApplicationKey key = ApplicationKey.valueOf("iAmAKeyWithMultiCase");
        final DbRole initial = dbRole(key).groups("one", "TWO").defaultGroups("one");
        final ApplicationRoleData write
                = new ApplicationRoleData(key, groups("onE", "twO"), groups("two"), false);

        assertSave(initial, write);
    }

    @Test
    public void setRoleReSetDefaults() {
        final ApplicationKey key = ApplicationKey.valueOf("iAmAKeyWithMultiCase");
        final DbRole initial = dbRole(key).groups("one", "TWO").defaultGroups("one");
        final ApplicationRoleData write
                = new ApplicationRoleData(key, groups("ONE", "two"), groups("one", "TWO"), false);

        assertSave(initial, write);
    }

    @Test
    public void setRoleSetDefault() {
        final ApplicationKey key = ApplicationKey.valueOf("iAmAKeyWithMultiCase");
        final DbRole initial = dbRole(key).groups("one", "TWO").defaultGroups();
        final ApplicationRoleData data
                = new ApplicationRoleData(key, groups("oNe", "two"), groups("two"), false);

        assertSave(initial, data);
    }

    @Test
    public void setRoleSetDefaults() {
        final ApplicationKey key = ApplicationKey.valueOf("iAmAKeyWithMultiCase");
        final DbRole initial = dbRole(key).groups("one", "two", "three").defaultGroups();
        final ApplicationRoleData data
                = new ApplicationRoleData(key, groups("three", "one", "two"), groups("oNe", "Two"), false);

        assertSave(initial, data);
    }

    @Test
    public void setRoleStoreSelectedByDefault() {
        final ApplicationKey key = ApplicationKey.valueOf("iAmAKeyWithMultiCase");
        final DbRole initial = dbRole(key).groups().defaultGroups();
        final ApplicationRoleData data = new ApplicationRoleData(key, groups(), groups(), true);

        assertSave(initial, data);
    }

    @Test
    public void setRoleStoreSelectedByDefaultDoesNotDuplicate() {
        final ApplicationKey key = ApplicationKey.valueOf("iAmAKeyWithMultiCase");
        final DbRole initial = dbRole(key).groups().defaultGroups().defaultRole(true);
        final ApplicationRoleData data = new ApplicationRoleData(key, groups(), groups(), true);

        assertSave(initial, data);
        assertThat(delegator.findAll(DEFAULT_ENTITY).size(), equalTo(1));
    }

    @Test
    public void setRoleStoreSelectedByDefaultDoesRemove() {
        final ApplicationKey key = ApplicationKey.valueOf("iAmAKeyWithMultiCase");
        final DbRole initial = dbRole(key).groups("one").defaultGroups().defaultRole(true);
        final ApplicationRoleData data = new ApplicationRoleData(key, groups("one"), groups(), false);

        assertSave(initial, data);
    }

    @Test
    public void setRoleStoreSelectedByDefaultDoesNotRemoveNotExisting() {
        final ApplicationKey key = ApplicationKey.valueOf("iAmAKeyWithMultiCase");
        final DbRole initial = dbRole(key).groups("one").defaultGroups().defaultRole(false);
        final ApplicationRoleData data = new ApplicationRoleData(key, groups("one"), groups(), false);

        assertSave(initial, data);
    }

    @Test
    public void setRoleSetDefaultIsNullInDB() {
        final ApplicationKey key = ApplicationKey.valueOf("iAmAKeyWithMultiCase");
        final String keyLowerCase = CaseFolding.foldString(key.value());

        createRoleRow(keyLowerCase, "one", null);
        createRoleRow(keyLowerCase, "two", false);

        ApplicationRoleData data =
                new ApplicationRoleData(key,
                        groups("one", "two"), groups("two"), false);

        final ApplicationRoleData result = roleStore.save(data);

        assertThat(result, new ApplicationRoleDataMatcher().key(key).groups("one", "two").defaultGroups("two"));
        assertThat(parseDbRoles(), equalTo(list(dbRole(key).defaultGroups("two").groups("one", "two"))));
    }

    @Test
    public void setRoleClearDefault() {
        final ApplicationKey key = ApplicationKey.valueOf("iAmAKeyWithMultiCase");
        final DbRole initial = dbRole(key).groups("one", "two").defaultGroups("one", "two").defaultRole(true);
        final ApplicationRoleData data = new ApplicationRoleData(key, groups("one", "two"), groups(), false);

        assertSave(initial, data);
    }

    @Test
    public void setRoleToEmptyLeavesDefaultAround() {
        final ApplicationKey key = ApplicationKey.valueOf("iAmAKeyWithMultiCase");
        final DbRole initial = dbRole(key).groups("one", "two").defaultGroups("one", "two").defaultRole(true);
        final ApplicationRoleData data = new ApplicationRoleData(key, groups(), groups(), true);

        assertSave(initial, data);
    }

    @Test
    public void removeGroupRemovesDefaultGroupToo() {
        //given
        final ApplicationKey key = ApplicationKey.valueOf("iAmAKeyWithMultiCase");
        setDbRoles(dbRole(key).groups("one", "two").defaultGroups("one").defaultRole(true));

        //when
        roleStore.removeGroup("ONE");

        //then
        assertThat(parseDbRoles(), equalTo(list(dbRole(key).groups("two").defaultRole(true))));
    }

    @Test
    public void removeGroupRemovesGroupOnly() {
        //given
        final ApplicationKey key = ApplicationKey.valueOf("iAmAKeyWithMultiCase");
        setDbRoles(dbRole(key).groups("one", "two").defaultGroups("one").defaultRole(true));

        //when
        roleStore.removeGroup("TWo");

        //then
        assertThat(parseDbRoles(), equalTo(list(dbRole(key).groups("one").defaultGroups("one").defaultRole(true))));
    }

    @Test
    public void removeLastGroupLeavesDefaultRoleAround() {
        //given
        final ApplicationKey key = ApplicationKey.valueOf("iAmAKeyWithMultiCase");
        setDbRoles(dbRole(key).groups("one").defaultRole(true));

        //when
        roleStore.removeGroup("one");

        //then
        assertThat(parseDbRoles(), equalTo(list(dbRole(key).groups().defaultGroups().defaultRole(true))));
    }

    @Test
    public void removeGroupIsNopWhenGroupDoesntMatch() {
        //given
        final ApplicationKey key = ApplicationKey.valueOf("iAmAKeyWithMultiCase");
        setDbRoles(dbRole(key).groups("one").defaultRole(true));

        //when
        roleStore.removeGroup("two");

        //then
        assertThat(parseDbRoles(), equalTo(list(dbRole(key).groups("one").defaultGroups().defaultRole(true))));
    }

    @Test
    public void removeRoleDataByKey() {
        //given
        final ApplicationKey key = ApplicationKey.valueOf("jira-tw");
        setDbRoles(dbRole(key).groups("one", "two").defaultRole(true).addDefaultGroup("two"));

        //when
        roleStore.removeByKey(key);

        //then
        assertThat(parseDbRoles(), is(empty()));
    }

    @Test
    public void testHasGroupConfigured() {
        setDbRoles(dbRole(ApplicationKey.valueOf("jira-tw")).groups("One", "two")
                .defaultRole(true)
                .addDefaultGroup("two"));
        assertThat(roleStore.isGroupUsed("one"), is(true));
    }

    @Test
    public void testHasNoGroupConfigured() {
        setDbRoles(dbRole(ApplicationKey.valueOf("jira-tw")).groups("one", "two")
                .defaultRole(true)
                .addDefaultGroup("two"));
        assertThat(roleStore.isGroupUsed("Three"), is(false));
    }

    @Test
    public void testHasNoGroupConfiguredWhenNoGroupsConfigurationExists() {
        assertThat(roleStore.isGroupUsed("Three"), is(false));
    }

    @Test
    public void testHasNoGroupConfiguredWhenNoGroupsConfigured() {
        setDbRoles(dbRole(ApplicationKey.valueOf("jira-tw")).groups().defaultRole(true));
        assertThat(roleStore.isGroupUsed("Three"), is(false));
    }

    private List<GenericValue> makeNullDefaultEntries(String id, Set<String> groups) {
        List<GenericValue> lists = Lists.newArrayList();
        for (String group : groups) {
            lists.add(createRoleRow(id, group, null));
        }
        return lists;
    }

    private void createRoleRows(String id, Set<String> groups, Set<String> primaries) {
        for (String group : groups) {
            createRoleRow(id, group, primaries.contains(group));
        }
    }

    private GenericValue createRoleRow(String id, String groupName, @Nullable Boolean primary) {
        Map<String, Object> fields = Maps.newHashMap();
        fields.put(ROLE_COL_NAME, id);
        fields.put(ROLE_COL_GROUP, groupName);
        fields.put(ROLE_COL_DEFAULT, primary);

        return delegator.createValue(ROLE_ENTITY, fields);
    }

    private GenericValue createDefaultRow(String id) {
        Map<String, Object> fields = Maps.newHashMap();
        fields.put(DEFAULT_COL_NAME, id);

        return delegator.createValue(DEFAULT_ENTITY, fields);
    }

    private static Set<String> groups() {
        return ImmutableSet.of();
    }

    private static Set<String> groups(String... elements) {
        return ImmutableSet.copyOf(elements);
    }

    private List<DbRole> parseDbRoles() {
        Map<ApplicationKey, DbRole> roles = Maps.newHashMap();
        for (final GenericValue genericValue : delegator.findAll(ROLE_ENTITY)) {
            final ApplicationKey key = ApplicationKey.valueOf(genericValue.getString(ROLE_COL_NAME));
            final String groupId = genericValue.getString(ROLE_COL_GROUP);
            final boolean defaultGroup = Boolean.TRUE.equals(genericValue.getBoolean(ROLE_COL_DEFAULT));

            final DbRole dbRole = roles.computeIfAbsent(key, DbRole::new);
            dbRole.addGroup(groupId);
            if (defaultGroup) {
                dbRole.addDefaultGroup(groupId);
            }
        }

        for (GenericValue genericValue : delegator.findAll(DEFAULT_ENTITY)) {
            final ApplicationKey name = ApplicationKey.valueOf(genericValue.getString(DEFAULT_COL_NAME));
            roles.computeIfAbsent(name, DbRole::new).defaultRole(true);
        }

        return Ordering.natural().onResultOf((Function<DbRole, String>) role -> role.getKey().value())
                .immutableSortedCopy(roles.values());
    }

    private static List<DbRole> dbRoles() {
        return ImmutableList.of();
    }

    private static DbRole dbRole(ApplicationKey key) {
        return new DbRole(key);
    }

    private static DbRole dbRole(ApplicationRoleData data) {
        return dbRole(data.getKey())
                .groups(lowerCase(data.getGroups()))
                .defaultGroups(lowerCase(data.getDefaultGroups()))
                .defaultRole(data.isSelectedByDefault());
    }

    private void setDbRoles(DbRole... roles) {
        for (DbRole role : roles) {
            final String id = CaseFolding.foldString(role.getKey().value());
            createRoleRows(id, lowerCase(role.getGroups()), lowerCase(role.getDefaultGroups()));
            if (role.isDefaultRole()) {
                createDefaultRow(id);
            }
        }
    }

    private static Set<String> lowerCase(Collection<String> data) {
        return data.stream().map(CaseFolding::foldString).collect(Collectors.toSet());
    }

    private void assertSave(DbRole initialState, ApplicationRoleData write) {
        //given
        setDbRoles(initialState);

        //when
        final ApplicationRoleData result = roleStore.save(write);

        //then
        if (write.getGroups().isEmpty() && !write.isSelectedByDefault()) {
            assertThat(parseDbRoles(), equalTo(dbRoles()));
            assertThat(result, new ApplicationRoleDataMatcher().key(write.getKey()));
        } else {

            assertThat(parseDbRoles(), equalTo(list(dbRole(write))));
            assertThat(result, matcherFromData(write));
        }
    }

    private ApplicationRoleDataMatcher matcherFromData(ApplicationRoleData write) {
        final Set<String> lowerGroups = lowerCase(write.getGroups());
        final Set<String> lowerDefaults = lowerCase(write.getDefaultGroups());
        return new ApplicationRoleDataMatcher().key(write.getKey())
                .groups(lowerGroups)
                .defaultGroups(lowerDefaults)
                .selectedByDefault(write.isSelectedByDefault());
    }

    @SafeVarargs
    private static <T> List<T> list(T... items) {
        return ImmutableList.copyOf(items);
    }

    private static class DbRole implements Comparable<DbRole> {
        private ApplicationKey key;
        private Set<String> groups;
        private Set<String> defaultGroups;
        private boolean defaultRole;

        private DbRole(ApplicationKey key) {
            this(key, ImmutableSet.of(), ImmutableSet.of());
        }

        private DbRole(final ApplicationKey key, Iterable<String> groups, Iterable<String> defaultGroups) {
            this.key = key;
            this.groups = Sets.newHashSet(groups);
            this.defaultGroups = Sets.newHashSet(defaultGroups);
        }

        private DbRole addGroup(String group) {
            this.groups.add(group);
            return this;
        }

        private DbRole addDefaultGroup(String group) {
            this.defaultGroups.add(group);
            return this;
        }

        private DbRole groups(String... groups) {
            this.groups = Sets.newHashSet(groups);
            return this;
        }

        private DbRole groups(Iterable<String> groups) {
            this.groups = Sets.newHashSet(groups);
            return this;
        }

        private DbRole defaultGroups(String... groups) {
            this.defaultGroups = Sets.newHashSet(groups);
            return this;
        }

        private DbRole defaultGroups(Iterable<String> groups) {
            this.defaultGroups = Sets.newHashSet(groups);
            return this;
        }

        private DbRole defaultRole(boolean value) {
            this.defaultRole = value;
            return this;
        }

        private ApplicationKey getKey() {
            return key;
        }

        public Set<String> getGroups() {
            return groups;
        }

        public Set<String> getDefaultGroups() {
            return defaultGroups;
        }

        public boolean isDefaultRole() {
            return defaultRole;
        }

        @Override
        public boolean equals(@Nullable final Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }
            final DbRole dbRole = (DbRole) o;
            return Objects.equals(isDefaultRole(), dbRole.isDefaultRole()) &&
                    Objects.equals(getKey(), dbRole.getKey()) &&
                    Objects.equals(getGroups(), dbRole.getGroups()) &&
                    Objects.equals(getDefaultGroups(), dbRole.getDefaultGroups());
        }

        @Override
        public int hashCode() {
            return Objects.hash(getKey(), getGroups(), getDefaultGroups(), isDefaultRole());
        }

        @Override
        public String toString() {
            return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
                    .append("key", key)
                    .append("groups", groups)
                    .append("defaults", defaultGroups)
                    .append("isDefault", defaultRole)
                    .toString();
        }

        @Override
        public int compareTo(final DbRole o) {
            return this.getKey().value().compareToIgnoreCase(o.getKey().value());
        }
    }
}