package com.atlassian.jira.issue.history;

import com.atlassian.cache.memory.MemoryCacheManager;
import com.atlassian.core.ofbiz.test.UtilsForTests;
import com.atlassian.crowd.embedded.api.CrowdService;
import com.atlassian.crowd.exception.InvalidCredentialException;
import com.atlassian.crowd.exception.InvalidUserException;
import com.atlassian.crowd.exception.OperationNotPermittedException;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.config.DefaultConstantsManager;
import com.atlassian.jira.config.MockIssueConstantFactory;
import com.atlassian.jira.config.SubTaskManager;
import com.atlassian.jira.database.MockDbConnectionManager;
import com.atlassian.jira.database.ResultRow;
import com.atlassian.jira.entity.EntityEngine;
import com.atlassian.jira.entity.EntityEngineImpl;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoContainer;
import com.atlassian.jira.mock.MockConstantsManager;
import com.atlassian.jira.mock.ofbiz.MockGenericValue;
import com.atlassian.jira.mock.ofbiz.MockOfBizDelegator;
import com.atlassian.jira.mock.ofbiz.ModelReaderMock;
import com.atlassian.jira.mock.security.MockSimpleAuthenticationContext;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.MockCrowdService;
import com.atlassian.jira.user.util.MockUserManager;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.collect.MapBuilder;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Maps;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.ofbiz.core.entity.GenericEntityException;
import org.ofbiz.core.entity.GenericValue;
import org.ofbiz.core.entity.model.MockModelEntity;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;

public class TestChangeLogUtils {
    @Rule
    public MockitoContainer mockitoContainer = new MockitoContainer(this);

    @Mock
    @AvailableInContainer
    private SubTaskManager subTaskManager;

    @Mock
    private EventPublisher eventPublisher;

    @AvailableInContainer
    private final OfBizDelegator ofbiz = new MockOfBizDelegator();

    @AvailableInContainer
    private EntityEngine entityEngine = new EntityEngineImpl(ofbiz);

    @AvailableInContainer
    private final CrowdService crowdService = new MockCrowdService();

    @AvailableInContainer
    private final UserManager userManager = new MockUserManager();

    private MockDbConnectionManager dbConnectionManager = new MockDbConnectionManager();

    ApplicationUser user;
    ConstantsManager constantsManager;

    @Before
    public void setUp() throws Exception {
        ((MockOfBizDelegator) ofbiz).setModelReader(ModelReaderMock.getMock(entityName -> {
            if ("Issue".equals(entityName)) {
                return new MockModelEntity(entityName, Arrays.asList("status", "summary"));
            } else {
                return null;
            }
        }));
        user = createMockUser("bob", "Bob the Builder");
        constantsManager = new DefaultConstantsManager(new MockSimpleAuthenticationContext(user), ofbiz, dbConnectionManager,
                new MockIssueConstantFactory(ofbiz), new MemoryCacheManager(), eventPublisher);
        mockitoContainer.getMockWorker().addMock(ConstantsManager.class, constantsManager);
    }

    @Test
    public void testGenerateChangeItemSame() throws GenericEntityException {
        final GenericValue before = new MockGenericValue("Issue", MapBuilder.build("id", "5"));
        final GenericValue after = new MockGenericValue("Issue", MapBuilder.build("id", "5"));

        assertNull(ChangeLogUtils.generateChangeItem(before, after, "id"));
    }

    @Test
    public void testGenerateChangeItemNulls() throws GenericEntityException {
        final GenericValue before = new MockGenericValue("Issue", MapBuilder.build("id", null));
        final GenericValue after = new MockGenericValue("Issue", MapBuilder.build("id", null));

        assertNull(ChangeLogUtils.generateChangeItem(before, after, "id"));
    }

    @Test
    public void testGenerateChangeItemBeforeNull() throws GenericEntityException {
        final GenericValue before = new MockGenericValue("Issue", MapBuilder.build("id", null));
        final GenericValue after = new MockGenericValue("Issue", MapBuilder.build("id", "5"));

        final ChangeItemBean expected = new ChangeItemBean(ChangeItemBean.STATIC_FIELD, "id", null, null, null, "5");

        assertEquals(expected, ChangeLogUtils.generateChangeItem(before, after, "id"));
    }

    @Test
    public void testGenerateChangeItemAfterNull() throws GenericEntityException {
        final GenericValue before = new MockGenericValue("Issue", MapBuilder.build("id", "5"));
        final GenericValue after = new MockGenericValue("Issue", MapBuilder.build("id", null));

        final ChangeItemBean expected = new ChangeItemBean(ChangeItemBean.STATIC_FIELD, "id", null, "5", null, null);

        assertEquals(expected, ChangeLogUtils.generateChangeItem(before, after, "id"));
    }

    @Test
    public void testGenerateChangeItemDifferent() throws GenericEntityException {
        final GenericValue before = new MockGenericValue("Issue", MapBuilder.build("id", "5"));
        final GenericValue after = new MockGenericValue("Issue", MapBuilder.build("id", "7"));

        final ChangeItemBean expected = new ChangeItemBean(ChangeItemBean.STATIC_FIELD, "id", null, "5", null, "7");

        assertEquals(expected, ChangeLogUtils.generateChangeItem(before, after, "id"));
    }

    @Test
    public void testGenerateChangeItemType() throws GenericEntityException {
        final GenericValue before = UtilsForTests.getTestEntity("Issue", MapBuilder.build("type", "10"));
        final GenericValue after = UtilsForTests.getTestEntity("Issue", Maps.newHashMap());

        UtilsForTests.getTestEntity("IssueType", MapBuilder.build("id", "10", "name", "Foo"));

        String sql = "select ISSUE_TYPE.id, ISSUE_TYPE.sequence, ISSUE_TYPE.pname, ISSUE_TYPE.pstyle, ISSUE_TYPE.description, ISSUE_TYPE.iconurl, ISSUE_TYPE.avatar\n"
                + "from issuetype ISSUE_TYPE\n"
                + "order by ISSUE_TYPE.pstyle asc, ISSUE_TYPE.sequence asc";
        dbConnectionManager.setQueryResults(sql, ImmutableList.of(new ResultRow("10", 1L, "Foo", null, null, null, null)));
        ComponentAccessor.getConstantsManager().refreshIssueTypes();

        final ChangeItemBean expected = new ChangeItemBean(ChangeItemBean.STATIC_FIELD, "type", "10", "Foo", null, null);

        assertEquals(expected, ChangeLogUtils.generateChangeItem(before, after, "type"));
    }

    @Test
    public void testGenerateChangeItemResolution() throws GenericEntityException {
        final GenericValue before = UtilsForTests.getTestEntity("Issue", MapBuilder.build("resolution", "5"));
        final GenericValue after = UtilsForTests.getTestEntity("Issue", Maps.newHashMap());

        String sql = "select RESOLUTION.id, RESOLUTION.sequence, RESOLUTION.pname, RESOLUTION.description, RESOLUTION.iconurl\n"
                + "from resolution RESOLUTION\n"
                + "order by RESOLUTION.sequence asc";
        dbConnectionManager.setQueryResults(sql, ImmutableList.of(new ResultRow("5", 1L, "Solved", null, null)));
        ComponentAccessor.getConstantsManager().refreshResolutions();

        final ChangeItemBean expected = new ChangeItemBean(ChangeItemBean.STATIC_FIELD, "resolution", "5", "Solved", null, null);

        assertEquals(expected, ChangeLogUtils.generateChangeItem(before, after, "resolution"));
    }

    @Test
    public void testGenerateChangeItemPriority() throws GenericEntityException {
        final GenericValue before = UtilsForTests.getTestEntity("Issue", MapBuilder.build("priority", "5"));
        final GenericValue after = UtilsForTests.getTestEntity("Issue", Maps.newHashMap());

        String sql = "select PRIORITY.id, PRIORITY.sequence, PRIORITY.pname, PRIORITY.description, PRIORITY.iconurl, PRIORITY.status_color\n"
                + "from priority PRIORITY\n"
                + "order by PRIORITY.sequence asc";
        dbConnectionManager.setQueryResults(sql, ImmutableList.of(new ResultRow("5", 1L, "Top Shelf", null, null, null)));
        ComponentAccessor.getConstantsManager().refreshPriorities();

        final ChangeItemBean expected = new ChangeItemBean(ChangeItemBean.STATIC_FIELD, "priority", "5", "Top Shelf", null, null);

        assertEquals(expected, ChangeLogUtils.generateChangeItem(before, after, "priority"));
    }

    @Test
    public void testGenerateChangeItemAssignee() throws Exception {
        final GenericValue before = UtilsForTests.getTestEntity("Issue", MapBuilder.build("assignee", "bob"));
        final GenericValue after = UtilsForTests.getTestEntity("Issue", Maps.newHashMap());

        final ChangeItemBean expected = new ChangeItemBean(ChangeItemBean.STATIC_FIELD, "assignee", "bob", "Bob the Builder", null, null);

        assertEquals(expected, ChangeLogUtils.generateChangeItem(before, after, "assignee"));
    }

    @Test
    public void testGenerateChangeItemReporter() throws Exception {
        final GenericValue before = UtilsForTests.getTestEntity("Issue", MapBuilder.build("reporter", "bob"));
        final GenericValue after = UtilsForTests.getTestEntity("Issue", Maps.newHashMap());

        final ChangeItemBean expected = new ChangeItemBean(ChangeItemBean.STATIC_FIELD, "reporter", "bob", "Bob the Builder", null, null);

        assertEquals(expected, ChangeLogUtils.generateChangeItem(before, after, "reporter"));
    }

    @Test
    public void testGenerateChangeItemEstimate() throws Exception {
        final GenericValue before = UtilsForTests.getTestEntity("Issue", MapBuilder.build("timeestimate", (long) 7200));
        final GenericValue after = UtilsForTests.getTestEntity("Issue", Maps.newHashMap());

        final ChangeItemBean expected = new ChangeItemBean(ChangeItemBean.STATIC_FIELD, "timeestimate", "7200", "7200", null, null);

        assertEquals(expected, ChangeLogUtils.generateChangeItem(before, after, "timeestimate"));
    }

    @Test
    public void testGenerateChangeItemSpent() throws Exception {
        final GenericValue before = UtilsForTests.getTestEntity("Issue", Maps.newHashMap());
        final GenericValue after = UtilsForTests.getTestEntity("Issue", MapBuilder.build("timespent", (long) 3600));

        final ChangeItemBean expected = new ChangeItemBean(ChangeItemBean.STATIC_FIELD, "timespent", null, null, "3600", "3600");

        assertEquals(expected, ChangeLogUtils.generateChangeItem(before, after, "timespent"));
    }

    @Test
    public void testGenerateChangeItemStatus() throws Exception {
        final GenericValue before = UtilsForTests.getTestEntity("Issue", MapBuilder.build("status", "1"));
        final GenericValue after = UtilsForTests.getTestEntity("Issue", Maps.newHashMap());

        final MockConstantsManager mcm = new MockConstantsManager();
        mockitoContainer.getMockComponentContainer().addMock(ConstantsManager.class, mcm);

        final GenericValue status = UtilsForTests.getTestEntity("Status", MapBuilder.build("id", "1", "name", "high"));
        mcm.addStatus(status);

        final ChangeItemBean expected = new ChangeItemBean(ChangeItemBean.STATIC_FIELD, "status", "1", "high", null, null);

        assertEquals(expected, ChangeLogUtils.generateChangeItem(before, after, "status"));
    }

    @Test
    public void testCreateChangeGroupIdentical() throws Exception {
        final GenericValue before = UtilsForTests.getTestEntity("Issue", MapBuilder.build("status", "1", "summary", "2"));
        assertNull(ChangeLogUtils.createChangeGroup((ApplicationUser) null, before, (GenericValue) before.clone(), null, true));
    }

    @Test
    public void testCreateChangeGroupNoValues() throws Exception {
        final GenericValue before = UtilsForTests.getTestEntity("Issue", MapBuilder.build("id", (long) 1, "status", null));
        final GenericValue after = before;
        assertNull(ChangeLogUtils.createChangeGroup((ApplicationUser) null, before, after, null, true));
    }

    @Test
    public void testCreateChangeGroup() throws Exception {
        final GenericValue before = UtilsForTests.getTestEntity("Issue", MapBuilder.build("status", "1", "summary", "2"));
        final GenericValue after = UtilsForTests.getTestEntity("Issue", MapBuilder.build("status", "2", "summary", "4"));
        String sql = "select STATUS.id, STATUS.sequence, STATUS.pname, STATUS.description, STATUS.iconurl, STATUS.statuscategory\n"
                + "from issuestatus STATUS\n"
                + "order by STATUS.sequence asc";
        dbConnectionManager.setQueryResults(sql, ImmutableList.of(
                new ResultRow("1", 2L, "summary", null, null),
                new ResultRow("2", 4L, "summary", null, null)));
        ComponentAccessor.getConstantsManager().refreshResolutions();

        final GenericValue changeGroup = ChangeLogUtils.createChangeGroup(user, before, after, null, true);
        assertNotNull(changeGroup.getLong("id"));
        assertNotNull(changeGroup.getTimestamp("created"));
        assertEquals("bob", changeGroup.getString("author"));

        final List<GenericValue> changeItems = ofbiz.findByAnd("ChangeItem", MapBuilder.build("group", changeGroup.getLong("id")));
        assertThat(changeItems, hasSize(2));
    }

    @Test
    public void testCreateChangeGroupNoChangeJustList() throws Exception {
        final GenericValue before = UtilsForTests.getTestEntity("Issue", MapBuilder.build("id", (long) 1, "status", null));

        final ChangeItemBean cib = new ChangeItemBean(ChangeItemBean.STATIC_FIELD, "foo", "bar", "baz");
        final GenericValue changeGroup = ChangeLogUtils.createChangeGroup((ApplicationUser) null, before, (GenericValue) before.clone(), ImmutableList.of(cib), true);
        final List<GenericValue> changeItems = ofbiz.findByAnd("ChangeItem", MapBuilder.build("group", changeGroup.getLong("id")));
        assertThat(changeItems, hasSize(1));
    }

    private ApplicationUser createMockUser(final String userName, final String fullname)
            throws OperationNotPermittedException, InvalidUserException, InvalidCredentialException {
        final ApplicationUser user = new MockApplicationUser(userName, fullname, "fullname@somewhere.com");
        final CrowdService crowdService = ComponentAccessor.getCrowdService();
        crowdService.addUser(user.getDirectoryUser(), "password");
        ((MockUserManager) userManager).addUser(user);
        return user;
    }
}

