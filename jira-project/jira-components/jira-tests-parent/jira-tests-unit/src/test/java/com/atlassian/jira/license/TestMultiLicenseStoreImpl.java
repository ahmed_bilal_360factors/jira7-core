package com.atlassian.jira.license;

import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.entity.Delete;
import com.atlassian.jira.entity.EntityEngine;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.List;

import static com.atlassian.jira.entity.Delete.from;
import static com.atlassian.jira.entity.Entity.PRODUCT_LICENSE;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

/**
 * Test case for {@link com.atlassian.jira.license.MultiLicenseStoreImpl}.
 *
 * @since v6.3
 */
public class TestMultiLicenseStoreImpl {
    private static final String LICENSE1 = "a1";
    private static final String LICENSE2 = "a2";

    @Rule
    public final MockitoRule initMocks = MockitoJUnit.rule();

    @Mock
    private EntityEngine entityEngine;
    @Mock
    private EntityEngine.SelectFromContext<ProductLicense> selectFromContext;
    @Mock
    private EntityEngine.WhereContext<ProductLicense> whereContext;
    @Mock
    private ApplicationProperties applicationProperties;
    private MultiLicenseStoreImpl multiLicenseStore;

    @Before
    public void setup() {
        when(entityEngine.selectFrom(PRODUCT_LICENSE)).thenReturn(selectFromContext);
        when(selectFromContext.findAll()).thenReturn(whereContext);
        List<ProductLicense> productLicenseEntities = Lists.newArrayList(new ProductLicense(LICENSE1), new ProductLicense(LICENSE2));
        when(whereContext.orderBy("id")).thenReturn(productLicenseEntities);

        multiLicenseStore = new MultiLicenseStoreImpl(entityEngine, applicationProperties);
    }

    @Test
    public void licensesReplacedUponStoring() {
        Delete.DeleteWhereContext from = from(PRODUCT_LICENSE).all();
        MultiLicenseStoreImpl multiLicenseStore = new MultiLicenseStoreImpl(entityEngine, applicationProperties);

        String licenseKey1 = "licenseKey1";
        String licenseKey2 = "licenseKey2";
        multiLicenseStore.store(ImmutableSet.of(licenseKey1, licenseKey2));

        verify(entityEngine).delete(from);
        verify(entityEngine).createValue(PRODUCT_LICENSE, new ProductLicense(licenseKey1));
        verify(entityEngine).createValue(PRODUCT_LICENSE, new ProductLicense(licenseKey2));
    }

    @Test
    public void absentLicenseIsRepresentedAsEmptyList() {
        when(entityEngine.selectFrom(PRODUCT_LICENSE)).thenReturn(selectFromContext);
        when(selectFromContext.findAll()).thenReturn(whereContext);
        when(whereContext.orderBy("id")).thenReturn(Lists.newArrayList());
        when(applicationProperties.getText(APKeys.JIRA_LICENSE)).thenReturn(null);

        assertThat(multiLicenseStore.retrieve().isEmpty(), Matchers.equalTo(true));
    }

    @Test
    public void multipleLicensesAreRetrieved() {
        when(applicationProperties.getText(APKeys.JIRA_LICENSE)).thenReturn(null);

        assertThat(multiLicenseStore.retrieve(), Matchers.containsInAnyOrder(LICENSE1, LICENSE2));
    }

    @Test(expected = IllegalArgumentException.class)
    public void noLicenseFails() {
        MultiLicenseStoreImpl multiLicenseStore = new MultiLicenseStoreImpl(entityEngine, applicationProperties);
        multiLicenseStore.store(Lists.newArrayList()); // this should store a null - in production this should fail.
    }

    @Test
    public void resetOldBuildConfirmationOnlyDelegatesToLicenseStore() {
        MultiLicenseStoreImpl multiLicenseStore = new MultiLicenseStoreImpl(entityEngine, applicationProperties);

        multiLicenseStore.resetOldBuildConfirmation();

        verify(applicationProperties).setOption(APKeys.JIRA_CONFIRMED_INSTALL_WITH_OLD_LICENSE, false);
        verify(applicationProperties).setString(APKeys.JIRA_CONFIRMED_INSTALL_WITH_OLD_LICENSE_TIMESTAMP, "");
        verify(applicationProperties).setString(APKeys.JIRA_CONFIRMED_INSTALL_WITH_OLD_LICENSE_USER, "");
        verifyNoMoreInteractions(entityEngine, applicationProperties);
    }

    @Test
    public void resetConfirmProceedUnderEvaluationTermsOnlyDelegatesToLicenseStore() {
        MultiLicenseStoreImpl multiLicenseStore = new MultiLicenseStoreImpl(entityEngine, applicationProperties);

        multiLicenseStore.confirmProceedUnderEvaluationTerms("a");

        verify(applicationProperties).setOption(APKeys.JIRA_CONFIRMED_INSTALL_WITH_OLD_LICENSE, true);
        verify(applicationProperties).setString(APKeys.JIRA_CONFIRMED_INSTALL_WITH_OLD_LICENSE_USER, "a");
        verify(applicationProperties).setString(eq(APKeys.JIRA_CONFIRMED_INSTALL_WITH_OLD_LICENSE_TIMESTAMP), anyString());

        verifyNoMoreInteractions(entityEngine, applicationProperties);
    }
}
