package com.atlassian.jira.imports.project.handler;

import com.atlassian.jira.imports.project.taskprogress.EntityCountTaskProgressProcessor;
import com.atlassian.jira.imports.project.taskprogress.TaskProgressInterval;
import com.atlassian.jira.junit.rules.InitMockitoMocks;
import com.atlassian.jira.mock.i18n.MockI18nHelper;
import com.atlassian.jira.task.TaskProgressSink;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;

import static org.mockito.Mockito.verify;

/**
 * @since v3.13
 */
public class TestEntityCountTaskProgressProcessor {
    private final MockI18nHelper i18nHelper = new MockI18nHelper();

    @Rule
    public InitMockitoMocks initMocks = new InitMockitoMocks(this);

    @Mock
    public TaskProgressSink taskProgressSink;


    @Test
    public void testNullTaskProgressInterval() {
        EntityCountTaskProgressProcessor entityCountTaskProgressProcessor = new EntityCountTaskProgressProcessor(null, "Testing", 1000, i18nHelper);
        entityCountTaskProgressProcessor.processTaskProgress("IssueType", 5, 40, 8);
        // That's all we do - just want to prove it doesn't throw NPE
    }

    @Test
    public void testProcessTaskProgress0Entities() {
        TaskProgressInterval taskProgressInterval = new TaskProgressInterval(taskProgressSink, 20, 30);

        EntityCountTaskProgressProcessor entityCountTaskProgressProcessor = new EntityCountTaskProgressProcessor(taskProgressInterval, "Testing", 0, i18nHelper);
        entityCountTaskProgressProcessor.processTaskProgress("IssueType", 1, 0, 0);

        // 0 percent done in our section - 20% overall.
        verifyProgress(20, 0, 0);
    }

    @Test
    public void testProcessTaskProgress0PercentDone() {
        TaskProgressInterval taskProgressInterval = new TaskProgressInterval(taskProgressSink, 20, 30);

        EntityCountTaskProgressProcessor entityCountTaskProgressProcessor = new EntityCountTaskProgressProcessor(taskProgressInterval, "Testing", 1000, i18nHelper);
        entityCountTaskProgressProcessor.processTaskProgress("IssueType", 1, 0, 0);

        // 0 percent done in our section - 20% overall.
        verifyProgress(20, 0, 1000);
    }

    @Test
    public void testProcessTaskProgress20PercentDone() {
        TaskProgressInterval taskProgressInterval = new TaskProgressInterval(taskProgressSink, 20, 40);

        EntityCountTaskProgressProcessor entityCountTaskProgressProcessor = new EntityCountTaskProgressProcessor(taskProgressInterval, "Testing", 1000, i18nHelper);
        entityCountTaskProgressProcessor.processTaskProgress("IssueType", 5, 250, 8);

        // 20 percent done in our section - 25% overall.
        verifyProgress(25, 250, 1000);
    }

    @Test
    public void testProcessTaskProgress100PercentDone() {
        TaskProgressInterval taskProgressInterval = new TaskProgressInterval(taskProgressSink, 20, 30);

        EntityCountTaskProgressProcessor entityCountTaskProgressProcessor = new EntityCountTaskProgressProcessor(taskProgressInterval, "Testing", 1000, i18nHelper);
        entityCountTaskProgressProcessor.processTaskProgress("IssueType", 20, 1000, 8);

        // 100 percent done in our section - 30% overall.
        verifyProgress(30, 1000, 1000);
    }

    private void verifyProgress(final int overallPercentage, final int entityProgress, final int entityCount) {
        verify(taskProgressSink).makeProgress(overallPercentage, "Testing. admin.message.task.progress.processing [IssueType]", "admin.message.task.progress.entity.of [" + entityProgress + "] [" + entityCount + "]");
    }

}
