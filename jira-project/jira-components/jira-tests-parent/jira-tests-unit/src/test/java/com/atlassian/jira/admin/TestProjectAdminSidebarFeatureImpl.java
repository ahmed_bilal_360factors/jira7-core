package com.atlassian.jira.admin;

import com.atlassian.jira.bc.project.ProjectService;
import com.atlassian.jira.mock.servlet.MockHttpServletRequest;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.web.ExecutingHttpRequest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.http.HttpServletRequest;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public final class TestProjectAdminSidebarFeatureImpl {
    @Mock
    private JiraAuthenticationContext jiraAuthenticationContext;
    @Mock
    private ProjectService projectService;
    @Mock
    private ProjectService.GetProjectResult getProjectResult;
    @Mock
    private ApplicationUser applicationUser;
    @Mock
    private Project project;
    @Mock
    private HttpServletRequest httpServletRequest = new MockHttpServletRequest();

    private ProjectAdminSidebarFeatureImpl projectAdminSidebarFeature;

    @Before
    public void setUp() {
        when(jiraAuthenticationContext.getLoggedInUser()).thenReturn(applicationUser);
        when(projectService.getProjectByKey(eq(applicationUser), anyString())).thenReturn(getProjectResult);
        when(getProjectResult.isValid()).thenReturn(true);
        when(httpServletRequest.getAttribute(anyString())).thenReturn("PROJECT-KEY");
        ExecutingHttpRequest.set(httpServletRequest, null);

        this.projectAdminSidebarFeature = spy(new ProjectAdminSidebarFeatureImpl(jiraAuthenticationContext, projectService));
    }

    @After
    public void tearDown() {
        ExecutingHttpRequest.clear();
    }

    @Test
    public void noIdentifiableUserShouldNotTriggerSidebar() {
        when(jiraAuthenticationContext.getLoggedInUser()).thenReturn(null);

        assertThat("null user means sidebar should not be present", projectAdminSidebarFeature.shouldDisplay(), is(false));
    }

    @Test
    public void emptyProjectKeyInContextShouldNotTriggerSidebar() {
        when(httpServletRequest.getAttribute(anyString())).thenReturn(null);

        assertThat("user does not have browse projects permission so sidebar should not be present", projectAdminSidebarFeature.shouldDisplay(), is(false));
    }

    @Test
    public void whenUserDoesNotHaveBrowseProjectsPermissionForCurrentProjectSidebarShouldNotBePresent() {
        when(getProjectResult.isValid()).thenReturn(false);

        assertThat("user does not have browse projects permission so sidebar should not be present", projectAdminSidebarFeature.shouldDisplay(), is(false));
    }

    @Test
    public void whenUserDoesHaveBrowseProjectsPermissionForCurrentProjectSidebarShouldBePresent() {
        assertThat("user does have permission to browse project so sidebar should be present", projectAdminSidebarFeature.shouldDisplay(), is(true));
    }
}
