package com.atlassian.jira.issue.search.util;

import com.atlassian.jira.JiraDataType;
import com.atlassian.jira.JiraDataTypes;
import com.atlassian.jira.issue.fields.Field;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.fields.NavigableField;
import com.atlassian.jira.issue.search.ClauseNames;
import com.atlassian.jira.issue.search.SearchContext;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.issue.search.managers.SearchHandlerManager;
import com.atlassian.jira.issue.search.searchers.IssueSearcher;
import com.atlassian.jira.issue.search.searchers.SearcherGroup;
import com.atlassian.jira.jql.ClauseHandler;
import com.atlassian.jira.jql.ClauseInformation;
import com.atlassian.jira.jql.context.ClauseContextFactory;
import com.atlassian.jira.jql.operator.OperatorClasses;
import com.atlassian.jira.jql.permission.ClausePermissionHandler;
import com.atlassian.jira.jql.query.ClauseQueryFactory;
import com.atlassian.jira.jql.validator.ClauseValidator;
import com.atlassian.jira.mock.i18n.MockI18nHelper;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.collect.CollectionBuilder;
import com.atlassian.query.QueryImpl;
import com.atlassian.query.clause.TerminalClauseImpl;
import com.atlassian.query.operator.Operator;
import com.atlassian.query.order.OrderBy;
import com.atlassian.query.order.OrderByImpl;
import com.atlassian.query.order.SearchSort;
import com.atlassian.query.order.SortOrder;
import com.google.common.collect.Maps;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.MethodRule;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static java.util.Collections.emptyList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestSearchSortUtilImpl {
    @Rule
    public final MethodRule initMockito = MockitoJUnit.rule();
    @Rule
    public final ExpectedException exception = ExpectedException.none();


    @Mock
    private SearchHandlerManager searchHandlerManager;
    @Mock
    private FieldManager fieldManager;

    private ApplicationUser theUser = new MockApplicationUser("lwlodarczyk");
    private I18nHelper i18n = new MockI18nHelper();

    @InjectMocks
    private SearchSortUtilImpl searchSortUtil;


    @Test
    public void testGetSearchSortsNullQuery() throws Exception {
        final List<SearchSort> sorts = searchSortUtil.getSearchSorts(null);
        assertThat(sorts, contains(SearchSortUtilImpl.DEFAULT_KEY_SORT));
    }

    @Test
    public void testGetSearchSortsNullOrderBy() throws Exception {
        final List<SearchSort> sorts = searchSortUtil.getSearchSorts(new QueryImpl(null, null, null));
        assertNull(sorts);
    }

    @Test
    public void testGetSearchSortsEmptyOrderByWhereClauseContainsText() throws Exception {
        final List<SearchSort> sorts = searchSortUtil.getSearchSorts(new QueryImpl(new TerminalClauseImpl("test", Operator.LIKE, "blah")));
        assertThat(sorts, empty());
    }

    @Test
    public void testGetSearchSortsHappyPath() throws Exception {
        final SearchSort mySort = new SearchSort("Test", SortOrder.DESC);
        final List<SearchSort> sorts = searchSortUtil.getSearchSorts(new QueryImpl(null, new OrderByImpl(mySort), null));
        assertThat(sorts, contains(mySort));
    }

    @Test
    public void testGetOrderByClauseNoSorts() throws Exception {
        Map<String, String[]> params = Maps.newHashMap();
        final OrderBy generatedOrderBy = searchSortUtil.getOrderByClause(params);
        assertThat(generatedOrderBy.getSearchSorts(), empty());
    }

    @Test
    public void testGetOrderByClauseUnevenParameters() throws Exception {
        Map<String, String[]> params = Maps.newHashMap();
        final String testField1 = "testField1";
        final String testField2 = "testField2";
        params.put(SearchSortUtil.SORTER_FIELD, new String[]{testField1, testField2});
        params.put(SearchSortUtil.SORTER_ORDER, new String[]{"ASC"});

        when(searchHandlerManager.getJqlClauseNames(testField1)).thenReturn(Collections.singleton(new ClauseNames("testClause1")));
        when(fieldManager.isNavigableField(testField1)).thenReturn(true);

        final OrderBy expectedOrderBy = new OrderByImpl(new SearchSort("testClause1", SortOrder.ASC));
        final OrderBy generatedOrderBy = searchSortUtil.getOrderByClause(params);

        assertEquals(expectedOrderBy, generatedOrderBy);
    }

    @Test
    public void testGetOrderByClauseOneNoMatchingClause() throws Exception {
        Map<String, String[]> params = Maps.newHashMap();
        final String testField1 = "testField1";
        final String testField2 = "testField2";
        params.put(SearchSortUtil.SORTER_FIELD, new String[]{testField1, testField2});
        params.put(SearchSortUtil.SORTER_ORDER, new String[]{"ASC", "DESC"});

        when(searchHandlerManager.getJqlClauseNames(testField1)).thenReturn(Collections.singleton(new ClauseNames("testClause1")));
        when(searchHandlerManager.getJqlClauseNames(testField2)).thenReturn(emptyList());
        when(fieldManager.isNavigableField(testField1)).thenReturn(true);

        final OrderBy expectedOrderBy = new OrderByImpl(new SearchSort("testClause1", SortOrder.ASC));
        final OrderBy generatedOrderBy = searchSortUtil.getOrderByClause(params);

        assertEquals(expectedOrderBy, generatedOrderBy);
    }

    @Test
    public void testGetOrderByClauseOneNotNavigable() throws Exception {
        Map<String, String[]> params = Maps.newHashMap();
        final String testField1 = "testField1";
        final String testField2 = "testField2";
        params.put(SearchSortUtil.SORTER_FIELD, new String[]{testField1, testField2});
        params.put(SearchSortUtil.SORTER_ORDER, new String[]{"ASC", "DESC"});

        when(searchHandlerManager.getJqlClauseNames(testField1)).thenReturn(Collections.singleton(new ClauseNames("testClause1")));
        when(searchHandlerManager.getJqlClauseNames(testField2)).thenReturn(Collections.singleton(new ClauseNames("testClause2")));

        when(fieldManager.isNavigableField(testField1)).thenReturn(true);
        when(fieldManager.isNavigableField(testField2)).thenReturn(false);

        final OrderBy expectedOrderBy = new OrderByImpl(new SearchSort("testClause1", SortOrder.ASC));
        final OrderBy generatedOrderBy = searchSortUtil.getOrderByClause(params);

        assertEquals(expectedOrderBy, generatedOrderBy);
    }

    @Test
    public void testGetOrderByClauseHappyPath() throws Exception {
        Map<String, String[]> params = Maps.newHashMap();
        final String testField1 = "testField1";
        final String testField2 = "testField2";
        params.put(SearchSortUtil.SORTER_FIELD, new String[]{testField1, testField2});
        params.put(SearchSortUtil.SORTER_ORDER, new String[]{"ASC", "DESC"});

        when(searchHandlerManager.getJqlClauseNames(testField1)).thenReturn(Collections.singleton(new ClauseNames("testClause1")));
        when(searchHandlerManager.getJqlClauseNames(testField2)).thenReturn(Collections.singleton(new ClauseNames("testClause2")));

        when(fieldManager.isNavigableField(testField1)).thenReturn(true);
        when(fieldManager.isNavigableField(testField2)).thenReturn(true);

        final OrderBy expectedOrderBy = new OrderByImpl(new SearchSort("testClause1", SortOrder.ASC), new SearchSort("testClause2", SortOrder.DESC));
        final OrderBy generatedOrderBy = searchSortUtil.getOrderByClause(params);

        assertEquals(expectedOrderBy, generatedOrderBy);
    }

    @Test
    public void testConcatSearchSortsHappyPath() throws Exception {
        final String test1 = "test1";
        final String test2 = "test2";
        final String test3 = "test3";
        final String test4 = "test4";
        final SearchSort sort1 = new SearchSort(test1, SortOrder.ASC);
        final SearchSort sort2 = new SearchSort(test2, SortOrder.ASC);
        final SearchSort sort3 = new SearchSort(test3, SortOrder.ASC);
        final SearchSort sort4 = new SearchSort(test4, SortOrder.ASC);
        final Collection<SearchSort> oldSorts = CollectionBuilder.newBuilder(sort1, sort2).asCollection();
        final Collection<SearchSort> newSorts = CollectionBuilder.newBuilder(sort3, sort4).asCollection();

        final List<SearchSort> expectedSorts = CollectionBuilder.newBuilder(sort3, sort4, sort1).asList();
        final List<SearchSort> generatedSorts = searchSortUtil.concatSearchSorts(newSorts, oldSorts, 3);

        assertEquals(expectedSorts, generatedSorts);
    }

    @Test
    public void testConcatSearchSortsNullOldSorts() throws Exception {
        final String test1 = "test1";
        final String test2 = "test2";
        final String test3 = "test3";
        final String test4 = "test4";
        final SearchSort sort1 = new SearchSort(test1, SortOrder.ASC);
        final SearchSort sort2 = new SearchSort(test2, SortOrder.ASC);
        final SearchSort sort3 = new SearchSort(test3, SortOrder.ASC);
        final SearchSort sort4 = new SearchSort(test4, SortOrder.ASC);
        final Collection<SearchSort> oldSorts = null;
        final Collection<SearchSort> newSorts = CollectionBuilder.newBuilder(sort3, sort4, sort1, sort2).asCollection();

        final List<SearchSort> expectedSorts = CollectionBuilder.newBuilder(sort3, sort4, sort1).asList();
        final List<SearchSort> generatedSorts = searchSortUtil.concatSearchSorts(newSorts, oldSorts, 3);

        assertEquals(expectedSorts, generatedSorts);
    }

    @Test
    public void testConcatSearchSortsNoSorts() throws Exception {
        searchSortUtil = new SearchSortUtilImpl(searchHandlerManager, fieldManager);
        assertThat(searchSortUtil.concatSearchSorts(Collections.<SearchSort>emptyList(), Collections.<SearchSort>emptyList(), 3), empty());
    }

    @Test
    public void testConcatSearchSortsNullNewSorts() throws Exception {
        searchSortUtil = new SearchSortUtilImpl(searchHandlerManager, fieldManager);
        exception.expect(IllegalArgumentException.class);
        searchSortUtil.concatSearchSorts(null, Collections.<SearchSort>emptyList(), 3);
    }

    @Test
    public void testMergeSearchSortsHappyPath() throws Exception {
        final String test1 = "test1";
        final String test2 = "test2";
        final String test3 = "test3";
        final String test4 = "test4";
        final SearchSort sort1 = new SearchSort(test1, SortOrder.ASC);
        final SearchSort sort2 = new SearchSort(test2, SortOrder.ASC);
        final SearchSort sort3 = new SearchSort(test3, SortOrder.ASC);
        final SearchSort sort4 = new SearchSort(test4, SortOrder.ASC);
        final Collection<SearchSort> oldSorts = CollectionBuilder.newBuilder(sort1, sort2).asCollection();
        final Collection<SearchSort> newSorts = CollectionBuilder.newBuilder(sort3, sort4).asCollection();

        when(searchHandlerManager.getClauseHandler(null, test3)).thenReturn(Collections.singleton(createClauseHandler(test3)));
        when(searchHandlerManager.getClauseHandler(null, test4)).thenReturn(Collections.singleton(createClauseHandler(test4)));
        when(searchHandlerManager.getClauseHandler(null, test1)).thenReturn(Collections.singleton(createClauseHandler(test1)));

        searchSortUtil = new SearchSortUtilImpl(searchHandlerManager, fieldManager) {
            @Override
            Collection<SearchSort> convertNewSortsToKeepOldSortNames(final ApplicationUser user, final Collection<SearchSort> newSorts, final Collection<SearchSort> oldSorts) {
                return newSorts;
            }
        };

        final List<SearchSort> expectedSorts = CollectionBuilder.newBuilder(sort3, sort4, sort1).asList();
        final List<SearchSort> generatedSorts = searchSortUtil.mergeSearchSorts(null, newSorts, oldSorts, 3);

        assertEquals(expectedSorts, generatedSorts);
    }

    @Test
    public void testMergeSearchSortsResolvesToMultipleFieldsWithOverlap() throws Exception {
        // clause1 maps to [field1, field2]
        // clause2 maps to [field2, field3]
        // clause3 maps to [field3, field4]
        final String clause1 = "clause1";
        final String clause2 = "clause2";
        final String clause3 = "clause3";
        final String field1 = "field1";
        final String field2 = "field2";
        final String field3 = "field3";
        final String field4 = "field4";
        final SearchSort inSort1 = new SearchSort(clause1, SortOrder.ASC);
        final SearchSort inSort2 = new SearchSort(clause2, SortOrder.DESC);
        final SearchSort inSort3 = new SearchSort(clause3, SortOrder.ASC);
        final SearchSort outSort1 = new SearchSort(clause1, SortOrder.ASC);
        final SearchSort outSort2 = new SearchSort(field3, SortOrder.DESC);
        final SearchSort outSort3 = new SearchSort(field4, SortOrder.ASC);
        final Collection<SearchSort> oldSorts = null;
        final Collection<SearchSort> newSorts = CollectionBuilder.newBuilder(inSort1, inSort2, inSort3).asCollection();

        when(searchHandlerManager.getClauseHandler(null, clause1))
                .thenReturn(CollectionBuilder.newBuilder(createClauseHandler(field2, clause1), createClauseHandler(field1, clause1)).asList());

        when(searchHandlerManager.getClauseHandler(null, clause2))
                .thenReturn(CollectionBuilder.newBuilder(createClauseHandler(field3, clause2), createClauseHandler(field2, clause2)).asList());

        when(searchHandlerManager.getClauseHandler(null, clause3))
                .thenReturn(CollectionBuilder.newBuilder(createClauseHandler(field3, clause3), createClauseHandler(field4, clause3)).asList());

        final List<SearchSort> expectedSorts = CollectionBuilder.newBuilder(outSort1, outSort2, outSort3).asList();
        final List<SearchSort> generatedSorts = searchSortUtil.mergeSearchSorts(null, newSorts, oldSorts, Integer.MAX_VALUE);

        assertEquals(expectedSorts, generatedSorts);
    }

    @Test
    public void testMergeSearchSortsTwoCustomFieldsWithSameDisplayNameOneExplicitOneBoth() throws Exception {
        // "NF" is the display name for both custom fields
        // incoming sorts are cf[10005] DESC, NF ASC
        // outgoing should be cf[10005] DESC, cf[10004] ASC
        final String clause1 = "cf[10005]";
        final String clause2 = "cf[10004]";
        final String clause3 = "NF";
        final SearchSort sort1 = new SearchSort(clause1, SortOrder.DESC);
        final SearchSort sort2 = new SearchSort(clause2, SortOrder.ASC);
        final SearchSort sort3 = new SearchSort(clause3, SortOrder.ASC);
        final Collection<SearchSort> oldSorts = Collections.singleton(sort3);
        final Collection<SearchSort> newSorts = Collections.singleton(sort1);

        when(searchHandlerManager.getClauseHandler(null, clause1))
                .thenReturn(CollectionBuilder.newBuilder(createClauseHandler(clause1, clause3)).asList());

        when(searchHandlerManager.getClauseHandler(null, clause3))
                .thenReturn(CollectionBuilder.newBuilder(createClauseHandler(clause1, clause3), createClauseHandler(clause2, clause3)).asList());

        searchSortUtil = new SearchSortUtilImpl(searchHandlerManager, fieldManager) {
            @Override
            Collection<SearchSort> convertNewSortsToKeepOldSortNames(final ApplicationUser user, final Collection<SearchSort> newSorts, final Collection<SearchSort> oldSorts) {
                return newSorts;
            }
        };

        final List<SearchSort> expectedSorts = CollectionBuilder.newBuilder(sort1, sort2).asList();
        final List<SearchSort> generatedSorts = searchSortUtil.mergeSearchSorts(null, newSorts, oldSorts, Integer.MAX_VALUE);

        assertEquals(expectedSorts, generatedSorts);
    }

    @Test
    public void testMergeSearchSortsThreeCustomFieldsTwoWithSameDisplayNameBothUseDisplay() throws Exception {
        // "CSF" is the display name for one custom field [10005]
        // "NF" is the display name for two custom fields [10004, 10003]
        // the display names should not be "split"
        final String clause1 = "CSF";
        final String clause2 = "NF";
        final String primary1 = "cf[10005]";
        final String primary2 = "cf[10004]";
        final String primary3 = "cf[10003]";
        final SearchSort sort1 = new SearchSort(clause1, SortOrder.DESC);
        final SearchSort sort2 = new SearchSort(clause2, SortOrder.ASC);
        final Collection<SearchSort> newSorts = Collections.singleton(sort1);
        final Collection<SearchSort> oldSorts = Collections.singleton(sort2);

        when(searchHandlerManager.getClauseHandler(null, clause1))
                .thenReturn(CollectionBuilder.newBuilder(createClauseHandler(primary1, clause1)).asList());

        when(searchHandlerManager.getClauseHandler(null, clause2))
                .thenReturn(CollectionBuilder.newBuilder(createClauseHandler(primary2, clause2), createClauseHandler(primary3, clause2)).asList());

        searchSortUtil = new SearchSortUtilImpl(searchHandlerManager, fieldManager) {
            @Override
            Collection<SearchSort> convertNewSortsToKeepOldSortNames(final ApplicationUser user, final Collection<SearchSort> newSorts, final Collection<SearchSort> oldSorts) {
                return newSorts;
            }
        };

        final List<SearchSort> expectedSorts = CollectionBuilder.newBuilder(sort1, sort2).asList();
        final List<SearchSort> generatedSorts = searchSortUtil.mergeSearchSorts(null, newSorts, oldSorts, Integer.MAX_VALUE);

        assertEquals(expectedSorts, generatedSorts);
    }

    @Test
    public void testMergeSearchSortsResolvesToSameFieldInNewSorts() throws Exception {
        final String clause1 = "clause1";
        final String clause2 = "clause2";
        final String clause3 = "clause3";
        final String clause4 = "clause4";
        final String field1 = "field1";
        final String field2 = "field2";
        final String field3 = "field3";

        final SearchSort sort1 = new SearchSort(clause1, SortOrder.ASC);
        final SearchSort sort2 = new SearchSort(clause2, SortOrder.ASC);
        final SearchSort sort3 = new SearchSort(clause3, SortOrder.ASC);
        final SearchSort sort4 = new SearchSort(clause4, SortOrder.ASC);
        final Collection<SearchSort> oldSorts = null;
        final Collection<SearchSort> newSorts = CollectionBuilder.newBuilder(sort1, sort2, sort3, sort4).asCollection();

        when(searchHandlerManager.getClauseHandler(null, clause1))
                .thenReturn(CollectionBuilder.newBuilder(createClauseHandler(field1, clause1, clause2)).asList());

        when(searchHandlerManager.getClauseHandler(null, clause2))
                .thenReturn(CollectionBuilder.newBuilder(createClauseHandler(field1, clause1, clause2)).asList());

        when(searchHandlerManager.getClauseHandler(null, clause3))
                .thenReturn(CollectionBuilder.newBuilder(createClauseHandler(field2, clause3)).asList());

        when(searchHandlerManager.getClauseHandler(null, clause4))
                .thenReturn(CollectionBuilder.newBuilder(createClauseHandler(field3, clause4)).asList());

        final List<SearchSort> expectedSorts = CollectionBuilder.newBuilder(sort1, sort3, sort4).asList();
        final List<SearchSort> generatedSorts = searchSortUtil.mergeSearchSorts(null, newSorts, oldSorts, Integer.MAX_VALUE);

        assertEquals(expectedSorts, generatedSorts);
    }

    @Test
    public void testMergeSearchSortsResolvesToSameField() throws Exception {
        final String clause1 = "clause1";
        final String clause2 = "clause2";
        final String clause3 = "clause3";
        final String clause4 = "clause4";
        final String field1 = "field1";
        final String field2 = "field2";
        final String field3 = "field3";

        final SearchSort sort1 = new SearchSort(clause1, SortOrder.ASC);
        final SearchSort sort2 = new SearchSort(clause2, SortOrder.ASC);
        final SearchSort sort3 = new SearchSort(clause3, SortOrder.ASC);
        final SearchSort sort4 = new SearchSort(clause4, SortOrder.ASC);
        final Collection<SearchSort> oldSorts = CollectionBuilder.newBuilder(sort1, sort2).asCollection();
        final Collection<SearchSort> newSorts = CollectionBuilder.newBuilder(sort3, sort4).asCollection();

        when(searchHandlerManager.getClauseHandler(null, clause3))
                .thenReturn(CollectionBuilder.newBuilder(createClauseHandler(field2, clause3)).asList());

        when(searchHandlerManager.getClauseHandler(null, clause4))
                .thenReturn(CollectionBuilder.newBuilder(createClauseHandler(field1, clause1, clause4)).asList());

        when(searchHandlerManager.getClauseHandler(null, clause1))
                .thenReturn(CollectionBuilder.newBuilder(createClauseHandler(field1, clause1, clause4)).asList());

        when(searchHandlerManager.getClauseHandler(null, clause2))
                .thenReturn(CollectionBuilder.newBuilder(createClauseHandler(field3, clause2)).asList());

        searchSortUtil = new SearchSortUtilImpl(searchHandlerManager, fieldManager) {
            @Override
            Collection<SearchSort> convertNewSortsToKeepOldSortNames(final ApplicationUser user, final Collection<SearchSort> newSorts, final Collection<SearchSort> oldSorts) {
                return newSorts;
            }
        };

        final List<SearchSort> expectedSorts = CollectionBuilder.newBuilder(sort3, sort4, sort2).asList();
        final List<SearchSort> generatedSorts = searchSortUtil.mergeSearchSorts(null, newSorts, oldSorts, 3);

        assertEquals(expectedSorts, generatedSorts);
    }

    @Test
    public void testMergeSearchSortsNoSorts() throws Exception {
        searchSortUtil = new SearchSortUtilImpl(searchHandlerManager, fieldManager);
        assertThat(searchSortUtil.mergeSearchSorts(null, Collections.<SearchSort>emptyList(), Collections.<SearchSort>emptyList(), 3), empty());
    }

    @Test
    public void testMergeSearchSortsNullNewSorts() throws Exception {
        searchSortUtil = new SearchSortUtilImpl(searchHandlerManager, fieldManager);
        exception.expect(IllegalArgumentException.class);
        searchSortUtil.mergeSearchSorts(null, null, Collections.<SearchSort>emptyList(), 3);
    }

    @Test
    public void testConvertNewSortsToKeepOldSortNamesOneNewOneOldDoConversion() throws Exception {
        final String clause1 = "clause1";
        final String clause2 = "clause2";

        final Collection<SearchSort> newSorts = Collections.singletonList(new SearchSort(clause1, SortOrder.ASC));
        final Collection<SearchSort> oldSorts = Collections.singletonList(new SearchSort(clause2, SortOrder.DESC));

        when(searchHandlerManager.getClauseHandler(theUser, clause1))
                .thenReturn(Collections.singleton(createClauseHandler(clause1, clause2)));

        when(searchHandlerManager.getClauseHandler(theUser, clause2))
                .thenReturn(Collections.singleton(createClauseHandler(clause1, clause2)));

        final Collection<SearchSort> result = searchSortUtil.convertNewSortsToKeepOldSortNames(theUser, newSorts, oldSorts);
        assertThat(result, contains(new SearchSort(clause2, SortOrder.ASC)));
    }

    @Test
    public void testConvertNewSortsToKeepOldSortNamesOneNewOneOldAlreadySameField() throws Exception {
        final String clause1 = "clause1";
        final String clause2 = "clause2";

        final Collection<SearchSort> newSorts = Collections.singletonList(new SearchSort(clause1, SortOrder.ASC));
        final Collection<SearchSort> oldSorts = Collections.singletonList(new SearchSort(clause1, SortOrder.DESC));

        when(searchHandlerManager.getClauseHandler(theUser, clause1))
                .thenReturn(Collections.singleton(createClauseHandler(clause1, clause2)));

        final Collection<SearchSort> result = searchSortUtil.convertNewSortsToKeepOldSortNames(theUser, newSorts, oldSorts);
        assertEquals(newSorts, result);
    }

    @Test
    public void testConvertNewSortsToKeepOldSortNamesOneNewOneOldDifferentClauseNames() throws Exception {
        final String clause1 = "clause1";
        final String clause2 = "clause2";

        final Collection<SearchSort> newSorts = Collections.singletonList(new SearchSort(clause1, SortOrder.ASC));
        final Collection<SearchSort> oldSorts = Collections.singletonList(new SearchSort(clause2, SortOrder.DESC));

        when(searchHandlerManager.getClauseHandler(theUser, clause1))
                .thenReturn(Collections.singleton(createClauseHandler(clause1)));

        when(searchHandlerManager.getClauseHandler(theUser, clause2))
                .thenReturn(Collections.singleton(createClauseHandler(clause2)));

        final Collection<SearchSort> result = searchSortUtil.convertNewSortsToKeepOldSortNames(theUser, newSorts, oldSorts);
        assertEquals(newSorts, result);
    }

    @Test
    public void testConvertNewSortsToKeepOldSortNamesNewReturnsMultipleClauseHandlers() throws Exception {
        final String clause1 = "clause1";
        final String clause2 = "clause2";

        final Collection<SearchSort> newSorts = Collections.singletonList(new SearchSort(clause1, SortOrder.ASC));
        final Collection<SearchSort> oldSorts = Collections.singletonList(new SearchSort(clause2, SortOrder.DESC));

        when(searchHandlerManager.getClauseHandler(theUser, clause1))
                .thenReturn(CollectionBuilder.newBuilder(createClauseHandler(clause1, clause2), createClauseHandler(clause1, clause2)).asList());

        final Collection<SearchSort> result = searchSortUtil.convertNewSortsToKeepOldSortNames(theUser, newSorts, oldSorts);
        assertEquals(newSorts, result);
    }

    @Test
    public void testConvertNewSortsToKeepOldSortNamesOldReturnsMultipleClauseHandlers() throws Exception {
        final String clause1 = "clause1";
        final String clause2 = "clause2";

        final Collection<SearchSort> newSorts = Collections.singletonList(new SearchSort(clause1, SortOrder.ASC));
        final Collection<SearchSort> oldSorts = Collections.singletonList(new SearchSort(clause2, SortOrder.DESC));

        when(searchHandlerManager.getClauseHandler(theUser, clause1))
                .thenReturn(CollectionBuilder.newBuilder(createClauseHandler(clause1, clause2)).asList());

        when(searchHandlerManager.getClauseHandler(theUser, clause2))
                .thenReturn(CollectionBuilder.newBuilder(createClauseHandler(clause1, clause2), createClauseHandler(clause1, clause2)).asList());

        final Collection<SearchSort> result = searchSortUtil.convertNewSortsToKeepOldSortNames(theUser, newSorts, oldSorts);
        assertEquals(newSorts, result);
    }

    private static ClauseHandler createClauseHandler(final String primaryName, final String... names) {
        return new ClauseHandler() {
            public ClauseInformation getInformation() {
                return new ClauseInformation() {
                    public ClauseNames getJqlClauseNames() {
                        return new ClauseNames(primaryName, names);
                    }

                    public String getIndexField() {
                        throw new UnsupportedOperationException();
                    }

                    public String getFieldId() {
                        throw new UnsupportedOperationException();
                    }

                    public Set<Operator> getSupportedOperators() {
                        return OperatorClasses.TEXT_OPERATORS;
                    }

                    public JiraDataType getDataType() {
                        return JiraDataTypes.TEXT;
                    }
                };
            }

            public ClauseQueryFactory getFactory() {
                throw new UnsupportedOperationException();
            }

            public ClauseValidator getValidator() {
                throw new UnsupportedOperationException();
            }

            public ClausePermissionHandler getPermissionHandler() {
                throw new UnsupportedOperationException();
            }

            public ClauseContextFactory getClauseContextFactory() {
                throw new UnsupportedOperationException();
            }
        };
    }


    @Test
    public void testGetSearchSortDescriptionsNoSorts() throws Exception {
        final Field mockField = createMockNavigableField("key");
        when(fieldManager.getField("key")).thenReturn(mockField);

        SearchRequest sr = new SearchRequest();
        searchSortUtil = new SearchSortUtilImpl(new MockSearchHandlerManager(), fieldManager);
        final List<String> list = searchSortUtil.getSearchSortDescriptions(sr, i18n, null);
        assertThat(list, hasSize(1));
    }

    @Test
    public void testGetSearchSortDescriptionsOneNonNavigableSort() throws Exception {
        final String fieldId = "goodField";
        SearchRequest sr = new SearchRequest();
        sr.setQuery(new QueryImpl(null, new OrderByImpl(new SearchSort(fieldId, SortOrder.DESC)), null));

        final Field mockField = createMockField(fieldId);
        when(fieldManager.getField(fieldId)).thenReturn(mockField);

        searchSortUtil = new SearchSortUtilImpl(new MockSearchHandlerManager(), fieldManager);
        final List<String> list = searchSortUtil.getSearchSortDescriptions(sr, i18n, null);
        assertThat(list, contains("goodField"));
    }

    @Test
    public void testGetSearchSortDescriptionsOneGoodSortDesc() throws Exception {
        final String fieldId = "goodField";
        SearchRequest sr = new SearchRequest();
        sr.setQuery(new QueryImpl(null, new OrderByImpl(new SearchSort(fieldId, SortOrder.DESC)), null));

        final Field mockField = createMockNavigableField(fieldId);
        when(fieldManager.getField(fieldId)).thenReturn(mockField);

        searchSortUtil = new SearchSortUtilImpl(new MockSearchHandlerManager(), fieldManager);
        final List<String> list = searchSortUtil.getSearchSortDescriptions(sr, i18n, null);
        assertThat(list, contains("goodField navigator.hidden.sortby.descending"));
    }

    @Test
    public void testGetSearchSortDescriptionsOneGoodSortAsc() throws Exception {
        final String fieldId = "goodField";
        SearchRequest sr = new SearchRequest();
        sr.setQuery(new QueryImpl(null, new OrderByImpl(new SearchSort(fieldId, SortOrder.ASC)), null));

        final Field mockField = createMockNavigableField(fieldId);
        when(fieldManager.getField(fieldId)).thenReturn(mockField);

        searchSortUtil = new SearchSortUtilImpl(new MockSearchHandlerManager(), fieldManager);
        final List<String> list = searchSortUtil.getSearchSortDescriptions(sr, i18n, null);
        assertThat(list, contains("goodField navigator.hidden.sortby.ascending"));
    }

    @Test
    public void testGetSearchSortDescriptionsOneBadSort() throws Exception {
        final Field mockField = null;
        final String fieldId = "badField";
        SearchRequest sr = new SearchRequest();
        sr.setQuery(new QueryImpl(null, new OrderByImpl(new SearchSort(fieldId, SortOrder.DESC)), null));

        when(fieldManager.getField(fieldId)).thenReturn(mockField);

        searchSortUtil = new SearchSortUtilImpl(new MockSearchHandlerManager(), fieldManager);
        final List<String> list = searchSortUtil.getSearchSortDescriptions(sr, i18n, null);
        assertThat(list, empty());
    }

    @Test
    public void testGetSearchSortDescriptionsTwoGoodSorts() throws Exception {
        final String fieldId1 = "goodField1";
        final String fieldId2 = "goodField2";
        SearchRequest sr = new SearchRequest();
        sr.setQuery(new QueryImpl(null, new OrderByImpl(new SearchSort(fieldId2, SortOrder.DESC), new SearchSort(fieldId1, SortOrder.ASC)), null));

        final Field mockField1 = createMockNavigableField(fieldId1);
        final Field mockField2 = createMockNavigableField(fieldId2);
        when(fieldManager.getField(fieldId2)).thenReturn(mockField2);
        when(fieldManager.getField(fieldId1)).thenReturn(mockField1);

        searchSortUtil = new SearchSortUtilImpl(new MockSearchHandlerManager(), fieldManager);
        final List<String> list = searchSortUtil.getSearchSortDescriptions(sr, i18n, null);
        assertThat(list, contains("goodField2 navigator.hidden.sortby.descending, navigator.hidden.sortby.then", "goodField1 navigator.hidden.sortby.ascending"));
    }

    @Test
    public void testGetSearchSortDescriptionsOneGoodSortOneBadSort() throws Exception {
        final String fieldId1 = "goodField1";
        final String fieldId2 = "goodField2";
        SearchRequest sr = new SearchRequest();
        sr.setQuery(new QueryImpl(null, new OrderByImpl(new SearchSort("DESC", fieldId2), new SearchSort("ASC", fieldId1)), null));

        final Field mockField1 = createMockNavigableField(fieldId1);
        final Field mockField2 = null;

        when(fieldManager.getField(fieldId2)).thenReturn(mockField2);
        when(fieldManager.getField(fieldId1)).thenReturn(mockField1);

        searchSortUtil = new SearchSortUtilImpl(new MockSearchHandlerManager(), fieldManager);
        final List<String> list = searchSortUtil.getSearchSortDescriptions(sr, i18n, null);
        assertThat(list, contains("goodField1 navigator.hidden.sortby.ascending"));
    }

    @Test
    public void testGetSearchSortDescriptionsOneGoodOneBadOneGoodSort() throws Exception {
        final String fieldId1 = "goodField1";
        final String fieldId2 = "goodField2";
        final String fieldId3 = "goodField3";
        SearchRequest sr = new SearchRequest();
        sr.setQuery(new QueryImpl(null, new OrderByImpl(new SearchSort("DESC", fieldId3), new SearchSort("DESC", fieldId2), new SearchSort("ASC", fieldId1)), null));

        final Field mockField1 = createMockNavigableField(fieldId1);
        final Field mockField2 = null;
        final Field mockField3 = createMockNavigableField(fieldId3);

        when(fieldManager.getField(fieldId3)).thenReturn(mockField3);
        when(fieldManager.getField(fieldId2)).thenReturn(mockField2);
        when(fieldManager.getField(fieldId1)).thenReturn(mockField1);

        searchSortUtil = new SearchSortUtilImpl(new MockSearchHandlerManager(), fieldManager);
        final List<String> list = searchSortUtil.getSearchSortDescriptions(sr, i18n, null);
        assertThat(list, contains("goodField3 navigator.hidden.sortby.descending, navigator.hidden.sortby.then", "goodField1 navigator.hidden.sortby.ascending"));
    }

    @Test
    public void testGetSearchSortDescriptionsOneBadTwoGoodSort() throws Exception {
        final String fieldId1 = "goodField1";
        final String fieldId2 = "goodField2";
        final String fieldId3 = "goodField3";
        SearchRequest sr = new SearchRequest();
        sr.setQuery(new QueryImpl(null, new OrderByImpl(new SearchSort("DESC", fieldId3), new SearchSort("DESC", fieldId2), new SearchSort("ASC", fieldId1)), null));

        final Field mockField1 = null;
        final Field mockField2 = createMockNavigableField(fieldId2);
        final Field mockField3 = createMockNavigableField(fieldId3);

        when(fieldManager.getField(fieldId3)).thenReturn(mockField3);
        when(fieldManager.getField(fieldId2)).thenReturn(mockField2);
        when(fieldManager.getField(fieldId1)).thenReturn(mockField1);

        searchSortUtil = new SearchSortUtilImpl(new MockSearchHandlerManager(), fieldManager);
        final List<String> list = searchSortUtil.getSearchSortDescriptions(sr, i18n, null);
        assertThat(list, contains("goodField3 navigator.hidden.sortby.descending, navigator.hidden.sortby.then", "goodField2 navigator.hidden.sortby.descending"));
    }

    private static class MockSearchHandlerManager implements SearchHandlerManager {
        @Override
        public Collection<IssueSearcher<?>> getSearchers(final ApplicationUser searcher, final SearchContext context) {
            return null;
        }

        @Override
        public Collection<IssueSearcher<?>> getAllSearchers() {
            return null;
        }

        @Override
        public Collection<SearcherGroup> getSearcherGroups(final SearchContext searchContext) {
            return null;
        }

        @Override
        public Collection<SearcherGroup> getSearcherGroups() {
            return null;
        }

        @Override
        public IssueSearcher<?> getSearcher(final String id) {
            return null;
        }

        @Override
        public void refresh() {
        }

        @Override
        public Collection<ClauseHandler> getClauseHandler(final ApplicationUser user, final String jqlClauseName) {
            return null;
        }

        @Override
        public Collection<ClauseHandler> getClauseHandler(final String jqlClauseName) {
            return null;
        }

        @Override
        public Collection<ClauseNames> getJqlClauseNames(final String fieldId) {
            return null;
        }

        @Override
        public Collection<String> getFieldIds(final ApplicationUser searcher, final String jqlClauseName) {
            return Collections.singleton(jqlClauseName);
        }

        @Override
        public Collection<String> getFieldIds(final String jqlClauseName) {
            return Collections.singleton(jqlClauseName);
        }

        @Override
        public Collection<ClauseNames> getVisibleJqlClauseNames(final ApplicationUser searcher) {
            return null;
        }

        @Override
        public Collection<ClauseHandler> getVisibleClauseHandlers(final ApplicationUser searcher) {
            return null;
        }

        @Override
        public Collection<IssueSearcher<?>> getSearchersByClauseName(final ApplicationUser user, final String jqlClauseName, final SearchContext searchContext) {
            return null;
        }

        @Override
        public Collection<IssueSearcher<?>> getSearchersByClauseName(final ApplicationUser user, final String jqlClauseName) {
            return null;
        }
    }

    private NavigableField createMockNavigableField(final String nameKey) {
        NavigableField field = mock(NavigableField.class);
        when(field.getNameKey()).thenReturn(nameKey);
        return field;
    }

    private Field createMockField(final String nameKey) {
        Field field = mock(Field.class);
        when(field.getNameKey()).thenReturn(nameKey);
        return field;
    }
}
