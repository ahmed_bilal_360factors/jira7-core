package com.atlassian.jira.task;

import org.junit.Test;
import org.mockito.InOrder;

import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.mock;

public class TestStepTaskProgressSink {
    private static final String SUB_TASK_A = "SubTaskA";
    private static final String MESSAGE_A = "MessageA";

    @Test(expected = IllegalArgumentException.class)
    public void constructorThrowsExceptionOnNullSink() {
        new StepTaskProgressSink(10, 100, 90, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructorThrowsExceptionOnStartProgressGreaterThanEnd() {
        new StepTaskProgressSink(100, 10, 90, TaskProgressSink.NULL_SINK);
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructorThrowsExceptionOnNegativeNumberOfActions() {
        new StepTaskProgressSink(10, 100, -9, TaskProgressSink.NULL_SINK);
    }

    @Test
    public void makeProgressWithDummyTaskDelegatesToSink() {
        TaskProgressSink mockSink = mock(TaskProgressSink.class);

        //check the simple percentage operations.
        TaskProgressSink actionSink = new StepTaskProgressSink(0, 100, 3, mockSink);
        actionSink.makeProgress(0, SUB_TASK_A, MESSAGE_A);
        actionSink.makeProgress(1, null, MESSAGE_A);
        actionSink.makeProgress(2, null, null);
        actionSink.makeProgress(3, SUB_TASK_A, null);
        actionSink.makeProgress(Long.MIN_VALUE, null, null);
        actionSink.makeProgress(Long.MAX_VALUE, null, null);

        InOrder inOrder = inOrder(mockSink);
        inOrder.verify(mockSink).makeProgress(0, SUB_TASK_A, MESSAGE_A);
        inOrder.verify(mockSink).makeProgress(33, null, MESSAGE_A);
        inOrder.verify(mockSink).makeProgress(67, null, null);
        inOrder.verify(mockSink).makeProgress(100, SUB_TASK_A, null);
        inOrder.verify(mockSink).makeProgress(0, null, null);
        inOrder.verify(mockSink).makeProgress(100, null, null);
        inOrder.verifyNoMoreInteractions();
    }

    @Test
    public void makeProgressWithAnotherDummyTaskDelegatesToSink() {
        TaskProgressSink mockSink = mock(TaskProgressSink.class);

        TaskProgressSink actionSink = new StepTaskProgressSink(-10, -5, 7, mockSink);
        actionSink.makeProgress(0, SUB_TASK_A, MESSAGE_A);
        actionSink.makeProgress(5, null, MESSAGE_A);
        actionSink.makeProgress(7, null, null);
        actionSink.makeProgress(3, SUB_TASK_A, null);

        InOrder inOrder = inOrder(mockSink);
        inOrder.verify(mockSink).makeProgress(-10, SUB_TASK_A, MESSAGE_A);
        inOrder.verify(mockSink).makeProgress(-6, null, MESSAGE_A);
        inOrder.verify(mockSink).makeProgress(-5, null, null);
        inOrder.verify(mockSink).makeProgress(-8, SUB_TASK_A, null);
        inOrder.verifyNoMoreInteractions();
    }

}
