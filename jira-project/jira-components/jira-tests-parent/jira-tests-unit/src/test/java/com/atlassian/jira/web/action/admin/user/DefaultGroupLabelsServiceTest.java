package com.atlassian.jira.web.action.admin.user;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.collectors.CollectorsUtil;
import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.jira.application.ApplicationRole;
import com.atlassian.jira.application.ApplicationRoleManager;
import com.atlassian.jira.bc.group.GroupAccessLabelsManager;
import com.atlassian.jira.mock.i18n.MockI18nHelper;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.web.component.admin.group.DefaultGroupLabelsService;
import com.atlassian.jira.web.component.admin.group.GroupLabelView;
import com.atlassian.jira.web.component.admin.group.GroupLabelView.LabelType;
import org.hamcrest.FeatureMatcher;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.MethodRule;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import java.util.Arrays;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.google.common.collect.ImmutableSet.of;
import static org.hamcrest.Matchers.contains;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class DefaultGroupLabelsServiceTest {
    @Rule
    public final MethodRule initMockito = MockitoJUnit.rule();
    @Rule
    public final ExpectedException exception = ExpectedException.none();

    private static final Optional<Long> DIRECTORY = Optional.of(126L);

    @Mock
    private GroupAccessLabelsManager labelsManager;
    @Mock
    private JiraAuthenticationContext authenticationContext;
    @Mock
    private ApplicationRoleManager roleManager;
    @Mock
    private Group group;

    @InjectMocks
    private DefaultGroupLabelsService groupLabels;

    @Before
    public void setUp() {
        when(authenticationContext.getI18nHelper()).thenReturn(new MockI18nHelper());
    }

    @Test
    public void shouldReturnEmptyWhenNoAccessAndNoAdmin() {
        setupLabels(false, of());

        assertThat(groupLabels.getGroupLabels(group, DIRECTORY), Matchers.emptyIterable());
    }

    @Test
    public void shouldContainAdminLabelWhenUserIsAdmin() {
        setupLabels(true, of());

        assertThat(groupLabels.getGroupLabels(group, DIRECTORY), contains(admin()));
    }

    @Test
    public void shouldContainSingleAccessLabelWhenOnlyOneApplicationIsAccessible() {
        setupLabels(false, of(mockRole("myrole", "My Role")));

        assertThat(groupLabels.getGroupLabels(group, DIRECTORY), contains(singleApp("My Role")));
    }

    @Test
    public void shouldContainMultipleAccessLabelWhenMultipleApplicationsAreAccessible() {
        final String[] roleKeyNames = {"rolea", "roleb", "rolec"};
        setupLabels(false, roles(roleKeyNames));

        assertThat(groupLabels.getGroupLabels(group, DIRECTORY), contains(multiApp(roleKeyNames)));
    }

    @SuppressWarnings("unchecked")
    @Test
    public void shouldReturnBothValuesWhenMultiAppAndAdmin() {
        final String[] roleKeyNames = {"rolea", "roleb"};
        setupLabels(true, roles(roleKeyNames));

        assertThat(groupLabels.getGroupLabels(group, DIRECTORY), contains(admin(), multiApp(roleKeyNames)));
    }

    @Test
    public void shouldReturnJustManyAppsWhenMoreThanFiveRolesGiven() {
        setupLabels(false, roles("rolea", "roleb", "rolec", "roled", "rolee", "rolef"));

        assertThat(groupLabels.getGroupLabels(group, DIRECTORY), contains(multiApp()));
    }

    @SuppressWarnings("unchecked")
    @Test
    public void shouldReturnBothValuesWhenSingleAppAndAdmin() {
        final ApplicationRole role = mockRole("myapp", "myapp");
        setupLabels(true, of(role));

        assertThat(groupLabels.getGroupLabels(group, DIRECTORY), contains(admin(), singleApp("myapp")));
    }

    @Test
    public void shouldApplicationNameBeSecure() {
        setupLabels(false, of(mockRole("scriptalert", "<script>alert(1);</script>")));

        assertThat(groupLabels.getGroupLabels(group, DIRECTORY), contains(singleApp("<script>alert(1);</script>", "&lt;script&gt;alert(1);&lt;/script&gt;")));
    }

    private Set<ApplicationRole> roles(String... roleKeyNames) {
        return Stream.of(roleKeyNames).map(roleKeyName -> this.mockRole(roleKeyName, roleKeyName))
                .collect(CollectorsUtil.toImmutableSet());
    }

    private ApplicationRole mockRole(final String key, final String name) {
        ApplicationRole role = mock(ApplicationRole.class);
        when(role.getKey()).thenReturn(ApplicationKey.valueOf(key));
        when(role.getName()).thenReturn(name);
        return role;
    }

    private void setupLabels(boolean isAdmin, final Set<ApplicationRole> roles) {
        when(labelsManager.getForGroup(group, DIRECTORY)).thenReturn(new GroupAccessLabelsManager.GroupAccessLabels(isAdmin, roles));
    }

    private Matcher<GroupLabelView> multiApp() {
        return labelWithTextTitleAndType("admin.viewgroup.labels.application.access.multiple.text", "admin.viewgroup.labels.application.access.multiple.description", LabelType.MULTIPLE);
    }

    private Matcher<GroupLabelView> multiApp(String... names) {
        final String appNamesArgs = Arrays.stream(names)
                .map(name -> " [<strong>" + name + "</strong>]")
                .collect(Collectors.joining());
        return labelWithTextAndTitle("admin.viewgroup.labels.application.access.multiple.text",
                String.format("admin.viewgroup.labels.application.access.description.%d.app%s", names.length, appNamesArgs));
    }

    private Matcher<GroupLabelView> singleApp(String name, String xssSecureName) {
        return labelWithTextTitleAndType(name, String.format("admin.viewgroup.labels.application.access.description.1.app [<strong>%s</strong>]", xssSecureName), LabelType.SINGLE);
    }

    private Matcher<GroupLabelView> singleApp(String name) {
        return singleApp(name, name);
    }

    private static Matcher<GroupLabelView> admin() {
        return labelWithTextTitleAndType("admin.viewgroup.labels.admin.text", "admin.viewgroup.labels.admin.description", LabelType.ADMIN);
    }


    private static Matcher<GroupLabelView> labelWithTextAndTitle(String text, String title) {
        return Matchers.allOf(withText(text), withTitle(title));
    }

    private static Matcher<GroupLabelView> labelWithTextTitleAndType(String text, String title, LabelType type) {
        return Matchers.allOf(withText(text), withTitle(title), withType(type));
    }

    private static Matcher<GroupLabelView> withText(String text) {
        return new FeatureMatcher<GroupLabelView, String>(Matchers.equalTo(text), "Label with text", "text") {
            @Override
            protected String featureValueOf(final GroupLabelView actual) {
                return actual.getText();
            }
        };
    }

    private static Matcher<GroupLabelView> withTitle(String title) {
        return new FeatureMatcher<GroupLabelView, String>(Matchers.equalTo(title), "Label with title", "title") {
            @Override
            protected String featureValueOf(final GroupLabelView actual) {
                return actual.getTitle();
            }
        };
    }

    private static Matcher<GroupLabelView> withType(LabelType type) {
        return new FeatureMatcher<GroupLabelView, LabelType>(Matchers.equalTo(type), "Admin label", "isAdmin") {
            @Override
            protected LabelType featureValueOf(final GroupLabelView actual) {
                return actual.getType();
            }
        };
    }
}