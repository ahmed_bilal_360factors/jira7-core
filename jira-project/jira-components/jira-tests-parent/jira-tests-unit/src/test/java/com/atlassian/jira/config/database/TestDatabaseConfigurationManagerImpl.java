package com.atlassian.jira.config.database;

import com.atlassian.jira.config.util.JiraHome;
import com.google.common.collect.Queues;
import org.hamcrest.Matchers;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.ofbiz.core.entity.config.JdbcDatasourceInfo;

import java.util.Queue;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Unit test for {@link DatabaseConfigurationManagerImpl}.
 *
 * @since v4.4
 */
public class TestDatabaseConfigurationManagerImpl {
    @Rule
    public MockitoRule mocks = MockitoJUnit.rule();

    @Mock
    DatabaseConfigurationLoader configurationLoader;


    @Test
    public void testIsDatabaseSetupGetDatabaseConfigurationFails() {
        when(configurationLoader.loadDatabaseConfiguration())
                .thenThrow(RuntimeException.class);
        final DatabaseConfigurationManagerImpl dcmi = new DatabaseConfigurationManagerImpl(null, configurationLoader, null);

        assertFalse(dcmi.isDatabaseSetup());
    }

    @Test
    public void testIsDatabaseSetupHappy() {
        final DatabaseConfig databaseConfig = new DatabaseConfig("fooDb", "skimar", new JndiDatasource("jndi;yo"));
        when(configurationLoader.loadDatabaseConfiguration()).thenReturn(databaseConfig);
        final DatabaseConfigurationManagerImpl dcmi = new DatabaseConfigurationManagerImpl(null, configurationLoader, null);

        assertTrue(dcmi.isDatabaseSetup());
    }

    /**
     * Tests the negative cache of whether the database is setup by first finding the database not setup and then
     * calling again without first calling setDatabaseConfig() which would be the only way to invalidate that cache.
     */
    @Test
    public void testUnconfiguredCache() {
        final DatabaseConfig databaseConfig = new DatabaseConfig("fooDb", "skimar", new JndiDatasource("jndi;yo"));
        when(configurationLoader.configExists())
                .thenReturn(false);
        when(configurationLoader.loadDatabaseConfiguration())
                .thenThrow(RuntimeException.class)
                .thenReturn(databaseConfig);
        final DatabaseConfigurationManagerImpl dcmi = new DatabaseConfigurationManagerImpl(null, configurationLoader, null);
        // first time around it's not setup
        assertFalse(dcmi.isDatabaseSetup());
        // now make it so that database config is actually setup, but without calling setDatabaseConfig()
        // the negative cache should answer here
        assertFalse(dcmi.isDatabaseSetup());
    }

    @Test
    public void testCreateInternalConfig() {
        final JiraHome jiraHome = mock(JiraHome.class);
        when(jiraHome.getHomePath()).thenReturn("noPlaceLike/Home");

        final DatabaseConfigurationManagerImpl dcmi = new DatabaseConfigurationManagerImpl(jiraHome, null, null);
        final DatabaseConfig internalConfig = dcmi.getInternalDatabaseConfiguration();

        assertEquals("defaultDS", internalConfig.getDatasourceName());
        assertEquals("default", internalConfig.getDelegatorName());
        assertEquals("h2", internalConfig.getDatabaseType());
        assertEquals("PUBLIC", internalConfig.getSchemaName());
        final JdbcDatasourceInfo jdbcDatasource = internalConfig.getDatasourceInfo().getJdbcDatasource();
        assertEquals("sa", jdbcDatasource.getUsername());
        assertEquals("", jdbcDatasource.getPassword());
        assertEquals("jdbc:h2:file:noPlaceLike/Home/database/h2db", jdbcDatasource.getUri());
        assertEquals("org.h2.Driver", jdbcDatasource.getDriverClassName());
    }

    @Test
    public void testSetDatabaseConfiguration() {
        final DatabaseConfig databaseConfig = new DatabaseConfig("fooDb", "skimar", new JndiDatasource("jndi;yo"));

        final DatabaseConfigurationManagerImpl dcmi = new DatabaseConfigurationManagerImpl(null, configurationLoader, null);
        dcmi.setDatabaseConfiguration(databaseConfig);

        verify(configurationLoader).saveDatabaseConfiguration(databaseConfig);
    }

    @Test
    public void testGetDatabaseConfiguration() {
        final DatabaseConfig databaseConfig = new DatabaseConfig("fooDb", "skimar", new JndiDatasource("jndi;yo"));
        when(configurationLoader.loadDatabaseConfiguration()).thenReturn(databaseConfig);

        final DatabaseConfigurationManagerImpl dcmi = new DatabaseConfigurationManagerImpl(null, configurationLoader, null);

        final DatabaseConfig returned = dcmi.getDatabaseConfiguration();
        final DatabaseConfig returned2 = dcmi.getDatabaseConfiguration();

        assertEquals(databaseConfig.getDatabaseType(), returned.getDatabaseType());
        assertEquals(databaseConfig.getDatabaseType(), returned2.getDatabaseType());

        verify(configurationLoader, times(1)).loadDatabaseConfiguration();
    }

    @Test
    public void testDoNow() {
        final DatabaseConfig databaseConfig = new DatabaseConfig("fooDb", "skimar", new JndiDatasource("jndi;yo"));
        when(configurationLoader.loadDatabaseConfiguration()).thenReturn(databaseConfig);

        final DatabaseConfigurationManagerImpl dcmi = new DatabaseConfigurationManagerImpl(null, configurationLoader, null) {
            @Override
            void configureOfbiz(final DatabaseConfig databaseConfig) {
                // do nothing, we are just erasing the behaviour that links out to static ofbiz land
                // can't remove this one. tooo much static :(
            }
        };

        final Runnable runMe = mock(Runnable.class);
        final Queue<Runnable> queue = Queues.newLinkedBlockingQueue();

        dcmi.doNowOrEnqueue("queueDesc", queue, runMe, "taskDesc");

        verify(runMe, times(1)).run();
        assertThat(queue, Matchers.empty());
    }


    @Test
    public void testEnqueue() {
        when(configurationLoader.loadDatabaseConfiguration()).thenThrow(RuntimeException.class);

        final DatabaseConfigurationManagerImpl dcmi = new DatabaseConfigurationManagerImpl(null, configurationLoader, null);

        final Runnable runMe = mock(Runnable.class);

        final Queue<Runnable> queue = Queues.newLinkedBlockingQueue();
        dcmi.doNowOrEnqueue("queueDesc", queue, runMe, "taskDesc");

        verify(runMe, times(0)).run();
        assertThat(queue, Matchers.contains(runMe));
    }

}
