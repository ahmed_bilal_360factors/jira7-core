package com.atlassian.jira.issue.statistics;

import com.atlassian.jira.action.issue.customfields.option.MockOption;
import com.atlassian.jira.issue.customfields.converters.SelectConverterImpl;
import com.atlassian.jira.issue.customfields.option.Option;
import com.atlassian.jira.issue.customfields.searchers.transformer.CustomFieldInputHelper;
import com.atlassian.jira.issue.customfields.statistics.GroupPickerStatisticsMapper;
import com.atlassian.jira.issue.customfields.statistics.ProjectSelectStatisticsMapper;
import com.atlassian.jira.issue.customfields.statistics.SelectStatisticsMapper;
import com.atlassian.jira.issue.customfields.statistics.TextStatisticsMapper;
import com.atlassian.jira.issue.customfields.statistics.UserPickerStatisticsMapper;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.search.ClauseNames;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.junit.rules.MockComponentContainer;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.query.QueryImpl;
import com.atlassian.query.clause.TerminalClauseImpl;
import com.atlassian.query.operand.EmptyOperand;
import com.atlassian.query.operator.Operator;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestJqlSelectStatisticsMapper {
    public static final String CF_10001 = "cf[10001]";
    public static final String CUSTOMFIELD_10001 = "customfield_10001";
    public static final String NUMBER_CF = "Number CF";
    private ClauseNames clauseNames = new ClauseNames(CF_10001);

    @Rule
    public final MethodRule initMockito = MockitoJUnit.rule();

    @Rule
    public MockComponentContainer container = new MockComponentContainer(this);

    @Mock
    private JiraAuthenticationContext authenticationContext;
    @Mock
    private CustomFieldInputHelper customFieldInputHelper;
    private ApplicationUser user;

    @Before
    public void setUp() throws Exception {
        user = new MockApplicationUser("fred");
        when(authenticationContext.getUser()).thenReturn(user);
    }

    @Test
    public void getSearchUrlSuffixNullSearchRequest() throws Exception {
        final CustomField customField = mock(CustomField.class);
        when(customField.getClauseNames()).thenReturn(new ClauseNames(CUSTOMFIELD_10001));
        when(customField.getName()).thenReturn(NUMBER_CF);
        when(customFieldInputHelper.getUniqueClauseName(user, CUSTOMFIELD_10001, NUMBER_CF)).thenReturn(CF_10001);

        SelectStatisticsMapper mapper = new SelectStatisticsMapper(customField, new SelectConverterImpl(null), authenticationContext, customFieldInputHelper);
        assertNull(mapper.getSearchUrlSuffix(null, null));
    }

    @Test
    public void getSearchUrlSuffixNullValue() throws Exception {
        final CustomField customField = mock(CustomField.class);
        when(customField.getClauseNames()).thenReturn(new ClauseNames(CF_10001));
        when(customField.getName()).thenReturn(NUMBER_CF);
        final SearchRequest searchRequest = mock(SearchRequest.class);
        when(customFieldInputHelper.getUniqueClauseName(user, CF_10001, NUMBER_CF)).thenReturn(CF_10001);
        when(searchRequest.getQuery()).thenReturn(new QueryImpl());

        SelectStatisticsMapper mapper = new SelectStatisticsMapper(customField, new SelectConverterImpl(null), authenticationContext, customFieldInputHelper);
        final SearchRequest urlSuffix = mapper.getSearchUrlSuffix(null, searchRequest);
        assertEquals(new TerminalClauseImpl(clauseNames.getPrimaryName(), Operator.IS, EmptyOperand.EMPTY), urlSuffix.getQuery().getWhereClause());
    }

    @Test
    public void getSearchUrlSuffix() throws Exception {
        final CustomField customField = mock(CustomField.class);
        when(customField.getClauseNames()).thenReturn(new ClauseNames(CF_10001));
        when(customField.getName()).thenReturn(NUMBER_CF);

        final SearchRequest searchRequest = mock(SearchRequest.class);
        when(customFieldInputHelper.getUniqueClauseName(user, CF_10001, NUMBER_CF)).thenReturn(CF_10001);

        when(searchRequest.getQuery()).thenReturn(new QueryImpl());

        SelectStatisticsMapper mapper = new SelectStatisticsMapper(customField, new SelectConverterImpl(null), authenticationContext, customFieldInputHelper);
        Option option = new MockOption(null, null, 1L, "value", null, 1L);
        final SearchRequest urlSuffix = mapper.getSearchUrlSuffix(option, searchRequest);
        assertEquals(new TerminalClauseImpl(clauseNames.getPrimaryName(), Operator.EQUALS, "value"), urlSuffix.getQuery().getWhereClause());
    }

    @Test
    public void equalsMethodContract() {
        CustomField mockCustomField = mock(CustomField.class);
        String cfId = CUSTOMFIELD_10001;
        when(mockCustomField.getId()).thenReturn(cfId);
        when(mockCustomField.getClauseNames()).thenReturn(new ClauseNames(CF_10001));

        SelectStatisticsMapper mapper = new SelectStatisticsMapper(mockCustomField, new SelectConverterImpl(null), authenticationContext, customFieldInputHelper);

        // identity test
        assertTrue(mapper.equals(mapper));
        assertEquals(mapper.hashCode(), mapper.hashCode());

        CustomField mockCustomField2 = mock(CustomField.class);
        when(mockCustomField2.getId()).thenReturn(cfId);
        when(mockCustomField2.getClauseNames()).thenReturn(new ClauseNames(CF_10001));

        SelectStatisticsMapper mapper2 = new SelectStatisticsMapper(mockCustomField2, new SelectConverterImpl(null), authenticationContext, customFieldInputHelper);

        // As the mappers are using the same custom field they should be equal
        assertTrue(mapper.equals(mapper2));
        assertEquals(mapper.hashCode(), mapper2.hashCode());

        CustomField mockCustomField3 = mock(CustomField.class);
        when(mockCustomField3.getId()).thenReturn("customfield_10002");
        when(mockCustomField3.getClauseNames()).thenReturn(new ClauseNames(CF_10001));

        SelectStatisticsMapper mapper3 = new SelectStatisticsMapper(mockCustomField3, new SelectConverterImpl(null), authenticationContext, customFieldInputHelper);

        // As the mappers are using different custom field they should *not* be equal
        assertFalse(mapper.equals(mapper3));
        assertFalse(mapper.hashCode() == mapper3.hashCode());


        assertFalse(mapper.equals(null));
        assertFalse(mapper.equals(new Object()));
        assertFalse(mapper.equals(new IssueKeyStatisticsMapper()));
        assertFalse(mapper.equals(new IssueTypeStatisticsMapper(null)));
        // ensure other implmentations of same base class are not equal even though they both extend
        // they inherit the equals and hashCode implementations
        assertFalse(mapper.equals(new UserPickerStatisticsMapper(mockCustomField, null, null)));
        assertFalse(mapper.equals(new TextStatisticsMapper(mockCustomField)));
        assertFalse(mapper.equals(new ProjectSelectStatisticsMapper(mockCustomField, null)));
        assertFalse(mapper.equals(new GroupPickerStatisticsMapper(mockCustomField, null, authenticationContext, customFieldInputHelper)));
    }
}
