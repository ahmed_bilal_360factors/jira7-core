package com.atlassian.jira.plugin.issueview;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.changehistory.ChangeHistoryManager;
import com.atlassian.jira.mock.issue.MockIssue;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.util.UserUtil;
import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.PluginAccessor;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnit;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyMap;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 *
 */
public class TestIssueViewURLHandler {
    private final Integer ERROR_400 = Integer.valueOf(400);
    private final Integer ERROR_404 = Integer.valueOf(404);
    private final String ERROR_EXAMPLE = "Invalid path format. Path should be of format /si/jira.issueviews:xml/JRA-10/JRA-10.xml";
    private final String ERROR_USER = "Could not find a user with the username invalid";
    private final String ERROR_PLUGIN = "Could not find any enabled plugin with key jira.issueviews:issue-novalidformat";
    private final String ERROR_FIELD = "No valid field defined for issue custom view";
    private final String ERROR_NO_ISSUE = "Could not find issue with issue key JRA-1";

    @Rule
    public MethodRule initMockito = MockitoJUnit.rule();
    @Mock
    private HttpServletRequest mockRequest;
    @Mock
    private HttpServletResponse mockResponse;
    @Mock
    private PluginAccessor mockPluginAccessor;
    @Mock
    private PermissionManager mockPermissionManager;
    @Mock
    private RequestDispatcher mockRequestDispatcher;
    @Mock
    private IssueView mockIssueView;
    @Mock
    private IssueViewRequestParamsHelper mockIssueViewRequestParamsHelper;
    @Mock
    private IssueViewFieldParams mockIssueFIeldViewParams;
    @Mock
    private IssueViewModuleDescriptor mockModuleDescriptor;
    @Mock
    private ChangeHistoryManager mockChangeHistoryManager;
    @Mock
    private IssueManager mockIssueManager;
    @Mock
    private UserUtil mockUserUtil;

    private MockApplicationUser applicationUser;
    private MockIssue mockIssue;


    @Before
    public void setUp() throws Exception {
        applicationUser = new MockApplicationUser("admin");

        mockIssue = new MockIssue();
        mockIssue.setKey("HSP-1");
    }

    @Test
    public void testHandleRequestMalformedURLErrorNoSlashes() throws IOException {
        IssueViewURLHandler issueViewURLHandler = new DefaultIssueViewURLHandler(null, null, null, null, null, null);

        when(mockRequest.getPathInfo()).thenReturn("I'm a dodgy URL (no slashes)");

        issueViewURLHandler.handleRequest(mockRequest, mockResponse);
        verify(mockResponse).sendError(eq(ERROR_400), eq(ERROR_EXAMPLE));
    }

    @Test
    public void testHandleRequestMalformedURLErrorOnlyOneSlash() throws IOException {
        IssueViewURLHandler issueViewURLHandler = new DefaultIssueViewURLHandler(null, null, null, null, null, null);

        when(mockRequest.getPathInfo()).thenReturn("/I'm a less dodgy URL (one slash)");

        issueViewURLHandler.handleRequest(mockRequest, mockResponse);
        verify(mockResponse).sendError(eq(ERROR_400), eq(ERROR_EXAMPLE));
    }

    @Test
    public void testHandleRequestMalformedURLErrorTwoSlashes() throws IOException {
        IssueViewURLHandler issueViewURLHandler = new DefaultIssueViewURLHandler(null, null, null, null, null, null);

        when(mockRequest.getPathInfo()).thenReturn("//I'm a less dodgy URL (two slash, no issue key)");

        issueViewURLHandler.handleRequest(mockRequest, mockResponse);
        verify(mockResponse).sendError(eq(ERROR_400), eq(ERROR_EXAMPLE));
    }

    @Test
    public void testInvalidUser() throws IOException {
        when(mockRequest.getPathInfo()).thenReturn("/jira.issueviews:issue-xml/HSP-1/HSP-1.xml");
        when(mockRequest.getRemoteUser()).thenReturn("invalid");

        when(mockUserUtil.getUserByName(eq("invalid"))).thenReturn(null);

        IssueViewURLHandler issueViewURLHandler = new DefaultIssueViewURLHandler(null, null, null, null, null, mockUserUtil);
        issueViewURLHandler.handleRequest(mockRequest, mockResponse);

        verify(mockResponse).sendError(eq(ERROR_400), eq(ERROR_USER));
    }

    @Test
    public void testNoUser() throws IOException, ServletException {
        when(mockModuleDescriptor.getCompleteKey()).thenReturn("jira.issueviews:issue-xml");
        when(mockModuleDescriptor.getFileExtension()).thenReturn("xml");

        when(mockModuleDescriptor.getIssueView()).thenReturn(mockIssueView);

        when(mockPluginAccessor.getEnabledPluginModule(eq("jira.issueviews:issue-xml"))).thenReturn((ModuleDescriptor) mockModuleDescriptor);

        when(mockIssueManager.getIssueObject(eq("HSP-1"))).thenReturn(mockIssue);

        when(mockPermissionManager.hasPermission(anyInt(), Mockito.<Issue>anyObject(), Mockito.<ApplicationUser>anyObject())).thenReturn(false);

        when(mockRequest.getPathInfo()).thenReturn("jira.issueviews:issue-xml/HSP-1/HSP-1.xml");
        when(mockRequest.getRemoteUser()).thenReturn(null);
        when(mockRequest.getParameter(eq("jira.issue.searchlocation"))).thenReturn("");

        IssueViewURLHandler issueViewURLHandler = new DefaultIssueViewURLHandler(mockPluginAccessor, mockIssueManager, mockPermissionManager, null, null, null);
        issueViewURLHandler.handleRequest(mockRequest, mockResponse);

        verify(mockResponse).sendRedirect(anyString());
    }

    @Test
    public void testNoPlugin() throws IOException {
        when(mockUserUtil.getUserByName(eq("admin"))).thenReturn(applicationUser);

        when(mockPluginAccessor.getEnabledPluginModule(eq("jira.issueviews:issue-novalidformat"))).thenReturn(null);

        when(mockRequest.getPathInfo()).thenReturn("jira.issueviews:issue-novalidformat/HSP-1/HSP-1.novalidformat");
        when(mockRequest.getRemoteUser()).thenReturn("admin");

        IssueViewURLHandler issueViewURLHandler = new DefaultIssueViewURLHandler(mockPluginAccessor, null, null, null, null, mockUserUtil);
        issueViewURLHandler.handleRequest(mockRequest, mockResponse);

        verify(mockResponse).sendError(eq(ERROR_400), eq(ERROR_PLUGIN));
    }

    @Test
    public void testNoPermissionOnIssue() throws IOException, ServletException {
        when(mockUserUtil.getUserByName(eq("admin"))).thenReturn(applicationUser);

        when(mockModuleDescriptor.getCompleteKey()).thenReturn("jira.issueviews:issue-xml");
        when(mockModuleDescriptor.getFileExtension()).thenReturn("xml");
        when(mockModuleDescriptor.getIssueView()).thenReturn(mockIssueView);

        when(mockPluginAccessor.getEnabledPluginModule(eq("jira.issueviews:issue-xml"))).thenReturn((ModuleDescriptor) mockModuleDescriptor);

        when(mockIssueManager.getIssueObject(eq("HSP-1"))).thenReturn(mockIssue);

        when(mockPermissionManager.hasPermission(anyInt(), Mockito.<Issue>anyObject(), Mockito.<ApplicationUser>anyObject())).thenReturn(false);

        when(mockRequest.getPathInfo()).thenReturn("jira.issueviews:issue-xml/HSP-1/HSP-1.xml");
        when(mockRequest.getRemoteUser()).thenReturn("admin");
        when(mockRequest.getParameter(eq("jira.issue.searchlocation"))).thenReturn("");
        when(mockRequest.getRequestDispatcher(eq("/secure/views/permissionviolation.jsp"))).thenReturn(mockRequestDispatcher);

        IssueViewURLHandler issueViewURLHandler = new DefaultIssueViewURLHandler(mockPluginAccessor, mockIssueManager, mockPermissionManager, null, null, mockUserUtil);
        issueViewURLHandler.handleRequest(mockRequest, mockResponse);

        verify(mockRequestDispatcher).forward(Matchers.<ServletRequest>anyObject(), Matchers.<ServletResponse>anyObject());
    }

    @Test
    public void testNoPermissionOnIssueError() throws IOException, ServletException {
        when(mockUserUtil.getUserByName(eq("admin"))).thenReturn(applicationUser);

        when(mockModuleDescriptor.getCompleteKey()).thenReturn("jira.issueviews:issue-xml");
        when(mockModuleDescriptor.getFileExtension()).thenReturn("xml");
        when(mockModuleDescriptor.getIssueView()).thenReturn(mockIssueView);

        when(mockPluginAccessor.getEnabledPluginModule(eq("jira.issueviews:issue-xml"))).thenReturn((ModuleDescriptor) mockModuleDescriptor);

        when(mockIssueManager.getIssueObject(eq("HSP-1"))).thenReturn(mockIssue);

        when(mockPermissionManager.hasPermission(anyInt(), Mockito.<Issue>anyObject(), Mockito.<ApplicationUser>anyObject())).thenReturn(false);

        doThrow(new ServletException()).when(mockRequestDispatcher).forward(Matchers.<ServletRequest>anyObject(), Matchers.<ServletResponse>anyObject());

        when(mockRequest.getPathInfo()).thenReturn("jira.issueviews:issue-xml/HSP-1/HSP-1.xml");
        when(mockRequest.getRemoteUser()).thenReturn("admin");
        when(mockRequest.getParameter(eq("jira.issue.searchlocation"))).thenReturn("");
        when(mockRequest.getRequestDispatcher(eq("/secure/views/permissionviolation.jsp"))).thenReturn(mockRequestDispatcher);

        IssueViewURLHandler issueViewURLHandler = new DefaultIssueViewURLHandler(mockPluginAccessor, mockIssueManager, mockPermissionManager, null, null, mockUserUtil);
        try {
            issueViewURLHandler.handleRequest(mockRequest, mockResponse);
            fail("Expected ServletException to was thrown");
        } catch (RuntimeException ex) {
            assertTrue("Expected nesting ServletException", ex.getCause() instanceof ServletException);
        }
    }

    @Test
    public void testNoFieldsDefined() throws Exception {
        when(mockUserUtil.getUserByName(eq("admin"))).thenReturn(applicationUser);

        when(mockModuleDescriptor.getCompleteKey()).thenReturn("jira.issueviews:issue-xml");
        when(mockModuleDescriptor.getFileExtension()).thenReturn("xml");
        when(mockModuleDescriptor.getIssueView()).thenReturn(mockIssueView);

        when(mockPluginAccessor.getEnabledPluginModule(eq("jira.issueviews:issue-xml"))).thenReturn((ModuleDescriptor) mockModuleDescriptor);

        when(mockIssueManager.getIssueObject(eq("HSP-1"))).thenReturn(mockIssue);

        when(mockPermissionManager.hasPermission(anyInt(), Mockito.<Issue>anyObject(), Mockito.<ApplicationUser>anyObject())).thenReturn(true);

        when(mockIssueViewRequestParamsHelper.getIssueViewFieldParams(anyMap())).thenReturn(mockIssueFIeldViewParams);

        when(mockIssueFIeldViewParams.isCustomViewRequested()).thenReturn(true);
        when(mockIssueFIeldViewParams.isAnyFieldDefined()).thenReturn(false);

        when(mockRequest.getPathInfo()).thenReturn("jira.issueviews:issue-xml/HSP-1/HSP-1.xml");
        when(mockRequest.getRemoteUser()).thenReturn("admin");
        when(mockRequest.getParameter(eq("jira.issue.searchlocation"))).thenReturn("");
        when(mockRequest.getParameterMap()).thenReturn(Collections.emptyMap());


        IssueViewURLHandler issueViewURLHandler = new DefaultIssueViewURLHandler(mockPluginAccessor, mockIssueManager, mockPermissionManager, null, mockIssueViewRequestParamsHelper, mockUserUtil);
        issueViewURLHandler.handleRequest(mockRequest, mockResponse);
        verify(mockResponse).sendError(eq(ERROR_400), eq(ERROR_FIELD));
    }

    @Test
    public void testHandleRequestForMovedIssue() throws Exception {
        when(mockIssueManager.getIssueObject(anyString())).thenReturn(new MockIssue(123, "MOVED-1"));

        when(mockUserUtil.getUserByName(eq("admin"))).thenReturn(applicationUser);

        when(mockModuleDescriptor.getCompleteKey()).thenReturn("jira.issueviews:issue-xml");
        when(mockModuleDescriptor.getFileExtension()).thenReturn("xml");

        when(mockPluginAccessor.getEnabledPluginModule(eq("jira.issueviews:issue-xml"))).thenReturn((ModuleDescriptor) mockModuleDescriptor);

        IssueViewURLHandler issueViewURLHandler = new DefaultIssueViewURLHandler(mockPluginAccessor, mockIssueManager, null, null, null, mockUserUtil);

        when(mockRequest.getPathInfo()).thenReturn("/jira.issueviews:issue-xml/JRA-1/JRA-1.xml");
        when(mockRequest.getContextPath()).thenReturn("/someJIRAInstall");
        when(mockRequest.getQueryString()).thenReturn("key=value");
        when(mockRequest.getRemoteUser()).thenReturn("admin");

        issueViewURLHandler.handleRequest(mockRequest, mockResponse);
        verify(mockResponse).sendRedirect(eq("/someJIRAInstall/si/jira.issueviews:issue-xml/MOVED-1/MOVED-1.xml?key=value"));
    }

    @Test
    public void testNoIssue() throws IOException {
        when(mockIssueManager.findMovedIssue(anyString())).thenReturn(null);

        when(mockModuleDescriptor.getCompleteKey()).thenReturn("jira.issueviews:issue-xml");
        when(mockModuleDescriptor.getFileExtension()).thenReturn("xml");

        when(mockUserUtil.getUserByName(eq("admin"))).thenReturn(applicationUser);

        when(mockPluginAccessor.getEnabledPluginModule(eq("jira.issueviews:issue-xml"))).thenReturn((ModuleDescriptor) mockModuleDescriptor);

        IssueViewURLHandler issueViewURLHandler = new DefaultIssueViewURLHandler(mockPluginAccessor, mockIssueManager, null, null, null, mockUserUtil);

        when(mockRequest.getPathInfo()).thenReturn("/jira.issueviews:issue-xml/JRA-1/JRA-1.xml");
        when(mockRequest.getRemoteUser()).thenReturn("admin");

        issueViewURLHandler.handleRequest(mockRequest, mockResponse);
        verify(mockResponse).sendError(eq(ERROR_404), eq(ERROR_NO_ISSUE));
    }
}
