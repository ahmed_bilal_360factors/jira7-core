package com.atlassian.jira.upgrade.tasks.role;

import com.atlassian.jira.test.util.lic.software.SoftwareLicenses;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import org.junit.Test;

import static com.atlassian.jira.application.ApplicationKeys.CORE;
import static com.atlassian.jira.application.ApplicationKeys.SERVICE_DESK;
import static com.atlassian.jira.application.ApplicationKeys.SOFTWARE;
import static com.atlassian.jira.test.util.lic.core.CoreLicenses.LICENSE_CORE;
import static com.atlassian.jira.test.util.lic.sd.ServiceDeskLicenses.LICENSE_SERVICE_DESK_ABP;
import static com.atlassian.jira.test.util.lic.sd.ServiceDeskLicenses.LICENSE_SERVICE_DESK_RBP;
import static com.atlassian.jira.test.util.lic.sd.ServiceDeskLicenses.LICENSE_SERVICE_DESK_TBP;
import static com.atlassian.jira.upgrade.tasks.role.TestUtils.toLicense;
import static com.atlassian.jira.upgrade.tasks.role.TestUtils.toLicenses;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

public class LicensesTest {
    @Test(expected = IllegalArgumentException.class)
    public void cotrRejectsNullLicenses() {
        //when
        new Licenses(null);

        //then exception
    }

    @Test(expected = IllegalArgumentException.class)
    public void cotrRejectsLicensesContainingNulls() {
        License sdLicense1 = new License(LICENSE_SERVICE_DESK_ABP.getLicenseString());

        //when
        new Licenses(Sets.newHashSet(sdLicense1, null));

        //then exception
    }

    @Test(expected = MigrationFailedException.class)
    public void cotrRejectsLicensesWithSameApplications() {
        //given
        License sdLicense1 = new License(LICENSE_SERVICE_DESK_ABP.getLicenseString());
        License sdLicense2 = new License(LICENSE_SERVICE_DESK_RBP.getLicenseString());

        //then
        new Licenses(ImmutableList.of(sdLicense1, sdLicense2));
    }

    @Test
    public void cotrCorrectlyParsesOutTheLicenses() {
        //given
        License sdLicense = new License(LICENSE_SERVICE_DESK_ABP.getLicenseString());
        License softwareLicense = new License(SoftwareLicenses.LICENSE_SOFTWARE.getLicenseString());

        //when
        final Licenses licenses = new Licenses(ImmutableList.of(sdLicense, softwareLicense));

        //then
        assertThat(licenses.get(), equalTo(ImmutableSet.of(sdLicense, softwareLicense)));
        assertThat(licenses.isEmpty(), equalTo(false));
        assertThat(licenses.keys(), equalTo(ImmutableSet.of(SERVICE_DESK, SOFTWARE)));
    }

    @Test
    public void cotrAcceptsNoLicenses() {
        //when
        final Licenses licenses = new Licenses(ImmutableList.of());

        //then
        assertThat(licenses.get(), equalTo(ImmutableSet.of()));
        assertThat(licenses.isEmpty(), equalTo(true));
        assertThat(licenses.keys(), equalTo(ImmutableSet.of()));
    }

    @Test
    public void cotrAcceptsDuplicateLicenses() {
        //given
        License sdLicense = new License(LICENSE_SERVICE_DESK_ABP.getLicenseString());

        //when
        final Licenses licenses = new Licenses(ImmutableList.of(sdLicense, sdLicense));

        //then
        assertThat(licenses.get(), equalTo(ImmutableSet.of(sdLicense)));
        assertThat(licenses.isEmpty(), equalTo(false));
        assertThat(licenses.keys(), equalTo(ImmutableSet.of(SERVICE_DESK)));
    }

    @Test
    public void addLicenseAcceptsGoodLicense() {
        //given
        License sdLicense = new License(LICENSE_SERVICE_DESK_ABP.getLicenseString());
        License coreLicense = new License(LICENSE_CORE.getLicenseString());
        Licenses licenses = new Licenses(ImmutableList.of(sdLicense));

        //when
        licenses = licenses.addLicense(coreLicense);

        //then
        assertThat(licenses.get(), equalTo(ImmutableSet.of(coreLicense, sdLicense)));
        assertThat(licenses.isEmpty(), equalTo(false));
        assertThat(licenses.keys(), equalTo(ImmutableSet.of(SERVICE_DESK, CORE)));
    }

    @Test
    public void addLicenseAcceptsDuplicateLicense() {
        //given
        License sdLicense = new License(LICENSE_SERVICE_DESK_ABP.getLicenseString());
        Licenses licenses = new Licenses(ImmutableList.of(sdLicense));

        //when
        licenses = licenses.addLicense(sdLicense);

        //then
        assertThat(licenses.get(), equalTo(ImmutableSet.of(sdLicense)));
        assertThat(licenses.isEmpty(), equalTo(false));
        assertThat(licenses.keys(), equalTo(ImmutableSet.of(SERVICE_DESK)));
    }

    @Test(expected = MigrationFailedException.class)
    public void addLicenseRejectsLicenseWithDuplicateKey() {
        //given
        License sdLicense1 = new License(LICENSE_SERVICE_DESK_ABP.getLicenseString());
        License sdLicense2 = new License(LICENSE_SERVICE_DESK_RBP.getLicenseString());
        License coreLicense = new License(LICENSE_CORE.getLicenseString());
        Licenses licenses = new Licenses(ImmutableList.of(sdLicense2));

        //when
        licenses.addLicense(coreLicense).addLicense(sdLicense1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void addLicenseRejectsNullLicense() {
        //given
        License coreLicense = new License(LICENSE_CORE.getLicenseString());
        Licenses licenses = new Licenses(ImmutableList.of(coreLicense));

        //when
        licenses.addLicense(coreLicense).addLicense(null);

        //then exception.
    }

    @Test(expected = IllegalArgumentException.class)
    public void canAddRejectsNullLicense() {
        //given
        License coreLicense = new License(LICENSE_CORE.getLicenseString());
        Licenses licenses = new Licenses(ImmutableList.of(coreLicense));

        //when
        licenses.addLicense(coreLicense).canAdd(null);

        //then exception.
    }

    @Test
    public void canAddReturnsTrueWhenLicensesAreDistinct() {
        //given
        final License sdLicense = toLicense(LICENSE_SERVICE_DESK_ABP);
        Licenses licenses = toLicenses(LICENSE_CORE);

        //when
        final boolean result = licenses.canAdd(sdLicense);

        //then
        assertThat(result, equalTo(true));
    }

    @Test
    public void canAddReturnsFalseWhenLicensesAreNotDistinct() {
        //given
        final License sdLicense = toLicense(LICENSE_SERVICE_DESK_ABP);
        Licenses licenses = toLicenses(LICENSE_SERVICE_DESK_TBP);

        //when
        final boolean result = licenses.canAdd(sdLicense);

        //then
        assertThat(result, equalTo(false));
    }
}