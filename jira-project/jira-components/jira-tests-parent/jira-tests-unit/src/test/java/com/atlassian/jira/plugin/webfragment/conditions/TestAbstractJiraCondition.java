package com.atlassian.jira.plugin.webfragment.conditions;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.mock.component.MockComponentWorker;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.util.UserUtil;
import com.atlassian.plugin.web.Condition;
import com.google.common.collect.ImmutableMap;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestAbstractJiraCondition {
    private static final Map<String, Object> ANONYMOUS = Collections.emptyMap();

    private ApplicationUser expectedUser;

    @Mock
    private UserUtil mockUserUtil;

    @After
    public void tearDown() {
        expectedUser = null;
        mockUserUtil = null;
    }

    @Test
    public void testShouldDisplayWithUser() {
        asFred();
        checkShouldDisplay(true, context("user", expectedUser));
    }

    @Test
    public void testShouldDisplayWithUsername() {
        asFred();
        checkShouldDisplay(true, context("username", "fred"));
    }

    @Test
    public void testShouldNotDisplayWithUser() {
        checkShouldDisplay(false, context("username", "fred"));
    }

    @Test
    public void testShouldDisplayAnonymous() {
        checkShouldDisplay(true, ANONYMOUS);
    }

    @Test
    public void testShouldNotDisplayAnonymous() {
        checkShouldDisplay(false, ANONYMOUS);
    }

    @Test
    public void testShouldReturnFalseIfExceptionOccur() {
        final Condition condition = new AbstractJiraCondition() {
            @Override
            UserUtil getUserUtil() {
                return mockUserUtil;
            }

            @Override
            public boolean shouldDisplay(ApplicationUser user, JiraHelper jiraHelper) {
                throw new LinkageError("That's really unexpected error here!");
            }
        };
        assertEquals(false, condition.shouldDisplay(ANONYMOUS));
    }


    private ApplicationUser asFred() {
        ComponentAccessor.initialiseWorker(new MockComponentWorker());
        expectedUser = new MockApplicationUser("fred");
        when(mockUserUtil.getUserByName("fred")).thenReturn(expectedUser);
        return expectedUser;
    }

    private void checkShouldDisplay(final boolean expectedResult, Map<String, Object> context) {
        final Condition condition = new AbstractJiraCondition() {
            @Override
            UserUtil getUserUtil() {
                return mockUserUtil;
            }

            @Override
            public boolean shouldDisplay(ApplicationUser user, JiraHelper jiraHelper) {
                assertEquals(expectedUser, user);
                return expectedResult;
            }
        };
        assertEquals(expectedResult, condition.shouldDisplay(context));
    }

    private Map<String, Object> context(String key, Object value) {
        return ImmutableMap.of(key, value);
    }
}
