package com.atlassian.jira.upgrade.tasks;

import com.atlassian.jira.config.properties.ApplicationProperties;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;

/**
 */
public class TestUpgradeTask_Build520 {
    @Rule
    public final MockitoRule initMockito = MockitoJUnit.rule();
    @Mock
    private ApplicationProperties applicationProperties;
    private UpgradeTask_Build520 upgradeTask_build520;

    @Before
    public void setUp() throws Exception {
        upgradeTask_build520 = new UpgradeTask_Build520(applicationProperties);
    }

    @Test
    public void testGetBuildNumber() {
        assertEquals(520, upgradeTask_build520.getBuildNumber());
    }

    @Test
    public void testDoUpgrade() throws Exception {
        upgradeTask_build520.doUpgrade(false);
        verify(applicationProperties).setString("jira.maximum.authentication.attempts.allowed", "3");
    }
}
