package com.atlassian.jira.util;

import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoContainer;
import com.atlassian.jira.upgrade.UpgradeVersionHistoryItem;
import com.atlassian.jira.upgrade.UpgradeVersionHistoryItemImpl;
import com.atlassian.jira.upgrade.UpgradeVersionHistoryManager;
import com.google.common.collect.ImmutableList;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;

import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Optional;

import static org.mockito.Mockito.when;

public class TestInstanceInfo {
    @Rule
    public MockitoContainer mockitoContainer = new MockitoContainer(this);

    private InstanceInfo instanceInfo;
    
    @Mock
    @AvailableInContainer
    private UpgradeVersionHistoryManager upgradeVersionHistoryManager;
    

    @Before
    public void setUp() {
        instanceInfo = new InstanceInfoImpl(upgradeVersionHistoryManager);
    }

    @Test
    public void emptyUpgradeHistoryShouldResultInEmptyOption() {
        when(upgradeVersionHistoryManager.getAllUpgradeVersionHistory()).thenReturn(Collections.emptyList());

        Assert.assertThat(instanceInfo.getInstanceCreatedDate(), Matchers.is(Optional.empty()));
    }

    @Test
    public void nullUpgradeHistoryShouldResultInEmptyOption() {
        when(upgradeVersionHistoryManager.getAllUpgradeVersionHistory()).thenReturn(null);

        Assert.assertThat(instanceInfo.getInstanceCreatedDate(), Matchers.is(Optional.empty()));
    }

    @Test
    public void nonEmptyUpgradeHistoryWithEmptyDateShouldResultInEmptyOption() {
        final UpgradeVersionHistoryItem uhi = new UpgradeVersionHistoryItemImpl(
                null,
                "1", "2", "3", "4"
        );
        when(upgradeVersionHistoryManager.getAllUpgradeVersionHistory()).thenReturn(ImmutableList.of(uhi));

        Assert.assertThat(instanceInfo.getInstanceCreatedDate(), Matchers.is(Optional.empty()));
    }

    @Test
    public void nonEmptyUpgradeHistoryShouldResultInFilledOption() {
        final Date expected = new Date();
        final UpgradeVersionHistoryItem uhi = new UpgradeVersionHistoryItemImpl(
                expected,
                "1", "2", "3", "4"
        );
        when(upgradeVersionHistoryManager.getAllUpgradeVersionHistory()).thenReturn(ImmutableList.of(uhi));

        Assert.assertThat(instanceInfo.getInstanceCreatedDate(), Matchers.is(Optional.of(expected)));
    }

    @Test
    public void minimalDateReturnedFromHistory() {
        Calendar c = Calendar.getInstance();
        final Date now = c.getTime();
        c.add(Calendar.DATE, 1);
        final Date nowPlus1 = c.getTime();
        c.add(Calendar.DATE, 1);
        final Date nowPlus2 = c.getTime();
        c.add(Calendar.DATE, 1);
        final Date nowPlus3 = c.getTime();

        final ImmutableList.Builder<UpgradeVersionHistoryItem> builder = ImmutableList.builder();
        for (Date d : new Date[]{nowPlus1, now, nowPlus3, nowPlus2}) {
            builder.add(new UpgradeVersionHistoryItemImpl(d, "1", "2", "3", "4"));
        }
        builder.add(new UpgradeVersionHistoryItemImpl(null, "1", "2", "3", "4"));

        when(upgradeVersionHistoryManager.getAllUpgradeVersionHistory()).thenReturn(builder.build());

        Assert.assertThat(instanceInfo.getInstanceCreatedDate(), Matchers.is(Optional.of(now)));
    }
}
