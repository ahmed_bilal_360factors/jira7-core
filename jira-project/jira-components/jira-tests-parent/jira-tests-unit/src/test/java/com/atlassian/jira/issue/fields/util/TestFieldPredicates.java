package com.atlassian.jira.issue.fields.util;

import com.atlassian.jira.action.issue.customfields.MockCustomFieldType;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.DateField;
import com.atlassian.jira.issue.fields.Field;
import com.atlassian.jira.issue.fields.MockOrderableField;
import com.atlassian.jira.issue.fields.UserField;
import com.atlassian.jira.util.Predicate;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Test {@link com.atlassian.jira.issue.fields.util.FieldPredicates}.
 *
 * @since v4.1
 */
public class TestFieldPredicates {
    @Test
    public void testIsCustomField() throws Exception {
        final CustomField customField = mock(CustomField.class);
        final Field field = mock(Field.class);

        final Predicate<Field> cfPredicate = FieldPredicates.isCustomField();
        assertTrue(cfPredicate.evaluate(customField));
        assertFalse(cfPredicate.evaluate(null));
        assertFalse(cfPredicate.evaluate(field));
    }

    @Test
    public void testIsDateField() throws Exception {
        final Field nothingField = mock(Field.class);

        final CustomField dateFieldCF = mock(CustomField.class);
        when(dateFieldCF.getCustomFieldType()).thenReturn(new DateType());

        final CustomField customField = mock(CustomField.class);
        when(customField.getCustomFieldType()).thenReturn(new MockCustomFieldType());

        final Predicate<Field> cfPredicate = FieldPredicates.isDateField();
        assertTrue(cfPredicate.evaluate(dateFieldCF));
        assertFalse(cfPredicate.evaluate(customField));
        assertFalse(cfPredicate.evaluate(nothingField));
        assertTrue(cfPredicate.evaluate(new TestDateField("blarg")));
        assertFalse(cfPredicate.evaluate(null));
    }

    @Test
    public void testIsCustomDateField() throws Exception {
        final Field nothingField = mock(Field.class);

        final CustomField dateFieldCF = mock(CustomField.class);
        when(dateFieldCF.getCustomFieldType()).thenReturn(new DateType());

        final CustomField customField = mock(CustomField.class);
        when(customField.getCustomFieldType()).thenReturn(new MockCustomFieldType());

        final Predicate<Field> cfPredicate = FieldPredicates.isCustomDateField();
        assertTrue(cfPredicate.evaluate(dateFieldCF));
        assertFalse(cfPredicate.evaluate(customField));
        assertFalse(cfPredicate.evaluate(nothingField));
        assertFalse(cfPredicate.evaluate(new TestDateField("blarg")));
        assertFalse(cfPredicate.evaluate(null));
    }

    @Test
    public void testIsUserField() throws Exception {
        final Field nothingField = mock(Field.class);

        final CustomField userFieldCF = mock(CustomField.class);
        when(userFieldCF.getCustomFieldType()).thenReturn(new UserType());

        final CustomField customField = mock(CustomField.class);
        when(customField.getCustomFieldType()).thenReturn(new DateType());

        final Predicate<Field> cfPredicate = FieldPredicates.isUserField();
        assertTrue(cfPredicate.evaluate(userFieldCF));
        assertFalse(cfPredicate.evaluate(customField));
        assertFalse(cfPredicate.evaluate(nothingField));
        assertTrue(cfPredicate.evaluate(new TestUserField("blarg")));
        assertFalse(cfPredicate.evaluate(null));
    }

    @Test
    public void testIsCustomUserField() throws Exception {
        final Field nothingField = mock(Field.class);

        final CustomField userFieldCF = mock(CustomField.class);
        when(userFieldCF.getCustomFieldType()).thenReturn(new UserType());

        final CustomField customField = mock(CustomField.class);
        when(customField.getCustomFieldType()).thenReturn(new DateType());

        final Predicate<Field> cfPredicate = FieldPredicates.isCustomUserField();
        assertTrue(cfPredicate.evaluate(userFieldCF));
        assertFalse(cfPredicate.evaluate(customField));
        assertFalse(cfPredicate.evaluate(nothingField));
        assertFalse(cfPredicate.evaluate(new TestUserField("blarg")));
        assertFalse(cfPredicate.evaluate(null));
    }

    @Test
    public void testIsStandardViewIssueCustomField() throws Exception {
        final Field nothingField = mock(Field.class);

        final CustomField userFieldCF = mock(CustomField.class);
        when(userFieldCF.getCustomFieldType()).thenReturn(new UserType());

        final CustomField dateFieldCf = mock(CustomField.class);
        when(dateFieldCf.getCustomFieldType()).thenReturn(new DateType());

        final CustomField standardCF = mock(CustomField.class);
        when(standardCF.getCustomFieldType()).thenReturn(new MockCustomFieldType());

        final Predicate<Field> cfPredicate = FieldPredicates.isStandardViewIssueCustomField();
        assertFalse(cfPredicate.evaluate(userFieldCF));
        assertFalse(cfPredicate.evaluate(dateFieldCf));
        assertTrue(cfPredicate.evaluate(standardCF));
        assertFalse(cfPredicate.evaluate(nothingField));
        assertFalse(cfPredicate.evaluate(new TestUserField("blarg")));
        assertFalse(cfPredicate.evaluate(new TestDateField("blarg")));
        assertFalse(cfPredicate.evaluate(null));
    }

    private static class DateType extends MockCustomFieldType implements DateField {
    }

    private static class UserType extends MockCustomFieldType implements UserField {
    }

    private static class TestDateField extends MockOrderableField implements DateField {
        public TestDateField(final String id) {
            super(id);
        }
    }

    private static class TestUserField extends MockOrderableField implements UserField {
        public TestUserField(final String id) {
            super(id);
        }
    }
}
