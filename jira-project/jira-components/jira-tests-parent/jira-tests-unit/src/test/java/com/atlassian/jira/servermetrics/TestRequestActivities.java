package com.atlassian.jira.servermetrics;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

import static org.hamcrest.Matchers.isIn;
import static org.junit.Assert.assertThat;

@RunWith(Parameterized.class)
public class TestRequestActivities {
    private static Set<String> whitelistedProperties;

    @BeforeClass
    public static void loadWhitelist() throws IOException {
        whitelistedProperties = TestRequestCheckpoints.loadWhitelistPropertiesForEvent("jira.http.request.stats");
    }

    @Parameterized.Parameters(name = "activityTimings.{0}")
    public static Collection<Object[]> data() {
        return Arrays.stream(RequestActivities.values()).map(activity -> new Object[]{activity}).collect(Collectors.toList());
    }

    private final RequestActivities testObj;

    public TestRequestActivities(RequestActivities checkpoint) {
        this.testObj = checkpoint;
    }


    @Test
    public void shouldPropertyBeIncludedInWhitelist() {
        assertThat("activityTimings." + testObj.name(), isIn(whitelistedProperties));
    }
}
