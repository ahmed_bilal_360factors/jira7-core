package com.atlassian.jira.web.action.admin;

import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.JiraWebActionSupportDependencies;
import com.atlassian.jira.junit.rules.MockitoContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.plugin.myjirahome.MyJiraHomeLinker;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.Mock;
import webwork.action.ServletActionContext;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(JUnit4.class)
public class TestLeaveAdmin {
    private static final String MY_JIRA_HOME = "/my-jira-home";

    @Rule
    public final MockitoContainer mockContainer = MockitoMocksInContainer.rule(this);
    @Rule
    public TestRule webActionSupport = JiraWebActionSupportDependencies.build(mockContainer);

    @Mock
    @AvailableInContainer
    private JiraAuthenticationContext mockJiraAuthenticationContext;
    @Mock
    private ApplicationUser mockUser;
    @Mock
    private HttpServletResponse mockHttpServletResponse;

    @Mock
    private MyJiraHomeLinker mockMyJiraHomeLinker;

    private LeaveAdmin action;

    @Before
    public void setUpMocks() {
        action = new LeaveAdmin(mockMyJiraHomeLinker);

        ServletActionContext.setResponse(mockHttpServletResponse);
    }

    @Test
    public void testLeaveAdminWhenUserNotLoggedIn() throws Exception {
        expectUserIsNotLoggedInAndHomeIsDefault();

        action.doExecute();

        verifyRedirect(MyJiraHomeLinker.DEFAULT_HOME_OD_ANON);
    }

    @Test
    public void testLeaveAdminWhenUserLoggedInWithHomeSet() throws Exception {
        expectUserIsLoggedInWithHomeSet();

        action.doExecute();

        verifyRedirect(MY_JIRA_HOME);
    }

    private void expectUserIsNotLoggedInAndHomeIsDefault() {
        when(mockJiraAuthenticationContext.getUser()).thenReturn(null);
        when(mockMyJiraHomeLinker.getHomeLink((ApplicationUser) null)).thenReturn(MyJiraHomeLinker.DEFAULT_HOME_OD_ANON);
    }

    private void expectUserIsLoggedInWithHomeSet() {
        when(mockJiraAuthenticationContext.getUser()).thenReturn(mockUser);
        expectFindMyHomeIs(MY_JIRA_HOME);
    }

    private void expectFindMyHomeIs(final String home) {
        when(mockMyJiraHomeLinker.getHomeLink(mockUser)).thenReturn(home);
    }

    private void verifyRedirect(final String expectedUrl) throws IOException {
        verify(mockHttpServletResponse).sendRedirect(eq(expectedUrl));
    }
}
