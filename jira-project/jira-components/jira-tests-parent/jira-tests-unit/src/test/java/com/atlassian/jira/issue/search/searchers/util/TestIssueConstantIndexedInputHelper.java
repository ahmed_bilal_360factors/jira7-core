package com.atlassian.jira.issue.search.searchers.util;

import com.atlassian.jira.issue.IssueConstant;
import com.atlassian.jira.issue.MockIssueConstant;
import com.atlassian.jira.issue.search.searchers.transformer.FieldFlagOperandRegistry;
import com.atlassian.jira.jql.operand.JqlOperandResolver;
import com.atlassian.jira.jql.resolver.IndexInfoResolver;
import com.atlassian.jira.jql.resolver.NameResolver;
import com.atlassian.query.operand.SingleValueOperand;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestIssueConstantIndexedInputHelper {
    @Rule
    public final MethodRule initMockito = MockitoJUnit.rule();

    @Mock
    private NameResolver<IssueConstant> nameResolver;
    @Mock
    private IndexInfoResolver<IssueConstant> indexInfoResolver;
    @Mock
    private JqlOperandResolver operandResolver;
    @Mock
    private FieldFlagOperandRegistry fieldFlagOperandRegistry;

    @InjectMocks
    private IssueConstantIndexedInputHelper helper;

    @Test
    public void testCreateSingleValueOperandFromIdIsntNumber() throws Exception {
        assertEquals(new SingleValueOperand("test"), helper.createSingleValueOperandFromId("test"));
    }

    @Test
    public void testCreateSingleValueOperandFromIdIsNotAComponent() throws Exception {
        assertEquals(new SingleValueOperand(123l), helper.createSingleValueOperandFromId("123"));
        verify(nameResolver).get(123l); // returned null
    }

    @Test
    public void testCreateSingleValueOperandFromIdIsAVersion() throws Exception {
        when(nameResolver.get(123l)).thenReturn(new MockIssueConstant("123", "Component 1"));
        assertEquals(new SingleValueOperand("Component 1"), helper.createSingleValueOperandFromId("123"));
    }
}
