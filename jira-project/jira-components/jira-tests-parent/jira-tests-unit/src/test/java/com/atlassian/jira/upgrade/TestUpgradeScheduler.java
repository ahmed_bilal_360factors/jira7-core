package com.atlassian.jira.upgrade;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.util.BuildUtilsInfo;
import com.atlassian.scheduler.JobRunner;
import com.atlassian.scheduler.JobRunnerRequest;
import com.atlassian.scheduler.JobRunnerResponse;
import com.atlassian.scheduler.SchedulerService;
import com.atlassian.scheduler.SchedulerServiceException;
import com.atlassian.scheduler.config.JobConfig;
import com.atlassian.scheduler.config.JobId;
import com.atlassian.scheduler.status.RunOutcome;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestUpgradeScheduler {

    private static final int DELAY_IN_MINUTES = 0;
    @Mock
    private ApplicationProperties applicationProperties;

    @Rule
    public MethodRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private IndexingUpgradeService indexingUpgradeService;

    @Mock
    private SchedulerService schedulerService;

    @Mock
    private JobRunnerRequest jobRunnerRequest;

    @Mock
    private EventPublisher eventPublisher;

    @Mock
    private BuildUtilsInfo buildUtilsInfo;

    @Captor
    private ArgumentCaptor<JobRunner> jobRunner;

    private UpgradeScheduler upgradeScheduler;

    @Before
    public void setUp() {
        upgradeScheduler = new UpgradeScheduler(
                indexingUpgradeService,
                schedulerService,
                eventPublisher,
                buildUtilsInfo
        );
    }

    @Test
    public void shouldRegisterJobAndRunUpgrades() throws SchedulerServiceException {

        when(indexingUpgradeService.runUpgrades()).thenReturn(UpgradeResult.OK);

        final UpgradeResult schedulingResult = upgradeScheduler.scheduleUpgrades(DELAY_IN_MINUTES);

        assertThat(schedulingResult.successful(), equalTo(true));
        verify(schedulerService, times(1)).registerJobRunner(any(), jobRunner.capture());
        verify(schedulerService, times(1)).scheduleJob(any(JobId.class), any(JobConfig.class));
        final JobRunnerResponse jobRunnerResponse = jobRunner.getValue().runJob(jobRunnerRequest);
        assertThat(jobRunnerResponse, notNullValue());
        //noinspection ConstantConditions
        assertThat(jobRunnerResponse.getRunOutcome(), equalTo(RunOutcome.SUCCESS));
        verify(indexingUpgradeService, times(1)).runUpgrades();
    }

    @Test
    public void shouldFailWhenUnableToUpgrade() throws SchedulerServiceException {

        when(indexingUpgradeService.runUpgrades()).thenReturn(new UpgradeResult("Upgrade error."));

        final UpgradeResult schedulingResult = upgradeScheduler.scheduleUpgrades(DELAY_IN_MINUTES);

        assertThat(schedulingResult.successful(), equalTo(true));
        verify(schedulerService, times(1)).registerJobRunner(any(), jobRunner.capture());
        verify(schedulerService, times(1)).scheduleJob(any(JobId.class), any(JobConfig.class));
        final JobRunnerResponse jobRunnerResponse = jobRunner.getValue().runJob(jobRunnerRequest);
        assertThat(jobRunnerResponse, notNullValue());
        //noinspection ConstantConditions
        assertThat(jobRunnerResponse.getRunOutcome(), equalTo(RunOutcome.FAILED));
        verify(indexingUpgradeService, times(1)).runUpgrades();
    }

    @Test
    public void shouldFailWhenUnableToScheduleJob() throws SchedulerServiceException {
        doThrow(new SchedulerServiceException("")).when(schedulerService).scheduleJob(any(), any());

        final UpgradeResult schedulingResult = upgradeScheduler.scheduleUpgrades(DELAY_IN_MINUTES);

        assertThat(schedulingResult.successful(), equalTo(false));
    }
}