package com.atlassian.jira.bc.filter;

import com.atlassian.fugue.Option;
import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.bc.ServiceOutcome;
import com.atlassian.jira.bc.project.property.ProjectPropertyService;
import com.atlassian.jira.entity.property.EntityPropertyImpl;
import com.atlassian.jira.entity.property.EntityPropertyService;
import com.atlassian.jira.issue.search.MockSearchRequest;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.jira.util.json.JSONArray;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;
import com.atlassian.jira.web.bean.MockI18nBean;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.mockito.Mock;

import java.sql.Timestamp;
import java.util.List;

import static com.atlassian.fugue.Iterables.first;
import static com.google.common.collect.Lists.newArrayList;
import static java.util.Collections.emptyList;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.argThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestProjectSearchRequestServiceImpl {
    @Rule
    public final RuleChain mocksInContainer = MockitoMocksInContainer.forTest(this);

    @Mock
    @AvailableInContainer
    private GlobalPermissionManager globalPermissionManager;
    @Mock
    private PermissionManager permissionManager;
    @Mock
    private ProjectPropertyService projectPropertyService;
    @Mock
    private SearchRequestService searchRequestService;

    private final I18nHelper.BeanFactory i18nFactory = new MockI18nBean.MockI18nBeanFactory();
    private final ApplicationUser admin = new MockApplicationUser("admin");
    private final ApplicationUser fred = new MockApplicationUser("fred");
    private final Project project = new MockProject(PID);
    private final SearchRequest searchRequest = new MockSearchRequest("admin", VALID_SEARCHREQUEST_ID);
    private final EntityPropertyService.SetPropertyValidationResult emptyValidationResult = emptyValidationResult();

    private static final long PID = 10230L;
    private static final long VALID_SEARCHREQUEST_ID = 10010L;
    private static final long INVALID_SEARCHREQUEST_ID = 234L;

    private ProjectSearchRequestServiceImpl service;

    @Before
    public void setUp() throws Exception {
        initGlobalAdminPermissions();
        service = new ProjectSearchRequestServiceImpl(permissionManager, projectPropertyService, i18nFactory, searchRequestService);
    }

    @Test
    public void mustBeAdminToAssociateSearchRequests() throws Exception {
        final ServiceOutcome<List<SearchRequest>> outcome = service.associateSearchRequestsWithProject(fred, project, 100023L);

        assertThat(outcome.isValid(), is(false));
        assertThat(first(outcome.getErrorCollection().getReasons()).get(), is(ErrorCollection.Reason.FORBIDDEN));
    }

    @Test
    public void invalidSearchRequestIdsAreIgnored() throws Exception {
        mockValidateAndGetProperty();

        final ServiceOutcome<List<SearchRequest>> outcome = service.associateSearchRequestsWithProject(admin, project, INVALID_SEARCHREQUEST_ID);

        assertThat(outcome.isValid(), is(true));
        assertThat(outcome.get().size(), is(0));
        verifySearchRequestIdsStoredAsProperty(emptyList());
    }

    @Test
    public void validSearchRequestIdsStored() throws Exception {
        mockValidateAndGetProperty();
        mockGetFilter();

        final ServiceOutcome<List<SearchRequest>> outcome = service.associateSearchRequestsWithProject(admin, project, VALID_SEARCHREQUEST_ID, INVALID_SEARCHREQUEST_ID);

        assertThat(outcome.isValid(), is(true));
        verifySearchRequestIdsStoredAsProperty(newArrayList(VALID_SEARCHREQUEST_ID));
    }

    @Test
    public void userMustHavePermissionToBrowseProjectToGetSearchRequests() {
        mockBrowsePermission(false);

        final List<SearchRequest> searchRequestsForProject = service.getSearchRequestsForProject(admin, project);

        assertThat(searchRequestsForProject.size(), is(0));
        verify(projectPropertyService, never()).getProperty(any(), any(Long.class), any());
    }

    @Test
    public void retrievesSearchRequestsProperly() {
        mockBrowsePermission(true);
        when(projectPropertyService.getProperty(admin, PID, "searchRequests")).thenReturn(mockPropertyResult());
        mockGetFilter();

        final List<SearchRequest> searchRequestsForProject = service.getSearchRequestsForProject(admin, project);

        assertThat(searchRequestsForProject.size(), is(1));
        assertThat(searchRequestsForProject.get(0), is(searchRequest));
    }

    private Matcher<EntityPropertyService.PropertyInput> newPropertyInputMatcher(final String expectedKey, final List<Long> expectedIds) {
        return new TypeSafeMatcher<EntityPropertyService.PropertyInput>() {
            @Override
            protected boolean matchesSafely(final EntityPropertyService.PropertyInput propertyInput) {
                try {
                    final JSONObject jsonObject = new JSONObject(propertyInput.getPropertyValue());
                    final JSONArray ids = jsonObject.getJSONArray("ids");
                    final List<Long> searchRequestIds = newArrayList();
                    for (int i = 0; i < ids.length(); i++) {
                        searchRequestIds.add(ids.getLong(i));
                    }
                    return expectedKey.equals(propertyInput.getPropertyKey()) &&
                            expectedIds.equals(searchRequestIds);
                } catch (JSONException e) {
                    throw new RuntimeException(e);
                }

            }

            @Override
            public void describeTo(final Description description) {
                description.appendValue(new EntityPropertyService.PropertyInput("{ids:[" + expectedIds.toString() + "]", expectedKey));
            }
        };
    }

    private void mockValidateAndGetProperty() {
        when(projectPropertyService.validateSetProperty(any(), any(Long.class), any())).thenReturn(emptyValidationResult);
        when(projectPropertyService.getProperty(any(), any(Long.class), any())).thenReturn(mockEmptyPropertyResult());
    }

    private void mockGetFilter() {
        when(searchRequestService.getFilter(any(JiraServiceContext.class), eq(VALID_SEARCHREQUEST_ID))).thenReturn(searchRequest);
    }

    private void verifySearchRequestIdsStoredAsProperty(final List<Long> validSearchrequestIds) {
        verify(projectPropertyService, times(1)).validateSetProperty(eq(this.admin), eq(PID), argThat(newPropertyInputMatcher("searchRequests", validSearchrequestIds)));
        verify(projectPropertyService, times(1)).setProperty(eq(this.admin), eq(emptyValidationResult));
    }

    private void mockBrowsePermission(final boolean hasPermission) {
        when(permissionManager.hasPermission(ProjectPermissions.BROWSE_PROJECTS, project, admin)).thenReturn(hasPermission);
    }

    private EntityPropertyService.PropertyResult mockPropertyResult() {
        return new EntityPropertyService.PropertyResult(new SimpleErrorCollection(), Option.option(
                EntityPropertyImpl.existing(10L, "Project", PID, "searchRequests", "{ids:[234,10010]}", new Timestamp(0), new Timestamp(0))));
    }

    private EntityPropertyService.PropertyResult mockEmptyPropertyResult() {
        return new EntityPropertyService.PropertyResult(new SimpleErrorCollection(), Option.none());
    }

    private EntityPropertyService.SetPropertyValidationResult emptyValidationResult() {
        return new EntityPropertyService.SetPropertyValidationResult(new SimpleErrorCollection(), Option.none());
    }

    private void initGlobalAdminPermissions() {
        when(globalPermissionManager.isGlobalPermission(0)).thenReturn(true);
        when(globalPermissionManager.isGlobalPermission(23)).thenReturn(false);
        when(permissionManager.hasPermission(0, admin)).thenReturn(true);
        when(permissionManager.hasPermission(0, fred)).thenReturn(false);
    }
}