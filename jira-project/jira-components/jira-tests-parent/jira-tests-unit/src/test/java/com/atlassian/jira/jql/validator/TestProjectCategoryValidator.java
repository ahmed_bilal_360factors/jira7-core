package com.atlassian.jira.jql.validator;

import com.atlassian.jira.jql.operand.JqlOperandResolver;
import com.atlassian.jira.jql.operand.QueryLiteral;
import com.atlassian.jira.jql.resolver.ProjectCategoryResolver;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.project.ProjectCategory;
import com.atlassian.jira.project.ProjectCategoryImpl;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.MessageSet;
import com.atlassian.jira.util.MessageSetImpl;
import com.atlassian.jira.util.collect.CollectionBuilder;
import com.atlassian.jira.web.bean.MockI18nBean;
import com.atlassian.query.clause.TerminalClause;
import com.atlassian.query.clause.TerminalClauseImpl;
import com.atlassian.query.operand.EmptyOperand;
import com.atlassian.query.operand.MultiValueOperand;
import com.atlassian.query.operand.Operand;
import com.atlassian.query.operand.SingleValueOperand;
import com.atlassian.query.operator.Operator;
import com.google.common.collect.Sets;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.mockito.Mock;

import static com.atlassian.jira.jql.operand.SimpleLiteralFactory.createLiteral;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertSame;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestProjectCategoryValidator {
    @Rule
    public final RuleChain mockitoMocks = MockitoMocksInContainer.forTest(this);

    @Mock
    private ProjectCategoryResolver projectCategoryResolver;

    private ApplicationUser theUser;

    private JqlOperandResolver jqlOperandResolver;

    @Before
    public void setUp() throws Exception {
        theUser = new MockApplicationUser("fred");

        jqlOperandResolver = MockJqlOperandResolver.createSimpleSupport();
    }

    @Test
    public void testValidateHappyPathEmptyLiteral() throws Exception {
        final Operand operand = EmptyOperand.EMPTY;
        final TerminalClause clause = new TerminalClauseImpl("test", Operator.IS, operand);

        final ProjectCategoryValidator projectCategoryValidator = new ProjectCategoryValidator(projectCategoryResolver, jqlOperandResolver) {
            @Override
            I18nHelper getI18n(final ApplicationUser user) {
                return new MockI18nBean();
            }
        };
        final MessageSet messageSet = projectCategoryValidator.validate(theUser, clause);
        assertFalse(messageSet.hasAnyMessages());
        verifyZeroInteractions(projectCategoryResolver);
    }

    @Test
    public void testValidateHappyPathMultipleCategories() throws Exception {
        final SingleValueOperand cat1Operand = new SingleValueOperand("cat1");
        final SingleValueOperand cat2Operand = new SingleValueOperand("cat2");
        final SingleValueOperand cat3Operand = new SingleValueOperand(123L);

        final QueryLiteral queryLiteral1 = createLiteral("cat1");
        final QueryLiteral queryLiteral2 = createLiteral("cat2");
        final QueryLiteral queryLiteral3 = createLiteral(123L);

        final MultiValueOperand operand = new MultiValueOperand(CollectionBuilder.newBuilder(cat1Operand, cat2Operand, cat3Operand).asList());
        final TerminalClause clause = new TerminalClauseImpl("test", Operator.IN, operand);

        when(projectCategoryResolver.getProjectCategory(queryLiteral1))
                .thenReturn(createMockProjectCategory(1L, "cat1"));
        when(projectCategoryResolver.getProjectCategory(queryLiteral2))
                .thenReturn(createMockProjectCategory(2L, "cat2"));
        when(projectCategoryResolver.getProjectCategory(queryLiteral3))
                .thenReturn(createMockProjectCategory(3L, "cat3"));

        final ProjectCategoryValidator projectCategoryValidator = new ProjectCategoryValidator(projectCategoryResolver, jqlOperandResolver) {
            @Override
            I18nHelper getI18n(final ApplicationUser user) {
                return new MockI18nBean();
            }
        };
        final MessageSet messageSet = projectCategoryValidator.validate(theUser, clause);
        assertFalse(messageSet.hasAnyMessages());
        verify(projectCategoryResolver).getProjectCategory(queryLiteral1);
        verify(projectCategoryResolver).getProjectCategory(queryLiteral2);
        verify(projectCategoryResolver).getProjectCategory(queryLiteral3);
    }

    @Test
    public void testErrorFindingCategoryByName() throws Exception {
        final SingleValueOperand cat1Operand = new SingleValueOperand("cat1");
        final SingleValueOperand cat2Operand = new SingleValueOperand("cat2");
        final SingleValueOperand cat3Operand = new SingleValueOperand(123L);

        final QueryLiteral queryLiteral1 = createLiteral("cat1");
        final QueryLiteral queryLiteral2 = createLiteral("cat2");
        final QueryLiteral queryLiteral3 = createLiteral(123L);

        final MultiValueOperand operand = new MultiValueOperand(CollectionBuilder.newBuilder(cat1Operand, cat2Operand, cat3Operand).asList());
        final TerminalClause clause = new TerminalClauseImpl("test", Operator.IN, operand);

        when(projectCategoryResolver.getProjectCategory(queryLiteral1))
                .thenReturn(createMockProjectCategory(1L, "cat1"));
        when(projectCategoryResolver.getProjectCategory(queryLiteral2))
                .thenReturn(null);
        when(projectCategoryResolver.getProjectCategory(queryLiteral3))
                .thenReturn(createMockProjectCategory(3L, "cat3"));

        final ProjectCategoryValidator projectCategoryValidator = new ProjectCategoryValidator(projectCategoryResolver, jqlOperandResolver) {
            @Override
            I18nHelper getI18n(final ApplicationUser user) {
                return new MockI18nBean();
            }
        };
        final MessageSet messageSet = projectCategoryValidator.validate(theUser, clause);
        assertEquals(Sets.newHashSet("The value 'cat2' does not exist for the field 'test'."), messageSet.getErrorMessages());
    }

    @Test
    public void testErrorFindingCategoryByNameFromFunction() throws Exception {
        final SingleValueOperand cat1Operand = new SingleValueOperand("cat1");
        final SingleValueOperand cat2Operand = new SingleValueOperand("cat2");
        final SingleValueOperand cat3Operand = new SingleValueOperand(123L);

        final QueryLiteral queryLiteral1 = new QueryLiteral(cat1Operand, "cat1");
        final QueryLiteral queryLiteral2 = new QueryLiteral(cat2Operand, "cat2");
        final QueryLiteral queryLiteral3 = new QueryLiteral(cat3Operand, 123L);

        final MultiValueOperand operand = new MultiValueOperand(CollectionBuilder.newBuilder(cat1Operand, cat2Operand, cat3Operand).asList());
        final TerminalClause clause = new TerminalClauseImpl("test", Operator.IN, operand);

        final JqlOperandResolver jqlOperandResolver = mock(JqlOperandResolver.class);
        when(jqlOperandResolver.getValues(theUser, operand, clause))
                .thenReturn(CollectionBuilder.newBuilder(queryLiteral1, queryLiteral2, queryLiteral3).asList());
        when(jqlOperandResolver.isFunctionOperand(cat2Operand)).thenReturn(true);

        when(projectCategoryResolver.getProjectCategory(queryLiteral1))
                .thenReturn(createMockProjectCategory(1L, "cat1"));
        when(projectCategoryResolver.getProjectCategory(queryLiteral2))
                .thenReturn(null);
        when(projectCategoryResolver.getProjectCategory(queryLiteral3))
                .thenReturn(createMockProjectCategory(3L, "cat3"));

        final ProjectCategoryValidator projectCategoryValidator = new ProjectCategoryValidator(projectCategoryResolver, jqlOperandResolver) {
            @Override
            I18nHelper getI18n(final ApplicationUser user) {
                return new MockI18nBean();
            }
        };
        final MessageSet messageSet = projectCategoryValidator.validate(theUser, clause);
        assertEquals(Sets.newHashSet("A value provided by the function 'SingleValueOperand' is invalid for the field 'test'."), messageSet.getErrorMessages());
    }

    @Test
    public void testErrorFindingCategoryById() throws Exception {
        final SingleValueOperand cat1Operand = new SingleValueOperand("cat1");
        final SingleValueOperand cat2Operand = new SingleValueOperand("cat2");
        final SingleValueOperand cat3Operand = new SingleValueOperand(123L);

        final QueryLiteral queryLiteral1 = createLiteral("cat1");
        final QueryLiteral queryLiteral2 = createLiteral("cat2");
        final QueryLiteral queryLiteral3 = createLiteral(123L);

        final MultiValueOperand operand = new MultiValueOperand(CollectionBuilder.newBuilder(cat1Operand, cat2Operand, cat3Operand).asList());
        final TerminalClause clause = new TerminalClauseImpl("test", Operator.IN, operand);

        when(projectCategoryResolver.getProjectCategory(queryLiteral1))
                .thenReturn(createMockProjectCategory(1L, "cat1"));
        when(projectCategoryResolver.getProjectCategory(queryLiteral2))
                .thenReturn(createMockProjectCategory(2L, "cat2"));
        when(projectCategoryResolver.getProjectCategory(queryLiteral3))
                .thenReturn(null);

        final ProjectCategoryValidator projectCategoryValidator = new ProjectCategoryValidator(projectCategoryResolver, jqlOperandResolver) {
            @Override
            I18nHelper getI18n(final ApplicationUser user) {
                return new MockI18nBean();
            }
        };
        final MessageSet messageSet = projectCategoryValidator.validate(theUser, clause);
        assertEquals(Sets.newHashSet("A value with ID '123' does not exist for the field 'test'."), messageSet.getErrorMessages());
    }

    @Test
    public void testErrorFindingCategoryByIdFromFunction() throws Exception {
        final SingleValueOperand cat1Operand = new SingleValueOperand("cat1");
        final SingleValueOperand cat2Operand = new SingleValueOperand("cat2");
        final SingleValueOperand cat3Operand = new SingleValueOperand(123L);

        final QueryLiteral queryLiteral1 = new QueryLiteral(cat1Operand, "cat1");
        final QueryLiteral queryLiteral2 = new QueryLiteral(cat2Operand, "cat2");
        final QueryLiteral queryLiteral3 = new QueryLiteral(cat3Operand, 123L);

        final MultiValueOperand operand = new MultiValueOperand(CollectionBuilder.newBuilder(cat1Operand, cat2Operand, cat3Operand).asList());
        final TerminalClause clause = new TerminalClauseImpl("test", Operator.IN, operand);

        final JqlOperandResolver jqlOperandResolver = mock(JqlOperandResolver.class);
        when(jqlOperandResolver.getValues(theUser, operand, clause))
                .thenReturn(CollectionBuilder.newBuilder(queryLiteral1, queryLiteral2, queryLiteral3).asList());
        when(jqlOperandResolver.isFunctionOperand(cat3Operand)).thenReturn(true);

        when(projectCategoryResolver.getProjectCategory(queryLiteral1))
                .thenReturn(createMockProjectCategory(1L, "cat1"));
        when(projectCategoryResolver.getProjectCategory(queryLiteral2))
                .thenReturn(createMockProjectCategory(2L, "cat2"));
        when(projectCategoryResolver.getProjectCategory(queryLiteral3))
                .thenReturn(null);

        final ProjectCategoryValidator projectCategoryValidator = new ProjectCategoryValidator(projectCategoryResolver, jqlOperandResolver) {
            @Override
            I18nHelper getI18n(final ApplicationUser user) {
                return new MockI18nBean();
            }
        };
        final MessageSet messageSet = projectCategoryValidator.validate(theUser, clause);
        assertEquals(Sets.newHashSet("A value provided by the function 'SingleValueOperand' is invalid for the field 'test'."), messageSet.getErrorMessages());
    }

    @Test
    public void testInvalidOperator() throws Exception {
        final MessageSetImpl messageSet = new MessageSetImpl();
        messageSet.addErrorMessage("dude");

        final TerminalClause clause = new TerminalClauseImpl("test", Operator.EQUALS, "a");

        final SupportedOperatorsValidator operatorsValidator = mock(SupportedOperatorsValidator.class);
        when(operatorsValidator.validate(theUser, clause)).thenReturn(messageSet);

        final ProjectCategoryValidator projectCategoryValidator = new ProjectCategoryValidator(projectCategoryResolver, jqlOperandResolver) {
            @Override
            SupportedOperatorsValidator getSupportedOperatorsValidator() {
                return operatorsValidator;
            }
        };

        final MessageSet foundSet = projectCategoryValidator.validate(theUser, clause);
        assertSame(messageSet, foundSet);
    }

    private ProjectCategory createMockProjectCategory(final Long id, final String name) {
        return new ProjectCategoryImpl(id, name, null);
    }
}
