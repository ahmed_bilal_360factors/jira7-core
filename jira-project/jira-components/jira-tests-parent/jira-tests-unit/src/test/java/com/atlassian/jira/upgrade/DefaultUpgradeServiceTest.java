package com.atlassian.jira.upgrade;

import com.atlassian.jira.cluster.zdu.ClusterUpgradeStateManager;
import com.atlassian.jira.util.BuildUtilsInfo;
import com.atlassian.upgrade.spi.UpgradeTaskFactory;
import com.google.common.collect.ImmutableList;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnit;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class DefaultUpgradeServiceTest {

    @Rule
    public MethodRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private IndexingUpgradeService indexingUpgradeService;

    @Mock
    private DelayedUpgradeService delayedUpgradeService;

    @Mock
    private UpgradeTaskFactory upgradeTaskFactory;

    @Mock
    private BuildUtilsInfo buildUtilsInfo;

    @Mock
    private ClusterUpgradeStateManager clusterUpgradeStateManager;


    private DefaultUpgradeService defaultUpgradeService;

    @Before
    public void setUp() {
        defaultUpgradeService = new DefaultUpgradeService(
                indexingUpgradeService,
                delayedUpgradeService,
                upgradeTaskFactory,
                buildUtilsInfo,
                clusterUpgradeStateManager
        );
    }

    @Test
    public void shouldScheduleUpgradesEvenWhenNoneArePending() {
        final AbstractUpgradeTask delayedUpgradeTask = mockUpgradeTask(3, UpgradeTask.ScheduleOption.AFTER_JIRA_STARTED);
        final AbstractUpgradeTask immediateUpgradeTask = mockUpgradeTask(2, UpgradeTask.ScheduleOption.BEFORE_JIRA_STARTED);
        when(buildUtilsInfo.getDatabaseBuildNumber()).thenReturn(3);
        when(upgradeTaskFactory.getAllUpgradeTasks()).thenReturn(ImmutableList.of(immediateUpgradeTask, delayedUpgradeTask));
        when(delayedUpgradeService.scheduleUpgrades()).thenReturn(UpgradeResult.OK);

        final UpgradeResult upgradeResult = defaultUpgradeService.runUpgrades();

        assertThat(upgradeResult, equalTo(UpgradeResult.OK));
        verify(indexingUpgradeService, never()).runUpgrades();
    }

    @Test
    public void shouldCallIndexingServiceWhenImmediateUpgradesArePending() {
        final AbstractUpgradeTask delayedUpgradeTask = mockUpgradeTask(3, UpgradeTask.ScheduleOption.AFTER_JIRA_STARTED);
        final AbstractUpgradeTask immediateUpgradeTask = mockUpgradeTask(2, UpgradeTask.ScheduleOption.BEFORE_JIRA_STARTED);
        when(buildUtilsInfo.getApplicationBuildNumber()).thenReturn(1);
        when(upgradeTaskFactory.getAllUpgradeTasks()).thenReturn(ImmutableList.of(immediateUpgradeTask, delayedUpgradeTask));
        when(indexingUpgradeService.runUpgrades()).thenReturn(UpgradeResult.OK);

        final UpgradeResult upgradeResult = defaultUpgradeService.runUpgrades();

        assertThat(upgradeResult, equalTo(UpgradeResult.OK));
        verify(indexingUpgradeService).runUpgrades();
        verify(delayedUpgradeService, never()).scheduleUpgrades();
    }

    @Test
    public void shouldScheduleUpgradesWhenOnlyDelayedUpgradesArePending() {
        final AbstractUpgradeTask delayedUpgradeTask = mockUpgradeTask(2, UpgradeTask.ScheduleOption.AFTER_JIRA_STARTED);
        when(buildUtilsInfo.getApplicationBuildNumber()).thenReturn(1);
        when(upgradeTaskFactory.getAllUpgradeTasks()).thenReturn(ImmutableList.of(delayedUpgradeTask));
        when(delayedUpgradeService.scheduleUpgrades()).thenReturn(UpgradeResult.OK);

        final UpgradeResult upgradeResult = defaultUpgradeService.runUpgrades();

        assertThat(upgradeResult, equalTo(UpgradeResult.OK));
        verify(delayedUpgradeService).scheduleUpgrades();
        verify(indexingUpgradeService, never()).runUpgrades();
    }

    @Test
    public void shouldNotRunDelayedUpgradesWhenClusterIsHandlingThem() {
        final AbstractUpgradeTask delayedUpgradeTask = mockUpgradeTask(2, UpgradeTask.ScheduleOption.AFTER_JIRA_STARTED);
        when(buildUtilsInfo.getApplicationBuildNumber()).thenReturn(1);
        when(upgradeTaskFactory.getAllUpgradeTasks()).thenReturn(ImmutableList.of(delayedUpgradeTask));
        when(clusterUpgradeStateManager.areDelayedUpgradesHandledByCluster()).thenReturn(true);

        final UpgradeResult upgradeResult = defaultUpgradeService.runUpgrades();

        assertThat(upgradeResult, equalTo(UpgradeResult.OK));
        verify(delayedUpgradeService, never()).scheduleUpgrades();
        verify(indexingUpgradeService, never()).runUpgrades();
    }

    private AbstractUpgradeTask mockUpgradeTask(final int buildNumber, final UpgradeTask.ScheduleOption scheduleOption) {
        final AbstractUpgradeTask upgradeTask = Mockito.mock(AbstractUpgradeTask.class);
        when(upgradeTask.getScheduleOption()).thenReturn(scheduleOption);
        when(upgradeTask.getBuildNumber()).thenReturn(buildNumber);

        return upgradeTask;
    }
}