package com.atlassian.jira.plugin.renderercomponent;

import com.atlassian.plugin.PluginParseException;
import com.atlassian.renderer.v2.components.phrase.PhraseRendererComponent;
import com.google.common.collect.ImmutableMap;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

/**
 * @since v3.12
 */
public class TestPhraseRendererComponentFactory {
    @Rule
    public MockitoRule mockito = MockitoJUnit.rule();
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Mock
    private RendererComponentFactoryDescriptor mockRendererComponentFactoryDescriptor;
    private PhraseRendererComponentFactory phraseRendererComponentFactory;

    @Before
    public void setUp() {
        when(mockRendererComponentFactoryDescriptor.getCompleteKey()).thenReturn("pluginkey");
        phraseRendererComponentFactory = new PhraseRendererComponentFactory();
    }

    @Test
    public void testInitMissingParam() {
        when(mockRendererComponentFactoryDescriptor.getParams()).thenReturn(ImmutableMap.of());

        expectedException.expect(PluginParseException.class);
        expectedException.expectMessage("missing the required 'phrase' parameter");
        expectedException.expectMessage("pluginkey");

        phraseRendererComponentFactory.init(mockRendererComponentFactoryDescriptor);
    }

    @Test
    public void testInitInvalidPhrase() throws PluginParseException {
        when(mockRendererComponentFactoryDescriptor.getParams()).thenReturn(ImmutableMap.of("phrase", "somephrase"));

        phraseRendererComponentFactory.init(mockRendererComponentFactoryDescriptor);

        assertNull(phraseRendererComponentFactory.getRendererComponent());
    }

    @Test
    public void testInitSuccessful() throws PluginParseException {
        when(mockRendererComponentFactoryDescriptor.getParams()).thenReturn(ImmutableMap.of("phrase", "strong"));

        phraseRendererComponentFactory.init(mockRendererComponentFactoryDescriptor);

        assertThat(phraseRendererComponentFactory.getRendererComponent(), Matchers.instanceOf(PhraseRendererComponent.class));
    }

}
