package com.atlassian.jira.util;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class TestKeyValueParser {
    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Test
    public void testNull() {
        KeyValuePair<String, String> keyValuePair = KeyValueParser.parse(null);
        assertNull(keyValuePair);
    }

    @Test
    public void testHappy() throws Exception {
        KeyValuePair<String, String> keyValuePair = KeyValueParser.parse("zed=dead");
        assertEquals("zed", keyValuePair.getKey());
        assertEquals("dead", keyValuePair.getValue());
    }

    @Test
    public void testMultiEquals() throws Exception {
        KeyValuePair<String, String> keyValuePair = KeyValueParser.parse("zed=dead=fred");
        assertEquals("zed", keyValuePair.getKey());
        assertEquals("dead=fred", keyValuePair.getValue());
    }

    @Test
    public void testIllegal() {
        assertIllegal("");
        assertIllegal(" ");
        assertIllegal("zed");
        assertIllegal("zed:dead");
    }

    private void assertIllegal(String s) {
        exception.expect(IllegalArgumentException.class);
        KeyValueParser.parse(s);
    }

}
