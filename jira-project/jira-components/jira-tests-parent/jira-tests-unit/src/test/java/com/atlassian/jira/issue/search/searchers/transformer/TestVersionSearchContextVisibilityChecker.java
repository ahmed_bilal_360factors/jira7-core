package com.atlassian.jira.issue.search.searchers.transformer;

import com.atlassian.jira.issue.search.SearchContext;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.project.version.VersionManager;
import com.atlassian.jira.util.collect.CollectionBuilder;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Set;

import static org.hamcrest.Matchers.emptyIterable;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestVersionSearchContextVisibilityChecker {
    @Rule
    public MockitoRule mockito = MockitoJUnit.rule();
    @Mock
    private VersionManager versionManager;
    @Mock
    private SearchContext searchContext;

    @Test
    public void testVisibleInContextIsVisible() throws Exception {
        final Version version1 = mock(Version.class);
        final Version version2 = mock(Version.class);

        final MockProject project1 = new MockProject(10L);
        final MockProject project2 = new MockProject(20L);

        when(searchContext.getProjectIds()).thenReturn(ImmutableList.of(10L));

        when(versionManager.getVersion(5L)).thenReturn(version1);
        when(version1.getProjectId()).thenReturn(project1.getId());

        when(versionManager.getVersion(7L)).thenReturn(version2);
        when(version2.getProjectId()).thenReturn(project2.getId());

        final VersionSearchContextVisibilityChecker checker = new VersionSearchContextVisibilityChecker(versionManager);
        final Set<String> result = checker.FilterOutNonVisibleInContext(searchContext, ImmutableList.of("5", "7", "ab"));
        assertEquals(ImmutableSet.of("5"), result);
    }

    @Test
    public void testVisibleInContextNoProjects() throws Exception {
        when(searchContext.getProjectIds()).thenReturn(ImmutableList.of());

        final VersionSearchContextVisibilityChecker checker = new VersionSearchContextVisibilityChecker(versionManager);
        final Set<String> result = checker.FilterOutNonVisibleInContext(searchContext, CollectionBuilder.newBuilder("5", "7", "ab").asList());
        assertThat(result, emptyIterable());
    }

    @Test
    public void testVisibleInContextTwoProjects() throws Exception {
        when(searchContext.getProjectIds()).thenReturn(ImmutableList.of(10L, 20L));

        final VersionSearchContextVisibilityChecker checker = new VersionSearchContextVisibilityChecker(versionManager);
        final Set<String> result = checker.FilterOutNonVisibleInContext(searchContext, CollectionBuilder.newBuilder("5", "7", "ab").asList());
        assertThat(result, emptyIterable());
    }

    @Test
    public void testVisibleInContextExcecption() throws Exception {
        when(searchContext.getProjectIds()).thenReturn(ImmutableList.of(10L));

        final VersionSearchContextVisibilityChecker checker = new VersionSearchContextVisibilityChecker(versionManager);
        final Set<String> result = checker.FilterOutNonVisibleInContext(searchContext, CollectionBuilder.newBuilder("5").asList());
        assertThat(result, emptyIterable());
    }
}
