package com.atlassian.jira.charts;

import com.atlassian.jira.issue.statistics.DateFieldSorter;
import com.atlassian.jira.issue.statistics.util.FieldableDocumentHitCollector;
import com.atlassian.jira.junit.rules.InitMockitoMocks;
import com.atlassian.jira.util.LuceneUtils;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.search.IndexSearcher;
import org.jfree.data.time.Day;
import org.jfree.data.time.RegularTimePeriod;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.mockito.Mock;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import static org.junit.Assert.assertEquals;

/**
 * @since v4.0
 */
public class TestTimeSinceChart {
    @Rule
    public TestRule initMockito = new InitMockitoMocks(this);
    @Mock
    private IndexSearcher mockSearcher;

    @Test
    public void testCollect() {
        final Map<RegularTimePeriod, Number> resolvedMap = new LinkedHashMap<>();

        final String dateFieldId = "targetDateField";

        final List<Document> docs = new ArrayList<>();
        final Date d1 = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeZone.UTC).toDate();
        final Date d2 = new DateTime(1970, 1, 2, 0, 0, 0, 0, DateTimeZone.UTC).toDate();

        final Document doc1 = new Document();
        doc1.add(new Field(dateFieldId, LuceneUtils.dateToString(d1), Field.Store.YES, Field.Index.NOT_ANALYZED));
        docs.add(doc1);

        final Document doc2 = new Document();
        doc2.add(new Field(dateFieldId, LuceneUtils.dateToString(d2), Field.Store.YES, Field.Index.NOT_ANALYZED));
        docs.add(doc2);

        final Document doc3 = new Document();
        doc3.add(new Field(dateFieldId, LuceneUtils.dateToString(d2), Field.Store.YES, Field.Index.NOT_ANALYZED));
        docs.add(doc3);

        final FieldableDocumentHitCollector documentHitCollector =
                new TimeSinceChart.GenericDateFieldIssuesHitCollector(resolvedMap, mockSearcher, DateFieldSorter.ISSUE_CREATED_STATSMAPPER,
                        Day.class, dateFieldId, TimeZone.getDefault());

        docs.forEach(documentHitCollector::collect);

        assertEquals(2, resolvedMap.size());
        final Iterator iterator = resolvedMap.values().iterator();
        assertEquals(1, iterator.next());
        assertEquals(2, iterator.next());
    }
}
