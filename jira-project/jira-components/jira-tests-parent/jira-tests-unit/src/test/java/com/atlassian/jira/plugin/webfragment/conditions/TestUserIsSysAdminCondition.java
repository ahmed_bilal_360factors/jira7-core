package com.atlassian.jira.plugin.webfragment.conditions;

import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

public class TestUserIsSysAdminCondition {
    @Rule
    public MockitoRule mockito = MockitoJUnit.rule();

    @Mock
    private GlobalPermissionManager permissionManager;

    final ApplicationUser user = new MockApplicationUser("name");
    private UserIsSysAdminCondition condition;

    @Before
    public void setUp() {
        condition = new UserIsSysAdminCondition(permissionManager);
    }

    @Test
    public void testAnonymousUserIsNotSystemAdmin() throws Exception {
        when(permissionManager.hasPermission(GlobalPermissionKey.SYSTEM_ADMIN, null)).thenReturn(false);
        assertFalse(condition.shouldDisplay(null, null));
    }

    @Test
    public void testAnonymousUserIsSystemAdmin() throws Exception {
        when(permissionManager.hasPermission(GlobalPermissionKey.SYSTEM_ADMIN, null)).thenReturn(true);
        assertTrue(condition.shouldDisplay(null, null));
    }

    @Test
    public void testUserIsNotSystemAdmin() throws Exception {
        when(permissionManager.hasPermission(GlobalPermissionKey.SYSTEM_ADMIN, user)).thenReturn(false);
        assertFalse(condition.shouldDisplay(user, null));
    }

    @Test
    public void testUserIsSystemAdmin() throws Exception {
        when(permissionManager.hasPermission(GlobalPermissionKey.SYSTEM_ADMIN, user)).thenReturn(true);
        assertTrue(condition.shouldDisplay(user, null));
    }

}
