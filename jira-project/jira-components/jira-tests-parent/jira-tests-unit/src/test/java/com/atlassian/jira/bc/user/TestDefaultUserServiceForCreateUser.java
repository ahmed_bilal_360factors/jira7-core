package com.atlassian.jira.bc.user;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.application.ApplicationRoleManager;
import com.atlassian.jira.bc.user.UserService.CreateUserRequest;
import com.atlassian.jira.bc.user.UserService.CreateUserValidationResult;
import com.atlassian.jira.crowd.embedded.ofbiz.MockDirectory;
import com.atlassian.jira.event.user.UserEvent;
import com.atlassian.jira.event.user.UserEventType;
import com.atlassian.jira.exception.AddException;
import com.atlassian.jira.exception.CreateException;
import com.atlassian.jira.exception.PermissionException;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.mock.security.MockSimpleAuthenticationContext;
import com.atlassian.jira.plugin.user.PreDeleteUserErrorsManager;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.MockGroup;
import com.atlassian.jira.user.MockUserDeleteVeto;
import com.atlassian.jira.user.UserDeleteVeto;
import com.atlassian.jira.user.UserDetails;
import com.atlassian.jira.user.util.MockUserManager;
import com.atlassian.jira.user.util.UserUtil;
import com.atlassian.jira.user.util.UserUtil.PasswordResetToken;
import com.atlassian.jira.util.ErrorCollection.Reason;
import com.atlassian.jira.util.GlobalPermissionGroupAssociationUtil;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.jira.util.SimpleWarningCollection;
import com.atlassian.jira.util.WarningCollection;
import com.atlassian.jira.web.bean.MockI18nBean;
import com.google.common.collect.ImmutableSet;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.RuleChain;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;

import java.util.Collections;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static com.atlassian.jira.user.util.UserUtilImpl.DIRECTORY_ID;
import static com.atlassian.jira.user.util.UserUtilImpl.DIRECTORY_NAME;
import static com.atlassian.jira.user.util.UserUtilImpl.DISPLAY_NAME;
import static com.atlassian.jira.user.util.UserUtilImpl.EMAIL;
import static com.atlassian.jira.user.util.UserUtilImpl.PASSWORD_HOURS;
import static com.atlassian.jira.user.util.UserUtilImpl.PASSWORD_TOKEN;
import static com.atlassian.jira.user.util.UserUtilImpl.SEND_EMAIL;
import static com.atlassian.jira.user.util.UserUtilImpl.USERNAME;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Test for createUser in UserService
 */
public class TestDefaultUserServiceForCreateUser {
    @Rule
    public final RuleChain mockContainer = MockitoMocksInContainer.forTest(this);
    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Mock
    private UserUtil userUtil;
    @Mock
    private PermissionManager permissionManager;
    @Mock
    private PreDeleteUserErrorsManager preDeleteUserErrorsManager;
    @Mock
    private CreateUserApplicationHelper applicationHelper;
    @Mock
    private ApplicationRoleManager applicationRoleManager;
    @Mock
    private UserValidationHelper validationHelper;
    @Mock
    private UserValidationHelper.Validations validations;
    final SimpleErrorCollection validationErrors = new SimpleErrorCollection();
    @Mock
    private WarningCollection validationWarnings;


    @Mock
    @AvailableInContainer
    private EventPublisher eventPublisher;
    @AvailableInContainer
    private JiraAuthenticationContext jiraAuthenticationContext = new MockSimpleAuthenticationContext(null);

    private UserDeleteVeto userDeleteVeto = new MockUserDeleteVeto();
    @Mock
    private MockUserManager userManager;
    private I18nHelper.BeanFactory i18nFactory = new MockI18nBean.MockI18nBeanFactory();

    private UserService userService;

    final Set<ApplicationKey> DEFAULT_APP_KEYS = new HashSet<>();
    @Mock
    private GlobalPermissionGroupAssociationUtil groupAccessLabelsManager;
    @Mock
    private GlobalPermissionManager globalPermissionManager;

    @Before
    public void setUp() throws Exception {
        DEFAULT_APP_KEYS.add(ApplicationKey.valueOf("default"));
        when(userManager.getDirectory(52L)).thenReturn(new MockDirectory(52L, "TestDirectory"));
        when(applicationRoleManager.getDefaultApplicationKeys()).thenReturn(DEFAULT_APP_KEYS);
        userService = new DefaultUserService(userUtil, userDeleteVeto, permissionManager, userManager,
                i18nFactory, jiraAuthenticationContext, null, preDeleteUserErrorsManager,
                applicationHelper, applicationRoleManager, validationHelper, groupAccessLabelsManager, globalPermissionManager);
        when(validationHelper.validations(anyObject())).thenReturn(validations);
        when(validations.getErrors()).thenReturn(validationErrors);
        when(validations.getWarnings()).thenReturn(validationWarnings);
    }

    @Test
    public void testNullCanNotBeProvidedToCreate() throws CreateException, PermissionException {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("You can not create a user, validation result");
        userService.createUser(null);
    }

    @Test
    public void testThatInvalidResultsDoesNotProceed() throws CreateException, PermissionException {
        thrown.expect(CreateException.class);
        thrown.expectMessage("Validation failed, user charlie cannot be created");

        UserService.CreateUserRequest userRequest = UserService.CreateUserRequest.withUserDetails(null, "charlie", null, null, null);
        final UserService.CreateUserValidationResult invalidResult = new UserService.CreateUserValidationResult(userRequest,
                Collections.emptySet(), new SimpleErrorCollection("Error", Reason.VALIDATION_FAILED),
                Collections.emptyList(), new SimpleWarningCollection());
        userService.createUser(invalidResult);
    }

    @Test
    public void testThatInvalidResultsWhenCreateUserRequestNotSet() throws CreateException, PermissionException {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Make sure to call validateCreateUser(CreateUserRequest) before calling createUser(CreateUserValidationResult).");
        final CreateUserValidationResult invalidResult = new CreateUserValidationResult(new SimpleErrorCollection());
        assertTrue(invalidResult.isValid());
        userService.createUser(invalidResult);
    }

    @Test
    public void testThatSpecifiedDirectoryGetsUsed() throws CreateException, PermissionException {

        when(userManager.getDirectory(57L)).thenReturn(new MockDirectory(57L, "TestDirectory"));
        final CreateUserRequest createUserRequest = CreateUserRequest.withUserDetails(null, "username", "password", "e@mail", "name")
                .inDirectory(57L)
                .skipValidation();
        final CreateUserValidationResult validationResult = userService.validateCreateUser(createUserRequest);

        userService.createUser(validationResult);

        ArgumentCaptor<UserDetails> argument = ArgumentCaptor.forClass(UserDetails.class);
        verify(userManager).createUser(argument.capture());
        assertThat(argument.getValue().getDirectoryId().get(), is(57L));
    }

    @Test
    public void testThatUserOnlyGetsAddedToSpecifiedApplications() throws CreateException, PermissionException {
        final ImmutableSet<ApplicationKey> applicationKeys = ImmutableSet.of(ApplicationKey.valueOf("jira-a"), ApplicationKey.valueOf("jira-b"));
        final CreateUserRequest createUserRequest = CreateUserRequest.withUserDetails(null, "username", "password", "e@mail", "name")
                .inDirectory(52L)
                .withApplicationAccess(applicationKeys)
                .skipValidation();
        final CreateUserValidationResult validationResult = userService.validateCreateUser(createUserRequest);
        when(userManager.createUser(anyObject())).thenReturn(new MockApplicationUser("testy"));

        userService.createUser(validationResult);

        verify(applicationHelper).getDefaultGroupsForNewUser(applicationKeys);
    }

    @Test
    public void testThatUserGetsAddedToDefaultApplications() throws CreateException, PermissionException {
        final ImmutableSet<ApplicationKey> applicationKeys = ImmutableSet.of(ApplicationKey.valueOf("jira-c"));
        final CreateUserRequest createUserRequest = CreateUserRequest.withUserDetails(null, "username", "password", "e@mail", "name")
                .inDirectory(52L)
                .skipValidation();
        when(applicationRoleManager.getDefaultApplicationKeys()).thenReturn(applicationKeys);
        final CreateUserValidationResult validationResult = userService.validateCreateUser(createUserRequest);
        when(userManager.createUser(anyObject())).thenReturn(new MockApplicationUser("testy"));

        userService.createUser(validationResult);

        verify(applicationHelper).getDefaultGroupsForNewUser(applicationKeys);
    }

    @Test
    public void testThatUserOnlyGetsAddedToApplicationsGroups()
            throws CreateException, PermissionException, AddException {
        final ImmutableSet<ApplicationKey> applicationKeys = ImmutableSet.of(ApplicationKey.valueOf("jira-c"));
        final CreateUserRequest createUserRequest = CreateUserRequest.withUserDetails(null, "username", "password", "e@mail", "name")
                .inDirectory(52L)
                .withApplicationAccess(applicationKeys)
                .skipValidation();
        when(applicationRoleManager.getDefaultApplicationKeys()).thenReturn(applicationKeys);
        final ImmutableSet<Group> groups = ImmutableSet.of(new MockGroup("grpA"));
        when(applicationHelper.getDefaultGroupsForNewUser(applicationKeys)).thenReturn(groups);
        final CreateUserValidationResult validationResult = userService.validateCreateUser(createUserRequest);
        final MockApplicationUser mockApplicationUser = new MockApplicationUser("testy");
        when(userManager.createUser(anyObject())).thenReturn(mockApplicationUser);

        final ApplicationUser user = userService.createUser(validationResult);

        assertThat(user, is(mockApplicationUser));

        verify(userUtil).addUserToGroups(groups, mockApplicationUser);
    }

    @Test
    public void testThatSignupEventIsSent() throws CreateException, PermissionException, AddException {
        final CreateUserRequest createUserRequest = CreateUserRequest.withUserDetails(null, "username", "password", "e@mail", "name")
                .inDirectory(52L)
                .sendUserSignupEvent()
                .skipValidation();
        final CreateUserValidationResult validationResult = userService.validateCreateUser(createUserRequest);
        final MockApplicationUser mockApplicationUser = new MockApplicationUser("testy");
        when(userManager.createUser(anyObject())).thenReturn(mockApplicationUser);

        final ApplicationUser user = userService.createUser(validationResult);

        assertThat(user, is(mockApplicationUser));

        ArgumentCaptor<UserEvent> captureUserEvent = ArgumentCaptor.forClass(UserEvent.class);
        verify(eventPublisher).publish(captureUserEvent.capture());

        UserEvent userEvent = captureUserEvent.getValue();
        assertThat(userEvent.getEventType(), is(UserEventType.USER_SIGNUP));
    }

    @Test
    public void testThatPasswordResetTokenHasBeenDispatched() throws CreateException, PermissionException, AddException {
        final CreateUserRequest createUserRequest = CreateUserRequest.withUserDetails(null, "username", "password", "e@mail", "name")
                .inDirectory(52L)
                .skipValidation();
        final CreateUserValidationResult validationResult = userService.validateCreateUser(createUserRequest);
        final MockApplicationUser mockApplicationUser = new MockApplicationUser("testy");
        when(userManager.createUser(anyObject())).thenReturn(mockApplicationUser);
        when(userManager.canUpdateUserPassword(mockApplicationUser)).thenReturn(true);
        final PasswordResetToken passwordResetToken = mock(PasswordResetToken.class);
        when(passwordResetToken.getToken()).thenReturn("T_1");
        when(passwordResetToken.getExpiryHours()).thenReturn(544);
        when(userUtil.generatePasswordResetToken(mockApplicationUser)).thenReturn(passwordResetToken);

        final ApplicationUser user = userService.createUser(validationResult);

        assertThat(user, is(mockApplicationUser));

        ArgumentCaptor<UserEvent> captureUserEvent = ArgumentCaptor.forClass(UserEvent.class);
        verify(eventPublisher).publish(captureUserEvent.capture());

        UserEvent userEvent = captureUserEvent.getValue();
        assertThat(userEvent.getParams().get(PASSWORD_TOKEN), is("T_1"));
        assertThat(userEvent.getParams().get(PASSWORD_HOURS), is(544));
    }

    @Test
    public void testThatMailWouldBeSentByDefault() throws CreateException, PermissionException, AddException {
        final CreateUserRequest createUserRequest = CreateUserRequest.withUserDetails(null, "username", "password", "e@mail", "name")
                .inDirectory(52L)
                .skipValidation();
        final CreateUserValidationResult validationResult = userService.validateCreateUser(createUserRequest);
        final MockApplicationUser mockApplicationUser = new MockApplicationUser("testy");
        when(userManager.createUser(anyObject())).thenReturn(mockApplicationUser);

        final ApplicationUser user = userService.createUser(validationResult);

        assertThat(user, is(mockApplicationUser));

        ArgumentCaptor<UserEvent> captureUserEvent = ArgumentCaptor.forClass(UserEvent.class);
        verify(eventPublisher).publish(captureUserEvent.capture());

        UserEvent userEvent = captureUserEvent.getValue();
        assertThat(userEvent.getParams().get(SEND_EMAIL), is(true));
    }

    @Test
    public void testThatMailDoesNotGetSentWhenNotificationShouldNotBeSent()
            throws CreateException, PermissionException, AddException {
        final CreateUserRequest createUserRequest = CreateUserRequest.withUserDetails(null, "username", "password", "e@mail", "name")
                .inDirectory(52L)
                .sendNotification(false)
                .skipValidation();
        final CreateUserValidationResult validationResult = userService.validateCreateUser(createUserRequest);
        final MockApplicationUser mockApplicationUser = new MockApplicationUser("testy");
        when(userManager.createUser(anyObject())).thenReturn(mockApplicationUser);

        final ApplicationUser user = userService.createUser(validationResult);

        assertThat(user, is(mockApplicationUser));

        ArgumentCaptor<UserEvent> captureUserEvent = ArgumentCaptor.forClass(UserEvent.class);
        verify(eventPublisher).publish(captureUserEvent.capture());

        UserEvent userEvent = captureUserEvent.getValue();
        assertThat(userEvent.getParams().containsKey(SEND_EMAIL), is(false));
    }

    @Test
    public void testThatDirectoryDetailsDoesNotGetSentWhenDirectoryIdNull()
            throws CreateException, PermissionException, AddException {
        final CreateUserRequest createUserRequest = CreateUserRequest.withUserDetails(null, "username", "password", "e@mail", "name")
                .inDirectory(null)
                .sendNotification(false)
                .skipValidation();
        final CreateUserValidationResult validationResult = userService.validateCreateUser(createUserRequest);
        final MockApplicationUser mockApplicationUser = new MockApplicationUser("testy");
        when(userManager.getDefaultCreateDirectory()).thenReturn(Optional.<Directory>empty());
        when(userManager.createUser(anyObject())).thenReturn(mockApplicationUser);


        final ApplicationUser user = userService.createUser(validationResult);

        assertThat(user, is(mockApplicationUser));

        ArgumentCaptor<UserEvent> captureUserEvent = ArgumentCaptor.forClass(UserEvent.class);
        verify(eventPublisher).publish(captureUserEvent.capture());

        UserEvent userEvent = captureUserEvent.getValue();
        assertThat(userEvent.getParams().containsKey(DIRECTORY_NAME), is(false));
        assertThat(userEvent.getParams().containsKey(DIRECTORY_ID), is(false));
    }

    @Test
    public void testThatDirectoryDetailsGetsSentWhenDirectoryIdSpecified()
            throws CreateException, PermissionException, AddException {
        final CreateUserRequest createUserRequest = CreateUserRequest.withUserDetails(null, "username", "password", "e@mail", "name")
                .inDirectory(62L)
                .sendNotification(false)
                .skipValidation();
        final CreateUserValidationResult validationResult = userService.validateCreateUser(createUserRequest);
        final MockApplicationUser mockApplicationUser = new MockApplicationUser("testy");
        final MockDirectory testDirectory = new MockDirectory(62L, "TestDirectory62");
        when(userManager.getDefaultCreateDirectory()).thenReturn(Optional.of(testDirectory));
        when(userManager.getDirectory(62L)).thenReturn(testDirectory);
        when(userManager.createUser(anyObject())).thenReturn(mockApplicationUser);


        final ApplicationUser user = userService.createUser(validationResult);

        assertThat(user, is(mockApplicationUser));

        ArgumentCaptor<UserEvent> captureUserEvent = ArgumentCaptor.forClass(UserEvent.class);
        verify(eventPublisher).publish(captureUserEvent.capture());

        UserEvent userEvent = captureUserEvent.getValue();
        assertThat(userEvent.getParams().get(DIRECTORY_NAME), is("TestDirectory62"));
        assertThat(userEvent.getParams().get(DIRECTORY_ID), is(62L));
    }

    @Test
    public void testThatSpecifiedEventTypeGetsSent() throws CreateException, PermissionException, AddException {
        final CreateUserRequest createUserRequest = CreateUserRequest.withUserDetails(null, "username", "password", "e@mail", "name")
                .inDirectory(52L)
                .withEventUserEvent(6546)
                .skipValidation();
        final CreateUserValidationResult validationResult = userService.validateCreateUser(createUserRequest);
        final MockApplicationUser mockApplicationUser = new MockApplicationUser("testy");
        when(userManager.createUser(anyObject())).thenReturn(mockApplicationUser);


        final ApplicationUser user = userService.createUser(validationResult);

        assertThat(user, is(mockApplicationUser));

        ArgumentCaptor<UserEvent> captureUserEvent = ArgumentCaptor.forClass(UserEvent.class);
        verify(eventPublisher).publish(captureUserEvent.capture());

        UserEvent userEvent = captureUserEvent.getValue();
        assertThat(userEvent.getEventType(), is(6546));
    }

    @Test
    public void testThatCorrectUserDetailsGetSent() throws CreateException, PermissionException, AddException {
        final CreateUserRequest createUserRequest = CreateUserRequest.withUserDetails(null, "username", "password", "e@mail", "name")
                .inDirectory(52L)
                .skipValidation();
        final CreateUserValidationResult validationResult = userService.validateCreateUser(createUserRequest);
        final MockApplicationUser mockApplicationUser = new MockApplicationUser("testy");
        when(userManager.createUser(anyObject())).thenReturn(mockApplicationUser);


        final ApplicationUser user = userService.createUser(validationResult);

        assertThat(user, is(mockApplicationUser));
        ArgumentCaptor<UserEvent> captureUserEvent = ArgumentCaptor.forClass(UserEvent.class);
        verify(eventPublisher).publish(captureUserEvent.capture());

        UserEvent userEvent = captureUserEvent.getValue();
        assertThat(userEvent.getParams().get(USERNAME), is("username"));
        assertThat(userEvent.getParams().get(EMAIL), is("e@mail"));
        assertThat(userEvent.getParams().get(DISPLAY_NAME), is("name"));

        assertThat(userEvent.getUser(), is(mockApplicationUser));
    }

    @Test
    public void testThatUserGetsCreatedButNotAddedToGroupsWhenNoAppKeys()
            throws CreateException, PermissionException, AddException {
        final CreateUserRequest createUserRequest = CreateUserRequest.withUserDetails(null, "username", "password", "e@mail", "name")
                .inDirectory(52L)
                .withApplicationAccess(ImmutableSet.of())
                .skipValidation();
        final CreateUserValidationResult validationResult = userService.validateCreateUser(createUserRequest);
        final MockApplicationUser mockApplicationUser = new MockApplicationUser("testy");
        when(applicationHelper.getDefaultGroupsForNewUser(ImmutableSet.of())).thenReturn(ImmutableSet.of());
        when(userManager.createUser(anyObject())).thenReturn(mockApplicationUser);

        final ApplicationUser user = userService.createUser(validationResult);

        assertThat(user, is(mockApplicationUser));
        verify(userUtil).addUserToGroups(ImmutableSet.<Group>of(), mockApplicationUser);
    }

    @Test
    public void userShouldBeCreatedButNotAddedToGroupsWhenValidateApplicationKeysFails()
            throws PermissionException, CreateException {
        WarningCollection warningCollection = new SimpleWarningCollection();
        when(applicationHelper.validateApplicationKeys(org.mockito.Matchers.<Optional<Long>>any(),
                any())).thenReturn(ImmutableSet.of("error")); // Has error
        when(validations.getWarnings()).thenReturn(warningCollection);
        when(validations.hasWritableDefaultCreateDirectory()).thenReturn(true);
        final ImmutableSet<ApplicationKey> applicationKeys = ImmutableSet.of(ApplicationKey.valueOf("jira-c"));
        final CreateUserRequest createUserRequest = CreateUserRequest.withUserDetails(null, "username", "password", "e@mail", "name")
                .performPermissionCheck(false);
        when(applicationRoleManager.getDefaultApplicationKeys()).thenReturn(applicationKeys);
        final CreateUserValidationResult validationResult = userService.validateCreateUser(createUserRequest);
        MockApplicationUser mockApplicationUser = new MockApplicationUser("testy");
        when(userManager.createUser(anyObject())).thenReturn(mockApplicationUser);

        ApplicationUser user = userService.createUser(validationResult);

        assertThat(user, is(mockApplicationUser));
        verify(applicationHelper, never()).getDefaultGroupsForNewUser(applicationKeys);
    }

    @Test
    public void userShouldBeCreatedAndAddedToGroupsWhenValidateApplicationKeysSucceeds()
            throws PermissionException, CreateException {
        when(applicationHelper.validateApplicationKeys(org.mockito.Matchers.<Optional<Long>>any(),
                any())).thenReturn(ImmutableSet.of()); // No errors
        when(validations.hasWritableDefaultCreateDirectory()).thenReturn(true);
        final ImmutableSet<ApplicationKey> applicationKeys = ImmutableSet.of(ApplicationKey.valueOf("jira-c"));
        final CreateUserRequest createUserRequest = CreateUserRequest.withUserDetails(null, "username", "password", "e@mail", "name")
                .performPermissionCheck(false);
        when(applicationRoleManager.getDefaultApplicationKeys()).thenReturn(applicationKeys);
        final CreateUserValidationResult validationResult = userService.validateCreateUser(createUserRequest);
        MockApplicationUser mockApplicationUser = new MockApplicationUser("testy");
        when(userManager.createUser(anyObject())).thenReturn(mockApplicationUser);

        ApplicationUser user = userService.createUser(validationResult);

        assertThat(user, is(mockApplicationUser));
        verify(applicationHelper).getDefaultGroupsForNewUser(applicationKeys);
    }

    @Test
    public void userIsCreatedWithoutBeingAddedToApplicationsWhenApplicationKeyValidationAndHasApplicationKeyValidationWarnings()
            throws PermissionException, CreateException {
        WarningCollection warningCollection = new SimpleWarningCollection();
        when(applicationHelper.validateApplicationKeys(org.mockito.Matchers.<Optional<Long>>any(),
                any())).thenReturn(ImmutableSet.of("error")); // Has error
        when(validations.getWarnings()).thenReturn(warningCollection);
        when(validations.hasWritableDefaultCreateDirectory()).thenReturn(true);
        final ImmutableSet<ApplicationKey> applicationKeys = ImmutableSet.of(ApplicationKey.valueOf("jira-c"));
        final CreateUserRequest createUserRequest = CreateUserRequest.withUserDetails(null, "username", "password", "e@mail", "name")
                .performPermissionCheck(false);
        when(applicationRoleManager.getDefaultApplicationKeys()).thenReturn(applicationKeys);
        final CreateUserValidationResult validationResult = userService.validateCreateUser(createUserRequest);
        MockApplicationUser mockApplicationUser = new MockApplicationUser("testy");
        when(userManager.createUser(anyObject())).thenReturn(mockApplicationUser);

        ApplicationUser user = userService.createUser(validationResult);

        assertThat(user, is(mockApplicationUser));
        verify(applicationHelper, never()).getDefaultGroupsForNewUser(applicationKeys);
    }

    @Test
    public void userCreationEventDispatchedAfterFailureToAddToGroups()
            throws PermissionException, CreateException, AddException {
        WarningCollection warningCollection = new SimpleWarningCollection();
        when(applicationHelper.validateApplicationKeys(org.mockito.Matchers.<Optional<Long>>any(),
                any())).thenReturn(ImmutableSet.of("error")); // Has error
        when(validations.getWarnings()).thenReturn(warningCollection);
        when(validations.hasWritableDefaultCreateDirectory()).thenReturn(true);
        doThrow(new AddException("failed to add user to groups")).when(userUtil).addUserToGroups(any(), any());
        final ImmutableSet<ApplicationKey> applicationKeys = ImmutableSet.of(ApplicationKey.valueOf("jira-c"));
        final CreateUserRequest createUserRequest = CreateUserRequest.withUserDetails(null, "userNoApps", "password", "e@mail", "name")
                .performPermissionCheck(false);
        when(applicationRoleManager.getDefaultApplicationKeys()).thenReturn(applicationKeys);
        final CreateUserValidationResult validationResult = userService.validateCreateUser(createUserRequest);
        MockApplicationUser mockApplicationUser = new MockApplicationUser("testy");
        when(userManager.createUser(anyObject())).thenReturn(mockApplicationUser);

        ApplicationUser user = userService.createUser(validationResult);

        ArgumentCaptor<UserEvent> captureUserEvent = ArgumentCaptor.forClass(UserEvent.class);
        verify(eventPublisher).publish(captureUserEvent.capture());

        UserEvent userEvent = captureUserEvent.getValue();
        assertThat(userEvent.getParams().get(USERNAME), is("userNoApps"));

        assertThat(user, is(mockApplicationUser));
        verify(applicationHelper, never()).getDefaultGroupsForNewUser(applicationKeys);
    }
}