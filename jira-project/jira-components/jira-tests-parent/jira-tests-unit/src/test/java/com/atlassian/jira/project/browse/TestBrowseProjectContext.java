package com.atlassian.jira.project.browse;

import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.jql.builder.JqlQueryBuilder;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.plugin.webfragment.model.ProjectHelper;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.query.Query;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.mock.web.MockHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestBrowseProjectContext {
    @Rule
    public final RuleChain mocksInContainer = MockitoMocksInContainer.forTest(this);

    @Mock
    @AvailableInContainer
    public SearchService searchService;

    @Test
    public void testCreateSearchQuery() throws Exception {
        BrowseProjectContext ctx = new BrowseProjectContext(null, new MockProject(100L, "STVO"));
        Query initialQuery = ctx.createQuery();
        assertEquals(JqlQueryBuilder.newBuilder().where().project("STVO").buildQuery(), initialQuery);
    }

    @Test
    public void testCreateParameters() throws Exception {
        final MockHttpServletRequest currentHttpRequest = new MockHttpServletRequest();
        final Project proj100 = new MockProject(100L, "JQL");
        final ApplicationUser admin = new MockApplicationUser("admin");

        final BrowseProjectContext ctx = new BrowseProjectContext(admin, proj100) {
            @Override
            protected HttpServletRequest getExecutingHttpRequest() {
                return currentHttpRequest;
            }
        };

        final JiraHelper helper = new ProjectHelper(currentHttpRequest, ctx);

        final Map<String, Object> params = ctx.createParameterMap();

        assertEquals(4, params.size());
        assertEquals(admin, params.get("user"));
        assertEquals(proj100, params.get("project"));
        assertEquals(helper, params.get("helper"));
        assertEquals("project", params.get("contextType"));
    }

    @Test
    public void testGetIssueSearchPath() throws Exception {
        BrowseProjectContext ctx = new BrowseProjectContext(null, new MockProject(100L, "STVO"));
        ArgumentCaptor<SearchService.IssueSearchParameters> argument = ArgumentCaptor.forClass(SearchService.IssueSearchParameters.class);
        when(searchService.getIssueSearchPath(any(), argument.capture())).thenReturn("/issues/?someJql");
        String issueSearchPath = ctx.getIssueSearchPath();
        assertEquals("/issues/?someJql", issueSearchPath);
        assertEquals("{project = \"STVO\"}", argument.getValue().query().toString());
        assertNull(argument.getValue().filterId());
    }
}
