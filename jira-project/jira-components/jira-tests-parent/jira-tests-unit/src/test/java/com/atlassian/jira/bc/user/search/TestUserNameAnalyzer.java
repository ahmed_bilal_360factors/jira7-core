package com.atlassian.jira.bc.user.search;

import com.google.common.collect.ImmutableList;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.junit.Test;

import java.io.StringReader;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;

public class TestUserNameAnalyzer {

    private final UserNameAnalyzer sut = new UserNameAnalyzer();

    @Test
    public void shouldTokenizeEmail() throws Exception {
        String email = "john.doe@localdomain.com";

        List<String> tokens = tokenize(email);

        assertThat(tokens, contains("john", "doe", "localdomain", "com"));
    }

    private List<String> tokenize(String text) throws Exception {
        ImmutableList.Builder<String> tokenListBuilder = ImmutableList.builder();
        TokenStream tokenStream = sut.tokenStream(text + "field", new StringReader(text));
        tokenStream.reset();
        while (tokenStream.incrementToken()) {
            tokenListBuilder.add(tokenStream.getAttribute(CharTermAttribute.class).toString());
        }
        tokenStream.end();
        return tokenListBuilder.build();
    }

    @Test
    public void shouldTokenizeHyphenatedName() throws Exception {
        String name = "Jos� Antonio G�mez-Iglesias";

        List<String> tokens = tokenize(name);

        assertThat(tokens, contains("jos�", "antonio", "g�mez", "iglesias"));
    }
}
