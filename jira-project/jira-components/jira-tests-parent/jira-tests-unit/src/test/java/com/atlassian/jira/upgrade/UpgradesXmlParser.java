package com.atlassian.jira.upgrade;

import com.atlassian.core.util.ClassLoaderUtils;
import electric.xml.Element;
import electric.xml.Elements;
import electric.xml.ParseException;
import org.apache.commons.io.IOUtils;

import java.io.InputStream;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 * Utility to parse our "upgrades.xml" file for usage in Unit Tests.
 *
 * @since v7.0
 */
public class UpgradesXmlParser {
    private static final String UPGRADES_XML = "upgrades.xml";

    /**
     * Parses the upgrade build numbers out of the upgrades.xml file.
     *
     * @return set of upgrade task build numbers sorted in ascending order
     */
    public static SortedSet<Integer> getUpgradeTaskNumbers() {
        SortedSet<Integer> sortedTasks = new TreeSet<>();

        final InputStream is = ClassLoaderUtils.getResourceAsStream(UPGRADES_XML, UpgradesXmlParser.class);
        try {
            final electric.xml.Document doc = new electric.xml.Document(is);
            final Element root = doc.getRoot();
            final Elements actions = root.getElements("upgrade");

            while (actions.hasMoreElements()) {
                final Element action = (Element) actions.nextElement();
                final String buildNo = action.getAttribute("build");

                sortedTasks.add(Integer.parseInt(buildNo));
            }
        } catch (ParseException e) {
            throw new RuntimeException(e);
        } finally {
            IOUtils.closeQuietly(is);
        }
        return sortedTasks;
    }
}
