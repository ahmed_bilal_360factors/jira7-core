package com.atlassian.jira.auditing;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import org.junit.Test;

import java.util.Arrays;

import static com.atlassian.jira.auditing.AuditingCategory.getCategoryByIdOrName;
import static org.junit.Assert.assertEquals;


public class TestAuditingCategory {
    @Test
    public void testEnumSynchronizedWithStable() throws Exception {
        final ImmutableList<String> expectedNames = ImmutableList.of(
                "AUDITING",
                "USER_MANAGEMENT",
                "GROUP_MANAGEMENT",
                "PERMISSIONS",
                "WORKFLOWS",
                "NOTIFICATIONS",
                "FIELDS",
                "PROJECTS",
                "SYSTEM",
                "MIGRATION",
                "APPLICATIONS");
        final ImmutableList<String> actualNames = ImmutableList.copyOf(
                Iterables.transform(
                        Arrays.asList(AuditingCategory.values()),
                        Enum::name));
        assertEquals("ACHTUNG! Any additions to AuditingCategory enum should be BACK PORTED to stable "
                        + "or otherwise there are problems downgrading from OnDemand - JRA-38083!"
                        + " (WARNING! It is not enough just to add new token to list above, you "
                        + " must go and change enum on current stable branch. Now!)",
                expectedNames, actualNames);
    }

    @Test
    public void getCategoryByIdOrNameCorrectlyParsesIdsAndNames() {
        for (AuditingCategory cat : AuditingCategory.values()) {
            assertEqualsByIdAndName(cat);
        }

        assertEqualsByIdAndName(AuditingCategory.AUDITING);
        assertEqualsByIdAndName(AuditingCategory.USER_MANAGEMENT);
        assertEqualsByIdAndName(AuditingCategory.GROUP_MANAGEMENT);
        assertEqualsByIdAndName(AuditingCategory.PERMISSIONS);
        assertEqualsByIdAndName(AuditingCategory.WORKFLOWS);
        assertEqualsByIdAndName(AuditingCategory.NOTIFICATIONS);
        assertEqualsByIdAndName(AuditingCategory.FIELDS);
        assertEqualsByIdAndName(AuditingCategory.PROJECTS);
        assertEqualsByIdAndName(AuditingCategory.SYSTEM);
        assertEqualsByIdAndName(AuditingCategory.MIGRATION);
        assertEqualsByIdAndName(AuditingCategory.APPLICATIONS);
    }

    private void assertEqualsByIdAndName(AuditingCategory category) {
        assertEquals(category.toString() + " did not parse ID correctly", category, getCategoryByIdOrName(category.getId()));
        assertEquals(category.toString() + " did not parse name correctly", category, getCategoryByIdOrName(category.name()));
    }
}

