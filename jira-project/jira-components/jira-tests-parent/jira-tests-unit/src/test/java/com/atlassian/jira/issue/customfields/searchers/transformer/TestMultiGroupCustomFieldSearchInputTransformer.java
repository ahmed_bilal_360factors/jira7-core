package com.atlassian.jira.issue.customfields.searchers.transformer;

import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.jira.issue.customfields.converters.GroupConverter;
import com.atlassian.jira.issue.customfields.impl.FieldValidationException;
import com.atlassian.jira.issue.customfields.view.CustomFieldParams;
import com.atlassian.jira.issue.customfields.view.CustomFieldParamsImpl;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.search.ClauseNames;
import com.atlassian.jira.issue.search.SearchContext;
import com.atlassian.jira.issue.transport.FieldValuesHolder;
import com.atlassian.jira.issue.transport.impl.FieldValuesHolderImpl;
import com.atlassian.jira.jql.operand.JqlOperandResolver;
import com.atlassian.jira.jql.operand.QueryLiteral;
import com.atlassian.jira.jql.validator.MockJqlOperandResolver;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.collect.CollectionBuilder;
import com.atlassian.jira.util.collect.MapBuilder;
import com.atlassian.query.Query;
import com.atlassian.query.QueryImpl;
import com.atlassian.query.clause.Clause;
import com.atlassian.query.clause.TerminalClause;
import com.atlassian.query.clause.TerminalClauseImpl;
import com.atlassian.query.operand.MultiValueOperand;
import com.atlassian.query.operand.SingleValueOperand;
import com.atlassian.query.operator.Operator;
import com.google.common.collect.ImmutableList;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import static com.atlassian.jira.jql.operand.SimpleLiteralFactory.createLiteral;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestMultiGroupCustomFieldSearchInputTransformer {
    @Rule
    public final MethodRule initMockito = MockitoJUnit.rule();

    @Mock
    private CustomField customField;
    @Mock
    private GroupConverter groupConverter;
    @Mock
    private SearchContext searchContext;
    @Mock
    private CustomFieldInputHelper customFieldInputHelper;

    private JqlOperandResolver jqlOperandResolver;

    private final String url = "cf_100";
    private final ClauseNames clauseNames = new ClauseNames("cf[100]");
    private ApplicationUser theUser = null;

    private MultiGroupCustomFieldSearchInputTransformer transformer;

    @Before
    public void setUp() throws Exception {
        when(customField.getId()).thenReturn(url);
        jqlOperandResolver = MockJqlOperandResolver.createSimpleSupport();
        transformer = new MultiGroupCustomFieldSearchInputTransformer(url, clauseNames, customField, jqlOperandResolver, customFieldInputHelper, groupConverter);
    }

    @Test
    public void testGetSearchClauseNoValues() throws Exception {
        FieldValuesHolder holder = new FieldValuesHolderImpl();
        assertNull(transformer.getSearchClause(null, holder));
    }

    @Test
    public void testGetSearchClauseEmptyParams() throws Exception {
        final CustomFieldParamsImpl params = new CustomFieldParamsImpl();
        FieldValuesHolder holder = new FieldValuesHolderImpl(MapBuilder.newBuilder(url, params).toMap());
        assertNull(transformer.getSearchClause(null, holder));
    }

    @Test
    public void testGetSearchClauseParamsHasOnlyInvalidValues() throws Exception {
        final CustomFieldParamsImpl params = new CustomFieldParamsImpl(customField, CollectionBuilder.newBuilder("").asCollection());
        FieldValuesHolder holder = new FieldValuesHolderImpl(MapBuilder.newBuilder(url, params).toMap());
        assertNull(transformer.getSearchClause(null, holder));
    }

    @Test
    public void testGetSearchClauseParamsHappyPath() throws Exception {
        when(customField.getUntranslatedName()).thenReturn("ABC");
        when(customFieldInputHelper.getUniqueClauseName(theUser, clauseNames.getPrimaryName(), "ABC")).thenReturn(clauseNames.getPrimaryName());

        final CustomFieldParamsImpl params = new CustomFieldParamsImpl(customField, CollectionBuilder.newBuilder("-1", "", "nick", "ross").asCollection());
        FieldValuesHolder holder = new FieldValuesHolderImpl(MapBuilder.newBuilder(url, params).toMap());
        final Clause result = transformer.getSearchClause(null, holder);
        final TerminalClause expectedResult = new TerminalClauseImpl(clauseNames.getPrimaryName(), Operator.IN, new MultiValueOperand("nick", "ross"));
        assertEquals(expectedResult, result);
    }

    @Test
    public void testGetParamsFromSearchRequestNoWhereClause() throws Exception {
        Query query = new QueryImpl();
        assertNull(transformer.getParamsFromSearchRequest(null, query, searchContext));
    }

    @Test
    public void testGetParamsFromSearchRequestNoContext() throws Exception {
        Query query = new QueryImpl(new TerminalClauseImpl(clauseNames.getPrimaryName(), Operator.EQUALS, "blah"));
        when(groupConverter.getGroup("blah")).thenReturn(createGroup("blah"));

        final CustomFieldParams paramsFromSearchRequest = transformer.getParamsFromSearchRequest(null, query, searchContext);
        assertEquals(ImmutableList.of("blah"), paramsFromSearchRequest.getAllValues());
    }

    @Test
    public void testGetParamsFromSearchRequestUnsupportedOperators() throws Exception {
        Query query0 = new QueryImpl(new TerminalClauseImpl(clauseNames.getPrimaryName(), Operator.IS, "x"));
        Query query1 = new QueryImpl(new TerminalClauseImpl(clauseNames.getPrimaryName(), Operator.IS_NOT, "x"));
        Query query2 = new QueryImpl(new TerminalClauseImpl(clauseNames.getPrimaryName(), Operator.NOT_EQUALS, "x"));
        Query query3 = new QueryImpl(new TerminalClauseImpl(clauseNames.getPrimaryName(), Operator.LESS_THAN_EQUALS, "x"));
        Query query4 = new QueryImpl(new TerminalClauseImpl(clauseNames.getPrimaryName(), Operator.NOT_IN, new MultiValueOperand("x", "y")));

        assertNull(transformer.getParamsFromSearchRequest(null, query0, searchContext));
        assertNull(transformer.getParamsFromSearchRequest(null, query1, searchContext));
        assertNull(transformer.getParamsFromSearchRequest(null, query2, searchContext));
        assertNull(transformer.getParamsFromSearchRequest(null, query3, searchContext));
        assertNull(transformer.getParamsFromSearchRequest(null, query4, searchContext));
    }

    @Test
    public void testGetParamsFromSearchRequestHappyPath() throws Exception {
        final QueryLiteral literal1 = createLiteral("value1");
        final QueryLiteral literal2 = createLiteral("value2");

        Query query = new QueryImpl(new TerminalClauseImpl(clauseNames.getPrimaryName(), Operator.IN, new MultiValueOperand(literal1, literal2)));

        when(groupConverter.getGroup("value1")).thenReturn(createGroup("value1"));
        when(groupConverter.getGroup("value2")).thenReturn(createGroup("value2"));

        final CustomFieldParams result = transformer.getParamsFromSearchRequest(null, query, searchContext);
        final CustomFieldParams expectedResult = new CustomFieldParamsImpl(customField, CollectionBuilder.newBuilder("value1", "value2").asList());
        assertEquals(expectedResult, result);
    }

    @Test
    public void testGetParamsFromSearchRequestNullLiterals() throws Exception {
        final SingleValueOperand operand = new SingleValueOperand("value1");
        final TerminalClauseImpl clause = new TerminalClauseImpl(clauseNames.getPrimaryName(), Operator.IN, operand);
        Query query = new QueryImpl(clause);

        jqlOperandResolver = mock(JqlOperandResolver.class);

        final MultiGroupCustomFieldSearchInputTransformer transformer = new MultiGroupCustomFieldSearchInputTransformer(url, clauseNames, customField, jqlOperandResolver, customFieldInputHelper, groupConverter);

        final CustomFieldParams result = transformer.getParamsFromSearchRequest(theUser, query, searchContext);
        assertNull(result);
        verify(jqlOperandResolver).getValues(theUser, operand, clause);
    }

    @Test
    public void testGetParamsFromSearchRequestNoGroups() throws Exception {
        final QueryLiteral literal1 = createLiteral("value1");
        final QueryLiteral literal2 = createLiteral("value2");

        final Query query = new QueryImpl(new TerminalClauseImpl(clauseNames.getPrimaryName(), Operator.IN, new MultiValueOperand(literal1, literal2)));

        final CustomFieldParams result = transformer.getParamsFromSearchRequest(null, query, searchContext);
        assertNull(result);
    }

    @Test
    public void testGetParamsFromSearchRequestErrorGroups() throws Exception {
        final QueryLiteral literal1 = createLiteral("value1");
        final QueryLiteral literal2 = createLiteral("value2");

        Query query = new QueryImpl(new TerminalClauseImpl(clauseNames.getPrimaryName(), Operator.IN, new MultiValueOperand(literal1, literal2)));

        when(groupConverter.getGroup("value1")).thenThrow(new FieldValidationException("bad"));
        when(groupConverter.getGroup("value2")).thenThrow(new FieldValidationException("bad"));

        final CustomFieldParams result = transformer.getParamsFromSearchRequest(null, query, searchContext);
        assertNull(result);
    }

    private Group createGroup(final String groupName) {
        return new Group() {
            @Override
            public String getName() {
                return groupName;
            }

            @Override
            public boolean equals(Object o) {
                if (o != null && o instanceof Group) {
                    return groupName.equals(((Group) o).getName());
                }

                return false;
            }

            @Override
            public int hashCode() {
                return groupName.hashCode();
            }

            @Override
            public int compareTo(Group o) {
                if (o == null) {
                    return 1;
                }
                return groupName.compareTo(o.getName());
            }
        };
    }

}
