package com.atlassian.jira.scheduler;

import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.config.properties.JiraProperties;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockComponentContainer;
import com.google.common.collect.ImmutableMap;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.IOException;
import java.io.Serializable;
import java.util.Map;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.when;

/*
 * @since v7.1.0
 */
@RunWith(MockitoJUnitRunner.class)
public class JiraParameterMapSerializerTest {

    @Rule
    public MockComponentContainer container = new MockComponentContainer(this);

    @AvailableInContainer
    @Mock
    private FeatureManager featureManager;

    @Mock
    private JiraProperties jiraProperties;

    private JiraParameterMapSerializer jiraParameterMapSerializer;

    private ClassLoader classLoader = Thread.currentThread().getContextClassLoader();

    @Before
    public void setUp() {
        jiraParameterMapSerializer = new JiraParameterMapSerializer();
    }

    @Test
    public void testJiraPropertyDisablesRestriction() throws Exception {
        when(jiraProperties.getBoolean(JiraParameterMapSerializer.JIRA_PARAMETER_MAP_SERIALIZER_DISABLE_RESTRICTIVE_CLASS_LOADER)).thenReturn(true);
        checkPermitted(new ForbiddenSerializable());
    }

    @Test
    public void testBTFDoesntRestrict() throws Exception {
        checkPermitted(new ForbiddenSerializable());
    }

    @Test
    public void testSerializerHandlesOldQuartzClasses() {
        String hexBytes = "aced0005737200156f72672e71756172747a2e4a6f62446174614d61709fb083e8bfa9b0cb020000787200266f72672e71756172747a2e7574696c732e537472696e674b65794469727479466c61674d61708208e8c3fbc55d280200015a0013616c6c6f77735472616e7369656e74446174617872001d6f72672e71756172747a2e7574696c732e4469727479466c61674d617013e62ead28760ace0200025a000564697274794c00036d617074000f4c6a6176612f7574696c2f4d61703b787001737200116a6176612e7574696c2e486173684d61700507dac1c31660d103000246000a6c6f6164466163746f724900097468726573686f6c6478703f4000000000000c7708000000100000000174000a706172616d6574657273757200025b42acf317f8060854e002000078700000014aaced000573720037636f6d2e676f6f676c652e636f6d6d6f6e2e636f6c6c6563742e496d6d757461626c6542694d61702453657269616c697a6564466f726d000000000000000002000078720035636f6d2e676f6f676c652e636f6d6d6f6e2e636f6c6c6563742e496d6d757461626c654d61702453657269616c697a6564466f726d00000000000000000200025b00046b6579737400135b4c6a6176612f6c616e672f4f626a6563743b5b000676616c75657371007e00027870757200135b4c6a6176612e6c616e672e4f626a6563743b90ce589f1073296c02000078700000000174000c4449524543544f52595f49447571007e0004000000017372000e6a6176612e6c616e672e4c6f6e673b8be490cc8f23df0200014a000576616c7565787200106a6176612e6c616e672e4e756d62657286ac951d0b94e08b020000787000000000000027107800";
        byte[] bytes = convertHexStringToByteArray(hexBytes);
        try {
            Map<String, Serializable> output = jiraParameterMapSerializer.deserializeParameters(classLoader, bytes);
            assertThat(output, notNullValue());
            assertThat(output.size(), is(1));
            assertThat(output.get("DIRECTORY_ID"), is(10000L));
        } catch (ClassNotFoundException e) {
            fail("Unexpected exception " + e);
        } catch (IOException e) {
            fail("Unexpected exception " + e);
        }
    }

    private static byte[] convertHexStringToByteArray(String s) throws IllegalArgumentException {
        try {
            int len = s.length();
            byte[] data = new byte[len / 2];
            for (int i = 0; i < len; i += 2) {
                data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                        + Character.digit(s.charAt(i + 1), 16));
            }
            return data;
        } catch (Exception e) {
            throw new IllegalArgumentException(s);
        }
    }

    private void checkForbidden(Serializable obj) {
        try {
            checkSerialization(obj);
            fail("Deserialization of " + obj.getClass() + " should have failed");
        } catch (ClassNotFoundException cnfe) {
            // success
        } catch (UnsupportedOperationException uoe) {
            // InvokerTransformer can't be serialized and throws this exception
        } catch (Exception e) {
            fail("Unexpected exception " + e);
        }
    }

    private void checkPermitted(Serializable obj) {
        try {
            Object output = checkSerialization(obj);
            assertThat(output, instanceOf(obj.getClass()));
        } catch (Exception e) {
            fail("Unexpected exception " + e);
        }
    }

    private Object checkSerialization(Serializable obj) throws Exception {
        Map<String, Serializable> input = ImmutableMap.of("key1", obj);
        byte[] bytes = jiraParameterMapSerializer.serializeParameters(input);
        Map<String, Serializable> output = jiraParameterMapSerializer.deserializeParameters(classLoader, bytes);
        assertThat(output.get("key1"), notNullValue());
        return output.get("key1");
    }

    private static final class ForbiddenSerializable implements Serializable {
    }
}
