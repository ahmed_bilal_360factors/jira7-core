package com.atlassian.jira.application;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.cache.CacheManager;
import com.atlassian.cache.memory.MemoryCacheManager;
import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.crowd.embedded.spi.DirectoryDao;
import com.atlassian.crowd.exception.CrowdException;
import com.atlassian.crowd.manager.directory.DirectoryManager;
import com.atlassian.crowd.manager.directory.SynchronisationStatusManager;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.config.database.DatabaseConfig;
import com.atlassian.jira.config.database.JndiDatasource;
import com.atlassian.jira.config.database.MockDatabaseConfigurationManager;
import com.atlassian.jira.crowd.embedded.ofbiz.InternalMembershipDao;
import com.atlassian.jira.crowd.embedded.ofbiz.OfBizUserDao;
import com.atlassian.jira.crowd.embedded.ofbiz.db.DefaultOfBizTransactionManager;
import com.atlassian.jira.database.DbConnectionManager;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.license.JiraLicenseManager;
import com.atlassian.jira.license.LicenseDetails;
import com.atlassian.jira.license.MockLicenseDetails;
import com.atlassian.jira.license.MockLicensedApplications;
import com.atlassian.jira.security.groups.DefaultGroupManager;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.security.groups.MockGroupManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.MockCrowdService;
import com.atlassian.jira.user.MockGroup;
import com.atlassian.jira.user.util.DisabledRecoveryMode;
import com.atlassian.jira.user.util.UserManager;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.mockito.Mock;
import org.mockito.stubbing.Answer;

import java.util.HashSet;
import java.util.Set;
import java.util.function.Consumer;

import static java.util.stream.Collectors.toSet;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

/**
 * Tests {@link com.atlassian.jira.application.DefaultApplicationRoleManager}.
 *
 * @see com.atlassian.jira.application.DefaultApplicationRoleManagerTest
 * @see com.atlassian.jira.application.DefaultApplicationRoleManagerUserCountTest
 * @see com.atlassian.jira.application.DefaultApplicationRoleManagerUserLimitTest
 */
public class DefaultApplicationRoleManagerGroupTest {
    @Rule
    public RuleChain initContainerMocks = MockitoMocksInContainer.forTest(this);

    @Mock
    FeatureManager featureManager;
    @Mock
    JiraLicenseManager licenseManager;
    @Mock
    DirectoryManager directoryManager;
    @Mock
    EventPublisher eventPublisher;
    @Mock
    UserManager userManager;
    @Mock
    InternalMembershipDao internalMembershipDao;
    @Mock
    DirectoryDao directoryDao;
    @Mock
    DbConnectionManager dbConnectionManager;
    @Mock
    SynchronisationStatusManager crowdSyncStatusManager;

    private MockApplicationRoleDefinitions definitions = new MockApplicationRoleDefinitions();

    private MockGroupManager groupManager = new MockGroupManager();
    private MockApplicationRoleStore applicationRoleStore = new MockApplicationRoleStore();
    private CacheManager cacheManager = new MemoryCacheManager();
    private MockCrowdService crowdService = new MockCrowdService();
    private OfBizUserDao ofBizUserDao;

    private Set<LicenseDetails> licenses = new HashSet<>();
    private Set<ApplicationKey> licensedApplications = new HashSet<>();

    private Group parentAppGroup, childAppGroup, otherChildGroup;
    private ApplicationKey permissiveApp = ApplicationKey.valueOf("com.test.application.one");
    private ApplicationKey restrictedApp = ApplicationKey.valueOf("com.test.application.two");

    private ApplicationRoleManager applicationRoleManager;

    @Before
    public void setup() {
        final MockDatabaseConfigurationManager databaseConfigurationManager = new MockDatabaseConfigurationManager();
        databaseConfigurationManager.setDatabaseConfiguration(new DatabaseConfig("postgres", "jira", new JndiDatasource("jira")));
        ofBizUserDao = new OfBizUserDao(null, directoryDao, null, null, null, cacheManager, null, null, new DefaultOfBizTransactionManager(), databaseConfigurationManager) {
            @Override
            public boolean useFullCache() {
                return false;
            }

            @Override
            public void processUsers(Consumer userProcessor) {
                userManager.getAllUsers().forEach(appUser -> userProcessor.accept(appUser.getDirectoryUser()));
            }
        };

        // mocks to return accumulated licenses & application keys
        when(licenseManager.getLicenses()).thenReturn(licenses);
        when(licenseManager.getAllLicensedApplicationKeys()).thenReturn(licensedApplications);

        // setup 3 nested groups with 2 apps:
        //
        //           parentAppGroup <-- permissiveApp
        //            /        \
        //  otherChildGroup  childAppGroup <-- restrictedApp
        //
        setupApplicationsWithNestedGroups();
    }

    @Test
    public void getDefaultReturnsDefaultGroups() {
        applicationRoleManager = new DefaultApplicationRoleManager(cacheManager, applicationRoleStore, definitions,
                groupManager, licenseManager, new DisabledRecoveryMode(), crowdService,
                eventPublisher, internalMembershipDao, directoryDao, ofBizUserDao, new DefaultOfBizTransactionManager(),
                crowdSyncStatusManager, featureManager);

        ApplicationKey app = ApplicationKey.valueOf("com.test.application");
        Set<Group> groups = ImmutableSet.of(groupManager.createGroup("group-a"), groupManager.createGroup("group-b"));

        setupApplication(app, 20, groups);

        assertThat(applicationRoleManager.getDefaultGroups(app), equalTo(groups));
    }

    @Test
    public void getDefaultReturnsDefaultGroupsWhenUsingNestedGroups() {
        assertThat(applicationRoleManager.getDefaultGroups(permissiveApp), equalTo(ImmutableSet.of(parentAppGroup)));
        assertThat(applicationRoleManager.getDefaultGroups(restrictedApp), equalTo(ImmutableSet.of(childAppGroup)));
    }

    @Test
    public void getRolesForGroupWorksForNestedGroups() {
        //           parentAppGroup <-- permissiveApp
        //            /        \
        //  otherChildGroup  childAppGroup <-- restrictedApp
        assertThat(applicationsFor(parentAppGroup), equalTo(ImmutableSet.of(permissiveApp)));
        assertThat(applicationsFor(otherChildGroup), equalTo(ImmutableSet.of(permissiveApp)));
        assertThat(applicationsFor(childAppGroup), equalTo(ImmutableSet.of(permissiveApp, restrictedApp)));
    }

    @Test
    public void getRolesForUserWorksForNestedGroups() throws CrowdException {
        when(directoryManager.isUserNestedGroupMember(anyLong(), anyString(), anyString()))
                .thenAnswer(withCrowdServiceLookup);

        // add some users
        MockApplicationUser user1 = new MockApplicationUser("user1");
        MockApplicationUser user2 = new MockApplicationUser("user2");
        MockApplicationUser user3 = new MockApplicationUser("user3");
        crowdService.addUserToGroup(user1, parentAppGroup);
        crowdService.addUserToGroup(user2, otherChildGroup);
        crowdService.addUserToGroup(user3, childAppGroup);

        // sanity check user-group relations
        assertThat(crowdService.isUserMemberOfGroup("user1", "parentAppGroup"), equalTo(true));
        assertThat(crowdService.isUserMemberOfGroup("user1", "childAppGroup"), equalTo(false));
        assertThat(crowdService.isUserMemberOfGroup("user3", "parentAppGroup"), equalTo(true));

        //           parentAppGroup <-- permissiveApp
        //            /        \
        //  otherChildGroup  childAppGroup <-- restrictedApp
        assertThat(applicationsFor(user1), equalTo(ImmutableSet.of(permissiveApp)));
        assertThat(applicationsFor(user2), equalTo(ImmutableSet.of(permissiveApp)));
        assertThat(applicationsFor(user3), equalTo(ImmutableSet.of(permissiveApp, restrictedApp)));
    }

    @Test
    public void hasAnyRoleWorksForNestedGroups() throws CrowdException {
        when(directoryManager.isUserNestedGroupMember(anyLong(), anyString(), anyString()))
                .thenAnswer(withCrowdServiceLookup);

        MockApplicationUser user1 = new MockApplicationUser("user1");
        MockApplicationUser user2 = new MockApplicationUser("user2");
        MockApplicationUser user3 = new MockApplicationUser("user3");
        MockApplicationUser user4 = new MockApplicationUser("user4");
        crowdService.addUserToGroup(user1, parentAppGroup);
        crowdService.addUserToGroup(user2, otherChildGroup);
        crowdService.addUserToGroup(user3, childAppGroup);
        crowdService.addUserToGroup(user4, new MockGroup("with-no-apps"));

        // sanity check groups
        assertThat(applicationRoleManager.getGroupsForLicensedRoles(),
                equalTo(ImmutableSet.of(parentAppGroup, childAppGroup)));

        //           parentAppGroup <-- permissiveApp
        //            /        \
        //  otherChildGroup  childAppGroup <-- restrictedApp
        assertThat(applicationRoleManager.hasAnyRole(user1), equalTo(true));
        assertThat(applicationRoleManager.hasAnyRole(user2), equalTo(true));
        assertThat(applicationRoleManager.hasAnyRole(user3), equalTo(true));
        assertThat(applicationRoleManager.hasAnyRole(user4), equalTo(false));

        // now grant a role to user4
        crowdService.addUserToGroup(user4, otherChildGroup);
        assertThat(applicationRoleManager.hasAnyRole(user4), equalTo(true));
    }

    /**
     * Sets up 3 nested groups with 2 apps.
     * <pre>
     *
     *           parentAppGroup <-- permissiveApp
     *            /        \
     *  otherChildGroup  childAppGroup <-- restrictedApp
     *
     * </pre>
     */
    private void setupApplicationsWithNestedGroups() {
        // create a ApplicationRoleManager capable of querying nested groups
        GroupManager groupManager = new DefaultGroupManager(crowdService, directoryManager, dbConnectionManager, null, null, null);
        applicationRoleManager = new DefaultApplicationRoleManager(cacheManager, applicationRoleStore, definitions,
                groupManager, licenseManager, new DisabledRecoveryMode(), crowdService,
                eventPublisher, internalMembershipDao, directoryDao, ofBizUserDao, new DefaultOfBizTransactionManager(),
                crowdSyncStatusManager, featureManager);

        // setup nested groups
        parentAppGroup = crowdService.addGroup(new MockGroup("parentAppGroup"));
        otherChildGroup = crowdService.addGroup(new MockGroup("otherChildGroup"));
        childAppGroup = crowdService.addGroup(new MockGroup("childAppGroup"));
        crowdService.addGroupToGroup(/*child*/ otherChildGroup, /*parent*/ parentAppGroup);
        crowdService.addGroupToGroup(/*child*/ childAppGroup, /*parent*/ parentAppGroup);

        // sanity check group relations
        assertThat(groupManager.getAllGroups(), hasItems(parentAppGroup, otherChildGroup, childAppGroup));
        assertThat(crowdService.isGroupMemberOfGroup(otherChildGroup, parentAppGroup), equalTo(true));
        assertThat(crowdService.isGroupMemberOfGroup(childAppGroup, parentAppGroup), equalTo(true));
        assertThat(crowdService.isGroupMemberOfGroup(parentAppGroup, otherChildGroup), equalTo(false));

        // setup 2 applications (with roles): app1 -> parentAppGroup, app2 -> childAppGroup
        setupApplication(permissiveApp, 20, ImmutableSet.of(parentAppGroup));
        setupApplication(restrictedApp, 20, ImmutableSet.of(childAppGroup));
        assertThat(applicationRoleManager.getRoles().size(), equalTo(2));
    }

    private ApplicationRole setupApplication(ApplicationKey applicationId, int numSeats, Set<Group> groups) {
        //Make sure the application is licensed.
        definitions.addLicensed(new MockApplicationRoleDefinition(applicationId.value()));
        final MockLicenseDetails details = new MockLicenseDetails();
        details.setLicensedApplications(new MockLicensedApplications(ImmutableMap.of(applicationId, numSeats)));
        boolean newLicenseWasAdded = licenses.add(details);
        assert newLicenseWasAdded;

        // add the application to the set of known applications
        licensedApplications.add(applicationId);

        // create the role & store
        MockApplicationRole role = new MockApplicationRole(applicationId)
                .defaultGroups(groups)
                .groups(groups);

        applicationRoleStore.save(role);

        return role;
    }

    private Answer<Boolean> withCrowdServiceLookup = args -> // directoryId, userName, groupName
    {
        String userName = (String) args.getArguments()[1];
        String groupName = (String) args.getArguments()[2];
        return crowdService.isUserMemberOfGroup(userName, groupName);
    };

    private Set<ApplicationKey> applicationsFor(Group g) {
        return applicationRoleManager.getRolesForGroup(g).stream().map(ApplicationRole::getKey).collect(toSet());
    }

    private Set<ApplicationKey> applicationsFor(ApplicationUser u) {
        return applicationRoleManager.getRolesForUser(u).stream().map(ApplicationRole::getKey).collect(toSet());
    }
}
