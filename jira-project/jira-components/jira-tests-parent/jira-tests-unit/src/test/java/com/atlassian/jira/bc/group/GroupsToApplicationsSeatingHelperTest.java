package com.atlassian.jira.bc.group;

import com.atlassian.jira.application.ApplicationKeys;
import com.atlassian.jira.application.ApplicationRole;
import com.atlassian.jira.application.ApplicationRoleManager;
import com.atlassian.jira.application.MockApplicationRole;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import java.util.Map;
import java.util.Optional;
import java.util.Set;

import static com.atlassian.labs.mockito.MockitoBooster.whenOption;
import static com.google.common.collect.ImmutableSet.of;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

public class GroupsToApplicationsSeatingHelperTest {
    private static final Optional<Long> DIRECTORY_ID = Optional.of(1L);

    @Rule
    public final MethodRule initMockito = MockitoJUnit.rule();

    @Mock
    private ApplicationRoleManager applicationRoleManager;
    @Mock
    private GroupRelationshipChecker relationshipChecker;

    private GroupsToApplicationsSeatingHelper helper;

    private MockApplicationRole coreRole;
    private MockApplicationRole softwareRole;
    private MockApplicationRole serviceDeskRole;

    private ApplicationUser emma;
    private ApplicationUser alana;
    private ApplicationUser will;

    @Before
    public void setUp() {
        coreRole = new MockApplicationRole(ApplicationKeys.CORE);
        softwareRole = new MockApplicationRole(ApplicationKeys.SOFTWARE);
        serviceDeskRole = new MockApplicationRole(ApplicationKeys.SERVICE_DESK);
        when(applicationRoleManager.getRoles()).thenReturn(of(coreRole, softwareRole, serviceDeskRole));

        whenOption(applicationRoleManager.getRole(ApplicationKeys.CORE)).thenSome(coreRole);

        //by definition groups with the same name are equal
        when(relationshipChecker.isGroupEqualOrNested(eq(DIRECTORY_ID), anyString(), anyString()))
                .thenAnswer(GroupRelationshipCheckerTest.eqalityMockitoAnswer());

        //prepare some users
        emma = new MockApplicationUser("emma");
        alana = new MockApplicationUser("alana");
        will = new MockApplicationUser("will");

        helper = new GroupsToApplicationsSeatingHelper(applicationRoleManager, relationshipChecker);
    }

    @Test
    public void shouldCountAllUsersToEachApplicationWhenUsersDoesNotHaveAnyAccess() throws Exception {
        softwareRole.groupNames("tigers");

        assertEquals(
                singleRoleWithUsers(softwareRole, will, emma),
                helper.rolesToBeAddedForSeatingCountPurpose(of(emma, will), of("tigers"))
        );
    }

    @Test
    public void shouldNotCountUsersWhichAlreadyHaveApplicationAccessGranted() {
        softwareRole.groupNames("tigers");
        givenUserWithRoles(emma, softwareRole);

        assertEquals(
                singleRoleWithUsers(softwareRole, will),
                helper.rolesToBeAddedForSeatingCountPurpose(of(emma, will), of("tigers"))
        );
    }


    @Test
    public void shouldNotCountCoreAccessWhenUserHasAlreadyApplicationAccess() {
        coreRole.groupNames("tigers");
        givenUserWithRoles(emma, softwareRole);
        givenUserWithRoles(alana, coreRole);

        assertEquals(
                singleRoleWithUsers(coreRole, will),
                helper.rolesToBeAddedForSeatingCountPurpose(of(emma, will, alana), of("tigers"))
        );
    }

    @SuppressWarnings("AssertEqualsBetweenInconvertibleTypes")
    @Test
    public void shouldCountAllApplicationsWhenOneGroupGivesAccessToManyApplications() {
        softwareRole.groupNames("tigers");
        serviceDeskRole.groupNames("tigers");

        assertEquals(
                ImmutableMap.of(softwareRole, of(emma, will), serviceDeskRole, of(emma, will)),
                helper.rolesToBeAddedForSeatingCountPurpose(of(emma, will), of("tigers"))
        );
    }

    @SuppressWarnings("AssertEqualsBetweenInconvertibleTypes")
    @Test
    public void shouldCombineAllGroupsWhenMultipleAppsAreBeingAdded() {
        softwareRole.groupNames("tigers");
        serviceDeskRole.groupNames("lions");

        assertEquals(
                ImmutableMap.of(softwareRole, of(emma, will), serviceDeskRole, of(emma, will)),
                helper.rolesToBeAddedForSeatingCountPurpose(of(emma, will), of("tigers", "lions"))
        );
    }


    @SuppressWarnings("AssertEqualsBetweenInconvertibleTypes")
    @Test
    public void shouldNotBreakWhenForSomeReasonCoreIsNotDefined() {
        whenOption(applicationRoleManager.getRole(ApplicationKeys.CORE)).thenNone();

        softwareRole.groupNames("tigers");
        coreRole.groupNames("tigers");

        assertEquals(
                "if we cannot retrieve Core application by key it means we have to treat 'coreRole' as regular one",
                ImmutableMap.of(softwareRole, of(emma, will), coreRole, of(emma, will)),
                helper.rolesToBeAddedForSeatingCountPurpose(of(emma, will), of("tigers"))

        );
    }

    @Test
    public void shouldCountSeatsWhenAssignedGroupIsASubgroupOfRoleGroup() throws Exception {
        softwareRole.groupNames("software-ubergroup");
        when(relationshipChecker.isGroupEqualOrNested(DIRECTORY_ID, "tiny-devs", "software-ubergroup")).thenReturn(true);

        assertEquals(
                singleRoleWithUsers(softwareRole, emma, will),
                helper.rolesToBeAddedForSeatingCountPurpose(of(emma, will), of("tiny-devs"))
        );
    }

    @Test
    public void shouldCountUserAsServiceDeskWhenCoreWillBeGrantedExplicitlyAndServiceDeskViaNestedGroups() throws Exception {
        coreRole.groupNames("tigers");
        serviceDeskRole.groupNames("all-agents");
        when(relationshipChecker.isGroupEqualOrNested(DIRECTORY_ID, "my-agents", "all-agents")).thenReturn(true);
        givenUserWithRoles(alana, serviceDeskRole);

        assertEquals(
                singleRoleWithUsers(serviceDeskRole, emma),
                helper.rolesToBeAddedForSeatingCountPurpose(of(emma, alana), of("tigers", "my-agents"))
        );
    }

    @Test
    public void shouldFindRolesBasingOnDefaultGroupsForGivenRoles() {
        serviceDeskRole.defaultGroupNames("tigers").groupNames("tigers");
        softwareRole.groupNames("tigers");

        final Set<ApplicationRole> expectedApplications = of(serviceDeskRole, softwareRole);
        assertEquals(
                expectedApplications,
                helper.findEffectiveApplications(DIRECTORY_ID, of(serviceDeskRole))
        );
        assertEquals(
                expectedApplications,
                helper.findEffectiveApplicationsByGroups(DIRECTORY_ID, of("tigers"))
        );
    }

    @Test
    public void shouldReturnBothApplicationsIfGroupsAreNested() throws Exception {
        serviceDeskRole.defaultGroupNames("tigers").groupNames("tigers");
        softwareRole.groupNames("all-agents");
        when(relationshipChecker.isGroupEqualOrNested(DIRECTORY_ID, "tigers", "all-agents")).thenReturn(true);

        final Set<ApplicationRole> expectedApplications = of(serviceDeskRole, softwareRole);
        assertEquals(
                expectedApplications,
                helper.findEffectiveApplications(DIRECTORY_ID, of(serviceDeskRole))
        );
        assertEquals(
                expectedApplications,
                helper.findEffectiveApplicationsByGroups(DIRECTORY_ID, of("tigers"))
        );
    }

    private Map<ApplicationRole, Set<ApplicationUser>> singleRoleWithUsers(final MockApplicationRole role, final ApplicationUser... users) {
        return ImmutableMap.of(role, ImmutableSet.copyOf(users));
    }

    private void givenUserWithRoles(final ApplicationUser user, final ApplicationRole... appRole) {
        when(applicationRoleManager.getRolesForUser(user)).thenReturn(ImmutableSet.copyOf(appRole));
    }


}