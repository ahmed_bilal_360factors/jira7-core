package com.atlassian.jira.crowd.embedded.ofbiz;

import com.atlassian.crowd.embedded.api.SearchRestriction;
import com.atlassian.crowd.search.query.entity.restriction.BooleanRestriction;
import com.atlassian.crowd.search.query.entity.restriction.BooleanRestrictionImpl;
import com.atlassian.crowd.search.query.entity.restriction.MatchMode;
import com.atlassian.crowd.search.query.entity.restriction.NullRestrictionImpl;
import com.atlassian.crowd.search.query.entity.restriction.PropertyImpl;
import com.atlassian.crowd.search.query.entity.restriction.PropertyRestriction;
import com.atlassian.crowd.search.query.entity.restriction.PropertyUtils;
import com.atlassian.crowd.search.query.entity.restriction.TermRestriction;
import com.atlassian.crowd.search.query.entity.restriction.constants.UserTermKeys;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.ofbiz.DefaultOfBizDelegator;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.mockito.Mock;
import org.ofbiz.core.entity.EntityCondition;
import org.ofbiz.core.entity.EntityConditionList;
import org.ofbiz.core.entity.EntityConditionParam;
import org.ofbiz.core.entity.EntityExpr;
import org.ofbiz.core.entity.EntityOperator;
import org.ofbiz.core.entity.GenericDelegator;
import org.ofbiz.core.entity.model.ModelEntity;
import org.ofbiz.core.entity.model.ModelField;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.stream.IntStream;

import static com.atlassian.crowd.search.query.entity.restriction.BooleanRestriction.BooleanLogic.AND;
import static com.atlassian.crowd.search.query.entity.restriction.BooleanRestriction.BooleanLogic.OR;
import static com.atlassian.crowd.search.query.entity.restriction.MatchMode.EXACTLY_MATCHES;
import static com.atlassian.crowd.search.query.entity.restriction.PropertyUtils.ofTypeString;
import static com.atlassian.crowd.search.query.entity.restriction.constants.UserTermKeys.USERNAME;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.emptyIterable;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.core.IsInstanceOf.instanceOf;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class UserEntityConditionFactoryTest {
    @Rule
    public RuleChain mockito = MockitoMocksInContainer.forTest(this);

    @Mock
    @AvailableInContainer
    private FeatureManager featureManager;

    private EntityConditionFactory factory;

    @Before
    public void setUp() throws Exception {
        ModelEntity entity = mock(ModelEntity.class);
        when(entity.getTableName(anyString())).thenReturn("cwd_user_attributes");

        GenericDelegator genericDelegator = mock(GenericDelegator.class);
        when(genericDelegator.getModelEntity(anyString())).thenReturn(entity);

        factory = new UserEntityConditionFactory(new DefaultOfBizDelegator(genericDelegator));

        when(featureManager.isEnabled(EntityConditionFactory.REWRITE_BIG_OR_CLAUSE_TO_IN_CLAUSE)).thenReturn(true);
    }

    @Test
    public void testNullRestriction() throws Exception {
        assertNull(factory.getEntityConditionFor(NullRestrictionImpl.INSTANCE));
    }

    @Test
    public void testLessThanRestrictionDate() throws Exception {
        Date date = new Date();
        final SearchRestriction searchRestriction = new TermRestriction<>(UserTermKeys.CREATED_DATE, MatchMode.LESS_THAN, date);

        final EntityCondition condition = factory.getEntityConditionFor(searchRestriction);
        final List<EntityConditionParam> parameterValues = new ArrayList<>();
        final String query = condition.makeWhereString(getModelEntity(), parameterValues);
        assertThat(query, equalTo("createdDate <  ? "));
        assertThat(parameterValues, contains(Matchers.allOf(
                conditionParam("createdDate=" + new Timestamp(date.getTime())),
                Matchers.hasProperty("fieldValue", equalTo(new Timestamp(date.getTime()))),
                Matchers.hasProperty("modelField", Matchers.hasProperty("name", equalTo("createdDate")))
        )));
    }

    @Test
    public void testUpdatedAndCreatedDateRestriction() throws Exception {
        Date date = new Date();
        final SearchRestriction createdDateRestriction = new TermRestriction<>(UserTermKeys.CREATED_DATE, MatchMode.LESS_THAN, date);
        final SearchRestriction updatedDateRestriction = new TermRestriction<>(UserTermKeys.UPDATED_DATE, MatchMode.LESS_THAN, date);
        final SearchRestriction restriction = new BooleanRestrictionImpl(BooleanRestriction.BooleanLogic.AND,
                createdDateRestriction, updatedDateRestriction);

        final EntityCondition condition = factory.getEntityConditionFor(restriction);
        final List<EntityConditionParam> parameterValues = new ArrayList<>();
        final String query = condition.makeWhereString(getModelEntity(), parameterValues);
        assertThat(query, equalTo("(createdDate <  ? ) AND (updatedDate <  ? )"));
        Timestamp timestamp = new Timestamp(date.getTime());
        assertThat(parameterValues, contains(conditionParam("createdDate=" + timestamp), conditionParam("updatedDate=" + timestamp)));
    }

    @Test
    public void testEqualRestrictionNullString() throws Exception {
        final SearchRestriction searchRestriction = new TermRestriction<>(UserTermKeys.USERNAME, EXACTLY_MATCHES, null);

        final EntityCondition condition = factory.getEntityConditionFor(searchRestriction);
        final List<EntityConditionParam> parameterValues = new ArrayList<>();
        final String query = condition.makeWhereString(getModelEntity(), parameterValues);
        assertThat(query, equalTo("lowerUserName IS NULL "));
        assertThat(parameterValues, emptyIterable());
    }

    @Test
    public void testEqualRestrictionEmptyString() throws Exception {
        final SearchRestriction searchRestriction = new TermRestriction<>(UserTermKeys.USERNAME, EXACTLY_MATCHES, "");

        final EntityCondition condition = factory.getEntityConditionFor(searchRestriction);
        final List<EntityConditionParam> parameterValues = new ArrayList<>();
        final String query = condition.makeWhereString(getModelEntity(), parameterValues);
        assertThat(query, equalTo("lowerUserName =  ? "));
        assertThat(parameterValues, contains(conditionParam(("lowerUserName="))));
    }

    @Test
    public void testEqualRestrictionString() throws Exception {
        final SearchRestriction searchRestriction = new TermRestriction<>(UserTermKeys.USERNAME, EXACTLY_MATCHES, "fred");

        final EntityCondition condition = factory.getEntityConditionFor(searchRestriction);
        final List<EntityConditionParam> parameterValues = new ArrayList<>();
        final String query = condition.makeWhereString(getModelEntity(), parameterValues);
        assertThat(query, equalTo("lowerUserName =  ? "));
        assertThat(parameterValues, contains(conditionParam("lowerUserName=fred")));
    }

    @Test
    public void testEqualRestrictionBooleanNull() throws Exception {
        final SearchRestriction searchRestriction = new TermRestriction<>(UserTermKeys.ACTIVE, EXACTLY_MATCHES, null);

        final EntityCondition condition = factory.getEntityConditionFor(searchRestriction);
        final List<EntityConditionParam> parameterValues = new ArrayList<>();
        final String query = condition.makeWhereString(getModelEntity(), parameterValues);
        assertThat(query, equalTo("active IS NULL "));
        assertThat(parameterValues, emptyIterable());
    }

    @Test
    public void testEqualRestrictionBooleanTrue() throws Exception {
        final SearchRestriction searchRestriction = new TermRestriction<>(UserTermKeys.ACTIVE, EXACTLY_MATCHES,
                Boolean.TRUE);

        final EntityCondition condition = factory.getEntityConditionFor(searchRestriction);
        final List<EntityConditionParam> parameterValues = new ArrayList<>();
        final String query = condition.makeWhereString(getModelEntity(), parameterValues);
        assertThat(query, equalTo("active =  ? "));
        assertThat(parameterValues, contains(conditionParam("active=1")));
    }

    @Test
    public void testEqualRestrictionBooleanFalse() throws Exception {
        final SearchRestriction searchRestriction = new TermRestriction<>(UserTermKeys.ACTIVE, EXACTLY_MATCHES,
                Boolean.FALSE);

        final EntityCondition condition = factory.getEntityConditionFor(searchRestriction);
        final List<EntityConditionParam> parameterValues = new ArrayList<>();
        final String query = condition.makeWhereString(getModelEntity(), parameterValues);
        assertThat(query, equalTo("active =  ? "));
        assertThat(parameterValues, contains(conditionParam("active=0")));
    }

    @Test
    public void testGTRestriction() throws Exception {
        final SearchRestriction searchRestriction = new TermRestriction<>(UserTermKeys.USERNAME, MatchMode.GREATER_THAN, "fred");

        final EntityCondition condition = factory.getEntityConditionFor(searchRestriction);
        final List<EntityConditionParam> parameterValues = new ArrayList<>();
        final String query = condition.makeWhereString(getModelEntity(), parameterValues);
        assertThat(query, equalTo("lowerUserName >  ? "));
        assertThat(parameterValues, contains(conditionParam("lowerUserName=fred")));
    }

    @Test
    public void testLTRestriction() throws Exception {
        final SearchRestriction searchRestriction = new TermRestriction<>(UserTermKeys.USERNAME, MatchMode.LESS_THAN, "fred");

        final EntityCondition condition = factory.getEntityConditionFor(searchRestriction);
        final List<EntityConditionParam> parameterValues = new ArrayList<>();
        final String query = condition.makeWhereString(getModelEntity(), parameterValues);
        assertThat(query, equalTo("lowerUserName <  ? "));
        assertThat(parameterValues, contains(conditionParam("lowerUserName=fred")));
    }

    @Test
    public void testContainsRestriction() throws Exception {
        final SearchRestriction searchRestriction = new TermRestriction<>(UserTermKeys.USERNAME, MatchMode.CONTAINS, "fred");

        final EntityCondition condition = factory.getEntityConditionFor(searchRestriction);
        final List<EntityConditionParam> parameterValues = new ArrayList<>();
        final String query = condition.makeWhereString(getModelEntity(), parameterValues);
        assertThat(query, equalTo("lowerUserName LIKE  ? "));
        assertThat(parameterValues, contains(conditionParam("lowerUserName=%fred%")));
    }

    @Test
    public void testStartsWithRestriction() throws Exception {
        final SearchRestriction searchRestriction = new TermRestriction<>(UserTermKeys.USERNAME, MatchMode.STARTS_WITH, "fred");

        final EntityCondition condition = factory.getEntityConditionFor(searchRestriction);
        final List<EntityConditionParam> parameterValues = new ArrayList<>();
        final String query = condition.makeWhereString(getModelEntity(), parameterValues);
        assertThat(query, equalTo("lowerUserName LIKE  ? "));
        assertThat(parameterValues, contains(conditionParam("lowerUserName=fred%")));
    }

    @Test
    public void testSimpleOr() throws Exception {
        final SearchRestriction searchRestriction1 = new TermRestriction<>(UserTermKeys.USERNAME, EXACTLY_MATCHES, "fred");
        final SearchRestriction searchRestriction2 = new TermRestriction<>(UserTermKeys.EMAIL, EXACTLY_MATCHES,
                "fred@example.com");
        final SearchRestriction restriction = new BooleanRestrictionImpl(BooleanRestriction.BooleanLogic.OR, searchRestriction1, searchRestriction2);

        final EntityCondition condition = factory.getEntityConditionFor(restriction);
        final List<EntityConditionParam> parameterValues = new ArrayList<>();
        final String query = condition.makeWhereString(getModelEntity(), parameterValues);
        assertThat(query, equalTo("(lowerUserName =  ? ) OR (lowerEmailAddress =  ? )"));
        assertThat(parameterValues, contains(conditionParam("lowerUserName=fred"), conditionParam("lowerEmailAddress=fred@example.com")));
    }

    @Test
    public void testSimpleAnd() throws Exception {
        final SearchRestriction searchRestriction1 = new TermRestriction<>(UserTermKeys.USERNAME, EXACTLY_MATCHES, "fred");
        final SearchRestriction searchRestriction2 = new TermRestriction<>(UserTermKeys.EMAIL, EXACTLY_MATCHES,
                "fred@example.com");
        final SearchRestriction restriction = new BooleanRestrictionImpl(BooleanRestriction.BooleanLogic.AND, searchRestriction1, searchRestriction2);

        final EntityCondition condition = factory.getEntityConditionFor(restriction);
        final List<EntityConditionParam> parameterValues = new ArrayList<>();
        final String query = condition.makeWhereString(getModelEntity(), parameterValues);
        assertThat(query, equalTo("(lowerUserName =  ? ) AND (lowerEmailAddress =  ? )"));
        assertThat(parameterValues, contains(conditionParam("lowerUserName=fred"), conditionParam("lowerEmailAddress=fred@example.com")));
    }

    @Test
    public void testSimpleAndWithNullRestriction() throws Exception {
        final SearchRestriction searchRestriction1 = new TermRestriction<>(UserTermKeys.USERNAME, EXACTLY_MATCHES, "fred");
        final SearchRestriction searchRestriction2 = NullRestrictionImpl.INSTANCE;
        final SearchRestriction restriction = new BooleanRestrictionImpl(BooleanRestriction.BooleanLogic.AND, searchRestriction1, searchRestriction2);

        final EntityCondition condition = factory.getEntityConditionFor(restriction);
        final List<EntityConditionParam> parameterValues = new ArrayList<>();
        final String query = condition.makeWhereString(getModelEntity(), parameterValues);
        assertThat(query, equalTo("(lowerUserName =  ? )"));
        assertThat(parameterValues, contains(conditionParam("lowerUserName=fred")));
    }

    @Test
    public void testTrivialBooleanAndWithOnlyNullRestriction() throws Exception {
        final SearchRestriction searchRestriction1 = NullRestrictionImpl.INSTANCE;
        final SearchRestriction searchRestriction2 = NullRestrictionImpl.INSTANCE;
        final SearchRestriction restriction = new BooleanRestrictionImpl(BooleanRestriction.BooleanLogic.AND, searchRestriction1, searchRestriction2);

        final EntityCondition condition = factory.getEntityConditionFor(restriction);
        final List<EntityConditionParam> parameterValues = new ArrayList<>();
        final String query = condition.makeWhereString(getModelEntity(), parameterValues);
        assertThat(query, equalTo(""));
        assertThat(parameterValues, emptyIterable());
    }

    @Test
    public void testSimpleOrWithNullRestriction() throws Exception {
        final SearchRestriction searchRestriction1 = new TermRestriction<>(UserTermKeys.USERNAME, EXACTLY_MATCHES, "fred");
        final SearchRestriction searchRestriction2 = NullRestrictionImpl.INSTANCE;
        final SearchRestriction restriction = new BooleanRestrictionImpl(BooleanRestriction.BooleanLogic.OR, searchRestriction1, searchRestriction2);

        final EntityCondition condition = factory.getEntityConditionFor(restriction);
        assertNull(condition);
    }

    @Test
    public void testNestedQuery() throws Exception {
        final SearchRestriction searchRestriction1 = new TermRestriction<>(UserTermKeys.USERNAME, EXACTLY_MATCHES, "fred");
        final SearchRestriction searchRestriction2 = new TermRestriction<>(UserTermKeys.EMAIL, EXACTLY_MATCHES,
                "fred@example.com");
        final SearchRestriction searchRestriction3 = new TermRestriction<>(UserTermKeys.USERNAME, EXACTLY_MATCHES, "joe");
        final SearchRestriction searchRestriction4 = new TermRestriction<>(UserTermKeys.EMAIL, EXACTLY_MATCHES,
                "joe@example.com");
        final SearchRestriction booleanRestriction1 = new BooleanRestrictionImpl(BooleanRestriction.BooleanLogic.AND, searchRestriction1,
                searchRestriction2);
        final SearchRestriction booleanRestriction2 = new BooleanRestrictionImpl(BooleanRestriction.BooleanLogic.AND, searchRestriction3,
                searchRestriction4);
        final SearchRestriction booleanRestriction3 = new BooleanRestrictionImpl(BooleanRestriction.BooleanLogic.OR, booleanRestriction1,
                booleanRestriction2);

        final EntityCondition condition = factory.getEntityConditionFor(booleanRestriction3);
        final List<EntityConditionParam> parameterValues = new ArrayList<>();
        final String query = condition.makeWhereString(getModelEntity(), parameterValues);
        assertThat(query, equalTo("((lowerUserName =  ? ) AND (lowerEmailAddress =  ? )) OR ((lowerUserName =  ? ) AND (lowerEmailAddress =  ? ))"));
        assertThat(parameterValues, contains(
                conditionParam("lowerUserName=fred"),
                conditionParam("lowerEmailAddress=fred@example.com"),
                conditionParam("lowerUserName=joe"),
                conditionParam("lowerEmailAddress=joe@example.com")));
    }

    @Test
    public void testAttributeRestriction() throws Exception {
        final SearchRestriction restriction = new TermRestriction<>(PropertyUtils.ofTypeString("login.count"), EXACTLY_MATCHES, "0");

        final EntityCondition condition = factory.getEntityConditionFor(restriction);
        final List<EntityConditionParam> parameterValues = new ArrayList<>();
        final String query = condition.makeWhereString(getModelEntity(), parameterValues);
        assertThat(query, equalTo("id IN (SELECT user_id FROM cwd_user_attributes WHERE attribute_name = ? AND lower_attribute_value =  ? )"));
        assertThat(parameterValues, contains(conditionParam("name=login.count"), conditionParam("value=0")));
    }

    @Test
    public void testNestedWithAttributesQuery() throws Exception {
        final SearchRestriction searchRestriction1 = new TermRestriction<>(USERNAME, EXACTLY_MATCHES, "fred");
        final SearchRestriction searchRestriction2 = new TermRestriction<>(ofTypeString("login.count"), EXACTLY_MATCHES, "0");
        final SearchRestriction searchRestriction3 = new TermRestriction<>(USERNAME, EXACTLY_MATCHES, "joe");
        final SearchRestriction searchRestriction4 = new TermRestriction<>(ofTypeString("login.count"), EXACTLY_MATCHES, "0");
        final SearchRestriction booleanRestriction1 = new BooleanRestrictionImpl(AND, searchRestriction1,
                searchRestriction2);
        final SearchRestriction booleanRestriction2 = new BooleanRestrictionImpl(AND, searchRestriction3,
                searchRestriction4);
        final SearchRestriction booleanRestriction3 = new BooleanRestrictionImpl(OR, booleanRestriction1,
                booleanRestriction2);

        final EntityCondition condition = factory.getEntityConditionFor(booleanRestriction3);
        final List<EntityConditionParam> parameterValues = new ArrayList<>();
        final String query = condition.makeWhereString(getModelEntity(), parameterValues);
        assertThat(query, equalTo(
                "((lowerUserName =  ? ) AND (id IN (SELECT user_id FROM cwd_user_attributes WHERE attribute_name = ? AND lower_attribute_value =  ? ))) OR ((lowerUserName =  ? ) AND (id IN (SELECT user_id FROM cwd_user_attributes WHERE attribute_name = ? AND lower_attribute_value =  ? )))"
        ));

        assertThat(parameterValues, contains(
                conditionParam("lowerUserName=fred"),
                conditionParam("name=login.count"),
                conditionParam("value=0"),
                conditionParam("lowerUserName=joe"),
                conditionParam("name=login.count"),
                conditionParam("value=0")));
    }

    @Test
    public void testRewriteBigOrClauseToInClause() {
        final PropertyRestriction<String>[] restrictions = generateRestrictions("lowerUserName", EXACTLY_MATCHES, 4);
        final SearchRestriction restriction = new BooleanRestrictionImpl(BooleanRestriction.BooleanLogic.OR, restrictions);

        final EntityCondition condition = factory.getEntityConditionFor(restriction);

        assertThat(condition, instanceOf(EntityExpr.class));
        EntityExpr entityExpr = (EntityExpr) condition;
        assertThat(entityExpr.getLhs(), equalTo("lowerUserName"));
        assertThat(entityExpr.isLUpper(), equalTo(false));
        assertThat(entityExpr.getOperator(), equalTo(EntityOperator.IN));
        assertThat(entityExpr.isRUpper(), equalTo(false));
        assertThat(entityExpr.getRhs(), instanceOf(Collection.class));
        final Collection<String> values = (Collection<String>) entityExpr.getRhs();
        assertThat("Should contain lowercased values", values, containsInAnyOrder("value0", "value1", "value2", "value3"));

        when(featureManager.isEnabled(EntityConditionFactory.REWRITE_BIG_OR_CLAUSE_TO_IN_CLAUSE)).thenReturn(false);

        final EntityCondition conditionFeatureDisabled = factory.getEntityConditionFor(restriction);

        assertThat(conditionFeatureDisabled, not(instanceOf(EntityExpr.class)));
    }

    @Test
    public void testShouldNotRewriteAndClauseToInClause() {
        final PropertyRestriction<String>[] restrictions = generateRestrictions("lowerUserName", EXACTLY_MATCHES, 4);
        final SearchRestriction restriction = new BooleanRestrictionImpl(BooleanRestriction.BooleanLogic.AND, restrictions);

        final EntityCondition condition = factory.getEntityConditionFor(restriction);

        assertThat(condition, instanceOf(EntityConditionList.class));
    }

    @Test
    public void testRewriteBigOrClauseOnlyForStringClauses() {
        final PropertyRestriction<String>[] restrictions = generateRestrictions("lowerUserName", EXACTLY_MATCHES, 4);
        final ArrayList<SearchRestriction> list = new ArrayList<>(Arrays.asList(restrictions));
        list.add(new TermRestriction(new PropertyImpl("lowerUserName", Boolean.class), EXACTLY_MATCHES, false));
        final SearchRestriction restriction = new BooleanRestrictionImpl(BooleanRestriction.BooleanLogic.OR, list);

        final EntityCondition condition = factory.getEntityConditionFor(restriction);

        assertThat(condition, instanceOf(EntityConditionList.class));
    }

    @Test
    public void testRewriteBigOrClauseOnlyForPropertyRestrictions() {
        final PropertyRestriction<String>[] restrictions = generateRestrictions("lowerUserName", EXACTLY_MATCHES, 4);
        final ArrayList<SearchRestriction> list = new ArrayList<>(Arrays.asList(restrictions));
        list.add(new BooleanRestrictionImpl(BooleanRestriction.BooleanLogic.AND, NullRestrictionImpl.INSTANCE));
        final SearchRestriction restriction = new BooleanRestrictionImpl(BooleanRestriction.BooleanLogic.OR, list);

        final EntityCondition condition = factory.getEntityConditionFor(restriction);

        assertThat(condition, instanceOf(EntityConditionList.class));
    }

    @Test
    public void testRewriteBigOrClauseOnlyForCoreProperties() {
        final PropertyRestriction<String>[] restrictions = generateRestrictions("nonCoreProperty", EXACTLY_MATCHES, 4);
        final ArrayList<SearchRestriction> list = new ArrayList<>(Arrays.asList(restrictions));
        final SearchRestriction restriction = new BooleanRestrictionImpl(BooleanRestriction.BooleanLogic.OR, list);

        final EntityCondition condition = factory.getEntityConditionFor(restriction);

        assertThat(condition, instanceOf(EntityConditionList.class));
    }

    @Test
    public void testRewriteBigOrClauseOnlyForPropertyRestrictionsThatUseEqualOperator() {
        final PropertyRestriction<String>[] restrictions = generateRestrictions("lowerUserName", EXACTLY_MATCHES, 4);
        final ArrayList<SearchRestriction> list = new ArrayList<>(Arrays.asList(restrictions));
        list.add(new TermRestriction<>(new PropertyImpl<>("lowerUserName", String.class), MatchMode.STARTS_WITH, "value"));
        final SearchRestriction restriction = new BooleanRestrictionImpl(BooleanRestriction.BooleanLogic.OR, list);

        final EntityCondition condition = factory.getEntityConditionFor(restriction);

        assertThat(condition, instanceOf(EntityConditionList.class));
    }

    private PropertyRestriction<String>[] generateRestrictions(String propertyName, MatchMode matchMode, int count) {
        return IntStream
                .range(0, count)
                .mapToObj(i -> new TermRestriction<String>(new PropertyImpl<>(propertyName, String.class), matchMode, "VALUE" + i))
                .toArray(i -> new PropertyRestriction[count]);
    }

    private ModelEntity getModelEntity() {
        final ModelEntity modelEntity = new ModelEntity();
        modelEntity.setTableName("User");
        modelEntity.addField(getModelField("userName"));
        modelEntity.addField(getModelField("lowerUserName"));
        modelEntity.addField(getModelField("emailAddress"));
        modelEntity.addField(getModelField("lowerEmailAddress"));
        modelEntity.addField(getModelField("active", "boolean"));
        modelEntity.addField(getModelField("createdDate", "java.util.Date"));
        modelEntity.addField(getModelField("updatedDate", "java.util.Date"));

        return modelEntity;
    }

    private ModelField getModelField(final String name) {
        return getModelField(name, "blah");
    }

    private ModelField getModelField(final String name, final String type) {
        return new ModelField(name, type, name, false, null);
    }

    private Matcher<? super EntityConditionParam> conditionParam(final String paramToString) {
        return new TypeSafeMatcher<EntityConditionParam>() {
            @Override
            protected boolean matchesSafely(final EntityConditionParam item) {
                return paramToString.equals(item.toString());
            }

            @Override
            public void describeTo(final Description description) {
                description.appendText("param with string representation of: '" + paramToString + "'");
            }
        };
    }
}
