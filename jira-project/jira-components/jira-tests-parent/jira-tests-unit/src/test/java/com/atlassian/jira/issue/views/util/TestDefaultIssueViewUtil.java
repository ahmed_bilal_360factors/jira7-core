package com.atlassian.jira.issue.views.util;

import com.atlassian.jira.issue.RendererManager;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutManager;
import com.atlassian.jira.issue.link.IssueLinkManager;
import com.atlassian.jira.issue.util.AggregateTimeTrackingBean;
import com.atlassian.jira.issue.util.AggregateTimeTrackingCalculator;
import com.atlassian.jira.issue.util.AggregateTimeTrackingCalculatorFactory;
import com.atlassian.jira.mock.issue.MockIssue;
import com.atlassian.jira.util.JiraDurationUtils;
import com.atlassian.jira.util.NoopI18nHelper;
import com.atlassian.jira.web.bean.TimeTrackingGraphBean;
import com.atlassian.jira.web.bean.TimeTrackingGraphBeanFactory;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import static org.junit.Assert.assertSame;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.same;
import static org.mockito.Mockito.when;

/**
 * This is a test for {@link com.atlassian.jira.issue.views.util.DefaultIssueViewUtil}.
 *
 * @since v4.1
 */
public class TestDefaultIssueViewUtil {
    @Rule
    public final MethodRule initMockito = MockitoJUnit.rule();

    @Mock
    private AggregateTimeTrackingCalculatorFactory aggregateTimeTrackingCalculatorFactory;
    @Mock
    private AggregateTimeTrackingCalculator aggregateTimeTrackingCalculator;
    @Mock
    private TimeTrackingGraphBeanFactory timeTrackingGraphBeanFactory;
    @Mock
    private IssueLinkManager issueLinkManager;
    @Mock
    private FieldLayoutManager fieldLayoutManager;
    @Mock
    private RendererManager rendererManager;
    @Mock
    private JiraDurationUtils jiraDurationUtils;

    private DefaultIssueViewUtil defaultIssueViewUtil;

    @Before
    public void setUp() throws Exception {
        defaultIssueViewUtil = new DefaultIssueViewUtil(issueLinkManager, fieldLayoutManager, rendererManager,
                jiraDurationUtils, aggregateTimeTrackingCalculatorFactory, timeTrackingGraphBeanFactory);
    }

    @Test
    public void testCreateAggregateBean() throws Exception {
        MockIssue issue = new MockIssue(1);
        final AggregateTimeTrackingBean bean = new AggregateTimeTrackingBean(78L, 28L, 38L, 0);

        when(aggregateTimeTrackingCalculator.getAggregates(issue)).thenReturn(bean);
        when(aggregateTimeTrackingCalculatorFactory.getCalculator(issue)).thenReturn(aggregateTimeTrackingCalculator);

        assertSame(bean, defaultIssueViewUtil.createAggregateBean(issue));
    }

    @Test
    public void testCreateTimeTrackingBean() throws Exception {
        final AggregateTimeTrackingBean aggBean = new AggregateTimeTrackingBean(78L, 28L, 38L, 0);
        final NoopI18nHelper nHelper = new NoopI18nHelper();
        final TimeTrackingGraphBean ttBean = new TimeTrackingGraphBean(new TimeTrackingGraphBean.Parameters(nHelper));

        when(timeTrackingGraphBeanFactory.createBean(same(aggBean), eq(TimeTrackingGraphBeanFactory.Style.NORMAL), same(nHelper))).thenReturn(ttBean);

        assertSame(ttBean, defaultIssueViewUtil.createTimeTrackingBean(aggBean, nHelper));
    }
}
