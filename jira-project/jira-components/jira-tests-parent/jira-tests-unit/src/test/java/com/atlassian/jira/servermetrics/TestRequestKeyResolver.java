package com.atlassian.jira.servermetrics;

import com.google.common.collect.ImmutableSet;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.mockito.Mockito;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;

import static java.util.stream.Collectors.toMap;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(Parameterized.class)
public class TestRequestKeyResolver {

    @Parameterized.Parameters(name = "request: {0}/{1}, parameters: {2}, expected result: {3}")
    public static Iterable<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {"", "secure/IssueNavAction.jspa", ImmutableSet.of(), Optional.of("jira.issue.view")},
                {"", "secure/IssueNavAction!default.jspa", ImmutableSet.of(), Optional.of("jira.issue.view")},
                {"", "secure/IssueNavAction!default.jspa", ImmutableSet.of("jql"), Optional.of("jira.issue.nav-list")},
                {"", "secure/IssueNavAction!default.jspa", ImmutableSet.of("jql", "anything"), Optional.of("jira.issue.nav-list")},
                {"", "secure/IssueNavAction!default.jspa", ImmutableSet.of("jql", "zomethig"), Optional.of("jira.issue.nav-list")},
                {"", "secure/IssueNavAction!default.jspa", ImmutableSet.of("jql", "serverRenderedViewIssue"), Optional.of("jira.issue.nav-detail")},
                {"", "secure/Dashboard!default.jspa", ImmutableSet.of("jql", "serverRenderedViewIssue"), Optional.of("jira.dashboard")},
                {"", "secure/RapidBoard!default.jspa", ImmutableSet.of("rapidView", "planning"), Optional.of("jira.agile.work")}
        });
    }

    final String servletPath;
    final String pathInfo;
    final Set<String> parameters;
    final Optional<String> expectedKey;

    private final RequestKeyResolver testObj = new RequestKeyResolver();

    public TestRequestKeyResolver(String servletPath, String pathInfo, Set<String> parameters, Optional<String> expectedKey) {
        this.servletPath = servletPath;
        this.pathInfo = pathInfo;
        this.parameters = parameters;
        this.expectedKey = expectedKey;
    }

    @Test
    public void shoudlGiveExpectedRequestKeys() {
        HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
        when(request.getServletPath()).thenReturn(servletPath);
        when(request.getPathInfo()).thenReturn(pathInfo);
        when(request.getParameterMap()).thenReturn(
                parameters
                        .stream()
                        .collect(toMap(
                                Function.identity(),
                                paramName -> new String[]{paramName + "-value"})
                        )
        );

        final Optional<String> requestId = testObj.getRequestId(request);

        assertThat(requestId, Matchers.equalTo(expectedKey));
    }
}