package com.atlassian.jira.license;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.core.util.Clock;
import com.atlassian.extras.api.AtlassianLicense;
import com.atlassian.extras.api.LicenseException;
import com.atlassian.extras.api.LicenseManager;
import com.atlassian.extras.api.LicenseType;
import com.atlassian.extras.api.Product;
import com.atlassian.extras.api.jira.JiraLicense;
import com.atlassian.fugue.Iterables;
import com.atlassian.fugue.Option;
import com.atlassian.jira.cluster.ClusterManager;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.datetime.DateTimeFormatter;
import com.atlassian.jira.test.util.lic.License;
import com.atlassian.jira.util.BuildUtilsInfo;
import com.atlassian.jira.util.ConstantClock;
import com.atlassian.jira.web.bean.I18nBean;
import com.atlassian.jira.web.util.ExternalLinkUtil;
import org.hamcrest.Description;
import org.hamcrest.TypeSafeDiagnosingMatcher;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static com.atlassian.jira.license.Licenses.LICENSE_ELA_4_ROLES;
import static com.atlassian.jira.license.Licenses.LICENSE_NO_APP_ROLES_INVALID;
import static com.atlassian.jira.license.Licenses.LICENSE_NO_ROLES;
import static com.atlassian.jira.license.Licenses.LICENSE_ONE_APP_ROLE_NO_JIRA_ACTIVE;
import static com.atlassian.jira.license.Licenses.LICENSE_ONE_ROLE;
import static com.atlassian.jira.license.Licenses.LICENSE_THREE_ROLES;
import static com.atlassian.jira.test.util.lic.cloud.CloudLicenses.LICENSE_ONDEMAND_6x_SERVICEDESK_TBP;
import static com.atlassian.jira.test.util.lic.sd.ServiceDeskLicenses.LICENSE_SERVICE_DESK_ABP;
import static com.atlassian.jira.test.util.lic.sd.ServiceDeskLicenses.LICENSE_SERVICE_DESK_ABP_EVAL;
import static com.atlassian.jira.test.util.lic.sd.ServiceDeskLicenses.LICENSE_SERVICE_DESK_RBP;
import static com.atlassian.jira.test.util.lic.sd.ServiceDeskLicenses.LICENSE_SERVICE_DESK_RBP_EVAL;
import static com.atlassian.jira.test.util.lic.sd.ServiceDeskLicenses.LICENSE_SERVICE_DESK_TBP_EVAL;
import static com.atlassian.jira.test.util.lic.sd.ServiceDeskLicenses.LICENSE_SERVICE_DESK_TBP_UNLIMITED;
import static java.lang.String.format;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestLicenseDetailsFactoryImpl {
    private static final String BAD_LICENSE = "A Bad License";

    @Mock
    private ApplicationProperties applicationProperties;
    @Mock
    private AtlassianLicense atlassianLicense;
    @Mock
    private BuildUtilsInfo buildUtilsInfo;
    @Mock
    private DateTimeFormatter dateTimeFormatter;
    @Mock
    private ExternalLinkUtil externalLinkUtil;
    @Mock
    private I18nBean.BeanFactory i18Factory;
    @Mock
    private LicenseManager licenseManager;
    @Mock
    private ClusterManager clusterManager;
    @Mock
    private JiraLicense jiraLicense;
    private Clock clock = new ConstantClock(10);
    private LicenseDetailsFactoryImpl licenseDetailsFactory;

    @Before
    public void setup() {
        licenseDetailsFactory = new LicenseDetailsFactoryImpl(
                applicationProperties, externalLinkUtil, buildUtilsInfo,
                i18Factory, dateTimeFormatter, clock);
    }

    @Test
    public void testDecodeFailure() {
        when(licenseManager.getLicense(BAD_LICENSE)).thenThrow(new LicenseException());
        assertFalse(licenseDetailsFactory.isDecodeable(BAD_LICENSE));
    }

    @Test
    public void testDecodeBlank() {
        assertFalse(licenseDetailsFactory.isDecodeable(""));
    }

    @Test
    public void testDecodeNull() {
        assertFalse(licenseDetailsFactory.isDecodeable(null));
    }

    @Test(expected = LicenseException.class)
    public void testThrowsExceptionForInvalidLicense() {
        licenseDetailsFactory.getLicense("*&^%");
    }

    @Test(expected = LicenseException.class)
    public void testThrowsExceptionForNullLicense() {
        licenseDetailsFactory.getLicense(null);
    }

    @Test
    public void testNoLicenseRolesCanBeDecoded() {
        assertTrue("No license roles is a valid license", canDecode(LICENSE_NO_ROLES));
    }

    @Test
    public void testOneLicenseRoleCanBeDecoded() {
        assertTrue("No license roles is a valid license", canDecode(LICENSE_ONE_ROLE));
    }

    @Test
    public void testThreeLicenseRolesCanBeDecoded() {
        assertTrue("No license roles is a valid license", canDecode(LICENSE_THREE_ROLES));
    }

    @Test
    public void testEnterpriseLicenseCreation() {
        final LicenseDetails license = getLicenseDetails(LICENSE_ELA_4_ROLES);

        assertTrue("Returned license is a subscription-based license",
                license instanceof SubscriptionLicenseDetails);

        assertThat(license, LicenseDetailsMatcher.equals(LICENSE_ELA_4_ROLES));
    }

    @Test
    public void testVersionSevenNoBackwardsCompatible() {
        boolean decodable = canDecode(LICENSE_ONE_APP_ROLE_NO_JIRA_ACTIVE);
        assertTrue("License with apps and jira.active being omitted is valid", decodable);
    }

    @Test
    public void testVersionSevenWithNoAppsFailsToDecode() {
        boolean decodable = canDecode(LICENSE_NO_APP_ROLES_INVALID);
        assertFalse("License without an application role and no jira.active is invalid", decodable);
    }

    // The following Service Desk (SD) tests uses the following acronyms:
    //   RBP: Role Based Pricing (i.e. JIRA SD 7.x licenses)
    //   ABP: Agent Based Pricing (i.e. SD 2.x licenses)
    //   TBP: Tier Base Pricing (i.e. SD 1.x licenses)
    // See ServiceDeskLicenses for more details of the different licenses.

    @Test
    public void testServiceDeskRBPLicenseCanBeDecoded() {
        assertTrue(canDecode(LICENSE_SERVICE_DESK_RBP));
    }

    @Test
    public void testServiceDeskRBPLicenseCanBeParsed() {
        final LicenseDetails details = getLicenseDetails(LICENSE_SERVICE_DESK_RBP);
        assertThat(details, LicenseDetailsMatcher.equals(LICENSE_SERVICE_DESK_RBP));

        final Product product = details.getJiraLicense().getProduct();
        assertTrue(Product.JIRA.getNamespace().equals(product.getNamespace()));
        assertFalse(details.isEvaluation());
    }

    @Test
    public void testServiceDeskRBPEvalLicenseCanBeDecoded() {
        assertTrue(canDecode(LICENSE_SERVICE_DESK_RBP_EVAL));
    }

    @Test
    public void testServiceDeskRBPEvalLicenseCanBeParsed() {
        final LicenseDetails details = getLicenseDetails(LICENSE_SERVICE_DESK_RBP_EVAL);
        assertThat(details, LicenseDetailsMatcher.equals(LICENSE_SERVICE_DESK_RBP_EVAL));

        final Product product = details.getJiraLicense().getProduct();
        assertTrue(Product.JIRA.getNamespace().equals(product.getNamespace()));
        assertTrue(details.isEvaluation());
    }

    @Test
    public void testServiceDeskTBPLicenseCanBeDecoded() {
        assertTrue(canDecode(LICENSE_SERVICE_DESK_TBP_UNLIMITED));
    }

    @Test
    public void testServiceDeskTBPLicenseCanBeParsed() {
        final LicenseDetails details = getLicenseDetails(LICENSE_SERVICE_DESK_TBP_UNLIMITED);
        assertThat(details, LicenseDetailsMatcher.equals(LICENSE_SERVICE_DESK_TBP_UNLIMITED));
        assertServiceDeskProduct(details);
    }

    @Test
    public void testServiceDeskTBPEvalLicenseCanBeDecoded() {
        assertTrue(canDecode(LICENSE_SERVICE_DESK_TBP_EVAL));
    }

    @Test
    public void testServiceDeskTBPEvalLicenseCanBeParsed() {
        final LicenseDetails details = getLicenseDetails(LICENSE_SERVICE_DESK_TBP_EVAL);
        assertThat(details, LicenseDetailsMatcher.equals(LICENSE_SERVICE_DESK_TBP_EVAL));
        assertServiceDeskProduct(details);
    }

    @Test
    public void testJira6xOnDemandLicenseWithServiceDesk() {
        final LicenseDetails details = getLicenseDetails(LICENSE_ONDEMAND_6x_SERVICEDESK_TBP);
        assertThat(details, LicenseDetailsMatcher.equals(LICENSE_ONDEMAND_6x_SERVICEDESK_TBP));
        assertServiceDeskProduct(details);
    }

    @Test
    public void testServiceDeskABPLicenseCanBeDecoded() {
        assertTrue(canDecode(LICENSE_SERVICE_DESK_ABP));
    }

    @Test
    public void testServiceDeskABPLicenseCanBeParsed() {
        final LicenseDetails details = getLicenseDetails(LICENSE_SERVICE_DESK_ABP);
        assertThat(details, LicenseDetailsMatcher.equals(LICENSE_SERVICE_DESK_ABP));
        assertServiceDeskProduct(details);
    }

    @Test
    public void testServiceDeskABPEvalLicenseCanBeDecoded() {
        assertTrue(canDecode(LICENSE_SERVICE_DESK_ABP_EVAL));
    }

    @Test
    public void testServiceDeskABPEvalLicenseCanBeParsed() {
        final LicenseDetails details = getLicenseDetails(LICENSE_SERVICE_DESK_ABP_EVAL);
        assertThat(details, LicenseDetailsMatcher.equals(LICENSE_SERVICE_DESK_ABP_EVAL));
        assertServiceDeskProduct(details);
    }

    private static void assertServiceDeskProduct(final LicenseDetails details) {
        final Option<Product> sdProduct = Iterables.findFirst(details.getJiraLicense().getProducts(),
                product -> product.getNamespace().contains("servicedesk"));

        assertTrue("Is ServiceDesk product?", sdProduct.isDefined());
    }

    private boolean canDecode(License license) {
        return licenseDetailsFactory.isDecodeable(license.getLicenseString());
    }

    private boolean canDecode(String license) {
        return licenseDetailsFactory.isDecodeable(license);
    }

    private LicenseDetails getLicenseDetails(License license) {
        return licenseDetailsFactory.getLicense(license.getLicenseString());
    }

    private static class LicenseDetailsMatcher extends TypeSafeDiagnosingMatcher<LicenseDetails> {
        private String description;
        private String organisation;
        private String sen;
        private boolean evaluation;
        private Map<ApplicationKey, Integer> roles = new HashMap<>();
        private LicenseType type;

        private LicenseDetailsMatcher(License license) {
            this.description = license.getDescription();
            this.organisation = license.getOrganisation();
            this.sen = license.getSen();
            this.evaluation = license.isEvaluation();

            Map<ApplicationKey, Integer> userCount = new HashMap<>();
            for (Map.Entry<String, License.Role> entry : license.getRoles().entrySet()) {
                userCount.put(ApplicationKey.valueOf(entry.getKey()), entry.getValue().getNumberOfUsers());
            }

            this.roles = userCount;
            this.type = license.getLicenseType();
        }

        private static LicenseDetailsMatcher equals(License license) {
            return new LicenseDetailsMatcher(license);
        }

        @Override
        protected boolean matchesSafely(final LicenseDetails item, final Description mismatchDescription) {
            boolean descriptionEquals = Objects.equals(description, item.getDescription());
            boolean organisationEquals = Objects.equals(organisation, item.getOrganisation());
            boolean senEquals = Objects.equals(sen, item.getSupportEntitlementNumber());
            boolean evaluationEquals = item.isEvaluation() == evaluation;
            final Map<ApplicationKey, Integer> rolesMap = toRoleMap(item);
            boolean rolesEquals = roles.equals(rolesMap);
            boolean typeEquals = type == item.getLicenseType();
            boolean result = descriptionEquals && organisationEquals && senEquals && evaluationEquals
                    && rolesEquals && typeEquals;

            if (!result) {
                mismatchDescription.appendText(format("[description: %s, organisation: %s, sen: %s, isEval: %s, type: %s, roles: %s]",
                        item.getDescription(), item.getOrganisation(), item.getSupportEntitlementNumber(),
                        item.isEvaluation(), type, rolesMap));
            }
            return result;
        }

        @Override
        public void describeTo(final Description description) {
            description.appendText(format("[description: %s, organisation: %s, sen: %s, isEval: %s, type %s, roles: %s]",
                    this.description, organisation, sen, evaluation, type, roles));
        }

        private static Map<ApplicationKey, Integer> toRoleMap(LicenseDetails details) {
            Map<ApplicationKey, Integer> result = new HashMap<>();
            final LicensedApplications licensedApplications = details.getLicensedApplications();
            for (ApplicationKey applicationKey : licensedApplications.getKeys()) {
                result.put(applicationKey, licensedApplications.getUserLimit(applicationKey));
            }
            return result;
        }
    }
}
