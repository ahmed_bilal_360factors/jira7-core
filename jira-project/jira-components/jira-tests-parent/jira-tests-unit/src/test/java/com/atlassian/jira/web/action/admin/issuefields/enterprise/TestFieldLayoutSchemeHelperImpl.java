package com.atlassian.jira.web.action.admin.issuefields.enterprise;

import com.atlassian.jira.issue.fields.layout.field.EditableFieldLayout;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutManager;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutScheme;
import com.atlassian.jira.issue.search.SearchProvider;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.query.Query;
import com.atlassian.query.QueryImpl;
import com.atlassian.query.clause.Clause;
import com.atlassian.query.clause.TerminalClauseImpl;
import com.atlassian.query.operand.MultiValueOperand;
import com.atlassian.query.operator.Operator;
import com.atlassian.query.order.OrderByImpl;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.mockito.Mock;

import java.util.Collection;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestFieldLayoutSchemeHelperImpl {
    private final ApplicationUser user = new MockApplicationUser("test");
    @Rule
    public TestRule initMockito = MockitoMocksInContainer.forTest(this);
    @Mock
    private FieldLayoutScheme scheme;
    @Mock
    private FieldLayoutManager fieldLayoutManager;
    @Mock
    private SearchProvider searchProvider;

    private FieldLayoutSchemeHelperImpl helper;

    @Before
    public void setUp() throws Exception {
        helper = new FieldLayoutSchemeHelperImpl(fieldLayoutManager, searchProvider);
    }

    @Test
    public void testChangeFieldLayoutAssociationRequiresMessageVisiblyEquivalent() throws Exception {
        when(fieldLayoutManager.isFieldLayoutsVisiblyEquivalent(1L, 2L)).thenReturn(true);
        final FieldLayoutSchemeHelperImpl helper = new FieldLayoutSchemeHelperImpl(fieldLayoutManager, searchProvider);

        assertFalse("Should not require a message", helper.doesChangingFieldLayoutAssociationRequireMessage(user, scheme, 1L, 2L));
    }

    @Test
    public void testChangeFieldLayoutAssociationRequiresMessageDifferentSchemeHasNoProjects() throws Exception {
        when(scheme.getProjectsUsing()).thenReturn(ImmutableSet.of());
        when(fieldLayoutManager.isFieldLayoutsVisiblyEquivalent(1L, 2L)).thenReturn(false);

        assertFalse("Should not require a message", helper.doesChangingFieldLayoutAssociationRequireMessage(user, scheme, 1L, 2L));
    }

    @Test
    public void testChangeFieldLayoutAssociationRequiresMessageDifferentSchemeHasProjects() throws Exception {
        final Collection<Project> projects = ImmutableList.of(new MockProject(111L), new MockProject(222L)
        );
        when(scheme.getProjectsUsing()).thenReturn(projects);
        when(fieldLayoutManager.isFieldLayoutsVisiblyEquivalent(1L, 2L)).thenReturn(false);
        final Clause whereClause = new TerminalClauseImpl("project", Operator.IN, new MultiValueOperand(111L, 222L));
        final Query query = new QueryImpl(whereClause, new OrderByImpl(), null);
        when(searchProvider.searchCountOverrideSecurity(query, user)).thenReturn(1L);

        assertTrue("Should require a message", helper.doesChangingFieldLayoutAssociationRequireMessage(user, scheme, 1L, 2L));

    }

    @Test
    public void testChangeFieldLayoutRequiresMessageNoProjects() throws Exception {
        final EditableFieldLayout fieldLayout = mock(EditableFieldLayout.class);
        when(fieldLayoutManager.getProjectsUsing(fieldLayout)).thenReturn(ImmutableList.of());

        assertFalse("Should not require a message", helper.doesChangingFieldLayoutRequireMessage(user, fieldLayout));
    }

    @Test
    public void testChangeFieldLayoutRequiresMessageHasProjects() throws Exception {
        final Collection<Project> projects = ImmutableList.of(new MockProject(111L), new MockProject(222L));
        final EditableFieldLayout fieldLayout = mock(EditableFieldLayout.class);
        when(fieldLayoutManager.getProjectsUsing(fieldLayout)).thenReturn(projects);
        final Clause whereClause = new TerminalClauseImpl("project", Operator.IN, new MultiValueOperand(111L, 222L));
        final Query query = new QueryImpl(whereClause, new OrderByImpl(), null);
        when(searchProvider.searchCountOverrideSecurity(query, user)).thenReturn(1L);

        assertTrue("Should require a message", helper.doesChangingFieldLayoutRequireMessage(user, fieldLayout));
    }

    @Test
    public void testChangeFieldLayoutSchemeRequiresMessageVisiblyEquivalent() throws Exception {
        when(fieldLayoutManager.isFieldLayoutSchemesVisiblyEquivalent(10001L, 10002L)).thenReturn(true);

        assertFalse("Should not require a message", helper.doesChangingFieldLayoutSchemeForProjectRequireMessage(user, 1L, 10001L, 10002L));
    }

    @Test
    public void testChangeFieldLayoutSchemeRequiresMessageDifferent() throws Exception {
        final long projectId = 1L;
        when(fieldLayoutManager.isFieldLayoutSchemesVisiblyEquivalent(10001L, 10002L)).thenReturn(false);
        final Clause whereClause = new TerminalClauseImpl("project", Operator.IN, new MultiValueOperand(projectId));
        final Query query = new QueryImpl(whereClause, new OrderByImpl(), null);
        when(searchProvider.searchCountOverrideSecurity(query, user)).thenReturn(0L);

        assertFalse("Should not require a message", helper.doesChangingFieldLayoutSchemeForProjectRequireMessage(user, projectId, 10001L, 10002L));
    }
}
