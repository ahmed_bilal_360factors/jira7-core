package com.atlassian.jira.issue.customfields.searchers.transformer;

import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.search.managers.SearchHandlerManager;
import com.atlassian.jira.jql.ClauseHandler;
import com.atlassian.jira.user.ApplicationUser;
import com.google.common.collect.ImmutableList;
import org.junit.After;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestDefaultCustomFieldInputHelper {
    @Rule
    public final MethodRule initMockito = MockitoJUnit.rule();

    @Mock
    private SearchHandlerManager searchHandlerManager;

    private ApplicationUser searcher;
    private DefaultCustomFieldInputHelper helper;

    @After
    public void tearDown() {
        searchHandlerManager = null;
        searcher = null;
        helper = null;
    }

    @Test
    public void testGetClauseNameNameIsUnique() throws Exception {
        final String fieldName = "ABC";
        final String primaryName = "cf[10000]";
        final ClauseHandler clauseHandler = mock(ClauseHandler.class);
        CustomField customField = mock(CustomField.class);

        when(searchHandlerManager.getClauseHandler(searcher, fieldName)).thenReturn(ImmutableList.of(clauseHandler));
        when(customField.getName()).thenReturn(fieldName);

        helper = new DefaultCustomFieldInputHelper(searchHandlerManager);

        final String result = helper.getUniqueClauseName(searcher, primaryName, fieldName);
        assertEquals(fieldName, result);
    }

    @Test
    public void testGetClauseNameNameIsNotUnique() throws Exception {
        final String fieldName = "ABC";
        final String primaryName = "cf[10000]";
        final ClauseHandler clauseHandler1 = mock(ClauseHandler.class);
        final ClauseHandler clauseHandler2 = mock(ClauseHandler.class);
        CustomField customField = mock(CustomField.class);

        when(searchHandlerManager.getClauseHandler(searcher, fieldName)).thenReturn(ImmutableList.of(clauseHandler1, clauseHandler2));
        when(customField.getName()).thenReturn(fieldName);

        helper = new DefaultCustomFieldInputHelper(searchHandlerManager);

        final String result = helper.getUniqueClauseName(searcher, primaryName, fieldName);
        assertEquals(primaryName, result);
    }

    @Test
    public void testGetClauseNameNameIsSystemFieldName() throws Exception {
        final String fieldName = "project";
        final String primaryName = "cf[10000]";
        CustomField customField = mock(CustomField.class);

        when(customField.getName()).thenReturn(fieldName);

        helper = new DefaultCustomFieldInputHelper(searchHandlerManager);

        final String result = helper.getUniqueClauseName(searcher, primaryName, fieldName);
        assertEquals(primaryName, result);
    }

    @Test
    public void testGetClauseNameNameIsCustomFieldId() throws Exception {
        final String fieldName = "cf[12345]";
        final String primaryName = "cf[10000]";
        CustomField customField = mock(CustomField.class);

        when(customField.getName()).thenReturn(fieldName);

        helper = new DefaultCustomFieldInputHelper(searchHandlerManager);

        final String result = helper.getUniqueClauseName(searcher, primaryName, fieldName);
        assertEquals(primaryName, result);
    }
}
