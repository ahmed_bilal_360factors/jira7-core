package com.atlassian.jira.event.issue;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.history.ChangeItemBean;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Optional;

import static java.util.Optional.empty;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;

public class TestIssueChangedEventImpl {

    private static final String FIELD_TYPE = "";
    private static final String FIELD_NAME = "field";
    private static final String OLD_VALUE = "from";
    private static final String NEW_VALUE = "to";

    IssueChangedEvent issueChangedEvent;

    Collection<ChangeItemBean> changeItemBeans;

    @Before
    public void before() {
        changeItemBeans = new ArrayList<>();
        issueChangedEvent = new IssueChangedEventImpl(mock(Issue.class), empty(), changeItemBeans, empty(), new Date());
    }

    @Test
    public void getChangeItemForFieldWithDifferentOldAndNewValue() {
        changeItemBeans.add(new ChangeItemBean(FIELD_TYPE, FIELD_NAME, OLD_VALUE, NEW_VALUE));
        Optional<ChangeItemBean> changeItemBean = issueChangedEvent.getChangeItemForField(FIELD_NAME);
        assertThat(changeItemBean.isPresent(), is(true));
        assertThat(changeItemBean.get().getField(), is(FIELD_NAME));
    }

    @Test
    public void getChangeItemForFieldWithSameOldAndNewValue() {
        changeItemBeans.add(new ChangeItemBean(FIELD_TYPE, FIELD_NAME, OLD_VALUE, OLD_VALUE));
        Optional<ChangeItemBean> changeItemBean = issueChangedEvent.getChangeItemForField(FIELD_NAME);
        assertThat(changeItemBean.isPresent(), is(false));
    }
    @Test
    public void getChangeItemForFieldWithOldAndNewValueBothNull() {
        changeItemBeans.add(new ChangeItemBean(FIELD_TYPE, FIELD_NAME, null, null));
        Optional<ChangeItemBean> changeItemBean = issueChangedEvent.getChangeItemForField(FIELD_NAME);
        assertThat(changeItemBean.isPresent(), is(false));
    }
    @Test
    public void getChangeItemForFieldWithFromNullToAValue() {
        changeItemBeans.add(new ChangeItemBean(FIELD_TYPE, FIELD_NAME, null, NEW_VALUE));
        Optional<ChangeItemBean> changeItemBean = issueChangedEvent.getChangeItemForField(FIELD_NAME);
        assertThat(changeItemBean.isPresent(), is(true));
        assertThat(changeItemBean.get().getField(), is(FIELD_NAME));
    }
    @Test
    public void getChangeItemForFieldWithFromAValueToNull() {
        changeItemBeans.add(new ChangeItemBean(FIELD_TYPE, FIELD_NAME, OLD_VALUE, null));
        Optional<ChangeItemBean> changeItemBean = issueChangedEvent.getChangeItemForField(FIELD_NAME);
        assertThat(changeItemBean.isPresent(), is(true));
        assertThat(changeItemBean.get().getField(), is(FIELD_NAME));
    }
}
