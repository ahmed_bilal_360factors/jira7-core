package com.atlassian.jira.license;

import com.atlassian.jira.matchers.OptionMatchers;
import com.atlassian.jira.mock.MockApplicationProperties;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;

import static com.atlassian.jira.license.Jira6xServiceDeskPluginEncodedLicenseSupplier.UPM_SERVICE_DESK_LICENSE_DELETED_KEY;
import static com.atlassian.jira.license.Jira6xServiceDeskPluginEncodedLicenseSupplier.UPM_SERVICE_DESK_LICENSE_KEY;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;


public class Jira6xServiceDeskPluginEncodedLicenseSupplierTest {
    private MockApplicationProperties applicationProperties;
    private Jira6xServiceDeskPluginEncodedLicenseSupplier encodedLicenseSupplier;

    @Before
    public void setup() {
        applicationProperties = new MockApplicationProperties();
        encodedLicenseSupplier = new Jira6xServiceDeskPluginEncodedLicenseSupplier(applicationProperties);
    }

    @Test
    public void returnsPluginLicense() {
        String license = "goodLicense";

        applicationProperties.setText(UPM_SERVICE_DESK_LICENSE_KEY, license);
        assertThat(encodedLicenseSupplier.get(), OptionMatchers.some(license));
    }

    @Test
    public void returnsPluginLicenseWhenUpgradeLicenseExists() {
        String license = "goodLicense";
        String badLicense = "badLicense";

        applicationProperties.setText(UPM_SERVICE_DESK_LICENSE_KEY, license);
        applicationProperties.setText(UPM_SERVICE_DESK_LICENSE_DELETED_KEY, badLicense);
        assertThat(encodedLicenseSupplier.get(), OptionMatchers.some(license));
    }

    @Test
    public void returnsNoneWhenPluginLicenseDoesNotExist() {
        applicationProperties.setText(UPM_SERVICE_DESK_LICENSE_KEY, null);
        applicationProperties.setText(UPM_SERVICE_DESK_LICENSE_DELETED_KEY, "ignoreMe");
        assertThat(encodedLicenseSupplier.get(), OptionMatchers.none());
    }

    @Test
    public void returnsNoneWhenPluginLicenseIsBlank() {
        applicationProperties.setText(UPM_SERVICE_DESK_LICENSE_KEY, "     ");
        applicationProperties.setText(UPM_SERVICE_DESK_LICENSE_DELETED_KEY, "ignoreMe");
        assertThat(encodedLicenseSupplier.get(), OptionMatchers.none());
    }

    @Test
    public void returnsUpgradeLicense() {
        String license = "goodLicense";

        applicationProperties.setText(UPM_SERVICE_DESK_LICENSE_DELETED_KEY, license);
        assertThat(encodedLicenseSupplier.getUpgrade(), OptionMatchers.some(license));
    }

    @Test
    public void returnsUpgradeLicenseWhenPluginLicenseExists() {
        String license = "goodLicense";
        String badLicense = "badLicense";

        applicationProperties.setText(UPM_SERVICE_DESK_LICENSE_KEY, badLicense);
        applicationProperties.setText(UPM_SERVICE_DESK_LICENSE_DELETED_KEY, license);
        assertThat(encodedLicenseSupplier.getUpgrade(), OptionMatchers.some(license));
    }

    @Test
    public void returnsNoneWhenUpgradeLicenseDoesNotExist() {
        applicationProperties.setText(UPM_SERVICE_DESK_LICENSE_KEY, "ignoreMe");
        applicationProperties.setText(UPM_SERVICE_DESK_LICENSE_DELETED_KEY, null);

        assertThat(encodedLicenseSupplier.getUpgrade(), OptionMatchers.none());
    }

    @Test
    public void returnsNoneWhenUpgradeLicenseIsBlank() {
        applicationProperties.setText(UPM_SERVICE_DESK_LICENSE_KEY, "ignoreMe");
        applicationProperties.setText(UPM_SERVICE_DESK_LICENSE_DELETED_KEY, "     ");

        assertThat(encodedLicenseSupplier.getUpgrade(), OptionMatchers.none());
    }

    @Test
    public void returnsPluginLicenseWhenBothExist() {
        String pluginLicense = "goodLicense";
        String upgradeLicense = "badLicense";

        applicationProperties.setText(UPM_SERVICE_DESK_LICENSE_KEY, pluginLicense);
        applicationProperties.setText(UPM_SERVICE_DESK_LICENSE_DELETED_KEY, upgradeLicense);
        assertThat(encodedLicenseSupplier.getCurrentOrUpgrade(), OptionMatchers.some(pluginLicense));
    }

    @Test
    public void returnsUpgradeLicenseWhenPluginLicenseDoesNotExist() {
        String upgradeLicense = "badLicense";

        applicationProperties.setText(UPM_SERVICE_DESK_LICENSE_KEY, null);
        applicationProperties.setText(UPM_SERVICE_DESK_LICENSE_DELETED_KEY, upgradeLicense);
        assertThat(encodedLicenseSupplier.getCurrentOrUpgrade(), OptionMatchers.some(upgradeLicense));
    }

    @Test
    public void returnsUpgradeLicenseWhenPluginLicenseBlank() {
        String upgradeLicense = "badLicense";

        applicationProperties.setText(UPM_SERVICE_DESK_LICENSE_KEY, "      ");
        applicationProperties.setText(UPM_SERVICE_DESK_LICENSE_DELETED_KEY, upgradeLicense);
        assertThat(encodedLicenseSupplier.getCurrentOrUpgrade(), OptionMatchers.some(upgradeLicense));
    }

    @Test
    public void returnsNoneWhenPluginAndUpgradeLicenseDontExist() {
        applicationProperties.setText(UPM_SERVICE_DESK_LICENSE_KEY, null);
        applicationProperties.setText(UPM_SERVICE_DESK_LICENSE_DELETED_KEY, null);
        assertThat(encodedLicenseSupplier.getCurrentOrUpgrade(), OptionMatchers.none());
    }

    @Test
    public void returnsNoneWhenPluginAndUpgradeLicenseAreBlank() {
        applicationProperties.setText(UPM_SERVICE_DESK_LICENSE_KEY, "    ");
        applicationProperties.setText(UPM_SERVICE_DESK_LICENSE_DELETED_KEY, "\t\n");
        assertThat(encodedLicenseSupplier.getCurrentOrUpgrade(), OptionMatchers.none());
    }

    @Test
    public void moveToUpgradeStoreReplacesExistingUpgradeLicense() {
        final String license = "goodLicense";

        applicationProperties.setText(UPM_SERVICE_DESK_LICENSE_KEY, license);
        applicationProperties.setText(UPM_SERVICE_DESK_LICENSE_DELETED_KEY, "clearMe");

        encodedLicenseSupplier.moveToUpgradeStore();

        assertThat(applicationProperties.getString(UPM_SERVICE_DESK_LICENSE_KEY), Matchers.nullValue());
        assertThat(applicationProperties.getString(UPM_SERVICE_DESK_LICENSE_DELETED_KEY), is(license));
    }

    @Test
    public void moveToUpgradeStoreClearsExistingUpgradeLicenseWhenPluginLicenseDoesNotExist() {
        applicationProperties.setText(UPM_SERVICE_DESK_LICENSE_KEY, null);
        applicationProperties.setText(UPM_SERVICE_DESK_LICENSE_DELETED_KEY, "clearMe");

        encodedLicenseSupplier.moveToUpgradeStore();

        assertThat(applicationProperties.getString(UPM_SERVICE_DESK_LICENSE_KEY), Matchers.nullValue());
        assertThat(applicationProperties.getString(UPM_SERVICE_DESK_LICENSE_DELETED_KEY), Matchers.nullValue());
    }

    @Test
    public void moveToUpgradeStoreClearsExistingUpgradeLicenseWhenPluginLicenseIsBlank() {
        applicationProperties.setText(UPM_SERVICE_DESK_LICENSE_KEY, "      ");
        applicationProperties.setText(UPM_SERVICE_DESK_LICENSE_DELETED_KEY, "clearMe");

        encodedLicenseSupplier.moveToUpgradeStore();

        assertThat(applicationProperties.getString(UPM_SERVICE_DESK_LICENSE_KEY), Matchers.nullValue());
        assertThat(applicationProperties.getString(UPM_SERVICE_DESK_LICENSE_DELETED_KEY), Matchers.nullValue());

    }

    @Test
    public void clearRemovesPluginAndUpgradeLicense() {
        applicationProperties.setText(UPM_SERVICE_DESK_LICENSE_KEY, "plugin");
        applicationProperties.setText(UPM_SERVICE_DESK_LICENSE_DELETED_KEY, "upgrade");

        encodedLicenseSupplier.clear();

        assertThat(applicationProperties.getString(UPM_SERVICE_DESK_LICENSE_KEY), Matchers.nullValue());
        assertThat(applicationProperties.getString(UPM_SERVICE_DESK_LICENSE_DELETED_KEY), Matchers.nullValue());
    }
}