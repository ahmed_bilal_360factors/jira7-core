package com.atlassian.jira.cluster.zdu;

import com.atlassian.event.api.EventListenerRegistrar;
import com.atlassian.jira.cluster.ClusterMessageConsumer;
import com.atlassian.jira.cluster.MessageHandlerService;
import com.atlassian.jira.mock.cluster.zdu.MockNodeBuildInfo;
import com.atlassian.jira.plugin.freeze.FreezeFileManager;
import com.atlassian.jira.util.BuildUtilsInfo;
import com.atlassian.plugin.ModuleDescriptorFactory;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.loaders.PluginLoader;
import com.atlassian.plugin.loaders.classloading.Scanner;
import com.google.common.base.Charsets;
import com.google.common.io.Files;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.internal.matchers.CapturingMatcher;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.File;
import java.io.IOException;

import static com.atlassian.jira.cluster.zdu.ClusterUpgradeStateManager.CLUSTER_UPGRADE_STATE_CHANGED;
import static com.atlassian.jira.cluster.zdu.UpgradeState.READY_TO_UPGRADE;
import static com.atlassian.jira.cluster.zdu.UpgradeState.STABLE;
import static java.util.Collections.emptyList;
import static org.mockito.Matchers.argThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 */
@RunWith(MockitoJUnitRunner.class)
public class TestDefaultClusterPluginLoaderFactory {

    @Rule
    public TemporaryFolder temporaryFolder = new TemporaryFolder();

    @Mock
    private BuildUtilsInfo buildUtilsInfo;
    @Mock
    private EventListenerRegistrar eventListenerRegistrar;
    @Mock
    private ClusterUpgradeStateManager clusterUpgradeStateManager;
    @Mock
    private MessageHandlerService messageHandlerService;
    @Mock
    private Scanner installedPluginsScanner;
    @Mock
    private Scanner freezeFileScanner;
    @Mock
    private PluginEventManager pluginEventManager;
    @Mock
    private ModuleDescriptorFactory moduleDescripterFactory;
    @Mock
    private NodeBuildInfoFactory nodeBuildInfoFactory;
    @Mock
    private FreezeFileManager freezeFileManager;

    private DefaultClusterUpgradePluginLoaderFactory factory;
    private File freezeFile;
    private File installedPluginsDirectory;
    private CapturingMatcher<Object> eventListenerRegisterCall;
    private CapturingMatcher<ClusterMessageConsumer> clusterMessageConsumerCall;

    @Before
    public void setUp() throws IOException {
        mockUpgradeState(STABLE);
        mockApplicationBuildNumber(73000, "7.3.0");
        mockClusterBuildNumber(73000L, "7.3.0");

        File tmpFolder = temporaryFolder.newFolder();
        freezeFile = new File(tmpFolder, "test.list");
        installedPluginsDirectory = new File(tmpFolder, "installed-plugins");
        installedPluginsDirectory.mkdir();
        Files.asCharSink(freezeFile, Charsets.UTF_8).write("installed-plugins/plugin1\ninstalled-plugins/plugin2");
        factory = new DefaultClusterUpgradePluginLoaderFactory(eventListenerRegistrar,
                clusterUpgradeStateManager,
                messageHandlerService,
                buildUtilsInfo,
                pluginEventManager,
                f -> freezeFileScanner,
                (x, y) -> freezeFileManager,
                nodeBuildInfoFactory);

        eventListenerRegisterCall = new CapturingMatcher<>();
        doNothing().when(eventListenerRegistrar).register(argThat(eventListenerRegisterCall));

        clusterMessageConsumerCall = new CapturingMatcher<>();
        doNothing().when(messageHandlerService).registerListener(eq(CLUSTER_UPGRADE_STATE_CHANGED), argThat(clusterMessageConsumerCall));
    }

    @Test
    public void freezingOnUpgradeStartedPluginEvent() {
        PluginLoader loader = callCreate();
        ((ClusterUpgradeLifecycleListener) eventListenerRegisterCall.getLastValue()).onUpgradeStarted(null);

        loader.loadFoundPlugins(moduleDescripterFactory);

        verifyFreezeFileUsed();
    }

    private void verifyFreezeFileUsed() {
        verify(freezeFileScanner, times(1)).scan();
        verify(installedPluginsScanner, never()).scan();
    }
    private void verifyFreezeFileNotUsed() {
        verify(freezeFileScanner, never()).scan();
        verify(installedPluginsScanner, times(1)).scan();
    }

    @Test
    public void unfreezingOnUpgradeCancelledEvent() {
        PluginLoader loader = callCreate();
        ((ClusterUpgradeLifecycleListener) eventListenerRegisterCall.getLastValue()).onUpgradeStarted(null);

        ((ClusterUpgradeLifecycleListener) eventListenerRegisterCall.getLastValue()).onUpgradeCancelled(null);

        loader.loadFoundPlugins(moduleDescripterFactory);
        verifyFreezeFileNotUsed();
    }

    @Test
    public void pluginDeletionOnUpgradeCancelledEvent() throws IOException {
        PluginLoader loader = callCreate();
        ((ClusterUpgradeLifecycleListener) eventListenerRegisterCall.getLastValue()).onUpgradeStarted(null);

        ((ClusterUpgradeLifecycleListener) eventListenerRegisterCall.getLastValue()).onUpgradeCancelled(null);

        verify(freezeFileManager, times(1)).removeUnfrozenPlugins();
    }

    @Test
    public void unfreezingOnUpgradeCompletedEvent() {
        PluginLoader loader = callCreate();
        ((ClusterUpgradeLifecycleListener) eventListenerRegisterCall.getLastValue()).onUpgradeStarted(null);

        ((ClusterUpgradeLifecycleListener) eventListenerRegisterCall.getLastValue()).onUpgradeCompleted(null);

        loader.loadFoundPlugins(moduleDescripterFactory);
        verifyFreezeFileNotUsed();
    }

    @Test
    public void freezingViaClusterMessage_oldNode() {
        PluginLoader loader = callCreate();
        mockUpgradeState(READY_TO_UPGRADE);
        mockApplicationBuildNumber(73000, "7.3.0");
        mockClusterBuildNumber(73000L, "7.3.0");

        sendMockClusterMessage(READY_TO_UPGRADE);

        loader.loadFoundPlugins(moduleDescripterFactory);
        verifyFreezeFileUsed();
    }

    @Test
    public void freezingViaClusterMessage_newNode() {
        PluginLoader loader = callCreate();
        mockUpgradeState(READY_TO_UPGRADE);
        mockApplicationBuildNumber(73001, "7.3.0");
        mockClusterBuildNumber(73000L, "7.3.0");

        sendMockClusterMessage(READY_TO_UPGRADE);

        loader.loadFoundPlugins(moduleDescripterFactory);
        verifyFreezeFileNotUsed();
    }

    @Test
    public void unfreezingViaClusterMessage() {
        PluginLoader loader = callCreate();
        mockUpgradeState(STABLE);
        mockApplicationBuildNumber(73000, "7.3.0");
        mockClusterBuildNumber(73000L, "7.3.0");

        sendMockClusterMessage(STABLE);

        loader.loadFoundPlugins(moduleDescripterFactory);
        verifyFreezeFileNotUsed();
    }

    @Test
    public void frozenWhenStartingUpInUpgradeModeOnOldNode() {
        mockUpgradeState(READY_TO_UPGRADE);
        mockApplicationBuildNumber(73000, "7.3.0");
        mockClusterBuildNumber(73000L, "7.3.0");

        PluginLoader loader = callCreate();

        loader.loadFoundPlugins(moduleDescripterFactory);
        verifyFreezeFileUsed();
    }

    @Test
    public void unfrozenWhenStartingUpInUpgradeModeOnNewNode() {
        mockUpgradeState(READY_TO_UPGRADE);
        mockApplicationBuildNumber(73001, "7.3.0");
        mockClusterBuildNumber(73000L, "7.3.0");

        PluginLoader loader = callCreate();

        loader.loadFoundPlugins(moduleDescripterFactory);
        verifyFreezeFileNotUsed();
    }

    @Test
    public void unfrozenWhenStartingUpInUpgradeModeOnOldNode() {
        mockUpgradeState(STABLE);
        mockApplicationBuildNumber(73000, "7.3.0");
        mockClusterBuildNumber(73000L, "7.3.0");

        PluginLoader loader = callCreate();

        loader.loadFoundPlugins(moduleDescripterFactory);
        verifyFreezeFileNotUsed();
    }

    private void sendMockClusterMessage(final UpgradeState message) {
        clusterMessageConsumerCall.getLastValue().receive(CLUSTER_UPGRADE_STATE_CHANGED, message.toString(), "xxx");
    }

    private void mockApplicationBuildNumber(final int number, final String version) {
        when(nodeBuildInfoFactory.currentApplicationInfo()).thenReturn(new MockNodeBuildInfo(number, version));
    }

    private void mockUpgradeState(final UpgradeState state) {
        when(clusterUpgradeStateManager.getUpgradeState()).thenReturn(state);
    }

    private void mockClusterBuildNumber(final long buildNumber, final String version) {
        when(clusterUpgradeStateManager.getClusterBuildInfo()).thenReturn(new MockNodeBuildInfo(buildNumber, version));
    }

    private PluginLoader callCreate() {
        return factory.create(
                installedPluginsScanner,
                installedPluginsDirectory,
                freezeFile,
                emptyList()
        );
    }
}
