package com.atlassian.jira.issue.fields.csv.custom;

import com.atlassian.jira.issue.customfields.CustomFieldType;
import com.atlassian.jira.issue.customfields.impl.LabelsCFType;
import com.atlassian.jira.issue.label.Label;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.google.common.collect.ImmutableSet;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Locale;
import java.util.Set;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class LabelsCFTypeCsvExportTest extends CustomFieldMultiValueCsvExporterTest<Set<Label>> {
    @Mock
    private JiraAuthenticationContext jiraAuthenticationContext;

    @Override
    protected CustomFieldType<Set<Label>, ?> createField() {
        when(jiraAuthenticationContext.getLocale()).thenReturn(Locale.ENGLISH);
        return new LabelsCFType(jiraAuthenticationContext, null, null, null, null, null, null);
    }

    @Test
    public void allLabelsAreExportedAsSeparateValues() {
        whenFieldValueIs(ImmutableSet.of(label("a"), label("b")));
        assertExportedValue("a", "b");
    }

    @Test
    public void allLabelsAreExportedAsSorted() {
        whenFieldValueIs(ImmutableSet.of(label("b"), label("a")));
        assertExportedValue("a", "b");
    }

    private Label label(final String name) {
        return new Label(0L, 0L, name);
    }
}
