package com.atlassian.jira.action.issue.customfields.option;

import com.atlassian.jira.issue.customfields.manager.OptionsManager;
import com.atlassian.jira.issue.customfields.option.LazyLoadedOption;
import com.atlassian.jira.issue.customfields.option.Option;
import com.atlassian.jira.mock.ofbiz.MockGenericValue;
import com.atlassian.jira.ofbiz.FieldMap;
import com.google.common.collect.ImmutableMap;
import org.junit.Test;
import org.ofbiz.core.entity.GenericValue;

import java.util.Map;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

public class TestLazyLoadedOption {
    private final GenericValue parentOption = new MockGenericValue("CustomFieldOption", FieldMap.build(
            "id", 1L,
            "value", "cars",
            "sequence", 0L));
    private final MockGenericValue child1 = new MockGenericValue("CustomFieldOption", FieldMap.build(
            "id", 2L,
            "value", "ford",
            "parentoptionid", 1L,
            "sequence", 0L));
    private final MockGenericValue child2 = new MockGenericValue("CustomFieldOption", FieldMap.build(
            "id", 3L,
            "parentoptionid", 1L,
            "value", "holden",
            "sequence", 1L));
    private final Map<Long, GenericValue> options = ImmutableMap.of(
            1L, parentOption,
            2L, child1,
            3L, child2);
    private final OptionsManager mockOptionManager = new MockOptionManager(options);

    @Test
    public void testOptionsPopulatesBasicValues() {
        final Option option = new LazyLoadedOption(parentOption, mockOptionManager, null);

        assertThat(option.getValue(), is("cars"));
        assertThat(option.getOptionId(), is(1L));
        assertThat(option.getSequence(), is(0L));

        option.setSequence(5L);

        assertThat(option.getSequence(), is(5L));
        assertThat(option.getParentOption(), nullValue());
    }

    @Test
    public void testChildPopulation() {
        final Option option = new LazyLoadedOption(parentOption, mockOptionManager, null);
        assertThat(option.getChildOptions(), hasSize(2));
    }

    @Test
    public void testParentPopulation() {
        final Option option = new LazyLoadedOption(parentOption, mockOptionManager, null);

        assertThat(option.getParentOption(), nullValue());

        final Option child = new LazyLoadedOption(child1, mockOptionManager, null);

        assertThat(child.getParentOption(), not(nullValue()));
        assertThat(child.getParentOption(), is(option));
    }

    @Test
    public void retrieveAllChildrenAcceptsNullArgument() {
        final Option option = new LazyLoadedOption(parentOption, mockOptionManager, null);
        assertThat(option.retrieveAllChildren(null), hasSize(2));
    }
}
