package com.atlassian.jira.issue.fields.csv;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.export.FieldExportParts;
import com.atlassian.jira.issue.fields.PrioritySystemField;
import com.atlassian.jira.issue.priority.Priority;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;

import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.I18nHelper;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

public class PrioritySystemFieldCsvExportTest {
    @Rule
    public final RuleChain mockito = MockitoMocksInContainer.forTest(this);

    @Mock
    private JiraAuthenticationContext authenticationContext;

    @Mock
    private I18nHelper i18nHelper;

    private static final String PRIORITY_NAME = "Issue priority";

    @Mock
    private Issue issue;

    @Mock
    private Priority priority;

    @InjectMocks
    private PrioritySystemField field;

    @Before
    public void setUp() {
        when(priority.getNameTranslation()).thenReturn(PRIORITY_NAME);
        when(authenticationContext.getI18nHelper()).thenReturn(i18nHelper);
    }

    @Test
    public void testCsvRepresentationForExpectedTypeAndValues() {
        when(issue.getPriority()).thenReturn(priority);

        final FieldExportParts representation = field.getRepresentationFromIssue(issue);
        assertThat(representation.getParts(), hasSize(1));
        assertThat(representation.getParts().get(0).getValues()::iterator, contains(PRIORITY_NAME));
    }

    @Test
    public void testCsvRepresentationWhenThereIsNoPriority() {
        when(issue.getPriority()).thenReturn(null);

        final FieldExportParts representation = field.getRepresentationFromIssue(issue);
        assertThat(representation.getParts(), hasSize(1));
        assertThat(representation.getParts().get(0).getValues()::iterator, contains(""));
    }
}
