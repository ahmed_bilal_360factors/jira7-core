package com.atlassian.jira.upgrade;

import com.atlassian.jira.util.BuildUtilsInfo;
import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class DefaultUpgradeConstraintsTest {
    @Test
    public void targetDatabaseBuildNumberIsTheSameAsApplicationBuildNumber() {
        BuildUtilsInfo buildUtilsInfo = mock(BuildUtilsInfo.class);
        when(buildUtilsInfo.getApplicationBuildNumber()).thenReturn(1900);

        final DefaultUpgradeConstraints constraints = new DefaultUpgradeConstraints(buildUtilsInfo);

        assertThat(constraints.getTargetDatabaseBuildNumber(), is(1900));
    }

    @Test
    public void anyTaskNumberIsAllowedToRun() {
        final DefaultUpgradeConstraints constraints = new DefaultUpgradeConstraints(null);
        assertTrue(constraints.shouldRunTask("1"));
        assertTrue(constraints.shouldRunTask("70030"));
        assertTrue(constraints.shouldRunTask(Integer.toString(Integer.MAX_VALUE)));
    }
}