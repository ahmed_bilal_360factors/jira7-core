package com.atlassian.jira.web.action.admin.permission;

import com.atlassian.jira.JiraTestUtil;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.permission.PermissionSchemeManager;
import com.atlassian.jira.permission.ProjectPermissionSchemeHelper;
import com.atlassian.jira.permission.management.ProjectPermissionFeatureHelper;
import com.atlassian.jira.scheme.Scheme;
import com.atlassian.jira.scheme.SchemeManager;
import com.atlassian.jira.scheme.SchemeManagerFactory;
import com.atlassian.jira.util.AnswerWith;
import com.atlassian.jira.web.action.RedirectSanitiser;
import com.atlassian.webresource.api.assembler.PageBuilderService;
import com.atlassian.webresource.api.assembler.RequiredData;
import com.atlassian.webresource.api.assembler.RequiredResources;
import com.atlassian.webresource.api.assembler.WebResourceAssembler;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import webwork.action.Action;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestProjectPermissions {

    @Mock
    private PageBuilderService pageBuilderService;
    @Mock
    private PermissionSchemeManager permissionSchemeManager;
    @Mock
    private ProjectPermissionSchemeHelper projectPermissionSchemeHelper;
    @Mock
    private Scheme scheme;
    @Mock
    private WebResourceAssembler webResourceAssembler;
    @Mock
    private RequiredResources requiredResources;
    @Mock
    private RequiredData requiredData;
    @Mock
    private ProjectPermissionFeatureHelper projectPermissionFeatureHelper;
    @Mock
    private SchemeManagerFactory schemeManagerFactory;
    @Mock
    private SchemeManager schemeManager;
    @Mock
    @AvailableInContainer
    private RedirectSanitiser redirectSanitiser;

    private ProjectPermissions projectPermissions;

    @Rule
    public final RuleChain ruleChain = MockitoMocksInContainer.forTest(this);

    @Before
    public void setup() {
        when(permissionSchemeManager.getSchemeObject(anyLong())).thenReturn(scheme);
        when(redirectSanitiser.makeSafeRedirectUrl(anyString())).thenAnswer(AnswerWith.firstParameter());
        when(pageBuilderService.assembler()).thenReturn(webResourceAssembler);
        when(webResourceAssembler.resources()).thenReturn(requiredResources);
        when(webResourceAssembler.data()).thenReturn(requiredData);
        when(schemeManagerFactory.getSchemeManager(anyString())).thenReturn(schemeManager);
        JiraTestUtil.resetRequestAndResponse();

        projectPermissions = new ProjectPermissions(pageBuilderService, permissionSchemeManager, projectPermissionSchemeHelper, projectPermissionFeatureHelper, schemeManagerFactory);
        projectPermissions.setSchemeId(100);
    }

    @After
    public void clean() {
        JiraTestUtil.resetRequestAndResponse();
    }

    @Test
    public void shouldProceedWithSuccessWithSchemeIdIsValid() throws Exception {
        final String result = projectPermissions.doDefault();

        assertThat("a valid id results in success, so the page can be correctly rendered", result, is(Action.SUCCESS));
    }

    @Test
    public void shouldRedirectBackToTheListingIfIdIsInvalid() throws Exception {
        when(permissionSchemeManager.getSchemeObject(anyLong())).thenReturn(null);
        projectPermissions.setSchemeId(777);

        projectPermissions.doDefault();

        verify(redirectSanitiser, atLeastOnce()).makeSafeRedirectUrl(argThat(containsString("ViewPermissionSchemes.jspa?invalidPermissionSchemeRequested=777")));
    }
}
