package com.atlassian.jira.user.flag;

import com.atlassian.core.util.Clock;
import com.atlassian.jira.propertyset.JiraPropertySetFactory;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.preferences.ExtendedPreferences;
import com.atlassian.jira.user.preferences.UserPreferencesManager;
import com.atlassian.jira.util.json.JSONObject;
import com.atlassian.mock.propertyset.MockPropertySet;
import com.opensymphony.module.propertyset.PropertySet;
import org.hamcrest.Matcher;
import org.hamcrest.collection.IsEmptyCollection;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;

import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Maps.newHashMap;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.argThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;


public class TestFlagDismissalServiceImpl {
    private static final String FLAG_KEY = "flag.key";
    private static final String DISMISSALS_KEY = "com.atlassian.jira.flag.dismissals";
    private static final String RESETS_KEY = "com.atlassian.jira.flag.resets";
    private static final long DISMISSAL_TIME = 1234L;

    @Mock
    private ExtendedPreferences extendedPreferences;
    @Mock
    private UserPreferencesManager userPreferencesManager;
    @Mock
    private JiraPropertySetFactory jiraPropertySetFactory;
    @Mock
    private PropertySet propertySet = new MockPropertySet();
    @Mock
    private Clock clock;
    @Mock
    private Date date;
    @Mock
    private ApplicationUser user;
    private FlagDismissalServiceImpl flagDismissalService;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        flagDismissalService = new FlagDismissalServiceImpl(
                userPreferencesManager,
                jiraPropertySetFactory,
                clock);

        when(userPreferencesManager
                .getExtendedPreferences(user))
                .thenReturn(extendedPreferences);
        when(extendedPreferences.getText(anyString())).thenReturn("");
        when(clock.getCurrentDate()).thenReturn(date);
        when(date.getTime()).thenReturn(DISMISSAL_TIME);

        when(jiraPropertySetFactory
                .buildCachingDefaultPropertySet(anyString()))
                .thenReturn(propertySet);
    }

    @Test
    public void removeDismissFlagForUserWhenUserIsNullIsNoop() {
        flagDismissalService.removeDismissFlagForUser("something", null);

        verifyNoMoreInteractions(userPreferencesManager);
        verifyNoMoreInteractions(propertySet);
        verifyNoMoreInteractions(extendedPreferences);
    }

    @Test
    public void removeDismissFlagForUserWhenFlagIsBlankIsNoop() {
        flagDismissalService.removeDismissFlagForUser("", user);
        flagDismissalService.removeDismissFlagForUser(null, user);

        verifyNoMoreInteractions(userPreferencesManager);
        verifyNoMoreInteractions(propertySet);
        verifyNoMoreInteractions(extendedPreferences);
    }

    @Test
    public void removeDismissFlagForUserRemovesFlagFromUser() throws Exception {
        //given
        JSONObject jsonObject = new JSONObject();
        jsonObject.put(FLAG_KEY, DISMISSAL_TIME);
        final ArgumentCaptor<String> captor = ArgumentCaptor.forClass(String.class);

        when(extendedPreferences.getText(DISMISSALS_KEY)).thenReturn(jsonObject.toString());
        doNothing().when(extendedPreferences).setText(eq(DISMISSALS_KEY), captor.capture());

        //when
        flagDismissalService.removeDismissFlagForUser(FLAG_KEY, user);

        //then
        JSONObject object = new JSONObject(captor.getValue());
        assertThat(object.opt(FLAG_KEY), nullValue());
    }

    @Test
    public void dismissFlagForUserIsNoOpWhenUserIsNull() throws Exception {
        flagDismissalService.dismissFlagForUser(FLAG_KEY, null);
        verifyNoMoreInteractions(userPreferencesManager);
        verifyNoMoreInteractions(jiraPropertySetFactory);
    }

    @Test
    public void dismissFlagForUserIsNoOpWhenFlagKeyIsBlank() throws Exception {
        flagDismissalService.dismissFlagForUser("", user);
        verifyNoMoreInteractions(userPreferencesManager);
        verifyNoMoreInteractions(jiraPropertySetFactory);
    }

    @Test
    public void dismissFlagForUserIsNoOpWhenFlagKeyIsNull() throws Exception {
        flagDismissalService.dismissFlagForUser(null, user);
        verifyNoMoreInteractions(userPreferencesManager);
        verifyNoMoreInteractions(jiraPropertySetFactory);
    }

    @Test
    public void dismissFlagForUserSavesDismissalsAgainstProvidedUser()
            throws Exception {
        flagDismissalService.dismissFlagForUser(FLAG_KEY, user);
        verify(extendedPreferences).getText(DISMISSALS_KEY);
        verify(extendedPreferences).setText(
                eq(DISMISSALS_KEY),
                anyString());
    }

    @Test
    public void dismissFlagForUserCreatesDismissalEntryWhenNoneExistsForUser()
            throws Exception {
        when(extendedPreferences.getText(anyString())).thenReturn(null);
        flagDismissalService.dismissFlagForUser(FLAG_KEY, user);
        verify(extendedPreferences).setText(eq(DISMISSALS_KEY), argThat(isJsonWithLongEntry(FLAG_KEY, DISMISSAL_TIME)));
    }

    @Test
    public void dismissFlagForUserUpdatesExistingDismissalEntryWhenOneExists()
            throws Exception {
        when(extendedPreferences.getText(anyString())).thenReturn(asJsonString(FLAG_KEY, 123L));
        flagDismissalService.dismissFlagForUser(FLAG_KEY, user);
        verify(extendedPreferences).setText(eq(DISMISSALS_KEY), argThat(isJsonWithLongEntry(FLAG_KEY, DISMISSAL_TIME)));
    }

    @Test
    public void dismissFlagForUserPreservesOtherDismissalsWhenTheyArePresent()
            throws Exception {
        when(extendedPreferences.getText(anyString())).thenReturn(asJsonString("other.flag.key", 10L));
        flagDismissalService.dismissFlagForUser(FLAG_KEY, user);
        verify(extendedPreferences).setText(eq(DISMISSALS_KEY), argThat(isJsonWithLongEntry(FLAG_KEY, DISMISSAL_TIME)));
        verify(extendedPreferences).setText(eq(DISMISSALS_KEY), argThat(isJsonWithLongEntry("other.flag.key", 10L)));
    }

    @Test
    public void testGetDismissedFlagsForUserReturnsDismissedFlagsForUser() throws Exception {
        when(extendedPreferences
                .getText(DISMISSALS_KEY))
                .thenReturn(asJsonString(FLAG_KEY, 1L, "other.flag.key", 2L));
        Collection<String> flags = flagDismissalService.getDismissedFlagsForUser(user);
        assertThat(flags, containsInAnyOrder(FLAG_KEY, "other.flag.key"));
    }

    @Test
    public void testGetDismissedFlagsForUserReturnsEmptyListWhenUserHasNoDismissals()
            throws Exception {
        when(extendedPreferences.getText(DISMISSALS_KEY)).thenReturn(null);
        Collection<String> flags = flagDismissalService.getDismissedFlagsForUser(user);
        assertThat(flags, IsEmptyCollection.<String>empty());
    }

    @Test
    public void testGetDismissedFlagsForUserExcludesResetFlags()
            throws Exception {
        when(extendedPreferences
                .getText(DISMISSALS_KEY))
                .thenReturn(asJsonString(FLAG_KEY, 999L));
        when(propertySet.getText(RESETS_KEY)).thenReturn(asJsonString(FLAG_KEY, 1000L));
        Collection<String> flags = flagDismissalService.getDismissedFlagsForUser(user);
        assertThat(flags, IsEmptyCollection.<String>empty());
    }

    @Test
    public void testGetDismissedFlagsForUserIncludesFlagsDismissedSinceReset() throws Exception {
        when(extendedPreferences
                .getText(DISMISSALS_KEY))
                .thenReturn(asJsonString(FLAG_KEY, 1000L));
        when(propertySet.getText(RESETS_KEY)).thenReturn(asJsonString(FLAG_KEY, 999L));
        Collection<String> flags = flagDismissalService.getDismissedFlagsForUser(user);
        assertThat(flags, containsInAnyOrder(FLAG_KEY));
    }

    @Test
    public void testResetFlagDismissalsStoresTimestampAgainstFlag()
            throws Exception {
        flagDismissalService.resetFlagDismissals(FLAG_KEY);
        verify(propertySet).setText(
                eq(RESETS_KEY), argThat(isJsonWithLongEntry(FLAG_KEY, DISMISSAL_TIME)));
    }

    private String asJsonString(Object... keysAndValues) {
        Iterator<Object> entries = newArrayList(keysAndValues).iterator();
        Map<String, Object> jsonEntries = newHashMap();
        while (entries.hasNext()) {
            jsonEntries.put(
                    String.valueOf(entries.next()),
                    entries.hasNext() ? entries.next() : null);
        }
        return new JSONObject(jsonEntries).toString();
    }

    private static Matcher<String> isJsonWithLongEntry(final String key, final long value) {
        return new JsonLongMatcher(key, value);
    }

}
