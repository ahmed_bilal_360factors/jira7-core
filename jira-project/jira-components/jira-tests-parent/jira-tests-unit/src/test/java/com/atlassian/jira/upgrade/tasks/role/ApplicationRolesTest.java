package com.atlassian.jira.upgrade.tasks.role;

import com.atlassian.fugue.Option;
import com.atlassian.jira.application.ApplicationKeys;
import com.atlassian.jira.matchers.OptionMatchers;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import org.junit.Test;

import static com.atlassian.jira.application.ApplicationKeys.CORE;
import static com.atlassian.jira.upgrade.tasks.role.ApplicationRole.forKey;
import static com.atlassian.jira.upgrade.tasks.role.TestUtils.asGroups;
import static com.atlassian.jira.upgrade.tasks.role.TestUtils.newGroup;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;

public class ApplicationRolesTest {
    @Test(expected = MigrationFailedException.class)
    public void cotrRejectsDuplicateRoles() {
        //when
        new ApplicationRoles(ImmutableList.of(ApplicationRole.forKey(ApplicationKeys.CORE),
                ApplicationRole.forKey(ApplicationKeys.CORE).addGroupAsDefault(newGroup("other"))));
    }

    @Test
    public void cotrIndexesRoles() {
        //given
        final ApplicationRole coreRole = forKey(CORE).addGroupAsDefault(newGroup("core"));
        final ApplicationRole softwareRole = ApplicationRole.forKey(ApplicationKeys.SOFTWARE);

        //when
        final ApplicationRoles roles = new ApplicationRoles(ImmutableList.of(coreRole, softwareRole));

        //then
        assertThat(roles, new ApplicationRolesMatcher()
                .role(ApplicationKeys.CORE, asGroups("core"), asGroups("core"))
                .role(ApplicationKeys.SOFTWARE));
    }

    @Test
    public void getReturnsNothingWhenRoleNotPresent() {
        //given
        final ApplicationRole coreRole = forKey(CORE).addGroupAsDefault(newGroup("core"));
        final ApplicationRoles roles = new ApplicationRoles(ImmutableList.of(coreRole));

        //when
        final Option<ApplicationRole> actual = roles.get(ApplicationKeys.SERVICE_DESK);

        //then
        assertThat(actual, OptionMatchers.none());
    }

    @Test
    public void getReturnsSomethingWhenPresent() {
        //given
        final ApplicationRole coreRole = forKey(CORE).addGroupAsDefault(newGroup("core"));
        final ApplicationRoles roles = new ApplicationRoles(ImmutableList.of(coreRole));

        //when
        final Option<ApplicationRole> actual = roles.get(ApplicationKeys.CORE);

        //then
        assertThat(actual,
                OptionMatchers.some(ApplicationRoleMatcher.forKey(ApplicationKeys.CORE)
                        .withGroups("core").withDefaultGroups("core")));
    }

    @Test
    public void putDoesNotChangeExistingRoles() {
        //given
        final ApplicationRole coreRole = forKey(CORE).addGroupAsDefault(newGroup("core"));
        final ApplicationRole coreRoleNew = ApplicationRole.forKey(ApplicationKeys.CORE);
        final ApplicationRoles roles = new ApplicationRoles(ImmutableList.of(coreRole));

        //when
        final ApplicationRoles newRoles = roles.put(coreRoleNew);

        //then
        assertThat(roles, new ApplicationRolesMatcher()
                .role(ApplicationKeys.CORE, asGroups("core"), asGroups("core")));
        assertThat(newRoles, new ApplicationRolesMatcher()
                .role(ApplicationKeys.CORE));
    }

    @Test
    public void putOverwritesExistingRole() {
        //given
        final ApplicationRole coreRole = forKey(CORE).addGroupAsDefault(newGroup("core"));
        final ApplicationRole softwareRole = ApplicationRole.forKey(ApplicationKeys.SOFTWARE);
        final ApplicationRoles roles = new ApplicationRoles(ImmutableList.of(coreRole));

        //when
        final ApplicationRoles newRoles = roles.put(softwareRole);

        //then
        assertThat(roles, new ApplicationRolesMatcher()
                .role(ApplicationKeys.CORE, asGroups("core"), asGroups("core")));
        assertThat(newRoles, new ApplicationRolesMatcher()
                .role(ApplicationKeys.CORE, asGroups("core"), asGroups("core"))
                .role(ApplicationKeys.SOFTWARE));
    }

    @Test
    public void equalityOnlyCaresAboutRoles() {
        //given
        final ApplicationRole coreRole = ApplicationRole.forKey(ApplicationKeys.CORE)
                .withGroups(ImmutableSet.of(), ImmutableSet.of());

        final ApplicationRole coreRole2 = ApplicationRole.forKey(ApplicationKeys.CORE)
                .withGroups(ImmutableSet.of(), ImmutableSet.of());

        final ApplicationRole differentCoreRole = ApplicationRole.forKey(ApplicationKeys.CORE)
                .withGroups(asGroups("core"), ImmutableSet.of());

        final ApplicationRole softwareRole = ApplicationRole.forKey(ApplicationKeys.SOFTWARE)
                .withGroups(ImmutableSet.of(), ImmutableSet.of());


        ApplicationRoles coreRoles = new ApplicationRoles(ImmutableList.of(coreRole));
        ApplicationRoles coreRoles2 = new ApplicationRoles(ImmutableList.of(coreRole2));
        ApplicationRoles coreAndSoftware = new ApplicationRoles(ImmutableList.of(coreRole, softwareRole));
        ApplicationRoles differentCoreRoles = new ApplicationRoles(ImmutableList.of(differentCoreRole));

        //then
        assertThat(coreRoles, equalTo(coreRoles2));
        assertThat(coreRoles2, equalTo(coreRoles));
        assertThat(coreRoles.hashCode(), equalTo(coreRoles2.hashCode()));
        assertThat(coreRoles2.hashCode(), equalTo(coreRoles.hashCode()));

        assertThat(coreRole, not(equalTo(coreAndSoftware)));
        assertThat(coreAndSoftware, not(equalTo(coreRole)));
        assertThat(coreRole2, not(equalTo(coreAndSoftware)));
        assertThat(coreAndSoftware, not(equalTo(coreRole2)));

        assertThat(coreRole, not(equalTo(differentCoreRoles)));
        assertThat(differentCoreRoles, not(equalTo(coreRole)));
        assertThat(coreRole2, not(equalTo(differentCoreRoles)));
        assertThat(differentCoreRoles, not(equalTo(coreRole2)));
    }
}