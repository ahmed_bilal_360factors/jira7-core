package com.atlassian.jira.permission.management;

import com.atlassian.fugue.Option;
import com.atlassian.jira.mock.ofbiz.MockGenericValue;
import com.atlassian.jira.permission.JiraPermissionHolderType;
import com.atlassian.jira.permission.PermissionGrantInput;
import com.atlassian.jira.permission.PermissionGrantValidator;
import com.atlassian.jira.permission.PermissionSchemeManager;
import com.atlassian.jira.permission.PermissionTypeManager;
import com.atlassian.jira.permission.ProjectPermission;
import com.atlassian.jira.permission.management.beans.GrantToPermissionInputBean;
import com.atlassian.jira.permission.management.beans.PermissionsInputBean;
import com.atlassian.jira.scheme.Scheme;
import com.atlassian.jira.scheme.SchemeEntity;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.plugin.ProjectPermissionKey;
import com.atlassian.jira.security.type.SecurityType;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockUserKeyService;
import com.atlassian.jira.util.AnswerWith;
import com.atlassian.jira.util.I18nHelper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.ofbiz.core.entity.GenericEntityException;
import org.ofbiz.core.entity.GenericValue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static com.atlassian.jira.util.ErrorCollections.empty;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ManagedPermissionSchemeEditingServiceImplTest {
    @Mock
    private PermissionSchemeManager permissionSchemeManager;
    @Mock
    private I18nHelper i18nHelper;
    @Mock
    private PermissionGrantValidator permissionGrantValidator;
    @Mock
    private PermissionTypeManager permissionTypeManager;
    @Mock
    private PermissionManager permissionManager;
    @Mock
    private PermissionsInputBean permissionsInputBean;
    @Mock
    private ProjectPermission projectPermission;
    @Mock
    private GrantToPermissionInputBean grantToPermissionInputBean;
    @Mock
    private ApplicationUser applicationUser;
    @Mock
    private Scheme scheme;
    @Mock
    private SecurityType securityType;
    final MockUserKeyService userKeyService = new MockUserKeyService();

    ManagedPermissionSchemeEditingService managedPermissionSchemeEditingService;

    @Before
    public void setup() {
        when(i18nHelper.getText(anyString())).thenAnswer(AnswerWith.firstParameter());
        when(i18nHelper.getText(anyString(), anyString())).thenAnswer(AnswerWith.firstParameter());
        when(i18nHelper.getText(anyString(), anyString(), anyString())).thenAnswer(AnswerWith.firstParameter());

        managedPermissionSchemeEditingService = new ManagedPermissionSchemeEditingServiceImpl(permissionSchemeManager,
                i18nHelper, permissionGrantValidator, permissionTypeManager, permissionManager, userKeyService);
    }

    @Test
    public void emptyDataShouldReturnErrorAboutPermissionMustBeSelected() {
        when(permissionsInputBean.getPermissionKeys()).thenReturn(null);

        final Collection<String> errorCollection = managedPermissionSchemeEditingService.validateAddPermissions(applicationUser, permissionsInputBean).getErrorMessages();

        assertThat("list of errors should contain a single message", errorCollection.size(), is(1));
        assertThat("appropriate error message should have been set", errorCollection.iterator().next(), is(ManagedPermissionSchemeEditingServiceImpl.ErrorMessages.MUST_SELECT_PERMISSION_ERROR.getKey()));
    }

    @Test
    public void emptyListOfPermissionsShouldReturnErrorMessage() {
        when(permissionsInputBean.getPermissionKeys()).thenReturn(Collections.emptyList());

        final Collection<String> errorCollection = managedPermissionSchemeEditingService.validateAddPermissions(applicationUser, permissionsInputBean).getErrorMessages();

        assertThat("list of errors should contain a single message", errorCollection.size(), is(1));
        assertThat("appropriate error message should have been set", errorCollection.iterator().next(), is(ManagedPermissionSchemeEditingServiceImpl.ErrorMessages.MUST_SELECT_PERMISSION_ERROR.getKey()));
    }

    @Test
    public void nullGrantsListShouldReturnErrorMessage() {
        when(permissionsInputBean.getPermissionKeys()).thenReturn(Arrays.asList("some-random-key"));
        when(permissionsInputBean.getGrants()).thenReturn(null);

        final Collection<String> errorCollection = managedPermissionSchemeEditingService.validateAddPermissions(applicationUser, permissionsInputBean).getErrorMessages();

        assertThat("list of errors should contain a single message", errorCollection.size(), is(1));
        assertThat("appropriate error message should have been set", errorCollection.iterator().next(), is(ManagedPermissionSchemeEditingServiceImpl.ErrorMessages.MUST_SELECT_GRANT_TYPE_ERROR.getKey()));
    }

    @Test
    public void emptyGrantsListShouldReturnErrorMessage() {
        when(permissionsInputBean.getPermissionKeys()).thenReturn(Arrays.asList("some-random-key"));
        when(permissionsInputBean.getGrants()).thenReturn(Collections.emptyList());

        final Collection<String> errorCollection = managedPermissionSchemeEditingService.validateAddPermissions(applicationUser, permissionsInputBean).getErrorMessages();

        assertThat("list of errors should contain a single message", errorCollection.size(), is(1));
        assertThat("appropriate error message should have been set", errorCollection.iterator().next(), is(ManagedPermissionSchemeEditingServiceImpl.ErrorMessages.MUST_SELECT_GRANT_TYPE_ERROR.getKey()));
    }

    @Test
    public void pluggableButNonExistentSecurityTypeKeyShouldReturnErrorMessage() {
        when(permissionsInputBean.getPermissionKeys()).thenReturn(Arrays.asList("some-random-key"));
        when(grantToPermissionInputBean.getSecurityType()).thenReturn("blafoobar"); // any value that is not listed in JiraPermissionHolderType
        when(permissionsInputBean.getGrants()).thenReturn(Arrays.asList(grantToPermissionInputBean));

        final Collection<String> errorCollection = managedPermissionSchemeEditingService.validateAddPermissions(applicationUser, permissionsInputBean).getErrorMessages();

        assertThat("list of errors should contain a single message", errorCollection.size(), is(1));
        assertThat("appropriate error message should have been set", errorCollection.iterator().next(), is(ManagedPermissionSchemeEditingServiceImpl.ErrorMessages.NONEXISTENT_PERMISSION.getKey()));
    }

    @Test
    public void invalidCombinationOfPluggablePermissionAndGrantShouldReturnErrorMessage() {
        when(permissionsInputBean.getPermissionKeys()).thenReturn(Arrays.asList("some-random-key"));
        when(grantToPermissionInputBean.getSecurityType()).thenReturn("blafoobar"); // any value that is not listed in JiraPermissionHolderType
        when(permissionsInputBean.getGrants()).thenReturn(Arrays.asList(grantToPermissionInputBean));
        when(permissionTypeManager.getSchemeType(anyString())).thenReturn(securityType);
        when(securityType.isValidForPermission(any(ProjectPermissionKey.class))).thenReturn(false);
        when(permissionManager.getProjectPermission(any(ProjectPermissionKey.class))).thenReturn(Option.option(projectPermission));

        final Collection<String> errorCollection = managedPermissionSchemeEditingService.validateAddPermissions(applicationUser, permissionsInputBean).getErrorMessages();

        assertThat("list of errors should contain a single message", errorCollection.size(), is(1));
        assertThat("appropriate error message should have been set", errorCollection.iterator().next(), is(ManagedPermissionSchemeEditingServiceImpl.ErrorMessages.INVALID_PERMISSION_PLUS_GRANT_COMBINATION_ERROR.getKey()));
    }

    @Test
    public void validCombinationOfPermissionAndGrantShouldReturnEmptyErrorCollection() {
        when(permissionsInputBean.getPermissionKeys()).thenReturn(Arrays.asList("another-permission-key"));
        when(grantToPermissionInputBean.getSecurityType()).thenReturn("group");
        when(permissionsInputBean.getGrants()).thenReturn(Arrays.asList(grantToPermissionInputBean));
        when(permissionGrantValidator.validateGrant(any(ApplicationUser.class), any(PermissionGrantInput.class))).thenReturn(empty());

        final Collection<String> errorCollection = managedPermissionSchemeEditingService.validateAddPermissions(applicationUser, permissionsInputBean).getErrorMessages();

        assertThat("list of errors should be empty", errorCollection.size(), is(0));
    }

    @Test
    public void validCombinationOfPluggablePermissionAndGrantShouldReturnEmptyErrorCollection() {
        when(permissionsInputBean.getPermissionKeys()).thenReturn(Arrays.asList("some-random-key"));
        when(grantToPermissionInputBean.getSecurityType()).thenReturn("blafoobar"); // any value that is not listed in JiraPermissionHolderType
        when(permissionsInputBean.getGrants()).thenReturn(Arrays.asList(grantToPermissionInputBean));
        when(permissionTypeManager.getSchemeType(anyString())).thenReturn(securityType);
        when(securityType.isValidForPermission(any(ProjectPermissionKey.class))).thenReturn(true);

        final Collection<String> errorCollection = managedPermissionSchemeEditingService.validateAddPermissions(applicationUser, permissionsInputBean).getErrorMessages();

        assertThat("list of errors should be empty", errorCollection.size(), is(0));
    }

    @Test
    public void addingNewPermissionRejectIfInvalidValuesAreProvided() throws GenericEntityException {
        // Given
        final PermissionsInputBean inputBean = mockPermission("permKey1", "type1", "value1");

        // When
        when(permissionSchemeManager.createSchemeEntity(any(GenericValue.class), any(SchemeEntity.class))).thenReturn(new MockGenericValue("entry"));
        final boolean added = managedPermissionSchemeEditingService.addNewSecurityTypes(scheme, inputBean);

        // Then
        assertThat("New Permission not added", added, is(true));
    }

    @Test
    public void addingUserPermissionTypeToSchemeShouldSaveUserKeyNotUserName() throws GenericEntityException {
        // Given
        final long schemeId = 123L;
        final String permKey1 = "permKey1";
        final String userName = "userName";
        final String userKey = "userKey";

        userKeyService.setMapping(userKey, userName);
        final PermissionsInputBean inputBean = mockPermission(permKey1, JiraPermissionHolderType.USER.getKey(), userName);

        MockGenericValue genericValue = new MockGenericValue(""+schemeId);
        when(permissionSchemeManager.getScheme(schemeId)).thenReturn(genericValue);
        SchemeEntity expectedSchemeEntry = new SchemeEntity(
                null,
                JiraPermissionHolderType.USER.getKey(),
                userKey,
                permKey1,
                null,
                schemeId
        );
        when(scheme.getId()).thenReturn(schemeId);
        when(permissionSchemeManager.createSchemeEntity(genericValue, expectedSchemeEntry)).thenReturn(new MockGenericValue("entry"));

        // When
        final boolean added = managedPermissionSchemeEditingService.addNewSecurityTypes(scheme, inputBean);

        // Then
        assertThat("New Permission not added", added, is(true));
    }

    @Test
    public void addingUserPermissionTypeToSchemeShouldSaveFailWhenNameSaved() throws GenericEntityException {
        // Given
        final long schemeId = 123L;
        final String permKey1 = "permKey1";
        final String userName = "userName";
        final String userKey = "userKey";

        userKeyService.setMapping(userKey, userName);
        final PermissionsInputBean inputBean = mockPermission(permKey1, JiraPermissionHolderType.USER.getKey(), userName);

        MockGenericValue genericValue = new MockGenericValue(""+schemeId);
        when(permissionSchemeManager.getScheme(schemeId)).thenReturn(genericValue);
        SchemeEntity expectedSchemeEntry = new SchemeEntity(
                null,
                JiraPermissionHolderType.USER.getKey(),
                userName,
                permKey1,
                null,
                schemeId
        );
        when(scheme.getId()).thenReturn(schemeId);
        when(permissionSchemeManager.createSchemeEntity(genericValue, expectedSchemeEntry)).thenReturn(new MockGenericValue("entry"));

        // When
        final boolean added = managedPermissionSchemeEditingService.addNewSecurityTypes(scheme, inputBean);

        // Then
        assertThat("Should not save the user's user name, user key expected", added, is(false));
    }

    private PermissionsInputBean mockPermission(String permissionKey, String permissionType, String permissionTypeValue) {
        List<String> permissionKeys = new ArrayList<>();
        permissionKeys.add(permissionKey);
        List<GrantToPermissionInputBean> grants = new ArrayList<>();
        grants.add(new GrantToPermissionInputBean(permissionType, permissionTypeValue));
        return new PermissionsInputBean(permissionKeys, grants);
    }
}
