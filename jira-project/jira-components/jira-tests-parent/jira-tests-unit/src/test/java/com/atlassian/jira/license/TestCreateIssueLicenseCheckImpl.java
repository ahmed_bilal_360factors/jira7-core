package com.atlassian.jira.license;

import com.atlassian.jira.application.ApplicationRoleManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.JiraContactHelper;
import com.google.common.collect.ImmutableList;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

/**
 * @since 7.0
 */
@RunWith(MockitoJUnitRunner.class)
public class TestCreateIssueLicenseCheckImpl {
    static final String CONTACT_MESSAGE = "Contact your admin at blah blah";

    CreateIssueLicenseCheck licenseCheck;

    @Mock
    JiraLicenseManager licenseManager;
    @Mock
    ApplicationRoleManager roleManager;
    @Mock
    JiraAuthenticationContext authContext;
    @Mock
    JiraContactHelper contactHelper;
    @Mock
    I18nHelper i18n;
    @Mock
    LicenseDetails lic1, lic2;

    @Before
    public void setup() {
        licenseCheck = new CreateIssueLicenseCheckImpl(licenseManager, roleManager, authContext, contactHelper, i18n);
        when(authContext.getLoggedInUser()).thenReturn(mock(ApplicationUser.class));
        when(roleManager.hasAnyRole(any(ApplicationUser.class))).thenReturn(true);
        when(contactHelper.getAdministratorContactMessage(i18n)).thenReturn(CONTACT_MESSAGE);
        when(licenseManager.isLicenseSet()).thenReturn(true);
        when(licenseManager.getLicenses()).thenReturn(ImmutableList.of(lic1, lic2));
        when(lic1.isExpired()).thenReturn(false);
        when(lic2.isExpired()).thenReturn(false);
    }

    @Test
    public void checkPassesWhenAllConditionsMet() {
        when(roleManager.hasExceededAllRoles(any(ApplicationUser.class))).thenReturn(false);

        LicenseCheck.Result result = licenseCheck.evaluate();
        assertThat(result.isPass(), is(true));
        assertThat(result.getFailedLicenses(), Matchers.empty());
    }

    @Test
    public void checkFailsWhenNoLicenseSet() {
        when(licenseManager.isLicenseSet()).thenReturn(false);
        when(licenseManager.getLicenses()).thenReturn(Collections.emptyList());

        LicenseCheck.Result result = licenseCheck.evaluate();
        assertThat(result.isPass(), is(false));
        verify(licenseManager, never()).getLicenses();

        result.getFailureMessage();
        verify(i18n).getText("createissue.error.invalid.license", CONTACT_MESSAGE);
    }

    @Test
    public void checkFailsWhenAnyLicenseExpired() {
        when(lic2.isExpired()).thenReturn(true);

        LicenseCheck.Result result = licenseCheck.evaluate();
        assertThat(result.isPass(), is(false));
        assertThat(result.getFailedLicenses(), contains(lic2));

        result.getFailureMessage();
        verify(i18n).getText("createissue.error.license.expired", CONTACT_MESSAGE);
    }

    @Test
    public void checkAnonymousFailsWhenAnyRoleExceeded() {
        when(roleManager.isAnyRoleLimitExceeded()).thenReturn(true);
        when(authContext.getLoggedInUser()).thenReturn(null);

        LicenseCheck.Result result = licenseCheck.evaluate();
        assertThat(result.isPass(), is(false));
        assertThat(result.getFailedLicenses(), Matchers.empty());

        result.getFailureMessage();
        verify(i18n).getText("createissue.error.license.user.limit.exceeded.anonymous", CONTACT_MESSAGE);
        verify(roleManager).isAnyRoleLimitExceeded();
    }

    @Test
    public void checkAnonymousPassesWhenNoRoleIsExceeded() {
        when(roleManager.isAnyRoleLimitExceeded()).thenReturn(false);
        when(authContext.getLoggedInUser()).thenReturn(null);

        LicenseCheck.Result result = licenseCheck.evaluate();
        assertThat(result.isPass(), is(true));
    }

    @Test
    public void checkFailsWhenAllRolesExceeded() {
        when(roleManager.hasExceededAllRoles(any(ApplicationUser.class))).thenReturn(true);

        LicenseCheck.Result result = licenseCheck.evaluate();
        assertThat(result.isPass(), is(false));
        assertThat(result.getFailedLicenses(), Matchers.empty());

        result.getFailureMessage();
        verify(i18n).getText("createissue.error.license.user.limit.exceeded", CONTACT_MESSAGE);
        verify(roleManager).hasExceededAllRoles(any(ApplicationUser.class));
    }

    @Test
    public void checkSpecificUserPassesWhenAllConditionsMet() {
        when(authContext.getLoggedInUser()).thenThrow(new AssertionError("Shouldn't have been called"));

        when(licenseManager.isLicenseSet()).thenReturn(true);
        when(roleManager.hasExceededAllRoles(any(ApplicationUser.class))).thenReturn(false);

        LicenseCheck.Result result = licenseCheck.evaluateWithUser(new MockApplicationUser("Oliver"));
        assertThat(result.isPass(), is(true));
        assertThat(result.getFailedLicenses(), Matchers.empty());
    }

    @Test
    public void checkSpecificUserFailsWhenConditionsNotMet() {
        when(roleManager.hasExceededAllRoles(any(ApplicationUser.class))).thenReturn(true);

        LicenseCheck.Result result = licenseCheck.evaluateWithUser(new MockApplicationUser("Oliver"));
        assertThat(result.isPass(), is(false));
        assertThat(result.getFailedLicenses(), Matchers.empty());

        result.getFailureMessage();
        verify(i18n).getText("createissue.error.license.user.limit.exceeded", CONTACT_MESSAGE);
        verifyZeroInteractions(authContext);
    }

    @Test
    public void checkFailsWhenRenaissanceIsEnabledAndUserIsLoggedInAndHasNoApplicationRole() {
        when(roleManager.hasAnyRole(any(ApplicationUser.class))).thenReturn(false);

        LicenseCheck.Result result = licenseCheck.evaluate();

        assertThat(result.isPass(), is(false));
        result.getFailureMessage();
        verify(i18n).getText(eq("createissue.error.license.user.no.application.role"));
    }
}
