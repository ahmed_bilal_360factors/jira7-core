package com.atlassian.jira.issue.search.parameters.filter;

import com.atlassian.jira.mock.issue.MockIssue;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import org.junit.Test;
import org.mockito.Mockito;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;

/**
 * @since v4.0
 */
public class TestNoBrowsePermissionPredicate {
    @Test
    public void testEvaluate() throws Exception {
        final ApplicationUser theUser = new MockApplicationUser("fred");
        final PermissionManager permissionManager = mock(PermissionManager.class);
        final MockIssue issue = new MockIssue(123L);
        Mockito.when(permissionManager.hasPermission(Permissions.BROWSE, issue, theUser)).thenReturn(false);

        final NoBrowsePermissionPredicate predicate = new NoBrowsePermissionPredicate(theUser, permissionManager);

        assertTrue(predicate.evaluate(issue));
    }
}
