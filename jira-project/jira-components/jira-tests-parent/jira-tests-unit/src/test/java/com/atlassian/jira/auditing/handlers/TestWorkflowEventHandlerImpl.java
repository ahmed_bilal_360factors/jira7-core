package com.atlassian.jira.auditing.handlers;

import java.util.Locale;
import java.util.Objects;

import com.atlassian.fugue.Option;
import com.atlassian.jira.auditing.ChangedValue;
import com.atlassian.jira.auditing.RecordRequest;
import com.atlassian.jira.event.WorkflowUpdatedEvent;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.local.runner.ListeningMockitoRunner;
import com.atlassian.jira.mock.i18n.MockI18nHelper;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.workflow.MockJiraWorkflow;

import com.google.common.collect.ImmutableList;
import com.opensymphony.workflow.loader.ActionDescriptor;
import com.opensymphony.workflow.loader.DescriptorFactory;
import com.opensymphony.workflow.loader.ResultDescriptor;
import com.opensymphony.workflow.loader.StepDescriptor;

import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.junit.runner.RunWith;
import org.mockito.Mock;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.mockito.Mockito.when;

@RunWith (ListeningMockitoRunner.class)
public class TestWorkflowEventHandlerImpl {

    @Rule
    public final RuleChain mockContainer = MockitoMocksInContainer.forTest(this);

    @Mock
    @AvailableInContainer
    private I18nHelper.BeanFactory beanFactory;

    @AvailableInContainer
    MockI18nHelper i18nHelper = new MockI18nHelper();

    private WorkflowEventHandlerImpl workflowEventHandler;
    private WorkflowUpdatedEvent event;
    private MockJiraWorkflow originalWorkflow;
    private MockJiraWorkflow currentWorkflow;
    private DescriptorFactory descriptorFactory;

    @Before
    public void setUp() throws Exception {
        when(beanFactory.getInstance(Locale.ENGLISH)).thenReturn(i18nHelper);

        descriptorFactory = DescriptorFactory.getFactory();

        workflowEventHandler = new WorkflowEventHandlerImpl();

        originalWorkflow = new MockJiraWorkflow("name");
        originalWorkflow.setWorkflowDescriptor(descriptorFactory.createWorkflowDescriptor());

        currentWorkflow = new MockJiraWorkflow("name");
        currentWorkflow.setWorkflowDescriptor(DescriptorFactory.getFactory().createWorkflowDescriptor());

        event = new WorkflowUpdatedEvent(currentWorkflow, originalWorkflow);
    }

    @Test
    public void shouldLogStatusNameChange() {
        originalWorkflow.addStep(1, "Ready");
        currentWorkflow.addStep(1, "Not Ready");

        final Option<RecordRequest> request = workflowEventHandler.onWorkflowUpdatedEvent(event);

        assertThat(request.get().getChangedValues(), contains(changedValueMatcher("Ready", "Not Ready")));
    }

    @Test
    public void shouldLogNewStatus() {
        currentWorkflow.addStep(1, "Done");

        final Option<RecordRequest> request = workflowEventHandler.onWorkflowUpdatedEvent(event);

        assertThat(request.get().getChangedValues(), contains(changedValueMatcher(null, "Done")));
    }

    @Test
    public void shouldLogRemovedStatus() {
        originalWorkflow.addStep(1, "Open");

        final Option<RecordRequest> request = workflowEventHandler.onWorkflowUpdatedEvent(event);

        assertThat(request.get().getChangedValues(), contains(changedValueMatcher("Open", null)));
    }
    @Test
    public void shouldLogNewTransition() {
        final ActionDescriptor action = mockAction("Re-open");
        currentWorkflow.addAction(action);

        final Option<RecordRequest> request = workflowEventHandler.onWorkflowUpdatedEvent(event);

        assertThat(request.get().getChangedValues(), contains(changedValueMatcher(null, "Re-open")));
    }

    @Test
    public void shouldLogRemovedTransition() {
        final ActionDescriptor action = mockAction("Re-open");
        originalWorkflow.addAction(action);

        final Option<RecordRequest> request = workflowEventHandler.onWorkflowUpdatedEvent(event);

        assertThat(request.get().getChangedValues(), contains(changedValueMatcher("Re-open", null)));
    }

    @Test
    public void shouldLogRenamedTransition() {
        final ActionDescriptor action = mockAction("Re-open");

        originalWorkflow.addAction(action);

        final ActionDescriptor changedAction = mockAction("Open again");

        currentWorkflow.addAction(changedAction);

        final Option<RecordRequest> request = workflowEventHandler.onWorkflowUpdatedEvent(event);

        assertThat(request.get().getChangedValues(), contains(changedValueMatcher("Re-open", "Open again")));
    }

    @Test
    public void shouldLogChangedTransition() {
        final StepDescriptor originalOpen = originalWorkflow.addStep(1, "Open");
        final StepDescriptor originalDone = originalWorkflow.addStep(2, "Done");
        final StepDescriptor originalReady = originalWorkflow.addStep(3, "Ready");

        final ActionDescriptor action = mockAction("Re-open", originalDone);
        originalWorkflow.setStepsForTransition(action, ImmutableList.of(originalOpen, originalDone));
        originalWorkflow.addAction(action);

        final StepDescriptor currentOpen = currentWorkflow.addStep(1, "Open");
        final StepDescriptor currentDone = currentWorkflow.addStep(2, "Done");
        final StepDescriptor currentReady = currentWorkflow.addStep(3, "Ready");

        final ActionDescriptor changedAction = mockAction("Re-open", currentReady);
        currentWorkflow.setStepsForTransition(changedAction, ImmutableList.of(currentOpen, currentReady));
        currentWorkflow.addAction(changedAction);

        final Option<RecordRequest> request = workflowEventHandler.onWorkflowUpdatedEvent(event);

        assertThat(request.get().getChangedValues(), contains(changedValueMatcher("Re-open (Open -> Done)", "Re-open (Open -> Ready)")));
    }

    @Test
    public void shouldLogTransitionFromAnyStatus() {
        final StepDescriptor originalOpen = originalWorkflow.addStep(1, "Open");

        final StepDescriptor currentOpen = currentWorkflow.addStep(1, "Open");

        final ActionDescriptor changedAction = mockAction("Re-open", currentOpen);
        currentWorkflow.setGlobalAction(changedAction);
        currentWorkflow.setStepsForTransition(changedAction, ImmutableList.of(currentOpen));
        currentWorkflow.addAction(changedAction);

        final Option<RecordRequest> request = workflowEventHandler.onWorkflowUpdatedEvent(event);

        assertThat(request.get().getChangedValues(), contains(changedValueMatcher(null, "Re-open (Open)")));
    }

    @Test
    public void shouldLogCommonTransition() {
        final StepDescriptor originalOpen = originalWorkflow.addStep(1, "Open");
        final StepDescriptor originalMaybe = originalWorkflow.addStep(2, "Maybe");
        final StepDescriptor originalDone = originalWorkflow.addStep(3, "Done");

        final StepDescriptor currentOpen = currentWorkflow.addStep(1, "Open");
        final StepDescriptor currentMaybe = currentWorkflow.addStep(2, "Maybe");
        final StepDescriptor currentDone = currentWorkflow.addStep(3, "Done");

        final ActionDescriptor changedAction = mockAction("Finish", currentDone);
        currentWorkflow.getDescriptor().addCommonAction(changedAction);
        currentWorkflow.setStepsForTransition(changedAction, ImmutableList.of(currentOpen, currentMaybe));
        currentWorkflow.addAction(changedAction);

        final Option<RecordRequest> request = workflowEventHandler.onWorkflowUpdatedEvent(event);

        assertThat(request.get().getChangedValues(), contains(changedValueMatcher(null, "Finish ([Open, Maybe] -> Done)")));
    }

    private ActionDescriptor mockAction(String name, StepDescriptor ...resultSteps) {
        final ActionDescriptor action = descriptorFactory.createActionDescriptor();
        action.setName(name);
        final ResultDescriptor result = descriptorFactory.createResultDescriptor();
        for (StepDescriptor resultStep : resultSteps) {
            result.setStep(resultStep.getId());
        }
        action.setUnconditionalResult(result);
        return action;
    }

    private static Matcher<ChangedValue> changedValueMatcher(final String from, final String to) {
        return new BaseMatcher<ChangedValue>() {

            @Override
            public void describeTo(final Description description) {
                description.appendText("Should change from ").appendValue(from).appendText(" to ").appendValue(to);
            }

            @Override
            public void describeMismatch(final Object item, final Description description) {
                final ChangedValue changedValue = (ChangedValue) item;
                description.appendText("was from ").appendValue(changedValue.getFrom()).appendText(" to ").appendValue(changedValue.getTo());
            }

            @Override
            public boolean matches(final Object item) {
                final ChangedValue changedValue = (ChangedValue) item;
                return Objects.equals(from, changedValue.getFrom()) && Objects.equals(to, changedValue.getTo());
            }
        };
    }
}
