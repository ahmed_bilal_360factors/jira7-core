package com.atlassian.jira.security.groups;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import com.atlassian.crowd.embedded.api.CrowdService;
import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.crowd.embedded.api.Query;
import com.atlassian.crowd.embedded.api.UnfilteredCrowdService;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.crowd.manager.directory.DirectoryManager;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.mock.MockFeatureManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.ApplicationUsers;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.MockCrowdService;
import com.atlassian.jira.user.MockGroup;
import com.atlassian.jira.user.MockUser;
import com.atlassian.jira.user.util.UserIdentity;
import com.atlassian.jira.user.util.UserManager;

import com.google.common.collect.ImmutableSet;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.mockito.Mock;
import org.mockito.Mockito;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.collection.IsEmptyCollection.empty;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;

public class TestDefaultGroupManager {
    @Rule
    public final RuleChain mockitoMocks = MockitoMocksInContainer.forTest(this);

    @Rule
    public final MockitoContainer init = MockitoMocksInContainer.rule(this);

    @Mock
    @AvailableInContainer
    private UserManager userManager;

    @Test
    public void testGroupExists() {
        //given
        final MockGroup groupExists = new MockGroup("groupExists");
        final MockGroup groupDoesntExist = new MockGroup("groupDoesntExist");
        final MockCrowdService mockCrowdService = new MockCrowdService();
        mockCrowdService.addGroup(groupExists);
        DefaultGroupManager groupManager = new DefaultGroupManager(mockCrowdService, null, null, null, null, null);

        //then
        assertTrue(groupManager.groupExists(groupExists));
        assertFalse(groupManager.groupExists(groupDoesntExist));
    }

    @Test
    public void testIsUserInGroupByUsername() throws Exception {
        final MockCrowdService mockCrowdService = new MockCrowdService();
        DefaultGroupManager groupManager = new DefaultGroupManager(mockCrowdService, null, null, null, null, null);
        assertFalse(groupManager.isUserInGroup("fred", "dudes"));

        mockCrowdService.addUserToGroup(new MockApplicationUser("fred"), new MockGroup("dudes"));
        assertTrue(groupManager.isUserInGroup("fred", "dudes"));
    }

    @Test
    public void testIsUserInGroupByUserObject() throws Exception {
        final MockCrowdService mockCrowdService = new MockCrowdService();
        final DirectoryManager mockDirectoryManager = mock(DirectoryManager.class);
        DefaultGroupManager groupManager = new DefaultGroupManager(mockCrowdService, mockDirectoryManager, null, null, null, null);

        when(mockDirectoryManager.isUserNestedGroupMember(1, "fred", "dudes")).thenReturn(false);
        assertFalse(groupManager.isUserInGroup(new MockApplicationUser("fred"), new MockGroup("dudes")));
        assertFalse(groupManager.isUserInGroup(new MockApplicationUser("fred"), "dudes"));

        when(mockDirectoryManager.isUserNestedGroupMember(1, "fred", "dudes")).thenReturn(true);
        assertTrue(groupManager.isUserInGroup(new MockApplicationUser("fred"), new MockGroup("dudes")));
        assertTrue(groupManager.isUserInGroup(new MockApplicationUser("fred"), "dudes"));
    }

    @Test
    public void testIsUserInGroupHandlesNulls() throws Exception {
        // Need to handle null user and null group in order to maintain behaviour from OSUser.
        DefaultGroupManager groupManager = new DefaultGroupManager(null, null, null, null, null, null);
        assertFalse(groupManager.isUserInGroup(null, new MockGroup("dudes")));
        assertFalse(groupManager.isUserInGroup(new MockApplicationUser("fred"), (Group) null));
        assertFalse(groupManager.isUserInGroup(new MockApplicationUser("fred"), (String) null));
        assertFalse(groupManager.isUserInGroup((String) null, null));
        assertFalse(groupManager.isUserInGroup(null, (Group) null));
        assertFalse(groupManager.isUserInGroup((ApplicationUser) null, (String) null));
    }

    @Test
    public void testGetAllGroupsPreservesIterableOrder() throws Exception {
        final List<Group> orderedGroups = Arrays.<Group>asList(new MockGroup("alpha"), new MockGroup("beta"), new MockGroup("gamma"), new MockGroup("delta"));
        final CrowdService crowdService = spy(new MockCrowdService());
        for (Group group : orderedGroups) {
            crowdService.addGroup(group);
        }
        doReturn(orderedGroups).when(crowdService).search(any(Query.class));

        final DefaultGroupManager groupManager = new DefaultGroupManager(crowdService, null, null, null, null, null);
        final Collection<Group> actualGroups = groupManager.getAllGroups();

        assertEquals(orderedGroups, new ArrayList<>(actualGroups));
    }

    @Test
    public void testGetAllUsersInGroupWithFilter() throws Exception {
        final MockCrowdService mockCrowdService = new MockCrowdService();
        final DirectoryManager mockDirectoryManager = Mockito.mock(DirectoryManager.class);
        DefaultGroupManager groupManager = new DefaultGroupManager(mockCrowdService, mockDirectoryManager, null, null, null, null);
        MockGroup group = new MockGroup("test-group");
        mockCrowdService.addGroup(group);

        MockApplicationUser inactiveUser = new MockApplicationUser("inactive-user");
        inactiveUser.setActive(false);
        mockCrowdService.addUserToGroup(inactiveUser, group);
        MockApplicationUser activeUser = new MockApplicationUser("active-user");
        mockCrowdService.addUserToGroup(activeUser, group);

        when(userManager.getUserIdentityByUsername(activeUser.getUsername())).thenReturn(Optional.of(UserIdentity.withId(activeUser.getId()).key(activeUser.getKey()).andUsername(activeUser.getUsername())));
        when(userManager.getUserIdentityByUsername(inactiveUser.getUsername())).thenReturn(Optional.of(UserIdentity.withId(inactiveUser.getId()).key(inactiveUser.getKey()).andUsername(inactiveUser.getUsername())));

        assertThat(groupManager.getUsersInGroup(group.getName(), false), is(Collections.singletonList(activeUser)));
    }

    @Test
    public void testThatNoConnectUsersAreReturnedInServer() {
        final CrowdService crowdService = spy(new MockCrowdService());
        final UnfilteredCrowdService unfilteredCrowdService = mock(UnfilteredCrowdService.class);
        final MockFeatureManager featureManager = new MockFeatureManager();

        final DefaultGroupManager groupManager = new DefaultGroupManager(crowdService, null, null, null, unfilteredCrowdService, featureManager);

        final MockUser mockUser = new MockUser("jo", -1);
        final ImmutableSet<User> connectUserFound = ImmutableSet.of(mockUser);

        when(userManager.getUserIdentityByUsername(mockUser.getName())).thenReturn(Optional.empty());
        doReturn(connectUserFound).when(unfilteredCrowdService).search(any(Query.class));

        // Connect users should not be returned
        assertThat(groupManager.getConnectUsers(), is(empty()));

        verifyZeroInteractions(crowdService);
        verifyZeroInteractions(unfilteredCrowdService);
    }
}
