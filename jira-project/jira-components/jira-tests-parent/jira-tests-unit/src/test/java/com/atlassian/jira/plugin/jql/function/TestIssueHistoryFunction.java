package com.atlassian.jira.plugin.jql.function;

import com.atlassian.jira.JiraDataTypes;
import com.atlassian.jira.jql.operand.QueryLiteral;
import com.atlassian.jira.jql.query.QueryCreationContext;
import com.atlassian.jira.jql.query.QueryCreationContextImpl;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.UserHistoryItem;
import com.atlassian.jira.user.UserIssueHistoryManager;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.MessageSet;
import com.atlassian.jira.util.NoopI18nHelper;
import com.atlassian.jira.util.collect.CollectionBuilder;
import com.atlassian.query.clause.TerminalClause;
import com.atlassian.query.operand.FunctionOperand;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Collections;
import java.util.List;

import static com.atlassian.jira.jql.operand.SimpleLiteralFactory.createLiteral;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

/**
 * Test for {@link com.atlassian.jira.plugin.jql.function.IssueHistoryFunction}.
 *
 * @since v4.0
 */
public class TestIssueHistoryFunction {
    @Rule
    public MockitoRule mockito = MockitoJUnit.rule();

    @Mock
    private UserIssueHistoryManager userHistoryManager;

    private TerminalClause terminalClause = null;
    private ApplicationUser theUser = null;
    private QueryCreationContext queryCreationContext;

    @Before
    public void setUp() throws Exception {
        queryCreationContext = new QueryCreationContextImpl(theUser);
    }

    @Test
    public void testDataType() throws Exception {
        final IssueHistoryFunction handler = new MyIssueHistoryFunction(userHistoryManager);
        assertEquals(JiraDataTypes.ISSUE, handler.getDataType());
    }

    @Test
    public void testValidateArgsHappy() throws Exception {
        final IssueHistoryFunction handler = new MyIssueHistoryFunction(userHistoryManager);
        final MessageSet messageSet = handler.validate(null, new FunctionOperand(IssueHistoryFunction.FUNCTION_ISSUE_HISTORY), terminalClause);

        assertNotNull(messageSet);
        assertFalse(messageSet.hasAnyErrors());
        assertFalse(messageSet.hasAnyWarnings());
    }

    @Test
    public void testValidateArgsBad() throws Exception {
        final IssueHistoryFunction handler = new MyIssueHistoryFunction(userHistoryManager);
        final MessageSet messageSet = handler.validate(null, new FunctionOperand(IssueHistoryFunction.FUNCTION_ISSUE_HISTORY, "1"), terminalClause);

        assertNotNull(messageSet);
        assertTrue(messageSet.hasAnyErrors());
        assertEquals(Collections.<String>singleton("jira.jql.function.arg.incorrect.exact{[issueHistory, 0, 1]}"), messageSet.getErrorMessages());
        assertFalse(messageSet.hasAnyWarnings());
    }

    @Test
    public void testGetValuesHappy() throws Exception {

        when(userHistoryManager.getFullIssueHistoryWithPermissionChecks(theUser)).thenReturn(Collections.singletonList(new UserHistoryItem(UserHistoryItem.ISSUE, "1")));

        final IssueHistoryFunction handler = new MyIssueHistoryFunction(userHistoryManager);
        final FunctionOperand operand = new FunctionOperand(IssueHistoryFunction.FUNCTION_ISSUE_HISTORY, "1");
        final List<QueryLiteral> expectedValues = handler.getValues(queryCreationContext, operand, terminalClause);

        assertEquals(Collections.singletonList(createLiteral(1L)), expectedValues);
        assertEquals(operand, expectedValues.get(0).getSourceOperand());
    }

    @Test
    public void testGetValuesHappyOverrideSecurity() throws Exception {
        queryCreationContext = new QueryCreationContextImpl(theUser, true);
        when(userHistoryManager.getFullIssueHistoryWithoutPermissionChecks(theUser)).thenReturn(Collections.singletonList(new UserHistoryItem(UserHistoryItem.ISSUE, "1")));

        final IssueHistoryFunction handler = new MyIssueHistoryFunction(userHistoryManager);
        final FunctionOperand operand = new FunctionOperand(IssueHistoryFunction.FUNCTION_ISSUE_HISTORY, "1");
        final List<QueryLiteral> expectedValues = handler.getValues(queryCreationContext, operand, terminalClause);

        assertEquals(Collections.singletonList(createLiteral(1L)), expectedValues);
        assertEquals(operand, expectedValues.get(0).getSourceOperand());

    }

    @Test
    public void testGetValuesNoItems() throws Exception {
        when(userHistoryManager.getFullIssueHistoryWithPermissionChecks(theUser)).thenReturn(Collections.<UserHistoryItem>emptyList());

        final IssueHistoryFunction handler = new MyIssueHistoryFunction(userHistoryManager);
        final List<QueryLiteral> expectedValues = handler.getValues(queryCreationContext, new FunctionOperand(IssueHistoryFunction.FUNCTION_ISSUE_HISTORY), terminalClause);

        assertEquals(Collections.<QueryLiteral>emptyList(), expectedValues);
    }

    @Test
    public void testGetValuesMultipleItems() throws Exception {
        final List<UserHistoryItem> list = CollectionBuilder.<UserHistoryItem>newBuilder(new UserHistoryItem(UserHistoryItem.ISSUE, "1"),
                new UserHistoryItem(UserHistoryItem.ISSUE, "2"), new UserHistoryItem(UserHistoryItem.ISSUE, ""),
                new UserHistoryItem(UserHistoryItem.ISSUE, "rkjwek")).asList();

        when(userHistoryManager.getFullIssueHistoryWithPermissionChecks(theUser)).thenReturn(list);

        final IssueHistoryFunction handler = new MyIssueHistoryFunction(userHistoryManager);
        final FunctionOperand operand = new FunctionOperand(IssueHistoryFunction.FUNCTION_ISSUE_HISTORY);
        final List<QueryLiteral> expectedValues = handler.getValues(queryCreationContext, operand, terminalClause);

        assertEquals(CollectionBuilder.newBuilder(createLiteral(1L), createLiteral(2L)).asList(), expectedValues);
        assertEquals(operand, expectedValues.get(0).getSourceOperand());
        assertEquals(operand, expectedValues.get(1).getSourceOperand());

    }

    @Test
    public void testGetMinimumNumberOfExpectedArguments() throws Exception {

        final IssueHistoryFunction handler = new MyIssueHistoryFunction(userHistoryManager);
        assertEquals(0, handler.getMinimumNumberOfExpectedArguments());
    }

    private static class MyIssueHistoryFunction extends IssueHistoryFunction {
        public MyIssueHistoryFunction(final UserIssueHistoryManager userHistoryManager) {
            super(userHistoryManager);
        }

        @Override
        protected I18nHelper getI18n() {
            return new NoopI18nHelper();
        }
    }
}
