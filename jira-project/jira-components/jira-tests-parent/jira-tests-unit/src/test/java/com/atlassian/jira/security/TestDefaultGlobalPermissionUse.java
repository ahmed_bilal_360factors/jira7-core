package com.atlassian.jira.security;

import com.atlassian.cache.CacheManager;
import com.atlassian.cache.memory.MemoryCacheManager;
import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.fugue.Option;
import com.atlassian.jira.application.ApplicationRoleManager;
import com.atlassian.jira.event.MockEventPublisher;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.license.LicenseCountService;
import com.atlassian.jira.mock.MockFeatureManager;
import com.atlassian.jira.mock.ofbiz.MockOfBizDelegator;
import com.atlassian.jira.ofbiz.FieldMap;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.permission.GlobalPermissionType;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.security.groups.MockGroupManager;
import com.atlassian.jira.security.plugin.GlobalPermissionTypesManager;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.MockCrowdService;
import com.atlassian.jira.user.MockGroup;
import com.atlassian.jira.user.util.StaticRecoveryMode;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.event.PluginEventManager;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.RuleChain;
import org.mockito.Mock;
import org.ofbiz.core.entity.GenericValue;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

/**
 * Rules for USE global permission when roles enabled (JIRA renaissance mode)
 * When used for configuration - unsupported.
 * When used for type conversion - supported.
 * When used for user has permission - supported.
 * When used for anonymous user permission - not supported as per 6.x.
 */
public class TestDefaultGlobalPermissionUse {
    private static final String RECOVERY_USER = "recovery_user";

    @Rule
    public final RuleChain mockitoMocks = MockitoMocksInContainer.forTest(this);

    @Rule
    public final ExpectedException exception = ExpectedException.none();
    protected MockCrowdService crowdService;
    protected MockEventPublisher eventPublisher;
    protected MockOfBizDelegator ofBizDelegator;
    protected DefaultGlobalPermissionManager globalPermissionManager;
    protected GroupManager groupManager;
    @Mock
    @AvailableInContainer
    private LicenseCountService licenseCountService;
    @Mock
    private GlobalPermissionTypesManager globalPermissionTypesManager;
    @Mock
    private CacheManager cacheManager;
    private MockFeatureManager featureManager = new MockFeatureManager();
    @Mock
    private PluginAccessor pluginAccessor;
    @Mock
    private PluginEventManager pluginEventManager;
    @Mock
    private ApplicationRoleManager applicationRoleManager;

    final GlobalPermissionType GPT_USE = new GlobalPermissionType(GlobalPermissionKey.USE.getKey(), null, null, false);
    final GlobalPermissionType GPT_ADMIN = new GlobalPermissionType(GlobalPermissionKey.ADMINISTER.getKey(), null, null, false);
    final GlobalPermissionType GPT_SYSADMIN = new GlobalPermissionType(GlobalPermissionKey.SYSTEM_ADMIN.getKey(), null, null, false);
    final GlobalPermissionType GPT_BULK_CHANGE = new GlobalPermissionType(GlobalPermissionKey.BULK_CHANGE.getKey(), null, null, true);

    final Group grpUSE = new MockGroup("grpUSE");
    final Group grpADMINISTER = new MockGroup("grpADMINISTER");
    final Group grpSYSTEM_ADMIN = new MockGroup("grpSYSTEM_ADMIN");
    final Group grpBULK_CHANGE = new MockGroup("grpBULK_CHANGE");

    @Before
    public void setUp() {
        int CAPTURE_SEAT_COUNT = 1;
        applicationRoleManager.getRoles().stream().anyMatch(role -> role.getNumberOfSeats() == CAPTURE_SEAT_COUNT || role.getNumberOfSeats() == -1);
        crowdService = new MockCrowdService();
        eventPublisher = new MockEventPublisher();
        ofBizDelegator = new MockOfBizDelegator();
        groupManager = new MockGroupManager();
        mockGroups();
        mockGlobalPermissionTypesManager();
        mockGlobalPermissionEntry();

        globalPermissionManager = new DefaultGlobalPermissionManager(crowdService, ofBizDelegator,
                eventPublisher, globalPermissionTypesManager, new MemoryCacheManager(),
                applicationRoleManager, groupManager, new StaticRecoveryMode(RECOVERY_USER), featureManager);

    }

    /**
     * These are the groups that exist in the user directory.
     */
    private void mockGroups() {
        crowdService.addGroup(grpUSE);
        crowdService.addGroup(grpADMINISTER);
        crowdService.addGroup(grpSYSTEM_ADMIN);
        crowdService.addGroup(grpBULK_CHANGE);
    }

    /**
     * This is the global permissions in the database.
     */
    private void mockGlobalPermissionEntry() {
        final List<GenericValue> entries = Lists.newArrayList();
        entries.add(ofBizDelegator.createValue("GlobalPermissionEntry", FieldMap.build("group_id", grpUSE.getName(), "permission", "USE")));
        entries.add(ofBizDelegator.createValue("GlobalPermissionEntry", FieldMap.build("group_id", grpADMINISTER.getName(), "permission", "ADMINISTER")));
        entries.add(ofBizDelegator.createValue("GlobalPermissionEntry", FieldMap.build("group_id", grpSYSTEM_ADMIN.getName(), "permission", "SYSTEM_ADMIN")));
        entries.add(ofBizDelegator.createValue("GlobalPermissionEntry", FieldMap.build("group_id", grpBULK_CHANGE.getName(), "permission", "BULK_CHANGE")));
        ofBizDelegator.addRelatedMap("GlobalPermissionEntry", null, entries);
    }

    /**
     * This gets registered (configured) by plugins in GlobalPermissionModuleDescriptor.
     * GlobalPermissionTypesManagerImpl.GlobalPermissionsResettableLazyReference.shouldAddPermission() Excludes USE when
     * applicationRoleManager.rolesEnabled()
     */
    private void mockGlobalPermissionTypesManager() {
        when(globalPermissionTypesManager.getGlobalPermission(GlobalPermissionKey.USE)).thenReturn(Option.option(GPT_USE));
        when(globalPermissionTypesManager.getGlobalPermission(GlobalPermissionKey.ADMINISTER)).thenReturn(Option.option(GPT_ADMIN));
        when(globalPermissionTypesManager.getGlobalPermission(GlobalPermissionKey.SYSTEM_ADMIN)).thenReturn(Option.option(GPT_SYSADMIN));
        when(globalPermissionTypesManager.getGlobalPermission(GlobalPermissionKey.BULK_CHANGE)).thenReturn(Option.option(GPT_BULK_CHANGE));

        when(globalPermissionTypesManager.getGlobalPermission("USE")).thenReturn(Option.option(GPT_USE));
        when(globalPermissionTypesManager.getGlobalPermission("ADMINISTER")).thenReturn(Option.option(GPT_ADMIN));
        when(globalPermissionTypesManager.getGlobalPermission("SYSTEM_ADMIN")).thenReturn(Option.option(GPT_SYSADMIN));
        when(globalPermissionTypesManager.getGlobalPermission("BULK_CHANGE")).thenReturn(Option.option(GPT_BULK_CHANGE));

        when(globalPermissionTypesManager.getAll()).thenReturn(ImmutableSet.of(GPT_ADMIN, GPT_SYSADMIN, GPT_BULK_CHANGE));
    }

    @Test
    public void testThatGetAllGlobalPermissionsExcludesUsePermission() {
        assertThat(globalPermissionManager.getAllGlobalPermissions().size(), is(3));
        assertThat(globalPermissionManager.getAllGlobalPermissions(), hasItems(GPT_ADMIN, GPT_BULK_CHANGE, GPT_SYSADMIN));
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testUnsupportedForUse_getPermissionsWithInt() {
        globalPermissionManager.getPermissions(1);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testUnsupportedForUse_getPermissionsWithGlobalPermissionType() {
        globalPermissionManager.getPermissions(GPT_USE);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testUnsupportedForUse_getPermissionsWithGlobalPermissionKey() {
        globalPermissionManager.getPermissions(GlobalPermissionKey.USE);
    }

    @Test
    public void testThatOtherPermissionsAreStillSupported() {
        assertThat(globalPermissionManager.getPermissions(0).size(), is(1));
        assertThat(globalPermissionManager.getPermissions(GPT_ADMIN).size(), is(1));
        assertThat(globalPermissionManager.getPermissions(GlobalPermissionKey.ADMINISTER).size(), is(1));
    }

    @Test
    public void testThatUseToGlobalPermissionTypeConversionReturnsUseType() {
        assertThat(globalPermissionManager.getGlobalPermission(GlobalPermissionKey.USE), is(Option.some(GPT_USE)));
        assertThat(globalPermissionManager.getGlobalPermission(1), is(Option.some(GPT_USE)));
        assertThat(globalPermissionManager.getGlobalPermission("USE"), is(Option.some(GPT_USE)));
    }

    @Test
    public void testThatUserHasUsePermissionIfUserHasALicensedRole() throws Exception {
        //Given
        final MockApplicationUser jo = new MockApplicationUser("jo");
        final MockApplicationUser adam = new MockApplicationUser("adam");
        final MockApplicationUser max = new MockApplicationUser("max");
        crowdService.addUser(jo);
        crowdService.addUser(adam);
        crowdService.addUser(max);

        when(applicationRoleManager.hasAnyRole(jo)).thenReturn(true);
        when(applicationRoleManager.hasAnyRole(adam)).thenReturn(false);

        groupManager.addUserToGroup(adam, grpADMINISTER);
        groupManager.addUserToGroup(max, grpSYSTEM_ADMIN);
        groupManager.addUserToGroup(max, grpUSE);

        assertTrue(globalPermissionManager.hasPermission(GPT_USE, jo));
        assertTrue(globalPermissionManager.hasPermission(GlobalPermissionKey.USE, jo));
        assertTrue(globalPermissionManager.hasPermission(1, jo));
        assertFalse(globalPermissionManager.hasPermission(GPT_ADMIN, jo));
        assertFalse(globalPermissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, jo));
        assertFalse(globalPermissionManager.hasPermission(0, jo));

        assertFalse(globalPermissionManager.hasPermission(GPT_USE, adam));
        assertFalse(globalPermissionManager.hasPermission(GlobalPermissionKey.USE, adam));
        assertFalse(globalPermissionManager.hasPermission(1, adam));
        assertTrue(globalPermissionManager.hasPermission(GPT_ADMIN, adam));
        assertTrue(globalPermissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, adam));
        assertTrue(globalPermissionManager.hasPermission(0, adam));

        assertFalse(globalPermissionManager.hasPermission(GPT_USE, max));
        assertFalse(globalPermissionManager.hasPermission(GlobalPermissionKey.USE, max));
        assertFalse(globalPermissionManager.hasPermission(1, max));
        assertTrue(globalPermissionManager.hasPermission(GPT_ADMIN, max));
        assertTrue(globalPermissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, max));
        assertTrue(globalPermissionManager.hasPermission(0, max));
        assertTrue(globalPermissionManager.hasPermission(GPT_SYSADMIN, max));
        assertTrue(globalPermissionManager.hasPermission(GlobalPermissionKey.SYSTEM_ADMIN, max));
        assertTrue(globalPermissionManager.hasPermission(44, max));
    }

    @Test
    public void testThatAnonymousUsersDoNotHaveUsePermission() {
        assertFalse(globalPermissionManager.hasPermission(1));
        assertFalse(globalPermissionManager.hasPermission(GPT_USE));
    }

    @Test
    public void testThatUseIsStillDistinguishableAsGlobalPermissionVsProjectPermission() {
        assertTrue(globalPermissionManager.isGlobalPermission(1));
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testThatRetrievingGroupsForUseByIntIsUnsupported() {
        globalPermissionManager.getGroupNames(1);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testThatRetrievingGroupsForUseByTypeIsUnsupported() {
        globalPermissionManager.getGroupNames(GPT_USE);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testThatRetrievingGroupsWithUsePermissionByIntIsUnsupported() {
        globalPermissionManager.getGroupsWithPermission(1);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testThatRetrievingGroupsWithUsePermissionByTypeIsUnsupported() {
        globalPermissionManager.getGroupsWithPermission(GPT_USE);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testThatRetrievingGroupsWithUsePermissionByKeyIsUnsupported() {
        globalPermissionManager.getGroupsWithPermission(GlobalPermissionKey.USE);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testThatRetrievingGroupsNamesWithUsePermissionByKeyIsUnsupported() {
        globalPermissionManager.getGroupNamesWithPermission(GlobalPermissionKey.USE);
    }
}