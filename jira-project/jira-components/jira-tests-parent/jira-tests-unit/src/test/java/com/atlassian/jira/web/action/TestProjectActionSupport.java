package com.atlassian.jira.web.action;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.mock.ofbiz.MockOfBizDelegator;
import com.atlassian.jira.ofbiz.FieldMap;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.permission.PermissionSchemeManager;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectFactory;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.google.common.collect.Lists;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.ofbiz.core.entity.GenericValue;

import java.util.Collections;

import static com.atlassian.jira.permission.ProjectPermissions.BROWSE_PROJECTS;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestProjectActionSupport {
    @Rule
    public RuleChain mocksInContainer = MockitoMocksInContainer.forTest(this);
    private OfBizDelegator mockOfBizDelegator = new MockOfBizDelegator();
    @Mock
    @AvailableInContainer
    private JiraAuthenticationContext mockJiraAuthenticationContext;
    @Mock
    @AvailableInContainer
    private ProjectManager mockJiraProjectManager;
    @Mock
    @AvailableInContainer
    private PermissionManager mockPermissionManager;
    @Mock
    @AvailableInContainer
    private PermissionSchemeManager mockPermissionSchemeManager;
    @Mock
    @AvailableInContainer
    private ProjectFactory mockProjectFactory;

    @Test
    public void testGetProjectManager() {
        ProjectActionSupport pas = new ProjectActionSupport();
        assertTrue(pas.getProjectManager() instanceof ProjectManager);
    }

    @Test
    public void testBrowsableProjects() throws Exception {
        GenericValue scheme = mockOfBizDelegator.createValue("PermissionScheme", FieldMap.build("id", new Long(10), "name", "Test Scheme"));
        GenericValue genericValue1 = mockOfBizDelegator.createValue("Project", FieldMap.build("id", new Long(4)));
        Project project1 = ComponentAccessor.getProjectFactory().getProject(genericValue1);
        ComponentAccessor.getPermissionSchemeManager().addSchemeToProject(genericValue1, scheme);

        GenericValue genericValue2 = mockOfBizDelegator.createValue("Project", FieldMap.build("id", new Long(5)));
        Project project2 = ComponentAccessor.getProjectFactory().getProject(genericValue2);

        ProjectActionSupport pas = new ProjectActionSupport();

        assertEquals(0, pas.getBrowsableProjects().size());

        pas = new ProjectActionSupport();
        when(mockJiraProjectManager.getProjectObjects()).thenReturn(Lists.newArrayList(project1, project2));
        when(mockPermissionManager.getProjects(BROWSE_PROJECTS, null)).thenReturn(Collections.singletonList(project1));
        assertEquals(1, pas.getBrowsableProjects().size());
        assertTrue(pas.getBrowsableProjects().contains(project1));

        pas = new ProjectActionSupport();

        when(mockPermissionManager.getProjects(BROWSE_PROJECTS, null)).thenReturn(Lists.newArrayList(project1, project2));
        assertEquals(2, pas.getBrowsableProjects().size());
        assertTrue(pas.getBrowsableProjects().contains(project1));
        assertTrue(pas.getBrowsableProjects().contains(project2));
    }
}
