package com.atlassian.jira.web.action.admin;

import com.atlassian.crowd.embedded.api.CrowdService;
import com.atlassian.crowd.exception.FailedAuthenticationException;
import com.atlassian.jira.bc.license.JiraLicenseService;
import com.atlassian.jira.bc.license.JiraLicenseUpdaterService;
import com.atlassian.jira.cluster.ClusterManager;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.config.properties.JiraProperties;
import com.atlassian.jira.config.properties.MockJiraProperties;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.license.JiraLicenseManager;
import com.atlassian.jira.license.LicenseDetails;
import com.atlassian.jira.license.LicenseMaintenancePredicate;
import com.atlassian.jira.matchers.ErrorCollectionMatchers;
import com.atlassian.jira.mock.MockApplicationProperties;
import com.atlassian.jira.mock.security.MockSimpleAuthenticationContext;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.DelegatingApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.UserKeyService;
import com.atlassian.jira.user.util.MockUserManager;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.BuildUtilsInfo;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.MockBuildUtilsInfo;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.jira.util.johnson.JohnsonProvider;
import com.atlassian.jira.util.system.JiraSystemRestarter;
import com.atlassian.johnson.DefaultJohnsonEventContainer;
import com.atlassian.johnson.Johnson;
import com.atlassian.johnson.JohnsonEventContainer;
import com.atlassian.johnson.event.Event;
import com.atlassian.johnson.event.EventType;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.mockobjects.servlet.MockServletContext;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.mockito.Mock;
import webwork.action.ServletActionContext;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.Map;

import static com.atlassian.jira.config.properties.APKeys.JIRA_PATCHED_VERSION;
import static com.atlassian.jira.config.properties.SystemPropertyKeys.UPGRADE_SYSTEM_PROPERTY;
import static com.atlassian.jira.license.LicenseJohnsonEventRaiser.LICENSE_TOO_OLD;
import static com.atlassian.jira.matchers.ErrorCollectionMatchers.containsFieldError;
import static com.atlassian.jira.matchers.ErrorCollectionMatchers.containsSystemError;
import static com.atlassian.jira.user.MockApplicationUser.FAKE_USER_DIRECTORY_ID;
import static com.atlassian.jira.util.NoopI18nHelper.makeTranslation;
import static com.atlassian.jira.web.action.admin.ConfirmNewInstallationWithOldLicense.MAINTENANCE_EXPIRY_PRIMARY_KEY;
import static com.atlassian.jira.web.action.admin.ConfirmNewInstallationWithOldLicense.MAINTENANCE_EXPIRY_SECONDARY_KEY;
import static com.atlassian.jira.web.action.admin.ConfirmNewInstallationWithOldLicense.RADIO_OPTION_EVALUATION;
import static com.atlassian.jira.web.action.admin.ConfirmNewInstallationWithOldLicense.SUBSCRIPTION_EXPIRY_PRIMARY_KEY;
import static com.atlassian.jira.web.action.admin.ConfirmNewInstallationWithOldLicense.SUBSCRIPTION_EXPIRY_SECONDARY_KEY;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.hasEntry;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.argThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

/**
 * Unit test of {@link ConfirmNewInstallationWithOldLicense}.
 *
 * @since 6.2
 */
public class TestConfirmNewInstallationWithOldLicense {
    @Rule
    public RuleChain container = MockitoMocksInContainer.forTest(this);

    private static final String PASSWORD = "testpassword";
    private static final String A_VALID_LICENSE = "a valid license";

    @Mock
    @AvailableInContainer
    private ApplicationProperties mockApplicationProperties = new MockApplicationProperties();

    private Date buildDate = new Date();

    private BuildUtilsInfo buildUtilsInfo = new MockBuildUtilsInfo()
            .setCurrentBuildNumber("610")
            .setBuildDate(buildDate);

    @Mock
    private CrowdService crowdService;

    @Mock
    private JiraLicenseUpdaterService jiraLicenseService;

    @Mock
    private JiraSystemRestarter jiraSystemRestarter;

    @Mock
    private GlobalPermissionManager mockPermissionManager;

    @Mock
    private ClusterManager clusterManager;

    @AvailableInContainer
    private UserManager userManager = new MockUserManager().alwaysReturnUsers();

    @Mock
    private JohnsonProvider johnsonProvider;

    @AvailableInContainer
    private JiraAuthenticationContext jiraAuthenticationContext =
            MockSimpleAuthenticationContext.createNoopContext(new MockApplicationUser("admin"));

    @AvailableInContainer
    @Mock
    private UserKeyService keyService;

    @Mock
    private LicenseMaintenancePredicate maintenancePredicate;

    @Mock
    private JiraLicenseManager jiraLicenseManager;

    private JohnsonEventContainer johnsonEventContainer;
    private ConfirmNewInstallationWithOldLicense confirmNewInstallationWithOldLicense;

    @Before
    public void setUp() {
        Johnson.initialize("test-johnson-config.xml");

        when(mockApplicationProperties.getString(JIRA_PATCHED_VERSION)).thenReturn("610");

        final JiraProperties jiraProperties = new MockJiraProperties();
        confirmNewInstallationWithOldLicense = spy(new ConfirmNewInstallationWithOldLicense(jiraLicenseService,
                buildUtilsInfo, jiraSystemRestarter, crowdService, mockPermissionManager,
                jiraProperties, clusterManager, userManager, johnsonProvider, maintenancePredicate,
                jiraLicenseManager));

        johnsonEventContainer = new DefaultJohnsonEventContainer();
        when(johnsonProvider.getContainer()).thenReturn(johnsonEventContainer);

        when(maintenancePredicate.test(any())).thenReturn(true);
        when(maintenancePredicate.negate()).thenReturn(t -> !maintenancePredicate.test(t));
    }

    @After
    public void tearDown() throws Exception {
        System.clearProperty(UPGRADE_SYSTEM_PROPERTY);
        Johnson.terminate();
    }

    @Test
    public void expiredShouldBeTrueIfAnyLicenseIsExpired() {
        LicenseDetails l1 = mock(LicenseDetails.class);
        LicenseDetails l2 = mock(LicenseDetails.class);
        when(l1.isExpired()).thenReturn(false);
        when(l2.isExpired()).thenReturn(true);
        when(jiraLicenseManager.getLicenses()).thenReturn(Lists.newArrayList(l1, l2));
        doReturn("42").when(confirmNewInstallationWithOldLicense).getServerId();
        doReturn(mock(HttpServletRequest.class)).when(confirmNewInstallationWithOldLicense).getHttpRequest();

        Map<String, Object> dataMap = confirmNewInstallationWithOldLicense.getDataMap();
        assertThat(dataMap, hasEntry("expired", true));
    }

    @Test
    public void evaluationOptionShouldNotDisplayInCluster() {
        LicenseDetails l1 = mock(LicenseDetails.class);
        when(l1.isExpired()).thenReturn(true);
        when(jiraLicenseManager.getLicenses()).thenReturn(Lists.newArrayList(l1));
        doReturn("42").when(confirmNewInstallationWithOldLicense).getServerId();
        doReturn(mock(HttpServletRequest.class)).when(confirmNewInstallationWithOldLicense).getHttpRequest();
        when(clusterManager.isClustered()).thenReturn(true);

        Map<String, Object> dataMap = confirmNewInstallationWithOldLicense.getDataMap();
        assertThat(dataMap, hasEntry("evaluationOptionDisplayable", false));
    }

    @Test
    public void evaluationOptionShouldNotDisplayIfDataCenter() {
        LicenseDetails l1 = mock(LicenseDetails.class);
        when(l1.isExpired()).thenReturn(true);
        when(l1.isDataCenter()).thenReturn(true);
        when(jiraLicenseManager.getLicenses()).thenReturn(Lists.newArrayList(l1));
        doReturn("42").when(confirmNewInstallationWithOldLicense).getServerId();
        doReturn(mock(HttpServletRequest.class)).when(confirmNewInstallationWithOldLicense).getHttpRequest();
        when(clusterManager.isClustered()).thenReturn(false);
        when(maintenancePredicate.test(l1)).thenReturn(false);

        Map<String, Object> dataMap = confirmNewInstallationWithOldLicense.getDataMap();
        assertThat(dataMap, hasEntry("evaluationOptionDisplayable", false));
    }

    @Test
    public void evaluationOptionShouldNotDisplayIfEnterprise() {
        LicenseDetails l1 = mock(LicenseDetails.class);
        LicenseDetails l2 = mock(LicenseDetails.class);
        when(maintenancePredicate.test(l1)).thenReturn(false);
        when(maintenancePredicate.test(l2)).thenReturn(false);
        when(l1.isEnterpriseLicenseAgreement()).thenReturn(false);
        when(l2.isEnterpriseLicenseAgreement()).thenReturn(true);
        when(jiraLicenseManager.getLicenses()).thenReturn(Lists.newArrayList(l1, l2));
        doReturn("42").when(confirmNewInstallationWithOldLicense).getServerId();
        doReturn(mock(HttpServletRequest.class)).when(confirmNewInstallationWithOldLicense).getHttpRequest();
        when(clusterManager.isClustered()).thenReturn(false);

        Map<String, Object> dataMap = confirmNewInstallationWithOldLicense.getDataMap();
        assertThat(dataMap, hasEntry("evaluationOptionDisplayable", false));
    }

    @Test
    public void deleteLicensesShouldNotAppearIfThereAreNotValidLicenses() {
        LicenseDetails l1 = mock(LicenseDetails.class);
        LicenseDetails l2 = mock(LicenseDetails.class);
        when(maintenancePredicate.test(l1)).thenReturn(false);
        when(maintenancePredicate.test(l2)).thenReturn(false);
        when(jiraLicenseManager.getLicenses()).thenReturn(Lists.newArrayList(l1, l2));
        doReturn("42").when(confirmNewInstallationWithOldLicense).getServerId();
        doReturn(mock(HttpServletRequest.class)).when(confirmNewInstallationWithOldLicense).getHttpRequest();

        Map<String, Object> dataMap = confirmNewInstallationWithOldLicense.getDataMap();
        assertThat(dataMap, hasEntry("thereAreValidLicenses", false));
    }

    @Test
    public void deleteLicensesShouldAppearIfThereArValidLicenses() {
        LicenseDetails l1 = mock(LicenseDetails.class);
        LicenseDetails l2 = mock(LicenseDetails.class);
        when(l1.isMaintenanceValidForBuildDate(buildDate)).thenReturn(true);
        when(l2.isMaintenanceValidForBuildDate(buildDate)).thenReturn(false);
        when(jiraLicenseManager.getLicenses()).thenReturn(Lists.newArrayList(l1, l2));
        doReturn("42").when(confirmNewInstallationWithOldLicense).getServerId();
        doReturn(mock(HttpServletRequest.class)).when(confirmNewInstallationWithOldLicense).getHttpRequest();

        Map<String, Object> dataMap = confirmNewInstallationWithOldLicense.getDataMap();
        assertThat(dataMap, hasEntry("thereAreValidLicenses", true));
    }

    @Test
    public void testMaintenanceExpiryMessageTakesPrecedence() {
        //given
        doReturn("42").when(confirmNewInstallationWithOldLicense).getServerId();
        doReturn(mock(HttpServletRequest.class)).when(confirmNewInstallationWithOldLicense).getHttpRequest();

        final String primaryGood = "primaryGood";
        final String secondaryGood = "secondaryGood";

        final LicenseDetails l1 = getDetailsWithMessages(ImmutableMap.of(
                MAINTENANCE_EXPIRY_PRIMARY_KEY, primaryGood,
                MAINTENANCE_EXPIRY_SECONDARY_KEY, secondaryGood));
        final LicenseDetails l2 = getDetailsWithMessages(ImmutableMap.of(
                SUBSCRIPTION_EXPIRY_PRIMARY_KEY, "bad1",
                SUBSCRIPTION_EXPIRY_SECONDARY_KEY, "bad2"));

        when(jiraLicenseManager.getLicenses()).thenReturn(Lists.newArrayList(l1, l2));

        //when
        Map<String, Object> data = confirmNewInstallationWithOldLicense.getDataMap();

        //then
        assertThat(data, hasEntry("licenseStatusMessage1Content", primaryGood));
        assertThat(data, hasEntry("licenseStatusMessage2Content", secondaryGood));
    }

    @Test
    public void testSubscriptionExpiryMessageIsReturnedWhenNoMaintenanceMessage() {
        //given
        doReturn("42").when(confirmNewInstallationWithOldLicense).getServerId();
        doReturn(mock(HttpServletRequest.class)).when(confirmNewInstallationWithOldLicense).getHttpRequest();

        final String primaryGood = "primaryGood";
        final String secondaryGood = "secondaryGood";

        final LicenseDetails l1 = mock(LicenseDetails.class);
        final LicenseDetails l2 = getDetailsWithMessages(ImmutableMap.of(
                SUBSCRIPTION_EXPIRY_PRIMARY_KEY, primaryGood,
                SUBSCRIPTION_EXPIRY_SECONDARY_KEY, secondaryGood));

        when(jiraLicenseManager.getLicenses()).thenReturn(ImmutableList.of(l1, l2));

        //when
        Map<String, Object> data = confirmNewInstallationWithOldLicense.getDataMap();

        //then
        assertThat(data, hasEntry("licenseStatusMessage1Content", primaryGood));
        assertThat(data, hasEntry("licenseStatusMessage2Content", secondaryGood));
    }

    @Test
    public void testErrorIfNoUserName() throws Exception {
        // Set up
        final MockServletContext mockServletContext = new MockServletContext();
        setLicenseTooOldEvent(mockServletContext);

        confirmNewInstallationWithOldLicense.setPassword(PASSWORD);

        // Set the confirmation so that no errors are raised for its absence (or the absence of the new license key)
        confirmNewInstallationWithOldLicense.setRadioOption(RADIO_OPTION_EVALUATION);

        // Invoke
        confirmNewInstallationWithOldLicense.execute();

        // Check
        assertThat(confirmNewInstallationWithOldLicense,
                containsSystemError(makeTranslation("admin.errors.invalid.username.or.pasword")));
    }

    private void setLicenseTooOldEvent(final MockServletContext mockServletContext) {
        ServletActionContext.setServletContext(mockServletContext);
        johnsonEventContainer.addEvent(new Event(EventType.get(LICENSE_TOO_OLD), "blah"));
    }

    @Test
    public void testErrorIfWrongUserName() throws Exception {
        // Set up
        when(crowdService.getUser("baduser")).thenReturn(null);
        confirmNewInstallationWithOldLicense.setUserName("baduser");

        final MockServletContext mockServletContext = new MockServletContext();
        setLicenseTooOldEvent(mockServletContext);

        // Set the confirmation so that no errors are raised for its absence (or the absence of the new license key)
        confirmNewInstallationWithOldLicense.setRadioOption(RADIO_OPTION_EVALUATION);

        // Invoke
        confirmNewInstallationWithOldLicense.execute();

        // Check
        assertThat(confirmNewInstallationWithOldLicense,
                containsSystemError(makeTranslation("admin.errors.invalid.username.or.pasword")));

    }

    @Test
    public void testErrorIfWrongPassword() throws Exception {
        setUpUser("testadminuser", "badpassword", true);

        final MockServletContext mockServletContext = new MockServletContext();
        setLicenseTooOldEvent(mockServletContext);

        // Set the confirmation so that no errors are raised for its absence (or the absence of the new license key)
        confirmNewInstallationWithOldLicense.setRadioOption(RADIO_OPTION_EVALUATION);

        // Invoke
        confirmNewInstallationWithOldLicense.execute();

        // Check
        assertThat(confirmNewInstallationWithOldLicense,
                containsSystemError(makeTranslation("admin.errors.invalid.username.or.pasword")));
    }

    @Test
    public void testErrorIfCredentialsCorrectButNotAdmin() throws Exception {
        setUpUser("testadminuser", PASSWORD, false);

        final MockServletContext mockServletContext = new MockServletContext();
        setLicenseTooOldEvent(mockServletContext);

        // Set the confirmation so that no errors are raised for its absence (or the absence of the new license key)
        confirmNewInstallationWithOldLicense.setRadioOption(RADIO_OPTION_EVALUATION);

        // Invoke
        confirmNewInstallationWithOldLicense.execute();

        // Check
        assertThat(confirmNewInstallationWithOldLicense,
                containsSystemError(makeTranslation("admin.errors.no.admin.permission")));
    }

    @Test
    public void testNoErrorIfCorrectAdminCredentialsProvided() throws Exception {
        ApplicationUser user = setUpUser("testadminuser", PASSWORD, true);

        final MockServletContext mockServletContext = new MockServletContext();
        setLicenseTooOldEvent(mockServletContext);

        confirmNewInstallationWithOldLicense.setRadioOption(RADIO_OPTION_EVALUATION);

        // Invoke
        confirmNewInstallationWithOldLicense.execute();

        // Check
        assertThat(confirmNewInstallationWithOldLicense, ErrorCollectionMatchers.isEmpty());
        verify(mockPermissionManager).hasPermission(GlobalPermissionKey.ADMINISTER, user);
        verifyNoMoreInteractions(mockPermissionManager);
    }

    @Test
    public void testNoErrorIfNotAdminWithSystemProperty() throws Exception {
        final ApplicationUser user = new MockApplicationUser("testadminuser", FAKE_USER_DIRECTORY_ID);
        when(crowdService.getUser("testadminuser")).thenReturn(user.getDirectoryUser());
        when(crowdService.authenticate("testadminuser", PASSWORD)).thenReturn(user.getDirectoryUser());

        System.setProperty(UPGRADE_SYSTEM_PROPERTY, "true");

        final MockServletContext mockServletContext = new MockServletContext();
        setLicenseTooOldEvent(mockServletContext);

        confirmNewInstallationWithOldLicense.setUserName(user.getName());
        confirmNewInstallationWithOldLicense.setPassword(PASSWORD);
        confirmNewInstallationWithOldLicense.setRadioOption(RADIO_OPTION_EVALUATION);

        // Invoke
        confirmNewInstallationWithOldLicense.execute();

        // Check
        assertThat(confirmNewInstallationWithOldLicense, ErrorCollectionMatchers.isEmpty());
        verifyZeroInteractions(mockPermissionManager);
    }

    @Test
    public void testErrorIfNoLicenseKeyAndNoConfirmation() throws Exception {
        // Set up
        setUpUser("testadminuser", PASSWORD, true);

        final MockServletContext mockServletContext = new MockServletContext();
        setLicenseTooOldEvent(mockServletContext);

        // Invoke
        confirmNewInstallationWithOldLicense.execute();

        // Check
        assertThat(confirmNewInstallationWithOldLicense,
                containsSystemError(makeTranslation("admin.errors.no.license")));
    }

    @Test
    public void testNoErrorIfLicenseKeySupplied() throws Exception {
        // Set up
        setUpUser("testadminuser", PASSWORD, true);

        final JiraLicenseService.ValidationResult validationResult = mock(JiraLicenseService.ValidationResult.class);
        final LicenseDetails licenseDetails = mock(LicenseDetails.class);
        final SimpleErrorCollection errorCollection = new SimpleErrorCollection();

        when(jiraLicenseService.validate(any(), eq(A_VALID_LICENSE))).thenReturn(validationResult);
        when(validationResult.getErrorCollection()).thenReturn(errorCollection);
        when(jiraLicenseService.setLicense(validationResult)).thenReturn(licenseDetails);
        when(maintenancePredicate.test(licenseDetails)).thenReturn(true);
        when(licenseDetails.isMaintenanceValidForBuildDate(buildDate)).thenReturn(true);


        final MockServletContext mockServletContext = new MockServletContext();
        setLicenseTooOldEvent(mockServletContext);

        confirmNewInstallationWithOldLicense.setLicense(A_VALID_LICENSE);
        confirmNewInstallationWithOldLicense.setRadioOption(ConfirmNewInstallationWithOldLicense.RADIO_OPTION_LICENSE);

        // Invoke
        confirmNewInstallationWithOldLicense.execute();

        // Check
        assertThat(confirmNewInstallationWithOldLicense, ErrorCollectionMatchers.isEmpty());
    }


    @Test
    public void testErrorIfOutdatedLicenseKey() throws Exception {
        // Set up
        final ApplicationUser user = new MockApplicationUser("testadminuser", FAKE_USER_DIRECTORY_ID);
        final ApplicationUser appUser = new DelegatingApplicationUser(user.getId(), "testadminuser", user.getDirectoryUser());
        when(crowdService.getUser("testadminuser")).thenReturn(user.getDirectoryUser());
        when(crowdService.authenticate("testadminuser", PASSWORD)).thenReturn(user.getDirectoryUser());
        when(mockPermissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, appUser)).thenReturn(true);

        final JiraLicenseService.ValidationResult validationResult = mock(JiraLicenseService.ValidationResult.class);
        final LicenseDetails licenseDetails = mock(LicenseDetails.class);
        final SimpleErrorCollection errorCollection = new SimpleErrorCollection();

        when(jiraLicenseService.validate(any(), eq(A_VALID_LICENSE))).thenReturn(validationResult);
        when(validationResult.getErrorCollection()).thenReturn(errorCollection);

        when(jiraLicenseService.setLicense(validationResult)).thenReturn(licenseDetails);
        when(maintenancePredicate.test(licenseDetails)).thenReturn(false);

        final MockServletContext mockServletContext = new MockServletContext();
        setLicenseTooOldEvent(mockServletContext);

        confirmNewInstallationWithOldLicense.setUserName(user.getName());
        confirmNewInstallationWithOldLicense.setPassword(PASSWORD);
        confirmNewInstallationWithOldLicense.setLicense(A_VALID_LICENSE);
        confirmNewInstallationWithOldLicense.setRadioOption(ConfirmNewInstallationWithOldLicense.RADIO_OPTION_LICENSE);

        // Invoke
        confirmNewInstallationWithOldLicense.execute();

        // Check
        assertThat(confirmNewInstallationWithOldLicense,
                containsFieldError("license", makeTranslation("admin.errors.license.too.old")));
    }

    @Test
    public void testErrorIfOutdatedLicenseKeyAndThereIsNoApplicationUserKeyForThisUser() throws Exception {
        ApplicationUser user = setUpUser("testadminuser", PASSWORD, false);

        when(keyService.getKeyForUsername("testadminuser")).thenReturn(null); // testing specific code path

        final MockServletContext mockServletContext = new MockServletContext();
        setLicenseTooOldEvent(mockServletContext);

        confirmNewInstallationWithOldLicense.setUserName(user.getName());
        confirmNewInstallationWithOldLicense.setPassword(PASSWORD);
        confirmNewInstallationWithOldLicense.setRadioOption(RADIO_OPTION_EVALUATION);

        // Invoke
        confirmNewInstallationWithOldLicense.execute();

        // Check
        assertThat(confirmNewInstallationWithOldLicense,
                containsSystemError(makeTranslation("admin.errors.no.admin.permission")));
        verify(mockPermissionManager).hasPermission(GlobalPermissionKey.ADMINISTER, user);
    }

    @Test
    public void testRemoveOutOfMaintenance() throws Exception {
        // Set up
        setUpUser("testadminuser", PASSWORD, true);

        //There are two out of maintenance licenses and one valid license
        final LicenseDetails expiredLicenseDetails1 = mock(LicenseDetails.class);
        final LicenseDetails expiredLicenseDetails2 = mock(LicenseDetails.class);
        final LicenseDetails validLicenseDetails = mock(LicenseDetails.class);

        when(maintenancePredicate.test(expiredLicenseDetails1)).thenReturn(false);
        when(maintenancePredicate.test(expiredLicenseDetails2)).thenReturn(false);
        when(jiraLicenseManager.getLicenses()).thenReturn(Lists.newArrayList(expiredLicenseDetails1,
                expiredLicenseDetails2, validLicenseDetails));

        final MockServletContext mockServletContext = new MockServletContext();
        setLicenseTooOldEvent(mockServletContext);

        confirmNewInstallationWithOldLicense.setRadioOption(ConfirmNewInstallationWithOldLicense.RADIO_OPTION_REMOVE_EXPIRED);

        // Invoke
        confirmNewInstallationWithOldLicense.execute();

        // Check
        assertThat(confirmNewInstallationWithOldLicense, ErrorCollectionMatchers.isEmpty());
        verify(jiraLicenseManager)
                .removeLicenses(argThat(containsInAnyOrder(expiredLicenseDetails1, expiredLicenseDetails2)));
    }

    /**
     * Creates an MockApplicationUser with name username and password PASSWORD. username and password will be entered in the form
     *
     * @param username the name of the user
     * @param password the password that will be entered in the form
     * @param isAdmin  if the user will be admin
     * @return created MockApplicationUser
     */
    private ApplicationUser setUpUser(String username, String password, boolean isAdmin) throws FailedAuthenticationException {
        final ApplicationUser user = new MockApplicationUser(username, FAKE_USER_DIRECTORY_ID);
        when(crowdService.getUser(username)).thenReturn(user.getDirectoryUser());
        when(crowdService.authenticate(username, PASSWORD)).thenReturn(user.getDirectoryUser());
        if (!password.equals(PASSWORD)) {
            when(crowdService.authenticate(username, password)).thenThrow(new FailedAuthenticationException());
        }
        when(mockPermissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, user)).thenReturn(isAdmin);
        confirmNewInstallationWithOldLicense.setUserName(username);
        confirmNewInstallationWithOldLicense.setPassword(password);
        return user;
    }

    private LicenseDetails getDetailsWithMessages(Map<String, String> messages) {
        final LicenseDetails.LicenseStatusMessage statusMessage = mock(LicenseDetails.LicenseStatusMessage.class);
        when(statusMessage.getAllMessages()).thenReturn(messages);

        final LicenseDetails details = mock(LicenseDetails.class);
        when(details.getLicenseStatusMessage(any(I18nHelper.class), eq(userManager))).thenReturn(statusMessage);

        return details;
    }
}
