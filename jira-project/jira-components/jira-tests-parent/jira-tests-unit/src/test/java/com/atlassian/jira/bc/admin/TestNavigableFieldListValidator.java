package com.atlassian.jira.bc.admin;

import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.fields.NavigableField;
import com.atlassian.jira.junit.rules.InitMockitoMocks;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Unit test for {@link NavigableFieldListValidator}.
 *
 * @since v4.4
 */
public class TestNavigableFieldListValidator {
    @Rule
    public InitMockitoMocks initMocks = new InitMockitoMocks(this);

    @Mock
    private NavigableField bazField;

    @Mock
    private NavigableField foobarField;

    @Mock
    private FieldManager fieldManager;

    @Before
    public void setUp() {
        when(bazField.getId()).thenReturn("baz");
        when(foobarField.getId()).thenReturn("foobar");
    }

    @Test
    public void testValidate() {
        when(fieldManager.getNavigableField("foobar")).thenReturn(foobarField);
        when(fieldManager.getNavigableField("baz")).thenReturn(bazField);
        when(fieldManager.getNavigableField("explodo")).thenThrow(new IllegalArgumentException("no way"));
        when(fieldManager.getNavigableField("missing")).thenReturn(null);

        final NavigableFieldListValidator validator = new NavigableFieldListValidator(fieldManager);

        assertFalse(validator.validate("foobar, missing").isValid());
        assertFalse(validator.validate("missing").isValid());
        assertFalse(validator.validate("foobar, baz, missing").isValid());
        assertFalse(validator.validate("explodo, baz, missing").isValid());
        assertFalse(validator.validate("foobar, baz, explodo").isValid());
        assertFalse(validator.validate("explodo").isValid());
        assertFalse(validator.validate("").isValid());

        assertTrue(validator.validate("foobar, baz").isValid());
        assertTrue(validator.validate("baz").isValid());

        verify(fieldManager, times(4)).getNavigableField("foobar");
        verify(fieldManager, times(5)).getNavigableField("baz");
        verify(fieldManager, times(3)).getNavigableField("explodo");
        verify(fieldManager, times(4)).getNavigableField("missing");
    }
}
