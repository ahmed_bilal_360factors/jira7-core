package com.atlassian.jira.upgrade.tasks.role.scenarios;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

/**
 * Executes every scenario placed in com.atlassian.jira.upgrade.tasks.role.scenario package.
 */
@RunWith(Cucumber.class)
@CucumberOptions(plugin = "pretty")
public class RenaissanceMigrationTest {
}
