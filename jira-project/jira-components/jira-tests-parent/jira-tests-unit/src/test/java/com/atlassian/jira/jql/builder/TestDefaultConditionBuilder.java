package com.atlassian.jira.jql.builder;

import com.atlassian.jira.util.collect.CollectionBuilder;
import com.atlassian.query.operand.EmptyOperand;
import com.atlassian.query.operand.Operand;
import com.atlassian.query.operand.Operands;
import com.atlassian.query.operand.SingleValueOperand;
import com.atlassian.query.operator.Operator;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.function.Supplier;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import static org.mockito.Mockito.when;

/**
 * Test for {@link com.atlassian.jira.jql.builder.DefaultJqlClauseBuilder}.
 *
 * @since v4.0
 */
public class TestDefaultConditionBuilder {
    @Rule
    public final MethodRule initMockito = MockitoJUnit.rule();
    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Mock
    private JqlClauseBuilder jqlClauseBuilder;

    @Test
    public void testConstructorNullClauseName() throws Exception {
        exception.expect(IllegalArgumentException.class);
        new DefaultConditionBuilder("blah", null);
    }

    @Test
    public void testConstructorNullBuilder() throws Exception {
        exception.expect(IllegalArgumentException.class);
        new DefaultConditionBuilder(null, jqlClauseBuilder);
    }

    @Test
    public void testEqString() throws Exception {
        final String name = "fieldName";
        final String value = "mine";

        when(jqlClauseBuilder.addStringCondition(name, Operator.EQUALS, value)).thenReturn(jqlClauseBuilder);

        DefaultConditionBuilder builder = new DefaultConditionBuilder(name, jqlClauseBuilder);
        assertSame(jqlClauseBuilder, builder.eq(value));
    }

    @Test
    public void testEqOperand() throws Exception {
        final String name = "fieldName";
        final Operand value = new SingleValueOperand("4372738");

        when(jqlClauseBuilder.addCondition(name, Operator.EQUALS, value)).thenReturn(jqlClauseBuilder);

        DefaultConditionBuilder builder = new DefaultConditionBuilder(name, jqlClauseBuilder);
        assertSame(jqlClauseBuilder, builder.eq(value));
    }

    @Test
    public void testEqLong() throws Exception {
        final String name = "fieldName";
        final long value = 1;

        when(jqlClauseBuilder.addNumberCondition(name, Operator.EQUALS, value)).thenReturn(jqlClauseBuilder);

        DefaultConditionBuilder builder = new DefaultConditionBuilder(name, jqlClauseBuilder);
        assertSame(jqlClauseBuilder, builder.eq(value));
    }

    @Test
    public void testEqBuilder() throws Exception {
        final String name = "fieldName";
        final long value = 1;

        when(jqlClauseBuilder.addNumberCondition(name, Operator.EQUALS, value)).thenReturn(jqlClauseBuilder);

        DefaultConditionBuilder builder = new DefaultConditionBuilder(name, jqlClauseBuilder);
        final ValueBuilder valueBuilder = builder.eq();
        assertNotNull(valueBuilder);
        assertSame(jqlClauseBuilder, valueBuilder.number(value));
    }

    @Test
    public void testEqEmpty() throws Exception {
        final String name = "fieldName";

        when(jqlClauseBuilder.addCondition(name, Operator.EQUALS, EmptyOperand.EMPTY)).thenReturn(jqlClauseBuilder);

        DefaultConditionBuilder builder = new DefaultConditionBuilder(name, jqlClauseBuilder);
        assertSame(jqlClauseBuilder, builder.eqEmpty());
    }

    @Test
    public void testEqDate() throws Exception {
        assertDate("testEqDate", Operator.EQUALS, ConditionBuilder::eq);
    }

    @Test
    public void testEqFunction() throws Exception {
        assertTestFunction("fieldName", Operator.EQUALS, "myFunc", (builder, argument) -> builder.eqFunc(argument.funcName));
    }

    @Test
    public void testEqFunctionVarArgs() throws Exception {
        assertFunctionVarArgs("clauseName", Operator.EQUALS, "funcName", (builder, argument) -> builder.eqFunc(argument.getFuncName(), argument.getArgument()));
    }

    @Test
    public void testEqFunctionCollection() throws Exception {
        final Supplier<TestCallable<FunctionArgument<Collection<String>>, JqlClauseBuilder>> callableSupplier = () ->
                ((builder, argument) -> builder.eqFunc(argument.getFuncName(), argument.getArgument()));

        assertFunctionCollection("testOne", Operator.EQUALS, "funcName", callableSupplier.get(), Collections.<String>emptyList());
        Collection<String> args = CollectionBuilder.newBuilder("arg1", "arg2", "...", "argN").asCollection();
        assertFunctionCollection("testTwo", Operator.EQUALS, "funcName", callableSupplier.get(), args);
    }

    @Test
    public void testNotEqString() throws Exception {
        final String name = "fieldName";
        final String value = "mine";

        when(jqlClauseBuilder.addStringCondition(name, Operator.NOT_EQUALS, value)).thenReturn(jqlClauseBuilder);

        DefaultConditionBuilder builder = new DefaultConditionBuilder(name, jqlClauseBuilder);
        assertSame(jqlClauseBuilder, builder.notEq(value));
    }

    @Test
    public void testNotEqOperand() throws Exception {
        final String name = "fieldName";
        final Operand value = new SingleValueOperand("4372738");

        when(jqlClauseBuilder.addCondition(name, Operator.NOT_EQUALS, value)).thenReturn(jqlClauseBuilder);

        DefaultConditionBuilder builder = new DefaultConditionBuilder(name, jqlClauseBuilder);
        assertSame(jqlClauseBuilder, builder.notEq(value));
    }

    @Test
    public void testNotEqLong() throws Exception {
        final String name = "fieldName";
        final long value = 1;

        when(jqlClauseBuilder.addNumberCondition(name, Operator.NOT_EQUALS, value)).thenReturn(jqlClauseBuilder);

        DefaultConditionBuilder builder = new DefaultConditionBuilder(name, jqlClauseBuilder);
        assertSame(jqlClauseBuilder, builder.notEq(value));
    }

    @Test
    public void testNotEqBuilder() throws Exception {
        final String name = "fieldName";
        final long value = 1;

        when(jqlClauseBuilder.addNumberCondition(name, Operator.NOT_EQUALS, value)).thenReturn(jqlClauseBuilder);

        DefaultConditionBuilder builder = new DefaultConditionBuilder(name, jqlClauseBuilder);
        final ValueBuilder valueBuilder = builder.notEq();
        assertNotNull(valueBuilder);
        assertSame(jqlClauseBuilder, valueBuilder.number(value));
    }

    @Test
    public void testNotEqEmpty() throws Exception {
        final String name = "fieldName";

        when(jqlClauseBuilder.addCondition(name, Operator.NOT_EQUALS, EmptyOperand.EMPTY)).thenReturn(jqlClauseBuilder);

        DefaultConditionBuilder builder = new DefaultConditionBuilder(name, jqlClauseBuilder);
        assertSame(jqlClauseBuilder, builder.notEqEmpty());
    }

    @Test
    public void testNotEqDate() throws Exception {
        assertDate("testNotEqDate", Operator.NOT_EQUALS, ConditionBuilder::notEq);
    }

    @Test
    public void testNotEqFunction() throws Exception {
        assertTestFunction("fieldName", Operator.NOT_EQUALS, "testNotEqFunction", (builder, argument) -> builder.notEqFunc(argument.getFuncName()));
    }

    @Test
    public void testNotEqFunctionVarArgs() throws Exception {
        assertFunctionVarArgs("clauseName", Operator.NOT_EQUALS, "testNotEqFunctionVarArgs", (builder, argument) -> builder.notEqFunc(argument.getFuncName(), argument.getArgument()));
    }

    @Test
    public void testNotEqFunctionCollection() throws Exception {
        final Supplier<TestCallable<FunctionArgument<Collection<String>>, JqlClauseBuilder>> callableSupplier = () ->
                ((builder, argument) -> builder.notEqFunc(argument.getFuncName(), argument.getArgument()));

        assertFunctionCollection("testOne", Operator.NOT_EQUALS, "ertter", callableSupplier.get(), Collections.<String>emptyList());
        Collection<String> args = CollectionBuilder.newBuilder("arg1", "arg2", "...", "argN").asCollection();
        assertFunctionCollection("adsa", Operator.NOT_EQUALS, "ertter", callableSupplier.get(), args);
    }

    @Test
    public void testLikeString() throws Exception {
        final String name = "fieldName";
        final String value = "mine";

        when(jqlClauseBuilder.addStringCondition(name, Operator.LIKE, value)).thenReturn(jqlClauseBuilder);

        DefaultConditionBuilder builder = new DefaultConditionBuilder(name, jqlClauseBuilder);
        assertSame(jqlClauseBuilder, builder.like(value));
    }

    @Test
    public void testLikeOperand() throws Exception {
        final String name = "fieldName";
        final Operand value = new SingleValueOperand("4372738");

        when(jqlClauseBuilder.addCondition(name, Operator.LIKE, value)).thenReturn(jqlClauseBuilder);

        DefaultConditionBuilder builder = new DefaultConditionBuilder(name, jqlClauseBuilder);
        assertSame(jqlClauseBuilder, builder.like(value));
    }

    @Test
    public void testLikeLong() throws Exception {
        final String name = "fieldName";
        final long value = 1;

        when(jqlClauseBuilder.addNumberCondition(name, Operator.LIKE, value)).thenReturn(jqlClauseBuilder);

        DefaultConditionBuilder builder = new DefaultConditionBuilder(name, jqlClauseBuilder);
        assertSame(jqlClauseBuilder, builder.like(value));
    }

    @Test
    public void testLikeBuilder() throws Exception {
        final String name = "fieldName";
        final long value = 1;

        when(jqlClauseBuilder.addNumberCondition(name, Operator.LIKE, value)).thenReturn(jqlClauseBuilder);

        DefaultConditionBuilder builder = new DefaultConditionBuilder(name, jqlClauseBuilder);
        final ValueBuilder valueBuilder = builder.like();
        assertNotNull(valueBuilder);
        assertSame(jqlClauseBuilder, valueBuilder.number(value));
    }

    @Test
    public void testLikeDate() throws Exception {
        assertDate("testNotNotNotNotLikeDate", Operator.LIKE, ConditionBuilder::like);
    }

    @Test
    public void testLikeFunction() throws Exception {
        assertTestFunction("aaaa", Operator.LIKE, "testLikeFunction", (builder, argument) -> builder.likeFunc(argument.getFuncName()));
    }

    @Test
    public void testLikeFunctionVarArgs() throws Exception {
        assertFunctionVarArgs("bbbb", Operator.LIKE, "testLikeFunctionVarArgs", (builder, argument) -> builder.likeFunc(argument.getFuncName(), argument.getArgument()));
    }

    @Test
    public void testLikeFunctionCollection() throws Exception {
        final Supplier<TestCallable<FunctionArgument<Collection<String>>, JqlClauseBuilder>> callableSupplier = () ->
                ((builder, argument) -> builder.likeFunc(argument.getFuncName(), argument.getArgument()));

        assertFunctionCollection("testOne", Operator.LIKE, "ertter", callableSupplier.get(), Collections.<String>emptyList());
        Collection<String> args = CollectionBuilder.newBuilder("arg1", "arg2", "...", "argN").asCollection();
        assertFunctionCollection("adsa", Operator.LIKE, "ertter", callableSupplier.get(), args);
    }

    @Test
    public void testNotlikeOperand() throws Exception {
        final String name = "fieldName";
        final Operand value = new SingleValueOperand("4372738");

        when(jqlClauseBuilder.addCondition(name, Operator.NOT_LIKE, value)).thenReturn(jqlClauseBuilder);

        DefaultConditionBuilder builder = new DefaultConditionBuilder(name, jqlClauseBuilder);
        assertSame(jqlClauseBuilder, builder.notLike(value));
    }

    @Test
    public void testNotlikeLong() throws Exception {
        final String name = "fieldName";
        final long value = 1;

        when(jqlClauseBuilder.addNumberCondition(name, Operator.NOT_LIKE, value)).thenReturn(jqlClauseBuilder);

        DefaultConditionBuilder builder = new DefaultConditionBuilder(name, jqlClauseBuilder);
        assertSame(jqlClauseBuilder, builder.notLike(value));
    }

    @Test
    public void testNotlikeBuilder() throws Exception {
        final String name = "fieldName";
        final long value = 1;

        when(jqlClauseBuilder.addNumberCondition(name, Operator.NOT_LIKE, value)).thenReturn(jqlClauseBuilder);

        DefaultConditionBuilder builder = new DefaultConditionBuilder(name, jqlClauseBuilder);
        final ValueBuilder valueBuilder = builder.notLike();
        assertNotNull(valueBuilder);
        assertSame(jqlClauseBuilder, valueBuilder.number(value));
    }

    @Test
    public void testNotLikeDate() throws Exception {
        assertDate("testNotLikeDate", Operator.NOT_LIKE, ConditionBuilder::notLike);
    }

    @Test
    public void testNotLikeFunction() throws Exception {
        assertTestFunction("ewtkjwijrwke", Operator.NOT_LIKE, "wqrkw;lkrl;ew", (builder, argument) -> builder.notLikeFunc(argument.getFuncName()));
    }

    @Test
    public void testNotLikeFunctionVarArgs() throws Exception {
        assertFunctionVarArgs("qewqke;lqkewq", Operator.NOT_LIKE, "af;alk;elkq;elwqdsajfaghekjrtiuywqroi", (builder, argument) -> builder.notLikeFunc(argument.getFuncName(), argument.getArgument()));
    }

    @Test
    public void testNotLikeFunctionCollection() throws Exception {
        final Supplier<TestCallable<FunctionArgument<Collection<String>>, JqlClauseBuilder>> callableSupplier = () ->
                ((builder, argument) -> builder.notLikeFunc(argument.getFuncName(), argument.getArgument()));

        assertFunctionCollection("qweqwrewr", Operator.NOT_LIKE, "ewqewqeq", callableSupplier.get(), Collections.<String>emptyList());
        Collection<String> args = CollectionBuilder.newBuilder("arg1", "arg2", "...", "argN").asCollection();
        assertFunctionCollection("qewqewqewqe", Operator.NOT_LIKE, "ertter", callableSupplier.get(), args);
    }

    @Test
    public void testIsEmpty() throws Exception {
        final String name = "fieldName";

        when(jqlClauseBuilder.addCondition(name, Operator.IS, EmptyOperand.EMPTY)).thenReturn(jqlClauseBuilder);

        DefaultConditionBuilder builder = new DefaultConditionBuilder(name, jqlClauseBuilder);
        assertSame(jqlClauseBuilder, builder.isEmpty());
    }

    @Test
    public void testIsBuilder() throws Exception {
        final String name = "fieldName";

        when(jqlClauseBuilder.addCondition(name, Operator.IS, EmptyOperand.EMPTY)).thenReturn(jqlClauseBuilder);

        DefaultConditionBuilder builder = new DefaultConditionBuilder(name, jqlClauseBuilder);
        final ValueBuilder valueBuilder = builder.is();
        assertNotNull(valueBuilder);
        assertSame(jqlClauseBuilder, valueBuilder.empty());
    }

    @Test
    public void testIsNotEmpty() throws Exception {
        final String name = "fieldName";

        when(jqlClauseBuilder.addCondition(name, Operator.IS_NOT, EmptyOperand.EMPTY)).thenReturn(jqlClauseBuilder);

        DefaultConditionBuilder builder = new DefaultConditionBuilder(name, jqlClauseBuilder);
        assertSame(jqlClauseBuilder, builder.isNotEmpty());
    }

    @Test
    public void testIsNotBuilder() throws Exception {
        final String name = "fieldName";

        when(jqlClauseBuilder.addCondition(name, Operator.IS_NOT, EmptyOperand.EMPTY)).thenReturn(jqlClauseBuilder);

        DefaultConditionBuilder builder = new DefaultConditionBuilder(name, jqlClauseBuilder);
        final ValueBuilder valueBuilder = builder.isNot();
        assertNotNull(valueBuilder);
        assertSame(jqlClauseBuilder, valueBuilder.empty());
    }

    @Test
    public void testLtString() throws Exception {
        final String name = "fieldName";
        final String value = "mine";

        when(jqlClauseBuilder.addStringCondition(name, Operator.LESS_THAN, value)).thenReturn(jqlClauseBuilder);

        DefaultConditionBuilder builder = new DefaultConditionBuilder(name, jqlClauseBuilder);
        assertSame(jqlClauseBuilder, builder.lt(value));
    }

    @Test
    public void testLtOperand() throws Exception {
        final String name = "fieldName";
        final Operand value = new SingleValueOperand("4372738");

        when(jqlClauseBuilder.addCondition(name, Operator.LESS_THAN, value)).thenReturn(jqlClauseBuilder);

        DefaultConditionBuilder builder = new DefaultConditionBuilder(name, jqlClauseBuilder);
        assertSame(jqlClauseBuilder, builder.lt(value));
    }

    @Test
    public void testLtLong() throws Exception {
        final String name = "fieldName";
        final long value = 1;

        when(jqlClauseBuilder.addNumberCondition(name, Operator.LESS_THAN, value)).thenReturn(jqlClauseBuilder);

        DefaultConditionBuilder builder = new DefaultConditionBuilder(name, jqlClauseBuilder);
        assertSame(jqlClauseBuilder, builder.lt(value));
    }

    @Test
    public void testLtBuilder() throws Exception {
        final String name = "fieldName";
        final long value = 1;

        when(jqlClauseBuilder.addNumberCondition(name, Operator.LESS_THAN, value)).thenReturn(jqlClauseBuilder);

        DefaultConditionBuilder builder = new DefaultConditionBuilder(name, jqlClauseBuilder);
        final ValueBuilder valueBuilder = builder.lt();
        assertNotNull(valueBuilder);
        assertSame(jqlClauseBuilder, valueBuilder.number(value));
    }

    @Test
    public void testLtDate() throws Exception {
        assertDate("testLtDate", Operator.LESS_THAN, ConditionBuilder::lt);
    }

    @Test
    public void testLtFunction() throws Exception {
        assertTestFunction("asdfgh", Operator.LESS_THAN, "23j3j", (builder, argument) -> builder.ltFunc(argument.getFuncName()));
    }

    @Test
    public void testLtFunctionVarArgs() throws Exception {
        assertFunctionVarArgs("zxcvbn", Operator.LESS_THAN, "fndsfsjfkjlsd", (builder, argument) -> builder.ltFunc(argument.getFuncName(), argument.getArgument()));
    }

    @Test
    public void testLtFunctionCollection() throws Exception {
        final Supplier<TestCallable<FunctionArgument<Collection<String>>, JqlClauseBuilder>> callableSupplier = () ->
                ((builder, argument) -> builder.ltFunc(argument.getFuncName(), argument.getArgument()));

        assertFunctionCollection("euroiwuer", Operator.LESS_THAN, "wwww", callableSupplier.get(), Collections.<String>emptyList());
        Collection<String> args = CollectionBuilder.newBuilder("arg1", "arg2", "...", "argN").asCollection();
        assertFunctionCollection("aaaa", Operator.LESS_THAN, "bbb", callableSupplier.get(), args);
    }

    @Test
    public void testLtEqOperand() throws Exception {
        final String name = "fieldName";
        final Operand value = new SingleValueOperand("4372738");

        when(jqlClauseBuilder.addCondition(name, Operator.LESS_THAN_EQUALS, value)).thenReturn(jqlClauseBuilder);

        DefaultConditionBuilder builder = new DefaultConditionBuilder(name, jqlClauseBuilder);
        assertSame(jqlClauseBuilder, builder.ltEq(value));
    }

    @Test
    public void testLtEqLong() throws Exception {
        final String name = "fieldName";
        final long value = 1;

        when(jqlClauseBuilder.addNumberCondition(name, Operator.LESS_THAN_EQUALS, value)).thenReturn(jqlClauseBuilder);

        DefaultConditionBuilder builder = new DefaultConditionBuilder(name, jqlClauseBuilder);
        assertSame(jqlClauseBuilder, builder.ltEq(value));
    }

    @Test
    public void testLtEqBuilder() throws Exception {
        final String name = "fieldName";
        final long value = 1;

        when(jqlClauseBuilder.addNumberCondition(name, Operator.LESS_THAN_EQUALS, value)).thenReturn(jqlClauseBuilder);

        DefaultConditionBuilder builder = new DefaultConditionBuilder(name, jqlClauseBuilder);
        final ValueBuilder valueBuilder = builder.ltEq();
        assertNotNull(valueBuilder);
        assertSame(jqlClauseBuilder, valueBuilder.number(value));
    }

    @Test
    public void testLtEqDate() throws Exception {
        assertDate("testLtEqDate", Operator.LESS_THAN_EQUALS, ConditionBuilder::ltEq);
    }

    @Test
    public void testLtEqFunction() throws Exception {
        assertTestFunction("aaaa", Operator.LESS_THAN_EQUALS, "aaaaaaaa", (builder, argument) -> builder.ltEqFunc(argument.getFuncName()));
    }

    @Test
    public void testLtEqFunctionVarArgs() throws Exception {
        assertFunctionVarArgs("bbbbbb", Operator.LESS_THAN_EQUALS, "bbbbbbb", (builder, argument) -> builder.ltEqFunc(argument.getFuncName(), argument.getArgument()));
    }

    @Test
    public void testLtEqFunctionCollection() throws Exception {
        final Supplier<TestCallable<FunctionArgument<Collection<String>>, JqlClauseBuilder>> callableSupplier = () ->
                ((builder, argument) -> builder.ltEqFunc(argument.getFuncName(), argument.getArgument()));

        assertFunctionCollection("ccccc", Operator.LESS_THAN_EQUALS, "ccccc", callableSupplier.get(), Collections.<String>emptyList());
        Collection<String> args = CollectionBuilder.newBuilder("arg1", "arg2", "...", "argN").asCollection();
        assertFunctionCollection("dddddd", Operator.LESS_THAN_EQUALS, "dddd", callableSupplier.get(), args);
    }

    @Test
    public void testGtString() throws Exception {
        final String name = "fieldName";
        final String value = "mine";

        when(jqlClauseBuilder.addStringCondition(name, Operator.GREATER_THAN, value)).thenReturn(jqlClauseBuilder);

        DefaultConditionBuilder builder = new DefaultConditionBuilder(name, jqlClauseBuilder);
        assertSame(jqlClauseBuilder, builder.gt(value));
    }

    @Test
    public void testGtOperand() throws Exception {
        final String name = "fieldName";
        final Operand value = new SingleValueOperand("4372738");

        when(jqlClauseBuilder.addCondition(name, Operator.GREATER_THAN, value)).thenReturn(jqlClauseBuilder);

        DefaultConditionBuilder builder = new DefaultConditionBuilder(name, jqlClauseBuilder);
        assertSame(jqlClauseBuilder, builder.gt(value));
    }

    @Test
    public void testGtLong() throws Exception {
        final String name = "fieldName";
        final long value = 1;

        when(jqlClauseBuilder.addNumberCondition(name, Operator.GREATER_THAN, value)).thenReturn(jqlClauseBuilder);

        DefaultConditionBuilder builder = new DefaultConditionBuilder(name, jqlClauseBuilder);
        assertSame(jqlClauseBuilder, builder.gt(value));
    }

    @Test
    public void testGtBuilder() throws Exception {
        final String name = "fieldName";
        final long value = 1;

        when(jqlClauseBuilder.addNumberCondition(name, Operator.GREATER_THAN, value)).thenReturn(jqlClauseBuilder);

        DefaultConditionBuilder builder = new DefaultConditionBuilder(name, jqlClauseBuilder);
        final ValueBuilder valueBuilder = builder.gt();
        assertNotNull(valueBuilder);
        assertSame(jqlClauseBuilder, valueBuilder.number(value));
    }

    @Test
    public void testGtDate() throws Exception {
        assertDate("testGtDate", Operator.GREATER_THAN, ConditionBuilder::gt);
    }

    @Test
    public void testGtFunction() throws Exception {
        assertTestFunction("qwerty", Operator.GREATER_THAN, "qwerty", (builder, argument) -> builder.gtFunc(argument.getFuncName()));
    }

    @Test
    public void testGtFunctionVarArgs() throws Exception {
        assertFunctionVarArgs("zxcvbnm", Operator.GREATER_THAN, "zxcvbnm", (builder, argument) -> builder.gtFunc(argument.getFuncName(), argument.getArgument()));
    }

    @Test
    public void testGtFunctionCollection() throws Exception {
        final Supplier<TestCallable<FunctionArgument<Collection<String>>, JqlClauseBuilder>> callableSupplier = () ->
                ((builder, argument) -> builder.gtFunc(argument.getFuncName(), argument.getArgument()));

        assertFunctionCollection("ccccc", Operator.GREATER_THAN, "ccccc", callableSupplier.get(), Collections.<String>emptyList());
        Collection<String> args = CollectionBuilder.newBuilder("arg1", "arg2", "...", "argN").asCollection();
        assertFunctionCollection("dddddd", Operator.GREATER_THAN, "dddd", callableSupplier.get(), args);
    }

    @Test
    public void testGtEqOperand() throws Exception {
        final String name = "fieldName";
        final Operand value = new SingleValueOperand("4372738");

        when(jqlClauseBuilder.addCondition(name, Operator.GREATER_THAN_EQUALS, value)).thenReturn(jqlClauseBuilder);

        DefaultConditionBuilder builder = new DefaultConditionBuilder(name, jqlClauseBuilder);
        assertSame(jqlClauseBuilder, builder.gtEq(value));
    }

    @Test
    public void testGtEqLong() throws Exception {
        final String name = "fieldName";
        final long value = 1;

        when(jqlClauseBuilder.addNumberCondition(name, Operator.GREATER_THAN_EQUALS, value)).thenReturn(jqlClauseBuilder);

        DefaultConditionBuilder builder = new DefaultConditionBuilder(name, jqlClauseBuilder);
        assertSame(jqlClauseBuilder, builder.gtEq(value));
    }

    @Test
    public void testGtEqBuilder() throws Exception {
        final String name = "fieldName";
        final long value = 1;

        when(jqlClauseBuilder.addNumberCondition(name, Operator.GREATER_THAN_EQUALS, value)).thenReturn(jqlClauseBuilder);

        DefaultConditionBuilder builder = new DefaultConditionBuilder(name, jqlClauseBuilder);
        final ValueBuilder valueBuilder = builder.gtEq();
        assertNotNull(valueBuilder);
        assertSame(jqlClauseBuilder, valueBuilder.number(value));
    }

    @Test
    public void testGtEqDate() throws Exception {
        assertDate("testGtEqDate", Operator.GREATER_THAN_EQUALS, ConditionBuilder::gtEq);
    }

    @Test
    public void testGtEqFunction() throws Exception {
        assertTestFunction("ghjkl", Operator.GREATER_THAN_EQUALS, "fghjkl", (builder, argument) -> builder.gtEqFunc(argument.getFuncName()));
    }

    @Test
    public void testGtEqFunctionVarArgs() throws Exception {
        assertFunctionVarArgs("zxcvbnm", Operator.GREATER_THAN_EQUALS, "zxcvbnm", (builder, argument) -> builder.gtEqFunc(argument.getFuncName(), argument.getArgument()));
    }

    @Test
    public void testGtEqFunctionCollection() throws Exception {
        final Supplier<TestCallable<FunctionArgument<Collection<String>>, JqlClauseBuilder>> callableSupplier = () ->
                ((builder, argument) -> builder.gtEqFunc(argument.getFuncName(), argument.getArgument()));

        assertFunctionCollection("ccccc", Operator.GREATER_THAN_EQUALS, "ccccc", callableSupplier.get(), Collections.<String>emptyList());
        Collection<String> args = CollectionBuilder.newBuilder("arg1", "arg2", "...", "argN").asCollection();
        assertFunctionCollection("dddddd", Operator.GREATER_THAN_EQUALS, "dddd", callableSupplier.get(), args);
    }

    @Test
    public void testInStrings() throws Exception {
        final String name = "fieldName";
        final String value1 = "value1";
        final String value2 = "value2";

        when(jqlClauseBuilder.addStringCondition(name, Operator.IN, value1, value2)).thenReturn(jqlClauseBuilder);

        DefaultConditionBuilder builder = new DefaultConditionBuilder(name, jqlClauseBuilder);
        assertSame(jqlClauseBuilder, builder.in(value1, value2));
    }

    @Test
    public void testInCollectionStrings() throws Exception {
        final String name = "fieldName";
        final String value1 = "value1";
        final String value2 = "value2";
        final Collection<String> values = CollectionBuilder.newBuilder(value1, value2).asList();

        when(jqlClauseBuilder.addStringCondition(name, Operator.IN, values)).thenReturn(jqlClauseBuilder);

        DefaultConditionBuilder builder = new DefaultConditionBuilder(name, jqlClauseBuilder);
        assertSame(jqlClauseBuilder, builder.inStrings(values));
    }

    @Test
    public void testInLongs() throws Exception {
        final String name = "fieldName";
        final long value1 = 5;
        final long value2 = 94092840932L;

        when(jqlClauseBuilder.addNumberCondition(name, Operator.IN, value1, value2)).thenReturn(jqlClauseBuilder);

        DefaultConditionBuilder builder = new DefaultConditionBuilder(name, jqlClauseBuilder);
        assertSame(jqlClauseBuilder, builder.in(value1, value2));
    }

    @Test
    public void testInCollectionLongs() throws Exception {
        final String name = "fieldName";
        final long value1 = 56690342893482L;
        final long value2 = -2381932L;
        final Collection<Long> values = CollectionBuilder.newBuilder(value1, value2).asList();

        when(jqlClauseBuilder.addNumberCondition(name, Operator.IN, values)).thenReturn(jqlClauseBuilder);

        DefaultConditionBuilder builder = new DefaultConditionBuilder(name, jqlClauseBuilder);
        assertSame(jqlClauseBuilder, builder.inNumbers(values));
    }

    @Test
    public void testInOperands() throws Exception {
        final String name = "fieldName";
        final Operand value1 = Operands.valueOf(5L);

        when(jqlClauseBuilder.addCondition(name, Operator.IN, new Operand[]{value1})).thenReturn(jqlClauseBuilder);

        DefaultConditionBuilder builder = new DefaultConditionBuilder(name, jqlClauseBuilder);
        assertSame(jqlClauseBuilder, builder.in(value1));
    }

    @Test
    public void testInCollectionOperands() throws Exception {
        final String name = "fieldName";
        final Operand value1 = Operands.valueOf(56690342893482L);
        final Operand value2 = Operands.valueOf(-2381932L);
        final Collection<Operand> values = CollectionBuilder.newBuilder(value1, value2).asList();

        when(jqlClauseBuilder.addCondition(name, Operator.IN, values)).thenReturn(jqlClauseBuilder);

        DefaultConditionBuilder builder = new DefaultConditionBuilder(name, jqlClauseBuilder);
        assertSame(jqlClauseBuilder, builder.inOperands(values));
    }

    @Test
    public void testInDatesVarArgs() throws Exception {
        final String name = "fieldName";
        final Date date1 = new Date(248294038423L);
        final Date date2 = new Date(2837229588L);

        Date[] dates = new Date[]{date1, date2};

        when(jqlClauseBuilder.addDateCondition(name, Operator.IN, dates)).thenReturn(jqlClauseBuilder);

        DefaultConditionBuilder builder = new DefaultConditionBuilder(name, jqlClauseBuilder);
        assertSame(jqlClauseBuilder, builder.in(dates));
    }

    @Test
    public void testInDatesCollection() throws Exception {
        final String name = "fieldName";
        final Date date1 = new Date(2482943L);
        final Date date2 = new Date(283722679588L);

        Collection<Date> values = CollectionBuilder.newBuilder(date1, date2).asCollection();

        when(jqlClauseBuilder.addDateCondition(name, Operator.IN, values)).thenReturn(jqlClauseBuilder);

        DefaultConditionBuilder builder = new DefaultConditionBuilder(name, jqlClauseBuilder);
        assertSame(jqlClauseBuilder, builder.inDates(values));
    }

    @Test
    public void testInFunction() throws Exception {
        assertTestFunction("qqqq", Operator.IN, "qqqqqq", (builder, argument) -> builder.inFunc(argument.getFuncName()));
    }

    @Test
    public void testInFunctionVarArgs() throws Exception {
        assertFunctionVarArgs("zxcvb", Operator.IN, "lkjhggf", (builder, argument) -> builder.inFunc(argument.getFuncName(), argument.getArgument()));
    }

    @Test
    public void testInFunctionCollection() throws Exception {
        final Supplier<TestCallable<FunctionArgument<Collection<String>>, JqlClauseBuilder>> callableSupplier = () ->
                ((builder, argument) -> builder.inFunc(argument.getFuncName(), argument.getArgument()));

        assertFunctionCollection("ccccc", Operator.IN, "ccccc", callableSupplier.get(), Collections.<String>emptyList());
        Collection<String> args = CollectionBuilder.newBuilder("arg1", "arg2", "...", "argN").asCollection();
        assertFunctionCollection("dqweqweqw", Operator.IN, "wqwqwq", callableSupplier.get(), args);
    }

    @Test
    public void testInBuilder() throws Exception {
        final String name = "fieldName";
        final long value = 5L;

        when(jqlClauseBuilder.addNumberCondition(name, Operator.IN, new Long[]{value})).thenReturn(jqlClauseBuilder);

        DefaultConditionBuilder builder = new DefaultConditionBuilder(name, jqlClauseBuilder);
        final ValueBuilder valueBuilder = builder.in();
        assertNotNull(valueBuilder);
        assertSame(jqlClauseBuilder, valueBuilder.numbers(value));
    }

    @Test
    public void testNotInStrings() throws Exception {
        final String name = "fieldName";
        final String value1 = "value1";
        final String value2 = "value2";

        when(jqlClauseBuilder.addStringCondition(name, Operator.NOT_IN, value1, value2)).thenReturn(jqlClauseBuilder);

        DefaultConditionBuilder builder = new DefaultConditionBuilder(name, jqlClauseBuilder);
        assertSame(jqlClauseBuilder, builder.notIn(value1, value2));
    }

    @Test
    public void testNotInCollectionStrings() throws Exception {
        final String name = "fieldName";
        final String value1 = "value1";
        final String value2 = "value2";
        final Collection<String> values = CollectionBuilder.newBuilder(value1, value2).asList();

        when(jqlClauseBuilder.addStringCondition(name, Operator.NOT_IN, values)).thenReturn(jqlClauseBuilder);

        DefaultConditionBuilder builder = new DefaultConditionBuilder(name, jqlClauseBuilder);
        assertSame(jqlClauseBuilder, builder.notInStrings(values));
    }

    @Test
    public void testNotInLongs() throws Exception {
        final String name = "fieldName";
        final long value1 = 5;
        final long value2 = 94092840932L;

        when(jqlClauseBuilder.addNumberCondition(name, Operator.NOT_IN, value1, value2)).thenReturn(jqlClauseBuilder);

        DefaultConditionBuilder builder = new DefaultConditionBuilder(name, jqlClauseBuilder);
        assertSame(jqlClauseBuilder, builder.notIn(value1, value2));
    }

    @Test
    public void testNotInCollectionLongs() throws Exception {
        final String name = "fieldName";
        final long value1 = 56690342893482L;
        final long value2 = -2381932L;
        final Collection<Long> values = CollectionBuilder.newBuilder(value1, value2).asList();

        when(jqlClauseBuilder.addNumberCondition(name, Operator.NOT_IN, values)).thenReturn(jqlClauseBuilder);

        DefaultConditionBuilder builder = new DefaultConditionBuilder(name, jqlClauseBuilder);
        assertSame(jqlClauseBuilder, builder.notInNumbers(values));
    }

    @Test
    public void testNotInOperands() throws Exception {
        final String name = "fieldName";
        final Operand value1 = Operands.valueOf(5L);

        when(jqlClauseBuilder.addCondition(name, Operator.NOT_IN, new Operand[]{value1})).thenReturn(jqlClauseBuilder);

        DefaultConditionBuilder builder = new DefaultConditionBuilder(name, jqlClauseBuilder);
        assertSame(jqlClauseBuilder, builder.notIn(value1));
    }

    @Test
    public void testNotInCollectionOperands() throws Exception {
        final String name = "fieldName";
        final Operand value1 = Operands.valueOf(56690342893482L);
        final Operand value2 = Operands.valueOf(-2381932L);
        final Collection<Operand> values = CollectionBuilder.newBuilder(value1, value2).asList();

        when(jqlClauseBuilder.addCondition(name, Operator.NOT_IN, values)).thenReturn(jqlClauseBuilder);

        DefaultConditionBuilder builder = new DefaultConditionBuilder(name, jqlClauseBuilder);
        assertSame(jqlClauseBuilder, builder.notInOperands(values));
    }

    @Test
    public void testNotInDatesVarArgs() throws Exception {
        final String name = "fieldName";
        final Date date1 = new Date(248294038423L);
        final Date date2 = new Date(2837229588L);

        Date[] dates = new Date[]{date1, date2};

        when(jqlClauseBuilder.addDateCondition(name, Operator.NOT_IN, dates)).thenReturn(jqlClauseBuilder);

        DefaultConditionBuilder builder = new DefaultConditionBuilder(name, jqlClauseBuilder);
        assertSame(jqlClauseBuilder, builder.notIn(dates));
    }

    @Test
    public void testNotInDatesCollection() throws Exception {
        final String name = "fieldName";
        final Date date1 = new Date(2482943L);
        final Date date2 = new Date(283722679588L);

        Collection<Date> values = CollectionBuilder.newBuilder(date1, date2).asCollection();

        when(jqlClauseBuilder.addDateCondition(name, Operator.NOT_IN, values)).thenReturn(jqlClauseBuilder);

        DefaultConditionBuilder builder = new DefaultConditionBuilder(name, jqlClauseBuilder);
        assertSame(jqlClauseBuilder, builder.notInDates(values));
    }

    @Test
    public void testNotInBuilder() throws Exception {
        final String name = "fieldName";
        final long value = 5L;

        when(jqlClauseBuilder.addNumberCondition(name, Operator.NOT_IN, new Long[]{value})).thenReturn(jqlClauseBuilder);

        DefaultConditionBuilder builder = new DefaultConditionBuilder(name, jqlClauseBuilder);
        final ValueBuilder valueBuilder = builder.notIn();
        assertNotNull(valueBuilder);
        assertSame(jqlClauseBuilder, valueBuilder.numbers(value));
    }

    @Test
    public void testNotInFunction() throws Exception {
        assertTestFunction("qqqq", Operator.NOT_IN, "qqqqqq", (builder, argument) -> builder.notInFunc(argument.getFuncName()));
    }

    @Test
    public void testNotInFunctionVarArgs() throws Exception {
        assertFunctionVarArgs("zxcvb", Operator.NOT_IN, "lkjhggf", (builder, argument) -> builder.notInFunc(argument.getFuncName(), argument.getArgument()));
    }

    @Test
    public void testNotInFunctionCollection() throws Exception {
        final Supplier<TestCallable<FunctionArgument<Collection<String>>, JqlClauseBuilder>> callableSupplier = () ->
                ((builder, argument) -> builder.notInFunc(argument.getFuncName(), argument.getArgument()));

        assertFunctionCollection("ccccc", Operator.NOT_IN, "ccccc", callableSupplier.get(), Collections.<String>emptyList());
        Collection<String> args = CollectionBuilder.newBuilder("arg1", "arg2", "...", "argN").asCollection();
        assertFunctionCollection("dqweqweqw", Operator.NOT_IN, "wqwqwq", callableSupplier.get(), args);
    }

    @Test
    public void testDateRange() throws Exception {
        final String fieldName = "testDateRange";
        final Date startDate = null;
        final Date endDate = new Date(4723842835492738L);

        when(jqlClauseBuilder.addDateRangeCondition(fieldName, startDate, endDate)).thenReturn(jqlClauseBuilder);

        DefaultConditionBuilder builder = new DefaultConditionBuilder(fieldName, jqlClauseBuilder);

        assertSame(jqlClauseBuilder, builder.range(startDate, endDate));
    }

    @Test
    public void testStringRange() throws Exception {
        final String fieldName = "testStringRange";
        final String startDate = "574835kfsdj";
        final String endDate = null;

        when(jqlClauseBuilder.addStringRangeCondition(fieldName, startDate, endDate)).thenReturn(jqlClauseBuilder);

        DefaultConditionBuilder builder = new DefaultConditionBuilder(fieldName, jqlClauseBuilder);

        assertSame(jqlClauseBuilder, builder.range(startDate, endDate));
    }

    @Test
    public void testLongRange() throws Exception {
        final String fieldName = "testLongRange";
        final Long start = 6L;
        final Long end = 773983L;

        when(jqlClauseBuilder.addNumberRangeCondition(fieldName, start, end)).thenReturn(jqlClauseBuilder);

        DefaultConditionBuilder builder = new DefaultConditionBuilder(fieldName, jqlClauseBuilder);

        assertSame(jqlClauseBuilder, builder.range(start, end));
    }

    @Test
    public void testOperandRange() throws Exception {
        final String fieldName = "testLongRange";
        final Operand start = Operands.valueOf("574835kfsdj");
        final Operand end = Operands.valueOf(6L);

        when(jqlClauseBuilder.addRangeCondition(fieldName, start, end)).thenReturn(jqlClauseBuilder);

        DefaultConditionBuilder builder = new DefaultConditionBuilder(fieldName, jqlClauseBuilder);

        assertSame(jqlClauseBuilder, builder.range(start, end));
    }

    private void assertTestFunction(String fieldName, Operator operator, String funcName,
                                    TestCallable<FunctionArgument<Void>, JqlClauseBuilder> functionCallable) {
        when(jqlClauseBuilder.addFunctionCondition(fieldName, operator, funcName)).thenReturn(jqlClauseBuilder);

        DefaultConditionBuilder builder = new DefaultConditionBuilder(fieldName, jqlClauseBuilder);

        assertSame(jqlClauseBuilder, functionCallable.call(builder, new FunctionArgument<>(funcName, null)));
    }

    private void assertFunctionVarArgs(String clauseName, Operator operator, String funcName,
                                       TestCallable<FunctionArgument<String[]>, JqlClauseBuilder> functionCallable, String... args) {
        when(jqlClauseBuilder.addFunctionCondition(clauseName, operator, funcName, args)).thenReturn(jqlClauseBuilder);

        DefaultConditionBuilder builder = new DefaultConditionBuilder(clauseName, jqlClauseBuilder);
        assertSame(jqlClauseBuilder, functionCallable.call(builder, new FunctionArgument<>(funcName, args)));
    }

    private void assertFunctionCollection(String clauseName, Operator operator, String funcName,
                                          TestCallable<FunctionArgument<Collection<String>>, JqlClauseBuilder> callable, Collection<String> args) {
        when(jqlClauseBuilder.addFunctionCondition(clauseName, operator, funcName, args)).thenReturn(jqlClauseBuilder);

        DefaultConditionBuilder builder = new DefaultConditionBuilder(clauseName, jqlClauseBuilder);
        assertSame(jqlClauseBuilder, callable.call(builder, new FunctionArgument<>(funcName, args)));
    }

    private void assertDate(String clauseName, Operator operator, TestCallable<Date, JqlClauseBuilder> call) {
        Date date = new Date();

        when(jqlClauseBuilder.addDateCondition(clauseName, operator, date)).thenReturn(jqlClauseBuilder);

        DefaultConditionBuilder builder = new DefaultConditionBuilder(clauseName, jqlClauseBuilder);

        assertSame(jqlClauseBuilder, call.call(builder, date));
    }

    private interface TestCallable<A, V> {
        V call(ConditionBuilder builder, A argument);
    }

    private static class FunctionArgument<A> {
        private final String funcName;
        private final A argument;

        private FunctionArgument(final String funcName, final A argument) {
            this.funcName = funcName;
            this.argument = argument;
        }

        public String getFuncName() {
            return funcName;
        }

        public A getArgument() {
            return argument;
        }
    }
}
