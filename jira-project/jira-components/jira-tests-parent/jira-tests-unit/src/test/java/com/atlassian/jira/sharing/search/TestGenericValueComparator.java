package com.atlassian.jira.sharing.search;

import com.atlassian.jira.mock.ofbiz.MockGenericValue;
import com.atlassian.jira.ofbiz.FieldMap;
import org.apache.commons.collections.ComparatorUtils;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.ofbiz.core.entity.GenericValue;

import java.util.Collections;
import java.util.Comparator;

import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.lessThan;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

/**
 * Test for {@link com.atlassian.jira.sharing.search.GenericValueComparator}.
 *
 * @since v3.13
 */
public class TestGenericValueComparator {
    private static final String ID = "id";
    private static final String OTHER = "other";
    @Rule
    public final MethodRule initMockito = MockitoJUnit.rule();

    @Mock
    private Comparator mockComparator;
    private GenericValue gv1 = new MockGenericValue("ShearchRequest", FieldMap.build(ID, new Long(50), OTHER, new Long(20)));
    private GenericValue gv2 = new MockGenericValue("ShearchRequest", FieldMap.build(ID, new Long(10), OTHER, new Long(100)));
    private GenericValue emptyGv = new MockGenericValue("ShearchRequest", Collections.emptyMap());

    private GenericValueComparator getGenericValueComparator(final String field) {
        return new GenericValueComparator(field, mockComparator);
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructorNullDelegator() {
        new GenericValueComparator("field", null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructorNullFieldWithComparator() {
        new GenericValueComparator(null, ComparatorUtils.NATURAL_COMPARATOR);

    }

    @Test(expected = IllegalArgumentException.class)
    public void constructorNullField() {
        new GenericValueComparator(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructorEmptyFieldWithComparator() {
        new GenericValueComparator("", ComparatorUtils.NATURAL_COMPARATOR);
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructorEmptyField() {
        new GenericValueComparator("");
    }

    @Test(expected = IllegalArgumentException.class)
    public void comparatorLeftNull() {
        final Comparator comparator = getGenericValueComparator(ID);
        comparator.compare(null, gv1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void comparatorRightNull() {
        final Comparator comparator = getGenericValueComparator(ID);
        comparator.compare(gv1, null);
    }

    @Test(expected = ClassCastException.class)
    public void comparatorIllegalLeft() {
        final Comparator comparator = getGenericValueComparator(ID);
        comparator.compare("balh", gv1);
    }

    @Test(expected = ClassCastException.class)
    public void comparatorIllegalRight() {
        final Comparator comparator = getGenericValueComparator(ID);
        comparator.compare(gv2, Collections.emptyList());
    }

    @Test
    public void comparatorDelegate() {
        when(mockComparator.compare(new Long(50), new Long(10))).thenReturn(1);

        final Comparator comparator = getGenericValueComparator(ID);

        assertThat(comparator.compare(gv1, gv2), is(1));
    }

    @Test
    public void comparatorDelegateOther() {
        when(mockComparator.compare(new Long(20), new Long(100))).thenReturn(1);

        final Comparator comparator = getGenericValueComparator(OTHER);

        assertThat(comparator.compare(gv1, gv2), is(1));
    }

    @Test
    public void comparatorDelegateLeftNull() {
        when(mockComparator.compare(null, new Long(20))).thenReturn(-1);

        final Comparator comparator = getGenericValueComparator(OTHER);

        assertThat(comparator.compare(emptyGv, gv1), is(-1));
    }

    @Test
    public void comparatorDelegateRightNull() {
        when(mockComparator.compare(new Long(10), null)).thenReturn(-1);

        final Comparator comparator = getGenericValueComparator(ID);

        assertThat(comparator.compare(gv2, emptyGv), is(-1));
    }

    @Test
    public void comparatorNaturalUsingOneField() {
        final Comparator comparator = new GenericValueComparator(ID);
        assertThat(comparator.compare(gv1, gv2), greaterThan(0));
        assertThat(comparator.compare(gv2, gv1), lessThan(0));
        assertThat(comparator.compare(gv1, gv1), is(0));
        assertThat(comparator.compare(gv2, gv2), is(0));
    }

    @Test
    public void comparatorNaturalUsingAnotherField() {
        final Comparator comparator = new GenericValueComparator(OTHER);
        assertThat(comparator.compare(gv1, gv2), lessThan(0));
        assertThat(comparator.compare(gv2, gv1), greaterThan(0));
        assertThat(comparator.compare(gv1, gv1), is(0));
        assertThat(comparator.compare(gv2, gv2), is(0));

        assertThat(comparator.compare(emptyGv, emptyGv), is(0));
    }

    @Test
    public void comparatorNaturalWithNulls() {
        Comparator comparator = new GenericValueComparator(ID);
        assertThat(comparator.compare(gv1, emptyGv), greaterThan(0));
        assertThat(comparator.compare(emptyGv, gv2), lessThan(0));

        assertThat(comparator.compare(emptyGv, emptyGv), is(0));
    }
}
