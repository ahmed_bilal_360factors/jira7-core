package com.atlassian.jira.web.task;

import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockComponentContainer;
import com.atlassian.jira.mock.servlet.MockHttpServletResponse;
import com.atlassian.jira.task.TaskManager;
import com.atlassian.jira.upgrade.UpgradeService;
import com.atlassian.scheduler.core.LifecycleAwareSchedulerService;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.isEmptyOrNullString;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TaskQuiescingServletTest {
    @Rule
    public MockComponentContainer container = new MockComponentContainer(this);
   
    @Mock
    @AvailableInContainer
    private FeatureManager featureManager;
    @Mock
    @AvailableInContainer
    private UpgradeService upgradeService;

    @Mock
    HttpServletRequest request;
    @Mock
    private TaskManager taskManager;
    @Mock
    private LifecycleAwareSchedulerService scheduler;
    
    private TaskQuiescingServlet tasksServlet;
    private HttpServletResponse response;
    private Writer responseOutput;

    @Before
    public void setUp() throws Exception {
        tasksServlet = new TaskQuiescingServlet();
        responseOutput = new StringWriter();

        when(request.getRequestURL()).thenReturn(new StringBuffer("http://127.127.127.127:8090/quiesce"));
        when(request.getRequestURI()).thenReturn("/quiesce");
        when(request.getParameter("action")).thenReturn("canceltasks");
        response = new MockHttpServletResponse(new PrintWriter(responseOutput));
    }

    @Test
    public void notAvailableInServer() throws Exception {
        tasksServlet.doGet(request, response);
        assertEquals("This servlet should only be accessible in cloud instances", 403, response.getStatus());
        assertThat(responseOutput.toString(), isEmptyOrNullString());
    }
}
