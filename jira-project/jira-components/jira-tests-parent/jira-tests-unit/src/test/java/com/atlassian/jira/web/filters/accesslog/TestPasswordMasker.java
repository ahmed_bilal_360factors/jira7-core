package com.atlassian.jira.web.filters.accesslog;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertThat;

public class TestPasswordMasker {
    @Test
    public void parametersThatContainTheStringPasswordAreMasked() {
        assertParameterIsMasked("someUrl", "password");
        assertParameterIsMasked("someUrl", "Password");
        assertParameterIsMasked("someUrl", "PASSWORD");
        assertParameterIsMasked("someUrl", "thePassword");
        assertParameterIsMasked("someUrl", "passwordValue");
        assertParameterIsMasked("someUrl", "thisIsAPasswordParameter");
    }

    @Test
    public void urlsThatContainTheStringPasswordAreMasked() {
        assertParameterIsMasked("http://password.jspa", "someParameter");
        assertParameterIsMasked("http://Password.jspa", "someParameter");
        assertParameterIsMasked("http://PASSWORD.jspa", "someParameter");
        assertParameterIsMasked("http://thePassword.jspa", "someParameter");
        assertParameterIsMasked("http://passwordUrl.jspa", "someParameter");
        assertParameterIsMasked("http://thisIsAPasswordUrl.jspa", "someParameter");
        assertParameterIsMasked("http://this-is-a-password-url.jspa", "someParameter");
    }

    /**
     * See JRA-43521.
     */
    @Test
    public void knownChangePasswordPagePasswordsAreMasked() {
        final String changePasswordPageUrl = "http://localhost:8090/jira/secure/ChangePassword.jspa";

        assertParameterIsMasked(changePasswordPageUrl, "current");
        assertParameterIsMasked(changePasswordPageUrl, "password");
        assertParameterIsMasked(changePasswordPageUrl, "confirm");
    }

    /**
     * See JRA-43521.
     */
    @Test
    public void knownWebSudoPagePasswordsAreMasked() {
        final String webSudoPageUrl = "http://localhost:8090/jira/secure/admin/WebSudoAuthenticate.jspa";

        assertParameterIsMasked(webSudoPageUrl, "webSudoPassword");
    }

    @Test
    public void nonPasswordParametersAreVisible() {
        assertParameterIsVisible("http://someUrl.jspa", "someParameter");
    }

    private void assertParameterIsMasked(final String requestUrl, final String parameterName) {
        final String parameterValue = "monkey";
        final String sanitisedParameterValue = new PasswordMasker(requestUrl).maskIfLikelyToBePassword(parameterName, parameterValue);
        assertThat(sanitisedParameterValue, is(not(parameterValue)));

    }

    private void assertParameterIsVisible(final String requestUrl, final String parameterName) {
        final String parameterValue = "monkey";
        final String sanitisedParameterValue = new PasswordMasker(requestUrl).maskIfLikelyToBePassword(parameterName, parameterValue);
        assertThat(sanitisedParameterValue, is(parameterValue));
    }
}
