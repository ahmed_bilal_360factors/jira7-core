package com.atlassian.jira.imports.project.core;

import com.atlassian.jira.plugin.PluginVersion;
import com.atlassian.jira.plugin.PluginVersionImpl;
import com.google.common.collect.Sets;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * @since v3.13
 */
public class TestBackupSystemInformationImpl {
    @Test
    public void testHappyPath() {
        final List<PluginVersion> pluginVersions = new ArrayList<PluginVersion>();
        Date versionDate = new Date(0L);
        final PluginVersionImpl abcPlugin = new PluginVersionImpl("abc-key", "abc-name", "abc-version", versionDate);
        final PluginVersionImpl defPlugin = new PluginVersionImpl("def-key", "def-name", "def-version", versionDate);
        pluginVersions.add(abcPlugin);
        pluginVersions.add(defPlugin);
        BackupSystemInformationImpl backupSystemInformation = new BackupSystemInformationImpl(
                "123", "Special", pluginVersions, true, Collections.emptyMap(), Sets.<String>newHashSet(), 0);
        assertEquals("123", backupSystemInformation.getBuildNumber());
        assertEquals("Special", backupSystemInformation.getEdition());
        assertTrue(backupSystemInformation.unassignedIssuesAllowed());

        // Now test pluginVersions
        assertEquals(2, backupSystemInformation.getPluginVersions().size());
        assertEquals(abcPlugin, backupSystemInformation.getPluginVersions().get(0));
        assertEquals(defPlugin, backupSystemInformation.getPluginVersions().get(1));
        // getPluginVersions() List should be unmodifiable
        try {
            backupSystemInformation.getPluginVersions().add(abcPlugin);
            fail("getPluginVersions() List should be unmodifiable");
        } catch (UnsupportedOperationException e) {
            // Expected.
        }
    }

    @Test
    public void testNullArguments() {
        try {
            new BackupSystemInformationImpl("123", "hello", null, true, Collections.emptyMap(), Sets.<String>newHashSet(), 0);
            fail("Should not construct with null Plugin versions");
        } catch (IllegalArgumentException e) {
            // expected
        }
    }

}
