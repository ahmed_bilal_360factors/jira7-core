package com.atlassian.jira.jql.values;

import com.atlassian.jira.action.issue.customfields.option.MockOption;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.context.JiraContextNode;
import com.atlassian.jira.issue.customfields.option.Option;
import com.atlassian.jira.issue.customfields.option.Options;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.config.FieldConfigScheme;
import com.atlassian.jira.issue.fields.config.manager.FieldConfigSchemeManager;
import com.atlassian.jira.issue.search.managers.SearchHandlerManager;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.collect.CollectionBuilder;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import static com.atlassian.jira.permission.ProjectPermissions.BROWSE_PROJECTS;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestCustomFieldOptionsClauseValuesGenerator {
    @Rule
    public MockitoRule mockito = MockitoJUnit.rule();
    @Mock
    private CustomFieldManager customFieldManager;
    @Mock
    private SearchHandlerManager searchHandlerManager;
    @Mock
    private FieldConfigSchemeManager fieldConfigSchemeManager;
    @Mock
    private PermissionManager permissionManager;
    @Mock
    JiraContextNode contextNode;

    private CustomFieldOptionsClauseValuesGenerator valuesGenerator;

    @Before
    public void setUp() throws Exception {
        this.valuesGenerator = new CustomFieldOptionsClauseValuesGenerator(customFieldManager, searchHandlerManager, fieldConfigSchemeManager, permissionManager) {
            @Override
            Locale getLocale(final ApplicationUser searcher) {
                return Locale.ENGLISH;
            }
        };
    }

    @Test
    public void testGetPossibleValuesNoFieldForClauseName() throws Exception {
        when(searchHandlerManager.getFieldIds(null, "clauseName")).thenReturn(Collections.emptyList());

        final ClauseValuesGenerator.Results possibleValues = valuesGenerator.getPossibleValues(null, "clauseName", "b", 10);

        assertEquals(0, possibleValues.getResults().size());
    }

    @Test
    public void testGetPossibleValuesNoCustomFieldForFieldName() throws Exception {
        when(searchHandlerManager.getFieldIds(null, "clauseName")).thenReturn(Collections.singletonList("fieldId"));
        when(permissionManager.getProjects(BROWSE_PROJECTS, null)).thenReturn(Collections.emptyList());
        when(customFieldManager.getCustomFieldObject("fieldId")).thenReturn(null);

        final ClauseValuesGenerator.Results possibleValues = valuesGenerator.getPossibleValues(null, "clauseName", "b", 10);

        assertEquals(0, possibleValues.getResults().size());
    }

    @Test
    public void testGetPossibleValuesNoContextForVisibleProjects() throws Exception {
        when(contextNode.getProjectObject()).thenReturn(new MockProject(2L, "ANA", "Another"));
        when(searchHandlerManager.getFieldIds(null, "clauseName")).thenReturn(Collections.singletonList("fieldId"));
        when(permissionManager.getProjects(BROWSE_PROJECTS, null)).thenReturn(Collections.singletonList(new MockProject(1L, "TST", "Test")));
        final CustomField customField = mock(CustomField.class);

        when(customFieldManager.getCustomFieldObject("fieldId")).thenReturn(customField);

        valuesGenerator = new CustomFieldOptionsClauseValuesGenerator(customFieldManager, searchHandlerManager, fieldConfigSchemeManager, permissionManager) {
            @Override
            Locale getLocale(final ApplicationUser searcher) {
                return Locale.ENGLISH;
            }

            @Override
            List<JiraContextNode> getContextsForCustomField(final CustomField customField) {
                return Collections.singletonList(contextNode);
            }
        };

        final ClauseValuesGenerator.Results possibleValues = valuesGenerator.getPossibleValues(null, "clauseName", "b", 10);

        assertEquals(0, possibleValues.getResults().size());
    }

    @Test
    public void testGetPossibleValuesHappyPathWithNoPrefix() throws Exception {
        final MockProject project = new MockProject(1L, "TST", "Test");
        when(contextNode.getProjectObject()).thenReturn(project);
        when(searchHandlerManager.getFieldIds(null, "clauseName")).thenReturn(Collections.singletonList("fieldId"));
        when(permissionManager.getProjects(BROWSE_PROJECTS, null)).thenReturn(Collections.singletonList(project));
        Option opt1 = new MockOption(null, null, 1L, "Opt 1", null, 1L);
        Option opt2 = new MockOption(null, null, 2L, "Opt 2", null, 2L);
        Option opt3 = new MockOption(null, null, 3L, "Not match", null, 3L);

        final Collection<Option> opts = CollectionBuilder.newBuilder(opt2, opt1, opt3).asCollection();
        final Options options = mock(Options.class);
        when(options.iterator()).thenReturn(opts.iterator());
        final CustomField customField = mock(CustomField.class);
        when(customField.getOptions(null, contextNode)).thenReturn(options);
        when(customFieldManager.getCustomFieldObject("fieldId")).thenReturn(customField);

        valuesGenerator = new CustomFieldOptionsClauseValuesGenerator(customFieldManager, searchHandlerManager, fieldConfigSchemeManager, permissionManager) {
            @Override
            Locale getLocale(final ApplicationUser searcher) {
                return Locale.ENGLISH;
            }

            @Override
            List<JiraContextNode> getContextsForCustomField(final CustomField customField) {
                return Collections.singletonList(contextNode);
            }
        };

        final ClauseValuesGenerator.Results possibleValues = valuesGenerator.getPossibleValues(null, "clauseName", "", 10);

        assertEquals(3, possibleValues.getResults().size());
        assertTrue(possibleValues.getResults().get(0).equals(new ClauseValuesGenerator.Result("Not match")));
        assertTrue(possibleValues.getResults().get(1).equals(new ClauseValuesGenerator.Result("Opt 1")));
        assertTrue(possibleValues.getResults().get(2).equals(new ClauseValuesGenerator.Result("Opt 2")));
    }

    @Test
    public void testGetPossibleValuesHappyPathWithNoPrefixHitsLimit() throws Exception {
        final MockProject project = new MockProject(1L, "TST", "Test");
        when(contextNode.getProjectObject()).thenReturn(project);
        when(searchHandlerManager.getFieldIds(null, "clauseName")).thenReturn(Collections.singletonList("fieldId"));
        when(permissionManager.getProjects(BROWSE_PROJECTS, null)).thenReturn(Collections.singletonList(project));
        Option opt1 = new MockOption(null, null, 1L, "Opt 1", null, 1L);
        Option opt2 = new MockOption(null, null, 2L, "Opt 2", null, 2L);
        Option opt3 = new MockOption(null, null, 3L, "Opt 3", null, 3L);
        Option opt4 = new MockOption(null, null, 4L, "Opt 4", null, 4L);
        Option opt5 = new MockOption(null, null, 5L, "Opt 5", null, 5L);

        final Collection<Option> opts = CollectionBuilder.newBuilder(opt2, opt1, opt3, opt4, opt5).asCollection();
        final Options options = mock(Options.class);
        when(options.iterator()).thenReturn(opts.iterator());
        final CustomField customField = mock(CustomField.class);
        when(customField.getOptions(null, contextNode)).thenReturn(options);
        when(customFieldManager.getCustomFieldObject("fieldId")).thenReturn(customField);

        valuesGenerator = new CustomFieldOptionsClauseValuesGenerator(customFieldManager, searchHandlerManager, fieldConfigSchemeManager, permissionManager) {
            @Override
            Locale getLocale(final ApplicationUser searcher) {
                return Locale.ENGLISH;
            }

            @Override
            List<JiraContextNode> getContextsForCustomField(final CustomField customField) {
                return Collections.singletonList(contextNode);
            }
        };

        final ClauseValuesGenerator.Results possibleValues = valuesGenerator.getPossibleValues(null, "clauseName", "", 4);

        assertEquals(4, possibleValues.getResults().size());
        assertTrue(possibleValues.getResults().get(0).equals(new ClauseValuesGenerator.Result("Opt 1")));
        assertTrue(possibleValues.getResults().get(1).equals(new ClauseValuesGenerator.Result("Opt 2")));
        assertTrue(possibleValues.getResults().get(2).equals(new ClauseValuesGenerator.Result("Opt 3")));
        assertTrue(possibleValues.getResults().get(3).equals(new ClauseValuesGenerator.Result("Opt 4")));
    }

    @Test
    public void testGetPossibleValuesHappyPathWithPrefix() throws Exception {
        final MockProject project = new MockProject(1L, "TST", "Test");
        when(contextNode.getProjectObject()).thenReturn(project);
        when(searchHandlerManager.getFieldIds(null, "clauseName")).thenReturn(Collections.singletonList("fieldId"));
        when(permissionManager.getProjects(BROWSE_PROJECTS, null)).thenReturn(Collections.singletonList(project));
        Option opt1 = new MockOption(null, null, 1L, "Opt 1", null, 1L);
        Option opt2 = new MockOption(null, null, 2L, "Opt 2", null, 2L);
        Option opt3 = new MockOption(null, null, 3L, "Not match", null, 3L);

        final Collection<Option> opts = CollectionBuilder.newBuilder(opt2, opt1, opt3).asCollection();
        final Options options = mock(Options.class);
        when(options.iterator()).thenReturn(opts.iterator());
        final CustomField customField = mock(CustomField.class);
        when(customField.getOptions(null, contextNode)).thenReturn(options);
        when(customFieldManager.getCustomFieldObject("fieldId")).thenReturn(customField);

        valuesGenerator = new CustomFieldOptionsClauseValuesGenerator(customFieldManager, searchHandlerManager, fieldConfigSchemeManager, permissionManager) {
            @Override
            Locale getLocale(final ApplicationUser searcher) {
                return Locale.ENGLISH;
            }

            @Override
            List<JiraContextNode> getContextsForCustomField(final CustomField customField) {
                return Collections.singletonList(contextNode);
            }
        };

        final ClauseValuesGenerator.Results possibleValues = valuesGenerator.getPossibleValues(null, "clauseName", "o", 10);

        assertEquals(2, possibleValues.getResults().size());
        assertTrue(possibleValues.getResults().get(0).equals(new ClauseValuesGenerator.Result("Opt 1")));
        assertTrue(possibleValues.getResults().get(1).equals(new ClauseValuesGenerator.Result("Opt 2")));
    }

    @Test
    public void testGetPossibleValuesHappyPathWithPrefixRootContext() throws Exception {
        final MockProject project = new MockProject(1L, "TST", "Test");
        when(contextNode.getProjectObject()).thenReturn(null);
        when(searchHandlerManager.getFieldIds(null, "clauseName")).thenReturn(Collections.singletonList("fieldId"));
        when(permissionManager.getProjects(BROWSE_PROJECTS, null)).thenReturn(Collections.singletonList(project));
        Option opt1 = new MockOption(null, null, 1L, "Opt 1", null, 1L);
        Option opt2 = new MockOption(null, null, 2L, "Opt 2", null, 2L);
        Option opt3 = new MockOption(null, null, 3L, "Not match", null, 3L);

        final Collection<Option> opts = CollectionBuilder.newBuilder(opt2, opt1, opt3).asCollection();
        final Options options = mock(Options.class);
        when(options.iterator()).thenReturn(opts.iterator());
        final CustomField customField = mock(CustomField.class);
        when(customField.getOptions(null, contextNode)).thenReturn(options);
        when(customFieldManager.getCustomFieldObject("fieldId")).thenReturn(customField);

        valuesGenerator = new CustomFieldOptionsClauseValuesGenerator(customFieldManager, searchHandlerManager, fieldConfigSchemeManager, permissionManager) {
            @Override
            Locale getLocale(final ApplicationUser searcher) {
                return Locale.ENGLISH;
            }

            @Override
            List<JiraContextNode> getContextsForCustomField(final CustomField customField) {
                return Collections.singletonList(contextNode);
            }
        };

        final ClauseValuesGenerator.Results possibleValues = valuesGenerator.getPossibleValues(null, "clauseName", "o", 10);

        assertEquals(2, possibleValues.getResults().size());
        assertTrue(possibleValues.getResults().get(0).equals(new ClauseValuesGenerator.Result("Opt 1")));
        assertTrue(possibleValues.getResults().get(1).equals(new ClauseValuesGenerator.Result("Opt 2")));
    }

    @Test
    public void testGetContextsForCustomFieldOnlyRootContext() throws Exception {
        final CustomField customField = mock(CustomField.class);

        when(fieldConfigSchemeManager.getConfigSchemesForField(customField)).thenReturn(Collections.emptyList());

        valuesGenerator = new CustomFieldOptionsClauseValuesGenerator(customFieldManager, searchHandlerManager, fieldConfigSchemeManager, permissionManager) {
            @Override
            JiraContextNode getRootContext() {
                return contextNode;
            }
        };

        final List<JiraContextNode> contexts = valuesGenerator.getContextsForCustomField(customField);
        assertEquals(1, contexts.size());
        assertTrue(contexts.contains(contextNode));
    }

    @Test
    public void testGetContextsForCustomFieldOtherContexts() throws Exception {
        final FieldConfigScheme scheme = mock(FieldConfigScheme.class);
        when(scheme.getContexts()).thenReturn(Collections.singletonList(contextNode));
        final CustomField customField = mock(CustomField.class);

        when(fieldConfigSchemeManager.getConfigSchemesForField(customField)).thenReturn(Collections.singletonList(scheme));

        final List<JiraContextNode> contexts = valuesGenerator.getContextsForCustomField(customField);
        assertEquals(1, contexts.size());
        assertTrue(contexts.contains(contextNode));

    }

    @Test
    public void testGetPossibleValuesMatchFullValue() throws Exception {
        final MockProject project = new MockProject(1L, "TST", "Test");
        when(contextNode.getProjectObject()).thenReturn(project);
        when(searchHandlerManager.getFieldIds(null, "clauseName")).thenReturn(Collections.singletonList("fieldId"));
        when(permissionManager.getProjects(BROWSE_PROJECTS, null)).thenReturn(Collections.singletonList(project));
        Option opt1 = new MockOption(null, null, 1L, "Opt 1", null, 1L);
        Option opt2 = new MockOption(null, null, 2L, "Opt 2", null, 2L);
        Option opt3 = new MockOption(null, null, 3L, "Not match", null, 3L);

        final Collection<Option> opts = CollectionBuilder.newBuilder(opt2, opt1, opt3).asCollection();
        final Options options = mock(Options.class);
        when(options.iterator()).thenReturn(opts.iterator());
        final CustomField customField = mock(CustomField.class);
        when(customField.getOptions(null, contextNode)).thenReturn(options);
        when(customFieldManager.getCustomFieldObject("fieldId")).thenReturn(customField);

        valuesGenerator = new CustomFieldOptionsClauseValuesGenerator(customFieldManager, searchHandlerManager, fieldConfigSchemeManager, permissionManager) {
            @Override
            Locale getLocale(final ApplicationUser searcher) {
                return Locale.ENGLISH;
            }

            @Override
            List<JiraContextNode> getContextsForCustomField(final CustomField customField) {
                return Collections.singletonList(contextNode);
            }
        };

        final ClauseValuesGenerator.Results possibleValues = valuesGenerator.getPossibleValues(null, "clauseName", "opt 1", 10);

        assertEquals(1, possibleValues.getResults().size());
        assertTrue(possibleValues.getResults().get(0).equals(new ClauseValuesGenerator.Result("Opt 1")));
    }

    @Test
    public void testGetPossibleValuesExactMatchWithOthers() throws Exception {
        final MockProject project = new MockProject(1L, "TST", "Test");
        when(contextNode.getProjectObject()).thenReturn(project);
        when(searchHandlerManager.getFieldIds(null, "clauseName")).thenReturn(Collections.singletonList("fieldId"));
        when(permissionManager.getProjects(BROWSE_PROJECTS, null)).thenReturn(Collections.singletonList(project));
        Option opt1 = new MockOption(null, null, 1L, "Opt 1", null, 1L);
        Option opt2 = new MockOption(null, null, 2L, "Opt 2", null, 2L);
        Option opt3 = new MockOption(null, null, 3L, "Opt 1.0", null, 3L);

        final Collection<Option> opts = CollectionBuilder.newBuilder(opt2, opt1, opt3).asCollection();
        final Options options = mock(Options.class);
        when(options.iterator()).thenReturn(opts.iterator());
        final CustomField customField = mock(CustomField.class);
        when(customField.getOptions(null, contextNode)).thenReturn(options);
        when(customFieldManager.getCustomFieldObject("fieldId")).thenReturn(customField);

        valuesGenerator = new CustomFieldOptionsClauseValuesGenerator(customFieldManager, searchHandlerManager, fieldConfigSchemeManager, permissionManager) {
            @Override
            Locale getLocale(final ApplicationUser searcher) {
                return Locale.ENGLISH;
            }

            @Override
            List<JiraContextNode> getContextsForCustomField(final CustomField customField) {
                return Collections.singletonList(contextNode);
            }
        };

        final ClauseValuesGenerator.Results possibleValues = valuesGenerator.getPossibleValues(null, "clauseName", "opt 1", 10);

        assertEquals(2, possibleValues.getResults().size());
        assertTrue(possibleValues.getResults().get(0).equals(new ClauseValuesGenerator.Result("Opt 1")));
        assertTrue(possibleValues.getResults().get(1).equals(new ClauseValuesGenerator.Result("Opt 1.0")));
    }

}
