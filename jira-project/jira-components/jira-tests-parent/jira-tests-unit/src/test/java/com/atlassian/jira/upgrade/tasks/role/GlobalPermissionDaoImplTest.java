package com.atlassian.jira.upgrade.tasks.role;

import com.atlassian.jira.mock.ofbiz.MockOfBizDelegator;
import com.atlassian.jira.ofbiz.FieldMap;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import static com.atlassian.jira.upgrade.tasks.role.TestUtils.asGroups;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class GlobalPermissionDaoImplTest {
    private MockOfBizDelegator delegator;
    private GlobalPermissionDaoImpl permissionDao;

    @Before
    public void setup() {
        delegator = Mockito.spy(new MockOfBizDelegator());
        permissionDao = new GlobalPermissionDaoImpl(delegator);
    }

    @Test
    public void getUseReturnsAllNonEmptyGroups() {
        addUseGlobalPermission("one", "two", null, "     ", "");
        addAdminGlobalPermission("three", "four");

        assertThat(permissionDao.groupsWithUsePermission(), containsInAnyOrder(
                asGroups("one", "two", "     ", "").toArray()));
    }

    @Test
    public void useGroupsAreCached() {
        permissionDao.groupsWithUsePermission();
        permissionDao.groupsWithUsePermission();

        verify(delegator, times(1)).findByAnd("GlobalPermissionEntry", ImmutableMap.of("permission", "USE"));
    }

    @Test
    public void adminGroupsAreCached() {
        permissionDao.groupsWithAdminPermission();
        permissionDao.groupsWithAdminPermission();

        verify(delegator, times(1)).findByOr(eq("GlobalPermissionEntry"), anyObject(), eq(ImmutableList.of("permission")));
    }

    @Test
    public void sdGroupsAreCached() {

        permissionDao.groupsWithSdAgentPermission();
        permissionDao.groupsWithSdAgentPermission();

        verify(delegator, times(1)).findByAnd("GlobalPermissionEntry", ImmutableMap.of("permission",
                "com.atlassian.servicedesk.agent.access"));
    }

    @Test
    public void getAdminsReturnsAllNonEmptyGroups() {
        addUseGlobalPermission("one", "two", null, "     ", "");
        addAdminGlobalPermission("three", "four", "", null, "     ");
        addSysAdminGlobalPermission("five", "six", "", null, "  ");

        assertThat(permissionDao.groupsWithAdminPermission(),
                containsInAnyOrder(asGroups("three", "four", "five", "six", "     ", "  ", "").toArray()));
    }

    @Test
    public void groupsWithSdAgentReturnsAllGroupsWithServiceDeskAgentPermission() {
        addSdAgentGlobalPermission("one", "two", "three", null);
        addAdminGlobalPermission("three", "four");
        addUseGlobalPermission("two");

        assertThat(permissionDao.groupsWithSdAgentPermission(),
                containsInAnyOrder(asGroups("one", "two", "three").toArray()));
    }

    private void addUseGlobalPermission(String... groups) {
        addGlobalPermission("USE", groups);
    }

    private void addAdminGlobalPermission(String... groups) {
        addGlobalPermission("ADMINISTER", groups);
    }

    private void addSysAdminGlobalPermission(String... groups) {
        addGlobalPermission("SYSTEM_ADMIN", groups);
    }

    private void addSdAgentGlobalPermission(String... groups) {
        addGlobalPermission("com.atlassian.servicedesk.agent.access", groups);
    }

    private void addGlobalPermission(String permission, String... groups) {
        for (String group : groups) {
            delegator.createValue("GlobalPermissionEntry", FieldMap.build("permission", permission, "group_id", group));
        }
    }
}