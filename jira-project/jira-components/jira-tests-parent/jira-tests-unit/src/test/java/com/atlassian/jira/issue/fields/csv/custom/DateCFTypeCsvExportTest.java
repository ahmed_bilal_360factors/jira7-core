package com.atlassian.jira.issue.fields.csv.custom;

import com.atlassian.jira.issue.customfields.impl.AbstractSingleFieldType;
import com.atlassian.jira.issue.customfields.impl.DateCFType;
import org.junit.Test;

import java.util.Date;

public class DateCFTypeCsvExportTest extends CustomFieldSingleValueCsvExporterTest<Date> {
    @Override
    protected AbstractSingleFieldType<Date> createField() {
        return new DateCFType(null, null, null, null, null, null, csvDateFormatter);
    }

    @Test
    public void dateIsFormattedWithCsvDateFormatter() {
        whenFieldValueIs(now.toDate());
        assertExportedValue(csvDateFormatter.formatDate(now.toDate()));
    }
}
