package com.atlassian.jira.plugin.issueview;

import com.atlassian.core.util.map.EasyMap;
import com.atlassian.jira.issue.IssueFieldConstants;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.Field;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.fields.MockCustomField;
import com.atlassian.jira.issue.fields.MockOrderableField;
import com.atlassian.jira.issue.fields.OrderableField;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestIssueViewRequestParamsHelper {
    @Rule
    public MethodRule initMockito = MockitoJUnit.rule();

    @Mock
    private FieldManager mockFieldManager;

    @Test
    public void testNoParamsDefined() {
        when(mockFieldManager.getOrderableFields()).thenReturn(getOrderableFieldIds());

        IssueViewRequestParamsHelper issueViewRequestParamsHelper = new IssueViewRequestParamsHelperImpl((FieldManager) mockFieldManager);
        IssueViewFieldParams issueViewFieldParams = issueViewRequestParamsHelper.getIssueViewFieldParams(EasyMap.build());

        assertFalse(issueViewFieldParams.isCustomViewRequested());
        assertFalse(issueViewFieldParams.isAnyFieldDefined());
        assertFalse(issueViewFieldParams.isAllCustomFields());
        assertTrue(issueViewFieldParams.getFieldIds().isEmpty());
        assertTrue(issueViewFieldParams.getCustomFieldIds().isEmpty());
        assertFalse(issueViewFieldParams.getOrderableFieldIds().isEmpty());
    }

    @Test
    public void testNullFieldParamsDefined() {
        when(mockFieldManager.getOrderableFields()).thenReturn(getOrderableFieldIds());

        IssueViewRequestParamsHelper issueViewRequestParamsHelper = new IssueViewRequestParamsHelperImpl((FieldManager) mockFieldManager);
        IssueViewFieldParams issueViewFieldParams = issueViewRequestParamsHelper.getIssueViewFieldParams(EasyMap.build("field", null));

        assertTrue(issueViewFieldParams.isCustomViewRequested());
        assertFalse(issueViewFieldParams.isAnyFieldDefined());
        assertFalse(issueViewFieldParams.isAllCustomFields());
        assertTrue(issueViewFieldParams.getFieldIds().isEmpty());
        assertTrue(issueViewFieldParams.getCustomFieldIds().isEmpty());
        assertFalse(issueViewFieldParams.getOrderableFieldIds().isEmpty());
    }

    @Test
    public void testEmptyFieldParamsDefined() {
        when(mockFieldManager.getOrderableFields()).thenReturn(getOrderableFieldIds());

        IssueViewRequestParamsHelper issueViewRequestParamsHelper = new IssueViewRequestParamsHelperImpl((FieldManager) mockFieldManager);
        IssueViewFieldParams issueViewFieldParams = issueViewRequestParamsHelper.getIssueViewFieldParams(EasyMap.build("field", new String[0]));

        assertTrue(issueViewFieldParams.isCustomViewRequested());
        assertFalse(issueViewFieldParams.isAnyFieldDefined());
        assertFalse(issueViewFieldParams.isAllCustomFields());
        assertTrue(issueViewFieldParams.getFieldIds().isEmpty());
        assertTrue(issueViewFieldParams.getCustomFieldIds().isEmpty());
        assertFalse(issueViewFieldParams.getOrderableFieldIds().isEmpty());
    }

    @Test
    public void testProperFieldParamsDefined() {
        when(mockFieldManager.getOrderableFields()).thenReturn(getOrderableFieldIds());
        when(mockFieldManager.getField(eq("name1"))).thenReturn(createField("name1", null, null));
        when(mockFieldManager.getField(eq("name3"))).thenReturn(createField("name3", null, null));

        IssueViewRequestParamsHelper issueViewRequestParamsHelper = new IssueViewRequestParamsHelperImpl(mockFieldManager);
        IssueViewFieldParams issueViewFieldParams = issueViewRequestParamsHelper.getIssueViewFieldParams(EasyMap.build("field", new String[]{"name1", "name3"}));

        assertTrue(issueViewFieldParams.isCustomViewRequested());
        assertTrue(issueViewFieldParams.isAnyFieldDefined());
        assertFalse(issueViewFieldParams.isAllCustomFields());
        assertFalse(issueViewFieldParams.getFieldIds().isEmpty());
        assertTrue(issueViewFieldParams.getCustomFieldIds().isEmpty());
        assertFalse(issueViewFieldParams.getOrderableFieldIds().isEmpty());
    }

    @Test
    public void testInvalidFieldsParamsDefined() {
        when(mockFieldManager.getOrderableFields()).thenReturn(getOrderableFieldIds());
        when(mockFieldManager.getField(eq("name5"))).thenReturn(null);
        when(mockFieldManager.getField(eq("name6"))).thenReturn(null);

        IssueViewRequestParamsHelper issueViewRequestParamsHelper = new IssueViewRequestParamsHelperImpl(mockFieldManager);
        IssueViewFieldParams issueViewFieldParams = issueViewRequestParamsHelper.getIssueViewFieldParams(EasyMap.build("field", new String[]{"name5", "name6"}));

        assertTrue(issueViewFieldParams.isCustomViewRequested());
        assertFalse(issueViewFieldParams.isAnyFieldDefined());
        assertFalse(issueViewFieldParams.isAllCustomFields());
        assertTrue(issueViewFieldParams.getFieldIds().isEmpty());
        assertTrue(issueViewFieldParams.getCustomFieldIds().isEmpty());
        assertFalse(issueViewFieldParams.getOrderableFieldIds().isEmpty());
    }

    @Test
    public void testValidAndInvalidFieldParamsDefined() {
        when(mockFieldManager.getOrderableFields()).thenReturn(getOrderableFieldIds());
        when(mockFieldManager.getField(eq("name1"))).thenReturn(createField("name1", null, null));
        when(mockFieldManager.getField(eq("name5"))).thenReturn(null);

        IssueViewRequestParamsHelper issueViewRequestParamsHelper = new IssueViewRequestParamsHelperImpl(mockFieldManager);
        IssueViewFieldParams issueViewFieldParams = issueViewRequestParamsHelper.getIssueViewFieldParams(EasyMap.build("field", new String[]{"name1", "name5"}));

        assertTrue(issueViewFieldParams.isCustomViewRequested());
        assertTrue(issueViewFieldParams.isAnyFieldDefined());
        assertFalse(issueViewFieldParams.isAllCustomFields());
        assertFalse(issueViewFieldParams.getFieldIds().isEmpty());
        assertTrue(issueViewFieldParams.getCustomFieldIds().isEmpty());
        assertFalse(issueViewFieldParams.getOrderableFieldIds().isEmpty());
    }

    @Test
    public void testCustomFieldParamsDefined() {
        when(mockFieldManager.getOrderableFields()).thenReturn(getOrderableFieldIds());
        when(mockFieldManager.getCustomField(eq("customfield_10000"))).thenReturn(createCustomField("customfield_10000"));
        when(mockFieldManager.getCustomField(eq("customfield_10001"))).thenReturn(createCustomField("customfield_10001"));

        IssueViewRequestParamsHelper issueViewRequestParamsHelper = new IssueViewRequestParamsHelperImpl(mockFieldManager);
        IssueViewFieldParams issueViewFieldParams = issueViewRequestParamsHelper.getIssueViewFieldParams(EasyMap.build("field", new String[]{"customfield_10000", "customfield_10001"}));

        assertTrue(issueViewFieldParams.isCustomViewRequested());
        assertTrue(issueViewFieldParams.isAnyFieldDefined());
        assertFalse(issueViewFieldParams.isAllCustomFields());
        assertTrue(issueViewFieldParams.getFieldIds().isEmpty());
        assertFalse(issueViewFieldParams.getCustomFieldIds().isEmpty());
        assertFalse(issueViewFieldParams.getOrderableFieldIds().isEmpty());
    }

    @Test
    public void testAllCustomFieldParamsDefined() {
        when(mockFieldManager.getOrderableFields()).thenReturn(getOrderableFieldIds());

        IssueViewRequestParamsHelper issueViewRequestParamsHelper = new IssueViewRequestParamsHelperImpl(mockFieldManager);
        IssueViewFieldParams issueViewFieldParams = issueViewRequestParamsHelper.getIssueViewFieldParams(EasyMap.build("field", new String[]{"allcustom"}));

        assertTrue(issueViewFieldParams.isCustomViewRequested());
        assertTrue(issueViewFieldParams.isAnyFieldDefined());
        assertTrue(issueViewFieldParams.isAllCustomFields());
        assertTrue(issueViewFieldParams.getFieldIds().isEmpty());
        assertTrue(issueViewFieldParams.getCustomFieldIds().isEmpty());
        assertFalse(issueViewFieldParams.getOrderableFieldIds().isEmpty());
    }

    @SuppressWarnings({"ThrowableInstanceNeverThrown"})
    @Test
    public void testInvalidCustomFieldParamsDefined() {
        when(mockFieldManager.getOrderableFields()).thenReturn(getOrderableFieldIds());
        when(mockFieldManager.getCustomField(eq("customfield_10000"))).thenThrow(new IllegalArgumentException());

        IssueViewRequestParamsHelper issueViewRequestParamsHelper = new IssueViewRequestParamsHelperImpl(mockFieldManager);
        IssueViewFieldParams issueViewFieldParams = issueViewRequestParamsHelper.getIssueViewFieldParams(EasyMap.build("field", new String[]{"customfield_10000"}));

        assertTrue(issueViewFieldParams.isCustomViewRequested());
        assertFalse(issueViewFieldParams.isAnyFieldDefined());
        assertFalse(issueViewFieldParams.isAllCustomFields());
        assertTrue(issueViewFieldParams.getFieldIds().isEmpty());
        assertTrue(issueViewFieldParams.getCustomFieldIds().isEmpty());
        assertFalse(issueViewFieldParams.getOrderableFieldIds().isEmpty());
    }


    @Test
    public void testRegualAndCustomFieldParamsDefined() {
        when(mockFieldManager.getOrderableFields()).thenReturn(getOrderableFieldIds());
        when(mockFieldManager.getField(eq("name1"))).thenReturn(createField("name1", null, null));
        when(mockFieldManager.getCustomField(eq("customfield_10001"))).thenReturn(createCustomField("customfield_10001"));

        IssueViewRequestParamsHelper issueViewRequestParamsHelper = new IssueViewRequestParamsHelperImpl(mockFieldManager);
        IssueViewFieldParams issueViewFieldParams = issueViewRequestParamsHelper.getIssueViewFieldParams(EasyMap.build("field", new String[]{"name1", "customfield_10001"}));

        assertTrue(issueViewFieldParams.isCustomViewRequested());
        assertTrue(issueViewFieldParams.isAnyFieldDefined());
        assertFalse(issueViewFieldParams.isAllCustomFields());
        assertFalse(issueViewFieldParams.getFieldIds().isEmpty());
        assertFalse(issueViewFieldParams.getCustomFieldIds().isEmpty());
        assertFalse(issueViewFieldParams.getOrderableFieldIds().isEmpty());
    }

    @Test
    public void testRegualAndNonIssueAndAllCustomFieldParamsDefined() {
        when(mockFieldManager.getOrderableFields()).thenReturn(getOrderableFieldIds());
        when(mockFieldManager.getField(eq("name1"))).thenReturn(createField("name1", null, null));
        when(mockFieldManager.getField(eq("link"))).thenReturn(null);

        IssueViewRequestParamsHelper issueViewRequestParamsHelper = new IssueViewRequestParamsHelperImpl(mockFieldManager);
        IssueViewFieldParams issueViewFieldParams = issueViewRequestParamsHelper.getIssueViewFieldParams(EasyMap.build("field", new String[]{"name1", "link", "allcustom"}));

        assertTrue(issueViewFieldParams.isCustomViewRequested());
        assertTrue(issueViewFieldParams.isAnyFieldDefined());
        assertTrue(issueViewFieldParams.isAllCustomFields());
        assertFalse(issueViewFieldParams.getFieldIds().isEmpty());
        assertTrue(issueViewFieldParams.getCustomFieldIds().isEmpty());
        assertFalse(issueViewFieldParams.getOrderableFieldIds().isEmpty());
    }

    @Test
    public void testRegualMappedFieldParamsDefined() {
        when(mockFieldManager.getOrderableFields()).thenReturn(getOrderableFieldIds());
        when(mockFieldManager.getField(eq("project"))).thenReturn(createField("project", null, null));

        IssueViewRequestParamsHelper issueViewRequestParamsHelper = new IssueViewRequestParamsHelperImpl(mockFieldManager);
        IssueViewFieldParams issueViewFieldParams = issueViewRequestParamsHelper.getIssueViewFieldParams(EasyMap.build("field", new String[]{"pid"}));

        assertTrue(issueViewFieldParams.isCustomViewRequested());
        assertTrue(issueViewFieldParams.isAnyFieldDefined());
        assertFalse(issueViewFieldParams.isAllCustomFields());
        assertFalse(issueViewFieldParams.getFieldIds().isEmpty());
        assertTrue(issueViewFieldParams.getCustomFieldIds().isEmpty());
        assertFalse(issueViewFieldParams.getOrderableFieldIds().isEmpty());
    }

    @Test
    public void testTimetrackingFieldParamsDefined() {
        when(mockFieldManager.getOrderableFields()).thenReturn(getOrderableFieldIds());
        when(mockFieldManager.getField(eq(IssueFieldConstants.TIME_SPENT))).thenReturn(createField(IssueFieldConstants.TIME_SPENT, null, null));
        when(mockFieldManager.getField(eq(IssueFieldConstants.TIME_ESTIMATE))).thenReturn(createField(IssueFieldConstants.TIME_ESTIMATE, null, null));
        when(mockFieldManager.getField(eq(IssueFieldConstants.TIME_ORIGINAL_ESTIMATE))).thenReturn(createField(IssueFieldConstants.TIME_ORIGINAL_ESTIMATE, null, null));
        when(mockFieldManager.getField(eq(IssueFieldConstants.AGGREGATE_TIME_SPENT))).thenReturn(createField(IssueFieldConstants.AGGREGATE_TIME_SPENT, null, null));
        when(mockFieldManager.getField(eq(IssueFieldConstants.AGGREGATE_TIME_ESTIMATE))).thenReturn(createField(IssueFieldConstants.AGGREGATE_TIME_ESTIMATE, null, null));
        when(mockFieldManager.getField(eq(IssueFieldConstants.AGGREGATE_TIME_ORIGINAL_ESTIMATE))).thenReturn(createField(IssueFieldConstants.AGGREGATE_TIME_ORIGINAL_ESTIMATE, null, null));
        when(mockFieldManager.getField(eq("link"))).thenReturn(null);

        IssueViewRequestParamsHelper issueViewRequestParamsHelper = new IssueViewRequestParamsHelperImpl(mockFieldManager);
        IssueViewFieldParams issueViewFieldParams = issueViewRequestParamsHelper.getIssueViewFieldParams(EasyMap.build("field", new String[]{IssueFieldConstants.TIMETRACKING}));

        assertTrue(issueViewFieldParams.isCustomViewRequested());
        assertTrue(issueViewFieldParams.isAnyFieldDefined());
        assertFalse(issueViewFieldParams.isAllCustomFields());
        assertFalse(issueViewFieldParams.getFieldIds().isEmpty());
        assertTrue(issueViewFieldParams.getCustomFieldIds().isEmpty());
        assertFalse(issueViewFieldParams.getOrderableFieldIds().isEmpty());
    }

    @Test
    public void testTimespentFieldParamsDefined() {
        when(mockFieldManager.getOrderableFields()).thenReturn(getOrderableFieldIds());
        when(mockFieldManager.getField(eq(IssueFieldConstants.TIME_SPENT))).thenReturn(createField(IssueFieldConstants.TIME_SPENT, null, null));

        IssueViewRequestParamsHelper issueViewRequestParamsHelper = new IssueViewRequestParamsHelperImpl(mockFieldManager);
        IssueViewFieldParams issueViewFieldParams = issueViewRequestParamsHelper.getIssueViewFieldParams(EasyMap.build("field", new String[]{IssueFieldConstants.TIME_SPENT}));

        assertTrue(issueViewFieldParams.isCustomViewRequested());
        assertTrue(issueViewFieldParams.isAnyFieldDefined());
        assertFalse(issueViewFieldParams.isAllCustomFields());
        assertFalse(issueViewFieldParams.getFieldIds().isEmpty());
        assertTrue(issueViewFieldParams.getCustomFieldIds().isEmpty());
        assertFalse(issueViewFieldParams.getOrderableFieldIds().isEmpty());
    }

    private Set<OrderableField> getOrderableFieldIds() {
        Set<OrderableField> fields = new HashSet<OrderableField>();
        fields.add(createOrderableField("name1", null, null));
        fields.add(createOrderableField("name2", null, null));
        fields.add(createOrderableField("name3", null, null));
        fields.add(createOrderableField("name4", null, null));
        return fields;
    }

    private OrderableField createOrderableField(final String id, final String name, final String nameKey) {
        final MockOrderableField mockOrderableField = new MockOrderableField(id, name);
        return mockOrderableField;
    }

    private Field createField(final String id, final String name, final String nameKey) {
        return new Field() {

            public String getId() {
                return id;
            }

            public String getNameKey() {
                return nameKey;
            }

            public String getName() {
                return name;
            }

            public int compareTo(final Object o) {
                return 0;
            }
        };
    }

    private CustomField createCustomField(final String id) {
        final MockCustomField customField = new MockCustomField();
        customField.setId(id);
        return customField;
    }
}
