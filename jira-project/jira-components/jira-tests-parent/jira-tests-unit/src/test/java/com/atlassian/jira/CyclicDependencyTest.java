package com.atlassian.jira;

import com.atlassian.jira.config.component.AppPropertiesComponentAdaptor;
import com.atlassian.plugin.osgi.hostcomponents.HostComponentProvider;
import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.HashMultiset;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.ImmutableSetMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Multiset;
import com.google.common.collect.SetMultimap;
import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.osgi.framework.ServiceFactory;
import org.picocontainer.ComponentAdapter;
import org.picocontainer.MutablePicoContainer;
import org.picocontainer.Parameter;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import java.lang.reflect.Constructor;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.Stack;

import static com.atlassian.jira.CyclicDependencyTest.GraphCycle.findCommonComponents;
import static com.google.common.collect.ImmutableList.copyOf;
import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Lists.newArrayListWithCapacity;
import static com.google.common.collect.Multisets.copyHighestCountFirst;
import static java.lang.String.format;
import static java.lang.reflect.Modifier.isPublic;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.arrayContaining;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.RETURNS_MOCKS;
import static org.mockito.Mockito.mock;

/**
 * Tests for cyclic dependencies in the various {@link ContainerRegistrar}s.
 * <p>
 * Also includes self-tests, as the cyclic dep testing is not trivial.
 * </p>
 * <p>
 * Note: Cycle-finding is optimised to be fast, not complete. The implementation used in
 * {@link GraphVisitor} will always find cycles if they exist, but will not report variants
 * of the "core" cycles it finds. This was a pragmatic decision to keep the test fast (<3sec),
 * rather than the ~45sec taken by using "complete" cycle-finding algorithms (Tarjan, Johnson).
 * See {@link com.atlassian.jira.CyclicDependencyTest.GraphVisitor} preamble for more info.
 * </p>
 *
 * @since 7.0
 */
@RunWith(MockitoJUnitRunner.class)
public class CyclicDependencyTest {
    @Test
    public void checkForCyclicDependenciesInMainContainer() {
        ContainerRegistrar containerRegistrar = new ContainerRegistrar() {
            @Override
            void registerCacheManager(ComponentContainer register) {
                // NOOP for testing. this is OK because registerCacheManager registers only implementations,
                // not implementation _classes_ & the alternative would necessitate static method mocking,
                // which requires an incompatible version of powermockito. this is the lesser of the 2 evils.
            }
        };

        ProxyComponentContainer fakeContainer = new ProxyComponentContainer();

        containerRegistrar.registerComponents(fakeContainer, true);
        assertThat(fakeContainer.getCyclicDependencies(), hasNoCycles());

        containerRegistrar.registerComponents(fakeContainer, false);
        assertThat(fakeContainer.getCyclicDependencies(), hasNoCycles());
    }

    @Test
    public void checkForCyclicDependenciesInBootstrapContainer() {
        BootstrapContainerRegistrar containerRegistrar = new BootstrapContainerRegistrar();
        ProxyComponentContainer fakeContainer = new ProxyComponentContainer();
        containerRegistrar.registerComponents(fakeContainer);

        assertThat(fakeContainer.getCyclicDependencies(), hasNoCycles());
    }

    @Test
    public void checkForCyclicDependenciesInSetupContainer() {
        SetupContainerRegistrar containerRegistrar = new SetupContainerRegistrar();
        ProxyComponentContainer fakeContainer = new ProxyComponentContainer();
        containerRegistrar.registerComponents(fakeContainer);

        assertThat(fakeContainer.getCyclicDependencies(), hasNoCycles());
    }

    // self-tests below here. the tests below should never fail.

    @Test
    public void graphCycleEquals() {
        assertEquals(GraphCycle.of('A', 'A'), GraphCycle.of('A', 'A'));
        assertEquals(GraphCycle.of('A', 'B', 'A'), GraphCycle.of('B', 'A', 'B'));
        assertEquals(GraphCycle.of('A', 'B', 'C', 'A'), GraphCycle.of('B', 'C', 'A', 'B'));
        assertEquals(GraphCycle.of('A', 'B', 'C', 'D', 'A'), GraphCycle.of('B', 'C', 'D', 'A', 'B'));
        assertEquals(GraphCycle.of(1, 2, 3, 4, 5, 6, 1), GraphCycle.of(6, 1, 2, 3, 4, 5, 6));

        // different elements
        assertNotSame(GraphCycle.of('A', 'B', 'A'), GraphCycle.of('C', 'B', 'C'));

        // reversed order
        assertNotSame(GraphCycle.of('A', 'B', 'C', 'A'), GraphCycle.of('C', 'B', 'A', 'C'));

        // same elements, different order
        assertNotSame(GraphCycle.of('A', 'B', 'C', 'D', 'A'), GraphCycle.of('A', 'B', 'D', 'C', 'A'));

        assertEquals(ImmutableSet.of(
                GraphCycle.of('A', 'B', 'A'),
                GraphCycle.of('A', 'B', 'C', 'A'),
                GraphCycle.of('A', 'B', 'C', 'D', 'A')
        ).size(), 3);
    }

    @Test
    public void graphCycleHashCode() {
        assertEquals(GraphCycle.of('A', 'A').hashCode(), GraphCycle.of('A', 'A').hashCode());
        assertEquals(GraphCycle.of('A', 'B', 'A').hashCode(), GraphCycle.of('B', 'A', 'B').hashCode());
        assertEquals(GraphCycle.of('A', 'B', 'C', 'A').hashCode(), GraphCycle.of('B', 'C', 'A', 'B').hashCode());
        assertEquals(GraphCycle.of('A', 'B', 'C', 'D', 'A').hashCode(), GraphCycle.of('B', 'C', 'D', 'A', 'B').hashCode());
        assertEquals(GraphCycle.of(1, 2, 3, 4, 5, 6, 1).hashCode(), GraphCycle.of(6, 1, 2, 3, 4, 5, 6).hashCode());
    }

    @Test
    public void graphCycleRejectsInvalidData() {
        invalidCycle(1);
        invalidCycle(1, 2);
        invalidCycle(1, 2, 3);
        invalidCycle(1, 1, 1);
        invalidCycle(1, 2, 2, 1);
        invalidCycle(1, 2, null, 1);
        invalidCycle(1, 2, 1, 2, 1);
    }

    @SafeVarargs
    private final <T> void invalidCycle(T... cycle) {
        try {
            fail("this cycle should be invalid: " + GraphCycle.of(cycle));
        } catch (IllegalArgumentException | NullPointerException expected) {
        }
    }

    /**
     * Graph:
     * <pre>
     * B -> C -> E -> G -> H
     * ^    |         ^    |
     * |    v         |    /
     * A <- D         I <-/
     * </pre>
     */
    @Test
    public void graphTraversalFindsMultipleCyclesWithoutCommonParts() {
        SetMultimap<Character, Character> dependencyGraph = HashMultimap.create();
        dependencyGraph.put('A', 'B');
        dependencyGraph.put('B', 'C');
        dependencyGraph.put('C', 'D');
        dependencyGraph.put('C', 'E');
        dependencyGraph.put('D', 'A');
        dependencyGraph.put('E', 'G');
        dependencyGraph.put('G', 'H');
        dependencyGraph.put('H', 'I');
        dependencyGraph.put('I', 'G');


        GraphVisitor<Character> v = new GraphVisitor<>(dependencyGraph);

        assertEquals(ImmutableSet.of(
                        GraphCycle.of('A', 'B', 'C', 'D', 'A'),
                        GraphCycle.of('G', 'H', 'I', 'G')),
                v.getGraphCycles());
    }

    /**
     * Graph:
     * <pre>
     *  101 -> 101
     * </pre>
     */
    @Test
    public void graphTraversalFindsSelfReferentialCycles() {
        SetMultimap<Integer, Integer> dependencyGraph = HashMultimap.create();
        dependencyGraph.put(101, 101);

        GraphVisitor<Integer> v = new GraphVisitor<>(dependencyGraph);

        assertEquals(ImmutableSet.of(GraphCycle.of(101, 101)), v.getGraphCycles());
    }

    /**
     * Graph:
     * <pre>
     *  1 -> 2
     *  2 -> 3
     *  3 -> 1
     *  11 -> 22
     *  22 -> 33
     *  33 -> 11
     * </pre>
     */
    @Test
    public void graphTraversalFindsDisconnectedCycles() {
        SetMultimap<Integer, Integer> dependencyGraph = HashMultimap.create();

        dependencyGraph.put(1, 2);
        dependencyGraph.put(2, 3);
        dependencyGraph.put(3, 1);

        dependencyGraph.put(11, 22);
        dependencyGraph.put(22, 33);
        dependencyGraph.put(33, 11);

        GraphVisitor<Integer> v = new GraphVisitor<>(dependencyGraph);

        assertEquals(ImmutableSet.of(
                GraphCycle.of(1, 2, 3, 1),
                GraphCycle.of(11, 22, 33, 11)), v.getGraphCycles());
    }

    @Test
    public void graphTraversalFindsMultipleIntersectingCycles() {
        SetMultimap<Integer, Integer> dependencyGraph = HashMultimap.create();

        dependencyGraph.put(1, 2);
        dependencyGraph.put(2, 3);
        dependencyGraph.put(3, 1);
        dependencyGraph.put(4, 2);
        dependencyGraph.put(2, 5);
        dependencyGraph.put(5, 4);

        GraphVisitor<Integer> v = new GraphVisitor<>(dependencyGraph);

        assertEquals(ImmutableSet.of(
                GraphCycle.of(1, 2, 3, 1), GraphCycle.of(4, 2, 5, 4)), v.getGraphCycles());
    }

    interface A {
    }

    interface B {
    }

    interface C extends A, B {
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testGetGreediestSatisfiableConstructor() {
        ProxyComponentContainer c = new ProxyComponentContainer();

        class Ai {
        }                                              // default ctor
        class Bi {
            Bi() {
            }
        }                                     // non-public empty ctor
        class Ci {
            Ci(B b) {
            }
        }                                  // non-public ctor
        class Di {
            public Di(A a, B b) {
            }
        }                      // single public ctor
        class Ei {
            public Ei(A a) {
            }

            public Ei(A a, B b) {
            }
        }    // 2 public ctors
        class Fi {
            public Fi(A a, B b) {
            }

            Fi(A a, B b, C c) {
            }
        } // public ctor is shorter than priv ctor

        assertThat(c.getGreediestSatisfiableConstructor(Ai.class), nullValue());
        assertThat(c.getGreediestSatisfiableConstructor(Bi.class), nullValue());
        assertThat(c.getGreediestSatisfiableConstructor(Ci.class), nullValue());

        // note: non-static inner classes, so outer class is reported as an implicit ctor param, hence the getClass()
        assertThat(c.getGreediestSatisfiableConstructor(Di.class), arrayContaining(getClass(), A.class, B.class));
        assertThat(c.getGreediestSatisfiableConstructor(Ei.class), arrayContaining(getClass(), A.class, B.class));
        assertThat(c.getGreediestSatisfiableConstructor(Fi.class), arrayContaining(getClass(), A.class, B.class));
    }

    @Test
    public void testContainerRevealsPublicCtorCycles() {
        ProxyComponentContainer c = new ProxyComponentContainer();

        class Ai implements A {
            public Ai(B b) {
            }
        }
        class Bi implements B {
            public Bi(A a) {
            }
        }

        c.implementation(null, A.class, Ai.class);
        c.implementation(null, B.class, Bi.class);

        //noinspection unchecked
        assertThat(c.getCyclicDependencies(), hasCycles(GraphCycle.of(Ai.class, Bi.class, Ai.class)));
    }

    @Test
    public void testContainerDoesntRevealNonPublicCtorCycles() {
        ProxyComponentContainer c = new ProxyComponentContainer();

        class Ai implements A {
            Ai(B b) {
            }

            public Ai() {
            }
        }
        class Bi implements B {
            Bi(A a) {
            }

            public Bi() {
            }
        }

        c.implementation(null, A.class, Ai.class);
        c.implementation(null, B.class, Bi.class);

        assertThat(c.getCyclicDependencies(), hasNoCycles());
    }

    @Test
    public void testContainerWithClassesThatDependOnTheInterfaceTheyAreMappedTo() {
        ProxyComponentContainer c = new ProxyComponentContainer();

        class A1 implements A {
            public A1(A delegate) {
            }
        }
        class A2 implements A {
            public A2(A a) {
            }
        }

        c.implementation(null, A2.class);
        c.implementation(null, A.class, A1.class);

        assertThat(c.getCyclicDependencies(), hasNoCycles());
    }


    // end of tests /~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    /**
     * Traverses a passed directed graph to find {@link GraphCycle}s.
     * <p>
     * This implementation deliberately trades off cycle-finding completeness for a large performance
     * gain over "complete" cycle-finding algorithms like Tarjan's and Johnson's, which are 1-2 orders
     * of magnitude slower than this one for the size of graphs we're searching (3.3K+ vertices).
     * </p>
     * <p>
     * The key difference is that this implementation visits _each edge (pair of vertices) only once_,
     * which means that this impl will not report cycles that share common edges. The simplest graph that
     * demonstrates this limitation (or benefit, depending on your POV):
     * <pre>
     *       1 -- 2 -- 3 -- 1
     *         \     /
     *            4
     * </pre>
     * ...this algorithm will report only the 1 -> 2 -> 3 -> 1 cycle, and not the 1 -> 4 -> 3 -> 1
     * cycle, because of the shared 3 -> 1 edge. This is considered to be a Good Thing (TM) for this
     * application as reporting the full set of all variant cycles in a 3K+ node graph blows the cycle
     * count out massively, performs so much slower, and is not particularly more informative than
     * simply reporting the most distinct "core" cycles, as this impl does.
     * </p>
     */
    private static class GraphVisitor<T> {
        private final SetMultimap<T, T> directedGraph;
        private final Set<GraphCycle<T>> cycles = new HashSet<>();

        /**
         * Per-graph branch set of visited vertices.
         */
        private final Set<T> branchSet = new HashSet<>();

        /**
         * Per-graph branch stack of visited vertices in order.
         */
        private final Stack<T> branch = new Stack<>();

        /**
         * Per visit graph of visited vertices. Ensures each distinct pair of vertices in {@link #directedGraph}
         * is only ever visited once, which saves a lot of otherwise pointless iteration.
         */
        private SetMultimap<T, T> visitedGraph;

        private GraphVisitor(SetMultimap<T, T> directedGraph) {
            this.directedGraph = ImmutableSetMultimap.copyOf(directedGraph);
        }

        private Set<GraphCycle<T>> getGraphCycles() {
            visit();
            return cycles;
        }

        private void visit() {
            visitedGraph = createEmptyGraphWithExpectedSize(directedGraph);

            for (T element : directedGraph.keySet()) {
                visit(element);

                assert branchSet.isEmpty() : branchSet;
                assert branch.isEmpty() : branch;
            }

            assert visitedGraph.size() == directedGraph.size();
            visitedGraph.clear();
        }

        private void visit(T c) {
            if (!directedGraph.containsKey(c)) {
                return;
            }

            if (branchSet.contains(c)) {
                // then there's a cycle starting and ending at c
                List<T> cycle = newArrayList(branch.subList(branch.lastIndexOf(c), branch.size()));
                cycle.add(c);
                cycles.add(new GraphCycle<>(cycle));
                return;
            }

            branch.push(c);
            branchSet.add(c);

            for (T dep : directedGraph.get(c)) {
                // visit each pair of vertices only once
                if (!visitedGraph.containsEntry(c, dep)) {
                    visitedGraph.put(c, dep);
                    visit(dep);
                }
            }

            branch.pop();
            branchSet.remove(c);
        }
    }

    /**
     * Creates a pre-sized empty graph pre-sized to the given graph.
     */
    private static <T> SetMultimap<T, T> createEmptyGraphWithExpectedSize(SetMultimap<T, T> graph) {
        int numKeys = graph.keySet().size();
        int numValuesPerKey = Integer.highestOneBit(1 + (graph.size() / numKeys)) << 1;
        return HashMultimap.create(numKeys, numValuesPerKey);
    }

    /**
     * Encapsulates a single, linear graph cycle, eg: a->b->c->a.
     * <p>
     * Cyclic permutations are considered equivalent, eg: a->b->c->a, b->c->a->b, c->a->b->c are considered equal.
     * <p>
     * Though not needed for function, this impl preserves construction order of elements for the sake of easier
     * printing. Because lazy.
     */
    @Immutable
    static final class GraphCycle<T> {
        /**
         * Adjacency list of elements.
         */
        private final Map<T, T> elements;

        /**
         * Preserve original construction order.
         */
        private final List<T> cycle;

        private final int hashCode;

        private GraphCycle(@Nonnull List<T> cycle) {
            this.cycle = copyOf(cycle);
            this.elements = createAdjacencyList(cycle);
            this.hashCode = elements.hashCode();
        }

        @SafeVarargs
        @SuppressWarnings("unchecked")
        private static <T> GraphCycle<T> of(T... elements) {
            return new GraphCycle(copyOf(elements));
        }

        @Override
        public boolean equals(Object other) {
            // considers only elements, order doesn't matter
            return other instanceof GraphCycle && elements.equals(((GraphCycle) other).elements);
        }

        @Override
        public int hashCode() {
            // considers only elements hashCode, order doesn't matter
            return hashCode;
        }

        @Override
        public String toString() {
            return Joiner.on(" -> ").join(cycle);
        }

        private String toString(@Nonnull Function<? super T, String> stringifyT) {
            return Joiner.on(" -> ").join(Lists.transform(cycle, stringifyT));
        }

        static <T> Multiset<Entry<T, T>> findCommonComponents(Iterable<GraphCycle<T>> cycles)
                throws NoSuchElementException {
            Multiset<Entry<T, T>> commonComponents = HashMultiset.create();
            for (GraphCycle<T> g : cycles) {
                for (Entry<T, T> dependencyPair : g.elements.entrySet()) {
                    commonComponents.add(dependencyPair);
                }
            }

            return commonComponents;
        }

        /**
         * Given a cycle of a->b->c, create map of a->b, b->c, c->a.
         */
        private Map<T, T> createAdjacencyList(@Nonnull List<T> cycle) {
            if (cycle.size() < 2) {
                throw new IllegalArgumentException("Not a cycle: " + cycle);
            }

            Map<T, T> elements = new HashMap<>(cycle.size() - 1, 1.0f);
            for (int i = 1; i < cycle.size(); i++) {
                elements.put(cycle.get(i - 1), cycle.get(i));
            }

            if (elements.size() != cycle.size() - 1 || !cycle.get(0).equals(cycle.get(cycle.size() - 1))) {
                throw new IllegalArgumentException("Not a cycle: " + cycle);
            }

            return ImmutableMap.copyOf(elements);
        }
    }

    /**
     * Masquerades as the {@link ComponentContainer} in the {@link ContainerRegistrar#registerComponents} call.
     */
    private static class ProxyComponentContainer extends ComponentContainer {
        private static final Class<?> NO_DEPENDENCIES = Void.class;

        /**
         * Map of all implementation classes to their respective set of dependencies (usually interfaces), as declared
         * by their {@link #getGreediestSatisfiableConstructor longest satisfiable constructor}. Classes with no
         * dependencies (ie empty ctors) map to {@link #NO_DEPENDENCIES}.
         */
        private final SetMultimap<Class<?>, Class<?>> dependencyGraph = HashMultimap.create();

        /**
         * Map of all interfaces made known to pico to their configured implementations.
         */
        private final Map<Class<?>, Class<?>> interfaceToImplementationMap = new HashMap<>();

        /**
         * Map of all classes added to pico _without_ a keyClass, ie: not mapped to an interface.
         */
        private Map<Class<?>, Class<?>> unkeyedImplementations = new HashMap<>();

        /**
         * Returns the Set of cyclic dependencies between DI-managed classes.
         */
        private Set<GraphCycle<Class<?>>> getCyclicDependencies() {
            // local copy because in-place modification of SetMultimap is fricken impossible
            SetMultimap<Class<?>, Class<?>> implDependencyGraph = getNormalisedImplementations();

            GraphVisitor<Class<?>> graphVisitor = new GraphVisitor<>(implDependencyGraph);

            return graphVisitor.getGraphCycles();
        }

        /**
         * Replaces all refs to interface usages with their configured concrete impls.
         */
        private SetMultimap<Class<?>, Class<?>> getNormalisedImplementations() {
            SetMultimap<Class<?>, Class<?>> implGraph = createEmptyGraphWithExpectedSize(dependencyGraph);

            for (Entry<Class<?>, Class<?>> entrySet : dependencyGraph.entries()) {
                Class<?> classWithDeps = entrySet.getKey();
                Class<?> dep = entrySet.getValue();

                if (dep.isInterface()) {
                    Class<?> impl = interfaceToImplementationMap.get(dep);
                    if (impl != null) {
                        // then a configured concrete impl exists
                        assert !impl.isInterface() : impl;

                        if (impl == classWithDeps && unkeyedImplementations.containsKey(dep)) {
                            // then class depends on an instance of the interface it is mapped to,
                            // eg: use of delegate, filter or decorator pattern, but pico knows it already has a
                            // class that it can use to satisfy the dep, so it won't be reported as a cycle.
                            continue;
                        } else {
                            implGraph.put(classWithDeps, impl);
                        }
                    } else {
                        // then dep has never been mapped to an implementing class, probably because
                        // DI of this interface is happening via a method provided to pico via a class that implements
                        // the Injector interface, so we effectively skip its deps, since the construction is explicit
                        // and can't (?) cause cycles.
                        implGraph.put(classWithDeps, dep);
                    }
                } else {
                    // then dep is already a concrete class
                    implGraph.put(classWithDeps, dep);
                }
            }

            return implGraph;
        }

        /**
         * Tries to emulate pico's constructor selection.
         *
         * @see org.picocontainer.injectors.ConstructorInjector#getGreediestSatisfiableConstructor
         */
        private void registerClassFor(@Nonnull Class<?> impl, @Nullable Class<?> keyClass) {
            Class<?>[] ctorParams = getGreediestSatisfiableConstructor(impl);
            registerClassFor(impl, keyClass, ctorParams);
        }

        private void registerClassFor(@Nonnull Class<?> impl, @Nullable Class<?> keyClass, @Nullable Class<?>[] deps) {
            assert !impl.isInterface() : impl;

            if (keyClass != null && keyClass.isInterface()) {
                interfaceToImplementationMap.put(keyClass, impl);
            }

            if (deps == null || deps.length == 0) {
                // no non-private, non-zero-length ctor, therefore no dependencies.
                dependencyGraph.put(impl, NO_DEPENDENCIES);
                return;
            }

            for (Class<?> dependency : deps) {
                dependencyGraph.put(impl, dependency);
            }
        }


        /**
         * Note: this impl doesn't attempt to do pico's "satisfiable" check as the longest public ctor
         * seems to be more than enough (for now).
         */
        @Nullable
        private Class<?>[] getGreediestSatisfiableConstructor(@Nonnull Class<?> impl) {
            Constructor<?>[] constructors = impl.getDeclaredConstructors();
            Class<?>[] ctorParams = null;

            for (int i = 0, max = 0; i < constructors.length; i++) {
                // jira seems to be using pico's default behaviour of using only public constructors
                if (!isPublic(constructors[i].getModifiers())) {
                    continue;
                }

                Class<?>[] params = constructors[i].getParameterTypes();
                if (params.length > max) {
                    max = params.length;
                    ctorParams = params;
                }
            }

            return ctorParams;
        }

        private void registerUnkeyedComponent(@Nonnull Class<?> c) {
            for (Class<?> iface : c.getInterfaces()) {
                // don't bother checking that interface mapping overlap, let pico handle.
                unkeyedImplementations.put(iface, c);
            }
            unkeyedImplementations.put(c, c);
        }

        // ComponentContainer overrides /~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

        @Override
        void implementation(Scope scope, Class<?> implementation) {
            registerClassFor(implementation, null);
            registerUnkeyedComponent(implementation);
        }

        @Override
        <T> void implementation(Scope scope, Class<? super T> key, Class<T> implementation) {
            registerClassFor(implementation, key);
        }

        @Override
        <T> void implementation(Scope scope, Class<? super T> key, Class<T> implementation, Parameter[] parameters) {
            registerClassFor(implementation, key);
        }

        @Override
        <T> void implementationUseDefaultConstructor(Scope scope, Class<T> key, Class<? extends T> implementation) {
            registerClassFor(implementation, key);
        }

        @Override
        <T> void implementation(Scope scope, Class<? super T> key, Class<T> implementation, Object... parameterKeys) {
            // This method is used to do funky stuff eg: register classes that require instances of themselves or
            // instances of the interface they're being mapped to, so need to explicitly exclude deliberate
            // self-cycles as pico won't have an issue with those.
            List<Class<?>> classes = newArrayListWithCapacity(parameterKeys.length);
            for (Object param : parameterKeys) {
                if (param instanceof Class && param != key && param != implementation) {
                    classes.add((Class<?>) param);
                }
            }
            registerClassFor(implementation, key, classes.toArray(new Class<?>[classes.size()]));
        }

        @Override
        <T> void provideViaFactory(Class<? super T> key, Class<? extends ServiceFactory<T>> factory) {
            // noop, because the method doesn't change Pico container
        }

        @Override
        void component(Scope scope, ComponentAdapter componentAdapter) {
            // special-cased because we can't handle it
            if (componentAdapter instanceof AppPropertiesComponentAdaptor) return;

            Object key = componentAdapter.getComponentKey();
            Class<?> impl = componentAdapter.getComponentImplementation();

            // impl can be null because of mocking accessed in MultipleKeyRegistrant
            if (impl == null || impl.isInterface())
                return;

            registerClassFor(impl, (key instanceof Class) ? (Class<?>) key : null);
        }

        // methods that can't be NOOPs /~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

        @Override
        <T> T getComponentInstance(final Class<T> key) {
            return mock(key, RETURNS_MOCKS);
        }

        @Override
        MutablePicoContainer getPicoContainer() {
            return mock(MutablePicoContainer.class, RETURNS_MOCKS);
        }

        @Override
        HostComponentProvider getHostComponentProvider() {
            return mock(HostComponentProvider.class, RETURNS_MOCKS);
        }

        @Override
        ComponentAdapter getComponentAdapter(final Class key) {
            return mock(ComponentAdapter.class, RETURNS_MOCKS);
        }

        // NOOPs /~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

        @Override
        void initializeEagerComponents() {
            // NOOP
        }

        @Override
        void instance(Scope scope, Object instance) {
            // NOOP, instances don't have deps; they are the leaves of the dep graph & can't form cycles
        }

        @Override
        void instance(Scope scope, String key, Object instance) {
            // NOOP, instances don't have deps; they are the leaves of the dep graph & can't form cycles
        }

        @Override
        <T, S extends T> void instance(Scope scope, Class<T> key, S instance) {
            // NOOP, instances don't have deps; they are the leaves of the dep graph & can't form cycles
        }
    }

    @SafeVarargs
    private static GraphCycleMatcher hasCycles(GraphCycle<Class<?>>... cycles) {
        return new GraphCycleMatcher(ImmutableSet.copyOf(cycles));
    }

    private static GraphCycleMatcher hasNoCycles() {
        return new GraphCycleMatcher(Collections.<GraphCycle<Class<?>>>emptySet());
    }

    /**
     * Matcher to describe {@link Class} cycles using {@link Class#getSimpleName()}.
     */
    private static class GraphCycleMatcher extends BaseMatcher<Set<GraphCycle<Class<?>>>> {
        private final Set<GraphCycle<Class<?>>> expectedCycleSet;

        private GraphCycleMatcher(@Nonnull Set<GraphCycle<Class<?>>> cycleSet) {
            this.expectedCycleSet = cycleSet;
        }

        @Override
        public boolean matches(@Nonnull Object beingMatched) {
            @SuppressWarnings("unchecked")
            Set<GraphCycle<Class<?>>> cycleSet = (Set<GraphCycle<Class<?>>>) beingMatched;

            return expectedCycleSet.equals(cycleSet);
        }

        @Override
        public void describeTo(Description description) {
            describeTo(description, expectedCycleSet);
        }

        @Override
        public void describeMismatch(Object failedMatch, Description description) {
            @SuppressWarnings("unchecked")
            Set<GraphCycle<Class<?>>> cycleSet = (Set<GraphCycle<Class<?>>>) failedMatch;

            description.appendText(format("at least %d cycle(s) found: ", cycleSet.size()));
            describeTo(description, cycleSet);

            Multiset<Entry<Class<?>, Class<?>>> commonComponents = copyHighestCountFirst(findCommonComponents(cycleSet));
            if (commonComponents.size() > 0) {
                description.appendText("\n\nBest dependencies to break/refactor to fix cycles:\n");

                int i = 0;
                for (Entry<Class<?>, Class<?>> dependencyPair : commonComponents.elementSet()) {
                    // print up to 5 of the most frequent pairs
                    if (i++ == 5) break;

                    description.appendText(format("    %s -> %s: found in %d/%d cycle(s)\n",
                            dependencyPair.getKey().getSimpleName(),
                            dependencyPair.getValue().getSimpleName(),
                            commonComponents.count(dependencyPair),
                            cycleSet.size()));
                }
            } else if (cycleSet.size() > 1) {
                description.appendText(
                        "\n\nThe above cycles have no common components; each cycle must be broken independently\n");
            }
        }

        private void describeTo(Description description, Set<GraphCycle<Class<?>>> cycleSet) {
            if (cycleSet.isEmpty()) {
                description.appendText("no cycles");
                return;
            }

            for (GraphCycle<Class<?>> cycle : cycleSet) {
                description.appendText(System.lineSeparator());
                description.appendText(cycle.toString(usingSimpleClassName));
            }
        }

        private static final Function<Class<?>, String> usingSimpleClassName = new Function<Class<?>, String>() {
            @Override
            public String apply(@Nullable final Class<?> c) {
                assert c != null;
                return c.getSimpleName();
            }
        };
    }
}
