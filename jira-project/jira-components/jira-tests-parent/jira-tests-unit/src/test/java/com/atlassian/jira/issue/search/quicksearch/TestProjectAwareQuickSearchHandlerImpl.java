package com.atlassian.jira.issue.search.quicksearch;

import com.atlassian.jira.junit.rules.InitMockitoMocks;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;

import java.util.Collections;

import static com.atlassian.jira.permission.ProjectPermissions.BROWSE_PROJECTS;
import static com.google.common.collect.ImmutableList.of;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

public class TestProjectAwareQuickSearchHandlerImpl {
    @Rule
    public final InitMockitoMocks mockitoFtw = new InitMockitoMocks(this);

    @Rule
    public final ExpectedException expectedException = ExpectedException.none();

    @Mock
    private ProjectManager mockProjectManager;

    @Mock
    private PermissionManager mockPermissionManager;

    @Mock
    private JiraAuthenticationContext mockAuthenticationContext;

    @Mock
    private ApplicationUser user;

    private ProjectAwareQuickSearchHandler projectAwareHandler;

    @Before
    public void setUp() throws Exception {
        user = new MockApplicationUser("testUser");
        when(mockAuthenticationContext.getUser()).thenReturn(user);

        projectAwareHandler = new ProjectAwareQuickSearchHandlerImpl(mockProjectManager, mockPermissionManager, mockAuthenticationContext);
    }

    @Test
    public void shouldFindValidProject() throws Exception {
        final Project project = createProject(10000L);

        assertThat(
                projectAwareHandler.getProjects(buildQuickSearchResult(idOf(project))),
                containsInAnyOrder(project)
        );

        verifyZeroInteractions(mockPermissionManager, mockAuthenticationContext);
    }

    @Test
    public void shouldReturnEmptySearchForNonExistentProjects() throws Exception {
        final long id = 10004L;
        createProject(id);

        assertEquals(
                projectAwareHandler.getProjects(buildQuickSearchResult(Long.toString(id + 1))),
                Collections.singletonList(null)
        );

        verifyZeroInteractions(mockPermissionManager, mockAuthenticationContext);
    }

    @Test
    public void shouldThrowNumberFormatExceptionOnMalformedInput() throws Exception {
        expectNumberFormatExceptionFor("invalid");
    }

    @Test
    public void shouldThrowNumberFormatExceptionOnEmptyInput() throws Exception {
        expectNumberFormatExceptionFor("");
    }

    @Test
    public void shouldThrowNumberFormatExceptionOnNullInput() throws Exception {
        expectNumberFormatExceptionFor(null);
    }

    @Test
    public void testReturnedBrowseableProjectsForNoInput() {
        final long id = 10090L;
        final Project project = createProject(id);
        when(mockPermissionManager.getProjects(eq(BROWSE_PROJECTS), any(ApplicationUser.class))).thenReturn(Collections.singletonList(project));

        assertThat(
                projectAwareHandler.getProjects(buildQuickSearchResult()),
                containsInAnyOrder(project)
        );
    }

    @Test
    public void shouldReturnAllBrowseableProjectsForNarrowerSearch() {
        final Project project1 = createProject(10010L);
        final Project project2 = createProject(10020L);
        final Project project3 = createProject(10030L);

        when(mockPermissionManager.getProjects(eq(BROWSE_PROJECTS), any(ApplicationUser.class))).thenReturn(of(project1, project2, project3));

        assertThat(
                projectAwareHandler.getProjects(buildQuickSearchResult(idOf(project1), idOf(project2))),
                containsInAnyOrder(project1, project2, project3)
        );
    }

    @Test
    public void shouldReturnOnlyBrowseableProjectsForWiderSearch() {
        final Project project1 = createProject(10010L);
        final Project project2 = createProject(10020L);
        final Project project3 = createProject(10030L);

        when(mockPermissionManager.getProjects(eq(BROWSE_PROJECTS), any(ApplicationUser.class))).thenReturn(of(project1, project2));

        assertThat(
                projectAwareHandler.getProjects(buildQuickSearchResult(idOf(project1), idOf(project2), idOf(project3))),
                containsInAnyOrder(project1, project2)
        );
    }

    @Test
    public void shouldReturnBrowseableProjectsForMultipleInputEvenIfMissed() {
        final Project project1 = createProject(10011L);
        final Project project2 = createProject(10021L);
        createProject(10031L);

        when(mockPermissionManager.getProjects(eq(BROWSE_PROJECTS), any(ApplicationUser.class))).thenReturn(of(project1, project2));

        assertThat(
                projectAwareHandler.getProjects(buildQuickSearchResult("1234", "5678")),
                containsInAnyOrder(project1, project2)
        );
    }

    @Test
    public void shouldNotThrowNumberFormatExceptionForMultipleInput() {
        final Project project1 = createProject(10011L);
        final Project project2 = createProject(10021L);
        createProject(10031L);

        when(mockPermissionManager.getProjects(eq(BROWSE_PROJECTS), any(ApplicationUser.class))).thenReturn(of(project1, project2));

        assertThat(
                projectAwareHandler.getProjects(buildQuickSearchResult("invalid", "input")),
                containsInAnyOrder(project1, project2)
        );
    }

    @Test
    public void shouldNotThrowNumberFormatExceptionForMultipleEmptyInputs() {
        final Project project1 = createProject(10011L);
        final Project project2 = createProject(10021L);
        createProject(10031L);

        when(mockPermissionManager.getProjects(eq(BROWSE_PROJECTS), any(ApplicationUser.class))).thenReturn(of(project1, project2));

        assertThat(
                projectAwareHandler.getProjects(buildQuickSearchResult("", "")),
                containsInAnyOrder(project1, project2)
        );
    }

    @Test
    public void shouldNotThrowNumberFormatExceptionForMultipleNullInputs() {
        final Project project1 = createProject(10011L);
        final Project project2 = createProject(10021L);
        createProject(10031L);

        when(mockPermissionManager.getProjects(eq(BROWSE_PROJECTS), any(ApplicationUser.class))).thenReturn(of(project1, project2));

        assertThat(
                projectAwareHandler.getProjects(buildQuickSearchResult(null, null)),
                containsInAnyOrder(project1, project2)
        );
    }

    private String idOf(final Project project) {
        return project.getId().toString();
    }

    private MockProject createProject(final long id) {
        final MockProject project = new MockProject(id);
        when(mockProjectManager.getProjectObj(id)).thenReturn(project);
        return project;
    }

    private void expectNumberFormatExceptionFor(final String input) {
        expectedException.expect(NumberFormatException.class);
        try {
            projectAwareHandler.getProjects(buildQuickSearchResult(input));
        } finally {
            verifyZeroInteractions(mockPermissionManager, mockAuthenticationContext);
        }
    }

    private QuickSearchResult buildQuickSearchResult(final String... projectIds) {
        final QuickSearchResult searchResult = new ModifiableQuickSearchResult("searchInput");
        for (final String projectId : projectIds) {
            searchResult.addSearchParameter("pid", projectId);
        }
        return searchResult;
    }
}
