package com.atlassian.jira.jql.values;

import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.bc.JiraServiceContextImpl;
import com.atlassian.jira.bc.user.search.UserSearchService;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.util.EmailFormatter;
import com.atlassian.jira.util.collect.CollectionBuilder;
import com.google.common.collect.ImmutableList;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Collections;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestUserClauseValuesGenerator {
    @Rule
    public MockitoRule mockito = MockitoJUnit.rule();
    @Mock
    private EmailFormatter emailFormatter;
    @Mock
    private UserSearchService userSearchService;
    @InjectMocks
    private UserClauseValuesGenerator valuesGenerator;

    @Test
    public void getPossibleValuesShouldReturnTheUsersEmailAddressIfEmailVisibilityIsOn() {
        final ApplicationUser loggedInUser = new MockApplicationUser("fred");
        final ApplicationUser userInQueryResults = new MockApplicationUser("andrew", "andrew stephens", "andrew@atlassian.com");

        UserClauseValuesGenerator.Results results = getPossibleValues(loggedInUser, userInQueryResults, "andrew@atlassian.com");

        assertUserClauseValuesHaveCorrectText(results, "andrew", "andrew stephens", "- andrew@atlassian.com", " (andrew)");
    }

    @Test
    public void getPossibleValuesShouldMaskTheUsersEmailIfEmailVisibilityIsMasked() {
        final ApplicationUser loggedInUser = new MockApplicationUser("fred");
        final ApplicationUser userInQueryResults = new MockApplicationUser("andrew", "andrew stephens", "andrew@atlassian.com");

        UserClauseValuesGenerator.Results results = getPossibleValues(loggedInUser, userInQueryResults, "andrew at atlassian dot com");

        assertUserClauseValuesHaveCorrectText(results, "andrew", "andrew stephens", "- andrew at atlassian dot com", " (andrew)");
    }

    @Test
    public void getPossibleValuesShouldHideTheUsersEmailAddressIfEmailVisibilityIsOff() {
        final ApplicationUser loggedInUser = new MockApplicationUser("fred");
        final ApplicationUser userInQueryResults = new MockApplicationUser("andrew", "andrew stephens", "andrew@atlassian.com");

        UserClauseValuesGenerator.Results results = getPossibleValues(loggedInUser, userInQueryResults, null);

        assertUserClauseValuesHaveCorrectText(results, "andrew", "andrew stephens", " (andrew)");
    }

    private ClauseValuesGenerator.Results getPossibleValues(ApplicationUser loggedInUser, ApplicationUser userInQueryResults, String formattedEmail) {
        final String queryValuePrefix = "andr";
        final UserSearchService userPickerSearchService = prepareMockedUserSearchService(queryValuePrefix, userInQueryResults);
        final EmailFormatter emailFormatter = prepareMockedEmailFormatter(userInQueryResults, loggedInUser, formattedEmail);

        final UserClauseValuesGenerator localUserClauseValuesGenerator = new UserClauseValuesGenerator(userPickerSearchService, emailFormatter);

        return localUserClauseValuesGenerator.getPossibleValues(loggedInUser, "assignee", queryValuePrefix, 10);
    }

    private UserSearchService prepareMockedUserSearchService(String queryValuePrefix, ApplicationUser userInQueryResults) {
        final UserSearchService userPickerSearchService = mock(UserSearchService.class);

        when(userPickerSearchService.canPerformAjaxSearch(Mockito.<JiraServiceContext>any())).
                thenReturn(true);

        when(userPickerSearchService.findUsersAllowEmptyQuery(Mockito.<JiraServiceContext>any(), Mockito.eq(queryValuePrefix))).
                thenReturn(ImmutableList.of(userInQueryResults));

        return userPickerSearchService;
    }

    private EmailFormatter prepareMockedEmailFormatter(ApplicationUser userInQueryResults, ApplicationUser loggedInUser, String formattedEmailAddress) {
        final EmailFormatter emailFormatter = Mockito.mock(EmailFormatter.class);

        when(emailFormatter.formatEmail(userInQueryResults, loggedInUser)).thenReturn(formattedEmailAddress);

        return emailFormatter;
    }

    private void assertUserClauseValuesHaveCorrectText(ClauseValuesGenerator.Results results, String username, String... displayNameParts) {
        assertEquals(1, results.getResults().size());

        final ClauseValuesGenerator.Result actualResult = results.getResults().get(0);
        final ClauseValuesGenerator.Result expectedResult = new ClauseValuesGenerator.Result(username, displayNameParts);

        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testGetPossibleValuesNoPrefix() throws Exception {
        final JiraServiceContextImpl ctx = new JiraServiceContextImpl((ApplicationUser) null);
        when(userSearchService.canPerformAjaxSearch(ctx)).thenReturn(true);

        when(userSearchService.findUsersAllowEmptyQuery(ctx, "")).thenReturn(Collections.emptyList());

        final ClauseValuesGenerator.Results possibleValues = valuesGenerator.getPossibleValues(null, "assignee", "", 10);

        assertEquals(0, possibleValues.getResults().size());
    }

    @Test
    public void testGetPossibleValuesAjaxSearchDisabled() throws Exception {
        when(userSearchService.canPerformAjaxSearch(new JiraServiceContextImpl((ApplicationUser) null))).thenReturn(false);

        final ClauseValuesGenerator.Results possibleValues = valuesGenerator.getPossibleValues(null, "assignee", "a", 10);

        assertEquals(0, possibleValues.getResults().size());
    }

    @Test
    public void testGetPossibleValuesHappyPath() throws Exception {
        final ApplicationUser user1 = new MockApplicationUser("adude", "A Dude", "adude@example.com");
        final ApplicationUser user2 = new MockApplicationUser("aadude", "Aa Dude", "aadude@example.com");
        final ApplicationUser user3 = new MockApplicationUser("bdude", "B Dude", "bdude@example.com");
        final ApplicationUser user4 = new MockApplicationUser("cdude", "C Dude", "cdude@example.com");

        when(emailFormatter.formatEmail(user1, null)).thenReturn(user1.getEmailAddress());
        when(emailFormatter.formatEmail(user2, null)).thenReturn(user2.getEmailAddress());
        when(emailFormatter.formatEmail(user3, null)).thenReturn(user3.getEmailAddress());
        when(emailFormatter.formatEmail(user4, null)).thenReturn(user4.getEmailAddress());

        final JiraServiceContextImpl ctx = new JiraServiceContextImpl((ApplicationUser) null);
        when(userSearchService.canPerformAjaxSearch(ctx)).thenReturn(true);

        when(userSearchService.findUsersAllowEmptyQuery(ctx, "a")).thenReturn(CollectionBuilder.newBuilder(user1, user2, user3, user4).asList());

        final ClauseValuesGenerator.Results possibleValues = valuesGenerator.getPossibleValues(null, "assignee", "a", 10);

        assertEquals(4, possibleValues.getResults().size());
        assertEquals(new ClauseValuesGenerator.Result(user1.getName(), new String[]{user1.getDisplayName(), "- " + user1.getEmailAddress(), " (" + user1.getName() + ")"}), possibleValues.getResults().get(0));
        assertEquals(new ClauseValuesGenerator.Result(user2.getName(), new String[]{user2.getDisplayName(), "- " + user2.getEmailAddress(), " (" + user2.getName() + ")"}), possibleValues.getResults().get(1));
        assertEquals(new ClauseValuesGenerator.Result(user3.getName(), new String[]{user3.getDisplayName(), "- " + user3.getEmailAddress(), " (" + user3.getName() + ")"}), possibleValues.getResults().get(2));
        assertEquals(new ClauseValuesGenerator.Result(user4.getName(), new String[]{user4.getDisplayName(), "- " + user4.getEmailAddress(), " (" + user4.getName() + ")"}), possibleValues.getResults().get(3));
    }

    @Test
    public void testGetPossibleValuesMatchToLimit() throws Exception {
        final ApplicationUser user1 = new MockApplicationUser("adude", "A Dude", "adude@example.com");
        final ApplicationUser user2 = new MockApplicationUser("aadude", "Aa Dude", "aadude@example.com");
        final ApplicationUser user3 = new MockApplicationUser("bdude", "B Dude", "bdude@example.com");
        final ApplicationUser user4 = new MockApplicationUser("cdude", "C Dude", "cdude@example.com");

        when(emailFormatter.formatEmail(user1, null)).thenReturn(user1.getEmailAddress());
        when(emailFormatter.formatEmail(user2, null)).thenReturn(user2.getEmailAddress());
        when(emailFormatter.formatEmail(user3, null)).thenReturn(user3.getEmailAddress());
        when(emailFormatter.formatEmail(user4, null)).thenReturn(user4.getEmailAddress());


        final JiraServiceContextImpl ctx = new JiraServiceContextImpl((ApplicationUser) null);
        when(userSearchService.canPerformAjaxSearch(ctx)).thenReturn(true);

        when(userSearchService.findUsersAllowEmptyQuery(ctx, "a")).thenReturn(CollectionBuilder.newBuilder(user1, user2, user3, user4).asList());

        final ClauseValuesGenerator.Results possibleValues = valuesGenerator.getPossibleValues(null, "assignee", "a", 3);

        assertEquals(3, possibleValues.getResults().size());
        assertEquals(new ClauseValuesGenerator.Result(user1.getName(), new String[]{user1.getDisplayName(), "- " + user1.getEmailAddress(), " (" + user1.getName() + ")"}), possibleValues.getResults().get(0));
        assertEquals(new ClauseValuesGenerator.Result(user2.getName(), new String[]{user2.getDisplayName(), "- " + user2.getEmailAddress(), " (" + user2.getName() + ")"}), possibleValues.getResults().get(1));
        assertEquals(new ClauseValuesGenerator.Result(user3.getName(), new String[]{user3.getDisplayName(), "- " + user3.getEmailAddress(), " (" + user3.getName() + ")"}), possibleValues.getResults().get(2));
    }

}
