package com.atlassian.jira.service.services.mail;

import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.mock.ofbiz.MockGenericValue;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.apache.log4j.Logger;
import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.ofbiz.core.entity.EntityExpr;
import org.ofbiz.core.entity.EntityExprList;
import org.ofbiz.core.entity.EntityOperator;
import org.ofbiz.core.entity.GenericValue;

import java.time.Clock;
import java.time.Duration;
import java.time.Instant;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.argThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isNull;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;
import static org.ofbiz.core.entity.EntityOperator.EQUALS;

public class TestDeadLetterStore {
    private static final String ENTITY = "DeadLetter";
    private static final String MESSAGE_ID = "messageId";
    private static final String LAST_SEEN = "lastSeen";
    private static final String MAILSERVER_ID = "mailServerId";
    private static final String FOLDER_NAME = "folderName";

    private static final String EXPIRATION_PROPERTY = "jira.email.deadletters.expiration.time.days";

    private static final String TEST_MESSAGE_ID = "messageId";
    private static final Long TEST_SERVER_ID = 1636L;
    private static final String TEST_FOLDER_NAME = "INBOX";

    private static final AtomicLong ID_COUNTER = new AtomicLong(0);

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @Mock
    OfBizDelegator ofBizDelegator;

    @Mock
    ApplicationProperties applicationProperties;

    @Mock
    Logger log;

    @Mock
    Clock clock;

    private final Instant now = Instant.now();

    @Before
    public void setUp() {
        when(clock.instant()).thenReturn(now);
        when(log.isDebugEnabled()).thenReturn(true);
    }

    @Test(expected = NullPointerException.class)
    public void constructorThrowsNPEWhenLogIsNull() {
        new DeadLetterStore(null);
    }

    @Test(expected = NullPointerException.class)
    public void existsThrowsNPEWhenServerIsNull() throws Exception {
        assertThat(getDeadLetterStore().exists(TEST_MESSAGE_ID, null, TEST_FOLDER_NAME), is(false));
    }

    @Test(expected = NullPointerException.class)
    public void existsThrowsNPEWhenFolderIsNull() throws Exception {
        assertThat(getDeadLetterStore().exists(TEST_MESSAGE_ID, TEST_SERVER_ID, null), is(false));
    }

    @Test
    public void existsReturnsFalseWhenDisabled() throws Exception {
        assertThat(getDisabledDeadLetterStore().exists(TEST_MESSAGE_ID, TEST_SERVER_ID, TEST_FOLDER_NAME), is(false));
        verifyZeroInteractions(ofBizDelegator);
    }

    @Test
    public void existsReturnsFalseWhenMessageIdIsNull() throws Exception {
        assertThat(getDeadLetterStore().exists(null, TEST_SERVER_ID, TEST_FOLDER_NAME), is(false));
        verifyZeroInteractions(ofBizDelegator);
    }

    @Test
    public void existsReturnsFalseWhenNothingFound() throws Exception {
        when(ofBizDelegator.findByCondition(eq(ENTITY),
                getEntityConditionMatcher(TEST_MESSAGE_ID, TEST_SERVER_ID, TEST_FOLDER_NAME), isNull(Collection.class), isNull(List.class)))
                .thenReturn(ImmutableList.of());

        assertThat(getDeadLetterStore().exists(TEST_MESSAGE_ID, TEST_SERVER_ID, TEST_FOLDER_NAME), is(false));
    }

    @Test
    public void existsReturnsTrueWhenEntryFound() throws Exception {
        when(ofBizDelegator.findByCondition(eq(ENTITY),
                getEntityConditionMatcher(TEST_MESSAGE_ID, TEST_SERVER_ID, TEST_FOLDER_NAME), isNull(Collection.class), isNull(List.class)))
                .thenReturn(ImmutableList.of(deadLetter(TEST_MESSAGE_ID, TEST_SERVER_ID, TEST_FOLDER_NAME)));

        assertThat(getDeadLetterStore().exists(TEST_MESSAGE_ID, TEST_SERVER_ID, TEST_FOLDER_NAME), is(true));
    }

    @Test(expected = NullPointerException.class)
    public void createOrUpdateThrowsNPEWhenServerIsNull() throws Exception {
        getDeadLetterStore().createOrUpdate(TEST_MESSAGE_ID, null, TEST_FOLDER_NAME);
    }

    @Test(expected = NullPointerException.class)
    public void createOrUpdateThrowsNPEWhenFolderIsNull() throws Exception {
        getDeadLetterStore().createOrUpdate(TEST_MESSAGE_ID, TEST_SERVER_ID, null);
    }

    @Test
    public void createOrUpdateDoesNothingWhenDisabled() throws Exception {
        getDisabledDeadLetterStore().createOrUpdate(TEST_MESSAGE_ID, TEST_SERVER_ID, TEST_FOLDER_NAME);
        verifyZeroInteractions(ofBizDelegator);
    }

    @Test
    public void createOrUpdateReturnsFalseWhenMessageIdIsNull() throws Exception {
        getDeadLetterStore().createOrUpdate(null, TEST_SERVER_ID, TEST_FOLDER_NAME);
        verifyZeroInteractions(ofBizDelegator);
    }

    @Test
    public void createOrUpdateCreatesNewValueWhenNothingFound() throws Exception {
        getDeadLetterStore().createOrUpdate(TEST_MESSAGE_ID, TEST_SERVER_ID, TEST_FOLDER_NAME);

        verify(ofBizDelegator).createValue(ENTITY,
                ImmutableMap.of(MESSAGE_ID, TEST_MESSAGE_ID, MAILSERVER_ID, TEST_SERVER_ID, LAST_SEEN, now.toEpochMilli(), FOLDER_NAME, TEST_FOLDER_NAME));
    }

    @Test
    public void createOrUpdateBumpsLastSeenWhenAlreadyExists() throws Exception {
        GenericValue deadLetter = deadLetter(TEST_MESSAGE_ID, TEST_SERVER_ID, TEST_FOLDER_NAME, now.minusSeconds(100));

        when(ofBizDelegator.findByCondition(eq(ENTITY),
                getEntityConditionMatcher(TEST_MESSAGE_ID, TEST_SERVER_ID, TEST_FOLDER_NAME), isNull(Collection.class), isNull(List.class)))
                .thenReturn(ImmutableList.of(deadLetter));

        getDeadLetterStore().createOrUpdate(TEST_MESSAGE_ID, TEST_SERVER_ID, TEST_FOLDER_NAME);

        verify(ofBizDelegator).bulkUpdateByAnd(ENTITY, ImmutableMap.of(LAST_SEEN, now.toEpochMilli()),
                ImmutableMap.of(MESSAGE_ID, TEST_MESSAGE_ID, MAILSERVER_ID, TEST_SERVER_ID, FOLDER_NAME, TEST_FOLDER_NAME));
    }

    @Test(expected = NullPointerException.class)
    public void deleteThrowsNPEWhenServerIsNull() throws Exception {
        getDeadLetterStore().delete(TEST_MESSAGE_ID, null, TEST_FOLDER_NAME);
    }

    @Test(expected = NullPointerException.class)
    public void deleteThrowsNPEWhenFolderIsNull() throws Exception {
        getDeadLetterStore().delete(TEST_MESSAGE_ID, TEST_SERVER_ID, null);
    }

    @Test
    public void deleteDoesNothingWhenDisabled() throws Exception {
        getDisabledDeadLetterStore().delete(TEST_MESSAGE_ID, TEST_SERVER_ID, TEST_FOLDER_NAME);
        verifyZeroInteractions(ofBizDelegator);
    }

    @Test
    public void deleteDoesNothingWhenMessageIdIsNull() throws Exception {
        getDeadLetterStore().delete(null, TEST_SERVER_ID, TEST_FOLDER_NAME);
        verifyZeroInteractions(ofBizDelegator);
    }

    @Test
    public void deleteRemovesSpecifiedDeadLetter() throws Exception {
        when(ofBizDelegator.removeByCondition(eq(ENTITY), getEntityConditionMatcher(TEST_MESSAGE_ID, TEST_SERVER_ID, TEST_FOLDER_NAME))).thenReturn(1);

        getDeadLetterStore().delete(TEST_MESSAGE_ID, TEST_SERVER_ID, TEST_FOLDER_NAME);

        verify(ofBizDelegator).removeByCondition(eq(ENTITY), getEntityConditionMatcher(TEST_MESSAGE_ID, TEST_SERVER_ID, TEST_FOLDER_NAME));
        verifyZeroInteractions(ofBizDelegator);
    }

    @Test
    public void deleteOldDeadLettersDoesNothingWhenDisabled() throws Exception {
        getDisabledDeadLetterStore().deleteOldDeadLetters();
        verifyZeroInteractions(ofBizDelegator);
    }

    @Test
    public void deleteOldDeadLettersRemovesEntriesOlderThanExpirationTime() throws Exception {
        int expirationTimeDays = 10;
        getDeadLetterStore(Integer.toString(expirationTimeDays)).deleteOldDeadLetters();

        verify(ofBizDelegator).removeByCondition(eq(ENTITY),
                argThat(new EntityExprMatcher(LAST_SEEN, EntityOperator.LESS_THAN, now.minus(Duration.ofDays(expirationTimeDays)).toEpochMilli())));
    }

    @Test
    public void invalidPropertyValueFallsBackToDefaultDuration() throws Exception {
        int defaultExpirationInDays = 30;

        getDeadLetterStore("notANumber").deleteOldDeadLetters();

        verify(ofBizDelegator).removeByCondition(eq(ENTITY),
                argThat(new EntityExprMatcher(LAST_SEEN, EntityOperator.LESS_THAN, now.minus(Duration.ofDays(defaultExpirationInDays)).toEpochMilli())));
    }

    private GenericValue deadLetter(final String message, final Long server, final String folder) {
        return new MockGenericValue(ENTITY, ImmutableMap.of("id", ID_COUNTER.incrementAndGet(), MESSAGE_ID, message, MAILSERVER_ID, server, FOLDER_NAME, folder));
    }

    private GenericValue deadLetter(final String message, final Long server, final String folder, final Instant time) {
        return new MockGenericValue(ENTITY, ImmutableMap.of("id", ID_COUNTER.incrementAndGet(), MESSAGE_ID, message, MAILSERVER_ID, server, FOLDER_NAME, folder, LAST_SEEN, time.toEpochMilli()));
    }

    private void setExpirationProperty(final String value) {
        when(applicationProperties.getDefaultBackedString(EXPIRATION_PROPERTY)).thenReturn(value);
    }

    private DeadLetterStore getDeadLetterStore() {
        return new DeadLetterStore(ofBizDelegator, clock, applicationProperties, log);
    }

    private DeadLetterStore getDeadLetterStore(String epxirationTime) {
        setExpirationProperty(epxirationTime);
        return new DeadLetterStore(ofBizDelegator, clock, applicationProperties, log);
    }

    private DeadLetterStore getDisabledDeadLetterStore() {
        setExpirationProperty("-1");
        return new DeadLetterStore(ofBizDelegator, clock, applicationProperties, log);
    }

    EntityExprList getEntityConditionMatcher(final String messageId, final Long serverId, final String folderName) {
        return argThat(new EntityExprListMatcher(new EntityExprMatcher(MESSAGE_ID, EQUALS, messageId),
                new EntityExprMatcher(MAILSERVER_ID, EQUALS, serverId),
                new EntityExprMatcher(FOLDER_NAME, EQUALS, folderName)));
    }

    private static class EntityExprListMatcher extends BaseMatcher<EntityExprList> {

        private final List<EntityExprMatcher> expresionMatchers;

        EntityExprListMatcher(EntityExprMatcher... expressions) {
            this.expresionMatchers = ImmutableList.copyOf(expressions);
        }

        @Override
        public boolean matches(final Object item) {
            final EntityExprList otherExpr = (EntityExprList) item;

            final Set<EntityExpr> otherExpressions = Sets.newHashSet();
            for (int i = 0; i < otherExpr.getExprListSize(); i++) {
                otherExpressions.add(otherExpr.getExpr(i));
            }

            final List<EntityExprMatcher> matchers = Lists.newArrayList(expresionMatchers);
            for (Iterator<EntityExpr> it = otherExpressions.iterator(); it.hasNext(); ) {
                final EntityExpr expr = it.next();
                for (Iterator<EntityExprMatcher> innerIt = matchers.iterator(); innerIt.hasNext(); ) {
                    final EntityExprMatcher matcher = innerIt.next();
                    if (matcher.matches(expr)) {
                        innerIt.remove();
                        it.remove();
                        break;
                    }
                }
            }
            return otherExpressions.isEmpty() && matchers.isEmpty();
        }

        @Override
        public void describeTo(final Description description) {
            description.appendText("Expected condition: ").appendText(expresionMatchers.toString());
        }
    }

    private static class EntityExprMatcher extends BaseMatcher<EntityExpr> {
        private final Object lhs;
        private final EntityOperator operator;
        private final Object rhs;

        EntityExprMatcher(final Object lhs, final EntityOperator operator, final Object rhs) {
            this.lhs = lhs;
            this.operator = operator;
            this.rhs = rhs;
        }

        @Override
        public boolean matches(final Object item) {
            EntityExpr otherExpr = (EntityExpr) item;
            return lhs.equals(otherExpr.getLhs()) && operator.equals(otherExpr.getOperator()) && rhs.equals(otherExpr.getRhs());
        }

        @Override
        public void describeTo(final Description description) {
            description.appendText("Expected condition: ").appendText(new EntityExpr(lhs.toString(), operator, rhs).toString());
        }
    }


}