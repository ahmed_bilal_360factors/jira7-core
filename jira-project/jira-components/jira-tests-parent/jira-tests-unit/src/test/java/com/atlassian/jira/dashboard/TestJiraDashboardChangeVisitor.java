package com.atlassian.jira.dashboard;

import com.atlassian.fugue.Option;
import com.atlassian.gadgets.DashboardItemState;
import com.atlassian.gadgets.GadgetId;
import com.atlassian.gadgets.GadgetState;
import com.atlassian.gadgets.LocalDashboardItemModuleId;
import com.atlassian.gadgets.LocalDashboardItemState;
import com.atlassian.gadgets.OpenSocialDashboardItemModuleId;
import com.atlassian.gadgets.dashboard.Color;
import com.atlassian.gadgets.dashboard.DashboardId;
import com.atlassian.gadgets.dashboard.DashboardState;
import com.atlassian.gadgets.dashboard.Layout;
import com.atlassian.gadgets.dashboard.spi.DashboardStateStoreException;
import com.atlassian.gadgets.dashboard.spi.GadgetLayout;
import com.atlassian.gadgets.dashboard.spi.changes.AddGadgetChange;
import com.atlassian.gadgets.dashboard.spi.changes.GadgetColorChange;
import com.atlassian.gadgets.dashboard.spi.changes.RemoveGadgetChange;
import com.atlassian.gadgets.dashboard.spi.changes.UpdateLayoutChange;
import com.atlassian.jira.portal.PortalPage;
import com.atlassian.jira.portal.PortalPageStore;
import com.atlassian.jira.portal.PortletConfigurationImpl;
import com.atlassian.jira.portal.PortletConfigurationStore;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.plugin.ModuleCompleteKey;
import com.google.common.collect.ImmutableList;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.atlassian.gadgets.dashboard.DashboardState.dashboard;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestJiraDashboardChangeVisitor {
    @Mock
    private PortletConfigurationStore portletConfigurationStore;
    @Mock
    private PortalPageStore portalPageStore;

    @Test
    public void testGadgetColorChange() {
        final PortletConfigurationImpl configuration = new PortletConfigurationImpl(10000L, 10020L, 0, 0, Option.<URI>none(), Color.color1, Collections.<String, String>emptyMap(), Option.<ModuleCompleteKey>none());
        when(portletConfigurationStore.getByPortletId(10000L)).thenReturn(configuration);

        final DashboardState updatedState = dashboard(DashboardId.valueOf(Long.toString(10020)))
                .title("My Dashboard").layout(Layout.AAA).build();
        initVisitor(updatedState).visit(new GadgetColorChange(GadgetId.valueOf("10000"), Color.color8));
        verify(portletConfigurationStore).updateGadgetColor(10000L, Color.color8);
    }

    @Test(expected = DashboardStateStoreException.class)
    public void testGadgetColorChangeNoGadget() {
        final DashboardState updatedState = dashboard(DashboardId.valueOf(Long.toString(10020)))
                .title("My Dashboard").layout(Layout.AAA).build();
        when(portletConfigurationStore.getByPortletId(-999L)).thenReturn(null);

        initVisitor(updatedState).visit(new GadgetColorChange(GadgetId.valueOf("-999"), Color.color8));
    }

    @Test
    public void testGadgetRemove() {
        final PortletConfigurationImpl pcToDelete = new PortletConfigurationImpl(10020L, 10025L, 0, 1, Option.<URI>none(), Color.color1, Collections.<String, String>emptyMap(), Option.<ModuleCompleteKey>none());
        when(portletConfigurationStore.getByPortletId(10020L)).thenReturn(pcToDelete);

        final List<DashboardItemState> firstColumn = new ArrayList<DashboardItemState>();
        firstColumn.add(GadgetState.gadget(GadgetId.valueOf("10000")).specUri(URI.create("http://example.gadet/spec1.xml")).build());
        firstColumn.add(GadgetState.gadget(GadgetId.valueOf("10030")).specUri(URI.create("http://example.gadet/spec1.xml")).build());
        final List<List<DashboardItemState>> columns = new ArrayList<List<DashboardItemState>>();
        columns.add(firstColumn);
        columns.add(Collections.<DashboardItemState>emptyList());

        final DashboardState updatedState = dashboard(DashboardId.valueOf(Long.toString(10025))).title("My Dashboard").
                layout(Layout.AA).dashboardColumns(columns).build();

        final JiraDashboardChangeHandler visitor = initVisitor(updatedState);

        visitor.visit(new RemoveGadgetChange(GadgetId.valueOf("10020")));

        verify(portletConfigurationStore).delete(pcToDelete);
        verify(portletConfigurationStore).updateGadgetPosition(10000L, 0, 0, 10025L);
        verify(portletConfigurationStore).updateGadgetPosition(10030L, 1, 0, 10025L);
    }

    @Test
    public void testUpdateLayout() {
        final PortalPage portalPage = PortalPage.id(10025L).name("Test Dashboard").description("").owner(new MockApplicationUser("admin")).favouriteCount(0L).layout(Layout.AAA).version(0L).build();
        when(portalPageStore.getPortalPage(10025L)).thenReturn(portalPage);

        when(portletConfigurationStore.getByPortletId(10000L)).thenReturn(new PortletConfigurationImpl(10000L, 10025L, 0, 0, Option.<URI>none(), Color.color1, Collections.<String, String>emptyMap(), Option.<ModuleCompleteKey>none()));
        when(portletConfigurationStore.getByPortletId(10030L)).thenReturn(new PortletConfigurationImpl(10030L, 10025L, 0, 1, Option.<URI>none(), Color.color1, Collections.<String, String>emptyMap(), Option.<ModuleCompleteKey>none()));

        //only gadgets in the second column need their position changed.
        when(portletConfigurationStore.getByPortletId(10040L)).thenReturn(new PortletConfigurationImpl(10040L, 10025L, 2, 0, Option.<URI>none(), Color.color1, Collections.<String, String>emptyMap(), Option.<ModuleCompleteKey>none()));
        when(portletConfigurationStore.getByPortletId(10050L)).thenReturn(new PortletConfigurationImpl(10050L, 10025L, 2, 1, Option.<URI>none(), Color.color1, Collections.<String, String>emptyMap(), Option.<ModuleCompleteKey>none()));
        when(portletConfigurationStore.getByPortletId(10060L)).thenReturn(new PortletConfigurationImpl(10060L, 10025L, 2, 2, Option.<URI>none(), Color.color1, Collections.<String, String>emptyMap(), Option.<ModuleCompleteKey>none()));

        final List<GadgetId> firstColumn = new ArrayList<GadgetId>();
        firstColumn.add(GadgetId.valueOf("10000"));
        firstColumn.add(GadgetId.valueOf("10030"));
        final List<GadgetId> secondColumn = new ArrayList<GadgetId>();
        secondColumn.add(GadgetId.valueOf("10040"));
        secondColumn.add(GadgetId.valueOf("10050"));
        secondColumn.add(GadgetId.valueOf("10060"));
        final List<List<GadgetId>> columns = new ArrayList<List<GadgetId>>();
        columns.add(firstColumn);
        columns.add(secondColumn);

        final DashboardState updatedState = dashboard(DashboardId.valueOf(Long.toString(10025))).title("My Dashboard").
                layout(Layout.AA).build();

        final JiraDashboardChangeHandler visitor = initVisitor(updatedState);

        visitor.visit(new UpdateLayoutChange(Layout.AA, new GadgetLayout(columns)));

        verify(portalPageStore).update(any(PortalPage.class));
        verify(portletConfigurationStore).updateGadgetPosition(10040L, 0, 1, 10025L);
        verify(portletConfigurationStore).updateGadgetPosition(10050L, 1, 1, 10025L);
        verify(portletConfigurationStore).updateGadgetPosition(10060L, 2, 1, 10025L);
    }

    @Test
    public void testAddGadget() {
        final long dashboardId = 10025L;
        final List<DashboardItemState> firstColumn = new ArrayList<DashboardItemState>();
        firstColumn.add(GadgetState.gadget(GadgetId.valueOf("10000")).specUri(URI.create("http://example.gadet/spec1.xml")).build());
        firstColumn.add(GadgetState.gadget(GadgetId.valueOf("10030")).specUri(URI.create("http://example.gadet/spec1.xml")).build());
        final List<DashboardItemState> secondColumn = new ArrayList<DashboardItemState>();
        secondColumn.add(GadgetState.gadget(GadgetId.valueOf("10040")).specUri(URI.create("http://example.gadet/spec1.xml")).build());
        secondColumn.add(GadgetState.gadget(GadgetId.valueOf("10070")).specUri(URI.create("http://example.gadet/spec2.xml")).build());
        secondColumn.add(createLocalItem(10050L, Color.color1, "key:plugin", Option.<URI>none()));
        secondColumn.add(GadgetState.gadget(GadgetId.valueOf("10060")).specUri(URI.create("http://example.gadet/spec1.xml")).build());
        final List<List<DashboardItemState>> columns = new ArrayList<List<DashboardItemState>>();
        columns.add(firstColumn);
        columns.add(secondColumn);

        final DashboardState updatedState = dashboard(DashboardId.valueOf(Long.toString(10025))).title("My Dashboard").
                layout(Layout.AA).dashboardColumns(columns).build();

        final JiraDashboardChangeHandler visitor = initVisitor(updatedState);

        final GadgetState state = GadgetState.gadget(GadgetId.valueOf("10070")).specUri(URI.create("http://example.gadet/spec2.xml")).color(Color.color1).build();
        visitor.visit(new AddGadgetChange(state, DashboardState.ColumnIndex.ONE, 1));

        verify(portletConfigurationStore).addDashboardItem(dashboardId, 10070L, 1, 1, Option.some(state.getGadgetSpecUri()), state.getColor(), state.getUserPrefs(), Option.<ModuleCompleteKey>none());
        verify(portletConfigurationStore).updateGadgetPosition(10050L, 2, 1, dashboardId);
        verify(portletConfigurationStore).updateGadgetPosition(10060L, 3, 1, dashboardId);
    }

    @Test
    public void testAddDashboardItem() {
        final String moduleKey = "key:plugin";
        final long dashboardId = 10025L;
        final DashboardItemState state = createLocalItem(10050L, Color.color1, moduleKey, Option.<URI>none());
        final List<List<DashboardItemState>> columns = ImmutableList.<List<DashboardItemState>>of(
                ImmutableList.of(
                        state,
                        GadgetState.gadget(GadgetId.valueOf("10070")).specUri(URI.create("http://example.gadet/spec2.xml")).build()
                )
        );
        final DashboardState updatedState = dashboard(DashboardId.valueOf(Long.toString(10025))).title("My Dashboard").
                layout(Layout.AA).dashboardColumns(columns).build();

        initVisitor(updatedState).visit(new AddGadgetChange(state, DashboardState.ColumnIndex.ZERO, 0));

        verify(portletConfigurationStore).addDashboardItem(dashboardId, 10050L, 0, 0, Option.<URI>none(), state.getColor(), Collections.<String, String>emptyMap(), Option.some(new ModuleCompleteKey(moduleKey)));
    }

    private DashboardItemState createLocalItem(final Long portletId, final Color color, final String moduleKey, final Option<URI> specUri) {
        return LocalDashboardItemState.builder()
                .gadgetId(GadgetId.valueOf(portletId.toString()))
                .color(color)
                .dashboardItemModuleId(buildLocalDashboardItemModuleId(new ModuleCompleteKey(moduleKey), specUri)).build();
    }

    private LocalDashboardItemModuleId buildLocalDashboardItemModuleId(final ModuleCompleteKey moduleKey, final Option<URI> specUri) {
        final Option<OpenSocialDashboardItemModuleId> openSocialId = specUri.map(
                new com.google.common.base.Function<URI, OpenSocialDashboardItemModuleId>() {
                    @Override
                    public OpenSocialDashboardItemModuleId apply(final URI openSocialSpecUri) {
                        return new OpenSocialDashboardItemModuleId(openSocialSpecUri);
                    }
                });
        return new LocalDashboardItemModuleId(moduleKey, openSocialId);
    }

    private JiraDashboardChangeHandler initVisitor(final DashboardState updatedState) {
        return new JiraDashboardChangeHandler(updatedState, portletConfigurationStore, portalPageStore);
    }
}
