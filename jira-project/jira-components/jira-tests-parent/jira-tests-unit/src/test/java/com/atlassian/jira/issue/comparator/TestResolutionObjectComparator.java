package com.atlassian.jira.issue.comparator;

import com.atlassian.jira.issue.resolution.Resolution;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import static org.hamcrest.Matchers.lessThan;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestResolutionObjectComparator {
    @Rule
    public final MethodRule initMockito = MockitoJUnit.rule();

    @Mock
    private Resolution mockResolution1;
    @Mock
    private Resolution mockResolution2;

    @Test
    public void simpleComparison() throws Exception {
        when(mockResolution1.getSequence()).thenReturn(1L);
        when(mockResolution2.getSequence()).thenReturn(3L);

        assertThat(ResolutionObjectComparator.RESOLUTION_OBJECT_COMPARATOR.compare(mockResolution1, mockResolution2), lessThan(0));
        assertEquals(0, ResolutionObjectComparator.RESOLUTION_OBJECT_COMPARATOR.compare(mockResolution2, mockResolution2));
        assertEquals(0, ResolutionObjectComparator.RESOLUTION_OBJECT_COMPARATOR.compare(mockResolution1, mockResolution1));
    }
}
