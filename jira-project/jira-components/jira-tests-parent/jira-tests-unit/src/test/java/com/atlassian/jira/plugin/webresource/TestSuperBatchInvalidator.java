package com.atlassian.jira.plugin.webresource;

import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.plugin.event.events.PluginFrameworkStartedEvent;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Arrays;
import java.util.Collection;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(Parameterized.class)
public class TestSuperBatchInvalidator {
    public static final String COUNTER = APKeys.WEB_RESOURCE_SUPER_BATCH_FLUSH_COUNTER;

    private final String superBatchVersion;
    private final String expectedSuperBatchVersionAfterIncrement;

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @Mock
    private PluginFrameworkStartedEvent pluginFrameworkStartedEvent;

    @Mock
    private ApplicationProperties applicationProperties;

    private SuperBatchInvalidator superBatchInvalidator;

    public TestSuperBatchInvalidator(final String superBatchVersion, final String expectedSuperBatchVersionAfterIncrement) {
        this.superBatchVersion = superBatchVersion;
        this.expectedSuperBatchVersionAfterIncrement = expectedSuperBatchVersionAfterIncrement;
    }

    @Before
    public void setUp() {
        superBatchInvalidator = new SuperBatchInvalidator(applicationProperties, null);
    }

    @Test
    public void testSuperBatchVersionIncrementedOnPluginFrameworkStarted() {
        when(applicationProperties.getDefaultBackedString(COUNTER)).thenReturn(superBatchVersion);
        when(applicationProperties.getString(APKeys.JIRA_SETUP)).thenReturn(null);

        superBatchInvalidator.pluginFrameworkStarted(pluginFrameworkStartedEvent);
        verify(applicationProperties).setString(COUNTER, expectedSuperBatchVersionAfterIncrement);
    }

    @Parameters
    public static Collection<Object[]> generateTestData() {
        return Arrays.asList(new Object[][]
                {
                        {"", "2"},
                        {"AAA", "2"},
                        {null, "2"},
                        {"123 ", "2"},
                        {"123 456", "2"},
                        {"1", "2"},
                        {"2134", "2135"}
                });
    }
}