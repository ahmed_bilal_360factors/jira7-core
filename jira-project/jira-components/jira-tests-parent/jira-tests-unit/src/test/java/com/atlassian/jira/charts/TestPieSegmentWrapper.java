package com.atlassian.jira.charts;

import com.atlassian.jira.bc.project.component.MockProjectComponent;
import com.atlassian.jira.bc.project.component.ProjectComponent;
import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.IssueConstant;
import com.atlassian.jira.issue.customfields.CustomFieldSearcher;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.statistics.FilterStatisticsValuesGenerator;
import com.atlassian.jira.junit.rules.InitMockitoMocks;
import com.atlassian.jira.plugin.customfield.CustomFieldSearcherModuleDescriptor;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.web.bean.MockI18nBean;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestPieSegmentWrapper {
    @Rule
    public TestRule initMockitoMocks = new InitMockitoMocks(this);

    @Test
    public void testProject() {
        final Project mockproject = createProjectMock("homosapien", 10000);
        final Project mockproject2 = createProjectMock("monkey", 900);

        final I18nHelper mockI18nBean = mock(I18nHelper.class);

        final PieSegmentWrapper pieSegmentWrapper = new PieSegmentWrapper(mockproject, mockI18nBean, "project", null, null);
        assertThat(pieSegmentWrapper.getKey(), equalTo(mockproject));
        assertThat(pieSegmentWrapper.getName(), equalTo("homosapien"));
        assertThat(pieSegmentWrapper.toString(), equalTo("homosapien"));

        final PieSegmentWrapper pieSegmentWrapper2 = new PieSegmentWrapper(mockproject2, mockI18nBean, "project", null, null);
        assertTrue(pieSegmentWrapper.compareTo(pieSegmentWrapper2) < 0);
        assertThat(pieSegmentWrapper.compareTo(pieSegmentWrapper), equalTo(0));
        assertTrue(pieSegmentWrapper2.compareTo(pieSegmentWrapper) > 0);
    }

    @Test
    public void testAssignees() {
        final ApplicationUser user = new MockApplicationUser("admin", "Administrator", "");

        final I18nHelper mockI18nBean = mock(I18nHelper.class);
        when(mockI18nBean.getText("gadget.filterstats.assignee.unassigned")).thenReturn("Not assigned!");

        final PieSegmentWrapper pieSegmentWrapper = new PieSegmentWrapper(user, mockI18nBean, "assignees", null, null);

        assertThat(pieSegmentWrapper.getKey(), equalTo(user));
        assertThat(pieSegmentWrapper.getName(), equalTo("Administrator"));
        assertThat(pieSegmentWrapper.toString(), equalTo("Administrator"));

        final PieSegmentWrapper nullWrapper = new PieSegmentWrapper(null, mockI18nBean, "assignees", null, null);
        assertThat(nullWrapper.getKey(), equalTo(null));
        assertThat(nullWrapper.getName(), equalTo("Not assigned!"));
        assertThat(nullWrapper.toString(), equalTo("Not assigned!"));
    }

    @Test
    public void testReporter() {
        final ApplicationUser user = new MockApplicationUser("admin", "Administrator", "");

        final I18nHelper mockI18nBean = mock(I18nHelper.class);
        when(mockI18nBean.getText("gadget.filterstats.reporter.unknown")).thenReturn("No reporter!");

        final PieSegmentWrapper pieSegmentWrapper = new PieSegmentWrapper(user, mockI18nBean, "reporter", null, null);

        assertThat(pieSegmentWrapper.getKey(), equalTo(user));
        assertThat(pieSegmentWrapper.getName(), equalTo("Administrator"));
        assertThat(pieSegmentWrapper.toString(), equalTo("Administrator"));

        final PieSegmentWrapper nullWrapper = new PieSegmentWrapper(null, mockI18nBean, "reporter", null, null);
        assertThat(nullWrapper.getKey(), equalTo(null));
        assertThat(nullWrapper.getName(), equalTo("No reporter!"));
        assertThat(nullWrapper.toString(), equalTo("No reporter!"));
    }

    @Test
    public void testResolution() {
        assertConstantPieSegment("resolution", "Resolved", "common.resolution.unresolved", "Unresolved");
    }

    @Test
    public void testPriorities() {
        assertConstantPieSegment("priorities", "High", "gadget.filterstats.priority.nopriority", "No Priority!");
    }

    @Test
    public void testIssueType() {
        assertConstantPieSegment("issuetype", "Bug", null, null);
    }

    @Test
    public void testStatuses() {
        assertConstantPieSegment("statuses", "Open", null, null);
    }

    @Test
    public void testIrrelevant() {
        final PieSegmentWrapper irrelevantWrapper = new PieSegmentWrapper(FilterStatisticsValuesGenerator.IRRELEVANT, new MockI18nBean(), "blah", null, null);

        assertFalse(irrelevantWrapper.isGenerateUrl());
        assertThat(irrelevantWrapper.getName(), equalTo("Irrelevant"));
        assertNull(irrelevantWrapper.getKey());
    }

    private void assertConstantPieSegment(final String statisticType, final String nameTranslation, final String i18nNullKey, final String i18nNullValue) {

        final IssueConstant mockIssueConstant = mock(IssueConstant.class);
        final ConstantsManager mockConstantsManager = mock(ConstantsManager.class);
        when(mockIssueConstant.getNameTranslation()).thenReturn(nameTranslation);

        final I18nHelper mockI18nBean = mock(I18nHelper.class);
        //some constants can't be null (issuetype, status)
        if (i18nNullKey != null) {
            when(mockI18nBean.getText(i18nNullKey)).thenReturn(i18nNullValue);
        }

        final PieSegmentWrapper pieSegmentWrapper = new PieSegmentWrapper(mockIssueConstant, mockI18nBean, statisticType, mockConstantsManager, null);

        assertThat(pieSegmentWrapper.getKey(), equalTo(mockIssueConstant));
        assertThat(pieSegmentWrapper.getName(), equalTo(nameTranslation));
        assertThat(pieSegmentWrapper.toString(), equalTo(nameTranslation));

        //some constants can't be null (issuetype, status)
        if (i18nNullKey != null) {
            final PieSegmentWrapper nullWrapper = new PieSegmentWrapper(null, mockI18nBean, statisticType, mockConstantsManager, null);
            assertThat(nullWrapper.getKey(), equalTo(null));
            assertThat(nullWrapper.getName(), equalTo(i18nNullValue));
            assertThat(nullWrapper.toString(), equalTo(i18nNullValue));
        }
    }

    @Test
    public void testComponents() {
        final ProjectComponent mock = new MockProjectComponent(10000L, "Security");

        final I18nHelper mockI18nBean = mock(I18nHelper.class);
        when(mockI18nBean.getText("gadget.filterstats.component.nocomponent")).thenReturn("No components!");

        final PieSegmentWrapper pieSegmentWrapper = new PieSegmentWrapper(mock, mockI18nBean, "components", null, null);
        assertThat(pieSegmentWrapper.getKey(), equalTo(mock));
        assertThat(pieSegmentWrapper.getName(), equalTo("Security"));
        assertThat(pieSegmentWrapper.toString(), equalTo("Security"));

        final PieSegmentWrapper nullWrapper = new PieSegmentWrapper(null, mockI18nBean, "components", null, null);
        assertThat(nullWrapper.getKey(), equalTo(null));
        assertThat(nullWrapper.getName(), equalTo("No components!"));
        assertThat(nullWrapper.toString(), equalTo("No components!"));
    }

    @Test
    public void testVersion() {
        final Version mockVersion = mock(Version.class);
        when(mockVersion.getName()).thenReturn("Version 1");

        final I18nHelper mockI18nBean = mock(I18nHelper.class);
        when(mockI18nBean.getText("gadget.filterstats.raisedin.unscheduled")).thenReturn("Unscheduled");

        final PieSegmentWrapper pieSegmentWrapper = new PieSegmentWrapper(mockVersion, mockI18nBean, "version", null, null);
        assertThat(pieSegmentWrapper.getKey(), equalTo(mockVersion));
        assertThat(pieSegmentWrapper.getName(), equalTo("Version 1"));
        assertThat(pieSegmentWrapper.toString(), equalTo("Version 1"));

        final PieSegmentWrapper allWrapper = new PieSegmentWrapper(mockVersion, mockI18nBean, "allVersion", null, null);
        assertThat(allWrapper.getKey(), equalTo(mockVersion));
        assertThat(allWrapper.getName(), equalTo("Version 1"));
        assertThat(allWrapper.toString(), equalTo("Version 1"));

        final PieSegmentWrapper nullWrapper = new PieSegmentWrapper(null, mockI18nBean, "version", null, null);
        assertThat(nullWrapper.getKey(), equalTo(null));
        assertThat(nullWrapper.getName(), equalTo("Unscheduled"));
        assertThat(nullWrapper.toString(), equalTo("Unscheduled"));
    }

    @Test
    public void testFixForVersion() {
        final Version mockVersion = mock(Version.class);
        when(mockVersion.getName()).thenReturn("Fix Version 1");

        final I18nHelper mockI18nBean = mock(I18nHelper.class);
        when(mockI18nBean.getText("gadget.filterstats.fixfor.unscheduled")).thenReturn("Unscheduled Fix");

        final PieSegmentWrapper pieSegmentWrapper = new PieSegmentWrapper(mockVersion, mockI18nBean, "fixfor", null, null);
        assertThat(pieSegmentWrapper.getKey(), equalTo(mockVersion));
        assertThat(pieSegmentWrapper.getName(), equalTo("Fix Version 1"));
        assertThat(pieSegmentWrapper.toString(), equalTo("Fix Version 1"));

        final PieSegmentWrapper allWrapper = new PieSegmentWrapper(mockVersion, mockI18nBean, "allFixfor", null, null);
        assertThat(allWrapper.getKey(), equalTo(mockVersion));
        assertThat(allWrapper.getName(), equalTo("Fix Version 1"));
        assertThat(allWrapper.toString(), equalTo("Fix Version 1"));

        final PieSegmentWrapper nullWrapper = new PieSegmentWrapper(null, mockI18nBean, "fixfor", null, null);
        assertThat(nullWrapper.getKey(), equalTo(null));
        assertThat(nullWrapper.getName(), equalTo("Unscheduled Fix"));
        assertThat(nullWrapper.toString(), equalTo("Unscheduled Fix"));
    }

    @Test
    public void testCustomField() {
        final String customFieldValue = "theValue";

        final CustomField mockCustomField = mock(CustomField.class);

        final CustomFieldSearcherModuleDescriptor mockCustomFieldSearcherModuleDescriptor = mock(CustomFieldSearcherModuleDescriptor.class);
        when(mockCustomFieldSearcherModuleDescriptor.getStatHtml(mockCustomField, customFieldValue, null)).thenReturn("<html>theValue</html>");

        final CustomFieldSearcher mockCustomFieldSearcher = mock(CustomFieldSearcher.class);
        final CustomFieldManager mockCustomFieldManager = mock(CustomFieldManager.class);
        when(mockCustomFieldManager.getCustomFieldObject("customfield_1025")).thenReturn(mockCustomField);
        when(mockCustomField.getCustomFieldSearcher()).thenReturn(mockCustomFieldSearcher);
        when(mockCustomFieldSearcher.getDescriptor()).thenReturn(mockCustomFieldSearcherModuleDescriptor);

        final I18nHelper mockI18nBean = mock(I18nHelper.class);
        when(mockI18nBean.getText("common.words.none")).thenReturn("None");

        final PieSegmentWrapper pieSegmentWrapper = new PieSegmentWrapper(customFieldValue, mockI18nBean, "customfield_1025", null, mockCustomFieldManager);
        assertThat(pieSegmentWrapper.getKey(), equalTo(customFieldValue));
        assertThat(pieSegmentWrapper.getName(), equalTo("<html>theValue</html>"));
        assertThat(pieSegmentWrapper.toString(), equalTo("<html>theValue</html>"));

        final PieSegmentWrapper nullWrapper = new PieSegmentWrapper(null, mockI18nBean, "customfield_1023", null, mockCustomFieldManager);
        assertThat(nullWrapper.getKey(), equalTo(null));
        assertThat(nullWrapper.getName(), equalTo("None"));
        assertThat(nullWrapper.toString(), equalTo("None"));
    }

    private Project createProjectMock(final String name, final long id) {
        return new MockProject(id, "KEY", name);
    }
}
