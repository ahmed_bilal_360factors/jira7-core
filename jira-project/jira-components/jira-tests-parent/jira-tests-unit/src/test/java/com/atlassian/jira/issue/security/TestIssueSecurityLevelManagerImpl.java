package com.atlassian.jira.issue.security;

import com.atlassian.cache.CacheManager;
import com.atlassian.cache.memory.MemoryCacheManager;
import com.atlassian.core.util.map.EasyMap;
import com.atlassian.jira.database.QueryDslAccessor;
import com.atlassian.jira.entity.EntityEngineImpl;
import com.atlassian.jira.issue.IssueFactory;
import com.atlassian.jira.issue.MockIssueFactory;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.mock.ofbiz.MockGenericValue;
import com.atlassian.jira.mock.ofbiz.MockOfBizDelegator;
import com.atlassian.jira.ofbiz.FieldMap;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.scheme.DefaultSchemeFactory;
import com.atlassian.jira.scheme.Scheme;
import com.atlassian.jira.security.MockIssueSecurityTypeManager;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.SecurityTypeManager;
import com.atlassian.jira.security.type.SecurityType;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.MockUserKeyService;
import com.atlassian.jira.user.UserKeyService;
import com.atlassian.jira.user.util.MockUserManager;
import com.atlassian.jira.util.collect.CollectionBuilder;
import com.atlassian.jira.util.collect.MapBuilder;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.mockito.Mock;
import org.ofbiz.core.entity.GenericEntityException;
import org.ofbiz.core.entity.GenericValue;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static com.atlassian.jira.permission.ProjectPermissions.BROWSE_PROJECTS;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

/**
 * @since v3.13
 */
public class TestIssueSecurityLevelManagerImpl {
    @Rule
    public final RuleChain mockitoMocks = MockitoMocksInContainer.forTest(this);

    @Mock
    private ProjectManager mockProjectManager;
    @Mock
    @AvailableInContainer
    private IssueSecuritySchemeManager mockIssueSecuritySchemeManager;
    @Mock
    private SecurityType mockReporterSecurityType;
    @Mock
    private SecurityType mockGroupSecurityType;
    @Mock
    private SecurityType mockAssigneeSecurityType;
    @Mock
    private SecurityType mockLeadSecurityType;
    @Mock
    private PermissionManager mockPermissionManager;
    @Mock
    private SecurityTypeManager securityTypeManager;
    @Mock
    @AvailableInContainer
    private IssueFactory issueFactory;
    @Mock
    private QueryDslAccessor queryDslAccessor;

    private CacheManager cacheManager;

    private MockUserManager mockUserManager = new MockUserManager();
    private UserKeyService mockUserKeyService = new MockUserKeyService();
    private ApplicationUser userFred = new MockApplicationUser("fred");

    @Before
    public void setUp() {
        cacheManager = new MemoryCacheManager();
    }

    @Test
    public void testGetUsersSecurityLevels_ProjectFirst() throws Exception {
        mockUserManager.addUser(userFred);

        Project project = new MockProject(1, "HSP");
        GenericValue projectGV = project.getGenericValue();

        MockIssueFactory.setProjectManager(mockProjectManager);
        when(mockProjectManager.getProjectObj(1L)).thenReturn(project);
        MutableIssue issue = MockIssueFactory.createIssue(12, "HSP-12", 1);
        when(issueFactory.getIssue(issue.getGenericValue())).thenReturn(issue);

        reset(mockProjectManager);
        when(mockProjectManager.getProjectObj(1L)).thenReturn(project);
        when(mockProjectManager.getProjectObj(issue.getProjectId())).thenReturn(new MockProject(projectGV));

        issue.setProjectObject(project);

        MockGenericValue schemeGV = new MockGenericValue("IssueSecurityScheme");
        // Mock IssueSecuritySchemeManager
        when(mockIssueSecuritySchemeManager.getEntities(schemeGV)).thenReturn(ImmutableList.of(
                createSecurityLevelPermission("reporter", null, 100),
                createSecurityLevelPermission("group", "dudes", 101),
                createSecurityLevelPermission("reporter", null, 102),
                createSecurityLevelPermission("group", "dudes", 102)
        ));

        Scheme scheme1 = new DefaultSchemeFactory().getScheme(schemeGV);
        reset(mockIssueSecuritySchemeManager);
        when(mockIssueSecuritySchemeManager.getSchemeFor(project)).thenReturn(scheme1);

        when(mockReporterSecurityType.hasPermission(project, null, userFred, false)).thenReturn(true);
        when(mockGroupSecurityType.hasPermission(project, "dudes", userFred, false)).thenReturn(false);

        final SecurityTypeManager securityTypeManager = new MockIssueSecurityTypeManager(null);
        securityTypeManager.setSecurityTypes(EasyMap.build(
                "reporter", mockReporterSecurityType,
                "group", mockGroupSecurityType
        ));

        IssueSecurityLevelManagerImpl issueSecurityLevelManagerImpl = new IssueSecurityLevelManagerImpl(mockIssueSecuritySchemeManager,
                securityTypeManager, mockProjectManager, mockUserManager, mockUserKeyService, null, new EntityEngineImpl(new MockOfBizDelegator()),
                cacheManager, queryDslAccessor) {
            @Override
            public GenericValue getIssueSecurityLevel(final Long id) {
                return new MockGenericValue("IssueSecurityLevel", FieldMap.build("id", id));
            }
        };

        List securityLevels = issueSecurityLevelManagerImpl.getUsersSecurityLevels(projectGV, userFred);
        // Doing a project-level search should always include security levels with "reporter" permission.
        assertEquals(2, securityLevels.size());
        assertTrue(securityLevels.contains(new MockGenericValue("IssueSecurityLevel", FieldMap.build("id", new Long(100)))));
        assertTrue(securityLevels.contains(new MockGenericValue("IssueSecurityLevel", FieldMap.build("id", new Long(102)))));

        // Now, if we call this again with an issue GV, then we should not have cached the project values:
        securityLevels = issueSecurityLevelManagerImpl.getUsersSecurityLevels(issue.getGenericValue(), userFred);
        // fred is not the reporter, so this should return nothing
        assertEquals(0, securityLevels.size());
        verify(mockReporterSecurityType, times(2)).hasPermission(issue, null, userFred, false);
        verify(mockGroupSecurityType, times(2)).hasPermission(issue, "dudes", userFred, false);

        // Do again so they come from the cache
        securityLevels = issueSecurityLevelManagerImpl.getUsersSecurityLevels(projectGV, userFred);
        // Doing a project-level search should always include security levels with "reporter" permission.
        assertEquals(2, securityLevels.size());
        assertTrue(securityLevels.contains(new MockGenericValue("IssueSecurityLevel", FieldMap.build("id", new Long(100)))));
        assertTrue(securityLevels.contains(new MockGenericValue("IssueSecurityLevel", FieldMap.build("id", new Long(102)))));
        verify(mockReporterSecurityType, times(2)).hasPermission(project, null, userFred, false);
        verify(mockGroupSecurityType).hasPermission(project, "dudes", userFred, false);

        // Now, if we call this again with an issue GV, then we should not use the cached the project values:
        securityLevels = issueSecurityLevelManagerImpl.getUsersSecurityLevels(issue.getGenericValue(), userFred);
        // fred is not the reporter, so this should return nothing
        assertEquals(0, securityLevels.size());
        verify(mockReporterSecurityType, times(4)).hasPermission(issue, null, userFred, false);
        verify(mockGroupSecurityType, times(4)).hasPermission(issue, "dudes", userFred, false);
    }

    @Test
    public void testGetUsersSecurityLevels_IssueFirst() throws Exception {
        mockUserManager.addUser(userFred);

        // Mock ProjectManager
        Project project = new MockProject(1, "HSP");
        when(mockProjectManager.getProjectObj(1L)).thenReturn(project);

        MockIssueFactory.setProjectManager(mockProjectManager);
        MutableIssue issue = MockIssueFactory.createIssue(12, "HSP-12", 1);
        when(issueFactory.getIssue(issue.getGenericValue())).thenReturn(issue);

        GenericValue projectGV = new MockProject(1).getGenericValue();
        reset(mockProjectManager);

        when(mockProjectManager.getProjectObj(issue.getProjectId())).thenReturn(new MockProject(projectGV));
        when(mockProjectManager.getProjectObj(1L)).thenReturn(project);

        MockGenericValue schemeGV = new MockGenericValue("IssueSecurityScheme", 10L);
        when(mockIssueSecuritySchemeManager.getEntities(schemeGV)).thenReturn(ImmutableList.of(
                createSecurityLevelPermission("reporter", null, 100),
                createSecurityLevelPermission("group", "dudes", 101),
                createSecurityLevelPermission("reporter", null, 102),
                createSecurityLevelPermission("group", "dudes", 102)
        ));
        Scheme scheme1 = new DefaultSchemeFactory().getScheme(schemeGV);
        reset(mockIssueSecuritySchemeManager);
        when(mockIssueSecuritySchemeManager.getSchemeFor(project)).thenReturn(scheme1);

        when(mockReporterSecurityType.hasPermission(project, null, userFred, false)).thenReturn(true);
        when(mockGroupSecurityType.hasPermission(project, "dudes", userFred, false)).thenReturn(false);

        final SecurityTypeManager securityTypeManager = new MockIssueSecurityTypeManager(null);
        securityTypeManager.setSecurityTypes(EasyMap.build(
                "reporter", mockReporterSecurityType,
                "group", mockGroupSecurityType
        ));

        IssueSecurityLevelManagerImpl issueSecurityLevelManagerImpl = new IssueSecurityLevelManagerImpl(mockIssueSecuritySchemeManager,
                securityTypeManager, mockProjectManager, mockUserManager, mockUserKeyService, null, new EntityEngineImpl(new MockOfBizDelegator()),
                cacheManager, queryDslAccessor) {
            public GenericValue getIssueSecurityLevel(final Long id) {
                return new MockGenericValue("IssueSecurityLevel", FieldMap.build("id", id));
            }
        };

        List securityLevels = issueSecurityLevelManagerImpl.getUsersSecurityLevels(issue.getGenericValue(), userFred);
        // Doing an issue-level search should give no levels
        assertEquals(0, securityLevels.size());
        verify(mockReporterSecurityType, times(2)).hasPermission(issue, null, userFred, false);
        verify(mockGroupSecurityType, times(2)).hasPermission(issue, "dudes", userFred, false);

        // Now, if we call this again with a project, then we should not have cached the issue values:
        securityLevels = issueSecurityLevelManagerImpl.getUsersSecurityLevels(projectGV, userFred);
        assertEquals(2, securityLevels.size());
        assertTrue(securityLevels.contains(new MockGenericValue("IssueSecurityLevel", FieldMap.build("id", 100L))));
        assertTrue(securityLevels.contains(new MockGenericValue("IssueSecurityLevel", FieldMap.build("id", 102L))));
        verify(mockReporterSecurityType, times(2)).hasPermission(project, null, userFred, false);
        verify(mockGroupSecurityType).hasPermission(project, "dudes", userFred, false);
    }

    @Test
    public void testGetUsersSecurityLevels_ProjectObjectFirst() throws Exception {
        mockUserManager.addUser(userFred);

        Project project = new MockProject(1, "HSP");

        MockIssueFactory.setProjectManager(mockProjectManager);
        when(mockProjectManager.getProjectObj(1L)).thenReturn(project);
        MutableIssue issue = MockIssueFactory.createIssue(12, "HSP-12", 1);

        reset(mockProjectManager);
        when(mockProjectManager.getProjectObj(1L)).thenReturn(project);

        issue.setProjectObject(project);

        MockGenericValue schemeGV = new MockGenericValue("IssueSecurityScheme");
        // Mock IssueSecuritySchemeManager
        when(mockIssueSecuritySchemeManager.getEntities(schemeGV)).thenReturn(ImmutableList.of(
                createSecurityLevelPermission("reporter", null, 100),
                createSecurityLevelPermission("group", "dudes", 101),
                createSecurityLevelPermission("reporter", null, 102),
                createSecurityLevelPermission("group", "dudes", 102)
        ));

        Scheme scheme1 = new DefaultSchemeFactory().getScheme(schemeGV);
        reset(mockIssueSecuritySchemeManager);
        when(mockIssueSecuritySchemeManager.getSchemeFor(project)).thenReturn(scheme1);

        when(mockReporterSecurityType.hasPermission(project, null, userFred, false)).thenReturn(true);
        when(mockGroupSecurityType.hasPermission(project, "dudes", userFred, false)).thenReturn(false);

        final SecurityTypeManager securityTypeManager = new MockIssueSecurityTypeManager(null);
        securityTypeManager.setSecurityTypes(EasyMap.build(
                "reporter", mockReporterSecurityType,
                "group", mockGroupSecurityType
        ));

        IssueSecurityLevelManagerImpl issueSecurityLevelManagerImpl = new IssueSecurityLevelManagerImpl(mockIssueSecuritySchemeManager, securityTypeManager, mockProjectManager, mockUserManager,
                mockUserKeyService, null, new EntityEngineImpl(new MockOfBizDelegator()), cacheManager, queryDslAccessor) {
            @Override
            public GenericValue getIssueSecurityLevel(final Long id) {
                return new MockGenericValue("IssueSecurityLevel", FieldMap.build("id", id));
            }
        };

        List securityLevels = issueSecurityLevelManagerImpl.getUsersSecurityLevels(project, userFred);
        // Doing a project-level search should always include security levels with "reporter" permission.
        assertEquals(2, securityLevels.size());
        assertTrue(securityLevels.contains(new IssueSecurityLevelImpl(100L, "reporter", null, 100L)));
        assertTrue(securityLevels.contains(new IssueSecurityLevelImpl(102L, "group", "dudes", 102L)));

        // Now, if we call this again with an issue GV, then we should not have cached the project values:
        securityLevels = issueSecurityLevelManagerImpl.getUsersSecurityLevels(issue, userFred);
        // fred is not the reporter, so this should return nothing
        assertEquals(0, securityLevels.size());
        verify(mockReporterSecurityType, times(2)).hasPermission(issue, null, userFred, false);
        verify(mockGroupSecurityType, times(2)).hasPermission(issue, "dudes", userFred, false);

        // Do again so they come from the cache
        securityLevels = issueSecurityLevelManagerImpl.getUsersSecurityLevels(project, userFred);
        // Doing a project-level search should always include security levels with "reporter" permission.
        assertEquals(2, securityLevels.size());
        assertTrue(securityLevels.contains(new IssueSecurityLevelImpl(100L, "reporter", null, 100L)));
        assertTrue(securityLevels.contains(new IssueSecurityLevelImpl(102L, "group", "dudes", 102L)));
        verify(mockReporterSecurityType, times(2)).hasPermission(project, null, userFred, false);
        verify(mockGroupSecurityType).hasPermission(project, "dudes", userFred, false);

        // Now, if we call this again with an issue GV, then we should not use the cached the project values:
        securityLevels = issueSecurityLevelManagerImpl.getUsersSecurityLevels(issue, userFred);
        // fred is not the reporter, so this should return nothing
        assertEquals(0, securityLevels.size());
        verify(mockReporterSecurityType, times(4)).hasPermission(issue, null, userFred, false);
        verify(mockGroupSecurityType, times(4)).hasPermission(issue, "dudes", userFred, false);
    }

    @Test
    public void testGetUsersSecurityLevels_IssueObjectFirst() throws Exception {
        mockUserManager.addUser(userFred);

        // Mock ProjectManager
        Project project = new MockProject(1, "HSP");
        when(mockProjectManager.getProjectObj(1L)).thenReturn(project);

        MockIssueFactory.setProjectManager(mockProjectManager);
        MutableIssue issue = MockIssueFactory.createIssue(12, "HSP-12", 1);

        reset(mockProjectManager);

        when(mockProjectManager.getProjectObj(1L)).thenReturn(project);

        MockGenericValue schemeGV = new MockGenericValue("IssueSecurityScheme", 10L);
        when(mockIssueSecuritySchemeManager.getEntities(schemeGV)).thenReturn(ImmutableList.of(
                createSecurityLevelPermission("reporter", null, 100),
                createSecurityLevelPermission("group", "dudes", 101),
                createSecurityLevelPermission("reporter", null, 102),
                createSecurityLevelPermission("group", "dudes", 102)
        ));
        Scheme scheme1 = new DefaultSchemeFactory().getScheme(schemeGV);
        reset(mockIssueSecuritySchemeManager);
        when(mockIssueSecuritySchemeManager.getSchemeFor(project)).thenReturn(scheme1);

        when(mockReporterSecurityType.hasPermission(project, null, userFred, false)).thenReturn(true);
        when(mockGroupSecurityType.hasPermission(project, "dudes", userFred, false)).thenReturn(false);

        final SecurityTypeManager securityTypeManager = new MockIssueSecurityTypeManager(null);
        securityTypeManager.setSecurityTypes(EasyMap.build(
                "reporter", mockReporterSecurityType,
                "group", mockGroupSecurityType
        ));

        IssueSecurityLevelManagerImpl issueSecurityLevelManagerImpl = new IssueSecurityLevelManagerImpl(mockIssueSecuritySchemeManager, securityTypeManager, mockProjectManager,
                mockUserManager, mockUserKeyService, null, new EntityEngineImpl(new MockOfBizDelegator()), cacheManager, queryDslAccessor) {
            public GenericValue getIssueSecurityLevel(final Long id) {
                return new MockGenericValue("IssueSecurityLevel", FieldMap.build("id", id));
            }
        };

        List securityLevels = issueSecurityLevelManagerImpl.getUsersSecurityLevels(issue, userFred);
        // Doing an issue-level search should give no levels
        assertEquals(0, securityLevels.size());
        verify(mockReporterSecurityType, times(2)).hasPermission(issue, null, userFred, false);
        verify(mockGroupSecurityType, times(2)).hasPermission(issue, "dudes", userFred, false);

        // Now, if we call this again with a project, then we should not have cached the issue values:
        securityLevels = issueSecurityLevelManagerImpl.getUsersSecurityLevels(project, userFred);
        assertEquals(2, securityLevels.size());
        assertTrue(securityLevels.contains(new IssueSecurityLevelImpl(100L, "reporter", null, 100L)));
        assertTrue(securityLevels.contains(new IssueSecurityLevelImpl(102L, "group", "dudes", 102L)));
        verify(mockReporterSecurityType, times(2)).hasPermission(project, null, userFred, false);
        verify(mockGroupSecurityType).hasPermission(project, "dudes", userFred, false);
    }

    @Test
    public void testGetAllUsersSecurityLevels() throws Exception {
        mockUserManager.addUser(userFred);

        MockProject project1 = new MockProject(1);
        MockProject project2 = new MockProject(2);

        MockGenericValue schemeGV1 = new MockGenericValue("IssueSecurityScheme", 10L);
        MockGenericValue schemeGV2 = new MockGenericValue("IssueSecurityScheme", 11L);

        // Mock IssueSecuritySchemeManager
        when(mockIssueSecuritySchemeManager.getEntities(schemeGV1)).thenReturn(ImmutableList.of(
                createSecurityLevelPermission("reporter", null, 100),
                createSecurityLevelPermission("group", "dudes", 101),
                createSecurityLevelPermission("reporter", null, 102),
                createSecurityLevelPermission("group", "dudes", 102)
        ));

        when(mockIssueSecuritySchemeManager.getEntities(schemeGV2)).thenReturn(ImmutableList.of(
                createSecurityLevelPermission("assignee", null, 103),
                createSecurityLevelPermission("lead", null, 104)
        ));
        Scheme scheme1 = new DefaultSchemeFactory().getScheme(schemeGV1);
        Scheme scheme2 = new DefaultSchemeFactory().getScheme(schemeGV2);
        reset(mockIssueSecuritySchemeManager);

        when(mockPermissionManager.getProjects(BROWSE_PROJECTS, userFred)).thenReturn(ImmutableList.of(project1, project2));
        when(mockProjectManager.getProjectObj(1L)).thenReturn(project1);
        when(mockIssueSecuritySchemeManager.getSchemeFor(project1)).thenReturn(scheme1);
        when(mockProjectManager.getProjectObj(2L)).thenReturn(project2);
        when(mockIssueSecuritySchemeManager.getSchemeFor(project2)).thenReturn(scheme2);

        when(mockReporterSecurityType.hasPermission(project1, null, userFred, false)).thenReturn(true);
        when(mockGroupSecurityType.hasPermission(project1, "dudes", userFred, false)).thenReturn(false);
        when(mockAssigneeSecurityType.hasPermission(project2, null, userFred, false)).thenReturn(true);
        when(mockLeadSecurityType.hasPermission(project2, null, userFred, false)).thenReturn(true);

        final SecurityTypeManager securityTypeManager = new MockIssueSecurityTypeManager(null);
        Map<String, SecurityType> objectObjectMap = ImmutableMap.<String, SecurityType>builder()
                .put("reporter", mockReporterSecurityType)
                .put("group", mockGroupSecurityType)
                .put("assignee", mockAssigneeSecurityType)
                .put("lead", mockLeadSecurityType)
                .build();
        securityTypeManager.setSecurityTypes(objectObjectMap);

        IssueSecurityLevelManagerImpl issueSecurityLevelManagerImpl = new IssueSecurityLevelManagerImpl(mockIssueSecuritySchemeManager,
                securityTypeManager, mockProjectManager, mockUserManager, mockUserKeyService, mockPermissionManager,
                new EntityEngineImpl(new MockOfBizDelegator()), cacheManager, queryDslAccessor) {
            public GenericValue getIssueSecurityLevel(final Long id) {
                return createMockSecurityLevel(id, null);
            }
        };

        Collection<GenericValue> securityLevels = issueSecurityLevelManagerImpl.getAllUsersSecurityLevels(userFred);
        assertEquals(4, securityLevels.size());
        assertTrue(securityLevels.contains(createMockSecurityLevel(100L, null)));
        assertTrue(securityLevels.contains(createMockSecurityLevel(102L, null)));
        assertTrue(securityLevels.contains(createMockSecurityLevel(103L, null)));
        assertTrue(securityLevels.contains(createMockSecurityLevel(104L, null)));
        verify(mockReporterSecurityType, times(2)).hasPermission(project1, null, userFred, false);
        verify(mockGroupSecurityType).hasPermission(project1, "dudes", userFred, false);
        verify(mockAssigneeSecurityType).hasPermission(project2, null, userFred, false);
        verify(mockLeadSecurityType).hasPermission(project2, null, userFred, false);

        // Do this again. This time the should come from the cache
        securityLevels = issueSecurityLevelManagerImpl.getAllUsersSecurityLevels(userFred);
        assertEquals(4, securityLevels.size());
        assertTrue(securityLevels.contains(createMockSecurityLevel(100L, null)));
        assertTrue(securityLevels.contains(createMockSecurityLevel(102L, null)));
        assertTrue(securityLevels.contains(createMockSecurityLevel(103L, null)));
        assertTrue(securityLevels.contains(createMockSecurityLevel(104L, null)));
        verifyNoMoreInteractions(mockReporterSecurityType);
        verifyNoMoreInteractions(mockGroupSecurityType);
        verifyNoMoreInteractions(mockAssigneeSecurityType);
        verifyNoMoreInteractions(mockLeadSecurityType);
    }

    @Test
    public void testGetUsersSecurityLevelsByName() throws Exception {
        mockUserManager.addUser(userFred);

        final SecurityTypeManager securityTypeManager = new MockIssueSecurityTypeManager(null);

        final MockGenericValue mock100 = createMockSecurityLevel(100L, "Level 1");
        final MockGenericValue mock101 = createMockSecurityLevel(101L, "Level 2");
        final MockGenericValue mock102 = createMockSecurityLevel(102L, "Level 3");
        final MockGenericValue mock103 = createMockSecurityLevel(103L, "Level 1");
        final List<GenericValue> usersLevels = ImmutableList.of(mock100, mock101, mock102, mock103);

        IssueSecurityLevelManagerImpl issueSecurityLevelManagerImpl = new IssueSecurityLevelManagerImpl(mockIssueSecuritySchemeManager,
                securityTypeManager, mockProjectManager, mockUserManager, mockUserKeyService, mockPermissionManager,
                new EntityEngineImpl(new MockOfBizDelegator()), cacheManager, queryDslAccessor) {
            @Override
            public Collection<GenericValue> getAllUsersSecurityLevels(final ApplicationUser user) throws GenericEntityException {
                return usersLevels;
            }
        };

        Collection<GenericValue> securityLevels = issueSecurityLevelManagerImpl.getUsersSecurityLevelsByName(userFred, "Level 1");
        assertEquals(2, securityLevels.size());
        assertTrue(securityLevels.contains(mock100));
        assertTrue(securityLevels.contains(mock103));
    }

    @Test
    public void testGetSecurityLevelsByName() throws Exception {
        final SecurityTypeManager securityTypeManager = new MockIssueSecurityTypeManager(null);

        final MockGenericValue mock100 = createMockSecurityLevel(100L, "Level 1");
        final MockGenericValue mock101 = createMockSecurityLevel(101L, "Level 2");
        final MockGenericValue mock102 = createMockSecurityLevel(102L, "Level 3");
        final MockGenericValue mock103 = createMockSecurityLevel(103L, "Level 1");
        final List<GenericValue> allLevels = ImmutableList.of(mock100, mock101, mock102, mock103);


        IssueSecurityLevelManagerImpl issueSecurityLevelManagerImpl = new IssueSecurityLevelManagerImpl(mockIssueSecuritySchemeManager, securityTypeManager, mockProjectManager, mockUserManager, mockUserKeyService, mockPermissionManager,
                new EntityEngineImpl(new MockOfBizDelegator()), cacheManager, queryDslAccessor) {
            @Override
            public Collection<GenericValue> getAllSecurityLevels() throws GenericEntityException {
                return allLevels;
            }
        };

        Collection<GenericValue> securityLevels = issueSecurityLevelManagerImpl.getSecurityLevelsByName("Level 1");
        assertEquals(2, securityLevels.size());
        assertTrue(securityLevels.contains(mock100));
        assertTrue(securityLevels.contains(mock103));
    }

    @Test
    public void testGetAllSecurityLevels() throws Exception {
        final Long inputSchemeId = 5555L;
        when(mockIssueSecuritySchemeManager.getSchemes())
                .thenReturn(CollectionBuilder.<GenericValue>newBuilder(createMockScheme(inputSchemeId)).asList());

        IssueSecurityLevelManagerImpl issueSecurityLevelManagerImpl = new IssueSecurityLevelManagerImpl(mockIssueSecuritySchemeManager, securityTypeManager, mockProjectManager, mockUserManager, mockUserKeyService, mockPermissionManager,
                new EntityEngineImpl(new MockOfBizDelegator()), cacheManager, queryDslAccessor) {
            @Override
            public List<GenericValue> getSchemeIssueSecurityLevels(final Long schemeId) {
                assertEquals(inputSchemeId, schemeId);
                return Collections.<GenericValue>singletonList(createMockSecurityLevel(44L, "Level 1"));
            }
        };

        final Collection<GenericValue> result = issueSecurityLevelManagerImpl.getAllSecurityLevels();
        assertEquals(1, result.size());
        assertEquals(new Long(44L), result.iterator().next().getLong("id"));
    }

    private MockGenericValue createMockScheme(final Long id) {
        return new MockGenericValue("Scheme", MapBuilder.<String, Object>newBuilder().add("id", id).toMap());
    }

    private MockGenericValue createMockSecurityLevel(final Long id, final String name) {
        return new MockGenericValue("IssueSecurityLevel", MapBuilder.<String, Object>newBuilder().add("id", id).add("name", name).toMap());
    }

    private GenericValue createSecurityLevelPermission(String type, String parameter, long securityLevelID) {
        GenericValue securityLevelPermissionGV = new MockGenericValue("SchemeIssueSecurities");
        securityLevelPermissionGV.set("type", type);
        securityLevelPermissionGV.set("parameter", parameter);
        securityLevelPermissionGV.set("security", securityLevelID);
        return securityLevelPermissionGV;
    }
}
