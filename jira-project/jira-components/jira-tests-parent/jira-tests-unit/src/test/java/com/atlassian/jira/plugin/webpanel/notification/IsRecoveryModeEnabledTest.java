package com.atlassian.jira.plugin.webpanel.notification;

import com.atlassian.jira.user.util.StaticRecoveryMode;
import com.google.common.collect.ImmutableMap;
import org.hamcrest.Matchers;
import org.junit.Test;

import static org.junit.Assert.assertThat;

public class IsRecoveryModeEnabledTest {
    @Test
    public void conditionReturnsTrueWhenRecoveryModeEnabled() {
        //given
        final StaticRecoveryMode enabled = StaticRecoveryMode.enabled("recovery_user");
        final IsRecoveryModeEnabled isRecoveryModeEnabled = new IsRecoveryModeEnabled(enabled);

        //when
        final boolean display = isRecoveryModeEnabled.shouldDisplay(ImmutableMap.of());

        //then
        assertThat(display, Matchers.equalTo(true));
    }

    @Test
    public void conditionReturnsFalseWhenRecoveryModeDisabled() {
        //given
        final StaticRecoveryMode disabled = StaticRecoveryMode.disabled();
        final IsRecoveryModeEnabled isRecoveryModeEnabled = new IsRecoveryModeEnabled(disabled);

        //when
        final boolean display = isRecoveryModeEnabled.shouldDisplay(ImmutableMap.of());

        //then
        assertThat(display, Matchers.equalTo(false));
    }
}