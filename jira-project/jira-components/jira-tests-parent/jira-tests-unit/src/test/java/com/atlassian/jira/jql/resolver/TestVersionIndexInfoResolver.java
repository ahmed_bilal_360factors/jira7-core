package com.atlassian.jira.jql.resolver;

import com.atlassian.jira.mock.project.MockVersion;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.util.collect.CollectionBuilder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Collections;
import java.util.List;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.empty;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestVersionIndexInfoResolver {
    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Mock
    private NameResolver<Version> nameResolver;

    @InjectMocks
    private VersionIndexInfoResolver resolver;

    @Test
    public void testGetIndexedValuesStringHappyPath() throws Exception {
        when(nameResolver.getIdsFromName("version1")).thenReturn(CollectionBuilder.newBuilder("1", "2").asList());

        final List<String> result = resolver.getIndexedValues("version1");

        assertThat(result, containsInAnyOrder("1", "2"));
    }

    @Test
    public void testGetIndexedValuesStringIsId() throws Exception {
        when(nameResolver.getIdsFromName("2")).thenReturn(Collections.emptyList());
        when(nameResolver.idExists(2L)).thenReturn(true);

        final List<String> result = resolver.getIndexedValues("2");

        assertThat(result, contains("2"));
    }

    @Test
    public void testGetIndexedValuesStringIsIdDoesntExist() throws Exception {
        when(nameResolver.getIdsFromName("2")).thenReturn(Collections.emptyList());
        when(nameResolver.idExists(2L)).thenReturn(false);

        final List<String> result = resolver.getIndexedValues("2");

        assertThat(result, empty());
    }

    @Test
    public void testGetIndexedValuesStringIsNotNameOrId() throws Exception {
        when(nameResolver.getIdsFromName("abc")).thenReturn(Collections.emptyList());

        final List<String> result = resolver.getIndexedValues("abc");

        assertThat(result, empty());
    }

    @Test
    public void testGetIndexedValuesLongExists() throws Exception {
        when(nameResolver.idExists(2L)).thenReturn(true);

        final List<String> result = resolver.getIndexedValues(2L);

        assertThat(result, contains("2"));
    }

    @Test
    public void testGetIndexedValuesLongIdIsName() throws Exception {
        when(nameResolver.idExists(100L)).thenReturn(false);
        when(nameResolver.getIdsFromName("100")).thenReturn(CollectionBuilder.newBuilder("2", "1").asList());

        final List<String> result = resolver.getIndexedValues(100L);

        assertThat(result, containsInAnyOrder("1", "2"));
    }

    @Test
    public void testGetIndexedValuesLongIdIsNameDoesntExist() throws Exception {
        when(nameResolver.idExists(100L)).thenReturn(false);
        when(nameResolver.getIdsFromName("100")).thenReturn(Collections.emptyList());

        final List<String> result = resolver.getIndexedValues(100L);

        assertThat(result, empty());
    }

    @Test
    public void testGetIndexedValue() throws Exception {
        final MockVersion mockVersion1 = new MockVersion(1L, "Version 1");

        final String indexedValue = resolver.getIndexedValue(mockVersion1);

        assertEquals("1", indexedValue);
    }

    @Test
    public void testGetIndexedValueDoesNotAcceptNull() {
        exception.expect(IllegalArgumentException.class);
        resolver.getIndexedValue(null);
    }
}
