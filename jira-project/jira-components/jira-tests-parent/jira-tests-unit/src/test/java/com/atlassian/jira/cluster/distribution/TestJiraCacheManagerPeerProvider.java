package com.atlassian.jira.cluster.distribution;

import com.atlassian.jira.cluster.ClusterManager;
import com.atlassian.jira.cluster.Node;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoContainer;
import com.google.common.collect.ImmutableSet;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Ehcache;
import net.sf.ehcache.distribution.CachePeer;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;

import java.net.MalformedURLException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestJiraCacheManagerPeerProvider {

    @Rule
    public MockitoContainer mockitoContainer = new MockitoContainer(this);

    @Mock
    private CacheManager cacheManager;

    @Mock
    @AvailableInContainer
    private ClusterManager clusterManager;

    private JiraCacheManagerPeerProvider provider;

    @Mock
    private Ehcache ehcache;

    private CountDownLatch countDownLatch;
    private ExecutorService executorService;

    @Before
    public void setUp() throws Exception {
        executorService = Executors.newFixedThreadPool(10);
        provider = new CountdownBarrierFixture(cacheManager, executorService);
        initLiveNodes();
    }

    @After
    public void tearDown() throws InterruptedException {
        executorService.shutdown();
        executorService.awaitTermination(10, TimeUnit.SECONDS);
    }

    @Test
    public void listAllPeersExceptMe() throws RemoteException {
        final List<CachePeer> list = provider.listRemoteCachePeers(ehcache);
        assertThat(list, hasSize(3));
        assertThat(list.get(0).getUrl(), is("//127.0.0.1:44/cacheName"));
    }

    @Test(timeout = 10_000)
    public void runLookupConcurrently() throws RemoteException {
        countDownLatch = new CountDownLatch(3);

        final List<CachePeer> list = provider.listRemoteCachePeers(ehcache);
        assertThat(list, hasSize(3));
        assertThat(list.get(0).getUrl(), is("//127.0.0.1:44/cacheName"));
        assertThat(list.get(1).getUrl(), is("//127.0.0.2:44/cacheName"));
        assertThat(list.get(2).getUrl(), is("//127.0.0.3:44/cacheName"));
    }

    @Test
    public void skipNodesOnExceptions() throws RemoteException {
        final JiraCacheManagerPeerProvider providerWithExceptions = new GeneratingExceptionFixture(cacheManager,
                executorService);

        final List<CachePeer> list = providerWithExceptions.listRemoteCachePeers(ehcache);
        assertThat(list, hasSize(2));
        assertThat(list.get(0).getUrl(), is("//127.0.0.1:44/cacheName"));
        assertThat(list.get(1).getUrl(), is("//127.0.0.3:44/cacheName"));
    }

    private void initLiveNodes() {
        Node node1 = new Node("node1", Node.NodeState.ACTIVE, Long.MAX_VALUE, "localhost", 43l);
        Node node2 = new Node("node2", Node.NodeState.ACTIVE, Long.MAX_VALUE, "127.0.0.1", 44l);
        Node node3 = new Node("node3", Node.NodeState.ACTIVE, Long.MAX_VALUE, "127.0.0.2", 44l);
        Node node4 = new Node("node4", Node.NodeState.ACTIVE, Long.MAX_VALUE, "127.0.0.3", 44l);

        when(clusterManager.getNodeId()).thenReturn("node1");
        when(clusterManager.findLiveNodes()).thenReturn(ImmutableSet.of(node1, node2, node3, node4));
        when(ehcache.getName()).thenReturn("cacheName");
    }

    class CountdownBarrierFixture extends JiraCacheManagerPeerProvider {
        public CountdownBarrierFixture(final CacheManager cacheManager, final ExecutorService executorService) {
            super(cacheManager, executorService);
        }

        /**
         * Override this method so the lookup does not fail.
         */
        @Override
        public CachePeer lookupRemoteCachePeer(final String url)
                throws MalformedURLException, NotBoundException, RemoteException {
            meetAtTheBarrier();
            final CachePeer peer = mock(CachePeer.class);
            when(peer.getUrl()).thenReturn(url);
            return peer;
        }

        private void meetAtTheBarrier() {
            if (countDownLatch != null) {
                countDownLatch.countDown();
                try {
                    countDownLatch.await();
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        }
    }

    class GeneratingExceptionFixture extends JiraCacheManagerPeerProvider {
        public GeneratingExceptionFixture(final CacheManager cacheManager, final ExecutorService executorService) {
            super(cacheManager, executorService);
        }

        @Override
        public CachePeer lookupRemoteCachePeer(final String url) throws MalformedURLException,
                NotBoundException, RemoteException {
            if (url.equals("//127.0.0.2:44/cacheName")) {
                throw new RemoteException("remote-exception-message");
            }
            final CachePeer peer = mock(CachePeer.class);
            when(peer.getUrl()).thenReturn(url);
            return peer;
        }
    }

}
