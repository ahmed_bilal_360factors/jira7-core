package com.atlassian.jira.bc.dataimport;

import com.atlassian.fugue.Pair;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.util.AttachmentPathManager;
import com.atlassian.jira.config.util.IndexPathManager;
import com.atlassian.jira.matchers.ErrorCollectionMatcher;
import com.atlassian.jira.mock.ofbiz.MockGenericValue;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.google.common.collect.ImmutableMap;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.ofbiz.core.entity.GenericEntityException;
import org.ofbiz.core.entity.GenericValue;
import org.xml.sax.Attributes;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collections;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.stream.Collectors;

import static com.atlassian.fugue.Pair.pair;
import static com.atlassian.jira.bc.dataimport.OfbizImportHandler.OSPROPERTY_ENTRY;
import static com.atlassian.jira.bc.dataimport.OfbizImportHandler.OSPROPERTY_STRING;
import static com.atlassian.jira.bc.dataimport.OfbizImportHandler.OSPROPERTY_TEXT;
import static com.google.common.collect.Sets.newHashSet;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

/**
 * Copyright All Rights Reserved. Created: christo 16/10/2006 16:39:14
 */
@RunWith(MockitoJUnitRunner.class)
public class TestOfbizImportHandler {
    @Mock
    private OfBizDelegator ofBizDelegator;
    @Mock
    private Executor executor;
    @Mock
    private IndexPathManager indexPathManager;
    @Mock
    private AttachmentPathManager attachmentPathManager;
    @Mock
    FeatureManager featureManager;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    private OfbizImportHandler handler;


    private static final String ENTITY_ENGINE_XML = "entity-engine-xml";
    private static final String PROPERTY_KEY = "propertyKey";
    private static final String VALUE_NAME = "value";

    private static final String ID = "666";
    private static final String ID3 = "665";
    private static final String BUILD_NUMBER = "12345";
    private static final String INDEX_PATH = "67789";
    private static final String ATTACHMENT_PATH = "88333";

    @Before
    public void setUp() throws Exception {
        handler = new OfbizImportHandler(ofBizDelegator, executor, indexPathManager, attachmentPathManager);
    }

    @Test
    public void testBuildNumberNotPresentInXml() throws Exception {
        handler.createBuildNumber();
        assertNull(handler.getBuildNumber());
    }

    @Test
    public void testBuildPresentInXml() {
        // faking SAX events...
        handler.recordElementsInfo(OSPROPERTY_ENTRY, saxAttr(ID, PROPERTY_KEY, APKeys.JIRA_PATCHED_VERSION));
        handler.recordElementsInfo(OSPROPERTY_STRING, saxAttr(ID, VALUE_NAME, BUILD_NUMBER));

        handler.createBuildNumber();
        assertEquals(BUILD_NUMBER, handler.getBuildNumber());
    }

    @Test
    public void testIndexPathInXml() {
        // faking SAX events...
        handler.recordElementsInfo(OSPROPERTY_ENTRY, saxAttr(ID, PROPERTY_KEY, APKeys.JIRA_PATH_INDEX));
        handler.recordElementsInfo(OSPROPERTY_STRING, saxAttr(ID, VALUE_NAME, INDEX_PATH));

        handler.createIndexPath();

        assertEquals(INDEX_PATH, handler.getIndexPath());
    }

    @Test
    public void testIndexPathNotInXml() {
        handler.createIndexPath();

        assertNull(handler.getIndexPath());
    }

    @Test
    public void testAttachmentPathInXml() {
        // faking SAX events...
        handler.recordElementsInfo(OSPROPERTY_ENTRY, saxAttr(ID, PROPERTY_KEY, APKeys.JIRA_PATH_ATTACHMENTS));
        handler.recordElementsInfo(OSPROPERTY_STRING, saxAttr(ID, VALUE_NAME, ATTACHMENT_PATH));

        handler.createAttachmentPath();

        assertEquals(ATTACHMENT_PATH, handler.getAttachmentPath());
    }

    @Test
    public void testAttachmentPathNotInXml() {
        handler.createAttachmentPath();

        assertNull(handler.getAttachmentPath());
    }

    @Test
    public void testWhenThereIsNoLicensePresentAtAll() throws Exception {
        handler.createLicenseString();
        assertFalse(handler.getLicenseStrings().iterator().hasNext());
    }

    @Test
    public void testCreateZeroDeadlock() throws Exception {
        GenericValue gv = mock(GenericValue.class);
        when(gv.create()).thenReturn(gv);

        handler.createWithRetry(Collections.singletonList(gv));
        Mockito.verify(gv, times(1)).create();
    }

    @Test
    public void testCreateOneDeadlock() throws Exception {
        GenericValue gv = mock(GenericValue.class);
        when(gv.create()).thenThrow(getSQLDeadlockException()).thenReturn(gv);

        handler.createWithRetry(Collections.singletonList(gv));
        Mockito.verify(gv, times(2)).create();
    }

    @Test
    public void testCreateThreeDeadlocks() throws Exception {
        GenericValue gv = mock(GenericValue.class);
        when(gv.create())
                .thenThrow(getSQLDeadlockException())
                .thenThrow(getSQLDeadlockException())
                .thenThrow(getSQLDeadlockException())
                .thenReturn(gv);

        handler.createWithRetry(Collections.singletonList(gv));
        Mockito.verify(gv, times(4)).create();
    }

    @Test
    public void testCreateFourDeadlocks() throws Exception {
        GenericValue gv = mock(GenericValue.class);
        when(gv.create())
                .thenThrow(getSQLDeadlockException())
                .thenThrow(getSQLDeadlockException())
                .thenThrow(getSQLDeadlockException())
                .thenThrow(getSQLDeadlockException());

        expectedException.expect(GenericEntityException.class);
        handler.createWithRetry(Collections.singletonList(gv));
    }

    @Test
    public void testCreateDeeplyNestedDeadlocks() throws Exception {
        GenericValue gv = mock(GenericValue.class);
        when(gv.create())
                .thenThrow(getSQLDeadlockException())
                .thenThrow(getSQLDeadlockNestedException())
                .thenThrow(getSQLDeadlockNestedException())
                .thenReturn(gv);

        handler.createWithRetry(Collections.singletonList(gv));
        Mockito.verify(gv, times(4)).create();
    }

    @Test
    public void testStartElement() throws Exception {
        handler.startElement("", "", ENTITY_ENGINE_XML, saxAttr(ID3, PROPERTY_KEY, APKeys.JIRA_LICENSE));

        when(ofBizDelegator.makeValue(Mockito.anyString())).thenReturn(new MockGenericValue("Action"));
        handler.startElement("", "", "Action", saxAttr(ID3, PROPERTY_KEY, APKeys.JIRA_LICENSE));
        Mockito.verify(ofBizDelegator, times(1)).makeValue(Mockito.anyString());
    }

    @Test
    public void testStartElementThrowsDuringValidation() throws Exception {
        handler.setCreateEntities(false);
        handler.startDocument();
        handler.startElement("", "", ENTITY_ENGINE_XML, saxAttr(ID3, PROPERTY_KEY, APKeys.JIRA_LICENSE));

        when(ofBizDelegator.makeValue(Mockito.anyString())).thenThrow(new RuntimeException());
        handler.startElement("", "", "Action", saxAttr(ID3, PROPERTY_KEY, APKeys.JIRA_LICENSE));
        assertTrue("Exception should be muted and recorded in error collection", handler.getErrorCollection().hasAnyErrors());
        Mockito.verify(ofBizDelegator, times(1)).makeValue(Mockito.anyString());
    }

    @Test
    public void testStartElementThrowsDuringImport() throws Exception {
        handler.setCreateEntities(true);
        handler.startDocument();
        handler.startElement("", "", ENTITY_ENGINE_XML, saxAttr(ID3, PROPERTY_KEY, APKeys.JIRA_LICENSE));

        when(ofBizDelegator.makeValue(Mockito.anyString())).thenThrow(new RuntimeException());
        expectedException.expect(RuntimeException.class);
        handler.startElement("", "", "Action", saxAttr(ID3, PROPERTY_KEY, APKeys.JIRA_LICENSE));
    }

    @Test
    public void shouldDecodeLicenseFromTextPropertyWithEmbeddedValue() throws Exception {
        // given
        final Map<String, String> osPropertyKeys = ImmutableMap.<String, String>builder()
                .put("id", "")
                .put("propertyKey", "")
                .put("type", "")
                .put("value", "").build();

        when(ofBizDelegator.makeValue(OSPROPERTY_TEXT)).thenReturn(new MockGenericValue(OSPROPERTY_TEXT, osPropertyKeys));
        when(ofBizDelegator.makeValue(OSPROPERTY_ENTRY)).thenReturn(new MockGenericValue(OSPROPERTY_ENTRY, osPropertyKeys));
        handler.setCreateEntities(false);


        // when
        handler.startDocument();
        final String licenseBody = "LICENSE_BODY";
        element(ENTITY_ENGINE_XML, saxAttr(),
                () -> {
                    final String LICENSE_PROPERTY_ID = "19";
                    element(OfbizImportHandler.OSPROPERTY_ENTRY,
                            saxAttr(pair("id", LICENSE_PROPERTY_ID),
                                    pair("entityName", "jira.properties"),
                                    pair("entityId", "1"),
                                    pair("propertyKey", APKeys.JIRA_LICENSE),
                                    pair("type", "6")));

                    element(OfbizImportHandler.OSPROPERTY_TEXT, saxAttr(pair("id", LICENSE_PROPERTY_ID)),
                            () -> element("value", saxAttr(),
                                    () -> {
                                        handler.characters(licenseBody.toCharArray(), 0, licenseBody.length());
                                    }
                            )
                    );
                }
        );
        handler.endDocument();

        // then
        assertThat(
                handler.getErrorCollection(),
                ErrorCollectionMatcher.hasNoErrors());
        assertThat(
                handler.getLicenseStrings(),
                contains(licenseBody));
    }

    @Test
    public void shouldDecodeLicenseFromProductLicense() throws Exception {
        // given
        final String licenseElementName = "license";
        final String productLicenseElementName = "ProductLicense";
        final Map<String, String> osPropertyKeys = ImmutableMap.<String, String>builder()
                .put("id", "")
                .put(licenseElementName, "").build();

        when(ofBizDelegator.makeValue(productLicenseElementName)).thenReturn(new MockGenericValue(productLicenseElementName, osPropertyKeys));
        handler.setCreateEntities(false);


        // when
        handler.startDocument();
        final String licenseBody = "LICENSE_BODY";
        element(ENTITY_ENGINE_XML, saxAttr(),
                () -> {
                    element(productLicenseElementName, saxAttr(pair("id", "10000")),
                            () -> {
                                element(licenseElementName, saxAttr(), () -> {
                                    handler.characters(licenseBody.toCharArray(), 0, licenseBody.length());
                                });
                            }
                    );
                });
        handler.endDocument();

        // then
        assertThat(
                handler.getErrorCollection(),
                ErrorCollectionMatcher.hasNoErrors());
        assertThat(
                handler.getLicenseStrings(),
                contains(licenseBody));
    }

    @Test
    public void shouldRecordPropertyEntriesWithMatchingPropertyKeys() {
        handler = new OfbizImportHandler(ofBizDelegator, executor, indexPathManager, attachmentPathManager,
                false, newHashSet("property.key.1", "property.key.2"));

        handler.recordElementsInfo(OSPROPERTY_ENTRY, saxAttr("10000", PROPERTY_KEY, "property.key.1"));
        handler.recordElementsInfo(OSPROPERTY_ENTRY, saxAttr("10100", PROPERTY_KEY, "property.key.3"));
        handler.recordElementsInfo(OSPROPERTY_ENTRY, saxAttr("10200", PROPERTY_KEY, "property.key.2"));

        assertThat(handler.getPropertyKeyToId().size(), is(2));
        assertThat(handler.getPropertyKeyToId(), hasEntry("property.key.1", "10000"));
        assertThat(handler.getPropertyKeyToId(), hasEntry("property.key.2", "10200"));
    }

    @Test
    public void shouldRecordNoPropertyEntriesWhenNoPropertyKeysAreSpecified() {
        handler = new OfbizImportHandler(ofBizDelegator, executor, indexPathManager, attachmentPathManager);

        handler.recordElementsInfo(OSPROPERTY_ENTRY, saxAttr("10000", PROPERTY_KEY, "property.key.1"));
        handler.recordElementsInfo(OSPROPERTY_ENTRY, saxAttr("10100", PROPERTY_KEY, "property.key.3"));
        handler.recordElementsInfo(OSPROPERTY_ENTRY, saxAttr("10200", PROPERTY_KEY, "property.key.2"));

        assertThat(handler.getPropertyKeyToId().isEmpty(), is(true));
    }

    @Test
    public void shouldRecordNoPropertyEntriesWhenNoPropertyKeysMatchTheOnesSpecified() {
        handler = new OfbizImportHandler(ofBizDelegator, executor, indexPathManager, attachmentPathManager,
                false, newHashSet("property.key.1", "property.key.2"));

        handler.recordElementsInfo(OSPROPERTY_ENTRY, saxAttr("10000", PROPERTY_KEY, "property.key.3"));
        handler.recordElementsInfo(OSPROPERTY_ENTRY, saxAttr("10100", PROPERTY_KEY, "property.key.4"));
        handler.recordElementsInfo(OSPROPERTY_ENTRY, saxAttr("10200", PROPERTY_KEY, "property.key.5"));

        assertThat(handler.getPropertyKeyToId().isEmpty(), is(true));
    }

    private interface ContentBuilder {
        void call() throws Exception;

        ContentBuilder NOOP = () -> {
        };
    }


    void element(final String localName, final Attributes atts) throws Exception {
        element(localName, atts, ContentBuilder.NOOP);
    }

    void element(final String localName, final Attributes atts, final ContentBuilder fillElementBody) throws Exception {
        handler.startElement("", "", localName, atts);
        fillElementBody.call();
        handler.endElement("", "", localName);
    }

    private Exception getSQLDeadlockException() {
        SQLException sqlEx = new SQLException("Dummy Deadlock", "40001");
        return new GenericEntityException(sqlEx.getMessage(), sqlEx);
    }

    private Exception getSQLDeadlockNestedException() {
        SQLException sqlEx = new SQLException("Dummy Deadlock", "40001");
        Exception ex2 = new GenericEntityException(sqlEx.getMessage(), sqlEx);
        Exception ex3 = new GenericEntityException(sqlEx.getMessage(), ex2);
        return new GenericEntityException(sqlEx.getMessage(), ex3);
    }

    private Attributes saxAttr(final String id, final String key, final String value) {
        return new MockAttributesBuilder().id(id).attr(key, value).build();
    }

    @SafeVarargs
    final private Attributes saxAttr(final Pair<String, String>... values) {
        final Map<String, String> atts = Arrays.stream(values).collect(Collectors.toMap(Pair::left, Pair::right));
        return new MockAttributesBuilder().attr(atts).build();
    }

}
