package com.atlassian.jira.bc.license;

import com.atlassian.application.api.Application;
import com.atlassian.application.api.ApplicationKey;
import com.atlassian.application.api.ApplicationManager;
import com.atlassian.collectors.CollectorsUtil;
import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.fugue.Option;
import com.atlassian.jira.application.ApplicationKeys;
import com.atlassian.jira.application.ApplicationRole;
import com.atlassian.jira.application.ApplicationRoleManager;
import com.atlassian.jira.user.MockGroup;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import org.joda.time.DateTime;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Set;

import static org.junit.Assert.fail;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * A helper class for application role mock objects. This is to make the unit tests more readable.
 *
 * @since v7.0
 */
public class MockApplicationRoleTestHelper {
    public final Set<ApplicationRole> VALID_APPLICATION_ROLES;
    public final Set<ApplicationRole> SOFTWARE_PORTFOLIO_OTHER_APP_ROLES;
    public final Set<Application> VALID_APPLICATIONS;

    public MockApplicationRoleTestHelper() {
        initApplicationUnlicensed();
        initApplicationSoftware();
        initApplicationPortfolio();
        initApplicationZeroUserLimit();
        initApplicationOther();
        initApplicationCore();
        initApplicationNoDefaultGroup();
        initApplicationNoDefaultGroup();
        initApplicationNoDefaultGroup();
        initApplicationNoDefaultGroup();

        VALID_APPLICATION_ROLES = Sets.newHashSet(SOFTWARE_APP_ROLE, PORTFOLIO_APP_ROLE);
        SOFTWARE_PORTFOLIO_OTHER_APP_ROLES = Sets.newHashSet(SOFTWARE_APP_ROLE, PORTFOLIO_APP_ROLE, OTHER_APP_ROLE);
        VALID_APPLICATIONS = Sets.newHashSet(SOFTWARE_APPLICATION, PORTFOLIO_APPLICATION);
    }

    public final ApplicationRole UNLICENSED_APP_ROLE = mock(ApplicationRole.class);
    public final String UNLICENSED_KEY_STR = "com.atlassian.not.licensed";
    public final ApplicationKey UNLICENSED_APP_KEY = ApplicationKey.valueOf(UNLICENSED_KEY_STR);
    public final String UNLICENSED_TRADEMARK = null;

    private void initApplicationUnlicensed() {
        when(UNLICENSED_APP_ROLE.getName()).thenReturn(UNLICENSED_TRADEMARK);
        when(UNLICENSED_APP_ROLE.getKey()).thenReturn(UNLICENSED_APP_KEY);
        when(UNLICENSED_APP_ROLE.getNumberOfSeats()).thenReturn(0);
        when(UNLICENSED_APP_ROLE.getDefaultGroups()).thenReturn(ImmutableSet.of());
        when(UNLICENSED_APP_ROLE.getGroups()).thenReturn(ImmutableSet.of());
    }

    public final ApplicationRole SOFTWARE_APP_ROLE = mock(ApplicationRole.class);
    public final String SOFTWARE_APP_KEY_STR = "com.atlassian.software";
    public final ApplicationKey SOFTWARE_APP_KEY = ApplicationKey.valueOf(SOFTWARE_APP_KEY_STR);
    public final String SOFTWARE_TRADEMARK = "JIRA Software";
    public final Group SOFTWARE_DEFAULT_GROUP = new MockGroup("group-software");
    public final Set<Group> SOFTWARE_GROUPS = groups("group-software", "group-software-2");
    public final int SOFTWARE_TEN_USER_LIC_LIMIT = 10;
    public final Application SOFTWARE_APPLICATION = mock(Application.class);

    private void initApplicationSoftware() {
        when(SOFTWARE_APPLICATION.getName()).thenReturn(SOFTWARE_TRADEMARK);
        when(SOFTWARE_APPLICATION.getKey()).thenReturn(SOFTWARE_APP_KEY);
        when(SOFTWARE_APPLICATION.getDefaultGroup()).thenReturn(SOFTWARE_DEFAULT_GROUP.getName());
        when(SOFTWARE_APP_ROLE.getName()).thenReturn(SOFTWARE_TRADEMARK);
        when(SOFTWARE_APP_ROLE.getKey()).thenReturn(SOFTWARE_APP_KEY);
        when(SOFTWARE_APP_ROLE.getNumberOfSeats()).thenReturn(SOFTWARE_TEN_USER_LIC_LIMIT);
        when(SOFTWARE_APP_ROLE.getDefaultGroups()).thenReturn(Collections.singleton(SOFTWARE_DEFAULT_GROUP));
        when(SOFTWARE_APP_ROLE.getGroups()).thenReturn(SOFTWARE_GROUPS);
    }

    public final ApplicationRole PORTFOLIO_APP_ROLE = mock(ApplicationRole.class);
    public final String PORTFOLIO_APP_KEY_STR = "com.atlassian.portfolio";
    public final ApplicationKey PORTFOLIO_APP_KEY = ApplicationKey.valueOf(PORTFOLIO_APP_KEY_STR);
    public final String PORTFOLIO_TRADEMARK = "JIRA Portfolio";
    public final Group PORTFOLIO_DEFAULT_GROUP = new MockGroup("group-portfolio");
    public final Group PORTFOLIO_NOT_DEFAULT_GROUP = new MockGroup("group-portfolio-2");
    public final Set<Group> PORTFOLIO_GROUPS = groups(PORTFOLIO_DEFAULT_GROUP.getName(), PORTFOLIO_NOT_DEFAULT_GROUP.getName());
    public final int PORTFOLIO_TEN_USER_LIC_LIMIT = 10;
    public final Application PORTFOLIO_APPLICATION = mock(Application.class);

    private void initApplicationPortfolio() {
        when(PORTFOLIO_APPLICATION.getName()).thenReturn(PORTFOLIO_TRADEMARK);
        when(PORTFOLIO_APPLICATION.getKey()).thenReturn(PORTFOLIO_APP_KEY);
        when(PORTFOLIO_APPLICATION.getDefaultGroup()).thenReturn(PORTFOLIO_DEFAULT_GROUP.getName());
        when(PORTFOLIO_APP_ROLE.getName()).thenReturn(PORTFOLIO_TRADEMARK);
        when(PORTFOLIO_APP_ROLE.getKey()).thenReturn(PORTFOLIO_APP_KEY);
        when(PORTFOLIO_APP_ROLE.getNumberOfSeats()).thenReturn(PORTFOLIO_TEN_USER_LIC_LIMIT);
        when(PORTFOLIO_APP_ROLE.getDefaultGroups()).thenReturn(Collections.singleton(PORTFOLIO_DEFAULT_GROUP));
        when(PORTFOLIO_APP_ROLE.getGroups()).thenReturn(PORTFOLIO_GROUPS);
    }

    public final ApplicationRole NO_DEFAULT_GROUP_APP_ROLE = mock(ApplicationRole.class);
    public final String NO_DEFAULT_GROUP_APP_KEY_STR = "com.atlassian.no.group";
    public final ApplicationKey NO_DEFAULT_GROUP_APP_KEY = ApplicationKey.valueOf(NO_DEFAULT_GROUP_APP_KEY_STR);
    public final String NO_DEFAULT_GROUP_TRADEMARK = "JIRA No Group";
    public final Set<Group> NO_DEFAULT_GROUP_GROUPS = groups("group-no-group-2");
    public final int NO_DEFAULT_GROUP_TEN_USER_LIC_LIMIT = 10;
    public final Application NO_DEFAULT_GROUP_APPLICATION = mock(Application.class);

    private void initApplicationNoDefaultGroup() {
        when(NO_DEFAULT_GROUP_APPLICATION.getName()).thenReturn(NO_DEFAULT_GROUP_TRADEMARK);
        when(NO_DEFAULT_GROUP_APPLICATION.getKey()).thenReturn(NO_DEFAULT_GROUP_APP_KEY);
        when(NO_DEFAULT_GROUP_APPLICATION.getDefaultGroup()).thenReturn(null);
        when(NO_DEFAULT_GROUP_APP_ROLE.getName()).thenReturn(NO_DEFAULT_GROUP_TRADEMARK);
        when(NO_DEFAULT_GROUP_APP_ROLE.getKey()).thenReturn(NO_DEFAULT_GROUP_APP_KEY);
        when(NO_DEFAULT_GROUP_APP_ROLE.getNumberOfSeats()).thenReturn(NO_DEFAULT_GROUP_TEN_USER_LIC_LIMIT);
        when(NO_DEFAULT_GROUP_APP_ROLE.getDefaultGroups()).thenReturn(Collections.emptySet());
        when(NO_DEFAULT_GROUP_APP_ROLE.getGroups()).thenReturn(NO_DEFAULT_GROUP_GROUPS);
    }

    public final ApplicationRole ZERO_USER_LIMIT_APP_ROLE = mock(ApplicationRole.class);
    public final String ZERO_USER_LIMIT_APP_KEY_STR = "com.atlassian.zero.user.limit";
    public final ApplicationKey ZERO_USER_LIMIT_APP_KEY = ApplicationKey.valueOf(ZERO_USER_LIMIT_APP_KEY_STR);
    public final String ZERO_USER_LIMIT_TRADEMARK = "JIRA Zero User Limit";
    public final Group ZERO_USER_LIMIT_DEFAULT_GROUP = new MockGroup("group-zero-user-limit");
    public final Set<Group> ZERO_USER_LIMIT_GROUPS = ImmutableSet.of(ZERO_USER_LIMIT_DEFAULT_GROUP, group("group-zero-user-limit-2"));
    public final int ZERO_USER_LIMIT_ZERO_USER_LIC_LIMIT = 0;
    public final Application ZERO_USER_LIMIT_APPLICATION = mock(Application.class);

    private void initApplicationZeroUserLimit() {
        when(ZERO_USER_LIMIT_APPLICATION.getName()).thenReturn(ZERO_USER_LIMIT_TRADEMARK);
        when(ZERO_USER_LIMIT_APPLICATION.getKey()).thenReturn(ZERO_USER_LIMIT_APP_KEY);
        when(ZERO_USER_LIMIT_APPLICATION.getDefaultGroup()).thenReturn(ZERO_USER_LIMIT_DEFAULT_GROUP.getName());
        when(ZERO_USER_LIMIT_APP_ROLE.getName()).thenReturn(ZERO_USER_LIMIT_TRADEMARK);
        when(ZERO_USER_LIMIT_APP_ROLE.getKey()).thenReturn(ZERO_USER_LIMIT_APP_KEY);
        when(ZERO_USER_LIMIT_APP_ROLE.getNumberOfSeats()).thenReturn(ZERO_USER_LIMIT_ZERO_USER_LIC_LIMIT);
        when(ZERO_USER_LIMIT_APP_ROLE.getDefaultGroups()).thenReturn(Collections.singleton(ZERO_USER_LIMIT_DEFAULT_GROUP));
        when(ZERO_USER_LIMIT_APP_ROLE.getGroups()).thenReturn(ZERO_USER_LIMIT_GROUPS);
    }

    public final ApplicationRole OTHER_APP_ROLE = mock(ApplicationRole.class);
    public final String OTHER_LIC_ID_STR = "com.atlassian.other";
    public final ApplicationKey OTHER_APP_KEY = ApplicationKey.valueOf(OTHER_LIC_ID_STR);
    public final String OTHER_TRADEMARK = "JIRA Other";
    public final Group OTHER_DEFAULT_GROUP = group("group-other");
    public final Set<Group> OTHER_GROUPS = groups("group-other", "group-other-2");
    public final int OTHER_TEN_USER_LIC_LIMIT = 10;
    public final Application OTHER_APPLICATION = mock(Application.class);

    private void initApplicationOther() {
        when(OTHER_APPLICATION.getName()).thenReturn(OTHER_TRADEMARK);
        when(OTHER_APPLICATION.getKey()).thenReturn(OTHER_APP_KEY);
        when(OTHER_APPLICATION.getDefaultGroup()).thenReturn(OTHER_DEFAULT_GROUP.getName());
        when(OTHER_APP_ROLE.getName()).thenReturn(OTHER_TRADEMARK);
        when(OTHER_APP_ROLE.getKey()).thenReturn(OTHER_APP_KEY);
        when(OTHER_APP_ROLE.getNumberOfSeats()).thenReturn(OTHER_TEN_USER_LIC_LIMIT);
        when(OTHER_APP_ROLE.getDefaultGroups()).thenReturn(Collections.singleton(OTHER_DEFAULT_GROUP));
        when(OTHER_APP_ROLE.getGroups()).thenReturn(OTHER_GROUPS);
    }

    public final ApplicationRole CORE_APP_ROLE = mock(ApplicationRole.class);
    public final ApplicationKey CORE_APP_KEY = ApplicationKeys.CORE;
    public final String CORE_TRADEMARK = "JIRA Core";
    public final Group CORE_DEFAULT_GROUP = group("group-core");
    public final Set<Group> CORE_GROUPS = groups("group-core", "group-core-2");
    public final int CORE_TEN_USER_LIC_LIMIT = 10;
    public final Application CORE_APPLICATION = mock(Application.class);

    private void initApplicationCore() {
        when(CORE_APPLICATION.getName()).thenReturn(CORE_TRADEMARK);
        when(CORE_APPLICATION.getKey()).thenReturn(CORE_APP_KEY);
        when(CORE_APPLICATION.getDefaultGroup()).thenReturn(CORE_DEFAULT_GROUP.getName());
        when(CORE_APP_ROLE.getName()).thenReturn(CORE_TRADEMARK);
        when(CORE_APP_ROLE.getKey()).thenReturn(CORE_APP_KEY);
        when(CORE_APP_ROLE.getNumberOfSeats()).thenReturn(CORE_TEN_USER_LIC_LIMIT);
        when(CORE_APP_ROLE.getDefaultGroups()).thenReturn(Collections.singleton(CORE_DEFAULT_GROUP));
        when(CORE_APP_ROLE.getGroups()).thenReturn(CORE_GROUPS);
    }

    boolean hasRunSetup = false;

    private void assertSetupNotPerformed() {
        if (hasRunSetup) {
            fail("Setup has already been called. "
                    + "Do not call a setup method more than once on " + this.getClass().getName() + ", you could get unexpected values. "
                    + "Fix: Perform setup in test method only not in @Before or re-create helper and call setup.");
        }
    }

    private void setupComplete() {
        hasRunSetup = true;
    }

    public void setup(final ApplicationManager applicationManager, Application applications,
                      final ApplicationRoleManager applicationRoleManager, ApplicationRole applicationRoles) {
        assertSetupNotPerformed();
        setupRole(applicationRoleManager, applicationRoles);
        when(applicationRoleManager.getRole(UNLICENSED_APP_KEY)).thenReturn(Option.none());
        when(applicationRoleManager.hasSeatsAvailable(eq(UNLICENSED_APP_KEY), anyInt())).thenReturn(Boolean.FALSE);
        when(applicationRoleManager.getRoles()).thenReturn(Sets.newHashSet(applicationRoles));
        setupApplicationsInternal(applicationManager, applications);
        setupComplete();
    }

    public void setup(final ApplicationManager applicationManager, Collection<Application> applications,
                      final ApplicationRoleManager applicationRoleManager, final Collection<ApplicationRole> applicationRoles) {
        assertSetupNotPerformed();
        for (ApplicationRole applicationRole : applicationRoles) {
            setupRole(applicationRoleManager, applicationRole);
        }
        when(applicationRoleManager.getRole(UNLICENSED_APP_KEY)).thenReturn(Option.<ApplicationRole>none());
        when(applicationRoleManager.hasSeatsAvailable(eq(UNLICENSED_APP_KEY), anyInt())).thenReturn(Boolean.FALSE);
        when(applicationRoleManager.getRoles()).thenReturn(Sets.newHashSet(applicationRoles));

        setupApplicationsInternal(applicationManager, applications.toArray(new Application[applications.size()]));
        setupComplete();
    }

    private void setupRole(final ApplicationRoleManager applicationRoleManager, final ApplicationRole applicationRole) {
        final ApplicationKey key = applicationRole.getKey();
        when(applicationRoleManager.getRole(key)).thenReturn(Option.some(applicationRole));
        when(applicationRoleManager.hasSeatsAvailable(eq(key), anyInt())).thenAnswer(invocationOnMock ->
        {
            Integer seatArg = (Integer) invocationOnMock.getArguments()[1];
            return applicationRole.getNumberOfSeats() >= seatArg;
        });
    }

    public void setupApplications(final ApplicationManager applicationManager, final Application... applications) {
        assertSetupNotPerformed();
        setupApplicationsInternal(applicationManager, applications);
        setupComplete();
    }

    private void setupApplicationsInternal(final ApplicationManager applicationManager, final Application... applications) {
        for (Application application : applications) {
            setupApplicationInternal(applicationManager, application);
        }
        when(applicationManager.getApplications()).thenReturn(Sets.newHashSet(applications));
    }

    private void setupApplicationInternal(final ApplicationManager applicationManager, final Application application) {
        when(application.buildDate()).thenReturn(DateTime.now());
        when(applicationManager.getApplication(application.getKey())).thenReturn(Option.some(application));
        when(applicationManager.getApplications()).thenReturn(Sets.newHashSet(application));
    }

    public static Group group(String group) {
        return new MockGroup(group);
    }

    public static Set<Group> groups(String... groups) {
        return Arrays.stream(groups).map(MockGroup::new).collect(CollectorsUtil.toImmutableSet());
    }
}