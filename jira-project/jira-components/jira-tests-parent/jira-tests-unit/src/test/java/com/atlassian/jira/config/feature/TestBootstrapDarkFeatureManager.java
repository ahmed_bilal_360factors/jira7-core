package com.atlassian.jira.config.feature;

import com.atlassian.jira.config.properties.JiraProperties;
import com.atlassian.sal.api.features.EnabledDarkFeatures;
import com.atlassian.sal.api.user.UserKey;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import java.util.Properties;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

/**
 * Test for BootstrapDarkFeatureManager.
 */
public class TestBootstrapDarkFeatureManager {
    @Rule
    public MethodRule initMockito = MockitoJUnit.rule();
    @Mock
    private JiraProperties jiraProperties;
    @Mock
    private FeaturesLoader featuresLoader;

    private BootstrapDarkFeatureManager manager;

    @Before
    public void setUp() throws Exception {
        final Properties coreProperties = new Properties();
        coreProperties.setProperty("property.valid", "true");
        coreProperties.setProperty("property.invalid", "false");
        coreProperties.setProperty("property.invalid2", "fal");
        when(featuresLoader.loadCoreProperties()).thenReturn(coreProperties);
        manager = new BootstrapDarkFeatureManager(featuresLoader);
    }

    @Test
    public void shouldReturnTrueForFeaturesEnabledInCore() {
        final boolean result = manager.isFeatureEnabledForUser(new UserKey("any"), "property.valid");

        assertTrue("Should return true for enabled dark features", result);
    }

    @Test
    public void shouldReturnFalseForFeaturesDisabledInCore() {
        final boolean result = manager.isFeatureEnabledForUser(new UserKey("any"), "property.invalid");

        assertFalse("Should return false for disabled dark features", result);
    }

    @Test
    public void shouldReturnFalseForFeaturesWitInvalidPropertyValueInDefaultFile() {
        final boolean result = manager.isFeatureEnabledForUser(new UserKey("any"), "property.invalid2");

        assertFalse("Should return false for dark features with invalid value", result);
    }


    @Test
    public void shouldReturnAllEnabledFeatures() {
        final EnabledDarkFeatures enabledDarkFeatures = manager.getFeaturesEnabledForUser(new UserKey("any"));

        assertTrue("Property should be enabled", enabledDarkFeatures.isFeatureEnabled("property.valid"));
        assertFalse("Property should be disabled", enabledDarkFeatures.isFeatureEnabled("system.invalid"));
    }
}