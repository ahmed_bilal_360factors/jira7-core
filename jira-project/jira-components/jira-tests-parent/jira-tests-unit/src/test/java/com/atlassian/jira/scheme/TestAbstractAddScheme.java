package com.atlassian.jira.scheme;

import com.atlassian.jira.JiraTestUtil;
import com.atlassian.jira.junit.rules.JiraWebActionSupportDependencies;
import com.atlassian.jira.junit.rules.MockitoContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.google.common.collect.ImmutableList;
import com.mockobjects.servlet.MockHttpServletResponse;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.mockito.Mock;
import org.ofbiz.core.entity.GenericEntityException;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

public class TestAbstractAddScheme {

    @Mock
    SchemeManager arbitrarySchemeManager;

    @Rule
    public final MockitoContainer mockContainer = MockitoMocksInContainer.rule(this);
    @Rule
    public TestRule webActionSupport = JiraWebActionSupportDependencies.build(mockContainer);


    AbstractAddScheme abstractAddScheme;
    boolean wasNameValidated;

    @Before
    public void init() {
        wasNameValidated = false;
        abstractAddScheme = new AbstractAddScheme() {
            @Override
            public SchemeManager getSchemeManager() {
                return arbitrarySchemeManager;
            }

            @Override
            public String getRedirectURL() throws GenericEntityException {
                return "DummyRedirectUrl.jspa?id=";
            }

            @Override
            public void doNameValidation(final String name, final String mode) {
                wasNameValidated = true;
            }
        };
    }

    @Test
    public void doExecuteShouldCreateNewSchemeBasingOnNameAndDescriptionAndRedirect() throws Exception {
        MockHttpServletResponse response = JiraTestUtil.setupExpectedRedirect("DummyRedirectUrl.jspa?id=341");

        abstractAddScheme.setName("interestingName");
        abstractAddScheme.setDescription("amazingDescription");

        final Scheme value = new Scheme(341L, null, null, ImmutableList.<SchemeEntity>of());
        when(arbitrarySchemeManager.createSchemeObject("interestingName", "amazingDescription")).thenReturn(value);

        abstractAddScheme.doExecute();

        response.verify();
    }

    @Test
    public void shouldValidateNameWhenOnValidation() {
        abstractAddScheme.doValidation();
        assertTrue(wasNameValidated);
    }


}
