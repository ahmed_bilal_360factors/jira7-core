package com.atlassian.jira.issue.search;

import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.jql.util.JqlCustomFieldId;
import com.atlassian.jira.util.collect.CollectionBuilder;
import com.google.common.collect.ImmutableSet;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.Collections;
import java.util.Set;

import static org.hamcrest.Matchers.instanceOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestClauseNames {
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void shouldNotAcceptNull() {
        expectedException.expect(instanceOf(IllegalArgumentException.class));
        new ClauseNames(null);
    }

    @Test
    public void shouldNotAcceptEmptyPrimaryName() {
        expectedException.expect(instanceOf(IllegalArgumentException.class));
        new ClauseNames("  ");
    }

    @Test
    public void shouldNotAcceptEmptyNames() {
        expectedException.expect(instanceOf(IllegalArgumentException.class));
        new ClauseNames("dude", (Set<String>) null);
    }

    @Test
    public void shouldNotAcceptEmptyNamesArray() {
        expectedException.expect(instanceOf(IllegalArgumentException.class));
        new ClauseNames("dude", (String[]) null);
    }

    @Test
    public void shouldNotAcceptNullNames() {
        final Set<String> nullNames = Collections.singleton(null);
        expectedException.expect(instanceOf(IllegalArgumentException.class));
        new ClauseNames("dude", nullNames);
    }

    @Test
    public void shouldNotAcceptNullNamesArray() {
        final String[] nullNamesInArray = new String[]{null};
        expectedException.expect(instanceOf(IllegalArgumentException.class));
        new ClauseNames("dude", nullNamesInArray);
    }

    @Test
    public void shouldNotAcceptEmptyNameParameterInSet() {
        final Set<String> emptyName = ImmutableSet.of("jill", "");
        expectedException.expect(instanceOf(IllegalArgumentException.class));
        new ClauseNames("dude", emptyName);
    }

    @Test
    public void shouldNotAcceptNullPrimaryNameWhenNamesOK() {
        final Set<String> goodNames = CollectionBuilder.newBuilder("jack").asSet();
        expectedException.expect(instanceOf(IllegalArgumentException.class));
        new ClauseNames(null, goodNames);
    }

    @Test
    public void testClauseNamesConstructorHappyPath() throws Exception {
        final Set<String> goodNames = CollectionBuilder.newBuilder("jack").asSet();
        final ClauseNames names = new ClauseNames("dude", goodNames);

        assertTrue(names.contains("dude"));
        assertTrue(names.contains("jack"));

        final ClauseNames names2 = new ClauseNames("dude", "jack");
        assertEquals(names, names2);
    }

    @Test
    public void testForCustomField() throws Exception {
        final long customFieldId = 10L;
        final String customFieldName = "NotSystemField";

        final CustomField mock = mock(CustomField.class);
        when(mock.getUntranslatedName()).thenReturn(customFieldName);
        when(mock.getIdAsLong()).thenReturn(customFieldId);

        final ClauseNames names = ClauseNames.forCustomField(mock);
        final ClauseNames expectedNames = new ClauseNames(JqlCustomFieldId.toString(customFieldId), customFieldName);

        assertEquals(expectedNames, names);
    }

    @Test
    public void testForCustomFieldWithSystemName() throws Exception {
        final long customFieldId = 10L;
        final String customFieldName = "pRoJeCT";

        final CustomField mock = mock(CustomField.class);
        when(mock.getUntranslatedName()).thenReturn(customFieldName);
        when(mock.getIdAsLong()).thenReturn(customFieldId);

        final ClauseNames names = ClauseNames.forCustomField(mock);
        final ClauseNames expectedNames = new ClauseNames(JqlCustomFieldId.toString(customFieldId));

        assertEquals(expectedNames, names);

    }

    @Test
    public void testForCustomFieldNameWithId() throws Exception {
        final long customFieldId = 10L;
        final String customFieldName = "cf [ 2889292 ]  ";

        final CustomField mock = mock(CustomField.class);
        when(mock.getUntranslatedName()).thenReturn(customFieldName);
        when(mock.getIdAsLong()).thenReturn(customFieldId);

        final ClauseNames names = ClauseNames.forCustomField(mock);
        final ClauseNames expectedNames = new ClauseNames(JqlCustomFieldId.toString(customFieldId));

        assertEquals(expectedNames, names);
    }
}
