package com.atlassian.jira.i18n;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.instrumentation.InstrumentRegistry;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.plugin.language.LanguageModuleDescriptor;
import com.atlassian.jira.plugin.language.TranslationTransform;
import com.atlassian.jira.plugin.language.TranslationTransformModuleDescriptor;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.UserLocaleStore;
import com.atlassian.jira.util.ComponentLocator;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.web.util.JiraLocaleUtils;
import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.PluginInformation;
import com.atlassian.plugin.elements.ResourceDescriptor;
import com.atlassian.plugin.event.events.PluginEnabledEvent;
import com.atlassian.plugin.event.events.PluginModuleDisabledEvent;
import com.atlassian.plugin.event.events.PluginModuleEnabledEvent;
import com.atlassian.plugin.event.events.PluginRefreshedEvent;
import com.google.common.collect.ImmutableList;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Locale;

import static com.google.common.collect.ImmutableList.of;
import static java.util.Arrays.asList;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @since v6.2.3
 */
@RunWith(MockitoJUnitRunner.class)
public class TestCachingI18nFactory {
    @Rule
    public TestRule container = MockitoMocksInContainer.forTest(this);

    @SuppressWarnings("deprecation")
    @Mock
    private JiraLocaleUtils jiraLocaleUtils;
    @Mock
    private UserLocaleStore userLocaleStore;
    @Mock
    private BackingI18nFactory backingI18nFactory;

    @Mock
    @AvailableInContainer
    private InstrumentRegistry registry;

    @Mock
    private EventPublisher publisher;
    @Mock
    private I18nHelper i18nHelper;
    @Mock
    private PluginAccessor pluginAccessor;

    @Mock
    private ResourceBundleCacheCleaner resourceBundleCacheCleaner;

    @Mock
    private ComponentLocator locator;

    private CachingI18nFactory i18nFactory;
    private int pluginKeyCount;

    @Before
    public void setup() {
        when(locator.getComponentSupplier(PluginAccessor.class)).thenReturn(() -> this.pluginAccessor);

        i18nFactory = new CachingI18nFactory(jiraLocaleUtils, publisher, backingI18nFactory, userLocaleStore,
                locator, resourceBundleCacheCleaner);
    }

    @Test
    public void shouldCacheCallsForGetInstanceWithTheSameLocale() {

        final I18nHelper i18nHelperEnglish = mock(I18nHelper.class);
        final I18nHelper i18nHelperFrench = mock(I18nHelper.class);
        when(backingI18nFactory.create(Locale.ENGLISH, of())).thenReturn(i18nHelperEnglish).thenReturn(null);
        when(backingI18nFactory.create(Locale.FRENCH, of())).thenReturn(i18nHelperFrench).thenReturn(null);
        final I18nHelper englishCall = i18nFactory.getInstance(Locale.ENGLISH);
        final I18nHelper englishCall2 = i18nFactory.getInstance(Locale.ENGLISH);

        assertThat(englishCall, sameInstance(englishCall2));

        final I18nHelper frenchCall = i18nFactory.getInstance(Locale.FRENCH);

        assertThat(frenchCall, not(sameInstance(englishCall2)));
        assertThat(frenchCall, sameInstance(i18nHelperFrench));
        final I18nHelper englishCall3 = i18nFactory.getInstance(Locale.ENGLISH);
        assertThat(englishCall3, sameInstance(englishCall2));
    }

    @Test
    public void getHelperForUserCached() {
        final ApplicationUser frenchUser = new MockApplicationUser("fr_FR");
        final ApplicationUser frenchUserApp = new MockApplicationUser("fr_FR");
        final MockApplicationUser german = new MockApplicationUser("de_DE");
        final I18nHelper i18nHelperFrance = mock(I18nHelper.class);
        final I18nHelper i18nHelperGermany = mock(I18nHelper.class);
        when(backingI18nFactory.create(Locale.FRANCE, of())).thenReturn(i18nHelperFrance);
        when(backingI18nFactory.create(Locale.GERMANY, of())).thenReturn(i18nHelperGermany);
        when(userLocaleStore.getLocale(frenchUser)).thenReturn(Locale.FRANCE);
        when(userLocaleStore.getLocale(frenchUserApp)).thenReturn(Locale.FRANCE);
        when(userLocaleStore.getLocale((german))).thenReturn(Locale.GERMANY);

        final I18nHelper french1 = i18nFactory.getInstance(frenchUser);
        final I18nHelper french2 = i18nFactory.getInstance(frenchUser);

        assertThat(french1, sameInstance(i18nHelperFrance));
        assertThat(french1, sameInstance(french2));

        final I18nHelper german1 = i18nFactory.getInstance(german);
        assertThat(i18nHelperGermany, sameInstance(german1));

        final I18nHelper french3 = i18nFactory.getInstance(frenchUserApp);
        assertThat(french3, sameInstance(french2));
    }

    @Test
    public void getHelperContainsCorrectTransforms() {
        final TranslationTransform transformAlpha = mock(TranslationTransform.class);
        final TranslationTransform transformBeta = mock(TranslationTransform.class);
        final TranslationTransformModuleDescriptor moduleAlpha = stubTranslationModule(transformAlpha);
        final TranslationTransformModuleDescriptor moduleBeta = stubTranslationModule(transformBeta);
        when(backingI18nFactory.create(Locale.CANADA, ImmutableList.of(transformAlpha, transformBeta)))
                .thenReturn(i18nHelper);
        when(pluginAccessor.getEnabledModuleDescriptorsByClass(TranslationTransformModuleDescriptor.class))
                .thenReturn(asList(moduleAlpha, moduleBeta));

        final I18nHelper instance = i18nFactory.getInstance(Locale.CANADA);
        assertThat(instance, sameInstance(instance));
    }

    @Test
    public void getHelperContainsCorrectTransformsInCorrectOrder() {
        final TranslationTransform transformAlpha = mock(TranslationTransform.class);
        final TranslationTransform transformBeta = mock(TranslationTransform.class);
        final TranslationTransformModuleDescriptor moduleAlpha = stubTranslationModule(2, transformAlpha);
        final TranslationTransformModuleDescriptor moduleBeta = stubTranslationModule(1, transformBeta);
        when(backingI18nFactory.create(Locale.CANADA, ImmutableList.of(transformBeta, transformAlpha)))
                .thenReturn(i18nHelper);
        when(pluginAccessor.getEnabledModuleDescriptorsByClass(TranslationTransformModuleDescriptor.class))
                .thenReturn(asList(moduleAlpha, moduleBeta));

        final I18nHelper instance = i18nFactory.getInstance(Locale.CANADA);
        assertThat(instance, sameInstance(instance));
    }

    @Test
    public void shouldCacheCallsToGetInstance() {
        //having
        final TranslationTransform transform = mock(TranslationTransform.class);
        final TranslationTransformModuleDescriptor module = stubTranslationModule(transform);

        when(backingI18nFactory.create(Locale.CANADA, of(transform)))
                .thenReturn(i18nHelper).thenReturn(null);
        when(pluginAccessor.getEnabledModuleDescriptorsByClass(TranslationTransformModuleDescriptor.class))
                .thenReturn(of(module));

        //when
        final I18nHelper instance1 = i18nFactory.getInstance(Locale.CANADA);
        final I18nHelper instance2 = i18nFactory.getInstance(Locale.CANADA);

        //then
        //We should be getting the same helper even if we getEnabledModuleDescriptorsByClass result changed
        assertThat(instance1, sameInstance(i18nHelper));
        assertThat(instance2, sameInstance(i18nHelper));
    }

    @Test
    public void shouldRefreshTransformsAfterCacheRefresh() {
        final TranslationTransform transformAlpha = mock(TranslationTransform.class);
        final TranslationTransform transformBeta = mock(TranslationTransform.class);
        final TranslationTransformModuleDescriptor moduleAlpha = stubTranslationModule(2, transformAlpha);
        final TranslationTransformModuleDescriptor moduleBeta = stubTranslationModule(1, transformBeta);

        when(backingI18nFactory.create(Locale.CANADA, of(transformAlpha)))
                .thenReturn(i18nHelper);
        final I18nHelper new18nHelper = mock(I18nHelper.class);
        when(backingI18nFactory.create(Locale.CANADA, of(transformBeta)))
                .thenReturn(new18nHelper);
        when(pluginAccessor.getEnabledModuleDescriptorsByClass(TranslationTransformModuleDescriptor.class))
                .thenReturn(of(moduleAlpha))
                .thenReturn(of(moduleBeta));

        //Make sure we get the correct transforms.
        assertThat(i18nFactory.getInstance(Locale.CANADA), sameInstance(i18nHelper));
        assertThat(i18nFactory.getInstance(Locale.CANADA), sameInstance(i18nHelper));

        //Trigger a cache reset. We will not get new transforms.
        i18nFactory.start();
        assertThat(i18nFactory.getInstance(Locale.CANADA), sameInstance(new18nHelper));
    }

    @Test
    public void shouldClearResourceBundleCacheWhenJiraWasStartedAndNewI18NHelperIsBuilt() {
        i18nFactory.start();
        when(backingI18nFactory.create(Locale.CANADA, of())).thenReturn(i18nHelper);
        //populate the cache
        i18nFactory.getInstance(Locale.CANADA);

        verify(resourceBundleCacheCleaner, times(2)).cleanPluginBundlesFromResourceBundleCache();
    }

    @Test
    public void shouldNotClearResourceBundleCacheBeforeJiraWasStarted() {
        when(backingI18nFactory.create(Locale.CANADA, of())).thenReturn(i18nHelper);
        //populate the cache
        i18nFactory.getInstance(Locale.CANADA);

        verify(resourceBundleCacheCleaner, times(0)).cleanPluginBundlesFromResourceBundleCache();
    }

    @Test
    public void registeredAsEventListene() {
        i18nFactory.afterInstantiation();
        Mockito.verify(publisher).register(i18nFactory);
    }

    @Test
    public void shouldClearCacheIfPluginContainsTrackedResources() {
        //having
        final Plugin plugin = stubModuleDescriptorWithPlugin(LanguageModuleDescriptor.class);
        final I18nHelper new18NHelper = mock(I18nHelper.class);
        when(backingI18nFactory.create(Locale.CANADA, of())).thenReturn(i18nHelper).thenReturn(new18NHelper);
        populateCache();

        //At startup, this event will trigger a cache clear.
        i18nFactory.pluginEnabled(new PluginEnabledEvent(plugin));

        //when
        final I18nHelper canada = i18nFactory.getInstance(Locale.CANADA);

        //then
        assertThat(canada, sameInstance(new18NHelper));
    }

    @Test
    public void shouldNotClearCacheIfPluginDoesNotContainTrackedResources() {
        //having
        final Plugin plugin = createPlugin();
        when(backingI18nFactory.create(Locale.CANADA, of())).thenReturn(i18nHelper).thenReturn(null);
        populateCache();
        //At startup, this event not trigger will trigger a cache clear.
        i18nFactory.pluginEnabled(new PluginEnabledEvent(plugin));

        //when
        final I18nHelper canada = i18nFactory.getInstance(Locale.CANADA);

        //then
        assertThat(canada, sameInstance(i18nHelper));
    }

    @Test
    public void shouldClearCacheIfJiraIsNotStartedAndPluginIsInvolved() {
        //having
        final LanguageModuleDescriptor moduleDescriptor = mock(LanguageModuleDescriptor.class);
        final Plugin plugin = attachDescriptorToPlugin(moduleDescriptor);
        final I18nHelper i18nHelperNew = mock(I18nHelper.class);
        when(backingI18nFactory.create(Locale.CANADA, of())).thenReturn(i18nHelper).thenReturn(i18nHelperNew);
        when(pluginAccessor.getEnabledPlugins()).thenReturn(of(plugin));
        populateCache();

        //when
        i18nFactory.pluginModuleDisabled(new PluginModuleDisabledEvent(moduleDescriptor, false));

        //then
        assertThat(i18nFactory.getInstance(Locale.CANADA), sameInstance(i18nHelperNew));
    }

    @Test
    public void shouldNotClearCacheIfJiraIsNotStartedAndPluginIsNotInvolved() {
        //having
        final ModuleDescriptor moduleDescriptor = mock(ModuleDescriptor.class);
        attachDescriptorToPlugin(moduleDescriptor);
        when(backingI18nFactory.create(Locale.CANADA, of())).thenReturn(i18nHelper).thenReturn(null);
        populateCache();

        //when
        i18nFactory.pluginModuleDisabled(new PluginModuleDisabledEvent(moduleDescriptor, false));

        //then
        assertThat(i18nFactory.getInstance(Locale.CANADA), sameInstance(i18nHelper));
    }

    @Test
    public void shouldClearCacheAlwaysWhenJiraStartedOnModuleDisabled() {
        //having
        final I18nHelper i18nHelperNew = mock(I18nHelper.class);
        when(backingI18nFactory.create(Locale.CANADA, of()))
                .thenReturn(i18nHelper)
                .thenReturn(i18nHelperNew);

        i18nFactory.start();
        populateCache();
        final TranslationTransformModuleDescriptor notRelevantModule = mock(TranslationTransformModuleDescriptor.class);
        attachDescriptorToPlugin(notRelevantModule);

        //when
        i18nFactory.pluginModuleDisabled(new PluginModuleDisabledEvent(notRelevantModule, false));

        //then
        assertThat(i18nFactory.getInstance(Locale.CANADA), sameInstance(i18nHelperNew));
    }

    @Test
    public void shouldNotClearCacheIfJiraWasNotStartedOnModuleEnabled() {
        //having
        when(backingI18nFactory.create(Locale.CANADA, of())).thenReturn(i18nHelper).thenReturn(null);
        final TranslationTransformModuleDescriptor transformModule = mock(TranslationTransformModuleDescriptor.class);
        attachDescriptorToPlugin(transformModule);
        populateCache();

        //when
        //When not started even relevant events wont clear the cached.
        i18nFactory.pluginModuleEnabled(new PluginModuleEnabledEvent(transformModule));

        //then
        assertThat(i18nFactory.getInstance(Locale.CANADA), sameInstance(i18nHelper));
    }

    @Test
    public void shouldClearCacheIfJiraWastStartedOnModuleEnabledWithLanguageModuleDescriptor() {
        //having
        final I18nHelper i18nHelperNew = mock(I18nHelper.class);
        when(backingI18nFactory.create(Locale.CANADA, of())).thenReturn(i18nHelper).thenReturn(i18nHelperNew);
        final LanguageModuleDescriptor languageModuleDescriptor = mock(LanguageModuleDescriptor.class);
        attachDescriptorToPlugin(languageModuleDescriptor);
        i18nFactory.start();
        populateCache();
        //when
        i18nFactory.pluginModuleEnabled(new PluginModuleEnabledEvent(languageModuleDescriptor));

        //then
        assertThat(i18nFactory.getInstance(Locale.CANADA), sameInstance(i18nHelperNew));
    }

    @Test
    public void shouldClearCacheIfJiraWasStartedOnModuleEnabledWithI18n() {
        //having
        final I18nHelper i18nHelperNew = mock(I18nHelper.class);
        when(backingI18nFactory.create(Locale.CANADA, of())).thenReturn(i18nHelper).thenReturn(i18nHelperNew);
        final ModuleDescriptor descriptor = mock(ModuleDescriptor.class);
        attachDescriptorToPlugin(descriptor);

        final ResourceDescriptor resource = mock(ResourceDescriptor.class);
        when(resource.getType()).thenReturn(CachingI18nFactory.I18N_RESOURCE_TYPE);
        when(descriptor.getResourceDescriptors()).thenReturn(ImmutableList.of(resource));
        i18nFactory.start();
        populateCache();

        //when
        i18nFactory.pluginModuleEnabled(new PluginModuleEnabledEvent(descriptor));

        //then
        assertThat(i18nFactory.getInstance(Locale.CANADA), sameInstance(i18nHelperNew));
    }

    @Test
    public void shouldNotClearCacheIfJiraWasStartedOnModuleEnabledWithOtherResourceType() {
        //having
        when(backingI18nFactory.create(Locale.CANADA, of())).thenReturn(i18nHelper).thenReturn(null);
        final ModuleDescriptor descriptor = mock(ModuleDescriptor.class);
        attachDescriptorToPlugin(descriptor);

        final ResourceDescriptor resource = mock(ResourceDescriptor.class);
        when(resource.getType()).thenReturn("future type");
        when(descriptor.getResourceDescriptors()).thenReturn(ImmutableList.of(resource));
        i18nFactory.start();
        populateCache();

        //when
        i18nFactory.pluginModuleEnabled(new PluginModuleEnabledEvent(descriptor));

        //then
        assertThat(i18nFactory.getInstance(Locale.CANADA), sameInstance(i18nHelper));
    }


    @Test
    public void shouldClearCacheOnPluginRefreshedEventIfPluginIsTracked() {
        //having
        final I18nHelper i18nHelperNew = mock(I18nHelper.class);
        when(backingI18nFactory.create(Locale.CANADA, of())).thenReturn(i18nHelper).thenReturn(i18nHelperNew);
        final Plugin plugin = stubModuleDescriptorWithPlugin(LanguageModuleDescriptor.class);
        when(pluginAccessor.getEnabledPlugins()).thenReturn(of(plugin));
        populateCache();
        //when
        i18nFactory.pluginRefreshed(new PluginRefreshedEvent(plugin));

        //then
        assertThat(i18nFactory.getInstance(Locale.CANADA), sameInstance(i18nHelperNew));
    }

    @Test
    public void shouldClearCacheIfJiraWasStartedOnModuleEnabledWithTranslationTransformModuleDescriptor() {
        //having
        final I18nHelper i18nHelperNew = mock(I18nHelper.class);
        when(backingI18nFactory.create(Locale.CANADA, of())).thenReturn(i18nHelper).thenReturn(i18nHelperNew);
        final TranslationTransformModuleDescriptor transformModuleDescriptor = mock(TranslationTransformModuleDescriptor.class);
        attachDescriptorToPlugin(transformModuleDescriptor);
        i18nFactory.start();
        populateCache();

        //when
        i18nFactory.pluginModuleEnabled(new PluginModuleEnabledEvent(transformModuleDescriptor));

        //then
        assertThat(i18nFactory.getInstance(Locale.CANADA), sameInstance(i18nHelperNew));
    }

    @Test
    public void shouldNotClearCacheIfJiraWasStartedOnModuleEnabledWithNotInvolvedPlugin() {
        //having
        when(backingI18nFactory.create(Locale.CANADA, of())).thenReturn(i18nHelper).thenReturn(null);
        final ModuleDescriptor descriptor = mock(ModuleDescriptor.class);
        attachDescriptorToPlugin(descriptor);

        i18nFactory.start();
        populateCache();

        //when
        i18nFactory.pluginModuleEnabled(new PluginModuleEnabledEvent(descriptor));

        //then
        assertThat(i18nFactory.getInstance(Locale.CANADA), sameInstance(i18nHelper));
    }

    @Test
    public void getStateHashCodeTracksPlugins() {
        //Hash should remain the same when no changes made.
        final String stateHashCode = i18nFactory.getStateHashCode();
        assertThat(stateHashCode, equalTo(i18nFactory.getStateHashCode()));
    }

    private TranslationTransformModuleDescriptor stubTranslationModule(final TranslationTransform transform) {
        return stubTranslationModule(0, transform);
    }


    private TranslationTransformModuleDescriptor stubTranslationModule(final int order, final TranslationTransform transform) {
        final TranslationTransformModuleDescriptor descriptor = mock(TranslationTransformModuleDescriptor.class);
        final Plugin plugin = createPlugin();

        when(descriptor.getModule()).thenReturn(transform);
        when(descriptor.getPlugin()).thenReturn(plugin);
        when(descriptor.getOrder()).thenReturn(order);

        return descriptor;
    }

    private Plugin createPlugin() {
        final Plugin plugin = mock(Plugin.class);
        when(plugin.getKey()).thenReturn(createPluginKey());
        final PluginInformation pluginInformation = mock(PluginInformation.class);
        when(pluginInformation.getVersion()).thenReturn("version");
        when(plugin.getPluginInformation()).thenReturn(pluginInformation);
        return plugin;
    }

    private String createPluginKey() {
        return String.format("pluign:key:%d", pluginKeyCount++);
    }

    private <T extends ModuleDescriptor> Plugin attachDescriptorToPlugin(final T moduleDescriptor) {
        final Plugin plugin = createPlugin();
        when(moduleDescriptor.getPlugin()).thenReturn(plugin);
        when(plugin.getModuleDescriptors()).thenReturn(ImmutableList.of(moduleDescriptor));
        return plugin;
    }

    private <T extends ModuleDescriptor> Plugin stubModuleDescriptorWithPlugin(final Class<T> descriptorClass) {
        final ModuleDescriptor descriptor = mock(descriptorClass);
        return attachDescriptorToPlugin(descriptor);
    }

    private void populateCache() {//populate the cache
        i18nFactory.getInstance(Locale.CANADA);
    }

}
