package com.atlassian.jira.cluster.lock;

import com.atlassian.beehive.db.spi.ClusterNodeHeartBeatDao;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.event.cluster.HeartbeatEvent;
import com.atlassian.scheduler.JobRunner;
import com.atlassian.scheduler.JobRunnerRequest;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.anyLong;
import static org.mockito.Mockito.verify;

/**
 * Created by pbielicki on 07/12/2016.
 */
public class StartableClusterNodeHeartbeatServiceTest {

    StartableClusterNodeHeartbeatService service;

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    ClusterNodeHeartBeatDao dao;

    @Mock
    EventPublisher publisher;

    @Mock
    JobRunnerRequest request;

    @Before
    public void before() {
        service = new StartableClusterNodeHeartbeatService(dao, null, null, null, publisher);
    }

    @Test
    public void testPublishHeartbeatEvent() throws Exception {
        JobRunner jobRunner = service.newHeartbeatJobRunner();
        jobRunner.runJob(request);
        verify(dao).writeHeartBeat(anyLong());
        verify(publisher).publish(HeartbeatEvent.INSTANCE);
    }
}