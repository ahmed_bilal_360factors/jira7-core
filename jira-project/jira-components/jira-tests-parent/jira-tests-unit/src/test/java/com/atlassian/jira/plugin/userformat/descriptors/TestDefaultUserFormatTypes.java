package com.atlassian.jira.plugin.userformat.descriptors;

import com.atlassian.jira.plugin.userformat.UserFormatModuleDescriptor;
import com.google.common.collect.ImmutableList;
import org.hamcrest.Matchers;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Collections;

import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Holds unit tests for {@link UserFormatTypes}
 *
 * @since v4.4
 */
public class TestDefaultUserFormatTypes {
    @Rule
    public MockitoRule mockito = MockitoJUnit.rule();

    @Mock
    private UserFormatModuleDescriptors mockUserFormatModuleDescriptors;

    @Test
    public void shouldReturnASingleTypeWhenThereIsOneEnabledUserFormatModuleDescriptor() {
        final UserFormatModuleDescriptor mockUserFormatModuleDescriptor = mock(UserFormatModuleDescriptor.class);
        when(mockUserFormatModuleDescriptor.getType()).thenReturn("profileLink");

        when(mockUserFormatModuleDescriptors.get()).thenReturn(ImmutableList.of(mockUserFormatModuleDescriptor));

        DefaultUserFormatTypes defaultUserFormatTypes = new DefaultUserFormatTypes(mockUserFormatModuleDescriptors);

        assertThat(defaultUserFormatTypes.get(), Matchers.contains("profileLink"));
    }

    @Test
    public void shouldReturnASingleTypeWhenThereIsMoreThanOneEnabledUserFormatModuleDescriptorForThatType() {
        final UserFormatModuleDescriptor userFormatModuleDescriptorForTypeProfileLink1 = mock(UserFormatModuleDescriptor.class);
        when(userFormatModuleDescriptorForTypeProfileLink1.getType()).thenReturn("profileLink");

        final UserFormatModuleDescriptor userFormatModuleDescriptorForTypeProfileLink2 = mock(UserFormatModuleDescriptor.class);
        when(userFormatModuleDescriptorForTypeProfileLink2.getType()).thenReturn("profileLink");

        when(mockUserFormatModuleDescriptors.get()).thenReturn(ImmutableList.of
                        (
                                userFormatModuleDescriptorForTypeProfileLink1,
                                userFormatModuleDescriptorForTypeProfileLink2
                        )
        );

        DefaultUserFormatTypes defaultUserFormatTypes = new DefaultUserFormatTypes(mockUserFormatModuleDescriptors);

        assertThat(defaultUserFormatTypes.get(), Matchers.contains("profileLink"));
    }

    @Test
    public void shouldReturnAnEmptyIterableWhenThereAreNoModuleDescriptorsForAnyType() {
        when(mockUserFormatModuleDescriptors.get()).thenReturn(Collections.<UserFormatModuleDescriptor>emptyList());

        DefaultUserFormatTypes defaultUserFormatTypes = new DefaultUserFormatTypes(mockUserFormatModuleDescriptors);

        assertThat(defaultUserFormatTypes.get(), Matchers.emptyIterable());
    }
}
