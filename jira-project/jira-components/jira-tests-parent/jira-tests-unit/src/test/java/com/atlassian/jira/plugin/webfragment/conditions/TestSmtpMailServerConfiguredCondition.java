package com.atlassian.jira.plugin.webfragment.conditions;

import com.atlassian.jira.mail.settings.MailSettings;
import com.atlassian.mail.server.MailServerManager;
import com.atlassian.mail.server.SMTPMailServer;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

public class TestSmtpMailServerConfiguredCondition {
    @Rule
    public MethodRule initMockito = MockitoJUnit.rule();

    @Mock
    private MailSettings settings;
    @Mock
    private MailSettings.Send sendSettings;
    @Mock
    private MailServerManager mailServerManager;
    @Mock
    private SMTPMailServer server;

    @Before
    public void setUp() throws Exception {
        when(settings.send()).thenReturn(sendSettings);
    }

    @Test
    public void shouldNotDisplayWhenSendingIsDisabled() {
        when(sendSettings.isDisabledViaApplicationProperty()).thenReturn(true);
        when(mailServerManager.getDefaultSMTPMailServer()).thenReturn(server);

        assertShouldBeDisabled();

        when(mailServerManager.getDefaultSMTPMailServer()).thenReturn(null);

        assertShouldBeDisabled();
    }

    @Test
    public void shouldNotDisplayWhenSendingIsEnabledAndServerIsNotConfigured() {
        when(sendSettings.isDisabledViaApplicationProperty()).thenReturn(false);
        when(mailServerManager.getDefaultSMTPMailServer()).thenReturn(null);

        assertShouldBeDisabled();
    }

    @Test
    public void shouldDisplayWhenSendingIsEnabledAndServerIsConfigured() {
        when(sendSettings.isDisabledViaApplicationProperty()).thenReturn(false);
        when(mailServerManager.getDefaultSMTPMailServer()).thenReturn(server);

        assertShouldBeEnabled();
    }

    private void assertShouldBeEnabled() {
        assertTrue("Outgoing mail should enabled", isEnabled());
    }

    private void assertShouldBeDisabled() {
        assertFalse("Outgoing mail should be disabled", isEnabled());
    }

    private boolean isEnabled() {
        return SmtpMailServerConfiguredCondition.isOutgoingMailEnabled(settings, mailServerManager);
    }
}
