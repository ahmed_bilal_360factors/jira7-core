package com.atlassian.jira.license;

import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.google.common.collect.ImmutableList;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.mockito.Mock;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 */
public class TestClusterLicenseCheckImpl {
    @Mock
    @AvailableInContainer
    JiraLicenseManager licenseManager;
    @Mock
    LicenseDetails lic1, lic2;
    ClusterLicenseCheck licenseCheck;

    @Rule
    public final RuleChain initMocks = MockitoMocksInContainer.forTest(this);

    @Before
    public void setup() {
        licenseCheck = new ClusterLicenseCheckImpl();
    }

    @Test
    public void checkPassesWhenAllConditionsMet() {
        when(licenseManager.isLicenseSet()).thenReturn(true);
        when(licenseManager.getLicenses()).thenReturn(ImmutableList.of(lic1, lic2));
        when(lic1.isDataCenter()).thenReturn(true);
        when(lic2.isDataCenter()).thenReturn(true);

        LicenseCheck.Result result = licenseCheck.evaluate();
        assertThat(result.isPass(), is(true));
        assertThat(result.getFailedLicenses(), Matchers.<LicenseDetails>empty());
    }

    @Test
    public void checkFailsWhenNoLicenseSet() {
        when(licenseManager.isLicenseSet()).thenReturn(false);

        LicenseCheck.Result result = licenseCheck.evaluate();
        assertThat(result.isPass(), is(false));
        verify(licenseManager, never()).getLicenses();
    }

    @Test
    public void checkFailsWhenNoDataCentreLicenses() {
        when(licenseManager.isLicenseSet()).thenReturn(true);
        when(licenseManager.getLicenses()).thenReturn(ImmutableList.of(lic1, lic2));
        when(lic1.isDataCenter()).thenReturn(false);
        when(lic2.isDataCenter()).thenReturn(false);

        LicenseCheck.Result result = licenseCheck.evaluate();
        assertThat(result.isPass(), is(false));
        assertThat(result.getFailedLicenses(), contains(lic1, lic2));
    }

    @Test
    public void checkFailsWhenDataCentreAndNonDataCenterLicensesAreMixed() {
        when(licenseManager.isLicenseSet()).thenReturn(true);
        when(licenseManager.getLicenses()).thenReturn(ImmutableList.of(lic1, lic2));
        when(lic1.isDataCenter()).thenReturn(false);
        when(lic2.isDataCenter()).thenReturn(true);

        LicenseCheck.Result result = licenseCheck.evaluate();
        assertThat(result.isPass(), is(false));
        assertThat(result.getFailedLicenses(), contains(lic1));
    }
}
