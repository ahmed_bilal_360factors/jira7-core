package com.atlassian.jira.startup;

import com.atlassian.jira.mock.plugin.MockPlugin;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.PluginInformation;
import com.atlassian.plugin.PluginState;
import com.atlassian.plugin.impl.UnloadablePlugin;
import com.atlassian.plugin.metadata.PluginMetadataManager;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import java.util.List;

import static org.hamcrest.Matchers.startsWith;
import static org.hamcrest.collection.IsIterableWithSize.iterableWithSize;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

public class TestPluginInfoProviderImpl {
    private static final String APLUGIN2 = "aplugin2";
    private static final String COM_ATLASSIAN_JIRA_PLUGIN2 = "com.atlassian.jira.plugin2";
    private static final String ZPLUGIN1 = "zplugin1";
    private static final String COM_ATLASSIAN_JIRA_PLUGIN1 = "com.atlassian.jira.plugin1";
    private static final String UNKNOWN = "Unknown";
    @Rule
    public final MethodRule initMockito = MockitoJUnit.rule();
    private List<Plugin> plugins;
    @Mock
    private PluginAccessor pluginAccessor;
    @Mock
    private PluginMetadataManager pluginMetadataManager;
    private PluginInformation pluginInformation;
    private PluginInfoProviderImpl infoProvider;

    @Before
    public void setUp() {
        pluginInformation = new PluginInformation();
        plugins = Lists.newArrayList();
        plugins.add(new MockPlugin(ZPLUGIN1, COM_ATLASSIAN_JIRA_PLUGIN1, pluginInformation, PluginState.DISABLED));
        plugins.add(new MockPlugin(APLUGIN2, COM_ATLASSIAN_JIRA_PLUGIN2, pluginInformation, PluginState.ENABLED));
        plugins.add(new UnloadablePlugin("unloadReasonText"));

        infoProvider = new PluginInfoProviderImpl(pluginAccessor, pluginMetadataManager);
        when(pluginAccessor.getPlugins()).thenReturn(plugins);
    }

    @Test
    public void getSystemPlugins() throws Exception {
        when(pluginMetadataManager.isUserInstalled(any(Plugin.class))).thenReturn(false);

        final PluginInfos systemPlugins = infoProvider.getSystemPlugins();

        standardAsserts(systemPlugins, true);
    }

    @Test
    public void getUserPlugins() throws Exception {
        when(pluginMetadataManager.isUserInstalled(any(Plugin.class))).thenReturn(true);

        final PluginInfos userPlugins = infoProvider.getUserPlugins();

        standardAsserts(userPlugins, false);
    }

    private void standardAsserts(final PluginInfos plugins, final boolean isSystemPlugin) {
        assertThat(plugins, iterableWithSize(3));

        PluginInfo info;

        info = Iterables.get(plugins, 0);
        assertThat(info.getName(), startsWith(UNKNOWN));
        assertThat(info.getKey(), startsWith(UNKNOWN));
        assertThat(info.isSystemPlugin(), is(isSystemPlugin));
        assertThat(info.isEnabled(), is(false));

        info = Iterables.get(plugins, 1);
        assertThat(info.getName(), is(APLUGIN2));
        assertThat(info.getKey(), is(COM_ATLASSIAN_JIRA_PLUGIN2));
        assertThat(info.isSystemPlugin(), is(isSystemPlugin));
        assertThat(info.isEnabled(), is(true));
        assertThat(info.getPluginInformation(), is(pluginInformation));

        info = Iterables.get(plugins, 2);
        assertThat(info.getName(), is(ZPLUGIN1));
        assertThat(info.getKey(), is(COM_ATLASSIAN_JIRA_PLUGIN1));
        assertThat(info.isSystemPlugin(), is(isSystemPlugin));
        assertThat(info.isEnabled(), is(false));
        assertThat(info.getPluginInformation(), is(pluginInformation));
    }

}
