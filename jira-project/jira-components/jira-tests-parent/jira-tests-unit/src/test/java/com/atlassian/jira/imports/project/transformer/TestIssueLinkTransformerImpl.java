package com.atlassian.jira.imports.project.transformer;

import com.atlassian.jira.external.beans.ExternalLink;
import com.atlassian.jira.imports.project.core.BackupSystemInformation;
import com.atlassian.jira.imports.project.mapper.ProjectImportMapperImpl;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.mock.issue.MockIssue;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

/**
 * @since v3.13
 */
public class TestIssueLinkTransformerImpl {
    @Rule
    public MethodRule initMockito = MockitoJUnit.rule();

    @Mock
    BackupSystemInformation mockBackupSystemInformation;
    @Mock
    IssueManager mockIssueManager;

    ExternalLink oldIssueLink;

    ProjectImportMapperImpl projectImportMapper;

    @InjectMocks
    IssueLinkTransformerImpl issueLinkTransformer;

    @Before
    public void setUp() {
        oldIssueLink = new ExternalLink();
        oldIssueLink.setLinkType("12");
        oldIssueLink.setSourceId("101");
        oldIssueLink.setDestinationId("201");
        oldIssueLink.setSequence("3");
        projectImportMapper = new ProjectImportMapperImpl(null, null);
    }

    /**
     * In this test, the source issue id does not have an Issue key in the backup file - this would be invalid data,
     * but we want to safely ignore orphaned data like this.
     */
    @Test
    public void testTransformOrphanedLink() {
        // when:
        final ExternalLink newIssueLink = issueLinkTransformer.transform(projectImportMapper, oldIssueLink);

        // then:
        assertNull(newIssueLink);
        verify(mockBackupSystemInformation).getIssueKeyForId("101");
        verifyZeroInteractions(mockIssueManager);
    }

    /**
     * In this test, the source issue key is not found in the current JIRA.
     * The link cannot be created.
     */
    @Test
    public void testTransformSourceNotInCurrentSystem() {
        // given:
        when(mockBackupSystemInformation.getIssueKeyForId("101")).thenReturn("RAT-1");

        // when:
        final ExternalLink newIssueLink = issueLinkTransformer.transform(projectImportMapper, oldIssueLink);

        // then:
        assertNull(newIssueLink);
        verify(mockIssueManager).getIssueObject("RAT-1");
    }

    /**
     * In this test, the Destination issue key is not found in the current JIRA.
     * The link cannot be created.
     */
    @Test
    public void testTransformDestinationNotInCurrentSystem() {
        // given:
        when(mockBackupSystemInformation.getIssueKeyForId("201")).thenReturn("COW-1");

        // when:
        // Let the source belong to imported Project - therefore it is mapped in the Issue Mapper.
        projectImportMapper.getIssueMapper().mapValue("101", "56");
        final ExternalLink newIssueLink = issueLinkTransformer.transform(projectImportMapper, oldIssueLink);

        // then:
        assertNull(newIssueLink);
        verify(mockIssueManager).getIssueObject("COW-1");
    }

    @Test
    public void testTransformDestinationHappyPath() {
        // given:
        when(mockBackupSystemInformation.getIssueKeyForId("201")).thenReturn("COW-1");
        when(mockIssueManager.getIssueObject("COW-1")).thenReturn(new MockIssue(67));

        projectImportMapper.getIssueLinkTypeMapper().mapValue("12", "34");
        // Let the source belong to imported Project - therefore it is mapped in the Issue Mapper.
        projectImportMapper.getIssueMapper().mapValue("101", "56");

        // when:
        // Finally do the transform
        final ExternalLink newIssueLink = issueLinkTransformer.transform(projectImportMapper, oldIssueLink);

        // then:
        assertEquals("34", newIssueLink.getLinkType());
        assertEquals("56", newIssueLink.getSourceId());
        assertEquals("67", newIssueLink.getDestinationId());
        assertEquals("3", newIssueLink.getSequence());
    }
}
