package com.atlassian.jira.issue.search.searchers.util;

import com.atlassian.jira.issue.search.ClauseNames;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.issue.search.searchers.transformer.FieldFlagOperandRegistry;
import com.atlassian.jira.jql.operand.JqlOperandResolver;
import com.atlassian.jira.jql.operand.OperandHandler;
import com.atlassian.jira.jql.resolver.IndexInfoResolver;
import com.atlassian.jira.jql.validator.MockJqlOperandResolver;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.collect.CollectionBuilder;
import com.atlassian.query.QueryImpl;
import com.atlassian.query.clause.OrClause;
import com.atlassian.query.clause.TerminalClause;
import com.atlassian.query.clause.TerminalClauseImpl;
import com.atlassian.query.operand.EmptyOperand;
import com.atlassian.query.operand.FunctionOperand;
import com.atlassian.query.operand.MultiValueOperand;
import com.atlassian.query.operand.Operand;
import com.atlassian.query.operand.SingleValueOperand;
import com.atlassian.query.operator.Operator;
import com.google.common.collect.ImmutableSet;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestDefaultIndexedInputHelper {
    private ApplicationUser theUser = null;
    @Rule
    public MockitoRule mockito = MockitoJUnit.rule();
    @Mock
    private SearchRequest searchRequest;
    @Mock
    private FieldFlagOperandRegistry fieldFlagOperandRegistry;
    @Mock
    private IndexInfoResolver indexInfoResolver;

    @Test
    public void testGetClauseForNavigatorValuesEmptySet() throws Exception {
        final String fieldName = "testfield";

        DefaultIndexedInputHelper helper = createHelper();
        assertNull(helper.getClauseForNavigatorValues(fieldName, Collections.<String>emptySet()));
    }

    @Test
    public void testGetClauseForNavigatorValuesOneValueNotFlag() throws Exception {
        final String fieldName = "testfield";

        final String id = "45";
        final TerminalClause expectedClause = new TerminalClauseImpl(fieldName, Operator.EQUALS, 45L);

        DefaultIndexedInputHelper helper = createHelper();
        assertEquals(expectedClause, helper.getClauseForNavigatorValues(fieldName, Collections.singleton(id)));
    }

    @Test
    public void testGetClauseForNavigatorValuesTwoValuesNotFlag() throws Exception {
        final String fieldName = "testfield";

        final String id1 = "45";
        final String id2 = "888";
        final TerminalClause expectedClause = new TerminalClauseImpl(fieldName, Operator.IN, new MultiValueOperand(45L, 888L));

        DefaultIndexedInputHelper helper = createHelper();
        assertEquals(expectedClause, helper.getClauseForNavigatorValues(fieldName, CollectionBuilder.newBuilder(id1, id2).asListOrderedSet()));
    }

    @Test
    public void testGetClauseForNavigatorValuesTwoValuesOneNotNumber() throws Exception {
        final String fieldName = "testfield";

        final String id1 = "45";
        final String id2 = "notanumber";
        final TerminalClause expectedClause = new TerminalClauseImpl(fieldName, Operator.IN, new MultiValueOperand(new SingleValueOperand(45L), new SingleValueOperand("notanumber")));

        DefaultIndexedInputHelper helper = createHelper();
        assertEquals(expectedClause, helper.getClauseForNavigatorValues(fieldName, CollectionBuilder.newBuilder(id1, id2).asListOrderedSet()));
    }

    @Test
    public void testGetClauseForNavigatorValuesOneValueIsListFlag() throws Exception {
        final String fieldName = "testfield";

        final String id = "-3";
        final Operand flagOperand = new MultiValueOperand(45L);
        final TerminalClause expectedClause = new TerminalClauseImpl(fieldName, Operator.IN, flagOperand);
        when(fieldFlagOperandRegistry.getOperandForFlag(fieldName, id)).thenReturn(flagOperand);

        DefaultIndexedInputHelper helper = createHelper();
        assertEquals(expectedClause, helper.getClauseForNavigatorValues(fieldName, Collections.singleton(id)));
    }

    @Test
    public void testGetClauseForNavigatorValuesOneValueIsFlagNotList() throws Exception {
        final String fieldName = "testfield";

        final String id = "-1";
        final Operand flagOperand = EmptyOperand.EMPTY;
        final TerminalClause expectedClause = new TerminalClauseImpl(fieldName, Operator.EQUALS, flagOperand);
        when(fieldFlagOperandRegistry.getOperandForFlag(fieldName, id)).thenReturn(flagOperand);

        DefaultIndexedInputHelper helper = createHelper();
        assertEquals(expectedClause, helper.getClauseForNavigatorValues(fieldName, Collections.singleton(id)));
    }

    @Test
    public void testGetClauseForNavigatorValuesTwoValuesOneIsListFlag() throws Exception {
        final String fieldName = "testfield";

        final String id1 = "-3";
        final String id2 = "888";
        final Operand flagOperand = new MultiValueOperand(45L);
        final Operand singleOperand = new SingleValueOperand(888L);
        final Operand expectedMultiOperand = new MultiValueOperand(CollectionBuilder.newBuilder(flagOperand, singleOperand).asList());
        final TerminalClause expectedClause = new TerminalClauseImpl(fieldName, Operator.IN, expectedMultiOperand);
        when(fieldFlagOperandRegistry.getOperandForFlag(fieldName, id1)).thenReturn(flagOperand);

        DefaultIndexedInputHelper helper = createHelper();
        assertEquals(expectedClause, helper.getClauseForNavigatorValues(fieldName, CollectionBuilder.newBuilder(id1, id2).asListOrderedSet()));
    }

    @Test
    public void testGetAllNavigatorValuesAsStringsNoWhereClause() throws Exception {
        DefaultIndexedInputHelper helper = createHelper();
        final Set<String> strings = helper.getAllNavigatorValuesForMatchingClauses(theUser, new ClauseNames("balrg"), new QueryImpl());
        assertEquals(0, strings.size());
    }

    @Test
    public void testGetAllIndexValuesAsStringsNoWhereClause() throws Exception {
        DefaultIndexedInputHelper helper = createHelper();
        final Set<String> strings = helper.getAllIndexValuesForMatchingClauses(theUser, new ClauseNames("balrg"), new QueryImpl());
        assertEquals(0, strings.size());
    }

    @Test
    public void testGetAllNavigatorValuesAsStringsFunctionalOperandHasFlag() throws Exception {
        final String fieldName = "testfield";
        final FunctionOperand opFunc = new FunctionOperand("func");

        when(fieldFlagOperandRegistry.getFlagForOperand(fieldName, opFunc)).thenReturn(ImmutableSet.of("-2"));
        final OperandHandler operandHandler = mock(OperandHandler.class);

        MockJqlOperandResolver jqlOperandSupport = new MockJqlOperandResolver();
        jqlOperandSupport.addHandler("func", operandHandler);

        final TerminalClauseImpl terminalClause = new TerminalClauseImpl(fieldName, Operator.EQUALS, opFunc);

        DefaultIndexedInputHelper helper = createHelper();
        assertNavigatorValidForSingleClause(helper, terminalClause, "-2");
    }

    @Test
    public void testNavigatorValuesWithMultiValueOperandsContainsFlaggedFuncsAndOperands() throws Exception {
        final String fieldName = "testfield";
        final SingleValueOperand opAnything = new SingleValueOperand("ImAFlag");
        final FunctionOperand opFunction = new FunctionOperand("myFunction");
        final MultiValueOperand opMulti = new MultiValueOperand(CollectionBuilder.newBuilder(opAnything, opFunction).asList());

        when(fieldFlagOperandRegistry.getFlagForOperand(fieldName, opMulti)).thenReturn(null);
        when(fieldFlagOperandRegistry.getFlagForOperand(fieldName, opAnything)).thenReturn(ImmutableSet.of("-1"));
        when(fieldFlagOperandRegistry.getFlagForOperand(fieldName, opFunction)).thenReturn(ImmutableSet.of("-2"));

        final OperandHandler operandHandler = mock(OperandHandler.class);

        MockJqlOperandResolver jqlOperandSupport = new MockJqlOperandResolver();
        jqlOperandSupport.addHandler(SingleValueOperand.OPERAND_NAME, operandHandler);
        jqlOperandSupport.addHandler("myFunction", operandHandler);

        final QueryImpl searchQuery = new QueryImpl(new OrClause(new TerminalClauseImpl(fieldName, Operator.IN, opMulti)));
        when(searchRequest.getQuery()).thenReturn(searchQuery);

        DefaultIndexedInputHelper helper = createHelper(jqlOperandSupport);

        assertNavigatorValidForSearchRequest(fieldName, helper, "-1", "-2");
    }

    private void assertNavigatorValidForSingleClause(final DefaultIndexedInputHelper<?> helper, final TerminalClause clause, final String... contains) {
        final Set<String> strings = helper.getAllNavigatorValues(theUser, clause.getName(), clause.getOperand(), clause);
        final List<String> expected = Arrays.asList(contains);
        assertEquals(expected.size(), strings.size());
        assertTrue(expected.containsAll(strings));
    }

    private void assertNavigatorValidForSearchRequest(final String fieldName, final DefaultIndexedInputHelper<?> helper, final String... contains) {
        final Set<String> strings = helper.getAllNavigatorValuesForMatchingClauses(theUser, new ClauseNames(fieldName), searchRequest.getQuery());

        final List<String> expected = Arrays.asList(contains);
        assertEquals(expected.size(), strings.size());
        assertTrue(expected.containsAll(strings));

    }

    private DefaultIndexedInputHelper createHelper() {
        return new DefaultIndexedInputHelper(indexInfoResolver, MockJqlOperandResolver.createSimpleSupport(), fieldFlagOperandRegistry);
    }

    private DefaultIndexedInputHelper createHelper(JqlOperandResolver jqlOperandResolver) {
        return new DefaultIndexedInputHelper(indexInfoResolver, jqlOperandResolver, fieldFlagOperandRegistry);
    }

}
