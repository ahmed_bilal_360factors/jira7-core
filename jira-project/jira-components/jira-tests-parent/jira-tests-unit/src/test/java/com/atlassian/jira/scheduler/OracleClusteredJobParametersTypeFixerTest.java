package com.atlassian.jira.scheduler;

import com.atlassian.jira.config.database.DatabaseConfig;
import com.atlassian.jira.config.database.DatabaseConfigurationManager;
import com.atlassian.jira.config.database.Datasource;
import com.atlassian.jira.junit.rules.MockitoContainer;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import static com.atlassian.jira.matchers.EitherMatchers.left;
import static com.atlassian.jira.matchers.EitherMatchers.right;
import static com.atlassian.jira.scheduler.OracleClusteredJobParametersTypeFixer.INDEXES;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

public class OracleClusteredJobParametersTypeFixerTest {
    @Rule
    public MockitoContainer mockitoContainer = new MockitoContainer(this);

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Mock
    private Connection conn;

    @Mock
    private Statement statement;

    @Mock
    private DatabaseConfigurationManager dbConfigManager;

    private DatabaseConfig dbConfig;
    private String parametersColumnType;
    private String schemaName = "";
    private OracleClusteredJobParametersTypeFixer fixture;

    @Before
    public void setUp() throws SQLException {
        when(conn.createStatement()).thenReturn(statement);

        dbConfig = new DatabaseConfig("oracle10g", "jira_user", mock(Datasource.class));
        when(dbConfigManager.isDatabaseSetup()).thenReturn(true);
        when(dbConfigManager.getDatabaseConfiguration()).thenAnswer(invocation -> dbConfig);

        fixture = new Fixture();
    }

    @Test
    public void doesNothingOnH2() {
        givenH2();

        assertThat(fixture.asStartupCheck().isOk(), is(true));
        assertThat(fixture.getResult(), right(false));

        verifyZeroInteractions(statement);
    }

    @Test
    public void fixDoesNothingIfColumnTypeIsBlob() throws SQLException {
        givenEverythingFails();
        parametersColumnType = "Blob";

        assertThat(fixture.asStartupCheck().isOk(), is(true));
        assertThat(fixture.getResult(), right(false));

        verifyZeroInteractions(statement);
    }

    @Test
    public void fixPerformedWhenColumnTypeIsLongRaw() throws SQLException {
        givenEverythingSucceeds();
        parametersColumnType = "LONG RAW";

        assertThat(fixture.asStartupCheck().isOk(), is(true));
        assertThat(fixture.getResult(), right(true));

        assertSql("ALTER TABLE clusteredjob MODIFY (parameters BLOB)");
        INDEXES.forEach(index -> assertSql("ALTER INDEX " + index + " REBUILD"));
    }

    @Test
    public void applyFixWorksToo() throws SQLException {
        givenEverythingSucceeds();
        parametersColumnType = "LONG RAW";

        assertThat(fixture.fix(), is(true));
        assertThat(fixture.getResult(), right(true));

        assertSql("ALTER TABLE clusteredjob MODIFY (parameters BLOB)");
        INDEXES.forEach(index -> assertSql("ALTER INDEX " + index + " REBUILD"));
    }

    @Test
    public void schemaNameGetsUsedWhenSet() throws SQLException {
        schemaName = "JIRA_USER";
        givenEverythingSucceeds();

        assertThat(fixture.fix(), is(true));
        assertThat(fixture.getResult(), right(true));

        assertSql("ALTER TABLE JIRA_USER.clusteredjob MODIFY (parameters BLOB)");
        INDEXES.forEach(index -> assertSql("ALTER INDEX JIRA_USER." + index + " REBUILD"));
    }

    @Test
    public void alterTableFailuresAreReported() throws SQLException {
        givenEverythingFails();

        assertThat(fixture.fix(), is(false));
        assertThat(fixture.getResult(), left(instanceOf(SQLException.class)));

        assertSql("ALTER TABLE clusteredjob MODIFY (parameters BLOB)");
        verify(statement).close();
        verifyNoMoreInteractions(statement);
    }

    @Test
    public void indexRebuildFailuresAreIgnored() throws SQLException {
        givenIndexRebuildsFail();

        assertThat(fixture.fix(), is(true));
        assertThat(fixture.getResult(), right(true));

        assertSql("ALTER TABLE clusteredjob MODIFY (parameters BLOB)");
        INDEXES.forEach(index -> assertSql("ALTER INDEX " + index + " REBUILD"));
    }


    private void givenH2() {
        dbConfig = new DatabaseConfig("h2", "public", mock(Datasource.class));
    }

    private void givenEverythingFails() throws SQLException {
        when(statement.execute(anyString())).thenAnswer(invocation -> {
            throw new SQLException("Nope!");
        });
    }

    private void givenEverythingSucceeds() throws SQLException {
        when(statement.execute(anyString())).thenReturn(true);
    }

    private void givenIndexRebuildsFail() throws SQLException {
        final SQLException sqle = new SQLException("Nope!");

        when(statement.execute(anyString())).thenAnswer(invocation -> {
            final String sql = (String) invocation.getArguments()[0];
            if (sql.contains("REBUILD")) {
                throw sqle;
            }
            return false;
        });
    }

    private void assertSql(String sql) {
        try {
            verify(statement).execute(sql);
        } catch (SQLException e) {
            throw new AssertionError(e);
        }
    }


    class Fixture extends OracleClusteredJobParametersTypeFixer {
        public Fixture() {
            super(dbConfigManager);
        }

        @Override
        Connection getConnection() throws SQLException {
            return conn;
        }

        @Override
        String getSchemaName() {
            return schemaName;
        }

        @Override
        String getParametersColumnType() {
            return parametersColumnType;
        }
    }
}