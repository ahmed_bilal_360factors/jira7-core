package com.atlassian.jira.upgrade.tasks.role;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.crowd.embedded.impl.DelegatingGroupWithAttributes;
import com.atlassian.crowd.embedded.impl.ImmutableAttributes;
import com.atlassian.crowd.embedded.impl.ImmutableGroup;
import com.atlassian.crowd.embedded.impl.ImmutableUser;
import com.atlassian.fugue.Iterables;
import com.atlassian.jira.auditing.AssociatedItem;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.mock.MockApplicationProperties;
import com.google.common.collect.ImmutableMap;
import org.junit.Before;
import org.junit.Test;

import java.util.Collections;

import static com.atlassian.jira.application.ApplicationKeys.SERVICE_DESK;
import static com.atlassian.jira.upgrade.tasks.role.MoveJira6xABPServiceDeskPermissions.PROPERTY_SERVICEDESK_NOT_MIGRATED_GROUPS;
import static com.atlassian.jira.upgrade.tasks.role.TestUtils.asGroups;
import static com.atlassian.jira.upgrade.tasks.role.TestUtils.newGroup;
import static com.google.common.collect.ImmutableSet.of;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.collection.IsEmptyCollection.empty;
import static org.hamcrest.collection.IsIterableContainingInOrder.contains;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class MoveJira6xABPServiceDeskPermissionsTest {
    private MigrationGroupService migrationGroupService;
    private GlobalPermissionDao globalPermissionDao;
    private ApplicationProperties applicationProperties;

    private MoveJira6xABPServiceDeskPermissions task;

    @Before
    public void setup() {
        migrationGroupService = mock(MigrationGroupService.class);
        globalPermissionDao = mock(GlobalPermissionDao.class);
        applicationProperties = new MockApplicationProperties();
        task = new MoveJira6xABPServiceDeskPermissions(migrationGroupService, globalPermissionDao, applicationProperties);
    }

    @Test
    public void groupWithAgentAccessIsGivenServiceDeskApplicationRoleIfAllUsersHasUse() {
        when(globalPermissionDao.groupsWithSdAgentPermission()).thenReturn(asGroups("sd-agents"));
        when(migrationGroupService.getUsersInGroup(newGroup("sd-agent")))
                .thenReturn(of(new UserWithPermissions(newUser("user1"), true, false)));

        final MigrationState result = executeTask(TestUtils.emptyState());

        assertThat(result.applicationRoles().get(SERVICE_DESK).get().groups(),
                contains(newGroup("sd-agents")));
        assertThat(applicationProperties.getString(PROPERTY_SERVICEDESK_NOT_MIGRATED_GROUPS), is(nullValue()));
    }

    @Test
    public void notMigratedPropertyIsOverwritten() {
        when(globalPermissionDao.groupsWithSdAgentPermission()).thenReturn(asGroups("sd-agents"));
        when(migrationGroupService.getUsersInGroup(newGroup("sd-agent")))
                .thenReturn(of(new UserWithPermissions(newUser("user1"), true, false)));

        applicationProperties.setString(PROPERTY_SERVICEDESK_NOT_MIGRATED_GROUPS, "some-not-migrated-group");

        executeTask(TestUtils.emptyState());

        assertThat(applicationProperties.getString(PROPERTY_SERVICEDESK_NOT_MIGRATED_GROUPS), is(nullValue()));
    }

    @Test
    public void groupServiceDeskAgentsWithAgentAccessAndSDAttributeIsGivenServiceDeskApplicationRoleAndSetAsDefault() {
        when(globalPermissionDao.groupsWithSdAgentPermission()).thenReturn(asGroups("service-desk-agents"));
        when(migrationGroupService.getUsersInGroup(newGroup("service-desk-agents"))).thenReturn(Collections.emptySet());
        givenGroupWithAttributes("service-desk-agents", "synch.created.by.jira.service.desk",
                "synch.created.by.jira.service.desk");

        final MigrationState result = executeTask(TestUtils.emptyState());

        final ApplicationRole sdRole = result.applicationRoles().get(SERVICE_DESK).get();
        assertThat(sdRole.groups(), contains(newGroup("service-desk-agents")));
        assertThat(sdRole.defaultGroups(), contains(newGroup("service-desk-agents")));
        assertThat(applicationProperties.getString(PROPERTY_SERVICEDESK_NOT_MIGRATED_GROUPS), is(nullValue()));
    }

    @Test
    public void groupServiceDeskAgentsIsNotSetAsDefaultIfItDoesNotHaveSDAttribute() {
        when(globalPermissionDao.groupsWithSdAgentPermission()).thenReturn(asGroups("service-desk-agents"));
        when(migrationGroupService.getUsersInGroup(newGroup("service-desk-agents"))).thenReturn(Collections.emptySet());

        MigrationState state = TestUtils.emptyState();
        final MigrationState result = executeTask(state);

        final ApplicationRole sdRole = result.applicationRoles().get(SERVICE_DESK).get();
        assertThat(sdRole.groups(), contains(newGroup("service-desk-agents")));
        assertThat(sdRole.defaultGroups(), is(empty()));
        assertThat(applicationProperties.getString(PROPERTY_SERVICEDESK_NOT_MIGRATED_GROUPS), is(nullValue()));
    }

    @Test
    public void groupServiceDeskAgentsIsNotSetAsDefaultIfItHasAdminPermission() {
        when(globalPermissionDao.groupsWithSdAgentPermission()).thenReturn(asGroups("service-desk-agents"));
        when(globalPermissionDao.groupsWithAdminPermission()).thenReturn(asGroups("service-desk-agents"));
        when(migrationGroupService.getUsersInGroup(newGroup("service-desk-agents"))).thenReturn(Collections.emptySet());
        givenGroupWithAttributes("service-desk-agents", "synch.created.by.jira.service.desk",
                "synch.created.by.jira.service.desk");

        final MigrationState result = executeTask(TestUtils.emptyState());

        final ApplicationRole sdRole = result.applicationRoles().get(SERVICE_DESK).get();
        assertThat(sdRole.groups(), contains(newGroup("service-desk-agents")));
        assertThat(sdRole.defaultGroups(), is(empty()));
        assertThat(applicationProperties.getString(PROPERTY_SERVICEDESK_NOT_MIGRATED_GROUPS), is(nullValue()));
    }

    @Test
    public void groupWithAgentAccessIsNotMigratedIfAllUsersCannotLogin() {
        when(globalPermissionDao.groupsWithSdAgentPermission()).thenReturn(asGroups("service-desk-agents"));
        when(migrationGroupService.getUsersInGroup(newGroup("service-desk-agents"))).thenReturn(Collections.emptySet());
        when(migrationGroupService.getUsersInGroup(newGroup("service-desk-agents")))
                .thenReturn(of(new UserWithPermissions(newUser("user1"), false, false)));

        final MigrationState initialState = TestUtils.emptyState().changeApplicationRole(SERVICE_DESK, role -> role);
        final MigrationState result = executeTask(initialState);

        final ApplicationRole sdRole = result.applicationRoles().get(SERVICE_DESK).get();
        assertThat(sdRole.groups(), is(empty()));
        assertThat(sdRole.defaultGroups(), is(empty()));

        final AuditEntry entry = Iterables.first(result.log().events()).get();
        assertThat(entry.getSeverity(), is(AuditEntrySeverity.INFO));
        assertThat(entry.getSummary(), is("Service Desk not migrated"));
        assertTrue("Cloud admins should be able to see this log", entry.eventShowsInCloudLog());
        assertThat(applicationProperties.getString(PROPERTY_SERVICEDESK_NOT_MIGRATED_GROUPS), is(nullValue()));
    }

    @Test
    public void groupWithAgentAccessIsNotMigratedIfSomeUsersCannotLogin() {
        when(globalPermissionDao.groupsWithSdAgentPermission()).thenReturn(asGroups("service-desk-agents"));
        when(globalPermissionDao.groupsWithSdAgentPermission()).thenReturn(asGroups("service-desk-agents"));
        when(migrationGroupService.getUsersInGroup(newGroup("service-desk-agents")))
                .thenReturn(of(new UserWithPermissions(newUser("user1"), false, false),
                        new UserWithPermissions(newUser("user2"), true, false)));

        final MigrationState initialState = TestUtils.emptyState().changeApplicationRole(SERVICE_DESK, role -> role);
        final MigrationState result = executeTask(initialState);

        final ApplicationRole sdRole = result.applicationRoles().get(SERVICE_DESK).get();
        assertThat(sdRole.groups(), is(empty()));
        assertThat(sdRole.defaultGroups(), is(empty()));

        AuditEntry entry = Iterables.first(result.log().events()).get();
        assertThat(entry.getSourceClass(), equalTo(MoveJira6xABPServiceDeskPermissions.class));
        assertThat(entry.getSummary(), is("Service Desk not migrated"));
        assertThat(entry.getDescription(), is(
                "Service Desk Migration - cannot add Service Desk application role to group: 'service-desk-agents'\n"
                        + "Group contains users with misconfigured permissions - not all users in the group are able to "
                        + "log in to JIRA, migration would cause privilege escalation.\n"
                        + "To fix this issue, disable these users or remove them from the group then associate that "
                        + "group with the Service Desk application.\n"
                        + "Agents without use permission:\n"
                        + "user1"));
        assertThat(entry.getType(), is(AssociatedItem.Type.APPLICATION_ROLE));
        assertThat(entry.getSeverity(), is(AuditEntrySeverity.WARNING));
        assertTrue("Cloud admins should be able to see this log", entry.eventShowsInCloudLog());
        assertThat(applicationProperties.getString(PROPERTY_SERVICEDESK_NOT_MIGRATED_GROUPS), is("service-desk-agents"));
    }

    private MigrationState executeTask(final MigrationState state) {
        MigrationState result = task.migrate(state, false);
        result.afterSaveTasks().forEach(Runnable::run);
        return result;
    }

    private void givenGroupWithAttributes(String groupName, String attribute, String value) {
        when(migrationGroupService.getGroupWithAttributes(newGroup(groupName))).thenReturn(
                new DelegatingGroupWithAttributes(new ImmutableGroup(groupName),
                        new ImmutableAttributes(ImmutableMap.of(attribute, Collections.singleton(value)))
                )
        );
    }

    private User newUser(String name) {
        return ImmutableUser.newUser().name(name).toUser();
    }

}