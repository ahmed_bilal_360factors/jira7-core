package com.atlassian.jira.bc.user.search;

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableList;
import org.junit.Test;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class TestIteratedSearcher {
    @Test
    public void testNoFiltering() {
        IteratedSearcher searcher = new IteratedSearcher();
        List<Integer> results = searcher.iteratedSearch(new NumberSource(), Predicates.alwaysTrue(), 5);

        assertEquals("Wrong result.",
                ImmutableList.of(0, 1, 2, 3, 4),
                results);
    }

    @Test
    public void testMultipleSearches() {
        IteratedSearcher searcher = new IteratedSearcher();
        List<Integer> results = searcher.iteratedSearch(new NumberSource(), new NumberContainsDigit(2), 10);

        assertEquals("Wrong result.",
                ImmutableList.of(2, 12, 20, 21, 22, 23, 24, 25, 26, 27),
                results);
    }

    @Test
    public void testManySearches() {
        IteratedSearcher searcher = new IteratedSearcher();
        List<Integer> results = searcher.iteratedSearch(new NumberSource(), new IntegerNthRoot(3), 10);

        assertEquals("Wrong result.",
                ImmutableList.of(0, 1, 8, 27, 64, 125, 216, 343, 512, 729),
                results);
    }

    @Test
    public void testHittingMaxResultsLimit() {
        IteratedSearcher searcher = new IteratedSearcher();
        List<Integer> results = searcher.iteratedSearch(new NumberSource(), new IntegerNthRoot(3), 25);

        assertEquals("Wrong result.",
                ImmutableList.of(0, 1, 8, 27, 64, 125, 216, 343, 512, 729, 1000, 1331, 1728, 2197, 2744, 3375, 4096, 4913, 5832, 6859, 8000, 9261, 10648, 12167, 13824),
                results);
    }

    @Test
    public void testHittingMaxResultsLimitOnList() {
        IteratedSearcher searcher = new IteratedSearcher();
        List<Integer> results = searcher.iteratedSearch(new ListSource<>(Arrays.asList(1, 2, 3, 4)), new NumberContainsDigit(3), 1);

        assertEquals("Wrong result.",
                ImmutableList.of(3),
                results);
    }

    @Test
    public void testEndOfSource() {
        IteratedSearcher searcher = new IteratedSearcher();
        List<Integer> results = searcher.iteratedSearch(new ListSource<>(Arrays.asList(1, 2, 3, 4)), new NumberContainsDigit(3), 10);

        assertEquals("Wrong result.",
                ImmutableList.of(3),
                results);
    }

    @Test
    public void testSearchZeroUnfilteredResults() {
        IteratedSearcher searcher = new IteratedSearcher();
        List<Integer> results = searcher.iteratedSearch(new ListSource<>(Collections.<Integer>emptyList()), new NumberContainsDigit(3), 10);

        assertEquals("Wrong result.",
                Collections.emptyList(),
                results);
    }

    private static class NumberSource implements IteratedSearcher.Source<Integer> {
        @Override
        public List<Integer> search(int offset, int limit) {
            List<Integer> results = new ArrayList<>(limit);
            for (int i = offset; i < offset + limit; i++) {
                results.add(i);
            }
            return (results);
        }
    }

    private static class ListSource<T> implements IteratedSearcher.Source<T> {
        private final List<T> items;

        public ListSource(List<T> items) {
            this.items = items;
        }

        @Override
        public List<T> search(int offset, int limit) {
            return items.subList(offset, Math.min(items.size(), offset + limit));
        }
    }

    private static class NumberContainsDigit implements Predicate<Number> {
        private final String digitString;

        public NumberContainsDigit(int digit) {
            digitString = String.valueOf(digit);
        }

        @Override
        public boolean apply(@Nullable Number input) {
            return String.valueOf(input).contains(digitString);
        }
    }

    private static class IntegerNthRoot implements Predicate<Number> {
        private final int n;

        public IntegerNthRoot(int n) {
            this.n = n;
        }

        @Override
        public boolean apply(@Nullable Number input) {
            if (input == null) {
                return false;
            }

            double num = input.doubleValue();
            double root = Math.pow(num, 1.0 / n);
            root = Math.round(root);
            double diff = Math.abs(Math.pow(root, n) - num);
            return (diff < 1.0E-6);

        }
    }
}
