package com.atlassian.jira.project.template.descriptor;


import com.atlassian.plugin.PluginParseException;
import org.dom4j.Attribute;
import org.dom4j.Element;
import org.dom4j.Node;
import org.junit.Test;

import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestDefaultPluginParseHelper {
    @Test
    public void theRawElementIsCorrectlyReturned() {
        Element element = anyElement();

        DefaultPluginParseHelper parser = new DefaultPluginParseHelper(element, anyXpath());

        assertThat(parser.rawElement(), is(element));
    }

    @Test
    public void theTextOfTheElementIsCorrectlyReturned() {
        Element element = elementWithText("expected-text");

        DefaultPluginParseHelper parser = new DefaultPluginParseHelper(element, anyXpath());

        assertThat(parser.text(), is("expected-text"));
    }

    @Test(expected = PluginParseException.class)
    public void anExceptionIsThrownIfARequiredElementIsNotPresent() {
        Element element = elementNotFinding("sub-element");

        DefaultPluginParseHelper parser = new DefaultPluginParseHelper(element, anyXpath());

        parser.element("sub-element");
    }

    @Test(expected = PluginParseException.class)
    public void anExceptionIsThrownIfARequiredElementIsPresentButHasAnUnexpectedClass() {
        Element element = elementWithSubElementOfUnexpectedClass("sub-element");

        DefaultPluginParseHelper parser = new DefaultPluginParseHelper(element, anyXpath());

        parser.element("sub-element");
    }

    @Test
    public void aRequiredElementOfTheExpectedClassIsCorrectlyReturned() {
        Element expectedSubElement = anyElement();
        Element element = elementWithSubElement("sub-element", expectedSubElement);

        DefaultPluginParseHelper parser = new DefaultPluginParseHelper(element, anyXpath());

        assertThat(parser.element("sub-element").rawElement(), is(expectedSubElement));
    }

    @Test
    public void emptyIsReturnedForAnOptionalElementThatIsNotPresent() {
        Element element = elementNotFinding("sub-element");

        DefaultPluginParseHelper parser = new DefaultPluginParseHelper(element, anyXpath());
        Optional<PluginParseHelper> result = parser.optElement("sub-element");

        assertThat(result.isPresent(), is(false));
    }

    @Test(expected = PluginParseException.class)
    public void anExceptionIsThrownForAnOptionalElementThatIsPresentButHasAnUnexpectedClass() {
        Element element = elementWithSubElementOfUnexpectedClass("sub-element");

        DefaultPluginParseHelper parser = new DefaultPluginParseHelper(element, anyXpath());

        parser.optElement("sub-element");
    }

    @Test
    public void anOptionalElementThatIsPresentAndHasTheExpectedClassIsCorrectlyReturned() {
        Element expectedSubElement = anyElement();
        Element element = elementWithSubElement("sub-element", expectedSubElement);

        DefaultPluginParseHelper parser = new DefaultPluginParseHelper(element, anyXpath());

        assertThat(parser.optElement("sub-element").get().rawElement(), is(expectedSubElement));
    }

    @Test(expected = PluginParseException.class)
    public void anExceptionIsThrownForARequiredAttributeThatIsNotPresent() {
        Element element = elementWithoutAttribute("some-attribute");

        DefaultPluginParseHelper parser = new DefaultPluginParseHelper(element, anyXpath());

        parser.attribute("some-attribute");
    }

    @Test
    public void theValueOfARequiredAttributeThatIsPresentIsCorrectlyReturned() {
        Element element = elementWithAttribute("attribute", "value");

        DefaultPluginParseHelper parser = new DefaultPluginParseHelper(element, anyXpath());

        assertThat(parser.attribute("attribute"), is("value"));
    }

    @Test
    public void nullIsReturnedForAnOptionalAttributeThatIsNotPresent() {
        Element element = elementWithoutAttribute("some-attribute");

        DefaultPluginParseHelper parser = new DefaultPluginParseHelper(element, anyXpath());

        assertThat(parser.optAttribute("some-attribute"), is(nullValue()));
    }

    @Test
    public void theValueOfAnOptionalAttributeThatIsPresentIsCorrectlyReturned() {
        Element element = elementWithAttribute("attribute", "value");

        DefaultPluginParseHelper parser = new DefaultPluginParseHelper(element, anyXpath());

        assertThat(parser.optAttribute("attribute"), is("value"));
    }

    private Element anyElement() {
        return mock(Element.class);
    }

    private Element elementWithText(String text) {
        Element element = anyElement();
        when(element.getText()).thenReturn(text);
        return element;
    }

    private Element elementNotFinding(String subElement) {
        Element element = anyElement();
        when(element.selectSingleNode(anyXpath() + "/" + subElement)).thenReturn(null);
        return element;
    }

    private Element elementWithSubElementOfUnexpectedClass(String subElement) {
        Element element = anyElement();
        when(element.selectSingleNode(anyXpath() + "/" + subElement)).thenReturn(mock(Node.class));
        return element;
    }

    private Element elementWithSubElement(String subElementName, Element subElement) {
        Element element = anyElement();
        when(element.selectSingleNode(anyXpath() + "/" + subElementName)).thenReturn(subElement);
        return element;
    }


    private Element elementWithoutAttribute(String attribute) {
        Element element = anyElement();
        when(element.attribute(attribute)).thenReturn(null);
        return element;
    }

    private Element elementWithAttribute(String attribute, String value) {
        Attribute attributeObj = attribute(value);
        Element element = anyElement();
        when(element.attribute(attribute)).thenReturn(attributeObj);
        return element;
    }

    private Attribute attribute(String value) {
        Attribute attribute = mock(Attribute.class);
        when(attribute.getText()).thenReturn(value);
        return attribute;
    }

    private String anyXpath() {
        return "any-xpath";
    }
}
