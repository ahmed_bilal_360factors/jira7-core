package com.atlassian.jira;

import com.atlassian.beehive.ClusterLockService;
import com.atlassian.beehive.db.spi.ClusterLockDao;
import com.atlassian.beehive.simple.SimpleClusterLockService;
import com.atlassian.jira.cluster.ClusterNodeProperties;
import com.atlassian.jira.cluster.DefaultClusterManager;
import com.atlassian.jira.cluster.lock.JiraClusterLockDao;
import com.atlassian.jira.cluster.lock.JiraClusterNodeHeartBeatDao;
import com.atlassian.jira.cluster.lock.NullJiraClusterNodeHeartBeatDao;
import com.atlassian.jira.cluster.lock.StartableDatabaseClusterLockService;
import com.atlassian.jira.cluster.zdu.DefaultClusterUpgradeStateManager;
import com.atlassian.jira.cluster.zdu.analytics.ClusterUpgradeAnalyticsListener;
import com.atlassian.jira.index.ha.DefaultReplicatedIndexManager;
import com.atlassian.jira.index.ha.NullReplicatedIndexManager;
import com.atlassian.jira.index.ha.ReplicatedIndexManager;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.picocontainer.ComponentAdapter;

import static com.atlassian.jira.ComponentContainer.Scope.INTERNAL;
import static com.atlassian.jira.ComponentContainer.Scope.PROVIDED;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestClusterServicesRegistrar {
    @Mock
    private ComponentContainer mockComponentContainer;
    @Mock
    private ComponentAdapter<?> mockComponentAdapter;

    @Before
    public void setup() {
        // set up a some transitive dependencies
        when(mockComponentContainer.getComponentAdapter(NullJiraClusterNodeHeartBeatDao.class))
                .thenReturn(mockComponentAdapter);
        when(mockComponentContainer.getComponentAdapter(JiraClusterNodeHeartBeatDao.class))
                .thenReturn(mockComponentAdapter);
        when(mockComponentContainer.getComponentAdapter(DefaultClusterManager.class))
                .thenReturn(mockComponentAdapter);
        when(mockComponentContainer.getComponentAdapter(DefaultClusterUpgradeStateManager.class))
                .thenReturn(mockComponentAdapter);
    }

    private void setUpClustering() {
        final ClusterNodeProperties mockClusterNodeProperties = mock(ClusterNodeProperties.class);
        when(mockClusterNodeProperties.getNodeId()).thenReturn("anyNodeId");
        when(mockComponentContainer.getComponentInstance(ClusterNodeProperties.class))
                .thenReturn(mockClusterNodeProperties);
    }

    @Test
    public void shouldRegisterSimpleClusterLockServiceWhenNotClustered() {
        ClusterServicesRegistrar.register(mockComponentContainer);

        verify(mockComponentContainer).implementation(
                PROVIDED, ClusterLockService.class, SimpleClusterLockService.class);
        verify(mockComponentContainer).implementation(INTERNAL,
                NullJiraClusterNodeHeartBeatDao.class, NullJiraClusterNodeHeartBeatDao.class);
        verify(mockComponentContainer).getComponentAdapter(NullJiraClusterNodeHeartBeatDao.class);
    }

    @Test
    public void shouldRegisterDatabaseBackedClusterLockServiceWhenClustered() {
        setUpClustering();
        ClusterServicesRegistrar.register(mockComponentContainer);

        verify(mockComponentContainer).implementation(
                PROVIDED, ClusterLockService.class, StartableDatabaseClusterLockService.class);
        verify(mockComponentContainer).implementation(
                INTERNAL, ClusterLockDao.class, JiraClusterLockDao.class);
        verify(mockComponentContainer).implementation(INTERNAL,
                JiraClusterNodeHeartBeatDao.class, JiraClusterNodeHeartBeatDao.class);
        verify(mockComponentContainer).getComponentAdapter(JiraClusterNodeHeartBeatDao.class);
    }

    @Test
    public void shouldRegisterNullReplicatedIndexManagerWhenNotClustered() {
        ClusterServicesRegistrar.register(mockComponentContainer);

        verify(mockComponentContainer).implementation(
                PROVIDED, ReplicatedIndexManager.class, NullReplicatedIndexManager.class);
    }

    @Test
    public void shouldRegisterDefaultReplicatedIndexManagerWhenClustered() {
        setUpClustering();
        ClusterServicesRegistrar.register(mockComponentContainer);

        verify(mockComponentContainer).implementation(
                PROVIDED, ReplicatedIndexManager.class, DefaultReplicatedIndexManager.class);
    }

    @Test
    public void shouldRegisterAnalyticsListenerWhenClustered() {
        setUpClustering();

        ClusterServicesRegistrar.register(mockComponentContainer);

        verify(mockComponentContainer, times(1)).implementation(INTERNAL, ClusterUpgradeAnalyticsListener.class);
    }

    @Test
    public void shouldNotRegisterAnalyticsListenerWhenNotClustered() {
        ClusterServicesRegistrar.register(mockComponentContainer);

        verify(mockComponentContainer, never()).implementation(INTERNAL, ClusterUpgradeAnalyticsListener.class);
    }
}
