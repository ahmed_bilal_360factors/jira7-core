package com.atlassian.jira.bulkedit.operation;

import com.atlassian.jira.issue.IssueFieldConstants;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.fields.LongIdsValueHolder;
import com.atlassian.jira.issue.fields.OrderableField;
import com.atlassian.jira.junit.rules.InitMockitoMocks;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.instanceOf;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

/**
 * @since v6.4
 */
public class TestBulkEditOperation {
    @Rule
    public TestRule initMockitoMocks = new InitMockitoMocks(this);

    @Mock
    private IssueManager issueManager;

    @Mock
    private PermissionManager permissionManager;

    @Mock
    private ProjectManager projectManager;

    @Mock
    private FieldManager fieldManager;

    @Mock
    private JiraAuthenticationContext authenticationContext;

    @Mock
    private OrderableField field;

    @InjectMocks
    private BulkEditOperation bulkEditOperation;

    @Test
    public void testSubtractValuesForLongIdsValueHolder() {
        List<Long> fieldValuesBefore = Lists.newArrayList(10001L, 10002L);
        List<Long> fieldValuesAfter = Lists.newArrayList(10001L, 10002L, 10004L);

        LongIdsValueHolder valueHolderAfter = new LongIdsValueHolder(Lists.newArrayList(fieldValuesAfter));
        LongIdsValueHolder valueHolderBefore = new LongIdsValueHolder(Lists.newArrayList(fieldValuesBefore));

        Collection<Object> result = bulkEditOperation.subtractValues(valueHolderAfter, valueHolderBefore);

        assertThat(result, hasSize(1));
        assertThat(result, contains(10004L));
        assertThat(result, instanceOf(List.class));
    }

    @Test
    public void testSubtractValuesForSets() {
        Set<String> fieldValuesBefore = Sets.newHashSet("Label1", "Label2");
        Set<String> fieldValuesAfter = Sets.newHashSet("Label1", "Label2", "Label3", "Label4");

        Collection<Object> result = bulkEditOperation.subtractValues(fieldValuesAfter, fieldValuesBefore);

        assertThat(result, instanceOf(Set.class));
        assertThat(result, hasSize(2));
        assertThat(result, hasItems("Label3", "Label4"));
    }

    @Test
    public void testUpdateFieldValuesHolderComponents() {
        List<Long> fieldValuesOld = Lists.newArrayList(10001L, 10002L);
        List<Long> fieldValuesNew = Lists.newArrayList(10003L, 10004L);

        LongIdsValueHolder valueHolderOld = new LongIdsValueHolder(Lists.newArrayList(fieldValuesOld));
        valueHolderOld.setValuesToAdd(Sets.newHashSet("NewValue"));
        LongIdsValueHolder valueHolderNew = new LongIdsValueHolder(Lists.newArrayList(fieldValuesNew));

        Map<String, Object> fieldValuesHolder = Maps.newHashMap();
        fieldValuesHolder.put(IssueFieldConstants.COMPONENTS, valueHolderOld);

        when(field.getId()).thenReturn(IssueFieldConstants.COMPONENTS);

        bulkEditOperation.updateFieldValuesHolder(field, valueHolderNew, fieldValuesHolder, true);

        assertThat(fieldValuesHolder.get(IssueFieldConstants.COMPONENTS), instanceOf(LongIdsValueHolder.class));
        final LongIdsValueHolder updatedHolder = (LongIdsValueHolder) fieldValuesHolder.get(IssueFieldConstants.COMPONENTS);

        assertThat(updatedHolder, hasItems(10001L, 10002L, 10003L, 10004L));
        assertThat(updatedHolder.getValuesToAdd(), hasSize(0));
    }

    @Test
    public void testUpdateFieldValuesHolderVersions() {
        List<Long> fieldValuesOld = Lists.newArrayList(10001L, 10002L);
        List<Long> fieldValuesNew = Lists.newArrayList(10003L, 10004L);

        LongIdsValueHolder valueHolderOld = new LongIdsValueHolder(Lists.newArrayList(fieldValuesOld));
        valueHolderOld.setValuesToAdd(Sets.newHashSet("NewValue"));
        LongIdsValueHolder valueHolderNew = new LongIdsValueHolder(Lists.newArrayList(fieldValuesNew));

        Map<String, LongIdsValueHolder> fieldValuesHolder = Maps.newHashMap();
        fieldValuesHolder.put(IssueFieldConstants.FIX_FOR_VERSIONS, valueHolderOld);

        when(field.getId()).thenReturn(IssueFieldConstants.FIX_FOR_VERSIONS);

        //noinspection unchecked
        bulkEditOperation.updateFieldValuesHolder(field, valueHolderNew, (Map) fieldValuesHolder, false);

        assertThat(fieldValuesHolder.get(IssueFieldConstants.FIX_FOR_VERSIONS), instanceOf(LongIdsValueHolder.class));
        assertThat(fieldValuesHolder.get(IssueFieldConstants.FIX_FOR_VERSIONS), hasSize(4));
        assertThat(fieldValuesHolder.get(IssueFieldConstants.FIX_FOR_VERSIONS), hasItems(10001L, 10002L, 10003L, 10004L));
        assertThat((fieldValuesHolder.get(IssueFieldConstants.FIX_FOR_VERSIONS)).getValuesToAdd(), hasSize(1));
        assertThat((fieldValuesHolder.get(IssueFieldConstants.FIX_FOR_VERSIONS)).getValuesToAdd(), hasItem("NewValue"));
    }

    @Test
    public void testUpdateFieldValuesHolderLabels() {
        Set<String> fieldValuesOld = Sets.newHashSet("Label1", "Label2");
        Set<String> fieldValuesNew = Sets.newHashSet("Label3");

        Map<String, Set<String>> fieldValuesHolder = Maps.newHashMap();
        fieldValuesHolder.put(IssueFieldConstants.LABELS, fieldValuesOld);

        when(field.getId()).thenReturn(IssueFieldConstants.LABELS);

        //noinspection unchecked
        bulkEditOperation.updateFieldValuesHolder(field, fieldValuesNew, (Map) fieldValuesHolder, false);

        assertThat(fieldValuesHolder.get(IssueFieldConstants.LABELS), instanceOf(Set.class));
        assertThat(fieldValuesHolder.get(IssueFieldConstants.LABELS), hasSize(2));
        assertThat(fieldValuesHolder.get(IssueFieldConstants.LABELS), hasItems("Label1", "Label2"));
    }
}
