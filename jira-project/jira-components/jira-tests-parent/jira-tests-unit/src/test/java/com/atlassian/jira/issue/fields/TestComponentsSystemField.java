package com.atlassian.jira.issue.fields;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.bc.project.component.ProjectComponent;
import com.atlassian.jira.bc.project.component.ProjectComponentImpl;
import com.atlassian.jira.bc.project.component.ProjectComponentManager;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.fields.rest.json.beans.JiraBaseUrls;
import com.atlassian.jira.issue.statistics.ComponentStatisticsMapper;
import com.atlassian.jira.issue.views.SearchLinkGenerator;
import com.atlassian.jira.local.testutils.UtilsForTestSetup;
import com.atlassian.jira.mock.component.MockComponentWorker;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.ofbiz.OfBizFactory;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.template.VelocityTemplatingEngine;
import com.google.common.collect.Lists;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Collections;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;

public class TestComponentsSystemField {
    @Before
    public void setUp() throws Exception {
        UtilsForTestSetup.deleteAllEntities();
        final MockComponentWorker componentAccessorWorker = new MockComponentWorker();
        componentAccessorWorker.registerMock(OfBizDelegator.class, OfBizFactory.getOfBizDelegator());
        ComponentAccessor.initialiseWorker(componentAccessorWorker);
    }

    @After
    public void tearDown() throws Exception {
        UtilsForTestSetup.deleteAllEntities();
    }

    @Test
    public void testComponentsSystemFieldCompareIdSets() {
        ComponentsSystemField componentsSystemField = new ComponentsSystemField(
                mock(VelocityTemplatingEngine.class),
                mock(ProjectComponentManager.class),
                mock(ApplicationProperties.class),
                mock(PermissionManager.class),
                mock(JiraAuthenticationContext.class),
                mock(ComponentStatisticsMapper.class),
                null, // can't mock final classes
                mock(ProjectManager.class),
                mock(JiraBaseUrls.class),
                mock(EventPublisher.class),
                mock(SearchLinkGenerator.class)
        );

        ProjectComponent projectComponent1 = new ProjectComponentImpl(1L, "Component", "", "", 1L, 1L);
        ProjectComponent projectComponent1Copy = new ProjectComponentImpl(1L, "Component", "", "", 1L, 1L);
        ProjectComponent projectComponent1CopyNull = new ProjectComponentImpl(1L, "Component", null, "", 1L, 1L);
        ProjectComponent projectComponent2 = new ProjectComponentImpl(2L, "Component", "comp 2", "", 1L, 1L);
        ProjectComponent projectComponent3 = new ProjectComponentImpl(3L, "Component", "comp 3", "", 1L, 1L);

        //check null cases
        assertTrue(componentsSystemField.compareIdSets(null, null));
        assertFalse(componentsSystemField.compareIdSets(null, Lists.newArrayList(projectComponent1)));
        assertFalse(componentsSystemField.compareIdSets(Lists.newArrayList(projectComponent1), null));

        //check null and empty cases
        assertFalse(componentsSystemField.compareIdSets(null, Collections.emptyList()));
        assertFalse(componentsSystemField.compareIdSets(Collections.emptyList(), null));

        //check empty cases
        assertTrue(componentsSystemField.compareIdSets(Collections.emptyList(), Collections.emptyList()));
        assertFalse(componentsSystemField.compareIdSets(Collections.emptyList(), Lists.newArrayList(projectComponent1)));
        assertFalse(componentsSystemField.compareIdSets(Lists.newArrayList(projectComponent1), Collections.emptyList()));

        //compare same single values
        assertTrue(componentsSystemField.compareIdSets(Lists.newArrayList(projectComponent1), Lists.newArrayList(projectComponent1)));
        assertTrue(componentsSystemField.compareIdSets(Lists.newArrayList(projectComponent2), Lists.newArrayList(projectComponent2)));

        //check same single value instances with/without different different attribute values
        assertTrue(componentsSystemField.compareIdSets(Lists.newArrayList(projectComponent1), Lists.newArrayList(projectComponent1Copy)));
        assertTrue(componentsSystemField.compareIdSets(Lists.newArrayList(projectComponent1Copy), Lists.newArrayList(projectComponent1CopyNull)));
        assertTrue(componentsSystemField.compareIdSets(Lists.newArrayList(projectComponent1CopyNull), Lists.newArrayList(projectComponent1)));

        //check different single values
        assertFalse(componentsSystemField.compareIdSets(Lists.newArrayList(projectComponent1), Lists.newArrayList(projectComponent2)));
        assertFalse(componentsSystemField.compareIdSets(Lists.newArrayList(projectComponent2), Lists.newArrayList(projectComponent1)));

        //check multiple values are same
        assertTrue(componentsSystemField.compareIdSets(Lists.newArrayList(projectComponent1, projectComponent2), Lists.newArrayList(projectComponent1, projectComponent2)));
        assertTrue(componentsSystemField.compareIdSets(Lists.newArrayList(projectComponent1, projectComponent2, projectComponent3), Lists.newArrayList(projectComponent1, projectComponent2, projectComponent3)));

        //check multiple values are same even with different ordering
        assertTrue(componentsSystemField.compareIdSets(Lists.newArrayList(projectComponent1, projectComponent2), Lists.newArrayList(projectComponent2, projectComponent1)));
        assertTrue(componentsSystemField.compareIdSets(Lists.newArrayList(projectComponent1, projectComponent3, projectComponent2), Lists.newArrayList(projectComponent2, projectComponent1, projectComponent3)));

        //check multiple values are same even with duplicate elements
        assertTrue(componentsSystemField.compareIdSets(Lists.newArrayList(projectComponent1, projectComponent1Copy), Lists.newArrayList(projectComponent1CopyNull, projectComponent1)));
        assertTrue(componentsSystemField.compareIdSets(Lists.newArrayList(projectComponent1, projectComponent2), Lists.newArrayList(projectComponent2, projectComponent1, projectComponent2)));
        assertTrue(componentsSystemField.compareIdSets(Lists.newArrayList(projectComponent1, projectComponent3, projectComponent1, projectComponent3, projectComponent2), Lists.newArrayList(projectComponent2, projectComponent1, projectComponent3)));

        //check multiple values are different (no common subset)
        assertFalse(componentsSystemField.compareIdSets(Lists.newArrayList(projectComponent1), Lists.newArrayList(projectComponent2, projectComponent3)));

        //check multiple values are different (with common subset)
        assertFalse(componentsSystemField.compareIdSets(Lists.newArrayList(projectComponent1), Lists.newArrayList(projectComponent1, projectComponent2, projectComponent3)));
        assertFalse(componentsSystemField.compareIdSets(Lists.newArrayList(projectComponent1, projectComponent3), Lists.newArrayList(projectComponent1, projectComponent2, projectComponent3)));
        assertFalse(componentsSystemField.compareIdSets(Lists.newArrayList(projectComponent2, projectComponent1), Lists.newArrayList(projectComponent3, projectComponent2)));
    }
}
