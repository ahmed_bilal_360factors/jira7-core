package com.atlassian.jira.security.roles.actor;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.database.MockDbConnectionManager;
import com.atlassian.jira.database.ResultRow;
import com.atlassian.jira.junit.rules.InitMockitoMocks;
import com.atlassian.jira.mock.component.MockComponentWorker;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.security.roles.ProjectRoleActor;
import com.atlassian.jira.user.ApplicationUser;
import com.google.common.collect.ImmutableList;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;

import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.atlassian.jira.security.roles.ProjectRoleActor.GROUP_ROLE_ACTOR_TYPE;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

public class TestGroupRoleActorFactory {
    @Rule
    public InitMockitoMocks mockitoMocks = new InitMockitoMocks(this);

    private MockDbConnectionManager dbConnectionManager = new MockDbConnectionManager();

    @Mock
    private GroupManager groupManager;

    @Mock
    private ApplicationUser user;

    private GroupRoleActorFactory groupRoleActorFactory;

    @Before
    public void setUp() throws NoSuchFieldException, IllegalAccessException {
        ComponentAccessor.initialiseWorker(new MockComponentWorker());
        groupRoleActorFactory = new GroupRoleActorFactory(groupManager, dbConnectionManager);
        when(user.getName()).thenReturn("user");
    }

    @Test
    public void testGetAllRoleActorsForUser() {
        final List<String> groupNames = ImmutableList.of
                ("group01", "group02", "group03", "group04", "group05", "group06", "group07");

        final ResultRow expectedActor1 = new ResultRow(1L, 100L, 200L, GROUP_ROLE_ACTOR_TYPE, "group01");
        final ResultRow expectedActor2 = new ResultRow(2L, 101L, 201L, GROUP_ROLE_ACTOR_TYPE, "group01");
        final ResultRow expectedActor3 = new ResultRow(3L, 102L, 202L, GROUP_ROLE_ACTOR_TYPE, "group07");
        final ResultRow expectedActor4 = new ResultRow(5L, 103L, 203L, GROUP_ROLE_ACTOR_TYPE, "group107");
        final ResultRow expectedActor5 = new ResultRow(4L, 104L, 204L, GROUP_ROLE_ACTOR_TYPE, "group207");
        final List<ResultRow> queryResults = ImmutableList.of(expectedActor1, expectedActor2, expectedActor3, expectedActor4, expectedActor5);

        when(groupManager.getGroupNamesForUser(user)).thenReturn(groupNames);

        dbConnectionManager.setQueryResults
                ("select pra.id, pra.pid, pra.projectroleid, pra.roletype, pra.roletypeparameter\n"
                                + "from projectroleactor pra\n"
                                + "where pra.roletype = 'atlassian-group-role-actor' and pra.pid is not null",
                        queryResults);

        final Set<ProjectRoleActor> actors = groupRoleActorFactory.getAllRoleActorsForUser(user);
        final Set<ResultRow> actorsAsRows = actors.stream().map(toResultRow).collect(Collectors.toSet());

        assertTrue(actorsAsRows.contains(expectedActor1));
        assertTrue(actorsAsRows.contains(expectedActor2));
        assertTrue(actorsAsRows.contains(expectedActor3));
        assertFalse(actorsAsRows.contains(expectedActor4));
        assertFalse(actorsAsRows.contains(expectedActor5));
        dbConnectionManager.assertAllExpectedStatementsWereRun();
    }

    @Test
    public void testGetAllRoleActorsForUserCaseInsensitive() {
        final List<String> groupNames = ImmutableList.of
                ("group01", "GROUP02", "group03", "group04", "group05", "group06", "group07");

        final ResultRow expectedActor1 = new ResultRow(1L, 100L, 200L, GROUP_ROLE_ACTOR_TYPE, "group01");
        final ResultRow expectedActor2 = new ResultRow(2L, 101L, 201L, GROUP_ROLE_ACTOR_TYPE, "group02");
        final ResultRow expectedActor3 = new ResultRow(3L, 102L, 202L, GROUP_ROLE_ACTOR_TYPE, "GROUP07");
        final ResultRow expectedActor3LowerCaseGroup = new ResultRow(3L, 102L, 202L, GROUP_ROLE_ACTOR_TYPE, "group07");
        final ResultRow expectedActor4 = new ResultRow(4L, 103L, 203L, GROUP_ROLE_ACTOR_TYPE, "group107");
        final ResultRow expectedActor5 = new ResultRow(5L, 104L, 204L, GROUP_ROLE_ACTOR_TYPE, "group207");
        final List<ResultRow> queryResults = ImmutableList.of(expectedActor1, expectedActor2, expectedActor3, expectedActor4, expectedActor5);

        when(groupManager.getGroupNamesForUser(user)).thenReturn(groupNames);

        dbConnectionManager.setQueryResults
                ("select pra.id, pra.pid, pra.projectroleid, pra.roletype, pra.roletypeparameter\n"
                                + "from projectroleactor pra\n"
                                + "where pra.roletype = 'atlassian-group-role-actor' and pra.pid is not null",
                        queryResults);

        final Set<ProjectRoleActor> actors = groupRoleActorFactory.getAllRoleActorsForUser(user);
        final Set<ResultRow> actorsAsRows = actors.stream().map(toResultRow).collect(Collectors.toSet());

        assertTrue(actorsAsRows.contains(expectedActor1));
        assertTrue(actorsAsRows.contains(expectedActor2));
        assertTrue(actorsAsRows.contains(expectedActor3LowerCaseGroup));
        assertFalse(actorsAsRows.contains(expectedActor4));
        assertFalse(actorsAsRows.contains(expectedActor5));
        dbConnectionManager.assertAllExpectedStatementsWereRun();
    }

    @Test
    public void testGetAllRoleActorsForUserNoActors() {
        final List<String> groupNames = ImmutableList.of("group01", "group02", "group03");

        when(groupManager.getGroupNamesForUser(user)).thenReturn(groupNames);

        final ResultRow expectedActor4 = new ResultRow(1L, 101L, 202L, GROUP_ROLE_ACTOR_TYPE, "group107", user);
        final ResultRow expectedActor5 = new ResultRow(1L, 101L, 202L, GROUP_ROLE_ACTOR_TYPE, "group207", user);

        final List<ResultRow> queryResults = ImmutableList.of(expectedActor4, expectedActor5);

        dbConnectionManager.setQueryResults
                ("select pra.id, pra.pid, pra.projectroleid, pra.roletype, pra.roletypeparameter\n"
                                + "from projectroleactor pra\n"
                                + "where pra.roletype = 'atlassian-group-role-actor' and pra.pid is not null",
                        queryResults);

        final Set<ProjectRoleActor> actors = groupRoleActorFactory.getAllRoleActorsForUser(user);

        assertTrue(actors.isEmpty());
        dbConnectionManager.assertAllExpectedStatementsWereRun();
    }

    @Test
    public void testGetAllRoleActorsForNullUser() {
        when(groupManager.getGroupNamesForUser((ApplicationUser) null)).thenThrow(new NullPointerException());

        final Set<ProjectRoleActor> actors = groupRoleActorFactory.getAllRoleActorsForUser(null);

        assertTrue(actors.isEmpty());
    }

    private Function<ProjectRoleActor, ResultRow> toResultRow =
            actor -> new ResultRow(actor.getId(), actor.getProjectId(), actor.getProjectRoleId(), actor.getType(), actor.getParameter());
}
