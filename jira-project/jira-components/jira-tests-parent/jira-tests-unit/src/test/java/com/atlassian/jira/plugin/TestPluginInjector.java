package com.atlassian.jira.plugin;

import com.atlassian.plugin.module.ContainerAccessor;
import com.atlassian.plugin.module.ContainerManagedPlugin;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertSame;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @since v4.4.4
 */
public class TestPluginInjector {
    private ContainerManagedPlugin containerManagedPlugin;

    @Before
    public void setUp() {
        containerManagedPlugin = mock(ContainerManagedPlugin.class);
    }

    @Test
    public void testContainerManagedPlugin() {
        Object answer = new Object();

        final ContainerAccessor containerAccessor = mock(ContainerAccessor.class);
        when(containerAccessor.createBean(eq(Object.class))).thenReturn(answer);
        when(containerManagedPlugin.getContainerAccessor()).thenReturn(containerAccessor);

        Object o = PluginInjector.newInstance(Object.class, containerManagedPlugin);

        assertSame(answer, o);

        verify(containerManagedPlugin).getContainerAccessor();
        verify(containerAccessor).createBean(eq(Object.class));
    }
}
