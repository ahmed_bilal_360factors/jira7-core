package com.atlassian.jira.issue.fields;

import com.atlassian.jira.avatar.Avatar;
import com.atlassian.jira.avatar.AvatarService;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.export.FieldExportPart;
import com.atlassian.jira.issue.export.FieldExportParts;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem;
import com.atlassian.jira.issue.fields.rest.FieldJsonRepresentation;
import com.atlassian.jira.issue.fields.rest.json.UserBeanFactory;
import com.atlassian.jira.issue.fields.rest.json.beans.JiraBaseUrls;
import com.atlassian.jira.issue.fields.rest.json.beans.UserJsonBean;
import com.atlassian.jira.issue.search.handlers.CreatorSearchHandlerFactory;
import com.atlassian.jira.issue.statistics.CreatorStatisticsMapper;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.mock.issue.MockIssue;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.template.VelocityTemplatingEngine;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.EmailFormatter;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.NoopI18nHelper;
import com.atlassian.plugin.webresource.WebResourceManager;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.net.URI;
import java.util.List;

import static java.util.stream.Collectors.toList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestCreatorSystemField {
    CreatorSystemField systemFieldUnderTest;
    @Rule
    public RuleChain mockitoMocksInContainer = MockitoMocksInContainer.forTest(this);

    @Mock
    private VelocityTemplatingEngine templatingEngine;
    @Mock
    @AvailableInContainer
    private ApplicationProperties applicationProperties;
    @Mock
    @AvailableInContainer
    private AvatarService avatarService;
    @Mock
    @AvailableInContainer
    private JiraAuthenticationContext authenticationContext;

    private I18nHelper i18nHelper = new NoopI18nHelper();
    @Mock
    private CreatorStatisticsMapper creatorStatisticsMapper;
    @Mock
    private UserManager userManager;
    @Mock
    private WebResourceManager webResourceManager;
    @Mock
    private FeatureManager featureManager;
    @Mock
    private JiraBaseUrls jiraBaseUrls;
    @Mock
    private FieldLayoutItem fieldLayoutItem;
    @Mock
    private ApplicationUser currentUser;
    @Mock
    private MockIssue issue;
    @Mock
    @AvailableInContainer
    private EmailFormatter emailFormatter;
    @Mock
    private UserBeanFactory userBeanFactory;

    private CreatorSearchHandlerFactory creatorSearchHandlerFactory;

    @Before
    public void setUp() throws Exception {
        final UserJsonBean userJsonBean = new UserJsonBean();
        userJsonBean.setSelf("rest/user?username=selector");
        currentUser = new MockApplicationUser("selector");
        when(authenticationContext.getUser()).thenReturn(currentUser);
        when(authenticationContext.getI18nHelper()).thenReturn(i18nHelper);
        when(userManager.getUserByName("selector")).thenReturn(currentUser);
        when(userBeanFactory.createBean(isA(ApplicationUser.class), eq(currentUser))).thenReturn(userJsonBean);
        when(applicationProperties.getEncoding()).thenReturn("UTF-8");
        systemFieldUnderTest = new CreatorSystemField(
                templatingEngine,
                applicationProperties,
                authenticationContext,
                creatorSearchHandlerFactory,
                jiraBaseUrls,
                creatorStatisticsMapper, userBeanFactory);
    }

    @Test
    public void testJson() throws Exception {
        when(issue.getCreator()).thenReturn(currentUser);
        when(jiraBaseUrls.restApi2BaseUrl()).thenReturn("rest/");
        when(avatarService.getAvatarAbsoluteURL(eq(currentUser), eq(currentUser), isA(Avatar.Size.class))).thenReturn(new URI("http://www.example.com/avatar"));
        FieldJsonRepresentation jsonRepresentation = systemFieldUnderTest.getJsonFromIssue(issue, false, fieldLayoutItem);
        UserJsonBean userBean = (UserJsonBean) jsonRepresentation.getStandardData().getData();
        assertEquals("rest/user?username=selector", userBean.getSelf());
    }


    @Test
    public void testGetRepresentationFromIssue() throws Exception {
        when(issue.getCreator()).thenReturn(currentUser);

        FieldExportParts exportRepresentation = systemFieldUnderTest.getRepresentationFromIssue(issue);
        assertThat(exportRepresentation.getParts(), hasSize(1));

        FieldExportPart item = exportRepresentation.getParts().get(0);

        List<String> values = item.getValues().collect(toList());

        assertThat(values, hasSize(1));
        assertThat(item.getItemLabel(), equalTo(systemFieldUnderTest.getName()));
        assertThat(values, contains(currentUser.getUsername()));
    }

    @Test
    public void testGetRepresentationFromIssueWhenNoCreator() throws Exception {
        when(issue.getCreator()).thenReturn(null);

        FieldExportParts exportRepresentation = systemFieldUnderTest.getRepresentationFromIssue(issue);
        assertThat(exportRepresentation.getParts(), hasSize(1));

        FieldExportPart item = exportRepresentation.getParts().get(0);
        List<String> values = item.getValues().collect(toList());

        assertThat(values, hasSize(1));
        assertThat(item.getItemLabel(), equalTo(systemFieldUnderTest.getName()));
        assertThat(values, contains(""));
    }
}
