package com.atlassian.jira.issue.index;

import com.atlassian.jira.issue.history.AssigneeDateRangeBuilder;
import com.atlassian.jira.issue.history.DateRangeBuilder;
import com.atlassian.jira.issue.history.ReporterDateRangeBuilder;
import com.atlassian.jira.issue.history.StatusDateRangeBuilder;
import com.google.common.collect.Sets;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import java.util.Collection;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;


public class TestIndexedFieldManager {
    @Rule
    public final MethodRule initMockito = MockitoJUnit.rule();

    @Mock
    private ChangeHistoryFieldConfigurationManager mockChangeHistoryFieldConfigurationManager;
    @Mock
    private DateRangeBuilder mockBuilder;

    @InjectMocks
    private DefaultIndexedChangeHistoryFieldManager manager;

    @Before
    public void setupMocks() {
        when(mockChangeHistoryFieldConfigurationManager.getAllFieldNames()).thenReturn(Sets.<String>newHashSet("status", "assignee", "reporter"));
        when(mockChangeHistoryFieldConfigurationManager.getDateRangeBuilder("status")).thenReturn(new StatusDateRangeBuilder());
        when(mockChangeHistoryFieldConfigurationManager.getDateRangeBuilder("assignee")).thenReturn(new AssigneeDateRangeBuilder());
        when(mockChangeHistoryFieldConfigurationManager.getDateRangeBuilder("reporter")).thenReturn(new ReporterDateRangeBuilder());
    }

    @Test
    public void testGetSupportedFields() {
        Collection<IndexedChangeHistoryField> indexedFields = manager.getIndexedChangeHistoryFields();
        assertEquals("Should support status, assignee and reporter", 3, indexedFields.size());
    }

    @Test
    public void testDeleteSupportedFields() {
        Collection<IndexedChangeHistoryField> indexedFields = manager.getIndexedChangeHistoryFields();
        manager.deleteIndexedChangeHistoryField(indexedFields.iterator().next());
        indexedFields = manager.getIndexedChangeHistoryFields();
        assertEquals("Should be 2 fields left", 2, indexedFields.size());
    }

    @Test
    public void testAddSupportedFields() {
        IndexedChangeHistoryField indexedField = new IndexedChangeHistoryField("fixVersion", mockBuilder);

        manager.addIndexedChangeHistoryField(indexedField);
        Collection<IndexedChangeHistoryField> indexedFields = manager.getIndexedChangeHistoryFields();
        assertEquals("Should have added fixVersion field", 4, indexedFields.size());
    }

    @Test
    public void testAddDuplicateFields() {
        IndexedChangeHistoryField indexedField = new IndexedChangeHistoryField("assignee", mockBuilder);

        manager.addIndexedChangeHistoryField(indexedField);
        Collection<IndexedChangeHistoryField> indexedFields = manager.getIndexedChangeHistoryFields();
        assertEquals("Should not add  duplicate assignee field", 3, indexedFields.size());
    }

}
