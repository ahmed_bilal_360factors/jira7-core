package com.atlassian.jira.avatar;

import com.atlassian.jira.icon.IconType;
import org.junit.Test;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by dszuksztul on 3/10/14.
 */
public class TestAvatarType {
    @Test
    public void shouldReturnNullTypeForNullName() {
        // when
        final Avatar.Type avatarTypeByName = Avatar.Type.getByName(null);

        // expect
        assertThat(avatarTypeByName, is(nullValue()));
    }

    @Test
    public void shouldReturnNullTypeForNonExistingName() {
        // when
        final Avatar.Type avatarTypeByName = Avatar.Type.getByName("s0m3-n0n-ex1st1ng");

        // expect
        assertThat(avatarTypeByName, is(nullValue()));
    }

    @Test
    public void shouldReturnTypeFromItsName() {
        // when
        final Avatar.Type avatarTypeByName = Avatar.Type.getByName(Avatar.Type.PROJECT.getName());

        // expect
        assertThat(avatarTypeByName, is(Avatar.Type.PROJECT));
    }

    @Test
    public void testGetByIconTypeNull() {
        Avatar.Type result = Avatar.Type.getByIconType(null);
        assertNull(result);
    }

    @Test
    public void testGetByIconTypeSupported() {
        IconType iconType = IconType.PROJECT_ICON_TYPE;
        Avatar.Type result = Avatar.Type.getByIconType(iconType);
        assertThat(result, is(Avatar.Type.PROJECT));
    }

    @Test
    public void testGetByIconTypeNotSupported() {
        IconType iconType = mock(IconType.class);
        when(iconType.getKey()).thenReturn("my secret");
        Avatar.Type result = Avatar.Type.getByIconType(iconType);
        assertThat(result, is(Avatar.Type.OTHER));
    }
}
