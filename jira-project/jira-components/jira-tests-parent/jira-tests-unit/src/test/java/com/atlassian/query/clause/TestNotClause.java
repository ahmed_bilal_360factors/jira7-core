package com.atlassian.query.clause;

import com.atlassian.query.operand.SingleValueOperand;
import com.atlassian.query.operator.Operator;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

/**
 * Unit test for {@link com.atlassian.query.clause.NotClause}.
 *
 * @since v4.0
 */
public class TestNotClause {
    @Rule
    public final MethodRule initMockito = MockitoJUnit.rule();
    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Mock
    private Clause mockClause;
    @Mock
    private ClauseVisitor clauseVisitor;

    @Test
    public void testNullConstructorArguments() throws Exception {
        exception.expect(Exception.class);
        new NotClause(null);
    }

    @Test
    public void testName() throws Exception {
        assertEquals("NOT", new NotClause(mockClause).getName());
    }

    @Test
    public void testToString() throws Exception {
        TerminalClause terminalClause = new TerminalClauseImpl("testField", Operator.EQUALS, new SingleValueOperand("test"));
        NotClause notClause = new NotClause(terminalClause);
        assertEquals("NOT {testField = \"test\"}", notClause.toString());
    }

    @Test
    public void testToStringWithOrPrecedence() throws Exception {
        OrClause orClause = new OrClause(new TerminalClauseImpl("fourthField", Operator.GREATER_THAN, new SingleValueOperand("other")),
                new TerminalClauseImpl("fifthField", Operator.GREATER_THAN, new SingleValueOperand("other")));
        NotClause notClause = new NotClause(orClause);
        assertEquals("NOT ( {fourthField > \"other\"} OR {fifthField > \"other\"} )", notClause.toString());
    }

    @Test
    public void testToStringWithAndPrecedence() throws Exception {
        AndClause andClause = new AndClause(new TerminalClauseImpl("fourthField", Operator.GREATER_THAN, new SingleValueOperand("other")),
                new TerminalClauseImpl("fifthField", Operator.GREATER_THAN, new SingleValueOperand("other")));
        NotClause notClause = new NotClause(andClause);
        assertEquals("NOT ( {fourthField > \"other\"} AND {fifthField > \"other\"} )", notClause.toString());
    }

    @Test
    public void testToStringWithNotPrecedence() throws Exception {
        NotClause subNotClause = new NotClause(new TerminalClauseImpl("fourthField", Operator.GREATER_THAN, new SingleValueOperand("other")));
        NotClause notClause = new NotClause(subNotClause);
        assertEquals("NOT NOT {fourthField > \"other\"}", notClause.toString());
    }

    @Test
    public void testGetClause() throws Exception {
        TerminalClause terminalClause = new TerminalClauseImpl("testField", Operator.EQUALS, new SingleValueOperand("test"));
        NotClause notClause = new NotClause(terminalClause);
        assertEquals(terminalClause, notClause.getSubClause());
        assertEquals(1, notClause.getClauses().size());
        assertEquals(terminalClause, notClause.getClauses().get(0));
    }

    @Test
    public void testVisit() throws Exception {
        final String success = "success!";
        when(clauseVisitor.visit(any(NotClause.class))).thenReturn(success);

        final Object result = new NotClause(mockClause).accept(clauseVisitor);

        assertThat(result, equalTo(success));
    }
}
