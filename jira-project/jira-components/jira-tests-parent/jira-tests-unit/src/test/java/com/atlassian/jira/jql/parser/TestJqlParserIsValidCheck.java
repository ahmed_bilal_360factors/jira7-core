package com.atlassian.jira.jql.parser;

import com.atlassian.jira.jql.util.JqlStringSupportImpl;
import com.atlassian.jira.util.Function;
import com.atlassian.jira.util.collect.CollectionBuilder;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TestJqlParserIsValidCheck {
    @Test
    public void testIsValidFieldNameGood() throws Exception {
        final DefaultJqlQueryParser parser = new DefaultJqlQueryParser();
        final CollectionBuilder<String> goodBuilder = createGoodList().addAll("cf[4784]", "cf [4784]", "cf[000001]", "8543859843095843098540938398493");
        assertGood(goodBuilder.asList(), parser::isValidFieldName);
    }

    @Test
    public void testIsValidFieldNameBad() throws Exception {
        final DefaultJqlQueryParser parser = new DefaultJqlQueryParser();
        assertBad(createBadList().asList(), parser::isValidFieldName);
    }

    @Test
    public void testIsValidFunctionArgumentGood() throws Exception {
        final DefaultJqlQueryParser parser = new DefaultJqlQueryParser();
        final CollectionBuilder<String> goodBuilder = createGoodList().add("8543859843095843098540938398493");
        assertGood(goodBuilder.asList(), parser::isValidFunctionArgument);
    }

    @Test
    public void testIsValidFunctionArgumentBad() throws Exception {
        final DefaultJqlQueryParser parser = new DefaultJqlQueryParser();
        final CollectionBuilder<String> badBuilder = createBadList().addAll("cf[9202]");
        assertBad(badBuilder.asList(), parser::isValidFunctionArgument);
    }

    @Test
    public void testIsValidFunctionNameGood() throws Exception {
        final DefaultJqlQueryParser parser = new DefaultJqlQueryParser();
        final CollectionBuilder<String> goodBuilder = createGoodList().add("8543859843095843098540938398493");
        assertGood(goodBuilder.asList(), parser::isValidFunctionName);
    }

    @Test
    public void testIsValidFunctionNameBad() throws Exception {
        final DefaultJqlQueryParser parser = new DefaultJqlQueryParser();
        final CollectionBuilder<String> badBuilder = createBadList().addAll("cf[27483]");
        assertBad(badBuilder.asList(), parser::isValidFunctionName);
    }

    @Test
    public void testIsValidValueGood() throws Exception {
        final DefaultJqlQueryParser parser = new DefaultJqlQueryParser();
        assertGood(createGoodList().asList(), parser::isValidValue);
    }

    @Test
    public void testIsValidValueBad() throws Exception {
        final DefaultJqlQueryParser parser = new DefaultJqlQueryParser();
        final CollectionBuilder<String> badBuilder = createBadList().addAll("cf[1]", "8543859843095843098540938398493");
        assertBad(badBuilder.asList(), parser::isValidValue);
    }

    private static CollectionBuilder<String> createGoodList() {
        return CollectionBuilder.newBuilder("nicename", "102748", "niceunicodename\u3737");
    }

    private static CollectionBuilder<String> createBadList() {
        final CollectionBuilder<String> builder = CollectionBuilder.newBuilder("namewith\\bslash", "\\namewithbcontrol",
                "bad name spaces", "badnamewithescapes\n", "badnamewithescapes\\u5775", "'badnamewithsquote",
                "\"badnamewithquote", "badescape\\k", "badquote\"name", "badsinglequote\'name", "", " ", "cf[ 38", "cf[aaa]",
                "cf[-1232]");

        for (int i = 0; i < JqlQueryParserConstants.ILLEGAL_CHARS_STRING.length(); i++) {
            builder.add(String.format("String%cwithillegalchar", JqlQueryParserConstants.ILLEGAL_CHARS_STRING.charAt(i)));
        }

        builder.addAll(JqlStringSupportImpl.RESERVED_WORDS);

        return builder;
    }

    private static void assertBad(final List<String> values, final Function<String, Boolean> function) {
        for (final String value : values) {
            assertFalse(String.format("Expected string '%s' to be invalid.", value), function.get(value));
        }
    }

    private static void assertGood(final List<String> values, final Function<String, Boolean> function) {
        for (final String value : values) {
            assertTrue(String.format("Expected string '%s' to be valid.", value), function.get(value));
        }
    }
}
