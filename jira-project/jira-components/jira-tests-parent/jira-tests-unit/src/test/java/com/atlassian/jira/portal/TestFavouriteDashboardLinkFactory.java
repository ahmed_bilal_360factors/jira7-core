package com.atlassian.jira.portal;

import com.atlassian.jira.bc.portal.PortalPageService;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.UserHistoryItem;
import com.atlassian.jira.user.UserHistoryManager;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.collect.CollectionBuilder;
import com.atlassian.jira.util.velocity.VelocityRequestContext;
import com.atlassian.jira.util.velocity.VelocityRequestContextFactory;
import com.atlassian.plugin.web.api.WebItem;
import com.atlassian.plugin.web.api.model.WebFragmentBuilder;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.List;
import java.util.Map;

import static com.atlassian.jira.plugin.webfragment.EqWebItem.eqWebItem;
import static org.junit.Assert.assertThat;
import static org.junit.matchers.JUnitMatchers.hasItems;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestFavouriteDashboardLinkFactory {
    @Rule
    public MockitoRule mockito = MockitoJUnit.rule();
    @Mock
    private VelocityRequestContext requestContext;
    @Mock
    private VelocityRequestContextFactory requestContextFactory;
    @Mock
    private PortalPageService portalPageService;
    @Mock
    private UserHistoryManager userHistoryManager;
    @Mock
    private I18nHelper.BeanFactory i18nFactory;
    @Mock
    private I18nHelper i18n;
    private ApplicationUser user;

    @InjectMocks
    private FavouriteDashboardLinkFactory linkFactory;
    private Map<String, Object> context;


    @Before
    public void setUp() throws Exception {

        user = new MockApplicationUser("admin");
        context = Maps.newHashMap();
        when(requestContextFactory.getJiraVelocityRequestContext()).thenReturn(requestContext);

    }

    @Test
    public void testNullUserNullDashboards() {
        final WebItem link = new WebFragmentBuilder(20).
                id("dash_lnk_system").
                label("View System Dashboard").
                title("View System Dashboard Title").
                webItem("home_link/dashboard_link_main").
                url("/secure/Dashboard.jspa").build();


        when(portalPageService.getFavouritePortalPages(null)).thenReturn(null);
        when(requestContext.getBaseUrl()).thenReturn("");
        when(i18nFactory.getInstance((ApplicationUser) null)).thenReturn(i18n);
        when(i18n.getText("menu.dashboard.view.system")).thenReturn("View System Dashboard");
        when(i18n.getText("menu.dashboard.view.system.title")).thenReturn("View System Dashboard Title");

        List<WebItem> returnList = Lists.newArrayList(linkFactory.getItems(context));

        assertThat(returnList, hasItems(eqWebItem(link)));
    }

    @Test
    public void testNullUserEmptyDashboards() {
        final WebItem link = new WebFragmentBuilder(20).
                id("dash_lnk_system").
                label("View System Dashboard").
                title("View System Dashboard Title").
                webItem("home_link/dashboard_link_main").
                url("/secure/Dashboard.jspa").build();

        when(portalPageService.getFavouritePortalPages(null)).thenReturn(ImmutableList.of());
        when(requestContext.getBaseUrl()).thenReturn("");
        when(i18nFactory.getInstance((ApplicationUser) null)).thenReturn(i18n);
        when(i18n.getText("menu.dashboard.view.system")).thenReturn("View System Dashboard");
        when(i18n.getText("menu.dashboard.view.system.title")).thenReturn("View System Dashboard Title");

        List<WebItem> returnList = Lists.newArrayList(linkFactory.getItems(context));

        assertThat(returnList, hasItems(eqWebItem(link)));
    }

    @Test
    public void testNullDashboards() {
        final WebItem link = new WebFragmentBuilder(20).
                id("dash_lnk_system").
                label("View System Dashboard").
                title("View System Dashboard Title").
                webItem("home_link/dashboard_link_main").
                url("/jira/secure/Dashboard.jspa").build();

        context.put("user", user);

        when(portalPageService.getFavouritePortalPages(user)).thenReturn(null);
        when(requestContext.getBaseUrl()).thenReturn("/jira");
        when(i18nFactory.getInstance(user)).thenReturn(i18n);
        when(i18n.getText("menu.dashboard.view.system")).thenReturn("View System Dashboard");
        when(i18n.getText("menu.dashboard.view.system.title")).thenReturn("View System Dashboard Title");

        List<WebItem> returnList = Lists.newArrayList(linkFactory.getItems(context));

        assertThat(returnList, hasItems(eqWebItem(link)));
    }

    @Test
    public void testEmptyDashboards() {
        final WebItem link = new WebFragmentBuilder(20).
                id("dash_lnk_system").
                label("View System Dashboard").
                title("View System Dashboard Title").
                webItem("home_link/dashboard_link_main").
                url("/jira/secure/Dashboard.jspa").build();

        context.put("user", user);
        when(portalPageService.getFavouritePortalPages(user)).thenReturn(ImmutableList.of());
        when(requestContext.getBaseUrl()).thenReturn("/jira");
        when(i18nFactory.getInstance(user)).thenReturn(i18n);
        when(i18n.getText("menu.dashboard.view.system")).thenReturn("View System Dashboard");
        when(i18n.getText("menu.dashboard.view.system.title")).thenReturn("View System Dashboard Title");

        List<WebItem> returnList = Lists.newArrayList(linkFactory.getItems(context));

        assertThat(returnList, hasItems(eqWebItem(link)));
    }

    @Test
    public void testNoSessionNullUser() {
        final WebItem link = new WebFragmentBuilder(20).
                id("dash_lnk_1").
                label("Portal Page 1").
                title("Portal Page 1 - Portal Description 1").
                webItem("home_link/dashboard_link_main").
                url("/secure/Dashboard.jspa?selectPageId=1").build();

        PortalPage page = PortalPage.id(1L).name("Portal Page 1").description("Portal Description 1").owner(new MockApplicationUser("admin")).favouriteCount(0L).version(0L).build();
        when(portalPageService.getFavouritePortalPages((ApplicationUser) null)).thenReturn(CollectionBuilder.newBuilder(page).asList());
        when(requestContext.getBaseUrl()).thenReturn("");
        when(i18nFactory.getInstance((ApplicationUser) null)).thenReturn(i18n);
        when(userHistoryManager.getHistory(UserHistoryItem.DASHBOARD, (ApplicationUser) null)).thenReturn(ImmutableList.of());
        when(i18n.getText("menu.dashboard.title", "Portal Page 1", "Portal Description 1")).thenReturn("Portal Page 1 - Portal Description 1");

        List<WebItem> returnList = Lists.newArrayList(linkFactory.getItems(context));
        assertThat(returnList, hasItems(eqWebItem(link)));
    }

    @Test
    public void testDiffInSession() {
        final WebItem link = new WebFragmentBuilder(20).
                id("dash_lnk_1").
                label("Portal Page 1").
                title("Portal Page 1 - Portal Description 1").
                webItem("home_link/dashboard_link_main").
                url("/jira/secure/Dashboard.jspa?selectPageId=1").build();

        context.put("user", user);
        PortalPage page = PortalPage.id(1L).name("Portal Page 1").description("Portal Description 1").owner(new MockApplicationUser("admin")).favouriteCount(0L).version(0L).build();
        when(portalPageService.getFavouritePortalPages(user)).thenReturn(CollectionBuilder.newBuilder(page).asList());
        when(requestContext.getBaseUrl()).thenReturn("/jira");
        when(i18nFactory.getInstance(user)).thenReturn(i18n);
        when(userHistoryManager.getHistory(UserHistoryItem.DASHBOARD, user)).thenReturn(CollectionBuilder.list(new UserHistoryItem(UserHistoryItem.DASHBOARD, "2")));
        when(i18n.getText("menu.dashboard.title", "Portal Page 1", "Portal Description 1")).thenReturn("Portal Page 1 - Portal Description 1");

        List<WebItem> returnList = Lists.newArrayList(linkFactory.getItems(context));

        assertThat(returnList, hasItems(eqWebItem(link)));
    }


    @Test
    public void testSameInSession() {
        final WebItem link = new WebFragmentBuilder(20).
                id("dash_lnk_1").
                label("Portal Page 1").
                title("Portal Page 1 - Portal Description 1").
                webItem("home_link/dashboard_link_main").
                url("/jira/secure/Dashboard.jspa?selectPageId=1").build();

        context.put("user", user);
        PortalPage page = PortalPage.id(1L).name("Portal Page 1").description("Portal Description 1").owner(new MockApplicationUser("admin")).favouriteCount(0L).version(0L).build();
        when(portalPageService.getFavouritePortalPages(user)).thenReturn(CollectionBuilder.newBuilder(page).asList());
        when(requestContext.getBaseUrl()).thenReturn("/jira");
        when(i18nFactory.getInstance(user)).thenReturn(i18n);
        when(userHistoryManager.getHistory(UserHistoryItem.DASHBOARD, user)).thenReturn(CollectionBuilder.list(new UserHistoryItem(UserHistoryItem.DASHBOARD, "1")));
        when(i18n.getText("menu.dashboard.title", "Portal Page 1", "Portal Description 1")).thenReturn("Portal Page 1 - Portal Description 1");

        List<WebItem> returnList = Lists.newArrayList(linkFactory.getItems(context));

        assertThat(returnList, hasItems(eqWebItem(link)));
    }

    @Test
    public void testSameInSessionWithMulti() {
        final WebItem link = new WebFragmentBuilder(20).
                id("dash_lnk_1").
                styleClass("bolded").
                label("Portal Page 1").
                title("Portal Page 1 - Portal Description 1").
                webItem("home_link/dashboard_link_main").
                url("/jira/secure/Dashboard.jspa?selectPageId=1").build();
        final WebItem link2 = new WebFragmentBuilder(30).
                id("dash_lnk_2").
                label("Portal Page 2").
                title("Portal Page 2").
                webItem("home_link/dashboard_link_main").
                url("/jira/secure/Dashboard.jspa?selectPageId=2").build();

        context.put("user", user);
        PortalPage page = PortalPage.id(1L).name("Portal Page 1").description("Portal Description 1").owner(new MockApplicationUser("admin")).favouriteCount(0L).version(0L).build();
        PortalPage page2 = PortalPage.id(2L).name("Portal Page 2").owner(new MockApplicationUser("admin")).favouriteCount(0L).version(0L).build();
        when(portalPageService.getFavouritePortalPages(user)).thenReturn(CollectionBuilder.newBuilder(page, page2).asList());
        when(requestContext.getBaseUrl()).thenReturn("/jira");
        when(i18nFactory.getInstance(user)).thenReturn(i18n);
        when(userHistoryManager.getHistory(UserHistoryItem.DASHBOARD, user)).thenReturn(CollectionBuilder.list(new UserHistoryItem(UserHistoryItem.DASHBOARD, "1"), new UserHistoryItem(UserHistoryItem.DASHBOARD, "2")));
        when(i18n.getText("menu.dashboard.title", "Portal Page 1", "Portal Description 1")).thenReturn("Portal Page 1 - Portal Description 1");

        List<WebItem> returnList = Lists.newArrayList(linkFactory.getItems(context));

        assertThat(returnList, hasItems(eqWebItem(link), eqWebItem(link2)));
    }


    @Test
    public void testLongLabel() {
        final WebItem link = new WebFragmentBuilder(20).
                id("dash_lnk_1").
                label("123456789012345678901234567890").
                title("123456789012345678901234567890 - Portal Description 1").
                webItem("home_link/dashboard_link_main").
                url("/jira/secure/Dashboard.jspa?selectPageId=1").build();

        context.put("user", user);
        PortalPage page = PortalPage.id(1L).name("123456789012345678901234567890").description("Portal Description 1").owner(new MockApplicationUser("admin")).favouriteCount(0L).version(0L).build();
        when(portalPageService.getFavouritePortalPages(user)).thenReturn(CollectionBuilder.newBuilder(page).asList());
        when(requestContext.getBaseUrl()).thenReturn("/jira");
        when(i18nFactory.getInstance(user)).thenReturn(i18n);
        when(userHistoryManager.getHistory(UserHistoryItem.DASHBOARD, user)).thenReturn(CollectionBuilder.list(new UserHistoryItem(UserHistoryItem.DASHBOARD, "1")));
        when(i18n.getText("menu.dashboard.title", "123456789012345678901234567890", "Portal Description 1")).thenReturn("123456789012345678901234567890 - Portal Description 1");

        List<WebItem> returnList = Lists.newArrayList(linkFactory.getItems(context));

        assertThat(returnList, hasItems(eqWebItem(link)));
    }

    @Test
    public void testTooLongLabel() {
        final WebItem link = new WebFragmentBuilder(20).
                id("dash_lnk_1").
                label("123456789012345678901234567890...").
                title("12345678901234567890123456789012345678901234567890 - Portal Description 1").
                webItem("home_link/dashboard_link_main").
                url("/jira/secure/Dashboard.jspa?selectPageId=1").build();

        context.put("user", user);
        PortalPage page = PortalPage.id(1L).name("12345678901234567890123456789012345678901234567890").description("Portal Description 1").owner(new MockApplicationUser("admin")).favouriteCount(0L).version(0L).build();
        when(portalPageService.getFavouritePortalPages(user)).thenReturn(CollectionBuilder.newBuilder(page).asList());
        when(requestContext.getBaseUrl()).thenReturn("/jira");
        when(i18nFactory.getInstance(user)).thenReturn(i18n);
        when(userHistoryManager.getHistory(UserHistoryItem.DASHBOARD, user)).thenReturn(CollectionBuilder.list(new UserHistoryItem(UserHistoryItem.DASHBOARD, "1")));
        when(i18n.getText("menu.dashboard.title", "12345678901234567890123456789012345678901234567890", "Portal Description 1")).thenReturn("12345678901234567890123456789012345678901234567890 - Portal Description 1");

        List<WebItem> returnList = Lists.newArrayList(linkFactory.getItems(context));

        assertThat(returnList, hasItems(eqWebItem(link)));
    }


}
