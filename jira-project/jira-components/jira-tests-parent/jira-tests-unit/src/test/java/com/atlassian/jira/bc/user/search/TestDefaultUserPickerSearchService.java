package com.atlassian.jira.bc.user.search;

import com.atlassian.crowd.embedded.api.ApplicationFactory;
import com.atlassian.crowd.embedded.api.CrowdService;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.Query;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.crowd.manager.directory.DirectoryManager;
import com.atlassian.crowd.model.application.Application;
import com.atlassian.crowd.model.application.DirectoryMapping;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.bc.JiraServiceContextImpl;
import com.atlassian.jira.bc.MockJiraServiceContext;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.crowd.embedded.ofbiz.MockDirectory;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.mock.MockApplicationProperties;
import com.atlassian.jira.mock.MockPermissionManager;
import com.atlassian.jira.mock.MockProjectManager;
import com.atlassian.jira.mock.component.MockComponentWorker;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.groups.MockGroupManager;
import com.atlassian.jira.security.roles.ProjectRole;
import com.atlassian.jira.security.roles.ProjectRoleActors;
import com.atlassian.jira.security.roles.ProjectRoleManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.MockGroup;
import com.atlassian.jira.user.UserFilter;
import com.atlassian.jira.user.util.MockUserManager;
import com.atlassian.jira.user.util.UserKeyStore;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.event.PluginEventManager;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;
import org.hamcrest.Matchers;
import org.hamcrest.core.IsCollectionContaining;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import static com.google.common.collect.Lists.newArrayList;
import static java.util.Collections.singletonList;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


/**
 * Tests DefaultUserPickerSearchService without being a JiraMockTestCase.
 *
 * @see UserSearchService
 */
public class TestDefaultUserPickerSearchService {
    private JiraServiceContext jiraCtx;
    private MockApplicationProperties applicationProperties = new MockApplicationProperties();
    private MockPermissionManager permissionManager;

    @AvailableInContainer
    @Mock
    private PluginEventManager pluginEventManager;
    @AvailableInContainer
    @Mock
    private PluginAccessor pluginAccessor;
    @Mock
    private JiraAuthenticationContext authenticationContext;
    @Mock
    private CrowdService crowdService;
    @Mock
    private FeatureManager featureManager;

    @Rule
    public RuleChain ruleChain = MockitoMocksInContainer.forTest(this);

    @Before
    public void setUp() throws Exception {
        permissionManager = new MockPermissionManager(true);
        jiraCtx = new MockJiraServiceContext();
        applicationProperties.setString(APKeys.JIRA_OPTION_EMAIL_VISIBLE, "show");
        when(authenticationContext.getLocale()).thenReturn(Locale.getDefault());

        ComponentAccessor.initialiseWorker(new MockComponentWorker().addMock(UserManager.class, new MockUserManager().alwaysReturnUsers()));
    }

    @Test
    public void testNullQuery() {
        DefaultUserPickerSearchService userPickerSearchService = new DefaultUserPickerSearchService(null, null, authenticationContext, null, null, null, null, null, null, null, null);

        final List<ApplicationUser> results = userPickerSearchService.findUsers(jiraCtx, null);
        assertNotNull(results);
        assertEquals(0, results.size());
    }

    @Test
    public void testEmptyQuery() {
        DefaultUserPickerSearchService userPickerSearchService = new DefaultUserPickerSearchService(null, null, authenticationContext, null, null, null, null, null, null, null, null);

        final List<ApplicationUser> results = userPickerSearchService.findUsers(jiraCtx, "");
        assertNotNull(results);
        assertEquals(0, results.size());
    }

    @Test
    public void testWhitespaceOnlyQuery() {
        DefaultUserPickerSearchService userPickerSearchService = new DefaultUserPickerSearchService(null, null, authenticationContext, null, null, null, null, null, null, null, null);

        final List<ApplicationUser> results = userPickerSearchService.findUsers(jiraCtx, "\t ");
        assertNotNull(results);
        assertEquals(0, results.size());
    }

    @Test
    public void testUserSearchParamsGroupFilters() {
        MockUserManager userManager = new MockUserManager();
        final MockApplicationUser userA = new MockApplicationUser("a", "Adam Smith", "mada@smith.com");
        final MockApplicationUser userB = new MockApplicationUser("b", "Bea Smith", "aeb@smith.com");
        final MockApplicationUser userC = new MockApplicationUser("c", "Bea Hive", "aeb@hive.com").setActive(false);
        final MockApplicationUser userD = new MockApplicationUser("d", "Adam Hive", "mada@hive.com");
        userManager.addUser(userA);
        userManager.addUser(userB);
        userManager.addUser(userC);
        userManager.addUser(userD);

        MockGroupManager groupManager = new MockGroupManager();
        final MockGroup group1 = new MockGroup("group1");
        final MockGroup group2 = new MockGroup("group2");
        groupManager.addUserToGroup(userB, group1);
        groupManager.addUserToGroup(userC, group1);
        groupManager.addUserToGroup(userD, group1);
        groupManager.addUserToGroup(userA, group2);

        DefaultUserPickerSearchService searchService = new DefaultUserPickerSearchService(userManager, applicationProperties, authenticationContext, permissionManager, groupManager, null, null, null, null, null, null);

        Collection<ApplicationUser> results;
        // Default is active only, no empty search allowed
        results = searchService.findUsers(jiraCtx, "bea", UserSearchParams.builder().includeActive(true).includeInactive(false)
                .filter(new UserFilter(true, null, ImmutableSet.<String>of())).build());
        assertNotNull(results);
        assertEquals(0, results.size());

        // Default is active only, no empty search allowed
        results = searchService.findUsers(jiraCtx, "bea", UserSearchParams.builder().includeActive(true).includeInactive(false)
                .filter(new UserFilter(true, null, ImmutableSet.of("group1"))).build());
        assertNotNull(results);
        assertEquals(1, results.size());
        assertTrue(results.contains(new MockApplicationUser("b")));

        // include Inactive
        results = searchService.findUsers(jiraCtx, "Bea", UserSearchParams.builder().includeActive(true).includeInactive(true)
                .filter(new UserFilter(true, null, ImmutableSet.of("group1"))).build());
        assertNotNull(results);
        assertEquals(2, results.size());
        assertTrue(results.contains(new MockApplicationUser("b")));
        assertTrue(results.contains(new MockApplicationUser("c")));

        // Inactive only
        results = searchService.findUsers(jiraCtx, "Bea", UserSearchParams.builder().includeActive(false).includeInactive(true)
                .filter(new UserFilter(true, null, ImmutableSet.of("group1"))).build());
        assertNotNull(results);
        assertEquals(1, results.size());
        assertTrue(results.contains(new MockApplicationUser("c")));

        // include no-one
        results = searchService.findUsers(jiraCtx, "Bea", UserSearchParams.builder().includeActive(false).includeInactive(false)
                .filter(new UserFilter(true, null, ImmutableSet.of("group1"))).build());
        assertNotNull(results);
        assertEquals(0, results.size());
    }

    @Test
    public void testUserSearchParamsWithProjectRoleFilters() throws Exception {
        MockUserManager userManager = new MockUserManager();
        final MockApplicationUser userA = new MockApplicationUser("a", "Adam Smith", "mada@smith.com");
        final MockApplicationUser userB = new MockApplicationUser("b", "Bea Smith", "aeb@smith.com");
        final MockApplicationUser userC = new MockApplicationUser("c", "Bea Hive", "aeb@hive.com").setActive(false);
        final MockApplicationUser userD = new MockApplicationUser("d", "Adam Hive", "mada@hive.com");
        userManager.addUser(userA);
        userManager.addUser(userB);
        userManager.addUser(userC);
        userManager.addUser(userD);

        final MockProjectManager projectManager = new MockProjectManager();
        final MockProject project1 = new MockProject(1L);
        final MockProject project2 = new MockProject(2L);
        projectManager.addProject(project1);
        projectManager.addProject(project2);

        ProjectRoleManager projectRoleManager = Mockito.mock(ProjectRoleManager.class);
        ProjectRole projectRole1 = Mockito.mock(ProjectRole.class);
        ProjectRole projectRole2 = Mockito.mock(ProjectRole.class);
        when(projectRoleManager.getProjectRole(1001L)).thenReturn(projectRole1);
        when(projectRoleManager.getProjectRole(1002L)).thenReturn(projectRole2);
        ProjectRoleActors projectRoleActors11 = Mockito.mock(ProjectRoleActors.class);
        when(projectRoleManager.getProjectRoleActors(projectRole1, project1)).thenReturn(projectRoleActors11);
        when(projectRoleActors11.getUsers()).thenReturn(ImmutableSet.<ApplicationUser>of(userA));
        ProjectRoleActors projectRoleActors21 = Mockito.mock(ProjectRoleActors.class);
        when(projectRoleManager.getProjectRoleActors(projectRole2, project1)).thenReturn(projectRoleActors21);
        when(projectRoleActors21.getUsers()).thenReturn(ImmutableSet.<ApplicationUser>of(userB));
        ProjectRoleActors projectRoleActors12 = Mockito.mock(ProjectRoleActors.class);
        when(projectRoleManager.getProjectRoleActors(projectRole1, project2)).thenReturn(projectRoleActors12);
        when(projectRoleActors12.getUsers()).thenReturn(ImmutableSet.<ApplicationUser>of(userC));
        ProjectRoleActors projectRoleActors22 = Mockito.mock(ProjectRoleActors.class);
        when(projectRoleManager.getProjectRoleActors(projectRole2, project2)).thenReturn(projectRoleActors22);
        when(projectRoleActors22.getUsers()).thenReturn(ImmutableSet.<ApplicationUser>of(userD));

        DefaultUserPickerSearchService searchService = new DefaultUserPickerSearchService(userManager, applicationProperties, authenticationContext, permissionManager, null, projectManager, projectRoleManager, null, null, null, null);

        Collection<ApplicationUser> results;
        // Default is active only, no empty search allowed
        results = searchService.findUsers(jiraCtx, "bea", UserSearchParams.builder().includeActive(true).includeInactive(false)
                .filter(new UserFilter(true, Collections.emptySet(), null)).build());
        assertNotNull(results);
        assertEquals(0, results.size());

        // Default is active only, no empty search allowed
        results = searchService.findUsers(jiraCtx, "Ada", UserSearchParams.builder().includeActive(true).includeInactive(false)
                .filter(new UserFilter(true, ImmutableSet.of(1001L), null)).filterByProjectIds(ImmutableSet.of(1L)).build());
        assertNotNull(results);
        assertEquals(1, results.size());
        assertThat(results, IsCollectionContaining.<ApplicationUser>hasItem(new MockApplicationUser("a")));

        // include Inactive
        results = searchService.findUsers(jiraCtx, "Hive", UserSearchParams.builder().includeActive(true).includeInactive(true)
                .filter(new UserFilter(true, ImmutableSet.of(1001L, 1002L), null)).filterByProjectIds(ImmutableSet.of(2L)).build());
        assertNotNull(results);
        assertEquals(2, results.size());
        assertTrue(results.contains(new MockApplicationUser("c")));
        assertTrue(results.contains(new MockApplicationUser("d")));

        // Inactive only
        results = searchService.findUsers(jiraCtx, "Hive", UserSearchParams.builder().includeActive(false).includeInactive(true)
                .filter(new UserFilter(true, ImmutableSet.of(1001L, 1002L), null)).filterByProjectIds(ImmutableSet.of(2L)).build());
        assertNotNull(results);
        assertEquals(1, results.size());
        assertTrue(results.contains(new MockApplicationUser("c")));

        // include no-one
        results = searchService.findUsers(jiraCtx, "Hive", UserSearchParams.builder().includeActive(true).includeInactive(true)
                .filter(new UserFilter(true, ImmutableSet.of(1001L, 1002L), null)).filterByProjectIds(ImmutableSet.of(1L)).build());
        assertNotNull(results);
        assertEquals(0, results.size());
    }

    @Test
    public void testCanPerformAjaxSearch() throws Exception {
        ApplicationUser user = new MockApplicationUser("sarah");
        DefaultUserPickerSearchService searchService = new DefaultUserPickerSearchService(null, applicationProperties, authenticationContext, permissionManager, null, null, null, null, null, null, null);

        permissionManager.setDefaultPermission(false);
        assertFalse(searchService.canPerformAjaxSearch(user));
        assertFalse(searchService.canPerformAjaxSearch(new JiraServiceContextImpl(user)));
        permissionManager.setDefaultPermission(true);
        assertTrue(searchService.canPerformAjaxSearch(user));
        assertTrue(searchService.canPerformAjaxSearch(new JiraServiceContextImpl(user)));
    }

    @Test
    public void testSearchReturnsEmptyListWhenUserDoesNotHavePermission() throws Exception {
        MockUserManager userManager = new MockUserManager();
        MockApplicationUser user1 = new MockApplicationUser("zzz", "Adam Smith", "asmith@example.com");
        userManager.addUser(user1);

        DefaultUserPickerSearchService searchService = new DefaultUserPickerSearchService(userManager, applicationProperties, authenticationContext, permissionManager, null, null, null, crowdService, null, null, null);

        // First make email invisible
        when(crowdService.search(any(Query.class))).thenReturn(ImmutableList.of(user1.getDirectoryUser()));
        permissionManager.setDefaultPermission(false);
        Collection<ApplicationUser> results = searchService.findUsers(jiraCtx, "zz");
        assertThat(results, empty());
    }

    @Test
    public void testCanShowEmailAddressesWhenSHOW() {
        final DefaultUserPickerSearchService searchService = new DefaultUserPickerSearchService(null, applicationProperties, authenticationContext, permissionManager, null, null, null, null, null, null, null);
        // show
        applicationProperties.setString(APKeys.JIRA_OPTION_EMAIL_VISIBLE, "show");
        assertTrue(searchService.canShowEmailAddresses(jiraCtx));
        // mask
        applicationProperties.setString(APKeys.JIRA_OPTION_EMAIL_VISIBLE, "mask");
        assertTrue(searchService.canShowEmailAddresses(jiraCtx));
        // hidden
        applicationProperties.setString(APKeys.JIRA_OPTION_EMAIL_VISIBLE, "hidden");
        assertFalse(searchService.canShowEmailAddresses(jiraCtx));
        // user
        applicationProperties.setString(APKeys.JIRA_OPTION_EMAIL_VISIBLE, "user");

        assertFalse(searchService.canShowEmailAddresses(new JiraServiceContextImpl(null)));
        ApplicationUser user = new MockApplicationUser("gimley");
        assertTrue(searchService.canShowEmailAddresses(new JiraServiceContextImpl(user)));
        // junk
        applicationProperties.setString(APKeys.JIRA_OPTION_EMAIL_VISIBLE, "asdfasd");
        assertFalse(searchService.canShowEmailAddresses(jiraCtx));
    }

    @Test
    public void testFindUserKeysByFullName() {
        final CrowdService crowdService = mock(CrowdService.class);
        mockUsersInCrowdService(crowdService,
                new MockApplicationUser("jgalah", "John Galah", "john.galah@example.com"),
                new MockApplicationUser("apiegon", "Alison Pigeon", "ally@example.com"));

        final UserKeyStore userKeyStore = mock(UserKeyStore.class);
        when(userKeyStore.getKeyForUsername("jgalah")).thenReturn("jgalah");
        when(userKeyStore.getKeyForUsername("apigeon")).thenReturn("apigeon");

        DefaultUserPickerSearchService searchService = new DefaultUserPickerSearchService(null, applicationProperties, authenticationContext, permissionManager, null, null, null, crowdService, null, null, userKeyStore);
        Iterable<String> result = searchService.findUserKeysByFullName("John Galah");

        assertThat(result, Matchers.<String>iterableWithSize(1));
        assertThat(result, Matchers.hasItem("jgalah"));
    }

    @Test
    public void testFindUserKeysByEmail() {
        final CrowdService crowdService = mock(CrowdService.class);
        mockUsersInCrowdService(crowdService,
                new MockApplicationUser("jgalah", "John Galah", "john.galah@example.com"),
                new MockApplicationUser("apiegon", "Alison Pigeon", "ally@example.com"));

        final UserKeyStore userKeyStore = mock(UserKeyStore.class);
        when(userKeyStore.getKeyForUsername("jgalah")).thenReturn("jgalah");
        when(userKeyStore.getKeyForUsername("apigeon")).thenReturn("apigeon");

        DefaultUserPickerSearchService searchService = new DefaultUserPickerSearchService(null, applicationProperties, authenticationContext, permissionManager, null, null, null, crowdService, null, null, userKeyStore);
        Iterable<String> result = searchService.findUserKeysByEmail("john.galah@example.com");

        assertThat(result, Matchers.<String>iterableWithSize(1));
        assertThat(result, Matchers.hasItem("jgalah"));
    }

    @Test
    public void testFindUserKeysWhenUserKeyDoesNotExist() {
        final CrowdService crowdService = mock(CrowdService.class);
        mockUsersInCrowdService(crowdService,
                new MockApplicationUser("jgalah", "John Galah", "john.galah@example.com"),
                new MockApplicationUser("apiegon", "Alison Pigeon", "ally@example.com"));

        final UserKeyStore userKeyStore = mock(UserKeyStore.class);
        when(userKeyStore.getKeyForUsername("apigeon")).thenReturn("apigeon");

        DefaultUserPickerSearchService searchService = new DefaultUserPickerSearchService(null, applicationProperties, authenticationContext, permissionManager, null, null, null, crowdService, null, null, userKeyStore);
        Iterable<String> result = searchService.findUserKeysByFullName("John Galah");

        assertThat(result, Matchers.<String>iterableWithSize(0));
    }

    @Test
    public void testFindUsersWithCrowdOffsetAndLimit()
            throws Exception {
        MockUserManager userManager = new MockUserManager();
        final MockApplicationUser userA = new MockApplicationUser("a", "Adam Smith", "");
        final MockApplicationUser userB = new MockApplicationUser("b", "Bea Smith", "");
        userManager.addUser(userA);
        userManager.addUser(userB);
        final MockApplicationUser userC = new MockApplicationUser("c", "Bea Hive", "");
        userManager.addUser(userC);
        final MockApplicationUser userD = new MockApplicationUser("d", "Cynthia Magpie", "");
        userManager.addUser(userD);
        final MockApplicationUser userE = new MockApplicationUser("e", "Donald Duck", "");
        userManager.addUser(userE);

        List<User> userList = ImmutableList.of(userA.getDirectoryUser(),
                userB.getDirectoryUser(),
                userC.getDirectoryUser(),
                userD.getDirectoryUser(),
                userE.getDirectoryUser());

        Directory directory = new MockDirectory(1L);
        Application application = mock(Application.class);
        when(application.getDirectoryMappings()).thenReturn(ImmutableList.of(new DirectoryMapping(application, directory, true)));
        ApplicationFactory applicationFactory = mock(ApplicationFactory.class);
        when(applicationFactory.getApplication()).thenReturn(application);


        DirectoryManager directoryManager = mock(DirectoryManager.class);
        when(directoryManager.searchUsers(anyLong(), any(EntityQuery.class))).thenAnswer(invocation ->
        {
            EntityQuery query = invocation.getArgumentAt(1, EntityQuery.class);
            return userList.subList(query.getStartIndex(), Math.min(query.getStartIndex() + query.getMaxResults(), userList.size()));
        });

        DefaultUserPickerSearchService searchService = new DefaultUserPickerSearchService(userManager, applicationProperties, authenticationContext, permissionManager, null, null, null, crowdService, directoryManager, applicationFactory, null);

        UserSearchParams.Builder searchParamsBuilder = UserSearchParams.builder()
                .maxResults(2)
                .allowEmptyQuery(true)
                .filter(user -> !ImmutableSet.of("b", "c", "d").contains(user.getName()));

        Collection<ApplicationUser> results = searchService.findUsers("", searchParamsBuilder.build());

        assertThat(results, contains(userA, userE));
    }

    /**
     * The database has less results than the limit defines.
     */
    @Test
    public void testFindUsersWithCrowdOffsetAndHighLimit()
            throws Exception {
        MockUserManager userManager = new MockUserManager();
        final MockApplicationUser userA = new MockApplicationUser("a", "Adam Smith", "");
        final MockApplicationUser userB = new MockApplicationUser("b", "Bea Smith", "");
        userManager.addUser(userA);
        userManager.addUser(userB);
        final MockApplicationUser userC = new MockApplicationUser("c", "Bea Hive", "");
        userManager.addUser(userC);
        final MockApplicationUser userD = new MockApplicationUser("d", "Cynthia Magpie", "");
        userManager.addUser(userD);
        final MockApplicationUser userE = new MockApplicationUser("e", "Donald Duck", "");
        userManager.addUser(userE);

        List<User> userList = ImmutableList.of(userA.getDirectoryUser(),
                userB.getDirectoryUser(),
                userC.getDirectoryUser(),
                userD.getDirectoryUser(),
                userE.getDirectoryUser());

        Directory directory = new MockDirectory(1L);
        Application application = mock(Application.class);
        when(application.getDirectoryMappings()).thenReturn(ImmutableList.of(new DirectoryMapping(application, directory, true)));
        ApplicationFactory applicationFactory = mock(ApplicationFactory.class);
        when(applicationFactory.getApplication()).thenReturn(application);


        DirectoryManager directoryManager = mock(DirectoryManager.class);
        when(directoryManager.searchUsers(anyLong(), any(EntityQuery.class))).thenAnswer(invocation ->
        {
            EntityQuery query = invocation.getArgumentAt(1, EntityQuery.class);
            return userList.subList(query.getStartIndex(), Math.min(query.getStartIndex() + query.getMaxResults(), userList.size()));
        });

        DefaultUserPickerSearchService searchService = new DefaultUserPickerSearchService(userManager, applicationProperties, authenticationContext, permissionManager, null, null, null, crowdService, directoryManager, applicationFactory, null);

        UserSearchParams.Builder searchParamsBuilder = UserSearchParams.builder()
                .maxResults(2)
                .allowEmptyQuery(true)
                .filter(user -> !ImmutableSet.of("b", "c", "d", "e").contains(user.getName()));

        Collection<ApplicationUser> results = searchService.findUsers("", searchParamsBuilder.build());

        assertThat(results, contains(userA));
    }

    @Test
    public void testUserSearchPagingExactResultsNoOffset() {
        assertGetPagedResults(0, 10, Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10));
    }

    @Test
    public void testUserSearchPagingMoreResultsNoOffset() {
        assertGetPagedResults(0, 20, Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10));
    }

    @Test
    public void testUserSearchPagingFewerResultsNoOffset() {
        assertGetPagedResults(0, 5, Arrays.asList(1, 2, 3, 4, 5));
    }

    @Test
    public void testUserSearchPagingExactResultsHalfOffset() {
        assertGetPagedResults(5, 5, Arrays.asList(6, 7, 8, 9, 10));
    }

    @Test
    public void testUserSearchPagingMoreResultsHalfOffset() {
        assertGetPagedResults(5, 10, Arrays.asList(6, 7, 8, 9, 10));
    }

    @Test
    public void testUserSearchPagingMoreResultsMaxOffset() {
        assertGetPagedResults(10, 10, Collections.emptyList());
    }

    @Test
    public void testUserMatches() {
        final UserSearchService searchService = new DefaultUserPickerSearchService(null, null, null, null, null, null, null, null, null, null, null);

        final ApplicationUser userA = new MockApplicationUser("a", "Adam Smith", "asmith@example.com");

        boolean result = searchService.userMatches(
                userA,
                UserSearchParams.builder(UserSearchParams.ACTIVE_USERS_IGNORE_EMPTY_QUERY).build()
        );

        assertThat(result, is(true));

        result = searchService.userMatches(
                null,
                UserSearchParams.builder(UserSearchParams.ACTIVE_USERS_IGNORE_EMPTY_QUERY).build()
        );

        assertThat(result, is(false));

        final UserSearchParams adamParams = UserSearchParams.builder(UserSearchParams.ACTIVE_USERS_IGNORE_EMPTY_QUERY)
                .filter(user -> StringUtils.startsWith(user.getDisplayName(), "Adam")).build();

        result = searchService.userMatches(userA, adamParams);

        assertThat(result, is(true));

        result = searchService.userMatches(new MockApplicationUser("b", "Bill Smith", "bsmith@example.com"), adamParams);

        assertThat(result, is(false));
    }

    @Test
    public void testFilterUsers() {
        final UserSearchService searchService = new DefaultUserPickerSearchService(null, null, authenticationContext, null, null, null, null, null, null, null, null);

        final ApplicationUser userA = new MockApplicationUser("a", "Adam Smith", "asmith@example.com");
        final ApplicationUser userB = new MockApplicationUser("b", "Bea Hive", "bea_hive@example.com");
        final ApplicationUser userC = new MockApplicationUser("c", "Bea Smith", "bsmith@example.com");
        final ApplicationUser userD = new MockApplicationUser("d", "Cynthia Magpie", "magpie@sample.com");
        final ApplicationUser userE = new MockApplicationUser("e", "Donald Duck", "d.duck@sample.com");

        List<ApplicationUser> results = searchService.filterUsers(
                ImmutableList.of(userA, userB, userC, userD, userE),
                "Be",
                UserSearchParams.ACTIVE_USERS_IGNORE_EMPTY_QUERY
        );

        assertThat("Received : " + results, results, contains(userB, userC));

        results = searchService.filterUsers(
                ImmutableList.of(userA, userB, userC, userD, userE),
                null,
                "sample",
                UserSearchParams.builder(UserSearchParams.ACTIVE_USERS_IGNORE_EMPTY_QUERY).canMatchEmail(true).build()
        );

        assertThat("Received : " + results, results, contains(userD, userE));

        results = searchService.filterUsers(
                ImmutableList.of(userA, userB, userC, userD, userE),
                null,
                UserSearchParams.builder(UserSearchParams.ACTIVE_USERS_IGNORE_EMPTY_QUERY).allowEmptyQuery(true).build()
        );

        assertThat("Received : " + results, results, contains(userA, userB, userC, userD, userE));

        // handle null
        results = searchService.filterUsers(
                newArrayList(userA, userB, null, userC),
                "",
                UserSearchParams.builder(UserSearchParams.ACTIVE_USERS_IGNORE_EMPTY_QUERY).allowEmptyQuery(true).build()
        );

        assertThat("Received : " + results, results, contains(userA, userB, userC));

        // max results, would return only first 2 that match
        results = searchService.filterUsers(
                ImmutableList.of(userE, userB, userC, userD, userA),
                null,
                UserSearchParams.builder(UserSearchParams.ACTIVE_USERS_IGNORE_EMPTY_QUERY).maxResults(2).sorted(false).allowEmptyQuery(true).build()
        );

        assertThat("Received : " + results, results, contains(userE, userB));

        results = searchService.filterUsers(
                ImmutableList.of(userE, userB, userC, userD, userA),
                null,
                UserSearchParams.builder(UserSearchParams.ACTIVE_USERS_IGNORE_EMPTY_QUERY).maxResults(2).allowEmptyQuery(true).build()
        );

        assertThat("Received : " + results, results, contains(userA, userB));
    }


    @Test
    public void testFilterUsersNoResults() {
        final UserSearchService searchService = new DefaultUserPickerSearchService(null, null, authenticationContext, null, null, null, null, null, null, null, null);

        // empty list when no results
        List<ApplicationUser> results = searchService.filterUsers(
                singletonList(null),
                "",
                UserSearchParams.builder(UserSearchParams.ACTIVE_USERS_IGNORE_EMPTY_QUERY).allowEmptyQuery(true).build()
        );

        assertThat("Received : " + results, results, empty());

        final ApplicationUser userA = new MockApplicationUser("a", "Adam Smith", "asmith@example.com");

        results = searchService.filterUsers(
                ImmutableList.of(userA),
                "yet",
                UserSearchParams.builder(UserSearchParams.ACTIVE_USERS_IGNORE_EMPTY_QUERY).build()
        );

        assertThat("Received : " + results, results, empty());

        results = searchService.filterUsers(
                ImmutableList.of(userA),
                null,
                UserSearchParams.builder().allowEmptyQuery(false).build()
        );

        assertThat("Received : " + results, results, empty());

        results = searchService.filterUsers(
                ImmutableList.of(userA),
                null,
                "",
                UserSearchParams.builder().allowEmptyQuery(false).build()
        );

        assertThat("Received : " + results, results, empty());
    }

    private void assertGetPagedResults(int startIndex, int maxResults, List<Integer> expected) {
        DefaultUserPickerSearchService searchService = new DefaultUserPickerSearchService(null, null, null, null, null, null, null, null, null, null, null);
        List<Integer> allResults = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);

        @SuppressWarnings("unchecked") EntityQuery<Integer> query = mock(EntityQuery.class);
        when(query.getMaxResults()).thenReturn(maxResults);
        when(query.getStartIndex()).thenReturn(startIndex);

        assertThat(searchService.getPagedResults(query, allResults), is(expected));
    }

    private void mockUsersInCrowdService(CrowdService crowdService, ApplicationUser... users) {
        when(crowdService.search(anyQuery())).thenReturn(
                Lists.transform(Arrays.asList(users), input -> input != null ? input.getName() : null)
        );
    }

    private Query anyQuery() {
        return any(Query.class);
    }
}
