package com.atlassian.jira.workflow.function.issue;

import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.google.common.collect.ImmutableMap;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import java.util.Map;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

/**
 * @since v3.12
 */
public class TestAssignToCurrentUserFunction {
    @Rule
    public final MethodRule initMockito = MockitoJUnit.rule();

    @Mock
    private MutableIssue issue;
    @Mock
    private PermissionManager mockPermissionManager;

    private ApplicationUser user = new MockApplicationUser("admin");

    @Test
    public void testAssignToSelfWithAssignableUserPermission() {
        when(mockPermissionManager.hasPermission(Permissions.ASSIGNABLE_USER, issue, user))
                .thenReturn(true);

        AssignToCurrentUserFunction assignToCurrentUserFunction = new AssignToCurrentUserFunction() {

            PermissionManager getPermissionManager() {
                return mockPermissionManager;
            }

            protected ApplicationUser getCallerUser(Map transientVars, Map args) {
                return user;
            }
        };

        assignToCurrentUserFunction.execute(ImmutableMap.of("issue", issue), null, null);
        verify(issue).setAssignee(user);
    }

    @Test
    public void testAssignToSelfWithoutAssignableUserPermission() {
        when(mockPermissionManager.hasPermission(Permissions.ASSIGNABLE_USER, issue, user))
                .thenReturn(false);

        AssignToCurrentUserFunction assignToCurrentUserFunction = new AssignToCurrentUserFunction() {

            PermissionManager getPermissionManager() {
                return mockPermissionManager;
            }

            protected ApplicationUser getCallerUser(Map transientVars, Map args) {
                return user;
            }
        };

        assignToCurrentUserFunction.execute(ImmutableMap.of("issue", issue), null, null);
        verifyZeroInteractions(issue);
    }

    @Test
    public void testAssignToSelfWithNoUser() {
        AssignToCurrentUserFunction assignToCurrentUserFunction = new AssignToCurrentUserFunction() {
            PermissionManager getPermissionManager() {
                return null;
            }

            protected ApplicationUser getCallerUser(Map transientVars, Map args) {
                return null;
            }
        };

        assignToCurrentUserFunction.execute(ImmutableMap.of("issue", issue), null, null);
        verifyZeroInteractions(issue);
    }
}
