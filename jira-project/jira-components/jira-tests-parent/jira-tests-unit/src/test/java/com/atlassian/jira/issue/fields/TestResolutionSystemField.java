package com.atlassian.jira.issue.fields;

import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.issue.customfields.OperationContext;
import com.atlassian.jira.issue.operation.WorkflowIssueOperation;
import com.atlassian.jira.issue.resolution.Resolution;
import com.atlassian.jira.workflow.JiraWorkflow;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.opensymphony.workflow.loader.ActionDescriptor;
import com.opensymphony.workflow.loader.DescriptorFactory;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import java.util.Collection;
import java.util.Map;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestResolutionSystemField {
    private static Resolution mockResolution1 = getResolutionMock("1");
    private static Resolution mockResolution2 = getResolutionMock("2");
    private static Resolution mockResolution3 = getResolutionMock("3");
    private static Resolution mockResolution4 = getResolutionMock("4");
    private static Resolution mockResolution5 = getResolutionMock("5");
    private static Collection<Resolution> resolutions = Lists.newArrayList(mockResolution1, mockResolution2, mockResolution3, mockResolution4, mockResolution5);

    @Rule
    public final MethodRule initMockito = MockitoJUnit.rule();

    @Mock
    ConstantsManager mockConstantsManager;

    @Before
    public void setUp() throws Exception {
        when(mockConstantsManager.getResolutionObjects()).thenReturn(resolutions);
    }

    @Test
    public void getExcludeIncludeResolutionIdsWithWorkflowExcludeResolutions() {
        // given
        final OperationContext mockOperationContext = getOperationContext(JiraWorkflow.JIRA_META_ATTRIBUTE_EXCLUDE_RESOLUTION, "1,5");
        final ResolutionSystemField resolutionSystemField = new ResolutionSystemField(null, null, null, null, null, null, null, null, null);

        // when
        final Collection<String> collection = resolutionSystemField.getIncludeResolutionIds(mockOperationContext, JiraWorkflow.JIRA_META_ATTRIBUTE_EXCLUDE_RESOLUTION);

        // then
        assertThat(collection, containsInAnyOrder("1", "5"));
    }

    @Test
    public void retrieveResolutionsWithWorkflowExcludeResolutions() {
        // given
        final OperationContext mockOperationContext = getOperationContext(JiraWorkflow.JIRA_META_ATTRIBUTE_EXCLUDE_RESOLUTION, "1,3");
        final ResolutionSystemField resolutionSystemField = new ResolutionSystemField(null, null, mockConstantsManager, null, null, null, null, null, null);

        // when
        final Collection<Resolution> collection = resolutionSystemField.retrieveResolutions(mockOperationContext, null, Maps.<String, Object>newHashMap());

        // then
        assertThat(collection, containsInAnyOrder(mockResolution2, mockResolution4, mockResolution5));
    }

    @Test
    public void retrieveResolutionsWithWorkflowIncludeResolutions() {
        // given
        final OperationContext mockOperationContext = getOperationContext(JiraWorkflow.JIRA_META_ATTRIBUTE_INCLUDE_RESOLUTION, "1,3");
        final ResolutionSystemField resolutionSystemField = new ResolutionSystemField(null, null, mockConstantsManager, null, null, null, null, null, null);

        // when
        final Collection<Resolution> collection = resolutionSystemField.retrieveResolutions(mockOperationContext, null, Maps.<String, Object>newHashMap());

        // then
        assertThat(collection, containsInAnyOrder(mockResolution1, mockResolution3));
    }


    private static OperationContext getOperationContext(final String type, final String resolutionString) {
        Map<String, String> attributeMap = Maps.newHashMap();
        attributeMap.put(type, resolutionString);

        final ActionDescriptor mockActionDescription = DescriptorFactory.getFactory().createActionDescriptor();
        mockActionDescription.setMetaAttributes(attributeMap);

        final WorkflowIssueOperation mockWorkflowIssueOperation = mock(WorkflowIssueOperation.class);
        when(mockWorkflowIssueOperation.getActionDescriptor()).thenReturn(mockActionDescription);

        final OperationContext mockOperationContext = mock(OperationContext.class);
        when(mockOperationContext.getIssueOperation()).thenReturn(mockWorkflowIssueOperation);

        return mockOperationContext;
    }

    private static Resolution getResolutionMock(String resolutionId) {
        Resolution resolution = mock(Resolution.class);
        when(resolution.getId()).thenReturn(resolutionId);
        when(resolution.getName()).thenReturn(resolutionId);
        return resolution;
    }
}