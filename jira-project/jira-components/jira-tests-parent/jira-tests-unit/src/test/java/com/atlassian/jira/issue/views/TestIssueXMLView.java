package com.atlassian.jira.issue.views;

import com.atlassian.jira.event.mau.MauEventService;
import com.atlassian.jira.issue.customfields.CustomFieldType;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.layout.field.FieldLayout;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutManager;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.mock.issue.MockIssue;
import com.atlassian.jira.plugin.customfield.CustomFieldTypeModuleDescriptor;
import com.atlassian.jira.util.BuildUtilsInfo;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

/**
 * Tests a the IssueXMLView in a very simple way.
 */
public class TestIssueXMLView {
    @Rule
    public final MethodRule initMockito = MockitoJUnit.rule();

    @Mock
    private BuildUtilsInfo buildUtilsInfo;
    @Mock
    private IssueType issueType;
    @Mock
    private FieldLayout fieldLayout;
    @Mock
    private FieldLayoutManager mockFieldLayoutManager;
    @Mock
    private CustomFieldTypeModuleDescriptor customFieldTypeModuleDescriptor;
    @Mock
    private CustomField mockCustomField;
    @Mock
    private CustomFieldType mockCustomFieldType;
    @Mock
    private MauEventService mauEventService;
    // All the mocking in this method is just so that we can get the call to see if an XML view
    // exists for the CustomField to return false. In this case we want to make sure we get back
    // and empty string
    @Test
    public void testGetCustomFieldXMLNoXMLTemplateExists() {
        MockIssue mockIssue = new MockIssue(1L);

        when(issueType.getId()).thenReturn("1");
        mockIssue.setIssueTypeObject(issueType);

        when(customFieldTypeModuleDescriptor.isXMLTemplateExists()).thenReturn(false);
        when(mockCustomFieldType.getDescriptor()).thenReturn(customFieldTypeModuleDescriptor);
        when(mockCustomField.getCustomFieldType()).thenReturn(mockCustomFieldType);
        when(mockFieldLayoutManager.getFieldLayout(mockIssue)).thenReturn(fieldLayout);

        IssueXMLView issueXMLView = new IssueXMLView(null, null, mockFieldLayoutManager, null, null, null, buildUtilsInfo, null, null, mauEventService);

        assertEquals("", issueXMLView.getCustomFieldXML(mockCustomField, mockIssue));
    }

    // All the mocking in this method is just so that we can get the call to see if an XML view
    // exists for the CustomField to return true. We then want the subsequent call to get the xml
    // view data to return null, and we want to assert that the method returns empty string.
    @Test
    public void testGetCustomFieldXMLTemplateExistsButReturnsNull() {
        MockIssue mockIssue = new MockIssue(1L);
        mockIssue.setIssueTypeObject(issueType);

        when(customFieldTypeModuleDescriptor.isXMLTemplateExists()).thenReturn(true);
        when(customFieldTypeModuleDescriptor.getViewXML(null, null, null, false)).thenReturn(null);

        when(mockCustomFieldType.getDescriptor()).thenReturn(customFieldTypeModuleDescriptor);
        when(mockCustomField.getCustomFieldType()).thenReturn(mockCustomFieldType);
        when(mockCustomField.getId()).thenReturn("");
        when(mockFieldLayoutManager.getFieldLayout(mockIssue)).thenReturn(fieldLayout);

        IssueXMLView issueXMLView = new IssueXMLView(null, null, mockFieldLayoutManager, null, null, null, buildUtilsInfo, null, null, mauEventService);

        assertEquals("", issueXMLView.getCustomFieldXML(mockCustomField, mockIssue));
    }

    // All the mocking in this method is just so that we can get the call to see if an XML view
    // exists for the CustomField to return true. We then want the subsequent call to get the xml
    // view data to return SOMETHING, and we want to assert that the method returns that SOMETHING.
    @Test
    public void testGetCustomFieldXMLGivesUsStuff() {
        String testReturnValue = "SOMETHING";

        MockIssue mockIssue = new MockIssue(1L);
        mockIssue.setIssueTypeObject(issueType);

        when(customFieldTypeModuleDescriptor.isXMLTemplateExists()).thenReturn(true);
        when(customFieldTypeModuleDescriptor.getViewXML(mockCustomField, mockIssue, null, false)).thenReturn(testReturnValue);
        when(mockCustomFieldType.getDescriptor()).thenReturn(customFieldTypeModuleDescriptor);
        when(mockCustomField.getCustomFieldType()).thenReturn(mockCustomFieldType);
        when(mockCustomField.getId()).thenReturn("");
        when(mockFieldLayoutManager.getFieldLayout(mockIssue)).thenReturn(fieldLayout);

        IssueXMLView issueXMLView = new IssueXMLView(null, null, mockFieldLayoutManager, null, null, null, buildUtilsInfo, null, null, mauEventService);

        assertEquals(testReturnValue, issueXMLView.getCustomFieldXML(mockCustomField, mockIssue));
    }

}
