package com.atlassian.jira.config;

import com.atlassian.fugue.Option;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class TestIssueTypePermissionService {
    public static final String ISSUE_TYPE_ID = "0";
    private IssueTypePermissionService service;
    @Mock
    IssueTypeService issueTypeService;
    @Mock
    GlobalPermissionManager globalPermissionManager;

    @Before
    public void setUp() throws Exception {
        service = new DefaultIssueTypePermissionService(issueTypeService, globalPermissionManager);
    }

    @Test
    public void testAdminCanEditIssueType() throws Exception {
        ApplicationUser admin = mock(ApplicationUser.class);
        when(globalPermissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, admin)).thenReturn(true);

        assertTrue(service.hasPermissionToEditIssueType(admin));
    }

    @Test
    public void testNonAdminCannotEditIssueType() throws Exception {
        ApplicationUser somebody = mock(ApplicationUser.class);
        when(globalPermissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, somebody)).thenReturn(false);

        assertFalse(service.hasPermissionToEditIssueType(somebody));
    }

    @Test
    public void testUserCanViewIssueType() throws Exception {
        ApplicationUser somebody = mock(ApplicationUser.class);
        when(globalPermissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, somebody)).thenReturn(false);

        IssueType issueType = mock(IssueType.class);
        when(issueTypeService.getIssueType(somebody, ISSUE_TYPE_ID)).thenReturn(Option.some(issueType));

        assertTrue(service.hasPermissionToViewIssueType(somebody, ISSUE_TYPE_ID));
    }

    @Test
    public void testUserCannotViewIssueType() throws Exception {
        ApplicationUser somebody = mock(ApplicationUser.class);
        when(globalPermissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, somebody)).thenReturn(false);

        when(issueTypeService.getIssueType(somebody, ISSUE_TYPE_ID)).thenReturn(Option.<IssueType>none());

        assertFalse(service.hasPermissionToViewIssueType(somebody, ISSUE_TYPE_ID));
    }
}
