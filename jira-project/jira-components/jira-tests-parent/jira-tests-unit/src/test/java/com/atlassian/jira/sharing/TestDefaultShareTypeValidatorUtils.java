package com.atlassian.jira.sharing;

import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.bc.MockJiraServiceContext;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.junit.rules.MockitoContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.sharing.SharedEntity.SharePermissions;
import com.atlassian.jira.sharing.search.PrivateShareTypeSearchParameter;
import com.atlassian.jira.sharing.search.ShareTypeSearchParameter;
import com.atlassian.jira.sharing.type.GlobalShareType;
import com.atlassian.jira.sharing.type.GroupShareType;
import com.atlassian.jira.sharing.type.ShareQueryFactory;
import com.atlassian.jira.sharing.type.ShareType;
import com.atlassian.jira.sharing.type.ShareTypeFactory;
import com.atlassian.jira.sharing.type.ShareTypePermissionChecker;
import com.atlassian.jira.sharing.type.ShareTypeRenderer;
import com.atlassian.jira.sharing.type.ShareTypeValidator;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;

import java.util.Comparator;
import java.util.HashSet;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestDefaultShareTypeValidatorUtils {
    @Rule
    public MockitoContainer container = MockitoMocksInContainer.rule(this);

    private ApplicationUser user;
    @Mock
    private ShareTypeFactory typeFactory;
    @Mock
    private PermissionManager permissionManager;
    private DefaultShareTypeValidatorUtils validator;
    private HashSet<SharePermission> permissions;

    @Before
    public void setUp() throws Exception {
        permissions = new HashSet<SharePermission>();
        user = new MockApplicationUser("admin");
        validator = new DefaultShareTypeValidatorUtils(typeFactory, permissionManager);
    }


    @Test
    public void testConstructionWithNullTypeFactory() {
        try {
            new DefaultShareTypeValidatorUtils(null, permissionManager);
            fail("Not allowed to accept null argument.");
        } catch (final IllegalArgumentException e) {
            // expected.
        }
    }

    @Test
    public void testConstructionWithNullPermissionManager() {
        try {
            new DefaultShareTypeValidatorUtils(typeFactory, null);
            fail("Not allowed to accept null argument.");
        } catch (final IllegalArgumentException e) {
            // expected.
        }
    }

    @Test
    public void testIsValidSharePermissionNullEntity() {
        final JiraServiceContext ctx = new MockJiraServiceContext(user);

        try {
            validator.isValidSharePermission(ctx, null);
            fail("can't pass in null entity");
        } catch (final IllegalArgumentException e) {
            // expect
        }

        assertFalse(ctx.getErrorCollection().hasAnyErrors());
    }

    @Test
    public void testIsValidSharePermissionPrivatePermissions() {
        final JiraServiceContext ctx = new MockJiraServiceContext((ApplicationUser) null);
        final SharedEntity entity = new MockSharedEntity(1L, SearchRequest.ENTITY_TYPE, user, SharePermissions.PRIVATE);
        validator.isValidSharePermission(ctx, entity);
        assertFalse(ctx.getErrorCollection().hasAnyErrors());
    }

    @Test
    public void testIsValidSharePermissionNullPermissionType() {
        final JiraServiceContext ctx = new MockJiraServiceContext(user);
        permissions.add(new SharePermissionImpl(new ShareType.Name("Nick"), null, null));

        when(typeFactory.getShareType(new ShareType.Name("Nick"))).thenReturn(null);
        when(permissionManager.hasPermission(Permissions.CREATE_SHARED_OBJECTS, user)).thenReturn(true);

        final SharedEntity entity = new MockSharedEntity(1L, SearchRequest.ENTITY_TYPE, user, new SharePermissions(permissions));
        validator.isValidSharePermission(ctx, entity);
        assertTrue(ctx.getErrorCollection().hasAnyErrors());

    }

    @Test
    public void testIsValidSharePermissionSingletonWithMoreThan1() {
        final JiraServiceContext ctx = new MockJiraServiceContext(user);
        permissions.add(new SharePermissionImpl(GlobalShareType.TYPE, null, null));
        permissions.add(new SharePermissionImpl(GlobalShareType.TYPE, "aah", null));

        when(typeFactory.getShareType(GlobalShareType.TYPE)).thenReturn(getSingletonType(null));
        when(permissionManager.hasPermission(Permissions.CREATE_SHARED_OBJECTS, user)).thenReturn(true);

        final SharedEntity entity = new MockSharedEntity(1L, SearchRequest.ENTITY_TYPE, user, new SharePermissions(permissions));
        validator.isValidSharePermission(ctx, entity);
        assertTrue(ctx.getErrorCollection().hasAnyErrors());
    }

    @Test
    public void testIsValidSharePermissionSingletonWith1() {
        final JiraServiceContext ctx = new MockJiraServiceContext(user);
        permissions.add(new SharePermissionImpl(GlobalShareType.TYPE, null, null));

        when(typeFactory.getShareType(GlobalShareType.TYPE)).thenReturn(getSingletonType(null));
        when(permissionManager.hasPermission(Permissions.CREATE_SHARED_OBJECTS, user)).thenReturn(true);

        final SharedEntity entity = new MockSharedEntity(1L, SearchRequest.ENTITY_TYPE, user, new SharePermissions(permissions));
        validator.isValidSharePermission(ctx, entity);
        assertFalse(ctx.getErrorCollection().hasAnyErrors());
    }

    @Test
    public void testIsValidSharePermissionSingletonWith1AndError() {
        final JiraServiceContext ctx = new MockJiraServiceContext(user);
        permissions.add(new SharePermissionImpl(GlobalShareType.TYPE, null, null));

        when(typeFactory.getShareType(GlobalShareType.TYPE)).thenReturn(getSingletonType("error"));
        when(permissionManager.hasPermission(Permissions.CREATE_SHARED_OBJECTS, user)).thenReturn(true);

        final SharedEntity entity = new MockSharedEntity(1L, SearchRequest.ENTITY_TYPE, user, new SharePermissions(permissions));
        validator.isValidSharePermission(ctx, entity);
        assertTrue(ctx.getErrorCollection().hasAnyErrors());
    }

    @Test
    public void testIsValidSharePermissionMultipleNonSingletons() {
        final JiraServiceContext ctx = new MockJiraServiceContext(user);
        permissions.add(new SharePermissionImpl(GroupShareType.TYPE, "jira-user", null));
        permissions.add(new SharePermissionImpl(GroupShareType.TYPE, "jira-developer", null));

        when(typeFactory.getShareType(GroupShareType.TYPE)).thenReturn(getNonSingletonType(null));
        when(permissionManager.hasPermission(Permissions.CREATE_SHARED_OBJECTS, user)).thenReturn(true);

        final SharedEntity entity = new MockSharedEntity(1L, SearchRequest.ENTITY_TYPE, user, new SharePermissions(permissions));
        validator.isValidSharePermission(ctx, entity);
        assertFalse(ctx.getErrorCollection().hasAnyErrors());
    }

    @Test
    public void testIsValidSharePermissionNonSingletonWith1() {
        final JiraServiceContext ctx = new MockJiraServiceContext(user);
        permissions.add(new SharePermissionImpl(GroupShareType.TYPE, "jira-user", null));

        when(typeFactory.getShareType(GroupShareType.TYPE)).thenReturn(getNonSingletonType(null));
        when(permissionManager.hasPermission(Permissions.CREATE_SHARED_OBJECTS, user)).thenReturn(true);

        final SharedEntity entity = new MockSharedEntity(1L, SearchRequest.ENTITY_TYPE, user, new SharePermissions(permissions));
        validator.isValidSharePermission(ctx, entity);
        assertFalse(ctx.getErrorCollection().hasAnyErrors());
    }

    @Test
    public void testIsValidSharePermissionNonSingletonWith1AndError() {
        final JiraServiceContext ctx = new MockJiraServiceContext(user);
        permissions.add(new SharePermissionImpl(GroupShareType.TYPE, "jira-user", null));

        when(typeFactory.getShareType(GroupShareType.TYPE)).thenReturn(getNonSingletonType("error"));
        when(permissionManager.hasPermission(Permissions.CREATE_SHARED_OBJECTS, user)).thenReturn(true);

        final SharedEntity entity = new MockSharedEntity(1L, SearchRequest.ENTITY_TYPE, user, new SharePermissions(permissions));
        validator.isValidSharePermission(ctx, entity);

        assertTrue(ctx.getErrorCollection().hasAnyErrors());
    }

    @Test
    public void testIsValidSharePermissionNoSharePermission() {
        final JiraServiceContext ctx = new MockJiraServiceContext(user);

        permissions.add(new SharePermissionImpl(GroupShareType.TYPE, "jira-user", null));

        when(permissionManager.hasPermission(Permissions.CREATE_SHARED_OBJECTS, user)).thenReturn(false);

        final SharedEntity entity = new MockSharedEntity(1L, SearchRequest.ENTITY_TYPE, user, new SharePermissions(permissions));
        validator.isValidSharePermission(ctx, entity);

        assertTrue(ctx.getErrorCollection().hasAnyErrors());
    }

    @Test
    public void testIsValidSearchParameterNullTypeReturned() {
        final JiraServiceContext ctx = new MockJiraServiceContext(user);

        when(typeFactory.getShareType(PrivateShareTypeSearchParameter.PRIVATE_PARAMETER.getType())).thenReturn(null);

        final boolean result = validator.isValidSearchParameter(ctx, PrivateShareTypeSearchParameter.PRIVATE_PARAMETER);
        assertFalse(result);
        assertTrue(ctx.getErrorCollection().hasAnyErrors());
    }

    @Test
    public void testIsValidSearchParameterCallsShareType() {
        final JiraServiceContext ctx = new MockJiraServiceContext(user);

        final ShareType mockShareType = mock(ShareType.class);
        final ShareTypeValidator mockShareTypeValidator = mock(ShareTypeValidator.class);

        when(typeFactory.getShareType(PrivateShareTypeSearchParameter.PRIVATE_PARAMETER.getType())).thenReturn(mockShareType);

        when(mockShareType.getValidator()).thenReturn(mockShareTypeValidator);
        when(mockShareTypeValidator.checkSearchParameter(ctx, PrivateShareTypeSearchParameter.PRIVATE_PARAMETER)).thenReturn(true);

        final boolean result = validator.isValidSearchParameter(ctx, PrivateShareTypeSearchParameter.PRIVATE_PARAMETER);
        assertTrue(result);
        assertFalse(ctx.getErrorCollection().hasAnyErrors());
    }

    @Test
    public void testIsValidSearchParameterBadArgs() {
        final JiraServiceContext ctx = new MockJiraServiceContext(user);
        try {
            validator.isValidSearchParameter(null, PrivateShareTypeSearchParameter.PRIVATE_PARAMETER);
            fail("Should not allow null context");
        } catch (final IllegalArgumentException e) {
            // expected
        }
        try {
            validator.isValidSearchParameter(ctx, null);
            fail("Should not allow null search parameter");
        } catch (final IllegalArgumentException e) {
            // expected
        }

    }

    private ShareType getNonSingletonType(final String validatorError) {
        return new ShareType() {
            public ShareType.Name getType() {
                return null;
            }

            public boolean isSingleton() {
                return false;
            }

            public int getPriority() {
                return 0;
            }

            public ShareTypeRenderer getRenderer() {
                return null;
            }

            public ShareTypeValidator getValidator() {
                return getShareValidator(validatorError);
            }

            public ShareTypePermissionChecker getPermissionsChecker() {
                return null;
            }

            public Comparator<SharePermission> getComparator() {
                return null;
            }

            public ShareQueryFactory<? extends ShareTypeSearchParameter> getQueryFactory() {
                return null;
            }
        };
    }

    private ShareType getSingletonType(final String validatorError) {
        return new ShareType() {
            public ShareType.Name getType() {
                return null;
            }

            public boolean isSingleton() {
                return true;
            }

            public int getPriority() {
                return 0;
            }

            public ShareTypeRenderer getRenderer() {
                return null;
            }

            public ShareTypeValidator getValidator() {
                return getShareValidator(validatorError);
            }

            public ShareTypePermissionChecker getPermissionsChecker() {
                return null;
            }

            public Comparator<SharePermission> getComparator() {
                return null;
            }

            public ShareQueryFactory<? extends ShareTypeSearchParameter> getQueryFactory() {
                return null;
            }
        };
    }

    private ShareTypeValidator getShareValidator(final String error) {
        return new ShareTypeValidator() {
            public boolean checkSharePermission(final JiraServiceContext ctx, final SharePermission permission) {
                if (error != null) {
                    ctx.getErrorCollection().addErrorMessage(error);
                    return false;
                } else {
                    return true;
                }
            }

            public boolean checkSearchParameter(final JiraServiceContext ctx, final ShareTypeSearchParameter searchParameter) {
                return false;
            }
        };
    }
}
