package com.atlassian.jira.web.startup;

import com.atlassian.jira.junit.rules.MockitoContainer;
import com.atlassian.jira.mock.servlet.MockHttpServletRequest;
import com.atlassian.jira.mock.servlet.MockHttpServletResponse;
import com.atlassian.jira.web.action.MockRedirectSanitiser;
import com.atlassian.jira.web.action.RedirectSanitiser;
import org.junit.Rule;
import org.junit.Test;

import java.net.URI;
import java.util.Locale;
import java.util.function.Function;

import static javax.servlet.http.HttpServletResponse.SC_FOUND;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;

/**
 * @since v7.1.0
 */
public class StartupPageSupportTest {
    private static final String CONTEXT_PATH = "/jira";
    private static final String ORIGINAL_REQUEST = "/browse/HSP-1?filter=-1&jql=project%20%3D%20HSP";
    private static final String RETURN_TO_QUERY = "returnTo=%2Fbrowse%2FHSP-1%3Ffilter%3D-1%26jql%3Dproject%2520%253D%2520HSP";
    private static final String REDIRECTED_REQUEST = "/jira/startup.jsp?" + RETURN_TO_QUERY;

    @Rule
    public MockitoContainer mockitoContainer = new MockitoContainer(this);

    @Test
    public void testTranslatorForSupportedLocale() {
        final MockHttpServletRequest request = new MockHttpServletRequest();
        request.setLocale(Locale.GERMAN);
        final Function<String, String> translator = StartupPageSupport.getTranslator(request);
        assertThat(translator.apply("startup.page.state.unknown"), is("Initialisierung ..."));
        assertThat(translator.apply("asdf"), is("asdf"));
    }

    @Test
    public void testTranslatorForUnsupportedLocaleGetsEnglish() {
        final MockHttpServletRequest request = new MockHttpServletRequest();
        request.setLocale(new Locale("zu"));  // I suppose one day we might support Zulu, but for now...
        final Function<String, String> translator = StartupPageSupport.getTranslator(request);
        assertThat(translator.apply("startup.page.state.unknown"), is("Initialising ..."));
        assertThat(translator.apply("asdf"), is("asdf"));
    }

    @Test
    public void testTranslatorBecauseAmerica() {
        final MockHttpServletRequest request = new MockHttpServletRequest();
        request.setLocale(Locale.US);
        final Function<String, String> translator = StartupPageSupport.getTranslator(request);
        assertThat(translator.apply("startup.page.state.unknown"), is("Initializing ..."));
        assertThat(translator.apply("asdf"), is("asdf"));
    }

    @Test
    public void testIsStartupPage() {
        final MockHttpServletRequest request = new MockHttpServletRequest();
        request.setContextPath(CONTEXT_PATH);
        request.setRequestURL(CONTEXT_PATH + "/startup.jsp");
        assertThat(StartupPageSupport.isStartupPage(request), is(true));
        request.setRequestURL(CONTEXT_PATH + "/secure/errors.jsp");
        assertThat(StartupPageSupport.isStartupPage(request), is(false));
        assertThat(StartupPageSupport.isStartupPage("/startup.jsp"), is(true));
        assertThat(StartupPageSupport.isStartupPage("/secure/errors.jsp"), is(false));
        assertThat(StartupPageSupport.isStartupPage("/secure/startup.jsp"), is(false));
    }

    @Test
    public void testRedirectToStartupPage() throws Exception {
        final MockHttpServletRequest request = new MockHttpServletRequest();
        request.setContextPath(CONTEXT_PATH);
        request.setRequestURL(CONTEXT_PATH + ORIGINAL_REQUEST);
        final MockHttpServletResponse response = new MockHttpServletResponse();
        StartupPageSupport.redirectToStartupPage(request, response);
        assertThat(response.getStatus(), is(SC_FOUND));
        assertThat(response.getRedirect(), is(REDIRECTED_REQUEST));

        final URI uri = URI.create(REDIRECTED_REQUEST);
        assertThat(uri.getPath(), is(CONTEXT_PATH + "/startup.jsp"));
        assertThat(uri.getRawQuery(), is(RETURN_TO_QUERY));
    }

    @Test
    public void testRedirectWithBlankContextPath() throws Exception {
        final MockHttpServletRequest request = new MockHttpServletRequest();
        request.setContextPath("");
        request.setRequestURL("/secure/Dashboard.jspa");
        final MockHttpServletResponse response = new MockHttpServletResponse();
        StartupPageSupport.redirectToStartupPage(request, response);
        assertThat(response.getStatus(), is(SC_FOUND));
        assertThat(response.getRedirect(), is("/startup.jsp?returnTo=%2Fsecure%2FDashboard.jspa"));
    }

    @Test
    public void testReturnFromStartupJspNoRedirectSanitiser() throws Exception {
        final MockHttpServletRequest request = new MockHttpServletRequest();
        final MockHttpServletResponse response = new MockHttpServletResponse();
        request.setContextPath(CONTEXT_PATH);
        request.setRequestURL(CONTEXT_PATH + "/startup.jsp");
        request.setParameter("returnTo", ORIGINAL_REQUEST);

        StartupPageSupport.returnFromStartupJsp(request, response);

        assertThat(response.getStatus(), is(SC_FOUND));
        assertThat(response.getRedirect(), is(CONTEXT_PATH));
    }

    @Test
    public void testReturnFromStartupJspReturnToRejected() throws Exception {
        final MockHttpServletRequest request = new MockHttpServletRequest();
        final MockHttpServletResponse response = new MockHttpServletResponse();
        request.setContextPath(CONTEXT_PATH);
        request.setRequestURL(CONTEXT_PATH + "/startup.jsp");
        request.setParameter("returnTo", ORIGINAL_REQUEST);
        mockitoContainer.getMockWorker().addMock(RedirectSanitiser.class, mock(RedirectSanitiser.class));

        StartupPageSupport.returnFromStartupJsp(request, response);

        assertThat(response.getStatus(), is(SC_FOUND));
        assertThat(response.getRedirect(), is(CONTEXT_PATH));
    }

    @Test
    public void testReturnFromStartupJspReturnToAccepted() throws Exception {
        final MockHttpServletRequest request = new MockHttpServletRequest();
        final MockHttpServletResponse response = new MockHttpServletResponse();
        request.setContextPath(CONTEXT_PATH);
        request.setRequestURL(CONTEXT_PATH + "/startup.jsp");
        request.setParameter("returnTo", ORIGINAL_REQUEST);
        mockitoContainer.getMockWorker().addMock(RedirectSanitiser.class, new MockRedirectSanitiser());

        StartupPageSupport.returnFromStartupJsp(request, response);

        assertThat(response.getStatus(), is(SC_FOUND));
        assertThat(response.getRedirect(), is(CONTEXT_PATH + ORIGINAL_REQUEST));
    }
}