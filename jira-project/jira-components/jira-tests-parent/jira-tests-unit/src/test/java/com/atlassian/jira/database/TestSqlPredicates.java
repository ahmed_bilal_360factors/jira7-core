package com.atlassian.jira.database;

import com.atlassian.jira.config.database.DatabaseConfig;
import com.atlassian.jira.config.database.JndiDatasource;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.google.common.collect.ImmutableSet;
import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.core.types.dsl.StringPath;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static com.atlassian.jira.matchers.RegexMatchers.regexMatches;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

/**
 * Tests for SqlPredicates.
 * <p>
 * Note, no db-specific SqlTemplates have been provided to QueryDSL here, so any generated SQL will use QueryDSL's default
 * template. For example, the OR operator will take the form of '||', not SQL's OR.
 */
public class TestSqlPredicates {
    @Rule
    public TestRule container = MockitoMocksInContainer.forTest(this);

    private static final DatabaseConfig oracleDbConfig = new DatabaseConfig("oracle", "public", new JndiDatasource("jndi;jira"));

    private static final DatabaseConfig postgresDbConfig = new DatabaseConfig("postgres72", "public", new JndiDatasource("jndi;jira"));

    /**
     * A value for the left side of expressions
     */
    private static final StringPath left = Expressions.stringPath("left");

    @Test
    public void inAny() {
        final SqlPredicates sqlPredicates = new SqlPredicates(postgresDbConfig);
        ImmutableSet<List<String>> lists = ImmutableSet.of(Arrays.asList("a", "b", "c"), Arrays.asList("d", "e", "f"));
        assertThat(sqlPredicates.inAny(left, lists).toString(), equalTo("left in [a, b, c] || left in [d, e, f]"));
    }

    /**
     * Given a list of values that exceeds Oracle's maximum parameters per IN clause
     * When partitionIn is called for those values
     * Then the predicate is split into multiple IN...OR...IN clauses that do not exceed the maximum.
     */
    @Test
    public void partitionedIn_largeCollectionsArePartitionedForOracle() {
        final SqlPredicates sqlPredicates = new SqlPredicates(oracleDbConfig);
        final Predicate predicate = sqlPredicates.partitionedIn(left, values(2001));

        // Expect "left in [a, a, ... (1000 times)] || left in [a, a, ... (1000 times) || left = a"
        final String expectedRegex = "left in \\[(a, ){999}a\\] \\|\\| left in \\[(a, ){999}a\\] \\|\\| left = a";
        assertThat(predicate.toString(), regexMatches(expectedRegex));
    }

    @Test
    public void partitionedIn_smallCollectionsAreNotPartitionedForOracle() {
        final SqlPredicates sqlPredicates = new SqlPredicates(oracleDbConfig);
        final Predicate predicate = sqlPredicates.partitionedIn(left, values(3));
        assertThat(predicate.toString(), equalTo("left in [a, a, a]"));
    }

    @Test
    public void partitionedIn_collectionsOfExactlyOneThousandAreNotPartitionedForOracle() {
        final SqlPredicates sqlPredicates = new SqlPredicates(oracleDbConfig);
        final Predicate predicate = sqlPredicates.partitionedIn(left, values(1000));
        // Expect "left in [a, a, ... (1000 times)]
        assertThat(predicate.toString(), regexMatches("left in \\[(a, ){999}a\\]"));
    }

    @Test
    public void partitionedIn_largeCollectionsAreNotPartitionedForNonOracleDatabases() {
        final SqlPredicates sqlPredicates = new SqlPredicates(postgresDbConfig);
        final Predicate predicate = sqlPredicates.partitionedIn(left, values(2001));

        // Expect "left in [a, a, ... (1000 times)] || left in [a, a, ... (1000 times) || left = a"
        assertThat(predicate.toString(), regexMatches("left in \\[(a, ){2000}a\\]"));
    }

    /**
     * Create a collection of values
     *
     * @param count the number of values
     */
    private Collection<String> values(final int count) {
        String[] values = new String[count];
        Arrays.fill(values, "a");
        return Arrays.asList(values);
    }
}
