package com.atlassian.jira.cluster.zdu;

import com.atlassian.jira.cluster.ClusterMessageConsumer;
import com.atlassian.jira.cluster.ClusterMessagingService;
import com.atlassian.jira.config.properties.JiraProperties;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.SimpleLayout;
import org.apache.log4j.WriterAppender;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.internal.matchers.CapturingMatcher;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.StringWriter;

import static com.atlassian.jira.cluster.zdu.ClusterUpgradeStateManager.CLUSTER_UPGRADE_STATE_CHANGED;
import static com.atlassian.jira.cluster.zdu.UpgradeState.READY_TO_UPGRADE;
import static com.atlassian.jira.cluster.zdu.UpgradeState.RUNNING_UPGRADE_TASKS;
import static com.atlassian.jira.cluster.zdu.UpgradeState.STABLE;
import static com.atlassian.jira.event.HasEventListenerFor.hasEventListenerFor;
import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestClusterUpgradeLogger {
    @Mock
    ClusterMessagingService mockMessagingService;

    @Mock
    JiraProperties mockJiraProperties;

    @Mock
    JiraUpgradeStartedEvent mockStartedEvent;

    @Mock
    JiraUpgradeCancelledEvent mockCancelledEvent;

    @Mock
    JiraUpgradeApprovedEvent mockApprovedEvent;

    @Mock
    JiraUpgradeFinishedEvent mockFinishedEvent;

    private ClusterUpgradeLogger clusterUpgradeLogger;

    private Logger logger;
    private final WriterAppender appender = new WriterAppender();
    private final StringWriter logWriter = new StringWriter();
    private CapturingMatcher<ClusterMessageConsumer> clusterMessageConsumerCall;

    @Before
    public void setup() {
        clusterUpgradeLogger = new ClusterUpgradeLogger(mockMessagingService, mockJiraProperties);

        clusterMessageConsumerCall = new CapturingMatcher<>();
        when(mockJiraProperties.getProperty("line.separator")).thenReturn("\n");

        logger = Logger.getLogger(ClusterUpgradeLogger.class);
        logger.setLevel(Level.ALL);

        appender.setLayout(new SimpleLayout());
        appender.setWriter(logWriter);

        logger.addAppender(appender);
    }

    @After
    public void teardown() {
        logger.removeAppender(appender);
    }

    @Test
    public void logContainsUpgradeState() {
        clusterUpgradeLogger.logClusterUpgradeState("foo");

        assertThat(logWriter.toString(), containsString("foo"));
    }

    @Test
    public void logContainsUpgradeStateAndSenderId() {
        clusterUpgradeLogger.logClusterUpgradeState("foo", "bar");

        assertThat(logWriter.toString(), containsString("foo"));
        assertThat(logWriter.toString(), containsString("bar"));
    }

    @Test
    public void itRegistersAsClusterMessageListener() {
        verify(mockMessagingService).registerListener(eq(CLUSTER_UPGRADE_STATE_CHANGED), eq(clusterUpgradeLogger));
    }

    @Test
    public void logOnClusterMessageReceived() {
        clusterUpgradeLogger.receive("foo", "bar", "baz");

        assertThat(logWriter.toString(), containsString("bar"));
        assertThat(logWriter.toString(), containsString("baz"));
    }

    @Test
    public void shouldHaveEventListenerForUpgradeStartedEvent() {
        assertThat(ClusterUpgradeLogger.class, hasEventListenerFor(JiraUpgradeStartedEvent.class));
    }

    @Test
    public void shouldHaveEventListenerForUpgradeCancelledEvent() {
        assertThat(ClusterUpgradeLogger.class, hasEventListenerFor(JiraUpgradeCancelledEvent.class));
    }

    @Test
    public void shouldHaveEventListenerForUpgradeFinishedEvent() {
        assertThat(ClusterUpgradeLogger.class, hasEventListenerFor(JiraUpgradeFinishedEvent.class));
    }

    @Test
    public void shouldHaveEventListenerForUpgradeApprovedEvent() {
        assertThat(ClusterUpgradeLogger.class, hasEventListenerFor(JiraUpgradeApprovedEvent.class));
    }

    @Test
    public void logOnUpgradeStartedEvent() {
        clusterUpgradeLogger.onUpgradeStarted(mockStartedEvent);

        assertThat(logWriter.toString(), containsString(READY_TO_UPGRADE.toString()));
    }

    @Test
    public void logOnUpgradeCancelledEvent() {
        clusterUpgradeLogger.onUpgradeCancelled(mockCancelledEvent);

        assertThat(logWriter.toString(), containsString(STABLE.toString()));
    }

    @Test
    public void logOnUpgradeFinishedEvent() {
        clusterUpgradeLogger.onUpgradeFinished(mockFinishedEvent);

        assertThat(logWriter.toString(), containsString(STABLE.toString()));
    }

    @Test
    public void logOnUpgradeApprovedEvent() {
        clusterUpgradeLogger.onUpgradeApproved(mockApprovedEvent);

        assertThat(logWriter.toString(), containsString(RUNNING_UPGRADE_TASKS.toString()));
    }

}
