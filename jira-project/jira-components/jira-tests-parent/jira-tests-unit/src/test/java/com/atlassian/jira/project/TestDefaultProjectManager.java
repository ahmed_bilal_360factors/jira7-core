package com.atlassian.jira.project;

import com.atlassian.core.ofbiz.test.UtilsForTests;
import com.atlassian.crowd.exception.InvalidCredentialException;
import com.atlassian.crowd.exception.InvalidUserException;
import com.atlassian.crowd.exception.OperationNotPermittedException;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.association.NodeAssociationStore;
import com.atlassian.jira.association.NodeAssociationStoreImpl;
import com.atlassian.jira.avatar.AvatarManager;
import com.atlassian.jira.bc.project.ProjectTypeValidator;
import com.atlassian.jira.bc.project.component.ProjectComponent;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.config.properties.PropertiesManager;
import com.atlassian.jira.database.DbConnectionManager;
import com.atlassian.jira.entity.Entity;
import com.atlassian.jira.entity.EntityEngineImpl;
import com.atlassian.jira.entity.property.JsonEntityPropertyManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueKey;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.security.IssueSecurityLevelManager;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.mock.MockApplicationProperties;
import com.atlassian.jira.mock.ofbiz.MockOfBizDelegator;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.project.util.ProjectKeyStore;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.roles.ProjectRoleManager;
import com.atlassian.jira.transaction.MockTransactionSupport;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Iterables;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.RuleChain;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.ofbiz.core.entity.GenericEntity;
import org.ofbiz.core.entity.GenericEntityException;
import org.ofbiz.core.entity.GenericValue;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import static com.atlassian.jira.permission.ProjectPermissions.ASSIGNABLE_USER;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@SuppressWarnings("deprecation")
public class TestDefaultProjectManager {

    @Rule
    public RuleChain mockitoMocksInContainer = MockitoMocksInContainer.forTest(this);

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Mock
    private UserManager userManager;

    @Mock
    private PermissionManager permissionManager;

    @Mock
    private IssueSecurityLevelManager issueSecurityLevelManager;

    private final OfBizDelegator ofBizDelegator = new MockOfBizDelegator();
    private final NodeAssociationStore nodeAssociationStore = new NodeAssociationStoreImpl(ofBizDelegator);

    @AvailableInContainer
    @Mock
    private ProjectFactory projectFactory;

    @Mock
    private IssueManager issueManager;

    private ProjectManager testedObject;

    private Project project1, project2;
    private GenericValue projectGV1;

    private GenericValue projectCategoryGV, projectCategoryGV2;
    private ProjectCategory projectCategory, projectCategory2;

    private final UserMockFactory userMockFactory = new UserMockFactory();
    private final ProjectMockFactory projectMockFactory = new ProjectMockFactory();
    private final ComponentMockFactory componentMockFactory = new ComponentMockFactory();

    private class UserMockFactory {

        private ApplicationUser projectLead;
        private ApplicationUser componentLead;

        public ApplicationUser getProjectLead() {
            if (projectLead == null) {
                projectLead = new MockApplicationUser("project-lead");
                when(userManager.getUserByName(projectLead.getName())).thenReturn(projectLead);
                when(userManager.getUserByKey(projectLead.getName())).thenReturn(projectLead);
            }
            return projectLead;
        }

        public ApplicationUser getComponentLead() {
            if (componentLead == null) {
                componentLead = new MockApplicationUser("component-lead");
                when(userManager.getUserByName(componentLead.getName())).thenReturn(componentLead);
                when(userManager.getUserByKey(componentLead.getName())).thenReturn(componentLead);
            }
            return componentLead;
        }
    }

    private class ProjectMockFactory {
        public GenericValue getProjectGvWithDefaultAssigneeLead() {
            return UtilsForTests.getTestEntity("Project", ImmutableMap.of("name", "projectWithAssigneeLead",
                    "key", "DAL", "lead", userMockFactory.getProjectLead().getName(), "assigneetype", new Long(
                            ProjectAssigneeTypes.PROJECT_LEAD)));
        }

        public Project getProjectWithDefaultAssigneeLead() {
            return new ProjectImpl(getProjectGvWithDefaultAssigneeLead());
        }

        public GenericValue getProjectGvWithDefaultUnassigned() {
            return UtilsForTests.getTestEntity("Project", ImmutableMap.of("name",
                    "projectWithDefaultUnassigned", "key", "DUL", "assigneetype", new Long(ProjectAssigneeTypes.UNASSIGNED)));
        }

        public Project getProjectWithDefaultUnassigned() {
            return new ProjectImpl(getProjectGvWithDefaultUnassigned());
        }
    }

    private class ComponentMockFactory {

        private GenericValue componentWithProjectLeadAssignee;
        private GenericValue componentWithProjectDefaultAssignee;
        private GenericValue componentWithComponentAssignee;
        private GenericValue componentWithComponentUnassigned;
        private GenericValue componentWithProjectDefaultUnassigned;

        public GenericValue getComponentGvWithProjectLeadAssignee() {
            if (componentWithProjectLeadAssignee == null) {
                componentWithProjectLeadAssignee = UtilsForTests.getTestEntity("Component", ImmutableMap.of("name",
                        "componentWithProjectLeadAssignee", "project",
                        projectMockFactory.getProjectGvWithDefaultAssigneeLead().getLong("id"), "assigneetype", new Long(
                                ComponentAssigneeTypes.PROJECT_LEAD)));
            }
            return componentWithProjectLeadAssignee;
        }

        public ProjectComponent getComponentWithProjectLeadAssignee() {
            return Entity.PROJECT_COMPONENT.build(getComponentGvWithProjectLeadAssignee());
        }

        public GenericValue getComponentGvWithProjectDefaultAssignee() {
            if (componentWithProjectDefaultAssignee == null) {
                componentWithProjectDefaultAssignee = UtilsForTests.getTestEntity("Component", ImmutableMap.of("name",
                        "componentWithProjectDefaultAssignee", "project",
                        projectMockFactory.getProjectGvWithDefaultAssigneeLead().getLong("id"), "assigneetype", new Long(
                                ComponentAssigneeTypes.PROJECT_DEFAULT)));
            }
            return componentWithProjectDefaultAssignee;
        }

        public ProjectComponent getComponentWithProjectDefaultAssignee() {
            return Entity.PROJECT_COMPONENT.build(getComponentGvWithProjectDefaultAssignee());
        }

        public GenericValue getComponentGvWithComponentAssignee() {
            if (componentWithComponentAssignee == null) {
                componentWithComponentAssignee = UtilsForTests.getTestEntity("Component", ImmutableMap.of("name",
                        "componentWithComponentAssignee", "project", projectMockFactory.getProjectGvWithDefaultUnassigned().getLong("id"),
                        "lead", userMockFactory.getComponentLead().getName().toLowerCase(), "assigneetype", new Long(
                                ComponentAssigneeTypes.COMPONENT_LEAD)));
            }
            return componentWithComponentAssignee;
        }

        public ProjectComponent getComponentWithComponentAssignee() {
            return Entity.PROJECT_COMPONENT.build(getComponentGvWithComponentAssignee());
        }

        public GenericValue getComponentGvWithComponentUnassigned() {
            if (componentWithComponentUnassigned == null) {
                componentWithComponentUnassigned = UtilsForTests.getTestEntity("Component", ImmutableMap.of("name",
                        "componentWithComponentUnassigned", "project", projectMockFactory.getProjectGvWithDefaultUnassigned().getLong("id"),
                        "assigneetype", new Long(ComponentAssigneeTypes.UNASSIGNED)));
            }
            return componentWithComponentUnassigned;
        }

        public ProjectComponent getComponentWithComponentUnassigned() {
            return Entity.PROJECT_COMPONENT.build(getComponentGvWithComponentUnassigned());
        }

        public GenericValue getComponentGvWithProjectDefaultUnassigned() {
            if (componentWithProjectDefaultUnassigned == null) {
                componentWithProjectDefaultUnassigned = UtilsForTests.getTestEntity("Component", ImmutableMap.of("name",
                        "componentWithProjectDefaultUnassigned", "project",
                        projectMockFactory.getProjectGvWithDefaultUnassigned().getLong("id"), "assigneetype", new Long(
                                ComponentAssigneeTypes.PROJECT_DEFAULT)));
            }
            return componentWithProjectDefaultUnassigned;
        }

        public ProjectComponent getComponentWithProjectDefaultUnassigned() {
            return Entity.PROJECT_COMPONENT.build(getComponentGvWithProjectDefaultUnassigned());
        }

    }

    @Before
    public void setUp() throws Exception {
        final MockitoContainer mockitoContainer = MockitoMocksInContainer.rule(mockitoMocksInContainer);

        final ApplicationProperties applicationProperties = new MockApplicationProperties();
        testedObject = new DefaultProjectManager(ofBizDelegator, mock(DbConnectionManager.class), nodeAssociationStore, projectFactory, mock(ProjectRoleManager.class),
                issueManager, userManager, new ProjectCategoryStoreImpl(new EntityEngineImpl(ofBizDelegator)), applicationProperties,
                mock(ProjectKeyStore.class), new MockTransactionSupport(), mock(PropertiesManager.class),
                mock(JsonEntityPropertyManager.class), mock(EventPublisher.class), mock(ProjectTypeValidator.class), mock(AvatarManager.class));

        mockitoContainer.getMockWorker()
                .addMock(OfBizDelegator.class, ofBizDelegator)
                .addMock(ApplicationProperties.class, applicationProperties)
                .addMock(UserManager.class, userManager)
                .addMock(PermissionManager.class, permissionManager)
                .addMock(IssueSecurityLevelManager.class, issueSecurityLevelManager)
                .addMock(ProjectManager.class, testedObject)
                .addMock(ProjectFactory.class, projectFactory)
                .init();

        when(projectFactory.getProject(Mockito.<GenericValue>any())).thenAnswer(new Answer<Project>() {

            @Override
            public Project answer(final InvocationOnMock invocation) throws Throwable {
                return new ProjectImpl((GenericValue) invocation.getArguments()[0]);
            }

        });
        when(projectFactory.getProjects(Mockito.<Collection<GenericValue>>any())).thenAnswer(new Answer<List<Project>>() {

            @Override
            public List<Project> answer(final InvocationOnMock invocation) throws Throwable {
                final List<Project> result = new LinkedList<Project>();
                @SuppressWarnings("unchecked")
                final Collection<GenericValue> projectGVs = (Collection<GenericValue>) invocation.getArguments()[0];
                for (final GenericValue projectGV : projectGVs) {
                    result.add(new ProjectImpl(projectGV));
                }
                return result;
            }

        });

        project1 = addProject(Long.valueOf(100), "ABC", "Project 1", Long.valueOf(100));
        projectGV1 = ofBizDelegator.findById("Project", project1.getId());
        project2 = addProject(Long.valueOf(101), "XYZ", "Project 2", Long.valueOf(101));

        addIssue(project1, Long.valueOf(99));
        addIssue(project1, Long.valueOf(100));
        addIssue(project1, Long.valueOf(101));
        addIssue(project1, Long.valueOf(102));

        projectCategoryGV = addProjectCategory(Long.valueOf(30), "foo", "bar");
        projectCategory = Entity.PROJECT_CATEGORY.build(projectCategoryGV);
        projectCategoryGV2 = addProjectCategory(Long.valueOf(31), "bib", "la");
        projectCategory2 = Entity.PROJECT_CATEGORY.build(projectCategoryGV2);
    }

    @Test
    public void testGetProjectObj() {
        // non-existing project - ID is null - not a requirement at the moment
        Project project = testedObject.getProjectObj(null);
        assertNull(project);

        // non-existing project
        project = testedObject.getProjectObj(new Long(666));
        assertNull(project);

        // existing project
        project = testedObject.getProjectObj(project1.getId());
        assertEquals(project1, project);

        project = testedObject.getProjectObj(project2.getId());
        assertEquals(project2, project);
    }

    @Test
    public void testGetNextIdWithDuplicateKey() throws GenericEntityException {
        // Set up
        /**
         * project (ABC) next counter = 101 existing issue keys: ABC-99, ABC-100, ABC-101, ABC-102 getNextId() should skip counters 101 and
         * 102 as they are already associated with existing issues
         */
        assertEquals(100, testedObject.getCurrentCounterForProject(project1.getId())); // ensure that the counter starts where we think

        // Invoke and check
        assertEquals(103, testedObject.getNextId(project1));
        assertEquals(104, testedObject.getNextId(project1));
        assertEquals(105, testedObject.getNextId(project1));

        project1 = testedObject.getProjectObj(project1.getId());
        assertEquals(105, testedObject.getCurrentCounterForProject(project1.getId())); // ensure that the counter is incremented properly
        projectGV1 = ofBizDelegator.findById("Project", project1.getId());
        assertEquals(projectGV1, testedObject.getProjectObjByKey("ABC").getGenericValue());
        assertEquals(projectGV1, testedObject.getProjectObjByName("Project 1").getGenericValue());
    }

    @Test
    public void testUpdateProject() {
        testedObject.updateProject(testedObject.getProjectObjByKey("ABC"), "Snookums", "Snook snooky", "vez", null,
                AssigneeTypes.PROJECT_LEAD);
        final Project project = testedObject.getProjectObjByKey("ABC");

        assertEquals("ABC", project.getKey());
        assertEquals("Snookums", project.getName());
        assertEquals("Snook snooky", project.getDescription());
        assertEquals("vez", project.getLeadUserKey());
        assertEquals(new Long(2), project.getAssigneeType());
        assertEquals(null, project.getUrl());

        assertNotNull(testedObject.getProjectObjByName("Snookums"));
        assertNotNull(testedObject.getProjectObjByKey("ABC"));
    }

    @Test
    public void testUpdateProjectWithParametersLeadUsernameUsed() {
        MockApplicationUser user = new MockApplicationUser("vez", "vezuser");
        when(userManager.getUserByName(user.getName())).thenReturn(user);

        UpdateProjectParameters updateProjectParameters = UpdateProjectParameters.forProject(testedObject.getProjectObjByKey("ABC").getId());
        updateProjectParameters.leadUsername(user.getUsername());

        testedObject.updateProject(updateProjectParameters);
        final Project updatedProject = testedObject.getProjectObjByKey("ABC");

        assertEquals(user.getKey(), updatedProject.getLeadUserKey());
    }

    @Test
    public void testUpdateProjectWithParametersSettingProjectType() {
        UpdateProjectParameters updateProjectParameters = UpdateProjectParameters.forProject(testedObject.getProjectObjByKey("ABC").getId());
        updateProjectParameters.projectType("business");

        testedObject.updateProject(updateProjectParameters);
        final Project updatedProject = testedObject.getProjectObjByKey("ABC");

        assertEquals("business", updatedProject.getProjectTypeKey().getKey());
    }

    @Test
    public void testGetProjects() throws GenericEntityException {
        final List<Project> projects = testedObject.getProjectObjects();
        assertEquals(2, projects.size());
        assertEquals(projects.get(0), project1);
        assertEquals(projects.get(1), project2);
    }

    @Test
    public void testUpdateProjectCategory() throws GenericEntityException {
        ProjectCategory retrievedProjectCat = testedObject.getProjectCategory(projectCategoryGV.getLong("id"));
        assertEquals("foo", retrievedProjectCat.getName());
        assertEquals("bar", retrievedProjectCat.getDescription());

        testedObject.updateProjectCategory(new ProjectCategoryImpl(projectCategoryGV.getLong("id"), "A New Name", "A New Description"));

        retrievedProjectCat = testedObject.getProjectCategory(projectCategoryGV.getLong("id"));
        assertEquals("A New Name", retrievedProjectCat.getName());
        assertEquals("A New Description", retrievedProjectCat.getDescription());
    }

    @Test
    public void getProjectCategoryForProject() throws GenericEntityException {
        // null project id
        ProjectCategory actualProjectCategory = testedObject.getProjectCategoryForProject(null);
        assertNull(actualProjectCategory);

        // valid project id but no association set
        projectCategory = testedObject.getProjectCategoryForProject(project1);
        assertNull(actualProjectCategory);

        // valid project id and association exists.. return the projectCategoryGV
        actualProjectCategory = testedObject.getProjectCategoryForProject(project1);
        assertEquals(projectCategory, actualProjectCategory);
    }

    @Test
    public void testGetProjectObjectsFromProjectCategory() throws GenericEntityException {
        // test null projectCategoryGV id
        Collection<Project> projects = testedObject.getProjectObjectsFromProjectCategory(null);
        assertTrue(projects.isEmpty());

        // test a valid projectCategoryGV id associated with NO projects
        projects = testedObject.getProjectObjectsFromProjectCategory(projectCategoryGV.getLong("id"));
        assertTrue(projects.isEmpty());

        // test a valid projectCategoryGV associated with a project
        final ProjectCategory projectCategoryObject = Entity.PROJECT_CATEGORY.build(projectCategoryGV);
        testedObject.setProjectCategory(project1, projectCategoryObject);
        projects = testedObject.getProjectObjectsFromProjectCategory(projectCategoryGV.getLong("id"));
        final Project project = Iterables.getOnlyElement(projects);
        assertEquals(project1.getId(), project.getId());
        assertEquals(projectCategoryObject.getId(), project.getProjectCategory().getId());
    }

    @Test
    public void testSetProjectCategoryNotNull() throws GenericEntityException {
        // test null project
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Cannot associate a category with a null project");
        testedObject.setProjectCategory((Project) null, null);
    }

    @Test
    public void testSetProjectCategory() throws GenericEntityException {
        // test setting up a relation with a project that has no categories
        assertNull(testedObject.getProjectCategoryForProject(null));
        testedObject.setProjectCategory(project1, projectCategory);
        assertEquals(projectCategory, testedObject.getProjectCategoryForProject(project1));
        assertEquals(1, nodeAssociationStore.getSinksFromSource(projectGV1, "ProjectCategory", ProjectRelationConstants.PROJECT_CATEGORY)
                .size());

        // test setting up a relation with a project that has one category already
        testedObject.setProjectCategory(project1, projectCategory2);
        assertEquals(projectCategory2, testedObject.getProjectCategoryForProject(project1));
        assertEquals(1, nodeAssociationStore.getSinksFromSource(projectGV1, "ProjectCategory", ProjectRelationConstants.PROJECT_CATEGORY)
                .size());

        // test setting up a relation with a null category (ie no project category)
        testedObject.setProjectCategory(project1, null);
        assertEquals(null, testedObject.getProjectCategoryForProject(project1));
        assertEquals(0, nodeAssociationStore.getSinksFromSource(projectGV1, "ProjectCategory", ProjectRelationConstants.PROJECT_CATEGORY)
                .size());
    }

    @Test
    public void testDefaultAssigneeWithNoUnassigned() throws DefaultAssigneeException, OperationNotPermittedException,
            InvalidUserException, InvalidCredentialException {
        final ApplicationUser projectLead = userMockFactory.getProjectLead();
        final Project projectWithDefaultAssigneeLead = projectMockFactory.getProjectWithDefaultAssigneeLead();

        // Should be false as project lead cannot be assigned issues.
        _testDefaultAssigneeExceptionProjectLeadNoPermission(projectWithDefaultAssigneeLead, null);

        when(permissionManager.hasPermission(ASSIGNABLE_USER, projectWithDefaultAssigneeLead, projectLead)).thenReturn(
                Boolean.TRUE);

        // Should be true as project lead can be assigned issues.
        _testDefaultAssignee(projectWithDefaultAssigneeLead, null, projectLead);
    }

    @Test
    public void testDefaultAssigneeWithUnassigned() throws DefaultAssigneeException, GenericEntityException,
            OperationNotPermittedException, InvalidUserException, InvalidCredentialException {
        final ApplicationUser projectLead = userMockFactory.getProjectLead();
        final Project projectWithDefaultAssigneeLead = projectMockFactory.getProjectWithDefaultAssigneeLead();
        final Project projectWithDefaultUnassigned = projectMockFactory.getProjectWithDefaultUnassigned();

        // Should be false as unassigned is turned off and project lead cannot be assigned issues.
        _testDefaultAssigneeExceptionUnassignedNotAllowed(projectWithDefaultUnassigned, null);

        when(permissionManager.hasPermission(ASSIGNABLE_USER, projectWithDefaultAssigneeLead, projectLead)).thenReturn(
                Boolean.TRUE);
        when(permissionManager.hasPermission(ASSIGNABLE_USER, projectWithDefaultUnassigned, projectLead)).thenReturn(
                Boolean.TRUE);

        final ApplicationUser defaultAssignee = testedObject.getDefaultAssignee(projectWithDefaultAssigneeLead, (ProjectComponent) null);
        assertEquals(projectLead, defaultAssignee);

        _testDefaultAssigneeExceptionUnassignedNotAllowed(projectWithDefaultUnassigned, null);
        projectWithDefaultAssigneeLead.getGenericValue().set("lead", null);
        // Should be false as unassigned is turned off and the lead is null so it fails
        _testDefaultAssigneeExceptionLeadUserNotConfigured(projectWithDefaultAssigneeLead, null);

        // Turn on unassigned
        ComponentAccessor.getApplicationProperties().setOption(APKeys.JIRA_OPTION_ALLOWUNASSIGNED, true);

        // Should be true as unassigned is turned on
        _testDefaultAssignee(projectWithDefaultUnassigned, null, null);
    }

    @Test
    public void testDefaultAssigneeProjectLead() throws OperationNotPermittedException, InvalidUserException, InvalidCredentialException {
        final ApplicationUser projectLead = userMockFactory.getProjectLead();
        final Project projectWithDefaultAssigneeLead = projectMockFactory.getProjectWithDefaultAssigneeLead();
        final ProjectComponent componentWithProjectLeadAssignee = componentMockFactory.getComponentWithProjectLeadAssignee();

        // Should return false as project lead is unassignable
        _testDefaultAssigneeExceptionProjectLeadNoPermission(projectWithDefaultAssigneeLead, componentWithProjectLeadAssignee);

        when(permissionManager.hasPermission(ASSIGNABLE_USER, projectWithDefaultAssigneeLead, projectLead)).thenReturn(
                Boolean.TRUE);

        // Should return true as project lead is assignable
        _testDefaultAssignee(projectWithDefaultAssigneeLead, componentWithProjectLeadAssignee, projectLead);
    }

    @Test
    public void testDefaultAssigneeProjectDefault() throws OperationNotPermittedException, InvalidUserException, InvalidCredentialException {
        final ApplicationUser projectLead = userMockFactory.getProjectLead();
        final Project projectWithDefaultAssigneeLead = projectMockFactory.getProjectWithDefaultAssigneeLead();
        final ProjectComponent componentWithProjectDefaultAssignee = componentMockFactory.getComponentWithProjectDefaultAssignee();

        // Should return false as project lead is unassignable and component's default assignee is the project default
        _testDefaultAssigneeExceptionProjectLeadNoPermission(projectWithDefaultAssigneeLead, componentWithProjectDefaultAssignee);

        when(permissionManager.hasPermission(ASSIGNABLE_USER, projectWithDefaultAssigneeLead, projectLead)).thenReturn(
                Boolean.TRUE);

        // Should return true as project lead is assignable and component's default assignee is the project default
        _testDefaultAssignee(projectWithDefaultAssigneeLead, componentWithProjectDefaultAssignee, projectLead);
    }

    @Test
    public void testDefaultAssigneeComponentLead() throws OperationNotPermittedException, InvalidUserException, InvalidCredentialException {
        final ApplicationUser componentLead = userMockFactory.getComponentLead();
        final Project projectWithDefaultUnassigned = projectMockFactory.getProjectWithDefaultUnassigned();
        final ProjectComponent componentWithComponentAssignee = componentMockFactory.getComponentWithComponentAssignee();

        // Should return false as components lead is unassignable, unassigned is turned off and project lead is unassignable
        _testDefaultAssigneeExceptionUnassignedNotAllowed(projectWithDefaultUnassigned, componentWithComponentAssignee);

        when(
                permissionManager.hasPermission(ASSIGNABLE_USER, projectWithDefaultUnassigned,
                        componentLead)).thenReturn(Boolean.TRUE);

        // Should return true as component lead is assignable
        _testDefaultAssignee(projectWithDefaultUnassigned, componentWithComponentAssignee, componentLead);
    }

    @Test
    public void testDefaultAssigneeComponentUnassigned() throws OperationNotPermittedException, InvalidUserException,
            InvalidCredentialException {
        final Project projectWithDefaultUnassigned = projectMockFactory.getProjectWithDefaultUnassigned();
        final ProjectComponent componentWithComponentUnassigned = componentMockFactory.getComponentWithComponentUnassigned();

        // Should return false as unassigned is NOT allowed and component's and project's default assignee are set to
        // unassigned
        _testDefaultAssigneeExceptionUnassignedNotAllowed(projectWithDefaultUnassigned, componentWithComponentUnassigned);

        // Turn on unassigned allowed
        ComponentAccessor.getApplicationProperties().setOption(APKeys.JIRA_OPTION_ALLOWUNASSIGNED, true);

        // Should return true as unassigned is turnned ON
        _testDefaultAssignee(projectWithDefaultUnassigned, componentWithComponentUnassigned, null);
    }

    @Test
    public void testDefaultAssigneeProjectDefaultUnassigned() throws OperationNotPermittedException, InvalidUserException,
            InvalidCredentialException {
        final Project projectWithDefaultUnassigned = projectMockFactory.getProjectWithDefaultUnassigned();
        final ProjectComponent componentWithProjectDefaultUnassigned = componentMockFactory.getComponentWithProjectDefaultUnassigned();
        final ProjectComponent componentWithComponentUnassigned = componentMockFactory.getComponentWithComponentUnassigned();

        // Should return false as unassigned is NOT allowed and components default assignee is set to
        // project's default (which is unassigned)
        _testDefaultAssigneeExceptionUnassignedNotAllowed(projectWithDefaultUnassigned, componentWithProjectDefaultUnassigned);

        // Turn on unassigned allowed
        ComponentAccessor.getApplicationProperties().setOption(APKeys.JIRA_OPTION_ALLOWUNASSIGNED, true);

        // Should return true as unassigned is turnned ON!!! yippeeeee (spelled owen's way)
        _testDefaultAssignee(projectWithDefaultUnassigned, componentWithComponentUnassigned, null);
    }

    @Test
    public void testGetProjectByLead() throws OperationNotPermittedException, InvalidUserException, InvalidCredentialException {
        final GenericEntity projectWithDefaultAssigneeLead = projectMockFactory.getProjectGvWithDefaultAssigneeLead();

        // Now test that we can retrieve the project with lead
        final Collection<Project> projectsWithLead = testedObject.getProjectsLeadBy(userMockFactory.getProjectLead());
        assertEquals(1, projectsWithLead.size());
        assertEquals("projectWithAssigneeLead", projectWithDefaultAssigneeLead.getString("name"));
    }

    @Test
    public void testGetProjectByLeadWhereNoProjectsExistForLead() throws OperationNotPermittedException, InvalidUserException,
            InvalidCredentialException {
        final Collection<Project> projectsWithoutLead = testedObject.getProjectsLeadBy(userMockFactory.getComponentLead());
        assertEquals(0, projectsWithoutLead.size());
    }

    /**
     * Tests that getDefaultAssignee() fails because of lack of assignable permission in default assignee user.
     *
     * @param project   the project
     * @param component the component
     */
    private void _testDefaultAssigneeExceptionProjectLeadNoPermission(final Project project, final ProjectComponent component) {
        try {
            testedObject.getDefaultAssignee(project, component);
            fail("Expected DefaultAssigneeException");
        } catch (final DefaultAssigneeException ex) {
            assertEquals("User 'project-lead' does not have assign permission.", ex.getMessage());
        }
    }

    private void _testDefaultAssigneeExceptionUnassignedNotAllowed(final Project project, final ProjectComponent component) {
        try {
            testedObject.getDefaultAssignee(project, component);
            fail("Expected DefaultAssigneeException");
        } catch (final DefaultAssigneeException ex) {
            assertEquals("Invalid default assignee for project 'DUL'. Unassigned issues not allowed.", ex.getMessage());
        }
    }

    private void _testDefaultAssigneeExceptionLeadUserNotConfigured(final Project project, final ProjectComponent component) {
        try {
            testedObject.getDefaultAssignee(project, component);
            fail("Expected DefaultAssigneeException");
        } catch (final DefaultAssigneeException ex) {
            assertEquals("Lead user not configured.", ex.getMessage());
        }
    }

    private void _testDefaultAssignee(final Project project, final ProjectComponent component, final ApplicationUser expectedLead) {
        final ApplicationUser defaultAssignee = testedObject.getDefaultAssignee(project, component);
        assertEquals(expectedLead, defaultAssignee);
    }

    private Project addProject(final long id, final String key, final String name, final long counter) {
        final GenericValue result = UtilsForTests.getTestEntity("Project",
                ImmutableMap.of("id", id, "key", key, "name", name, "counter", counter));
        UtilsForTests.getTestEntity("ProjectKey", ImmutableMap.of("projectId", id, "projectKey", key));
        return new ProjectImpl(result);
    }

    private Issue addIssue(final Project project, final long number) throws Exception {
        final Issue result = mock(Issue.class);
        when(result.getProjectObject()).thenReturn(project);
        when(result.getNumber()).thenReturn(number);

        UtilsForTests.getTestEntity("Issue", ImmutableMap.of("number", result.getNumber(), "project", result.getProjectId()));
        when(issueManager.isExistingIssueKey(IssueKey.format(project, result.getNumber()))).thenReturn(Boolean.TRUE);

        return result;
    }

    private GenericValue addProjectCategory(final long id, final String name, final String description) {
        return UtilsForTests.getTestEntity("ProjectCategory", ImmutableMap.of("id", id, "name", name, "description", description));
    }

}
