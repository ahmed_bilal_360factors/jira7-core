package com.atlassian.jira.web.startup;

import com.atlassian.jira.ComponentManager.State;
import com.atlassian.jira.junit.rules.JiraWebActionSupportDependencies;
import com.atlassian.jira.junit.rules.MockitoContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.mock.servlet.MockHttpServletRequest;
import com.atlassian.jira.mock.servlet.MockHttpServletResponse;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.mockito.Mock;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Objects;

import static com.atlassian.jira.web.startup.StartupServlet.ACCEPT;
import static com.atlassian.jira.web.startup.StartupServlet.APPLICATION_JSON;
import static javax.servlet.http.HttpServletResponse.SC_FOUND;
import static javax.servlet.http.HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
import static javax.servlet.http.HttpServletResponse.SC_OK;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.when;

public class StartupServletTest {
    @Rule
    public MockitoContainer mockitoContainer = new MockitoContainer(this);

    private static final String BASE_URL = "http://localhost:8090/jira";
    private static final String STARTUP_JSP = "/startup.jsp";
    private static final String STARTUP_JSP_HTML = "<html><body><p>Hello, world.</p></body></html>";
    private static final String STARTUP_JSP_JSON = "[ \"hello\" : \"world\" ]";
    private static final String RETURN_TO = "/returnTo.jsp";

    private MockHttpServletRequest request = new MockHttpServletRequest();

    @Rule
    public final MockitoContainer mockContainer = MockitoMocksInContainer.rule(this);
    @Rule
    public TestRule webActionSupport = JiraWebActionSupportDependencies.build(mockContainer);

    @Mock
    private State state;

    private boolean hasJohnsonEvents;

    @Before
    public void setUp() {
        request.setContextPath(BASE_URL);
        request.setRequestURL(BASE_URL + STARTUP_JSP);
        request.setServletPath(STARTUP_JSP);
        request.setParameter("returnTo", RETURN_TO);
    }


    @Test
    public void notStartedMakesJsonRender() throws Exception {
        request.setHeader(ACCEPT, APPLICATION_JSON);
        assertContent(STARTUP_JSP_JSON);
    }

    @Test
    public void notStartedMakesNonJsonRender() throws Exception {
        assertContent(STARTUP_JSP_HTML);
    }

    @Test
    public void startedMakesJsonRender() throws Exception {
        request.setHeader(ACCEPT, APPLICATION_JSON);
        when(state.isStarted()).thenReturn(true);
        assertContent(STARTUP_JSP_JSON);
    }

    @Test
    public void startedMakesNonJsonRedirect() throws Exception {
        when(state.isStarted()).thenReturn(true);
        assertRedirect(BASE_URL + RETURN_TO);
    }

    @Test
    public void errorMakesJson500() throws Exception {
        request.setHeader(ACCEPT, APPLICATION_JSON);
        hasJohnsonEvents = true;
        assertResult(SC_INTERNAL_SERVER_ERROR, null, "");
    }

    @Test
    public void errorMakesNonJsonRedirect() throws Exception {
        hasJohnsonEvents = true;
        assertRedirect(BASE_URL + RETURN_TO);
    }


    private void assertRedirect(String redirect) throws ServletException, IOException {
        assertResult(SC_FOUND, redirect, "");
    }

    private void assertContent(String content) throws ServletException, IOException {
        assertResult(SC_OK, null, content);
    }

    private void assertResult(int status, String redirect, String content) throws ServletException, IOException {
        final StringWriter sw = new StringWriter();
        final MockHttpServletResponse response = doGet(sw);
        final String output = sw.toString();

        if (response.getStatus() != status
                || !Objects.equals(redirect, response.getRedirect())
                || !Objects.equals(content, output)) {
            fail("Expected status[" + status + "] redirect[" + redirect + "] content[" + content +
                    "]; but got response[" + response + "] output=[" + output + ']');
        }
    }

    MockHttpServletResponse doGet(final StringWriter sw) throws ServletException, IOException {
        final MockHttpServletResponse response;
        try (PrintWriter pw = new PrintWriter(sw)) {
            response = new MockHttpServletResponse(pw);
            new Fixture().doGet(request, response);
            pw.flush();
        }
        return response;
    }


    class Fixture extends StartupServlet {
        @Override
        protected State getCurrentState() {
            return state;
        }

        @Override
        protected boolean hasJohnsonEvents(final HttpServletRequest request) {
            return hasJohnsonEvents;
        }

        @Override
        protected void includeStartupJsp(final HttpServletRequest request, final HttpServletResponse response)
                throws ServletException, IOException {
            response.getWriter().print(STARTUP_JSP_HTML);
            response.setStatus(SC_OK);
        }

        @Override
        protected void writeJson(final HttpServletRequest request, final HttpServletResponse response,
                                 final State stateParam) throws IOException {
            assertThat("state", stateParam, is(state));
            response.getWriter().print(STARTUP_JSP_JSON);
        }
    }
}