package com.atlassian.jira.project.type;

import com.atlassian.application.api.Application;
import com.atlassian.fugue.Option;
import com.atlassian.jira.application.JiraApplication;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;
import java.util.List;

import static java.util.Arrays.asList;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.iterableWithSize;
import static org.hamcrest.collection.IsIterableContainingInOrder.contains;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestProjectTypeManagerImpl {
    private static final ApplicationUser USER = new MockApplicationUser("user");

    @Mock
    private JiraApplicationAdapter applicationAdapter;

    private ProjectTypeManagerImpl manager;

    private static final ProjectType BUSINESS = newProjectType("business", 10);
    private static final ProjectType SOFTWARE = newProjectType("software", 20);
    private static final ProjectType SERVICE_DESK = newProjectType("servicedesk", 30);

    @Before
    public void setUp() {
        manager = new ProjectTypeManagerImpl(applicationAdapter);
    }

    @Test
    public void noProjectTypesAreReturnedIfThereAreNoApplicationsDefined() {
        havingNoApplicationsDefined();

        Iterable<ProjectType> projectTypes = manager.getAllProjectTypes();

        assertThat(projectTypes, iterableWithSize(0));
    }

    @Test
    public void noProjectTypesAreReturnedIfNoneOfTheDefinedApplicationsHaveProjectTypes() {
        havingApplications(
                applicationWithNoProjectTypes(),
                applicationWithNoProjectTypes()
        );

        Iterable<ProjectType> projectTypes = manager.getAllProjectTypes();

        assertThat(projectTypes, iterableWithSize(0));
    }

    @Test
    public void theCorrectListOfProjectTypesIsReturnedIfTheApplicationsHaveSomeProjectTypesDefined() {
        havingApplications(
                applicationWithProjectTypes(SOFTWARE, SERVICE_DESK),
                applicationWithProjectTypes(BUSINESS)
        );

        Iterable<ProjectType> projectTypes = manager.getAllProjectTypes();

        assertThat(projectTypes, iterableWithSize(3));
        assertThat(projectTypes, contains(BUSINESS, SOFTWARE, SERVICE_DESK));
    }

    @Test
    public void noneIsReturnedWhenLookingUpAProjectTypeWithAKeyThatDoesNotExist() {
        havingApplications(applicationWithProjectTypes(SOFTWARE));

        Option<ProjectType> projectTypeByKey = manager.getByKey(new ProjectTypeKey("non-existent"));

        assertTrue(projectTypeByKey.isEmpty());
    }

    @Test
    public void theCorrectProjectTypeIsReturnedWhenLookingUpAProjectTypeWithAnExistentKey() {
        havingApplications(
                applicationWithNoProjectTypes(),
                applicationWithProjectTypes(SOFTWARE, BUSINESS)
        );

        Option<ProjectType> projectTypeByKey = manager.getByKey(new ProjectTypeKey("business"));

        assertFalse(projectTypeByKey.isEmpty());
        assertThat(projectTypeByKey.get().getKey(), is(new ProjectTypeKey("business")));
    }

    @Test
    public void noneIsReturnedWhenLookingUpAnAccessibleProjectTypeWithAKeyThatDoesNotExist() {
        havingApplicationsAccessibleInTheInstance(applicationWithProjectTypes(SOFTWARE));

        Option<ProjectType> projectTypeByKey = manager.getAccessibleProjectType(new ProjectTypeKey("non-existent"));

        assertTrue(projectTypeByKey.isEmpty());
    }

    @Test
    public void theCorrectProjectTypeIsReturnedWhenLookingUpAnAccessibleProjectTypeWithAnExistentKey() {
        havingApplicationsAccessibleInTheInstance(
                applicationWithNoProjectTypes(),
                applicationWithProjectTypes(SOFTWARE, BUSINESS)
        );

        Option<ProjectType> projectTypeByKey = manager.getAccessibleProjectType(new ProjectTypeKey("business"));

        assertFalse(projectTypeByKey.isEmpty());
        assertThat(projectTypeByKey.get().getKey(), is(new ProjectTypeKey("business")));
    }

    @Test
    public void noneIsReturnedWhenASpecificUserIsLookingUpAnAccessibleProjectTypeWithAKeyThatDoesNotExist() {
        havingApplicationsAccessibleToUser(USER, applicationWithProjectTypes(SOFTWARE));

        Option<ProjectType> projectTypeByKey = manager.getAccessibleProjectType(USER, new ProjectTypeKey("non-existent"));

        assertTrue(projectTypeByKey.isEmpty());
    }

    @Test
    public void theCorrectProjectTypeIsReturnedWhenASpecificUserIsLookingUpAnAccessibleProjectTypeWithAnExistentKey() {
        havingApplicationsAccessibleToUser(
                USER,
                applicationWithNoProjectTypes(),
                applicationWithProjectTypes(SOFTWARE, BUSINESS)
        );

        Option<ProjectType> projectTypeByKey = manager.getAccessibleProjectType(USER, new ProjectTypeKey("business"));

        assertFalse(projectTypeByKey.isEmpty());
        assertThat(projectTypeByKey.get().getKey(), is(new ProjectTypeKey("business")));
    }

    @Test
    public void theCorrectProjectTypeIsReturnedWhenAskingForTheInaccessibleType() {
        ProjectType inaccessibleProjectType = manager.getInaccessibleProjectType();

        assertThat(inaccessibleProjectType.getKey().getKey(), is("jira-project-type-inaccessible"));
        assertThat(inaccessibleProjectType.getDescriptionI18nKey(), is("jira.project.type.inaccessible.description"));
        assertThat(inaccessibleProjectType.getColor(), is("#999999"));
        assertThat(inaccessibleProjectType.getIcon().length(), is(greaterThan(0)));
    }

    @Test
    public void shouldSortTheProjectTypeByWeight() throws Exception {
        havingApplications(
                applicationWithProjectTypes(BUSINESS),
                applicationWithProjectTypes(SERVICE_DESK, SOFTWARE)
        );

        Iterable<ProjectType> projectTypes = manager.getAllProjectTypes();

        assertThat(projectTypes, iterableWithSize(3));
        assertThat(projectTypes, contains(BUSINESS, SOFTWARE, SERVICE_DESK));
    }

    @Test
    public void aProjectTypeThatIsInstalledIsNotDescribedAsUninstalled() {
        havingApplications(
                applicationWithProjectTypes(newProjectType("software"))
        );

        boolean isUninstalled = manager.isProjectTypeUninstalled(new ProjectTypeKey("software"));

        assertThat(isUninstalled, is(false));
    }

    @Test
    public void aProjectTypeThatIsNotInstalledIsNotDescribedAsUninstalled() {
        havingApplications(
                applicationWithProjectTypes(newProjectType("software"))
        );

        boolean isUninstalled = manager.isProjectTypeUninstalled(new ProjectTypeKey("other-type"));

        assertThat(isUninstalled, is(true));
    }

    @Test
    public void aProjectTypeThatIsUninstalledShouldNotBeDescribedAsInstalledButInaccessible() {
        havingApplications(applicationWithProjectTypes(newProjectType("software")));

        boolean isInstalledButInaccessible = manager.isProjectTypeInstalledButInaccessible(new ProjectTypeKey("other-type"));

        assertThat(isInstalledButInaccessible, is(false));
    }

    @Test
    public void aProjectTypeThatIsInstalledAndAccessibleShouldNotBeDescribedAsInstalledButInaccessible() {
        havingApplications(applicationWithProjectTypes(newProjectType("software")));
        havingApplicationsAccessibleInTheInstance(applicationWithProjectTypes(newProjectType("software")));

        boolean isInstalledButInaccessible = manager.isProjectTypeInstalledButInaccessible(new ProjectTypeKey("software"));

        assertThat(isInstalledButInaccessible, is(false));
    }

    @Test
    public void aProjectTypeThatIsInstalledButInaccessibleShouldBeDescribedAsInstalledButInaccessible() {
        havingApplications(applicationWithProjectTypes(newProjectType("software")));
        havingApplicationsAccessibleInTheInstance();

        boolean isInstalledButInaccessible = manager.isProjectTypeInstalledButInaccessible(new ProjectTypeKey("software"));

        assertThat(isInstalledButInaccessible, is(true));
    }

    @Test
    public void shouldReturnAllInstalledAndGloballyLicensedProjectTypesAndSortByWeight() throws Exception {
        havingApplicationsAccessibleInTheInstance(applicationWithProjectTypes(SOFTWARE), applicationWithProjectTypes(BUSINESS), applicationWithProjectTypes(SERVICE_DESK));

        final List<ProjectType> projectTypes = manager.getAllAccessibleProjectTypes();

        assertThat(projectTypes, contains(BUSINESS, SOFTWARE, SERVICE_DESK));
    }

    @Test
    public void locatesTheApplicationBasedOnTheProjectTypeIfTheApplicationExists() {
        JiraApplication application = applicationWithProjectTypes(SOFTWARE);
        havingApplications(application);

        final Option<Application> applicationWithType = manager.getApplicationWithType(SOFTWARE.getKey());

        assertThat(applicationWithType.get(), is(application));
    }

    @Test
    public void doesNotLocateTheApplicationBasedOnTheProjectTypeIfTheApplicationDoesNotExist() {
        JiraApplication application = applicationWithProjectTypes(SOFTWARE);
        havingApplications(application);

        final Option<Application> applicationWithType = manager.getApplicationWithType(BUSINESS.getKey());

        assertThat(applicationWithType.isEmpty(), is(true));
    }

    private JiraApplication applicationWithNoProjectTypes() {
        return applicationWithProjectTypes();
    }

    private JiraApplication applicationWithProjectTypes(ProjectType... types) {
        JiraApplication application = mock(JiraApplication.class);
        when(application.getProjectTypes()).thenReturn(asList(types));
        return application;
    }

    private void havingNoApplicationsDefined() {
        when(applicationAdapter.getJiraApplications()).thenReturn(Collections.<JiraApplication>emptyList());
    }

    private void havingApplications(JiraApplication... applications) {
        when(applicationAdapter.getJiraApplications()).thenReturn(asList(applications));
    }

    private void havingApplicationsAccessibleToUser(ApplicationUser user, JiraApplication... applications) {
        when(applicationAdapter.getAccessibleJiraApplications(user)).thenReturn(asList(applications));
    }

    private void havingApplicationsAccessibleInTheInstance(JiraApplication... applications) {
        when(applicationAdapter.getAccessibleJiraApplications()).thenReturn(asList(applications));
    }

    private static ProjectType newProjectType(String key) {
        return newProjectType(key, 0);
    }

    private static ProjectType newProjectType(String key, int weight) {
        return new ProjectType(new ProjectTypeKey(key), null, null, null, weight);
    }
}
