package com.atlassian.jira.imports.project.handler;

import com.atlassian.jira.imports.project.ProjectImportPersister;
import com.atlassian.jira.imports.project.core.BackupSystemInformation;
import com.atlassian.jira.imports.project.core.EntityRepresentation;
import com.atlassian.jira.imports.project.core.EntityRepresentationImpl;
import com.atlassian.jira.imports.project.core.ProjectImportResultsImpl;
import com.atlassian.jira.imports.project.customfield.ExternalCustomFieldValueImpl;
import com.atlassian.jira.imports.project.mapper.ProjectImportMapper;
import com.atlassian.jira.imports.project.mapper.ProjectImportMapperImpl;
import com.atlassian.jira.imports.project.parser.CustomFieldValueParser;
import com.atlassian.jira.imports.project.transformer.CustomFieldValueTransformer;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.web.bean.MockI18nBean;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static com.atlassian.jira.imports.project.parser.CustomFieldValueParser.CUSTOM_FIELD_VALUE_ENTITY_NAME;
import static java.util.Collections.emptyMap;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

/**
 * @since v3.13
 */
public class TestCustomFieldValuePersisterHandler {
    private final Map<String, CustomFieldValueParser> parsers = new HashMap<>();
    private ProjectImportMapper projectImportMapper = new ProjectImportMapperImpl(null, null);

    @Test
    public void testHandleMappedValue() throws Exception {
        ExternalCustomFieldValueImpl externalCustomFieldValue = new ExternalCustomFieldValueImpl("12", "123", "1234");
        EntityRepresentation entityRepresentation = new EntityRepresentationImpl(CUSTOM_FIELD_VALUE_ENTITY_NAME, emptyMap());

        final ProjectImportPersister mockProjectImportPersister = mock(ProjectImportPersister.class);
        when(mockProjectImportPersister.createEntity(entityRepresentation)).thenReturn(12L);

        final CustomFieldManager mockCustomFieldManager = mock(CustomFieldManager.class);

        final CustomFieldValueTransformer mockCustomFieldValueTransformer = mock(CustomFieldValueTransformer.class);
        when(mockCustomFieldValueTransformer.transform(projectImportMapper, externalCustomFieldValue, 12L)).thenReturn(externalCustomFieldValue);

        final CustomFieldValueParser mockCustomFieldValueParser = mock(CustomFieldValueParser.class);
        when(mockCustomFieldValueParser.parse(emptyMap())).thenReturn(externalCustomFieldValue);
        when(mockCustomFieldValueParser.getEntityRepresentation(externalCustomFieldValue)).thenReturn(entityRepresentation);

        final ProjectImportResultsImpl projectImportResults = new ProjectImportResultsImpl(0, 0, 0, 0, new MockI18nBean());
        parsers.put(CUSTOM_FIELD_VALUE_ENTITY_NAME, mockCustomFieldValueParser);
        CustomFieldValuePersisterHandler customFieldValuePersisterHandler = new CustomFieldValuePersisterHandler(mockProjectImportPersister, projectImportMapper, mockCustomFieldManager, 12L, projectImportResults, null, new ExecutorForTests(), parsers) {
            CustomFieldValueTransformer getCustomFieldValueTransformer() {
                return mockCustomFieldValueTransformer;
            }
        };

        customFieldValuePersisterHandler.handleEntity(CUSTOM_FIELD_VALUE_ENTITY_NAME, emptyMap());
        customFieldValuePersisterHandler.handleEntity("NOT_CUSTOM_FIELD_VALUE", emptyMap());

        assertThat(projectImportResults.getErrors(), hasSize(0));
    }

    @Test
    public void testHandleNullMappedValue() throws Exception {
        ExternalCustomFieldValueImpl externalCustomFieldValue = new ExternalCustomFieldValueImpl("12", "123", "1234");

        final ProjectImportPersister mockProjectImportPersister = mock(ProjectImportPersister.class);

        final CustomFieldManager mockCustomFieldManager = mock(CustomFieldManager.class);

        final CustomFieldValueTransformer mockCustomFieldValueTransformer = mock(CustomFieldValueTransformer.class);
        when(mockCustomFieldValueTransformer.transform(projectImportMapper, externalCustomFieldValue, 12L)).thenReturn(null);

        final CustomFieldValueParser mockCustomFieldValueParser = mock(CustomFieldValueParser.class);
        when(mockCustomFieldValueParser.parse(emptyMap())).thenReturn(externalCustomFieldValue);

        final BackupSystemInformation mockBackupSystemInformation = mock(BackupSystemInformation.class);
        when(mockBackupSystemInformation.getIssueKeyForId("1234")).thenReturn("TST-1");

        final ProjectImportResultsImpl projectImportResults = new ProjectImportResultsImpl(0, 0, 0, 0, new MockI18nBean());
        parsers.put(CUSTOM_FIELD_VALUE_ENTITY_NAME, mockCustomFieldValueParser);
        CustomFieldValuePersisterHandler customFieldValuePersisterHandler = new CustomFieldValuePersisterHandler(mockProjectImportPersister, projectImportMapper, mockCustomFieldManager, 12L, projectImportResults, mockBackupSystemInformation, null, parsers) {
            CustomFieldValueTransformer getCustomFieldValueTransformer() {
                return mockCustomFieldValueTransformer;
            }
        };

        customFieldValuePersisterHandler.handleEntity(CUSTOM_FIELD_VALUE_ENTITY_NAME, emptyMap());

        verifyZeroInteractions(mockProjectImportPersister);
    }

    @Test
    public void testHandleMappedValueWithError() throws Exception {
        ExternalCustomFieldValueImpl externalCustomFieldValue = new ExternalCustomFieldValueImpl("12", "123", "1234");
        EntityRepresentation entityRepresentation = new EntityRepresentationImpl(CUSTOM_FIELD_VALUE_ENTITY_NAME, emptyMap());

        final ProjectImportPersister mockProjectImportPersister = mock(ProjectImportPersister.class);
        when(mockProjectImportPersister.createEntity(entityRepresentation)).thenReturn(null);

        final CustomFieldManager mockCustomFieldManager = mock(CustomFieldManager.class);

        final CustomFieldValueTransformer mockCustomFieldValueTransformer = mock(CustomFieldValueTransformer.class);
        when(mockCustomFieldValueTransformer.transform(projectImportMapper, externalCustomFieldValue, 12L)).thenReturn(externalCustomFieldValue);

        final CustomFieldValueParser mockCustomFieldValueParser = mock(CustomFieldValueParser.class);
        when(mockCustomFieldValueParser.parse(emptyMap())).thenReturn(externalCustomFieldValue);
        when(mockCustomFieldValueParser.getEntityRepresentation(externalCustomFieldValue)).thenReturn(entityRepresentation);

        final BackupSystemInformation mockBackupSystemInformation = mock(BackupSystemInformation.class);
        when(mockBackupSystemInformation.getIssueKeyForId("1234")).thenReturn("TST-1");

        final ProjectImportResultsImpl projectImportResults = new ProjectImportResultsImpl(0, 0, 0, 0, new MockI18nBean());
        parsers.put(CUSTOM_FIELD_VALUE_ENTITY_NAME, mockCustomFieldValueParser);
        CustomFieldValuePersisterHandler customFieldValuePersisterHandler = new CustomFieldValuePersisterHandler(mockProjectImportPersister, projectImportMapper, mockCustomFieldManager, 12L, projectImportResults, mockBackupSystemInformation, new ExecutorForTests(), parsers) {
            CustomFieldValueTransformer getCustomFieldValueTransformer() {
                return mockCustomFieldValueTransformer;
            }
        };

        customFieldValuePersisterHandler.handleEntity(CUSTOM_FIELD_VALUE_ENTITY_NAME, emptyMap());
        customFieldValuePersisterHandler.handleEntity("NOT_CUSTOM_FIELD_VALUE", emptyMap());

        assertThat(projectImportResults.getErrors(), hasSize(1));
        assertThat(projectImportResults.getErrors(), hasItem("There was a problem saving custom field value with id '12' for issue 'TST-1'."));
    }
}
