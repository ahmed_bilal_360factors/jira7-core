package com.atlassian.jira.upgrade.tasks;

import com.atlassian.upgrade.api.UpgradeContext;
import com.atlassian.upgrade.spi.UpgradeTask;

public class MockUpgradeTask implements UpgradeTask {
    @Override
    public int getBuildNumber() {
        return 0;
    }

    @Override
    public String getShortDescription() {
        return "";
    }

    @Override
    public void runUpgrade(UpgradeContext upgradeContext) {
        //do nothing
    }

}