package com.atlassian.jira.security;

import com.atlassian.core.util.collection.EasyList;
import com.atlassian.jira.mock.ofbiz.MockGenericValue;
import com.atlassian.jira.ofbiz.FieldMap;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectFactory;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.util.collect.CollectionBuilder;
import com.google.common.collect.Lists;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.ofbiz.core.entity.GenericValue;

import java.util.Collection;
import java.util.List;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

/**
 *
 */
public class TestPermissionsCache {
    @Rule
    public final MethodRule initMockito = MockitoJUnit.rule();
    @Mock
    private ProjectFactory mockProjectFactory;

    @Test
    public void testPermissionsCacheNullUser() {
        final MockGenericValue mockProject1 = new MockGenericValue("project", FieldMap.build("id", 1000L));
        final MockGenericValue mockProject2 = new MockGenericValue("project", FieldMap.build("id", 1002L));
        final List<GenericValue> projectList = EasyList.build(mockProject1, mockProject2);

        when(mockProjectFactory.getProject(mockProject1)).thenReturn(new MockProject(1000));
        when(mockProjectFactory.getProject(mockProject2)).thenReturn(new MockProject(1002));

        PermissionsCache permissionsCache = new PermissionsCache(mockProjectFactory);
        permissionsCache.setProjectsWithBrowsePermission(null, projectList);
        Collection<GenericValue> projects = permissionsCache.getProjectsWithBrowsePermission(null);

        assertThat(projects, containsInAnyOrder(mockProject1, mockProject2));

        ApplicationUser user = new MockApplicationUser("test");
        projects = permissionsCache.getProjectsWithBrowsePermission(user);
        assertNull(projects);
    }

    @Test
    public void testSetProjectObjects() {
        final MockGenericValue mockProject1 = new MockGenericValue("project", FieldMap.build("id", new Long(1000)));
        final Project mockProjectObject = new MockProject(mockProject1);

        PermissionsCache permissionsCache = new PermissionsCache(null);
        permissionsCache.setProjectObjectsWithBrowsePermission(null, CollectionBuilder.newBuilder(mockProjectObject).asList());

        final Collection<Project> projects = permissionsCache.getProjectObjectsWithBrowsePermission(null);
        assertEquals(1, projects.size());
        assertEquals(Long.valueOf(1000), projects.iterator().next().getId());

        final Collection<GenericValue> projectGvs = permissionsCache.getProjectsWithBrowsePermission(null);
        assertEquals(mockProject1, projectGvs.iterator().next());
    }

    @Test
    public void testPermissionsCache() {

        final MockGenericValue mockProject1 = new MockGenericValue("project", FieldMap.build("id", new Long(1000)));
        final MockGenericValue mockProject2 = new MockGenericValue("project", FieldMap.build("id", new Long(1002)));
        final MockGenericValue mockProject3 = new MockGenericValue("project", FieldMap.build("id", new Long(1003)));
        final MockGenericValue mockProject4 = new MockGenericValue("project", FieldMap.build("id", new Long(1004)));
        ApplicationUser user = new MockApplicationUser("test");
        ApplicationUser user2 = new MockApplicationUser("test2");
        final List projectList = EasyList.build(mockProject1, mockProject2);

        when(mockProjectFactory.getProject(mockProject1)).thenReturn(new MockProject(1000));
        when(mockProjectFactory.getProject(mockProject2)).thenReturn(new MockProject(1002));
        when(mockProjectFactory.getProject(mockProject3)).thenReturn(new MockProject(1003));
        when(mockProjectFactory.getProject(mockProject4)).thenReturn(new MockProject(1004));

        PermissionsCache permissionsCache = new PermissionsCache(mockProjectFactory);
        permissionsCache.setProjectsWithBrowsePermission(null, projectList);
        permissionsCache.setProjectsWithBrowsePermission(user, Lists.<GenericValue>newArrayList(mockProject3));
        permissionsCache.setProjectsWithBrowsePermission(user2, Lists.<GenericValue>newArrayList(mockProject4));

        Collection<GenericValue> projects = permissionsCache.getProjectsWithBrowsePermission(null);

        assertNotNull(projects);
        assertEquals(2, projects.size());
        assertTrue(projects.contains(mockProject1));
        assertTrue(projects.contains(mockProject2));

        projects = permissionsCache.getProjectsWithBrowsePermission(user);
        assertNotNull(projects);
        assertEquals(1, projects.size());
        assertTrue(projects.contains(mockProject3));

        projects = permissionsCache.getProjectsWithBrowsePermission(user2);
        assertNotNull(projects);
        assertEquals(1, projects.size());
        assertTrue(projects.contains(mockProject4));

        final Collection<Project> projectObjects = permissionsCache.getProjectObjectsWithBrowsePermission(user);
        assertNotNull(projects);
        assertEquals(1, projects.size());
        assertEquals(Long.valueOf(1003), projectObjects.iterator().next().getId());
    }

    @Test
    public void testGetNullFromPermissionCache() {
        PermissionsCache permissionsCache = new PermissionsCache(null);
        Collection<GenericValue> projects = permissionsCache.getProjectsWithBrowsePermission(null);
        assertNull(projects);

        ApplicationUser user = new MockApplicationUser("test");
        projects = permissionsCache.getProjectsWithBrowsePermission(user);
        assertNull(projects);
    }
}
