package com.atlassian.jira.favourites;

import com.atlassian.cache.memory.MemoryCacheManager;
import com.atlassian.fugue.Effect;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.junit.rules.InitMockitoMocks;
import com.atlassian.jira.portal.PortalPage;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.google.common.collect.ImmutableList;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.TestRule;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.util.Collection;
import java.util.List;
import java.util.function.Function;

import static com.atlassian.jira.portal.PortalPage.ENTITY_TYPE;
import static java.util.Collections.emptyList;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

/**
 * Test for @{link TestCachingFavouritesStore}.
 *
 * @since 4.0.
 */
public class TestCachingFavouritesStore {
    @Rule
    public TestRule initMockitoMocks = new InitMockitoMocks(this);

    @Rule
    public ExpectedException exception = ExpectedException.none();

    private final static ApplicationUser USER = new MockApplicationUser("testUser");
    private final static Long ENTITY1_ID = 10L;
    private final static Long ENTITY2_ID = 11L;

    private final static PortalPage ENTITY_1 = PortalPage.id(ENTITY1_ID).name("page1").owner(USER).build();
    private final static PortalPage ENTITY_2 = PortalPage.id(ENTITY2_ID).name("page2").owner(USER).build();

    private final static List LIST_1 = ImmutableList.of(ENTITY1_ID);
    private final static List LIST_2 = ImmutableList.of(ENTITY1_ID, ENTITY2_ID);

    @Mock
    private FavouritesStore favStore;

    private FavouritesStore store;

    @Before
    public void setUp() throws Exception {
        store = new CachingFavouritesStore(favStore, new MemoryCacheManager());
    }

    @Test
    public void addFavouriteClearsCache() {
        assertThatCacheIsClearedAfterActionWithResult(store -> store.addFavourite(USER, ENTITY_1), true);
    }

    @Test
    public void failingToAddFavouriteClearsCache() {
        assertThatCacheIsClearedAfterActionWithResult(store -> store.addFavourite(USER, ENTITY_1), false);
    }

    @Test
    public void removingFavouriteClearsCache() {
        assertThatCacheIsClearedAfterActionWithResult(store -> store.removeFavourite(USER, ENTITY_1), true);
    }

    @Test
    public void failingToRemovingFavouriteClearsCache() {
        assertThatCacheIsClearedAfterActionWithResult(store -> store.removeFavourite(USER, ENTITY_1), false);
    }

    @Test
    public void isFavouriteReturnsResultsBasedOnCache() {
        List expectedPrimeList = ImmutableList.of(ENTITY1_ID);

        when(favStore.getFavouriteIds(USER.getKey(), ENTITY_TYPE)).thenReturn(expectedPrimeList, null);

        assertTrue(store.isFavourite(USER, ENTITY_1));
        assertTrue(store.isFavourite(USER, ENTITY_1));
        assertFalse(store.isFavourite(USER, ENTITY_2));
    }

    @Test
    public void nullResponseFromUnderlyingStoreIsHandledCorrectlyByIsFavouriteMethod() {
        when(favStore.getFavouriteIds(USER.getKey(), ENTITY_TYPE)).thenReturn(null);

        assertFalse(store.isFavourite(USER, ENTITY_1));
        assertFalse(store.isFavourite(USER, ENTITY_1));
        assertFalse(store.isFavourite(USER, ENTITY_2));
    }

    @Test
    public void nullResponseFromUnderlyingStoreIsTreatedAsEmptyList() {
        when(favStore.getFavouriteIds(USER.getKey(), SearchRequest.ENTITY_TYPE)).thenReturn(null);
        assertEquals(emptyList(), store.getFavouriteIds(USER, SearchRequest.ENTITY_TYPE));
    }

    @Test
    public void removingFavouritesForUserClearsCache() {
        assertThatCacheIsClearedAfterAction(store -> store.removeFavouritesForUser(USER, ENTITY_TYPE));
    }

    @Test
    public void removingFavouritesForUserClearsCacheEvenIfExceptionIsThrown() {
        Mockito.doThrow(new RuntimeException("blah")).when(favStore).removeFavouritesForUser(USER, ENTITY_TYPE);

        assertThatCacheIsClearedAfterAction(store -> {
            exception.expect(RuntimeException.class);
            exception.expectMessage("blah");
            store.removeFavouritesForUser(USER, ENTITY_TYPE);
        });
    }

    @Test
    public void testRemoveFavouritesForEntity() {
        ApplicationUser userBad = new MockApplicationUser("userBad");
        ApplicationUser userBad2 = new MockApplicationUser("userBad2");

        List<Long> portalList1 = ImmutableList.of(10L);
        List<Long> portalList2 = ImmutableList.of(10L, 11L);
        List<Long> portalList3 = ImmutableList.of(9L, 11L);

        List<Long> searchRequest1 = ImmutableList.of(10L);

        when(favStore.getFavouriteIds(USER.getKey(), ENTITY_TYPE)).thenReturn(portalList1);

        when(favStore.getFavouriteIds(userBad.getKey(), ENTITY_TYPE)).thenReturn(portalList2);

        when(favStore.getFavouriteIds(userBad2.getKey(), ENTITY_TYPE)).thenReturn(portalList3);

        when(favStore.getFavouriteIds(USER.getKey(), SearchRequest.ENTITY_TYPE)).thenReturn(searchRequest1);

        favStore.removeFavouritesForEntity(ENTITY_1);

        // After the remove the cache should be clear and everything need reloading
        when(favStore.getFavouriteIds(USER.getKey(), ENTITY_TYPE)).thenReturn(portalList1);

        when(favStore.getFavouriteIds(userBad.getKey(), ENTITY_TYPE)).thenReturn(portalList2);

        when(favStore.getFavouriteIds(userBad2.getKey(), ENTITY_TYPE)).thenReturn(portalList3);

        when(favStore.getFavouriteIds(USER.getKey(), SearchRequest.ENTITY_TYPE)).thenReturn(searchRequest1);

        store.getFavouriteIds(USER, ENTITY_TYPE);
        store.getFavouriteIds(userBad, ENTITY_TYPE);
        store.getFavouriteIds(userBad2, ENTITY_TYPE);
        store.getFavouriteIds(USER, SearchRequest.ENTITY_TYPE);

        store.removeFavouritesForEntity(ENTITY_1);

        store.getFavouriteIds(USER, ENTITY_TYPE);
        store.getFavouriteIds(userBad, ENTITY_TYPE);
        store.getFavouriteIds(userBad2, ENTITY_TYPE);
        store.getFavouriteIds(USER, SearchRequest.ENTITY_TYPE);
    }

    @Test
    public void testUpdateSequenceHappy() {
        List expectedSequenceList = ImmutableList.of(ENTITY_1);

        when(favStore.getFavouriteIds(USER.getKey(), ENTITY_TYPE)).thenReturn(LIST_1, LIST_2);

        favStore.updateSequence(USER, expectedSequenceList);

        Collection ids = store.getFavouriteIds(USER, ENTITY_TYPE);
        assertEquals(LIST_1, ids);

        store.updateSequence(USER, expectedSequenceList);

        assertEquals(LIST_2, store.getFavouriteIds(USER.getKey(), ENTITY_TYPE));
        assertEquals(LIST_2, store.getFavouriteIds(USER.getKey(), ENTITY_TYPE));
    }

    @Test
    public void testUpdateSequenceEmptyList() {
        List expectedSequenceList = ImmutableList.of();

        when(favStore.getFavouriteIds(USER.getKey(), ENTITY_TYPE)).thenReturn(LIST_1);

        favStore.updateSequence(USER, expectedSequenceList);

        Collection ids = store.getFavouriteIds(USER, ENTITY_TYPE);
        assertEquals(LIST_1, ids);

        store.updateSequence(USER, expectedSequenceList);

        assertEquals(LIST_1, store.getFavouriteIds(USER.getKey(), ENTITY_TYPE));
        assertEquals(LIST_1, store.getFavouriteIds(USER.getKey(), ENTITY_TYPE));
    }

    private void assertThatCacheIsClearedAfterAction(Effect<FavouritesStore> action) {
        when(favStore.getFavouriteIds(USER.getKey(), ENTITY_TYPE)).thenReturn(LIST_1, LIST_2, null);

        assertEquals(LIST_1, store.getFavouriteIds(USER, ENTITY_TYPE));
        assertEquals(LIST_1, store.getFavouriteIds(USER, ENTITY_TYPE));

        action.apply(store);

        assertEquals(LIST_2, store.getFavouriteIds(USER, ENTITY_TYPE));
        assertEquals(LIST_2, store.getFavouriteIds(USER, ENTITY_TYPE));
    }

    private <T> void assertThatCacheIsClearedAfterActionWithResult(Function<FavouritesStore, T> action, T actionResult) {
        when(favStore.getFavouriteIds(USER.getKey(), ENTITY_TYPE)).thenReturn(LIST_1, LIST_2, null);

        assertEquals(LIST_1, store.getFavouriteIds(USER, ENTITY_TYPE));
        assertEquals(LIST_1, store.getFavouriteIds(USER, ENTITY_TYPE));

        when(action.apply(favStore)).thenReturn(actionResult);

        assertThat(action.apply(store), equalTo(actionResult));
        assertEquals(LIST_2, store.getFavouriteIds(USER, ENTITY_TYPE));
        assertEquals(LIST_2, store.getFavouriteIds(USER, ENTITY_TYPE));
    }
}
