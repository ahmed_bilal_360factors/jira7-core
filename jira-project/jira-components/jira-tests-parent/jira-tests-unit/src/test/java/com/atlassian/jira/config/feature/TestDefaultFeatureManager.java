package com.atlassian.jira.config.feature;

import com.atlassian.cache.memory.MemoryCacheManager;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.cache.request.MockRequestCacheFactory;
import com.atlassian.jira.config.CoreFeatures;
import com.atlassian.jira.config.FeatureFlag;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.config.FeatureStore;
import com.atlassian.jira.config.InstanceFeatureManager;
import com.atlassian.jira.config.properties.JiraProperties;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.tenancy.JiraTenantImpl;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.tenancy.api.Tenant;
import com.atlassian.tenancy.api.TenantAccessor;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.TestRule;
import org.mockito.Mock;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.isA;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.internal.matchers.ThrowableCauseMatcher.hasCause;
import static org.junit.internal.matchers.ThrowableMessageMatcher.hasMessage;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Test case for {@link DefaultFeatureManager}.
 *
 * @since v4.4
 */
public class TestDefaultFeatureManager {
    @Rule
    public final TestRule initMockito = MockitoMocksInContainer.forTest(this);
    @Rule
    public final ExpectedException expectedException = ExpectedException.none();

    @AvailableInContainer
    @Mock
    private EventPublisher mockEventPublisher;
    @Mock
    private JiraAuthenticationContext authenticationContext;
    @Mock
    private FeatureStore featureStore;
    @Mock
    private TenantAccessor tenantAccessor;

    @Mock
    private JiraProperties jiraProperties;
    @Mock
    private FeatureFlagProviderAccessor featureFlagProviderAccessor;

    @Mock
    private InstanceFeatureManager instanceFeatureManager;

    @Rule
    public MockRequestCacheFactory mockRequestCacheFactory = MockRequestCacheFactory.rule();

    private final MemoryCacheManager cacheManager = new MemoryCacheManager();

    @Before
    public void setUp() {
        when(tenantAccessor.getAvailableTenants()).thenReturn(ImmutableList.<Tenant>of(new JiraTenantImpl("BaseTenant")));
    }

    @Test
    public void shouldNotAllowUsersToEnableOrDisableCoreFeature() {
        final FeatureManager tested = makeFeatureManager();

        //then
        //noinspection unchecked
        expectedException.expect(allOf(
                isA(IllegalStateException.class),
                hasMessage(equalTo("User cannot set feature 'com.atlassian.jira.config.CoreFeatures.PREVENT_COMMENTS_LIMITING' at runtime. It must be set by an admin via properties.")),
                hasCause(nullValue(Throwable.class))));

        //when
        tested.enableUserDarkFeature(new MockApplicationUser("fred"), CoreFeatures.PREVENT_COMMENTS_LIMITING.featureKey());
    }


    @Test
    public void shouldAllowUsersToEnableFeatureThatIsNotCore() throws Exception {
        final FeatureManager tested = makeFeatureManager();
        tested.enableUserDarkFeature(new MockApplicationUser("fred"), "test.feat");

        verify(featureStore).create("test.feat", "fred");
        when(featureStore.getUserFeatures("fred")).thenReturn(ImmutableSet.of("test.feat"));
        assertThat(tested.getDarkFeaturesForUser(new MockApplicationUser("fred")).isFeatureEnabled("test.feat"),
                is(true));
    }

    @Test
    public void shouldAllowUsersToEnableFeatureThatLooksLikeACoreFeatureButDoesNotExist() throws Exception {
        final FeatureManager tested = makeFeatureManager();
        final String featureKey = CoreFeatures.PREVENT_COMMENTS_LIMITING.featureKey() + 'x';
        tested.enableUserDarkFeature(new MockApplicationUser("fred"), featureKey);
        verify(featureStore).create(featureKey, "fred");
    }

    @Test
    public void shouldPickUpFeaturesFromInstanceFeatureManager() throws Exception {
        when(instanceFeatureManager.isInstanceFeatureEnabled("key1")).thenReturn(true);
        when(instanceFeatureManager.isInstanceFeatureEnabled("key2")).thenReturn(false);

        final DefaultFeatureManager tested = makeFeatureManager();

        assertTrue(tested.isEnabled("key1"));
        assertFalse(tested.isEnabled("key2"));
        assertFalse(tested.isEnabled("key3"));
    }

    @Test
    public void testFeatureIsDisabledIf_DefaultIsOff() {
        final DefaultFeatureManager tested = makeFeatureManager();

        final FeatureFlag featureFlag = FeatureFlag.featureFlag("feature");
        assertFalse(tested.isEnabled(featureFlag));
    }

    @Test
    public void testFeatureIsEnabledIf_DefaultIsOff_ButEnabledKeyIsAdded() {
        final DefaultFeatureManager tested = makeFeatureManager();
        when(instanceFeatureManager.isInstanceFeatureEnabled("feature.enabled")).thenReturn(true);
        when(instanceFeatureManager.isInstanceFeatureEnabled("feature.disabled")).thenReturn(false);

        final FeatureFlag featureFlag = FeatureFlag.featureFlag("feature");
        assertTrue(tested.isEnabled(featureFlag));
    }

    @Test
    public void testFeatureIsEnabledIf_DefaultIsOff_ButEnabledAndDisabledKeyIsAdded() {
        final DefaultFeatureManager tested = makeFeatureManager();

        when(instanceFeatureManager.isInstanceFeatureEnabled("feature.enabled")).thenReturn(true);
        when(instanceFeatureManager.isInstanceFeatureEnabled("feature.disabled")).thenReturn(true);


        final FeatureFlag featureFlag = FeatureFlag.featureFlag("feature");
        assertTrue(tested.isEnabled(featureFlag));
    }

    @Test
    public void testFeatureIsDisabledIf_DefaultIsOn() {
        final DefaultFeatureManager tested = makeFeatureManager();

        when(instanceFeatureManager.isInstanceFeatureEnabled("feature.enabled")).thenReturn(false);
        when(instanceFeatureManager.isInstanceFeatureEnabled("feature.disabled")).thenReturn(false);

        final FeatureFlag featureFlag = FeatureFlag.featureFlag("feature").onByDefault();
        assertTrue(tested.isEnabled(featureFlag));
    }

    @Test
    public void testFeatureIsEnabledIf_DefaultIsOn_AndEnabledFlagIsAdded() {
        final DefaultFeatureManager tested = makeFeatureManager();

        when(instanceFeatureManager.isInstanceFeatureEnabled("feature.enabled")).thenReturn(true);
        when(instanceFeatureManager.isInstanceFeatureEnabled("feature.disabled")).thenReturn(false);


        final FeatureFlag featureFlag = FeatureFlag.featureFlag("feature").onByDefault();
        assertTrue(tested.isEnabled(featureFlag));
    }

    @Test
    public void testFeatureIsDisabledIf_DefaultIsOn_AndEnabledDisabledFlagIsAdded() {
        final DefaultFeatureManager tested = makeFeatureManager();

        when(instanceFeatureManager.isInstanceFeatureEnabled("feature.enabled")).thenReturn(true);
        when(instanceFeatureManager.isInstanceFeatureEnabled("feature.disabled")).thenReturn(true);


        final FeatureFlag featureFlag = FeatureFlag.featureFlag("feature").onByDefault();
        assertFalse(tested.isEnabled(featureFlag));
    }

    private DefaultFeatureManager makeFeatureManager() {
        return new DefaultFeatureManager(
                authenticationContext,
                featureStore,
                jiraProperties,
                tenantAccessor,
                featureFlagProviderAccessor,
                cacheManager,
                mockRequestCacheFactory,
                instanceFeatureManager);
    }
}
