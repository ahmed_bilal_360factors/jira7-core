package com.atlassian.jira.plugin.bigpipe;

import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.mock.MockFeatureManager;
import com.atlassian.jira.mock.plugin.MockPlugin;
import com.atlassian.jira.mock.servlet.MockHttpServletRequest;
import com.atlassian.jira.mock.servlet.MockHttpServletResponse;
import com.atlassian.jira.servermetrics.ServerMetricsDetailCollector;
import com.atlassian.jira.web.ExecutingHttpRequest;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.hostcontainer.HostContainer;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugin.web.WebInterfaceManager;
import com.atlassian.plugin.web.descriptors.WebPanelRendererModuleDescriptor;
import com.atlassian.plugin.web.model.ResourceTemplateWebPanel;
import com.atlassian.plugin.web.model.WebPanel;
import com.atlassian.plugin.web.renderer.WebPanelRenderer;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.mockito.Mock;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.function.BiFunction;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(Parameterized.class)
public class TestBigPipeWebPanelModuleDescriptor {
    private static final String PLACEHOLDER_TEMPLATE = "viewissue/placeholder.vm";
    private static final String ACTIVITYBLOCK_TEMPLATE = "viewissue/activityblock.vm";
    private final BiFunction<WebPanel, Map<String, Object>, String> methodUnderTest;
    @Rule
    public MockitoContainer mockitoMocksInContainer = MockitoMocksInContainer.rule(this);

    @Mock
    private HostContainer hostContainer;

    @Mock
    private ModuleFactory moduleFactory;

    @Mock
    private WebInterfaceManager webInterfaceManager;

    @Mock
    private BigPipeService bigPipeService;

    @AvailableInContainer
    @Mock
    private PluginAccessor pluginAccessor;

    private MockFeatureManager featureManager = new MockFeatureManager();

    @Mock
    private ServerMetricsDetailCollector detailCollector;

    private MockPlugin plugin = new MyMockPlugin();

    @Mock
    private WebPanelRenderer renderer;

    @Parameterized.Parameters(name = "{0}")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
                { "getHtml", (BiFunction<WebPanel, Map<String, Object>, String>) WebPanel::getHtml},
                { "writeHtml", (BiFunction<WebPanel, Map<String, Object>, String>) (panel, context) -> {
                    StringWriter out = new StringWriter();
                    try {
                        panel.writeHtml(out, context);
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                    return out.toString();
                }}
        });
    }

    public TestBigPipeWebPanelModuleDescriptor(String testCase, BiFunction<WebPanel, Map<String, Object>, String> methodUnderTest) {
        this.methodUnderTest = methodUnderTest;
    }

    @Before
    public void setUpContainer() {
        when(hostContainer.create(ResourceTemplateWebPanel.class)).thenAnswer((invocation) -> new ResourceTemplateWebPanel(pluginAccessor));
        WebPanelRendererModuleDescriptor descriptor = mock(WebPanelRendererModuleDescriptor.class);
        when(renderer.getResourceType()).thenReturn("velocity");
        when(descriptor.getModule()).thenReturn(renderer);
        when(pluginAccessor.getEnabledModuleDescriptorsByClass(WebPanelRendererModuleDescriptor.class)).thenReturn(Collections.singletonList(descriptor));
        ResourceTemplateWebPanel resourceTemplateWebPanel = new ResourceTemplateWebPanel(pluginAccessor);
        mockitoMocksInContainer.getMockWorker().addMock(ResourceTemplateWebPanel.class, resourceTemplateWebPanel);
    }

    @Before
    public void setUpExecutingHttpRequest() {
        ExecutingHttpRequest.set(new MockHttpServletRequest(), new MockHttpServletResponse());
    }

    @After
    public void tearDownExecutingHttpRequest() {
        ExecutingHttpRequest.clear();
    }

    @Test
    public void bigPipeDisabled()
            throws Exception {
        featureManager.disable(() -> BigPipeService.FEATURE_BIG_PIPE);
        when(bigPipeService.isBigPipeEnabled()).thenReturn(false);

        String descriptorXml =
                "      <web-panel key=\"activitymodule\" location=\"atl.jira.view.issue.left.context\" weight=\"900\">\n"
                        + "        <resource name=\"view\" type=\"velocity\" location=\"" + ACTIVITYBLOCK_TEMPLATE + "\"/>\n"
                        + "        <resource name=\"bigPipe\" type=\"velocity\" location=\"" + PLACEHOLDER_TEMPLATE + "\">\n"
                        + "            <param name=\"bigPipeId\" value=\"my-pipe-id\"/>\n"
                        + "        </resource>\n"
                        + "    </web-panel>";

        Element descriptorElement = DocumentHelper.parseText(descriptorXml).getRootElement();

        BigPipeWebPanelModuleDescriptor descriptor = new BigPipeWebPanelModuleDescriptor(hostContainer, moduleFactory, webInterfaceManager, bigPipeService, featureManager, detailCollector);
        descriptor.init(plugin, descriptorElement);
        descriptor.enabled();
        methodUnderTest.apply(descriptor.getModule(), Collections.emptyMap());

        //No bigpipe 'action' methods should be called, only isBigPipeEnabled() is allowed
        verify(bigPipeService, never()).pipeContent(any(), any(), any());
        verify(bigPipeService, never()).executeSingleTask();
        verify(bigPipeService, never()).closeExecutor();
        verifyActualPanelRendered();
    }

    @Test
    public void noPriorityConfigured()
            throws Exception {
        featureManager.enable(() -> BigPipeService.FEATURE_BIG_PIPE);
        when(bigPipeService.isBigPipeEnabled()).thenReturn(true);

        /*
        String descriptorXml =
                "      <web-panel key=\"activitymodule\" location=\"atl.jira.view.issue.left.context\" weight=\"900\">\n"
                + "        <resource name=\"view\" type=\"velocity\" location=\"viewissue/activityblock.vm\"/>\n"
                + "        <resource name=\"bigPipe\" type=\"velocity\" location=\"viewissue/placeholder.vm\">\n"
                + "            <param name=\"bigPipeId\" value=\"activity-panel-pipe-id\"/>\n"
                + "            <param name=\"bigPipePriority\" value=\"100\"/>\n"
                + "            <param name=\"bigPipePriorityConfigurer\">com.atlassian.jira.plugin.bigpipe.GlobalPriorityConfigurer</param>\n"
                + "        </resource>\n"
                + "    </web-panel>";
        */

        String descriptorXml =
                "      <web-panel key=\"activitymodule\" location=\"atl.jira.view.issue.left.context\" weight=\"900\">\n"
                        + "        <resource name=\"view\" type=\"velocity\" location=\"" + ACTIVITYBLOCK_TEMPLATE + "\"/>\n"
                        + "        <resource name=\"bigPipe\" type=\"velocity\" location=\"" + PLACEHOLDER_TEMPLATE + "\">\n"
                        + "            <param name=\"bigPipeId\" value=\"my-pipe-id\"/>\n"
                        + "        </resource>\n"
                        + "    </web-panel>";

        Element descriptorElement = DocumentHelper.parseText(descriptorXml).getRootElement();

        BigPipeWebPanelModuleDescriptor descriptor = new BigPipeWebPanelModuleDescriptor(hostContainer, moduleFactory, webInterfaceManager, bigPipeService, featureManager, detailCollector);
        descriptor.init(plugin, descriptorElement);
        descriptor.enabled();
        methodUnderTest.apply(descriptor.getModule(), Collections.emptyMap());

        verify(bigPipeService).pipeContent(eq("my-pipe-id"), eq(null), any());
        verifyPlaceholderRendered();
    }

    @Test
    public void globalPriorityConfigured()
            throws Exception {
        String descriptorXml =
                "      <web-panel key=\"activitymodule\" location=\"atl.jira.view.issue.left.context\" weight=\"900\">\n"
                        + "        <resource name=\"view\" type=\"velocity\" location=\"" + ACTIVITYBLOCK_TEMPLATE + "\"/>\n"
                        + "        <resource name=\"bigPipe\" type=\"velocity\" location=\"" + PLACEHOLDER_TEMPLATE + "\">\n"
                        + "            <param name=\"bigPipeId\" value=\"my-pipe-id\"/>\n"
                        + "            <param name=\"bigPipePriority\" value=\"75\"/>\n"
                        + "        </resource>\n"
                        + "    </web-panel>";

        featureManager.enable(() -> BigPipeService.FEATURE_BIG_PIPE);
        when(bigPipeService.isBigPipeEnabled()).thenReturn(true);

        Element descriptorElement = DocumentHelper.parseText(descriptorXml).getRootElement();

        BigPipeWebPanelModuleDescriptor descriptor = new BigPipeWebPanelModuleDescriptor(hostContainer, moduleFactory, webInterfaceManager, bigPipeService, featureManager, detailCollector);
        descriptor.init(plugin, descriptorElement);
        descriptor.enabled();
        methodUnderTest.apply(descriptor.getModule(), Collections.emptyMap());

        verify(bigPipeService).pipeContent(eq("my-pipe-id"), eq(75), any());
        verifyPlaceholderRendered();
    }

    @Test
    public void customPriorityConfigurer()
            throws Exception {
        featureManager.enable(() -> BigPipeService.FEATURE_BIG_PIPE);
        when(bigPipeService.isBigPipeEnabled()).thenReturn(true);

        String descriptorXml =
                "      <web-panel key=\"activitymodule\" location=\"atl.jira.view.issue.left.context\" weight=\"900\">\n"
                        + "        <resource name=\"view\" type=\"velocity\" location=\"" + ACTIVITYBLOCK_TEMPLATE + "\"/>\n"
                        + "        <resource name=\"bigPipe\" type=\"velocity\" location=\"" + PLACEHOLDER_TEMPLATE + "\">\n"
                        + "            <param name=\"bigPipeId\" value=\"my-pipe-id\"/>\n"
                        + "            <param name=\"bigPipePriorityConfigurer\">com.atlassian.jira.plugin.bigpipe.SimplePriorityConfigurerForTesting</param>\n"
                        + "        </resource>\n"
                        + "    </web-panel>";

        Element descriptorElement = DocumentHelper.parseText(descriptorXml).getRootElement();

        BigPipeWebPanelModuleDescriptor descriptor = new BigPipeWebPanelModuleDescriptor(hostContainer, moduleFactory, webInterfaceManager, bigPipeService, featureManager, detailCollector);
        descriptor.init(plugin, descriptorElement);
        descriptor.enabled();
        methodUnderTest.apply(descriptor.getModule(), Collections.emptyMap());

        verify(bigPipeService).pipeContent(eq("my-pipe-id"), eq(SimplePriorityConfigurerForTesting.PRIORITY), any());
        verifyPlaceholderRendered();
    }

    @Test
    public void customPriorityConfigurerThatUsesContext()
            throws Exception {
        featureManager.enable(() -> BigPipeService.FEATURE_BIG_PIPE);
        when(bigPipeService.isBigPipeEnabled()).thenReturn(true);

        String descriptorXml =
                "      <web-panel key=\"activitymodule\" location=\"atl.jira.view.issue.left.context\" weight=\"900\">\n"
                        + "        <resource name=\"view\" type=\"velocity\" location=\"" + ACTIVITYBLOCK_TEMPLATE + "\"/>\n"
                        + "        <resource name=\"bigPipe\" type=\"velocity\" location=\"" + PLACEHOLDER_TEMPLATE + "\">\n"
                        + "            <param name=\"bigPipeId\" value=\"my-pipe-id\"/>\n"
                        + "            <param name=\"bigPipePriorityConfigurer\">com.atlassian.jira.plugin.bigpipe.ContextPriorityConfigurerForTesting</param>\n"
                        + "        </resource>\n"
                        + "    </web-panel>";

        Element descriptorElement = DocumentHelper.parseText(descriptorXml).getRootElement();

        BigPipeWebPanelModuleDescriptor descriptor = new BigPipeWebPanelModuleDescriptor(hostContainer, moduleFactory, webInterfaceManager, bigPipeService, featureManager, detailCollector);
        descriptor.init(plugin, descriptorElement);
        descriptor.enabled();
        Map<String, Object> contextMap = Collections.singletonMap(ContextPriorityConfigurerForTesting.PRIORITY_KEY, 99);
        methodUnderTest.apply(descriptor.getModule(), contextMap);

        verify(bigPipeService).pipeContent(eq("my-pipe-id"), eq(99), any());
        verifyPlaceholderRendered();
    }

    private void verifyPlaceholderRendered() throws IOException {
        verify(renderer, times(1)).render(eq(PLACEHOLDER_TEMPLATE), any(), any(), any());
        verify(renderer, never()).render(eq(ACTIVITYBLOCK_TEMPLATE), any(), any(), any());
    }

    private void verifyActualPanelRendered() throws IOException {
        verify(renderer, never()).render(eq(PLACEHOLDER_TEMPLATE), any(), any(), any());
        verify(renderer, times(1)).render(eq(ACTIVITYBLOCK_TEMPLATE), any(), any(), any());
    }

    /**
     * For testing BigPipe we only look up the default priority configurer in the JIRA-standard classloader so we can
     * implement loadClass to do this.
     */
    private static class MyMockPlugin extends MockPlugin {
        @SuppressWarnings("unchecked") //No way to make this safe given API design
        @Override
        public <T> Class<T> loadClass(String clazz, Class<?> callingClass) throws ClassNotFoundException {
            return (Class) Class.forName(clazz, true, callingClass.getClassLoader());
        }
    }
}
