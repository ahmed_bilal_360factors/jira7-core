package com.atlassian.jira.web.action.issue;

import com.atlassian.core.user.preferences.Preferences;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.preferences.PreferenceKeys;
import com.atlassian.jira.user.preferences.UserPreferencesManager;
import com.atlassian.jira.web.bean.PagerFilter;
import com.atlassian.jira.web.session.SessionPagerFilterManager;
import com.atlassian.jira.web.session.SessionSearchObjectManagerFactory;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.AdditionalMatchers;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import static org.mockito.Matchers.same;
import static org.mockito.Mockito.isA;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Simple test for {@link com.atlassian.jira.web.action.issue.TestSearchActionHelper}.
 *
 * @since v4.0
 */
public class TestSearchActionHelper {
    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @Mock
    private Preferences mockUserPreferences;
    @Mock
    private SessionPagerFilterManager mockSessionPagerFilterManager;
    @Mock
    private JiraAuthenticationContext mockAuthContext;
    @Mock
    private UserPreferencesManager mockPreferencesManager;
    @Mock
    private SessionSearchObjectManagerFactory mockSessionSearchObjectManagerFactory;

    @InjectMocks
    private SearchActionHelperImpl actionHelper;

    @Before
    public void setup() {
        when(mockPreferencesManager.getPreferences(null)).thenReturn(mockUserPreferences);
        when(mockSessionSearchObjectManagerFactory.createPagerFilterManager()).thenReturn(mockSessionPagerFilterManager);
    }

    /**
     * Make sure that reseting the pager works.
     */
    @Test
    public void testResetPager() {
        final PagerFilter originalPagerFilter = new PagerFilter();

        when(mockUserPreferences.getLong(PreferenceKeys.USER_ISSUES_PER_PAGE))
                .thenReturn(10L);

        actionHelper.resetPager();
        verify(mockSessionPagerFilterManager).setCurrentObject(AdditionalMatchers.not(same(originalPagerFilter)));
    }

    /**
     * Make sure that reseting the pager works when there is an error with the user's settings.
     */
    @Test
    public void testResetPagerMaxInvalid() {
        when(mockUserPreferences.getLong(PreferenceKeys.USER_ISSUES_PER_PAGE))
                .thenThrow(new NumberFormatException("Injected error from testResetPagerInvalid"));

        actionHelper.resetPager();

        final ArgumentCaptor<PagerFilter> pagerCapture = ArgumentCaptor.forClass(PagerFilter.class);
        verify(mockSessionPagerFilterManager).setCurrentObject(pagerCapture.capture());
        assertEquals(20, pagerCapture.getValue().getMax());
    }

    /**
     * Make sure that we retrieve the pager from the session.
     */
    @Test
    public void testGetPagerFilterAlreadyInSession() {
        final int max = 4859;
        final PagerFilter originalPagerFilter = new PagerFilter(max);
        when(mockSessionPagerFilterManager.getCurrentObject()).thenReturn(originalPagerFilter);

        final PagerFilter pagerFilter = actionHelper.getPagerFilter();

        assertNotNull(pagerFilter);
        assertSame(originalPagerFilter, pagerFilter);
        assertEquals(max, pagerFilter.getMax());
    }

    /**
     * Make sure that we add a new pager to the session it does not exist.
     */
    @Test
    public void testGetPagerFilterMissing() {
        final long expectedTempMax = 0x687fab;

        when(mockUserPreferences.getLong(PreferenceKeys.USER_ISSUES_PER_PAGE))
                .thenReturn(expectedTempMax);

        final PagerFilter pagerFilter = actionHelper.getPagerFilter();

        assertNotNull(pagerFilter);
        assertEquals(expectedTempMax, pagerFilter.getMax());
        verify(mockSessionPagerFilterManager).setCurrentObject(isA(PagerFilter.class));
    }

    /**
     * Make sure that the pager is returned with the correct tempMax when it is configured.
     */
    @Test
    public void testGetPagerFilterWithTempMax() {
        // max is not the same as temp max
        final PagerFilter originalPagerFilter = new PagerFilter(4859);
        when(mockSessionPagerFilterManager.getCurrentObject()).thenReturn(originalPagerFilter);
        final int tempMax = 90;

        final PagerFilter pagerFilter = actionHelper.getPagerFilter(tempMax);

        assertEquals(tempMax, pagerFilter.getMax());
        assertSame(originalPagerFilter, pagerFilter);
    }
}
