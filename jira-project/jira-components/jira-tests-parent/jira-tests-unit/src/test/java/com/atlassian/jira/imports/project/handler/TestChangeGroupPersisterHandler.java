package com.atlassian.jira.imports.project.handler;

import com.atlassian.jira.external.beans.ExternalChangeGroup;
import com.atlassian.jira.imports.project.ProjectImportPersister;
import com.atlassian.jira.imports.project.core.BackupSystemInformation;
import com.atlassian.jira.imports.project.core.ProjectImportResults;
import com.atlassian.jira.imports.project.core.ProjectImportResultsImpl;
import com.atlassian.jira.imports.project.mapper.ProjectImportMapper;
import com.atlassian.jira.imports.project.mapper.ProjectImportMapperImpl;
import com.atlassian.jira.imports.project.parser.ChangeGroupParser;
import com.atlassian.jira.imports.project.transformer.ChangeGroupTransformer;
import com.atlassian.jira.web.bean.MockI18nBean;
import org.junit.Before;
import org.junit.Test;

import static com.atlassian.jira.imports.project.parser.ChangeGroupParser.CHANGE_GROUP_ENTITY_NAME;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

/**
 * @since v3.13
 */

public class TestChangeGroupPersisterHandler {
    private ProjectImportMapper projectImportMapper;

    @Before
    public void setUp() {
        projectImportMapper = new ProjectImportMapperImpl(null, null);
    }

    @Test
    public void testHandle() throws Exception {
        ExternalChangeGroup externalChangeGroup = new ExternalChangeGroup();
        externalChangeGroup.setId("111");
        externalChangeGroup.setIssueId("34");

        final ChangeGroupParser mockChangeGroupParser = mock(ChangeGroupParser.class);
        when(mockChangeGroupParser.parse(null)).thenReturn(externalChangeGroup);
        when(mockChangeGroupParser.getEntityRepresentation(externalChangeGroup)).thenReturn(null);

        final ChangeGroupTransformer mockChangeGroupTransformer = mock(ChangeGroupTransformer.class);
        when(mockChangeGroupTransformer.transform(projectImportMapper, externalChangeGroup)).thenReturn(externalChangeGroup);

        final ProjectImportPersister mockProjectImportPersister = mock(ProjectImportPersister.class);
        when(mockProjectImportPersister.createEntity(null)).thenReturn(123L);

        final ProjectImportResultsImpl projectImportResults = new ProjectImportResultsImpl(0, 0, 0, 0, new MockI18nBean());
        ChangeGroupPersisterHandler ChangeGroupPersisterHandler = new ChangeGroupPersisterHandler(mockProjectImportPersister, projectImportMapper, projectImportResults, null, new ExecutorForTests()) {
            ChangeGroupParser getChangeGroupParser() {
                return mockChangeGroupParser;
            }

            ChangeGroupTransformer getChangeGroupTransformer() {
                return mockChangeGroupTransformer;
            }
        };

        ChangeGroupPersisterHandler.handleEntity(CHANGE_GROUP_ENTITY_NAME, null);
        ChangeGroupPersisterHandler.handleEntity("NOTChangeGroup", null);

        assertEquals("123", projectImportMapper.getChangeGroupMapper().getMappedId("111"));
        assertThat(projectImportResults.getErrors(), hasSize(0));
    }

    @Test
    public void testHandleNullTransformedChangeGroup() throws Exception {
        ProjectImportMapper projectImportMapper = mock(ProjectImportMapper.class);
        ExternalChangeGroup externalChangeGroup = new ExternalChangeGroup();
        externalChangeGroup.setIssueId("34");

        ExternalChangeGroup transformedExternalChangeGroup = new ExternalChangeGroup();

        final ChangeGroupParser mockChangeGroupParser = mock(ChangeGroupParser.class);
        when(mockChangeGroupParser.parse(null)).thenReturn(externalChangeGroup);

        final ChangeGroupTransformer mockChangeGroupTransformer = mock(ChangeGroupTransformer.class);
        when(mockChangeGroupTransformer.transform(projectImportMapper, externalChangeGroup)).thenReturn(transformedExternalChangeGroup);

        final ProjectImportPersister mockProjectImportPersister = mock(ProjectImportPersister.class);

        final BackupSystemInformation mockBackupSystemInformation = mock(BackupSystemInformation.class);
        when(mockBackupSystemInformation.getIssueKeyForId("34")).thenReturn("TST-1");

        final ProjectImportResults projectImportResults = mock(ProjectImportResults.class);
        ChangeGroupPersisterHandler ChangeGroupPersisterHandler = new ChangeGroupPersisterHandler(mockProjectImportPersister, projectImportMapper, projectImportResults, mockBackupSystemInformation, null) {
            ChangeGroupParser getChangeGroupParser() {
                return mockChangeGroupParser;
            }

            ChangeGroupTransformer getChangeGroupTransformer() {
                return mockChangeGroupTransformer;
            }
        };

        ChangeGroupPersisterHandler.handleEntity(CHANGE_GROUP_ENTITY_NAME, null);
        ChangeGroupPersisterHandler.handleEntity("NOTChangeGroup", null);

        verifyZeroInteractions(projectImportMapper, mockProjectImportPersister, projectImportResults);
    }

    @Test
    public void testHandleErrorAddingChangegroup() throws Exception {
        final ExternalChangeGroup externalChangeGroup = new ExternalChangeGroup();
        externalChangeGroup.setIssueId("34");
        externalChangeGroup.setId("12");

        final ChangeGroupParser mockChangeGroupParser = mock(ChangeGroupParser.class);
        when(mockChangeGroupParser.parse(null)).thenReturn(externalChangeGroup);
        when(mockChangeGroupParser.getEntityRepresentation(externalChangeGroup)).thenReturn(null);

        final ChangeGroupTransformer mockChangeGroupTransformer = mock(ChangeGroupTransformer.class);
        when(mockChangeGroupTransformer.transform(projectImportMapper, externalChangeGroup)).thenReturn(externalChangeGroup);

        final ProjectImportPersister mockProjectImportPersister = mock(ProjectImportPersister.class);
        when(mockProjectImportPersister.createEntity(null)).thenReturn(null);

        final BackupSystemInformation mockBackupSystemInformation = mock(BackupSystemInformation.class);
        when(mockBackupSystemInformation.getIssueKeyForId("34")).thenReturn("TST-1");

        final ProjectImportResultsImpl projectImportResults = new ProjectImportResultsImpl(0, 0, 0, 0, new MockI18nBean());
        ChangeGroupPersisterHandler changegroupPersisterHandler = new ChangeGroupPersisterHandler(mockProjectImportPersister, projectImportMapper, projectImportResults, mockBackupSystemInformation, new ExecutorForTests()) {
            ChangeGroupParser getChangeGroupParser() {
                return mockChangeGroupParser;
            }

            ChangeGroupTransformer getChangeGroupTransformer() {
                return mockChangeGroupTransformer;
            }
        };

        changegroupPersisterHandler.handleEntity(CHANGE_GROUP_ENTITY_NAME, null);
        changegroupPersisterHandler.handleEntity("NOTChangeGroup", null);

        assertThat(projectImportResults.getErrors(), hasSize(1));
        assertThat(projectImportResults.getErrors(), hasItem("There was a problem saving change group with id '12' for issue 'TST-1'."));
    }
}
