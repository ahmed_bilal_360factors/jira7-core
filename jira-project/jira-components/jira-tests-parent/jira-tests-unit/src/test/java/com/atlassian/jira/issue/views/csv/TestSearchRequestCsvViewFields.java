package com.atlassian.jira.issue.views.csv;

import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.IssueFieldConstants;
import com.atlassian.jira.issue.export.customfield.CsvIssueExporter;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.Field;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.web.component.TableLayoutFactory;
import com.google.common.collect.ImmutableList;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import java.io.IOException;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestSearchRequestCsvViewFields {

    @Rule
    public final MethodRule mockitoRule = MockitoJUnit.rule();

    @Mock
    @AvailableInContainer
    private TableLayoutFactory tableLayoutFactory;

    @Mock
    @AvailableInContainer
    private CsvIssueExporter issueExporter;

    private SearchRequestCsvViewAllFields requestAllFields;

    private SearchRequestCsvViewCurrentFields requestCurrentFields;


    @Before
    public void setUp() {
        requestAllFields = new SearchRequestCsvViewAllFields(
                mock(ApplicationProperties.class),
                issueExporter,
                mock(JiraAuthenticationContext.class),
                tableLayoutFactory
        );

        requestCurrentFields = new SearchRequestCsvViewCurrentFields(
                mock(ApplicationProperties.class),
                issueExporter,
                mock(JiraAuthenticationContext.class),
                tableLayoutFactory
        );
    }

    @Test
    public void testFieldOrdering() throws IOException, SearchException {
        final Field firstCustomField = mockCustomField("first name");
        final Field secondCustomField = mockCustomField("before first");
        final Field priorityField = mockPriorityField();
        final Field summaryField = mockSummaryField();
        final Field unknownField = mockUnknownField("First unknown field name");
        final Field secondUnknownField = mockUnknownField("Before First unknown field name");

        final List<Field> unorderedFields = ImmutableList.of(
                unknownField,
                secondUnknownField,
                firstCustomField,
                secondCustomField,
                priorityField,
                summaryField
        );

        final List<Field> expectedOrder = ImmutableList.of(
                summaryField,
                priorityField,
                secondCustomField,
                firstCustomField,
                secondUnknownField,
                unknownField
        );

        when(tableLayoutFactory.getAllUserCsvColumnsFields(any(), any())).thenReturn(unorderedFields);

        final List<Field> actualOrder = requestAllFields.getFieldsToBeExported(mock(SearchRequest.class));

        assertThat(actualOrder, contains(expectedOrder.toArray()));
    }

    @Test
    public void testPreserveOrderWhenExportingCurrentFields() {
        final List<Field> expectedFields = ImmutableList.of(
                mockCustomField("first"),
                mockSummaryField(),
                mockCustomField("third")
        );

        when(tableLayoutFactory.getCurrentUserCsvColumnsFields(any(), any())).thenReturn(expectedFields);

        final List<Field> actualFields = requestCurrentFields.getFieldsToBeExported(mock(SearchRequest.class));

        assertThat(expectedFields, contains(actualFields.toArray()));
    }

    private static Field mockSummaryField() {
        return mockField(IssueFieldConstants.SUMMARY);
    }

    private static Field mockPriorityField() {
        return mockField(IssueFieldConstants.PRIORITY);
    }

    private static Field mockCustomField(final String name) {
        final Field customField = mock(CustomField.class);
        when(customField.getName()).thenReturn(name);
        return customField;
    }

    private static Field mockUnknownField(final String name) {
        return mockField("unknown-id", name);

    }

    private static Field mockField(final String id) {
        return mockField(id, null);
    }

    private static Field mockField(final String id, final String name) {
        final Field field = mock(Field.class);
        when(field.getId()).thenReturn(id);
        when(field.getName()).thenReturn(name);
        when(field.toString()).thenReturn(String.format("Mock of Field: %s \"%s\"", id, name));
        return field;
    }
}
