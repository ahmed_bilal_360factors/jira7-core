package com.atlassian.jira.upgrade.tasks.role;

import com.atlassian.jira.application.ApplicationKeys;
import com.atlassian.jira.test.util.lic.core.CoreLicenses;
import com.atlassian.jira.test.util.lic.sd.ServiceDeskLicenses;
import com.atlassian.jira.test.util.lic.software.SoftwareLicenses;
import com.google.common.collect.ImmutableList;
import org.junit.Test;

import static com.atlassian.jira.application.ApplicationKeys.CORE;
import static com.atlassian.jira.application.ApplicationKeys.SOFTWARE;
import static com.atlassian.jira.upgrade.tasks.role.TestUtils.emptyLicenses;
import static com.atlassian.jira.upgrade.tasks.role.TestUtils.emptyState;
import static com.atlassian.jira.upgrade.tasks.role.TestUtils.newGroup;
import static com.atlassian.jira.upgrade.tasks.role.TestUtils.toLicense;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.mockito.Mockito.mock;

public class MigrationStateTest {

    @Test
    public void addAfterSaveTask() {
        //given
        Runnable task1 = mock(Runnable.class);

        final License swLicense = toLicense(SoftwareLicenses.LICENSE_SOFTWARE);
        final ApplicationRole role = ApplicationRole.forKey(SOFTWARE).addGroup(newGroup("jira-core"));

        final MigrationState state = new MigrationState(new Licenses(ImmutableList.of(swLicense)),
                new ApplicationRoles(ImmutableList.of(role)),
                ImmutableList.of(task1), new MigrationLogImpl());

        //when
        Runnable task2 = mock(Runnable.class);
        final MigrationState result = state.withAfterSaveTask(task2);

        //then
        assertThat(result, new MigrationStateMatcher().licenses(swLicense).roles(role));
        assertThat(result.afterSaveTasks(), contains(task1, task2));
    }

    @Test
    public void changeLicense() {
        //given
        final License sdLicense = toLicense(ServiceDeskLicenses.LICENSE_SERVICE_DESK_RBP);
        final License swLicense = toLicense(SoftwareLicenses.LICENSE_SOFTWARE);
        final License coreLicense = toLicense(CoreLicenses.LICENSE_CORE);

        final MigrationState state = emptyState().changeLicenses(l -> l.addLicense(sdLicense));

        //when
        final MigrationState swAndSdState = state.changeLicenses(l -> l.addLicense(swLicense));
        final MigrationState coreSwSdState = swAndSdState.changeLicenses(l -> l.addLicense(coreLicense));

        //then
        assertThat(state, new MigrationStateMatcher().licenses(sdLicense));
        assertThat(swAndSdState, new MigrationStateMatcher().licenses(sdLicense, swLicense));
        assertThat(coreSwSdState, new MigrationStateMatcher().licenses(sdLicense, swLicense, coreLicense));
    }

    @Test
    public void changeApplicationRoleForExistingRole() {
        //given
        final ApplicationRole role = ApplicationRole.forKey(CORE).addGroup(newGroup("jira-core"));
        final MigrationState state = new MigrationState(emptyLicenses(), new ApplicationRoles(ImmutableList.of(role)),
                ImmutableList.<Runnable>of(), new MigrationLogImpl());

        //when
        final MigrationState changed = state.changeApplicationRole(ApplicationKeys.CORE,
                r -> r.addGroup(newGroup("jira-software")));

        //then
        assertThat(state, new MigrationStateMatcher().roles(role));
        assertThat(changed, new MigrationStateMatcher().roles(role.addGroup(newGroup("jira-software"))));
    }

    @Test
    public void changeApplicationRoleForNewRole() {
        //given
        final ApplicationRole coreRole = ApplicationRole.forKey(CORE).addGroup(newGroup("jira-core"));
        final MigrationState state = new MigrationState(emptyLicenses(),
                new ApplicationRoles(ImmutableList.of(coreRole)), ImmutableList.<Runnable>of(),
                new MigrationLogImpl());

        //when
        final MigrationState changed = state.changeApplicationRole(ApplicationKeys.SOFTWARE, r ->
                r.addGroup(newGroup("jira-software")));

        //then
        assertThat(state, new MigrationStateMatcher().roles(coreRole));
        assertThat(changed, new MigrationStateMatcher().roles(coreRole,
                ApplicationRole.forKey(SOFTWARE).addGroup(newGroup("jira-software"))));
    }

    @Test
    public void changeEachRole() {
        //given
        final ApplicationRole coreRole = ApplicationRole.forKey(CORE).addGroup(newGroup("jira-core"));
        final ApplicationRole software = ApplicationRole.forKey(SOFTWARE).addGroup(newGroup("jira-software"));
        final ApplicationRoles roles = new ApplicationRoles(ImmutableList.of(coreRole, software));
        final MigrationState state = new MigrationState(emptyLicenses(), roles, ImmutableList.<Runnable>of(),
                new MigrationLogImpl());

        //when
        final MigrationState forEachState = state.changeEachRole(role -> role.addGroup(newGroup("new-group")));

        //then
        assertThat(state, new MigrationStateMatcher().roles(coreRole, software));
        assertThat(forEachState,
                new MigrationStateMatcher().roles(coreRole.addGroup(newGroup("new-group")), software.addGroup(newGroup("new-group"))));
    }
}