package com.atlassian.jira.plugin.customfield;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.customfields.CustomFieldType;
import com.atlassian.jira.issue.customfields.CustomFieldTypeCategory;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.rest.json.JsonData;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.module.ContainerAccessor;
import com.atlassian.plugin.module.ContainerManagedPlugin;
import com.atlassian.plugin.module.ModuleFactory;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;
import java.util.HashSet;
import java.util.Map;

import static com.atlassian.jira.plugin.customfield.CustomFieldTypeModuleDescriptorImpl.REST_SERIALIZER_ELEMENT_NAME;
import static com.atlassian.jira.plugin.customfield.CustomFieldTypeModuleDescriptorImpl.SERIALIZER_CLASS_ARGUMENT_NAME;
import static com.atlassian.jira.plugin.customfield.CustomFieldTypeModuleDescriptorImpl.VERSION_NUMBER_ARGUMENT_NAME;
import static com.google.common.collect.Sets.newHashSet;
import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestCustomFieldTypeModuleDescriptorImpl {
    private final Integer version2 = 2;
    private final Integer version3 = 3;
    private final String restSerializerClass1 = "com.atlassian.jira.plugin.customfield.FirstExtraRestSerializerForCustomField";
    private final String restSerializerClass2 = "com.atlassian.jira.plugin.customfield.SecondExtraRestSerializerForCustomField";

    private CustomFieldTypeModuleDescriptorImpl descriptor;

    @Mock
    private ContainerManagedPlugin plugin;

    private Element basicModuleDescriptor;

    @Before
    public void setUp() throws DocumentException, ClassNotFoundException {
        final String basicXML = "<customfield-type key=\"foo\" name=\"Foo\"\n" +
                "        class=\"com.atlassian.jira.issue.customfields.impl.RenderableTextCFType\">\n" +
                "    </customfield-type>";
        basicModuleDescriptor = DocumentHelper.parseText(basicXML).getRootElement();
        this.descriptor = new CustomFieldTypeModuleDescriptorImpl(null, null, ModuleFactory.LEGACY_MODULE_FACTORY, null);

        // This line is needed to prevent NPE in LegacyModuleFactory#getModuleClass
        when(plugin.<CustomFieldType>loadClass(anyString(), eq((Class<?>) null))).thenReturn(CustomFieldType.class);
    }

    @Test
    public void testNoCategoriesFromXML() {
        descriptor.init(plugin, basicModuleDescriptor);
        assertEquals(newHashSet(CustomFieldTypeCategory.ALL, CustomFieldTypeCategory.ADVANCED),
                descriptor.getCategories());
    }

    @Test
    public void testDeclaredCategoriesFromXML() {
        basicModuleDescriptor.addElement("category").addText("ALL");
        basicModuleDescriptor.addElement("category").addText("STANDARD");
        descriptor.init(plugin, basicModuleDescriptor);
        assertEquals(newHashSet(CustomFieldTypeCategory.ALL, CustomFieldTypeCategory.STANDARD),
                descriptor.getCategories());
    }

    @Test
    public void testDeduceCategories() {
        // [ALL] -> [ALL, ADVANCED]
        assertEquals(newHashSet(CustomFieldTypeCategory.ALL, CustomFieldTypeCategory.ADVANCED),
                descriptor.deduceCategories(newHashSet("ALL")));

        // [ALL, ADVANCED] -> [ALL, ADVANCED]
        assertEquals(newHashSet(CustomFieldTypeCategory.ALL, CustomFieldTypeCategory.ADVANCED),
                descriptor.deduceCategories(newHashSet("ALL", "ADVANCED")));

        // [ADVANCED] -> [ALL, ADVANCED]
        assertEquals(newHashSet(CustomFieldTypeCategory.ALL, CustomFieldTypeCategory.ADVANCED),
                descriptor.deduceCategories(newHashSet("ADVANCED")));

        // [] -> [ALL, ADVANCED]
        assertEquals(newHashSet(CustomFieldTypeCategory.ALL, CustomFieldTypeCategory.ADVANCED),
                descriptor.deduceCategories(new HashSet<String>()));

        // [STANDARD] -> [ALL, STANDARD]
        assertEquals(newHashSet(CustomFieldTypeCategory.ALL, CustomFieldTypeCategory.STANDARD),
                descriptor.deduceCategories(newHashSet("STANDARD")));

        // [ADVANCED, STANDARD] -> [ALL, STANDARD, ADVANCED]
        assertEquals(newHashSet(CustomFieldTypeCategory.ALL, CustomFieldTypeCategory.STANDARD, CustomFieldTypeCategory.ADVANCED),
                descriptor.deduceCategories(newHashSet("STANDARD", "ADVANCED")));
    }

    @Test
    public void testRestSerializersFromXML() throws DocumentException, ClassNotFoundException {
        final ContainerAccessor containerAccessor = mock(ContainerAccessor.class);

        RestSerializerImpl restSerializer = new RestSerializerImpl();
        when(plugin.<RestSerializerImpl>loadClass(restSerializerClass1, CustomFieldTypeModuleDescriptorImpl.class)).thenReturn(RestSerializerImpl.class);
        when(plugin.getContainerAccessor()).thenReturn(containerAccessor);
        when(containerAccessor.createBean(RestSerializerImpl.class)).thenReturn(restSerializer);

        basicModuleDescriptor.add(createRestSerializer(version2, restSerializerClass1));
        basicModuleDescriptor.add(createRestSerializer(version3, restSerializerClass1));
        final Map<Integer, CustomFieldRestSerializer> returnedSerializers = initDescriptorAndGetRestSerializers();

        assertThat(returnedSerializers, hasEntry(is(version2), is((CustomFieldRestSerializer) restSerializer)));
        assertThat(returnedSerializers, hasEntry(is(version3), is((CustomFieldRestSerializer) restSerializer)));
    }

    @Test(expected = IllegalStateException.class)
    public void testPluginIsNotContainerManaged() throws ClassNotFoundException {
        final Plugin plugin = mock(Plugin.class);

        // This line is needed to prevent NPE in LegacyModuleFactory#getModuleClass
        when(plugin.<CustomFieldType>loadClass(anyString(), eq((Class<?>) null))).thenReturn(CustomFieldType.class);

        basicModuleDescriptor.add(createRestSerializer(version3, restSerializerClass1));
        descriptor.init(plugin, basicModuleDescriptor);
        descriptor.enabled();
    }

    @Test(expected = PluginParseException.class)
    public void testEnablingWhenClassDoesNotExist() throws ClassNotFoundException {
        basicModuleDescriptor.add(createRestSerializer(version2, restSerializerClass1));
        when(plugin.<CustomFieldType>loadClass(restSerializerClass1, CustomFieldTypeModuleDescriptorImpl.class)).thenThrow(new ClassNotFoundException());

        initDescriptorAndGetRestSerializers();
    }

    @Test
    public void testNoExtraSerializersFromXML() throws DocumentException {
        final Map<Integer, CustomFieldRestSerializer> restSerializers = initDescriptorAndGetRestSerializers();
        assertThat(restSerializers, is(Collections.<Integer, CustomFieldRestSerializer>emptyMap()));
    }

    @Test(expected = PluginParseException.class)
    public void testRestSerializersFromXMLWhenSerializerDoesntHaveVersion() throws DocumentException {
        final Element element = DocumentHelper.createElement(REST_SERIALIZER_ELEMENT_NAME)
                .addAttribute(SERIALIZER_CLASS_ARGUMENT_NAME, restSerializerClass1);
        basicModuleDescriptor.add(element);

        initDescriptorAndGetRestSerializers();
    }

    @Test(expected = PluginParseException.class)
    public void testExtraSerializersFromXMLWhenSerializerDoesntHaveClass() throws DocumentException {
        final Element element = DocumentHelper.createElement(REST_SERIALIZER_ELEMENT_NAME).addAttribute(VERSION_NUMBER_ARGUMENT_NAME, "first");
        basicModuleDescriptor.add(element);

        initDescriptorAndGetRestSerializers();
    }

    @Test(expected = PluginParseException.class)
    public void testExtraRestSerializersFromXMLWhenSerializersHaveCollidingVersions()
            throws PluginParseException, DocumentException {
        basicModuleDescriptor.add(createRestSerializer(version3, restSerializerClass1));
        basicModuleDescriptor.add(createRestSerializer(version3, restSerializerClass2));

        initDescriptorAndGetRestSerializers();
    }

    @Test(expected = PluginParseException.class)
    public void testExtraRestSerializersFromXMLWhenVersionIsNotANumber() throws PluginParseException, DocumentException {
        basicModuleDescriptor.add(createRestSerializer("2.1", restSerializerClass2));

        initDescriptorAndGetRestSerializers();
    }

    @Test(expected = PluginParseException.class)
    public void testExtraRestSerializersFromXMLWhenVersionIsLessThanTwo() throws PluginParseException, DocumentException {
        basicModuleDescriptor.add(createRestSerializer(1, restSerializerClass1));

        initDescriptorAndGetRestSerializers();
    }

    private Map<Integer, CustomFieldRestSerializer> initDescriptorAndGetRestSerializers() {
        descriptor.init(plugin, basicModuleDescriptor);
        descriptor.enabled();
        return descriptor.getRestSerializers();
    }

    private Element createRestSerializer(final Integer versionNumber, final String clazz) {
        return createRestSerializer(versionNumber.toString(), clazz);
    }

    private Element createRestSerializer(final String versionNumber, final String clazz) {
        return DocumentHelper.createElement(REST_SERIALIZER_ELEMENT_NAME)
                .addAttribute(VERSION_NUMBER_ARGUMENT_NAME, versionNumber)
                .addAttribute(SERIALIZER_CLASS_ARGUMENT_NAME, clazz);
    }

    private static class RestSerializerImpl implements CustomFieldRestSerializer {
        @Override
        public JsonData getJsonData(final CustomField field, final Issue issue) {
            throw new UnsupportedOperationException("Not implemented");
        }
    }
}

