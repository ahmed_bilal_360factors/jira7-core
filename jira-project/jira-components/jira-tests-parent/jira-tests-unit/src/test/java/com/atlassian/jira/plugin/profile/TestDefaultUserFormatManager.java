package com.atlassian.jira.plugin.profile;

import com.atlassian.jira.plugin.userformat.DefaultUserFormatManager;
import com.atlassian.jira.plugin.userformat.UserFormats;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Collections;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestDefaultUserFormatManager {
    @Mock
    private UserFormats mockUserFormats;

    @Rule
    public MockitoRule initializeMocksRule = MockitoJUnit.rule();

    @Test
    public void formatUserShouldFormatAccordingToTheUserFormatForTheSpecifiedType() {
        final String expectedFormatOutputForUser = "<profile>john</profile>";
        final UserFormat userFormatForSpecifiedType = mock(UserFormat.class);

        when(userFormatForSpecifiedType.format("john", "someid")).thenReturn(expectedFormatOutputForUser);
        when(mockUserFormats.forType("specified-type")).thenReturn(userFormatForSpecifiedType);


        final DefaultUserFormatManager userFormatManager = new DefaultUserFormatManager(mockUserFormats);
        final String actualFormatOutputForUser = userFormatManager.formatUser("john", "specified-type", "someid");

        assertEquals(actualFormatOutputForUser, expectedFormatOutputForUser);
    }

    @Test
    public void formatUserShouldReturnNullWhenTheSpecifiedTypeHasNoUserFormat() {
        when(mockUserFormats.forType("invalidType")).thenReturn(null);

        DefaultUserFormatManager userFormatManager = new DefaultUserFormatManager(mockUserFormats);
        final String formattedUser = userFormatManager.formatUser("john", "invalidType", "someid");

        assertNull(formattedUser);
    }

    @Test
    public void formatUserWithParametersShouldFormatAccordingToTheUserFormatForTheSpecifiedType() throws Exception {
        final Map<String, Object> parametersMap = Collections.emptyMap();
        final String expectedFormatOutputForUser = "<profile>john</profile>";
        final UserFormat userFormatForSpecifiedType = mock(UserFormat.class);

        when(userFormatForSpecifiedType.format("john", "someid", parametersMap)).thenReturn(expectedFormatOutputForUser);
        when(mockUserFormats.forType("specified-type")).thenReturn(userFormatForSpecifiedType);


        final DefaultUserFormatManager userFormatManager = new DefaultUserFormatManager(mockUserFormats);
        final String actualFormatOutputForUser =
                userFormatManager.formatUser("john", "specified-type", "someid", parametersMap);

        assertEquals(actualFormatOutputForUser, expectedFormatOutputForUser);
    }

    @Test
    public void formatUserWithParametersShouldReturnNullWhenTheSpecifiedTypeHasNoUserFormat() throws Exception {
        final Map<String, Object> parametersMap = Collections.emptyMap();
        when(mockUserFormats.forType("invalidType")).thenReturn(null);

        DefaultUserFormatManager userFormatManager = new DefaultUserFormatManager(mockUserFormats);
        final String formattedUser = userFormatManager.formatUser("john", "invalidType", "someid", parametersMap);

        assertNull(formattedUser);
    }
}
