package com.atlassian.jira.plugin.icon;

import com.atlassian.jira.avatar.Avatar;
import com.atlassian.jira.bc.project.ProjectService;
import com.atlassian.jira.icon.IconOwningObjectId;
import com.atlassian.jira.icon.IconType;
import com.atlassian.jira.mock.MockAvatar;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static com.atlassian.jira.permission.GlobalPermissionKey.ADMINISTER;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestIssueTypeIconTypePolicy {
    @Mock
    private GlobalPermissionManager globalPermissionManager;
    @Mock
    private PermissionManager permissionManager;
    @Mock
    private ProjectService projectService;

    @InjectMocks
    private IssueTypeIconTypePolicy testObj;

    @Test
    public void testUserCanView() throws Exception {
        ApplicationUser user = mock(ApplicationUser.class);
        Avatar systemAvatar = new MockAvatar(123L, "abc.gif", "text/plain", IconType.ISSUE_TYPE_ICON_TYPE, null, true);
        Avatar userAvatar = new MockAvatar(123L, "abc.gif", "text/plain", IconType.ISSUE_TYPE_ICON_TYPE, "abc", false);

        boolean result = testObj.userCanView(user, systemAvatar);
        assertTrue(result);
        result = testObj.userCanView(user, userAvatar);
        assertTrue(result);
    }

    @Test
    public void testUserCanDeleteAdmin() throws Exception {
        reset(globalPermissionManager);

        ApplicationUser user = mock(ApplicationUser.class);
        when(globalPermissionManager.hasPermission(ADMINISTER, user)).thenReturn(true);
        Avatar avatar = new MockAvatar(123L, "abc.gif", "text/plain", IconType.ISSUE_TYPE_ICON_TYPE, "abc", false);

        boolean result = testObj.userCanDelete(user, avatar);
        assertTrue("Admin user could not delete", result);

    }

    @Test
    public void testUserCanDeleteNonAdmin() throws Exception {
        reset(globalPermissionManager);

        ApplicationUser user = mock(ApplicationUser.class);
        when(globalPermissionManager.hasPermission(ADMINISTER, user)).thenReturn(false);
        Avatar avatar = new MockAvatar(123L, "abc.gif", "text/plain", IconType.ISSUE_TYPE_ICON_TYPE, "abc", false);

        boolean result = testObj.userCanDelete(user, avatar);
        assertFalse("Non admin user could delete", result);
    }

    @Test
    public void testUserCanCreateForAdmin() throws Exception {
        reset(globalPermissionManager);

        ApplicationUser user = mock(ApplicationUser.class);
        when(globalPermissionManager.hasPermission(ADMINISTER, user)).thenReturn(true);
        Avatar avatar = new MockAvatar(123L, "abc.gif", "text/plain", IconType.ISSUE_TYPE_ICON_TYPE, "abc", false);

        boolean result = testObj.userCanCreateFor(user, new IconOwningObjectId("hello"));
        assertTrue("Admin user could not create", result);
    }

    @Test
    public void testUserCanCreateForNonAdmin() throws Exception {
        reset(globalPermissionManager);

        ApplicationUser user = mock(ApplicationUser.class);
        when(globalPermissionManager.hasPermission(ADMINISTER, user)).thenReturn(false);
        Avatar avatar = new MockAvatar(123L, "abc.gif", "text/plain", IconType.ISSUE_TYPE_ICON_TYPE, "abc", false);

        boolean result = testObj.userCanCreateFor(user, new IconOwningObjectId("hello"));
        assertFalse("Non admin user could create", result);
    }
}