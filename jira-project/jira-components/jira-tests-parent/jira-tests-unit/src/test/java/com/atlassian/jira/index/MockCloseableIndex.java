package com.atlassian.jira.index;

import javax.annotation.Nonnull;
import java.io.IOException;

public class MockCloseableIndex implements CloseableIndex {
    @Nonnull
    public Result perform(@Nonnull final Operation op) {
        try {
            //noinspection ConstantConditions
            op.perform(null);
        } catch (final IOException e) {
            ///CLOVER:OFF
            throw new RuntimeException(e);
            ///CLOVER:ON
        }
        return new MockResult();
    }

    public void close() {
    }
}
