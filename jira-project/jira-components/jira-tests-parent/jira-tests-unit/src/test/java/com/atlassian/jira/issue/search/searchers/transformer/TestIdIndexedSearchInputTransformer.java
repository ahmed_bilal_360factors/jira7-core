package com.atlassian.jira.issue.search.searchers.transformer;

import com.atlassian.core.util.map.EasyMap;
import com.atlassian.jira.issue.search.ClauseNames;
import com.atlassian.jira.issue.search.SearchContext;
import com.atlassian.jira.issue.search.searchers.util.IndexedInputHelper;
import com.atlassian.jira.issue.transport.impl.ActionParamsImpl;
import com.atlassian.jira.issue.transport.impl.FieldValuesHolderImpl;
import com.atlassian.jira.jql.operand.JqlOperandResolver;
import com.atlassian.jira.jql.operand.OperandHandler;
import com.atlassian.jira.jql.resolver.IndexInfoResolver;
import com.atlassian.jira.jql.validator.MockJqlOperandResolver;
import com.atlassian.jira.project.version.Version;
import com.atlassian.query.Query;
import com.atlassian.query.QueryImpl;
import com.atlassian.query.clause.AndClause;
import com.atlassian.query.clause.Clause;
import com.atlassian.query.clause.TerminalClauseImpl;
import com.atlassian.query.operand.FunctionOperand;
import com.atlassian.query.operand.MultiValueOperand;
import com.atlassian.query.operand.Operand;
import com.atlassian.query.operand.SingleValueOperand;
import com.atlassian.query.operator.Operator;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static com.google.common.collect.ImmutableList.of;
import static org.hamcrest.Matchers.hasItem;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestIdIndexedSearchInputTransformer {
    private static final String FIELD_NAME = "field";

    @Rule
    public ExpectedException expectedException = ExpectedException.none();
    @Rule
    public MockitoRule mockito = MockitoJUnit.rule();
    @Mock
    private SearchContext searchContext;
    @Mock
    private IndexInfoResolver mockIndexInfoResolver;
    @Mock
    private FieldFlagOperandRegistry mockFieldFlagOperandRegistry;

    @Test
    public void testPopulateFromParamsOneId() throws Exception {
        final JqlOperandResolver mockJqlOperandResolver = mock(JqlOperandResolver.class);

        final IdIndexedSearchInputTransformer transformer = new MockIdIndexedSearchInputTransformer(new ClauseNames("testId"), mockIndexInfoResolver, mockJqlOperandResolver, mockFieldFlagOperandRegistry);
        final FieldValuesHolderImpl valuesHolder = new FieldValuesHolderImpl();
        final String[] values = {"val1", "val2"};
        final ActionParamsImpl actionParams = new ActionParamsImpl(EasyMap.build("testId", values));
        transformer.populateFromParams(null, valuesHolder, actionParams);
        assertEquals(Arrays.asList(values), valuesHolder.get("testId"));
    }

    @Test
    public void testPopulateFromSearchRequest() throws Exception {
        final QueryImpl query = new QueryImpl();

        final IndexedInputHelper indexedInputHelper = mock(IndexedInputHelper.class);
        when(indexedInputHelper.getAllNavigatorValuesForMatchingClauses(null, new ClauseNames("testId"), query)).thenReturn(ImmutableSet.of("stuff"));

        final IdIndexedSearchInputTransformer transformer = new IdIndexedSearchInputTransformer(new ClauseNames("testId"), mockIndexInfoResolver, MockJqlOperandResolver.createSimpleSupport(), mockFieldFlagOperandRegistry) {
            @Override
            IndexedInputHelper createIndexedInputHelper() {
                return indexedInputHelper;
            }
        };

        final FieldValuesHolderImpl valuesHolder = new FieldValuesHolderImpl();
        transformer.populateFromQuery(null, valuesHolder, query, searchContext);

        @SuppressWarnings({"unchecked"}) final Collection<String> values = (Collection<String>) valuesHolder.get("testId");
        assertThat(values, hasItem("stuff"));
    }

    @Test
    public void testGetSearchClauseSingleValueStringOneId() throws Exception {
        final FieldValuesHolderImpl valuesHolder = new FieldValuesHolderImpl(ImmutableMap.of("testId", of("123")));

        when(mockFieldFlagOperandRegistry.getOperandForFlag("testId", "123")).thenReturn(null);

        final IdIndexedSearchInputTransformer transformer = new MockIdIndexedSearchInputTransformer(new ClauseNames("testId"), mockIndexInfoResolver, MockJqlOperandResolver.createSimpleSupport(), mockFieldFlagOperandRegistry);
        final Clause clause = transformer.getSearchClause(null, valuesHolder);
        final TerminalClauseImpl expectedClause = new TerminalClauseImpl("testId", Operator.EQUALS, 123L);

        assertEquals(expectedClause, clause);
    }

    @Test
    public void testGetSearchClauseSingleValueLongOneId() throws Exception {
        final FieldValuesHolderImpl valuesHolder = new FieldValuesHolderImpl(ImmutableMap.of("testId", of(123L)));


        final IdIndexedSearchInputTransformer transformer = new MockIdIndexedSearchInputTransformer(new ClauseNames("testId"), mockIndexInfoResolver, MockJqlOperandResolver.createSimpleSupport(), mockFieldFlagOperandRegistry);
        expectedException.expect(IllegalArgumentException.class);
        transformer.getSearchClause(null, valuesHolder);
    }

    @Test
    public void testGetSearchClauseMultiValueStringOneId() throws Exception {
        final FieldValuesHolderImpl valuesHolder = new FieldValuesHolderImpl(ImmutableMap.of("testId", of("123", "456")));

        when(mockFieldFlagOperandRegistry.getOperandForFlag("testId", "123")).thenReturn(null);
        when(mockFieldFlagOperandRegistry.getOperandForFlag("testId", "456")).thenReturn(null);

        final IdIndexedSearchInputTransformer transformer = new MockIdIndexedSearchInputTransformer(new ClauseNames("testId"), mockIndexInfoResolver, MockJqlOperandResolver.createSimpleSupport(), mockFieldFlagOperandRegistry);
        final Clause clause = transformer.getSearchClause(null, valuesHolder);
        final TerminalClauseImpl expectedClause = new TerminalClauseImpl("testId", Operator.IN, new MultiValueOperand(123L, 456L));

        assertEquals(expectedClause, clause);

    }

    @Test
    public void testGetSearchClauseNoValuesOneId() throws Exception {
        final FieldValuesHolderImpl valuesHolder = new FieldValuesHolderImpl(ImmutableMap.of("testId", of()));


        final IdIndexedSearchInputTransformer transformer = new MockIdIndexedSearchInputTransformer(new ClauseNames("testId"), mockIndexInfoResolver, MockJqlOperandResolver.createSimpleSupport(), mockFieldFlagOperandRegistry);
        final Clause clause = transformer.getSearchClause(null, valuesHolder);

        assertNull(clause);
    }

    @Test
    public void testPopulateFromParamsTwoIds() throws Exception {
        final JqlOperandResolver mockJqlOperandResolver = mock(JqlOperandResolver.class);

        final IdIndexedSearchInputTransformer transformer = new MockIdIndexedSearchInputTransformer(new ClauseNames("testFieldName"), "testSearcherId", mockIndexInfoResolver, mockJqlOperandResolver, mockFieldFlagOperandRegistry);
        final FieldValuesHolderImpl valuesHolder = new FieldValuesHolderImpl();
        final String[] values = {"val1", "val2"};
        final ActionParamsImpl actionParams = new ActionParamsImpl(EasyMap.build("testSearcherId", values));
        transformer.populateFromParams(null, valuesHolder, actionParams);
        assertEquals(Arrays.asList(values), valuesHolder.get("testSearcherId"));
    }

    @Test
    public void testGetSearchClauseSingleValueStringTwoIds() throws Exception {
        final FieldValuesHolderImpl valuesHolder = new FieldValuesHolderImpl(ImmutableMap.of("testSearcherId", of("123")));

        when(mockFieldFlagOperandRegistry.getOperandForFlag("testFieldName", "123")).thenReturn(null);

        final IdIndexedSearchInputTransformer transformer = new MockIdIndexedSearchInputTransformer(new ClauseNames("testFieldName"), "testSearcherId", mockIndexInfoResolver, MockJqlOperandResolver.createSimpleSupport(), mockFieldFlagOperandRegistry);
        final Clause clause = transformer.getSearchClause(null, valuesHolder);
        final TerminalClauseImpl expectedClause = new TerminalClauseImpl("testFieldName", Operator.EQUALS, 123L);

        assertEquals(expectedClause, clause);
    }

    @Test
    public void testGetSearchClauseSingleValueLongTwoIds() throws Exception {
        final FieldValuesHolderImpl valuesHolder = new FieldValuesHolderImpl(ImmutableMap.of("testSearcherId", of(123L)));

        final IdIndexedSearchInputTransformer transformer = new MockIdIndexedSearchInputTransformer(new ClauseNames("testFieldName"), "testSearcherId", mockIndexInfoResolver, MockJqlOperandResolver.createSimpleSupport(), mockFieldFlagOperandRegistry);

        expectedException.expect(IllegalArgumentException.class);
        transformer.getSearchClause(null, valuesHolder);
    }

    @Test
    public void testGetSearchClauseMultiValueStringTwoIds() throws Exception {
        final FieldValuesHolderImpl valuesHolder = new FieldValuesHolderImpl(ImmutableMap.of("testSearcherId", of("123", "456")));

        when(mockFieldFlagOperandRegistry.getOperandForFlag("testFieldName", "123")).thenReturn(null);
        when(mockFieldFlagOperandRegistry.getOperandForFlag("testFieldName", "456")).thenReturn(null);

        final IdIndexedSearchInputTransformer transformer = new MockIdIndexedSearchInputTransformer(new ClauseNames("testFieldName"), "testSearcherId", mockIndexInfoResolver, MockJqlOperandResolver.createSimpleSupport(), mockFieldFlagOperandRegistry);
        final Clause clause = transformer.getSearchClause(null, valuesHolder);
        final TerminalClauseImpl expectedClause = new TerminalClauseImpl("testFieldName", Operator.IN, new MultiValueOperand(123L, 456L));

        assertEquals(expectedClause, clause);
    }

    @Test
    public void testGetSearchClauseNoValuesTwoIds() throws Exception {
        final FieldValuesHolderImpl valuesHolder = new FieldValuesHolderImpl(ImmutableMap.of("testSearcherId", of()));


        final IdIndexedSearchInputTransformer transformer = new MockIdIndexedSearchInputTransformer(new ClauseNames("testFieldName"), "testSearcherId", mockIndexInfoResolver, MockJqlOperandResolver.createSimpleSupport(), mockFieldFlagOperandRegistry);
        final Clause clause = transformer.getSearchClause(null, valuesHolder);

        assertNull(clause);
    }

    @Test
    public void testValidateForNavigatorHappyPath() throws Exception {
        AndClause andClause = new AndClause(new TerminalClauseImpl(FIELD_NAME, Operator.EQUALS, "value"),
                new TerminalClauseImpl("other", Operator.EQUALS, "valueother"));

        IdIndexedSearchInputTransformer<Version> transformer = new MockIdIndexedSearchInputTransformer<Version>(new ClauseNames(FIELD_NAME), mockIndexInfoResolver, mock(JqlOperandResolver.class), mockFieldFlagOperandRegistry) {
            @Override
            NavigatorStructureChecker<Version> createNavigatorStructureChecker() {
                return new NavigatorStructureChecker<Version>(new ClauseNames(FIELD_NAME), true, fieldFlagOperandRegistry, operandResolver) {
                    @Override
                    public boolean checkSearchRequest(final Query query) {
                        return true;
                    }
                };
            }
        };

        assertValidate(andClause, transformer, true);
    }

    @Test
    public void testValidateForNavigatorSadPath() throws Exception {
        AndClause andClause = new AndClause(new TerminalClauseImpl(FIELD_NAME, Operator.EQUALS, "value"),
                new TerminalClauseImpl("other", Operator.EQUALS, "valueother"));

        IdIndexedSearchInputTransformer transformer = new MockIdIndexedSearchInputTransformer<Version>(new ClauseNames(FIELD_NAME), mockIndexInfoResolver, mock(JqlOperandResolver.class), mockFieldFlagOperandRegistry) {
            @Override
            NavigatorStructureChecker<Version> createNavigatorStructureChecker() {
                return new NavigatorStructureChecker<Version>(new ClauseNames(FIELD_NAME), true, fieldFlagOperandRegistry, operandResolver) {
                    @Override
                    public boolean checkSearchRequest(final Query query) {
                        return false;
                    }
                };
            }
        };

        assertValidate(andClause, transformer, false);
    }

    @Test
    public void testGetSearchClauseFlagMultiValues() throws Exception {
        final FieldValuesHolderImpl valuesHolder = new FieldValuesHolderImpl(ImmutableMap.of("testdid", of("123", "456")));

        FunctionOperand functionOperand = new FunctionOperand("function");

        when(mockFieldFlagOperandRegistry.getOperandForFlag("testdid", "123")).thenReturn(null);
        when(mockFieldFlagOperandRegistry.getOperandForFlag("testdid", "456")).thenReturn(functionOperand);

        final OperandHandler handler = mock(OperandHandler.class);
        when(handler.isList()).thenReturn(true);

        final JqlOperandResolver jqlOperandResolver = MockJqlOperandResolver.createSimpleSupport().addHandler("function", handler);

        final IdIndexedSearchInputTransformer transformer = new MockIdIndexedSearchInputTransformer(new ClauseNames("testdid"), mockIndexInfoResolver, jqlOperandResolver, mockFieldFlagOperandRegistry);
        final Clause clause = transformer.getSearchClause(null, valuesHolder);

        final List<Operand> operands = new ArrayList<Operand>();
        operands.add(new SingleValueOperand(123L));
        operands.add(new FunctionOperand("function"));
        final TerminalClauseImpl expectedClause = new TerminalClauseImpl("testdid", Operator.IN, new MultiValueOperand(operands));

        assertEquals(expectedClause, clause);
    }

    @Test
    public void testGetSearchClauseFlagOneOnlyWithList() throws Exception {
        final FieldValuesHolderImpl valuesHolder = new FieldValuesHolderImpl(ImmutableMap.of("testdid", of("456")));
        FunctionOperand functionOperand = new FunctionOperand("function");

        when(mockFieldFlagOperandRegistry.getOperandForFlag("testdid", "456")).thenReturn(functionOperand);

        final OperandHandler handler = mock(OperandHandler.class);
        when(handler.isList()).thenReturn(true);

        final JqlOperandResolver jqlOperandResolver = MockJqlOperandResolver.createSimpleSupport().addHandler("function", handler);

        final IdIndexedSearchInputTransformer transformer = new MockIdIndexedSearchInputTransformer(new ClauseNames("testdid"), mockIndexInfoResolver, jqlOperandResolver, mockFieldFlagOperandRegistry);
        final Clause clause = transformer.getSearchClause(null, valuesHolder);

        final TerminalClauseImpl expectedClause = new TerminalClauseImpl("testdid", Operator.IN, functionOperand);

        assertEquals(expectedClause, clause);
    }

    @Test
    public void testGetSearchClauseFlagOneOnlyWithNotList() throws Exception {
        final FieldValuesHolderImpl valuesHolder = new FieldValuesHolderImpl(ImmutableMap.of("testdid", of("456")));


        FunctionOperand functionOperand = new FunctionOperand("function");

        when(mockFieldFlagOperandRegistry.getOperandForFlag("testdid", "456")).thenReturn(functionOperand);

        final OperandHandler handler = mock(OperandHandler.class);
        when(handler.isList()).thenReturn(false);

        final JqlOperandResolver jqlOperandResolver = MockJqlOperandResolver.createSimpleSupport().addHandler("function", handler);

        final IdIndexedSearchInputTransformer transformer = new MockIdIndexedSearchInputTransformer(new ClauseNames("testdid"), mockIndexInfoResolver, jqlOperandResolver, mockFieldFlagOperandRegistry);
        final Clause clause = transformer.getSearchClause(null, valuesHolder);

        final TerminalClauseImpl expectedClause = new TerminalClauseImpl("testdid", Operator.EQUALS, functionOperand);

        assertEquals(expectedClause, clause);
    }

    @Test
    public void testGetValuesFromHolderThrowsException() throws Exception {
        final FieldValuesHolderImpl valuesHolder = new FieldValuesHolderImpl(ImmutableMap.of("testdid", of(1L)));

        final IdIndexedSearchInputTransformer transformer = new MockIdIndexedSearchInputTransformer(new ClauseNames("testdid"), mockIndexInfoResolver, MockJqlOperandResolver.createSimpleSupport(), mockFieldFlagOperandRegistry);

        expectedException.expect(IllegalArgumentException.class);
        transformer.getValuesFromHolder(valuesHolder);
    }

    private void assertValidate(final AndClause andClause, final IdIndexedSearchInputTransformer transformer, final boolean isClauseValid) {
        final QueryImpl query = new QueryImpl(andClause);
        assertEquals(isClauseValid, transformer.doRelevantClausesFitFilterForm(null, query, searchContext));
    }

    private static class MockIdIndexedSearchInputTransformer<T> extends IdIndexedSearchInputTransformer<T> {
        private MockIdIndexedSearchInputTransformer(ClauseNames id, IndexInfoResolver<T> indexInfoResolver, JqlOperandResolver operandResolver, FieldFlagOperandRegistry fieldFlagOperandRegistry) {
            super(id, indexInfoResolver, operandResolver, fieldFlagOperandRegistry);
        }

        private MockIdIndexedSearchInputTransformer(ClauseNames clauseNames, String urlParameterName, IndexInfoResolver<T> indexInfoResolver, JqlOperandResolver operandResolver, FieldFlagOperandRegistry fieldFlagOperandRegistry) {
            super(clauseNames, urlParameterName, indexInfoResolver, operandResolver, fieldFlagOperandRegistry);
        }

        IndexedInputHelper createIndexedInputHelper() {
            return getDefaultIndexedInputHelper();
        }
    }
}
