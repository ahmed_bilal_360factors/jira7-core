package com.atlassian.jira.startup;


import com.atlassian.jira.startup.JiraStartupPluginSystemListener.DoneTracker;
import com.atlassian.jira.startup.JiraStartupPluginSystemListener.StartupTracker;
import com.atlassian.jira.startup.JiraStartupPluginSystemListener.Tracker;
import com.google.common.collect.ImmutableList;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Supplier;

import static java.util.Arrays.asList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.lessThan;
import static org.hamcrest.Matchers.lessThanOrEqualTo;
import static org.junit.Assert.assertEquals;

public class JiraStartupPluginSystemListenerTest {
    private static final long EPOCH = 123456L;
    private static final List<Integer> TEST_BUDGETS = ImmutableList.of(0, 42, 100);
    private static final List<Integer> TEST_WEIGHTS = ImmutableList.of(30, 100, 1000);

    @Test
    public void testDoneTracker() {
        final Tracker tracker = new DoneTracker();
        assertProgress(tracker, 0, 42, 100);

        tracker.expect(42);
        tracker.accept(1234);
        assertProgress(tracker, 0, 42, 100);
    }

    private static void assertProgress(Tracker tracker, Integer... expected) {
        final ImmutableList.Builder<Integer> actual = ImmutableList.builder();
        for (int budget : TEST_BUDGETS) {
            actual.add(tracker.getProgress(budget));
        }
        assertEquals("Expected results for budgets of (0, 42, 100)", asList(expected), actual.build());
    }

    @Test
    public void testProgressWithoutFudge() {
        assertProgressMatters(0);
    }

    @Test
    public void testProgressWithSomeFudge() {
        assertProgressMatters(30);
    }

    @Test
    public void testProgressWithLotsOfFudge() {
        assertProgressMatters(60);
    }

    @Test
    public void testProgressWithMoreFudgeThanYouCanEvenImagine() {
        assertProgressMatters(120);
    }

    private static void assertProgressMatters(int fudge) {
        final Supplier<Fixture> supplier = () -> new Fixture().seconds(fudge);
        assertWeightSteps(supplier);
        assertIncrease("50% progress should make a diff", supplier, fixture -> fixture.pct(50));
        assertEquals("Should reach 100%", 100, supplier.get().pct(100).getProgress(100));
    }

    @Test
    public void testFudgeWithoutProgress() {
        assertFudgeMatters(() -> new Fixture().pct(0));
    }

    @Test
    public void testFudgeWithSomeProgress() {
        assertFudgeMatters(() -> new Fixture().pct(30));
    }

    @Test
    public void testFudgeWithLotsOfProgress() {
        assertFudgeDoesNotMatter(() -> new Fixture().pct(100));
    }

    @Test
    public void testFudgeWithMoreProgressThanWasEverExpected() {
        assertFudgeDoesNotMatter(() -> new Fixture().pct(200));
    }

    @Test
    public void testOffsetAndScale() {
        final Fixture fixture = new Fixture(0.25, 0.5);
        assertThat(fixture.getProgress(80), is(20));
        fixture.expect(100);
        assertThat(fixture.getProgress(80), is(20));
        fixture.accept(50);
        assertThat(fixture.getProgress(80), is(40));
        fixture.accept(50);
        assertThat(fixture.getProgress(80), is(60));
    }

    private static void assertFudgeMatters(Supplier<Fixture> supplier) {
        assertFudgeSteps(supplier);
        assertIncrease("45 seconds should make a diff", supplier, fixture -> fixture.seconds(45));
        assertThat("Should not reach 100%", supplier.get().seconds(60).getProgress(100), lessThan(100));
    }

    private static void assertFudgeDoesNotMatter(Supplier<Fixture> supplier) {
        assertFudgeSteps(supplier);
        assertEquals("Should already be at 100%", 100, supplier.get().getProgress(100));
        assertEquals("Fudge makes no diff to 100%", 100, supplier.get().seconds(120).getProgress(100));
    }

    private static void assertFudgeSteps(Supplier<Fixture> supplier) {
        final Fixture tracker = supplier.get().seconds(0);

        List<Integer> prevProgress = getProgress(tracker);
        prevProgress = assertFudgeSteps(tracker, 15, prevProgress);
        prevProgress = assertFudgeSteps(tracker, 30, prevProgress);
        prevProgress = assertFudgeSteps(tracker, 45, prevProgress);
        prevProgress = assertFudgeSteps(tracker, 60, prevProgress);

        tracker.seconds(120);
        final List<Integer> lastProgress = getProgress(tracker);
        assertEquals("Should be no further change past the maximum amount of fudge", prevProgress, lastProgress);
    }

    private static List<Integer> assertFudgeSteps(Fixture tracker, int seconds, List<Integer> prevProgress) {
        tracker.seconds(seconds);
        return assertNondecreasing(tracker, prevProgress);
    }

    private static void assertIncrease(String message, Supplier<Fixture> supplier, Consumer<Fixture> mutator) {
        final Fixture tracker = supplier.get();
        final int before = tracker.getProgress(100);
        mutator.accept(tracker);
        final int after = tracker.getProgress(100);
        assertThat(message, after, greaterThan(before));
    }

    private static void assertWeightSteps(Supplier<Fixture> supplier) {
        TEST_WEIGHTS.forEach(weightLimit -> assertWeightSteps(supplier.get(), weightLimit));
    }

    private static void assertWeightSteps(Tracker tracker, int weightLimit) {
        TEST_BUDGETS.forEach(budget -> assertThat(
                "All progresses should be 0 before we have any expectations",
                tracker.getProgress(budget), is(0)));

        tracker.expect(weightLimit);

        List<Integer> prevProgress = getProgress(tracker);
        for (int total = 0; total < weightLimit; total += 10) {
            prevProgress = assertWeightStep(tracker, prevProgress, 10);
        }

        prevProgress = assertWeightStep(tracker, prevProgress, 10);
        assertEquals("Once we hit the limit, we should be at the full budget", TEST_BUDGETS, prevProgress);

        prevProgress = assertWeightStep(tracker, prevProgress, weightLimit);
        assertEquals("Once we hit the limit, there should be no further progress", TEST_BUDGETS, prevProgress);
    }

    private static List<Integer> assertWeightStep(Tracker tracker, List<Integer> prevProgress, int weightStep) {
        tracker.accept(weightStep);
        return assertNondecreasing(tracker, prevProgress);
    }

    private static List<Integer> assertNondecreasing(Tracker tracker, List<Integer> prevProgress) {
        return assertInvariant(tracker, prevProgress, (p, n) -> p.compareTo(n) <= 0);
    }

    private static List<Integer> assertInvariant(Tracker tracker, List<Integer> prevProgress,
                                                 BiFunction<Integer, Integer, Boolean> assertion) {
        final List<Integer> progress = getProgress(tracker);
        for (int i = 0; i < TEST_BUDGETS.size(); ++i) {
            assertThat(assertion.apply(prevProgress.get(i), progress.get(i)), is(true));
        }
        return progress;
    }

    private static List<Integer> getProgress(Tracker tracker) {
        final List<Integer> progress = new ArrayList<>(TEST_BUDGETS.size());
        for (int budget : TEST_BUDGETS) {
            final int value = tracker.getProgress(budget);
            assertThat("Progress value cannot be negative", value, greaterThanOrEqualTo(0));
            assertThat("Progress value cannot exceed the budget", value, lessThanOrEqualTo(budget));
            progress.add(value);
        }
        return progress;
    }


    static class Fixture extends StartupTracker {
        private final AtomicLong now = new AtomicLong(EPOCH);

        Fixture() {
            this(0.0, 1.0);
        }

        Fixture(double offset, double scale) {
            super(offset, scale);
        }

        Fixture seconds(int seconds) {
            now.set(EPOCH + TimeUnit.SECONDS.toNanos(seconds));
            return this;
        }

        Fixture pct(int pct) {
            expect(100);
            accept(pct);
            return this;
        }

        @Override
        protected long now() {
            // Null check needed because this gets called by StartupTracker's constructor, which runs before ours does
            return (now != null) ? now.get() : EPOCH;
        }

        @Override
        protected int timeout() {
            return 60;
        }
    }
}