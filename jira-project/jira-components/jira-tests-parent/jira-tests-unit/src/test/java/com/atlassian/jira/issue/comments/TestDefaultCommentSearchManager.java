package com.atlassian.jira.issue.comments;

import com.atlassian.jira.bc.admin.ApplicationPropertiesService;
import com.atlassian.jira.bc.admin.ApplicationProperty;
import com.atlassian.jira.config.CoreFeatures;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.issue.ActionConstants;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.IssueRelationConstants;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.mock.issue.MockIssue;
import com.atlassian.jira.mock.ofbiz.MockOfBizDelegator;
import com.atlassian.jira.ofbiz.FieldMap;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.security.roles.ProjectRoleManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.ofbiz.core.entity.EntityCondition;
import org.ofbiz.core.entity.EntityConditionList;
import org.ofbiz.core.entity.EntityExpr;
import org.ofbiz.core.entity.EntityOperator;
import org.ofbiz.core.entity.GenericEntityException;
import org.ofbiz.core.entity.GenericValue;
import org.springframework.test.util.ReflectionTestUtils;

import java.sql.Timestamp;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestDefaultCommentSearchManager {
    private static final Long L1 = 1L;
    private static final Long L1000 = 1000L;
    private static final Long L1001 = 1001L;
    private static final int TOTAL_COMMENTS_CREATED = 10;
    private static final int DEFAULT_COMMENTS_TO_SHOW = 5;

    private Issue issueObject;
    private ApplicationUser author;
    private Timestamp timestamp;

    @Mock
    @AvailableInContainer
    private UserManager mockUserManager;
    @AvailableInContainer
    OfBizDelegator mockDelegator = new MockOfBizDelegator();
    @Mock
    private IssueManager mockIssueManager;
    @Mock
    private ProjectRoleManager mockProjectRoleManager;
    @Mock
    private CommentPermissionManager mockCommentPermissionManager;
    @Mock
    private FeatureManager mockFeatureManager;
    @Mock
    private ApplicationPropertiesService mockApplicationPropertiesService;
    @Mock
    private StreamingCommentsRetriever streamingCommentsRetriever;

    private CommentSearchManager commentSearchManager;

    @Rule
    public RuleChain mocksInContainer = MockitoMocksInContainer.forTest(this);
    private GenericValue commentGv;

    @Before
    public void setUp() throws Exception {

        author = new MockApplicationUser("Owen");

        final GenericValue project = mockDelegator.createValue("Project", FieldMap.build("name", "test project"));

        timestamp = new Timestamp(System.currentTimeMillis());
        final String key = "TST-1";
        final GenericValue issue = mockDelegator.createValue("Issue", FieldMap.build("id", L1, "project", project.getLong("id"), "key",
                key, "updated", timestamp));
        issueObject = new MockIssue(issue);
        when(mockIssueManager.getIssueObject(anyLong())).thenReturn((MockIssue) issueObject);


        commentGv = mockDelegator.createValue("Action", FieldMap.build("id", L1000, "issue", L1, "body", "a comment", "type",
                ActionConstants.TYPE_COMMENT).add("level", "Group A").add("created", timestamp));
        when(mockIssueManager.getEntitiesByIssueObject(IssueRelationConstants.COMMENTS, issueObject)).thenReturn(Collections.singletonList(commentGv));

        commentSearchManager = new CommentSearchManager(mockUserManager, mockDelegator,
                mockIssueManager, mockProjectRoleManager, mockCommentPermissionManager, mockFeatureManager, mockApplicationPropertiesService, streamingCommentsRetriever);
    }

    @Test
    public void testGetMutableComment() throws Exception {
        when(mockCommentPermissionManager.hasBrowsePermission(eq(author), any(Comment.class))).thenReturn(true);

        final Comment comment = commentSearchManager.getMutableComment(L1000);
        assertNotNull(comment);
        assertEquals(L1000, comment.getId());

        try {
            commentSearchManager.getMutableComment(null);
            fail("Null comment id should throw IllegalArgumentException");
        } catch (final IllegalArgumentException e) {
            // expected
        }

    }

    @Test
    public void testActionsOneComment() throws Exception {
        when(mockCommentPermissionManager.hasBrowsePermission(eq(author), any(Comment.class))).thenReturn(true);

        List<Comment> comments = commentSearchManager.getCommentsForUser(issueObject, null);
        assertNotNull(comments);
        assertTrue(comments.isEmpty());
        comments = commentSearchManager.getCommentsForUser(issueObject, author);
        assertNotNull(comments);
        assertEquals(1, comments.size());
        assertEquals(L1000, comments.get(0).getId());
    }

    @Test
    public void testActionsMultipleComments() throws Exception {

        // Setup another comment
        final Timestamp anotherTimestamp = new Timestamp(timestamp.getTime() + 1);
        GenericValue commentGv2 = mockDelegator.createValue("Action", FieldMap.build("id", L1001, "issue", L1, "body", "the body of the comment", "type",
                ActionConstants.TYPE_COMMENT).add("level", "Group A").add("created", anotherTimestamp));
        when(mockIssueManager.getEntitiesByIssueObject(IssueRelationConstants.COMMENTS, issueObject)).thenReturn(Lists.newArrayList(commentGv, commentGv2));
        when(mockCommentPermissionManager.hasBrowsePermission(eq(author), any(Comment.class))).thenReturn(true);

        List<Comment> comments = commentSearchManager.getCommentsForUser(issueObject, null);
        assertNotNull(comments);
        assertTrue(comments.isEmpty());
        comments = commentSearchManager.getCommentsForUser(issueObject, author);
        assertNotNull(comments);
        assertEquals(2, comments.size());
        assertEquals(L1000, comments.get(0).getId());
        assertEquals(L1001, comments.get(1).getId());
    }

    @Test
    public void testCommentsNotInGroup() throws GenericEntityException {
        final List<Comment> comments = commentSearchManager.getCommentsForUser(issueObject, author);
        assertNotNull(comments);
        assertTrue(comments.isEmpty());
    }

    @Test
    public void testCommentsGroup() throws Exception {
        when(mockCommentPermissionManager.hasBrowsePermission(eq(author), any(Comment.class))).thenReturn(true);

        final List<Comment> comments = commentSearchManager.getCommentsForUser(issueObject, author);
        assertNotNull(comments);
        assertEquals(1, comments.size());
        assertEquals(L1000, comments.get(0).getId());
    }

    @Test
    public void testGetCommentById() throws Exception {
        when(mockCommentPermissionManager.hasBrowsePermission(eq(author), any(Comment.class))).thenReturn(true);

        final Comment comment = commentSearchManager.getCommentById(L1000);
        assertNotNull(comment);
        assertEquals(L1000, comment.getId());

        try {
            commentSearchManager.getCommentById(null);
            fail("Null comment id should throw IllegalArgumentException");
        } catch (final IllegalArgumentException e) {
            // expected
        }
    }

    @Test
    @SuppressWarnings("unchecked")
    public void getCommentsForUserSinceShouldPassCorrectConditionToDelegator() {
        // Set up
        Date sinceDate = new Date();
        final OfBizDelegator mockOfBizDelegator = mock(OfBizDelegator.class);
        ReflectionTestUtils.setField(commentSearchManager, "delegator", mockOfBizDelegator);

        // Invoke
        commentSearchManager.getCommentsForUserSince(issueObject, author, sinceDate);

        // Check
        final ArgumentCaptor<EntityCondition> entityConditionCaptor = ArgumentCaptor.forClass(EntityCondition.class);
        verify(mockOfBizDelegator).findByCondition(eq(DefaultCommentManager.COMMENT_ENTITY),
                entityConditionCaptor.capture(), isNull(List.class), eq(ImmutableList.of("updated DESC", "id ASC")));
        final EntityCondition entityCondition = entityConditionCaptor.getValue();
        assertEquals(EntityConditionList.class, entityCondition.getClass());
        final EntityConditionList entityConditionList = (EntityConditionList) entityCondition;
        final EntityExpr dateCondition = (EntityExpr) entityConditionList.getCondition(1);
        assertThat(dateCondition.getLhs(), equalTo("updated"));
        assertThat(dateCondition.getOperator(), equalTo(EntityOperator.GREATER_THAN));
        assertThat(((Timestamp) dateCondition.getRhs()).getTime(), equalTo(sinceDate.getTime()));
    }

    @Test
    public void canRetrieveCommentSummaryForIssueWithNoComments() {
        final CommentSummary commentSummary = commentSearchManager.getCommentSummary(author, new MockIssue(99999L), Optional.empty());

        assertThat(commentSummary.getTotal(), is(0));
        assertThat(commentSummary.getComments().size(), is(0));
    }

    @Test
    public void commentSummaryFiltersOutCommentsNotVisibleToUser() {
        when(mockCommentPermissionManager.hasBrowsePermission(eq(author), any(Comment.class))).thenReturn(false);

        final CommentSummary commentSummary = commentSearchManager.getCommentSummary(author, issueObject, Optional.empty());

        assertThat(commentSummary.getTotal(), is(0));
    }

    @Test
    public void commentSummaryIncludesCommentsVisibleToUser() {
        when(mockCommentPermissionManager.hasBrowsePermission(eq(author), any(Comment.class))).thenReturn(true);

        final CommentSummary commentSummary = commentSearchManager.getCommentSummary(author, issueObject, Optional.empty());

        verifyZeroInteractions(mockIssueManager);
        assertThat(commentSummary.getTotal(), is(1));
        final Comment comment = commentSummary.getComments().get(0);
        assertThat(comment.getId(), is(L1000));
        assertThat(comment.getBody(), is("a comment"));
    }

    @Test
    public void commentSummaryLimitsCommentsReturned() {
        createManyComments(TOTAL_COMMENTS_CREATED);

        final CommentSummary commentSummary = commentSearchManager.getCommentSummary(author, issueObject, Optional.empty());

        assertThat(commentSummary.getTotal(), is(TOTAL_COMMENTS_CREATED));
        assertThat(commentSummary.getComments().size(), is(DEFAULT_COMMENTS_TO_SHOW));
    }

    @Test
    public void commentSummaryReturnsAllCommentsWhenLimitingCommentsIsTurnedOff() {
        createManyComments(TOTAL_COMMENTS_CREATED);
        when(mockFeatureManager.isEnabled(CoreFeatures.PREVENT_COMMENTS_LIMITING)).thenReturn(true);

        final CommentSummary commentSummary = commentSearchManager.getCommentSummary(author, issueObject, Optional.empty());

        verifyZeroInteractions(mockIssueManager);
        assertThat(commentSummary.getTotal(), is(TOTAL_COMMENTS_CREATED));
        assertThat(commentSummary.getComments().size(), is(TOTAL_COMMENTS_CREATED));
    }

    @Test
    public void commentSummaryReturnsAllCommentsWhenHiddenCommentsLessThanMinimum() {
        createManyComments(TOTAL_COMMENTS_CREATED);
        when(mockApplicationPropertiesService.getApplicationProperty(APKeys.COMMENT_COLLAPSING_MINIMUM_HIDDEN)).thenReturn(new ApplicationProperty(null, "6"));

        final CommentSummary commentSummary = commentSearchManager.getCommentSummary(author, issueObject, Optional.empty());

        assertThat(commentSummary.getTotal(), is(TOTAL_COMMENTS_CREATED));
        assertThat(commentSummary.getComments().size(), is(TOTAL_COMMENTS_CREATED));
    }

    @Test
    public void allCommentsRetrievedFromIssueEntityManagerIfTooManyToLoadByIdAndLimitingOff() throws GenericEntityException {
        createManyComments(200);
        when(mockFeatureManager.isEnabled(CoreFeatures.PREVENT_COMMENTS_LIMITING)).thenReturn(true);

        final CommentSummary commentSummary = commentSearchManager.getCommentSummary(author, issueObject, Optional.empty());

        verify(mockIssueManager, times(1)).getEntitiesByIssueObject(any(String.class), any(Issue.class));
        //only getting 1 comment back here, because that's all that's mocked out in setup for the issueManager.getEntitiesByIssueObject interaction.
        assertThat(commentSummary.getTotal(), is(1));
        assertThat(commentSummary.getComments().size(), is(1));
    }

    @Test
    public void allCommentsReturnedIfFocusedIdNotPresentInLastPage() throws GenericEntityException {
        createManyComments(TOTAL_COMMENTS_CREATED);

        final CommentSummary commentSummary = commentSearchManager.getCommentSummary(author, issueObject, Optional.of(1212121212L));

        assertThat(commentSummary.getTotal(), is(TOTAL_COMMENTS_CREATED));
        assertThat(commentSummary.getComments().size(), is(TOTAL_COMMENTS_CREATED));
    }

    @Test
    public void shouldExecuteCallbackAndReturnCorrectNumberOfCommentsForAGivenIssue() {
        Issue issue = new MockIssue(1L);
        Stream<Comment> stream = Stream.empty();
        when(streamingCommentsRetriever.stream(author, issue)).thenReturn(stream);

        Stream<Comment> actualStream = commentSearchManager.streamComments(author, issue);

        assertThat(actualStream, is(stream));
    }

    private void createManyComments(int commentsToCreate) {
        //we've already created 1 comment in setup so we subtract one here from the total.
        for (int i = 0; i < (commentsToCreate - 1); i++) {
            mockDelegator.createValue("Action", FieldMap.build("id", 10010L + i, "issue", L1, "body", "a comment " + i, "type", ActionConstants.TYPE_COMMENT).add("created", timestamp));
        }
        when(mockCommentPermissionManager.hasBrowsePermission(eq(author), any(Comment.class))).thenReturn(true);
    }
}
