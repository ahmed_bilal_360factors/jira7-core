package com.atlassian.jira.issue.statistics;

import com.atlassian.jira.issue.customfields.converters.SelectConverter;
import com.atlassian.jira.issue.customfields.searchers.transformer.CustomFieldInputHelper;
import com.atlassian.jira.issue.customfields.statistics.GroupPickerStatisticsMapper;
import com.atlassian.jira.issue.customfields.statistics.SelectStatisticsMapper;
import com.atlassian.jira.issue.customfields.statistics.TextStatisticsMapper;
import com.atlassian.jira.issue.customfields.statistics.UserPickerStatisticsMapper;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.search.ClauseNames;
import com.atlassian.jira.security.JiraAuthenticationContext;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

public class TestUserPickerStatisticsMapper {
    @Rule
    public final MethodRule initMockito = MockitoJUnit.rule();

    @Mock
    CustomField mockCustomField;
    @Mock
    CustomField mockCustomField2;
    @Mock
    CustomField mockCustomField3;
    @Mock
    JiraAuthenticationContext authenticationContext;
    @Mock
    CustomFieldInputHelper customFieldInputHelper;
    @Mock
    SelectConverter mockSelectConverter;

    @Test
    public void testEquals() {
        String cfId = "customfield_10001";
        when(mockCustomField.getId()).thenReturn("customfield_10001");

        UserPickerStatisticsMapper mapper = new UserPickerStatisticsMapper(mockCustomField, null, null);

        // identity test
        assertTrue(mapper.equals(mapper));
        assertEquals(mapper.hashCode(), mapper.hashCode());

        when(mockCustomField2.getId()).thenReturn("customfield_10001");
        UserPickerStatisticsMapper mapper2 = new UserPickerStatisticsMapper(mockCustomField2, null, null);

        // As the mappers are using the same custom field they should be equal
        assertTrue(mapper.equals(mapper2));
        assertEquals(mapper.hashCode(), mapper2.hashCode());

        when(mockCustomField3.getId()).thenReturn("customfield_10002");
        UserPickerStatisticsMapper mapper3 = new UserPickerStatisticsMapper(mockCustomField3, null, null);

        // As the mappers are using different custom field they should *not* be equal
        assertFalse(mapper.equals(mapper3));
        assertFalse(mapper.hashCode() == mapper3.hashCode());

        assertFalse(mapper.equals(null));
        assertFalse(mapper.equals(new Object()));
        assertFalse(mapper.equals(new IssueKeyStatisticsMapper()));
        assertFalse(mapper.equals(new IssueTypeStatisticsMapper(null)));
        // ensure other implmentations of same base class are not equal even though they both extend
        // they inherit the equals and hashCode implementations
        assertFalse(mapper.equals(new TextStatisticsMapper(mockCustomField)));
        assertFalse(mapper.equals(new ProjectStatisticsMapper(null, "clause", cfId)));

        ClauseNames clauseNames = new ClauseNames("clauseName");
        when(mockCustomField.getClauseNames()).thenReturn(clauseNames);
        assertFalse(mapper.equals(new GroupPickerStatisticsMapper(mockCustomField, null, authenticationContext, customFieldInputHelper)));

        assertFalse(mapper.equals(new SelectStatisticsMapper(mockCustomField, mockSelectConverter, authenticationContext, customFieldInputHelper)));
    }
}
