package com.atlassian.jira.bulkedit;

import com.atlassian.jira.issue.IssueFieldConstants;
import com.atlassian.jira.issue.fields.LongIdsValueHolder;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import org.hamcrest.Matchers;
import org.junit.Rule;
import org.junit.Test;

import static com.google.common.collect.ImmutableList.of;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

/**
 * @since v6.4
 */
public class TestBulkEditMultiSelectFieldOptionReplace {
    private static final String LABEL1 = "Label1";
    private static final String LABEL2 = "Label2";
    private static final String LABEL3 = "Label3";
    private static final String VALUE1 = "Value1";
    private static final String VALUE2 = "Value2";
    private static final String VALUE3 = "Value3";

    @Rule
    public BulkEditTestHelper helper = new BulkEditTestHelper() {
        @Override
        public BulkEditMultiSelectFieldOption instantiateFieldOption() {
            return new BulkEditMultiSelectFieldOptionReplace();
        }
    };

    @Test
    public void testGetFieldValuesMapForComponentsNoValuesToAdd() throws Exception {
        helper.mockFieldValuesFromIssue(IssueFieldConstants.COMPONENTS, new LongIdsValueHolder(of(10001L, 10002L)));
        helper.setValuesFromBulkEdit(IssueFieldConstants.COMPONENTS, new LongIdsValueHolder(of(10003L, 10004L)));

        final LongIdsValueHolder valueHolder = helper.getFieldLongIdsFromValuesMap(IssueFieldConstants.COMPONENTS);

        assertThat(valueHolder, Matchers.containsInAnyOrder(10003L, 10004L));
        assertThat(valueHolder.getValuesToAdd(), Matchers.emptyIterable());
    }

    @Test
    public void testGetFieldValuesMapForComponentsWithValuesToAdd() throws Exception {
        ImmutableSet<String> valuesToAdd = ImmutableSet.of("Component1", "Component2");
        LongIdsValueHolder componentsFromBulkEdit = new LongIdsValueHolder(of(10001L, 10003L));
        componentsFromBulkEdit.setValuesToAdd(valuesToAdd);

        helper.mockFieldValuesFromIssue(IssueFieldConstants.COMPONENTS, new LongIdsValueHolder(of(10001L, 10002L)));
        helper.setValuesFromBulkEdit(IssueFieldConstants.COMPONENTS, componentsFromBulkEdit);

        final LongIdsValueHolder valueHolder = helper.getFieldLongIdsFromValuesMap(IssueFieldConstants.COMPONENTS);

        assertThat(valueHolder, Matchers.containsInAnyOrder(10001L, 10003L));
        assertEquals(valuesToAdd, valueHolder.getValuesToAdd());
    }

    @Test
    public void testGetFieldValuesMapForVersionsWithValuesToAdd() throws Exception {
        LongIdsValueHolder versionsFromBulkEdit = new LongIdsValueHolder(of(10002L, 10003L));
        versionsFromBulkEdit.setValuesToAdd(ImmutableSet.of("Version1"));

        helper.mockFieldValuesFromIssue(IssueFieldConstants.FIX_FOR_VERSIONS, new LongIdsValueHolder(of(10001L)));
        helper.setValuesFromBulkEdit(IssueFieldConstants.FIX_FOR_VERSIONS, versionsFromBulkEdit);

        final LongIdsValueHolder valueHolder = helper.getFieldLongIdsFromValuesMap(IssueFieldConstants.FIX_FOR_VERSIONS);

        assertThat(valueHolder, Matchers.containsInAnyOrder(10002L, 10003L));
        assertThat(valueHolder.getValuesToAdd(), Matchers.containsInAnyOrder("Version1"));
    }

    @Test
    public void testGetFieldValuesMapForLabels() throws Exception {
        helper.mockFieldValuesFromIssue(IssueFieldConstants.LABELS, ImmutableSet.of(LABEL1));
        helper.setValuesFromBulkEdit(IssueFieldConstants.LABELS, ImmutableSet.of(LABEL2, LABEL3));

        assertThat(helper.getCollectionFromValuesMap(IssueFieldConstants.LABELS), Matchers.containsInAnyOrder(LABEL2, LABEL3));
    }

    @Test
    public void testValidateOperationForComponentsOnlyNewValueToAdd() throws Exception {
        LongIdsValueHolder componentsFromBulkEdit = new LongIdsValueHolder(ImmutableList.<Long>of());
        componentsFromBulkEdit.setValuesToAdd(ImmutableSet.of(VALUE1));

        helper.setValuesFromBulkEdit(IssueFieldConstants.COMPONENTS, componentsFromBulkEdit);

        assertTrue(helper.validate());
    }

    @Test
    public void testValidateOperationForVersionsOnlyExistingValueToAdd() throws Exception {
        helper.setValuesFromBulkEdit(IssueFieldConstants.AFFECTED_VERSIONS, new LongIdsValueHolder(of(10001L)));

        assertTrue(helper.validate());
    }

    @Test
    public void testValidateOperationForVersionsNoValueToAdd() throws Exception {
        helper.setValuesFromBulkEdit(IssueFieldConstants.FIX_FOR_VERSIONS, new LongIdsValueHolder(ImmutableList.<Long>of()));

        assertFalse(helper.validate());
    }

    @Test
    public void testValidateOperationLabelsNewValueToAdd() throws Exception {
        helper.setValuesFromBulkEdit(IssueFieldConstants.LABELS, ImmutableSet.of(LABEL1));

        assertTrue(helper.validate());
    }

    @Test
    public void testValidateOperationLabelsNoValueToAdd() throws Exception {
        helper.setValuesFromBulkEdit(IssueFieldConstants.LABELS, ImmutableSet.of());

        assertFalse(helper.validate());
    }

    @Test
    public void testGetFieldValuesToAddForComponents() throws Exception {
        LongIdsValueHolder componentsFromBulkEdit = new LongIdsValueHolder(of(10003L, 10004L));
        componentsFromBulkEdit.setValuesToAdd(ImmutableSet.of(VALUE1, VALUE2, VALUE3));

        helper.setValuesFromBulkEdit(IssueFieldConstants.COMPONENTS, componentsFromBulkEdit);

        assertThat(helper.getValuesToAdd(), equalTo(VALUE1 + ", " + VALUE2 + ", " + VALUE3));
    }

    @Test
    public void testGetFieldValuesToAddForVersions() throws Exception {
        LongIdsValueHolder versionsFromBulkEdit = new LongIdsValueHolder(of(10003L, 10004L));
        versionsFromBulkEdit.setValuesToAdd(ImmutableSet.of(VALUE1, VALUE2, VALUE3));

        helper.setValuesFromBulkEdit(IssueFieldConstants.FIX_FOR_VERSIONS, versionsFromBulkEdit);

        assertThat(helper.getValuesToAdd(), equalTo(VALUE1 + ", " + VALUE2 + ", " + VALUE3));
    }

    @Test
    public void testGetFieldValuesToAddForLabels() throws Exception {
        helper.setValuesFromBulkEdit(IssueFieldConstants.LABELS, ImmutableSet.of(LABEL1, LABEL2, LABEL3));

        assertThat(helper.getValuesToAdd(), Matchers.isEmptyString());
    }
}
