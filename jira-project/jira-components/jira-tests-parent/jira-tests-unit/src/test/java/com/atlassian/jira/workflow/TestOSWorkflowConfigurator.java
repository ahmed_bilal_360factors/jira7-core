package com.atlassian.jira.workflow;

import com.atlassian.jira.util.collect.MapBuilder;
import com.opensymphony.workflow.Condition;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.Register;
import com.opensymphony.workflow.TypeResolver;
import com.opensymphony.workflow.Validator;
import com.opensymphony.workflow.Workflow;
import org.junit.Test;

import java.util.Map;

import static com.atlassian.jira.util.collect.MapBuilder.singletonMap;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @since v4.1.1
 */
public class TestOSWorkflowConfigurator {
    private static final String CLASS_NAME = "className";
    private static final String TYPE = "type";

    @Test
    public void testGetConditionDelegatorNoClass() throws Exception {
        final DefaultOSWorkflowConfigurator configurator = new DefaultOSWorkflowConfigurator();
        final DefaultOSWorkflowConfigurator.JiraTypeResolverDelegator delegator = configurator.new JiraTypeResolverDelegator();

        final Condition result = delegator.getCondition(TYPE, MapBuilder.emptyMap());
        assertNull(result);
    }

    @Test
    public void testGetConditionDelegatorNotRegistered() throws Exception {
        final DefaultOSWorkflowConfigurator configurator = new DefaultOSWorkflowConfigurator();
        final DefaultOSWorkflowConfigurator.JiraTypeResolverDelegator delegator = configurator.new JiraTypeResolverDelegator();

        final Condition result = delegator.getCondition(TYPE, MapBuilder.singletonMap(Workflow.CLASS_NAME, CLASS_NAME));
        assertNull(result);
    }

    @Test
    public void testGetConditionDelegatorRegistered() throws Exception {
        final DefaultOSWorkflowConfigurator configurator = new DefaultOSWorkflowConfigurator();
        final DefaultOSWorkflowConfigurator.JiraTypeResolverDelegator delegator = configurator.new JiraTypeResolverDelegator();

        // register our Delegate
        final TypeResolver typeResolver = mock(TypeResolver.class);
        configurator.registerTypeResolver(CLASS_NAME, typeResolver);

        // set up expectations
        final Map<String, String> args = singletonMap(Workflow.CLASS_NAME, CLASS_NAME);
        final Condition expectedCondition = mock(Condition.class);
        when(typeResolver.getCondition(TYPE, args)).thenReturn(expectedCondition);

        final Condition condition = delegator.getCondition(TYPE, args);

        assertThat(condition, allOf(instanceOf(SkippableCondition.class), hasProperty("condition", is(expectedCondition))));
    }

    @Test
    public void testGetConditionDelegatorRegisteredThenUnregistered() throws Exception {
        final DefaultOSWorkflowConfigurator configurator = new DefaultOSWorkflowConfigurator();
        final DefaultOSWorkflowConfigurator.JiraTypeResolverDelegator delegator = configurator.new JiraTypeResolverDelegator();

        // register our Delegate
        final TypeResolver typeResolver = mock(TypeResolver.class);
        configurator.registerTypeResolver(CLASS_NAME, typeResolver);

        // set up expectations
        final Map<String, String> args = MapBuilder.singletonMap(Workflow.CLASS_NAME, CLASS_NAME);
        when(typeResolver.getCondition(TYPE, args)).thenReturn(null);

        delegator.getCondition(TYPE, args);
        configurator.unregisterTypeResolver(CLASS_NAME, typeResolver);

        assertNull(delegator.getCondition(TYPE, args));
    }

    @Test
    public void testGetConditionDelegatorRegisteredTwice() throws Exception {
        final DefaultOSWorkflowConfigurator configurator = new DefaultOSWorkflowConfigurator();
        final DefaultOSWorkflowConfigurator.JiraTypeResolverDelegator delegator = configurator.new JiraTypeResolverDelegator();

        // register our Delegate
        final TypeResolver typeResolver1 = mock(TypeResolver.class);
        configurator.registerTypeResolver(CLASS_NAME, typeResolver1);
        final TypeResolver typeResolver2 = mock(TypeResolver.class);
        configurator.registerTypeResolver(CLASS_NAME, typeResolver2);

        // set up expectations
        final Map<String, String> args = singletonMap(Workflow.CLASS_NAME, CLASS_NAME);
        Condition expectedCondition1 = mock(Condition.class);
        Condition expectedCondition2 = mock(Condition.class);
        when(typeResolver2.getCondition(TYPE, args)).thenReturn(expectedCondition2);
        when(typeResolver1.getCondition(TYPE, args)).thenReturn(expectedCondition1);

        final Condition condition = delegator.getCondition(TYPE, args);

        assertThat(condition, allOf(instanceOf(SkippableCondition.class), hasProperty("condition", is(expectedCondition2))));
    }

    @Test
    public void testGetValidatorDelegatorNotRegistered() throws Exception {
        final DefaultOSWorkflowConfigurator configurator = new DefaultOSWorkflowConfigurator();
        final DefaultOSWorkflowConfigurator.JiraTypeResolverDelegator delegator = configurator.new JiraTypeResolverDelegator();

        final Validator result = delegator.getValidator(TYPE, MapBuilder.singletonMap(Workflow.CLASS_NAME, CLASS_NAME));
        assertNull(result);
    }

    @Test
    public void testGetValidatorDelegatorRegistered() throws Exception {
        final DefaultOSWorkflowConfigurator configurator = new DefaultOSWorkflowConfigurator();
        final DefaultOSWorkflowConfigurator.JiraTypeResolverDelegator delegator = configurator.new JiraTypeResolverDelegator();

        // register our Delegate
        final TypeResolver typeResolver = mock(TypeResolver.class);
        configurator.registerTypeResolver(CLASS_NAME, typeResolver);

        // set up expectations
        final Map<String, String> args = singletonMap(Workflow.CLASS_NAME, CLASS_NAME);
        final Validator expectedValidator = mock(Validator.class);
        when(typeResolver.getValidator(TYPE, args)).thenReturn(expectedValidator);

        final Validator validator = delegator.getValidator(TYPE, args);

        assertThat(validator, allOf(instanceOf(SkippableValidator.class), hasProperty("validator", is(expectedValidator))));
    }

    @Test
    public void testGetFunctionDelegatorNotRegistered() throws Exception {
        final DefaultOSWorkflowConfigurator configurator = new DefaultOSWorkflowConfigurator();
        final DefaultOSWorkflowConfigurator.JiraTypeResolverDelegator delegator = configurator.new JiraTypeResolverDelegator();

        final FunctionProvider result = delegator.getFunction(TYPE, MapBuilder.singletonMap(Workflow.CLASS_NAME, CLASS_NAME));
        assertNull(result);
    }

    @Test
    public void testGetFunctionDelegatorRegistered() throws Exception {
        final DefaultOSWorkflowConfigurator configurator = new DefaultOSWorkflowConfigurator();
        final DefaultOSWorkflowConfigurator.JiraTypeResolverDelegator delegator = configurator.new JiraTypeResolverDelegator();

        // register our Delegate
        final TypeResolver typeResolver = mock(TypeResolver.class);
        configurator.registerTypeResolver(CLASS_NAME, typeResolver);

        // set up expectations
        final Map<String, String> args = singletonMap(Workflow.CLASS_NAME, CLASS_NAME);
        final FunctionProvider expectedFunctionProvider = mock(FunctionProvider.class);
        when(typeResolver.getFunction(TYPE, args)).thenReturn(expectedFunctionProvider);

        final FunctionProvider functionProvider = delegator.getFunction(TYPE, args);

        assertEquals(expectedFunctionProvider, functionProvider);
    }

    @Test
    public void testGetRegisterDelegatorNotRegistered() throws Exception {
        final DefaultOSWorkflowConfigurator configurator = new DefaultOSWorkflowConfigurator();
        final DefaultOSWorkflowConfigurator.JiraTypeResolverDelegator delegator = configurator.new JiraTypeResolverDelegator();

        final Register result = delegator.getRegister(TYPE, MapBuilder.singletonMap(Workflow.CLASS_NAME, CLASS_NAME));
        assertNull(result);
    }

    @Test
    public void testGetRegisterDelegatorRegistered() throws Exception {
        final DefaultOSWorkflowConfigurator configurator = new DefaultOSWorkflowConfigurator();
        final DefaultOSWorkflowConfigurator.JiraTypeResolverDelegator delegator = configurator.new JiraTypeResolverDelegator();

        // register our Delegate
        final TypeResolver typeResolver = mock(TypeResolver.class);
        configurator.registerTypeResolver(CLASS_NAME, typeResolver);

        // set up expectations
        final Map<String, String> args = singletonMap(Workflow.CLASS_NAME, CLASS_NAME);
        final Register expectedRegister = mock(Register.class);
        when(typeResolver.getRegister(TYPE, args)).thenReturn(expectedRegister);

        final Register register = delegator.getRegister(TYPE, args);

        assertEquals(expectedRegister, register);
    }
}
