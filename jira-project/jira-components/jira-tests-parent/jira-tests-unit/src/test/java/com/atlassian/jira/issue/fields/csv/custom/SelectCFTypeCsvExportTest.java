package com.atlassian.jira.issue.fields.csv.custom;

import com.atlassian.jira.action.issue.customfields.option.MockOption;
import com.atlassian.jira.issue.customfields.CustomFieldType;
import com.atlassian.jira.issue.customfields.impl.SelectCFType;
import com.atlassian.jira.issue.customfields.option.Option;
import org.junit.Test;

public class SelectCFTypeCsvExportTest extends CustomFieldSingleValueCsvExporterTest<Option> {
    @Override
    protected CustomFieldType<Option, ?> createField() {
        return new SelectCFType(null, null, null, null);
    }

    @Test
    public void optionValueIsExported() {
        whenFieldValueIs(MockOption.value("someOption"));
        assertExportedValue("someOption");
    }
}
