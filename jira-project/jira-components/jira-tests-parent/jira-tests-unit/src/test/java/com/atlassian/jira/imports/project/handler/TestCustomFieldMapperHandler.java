package com.atlassian.jira.imports.project.handler;

import com.atlassian.fugue.Effect;
import com.atlassian.jira.exception.ParseException;
import com.atlassian.jira.external.beans.ExternalProject;
import com.atlassian.jira.imports.project.core.BackupProject;
import com.atlassian.jira.imports.project.core.BackupProjectImpl;
import com.atlassian.jira.imports.project.customfield.ExternalCustomFieldValueImpl;
import com.atlassian.jira.imports.project.mapper.CustomFieldMapper;
import com.atlassian.jira.imports.project.parser.CustomFieldValueParser;
import com.atlassian.jira.imports.project.parser.CustomFieldValueParserImpl;
import com.atlassian.jira.junit.rules.InitMockitoMocks;
import com.atlassian.jira.util.collect.MapBuilder;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.mockito.Mock;

import java.util.Collections;

import static org.mockito.Matchers.anyMap;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

/**
 * @since v3.13
 */
public class TestCustomFieldMapperHandler {
    @Rule
    public TestRule initMockitoMocks = new InitMockitoMocks(this);

    @Mock
    private CustomFieldMapper mockCustomFieldMapper;
    @Mock
    private CustomFieldValueParser customFieldValueParser;

    private final MapBuilder<String, CustomFieldValueParser> entities = MapBuilder.newBuilder();

    @Before
    public void setUp() throws Exception {
        entities.add(CustomFieldValueParser.CUSTOM_FIELD_VALUE_ENTITY_NAME, new CustomFieldValueParserImpl());
    }

    @Test
    public void testCustomFieldValueFlaggedAsInUse() throws ParseException {
        testFlagValueAsRequired(backupProjectWithIssues(33333L), "22222", "33333", customFieldMapper ->
                verify(customFieldMapper).flagValueAsRequired("22222", "33333"));
    }

    @Test
    public void testCustomFieldValueIssueNotInProject() throws ParseException {
        testFlagValueAsRequired(backupProjectWithIssues(66666L), "22222", "33333", customFieldMapper ->
                verify(customFieldMapper, times(0)).flagValueAsRequired(anyString(), anyString()));
    }

    @Test
    public void testCustomFieldValueWrongEntityType() throws ParseException {
        CustomFieldMapperHandler customFieldMapperHandler = new CustomFieldMapperHandler(backupProjectWithIssues(66666L), mockCustomFieldMapper, entities.toMap());

        customFieldMapperHandler.handleEntity("BSENTITY", Collections.emptyMap());

        verifyZeroInteractions(mockCustomFieldMapper);
    }

    private void testFlagValueAsRequired(BackupProject project, String fieldId, String issueId, Effect<CustomFieldMapper> verifyFunction)
            throws ParseException {

        ExternalCustomFieldValueImpl externalCustomFieldValue = new ExternalCustomFieldValueImpl("11111", fieldId, issueId);
        when(customFieldValueParser.parse(anyMap())).thenReturn(externalCustomFieldValue);

        entities.add(CustomFieldValueParser.CUSTOM_FIELD_VALUE_ENTITY_NAME, customFieldValueParser);
        CustomFieldMapperHandler customFieldMapperHandler = new CustomFieldMapperHandler(project, mockCustomFieldMapper, entities.toMap());

        customFieldMapperHandler.handleEntity(CustomFieldValueParser.CUSTOM_FIELD_VALUE_ENTITY_NAME, Collections.emptyMap());

        verifyFunction.apply(mockCustomFieldMapper);
    }

    private BackupProject backupProjectWithIssues(Long... issues) {
        final ExternalProject project = new ExternalProject();
        project.setId("1234");
        return new BackupProjectImpl(project, Collections.emptyList(), Collections.emptyList(),
                Collections.emptyList(), ImmutableList.copyOf(issues), 0, ImmutableMap.of());
    }
}
