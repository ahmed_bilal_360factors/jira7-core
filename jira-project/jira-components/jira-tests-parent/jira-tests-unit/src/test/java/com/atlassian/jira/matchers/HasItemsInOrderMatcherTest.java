package com.atlassian.jira.matchers;

import com.google.common.collect.ImmutableList;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.function.Function;

import static com.google.common.collect.ImmutableList.of;
import static org.junit.Assert.assertThat;

@RunWith(Parameterized.class)
public class HasItemsInOrderMatcherTest {
    final Iterable<Integer> expected;
    final Iterable<Integer> actual;
    final Function<Matcher<Iterable<Integer>>, Matcher<Iterable<Integer>>> notModifier;

    public HasItemsInOrderMatcherTest(
            final Iterable<Integer> actual,
            final Function<Matcher<Iterable<Integer>>,
                    Matcher<Iterable<Integer>>> notModifier, final Iterable<Integer> expected) {

        this.expected = expected;
        this.actual = actual;
        this.notModifier = notModifier;
    }

    @Parameterized.Parameters(name = "{2} {1} {0}")
    public static Iterable<Object[]> parameters() {
        final Function<Matcher<Iterable<Integer>>, Matcher<Iterable<Integer>>> hasItemsInOrder = describedFunction(Matchers::is, "in");
        final Function<Matcher<Iterable<Integer>>, Matcher<Iterable<Integer>>> hasNotItemsInOrder = describedFunction(Matchers::not, "not in");

        final ImmutableList<Integer> LIST_1_TO_6 = of(1, 2, 3, 4, 5, 6);
        return ImmutableList.copyOf(new Object[][]{
                {LIST_1_TO_6, hasItemsInOrder, of(1, 4, 6)},
                {LIST_1_TO_6, hasItemsInOrder, of(6)},
                {LIST_1_TO_6, hasItemsInOrder, of()},
                {of(), hasItemsInOrder, of()},
                {of(1, 1, 2, 2, 3, 3), hasItemsInOrder, of(1, 2, 3)},

                {LIST_1_TO_6, hasNotItemsInOrder, of(1, 2, 7)},
                {LIST_1_TO_6, hasNotItemsInOrder, of(1, 1)},
                {of(), hasNotItemsInOrder, of(1)}
        });
    }

    @Test
    public void checkIfMatches() {
        final Matcher<Iterable<Integer>> testedMatcher = HasItemsInOrderMatcher.hasItemsInOrder(expected);
        final Matcher<Iterable<Integer>> modifiedTestedMatcher = notModifier.apply(testedMatcher);

        assertThat(actual, modifiedTestedMatcher);
    }

    public static <P, R> Function<P, R> describedFunction(final Function<P, R> fn, final String description) {
        return new Function<P, R>() {
            @Override
            public R apply(final P p) {
                return fn.apply(p);
            }

            @Override
            public String toString() {
                return description;
            }
        };
    }
}