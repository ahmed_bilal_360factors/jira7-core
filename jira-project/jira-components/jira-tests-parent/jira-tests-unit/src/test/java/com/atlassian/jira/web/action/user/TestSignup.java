package com.atlassian.jira.web.action.user;

import com.atlassian.core.test.util.DuckTypeProxy;
import com.atlassian.jira.application.ApplicationRoleManager;
import com.atlassian.jira.bc.user.CreateUserApplicationHelper;
import com.atlassian.jira.bc.user.UserService;
import com.atlassian.jira.bc.user.UserService.CreateUserRequest;
import com.atlassian.jira.bc.user.UserServiceResultHelper;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.servlet.JiraCaptchaService;
import com.atlassian.jira.servlet.JiraCaptchaServiceImpl;
import com.atlassian.jira.servlet.NoOpImageCaptchaService;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.user.util.UserUtil;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.webresource.api.assembler.PageBuilderService;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import webwork.action.Action;
import webwork.action.ActionContext;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class TestSignup {

    @Rule
    public final RuleChain mockContainer = MockitoMocksInContainer.forTest(this);


    JiraCaptchaService jiraCaptchaService;
    @Mock
    @AvailableInContainer
    private UserService mockUserService;
    @Mock
    @AvailableInContainer
    private UserManager userManager;
    @Mock
    @AvailableInContainer
    private ApplicationProperties mockApplicationProperties;
    @Mock
    @AvailableInContainer
    private JiraAuthenticationContext mockJiraAuthenticationContext;
    @Mock
    private I18nHelper mockI18nHelper;
    @Mock
    @AvailableInContainer
    private FeatureManager mockFeatureManager;
    @Mock
    @AvailableInContainer
    private CreateUserApplicationHelper applicationHelper;
    @Mock
    @AvailableInContainer
    private ApplicationRoleManager roleManager;
    @Mock
    @AvailableInContainer
    private UserUtil userUtil;
    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private PageBuilderService pageBuilderService;


    private Signup signup;


    UserService.CreateUserValidationResult mockValidationResult;


    @Before
    public void setUp() throws Exception {

        jiraCaptchaService = new JiraCaptchaServiceImpl();
        when(mockApplicationProperties.getOption(APKeys.JIRA_OPTION_CAPTCHA_ON_SIGNUP)).thenReturn(true);
        when(mockApplicationProperties.getString(APKeys.JIRA_MODE)).thenReturn("public");
        when(userManager.hasWritableDirectory()).thenReturn(true);
        when(mockJiraAuthenticationContext.getI18nHelper()).thenReturn(mockI18nHelper);
        when(mockI18nHelper.getText(anyString())).thenAnswer(invocation -> invocation.getArguments()[0]);

        signup = new Signup(mockApplicationProperties, mockUserService, jiraCaptchaService, null, applicationHelper, roleManager, userUtil, pageBuilderService);

    }

    @Test
    public void testValidationOfCaptcha() throws Exception {
        _testCaptchaValidation();
    }


    private void _testCaptchaValidation() throws Exception {
        final String sessionId = "testSessionId";
        Object sessionDelegate = new Object() {
            public String getId() {
                return sessionId;
            }
        };
        final HttpSession session = (HttpSession) DuckTypeProxy.getProxy(HttpSession.class, sessionDelegate);

        Object requestDelegate = new Object() {
            public Object getAttribute(String string) {
                return null;
            }

            public HttpSession getSession(boolean create) {
                return session;
            }
        };
        HttpServletRequest request = (HttpServletRequest) DuckTypeProxy.getProxy(HttpServletRequest.class, requestDelegate);

        mockValidationResult = UserServiceResultHelper.getCreateUserValidationResult();
        setupUserServiceValidation(mockValidationResult);

        signup.setUsername("abc");

        jiraCaptchaService.getImageCaptchaService().getImageChallengeForID(sessionId);
        HttpServletRequest oldRequest = ActionContext.getRequest();
        try {
            ActionContext.setRequest(request);
            String result = signup.execute();

            assertEquals(Action.INPUT, result);
            assertEquals(signup.getText("signup.error.captcha.incorrect"), signup.getErrors().get("captcha"));
        } finally {
            ActionContext.setRequest(oldRequest);
        }
    }

    @Test
    public void testNoValidationOfCaptchaIfOff() throws Exception {
        final ErrorCollection errors = new SimpleErrorCollection();
        errors.addError("fullname", "You must specify a full name.");
        errors.addError("email", "You must specify an email address.");
        errors.addError("password", "You must specify a password.");
        mockValidationResult = UserServiceResultHelper.getCreateUserValidationResult(errors);
        setupUserServiceValidation(mockValidationResult);
        when(mockApplicationProperties.getOption(APKeys.JIRA_OPTION_CAPTCHA_ON_SIGNUP)).thenReturn(false);

        signup.setUsername("abc");

        String result = signup.execute();

        assertEquals(Action.INPUT, result);
        assertNull(signup.getErrors().get("captcha"));
    }

    @Test
    public void testExecute() throws Exception {
        when(mockApplicationProperties.getOption(APKeys.JIRA_OPTION_CAPTCHA_ON_SIGNUP)).thenReturn(false);

        mockValidationResult = UserServiceResultHelper.getCreateUserValidationResult();
        setupUserServiceValidation(mockValidationResult);
        signup.setUsername("a");
        signup.setPassword("b");
        signup.setFullname("c");
        signup.setEmail("d@d.com");

        when(mockUserService.createUser(mockValidationResult)).thenReturn(new MockApplicationUser("a", "c", "d@d.com"));
        String result = signup.execute();

        assertEquals(Action.SUCCESS, result);
        ArgumentCaptor<CreateUserRequest> createUserRequest = ArgumentCaptor.forClass(CreateUserRequest.class);
        verify(mockUserService, times(1)).validateCreateUser(createUserRequest.capture());
        verify(mockUserService, times(1)).createUser(mockValidationResult);

    }

    @Test
    public void testValidateNoWritableDirectory() throws Exception {
        when(userManager.hasWritableDirectory()).thenReturn(false);

        assertEquals("modebreach", signup.doDefault());
        verifyNoMoreInteractions(applicationHelper);
    }

    private void setupUserServiceValidation(UserService.CreateUserValidationResult result) {
        when(mockUserService.validateCreateUser(
                any(CreateUserRequest.class))).thenReturn(result);
    }
}
