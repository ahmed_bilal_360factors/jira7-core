package com.atlassian.jira.user.util;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.cache.memory.MemoryCacheManager;
import com.atlassian.crowd.embedded.api.CrowdDirectoryService;
import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.crowd.exception.FailedAuthenticationException;
import com.atlassian.crowd.manager.directory.DirectoryManager;
import com.atlassian.extras.api.jira.JiraLicense;
import com.atlassian.jira.application.ApplicationRole;
import com.atlassian.jira.application.ApplicationRoleManager;
import com.atlassian.jira.application.MockApplicationRole;
import com.atlassian.jira.bc.license.JiraLicenseService;
import com.atlassian.jira.bc.user.UserService;
import com.atlassian.jira.entity.property.JsonEntityPropertyManager;
import com.atlassian.jira.event.MockEventPublisher;
import com.atlassian.jira.exception.CreateException;
import com.atlassian.jira.exception.PermissionException;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.license.LicenseCountService;
import com.atlassian.jira.license.LicenseDetails;
import com.atlassian.jira.mock.MockFeatureManager;
import com.atlassian.jira.mock.component.MockComponentWorker;
import com.atlassian.jira.mock.ofbiz.MockOfBizDelegator;
import com.atlassian.jira.permission.GlobalPermissionType;
import com.atlassian.jira.security.DefaultGlobalPermissionManager;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.MockGlobalPermissionTypeManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.security.login.LoginManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.MockCrowdService;
import com.atlassian.jira.user.MockGroup;
import com.atlassian.jira.user.MockUser;
import com.atlassian.jira.user.UserDetails;
import com.atlassian.seraph.spi.rememberme.RememberMeTokenDao;
import com.google.common.base.Objects;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.RuleChain;
import org.mockito.Mock;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Optional;
import java.util.Set;
import java.util.SortedSet;

import static com.atlassian.jira.permission.GlobalPermissionKey.USE;
import static com.atlassian.jira.user.ApplicationUsers.toDirectoryUsers;
import static com.atlassian.jira.util.CollectionAssert.assertContainsExactly;
import static com.google.common.collect.Sets.newHashSet;
import static java.util.Arrays.asList;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasItems;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.junit.matchers.JUnitMatchers.hasItem;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * TestUserUtilImpl without being a JiraMockTestCase
 *
 * @since v4.3
 */
public class TestUserUtilImpl {
    @Rule
    public final RuleChain mockContainer = MockitoMocksInContainer.forTest(this);
    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Mock
    @AvailableInContainer
    private JiraLicenseService jiraLicenseService;
    @Mock
    @AvailableInContainer
    private RememberMeTokenDao rememberMeTokenDao;
    @Mock
    @AvailableInContainer
    private LoginManager loginManager;
    @Mock
    @AvailableInContainer
    private DirectoryManager directoryManager;
    @Mock
    @AvailableInContainer
    private CrowdDirectoryService crowdDirectoryService;
    @Mock
    private LicenseDetails licenseDetails;
    @Mock
    @AvailableInContainer
    private ApplicationRoleManager applicationRoleManager;

    MockGlobalPermissionTypeManager globalPermissionTypesManager = new MockGlobalPermissionTypeManager();

    @Mock
    @AvailableInContainer
    private UserManager mockUserManager;
    @Mock
    @AvailableInContainer
    private LicenseCountService licenseCountService;
    @Mock
    @AvailableInContainer
    private UserService userService;

    private MockUserManager userManager;
    private MockCrowdService crowdService;
    private GlobalPermissionManager globalPermissionManager;
    private UserUtilImpl userUtil;
    private GlobalPermissionType usePermission;

    @Mock
    private JsonEntityPropertyManager jsonEntityPropertyManager;

    @Mock
    private MockFeatureManager featureManager = new MockFeatureManager();
    @Mock
    private GroupManager groupManager;

    @Before
    public void setup() {
        when(mockUserManager.getUserIdentityByKey(anyString())).thenAnswer(invocationOnMock -> {
            String key = Objects.firstNonNull((String) invocationOnMock.getArguments()[0], "key");
            return Optional.of(UserIdentity.withId(MockApplicationUser.SEQUENCE.incrementAndGet()).key(key).andUsername(key));
        });
        when(mockUserManager.getUserIdentityByUsername(anyString())).thenAnswer(invocationOnMock -> {
            String name = Objects.firstNonNull((String) invocationOnMock.getArguments()[0], "name");
            return Optional.of(UserIdentity.withId(MockApplicationUser.SEQUENCE.incrementAndGet()).key(name).andUsername(name));
        });

        globalPermissionTypesManager = new MockGlobalPermissionTypeManager();
        crowdService = spy(new MockCrowdService());
        userManager = spy(new MockUserManager());

        userManager.setCrowdService(crowdService);

        globalPermissionManager = spy(new DefaultGlobalPermissionManager(crowdService,
                new MockOfBizDelegator(), new MockEventPublisher(), new MockGlobalPermissionTypeManager(),
                new MemoryCacheManager(), applicationRoleManager, groupManager, new DisabledRecoveryMode(), new MockFeatureManager()));
        usePermission = globalPermissionManager.getGlobalPermission(USE).get();

        when(jiraLicenseService.isLicenseSet()).thenReturn(true);
        when(jiraLicenseService.getLicenses()).thenReturn(ImmutableList.of(licenseDetails));

        mockLicenseMaximumNumberOfUsers(10);

        userUtil = new UserUtilImpl(null, globalPermissionManager, crowdService, null, null, null, null, null, null,
                null, null, null, userManager, licenseCountService, applicationRoleManager,
                jsonEntityPropertyManager);
        when(licenseDetails.isUnlimitedNumberOfUsers()).thenReturn(true);
        when(globalPermissionManager.getAllGlobalPermissions()).thenReturn(globalPermissionTypesManager.getAll());
    }

    private void mockLicenseMaximumNumberOfUsers(int seatCount) {
        JiraLicense jiraLicense = mock(JiraLicense.class);
        when(licenseDetails.getJiraLicense()).thenReturn(jiraLicense);
        when(jiraLicense.getMaximumNumberOfUsers()).thenReturn(seatCount);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetAllUsersInGroupNamesIllegalArgument() throws Exception {
        userUtil.getAllUsersInGroupNames(null);
    }

    @Test
    public void testGetAllUsersInGroupNamesEmpty() throws Exception {
        SortedSet<ApplicationUser> users = userUtil.getAllUsersInGroupNames(new ArrayList<>());
        assertTrue(users.isEmpty());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetAllUsersInGroupsIllegalArgument() throws Exception {
        userUtil.getAllUsersInGroups(null);
    }

    @Test
    public void testGetAllUsersInGroupsEmpty() throws Exception {
        SortedSet<ApplicationUser> users = userUtil.getAllUsersInGroups(new ArrayList<Group>());
        assertTrue(users.isEmpty());
    }

    @Test
    public void testGetAllUsersInGroupNames() throws Exception {
        // Set up users and groups.
        crowdService.addUser(new MockUser("ZAC"), "");
        final User userAdam = new MockUser("adam");
        final Group groupAnts = new MockGroup("ants");
        crowdService.addUser(userAdam, "");
        crowdService.addGroup(groupAnts);
        crowdService.addUserToGroup(userAdam, groupAnts);
        final User userBetty = new MockUser("betty");
        final User userBertie = new MockUser("bertie");
        final Group groupBeetles = new MockGroup("beetles");
        crowdService.addUser(userBetty, "");
        crowdService.addUser(userBertie, "");
        crowdService.addGroup(groupBeetles);
        crowdService.addUserToGroup(userBetty, groupBeetles);
        crowdService.addUserToGroup(userBertie, groupBeetles);

        // Ants
        SortedSet<ApplicationUser> antUsers = userUtil.getAllUsersInGroupNames(asList("ants"));
        assertEquals(1, antUsers.size());
        assertThat(toDirectoryUsers(antUsers), contains(userAdam));

        // Beetles
        SortedSet<ApplicationUser> beetlesUsers = userUtil.getAllUsersInGroupNames(asList("beetles"));
        assertEquals(2, beetlesUsers.size());
        assertThat(toDirectoryUsers(beetlesUsers), contains(userBertie, userBetty));

        // Ants and Beetles
        SortedSet<ApplicationUser> antAndBeetlesUsers = userUtil.getAllUsersInGroupNames(asList("ants", "beetles"));
        assertEquals(3, antAndBeetlesUsers.size());
        assertThat(toDirectoryUsers(antAndBeetlesUsers), contains(userAdam, userBertie, userBetty));
    }

    @Test
    public void testGetAllUsersInGroups() throws Exception {
        // Set up users and groups.
        crowdService.addUser(new MockUser("ZAC"), "");
        final User userAdam = new MockUser("adam");
        final Group groupAnts = new MockGroup("ants");
        crowdService.addUser(userAdam, "");
        crowdService.addGroup(groupAnts);
        crowdService.addUserToGroup(userAdam, groupAnts);
        final User userBetty = new MockUser("betty");
        final User userBertie = new MockUser("bertie");
        final Group groupBeetles = new MockGroup("beetles");
        crowdService.addUser(userBetty, "");
        crowdService.addUser(userBertie, "");
        crowdService.addGroup(groupBeetles);
        crowdService.addUserToGroup(userBetty, groupBeetles);
        crowdService.addUserToGroup(userBertie, groupBeetles);

        // Ants
        final SortedSet<ApplicationUser> antUsers = userUtil.getAllUsersInGroups(asList(groupAnts));
        assertEquals(1, antUsers.size());
        assertThat(toDirectoryUsers(antUsers), contains(userAdam));

        // Beetles
        final SortedSet<ApplicationUser> beetlesUsers = userUtil.getAllUsersInGroups(asList(groupBeetles));
        assertEquals(2, beetlesUsers.size());
        assertThat(toDirectoryUsers(beetlesUsers), contains(userBertie, userBetty));

        // Ants and Beetles
        final SortedSet<ApplicationUser> antAndBeetlesUsers = userUtil.getAllUsersInGroups(asList(groupAnts, groupBeetles));
        assertEquals(3, antAndBeetlesUsers.size());
        assertThat(toDirectoryUsers(antAndBeetlesUsers), contains(userAdam, userBertie, userBetty));
    }

    @Test
    public void testGetGroupsForUser() throws Exception {
        // Set up users and groups.
        crowdService.addUser(new MockUser("ZAC"), "");
        final User userAdam = new MockUser("adam");
        final Group groupAnts = new MockGroup("ants");
        crowdService.addUser(userAdam, "");
        crowdService.addGroup(groupAnts);
        crowdService.addUserToGroup(userAdam, groupAnts);
        final User userBetty = new MockUser("betty");
        final User userBertie = new MockUser("bertie");
        final Group groupBeetles = new MockGroup("beetles");
        crowdService.addUser(userBetty, "");
        crowdService.addUser(userBertie, "");
        crowdService.addGroup(groupBeetles);
        crowdService.addUserToGroup(userBetty, groupBeetles);
        crowdService.addUserToGroup(userBertie, groupBeetles);

        assertThat(userUtil.getGroupsForUser(userAdam.getName()), contains(groupAnts));
        assertThat(userUtil.getGroupsForUser(userBertie.getName()), contains(groupBeetles));
        assertThat(userUtil.getGroupsForUser(userBetty.getName()), contains(groupBeetles));
    }

    @Test
    public void testAddUserToGroup() throws Exception {
        final Group group = new MockGroup("goodies");
        crowdService.addGroup(new MockGroup("goodies"));
        final ApplicationUser user = new MockApplicationUser("bill");
        crowdService.addUser(user.getDirectoryUser(), null);

        // Pre condition
        assertThat(userUtil.getGroupNamesForUser("bill"), not(hasItem("goodies")));
        userUtil.addUserToGroup(group, user);
        // post condition
        assertThat(userUtil.getGroupNamesForUser("bill"), hasItem("goodies"));
    }

    @Test
    public void testRemoveUserFromGroup() throws Exception {
        final Group group = new MockGroup("goodies");
        crowdService.addGroup(new MockGroup("goodies"));
        final ApplicationUser user = new MockApplicationUser("bill");
        crowdService.addUser(user.getDirectoryUser(), null);
        crowdService.addUserToGroup(user, group);

        // Pre condition
        assertThat(userUtil.getGroupNamesForUser("bill"), hasItem("goodies"));
        userUtil.removeUserFromGroup(group, user);
        // post condition
        assertThat(userUtil.getGroupNamesForUser("bill"), not(hasItem("goodies")));
    }

    @Test
    public void testAddUserToGroups() throws Exception {
        final Group group1 = new MockGroup("goodies");
        crowdService.addGroup(group1);
        final Group group2 = new MockGroup("baddies");
        crowdService.addGroup(group2);
        final ApplicationUser user = new MockApplicationUser("bill");
        crowdService.addUser(user.getDirectoryUser(), null);

        // Pre condition
        assertThat(userUtil.getGroupNamesForUser("bill"), not(hasItem("goodies")));
        assertThat(userUtil.getGroupNamesForUser("bill"), not(hasItem("baddies")));
        userUtil.addUserToGroups(asList(group1, group2), user);
        // post condition
        assertThat(userUtil.getGroupNamesForUser("bill"), hasItem("goodies"));
        assertThat(userUtil.getGroupNamesForUser("bill"), hasItem("baddies"));
    }

    @Test
    public void testRemoveUserFromGroups() throws Exception {
        final Group group1 = new MockGroup("goodies");
        crowdService.addGroup(group1);
        final Group group2 = new MockGroup("baddies");
        crowdService.addGroup(group2);
        final Group group3 = new MockGroup("others");
        crowdService.addGroup(group3);
        final ApplicationUser user = new MockApplicationUser("bill");
        crowdService.addUser(user.getDirectoryUser(), null);
        userUtil.addUserToGroups(asList(group1, group2, group3), user);

        // Pre condition
        assertThat(userUtil.getGroupNamesForUser("bill"), hasItem("goodies"));
        assertThat(userUtil.getGroupNamesForUser("bill"), hasItem("baddies"));
        assertThat(userUtil.getGroupNamesForUser("bill"), hasItem("others"));
        userUtil.removeUserFromGroups(asList(group1, group2), user);
        // post condition
        assertThat(userUtil.getGroupNamesForUser("bill"), not(hasItem("goodies")));
        assertThat(userUtil.getGroupNamesForUser("bill"), not(hasItem("baddies")));
        assertThat(userUtil.getGroupNamesForUser("bill"), hasItem("others"));
    }

    @Test
    public void getActiveUserCountShouldDelegateToLicenseCountService() throws Exception {

        when (licenseCountService.totalBillableUsers()).thenReturn(-63);
        final int activeUserCount = userUtil.getActiveUserCount();
        assertEquals(-63, activeUserCount);
        verify(licenseCountService).totalBillableUsers();
    }

    @Test
    public void testCanActivateUnlimitedNumberOfUsers() throws Exception {
        when(licenseDetails.isUnlimitedNumberOfUsers()).thenReturn(true);
        assertTrue(userUtil.canActivateNumberOfUsers(3000000));
    }

    @Test
    public void testCanActivateNumberOfUsersWhenRoleHasSeatsAvailable() {
        MockApplicationRole roleOne = new MockApplicationRole(ApplicationKey.valueOf("jira-roleOne")).selectedByDefault(true);
        when(applicationRoleManager.getDefaultApplicationKeys()).thenReturn(newHashSet(roleOne.getKey()));
        when(applicationRoleManager.hasSeatsAvailable(roleOne.getKey(), 5)).thenReturn(true);

        assertTrue(userUtil.canActivateNumberOfUsers(5));
        assertFalse(userUtil.canActivateNumberOfUsers(6));
    }

    @Test
    public void testCanNotActivateNumberOfUsersWhenRoleDoesNotHaveSeatsAvailable() {
        MockApplicationRole roleOne = new MockApplicationRole(ApplicationKey.valueOf("jira-roleOne")).selectedByDefault(true);
        when(applicationRoleManager.getDefaultApplicationKeys()).thenReturn(newHashSet(roleOne.getKey()));
        when(applicationRoleManager.hasSeatsAvailable(eq(roleOne.getKey()), anyInt())).thenReturn(false);

        assertFalse(userUtil.canActivateNumberOfUsers(4));
    }

    /**
     * When roles are enabled and there isn't any default roles configured.
     * Then users can be added to the defaults because there is not any defaults.
     * This logic is for backwards compatibility.
     */
    @Test
    public void testCanNotActivateNumberOfUsersWhenThereAreNoDefaultRoles() {
        when(applicationRoleManager.getDefaultApplicationKeys()).thenReturn(newHashSet());

        assertTrue(userUtil.canActivateNumberOfUsers(4));
    }

    @Test
    public void addToJiraUsePermission_WhenAllowed() throws Exception {
        new MockComponentWorker().init()
                .addMock(UserUtil.class, userUtil)
                .addMock(LicenseCountService.class, licenseCountService)
                .addMock(JiraLicenseService.class, jiraLicenseService);

        final Group usersGroup1 = new MockGroup("group1");
        crowdService.addGroup(usersGroup1);
        final Group usersGroup2 = new MockGroup("group2");
        crowdService.addGroup(usersGroup2);
        final Group usersGroup3 = new MockGroup("group3");
        crowdService.addGroup(usersGroup3);
        final ApplicationUser user1 = new MockApplicationUser("bill");
        crowdService.addUser(user1.getDirectoryUser(), null);

        final ApplicationRole applicationRoleSoftware = mock(ApplicationRole.class);
        final ApplicationKey softwareAppKey = ApplicationKey.valueOf("software");
        when(applicationRoleSoftware.getKey()).thenReturn(softwareAppKey);
        when(applicationRoleSoftware.getDefaultGroups()).thenReturn(ImmutableSet.of(usersGroup1, usersGroup3));
        when(applicationRoleSoftware.getGroups()).thenReturn(ImmutableSet.of(usersGroup1, usersGroup3));
        when(applicationRoleManager.getDefaultApplicationKeys()).thenReturn(newHashSet(softwareAppKey));
        when(applicationRoleManager.getDefaultGroups(softwareAppKey)).thenReturn(ImmutableSet.of(usersGroup1, usersGroup3));

        when(applicationRoleManager.hasSeatsAvailable(any(), eq(1))).thenReturn(true);

        userUtil.addToJiraUsePermission(user1);

        // assert they now belong to groups 1 and 3 but not 2
        assertTrue(crowdService.isUserMemberOfGroup(user1.getDirectoryUser(), usersGroup1));
        assertFalse(crowdService.isUserMemberOfGroup(user1.getDirectoryUser(), usersGroup2));
        assertTrue(crowdService.isUserMemberOfGroup(user1.getDirectoryUser(), usersGroup3));
    }

    @Test
    public void addToJiraUsePermission_WhenNotAllowed() throws Exception {
        when(licenseDetails.isUnlimitedNumberOfUsers()).thenReturn(false);
        new MockComponentWorker().init()
                .addMock(UserUtil.class, userUtil)
                .addMock(LicenseCountService.class, licenseCountService)
                .addMock(JiraLicenseService.class, jiraLicenseService)
                .addMock(GlobalPermissionManager.class, globalPermissionManager);

        final Group usersGroup1 = new MockGroup("group1");
        crowdService.addGroup(usersGroup1);
        final Group usersGroup2 = new MockGroup("group2");
        crowdService.addGroup(usersGroup2);
        final Group usersGroup3 = new MockGroup("group3");
        crowdService.addGroup(usersGroup3);

        final ApplicationUser user1 = new MockApplicationUser("bill");
        crowdService.addUser(user1.getDirectoryUser(), null);

        final ApplicationRole applicationRoleSoftware = mock(ApplicationRole.class);
        final ApplicationKey softwareAppKey = ApplicationKey.valueOf("software");
        when(applicationRoleSoftware.getKey()).thenReturn(softwareAppKey);
        when(applicationRoleSoftware.getDefaultGroups()).thenReturn(ImmutableSet.of(usersGroup1, usersGroup2, usersGroup3));
        when(applicationRoleManager.getDefaultApplicationKeys()).thenReturn(newHashSet(softwareAppKey));


        JiraLicense jiraLicense = mock(JiraLicense.class);
        when(licenseDetails.getJiraLicense()).thenReturn(jiraLicense);
        when(jiraLicense.getMaximumNumberOfUsers()).thenReturn(12);
        when(licenseCountService.totalBillableUsers()).thenReturn(12);
        userUtil.addToJiraUsePermission(user1);

        // assert they now belong to no groups
        assertFalse(crowdService.isUserMemberOfGroup(user1.getDirectoryUser(), usersGroup1));
        assertFalse(crowdService.isUserMemberOfGroup(user1.getDirectoryUser(), usersGroup2));
        assertFalse(crowdService.isUserMemberOfGroup(user1.getDirectoryUser(), usersGroup3));
    }

    @Test
    public void getAdmins() throws Exception {
        new MockComponentWorker().init()
                .addMock(LicenseCountService.class, licenseCountService)
                .addMock(UserUtil.class, userUtil)
                .addMock(UserManager.class, mockUserManager)
                .addMock(GlobalPermissionManager.class, globalPermissionManager);

        final Group group1 = new MockGroup("group1");
        final Group group2 = new MockGroup("group2");
        final ApplicationUser user1 = new MockApplicationUser("bill");
        final ApplicationUser user2 = new MockApplicationUser("ted");
        crowdService.addGroup(group1);
        crowdService.addGroup(group2);
        crowdService.addUser(user1.getDirectoryUser(), null);
        crowdService.addUserToGroup(user1, group1);
        crowdService.addUser(user2.getDirectoryUser(), null);
        crowdService.addUserToGroup(user2, group2);

        globalPermissionManager.addPermission(Permissions.ADMINISTER, "group1");
        globalPermissionManager.addPermission(Permissions.SYSTEM_ADMIN, "group2");

        Collection<ApplicationUser> admins = userUtil.getAdministrators();
        assertEquals(1, admins.size());
        assertThat(admins, hasItems(user1));
        Collection<ApplicationUser> sysAdmins = userUtil.getSystemAdministrators();
        assertEquals(1, sysAdmins.size());
        assertThat(sysAdmins, hasItems(user2));
    }

    @Test
    public void changePassword() throws Exception {
        User userOne = new MockUser("bill", "Billy Jean", "billy@bob.com");
        crowdService.addUser(userOne, "asdfasdf");
        User authenticatedUser = crowdService.authenticate("bill", "asdfasdf");
        assertEquals("Billy Jean", authenticatedUser.getDisplayName());

        try {
            crowdService.authenticate("bill", "xxx");
            fail("authenticate should fail with invalid password");
        } catch (FailedAuthenticationException ex) {
            // expected
        }

        userUtil.changePassword(new MockApplicationUser("bill"), "xxx");
        authenticatedUser = crowdService.authenticate("bill", "xxx");
        assertEquals("Billy Jean", authenticatedUser.getDisplayName());
    }

    @Test
    public void createUserShouldNotAddToGroupsIfUserLimitExceeded() throws PermissionException, CreateException {
        UserDetails userData = new UserDetails("foo", "bar");
        when(licenseCountService.totalBillableUsers()).thenReturn(10);

        userUtil.createUser(userData, false, 0, Collections.emptySet());

        verify(crowdService, never()).addUserToGroup(any(User.class), any(Group.class));
    }

    @Test
    public void testGetDisplayableNameSafely() {
        assertNull(userUtil.getDisplayableNameSafely(null));
        assertEquals("bob", userUtil.getDisplayableNameSafely(new MockApplicationUser("bob", null, null)));
        assertEquals("bob", userUtil.getDisplayableNameSafely(new MockApplicationUser("bob", "", "")));
        assertEquals("Bob Belcher", userUtil.getDisplayableNameSafely(new MockApplicationUser("bob", "Bob Belcher", "")));

        assertNull(userUtil.getDisplayableNameSafely(null));
        assertEquals("bob", userUtil.getDisplayableNameSafely(new MockApplicationUser("bob", null, null)));
        assertEquals("bob", userUtil.getDisplayableNameSafely(new MockApplicationUser("bob", "", "")));
        assertEquals("Bob Belcher", userUtil.getDisplayableNameSafely(new MockApplicationUser("bob", "Bob Belcher", "")));
    }

    @Test
    public void getUsersInGroupNames() throws Exception {
        final Group group1 = new MockGroup("group1");
        crowdService.addGroup(group1);
        final Group group2 = new MockGroup("group2");
        crowdService.addGroup(group2);
        final Group group3 = new MockGroup("group3");
        crowdService.addGroup(group3);
        final ApplicationUser user1 = new MockApplicationUser("bill");
        crowdService.addUser(user1.getDirectoryUser(), null);
        crowdService.addUserToGroup(user1, group1);
        crowdService.addUserToGroup(user1, group2);
        final ApplicationUser user2 = new MockApplicationUser("ted");
        crowdService.addUser(user2.getDirectoryUser(), null);
        crowdService.addUserToGroup(user2, group2);
        final ApplicationUser user3 = new MockApplicationUser("edna");
        crowdService.addUser(user3.getDirectoryUser(), null);

        assertContainsExactly(asList(user1), userUtil.getUsersInGroupNames(asList("group1")));
        assertContainsExactly(asList(user1, user2), userUtil.getUsersInGroupNames(asList("group2")));
        assertContainsExactly(asList(user1, user2), userUtil.getUsersInGroupNames(asList("group1", "group2")));
        assertContainsExactly(asList(), userUtil.getUsersInGroupNames(asList("rubbish")));
        assertContainsExactly(asList(user1), userUtil.getUsersInGroupNames(asList("group1", "rubbish")));
    }

    @Test
    public void shouldGetTheCorrectGroupsForUser() throws Exception {
        final Group group1 = new MockGroup("group1");
        crowdService.addGroup(group1);
        final Group group2 = new MockGroup("group2");
        crowdService.addGroup(group2);
        final Group group3 = new MockGroup("group3");
        crowdService.addGroup(group3);

        final ApplicationUser user1 = new MockApplicationUser("bill");
        crowdService.addUserToGroup(user1, group1);
        crowdService.addUserToGroup(user1, group2);
        crowdService.addUserToGroup(user1, group3);

        final ApplicationUser user2 = new MockApplicationUser("ted");
        crowdService.addUserToGroup(user2, group2);

        final ApplicationUser user3 = new MockApplicationUser("edna");
        crowdService.addUser(user3.getDirectoryUser(), null);

        assertContainsExactly(asList(user1), userUtil.getUsersInGroups(asList(group1)));
        assertContainsExactly(asList(user1, user2), userUtil.getUsersInGroups(asList(group2)));
        assertContainsExactly(asList(user1, user2), userUtil.getUsersInGroups(asList(group1, group2)));
        assertContainsExactly(asList(), userUtil.getUsersInGroups(Arrays.<Group>asList(new MockGroup("xxxx"))));
        assertContainsExactly(asList(user1), userUtil.getUsersInGroups(asList(group1, new MockGroup("xxxx"))));
    }

    @Test
    public void getUsersInGroups() throws Exception {
        final Group group1 = new MockGroup("group1");
        crowdService.addGroup(group1);
        final Group group2 = new MockGroup("group2");
        crowdService.addGroup(group2);
        final Group group3 = new MockGroup("group3");
        crowdService.addGroup(group3);
        final ApplicationUser user1 = new MockApplicationUser("bill");
        crowdService.addUser(user1.getDirectoryUser(), null);
        crowdService.addUserToGroup(user1, group1);
        crowdService.addUserToGroup(user1, group2);
        final ApplicationUser user2 = new MockApplicationUser("ted");
        crowdService.addUser(user2.getDirectoryUser(), null);
        crowdService.addUserToGroup(user2, group2);
        final ApplicationUser user3 = new MockApplicationUser("edna");
        crowdService.addUser(user3.getDirectoryUser(), null);

        assertContainsExactly(asList(user1), userUtil.getUsersInGroups(asList(group1)));
        assertContainsExactly(asList(user1, user2), userUtil.getUsersInGroups(asList(group2)));
        assertContainsExactly(asList(user1, user2), userUtil.getUsersInGroups(asList(group1, group2)));
        assertContainsExactly(asList(), userUtil.getUsersInGroups(Arrays.<Group>asList(new MockGroup("xxxx"))));
        assertContainsExactly(asList(user1), userUtil.getUsersInGroups(asList(group1, new MockGroup("xxxx"))));
    }

    @Test
    public void getUsersInGroupNamesSortsCorrectly() throws Exception {
        final Group group1 = new MockGroup("group1");
        crowdService.addGroup(group1);
        final User user1 = new MockUser("a", "Erin Erlang", "");
        crowdService.addUser(user1, null);
        crowdService.addUserToGroup(user1, group1);
        final User user2 = new MockUser("b", "bob down", "");
        crowdService.addUser(user2, null);
        crowdService.addUserToGroup(user2, group1);
        final User user3 = new MockUser("c", "Andrew Aadvark", "");
        crowdService.addUser(user3, null);
        crowdService.addUserToGroup(user3, group1);
        final User user4 = new MockUser("d", "Cathy Caprioska", "");
        crowdService.addUser(user4, null);
        crowdService.addUserToGroup(user4, group1);
        final User user5 = new MockUser("e", "Derryn Derp", "");
        crowdService.addUser(user5, null);
        crowdService.addUserToGroup(user5, group1);

        SortedSet<ApplicationUser> users = userUtil.getUsersInGroupNames(asList("group1"));

        Iterator<ApplicationUser> iter = users.iterator();
        assertEquals("Andrew Aadvark", iter.next().getDisplayName());
        assertEquals("bob down", iter.next().getDisplayName());
        assertEquals("Cathy Caprioska", iter.next().getDisplayName());
        assertEquals("Derryn Derp", iter.next().getDisplayName());
        assertEquals("Erin Erlang", iter.next().getDisplayName());
    }

    @Test
    public void addToJiraUsePermissionAddsToDefaultGroup() throws Exception {
        // Given: there is a single application role
        // and ApplicationRole has a one default group
        // and there are other groups in the directory
        final String SOFTWARE_GROUP = "software.group";
        final String OTHER_GROUP = "other-group";
        final Group softwareGroup = new MockGroup(SOFTWARE_GROUP);
        final Group otherGroup = new MockGroup(OTHER_GROUP);
        final ApplicationRole applicationRoleSoftware = mock(ApplicationRole.class);
        final ApplicationKey softwareAppKey = ApplicationKey.valueOf("software");
        when(applicationRoleSoftware.getKey()).thenReturn(softwareAppKey);
        when(applicationRoleSoftware.getDefaultGroups()).thenReturn(ImmutableSet.of(softwareGroup));
        crowdService.addGroup(new MockGroup(SOFTWARE_GROUP));
        when(applicationRoleManager.getDefaultApplicationKeys()).thenReturn(newHashSet(softwareAppKey));
        when(applicationRoleManager.getUserCount(any(ApplicationKey.class))).thenReturn(3);
        when(applicationRoleManager.getRemainingSeats(any(ApplicationKey.class))).thenReturn(5);
        when(applicationRoleManager.hasSeatsAvailable(any(ApplicationKey.class), anyInt())).thenReturn(true);

        when(applicationRoleManager.getDefaultGroups(softwareAppKey)).thenReturn(newHashSet(softwareGroup, otherGroup));

        final Group usersGroup1 = new MockGroup("goupOne");
        crowdService.addGroup(usersGroup1);
        crowdService.addGroup(softwareGroup);
        crowdService.addGroup(otherGroup);

        final ApplicationUser user1 = new MockApplicationUser("bill");

        // when: adding another user to JIRA's default groups
        userUtil.addToJiraUsePermission(user1);
        // then: the user has been added to the default group of the application role and not to any other group.
        User user = user1.getDirectoryUser();
        assertTrue("user is added in the default group", crowdService.isUserMemberOfGroup(user, softwareGroup));
        assertTrue("user is added in the default group", crowdService.isUserMemberOfGroup(user, otherGroup));
        assertFalse("User is not added in any other group", crowdService.isUserMemberOfGroup(user, usersGroup1));
    }

    @Test
    public void addToJiraUsePermissionAddsToDefaultGroupFailsWhenNoSeatsAvailable() throws Exception {
        // Given: there is a single application role
        // and ApplicationRole has a default group
        // and ApplicationRole does not have any more seats
        ApplicationRole applicationRoleSoftware = mock(ApplicationRole.class);
        final int NO_MORE_SEATS_AVAILABLE = 0;
        final String SOFTWARE_GROUP = "software.group";
        final Set<Group> softwareUserGroup = Collections.singleton(new MockGroup(SOFTWARE_GROUP));
        final HashSet<ApplicationRole> roles = new HashSet<>(1);
        roles.add(applicationRoleSoftware);
        when(applicationRoleManager.getRoles()).thenReturn(roles);
        when(applicationRoleManager.getUserCount(any(ApplicationKey.class))).thenReturn(3);
        when(applicationRoleManager.getRemainingSeats(any(ApplicationKey.class))).thenReturn(NO_MORE_SEATS_AVAILABLE);
        when(applicationRoleSoftware.getKey()).thenReturn(ApplicationKey.valueOf("software"));
        when(applicationRoleSoftware.getDefaultGroups()).thenReturn(softwareUserGroup);
        crowdService.addGroup(new MockGroup(SOFTWARE_GROUP));

        new MockComponentWorker().init()
                .addMock(UserUtil.class, userUtil)
                .addMock(LicenseCountService.class, licenseCountService)
                .addMock(GlobalPermissionManager.class, globalPermissionManager)
                .addMock(JiraLicenseService.class, jiraLicenseService);

        final Group usersGroup1 = new MockGroup("goupOne");
        crowdService.addGroup(usersGroup1);
        final Group softwareGroup = new MockGroup(SOFTWARE_GROUP);
        crowdService.addGroup(softwareGroup);
        final ApplicationUser user1 = new MockApplicationUser("bill");

        // when: trying to add the user to the default groups
        userUtil.addToJiraUsePermission(user1);

        // then: does not add the user to the role's primary group as the role has no more available seats
        User user = user1.getDirectoryUser();
        assertFalse("user is added in the primary group", crowdService.isUserMemberOfGroup(user, softwareGroup));
        assertFalse("User is not added in any other group", crowdService.isUserMemberOfGroup(user, usersGroup1));
    }

    @Test
    public void addToJiraUsePermissionFailsWhenNoDefaultGroupExists() throws Exception {
        // Given: there is a single application role
        // and ApplicationRole hasn't got any default group
        // and there are other groups in the directory
        ApplicationRole applicationRoleSoftware = mock(ApplicationRole.class);
        final String SOFTWARE_GROUP = "software.group";
        final HashSet<ApplicationRole> roles = new HashSet<>(1);
        roles.add(applicationRoleSoftware);
        when(applicationRoleManager.getRoles()).thenReturn(roles);
        when(applicationRoleManager.getUserCount(any(ApplicationKey.class))).thenReturn(3);
        when(applicationRoleManager.getRemainingSeats(any(ApplicationKey.class))).thenReturn(5);
        //no default group
        when(applicationRoleSoftware.getDefaultGroups()).thenReturn(Collections.emptySet());
        when(applicationRoleSoftware.getKey()).thenReturn(ApplicationKey.valueOf("software"));
        crowdService.addGroup(new MockGroup(SOFTWARE_GROUP));

        new MockComponentWorker().init()
                .addMock(UserUtil.class, userUtil)
                .addMock(LicenseCountService.class, licenseCountService)
                .addMock(GlobalPermissionManager.class, globalPermissionManager)
                .addMock(JiraLicenseService.class, jiraLicenseService);

        final Group usersGroup1 = new MockGroup("groupOne");
        crowdService.addGroup(usersGroup1);
        final Group softwareGroup = new MockGroup(SOFTWARE_GROUP);
        crowdService.addGroup(softwareGroup);
        final ApplicationUser user1 = new MockApplicationUser("bill");

        // when: adding another user to JIRA's default groups
        userUtil.addToJiraUsePermission(user1);
        // then: the user has not been added to any group
        User user = user1.getDirectoryUser();
        assertFalse("user is added in the primary group", crowdService.isUserMemberOfGroup(user, softwareGroup));
        assertFalse("User is not added in any other group", crowdService.isUserMemberOfGroup(user, usersGroup1));
    }

    @Test
    public void addToJiraUsePermissionNoGroupAssociationWhenNoPrimaryRole() throws Exception {
        // Given: No application role
        // and there are groups in the directory
        ApplicationRole applicationRoleSoftware = mock(ApplicationRole.class);
        final String SOFTWARE_GROUP = "software.group";
        final Set<Group> softwareUserGroup = Collections.singleton(new MockGroup(SOFTWARE_GROUP));
        final HashSet<ApplicationRole> roles = new HashSet<>(1);
        when(applicationRoleManager.getRoles()).thenReturn(roles);
        when(applicationRoleManager.getUserCount(any(ApplicationKey.class))).thenReturn(3);
        when(applicationRoleManager.getRemainingSeats(any(ApplicationKey.class))).thenReturn(5);
        when(applicationRoleSoftware.getDefaultGroups()).thenReturn(softwareUserGroup);
        crowdService.addGroup(new MockGroup(SOFTWARE_GROUP));

        new MockComponentWorker().init()
                .addMock(LicenseCountService.class, licenseCountService)
                .addMock(UserUtil.class, userUtil)
                .addMock(GlobalPermissionManager.class, globalPermissionManager)
                .addMock(JiraLicenseService.class, jiraLicenseService);

        final Group usersGroup1 = new MockGroup("groupOne");
        crowdService.addGroup(usersGroup1);
        final Group softwareGroup = new MockGroup(SOFTWARE_GROUP);
        crowdService.addGroup(softwareGroup);
        final ApplicationUser user1 = new MockApplicationUser("bill");

        // when: adding another user to JIRA's default groups
        userUtil.addToJiraUsePermission(user1);
        // then: the user has not been added to any group
        User user = user1.getDirectoryUser();
        assertFalse("user is added in the primary group", crowdService.isUserMemberOfGroup(user, softwareGroup));
        assertFalse("User is not added in any other group", crowdService.isUserMemberOfGroup(user, usersGroup1));
    }

    @Test
    public void getActiveUserCountTest() {
        when(licenseCountService.totalBillableUsers()).thenReturn(4);
        when(userManager.getTotalUserCount()).thenReturn(6);
        assertEquals(4, userUtil.getActiveUserCount());
    }

    @Test
    public void getTotalUserCountTest() {
        when(licenseCountService.totalBillableUsers()).thenReturn(4);
        when(userManager.getTotalUserCount()).thenReturn(6);
        assertEquals(6, userUtil.getTotalUserCount());
    }

    @Test
    public void clearActiveUserCountTest() {
        userUtil.clearActiveUserCount();
        verify(licenseCountService).flush();
    }
}