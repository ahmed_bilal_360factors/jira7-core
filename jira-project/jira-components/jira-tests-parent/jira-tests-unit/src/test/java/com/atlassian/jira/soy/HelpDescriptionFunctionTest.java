package com.atlassian.jira.soy;

import com.atlassian.jira.help.MockHelpUrl;
import com.atlassian.jira.help.MockHelpUrls;
import com.atlassian.soy.renderer.JsExpression;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

public class HelpDescriptionFunctionTest {

    private MockHelpUrls helpUrls;
    private AbstractHelpFunction abstractHelpFunction;

    @Before
    public void setUp() {
        helpUrls = new MockHelpUrls();
        helpUrls.addUrl(new MockHelpUrl().setDescription("sampleDesc").setKey("key1"));
        abstractHelpFunction = new HelpDescriptionFunction(helpUrls);
    }

    @Test
    public void testGenerateDescription() {
        final String urlForKey1 = abstractHelpFunction.generate(new JsExpression("'key1'")).getText();
        assertThat(urlForKey1, equalTo("\"sampleDesc\""));
    }

    @Test
    public void testApplyDescription() {
        final String titleForKey1 = abstractHelpFunction.apply("key1");
        assertThat(titleForKey1, equalTo("sampleDesc"));
    }

}
