package com.atlassian.jira.board;

import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.issue.status.Status;
import com.atlassian.jira.jql.context.AllProjectsContext;
import com.atlassian.jira.jql.context.IssueTypeContext;
import com.atlassian.jira.jql.context.ProjectContext;
import com.atlassian.jira.jql.context.QueryContext;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.collect.MapBuilder;
import com.atlassian.jira.workflow.JiraWorkflow;
import com.atlassian.jira.workflow.WorkflowManager;
import com.atlassian.jira.workflow.WorkflowScheme;
import com.atlassian.jira.workflow.WorkflowSchemeManager;
import com.atlassian.query.Query;
import com.opensymphony.workflow.loader.ActionDescriptor;
import com.opensymphony.workflow.loader.ResultDescriptor;
import com.opensymphony.workflow.loader.StepDescriptor;
import com.opensymphony.workflow.loader.WorkflowDescriptor;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.atlassian.collectors.CollectorsUtil.toImmutableList;
import static com.atlassian.collectors.CollectorsUtil.toImmutableSet;
import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Sets.newHashSet;
import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;
import static java.util.stream.IntStream.range;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestDefaultBoardWorkflowService {
    @Mock
    private SearchService searchService;

    @Mock
    private ProjectManager projectManager;

    @Mock
    private WorkflowManager workflowManager;

    @Mock
    private WorkflowSchemeManager workflowSchemeManager;

    @Mock
    private ApplicationUser user;

    @Mock
    private Query query;

    private DefaultBoardWorkflowService defaultBoardWorkflowService;

    @Before
    public void setup() {
        defaultBoardWorkflowService = new DefaultBoardWorkflowService(
                searchService,
                projectManager,
                workflowManager,
                workflowSchemeManager
        );
    }

    @Test
    public void shouldReturnStatusesFromAllActiveWorkflowsWhenQueryContextContainsAllProjectContext() {
        givenQueryContainsAllProjectContext(user, query);

        List<Status> expected = newArrayList(mock(Status.class), mock(Status.class), mock(Status.class));
        JiraWorkflow jiraWorkflow = givenAJiraWorkflowWithNameAndStatuses("a workflow", expected);
        activeWorkflowsAre(newArrayList(jiraWorkflow));

        Set<Status> statuses = defaultBoardWorkflowService.getAccessibleStatuses(user, query);

        assertThat(statuses, is(newHashSet(expected)));
    }

    @Test
    public void shouldReturnStatusesRelatedToAllWorkflowsForTheProjectWhenProjectIssueTypeContextsHasAnAllIssueTypeContext() {
        Long projectId = 1L;
        setupProjectWithId(projectId);
        givenQueryContainsAProjectWithAllIssueTypeContext(user, query, projectId);

        Status uniqueStatus1 = statusWithName("Unique Status 1");
        Status uniqueStatus2 = statusWithName("Unique Status 2");
        Status uniqueStatus3 = statusWithName("Unique Status 3");
        Status uniqueStatus4 = statusWithName("Unique Status 4");
        Status sharedStatus = statusWithName("Shared Status 1");
        givenAProjectWithWorkflows(projectId, MapBuilder.build(
                "Workflow 1", newHashSet(uniqueStatus1, uniqueStatus2, sharedStatus),
                "Workflow 2", newHashSet(uniqueStatus3, uniqueStatus4, sharedStatus)
        ));

        Set<Status> statuses = defaultBoardWorkflowService.getAccessibleStatuses(user, query);

        assertThat(statuses.size(), is(5));
        assertThat(statuses, hasItems(sharedStatus, uniqueStatus1, uniqueStatus2, uniqueStatus3, uniqueStatus4));
    }

    @Test
    public void shouldReturnRelevantStatuesWhenQueryContextDoesNotContainAnyWildcardContext() {
        setupProjectWithIdAndIssueTypes(1L, newHashSet("IssueType1", "IssueType2"));
        setupProjectWithIdAndIssueTypes(2L, newHashSet("IssueType2", "IssueType3"));
        setupProjectWithIdAndIssueTypes(3L, newHashSet("IssueType4", "IssueType5"));
        givenQueryContainsTheFollowingProjectIssueTypeAssociation(user, query, MapBuilder.build(
                1L, newHashSet("IssueType1", "IssueType2"),
                2L, newHashSet("IssueType2", "IssueType3"),
                3L, newHashSet("IssueType4", "IssueType5")
        ));

        Status uniqueStatus1 = statusWithName("Unique Status 1");
        Status uniqueStatus2 = statusWithName("Unique Status 2");
        Status uniqueStatus3 = statusWithName("Unique Status 3");
        Status uniqueStatus4 = statusWithName("Unique Status 4");

        JiraWorkflow workflow1 = givenAJiraWorkflowWithNameAndStatuses("Workflow 1", newArrayList(uniqueStatus1, uniqueStatus2));
        JiraWorkflow workflow2 = givenAJiraWorkflowWithNameAndStatuses("Workflow 2", newArrayList(uniqueStatus1, uniqueStatus3));
        JiraWorkflow workflow3 = givenAJiraWorkflowWithNameAndStatuses("Workflow 3", newArrayList(uniqueStatus2, uniqueStatus4));

        workflowsForIssueTypesAre(MapBuilder.build(
                workflow1, newHashSet("IssueType1"),
                workflow2, newHashSet("IssueType2", "IssueType5"),
                workflow3, newHashSet("IssueType3", "IssueType4")
        ));

        Set<Status> statuses = defaultBoardWorkflowService.getAccessibleStatuses(user, query);

        assertThat(statuses.size(), is(4));
        assertThat(statuses, hasItems(uniqueStatus1, uniqueStatus2, uniqueStatus3, uniqueStatus4));
    }

    @Test
    public void shouldReturnAnEmptyCollectionOfStatusesWhenThereAreNoActiveWorkflow() {
        activeWorkflowsAre(emptyList());

        Set<Status> allStatuses = defaultBoardWorkflowService.getAllActiveWorkflowStatuses();

        assertThat(allStatuses.isEmpty(), is(true));
    }

    @Test
    public void shouldReturnAllStatusesAssociatedWithActiveWorkflows() {
        Status uniqueStatus1 = statusWithName("Unique Status 1");
        Status uniqueStatus2 = statusWithName("Unique Status 2");
        Status uniqueStatus3 = statusWithName("Unique Status 3");
        Status uniqueStatus4 = statusWithName("Unique Status 4");

        JiraWorkflow workflow1 = givenAJiraWorkflowWithNameAndStatuses("Workflow 1", newArrayList(uniqueStatus1, uniqueStatus2));
        JiraWorkflow workflow2 = givenAJiraWorkflowWithNameAndStatuses("Workflow 2", newArrayList(uniqueStatus1, uniqueStatus3));
        JiraWorkflow workflow3 = givenAJiraWorkflowWithNameAndStatuses("Workflow 3", newArrayList(uniqueStatus2, uniqueStatus4));
        activeWorkflowsAre(newArrayList(workflow1, workflow2, workflow3));

        Set<Status> statuses = defaultBoardWorkflowService.getAllActiveWorkflowStatuses();

        assertThat(statuses.size(), is(4));
        assertThat(statuses, hasItems(uniqueStatus1, uniqueStatus2, uniqueStatus3, uniqueStatus4));
    }

    @Test
    public void returnsTheExpectedInitialStatusesGivenAQuery() {
        setupProjectWithIdAndIssueTypes(1L, newHashSet("IssueType1", "IssueType2"));
        setupProjectWithIdAndIssueTypes(2L, newHashSet("IssueType2", "IssueType3"));
        setupProjectWithIdAndIssueTypes(3L, newHashSet("IssueType4", "IssueType5"));
        givenQueryContainsTheFollowingProjectIssueTypeAssociation(user, query, MapBuilder.build(
                1L, newHashSet("IssueType1", "IssueType2"),
                2L, newHashSet("IssueType2", "IssueType3"),
                3L, newHashSet("IssueType4", "IssueType5")
        ));

        Status status1 = statusWithName("status");
        Status status2 = statusWithName("status");
        JiraWorkflow workflow = workflowWithInitialStatus(status1, status2);
        workflowsForIssueTypesAre(MapBuilder.build(
                workflow, newHashSet("IssueType1", "IssueType2", "IssueType3", "IssueType4", "IssueType5")
        ));

        Set<Status> initialStatuses = defaultBoardWorkflowService.getInitialStatusesForQuery(user, query);

        assertThat(initialStatuses, is(newHashSet(status1, status2)));
    }

    @Test
    public void shouldUseIssueTypesSpecifiedInTheQueryToGetTheWorkflows() {
        setupProjectWithIdAndIssueTypes(1L, newHashSet("IssueType1", "IssueType2"));
        setupProjectWithIdAndIssueTypes(2L, newHashSet("IssueType2", "IssueType3"));
        setupProjectWithIdAndIssueTypes(3L, newHashSet("IssueType4", "IssueType5"));
        setupProjectWithIdAndIssueTypes(4L, newHashSet("IssueType5", "IssueType6"));
        givenQueryContainsTheFollowingProjectIssueTypeAssociation(user, query, MapBuilder.build(
                1L, newHashSet("IssueType1", "IssueType2"),
                2L, newHashSet("IssueType2", "IssueType3"),
                3L, newHashSet("IssueType4", "IssueType5"),
                4L, newHashSet("IssueType5", "IssueType6")
        ));
        JiraWorkflow workflow1 = mock(JiraWorkflow.class);
        JiraWorkflow workflow2 = mock(JiraWorkflow.class);
        workflowsForIssueTypesAre(MapBuilder.build(
                workflow1, newHashSet("IssueType1", "IssueType2", "IssueType3"),
                workflow2, newHashSet("IssueType4", "IssueType5", "IssueType6")
        ));

        Collection<JiraWorkflow> workflowsGivenQuery = defaultBoardWorkflowService.getWorkflowsForQuery(user, query);

        assertThat(workflowsGivenQuery, containsInAnyOrder(workflow1, workflow2));
    }

    @Test
    public void shouldExcludeIssueTypesSpecifiedInTheQueryButNotInTheProjectToGetTheWorkflows() {
        setupProjectWithIdAndIssueTypes(1L, newHashSet("IssueType1", "IssueType2"));
        givenQueryContainsTheFollowingProjectIssueTypeAssociation(user, query, MapBuilder.build(
                1L, newHashSet("IssueType1", "IssueType2", "IssueType3")
        ));
        JiraWorkflow workflow1 = mock(JiraWorkflow.class);
        JiraWorkflow workflow2 = mock(JiraWorkflow.class);
        JiraWorkflow workflow3 = mock(JiraWorkflow.class);
        workflowsForIssueTypesAre(MapBuilder.build(
                workflow1, newHashSet("IssueType1"),
                workflow2, newHashSet("IssueType2"),
                workflow3, newHashSet("IssueType3")
        ));

        Collection<JiraWorkflow> workflowsGivenQuery = defaultBoardWorkflowService.getWorkflowsForQuery(user, query);

        assertThat(workflowsGivenQuery, containsInAnyOrder(workflow1, workflow2));
    }

    @Test
    public void shouldUseProjectIssueTypesToGetWorkflowsIfNoIssueTypeSpecifiedInTheQuery() throws Exception {
        long projectId = 1L;
        setupProjectWithId(projectId);
        givenQueryContainsTheFollowingProjectIssueTypeAssociation(user, query, MapBuilder.build(
                projectId, newHashSet()));
        Set<JiraWorkflow> expectedWorkflows = givenAProjectWithWorkflows(projectId, MapBuilder.build(
                "test workflow", newHashSet()
        ));

        Collection<JiraWorkflow> actaulWorkflows = defaultBoardWorkflowService.getWorkflowsForQuery(user, query);

        assertEquals(actaulWorkflows, expectedWorkflows);
    }

    private void givenQueryContainsAllProjectContext(ApplicationUser user, Query query) {
        List<QueryContext.ProjectIssueTypeContexts> projectIssueTypeContexts = newArrayList();
        projectIssueTypeContexts.add(new QueryContext.ProjectIssueTypeContexts(AllProjectsContext.INSTANCE, Collections.emptyList()));

        QueryContext queryContext = mock(QueryContext.class);
        when(queryContext.getProjectIssueTypeContexts()).thenReturn(projectIssueTypeContexts);

        when(searchService.getQueryContext(user, query)).thenReturn(queryContext);
    }

    private void givenQueryContainsTheFollowingProjectIssueTypeAssociation(ApplicationUser user, Query query, Map<Long, Set<String>> projectIssueTypeAssoc) {
        List<QueryContext.ProjectIssueTypeContexts> projectIssueTypeContexts = newArrayList();
        for (Map.Entry<Long, Set<String>> ent : projectIssueTypeAssoc.entrySet()) {
            List<IssueTypeContext> issueTypeContexts = newArrayList();
            issueTypeContexts.addAll(
                    ent.getValue().stream().map(this::givenAnIssueType).collect(toList())
            );

            QueryContext.ProjectIssueTypeContexts elem = new QueryContext.ProjectIssueTypeContexts(givenAProjectContextWithId(ent.getKey()), issueTypeContexts);
            projectIssueTypeContexts.add(elem);
        }

        QueryContext queryContext = mock(QueryContext.class);
        when(queryContext.getProjectIssueTypeContexts()).thenReturn(projectIssueTypeContexts);

        when(searchService.getQueryContext(user, query)).thenReturn(queryContext);
    }

    private JiraWorkflow givenAJiraWorkflowWithNameAndStatuses(String workflowName, List<Status> workflowStatuses) {
        JiraWorkflow jiraWorkflow = mock(JiraWorkflow.class);
        when(jiraWorkflow.getName()).thenReturn(workflowName);
        when(jiraWorkflow.getLinkedStatusObjects()).thenReturn(workflowStatuses);
        return jiraWorkflow;
    }

    private Status statusWithName(String statusName) {
        Status status = mock(Status.class);
        when(status.getName()).thenReturn(statusName);
        return status;
    }

    private Project setupProjectWithId(Long projectId) {
        return setupProjectWithIdAndIssueTypes(projectId, Collections.emptySet());
    }

    private Project setupProjectWithIdAndIssueTypes(Long projectId, Set<String> issueTypeIds) {
        Project project = mock(Project.class);
        Collection<IssueType> issueTypes = issueTypeIds
                .stream()
                .map(issueTypeId -> {
                    IssueType issueType = mock(IssueType.class);
                    when(issueType.getId()).thenReturn(issueTypeId);
                    return issueType;
                })
                .collect(toImmutableSet());
        when(project.getId()).thenReturn(projectId);
        when(project.getIssueTypes()).thenReturn(issueTypes);
        when(projectManager.getProjectObj(projectId)).thenReturn(project);
        return project;
    }

    private IssueTypeContext givenAnIssueType(String issueTypeId) {
        IssueTypeContext issueTypeContext = mock(IssueTypeContext.class);
        when(issueTypeContext.getIssueTypeId()).thenReturn(issueTypeId);
        when(issueTypeContext.isAll()).thenReturn(false);
        return issueTypeContext;
    }

    private ProjectContext givenAProjectContextWithId(Long projectId) {
        ProjectContext projectContext = mock(ProjectContext.class);
        when(projectContext.getProjectId()).thenReturn(projectId);
        when(projectContext.isAll()).thenReturn(false);
        return projectContext;
    }

    private void givenQueryContainsAProjectWithAllIssueTypeContext(ApplicationUser user, Query query, Long projectId) {
        QueryContext queryContext = mock(QueryContext.class);
        ProjectContext projectContext = givenAProjectContextWithId(projectId);
        when(queryContext.getProjectIssueTypeContexts()).thenReturn(Arrays.asList(new QueryContext.ProjectIssueTypeContexts(projectContext, Collections.emptyList())));
        when(searchService.getQueryContext(user, query)).thenReturn(queryContext);
    }

    private Set<JiraWorkflow> givenAProjectWithWorkflows(Long projectId, Map<String, Set<Status>> workflows) {
        Project project = projectManager.getProjectObj(projectId);
        Set<IssueType> issueTypes = newHashSet();

        Set<JiraWorkflow> jiraWorkflows = newHashSet();
        for (Map.Entry<String, Set<Status>> ent : workflows.entrySet()) {
            JiraWorkflow jiraWorkflow = givenAJiraWorkflowWithNameAndStatuses(ent.getKey(), newArrayList(ent.getValue()));
            jiraWorkflows.add(jiraWorkflow);
            IssueType issueType = mock(IssueType.class);
            when(issueType.getId()).thenReturn(ent.getKey());
            when(workflowManager.getWorkflowFromScheme(any(WorkflowScheme.class), eq(ent.getKey()))).thenReturn(jiraWorkflow);
            issueTypes.add(issueType);
        }
        when(project.getIssueTypes()).thenReturn(issueTypes);
        return jiraWorkflows;
    }

    private void activeWorkflowsAre(Collection<JiraWorkflow> workflows) {
        when(workflowManager.getActiveWorkflows()).thenReturn(workflows);
    }

    private void workflowsForIssueTypesAre(Map<JiraWorkflow, Set<String>> workflowsAndIssueTypeMapping) {
        workflowsAndIssueTypeMapping.entrySet()
                .stream()
                .forEach(entry -> {
                    JiraWorkflow workflow = entry.getKey();
                    Set<String> issueTypeIds = entry.getValue();

                    issueTypeIds.stream().forEach(issueTypeId -> workflowForIssueTypeIs(workflow, issueTypeId));
                });
    }

    private void workflowForIssueTypeIs(JiraWorkflow workflow, String issueType) {
        when(workflowManager.getWorkflowFromScheme(any(WorkflowScheme.class), eq(issueType))).thenReturn(workflow);
    }

    private JiraWorkflow workflowWithInitialStatus(Status... statuses) {
        WorkflowDescriptor workflowDescriptor = mock(WorkflowDescriptor.class);

        JiraWorkflow workflow = mock(JiraWorkflow.class);
        when(workflow.getDescriptor()).thenReturn(workflowDescriptor);

        List<ActionDescriptor> initialActions = actionDescriptorsForStatusesInWorkflow(workflowDescriptor, workflow, statuses);
        when(workflowDescriptor.getInitialActions()).thenReturn(initialActions);

        return workflow;
    }

    private List<ActionDescriptor> actionDescriptorsForStatusesInWorkflow(WorkflowDescriptor workflowDescriptor, JiraWorkflow workflow, Status... statuses) {
        return range(0, statuses.length)
                .mapToObj(index -> {
                    ActionDescriptor actionDescriptor = actionDescriptorWithStep(index);

                    StepDescriptor stepDescriptor = mock(StepDescriptor.class);
                    when(workflowDescriptor.getStep(index)).thenReturn(stepDescriptor);
                    when(workflow.getLinkedStatus(stepDescriptor)).thenReturn(statuses[index]);

                    return actionDescriptor;
                })
                .collect(toImmutableList());
    }

    private ResultDescriptor resultDescriptorWithStep(int step) {
        ResultDescriptor resultDescriptor = mock(ResultDescriptor.class);
        when(resultDescriptor.getStep()).thenReturn(step);

        return resultDescriptor;
    }

    private ActionDescriptor actionDescriptorWithStep(int step) {
        ResultDescriptor resultDescriptor = resultDescriptorWithStep(step);
        ActionDescriptor actionDescriptor = mock(ActionDescriptor.class);
        when(actionDescriptor.getUnconditionalResult()).thenReturn(resultDescriptor);

        return actionDescriptor;
    }
}
