package com.atlassian.jira.upgrade.tasks;

import com.atlassian.jira.mock.ofbiz.MockGenericValue;
import com.atlassian.jira.mock.ofbiz.MockOfBizDelegator;
import com.atlassian.jira.portal.CachingPortletConfigurationStore;
import com.atlassian.jira.portal.OfbizPortletConfigurationStore;
import com.google.common.collect.ImmutableMap;
import org.junit.Before;
import org.junit.Test;
import org.ofbiz.core.entity.GenericValue;

import static java.util.Arrays.asList;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

public class UpgradeTask_Build70007Test {
    private final String INTRO_GADGET_URI = "rest/gadgets/1.0/g/com.atlassian.jira.gadgets:introduction-gadget/gadgets/introduction-gadget.xml";
    private static final String INTRO_DASHBOARD_ITEM_KEY = "com.atlassian.jira.gadgets:introduction-dashboard-item";

    private final GenericValue SOME_GADGET = new MockGenericValue(OfbizPortletConfigurationStore.TABLE,
            ImmutableMap.of(OfbizPortletConfigurationStore.Columns.GADGET_XML, "some-gadget.xml",
                    OfbizPortletConfigurationStore.Columns.ID, 1L));

    private final GenericValue INTRO_GADGET = new MockGenericValue(OfbizPortletConfigurationStore.TABLE,
            ImmutableMap.of(OfbizPortletConfigurationStore.Columns.GADGET_XML, INTRO_GADGET_URI,
                    OfbizPortletConfigurationStore.Columns.ID, 2L));

    private final GenericValue INTRO_DASHBOARD_ITEM = new MockGenericValue(OfbizPortletConfigurationStore.TABLE,
            ImmutableMap.of(OfbizPortletConfigurationStore.Columns.MODULE_KEY, INTRO_DASHBOARD_ITEM_KEY,
                    OfbizPortletConfigurationStore.Columns.GADGET_XML, INTRO_GADGET_URI,
                    OfbizPortletConfigurationStore.Columns.ID, 2L));


    private MockOfBizDelegator mockOfBizDelegator;
    private UpgradeTask_Build70007 upgradeTask;
    private CachingPortletConfigurationStore cachingPortletConfigurationStore;

    @Before
    public void setup() {
        cachingPortletConfigurationStore = mock(CachingPortletConfigurationStore.class);
        mockOfBizDelegator = new MockOfBizDelegator();
        mockOfBizDelegator.setGenericValues(asList(SOME_GADGET, INTRO_GADGET));
        upgradeTask = new UpgradeTask_Build70007(mockOfBizDelegator, cachingPortletConfigurationStore);
    }

    @Test
    public void convertsAdminGadgetIntoDashboardItemAndFlushesPortletConfigurationStore() throws Exception {
        upgradeTask.doUpgrade(false);

        mockOfBizDelegator.verifyAll(SOME_GADGET, INTRO_DASHBOARD_ITEM);
        verify(cachingPortletConfigurationStore).flush();
        verifyNoMoreInteractions(cachingPortletConfigurationStore);
    }
}