package com.atlassian.jira.project.util;

import com.atlassian.crowd.exception.InvalidCredentialException;
import com.atlassian.crowd.exception.InvalidUserException;
import com.atlassian.crowd.exception.OperationNotPermittedException;
import com.atlassian.jira.bc.project.version.VersionBuilderImpl;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationPropertiesImpl;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.issue.issuetype.MockIssueType;
import com.atlassian.jira.mock.component.MockComponentWorker;
import com.atlassian.jira.mock.ofbiz.MockGenericValue;
import com.atlassian.jira.mock.project.MockVersion;
import com.atlassian.jira.ofbiz.FieldMap;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.template.VelocityTemplatingEngine;
import com.atlassian.jira.template.mocks.VelocityTemplatingEngineMocks;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.google.common.collect.ImmutableList;
import org.junit.Before;
import org.junit.Test;
import org.mockito.stubbing.OngoingStubbing;

import java.util.Collection;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


public class TestReleaseNoteManager {
    public TestReleaseNoteManager() {
        ComponentAccessor.initialiseWorker(new MockComponentWorker());
    }

    Project project;

    @Before
    public void setUp() {
        this.project = new MockProject(0L, "ABC");
    }

    @Test
    public void testGetReleaseNoteStyles() {
        final Map expectedStyles = FieldMap.build("text", "text-template", "html", "html-template");
        final String releaseNoteName = "text, html";
        final String releaseNoteTemplate = "text-template, html-template";
        ApplicationPropertiesImpl applicationProperties = new MyApplicationProperties(releaseNoteName, releaseNoteTemplate);
        ReleaseNoteManager releaseNoteManager = new ReleaseNoteManager(applicationProperties, null, null, null, null);
        assertEquals(expectedStyles, releaseNoteManager.getStyles());
    }

    @Test
    public void testGetReleaseNoteStylesHandlesNulls() {
        ApplicationPropertiesImpl applicationProperties = new MyApplicationProperties(null, null);
        ReleaseNoteManager releaseNoteManager = new ReleaseNoteManager(applicationProperties, null, null, null, null);
        releaseNoteManager.getStyles(); // just check no exceptions
    }

    @Test
    public void testGetReleaseNoteStylesWithCorruptedProperties() {
        ApplicationPropertiesImpl applicationProperties = new MyApplicationProperties("text", null);
        ReleaseNoteManager releaseNoteManager = new ReleaseNoteManager(applicationProperties, null, null, null, null);
        try {
            releaseNoteManager.getStyles();
        } catch (RuntimeException re) {
            assertNotNull(re.getMessage());
        }
    }

    @Test
    public void testGetReleaseNoteWithInvalidStyleName()
            throws OperationNotPermittedException, InvalidUserException, InvalidCredentialException {
        final ApplicationUser testUser = new MockApplicationUser("testuser");
        final Version version = new MockVersion(new MockGenericValue("Version", FieldMap.build("name", "Version 1", "id", (long) 1001, "sequence", (long) 1, "project", (long) 101, "released", "true", "archived", "true")));

        final ApplicationPropertiesImpl applicationProperties = new MyApplicationProperties("text", "text-template", "text");
        final VelocityTemplatingEngine templatingEngine = VelocityTemplatingEngineMocks.alwaysOutput("BODY").get();
        final ConstantsManager constantsManager = mock(ConstantsManager.class);
        returnTwoIssueTypeGvs(constantsManager);

        final ReleaseNoteManager releaseNoteManager = new ReleaseNoteManager(applicationProperties, templatingEngine, constantsManager, null, null);
        assertEquals("BODY", releaseNoteManager.getReleaseNote(null, "xml", version, testUser, project));
    }

    @Test
    public void testGetReleaseNoteWithInvalidStyleNameAndInvalidDefault()
            throws OperationNotPermittedException, InvalidUserException, InvalidCredentialException {
        final ApplicationUser testUser = new MockApplicationUser("testuser");
        final Version version = new VersionBuilderImpl(
                new MockVersion(new MockGenericValue("Version", FieldMap.build("name", "Version 1", "id", (long) 1001, "sequence", (long) 1, "project", (long) 101, "released", "true", "archived", "true"))))
                .name("Ver 1")
                .sequence(1L).build();

        final ApplicationPropertiesImpl applicationProperties = new MyApplicationProperties("text", "text-template", "nicks");
        final VelocityTemplatingEngine templatingEngine = VelocityTemplatingEngineMocks.alwaysOutput("BODY").get();
        final ConstantsManager constantsManager = mock(ConstantsManager.class);
        returnTwoIssueTypeGvs(constantsManager);

        final ReleaseNoteManager releaseNoteManager = new ReleaseNoteManager(applicationProperties, templatingEngine, constantsManager, null, null);
        assertEquals("BODY", releaseNoteManager.getReleaseNote(null, "xml", version, testUser, project));
    }

    @Test
    public void testGetReleaseNote()
            throws OperationNotPermittedException, InvalidUserException, InvalidCredentialException {
        final ApplicationUser testUser = new MockApplicationUser("testuser");
        final Version version = new MockVersion(new MockGenericValue("Version", FieldMap.build("name", "Version 1", "id", (long) 1001, "sequence", (long) 1, "project", (long) 101, "released", "true", "archived", "true")));

        final ApplicationPropertiesImpl applicationProperties = new MyApplicationProperties("text", "text-template");
        final VelocityTemplatingEngine templatingEngine = VelocityTemplatingEngineMocks.alwaysOutput("BODY").get();
        final ConstantsManager constantsManager = mock(ConstantsManager.class);
        returnTwoIssueTypeGvs(constantsManager);


        final ReleaseNoteManager releaseNoteManager = new ReleaseNoteManager(applicationProperties, templatingEngine, constantsManager, null, null);
        assertEquals("BODY", releaseNoteManager.getReleaseNote(null, "text", version, testUser, project));
    }

    private OngoingStubbing<Collection<IssueType>> returnTwoIssueTypeGvs(ConstantsManager constantsManager) {
        return when(constantsManager.getRegularIssueTypeObjects()).thenReturn
                (
                        ImmutableList.<IssueType>of
                                (
                                        new MockIssueType
                                                (
                                                        "100", "testtype",
                                                        "test issue type"
                                                ),
                                        new MockIssueType
                                                (
                                                        "200", "another testtype",
                                                        "another test issue type"
                                                )
                                )
                );
    }

    private static class MyApplicationProperties extends ApplicationPropertiesImpl {
        private final String changeLogName;
        private final String changeLogTemplate;
        private final String defaultTemplate;

        public MyApplicationProperties(String changeLogName, String changeLogTemplate) {
            super(null);
            this.changeLogName = changeLogName;
            this.changeLogTemplate = changeLogTemplate;
            defaultTemplate = null;
        }

        public MyApplicationProperties(String changeLogName, String changeLogTemplate, String defaultTemplate) {
            super(null);
            this.changeLogName = changeLogName;
            this.changeLogTemplate = changeLogTemplate;
            this.defaultTemplate = defaultTemplate;
        }

        @Override
        public String getDefaultBackedString(String name) {
            if (ReleaseNoteManager.RELEASE_NOTE_NAME.equals(name)) {
                return changeLogName;
            } else if (ReleaseNoteManager.RELEASE_NOTE_TEMPLATE.equals(name)) {
                return changeLogTemplate;
            } else if (ReleaseNoteManager.RELEASE_NOTE_DEFAULT.equals(name)) {
                return defaultTemplate;
            } else {
                return null;
            }
        }

        @Override
        public String getString(String name) {
            if (APKeys.JIRA_BASEURL.equals(name)) {
                return "jira";
            } else {
                return null;
            }
        }
    }
}
