package com.atlassian.jira.project;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.association.NodeAssociationStore;
import com.atlassian.jira.avatar.AvatarManager;
import com.atlassian.jira.bc.project.ProjectCreationData;
import com.atlassian.jira.bc.project.ProjectTypeValidator;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.config.properties.PropertiesManager;
import com.atlassian.jira.database.DbConnectionManager;
import com.atlassian.jira.entity.property.JsonEntityPropertyManager;
import com.atlassian.jira.exception.DataAccessException;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.ofbiz.PrimitiveMap;
import com.atlassian.jira.project.type.ProjectTypeKey;
import com.atlassian.jira.project.util.ProjectKeyStore;
import com.atlassian.jira.security.roles.ProjectRoleManager;
import com.atlassian.jira.transaction.Transaction;
import com.atlassian.jira.transaction.TransactionSupport;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Map;

import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyMap;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestDefaultProjectManagerProjectCreation {
    private static final String ANY_NAME = "";
    private static final String ANY_KEY = "";
    private static final String ANY_DESC = "";
    private static final ApplicationUser ANY_LEAD = new MockApplicationUser("");
    private static final String ANY_URL = "";
    private static final Long ANY_ASSIGNEE = 1L;
    private static final Long ANY_AVATAR = 2L;
    private static final ApplicationUser USER = new MockApplicationUser("user");

    @Mock
    private TransactionSupport transactionSupport;
    @Mock
    private OfBizDelegator ofBizDelegator;
    @Mock
    private ProjectKeyStore projectKeyStore;
    @Mock
    private ProjectTypeValidator projectTypeValidator;

    private DefaultProjectManager projectManager;

    @Before
    public void setUp() {
        projectManager = newProjectManager(transactionSupport, ofBizDelegator, projectKeyStore, projectTypeValidator);
    }

    @Test
    public void createProjectRefreshesTheProjectKeyStoreAfterTheTransactionHasBeenCommitted() {
        Transaction transaction = mock(Transaction.class);
        when(transactionSupport.begin()).thenReturn(transaction);
        when(projectTypeValidator.isValid(any(ApplicationUser.class), any(ProjectTypeKey.class))).thenReturn(true);

        createProject();
        InOrder inOrder = inOrder(transaction, projectKeyStore);

        inOrder.verify(transaction).commit();
        inOrder.verify(projectKeyStore).refresh();
    }

    @Test
    public void createProjectRefreshesTheCacheEvenIfAnExceptionIsThrownAndTheTransactionIsNotCommitted() {
        Transaction transaction = mock(Transaction.class);
        when(transactionSupport.begin()).thenReturn(transaction);
        when(ofBizDelegator.createValue(anyString(), anyMap())).thenThrow(new DataAccessException("any message"));
        when(projectTypeValidator.isValid(any(ApplicationUser.class), any(ProjectTypeKey.class))).thenReturn(true);

        try {
            createProject();
            fail("a DataAccessException was supposed to be thrown");
        } catch (DataAccessException e) {
            // do nothing, the exception is the one thrown by our mock
        }

        verify(transaction, never()).commit();
        verify(projectKeyStore).refresh();
    }

    @Test
    public void projectTypeIsPassedToTheOfBizDelegator() {
        initTransaction();
        ProjectTypeKey type = new ProjectTypeKey("type");
        when(projectTypeValidator.isValid(USER, type)).thenReturn(true);

        createProjectWithTypeOf(USER, type);

        verify(ofBizDelegator).createValue("Project", parameterMapWithProjectTypeOf(type.getKey()));
    }

    @Test(expected = IllegalStateException.class)
    public void projectTypeMustBeValid() {
        Transaction transaction = mock(Transaction.class);
        when(transactionSupport.begin()).thenReturn(transaction);

        ProjectTypeKey type = new ProjectTypeKey("type");
        when(projectTypeValidator.isValid(USER, type)).thenReturn(false);

        createProjectWithTypeOf(USER, type);
    }

    private void initTransaction() {
        Transaction transaction = mock(Transaction.class);
        when(transactionSupport.begin()).thenReturn(transaction);
    }

    private Map<String, Object> parameterMapWithProjectTypeOf(final String projectType) {
        return new PrimitiveMap.Builder()
                .add("key", ANY_KEY)
                .add("originalkey", ANY_KEY)
                .add("name", ANY_NAME)
                .add("url", ANY_URL)
                .add("lead", ANY_LEAD.getKey())
                .add("description", ANY_DESC)
                .add("counter", 0L)
                .add("assigneetype", ANY_ASSIGNEE)
                .add("avatar", ANY_AVATAR)
                .add("projecttype", projectType)
                .toMap();
    }

    private void createProjectWithTypeOf(ApplicationUser user, ProjectTypeKey type) {
        projectManager.createProject(
                user,
                new ProjectCreationData.Builder()
                        .withName(ANY_NAME)
                        .withKey(ANY_KEY)
                        .withDescription(ANY_DESC)
                        .withType(type)
                        .withLead(ANY_LEAD)
                        .withUrl(ANY_URL)
                        .withAssigneeType(ANY_ASSIGNEE)
                        .withAvatarId(ANY_AVATAR)
                        .build()
        );
    }

    private void createProject() {
        createProjectWithTypeOf(USER, new ProjectTypeKey("any-type"));
    }

    private DefaultProjectManager newProjectManager(
            final TransactionSupport transactionSupport,
            final OfBizDelegator ofBizDelegator,
            final ProjectKeyStore projectKeyStore,
            final ProjectTypeValidator projectTypeValidator) {
        return new DefaultProjectManager(
                ofBizDelegator,
                mock(DbConnectionManager.class),
                mock(NodeAssociationStore.class),
                mock(ProjectFactory.class),
                mock(ProjectRoleManager.class),
                mock(IssueManager.class),
                mock(UserManager.class),
                mock(ProjectCategoryStore.class),
                mock(ApplicationProperties.class),
                projectKeyStore,
                transactionSupport,
                mock(PropertiesManager.class),
                mock(JsonEntityPropertyManager.class),
                mock(EventPublisher.class),
                projectTypeValidator,
                mock(AvatarManager.class)
        );
    }
}
