package com.atlassian.jira.web.action.admin.workflow.tabs;

import com.google.common.collect.ImmutableList;
import com.opensymphony.workflow.loader.ActionDescriptor;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;
import java.util.Map;

import static com.atlassian.jira.web.action.admin.workflow.tabs.WorkflowTransitionContextUtils.TRANSITION_KEY;
import static java.util.Collections.emptyList;
import static java.util.Collections.emptyMap;
import static java.util.Collections.singletonMap;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestWorkflowTransitionValidatorsContextProvider {

    @Mock private ActionDescriptor transition;

    private WorkflowTransitionValidatorsContextProvider contextProvider;

    private Map<String, Object> context;

    @Before
    public void setUp() {
        contextProvider = new WorkflowTransitionValidatorsContextProvider();
        context = singletonMap(TRANSITION_KEY, transition);
    }

    @Test
    public void countShouldBeZeroWhenNoTransitionInContext() {
        // Invoke
        final int count = contextProvider.getCount(emptyMap());

        // Check
        assertThat(count, equalTo(0));
    }

    @Test
    public void countShouldBeZeroWhenTransitionHasNullListOfValidators() {
        // Set up
        when(transition.getValidators()).thenReturn(null);

        // Invoke
        final int count = contextProvider.getCount(context);

        // Check
        assertThat(count, equalTo(0));
    }

    @Test
    public void countShouldBeZeroWhenTransitionHasEmptyListOfValidators() {
        // Set up
        when(transition.getValidators()).thenReturn(emptyList());

        // Invoke
        final int count = contextProvider.getCount(context);

        // Check
        assertThat(count, equalTo(0));
    }

    @Test
    public void countShouldBeEqualToTransitionsNumberOfValidators() {
        // Set up
        final List<?> validators = ImmutableList.of(new Object(), new Object());
        when(transition.getValidators()).thenReturn(validators);

        // Invoke
        final int count = contextProvider.getCount(context);

        // Check
        assertThat(count, equalTo(2));
    }
}
