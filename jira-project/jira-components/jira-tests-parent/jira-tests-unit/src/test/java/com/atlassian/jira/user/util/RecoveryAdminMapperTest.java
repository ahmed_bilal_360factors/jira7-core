package com.atlassian.jira.user.util;

import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

public class RecoveryAdminMapperTest {
    @Rule
    public MockitoContainer container = MockitoMocksInContainer.rule(this);

    @Mock
    @AvailableInContainer
    private UserKeyStore store;

    private final RecoveryAdminMapper mapper = new RecoveryAdminMapper();

    @Test
    public void noMappingSetWhenNotInRecoveryMode() {
        //given
        addMockRecoveryMode(new DisabledRecoveryMode());

        //when
        mapper.map();

        //then
        verifyZeroInteractions(store);
    }

    @Test
    public void noMappingSetWhenItAlreadyExists() {
        //given
        final String RECOVERY_USER = "recovery_user";
        addMockRecoveryMode(new StaticRecoveryMode(RECOVERY_USER));
        when(store.getKeyForUsername(RECOVERY_USER)).thenReturn("something");

        //when
        mapper.map();

        //then
        verify(store, never()).ensureUniqueKeyForNewUser(any(String.class));
    }

    @Test
    public void mappingSetWhenNeeded() {
        //given
        final String RECOVERY_USER = "recovery_user";
        addMockRecoveryMode(new StaticRecoveryMode(RECOVERY_USER));
        when(store.getKeyForUsername(RECOVERY_USER)).thenReturn(null);
        when(store.ensureUniqueKeyForNewUser(RECOVERY_USER)).thenReturn("iAmUnique");

        //when
        mapper.map();

        //then
        verify(store).ensureUniqueKeyForNewUser("recovery_user");
    }

    private void addMockRecoveryMode(RecoveryMode mode) {
        container.getMockComponentContainer().addMock(RecoveryMode.class, mode);
    }
}