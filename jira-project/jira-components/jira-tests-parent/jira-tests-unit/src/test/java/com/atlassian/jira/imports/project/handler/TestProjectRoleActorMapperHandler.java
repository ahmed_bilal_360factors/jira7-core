package com.atlassian.jira.imports.project.handler;

import com.atlassian.jira.exception.ParseException;
import com.atlassian.jira.external.beans.ExternalProject;
import com.atlassian.jira.external.beans.ExternalProjectRoleActor;
import com.atlassian.jira.imports.project.core.BackupProject;
import com.atlassian.jira.imports.project.core.BackupProjectImpl;
import com.atlassian.jira.imports.project.mapper.ProjectRoleActorMapper;
import com.atlassian.jira.imports.project.parser.ProjectRoleActorParser;
import com.atlassian.jira.junit.rules.InitMockitoMocks;
import com.google.common.collect.ImmutableMap;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.mockito.Mock;

import static com.atlassian.jira.imports.project.parser.ProjectRoleActorParser.PROJECT_ROLE_ACTOR_ENTITY_NAME;
import static java.util.Collections.emptyList;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

/**
 * @since v3.13
 */
public class TestProjectRoleActorMapperHandler {
    @Rule
    public TestRule initMockitoMocks = new InitMockitoMocks(this);

    @Mock
    private ProjectRoleActorParser mockProjectRoleActorParser;
    @Mock
    private ProjectRoleActorMapper mockProjectRoleActorMapper;

    @Test
    public void testHandle() throws ParseException {
        final ExternalProjectRoleActor externalProjectRoleActor = new ExternalProjectRoleActor("123", "12", "3434", "role:type", "fred");

        when(mockProjectRoleActorParser.parse(null)).thenReturn(externalProjectRoleActor);

        final ExternalProject project = new ExternalProject();
        project.setId("12");
        BackupProject backupProject = new BackupProjectImpl(project, emptyList(), emptyList(), emptyList(), emptyList(), 0, ImmutableMap.of());

        ProjectRoleActorMapperHandler projectRoleActorMapperHandler = new ProjectRoleActorMapperHandler(backupProject, mockProjectRoleActorMapper) {
            ProjectRoleActorParser getProjectRoleActorParser() {
                return mockProjectRoleActorParser;
            }
        };

        projectRoleActorMapperHandler.handleEntity(PROJECT_ROLE_ACTOR_ENTITY_NAME, null);

        verify(mockProjectRoleActorMapper).flagValueActorAsInUse(externalProjectRoleActor);
    }

    @Test
    public void testProjectRoleNullProject() throws ParseException {
        final ExternalProjectRoleActor externalProjectRoleActor = new ExternalProjectRoleActor("123", null, "3434", "role:type", "fred");

        when(mockProjectRoleActorParser.parse(null)).thenReturn(externalProjectRoleActor);

        final ExternalProject project = new ExternalProject();
        project.setId("12");
        BackupProject backupProject = new BackupProjectImpl(project, emptyList(), emptyList(), emptyList(), emptyList(), 0, ImmutableMap.of());

        ProjectRoleActorMapperHandler projectRoleActorMapperHandler = new ProjectRoleActorMapperHandler(backupProject, mockProjectRoleActorMapper) {
            ProjectRoleActorParser getProjectRoleActorParser() {
                return mockProjectRoleActorParser;
            }
        };

        projectRoleActorMapperHandler.handleEntity(PROJECT_ROLE_ACTOR_ENTITY_NAME, null);
        verifyZeroInteractions(mockProjectRoleActorMapper);
    }

    @Test
    public void testProjectRoleWrongEntity() throws ParseException {
        ProjectRoleActorMapperHandler projectRoleActorMapperHandler = new ProjectRoleActorMapperHandler(null, null);
        projectRoleActorMapperHandler.handleEntity("BSENTITY", null);
        verifyZeroInteractions(mockProjectRoleActorMapper);
    }
}
