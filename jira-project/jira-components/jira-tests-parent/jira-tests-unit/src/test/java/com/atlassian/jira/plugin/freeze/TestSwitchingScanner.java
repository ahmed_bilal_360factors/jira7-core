package com.atlassian.jira.plugin.freeze;

import com.atlassian.plugin.loaders.classloading.DeploymentUnit;
import com.atlassian.plugin.loaders.classloading.Scanner;
import com.google.common.collect.ImmutableList;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.File;
import java.util.Collection;
import java.util.List;

import static java.util.Collections.emptyList;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

/**
 */
@RunWith(MockitoJUnitRunner.class)
public class TestSwitchingScanner {

    @Mock
    private Scanner defaultScanner;
    @Mock
    private Scanner otherScanner;
    private DeploymentUnit unit1 = new DeploymentUnit(new File("a"));
    private DeploymentUnit unit2 = new DeploymentUnit(new File("b"));
    private DeploymentUnit unit3 = new DeploymentUnit(new File("c"));

    private SwitchingScanner scanner;

    @Before
    public void setUp() {
        scanner = new SwitchingScanner(defaultScanner, otherScanner);
    }

    @Test
    public void noPluginsLoaded() {
        Collection<DeploymentUnit> plugins = scanner.getDeploymentUnits();

        assertThat(plugins, empty());
    }

    @Test
    public void somePluginsLoaded() {
        List<DeploymentUnit> units = ImmutableList.of(unit1, unit2, unit3);
        when(defaultScanner.scan()).thenReturn(units);
        when(defaultScanner.getDeploymentUnits()).thenReturn(units);

        Collection<DeploymentUnit> plugins = scanner.getDeploymentUnits();

        assertThat(plugins, hasItems(unit1, unit2, unit3));
    }

    @Test
    public void switchingToNewScannerWithScanning() {
        mockTwoScanners();

        Collection<DeploymentUnit> pluginsBeforeSwitch = scanner.scan();
        scanner.switchTo(true);
        Collection<DeploymentUnit> pluginsAfterSwitch = scanner.scan();
        Collection<DeploymentUnit> pluginsAfterRescan = scanner.scan();

        assertThat(pluginsBeforeSwitch, hasItems(unit1, unit2));
        assertThat(pluginsAfterSwitch, hasItems(unit3));
        assertThat(pluginsAfterRescan, empty());
    }
    @Test
    public void switchingToNewScannerWithAllPlugins() {
        mockTwoScanners();

        scanner.scan();
        Collection<DeploymentUnit> pluginsBeforeSwitch = scanner.getDeploymentUnits();
        scanner.switchTo(true);
        Collection<DeploymentUnit> pluginsAfterSwitch = scanner.getDeploymentUnits();

        assertThat(pluginsBeforeSwitch, hasItems(unit1, unit2));
        assertThat(pluginsAfterSwitch, hasItems(unit2, unit3));
    }


    @Test
    public void switchingBackAndForthWithoutScanning() {
        mockTwoScanners();

        scanner.scan();

        scanner.switchTo(true);
        scanner.switchTo(false);

        assertThat(scanner.scan(), hasSize(0));
    }

    @Test
    public void switchingWithoutScanningFirstReturnsScannedResult() {
        mockTwoScanners();

        scanner.switchTo(true);

        assertThat(scanner.scan(), hasItems(unit2, unit3));
    }

    private void mockTwoScanners() {
        List<DeploymentUnit> oldUnits = ImmutableList.of(unit1, unit2);
        List<DeploymentUnit> newUnits = ImmutableList.of(unit2, unit3);
        when(defaultScanner.scan()).then(invocation -> {
            when(defaultScanner.getDeploymentUnits()).thenReturn(oldUnits);
            return oldUnits;
        }).thenReturn(emptyList());
        when(otherScanner.scan()).then(invocation -> {
            when(otherScanner.getDeploymentUnits()).thenReturn(newUnits);
            return newUnits;
        }).thenReturn(emptyList());

    }
}
