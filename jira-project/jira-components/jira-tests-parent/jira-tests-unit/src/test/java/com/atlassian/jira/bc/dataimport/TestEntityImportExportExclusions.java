package com.atlassian.jira.bc.dataimport;

import com.atlassian.jira.entity.Entity;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertFalse;

/**
 * @since v4.4
 */
@RunWith(MockitoJUnitRunner.class)
public class TestEntityImportExportExclusions {

    @Test
    public void getExcludedEntitiesShallNotContainOsPropertyDataInBtf() {
        //when
        boolean result = EntityImportExportExclusions.ENTITIES_EXCLUDED_FROM_IMPORT_EXPORT
                .contains(Entity.Name.OS_PROPERTY_DATA);
        //then
        assertFalse("getExcludedEntities(BTF).contains(OsPropertyData)", result);
    }
}