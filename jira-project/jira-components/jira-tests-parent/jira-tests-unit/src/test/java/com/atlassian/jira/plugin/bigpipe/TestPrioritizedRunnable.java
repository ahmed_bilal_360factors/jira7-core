package com.atlassian.jira.plugin.bigpipe;

import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class TestPrioritizedRunnable {
    private final Runnable nothingRunner = () -> {
    };

    @Test
    public void priorityOrder() {
        PrioritizedRunnable r1 = new PrioritizedRunnable(100, nothingRunner);
        PrioritizedRunnable r2 = new PrioritizedRunnable(200, nothingRunner);
        PrioritizedRunnable r3 = new PrioritizedRunnable(150, nothingRunner);
        List<PrioritizedRunnable> list = Arrays.asList(r1, r2, r3);
        Collections.sort(list);
        assertThat(list, contains(r2, r3, r1));
    }

    @Test
    public void innerRunnableIsExecuted() {
        AtomicLong counter = new AtomicLong();
        PrioritizedRunnable r = new PrioritizedRunnable(100, counter::incrementAndGet);
        r.run();
        assertThat(counter.get(), is(1L));
    }
}
