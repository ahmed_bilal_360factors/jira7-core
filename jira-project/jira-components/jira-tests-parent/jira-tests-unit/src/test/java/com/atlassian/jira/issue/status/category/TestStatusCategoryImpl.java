package com.atlassian.jira.issue.status.category;

import org.junit.Test;

import java.util.List;

import static com.atlassian.collectors.CollectorsUtil.toImmutableList;
import static com.atlassian.jira.issue.status.category.StatusCategory.COMPLETE;
import static com.atlassian.jira.issue.status.category.StatusCategory.IN_PROGRESS;
import static com.atlassian.jira.issue.status.category.StatusCategory.TO_DO;
import static com.atlassian.jira.issue.status.category.StatusCategory.UNDEFINED;
import static com.atlassian.jira.issue.status.category.StatusCategoryImpl.findByKey;
import static com.atlassian.jira.issue.status.category.StatusCategoryImpl.findByName;
import static com.google.common.collect.Lists.newArrayList;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertThat;
import static org.junit.matchers.JUnitMatchers.hasItem;

/**
 * Test Case for StatusCategoryImpl
 *
 * @since v6.1
 */
public class TestStatusCategoryImpl {
    private static final String ALIAS_TO_DO = "To Do";
    private static final String ALIAS_DONE = "Done";

    @Test
    public void testGetDefault() {
        StatusCategory statusCategory = StatusCategoryImpl.getDefault();

        assertEquals("id of status category should match", 1L, (long) statusCategory.getId());
        assertEquals(UNDEFINED, statusCategory.getKey());
        assertEquals("medium-gray", statusCategory.getColorName());
    }

    @Test
    public void testGetUserVisibleCategoriesWithLogicalProgressSequence() {
        List<StatusCategory> categories = StatusCategoryImpl.getUserVisibleCategories();
        assertEquals(3, categories.size());
        assertEquals(TO_DO, categories.get(0).getKey());
        assertEquals(IN_PROGRESS, categories.get(1).getKey());
        assertEquals(COMPLETE, categories.get(2).getKey());
    }

    @Test
    public void testGetAliases() {
        testGetAliases(TO_DO, ALIAS_TO_DO);
        testGetAliases(COMPLETE, ALIAS_DONE);
    }

    @Test
    public void testFindByNameUsingAlias() {
        assertSame(findByKey(TO_DO), findByName(ALIAS_TO_DO));
        assertSame(findByKey(COMPLETE), findByName(ALIAS_DONE));
    }

    @Test
    public void testFindByName() {
        assertSame(findByKey(TO_DO), findByName("New"));
        assertSame(findByKey(COMPLETE), findByName("Complete"));
        assertSame(findByKey(IN_PROGRESS), findByName("In Progress"));
        assertSame(findByKey(UNDEFINED), findByName("No Category"));
    }
    
    @Test
    public void statusCategoriesAreOrderedBySequence() {
        List<StatusCategory> statusCategoriesInMixedOrder = newArrayList(
                findByName("In Progress"),
                findByName("Complete"),
                findByName("No Category"),
                findByName("New")
        );
        
        List<StatusCategory> sortedStatusCategories = statusCategoriesInMixedOrder.stream().sorted().collect(toImmutableList());

        List<StatusCategory> statusCategoriesInSortedOrder = newArrayList(
                findByName("No Category"),
                findByName("New"),
                findByName("In Progress"),
                findByName("Complete")
        );
        assertThat(sortedStatusCategories, is(statusCategoriesInSortedOrder));
    }

    private void testGetAliases(String categoryKey, String expectedAlias) {
        StatusCategory category = findByKey(categoryKey);
        assertEquals(1, category.getAliases().size());
        assertThat(category.getAliases(), hasItem(expectedAlias));
    }
}
