package com.atlassian.jira.appconsistency.db;

import com.atlassian.jira.config.database.DatabaseConfig;
import com.atlassian.jira.config.database.DatabaseConfigurationManager;
import com.atlassian.jira.config.database.JndiDatasource;
import com.atlassian.jira.config.database.ManagedDatasourceInfoSupplier;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

/**
 * Unit test for {@link PublicSchemaConfigCheck}.
 *
 * @since v4.4
 */
public class TestPublicSchemaConfigCheck {
    public static final JndiDatasource DATASOURCE = new JndiDatasource("jndi;yo");

    @Rule
    public MethodRule initMockito = MockitoJUnit.rule();

    @Mock
    private DatabaseConfigurationManager databaseConfigurationManager;

    @Test
    public void isOkNonH2() {
        final DatabaseConfig databaseConfig = new DatabaseConfig("fooDb", "skimar", DATASOURCE);
        when(databaseConfigurationManager.getDatabaseConfiguration()).thenReturn(databaseConfig);

        PublicSchemaConfigCheck schemeConfigCheck = new PublicSchemaConfigCheck(new ManagedDatasourceInfoSupplier(databaseConfigurationManager));
        assertTrue("Scheme config check should pass", schemeConfigCheck.isOk());
    }

    @Test
    public void isOkH2() {
        final DatabaseConfig databaseConfig = new DatabaseConfig("h2", "PUBLIC", DATASOURCE);
        when(databaseConfigurationManager.getDatabaseConfiguration()).thenReturn(databaseConfig);

        PublicSchemaConfigCheck schemeConfigCheck = new PublicSchemaConfigCheck(new ManagedDatasourceInfoSupplier(databaseConfigurationManager));
        assertTrue("Scheme config check should pass", schemeConfigCheck.isOk());
    }

    @Test
    public void isNotOkH2() {
        final DatabaseConfig databaseConfig = new DatabaseConfig("bogusql", "PUBLIC", DATASOURCE);
        when(databaseConfigurationManager.getDatabaseConfiguration()).thenReturn(databaseConfig);

        PublicSchemaConfigCheck schemeConfigCheck = new PublicSchemaConfigCheck(new ManagedDatasourceInfoSupplier(databaseConfigurationManager));
        assertFalse("Scheme config check should fail", schemeConfigCheck.isOk());
    }

}
