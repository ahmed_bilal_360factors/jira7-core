package com.atlassian.jira.web.action.admin;

import com.atlassian.jira.admin.IntroductionProperty;
import com.atlassian.jira.avatar.GravatarSettings;
import com.atlassian.jira.bc.license.JiraLicenseService;
import com.atlassian.jira.bc.user.search.UserSearchService;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.config.LocaleManager;
import com.atlassian.jira.issue.RendererManager;
import com.atlassian.jira.junit.rules.MockitoContainer;
import com.atlassian.jira.license.LicenseDetails;
import com.atlassian.jira.setting.GzipCompression;
import com.atlassian.jira.timezone.TimeZoneService;
import com.atlassian.plugin.PluginAccessor;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ViewApplicationPropertiesTest {
    @Mock
    private UserSearchService searchService;
    @Mock
    private LocaleManager localeManager;
    @Mock
    private TimeZoneService timeZoneService;
    @Mock
    private RendererManager rendererManager;
    @Mock
    private PluginAccessor pluginAccessor;
    @Mock
    private GzipCompression gZipCompression;
    @Mock
    private FeatureManager featureManager;
    @Mock
    private IntroductionProperty introductionProperty;
    @Mock
    private JiraLicenseService jiraLicenseService;
    @Mock
    private GravatarSettings gravatarSettings;
    @Rule
    public MockitoContainer mockitoContainer = new MockitoContainer(this);

    private ViewApplicationProperties viewApplicationProperties;

    @Before
    public void setUp() {
        viewApplicationProperties = new ViewApplicationProperties(searchService, localeManager, timeZoneService,
                rendererManager, pluginAccessor, gZipCompression, featureManager, introductionProperty,
                jiraLicenseService, gravatarSettings);
    }

    @Test
    public void testIsProductRecommendationsEditableWhenThereAreEvaluationLicenses() {
        LicenseDetails lic1 = mock(LicenseDetails.class);
        LicenseDetails lic2 = mock(LicenseDetails.class);
        when(lic1.isEvaluation()).thenReturn(false);
        when(lic2.isEvaluation()).thenReturn(true);
        when(jiraLicenseService.getLicenses()).thenReturn(Lists.newArrayList(lic1, lic2));
        assertTrue(viewApplicationProperties.isProductRecommendationsEditable());
    }

    @Test
    public void testIsProductRecommendationsEditableWhenThereAreNoEvaluationLicenses() {
        LicenseDetails lic1 = mock(LicenseDetails.class);
        when(lic1.isEvaluation()).thenReturn(false);
        when(jiraLicenseService.getLicenses()).thenReturn(Lists.newArrayList(lic1));
        assertFalse(viewApplicationProperties.isProductRecommendationsEditable());
    }

    @Test
    public void testIsProductRecommendationsBTF() {
        assertFalse(viewApplicationProperties.isProductRecommendations());
    }
}
