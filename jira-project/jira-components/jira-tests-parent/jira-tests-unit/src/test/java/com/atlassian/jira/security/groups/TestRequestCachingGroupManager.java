package com.atlassian.jira.security.groups;

import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.jira.cache.request.MockRequestCacheFactory;
import com.atlassian.jira.cache.request.RequestCacheFactoryImpl;
import com.atlassian.jira.junit.rules.MockitoContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.MockGroup;
import com.google.common.collect.Sets;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;

import java.util.Collection;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestRequestCachingGroupManager {

    @Rule
    public final MockitoContainer mockitoMocks = MockitoMocksInContainer.rule(this);

    @Rule
    public MockRequestCacheFactory requestCacheFactory = MockRequestCacheFactory.rule();

    private RequestCachingGroupManager requestCachingGroupManager;

    @Mock
    private DefaultGroupManager delegatedGroupManager;

    @Before
    public void setUp() throws Exception {
        requestCachingGroupManager = new RequestCachingGroupManager(delegatedGroupManager, new RequestCacheFactoryImpl());
    }

    @Test
    public void shouldDelegateAddingUserToGroup() throws Exception {
        final Group group = mock(Group.class);
        final ApplicationUser user = mock(ApplicationUser.class);

        requestCachingGroupManager.addUserToGroup(user, group);

        verify(delegatedGroupManager).addUserToGroup(user, group);
    }

    @Test
    public void shouldLoadGroupNamesOfUserFromCache() throws Exception {
        final ApplicationUser user = new MockApplicationUser("user1");
        final String[] expectedGroupsNames = new String[]{"group1", "group2"};

        when(delegatedGroupManager.getGroupsForUser(user.getName()))
                .thenReturn(Sets.newHashSet(mockGroups(expectedGroupsNames)));

        // Call it more then once
        requestCachingGroupManager.getGroupNamesForUser(user);
        requestCachingGroupManager.getGroupNamesForUser(user);
        requestCachingGroupManager.getGroupNamesForUser(user);
        final Collection<String> groupNamesForUser = requestCachingGroupManager.getGroupNamesForUser(user);

        assertThat(groupNamesForUser, containsInAnyOrder(expectedGroupsNames));

        // Verify that groups were calculated only once
        verify(delegatedGroupManager, times(1)).getGroupsForUser(user.getName());
    }

    @Test
    public void shouldVerifyIfUserBelongsToGroupBasedOnGroupsInCache() throws Exception {
        final ApplicationUser user = new MockApplicationUser("user1");
        final String[] expectedGroups = new String[]{"group1", "group2"};

        when(delegatedGroupManager.getGroupsForUser(user.getName()))
                .thenReturn(Sets.newHashSet(mockGroups(expectedGroups)));

        assertThat("User should not belong to not_existing_group",
                requestCachingGroupManager.isUserInGroup(user, "not_existing_group"), equalTo(false));

        for (final String groupName : expectedGroups) {
            assertThat("User should belong to " + groupName,
                    requestCachingGroupManager.isUserInGroup(user, groupName), equalTo(true));
        }

        // Verify that groups were calculated only once
        verify(delegatedGroupManager, times(1)).getGroupsForUser(user.getName());
    }

    @Test
    public void shouldCheckUserInGroupCaseInsensitive() throws Exception {
        final ApplicationUser user = new MockApplicationUser("User");
        final String[] expectedGroups = new String[]{"GrOuP1", "GRoup2"};

        when(delegatedGroupManager.getGroupsForUser(user.getName()))
                .thenReturn(Sets.newHashSet(mockGroups(expectedGroups)));

        for (final String groupName : expectedGroups) {
            assertThat("User should belong to " + groupName.toLowerCase(),
                    requestCachingGroupManager.isUserInGroup(user, groupName.toLowerCase()), equalTo(true));
        }
    }

    @Test
    public void shouldCheckUserCaseInsensitive() throws Exception {
        final ApplicationUser user = new MockApplicationUser("User");
        final String[] expectedGroups = new String[]{"GrOuP1", "GRoup2"};

        when(delegatedGroupManager.getGroupsForUser(user.getName()))
                .thenReturn(Sets.newHashSet(mockGroups(expectedGroups)));
        when(delegatedGroupManager.getGroupsForUser(user.getName().toLowerCase()))
                .thenReturn(Sets.newHashSet(mockGroups(expectedGroups)));

        for (final String groupName : expectedGroups) {
            assertThat("User should belong to " + groupName.toLowerCase(),
                    requestCachingGroupManager.isUserInGroup(user.getName().toLowerCase(), groupName.toLowerCase()), equalTo(true));
        }
    }

    @Test
    public void shouldReturnGroupsForUserWithCasePreserving() throws Exception {
        final ApplicationUser user = new MockApplicationUser("User");
        final String[] expectedGroups = new String[]{"GrOuP1", "GRoup2"};

        when(delegatedGroupManager.getGroupsForUser(user.getName()))
                .thenReturn(Sets.newHashSet(mockGroups(expectedGroups)));

        final Collection<String> groupNamesForUser = requestCachingGroupManager.getGroupNamesForUser(user);
        assertThat(groupNamesForUser, containsInAnyOrder(expectedGroups));
    }

    private Group[] mockGroups(final String... groupNames) {
        final Group[] groups = new Group[groupNames.length];
        for (int i = 0; i < groupNames.length; i++) {
            groups[i] = new MockGroup(groupNames[i]);
        }
        return groups;
    }
}