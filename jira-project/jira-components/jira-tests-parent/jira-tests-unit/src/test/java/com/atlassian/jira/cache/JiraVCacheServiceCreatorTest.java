package com.atlassian.jira.cache;

import com.atlassian.cache.memory.MemoryCacheManager;
import com.atlassian.jira.config.properties.JiraProperties;
import com.atlassian.jira.config.properties.MockJiraProperties;
import com.atlassian.vcache.DirectExternalCache;
import com.atlassian.vcache.ExternalCacheSettings;
import com.atlassian.vcache.ExternalCacheSettingsBuilder;
import com.atlassian.vcache.internal.core.service.AbstractVCacheService;
import com.atlassian.vcache.internal.legacy.LegacyVCacheService;
import com.atlassian.vcache.internal.memcached.MemcachedVCacheService;
import com.atlassian.vcache.marshallers.MarshallerFactory;
import org.junit.Before;
import org.junit.Test;

import java.time.Duration;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;


public class JiraVCacheServiceCreatorTest {
    private static final String CACHE_NAME = "testCache";

    private JiraProperties jiraProperties;
    private JiraVCacheServiceCreator vCacheServiceCreator;

    @Before
    public void setUp() {
        jiraProperties = new MockJiraProperties();

        vCacheServiceCreator = new JiraVCacheServiceCreator(jiraProperties,
                Optional.of(new MemoryCacheManager()), false);
    }

    @Test
    public void createsLegacyVCacheByDefaultWithMax24HoursTTLAndMaxIntMaxValueEntries() {
        final AbstractVCacheService vCacheService = createService();
        final DirectExternalCache<String> cache = createExternalCache(vCacheService);

        final ExternalCacheSettings settings = vCacheService.allExternalCacheDetails().get(CACHE_NAME).getSettings();

        assertThat(vCacheService, is(instanceOf(LegacyVCacheService.class)));
        assertNotNull(cache);
        assertThat(settings.getDefaultTtl().get(), is(Duration.ofHours(24)));
        assertThat(settings.getEntryCountHint().get(), is(Integer.MAX_VALUE));
    }

    @Test(expected = IllegalStateException.class)
    public void legacyVCacheCannotBeCreatedWithoutCacheFactory() {
        vCacheServiceCreator = new JiraVCacheServiceCreator(jiraProperties,
                Optional.empty(), false);
        vCacheServiceCreator.createVCacheService();
    }

    @Test
    public void maxTTLSettingCanBeOverwritten() {
        jiraProperties.setProperty(JiraVCacheServiceCreator.PROPERTY_MAX_TTL_IN_SEC, Duration.ofHours(47).getSeconds());
        final AbstractVCacheService vCacheService = createService();
        createExternalCache(vCacheService);

        final ExternalCacheSettings settings = vCacheService.allExternalCacheDetails().get(CACHE_NAME).getSettings();

        assertThat(settings.getDefaultTtl().get(), is(Duration.ofHours(47)));
    }

    @Test
    public void entriesCountSettingCanBeOverwritten() {
        jiraProperties.setProperty(JiraVCacheServiceCreator.PROPERTY_MAX_ENTRIES, 1000);
        final AbstractVCacheService vCacheService = createService();
        createExternalCache(vCacheService);

        final ExternalCacheSettings settings = vCacheService.allExternalCacheDetails().get(CACHE_NAME).getSettings();

        assertThat(settings.getEntryCountHint().get(), is(1000));
    }

    @Test
    public void vCacheBackendCanBeForcedToLegacy() {
        jiraProperties.setProperty(JiraVCacheServiceCreator.PROPERTY_FORCE_VCACHE_TYPE, "legacy");
        final AbstractVCacheService vCacheService = createService();
        assertTrue(vCacheService instanceof LegacyVCacheService);
    }

    @Test
    public void createsMemcachedServiceWhenPropertiesAreSet() {
        jiraProperties.setProperty(JiraVCacheServiceCreator.PROPERTY_FORCE_VCACHE_TYPE, "memcached");
        jiraProperties.setProperty(JiraVCacheServiceCreator.PROPERTY_MEMCACHED_HOST, "somehost");
        jiraProperties.setProperty(JiraVCacheServiceCreator.PROPERTY_MEMCACHED_PORT, 11211);

        final AbstractVCacheService vCacheService = createService();

        assertThat(vCacheService, is(instanceOf(MemcachedVCacheService.class)));
    }

    @Test(expected = IllegalStateException.class)
    public void hostIsRequiredForMemcachedService() {
        jiraProperties.setProperty(JiraVCacheServiceCreator.PROPERTY_FORCE_VCACHE_TYPE, "memcached");
        jiraProperties.setProperty(JiraVCacheServiceCreator.PROPERTY_MEMCACHED_PORT, 11212);

        vCacheServiceCreator.createVCacheService();
    }

    @Test
    public void portIsNotRequiredForMemcachedService() {
        jiraProperties.setProperty(JiraVCacheServiceCreator.PROPERTY_FORCE_VCACHE_TYPE, "memcached");
        jiraProperties.setProperty(JiraVCacheServiceCreator.PROPERTY_MEMCACHED_HOST, "somehost");

        assertThat(vCacheServiceCreator.createVCacheService().right(), is(instanceOf(MemcachedVCacheService.class)));
    }

    private DirectExternalCache<String> createExternalCache(final AbstractVCacheService vCacheService) {
        return vCacheService.getDirectExternalCache(CACHE_NAME,
                MarshallerFactory.stringMarshaller(), new ExternalCacheSettingsBuilder()
                        .defaultTtl(Duration.ofHours(48))
                        .entryCountHint(Integer.MAX_VALUE)
                        .build());
    }

    private AbstractVCacheService createService() {
        return vCacheServiceCreator.createVCacheService().right();
    }
}