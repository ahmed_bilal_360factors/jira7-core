package com.atlassian.jira.upgrade.tasks.role;

import com.atlassian.jira.test.util.lic.core.CoreLicenses;
import com.atlassian.jira.test.util.lic.other.OtherLicenses;
import com.atlassian.jira.test.util.lic.sd.ServiceDeskLicenses;
import com.atlassian.jira.test.util.lic.software.SoftwareLicenses;
import com.atlassian.jira.upgrade.tasks.role.LicenseUtils.ServiceDeskLicenseType;
import org.junit.Test;

import static com.atlassian.jira.test.util.lic.sd.ServiceDeskLicenses.LICENSE_SERVICE_DESK_ABP;
import static com.atlassian.jira.test.util.lic.sd.ServiceDeskLicenses.LICENSE_SERVICE_DESK_ABP_ACADEMIC;
import static com.atlassian.jira.test.util.lic.sd.ServiceDeskLicenses.LICENSE_SERVICE_DESK_ABP_EVAL;
import static com.atlassian.jira.test.util.lic.sd.ServiceDeskLicenses.LICENSE_SERVICE_DESK_JIRA7_NON_BACKWARD_COMPATIBLE;
import static com.atlassian.jira.test.util.lic.sd.ServiceDeskLicenses.LICENSE_SERVICE_DESK_RBP;
import static com.atlassian.jira.test.util.lic.sd.ServiceDeskLicenses.LICENSE_SERVICE_DESK_RBP_EVAL;
import static com.atlassian.jira.test.util.lic.sd.ServiceDeskLicenses.LICENSE_SERVICE_DESK_TBP;
import static com.atlassian.jira.test.util.lic.sd.ServiceDeskLicenses.LICENSE_SERVICE_DESK_TBP_EVAL;
import static com.atlassian.jira.test.util.lic.sd.ServiceDeskLicenses.LICENSE_SERVICE_DESK_TBP_EVAL_EXPIRED;
import static com.atlassian.jira.upgrade.tasks.role.LicenseUtils.determineServiceDeskLicenseType;
import static com.atlassian.jira.upgrade.tasks.role.LicenseUtils.isServiceDeskLicense;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class LicenseUtilsTest {

    @Test
    public void determinesServiceDeskABPLicense() {
        assertThat(determineServiceDeskLicenseType(toLicense(LICENSE_SERVICE_DESK_ABP)), is(ServiceDeskLicenseType.AgentBasedPricing));
        assertThat(determineServiceDeskLicenseType(toLicense(LICENSE_SERVICE_DESK_ABP_EVAL)), is(ServiceDeskLicenseType.AgentBasedPricing));
        assertThat(determineServiceDeskLicenseType(toLicense(LICENSE_SERVICE_DESK_ABP_ACADEMIC)), is(ServiceDeskLicenseType.AgentBasedPricing));
    }

    @Test
    public void determinesServiceDeskTBPLicense() {
        assertThat(determineServiceDeskLicenseType(toLicense(LICENSE_SERVICE_DESK_TBP)), is(ServiceDeskLicenseType.TierBasedPricing));
        assertThat(determineServiceDeskLicenseType(toLicense(LICENSE_SERVICE_DESK_TBP_EVAL)), is(ServiceDeskLicenseType.TierBasedPricing));
        assertThat(determineServiceDeskLicenseType(toLicense(LICENSE_SERVICE_DESK_TBP_EVAL_EXPIRED)), is(ServiceDeskLicenseType.TierBasedPricing));
    }

    @Test
    public void rbpLicenseBackwardCompatibleWithAbpIsDeterminedAsAbp() {
        assertThat(determineServiceDeskLicenseType(toLicense(LICENSE_SERVICE_DESK_RBP)), is(ServiceDeskLicenseType.AgentBasedPricing));
        assertThat(determineServiceDeskLicenseType(toLicense(LICENSE_SERVICE_DESK_RBP_EVAL)), is(ServiceDeskLicenseType.AgentBasedPricing));
    }

    @Test
    public void rbpLicenseNotCompatibleWithABPIsDeterminedAsTbp() {
        assertThat(determineServiceDeskLicenseType(toLicense(LICENSE_SERVICE_DESK_JIRA7_NON_BACKWARD_COMPATIBLE)), is(ServiceDeskLicenseType.TierBasedPricing));
    }

    @Test(expected = MigrationFailedException.class)
    public void determineServiceDeskLicenseTypeThrowsExceptionIfLicenseIsNotForServiceDesk() {
        determineServiceDeskLicenseType(toLicense(CoreLicenses.LICENSE_CORE));
    }

    @Test
    public void isServiceDeskLicenseReturnsTrueForCorrectSdLicense() {
        assertTrue(isServiceDeskLicense(toLicense(ServiceDeskLicenses.LICENSE_SERVICE_DESK_ABP)));
        assertTrue(isServiceDeskLicense(toLicense(ServiceDeskLicenses.LICENSE_SERVICE_DESK_RBP_EVAL)));
        assertTrue(isServiceDeskLicense(toLicense(ServiceDeskLicenses.LICENSE_SERVICE_DESK_TBP)));
        assertTrue(isServiceDeskLicense(toLicense(ServiceDeskLicenses.LICENSE_SERVICE_DESK_JIRA7_NON_BACKWARD_COMPATIBLE)));
    }

    @Test
    public void isServiceDeskLicenseReturnsFalseForNonSdLicense() {
        assertFalse(isServiceDeskLicense(toLicense(CoreLicenses.LICENSE_CORE)));
        assertFalse(isServiceDeskLicense(toLicense(OtherLicenses.LICENSE_PRE_JIRA_APP_V2_COMMERCIAL)));
        assertFalse(isServiceDeskLicense(toLicense(SoftwareLicenses.LICENSE_SOFTWARE)));
    }

    private License toLicense(com.atlassian.jira.test.util.lic.License license) {
        return new License(license.getLicenseString());
    }

}