package com.atlassian.jira.plugin.jql.function;

import com.atlassian.crowd.embedded.api.CrowdService;
import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.jira.JiraDataTypes;
import com.atlassian.jira.issue.comparator.UserCachingComparator;
import com.atlassian.jira.jql.operand.QueryLiteral;
import com.atlassian.jira.jql.query.QueryCreationContext;
import com.atlassian.jira.jql.query.QueryCreationContextImpl;
import com.atlassian.jira.mock.plugin.jql.operand.MockJqlFunctionModuleDescriptor;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.MockGroup;
import com.atlassian.jira.user.util.UserUtil;
import com.atlassian.jira.util.MessageSet;
import com.atlassian.query.clause.TerminalClause;
import com.atlassian.query.operand.FunctionOperand;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

import static com.atlassian.jira.jql.operand.SimpleLiteralFactory.createLiteral;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestMembersOfFunction {
    @Rule
    public MethodRule initMock = MockitoJUnit.rule();
    private static final String FUNC_NAME = "funcName";
    private MembersOfFunction membersOfFunction;
    private final TerminalClause terminalClause = null;
    private QueryCreationContext queryCreationContext;
    @Mock
    private UserUtil userUtil;
    @Mock
    private CrowdService crowdService;
    private Group groupForTest;
    private ApplicationUser userForTest;
    private ApplicationUser testUser;

    @Before
    public void setUp() throws Exception {

        testUser = new MockApplicationUser("theUser");
        groupForTest = new MockGroup("group1");
        userForTest = new MockApplicationUser("fred");

        membersOfFunction = new MembersOfFunction(userUtil, crowdService);
        membersOfFunction.init(MockJqlFunctionModuleDescriptor.create(FUNC_NAME, true));
        queryCreationContext = new QueryCreationContextImpl((ApplicationUser) null);
    }

    @Test
    public void testDataType() throws Exception {
        assertEquals(JiraDataTypes.USER, membersOfFunction.getDataType());
    }

    @Test
    public void testValidateArguments() throws Exception {
        final FunctionOperand functionOperand = new FunctionOperand(FUNC_NAME, Collections.<String>emptyList());
        final MessageSet messageSet = membersOfFunction.validate(null, functionOperand, terminalClause);
        assertTrue(messageSet.hasAnyErrors());
        assertEquals("Function 'funcName' expected '1' arguments but received '0'.", messageSet.getErrorMessages().iterator().next());
    }

    @Test
    public void testGroupDoesNotExist() throws Exception {
        final FunctionOperand functionOperand = new FunctionOperand(FUNC_NAME, Arrays.asList("myGroupWillNeverExistHaHaHa"));
        final MessageSet messageSet = membersOfFunction.validate(null, functionOperand, terminalClause);
        assertTrue(messageSet.hasAnyErrors());
        assertEquals("Function 'funcName' can not generate a list of usernames for group 'myGroupWillNeverExistHaHaHa'; the group does not exist.", messageSet.getErrorMessages().iterator().next());
    }

    @Test
    public void testValidationHappyPath() throws Exception {
        // One valid user name supplied
        when(crowdService.getGroup("group")).thenReturn(groupForTest);

        final FunctionOperand functionOperand = new FunctionOperand(FUNC_NAME, Arrays.asList("group"));
        final MessageSet messageSet = membersOfFunction.validate(testUser, functionOperand, terminalClause);
        assertFalse(messageSet.hasAnyMessages());
    }

    @Test
    public void testGetValuesBadArgument() {
        final FunctionOperand functionOperand = new FunctionOperand(FUNC_NAME);
        final List<QueryLiteral> stringList = membersOfFunction.getValues(null, functionOperand, terminalClause);
        assertTrue(stringList.isEmpty());
    }

    @Test
    public void testGetValuesNoSuchGroup() throws Exception {
        final FunctionOperand functionOperand = new FunctionOperand(FUNC_NAME, Arrays.asList("myGroupWillNeverExistHaHaHa"));
        final List<QueryLiteral> stringList = membersOfFunction.getValues(null, functionOperand, terminalClause);
        assertTrue(stringList.isEmpty());
    }

    @Test
    public void testGetValuesGroupDoesNotContainUser() throws Exception {
//        Group testGroup = createMockGroup("groupFunctionTestGroupDoesNotExist");
//        try
//        {
        final FunctionOperand functionOperand = new FunctionOperand(FUNC_NAME, Arrays.asList("groupFunctionTestGroupDoesNotExist"));
        final List<QueryLiteral> stringList = membersOfFunction.getValues(null, functionOperand, terminalClause);
        assertTrue(stringList.isEmpty());
//        }
//        finally
//        {
//            testGroup.remove();
//        }
    }

    @Test
    public void testGetValuesHappyPath() throws Exception {
        when(crowdService.getGroup("group")).thenReturn(groupForTest);

        List<Group> groups = Collections.singletonList(groupForTest);
        final SortedSet<ApplicationUser> setOfUsers = new TreeSet<>(new UserCachingComparator());
        setOfUsers.add(userForTest);
        when(userUtil.getAllUsersInGroups(groups)).thenReturn(setOfUsers);

        final FunctionOperand functionOperand = new FunctionOperand(FUNC_NAME, Arrays.asList("group"));
        final List<QueryLiteral> stringList = membersOfFunction.getValues(queryCreationContext, functionOperand, terminalClause);
        assertEquals(1, stringList.size());
        assertEquals("fred", stringList.get(0).getStringValue());
        assertEquals(functionOperand, stringList.get(0).getSourceOperand());
    }

    @Test
    public void testGetValuesMultipleGroups() throws Exception {
        final Group group2ForTest = new MockGroup("group2");
        final ApplicationUser user2ForTest = new MockApplicationUser("mary");

        when(crowdService.getGroup("group")).thenReturn(groupForTest);
        when(crowdService.getGroup("group2")).thenReturn(group2ForTest);

        List<Group> groups = Collections.singletonList(groupForTest);
        SortedSet<ApplicationUser> setOfUsers = new TreeSet<>(new UserCachingComparator());
        setOfUsers.add(userForTest);
        setOfUsers.add(user2ForTest);
        when(userUtil.getAllUsersInGroups(groups)).thenReturn(setOfUsers);

        groups = Collections.singletonList(group2ForTest);
        setOfUsers = new TreeSet<>(new UserCachingComparator());
        setOfUsers.add(user2ForTest);
        when(userUtil.getAllUsersInGroups(groups)).thenReturn(setOfUsers);


        final FunctionOperand functionOperand = new FunctionOperand(FUNC_NAME, Arrays.asList("group", "group2"));
        final List<QueryLiteral> stringList = membersOfFunction.getValues(queryCreationContext, functionOperand, terminalClause);
        assertEquals(2, stringList.size());
        assertTrue(stringList.contains(createLiteral("fred")));
        assertTrue(stringList.contains(createLiteral("mary")));
        assertEquals(functionOperand, stringList.get(0).getSourceOperand());
        assertEquals(functionOperand, stringList.get(1).getSourceOperand());
    }

    @Test
    public void testGetMinimumNumberOfExpectedArguments() throws Exception {
        assertEquals(1, membersOfFunction.getMinimumNumberOfExpectedArguments());
    }

}
