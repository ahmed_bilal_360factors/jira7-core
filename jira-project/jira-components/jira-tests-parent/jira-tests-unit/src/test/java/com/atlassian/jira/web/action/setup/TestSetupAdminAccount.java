package com.atlassian.jira.web.action.setup;

import com.atlassian.application.api.ApplicationManager;
import com.atlassian.collectors.CollectorsUtil;
import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.jira.application.ApplicationConfigurationHelper;
import com.atlassian.jira.application.ApplicationRoleManager;
import com.atlassian.jira.bc.license.JiraLicenseService;
import com.atlassian.jira.bc.user.UserService;
import com.atlassian.jira.bc.user.UserService.CreateUserRequest;
import com.atlassian.jira.bc.user.UserServiceResultHelper;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.exception.PermissionException;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoContainer;
import com.atlassian.jira.license.LicenseDetails;
import com.atlassian.jira.mock.MockApplicationProperties;
import com.atlassian.jira.mock.i18n.MockI18nHelper;
import com.atlassian.jira.mock.servlet.MockHttpServletRequest;
import com.atlassian.jira.mock.servlet.MockHttpSession;
import com.atlassian.jira.plugin.webresource.JiraWebResourceManager;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.security.groups.MockGroupManager;
import com.atlassian.jira.security.websudo.InternalWebSudoManager;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.MockGroup;
import com.atlassian.jira.user.util.UserUtil;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.JiraProductInformation;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.jira.web.HttpServletVariables;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSortedSet;
import com.google.common.collect.Sets;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import webwork.action.Action;
import webwork.action.ServletActionContext;

import javax.servlet.http.HttpSession;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import static com.atlassian.jira.permission.GlobalPermissionKey.ADMINISTER;
import static com.atlassian.jira.permission.GlobalPermissionKey.SYSTEM_ADMIN;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

public class TestSetupAdminAccount {
    @Rule
    public MockitoContainer mockitoContainer = new MockitoContainer(this);
    private SetupAdminAccount setupAdminAccountAction;
    @Mock
    private UserService userService;

    @Mock
    private UserUtil userUtil;

    private MockGroupManager groupManager = new MockGroupManager();

    @AvailableInContainer(instantiateMe = true)
    private MockApplicationProperties applicationProperties;

    @Mock
    @AvailableInContainer
    private JiraAuthenticationContext jiraAuthenticationContext;

    @AvailableInContainer(instantiateMe = true)
    private MockI18nHelper mockI18nHelper;

    @Mock
    @AvailableInContainer
    private GlobalPermissionManager globalPermissionManager;

    @Mock
    @AvailableInContainer
    private ApplicationManager applicationManager;

    @Mock
    @AvailableInContainer
    private ApplicationRoleManager applicationRoleManager;

    @Mock
    private JiraLicenseService jiraLicenseService;
    @Mock
    private LicenseDetails licenseDetails;
    @Mock
    private JiraWebResourceManager jiraWebResourceManager;
    @Mock
    private InternalWebSudoManager webSudoManager;
    @Mock
    private HttpServletVariables servletVariables;
    @Mock
    private HttpSession httpSession;
    @Mock
    private JiraProductInformation jiraProductInformation;
    @Mock
    private ApplicationConfigurationHelper appConfigHelper;

    @Before
    public void setUp() throws Exception {
        when(globalPermissionManager.getGroupNamesWithPermission(ADMINISTER)).thenReturn(Sets.newHashSet());
        when(globalPermissionManager.getGroupNamesWithPermission(SYSTEM_ADMIN)).thenReturn(Sets.newHashSet());

        ServletActionContext.setRequest(new MockHttpServletRequest(new MockHttpSession()));
        when(jiraAuthenticationContext.getI18nHelper()).thenReturn(mockI18nHelper);
        when(jiraLicenseService.getLicenses()).thenReturn(Collections.singletonList(licenseDetails));
        when(servletVariables.getHttpSession()).thenReturn(httpSession);

        setupAdminAccountAction = new SetupAdminAccount(userService, groupManager, userUtil, null,
                jiraLicenseService, jiraWebResourceManager,
                null, globalPermissionManager, jiraProductInformation, appConfigHelper);
    }

    @After
    public void tearDown() throws Exception {
        ServletActionContext.setRequest(null);
    }

    @Test
    public void testWillVerifyMutators() {
        Assert.assertNull(setupAdminAccountAction.getUsername());
        Assert.assertNull(setupAdminAccountAction.getFullname());
        Assert.assertNull(setupAdminAccountAction.getEmail());
        Assert.assertNull(setupAdminAccountAction.getPassword());
        Assert.assertNull(setupAdminAccountAction.getConfirm());

        setupAdminAccountAction.setUsername("bob");
        setupAdminAccountAction.setFullname("bob smith");
        setupAdminAccountAction.setEmail("bob@bob.com");
        setupAdminAccountAction.setPassword("password");
        setupAdminAccountAction.setConfirm("password");

        assertEquals("bob", setupAdminAccountAction.getUsername());
        assertEquals("bob smith", setupAdminAccountAction.getFullname());
        assertEquals("bob@bob.com", setupAdminAccountAction.getEmail());
        assertEquals("password", setupAdminAccountAction.getPassword());
        assertEquals("password", setupAdminAccountAction.getConfirm());

    }

    @Test
    public void testWillVerifyForwardWhenJiraAlreadyConfigured() throws Exception {
        setupAdminAccountAction.getApplicationProperties().setString(APKeys.JIRA_SETUP, "true");
        assertEquals("setupalready", setupAdminAccountAction.doDefault());
    }


    @Test
    public void testWillVerifyForwardToExistingAdmins() throws Exception {
        when(userUtil.getJiraAdministrators()).thenReturn(ImmutableList.of(new MockApplicationUser("fred"), new MockApplicationUser("george")));

        assertEquals("existingadmins", setupAdminAccountAction.doDefault());
    }

    @Test
    public void addsSENToMetadataIfThereIsOneSEN() throws Exception {
        when(jiraLicenseService.getSupportEntitlementNumbers()).thenReturn(ImmutableSortedSet.of("sen1"));
        when(userUtil.getJiraAdministrators()).thenReturn(Collections.emptyList());

        assertEquals(Action.INPUT, setupAdminAccountAction.doDefault());
        verify(jiraWebResourceManager).putMetadata("SEN", "sen1");
    }

    @Test
    public void addsFirstSENToMetadataIfThereAreManySENs() throws Exception {
        when(jiraLicenseService.getSupportEntitlementNumbers()).thenReturn(ImmutableSortedSet.of("sen1", "sen2"));
        when(userUtil.getJiraAdministrators()).thenReturn(Collections.emptyList());

        assertEquals(Action.INPUT, setupAdminAccountAction.doDefault());
        verify(jiraWebResourceManager).putMetadata("SEN", "sen1");
    }

    @Test
    public void metadataDoesNotHaveSENKeyIfThereIsNoSENReturnedByService() throws Exception {
        when(jiraLicenseService.getSupportEntitlementNumbers()).thenReturn(ImmutableSortedSet.of());
        when(userUtil.getJiraAdministrators()).thenReturn(Collections.emptyList());

        assertEquals(Action.INPUT, setupAdminAccountAction.doDefault());
        verifyZeroInteractions(jiraWebResourceManager);
    }

    @Test
    public void shouldDelegateUsersValidationToTheService() throws Exception {
        applicationProperties.setString(APKeys.JIRA_SETUP, "true");
        assertEquals("setupalready", setupAdminAccountAction.execute());
        applicationProperties.setString(APKeys.JIRA_SETUP, null);

        setupAdminAccountAction.setEmail("test@atlassian.com");
        setupAdminAccountAction.setFullname("test fullname");
        setupAdminAccountAction.setUsername("test username");
        setupAdminAccountAction.setPassword("password");
        setupAdminAccountAction.setConfirm("passwordconfirm");

        final MockApplicationUser fred = new MockApplicationUser("fred");
        when(jiraAuthenticationContext.getUser()).thenReturn(fred);

        ErrorCollection ec = new SimpleErrorCollection();
        ec.addError("email", "This email address looks silly");
        UserService.CreateUserValidationResult result = UserServiceResultHelper.getCreateUserValidationResult(ec);
        when(userService.validateCreateUser(any(CreateUserRequest.class))).thenReturn(result);

        assertEquals(Action.INPUT, setupAdminAccountAction.execute());
        assertEquals(ec.getErrors(), setupAdminAccountAction.getErrors());
    }


    @Test
    public void testWillForwardToExistingAdminsForExistingAdmin() throws Exception {
        setAllValidData();

        when(userUtil.getJiraAdministrators()).thenReturn(ImmutableList.of(new MockApplicationUser("fred"), new MockApplicationUser("george")));

        assertEquals("existingadmins", setupAdminAccountAction.execute());
    }

    @Test
    public void testWillCreateUserSuccessfullyAndPopulateSENMetadata() throws Exception {
        when(jiraLicenseService.getSupportEntitlementNumbers()).thenReturn(ImmutableSortedSet.of("sen1"));
        final UserService.CreateUserValidationResult result = setAllValidData();

        assertEquals(Action.SUCCESS, setupAdminAccountAction.execute());
        verify(userService).createUser(result);
        verify(jiraWebResourceManager).putMetadata("SEN", "sen1");

    }

    @Test
    public void shouldAddProperErrorOnPermissionException() throws Exception {
        final UserService.CreateUserValidationResult result = setAllValidData();
        when(userService.createUser(result)).thenThrow(new PermissionException());
        assertEquals(Action.ERROR, setupAdminAccountAction.execute());

        assertThat(setupAdminAccountAction.getErrorMessages(), containsInAnyOrder("signup.error.group.database.immutable [test username]"));
    }

    @Test
    public void shouldCreateUserGroupsWhichAreMissing() throws Exception {
        final UserService.CreateUserValidationResult result = setAllValidData();
        when(userService.createUser(result)).thenReturn(new MockApplicationUser(result.getUsername()));
        assertTrue(groupManager.getAllGroups().isEmpty());
        setupAdminAccountAction.execute();

        assertNotNull(groupManager.getGroup(AbstractSetupAction.DEFAULT_GROUP_ADMINS));
    }

    @Test
    public void shouldAddUserToAllGroupsGroups() throws Exception {
        testAddUserToSpecifiedGroup(AbstractSetupAction.DEFAULT_GROUP_ADMINS);
    }

    @Test
    public void shouldNotAddUserToAnyGroupWHenIsAlreadyThere() throws Exception {
        testAddUserToSpecifiedGroup(/*empty group list */);
    }

    @Test
    public void shouldAddOnlyToSpecifiedGroupsWhenUserIsAlreadyInSomeOfThem() throws Exception {
        testAddUserToSpecifiedGroup(AbstractSetupAction.DEFAULT_GROUP_ADMINS);
    }

    @Test
    public void shouldAddGlobalAdministrationPermissionForAdmins() throws Exception {
        final UserService.CreateUserValidationResult result = setAllValidData();
        final MockApplicationUser user = new MockApplicationUser(result.getUsername());
        when(userService.createUser(result)).thenReturn(user);

        when(globalPermissionManager.getGroupNames(Permissions.ADMINISTER)).thenReturn(ImmutableList.of());

        setupAdminAccountAction.execute();

        verify(globalPermissionManager).addPermission(Permissions.ADMINISTER, AbstractSetupAction.DEFAULT_GROUP_ADMINS);
    }

    @Test
    public void applicationsAreSetUpCorrectly() throws Exception {
        //given
        final UserService.CreateUserValidationResult result = setAllValidData();
        final MockApplicationUser user = new MockApplicationUser(result.getUsername());
        when(userService.createUser(result)).thenReturn(user);

        //when
        setupAdminAccountAction.execute();

        //then
        final Group adminGroup = groupManager.getGroup(AbstractSetupAction.DEFAULT_GROUP_ADMINS);
        verify(appConfigHelper).configureApplicationsForSetup(Collections.singleton(adminGroup), true);
        verify(appConfigHelper).setupAdminForDefaultApplications(user);
    }

    private void testAddUserToSpecifiedGroup(String... notInGroups) throws Exception {
        final Set<Group> notAlreadyIn = Arrays.stream(notInGroups)
                .map(MockGroup::new)
                .collect(CollectorsUtil.toImmutableSet());

        final UserService.CreateUserValidationResult result = setAllValidData();
        final MockApplicationUser user = new MockApplicationUser(result.getUsername());
        when(userService.createUser(result)).thenReturn(user);

        List<MockGroup> groups = ImmutableList.of(new MockGroup(AbstractSetupAction.DEFAULT_GROUP_ADMINS));

        groups.forEach(groupManager::addGroup);
        groups.stream()
                .filter(group -> !notAlreadyIn.contains(group))
                .forEach(group -> groupManager.addUserToGroup(user, group));

        setupAdminAccountAction.execute();

        groups.forEach(group -> assertTrue(groupManager.isUserInGroup(user, group)));
    }

    private UserService.CreateUserValidationResult setAllValidData() {
        setupAdminAccountAction.setEmail("test@atlassian.com");
        setupAdminAccountAction.setFullname("test fullname");
        setupAdminAccountAction.setUsername("test username");
        setupAdminAccountAction.setPassword("password");
        setupAdminAccountAction.setConfirm("passwordconfirm");
        UserService.CreateUserValidationResult result = UserServiceResultHelper.getCreateUserValidationResult("test username", "password", "test@atlassian.com", "test fullname");
        when(userService.validateCreateUser(any(CreateUserRequest.class))).thenReturn(result);
        return result;
    }
}
