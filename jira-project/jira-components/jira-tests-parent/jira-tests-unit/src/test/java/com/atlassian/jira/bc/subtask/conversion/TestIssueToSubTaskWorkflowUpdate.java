package com.atlassian.jira.bc.subtask.conversion;

import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.bc.MockJiraServiceContext;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.issue.status.Status;
import com.atlassian.jira.junit.rules.InitMockitoMocks;
import com.atlassian.jira.project.MockProject;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.mockito.Mock;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

public class TestIssueToSubTaskWorkflowUpdate {
    @Rule
    public TestRule initMockitoMocks = new InitMockitoMocks(this);

    private final JiraServiceContext ctx = new MockJiraServiceContext();
    private final MockProject project = new MockProject(999);

    @Mock
    private IssueType issueType;
    @Mock
    private Status status;
    @Mock
    private Issue issue;

    @Before
    public void setUp() {
        when(issue.getId()).thenReturn(9L);
        when(issue.getStatusObject()).thenReturn(status);
        when(issue.getProjectObject()).thenReturn(project);
    }

    @Test
    public void statusChangeIsNotRequiredWhenStatusIsInWorkflowForProjectAndIssueType() {
        DefaultIssueToSubTaskConversionService service = new DefaultIssueToSubTaskConversionService(null, null, null, null, null, null, null, null, null) {
            protected boolean isStatusInWorkflowForProjectAndIssueType(Status status, Long projectId, String subTaskId) {
                return true;
            }
        };

        assertThat("Status change should be required", service.isStatusChangeRequired(ctx, issue, issueType), equalTo(false));
    }

    @Test
    public void statusChangeIsRequiredWhenStatusIsNotInWorkflowForProjectAndIssueType() {
        DefaultIssueToSubTaskConversionService service = new DefaultIssueToSubTaskConversionService(null, null, null, null, null, null, null, null, null) {
            protected boolean isStatusInWorkflowForProjectAndIssueType(Status status, Long projectId, String subTaskId) {
                return false;
            }
        };

        assertThat("Status change should not be required", service.isStatusChangeRequired(ctx, issue, issueType), equalTo(true));
    }
}
