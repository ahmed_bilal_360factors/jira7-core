package com.atlassian.jira.jql.resolver;

import com.atlassian.jira.config.StatusCategoryManager;
import com.atlassian.jira.issue.status.category.StatusCategory;
import com.atlassian.jira.jql.operand.QueryLiteral;
import com.atlassian.jira.jql.operand.SimpleLiteralFactory;
import com.atlassian.query.operand.EmptyOperand;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @since v6.2
 */
@RunWith(MockitoJUnitRunner.class)
public class TestStatusCategoryResolver {

    private StatusCategoryResolver resolver;

    @Mock
    private StatusCategoryManager categoryManager;

    @Before
    public void setUp() {
        resolver = new StatusCategoryResolver(categoryManager);
    }

    @Test
    public void testEmptyResolvesToDefault() throws Exception {
        final StatusCategory unknownCategory = mock(StatusCategory.class);
        when(categoryManager.getDefaultStatusCategory()).thenReturn(unknownCategory);

        assertThat(resolver.getStatusCategories(ImmutableList.of(new QueryLiteral(EmptyOperand.EMPTY))), contains(unknownCategory));
    }

    @Test
    public void testForName() {
        final StatusCategory cName = mock(StatusCategory.class);
        when(cName.getId()).thenReturn(1L);
        when(categoryManager.getStatusCategoryByName("Name")).thenReturn(cName);

        assertThat(resolver.getStatusCategories(ImmutableSet.of(SimpleLiteralFactory.createLiteral("Name"))), contains(cName));
        assertThat(resolver.getIdsFromName("Name"), contains(new Long(1L).toString()));
    }

    @Test
    public void testForKey() {
        final StatusCategory cKey = mock(StatusCategory.class);
        when(categoryManager.getStatusCategoryByKey("key")).thenReturn(cKey);

        assertThat(resolver.getStatusCategories(ImmutableSet.of(SimpleLiteralFactory.createLiteral("key"))), contains(cKey));
    }

    @Test
    public void testForId() {
        final StatusCategory c1 = mock(StatusCategory.class);
        when(categoryManager.getStatusCategory(1L)).thenReturn(c1);

        assertThat(resolver.getStatusCategories(ImmutableSet.of(SimpleLiteralFactory.createLiteral(1L))), contains(c1));
        assertThat(resolver.get(1L), equalTo(c1));
    }

    @Test
    public void testMissingId() {
        assertThat(resolver.get(1L), is(nullValue()));
    }

    @Test
    public void testDefaultStatusCategory() {
        final StatusCategory unknownCategory = mock(StatusCategory.class);
        when(categoryManager.getDefaultStatusCategory()).thenReturn(unknownCategory);

        assertThat(resolver.getStatusCategories(ImmutableSet.of(SimpleLiteralFactory.createLiteral("key"), new QueryLiteral(EmptyOperand.EMPTY))), contains(unknownCategory));
    }

}