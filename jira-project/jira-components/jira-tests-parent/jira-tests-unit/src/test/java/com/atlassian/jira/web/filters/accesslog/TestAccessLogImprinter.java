package com.atlassian.jira.web.filters.accesslog;

import com.atlassian.instrumentation.operations.OpSnapshot;
import com.atlassian.instrumentation.operations.OpTimerFactory;
import com.atlassian.instrumentation.operations.ThreadOpTimerFactory;
import com.atlassian.instrumentation.operations.registry.OpRegistry;
import com.atlassian.jira.cluster.ClusterManager;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.mock.component.MockComponentWorker;
import com.atlassian.jira.mock.servlet.MockHttpServletRequest;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import java.time.Clock;
import java.util.LinkedList;
import java.util.List;

import static com.atlassian.jira.web.filters.accesslog.AccessLogRequestInfo.JIRA_REQUEST_ASESSIONID;
import static com.atlassian.jira.web.filters.accesslog.AccessLogRequestInfo.JIRA_REQUEST_ID;
import static com.atlassian.jira.web.filters.accesslog.AccessLogRequestInfo.JIRA_REQUEST_START_MILLIS;
import static com.atlassian.jira.web.filters.accesslog.AccessLogRequestInfo.JIRA_SESSION_LAST_ACCESSED_TIME;
import static com.atlassian.jira.web.filters.accesslog.AccessLogRequestInfo.JIRA_SESSION_MAX_INACTIVE_INTERVAL;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.endsWith;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.startsWith;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

/**
 * Unit test case for {@link com.atlassian.jira.web.filters.accesslog.AccessLogImprinter}.
 */
public class TestAccessLogImprinter {
    @Mock
    private ThreadOpTimerFactory opTimerFactory;

    @Mock
    private ClusterManager clusterManager;

    @Mock
    private Clock clock;

    @Mock
    OpRegistry registry;

    private List<OpSnapshot> opSnapshots = new LinkedList<>();

    @Before
    public void setUp() throws Exception {
        opSnapshots.add(new OpSnapshot("db.reads", 15, 1000000, 1000, 100, 0, 0, 0, 0));
        opSnapshots.add(new OpSnapshot("db.conns", 30, 2000000, 2000, 200, 0, 0, 0, 0));
        initMocks(this);
        ComponentAccessor.initialiseWorker(new MockComponentWorker()
                        .addMock(OpTimerFactory.class, opTimerFactory)
                        .addMock(ClusterManager.class, clusterManager)
                        .addMock(Clock.class, clock)
        );

        when(opTimerFactory.getOpRegistry()).thenReturn(registry);
    }

    @Test
    public void testNoAttributes() {
        MockHttpServletRequest httpServletRequest = new MockHttpServletRequest();
        AccessLogImprinter imprinter = new AccessLogImprinter(httpServletRequest, clock);

        String value = imprinter.imprintHTMLComment();

        assertThat(value, startsWith("\n<!--"));
        assertThat(value, endsWith("\n-->"));
        assertThat(value, containsString("REQUEST ID : -"));
        assertThat(value, containsString("REQUEST TIMESTAMP : -"));
        assertThat(value, containsString("REQUEST TIME : -"));
        assertThat(value, containsString("ASESSIONID : -"));
        assertThat(value, is(not(containsString("NODE ID"))));
    }

    @Test
    public void testValuesSet() {
        MockHttpServletRequest httpServletRequest = new MockHttpServletRequest();
        httpServletRequest.setAttribute(JIRA_REQUEST_START_MILLIS, clock.millis());
        httpServletRequest.setAttribute(JIRA_REQUEST_ID, "requestId");
        httpServletRequest.setAttribute(JIRA_REQUEST_ASESSIONID, "ABCDEF1234");
        AccessLogImprinter imprinter = new AccessLogImprinter(httpServletRequest, clock);
        when(clock.millis()).thenReturn(3000L);

        String value = imprinter.imprintHTMLComment();

        assertThat(value, not(containsString("REQUEST TIMESTAMP : 3000")));
        assertThat(value, containsString("REQUEST ID : requestId"));
        assertThat(value, containsString("ASESSIONID : ABCDEF1234"));
        assertThat(value, is(not(containsString("NODE ID"))));
    }

    @Test
    public void shouldPrintHtmlCommentWithDatabaseTimings() {
        when(registry.snapshot()).thenReturn(opSnapshots);
        AccessLogImprinter imprinter = new AccessLogImprinter(new MockHttpServletRequest(), clock);

        String value = imprinter.imprintHTMLComment();

        assertThat(value, containsString("name='db.reads', invocationCount=15, elapsedTotal=1000000, elapsedMin=1000, elapsedMax=100, resultSetSize=0, cpuTotal=0, cpuMin=0, cpuMax=0"));
        assertThat(value, containsString("name='db.conns', invocationCount=30, elapsedTotal=2000000, elapsedMin=2000, elapsedMax=200, resultSetSize=0, cpuTotal=0, cpuMin=0, cpuMax=0"));
    }

    @Test
    public void nodeIdShouldBeSetWhenClustered() {
        when(clusterManager.isClustered()).thenReturn(true);
        when(clusterManager.getNodeId()).thenReturn("node");
        MockHttpServletRequest httpServletRequest = new MockHttpServletRequest();
        AccessLogImprinter imprinter = new AccessLogImprinter(httpServletRequest, clock);

        final String value = imprinter.imprintHTMLComment();

        assertThat(value, containsString("NODE ID : node"));
    }

    @Test
    public void testEscaping() {
        MockHttpServletRequest httpServletRequest = new MockHttpServletRequest();
        AccessLogImprinter imprinter = new AccessLogImprinter(httpServletRequest, clock);
        httpServletRequest.setAttribute(JIRA_REQUEST_ID, "a man smoking a pipe <!-- in it");

        String value = imprinter.imprintHTMLComment();

        assertThat(value, not(containsString("REQUEST ID : \"a man smoking a pipe  <!-: comment in it\"")));
    }

    @Test
    public void shouldPrintHiddenInputsWithRequestBasedValues() {
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.setAttribute(JIRA_REQUEST_ID, "requestID");
        request.setAttribute(JIRA_REQUEST_START_MILLIS, 1L);
        request.setAttribute(JIRA_SESSION_LAST_ACCESSED_TIME, 2L);
        request.setAttribute(JIRA_SESSION_MAX_INACTIVE_INTERVAL, 4);
        AccessLogImprinter imprinter = new AccessLogImprinter(request, clock);
        when(clock.millis()).thenReturn(10L);

        String value = imprinter.imprintHiddenHtml();

        assertThat(value, containsString("<input type=\"hidden\" title=\"jira.request.id\" value=\"requestID\" />"));
        assertThat(value, containsString("<input type=\"hidden\" title=\"jira.request.start.millis\" value=\"" + 1 + "\" />"));
        assertThat(value, containsString("<input type=\"hidden\" title=\"jira.request.server.time\""));
        assertThat(value, containsString("<input type=\"hidden\" title=\"jira.session.expiry.time\" value=\"" + 4002 + "\" />"));
        assertThat(value, containsString("<input type=\"hidden\" title=\"jira.session.expiry.in.mins\" value=\"" + 0 + "\" />"));
        assertThat(value, containsString("<input id=\"jiraConcurrentRequests\" type=\"hidden\" name=\"jira.request.concurrent.requests\""));

    }

    @Test
    public void shouldPrintDatabaseTimingsInHtmlFormWhenGotFromOpTimerFactory() {
        when(registry.snapshot()).thenReturn(opSnapshots);
        AccessLogImprinter imprinter = new AccessLogImprinter(new MockHttpServletRequest(), clock);

        String value = imprinter.imprintHiddenHtml();

        assertThat(value, containsString("<input type=\"hidden\" title=\"db.reads.time.in.ms\" value=\"1\" />"));
        assertThat(value, containsString("<input type=\"hidden\" title=\"db.conns.time.in.ms\" value=\"2\" />"));
    }
}
