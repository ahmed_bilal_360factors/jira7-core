package com.atlassian.jira.cache;

import com.atlassian.jira.matchers.LangMatchers;
import org.junit.After;
import org.junit.Test;

import static com.atlassian.jira.cache.JiraVCacheRequestContextSupplier.NoopRequestContext;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;


public class JiraVCacheRequestContextSupplierTest {

    @After
    public void cleanup() {
        JiraVCacheRequestContextSupplier.clearStaticContext();
    }

    @Test
    public void usesStaticContextIfCurrentContextNotInitialised() throws Exception {
        JiraVCacheRequestContextSupplier supplier = new JiraVCacheRequestContextSupplier(false);
        JiraVCacheRequestContextSupplier.initStaticContext("static");

        assertThat(supplier.get().partitionIdentifier(), is("static"));

        JiraVCacheRequestContextSupplier.clearStaticContext();
        assertThat(supplier.get(), LangMatchers.isInstance(NoopRequestContext.class));
    }

    @Test
    public void usesProvidedContextByDefaultIfPresent() throws Exception {
        JiraVCacheRequestContextSupplier supplier = new JiraVCacheRequestContextSupplier(false);
        supplier.initThread("provided");
        JiraVCacheRequestContextSupplier.initStaticContext("static");

        assertThat(supplier.get().partitionIdentifier(), is("provided"));
    }

    @Test
    public void usesNoopContextIfThereIsNoProvidedNorStaticContext() throws Exception {
        JiraVCacheRequestContextSupplier supplier = new JiraVCacheRequestContextSupplier(false);

        assertThat(supplier.get(), LangMatchers.isInstance(NoopRequestContext.class));
    }
}