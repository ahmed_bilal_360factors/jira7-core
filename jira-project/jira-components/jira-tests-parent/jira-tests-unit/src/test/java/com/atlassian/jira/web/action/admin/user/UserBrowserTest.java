package com.atlassian.jira.web.action.admin.user;

import com.atlassian.application.api.ApplicationKey;
import com.atlassian.crowd.embedded.api.CrowdDirectoryService;
import com.atlassian.crowd.embedded.api.CrowdService;
import com.atlassian.jira.application.ApplicationRoleManager;
import com.atlassian.jira.application.MockApplicationRole;
import com.atlassian.jira.avatar.AvatarService;
import com.atlassian.jira.bc.issue.worklog.TimeTrackingConfiguration;
import com.atlassian.jira.issue.AttachmentManager;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.mock.i18n.MockI18nHelper;
import com.atlassian.jira.plugin.webfragment.SimpleLinkManager;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.project.version.VersionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.UserIssueHistoryManager;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.user.util.UserUtil;
import com.atlassian.json.marshal.Jsonable;
import com.atlassian.webresource.api.assembler.PageBuilderService;
import com.atlassian.webresource.api.assembler.RequiredData;
import com.atlassian.webresource.api.assembler.WebResourceAssembler;
import com.google.common.collect.ImmutableSet;
import org.apache.commons.lang.StringUtils;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Locale;

import static org.hamcrest.Matchers.contains;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @since v7.0
 */
public class UserBrowserTest {
    @Rule
    public MockitoContainer container = MockitoMocksInContainer.rule(this);

    @AvailableInContainer
    @Mock
    private IssueManager issueManager;
    @AvailableInContainer
    @Mock
    private CustomFieldManager customFieldManager;
    @AvailableInContainer
    @Mock
    private AttachmentManager attachmentManager;
    @AvailableInContainer
    @Mock
    private ProjectManager projectManager;
    @AvailableInContainer
    @Mock
    private PermissionManager permissionManager;
    @AvailableInContainer
    @Mock
    private VersionManager versionManager;
    @AvailableInContainer
    @Mock
    private UserIssueHistoryManager userIssueHistoryManager;
    @AvailableInContainer
    @Mock
    private TimeTrackingConfiguration timeTrackingConfiguration;
    @AvailableInContainer
    @Mock
    private JiraAuthenticationContext jiraAuthenticationContext;

    @Mock
    private UserUtil userUtil;
    @Mock
    private CrowdService crowdService;
    @Mock
    private CrowdDirectoryService crowdDirectoryService;
    @Mock
    private UserManager userManager;
    @Mock
    private AvatarService avatarService;
    @Mock
    private SimpleLinkManager simpleLinkManager;
    @Mock
    private ApplicationRoleManager applicationRoleManager;

    @Mock
    private PageBuilderService pageBuilderService;

    private UserBrowser userBrowser;

    @Before
    public void setUp() {
        userBrowser = new UserBrowser(userUtil, crowdService, crowdDirectoryService, userManager, avatarService, simpleLinkManager, applicationRoleManager, pageBuilderService);
        MockI18nHelper i18nHelper = new MockI18nHelper();
        i18nHelper.setLocale(Locale.ENGLISH);
        when(jiraAuthenticationContext.getI18nHelper()).thenReturn(i18nHelper);
    }

    @Test
    public void listShouldBeEmptyWhenUsersAreEmpty() throws Exception {
        final RequiredData requiredData = mockPageBuilderService();

        userBrowser.requireCreatedUsersNames();

        final String displayNames = getCreatedUsersDisplayNamesString(requiredData);
        assertEquals("[]", displayNames);
    }

    @Test
    public void shouldReturnListOfCreatedUsersNames() throws Exception {
        final RequiredData requiredData = mockPageBuilderService();
        when(userManager.getUserByName("charlie")).thenReturn(new MockApplicationUser("charlie", "CHARLIE", "email@email.com"));
        userBrowser.setCreatedUser(new String[]{"charlie"});

        userBrowser.requireCreatedUsersNames();

        final String displayNames = getCreatedUsersDisplayNamesString(requiredData);
        assertEquals("[\"CHARLIE\"]", displayNames);
    }

    @Test
    public void shouldReturnEmptyListWhenUsersDoNotExists() throws Exception {
        final RequiredData requiredData = mockPageBuilderService();
        when(userManager.getUserByName("charlie")).thenReturn(null);
        userBrowser.setCreatedUser(new String[]{"charlie"});

        userBrowser.requireCreatedUsersNames();

        final String displayNames = getCreatedUsersDisplayNamesString(requiredData);
        assertEquals("[]", displayNames);
    }

    @Test
    public void shouldReturnNoneOptionsWhenThereIsNoRoles() {
        assertThat(userBrowser.getApplicationRoles(), contains(ITEM_NONE));
    }

    @Test
    public void shouldNotReturnAnyOptionWhenThereIsOnlyOneRole() {
        when(applicationRoleManager.getRoles()).thenReturn(ImmutableSet.of(mockRole("dummy-role")));

        assertThat(userBrowser.getApplicationRoles(), contains(
                ITEM_NONE,
                item("DUMMY-ROLE", "dummy-role")
        ));
    }

    @Test
    public void shouldReturnAllOptionsWhereAreManyRoles() {
        when(applicationRoleManager.getRoles()).thenReturn(ImmutableSet.of(
                mockRole("dummy-role"),
                mockRole("another-role"),
                mockRole("yet-another-role")
        ));

        assertThat(userBrowser.getApplicationRoles(), contains(
                ITEM_ANY,
                ITEM_NONE,
                item("ANOTHER-ROLE", "another-role"), //following 3 are in alphabetical order
                item("DUMMY-ROLE", "dummy-role"),
                item("YET-ANOTHER-ROLE", "yet-another-role")
        ));
    }

    private RequiredData mockPageBuilderService() {
        final WebResourceAssembler webResourceAssembler = Mockito.mock(WebResourceAssembler.class);
        final RequiredData requiredDataMock = Mockito.mock(RequiredData.class);
        when(pageBuilderService.assembler()).thenReturn(webResourceAssembler);
        when(webResourceAssembler.data()).thenReturn(requiredDataMock);
        return requiredDataMock;
    }

    private String getCreatedUsersDisplayNamesString(final RequiredData requiredData) throws IOException {
        final ArgumentCaptor<Jsonable> argument = ArgumentCaptor.forClass(Jsonable.class);

        verify(requiredData).requireData(eq("UserBrowser:createdUsersDisplayNames"), argument.capture());

        final StringWriter sw = new StringWriter();

        argument.getValue().write(sw);
        return sw.toString();
    }

    private MockApplicationRole mockRole(final String key) {
        return new MockApplicationRole(ApplicationKey.valueOf(key)).name(StringUtils.swapCase(key));
    }

    private static final Matcher<UserBrowser.ApplicationRoleSelectItem> ITEM_NONE = item("common.words.none", "-2");
    private static final Matcher<UserBrowser.ApplicationRoleSelectItem> ITEM_ANY = item("common.filters.any", "-1");

    private static Matcher<UserBrowser.ApplicationRoleSelectItem> item(String name, String value) {
        return new TypeSafeMatcher<UserBrowser.ApplicationRoleSelectItem>() {
            @Override
            protected boolean matchesSafely(final UserBrowser.ApplicationRoleSelectItem item) {
                return item.getValue().equals(value) && item.getName().equals(name);
            }

            @Override
            public void describeTo(final Description description) {
                description.appendText("ApplicationRoleSelectItem with name ").appendValue(name).appendText(" and value ").appendValue(value);
            }
        };
    }
}
