package com.atlassian.jira.jql.operator;

import com.atlassian.jira.util.Predicate;
import com.atlassian.query.operator.Operator;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import java.util.Comparator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestOperatorPredicates {
    @Rule
    public final MethodRule initMockito = MockitoJUnit.rule();

    @Mock
    Comparator comparator;

    @Test
    public void testLikeWithComparator() {
        try {
            Operator.LIKE.getPredicateForValue(comparator, 5);
            fail("Like operator did not throw IllegalStateException exception on getPredicate with comparator");
        } catch (IllegalStateException e) {
            // Expected
        }

    }

    @Test
    public void testNotLikeWithComparator() {
        try {
            Operator.NOT_LIKE.getPredicateForValue(comparator, 5);
            fail("Not Like operator did not throw IllegalStateException exception on getPredicate with comparator");
        } catch (IllegalStateException e) {
            // Expected
        }

    }

    @Test
    public void testEqualsWithComparator() {
        try {
            Operator.EQUALS.getPredicateForValue(comparator, 5);
            fail("Equals operator did not throw IllegalStateException exception on getPredicate with comparator");
        } catch (IllegalStateException e) {
            // Expected
        }

    }

    @Test
    public void testNotEqualsWithComparator() {
        try {
            Operator.NOT_EQUALS.getPredicateForValue(comparator, 5);
            fail("Should not be able to get != with a comparator.");
        } catch (IllegalStateException e) {
            // expected
        }
    }

    @Test
    public void testIn() {
        try {
            Operator.IN.getPredicateForValue(comparator, 5);
            fail("Should not be able to get in with a comparator.");
        } catch (IllegalStateException e) {
            // expected
        }
    }

    @Test
    public void testNotIn() {
        try {
            Operator.NOT_IN.getPredicateForValue(comparator, 5);
            fail("Should not be able to get in with a comparator.");
        } catch (IllegalStateException e) {
            // expected
        }
    }

    @Test
    public void testIs() {
        try {
            Operator.IS.getPredicateForValue(comparator, 5);
            fail("Should not be able to get in with a comparator.");
        } catch (IllegalStateException e) {
            // expected
        }
    }

    @Test
    public void testIsNot() {
        try {
            Operator.IS_NOT.getPredicateForValue(comparator, 5);
            fail("Should not be able to get in with a comparator.");
        } catch (IllegalStateException e) {
            // expected
        }
    }

    protected <T> void checkCompare(Predicate<T> pred, T fieldValue, T operandValue, int returnValue, boolean predResult) {
        when(comparator.compare(eq(operandValue), eq(fieldValue))).thenReturn(returnValue);

        assertEquals(predResult, pred.evaluate(operandValue));
    }

    @Test
    public void testLessThan() {
        Predicate<Integer> pred = Operator.LESS_THAN.getPredicateForValue(comparator, 5);

        checkCompare(pred, 5, -1, 1, false);
        checkCompare(pred, 5, 5, 0, false);
        checkCompare(pred, 5, 4, 1, false);
        checkCompare(pred, 5, 6, -1, true);
    }

    @Test
    public void testGreaterThan() {
        Predicate<Integer> pred = Operator.GREATER_THAN.getPredicateForValue(comparator, 5);

        checkCompare(pred, 5, -1, 1, true);
        checkCompare(pred, 5, 5, 0, false);
        checkCompare(pred, 5, 4, 1, true);
        checkCompare(pred, 5, 6, -1, false);
    }

    @Test
    public void testLessThanOrEqual() {
        Predicate<Integer> pred = Operator.LESS_THAN_EQUALS.getPredicateForValue(comparator, 5);

        checkCompare(pred, 5, -1, 1, false);
        checkCompare(pred, 5, 5, 0, true);
        checkCompare(pred, 5, 4, 1, false);
        checkCompare(pred, 5, 6, -1, true);
    }

    @Test
    public void testGreaterThanOrEqual() {
        Predicate<Integer> pred = Operator.GREATER_THAN_EQUALS.getPredicateForValue(comparator, 5);

        checkCompare(pred, 5, -1, 1, true);
        checkCompare(pred, 5, 5, 0, true);
        checkCompare(pred, 5, 4, 1, true);
        checkCompare(pred, 5, 6, -1, false);
    }

}
