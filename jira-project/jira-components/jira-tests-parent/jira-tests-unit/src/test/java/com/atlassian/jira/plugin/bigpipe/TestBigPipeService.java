package com.atlassian.jira.plugin.bigpipe;

import com.atlassian.jira.concurrent.MockBarrierFactory;
import com.atlassian.jira.mock.MockFeatureManager;
import com.atlassian.jira.mock.web.pagebuilder.MockDefaultJiraPageBuilderService;
import com.atlassian.json.marshal.Jsonable;
import com.atlassian.plugin.webresource.WebResourceIntegration;
import com.atlassian.plugin.webresource.prebake.PrebakeWebResourceAssembler;
import com.atlassian.plugin.webresource.prebake.PrebakeWebResourceAssemblerBuilder;
import com.atlassian.plugin.webresource.prebake.PrebakeWebResourceAssemblerFactory;
import com.atlassian.webresource.api.assembler.RequiredData;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.io.IOException;
import java.io.StringWriter;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.ConcurrentHashMap;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

public class TestBigPipeService {
    @Rule
    public MockitoRule mockitoInitializerRule = MockitoJUnit.rule();

    @Mock
    private WebResourceIntegration webResourceIntegration;

    @Mock
    private PrebakeWebResourceAssemblerFactory webResourceAssemblerFactory;

    @Mock
    private PrebakeWebResourceAssemblerBuilder webResourceAssemblerBuilder;

    @Mock
    private PrebakeWebResourceAssembler webResourceAssembler;

    private MockFeatureManager featureManager;

    @Spy
    private MockRequiredData requiredData;

    private BigPipeService bigPipeService;

    @Before
    public void setUpPageBuilderService() {
        featureManager = new MockFeatureManager();
        Map<String, Object> requestCache = new ConcurrentHashMap<>();
        MockDefaultJiraPageBuilderService pageBuilderService = new MockDefaultJiraPageBuilderService(webResourceIntegration, webResourceAssemblerFactory);
        when(webResourceAssemblerFactory.create()).thenReturn(webResourceAssemblerBuilder);
        when(webResourceAssemblerBuilder.build()).thenReturn(webResourceAssembler);
        when(webResourceAssembler.data()).thenReturn(requiredData);
        when(webResourceIntegration.getRequestCache()).thenReturn(requestCache);
        bigPipeService = new BigPipeService(webResourceIntegration, pageBuilderService, featureManager, new MockBarrierFactory());
    }

    @Test
    public void noItems()
            throws Exception {
        boolean executed = bigPipeService.executeSingleTask();
        assertThat(executed, is(false));
    }

    @Test
    public void singleItem()
            throws Exception {
        bigPipeService.pipeContent("one", 100, () -> "Good morning");
        assertThat(requiredData.promises.keySet(), contains("one"));
        boolean executed = bigPipeService.executeSingleTask();
        assertThat(executed, is(true));
        executed = bigPipeService.executeSingleTask();
        assertThat(executed, is(false));

        CompletionStage<Jsonable> stage = requiredData.promises.get("one");
        StringWriter buffer = new StringWriter();
        stage.toCompletableFuture().getNow(null).write(buffer);
        assertThat(buffer.toString(), is("\"Good morning\"")); //JSON string
    }

    @Test
    public void multipleItems()
            throws Exception {
        bigPipeService.pipeContent("a", 100, () -> "Blue");
        bigPipeService.pipeContent("b", 300, () -> "Red");
        bigPipeService.pipeContent("c", 200, () -> "Green");
        assertThat(requiredData.promises.keySet(), containsInAnyOrder("a", "b", "c"));
        boolean executed = bigPipeService.executeSingleTask();
        assertThat(executed, is(true));
        assertThat(readJsonAsString("b"), is("\"Red\"")); //JSON string
        executed = bigPipeService.executeSingleTask();
        assertThat(executed, is(true));
        assertThat(readJsonAsString("c"), is("\"Green\"")); //JSON string
        executed = bigPipeService.executeSingleTask();
        assertThat(executed, is(true));
        assertThat(readJsonAsString("a"), is("\"Blue\"")); //JSON string
        executed = bigPipeService.executeSingleTask();
        assertThat(executed, is(false));
    }

    @Test
    public void noPriority()
            throws Exception {
        bigPipeService.pipeContent("one", null, () -> "Good morning");
        assertThat(requiredData.promises.keySet(), contains("one"));
        boolean executed = bigPipeService.executeSingleTask();
        assertThat(executed, is(true));
        executed = bigPipeService.executeSingleTask();
        assertThat(executed, is(false));

        CompletionStage<Jsonable> stage = requiredData.promises.get("one");
        StringWriter buffer = new StringWriter();
        stage.toCompletableFuture().getNow(null).write(buffer);
        assertThat(buffer.toString(), is("\"Good morning\"")); //JSON string
    }

    /**
     * Reads a JSON promise from required data as a string.
     *
     * @param promiseKey the promise key to read.
     */
    private String readJsonAsString(String promiseKey)
            throws IOException {
        CompletionStage<Jsonable> stage = requiredData.promises.get(promiseKey);
        StringWriter buffer = new StringWriter();
        stage.toCompletableFuture().getNow(null).write(buffer);
        return buffer.toString();
    }

    /**
     * Partial implementation of required data just to hold Jsonable promises.
     */
    public static abstract class MockRequiredData implements RequiredData {
        private final Map<String, CompletionStage<Jsonable>> promises = new LinkedHashMap<>();

        @Override
        public RequiredData requireData(String key, CompletionStage<Jsonable> promise) {
            promises.put(key, promise);
            return this;
        }
    }
}
