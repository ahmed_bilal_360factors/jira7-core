package com.atlassian.jira.issue.fields.config.persistence;

import com.atlassian.cache.CacheManager;
import com.atlassian.cache.CachedReference;
import com.atlassian.cache.Supplier;
import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.database.MockQueryDslAccessor;
import com.atlassian.jira.database.ResultRow;
import com.atlassian.jira.exception.DataAccessException;
import com.atlassian.jira.issue.context.persistence.FieldConfigContextPersister;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.issue.fields.config.FieldConfigScheme;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.mock.ofbiz.MockGenericValue;
import com.atlassian.jira.ofbiz.FieldMap;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.querydsl.core.NonUniqueResultException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.MethodRule;
import org.junit.rules.RuleChain;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.ofbiz.core.entity.GenericValue;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import static com.atlassian.jira.matchers.FeatureMatchers.hasFeature;
import static com.google.common.collect.ImmutableMap.of;
import static com.google.common.collect.Lists.newArrayList;
import static java.util.Collections.singletonList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.collection.IsEmptyCollection.empty;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.only;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * The idea is to rewrite the tests in TestFieldConfigSchemePersisterImpl using Mockito
 * and then remove the old class using Easy Mock.
 *
 * @since v6.4
 */
public class TestFieldConfigSchemePersisterImpl {

    @Mock
    OfBizDelegator ofBizDelegator;

    @Mock
    ConstantsManager constantsManager;

    @Mock
    FieldConfigPersister fieldConfigPersister;

    @Mock
    FieldConfigContextPersister configContextPersister;

    @Mock
    FieldConfig fieldConfig;

    @Mock
    IssueType issueType;

    @Mock
    CachedReference fieldConfigSchemesById;

    @Mock
    CachedReference fieldConfigSchemeIssueTypesBySchemeId;

    MockQueryDslAccessor queryDslAccessor = new MockQueryDslAccessor();

    @Mock
    CacheManager cacheManager;

    @Rule
    public final MethodRule initMockito = MockitoJUnit.rule();
    @Rule
    public ExpectedException expectedException = ExpectedException.none();
    @Rule
    public RuleChain mockitoMocksInContainer = MockitoMocksInContainer.forTest(this);

    FieldConfigSchemePersisterImpl fieldConfigSchemePersister;

    final String ENTITY_RELATED_TABLE_NAME = "FieldConfigSchemeIssueType";
    final String ENTITY_ISSUE_TYPE = "issuetype";

    @Before
    public void setUp() {
        when(cacheManager.getCachedReference(eq(FieldConfigSchemePersisterImpl.class.getName() + ".fieldConfigSchemesById"),
                any(Supplier.class))).thenReturn(fieldConfigSchemesById);

        when(cacheManager.getCachedReference(eq(FieldConfigSchemePersisterImpl.class.getName() + ".fieldConfigSchemeIssueTypesBySchemeId"),
                any(Supplier.class))).thenReturn(fieldConfigSchemeIssueTypesBySchemeId);

        //@injectMock does not play well with FieldConfigSchemePersisterImpl constructor here
        fieldConfigSchemePersister = new FieldConfigSchemePersisterImpl(ofBizDelegator, queryDslAccessor, constantsManager, fieldConfigPersister, configContextPersister, cacheManager);
    }

    @Test
    public void testIllegalArgumentToRemoveIssue() {
        FieldConfigSchemePersisterImpl persister = new FieldConfigSchemePersisterImpl(null, queryDslAccessor, null, null, null, cacheManager);
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("issueType should not be null!");
        persister.removeByIssueType(null);
    }

    @Test
    public void testIllegalArgumentTogetInvalidFieldConfigSchemeAfterIssueTypeRemoval() {
        FieldConfigSchemePersisterImpl persister = new FieldConfigSchemePersisterImpl(null, queryDslAccessor, null, null, null, cacheManager);
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("issueType should not be null!");
        persister.getInvalidFieldConfigSchemeAfterIssueTypeRemoval(null);
    }

    @Test
    public void testGetConfigSchemeIdsForCustomFieldIdShouldThrowExceptionWhenIdIsNull() {
        final FieldConfigSchemePersisterImpl persister = new FieldConfigSchemePersisterImpl(ofBizDelegator, queryDslAccessor, constantsManager, fieldConfigPersister, configContextPersister, cacheManager);
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("customFieldId should not be null!");
        persister.getConfigSchemeIdsForCustomFieldId(null);
    }


    @Test
    public void testGetConfigSchemeIdsForCustomFieldIdShouldReturnEmptyListWhenNothingFound() {
        when(ofBizDelegator.findByAnd("FieldConfigScheme", of("fieldid", "customfield_10000"))).thenReturn(Collections.<GenericValue>emptyList());

        final FieldConfigSchemePersisterImpl persister = new FieldConfigSchemePersisterImpl(ofBizDelegator, queryDslAccessor, constantsManager, fieldConfigPersister, configContextPersister, cacheManager);

        final List<Long> ids = persister.getConfigSchemeIdsForCustomFieldId("customfield_10000");
        assertTrue("Ids are not empty", ids.isEmpty());
    }

    @Test
    public void testGetConfigSchemeIdsForCustomFieldIdShouldReturnIdsOfSchemes() {

        final GenericValue gv1 = mock(GenericValue.class);
        when(gv1.getLong("id")).thenReturn(234L);

        final GenericValue gv2 = mock(GenericValue.class);
        when(gv2.getLong("id")).thenReturn(567L);

        when(ofBizDelegator.findByAnd("FieldConfigScheme", of("fieldid", "customfield_10020")))
                .thenReturn(ImmutableList.of(gv1, gv2));

        final FieldConfigSchemePersisterImpl persister = new FieldConfigSchemePersisterImpl(ofBizDelegator, queryDslAccessor, constantsManager, fieldConfigPersister, configContextPersister, cacheManager);

        final List<Long> configSchemeIds = persister.getConfigSchemeIdsForCustomFieldId("customfield_10020");
        assertThat("Config scheme Ids were found", configSchemeIds.isEmpty(), is(false));
        assertThat(configSchemeIds.get(0), is(new Long(234)));
        assertThat(configSchemeIds.get(1), is(new Long(567)));
    }

    @Test
    public void testGetConfigSchemeForFieldConfigNoSchemesAndExpectDataAccessException() throws Exception {
        final Long fieldConfigId = 10L;

        when(fieldConfig.getId()).thenReturn(fieldConfigId);

        queryDslAccessor.setQueryResults(
                "select distinct FIELD_CONFIG_SCHEME_ISSUE_TYPE.fieldconfigscheme\n"
                        + "from fieldconfigschemeissuetype FIELD_CONFIG_SCHEME_ISSUE_TYPE\n"
                        + "where FIELD_CONFIG_SCHEME_ISSUE_TYPE.fieldconfiguration = " + fieldConfigId + "\n"
                        + "limit 2",
                Collections.emptyList());

        final FieldConfigSchemePersisterImpl configSchemePersister = new FieldConfigSchemePersisterImpl(ofBizDelegator, queryDslAccessor, constantsManager, fieldConfigPersister, configContextPersister, cacheManager);
        expectedException.expect(DataAccessException.class);
        configSchemePersister.getConfigSchemeForFieldConfig(fieldConfig);
    }

    @Test
    public void testRemoveByIssueType() {
        final String ENTITY_RELATED_TABLE_NAME = "FieldConfigSchemeIssueType";
        final String ENTITY_ISSUE_TYPE = "issuetype";

        when(issueType.getId()).thenReturn("123");

        fieldConfigSchemePersister.removeByIssueType(issueType);

        verify(ofBizDelegator, only()).removeByAnd(ENTITY_RELATED_TABLE_NAME, of(ENTITY_ISSUE_TYPE, issueType.getId()));
        verify(fieldConfigSchemesById, only()).reset();
        verify(fieldConfigSchemeIssueTypesBySchemeId, only()).reset();
    }


    @Test
    public void testInvalidFieldConfigSchemeAfterIssueTypeRemovalWithNoMatchingSchemes() {
        when(issueType.getId()).thenReturn("123");
        when(ofBizDelegator.findByAnd(ENTITY_RELATED_TABLE_NAME, of(ENTITY_ISSUE_TYPE, issueType.getId()))).thenReturn(Lists.newArrayList());

        final Collection<FieldConfigScheme> result = fieldConfigSchemePersister.getInvalidFieldConfigSchemeAfterIssueTypeRemoval(issueType);

        assertThat(result, empty());
        verify(ofBizDelegator, only()).findByAnd(ENTITY_RELATED_TABLE_NAME, of(ENTITY_ISSUE_TYPE, issueType.getId()));
    }

    @Test
    public void testInvalidFieldConfigSchemeAfterIssueTypeRemoval_AllWithMoreThanOneAssociation() {
        String id = "1111";
        when(issueType.getId()).thenReturn(id);

        List<GenericValue> gvList = newArrayList();

        when(ofBizDelegator.findByAnd("FieldConfigSchemeIssueType", of("issuetype", id))).thenReturn(gvList);

        for (int i = 0; i < 5; i++) {
            Long fcsId = i * 1000L;
            GenericValue gv = new MockGenericValue("FieldConfigSchemeIssueType", FieldMap.build("fieldconfigscheme", fcsId));
            gvList.add(gv);

            when(ofBizDelegator.findByAnd("FieldConfigSchemeIssueType", of("fieldconfigscheme", fcsId))).thenReturn(gvList);
        }

        FieldConfigSchemePersisterImpl persister = new FieldConfigSchemePersisterImpl(ofBizDelegator, queryDslAccessor, null, null, null, cacheManager);
        Collection results = persister.getInvalidFieldConfigSchemeAfterIssueTypeRemoval(issueType);
        assertNotNull(results);
        assertThat("No Field Config schemes were found", results.size(), is(0));

        verify(ofBizDelegator).findByAnd(ENTITY_RELATED_TABLE_NAME, of(ENTITY_ISSUE_TYPE, issueType.getId()));
    }

    @Test
    public void testInvalidFieldConfigSchemeAfterIssueTypeRemoval_OneWithOneAssociation() {
        final GenericValue gv1 = mock(GenericValue.class);
        final GenericValue gv2 = mock(GenericValue.class);
        final String id = "1111";
        when(issueType.getId()).thenReturn(id);

        when(ofBizDelegator.findByAnd("FieldConfigSchemeIssueType", ImmutableMap.of("issuetype", id)))
                .thenReturn(newArrayList(
                        new MockGenericValue("FieldConfigSchemeIssueType", FieldMap.build("fieldconfigscheme", 1000l)),
                        new MockGenericValue("FieldConfigSchemeIssueType", FieldMap.build("fieldconfigscheme", 2000l))
                ));

        when(ofBizDelegator.findByAnd("FieldConfigSchemeIssueType", ImmutableMap.of("fieldconfigscheme", 1000l)))
                .thenReturn(singletonList(gv1)); // only one member
        when(ofBizDelegator.findByAnd("FieldConfigSchemeIssueType", ImmutableMap.of("fieldconfigscheme", 2000l)))
                .thenReturn(Lists.newArrayList(gv1, gv2)); // we just need a list with more than 1 member.  Hence we re-use the one we have

        FieldConfigSchemePersisterImpl persister = new FieldConfigSchemePersisterImpl(ofBizDelegator, queryDslAccessor, null, null, null, cacheManager) {
            public FieldConfigScheme getFieldConfigScheme(Long configSchemeId) {
                return new FieldConfigScheme.Builder().setName("Name").setDescription("Description").setId(configSchemeId).toFieldConfigScheme();
            }
        };

        Collection<FieldConfigScheme> results = persister.getInvalidFieldConfigSchemeAfterIssueTypeRemoval(issueType);
        assertThat(results, containsInAnyOrder(singletonList(hasFeature(FieldConfigScheme::getId, equalTo(1000L), "Scheme with id", "id"))));
    }

    @Test
    public void testGetConfigSchemeForFieldConfigHappyPath() throws Exception {
        final Long fieldConfigId = 10L;
        final Long schemeId = 200L;

        when(fieldConfig.getId()).thenReturn(fieldConfigId);

        final AtomicBoolean called = new AtomicBoolean(false);

        queryDslAccessor.setQueryResults(
                "select distinct FIELD_CONFIG_SCHEME_ISSUE_TYPE.fieldconfigscheme\n"
                        + "from fieldconfigschemeissuetype FIELD_CONFIG_SCHEME_ISSUE_TYPE\n"
                        + "where FIELD_CONFIG_SCHEME_ISSUE_TYPE.fieldconfiguration = " + fieldConfigId +"\n"
                        + "limit 2",
                Collections.singletonList(new ResultRow(schemeId)));

        final FieldConfigSchemePersisterImpl configSchemePersister = new FieldConfigSchemePersisterImpl(ofBizDelegator, queryDslAccessor, constantsManager, fieldConfigPersister, null, cacheManager) {

            @Override
            public FieldConfigScheme getFieldConfigScheme(final Long configSchemeId) {
                called.set(true);
                assertThat("Retrieved value is correct", configSchemeId, is(schemeId));
                return null;
            }
        };

        configSchemePersister.getConfigSchemeForFieldConfig(fieldConfig);
        assertTrue(called.get());
    }

    @Test
    public void testGetConfigSchemeForFieldConfigMoreThanOneScheme() throws Exception {
        final Long fieldConfigId = 10L;
        final Long schemeId1 = 200L;
        final Long schemeId2 = 400L;

        when(fieldConfig.getId()).thenReturn(fieldConfigId);

        final List<ResultRow> returnedValues = ImmutableList.of
                (
                        new ResultRow(schemeId1),
                        new ResultRow(schemeId2)
                );

        queryDslAccessor.setQueryResults(
                "select distinct FIELD_CONFIG_SCHEME_ISSUE_TYPE.fieldconfigscheme\n"
                        + "from fieldconfigschemeissuetype FIELD_CONFIG_SCHEME_ISSUE_TYPE\n"
                        + "where FIELD_CONFIG_SCHEME_ISSUE_TYPE.fieldconfiguration = " + fieldConfigId +"\n"
                        + "limit 2",
                returnedValues);

        final FieldConfigSchemePersisterImpl configSchemePersister = new FieldConfigSchemePersisterImpl(ofBizDelegator, queryDslAccessor, constantsManager, fieldConfigPersister, null, cacheManager);

        expectedException.expect(NonUniqueResultException.class);
        configSchemePersister.getConfigSchemeForFieldConfig(fieldConfig);
    }
}
