package com.atlassian.jira.event.user;

import com.atlassian.event.spi.ListenerInvoker;
import org.hamcrest.Matchers;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

/**
 * @since v4.1
 */
public class TestUserEventListenerHandler {
    @Test
    public void shouldReturnUserEventInvokerWhenUserEventListenerPassed() throws Exception {
        final UserEventListener listener = mock(UserEventListener.class);
        final UserEventListenerHandler userEventListenerHandler = new UserEventListenerHandler();
        final List<? extends ListenerInvoker> invokers = userEventListenerHandler.getInvokers(listener);

        assertThat(invokers, Matchers.contains(Matchers.instanceOf(UserEventListenerHandler.UserEventInvoker.class)));
    }

    @Test
    public void shouldNotReturnInvokersWhenListenerNotPassed() throws Exception {
        final UserEventListenerHandler userEventListenerHandler = new UserEventListenerHandler();
        final List<? extends ListenerInvoker> invokers = userEventListenerHandler.getInvokers(new Object());
        assertThat(invokers, Matchers.emptyIterable());
    }

    @Test
    public void generatedInvokerShouldCallProperEventsForSpecifiedEventTypes() {
        verifyThatListenerCallsMethodOnEvent(UserEventType.USER_SIGNUP, UserEventListener::userSignup);
        verifyThatListenerCallsMethodOnEvent(UserEventType.USER_CREATED, UserEventListener::userCreated);
        verifyThatListenerCallsMethodOnEvent(UserEventType.USER_FORGOTPASSWORD, UserEventListener::userForgotPassword);
        verifyThatListenerCallsMethodOnEvent(UserEventType.USER_FORGOTUSERNAME, UserEventListener::userForgotUsername);
        verifyThatListenerCallsMethodOnEvent(UserEventType.USER_CANNOTCHANGEPASSWORD, UserEventListener::userCannotChangePassword);
    }

    @Test
    public void shouldNotInvokeAnyMethodWhenEventTypeUnknown() {
        //given
        final UserEventListener listener = mock(UserEventListener.class);
        final ListenerInvoker listenerInvoker = prepareHandlerForListener(listener);

        //when
        final UserEvent userEvent = mock(UserEvent.class);
        when(userEvent.getEventType()).thenReturn(-1);

        //then
        listenerInvoker.invoke(userEvent);
        verifyZeroInteractions(listener);
    }

    private void verifyThatListenerCallsMethodOnEvent(final int eventType, final ListenerMethod listenerMethod) {
        // given
        final UserEventListener listener = mock(UserEventListener.class);
        final ListenerInvoker listenerInvoker = prepareHandlerForListener(listener);
        final UserEvent userEvent = mock(UserEvent.class);
        when(userEvent.getEventType()).thenReturn(eventType);

        // when
        listenerInvoker.invoke(userEvent);

        // then
        // verify that listenerMethod was called with userEvent
        listenerMethod.apply(verify(listener), userEvent);
    }

    private ListenerInvoker prepareHandlerForListener(final UserEventListener listener) {
        final UserEventListenerHandler userEventListenerHandler = new UserEventListenerHandler();
        return userEventListenerHandler.getInvokers(listener).get(0);
    }

    private interface ListenerMethod {
        void apply(UserEventListener thiz, UserEvent userEvent);
    }
}
