package com.atlassian.jira.application.install;

import com.atlassian.jira.matchers.OptionalMatchers;
import org.apache.commons.io.IOUtils;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Optional;
import java.util.jar.JarFile;
import java.util.jar.JarOutputStream;
import java.util.zip.ZipEntry;

import static org.junit.Assert.assertThat;

public class BundlesVersionDiscoveryTest {

    @Rule
    public TemporaryFolder temporaryFolder = new TemporaryFolder();

    public static String MANIFEST = "Manifest-Version: 1.0\n"
            + "Archiver-Version: Plexus Archiver\n"
            + "Created-By: Apache Maven Bundle Plugin\n"
            + "Built-By: bamboo-agent\n"
            + "Build-Jdk: 1.7.0_72\n"
            + "Atlassian-Build-Date: 2015-03-02T01:28:12+0000\n"
            + "Bnd-LastModified: 1425259696628\n"
            + "Bundle-Description: This is the GreenHopper plugin for Atlassian JIRA.\n"
            + "Bundle-DocURL: http://www.atlassian.com/\n"
            + "Bundle-ManifestVersion: 2\n"
            + "Bundle-Name: Atlassian GreenHopper\n"
            + "Bundle-SymbolicName: com.pyxis.greenhopper.jira\n"
            + "Bundle-Vendor: Atlassian\n"
            + "Bundle-Version: 6.6.80.D20150302T012537\n\n";
    public static String INVALID_MANIFEST = "Manifest-Version: 1.0\n"
            + "Archiver-Version: Plexus Archiver\n"
            + "Created-By: Apache Maven Bundle Plugin\n"
            + "Built-By: bamboo-agent\n"
            + "Build-Jdk: 1.7.0_72\n\n";
    public static String VERSION_ONLY_MANIFEST = "Manifest-Version: 1.0\n"
            + "Archiver-Version: Plexus Archiver\n"
            + "Created-By: Apache Maven Bundle Plugin\n"
            + "Built-By: bamboo-agent\n"
            + "Bundle-Version: 6.6.80.D20150302T012537\n"
            + "Build-Jdk: 1.7.0_72\n\n";
    public static String SHAKESPEARE_POETRY = "The master, the swabber, the boatswain, and I,\n\n"
            + "    The gunner, and his mate,\n"
            + "Loved Mall, Meg, and Marian, and Margery,\n"
            + "    But none of us cared for Kate;\n"
            + "    For she has a tongue with a tang,\n"
            + "    Would cry to a sailor, Go hang!\n"
            + "She loved not the savour of tar nor of pitch;\n"
            + "Yet a tailor might scratch her where?er she did itch.\n"
            + "    Then, to sea, boys, and let her go hang!";
    BundlesVersionDiscovery testObj = new BundlesVersionDiscovery();

    @Test
    public void testGetBundleNameAndVersion() throws IOException {
        // given
        File jarFile = createJarWithManifest(MANIFEST);

        // when
        final Optional<BundlesVersionDiscovery.PluginIdentification> bundleNameAndVersion = testObj.getBundleNameAndVersion(jarFile);

        // then
        assertThat(
                bundleNameAndVersion,
                OptionalMatchers.some(
                        new BundlesVersionDiscovery.PluginIdentification("com.pyxis.greenhopper.jira", "6.6.80.D20150302T012537")));
    }

    @Test
    public void testJarWithInvalidManifestYieldsNoVersionInformation() throws IOException {
        // given
        File jarFile = createJarWithManifest(INVALID_MANIFEST);

        // when
        final Optional<BundlesVersionDiscovery.PluginIdentification> bundleNameAndVersion = testObj.getBundleNameAndVersion(jarFile);

        // then
        assertThat(bundleNameAndVersion, OptionalMatchers.none());
    }

    @Test
    public void testJarWithoutSymbolicNameInformationYieldsNoVersionInformation() throws IOException {
        // given
        File jarFile = createJarWithManifest(VERSION_ONLY_MANIFEST);

        // when
        final Optional<BundlesVersionDiscovery.PluginIdentification> bundleNameAndVersion = testObj.getBundleNameAndVersion(jarFile);

        // then
        assertThat(bundleNameAndVersion, OptionalMatchers.none());
    }

    @Test
    public void testShakespearePoetryGivesNoVersionInformation() throws IOException {
        // given
        File jarFile = createJarWithManifest(SHAKESPEARE_POETRY);

        // when
        final Optional<BundlesVersionDiscovery.PluginIdentification> bundleNameAndVersion = testObj.getBundleNameAndVersion(jarFile);

        // then
        assertThat(bundleNameAndVersion, OptionalMatchers.none());
    }

    @Test
    public void testJarWithoutManifestYieldsNoVersionInformation() throws IOException {
        // given
        File jarFile = temporaryFolder.newFile();
        try (OutputStream out = new FileOutputStream(jarFile)) {
            final JarOutputStream jarOutputStream = new JarOutputStream(out);
            jarOutputStream.putNextEntry(new ZipEntry("some-file"));
            jarOutputStream.write(new byte[]{1, 2, 3, 4});
            jarOutputStream.closeEntry();
            jarOutputStream.close();
        }

        // when
        final Optional<BundlesVersionDiscovery.PluginIdentification> bundleNameAndVersion = testObj.getBundleNameAndVersion(jarFile);

        // then
        assertThat(bundleNameAndVersion, OptionalMatchers.none());
    }

    private File createJarWithManifest(final String manifest) throws IOException {
        File jarFile = temporaryFolder.newFile();
        try (OutputStream out = new FileOutputStream(jarFile)) {
            final JarOutputStream jarOutputStream = new JarOutputStream(out);
            jarOutputStream.putNextEntry(new ZipEntry(JarFile.MANIFEST_NAME));
            IOUtils.write(manifest, jarOutputStream, "ASCII");
            jarOutputStream.closeEntry();
            jarOutputStream.close();
        }
        return jarFile;
    }
}