package com.atlassian.jira.issue.fields.csv.custom;

import com.atlassian.jira.issue.customfields.impl.ProjectCFType;

/**
 * This is testing a CustomFieldType where it should only be exporting a single value. E.g. the {@link ProjectCFType}
 * only exports the name of the project, therefore if there is no value it should be an empty string for the export.
 */
public abstract class CustomFieldSingleValueCsvExporterTest<T>  extends CustomFieldCsvExporterTest<T> {
    @Override
    public void testExportWithNullValue() {
        whenFieldValueIs(null);
        assertExportedValue("");
    }
}
