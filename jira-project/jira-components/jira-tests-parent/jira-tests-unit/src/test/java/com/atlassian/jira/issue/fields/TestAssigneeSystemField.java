package com.atlassian.jira.issue.fields;

import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.crowd.embedded.TestData;
import com.atlassian.jira.issue.ModifiedValue;
import com.atlassian.jira.issue.fields.layout.field.FieldLayout;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem;
import com.atlassian.jira.issue.fields.rest.json.UserBeanFactory;
import com.atlassian.jira.issue.fields.rest.json.beans.JiraBaseUrls;
import com.atlassian.jira.issue.fields.util.MessagedResult;
import com.atlassian.jira.issue.search.handlers.AssigneeSearchHandlerFactory;
import com.atlassian.jira.issue.statistics.AssigneeStatisticsMapper;
import com.atlassian.jira.mock.issue.MockIssue;
import com.atlassian.jira.mock.user.MockUserHistoryManager;
import com.atlassian.jira.plugin.assignee.AssigneeResolver;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.template.VelocityTemplatingEngine;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.google.common.collect.ImmutableList;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.atlassian.jira.permission.ProjectPermissions.ASSIGNABLE_USER;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestAssigneeSystemField {
    AssigneeSystemField systemUnderTest;

    @Mock
    private VelocityTemplatingEngine templatingEngine;
    @Mock
    private PermissionManager permissionManager;
    @Mock
    private ApplicationProperties applicationProperties;
    @Mock
    private JiraAuthenticationContext authenticationContext;
    @Mock
    private AssigneeStatisticsMapper assigneeStatisticsMapper;
    @Mock
    private AssigneeResolver assigneeResolver;
    //@Mock
    private AssigneeSearchHandlerFactory assigneeSearchHandlerFactory;
    @Mock
    private UserManager userManager;
    @Mock
    private WebResourceManager webResourceManager;
    @Mock
    private FeatureManager featureManager;
    @Mock
    private JiraBaseUrls jiraBaseUrls;
    @Mock
    private Assignees assignees;
    @Mock
    private MockUserHistoryManager userHistoryManager;
    @Mock
    private ApplicationUser currentUser;
    @Mock
    private MockIssue issue;
    @Mock
    private ApplicationUser someOtherUser;
    @Mock
    private UserBeanFactory userBeanFactory;

    private ApplicationUser assignableUser = new MockApplicationUser("assignable-user");
    private ApplicationUser nonAssignableUser = new MockApplicationUser("non-assignable-user");

    @Before
    public void setUp() throws Exception {
        userHistoryManager = new MockUserHistoryManager();
        currentUser = new MockApplicationUser("selector");
        someOtherUser = new MockApplicationUser("someone");
        when(authenticationContext.getUser()).thenReturn(currentUser);
        when(permissionManager.hasPermission(any(Integer.class), any(ApplicationUser.class))).thenReturn(true);
        when(userManager.getUserByName("someone")).thenReturn(someOtherUser);
        when(userManager.getUserByName("selector")).thenReturn(currentUser);
        systemUnderTest = new AssigneeSystemField(
                templatingEngine,
                permissionManager,
                applicationProperties,
                authenticationContext,
                assigneeStatisticsMapper,
                assigneeResolver,
                assigneeSearchHandlerFactory,
                userManager,
                webResourceManager,
                featureManager,
                jiraBaseUrls,
                assignees,
                userHistoryManager,
                userBeanFactory);

        when(permissionManager.hasPermission(eq(ASSIGNABLE_USER), any(Project.class), eq(assignableUser))).thenReturn(true);
        when(permissionManager.hasPermission(eq(ASSIGNABLE_USER), any(Project.class), eq(nonAssignableUser))).thenReturn(false);
    }

    private Object accessPrivateField(Object instance, String fieldName)
            throws NoSuchFieldException, IllegalAccessException {
        Field field = instance.getClass().getDeclaredField(fieldName);
        field.setAccessible(true);
        Object fieldValue = field.get(instance);
        return fieldValue;
    }

    private void setPrivateFieldValue(Object instance, String fieldName, Object newValue)
            throws NoSuchFieldException, IllegalAccessException {
        Field field = instance.getClass().getDeclaredField(fieldName);
        field.setAccessible(true);
        field.set(instance, newValue);
        field.setAccessible(false);
    }

    private static class MockFieldLayoutItem implements FieldLayoutItem {
        @Override
        public OrderableField getOrderableField() {
            throw new UnsupportedOperationException("Not implemented");
        }

        @Override
        public String getFieldDescription() {
            throw new UnsupportedOperationException("Not implemented");
        }

        @Override
        public String getRawFieldDescription() {
            throw new UnsupportedOperationException("Not implemented");
        }

        @Override
        public boolean isHidden() {
            return false;
        }

        @Override
        public boolean isRequired() {
            return true;
        }

        @Override
        public String getRendererType() {
            return "";
        }

        @Override
        public FieldLayout getFieldLayout() {
            return null;
        }

        @Override
        public int compareTo(FieldLayoutItem fieldLayoutItem) {
            throw new UnsupportedOperationException("Not implemented");
        }
    }

    @Test
    public void testUpdateIssueAddsUserToUsedUserHistory() throws Exception {
        Map<String, ModifiedValue> modifiedFields = new HashMap<String, ModifiedValue>();
        modifiedFields.put("assignee", new ModifiedValue(currentUser, someOtherUser));
        when(issue.getModifiedFields()).thenReturn(modifiedFields);
        HashMap<String, String> fieldValuesHolder = new HashMap<String, String>();
        fieldValuesHolder.put("assignee", "someone");
        systemUnderTest.updateIssue(new MockFieldLayoutItem(), issue, fieldValuesHolder);
        assertTrue(userHistoryManager.getAddedUsers().contains("someone"));
    }

    @Test
    public void testUpdateIssueDoesNotAddUserToHistoryIfNull() throws Exception {
        Map<String, ModifiedValue> modifiedFields = new HashMap<String, ModifiedValue>();
        modifiedFields.put("assignee", new ModifiedValue(currentUser, null));
        when(issue.getModifiedFields()).thenReturn(modifiedFields);
        HashMap<String, String> fieldValuesHolder = new HashMap<String, String>();
        fieldValuesHolder.put("assignee", "someone");
        systemUnderTest.updateIssue(new MockFieldLayoutItem(), issue, fieldValuesHolder);
        assertTrue(userHistoryManager.getAddedUsers().size() == 0);
    }

    @Test
    public void testUpdateIssueDoesNotAddUserToHistoryIfCurrentUser() throws Exception {
        Map<String, ModifiedValue> modifiedFields = new HashMap<String, ModifiedValue>();
        modifiedFields.put("assignee", new ModifiedValue(someOtherUser, currentUser));
        when(issue.getModifiedFields()).thenReturn(modifiedFields);
        HashMap<String, String> fieldValuesHolder = new HashMap<String, String>();
        fieldValuesHolder.put("assignee", "selector");
        systemUnderTest.updateIssue(new MockFieldLayoutItem(), issue, fieldValuesHolder);
        assertTrue(userHistoryManager.getAddedUsers().size() == 0);
    }

    @Test
    public void testUpdateIssueDoesNotAddUserToHistoryIfDefault() throws Exception {
        Map<String, ModifiedValue> modifiedFields = new HashMap<String, ModifiedValue>();
        modifiedFields.put("assignee", new ModifiedValue(someOtherUser, currentUser));
        when(issue.getModifiedFields()).thenReturn(modifiedFields);
        HashMap<String, String> fieldValuesHolder = new HashMap<String, String>();
        fieldValuesHolder.put("assignee", "-1");
        systemUnderTest.updateIssue(new MockFieldLayoutItem(), issue, fieldValuesHolder);
        assertTrue(userHistoryManager.getAddedUsers().size() == 0);
    }

    @Test
    public void testNeedsMoveWithNullAssignee() throws Exception {
        when(issue.getAssignee()).thenReturn(null);
        when(applicationProperties.getOption(APKeys.JIRA_OPTION_ALLOWUNASSIGNED)).thenReturn(true);

        Collection issues = new ArrayList();
        issues.add(issue);
        MessagedResult result1 = systemUnderTest.needsMove(issues, null, null);
        assertFalse(result1.getResult());

        when(applicationProperties.getOption(APKeys.JIRA_OPTION_ALLOWUNASSIGNED)).thenReturn(false);
        MessagedResult result2 = systemUnderTest.needsMove(issues, null, null);
        assertTrue("Only need move when assignee is null and JIRA doesn't allow unassigned issue", result2.getResult());
    }

    @Test
    public void testNeedsMoveWithAssignee() throws Exception {
        when(issue.getAssignee()).thenReturn(new MockApplicationUser("dev"));
        when(applicationProperties.getOption(APKeys.JIRA_OPTION_ALLOWUNASSIGNED)).thenReturn(true);

        Collection issues = new ArrayList();
        issues.add(issue);
        MessagedResult result1 = systemUnderTest.needsMove(issues, null, null);
        assertFalse(result1.getResult());

        when(applicationProperties.getOption(APKeys.JIRA_OPTION_ALLOWUNASSIGNED)).thenReturn(false);
        MessagedResult result2 = systemUnderTest.needsMove(issues, null, null);
        assertFalse(result2.getResult());
    }

    @Test
    public void testNeedsMoveWithNoAssignablePermission() {
        when(issue.getAssignee()).thenReturn(new MockApplicationUser("dev"));
        when(issue.getProjectObject()).thenReturn(mock(Project.class));
        when(applicationProperties.getOption(APKeys.JIRA_OPTION_ALLOWUNASSIGNED)).thenReturn(true);
        when(permissionManager.hasPermission(eq(ASSIGNABLE_USER), any(Project.class), any(ApplicationUser.class))).thenReturn(false);

        Collection issues = ImmutableList.of(issue);
        MessagedResult result = systemUnderTest.needsMove(issues, issue, null);
        assertTrue(result.getResult());
    }

    @Test
    public void testNullAssgineeNotNeedMove() {
        when(issue.getAssignee()).thenReturn(null);
        when(issue.getProjectObject()).thenReturn(mock(Project.class));
        when(applicationProperties.getOption(APKeys.JIRA_OPTION_ALLOWUNASSIGNED)).thenReturn(true);

        MessagedResult result = systemUnderTest.needsMove(ImmutableList.of(issue), new MockIssue(), null);

        assertFalse(result.getResult());
    }

    @Test
    public void testNeedsMoveWhenBulkEditing() {
        when(applicationProperties.getOption(APKeys.JIRA_OPTION_ALLOWUNASSIGNED)).thenReturn(true);

        List<MockIssue> issues = ImmutableList.of(
                mock(MockIssue.class),
                mock(MockIssue.class),
                mock(MockIssue.class)
        );

        for (MockIssue issue : issues) {
            when(issue.getProjectObject()).thenReturn(mock(Project.class));
        }
        when(issue.getProjectObject()).thenReturn(mock(Project.class));

        when(issues.get(0).getAssignee()).thenReturn(assignableUser);
        when(issues.get(1).getAssignee()).thenReturn(nonAssignableUser);
        when(issues.get(2).getAssignee()).thenReturn(null);

        MessagedResult result = systemUnderTest.needsMove(issues, issue, null);

        assertTrue(result.getResult());
    }
}