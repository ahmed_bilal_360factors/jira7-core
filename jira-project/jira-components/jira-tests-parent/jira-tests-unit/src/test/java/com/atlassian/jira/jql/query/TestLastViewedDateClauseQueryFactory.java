package com.atlassian.jira.jql.query;

import com.atlassian.jira.jql.operand.JqlOperandResolver;
import com.atlassian.jira.jql.operand.QueryLiteral;
import com.atlassian.jira.jql.operator.OperatorClasses;
import com.atlassian.jira.jql.util.JqlDateSupport;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.UserHistoryItem;
import com.atlassian.jira.user.UserIssueHistoryManager;
import com.atlassian.jira.util.collect.CollectionBuilder;
import com.atlassian.query.clause.TerminalClause;
import com.atlassian.query.clause.TerminalClauseImpl;
import com.atlassian.query.operand.EmptyOperand;
import com.atlassian.query.operand.MultiValueOperand;
import com.atlassian.query.operand.Operand;
import com.atlassian.query.operand.SingleValueOperand;
import com.atlassian.query.operator.Operator;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestLastViewedDateClauseQueryFactory {
    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @Mock
    private JqlOperandResolver jqlOperandResolver;
    @Mock
    private JqlDateSupport jqlDateSupport;
    @Mock
    private UserIssueHistoryManager userHistoryManager;

    private final ApplicationUser theUser = null;
    private final QueryCreationContext queryCreationContext = new QueryCreationContextImpl(theUser);

    @InjectMocks
    private LastViewedDateClauseQueryFactory factory;

    @Test
    public void testInvalidOperand() throws Exception {
        final Operand blah = new SingleValueOperand("blah");

        when(jqlOperandResolver.isValidOperand(blah)).thenReturn(false);

        assertEquals(QueryFactoryResult.createFalseResult(), factory.getQuery(queryCreationContext, new TerminalClauseImpl("lastViewed", Operator.EQUALS, blah)));
    }

    @Test
    public void testGetQueryBadOperators() throws Exception {
        final Operand blah = new SingleValueOperand("blah");

        when(jqlOperandResolver.isValidOperand(blah)).thenReturn(true);

        final Set<Operator> changeHistoryOperators = OperatorClasses.CHANGE_HISTORY_OPERATORS;
        for (final Operator operator : changeHistoryOperators) {
            assertEquals(QueryFactoryResult.createFalseResult(), factory.getQuery(queryCreationContext, new TerminalClauseImpl("lastViewed", operator, blah)));
        }

        verify(jqlOperandResolver, times(5)).isValidOperand(blah);
    }

    @Test
    public void testGetQueryEmptyHistory() throws Exception {
        final Operand blah = new SingleValueOperand("blah");

        when(jqlOperandResolver.isValidOperand(blah)).thenReturn(true);
        when(userHistoryManager.getFullIssueHistoryWithoutPermissionChecks(theUser)).thenReturn(Collections.<UserHistoryItem>emptyList());

        assertEquals(QueryFactoryResult.createFalseResult(), factory.getQuery(queryCreationContext, new TerminalClauseImpl("lastViewed", Operator.EQUALS, blah)));

        verify(jqlOperandResolver).isValidOperand(blah);
    }

    //  is//= Empty

    @Test
    public void testGetQueryEqualsEmpty() throws Exception {
        final Operand blah = new EmptyOperand();

        final UserHistoryItem item1 = new UserHistoryItem(UserHistoryItem.ISSUE, "00001", 100l);

        final List<UserHistoryItem> history = CollectionBuilder.list(item1);
        when(jqlOperandResolver.isValidOperand(blah)).thenReturn(true);
        when(jqlOperandResolver.isEmptyOperand(blah)).thenReturn(true);
        when(userHistoryManager.getFullIssueHistoryWithoutPermissionChecks(theUser)).thenReturn(history);

        final QueryFactoryResult result = factory.getQuery(queryCreationContext, new TerminalClauseImpl("lastViewed", Operator.EQUALS, blah));
        assertEquals("issue_id:00001", result.getLuceneQuery().toString());
        assertTrue(result.mustNotOccur());

        verify(jqlOperandResolver).isValidOperand(blah);
        verify(jqlOperandResolver).isEmptyOperand(blah);

    }

    @Test
    public void testGetQueryIsEmpty() throws Exception {
        final Operand blah = new EmptyOperand();

        final UserHistoryItem item1 = new UserHistoryItem(UserHistoryItem.ISSUE, "00001", 100l);
        final UserHistoryItem item2 = new UserHistoryItem(UserHistoryItem.ISSUE, "00002", 200l);

        final List<UserHistoryItem> history = CollectionBuilder.list(item1, item2);
        when(jqlOperandResolver.isValidOperand(blah)).thenReturn(true);
        when(jqlOperandResolver.isEmptyOperand(blah)).thenReturn(true);
        when(userHistoryManager.getFullIssueHistoryWithoutPermissionChecks(theUser)).thenReturn(history);

        final QueryFactoryResult result = factory.getQuery(queryCreationContext, new TerminalClauseImpl("lastViewed", Operator.IS, blah));
        assertEquals("issue_id:00001 issue_id:00002", result.getLuceneQuery().toString());
        assertTrue(result.mustNotOccur());

        verify(jqlOperandResolver).isValidOperand(blah);
        verify(jqlOperandResolver).isEmptyOperand(blah);
    }

    @Test
    public void testGetQueryInEmptyOnly() throws Exception {
        final Operand blah = new EmptyOperand();

        final UserHistoryItem item1 = new UserHistoryItem(UserHistoryItem.ISSUE, "00001", 100l);
        final UserHistoryItem item2 = new UserHistoryItem(UserHistoryItem.ISSUE, "00002", 200l);

        final List<UserHistoryItem> history = CollectionBuilder.list(item1, item2);
        when(jqlOperandResolver.isValidOperand(blah)).thenReturn(true);
        when(jqlOperandResolver.isEmptyOperand(blah)).thenReturn(false);
        when(userHistoryManager.getFullIssueHistoryWithoutPermissionChecks(theUser)).thenReturn(history);
        final TerminalClause clause = new TerminalClauseImpl("lastViewed", Operator.IN, blah);
        when(jqlOperandResolver.getValues(queryCreationContext, clause.getOperand(), clause)).thenReturn(CollectionBuilder.list(new QueryLiteral()));
        when(jqlOperandResolver.isListOperand(blah)).thenReturn(true);

        final QueryFactoryResult result = factory.getQuery(queryCreationContext, clause);
        assertEquals("issue_id:00001 issue_id:00002", result.getLuceneQuery().toString());
        assertTrue(result.mustNotOccur());

        verify(jqlOperandResolver).isValidOperand(blah);
        verify(jqlOperandResolver).isEmptyOperand(blah);
        verify(jqlOperandResolver).getValues(queryCreationContext, clause.getOperand(), clause);
        verify(jqlOperandResolver).isListOperand(blah);
    }


    // is not/!= empty

    @Test
    public void testGetQueryIsNotEmpty() throws Exception {
        final Operand blah = new EmptyOperand();

        final UserHistoryItem item1 = new UserHistoryItem(UserHistoryItem.ISSUE, "00001", 100l);
        final UserHistoryItem item2 = new UserHistoryItem(UserHistoryItem.ISSUE, "00002", 200l);

        final List<UserHistoryItem> history = CollectionBuilder.list(item1, item2);
        when(jqlOperandResolver.isValidOperand(blah)).thenReturn(true);
        when(jqlOperandResolver.isEmptyOperand(blah)).thenReturn(true);
        when(userHistoryManager.getFullIssueHistoryWithoutPermissionChecks(theUser)).thenReturn(history);

        final QueryFactoryResult result = factory.getQuery(queryCreationContext, new TerminalClauseImpl("lastViewed", Operator.IS_NOT, blah));
        assertEquals("issue_id:00001 issue_id:00002", result.getLuceneQuery().toString());
        assertFalse(result.mustNotOccur());

        verify(jqlOperandResolver).isValidOperand(blah);
        verify(jqlOperandResolver).isEmptyOperand(blah);
    }

    @Test
    public void testGetQueryNotEqualsEmpty() throws Exception {
        final Operand blah = new EmptyOperand();

        final UserHistoryItem item1 = new UserHistoryItem(UserHistoryItem.ISSUE, "00001", 100l);
        final UserHistoryItem item2 = new UserHistoryItem(UserHistoryItem.ISSUE, "00002", 200l);

        final List<UserHistoryItem> history = CollectionBuilder.list(item1, item2);
        when(jqlOperandResolver.isValidOperand(blah)).thenReturn(true);
        when(jqlOperandResolver.isEmptyOperand(blah)).thenReturn(true);
        when(userHistoryManager.getFullIssueHistoryWithoutPermissionChecks(theUser)).thenReturn(history);

        final QueryFactoryResult result = factory.getQuery(queryCreationContext, new TerminalClauseImpl("lastViewed", Operator.NOT_EQUALS, blah));
        assertEquals("issue_id:00001 issue_id:00002", result.getLuceneQuery().toString());
        assertFalse(result.mustNotOccur());

        verify(jqlOperandResolver).isValidOperand(blah);
        verify(jqlOperandResolver).isEmptyOperand(blah);
    }

    @Test
    public void testGetQueryNotInEmptyOnly() throws Exception {
        final Operand blah = new EmptyOperand();

        final UserHistoryItem item1 = new UserHistoryItem(UserHistoryItem.ISSUE, "00001", 100l);
        final UserHistoryItem item2 = new UserHistoryItem(UserHistoryItem.ISSUE, "00002", 200l);

        final List<UserHistoryItem> history = CollectionBuilder.list(item1, item2);
        when(jqlOperandResolver.isValidOperand(blah)).thenReturn(true);
        when(jqlOperandResolver.isEmptyOperand(blah)).thenReturn(false);
        when(userHistoryManager.getFullIssueHistoryWithoutPermissionChecks(theUser)).thenReturn(history);
        final TerminalClause clause = new TerminalClauseImpl("lastViewed", Operator.NOT_IN, blah);
        when(jqlOperandResolver.getValues(queryCreationContext, clause.getOperand(), clause)).thenReturn(CollectionBuilder.list(new QueryLiteral()));
        when(jqlOperandResolver.isListOperand(blah)).thenReturn(true);

        final QueryFactoryResult result = factory.getQuery(queryCreationContext, clause);
        assertEquals("issue_id:00001 issue_id:00002", result.getLuceneQuery().toString());
        assertFalse(result.mustNotOccur());

        verify(jqlOperandResolver).isValidOperand(blah);
        verify(jqlOperandResolver).isEmptyOperand(blah);
        verify(jqlOperandResolver).getValues(queryCreationContext, clause.getOperand(), clause);
        verify(jqlOperandResolver).isListOperand(blah);
    }

    // in/= single

    @Test
    public void testGetQueryEquals() throws Exception {
        final Date now = new Date();

        final Operand blah = new SingleValueOperand(now.getTime());

        final UserHistoryItem item1 = new UserHistoryItem(UserHistoryItem.ISSUE, "00001", now.getTime());
        final UserHistoryItem item2 = new UserHistoryItem(UserHistoryItem.ISSUE, "00002", 200l);

        final List<UserHistoryItem> history = CollectionBuilder.list(item1, item2);
        when(jqlOperandResolver.isValidOperand(blah)).thenReturn(true);
        when(jqlOperandResolver.isEmptyOperand(blah)).thenReturn(false);
        when(userHistoryManager.getFullIssueHistoryWithoutPermissionChecks(theUser)).thenReturn(history);
        final TerminalClause clause = new TerminalClauseImpl("lastViewed", Operator.EQUALS, blah);
        when(jqlOperandResolver.getValues(queryCreationContext, clause.getOperand(), clause)).thenReturn(CollectionBuilder.list(new QueryLiteral(clause.getOperand(), now.getTime())));
        when(jqlOperandResolver.isListOperand(blah)).thenReturn(false);
        when(jqlDateSupport.convertToDate(now.getTime())).thenReturn(now);

        final QueryFactoryResult result = factory.getQuery(queryCreationContext, clause);
        assertEquals("issue_id:00001", result.getLuceneQuery().toString());
        assertFalse(result.mustNotOccur());

        verify(jqlOperandResolver).isValidOperand(blah);
        verify(jqlOperandResolver).isEmptyOperand(blah);
        verify(jqlOperandResolver).getValues(queryCreationContext, clause.getOperand(), clause);
        verify(jqlOperandResolver).isListOperand(blah);
        verify(jqlDateSupport).convertToDate(now.getTime());
    }

    @Test
    public void testGetQueryEqualsNoneMatch() throws Exception {
        final Date now = new Date();

        final Operand blah = new SingleValueOperand(now.getTime());

        final UserHistoryItem item1 = new UserHistoryItem(UserHistoryItem.ISSUE, "00001", 100l);
        final UserHistoryItem item2 = new UserHistoryItem(UserHistoryItem.ISSUE, "00002", 200l);

        final List<UserHistoryItem> history = CollectionBuilder.list(item1, item2);
        when(jqlOperandResolver.isValidOperand(blah)).thenReturn(true);
        when(jqlOperandResolver.isEmptyOperand(blah)).thenReturn(false);
        when(userHistoryManager.getFullIssueHistoryWithoutPermissionChecks(theUser)).thenReturn(history);
        final TerminalClause clause = new TerminalClauseImpl("lastViewed", Operator.EQUALS, blah);
        when(jqlOperandResolver.getValues(queryCreationContext, clause.getOperand(), clause)).thenReturn(CollectionBuilder.list(new QueryLiteral(clause.getOperand(), now.getTime())));
        when(jqlOperandResolver.isListOperand(blah)).thenReturn(false);
        when(jqlDateSupport.convertToDate(now.getTime())).thenReturn(now);

        final QueryFactoryResult result = factory.getQuery(queryCreationContext, clause);
        assertEquals("", result.getLuceneQuery().toString());
        assertFalse(result.mustNotOccur());

        verify(jqlOperandResolver).isValidOperand(blah);
        verify(jqlOperandResolver).isEmptyOperand(blah);
        verify(jqlOperandResolver).getValues(queryCreationContext, clause.getOperand(), clause);
        verify(jqlOperandResolver).isListOperand(blah);
        verify(jqlDateSupport).convertToDate(now.getTime());
    }

    @Test
    public void testGetQueryIn() throws Exception {
        final Date now = new Date();

        final Operand blah = new SingleValueOperand(now.getTime());

        final UserHistoryItem item1 = new UserHistoryItem(UserHistoryItem.ISSUE, "00001", now.getTime());
        final UserHistoryItem item2 = new UserHistoryItem(UserHistoryItem.ISSUE, "00002", 200l);

        final List<UserHistoryItem> history = CollectionBuilder.list(item1, item2);
        when(jqlOperandResolver.isValidOperand(blah)).thenReturn(true);
        when(jqlOperandResolver.isEmptyOperand(blah)).thenReturn(false);
        when(userHistoryManager.getFullIssueHistoryWithoutPermissionChecks(theUser)).thenReturn(history);
        final TerminalClause clause = new TerminalClauseImpl("lastViewed", Operator.IN, blah);
        when(jqlOperandResolver.getValues(queryCreationContext, clause.getOperand(), clause)).thenReturn(CollectionBuilder.list(new QueryLiteral(clause.getOperand(), now.getTime())));
        when(jqlOperandResolver.isListOperand(blah)).thenReturn(true);
        when(jqlDateSupport.convertToDate(now.getTime())).thenReturn(now);

        final QueryFactoryResult result = factory.getQuery(queryCreationContext, clause);
        assertEquals("issue_id:00001", result.getLuceneQuery().toString());
        assertFalse(result.mustNotOccur());

        verify(jqlOperandResolver).isValidOperand(blah);
        verify(jqlOperandResolver).isEmptyOperand(blah);
        verify(jqlOperandResolver).getValues(queryCreationContext, clause.getOperand(), clause);
        verify(jqlOperandResolver).isListOperand(blah);
        verify(jqlDateSupport).convertToDate(now.getTime());
    }

    @Test
    public void testGetQueryInNoneMatch() throws Exception {
        final Date now = new Date();

        final Operand blah = new SingleValueOperand(now.getTime());

        final UserHistoryItem item1 = new UserHistoryItem(UserHistoryItem.ISSUE, "00001", 100l);
        final UserHistoryItem item2 = new UserHistoryItem(UserHistoryItem.ISSUE, "00002", 200l);

        final List<UserHistoryItem> history = CollectionBuilder.list(item1, item2);
        when(jqlOperandResolver.isValidOperand(blah)).thenReturn(true);
        when(jqlOperandResolver.isEmptyOperand(blah)).thenReturn(false);
        when(userHistoryManager.getFullIssueHistoryWithoutPermissionChecks(theUser)).thenReturn(history);
        final TerminalClause clause = new TerminalClauseImpl("lastViewed", Operator.IN, blah);
        when(jqlOperandResolver.getValues(queryCreationContext, clause.getOperand(), clause)).thenReturn(CollectionBuilder.list(new QueryLiteral(clause.getOperand(), now.getTime())));
        when(jqlOperandResolver.isListOperand(blah)).thenReturn(true);
        when(jqlDateSupport.convertToDate(now.getTime())).thenReturn(now);

        final QueryFactoryResult result = factory.getQuery(queryCreationContext, clause);
        assertEquals("", result.getLuceneQuery().toString());
        assertFalse(result.mustNotOccur());

        verify(jqlOperandResolver).isValidOperand(blah);
        verify(jqlOperandResolver).isEmptyOperand(blah);
        verify(jqlOperandResolver).getValues(queryCreationContext, clause.getOperand(), clause);
        verify(jqlOperandResolver).isListOperand(blah);
        verify(jqlDateSupport).convertToDate(now.getTime());
    }

    @Test
    public void testGetQueryEqualsMatchesMultiple() throws Exception {
        final Date now = new Date();

        final Operand blah = new SingleValueOperand(now.getTime());

        final UserHistoryItem item1 = new UserHistoryItem(UserHistoryItem.ISSUE, "00001", now.getTime());
        final UserHistoryItem item2 = new UserHistoryItem(UserHistoryItem.ISSUE, "00002", 200l);
        final UserHistoryItem item3 = new UserHistoryItem(UserHistoryItem.ISSUE, "00003", now.getTime());

        final List<UserHistoryItem> history = CollectionBuilder.list(item1, item2, item3);
        when(jqlOperandResolver.isValidOperand(blah)).thenReturn(true);
        when(jqlOperandResolver.isEmptyOperand(blah)).thenReturn(false);
        when(userHistoryManager.getFullIssueHistoryWithoutPermissionChecks(theUser)).thenReturn(history);
        final TerminalClause clause = new TerminalClauseImpl("lastViewed", Operator.EQUALS, blah);
        when(jqlOperandResolver.getValues(queryCreationContext, clause.getOperand(), clause)).thenReturn(CollectionBuilder.list(new QueryLiteral(clause.getOperand(), now.getTime())));
        when(jqlOperandResolver.isListOperand(blah)).thenReturn(false);
        when(jqlDateSupport.convertToDate(now.getTime())).thenReturn(now);

        final QueryFactoryResult result = factory.getQuery(queryCreationContext, clause);
        assertEquals("issue_id:00001 issue_id:00003", result.getLuceneQuery().toString());
        assertFalse(result.mustNotOccur());

        verify(jqlOperandResolver).isValidOperand(blah);
        verify(jqlOperandResolver).isEmptyOperand(blah);
        verify(jqlOperandResolver).getValues(queryCreationContext, clause.getOperand(), clause);
        verify(jqlOperandResolver).isListOperand(blah);
        verify(jqlDateSupport).convertToDate(now.getTime());
    }

    @Test
    public void testGetQueryInMatchesMultiple() throws Exception {
        final Date now = new Date();

        final Operand blah = new SingleValueOperand(now.getTime());

        final UserHistoryItem item1 = new UserHistoryItem(UserHistoryItem.ISSUE, "00001", now.getTime());
        final UserHistoryItem item2 = new UserHistoryItem(UserHistoryItem.ISSUE, "00002", 200l);
        final UserHistoryItem item3 = new UserHistoryItem(UserHistoryItem.ISSUE, "00003", now.getTime());

        final List<UserHistoryItem> history = CollectionBuilder.list(item1, item2, item3);
        when(jqlOperandResolver.isValidOperand(blah)).thenReturn(true);
        when(jqlOperandResolver.isEmptyOperand(blah)).thenReturn(false);
        when(userHistoryManager.getFullIssueHistoryWithoutPermissionChecks(theUser)).thenReturn(history);
        final TerminalClause clause = new TerminalClauseImpl("lastViewed", Operator.IN, blah);
        when(jqlOperandResolver.getValues(queryCreationContext, clause.getOperand(), clause)).thenReturn(CollectionBuilder.list(new QueryLiteral(clause.getOperand(), now.getTime())));
        when(jqlOperandResolver.isListOperand(blah)).thenReturn(true);
        when(jqlDateSupport.convertToDate(now.getTime())).thenReturn(now);

        final QueryFactoryResult result = factory.getQuery(queryCreationContext, clause);
        assertEquals("issue_id:00001 issue_id:00003", result.getLuceneQuery().toString());
        assertFalse(result.mustNotOccur());

        verify(jqlOperandResolver).isValidOperand(blah);
        verify(jqlOperandResolver).isEmptyOperand(blah);
        verify(jqlOperandResolver).getValues(queryCreationContext, clause.getOperand(), clause);
        verify(jqlOperandResolver).isListOperand(blah);
        verify(jqlDateSupport).convertToDate(now.getTime());
    }

    @Test
    public void testGetQueryInMultipleNoMatch() throws Exception {
        final Date now = new Date();

        final long otherTime = now.getTime() - 100l;
        final Operand blah = new MultiValueOperand(now.getTime(), otherTime);

        final UserHistoryItem item1 = new UserHistoryItem(UserHistoryItem.ISSUE, "00001", 100l);
        final UserHistoryItem item2 = new UserHistoryItem(UserHistoryItem.ISSUE, "00002", 200l);
        final UserHistoryItem item3 = new UserHistoryItem(UserHistoryItem.ISSUE, "00003", 300l);

        final List<UserHistoryItem> history = CollectionBuilder.list(item1, item2, item3);
        when(jqlOperandResolver.isValidOperand(blah)).thenReturn(true);
        when(jqlOperandResolver.isEmptyOperand(blah)).thenReturn(false);
        when(userHistoryManager.getFullIssueHistoryWithoutPermissionChecks(theUser)).thenReturn(history);
        final TerminalClause clause = new TerminalClauseImpl("lastViewed", Operator.IN, blah);
        when(jqlOperandResolver.getValues(queryCreationContext, clause.getOperand(), clause)).thenReturn(CollectionBuilder.list(new QueryLiteral(clause.getOperand(), now.getTime()), new QueryLiteral(clause.getOperand(), otherTime)));
        when(jqlOperandResolver.isListOperand(blah)).thenReturn(true);
        when(jqlDateSupport.convertToDate(now.getTime())).thenReturn(now);
        when(jqlDateSupport.convertToDate(otherTime)).thenReturn(new Date(otherTime));

        final QueryFactoryResult result = factory.getQuery(queryCreationContext, clause);
        assertEquals("", result.getLuceneQuery().toString());
        assertFalse(result.mustNotOccur());

        verify(jqlOperandResolver).isValidOperand(blah);
        verify(jqlOperandResolver).isEmptyOperand(blah);
        verify(jqlOperandResolver).getValues(queryCreationContext, clause.getOperand(), clause);
        verify(jqlOperandResolver).isListOperand(blah);
        verify(jqlDateSupport).convertToDate(now.getTime());
        verify(jqlDateSupport).convertToDate(otherTime);
    }

    @Test
    public void testGetQueryInMultipleSingleMatch() throws Exception {
        final Date now = new Date();

        final long otherTime = now.getTime() - 100l;
        final Operand blah = new MultiValueOperand(now.getTime(), otherTime);

        final UserHistoryItem item1 = new UserHistoryItem(UserHistoryItem.ISSUE, "00001", now.getTime());
        final UserHistoryItem item2 = new UserHistoryItem(UserHistoryItem.ISSUE, "00002", 200l);
        final UserHistoryItem item3 = new UserHistoryItem(UserHistoryItem.ISSUE, "00003", 300l);

        final List<UserHistoryItem> history = CollectionBuilder.list(item1, item2, item3);
        when(jqlOperandResolver.isValidOperand(blah)).thenReturn(true);
        when(jqlOperandResolver.isEmptyOperand(blah)).thenReturn(false);
        when(userHistoryManager.getFullIssueHistoryWithoutPermissionChecks(theUser)).thenReturn(history);
        final TerminalClause clause = new TerminalClauseImpl("lastViewed", Operator.IN, blah);
        when(jqlOperandResolver.getValues(queryCreationContext, clause.getOperand(), clause)).thenReturn(CollectionBuilder.list(new QueryLiteral(clause.getOperand(), now.getTime()), new QueryLiteral(clause.getOperand(), otherTime)));
        when(jqlOperandResolver.isListOperand(blah)).thenReturn(true);
        when(jqlDateSupport.convertToDate(now.getTime())).thenReturn(now);
        when(jqlDateSupport.convertToDate(otherTime)).thenReturn(new Date(otherTime));

        final QueryFactoryResult result = factory.getQuery(queryCreationContext, clause);
        assertEquals("issue_id:00001", result.getLuceneQuery().toString());
        assertFalse(result.mustNotOccur());

        verify(jqlOperandResolver).isValidOperand(blah);
        verify(jqlOperandResolver).isEmptyOperand(blah);
        verify(jqlOperandResolver).getValues(queryCreationContext, clause.getOperand(), clause);
        verify(jqlOperandResolver).isListOperand(blah);
        verify(jqlDateSupport).convertToDate(now.getTime());
        verify(jqlDateSupport).convertToDate(otherTime);
    }

    @Test
    public void testGetQueryInMultipleMultipleMatch() throws Exception {
        final Date now = new Date();

        final long otherTime = now.getTime() - 100l;
        final Operand blah = new MultiValueOperand(now.getTime(), otherTime);

        final UserHistoryItem item1 = new UserHistoryItem(UserHistoryItem.ISSUE, "00001", now.getTime());
        final UserHistoryItem item2 = new UserHistoryItem(UserHistoryItem.ISSUE, "00002", 200l);
        final UserHistoryItem item3 = new UserHistoryItem(UserHistoryItem.ISSUE, "00003", otherTime);

        final List<UserHistoryItem> history = CollectionBuilder.list(item1, item2, item3);
        when(jqlOperandResolver.isValidOperand(blah)).thenReturn(true);
        when(jqlOperandResolver.isEmptyOperand(blah)).thenReturn(false);
        when(userHistoryManager.getFullIssueHistoryWithoutPermissionChecks(theUser)).thenReturn(history);
        final TerminalClause clause = new TerminalClauseImpl("lastViewed", Operator.IN, blah);
        when(jqlOperandResolver.getValues(queryCreationContext, clause.getOperand(), clause)).thenReturn(CollectionBuilder.list(new QueryLiteral(clause.getOperand(), now.getTime()), new QueryLiteral(clause.getOperand(), otherTime)));
        when(jqlOperandResolver.isListOperand(blah)).thenReturn(true);
        when(jqlDateSupport.convertToDate(now.getTime())).thenReturn(now);
        when(jqlDateSupport.convertToDate(otherTime)).thenReturn(new Date(otherTime));

        final QueryFactoryResult result = factory.getQuery(queryCreationContext, clause);
        assertEquals("issue_id:00003 issue_id:00001", result.getLuceneQuery().toString());
        assertFalse(result.mustNotOccur());

        verify(jqlOperandResolver).isValidOperand(blah);
        verify(jqlOperandResolver).isEmptyOperand(blah);
        verify(jqlOperandResolver).getValues(queryCreationContext, clause.getOperand(), clause);
        verify(jqlOperandResolver).isListOperand(blah);
        verify(jqlDateSupport).convertToDate(now.getTime());
        verify(jqlDateSupport).convertToDate(otherTime);
    }

    @Test
    public void testGetQueryInMultipleWithEmptyNoneMatch() throws Exception {
        final Date now = new Date();

        final Operand blah = new MultiValueOperand(new SingleValueOperand(now.getTime()), new EmptyOperand());

        final UserHistoryItem item1 = new UserHistoryItem(UserHistoryItem.ISSUE, "00001", 100l);
        final UserHistoryItem item2 = new UserHistoryItem(UserHistoryItem.ISSUE, "00002", 200l);
        final UserHistoryItem item3 = new UserHistoryItem(UserHistoryItem.ISSUE, "00003", 300l);

        final List<UserHistoryItem> history = CollectionBuilder.list(item1, item2, item3);
        when(jqlOperandResolver.isValidOperand(blah)).thenReturn(true);
        when(jqlOperandResolver.isEmptyOperand(blah)).thenReturn(false);
        when(userHistoryManager.getFullIssueHistoryWithoutPermissionChecks(theUser)).thenReturn(history);
        final TerminalClause clause = new TerminalClauseImpl("lastViewed", Operator.IN, blah);
        when(jqlOperandResolver.getValues(queryCreationContext, clause.getOperand(), clause)).thenReturn(CollectionBuilder.list(new QueryLiteral(clause.getOperand(), now.getTime()), new QueryLiteral(clause.getOperand(), (Long) null)));
        when(jqlOperandResolver.isListOperand(blah)).thenReturn(true);
        when(jqlDateSupport.convertToDate(now.getTime())).thenReturn(now);

        final QueryFactoryResult result = factory.getQuery(queryCreationContext, clause);
        assertEquals("-issue_id:00001 -issue_id:00002 -issue_id:00003", result.getLuceneQuery().toString());
        assertFalse(result.mustNotOccur());

        verify(jqlOperandResolver).isValidOperand(blah);
        verify(jqlOperandResolver).isEmptyOperand(blah);
        verify(jqlOperandResolver).getValues(queryCreationContext, clause.getOperand(), clause);
        verify(jqlOperandResolver).isListOperand(blah);
        verify(jqlDateSupport).convertToDate(now.getTime());
    }

    @Test
    public void testGetQueryInMultipleWithEmptyWithMatch() throws Exception {
        final Date now = new Date();

        final Operand blah = new MultiValueOperand(new SingleValueOperand(now.getTime()), new EmptyOperand());

        final UserHistoryItem item1 = new UserHistoryItem(UserHistoryItem.ISSUE, "00001", now.getTime());
        final UserHistoryItem item2 = new UserHistoryItem(UserHistoryItem.ISSUE, "00002", 200l);
        final UserHistoryItem item3 = new UserHistoryItem(UserHistoryItem.ISSUE, "00003", 300l);

        final List<UserHistoryItem> history = CollectionBuilder.list(item1, item2, item3);
        when(jqlOperandResolver.isValidOperand(blah)).thenReturn(true);
        when(jqlOperandResolver.isEmptyOperand(blah)).thenReturn(false);
        when(userHistoryManager.getFullIssueHistoryWithoutPermissionChecks(theUser)).thenReturn(history);
        final TerminalClause clause = new TerminalClauseImpl("lastViewed", Operator.IN, blah);
        when(jqlOperandResolver.getValues(queryCreationContext, clause.getOperand(), clause)).thenReturn(CollectionBuilder.list(new QueryLiteral(clause.getOperand(), now.getTime()), new QueryLiteral(clause.getOperand(), (Long) null)));
        when(jqlOperandResolver.isListOperand(blah)).thenReturn(true);
        when(jqlDateSupport.convertToDate(now.getTime())).thenReturn(now);

        final QueryFactoryResult result = factory.getQuery(queryCreationContext, clause);
        assertEquals("-issue_id:00002 -issue_id:00003", result.getLuceneQuery().toString());
        assertFalse(result.mustNotOccur());

        verify(jqlOperandResolver).isValidOperand(blah);
        verify(jqlOperandResolver).isEmptyOperand(blah);
        verify(jqlOperandResolver).getValues(queryCreationContext, clause.getOperand(), clause);
        verify(jqlOperandResolver).isListOperand(blah);
        verify(jqlDateSupport).convertToDate(now.getTime());
    }

    @Test
    public void testGetQueryInMultipleWithEmptyWithMultipleMatch() throws Exception {
        final Date now = new Date();
        final long otherTime = now.getTime() - 100l;

        final Operand blah = new MultiValueOperand(new SingleValueOperand(now.getTime()), new EmptyOperand(), new SingleValueOperand(otherTime));

        final UserHistoryItem item1 = new UserHistoryItem(UserHistoryItem.ISSUE, "00001", now.getTime());
        final UserHistoryItem item2 = new UserHistoryItem(UserHistoryItem.ISSUE, "00002", 200l);
        final UserHistoryItem item3 = new UserHistoryItem(UserHistoryItem.ISSUE, "00003", otherTime);

        final List<UserHistoryItem> history = CollectionBuilder.list(item1, item2, item3);
        when(jqlOperandResolver.isValidOperand(blah)).thenReturn(true);
        when(jqlOperandResolver.isEmptyOperand(blah)).thenReturn(false);
        when(userHistoryManager.getFullIssueHistoryWithoutPermissionChecks(theUser)).thenReturn(history);
        final TerminalClause clause = new TerminalClauseImpl("lastViewed", Operator.IN, blah);
        when(jqlOperandResolver.getValues(queryCreationContext, clause.getOperand(), clause)).thenReturn(CollectionBuilder.list(new QueryLiteral(clause.getOperand(), now.getTime()), new QueryLiteral(clause.getOperand(), (Long) null), new QueryLiteral(clause.getOperand(), otherTime)));
        when(jqlOperandResolver.isListOperand(blah)).thenReturn(true);
        when(jqlDateSupport.convertToDate(now.getTime())).thenReturn(now);
        when(jqlDateSupport.convertToDate(otherTime)).thenReturn(new Date(otherTime));

        final QueryFactoryResult result = factory.getQuery(queryCreationContext, clause);
        assertEquals("-issue_id:00002", result.getLuceneQuery().toString());
        assertFalse(result.mustNotOccur());

        verify(jqlOperandResolver).isValidOperand(blah);
        verify(jqlOperandResolver).isEmptyOperand(blah);
        verify(jqlOperandResolver).getValues(queryCreationContext, clause.getOperand(), clause);
        verify(jqlOperandResolver).isListOperand(blah);
        verify(jqlDateSupport).convertToDate(now.getTime());
        verify(jqlDateSupport).convertToDate(otherTime);
    }

    @Test
    public void testGetQueryNotEquals() throws Exception {
        final Date now = new Date();

        final Operand blah = new SingleValueOperand(now.getTime());

        final UserHistoryItem item1 = new UserHistoryItem(UserHistoryItem.ISSUE, "00001", now.getTime());
        final UserHistoryItem item2 = new UserHistoryItem(UserHistoryItem.ISSUE, "00002", 200l);

        final List<UserHistoryItem> history = CollectionBuilder.list(item1, item2);
        when(jqlOperandResolver.isValidOperand(blah)).thenReturn(true);
        when(jqlOperandResolver.isEmptyOperand(blah)).thenReturn(false);
        when(userHistoryManager.getFullIssueHistoryWithoutPermissionChecks(theUser)).thenReturn(history);
        final TerminalClause clause = new TerminalClauseImpl("lastViewed", Operator.NOT_EQUALS, blah);
        when(jqlOperandResolver.getValues(queryCreationContext, clause.getOperand(), clause)).thenReturn(CollectionBuilder.list(new QueryLiteral(clause.getOperand(), now.getTime())));
        when(jqlOperandResolver.isListOperand(blah)).thenReturn(false);
        when(jqlDateSupport.convertToDate(now.getTime())).thenReturn(now);

        final QueryFactoryResult result = factory.getQuery(queryCreationContext, clause);
        assertEquals("issue_id:00001", result.getLuceneQuery().toString());
        assertTrue(result.mustNotOccur());

        verify(jqlOperandResolver).isValidOperand(blah);
        verify(jqlOperandResolver).isEmptyOperand(blah);
        verify(jqlOperandResolver).getValues(queryCreationContext, clause.getOperand(), clause);
        verify(jqlOperandResolver).isListOperand(blah);
        verify(jqlDateSupport).convertToDate(now.getTime());
    }

    @Test
    public void testGetQueryNotEqualsNoneMatch() throws Exception {
        final Date now = new Date();

        final Operand blah = new SingleValueOperand(now.getTime());

        final UserHistoryItem item1 = new UserHistoryItem(UserHistoryItem.ISSUE, "00001", 100l);
        final UserHistoryItem item2 = new UserHistoryItem(UserHistoryItem.ISSUE, "00002", 200l);

        final List<UserHistoryItem> history = CollectionBuilder.list(item1, item2);
        when(jqlOperandResolver.isValidOperand(blah)).thenReturn(true);
        when(jqlOperandResolver.isEmptyOperand(blah)).thenReturn(false);
        when(userHistoryManager.getFullIssueHistoryWithoutPermissionChecks(theUser)).thenReturn(history);
        final TerminalClause clause = new TerminalClauseImpl("lastViewed", Operator.NOT_EQUALS, blah);
        when(jqlOperandResolver.getValues(queryCreationContext, clause.getOperand(), clause)).thenReturn(CollectionBuilder.list(new QueryLiteral(clause.getOperand(), now.getTime())));
        when(jqlOperandResolver.isListOperand(blah)).thenReturn(false);
        when(jqlDateSupport.convertToDate(now.getTime())).thenReturn(now);

        final QueryFactoryResult result = factory.getQuery(queryCreationContext, clause);
        assertEquals("", result.getLuceneQuery().toString());
        assertTrue(result.mustNotOccur());

        verify(jqlOperandResolver).isValidOperand(blah);
        verify(jqlOperandResolver).isEmptyOperand(blah);
        verify(jqlOperandResolver).getValues(queryCreationContext, clause.getOperand(), clause);
        verify(jqlOperandResolver).isListOperand(blah);
        verify(jqlDateSupport).convertToDate(now.getTime());
    }

    @Test
    public void testGetQueryNotIn() throws Exception {
        final Date now = new Date();

        final Operand blah = new SingleValueOperand(now.getTime());

        final UserHistoryItem item1 = new UserHistoryItem(UserHistoryItem.ISSUE, "00001", now.getTime());
        final UserHistoryItem item2 = new UserHistoryItem(UserHistoryItem.ISSUE, "00002", 200l);

        final List<UserHistoryItem> history = CollectionBuilder.list(item1, item2);
        when(jqlOperandResolver.isValidOperand(blah)).thenReturn(true);
        when(jqlOperandResolver.isEmptyOperand(blah)).thenReturn(false);
        when(userHistoryManager.getFullIssueHistoryWithoutPermissionChecks(theUser)).thenReturn(history);
        final TerminalClause clause = new TerminalClauseImpl("lastViewed", Operator.NOT_IN, blah);
        when(jqlOperandResolver.getValues(queryCreationContext, clause.getOperand(), clause)).thenReturn(CollectionBuilder.list(new QueryLiteral(clause.getOperand(), now.getTime())));
        when(jqlOperandResolver.isListOperand(blah)).thenReturn(true);
        when(jqlDateSupport.convertToDate(now.getTime())).thenReturn(now);

        final QueryFactoryResult result = factory.getQuery(queryCreationContext, clause);
        assertEquals("issue_id:00001", result.getLuceneQuery().toString());
        assertTrue(result.mustNotOccur());

        verify(jqlOperandResolver).isValidOperand(blah);
        verify(jqlOperandResolver).isEmptyOperand(blah);
        verify(jqlOperandResolver).getValues(queryCreationContext, clause.getOperand(), clause);
        verify(jqlOperandResolver).isListOperand(blah);
        verify(jqlDateSupport).convertToDate(now.getTime());
    }

    @Test
    public void testGetQueryNotInNoneMatch() throws Exception {
        final Date now = new Date();

        final Operand blah = new SingleValueOperand(now.getTime());

        final UserHistoryItem item1 = new UserHistoryItem(UserHistoryItem.ISSUE, "00001", 100l);
        final UserHistoryItem item2 = new UserHistoryItem(UserHistoryItem.ISSUE, "00002", 200l);

        final List<UserHistoryItem> history = CollectionBuilder.list(item1, item2);
        when(jqlOperandResolver.isValidOperand(blah)).thenReturn(true);
        when(jqlOperandResolver.isEmptyOperand(blah)).thenReturn(false);
        when(userHistoryManager.getFullIssueHistoryWithoutPermissionChecks(theUser)).thenReturn(history);
        final TerminalClause clause = new TerminalClauseImpl("lastViewed", Operator.NOT_IN, blah);
        when(jqlOperandResolver.getValues(queryCreationContext, clause.getOperand(), clause)).thenReturn(CollectionBuilder.list(new QueryLiteral(clause.getOperand(), now.getTime())));
        when(jqlOperandResolver.isListOperand(blah)).thenReturn(true);
        when(jqlDateSupport.convertToDate(now.getTime())).thenReturn(now);

        final QueryFactoryResult result = factory.getQuery(queryCreationContext, clause);
        assertEquals("", result.getLuceneQuery().toString());
        assertTrue(result.mustNotOccur());

        verify(jqlOperandResolver).isValidOperand(blah);
        verify(jqlOperandResolver).isEmptyOperand(blah);
        verify(jqlOperandResolver).getValues(queryCreationContext, clause.getOperand(), clause);
        verify(jqlOperandResolver).isListOperand(blah);
        verify(jqlDateSupport).convertToDate(now.getTime());
    }

    @Test
    public void testGetQueryNotEqualsMatchesMultiple() throws Exception {
        final Date now = new Date();

        final Operand blah = new SingleValueOperand(now.getTime());

        final UserHistoryItem item1 = new UserHistoryItem(UserHistoryItem.ISSUE, "00001", now.getTime());
        final UserHistoryItem item2 = new UserHistoryItem(UserHistoryItem.ISSUE, "00002", 200l);
        final UserHistoryItem item3 = new UserHistoryItem(UserHistoryItem.ISSUE, "00003", now.getTime());

        final List<UserHistoryItem> history = CollectionBuilder.list(item1, item2, item3);
        when(jqlOperandResolver.isValidOperand(blah)).thenReturn(true);
        when(jqlOperandResolver.isEmptyOperand(blah)).thenReturn(false);
        when(userHistoryManager.getFullIssueHistoryWithoutPermissionChecks(theUser)).thenReturn(history);
        final TerminalClause clause = new TerminalClauseImpl("lastViewed", Operator.NOT_EQUALS, blah);
        when(jqlOperandResolver.getValues(queryCreationContext, clause.getOperand(), clause)).thenReturn(CollectionBuilder.list(new QueryLiteral(clause.getOperand(), now.getTime())));
        when(jqlOperandResolver.isListOperand(blah)).thenReturn(false);
        when(jqlDateSupport.convertToDate(now.getTime())).thenReturn(now);

        final QueryFactoryResult result = factory.getQuery(queryCreationContext, clause);
        assertEquals("issue_id:00001 issue_id:00003", result.getLuceneQuery().toString());
        assertTrue(result.mustNotOccur());

        verify(jqlOperandResolver).isValidOperand(blah);
        verify(jqlOperandResolver).isEmptyOperand(blah);
        verify(jqlOperandResolver).getValues(queryCreationContext, clause.getOperand(), clause);
        verify(jqlOperandResolver).isListOperand(blah);
        verify(jqlDateSupport).convertToDate(now.getTime());
    }

    @Test
    public void testGetQueryNotInMatchesMultiple() throws Exception {
        final Date now = new Date();

        final Operand blah = new SingleValueOperand(now.getTime());

        final UserHistoryItem item1 = new UserHistoryItem(UserHistoryItem.ISSUE, "00001", now.getTime());
        final UserHistoryItem item2 = new UserHistoryItem(UserHistoryItem.ISSUE, "00002", 200l);
        final UserHistoryItem item3 = new UserHistoryItem(UserHistoryItem.ISSUE, "00003", now.getTime());

        final List<UserHistoryItem> history = CollectionBuilder.list(item1, item2, item3);
        when(jqlOperandResolver.isValidOperand(blah)).thenReturn(true);
        when(jqlOperandResolver.isEmptyOperand(blah)).thenReturn(false);
        when(userHistoryManager.getFullIssueHistoryWithoutPermissionChecks(theUser)).thenReturn(history);
        final TerminalClause clause = new TerminalClauseImpl("lastViewed", Operator.NOT_IN, blah);
        when(jqlOperandResolver.getValues(queryCreationContext, clause.getOperand(), clause)).thenReturn(CollectionBuilder.list(new QueryLiteral(clause.getOperand(), now.getTime())));
        when(jqlOperandResolver.isListOperand(blah)).thenReturn(true);
        when(jqlDateSupport.convertToDate(now.getTime())).thenReturn(now);

        final QueryFactoryResult result = factory.getQuery(queryCreationContext, clause);
        assertEquals("issue_id:00001 issue_id:00003", result.getLuceneQuery().toString());
        assertTrue(result.mustNotOccur());

        verify(jqlOperandResolver).isValidOperand(blah);
        verify(jqlOperandResolver).isEmptyOperand(blah);
        verify(jqlOperandResolver).getValues(queryCreationContext, clause.getOperand(), clause);
        verify(jqlOperandResolver).isListOperand(blah);
        verify(jqlDateSupport).convertToDate(now.getTime());
    }

    @Test
    public void testGetQueryNotInMultipleNoMatch() throws Exception {
        final Date now = new Date();

        final long otherTime = now.getTime() - 100l;
        final Operand blah = new MultiValueOperand(now.getTime(), otherTime);

        final UserHistoryItem item1 = new UserHistoryItem(UserHistoryItem.ISSUE, "00001", 100l);
        final UserHistoryItem item2 = new UserHistoryItem(UserHistoryItem.ISSUE, "00002", 200l);
        final UserHistoryItem item3 = new UserHistoryItem(UserHistoryItem.ISSUE, "00003", 300l);

        final List<UserHistoryItem> history = CollectionBuilder.list(item1, item2, item3);
        when(jqlOperandResolver.isValidOperand(blah)).thenReturn(true);
        when(jqlOperandResolver.isEmptyOperand(blah)).thenReturn(false);
        when(userHistoryManager.getFullIssueHistoryWithoutPermissionChecks(theUser)).thenReturn(history);
        final TerminalClause clause = new TerminalClauseImpl("lastViewed", Operator.NOT_IN, blah);
        when(jqlOperandResolver.getValues(queryCreationContext, clause.getOperand(), clause)).thenReturn(CollectionBuilder.list(new QueryLiteral(clause.getOperand(), now.getTime()), new QueryLiteral(clause.getOperand(), otherTime)));
        when(jqlOperandResolver.isListOperand(blah)).thenReturn(true);
        when(jqlDateSupport.convertToDate(now.getTime())).thenReturn(now);
        when(jqlDateSupport.convertToDate(otherTime)).thenReturn(new Date(otherTime));

        final QueryFactoryResult result = factory.getQuery(queryCreationContext, clause);
        assertEquals("", result.getLuceneQuery().toString());
        assertTrue(result.mustNotOccur());

        verify(jqlOperandResolver).isValidOperand(blah);
        verify(jqlOperandResolver).isEmptyOperand(blah);
        verify(jqlOperandResolver).getValues(queryCreationContext, clause.getOperand(), clause);
        verify(jqlOperandResolver).isListOperand(blah);
        verify(jqlDateSupport).convertToDate(now.getTime());
        verify(jqlDateSupport).convertToDate(otherTime);
    }

    @Test
    public void testGetQueryNotInMultipleSingleMatch() throws Exception {
        final Date now = new Date();

        final long otherTime = now.getTime() - 100l;
        final Operand blah = new MultiValueOperand(now.getTime(), otherTime);

        final UserHistoryItem item1 = new UserHistoryItem(UserHistoryItem.ISSUE, "00001", now.getTime());
        final UserHistoryItem item2 = new UserHistoryItem(UserHistoryItem.ISSUE, "00002", 200l);
        final UserHistoryItem item3 = new UserHistoryItem(UserHistoryItem.ISSUE, "00003", 300l);

        final List<UserHistoryItem> history = CollectionBuilder.list(item1, item2, item3);
        when(jqlOperandResolver.isValidOperand(blah)).thenReturn(true);
        when(jqlOperandResolver.isEmptyOperand(blah)).thenReturn(false);
        when(userHistoryManager.getFullIssueHistoryWithoutPermissionChecks(theUser)).thenReturn(history);
        final TerminalClause clause = new TerminalClauseImpl("lastViewed", Operator.NOT_IN, blah);
        when(jqlOperandResolver.getValues(queryCreationContext, clause.getOperand(), clause)).thenReturn(CollectionBuilder.list(new QueryLiteral(clause.getOperand(), now.getTime()), new QueryLiteral(clause.getOperand(), otherTime)));
        when(jqlOperandResolver.isListOperand(blah)).thenReturn(true);
        when(jqlDateSupport.convertToDate(now.getTime())).thenReturn(now);
        when(jqlDateSupport.convertToDate(otherTime)).thenReturn(new Date(otherTime));

        final QueryFactoryResult result = factory.getQuery(queryCreationContext, clause);
        assertEquals("issue_id:00001", result.getLuceneQuery().toString());
        assertTrue(result.mustNotOccur());

        verify(jqlOperandResolver).isValidOperand(blah);
        verify(jqlOperandResolver).isEmptyOperand(blah);
        verify(jqlOperandResolver).getValues(queryCreationContext, clause.getOperand(), clause);
        verify(jqlOperandResolver).isListOperand(blah);
        verify(jqlDateSupport).convertToDate(now.getTime());
        verify(jqlDateSupport).convertToDate(otherTime);
    }

    @Test
    public void testGetQueryNotInMultipleMultipleMatch() throws Exception {
        final Date now = new Date();

        final long otherTime = now.getTime() - 100l;
        final Operand blah = new MultiValueOperand(now.getTime(), otherTime);

        final UserHistoryItem item1 = new UserHistoryItem(UserHistoryItem.ISSUE, "00001", now.getTime());
        final UserHistoryItem item2 = new UserHistoryItem(UserHistoryItem.ISSUE, "00002", 200l);
        final UserHistoryItem item3 = new UserHistoryItem(UserHistoryItem.ISSUE, "00003", otherTime);

        final List<UserHistoryItem> history = CollectionBuilder.list(item1, item2, item3);
        when(jqlOperandResolver.isValidOperand(blah)).thenReturn(true);
        when(jqlOperandResolver.isEmptyOperand(blah)).thenReturn(false);
        when(userHistoryManager.getFullIssueHistoryWithoutPermissionChecks(theUser)).thenReturn(history);
        final TerminalClause clause = new TerminalClauseImpl("lastViewed", Operator.NOT_IN, blah);
        when(jqlOperandResolver.getValues(queryCreationContext, clause.getOperand(), clause)).thenReturn(CollectionBuilder.list(new QueryLiteral(clause.getOperand(), now.getTime()), new QueryLiteral(clause.getOperand(), otherTime)));
        when(jqlOperandResolver.isListOperand(blah)).thenReturn(true);
        when(jqlDateSupport.convertToDate(now.getTime())).thenReturn(now);
        when(jqlDateSupport.convertToDate(otherTime)).thenReturn(new Date(otherTime));

        final QueryFactoryResult result = factory.getQuery(queryCreationContext, clause);
        assertEquals("issue_id:00003 issue_id:00001", result.getLuceneQuery().toString());
        assertTrue(result.mustNotOccur());

        verify(jqlOperandResolver).isValidOperand(blah);
        verify(jqlOperandResolver).isEmptyOperand(blah);
        verify(jqlOperandResolver).getValues(queryCreationContext, clause.getOperand(), clause);
        verify(jqlOperandResolver).isListOperand(blah);
        verify(jqlDateSupport).convertToDate(now.getTime());
        verify(jqlDateSupport).convertToDate(otherTime);
    }

    @Test
    public void testGetQueryNotInMultipleWithEmptyNoneMatch() throws Exception {
        final Date now = new Date();

        final Operand blah = new MultiValueOperand(new SingleValueOperand(now.getTime()), new EmptyOperand());

        final UserHistoryItem item1 = new UserHistoryItem(UserHistoryItem.ISSUE, "00001", 100l);
        final UserHistoryItem item2 = new UserHistoryItem(UserHistoryItem.ISSUE, "00002", 200l);
        final UserHistoryItem item3 = new UserHistoryItem(UserHistoryItem.ISSUE, "00003", 300l);

        final List<UserHistoryItem> history = CollectionBuilder.list(item1, item2, item3);
        when(jqlOperandResolver.isValidOperand(blah)).thenReturn(true);
        when(jqlOperandResolver.isEmptyOperand(blah)).thenReturn(false);
        when(userHistoryManager.getFullIssueHistoryWithoutPermissionChecks(theUser)).thenReturn(history);
        final TerminalClause clause = new TerminalClauseImpl("lastViewed", Operator.NOT_IN, blah);
        when(jqlOperandResolver.getValues(queryCreationContext, clause.getOperand(), clause)).thenReturn(CollectionBuilder.list(new QueryLiteral(clause.getOperand(), now.getTime()), new QueryLiteral(clause.getOperand(), (Long) null)));
        when(jqlOperandResolver.isListOperand(blah)).thenReturn(true);
        when(jqlDateSupport.convertToDate(now.getTime())).thenReturn(now);

        final QueryFactoryResult result = factory.getQuery(queryCreationContext, clause);
        assertEquals("-issue_id:00001 -issue_id:00002 -issue_id:00003", result.getLuceneQuery().toString());
        assertTrue(result.mustNotOccur());

        verify(jqlOperandResolver).isValidOperand(blah);
        verify(jqlOperandResolver).isEmptyOperand(blah);
        verify(jqlOperandResolver).getValues(queryCreationContext, clause.getOperand(), clause);
        verify(jqlOperandResolver).isListOperand(blah);
        verify(jqlDateSupport).convertToDate(now.getTime());
    }

    @Test
    public void testGetQueryNotInMultipleWithEmptyWithMatch() throws Exception {
        final Date now = new Date();

        final Operand blah = new MultiValueOperand(new SingleValueOperand(now.getTime()), new EmptyOperand());

        final UserHistoryItem item1 = new UserHistoryItem(UserHistoryItem.ISSUE, "00001", now.getTime());
        final UserHistoryItem item2 = new UserHistoryItem(UserHistoryItem.ISSUE, "00002", 200l);
        final UserHistoryItem item3 = new UserHistoryItem(UserHistoryItem.ISSUE, "00003", 300l);

        final List<UserHistoryItem> history = CollectionBuilder.list(item1, item2, item3);
        when(jqlOperandResolver.isValidOperand(blah)).thenReturn(true);
        when(jqlOperandResolver.isEmptyOperand(blah)).thenReturn(false);
        when(userHistoryManager.getFullIssueHistoryWithoutPermissionChecks(theUser)).thenReturn(history);
        final TerminalClause clause = new TerminalClauseImpl("lastViewed", Operator.NOT_IN, blah);
        when(jqlOperandResolver.getValues(queryCreationContext, clause.getOperand(), clause)).thenReturn(CollectionBuilder.list(new QueryLiteral(clause.getOperand(), now.getTime()), new QueryLiteral(clause.getOperand(), (Long) null)));
        when(jqlOperandResolver.isListOperand(blah)).thenReturn(true);
        when(jqlDateSupport.convertToDate(now.getTime())).thenReturn(now);

        final QueryFactoryResult result = factory.getQuery(queryCreationContext, clause);
        assertEquals("-issue_id:00002 -issue_id:00003", result.getLuceneQuery().toString());
        assertTrue(result.mustNotOccur());

        verify(jqlOperandResolver).isValidOperand(blah);
        verify(jqlOperandResolver).isEmptyOperand(blah);
        verify(jqlOperandResolver).getValues(queryCreationContext, clause.getOperand(), clause);
        verify(jqlOperandResolver).isListOperand(blah);
        verify(jqlDateSupport).convertToDate(now.getTime());
    }

    @Test
    public void testGetQueryNotInMultipleWithEmptyWithMultipleMatch() throws Exception {
        final Date now = new Date();
        final long otherTime = now.getTime() - 100l;

        final Operand blah = new MultiValueOperand(new SingleValueOperand(now.getTime()), new EmptyOperand(), new SingleValueOperand(otherTime));

        final UserHistoryItem item1 = new UserHistoryItem(UserHistoryItem.ISSUE, "00001", now.getTime());
        final UserHistoryItem item2 = new UserHistoryItem(UserHistoryItem.ISSUE, "00002", 200l);
        final UserHistoryItem item3 = new UserHistoryItem(UserHistoryItem.ISSUE, "00003", otherTime);

        final List<UserHistoryItem> history = CollectionBuilder.list(item1, item2, item3);
        when(jqlOperandResolver.isValidOperand(blah)).thenReturn(true);
        when(jqlOperandResolver.isEmptyOperand(blah)).thenReturn(false);
        when(userHistoryManager.getFullIssueHistoryWithoutPermissionChecks(theUser)).thenReturn(history);
        final TerminalClause clause = new TerminalClauseImpl("lastViewed", Operator.NOT_IN, blah);
        when(jqlOperandResolver.getValues(queryCreationContext, clause.getOperand(), clause)).thenReturn(CollectionBuilder.list(new QueryLiteral(clause.getOperand(), now.getTime()), new QueryLiteral(clause.getOperand(), (Long) null), new QueryLiteral(clause.getOperand(), otherTime)));
        when(jqlOperandResolver.isListOperand(blah)).thenReturn(true);
        when(jqlDateSupport.convertToDate(now.getTime())).thenReturn(now);
        when(jqlDateSupport.convertToDate(otherTime)).thenReturn(new Date(otherTime));

        final QueryFactoryResult result = factory.getQuery(queryCreationContext, clause);
        assertEquals("-issue_id:00002", result.getLuceneQuery().toString());
        assertTrue(result.mustNotOccur());

        verify(jqlOperandResolver).isValidOperand(blah);
        verify(jqlOperandResolver).isEmptyOperand(blah);
        verify(jqlOperandResolver).getValues(queryCreationContext, clause.getOperand(), clause);
        verify(jqlOperandResolver).isListOperand(blah);
        verify(jqlDateSupport).convertToDate(now.getTime());
        verify(jqlDateSupport).convertToDate(otherTime);
    }

    // >
    @Test
    public void testGetQueryGreaterThanNoMatches() throws Exception {
        final long compareTime = 300l;
        final Operand blah = new SingleValueOperand(compareTime);

        final UserHistoryItem item1 = new UserHistoryItem(UserHistoryItem.ISSUE, "00001", 100l);
        final UserHistoryItem item2 = new UserHistoryItem(UserHistoryItem.ISSUE, "00002", 200l);
        final UserHistoryItem item3 = new UserHistoryItem(UserHistoryItem.ISSUE, "00003", 300l);

        final List<UserHistoryItem> history = CollectionBuilder.list(item1, item2, item3);
        when(jqlOperandResolver.isValidOperand(blah)).thenReturn(true);
        when(jqlOperandResolver.isEmptyOperand(blah)).thenReturn(false);
        when(userHistoryManager.getFullIssueHistoryWithoutPermissionChecks(theUser)).thenReturn(history);
        final TerminalClause clause = new TerminalClauseImpl("lastViewed", Operator.GREATER_THAN, blah);
        when(jqlOperandResolver.getValues(queryCreationContext, clause.getOperand(), clause)).thenReturn(CollectionBuilder.list(new QueryLiteral(clause.getOperand(), compareTime)));
        when(jqlOperandResolver.isListOperand(blah)).thenReturn(false);
        when(jqlDateSupport.convertToDate(compareTime)).thenReturn(new Date(compareTime));

        final QueryFactoryResult result = factory.getQuery(queryCreationContext, clause);
        assertEquals("", result.getLuceneQuery().toString());
        assertFalse(result.mustNotOccur());

        verify(jqlOperandResolver).isValidOperand(blah);
        verify(jqlOperandResolver).isEmptyOperand(blah);
        verify(jqlOperandResolver).getValues(queryCreationContext, clause.getOperand(), clause);
        verify(jqlOperandResolver).isListOperand(blah);
        verify(jqlDateSupport).convertToDate(compareTime);
    }

    @Test
    public void testGetQueryGreaterThanMatches() throws Exception {
        final long compareTime = 200l;
        final Operand blah = new SingleValueOperand(compareTime);

        final UserHistoryItem item1 = new UserHistoryItem(UserHistoryItem.ISSUE, "00001", 100l);
        final UserHistoryItem item2 = new UserHistoryItem(UserHistoryItem.ISSUE, "00002", 200l);
        final UserHistoryItem item3 = new UserHistoryItem(UserHistoryItem.ISSUE, "00003", 300l);

        final List<UserHistoryItem> history = CollectionBuilder.list(item1, item2, item3);
        when(jqlOperandResolver.isValidOperand(blah)).thenReturn(true);
        when(jqlOperandResolver.isEmptyOperand(blah)).thenReturn(false);
        when(userHistoryManager.getFullIssueHistoryWithoutPermissionChecks(theUser)).thenReturn(history);
        final TerminalClause clause = new TerminalClauseImpl("lastViewed", Operator.GREATER_THAN, blah);
        when(jqlOperandResolver.getValues(queryCreationContext, clause.getOperand(), clause)).thenReturn(CollectionBuilder.list(new QueryLiteral(clause.getOperand(), compareTime)));
        when(jqlOperandResolver.isListOperand(blah)).thenReturn(false);
        when(jqlDateSupport.convertToDate(compareTime)).thenReturn(new Date(compareTime));

        final QueryFactoryResult result = factory.getQuery(queryCreationContext, clause);
        assertEquals("issue_id:00003", result.getLuceneQuery().toString());
        assertFalse(result.mustNotOccur());

        verify(jqlOperandResolver).isValidOperand(blah);
        verify(jqlOperandResolver).isEmptyOperand(blah);
        verify(jqlOperandResolver).getValues(queryCreationContext, clause.getOperand(), clause);
        verify(jqlOperandResolver).isListOperand(blah);
        verify(jqlDateSupport).convertToDate(compareTime);
    }

    // >=
    @Test
    public void testGetQueryGreaterEqualsThanNoMatches() throws Exception {
        final long compareTime = 400l;
        final Operand blah = new SingleValueOperand(compareTime);

        final UserHistoryItem item1 = new UserHistoryItem(UserHistoryItem.ISSUE, "00001", 100l);
        final UserHistoryItem item2 = new UserHistoryItem(UserHistoryItem.ISSUE, "00002", 200l);
        final UserHistoryItem item3 = new UserHistoryItem(UserHistoryItem.ISSUE, "00003", 300l);

        final List<UserHistoryItem> history = CollectionBuilder.list(item1, item2, item3);
        when(jqlOperandResolver.isValidOperand(blah)).thenReturn(true);
        when(jqlOperandResolver.isEmptyOperand(blah)).thenReturn(false);
        when(userHistoryManager.getFullIssueHistoryWithoutPermissionChecks(theUser)).thenReturn(history);
        final TerminalClause clause = new TerminalClauseImpl("lastViewed", Operator.GREATER_THAN_EQUALS, blah);
        when(jqlOperandResolver.getValues(queryCreationContext, clause.getOperand(), clause)).thenReturn(CollectionBuilder.list(new QueryLiteral(clause.getOperand(), compareTime)));
        when(jqlOperandResolver.isListOperand(blah)).thenReturn(false);
        when(jqlDateSupport.convertToDate(compareTime)).thenReturn(new Date(compareTime));

        final QueryFactoryResult result = factory.getQuery(queryCreationContext, clause);
        assertEquals("", result.getLuceneQuery().toString());
        assertFalse(result.mustNotOccur());

        verify(jqlOperandResolver).isValidOperand(blah);
        verify(jqlOperandResolver).isEmptyOperand(blah);
        verify(jqlOperandResolver).getValues(queryCreationContext, clause.getOperand(), clause);
        verify(jqlOperandResolver).isListOperand(blah);
        verify(jqlDateSupport).convertToDate(compareTime);
    }

    @Test
    public void testGetQueryGreaterEqualsThanMatches() throws Exception {
        final long compareTime = 200l;
        final Operand blah = new SingleValueOperand(compareTime);

        final UserHistoryItem item1 = new UserHistoryItem(UserHistoryItem.ISSUE, "00001", 100l);
        final UserHistoryItem item2 = new UserHistoryItem(UserHistoryItem.ISSUE, "00002", 200l);
        final UserHistoryItem item3 = new UserHistoryItem(UserHistoryItem.ISSUE, "00003", 300l);

        final List<UserHistoryItem> history = CollectionBuilder.list(item1, item2, item3);
        when(jqlOperandResolver.isValidOperand(blah)).thenReturn(true);
        when(jqlOperandResolver.isEmptyOperand(blah)).thenReturn(false);
        when(userHistoryManager.getFullIssueHistoryWithoutPermissionChecks(theUser)).thenReturn(history);
        final TerminalClause clause = new TerminalClauseImpl("lastViewed", Operator.GREATER_THAN_EQUALS, blah);
        when(jqlOperandResolver.getValues(queryCreationContext, clause.getOperand(), clause)).thenReturn(CollectionBuilder.list(new QueryLiteral(clause.getOperand(), compareTime)));
        when(jqlOperandResolver.isListOperand(blah)).thenReturn(false);
        when(jqlDateSupport.convertToDate(compareTime)).thenReturn(new Date(compareTime));

        final QueryFactoryResult result = factory.getQuery(queryCreationContext, clause);
        assertEquals("issue_id:00002 issue_id:00003", result.getLuceneQuery().toString());
        assertFalse(result.mustNotOccur());

        verify(jqlOperandResolver).isValidOperand(blah);
        verify(jqlOperandResolver).isEmptyOperand(blah);
        verify(jqlOperandResolver).getValues(queryCreationContext, clause.getOperand(), clause);
        verify(jqlOperandResolver).isListOperand(blah);
        verify(jqlDateSupport).convertToDate(compareTime);
    }

    // >
    @Test
    public void testGetQueryLessThanNoMatches() throws Exception {
        final long compareTime = 100l;
        final Operand blah = new SingleValueOperand(compareTime);

        final UserHistoryItem item1 = new UserHistoryItem(UserHistoryItem.ISSUE, "00001", 100l);
        final UserHistoryItem item2 = new UserHistoryItem(UserHistoryItem.ISSUE, "00002", 200l);
        final UserHistoryItem item3 = new UserHistoryItem(UserHistoryItem.ISSUE, "00003", 300l);

        final List<UserHistoryItem> history = CollectionBuilder.list(item1, item2, item3);
        when(jqlOperandResolver.isValidOperand(blah)).thenReturn(true);
        when(jqlOperandResolver.isEmptyOperand(blah)).thenReturn(false);
        when(userHistoryManager.getFullIssueHistoryWithoutPermissionChecks(theUser)).thenReturn(history);
        final TerminalClause clause = new TerminalClauseImpl("lastViewed", Operator.LESS_THAN, blah);
        when(jqlOperandResolver.getValues(queryCreationContext, clause.getOperand(), clause)).thenReturn(CollectionBuilder.list(new QueryLiteral(clause.getOperand(), compareTime)));
        when(jqlOperandResolver.isListOperand(blah)).thenReturn(false);
        when(jqlDateSupport.convertToDate(compareTime)).thenReturn(new Date(compareTime));

        final QueryFactoryResult result = factory.getQuery(queryCreationContext, clause);
        assertEquals("", result.getLuceneQuery().toString());
        assertFalse(result.mustNotOccur());

        verify(jqlOperandResolver).isValidOperand(blah);
        verify(jqlOperandResolver).isEmptyOperand(blah);
        verify(jqlOperandResolver).getValues(queryCreationContext, clause.getOperand(), clause);
        verify(jqlOperandResolver).isListOperand(blah);
        verify(jqlDateSupport).convertToDate(compareTime);
    }

    @Test
    public void testGetQueryLessThanMatches() throws Exception {
        final long compareTime = 200l;
        final Operand blah = new SingleValueOperand(compareTime);

        final UserHistoryItem item1 = new UserHistoryItem(UserHistoryItem.ISSUE, "00001", 100l);
        final UserHistoryItem item2 = new UserHistoryItem(UserHistoryItem.ISSUE, "00002", 200l);
        final UserHistoryItem item3 = new UserHistoryItem(UserHistoryItem.ISSUE, "00003", 300l);

        final List<UserHistoryItem> history = CollectionBuilder.list(item1, item2, item3);
        when(jqlOperandResolver.isValidOperand(blah)).thenReturn(true);
        when(jqlOperandResolver.isEmptyOperand(blah)).thenReturn(false);
        when(userHistoryManager.getFullIssueHistoryWithoutPermissionChecks(theUser)).thenReturn(history);
        final TerminalClause clause = new TerminalClauseImpl("lastViewed", Operator.LESS_THAN, blah);
        when(jqlOperandResolver.getValues(queryCreationContext, clause.getOperand(), clause)).thenReturn(CollectionBuilder.list(new QueryLiteral(clause.getOperand(), compareTime)));
        when(jqlOperandResolver.isListOperand(blah)).thenReturn(false);
        when(jqlDateSupport.convertToDate(compareTime)).thenReturn(new Date(compareTime));

        final QueryFactoryResult result = factory.getQuery(queryCreationContext, clause);
        assertEquals("issue_id:00001", result.getLuceneQuery().toString());
        assertFalse(result.mustNotOccur());

        verify(jqlOperandResolver).isValidOperand(blah);
        verify(jqlOperandResolver).isEmptyOperand(blah);
        verify(jqlOperandResolver).getValues(queryCreationContext, clause.getOperand(), clause);
        verify(jqlOperandResolver).isListOperand(blah);
        verify(jqlDateSupport).convertToDate(compareTime);
    }

    // >=
    @Test
    public void testGetQueryLessThanEqualsNoMatches() throws Exception {
        final long compareTime = 50l;
        final Operand blah = new SingleValueOperand(compareTime);

        final UserHistoryItem item1 = new UserHistoryItem(UserHistoryItem.ISSUE, "00001", 100l);

        final UserHistoryItem item2 = new UserHistoryItem(UserHistoryItem.ISSUE, "00002", 200l);
        final UserHistoryItem item3 = new UserHistoryItem(UserHistoryItem.ISSUE, "00003", 300l);

        final List<UserHistoryItem> history = CollectionBuilder.list(item1, item2, item3);
        when(jqlOperandResolver.isValidOperand(blah)).thenReturn(true);
        when(jqlOperandResolver.isEmptyOperand(blah)).thenReturn(false);
        when(userHistoryManager.getFullIssueHistoryWithoutPermissionChecks(theUser)).thenReturn(history);
        final TerminalClause clause = new TerminalClauseImpl("lastViewed", Operator.LESS_THAN_EQUALS, blah);
        when(jqlOperandResolver.getValues(queryCreationContext, clause.getOperand(), clause)).thenReturn(CollectionBuilder.list(new QueryLiteral(clause.getOperand(), compareTime)));
        when(jqlOperandResolver.isListOperand(blah)).thenReturn(false);
        when(jqlDateSupport.convertToDate(compareTime)).thenReturn(new Date(compareTime));

        final QueryFactoryResult result = factory.getQuery(queryCreationContext, clause);
        assertEquals("", result.getLuceneQuery().toString());
        assertFalse(result.mustNotOccur());

        verify(jqlOperandResolver).isValidOperand(blah);
        verify(jqlOperandResolver).isEmptyOperand(blah);
        verify(jqlOperandResolver).getValues(queryCreationContext, clause.getOperand(), clause);
        verify(jqlOperandResolver).isListOperand(blah);
        verify(jqlDateSupport).convertToDate(compareTime);
    }

    @Test
    public void testGetQueryLessThanEqualsMatches() throws Exception {
        final long compareTime = 200l;
        final Operand blah = new SingleValueOperand(compareTime);

        final UserHistoryItem item1 = new UserHistoryItem(UserHistoryItem.ISSUE, "00001", 100l);
        final UserHistoryItem item2 = new UserHistoryItem(UserHistoryItem.ISSUE, "00002", 200l);
        final UserHistoryItem item3 = new UserHistoryItem(UserHistoryItem.ISSUE, "00003", 300l);

        final List<UserHistoryItem> history = CollectionBuilder.list(item1, item2, item3);
        when(jqlOperandResolver.isValidOperand(blah)).thenReturn(true);
        when(jqlOperandResolver.isEmptyOperand(blah)).thenReturn(false);
        when(userHistoryManager.getFullIssueHistoryWithoutPermissionChecks(theUser)).thenReturn(history);
        final TerminalClause clause = new TerminalClauseImpl("lastViewed", Operator.LESS_THAN_EQUALS, blah);
        when(jqlOperandResolver.getValues(queryCreationContext, clause.getOperand(), clause)).thenReturn(CollectionBuilder.list(new QueryLiteral(clause.getOperand(), compareTime)));
        when(jqlOperandResolver.isListOperand(blah)).thenReturn(false);
        when(jqlDateSupport.convertToDate(compareTime)).thenReturn(new Date(compareTime));

        final QueryFactoryResult result = factory.getQuery(queryCreationContext, clause);
        assertEquals("issue_id:00001 issue_id:00002", result.getLuceneQuery().toString());
        assertFalse(result.mustNotOccur());

        verify(jqlOperandResolver).isValidOperand(blah);
        verify(jqlOperandResolver).isEmptyOperand(blah);
        verify(jqlOperandResolver).getValues(queryCreationContext, clause.getOperand(), clause);
        verify(jqlOperandResolver).isListOperand(blah);
        verify(jqlDateSupport).convertToDate(compareTime);
    }


}
