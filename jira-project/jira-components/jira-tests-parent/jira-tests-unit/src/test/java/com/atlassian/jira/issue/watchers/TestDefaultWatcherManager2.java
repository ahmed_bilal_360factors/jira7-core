package com.atlassian.jira.issue.watchers;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.association.UserAssociationStore;
import com.atlassian.jira.association.UserAssociationStoreImpl;
import com.atlassian.jira.database.QueryDslAccessor;
import com.atlassian.jira.issue.IssueFactory;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.index.IssueIndexManager;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.mock.issue.MockIssue;
import com.atlassian.jira.mock.ofbiz.MockOfBizDelegator;
import com.atlassian.jira.security.JiraAuthenticationContextImpl;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.util.MockUserManager;
import com.atlassian.jira.user.util.UserManager;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

/**
 * Lightweight TestDefaultWatcherManager
 *
 * @since v6.0
 */
@RunWith(MockitoJUnitRunner.class)
public class TestDefaultWatcherManager2 {
    @Mock
    private IssueIndexManager indexManager;
    @Mock
    private IssueFactory issueFactory;
    @Mock
    private EventPublisher eventPublisher;
    @Mock
    private IssueManager issueManager;
    @Mock
    private UserManager userManager;
    @Mock
    private QueryDslAccessor queryDslAccessor;

    @Before
    public void setUp() {
        JiraAuthenticationContextImpl.clearRequestCache();
    }

    @Test
    public void test_startWatching_stopWatching() {
        UserAssociationStore userAssociationStore = new UserAssociationStoreImpl(new MockOfBizDelegator(), new MockUserManager());
        WatcherManager watcherManager = new DefaultWatcherManager(userAssociationStore, null, indexManager, issueFactory, eventPublisher, issueManager, userManager, queryDslAccessor);
        MockIssue issue = new MockIssue(12L);

        when(issueManager.getIssueObject(12L)).thenReturn(issue);

        ApplicationUser user = new MockApplicationUser("mary");
        assertEquals(0, watcherManager.getWatcherUserKeys(issue).size());

        watcherManager.startWatching(user, issue);

        assertEquals(1, watcherManager.getWatcherUserKeys(issue).size());
        assertEquals("mary", watcherManager.getWatcherUserKeys(issue).iterator().next());

        issue.setWatches(1L);
        watcherManager.stopWatching(user, issue);

        assertEquals(0, watcherManager.getWatcherUserKeys(issue).size());
    }
}
