package com.atlassian.jira.bc.user.search;

import com.atlassian.crowd.embedded.api.SearchRestriction;
import com.atlassian.crowd.search.query.entity.restriction.MatchMode;
import com.atlassian.crowd.search.query.entity.restriction.Property;
import com.atlassian.crowd.search.query.entity.restriction.TermRestriction;
import com.google.common.collect.ImmutableList;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collection;
import java.util.Collections;
import java.util.function.Predicate;

import static org.hamcrest.Matchers.contains;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

@RunWith(MockitoJUnitRunner.class)
public class TestUserSearchUtilities {

    @Mock
    private Property<String> property;

    @Mock
    private Property<String> property2;

    @Test(expected = IllegalArgumentException.class)
    public void test_userSearchTermRestrictions_null_query() {
        UserSearchUtilities.userSearchTermRestrictions(null, ImmutableList.of(property));
    }

    @Test(expected = IllegalArgumentException.class)
    public void test_userSearchTermRestrictions_null_search_keys() {
        UserSearchUtilities.userSearchTermRestrictions("test", null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void test_userSearchTermRestrictions_null_search_keys_value() {
        UserSearchUtilities.userSearchTermRestrictions("test", Collections.singleton(null));
    }

    @Test
    public void test_userSearchTermRestrictions() {
        final String query = "test";

        final Collection<SearchRestriction> result = UserSearchUtilities.userSearchTermRestrictions(query, ImmutableList.of(property));

        assertThat(result, contains(new TermRestriction<>(property, MatchMode.STARTS_WITH, query)));
    }

    @Test
    public void test_userSearchTermRestrictions_collection() {
        final String query = "test";

        final Collection<SearchRestriction> result = UserSearchUtilities.userSearchTermRestrictions(query, ImmutableList.of(property, property2));

        assertThat(result, contains(
                new TermRestriction<>(property, MatchMode.STARTS_WITH, query),
                new TermRestriction<>(property2, MatchMode.STARTS_WITH, query)
        ));
    }

    @Test(expected = IllegalArgumentException.class)
    public void test_createUserMatchPredicate_null_query() {
        UserSearchUtilities.createUserMatchPredicate(null);
    }

    public void test_createUserMatchPredicate_null_userpart() {
        final Predicate<String> test = UserSearchUtilities.createUserMatchPredicate("test");

        assertFalse(test.test(null));
    }

    @Test
    public void test_createUserMatchPredicate_match() {
        final String query = "test";

        final Predicate<String> test = UserSearchUtilities.createUserMatchPredicate(query);

        assertTrue(test.test("test"));
        assertTrue(test.test("TEST"));
        assertTrue(test.test("testing"));
        assertTrue(test.test("teSTiNg"));
        assertFalse(test.test("atesting"));
    }

    @Test
    public void test_createUserMatchPredicate_match_empty() {
        final String query = "";

        final Predicate<String> test = UserSearchUtilities.createUserMatchPredicate(query);

        assertTrue(test.test("a.test"));
        assertTrue(test.test(""));
        assertTrue(test.test("anything"));
        assertTrue(test.test("-@"));
        assertFalse(test.test(null));
    }

    @Test
    public void test_createUserMatchPredicate_match_splitting() {
        final String query = "test";

        final Predicate<String> test = UserSearchUtilities.createUserMatchPredicate(query);

        assertTrue(test.test("a.test"));
        assertTrue(test.test("A.TEST"));
        assertTrue(test.test("this-testing"));
        assertTrue(test.test("you teSTiNg"));

        assertTrue(test.test("a large.test"));
        assertTrue(test.test("A.TEST \"for\" here"));
        assertTrue(test.test("this_\"testing\".over"));
        assertTrue(test.test("o'teSTiNg"));

        assertTrue(test.test("a@egg,large._ test"));
        assertTrue(test.test("\"test\" \"for\" here"));
        assertTrue(test.test("-TEST \"for\" here"));
        assertTrue(test.test(" -TEST \"for\" here"));
        assertTrue(test.test("@ -TEST \"for\" here"));
        assertTrue(test.test("-ag-@',TEST \"for\" here"));
        assertTrue(test.test("-ag-@,TEST \"for\" here"));
        assertTrue(test.test("this_\"testing\".over"));
        assertTrue(test.test("llfdsf-teSTiNg"));
        assertTrue(test.test("tEster playing"));
        assertTrue(test.test("the \"tEst\" playing"));
        assertTrue(test.test("the (tEst) playing"));

        assertFalse(test.test("you atesting"));
    }

    @Test
    public void test_createUserMatchPredicate_no_match_if_separator_in_query() {
        final String query = "@test";

        final Predicate<String> test = UserSearchUtilities.createUserMatchPredicate(query);

        assertFalse(test.test("a@test.com"));

        // but this would match as a different separator before it
        assertTrue(test.test("a-@test.com"));
    }

}
