package com.atlassian.jira.issue.fields;

import com.atlassian.jira.bulkedit.operation.BulkMigrateOperation;
import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.datetime.DateTimeFormatter;
import com.atlassian.jira.datetime.DateTimeFormatterFactoryStub;
import com.atlassian.jira.help.HelpUrls;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueFieldConstants;
import com.atlassian.jira.issue.customfields.OperationContext;
import com.atlassian.jira.issue.fields.config.manager.IssueTypeSchemeManager;
import com.atlassian.jira.issue.fields.option.OptionSetManager;
import com.atlassian.jira.issue.fields.rest.json.SuggestionBeanFactory;
import com.atlassian.jira.issue.fields.rest.json.beans.JiraBaseUrls;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.issue.search.handlers.IssueTypeSearchHandlerFactory;
import com.atlassian.jira.issue.statistics.IssueTypeStatisticsMapper;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.mail.util.MailAttachmentsManagerImpl;
import com.atlassian.jira.mock.issue.MockIssue;
import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.xsrf.XsrfTokenGenerator;
import com.atlassian.jira.template.TemplateSource;
import com.atlassian.jira.template.VelocityTemplatingEngine;
import com.atlassian.jira.template.soy.SoyTemplateRendererProvider;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.BaseUrl;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.collect.MapBuilder;
import com.atlassian.jira.web.action.util.CalendarLanguageUtil;
import com.atlassian.jira.web.bean.BulkEditBean;
import com.atlassian.jira.workflow.WorkflowManager;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import org.hamcrest.Matchers;
import org.hamcrest.collection.IsMapContaining;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.ArgumentMatcher;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.net.URI;
import java.util.Collections;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyMap;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith (MockitoJUnitRunner.class)
public class TestIssueTypeSystemField {
    @Rule
    public RuleChain init = MockitoMocksInContainer.forTest(this);

    @Mock
    @AvailableInContainer
    private PermissionManager permissionManager;
    @Mock
    private JiraAuthenticationContext jiraAuthenticationContext;
    @Mock
    @AvailableInContainer
    HelpUrls helpUrls;
    @Mock
    @AvailableInContainer
    CalendarLanguageUtil calendarLanguageUtil;
    @AvailableInContainer
    DateTimeFormatter dateTimeFormatter = new DateTimeFormatterFactoryStub().formatter();
    @Mock
    @AvailableInContainer
    SoyTemplateRendererProvider soyTemplateRendererProvider;
    @Mock
    @AvailableInContainer
    FieldManager fieldManager;
    @Mock
    @AvailableInContainer
    XsrfTokenGenerator xsrfTokenGenerator;
    @Mock
    BulkEditBean bulkEditBean;
    @Mock
    VelocityTemplatingEngine.RenderRequest renderRequestMock;
    @Mock
    OperationContext operationContext;

    final String expectedTemplate = "issuetype-edit.vm";

    @Mock
    private VelocityTemplatingEngine templatingEngine;

    @Mock
    private BaseUrl baseUrl;

    @Mock
    @AvailableInContainer
    private FeatureManager featureManager;

    IssueTypeSystemField field;

    @Before
    public void setUp() {
        IssueTypeSearchHandlerFactory issueTypeSearchHandlerFactory = null;

        templatingEngine = mock(VelocityTemplatingEngine.class);
        field = new IssueTypeSystemField(
                templatingEngine,
                mock(ApplicationProperties.class),
                jiraAuthenticationContext,
                mock(ConstantsManager.class),
                mock(WorkflowManager.class),
                permissionManager,
                mock(IssueTypeStatisticsMapper.class),
                mock(OptionSetManager.class),
                mock(IssueTypeSchemeManager.class),
                issueTypeSearchHandlerFactory,
                mock(JiraBaseUrls.class),
                baseUrl,
                mock(SuggestionBeanFactory.class)
        );

        ProjectSystemField fieldMock = mock(ProjectSystemField.class);
        when(fieldManager.getField(IssueFieldConstants.PROJECT)).thenReturn(fieldMock);
        when(fieldMock.getAllowedProjects()).thenReturn(Collections.emptyList());

        I18nHelper i18nHelper = mock(I18nHelper.class);
        when(jiraAuthenticationContext.getI18nHelper()).thenReturn(i18nHelper);
        when(i18nHelper.getLocale()).thenReturn(new Locale("en", "US"));

        when(bulkEditBean.getParentIssueObject()).thenReturn(new MockIssue(1l));
        when(bulkEditBean.getOperationName()).thenReturn(BulkMigrateOperation.OPERATION_NAME);

        when(templatingEngine.render(any(TemplateSource.class))).thenReturn(renderRequestMock);
        when(renderRequestMock.applying(anyMap())).thenReturn(mock(VelocityTemplatingEngine.RenderRequest.class));

        when(operationContext.getFieldValuesHolder()).thenReturn(MapBuilder.emptyMap());
        when(calendarLanguageUtil.hasTranslationForLanguage(anyObject())).thenReturn(true);
    }

    @Test
    public void userHasMovePermissionChecksPermissionOverTheIssue() {
        Issue issue = mock(Issue.class);
        ApplicationUser user = mock(ApplicationUser.class);

        when(jiraAuthenticationContext.getUser()).thenReturn(user);
        when(permissionManager.hasPermission(ProjectPermissions.MOVE_ISSUES, issue, user)).thenReturn(true);

        boolean hasMovePermission = field.userHasMovePermission(issue);

        assertThat(hasMovePermission, is(true));
    }

    @Test
    public void testShowVelocityParameterConfigToDefaultOptionJsonForIssue() {
        field.getBulkEditHtml(operationContext, null, bulkEditBean, new HashMap<>());

        TemplateSource.File templateSource = captureTemplateSource();
        Map<String, Object> mapPassedToVelocity = captuteMapArgument();

        assertThat("Should render correct template", templateSource.getPath(), Matchers.endsWith(expectedTemplate));
        assertThat("configToDefaultOptionJson should be present for issues", mapPassedToVelocity, IsMapContaining.hasKey("configToDefaultOptionJson"));
    }

    @Test
    public void testShowVelocityParameterConfigToDefaultOptionJsonForSubtask() {
        when(bulkEditBean.getParentBulkEditBean()).thenReturn(mock(BulkEditBean.class));

        field.getBulkEditHtml(operationContext, null, bulkEditBean, new HashMap<>());

        TemplateSource.File templateSource = captureTemplateSource();
        Map<String, Object> mapPassedToVelocity = captuteMapArgument();

        assertThat("Should render correct template", templateSource.getPath(), Matchers.endsWith(expectedTemplate));
        assertThat("configToDefaultOptionJson should be present for subtasks", mapPassedToVelocity, IsMapContaining.hasKey("configToDefaultOptionJson"));
    }

    private Map<String, Object> captuteMapArgument() {
        ArgumentCaptor<Map<String, Object>> mapArgumentCaptor = ArgumentCaptor.forClass((Class<Map<String, Object>>) (Class) Map.class);
        verify(renderRequestMock).applying(mapArgumentCaptor.capture());
        return mapArgumentCaptor.getValue();
    }

    private TemplateSource.File captureTemplateSource() {
        ArgumentCaptor<TemplateSource> templateSourceArgumentCaptor = ArgumentCaptor.forClass(TemplateSource.class);
        verify(templatingEngine).render(templateSourceArgumentCaptor.capture());
        return (TemplateSource.File) templateSourceArgumentCaptor.getValue();
    }

    @Test
    public void shouldReturnEmailUrlIfEmailView() {
        final String expectedIssueTypeIconUrl = "SOMEURL";
        final String expectedHtml = "HTML";
        final MailAttachmentsManagerImpl mailAttachmentsManager = mock(MailAttachmentsManagerImpl.class);
        final Map<String, Object> displayParams = Maps.newHashMap(ImmutableMap.of("attachmentsManager", mailAttachmentsManager, "email_view", true));
        final Issue issue = mock(Issue.class);
        final IssueType issueType = mock(IssueType.class);
        final VelocityTemplatingEngine.RenderRequest renderRequest = mock(VelocityTemplatingEngine.RenderRequest.class);

        when(templatingEngine.render(anyObject())).thenReturn(renderRequest);
        when(renderRequest.applying(anyMap())).thenReturn(renderRequest);
        when(renderRequest.asHtml()).thenReturn(expectedHtml);
        when(mailAttachmentsManager.getIssueTypeIconUrl(issueType)).thenReturn(expectedIssueTypeIconUrl);
        when(issue.getIssueType()).thenReturn(issueType);

        final String html = field.getColumnViewHtml(null, displayParams, issue);

        verify(renderRequest).applying(argThat(isMapWithIconUrl(expectedIssueTypeIconUrl)));
        assertThat(html, equalTo(expectedHtml));
    }

    @Test
    public void shouldReturnValidAbsoluteUrl() {
        final String expectedIssueTypeIconUrl = "http://localhost:1234/jira/something.png";
        final String expectedHtml = "HTML";
        final Map<String, Object> displayParams = Maps.newHashMap();
        final Issue issue = mock(Issue.class);
        final IssueType issueType = mock(IssueType.class);
        final VelocityTemplatingEngine.RenderRequest renderRequest = mock(VelocityTemplatingEngine.RenderRequest.class);

        when(templatingEngine.render(anyObject())).thenReturn(renderRequest);
        when(renderRequest.applying(anyMap())).thenReturn(renderRequest);
        when(renderRequest.asHtml()).thenReturn(expectedHtml);
        when(baseUrl.getBaseUri()).thenReturn(URI.create("http://localhost:1234/jira/"));
        when(issue.getIssueType()).thenReturn(issueType);
        when(issueType.getIconUrl()).thenReturn("something.png");
        when(issueType.getIconUrlHtml()).thenReturn("something.png");

        final String html = field.getColumnViewHtml(null, displayParams, issue);

        verify(renderRequest).applying(argThat(isMapWithIconUrl(expectedIssueTypeIconUrl)));
        assertThat(html, equalTo(expectedHtml));
    }

    @Test
    public void shouldReturnValidAbsoluteUrlEvenWhenIconUrlStartsWithSlash() {
        final String expectedIssueTypeIconUrl = "http://localhost:1234/jira/something.png";
        final String expectedHtml = "HTML";
        final Map<String, Object> displayParams = Maps.newHashMap();
        final Issue issue = mock(Issue.class);
        final IssueType issueType = mock(IssueType.class);
        final VelocityTemplatingEngine.RenderRequest renderRequest = mock(VelocityTemplatingEngine.RenderRequest.class);

        when(templatingEngine.render(anyObject())).thenReturn(renderRequest);
        when(renderRequest.applying(anyMap())).thenReturn(renderRequest);
        when(renderRequest.asHtml()).thenReturn(expectedHtml);
        when(baseUrl.getBaseUri()).thenReturn(URI.create("http://localhost:1234/jira/"));
        when(issue.getIssueType()).thenReturn(issueType);
        when(issueType.getIconUrl()).thenReturn("/something.png");
        when(issueType.getIconUrlHtml()).thenReturn("/something.png");

        final String html = field.getColumnViewHtml(null, displayParams, issue);

        verify(renderRequest).applying(argThat(isMapWithIconUrl(expectedIssueTypeIconUrl)));
        assertThat(html, equalTo(expectedHtml));
    }

    private IsMapWithIconUrlParamSetTo isMapWithIconUrl(final String iconUrl) {
        return new IsMapWithIconUrlParamSetTo(iconUrl);
    }

    private static class IsMapWithIconUrlParamSetTo extends ArgumentMatcher<Map<String, Object>> {
        private final Optional<String> expectedIconUrl;

        private IsMapWithIconUrlParamSetTo(final String expectedIconUrl) {
            this.expectedIconUrl = Optional.ofNullable(expectedIconUrl);
        }

        @Override
        public boolean matches(final Object argument) {

            return expectedIconUrl.equals(((Map) argument).get("iconurl"));
        }
    }
}
