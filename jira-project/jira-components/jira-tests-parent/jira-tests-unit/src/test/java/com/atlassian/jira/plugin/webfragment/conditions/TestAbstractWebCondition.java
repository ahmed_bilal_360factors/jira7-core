package com.atlassian.jira.plugin.webfragment.conditions;

import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.util.UserUtil;
import com.atlassian.plugin.web.Condition;
import com.google.common.collect.ImmutableMap;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.mockito.Mock;

import java.util.Collections;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

public class TestAbstractWebCondition {
    private static final Map<String, Object> ANONYMOUS = Collections.emptyMap();

    @Rule
    public RuleChain ruleChain = MockitoMocksInContainer.forTest(this);

    private ApplicationUser expectedUser;

    @Mock
    @AvailableInContainer
    private UserUtil mockUserUtil;

    @Before
    public void setUp() {
        expectedUser = null;
    }

    @Test
    public void testShouldDisplayWithUser() {
        asFred();
        checkShouldDisplay(true, context("user", expectedUser));
    }

    @Test
    public void testShouldDisplayWithUsername() {
        asFred();
        checkShouldDisplay(true, context("username", "fred"));
    }

    @Test
    public void testShouldNotDisplayWithUser() {
        checkShouldDisplay(false, context("username", "fred"));
    }

    @Test
    public void testShouldDisplayAnonymous() {
        checkShouldDisplay(true, ANONYMOUS);
    }

    @Test
    public void testShouldNotDisplayAnonymous() {
        checkShouldDisplay(false, ANONYMOUS);
    }

    @Test
    public void testShouldReturnFalseIfExceptionOccur() {
        checkShouldDisplay(false, ANONYMOUS, new AbstractWebCondition() {
            @Override
            public boolean shouldDisplay(final ApplicationUser user, final JiraHelper jiraHelper) {
                throw new LinkageError("That's really unexpected error here!");
            }
        });
    }

    @Test
    public void conditionShouldIgnoreUserEntryWithTypeDifferentThanApplicationUser() {
        asFred();
        ApplicationUser expectedResolvedUser = null;
        checkShouldDisplay(false, context("user", ImmutableMap.of("key", "fred")), expectedResolvedUser);
    }

    @Test
    public void conditionShouldSwitchToCheckingUsernameIfUserEntryHasTypeDifferentThanApplicationUser() {
        asFred();

        ImmutableMap<String, Object> context = ImmutableMap.of(
                "user", ImmutableMap.of("key", "fred"),
                "username", "fred");

        checkShouldDisplay(true, context);
    }

    private void asFred() {
        expectedUser = new MockApplicationUser("fred");
        when(mockUserUtil.getUserByName("fred")).thenReturn(expectedUser);
    }

    private void checkShouldDisplay(final boolean expectedResult, final Map<String, Object> context) {
        checkShouldDisplay(expectedResult, context, expectedUser);
    }

    private void checkShouldDisplay(final boolean expectedResult, final Map<String, Object> context, ApplicationUser expectedUser) {
        checkShouldDisplay(expectedResult, context, new AbstractWebCondition() {
            @Override
            public boolean shouldDisplay(ApplicationUser user, JiraHelper jiraHelper) {
                assertEquals(expectedUser, user);
                return expectedResult;
            }
        });
    }

    private void checkShouldDisplay(final boolean expectedResult, final Map<String, Object> context, final Condition condition) {
        assertEquals(expectedResult, condition.shouldDisplay(context));
    }

    private Map<String, Object> context(String key, Object value) {
        return ImmutableMap.of(key, value);
    }
}
