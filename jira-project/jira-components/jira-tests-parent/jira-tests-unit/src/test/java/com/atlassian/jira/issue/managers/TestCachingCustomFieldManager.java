package com.atlassian.jira.issue.managers;

import com.atlassian.cache.memory.MemoryCacheManager;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.fugue.Option;
import com.atlassian.jira.action.issue.customfields.MockCustomFieldType;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.config.InstanceFeatureManager;
import com.atlassian.jira.database.MockQueryDslAccessor;
import com.atlassian.jira.database.ResultRow;
import com.atlassian.jira.event.issue.field.CustomFieldCreatedEvent;
import com.atlassian.jira.event.issue.field.CustomFieldDeletedEvent;
import com.atlassian.jira.event.issue.field.CustomFieldUpdatedEvent;
import com.atlassian.jira.exception.RemoveException;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.context.IssueContext;
import com.atlassian.jira.issue.context.JiraContextNode;
import com.atlassian.jira.issue.customfields.CustomFieldSearcher;
import com.atlassian.jira.issue.customfields.CustomFieldType;
import com.atlassian.jira.issue.customfields.persistence.CustomFieldValuePersister;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.CustomFieldFactory;
import com.atlassian.jira.issue.fields.CustomFieldScope;
import com.atlassian.jira.issue.fields.CustomFieldScopeFactory;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.fields.ImmutableCustomField;
import com.atlassian.jira.issue.fields.config.MockFieldConfigScheme;
import com.atlassian.jira.issue.fields.config.manager.FieldConfigSchemeManager;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutManager;
import com.atlassian.jira.issue.fields.screen.FieldScreenManager;
import com.atlassian.jira.issue.index.managers.FieldIndexerManager;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.issue.search.SearchContext;
import com.atlassian.jira.issue.search.managers.IssueSearcherManager;
import com.atlassian.jira.issue.search.searchers.MockCustomFieldSearcher;
import com.atlassian.jira.junit.rules.MockitoContainer;
import com.atlassian.jira.mock.component.MockComponentWorker;
import com.atlassian.jira.mock.ofbiz.MockOfBizDelegator;
import com.atlassian.jira.model.querydsl.CustomFieldDTO;
import com.atlassian.jira.notification.NotificationSchemeManager;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.plugin.customfield.CustomFieldSearcherModuleDescriptor;
import com.atlassian.jira.plugin.customfield.CustomFieldSearcherModuleDescriptors;
import com.atlassian.jira.plugin.customfield.CustomFieldTypeModuleDescriptor;
import com.atlassian.jira.plugin.customfield.CustomFieldTypeModuleDescriptors;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.web.action.admin.customfields.CreateCustomField;
import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.internal.util.reflection.Whitebox;
import org.ofbiz.core.entity.GenericEntityException;
import org.ofbiz.core.entity.GenericValue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

import static java.util.Collections.singletonList;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.stub;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestCachingCustomFieldManager {
    private CustomFieldManager customFieldManager;

    @Rule
    public MockitoContainer container = new MockitoContainer(this);

    // Level 1 Dependencies
    @Mock
    private PluginAccessor mockPluginAccessor;

    private OfBizDelegator mockDelegator = new MockOfBizDelegator();
    @Mock
    private FieldScreenManager mockScreenManager;
    @Mock
    private EventPublisher mockEventPublisher;
    @Mock
    private FieldConfigSchemeManager mockConfigSchemeManager;
    @Mock
    private NotificationSchemeManager mockNotificationSchemeManager;
    @Mock
    private FieldManager mockFieldManager;
    @Mock
    private CustomFieldValuePersister mockFieldValuePersister;
    @Mock
    private CustomFieldFactory customFieldFactory;
    @Mock
    private CustomFieldTypeModuleDescriptors customFieldTypeModuleDescriptors;
    @Mock
    private CustomFieldSearcherModuleDescriptors customFieldSearcherModuleDescriptors;

    // Level 2 Dependencies
    @Mock
    private CustomFieldTypeModuleDescriptor customFieldTypeModuleDescriptor;
    @Mock
    private CustomFieldSearcherModuleDescriptor customFieldSearcherModuleDescriptor;
    @Mock
    private CustomFieldSearcher customFieldSearcher;
    @Mock
    private List<Plugin> plugins;
    @Mock
    private GenericValue mockGenericValue;
    @Mock
    private CustomFieldType customFieldType;
    @Mock
    private IssueSearcherManager issueSearcherManager;
    @Mock
    private FieldIndexerManager fieldIndexerManager;
    @Mock
    private ProjectManager projectManager;
    @Mock
    private ConstantsManager constantsManager;
    @Mock
    private CustomFieldScopeFactory customFieldScopeFactory;
    @Mock
    private CustomFieldSearcherManager customFieldSearcherManager;
    @Mock
    protected InstanceFeatureManager featureManager;
    @Mock
    private JiraContextNode context;

    MockQueryDslAccessor dbConnectionManager = new MockQueryDslAccessor();
    private static final Long TEST_CUSTOMFIELD_ID = 1001L;
    private static final Long TEST_ID = 999L;
    private static final Long NON_EXISTING_ID = 777L;
    private static final Long NON_EXISTENT_ID = 22L;

    final List<ResultRow> allCustomFieldsResultRows = new ArrayList<>();

    @Before
    public void setUp() throws Exception {
        stub(mockPluginAccessor.getPlugins()).toReturn(plugins);
        stub(plugins.isEmpty()).toReturn(false);

        final MockComponentWorker componentAccessorWorker = new MockComponentWorker();
        componentAccessorWorker.registerMock(OfBizDelegator.class, mockDelegator);
        ComponentAccessor.initialiseWorker(componentAccessorWorker);

        doNothing().when(mockFieldManager).refresh();

        final Collection<Plugin> plugins = singletonList(null);
        when(mockPluginAccessor.getPlugins()).thenReturn(plugins);
        customFieldManager = new CachingCustomFieldManager(
                mockPluginAccessor,
                dbConnectionManager,
                mockConfigSchemeManager,
                constantsManager,
                projectManager,
                null,
                mockScreenManager,
                mockFieldValuePersister,
                mockNotificationSchemeManager,
                mockFieldManager,
                mockEventPublisher,
                customFieldFactory,
                customFieldTypeModuleDescriptors,
                customFieldSearcherModuleDescriptors,
                customFieldSearcherManager,
                new MemoryCacheManager()
        );
        ComponentAccessor.initialiseWorker(
                new MockComponentWorker()
                        .addMock(EventPublisher.class, mock(EventPublisher.class))
                        .addMock(IssueSearcherManager.class, issueSearcherManager)
                        .addMock(FieldIndexerManager.class, fieldIndexerManager)
        );
        when(customFieldSearcher.getDescriptor()).thenReturn(mock(CustomFieldSearcherModuleDescriptor.class));
        when(customFieldFactory.create(any(CustomFieldDTO.class))).thenAnswer(invocationOnMock -> {
            CustomFieldDTO customFieldDTO = (CustomFieldDTO) invocationOnMock.getArguments()[0];
            CustomField customField = mock(ImmutableCustomField.class);
            Whitebox.setInternalState(customField, "scopeFactory", customFieldScopeFactory);
            if (customFieldDTO != null) {
                GenericValue gv = customFieldDTO.toGenericValue(mockDelegator);
                when(customField.getId()).thenReturn(customFieldDTO.getId().toString());
                when(customField.getIdAsLong()).thenReturn(customFieldDTO.getId());
                when(customField.getName()).thenReturn(customFieldDTO.getName());
                when(customField.getDescription()).thenReturn(customFieldDTO.getDescription());
                when(customField.isInScopeForSearch(any(Project.class), any(List.class))).thenCallRealMethod();
                when(customField.getGenericValue()).thenReturn(gv);
                when(customField.getCustomFieldType()).thenReturn(new MockCustomFieldType());
            }
            return customField;
        });
    }

    private void setDescriptor(final CustomFieldType<?, ?> matchingType, final String validSearcherKey) {
        final CustomFieldTypeModuleDescriptor descriptor = mock(CustomFieldTypeModuleDescriptor.class);
        when(descriptor.getValidSearcherKeys()).thenReturn(ImmutableSet.of(validSearcherKey));
        matchingType.init(descriptor);
    }

    private CustomFieldSearcher createCustomFieldSearcher(
            final String customFieldKey,
            final String descriptorCompleteKey
    ) {
        final CustomFieldSearcher searcher = mock(CustomFieldSearcher.class);
        final CustomFieldSearcherModuleDescriptor searcherDescriptor = mock(CustomFieldSearcherModuleDescriptor.class);
        when(searcherDescriptor.getValidCustomFieldKeys()).thenReturn(ImmutableSet.of(customFieldKey));
        when(searcherDescriptor.getCompleteKey()).thenReturn(descriptorCompleteKey);
        when(searcher.getDescriptor()).thenReturn(searcherDescriptor);
        return searcher;
    }

    private void expectInsert() {
        dbConnectionManager.setUpdateResults(
                "insert into customfield (description, cfname, id)\n"
                        + "values ('description', 'fieldName', " + TEST_CUSTOMFIELD_ID + ")", 1
        );
        when(dbConnectionManager.getMockDelegatorInterface().getNextSeqId("CustomField")).thenReturn(TEST_CUSTOMFIELD_ID);
    }

    private void expectSelectByAll() {
        final ResultRow resultRow = new ResultRow(TEST_CUSTOMFIELD_ID, "key", null, "CF A", null, null, 1L, null, null);
        dbConnectionManager.setQueryResults(
                "select CUSTOM_FIELD.id, CUSTOM_FIELD.customfieldtypekey, CUSTOM_FIELD.customfieldsearcherkey, CUSTOM_FIELD.cfname, CUSTOM_FIELD.description, CUSTOM_FIELD.defaultvalue, CUSTOM_FIELD.fieldtype, CUSTOM_FIELD.project, CUSTOM_FIELD.issuetype\n" +
                        "from customfield CUSTOM_FIELD",
                ImmutableList.of(resultRow)
        );
    }

    private void expectCustomFieldDeletes(final MockQueryDslAccessor dbConnectionManager) {
        expectLayoutItemDeletes(dbConnectionManager);
        dbConnectionManager.setUpdateResults(
                "delete from customfield\n"
                        + "where customfield.id = " + TEST_ID, 1
        );
    }

    private void expectLayoutItemDeletes(final MockQueryDslAccessor dbConnectionManager) {
        dbConnectionManager.setUpdateResults(
                "delete from columnlayoutitem\n"
                        + "where columnlayoutitem.fieldidentifier = 'customfield_" + TEST_ID + "'", 1
        );
        dbConnectionManager.setUpdateResults(
                "delete from fieldlayoutitem\n"
                        + "where fieldlayoutitem.fieldidentifier = 'customfield_" + TEST_ID + "'", 1
        );
    }

    private void mockCustomFieldLoader(
            final String name,
            final String description,
            final String searcherKey,
            final CustomFieldType cfType
    ) throws Exception {
        final ResultRow resultRow = new ResultRow(TEST_ID, "key", searcherKey, name, description, null, 1L, null, null);
        dbConnectionManager.setQueryResults(
                "select CUSTOM_FIELD.id, CUSTOM_FIELD.customfieldtypekey, CUSTOM_FIELD.customfieldsearcherkey, CUSTOM_FIELD.cfname, CUSTOM_FIELD.description, CUSTOM_FIELD.defaultvalue, CUSTOM_FIELD.fieldtype, CUSTOM_FIELD.project, CUSTOM_FIELD.issuetype\n" +
                        "from customfield CUSTOM_FIELD",
                ImmutableList.of(resultRow)
        );

        when(customFieldTypeModuleDescriptors.getCustomFieldType(anyString())).thenReturn(Option.option(cfType));

    }

    private void mockCustomFieldUpdate(
            final String name,
            final String description,
            final String searcherKey,
            final CustomFieldType cfType
    ) throws Exception {
        final String updateSql = "update customfield\n" +
                "set cfname = '" + name + "', description = '" + description + "', customfieldsearcherkey = '" + searcherKey + "'\n" +
                "where customfield.id = " + TEST_ID;
        dbConnectionManager.setUpdateResults(updateSql, 1);
        dbConnectionManager.onSqlListener(
                updateSql, () -> {
                    final ResultRow updatedRow = new ResultRow(
                            TEST_ID,
                            "key",
                            searcherKey,
                            name,
                            description,
                            null,
                            1L,
                            null,
                            null
                    );
                    dbConnectionManager.setQueryResults(
                            "select CUSTOM_FIELD.id, CUSTOM_FIELD.customfieldtypekey, CUSTOM_FIELD.customfieldsearcherkey, CUSTOM_FIELD.cfname, CUSTOM_FIELD.description, CUSTOM_FIELD.defaultvalue, CUSTOM_FIELD.fieldtype, CUSTOM_FIELD.project, CUSTOM_FIELD.issuetype\n" +
                                    "from customfield CUSTOM_FIELD",
                            ImmutableList.of(updatedRow)
                    );
                }
        );

    }

    private void setUpSearcher(final CustomFieldSearcher searcher, final String searcherKey) {
        final CustomFieldSearcherModuleDescriptor searcherModuleDescriptor = mock(CustomFieldSearcherModuleDescriptor.class);
        when(searcher.getDescriptor()).thenReturn(searcherModuleDescriptor);
        when(searcherModuleDescriptor.getCompleteKey()).thenReturn(searcherKey);
    }

    private void populateCustomFieldsStoreMock(
            final Long id, final String customFieldTypeKey,
            final String customFieldSearcherKey, final String name, final String description, final String defaultValue,
            final Long fieldType, final Long project, final String issueType
    ) throws GenericEntityException {
        dbConnectionManager.setUpdateResults(
                "insert into customfield (cfname, id)\n"
                        + "values ('" + name + "', " + id + ")", id.intValue()
        );
        final ResultRow resultRow = new ResultRow(
                id, customFieldTypeKey,
                customFieldSearcherKey, name, description, defaultValue,
                fieldType, project, issueType
        );
        allCustomFieldsResultRows.add(resultRow);

        dbConnectionManager.setQueryResults(
                "select CUSTOM_FIELD.id, CUSTOM_FIELD.customfieldtypekey, CUSTOM_FIELD.customfieldsearcherkey, CUSTOM_FIELD.cfname, CUSTOM_FIELD.description, CUSTOM_FIELD.defaultvalue, CUSTOM_FIELD.fieldtype, CUSTOM_FIELD.project, CUSTOM_FIELD.issuetype\n" +
                        "from customfield CUSTOM_FIELD",
                ImmutableList.copyOf(allCustomFieldsResultRows)
        );

        when(dbConnectionManager.getMockDelegatorInterface().getNextSeqId("CustomField")).thenReturn(id);
        customFieldManager.createCustomField(name, description, customFieldType, customFieldSearcher, null, null);
    }

    private void removeCustomFieldsFromDB() {
        allCustomFieldsResultRows.clear();
        dbConnectionManager.setQueryResults(
                "select CUSTOM_FIELD.id, CUSTOM_FIELD.customfieldtypekey, CUSTOM_FIELD.customfieldsearcherkey, CUSTOM_FIELD.cfname, CUSTOM_FIELD.description, CUSTOM_FIELD.defaultvalue, CUSTOM_FIELD.fieldtype, CUSTOM_FIELD.project, CUSTOM_FIELD.issuetype\n" +
                        "from customfield CUSTOM_FIELD",
                ImmutableList.copyOf(allCustomFieldsResultRows)
        );
    }

    private interface Lambda {
        List<CustomField> invoke(Long projectId, List<String> issueTypeIds, CustomFieldScope customFieldScope);
    }

    private void checkGetCustomFieldObjectsMethod(final Lambda lambda) {
        final Long projectId = 1L;
        final Project project = mock(Project.class);
        when(projectManager.getProjectObj(projectId)).thenReturn(project);

        final List<String> issueTypeIds = Arrays.asList("bug", "task");
        when(constantsManager.expandIssueTypeIds(issueTypeIds)).thenReturn(issueTypeIds);

        final ImmutableCustomField customField = mock(ImmutableCustomField.class);
        when(customField.isInScopeForSearch(eq(project), eq(issueTypeIds))).thenReturn(true);

        final CustomFieldScope customFieldScope = mock(CustomFieldScope.class);
        when(customFieldScopeFactory.createFor(anyObject())).thenReturn(customFieldScope);
        when(customFieldScope.isIncludedIn(any(IssueContext.class))).thenReturn(true);

        final List<CustomField> customFieldObjects = lambda.invoke(projectId, issueTypeIds, customFieldScope);

        verify(customFieldScope, times(3)).isIncludedIn(anyObject());
        assertThat(customFieldObjects.size(), is(3));
    }

    private void prepareGetTests() throws Exception {
        final String customfieldtypekey = "key";
        when(customFieldTypeModuleDescriptors.getCustomFieldType(anyString())).thenReturn(Option.option(new MockCustomFieldType()));
        populateCustomFieldsStoreMock(1001L, customfieldtypekey, null, "CF A", null, null, 1L, null, null);
        populateCustomFieldsStoreMock(1002L, customfieldtypekey, null, "CF A", null, null, 1L, null, null);
        populateCustomFieldsStoreMock(1003L, customfieldtypekey, null, "CF B", null, null, 1L, null, null);
    }

    @Test
    public void testSchemeEquality() {
        final MockFieldConfigScheme scheme1 = new MockFieldConfigScheme();
        scheme1.setId(1L);
        final MockFieldConfigScheme scheme2 = new MockFieldConfigScheme();
        scheme2.setId(2L);
        final MockFieldConfigScheme scheme3 = new MockFieldConfigScheme();
        scheme3.setId(3L);
        final MockFieldConfigScheme scheme4 = new MockFieldConfigScheme();
        scheme4.setId(4L);

        final CachingCustomFieldManager manager = (CachingCustomFieldManager) customFieldManager;

        assertThat(manager.areConfigSchemesEqual(null, null), is(true));
        assertThat(
                "Nulls should not equal anything else",
                manager.areConfigSchemesEqual(null, ImmutableList.of()),
                is(false)
        );
        assertThat(
                "Nulls should not equal anything else",
                manager.areConfigSchemesEqual(null, ImmutableList.of(scheme2)),
                is(false)
        );

        assertThat(
                manager.areConfigSchemesEqual(
                        ImmutableList.of(scheme1, scheme2, scheme3),
                        ImmutableList.of(scheme1, scheme2, scheme3)
                ), is(true)
        );
        assertThat(
                manager.areConfigSchemesEqual(
                        ImmutableList.of(scheme1, scheme2, scheme3),
                        ImmutableList.of(scheme1, scheme2, scheme4)
                ), is(false)
        );
        assertThat(
                manager.areConfigSchemesEqual(
                        ImmutableList.of(scheme1, scheme2, scheme3),
                        ImmutableList.of(scheme1, scheme2)
                ), is(false)
        );
        assertThat(
                manager.areConfigSchemesEqual(
                        ImmutableList.of(scheme1),
                        ImmutableList.of(scheme1, scheme2)
                ), is(false)
        );
        assertThat(
                manager.areConfigSchemesEqual(
                        ImmutableList.of(),
                        ImmutableList.of(scheme1, scheme2)
                ), is(false)
        );
    }

    @Test
    public void testGetCustomFieldTypeDelegatesOnCustomFieldTypeModuleDescriptors() {
        final String key = "aPluginKey";
        final CustomFieldType expectedCustomFieldType = mock(CustomFieldType.class);
        when(customFieldTypeModuleDescriptors.getCustomFieldType(key)).thenReturn(Option.some(expectedCustomFieldType));

        final CustomFieldType actualCustomFieldType = customFieldManager.getCustomFieldType(key);

        assertThat(actualCustomFieldType, is(expectedCustomFieldType));
    }

    @Test
    public void testGetCustomFieldTypesDelegatesOnCustomFieldTypeModuleDescriptors() {
        final List<CustomFieldType<?, ?>> expectedCustomFieldTypes = Collections.emptyList();
        when(customFieldTypeModuleDescriptors.getCustomFieldTypes()).thenReturn(expectedCustomFieldTypes);

        final List<CustomFieldType<?, ?>> actualCustomFieldType = customFieldManager.getCustomFieldTypes();

        assertThat(actualCustomFieldType, is(expectedCustomFieldTypes));
    }

    @Test
    public void shouldRecogniseCustomField() throws Exception {
        assertThat(customFieldManager.isCustomField("customfield_"), is(true));
        assertThat(customFieldManager.isCustomField("random string"), is(false));
    }

    @Test
    public void testGetCustomFieldSearcherDelegatesOnCustomFieldSearcherModuleDescriptors() {
        final String key = "aPluginKey";
        final CustomFieldSearcher expectedSearcher = mock(CustomFieldSearcher.class);
        when(customFieldSearcherModuleDescriptors.getCustomFieldSearcher(key)).thenReturn(Option.some(expectedSearcher));

        final CustomFieldSearcher actualSearcher = customFieldManager.getCustomFieldSearcher(key);

        assertThat(actualSearcher, is(expectedSearcher));
    }

    @Test
    public void testGetCustomFieldSearcherClass() {
        // Key is "None"
        assertNull(customFieldManager.getCustomFieldSearcherClass("-1"));

        // Wrong type
        doReturn(customFieldTypeModuleDescriptor).when(mockPluginAccessor).getEnabledPluginModule("key");
        assertNull(customFieldManager.getCustomFieldSearcherClass("key"));

        // Right type
        doReturn(customFieldSearcherModuleDescriptor).when(mockPluginAccessor).getEnabledPluginModule("key");
        doReturn(MockCustomFieldSearcher.class).when(customFieldSearcherModuleDescriptor).getModuleClass();
        assertEquals(MockCustomFieldSearcher.class, customFieldManager.getCustomFieldSearcherClass("key"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void getDefaultSearcherNull() {
        customFieldManager.getDefaultSearcher(null);
    }

    @Test
    public void getDefaultSearcher() {
        final CustomFieldManager manager = spy(customFieldManager);
        final CustomFieldType<?, ?> type = new MockCustomFieldType("type", "Type");
        final MockCustomFieldSearcher searcher = new MockCustomFieldSearcher("searcher");

        doReturn(Lists.newArrayList(searcher)).when(manager).getCustomFieldSearchers(type);
        assertSame(searcher, manager.getDefaultSearcher(type));
    }

    @Test
    public void getDefaultSearcherNoSearcher() {
        final CustomFieldManager manager = spy(customFieldManager);
        final CustomFieldType<?, ?> type = new MockCustomFieldType("type", "Type");
        doReturn(Collections.emptyList()).when(manager).getCustomFieldSearchers(type);
        Assert.assertNull(manager.getDefaultSearcher(type));
    }

    @Test
    public void shouldReturnMatchingCustomFieldSearchers() {
        final CustomFieldType<?, ?> matchingType = new MockCustomFieldType("matchingType", "M Type");
        setDescriptor(matchingType, "matchingDescriptor");
        final CustomFieldSearcher expectedSearcher = createCustomFieldSearcher("matchingType", "matchingDescriptor");
        final CustomFieldSearcher expectedSearcherMatchingDescriptor =
                createCustomFieldSearcher("randomType", "matchingDescriptor");
        final CustomFieldSearcher otherSearcher = createCustomFieldSearcher("randomType", "randomDescriptor");
        final ImmutableList<CustomFieldSearcher> mockCustomFieldSearchers = ImmutableList.of(
                expectedSearcher,
                expectedSearcherMatchingDescriptor,
                otherSearcher
        );
        final ImmutableList<CustomFieldSearcher> expectedCutomFieldSearchers = ImmutableList.of(
                expectedSearcher,
                expectedSearcherMatchingDescriptor
        );
        when(mockPluginAccessor.getEnabledModulesByClass(CustomFieldSearcher.class)).thenReturn(mockCustomFieldSearchers);
        when(customFieldSearcherManager.getSearchersValidFor(matchingType)).thenReturn(expectedCutomFieldSearchers);

        final List<CustomFieldSearcher> customFieldSearchers = customFieldManager.getCustomFieldSearchers(matchingType);

        assertThat(customFieldSearchers, is(expectedCutomFieldSearchers));
    }

    @Test
    public void testCreateCFCreatesDefaultScheme() throws GenericEntityException {
        expectSelectByAll();
        expectInsert();
        final CustomFieldManager manager = spy(customFieldManager);
        doReturn(null).when(manager).getCustomFieldObject(anyLong());

        final CustomField customField = mock(ImmutableCustomField.class);

        when(customFieldFactory.create(any(CustomFieldDTO.class))).thenAnswer(invocationOnMock -> {
            CustomFieldDTO customFieldDTO = (CustomFieldDTO) invocationOnMock.getArguments()[0];
            Whitebox.setInternalState(customField, "scopeFactory", customFieldScopeFactory);
            if (customFieldDTO != null) {
                when(customField.getId()).thenReturn(customFieldDTO.getId().toString());
                when(customField.getIdAsLong()).thenReturn(customFieldDTO.getId());
                when(customField.getName()).thenReturn(customFieldDTO.getName());
                // return null first time, and then a real value!
                when(customField.getCustomFieldType()).thenReturn(new MockCustomFieldType());
            }
            return customField;
        });

        final List<JiraContextNode> contexts = singletonList(context);
        final List<IssueType> issueTypes = new ArrayList<>();
        customFieldManager.createCustomField(
                "fieldName",
                "description",
                new MockCustomFieldType(),
                null,
                contexts,
                issueTypes
        );
        verify(mockConfigSchemeManager).createDefaultScheme(eq(customField), eq(contexts), eq(issueTypes));
        dbConnectionManager.assertAllExpectedStatementsWereRun();
    }

    /**
     * https://jira.atlassian.com/browse/JRA-30070
     */
    @Test
    public void testCreateDoesntPutCustomFieldTwiceInTheCache() throws GenericEntityException {
        dbConnectionManager.reset();
        dbConnectionManager.setUpdateResults(
                "insert into customfield (customfieldtypekey, cfname, id)\n"
                        + "values ('com.atlassian.jira.plugin.system.customfieldtypes:textfield', 'Test', " + TEST_CUSTOMFIELD_ID + ")",
                1
        );
        when(dbConnectionManager.getMockDelegatorInterface().getNextSeqId("CustomField")).thenReturn(TEST_CUSTOMFIELD_ID);
        final CustomFieldType cfType = mock(CustomFieldType.class);
        when(cfType.getKey()).thenReturn(CreateCustomField.FIELD_TYPE_PREFIX + "textfield");

        final CustomFieldSearcher cfSearcher = mock(CustomFieldSearcher.class);
        final CustomFieldSearcherModuleDescriptor cfSearcherDescriptor = mock(CustomFieldSearcherModuleDescriptor.class);
        when(cfSearcher.getDescriptor()).thenReturn(cfSearcherDescriptor);

        final CustomFieldTypeModuleDescriptor cfTypeDescriptor = mock(CustomFieldTypeModuleDescriptor.class);
        when(mockPluginAccessor.getEnabledPluginModule(CreateCustomField.FIELD_TYPE_PREFIX + "textfield")).thenReturn((ModuleDescriptor) cfTypeDescriptor);

        when(cfTypeDescriptor.getModule()).thenReturn(cfType);

        final String name = "Test";

        final ResultRow resultRow = new ResultRow(TEST_CUSTOMFIELD_ID, "key", null, name, null, null, 1L, null, null);
        dbConnectionManager.setQueryResults(
                "select CUSTOM_FIELD.id, CUSTOM_FIELD.customfieldtypekey, CUSTOM_FIELD.customfieldsearcherkey, CUSTOM_FIELD.cfname, CUSTOM_FIELD.description, CUSTOM_FIELD.defaultvalue, CUSTOM_FIELD.fieldtype, CUSTOM_FIELD.project, CUSTOM_FIELD.issuetype\n" +
                        "from customfield CUSTOM_FIELD",
                ImmutableList.of(resultRow)
        );

        customFieldManager.createCustomField(
                name,
                null,
                cfType,
                cfSearcher,
                null,
                singletonList(null)
        );
        final List<CustomField> customFields = customFieldManager.getCustomFieldObjects();
        Assert.assertEquals(1, customFields.size());
        Assert.assertEquals(name, customFields.get(0).getName());
        dbConnectionManager.assertAllExpectedStatementsWereRun();
    }

    @Test
    public void testCreatePublishesEvent() throws GenericEntityException {
        expectSelectByAll();
        expectInsert();
        final CustomFieldManager manager = spy(customFieldManager);
        doReturn(null).when(manager).getCustomFieldObject(anyLong());

        final CustomField customField = mock(ImmutableCustomField.class);

        when(customFieldFactory.create(any(CustomFieldDTO.class))).thenAnswer(invocationOnMock -> {
            CustomFieldDTO customFieldDTO = (CustomFieldDTO) invocationOnMock.getArguments()[0];
            Whitebox.setInternalState(customField, "scopeFactory", customFieldScopeFactory);
            if (customFieldDTO != null) {
                when(customField.getId()).thenReturn(customFieldDTO.getId().toString());
                when(customField.getIdAsLong()).thenReturn(customFieldDTO.getId());
                when(customField.getName()).thenReturn(customFieldDTO.getName());
                // return null first time, and then a real value!
                when(customField.getCustomFieldType()).thenReturn(new MockCustomFieldType());
            }
            return customField;
        });

        customFieldManager.createCustomField("fieldName", "description", new MockCustomFieldType(), null, null, null);
        verify(mockEventPublisher).publish(isA(CustomFieldCreatedEvent.class));
        dbConnectionManager.assertAllExpectedStatementsWereRun();
    }

    @Test
    public void testRemoveCustomFieldPossiblyLeavingOrphanedDataWithBadId() throws RemoveException {
        dbConnectionManager.setQueryResults(
                "select CUSTOM_FIELD.id, CUSTOM_FIELD.customfieldtypekey, CUSTOM_FIELD.customfieldsearcherkey, CUSTOM_FIELD.cfname, CUSTOM_FIELD.description, CUSTOM_FIELD.defaultvalue, CUSTOM_FIELD.fieldtype, CUSTOM_FIELD.project, CUSTOM_FIELD.issuetype\n"
                        + "from customfield CUSTOM_FIELD\n"
                        + "where CUSTOM_FIELD.id = " + NON_EXISTING_ID + "\n"
                        + "limit 1",
                ImmutableList.of()
        );
        dbConnectionManager.setQueryResults(
                "select CUSTOM_FIELD.id, CUSTOM_FIELD.customfieldtypekey, CUSTOM_FIELD.customfieldsearcherkey, CUSTOM_FIELD.cfname, CUSTOM_FIELD.description, CUSTOM_FIELD.defaultvalue, CUSTOM_FIELD.fieldtype, CUSTOM_FIELD.project, CUSTOM_FIELD.issuetype\n"
                        + "from customfield CUSTOM_FIELD",
                ImmutableList.of()
        );
        try {
            customFieldManager.removeCustomFieldPossiblyLeavingOrphanedData(NON_EXISTING_ID);
            fail("Expected exception to be thrown");
        } catch (IllegalArgumentException e) {
            assertEquals("Tried to remove custom field with id '777' that doesn't exist!", e.getMessage());
        }
        dbConnectionManager.assertAllExpectedStatementsWereRun();
    }

    @Test
    public void testRemoveCustomFieldPossiblyLeavingOrphanedDataWithExistingCustomField() throws RemoveException {
        dbConnectionManager.setQueryResults(
                "select CUSTOM_FIELD.id, CUSTOM_FIELD.customfieldtypekey, CUSTOM_FIELD.customfieldsearcherkey, CUSTOM_FIELD.cfname, CUSTOM_FIELD.description, CUSTOM_FIELD.defaultvalue, CUSTOM_FIELD.fieldtype, CUSTOM_FIELD.project, CUSTOM_FIELD.issuetype\n"
                        + "from customfield CUSTOM_FIELD",
                ImmutableList.of()
        );
        expectLayoutItemDeletes(dbConnectionManager);
        final String fieldSId = "customfield_" + TEST_ID;
        final String fieldName = "name";
        final CustomField mockCustomField = mock(CustomField.class);

        stub(mockGenericValue.getLong("id")).toReturn(TEST_ID);
        stub(mockGenericValue.getString("name")).toReturn(fieldName);
        stub(mockGenericValue.getString("customfieldtypekey")).toReturn("CustomFieldType");

        doReturn(customFieldTypeModuleDescriptor).when(mockPluginAccessor).getEnabledPluginModule("CustomFieldType");

        stub(customFieldTypeModuleDescriptor.getModule()).toReturn(customFieldType);

        stub(mockCustomField.getId()).toReturn(fieldSId);
        stub(mockCustomField.getName()).toReturn(fieldName);
        stub(mockCustomField.getIdAsLong()).toReturn(TEST_ID);
        when(mockCustomField.remove()).thenAnswer(
                invocation -> {
                    mockEventPublisher.publish(new CustomFieldDeletedEvent(mockCustomField));
                    return new HashSet<>(Lists.newArrayList(10001L));
                }
        );

        final CustomFieldManager customFieldManagerSpy = spy(customFieldManager);
        doReturn(mockCustomField).when(customFieldManagerSpy).getCustomFieldObject(TEST_ID);

        customFieldManagerSpy.removeCustomFieldPossiblyLeavingOrphanedData(TEST_ID);
        assertThat(customFieldManager.getCustomFieldObjects(), empty());
        verify(mockEventPublisher).publish(isA(CustomFieldDeletedEvent.class));
        dbConnectionManager.assertAllExpectedStatementsWereRun();
    }

    @Test
    public void testRemoveCustomFieldPossiblyLeavingOrphanedDataWithExistingCustomFieldFromdb() throws RemoveException {
        expectCustomFieldDeletes(dbConnectionManager);
        final ResultRow resultRow = new ResultRow(TEST_ID, "key", null, "CF A", null, null, 1L, null, null);
        dbConnectionManager.setQueryResults(
                "select CUSTOM_FIELD.id, CUSTOM_FIELD.customfieldtypekey, CUSTOM_FIELD.customfieldsearcherkey, CUSTOM_FIELD.cfname, CUSTOM_FIELD.description, CUSTOM_FIELD.defaultvalue, CUSTOM_FIELD.fieldtype, CUSTOM_FIELD.project, CUSTOM_FIELD.issuetype\n"
                        + "from customfield CUSTOM_FIELD\n"
                        + "where CUSTOM_FIELD.id = " + TEST_ID + "\n"
                        + "limit 1",
                ImmutableList.of(resultRow)
        );
        dbConnectionManager.setQueryResults(
                "select CUSTOM_FIELD.id, CUSTOM_FIELD.customfieldtypekey, CUSTOM_FIELD.customfieldsearcherkey, CUSTOM_FIELD.cfname, CUSTOM_FIELD.description, CUSTOM_FIELD.defaultvalue, CUSTOM_FIELD.fieldtype, CUSTOM_FIELD.project, CUSTOM_FIELD.issuetype\n"
                        + "from customfield CUSTOM_FIELD",
                ImmutableList.of(resultRow)
        );

        final String fieldSId = "customfield_" + TEST_ID;

        stub(mockFieldValuePersister.removeAllValues(fieldSId)).toReturn(new HashSet<>(Lists.newArrayList(10001L)));

        when(customFieldFactory.create(any(CustomFieldDTO.class))).thenReturn(null);

        customFieldManager.removeCustomFieldPossiblyLeavingOrphanedData(TEST_ID);

        assertThat(customFieldManager.getCustomFieldObjects(), empty());
        verify(mockFieldValuePersister).removeAllValues(fieldSId);
        verify(mockFieldManager).refresh();
        verify(mockScreenManager).removeFieldScreenItems(fieldSId);
        dbConnectionManager.assertAllExpectedStatementsWereRun();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testRemoveCustomFieldPossiblyLeavingOrphanedDataWithNullId() throws RemoveException {
        try {
            customFieldManager.removeCustomFieldPossiblyLeavingOrphanedData(null);
        } finally {
            dbConnectionManager.assertAllExpectedStatementsWereRun();
        }
    }

    @Test
    public void shouldRemoveCustomFieldValues() throws Exception {
        final GenericValue issue = mock(GenericValue.class);
        when(issue.getLong("id")).thenReturn(TEST_ID);
        dbConnectionManager.setUpdateResults(
                "delete from customfieldvalue\n" +
                        "where customfieldvalue.issue = " + TEST_ID, 1
        );

        customFieldManager.removeCustomFieldValues(issue);

        dbConnectionManager.assertAllExpectedStatementsWereRun();
    }

    @Test
    public void testUpdate() throws Exception {
        final String oldName = "oldName";
        final String oldDescription = "old description";
        final String newName = "newName";
        final String newDescription = "new description";
        final CustomFieldSearcher oldSearcher = mock(CustomFieldSearcher.class);
        final CustomFieldSearcher newSearcher = mock(CustomFieldSearcher.class);
        final String oldSearcherKey = "oldSearcherKey";
        final String newSearcherKey = "newSearcherKey";

        setUpSearcher(oldSearcher, oldSearcherKey);
        setUpSearcher(newSearcher, newSearcherKey);

        final MockCustomFieldType cfType = new MockCustomFieldType();
        cfType.setKey(CreateCustomField.FIELD_TYPE_PREFIX + "textfield");

        mockCustomFieldLoader(oldName, oldDescription, oldSearcherKey, cfType);
        mockCustomFieldUpdate(newName, newDescription, newSearcherKey, cfType);

        customFieldManager.updateCustomField(TEST_ID, newName, newDescription, newSearcher);

        final CustomField updatedCustomField = customFieldManager.getCustomFieldObject(TEST_ID);
        assertEquals(newName, updatedCustomField.getName());
        assertEquals(newDescription, updatedCustomField.getDescription());
        assertEquals(newSearcherKey, updatedCustomField.getGenericValue().get("customfieldsearcherkey"));

        verify(mockEventPublisher).publish(isA(CustomFieldUpdatedEvent.class));
        dbConnectionManager.assertAllExpectedStatementsWereRun();
    }

    @Test
    public void shouldValidateExistencesOfCustomField() throws Exception {
        prepareGetTests();
        dbConnectionManager.setQueryResults(
                "select CUSTOM_FIELD.id, CUSTOM_FIELD.customfieldtypekey, CUSTOM_FIELD.customfieldsearcherkey, CUSTOM_FIELD.cfname, CUSTOM_FIELD.description, CUSTOM_FIELD.defaultvalue, CUSTOM_FIELD.fieldtype, CUSTOM_FIELD.project, CUSTOM_FIELD.issuetype\n" +
                        "from customfield CUSTOM_FIELD\n" +
                        "where CUSTOM_FIELD.id = " + NON_EXISTENT_ID + "\n" +
                        "limit 1",
                ImmutableList.of()
        );
        assertThat(customFieldManager.exists("customfield_1001"), is(true));
        assertThat(customFieldManager.exists("customfield_" + NON_EXISTENT_ID), is(false));
    }

    @Test
    public void shouldReturnCustomFieldByKey() throws Exception {
        prepareGetTests();
        final CustomField customField = customFieldManager.getCustomFieldObject("customfield_1001");
        assertThat(customField.getIdAsLong(), is(1001L));
        assertNull(customFieldManager.getCustomFieldObject("strangePrefix_1001"));
        dbConnectionManager.assertAllExpectedStatementsWereRun();
    }

    @Test
    public void shouldReturnCustomFieldsByName() throws Exception {
        prepareGetTests();
        final Collection<CustomField> customFieldObjectsByName = customFieldManager.getCustomFieldObjectsByName("CF A");
        final List<CustomField> list = new ArrayList(customFieldObjectsByName);
        assertThat(list.size(), is(2));
        assertThat(list.get(0).getIdAsLong(), is(1001L));
        assertThat(list.get(1).getIdAsLong(), is(1002L));
        dbConnectionManager.assertAllExpectedStatementsWereRun();
    }

    @Test
    public void shouldReturnOneCustomFieldByName() throws Exception {
        prepareGetTests();
        assertThat(customFieldManager.getCustomFieldObjectByName("CF A").getIdAsLong(), is(1001L));
        assertThat(customFieldManager.getCustomFieldObjectByName("CF B").getIdAsLong(), is(1003L));
        assertNull(customFieldManager.getCustomFieldObjectByName("XXX"));
        dbConnectionManager.assertAllExpectedStatementsWereRun();
    }

    @Test
    public void getCustomFieldObjectsByGenericValue() throws Exception {
        prepareGetTests();
        checkGetCustomFieldObjectsMethod(
                (projectId, issueTypeIds, scope) -> {
                    GenericValue genericValue = mock(GenericValue.class);
                    when(genericValue.getLong(eq("project"))).thenReturn(projectId);
                    when(genericValue.getString(eq("type"))).thenReturn(issueTypeIds.get(0));
                    return customFieldManager.getCustomFieldObjects(genericValue);
                }
        );
        dbConnectionManager.assertAllExpectedStatementsWereRun();
    }

    @Test
    public void getCustomFieldObjectsByIssue() throws Exception {
        prepareGetTests();
        checkGetCustomFieldObjectsMethod(
                (projectId, issueTypeIds, scope) -> {
                    Issue issue = mock(Issue.class);
                    when(issue.getProjectId()).thenReturn(projectId);
                    when(issue.getIssueTypeId()).thenReturn(issueTypeIds.get(0));
                    return customFieldManager.getCustomFieldObjects(issue);
                }
        );
        dbConnectionManager.assertAllExpectedStatementsWereRun();
    }

    @Test
    public void getCustomFieldObjectsByProjectIdAndIssueTypeIds() throws Exception {
        prepareGetTests();
        checkGetCustomFieldObjectsMethod(
                (projectId, issueTypeIds, scope) ->
                        customFieldManager.getCustomFieldObjects(projectId, issueTypeIds)
        );
        dbConnectionManager.assertAllExpectedStatementsWereRun();
    }

    @Test
    public void shouldRefreshProperly() throws Exception {
        prepareGetTests();
        reset(issueSearcherManager);
        reset(mockConfigSchemeManager);
        reset(fieldIndexerManager);

        assertThat(customFieldManager.getCustomFieldObjects(), not(empty()));
        customFieldManager.refresh();

        removeCustomFieldsFromDB();
        assertThat(customFieldManager.getCustomFieldObjects(), empty());
        verify(mockConfigSchemeManager).init();
        verify(issueSearcherManager).refresh();
        verify(fieldIndexerManager).refresh();
        dbConnectionManager.assertAllExpectedStatementsWereRun();
    }

    @Test
    public void shouldClearProperly() throws Exception {
        prepareGetTests();
        final MockComponentWorker componentAccessorWorker = new MockComponentWorker();
        final FieldLayoutManager fieldLayoutManager = mock(FieldLayoutManager.class);
        componentAccessorWorker.registerMock(FieldLayoutManager.class, fieldLayoutManager);
        ComponentAccessor.initialiseWorker(componentAccessorWorker);

        assertThat(customFieldManager.getCustomFieldObjects(), not(empty()));
        customFieldManager.clear();

        removeCustomFieldsFromDB();
        assertThat(customFieldManager.getCustomFieldObjects(), empty());
        dbConnectionManager.assertAllExpectedStatementsWereRun();
    }

    @Test
    public void shouldReturnCustomFieldsForSearchContext() throws Exception {
        final CustomFieldManager customFieldManagerSpy = spy(this.customFieldManager);
        final CustomField cfMatching = mock(CustomField.class);
        final CustomField cfNonMatching = mock(CustomField.class);
        final SearchContext searchContext = mock(SearchContext.class);
        when(cfMatching.isInScope(searchContext)).thenReturn(true);
        doReturn(ImmutableList.of(cfMatching, cfNonMatching))
                .when(customFieldManagerSpy).getCustomFieldObjects();

        final List<CustomField> customFields = customFieldManagerSpy.getCustomFieldObjects(searchContext);

        assertThat(ImmutableList.of(cfMatching), is(customFields));
    }

    @Test
    public void shouldReturnGlobalCustomFieldObjects() throws Exception {
        final CustomFieldManager customFieldManagerSpy = spy(this.customFieldManager);
        final CustomField cfGlobal = mock(CustomField.class);
        final CustomField cfNonGlobal = mock(CustomField.class);
        when(cfGlobal.isGlobal()).thenReturn(true);
        when(cfNonGlobal.isGlobal()).thenReturn(false);
        doReturn(ImmutableList.of(cfGlobal, cfNonGlobal))
                .when(customFieldManagerSpy).getCustomFieldObjects();

        final List<CustomField> customFields = customFieldManagerSpy.getGlobalCustomFieldObjects();

        assertThat(ImmutableList.of(cfGlobal), is(customFields));
    }

    @Test
    public void shouldResetCacheBeforeFiringEvent() throws Exception {
        // Prepare the cache and make sure it's going to be empty.
        dbConnectionManager.setQueryResults(
                "select CUSTOM_FIELD.id, CUSTOM_FIELD.customfieldtypekey, CUSTOM_FIELD.customfieldsearcherkey, CUSTOM_FIELD.cfname, CUSTOM_FIELD.description, CUSTOM_FIELD.defaultvalue, CUSTOM_FIELD.fieldtype, CUSTOM_FIELD.project, CUSTOM_FIELD.issuetype\n" +
                        "from customfield CUSTOM_FIELD",
                ImmutableList.of()
        );

        // Populate the cache.
        assertThat(customFieldManager.getCustomFieldObject(TEST_CUSTOMFIELD_ID), nullValue());
        dbConnectionManager.reset();

        doAnswer(invocationOnMock -> {
            assertThat(customFieldManager.getCustomFieldObject(TEST_CUSTOMFIELD_ID), notNullValue());
            return null;
        }).when(mockEventPublisher).publish(any(CustomFieldCreatedEvent.class));

        expectSelectByAll();
        expectInsert();
        customFieldManager.createCustomField(
                "fieldName",
                "description",
                new MockCustomFieldType(),
                null,
                Arrays.asList(context),
                new ArrayList<>()
        );
        verify(mockEventPublisher).publish(any(CustomFieldCreatedEvent.class));
        dbConnectionManager.assertAllExpectedStatementsWereRun();
    }

    @Test
    public void cacheRequestsCustomFieldTypeOnlyWhenRequired() throws Exception {
        final String typeKey = "key";
        final CustomField customField = mock(ImmutableCustomField.class);

        when(customFieldFactory.create(any(CustomFieldDTO.class))).thenAnswer(invocationOnMock -> {
            CustomFieldDTO customFieldDTO = (CustomFieldDTO) invocationOnMock.getArguments()[0];
            Whitebox.setInternalState(customField, "scopeFactory", customFieldScopeFactory);
            if (customFieldDTO != null) {
                assertThat("CustomFieldKey should be set", customFieldDTO.getCustomfieldtypekey(), is(typeKey));

                when(customField.getId()).thenReturn(customFieldDTO.getId().toString());
                when(customField.getIdAsLong()).thenReturn(customFieldDTO.getId());
                when(customField.getName()).thenReturn(customFieldDTO.getName());
                // return null first time, and then a real value!
                when(customField.getCustomFieldType()).thenReturn(null, new MockCustomFieldType());
            }
            return customField;
        });

        final ResultRow resultRow = new ResultRow(TEST_ID, typeKey, null, "CF A", null, null, 1L, null, null);

        dbConnectionManager.setQueryResults(
                "select CUSTOM_FIELD.id, CUSTOM_FIELD.customfieldtypekey, CUSTOM_FIELD.customfieldsearcherkey, CUSTOM_FIELD.cfname, CUSTOM_FIELD.description, CUSTOM_FIELD.defaultvalue, CUSTOM_FIELD.fieldtype, CUSTOM_FIELD.project, CUSTOM_FIELD.issuetype\n" +
                        "from customfield CUSTOM_FIELD",
                ImmutableList.of(resultRow)
        );

        // Retrive the cached value, the first time it is null as no custom field type
        assertThat(customFieldManager.getCustomFieldObject(TEST_ID), nullValue());
        verify(customField).getCustomFieldType();

        // Retrive the cached value, the second time it is the actual value
        // we are asserting that the cache always requests the custom field type when called, rather than only
        // once on cache creation as previous
        assertThat(customFieldManager.getCustomFieldObject(TEST_ID), is(customField));
        verify(customField, times(2)).getCustomFieldType();
    }
}
