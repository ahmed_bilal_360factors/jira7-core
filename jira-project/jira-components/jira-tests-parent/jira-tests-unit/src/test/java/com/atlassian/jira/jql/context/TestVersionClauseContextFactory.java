package com.atlassian.jira.jql.context;

import com.atlassian.jira.jql.operand.JqlOperandResolver;
import com.atlassian.jira.jql.operand.QueryLiteral;
import com.atlassian.jira.jql.resolver.VersionResolver;
import com.atlassian.jira.jql.validator.MockJqlOperandResolver;
import com.atlassian.jira.mock.project.MockVersion;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.project.version.VersionManager;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.collect.CollectionBuilder;
import com.atlassian.query.clause.TerminalClauseImpl;
import com.atlassian.query.operand.EmptyOperand;
import com.atlassian.query.operand.SingleValueOperand;
import com.atlassian.query.operator.Operator;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.atlassian.jira.jql.operand.SimpleLiteralFactory.createLiteral;
import static com.google.common.collect.Sets.union;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestVersionClauseContextFactory {
    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @Mock
    private JqlOperandResolver jqlOperandResolver;
    @Mock
    private VersionManager versionManager;
    @Mock
    private PermissionManager permissionManager;

    private VersionResolver versionResolver;
    private ApplicationUser theUser = null;

    @Before
    public void setUp() throws Exception {
        versionResolver = new VersionResolver(versionManager);
    }

    @Test
    public void testGetContextFromClauseSingleValueEqualityOperand() throws Exception {
        final MockProject project = new MockProject(1234L);
        final SingleValueOperand operand = new SingleValueOperand("blarg");
        final TerminalClauseImpl clause = new TerminalClauseImpl("blarg", Operator.EQUALS, operand);

        when(jqlOperandResolver.getValues(theUser, operand, clause))
                .thenReturn(CollectionBuilder.newBuilder(createLiteral("blarg")).asList());

        final Version version = new MockVersion(10, "version", project);

        VersionResolver versionResolver = new VersionResolver(versionManager) {
            @Override
            public Version get(final Long id) {
                return version;
            }
        };
        final Set<ProjectIssueTypeContext> issueTypeContexts = CollectionBuilder.<ProjectIssueTypeContext>newBuilder(new ProjectIssueTypeContextImpl(new ProjectContextImpl(10L), new IssueTypeContextImpl("it"))).asListOrderedSet();

        VersionClauseContextFactory factory = new VersionClauseContextFactory(jqlOperandResolver, versionResolver, permissionManager) {
            @Override
            List<Long> getIds(final QueryLiteral literal) {
                return CollectionBuilder.newBuilder(10L).asList();
            }

            @Override
            Set<ProjectIssueTypeContext> getContextsForProject(final ApplicationUser searcher, final Project project) {
                return issueTypeContexts;
            }
        };

        final ClauseContext result = factory.getContextFromClause(theUser, clause);
        ClauseContext expectedResult = new ClauseContextImpl(issueTypeContexts);

        assertEquals(expectedResult, result);
    }

    @Test
    public void testGetContextFromClauseSingleValueNegationOperand() throws Exception {
        final SingleValueOperand operand = new SingleValueOperand("blarg");
        final TerminalClauseImpl clause = new TerminalClauseImpl("blarg", Operator.NOT_EQUALS, operand);

        when(jqlOperandResolver.getValues(theUser, operand, clause))
                .thenReturn(CollectionBuilder.newBuilder(createLiteral(10L)).asList());

        final MockProject project1 = new MockProject(1234L);
        final MockProject project2 = new MockProject(5678L);
        final MockProject project3 = new MockProject(9876L);
        final Version excludedVersion = new MockVersion(10, "excludedVersion", project1);
        final Version version1 = new MockVersion(15, "version1", project2);
        final Version version2 = new MockVersion(20, "version2", project3);

        VersionResolver versionResolver = new VersionResolver(versionManager) {
            @Override
            public Version get(final Long id) {
                if (id.equals(10L)) {
                    return excludedVersion;
                }
                return null;
            }

            @Override
            public Collection<Version> getAll() {
                return CollectionBuilder.newBuilder(excludedVersion, version1, version2).asList();
            }
        };

        final Set<ProjectIssueTypeContext> issueTypeContexts1 = CollectionBuilder.<ProjectIssueTypeContext>newBuilder(new ProjectIssueTypeContextImpl(new ProjectContextImpl(10L), new IssueTypeContextImpl("it"))).asListOrderedSet();
        final Set<ProjectIssueTypeContext> issueTypeContexts2 = CollectionBuilder.<ProjectIssueTypeContext>newBuilder(new ProjectIssueTypeContextImpl(new ProjectContextImpl(50L), new IssueTypeContextImpl("it2"))).asListOrderedSet();

        VersionClauseContextFactory factory = new VersionClauseContextFactory(jqlOperandResolver, versionResolver, permissionManager) {
            @Override
            List<Long> getIds(final QueryLiteral literal) {
                return CollectionBuilder.newBuilder(10L).asList();
            }

            @Override
            Set<ProjectIssueTypeContext> getContextsForProject(final ApplicationUser searcher, final Project project) {
                if (project.getId().equals(5678L)) {
                    return issueTypeContexts1;
                } else if (project.getId().equals(9876L)) {
                    return issueTypeContexts2;
                }
                return null;
            }
        };

        final ClauseContext result = factory.getContextFromClause(theUser, clause);
        ClauseContext expectedResult = new ClauseContextImpl(new HashSet<>(union(issueTypeContexts1, issueTypeContexts2)));

        assertEquals(expectedResult, result);
    }

    @Test
    public void testGetContextFromClauseMultipleValueNegationOperand() throws Exception {
        final SingleValueOperand operand = new SingleValueOperand("blarg");
        final TerminalClauseImpl clause = new TerminalClauseImpl("blarg", Operator.NOT_EQUALS, operand);

        when(jqlOperandResolver.getValues(theUser, operand, clause))
                .thenReturn(CollectionBuilder.newBuilder(createLiteral(10L)).asList());

        final MockProject project1 = new MockProject(1234L);
        final MockProject project2 = new MockProject(5678L);
        final MockProject project3 = new MockProject(9876L);
        final Version excludedVersion1 = new MockVersion(10, "excludedVersion1", project1);
        final Version excludedVersion2 = new MockVersion(20, "excludedVersion2", project3);
        final Version version1 = new MockVersion(15, "version1", project2);

        VersionResolver versionResolver = new VersionResolver(versionManager) {
            @Override
            public Version get(final Long id) {
                if (id.equals(10L)) {
                    return excludedVersion1;
                }
                if (id.equals(20L)) {
                    return excludedVersion2;
                }
                return null;
            }

            @Override
            public Collection<Version> getAll() {
                return CollectionBuilder.newBuilder(excludedVersion1, version1, excludedVersion2).asList();
            }
        };

        final Set<ProjectIssueTypeContext> issueTypeContexts1 = CollectionBuilder.<ProjectIssueTypeContext>newBuilder(new ProjectIssueTypeContextImpl(new ProjectContextImpl(10L), new IssueTypeContextImpl("it"))).asListOrderedSet();

        VersionClauseContextFactory factory = new VersionClauseContextFactory(jqlOperandResolver, versionResolver, permissionManager) {
            @Override
            List<Long> getIds(final QueryLiteral literal) {
                return CollectionBuilder.newBuilder(10L, 20L).asList();
            }

            @Override
            Set<ProjectIssueTypeContext> getContextsForProject(final ApplicationUser searcher, final Project project) {
                if (project.getId().equals(5678L)) {
                    return issueTypeContexts1;
                }
                return null;
            }
        };

        final ClauseContext result = factory.getContextFromClause(theUser, clause);
        ClauseContext expectedResult = new ClauseContextImpl(issueTypeContexts1);

        assertEquals(expectedResult, result);
    }

    @Test
    public void testGetContextFromClauseSingleValueRelationalOperand() throws Exception {
        final SingleValueOperand operand = new SingleValueOperand("specifiedVersion");
        final TerminalClauseImpl clause = new TerminalClauseImpl("blarg", Operator.GREATER_THAN, operand);

        when(jqlOperandResolver.getValues(theUser, operand, clause))
                .thenReturn(CollectionBuilder.newBuilder(createLiteral(10L)).asList());

        final Project project = new MockProject(50, "name");

        final Version specifiedVersion = new MockVersion(10, "specifiedVersion", project, 1L);
        final Version version1 = new MockVersion(15, "version1", project, 2L);
        final Version version2 = new MockVersion(20, "version2", project, 3L);

        VersionResolver versionResolver = new VersionResolver(versionManager) {
            @Override
            public Version get(final Long id) {
                if (id.equals(10L)) {
                    return specifiedVersion;
                }
                return null;
            }

            @Override
            public Collection<Version> getAll() {
                return CollectionBuilder.newBuilder(specifiedVersion, version1, version2).asList();
            }
        };

        final Set<ProjectIssueTypeContext> issueTypeContexts1 = CollectionBuilder.<ProjectIssueTypeContext>newBuilder(new ProjectIssueTypeContextImpl(new ProjectContextImpl(50L), new IssueTypeContextImpl("it2"))).asListOrderedSet();

        VersionClauseContextFactory factory = new VersionClauseContextFactory(jqlOperandResolver, versionResolver, permissionManager) {
            boolean first = true;

            @Override
            List<Long> getIds(final QueryLiteral literal) {
                return CollectionBuilder.newBuilder(10L).asList();
            }

            @Override
            Set<ProjectIssueTypeContext> getContextsForProject(final ApplicationUser searcher, final Project project) {
                if (project.getId() == 50L) {
                    return issueTypeContexts1;
                } else {
                    throw new IllegalStateException();
                }
            }
        };

        final ClauseContext result = factory.getContextFromClause(theUser, clause);
        ClauseContext expectedResult = new ClauseContextImpl(new HashSet<>(issueTypeContexts1));

        assertEquals(expectedResult, result);
    }

    @Test
    public void testGetContextFromClauseMultiValueEqualityOperand() throws Exception {
        final SingleValueOperand operand = new SingleValueOperand("blarg");
        final TerminalClauseImpl clause = new TerminalClauseImpl("blarg", Operator.EQUALS, operand);

        when(jqlOperandResolver.getValues(theUser, operand, clause))
                .thenReturn(CollectionBuilder.newBuilder(createLiteral(10L), createLiteral(20L)).asList());

        final MockProject project1 = new MockProject(5678L);
        final MockProject project2 = new MockProject(9876L);
        final Version version1 = new MockVersion(10, "version", project1);
        final Version version2 = new MockVersion(20, "version", project2);

        VersionResolver versionResolver = new VersionResolver(versionManager) {
            @Override
            public Version get(final Long id) {
                if (id.equals(10L)) {
                    return version1;
                } else if (id.equals(20L)) {
                    return version2;
                }
                return null;
            }
        };

        final Set<ProjectIssueTypeContext> issueTypeContexts1 = CollectionBuilder.<ProjectIssueTypeContext>newBuilder(new ProjectIssueTypeContextImpl(new ProjectContextImpl(10L), new IssueTypeContextImpl("it"))).asListOrderedSet();
        final Set<ProjectIssueTypeContext> issueTypeContexts2 = CollectionBuilder.<ProjectIssueTypeContext>newBuilder(new ProjectIssueTypeContextImpl(new ProjectContextImpl(20L), new IssueTypeContextImpl("it2"))).asListOrderedSet();

        VersionClauseContextFactory factory = new VersionClauseContextFactory(jqlOperandResolver, versionResolver, permissionManager) {
            @Override
            List<Long> getIds(final QueryLiteral literal) {
                return CollectionBuilder.newBuilder(literal.getLongValue()).asList();
            }

            @Override
            Set<ProjectIssueTypeContext> getContextsForProject(final ApplicationUser searcher, final Project project) {
                if (project.getId().equals(5678L)) {
                    return issueTypeContexts1;
                } else if (project.getId().equals(9876L)) {
                    return issueTypeContexts2;
                }
                return null;
            }
        };

        final ClauseContext result = factory.getContextFromClause(theUser, clause);
        ClauseContext expectedResult = new ClauseContextImpl(new HashSet<>(union(issueTypeContexts1, issueTypeContexts2)));

        assertEquals(expectedResult, result);
    }

    @Test
    public void testGetContextFromClauseEmpty() throws Exception {
        final EmptyOperand operand = EmptyOperand.EMPTY;
        final TerminalClauseImpl clause = new TerminalClauseImpl("blarg", Operator.IS, operand);

        jqlOperandResolver = MockJqlOperandResolver.createSimpleSupport();

        VersionClauseContextFactory factory = new VersionClauseContextFactory(jqlOperandResolver, versionResolver, permissionManager);

        final ClauseContext result = factory.getContextFromClause(theUser, clause);
        ClauseContext expectedResult = ClauseContextImpl.createGlobalClauseContext();

        assertEquals(expectedResult, result);
    }

    @Test
    public void testGetContextFromClauseNullLiterals() throws Exception {
        final EmptyOperand operand = EmptyOperand.EMPTY;
        final TerminalClauseImpl clause = new TerminalClauseImpl("blarg", Operator.IS, operand);

        when(jqlOperandResolver.getValues(theUser, operand, clause)).thenReturn(null);

        VersionClauseContextFactory factory = new VersionClauseContextFactory(jqlOperandResolver, versionResolver, permissionManager);

        final ClauseContext result = factory.getContextFromClause(theUser, clause);
        ClauseContext expectedResult = ClauseContextImpl.createGlobalClauseContext();

        assertEquals(expectedResult, result);
    }

    @Test
    public void testGetContextFromClauseInvalidOperator() throws Exception {
        final SingleValueOperand operand = new SingleValueOperand("blarg");
        final TerminalClauseImpl clause = new TerminalClauseImpl("blarg", Operator.LIKE, operand);

        VersionClauseContextFactory factory = new VersionClauseContextFactory(jqlOperandResolver, versionResolver, permissionManager);

        final ClauseContext result = factory.getContextFromClause(theUser, clause);
        ClauseContext expectedResult = ClauseContextImpl.createGlobalClauseContext();

        assertEquals(expectedResult, result);
    }
}
