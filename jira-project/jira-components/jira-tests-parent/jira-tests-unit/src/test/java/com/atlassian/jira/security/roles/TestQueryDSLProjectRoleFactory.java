package com.atlassian.jira.security.roles;

import com.atlassian.jira.model.querydsl.QProjectRole;
import com.querydsl.core.Tuple;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestQueryDSLProjectRoleFactory {
    private static final Long PROJECT_ROLE_ID = 1l;
    private static final String PROJECT_ROLE_NAME = "project role name";
    private static final String PROJECT_ROLE_DESCRIPTION = "description";

    @Test
    public void testCreatinfProjectRoleFromQueryDSLTuple() {
        final ProjectRole projectRole = new QueryDSLProjectRoleFactory().createProjectRole(mockTuple());

        assertEquals(PROJECT_ROLE_ID, projectRole.getId());
        assertEquals(PROJECT_ROLE_NAME, projectRole.getName());
        assertEquals(PROJECT_ROLE_DESCRIPTION, projectRole.getDescription());
    }

    private static Tuple mockTuple() {
        final Tuple tuple = mock(Tuple.class);
        when(tuple.get(QProjectRole.PROJECT_ROLE.id)).thenReturn(PROJECT_ROLE_ID);
        when(tuple.get(QProjectRole.PROJECT_ROLE.name)).thenReturn(PROJECT_ROLE_NAME);
        when(tuple.get(QProjectRole.PROJECT_ROLE.description)).thenReturn(PROJECT_ROLE_DESCRIPTION);
        return tuple;
    }
}
