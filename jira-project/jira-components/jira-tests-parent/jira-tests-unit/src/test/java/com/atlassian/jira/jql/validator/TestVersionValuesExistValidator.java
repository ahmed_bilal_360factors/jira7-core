package com.atlassian.jira.jql.validator;

import com.atlassian.jira.jql.operand.JqlOperandResolver;
import com.atlassian.jira.jql.resolver.VersionIndexInfoResolver;
import com.atlassian.jira.mock.project.MockVersion;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.version.VersionManager;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.collect.CollectionBuilder;
import com.google.common.collect.ImmutableList;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import static com.atlassian.jira.permission.ProjectPermissions.BROWSE_PROJECTS;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestVersionValuesExistValidator {
    @Rule
    public MockitoRule mockito = MockitoJUnit.rule();
    @Mock
    private VersionIndexInfoResolver indexInfoResolver;
    @Mock
    private PermissionManager permissionManager;
    @Mock
    private VersionManager versionManager;
    @Mock
    private JqlOperandResolver operandResolver;
    @Mock
    private I18nHelper.BeanFactory beanFactory;
    @InjectMocks
    private VersionValuesExistValidator validator;


    @Test
    public void testVersionExistsAndHasPermssion() throws Exception {
        final String name = "blah";
        final Long id = 10L;
        final MockProject project = new MockProject();
        final MockVersion version = new MockVersion(id, name, project);

        when(indexInfoResolver.getIndexedValues(name)).thenReturn(CollectionBuilder.newBuilder(id.toString()).asList());
        when(versionManager.getVersion(id)).thenReturn(version);
        when(permissionManager.hasPermission(BROWSE_PROJECTS, project, null)).thenReturn(true);

        assertTrue(validator.stringValueExists(null, name));
    }

    @Test
    public void testVersionExistsAndHasNoPermssion() throws Exception {
        final String name = "blah";
        final Long id = 10L;
        final MockProject project = new MockProject();
        final MockVersion version = new MockVersion(id, name, project);

        when(indexInfoResolver.getIndexedValues(name)).thenReturn(CollectionBuilder.newBuilder(id.toString()).asList());
        when(versionManager.getVersion(id)).thenReturn(version);
        when(permissionManager.hasPermission(BROWSE_PROJECTS, project, null)).thenReturn(false);

        assertFalse(validator.stringValueExists(null, name));
    }

    @Test
    public void testTwoVersionsExistsAndOneHasNoPermssion() throws Exception {
        final String name = "blah";
        final Long id1 = 10L;
        final Long id2 = 10L;
        final MockProject project1 = new MockProject();
        final MockProject project2 = new MockProject();
        final MockVersion version1 = new MockVersion(id1, name, project1);
        final MockVersion version2 = new MockVersion(id2, name, project2);

        when(indexInfoResolver.getIndexedValues(name)).thenReturn(CollectionBuilder.newBuilder(id1.toString(), id2.toString()).asList());
        when(versionManager.getVersion(id1)).thenReturn(version1);
        when(permissionManager.hasPermission(BROWSE_PROJECTS, project1, null)).thenReturn(false);
        when(versionManager.getVersion(id2)).thenReturn(version2);
        when(permissionManager.hasPermission(BROWSE_PROJECTS, project2, null)).thenReturn(true);

        assertTrue(validator.stringValueExists(null, name));
    }

    @Test
    public void testNoVersionExist() throws Exception {
        final String name = "blah";

        when(indexInfoResolver.getIndexedValues(name)).thenReturn(ImmutableList.of());

        assertFalse(validator.stringValueExists(null, name));
    }

    @Test
    public void testLongValueExist() throws Exception {
        Long id = 10L;
        when(indexInfoResolver.getIndexedValues(id)).thenReturn(Collections.emptyList());
        when(indexInfoResolver.getIndexedValues(id)).thenReturn(Collections.emptyList());

        final AtomicInteger called = new AtomicInteger(0);
        VersionValuesExistValidator validator = new VersionValuesExistValidator(operandResolver, indexInfoResolver, permissionManager, versionManager, beanFactory) {
            @Override
            boolean versionExists(final ApplicationUser searcher, final List<String> ids) {
                return called.incrementAndGet() == 1;
            }
        };

        assertTrue(validator.longValueExist(null, id));
        assertFalse(validator.longValueExist(null, id));

        assertEquals(2, called.get());
    }
}
