package com.atlassian.jira.upgrade.tasks;

import com.atlassian.jira.action.issue.customfields.MockCustomFieldType;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.fields.MockCustomField;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutManager;
import com.atlassian.jira.mock.ofbiz.MockGenericValue;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.util.collect.CollectionBuilder;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.ofbiz.core.entity.GenericValue;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestUpgradeTask_Build571 {
    @Rule
    public MockitoRule mockito = MockitoJUnit.rule();
    @Mock
    private OfBizDelegator mockOfBizDelegator;
    @Mock
    private CustomFieldManager mockCustomFieldManager;
    @Mock
    private FieldLayoutManager mockLayoutManager;
    private UpgradeTask_Build571 upgradeTask;

    @Before
    public void setUp() throws Exception {
        upgradeTask = new UpgradeTask_Build571(mockOfBizDelegator, mockCustomFieldManager, mockLayoutManager);
    }

    @Test
    public void testMetaData() {
        assertEquals(571, upgradeTask.getBuildNumber());
        assertEquals("Initialize versions & components field renderers to autocomplete renderer default", upgradeTask.getShortDescription());
    }

    @Test
    public void shouldUpdateMultiVersionFields() throws Exception {
        final GenericValue gv1 = mockFieldLayoutGV(10034L, "versions");
        final GenericValue gv2 = mockFieldLayoutGV(10064L, "components");
        final GenericValue gv3 = mockFieldLayoutGV(10037L, "fixVersions");
        final GenericValue gv4 = mockFieldLayoutGV(10039L, "customfield_23405");
        final GenericValue gv5 = mockFieldLayoutGV(10099L, "customfield_10780");
        final GenericValue gv6 = mockFieldLayoutGV(10079L, "customfield_10785");

        when(mockOfBizDelegator.findByCondition("FieldLayoutItem", null, CollectionBuilder.list("fieldidentifier", "id"))).thenReturn(
                ImmutableList.of(gv1, gv2, gv3, gv4, gv5, gv6));

        mockCustomField("customfield_23405", "com.atlassian.jira.plugin.system.customfieldtypes:multiversion", "Multi picker"); //should be picked up
        mockCustomField("customfield_10780", "com.atlassian.jira.plugin.system.customfieldtypes:text", "text picker");
        mockCustomField("customfield_10785", "com.atlassian.jira.plugin.system.customfieldtypes:multiversion", "Another version"); //should be picked up

        upgradeTask.doUpgrade(false);

        verify(mockOfBizDelegator).bulkUpdateByPrimaryKey("FieldLayoutItem",
                ImmutableMap.of("renderertype", "frother-control-renderer"), CollectionBuilder.list(10039L, 10079L));
    }

    @Test
    public void shouldUpdateKnownSystemFields() throws Exception {
        upgradeTask.doUpgrade(false);

        verify(mockOfBizDelegator).bulkUpdateByAnd("FieldLayoutItem",
                ImmutableMap.of("renderertype", "frother-control-renderer"), ImmutableMap.of("fieldidentifier", "fixVersions"));
        verify(mockOfBizDelegator).bulkUpdateByAnd("FieldLayoutItem",
                ImmutableMap.of("renderertype", "frother-control-renderer"), ImmutableMap.of("fieldidentifier", "versions"));
        verify(mockOfBizDelegator).bulkUpdateByAnd("FieldLayoutItem",
                ImmutableMap.of("renderertype", "frother-control-renderer"), ImmutableMap.of("fieldidentifier", "components"));

        verify(mockLayoutManager).refresh();
    }

    @Test
    public void shouldRefreshLayoutManagerAfterUpdatesEvenIfUpdateThrowsException() throws Exception {
        doThrow(new RuntimeException("shouldRefreshLayoutManagerAfterUpdatesEvenIfUpdateThrowsException")).when(mockOfBizDelegator).bulkUpdateByAnd(anyString(), any(), any());

        try {
            upgradeTask.doUpgrade(false);
            fail();
        } catch (RuntimeException e) {
            assertThat(e.getMessage(), Matchers.equalTo("shouldRefreshLayoutManagerAfterUpdatesEvenIfUpdateThrowsException"));
        }

        verify(mockLayoutManager).refresh();
    }


    private void mockCustomField(final String fieldId, final String fieldTypeKey, final String fieldName) {
        final MockCustomField cf1 = new MockCustomField();
        cf1.setCustomFieldType(new MockCustomFieldType(fieldTypeKey, fieldName));
        when(mockCustomFieldManager.getCustomFieldObject(fieldId)).thenReturn(cf1);
    }

    private MockGenericValue mockFieldLayoutGV(final long id, final String fieldIdentifier) {
        return new MockGenericValue("FieldLayoutItem", ImmutableMap.of("id", id, "fieldidentifier", fieldIdentifier));
    }
}
