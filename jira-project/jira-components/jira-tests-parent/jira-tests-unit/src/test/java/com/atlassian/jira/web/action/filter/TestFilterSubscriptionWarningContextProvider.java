package com.atlassian.jira.web.action.filter;

import com.atlassian.jira.bc.filter.FilterSubscriptionService;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.issue.search.SearchRequestManager;
import com.atlassian.jira.issue.subscription.FilterSubscription;
import com.atlassian.jira.user.ApplicationUser;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Map;

import static com.atlassian.jira.bc.filter.FilterDeletionWarningViewProviderImpl.CONTEXT_KEY_SEARCH_REQUEST;
import static com.atlassian.jira.web.action.filter.FilterSubscriptionWarningContextProvider.CONTEXT_KEY_SUBSCRIPTION_COUNT;
import static com.atlassian.jira.web.action.filter.FilterSubscriptionWarningContextProvider.CONTEXT_KEY_SUBSCRIPTION_PAGE_URL;
import static com.google.common.collect.Maps.newHashMap;
import static org.hamcrest.collection.IsMapContaining.hasEntry;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestFilterSubscriptionWarningContextProvider {
    @Mock
    private FilterSubscriptionService filterSubscriptionService;

    @Mock
    private SearchRequestManager searchRequestManager;
    
    @Mock
    private SearchRequest searchRequest;
    
    @Mock
    private ApplicationUser filterOwner;

    @InjectMocks
    private FilterSubscriptionWarningContextProvider filterSubscriptionWarningContextProvider;

    private Map<String, Object> contextMap;

    @Before
    public void setUp() throws Exception {
        when(searchRequest.getId()).thenReturn(10000L);
        when(searchRequestManager.getSearchRequestOwner(eq(searchRequest.getId()))).thenReturn(filterOwner);

        contextMap = newHashMap();
        contextMap.put(CONTEXT_KEY_SEARCH_REQUEST, searchRequest);
    }

    private void givenSubscriptionCount(int count) {
        ArrayList<FilterSubscription> filterSubscriptions = mock(ArrayList.class);
        when(filterSubscriptions.size()).thenReturn(count);
        when(filterSubscriptionService.getVisibleFilterSubscriptions(eq(filterOwner), eq(searchRequest))).thenReturn(filterSubscriptions);
    }

    @Test
    public void testWarningForOneSubscription() throws Exception {
        givenSubscriptionCount(1);

        Map<String, Object> resultMap = filterSubscriptionWarningContextProvider.getContextMap(contextMap);

        assertThat(resultMap, hasEntry(CONTEXT_KEY_SUBSCRIPTION_COUNT, 1));
        assertThat(resultMap, hasEntry(CONTEXT_KEY_SUBSCRIPTION_PAGE_URL, "/secure/ViewSubscriptions.jspa?filterId=10000"));
    }

    @Test
    public void testWarningForManySubscriptions() throws Exception {
        givenSubscriptionCount(5);

        Map<String, Object> resultMap = filterSubscriptionWarningContextProvider.getContextMap(contextMap);

        assertThat(resultMap, hasEntry(CONTEXT_KEY_SUBSCRIPTION_COUNT, 5));
        assertThat(resultMap, hasEntry(CONTEXT_KEY_SUBSCRIPTION_PAGE_URL, "/secure/ViewSubscriptions.jspa?filterId=10000"));
    }

    @Test
    public void testReturnsNoWarningForNoSubscriptions() throws Exception {
        givenSubscriptionCount(0);

        Map<String, Object> resultMap = filterSubscriptionWarningContextProvider.getContextMap(contextMap);

        assertThat(resultMap, hasEntry(CONTEXT_KEY_SUBSCRIPTION_COUNT, 0));
        assertThat(resultMap, hasEntry(CONTEXT_KEY_SUBSCRIPTION_PAGE_URL, "/secure/ViewSubscriptions.jspa?filterId=10000"));
    }
}