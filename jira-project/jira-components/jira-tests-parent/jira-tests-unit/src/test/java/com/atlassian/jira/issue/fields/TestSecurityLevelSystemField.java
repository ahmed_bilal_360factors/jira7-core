package com.atlassian.jira.issue.fields;

import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.datetime.DateTimeFormatter;
import com.atlassian.jira.datetime.DateTimeFormatterFactoryStub;
import com.atlassian.jira.issue.MockIssueFactory;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.customfields.OperationContext;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem;
import com.atlassian.jira.issue.fields.util.MessagedResult;
import com.atlassian.jira.issue.issuetype.MockIssueType;
import com.atlassian.jira.issue.security.IssueSecurityLevelImpl;
import com.atlassian.jira.issue.security.MockIssueSecurityLevelManager;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.mock.MockProjectManager;
import com.atlassian.jira.mock.issue.MockIssue;
import com.atlassian.jira.mock.security.MockAuthenticationContext;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.template.TemplateSource;
import com.atlassian.jira.template.VelocityTemplatingEngine;
import com.atlassian.jira.template.soy.SoyTemplateRendererProvider;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.web.action.util.CalendarLanguageUtil;
import com.atlassian.jira.web.bean.BulkEditBean;
import com.google.common.collect.ImmutableList;
import org.hamcrest.Matchers;
import org.hamcrest.collection.IsMapContaining;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyMap;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Test SecurityLevelSystemField
 *
 * @since v3.13
 */
public class TestSecurityLevelSystemField {
    private MockProjectManager mockProjectManager = new MockProjectManager();

    @Rule
    public RuleChain init = MockitoMocksInContainer.forTest(this);

    @Mock
    @AvailableInContainer
    CalendarLanguageUtil calendarLanguageUtil;

    @Mock
    @AvailableInContainer
    FeatureManager featureManager;

    @Mock
    @AvailableInContainer
    SoyTemplateRendererProvider soyTemplateRendererProvider;

    @Mock
    @AvailableInContainer
    PermissionManager permissionManager;

    @AvailableInContainer
    DateTimeFormatter dateTimeFormatter = new DateTimeFormatterFactoryStub().formatter();

    @Before
    public void setUp() {
        MockIssueFactory.setProjectManager(mockProjectManager);
        mockProjectManager.addProject(new MockProject(1, "COW"));
        mockProjectManager.addProject(new MockProject(2, "DOG"));
    }

    @Test
    public void testNeedsMoveWithDefaultTargetSecurityLevel() throws Exception {
        MockIssueSecurityLevelManager mockIssueSecurityLevelManager = new MockIssueSecurityLevelManager();
        mockIssueSecurityLevelManager.setDefaultSecurityLevelForProject(2L, 20000L);
        SecurityLevelSystemField securityLevelSystemField = new SecurityLevelSystemField(null, null, null, null,
                mockIssueSecurityLevelManager, null, null, null);

        MutableIssue mockSourceIssue = MockIssueFactory.createIssue(1, "RANDOM-1", 2);
        MutableIssue mockTargetIssue = MockIssueFactory.createIssue(1, "RANDOM-1", 2);

        // Needs move if the target is a required field
        FieldLayoutItem mockTargetFieldLayoutItem = mock(FieldLayoutItem.class);
        when(mockTargetFieldLayoutItem.isRequired()).thenReturn(true);

        MessagedResult messagedResult = securityLevelSystemField.needsMove(Arrays.asList(mockSourceIssue), mockTargetIssue, mockTargetFieldLayoutItem);
        assertTrue(messagedResult.getResult());
        assertNull(messagedResult.getMessage());

        // Source and target projects are the same - Should not need move
        when(mockTargetFieldLayoutItem.isRequired()).thenReturn(false);

        messagedResult = securityLevelSystemField.needsMove(Arrays.asList(mockSourceIssue), mockTargetIssue, mockTargetFieldLayoutItem);
        assertFalse(messagedResult.getResult());
        assertNull(messagedResult.getMessage());

        // Make the Source and Target Projects different, so that we take notice of the default.
        mockSourceIssue.setProjectId(1L);
        messagedResult = securityLevelSystemField.needsMove(Arrays.asList(mockSourceIssue), mockTargetIssue, mockTargetFieldLayoutItem);
        assertTrue(messagedResult.getResult());
        assertNull(messagedResult.getMessage());
    }

    @Test
    public void testNeedsMoveWithoutDefaultTargetSecurityLevel() throws Exception {
        MockIssueSecurityLevelManager mockIssueSecurityLevelManager = new MockIssueSecurityLevelManager();

        MutableIssue mockSourceIssue = MockIssueFactory.createIssue(1, "RANDOM-1", 2);
        MutableIssue mockTargetIssue = MockIssueFactory.createIssue(1, "RANDOM-1", 1);

        SecurityLevelSystemField securityLevelSystemField = new SecurityLevelSystemField(null, null, null, null,
                mockIssueSecurityLevelManager, null, null, null);

        // Needs move if is a required field on the target
        FieldLayoutItem mockTargetFieldLayoutItem = mock(FieldLayoutItem.class);
        when(mockTargetFieldLayoutItem.isRequired()).thenReturn(true);

        MessagedResult messagedResult = securityLevelSystemField.needsMove(Arrays.asList(mockSourceIssue), mockTargetIssue, mockTargetFieldLayoutItem);
        assertTrue(messagedResult.getResult());
        assertNull(messagedResult.getMessage());

        // .. but not if the not required field
        when(mockTargetFieldLayoutItem.isRequired()).thenReturn(false);

        messagedResult = securityLevelSystemField.needsMove(Arrays.asList(mockSourceIssue), mockTargetIssue, mockTargetFieldLayoutItem);
        assertFalse(messagedResult.getResult());
        assertNull(messagedResult.getMessage());
    }

    @Test
    public void testNeedsMoveSecuredSubtaskToSecuredSubtaskWithSameSecurityLevel() {
        MockIssueSecurityLevelManager mockIssueSecurityLevelManager = new MockIssueSecurityLevelManager();
        MockIssueFactory.setIssueSecurityLevelManager(mockIssueSecurityLevelManager);
        mockIssueSecurityLevelManager.addIssueSecurityLevel(new IssueSecurityLevelImpl(10000l, "Test security level", "Test security level", 1l));
        mockIssueSecurityLevelManager.setUsersSecurityLevelsResult(Arrays.asList(mockIssueSecurityLevelManager.getSecurityLevel(10000L)));

        MutableIssue mockSourceIssue = MockIssueFactory.createIssue(1, "RANDOM-1", 1);
        MutableIssue mockSourceParentIssue = createTargetParentIssue(mockSourceIssue, 777l, 10000l);
        mockSourceIssue.setSecurityLevelId(10000l);
        MutableIssue mockTargetIssue = MockIssueFactory.createIssue(1, "RANDOM-1", 1);
        MutableIssue mockTargetParentIssue = createTargetParentIssue(mockTargetIssue, 999l, 10000l);

        SecurityLevelSystemField securityLevelSystemField = new SecurityLevelSystemField(null, null, null, new MockAuthenticationContext(null),
                mockIssueSecurityLevelManager, null, null, null);

        // No need to move when security levels are the same
        FieldLayoutItem mockTargetFieldLayoutItem = mock(FieldLayoutItem.class);
        when(mockTargetFieldLayoutItem.isRequired()).thenReturn(false);

        //test same project
        MessagedResult messagedResult = securityLevelSystemField.needsMove(Arrays.asList(mockSourceIssue), mockTargetIssue, mockTargetFieldLayoutItem);
        assertFalse(messagedResult.getResult());
        assertNull(messagedResult.getMessage());

        //test different project
        mockTargetIssue = MockIssueFactory.createIssue(1, "RANDOM-1", 2);
        mockTargetParentIssue = createTargetParentIssue(mockTargetIssue, 999l, 10000l);
        messagedResult = securityLevelSystemField.needsMove(Arrays.asList(mockSourceIssue), mockTargetIssue, mockTargetFieldLayoutItem);
        assertFalse(messagedResult.getResult());
        assertNull(messagedResult.getMessage());
    }

    private MutableIssue createTargetParentIssue(final MutableIssue mockTargetIssue, final Long parentId, final Long securityLevelId) {
        MutableIssue mockTargetParentIssue = MockIssueFactory.createIssue(parentId, "PARENT-1", mockTargetIssue.getProjectId());
        mockTargetParentIssue.setSecurityLevelId(securityLevelId);
        mockTargetIssue.setParentObject(mockTargetParentIssue);

        return mockTargetParentIssue;
    }

    @Test
    public void testNeedsMoveSecuredSubtaskToSecuredSubtaskWithDifferentSecurityLevel() {
        MockIssueSecurityLevelManager mockIssueSecurityLevelManager = new MockIssueSecurityLevelManager();
        MockIssueFactory.setIssueSecurityLevelManager(mockIssueSecurityLevelManager);
        mockIssueSecurityLevelManager.addIssueSecurityLevel(new IssueSecurityLevelImpl(10000l, "Test security level", "Test security level", 1l));
        mockIssueSecurityLevelManager.setUsersSecurityLevelsResult(Arrays.asList(mockIssueSecurityLevelManager.getSecurityLevel(10000L)));
        mockIssueSecurityLevelManager.addIssueSecurityLevel(new IssueSecurityLevelImpl(20000l, "Test security level", "Test security level", 1l));
        mockIssueSecurityLevelManager.setUsersSecurityLevelsResult(Arrays.asList(mockIssueSecurityLevelManager.getSecurityLevel(20000L)));

        MutableIssue mockSourceIssue = MockIssueFactory.createIssue(1, "RANDOM-1", 1);
        mockSourceIssue.setSecurityLevelId(10000l);
        MutableIssue mockSourceParentIssue = createTargetParentIssue(mockSourceIssue, 777l, 10000l);
        MutableIssue mockTargetIssue = MockIssueFactory.createIssue(1, "RANDOM-1", 1);
        MutableIssue mockTargetParentIssue = createTargetParentIssue(mockTargetIssue, 999l, 20000l);

        SecurityLevelSystemField securityLevelSystemField = new SecurityLevelSystemField(null, null, null, new MockAuthenticationContext(null),
                mockIssueSecurityLevelManager, null, null, null);

        // No need to move when security levels are the same
        FieldLayoutItem mockTargetFieldLayoutItem = mock(FieldLayoutItem.class);
        when(mockTargetFieldLayoutItem.isRequired()).thenReturn(false);

        //test same project
        MessagedResult messagedResult = securityLevelSystemField.needsMove(Arrays.asList(mockSourceIssue), mockTargetIssue, mockTargetFieldLayoutItem);
        assertTrue(messagedResult.getResult());
        assertNull(messagedResult.getMessage());

        //test different project
        mockTargetIssue = MockIssueFactory.createIssue(1, "RANDOM-1", 2);
        mockTargetParentIssue = createTargetParentIssue(mockTargetIssue, 999l, 20000l);
        messagedResult = securityLevelSystemField.needsMove(Arrays.asList(mockSourceIssue), mockTargetIssue, mockTargetFieldLayoutItem);
        assertTrue(messagedResult.getResult());
        assertNull(messagedResult.getMessage());
    }

    @Test
    public void testNeedsMoveNotSecuredSubtaskToSecuredSubtask() {
        MockIssueSecurityLevelManager mockIssueSecurityLevelManager = new MockIssueSecurityLevelManager();
        MockIssueFactory.setIssueSecurityLevelManager(mockIssueSecurityLevelManager);
        mockIssueSecurityLevelManager.addIssueSecurityLevel(new IssueSecurityLevelImpl(10000l, "Test security level", "Test security level", 1l));
        mockIssueSecurityLevelManager.setUsersSecurityLevelsResult(Arrays.asList(mockIssueSecurityLevelManager.getSecurityLevel(10000L)));

        MutableIssue mockSourceIssue = MockIssueFactory.createIssue(1, "RANDOM-1", 1);
        MutableIssue mockSourceParentIssue = createTargetParentIssue(mockSourceIssue, 777l, null);
        MutableIssue mockTargetIssue = MockIssueFactory.createIssue(1, "RANDOM-1", 1);
        MutableIssue mockTargetParentIssue = createTargetParentIssue(mockTargetIssue, 999l, 10000l);

        SecurityLevelSystemField securityLevelSystemField = new SecurityLevelSystemField(null, null, null, new MockAuthenticationContext(null),
                mockIssueSecurityLevelManager, null, null, null);

        // No need to move when security levels are the same
        FieldLayoutItem mockTargetFieldLayoutItem = mock(FieldLayoutItem.class);
        when(mockTargetFieldLayoutItem.isRequired()).thenReturn(false);

        //test same project
        MessagedResult messagedResult = securityLevelSystemField.needsMove(Arrays.asList(mockSourceIssue), mockTargetIssue, mockTargetFieldLayoutItem);
        assertTrue(messagedResult.getResult());
        assertNull(messagedResult.getMessage());

        //test different project
        mockTargetIssue = MockIssueFactory.createIssue(1, "RANDOM-1", 2);
        mockTargetParentIssue = createTargetParentIssue(mockTargetIssue, 999l, 10000l);
        messagedResult = securityLevelSystemField.needsMove(Arrays.asList(mockSourceIssue), mockTargetIssue, mockTargetFieldLayoutItem);
        assertTrue(messagedResult.getResult());
        assertNull(messagedResult.getMessage());
    }

    @Test
    public void testNeedsMoveNotSecuredSubtaskToNotSecuredSubtaskWithDifferentParent() {
        MockIssueSecurityLevelManager mockIssueSecurityLevelManager = new MockIssueSecurityLevelManager();
        MockIssueFactory.setIssueSecurityLevelManager(mockIssueSecurityLevelManager);

        MutableIssue mockSourceIssue = MockIssueFactory.createIssue(1, "RANDOM-1", 1);
        MutableIssue mockSourceParentIssue = createTargetParentIssue(mockSourceIssue, 777l, null);
        MutableIssue mockTargetIssue = MockIssueFactory.createIssue(1, "RANDOM-1", 1);
        MutableIssue mockTargetParentIssue = createTargetParentIssue(mockTargetIssue, 999l, null);

        SecurityLevelSystemField securityLevelSystemField = new SecurityLevelSystemField(null, null, null, new MockAuthenticationContext(null),
                mockIssueSecurityLevelManager, null, null, null);

        // No need to move when security levels are the same
        FieldLayoutItem mockTargetFieldLayoutItem = mock(FieldLayoutItem.class);
        when(mockTargetFieldLayoutItem.isRequired()).thenReturn(false);

        //test same project
        MessagedResult messagedResult = securityLevelSystemField.needsMove(Arrays.asList(mockSourceIssue), mockTargetIssue, mockTargetFieldLayoutItem);
        assertFalse(messagedResult.getResult());
        assertNull(messagedResult.getMessage());

        //test different project
        mockTargetIssue = MockIssueFactory.createIssue(1, "RANDOM-1", 2);
        mockTargetParentIssue = createTargetParentIssue(mockTargetIssue, 999l, null);
        messagedResult = securityLevelSystemField.needsMove(Arrays.asList(mockSourceIssue), mockTargetIssue, mockTargetFieldLayoutItem);
        assertFalse(messagedResult.getResult());
        assertNull(messagedResult.getMessage());
    }

    @Test
    public void testNeedsMoveSecuredIssueToSecuredSubtaskWithSameSecurityLevel() {
        MockIssueSecurityLevelManager mockIssueSecurityLevelManager = new MockIssueSecurityLevelManager();
        MockIssueFactory.setIssueSecurityLevelManager(mockIssueSecurityLevelManager);
        mockIssueSecurityLevelManager.addIssueSecurityLevel(new IssueSecurityLevelImpl(10000l, "Test security level", "Test security level", 1l));
        mockIssueSecurityLevelManager.setUsersSecurityLevelsResult(Arrays.asList(mockIssueSecurityLevelManager.getSecurityLevel(10000L)));

        MutableIssue mockSourceIssue = MockIssueFactory.createIssue(1, "RANDOM-1", 1);
        mockSourceIssue.setSecurityLevelId(10000l);
        MutableIssue mockTargetIssue = MockIssueFactory.createIssue(1, "RANDOM-1", 1);
        MutableIssue mockTargetParentIssue = createTargetParentIssue(mockTargetIssue, 999l, 10000l);

        SecurityLevelSystemField securityLevelSystemField = new SecurityLevelSystemField(null, null, null, new MockAuthenticationContext(null),
                mockIssueSecurityLevelManager, null, null, null);

        // No need to move when security levels are the same
        FieldLayoutItem mockTargetFieldLayoutItem = mock(FieldLayoutItem.class);
        when(mockTargetFieldLayoutItem.isRequired()).thenReturn(false);

        //test same project
        MessagedResult messagedResult = securityLevelSystemField.needsMove(Arrays.asList(mockSourceIssue), mockTargetIssue, mockTargetFieldLayoutItem);
        assertFalse(messagedResult.getResult());
        assertNull(messagedResult.getMessage());

        //test different project
        mockTargetIssue = MockIssueFactory.createIssue(1, "RANDOM-1", 2);
        mockTargetParentIssue = createTargetParentIssue(mockTargetIssue, 999l, 10000l);
        messagedResult = securityLevelSystemField.needsMove(Arrays.asList(mockSourceIssue), mockTargetIssue, mockTargetFieldLayoutItem);
        assertFalse(messagedResult.getResult());
        assertNull(messagedResult.getMessage());
    }

    @Test
    public void testNeedsMoveSecuredIssueToSecuredSubtaskWithDifferentSecurityLevel() {
        MockIssueSecurityLevelManager mockIssueSecurityLevelManager = new MockIssueSecurityLevelManager();
        MockIssueFactory.setIssueSecurityLevelManager(mockIssueSecurityLevelManager);
        mockIssueSecurityLevelManager.addIssueSecurityLevel(new IssueSecurityLevelImpl(10000l, "Test security level", "Test security level", 1l));
        mockIssueSecurityLevelManager.setUsersSecurityLevelsResult(Arrays.asList(mockIssueSecurityLevelManager.getSecurityLevel(10000L)));
        mockIssueSecurityLevelManager.addIssueSecurityLevel(new IssueSecurityLevelImpl(20000l, "Test security level", "Test security level", 1l));
        mockIssueSecurityLevelManager.setUsersSecurityLevelsResult(Arrays.asList(mockIssueSecurityLevelManager.getSecurityLevel(20000L)));

        MutableIssue mockSourceIssue = MockIssueFactory.createIssue(1, "RANDOM-1", 1);
        mockSourceIssue.setSecurityLevelId(10000l);
        MutableIssue mockTargetIssue = MockIssueFactory.createIssue(1, "RANDOM-1", 1);
        MutableIssue mockTargetParentIssue = createTargetParentIssue(mockTargetIssue, 999l, 20000l);

        SecurityLevelSystemField securityLevelSystemField = new SecurityLevelSystemField(null, null, null, new MockAuthenticationContext(null),
                mockIssueSecurityLevelManager, null, null, null);

        // No need to move when security levels are the same
        FieldLayoutItem mockTargetFieldLayoutItem = mock(FieldLayoutItem.class);
        when(mockTargetFieldLayoutItem.isRequired()).thenReturn(false);

        //test same project
        MessagedResult messagedResult = securityLevelSystemField.needsMove(Arrays.asList(mockSourceIssue), mockTargetIssue, mockTargetFieldLayoutItem);
        assertTrue(messagedResult.getResult());
        assertNull(messagedResult.getMessage());

        //test different project
        mockTargetIssue = MockIssueFactory.createIssue(1, "RANDOM-1", 2);
        mockTargetParentIssue = createTargetParentIssue(mockTargetIssue, 999l, 20000l);
        messagedResult = securityLevelSystemField.needsMove(Arrays.asList(mockSourceIssue), mockTargetIssue, mockTargetFieldLayoutItem);
        assertTrue(messagedResult.getResult());
        assertNull(messagedResult.getMessage());
    }

    @Test
    public void testNeedsMoveSecuredIssueToNotSecuredSubtask() {
        MockIssueSecurityLevelManager mockIssueSecurityLevelManager = new MockIssueSecurityLevelManager();
        MockIssueFactory.setIssueSecurityLevelManager(mockIssueSecurityLevelManager);
        mockIssueSecurityLevelManager.addIssueSecurityLevel(new IssueSecurityLevelImpl(10000l, "Test security level", "Test security level", 1l));
        mockIssueSecurityLevelManager.setUsersSecurityLevelsResult(Arrays.asList(mockIssueSecurityLevelManager.getSecurityLevel(10000L)));
        mockIssueSecurityLevelManager.addIssueSecurityLevel(new IssueSecurityLevelImpl(20000l, "Test security level", "Test security level", 1l));
        mockIssueSecurityLevelManager.setUsersSecurityLevelsResult(Arrays.asList(mockIssueSecurityLevelManager.getSecurityLevel(20000L)));

        MutableIssue mockSourceIssue = MockIssueFactory.createIssue(1, "RANDOM-1", 1);
        mockSourceIssue.setSecurityLevelId(10000l);
        MutableIssue mockTargetIssue = MockIssueFactory.createIssue(1, "RANDOM-1", 1);
        MutableIssue mockTargetParentIssue = createTargetParentIssue(mockTargetIssue, 999l, null);

        SecurityLevelSystemField securityLevelSystemField = new SecurityLevelSystemField(null, null, null, new MockAuthenticationContext(null),
                mockIssueSecurityLevelManager, null, null, null);

        // No need to move when security levels are the same
        FieldLayoutItem mockTargetFieldLayoutItem = mock(FieldLayoutItem.class);
        when(mockTargetFieldLayoutItem.isRequired()).thenReturn(false);

        //test same project
        MessagedResult messagedResult = securityLevelSystemField.needsMove(Arrays.asList(mockSourceIssue), mockTargetIssue, mockTargetFieldLayoutItem);
        assertTrue(messagedResult.getResult());
        assertNull(messagedResult.getMessage());

        //test different project
        mockTargetIssue = MockIssueFactory.createIssue(1, "RANDOM-1", 2);
        mockTargetParentIssue = createTargetParentIssue(mockTargetIssue, 999l, null);
        messagedResult = securityLevelSystemField.needsMove(Arrays.asList(mockSourceIssue), mockTargetIssue, mockTargetFieldLayoutItem);
        assertTrue(messagedResult.getResult());
        assertNull(messagedResult.getMessage());
    }

    @Test
    public void testNeedsMoveNotSecuredIssueToSecuredSubtask() {
        MockIssueSecurityLevelManager mockIssueSecurityLevelManager = new MockIssueSecurityLevelManager();
        MockIssueFactory.setIssueSecurityLevelManager(mockIssueSecurityLevelManager);
        mockIssueSecurityLevelManager.addIssueSecurityLevel(new IssueSecurityLevelImpl(10000l, "Test security level", "Test security level", 1l));
        mockIssueSecurityLevelManager.setUsersSecurityLevelsResult(Arrays.asList(mockIssueSecurityLevelManager.getSecurityLevel(10000L)));
        mockIssueSecurityLevelManager.addIssueSecurityLevel(new IssueSecurityLevelImpl(20000l, "Test security level", "Test security level", 1l));
        mockIssueSecurityLevelManager.setUsersSecurityLevelsResult(Arrays.asList(mockIssueSecurityLevelManager.getSecurityLevel(20000L)));

        MutableIssue mockSourceIssue = MockIssueFactory.createIssue(1, "RANDOM-1", 1);
        MutableIssue mockTargetIssue = MockIssueFactory.createIssue(1, "RANDOM-1", 1);
        MutableIssue mockTargetParentIssue = createTargetParentIssue(mockTargetIssue, 999l, 20000l);

        SecurityLevelSystemField securityLevelSystemField = new SecurityLevelSystemField(null, null, null, new MockAuthenticationContext(null),
                mockIssueSecurityLevelManager, null, null, null);

        // No need to move when security levels are the same
        FieldLayoutItem mockTargetFieldLayoutItem = mock(FieldLayoutItem.class);
        when(mockTargetFieldLayoutItem.isRequired()).thenReturn(false);

        //test same project
        MessagedResult messagedResult = securityLevelSystemField.needsMove(Arrays.asList(mockSourceIssue), mockTargetIssue, mockTargetFieldLayoutItem);
        assertTrue(messagedResult.getResult());
        assertNull(messagedResult.getMessage());

        //test different project
        mockTargetIssue = MockIssueFactory.createIssue(1, "RANDOM-1", 2);
        mockTargetParentIssue = createTargetParentIssue(mockTargetIssue, 999l, 20000l);
        messagedResult = securityLevelSystemField.needsMove(Arrays.asList(mockSourceIssue), mockTargetIssue, mockTargetFieldLayoutItem);
        assertTrue(messagedResult.getResult());
        assertNull(messagedResult.getMessage());
    }

    @Test
    public void testNeedsMoveNotSecuredIssueToNotSecuredSubtask() {
        MockIssueSecurityLevelManager mockIssueSecurityLevelManager = new MockIssueSecurityLevelManager();
        MockIssueFactory.setIssueSecurityLevelManager(mockIssueSecurityLevelManager);

        MutableIssue mockSourceIssue = MockIssueFactory.createIssue(1, "RANDOM-1", 1);
        MutableIssue mockTargetIssue = MockIssueFactory.createIssue(1, "RANDOM-1", 1);
        MutableIssue mockTargetParentIssue = createTargetParentIssue(mockTargetIssue, 999l, null);

        SecurityLevelSystemField securityLevelSystemField = new SecurityLevelSystemField(null, null, null, new MockAuthenticationContext(null),
                mockIssueSecurityLevelManager, null, null, null);

        // No need to move when security levels are the same
        FieldLayoutItem mockTargetFieldLayoutItem = mock(FieldLayoutItem.class);
        when(mockTargetFieldLayoutItem.isRequired()).thenReturn(false);

        //test same project
        MessagedResult messagedResult = securityLevelSystemField.needsMove(Arrays.asList(mockSourceIssue), mockTargetIssue, mockTargetFieldLayoutItem);
        assertFalse(messagedResult.getResult());
        assertNull(messagedResult.getMessage());

        //test different project
        mockTargetIssue = MockIssueFactory.createIssue(1, "RANDOM-1", 2);
        mockTargetParentIssue = createTargetParentIssue(mockTargetIssue, 999l, null);
        messagedResult = securityLevelSystemField.needsMove(Arrays.asList(mockSourceIssue), mockTargetIssue, mockTargetFieldLayoutItem);
        assertFalse(messagedResult.getResult());
        assertNull(messagedResult.getMessage());
    }

    @Test
    public void testNeedsMoveWithSecurityLevelOnOriginalIssue() throws Exception {
        MockIssueSecurityLevelManager mockIssueSecurityLevelManager = new MockIssueSecurityLevelManager();
        mockIssueSecurityLevelManager.addIssueSecurityLevel(new IssueSecurityLevelImpl(10001L, "Blue", null, null));

        MockIssueFactory.setIssueSecurityLevelManager(mockIssueSecurityLevelManager);
        MutableIssue mockSourceIssue = MockIssueFactory.createIssue(1, "RANDOM-1", 2);
        mockSourceIssue.setSecurityLevelId(10001L);
        MutableIssue mockTargetIssue = MockIssueFactory.createIssue(1, "RANDOM-1", 1);

        SecurityLevelSystemField securityLevelSystemField = new SecurityLevelSystemField(null, null, null, new MockAuthenticationContext(null),
                mockIssueSecurityLevelManager, null, null, null);

        // Needs move because user does not have this security level
        MessagedResult messagedResult = securityLevelSystemField.needsMove(Arrays.asList(mockSourceIssue), mockTargetIssue, null);
        assertTrue(messagedResult.getResult());
        assertNull(messagedResult.getMessage());
    }

    @Test
    public void testDoesNotNeedMoveWithSecurityLevelOnOriginalIssue() throws Exception {
        MockIssueSecurityLevelManager mockIssueSecurityLevelManager = new MockIssueSecurityLevelManager();
        mockIssueSecurityLevelManager.addIssueSecurityLevel(new IssueSecurityLevelImpl(10001L, "Blue", null, null));

        MockIssueFactory.setIssueSecurityLevelManager(mockIssueSecurityLevelManager);
        MutableIssue mockSourceIssue = MockIssueFactory.createIssue(1, "RANDOM-1", 2);
        mockSourceIssue.setSecurityLevelId(10001L);
        MutableIssue mockTargetIssue = MockIssueFactory.createIssue(1, "RANDOM-1", 1);

        // Let the user have access to current security level in new project
        mockIssueSecurityLevelManager.setUsersSecurityLevelsResult(Arrays.asList(mockIssueSecurityLevelManager.getSecurityLevel(10001L)));

        SecurityLevelSystemField securityLevelSystemField = new SecurityLevelSystemField(null, null, null, new MockAuthenticationContext(null),
                mockIssueSecurityLevelManager, null, null, null);

        // Needs move because user does not have this security level
        MessagedResult messagedResult = securityLevelSystemField.needsMove(Arrays.asList(mockSourceIssue), mockTargetIssue, null);
        assertFalse(messagedResult.getResult());
        assertNull(messagedResult.getMessage());
    }

    @Test
    public void testNeedsMoveIssueToSubtaskWithSecurityLevelOnDestinationParentIssue() throws Exception {
        MockIssueSecurityLevelManager mockIssueSecurityLevelManager = new MockIssueSecurityLevelManager();
        mockIssueSecurityLevelManager.addIssueSecurityLevel(new IssueSecurityLevelImpl(10001L, "Blue", null, null));

        MockIssueFactory.setIssueSecurityLevelManager(mockIssueSecurityLevelManager);
        MutableIssue mockSourceIssue = MockIssueFactory.createIssue(1, "RANDOM-1", 1);
        MutableIssue mockTargetIssue = MockIssueFactory.createIssue(1, "RANDOM-1", 1);
        // set parent with security level on target issue
        MutableIssue mockTargetIssueParent = MockIssueFactory.createIssue(1, "PARENT-1", 1);
        mockTargetIssueParent.setSecurityLevelId(10001L);
        mockTargetIssue.setParentObject(mockTargetIssueParent);

        SecurityLevelSystemField securityLevelSystemField = new SecurityLevelSystemField(null, null, null, new MockAuthenticationContext(null),
                mockIssueSecurityLevelManager, null, null, null);

        // Needs move because security level needs to be copied from new parent issue
        FieldLayoutItem mockTargetFieldLayoutItem = mock(FieldLayoutItem.class);
        when(mockTargetFieldLayoutItem.isRequired()).thenReturn(false);

        MessagedResult messagedResult = securityLevelSystemField.needsMove(Arrays.asList(mockSourceIssue), mockTargetIssue, mockTargetFieldLayoutItem);
        assertTrue(messagedResult.getResult());
        assertNull(messagedResult.getMessage());
    }

    @Test
    public void testShowOnlyMessageWhenDoingBulkMoveAndIssueToSubtaskConversion() {
        VelocityTemplatingEngine velocityTemplatingEngineMock = mock(VelocityTemplatingEngine.class);
        JiraAuthenticationContext jiraAuthenticationContext = mock(JiraAuthenticationContext.class);
        I18nHelper i18nHelper = mock(I18nHelper.class);
        when(jiraAuthenticationContext.getI18nHelper()).thenReturn(i18nHelper);
        when(i18nHelper.getLocale()).thenReturn(new Locale("en", "US"));
        SecurityLevelSystemField securityLevelSystemField = new SecurityLevelSystemField(velocityTemplatingEngineMock, null, null, jiraAuthenticationContext,
                null, null, null, null);
        BulkEditBean bulkEditBean = mock(BulkEditBean.class);
        when(bulkEditBean.getParentIssueObject()).thenReturn(new MockIssue(1l));
        when(bulkEditBean.getTargetIssueType()).thenReturn(new MockIssueType("1", "Sub-task", true));
        VelocityTemplatingEngine.RenderRequest renderRequestMock = mock(VelocityTemplatingEngine.RenderRequest.class);
        when(velocityTemplatingEngineMock.render(any(TemplateSource.class))).thenReturn(renderRequestMock);
        when(renderRequestMock.applying(anyMap())).thenReturn(mock(VelocityTemplatingEngine.RenderRequest.class));

        securityLevelSystemField.getBulkEditHtml(null, null, bulkEditBean, new HashMap<>());

        ArgumentCaptor<TemplateSource> templateSourceArgumentCaptor = ArgumentCaptor.forClass(TemplateSource.class);
        ArgumentCaptor<Map<String, Object>> mapArgumentCaptor = ArgumentCaptor.forClass((Class<Map<String, Object>>) (Class) Map.class);
        verify(velocityTemplatingEngineMock).render(templateSourceArgumentCaptor.capture());
        verify(renderRequestMock).applying(mapArgumentCaptor.capture());
        TemplateSource.File templateSource = (TemplateSource.File) templateSourceArgumentCaptor.getValue();
        final String expectedTemplate = "securitylevel-subtask.vm";
        Map<String, Object> mapPassedToVelocity = mapArgumentCaptor.getValue();

        assertThat("Should render template for subtask (message only)", templateSource.getPath(), Matchers.endsWith(expectedTemplate));
        assertThat("Velocity template should have info parameter passed", mapPassedToVelocity, IsMapContaining.hasEntry("infoMessageKey", "bulk.edit.security.level.subtask.message"));
    }

    @Test
    public void testDontShowOnlyMessageWhenDoingBulkMoveAndSubtaskToIssueConversion() {
        MockIssueSecurityLevelManager mockIssueSecurityLevelManager = new MockIssueSecurityLevelManager();
        VelocityTemplatingEngine velocityTemplatingEngineMock = mock(VelocityTemplatingEngine.class);
        JiraAuthenticationContext jiraAuthenticationContext = mock(JiraAuthenticationContext.class);
        I18nHelper i18nHelper = mock(I18nHelper.class);
        when(jiraAuthenticationContext.getI18nHelper()).thenReturn(i18nHelper);
        when(i18nHelper.getLocale()).thenReturn(new Locale("en", "US"));
        SecurityLevelSystemField securityLevelSystemField = new SecurityLevelSystemField(velocityTemplatingEngineMock, null, null, jiraAuthenticationContext,
                mockIssueSecurityLevelManager, null, null, null);
        BulkEditBean bulkEditBean = mock(BulkEditBean.class);
        when(bulkEditBean.isSubTaskOnly()).thenReturn(true);
        when(bulkEditBean.getTargetIssueType()).thenReturn(new MockIssueType("1", "Issue", false));
        Project targetProject = new MockProject();
        when(bulkEditBean.getTargetProject()).thenReturn(targetProject);
        when(bulkEditBean.getOperationName()).thenReturn("BulkMove");
        VelocityTemplatingEngine.RenderRequest renderRequestMock = mock(VelocityTemplatingEngine.RenderRequest.class);
        when(velocityTemplatingEngineMock.render(any(TemplateSource.class))).thenReturn(renderRequestMock);
        when(renderRequestMock.applying(anyMap())).thenReturn(mock(VelocityTemplatingEngine.RenderRequest.class));
        OperationContext context = mock(OperationContext.class);
        Map<String, Object> contextMap = new HashMap<>();
        contextMap.put(securityLevelSystemField.getId(), null);
        when(context.getFieldValuesHolder()).thenReturn(contextMap);

        securityLevelSystemField.getBulkEditHtml(context, null, bulkEditBean, new HashMap<>());

        ArgumentCaptor<TemplateSource> templateSourceArgumentCaptor = ArgumentCaptor.forClass(TemplateSource.class);
        ArgumentCaptor<Map<String, Object>> mapArgumentCaptor = ArgumentCaptor.forClass((Class<Map<String, Object>>) (Class) Map.class);
        verify(velocityTemplatingEngineMock).render(templateSourceArgumentCaptor.capture());
        verify(renderRequestMock).applying(mapArgumentCaptor.capture());
        TemplateSource.File templateSource = (TemplateSource.File) templateSourceArgumentCaptor.getValue();
        final String expectedTemplate = "securitylevel-edit.vm";
        Map<String, Object> mapPassedToVelocity = mapArgumentCaptor.getValue();

        assertThat("Should render template for issue (with select)", templateSource.getPath(), Matchers.endsWith(expectedTemplate));
        assertThat("Velocity template should have info parameter passed", mapPassedToVelocity, IsMapContaining.hasKey("securityLevels"));
    }
}