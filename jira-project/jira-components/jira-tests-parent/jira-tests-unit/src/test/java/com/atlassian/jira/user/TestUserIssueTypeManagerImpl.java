package com.atlassian.jira.user;


import com.atlassian.core.AtlassianCoreException;
import com.atlassian.jira.issue.issuetype.IssueTypeId;
import com.atlassian.jira.user.preferences.ExtendedPreferences;
import com.atlassian.jira.user.preferences.UserPreferencesManager;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Optional;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

public class TestUserIssueTypeManagerImpl {
    private static final ApplicationUser ANONYMOUS_USER = null;
    private static final ApplicationUser FRED = new MockApplicationUser("fred");

    @Rule
    public final MockitoRule initMockito = MockitoJUnit.rule();

    @Mock
    private UserPreferencesManager userPreferencesManager;
    @Mock
    private ExtendedPreferences extendedPreferences;

    private UserIssueTypeManager userIssueTypeManager;

    @Before
    public void setUp() throws Exception {
        when(userPreferencesManager.getExtendedPreferences(Mockito.any())).thenReturn(extendedPreferences);

        userIssueTypeManager = new UserIssueTypeManagerImpl(userPreferencesManager);
    }

    @Test
    public void shouldIgnoreAnonymousUsersWhenStoringIssueType() {
        userIssueTypeManager.setLastUsedIssueTypeId(ANONYMOUS_USER, new IssueTypeId("10000"));

        verifyZeroInteractions(userPreferencesManager);
    }

    @Test
    public void shouldIgnoreAnonymousUsersWhenStoringSubtaskIssueType() {
        userIssueTypeManager.setLastUsedSubtaskIssueTypeId(ANONYMOUS_USER, new IssueTypeId("10000"));

        verifyZeroInteractions(userPreferencesManager);
    }

    @Test
    public void shouldStoreIssueType() throws AtlassianCoreException {
        userIssueTypeManager.setLastUsedIssueTypeId(FRED, new IssueTypeId("10002"));

        verify(extendedPreferences).setString("user.last.issue.type.id", "10002");
    }

    @Test
    public void shouldStoreSubtaskIssueType() throws AtlassianCoreException {
        userIssueTypeManager.setLastUsedSubtaskIssueTypeId(FRED, new IssueTypeId("10002"));

        verify(extendedPreferences).setString("user.last.subtask.issue.type.id", "10002");
    }

    @Test
    public void returnsEmptyForAnonymousUser() {
        final Optional<IssueTypeId> issueTypeId = userIssueTypeManager.getLastUsedIssueTypeId(ANONYMOUS_USER);
        final Optional<IssueTypeId> subtaskIssueTypeId = userIssueTypeManager.getLastUsedSubtaskIssueTypeId(ANONYMOUS_USER);

        assertThat(issueTypeId.isPresent(), is(false));
        assertThat(subtaskIssueTypeId.isPresent(), is(false));
        verifyZeroInteractions(userPreferencesManager);
    }

    @Test
    public void returnsLastStoredIssueType() {
        when(extendedPreferences.getString("user.last.issue.type.id")).thenReturn("99213");

        final Optional<IssueTypeId> issueTypeId = userIssueTypeManager.getLastUsedIssueTypeId(FRED);

        assertThat(issueTypeId.get(), is(new IssueTypeId("99213")));
    }

    @Test
    public void returnsLastStoredSubtaskIssueType() {
        when(extendedPreferences.getString("user.last.subtask.issue.type.id")).thenReturn("11112");

        final Optional<IssueTypeId> issueTypeId = userIssueTypeManager.getLastUsedSubtaskIssueTypeId(FRED);

        assertThat(issueTypeId.get(), is(new IssueTypeId("11112")));
    }
}