package com.atlassian.jira.bulkedit;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.fields.LongIdsValueHolder;
import com.atlassian.jira.issue.fields.OrderableField;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.Collection;
import java.util.Map;

import static org.mockito.Mockito.when;

abstract public class BulkEditTestHelper extends TestWatcher {

    @Mock
    private OrderableField field;
    @Mock
    private Issue issue;

    private Map<String, Object> fieldValuesHolder;

    private BulkEditMultiSelectFieldOption bulkEditMultiSelectFieldOption;

    @Override
    protected void starting(final Description description) {
        MockitoAnnotations.initMocks(this);
        fieldValuesHolder = Maps.newHashMap();
        bulkEditMultiSelectFieldOption = instantiateFieldOption();
    }


    public void setValuesFromBulkEdit(final String fieldId, final Object fieldValue) {
        when(field.getId()).thenReturn(fieldId);
        fieldValuesHolder.put(fieldId, fieldValue);
    }

    @SuppressWarnings("unchecked")
    public void mockFieldValuesFromIssue(String fieldId, Object fieldValue) {
        final Map<String, Object> issueFieldValuesHolder = ImmutableMap.of(fieldId, fieldValue);
        Mockito.doAnswer(invocationOnMock -> {
            Map<String, Object> valuesHolder = (Map<String, Object>) invocationOnMock.getArguments()[0];
            valuesHolder.putAll(issueFieldValuesHolder);
            return null;
        }).when(field).populateFromIssue(Mockito.anyMapOf(String.class, Object.class), Mockito.eq(issue));

    }

    abstract public BulkEditMultiSelectFieldOption instantiateFieldOption();


    public String getValuesToAdd() {
        return bulkEditMultiSelectFieldOption.getFieldValuesToAdd(field, fieldValuesHolder);
    }

    public boolean validate() {
        return bulkEditMultiSelectFieldOption.validateOperation(field, fieldValuesHolder);
    }


    public Collection<?> getCollectionFromValuesMap(String fieldId) {
        Map<String, Object> valuesMap = bulkEditMultiSelectFieldOption.getFieldValuesMap(issue, field, fieldValuesHolder);
        return (Collection<?>) valuesMap.get(fieldId);
    }

    public LongIdsValueHolder getFieldLongIdsFromValuesMap(String fieldId) {
        Map<String, Object> valuesMap = bulkEditMultiSelectFieldOption.getFieldValuesMap(issue, field, fieldValuesHolder);
        return (LongIdsValueHolder) valuesMap.get(fieldId);
    }
}
