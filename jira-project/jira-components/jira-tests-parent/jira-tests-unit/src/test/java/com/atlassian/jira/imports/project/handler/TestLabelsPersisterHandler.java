package com.atlassian.jira.imports.project.handler;

import com.atlassian.jira.exception.ParseException;
import com.atlassian.jira.external.beans.ExternalLabel;
import com.atlassian.jira.imports.project.ProjectImportPersister;
import com.atlassian.jira.imports.project.core.EntityRepresentationImpl;
import com.atlassian.jira.imports.project.core.ProjectImportResults;
import com.atlassian.jira.imports.project.mapper.ProjectImportMapper;
import com.atlassian.jira.imports.project.parser.LabelParser;
import com.atlassian.jira.imports.project.transformer.LabelTransformer;
import com.atlassian.jira.util.collect.MapBuilder;
import com.google.common.collect.ImmutableMap;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import java.util.Map;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

public class TestLabelsPersisterHandler {
    @Rule
    public final MethodRule initMockito = MockitoJUnit.rule();

    @Mock
    private ProjectImportPersister mockImportPersister;
    @Mock
    private ProjectImportMapper mockProjectImportMapper;
    @Mock
    private LabelParser mockLabelParser;
    @Mock
    private LabelTransformer mockLabelTransformer;
    @Mock
    private ProjectImportResults mockProjectImportResults;

    @Test
    public void testHandleNothing() throws ParseException, AbortImportException {
        LabelsPersisterHandler handler = new LabelsPersisterHandler(mockImportPersister, null, null, null, null);
        handler.handleEntity("Dude", MapBuilder.newBuilder("sweet", "whatsminesay").toMap());

        verifyZeroInteractions(mockImportPersister);
    }

    @Test
    public void testHandle() throws ParseException, AbortImportException {
        Map<String, String> badAttributes = ImmutableMap.of(
                "id", "10000",
                "issue", "10001",
                "fieldid", "12000",
                "label", "alabel"
        );
        Map<String, String> goodAttributes = ImmutableMap.of(
                "id", "10003",
                "issue", "10004",
                "label", "alabel"
        );

        ExternalLabel badLabel = new ExternalLabel();
        badLabel.setId("10000");
        badLabel.setIssueId("10001");
        badLabel.setCustomFieldId("12000");
        badLabel.setLabel("alabel");
        ExternalLabel goodLabel = new ExternalLabel();
        goodLabel.setId("10003");
        goodLabel.setIssueId("10004");
        goodLabel.setLabel("alabel");
        when(mockLabelParser.parse(badAttributes)).thenReturn(badLabel);
        when(mockLabelParser.parse(goodAttributes)).thenReturn(goodLabel);

        ExternalLabel badTransformedLabel = new ExternalLabel();
        badTransformedLabel.setIssueId("10001");
        badTransformedLabel.setCustomFieldId("12000");
        badTransformedLabel.setLabel("alabel");
        ExternalLabel goodTransformedLabel = new ExternalLabel();
        goodTransformedLabel.setIssueId("10004");
        goodTransformedLabel.setLabel("alabel");
        when(mockLabelTransformer.transform(mockProjectImportMapper, badLabel)).thenReturn(badTransformedLabel);
        when(mockLabelTransformer.transform(mockProjectImportMapper, goodLabel)).thenReturn(goodTransformedLabel);

        final Map<String, String> transformedAttributes = ImmutableMap.of("issue", "10004", "label", "alabel");
        final EntityRepresentationImpl representation = new EntityRepresentationImpl("Label", transformedAttributes);
        when(mockLabelParser.getEntityRepresentation(goodTransformedLabel)).thenReturn(representation);
        when(mockLabelParser.getEntityRepresentation(badTransformedLabel)).thenThrow(new IllegalArgumentException("Should not be doing this because of the custom field!"));

        when(mockProjectImportResults.abortImport()).thenReturn(false);

        LabelsPersisterHandler handler = new LabelsPersisterHandler(mockImportPersister, mockProjectImportMapper, mockProjectImportResults, null, new ExecutorForTests()) {
            @Override
            LabelParser getLabelParser() {
                return mockLabelParser;
            }

            @Override
            LabelTransformer getLabelTransformer() {
                return mockLabelTransformer;
            }
        };

        // should get persisted
        handler.handleEntity("Label", goodAttributes);
        verify(mockImportPersister).createEntity(representation);

        //shouldn't get persisted due to custom field
        handler.handleEntity("Label", badAttributes);
        verifyNoMoreInteractions(mockImportPersister);
    }
}
