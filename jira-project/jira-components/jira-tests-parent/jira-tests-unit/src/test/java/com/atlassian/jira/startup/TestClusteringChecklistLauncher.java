package com.atlassian.jira.startup;

import com.atlassian.jira.appconsistency.clustering.ClusterLicenseStartupCheck;
import com.atlassian.jira.cluster.ClusterManager;
import com.atlassian.jira.cluster.ClusterNodeProperties;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.config.util.JiraHome;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.mock.i18n.MockI18nHelper;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.johnson.JohnsonProvider;
import com.atlassian.johnson.Johnson;
import com.atlassian.johnson.JohnsonEventContainer;
import com.atlassian.johnson.event.Event;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.ServletContext;
import java.io.File;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @since v6.1
 */
@RunWith(MockitoJUnitRunner.class)
public class TestClusteringChecklistLauncher {
    @Rule
    public final RuleChain mocksInContainer = MockitoMocksInContainer.forTest(this);

    @Mock
    @AvailableInContainer
    private ClusterManager mockClusterManager;
    @Mock
    @AvailableInContainer
    private com.atlassian.jira.license.ClusterLicenseCheck clusterLicenseCheck;
    @Mock
    @AvailableInContainer(interfaceClass = ClusterLicenseStartupCheck.class)
    private ClusterLicenseStartupCheck clusterLicenseStartupCheck;
    @Mock
    @AvailableInContainer
    private ClusterNodeProperties mockClusterNodeProperties;
    @Mock
    @AvailableInContainer
    private I18nHelper.BeanFactory mockI18nHelperBeanFactory;
    @Mock
    @AvailableInContainer
    private JiraHome mockJiraHome;
    @Mock
    @AvailableInContainer
    private ApplicationProperties mockApplicationProperties;
    @Mock
    private ServletContext servletContext;
    @Mock
    private JohnsonEventContainer mockJohnsonEventContainer;
    @Mock
    private JohnsonProvider johnsonProvider;
    private MockI18nHelper i18nHelper = new MockI18nHelper();

    @Before
    public void setup() {
        when(servletContext.getAttribute(JohnsonEventContainer.class.getName())).thenReturn(mockJohnsonEventContainer);
        when(mockI18nHelperBeanFactory.getInstance(any(ApplicationUser.class))).thenReturn(i18nHelper);
        when(mockApplicationProperties.getString(APKeys.JIRA_SETUP)).thenReturn("true");
        Johnson.initialize("test-johnson-config.xml");
        when(johnsonProvider.getContainer()).thenReturn(mockJohnsonEventContainer);
    }

    @After
    public void after() {
        Johnson.terminate();
    }

    @Test
    public void failureCausesMultipleJohnsonEvent() {
        when(mockClusterManager.isClustered()).thenReturn(true);
        when(mockClusterNodeProperties.propertyFileExists()).thenReturn(true);

        final ClusteringChecklistLauncher checklistLauncher = new ClusteringChecklistLauncher(johnsonProvider);
        checklistLauncher.start();

        verify(mockJohnsonEventContainer, times(3)).addEvent(any(Event.class));
    }

    @Test
    public void singleFailureCausesSingleJohnsonEvent() {
        when(mockClusterManager.isClustered()).thenReturn(true);
        when(clusterLicenseStartupCheck.isOk()).thenReturn(true);
        when(mockClusterNodeProperties.propertyFileExists()).thenReturn(true);
        when(mockClusterNodeProperties.getNodeId()).thenReturn("a");

        final ClusteringChecklistLauncher checklistLauncher = new ClusteringChecklistLauncher(johnsonProvider);
        checklistLauncher.start();

        verify(mockJohnsonEventContainer).addEvent(any(Event.class));
    }

    @Test
    public void successDoesNotRaiseJohnsonEvent() {
        when(mockClusterManager.isClustered()).thenReturn(true);
        when(clusterLicenseStartupCheck.isOk()).thenReturn(true);
        when(mockClusterNodeProperties.propertyFileExists()).thenReturn(true);
        when(mockClusterNodeProperties.getNodeId()).thenReturn("a");
        when(mockClusterNodeProperties.getSharedHome()).thenReturn("b");
        when(mockJiraHome.getHome()).thenReturn(new File("b"));
        when(mockJiraHome.getLocalHome()).thenReturn(new File("c"));

        final ClusteringChecklistLauncher checklistLauncher = new ClusteringChecklistLauncher(johnsonProvider);
        checklistLauncher.start();

        verify(mockJohnsonEventContainer, never()).addEvent(any(Event.class));
    }
}
