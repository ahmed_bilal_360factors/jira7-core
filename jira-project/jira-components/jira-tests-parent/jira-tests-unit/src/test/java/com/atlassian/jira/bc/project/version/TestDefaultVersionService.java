package com.atlassian.jira.bc.project.version;

import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.bc.MockJiraServiceContext;
import com.atlassian.jira.bc.ServiceOutcome;
import com.atlassian.jira.bc.ServiceResult;
import com.atlassian.jira.bc.project.version.DefaultVersionService.ValidateResult;
import com.atlassian.jira.bc.project.version.VersionService.ArchiveVersionValidationResult;
import com.atlassian.jira.bc.project.version.VersionService.CreateVersionValidationResult;
import com.atlassian.jira.bc.project.version.VersionService.VersionBuilderValidationResult;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.search.SearchProvider;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.matchers.ErrorCollectionMatcher;
import com.atlassian.jira.matchers.FeatureMatchers;
import com.atlassian.jira.mock.MockPermissionManager;
import com.atlassian.jira.mock.project.MockVersion;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.project.version.VersionImpl;
import com.atlassian.jira.project.version.VersionManager;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.security.plugin.ProjectPermissionOverrideModuleDescriptor;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.util.DateFieldFormat;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.ErrorCollection.Reason;
import com.atlassian.jira.util.NoopI18nHelper;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.jira.util.collect.MapBuilder;
import com.atlassian.jira.web.bean.I18nBean;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.event.PluginEventManager;
import com.google.common.collect.Lists;
import org.hamcrest.Matchers;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;
import org.ofbiz.core.entity.GenericEntityException;

import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.EnumSet;
import java.util.Locale;
import java.util.Map;

import static com.atlassian.jira.bc.project.version.VersionService.CreateVersionValidationResult.Reason.BAD_NAME;
import static com.atlassian.jira.bc.project.version.VersionService.CreateVersionValidationResult.Reason.BAD_PROJECT;
import static com.atlassian.jira.bc.project.version.VersionService.CreateVersionValidationResult.Reason.BAD_RELEASE_DATE;
import static com.atlassian.jira.bc.project.version.VersionService.CreateVersionValidationResult.Reason.BAD_START_DATE;
import static com.atlassian.jira.bc.project.version.VersionService.CreateVersionValidationResult.Reason.DUPLICATE_NAME;
import static com.atlassian.jira.bc.project.version.VersionService.CreateVersionValidationResult.Reason.FORBIDDEN;
import static com.atlassian.jira.bc.project.version.VersionService.CreateVersionValidationResult.Reason.VERSION_NAME_TOO_LONG;
import static com.atlassian.jira.permission.ProjectPermissions.ADMINISTER_PROJECTS;
import static com.atlassian.jira.permission.ProjectPermissions.BROWSE_PROJECTS;
import static com.google.common.collect.Lists.newArrayList;
import static java.util.Collections.emptyList;
import static java.util.Collections.emptySet;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Tests the DefaultVersionService class.
 *
 * @since v3.13
 */
@SuppressWarnings("deprecation")
public class TestDefaultVersionService {
    private static final String versionName = "version name";
    private static final Long versionId = 10000L;
    private static final Long nonExistsVersionId = 9999L;
    private static final Long swapVersionId = 10001L;
    private static final Long swapVersionBadProjectId = 10002L;

    private static final Long goodProjectId = 20000L;
    private static final Long badProjectId = 20001L;

    private static final Long projectId = 20000L;
    private static final Project project = new MockProject(projectId);
    private static final Project projectWithNullId = new MockProject((Long) null);

    private static final String startDate = "21/10/21";
    private static final String releaseDate = "27/10/21";
    private static final Date startDateParsed = new Date();
    private static final Date releaseDateParsed = new Date();

    @Rule
    public MockitoContainer initMockitoMocks = MockitoMocksInContainer.rule(this);
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Mock
    private I18nBean.BeanFactory nopI18nFactory;

    @AvailableInContainer
    @Mock
    private PluginEventManager pluginEventManager;
    @AvailableInContainer
    @Mock
    private PluginAccessor pluginAccessor;

    @Mock
    private VersionManager versionManager;
    @Mock
    private PermissionManager permissionManager;
    @Mock
    private IssueManager issueManager;
    @Mock
    private SearchProvider searchProvider;
    @Mock
    private DateFieldFormat dateFieldFormat;

    @AvailableInContainer
    @Mock
    private ProjectManager projectManager;

    private DefaultVersionService service;
    private JiraServiceContext context;
    private ApplicationUser user;

    @Before
    public void setUp() throws Exception {
        NoopI18nHelper i18nHelper = new NoopI18nHelper();
        when(nopI18nFactory.getInstance(any(ApplicationUser.class))).thenReturn(i18nHelper);
        when(nopI18nFactory.getInstance(any(Locale.class))).thenReturn(i18nHelper);

        when(pluginAccessor.getEnabledModuleDescriptorsByClass(eq(ProjectPermissionOverrideModuleDescriptor.class)))
                .thenReturn(Lists.<ProjectPermissionOverrideModuleDescriptor>newArrayList());

        when(projectManager.getProjectObj(projectId)).thenReturn(project);
        when(versionManager.getVersions(projectId)).thenReturn(emptyList());

        when(dateFieldFormat.getFormatHint()).thenReturn("dd-MMMMMMM-y");

        context = new MockJiraServiceContext();

        user = new MockApplicationUser("admin");
        when(permissionManager.hasPermission(Permissions.ADMINISTER, user)).thenReturn(true);

        service = new DefaultVersionService(versionManager, permissionManager, issueManager, searchProvider, nopI18nFactory, dateFieldFormat, projectManager);
    }

    @Test
    public void versionDeletionShouldFailWhenPassedArgumentsAreInvalid() {
        expectedException.expect(IllegalArgumentException.class);
        assertHasErrorsAndFlush(context, service.validateDelete(context, versionId, null, null));
    }

    @Test
    public void shouldFailWhenVersionToDeleteIsInvalid() {
        assertHasErrorsAndFlush(context, service.validateDelete(context, null, VersionService.REMOVE, VersionService.REMOVE));
        assertHasErrorsAndFlush(context, service.validateDelete(context, versionId, VersionService.REMOVE, VersionService.REMOVE));
    }

    /**
     * When swapping in a version for the one that is being deleted, the new version cannot be the same as the one which
     * is being deleted. It must also exist.
     */
    @Test
    public void shouldFailWhenUserHasNoPermissionToProject() {
        MockPermissionManager permissionManager = MyPermissionManager.createPermissionManager(false);

        Version version = mockVersionWithProjectId(goodProjectId);
        Version swapVersionBadProject = mockVersionWithProjectId(badProjectId);
        Version swapVersion = mockVersionWithProjectId(goodProjectId);
        versionManagerWithVersions(MapBuilder.build(versionId, version,
                swapVersionId, swapVersion,
                swapVersionBadProjectId, swapVersionBadProject,
                nonExistsVersionId, null));

        DefaultVersionService defaultVersionService = new DefaultVersionService(versionManager, permissionManager, null, null, null, dateFieldFormat, null);
        // if user doesn't have admin permission then fail
        assertHasErrorsAndFlush(context, defaultVersionService.validateDelete(context, versionId, VersionService.REMOVE, VersionService.REMOVE));

        // reset permissions and it should pass
        permissionManager.setDefaultPermission(true);
        assertHasNoErrors(context, defaultVersionService.validateDelete(context, versionId, VersionService.REMOVE, VersionService.REMOVE));
    }

    /**
     * When swapping in a version for the one that is being deleted, the new version cannot be the same as the one which
     * is being deleted. It must also exist.
     */
    @Test
    public void shouldFailToSwapVersionWhenArgumentsAreInvalid() {
        MockPermissionManager permissionManager = MyPermissionManager.createPermissionManager(true);

        Version version = mockVersionWithProjectId(goodProjectId);
        Version swapVersionBadProject = mockVersionWithProjectId(badProjectId);
        Version swapVersion = mockVersionWithProjectId(goodProjectId);
        versionManagerWithVersions(MapBuilder.build(versionId, version,
                swapVersionId, swapVersion,
                swapVersionBadProjectId, swapVersionBadProject,
                nonExistsVersionId, null));

        DefaultVersionService defaultVersionService = new DefaultVersionService(versionManager, permissionManager, null, null, null, dateFieldFormat, null);
        // first check SWAP functionality for Affects Version
        // null id, bad version id and same version id will all fail
        assertHasErrorsAndFlush(context, defaultVersionService.validateDelete(context, versionId, new SwapVersionAction(null), VersionService.REMOVE));
        assertHasErrorsAndFlush(context, defaultVersionService.validateDelete(context, versionId, new SwapVersionAction(nonExistsVersionId), VersionService.REMOVE));
        assertHasErrorsAndFlush(context, defaultVersionService.validateDelete(context, versionId, new SwapVersionAction(versionId), VersionService.REMOVE));

        // good version id but different project ids will fail
        assertHasErrorsAndFlush(context, defaultVersionService.validateDelete(context, versionId, new SwapVersionAction(swapVersionBadProjectId), VersionService.REMOVE));

        // if user has admin permission then pass
        assertHasNoErrors(context, defaultVersionService.validateDelete(context, versionId, new SwapVersionAction(swapVersionId), VersionService.REMOVE));

        // then check SWAP functionality for Fix Version
        // null id, bad version id and same version id will all fail
        assertHasErrorsAndFlush(context, defaultVersionService.validateDelete(context, versionId, VersionService.REMOVE, new SwapVersionAction(null)));
        assertHasErrorsAndFlush(context, defaultVersionService.validateDelete(context, versionId, VersionService.REMOVE, new SwapVersionAction(nonExistsVersionId)));
        assertHasErrorsAndFlush(context, defaultVersionService.validateDelete(context, versionId, VersionService.REMOVE, new SwapVersionAction(versionId)));

        // good version id but different project ids will fail
        assertHasErrorsAndFlush(context, defaultVersionService.validateDelete(context, versionId, VersionService.REMOVE, new SwapVersionAction(swapVersionBadProjectId)));

        // good id will work
        assertHasNoErrors(context, defaultVersionService.validateDelete(context, versionId, VersionService.REMOVE, new SwapVersionAction(swapVersionId)));
    }

    /**
     * When swapping in a version for the one that is being deleted, the new version cannot be the same as the one which
     * is being deleted. It must also exist.
     */
    @Test
    public void shouldFailToMergeWhenArgumentsAreInvalid() {
        MockPermissionManager permissionManager = MyPermissionManager.createPermissionManager(false);

        Version version = mockVersionWithProjectId(goodProjectId);
        Version swapVersionBadProject = mockVersionWithProjectId(badProjectId);
        Version swapVersion = mockVersionWithProjectId(goodProjectId);
        versionManagerWithVersions(MapBuilder.build(versionId, version,
                swapVersionId, swapVersion,
                swapVersionBadProjectId, swapVersionBadProject,
                nonExistsVersionId, null));

        DefaultVersionService defaultVersionService = new DefaultVersionService(versionManager, permissionManager, null, null, null, dateFieldFormat, null);
        // first check SWAP functionality for Affects Version
        // null id, bad version id and same version id will all fail
        assertHasErrorsAndFlush(context, defaultVersionService.validateMerge(context, versionId, null));
        assertHasErrorsAndFlush(context, defaultVersionService.validateMerge(context, versionId, nonExistsVersionId));
        assertHasErrorsAndFlush(context, defaultVersionService.validateMerge(context, versionId, versionId));

        // good version id but different project ids will fail
        assertHasErrorsAndFlush(context, defaultVersionService.validateMerge(context, versionId, swapVersionBadProjectId));

        // if user doesn't have admin permission then fail
        assertHasErrorsAndFlush(context, defaultVersionService.validateMerge(context, versionId, swapVersionId));

        // reset permissions and it should pass
        permissionManager.setDefaultPermission(true);
        assertHasNoErrors(context, defaultVersionService.validateMerge(context, versionId, swapVersionId));
    }

    @Test
    public void shouldReturnCorrectValidatoionResult() {
        MockPermissionManager permissionManager = MyPermissionManager.createPermissionManager(true);

        Long affectsSwapVersionId = 10001L;
        Long fixSwapVersionId = 10002L;

        Version version = versionWithProject(project);
        Version affectsSwapVersion = versionWithProject(project);
        Version fixSwapVersion = versionWithProject(project);
        versionManagerWithVersions(MapBuilder.build(versionId, version,
                affectsSwapVersionId, affectsSwapVersion,
                fixSwapVersionId, fixSwapVersion));

        DefaultVersionService defaultVersionService = new DefaultVersionService(versionManager, permissionManager, null, null, null, dateFieldFormat, null);
        // if not swapping in any versions for Affects or Fix, the returned Versions in the result object will be null,
        // regardless of id passed into method
        VersionService.ValidationResult result = defaultVersionService.validateDelete(context, versionId, VersionService.REMOVE, VersionService.REMOVE);
        assertHasNoErrors(context, result);
        assertEquals(version, result.getVersionToDelete());
        assertNull(result.getAffectsSwapVersion());
        assertNull(result.getFixSwapVersion());

        result = defaultVersionService.validateDelete(context, versionId, VersionService.REMOVE, VersionService.REMOVE);
        assertHasNoErrors(context, result);
        assertEquals(version, result.getVersionToDelete());
        assertNull(result.getAffectsSwapVersion());
        assertNull(result.getFixSwapVersion());

        // if swapping versions, the result object must contain the version that was referenced by the id
        result = defaultVersionService.validateDelete(context, versionId, new SwapVersionAction(affectsSwapVersionId), new SwapVersionAction(fixSwapVersionId));
        assertHasNoErrors(context, result);
        assertEquals(version, result.getVersionToDelete());
        assertEquals(affectsSwapVersion, result.getAffectsSwapVersion());
        assertEquals(fixSwapVersion, result.getFixSwapVersion());
    }

    @Test
    public void shouldReturnCorrectValidationResultForMergeOperation() {
        MockPermissionManager permissionManager = MyPermissionManager.createPermissionManager(true);

        Version version = versionWithProject(project);
        Version swapVersion = versionWithProject(project);
        versionManagerWithVersions(MapBuilder.build(versionId, version, swapVersionId, swapVersion));

        DefaultVersionService defaultVersionService = new DefaultVersionService(versionManager, permissionManager, null, null, null, dateFieldFormat, null);

        // if swapping versions, the result object must contain the version that was referenced by the id
        VersionService.ValidationResult result = defaultVersionService.validateMerge(context, versionId, swapVersionId);
        assertHasNoErrors(context, result);
        assertEquals(version, result.getVersionToDelete());
        assertEquals(swapVersion, result.getAffectsSwapVersion());
        assertEquals(swapVersion, result.getFixSwapVersion());
    }

    @Test
    public void deleteShouldFailWhenValidationResultIsInvalid() throws Exception {
        Version version = versionWithProject(project);
        Version affectsSwapVersion = versionWithProject(project);
        Version fixSwapVersion = versionWithProject(project);

        ValidationResultImpl badResult = new ValidationResultImpl(new SimpleErrorCollection(), version, affectsSwapVersion, fixSwapVersion, false, Collections.<VersionService.ValidationResult.Reason>emptySet());

        expectedException.expect(IllegalArgumentException.class);
        service.delete(context, badResult);
    }

    @Test
    public void mergeShouldFailWhenValidationResultIsInvalid() throws Exception {
        Version version = versionWithProject(project);
        Version affectsSwapVersion = versionWithProject(project);
        Version fixSwapVersion = versionWithProject(project);

        ValidationResultImpl badResult = new ValidationResultImpl(new SimpleErrorCollection(), version, affectsSwapVersion, fixSwapVersion, false, Collections.<VersionService.ValidationResult.Reason>emptySet());

        expectedException.expect(IllegalArgumentException.class);
        service.merge(context, badResult);
    }

    @Test
    public void shouldCreateVersionWithCorrectDate() {
        Calendar calendar = Calendar.getInstance(Locale.ENGLISH);
        calendar.set(2000, Calendar.MARCH, 1, 1, 1, 1);
        calendar.set(Calendar.MILLISECOND, 101);

        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        calendar = Calendar.getInstance(Locale.ENGLISH);
        calendar.set(2000, Calendar.APRIL, 1, 1, 1, 1);
        calendar.set(Calendar.MILLISECOND, 101);

        Date inputReleaseDate = calendar.getTime();
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        Date whenReleaseDate = calendar.getTime();

        CreateVersionValidationResult result = service.validateCreateVersion(user, project, versionName, inputReleaseDate, null, null);

        assertTrue(result.isValid());
        assertSame(project, result.getProject());
        assertEquals(versionName, result.getVersionName());
        assertNull(result.getStartDate());
        assertEquals(whenReleaseDate, result.getReleaseDate());
    }

    @Test
    public void shouldReturnCorrectInformationForBadVersionDate() {
        String badDate = "invalid date";
        when(dateFieldFormat.parseDatePicker(badDate)).thenThrow(new IllegalArgumentException());

        CreateVersionValidationResult result = service.validateCreateVersion(user, project, versionName, badDate, null, null);

        assertFalse(result.isValid());
        assertEquals(EnumSet.of(BAD_RELEASE_DATE), result.getReasons());
    }

    @Test
    public void shouldCreateCorrectVersionDateString() {
        dateStringRepresentsDate(startDate, new Date(new Date().getTime() - 10000));
        Date parsedReleaseDate = new LocalDate(2012, 9, 27).toDate();
        dateStringRepresentsDate(releaseDate, parsedReleaseDate);

        CreateVersionValidationResult result = service.validateCreateVersion(user, project, versionName, releaseDate, null, null);

        assertTrue(result.isValid());
        assertSame(project, result.getProject());
        assertEquals(versionName, result.getVersionName());
        assertNull(result.getStartDate());
        assertEquals(parsedReleaseDate, result.getReleaseDate());
    }

    @Test
    public void shouldReturnProperValidationResultForValidationWithWrongVersionName() {
        String badVersionName = "";

        CreateVersionValidationResult result = service.validateCreateVersion(user, project, badVersionName, releaseDate, null, null);

        assertFalse(result.isValid());
        assertEquals(EnumSet.of(BAD_NAME), result.getReasons());
    }

    @Test
    public void shouldReturnProperValidationResultForValidationOfNullParameters() {
        ValidateResult result = service.validateCreateParameters(user, null, null, null, null);

        assertFalse(result.isValid());
        assertEquals(errors("admin.errors.must.specify.valid.project{[]}"), result.getErrors());
        assertNull(result.getParsedStartDate());
        assertNull(result.getParsedReleaseDate());
        assertEquals(EnumSet.of(BAD_PROJECT), result.getReasons());
    }

    @Test
    public void shouldReturnProperValidationResultForValidationWithoutProjectPersmission() {
        noPermissionsForProject(project);

        ValidateResult result = service.validateCreateParameters(user, project, null, null, null);

        assertFalse(result.isValid());
        assertEquals(errors("admin.errors.version.no.permission{[]}"), result.getErrors());
        assertNull(result.getParsedStartDate());
        assertNull(result.getParsedReleaseDate());
        assertEquals(EnumSet.of(FORBIDDEN), result.getReasons());
    }

    @Test
    public void shouldFailCheckVersionNameWhenNullIsPassed() {
        checkVersionNameError(null);
    }

    @Test
    public void shouldFailCheckVersionNameWhenEmptyStringIsPassed() {
        checkVersionNameError("");
    }

    @Test
    public void shouldReturnProperValidationResultWhenPassedExistingProjectName() {
        Version mockVersion = versionWithName(versionName);
        versionManagerHasVersionsForProject(projectId, mockVersion);

        ValidateResult result = service.validateCreateParameters(user, project, versionName, null, null);

        assertFalse(result.isValid());
        assertEquals(errorMap("name", "admin.errors.version.already.exists{[]}"), result.getErrors());
        assertNull(result.getParsedStartDate());
        assertNull(result.getParsedReleaseDate());
        assertEquals(EnumSet.of(DUPLICATE_NAME), result.getReasons());
    }

    @Test
    public void shouldCreateProjectForValidParameters() {
        Version mockVersion = versionWithName("1.0");
        versionManagerHasVersionsForProject(projectId, mockVersion);

        CreateVersionValidationResult result = service.validateCreateVersion(user, project, "1.1", (String) null, null, null);

        assertTrue(result.isValid());
        assertFalse(result.getProject() == null);
        assertFalse(result.getVersionName() == null);
        assertTrue(result.getStartDate() == null);
        assertTrue(result.getReleaseDate() == null);
    }

    @Test
    public void shouldCreateCorrectVersionWithCorrectDates() {
        Version mockVersion = versionWithName("1.0");
        versionManagerHasVersionsForProject(projectId, mockVersion);
        dateStringRepresentsDate(startDate, startDateParsed);
        dateStringRepresentsDate(releaseDate, releaseDateParsed);

        ValidateResult result = service.validateCreateParameters(user, project, "1.1", startDate, releaseDate);

        assertTrue(result.isValid());
        assertEquals(startDateParsed, result.getParsedStartDate());
        assertEquals(releaseDateParsed, result.getParsedReleaseDate());
        assertTrue(result.getReasons().isEmpty());
    }

    @Test
    public void shouldReturnInValidResultWhenPassedDatesAreInWrongOrder() {
        Version mockVersion = versionWithName("1.0");
        versionManagerHasVersionsForProject(projectId, mockVersion);
        Date startDateParsed = new Date();
        Date releaseDateParsed = new Date(System.currentTimeMillis() - 1000);
        dateStringRepresentsDate(startDate, startDateParsed);
        dateStringRepresentsDate(releaseDate, releaseDateParsed);

        ValidateResult result = service.validateCreateParameters(user, project, "1.1", startDate, releaseDate);

        assertFalse(result.isValid());
        assertEquals(startDateParsed, result.getParsedStartDate());
        assertEquals(releaseDateParsed, result.getParsedReleaseDate());
    }

    @Test
    public void shouldReturnInValidResultWhenStartDateCreationThrowsException() {
        Version mockVersion = versionWithName("1.0");
        versionManagerHasVersionsForProject(projectId, mockVersion);
        when(dateFieldFormat.parseDatePicker(startDate)).thenThrow(new IllegalArgumentException());

        ValidateResult result = service.validateCreateParameters(user, project, "1.1", startDate, null);

        assertFalse(result.isValid());
        assertEquals(errorMap("startDate", "admin.errors.incorrect.date.format{[dd-MMMMMMM-y]}"), result.getErrors());
        assertNull(result.getParsedStartDate());
        assertNull(result.getParsedReleaseDate());
        assertEquals(EnumSet.of(BAD_START_DATE), result.getReasons());
    }

    @Test
    public void shouldReturnInValidResultWhenReleaseDateCreationThrowsException() throws Exception {
        Version mockVersion = versionWithName("1.0");
        versionManagerHasVersionsForProject(projectId, mockVersion);
        when(dateFieldFormat.parseDatePicker(releaseDate)).thenThrow(new IllegalArgumentException());

        ValidateResult result = service.validateCreateParameters(user, project, "1.1", null, releaseDate);

        assertFalse(result.isValid());
        assertEquals(errorMap("releaseDate", "admin.errors.incorrect.date.format{[dd-MMMMMMM-y]}"), result.getErrors());
        assertNull(result.getParsedStartDate());
        assertNull(result.getParsedReleaseDate());
        assertEquals(EnumSet.of(BAD_RELEASE_DATE), result.getReasons());
    }

    @Test
    public void shouldFailCreatingVersionWhenVersionNameIsTooLong() throws Exception {
        String longText = "";
        while (longText.length() < 256) {
            longText += "a";
        }

        ValidateResult result = service.validateCreateParameters(user, project, longText, null, null);

        assertFalse(result.isValid());
        assertEquals(errorMap("name", "admin.errors.version.name.toolong{[]}"), result.getErrors());
        assertNull(result.getParsedStartDate());
        assertNull(result.getParsedReleaseDate());
        assertEquals(EnumSet.of(VERSION_NAME_TOO_LONG), result.getReasons());
    }

    @Test
    public void shouldBeAbleCreateAReleasedVersionFromAValidationResultInstance() throws Exception {
        final Date MOCK_RELEASE_DATE = new LocalDate().toDate();
        final Date MOCK_START_DATE = new LocalDate().toDate();
        final String MOCK_VERSION_DESCRIPTION = "Test description";
        final String MOCK_VERSION_NAME = "test 1.1";
        final boolean MOCK_VERSION_RELEASED = true;
        final Long MOCK_PROJECT_ID = projectId;
        final Long MOCK_SCHEDULE_AFTER_VERSION = null;

        Version mockVersion = versionWithName(MOCK_VERSION_NAME);
        when(mockVersion.getStartDate()).thenReturn(MOCK_START_DATE);
        when(mockVersion.getReleaseDate()).thenReturn(MOCK_RELEASE_DATE);
        when(mockVersion.getDescription()).thenReturn(MOCK_VERSION_DESCRIPTION);
        when(mockVersion.getProjectId()).thenReturn(MOCK_PROJECT_ID);
        when(mockVersion.isReleased()).thenReturn(MOCK_VERSION_RELEASED);

        when(versionManager.createVersion(MOCK_VERSION_NAME,
                MOCK_START_DATE,
                MOCK_RELEASE_DATE,
                MOCK_VERSION_DESCRIPTION,
                MOCK_PROJECT_ID,
                MOCK_SCHEDULE_AFTER_VERSION,
                MOCK_VERSION_RELEASED)).thenReturn(mockVersion);

        VersionBuilder versionBuilder = new VersionBuilderImpl().name(MOCK_VERSION_NAME).startDate(MOCK_START_DATE).releaseDate(MOCK_RELEASE_DATE).projectId(MOCK_PROJECT_ID).released(MOCK_VERSION_RELEASED).description(MOCK_VERSION_DESCRIPTION);
        VersionBuilderValidationResult validationResult = service.validateCreate(user, versionBuilder);

        ServiceOutcome<Version> versionServiceOutcome = service.create(user, validationResult);

        Version createdVersion = versionServiceOutcome.getReturnedValue();

        assertNotNull(createdVersion);
        assertEquals(createdVersion.getName(), MOCK_VERSION_NAME);
        assertEquals(createdVersion.getStartDate(), MOCK_START_DATE);
        assertEquals(createdVersion.getReleaseDate(), MOCK_RELEASE_DATE);
        assertEquals(createdVersion.getDescription(), MOCK_VERSION_DESCRIPTION);
        assertEquals(createdVersion.getProjectId(), MOCK_PROJECT_ID);
        assertEquals(createdVersion.isReleased(), MOCK_VERSION_RELEASED);
    }

    @Test
    public void shouldBeAbleToCreateProperReleasedVersion() throws Exception {
        Date date = new LocalDate().toDate();
        Version mockVersion = versionWithName("1.1");
        when(versionManager.createVersion("1.1", null, date, null, projectId, null, false)).thenReturn(mockVersion);

        CreateVersionValidationResult request = new CreateVersionValidationResult(new SimpleErrorCollection(), project, "1.1", null, date, null, null);

        Version version = service.createVersion(user, request);

        assertFalse(version == null);
        assertEquals("1.1", version.getName());
    }

    @Test
    public void shouldFailToCreateVersionForBadRequest() throws Exception {
        CreateVersionValidationResult request = new CreateVersionValidationResult(new SimpleErrorCollection(), null, null, null, null, null, null);

        expectedException.expect(RuntimeException.class);
        service.createVersion(user, request);
    }

    @Test
    public void shouldBeAbleToCreateVersion() throws Exception {
        Date startDate = new LocalDate().toDate();
        Date releaseDate = new LocalDate().toDate();
        Version mockVersion = versionWithName("1.1");
        when(versionManager.createVersion("1.1", startDate, releaseDate, null, projectId, null, false)).thenReturn(mockVersion);

        CreateVersionValidationResult request = new CreateVersionValidationResult(new SimpleErrorCollection(), project, "1.1", startDate, releaseDate, null, null);

        Version version = service.createVersion(user, request);
        assertFalse(version == null);
        assertEquals("1.1", version.getName());
    }

    @Test
    public void shouldThrowExceptionWhenVersionIsGetByNullId() {
        expectedException.expect(instanceOf(IllegalArgumentException.class));
        service.getVersionById(user, null, null);
    }

    @Test
    public void shouldReturnInvalidResultWhenVersionWithoutProperPermissionsIsRetrieved() {
        noPermissionsForProject(projectWithNullId);

        VersionService.VersionResult result = service.getVersionById(user, projectWithNullId, 1L);

        assertFalse(result.isValid());
        assertEquals("admin.errors.version.no.read.permission{[]}", result.getErrorCollection().getErrorMessages().iterator().next());
    }

    @Test
    public void shouldReturnInvalidResultWhenVersionForProjectIsRetrievedWithoutPermissions() {
        noPermissionsForProject(projectWithNullId);

        VersionService.VersionsResult result = service.getVersionsByProject(user, projectWithNullId);

        assertFalse(result.isValid());
        assertEquals("admin.errors.version.no.read.permission{[]}", result.getErrorCollection().getErrorMessages().iterator().next());
    }

    @Test
    public void shouldReturnInvalidResultWhenVersionIsRetrievedByNameAndProjectWithoutPermission() {
        noPermissionsForProject(projectWithNullId);

        VersionService.VersionResult result = service.getVersionByProjectAndName(user, projectWithNullId, "version name");

        assertFalse(result.isValid());
        assertEquals("admin.errors.version.no.read.permission{[]}", result.getErrorCollection().getErrorMessages().iterator().next());
    }

    @Test
    public void shouldReturnInvalidResultWhenNonExistingVersionIsRetrieved() {
        when(permissionManager.hasPermission(Permissions.ADMINISTER, user)).thenReturn(false);
        when(permissionManager.hasPermission(ADMINISTER_PROJECTS, projectWithNullId, user)).thenReturn(true);

        VersionService.VersionResult result = service.getVersionById(user, projectWithNullId, 1L);

        assertFalse(result.isValid());
        assertEquals("admin.errors.version.not.exist.with.id{[1]}", result.getErrorCollection().getErrorMessages().iterator().next());
    }

    @Test
    public void shouldRetrieveVersionCorrectly() {
        Version mockVersion = aVersion();
        when(versionManager.getVersion(1L)).thenReturn(mockVersion);

        VersionService.VersionResult result = service.getVersionById(user, projectWithNullId, 1L);

        assertTrue(result.isValid());
        assertEquals(mockVersion, result.getVersion());
    }

    @Test
    public void shouldThrowExceptionWhenValidatingNullVersion() {
        expectedException.expect(IllegalArgumentException.class);
        service.validateReleaseVersion(user, null, (Date) null);
    }

    @Test
    public void shouldReturnInvalidResultValidatingVersionWithoutValidProject() {
        VersionService.ReleaseVersionValidationResult result = service.validateReleaseVersion(user, aVersion(), (Date) null);

        assertFalse(result.isValid());
        assertEquals("admin.errors.must.specify.valid.project{[]}", result.getErrorCollection().getErrorMessages().iterator().next());
    }

    @Test
    public void shouldReturnInvalidResultForReleseVersionValidationForProjectWithoutPermission() {
        noPermissionsForProject(projectWithNullId);

        VersionService.ReleaseVersionValidationResult result =
                service.validateReleaseVersion(user, versionWithProject(projectWithNullId), (Date) null);

        assertFalse(result.isValid());
        assertEquals("admin.errors.version.no.permission{[]}", result.getErrorCollection().getErrorMessages().iterator().next());
    }

    @Test
    public void shouldReturnInvalidResultForValidationReleaseVersionWithInvalidVersionName() {
        VersionService.ReleaseVersionValidationResult result =
                service.validateReleaseVersion(user, versionWithProject(projectWithNullId), (Date) null);

        assertFalse(result.isValid());
        assertEquals("admin.errors.enter.valid.version.name{[]}", result.getErrorCollection().getErrors().get("name"));
    }

    @Test
    public void shouldReturnInvalidResultWhenVersionIsAlreadyReleased() {
        Version version = versionWithProjectAndIsReleased(projectWithNullId, true);

        VersionService.ReleaseVersionValidationResult result = service.validateReleaseVersion(user, version, (Date) null);

        assertFalse(result.isValid());
        assertEquals("admin.errors.release.already.released{[]}", result.getErrorCollection().getErrorMessages().iterator().next());
    }

    @Test
    public void shouldReturnValidResultForSuccessfullValidationOfVersionRelease() throws Exception {
        Version version = versionWithProjectAndIsReleased(projectWithNullId, false);

        VersionService.ReleaseVersionValidationResult result = service.validateReleaseVersion(user, version, releaseDateParsed);

        assertTrue(result.isValid());
        assertEquals(version, result.getVersion());
        assertEquals(releaseDateParsed, result.getReleaseDate());
    }

    /*
     * Note, this does not test the checkVersionDetails validation, since the all the testValidateRelease**() methods
     * above already test this.
     */
    @Test
    public void shouldReturnInvalidResultForUnreleasingNotReleasedVersion() throws GenericEntityException {
        Version version = versionWithProjectAndIsReleased(projectWithNullId, false);

        VersionService.ReleaseVersionValidationResult result = service.validateUnreleaseVersion(user, version, (Date) null);

        assertFalse(result.isValid());
        assertEquals("admin.errors.release.not.released{[]}", result.getErrorCollection().getErrorMessages().iterator().next());
    }

    @Test
    public void shouldUnreleaseVersionSuccesfully() {
        Version version = versionWithProjectAndIsReleased(projectWithNullId, true);

        VersionService.ReleaseVersionValidationResult result = service.validateUnreleaseVersion(user, version, (Date) null);

        assertTrue(result.isValid());
        assertEquals(version, result.getVersion());
    }

    @Test
    public void shouldThrowsExceptionWhenVersionIsReleasedWithNullValidationResult() {
        expectedException.expectMessage("You can not release a version with a null validation result.");
        service.releaseVersion(null);
    }

    @Test
    public void shouldThrowsExceptionWhenVersionIsReleasedWithInvalidValidationResult() {
        VersionService.ReleaseVersionValidationResult result = new VersionService.ReleaseVersionValidationResult(errors("Something bad happened"));

        expectedException.expectMessage("You can not release a version with an invalid validation result.");
        service.releaseVersion(result);
    }

    @Test
    public void shouldReleaseVersionSuccessfully() throws Exception {
        VersionService.ReleaseVersionValidationResult result = new VersionService.ReleaseVersionValidationResult(errors(), versionWithId(99L), new Date());

        service.releaseVersion(result);

        verify(versionManager).releaseVersion(any(Version.class), eq(true));
        verify(versionManager).getVersion(99L);
    }

    @Test
    public void shouldThrowsExceptionWhenVersionIsUnreleasedWithNullValidationResult() throws Exception {
        expectedException.expectMessage("You can not unrelease a version with a null validation result.");
        service.unreleaseVersion(null);
    }

    @Test
    public void shouldThrowsExceptionWhenVersionIsUnreleasedWithInvalidValidationResult() throws Exception {
        VersionService.ReleaseVersionValidationResult result = new VersionService.ReleaseVersionValidationResult(errors("Something bad happened"));
        expectedException.expectMessage("You can not unrelease a version with an invalid validation result.");
        service.unreleaseVersion(result);
    }

    @Test
    public void shouldUnreleaseVersionSuccessfully() throws Exception {
        ErrorCollection errors = new SimpleErrorCollection();
        VersionService.ReleaseVersionValidationResult result = new VersionService.ReleaseVersionValidationResult(errors, versionWithId(99L), new Date());

        service.unreleaseVersion(result);

        verify(versionManager).releaseVersion(any(Version.class), eq(false));
        verify(versionManager).getVersion(99L);
    }

    /*
     * Note, this does not test the checkVersionDetails validation, since the all the testValidateRelease**() methods
     * above already test this.
     */
    @Test
    public void shouldReturnInvalidResultWhenVersionIsAlreadyArchived() throws Exception {
        Version mockVersion = versionWithProjectAndIsArchived(project, true);

        ArchiveVersionValidationResult result = service.validateArchiveVersion(user, mockVersion);

        assertFalse(result.isValid());
        assertEquals("admin.errors.archive.already.archived{[]}", result.getErrorCollection().getErrorMessages().iterator().next());
    }

    @Test
    public void shouldReturnValidResultForArchiveVersionValidation() throws Exception {
        Version mockVersion = versionWithProjectAndIsArchived(project, false);

        ArchiveVersionValidationResult result = service.validateArchiveVersion(user, mockVersion);

        assertTrue(result.isValid());
        assertEquals(mockVersion, result.getVersion());
    }

    /*
     * Note, this does not test the checkVersionDetails validation, since the all the testValidateRelease**() methods
     * above already test this.
     */
    @Test
    public void shouldReturnInvalidResultForValidationOfUnarchiveVersion() throws Exception {
        Version mockVersion = versionWithProjectAndIsArchived(project, false);

        ArchiveVersionValidationResult result = service.validateUnarchiveVersion(user, mockVersion);

        assertFalse(result.isValid());
        assertEquals("admin.errors.archive.not.archived{[]}", result.getErrorCollection().getErrorMessages().iterator().next());
    }

    @Test
    public void shouldPerformUnarchivingValidationSuccessfully() throws Exception {
        Version mockVersion = versionWithProjectAndIsArchived(project, true);

        ArchiveVersionValidationResult result = service.validateUnarchiveVersion(user, mockVersion);

        assertTrue(result.isValid());
        assertEquals(mockVersion, result.getVersion());
    }

    @Test
    public void shouldThrowExceptionWhenVersionArchivingIsPerformedForNullValidationResult() {
        expectedException.expectMessage("You can not archive a version with a null validation result.");
        service.archiveVersion(null);
    }

    @Test
    public void shouldThrowExceptionWhenVersionArchivingIsPerformedForInvalidValidationResult() {
        ArchiveVersionValidationResult result = new ArchiveVersionValidationResult(errors("Something bad happened"));
        expectedException.expectMessage("You can not archive a version with an invalid validation result.");
        service.archiveVersion(result);
    }

    @Test
    public void shouldArchiveVersionSuccessfully() {
        ArchiveVersionValidationResult result = new ArchiveVersionValidationResult(errors(), versionWithId(103L));

        service.archiveVersion(result);

        verify(versionManager).archiveVersion(any(Version.class), eq(true));
        verify(versionManager).getVersion(103L);
    }

    @Test
    public void shouldThrowExceptionDuringVersionUnarchivingWithNullValidationResult() {
        expectedException.expectMessage("You can not unarchive a version with a null validation result.");
        service.unarchiveVersion(null);
    }

    @Test
    public void shouldThrowExceptionWhenVersionIsUnarchivedWithInvalidValidationResult() {
        ArchiveVersionValidationResult result = new ArchiveVersionValidationResult(errors("Something bad happened"));

        expectedException.expectMessage("You can not unarchive a version with an invalid validation result.");
        service.unarchiveVersion(result);
    }

    @Test
    public void shouldPerformUnarchivingSuccessfully() {
        ArchiveVersionValidationResult result = new ArchiveVersionValidationResult(errors(), versionWithId(99L));

        service.unarchiveVersion(result);

        verify(versionManager).archiveVersion(any(Version.class), eq(false));
        verify(versionManager).getVersion(99L);
    }

    @Test
    public void shouldThrowExceptionWhileCheckingIfNullVersionIsOverdue() {
        expectedException.expect(instanceOf(IllegalArgumentException.class));
        service.isOverdue(null);
    }

    @Test
    public void testIsOverdueProject() {
        Version version = aVersion();
        when(versionManager.isVersionOverDue(version)).thenReturn(false).thenReturn(true);

        assertFalse(service.isOverdue(version));
        assertTrue(service.isOverdue(version));
    }

    @Test
    public void shouldConvertDatesToMidnight() {
        Version mockVersion = versionWithName(versionName);
        when(versionManager.isDuplicateName(mockVersion, versionName)).thenReturn(false);
        DateTime now = new DateTime();
        Date midnightNow = new LocalDate(now.getMillis()).toDate();
        Date nonMidnight = new DateTime(now).withHourOfDay(4).toDate();

        VersionBuilder builder = new VersionBuilderImpl(mockVersion)
                .startDate(nonMidnight)
                .releaseDate(nonMidnight);

        VersionBuilderValidationResult result = service.validateUpdate(user, builder);

        VersionBuilderImpl builderResult = (VersionBuilderImpl) result.getResult();
        assertThat(builderResult.getStartDate(), is(midnightNow));
        assertThat(builderResult.getReleaseDate(), is(midnightNow));
    }

    @Test
    public void shouldPerfromProperVersionDeletionAndRemoveItFromIssues() {
        Version versionToDelete = aVersion();
        ValidationResultImpl validationResult = new ValidationResultImpl(new SimpleErrorCollection(), versionToDelete, null, null, true, emptySet());

        service.delete(context, validationResult);

        verify(versionManager).deleteAndRemoveFromIssues(context.getLoggedInApplicationUser(), versionToDelete);
    }

    @Test
    public void addingAnIssueToAnArchivedVersionResultsInAnError() {
        Version archivedVersion = archivedVersionWithName(versionName);
        versionManagerHasVersionsForProject(projectId, archivedVersion);
        VersionBuilder versionBuilder = new VersionBuilderImpl().name(versionName).projectId(projectId);

        VersionBuilderValidationResult validationResult = service.validateCreate(user, versionBuilder);

        assertThat(validationResult.getErrorCollection().getErrors().get("name"), is("admin.errors.version.cannot.add.archived.version{[]}"));
    }

    @Test
    public void shouldFailIfNoTargetVersionWasPassedToRemoveAndSwap() {
        final ServiceResult serviceResult = service.deleteVersionAndSwap(
                context,
                service.createVersionDeletaAndReplaceParameters(new VersionImpl(null, null, null)).build()
        );

        assertThat(serviceResult,
                FeatureMatchers.hasFeature(
                        ServiceResult::isValid, Matchers.is(false),
                        "isValid",
                        "isValid"));
        assertThat(serviceResult.getErrorCollection(), ErrorCollectionMatcher.hasErrorMessage(
                "You must specify a version."
        ));
        assertThat(serviceResult.getErrorCollection().getReasons(), Matchers.contains(Reason.NOT_FOUND));
    }

    @Test
    public void shouldFailIfUserHasNoPermissionToProjectWhileRemoveAndSwap() {
        final MockApplicationUser youShallNotPass = new MockApplicationUser("admin");
        when(permissionManager.hasPermission(Permissions.ADMINISTER, youShallNotPass)).thenReturn(false);
        when(permissionManager.hasPermission(ADMINISTER_PROJECTS, project, youShallNotPass)).thenReturn(false);
        final MockJiraServiceContext youShallNotPassContext = new MockJiraServiceContext(youShallNotPass);

        final ServiceResult serviceResult = service.deleteVersionAndSwap(
                youShallNotPassContext,
                service.createVersionDeletaAndReplaceParameters(new VersionImpl(projectId, 1L, null)).build()
        );

        assertThat(serviceResult,
                FeatureMatchers.hasFeature(
                        ServiceResult::isValid, Matchers.is(false),
                        "isValid",
                        "isValid"));
        assertThat(serviceResult.getErrorCollection(), ErrorCollectionMatcher.hasErrorMessage(
                "This user does not have permission to complete this operation."
        ));
        assertThat(serviceResult.getErrorCollection().getReasons(), Matchers.contains(Reason.FORBIDDEN));
    }

    @Test
    public void shouldFailIfTargetVersionIsInDifferentProjectForRemoveAndSwap() {
        final MockApplicationUser youCanPass = new MockApplicationUser("admin");
        when(permissionManager.hasPermission(Permissions.ADMINISTER, youCanPass)).thenReturn(true);
        final MockJiraServiceContext youCanPassContext = new MockJiraServiceContext(youCanPass);

        final ServiceResult serviceResult = service.deleteVersionAndSwap(
                youCanPassContext,
                service
                        .createVersionDeletaAndReplaceParameters(new VersionImpl(projectId, 1L, null))
                        .moveFixIssuesTo(mockVersionWithProjectId(badProjectId, 2L))
                        .build()
        );

        assertThat(serviceResult,
                FeatureMatchers.hasFeature(
                        ServiceResult::isValid, Matchers.is(false),
                        "isValid",
                        "isValid"));
        assertThat(serviceResult.getErrorCollection(), ErrorCollectionMatcher.hasError("fixVersionReplacement",
                "You cannot move the issues to a version from a different project."
        ));
        assertThat(serviceResult.getErrorCollection().getReasons(), Matchers.contains(Reason.VALIDATION_FAILED));
    }


    @Test
    public void shouldFailIfTargetVersionIsSameAsSourceRemoveAndSwap() {
        final MockApplicationUser youCanPass = new MockApplicationUser("admin");
        when(permissionManager.hasPermission(Permissions.ADMINISTER, youCanPass)).thenReturn(true);
        final MockJiraServiceContext youCanPassContext = new MockJiraServiceContext(youCanPass);

        final ServiceResult serviceResult = service.deleteVersionAndSwap(
                youCanPassContext,
                service
                        .createVersionDeletaAndReplaceParameters(new VersionImpl(projectId, 1L, null))
                        .moveAffectedIssuesTo(mockVersionWithProjectId(projectId, 1L))
                        .build()
        );

        assertThat(serviceResult,
                FeatureMatchers.hasFeature(
                        ServiceResult::isValid, Matchers.is(false),
                        "isValid",
                        "isValid"));
        assertThat(serviceResult.getErrorCollection(), ErrorCollectionMatcher.hasError("affectedVersionReplacement",
                "You cannot move the issues to the version being deleted."
        ));
        assertThat(serviceResult.getErrorCollection().getReasons(), Matchers.contains(Reason.VALIDATION_FAILED));
    }


    @Test
    public void shouldGiveErrorInformationForAllInvalidFieldsRemoveAndSwap() {
        final MockApplicationUser youCanPass = new MockApplicationUser("admin");
        when(permissionManager.hasPermission(Permissions.ADMINISTER, youCanPass)).thenReturn(true);
        final MockJiraServiceContext youCanPassContext = new MockJiraServiceContext(youCanPass);

        final MockVersion versionInOtherProject = mockVersionWithProjectId(badProjectId, 2L);
        final ServiceResult serviceResult = service.deleteVersionAndSwap(
                youCanPassContext,
                service
                        .createVersionDeletaAndReplaceParameters(new VersionImpl(projectId, 1L, null))
                        .moveAffectedIssuesTo(versionInOtherProject)
                        .moveFixIssuesTo(versionInOtherProject)
                        .moveCustomFieldTo(12, versionInOtherProject)
                        .build()
        );

        assertThat(serviceResult,
                FeatureMatchers.hasFeature(
                        ServiceResult::isValid, Matchers.is(false),
                        "isValid",
                        "isValid"));
        assertThat(serviceResult.getErrorCollection(),
                allOf(
                        ErrorCollectionMatcher.hasError("affectedVersionReplacement",
                                "You cannot move the issues to a version from a different project."),
                        ErrorCollectionMatcher.hasError("fixVersionReplacement",
                                "You cannot move the issues to a version from a different project."),
                        ErrorCollectionMatcher.hasError("customFieldReplacement",
                                "You cannot move the issues to a version from a different project.")
                )
        );
        assertThat(serviceResult.getErrorCollection().getReasons(), Matchers.contains(Reason.VALIDATION_FAILED));
    }

    @Test
    public void shouldFailIfCustomVersion_TargetVersionIsInDifferentProjectForRemoveAndSwap() {
        final MockApplicationUser youCanPass = new MockApplicationUser("admin");
        when(permissionManager.hasPermission(Permissions.ADMINISTER, youCanPass)).thenReturn(true);
        final MockJiraServiceContext youCanPassContext = new MockJiraServiceContext(youCanPass);

        final ServiceResult serviceResult = service.deleteVersionAndSwap(
                youCanPassContext,
                service
                        .createVersionDeletaAndReplaceParameters(new VersionImpl(projectId, 1L, null))
                        .moveCustomFieldTo(10000, mockVersionWithProjectId(projectId, 1L))
                        .build()
        );

        assertThat(serviceResult,
                FeatureMatchers.hasFeature(
                        ServiceResult::isValid, Matchers.is(false),
                        "isValid",
                        "isValid"));
        assertThat(serviceResult.getErrorCollection(), ErrorCollectionMatcher.hasError("customFieldReplacement",
                "You cannot move the issues to the version being deleted."
        ));
        assertThat(serviceResult.getErrorCollection().getReasons(), Matchers.contains(Reason.VALIDATION_FAILED));
    }

    @Test
    public void shouldFailIfCustomVersion_TargetVersionIsSameAsSourceForRemoveAndSwap() {
        final MockApplicationUser youCanPass = new MockApplicationUser("admin");
        when(permissionManager.hasPermission(Permissions.ADMINISTER, youCanPass)).thenReturn(true);
        final MockJiraServiceContext youCanPassContext = new MockJiraServiceContext(youCanPass);

        final ServiceResult serviceResult = service.deleteVersionAndSwap(
                youCanPassContext,
                service
                        .createVersionDeletaAndReplaceParameters(new VersionImpl(projectId, 1L, null))
                        .moveCustomFieldTo(10000, mockVersionWithProjectId(badProjectId, 2L))
                        .build()
        );

        assertThat(serviceResult,
                FeatureMatchers.hasFeature(
                        ServiceResult::isValid, Matchers.is(false),
                        "isValid",
                        "isValid"));
        assertThat(serviceResult.getErrorCollection(), ErrorCollectionMatcher.hasError("customFieldReplacement",
                "You cannot move the issues to a version from a different project."
        ));
        assertThat(serviceResult.getErrorCollection().getReasons(), Matchers.contains(Reason.VALIDATION_FAILED));
    }

    @Test
    public void nullInTargetVersionIsAllowedForRemoveAndSwap() {
        final MockApplicationUser youCanPass = new MockApplicationUser("admin");
        when(permissionManager.hasPermission(Permissions.ADMINISTER, youCanPass)).thenReturn(true);
        final MockJiraServiceContext youCanPassContext = new MockJiraServiceContext(youCanPass);

        final ServiceResult serviceResult = service.deleteVersionAndSwap(
                youCanPassContext,
                service
                        .createVersionDeletaAndReplaceParameters(new VersionImpl(projectId, 1L, null))
                        .moveFixIssuesTo(null)
                        .moveCustomFieldTo(10000, null)
                        .build()
        );

        assertThat(serviceResult,
                FeatureMatchers.hasFeature(
                        ServiceResult::isValid, Matchers.is(true),
                        "isValid",
                        "isValid"));
    }

    private Version archivedVersionWithName(String versionName) {
        Version version = versionWithName(versionName);
        when(version.isArchived()).thenReturn(true);

        return version;
    }

    private Version aVersion() {
        return mock(Version.class);
    }

    private Version versionWithId(Long versionId) {
        Version version = aVersion();
        when(version.getId()).thenReturn(versionId);

        return version;
    }

    private Version versionWithName(String versionName) {
        Version version = aVersion();
        when(version.getName()).thenReturn(versionName);

        return version;
    }

    private Version versionWithProject(Project project) {
        return versionWithProjectAndName(project, null);
    }

    private Version versionWithProjectAndName(Project project, String versionName) {
        Version version = aVersion();
        when(version.getProject()).thenReturn(project);
        when(version.getProjectObject()).thenReturn(project);
        when(version.getProjectId()).thenReturn(project.getId());
        when(version.getName()).thenReturn(versionName);

        return version;
    }

    private Version versionWithProjectAndIsReleased(Project project, boolean released) {
        Version version = versionWithProjectAndName(project, "version name");
        when(version.isReleased()).thenReturn(released);

        return version;
    }

    private Version versionWithProjectAndIsArchived(Project project, boolean isArchived) {
        Version version = versionWithProjectAndName(project, "version name");
        when(version.isArchived()).thenReturn(isArchived);

        return version;
    }

    private void noPermissionsForProject(Project project) {
        when(permissionManager.hasPermission(Permissions.ADMINISTER, user)).thenReturn(false);
        when(permissionManager.hasPermission(ADMINISTER_PROJECTS, project, user)).thenReturn(false);
        when(permissionManager.hasPermission(BROWSE_PROJECTS, project, user)).thenReturn(false);
    }

    private void dateStringRepresentsDate(String dateString, Date date) {
        when(dateFieldFormat.parseDatePicker(dateString)).thenReturn(date);
    }

    private void versionManagerHasVersionsForProject(Long projectId, Version... versions) {
        when(versionManager.getVersions(projectId)).thenReturn(newArrayList(versions));
    }

    private ErrorCollection errorMap(String name, String message) {
        SimpleErrorCollection collection = new SimpleErrorCollection();
        collection.addError(name, message);
        return collection;
    }

    private ErrorCollection errors(String... args) {
        SimpleErrorCollection collection = new SimpleErrorCollection();
        for (String arg : args) {
            collection.addErrorMessage(arg);
        }
        return collection;
    }

    private void versionManagerWithVersions(Map<Long, Version> versions) {
        versions.entrySet()
                .stream()
                .forEach(
                        entry -> when(versionManager.getVersion(entry.getKey())).thenReturn(entry.getValue())
                );
    }

    private void assertHasErrorsAndFlush(JiraServiceContext context, VersionService.ValidationResult result) {
        assertNotNull(result);
        assertFalse(context.getErrorCollection().getFlushedErrorMessages().isEmpty());
        assertFalse(result.isValid());
    }

    private void assertHasNoErrors(JiraServiceContext context, VersionService.ValidationResult result) {
        assertNotNull(result);
        if (context.getErrorCollection().hasAnyErrors()) {
            assertFalse(context.getErrorCollection().getErrorMessages().iterator().next(), context.getErrorCollection().hasAnyErrors());
        }
        assertTrue(result.isValid());
    }

    private MockVersion mockVersionWithProjectId(final Long projectId, final long verisionId) {
        return new MockVersion(verisionId, "version-" + verisionId) {
            public Project getProject() {
                return new MockProject(projectId);
            }

            public Long getProjectId() {
                return projectId;
            }
        };
    }

    private MockVersion mockVersionWithProjectId(final Long projectId) {
        return new MockVersion() {
            public Project getProject() {
                return new MockProject(projectId);
            }

            public Long getProjectId() {
                return projectId;
            }
        };
    }

    private void checkVersionNameError(String versionName) {
        ValidateResult result = service.validateCreateParameters(user, project, versionName, null, null);

        assertFalse(result.isValid());
        assertEquals(errorMap("name", "admin.errors.enter.valid.version.name{[]}"), result.getErrors());
        assertNull(result.getParsedStartDate());
        assertNull(result.getParsedReleaseDate());
        assertEquals(EnumSet.of(BAD_NAME), result.getReasons());
    }
}
