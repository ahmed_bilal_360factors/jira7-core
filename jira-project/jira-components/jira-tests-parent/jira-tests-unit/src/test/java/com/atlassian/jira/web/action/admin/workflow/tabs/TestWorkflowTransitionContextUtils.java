package com.atlassian.jira.web.action.admin.workflow.tabs;

import com.atlassian.jira.workflow.JiraWorkflow;
import com.opensymphony.workflow.loader.ActionDescriptor;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Map;
import java.util.Optional;

import static com.atlassian.jira.web.action.admin.workflow.tabs.WorkflowTransitionContextUtils.TRANSITION_KEY;
import static com.atlassian.jira.web.action.admin.workflow.tabs.WorkflowTransitionContextUtils.WORKFLOW_KEY;
import static java.util.Collections.emptyMap;
import static java.util.Collections.singletonMap;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class TestWorkflowTransitionContextUtils {

    @Mock
    private ActionDescriptor mockTransition;

    @Mock
    private JiraWorkflow mockWorkflow;

    @Test
    public void shouldGetTransitionWhenItExists() {
        // Set up
        final Map<String, Object> context = singletonMap(TRANSITION_KEY, mockTransition);

        // Invoke
        final Optional<ActionDescriptor> transition = WorkflowTransitionContextUtils.getTransition(context);

        // Check
        assertThat(transition, is(Optional.of(mockTransition)));
    }

    @Test
    public void shouldGetWorkflowWhenItExists() {
        // Set up
        final Map<String, Object> context = singletonMap(WORKFLOW_KEY, mockWorkflow);

        // Invoke
        final Optional<JiraWorkflow> workflow = WorkflowTransitionContextUtils.getWorkflow(context);

        // Check
        assertThat(workflow, is(Optional.of(mockWorkflow)));
    }

    @Test
    public void shouldNotGetWorkflowWhenItHasTheWrongType() {
        // Set up
        final Map<String, Object> context = singletonMap(WORKFLOW_KEY, "not a workflow");

        // Invoke
        final Optional<JiraWorkflow> workflow = WorkflowTransitionContextUtils.getWorkflow(context);

        // Check
        assertThat(workflow, is(Optional.empty()));
    }

    @Test
    public void shouldNotGetWorkflowWhenTheKeyIsAbsent() {
        // Set up
        final Map<String, Object> context = singletonMap("wrong key", mockWorkflow);

        // Invoke
        final Optional<JiraWorkflow> workflow = WorkflowTransitionContextUtils.getWorkflow(context);

        // Check
        assertThat(workflow, is(Optional.empty()));
    }

    @Test
    public void shouldNotGetWorkflowWhenTheContextIsEmpty() {
        // Set up
        final Map<String, Object> context = emptyMap();

        // Invoke
        final Optional<JiraWorkflow> workflow = WorkflowTransitionContextUtils.getWorkflow(context);

        // Check
        assertThat(workflow, is(Optional.empty()));
    }
}
