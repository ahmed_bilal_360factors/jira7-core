package com.atlassian.jira.notification;

import com.atlassian.jira.mock.security.MockSimpleAuthenticationContext;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.scheme.Scheme;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

/**
 * @since v4.4
 */
public class TestDefaultProjectNotificationsSchemeHelper {
    @Rule
    public MethodRule initMockito = MockitoJUnit.rule();

    private MockSimpleAuthenticationContext authContext;
    private MockApplicationUser user;
    @Mock
    private PermissionManager permissionManager;
    @Mock
    private NotificationSchemeManager notificationSchemeManager;


    @Before
    public void setUp() throws Exception {
        user = new MockApplicationUser("mtan");
        authContext = new MockSimpleAuthenticationContext(user);
    }

    @Test
    public void testGetProjectsNonDefault() throws Exception {
        final MockProject project1 = new MockProject(101928282L, "ONE");
        final MockProject project2 = new MockProject(35438590L, "TWO");

        final Scheme issueSecurityScheme = new Scheme("101010L", "lalala");

        when(notificationSchemeManager.getProjects(eq(issueSecurityScheme)))
                .thenReturn(Lists.<Project>newArrayList(project1, project2));

        ProjectNotificationsSchemeHelper helper = createHelper(Sets.<Project>newHashSet(project1));

        List<Project> projects = helper.getSharedProjects(issueSecurityScheme);
        assertEquals(Arrays.<Project>asList(project1), projects);
    }

    private ProjectNotificationsSchemeHelper createHelper(final Collection<? extends Project> allowedProjects) {
        return new DefaultProjectNotificationsSchemeHelper(notificationSchemeManager, authContext, permissionManager) {
            boolean hasEditPermission(final ApplicationUser user, final Project project) {
                return allowedProjects.contains(project);
            }
        };
    }

}
