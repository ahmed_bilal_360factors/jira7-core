package com.atlassian.jira.crowd.embedded.ofbiz;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.crowd.embedded.api.UserComparator;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.DelegatingApplicationUser;
import org.junit.Test;
import org.ofbiz.core.entity.GenericValue;

import static org.apache.commons.lang.StringUtils.isEmpty;
import static org.apache.commons.lang3.SerializationUtils.deserialize;
import static org.apache.commons.lang3.SerializationUtils.serialize;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestOfBizUser {
    @Test
    public void usersShouldHaveABlankEmailAddressWhenBuiltFromAGenericValueWithANullAddress() {
        final GenericValue userAsGenericValue = mock(GenericValue.class);
        when(userAsGenericValue.getString(UserEntity.EMAIL_ADDRESS)).thenReturn(null);

        final OfBizUser userObject = OfBizUser.from(userAsGenericValue);

        assertTrue
                (
                        "The email address on the user object should be set to the empty string if the email address "
                                + "value stored in the generic value is 'null'.", isEmpty(userObject.getEmailAddress())
                );
    }

    @Test
    public void usersEmailAddressShouldBePopulatedFromTheAddressInTheGenericValueIfItIsNotNull() {
        final GenericValue userAsGenericValue = mock(GenericValue.class);
        when(userAsGenericValue.getString(UserEntity.EMAIL_ADDRESS)).thenReturn("test-user@example.com");

        final OfBizUser userObject = OfBizUser.from(userAsGenericValue);

        assertTrue
                (
                        "The email address on the user object should be set to the email address value stored in the "
                                + "generic value when it is not 'null'.",
                        userObject.getEmailAddress().equals("test-user@example.com")
                );
    }

    @Test
    public void instancesShouldBeSerializableInOrderToBeReplicatedAsCacheValues() {
        // Set up
        final GenericValue userAsGenericValue = mock(GenericValue.class);
        when(userAsGenericValue.getString(UserEntity.USER_NAME)).thenReturn("test");
        final OfBizUser user = OfBizUser.from(userAsGenericValue);

        // Invoke
        final Object deserializedUser = deserialize(serialize(user));

        // Check
        assertEquals(user, deserializedUser);
        assertNotSame(user, deserializedUser);
    }


    @Test
    public void assumeOfBizUserEqualToUserWhenHavingSameDirectoryAndUsername() {
        final User user = getUser(1L, "crowdUser");
        assertEquals(true, user.equals(getCrowdUser()));
    }


    @Test
    public void assumeOfBizUserNotEqualToUserWhenHavingOnlySameDirectory() {
        final User user = getUser(1L, "jiraUser");
        assertEquals(false, user.equals(getCrowdUser()));
    }


    @Test
    public void assumeOfBizUserNotEqualToUserWhenHavingOnlySameUsername() {
        final User user = getUser(2L, "crowduser");
        assertEquals(false, user.equals(getCrowdUser()));
    }

    @Test
    public void assumeApplicationUserEqualToUserWhenHavingOnlySameDirectory() {

        final User user = getUser(1L, "crowdUser");
        final User appUser = getUser(1L, "jiraUser");

        final ApplicationUser applicationUser = getApplicationUser(appUser);
        assertEquals(false, user.equals(applicationUser));
    }

    @Test
    public void assumeApplicationUserEqualToUserWhenHavingOnlySameUsername() {

        final User user = getUser(1L, "crowdUser");
        final User appUser = getUser(2L, "crowdUser");

        final ApplicationUser applicationUser = getApplicationUser(appUser);
        assertEquals(false, user.equals(applicationUser));
    }

    private ApplicationUser getApplicationUser(final User user) {
        return new DelegatingApplicationUser(1L, "user_key", user);
    }

    private User getUser(final long directoryId, final String username) {
        final GenericValue userAsGenericValue = mock(GenericValue.class);
        when(userAsGenericValue.getLong(UserEntity.DIRECTORY_ID)).thenReturn(directoryId);
        when(userAsGenericValue.getString(UserEntity.USER_NAME)).thenReturn(username);

        return OfBizUser.from(userAsGenericValue);
    }

    private User getCrowdUser() {

        return new User() {

            @Override
            public String getName() {
                return "CrowdUser";
            }

            @Override
            public long getDirectoryId() {
                return 1L;
            }

            @Override
            public boolean isActive() {
                return true;
            }

            @Override
            public String getEmailAddress() {
                return "crowd@user.com";
            }

            @Override
            public String getDisplayName() {
                return "Mr. Crowd User";
            }

            @Override
            public int compareTo(final User user) {
                return UserComparator.compareTo(this, user);
            }
        };
    }
}
