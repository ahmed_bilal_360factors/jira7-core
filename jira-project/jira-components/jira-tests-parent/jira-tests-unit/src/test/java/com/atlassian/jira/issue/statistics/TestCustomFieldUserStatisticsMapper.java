package com.atlassian.jira.issue.statistics;

import com.atlassian.jira.issue.customfields.searchers.transformer.CustomFieldInputHelper;
import com.atlassian.jira.issue.customfields.statistics.GroupPickerStatisticsMapper;
import com.atlassian.jira.issue.customfields.statistics.ProjectSelectStatisticsMapper;
import com.atlassian.jira.issue.customfields.statistics.TextStatisticsMapper;
import com.atlassian.jira.issue.customfields.statistics.UserPickerStatisticsMapper;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.search.ClauseNames;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.junit.rules.MockComponentContainer;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.query.Query;
import com.atlassian.query.QueryImpl;
import com.atlassian.query.clause.TerminalClauseImpl;
import com.atlassian.query.operator.Operator;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestCustomFieldUserStatisticsMapper {
    private static final String CUSTOMFIELD_10001 = "customfield_10001";
    private static final long CUSTOMFIELD_10001L = 10001l;
    private static final String CF_10001 = "cf[10001]";
    private static final String MY_CUSTOM_CF = "My Custom CF";

    @Mock
    private UserManager userManager;
    @Mock
    private CustomField customField;
    @Mock
    private CustomFieldInputHelper customFieldInputHelper;
    @Mock
    private JiraAuthenticationContext authenticationContext;

    @Rule
    public final MethodRule initMockito = MockitoJUnit.rule();

    @Rule
    public MockComponentContainer container = new MockComponentContainer(this);

    @Test
    public void equalsMethodContract() throws Exception {
        when(customField.getIdAsLong()).thenReturn(CUSTOMFIELD_10001L);
        when(customField.getId()).thenReturn(CUSTOMFIELD_10001);
        when(customField.getIdAsLong()).thenReturn(CUSTOMFIELD_10001L);
        when(customField.getId()).thenReturn(CUSTOMFIELD_10001);
        when(customField.getClauseNames()).thenReturn(new ClauseNames("test"));

        final CustomField customField1 = mock(CustomField.class);

        when(customField1.getIdAsLong()).thenReturn(10002l);
        when(customField1.getId()).thenReturn("customfield_10002");

        CustomFieldUserStatisticsMapper userStatisticsMapper = new CustomFieldUserStatisticsMapper(customField, userManager, authenticationContext, customFieldInputHelper);

        assertTrue(userStatisticsMapper.equals(userStatisticsMapper));
        assertEquals(userStatisticsMapper.hashCode(), userStatisticsMapper.hashCode());

        CustomFieldUserStatisticsMapper projectStatisticsMapper1 = new CustomFieldUserStatisticsMapper(customField, userManager, authenticationContext, customFieldInputHelper);
        // As the mappers are using the same custom field they should be equal

        assertTrue(userStatisticsMapper.equals(projectStatisticsMapper1));
        assertEquals(userStatisticsMapper.hashCode(), projectStatisticsMapper1.hashCode());

        CustomFieldUserStatisticsMapper projectStatisticsMapper2 = new CustomFieldUserStatisticsMapper(customField1, userManager, authenticationContext, customFieldInputHelper);

        // As the mappers are using different custom field they should *not* be equal
        assertFalse(userStatisticsMapper.equals(projectStatisticsMapper2));
        assertFalse(userStatisticsMapper.hashCode() == projectStatisticsMapper2.hashCode());

        assertFalse(userStatisticsMapper.equals(null));
        assertFalse(userStatisticsMapper.equals(null));
        assertFalse(userStatisticsMapper.equals(new Object()));
        assertFalse(userStatisticsMapper.equals(new IssueKeyStatisticsMapper()));
        assertFalse(userStatisticsMapper.equals(new IssueTypeStatisticsMapper(null)));
        assertFalse(userStatisticsMapper.equals(new UserPickerStatisticsMapper(null, null, null)));
        assertFalse(userStatisticsMapper.equals(new TextStatisticsMapper(null)));
        assertFalse(userStatisticsMapper.equals(new ProjectSelectStatisticsMapper(null, null)));
        assertFalse(userStatisticsMapper.equals(new GroupPickerStatisticsMapper(customField, null, authenticationContext, customFieldInputHelper)));
    }

    @Test
    public void getUrlSuffixForUniqueCustomFieldName() throws Exception {
        final SearchRequest searchRequest = mock(SearchRequest.class);
        when(searchRequest.getQuery()).thenReturn(new QueryImpl());

        final ApplicationUser user = new MockApplicationUser("fred");
        when(authenticationContext.getUser()).thenReturn(user);

        when(customField.getIdAsLong()).thenReturn(CUSTOMFIELD_10001L);
        when(customField.getId()).thenReturn(CUSTOMFIELD_10001);
        final ClauseNames clauseNames = new ClauseNames(CF_10001);
        when(customField.getClauseNames()).thenReturn(clauseNames);
        when(customField.getName()).thenReturn(MY_CUSTOM_CF);

        when(customFieldInputHelper.getUniqueClauseName(user, CF_10001, MY_CUSTOM_CF)).thenReturn(MY_CUSTOM_CF);

        CustomFieldUserStatisticsMapper userStatisticsMapper = new CustomFieldUserStatisticsMapper(customField, userManager, authenticationContext, customFieldInputHelper);

        ApplicationUser value = new MockApplicationUser("humptydumpty");

        final SearchRequest modifiedSearchRequest = userStatisticsMapper.getSearchUrlSuffix(value, searchRequest);

        final Query query = modifiedSearchRequest.getQuery();

        assertEquals(new QueryImpl(new TerminalClauseImpl(MY_CUSTOM_CF, Operator.EQUALS, "humptydumpty")), query);
    }

    @Test
    public void getUrlSuffixForNonUniqueCustomFieldName() throws Exception {
        final SearchRequest searchRequest = mock(SearchRequest.class);
        when(searchRequest.getQuery()).thenReturn(new QueryImpl());

        final ApplicationUser user = new MockApplicationUser("fred");
        when(authenticationContext.getUser()).thenReturn(user);
        when(customField.getId()).thenReturn(CUSTOMFIELD_10001);
        when(customField.getIdAsLong()).thenReturn(CUSTOMFIELD_10001L);
        final ClauseNames clauseNames = new ClauseNames(CF_10001);
        when(customField.getClauseNames()).thenReturn(clauseNames);
        when(customField.getName()).thenReturn(MY_CUSTOM_CF);

        when(customFieldInputHelper.getUniqueClauseName(user, CF_10001, MY_CUSTOM_CF)).thenReturn(CF_10001);

        CustomFieldUserStatisticsMapper userStatisticsMapper = new CustomFieldUserStatisticsMapper(customField, userManager, authenticationContext, customFieldInputHelper);

        final ApplicationUser value = new MockApplicationUser("Eggskymp");
        final SearchRequest modifiedSearchRequest = userStatisticsMapper.getSearchUrlSuffix(value, searchRequest);

        final Query query = modifiedSearchRequest.getQuery();

        assertEquals(new QueryImpl(new TerminalClauseImpl(CF_10001, Operator.EQUALS, "Eggskymp")), query);
    }

}
