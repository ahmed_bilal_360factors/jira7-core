package com.atlassian.jira.web.action.issue.bulkedit;

import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.bulkedit.operation.BulkWorkflowTransitionOperation;
import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutManager;
import com.atlassian.jira.issue.fields.screen.FieldScreenRendererFactory;
import com.atlassian.jira.issue.status.MockStatus;
import com.atlassian.jira.issue.status.Status;
import com.atlassian.jira.junit.rules.MockitoContainer;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.task.TaskManager;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.lang.Pair;
import com.atlassian.jira.web.bean.BulkEditBeanSessionHelper;
import com.atlassian.jira.workflow.IssueWorkflowManager;
import com.atlassian.jira.workflow.MockJiraWorkflow;
import com.atlassian.jira.workflow.WorkflowManager;
import com.opensymphony.workflow.loader.ActionDescriptor;
import com.opensymphony.workflow.loader.MockStepDescriptor;
import com.opensymphony.workflow.loader.StepDescriptor;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;

import java.util.Collection;
import java.util.LinkedList;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestBulkWorkflowTransition {
    @Rule
    public MockitoContainer container = new MockitoContainer(this);
    @Mock
    private SearchService searchService;
    @Mock
    private IssueWorkflowManager issueWorkflowManager;
    @Mock
    private WorkflowManager workflowManager;
    @Mock
    private IssueManager issueManager;
    @Mock
    private FieldScreenRendererFactory fieldScreenRendererFactory;
    @Mock
    private FieldManager fieldManager;
    @Mock
    private JiraAuthenticationContext authenticationContext;
    @Mock
    private FieldLayoutManager fieldLayoutManager;
    @Mock
    private BulkWorkflowTransitionOperation bulkWorkflowTransitionOperation;
    @Mock
    private PermissionManager permissionManager;
    @Mock
    private ConstantsManager constantsManager;
    @Mock
    private BulkEditBeanSessionHelper bulkEditBeanSessionHelper;
    @Mock
    private TaskManager taskManager;
    @Mock
    private I18nHelper i18nHelper;

    @Test
    public void allPossibleOriginStatusesAreReturnedForAGivenDestinationStatus() {
        WorkflowTransitionKey key = new WorkflowTransitionKey("testName", "1", "testDestinationStatus");
        MockJiraWorkflow mockWorkflow = mock(MockJiraWorkflow.class);

        ActionDescriptor actionDescriptor = mock(ActionDescriptor.class);
        when(actionDescriptor.getId()).thenReturn(Integer.parseInt(key.getActionDescriptorId()));

        Collection<ActionDescriptor> actionDescriptors = new LinkedList<>();
        actionDescriptors.add(actionDescriptor);
        when(mockWorkflow.getAllActions()).thenReturn(actionDescriptors);

        Collection<Status> expectedStatuses = new LinkedList<>();
        Collection<StepDescriptor> expectedStepDescriptions = new LinkedList<>();

        for (Pair<MockStepDescriptor, MockStatus> pair: generateStatuses(3)) {
            when(mockWorkflow.getLinkedStatus(pair.first())).thenReturn(pair.second());
            expectedStepDescriptions.add(pair.first());
            expectedStatuses.add(pair.second());
        }
        when(mockWorkflow.getStepsForTransition(actionDescriptor)).thenReturn(expectedStepDescriptions);

        when(workflowManager.getWorkflow(key.getWorkflowName())).thenReturn(mockWorkflow);

        BulkWorkflowTransition bulkWorkflowTransition = new BulkWorkflowTransition(searchService, issueWorkflowManager, workflowManager,
                issueManager, fieldScreenRendererFactory, fieldManager, authenticationContext, fieldLayoutManager,
                bulkWorkflowTransitionOperation, permissionManager, constantsManager, bulkEditBeanSessionHelper,
                taskManager, i18nHelper);

        assertEquals(expectedStatuses, bulkWorkflowTransition.getAllOriginStatusObjects(key));
    }

    private Collection<Pair<MockStepDescriptor, MockStatus>> generateStatuses(int number) {
        LinkedList<Pair<MockStepDescriptor, MockStatus>> generatedStatuses = new LinkedList<>();
        for (int i = 0; i < number; ++i) {
            generatedStatuses.add(Pair.of(new MockStepDescriptor(), new MockStatus(String.valueOf(i), "test" + i)));
        }
        return generatedStatuses;
    }
}