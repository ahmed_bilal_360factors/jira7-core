package com.atlassian.jira.issue.customfields.searchers.transformer;

import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.search.ClauseNames;
import com.atlassian.jira.issue.search.SearchContext;
import com.atlassian.jira.issue.search.searchers.transformer.FieldFlagOperandRegistry;
import com.atlassian.jira.issue.search.searchers.util.IndexedInputHelper;
import com.atlassian.jira.jql.operand.JqlOperandResolver;
import com.atlassian.jira.jql.resolver.NameResolver;
import com.atlassian.jira.jql.resolver.ProjectIndexInfoResolver;
import com.atlassian.jira.project.MockProject;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.query.Query;
import com.atlassian.query.clause.Clause;
import com.atlassian.query.clause.TerminalClauseImpl;
import com.atlassian.query.operator.Operator;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestProjectCustomFieldSearchInputTransformer {

    @Rule
    public MockitoRule mockito = MockitoJUnit.rule();

    private final String id = "cf[100]";
    @Mock
    private CustomField customField;
    @Mock
    private JqlOperandResolver jqlOperandResolver;
    @Mock
    private ProjectIndexInfoResolver projectIndexInfoResolver;
    @Mock
    private FieldFlagOperandRegistry fieldFlagOperandRegistry;
    @Mock
    private IndexedInputHelper indexedInputHelper;
    @Mock
    private NameResolver<Project> projectResolver;
    @Mock
    private ApplicationUser searcher;
    @Mock
    private CustomFieldInputHelper customFieldInputHelper;
    private ClauseNames clauseNames;

    @Before
    public void setUp() throws Exception {
        clauseNames = new ClauseNames(id);
    }

    @Test
    public void testGetParamsFromSearchRequest() throws Exception {
        final Query query = mock(Query.class);
        final SearchContext searchContext = mock(SearchContext.class);
        final ClauseNames names = clauseNames;

        final ProjectCustomFieldSearchInputTransformer transformer = new ProjectCustomFieldSearchInputTransformer(id, names, customField, jqlOperandResolver, projectIndexInfoResolver, fieldFlagOperandRegistry, projectResolver, customFieldInputHelper) {
            @Override
            IndexedInputHelper createIndexedInputHelper() {
                return indexedInputHelper;
            }
        };
        transformer.getParamsFromSearchRequest(searcher, query, searchContext);

        verify(indexedInputHelper).getAllNavigatorValuesForMatchingClauses(searcher, names, query);
    }

    @Test
    public void testGetParamsFromSearchRequestNullQuery() throws Exception {
        final SearchContext searchContext = mock(SearchContext.class);

        final ProjectCustomFieldSearchInputTransformer transformer = new ProjectCustomFieldSearchInputTransformer(id, clauseNames, customField, jqlOperandResolver, projectIndexInfoResolver, fieldFlagOperandRegistry, projectResolver, customFieldInputHelper) {
            @Override
            IndexedInputHelper createIndexedInputHelper() {
                return indexedInputHelper;
            }
        };
        transformer.getParamsFromSearchRequest(null, null, searchContext);
    }

    @Test
    public void testGetSearchClauseAllFlag() throws Exception {
        final ProjectCustomFieldSearchInputTransformer transformer = new ProjectCustomFieldSearchInputTransformer(id, clauseNames, customField, jqlOperandResolver, projectIndexInfoResolver, fieldFlagOperandRegistry, projectResolver, customFieldInputHelper);
        assertNull(transformer.createSearchClause(searcher, "-1"));
    }

    @Test
    public void testGetSearchClauseString() throws Exception {
        mockCustomField(id);

        final ProjectCustomFieldSearchInputTransformer transformer = new ProjectCustomFieldSearchInputTransformer(id, clauseNames, customField, jqlOperandResolver, projectIndexInfoResolver, fieldFlagOperandRegistry, projectResolver, customFieldInputHelper);
        final Clause result = transformer.createSearchClause(searcher, "blurble");
        final Clause exptectedResult = new TerminalClauseImpl(id, Operator.EQUALS, "blurble");
        assertEquals(exptectedResult, result);
    }

    @Test
    public void testGetSearchClauseLongIsProject() throws Exception {
        when(projectResolver.get(789L)).thenReturn(new MockProject(789L, "MKY"));
        mockCustomField(id);

        final ProjectCustomFieldSearchInputTransformer transformer = new ProjectCustomFieldSearchInputTransformer(id, clauseNames, customField, jqlOperandResolver, projectIndexInfoResolver, fieldFlagOperandRegistry, projectResolver, customFieldInputHelper);
        final Clause result = transformer.createSearchClause(searcher, "789");
        final Clause exptectedResult = new TerminalClauseImpl(id, Operator.EQUALS, "MKY");
        assertEquals(exptectedResult, result);
    }

    @Test
    public void testGetSearchClauseLongIsNotProject() throws Exception {
        when(projectResolver.get(789L)).thenReturn(null);
        mockCustomField(id);

        final ProjectCustomFieldSearchInputTransformer transformer = new ProjectCustomFieldSearchInputTransformer(id, clauseNames, customField, jqlOperandResolver, projectIndexInfoResolver, fieldFlagOperandRegistry, projectResolver, customFieldInputHelper);
        final Clause result = transformer.createSearchClause(searcher, "789");
        final Clause exptectedResult = new TerminalClauseImpl(id, Operator.EQUALS, 789L);
        assertEquals(exptectedResult, result);
    }

    private void mockCustomField(final String fieldId) {
        when(customField.getUntranslatedName()).thenReturn(fieldId);
        when(customFieldInputHelper.getUniqueClauseName(searcher, fieldId, fieldId)).thenReturn(fieldId);
    }

}
