package com.atlassian.jira.web.action;

import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.JiraWebActionSupportDependencies;
import com.atlassian.jira.junit.rules.MockitoContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.plugin.myjirahome.MyJiraHomeLinker;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.login.LoginManagerImpl;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.web.landingpage.LandingPageRedirectManager;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.Mock;
import org.mockito.internal.verification.Times;
import webwork.action.ServletActionContext;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(JUnit4.class)
public class TestMyJiraHome {
    @Rule
    public final MockitoContainer mockContainer = MockitoMocksInContainer.rule(this);
    @Rule
    public TestRule webActionSupport = JiraWebActionSupportDependencies.build(mockContainer);

    @Mock
    private MyJiraHomeLinker myJiraHomeLinker;
    @Mock
    @AvailableInContainer
    private JiraAuthenticationContext mockJiraAuthenticationContext;
    @Mock
    private ApplicationUser mockUser;
    @Mock
    private UserManager userManager;
    @Mock
    private HttpServletRequest httpRequest;
    @Mock
    private HttpServletResponse httpResponse;

    @Mock
    private LandingPageRedirectManager landingPageRedirectManager;

    private MyJiraHome action;

    @Before
    public void setUpMocks() {
        ServletActionContext.setResponse(httpResponse);

        action = new MyJiraHome(myJiraHomeLinker, userManager, landingPageRedirectManager) {
            public HttpServletRequest getHttpRequest() {
                return httpRequest;
            }

            @Override
            public HttpServletResponse getHttpResponse() {
                return httpResponse;
            }
        };
    }

    @Test
    public void testExecuteAsAnonymousUser() throws Exception {
        expectAnonymousUser();

        action.doExecute();

        verify(myJiraHomeLinker, new Times(0)).getDefaultUserHome();
        verifyRedirect(MyJiraHomeLinker.DEFAULT_HOME_OD_ANON);
    }


    @Test
    public void testExecuteAsAuthenticatedUser() throws Exception {
        expectAuthenticatedUser();

        action.doExecute();

        verifyRedirect(MyJiraHomeLinker.DEFAULT_HOME_NOT_ANON);
    }

    @Test
    public void testExecuteAsAuthenticatedNonJiraUser() throws Exception {
        expectAuthenticatedNonJiraUser();

        action.doExecute();

        verify(myJiraHomeLinker).getDefaultUserHome();
        verifyRedirect(MyJiraHomeLinker.DEFAULT_HOME_NOT_ANON);
    }

    @Test
    public void testRedirectToLocationReturnedByLandingPageRedirect() throws Exception {
        expectAuthenticatedNonJiraUser();
        when(landingPageRedirectManager.redirectUrl(null)).thenReturn(Optional.of("/pageRedir"));

        action.doExecute();

        verifyRedirect("/pageRedir");
    }

    @Test
    public void shouldForwardWhitelistedPaths() throws Exception {
        shouldForward("/secure/Dashboard.jspa");
        shouldForward("/secure/IssueNavigator.jspa");
        shouldForward("/secure/GreenHopper.jspa");
    }

    private void shouldForward(final String path) throws Exception {
        when(mockJiraAuthenticationContext.getUser()).thenReturn(mockUser);
        when(landingPageRedirectManager.redirectUrl(mockUser)).thenReturn(Optional.of(path));

        action.doExecute();

        verifyRedirect(path);
    }

    private void expectAnonymousUser() {
        final ApplicationUser user = null;
        when(mockJiraAuthenticationContext.getUser()).thenReturn(user);
        when(landingPageRedirectManager.redirectUrl(user)).thenReturn(Optional.empty());
        when(myJiraHomeLinker.getHomeLink(user)).thenReturn(MyJiraHomeLinker.DEFAULT_HOME_OD_ANON);
        when(httpRequest.getAttribute(LoginManagerImpl.AUTHORISED_FAILURE)).thenReturn(false);
    }

    private void expectAuthenticatedUser() {
        when(mockJiraAuthenticationContext.getUser()).thenReturn(mockUser);
        when(landingPageRedirectManager.redirectUrl(mockUser)).thenReturn(Optional.empty());
        when(myJiraHomeLinker.getHomeLink(mockUser)).thenReturn(MyJiraHomeLinker.DEFAULT_HOME_NOT_ANON);
    }

    private void expectAuthenticatedNonJiraUser() {
        String testUserKey = "testUser:1234";
        final ApplicationUser user = null;
        when(mockJiraAuthenticationContext.getUser()).thenReturn(user);
        when(landingPageRedirectManager.redirectUrl(user)).thenReturn(Optional.empty());
        when(httpRequest.getAttribute(LoginManagerImpl.AUTHORISED_FAILURE)).thenReturn(true);
        when(httpRequest.getAttribute(LoginManagerImpl.AUTHORISING_USER_KEY)).thenReturn(testUserKey);
        when(userManager.getUserByKey(testUserKey)).thenReturn(mockUser);
        when(myJiraHomeLinker.getDefaultUserHome()).thenReturn(MyJiraHomeLinker.DEFAULT_HOME_NOT_ANON);
    }

    private void verifyRedirect(final String expectedUrl) throws IOException {
        verify(httpResponse).sendRedirect(eq(expectedUrl));
    }

    public HttpServletRequest getHttpRequest() {
        return httpRequest;
    }
}
