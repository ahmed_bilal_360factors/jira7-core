package com.atlassian.jira.imports.project.handler;

import com.atlassian.jira.external.beans.ExternalVoter;
import com.atlassian.jira.external.beans.ExternalWatcher;
import com.atlassian.jira.imports.project.ProjectImportPersister;
import com.atlassian.jira.imports.project.core.BackupSystemInformation;
import com.atlassian.jira.imports.project.core.ProjectImportResultsImpl;
import com.atlassian.jira.imports.project.mapper.ProjectImportMapper;
import com.atlassian.jira.imports.project.mapper.ProjectImportMapperImpl;
import com.atlassian.jira.imports.project.parser.UserAssociationParser;
import com.atlassian.jira.imports.project.transformer.VoterTransformer;
import com.atlassian.jira.imports.project.transformer.WatcherTransformer;
import com.atlassian.jira.web.bean.MockI18nBean;
import org.junit.Test;

import static com.atlassian.jira.imports.project.parser.UserAssociationParser.USER_ASSOCIATION_ENTITY_NAME;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @since v3.13
 */
public class TestUserAssociationPersisterHandler {
    ProjectImportMapper projectImportMapper = new ProjectImportMapperImpl(null, null);

    @Test
    public void testHandleVoter() throws Exception {
        ExternalVoter externalVoter = new ExternalVoter();
        externalVoter.setIssueId("12");
        externalVoter.setVoter("admin");

        final UserAssociationParser mockUserAssociationParser = mock(UserAssociationParser.class);
        when(mockUserAssociationParser.parseVoter(null)).thenReturn(externalVoter);
        when(mockUserAssociationParser.parseWatcher(null)).thenReturn(null);

        final VoterTransformer mockVoterTransformer = mock(VoterTransformer.class);
        when(mockVoterTransformer.transform(projectImportMapper, externalVoter)).thenReturn(externalVoter);

        final ProjectImportPersister mockProjectImportPersister = mock(ProjectImportPersister.class);
        when(mockProjectImportPersister.createVoter(externalVoter)).thenReturn(true);

        final ProjectImportResultsImpl projectImportResults = new ProjectImportResultsImpl(0, 0, 0, 0, null);
        UserAssociationPersisterHandler userAssociationPersisterHandler = new UserAssociationPersisterHandler(mockProjectImportPersister, projectImportMapper, projectImportResults, null, new ExecutorForTests()) {
            UserAssociationParser getUserAssociationParser() {
                return mockUserAssociationParser;
            }

            VoterTransformer getVoterTransformer() {
                return mockVoterTransformer;
            }
        };

        userAssociationPersisterHandler.handleEntity(USER_ASSOCIATION_ENTITY_NAME, null);
        userAssociationPersisterHandler.handleEntity("NOTVoter", null);

        assertEquals(0, projectImportResults.getErrors().size());
    }

    @Test
    public void testHandleVoterSaveError() throws Exception {
        ExternalVoter externalVoter = new ExternalVoter();
        externalVoter.setIssueId("12");
        externalVoter.setVoter("admin");

        final UserAssociationParser mockUserAssociationParser = mock(UserAssociationParser.class);
        when(mockUserAssociationParser.parseVoter(null)).thenReturn(externalVoter);
        when(mockUserAssociationParser.parseWatcher(null)).thenReturn(null);

        final VoterTransformer mockVoterTransformer = mock(VoterTransformer.class);
        when(mockVoterTransformer.transform(projectImportMapper, externalVoter)).thenReturn(externalVoter);

        final ProjectImportPersister mockProjectImportPersister = mock(ProjectImportPersister.class);
        when(mockProjectImportPersister.createVoter(externalVoter)).thenReturn(false);

        final BackupSystemInformation mockBackupSystemInformation = mock(BackupSystemInformation.class);
        when(mockBackupSystemInformation.getIssueKeyForId("12")).thenReturn("TST-1");

        final ProjectImportResultsImpl projectImportResults = new ProjectImportResultsImpl(0, 0, 0, 0, new MockI18nBean());
        UserAssociationPersisterHandler userAssociationPersisterHandler = new UserAssociationPersisterHandler(mockProjectImportPersister, projectImportMapper, projectImportResults, mockBackupSystemInformation, new ExecutorForTests()) {
            UserAssociationParser getUserAssociationParser() {
                return mockUserAssociationParser;
            }

            VoterTransformer getVoterTransformer() {
                return mockVoterTransformer;
            }
        };

        userAssociationPersisterHandler.handleEntity(USER_ASSOCIATION_ENTITY_NAME, null);
        userAssociationPersisterHandler.handleEntity("NOTVoter", null);

        assertEquals(1, projectImportResults.getErrors().size());
        assertTrue(projectImportResults.getErrors().contains("There was a problem saving voter 'admin' for issue 'TST-1'."));
    }

    @Test
    public void testHandleVoterNullTransformed() throws Exception {
        ExternalVoter externalVoter = new ExternalVoter();
        externalVoter.setIssueId("12");
        externalVoter.setVoter("admin");

        ExternalVoter transformedExternalVoter = new ExternalVoter();
        transformedExternalVoter.setVoter("admin");

        final UserAssociationParser mockUserAssociationParser = mock(UserAssociationParser.class);
        when(mockUserAssociationParser.parseVoter(null)).thenReturn(externalVoter);
        when(mockUserAssociationParser.parseWatcher(null)).thenReturn(null);

        final VoterTransformer mockVoterTransformer = mock(VoterTransformer.class);
        when(mockVoterTransformer.transform(projectImportMapper, externalVoter)).
                thenReturn(transformedExternalVoter);

        final ProjectImportPersister mockProjectImportPersister = mock(ProjectImportPersister.class);

        final BackupSystemInformation mockBackupSystemInformation = mock(BackupSystemInformation.class);
        when(mockBackupSystemInformation.getIssueKeyForId("12")).thenReturn("TST-1");

        final ProjectImportResultsImpl projectImportResults = new ProjectImportResultsImpl(0, 0, 0, 0, null);
        UserAssociationPersisterHandler userAssociationPersisterHandler = new UserAssociationPersisterHandler(mockProjectImportPersister, projectImportMapper, projectImportResults, mockBackupSystemInformation, null) {
            UserAssociationParser getUserAssociationParser() {
                return mockUserAssociationParser;
            }

            VoterTransformer getVoterTransformer() {
                return mockVoterTransformer;
            }
        };

        userAssociationPersisterHandler.handleEntity(USER_ASSOCIATION_ENTITY_NAME, null);
        userAssociationPersisterHandler.handleEntity("NOTVoter", null);

    }

    @Test
    public void testHandleWatcher() throws Exception {
        ExternalWatcher externalWatcher = new ExternalWatcher();
        externalWatcher.setIssueId("12");
        externalWatcher.setWatcher("admin");

        final UserAssociationParser mockUserAssociationParser = mock(UserAssociationParser.class);
        when(mockUserAssociationParser.parseVoter(null)).thenReturn(null);
        when(mockUserAssociationParser.parseWatcher(null)).thenReturn(externalWatcher);

        final WatcherTransformer mockWatcherTransformer = mock(WatcherTransformer.class);
        when(mockWatcherTransformer.transform(projectImportMapper, externalWatcher)).
                thenReturn(externalWatcher);

        final ProjectImportPersister mockProjectImportPersister = mock(ProjectImportPersister.class);
        when(mockProjectImportPersister.createWatcher(externalWatcher)).thenReturn(true);

        final ProjectImportResultsImpl projectImportResults = new ProjectImportResultsImpl(0, 0, 0, 0, null);
        UserAssociationPersisterHandler userAssociationPersisterHandler = new UserAssociationPersisterHandler(mockProjectImportPersister, projectImportMapper, projectImportResults, null, new ExecutorForTests()) {
            UserAssociationParser getUserAssociationParser() {
                return mockUserAssociationParser;
            }

            WatcherTransformer getWatcherTransformer() {
                return mockWatcherTransformer;
            }
        };

        userAssociationPersisterHandler.handleEntity(USER_ASSOCIATION_ENTITY_NAME, null);
        userAssociationPersisterHandler.handleEntity("NOTVoter", null);

        assertEquals(0, projectImportResults.getErrors().size());
    }

    @Test
    public void testHandleWatcherNullTransformed() throws Exception {
        ExternalWatcher externalWatcher = new ExternalWatcher();
        externalWatcher.setIssueId("12");
        externalWatcher.setWatcher("admin");

        ExternalWatcher transformedExternalWatcher = new ExternalWatcher();
        transformedExternalWatcher.setWatcher("admin");

        final UserAssociationParser mockUserAssociationParser = mock(UserAssociationParser.class);
        when(mockUserAssociationParser.parseVoter(null)).thenReturn(null);
        when(mockUserAssociationParser.parseWatcher(null)).thenReturn(externalWatcher);

        final WatcherTransformer mockWatcherTransformer = mock(WatcherTransformer.class);
        when(mockWatcherTransformer.transform(projectImportMapper, externalWatcher)).
                thenReturn(transformedExternalWatcher);

        final ProjectImportPersister mockProjectImportPersister = mock(ProjectImportPersister.class);

        final BackupSystemInformation mockBackupSystemInformation = mock(BackupSystemInformation.class);
        when(mockBackupSystemInformation.getIssueKeyForId("12")).thenReturn("TST-1");

        final ProjectImportResultsImpl projectImportResults = new ProjectImportResultsImpl(0, 0, 0, 0, null);
        UserAssociationPersisterHandler userAssociationPersisterHandler = new UserAssociationPersisterHandler(mockProjectImportPersister, projectImportMapper, projectImportResults, mockBackupSystemInformation, null) {
            UserAssociationParser getUserAssociationParser() {
                return mockUserAssociationParser;
            }

            WatcherTransformer getWatcherTransformer() {
                return mockWatcherTransformer;
            }
        };

        userAssociationPersisterHandler.handleEntity(USER_ASSOCIATION_ENTITY_NAME, null);
        userAssociationPersisterHandler.handleEntity("NOTVoter", null);

    }

    @Test
    public void testHandleWatcherSaveError() throws Exception {
        ExternalWatcher externalWatcher = new ExternalWatcher();
        externalWatcher.setIssueId("12");
        externalWatcher.setWatcher("admin");

        final UserAssociationParser mockUserAssociationParser = mock(UserAssociationParser.class);
        when(mockUserAssociationParser.parseVoter(null)).thenReturn(null);
        when(mockUserAssociationParser.parseWatcher(null)).thenReturn(externalWatcher);

        final WatcherTransformer mockWatcherTransformer = mock(WatcherTransformer.class);
        when(mockWatcherTransformer.transform(projectImportMapper, externalWatcher)).
                thenReturn(externalWatcher);

        final ProjectImportPersister mockProjectImportPersister = mock(ProjectImportPersister.class);
        when(mockProjectImportPersister.createWatcher(externalWatcher)).thenReturn(false);

        final BackupSystemInformation mockBackupSystemInformation = mock(BackupSystemInformation.class);
        when(mockBackupSystemInformation.getIssueKeyForId("12")).thenReturn("TST-1");

        final ProjectImportResultsImpl projectImportResults = new ProjectImportResultsImpl(0, 0, 0, 0, new MockI18nBean());
        UserAssociationPersisterHandler userAssociationPersisterHandler = new UserAssociationPersisterHandler(mockProjectImportPersister, projectImportMapper, projectImportResults, mockBackupSystemInformation, new ExecutorForTests()) {
            UserAssociationParser getUserAssociationParser() {
                return mockUserAssociationParser;
            }

            WatcherTransformer getWatcherTransformer() {
                return mockWatcherTransformer;
            }
        };

        userAssociationPersisterHandler.handleEntity(USER_ASSOCIATION_ENTITY_NAME, null);
        userAssociationPersisterHandler.handleEntity("NOTVoter", null);

        assertEquals(1, projectImportResults.getErrors().size());
        assertTrue(projectImportResults.getErrors().contains("There was a problem saving watcher 'admin' for issue 'TST-1'."));
    }

}
