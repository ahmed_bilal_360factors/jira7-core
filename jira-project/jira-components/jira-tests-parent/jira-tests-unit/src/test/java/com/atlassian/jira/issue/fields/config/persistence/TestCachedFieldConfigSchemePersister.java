package com.atlassian.jira.issue.fields.config.persistence;

import com.atlassian.cache.CacheManager;
import com.atlassian.cache.CachedReference;
import com.atlassian.cache.memory.MemoryCacheManager;
import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.database.MockQueryDslAccessor;
import com.atlassian.jira.database.ResultRow;
import com.atlassian.jira.issue.context.persistence.FieldConfigContextPersister;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.issue.fields.config.FieldConfigScheme;
import com.atlassian.jira.issue.fields.config.manager.FieldConfigManager;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.mockito.Mock;

import java.util.Collections;
import java.util.concurrent.atomic.AtomicInteger;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestCachedFieldConfigSchemePersister {

    @Rule
    public RuleChain mockitoMocksInContainer = MockitoMocksInContainer.forTest(this);

    @Mock
    OfBizDelegator ofBizDelegator;

    @Mock
    ConstantsManager constantsManager;

    @Mock
    FieldConfigPersister fieldConfigPersister;

    @Mock
    FieldConfig fieldConfig;

    @Mock
    FieldConfigScheme configScheme;

    @Mock
    FieldConfigManager fieldConfigManager;

    MockQueryDslAccessor queryDslAccessor = new MockQueryDslAccessor();

    private CacheManager cacheManager = new MemoryCacheManager();

    @Test
    public void testGetConfigSchemeForFieldConfig() throws Exception {
        final Long fieldConfigId = 10L;
        final Long schemeId = 200L;

        when(fieldConfig.getId()).thenReturn(fieldConfigId);
        when(fieldConfigManager.getFieldConfig(10L)).thenReturn(fieldConfig);

        queryDslAccessor.setQueryResults(
                "select distinct FIELD_CONFIG_SCHEME_ISSUE_TYPE.fieldconfigscheme\n"
                        + "from fieldconfigschemeissuetype FIELD_CONFIG_SCHEME_ISSUE_TYPE\n"
                        + "where FIELD_CONFIG_SCHEME_ISSUE_TYPE.fieldconfiguration = " + fieldConfigId +"\n"
                        + "limit 2",
                Collections.singletonList(new ResultRow(schemeId)));

        final AtomicInteger called = new AtomicInteger(0);
        final CachedFieldConfigSchemePersister persister = new CachedFieldConfigSchemePersister(ofBizDelegator, queryDslAccessor, constantsManager,
                fieldConfigPersister, null, fieldConfigManager, cacheManager) {
            @Override
            public FieldConfigScheme getFieldConfigScheme(final Long configSchemeId) {
                called.incrementAndGet();
                assertThat("Retrieved value is correct", configSchemeId, is(schemeId));
                return configScheme;
            }
        };

        persister.getConfigSchemeForFieldConfig(fieldConfig);
        persister.getConfigSchemeForFieldConfig(fieldConfig);

        assertThat("The config scheme is cached", called.get(), is(1));
    }
}
