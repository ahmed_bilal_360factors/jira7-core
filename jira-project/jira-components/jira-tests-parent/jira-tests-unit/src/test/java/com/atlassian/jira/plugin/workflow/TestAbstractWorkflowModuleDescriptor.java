package com.atlassian.jira.plugin.workflow;

import com.atlassian.jira.plugin.ComponentClassManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.workflow.OSWorkflowConfigurator;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginState;
import com.atlassian.plugin.module.ModuleFactory;
import com.opensymphony.workflow.loader.AbstractDescriptor;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.hamcrest.Matchers;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mockito;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

/**
 * @since v4.1.1
 */
public class TestAbstractWorkflowModuleDescriptor {
    @Rule
    public ExpectedException expectedException = ExpectedException.none();
    private static final String IMP_CLASS_NAME = "com.atlassian.jira.workflow.condition.AllowOnlyReporter";
    private static final String MODULE_CLASS_NAME = "com.atlassian.jira.plugin.workflow.WorkflowAllowOnlyReporterConditionFactoryImpl";
    private static final String MODULE_KEY = "MODULEKEY";

    @Test
    public void testEnableDisableTypeResolverLifecycle() throws Exception {
        final OSWorkflowConfigurator osWorkflowConfigurator = mock(OSWorkflowConfigurator.class);


        final Plugin testPlugin = mock(Plugin.class);

        when(testPlugin.getKey()).thenReturn("TestPluginKey");
        when(testPlugin.<Object>loadClass(IMP_CLASS_NAME, MyWorkflowModuleDescriptor.class)).thenReturn(null);
        when(testPlugin.<Object>loadClass(MODULE_CLASS_NAME, null)).thenReturn(Object.class);

        final MyWorkflowModuleDescriptor moduleDescriptor = new MyWorkflowModuleDescriptor(null, osWorkflowConfigurator, null);

        moduleDescriptor.init(testPlugin, getModuleDescriptionElement(IMP_CLASS_NAME, MODULE_CLASS_NAME));

        moduleDescriptor.enabled();
        verify(osWorkflowConfigurator).registerTypeResolver(
                Mockito.eq(IMP_CLASS_NAME),
                Mockito.argThat(Matchers.instanceOf(AbstractWorkflowModuleDescriptor.PluginTypeResolver.class)));


        moduleDescriptor.disabled();

        verify(osWorkflowConfigurator).unregisterTypeResolver(
                Mockito.eq(IMP_CLASS_NAME),
                Mockito.argThat(Matchers.instanceOf(AbstractWorkflowModuleDescriptor.PluginTypeResolver.class)));

        verifyNoMoreInteractions(osWorkflowConfigurator);
    }

    @Test
    public void testPluginTypeResolverNullPluginThrowsIse() throws Exception {
        final Plugin overridePlugin = null;
        final MyWorkflowModuleDescriptor moduleDescriptor = new MyWorkflowModuleDescriptor(null, null, null, overridePlugin, MODULE_KEY, null);
        final AbstractWorkflowModuleDescriptor.PluginTypeResolver resolver = moduleDescriptor.createPluginTypeResolver();

        expectedException.expect(IllegalStateException.class);
        resolver.loadObject(IMP_CLASS_NAME);
    }

    @Test
    public void testPluginTypeResolverPluginIsDisabledReturnsNull() throws Exception {
        final Plugin overridePlugin = mock(Plugin.class);

        when(overridePlugin.getPluginState()).thenReturn(PluginState.DISABLED);

        final MyWorkflowModuleDescriptor moduleDescriptor = new MyWorkflowModuleDescriptor(null, null, null, overridePlugin, MODULE_KEY, null);
        final AbstractWorkflowModuleDescriptor.PluginTypeResolver resolver = moduleDescriptor.createPluginTypeResolver();

        assertNull(resolver.loadObject(IMP_CLASS_NAME));
    }

    @Test
    public void testPluginTypeResolverPluginIsEnabled() throws Exception {
        final Plugin overridePlugin = mock(Plugin.class);

        when(overridePlugin.getPluginState()).thenReturn(PluginState.ENABLED);

        final ComponentClassManager componentClassManager = mock(ComponentClassManager.class);

        final Object expectedInstance = mock(Object.class);
        when(componentClassManager.newInstanceFromPlugin(Object.class, overridePlugin)).thenReturn(expectedInstance);

        final MyWorkflowModuleDescriptor moduleDescriptor = new MyWorkflowModuleDescriptor(null, null, componentClassManager, overridePlugin, MODULE_KEY, Object.class);
        final AbstractWorkflowModuleDescriptor.PluginTypeResolver resolver = moduleDescriptor.createPluginTypeResolver();

        assertSame(expectedInstance, resolver.loadObject(IMP_CLASS_NAME));
    }

    private Element getModuleDescriptionElement(final String conditionClassName, final String moduleClassName) throws Exception {
        return DocumentHelper.parseText(
                "<workflow-condition key=\"onlyreporter-condition\" name=\"Only Reporter Condition\" i18n-name-key=\"admin.workflow.condition.onlyreporter.display.name\" class=\"" + moduleClassName + "\">\n"
                        + "        <description key=\"admin.workflow.condition.onlyreporter\">Condition to allow only the reporter to execute a transition.</description>\n"
                        + "        <condition-class>" + conditionClassName + "</condition-class>\n"
                        + "        <resource type=\"velocity\" name=\"view\" location=\"templates/jira/workflow/com/atlassian/jira/plugin/onlyreporter-condition-view.vm\"/>\n"
                        + "    </workflow-condition>").getRootElement();
    }

    private class MyWorkflowModuleDescriptor extends AbstractWorkflowModuleDescriptor<Object> {
        private Plugin overridePlugin;
        private String overrideKey;
        private Class<Object> overridenImplementationClass;

        // used by instantiate() - do not delete
        @SuppressWarnings({"UnusedDeclaration"})
        private MyWorkflowModuleDescriptor(final JiraAuthenticationContext authenticationContext,
                                           final OSWorkflowConfigurator workflowConfigurator, final ComponentClassManager componentClassManager) {
            super(authenticationContext, workflowConfigurator, componentClassManager, ModuleFactory.LEGACY_MODULE_FACTORY);
        }

        private MyWorkflowModuleDescriptor(final JiraAuthenticationContext authenticationContext,
                                           final OSWorkflowConfigurator workflowConfigurator, final ComponentClassManager componentClassManager,
                                           final Plugin overridePlugin, final String overrideKey, final Class<Object> overridenImplementationClass) {
            super(authenticationContext, workflowConfigurator, componentClassManager, ModuleFactory.LEGACY_MODULE_FACTORY);
            this.overridePlugin = overridePlugin;
            this.overrideKey = overrideKey;
            this.overridenImplementationClass = overridenImplementationClass;
        }

        @Override
        public Class<Object> getImplementationClass() {
            if (overridenImplementationClass != null) {
                return overridenImplementationClass;
            }
            return super.getImplementationClass();
        }

        @Override
        public Plugin getPlugin() {
            if (overridePlugin != null) {
                return overridePlugin;
            }
            return super.getPlugin();
        }

        @Override
        public String getKey() {
            if (overrideKey != null) {
                return overrideKey;
            }
            return super.getKey();
        }

        @Override
        protected String getParameterName() {
            return "condition-class";
        }

        @Override
        public String getHtml(final String resourceName, final AbstractDescriptor descriptor) {
            return null;
        }

        @Override
        public boolean isOrderable() {
            return false;
        }

        @Override
        public boolean isUnique() {
            return false;
        }

        @Override
        public boolean isDeletable() {
            return false;
        }

        @Override
        public boolean isAddable(final String actionType) {
            return false;
        }
    }
}
