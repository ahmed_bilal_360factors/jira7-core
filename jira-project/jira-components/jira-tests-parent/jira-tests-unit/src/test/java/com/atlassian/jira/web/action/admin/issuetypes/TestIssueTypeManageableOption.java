package com.atlassian.jira.web.action.admin.issuetypes;

import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.util.Arrays;
import java.util.Collection;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 *
 */
public class TestIssueTypeManageableOption {
    @Rule
    public TestRule getRule() {
        return MockitoMocksInContainer.rule(this);
    }

    @Mock
    private ConstantsManager constantsManager;

    @InjectMocks
    private IssueTypeManageableOption manageableOption;

    @Test
    public void testGetAllOptionsOrdering(){

        IssueType t1 = taskType("Heroic mission on Mars");
        IssueType t2 = taskType("another Heroic mission on Mars");
        IssueType t3 = taskType("Heroic mission on Bruce Willis Asteroid");
        IssueType t4 = taskType("Tell interesting story, Mark");
        IssueType t5 = taskType("Kill all humans task");

        IssueType s1 = subtaskType("And other lifeforms");
        IssueType s2 = subtaskType("a little but important subtask");
        IssueType s3 = subtaskType("Also kill all mutants");

        when(constantsManager.getAllIssueTypeObjects())
                .thenReturn(Arrays.asList(t1, t2, t3, t4, t5, s1, s2, s3));

        Assert.assertThat((Collection<IssueType>)manageableOption.getAllOptions(), Matchers.contains(t3, t1, t5, t4, t2, s3, s1, s2));

    }

    private IssueType taskType(String name){
        IssueType issueType = mock(IssueType.class);
        when(issueType.isSubTask()).thenReturn(false);
        when(issueType.getName()).thenReturn(name);
        when(issueType.toString()).thenReturn("task '" + name + "'");
        return issueType;
    }

    private IssueType subtaskType(String name){
        IssueType issueType = mock(IssueType.class);
        when(issueType.isSubTask()).thenReturn(true);
        when(issueType.getName()).thenReturn(name);
        when(issueType.toString()).thenReturn("subtask '" + name + "'");
        return issueType;
    }
}
