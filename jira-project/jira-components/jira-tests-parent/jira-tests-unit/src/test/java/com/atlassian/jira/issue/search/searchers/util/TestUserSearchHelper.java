package com.atlassian.jira.issue.search.searchers.util;

import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.jira.bc.user.search.UserSearchParams;
import com.atlassian.jira.bc.user.search.UserSearchService;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockitoContainer;
import com.atlassian.jira.junit.rules.MockitoMocksInContainer;
import com.atlassian.jira.mock.i18n.MockI18nHelper;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.MockGroup;
import com.atlassian.jira.user.UserFilter;
import com.atlassian.jira.user.UserHistoryItem;
import com.atlassian.jira.user.UserHistoryManager;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.I18nHelper;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import org.apache.commons.collections.CollectionUtils;
import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static com.google.common.collect.ImmutableList.copyOf;
import static com.google.common.collect.Lists.newArrayList;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.hasKey;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.core.IsCollectionContaining.hasItems;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.argThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isNull;
import static org.mockito.Mockito.when;

/**
 * Tests for {@link com.atlassian.jira.issue.search.searchers.renderer.AbstractUserSearchRenderer}.
 *
 * @since v5.2
 */
@RunWith(MockitoJUnitRunner.class)
public class TestUserSearchHelper {

    private UserSearcherHelperImpl concreteUserSearchHelper;
    @Mock
    private GroupManager groupManager;
    @Mock
    private PermissionManager permissionManager;
    @Mock
    private UserHistoryManager userHistoryManager;
    @Mock
    private ApplicationUser user;
    @Mock
    private UserManager userManager;
    @Mock
    private UserSearchService userSearchService;

    @Rule
    public MockitoContainer mockitoContainer = MockitoMocksInContainer.rule(this);

    @Mock
    @AvailableInContainer
    private I18nHelper.BeanFactory beanFactory;
    private MockI18nHelper i18nHelper = new MockI18nHelper();

    @Before
    public void setUp() {
        concreteUserSearchHelper = new UserSearcherHelperImpl(groupManager, permissionManager,
                userManager, userHistoryManager, userSearchService);
        when(user.getName()).thenReturn("user");
        when(beanFactory.getInstance(any(ApplicationUser.class))).thenReturn(i18nHelper);
    }

    /**
     * If the user is logged in, we should suggest groups they're a member of.
     */
    @Test
    public void testGetSuggestedGroups() {
        List<Group> groups = Lists.<Group>newArrayList(
                new MockGroup("1"), new MockGroup("2"), new MockGroup("3"),
                new MockGroup("4"), new MockGroup("5"), new MockGroup("6"));

        when(groupManager.getGroupsForUser(user)).thenReturn(groups);

        assertEquals(groups.subList(0, 5), concreteUserSearchHelper.getSuggestedGroups(user));
    }

    /**
     * If the user isn't logged in and anonymous users don't have browse
     * permission, we shouldn't suggest any groups.
     */
    @Test
    public void testGetSuggestedGroupsAnonymous() {
        //noinspection deprecation
        when(permissionManager.hasPermission(Permissions.USER_PICKER, null)).thenReturn(false);
        assertNull(concreteUserSearchHelper.getSuggestedGroups(null));
    }

    /**
     * If the user isn't logged in but anonymous users have browse permission,
     * we should suggest the first 5 groups in the system sorted alphabetically.
     */
    @Test
    public void testGetSuggestedGroupsAnonymousBrowsePermission() {
        List<Group> groups = Lists.<Group>newArrayList(
                new MockGroup("1"), new MockGroup("2"), new MockGroup("3"),
                new MockGroup("4"), new MockGroup("5"), new MockGroup("6"));

        when(groupManager.getAllGroups()).thenReturn(groups);
        when(permissionManager.hasPermission(Permissions.USER_PICKER, null)).thenReturn(true);
        assertEquals(groups.subList(0, 5), concreteUserSearchHelper.getSuggestedGroups(null));
    }

    /**
     * If the user has browse permission, we should suggest the first 5 users in
     * the system, sorted alphabetically.
     */
    @Test
    public void testGetSuggestedUsers() {
        List<ApplicationUser> users = Lists.<ApplicationUser>newArrayList(
                new MockApplicationUser("1"), new MockApplicationUser("2"), new MockApplicationUser("3"),
                new MockApplicationUser("4"), new MockApplicationUser("5"), new MockApplicationUser("6"));

        when(permissionManager.hasPermission(Permissions.USER_PICKER, user)).thenReturn(true);
        when(userSearchService.findUsers(eq(""), any(UserSearchParams.class))).thenReturn(users);
        when(userSearchService.userMatches(any(ApplicationUser.class), any(UserSearchParams.class))).thenReturn(true);
        final List<ApplicationUser> applicationUsers = copyOf(users.subList(0, 5));
        assertThat(concreteUserSearchHelper.getSuggestedUsers(user, new ArrayList<>(), UserSearchParams.ACTIVE_USERS_ALLOW_EMPTY_QUERY),
                hasItems(applicationUsers.toArray(new ApplicationUser[applicationUsers.size()])));
    }

    /**
     * If the user has USED_USER history, then we should expect to see that in the results
     */
    @Test
    public void testGetSuggestedUsersWithUsedUserHistory() {
        MockApplicationUser u1 = new MockApplicationUser("1");
        MockApplicationUser u2 = new MockApplicationUser("2");
        MockApplicationUser u3 = new MockApplicationUser("3");
        MockApplicationUser u4 = new MockApplicationUser("4");
        MockApplicationUser u5 = new MockApplicationUser("5");
        MockApplicationUser u6 = new MockApplicationUser("6");
        MockApplicationUser u7 = new MockApplicationUser("7");
        List<ApplicationUser> allUsers = Lists.<ApplicationUser>newArrayList(u1, u2, u3, u4, u5, u6, u7);

        when(permissionManager.hasPermission(Permissions.USER_PICKER, user)).thenReturn(true);
        List<UserHistoryItem> usedusers = Lists.newArrayList(
                new UserHistoryItem(UserHistoryItem.USED_USER, "2"), new UserHistoryItem(UserHistoryItem.USED_USER, "5"));
        when(userManager.getUserByName("2")).thenReturn(u2);
        when(userManager.getUserByName("5")).thenReturn(u5);

        when(userHistoryManager.getHistory(UserHistoryItem.USED_USER, user)).thenReturn(usedusers);
        when(userSearchService.findUsers(eq(""), any(UserSearchParams.class))).thenReturn(allUsers);
        when(userSearchService.userMatches(any(ApplicationUser.class), any(UserSearchParams.class))).thenReturn(true);

        assertThat(concreteUserSearchHelper.getSuggestedUsers(user, newArrayList("5", "4"), UserSearchParams.ACTIVE_USERS_ALLOW_EMPTY_QUERY),
                equalTo((List<ApplicationUser>) ImmutableList.<ApplicationUser>of(u2, u1, u3, u6, u7)));
    }

    /**
     * If the user has USED_USER history, then we should expect to see that in the results, but filtered by group restrictions
     */
    @Test
    public void testGetSuggestedUsersWithUsedUserHistoryWithGroupFilters() {
        MockApplicationUser u1 = new MockApplicationUser("1");
        MockApplicationUser u2 = new MockApplicationUser("2");
        MockApplicationUser u3 = new MockApplicationUser("3");
        MockApplicationUser u4 = new MockApplicationUser("4");
        MockApplicationUser u5 = new MockApplicationUser("5");
        MockApplicationUser u6 = new MockApplicationUser("6");
        MockApplicationUser u7 = new MockApplicationUser("7");
        MockApplicationUser u8 = new MockApplicationUser("8");
        List<ApplicationUser> allUsers = Lists.<ApplicationUser>newArrayList(u1, u2, u3, u4, u5, u6, u7, u8);

        when(permissionManager.hasPermission(Permissions.USER_PICKER, user)).thenReturn(true);
        List<UserHistoryItem> usedusers = Lists.newArrayList(
                new UserHistoryItem(UserHistoryItem.USED_USER, "2"),
                new UserHistoryItem(UserHistoryItem.USED_USER, "3"),
                new UserHistoryItem(UserHistoryItem.USED_USER, "5"));
        when(userManager.getUserByName("2")).thenReturn(u2);
        when(userManager.getUserByName("3")).thenReturn(u3);
        when(userManager.getUserByName("5")).thenReturn(u5);

        when(userHistoryManager.getHistory(UserHistoryItem.USED_USER, user)).thenReturn(usedusers);
        when(userSearchService.findUsers(eq(""), any(UserSearchParams.class))).thenReturn(allUsers);
        when(userSearchService.userMatches(any(ApplicationUser.class), eqGroup("group1"))).thenReturn(true); // group1 is considered matched
        when(userSearchService.userMatches(eq(u3), any(UserSearchParams.class))).thenReturn(false); // u3 not matched
        when(userSearchService.userMatches(eq(u2), any(UserSearchParams.class))).thenReturn(true); // u2 matched

        // only u2 is returned, u3 fitlered by group
        assertThat(concreteUserSearchHelper.getSuggestedUsers(user, newArrayList("5", "4"),
                        UserSearchParams.builder(UserSearchParams.ACTIVE_USERS_ALLOW_EMPTY_QUERY).filter(new UserFilter(true, null, ImmutableSet.of("group1"))).build()),
                equalTo((List<ApplicationUser>) ImmutableList.<ApplicationUser>of(u2, u1, u6, u7, u8)));
    }

    /**
     * If the user isn't logged in and anonymous users don't have browse
     * permission, we shouldn't suggest any users.
     */
    @Test
    public void testGetSuggestedUsersAnonymous() {
        //noinspection deprecation
        when(permissionManager.hasPermission(Permissions.USER_PICKER, null)).thenReturn(false);
        assertNull(concreteUserSearchHelper.getSuggestedUsers(null, Collections.<String>emptyList(), UserSearchParams.ACTIVE_USERS_ALLOW_EMPTY_QUERY));
    }

    /**
     * If the user isn't logged in, but anonymous users have browse permission,
     * we should suggest the first 5 users in the system, sorted alphabetically.
     */
    @Test
    public void testGetSuggestedUsersAnonymousBrowsePermission() {
        List<ApplicationUser> users = Lists.<ApplicationUser>newArrayList(
                new MockApplicationUser("1"), new MockApplicationUser("2"), new MockApplicationUser("3"),
                new MockApplicationUser("4"), new MockApplicationUser("5"), new MockApplicationUser("6"));

        when(permissionManager.hasPermission(eq(Permissions.USER_PICKER), isNull(ApplicationUser.class))).thenReturn(true);
        when(userSearchService.findUsers(eq(""), any(UserSearchParams.class))).thenReturn(users);
        when(userSearchService.userMatches(any(ApplicationUser.class), emptyGroup())).thenReturn(true); // all null groups are considered matched
        final List<ApplicationUser> applicationUsers = copyOf(users.subList(0, 5));
        assertThat(concreteUserSearchHelper.getSuggestedUsers(null, Collections.<String>emptyList(), UserSearchParams.ACTIVE_USERS_ALLOW_EMPTY_QUERY),
                hasItems(applicationUsers.toArray(new ApplicationUser[applicationUsers.size()])));
    }

    @Test
    public void testUsersToSelectEmpty() {
        LinkedHashSet<ApplicationUser> usersToSelectFrom = Sets.newLinkedHashSet(
                ImmutableList.<ApplicationUser>of(new MockApplicationUser("1")));
        when(userSearchService.userMatches(any(ApplicationUser.class), any(UserSearchParams.class))).thenReturn(true);

        final Set<ApplicationUser> suggestedUsers = concreteUserSearchHelper.getUsersToSelect(
                usersToSelectFrom.stream(), ImmutableSet.<String>of(), 10,
                UserSearchParams.ACTIVE_USERS_ALLOW_EMPTY_QUERY);

        assertThat(suggestedUsers, equalTo(usersToSelectFrom));
    }

    @Test
    public void testUsersToSelectOnlySelectsMatchingUsers() {
        ApplicationUser user1 = Mockito.mock(ApplicationUser.class);
        ApplicationUser user2 = Mockito.mock(ApplicationUser.class);
        LinkedHashSet<ApplicationUser> usersToSelectFrom = Sets.newLinkedHashSet(
                ImmutableList.of(user1, user2));

        when(userSearchService.userMatches(any(ApplicationUser.class), any(UserSearchParams.class))).thenReturn(true);
        when(userSearchService.userMatches(eq(user2), any(UserSearchParams.class))).thenReturn(false); // return false for user2

        final Set<ApplicationUser> suggestedUsers = concreteUserSearchHelper.getUsersToSelect(
                usersToSelectFrom.stream(), ImmutableSet.<String>of(), 10,
                UserSearchParams.ACTIVE_USERS_ALLOW_EMPTY_QUERY);

        assertThat(suggestedUsers.size(), equalTo(1));
        assertThat(suggestedUsers, hasItem(user1));
    }

    @Test
    public void testUsersToSelectRespectsLimit() {
        final int limit = 3;
        LinkedHashSet<ApplicationUser> usersToSelectFrom = Sets.newLinkedHashSet(
                ImmutableList.<ApplicationUser>of(new MockApplicationUser("1"), new MockApplicationUser("2"),
                        new MockApplicationUser("3"), new MockApplicationUser("4"), new MockApplicationUser("5")));

        when(userSearchService.userMatches(any(ApplicationUser.class), any(UserSearchParams.class))).thenReturn(true);

        final Set<ApplicationUser> suggestedUsers = concreteUserSearchHelper.getUsersToSelect(
                usersToSelectFrom.stream(), ImmutableSet.<String>of(), limit,
                UserSearchParams.ACTIVE_USERS_ALLOW_EMPTY_QUERY);

        assertThat(suggestedUsers.size(), equalTo(limit));
    }

    @Test
    public void testUsersToSelectDoesNotIncludeAlreadySelectedUsers() {
        ApplicationUser user1 = Mockito.mock(ApplicationUser.class);
        ApplicationUser user2 = Mockito.mock(ApplicationUser.class);
        when(user2.getName()).thenReturn("user 2");
        LinkedHashSet<ApplicationUser> usersToSelectFrom = Sets.newLinkedHashSet(ImmutableList.of(
                user1, user2));

        when(userSearchService.userMatches(any(ApplicationUser.class), any(UserSearchParams.class))).thenReturn(true);

        final Set<ApplicationUser> suggestedUsers = concreteUserSearchHelper.getUsersToSelect(
                usersToSelectFrom.stream(), ImmutableSet.of(user2.getName()), 10,
                UserSearchParams.ACTIVE_USERS_ALLOW_EMPTY_QUERY);

        assertThat(suggestedUsers.size(), equalTo(1));
        assertThat(suggestedUsers, hasItem(user1));
    }

    @Test
    public void addUserGroupSuggestionParamsReturnsOtherFieldsWhenSearchParamsIsNull() throws Exception {
        final ImmutableList<String> selectedUsers = ImmutableList.of();
        final Map<String, Object> params = Maps.newHashMap();
        concreteUserSearchHelper.addUserGroupSuggestionParams(null, selectedUsers, null, params);
        assertThat(params, hasKey("hasPermissionToPickUsers"));
        assertThat(params, not(hasKey("suggestedUsers")));
        assertThat(params, hasKey("suggestedGroups"));
        assertThat(params, hasKey("placeholderText"));
    }

    @Test
    public void addUserGroupSuggestionParamsReturnsAllFieldsWhenSearchParamsIsNotNull() throws Exception {
        final ImmutableList<String> selectedUsers = ImmutableList.of();
        final Map<String, Object> params = Maps.newHashMap();
        concreteUserSearchHelper.addUserGroupSuggestionParams(null, selectedUsers, UserSearchParams.ACTIVE_USERS_ALLOW_EMPTY_QUERY, params);
        assertThat(params, hasKey("hasPermissionToPickUsers"));
        assertThat(params, hasKey("suggestedUsers"));
        assertThat(params, hasKey("suggestedGroups"));
        assertThat(params, hasKey("placeholderText"));
    }

    /**
     * Non evaluator system should not load any suggestions apart from the history which in this test is empty.
     */
    @Test
    public void nonEvaluatorSystemDoesNotLoadAllUsers() throws Exception {
        List<ApplicationUser> users = IntStream.rangeClosed(1, 11).mapToObj(i -> new MockApplicationUser("User" + i))
                                        .collect(Collectors.toList());

        when(permissionManager.hasPermission(Permissions.USER_PICKER, user)).thenReturn(true);
        when(userSearchService.findUsers(eq(""), any(UserSearchParams.class))).thenReturn(users);
        when(userSearchService.findUserNames(eq(""), any(UserSearchParams.class))).thenAnswer(invocation -> {
            UserSearchParams params = invocation.getArgumentAt(1, UserSearchParams.class);
            return users.subList(0, Math.min(users.size(), params.getMaxResults()));
        });
        when(userSearchService.userMatches(any(ApplicationUser.class), any(UserSearchParams.class))).thenReturn(true);
        List<ApplicationUser> results = concreteUserSearchHelper.getSuggestedUsers(user, new ArrayList<>(), UserSearchParams.ACTIVE_USERS_ALLOW_EMPTY_QUERY);
        assertThat(results, empty());
    }

    /**
     * Evaluator system should load first 5 users.
     */
    @Test
    public void evaluatorSystemLoadsUsers() throws Exception {
        List<ApplicationUser> users = IntStream.rangeClosed(1, 10).mapToObj(i -> new MockApplicationUser("User" + i))
                .collect(Collectors.toList());

        when(permissionManager.hasPermission(Permissions.USER_PICKER, user)).thenReturn(true);
        when(userSearchService.findUsers(eq(""), any(UserSearchParams.class))).thenReturn(users);
        when(userSearchService.findUserNames(eq(""), any(UserSearchParams.class))).thenAnswer(invocation -> {
            UserSearchParams params = invocation.getArgumentAt(1, UserSearchParams.class);
            return users.subList(0, Math.min(users.size(), params.getMaxResults()));
        });
        when(userSearchService.userMatches(any(ApplicationUser.class), any(UserSearchParams.class))).thenReturn(true);
        List<ApplicationUser> results = concreteUserSearchHelper.getSuggestedUsers(user, new ArrayList<>(), UserSearchParams.ACTIVE_USERS_ALLOW_EMPTY_QUERY);
        assertThat(results, hasSize(5));
    }

    private UserSearchParams emptyGroup() {
        return argThat(new TypeSafeMatcher<UserSearchParams>() {
            @Override
            protected boolean matchesSafely(final UserSearchParams userSearchParams) {
                return userSearchParams.getUserFilter() == null || CollectionUtils.isEmpty(userSearchParams.getUserFilter().getGroups());
            }

            @Override
            public void describeTo(final Description description) {
            }
        });
    }

    private UserSearchParams eqGroup(final String... groupNames) {
        return argThat(new TypeSafeMatcher<UserSearchParams>() {
            @Override
            protected boolean matchesSafely(final UserSearchParams userSearchParams) {
                return userSearchParams.getUserFilter().getGroups().equals(ImmutableSet.copyOf(groupNames));
            }

            @Override
            public void describeTo(final Description description) {
            }
        });
    }
}
