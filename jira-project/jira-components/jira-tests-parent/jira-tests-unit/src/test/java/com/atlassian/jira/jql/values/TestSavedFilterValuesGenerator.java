package com.atlassian.jira.jql.values;

import com.atlassian.jira.bc.JiraServiceContextImpl;
import com.atlassian.jira.bc.filter.SearchRequestService;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.sharing.search.SharedEntitySearchParametersBuilder;
import com.atlassian.jira.sharing.search.SharedEntitySearchResult;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.query.QueryImpl;
import com.google.common.collect.ImmutableList;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Collections;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

/**
 * @since v4.0
 */
public class TestSavedFilterValuesGenerator {
    @Rule
    public MockitoRule mockito = MockitoJUnit.rule();
    @Mock
    private SearchRequestService searchRequestService;
    @Mock
    private SharedEntitySearchResult<SearchRequest> result;
    @InjectMocks
    private SavedFilterValuesGenerator valuesGenerator;

    @Test
    public void testGetPossibleValuesNoMatchingValuesNullPassedValue() throws Exception {
        final SharedEntitySearchParametersBuilder builder = new SharedEntitySearchParametersBuilder().setName("");

        when(result.getResults()).thenReturn(Collections.emptyList());

        when(searchRequestService.search(new JiraServiceContextImpl((ApplicationUser) null), builder.toSearchParameters(), 0, 10)).thenReturn(result);

        final ClauseValuesGenerator.Results possibleValues = valuesGenerator.getPossibleValues(null, "filter", null, 10);
        assertEquals(0, possibleValues.getResults().size());
    }

    @Test
    public void testGetPossibleValuesNoMatchingValues() throws Exception {
        final SharedEntitySearchParametersBuilder builder = new SharedEntitySearchParametersBuilder().setName("");

        when(result.getResults()).thenReturn(Collections.emptyList());
        when(searchRequestService.search(new JiraServiceContextImpl((ApplicationUser) null), builder.toSearchParameters(), 0, 10)).thenReturn(result);

        final ClauseValuesGenerator.Results possibleValues = valuesGenerator.getPossibleValues(null, "filter", "", 10);
        assertEquals(0, possibleValues.getResults().size());
    }

    @Test
    public void testGetPossibleValuesFindValues() throws Exception {
        final SharedEntitySearchParametersBuilder builder = new SharedEntitySearchParametersBuilder().setName("a");

        when(result.getResults()).thenReturn(ImmutableList.of(
                new SearchRequest(new QueryImpl(), (ApplicationUser) null, "Aa sr", "desc"),
                new SearchRequest(new QueryImpl(), (ApplicationUser) null, "A sr", "desc")));

        when(searchRequestService.search(new JiraServiceContextImpl((ApplicationUser) null), builder.toSearchParameters(), 0, 10)).thenReturn(result);

        final ClauseValuesGenerator.Results possibleValues = valuesGenerator.getPossibleValues(null, "filter", "a", 10);
        assertEquals(2, possibleValues.getResults().size());
        assertEquals(possibleValues.getResults().get(0), new ClauseValuesGenerator.Result("Aa sr"));
        assertEquals(possibleValues.getResults().get(1), new ClauseValuesGenerator.Result("A sr"));
    }
}
