/**
 * Contains cucumber related classes. This package is automatically scanned at runtime by cucumber - it picks up
 * and initializes classes like {@link cucumber.runtime.PicoObjectFactory}. As package to be scanned is hardcoded
 * in cucumber, this package must be named 'cucumber.runtime'
 */
package cucumber.runtime;