package cucumber.runtime;

import java.lang.annotation.ElementType;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Indicates {@link Module} class that would used during the test. Can be repeated to include
 * multiple modules.
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Repeatable(UsesModules.class)
public @interface UsesModule {
    Class<? extends Module> value();
}