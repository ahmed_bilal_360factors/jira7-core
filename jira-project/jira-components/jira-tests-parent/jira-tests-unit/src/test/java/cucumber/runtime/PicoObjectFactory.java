package cucumber.runtime;


import com.atlassian.jira.config.component.PicoContainerFactory;
import cucumber.runtime.java.ObjectFactory;
import org.picocontainer.MutablePicoContainer;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Dependency injection for Cucumber based on pico. Dependencies are recreated before each scenario, but new behaviour
 * can be added to create them only once - it may be useful in integration tests that need to do expensive
 * initialization operations, like database creation.
 * <p>
 * Dependencies are injected into cucumber classes with steps (containing @Given, @When or @Then annotation) or hooks
 * either through the constructor or setters.
 * <p>
 * Cucumber classes can be annotated with {@link UsesModule} in order to initialize additional dependencies that would
 * be available for injection.
 */
public class PicoObjectFactory implements ObjectFactory {
    private MutablePicoContainer container;
    //Set of Cucumber StepDefinitions and Hooks that needs to be injected
    private final Set<Class<?>> cucumberClasses = new HashSet<>();
    private final List<Module> modules = new ArrayList<>();

    public void start() {
        container = PicoContainerFactory.defaultJIRAContainer();
        cucumberClasses.forEach(container::addComponent);

        modules.forEach(module -> module.wire(container));
        container.start();
    }

    public void stop() {
        container.stop();
        container.dispose();
    }

    public void addClass(Class<?> clazz) {
        if (cucumberClasses.add(clazz)) {
            initializeModules(clazz);
        }
    }

    public <T> T getInstance(Class<T> type) {
        return container.getComponent(type);
    }

    private final void initializeModules(Class clazz) {
        final UsesModule[] annotations = (UsesModule[]) clazz.getDeclaredAnnotationsByType(UsesModule.class);
        for (UsesModule annotation : annotations) {
            try {
                modules.add(annotation.value().newInstance());
            } catch (Exception e) {
                throw new RuntimeException("Cannot instantiate module class: " + annotation.value().getCanonicalName());
            }

        }
    }
}
