package cucumber.runtime;

import com.google.common.collect.ImmutableList;
import cucumber.deps.com.thoughtworks.xstream.converters.SingleValueConverter;

import static org.apache.commons.lang3.Validate.notNull;

/**
 * Converts comma-separated string into {@link ImmutableList} of strings.
 * E.g. text "one, two,three" will be converted to ["one", "two", "three"]
 */
public class CommaSeparatedStringToListConverter implements SingleValueConverter {
    @Override
    public String toString(final Object obj) {
        return obj.toString();
    }

    @Override
    public Object fromString(final String str) {
        final String trim = notNull(str).trim();
        if (trim.length() > 0) {
            return ImmutableList.copyOf(trim.split(",\\s*"));
        } else {
            return ImmutableList.of();
        }
    }

    @Override
    public boolean canConvert(final Class type) {
        return true;
    }
}
