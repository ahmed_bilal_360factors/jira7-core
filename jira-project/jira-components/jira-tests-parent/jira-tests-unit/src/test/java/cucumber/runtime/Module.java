package cucumber.runtime;

import org.picocontainer.MutablePicoContainer;

public interface Module {
    void wire(MutablePicoContainer container);
}
