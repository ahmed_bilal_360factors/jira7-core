package com.atlassian.jira.event;

import com.atlassian.analytics.api.annotations.EventName;
import com.atlassian.annotations.Internal;
import com.atlassian.annotations.PublicApi;
import com.atlassian.jira.avatar.Avatar;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.web.action.RequestSourceType;
import org.apache.commons.lang.StringUtils;

import javax.annotation.ParametersAreNonnullByDefault;

/**
 * Event detailing which fields have changed when a project is updated
 *
 * @since v7.2
 */
@PublicApi
@ParametersAreNonnullByDefault
@EventName("jira.administration.projectdetails.updated")
public class ProjectUpdatedDetailedChangesEvent extends AbstractEvent {
    private final boolean isProjectNameChanged;
    private final boolean isProjectUrlChanged;
    private final boolean isProjectDescriptionChanged;
    private boolean isProjectAvatarChanged;
    private String projectAvatarChangedTo;
    private String requestSourceType;

    @Internal
    public ProjectUpdatedDetailedChangesEvent(final Project oldProject, final Project newProject, final RequestSourceType requestSourceType) {
        this.isProjectNameChanged = !StringUtils.equals(oldProject.getName(), newProject.getName());
        this.isProjectUrlChanged = !StringUtils.equals(oldProject.getUrl(), newProject.getUrl());
        this.isProjectDescriptionChanged = !StringUtils.equals(oldProject.getDescription(), newProject.getDescription());

        final Avatar newAvatar = newProject.getAvatar();
        if (oldProject.getAvatar() != null && newAvatar != null && !oldProject.getAvatar().getId().equals(newAvatar.getId())) {
            this.isProjectAvatarChanged = true;
            this.projectAvatarChangedTo = newAvatar.isSystemAvatar() ? newAvatar.getId().toString() : "custom";
        }

        this.requestSourceType = requestSourceType.getType();
    }

    public boolean getIsProjectNameChanged() {
        return isProjectNameChanged;
    }

    public boolean getIsProjectUrlChanged() {
        return isProjectUrlChanged;
    }

    public boolean getIsProjectDescriptionChanged() {
        return isProjectDescriptionChanged;
    }

    public boolean getIsProjectAvatarChanged() {
        return isProjectAvatarChanged;
    }

    public String getProjectAvatarChangedTo() {
        return projectAvatarChangedTo;
    }

    public String getRequestSourceType() {
        return requestSourceType;
    }
}
