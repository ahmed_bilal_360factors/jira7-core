package com.atlassian.jira.bc.user.search;

import com.atlassian.annotations.PublicApi;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.user.ApplicationUser;
import com.opensymphony.workflow.loader.ActionDescriptor;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Service that retrieves issue-assignable {@link com.atlassian.jira.user.ApplicationUser} objects.
 * <p>
 * Assignees may be filtered on a search string or on recent issue or user history.
 *
 * @since v5.0
 */
@PublicApi
public interface AssigneeService {
    /**
     * Determines if a user is assignable to an issue.
     *
     * @param issue the issue.
     * @param user  the user to check.
     * @return true if user is assignable to the issue, false if not.
     * @since v7.0
     */
    public boolean isAssignable(@Nonnull Issue issue, @Nonnull ApplicationUser user, ActionDescriptor actionDescriptor);

    /**
     * Determines if a user is assignable to an issue in a project.
     *
     * @param project the project.
     * @param user    the user to check.
     * @return true if user is assignable to issues in the project, false if not.
     * @since v7.0
     */
    public boolean isAssignable(@Nonnull Project project, @Nonnull ApplicationUser user);

    /**
     * Get all {@link com.atlassian.jira.user.ApplicationUser}s that may have an {@link Issue} assigned to them, for a given workflow state.
     * <p>
     * The {@link ActionDescriptor} may be used to check for workflow states that only allow a subset of
     * normally-assignable users.
     *
     * @param issue            the Issue to find assignable users for
     * @param actionDescriptor workflow action descriptor to filter users on
     * @return a list of Users sorted by name
     */
    List<ApplicationUser> getAssignableUsers(Issue issue, @Nullable ActionDescriptor actionDescriptor);

    /**
     * Get all {@link com.atlassian.jira.user.ApplicationUser}s that may have all of the given {@link Issue}s assigned to them, for a
     * given workflow state.
     * <p>
     * The {@link ActionDescriptor} may be used to check for workflow states that only allow a subset of
     * normally-assignable users.
     * </p><p>
     * Note: This method is exactly equivalent to {@link #getAssignableUsers(Issue, ActionDescriptor)},
     * but returns only those users that are assignable for <em>all</em> of the issues.  This is
     * significantly more efficient than calling {@link #getAssignableUsers(Issue, ActionDescriptor)}
     * multiple times and filtering the lists yourself.
     * </p>
     *
     * @param issues           the Issues to find assignable users for
     * @param actionDescriptor workflow action descriptor to filter users on
     * @return a list of Users sorted by name
     */
    List<ApplicationUser> getAssignableUsers(Collection<Issue> issues, @Nullable ActionDescriptor actionDescriptor);

    /**
     * Get assignable Users based on a query string and issue.
     * <p>
     * Matches on the start of username, Each word in Full Name & email.
     * <p>
     * Results are sorted according to the {@link com.atlassian.jira.issue.comparator.UserCachingComparator}.
     *
     * @param query            String to search for.
     * @param issue            Issue to check Assignee permissions against
     * @param actionDescriptor an {@link com.opensymphony.workflow.loader.ActionDescriptor} describing the context in which the Assignee is being searched
     * @return List of {@link com.atlassian.jira.user.ApplicationUser} objects that match criteria.
     */
    Collection<ApplicationUser> findAssignableUsers(String query, Issue issue, ActionDescriptor actionDescriptor);

    /**
     * Get assignable Users based on a query string and project.
     * <p>
     * Matches on the start of username, Each word in Full Name & email.
     * <p>
     * Results are sorted according to the {@link com.atlassian.jira.issue.comparator.UserCachingComparator}.
     *
     * @param query   String to search for.
     * @param project Project check Assignee permissions against
     * @return List of {@link com.atlassian.jira.user.ApplicationUser} objects that match criteria.
     */
    Collection<ApplicationUser> findAssignableUsers(String query, Project project);

    /**
     * Returns a list of suggested Assignee {@link com.atlassian.jira.user.ApplicationUser}s for a given Issue and logged-in com.atlassian.jira.user.ApplicationUser.
     * <p>
     * This version accepts a pre-populated list of assignable Users to filter.
     *
     * @param issue            Issue to get suggested Assignees for
     * @param loggedInUser     the user getting the suggestions, whose Assignee history may be queried
     * @param actionDescriptor workflow action descriptor to filter users on
     * @return List of {@link com.atlassian.jira.user.ApplicationUser} objects deemed relevant to the given Issue and com.atlassian.jira.user.ApplicationUser, sorted by UserBestNameComparator
     */
    List<ApplicationUser> getSuggestedAssignees(Issue issue, @Nullable ApplicationUser loggedInUser, @Nullable ActionDescriptor actionDescriptor);

    /**
     * Returns a list of suggested Assignee {@link com.atlassian.jira.user.ApplicationUser}s for a given Issue and logged-in com.atlassian.jira.user.ApplicationUser.
     * <p>
     * This version accepts a pre-populated list of assignable Users to filter.
     *
     * @param issue           Issue to get suggested Assignees for
     * @param loggedInUser    the user getting the suggestions, whose Assignee history may be queried
     * @param assignableUsers a list of {@link com.atlassian.jira.user.ApplicationUser}s to filter
     * @return List of {@link com.atlassian.jira.user.ApplicationUser} objects deemed relevant to the given Issue and com.atlassian.jira.user.ApplicationUser.
     */
    List<ApplicationUser> getSuggestedAssignees(Issue issue, ApplicationUser loggedInUser, List<ApplicationUser> assignableUsers);


    /**
     * Converts a collection of Users to a Map where the key is the com.atlassian.jira.user.ApplicationUser full name and the value is true or false.
     * <p>
     * The value will be true if no other user with that exact full name exists.
     * The value will be false if at least one other user with that exact full name exists.
     *
     * @param users a collection of Users that may contain multiple users with the same full name
     * @return a map of user full name Strings to a uniqueness boolean flag
     */
    Map<String, Boolean> makeUniqueFullNamesMap(Collection<ApplicationUser> users);

    /**
     * Converts a set of suggested assignee name Strings to a list of suggested {@link com.atlassian.jira.user.ApplicationUser} objects.
     * <p>
     * Suggested user names may not be returned as suggested users if they are not in the assignable user list.
     *
     * @param suggestedAssigneeNames the names of the users to return
     * @param assignableUsers        a list of Users to filter by the suggested assignee names
     * @return a filtered List of assignable Users that are suggested as Assignees
     */
    List<ApplicationUser> getSuggestedAssignees(final Set<String> suggestedAssigneeNames, List<ApplicationUser> assignableUsers);

    /**
     * Returns the names of Users that the given Issue has recently been assigned to.
     * <p>
     * The current assignee should be included in the returned list.
     *
     * @param issue Issue to get recent assignees for
     * @return a Set of assignee usernames
     * @see #getRecentAssigneeKeysForIssue(com.atlassian.jira.issue.Issue)
     */
    Set<String> getRecentAssigneeNamesForIssue(Issue issue);

    /**
     * Returns the names of {@link com.atlassian.jira.user.ApplicationUser}s that have recently been assigned to issues by the specified com.atlassian.jira.user.ApplicationUser.
     *
     * @param user com.atlassian.jira.user.ApplicationUser to check for assignees in history manager
     * @return a Set of assignee usernames
     */
    Set<String> getRecentAssigneeNamesForUser(ApplicationUser user);

    /**
     * Returns the keys of Users that the given Issue has recently been assigned to.
     * <p>
     * The current assignee should be included in the returned list.
     *
     * @param issue Issue to get recent assignees for
     * @return a Set of assignee keys
     * @see #getRecentAssigneeNamesForIssue(com.atlassian.jira.issue.Issue)
     */
    Set<String> getRecentAssigneeKeysForIssue(Issue issue);

    /**
     * Returns the keys of {@link com.atlassian.jira.user.ApplicationUser}s that have recently been assigned to issues by the specified com.atlassian.jira.user.ApplicationUser.
     *
     * @param remoteUser com.atlassian.jira.user.ApplicationUser to check for assignees in history manager
     * @return a Set of assignee usernames
     */
    Set<String> getRecentAssigneeKeysForUser(ApplicationUser remoteUser);
}
