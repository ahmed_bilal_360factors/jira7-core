package com.atlassian.jira.event.project;

import com.atlassian.annotations.Internal;
import com.atlassian.fugue.Option;
import com.atlassian.jira.project.version.DeleteVersionWithCustomFieldParameters.CustomFieldReplacement;
import com.atlassian.jira.project.version.Version;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import java.util.Collections;
import java.util.List;

import static com.atlassian.fugue.Option.option;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Event indicating a version has been deleted
 *
 * @since v4.4
 */
public class VersionDeleteEvent extends AbstractVersionEvent {
    private final Option<Version> mergedTo;
    private final Option<Version> affectsVersionSwappedTo;
    private final Option<Version> fixVersionSwappedTo;
    private final List<CustomFieldReplacement> customFieldReplacements;

    /**
     * @deprecated Use {@link com.atlassian.jira.event.project.VersionDeleteEvent.VersionDeleteEventBuilder} instead.
     */
    @Internal
    @Deprecated
    public VersionDeleteEvent(@Nonnull Version version) {
        this(version, null, null, null, Collections.emptyList());
    }

    private VersionDeleteEvent(@Nonnull Version version,
                               @Nullable Version mergedTo,
                               @Nullable Version affectsVersionSwappedTo,
                               @Nullable Version fixSwapVersion,
                               @Nonnull List<CustomFieldReplacement> customFieldReplacements) {
        super(version);
        this.mergedTo = option(mergedTo);
        this.affectsVersionSwappedTo = option(affectsVersionSwappedTo);
        this.fixVersionSwappedTo = option(fixSwapVersion);
        this.customFieldReplacements = customFieldReplacements;
    }

    public static VersionDeleteEvent deletedAndMerged(@Nonnull Version deletedVersion, @Nonnull Version mergedTo) {
        return new VersionDeleteEventBuilder(deletedVersion).setMergedTo(mergedTo).createEvent();
    }

    public static VersionDeleteEvent deleted(@Nonnull Version deletedVersion) {
        return new VersionDeleteEventBuilder(deletedVersion).createEvent();
    }

    public Option<Version> getMergedTo() {
        return mergedTo;
    }

    public Option<Version> getAffectsVersionSwappedTo() {
        return affectsVersionSwappedTo;
    }

    public Option<Version> getFixVersionSwappedTo() {
        return fixVersionSwappedTo;
    }

    public List<CustomFieldReplacement> getCustomFieldReplacements() {
        return customFieldReplacements;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }

        final VersionDeleteEvent that = (VersionDeleteEvent) o;

        if (affectsVersionSwappedTo != null ? !affectsVersionSwappedTo.equals(that.affectsVersionSwappedTo) : that.affectsVersionSwappedTo != null) {
            return false;
        }
        if (fixVersionSwappedTo != null ? !fixVersionSwappedTo.equals(that.fixVersionSwappedTo) : that.fixVersionSwappedTo != null) {
            return false;
        }
        if (mergedTo != null ? !mergedTo.equals(that.mergedTo) : that.mergedTo != null) {
            return false;
        }
        if (!customFieldReplacements.equals(that.customFieldReplacements)) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (mergedTo != null ? mergedTo.hashCode() : 0);
        result = 31 * result + (affectsVersionSwappedTo != null ? affectsVersionSwappedTo.hashCode() : 0);
        result = 31 * result + (fixVersionSwappedTo != null ? fixVersionSwappedTo.hashCode() : 0);
        result = 31 * result + customFieldReplacements.hashCode();
        return result;
    }

    public static class VersionDeleteEventBuilder {
        private final Version version;
        private Version mergedTo;
        private Version affectsVersionSwappedTo;
        private Version fixSwapVersion;
        private List<CustomFieldReplacement> customFieldReplacements;

        public VersionDeleteEventBuilder(final Version version) {
            this.version = checkNotNull(version);
        }

        public VersionDeleteEventBuilder setMergedTo(final Version mergedTo) {
            this.mergedTo = mergedTo;
            return this;
        }

        public VersionDeleteEventBuilder affectsVersionSwappedTo(final Version affectsVersionSwappedTo) {
            this.affectsVersionSwappedTo = affectsVersionSwappedTo;
            return this;
        }

        public VersionDeleteEventBuilder fixVersionSwappedTo(final Version fixSwapVersion) {
            this.fixSwapVersion = fixSwapVersion;
            return this;
        }

        public VersionDeleteEventBuilder customFieldReplacements(final List<CustomFieldReplacement> customFieldReplacements) {
            this.customFieldReplacements = customFieldReplacements;
            return this;
        }

        public VersionDeleteEvent createEvent() {
            return new VersionDeleteEvent(version, mergedTo, affectsVersionSwappedTo, fixSwapVersion,
                    customFieldReplacements==null ? Collections.emptyList() : customFieldReplacements);
        }
    }
}
