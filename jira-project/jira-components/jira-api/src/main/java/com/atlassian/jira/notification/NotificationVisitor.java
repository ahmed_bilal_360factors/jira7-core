package com.atlassian.jira.notification;

import com.atlassian.annotations.PublicSpi;

/**
 * Visitor for all types of notifications which can be configured in JIRA.
 *
 * @param <X> the returned type of each method.
 * @since 7.0
 */
@PublicSpi
public interface NotificationVisitor<X> {
    X visit(UserNotification userNotification);

    X visit(GroupNotification groupNotification);

    X visit(ProjectRoleNotification projectRoleNotification);

    X visit(EmailNotification emailNotification);

    X visit(CustomFieldValueNotification customFieldValueNotification);

    X visit(RoleNotification roleNotification);
}
