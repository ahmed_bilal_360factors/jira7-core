package com.atlassian.jira.util;

import com.atlassian.annotations.PublicApi;
import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;

import java.util.List;
import java.util.stream.StreamSupport;

import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toList;

/**
 * Class containing static factory methods used to create {@link Page} instances.
 *
 * @since 6.4.7
 */
@PublicApi
public class Pages {

    /**
     * Creates a page given a list of values consisting the page, a total count of all values and a page request
     *
     * @param values values that appear on the page
     * @param totalCount total count of all values
     * @param pageRequest a page request used to create this page
     * @param <T> type of entities in the list
     * @return the requested page
     *
     * @throws IllegalArgumentException if arguments are inconsistent
     */
    public static <T> Page<T> page(final Iterable<T> values, final long totalCount, final PageRequest pageRequest) {
        ImmutableList<T> valuesList = ImmutableList.copyOf(values);

        boolean isLast = pageRequest.getStart() + valuesList.size() == totalCount;

        Preconditions.checkArgument(isLast || pageRequest.getLimit() == valuesList.size(),
                "Inconsistent arguments: this is not the last page and yet the number of values is different than the page limit");
        Preconditions.checkArgument(valuesList.size() <= totalCount,
                "Inconsistent arguments: size of the page is greater than totalCount");

        return PageImpl.<T>builder()
                .setIsLast(isLast)
                .setSize(valuesList.size())
                .setStart(pageRequest.getStart())
                .setTotal(totalCount)
                .setValues(valuesList)
                .build();
    }

    /**
     * Given all values and a page request creates a page that conforms to the request.
     *
     * @param allValues   all not paged values
     * @param pageRequest page request
     * @param <T>         type of entities in the list
     * @return a requested page
     */
    public static <T> Page<T> toPage(Iterable<T> allValues, PageRequest pageRequest) {
        return toPage(allValues, pageRequest, val -> true, identity());
    }

    /**
     * Creates a page from values and a {@link com.atlassian.jira.util.PageRequest}. This method will
     * fill information about the the total number of values, start and size of the returned page.
     *
     * @param values      unfiltered and not-transformed values from which the page is created
     * @param pageRequest request for the page
     * @param filter      predicate used to filter values
     * @param transform   function which transforms the values from <strong>T</strong> to <strong>R</strong>
     * @param <T>         the type of values from which page is created
     * @param <R>         the type of the page
     * @return the page with filtered and transformed values
     */
    public static <T, R> Page<R> toPage(final Iterable<T> values,
                                        final PageRequest pageRequest,
                                        final java.util.function.Predicate<T> filter,
                                        final java.util.function.Function<T, R> transform) {
        List<T> filtered = StreamSupport.stream(values.spliterator(), false)
                .filter(filter)
                .collect(toList());

        List<T> limitedWithOneExtra = filtered
                .stream()
                .skip(pageRequest.getStart())
                .limit(pageRequest.getLimit() + 1)
                .collect(toList());

        boolean isLast = limitedWithOneExtra.size() < pageRequest.getLimit() + 1;

        List<R> valuesForPage = ImmutableList.copyOf(limitedWithOneExtra
                .stream()
                .limit(pageRequest.getLimit())
                .map(transform)
                .iterator());

        return PageImpl.<R>builder()
                .setValues(valuesForPage)
                .setSize(Iterables.size(valuesForPage))
                .setStart(pageRequest.getStart())
                .setTotal((long) filtered.size())
                .setIsLast(isLast)
                .build();
    }

    private static final class PageImpl<T> implements Page<T> {
        private final long start;
        private final Long total;
        private final int size;
        private final boolean isLast;
        private final List<T> values;

        private PageImpl(long start, Long total, int size, boolean isLast, List<T> values) {
            this.start = start;
            this.total = total;
            this.size = size;
            this.isLast = isLast;
            this.values = values;
        }

        public long getStart() {
            return start;
        }

        public Long getTotal() {
            return total;
        }

        public int getSize() {
            return size;
        }

        public boolean isLast() {
            return isLast;
        }

        public List<T> getValues() {
            return values;
        }

        public static <T> Builder<T> builder() {
            return new Builder<>();
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }

            PageImpl that = (PageImpl) o;

            return Objects.equal(this.start, that.start) &&
                    Objects.equal(this.total, that.total) &&
                    Objects.equal(this.size, that.size) &&
                    Objects.equal(this.isLast, that.isLast) &&
                    Objects.equal(this.values, that.values);
        }

        @Override
        public int hashCode() {
            return Objects.hashCode(start, total, size, isLast, values);
        }

        @Override
        public String toString() {
            return MoreObjects.toStringHelper(this)
                    .add("start", start)
                    .add("total", total)
                    .add("size", size)
                    .add("hasNext", isLast)
                    .add("values", values)
                    .toString();
        }

        public static final class Builder<T> {
            private long start;
            private Long total;
            private int size;
            private boolean isLast;
            private List<T> values = ImmutableList.of();

            private Builder() {
            }

            public Builder<T> setStart(long start) {
                this.start = start;
                return this;
            }

            public Builder<T> setTotal(Long total) {
                this.total = total;
                return this;
            }

            public Builder<T> setSize(int size) {
                this.size = size;
                return this;
            }

            public Builder<T> setIsLast(boolean isLast) {
                this.isLast = isLast;
                return this;
            }

            public Builder<T> setValues(List<T> values) {
                this.values = values;
                return this;
            }

            public PageImpl<T> build() {
                return new PageImpl<>(start, total, size, isLast, values);
            }
        }
    }
}
