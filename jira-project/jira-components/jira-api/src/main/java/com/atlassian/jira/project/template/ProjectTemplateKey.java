package com.atlassian.jira.project.template;

import com.atlassian.annotations.PublicApi;

/**
 * Represents the key for a project template.
 *
 * @since 7.0
 */
@PublicApi
public class ProjectTemplateKey {
    private final String key;

    public ProjectTemplateKey(final String key) {
        this.key = key;
    }

    public String getKey() {
        return key;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final ProjectTemplateKey that = (ProjectTemplateKey) o;

        if (!key.equals(that.key)) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        return key.hashCode();
    }
}
