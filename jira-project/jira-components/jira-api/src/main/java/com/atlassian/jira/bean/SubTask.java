package com.atlassian.jira.bean;

import com.atlassian.annotations.PublicApi;
import com.atlassian.jira.issue.Issue;

@PublicApi
public interface SubTask {
    public Long getSequence();

    public Long getDisplaySequence();

    /**
     * Returns the Parent Issue.
     *
     * @return the Parent Issue.
     */
    public Issue getParent();

    /**
     * Returns the SubTask Issue.
     *
     * @return the SubTask Issue.
     */
    public Issue getSubTask();
}
