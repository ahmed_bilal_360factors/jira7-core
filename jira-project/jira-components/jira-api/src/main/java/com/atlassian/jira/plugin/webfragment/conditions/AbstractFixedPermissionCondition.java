package com.atlassian.jira.plugin.webfragment.conditions;

import com.atlassian.annotations.ExperimentalSpi;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.plugin.PluginParseException;

import java.util.Map;

import static com.atlassian.jira.plugin.webfragment.conditions.cache.ConditionCacheKeys.permission;
import static com.atlassian.jira.plugin.webfragment.conditions.cache.RequestCachingConditionHelper.cacheConditionResultInRequest;

/**
 * Convenient abstraction for conditions which check a particular global permission
 *
 * @since v6.4
 */
@ExperimentalSpi
public abstract class AbstractFixedPermissionCondition extends AbstractWebCondition {
    protected final GlobalPermissionManager permissionManager;
    protected final GlobalPermissionKey permission;

    public AbstractFixedPermissionCondition(GlobalPermissionManager permissionManager, GlobalPermissionKey globalPermissionKey) {
        this.permissionManager = permissionManager;
        this.permission = globalPermissionKey;
    }

    public final void init(Map<String, String> params) throws PluginParseException {
        super.init(params);
    }

    public final boolean shouldDisplay(final ApplicationUser user, JiraHelper jiraHelper) {
        return cacheConditionResultInRequest(permission(permission, user),
                () -> permissionManager.hasPermission(permission, user));
    }

}
