package com.atlassian.jira.issue.export;

import com.atlassian.annotations.ExperimentalApi;
import com.atlassian.fugue.Option;
import com.google.common.collect.ImmutableList;

import java.util.List;
import java.util.Optional;

/**
 * This represents a Field to be exported and can contain multiple parts within it.
 * <p>
 * It is common to have just one "part" but a complex field may want more than one.
 * For example, a "Money" field might have a "currency" part and an "amount" part.
 * <p>
 * In turn, each part may have a single value or multiple values (for multi-value fields). {@see FieldExportPartsBuilder}
 * for building this object.
 *
 * @since 7.2.0
 */
@ExperimentalApi
public class FieldExportParts {
    private final List<FieldExportPart> parts;

    FieldExportParts(List<FieldExportPart> parts) {
        this.parts = ImmutableList.copyOf(parts);
    }

    /**
     * Each field export can have multiple parts within it that represent a different component of the field. This
     * returns the list of these parts in order that it should be outputted as.
     *
     * @return the list of parts that are needed to represent this field representation.
     */
    public List<FieldExportPart> getParts() {
        return parts;
    }

    /**
     * Each of the parts in the field export have an id to represent it. This returns the part with a given id or an
     * empty {@link Option} if it cannot be found.
     *
     * @param id to find the part for
     * @return optional part
     */
    public Optional<FieldExportPart> getPartWithId(final String id) {
        return parts.stream().filter(part -> part.getId().equals(id)).findAny();
    }
}
