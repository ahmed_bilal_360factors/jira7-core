package com.atlassian.jira.event.user;

import com.atlassian.annotations.PublicApi;
import com.atlassian.jira.user.ApplicationUser;

/**
 * Published when a user successfully logs in.
 */
@PublicApi
public final class LoginEvent extends UserEvent {
    public LoginEvent(ApplicationUser user) {
        super(user, UserEventType.USER_LOGIN);
    }
}
