package com.atlassian.jira.project.type;

import com.atlassian.annotations.PublicApi;

/**
 * Represents a project type defined by a {@link com.atlassian.jira.application.JiraApplication}.
 *
 * @since 7.0
 */
@PublicApi
public final class ProjectType {
    private final ProjectTypeKey key;
    private final String formattedKey;
    private final String descriptionI18nKey;
    private final String icon;
    private final String color;
    private final int weight;

    /**
     * @param key                A unique identifier for a project type.  This should be a semi-human readable string that can be shown in the UI
     *                           of an application in the absence of this pluggable project type definition.
     * @param descriptionI18nKey The i18n description key for this project type
     * @param icon               A Base64 representation of a SVG image that represents the icon for the projects of this type.
     * @param color              A string representing the color to be used on lozenges that are displayed for projects of this type. It should be in hex RGB format (i.e "#AABBCC")
     * @param weight             An integer used for sorting project types when displaying.
     */
    public ProjectType(final ProjectTypeKey key, final String descriptionI18nKey, final String icon, final String color, final int weight) {
        this.key = key;
        this.formattedKey = ProjectTypeKeyFormatter.format(this.key);
        this.descriptionI18nKey = descriptionI18nKey;
        this.icon = icon;
        this.color = color;
        this.weight = weight;
    }

    public ProjectTypeKey getKey() {
        return key;
    }

    /**
     * @return a formatted project type key that can be used in Project Type select lists, etc.
     */
    public String getFormattedKey() {
        return formattedKey;
    }

    public String getDescriptionI18nKey() {
        return descriptionI18nKey;
    }

    public String getIcon() {
        return icon;
    }

    public String getColor() {
        return color;
    }

    public int getWeight() {
        return weight;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final ProjectType that = (ProjectType) o;

        if (weight != that.weight) {
            return false;
        }
        if (color != null ? !color.equals(that.color) : that.color != null) {
            return false;
        }
        if (descriptionI18nKey != null ? !descriptionI18nKey.equals(that.descriptionI18nKey) : that.descriptionI18nKey != null) {
            return false;
        }
        if (icon != null ? !icon.equals(that.icon) : that.icon != null) {
            return false;
        }
        if (key != null ? !key.equals(that.key) : that.key != null) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = key != null ? key.hashCode() : 0;
        result = 31 * result + (formattedKey != null ? formattedKey.hashCode() : 0);
        result = 31 * result + (descriptionI18nKey != null ? descriptionI18nKey.hashCode() : 0);
        result = 31 * result + (icon != null ? icon.hashCode() : 0);
        result = 31 * result + (color != null ? color.hashCode() : 0);
        result = 31 * result + weight;
        return result;
    }

    @Override
    public String toString() {
        return "ProjectType{" +
                "key=" + key +
                ", formattedKey=" + formattedKey + '\'' +
                ", descriptionI18nKey='" + descriptionI18nKey + '\'' +
                ", icon='" + icon + '\'' +
                ", color='" + color + '\'' +
                ", weight=" + weight +
                '}';
    }
}