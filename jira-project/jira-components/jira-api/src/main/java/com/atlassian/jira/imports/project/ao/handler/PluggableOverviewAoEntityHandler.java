package com.atlassian.jira.imports.project.ao.handler;

import com.atlassian.annotations.ExperimentalSpi;
import com.atlassian.jira.imports.project.core.BackupOverviewBuilder;

/**
 * Defines a handler class that will be able to gather data from the backup, that can be used for validation or other purposes,
 * <p>This handler will be called in the initial stages of the import before the user is presented with a list of
 * projects from which to select the project to import.</p>
 * <p>
 * The role of this handler is gather information that is used in the backup that is required to check that the target system
 * is configured so that the import can be correctly completed.</p>
 * <p>For example: A Plugin that provides Test Planning, might store any Test Plans that are referenced by the issues
 * being imported.</p>
 *
 * @since v7.0
 */
@ExperimentalSpi
public interface PluggableOverviewAoEntityHandler extends AoEntityHandler {
    void setBackupOverviewBuilder(BackupOverviewBuilder backupOverviewBuilder);
}
