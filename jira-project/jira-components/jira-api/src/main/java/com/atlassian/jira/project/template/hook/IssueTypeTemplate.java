package com.atlassian.jira.project.template.hook;

import com.atlassian.annotations.PublicApi;

import java.net.URL;
import java.util.Optional;

/**
 * An issue type template used for Project Template configuration.
 *
 * @since 7.0
 */
@PublicApi
public interface IssueTypeTemplate {
    /**
     * Returns the template key of the issue type.
     *
     * @return the key of the issue type
     */
    String key();

    /**
     * Returns the name of the issue type.
     *
     * @return the name
     */
    String name();

    /**
     * Returns the description of the issue type.
     *
     * @return the description
     */
    String description();

    /**
     * Returns the icon path of the issue type.
     *
     * @return the icon path
     */
    String iconPath();

    /**
     * Returns the icon url of the issue type.
     *
     * @return the icon url
     */
    URL iconUrl();

    /**
     * Returns the key of the associated workflow.
     *
     * @return the workflow key
     */
    Optional<String> workflow();

    /**
     * Returns the Issue Type Style. This is empty by default which refers to a Standard Issue Type.
     *
     * @return the issue type style
     */
    String style();

    /**
     * Indicates whether this is a sub-task.
     *
     * @return whether this is a sub-task
     */
    boolean isSubtask();

    /**
     * Returns the name of the screen scheme.
     *
     * @return The name of the screen scheme.
     */
    Optional<String> screenScheme();

    /**
     * Returns the file name of the avatar that should be used for this issue type.
     * If an avatar is specified and exists it should be used instead of an icon.
     *
     * @return The file name of the avatar.
     */
    Optional<String> avatar();
}
