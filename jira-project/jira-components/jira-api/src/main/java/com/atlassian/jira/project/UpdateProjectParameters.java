package com.atlassian.jira.project;

import com.atlassian.fugue.Option;

import javax.annotation.ParametersAreNonnullByDefault;

import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * Creates a mutable version of a project, defined by the inputted projectId, which allows changes to be made for updating
 * the project using the {@link ProjectManager#updateProject(UpdateProjectParameters)}.
 */
@ParametersAreNonnullByDefault
public class UpdateProjectParameters {
    private final Long projectId;

    private Option<String> key = Option.none();
    private Option<String> name = Option.none();
    private Option<String> description = Option.none();
    private Option<String> leadUserKey = Option.none();
    private Option<String> leadUsername = Option.none();
    private Option<String> url = Option.none();
    private Option<Long> assigneeType = Option.none();
    private Option<Long> avatarId = Option.none();
    private Option<String> projectTypeKey = Option.none();
    private Option<Long> projectCategoryId = Option.none();

    public static UpdateProjectParameters forProject(final Long projectId) {
        return new UpdateProjectParameters(projectId);
    }

    private UpdateProjectParameters(final Long projectId) {
        this.projectId = notNull(projectId);
    }

    public UpdateProjectParameters key(String key) {
        this.key = Option.option(key);
        return this;
    }

    public UpdateProjectParameters name(String name) {
        this.name = Option.option(name);
        return this;
    }

    public UpdateProjectParameters description(String description) {
        this.description = Option.option(description);
        return this;
    }

    public UpdateProjectParameters leadUserKey(String leadUserKey) {
        this.leadUserKey = Option.option(leadUserKey);
        return this;
    }

    /**
     * @param leadUsername to set the lead as
     * @return this
     * @deprecated use {@link UpdateProjectParameters#leadUserKey} and {@link UpdateProjectParameters#getLeadUserKey()} instead
     */
    public UpdateProjectParameters leadUsername(String leadUsername) {
        this.leadUsername = Option.option(leadUsername);
        return this;
    }

    public UpdateProjectParameters url(String url) {
        this.url = Option.option(url);
        return this;
    }

    public UpdateProjectParameters assigneeType(Long assigneeType) {
        this.assigneeType = Option.option(assigneeType);
        return this;
    }

    public UpdateProjectParameters avatarId(Long avatarId) {
        this.avatarId = Option.option(avatarId);
        return this;
    }

    public UpdateProjectParameters projectType(String projectType) {
        this.projectTypeKey = Option.option(projectType);
        return this;
    }

    public UpdateProjectParameters projectCategoryId(Long projectCategoryId) {
        this.projectCategoryId = Option.option(projectCategoryId);
        return this;
    }

    public Option<String> getKey() {
        return key;
    }

    public Option<String> getName() {
        return name;
    }

    public Option<String> getDescription() {
        return description;
    }

    public Option<String> getLeadUserKey() {
        return leadUserKey;
    }

    /**
     * @return Optional change for the lead.
     * @deprecated use {@link UpdateProjectParameters#leadUserKey} and {@link UpdateProjectParameters#getLeadUserKey()} instead
     */
    public Option<String> getLeadUsername() {
        return leadUsername;
    }

    public Option<String> getUrl() {
        return url;
    }

    public Option<Long> getAssigneeType() {
        return assigneeType;
    }

    public Option<Long> getAvatarId() {
        return avatarId;
    }

    public Option<String> getProjectTypeKey() {
        return projectTypeKey;
    }

    public Option<Long> getProjectCategoryId() {
        return projectCategoryId;
    }

    public Long getProjectId() {
        return projectId;
    }
}
