package com.atlassian.jira.license;

import com.atlassian.annotations.PublicApi;
import com.atlassian.application.api.ApplicationKey;
import com.atlassian.extras.api.LicenseType;
import com.atlassian.extras.api.jira.JiraLicense;
import com.atlassian.jira.datetime.DateTimeFormatter;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.web.util.OutlookDate;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collection;
import java.util.Date;
import java.util.Map;

/**
 * A set of methods which describe the state of the currently installed license.
 *
 * @since v3.13
 */
@PublicApi
public interface LicenseDetails {
    /**
     * Gets the version of the current license, 0 if not set. This is the version of encoder/decoder that was used with
     * this license.
     *
     * @return the version of the current license, 0 if not set.
     */
    int getLicenseVersion();

    /**
     * @return true for licenses that are entitled to support and updates of JIRA. This includes everything which is not
     * a Personal License or Non-Commercial-Non-Renewable.
     */
    boolean isEntitledToSupport();

    /**
     * If the license is Evaluation or Extended (New Build, Old License), returns true if we are within 7 days of the
     * expiry date. Otherwise, returns true if within 6 weeks before the supported period end date.
     *
     * @return true if the license is close to expiry; false otherwise.
     * @deprecated since 7.0.0 use {@link LicenseDetails#getMaintenanceExpiryDate} instead
     */
    @Deprecated
    boolean isLicenseAlmostExpired();

    /**
     * Retrieve the JiraLicense object corresponding to this license. This will be useful to cross-product code that
     * wants to deal with licenses from different products consistently using the ProductLicense interface (which
     * JiraLicense extends).
     *
     * @return the JiraLicense object corresponding to this license
     */
    JiraLicense getJiraLicense();

    /**
     * Returns all the application/role information contained within this license, even if they are non active.
     *
     * @return the application/role information contained within this license.
     * @since 7.0
     */
    @Nonnull
    LicensedApplications getLicensedApplications();

    /**
     * Returns true if the given {@link ApplicationKey application} is contained within this license.
     *
     * @param application the application to query
     * @return true if the application is found in this license
     * @since 7.0
     */
    boolean hasApplication(@Nonnull ApplicationKey application);

    /**
     * Return the all messages which contain status message.
     *
     * @param i18n        i18n bean
     * @param userManager used to get user context if necessary
     * @return the status message
     */
    LicenseStatusMessage getLicenseStatusMessage(I18nHelper i18n, UserManager userManager);

    /**
     * Get the maintenance message for this license, this does not evaluate the maintenance date, it only provides the
     * message that should be displayed when license out of maintenance for a given JIRA Application. For subscription
     * license no message would be returned {@link com.atlassian.jira.license.LicenseDetails.LicenseStatusMessage#hasAnyMessages()}
     * would return false.
     *
     * @param i18n            i18n helper
     * @param applicationName JIRA Application name (trademark for display)
     * @return the status message, subscription license would return no messages.
     * @since v7.0
     */
    LicenseStatusMessage getMaintenanceMessage(@Nonnull I18nHelper i18n, String applicationName);

    /**
     * Return the HTML message that describes the current status of the license.
     *
     * @param user        the user for whom the message should be i18n'ed
     * @param delimiter   the line delimiter for the message
     * @param userManager used to get user context if necessary
     * @return the status message
     * @deprecated use {@link #getLicenseStatusMessage(com.atlassian.jira.util.I18nHelper, UserManager)}
     */
    @Deprecated
    String getLicenseStatusMessage(@Nullable ApplicationUser user, String delimiter, UserManager userManager);

    /**
     * Return the HTML message that describes the current status of the license.
     *
     * @param i18n        i18n bean
     * @param ignored     ignored
     * @param userManager used to get user context if necessary
     * @param delimiter   the line delimiter for the message
     * @return the status message
     * @deprecated use {@link #getLicenseStatusMessage(com.atlassian.jira.util.I18nHelper, UserManager)}
     */
    @Deprecated
    String getLicenseStatusMessage(I18nHelper i18n, @Nullable OutlookDate ignored, String delimiter, UserManager userManager);

    /**
     * Return the HTML message that briefly describes the expiry status of the license. Intended for use with the Admin
     * Portlet.
     *
     * @param user the user for whom the message should be i18n'ed
     * @return the status message, null for normal license outside of support period
     * @deprecated since 7.0.0 No replacement, generate your own message
     */
    @Deprecated
    String getLicenseExpiryStatusMessage(@Nullable ApplicationUser user);

    /**
     * Return the HTML message that briefly describes the expiry status of the license. Intended for use with the Admin
     * Portlet.
     *
     * @param i18n    i18n bean
     * @param ignored outlookDate bean
     * @return the status message, null for normal license outside of support period
     * @deprecated since 5.0 No replacement, generate your own message
     */
    @Deprecated
    String getLicenseExpiryStatusMessage(I18nHelper i18n, @Nullable OutlookDate ignored);

    /**
     * Return the single word description of the maintenance status of the license. Intended for use with the Support
     * Request and System Info pages.
     *
     * @param i18n i18n bean
     * @return the status message - either "Supported", "Expired" or "Unsupported"
     * @deprecated since 7.0.0 No replacement, generate your own message
     */
    @Deprecated
    String getBriefMaintenanceStatusMessage(I18nHelper i18n);

    /**
     * Returns the maintenance expiry {@link Date} of this license, or null if the maintenance period of this
     * license is unlimited.
     *
     * @return the maintenance expiry {@link Date} of this license, or null if the maintenance period of this
     * license is unlimited.
     * @since 7.0
     */
    @Nullable
    Date getMaintenanceExpiryDate();

    /**
     * Return the date string representing the end of maintenance of the license, whether the license is Evaluation, New
     * Build Old License or otherwise.
     * <p>
     * Note that the return type here is a String to intentionally signify that this value should not be used in any
     * logic calculations and only for displaying to the user.
     *
     * @param outlookDate outlookDate bean
     * @return the date as a string (should never be null)
     * @deprecated since 7.0.0 No replacement, generate your own message
     */
    @Deprecated
    String getMaintenanceEndString(OutlookDate outlookDate);

    /**
     * Tells whether the current license authorise an unlimited number of users.
     *
     * @return {@code true} if the license authorise an unlimited number of users, {@code false} otherwise.
     * @deprecated Use use {@link #getLicensedApplications} and {@link com.atlassian.jira.license.LicensedApplications#getKeys()}
     * {@link com.atlassian.jira.license.LicensedApplications#getUserLimit(com.atlassian.application.api.ApplicationKey)}
     * instead. Since 7.0
     */
    @Deprecated
    boolean isUnlimitedNumberOfUsers();

    /**
     * Returns the literal description of the current license as given in the license itself.
     *
     * @return the literal description of the current license as given in the license itself.
     */
    String getDescription();

    /**
     * @return the Partner name inside the current license or null if its not set
     */
    String getPartnerName();

    /**
     * Checks whether the license is either expired or the grace period for an extended license (after upgrade) is
     * over.
     *
     * @return true if has, false otherwise.
     */
    boolean isExpired();

    /**
     * Gets a nicely formatted purchase date for the current license
     *
     * @param outlookDate the date formatter
     * @return a formatted purchased date.
     * @deprecated Use {@link #getPurchaseDate(DateTimeFormatter)} instead. Since v7.1
     */
    @Deprecated
    String getPurchaseDate(OutlookDate outlookDate);

    /**
     * Gets a nicely formatted purchase date for the current license
     *
     * @param dateTimeFormatter the date formatter
     * @return a formatted purchased date.
     */
    default String getPurchaseDate(DateTimeFormatter dateTimeFormatter) {
        throw new UnsupportedOperationException("Not implemented");
    }

    /**
     * Tells whether this is an evaluation license or not
     *
     * @return {@code true} if this is an evaluation license, {@code false} otherwise.
     */
    boolean isEvaluation();

    /**
     * Tells whether this is a starter license or not
     *
     * @return {@code true} if this is a starter license, {@code false} otherwise.
     */
    boolean isStarter();

    /**
     * Checks whether the license type is a paid type. The paid types are ACADEMIC, COMMERCIAL, and STARTER.
     *
     * @return {@code true} if the license is a paid type, {@code false} otherwise.
     * @since v7.0
     */
    boolean isPaidType();

    /**
     * Tells whether this is a commercial license or not
     *
     * @return {@code true} if this is a commercial license, {@code false} otherwise.
     */
    boolean isCommercial();

    /**
     * Tells whether this is a personal license or not
     *
     * @return {@code true} if this is a personal license, {@code false} otherwise.
     */
    boolean isPersonalLicense();

    /**
     * Tells whether this is a community license or not
     *
     * @return {@code true} if this is a community license, {@code false} otherwise.
     */
    boolean isCommunity();

    /**
     * Tells whether this is an open source license or not
     *
     * @return {@code true} if this is an open source license, {@code false} otherwise.
     */
    boolean isOpenSource();

    /**
     * Tells whether this is a non profit license or not
     *
     * @return {@code true} if this is a non profit license, {@code false} otherwise.
     */
    boolean isNonProfit();

    /**
     * Tells whether this is a demonstration license or not
     *
     * @return {@code true} if this is a demonstration license, {@code false} otherwise.
     */
    boolean isDemonstration();

    /**
     * Tells whether this is an OnDemand license or not
     *
     * @return {@code true} if this is a OnDemand, {@code false} otherwise.
     */
    boolean isOnDemand();

    /**
     * Returns true if this is a DataCenter license.
     *
     * @return true if this is a DataCenter license.
     * @since v6.3
     */
    boolean isDataCenter();

    /**
     * Tells whether this is a developer license or not
     *
     * @return {@code true} if this is a developer license, {@code false} otherwise.
     */
    boolean isDeveloper();

    /**
     * Gets the organisation this license belongs to
     *
     * @return the organisation this license belongs to
     */
    String getOrganisation();

    /**
     * @return the encoded license string that was was decode to produce the current license. This will return null if
     * it is not set
     */
    String getLicenseString();

    /**
     * Tells whether the current build date is within the maintenance of the license
     *
     * @param currentBuildDate the current build date
     * @return {@code true} if the build date is within the maintenance period, {@code false} otherwise.
     */
    boolean isMaintenanceValidForBuildDate(Date currentBuildDate);

    /**
     * Gets the SEN from the license
     *
     * @return the SEN from the license
     */
    String getSupportEntitlementNumber();

    /**
     * Gets the contact people for the license (e.g. Name and Email of whoever first signed up for the OD license)
     *
     * @return collection of contact people for the license
     */
    Collection<LicenseContact> getContacts();

    /**
     * Indicates whether this licence is a subscription-based Enterprise License Agreement (ELA).
     *
     * @return true if this licence is a subscription-based Enterprise License Agreement (ELA).
     * @since v6.3
     */
    boolean isEnterpriseLicenseAgreement();

    /**
     * Returns the number of days until the license expires (i.e. JIRA will enter read-only mode).
     * <p>
     * <ul>
     * <li>A value of {@code 0} indicates that license will expire today (but is still valid).</li>
     * <li>A negative value indicates the number of days the license is expired.</li>
     * <li>A positive value indicates the number of days the license will be valid for.
     * A value of {@link java.lang.Integer#MAX_VALUE} indicates that the license will never expire.</li>
     * </ul>
     * <p>
     * A partial day will be rounded down. For example, {@code 1.5} days will be reported as {@code 1} while
     * {@code -0.5} days will be reported as {@code -1} days.
     *
     * @return the number of days until the license expires (i.e. JIRA will enter read-only mode)
     */
    int getDaysToLicenseExpiry();

    /**
     * Returns the number of days until the license maintenance expires (i.e. support and upgrades will cease).
     * <p>
     * <ul>
     * <li>A value of {@code 0} indicates that license maintenance will expire today (but is still valid).</li>
     * <li>A negative value indicates the number of days the license maintenance is expired.</li>
     * <li>A positive value indicates the number of days the license maintenance will be value for.
     * value of {@link java.lang.Integer#MAX_VALUE} indicates that the license maintenance will never expire.</li>
     * </ul>
     * <p>
     * A partial day will be rounded down. For example, {@code 1.5} days will be reported as {@code 1} while
     * {@code -0.5} days will be reported as {@code -1} days.
     *
     * @return the number of days until the license maintenance expires (i.e. support and upgrades will cease).
     */
    int getDaysToMaintenanceExpiry();

    LicenseType getLicenseType();

    /**
     * Returns a user-friendly {@link String} description of the {@link com.atlassian.application.api.Application}(s)
     * granted by this license. This method differs from {@link #getDescription()} in that the returned String is
     * {@code Application}-focused and heuristically determined, and does not include license type information,
     * number of seats etc.
     * <p>
     * If there are no {@code Application}s in the current license, then this method returns the String {@code "JIRA"}.
     * </p>
     */
    @Nonnull
    String getApplicationDescription();

    /**
     * Encapsulates a collection of unordered, internationalised license-related status messages.
     */
    interface LicenseStatusMessage {
        String getAllMessages(String delimiter);

        Map<String, String> getAllMessages();

        boolean hasMessageOfType(String messageKey);

        /**
         * Determines whether this {@link com.atlassian.jira.license.LicenseDetails.LicenseStatusMessage} contains any
         * messages.
         *
         * @return true if there are messages, false no messages
         */
        boolean hasAnyMessages();
    }

    /**
     * Represents a license contact name/email pair.
     */
    interface LicenseContact {
        String getName();

        String getEmail();
    }
}
