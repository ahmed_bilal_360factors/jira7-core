package com.atlassian.jira.project.template;

import com.atlassian.annotations.PublicApi;
import com.atlassian.jira.project.template.hook.AddProjectHook;
import com.atlassian.jira.project.template.hook.AddProjectModule;
import com.atlassian.jira.project.template.hook.EmptyAddProjectHook;
import com.atlassian.jira.project.type.ProjectTypeKey;

import javax.annotation.Nonnull;

import static org.apache.commons.lang3.StringUtils.defaultString;

/**
 * Class representing a project template.
 *
 * @since 7.0
 */
@PublicApi
public class ProjectTemplate implements Comparable<ProjectTemplate> {
    private final ProjectTemplateKey key;
    private final ProjectTypeKey projectTypeKey;
    private final int weight;
    private final String name;
    private final String description;
    private final String longDescriptionContent;
    private final String iconUrl;
    private final String backgroundIconUrl;
    private final AddProjectModule addProjectModule;
    private final String infoSoyPath;

    public ProjectTemplate(
            final ProjectTemplateKey key,
            final ProjectTypeKey projectTypeKey,
            final int weight,
            final String name,
            final String description,
            final String longDescriptionContent,
            final String iconUrl,
            final String backgroundIconUrl,
            final AddProjectModule addProjectModule,
            final String infoSoyPath) {
        this.key = key;
        this.projectTypeKey = projectTypeKey;
        this.weight = weight;
        this.name = defaultString(name);
        this.description = defaultString(description);
        this.longDescriptionContent = defaultString(longDescriptionContent);
        this.iconUrl = defaultString(iconUrl);
        this.backgroundIconUrl = defaultString(backgroundIconUrl);
        this.addProjectModule = addProjectModule;
        this.infoSoyPath = infoSoyPath;
    }

    public ProjectTemplateKey getKey() {
        return key;
    }

    public ProjectTypeKey getProjectTypeKey() {
        return projectTypeKey;
    }

    public int getWeight() {
        return weight;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getLongDescriptionContent() {
        return longDescriptionContent;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public String getBackgroundIconUrl() {
        return backgroundIconUrl;
    }

    public Boolean getCreateProject() {
        return addProjectModule != null;
    }

    public Boolean isHasCreateHook() {
        return getCreateProject() && addProjectModule.hasProjectCreateHook();
    }

    @Nonnull
    public AddProjectHook getAddProjectHook() {
        if (!isHasCreateHook()) {
            return new EmptyAddProjectHook();
        }

        return addProjectModule.addProjectHook();
    }

    public boolean hasAddProjectModule() {
        return addProjectModule != null;
    }

    public AddProjectModule addProjectModule() {
        return addProjectModule;
    }

    public String getInfoSoyPath() {
        return infoSoyPath;
    }

    @Override
    public int compareTo(ProjectTemplate that) {
        int difference = this.weight - that.weight;
        if (difference == 0) {
            // ensure sort is stable if weights are equal
            return name.compareTo(that.name);
        }

        return difference;
    }
}
