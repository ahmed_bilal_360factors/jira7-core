package com.atlassian.jira.issue.fields.layout.column;

import com.atlassian.jira.user.ApplicationUser;

public interface UserColumnLayout extends ColumnLayout {
    ApplicationUser getUser();
}
