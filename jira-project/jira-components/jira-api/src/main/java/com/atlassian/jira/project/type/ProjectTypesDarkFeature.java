package com.atlassian.jira.project.type;

import com.atlassian.annotations.Internal;

/**
 * Dark feature manager for project types.
 *
 * @Deprecated Project type is enabled by default after JIRA 7, so we don't need it any more. Since 7.0
 * @since 7.0
 */
@Internal
public interface ProjectTypesDarkFeature {
    /**
     * Returns a boolean indicating whether the project types feature is enabled on the instance.
     *
     * @return whether the project types feature is enabled.
     */
    boolean isEnabled();
}
