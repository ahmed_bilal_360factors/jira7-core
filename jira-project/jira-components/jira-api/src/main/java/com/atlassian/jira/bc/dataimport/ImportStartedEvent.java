package com.atlassian.jira.bc.dataimport;

import com.atlassian.annotations.PublicApi;
import com.atlassian.fugue.Option;

/**
 * Raised before a JIRA XML import is performed.
 *
 * @since v5.1
 */
@PublicApi
public final class ImportStartedEvent implements DataImportEvent {
    private final ImportType xmlImportType;
    private Option<Long> xmlExportTime;

    @Deprecated
    public ImportStartedEvent() {
        this(Option.<Long>none());
    }

    public ImportStartedEvent(Option<Long> xmlExportTime) {
        this.xmlExportTime = xmlExportTime;
        this.xmlImportType = ImportType.UNKNOWN;
    }

    public ImportStartedEvent(Option<Long> xmlExportTime, final ImportType xmlImportType) {
        this.xmlExportTime = xmlExportTime;
        this.xmlImportType = xmlImportType;
    }

    @Override
    public Option<Long> getXmlExportTime() {
        return xmlExportTime;
    }

    /**
     * @return a String indicating if the import was from SERVER or CLOUD
     */
    public ImportType getXmlImportType() {
        return xmlImportType;
    }
}
