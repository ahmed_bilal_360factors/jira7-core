package com.atlassian.jira.cluster.zdu;

import com.atlassian.annotations.ExperimentalApi;

/**
 * This event is fired when the cluster enters {@link UpgradeState#STABLE} due to cancellation.
 *
 * @since v7.3
 * @see UpgradeState
 */
@ExperimentalApi
public class JiraUpgradeCancelledEvent {
    private final NodeBuildInfo nodeBuildInfo;

    public JiraUpgradeCancelledEvent(final NodeBuildInfo nodeBuildInfo) {
        this.nodeBuildInfo = nodeBuildInfo;
    }

    public NodeBuildInfo getNodeBuildInfo() {
        return nodeBuildInfo;
    }
}
