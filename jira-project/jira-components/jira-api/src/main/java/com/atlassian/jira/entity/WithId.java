package com.atlassian.jira.entity;

import com.atlassian.annotations.PublicApi;

import java.util.Comparator;

import static java.util.Comparator.comparing;
import static java.util.Comparator.naturalOrder;
import static java.util.Comparator.nullsFirst;

/**
 * Entities implementing this interface are supposed to be uniquely identifiable by id.
 *
 * @since v6.2
 */
@PublicApi
public interface WithId {
    /**
     * @return the unique id of the entity.
     */
    Long getId();

    Comparator<WithId> ID_COMPARATOR = comparing(WithId::getId, nullsFirst(naturalOrder()));
}
