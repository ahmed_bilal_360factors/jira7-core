package com.atlassian.jira.bc.dataimport;

import com.atlassian.annotations.PublicApi;
import com.atlassian.jira.user.ApplicationUser;

/**
 * Event raised when a data export begins.
 *
 * @since v5.0
 */
@PublicApi
public class ExportStartedEvent implements DataExportEvent {
    /**
     * The user that instigated the export. May be null if, for instance, it is triggered by a scheduled job and not a
     * user.
     */
    public final ApplicationUser loggedInUser;

    /**
     * The filename the data is being saved to.
     */
    public final String filename;

    /**
     * The time in milliseconds when the export was started..
     */
    public final Long xmlExportTime;

    public ExportStartedEvent(final ApplicationUser user, final String filename, final Long xmlExportTime) {
        this.loggedInUser = user;
        this.filename = filename;
        this.xmlExportTime = xmlExportTime;
    }

    @Override
    public Long getXmlExportTime() {
        return xmlExportTime;
    }
}
