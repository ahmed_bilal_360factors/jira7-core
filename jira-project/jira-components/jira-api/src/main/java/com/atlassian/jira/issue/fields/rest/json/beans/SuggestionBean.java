package com.atlassian.jira.issue.fields.rest.json.beans;

import com.atlassian.annotations.PublicApi;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.Objects;

/**
 * Represents possible value (suggestion) for single select field.
 *
 * @since 7.0
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@PublicApi
public class SuggestionBean {
    @JsonProperty
    private String label;

    @JsonProperty
    private String value;

    @JsonProperty
    private String icon;

    @JsonProperty
    private boolean selected;

    public SuggestionBean(final String label, final String value, final String icon, final boolean selected) {
        this.label = label;
        this.value = value;
        this.icon = icon;
        this.selected = selected;
    }

    public String getLabel() {
        return label;
    }

    public String getValue() {
        return value;
    }

    public String getIcon() {
        return icon;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(final boolean selected) {
        this.selected = selected;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final SuggestionBean that = (SuggestionBean) o;
        return Objects.equals(selected, that.selected) &&
                Objects.equals(label, that.label) &&
                Objects.equals(value, that.value) &&
                Objects.equals(icon, that.icon);
    }

    @Override
    public int hashCode() {
        return Objects.hash(label, value, icon, selected);
    }
}
