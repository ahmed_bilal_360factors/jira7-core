package com.atlassian.jira.project.type;

import com.atlassian.annotations.PublicSpi;
import com.atlassian.jira.project.Project;

/**
 * Interface to be implemented by clients who wish to be notified once the project type of a project has been updated.
 * An object of this type must be returned from the call to {@link ProjectTypeUpdatedHandler#projectTypeUpdated(Project, ProjectType, ProjectType)}
 *
 * @since 7.0
 */
@PublicSpi
public interface ProjectTypeUpdatedOutcome {
    /**
     * Indicates whether the {@link ProjectTypeUpdatedHandler#projectTypeUpdated(Project, ProjectType, ProjectType)}
     * call succeeded or failed.
     *
     * @return whether the {@link ProjectTypeUpdatedHandler#projectTypeUpdated(Project, ProjectType, ProjectType)} call
     * succeeded or failed.
     */
    boolean isSuccessful();
}
