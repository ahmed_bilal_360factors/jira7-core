package com.atlassian.jira.notification;

import com.atlassian.annotations.PublicApi;
import com.atlassian.jira.notification.type.NotificationType;
import com.atlassian.jira.security.roles.ProjectRole;

/**
 * Notification to the configured project role.
 *
 * @since 7.0
 */
@PublicApi
public class ProjectRoleNotification extends AbstractNotification {
    private final ProjectRole projectRole;

    public ProjectRoleNotification(final Long id, final NotificationType notificationType, final ProjectRole projectRole, final String parameter) {
        super(id, notificationType, parameter);
        this.projectRole = projectRole;
    }

    public ProjectRole getProjectRole() {
        return projectRole;
    }

    @Override
    public <X> X accept(final NotificationVisitor<X> notificationVisitor) {
        return notificationVisitor.visit(this);
    }
}
