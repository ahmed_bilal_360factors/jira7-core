package com.atlassian.jira.security;

import com.atlassian.annotations.ExperimentalApi;
import com.atlassian.annotations.Internal;
import com.atlassian.annotations.PublicApi;
import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.fugue.Option;
import com.atlassian.jira.exception.RemoveException;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.status.Status;
import com.atlassian.jira.permission.ProjectPermission;
import com.atlassian.jira.permission.ProjectPermissionCategory;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectCategory;
import com.atlassian.jira.security.plugin.ProjectPermissionKey;
import com.atlassian.jira.user.ApplicationUser;
import com.opensymphony.workflow.loader.ActionDescriptor;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collection;

/**
 * PermissionManager responsible for all project specific permissions.
 * <p>
 * See  <a href="http://www.atlassian.com/software/jira/docs/latest/permissions.html">JIRA Permissions</a>.
 * <p>
 * For all global Permissions it is recommended to use {@link GlobalPermissionManager}.
 */
@PublicApi
public interface PermissionManager {
    /**
     * @return all project permissions.
     * @since v6.3
     */
    Collection<ProjectPermission> getAllProjectPermissions();

    /**
     * @param category project permission category.
     * @return all project permissions of the specified category.
     * @since v6.3
     */
    Collection<ProjectPermission> getProjectPermissions(@Nonnull ProjectPermissionCategory category);

    /**
     * Returns a project permission matching the specified key.
     *
     * @param permissionKey A project permission key.
     * @return a project permission for the given permission key.
     * {@link Option#none} if there is no permission with this key.
     * @since v6.3
     */
    Option<ProjectPermission> getProjectPermission(@Nonnull ProjectPermissionKey permissionKey);

    /**
     * Checks to see if this user has the specified permission. It will check only global permissions as there are
     * no other permissions to check.
     *
     * @param permissionsId permission id
     * @param user          user, can be null - anonymous user
     * @return true if user is granted given permission, false otherwise
     * @see com.atlassian.jira.security.GlobalPermissionManager#hasPermission(int, ApplicationUser)
     * @deprecated Use {@link GlobalPermissionManager#hasPermission(com.atlassian.jira.permission.GlobalPermissionKey, com.atlassian.jira.user.ApplicationUser)} instead. Since v6.2.5.
     */
    @Deprecated
    public boolean hasPermission(int permissionsId, ApplicationUser user);

    /**
     * Checks to see if this user has permission to see the specified issue.
     * <p>
     * Note that if the issue's generic value is null, it is assumed that the issue is currently being created, and so
     * the permission check call is deferred to the issue's project object, with the issueCreation flag set to true. See
     * JRA-14788 for more info.
     *
     * @param permissionsId Not a global permission
     * @param issue         The Issue (cannot be null)
     * @param user          User object, possibly null if JIRA is accessed anonymously
     * @return True if there are sufficient rights to access the entity supplied
     * @deprecated Use {@link #hasPermission(ProjectPermissionKey, Issue, ApplicationUser)} instead. Since v6.3.
     */
    @Deprecated
    public boolean hasPermission(int permissionsId, Issue issue, ApplicationUser user);

    /**
     * Checks to see if this user has permission to see the specified issue.
     * <p>
     * Note that if the issue's generic value is null, it is assumed that the issue is currently being created, and so
     * the permission check call is deferred to the issue's project object, with the issueCreation flag set to true. See
     * JRA-14788 for more info.
     *
     * @param permissionKey Not a global permission key
     * @param issue         The Issue (cannot be null)
     * @param user          User object, possibly null if JIRA is accessed anonymously
     * @return True if there are sufficient rights to access the entity supplied
     * @since v6.3
     */
    boolean hasPermission(@Nonnull ProjectPermissionKey permissionKey, @Nonnull Issue issue, @Nullable ApplicationUser user);

    /**
     * Checks to see if this user has the given permission to the specified issue after the given workflow transition takes effect.
     *
     * @param permissionKey    The project permission key.
     * @param issue            The Issue (cannot be null)
     * @param user             User object, possibly null if JIRA is accessed anonymously
     * @param actionDescriptor Represents the current workflow transition
     * @return True if there are sufficient rights to access the entity supplied
     * @since v7.0.1
     */
    @Internal
    boolean hasPermission(@Nonnull ProjectPermissionKey permissionKey, @Nonnull Issue issue, @Nullable ApplicationUser user, @Nullable ActionDescriptor actionDescriptor);

    /**
     * Checks to see if this user has the given permission to the specified issue if the issue was in the given status.
     * <p>
     * This method is useful during a workflow transition to check what the permissions will be in the new status, or (after the status is updated in the Issue object)
     * to check what the permission would have been in the old status.
     *
     * @param permissionKey    The project permission key.
     * @param issue            The Issue (cannot be null)
     * @param user             User object, possibly null if JIRA is accessed anonymously
     * @param status           Represents the state we are checking permissions against
     * @return True if there are sufficient rights to access the entity supplied
     * @since v7.0.3
     */
    boolean hasPermission(@Nonnull ProjectPermissionKey permissionKey, @Nonnull Issue issue, @Nullable ApplicationUser user, @Nonnull Status status);

    /**
     * Checks whether the specified user has a specified permission within the context of a specified project.
     *
     * @param permissionsId A non-global permission, i.e. a permission that is granted via a project context
     * @param project       The project that is the context of the permission check.
     * @param user          The person to perform the permission check for
     * @return true if the user has the specified permission in the context of the supplied project
     * @deprecated Use {@link #hasPermission(ProjectPermissionKey, Project, ApplicationUser)} instead. Since v6.3.
     */
    @Deprecated
    public boolean hasPermission(int permissionsId, Project project, ApplicationUser user);

    /**
     * Checks whether the specified user has a specified permission within the context of a specified project.
     *
     * @param permissionKey A non-global permission, i.e. a permission that is granted via a project context
     * @param project       The project that is the context of the permission check.
     * @param user          The person to perform the permission check for
     * @return true if the user has the specified permission in the context of the supplied project
     * @see #hasProjectWidePermission(ProjectPermissionKey, Project, ApplicationUser)
     * @since v6.3
     */
    boolean hasPermission(@Nonnull ProjectPermissionKey permissionKey, @Nonnull Project project, @Nullable ApplicationUser user);

    /**
     * Checks whether a user has a particular permission in the given project.
     * <p>
     * This method returns a tri-state enum in order to convey information about permissions that are granted on a per-issue basis.
     * <ul>
     * <li>ALL_ISSUES : this user has the given permission for all issues in this project</li>
     * <li>NO_ISSUES : this user definitely does not have the given permission for any issues in this project</li>
     * <li>ISSUE_SPECIFIC : the user may have this permission on some issues, but not others</li>
     * </ul>
     * Note that even if this method returns {@code ISSUE_SPECIFIC}, it may be that there are no issues for which the
     * user has the permission granted.
     * </p>
     *
     * @param permissionKey A project permission
     * @param project       The project that is the context of the permission check.
     * @param user          The person to perform the permission check for (null means anonymous)
     * @return {@code ALL_ISSUES}, {@code NO_ISSUES}, or {@code ISSUE_SPECIFIC}
     * @see #hasPermission(ProjectPermissionKey, Project, ApplicationUser)
     * @since v6.4.8
     */
    @ExperimentalApi
    @Nonnull
    ProjectWidePermission hasProjectWidePermission(@Nonnull ProjectPermissionKey permissionKey, @Nonnull Project project, @Nullable ApplicationUser user);

    /**
     * Checks whether the specified user has a specified permission within the context of a specified project.
     *
     * @param permissionsId A non-global permission, i.e. a permission that is granted via a project context
     * @param project       The project that is the context of the permission check.
     * @param user          The person to perform the permission check for
     * @param issueCreation Whether this permission is being checked during issue creation
     * @return true if the user has the specified permission in the context of the supplied project
     * @deprecated Use {@link #hasPermission(ProjectPermissionKey, Project, ApplicationUser, boolean)} instead. Since v6.3.
     */
    @Deprecated
    public boolean hasPermission(int permissionsId, Project project, ApplicationUser user, boolean issueCreation);

    /**
     * Checks whether the specified user has a specified permission within the context of a specified project.
     *
     * @param permissionKey A non-global permission, i.e. a permission that is granted via a project context
     * @param project       The project that is the context of the permission check.
     * @param user          The person to perform the permission check for
     * @param issueCreation Whether this permission is being checked during issue creation
     * @return true if the user has the specified permission in the context of the supplied project
     * @since v6.3
     */
    boolean hasPermission(@Nonnull ProjectPermissionKey permissionKey, @Nonnull Project project, @Nullable ApplicationUser user, boolean issueCreation);

    /**
     * Remove all permissions that have used this group
     *
     * @param group The name of the group that needs to be removed, must NOT be null and must be a real group
     * @throws RemoveException if permission removal fails
     */
    public void removeGroupPermissions(String group) throws RemoveException;

    /**
     * Remove all permissions that have been assigned to this user
     *
     * @param user the user whose permissions are to be removed
     * @throws RemoveException
     * @since v6.0
     */
    public void removeUserPermissions(ApplicationUser user) throws RemoveException;

    /////////////// Project Permission Methods //////////////////////////////////////////

    /**
     * Can this user see at least one project with this permission
     *
     * @param permissionId must NOT be a global permission
     * @param user         user being checked
     * @return true the given user can see at least one project with the given permission, false otherwise
     * @deprecated Use {@link #hasProjects(ProjectPermissionKey, ApplicationUser)} instead. Since v6.3.
     */
    @Deprecated
    public boolean hasProjects(int permissionId, ApplicationUser user);

    /**
     * Can this user see at least one project with this permission
     *
     * @param permissionKey must NOT be a global permission
     * @param user          user being checked
     * @return true the given user can see at least one project with the given permission, false otherwise
     * @since v6.3
     */
    boolean hasProjects(@Nonnull ProjectPermissionKey permissionKey, @Nullable ApplicationUser user);

    /**
     * Retrieve a list of project objects this user has the permission for
     *
     * @param permissionId must NOT be a global permission
     * @param user         user
     * @return a collection of {@link Project} objects
     * @deprecated Use {@link #getProjects(ProjectPermissionKey, ApplicationUser)} instead. Since v6.3.
     */
    @Deprecated
    public Collection<Project> getProjects(int permissionId, ApplicationUser user);

    /**
     * Retrieve a list of project objects this user has the permission for
     *
     * @param permissionKey must NOT be a global permission
     * @param user          user
     * @return a collection of {@link Project} objects
     * @since v6.3
     */
    Collection<Project> getProjects(@Nonnull ProjectPermissionKey permissionKey, @Nullable ApplicationUser user);

    /**
     * Returns the list of projects associated with the specified category, that this user has the permissions for.
     *
     * @param permissionId    permission id
     * @param user            user
     * @param projectCategory the ProjectCategory
     * @return the list of projects associated with the specified category, that this user has the permissions for.
     * @deprecated Use {@link #getProjects(ProjectPermissionKey, ApplicationUser, ProjectCategory)} instead. Since v6.3.
     */
    @Deprecated
    public Collection<Project> getProjects(int permissionId, ApplicationUser user, ProjectCategory projectCategory);

    /**
     * Returns the list of projects associated with the specified category, that this user has the permissions for.
     *
     * @param permissionKey   permission key
     * @param user            user
     * @param projectCategory the ProjectCategory - null means find projects with no category.
     * @return the list of projects associated with the specified category, that this user has the permissions for.
     * @since v6.3
     */
    Collection<Project> getProjects(@Nonnull ProjectPermissionKey permissionKey, @Nullable ApplicationUser user, @Nullable ProjectCategory projectCategory);

    /**
     * Flushes any cached project permissions for all users.
     *
     * @since v7.1
     */
    @ExperimentalApi
    void flushCache();

    /////////////// Group Permission Methods //////////////////////////////////////////

    /**
     * Retrieve all groups that are used in the permission globally and in the project.
     *
     * @param permissionId permission id
     * @param project      project from which to retrieve groups
     * @return a collection of Groups
     */
    public Collection<Group> getAllGroups(int permissionId, Project project);
}
