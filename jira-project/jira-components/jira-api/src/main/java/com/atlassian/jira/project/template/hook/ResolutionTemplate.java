package com.atlassian.jira.project.template.hook;

import com.atlassian.annotations.PublicApi;

/**
 * A resolution template used for Project Template configuration.
 *
 * @since 7.0
 */
@PublicApi
public interface ResolutionTemplate {
    /**
     * Returns the name of the resolution.
     *
     * @return the name
     */
    String name();

    /**
     * Returns the description of the resolution.
     *
     * @return the description
     */
    String description();
}
