package com.atlassian.jira.event.mau;

import com.atlassian.annotations.ExperimentalApi;
import com.atlassian.jira.project.Project;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * Service to help with triggering eyeball events used to define monthly active users in JIRA.
 * <p>
 * Implementations should allow clients to set an {@link MauApplicationKey} for the current request/thread. Clients
 * can then trigger an event with the correct application set on the event using the {@link #triggerEvent()}
 * method. This will usually be done at the end of the servlet filter chain or explicitly in some rare circumstances.
 *
 * @since v7.0.1
 */
@ExperimentalApi
public interface MauEventService {
    /**
     * Stores the provided application key in a thread local store to be retrieved later in the same thread when calling
     * {@link #triggerEvent()}
     *
     * @param applicationKey the application servicing the current request
     */
    void setApplicationForThread(@Nonnull MauApplicationKey applicationKey);

    /**
     * Convencience method that does the same as {@link #setApplicationForThread(MauApplicationKey)} by looking up
     * the appropriate {@link com.atlassian.application.api.ApplicationKey} based on the project type of the passed in
     * project
     *
     * @param project the project to use to look up the application key
     */
    void setApplicationForThreadBasedOnProject(@Nullable Project project);

    /**
     * Collects together and then returns the Application key, email address and userId for the current MAU request
     * @return LastSentKey containing the key, email address and userID for the current mau requets
     */
    LastSentKey getKeyWithCurrentApplication();

    /**
     * Triggers an {@link com.atlassian.analytics.api.events.MauEvent} using the application previously set for this
     * request using {@link #setApplicationForThread(MauApplicationKey)} or {@link
     * #setApplicationForThreadBasedOnProject(Project)}.
     */
    void triggerEvent();
}