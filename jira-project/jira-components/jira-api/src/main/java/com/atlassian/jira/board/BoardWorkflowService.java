package com.atlassian.jira.board;

import com.atlassian.annotations.ExperimentalApi;
import com.atlassian.jira.issue.status.Status;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.workflow.JiraWorkflow;
import com.atlassian.query.Query;

import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.Set;

/**
 * Provides workflow-related service methods.
 *
 * @since 7.1
 */
@ExperimentalApi
public interface BoardWorkflowService {
    /**
     * Get the set of accessible statuses for issues potentially returned by the current query. In effect we are asking:
     * "What projects and issue types could be returned by this query for this user, and what statuses are linked to
     * workflows assigned to those projects and issue types?"
     *
     * @param user  the user who would be performing the search
     * @param query the query being asked about
     * @return the set of statuses
     */
    Set<Status> getAccessibleStatuses(ApplicationUser user, Query query);

    /**
     * Get all active workflow statuses available in the instance.
     *
     * @return the set of statuses
     */
    Set<Status> getAllActiveWorkflowStatuses();

    /**
     * Given a project, returns all JIRA workflows which are associated to that project.
     * Empty on error.
     *
     * @param projectObj the project to get all associated workflows for
     * @return the collection of JIRA workflows
     */
    Collection<JiraWorkflow> getJiraWorkflows(Project projectObj);

    /**
     * Given a project and an issue type id, returns the JIRA Workflows which are associated to that project for that
     * issue type.
     * Empty on error.
     *
     * @param projectObj  the project to get workflows for
     * @param issueTypeId the issue type id to get workflows for
     * @return the collection of JIRA workflows
     */
    Collection<JiraWorkflow> getJiraWorkflows(Project projectObj, String issueTypeId);

    /**
     * Given a user and query, return the set of initial statuses of all workflows of issues in that query.
     *
     * @param user  the user performing the search
     * @param query the query being asked about
     * @return the set of initial statuses
     */
    Set<Status> getInitialStatusesForQuery(ApplicationUser user, Query query);

    /**
     * Given a user and query, return all JIRA workflows of issues in that query.
     *
     * @param user  the user performing the search
     * @param query the query being asked about
     * @return the collection of JIRA workflows
     */
    @NotNull
    Collection<JiraWorkflow> getWorkflowsForQuery(@NotNull ApplicationUser user, @NotNull Query query);
}
