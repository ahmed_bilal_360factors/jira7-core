package com.atlassian.jira.issue.fields.rest.json.serializer;

import com.atlassian.jira.bc.issue.comment.CommentService;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.datetime.DateTimeFormatterFactory;
import com.atlassian.jira.issue.RendererManager;
import com.atlassian.jira.issue.comments.Comment;
import com.atlassian.jira.issue.fields.rest.json.beans.CommentJsonBean;
import com.atlassian.jira.issue.fields.rest.json.beans.JiraBaseUrls;
import com.atlassian.jira.issue.fields.rest.json.beans.StreamingCommentsJsonBean;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.roles.ProjectRoleManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.EmailFormatter;
import com.google.common.annotations.VisibleForTesting;
import org.apache.commons.lang.mutable.MutableInt;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.map.JsonSerializer;
import org.codehaus.jackson.map.SerializerProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * This class is used to serialize comments directly into JSON without loading all comments into memory at once.
 *
 * @since 7.2
 */
public class StreamingCommentsJsonBeanSerializer extends JsonSerializer<StreamingCommentsJsonBean> {
    private static final Logger LOGGER = LoggerFactory.getLogger(StreamingCommentsJsonBeanSerializer.class);

    private final CommentService commentService = ComponentAccessor.getComponent(CommentService.class);
    private final JiraAuthenticationContext authenticationContext = ComponentAccessor.getJiraAuthenticationContext();
    private final JiraBaseUrls jiraBaseUrls = ComponentAccessor.getComponent(JiraBaseUrls.class);
    private final ProjectRoleManager projectRoleManager = ComponentAccessor.getComponent(ProjectRoleManager.class);
    private final EmailFormatter emailFormatter = ComponentAccessor.getComponent(EmailFormatter.class);
    private final DateTimeFormatterFactory dateTimeFormatterFactory = ComponentAccessor.getComponent(DateTimeFormatterFactory.class);
    private final RendererManager rendererManager = ComponentAccessor.getRendererManager();

    @Override
    public void serialize(StreamingCommentsJsonBean serializableCommentsBean, JsonGenerator jsonGenerator, SerializerProvider provider) throws IOException {
        Long issueId = serializableCommentsBean.getIssue().getId();
        LOGGER.debug("Started serialization of comments for issue id: " + issueId);
        jsonGenerator.writeStartObject();
        jsonGenerator.writeFieldName("comments");
        jsonGenerator.writeStartArray();

        MutableInt counter = new MutableInt();
        ApplicationUser loggedInUser = authenticationContext.getLoggedInUser();
        commentService.streamComments(loggedInUser, serializableCommentsBean.getIssue())
                .peek(comment -> counter.increment())
                .forEach(comment -> {
                    CommentJsonBean commentJsonBean;
                    if (serializableCommentsBean.isRendered()) {
                        commentJsonBean = getRenderedCommentJsonBean(serializableCommentsBean, comment, loggedInUser);
                    } else {
                        commentJsonBean = getShortCommentJsonBean(comment, loggedInUser);
                    }
                    try {
                        jsonGenerator.writeObject(commentJsonBean);
                    } catch (IOException ioException) {
                        throw new RuntimeException("Encountered an error while trying to serialize a comment", ioException);
                    }
                });

        jsonGenerator.writeEndArray();
        int commentCount = counter.intValue();
        jsonGenerator.writeNumberField("maxResults", commentCount);
        jsonGenerator.writeNumberField("total", commentCount);
        jsonGenerator.writeNumberField("startAt", 0);
        jsonGenerator.writeEndObject();
        LOGGER.debug("Finished serialization of comments for issue id: " + issueId);
    }

    @VisibleForTesting
    protected CommentJsonBean getShortCommentJsonBean(Comment comment, ApplicationUser loggedInUser) {
        return CommentJsonBean.shortBean(comment, jiraBaseUrls, projectRoleManager, loggedInUser, emailFormatter);
    }

    @VisibleForTesting
    protected CommentJsonBean getRenderedCommentJsonBean(StreamingCommentsJsonBean serializableCommentsBean, Comment comment, ApplicationUser loggedInUser) {
        return CommentJsonBean.renderedShortBean(
                comment,
                jiraBaseUrls,
                projectRoleManager,
                dateTimeFormatterFactory,
                rendererManager,
                serializableCommentsBean.getRendererType(),
                serializableCommentsBean.getIssue().getIssueRenderContext(),
                loggedInUser,
                emailFormatter
        );
    }
}
