package com.atlassian.jira.board;

import com.atlassian.annotations.ExperimentalApi;

import java.util.Objects;

/**
 * Represents board id.
 * @since v7.1
 */
@ExperimentalApi
public final class BoardId {
    private long id;

    public BoardId(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BoardId boardId = (BoardId) o;
        return Objects.equals(id, boardId.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "BoardId{" +
                "id=" + id +
                '}';
    }
}
