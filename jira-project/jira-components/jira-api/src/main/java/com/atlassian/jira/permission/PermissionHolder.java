package com.atlassian.jira.permission;

import com.atlassian.annotations.PublicApi;
import com.atlassian.fugue.Option;
import com.google.common.base.Objects;
import com.google.common.base.Preconditions;

import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;

import static com.atlassian.fugue.Option.option;
import static com.google.common.base.Strings.emptyToNull;
import static java.lang.String.format;

/**
 * A person or a group of people that holds a permission.
 * <p>
 * When you grant a permission, you need to define to whom you grant it. {@code PermissionHolder} lets you do that.
 * </p>
 * <p>
 * It consists of two things:
 * <dl>
 * <dt>{@link PermissionHolderType} type</dt> <dd>
 * Type of the permission holder. This can be e.g. <b>user</b>, <b>group</b>, <b>project role</b> but also more ephemeral things like
 * <b>issue assignee</b> or <b>reporter</b>.
 * <p>
 * Standard JIRA reporter types are gathered in the {@link JiraPermissionHolderType} enum.
 * </p>
 * </dd>
 * <p>
 * <dt>{@code String} parameter</dt> <dd>
 * Some types require a parameter (e.g. when the type is <b>user</b>, the parameter needs to
 * contain the user id, <b>group</b> requires the group id, etc.) and some don't (e.g. <b>assignee</b>, <b>reporter</b>, <b>project lead</b>).
 * <p>
 * Note that each {@link PermissionHolderType} has a {@link PermissionHolderType#requiresParameter()} method, which specifies whether it requires
 * a parameter or not. It is not possible to create a {@code PermissionHolder} without a parameter when the type requires one, and <i>vice versa</i>.
 * </p>
 * </dd>
 * </dl>
 * </p>
 */
@Immutable
@PublicApi
public final class PermissionHolder {
    private final PermissionHolderType type;
    private final String parameter;

    private PermissionHolder(final PermissionHolderType type, @Nullable final String parameter) {
        this.type = type;
        this.parameter = parameter;
    }

    public static PermissionHolder holder(PermissionHolderType type) {
        return holder(type, null);
    }

    public static PermissionHolder holder(PermissionHolderType type, @Nullable String parameter) {
        parameter = emptyToNull(parameter);
        Preconditions.checkArgument(type.requiresParameter() && parameter != null || !type.requiresParameter() && parameter == null,
                "Parameter should be set if and only if type requires it. Type was: " + type + ", parameter was: " + parameter);

        return new PermissionHolder(type, parameter);
    }

    public PermissionHolderType getType() {
        return type;
    }

    public Option<String> getParameter() {
        return option(parameter);
    }

    @Override
    public String toString() {
        return format("(%s, %s)", getType(), getParameter());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PermissionHolder that = (PermissionHolder) o;

        return Objects.equal(this.type, that.type) &&
                Objects.equal(this.parameter, that.parameter);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(type, parameter);
    }
}
