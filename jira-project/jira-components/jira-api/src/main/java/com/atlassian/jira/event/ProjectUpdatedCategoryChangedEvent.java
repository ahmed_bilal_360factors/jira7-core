package com.atlassian.jira.event;

import com.atlassian.analytics.api.annotations.EventName;
import com.atlassian.annotations.Internal;
import com.atlassian.annotations.PublicApi;
import com.atlassian.jira.web.action.RequestSourceType;

import javax.annotation.ParametersAreNonnullByDefault;

/**
 * Event detailing when the association between a project and a category is changed.
 * It is fired ONLY considering a project's perspective, in which the project was updated to be associated to a different category.
 * NOT fired when the category is changed directly, or removed, for example.
 *
 * @since v7.2
 */
@PublicApi
@ParametersAreNonnullByDefault
@EventName("jira.administration.projectdetails.updated.category")
public class ProjectUpdatedCategoryChangedEvent extends AbstractEvent {
    private String requestSourceType;

    @Internal
    public ProjectUpdatedCategoryChangedEvent(RequestSourceType requestSourceType) {
        this.requestSourceType = requestSourceType.getType();
    }

    public String getRequestSourceType() {
        return requestSourceType;
    }
}
