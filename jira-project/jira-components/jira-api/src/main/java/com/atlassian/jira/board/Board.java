package com.atlassian.jira.board;

import com.atlassian.annotations.ExperimentalApi;

import java.util.Objects;

/**
 * Represents the core board.
 * @since v7.1
 */
@ExperimentalApi
public final class Board {
    private BoardId id;
    private String jql;

    public Board(BoardId boardId, String jql) {
        this.id = boardId;
        this.jql = jql;
    }

    public BoardId getId() {
        return id;
    }

    public String getJql() {
        return jql;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Board board = (Board) o;
        return Objects.equals(id, board.id) &&
                Objects.equals(jql, board.jql);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, jql);
    }

    @Override
    public String toString() {
        return "Board{" +
                "id=" + id +
                ", jql='" + jql + '\'' +
                '}';
    }
}
