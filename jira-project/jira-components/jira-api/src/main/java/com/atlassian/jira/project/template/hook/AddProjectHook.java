package com.atlassian.jira.project.template.hook;

import com.atlassian.annotations.PublicSpi;

/**
 * SPI for the "Add Project" operation. Implementations are encouraged to extend {@link EmptyAddProjectHook} for ease of
 * use.
 *
 * @since 7.0
 */
@PublicSpi
public interface AddProjectHook {
    /**
     * Gets called before a project is added. Implementations may veto the project creation by adding errors to the
     * ValidateResponse.
     *
     * @param validateData a ValidateData
     * @return a ValidateResponse
     */
    ValidateResponse validate(ValidateData validateData);

    /**
     * Gets called after a project has been added successfully. Implementations can use the ConfigureResponse to specify
     * where the user should be redirected to after.
     *
     * @param configureData a ConfigureData
     * @return a ConfigureResponse
     */
    ConfigureResponse configure(ConfigureData configureData);
}
