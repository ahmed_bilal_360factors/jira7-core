package com.atlassian.jira.project.type;

import com.atlassian.annotations.PublicApi;
import com.atlassian.application.api.Application;
import com.atlassian.fugue.Option;
import com.atlassian.jira.user.ApplicationUser;

import java.util.List;

/**
 * Manager to access information about the different project types defined on the JIRA instance.
 *
 * @since 7.0
 */
@PublicApi
public interface ProjectTypeManager {
    /**
     * Returns all the project types defined on the JIRA instance sorted by weight not taking into account whether
     * the license to use those project types is valid or not.
     *
     * @return all the project types defined on the JIRA instance sorted by weight.
     */
    List<ProjectType> getAllProjectTypes();

    /**
     * Returns all the accessible project types defined on the JIRA instance sorted by weight.
     * A project type is considered accessible if the application that defines it is
     * present on the JIRA instance and if there is a license for that application on the instance.
     * This method should be used when displaying available project types to admins (e.g. project
     * create dialog, change project type dialog)
     *
     * @return all the accessible project types defined on the JIRA instance sorted by weight.
     */
    List<ProjectType> getAllAccessibleProjectTypes();

    /**
     * Returns the project type with the given key, if it is present on the JIRA instance.
     *
     * @param key The project type key
     * @return The project type for the given key
     */
    Option<ProjectType> getByKey(ProjectTypeKey key);

    /**
     * Returns the project type with the given key, if it is accessible.
     * A project type is considered accessible if the application that defines it is
     * present on the JIRA instance and if there is a license for that application in the instance.
     * Generally this method should be used when retrieving a project type for display in the application.
     * The convention is to always display the project type if the application is licensed, even if the current
     * user isn't licensed to access that application.
     * Not showing the project type due to the user's individual license access should be the exception.
     *
     * @param key The project type key
     * @return The project type for the given key
     */
    Option<ProjectType> getAccessibleProjectType(ProjectTypeKey key);

    /**
     * Returns the project type with the given key, if it is accessible for the given user.
     * A project type is considered accessible for an user if the application that defines it
     * is present on the JIRA instance, that application has a license and the given user is
     * licensed for that application.
     *
     * @param user The user
     * @param key  The project type key
     * @return The project type of the given key
     */
    Option<ProjectType> getAccessibleProjectType(ApplicationUser user, ProjectTypeKey key);

    /**
     * Returns a project type with information to be displayed to users when a specific project type is not accessible.
     * <p>
     * A project type might be inaccessible if the user is not licensed under the application that
     * defines the type or if the application that defines the type is not installed on the JIRA instance.
     *
     * @return A project type encapsulating information for when a specific type is not accessible;
     */
    ProjectType getInaccessibleProjectType();

    /**
     * Returns the application that provides the given project type.
     *
     * @param key The project type key
     * @return The application associated to that project.
     */
    Option<Application> getApplicationWithType(final ProjectTypeKey key);

    /**
     * Returns whether the project type with the given key is uninstalled.
     *
     * @param projectTypeKey The key of the project type for which we are performing the check.
     * @return Whether the project type is uninstalled.
     */
    boolean isProjectTypeUninstalled(ProjectTypeKey projectTypeKey);

    /**
     * Returns whether the project type with the given key is installed but inaccessible.
     *
     * @param projectTypeKey The key of the project type for which we are performing the check.
     * @return Whether the project type is uninstalled but inaccessible.
     */
    boolean isProjectTypeInstalledButInaccessible(ProjectTypeKey projectTypeKey);
}
