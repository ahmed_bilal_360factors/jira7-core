package com.atlassian.jira.permission;

import com.atlassian.annotations.PublicApi;
import com.atlassian.fugue.Option;
import com.google.common.base.Function;
import com.google.common.base.Objects;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.google.common.collect.Sets;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.Immutable;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import static com.atlassian.fugue.Option.option;
import static com.atlassian.jira.permission.PermissionGrantInput.newGrant;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Strings.emptyToNull;
import static java.util.Collections.singleton;

/**
 * Input for building a {@link PermissionScheme}.
 */
@Immutable
@PublicApi
public final class PermissionSchemeInput {
    private final String name;
    private final String description;
    private final List<PermissionGrantInput> permissions;

    private PermissionSchemeInput(final String name, final String description, final Iterable<PermissionGrantInput> permissions) {
        this.name = checkNotNull(emptyToNull(name));
        this.description = description;
        this.permissions = permissions != null ? ImmutableList.copyOf(permissions) : Collections.<PermissionGrantInput>emptyList();
    }

    public String getName() {
        return name;
    }

    public Option<String> getDescription() {
        return option(description);
    }

    public List<PermissionGrantInput> getPermissions() {
        return permissions;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PermissionSchemeInput that = (PermissionSchemeInput) o;

        return Objects.equal(this.name, that.name) &&
                Objects.equal(this.description, that.description) &&
                Objects.equal(this.permissions, that.permissions);
    }

    /**
     * Creates a builder of {@code PermissionSchemeInput} objects. Accepts a {@code name} as an argument
     * as it's the only required field for a permission scheme.
     *
     * @param name permission scheme name
     * @return a new builder
     */
    public static Builder builder(@Nonnull String name) {
        return new Builder(name);
    }

    /**
     * Creates a new builder which has all the fields set as an input {@code data}.
     *
     * @param data data to set in the returned builder
     * @return a new builder with the data matching the argument.
     */
    public static Builder builder(@Nonnull PermissionSchemeInput data) {
        return new Builder(data);
    }

    /**
     * Creates a new builder which has all the fields set as an input {@code scheme}.
     * Note that all id-s will be lost.
     *
     * @param scheme data to set in the returned builder
     * @return a new builder with the data matching the argument.
     */
    public static Builder builder(@Nonnull PermissionScheme scheme) {
        return new Builder(scheme);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(name, description, permissions);
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("name", name)
                .add("description", description)
                .add("permissions", permissions)
                .toString();
    }

    public static class Builder {
        private String name;
        private String description;
        private Set<PermissionGrantInput> permissions = Sets.newLinkedHashSet();

        public Builder(String name) {
            this.name = checkNotNull(name);
        }

        public Builder(PermissionSchemeInput schemeData) {
            this.name = schemeData.getName();
            this.description = schemeData.getDescription().getOrNull();
            setPermissions(schemeData.getPermissions());
        }

        public Builder(PermissionScheme scheme) {
            this.name = scheme.getName();
            this.description = Strings.emptyToNull(scheme.getDescription());
            setOriginalPermissions(scheme.getPermissions());
        }

        public Builder setName(@Nonnull final String name) {
            this.name = checkNotNull(name);
            return this;
        }

        public Builder setDescription(final String description) {
            this.description = description;
            return this;
        }

        public Builder setPermissions(final Iterable<PermissionGrantInput> permissions) {
            this.permissions.clear();
            return addPermissions(permissions);
        }

        public Builder setOriginalPermissions(final Iterable<PermissionGrant> permissions) {
            this.permissions.clear();
            return addPermissions(Iterables.transform(permissions, new Function<PermissionGrant, PermissionGrantInput>() {
                @Override
                public PermissionGrantInput apply(final PermissionGrant input) {
                    return newGrant(input.getHolder(), input.getPermission());
                }
            }));
        }

        public Builder addPermissions(final Iterable<PermissionGrantInput> permissions) {
            for (PermissionGrantInput permission : permissions) {
                this.permissions.add(permission);
            }
            return this;
        }

        public Builder addPermission(final PermissionGrantInput permissions) {
            return addPermissions(singleton(permissions));
        }

        public PermissionSchemeInput build() {
            return new PermissionSchemeInput(name, description, permissions);
        }
    }
}
