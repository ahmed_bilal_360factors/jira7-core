package com.atlassian.jira.project.template.hook;

import com.atlassian.annotations.PublicApi;

/**
 * Response object for the "configure project" hook.
 *
 * @since 7.0
 */
@PublicApi
public class ConfigureResponse {
    private String redirect;

    /**
     * Creates a new ConfigureResponse.
     *
     * @return a new ConfigureResponse
     */
    public static ConfigureResponse create() {
        return new ConfigureResponse();
    }

    private ConfigureResponse() {
    }

    /**
     * Sets the redirect URL to be used after successful project creation.
     *
     * @param redirect a redirect URL
     * @return this
     * @deprecated All projects created via a template will be redirected to the Browse Project page. This value will be ignored.
     */
    @Deprecated
    public ConfigureResponse setRedirect(String redirect) {
        this.redirect = redirect;
        return this;
    }

    /**
     * Gets the redirect URL.
     *
     * @return a URL
     * @deprecated All projects created via a template will be redirected to the Browse Project page. This value will be ignored.
     */
    @Deprecated
    public String getRedirect() {
        return redirect;
    }
}
