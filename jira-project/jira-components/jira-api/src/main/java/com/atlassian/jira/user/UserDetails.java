package com.atlassian.jira.user;

import com.atlassian.annotations.ExperimentalApi;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Objects;
import java.util.Optional;

import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * Contains the details that is required to represent a {@link ApplicationUser} they may or may not exist.
 * Mainly used as transfer object during create and update to avoid overloading methods.
 *
 * @since v7.0
 */
@ExperimentalApi
public final class UserDetails {
    private final String username;
    private final String password;
    private final String displayName;
    private final String emailAddress;
    private final Optional<Long> directoryId;

    private UserDetails(String username, String displayName, String password, String emailAddress,
                        @Nullable Long directoryId) {
        this.username = notNull("username", username);
        this.displayName = notNull("displayName", displayName);
        this.password = password;
        this.emailAddress = emailAddress;
        this.directoryId = directoryId == null ? Optional.empty() : Optional.of(directoryId);
    }

    public UserDetails(String username, String displayName) {
        this(username, displayName, null, null, null);
    }

    public UserDetails withPassword(String password) {
        return new UserDetails(this.username, this.displayName, password, this.emailAddress,
                this.directoryId.orElse(null));
    }

    public UserDetails withEmail(String email) {
        return new UserDetails(this.username, this.displayName, this.password, email,
                this.directoryId.orElse(null));
    }

    /**
     * @param directoryId directory where the user should be created in, if not specified the user should be created
     *                    in the default directory.
     */
    public UserDetails withDirectory(Long directoryId) {
        return new UserDetails(this.username, this.displayName, this.password, this.emailAddress,
                directoryId);
    }

    /**
     * Return the user's username.
     *
     * @return user's username; must never be {@code null}.
     */
    @Nonnull
    public String getUsername() {
        return username;
    }

    /**
     * Return the user's password.
     *
     * @return user's password; this can be {@code null}.
     */
    public String getPassword() {
        return password;
    }

    /**
     * Returns the user's display name. This is sometimes referred to as "full name".
     *
     * @return user's display name, must never be null.
     */
    @Nonnull
    public String getDisplayName() {
        return displayName;
    }

    /**
     * Return the user's email address.
     *
     * @return email address of the user.
     */
    public String getEmailAddress() {
        return emailAddress;
    }

    /**
     * Return the specified directory where the user should be created in, or {@link Optional#empty()}
     * the user should be created in the default directory.
     *
     * @return the ID of the user directory for this user.
     */
    public Optional<Long> getDirectoryId() {
        return directoryId;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof UserDetails)) {
            return false;
        }
        final UserDetails that = (UserDetails) o;
        return Objects.equals(username, that.username) &&
                Objects.equals(password, that.password) &&
                Objects.equals(displayName, that.displayName) &&
                Objects.equals(emailAddress, that.emailAddress) &&
                Objects.equals(directoryId, that.directoryId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(username, password, displayName, emailAddress, directoryId);
    }

    @Override
    public String toString() {
        return "UserDetails{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", displayName='" + displayName + '\'' +
                ", emailAddress='" + emailAddress + '\'' +
                ", directoryId=" + directoryId +
                '}';
    }
}