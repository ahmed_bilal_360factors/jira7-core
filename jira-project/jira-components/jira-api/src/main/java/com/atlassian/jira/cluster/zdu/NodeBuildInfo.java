package com.atlassian.jira.cluster.zdu;

import com.atlassian.annotations.ExperimentalApi;
import com.atlassian.jira.util.BuildUtilsInfo;

/**
 * This information uniquely identifies the code version of a node.
 *
 * @since v7.3
 */
@ExperimentalApi
public interface NodeBuildInfo {
    /**
     * @return the build number of the node's JIRA install.
     * @see BuildUtilsInfo#getApplicationBuildNumber()
     */
    long getBuildNumber();
    /**
     * @return the version of the node's JIRA install.
     * @see BuildUtilsInfo#getVersion()
     */
    String getVersion();
}
