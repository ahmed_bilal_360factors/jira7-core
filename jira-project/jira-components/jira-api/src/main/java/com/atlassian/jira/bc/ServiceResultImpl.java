package com.atlassian.jira.bc;

import com.atlassian.annotations.PublicApi;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.jira.util.SimpleWarningCollection;
import com.atlassian.jira.util.WarningCollection;

import javax.annotation.Nonnull;

import java.io.Serializable;

import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * Simple implementation of a validation result.
 * <p>
 * Consider using {@link com.atlassian.jira.bc.ServiceOutcome}, which avoids to have to create a new class.
 * <p>
 * Included in the jira-api module not because it should be used by plugin developers, but because it is extended by
 * inner classes on many Services eg see {@link com.atlassian.jira.bc.issue.IssueService.TransitionValidationResult}
 *
 * @since v4.0
 */
@PublicApi
public class ServiceResultImpl implements ServiceResult, Serializable {
    private static final long serialVersionUID = -4206122924974290538L;

    private final ErrorCollection errorCollection;
    private final WarningCollection warningCollection;

    public ServiceResultImpl(@Nonnull final ErrorCollection errorCollection) {
        this(errorCollection, new SimpleWarningCollection());
    }

    public ServiceResultImpl(
            @Nonnull final ErrorCollection errorCollection, @Nonnull final WarningCollection warningCollection) {
        this.errorCollection = notNull("errorCollection", errorCollection);
        this.warningCollection = notNull("warningCollection", warningCollection);
    }

    /**
     * Typical error collections can't actually be serialized, such as JiraWebActionSupport subclasses.
     * So when trying to serialize, only serialize the errors and warnings themselves.
     */
    private Object writeReplace() {
        return new ServiceResultImpl(new SimpleErrorCollection(this.errorCollection),
                                    new SimpleWarningCollection(this.warningCollection));
    }

    public boolean isValid() {
        return !errorCollection.hasAnyErrors();
    }

    public ErrorCollection getErrorCollection() {
        return errorCollection;
    }

    public WarningCollection getWarningCollection() {
        return warningCollection;
    }
}
