package com.atlassian.jira.bc.filter;

import com.atlassian.annotations.ExperimentalApi;
import com.atlassian.jira.bc.ServiceOutcome;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.user.ApplicationUser;

import javax.annotation.Nonnull;
import java.util.List;

/**
 * Provides functionality to associate a search request with a given project.
 *
 * @since v7.0
 */
@ExperimentalApi
public interface ProjectSearchRequestService {

    /**
     * For a given project this method will associated the provided search requests such that they will be returned by
     * subsequent calls to {@link #getSearchRequestsForProject(ApplicationUser, Project)}.
     * <p>
     * The user performing this operation must have rights to {@link com.atlassian.jira.bc.project.ProjectAction#EDIT_PROJECT_CONFIG},
     * otherwise this this method will return an error result.
     * <p>
     * If a search request cannot be found by its id it will simply not be associated with the project.
     *
     * @param user             The user performing this operation
     * @param project          The project to associate the search requests with
     * @param searchRequestIds A list of search requests to associate
     * @since 7.0
     */
    ServiceOutcome<List<SearchRequest>> associateSearchRequestsWithProject(final ApplicationUser user, @Nonnull final Project project, final Long... searchRequestIds);

    /**
     * Retrieves all search requests for a given project that the user is allowed to see.  If the provided user does
     * not have the {@link com.atlassian.jira.bc.project.ProjectAction#VIEW_ISSUES} permission this method should
     * return an empty list.
     *
     * @param user    The user performing this operation
     * @param project The project that search requests are associated with
     * @return all search requests for the given project that the user provided is allowed to see.  Empty if none are
     * found.
     * @since 7.0
     */
    List<SearchRequest> getSearchRequestsForProject(final ApplicationUser user, @Nonnull final Project project);
}
