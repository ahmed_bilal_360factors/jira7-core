package com.atlassian.jira.bc.dataimport;

import com.atlassian.annotations.PublicApi;
import com.atlassian.jira.bc.ServiceResult;

import java.util.Optional;
import java.util.Set;

/**
 * Plugins can implement this interface to add logic to validate {@code <OSPropertyEntry>} elements in the backup XML.
 * All {@link DataImportOSPropertyValidator} instances will be invoked before the import
 * (restore) process starts.
 *
 * To register an implementation of this interface, use the {@code <data-import-properties-validator>} element in your
 * {@code atlassian-plugin.xml}.
 *
 * Example:
 *
 * {@code
 * <data-import-properties-validator key="my-data-import-validator"
 *                                   i18n-name-key="my.data.import.validator.name"
 *                                   class="com.example.myplugin.MyDataImportPropertiesValidator" />
 * }
 *
 * @since v7.1
 */
@PublicApi
public interface DataImportOSPropertyValidator {
    /**
     * Specifies a set of property entries that will be validated. Any entries specified here will then be made
     * available in the {@link DataImportOSPropertyValidator.DataImportProperties} instance
     * that gets passed into the {@link #validate(DataImportParams, DataImportOSPropertyValidator.DataImportProperties)}
     * method.
     *
     * Note that the {@link DataImportOSPropertyValidator.DataImportProperties} instance
     * will also contain properties that are requested by other instances of {@link DataImportOSPropertyValidator}.
     *
     * @return A set of property entries that will be validated by this validator.
     */
    Set<String> getPropertyKeysToValidate();

    /**
     * Validates the backup XML.
     *
     * @param dataImportParams Import parameters provided by user during import process
     * @param dataImportProperties Property entries loaded from the backup XML being validated. To guarantee that the properties
     *                             we need to validate are available, we need to ensure that they are returned by the
     *                             {@link #getPropertyKeysToValidate()} method. The entries contained here are those requested
     *                             by all validator instances, i.e. you may get additional properties beyond what's specified by
     *                             {@link #getPropertyKeysToValidate()}.
     *
     * @return Valid service result if all properties are valid, else a service result containing some errors.
     */
    ServiceResult validate(DataImportParams dataImportParams, DataImportProperties dataImportProperties);

    /**
     * Accessors to property entries in the backup XML.
     */
    interface DataImportProperties {
        Optional<String> getStringProperty(String propertyKey);

        Optional<String> getTextProperty(String propertyKey);

        Optional<Long> getNumberProperty(String propertyKey);
    }
}
