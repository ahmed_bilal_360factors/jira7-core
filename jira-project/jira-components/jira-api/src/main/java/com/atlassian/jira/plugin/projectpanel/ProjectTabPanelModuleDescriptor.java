package com.atlassian.jira.plugin.projectpanel;

import com.atlassian.jira.plugin.TabPanelModuleDescriptor;

/**
 * A project tab panel plugin adds extra panel tabs to JIRA's Browse Project page.
 *
 * @deprecated This is plugin module is slated for removal. https://developer.atlassian.com/x/dsfgAQ details how to
 * integrate with the new project centric view in JIRA going forward. Since 7.0.
 */
@Deprecated
public interface ProjectTabPanelModuleDescriptor extends TabPanelModuleDescriptor<ProjectTabPanel> {

}
