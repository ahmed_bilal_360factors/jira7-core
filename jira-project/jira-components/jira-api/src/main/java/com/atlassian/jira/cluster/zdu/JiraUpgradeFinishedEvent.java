package com.atlassian.jira.cluster.zdu;

import com.atlassian.annotations.ExperimentalApi;

/**
 * This event is fired when the cluster enters {@link UpgradeState#STABLE} due to the upgrade having finished successfully.
 *
 * @since v7.3
 * @see UpgradeState#RUNNING_UPGRADE_TASKS
 */
@ExperimentalApi
public class JiraUpgradeFinishedEvent {
    private final NodeBuildInfo fromVersion;
    private final NodeBuildInfo toVersion;

    public JiraUpgradeFinishedEvent(final NodeBuildInfo fromVersion, final NodeBuildInfo toVersion) {
        this.fromVersion = fromVersion;
        this.toVersion = toVersion;
    }

    public NodeBuildInfo getFromVersion() {
        return fromVersion;
    }

    public NodeBuildInfo getToVersion() {
        return toVersion;
    }
}
