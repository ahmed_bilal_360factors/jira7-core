package com.atlassian.jira.issue.search;

import com.atlassian.jira.component.ComponentAccessor;
import org.apache.commons.collections.ListUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.List;

public class SearchRequestUtils {
    private static final Logger log = LoggerFactory.getLogger(SearchRequestUtils.class);

    /**
     * Creates the most specific {@link SearchContext} possible from the two parameters. The baseSearchContext is used as
     * the parent context and the possibleContext is used to narrow it down. Thus if the possibleContext contains Project A
     * and the baseSearchContext is global, a context with Project A will be returned. If baseSearchContext is Project A &amp; Project B
     * the Project A will be returned. If baseSearchContext is only Project B, then Project B will be returned. The same
     * logic applies for issue types.
     *
     * @param baseSearchContext the base <em>parent</em> context
     * @param possibleContext   the context to try to narrow the baseSearchContext on
     * @return a combineed {@link SearchContext} object based on the baseSearchContext. Null if baseSearchContext is null
     */
    public static SearchContext getCombinedSearchContext(SearchContext baseSearchContext, SearchContext possibleContext) {
        if (baseSearchContext != null) {
            if (possibleContext != null) {
                // Deal with the projects
                List combinedProjects;
                if (baseSearchContext.isForAnyProjects()) {
                    combinedProjects = possibleContext.getProjectIds();
                } else {
                    combinedProjects = ListUtils.intersection(baseSearchContext.getProjectIds(), possibleContext.getProjectIds() != null ? possibleContext.getProjectIds() : Collections.emptyList());
                    if (combinedProjects.isEmpty()) {
                        combinedProjects = baseSearchContext.getProjectIds();
                    }
                }

                // Deal with the issue types
                List combinedIssuetypes;
                if (baseSearchContext.isForAnyIssueTypes()) {
                    combinedIssuetypes = possibleContext.getIssueTypeIds();
                } else {
                    combinedIssuetypes = ListUtils.intersection(baseSearchContext.getIssueTypeIds(), possibleContext.getIssueTypeIds() != null ? possibleContext.getIssueTypeIds() : Collections.emptyList());
                    if (combinedIssuetypes.isEmpty()) {
                        combinedIssuetypes = baseSearchContext.getIssueTypeIds();
                    }
                }
                SearchContextFactory searchContextFactory = ComponentAccessor.getComponent(SearchContextFactory.class);
                return searchContextFactory.create(null, combinedProjects, combinedIssuetypes);

            } else {
                SearchContextFactory searchContextFactory = ComponentAccessor.getComponent(SearchContextFactory.class);
                return searchContextFactory.create(baseSearchContext);
            }
        } else {
            return null;
        }
    }
}
