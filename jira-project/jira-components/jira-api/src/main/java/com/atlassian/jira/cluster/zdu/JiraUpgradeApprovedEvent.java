package com.atlassian.jira.cluster.zdu;

import com.atlassian.annotations.ExperimentalApi;

/**
 * This event is fired when all nodes have been upgraded and the upgrade tasks are about to be started.
 *
 * @since v7.3
 * @see UpgradeState#RUNNING_UPGRADE_TASKS
 */
@ExperimentalApi
public class JiraUpgradeApprovedEvent {
    private final NodeBuildInfo fromVersion;
    private final NodeBuildInfo toVersion;

    public JiraUpgradeApprovedEvent(final NodeBuildInfo fromVersion, final NodeBuildInfo toVersion) {
        this.fromVersion = fromVersion;
        this.toVersion = toVersion;
    }

    public NodeBuildInfo getFromVersion() {
        return fromVersion;
    }

    public NodeBuildInfo getToVersion() {
        return toVersion;
    }
}
