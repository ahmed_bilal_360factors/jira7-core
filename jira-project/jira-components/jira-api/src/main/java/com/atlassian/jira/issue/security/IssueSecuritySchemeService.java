package com.atlassian.jira.issue.security;

import com.atlassian.annotations.PublicApi;
import com.atlassian.jira.bc.ServiceOutcome;
import com.atlassian.jira.user.ApplicationUser;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Service which can be used to access issue security functionality.
 * Checks permissions before handing off performing the action to the correct manager.
 * See {@link com.atlassian.jira.issue.security.IssueSecurityLevelManager} and {@link com.atlassian.jira.issue.security.IssueSecuritySchemeManager}.
 *
 * @since 7.0
 */
@PublicApi
public interface IssueSecuritySchemeService {
    /**
     * Returns all defined security level schemes.
     *
     * @param user user for which permission checks are performed. Only users with an admin permission can perform this action.
     * @return a service outcome with a collection of security level schemes or with an error collection
     */
    ServiceOutcome<? extends Collection<IssueSecurityLevelScheme>> getIssueSecurityLevelSchemes(ApplicationUser user);

    /**
     * Returns a single issue security scheme with a given id.
     *
     * @param user user for which permission checks are performed.
     *             Permission to get the issue security scheme with a given key is granted in the following cases:
     *             1. User has the administrator global permission.
     *             2. The scheme is used in a project to which the user has the project administrator permission.
     * @return a service outcome with an IssueSecurityLevelScheme or an error collection
     */
    ServiceOutcome<IssueSecurityLevelScheme> getIssueSecurityLevelScheme(ApplicationUser user, long schemeId);

    /**
     * Returns a single issue security scheme for a given project.
     *
     * @param user      user for which permissions checks are performed.
     *                  Permission to get the issue security scheme with a given key is granted in the following cases:
     *                  1. User has the administrator global permission.
     *                  2. The scheme is used in a project to which the user has the project administrator permission.
     * @param projectId id of the project to return scheme for.
     * @return a service outcome with an issueSecurityLevelScheme or an error collection
     */
    ServiceOutcome<IssueSecurityLevelScheme> getIssueSecurityLevelSchemeForProject(ApplicationUser user, long projectId);

    /**
     * Returns a single issue security scheme for a given project.
     *
     * @param user       user for which permissions checks are performed.
     *                   Permission to get the issue security scheme with a given key is granted in the following cases:
     *                   1. User has the administrator global permission.
     *                   2. The scheme is used in a project to which the user has the project administrator permission.
     * @param projectKey key of the project to return scheme for.
     * @return a service outcome with an issueSecurityLevelScheme or an error collection
     */
    ServiceOutcome<IssueSecurityLevelScheme> getIssueSecurityLevelSchemeForProject(ApplicationUser user, String projectKey);

    /**
     * Assigns a new Issue Security Scheme to a project.
     * If there was a prior scheme in place a mapping of old to new levels should be passed.
     *
     * @param user                          user for which permissions checks are performed.
     *                                      Permission to get the issue security scheme with a given key is granted in the following cases:
     *                                      1. User has the administrator global permission.
     *                                      2. The scheme is used in a project to which the user has the project administrator permission.
     * @param projectId                     Project to assign
     * @param newSchemeId                   Id of the new IssueSecurity Scheme
     * @param oldToNewSecurityLevelMappings Mapping of old to new security levels to apply to issues in the project.
     * @return a service outcome with a Task progress URL.
     * @since v7.1.1
     */
    ServiceOutcome<String> assignSchemeToProject(ApplicationUser user, long projectId, Long newSchemeId, Map<Long, Long> oldToNewSecurityLevelMappings);

    /**
     * Returns the list of security levels for the given issue security level scheme.
     * The elements are ordered by name.
     *
     * @param user user for which permission checks are performed.
     *             Permission to get the issue security levels for a scheme with a given key is granted in the following cases:
     *             1. User has the administrator global permission.
     *             2. The scheme is used in a project to which the user has the project administrator permission.
     * @return a service outcome with a list of Security Levels for the given issue security level scheme, or with an error collection
     */
    ServiceOutcome<? extends List<IssueSecurityLevel>> getIssueSecurityLevels(ApplicationUser user, long schemeId);

    /**
     * Returns an issue security level with a given id.
     *
     * @param user user for which permission checks are performed. Only users with an admin permission can perform this action.
     * @return a service outcome with an issue security level or an error collection
     */
    ServiceOutcome<IssueSecurityLevel> getIssueSecurityLevel(ApplicationUser user, long securityLevelId);

    /**
     * Returns permissions for the given issue security level.
     *
     * @param user user for which permission checks are performed. Only users with an admin permission can perform this action.
     * @return a service outcome with a list of issue security level permissions or an error collection
     */
    ServiceOutcome<? extends Collection<IssueSecurityLevelPermission>> getPermissionsByIssueSecurityLevel(ApplicationUser user, long securityLevelId);
}
