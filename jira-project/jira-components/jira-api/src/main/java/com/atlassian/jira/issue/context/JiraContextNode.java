package com.atlassian.jira.issue.context;

import com.atlassian.annotations.PublicApi;

import javax.annotation.Nullable;
import java.util.Map;

/**
 * A constructed {@link IssueContext} with the ability to climb nodes
 */
@PublicApi
public interface JiraContextNode extends IssueContext, Comparable<JiraContextNode> {
    String FIELD_PROJECT = "project";

    /**
     * @return Whether or not this context has a parent context.
     */
    boolean hasParentContext();

    /**
     * @return This context's parent, or null if it is the root context.
     * @since 6.4
     */
    @Nullable
    JiraContextNode getParent();

    public boolean isInContext(IssueContext issueContext);

    /**
     * Copy the supplied parameters and add new ones.
     *
     * @param props to copy from
     * @return the copied map
     */
    Map<String, Object> appendToParamsMap(Map<String, Object> props);

}
