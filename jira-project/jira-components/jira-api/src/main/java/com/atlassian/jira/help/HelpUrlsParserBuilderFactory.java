package com.atlassian.jira.help;

import com.atlassian.annotations.ExperimentalApi;
import com.atlassian.fugue.Option;

import javax.annotation.Nonnull;

/**
 * Factory of HelpUrlsParser builders. This is the recommended way of obtaining a {@code HelpUrlsParser}. A {@code
 * HelpUrlsParser} instance created by calling {@link HelpUrlsParserBuilder#build()} with no additional builder
 * configuration is configured with the default url being {@code null} and with the correct OnDemand status.
 * The applicationHelpSpace property is unset by default to reflect the behavior of injected {@code HelpUrlsParser}.
 *
 * @see HelpUrlsParser
 * @since 7.0
 */
@ExperimentalApi
public interface HelpUrlsParserBuilderFactory {

    @Nonnull
    HelpUrlsParserBuilder newBuilder();

    interface HelpUrlsParserBuilder {
        /**
         * Set the default url for parser.
         *
         * @param url   default url address
         * @param title default url title
         * @return this instance for chaining
         */
        @Nonnull
        HelpUrlsParserBuilder defaultUrl(@Nonnull final String url, final String title);

        /**
         * Set the target space that will be used to replace application help space placeholder in parsed help urls.
         * If not set the replacement will not occur which matches the behaviour of injected {@code HelpUrlsParser}.
         *
         * @param applicationHelpSpace space for application help
         * @return this instance for chaining
         */
        @Nonnull
        HelpUrlsParserBuilder applicationHelpSpace(@Nonnull final Option<String> applicationHelpSpace);

        /**
         * Create new parser instance.
         *
         * @return new {@code HelpUrlsParser} instance.
         */
        @Nonnull
        HelpUrlsParser build();
    }
}
