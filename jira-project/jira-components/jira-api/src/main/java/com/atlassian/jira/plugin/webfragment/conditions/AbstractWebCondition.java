package com.atlassian.jira.plugin.webfragment.conditions;

import com.atlassian.annotations.PublicSpi;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.plugin.webfragment.JiraWebContext;
import com.atlassian.jira.plugin.webfragment.JiraWebInterfaceManager;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserUtil;
import com.atlassian.ozymandias.SafePluginPointAccess;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.web.Condition;

import java.util.Map;

/**
 * Convenient abstraction for {@link Condition}s that are aware of JIRA's
 * authentication and project- or issue-related contexts.  These can be
 * used in action configurations to guard conditionally displayed content.
 *
 * @since v6.0
 */
@PublicSpi
public abstract class AbstractWebCondition implements Condition {
    public void init(final Map<String, String> params) throws PluginParseException {
    }

    public boolean shouldDisplay(final Map<String, Object> context) {
        final JiraHelper jiraHelper = (JiraHelper) context.get(JiraWebInterfaceManager.CONTEXT_KEY_HELPER);
        final ApplicationUser appUser = getApplicationUser(context);

        return SafePluginPointAccess.call(() -> shouldDisplay(appUser, jiraHelper)).getOrElse(false);
    }

    private ApplicationUser getApplicationUser(final Map<String, Object> context) {
        return JiraWebContext.from(context).getUser().orElse(null);
    }

    UserUtil getUserUtil() {
        return ComponentAccessor.getUserUtil();
    }

    public abstract boolean shouldDisplay(ApplicationUser user, JiraHelper jiraHelper);
}
