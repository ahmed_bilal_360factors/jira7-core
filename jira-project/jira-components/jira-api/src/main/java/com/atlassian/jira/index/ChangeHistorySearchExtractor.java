package com.atlassian.jira.index;

import com.atlassian.annotations.ExperimentalSpi;
import com.atlassian.jira.issue.changehistory.ChangeHistoryGroup;

/**
 * Interface for extractors adding fields based on comments
 *
 * @since 6.2
 */
@ExperimentalSpi
public interface ChangeHistorySearchExtractor extends EntitySearchExtractor<ChangeHistoryGroup> {

}
