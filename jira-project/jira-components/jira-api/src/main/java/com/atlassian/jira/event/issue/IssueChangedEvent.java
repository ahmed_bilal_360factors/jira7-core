package com.atlassian.jira.event.issue;

import com.atlassian.annotations.PublicApi;
import com.atlassian.jira.issue.comments.Comment;
import com.atlassian.jira.issue.history.ChangeItemBean;
import com.atlassian.jira.user.ApplicationUser;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.Collection;
import java.util.Optional;

/**
 * This event represents any issue change
 *
 * @since v7.2
 */
@PublicApi
public interface IssueChangedEvent extends IssueRelatedEvent {

    /**
     * The user who caused the event, or <code>empty</code>
     * representing an anonymous action.
     *
     * @return the user, can be empty
     */
    @Nonnull
    Optional<ApplicationUser> getAuthor();

    /**
     * The comment that was added to the issue with the change.
     *
     * @return the comment, can be empty
     */
    @Nonnull
    Optional<Comment> getComment();

    /**
     * The changes that were made to the issue
     * with the action that caused the event.
     *
     * @return the changes
     */
    @Nonnull
    Collection<ChangeItemBean> getChangeItems();

    /**
     * The change of the given field in the event, if
     * <ol>
     * <li>the given <code>fieldName</code> equals the name of one of the {@link ChangeItemBean} in the event</li>
     * <li>the old and the new value do not equal (don't have the same value and are not both null)</li>
     * </ol>
     *
     * @param fieldName the name of the field
     * @return the change, which is empty if the field wasn't changed.
     */
    @Nonnull
    Optional<ChangeItemBean> getChangeItemForField(@Nonnull String fieldName);
}
