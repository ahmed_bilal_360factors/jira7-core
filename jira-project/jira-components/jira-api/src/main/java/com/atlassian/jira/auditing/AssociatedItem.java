package com.atlassian.jira.auditing;

import com.atlassian.annotations.ExperimentalApi;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * @since v6.2
 */
@ExperimentalApi
public interface AssociatedItem {
    enum Type {
        USER,
        PROJECT,
        GROUP,
        SCHEME,
        REMOTE_DIRECTORY,
        WORKFLOW,
        PERMISSIONS,
        VERSION,
        CUSTOM_FIELD,
        PROJECT_CATEGORY,
        PROJECT_COMPONENT,
        PROJECT_ROLE,
        LICENSE,
        APPLICATION_ROLE,
        SCREEN
    }

    @Nonnull
    String getObjectName();

    @Nullable
    String getObjectId();

    @Nullable
    String getParentName();

    @Nullable
    String getParentId();

    @Nonnull
    Type getObjectType();
}
