package com.atlassian.jira.config;

import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * A FeatureFlag is a simple declaration of a key and whether the flag is considered on or off by default.
 * The FeatureFlag is different from a {@link com.atlassian.jira.config.Feature} because it can have a default value. Additionally,
 * feature flags can be declared via xml and shared across add-ons.
 *
 * @since 7.1
 */
public class FeatureFlag implements Feature {
    public static final String POSTFIX_ENABLED = ".enabled";
    public static final String POSTFIX_DISABLED = ".disabled";

    private String featureKey;
    private boolean defaultOn;

    private FeatureFlag(String featureKey, boolean defaultOn) {
        this.featureKey = featureKey;
        this.defaultOn = defaultOn;
    }

    public String featureKey() {
        return featureKey;
    }

    /**
     * @return true if this feature is on by default
     */
    public boolean isOnByDefault() {
        return defaultOn;
    }

    /**
     * @return this feature key with the enabled postfix of {@link #POSTFIX_ENABLED}
     */
    public String enabledFeatureKey() {
        return featureKey + POSTFIX_ENABLED;
    }

    /**
     * @return this feature key with the enabled postfix of {@link #POSTFIX_DISABLED}
     */
    public String disabledFeatureKey() {
        return featureKey + POSTFIX_DISABLED;
    }

    /**
     * Creates a new FeatureFlag with the specified key and off by default
     *
     * @param featureKey the key in play
     * @return a new feature flag
     */
    public static FeatureFlag featureFlag(String featureKey) {
        notNull(featureKey);
        return new FeatureFlag(featureKey, false);
    }

    /**
     * Creates a new FeatureFlag that is on by default
     *
     * @return a new feature flag
     */
    public FeatureFlag onByDefault() {
        return new FeatureFlag(this.featureKey, true);
    }

    /**
     * Creates a new FeatureFlag that is off by default
     *
     * @return a new feature flag
     */
    public FeatureFlag offByDefault() {
        return new FeatureFlag(this.featureKey, false);
    }

    /**
     * Creates a new FeatureFlag that is defaulted to the specified value
     *
     * @return a new feature flag
     */
    public FeatureFlag defaultedTo(boolean defaultValue) {
        return new FeatureFlag(this.featureKey, defaultValue);
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final FeatureFlag that = (FeatureFlag) o;

        return defaultOn == that.defaultOn && featureKey.equals(that.featureKey);
    }

    @Override
    public int hashCode() {
        int result = featureKey.hashCode();
        result = 31 * result + (defaultOn ? 1 : 0);
        return result;
    }

    @Override
    public String toString() {
        return featureKey + ":" + defaultOn;
    }
}
